
translate chinese day3_main1_c980a67d:


    "我这一晚没有休息好，起床的时候全身心的感到疲倦。"


translate chinese day3_main1_d58e9a94:


    "这种事情总是在发生：梦里边发生了好多神奇的事情，好像进入了一个好莱坞动作大片之中，明星云集，特效烧钱，然后你早上醒来一点不记得。"


translate chinese day3_main1_5cd53aa6:


    "我就是在试着做这件事情：取回大脑放松时的记忆。不过我什么也取不到，要么是被加密了，或者就是被格式化了什么的。"


translate chinese day3_main1_cc6f7f6e:


    "表上显示现在是7点钟。"


translate chinese day3_main1_4b2cb0f9:


    th "今天我好像起的挺早。"


translate chinese day3_main1_a9a851c3:


    "奥尔加·德米特里耶夫娜还在睡觉，紧紧的裹在被子里。"


translate chinese day3_main1_2ee0953d:


    th "裹得这么紧？在这样炎热的夏天？呃..."


translate chinese day3_main1_de284251:


    "我起来伸了个懒腰，走到镜子前照照自己的模样。"


translate chinese day3_main1_eb6077b0:


    th "我最好刮刮胡子。"


translate chinese day3_main1_832ce63c:


    "这主意很奇怪，过去一个月两次就行，现在似乎没有什么和我的长相有关。"


translate chinese day3_main1_4e0332f2:


    "反正我也找不到刮胡刀。"


translate chinese day3_main1_cbc809e5:


    th "而且在这种看起来最多17岁的情况下，我为什么需要刮啊？"


translate chinese day3_main1_eddcb947:


    th "那么...{w}9点之前还吃不上早饭，也就是说我还有充裕的时间。"


translate chinese day3_main1_26c1194a:


    "比如说我可以散散步，洗洗脸。"


translate chinese day3_main1_4144add1:


    "水没有昨天那么冰了。"


translate chinese day3_main1_289c3fa7:


    "相反，冰冷的感觉可以让我清醒，回到现实（不过这些事情到底能不能叫做现实）"


translate chinese day3_main1_399d144c:


    "牙粉用起来也没有多么落伍，我突然感觉它和牙膏其实没什么区别。"


translate chinese day3_main1_300a064a:


    "等我回到宿舍的时候，奥尔加·德米特里耶夫娜已经起来了。"


translate chinese day3_main1_d6087460:


    "她正站在镜子前整理头发。"


translate chinese day3_main1_b8d13a2a:


    mt "早上好，谢苗！"


translate chinese day3_main1_3773033f:


    me "好..."


translate chinese day3_main1_c0392c73:


    "因为缺乏睡眠，我的脑袋还在嗡嗡响，各种想法成了一团浆糊，不过我并没有后悔早起而不是像平常一样赖床。"


translate chinese day3_main1_52b8b2dd:


    mt "你怎么起的那么早？"


translate chinese day3_main1_a3d19ed0:


    me "我也不知道..."


translate chinese day3_main1_3e828ab9:


    mt "又没有带红领巾，让我帮你..."


translate chinese day3_main1_e3b2ca3e:


    me "我自己来吧，又不是什么大事...去食堂的路上就解决了。"


translate chinese day3_main1_74f457d8:


    mt "我可不这么认为！现在就做！{w}你忘了早饭之前要站队集合了吗？"


translate chinese day3_main1_3ac43883:


    th "我怎么可能忘掉！"


translate chinese day3_main1_a9616761:


    me "那么今天集合的时候要讲什么内容呢？"


translate chinese day3_main1_ac9273a5:


    mt "我们要讨论今天的日程。"


translate chinese day3_main1_a3da2eed:


    me "所以说日程是什么？"


translate chinese day3_main1_689ac639:


    "说实话，我不在乎勇敢的少先队员们有{i}什么{/i}任务，我在乎的是我们伟大的指导员要带领少先队员们去{i}哪里{/i}。{w}也许我可以设法避开敌军..."


translate chinese day3_main1_045f8001:


    mt "你集合的时候就会明白了！"


translate chinese day3_main1_945c59bc:


    "她对我狡猾的一笑。"


translate chinese day3_main1_fb73d398:


    me "奥尔加·德米特里耶夫娜，我的胃不太舒服，我还是..."


translate chinese day3_main1_6e70eda4:


    me "呃，这个你懂的..."


translate chinese day3_main1_c0565634:


    me "我只是缺勤一次没什么问题吧，可如果我在集合的时候失去控制，可能会发生一些无法挽回的事情..."


translate chinese day3_main1_923b9fdb:


    th "我这勇气都是哪来的？"


translate chinese day3_main1_063c7cf2:


    mt "我多给你五分钟。"


translate chinese day3_main1_95a2da76:


    me "恐怕我还需要更多的时间，{w}你能不能现在告诉我呀~"


translate chinese day3_main1_7ffb918a:


    me "好吧，我试着忍着好了，不过万一出了问题..."


translate chinese day3_main1_bdf2ab42:


    mt "好啦！{w}今天的第一件事情是营地的扫除，然后是在图书馆整理图书，还有一些琐事，我们晚上还有一个联欢会。"


translate chinese day3_main1_fa2ec241:


    mt "看着我！"


translate chinese day3_main1_dc6aa38c:


    mt "你今天必须参加夏令营的活动！"


translate chinese day3_main1_585915f9:


    th "那当然！"


translate chinese day3_main1_c2355535:


    me "没问题，那么现在我得..."


translate chinese day3_main1_41c48b54:


    mt "咱们上路吧！"


translate chinese day3_main1_2f2e2be3:


    "我走出小屋，躲在屋后的灌木丛中，等着奥尔加·德米特里耶夫娜离开。"


translate chinese day3_main1_8eee44c3:


    "实际上，我找不到这样做的理由。"


translate chinese day3_main1_740a8290:


    "不过我内心的声音告诉我去集合没有什么可干的。"


translate chinese day3_main1_a20cefa7:


    "..."


translate chinese day3_main1_1466986b:


    "等到辅导员离开以后，我回到了小屋里等着早饭开饭。"


translate chinese day3_main1_24e4a223:


    th "这...我今天没打算义务劳动什么的。"


translate chinese day3_main1_dfefa0e2:


    th "扫除整理—我现在的处境可不适合做那种事。"


translate chinese day3_main1_0ed74327:


    "已经两天过去了，我却还是一点进展也没有。"


translate chinese day3_main1_a8e4db15:


    "这里所有的居民看起来都像是普通人，好像并没有什么阴谋，也没有和外星人进行秘密交谈或是掌握什么时间旅行的奥秘。"


translate chinese day3_main1_e9ea2720:


    "诚然，每个人都有自己的特点，不过也都还算正常。"


translate chinese day3_main1_e9429608:


    "而且我要真是在现实世界中遇到了这些少先队员，估计也并不会太过惊讶。"


translate chinese day3_main1_469fa83c:


    "感觉他们甚至比我还要正常。"


translate chinese day3_main1_32fd1d9f:


    "这其中似乎没有什么异常，要说异常就是找不到答案。"


translate chinese day3_main1_097dcaf5:


    th "普通人在一个特殊的地方。"


translate chinese day3_main1_2f89955c:


    th "我曾经见识过，不知在哪里..."


translate chinese day3_main1_23beae7b:


    th "很多个少先队员每天做着差不多的事情，他们不知道外面的世界并不是他们想象的那个样子。"


translate chinese day3_main1_c2aba579:


    th "好吧。{w}不过这是他们的观点。{w}我又和他们不熟。虽然他们在这种情况下显得也很正常。"


translate chinese day3_main1_83ea97da:


    th "我需要作出判断：那些人是局中人吗？"


translate chinese day3_main1_4427373a:


    th "我能从他们那里得到答案吗？"


translate chinese day3_main1_6d2752d6:


    "或者我应该去别的什么地方寻找答案？"


translate chinese day3_main1_a20cefa7_1:


    "..."


translate chinese day3_main1_ffb0123f:


    "看来我想多了，食堂这里并没有多少少先队员。"


translate chinese day3_main1_2fba02c8:


    "里边也没什么人。"


translate chinese day3_main1_fe31d110:


    "可能大多数孩子9点之前吃过饭了吧。"


translate chinese day3_main1_378c20a9:


    th "那正好，人少清静。"


translate chinese day3_main1_51afaa36:


    "列娜坐在食堂的那头，懒洋洋的用叉子叉着一坨像是粥一样的东西。"


translate chinese day3_main1_1fcd8de0:


    "和她一起吃饭是个好主意，我们可以安静的聊天。"


translate chinese day3_main1_33a88c19:


    th "至少可以尝试一下。"


translate chinese day3_main1_e70262c5:


    "我正要过去，突然有人抓住了我的袖子。"


translate chinese day3_main1_d960d8bf:


    "热尼娅，图书管理员。"


translate chinese day3_main1_c2aade8d:


    mz "拿着你的饭过来，我们需要谈谈。"


translate chinese day3_main1_51149e68:


    me "..."


translate chinese day3_main1_be6dc4ed:


    mz "你在等什么？"


translate chinese day3_main1_0b5fe8db:


    "我有点糊涂了。"


translate chinese day3_main1_1d988865:


    me "抱歉，这好像...很突然？"


translate chinese day3_main1_7ff5889f:


    mz "有什么问题吗？拿好你的饭然后坐下。"


translate chinese day3_main1_ab08d3e6:


    "我看她还觉得自己的行为一点问题没有呢。"


translate chinese day3_breakfast_un_c5c4ca0e:


    "我确定列娜会同意我坐在她旁边的。"


translate chinese day3_breakfast_un_20c17916:


    mz "别走！等一下！"


translate chinese day3_breakfast_un_9a143b5c:


    me "抱歉，也许，过一会儿吧。"


translate chinese day3_breakfast_un_1643039e:


    "我轻轻的挣脱了她的手，开始往食堂的那边走。"


translate chinese day3_breakfast_un_a9a20700:


    "热尼娅朝我看着什么，不过我没有在意。"


translate chinese day3_breakfast_un_b5bcbe39:


    me "嗨，早上好！"


translate chinese day3_breakfast_un_5cbc2f90:


    "听到了图书管理员的喊声，列娜偷偷往我这边看了看。"


translate chinese day3_breakfast_un_480fc425:


    un "早上好..."


translate chinese day3_breakfast_un_49f1d3e1:


    "奇怪，这次她没有脸红，而是笑了。"


translate chinese day3_breakfast_un_80a13296:


    me "我能和你一起吃饭吗？我马上就把我的那份拿过来。"


translate chinese day3_breakfast_un_3d4036d2:


    un "好的，当然。"


translate chinese day3_breakfast_un_a20cefa7:


    "..."


translate chinese day3_breakfast_un_ea39cb42:


    "一会儿过后我就带着一盘简单的早餐坐在她对面了：一碗粥，两个鸡蛋，四个小面包，香肠，水果饮料，还有一些不知名的水果什么的。"


translate chinese day3_breakfast_un_9aeb930e:


    me "吃好喝好~"


translate chinese day3_breakfast_un_626232a6:


    un "谢谢。"


translate chinese day3_breakfast_un_5a8d98a1:


    "我尽量保持自己的吃相雅观—不发出声音，不把周围弄脏。"


translate chinese day3_breakfast_un_7bb18871:


    th "看起来保持一个人的吃相也不是那么难。"


translate chinese day3_breakfast_un_997be624:


    "向平常一样，列娜一言不发，所以只能由我引出话题......"


translate chinese day3_breakfast_un_c7fca932:


    me "今晚去参加跳舞吗？"


translate chinese day3_breakfast_un_1aa04f80:


    un "可能会吧..."


translate chinese day3_breakfast_un_34d1d8f6:


    "她沉默了一段时间。"


translate chinese day3_breakfast_un_44be8128:


    un "你呢？"


translate chinese day3_breakfast_un_799c0105:


    me "我还不知道..."


translate chinese day3_breakfast_un_911940f8:


    "虽然我完全不想去。"


translate chinese day3_breakfast_un_cc5d1c46:


    "我上学的时候就不喜欢参加舞会..."


translate chinese day3_breakfast_un_49e77139:


    un "为什么呢？"


translate chinese day3_breakfast_un_3956a59c:


    me "什么意思？"


translate chinese day3_breakfast_un_121175cf:


    "我的意识还回荡在学生时代。"


translate chinese day3_breakfast_un_d3f48c81:


    un "你为什么不想去呢？"


translate chinese day3_breakfast_un_949e4fbd:


    me "我没说不想去啊..."


translate chinese day3_breakfast_un_da258ccf:


    un "那好吧。"


translate chinese day3_breakfast_un_19437dd5:


    me "不过如果你邀请我的话..."


translate chinese day3_breakfast_un_153a3cb2:


    "列娜脸变红了，看向一边。"


translate chinese day3_breakfast_un_d6e377c8:


    un "呃，我不是..."


translate chinese day3_breakfast_un_f61b93da:


    me "抱歉，我只是开玩笑的。"


translate chinese day3_breakfast_un_aa3ce0f5:


    "我有些疑惑，还是不要那样开玩笑比较好吧。"


translate chinese day3_breakfast_un_b993fafe:


    un "好吧。"


translate chinese day3_breakfast_un_3198fcbf:


    "列娜平淡的说道，低头专心吃着饭，脸变得更加红了。"


translate chinese day3_breakfast_un_32938f90:


    th "为什么我非得说这个舞会的问题啊？好像就没有别的话题了似的！{w}等一下，真的有什么..."


translate chinese day3_breakfast_un_998716c3:


    me "对了，也许你知道热尼娅刚才想问我什么？"


translate chinese day3_breakfast_un_d0ef9648:


    un "啊...？"


translate chinese day3_breakfast_un_d9512f33:


    me "我是说，我刚刚进来的时候，她好像找我有事。"


translate chinese day3_breakfast_un_66762623:


    un "我不知道..."


translate chinese day3_breakfast_un_855db9ee:


    me "我明白了。"


translate chinese day3_breakfast_un_67900051:


    "我成功的终结了对话，于是只能闷头吃饭了。"


translate chinese day3_breakfast_un_359fdf19:


    "列娜提前吃完了。"


translate chinese day3_breakfast_un_82988e32:


    un "我应该走了，一会儿见。"


translate chinese day3_breakfast_un_6384aca8:


    me "嗯，拜拜！"


translate chinese day3_breakfast_mz_e36ce35f:


    "我不太清楚为什么，不过还是接受了邀请。"


translate chinese day3_breakfast_mz_25dd7e46:


    th "要是我可以学到什么新东西呢？{w}她简直是个书虫。"


translate chinese day3_breakfast_mz_3159bf22:


    "一会儿过后我就带着一盘简单的早餐坐在她对面了：一碗粥，两个鸡蛋，四个小面包，香肠，水果饮料，还有一些不知名的水果什么的。"


translate chinese day3_breakfast_mz_9aeb930e:


    me "吃好喝好~"


translate chinese day3_breakfast_mz_ca4481dc:


    mz "谢谢。"


translate chinese day3_breakfast_mz_8df9aaff:


    "我尽量保持自己的吃相雅观—不发出声音，不把周围弄脏。"


translate chinese day3_breakfast_mz_ebae22b0:


    me "那么，你想说什么事情？"


translate chinese day3_breakfast_mz_f4ef416b:


    mz "今天在图书馆..."


translate chinese day3_breakfast_mz_f6feb39d:


    "我听不进去，开始回想辅导员同志关于今天行动的计划。"


translate chinese day3_breakfast_mz_63e91f2f:


    mz "...别忘了！"


translate chinese day3_breakfast_mz_0e394660:


    me "啊，什么？"


translate chinese day3_breakfast_mz_8af6f3a4:


    mz "我说别忘了午饭后去图书馆！"


translate chinese day3_breakfast_mz_7332342b:


    me "干什么？"


translate chinese day3_breakfast_mz_486c6399:


    mz "你究竟听没听啊？"


translate chinese day3_breakfast_mz_5a7a7577:


    me "没听。"


translate chinese day3_breakfast_mz_9fb02d3f:


    "我诚实地回答道。."


translate chinese day3_breakfast_mz_b4215fb3:


    "热尼娅的脸变红了，红里透着绿，绿里透着青。"


translate chinese day3_breakfast_mz_198f044e:


    mz "今天！午饭后！你！去图书馆！明白？你这二货！"


translate chinese day3_breakfast_mz_a758b252:


    th "二货？{w}怎么这么说话啊，{w}少先队员不能够说脏话，我都记着呢。"


translate chinese day3_breakfast_mz_0b0c4f9e:


    me "是！是！我错了！我会准时到达的！"


translate chinese day3_breakfast_mz_63573b0e:


    "说实话，我哪里也不想去，不过为了让一个人开口，你总得有点付出。"


translate chinese day3_breakfast_mz_55998771:


    me "顺便问一下，现在是那一年？我数不清了。"


translate chinese day3_breakfast_mz_42d07202:


    "我决定把话说开。"


translate chinese day3_breakfast_mz_f7f9fcdc:


    "她疑惑的看着我说道："


translate chinese day3_breakfast_mz_8ff4b6fa:


    mz "你脑子进水了？"


translate chinese day3_breakfast_mz_6303a3f4:


    me "实际上，我这几天是有这种感觉。"


translate chinese day3_breakfast_mz_133cbf39:


    "热尼娅没有回答。"


translate chinese day3_breakfast_mz_b1214d8e:


    me "所以说，今年是那一年？"


translate chinese day3_breakfast_mz_085d124a:


    "我善良的笑了。"


translate chinese day3_breakfast_mz_4899008d:


    mz "听着，你怎么不去看看医生。"


translate chinese day3_breakfast_mz_fddf5e90:


    me "他们会告诉我哪一年吗？"


translate chinese day3_breakfast_mz_4686520d:


    mz "哦，当然，还会告诉你别的！"


translate chinese day3_breakfast_mz_63613035:


    "热尼娅站起来往出口走去。"


translate chinese day3_breakfast_mz_6999c456:


    me "等一下，你没有吃完饭啊..."


translate chinese day3_breakfast_mz_e9ec16dc:


    "不过她看起来没有要回头的意思。"


translate chinese day3_main2_0c42e260:


    "我在费力地吃着一根香肠，那感觉像是一股缆绳。"


translate chinese day3_main2_d8c4da3f:


    me "我很好奇他们这里有没有互联网？"


translate chinese day3_main2_c0d3e816:


    me "我看是没有。"


translate chinese day3_main2_7ce38afc:


    "坐在旁边桌子的一位红领巾发现我在自言自语，于是奇怪的看着我，我微笑着挥手致意。"


translate chinese day3_main2_cec89b26:


    "受到了我热情的问候之后，不知怎么的，他饭都没吃完就提前离开了食堂。"


translate chinese day3_main2_b91a15cc:


    th "吃完饭，现在该寻找线索了。"


translate chinese day3_main2_db7938e1:


    "过了一会儿，我终于来到了医务室。"


translate chinese day3_main2_80770b79:


    th "我好像没有必要去那个地方吧。"


translate chinese day3_main2_495d074a:


    "虽然护士阿姨很有爱心，可是还是离她远一点好。"


translate chinese day3_main2_0c5af512:


    "如此的关爱实在是让人受宠若惊啊。"


translate chinese day3_main2_4357f4cf:


    "她可能知道什么，不过我还是没有勇气走进医务室。"


translate chinese day3_main2_7ce2c0d2:


    th "而且，我也没有理由去，{w}一个都没有。"


translate chinese day3_main2_f48ed40c:


    "我沉浸在自己的想法里，没有看到护士就站在门口。"


translate chinese day3_main2_e091fcfe:


    cs "你好啊...少先队员！"


translate chinese day3_main2_b93477a9:


    "她靠近了一步。"


translate chinese day3_main2_b306dd8f:


    cs "你一定是生病了，你是来取药的吗？"


translate chinese day3_main2_23823de6:


    me "不，不...我只是在这附近闲逛..."


translate chinese day3_main2_10ca59a3:


    cs "哈哈，原来是逃学呀。"


translate chinese day3_main2_d264f0d9:


    cs "不过既然你来了这里，我有个严峻的任务交给你。"


translate chinese day3_main2_4a52b166:


    "我疑惑的看着她。"


translate chinese day3_main2_54a8b79a:


    th "任务这个词让我不太舒服，感觉是无法避免的事情。"


translate chinese day3_main2_82b55e26:


    "护士狡猾的笑着。"


translate chinese day3_main2_972a2399:


    cs "你们俩一起帮我登记一下今天刚刚运到的药品。"


translate chinese day3_main2_35ca7b8b:


    th "俩？{w}我们在哪儿？{w}我是不是听错了？"


translate chinese day3_main2_51c41ef6:


    th "可是什么字和‘俩’听起来很像吗？"


translate chinese day3_main2_8e5e0396:


    un "你好，又见面了..."


translate chinese day3_main2_e8479859:


    un "你好..."


translate chinese day3_main2_3b501dc9:


    "列娜从护士的背后出现了。"


translate chinese day3_main2_f4ec6c0a:


    "我一直没有注意到她，很奇怪。"


translate chinese day3_main2_32f43013:


    cs "晚饭之后过来这里，我和列娜都说过了，你听她讲就可以了。"


translate chinese day3_main2_1b0bb99e:


    th "药品呵，今天刚到。"


translate chinese day3_main2_edbbd70f:


    th "奥尔加·德米特里耶夫娜还告诉我说几天之内都没有公交车呢。"


translate chinese day3_main2_513f14a5:


    me "所以说，今天有人来到这个营地吗？"


translate chinese day3_main2_d589ffac:


    cs "是的，他们从城市过来，有什么问题吗？"


translate chinese day3_main2_0baf4d95:


    me "只是好奇..."


translate chinese day3_main2_29261639:


    cs "那好吧...少先队员！{w}那么，今晚，晚饭后，在这里等你！"


translate chinese day3_main2_918e5db5:


    un "也许我..."


translate chinese day3_main2_f1184b36:


    "整个过程中列娜都没有什么存在感。"


translate chinese day3_main2_5a680cf2:


    "对啊，她可以像一个忍者一样保持隐形。"


translate chinese day3_main2_0dac54fc:


    un "我一个人就可以的。"


translate chinese day3_main2_c4e56f03:


    "奇怪，她看起来比吃饭时更有精神了。"


translate chinese day3_main2_07ecb7bb:


    cs "不行，有很多大箱子。"


translate chinese day3_main2_189f0241:


    cs "而且一个少先队员要时刻准备着！"


translate chinese day3_main2_92699031:


    cs "不是吗？...少先队员同志？"


translate chinese day3_main2_96567aee:


    "她看着我又狡猾的笑着。"


translate chinese day3_main2_fb4b4310:


    "我可不想一晚上干这个，而且我还有更重要的事情呢。"


translate chinese day3_main2_b7a89246:


    "不过拒绝这个要求又不合适。"


translate chinese day3_main2_e73ac8a5:


    "如果不是列娜在这里的话，我肯定要想辙溜了。"


translate chinese day3_helpaccept_752f2744:


    cs "不错！{w}这是一个真正的少先队员应有的回答！"


translate chinese day3_helpaccept_512b33c1:


    "我从来没有接近做到‘时刻准备着’。"


translate chinese day3_helpaccept_eb225364:


    cs "那么，你可以走了。"


translate chinese day3_helpaccept_85a9dc87:


    "我看了看列娜，她还在低头看着地面。"


translate chinese day3_helpaccept_83ee98b6:


    "我觉得她看着脚下的时间加在一起都可以做昆虫的生活习性研究了。"


translate chinese day3_helpaccept_80cb7fbb:


    "再加上列娜还喜欢读书，所以她可能看过各种关于动物植物学的书籍。"


translate chinese day3_helpaccept_84a80626:


    th "也许她想要做一名昆虫学家？"


translate chinese day3_helpaccept_a20cefa7:


    "..."


translate chinese day3_helpaccept_9a9ab591:


    un "你一会儿去哪儿？"


translate chinese day3_helpaccept_e6c70fa7:


    "列娜的话把我从乱七八糟的想法中拽了回来。"


translate chinese day3_helpaccept_928a1e98:


    "我们已经在广场呆了一段时间了，而我对昆虫的观察活动—只是为了逃离现在的窘境。"


translate chinese day3_helpaccept_553ade04:


    th "不过，我为什么觉得这是窘境啊？"


translate chinese day3_helpaccept_4f92bcbe:


    un "只要别忘了就好，好吗？"


translate chinese day3_helpaccept_56e472ea:


    me "别忘了什么？"


translate chinese day3_helpaccept_7ae819c0:


    un "那个...今晚...在医务室..."


translate chinese day3_helpaccept_0734476b:


    "她有点生气的表情。"


translate chinese day3_helpaccept_d7ab165c:


    th "没门啊，那不可能！"


translate chinese day3_helpaccept_41dde6d3:


    me "好吧，好吧！吃过晚饭后听您吩咐！"


translate chinese day3_helpaccept_31f1800b:


    "我突然明白过来，不过晚了，列娜的脸越来越红了。"


translate chinese day3_helpaccept_3d2683cd:


    "我需要找一个方法缓解现在的尴尬处境。"


translate chinese day3_helpaccept_e57a438b:


    me "护士为什么找你来做这件事啊？"


translate chinese day3_helpaccept_dc5220db:


    un "我不知道。{w}我当时只是坐在长椅上看着书，然后她过来..."


translate chinese day3_helpaccept_3d5bff62:


    th "真是聪明啊，找一个靠得住又不会拒绝别人的孩子。"


translate chinese day3_helpaccept_5364658a:


    me "OK。{w}那好吧，晚饭后..."


translate chinese day3_helpaccept_37b80610:


    un "嗯。{w}到时候见！"


translate chinese day3_helpaccept_4bfb0b4b:


    me "是啊，当然！"


translate chinese day3_helpaccept_92254f5c:


    "她往社团那边走去了，我接着在广场上站了一段时间。"


translate chinese day3_helpreject_28e3d09b:


    th "也许我不应该和她撒谎。"


translate chinese day3_helpreject_78a68a86:


    "我说话之前应该过脑子的。"


translate chinese day3_helpreject_64ea43f2:


    cs "真的吗？你具体是要帮她做什么呢？"


translate chinese day3_helpreject_90328db4:


    "事情有些不妙，不过我决定一路走到黑。"


translate chinese day3_helpreject_20aa6bf2:


    me "她的房间...{w}她让我帮她收拾屋子。"


translate chinese day3_helpreject_eb1f3b24:


    me "你肯定见识过她的房间有多乱吧！"


translate chinese day3_helpreject_71a1959c:


    cs "是啊，她也要求你帮她打扫衣柜了吧？"


translate chinese day3_helpreject_bd7955c4:


    "护士使劲等着我，以至于我觉得拿破仑在滑铁卢的惨败都不算什么了。"


translate chinese day3_helpreject_4d02b8d1:


    "我还想说点什么，她又说了下去。"


translate chinese day3_helpreject_bd364f86:


    cs "那好吧...少先队员。{w}我们自己尽力吧。"


translate chinese day3_helpreject_48cb1418:


    me "抱歉，不过，你看..."


translate chinese day3_helpreject_a1643280:


    "我看着列娜说着。"


translate chinese day3_helpreject_bd547c7b:


    "她看着脚下，脸变得越来越红。"


translate chinese day3_helpreject_28645b37:


    un "没关系...我自己会想办法的..."


translate chinese day3_helpreject_084c94a3:


    me "好吧。"


translate chinese day3_helpreject_ffe91023:


    "接下来是一阵尴尬的沉默。"


translate chinese day3_helpreject_6c088e5b:


    "我那个糟糕的谎言可能让列娜伤心了，我真想陷到地里面去。"


translate chinese day3_helpreject_1c49e0e4:


    un "好，那么，再见？"


translate chinese day3_helpreject_670845c1:


    me "再见。"


translate chinese day3_helpreject_5ae42b21:


    "护士咬牙切齿的笑着。"


translate chinese day3_helpreject_d6986490:


    "我转身离开了，几乎是在跑步。"


translate chinese day3_helpreject_5c0a5a05:


    "虽然事实上我的确想要跑起来，不过常识告诉我不能那样做。"


translate chinese day3_helpreject_53287076:


    "我来到广场的时候恢复了理智。"


translate chinese day3_main3_16660e40:


    th "那么接下来又是什么事..."


translate chinese day3_main3_212cd6fc:


    "我决定返回小屋，好拿上我的电话。"


translate chinese day3_main3_a2b246f4:


    th "我不知道今天到底会发生什么事情，不过带个表总是极好的！"


translate chinese day3_main3_a387e44b:


    th "虽然说有一块手表就更好了。"


translate chinese day3_main3_2aff1bdf:


    "突然我听到了一个不太一样的声音，盖过了营地里其他的声音。"


translate chinese day3_main3_f9785632:


    "我仔细的听着，{w}听起来像是电吉他。"


translate chinese day3_main3_adb705e3:


    "三声重复的和弦，没有更多了。"


translate chinese day3_main3_f1b3a7a5:


    "可是这个旋律却有一种暖暖的感觉，好像是用了真空管扩音器似的。"


translate chinese day3_main3_93557c84:


    th "这里可能有那么贵重的东西吗？"


translate chinese day3_main3_a012ce31:


    "声音肯定是从舞台那边传过来的。"


translate chinese day3_main3_da2b4460:


    th "我很想知道那是谁。"


translate chinese day3_main3_32f4a81d:


    th "那是谁很重要吗？我不是有自己的事情要做吗？"


translate chinese day3_house_of_mt_c93a9c6d:


    "我回到了辅导员的小屋。"


translate chinese day3_house_of_mt_81f57dce:


    "奥尔加·德米特里耶夫娜懒洋洋的躺在床上看着书。"


translate chinese day3_house_of_mt_ca064f09:


    mt "谢苗！你在这里干什么？"


translate chinese day3_house_of_mt_ff169076:


    th "我也想问她这个问题呢。"


translate chinese day3_house_of_mt_ba187c2f:


    me "我忘了点东西...{w}所以就回来了。"


translate chinese day3_house_of_mt_63ff0148:


    mt "你集合的时候缺勤了！"


translate chinese day3_house_of_mt_12ae07e8:


    me "噢，是啊，我道歉..."


translate chinese day3_house_of_mt_c5cabca5:


    mt "算了。{w}不管怎样，你今天必须要做服务社会的事情。"


translate chinese day3_house_of_mt_cea8428c:


    "就和我打算的一样。{w}不过我对服务社会有自己的理解。"


translate chinese day3_house_of_mt_e473fa2f:


    me "比如？"


translate chinese day3_house_of_mt_2410adae:


    mt "你可以去广场，帮忙扫除，斯拉维娅在那里负责。"


translate chinese day3_house_of_mt_4bc77a00:


    th "奇怪，我刚刚到过那里，一个人都没有。"


translate chinese day3_house_of_mt_bce5b940:


    mt "你也可以去电子社团给那些男生帮帮忙，他们请求过我。"


translate chinese day3_house_of_mt_280f15cf:


    th "又是那些“发明之父”们！"


translate chinese day3_house_of_mt_ad452bb7:


    mt "最后，你也可以在体育社团帮忙。"


translate chinese day3_house_of_mt_e2d85953:


    me "他们是干什么的？"


translate chinese day3_house_of_mt_1ab1a7d4:


    mt "他们正在打扫运动场：修理长椅，更换球门网等等。"


translate chinese day3_house_of_mt_00d98ab0:


    "说真的，这些工作没有一个适合我。"


translate chinese day3_house_of_mt_3f6ade2d:


    th "她为什么总是想让我干活儿？"


translate chinese day3_house_of_mt_14075888:


    th "讲真，除了斯拉维娅，没有人在这里工作，而有的人在这里只是在享受她们的青春。"


translate chinese day3_house_of_mt_88a88f60:


    "辅导员的每一个决定都是让我忙起来，作用都是一个：防止我发现这是哪里。"


translate chinese day3_house_of_mt_c75c7820:


    "真的，有深度的至关重要的那些问题都被她用扫帚一把扫清，连带着尘土和垃圾一起请出大门。"


translate chinese day3_house_of_mt_11998600:


    "对于我来说，也许去干活儿能有些收获，不过我决定像生了坏疽却拒绝截肢的人一样，坚持我的信念。"


translate chinese day3_house_of_mt_c8e476b7:


    me "我已经有了我自己的安排。"


translate chinese day3_house_of_mt_f11d83c9:


    mt "真的吗？是什么样的安排呢？"


translate chinese day3_house_of_mt_8efbf7e2:


    "她来了精神。"


translate chinese day3_house_of_mt_f883183e:


    me "这个..."


translate chinese day3_house_of_mt_eec772da:


    th "我不能把自己真实的想法告诉她！"


translate chinese day3_house_of_mt_b155d7d8:


    th "另一方面来说，告诉又有何妨？"


translate chinese day3_house_of_mt_e7621f5a:


    th "不行，那太危险了！"


translate chinese day3_house_of_mt_d4332ab2:


    th "至少现在我可以保持一个不受威胁的微妙状态，{w}所有事情都保持着平衡。"


translate chinese day3_house_of_mt_4c7fb1b8:


    mt "就和我想的一样！"


translate chinese day3_house_of_mt_508a7e16:


    "她是怎么{i}想的{/i}还是个谜。"


translate chinese day3_house_of_mt_3a32c862:


    mt "你大概还不知道参加社会活动有多么重要吧？{w}这是成为光荣的少先队员必须要做的！"


translate chinese day3_house_of_mt_34d07d39:


    "我不想再听一场无聊的演讲，所以决定找一个活儿干。"


translate chinese day3_house_of_mt_3a33dc3e:


    th "有时候答案就在这个世界上最不可能的地方，{w}至少我想这样相信..."


translate chinese day3_house_of_mt_0137337f:


    mt "那么，要我来帮你选择吗？"


translate chinese day3_house_of_mt_d6a8a466:


    "辅导员的声音变得威严起来。"


translate chinese day3_house_of_mt_a0d17775:


    me "我自己就可以的，谢谢..."


translate chinese day3_house_of_mt_1553fbf7:


    "我嘀嘀咕咕的向外走去。"


translate chinese day3_square_b65de720:


    "两害相权取其轻，现在是三害。"


translate chinese day3_square_0685b6b9:


    "斯拉维娅不是害，所以我的选择很明显。"


translate chinese day3_square_c3fd147b:


    "有些激进的人总是不停地打扫屋子，可我觉得这和在健身房锻炼一样，有好处，但是我就是不想去。"


translate chinese day3_square_bd3a88ba:


    "然而，给长椅上漆或者陪同几个未来的俄罗斯（或者是苏联？）天才搞发明更让我烦。"


translate chinese day3_square_52919e7f:


    "广场上有十来个少先队员。"


translate chinese day3_square_b54a584e:


    th "他们都是从哪里来的，好奇怪。"


translate chinese day3_square_70ff76da:


    "我走向斯拉维娅。"


translate chinese day3_square_1a8f3935:


    me "嗨！你好。"


translate chinese day3_square_c2326fc9:


    sl "嗨！你来帮忙了吗？"


translate chinese day3_square_eac909f4:


    me "嗯，虽然是被迫来的..."


translate chinese day3_square_f0f5042d:


    "我又想了一遍。"


translate chinese day3_square_01e9adbb:


    sl "我明白了。"


translate chinese day3_square_85d5b601:


    sl "拿着这把笤帚，你负责雕像旁边的区域。"


translate chinese day3_square_59f34355:


    "我倒是觉得这里已经够干净的了。"


translate chinese day3_square_8c05f3f7:


    "虽然还有点垃圾。"


translate chinese day3_square_a20cefa7:


    "..."


translate chinese day3_square_4bf5b11e:


    "打扫了一段时间之后我找到斯拉维娅，她正坐在长椅上休息。"


translate chinese day3_square_3d0d567b:


    me "真是个好天气，是吧？"


translate chinese day3_square_93a88d17:


    sl "是啊，就是有点热。"


translate chinese day3_square_2a6651bc:


    "她用手遮住额头往天空上看去。"


translate chinese day3_square_d1bfa7c3:


    me "你就像是一只共产主义无私的小蜜蜂。"


translate chinese day3_square_b61ed746:


    sl "嗨，算了，我只是热心肠而已！"


translate chinese day3_square_69d18c31:


    me "那多好啊。"


translate chinese day3_square_5c7b7c48:


    sl "你呢？"


translate chinese day3_square_08572abe:


    me "我什么？"


translate chinese day3_square_7b636361:


    sl "你好像感觉社区服务是一种负担？"


translate chinese day3_square_d76f93fa:


    me "嗯，有可能。"


translate chinese day3_square_70f37a53:


    sl "为什么呢？"


translate chinese day3_square_7479b07e:


    me "我说不上来..."


translate chinese day3_square_03e6aa1b:


    th "说真的，她不能要求我把所有心里话都说出来吧？"


translate chinese day3_square_5c2a445c:


    th "她会窒息的，上帝不会允许！"


translate chinese day3_square_b87ff550:


    sl "也许你只是不想有人在一起？"


translate chinese day3_square_b9c3fa63:


    me "那倒是有可能。"


translate chinese day3_square_f5936064:


    "斯拉维娅的话有一些道理。{w}她大概是善于猜透别人的想法。"


translate chinese day3_square_553e30c8:


    "或者至少是猜透我的想法。"


translate chinese day3_square_cff06c81:


    th "可是，为什么我有时候觉得自己都很难理解自己？！"


translate chinese day3_square_6466d398:


    sl "下一个阶段你准备做什么呢？"


translate chinese day3_square_e54b39e9:


    me "那是什么意思？"


translate chinese day3_square_15860af3:


    sl "嗯，你是想要上大学呢，还是准备找工作？"


translate chinese day3_square_08e5bdfd:


    "我已经是我工作上的砖家了——看动漫和上网。"


translate chinese day3_square_862a00b6:


    me "我还不知道，你呢？"


translate chinese day3_square_65f5243e:


    sl "我们家有个小农场，我打算回家帮忙。"


translate chinese day3_square_42782762:


    "这很奇怪，苏联有私人农场吗？不是都被收归国有了吗？"


translate chinese day3_square_a5764d60:


    "不过我决定不过问细节。"


translate chinese day3_square_c14f0a08:


    sl "你的父母是做什么的呢？"


translate chinese day3_square_f74fad8a:


    "这可不是个好的话题。"


translate chinese day3_square_2975c9e6:


    "不是说他们不是好人，相反，他们都是很善良的人。"


translate chinese day3_square_0a57324e:


    "只是时机不合适。"


translate chinese day3_square_884ea735:


    me "我的父亲是政府职员，母亲是老师。"


translate chinese day3_square_0130c38d:


    "那不完全是事实。{w}算是一半事实吧。"


translate chinese day3_square_a9edc837:


    sl "好厉害。"


translate chinese day3_square_5451282f:


    "她好像真的觉得很厉害似的。"


translate chinese day3_square_907b6d20:


    me "也许是吧..."


translate chinese day3_square_14c9829e:


    "又没什么可说的了，所以我移开了视线，往脚下看去，或是看看天，看看四周。"


translate chinese day3_square_d0928e48:


    sl "那个，我也很确定你也能成为一个有用的人。"


translate chinese day3_square_edee7f76:


    "斯拉维娅深沉的看着我。"


translate chinese day3_square_e4df3286:


    th "这是什么意思？"


translate chinese day3_square_4752f785:


    th "那个“也”是什么意思？"


translate chinese day3_square_1e5c4921:


    me "谢，谢谢..."


translate chinese day3_square_87af141e:


    "我自言自语。"


translate chinese day3_square_e18b6de3:


    me "你是什么意思呀？"


translate chinese day3_square_2f5cbfd7:


    sl "我觉得你有的时候太悲观了。"


translate chinese day3_square_be54772f:


    th "这是不是有点，太直白了？"


translate chinese day3_square_2edc6114:


    me "也许吧。"


translate chinese day3_square_a6743adf:


    sl "不过你没问题的！"


translate chinese day3_square_2edc6114_1:


    me "也许吧。"


translate chinese day3_square_723945e3:


    "这话说的我有点恍惚。"


translate chinese day3_square_80cd1196:


    "不过她其实也没有说什么特殊的。"


translate chinese day3_square_fbf07be0:


    "总的来说，谁处在那个位置大概都会得出差不多的结论。"


translate chinese day3_square_e331d636:


    "不管怎么说，我又一次觉得斯拉维娅可以看透我的心。"


translate chinese day3_square_a3f06af0:


    me "那么咱们继续打扫吧。"


translate chinese day3_square_f021de41:


    sl "嗯。"


translate chinese day3_square_31b44837:


    "她微笑着拿起了扫帚。"


translate chinese day3_square_5475d679:


    "我没想到打扫卫生还可以是如此的享受。"


translate chinese day3_square_68594816:


    "这个对话没有让我感到失落，相反和斯拉维娅谈话十分愉快。"


translate chinese day3_square_a5d8ba12:


    "这是第一次，和别人谈论我的人生的时候他们没有批评我，没有强硬的用他们自己的经验指导我，没有试着评头论足，对我乱下定义。"


translate chinese day3_square_e172c3fb:


    "能有人支持我，让我很高兴。"


translate chinese day3_square_c7b3226b:


    th "不对，这不太对啊！"


translate chinese day3_square_01f6d7a7:


    "我高兴是因为{i}斯拉维娅{/i}支持我。"


translate chinese day3_square_a20cefa7_1:


    "..."


translate chinese day3_square_5daea6b4:


    "过了一会儿，营地里想起了音乐，提醒大家去吃午饭，我放下扫把，轻松的舒一口气，往食堂走去。"


translate chinese day3_clubs_7484a246:


    "也许这也不算大。"


translate chinese day3_clubs_f63f81c0:


    "那确实是个难做的决定。"


translate chinese day3_clubs_2a9a2232:


    th "是啊，浪费时间和几个20世纪上半叶科幻风格的小伙子鬼混实在是没什么意思。"


translate chinese day3_clubs_2344f7dc:


    th "不过要是我设法跟他们做出点有意思的事情呢？"


translate chinese day3_clubs_f8ea823f:


    th "至少可以试试。"


translate chinese day3_clubs_f58fc437:


    "我站在社团活动室的门口犹豫了一阵，最终还是扫清了疑虑打开了大门。"


translate chinese day3_clubs_a57a7173:


    "电子小子和舒里克正在桌子那里弯着腰仔细的学习着什么。"


translate chinese day3_clubs_247d8ad8:


    me "你们好啊，电子小子们，最近怎么样？"


translate chinese day3_clubs_0a24ed23:


    "我尽量表现的友好一些。"


translate chinese day3_clubs_994b5e60:


    el "你好，我们等你很久了！"


translate chinese day3_clubs_2b8da8a8:


    me "你是怎么...？"


translate chinese day3_clubs_ed2501a3:


    "电子小子好像很想当个大预言家。"


translate chinese day3_clubs_736892e2:


    me "那么，你们需要什么帮忙？"


translate chinese day3_clubs_b2eaaab0:


    sh "你得帮我们完成机器人。"


translate chinese day3_clubs_8b36978a:


    me "不可能。{w}那个我完全不懂。"


translate chinese day3_clubs_df6d570c:


    sh "别担心，我们会教你的。{w}你越是烦，我越会坚持..."


translate chinese day3_clubs_0e64dff5:


    el "...我可是很公平的！"


translate chinese day3_clubs_14702694:


    "电子小子愉快的加入了对话。"


translate chinese day3_clubs_034978a3:


    me "我更倾向于讨论人生的话题...{w}我有个问题，你们似乎已经有了答案了。"


translate chinese day3_clubs_51e049ed:


    sh "什么问题？"


translate chinese day3_clubs_db02021d:


    "他警戒的问道。"


translate chinese day3_clubs_3dcaa86c:


    me "你觉得时间旅行可能吗？"


translate chinese day3_clubs_d54b6c5c:


    el "你怎么会想这个问题？"


translate chinese day3_clubs_c7fd889f:


    "电子小子变得严肃起来了。"


translate chinese day3_clubs_09834bc7:


    me "这不是我的主意..."


translate chinese day3_clubs_1b1a3349:


    me "我昨天从图书馆拿了本书—H.G. Wells的《时间机器》，你们肯定看过，我正在思考这个问题。"


translate chinese day3_clubs_2db74799:


    sh "啊..."


translate chinese day3_clubs_a1121260:


    el "你想要穿越到未来看看那时候的生活是怎样的吗？"


translate chinese day3_clubs_9cd1fcea:


    me "不，不是那样...{w}我对回到过去更感兴趣。"


translate chinese day3_clubs_684146a1:


    el "为什么呢？"


translate chinese day3_clubs_01e55b39:


    me "我不知道。{w}为什么不呢？"


translate chinese day3_clubs_e63df87d:


    me "那么，你们是怎么想的呢？"


translate chinese day3_clubs_7094c1fe:


    sh "广义相对论预言了虫洞，{w}但是那个跟你也说不明白。"


translate chinese day3_clubs_4d434542:


    th "我看也是。"


translate chinese day3_clubs_fa38dcf0:


    sh "总之，这种推测会带来很多的逻辑悖论。"


translate chinese day3_clubs_d7d34f50:


    sh "比如说你如果穿越到过去杀死了自己，那么未来的自己就不存在了。"


translate chinese day3_clubs_14901a5e:


    me "这倒是..."


translate chinese day3_clubs_c54945d0:


    el "总之是非常不科学的。"


translate chinese day3_clubs_99f562b4:


    me "我知道了，可是万一这是可行的呢？"


translate chinese day3_clubs_999acc60:


    me "我是说，我需要一种什么工具来到达过去的时间吗?"


translate chinese day3_clubs_fc3a598d:


    me "还是说我只要睡着然后醒来就会发现我来到了陌生的时间陌生的地点。"


translate chinese day3_clubs_722901ae:


    sh "我确定现代科学无法解释你的问题。"


translate chinese day3_clubs_14c45b6b:


    th "好，他的话听起来就像是'Popular Mechanics'杂志。{w}我的暑假上还有几期呢。"


translate chinese day3_clubs_855db9ee:


    me "我明白了。"


translate chinese day3_clubs_4ca8b273:


    th "看来到这里来不是个好主意..."


translate chinese day3_clubs_84c5e8c5:


    sh "不过如果现在的理论有问题的话...{w}或者我们还没有发现足够的理论。"


translate chinese day3_clubs_a81295d2:


    sh "那么可以说一定是有可能的。"


translate chinese day3_clubs_2ad79933:


    me "好吧..."


translate chinese day3_clubs_d061ee8b:


    el "或者是拥有比人类更多知识的种族做出来这种事。"


translate chinese day3_clubs_2ad79933_1:


    me "好吧..."


translate chinese day3_clubs_d6791c6b:


    sh "他们可以用其他的方法进行时间旅行。"


translate chinese day3_clubs_1fe7cd27:


    me "好吧..."


translate chinese day3_clubs_fde5f662:


    th "所以说有一种人类的老大哥把我扔到了过去？"


translate chinese day3_clubs_738b4f20:


    th "有意思。{w}而且想想的话也有道理。"


translate chinese day3_clubs_ec5fec4e:


    me "那么如果你已经到了过去了，你如何发现这些事情呢？"


translate chinese day3_clubs_3d1c4a13:


    el "我们怎么可能知道嘛？！"


translate chinese day3_clubs_e6875f9c:


    "他们俩大声的笑着。"


translate chinese day3_clubs_d66197e0:


    me "好，谢谢你们。"


translate chinese day3_clubs_385627a7:


    "我准备离开。"


translate chinese day3_clubs_4469b6cb:


    el "嘿，等一下，机器人怎么办？"


translate chinese day3_clubs_a54d7085:


    me "我确定一切都没问题的！"


translate chinese day3_clubs_ebde4c68:


    "他们讲的这些理论好像都很有道理，不过我还是一点进展都没有。"


translate chinese day3_clubs_aaeb79f3:


    "我在营地里闲逛了一会儿，然后前往食堂。"


translate chinese day3_playground_us_8f643492:


    "我希望帮体育社团打扫的话可以让我逃过那些义务。"


translate chinese day3_playground_us_0eacc803:


    th "他们一定人手很多，我可以滥竽充数。"


translate chinese day3_playground_us_cf36ec08:


    "总之我得去一趟运动场，报个到什么的。"


translate chinese day3_playground_us_1ea33f5c:


    th "以防奥尔加·德米特里耶夫娜发现我怠工。"


translate chinese day3_playground_us_994a3de0:


    th "没准会有严厉的惩罚措施。"


translate chinese day3_playground_us_19538e61:


    "那种事情可是不在我的计划之内。"


translate chinese day3_playground_us_d1f23116:


    "就在这时，我看见足球场那里，没有刷漆，没有打扫活动，而是正在踢着足球比赛。"


translate chinese day3_playground_us_5c2f927e:


    "5打6。"


translate chinese day3_playground_us_6a412919:


    "我仔细看了一下，发现了乌里扬娜。"


translate chinese day3_playground_us_bb01375d:


    th "我就不打断了，我静静的看着。"


translate chinese day3_playground_us_4d2c583d:


    "队伍的设置非常不合理。"


translate chinese day3_playground_us_3156dd24:


    "一队都是小孩子，12岁左右。"


translate chinese day3_playground_us_826e23c2:


    "另一队都是青少年，大概中学生的样子。"


translate chinese day3_playground_us_2cec2c18:


    "他们还多一个人——乌里扬卡。"


translate chinese day3_playground_us_9cd2564c:


    "不过这倒是无所谓，多一个小女孩不会有什么两样吧。"


translate chinese day3_playground_us_e419209b:


    "很快我发现自己错了。"


translate chinese day3_playground_us_8eabf611:


    "乌里扬卡带球技术很好，晃过了一个又一个对手。"


translate chinese day3_playground_us_ec740a50:


    "她显然不怎么懂得配合，不过她的水平也不需要配合，她的得分让人好羡慕。"


translate chinese day3_playground_us_4ef98874:


    "我往前走了几步。"


translate chinese day3_playground_us_ad1edcfd:


    us "嗨！过来一起玩啊，对面少一个人！"


translate chinese day3_playground_us_c1521ece:


    me "我看那就算是社区服务了吧！"


translate chinese day3_playground_us_c8c094f8:


    us "我们全都干完了！"


translate chinese day3_playground_us_59c94eb5:


    "乌里扬卡生气的说到。"


translate chinese day3_playground_us_c5377880:


    "我观察了一下球场，长椅刚刚刷了漆，球网也换了新的。"


translate chinese day3_playground_us_f94073b8:


    th "他们是怎么做到的？"


translate chinese day3_playground_us_c9ad2b53:


    us "加入他们队吧！"


translate chinese day3_playground_us_c387cb1e:


    th "嗯，想想上次我没有参加，这次我还是不要再逃了吧。"


translate chinese day3_playground_us_fa4c2e93:



    nvl clear
    "中场开球。"


translate chinese day3_playground_us_231641a0:


    "我的开球很脆弱。"


translate chinese day3_playground_us_4e23a665:


    "实际上这也没那么难，球场顶多50米长，而且对面的守门员根本够不着横梁。"


translate chinese day3_playground_us_6f9f7610:


    "过了一会儿，比赛变成平局，虽然我们一开始就落后七八个球。"


translate chinese day3_playground_us_f50e5ca2:


    "不过反过来说，我上场之后她们队还是进了好多球。"


translate chinese day3_playground_us_676b6f90:


    "准确的说，就是她进的。"


translate chinese day3_playground_us_4c72f6da:


    "也不是说她是罗纳尔多还是贝克汉姆一样的大师，她只是把球踢起来然后追过去。"


translate chinese day3_playground_us_0a5728b2:



    nvl clear
    "然而这种无聊的战略却有很大的效果，我一个人也盯不住对面所有的人。"


translate chinese day3_playground_us_d2f46bc1:


    "我当然比他们玩的都好，可是往对面使劲灌显得太没有体育精神了。{w}友谊第一，比赛第二嘛。"


translate chinese day3_playground_us_6cc66158:


    "不过接下来乌里扬娜说到："


translate chinese day3_playground_us_0de2793e:


    us "点球{w}直接决胜负。"


translate chinese day3_playground_us_36f04bc6:


    "我决定不想输。"


translate chinese day3_playground_us_002ee4a4:


    "现在是11：9我们领先，所以直接拒绝不合适。"


translate chinese day3_playground_us_1c08886f:


    us "你守门，我踢，一会儿再换。"


translate chinese day3_playground_us_bcaae21b:



    nvl clear
    me "一人踢一次？"


translate chinese day3_playground_us_df6dbf74:


    us "是的。"


translate chinese day3_playground_us_04bd3ea8:


    "我在门前准备好。"


translate chinese day3_playground_us_db151b99:


    "她开始助跑...{w}然后把球送进我的怀中。"


translate chinese day3_playground_us_57a622de:


    us "这不公平！"


translate chinese day3_playground_us_16bdda4a:


    me "什么不公平？"


translate chinese day3_playground_us_098f4867:


    us "我再踢一次。"


translate chinese day3_playground_us_3065208e:


    me "好吧，踢吧。"


translate chinese day3_playground_us_d602bb2d:


    "我笑着说道。"


translate chinese day3_playground_us_a6e809e4:


    "这一次要是在橄榄球中就算是好球了。"


translate chinese day3_playground_us_57923b1f:


    me "再踢一次？"


translate chinese day3_playground_us_76b2fe88:


    nvl clear


translate chinese day3_playground_us_5ebb10b9:


    us "我拒绝！"


translate chinese day3_playground_us_393056bc:


    "她抱怨道。"


translate chinese day3_playground_us_f88907e2:


    us "不过你绝对进不了我的门！"


translate chinese day3_playground_us_f4d2ad6b:


    me "我们走着瞧..."


translate chinese day3_playground_us_05c10dae:


    "我准确的把球踢到右下角。"


translate chinese day3_playground_us_9f646b2e:


    "乌里扬娜一动没动。"


translate chinese day3_playground_us_94dc722a:


    "不过如此的点球是扑不着的。"


translate chinese day3_playground_us_34ad2df9:


    me "我好像赢了。"


translate chinese day3_playground_us_a01972f6:


    "她什么也没说，只是愤怒的看着我。"


translate chinese day3_playground_us_24a977a7:


    "我得缓解一下她的情绪。"


translate chinese day3_playground_us_f5bed6f9:


    me "别失望。我只是..."


translate chinese day3_playground_us_e3b0eb4b:


    "这个时候音乐响起，召集少先队员们去吃饭了。"


translate chinese day3_playground_us_f6bba3b9:


    us "好吧，我会追平你的！"


translate chinese day3_playground_us_a66acadc:


    "她笑着，挥着手往食堂跑去。"


translate chinese day3_playground_us_19a2ba8b:


    "我把球踢起来做了几个动作然后追了上去。"


translate chinese day3_stage_dv_57fadfbd:


    "来到了舞台这边我看到了阿丽夏。"


translate chinese day3_stage_dv_14e3da9e:


    "这个女孩而在尽情表演，闭着眼睛，踩着扩音器，随着音乐的节奏摇摆着身体。"


translate chinese day3_stage_dv_ebaf2447:


    "我想不起来这是什么旋律了不过我确定以前肯定听过。"


translate chinese day3_stage_dv_bbd1630b:


    th "可能是某种苏联摇滚？"


translate chinese day3_stage_dv_b91c885c:


    "我能说上来好几个这个风格的乐队，不过我也不是太懂。"


translate chinese day3_stage_dv_abb4c6d0:


    "音乐听起来十分简单，只是和谐的三和弦。"


translate chinese day3_stage_dv_61c07cfb:


    "可能我练一练也能弹得出来吧。"


translate chinese day3_stage_dv_3ae0fcac:


    th "我很奇怪阿丽夏是从哪里学的。"


translate chinese day3_stage_dv_3e58d153:


    "这种音乐在苏联不受欣赏。"


translate chinese day3_stage_dv_923112f9:


    "未来的摇滚明星没有注意到我。"


translate chinese day3_stage_dv_3aea360d:


    "她好像想要和音乐融为一体，让每一个音符回想起来。"


translate chinese day3_stage_dv_d8f81d0f:


    "八十年代的摇滚明星出现在了我的眼前，很多人也像她一样现身音乐了吧。"


translate chinese day3_stage_dv_a08c8857:


    th "如果她也穿上那个时代的衣服而不是这套少先队服的话，别人大概是看不出什么区别的。"


translate chinese day3_stage_dv_8ec8402b:


    "她的{i}表演{/i}结束了。"


translate chinese day3_stage_dv_2a480da6:


    "弹完最后几个音符，阿丽夏注意到了我。"


translate chinese day3_stage_dv_13716aa5:


    dv "你喜欢吗？"


translate chinese day3_stage_dv_4f67d38e:


    "她不像我想象的那么惊讶。"


translate chinese day3_stage_dv_4041176b:


    me "是啊，不错啊。"


translate chinese day3_stage_dv_944e8574:


    dv "好像你比我厉害似的！"


translate chinese day3_stage_dv_495c9ac7:


    "阿丽夏顽皮的笑着。"


translate chinese day3_stage_dv_149c1b07:


    me "这我可没说啊，{w}我在音乐方面不太擅长。"


translate chinese day3_stage_dv_65c958c7:


    dv "那是。"


translate chinese day3_stage_dv_bd025218:


    me "嗯。"


translate chinese day3_stage_dv_d16a7088:


    "我不知道接下来该和她说什么，于是准备离开。"


translate chinese day3_stage_dv_f72c72fe:


    dv "等一下。"


translate chinese day3_stage_dv_ad02f53a:


    me "什么？"


translate chinese day3_stage_dv_4657233d:


    dv "你听说了晚上的舞会了吗？"


translate chinese day3_stage_dv_f14dc8aa:


    me "我以为是联欢..."


translate chinese day3_stage_dv_f48c6409:


    dv "联欢会，舞会，有什么区别嘛。"


translate chinese day3_stage_dv_823e3ce8:


    "她皱起了眉头。"


translate chinese day3_stage_dv_c6a323d5:


    me "嗯，我听说了，所以说？"


translate chinese day3_stage_dv_a67a2d53:


    dv "你准备去吗？"


translate chinese day3_stage_dv_2625e9c4:


    "她说完这些话就转过身去。"


translate chinese day3_stage_dv_0e943fd7:


    me "我不知道，你呢？"


translate chinese day3_stage_dv_2e685a48:


    dv "我去那里干什么？看一群蠢货？"


translate chinese day3_stage_dv_30498730:


    "阿丽夏说的也不是没有道理，不过说的如此直白还是让我有一些吃惊。"


translate chinese day3_stage_dv_a94386e7:


    "她讨厌的肯定不止是舞会。"


translate chinese day3_stage_dv_977aad9c:


    me "可是为什么呢？"


translate chinese day3_stage_dv_a35f4984:


    dv "什么为什么？"


translate chinese day3_stage_dv_99ad6855:


    me "就是说，你为什么不想来呢？{w}舞会都是挺有意思的嘛..."


translate chinese day3_stage_dv_c217ed8b:


    dv "你真的这样想吗？"


translate chinese day3_stage_dv_868730ed:


    "她的语气中有一丝不屑。"


translate chinese day3_stage_dv_9d80b9cf:


    me "我不知道，{w}可是我觉得你..."


translate chinese day3_stage_dv_86c3ec28:


    dv "我从来都不喜欢他们。"


translate chinese day3_stage_dv_0d425fdb:


    "她打断了我。"


translate chinese day3_stage_dv_c0fab6ae:


    dv "这种聚会上一点有意思的事情都没有！"


translate chinese day3_stage_dv_97523377:


    me "我明白了...那么你有什么计划呢？"


translate chinese day3_stage_dv_a2c6cc1c:


    "我继续问下去，只是想要继续谈话。"


translate chinese day3_stage_dv_81a1f427:


    dv "我会继续练习。"


translate chinese day3_stage_dv_2c8d8df5:


    me "练习什么？"


translate chinese day3_stage_dv_096cd639:


    dv "那首歌啊，傻子，你都听到了好不好！"


translate chinese day3_stage_dv_d4729704:


    "阿丽夏完全生气了，我只要再说错几个字，我的人生就完蛋了。"


translate chinese day3_stage_dv_5f54b3cd:


    me "那首曲子是谁写的？"


translate chinese day3_stage_dv_b4427906:


    dv "我写的！"


translate chinese day3_stage_dv_2d4780ec:


    me "所以说，就是你自己写的？"


translate chinese day3_stage_dv_5977fdff:


    dv "我自己写的！"


translate chinese day3_stage_dv_67492ba0:


    me "厉害..."


translate chinese day3_stage_dv_4933fe62:


    "空气中弥漫着一种尴尬的沉默。"


translate chinese day3_stage_dv_621c7bd3:


    me "那好吧..."


translate chinese day3_stage_dv_caeeba52:


    dv "你不想听吗？"


translate chinese day3_stage_dv_98aef877:


    me "我刚才不是听到了吗？"


translate chinese day3_stage_dv_0433bfa6:


    dv "我不是那个意思！"


translate chinese day3_stage_dv_e16a3b7b:


    "她撅起嘴来。"


translate chinese day3_stage_dv_83ff5116:


    dv "整个曲子！"


translate chinese day3_stage_dv_0d499a44:


    me "啊...那好吧，赶快开始吧..."


translate chinese day3_stage_dv_2af74aa2:


    dv "不是现在！"


translate chinese day3_stage_dv_9e500e36:


    me "那就不要表演了..."


translate chinese day3_stage_dv_46a31379:


    "我不太明白她想要我做什么。"


translate chinese day3_stage_dv_3564e101:


    dv "你是说你不想听我表演。"


translate chinese day3_stage_dv_1594019e:


    th "好像我让她失望了。"


translate chinese day3_stage_dv_9a66cc72:


    "我准备防御砸向我头上的吉他一击甚至是更加暴力的。"


translate chinese day3_stage_dv_d7b3342b:


    "不过接下来的只是阿丽夏失望的表情。"


translate chinese day3_stage_dv_02a64e1d:


    me "我想听啊，我告诉你了。"


translate chinese day3_stage_dv_57a1e5c0:


    dv "那晚上来这里找我，我给你表演。"


translate chinese day3_stage_dv_421da051:


    th "“给我表演”？{w}好可爱呀。"


translate chinese day3_stage_dv_964231c7:


    me "今晚是舞会啊。"


translate chinese day3_stage_dv_c9fb6705:


    dv "你说了你不会去的！"


translate chinese day3_stage_dv_42ff6b7c:


    th "我好像没说那个啊..."


translate chinese day3_stage_dv_2c3eb69e:


    "说起来我也不怎么想去那个舞会，不过和阿丽夏呆着也没什么意思。"


translate chinese day3_stage_dv_117c8fe1:


    me "奥尔加·德米特里耶夫娜不会喜欢的..."


translate chinese day3_stage_dv_73cb60c5:


    dv "见她的鬼去吧！"


translate chinese day3_stage_dv_c8090db3:


    "阿丽夏愤怒地说。"


translate chinese day3_stage_dv_7f329f3b:


    "要让我从舞会上出丑和观看二酱的疯狂吉他演奏中选择的话，还是后者好一些吧。"


translate chinese day3_stage_dv_0d0c3fa8:


    dv "好孩子。"


translate chinese day3_stage_dv_1f246e2c:


    "她笑得好奇怪。"


translate chinese day3_stage_dv_19538e61:


    "这和我计划的完全不一样。"


translate chinese day3_stage_dv_2567c047:


    dv "算了，反正也没人在乎。"


translate chinese day3_stage_dv_412caa87:


    "她叹着气离开了。"


translate chinese day3_stage_dv_a97da226:


    "我正在琢磨自己的决定对不对，这时午饭的铃声响起了。"


translate chinese day3_stage_dv_9256eb27:


    "我往食堂那边看过去。"


translate chinese day3_stage_dv_20b3b612:


    th "该走了，食物在召唤我。"


translate chinese day3_stage_dv_402e53da:


    "我转过身想要叫上阿丽夏一起。"


translate chinese day3_stage_dv_c0edcd63:


    "不过她看起来垂头丧气的。"


translate chinese day3_stage_dv_be5bcccb:


    me "音乐家要知道什么时候该离开。"


translate chinese day3_stage_dv_3e1d421a:


    "我嘀嘀咕咕的抱怨道。"


translate chinese day3_main4_a20cefa7:


    "..."


translate chinese day3_main4_e3a9b50b:


    "“是谁在列队前进？是我们的少先队！”"


translate chinese day3_main4_9086ffe8:


    "少先队员们在这个夏令营里经常列队前进。"


translate chinese day3_main4_ffd49d36:


    "我想要找一个像之前一样没有人打扰我的地方。"


translate chinese day3_main4_45805593:


    th "我才可以安静的吃饭！"


translate chinese day3_main4_5b7d20b3:


    "所以我至少不能是最后一个到达的！"


translate chinese day3_main4_3d31ce74:


    th "不过我现在很确定:这里的事情可不是我能作得了主的。"


translate chinese day3_main4_4cb70312:


    "食堂像鸡蛋一样被填的满满的。"


translate chinese day3_main4_7168ef23:


    "奥尔加·德米特里耶夫娜像一只老鹰一样站在门口看守着。"


translate chinese day3_main4_764d9df7:


    mt "嘿，谢苗，你今天还努力吗？"


translate chinese day3_main4_aac8720f:


    me "够努力的了。"


translate chinese day3_main4_4e3031c8:


    mt "不错不错！努力会有收获的！"


translate chinese day3_main4_f1d5cd91:


    th "那是..."


translate chinese day3_main4_63b5e296:


    mt "好了，在女生旁边找个座位吧。"


translate chinese day3_main4_4cd2fcde:


    "她指向柱子旁边的一张桌子。{w}斯拉维娅，乌里扬娜，还有列娜已经坐在那里了。"


translate chinese day3_main4_65e51c2d:


    th "还不错嘛。{w}至少不是最糟糕的..."


translate chinese day3_main4_74860e53:


    "我取来自己的饭往她们那边走去。"


translate chinese day3_main4_4bf10d5d:


    me "我坐在这里可以吗？"


translate chinese day3_main4_a4520622:


    "我的话听起来好像没什么道理，反正现在没有别的地方了。"


translate chinese day3_main4_ae35202b:


    sl "当然，没问题！"


translate chinese day3_main4_ae6727cd:


    us "请坐。"


translate chinese day3_main4_2622f621:


    "列娜一言不发。"


translate chinese day3_main4_fe3ea323:


    "今天有一盘红菜汤(我怀疑里面隐藏着一些肉，但是我没有证据), 还有某种家禽的肉，还有炸土豆和经典的水果汤。"


translate chinese day3_main4_6e7c603f:


    "我发现自己越来越喜欢这里的饭了。"


translate chinese day3_main4_b6d69ff6:


    "我没有选择，所以抱怨是没有用的。"


translate chinese day3_main4_123f8387:


    th "幸好还有点吃的。"


translate chinese day3_main4_b15bd6e7:


    sl "今晚来参加舞会吗？"


translate chinese day3_main4_595d7b8c:


    me "我不知道。"


translate chinese day3_main4_6e25c9b3:


    th "虽然我已经有了和阿丽夏的安排了..."


translate chinese day3_main4_4c1600f4:


    us "他会来的！反正也没有什么地方可以去！"


translate chinese day3_main4_99ce3e5f:


    "乌里扬娜高兴的说道。"


translate chinese day3_main4_56e1d2cc:


    me "我看你是肯定会来的..."


translate chinese day3_main4_aa1aa469:


    us "当然！我不能错过观摩你出丑的机会！"


translate chinese day3_main4_15dfe923:


    "她好像说的很有道理，所以我没有回答。"


translate chinese day3_main4_b78c6e26:


    me "你呢？"


translate chinese day3_main4_3329e7d9:


    "我问列娜。"


translate chinese day3_main4_31b01f67:


    un "是的..."


translate chinese day3_main4_521b6831:


    "她回答的很简短。"


translate chinese day3_main4_56d548d4:


    sl "看吧？你不应该错过的。"


translate chinese day3_main4_f6a30a37:


    "斯拉维娅说的我好像没有任何选择。"


translate chinese day3_main4_e75e0db2:


    us "别忘了穿燕尾服。"


translate chinese day3_main4_0033c883:


    "乌里扬娜对于自己的笑话很满意，于是便笑了起来。"


translate chinese day3_main4_bb5ceb0f:


    "不过我真的没有什么可以穿的。"


translate chinese day3_main4_006252b0:


    "我的衣柜里只有少先队员服装以及我的冬装。"


translate chinese day3_main4_99eeab2f:


    me "那你要穿什么？你个活宝？"


translate chinese day3_main4_0063fafa:


    us "秘~密~"


translate chinese day3_main4_85dd239b:


    me "会不会是幼儿园的演出用小裙子呀？"


translate chinese day3_main4_8d42266c:


    "乌里扬娜脸红了，看来我成功了。"


translate chinese day3_main4_90fa481a:


    us "不！我要穿一件生化服，防止你的传染！"


translate chinese day3_main4_405ea5ee:


    me "我很奇怪你怕我传给你什么？"


translate chinese day3_main4_a840c516:


    sl "好了，大家，不要吵架！"


translate chinese day3_main4_1c4b960b:


    us "当然是你的笨蛋病菌啊！"


translate chinese day3_main4_59986520:


    "看起来乌里扬卡又对自己天才的回答十分满意。"


translate chinese day3_main4_7312ace7:


    me "你懂不懂自己得了流感以后就不能再感冒了啊？"


translate chinese day3_main4_dbe6fc9f:


    "好啊，你要玩我就陪你玩。"


translate chinese day3_main4_4d08faa9:


    us "你这是什么意思？"


translate chinese day3_main4_8d6a94af:


    me "啊，没有，没什么意思..."


translate chinese day3_main4_2ed6c63f:


    "我若无其事的看向一边。"


translate chinese day3_main4_b155806b:


    us "你是不是...？"


translate chinese day3_main4_a852076f:


    "她又变红了。"


translate chinese day3_main4_36af1581:


    me "我什么都没有说啊。"


translate chinese day3_main4_e7059468:


    un "你们..."


translate chinese day3_main4_94c9b804:


    "既然列娜都开口了，说明的确应该打住了。"


translate chinese day3_main4_e4c12ca4:


    us "这是你自找的！！"


translate chinese day3_main4_b9d24980:


    me "什么？你终于开窍了？"


translate chinese day3_main4_5fea1e70:


    "乌里扬卡没有说话，而是端起一盘红菜汤拍在了我头上。"


translate chinese day3_main4_9f238d85:


    "真是出人意料的结局..."


translate chinese day3_main4_670b85c3:


    me "你个小...！"


translate chinese day3_main4_874c2400:


    "她跳起来打算逃走。"


translate chinese day3_main4_3d220a8d:


    "不过这次她跑不掉了，我抓住了她的手。"


translate chinese day3_main4_e4c890fc:


    th "呃，接下来该做什么？总不能拿她的脑袋砸桌子吧。"


translate chinese day3_main4_8f1d5041:


    "这个可笑的场景持续了几秒钟。"


translate chinese day3_main4_8811fef6:


    "突然间，乌里扬娜抓起了一杯水果饮料甩在我的脸上。"


translate chinese day3_main4_3958434a:


    "然后挣脱了我的束缚。"


translate chinese day3_main4_7e7c5066:


    "她往反方向逃跑，我紧追不舍。"


translate chinese day3_main4_7321633c:


    "这导致了多个饭桌倾覆，多张桌布破损，多名少先队员负伤以及对战双方精疲力竭。"


translate chinese day3_main4_b1e8f257:


    th "算是平手吧。"


translate chinese day3_main4_17b22f81:


    th "在穷兵黩武之后的两败俱伤。"


translate chinese day3_main4_e6d45acc:


    "我们面对面的喘着粗气。"


translate chinese day3_main4_0224f2b4:


    me "你说，你不会再搞这种事了！"


translate chinese day3_main4_54810cc5:


    us "你呢？"


translate chinese day3_main4_23a87547:


    "奥尔加·德米特里耶夫娜从我们背后突然使出致命一击。"


translate chinese day3_main4_2948bc41:


    "如果你想想的话，这么大的动作不可能注意不到。"


translate chinese day3_main4_501e435e:


    mt "好了，你们现在...满意了？"


translate chinese day3_main4_a3227502:


    "她的声音听起来很和蔼可亲，不过我确定她就要爆炸了。"


translate chinese day3_main4_9799cd00:


    mt "谁要负责清理这一片狼藉？？！！"


translate chinese day3_main4_643530f6:


    "不出所料。"


translate chinese day3_main4_a71a3e52:


    mt "谁啊？！我问你们呢？！谁？？！！"


translate chinese day3_main4_cc05f4a5:


    us "他！"


translate chinese day3_main4_6e0e9ab6:


    "乌里扬卡充满自信的回答道。"


translate chinese day3_main4_8cc8ea72:


    me "她！"


translate chinese day3_main4_078c536a:


    "我充不满自信的回答道。"


translate chinese day3_main4_221ac838:


    mt "你们两个人！！"


translate chinese day3_main4_cd7733bf:


    "辅导员一锤定音。"


translate chinese day3_main4_abca6dac:


    "总的来说，我也说不清我们俩谁应该负主要责任。"


translate chinese day3_main4_d4b025ce:


    "虽然乌里扬卡脸上一点愧疚的表情也没有。"


translate chinese day3_main4_3297e1dc:


    us "又是这种事！我不干了！！"


translate chinese day3_main4_79a78d45:


    us "这是他的问题！他先引起的！！"


translate chinese day3_main4_fa057752:


    me "不是！"


translate chinese day3_main4_0f7ff14d:


    us "是！！"


translate chinese day3_main4_179e42ee:


    mt "我才不管你们俩的事！"


translate chinese day3_main4_97932981:


    mt "谢苗，你去拿水桶和墩布（拖把），还有抹布，就是那个...你懂的，还有你！！"


translate chinese day3_main4_ccad0a84:


    "她燃烧的目光愤怒的盯着乌里扬娜，让我都有点替这个小女孩儿难过了。"


translate chinese day3_main4_f438658b:


    mt "你！！马上开始捡碎掉的桌布！！！"


translate chinese day3_main4_097f4a0a:


    "奥尔加·德米特里耶夫娜深呼吸一口气然后说到："


translate chinese day3_main4_12c1e19a:


    mt "你整天就知道添乱！我和你说过多少次了！"


translate chinese day3_main4_051185fa:


    "我决定翘掉这次演说，直接去找抹布。"


translate chinese day3_main4_2ffc3634:


    "突然我灵机一动，我为什么不直接溜了呀。"


translate chinese day3_main4_27e1f3fe:


    "虽然奥尔加·德米特里耶夫娜已经抓到我们了，可是反正错误也不在我。"


translate chinese day3_main4_9c178a5f:


    "虽然我不是那么忠诚的少先队员，不过乌里扬卡..."


translate chinese day3_main4_4ec22d66:


    "总之，她是主谋。"


translate chinese day3_main4_87ca6467:


    th "我不是只能呆在这里清理这一片垃圾，我可以去追求真理！"


translate chinese day3_main4_bfb5e7fb:


    "真理就在外边，在大自然之中！"


translate chinese day3_library_sl_ccebe3fc:


    "一缕清风给我送来了答案。"


translate chinese day3_library_sl_9cad1570:


    "奥尔加·德米特里耶夫娜还在忙着训斥乌里扬娜的时候我开始悄悄地潜行，幸好我离门口不远。"


translate chinese day3_library_sl_2a352a63:


    "溜出来之后，我犹豫了一会儿，然后撞见了电子小子。"


translate chinese day3_library_sl_f9674fb7:


    "他好像在社团里活动到很晚。"


translate chinese day3_library_sl_023f2a34:


    el "你这么着急干什么？"


translate chinese day3_library_sl_6e69e20d:


    me "着急？我的飞船十分钟之内就要起飞了。"


translate chinese day3_library_sl_e64f933d:


    el "什么？等一下？！"


translate chinese day3_library_sl_d7d54b7f:


    "电子小子的话停留在了我身后的空气中。"


translate chinese day3_library_sl_1afa7cdc:


    "我疯狂的跑向广场。"


translate chinese day3_library_sl_f44b49d9:


    me "这里可不是适合躲藏的地方。"


translate chinese day3_library_sl_863b1e12:


    "的确，在紧张和疲劳的情况下我总是喜欢自言自语。"


translate chinese day3_library_sl_f5dc1ea7:


    "呃，不是真正的说出来，是随着我的气息嘀嘀咕咕。"


translate chinese day3_library_sl_6c76fea4:


    "这可以让我快速的寻找到事情的解决方案。"


translate chinese day3_library_sl_2e3c65cd:


    me "好了，接下来该去哪里了？"


translate chinese day3_library_sl_ea2b71fd:


    "人越少的地方越好啊。"


translate chinese day3_library_sl_bfa9bfb0:


    th "结果我前往了树林。"


translate chinese day3_library_sl_a088dacb:


    "过了几分钟我已经坐在森林中一个树桩上了。"


translate chinese day3_library_sl_caca443f:


    th "啊...刚才可真是落花流水啊。"


translate chinese day3_library_sl_15e7cffc:


    th "我希望乌里扬卡可以明白一些了，她可不再是小孩子了！"


translate chinese day3_library_sl_cbda97c3:


    th "是啊，没错啊，不再是小孩子了..."


translate chinese day3_library_sl_be69861b:


    "现在我开始回想刚才的事件有一部分还是因为我的。"


translate chinese day3_library_sl_fb96d515:


    th "有一部分桌布其实是我弄破的。"


translate chinese day3_library_sl_e732f41b:


    th "而且说起来我开始本可以避免这次擦枪走火的。"


translate chinese day3_library_sl_89141735:


    "我陷入了沉思。"


translate chinese day3_library_sl_72cbabba:


    sl "天气怎么样？"


translate chinese day3_library_sl_5d2bdfb2:


    "斯拉维娅站在我的面前。"


translate chinese day3_library_sl_a99618e8:


    th "她到底是怎么摸过来的啊？感谢上帝不是奥尔加·德米特里耶夫娜。"


translate chinese day3_library_sl_84662d7a:


    me "还好..."


translate chinese day3_library_sl_78884236:


    "我只挤出了这几个字。"


translate chinese day3_library_sl_c9b49156:


    sl "我觉得你就在这里。"


translate chinese day3_library_sl_5ac38cf1:


    me "为什么？"


translate chinese day3_library_sl_f1262d21:


    sl "我不确定，只是..."


translate chinese day3_library_sl_72426581:


    "她抬头看向天空。"


translate chinese day3_library_sl_42471a85:


    sl "这里人更少。"


translate chinese day3_library_sl_becb13d6:


    th "斯拉维娅真的觉得我有社交恐惧？{w}不过反过来说，她这么想是很有道理的。"


translate chinese day3_library_sl_bf693f95:


    me "那你是来这里...干什么呀？"


translate chinese day3_library_sl_94fc3843:


    sl "找你。"


translate chinese day3_library_sl_9fa90903:


    "她又笑了，不过有点不一样，好像多了一点友善。"


translate chinese day3_library_sl_8052945a:


    me "我？为什么？"


translate chinese day3_library_sl_b57bc8ed:


    sl "你猜不到吗？"


translate chinese day3_library_sl_a5d87f4f:


    "我准备迎接一场严厉的演讲。"


translate chinese day3_library_sl_e2e2018f:


    sl "我知道责任主要在乌里扬娜。"


translate chinese day3_library_sl_66b23847:


    th "现在辅导员的助理也在替我说话了！{w}我感觉自己好悲剧。"


translate chinese day3_library_sl_4fa4d55e:


    th "不对，要谨慎，他们可能是钓鱼..."


translate chinese day3_library_sl_50c80811:


    sl "不过我可以允许你不帮她，但是你一定要帮我啊。"


translate chinese day3_library_sl_d322f1a0:


    "说着这些话，斯拉维娅又看向了天空。"


translate chinese day3_library_sl_7b2c24d7:


    th "我很奇怪她在抬头找什么？"


translate chinese day3_library_sl_a9c6d0dd:


    me "我需要做什么？"


translate chinese day3_library_sl_b90384cf:


    sl "我们得整理图书馆的图书。"


translate chinese day3_library_sl_28c44327:


    "这比和乌里扬卡一起做苦力要好多了。{w}总之，这是我无法拒绝的请求。"


translate chinese day3_library_sl_c7aaed10:


    me "听从您的吩咐，女士。或者是小姐？"


translate chinese day3_library_sl_ec2c2a7c:


    "她笑了起来。"


translate chinese day3_library_sl_e6b3e7b4:


    sl "那么请跟着我，先生！"


translate chinese day3_library_sl_f4346447:


    "过了几分钟，我们来到了图书馆。"


translate chinese day3_library_sl_4faed1c0:


    "一路上我都在警惕的观察四周以防奥尔加·德米特里耶夫娜突袭。"


translate chinese day3_library_sl_e82faf41:


    "斯拉维娅看上去没有注意到我的慌张。"


translate chinese day3_library_sl_9a6e5488:


    th "而且她没有责备我，这好像很奇怪。"


translate chinese day3_library_sl_1c2eed77:


    "如果继续分析的话......一定是斯拉维娅唱红脸，引诱我上钩。"


translate chinese day3_library_sl_f58d39b1:


    "当我们走进门的时候，柔和的阳光流淌到图书馆中。"


translate chinese day3_library_sl_35c68aa3:


    "图书馆里有一种特殊的气味，让你能够一下就识别出来。"


translate chinese day3_library_sl_1d93103b:


    "破破烂烂的马克思列宁全集。"


translate chinese day3_library_sl_325f1bcd:


    th "有意思，小说类书附近空气的味道和物理化学的会是一样的吗？"


translate chinese day3_library_sl_826e40a7:


    sl "你从那一边开始。"


translate chinese day3_library_sl_17c95df4:


    "斯拉维娅指向列宁头像。"


translate chinese day3_library_sl_46f0983a:


    me "好的，不过我具体是应该做什么呢？"


translate chinese day3_library_sl_968cfe00:


    sl "把书从书架上拿下来，{w}我们一会儿把它们打扫干净然后再放回去。"


translate chinese day3_library_sl_6febb73f:


    "看看这些书上的灰尘就知道这些少先队员们不怎么爱看书。"


translate chinese day3_library_sl_3a98efa2:


    me "你知道热尼娅去哪里了吗？"


translate chinese day3_library_sl_a1e6dd7a:


    sl "不知道，她应该出去了吧。"


translate chinese day3_library_sl_ea719463:


    th "奇怪。"


translate chinese day3_library_sl_a2356fab:


    "好像只能在这两个地方遇到热尼娅——图书馆和食堂。"


translate chinese day3_library_sl_ac2cba61:


    "我进入了工作状态。"


translate chinese day3_library_sl_0eb6b971:


    "我猜我一辈子也学不会这么多的书。"


translate chinese day3_library_sl_ce83d765:


    "总之我还是更擅长阅读电脑屏幕或是平板什么的。"


translate chinese day3_library_sl_de4998dd:


    "电子书不需要付费，而且有些东西是纸质书展示不出来的。"


translate chinese day3_library_sl_77254c81:


    sl "进展如何？"


translate chinese day3_library_sl_515b792e:


    "斯拉维娅隔着书架问我。"


translate chinese day3_library_sl_31dbcb74:


    me "还好啦，一点一点的。"


translate chinese day3_library_sl_3799372a:


    me "你读过这种书吗？"


translate chinese day3_library_sl_8f3fa4b1:


    sl "什么书？"


translate chinese day3_library_sl_5625a91d:


    me "喏，就是关于共产主义的..."


translate chinese day3_library_sl_d51d54a0:


    sl "没有...我更喜欢历史类的，还有冒险的。"


translate chinese day3_library_sl_7f0819cb:


    me "我也是..."


translate chinese day3_library_sl_a3f8af02:


    "历史文学我也不是那么喜欢，可比起无产阶级的..."


translate chinese day3_library_sl_eae58056:


    sl "你别记乌里扬娜的仇啊..."


translate chinese day3_library_sl_ec1593ec:


    me "我没有..."


translate chinese day3_library_sl_e1c77be8:


    "的确，我中午在食堂找到了自我，{w}而且我还可以在森林里消消食，还遇到了斯拉维娅。"


translate chinese day3_library_sl_ca83718e:


    sl "她不是出于怨恨才做那些事的。"


translate chinese day3_library_sl_04a51e77:


    me "不管那个，一个人总要懂得控制自己的言行。{w}啊，算了..."


translate chinese day3_library_sl_0f8bf2e5:


    "我继续整理着不知道是谁写的书。"


translate chinese day3_library_sl_92df4c4a:


    th "我过不了一分钟就会忘了他们的名字，而且我一辈子可能也不会有机会阅读他们的作品。"


translate chinese day3_library_sl_4012f849:


    th "这真是悲剧——成为一个现实中不存在的图书馆里一本被遗弃的大部头。"


translate chinese day3_library_sl_73956111:


    sl "你完成了吗？"


translate chinese day3_library_sl_f8226aa9:


    me "是的，应该是这样。"


translate chinese day3_library_sl_7bfe1a7a:


    sl "我也是，好吧我们开始吧，我把它们擦干净，你负责放回去。"


translate chinese day3_library_sl_f38a2469:


    me "可是我不记得那一本该放到哪里了。"


translate chinese day3_library_sl_57b94aaa:


    sl "没关系，反正也没有人会去读。"


translate chinese day3_library_sl_68a9ffa6:


    th "看起来她好像会在任何场合露出微笑。"


translate chinese day3_library_sl_19451d64:


    "我还观察到过几次细微的变化，笑容中还蕴含着其他的情感，快乐、悲伤、幸福..."


translate chinese day3_library_sl_853f82b2:


    "可是这些细小的表情仅仅是昙花一现，我不能确定那是不是想象出来的。"


translate chinese day3_library_sl_5ff6b0ce:


    "感觉这次她笑得有些挑逗的感觉。"


translate chinese day3_library_sl_1e475060:


    me "嗯，大概是。{w}至少我就不会去读。"


translate chinese day3_library_sl_08cfb744:


    "斯拉维娅用一块布擦拭着，我则把书一本本放回去。"


translate chinese day3_library_sl_3a2c6311:


    "我们这个组合还不错，我们很快就接近完成了。"


translate chinese day3_library_sl_9fdf4842:


    sl "为什么上层书架你完全没有动啊？"


translate chinese day3_library_sl_4c1d6d10:


    me "上层书架？"


translate chinese day3_library_sl_f8c41e21:


    "我往上看去。"


translate chinese day3_library_sl_cbcf5cb7:


    me "抱歉，我没有注意到。"


translate chinese day3_library_sl_705bea28:


    sl "没关系。"


translate chinese day3_library_sl_f9e5b237:


    me "等一下！我把椅子搬来！"


translate chinese day3_library_sl_164801d0:


    "以我还算可以的身高仍然够不到。"


translate chinese day3_library_sl_5c3aa9a0:


    "我站在椅子上继续帮斯拉维娅往下拿书。"


translate chinese day3_library_sl_6f01b19b:


    "过了一会儿，只剩下远处有几本书了。"


translate chinese day3_library_sl_a29261f6:


    "看起来好像没那么远。"


translate chinese day3_library_sl_45a87cde:


    "我是那种宁可一下吃撑的人。{w}然后我没有挪动椅子就伸手去够那边的书..."


translate chinese day3_library_sl_57e655b2:


    "结果我一失足成千古恨..."


translate chinese day3_library_sl_3b6da452:


    "睁开眼后，我发现我在斯拉维娅的上面。"


translate chinese day3_library_sl_64eccf44:


    me "你是...我是不是......我还活着？！"


translate chinese day3_library_sl_df45d6bc:


    "吓得我都看到马克思的背影了。"


translate chinese day3_library_sl_9f4025f7:


    th "虽然我现在只有17岁，可是掉在一个女孩儿身上还是......"


translate chinese day3_library_sl_0451d1e4:


    sl "我还好。"


translate chinese day3_library_sl_55c4c14d:


    "她的脸离我只有几厘米。"


translate chinese day3_library_sl_f3139a0d:


    me "有没有什么东西摔坏了？"


translate chinese day3_library_sl_3f137cac:


    sl "没有吧。"


translate chinese day3_library_sl_673a4231:


    sl "作为特技演员你可是糟透了。"


translate chinese day3_library_sl_ce3666e5:


    "她笑了起来。"


translate chinese day3_library_sl_e2d2475f:


    me "是啊。"


translate chinese day3_library_sl_e10fbc50:


    "我看着她的眼睛。只是看着。"


translate chinese day3_library_sl_382be24f:


    "我不知道该说什么...{w}斯拉维娅在犹豫，或者是等着我。"


translate chinese day3_library_sl_f0551a4a:


    th "在这种情况下可能会有什么事情发生..."


translate chinese day3_library_sl_5ff7b840:


    th "她的嘴唇离我好近！"


translate chinese day3_library_sl_44711d46:


    "我身体的本能好像在觉醒，可是这{i}对吗{/i}？"


translate chinese day3_library_sl_f481ef93:


    "我想赶紧站起来，可是却做不到。"


translate chinese day3_library_sl_ef3ea7a1:


    "斯拉维娅只是静静的躺着，直直的盯着我的眼睛。"


translate chinese day3_library_sl_5ab0d3a1:


    "突然间，铃声响了起来。"


translate chinese day3_library_sl_8e8e0763:


    sl "到时间了。"


translate chinese day3_library_sl_751ac511:


    me "嗯..."


translate chinese day3_library_sl_d8eca9e9:


    sl "我们应该走吗？{w}还是应该再多躺一会儿？"


translate chinese day3_library_sl_e4a6b1bd:


    "她又笑了起来，这次看上去很迷人，好像在暗示着什么。"


translate chinese day3_library_sl_050169d2:


    me "咱们走吧..."


translate chinese day3_library_sl_19989263:


    "我一动不动。"


translate chinese day3_library_sl_0f8c2844:


    sl "嗯...如果想要走的话，你得先站起来。"


translate chinese day3_library_sl_85a7ebd8:


    me "当然..."


translate chinese day3_library_sl_e6991c34:


    "我好像被什么看不见的力量按在了这里。"


translate chinese day3_library_sl_8f6ef867:


    "斯拉维娅好像发现了这一点，小心的从下面挪了出去。"


translate chinese day3_library_sl_c0e00208:


    sl "你想呆在这里吗？"


translate chinese day3_library_sl_ec2c2a7c_1:


    "她笑了。"


translate chinese day3_library_sl_491711dd:


    "到了这个时候我终于回过神来，站起身来。"


translate chinese day3_library_sl_8a8c94c1:


    me "抱歉..."


translate chinese day3_library_sl_a707867b:


    sl "没关系。"


translate chinese day3_library_sl_cd4fe72c:


    sl "不过我们还没有完成呦，{w}一会儿还要把剩下的清理掉。"


translate chinese day3_library_sl_85279b68:


    me "是啊，当然。"


translate chinese day3_library_sl_92690f30:


    "去食堂的路上我一言不发。"


translate chinese day3_library_sl_f1edeb24:


    "斯拉维娅随便聊着她在夏令营的生活，但因为是她独自在说，所以我也没怎么听进去。"


translate chinese day3_library_sl_d67f2f47:


    "来到食堂后，我看见奥尔加·德米特里耶夫娜站在门口。"


translate chinese day3_library_sl_2c96d9df:


    mt "你不想解释解释吗？小伙子？"


translate chinese day3_library_sl_c3fe9d67:


    "现在我终于想起来自己把什么给忘了..."


translate chinese day3_library_sl_5cb822ee:


    me "Nope..."


translate chinese day3_library_sl_18279173:


    mt "你的打扫工作做得怎么样？"


translate chinese day3_library_sl_65bf82fc:


    me "啊，不错..."


translate chinese day3_library_sl_09a04ec4:


    mt "乌里扬娜不是那样和我说的。"


translate chinese day3_library_sl_3074c0ad:


    me "那完全是她的问题嘛！"


translate chinese day3_library_sl_6fc0750c:


    mt "我怎么和你说的？"


translate chinese day3_library_sl_6ced5f4c:


    mt "我命令你们一起收拾！{w}你是怎么做的？！"


translate chinese day3_library_sl_d2aa1636:


    "我不知道该说什么，因为内心里觉得她说的有道理。"


translate chinese day3_library_sl_418c13dd:


    sl "别怪他！{w}谢苗在图书馆给我帮忙！"


translate chinese day3_library_sl_38689622:


    mt "噢，真的？"


translate chinese day3_library_sl_7314bee2:


    sl "是的！"


translate chinese day3_library_sl_bc20eb0b:


    mt "唔，那还行...{w}不过废话少说，今晚没你的饭了！"


translate chinese day3_library_sl_a2911372:


    me "怎么？！为什么？"


translate chinese day3_library_sl_902a9a2f:


    "我抗议道。"


translate chinese day3_library_sl_5ec7a533:


    "可是我的抗议就像是在监狱的食堂里要求再来一碗一样——有胆量，可是愚蠢而没有建设性。"


translate chinese day3_library_sl_b77f1870:


    mt "希望你能长个教训！"


translate chinese day3_library_sl_fcc1c309:


    "这个时候的奥尔加·德米特里耶夫娜看起来更像是一个古罗马的军团指挥官。"


translate chinese day3_library_sl_5bdcf334:


    "我不知道该怎么反驳。"


translate chinese day3_library_sl_ae8c4442:


    sl "请原谅他！{w}看在我的份上！"


translate chinese day3_library_sl_d4764288:


    "辅导员思考了一段时间。"


translate chinese day3_library_sl_777093a7:


    mt "好吧，如果你这样坚持的话！"


translate chinese day3_library_sl_309686e8:


    sl "谢谢！"


translate chinese day3_library_sl_73aab3cd:


    me "谢谢..."


translate chinese day3_library_sl_34c73a83:


    "我们匆忙进去。"


translate chinese day3_library_sl_4df43400:


    sl "我去找女生们了，我们有约好，保重！"


translate chinese day3_library_sl_3ec70e0f:


    "她笑着和我挥手道别。"


translate chinese day3_library_sl_99d3acba:


    "没有她可怎么办？"


translate chinese day3_library_sl_0eba76f0:


    "斯拉维娅附近没有座位了，而且我也不认识她的朋友们。"


translate chinese day3_cleaning_us_99ef151c:


    "逃跑可不是个好主意。"


translate chinese day3_cleaning_us_93c60cc4:


    "奥尔加·德米特里耶夫娜已经抓到我一次了，再逃跑只会雪上加霜。"


translate chinese day3_cleaning_us_394c9d9e:


    "而且，我也有一定的责任..."


translate chinese day3_cleaning_us_2f94b8f1:


    th "这肯定都是她的错！"


translate chinese day3_cleaning_us_25f82315:


    "可是如果我没有那样做的话，可能也就不会有这一番争斗了。"


translate chinese day3_cleaning_us_996e7fec:


    th "可能..."


translate chinese day3_cleaning_us_3e5b8732:


    "我打开橱柜，取出扫把、拖把和簸箕。"


translate chinese day3_cleaning_us_f469532f:


    "我回来的时候奥尔加·德米特里耶夫娜已经不见了。"


translate chinese day3_cleaning_us_24ab10f2:


    me "她走了？"


translate chinese day3_cleaning_us_7e694dd0:


    us "你自己不会看吗？！"


translate chinese day3_cleaning_us_afe5cb8e:


    "乌里扬卡看起来很失落，她脸上的青春活力消失的无影无踪。"


translate chinese day3_cleaning_us_3b4a6242:


    me "好了，等一下，我去洗洗。"


translate chinese day3_cleaning_us_a464805b:


    "我愤怒的看了她一眼，走向出口。"


translate chinese day3_cleaning_us_80aa159b:


    "我把身上的食物洗干净，返回食堂。"


translate chinese day3_cleaning_us_8bf25c46:


    me "好了，不能逃跑了，我们得把它清理干净。"


translate chinese day3_cleaning_us_0713a7b2:


    us "都是因为你！"


translate chinese day3_cleaning_us_97c9d07e:


    "我只是轻轻瞟了她一眼就招来了万箭齐发。"


translate chinese day3_cleaning_us_771aa502:


    me "当然！"


translate chinese day3_cleaning_us_12685598:


    me "什么事都是我的错！我好像就是一个安全隐患！"


translate chinese day3_cleaning_us_005b5a5d:


    us "行了，闭嘴..."


translate chinese day3_cleaning_us_abd7f53b:


    "可是她没有试着逃跑还是很奇怪。"


translate chinese day3_cleaning_us_87e97c80:


    "乌里扬卡有很好的机会可以逃走，但是却老老实实的打扫了垃圾和桌椅。"


translate chinese day3_cleaning_us_ad056558:


    "事实上，她的动作很快我都跟不上她。"


translate chinese day3_cleaning_us_a3e4be71:


    me "好吧，你现在是不是想要装好人啊？"


translate chinese day3_cleaning_us_a9babbd5:


    us "我又不想在这里待上一整天，你个呆子！"


translate chinese day3_cleaning_us_9fc103c6:


    "她听起来还是有些愤懑。"


translate chinese day3_cleaning_us_19fa1fbf:


    me "听着，你得知道你不能那样做的，至少不能那么夸张..."


translate chinese day3_cleaning_us_fbfed032:


    "出于某种原因我开始给她上思想品德课程。"


translate chinese day3_cleaning_us_e142a9de:


    us "我什么都没干！是你一直在喊我的名字！"


translate chinese day3_cleaning_us_4f354c21:


    "乌里扬娜抓起了水桶和拖把往食堂那边走去。"


translate chinese day3_cleaning_us_d71063b0:


    th "她看起来还是很生气。"


translate chinese day3_cleaning_us_b32f5157:


    "看着堆积起来的坏桌布，我终于理解了什么是灾难。"


translate chinese day3_cleaning_us_7300ad57:


    th "我们得庆幸刀子叉子都是金属的，至少还有可以用的。{w}可是没剩下多少个盘子..."


translate chinese day3_cleaning_us_e66e1571:


    us "嘿..."


translate chinese day3_cleaning_us_81282f69:


    "乌里扬娜叫着我。"


translate chinese day3_cleaning_us_cbdad383:


    "我走过去。"


translate chinese day3_cleaning_us_557a9104:


    us "我不明白，你为什么这么讨厌我？"


translate chinese day3_cleaning_us_8e1a6421:


    "她的表情很严肃，以至于我开始考虑这可能不是她的另一个恶作剧。"


translate chinese day3_cleaning_us_dffd29c3:


    me "你为什么这么想？"


translate chinese day3_cleaning_us_f0847682:


    us "我不知道，所以才问你的。"


translate chinese day3_cleaning_us_e5f99c8b:


    me "我不讨厌你，只是有时候你比较...{w}呃，你懂的。"


translate chinese day3_cleaning_us_8c306ba5:


    "就是那么简单，但是确实真的。"


translate chinese day3_cleaning_us_a00845ac:


    us "怎样？我不明白啊。"


translate chinese day3_cleaning_us_cc439301:


    "她认真的盯着我。"


translate chinese day3_cleaning_us_4470295b:


    me "最开始，你为什么要往我身上撒水果汤啊？"


translate chinese day3_cleaning_us_c656e178:


    us "这是你自找的。"


translate chinese day3_cleaning_us_65b7d3f3:


    "这是她出了这件事以后第一次笑。"


translate chinese day3_cleaning_us_2bb47ab4:


    me "是，是啊..."


translate chinese day3_cleaning_us_47d09bdb:


    "我重重的叹了口气。"


translate chinese day3_cleaning_us_0621ba40:


    me "那么你到底希望怎样？"


translate chinese day3_cleaning_us_ff47fe7e:


    us "没什么。"


translate chinese day3_cleaning_us_2f6a59ef:


    "她刻薄的回答道。"


translate chinese day3_cleaning_us_8e2b133b:


    "我们的讨论结束了，我又开始静静的打扫卫生。"


translate chinese day3_cleaning_us_a20cefa7:


    "..."


translate chinese day3_cleaning_us_86688e12:


    "我们用了几个小时才把食堂收拾干净。."


translate chinese day3_cleaning_us_0a8d709c:


    "至少破掉的桌布都消失了，桌椅全都复位，地面也擦干净了。"


translate chinese day3_cleaning_us_d6447ba7:


    "我们坐在一起，乌里扬卡大大咧咧的坐在食物台旁边休息着。"


translate chinese day3_cleaning_us_a4eb05ec:


    me "现在你知道因为因为你的恶作剧我们要付出多大的代价了吧！"


translate chinese day3_cleaning_us_d9f878c6:


    us "可是我一点也不累！"


translate chinese day3_cleaning_us_fc386561:


    "虽然她脸上流下的汗水表达着相反的意思。"


translate chinese day3_cleaning_us_8e0db1a9:


    me "好吧，你行..."


translate chinese day3_cleaning_us_926c6d27:


    us "那么，我们接下来该做什么？"


translate chinese day3_cleaning_us_2a831701:


    me "我不懂你，我还是走吧..."


translate chinese day3_cleaning_us_2fe78b26:


    us "不！还没完！"


translate chinese day3_cleaning_us_86e9c04d:


    us "你一定..."


translate chinese day3_cleaning_us_6e6e30b3:


    "她犹豫了一下。"


translate chinese day3_cleaning_us_cbab93c7:


    us "再帮我一个小忙！"


translate chinese day3_cleaning_us_341c6bf4:


    me "又在计划一个恶作剧？"


translate chinese day3_cleaning_us_7862b445:


    us "嘿嘿！"


translate chinese day3_cleaning_us_6d77584d:


    "她得意的笑着。"


translate chinese day3_cleaning_us_b72aa87d:


    me "我不是你的什么助手，今天挨一次训我已经受够了！"


translate chinese day3_cleaning_us_005a932e:


    us "好吧，我们来做个交易，如果你这次帮了我，我就再也不做恶作剧了！"


translate chinese day3_cleaning_us_d5e744c0:


    "这个条件很诱人，可是我却不怎么相信乌里扬娜。"


translate chinese day3_cleaning_us_24184228:


    th "至少问问没什么影响。"


translate chinese day3_cleaning_us_72d07964:


    me "那么你的奸计是怎样的呢？"


translate chinese day3_cleaning_us_8e695095:


    us "我们要偷点儿糖！"


translate chinese day3_cleaning_us_3d342d5a:


    me "什么？！"


translate chinese day3_cleaning_us_cce1c4dc:


    "我就应该猜到。"


translate chinese day3_cleaning_us_82811001:


    th "糖果是小孩子吃的东西..."


translate chinese day3_cleaning_us_f544383f:


    us "很快头就要出来扔垃圾的，没有人会看见我们。"


translate chinese day3_cleaning_us_df96cac2:


    me "这我可不干！"


translate chinese day3_cleaning_us_44f47ba7:


    us "哼，随便你！"


translate chinese day3_cleaning_us_99aa653e:


    "她嘟囔着跑开了。"


translate chinese day3_cleaning_us_1838e844:


    us "那我自己干！"


translate chinese day3_cleaning_us_821e8719:


    me "我才不会让..."


translate chinese day3_cleaning_us_e1f72ec9:


    "我还没来得及说完话，乌里扬卡就已经翻过食物台，打开壁橱，开始翻箱倒柜。"


translate chinese day3_cleaning_us_e182607e:


    me "你快停下来吧！你还没有被奥尔加·德米特里耶夫娜训够啊！"


translate chinese day3_cleaning_us_6586cba7:


    "她没有回答。"


translate chinese day3_cleaning_us_fdb724d1:


    me "做了扫除可不是就允许你这样随便......"


translate chinese day3_cleaning_us_2992696a:


    "乌里扬卡关上了柜门，怀里抱着一大包糖果。"


translate chinese day3_cleaning_us_244f004d:


    me "啊！！你个小...赶紧放回去！！"


translate chinese day3_cleaning_us_5cc17f12:


    "她向我吐了吐舌头，从后门跑了出去。"


translate chinese day3_cleaning_us_8d50913c:


    "这我不能不管！我随后追了出去。"


translate chinese day3_cleaning_us_499e4041:


    "这孩子虽然已经跑出了老远，但是我用尽全身的力量使劲追去。"


translate chinese day3_cleaning_us_0ab77e34:


    th "我不会再跟丢了！"


translate chinese day3_cleaning_us_a0344103:


    "我们穿过了广场..."


translate chinese day3_cleaning_us_971a61ea:


    "在音乐社团那里转弯..."


translate chinese day3_cleaning_us_c8e8d113:


    "然后来到了森林的小路。"


translate chinese day3_cleaning_us_596c2686:


    "我马上就要抓到乌里扬卡了，这时她突然停了下来..."


translate chinese day3_cleaning_us_13e17fb4:


    "我来不及刹车，直接撞在了她身上。"


translate chinese day3_cleaning_us_31ba2a96:


    "我们连着在草地上滚了好几圈..."


translate chinese day3_cleaning_us_9cd22020:


    me "抓到你啦！"


translate chinese day3_cleaning_us_7aeb2243:


    "我胜利的欢呼。"


translate chinese day3_cleaning_us_c67ac3de:


    us "你才没有..."


translate chinese day3_cleaning_us_ed702531:


    "她不好意思的说道。"


translate chinese day3_cleaning_us_0ec6d3ef:


    "乌里扬娜躺在我下面。"


translate chinese day3_cleaning_us_92dbd985:


    "我们的脸靠得很近。"


translate chinese day3_cleaning_us_cb6f36c0:


    "我感觉到了她的体温还有那不规则的呼吸。"


translate chinese day3_cleaning_us_57515ee8:


    th "虽然她现在还是个小孩子，不过很快就会成为一个女人了。"


translate chinese day3_cleaning_us_b08ceff8:


    "感觉好尴尬。"


translate chinese day3_cleaning_us_c19988a4:


    us "你要强暴我吗？"


translate chinese day3_cleaning_us_436a485d:


    "她逐渐清醒过来说着。"


translate chinese day3_cleaning_us_0c857a98:


    me "你是这样希望的吗？"


translate chinese day3_cleaning_us_b4df4200:


    "她好像在玩游戏。"


translate chinese day3_cleaning_us_0fca62ac:


    us "然后呢!"


translate chinese day3_cleaning_us_0f58246c:


    "她顽皮的笑起来，伴随着轻轻的呼吸。{w}这是不是我的幻觉？"


translate chinese day3_cleaning_us_568b5895:


    me "我现在没这个心情..."


translate chinese day3_cleaning_us_44f47ba7_1:


    us "哈...随便你。"


translate chinese day3_cleaning_us_bd670e0e:


    "乌里扬卡起来咬了我的鼻子。"


translate chinese day3_cleaning_us_b47815ea:


    "这让我有点意外，甚至还往后退了一下。"


translate chinese day3_cleaning_us_4e8535ee:


    "只要犹豫一个瞬间，她就可以把我撂倒然后跑出去老远。"


translate chinese day3_cleaning_us_28d12718:


    us "看着，你可是会后悔的！"


translate chinese day3_cleaning_us_5cabe167:


    "她大声笑着，消失在了丛林中。"


translate chinese day3_cleaning_us_780778e4:


    "那一袋糖果被留在了我身边。"


translate chinese day3_cleaning_us_6e19bf3f:


    th "我很奇怪她是不是故意留在这里的？"


translate chinese day3_cleaning_us_11007c59:


    "晚饭时间快要到了，所以我得加快脚步归还糖果。"


translate chinese day3_cleaning_us_ff6853b7:


    "...而且最好是不要被发现。"


translate chinese day3_cleaning_us_f7b4ae2b:


    "到时候我也可以解释：不是我干的，是乌里扬娜..."


translate chinese day3_cleaning_us_e4f68848:


    th "可是谁会相信我啊？"


translate chinese day3_cleaning_us_492cba74:


    "奥尔加·德米特里耶夫娜已经在食堂门口等着了…"


translate chinese day3_cleaning_us_2299f5be:


    mt "谢苗干得好！"


translate chinese day3_cleaning_us_fb544be4:


    me "什么干得好？"


translate chinese day3_cleaning_us_1ed8ab85:


    "我把那袋糖果藏在了身后。"


translate chinese day3_cleaning_us_9d2438b5:


    "因为袋子是透明的，而且太大塞不进我的口袋里。"


translate chinese day3_cleaning_us_7746de39:


    mt "我是说你们做的扫除啊，已经又干净又整齐了。"


translate chinese day3_cleaning_us_59d93851:


    me "嗯，是啊..."


translate chinese day3_cleaning_us_a8ce4ea0:


    mt "而乌里扬娜呢？"


translate chinese day3_cleaning_us_f7c4eac6:


    th "我真希望自己知道！"


translate chinese day3_cleaning_us_c7337a6d:


    me "她...她很快就会来了..."


translate chinese day3_cleaning_us_881d262a:


    mt "好吧，你去吃晚饭吧。"


translate chinese day3_cleaning_us_ad1ee043:


    "我走进了食堂。"


translate chinese day3_cleaning_us_5a56cd46:


    "让我吃惊的是，食堂里人满为患。"


translate chinese day3_cleaning_us_0a6a1ce5:


    "这下可难办了。"


translate chinese day3_cleaning_us_4bcefbd3:


    th "我是可以晚上再来送，可是现在我怎么处理？"


translate chinese day3_cleaning_us_2a454c26:


    sl "谢苗！"


translate chinese day3_cleaning_us_71e7861c:


    "我转过身去，斯拉维娅就在我面前。"


translate chinese day3_cleaning_us_da29ffa7:


    sl "哇，那是什么？"


translate chinese day3_cleaning_us_6651226b:


    "她发现了我没来得及藏起来的包裹。"


translate chinese day3_cleaning_us_5a9cf61b:


    th "真相大白！东窗事发！灰头土脸！全盘皆输！"


translate chinese day3_cleaning_us_1a98bf30:


    me "这些是...{w}糖果..."


translate chinese day3_cleaning_us_81dfe51a:


    sl "从哪里来的？"


translate chinese day3_cleaning_us_d9ec0a9f:


    th "我偷的，糟糕..."


translate chinese day3_cleaning_us_934cbef8:


    me "从乌里扬娜那里搞到的。"


translate chinese day3_cleaning_us_320366d1:


    sl "喔，我明白了，还是老节目啊。"


translate chinese day3_cleaning_us_5d6eebdb:


    me "这是什么意思"


translate chinese day3_cleaning_us_63f30cbb:


    sl "这不是她第一次偷吃糖果了。"


translate chinese day3_cleaning_us_11ef631b:


    th "我为什么一点不惊讶呢？"


translate chinese day3_cleaning_us_ee1d96ff:


    sl "交给我处理吧。"


translate chinese day3_cleaning_us_73aab3cd:


    me "多谢..."


translate chinese day3_cleaning_us_03e8e491:


    "斯拉维娅又一次救了我。"


translate chinese day3_cleaning_us_1751857b:


    "她拿走了袋子走向了食物台。"


translate chinese day3_cleaning_us_574fb3bf:


    "我不太想了解她到底是怎么解释的，所以我环视四周寻找空的座位。"


translate chinese day3_main5_144331d6:


    "看来我的晚饭得和电子小子和舒里克一起吃了。"


translate chinese day3_main5_967e239c:


    "别处没有地方了。"


translate chinese day3_main5_67af4832:


    me "先生们，怎么啦？"


translate chinese day3_main5_cc50a49a:


    "每次和他们打交道，我总是不由自主的嬉皮笑脸。"


translate chinese day3_main5_887a8a04:


    "虽然这样做有一定的危险性，可是电子兄弟是我唯一的正面情绪来源。"


translate chinese day3_main5_1d386f5f:


    me "你怎么样？"


translate chinese day3_main5_489d4411:


    el "还好，你呢？"


translate chinese day3_main5_7cf58fb3:


    me "一个回合。"


translate chinese day3_main5_2e8cf482:


    sh "发生了什么事吗？"


translate chinese day3_main5_ad9962cd:


    me "很多事。"


translate chinese day3_main5_a4a08fa3:


    el "愿意讲一讲吗？"


translate chinese day3_main5_b216e161:


    me "还是下一次吧。"


translate chinese day3_main5_44ead865:


    sh "这是你的风格。"


translate chinese day3_main5_b491ceac:


    "他做了一个无助的手势。"


translate chinese day3_main5_3ca98c02:


    el "晚饭后我们要进行舞会！"


translate chinese day3_main5_b528275f:


    "电子小子笑着。"


translate chinese day3_main5_e1a72042:


    me "我知道。"


translate chinese day3_main5_d1f8823e:


    el "你想找谁呢？"


translate chinese day3_main5_7b2a4bde:


    me "我还没有考虑过呢，你呢？"


translate chinese day3_main5_7d7e9876:


    el "我...呃，我..."


translate chinese day3_main5_94a58e2f:


    "这个问题好像直击要害。"


translate chinese day3_main5_7607ce16:


    me "去问问乌里扬卡吧，她会很高兴的。"


translate chinese day3_main5_8da8904a:


    el "不用了，谢谢！"


translate chinese day3_main5_5753b37f:


    "电子小子慌张的挥着手。"


translate chinese day3_main5_5fe023fb:


    me "你呢，舒里克，你要找阿丽夏。"


translate chinese day3_main5_e778b8e0:


    sh "多谢，我还是算了吧。"


translate chinese day3_main5_1d41d511:


    "他看起来显得更从容一些。"


translate chinese day3_main5_246c6e50:


    me "别不好意思啊，伙计们，会很有意思的！"


translate chinese day3_main5_29c157fd:


    el "可是我们还有事！我们...我们还要做机器人！"


translate chinese day3_main5_41e8d12f:


    me "哇，那是个好主意！邀请你的机器人吧。他会不会跳舞啊？"


translate chinese day3_main5_9eb621e9:


    sh "它现在连走还不会呢。"


translate chinese day3_main5_067ae991:


    "舒里克真是不开窍。"


translate chinese day3_main5_04d8b901:


    el "为什么，这是一个展示我们的成果的好机会啊！"


translate chinese day3_main5_345dabaf:


    sh "我们能展示什么啊？"


translate chinese day3_main5_69ce28cd:


    el "是啊，你说得对..."


translate chinese day3_main5_b54d08ef:


    "他们一起垂头丧气的盯着盘子。"


translate chinese day3_main5_a20cefa7:


    "..."


translate chinese day3_main5_a20cefa7_1:


    "..."


translate chinese day3_main5_a20cefa7_2:


    "..."


translate chinese day3_main5_3f89d2ed:


    "晚饭结束了，少先队员们陆续离开。"


translate chinese day3_main5_ad0e1b9c:


    me "你们舞会时穿什么衣服呢？"


translate chinese day3_main5_83b23459:


    "我询问着舒里克和电子小子。"


translate chinese day3_main5_3b3ff830:


    el "我们没有特殊的衣服，我们会本色出演。"


translate chinese day3_main5_4bca7d01:


    "他是准备穿少先队员服吧。"


translate chinese day3_main5_51acbdbc:


    th "他们好像对外观一点也不在意。"


translate chinese day3_main5_46388e2b:


    th "不对，那我为什么要在意呢？！"


translate chinese day3_main5_a2a4c2bc:


    th "穿冬装不太合理，所以我决定这样。"


translate chinese day3_main5_0058648c:


    me "什么时候开始啊？"


translate chinese day3_main5_129ca123:


    el "在九点钟的时候。"


translate chinese day3_main5_855db9ee:


    me "我明白了。"


translate chinese day3_main5_a8b4968b:


    "我离开了食堂，呼吸着傍晚的清新空气。"


translate chinese day3_main5_52c76225:


    "我想起来了上学时的那些跳舞活动。"


translate chinese day3_main5_b1cb0198:


    "犹豫，不安，甚至是害怕..."


translate chinese day3_main5_68f4a1be:


    "我不会跳舞，我不知道别人邀请我时怎么办，我也不知道该如何邀请别人。"


translate chinese day3_main5_c972d742:


    "总的来说就是感觉不舒服。"


translate chinese day3_main5_742d0473:


    "看着别人玩的那么开心则更不舒服。"


translate chinese day3_main5_fa8fda07:


    "这不是嫉妒。{w}只是其他人如此享受的事情我却享受不了让我感觉很窘迫。"


translate chinese day3_main5_1ecbd57e:


    "我想离舞会还有好一段时间，所以打算先休息一下。"


translate chinese day3_main5_629eed15:


    "走进辅导员的房间，我扑到床上，马上就闭上了眼。"


translate chinese day3_main5_a20cefa7_3:


    "..."


translate chinese day3_main5_f7195c18:


    "奇怪的是，我没有用闹钟却准时的醒来了。"


translate chinese day3_main5_da045b54:


    "表针指向9点。"


translate chinese day3_main5_2da36184:


    "这种事情真是罕见。"


translate chinese day3_main5_97bec1c5:


    "但是我懒得起来。"


translate chinese day3_main5_dab6de06:


    th "可能白天睡这么长时间不太好吧。"


translate chinese day3_main5_48355910:


    "我准备翘掉舞会去找阿丽夏。"


translate chinese day3_main5_d91422b8:


    "到时间了。"


translate chinese day3_main5_abd3bafa:


    "可是我答应去给列娜帮忙..."


translate chinese day3_main5_f2246ca0:


    th "我现在应该怎么做呢？"


translate chinese day3_main5_5dd208f8:


    "唔，该走了。"


translate chinese day3_main5_a74ee93a:


    "没过几分钟，我已经来到了广场。"


translate chinese day3_main5_c5292350:


    "雕像周围安装上了一些扩音器等音响设备，树木也被装饰上了彩灯。"


translate chinese day3_main5_bfae52bf:


    th "啊，一个典型的乡下的舞会。"


translate chinese day3_main5_b71fb061:


    "附近有很多少先队员，可是没有熟悉的面孔，所以我坐下来等着。"


translate chinese day3_main5_bf88823d:


    th "反正我也不用跳。"


translate chinese day3_main5_ba5eaafc:


    "也许我可以就坐在这里和谁聊聊天。"


translate chinese day3_main5_f62bc905:


    us "怎么这么沮丧啊？"


translate chinese day3_main5_c9bf617b:


    "乌里扬娜。"


translate chinese day3_main5_b9767350:


    me "你有什么建议吗？"


translate chinese day3_main5_ab2952f4:


    us "来吧，一起跳舞啊！"


translate chinese day3_main5_9528c3cd:


    me "现在还太早啦...{w}音乐都还没有开始。"


translate chinese day3_main5_0225548f:


    us "呃，你这人一点意思都没有。"


translate chinese day3_main5_386b2a74:


    th "是，这种活动里我可不是最欢的。"


translate chinese day3_main5_7ad70676:


    "她跑开了。"


translate chinese day3_main5_a11f2300:


    th "乌里扬卡真的穿了一件晚礼服之类的东西。{w}好好笑。"


translate chinese day3_main5_9104ac12:


    sl "嗨！"


translate chinese day3_main5_3c7474d6:


    "斯拉维娅。"


translate chinese day3_main5_a87c71d7:


    me "嘿。"


translate chinese day3_main5_81c4dbcb:


    "她坐在了我的旁边。"


translate chinese day3_main5_c2bffcd7:


    sl "晚上过得怎么样？"


translate chinese day3_main5_e7ce88cd:


    me "还不错。"


translate chinese day3_main5_0d67c838:


    sl "有什么伤心事吗？"


translate chinese day3_main5_b97e3fd5:


    me "我没有..."


translate chinese day3_main5_795edb64:


    sl "好啦，跳跳舞，肯定能让你打起精神来的。"


translate chinese day3_main5_907b6d20:


    me "大概是..."


translate chinese day3_main5_0981716b:


    sl "别忘了为我留一支舞。"


translate chinese day3_main5_5fc53cc4:


    "她笑着跑向了音乐器材那边。"


translate chinese day3_main5_aedce5d2:


    "情况越来越严重了，我无法做到舞会结束。"


translate chinese day3_main5_bd946546:


    un "嗨。"


translate chinese day3_main5_d106dd84:


    "列娜靠了过来。"


translate chinese day3_main5_8bd25464:


    me "哇，你也来咯..."


translate chinese day3_main5_6837bff6:


    th "这有什么奇怪的吗？"


translate chinese day3_main5_f1be5fed:


    un "是的。"


translate chinese day3_main5_4457b2a1:


    me "不错...想要给这场舞会来点亮点吗？"


translate chinese day3_main5_a4468653:


    "没什么意思的笑话。"


translate chinese day3_main5_47092547:


    un "…"


translate chinese day3_main5_c5ac7756:


    "列娜红着脸低下了头。"


translate chinese day3_main5_8c3325f1:


    me "这好像不是个好主意——把事情搞糟..."


translate chinese day3_main5_80702140:


    un "好吧，我猜我会..."


translate chinese day3_main5_279eeea1:


    me "哈..."


translate chinese day3_main5_bc5b46e9:


    "她离开了。"


translate chinese day3_main5_a20cefa7_4:


    "..."


translate chinese day3_main5_94d94b39:


    "好像整个夏令营都来到了这个广场。"


translate chinese day3_main5_3d7269a4:


    "一群群的少先队员们谈笑风生。"


translate chinese day3_main5_e3412cd3:


    "在主持人的位置乌里扬卡和奥尔加·德米特里耶夫娜为了音乐列表的事情吵得不可开交。"


translate chinese day3_main5_461da4d2:


    "这就是了，现在音乐开始了。"


translate chinese day3_main5_e9f89819:


    "我不知道这是什么乐队什么歌曲，不过如果你要问我的话，我会说是经典苏联摇滚。"


translate chinese day3_main5_337cd741:


    "少先队员们好像没有听到音乐一样站在那里。"


translate chinese day3_main5_5f0a6bf8:


    "迈出第一步总是艰难的，尤其是你知道自己会是第一个人的时候..."


translate chinese day3_main5_f14850ff:


    "乌里扬卡好像并没有理解这个简单的道理。"


translate chinese day3_main5_1b76cd8a:


    "她蹦蹦跳跳的跑到广场的中间，然后大喊道："


translate chinese day3_main5_617994a2:


    us "你们还在等什么？！"


translate chinese day3_main5_62d41fb8:


    "然后就开始滑稽的扭动起来。"


translate chinese day3_main5_b59f99e6:


    "扭动是最合适的词语了。{w}没有更合适的了。"


translate chinese day3_main5_530ffc25:


    "看起来很傻，很好笑，我忍不住笑了出来。"


translate chinese day3_main5_da8b7f2f:


    "被她发现了。"


translate chinese day3_main5_015e0ef0:


    us "嘿！谢苗！"


translate chinese day3_main5_11709dee:


    "我装出听不见的样子。"


translate chinese day3_main5_81e15cab:


    us "别装了！你给我过来！"


translate chinese day3_main5_0acd34c9:


    "我继续无视她。"


translate chinese day3_main5_57b6835b:


    "少先队员们开始意识到这场舞会的意义所在，慢慢的开始跳起舞来。"


translate chinese day3_main5_0cfd660b:


    "从我的角度看还是很傻。"


translate chinese day3_main5_6ca388ff:


    th "伴随着这些过时的节奏晃动着全身？{w}现实一点吧！"


translate chinese day3_main5_785dd32e:


    "当然我自己也不会跳舞，不过他们这种恐怕也不是。"


translate chinese day3_main5_7023662f:


    sl "嘿，谢苗，你在这里坐着干什么？{w}你不想跳舞吗？"


translate chinese day3_main5_19346aa1:


    "我沉浸在自己深沉的思考之中，没有注意到斯拉维娅。"


translate chinese day3_main5_6a4b94d5:


    me "没有那么..."


translate chinese day3_main5_d7a9e07a:


    sl "你确定？"


translate chinese day3_main5_d1046a49:


    "她笑了。"


translate chinese day3_main5_65003d93:


    me "一会儿吧。"


translate chinese day3_main5_7ccf3096:


    th "见鬼！我到底是来这里干什么的？！"


translate chinese day3_evening_sl_906aa1af:


    th "如果只是想缩在角落里呆着不动，来这个舞会还有什么意义？"


translate chinese day3_evening_sl_007e73c0:


    me "好吧，只要一下下..."


translate chinese day3_evening_sl_d0c11d8b:


    sl "这才对嘛。"


translate chinese day3_evening_sl_55ab51a9:


    "她向我伸出了她的手。"


translate chinese day3_evening_sl_e6ed4325:


    "我们站在跳舞的人群中间。"


translate chinese day3_evening_sl_0dd1c4e2:


    "事实上，我是在站着，斯拉维娅在“热身”，轻轻摇动着。"


translate chinese day3_evening_sl_588cc129:


    "我已经开始责备自己为什么接受这个请求了。"


translate chinese day3_evening_sl_8bd9ab89:


    th "好了，我上道儿了，然后呢？"


translate chinese day3_evening_sl_3987eb50:


    "我仔细观察着周围的少先队员们。"


translate chinese day3_evening_sl_7cf46727:


    th "不行！我还没有准备好！"


translate chinese day3_evening_sl_6a467bdf:


    "音乐突然中断了。"


translate chinese day3_evening_sl_dd530cb7:


    "有些人还在随着惯性跳动着，不过大多数人都停了下来。"


translate chinese day3_evening_sl_a231e522:


    mt "别紧张！"


translate chinese day3_evening_sl_cdf4ba52:


    "辅导员去音响那里调整了什么。"


translate chinese day3_evening_sl_80ab8af2:


    mt "下一首歌，女生邀请男生。"


translate chinese day3_evening_sl_69f37ce2:


    "我看着斯拉维娅。"


translate chinese day3_evening_sl_e0a7c20b:


    "她微笑着向我伸出了手。"


translate chinese day3_evening_sl_660ca959:


    "你不应该拒绝一个女孩儿。"


translate chinese day3_evening_sl_480cbce8:


    "我们跳了几分钟。"


translate chinese day3_evening_sl_3ff4b37b:


    "她的胸口起伏越来越快，脸色变得越来越红。"


translate chinese day3_evening_sl_98bcf9db:


    "斯拉维娅紧盯着我的眼睛。"


translate chinese day3_evening_sl_77871c15:


    "我尝试着往其他少先队员那里看，或者是看向自己的脚下。"


translate chinese day3_evening_sl_a4bce591:


    "这种感觉好奇怪：我的身体不停地颤抖，但是却很痛快，没有不适的感觉。"


translate chinese day3_evening_sl_d4eb018d:


    "我的意识奇迹般的变得十分冷静。"


translate chinese day3_evening_sl_f5c6ebae:


    "我意识到自己完全不想放开这个女孩儿的手，想要永远的和她跳下去！"


translate chinese day3_evening_sl_5cf561ad:


    "跳来跳去的乌里扬卡停了下来，对着我们傻笑。"


translate chinese day3_evening_sl_67912668:


    "我也说不清，不过这个傲慢的女孩好像在暗示着什么事情。"


translate chinese day3_evening_sl_1eafcf89:


    "我做了一个吓人的表情，可是她已经离开了。"


translate chinese day3_evening_sl_a428f2b1:


    sl "有什么问题吗？"


translate chinese day3_evening_sl_e6509443:


    "斯拉维娅悄悄的问道。"


translate chinese day3_evening_sl_fb700e8f:


    me "一点问题没有...{w}我是说，呃，一切都还好。"


translate chinese day3_evening_sl_46fbad31:


    "我现在还没有结巴的毛病，不过看来是快有了。"


translate chinese day3_evening_sl_df85f677:


    sl "你太紧张了。"


translate chinese day3_evening_sl_89875d8e:


    me "有一点。"


translate chinese day3_evening_sl_9fb02d3f:


    "我诚实的说道。"


translate chinese day3_evening_sl_478c945d:


    sl "男生们不喜欢跳舞。"


translate chinese day3_evening_sl_48176bfb:


    me "好像是的..."


translate chinese day3_evening_sl_43c3e662:


    "她什么也没有说，只是淡淡的笑着。"


translate chinese day3_evening_sl_a20cefa7:


    "..."


translate chinese day3_evening_sl_b2671e23:


    "歌曲最终结束了。"


translate chinese day3_evening_sl_97f9a3fb:


    "我还抱着斯拉维娅，不过她巧妙的滑了出去。"


translate chinese day3_evening_sl_42e06c7b:


    sl "谢谢你陪我跳舞！"


translate chinese day3_evening_sl_39a579dc:


    me "我应该谢谢你。"


translate chinese day3_evening_sl_ff3d4c64:


    "我站在那里继续看着她。"


translate chinese day3_evening_sl_ed185ba1:


    "下一首歌已经开始了。"


translate chinese day3_evening_sl_69421a79:


    sl "继续跳吗？"


translate chinese day3_evening_sl_0a5e88b4:


    me "不了，我觉得我得休息一会儿。"


translate chinese day3_evening_sl_144a26aa:


    "我尽快离开了广场。"


translate chinese day3_evening_sl_46643faf:


    "我不记得自己是怎么来到的海滩了。"


translate chinese day3_evening_sl_b8ec268f:


    th "我为什么要逃跑呢？"


translate chinese day3_evening_sl_52f987f3:


    "那看起来就是逃跑。"


translate chinese day3_evening_sl_a727c48b:


    th "舞跳得不错，斯拉维娅也很高兴。"


translate chinese day3_evening_sl_c5789e6f:


    "可是有些事情不太对，我充满疲倦的回想着刚才的舞会。"


translate chinese day3_evening_sl_915c2c39:


    "也许我只是控制不住局面？"


translate chinese day3_evening_sl_073908ff:


    "那种情况很少发生。"


translate chinese day3_evening_sl_e79ce22b:


    "我总是试着保持冷静的分析自己的处境。{w}但是这次我做到了吗？"


translate chinese day3_evening_sl_cd66cbff:


    "我坐在沙滩上，望着小河。"


translate chinese day3_evening_sl_8a61b36d:


    "水面上挂着一轮满月。"


translate chinese day3_evening_sl_71e227a7:


    "我被自己的回忆淹没。"


translate chinese day3_evening_sl_98e1accf:



    nvl clear
    "6岁的时候，我和父亲一起去钓鱼。我甩出鱼钩就坐下来等着，可是一分钟，五分钟，十分钟过去了，一点动静也没有，相反父亲那边一条接着一条。"


translate chinese day3_evening_sl_01fe6588:


    me "你是怎么做到的？"


translate chinese day3_evening_sl_454a26a0:


    "他向我解释。可是我却想不起来那些细节了。"


translate chinese day3_evening_sl_cd97be40:


    "9岁的时候，一座没有完成的大楼里充满了鬼魂，吸血鬼，或者只是一些流浪汉。一座摇摇晃晃的楼梯跨过直通地下室的深渊连接着二层。"


translate chinese day3_evening_sl_0a0165e9:


    "我很害怕，可还是每天在这条楼梯上爬上爬下，一块砖头砸在了自己的脚上导致我失去了一片指甲。"


translate chinese day3_evening_sl_742d2cdc:


    "12岁，那是我玩游戏时的第一场胜利，虚拟的观众为我欢呼。"


translate chinese day3_evening_sl_6fffdf4d:



    nvl clear
    "15岁的时候，我用脚后跟提出了很多精彩的进球，还有一个三十米之外的射门..."


translate chinese day3_evening_sl_20e21081:


    "17岁的时候，我第一次恋爱，短暂的画面，接着，一个人影消失在城市的雾气之中，我在城市的角落里找不到她。"


translate chinese day3_evening_sl_b039b984:


    "接下来的事情变得更清楚了，可是却不那么现实。"


translate chinese day3_evening_sl_294cee60:


    "儿时的情景并没有那么清楚，那些画面缺少细节，而且我看不清他们的脸，就好像一幅印象派的油画作品。"


translate chinese day3_evening_sl_f7b289c8:


    "不过它们却那么温暖。"


translate chinese day3_evening_sl_76b2fe88:


    nvl clear


translate chinese day3_evening_sl_a20cefa7_1:


    "..."


translate chinese day3_evening_sl_6792bf1f:


    sl "你在这里啊！"


translate chinese day3_evening_sl_f493af42:


    "斯拉维娅靠过来。{w}她没有吓着我。"


translate chinese day3_evening_sl_93544105:


    sl "你怎么那么快就走了呢？"


translate chinese day3_evening_sl_3b07d9cd:


    me "只是想自己呆一会儿。"


translate chinese day3_evening_sl_e79530ba:


    "这就像是躲在壳中，对我来说没有外面的世界。我对外界的刺激总做出相同的反应。{w} 就像是三个猴子，一个聋，一个盲，还有一个傻。"


translate chinese day3_evening_sl_3a8dd74d:


    sl "很漂亮，不是吗？"


translate chinese day3_evening_sl_21eebc87:


    "斯拉维娅看着河水。"


translate chinese day3_evening_sl_63d245c0:


    me "是啊..."


translate chinese day3_evening_sl_d3e0ec8b:


    sl "还继续跳吗？"


translate chinese day3_evening_sl_70849ba4:


    me "不用了，谢谢。"


translate chinese day3_evening_sl_c8cd5039:


    "在我的内心中我不想对斯拉维娅那么，但是我就像是一个木偶，被操控着说出一般情况下我说不出来的话。"


translate chinese day3_evening_sl_29dcd13b:


    sl "你在想什么？"


translate chinese day3_evening_sl_f0e61106:


    me "没什么..."


translate chinese day3_evening_sl_3da81d34:


    sl "你不可能什么都不想！"


translate chinese day3_evening_sl_b29aedbf:


    "她撅起嘴，不过之后又笑了，让我知道她没有生气。"


translate chinese day3_evening_sl_4ab2d20d:


    sl "你总是在想些什么，即使你自己没有意识到。"


translate chinese day3_evening_sl_6ab05865:


    me "也许吧。"


translate chinese day3_evening_sl_fbb41494:


    sl "那么，你现在在想什么呢？"


translate chinese day3_evening_sl_a3db8fcb:


    me "想猫头鹰。"


translate chinese day3_evening_sl_870d8d2c:


    "我说出了头脑中蹦出来的第一件事。"


translate chinese day3_evening_sl_37437f1e:


    sl "为什么是猫头鹰？"


translate chinese day3_evening_sl_ce3666e5:


    "她笑了。"


translate chinese day3_evening_sl_595d7b8c:


    me "我不知道。"


translate chinese day3_evening_sl_cd57019d:


    sl "你见到过猫头鹰吗？"


translate chinese day3_evening_sl_d490fd5c:


    me "当然见过了。"


translate chinese day3_evening_sl_05e130d4:


    th "好傻的问题..."


translate chinese day3_evening_sl_1537169b:


    sl "你喜欢它们吗？"


translate chinese day3_evening_sl_66bebe85:


    me "就和其他的鸟一样。"


translate chinese day3_evening_sl_1dcb4b49:


    sl "是夜行鸟。"


translate chinese day3_evening_sl_26b4f833:


    "斯拉维娅说的更具体。"


translate chinese day3_evening_sl_c6d4e117:


    me "是啊，是夜行鸟。"


translate chinese day3_evening_sl_67f5dc8a:


    th "夜行..."


translate chinese day3_evening_sl_f08d71cf:


    "我想象一个猫头鹰的形象，一个羽毛盒子装上大大的一眨一眨的眼睛。"


translate chinese day3_evening_sl_35f51a31:


    sl "你是夜猫子吗？"


translate chinese day3_evening_sl_e54b39e9:


    me "什么？"


translate chinese day3_evening_sl_0db8f540:


    sl "就是说有百灵鸟和和夜猫子，有人爱早起，有人爱晚睡。"


translate chinese day3_evening_sl_e90153b6:


    "这些话把我带回了现实。"


translate chinese day3_evening_sl_a6f160d4:


    "我开始回忆我乱糟糟的房间，到处是垃圾，堆积如山的脏盘子，角落里吃灰的吉他，灯上挂着领带，还有在最高处的旧袜子。"


translate chinese day3_evening_sl_4ac82707:


    "我确实属于夜猫子。{w}晚上是我的时间。"


translate chinese day3_evening_sl_2e21ef54:


    "可是不知道为什么在这个夏令营我可以早起。"


translate chinese day3_evening_sl_dc4d60ee:


    me "我知道。"


translate chinese day3_evening_sl_b712550e:


    sl "那么你是什么呢？"


translate chinese day3_evening_sl_1d219eb9:


    me "夜猫子吧...我喜欢睡觉。"


translate chinese day3_evening_sl_6b0f0e89:


    sl "那我就是百灵鸟！{w}你起的越早，白天能干的事就越多。"


translate chinese day3_evening_sl_8392621f:


    "我真的没有什么事干，所以感觉不出什么区别。{w}只是晚上更安静。"


translate chinese day3_evening_sl_09bf6d3d:


    me "斯拉维娅，你的生活中难道就没有一点困难吗？"


translate chinese day3_evening_sl_5240e44d:


    sl "是什么意思？"


translate chinese day3_evening_sl_a4614188:


    "她感到不解。"


translate chinese day3_evening_sl_77f4a782:


    me "你总是这么开心，时刻准备着帮助别人，充满活力做一件又一件的事。"


translate chinese day3_evening_sl_7f5e995c:


    me "好像没有什么可以让你失望。"


translate chinese day3_evening_sl_af407039:


    sl "为什么？！"


translate chinese day3_evening_sl_ec2c2a7c:


    "她笑了。"


translate chinese day3_evening_sl_cbff0809:


    sl "我只是一个普通人。"


translate chinese day3_evening_sl_7c783ece:


    th "是啊。{w}只是一个在普通的地方的普通人。"


translate chinese day3_evening_sl_cce49fa3:


    me "有时候我觉得在这里我是最奇怪的人。"


translate chinese day3_evening_sl_52f705e3:


    sl "你不喜欢这个夏令营吗？"


translate chinese day3_evening_sl_8f568239:


    me "我不是说只有营地，在之前..."


translate chinese day3_evening_sl_2a7a8a3b:


    "我自言自语。"


translate chinese day3_evening_sl_4b44803d:


    th "我和她也不应该这么坦白吧。"


translate chinese day3_evening_sl_5d943aa7:


    me "在家也是。{w}我觉得我和别人不一样，我不属于这里..."


translate chinese day3_evening_sl_4b441221:


    sl "别那样说啊，那样想太傻了。"


translate chinese day3_evening_sl_81571d87:


    th "刚刚我还把自己和这个世界隔离开来，现在我却对这个女孩而打开心扉。"


translate chinese day3_evening_sl_15fa3132:


    th "我到底怎么了？"


translate chinese day3_evening_sl_97fb9be9:


    me "不，是真的。如果是在别的时候你不会注意到我的。{w}我和你的差别如此之大，我很懒，不擅长交际，又没有什么特长。"


translate chinese day3_evening_sl_45a7a568:


    me "在一大群人里我会是你最后注意到的人。{w}虽然我都很少出门。"


translate chinese day3_evening_sl_618ab2ac:


    sl "谢苗，你吓到我了。"


translate chinese day3_evening_sl_453d16cb:


    "她认真的看着我。"


translate chinese day3_evening_sl_d4064844:


    "我很不好意思，但是没有移开自己的目光。"


translate chinese day3_evening_sl_aad6bab5:


    me "我错了吗？"


translate chinese day3_evening_sl_738f16d6:


    sl "当然啊！{w}你就是你，是独一无二的。你只是缺少一点自尊心和耐心，我可以确定，再加上这两点，你肯定没问题的。"


translate chinese day3_evening_sl_c4f4bcb6:


    "我看向天空。"


translate chinese day3_evening_sl_3b021425:


    me "要是事情都向你说得那么简单就好了..."


translate chinese day3_evening_sl_b574b5d1:


    sl "这一点都不难，我们现在就可以开始。"


translate chinese day3_evening_sl_1ad35916:


    me "开始什么？"


translate chinese day3_evening_sl_7109d569:


    sl "改变！"


translate chinese day3_evening_sl_ee1a91b3:


    th "能改变什么呢？"


translate chinese day3_evening_sl_ea49f8ff:


    me "你为什么觉得我们可以做到呢？"


translate chinese day3_evening_sl_541b7562:


    sl "我们应该做一些有用的事。"


translate chinese day3_evening_sl_e473fa2f:


    me "比如说？"


translate chinese day3_evening_sl_7db509b3:


    sl "那个..."


translate chinese day3_evening_sl_60f3d758:


    "她思考了一会儿。"


translate chinese day3_evening_sl_44f18559:


    sl "咱们清理一下广场吧！舞会已经结束了！"


translate chinese day3_evening_sl_6e3178d0:


    me "只有咱们两个吗？"


translate chinese day3_evening_sl_bd88c88c:


    th "两个人可以干得过来吗？"


translate chinese day3_evening_sl_be7f9c31:


    sl "其实没有那么麻烦的。{w}第一步才是最重要的！"


translate chinese day3_evening_sl_c3348a75:


    th "我说不过她，可是在黑夜里摘掉树上的灯光，搬动很重的音响还有打扫卫生..."


translate chinese day3_evening_sl_03eb9a2e:


    "我的计划清单里可没有这些项目。"


translate chinese day3_evening_sl_0e6e2d6e:


    "可是我不知道应该如何有礼貌的拒绝她。"


translate chinese day3_evening_sl_615361ab:


    me "也许我们可以明天再做？和大家一起？"


translate chinese day3_evening_sl_c0da7d07:


    sl "为什么推到明天呢？{w}集合的时候站在一个干净的广场不是更好吗？"


translate chinese day3_evening_sl_8cede868:


    th "而且我也不怎么喜欢那个集合仪式。"


translate chinese day3_evening_sl_5b4109bc:


    me "喂，我理解你...{w}可是这需要干的活儿太多了吧？！"


translate chinese day3_evening_sl_bbc1f2e4:


    "斯拉维娅想了想。"


translate chinese day3_evening_sl_245f16e1:


    sl "是的，好像你说的是对的。"


translate chinese day3_evening_sl_3e8d56f1:


    th "我也有可能说得对啊。{w}她也有错的时候。"


translate chinese day3_evening_sl_d746a7a3:


    sl "那么..."


translate chinese day3_evening_sl_83de587e:


    "她伸了个懒腰。"


translate chinese day3_evening_sl_3b5d4e01:


    sl "那么我们就去睡觉吧！"


translate chinese day3_evening_sl_3cd13b17:


    "好主意，可是我还想再聊聊。"


translate chinese day3_evening_sl_186a1c59:


    me "现在不是有点...太早了？"


translate chinese day3_evening_sl_bf68bee5:


    sl "不！"


translate chinese day3_evening_sl_8b29d788:


    "斯拉维娅充满责备的反驳道。"


translate chinese day3_evening_sl_440467e0:


    sl "你明天会睡一整天的。"


translate chinese day3_evening_sl_3dd3f806:


    me "那就给我讲一个睡前故事！"


translate chinese day3_evening_sl_75c35567:


    "我随便把心里想到的第一件事说了出来。"


translate chinese day3_evening_sl_e8eb4335:


    sl "我只知道书上的那些，你一定也会知道的。"


translate chinese day3_evening_sl_807b7383:


    th "也许是的..."


translate chinese day3_evening_sl_744ed324:


    "好失败啊。"


translate chinese day3_evening_sl_ad3d2e0c:


    sl "我们可以走了吗？"


translate chinese day3_evening_sl_55ab51a9_1:


    "她向我伸出了手。"


translate chinese day3_evening_sl_6199f44f:


    me "我再待一会儿，再呼吸一下新鲜空气。"


translate chinese day3_evening_sl_76d0ac27:


    me "你先走吧！晚安！"


translate chinese day3_evening_sl_5f3273c1:


    sl "晚安！"


translate chinese day3_evening_sl_ec475aa3:


    "斯拉维娅跑开了。"


translate chinese day3_evening_sl_22e44be2:


    "我想先享受一下自己的失败。"


translate chinese day3_evening_sl_4ff0543c:


    "刚才那句话说出来之后我就不好意思再和斯拉维娅待着了，睡前故事？亏我好意思说..."


translate chinese day3_evening_sl_335c00f5:


    th "事情就是这样的，你想女孩子好好的谈谈话，然后就会一败涂地。"


translate chinese day3_evening_sl_368a09ed:


    "我望着夜空。"


translate chinese day3_evening_sl_7de6936f:


    "眨着眼的遥远恒星好像在嘲笑我一样。"


translate chinese day3_evening_sl_b61a3a4c:


    "我转过身把脸埋在沙土里。"


translate chinese day3_evening_sl_10854c7a:


    "过了一秒钟我发现这也不是个好主意。"


translate chinese day3_evening_sl_a8978170:


    "我站起来，吐掉嘴里的沙子，走向辅导员的房间。"


translate chinese day3_evening_sl_a20cefa7_2:


    "..."


translate chinese day3_evening_sl_5195ec0b:


    "我的后背一直在疼痛。"


translate chinese day3_evening_sl_81f35693:


    "不只是后背，而是整个身体。"


translate chinese day3_evening_sl_533207f2:


    "这很正常，我好几天没有洗澡了，而且在这里你出汗像个猪一样。"


translate chinese day3_evening_sl_17474fa5:


    th "我应该找到香皂还有毛巾什么的。"


translate chinese day3_evening_sl_aadfea7e:


    "我应该可以在奥尔加·德米特里耶夫娜的房间里找到这些东西。"


translate chinese day3_evening_sl_f8c52b26:


    "过了几分钟，我又拿着洗澡装备回到了广场。"


translate chinese day3_evening_sl_b0cbc573:


    th "我现在得找到浴室。"


translate chinese day3_evening_sl_3b2b772c:


    "这很简单，那栋建筑就在森林的边上。"


translate chinese day3_evening_sl_662916c1:


    th "这真是建造浴室的好地方。"


translate chinese day3_evening_sl_d2fefd9e:


    "我觉得这里边应该没有雅加婆婆什么的吧，可是灯是亮着的。"


translate chinese day3_evening_sl_2ee9293b:


    me "又有谁喜欢半夜在这里洗澡呢？"


translate chinese day3_evening_sl_2eb0d079:


    "I我轻轻的自言自语。"


translate chinese day3_evening_sl_68584892:


    "这只有一个问题：是男孩还是女孩？"


translate chinese day3_evening_sl_c7224776:


    "要是第一种情况的话，我们可以很愉悦的一起洗澡。"


translate chinese day3_evening_sl_289b8414:


    th "虽然我不喜欢公共澡堂，可是还算应付得过来。"


translate chinese day3_evening_sl_0c723b2c:


    "如果是第二种情况，我得等一会儿或者一整夜挠来挠去。"


translate chinese day3_evening_sl_99683490:


    th "谁知道我得等多长时间啊..."


translate chinese day3_evening_sl_9c652616:


    "我偷偷向窗户里看去，可是被水蒸气遮挡着什么也看不到。"


translate chinese day3_evening_sl_de4b06b3:


    "突然间，斯拉维娅像从浓雾里出现了。"


translate chinese day3_evening_sl_3300e119:


    "很正常的，她没有穿衣服，就和一般人洗澡的时候一样。"


translate chinese day3_evening_sl_f8f1d6e9:


    "我愣住了，呆呆的看着她。"


translate chinese day3_evening_sl_45d87efb:


    "我从来没有这么近距离的观察一个裸体的女孩子，即使是隔着玻璃。"


translate chinese day3_evening_sl_4029c0f3:


    "不用说，我的身体的反应是迅速的。"


translate chinese day3_evening_sl_0ad798c3:


    "我全身上下的神经都变得紧张起来。"


translate chinese day3_evening_sl_c0894e89:


    "好像没有什么可以让我的视线离开，即使是一场战争爆发。"


translate chinese day3_evening_sl_addbff34:


    "可是斯拉维娅没有发现我。"


translate chinese day3_evening_sl_defd30a4:


    "她悠闲的洗着。洗过脸后，用海绵擦遍全身然后举起水桶冲净。"


translate chinese day3_evening_sl_4cdeecd0:


    "然后开始洗头发。"


translate chinese day3_evening_sl_f24eb2b8:


    "我不知道她到底需要多长时间才能洗完。不过这几分钟过的像一秒钟，我就这样沉浸在这幅画面里。"


translate chinese day3_evening_sl_a20cefa7_3:


    "..."


translate chinese day3_evening_sl_9d93e651:


    "最后她终于洗完了，满意的叹了口气，走向了门口。"


translate chinese day3_evening_sl_a20cefa7_4:


    "..."


translate chinese day3_evening_sl_4ad1145a:


    "她走进更衣室我才回过神来，急忙躲进草丛中。"


translate chinese day3_evening_sl_9d9ca7f3:


    "最好的选择还是承认了，不过等我意识到的时候已经太晚了。"


translate chinese day3_evening_sl_bf131a4a:


    "斯拉维娅不一会儿就走了出来，站在门口享受了一会儿夜晚的清风，便径直朝我躲藏的方向走了过来。"


translate chinese day3_evening_sl_c31ff7a6:


    "我的大脑中闪现过十几种理由，可是她走了过去，即便是向我这边张望了之后。"


translate chinese day3_evening_sl_efff4fc1:


    th "看来我很幸运啊..."


translate chinese day3_evening_sl_62c6e320:


    "我小心的回到辅导员的房间，完全忘记了想要洗澡的想法。"


translate chinese day3_evening_sl_a085be61:


    "我累极了，只想赶快扑到床上。"


translate chinese day3_evening_sl_d8f66d7a:


    "我的眼睛不受控制的不停闭上。"


translate chinese day3_evening_sl_a936b78e:


    "连眨一下眼都变得很危险。"


translate chinese day3_evening_sl_3d48081a:


    "我对营地还没有熟悉到闭着眼可以走路的程度。"


translate chinese day3_evening_sl_a20cefa7_5:


    "..."


translate chinese day3_evening_sl_44fae48f:


    "突然斯拉维娅出现在了我的面前。"


translate chinese day3_evening_sl_3820771a:


    me "..."


translate chinese day3_evening_sl_6de32e31:


    sl "你要准备睡觉了吗？"


translate chinese day3_evening_sl_da49764c:


    me "嗯...是的..."


translate chinese day3_evening_sl_97fc1ed6:


    "这个时候我突然想起来刚才浴室的画面，不好意思的移开了目光。"


translate chinese day3_evening_sl_b78c6e26:


    me "你呢？"


translate chinese day3_evening_sl_7405f4b0:


    sl "我准备打扫卫生！"


translate chinese day3_evening_sl_2214025f:


    "她从身后拿出了扫把。"


translate chinese day3_evening_sl_dfc6b36d:


    "斯拉维娅拿着扫把站在黑夜中的广场就像是童话中的巫师。"


translate chinese day3_evening_sl_e07c37b3:


    sl "什么？"


translate chinese day3_evening_sl_62fd010a:


    me "没什么，只是刚刚洗完澡就打扫卫生是个好主意吗？"


translate chinese day3_evening_sl_f52a7f9e:


    sl "你怎么知道我刚刚洗完澡？"


translate chinese day3_evening_sl_28bebef0:


    "我感觉到恐惧占领了我的全身，冷汗不断沿着后背流下，我的意识凝固了，想不出任何理由。"


translate chinese day3_evening_sl_3820771a_1:


    me "..."


translate chinese day3_evening_sl_a684124e:


    "斯拉维娅还在惊讶的盯着我。"


translate chinese day3_evening_sl_888019d6:


    "看起来我的人生都取决于这个答案了。"


translate chinese day3_evening_sl_12308c9c:


    me "那个，你的头发湿湿的..."


translate chinese day3_evening_sl_1f26ca7d:


    sl "啊...对了。"


translate chinese day3_evening_sl_5d0b4e88:


    "我想要找个地洞钻进去，这样就可以瞬间从地球上消失，就像我是怎么从现实世界消失的一样。"


translate chinese day3_evening_sl_c493719d:


    sl "你也要洗澡吗？"


translate chinese day3_evening_sl_2e67d98e:


    me "我...这个嘛..."


translate chinese day3_evening_sl_3a1790bb:


    "斯拉维娅看向我的包包。"


translate chinese day3_evening_sl_92d5dd33:


    me "是的！那么晚安了！"


translate chinese day3_evening_sl_6b587b4c:


    "我转过身迅速冲向浴室。"


translate chinese day3_evening_sl_63c688cb:


    th "真是一个悲剧啊，好羞耻..."


translate chinese day3_evening_sl_09404bd9:


    "我慢慢的走着，为了刚才的事情诅咒着自己。"


translate chinese day3_evening_sl_ee7118ff:


    th "我一开始就不应该偷看的..."


translate chinese day3_evening_sl_e5405609:


    th "如果我偷看了也应该提前想好一个借口的。"


translate chinese day3_evening_sl_45531a98:


    th "唔..."


translate chinese day3_evening_sl_a20cefa7_6:


    "..."


translate chinese day3_evening_sl_0cfbc599:


    "洗澡并不需要多长时间。"


translate chinese day3_evening_sl_9dc5cc01:


    "夜晚就像是一场舞会。{w}星星作为霓虹灯，虫鸟演奏音乐，不知在哪里的一只猫头鹰是整场表演的导演。树林里的风声则是观众的掌声。"


translate chinese day3_evening_sl_36d666ac:


    "洗干净之后夜色也显得更美了。"


translate chinese day3_evening_sl_501fc9a9:


    "突然我听见附近的草丛里有些动静。"


translate chinese day3_evening_sl_7492209c:


    "我哆嗦了一下，但还不是很害怕。"


translate chinese day3_evening_sl_c160bc4e:


    th "也许是松鼠或者是别的什么小动物。"


translate chinese day3_evening_sl_a710287b:


    th "即使如此也应该查看一下。"


translate chinese day3_evening_sl_add052f9:


    "我走向灌木丛。"


translate chinese day3_evening_sl_f110d6af:


    "但是一无所获。"


translate chinese day3_evening_sl_869f707c:


    th "也许是错觉吧..."


translate chinese day3_evening_sl_bd3b5e0d:


    "我神清气爽的回到了辅导员的房间。"


translate chinese day3_evening_sl_1424964a:


    "辅导员已经睡了。"


translate chinese day3_evening_sl_01ffcfe1:


    "我衣服也没脱就躺在了床上，然后盖上了一张毯子。"


translate chinese day3_evening_sl_b675339f:


    "我辗转反侧难以入睡，回想到斯拉维娅的身体，任凭我思考什么问题也无法将那幅画面抹消。"


translate chinese day3_evening_un_dee15e4b:


    "如果再跳一次舞，我的自尊心大概会归零。"


translate chinese day3_evening_un_f61e6089:


    "我正在绞尽脑汁思考有没有离开这里的借口，正在这时，列娜正在往我这边走过来。"


translate chinese day3_evening_un_b9dd006a:


    un "我们大概需要走了吧？"


translate chinese day3_evening_un_25f7abbc:


    me "去哪儿？"


translate chinese day3_evening_un_f12b72b3:


    "我沉浸在自己的想法里，没有理解她到底是什么意思。"


translate chinese day3_evening_un_bf618d8b:


    un "去医务室。{w}如果你想要在这里继续跳舞的话..."


translate chinese day3_evening_un_a35ed640:


    "我很怀疑她想不想跳舞，列娜整晚都站在一旁看着。"


translate chinese day3_evening_un_5b6584a0:


    me "不，我就算了！{w}咱们走吧！"


translate chinese day3_evening_un_ea985af0:


    th "至少我不用再像个傻瓜一样站在这里！"


translate chinese day3_evening_un_329854dd:


    "说真的，把自己隐藏在角落里不是很舒服。"


translate chinese day3_evening_un_c5a5dd23:


    th "瓷器店里的一头牛也会比我站在舞台上灵活。"


translate chinese day3_evening_un_3db6c6b7:


    "我本来就没有一点想要跳舞的打算。"


translate chinese day3_evening_un_68b672da:


    un "好吧，我们可以去医务室了吗？"


translate chinese day3_evening_un_ef4b7608:


    "列娜把我从自己的思考中带了回来。"


translate chinese day3_evening_un_3b8ef189:


    "我们一言不发的在食堂前面站了一会儿。"


translate chinese day3_evening_un_95bffd3a:


    me "好的...谢谢你！"


translate chinese day3_evening_un_2e3d9fb1:


    un "为什么？"


translate chinese day3_evening_un_f8a6296f:


    "她吃惊的看着我。"


translate chinese day3_evening_un_177d6e89:


    me "那个...因为你帮我逃离那个地方..."


translate chinese day3_evening_un_cea5943f:


    "我不应该告诉她我不擅长跳舞的。"


translate chinese day3_evening_un_966aaa9d:


    me "就是说...那个...你懂的，那里太没有意思了！"


translate chinese day3_evening_un_fb773468:


    un "看起来你好像不喜欢跳舞。"


translate chinese day3_evening_un_d153fd8d:


    "她纯洁甚至有点孩子气的脸上看不出一点讽刺的意思。"


translate chinese day3_evening_un_5e7d152e:


    "她似乎真的不理解。"


translate chinese day3_evening_un_bae3aca3:


    me "是啊，完全不喜欢。"


translate chinese day3_evening_un_5b72d7bd:


    un "我也是。{w}从来没有人邀请我去跳舞。"


translate chinese day3_evening_un_2f7bb5d6:


    "列娜像平常一样红着脸看向脚下。"


translate chinese day3_evening_un_4cb51946:


    me "奇怪。"


translate chinese day3_evening_un_940302b3:


    un "什么奇怪？"


translate chinese day3_evening_un_f9720baf:


    me "就是说...怎么会没有人邀请你呢？"


translate chinese day3_evening_un_4429143b:


    un "你是这么想的吗？"


translate chinese day3_evening_un_ca11e47a:


    "她又一次用那种没有明白的吃惊表情看着我。"


translate chinese day3_evening_un_92393659:


    "我有点迷惑，不知道该如何回应。"


translate chinese day3_evening_un_34c195e3:


    me "是啊！当然！如果我擅长跳舞的话，肯定会邀请你的！"


translate chinese day3_evening_un_082d3571:


    un "多谢..."


translate chinese day3_evening_un_e99c1593:


    "我们后来便没有再说话。"


translate chinese day3_evening_un_542f1cb7:


    "列娜好像被我的赞赏说的晕乎乎的，我也一直没有找到可以说的话题。"


translate chinese day3_evening_un_c0de7e94:


    "这是天已经完全黑了，夜晚的雾气中朦朦胧胧的医务室就像是一个鬼屋。"


translate chinese day3_evening_un_2079dd47:


    "我十分想悄悄的离开这个鬼地方。"


translate chinese day3_evening_un_13fbafd7:


    "我偷偷看了看列娜，她似乎和平常差不多，有些害羞并且保持端庄，但是看不出一点害怕。"


translate chinese day3_evening_un_6cdc223c:


    "这让我更加心虚了。"


translate chinese day3_evening_un_095403d0:


    th "这可不行啊，她都没有害怕，我却..."


translate chinese day3_evening_un_cc26ac1a:


    "突然一只猫头鹰咕咕的叫了起来，吓得我一个激灵。"


translate chinese day3_evening_un_cc73b68c:


    "列娜看起来要么是没有听见，要么是没注意，或者是完全不害怕。"


translate chinese day3_evening_un_9b14946c:


    "我觉得不太可能是第三种，但是不想询问她，暴露出自己的恐惧。"


translate chinese day3_evening_un_24cab478:


    "我走进医务室，在黑暗中找到了一个开关。"


translate chinese day3_evening_un_49249630:


    me "护士一会儿回来吗？"


translate chinese day3_evening_un_404b569f:


    un "她不会来的..."


translate chinese day3_evening_un_e0ce2859:


    th "好吧，她不会来的，明白了...{w}等一下...{w}什么叫她不会来了？"


translate chinese day3_evening_un_e1a4709a:


    me "啊，我懂了..."


translate chinese day3_evening_un_5fa11b3d:


    "不是我在这种情况下和列娜独处感觉害怕。{w}在小屋里。{w}夜晚。{w}周围一个人没有..."


translate chinese day3_evening_un_36602009:


    th "只有电影里才会在这种情况下出点儿什么事。"


translate chinese day3_evening_un_0216a72c:


    "只是我和列娜在一起，不是乌里扬娜或者斯拉维娅。"


translate chinese day3_evening_un_83f4508f:


    "这导致我对很多事情的反应有些不同。"


translate chinese day3_evening_un_9e2d7d99:


    un "就是这些箱子。"


translate chinese day3_evening_un_923e6e0a:


    "她指向那堆乱糟糟的东西。"


translate chinese day3_evening_un_9066f7f1:


    "大概有十几个。"


translate chinese day3_evening_un_75fde160:


    "这可不是几分钟就能干完的。"


translate chinese day3_evening_un_3a066bb2:


    "我搬起一个箱子放到面前开始检查里面的内容。"


translate chinese day3_evening_un_30cdf24e:


    "里边是各种创可贴，一小包一小包的。"


translate chinese day3_evening_un_b4c9e1a9:


    un "拿着这个。"


translate chinese day3_evening_un_ec9bb664:


    "列娜给了我一小张纸片。"


translate chinese day3_evening_un_b5daf913:


    "我很快明白应该把名称写在左侧，描述写在中间，然后如果需要的话把数量写在右边。"


translate chinese day3_evening_un_50cb3d48:


    th "没有数据库什么的，不过我还是没问题的。"


translate chinese day3_evening_un_48b8c403:


    "工作开始了。"


translate chinese day3_evening_un_2b543552:


    un "谢苗..."


translate chinese day3_evening_un_ad02f53a:


    me "什么？"


translate chinese day3_evening_un_0c8d272a:


    "我看向列娜。"


translate chinese day3_evening_un_3500c639:


    "列娜盯着我看了一会儿，好像要下决心做什么事，可是还是低下了头继续工作。"


translate chinese day3_evening_un_8da69387:


    un "没事，别在意。"


translate chinese day3_evening_un_9f980148:


    "我生理上忍受不了坐在这里一言不发的状态。{w}可是我不敢先说话，不是因为我想不出话题，而是我不好意思说，对这个女孩儿说什么都有可能让她害羞。"


translate chinese day3_evening_un_2b543552_1:


    un "谢苗..."


translate chinese day3_evening_un_e79d0fd5:


    me "哎？"


translate chinese day3_evening_un_dbc3003f:


    un "你数重了。"


translate chinese day3_evening_un_e1d4a194:


    "哇，好像是的。"


translate chinese day3_evening_un_87731a54:


    me "唔...抱歉。"


translate chinese day3_evening_un_d00d3d0f:


    "她没有回答。"


translate chinese day3_evening_un_6b863b6b:


    me "嘿。你来自哪里？就是说...你是哪里人？那个，你从哪里来？呃...你住在哪里？"


translate chinese day3_evening_un_4dbf341d:


    un "我...{w}附近有一个小镇..."


translate chinese day3_evening_un_652ee7d0:


    me "不远是哪里？"


translate chinese day3_evening_un_e807e738:


    un "某个地方..."


translate chinese day3_evening_un_adc0b8f3:


    "她好像不太想说这个问题。"


translate chinese day3_evening_un_270ee4bf:


    th "列娜也在隐藏着什么吗？"


translate chinese day3_evening_un_1663f57c:


    "奥尔加·德米特里耶夫娜这样做可以理解，可是列娜..."


translate chinese day3_evening_un_7edc04e0:


    th "这完全不是她的风格。"


translate chinese day3_evening_un_4b79f78a:


    me "那是秘密？"


translate chinese day3_evening_un_f4abdb32:


    un "不...{w}只是..."


translate chinese day3_evening_un_8a3717f6:


    me "所以说，也是南方的某个地方吗？"


translate chinese day3_evening_un_9857e8f9:


    "我应该说点更有意义的话来着。"


translate chinese day3_evening_un_6791c4d1:


    "我的每个主意都很糟糕，这个当特务的想法尤其如此。"


translate chinese day3_evening_un_fa0dd942:


    "我想起南方是因为我记得的苏联夏令营只有Artek。（位于克里米亚）"


translate chinese day3_evening_un_c81cda2f:


    un "...是的..."


translate chinese day3_evening_un_e39caf93:


    "列娜回答的很犹豫。"


translate chinese day3_evening_un_45925f2f:


    "我听不出来她是不是在说谎。"


translate chinese day3_evening_un_3dfaf878:


    un "你不喜欢这里吗？"


translate chinese day3_evening_un_38d9fd3d:


    th "我很奇怪我说的什么话让她这么想？"


translate chinese day3_evening_un_8610e699:


    me "当然不是啊！我很喜欢这里！"


translate chinese day3_evening_un_f13d454c:


    "我的虚情假意听起来一点都不真诚，反而让人恼火。"


translate chinese day3_evening_un_1a377d44:


    me "你呢？"


translate chinese day3_evening_un_cc213e9d:


    un "我很喜欢。{w}这里很安静，图书馆里有很多的书。{w}这里的人们也都很好..."


translate chinese day3_evening_un_a3126422:


    me "好...但是并不都是这样..."


translate chinese day3_evening_un_b672514d:


    un "为什么？"


translate chinese day3_evening_un_6cde6e35:


    "我刚才说出声来了吗？"


translate chinese day3_evening_un_4c03a0c5:


    "看来是的。"


translate chinese day3_evening_un_339a1158:


    me "这个嘛...{w}比如说乌里扬卡，她就像一个失控的劲量电池。"


translate chinese day3_evening_un_d39ab564:


    un "电池...{w}什么？"


translate chinese day3_evening_un_4d8feba6:


    "可能我确实被送到了过去。"


translate chinese day3_evening_un_6421514b:


    me "没什么。"


translate chinese day3_evening_un_a656cdf8:


    me "或者是阿丽夏！别人都说少先队员是模范好孩子，肯定不包括她！如果每个人都学习她的话会给国家带来二十年的灾难..."


translate chinese day3_evening_un_61114f6a:


    th "不过现在想想的话，你可以得出80年代确实每个人都学习了阿丽夏·朵切芙斯卡娅的结论。"


translate chinese day3_evening_un_f187db47:


    un "她不是那样的，真的。"


translate chinese day3_evening_un_76bd4f62:


    me "“不是那样的”，不是哪样的？"


translate chinese day3_evening_un_0a526940:


    un "她不是像你说的那样的。"


translate chinese day3_evening_un_61c3831d:


    me "首先来说，我还什么都没有说呢。{w}我只是说她不是最适合我们学习的对象。"


translate chinese day3_evening_un_21b0641e:


    un "嗯...也许吧。"


translate chinese day3_evening_un_25b8be5c:


    me "听起来你很了解她。"


translate chinese day3_evening_un_cd1ed0e4:


    un "大概。"


translate chinese day3_evening_un_2bf6a5e9:


    "我问这个问题只是为了把对话继续下去，没想到得到这样的回答。"


translate chinese day3_evening_un_b3051fc6:


    "阿丽夏和列娜相差太多了，她们两个应该不会那么熟。"


translate chinese day3_evening_un_68730dd3:


    un "我们来自同一个小镇。"


translate chinese day3_evening_un_508a08eb:


    "好像她预知了我的下一个问题。"


translate chinese day3_evening_un_2a8c5047:


    un "我们有一些共同的朋友，虽然阿丽夏要大一岁。"


translate chinese day3_evening_un_eaa5b39b:


    me "好吧，这有一点奇怪。{w}不是那样，我只是有点惊讶。"


translate chinese day3_evening_un_25ed7170:


    un "每个人都会吃惊的。"


translate chinese day3_evening_un_d7a43efd:


    "列娜微笑着。"


translate chinese day3_evening_un_0bf46b97:


    "我搬起第二个箱子。"


translate chinese day3_evening_un_f520e822:


    "安乃近，活性炭，安乃近，活性炭， 生理盐水，高锰酸钾，呋喃西林，安乃近..."


translate chinese day3_evening_un_88d5fb70:


    "列娜总是说一些简单句。"


translate chinese day3_evening_un_2594eb2e:


    "如果我们的交流最后都会变成独白或者是冷场...我还怎么说话啊？"


translate chinese day3_evening_un_d0a225ef:


    "我不喜欢这种感觉。"


translate chinese day3_evening_un_69f23bef:


    "有时候我感觉在她害羞的面具后面隐藏着什么。{w}但是究竟是什么呢？"


translate chinese day3_evening_un_52bebeb3:


    me "话说，我不久前刚刚读了一本书...{w}你喜欢科幻小说吗？"


translate chinese day3_evening_un_267018a5:


    un "不是很热衷。"


translate chinese day3_evening_un_eda10b2f:


    th "见鬼，又一次失败。"


translate chinese day3_evening_un_41202424:


    me "那个，如果你不喜欢的话...{w}你究竟喜欢什么书呢？"


translate chinese day3_evening_un_25828f24:


    un "各种..."


translate chinese day3_evening_un_c28a338c:


    "对话不太顺利，我最好就此打住。"


translate chinese day3_evening_un_c7228702:


    "谁知道这是怎么回事，不过我又想起了舞会。"


translate chinese day3_evening_un_7e05f767:


    "紧张和局促又一次淹没了我。"


translate chinese day3_evening_un_9a396b7b:


    "看来我和列娜遇到事情的时候反应差不多。当我遇到不了解或者不会的事情的时候就会变得不知所措。"


translate chinese day3_evening_un_54d0f688:


    th "我的当务之急是战胜自己的恐惧。"


translate chinese day3_evening_un_cd4e8ca1:


    th "这会让我更加了解她。"


translate chinese day3_evening_un_7c4840c7:


    "我做出决定了。"


translate chinese day3_evening_un_f8fb534c:


    me "只剩下几个箱子了..."


translate chinese day3_evening_un_f1be5fed:


    un "是的。"


translate chinese day3_evening_un_e1fb3662:


    me "嘿，我有了个主意！{w}咱们一会儿一块儿去食堂怎么样？{w}我记得录音机在那里。"


translate chinese day3_evening_un_9a25521c:


    th "去食堂能干什么呀？"


translate chinese day3_evening_un_3db7cd22:


    "我犹豫了。"


translate chinese day3_evening_un_41cfffd5:


    un "为什么？"


translate chinese day3_evening_un_4125d583:


    "她真诚的表情让我明白了她完全不明白我是什么意思。"


translate chinese day3_evening_un_1279bc8e:


    me "那个...说实话，我不知道该怎么跳舞，所以我不喜欢跳舞。{w}所以我刚才在那里有些应付不过来。"


translate chinese day3_evening_un_054e8b5f:


    me "也许我可以和你跳舞来感谢你。"


translate chinese day3_evening_un_8ebabae6:


    un "可是我..."


translate chinese day3_evening_un_7c35a05f:


    "她停下手上的工作想了一会儿，然后红着脸看着我的眼睛。"


translate chinese day3_evening_un_7f15ab29:


    "我很困惑。"


translate chinese day3_evening_un_72e48efd:


    th "这真是个愚蠢的主意。"


translate chinese day3_evening_un_99ad9909:


    me "没关系，如果你不愿意的话...{w}就当我没说！"


translate chinese day3_evening_un_d8e58db9:


    un "要是我们被看到了怎么办？"


translate chinese day3_evening_un_5226ed1b:


    "我肯定是没有想到那个情况。"


translate chinese day3_evening_un_9cb1a370:


    "这不是什么问题嘛。{w}可是..."


translate chinese day3_evening_un_606af4d5:


    me "大家都睡觉了。{w}而且没有人半夜去食堂！"


translate chinese day3_evening_un_3855e2ee:


    un "我们怎么进去啊？"


translate chinese day3_evening_un_6e8ce990:


    "我也应该事先想到这个问题的。"


translate chinese day3_evening_un_962242d8:


    me "这个..."


translate chinese day3_evening_un_e9f68551:


    "我确实有斯拉维娅的钥匙，但是我不想说出来。会引起误会的。"


translate chinese day3_evening_un_964defe1:


    "这个时候我开始后悔自己没有把手机从辅导员的房间里拿出来。"


translate chinese day3_evening_un_4ee977cd:


    "奥尔加·德米特里耶夫娜喋喋不休让我把这件事给忘了。"


translate chinese day3_evening_un_6f016ce7:


    "总之，我得说点什么引开话题。"


translate chinese day3_evening_un_7e3db1d5:


    me "呃...你喜欢什么类型的音乐？"


translate chinese day3_evening_un_9c66e26a:


    un "各种...{w}我对音乐不是很有兴趣。"


translate chinese day3_evening_un_f8be0238:


    me "咱们想象一下正在播放着音乐...{w}我是说你... 咱们听到音乐。"


translate chinese day3_evening_un_52f34a61:


    un "那是怎么样？"


translate chinese day3_evening_un_0c4b535c:


    me "就像是在你的大脑中播放音乐。"


translate chinese day3_evening_un_43e2549c:


    "我还记得少先队员们跳舞的那个音乐。{w}歌词和曲调我都还记得很清楚。"


translate chinese day3_evening_un_db88fd32:


    un "我不知道自己能不能行。"


translate chinese day3_evening_un_7b5327be:


    me "你只要试试就行了。"


translate chinese day3_evening_un_3c68ea6c:


    un "大概..."


translate chinese day3_evening_un_04cfc77c:


    "她同意了！"


translate chinese day3_evening_un_7ddfa955:


    "对于列娜来说“大概”就是“是”的意思了！"


translate chinese day3_evening_un_a20cefa7:


    "..."


translate chinese day3_evening_un_067ecc79:


    "接下来我们静静的筛选药品，写下名称和数量。"


translate chinese day3_evening_un_f7242642:


    "经历到如此的幸运之后我应该更加注意自己的言行。"


translate chinese day3_evening_un_3c9b2687:


    "虽然大多数时间我继续保持沉默。"


translate chinese day3_evening_un_72af65bb:


    "很快我们完成了最后一箱。"


translate chinese day3_evening_un_5b5ee5ef:


    "我把表格递给列娜然后看着她，就像我看到了大脚怪在骑独轮车的时候耍小猪。{w}令人惊讶，令人恐惧，令人着迷。"


translate chinese day3_evening_un_2342bdd6:


    "她突然笑了起来。"


translate chinese day3_evening_un_3d342d5a:


    me "什么？！"


translate chinese day3_evening_un_6f2b6d18:


    un "你的表情..."


translate chinese day3_evening_un_094d6b91:


    me "怎么啦？"


translate chinese day3_evening_un_d460b3a8:


    un "很有趣。"


translate chinese day3_evening_un_f90f07e5:


    me "真的...？"


translate chinese day3_evening_un_791682db:


    un "是的，那么我们应该去哪儿？"


translate chinese day3_evening_un_25f7abbc_1:


    me "哪儿？"


translate chinese day3_evening_un_530d77f3:


    "她的最后一句话让我困扰起来，我已经完全忘了这回事。"


translate chinese day3_evening_un_9ac6ce51:


    "说实话，已经有一段时间了。"


translate chinese day3_evening_un_3d8fbc00:


    un "就是说...{w}去跳舞。"


translate chinese day3_evening_un_48e08966:


    "列娜马上就脸红了，表情中充满了害羞和不安。"


translate chinese day3_evening_un_558e6a65:


    me "啊，对了！我差点忘了。{w}咱们去码头吧。"


translate chinese day3_evening_un_38967a03:


    "我不明白自己为什么选这个地方。"


translate chinese day3_evening_un_c980d140:


    "也许是因为在广场，在住宿区，在食堂都有可能撞到别人，但是码头是保险的。{w}至少我是这么想的。"


translate chinese day3_evening_un_cd9d64c3:


    "或者是夜色中水面上映着的一轮明月。"


translate chinese day3_evening_un_5626210f:


    "今天又是满月。"


translate chinese day3_evening_un_7a658aae:


    "我也不知道为什么，大脑中就突然闪现出了这个主意。"


translate chinese day3_evening_un_c8879bbd:


    me "如果你不喜欢的话，咱们可以..."


translate chinese day3_evening_un_ac6b93bc:


    un "不，那是个好地方。"


translate chinese day3_evening_un_0899349d:


    "列娜锁上了门，我们来到了码头。"


translate chinese day3_evening_un_2baf966e:


    "黑夜降临了熟睡的夏令营。"


translate chinese day3_evening_un_9380078a:


    un "我们应该沿着路走吗？"


translate chinese day3_evening_un_22b1f5c5:


    me "为什么？穿过森林快多了，有一条很好的小路。"


translate chinese day3_evening_un_2745ec45:


    un "会有..."


translate chinese day3_evening_un_c8d94649:


    me "谁？猫头鹰吗？"


translate chinese day3_evening_un_60bde9d8:


    "我笑了。"


translate chinese day3_evening_un_7b76c769:


    un "啊，你..."


translate chinese day3_evening_un_fa1386f8:


    "她的脸上出现了一点阴影。"


translate chinese day3_evening_un_b2a637c4:


    me "别在意。抱歉！"


translate chinese day3_evening_un_68893216:


    un "这里很暗。"


translate chinese day3_evening_un_413580ee:


    me "你害怕吗？"


translate chinese day3_evening_un_9c33c3aa:


    un "还好..."


translate chinese day3_evening_un_732bddc6:


    me "如果你想..."


translate chinese day3_evening_un_a798f094:


    un "好的...{w}只是我能...？"


translate chinese day3_evening_un_44ced226:


    "她没有说完，便拉起了我的胳膊。"


translate chinese day3_evening_un_17ef55d6:


    un "可以...吗？"


translate chinese day3_evening_un_006ab8be:


    "这次换我脸红了。"


translate chinese day3_evening_un_771aa502:


    me "当然！"


translate chinese day3_evening_un_c41c7779:


    "我们穿过丛林。"


translate chinese day3_evening_un_90e23667:


    "其实这说不上是森林。{w}更像是营地和码头之间的小树林。"


translate chinese day3_evening_un_8ba91e28:


    "大概有一百米。"


translate chinese day3_evening_un_e7d25333:


    "即使只有我自己也不会多害怕。"


translate chinese day3_evening_un_b12f537c:


    "可是列娜的恐惧感好像是有传染性的。"


translate chinese day3_evening_un_cd8be3f9:


    "枝叶在我们的四周摇动。{w}我有些发抖，列娜抱的更紧了。"


translate chinese day3_evening_un_4a492d8c:


    me "别害怕，肯定是松鼠什么的。"


translate chinese day3_evening_un_0905212f:


    un "嗯..."


translate chinese day3_evening_un_51f323e1:


    "我们来到了码头。{w}夜晚的景色真的很漂亮。"


translate chinese day3_evening_un_52acfaa6:


    "我走近河边，对列娜喊道"


translate chinese day3_evening_un_6bf1e9fc:


    me "看!"


translate chinese day3_evening_un_5732579f:


    "小码头和船坞倒映在河水的夜色中。"


translate chinese day3_evening_un_4de4a131:


    "睡眠就像是通向奇幻世界的大门。{w}好像你跳进去就可以从另外一侧出来一样。"


translate chinese day3_evening_un_983cba26:


    me "能不能请你和我跳舞呢？"


translate chinese day3_evening_un_cc87c2d0:


    "我笨拙的鞠着躬，向列娜伸出了手。"


translate chinese day3_evening_un_36d02627:


    "列娜犹豫了。{w}我的礼仪一定是表演的过于夸张了。"


translate chinese day3_evening_un_d77a4476:


    me "也没什么好担心的，反正我也跳的不好。"


translate chinese day3_evening_un_981f5474:


    th "为什么是“也”？{w}她并没有说自己不会跳舞，虽然看起来显然是这样的。"


translate chinese day3_evening_un_b993fafe:


    un "好的。"


translate chinese day3_evening_un_d32a46e3:


    "列娜向我伸出了手，我拉着她站好，把手臂轻轻放在她的腰间。"


translate chinese day3_evening_un_2c69e0e2:


    "我们就这样站了一会儿。"


translate chinese day3_evening_un_6cb44a45:


    un "然后呢？"


translate chinese day3_evening_un_bdc4a7dc:


    me "呃，我不知道...{w}你还记得那首歌吗？"


translate chinese day3_evening_un_38866932:


    un "基本上都忘掉了...{w}不过还有些印象。"


translate chinese day3_evening_un_cdeaa5d1:


    me "太棒了！咱们像电影里一样的跳华尔兹吧。"


translate chinese day3_evening_un_e0124613:


    un "那要怎么跳呢？"


translate chinese day3_evening_un_dd7ae836:


    "我没有回答，而是和她沿着弧线缓缓移动。"


translate chinese day3_evening_un_2d6366e8:


    me "你看，一点也不难呦~"


translate chinese day3_evening_un_a35af87c:


    un "嗯..."


translate chinese day3_evening_un_480cbce8:


    "我们就这样跳了几分钟圆舞。"


translate chinese day3_evening_un_0e0749ce:


    "或者是随便别的什么名字。"


translate chinese day3_evening_un_16ce780a:


    "我感觉到了她的体温，虽然我们并没有离得那么近。"


translate chinese day3_evening_un_3ff4b37b:


    "她的胸部不停起伏，脸蛋变得越来越红。"


translate chinese day3_evening_un_2c5d4171:


    "列娜没有看着我，她的视线不停地在四周游离。"


translate chinese day3_evening_un_fdcc8c70:


    "我突然发现自己从来没有过这样的感觉。"


translate chinese day3_evening_un_70d5f90d:


    "现实中突然有一种温存降临，我感觉自己到了一个更好的世界。"


translate chinese day3_evening_un_2d5d1563:


    "我发现自己不想放开这个女孩儿的手，愿意用任何事情换来和她永远的跳着华尔兹。"


translate chinese day3_evening_un_b3998185:


    "我稍稍用力保住了列娜，这时她才看着我的眼睛。"


translate chinese day3_evening_un_ef0ec8a2:


    "她的眼睛中充满了惊讶和疑惑，但是没有恐惧。"


translate chinese day3_evening_un_5c5e383c:


    "她并没有感到害怕，也没有推开我。"


translate chinese day3_evening_un_cc8b4535:


    un "你还说自己不会跳舞。"


translate chinese day3_evening_un_1ec032b9:


    me "我真的不会..."


translate chinese day3_evening_un_615f491b:


    "我有些不解。"


translate chinese day3_evening_un_20470c50:


    "我没有预料到她的这种反应。"


translate chinese day3_evening_un_9dc68533:


    "她的腼腆和恐惧去哪里了？"


translate chinese day3_evening_un_48c7652f:


    me "你也是...跳的很好..."


translate chinese day3_evening_un_466707e6:


    un "我知道！"


translate chinese day3_evening_un_f8ce941d:


    "她的脸上闪过调皮的笑容。"


translate chinese day3_evening_un_ea63114f:


    "也许只是我有这种感觉？"


translate chinese day3_evening_un_3e1fa0d9:


    "不会，我发誓我看到了。"


translate chinese day3_evening_un_160309c6:


    th "这怎么可能？"


translate chinese day3_evening_un_d8f01710:


    "害羞又内向的列娜和现在的场面可不协调啊。"


translate chinese day3_evening_un_d941a3d5:


    th "我应该说什么？{w}接下来怎么办？"


translate chinese day3_evening_un_93db6486:


    "我只能继续把舞跳下去，虽然这越来越奇怪了。"


translate chinese day3_evening_un_c81d9d23:


    mt "谢苗！谢苗，你在哪里？"


translate chinese day3_evening_un_438b28d5:


    "森林的尽头传来了呼喊声。"


translate chinese day3_evening_un_e5902723:


    th "糟糕！怎么选在这个时候！"


translate chinese day3_evening_un_105fc865:


    mt "谢苗!"


translate chinese day3_evening_un_980eb22f:


    "是奥尔加·德米特里耶夫娜。{w}她一定是好长时间没看见我担心了。"


translate chinese day3_evening_un_22027067:


    "我本来应该猜到的，那个精力过剩的辅导员发现丢了一个学生会怎么样。"


translate chinese day3_evening_un_d26f19fa:


    th "早知道应该提前告诉她的说。"


translate chinese day3_evening_un_f5e453bf:


    th "啊！现在可不是想这个的时候..."


translate chinese day3_evening_un_74229088:


    "列娜疑惑的看着我。"


translate chinese day3_evening_un_42de22f4:


    un "被发现我们在一起不好吧。"


translate chinese day3_evening_un_f14f0728:


    me "为什么？{w}咱们过去告诉她不用担心就行啦。"


translate chinese day3_evening_un_211c6e09:


    un "不。还是躲起来，一会儿再偷偷的回到营地去吧。"


translate chinese day3_evening_un_93c1d6e9:


    th "她的想法很奇怪。{w}一切都进展的很顺利..."


translate chinese day3_evening_un_5e1c6dc6:


    th "至少有什么事情开始发生了。{w}或者看起来是这样..."


translate chinese day3_evening_un_04f3f218:


    th "可是那个“什么”是什么？"


translate chinese day3_evening_un_c5b4a9e1:


    me "那好吧。"


translate chinese day3_evening_un_83831235:


    "我不想引起争论，在这种情况下那可不太合适。"


translate chinese day3_evening_un_e073d1d4:


    "奥尔加·德米特里耶夫娜喊了一会儿就离开了。"


translate chinese day3_evening_un_2308a96f:


    un "走吧。"


translate chinese day3_evening_un_9513cd87:


    me "嗯..."


translate chinese day3_evening_un_63803f24:


    "返程的时候列娜没有抓着我的手。"


translate chinese day3_evening_un_a604adb5:


    "我有点失落，但是又不敢迈出第一步。"


translate chinese day3_evening_un_31cc1eca:


    "我们又回到了沉默。引出一段对话不是列娜的风格，而我在经历了码头的事情之后又不知道该说什么好。"


translate chinese day3_evening_un_89f67cb6:


    "她一直看着脚下，保持着她的一贯表情。"


translate chinese day3_evening_un_985381f5:


    "好奇怪的变化！"


translate chinese day3_evening_un_46255706:


    "或者准确的说，刚刚列娜的微笑以及那一番话很奇怪！"


translate chinese day3_evening_un_a20cefa7_1:


    "..."


translate chinese day3_evening_un_4f71cd7e:


    "她在广场停了下来。"


translate chinese day3_evening_un_ccdf7460:


    un "嗯...我得走了。"


translate chinese day3_evening_un_9513cd87_1:


    me "嗯..."


translate chinese day3_evening_un_375d8a14:


    un "今天...谢谢你..."


translate chinese day3_evening_un_9513cd87_2:


    me "嗯..."


translate chinese day3_evening_un_26a8bbab:


    "列娜转身走向了自己的房间，我还站在原地。"


translate chinese day3_evening_un_59d28a2d:


    th "刚才到底是怎么回事啊？"


translate chinese day3_evening_un_8e35b95f:


    th "刚才的舞蹈，突然转变的心情，然后又突然恢复了常态。"


translate chinese day3_evening_un_a2d69018:


    "刚刚的瞬间好像我的怀里抱着的是另外一个列娜。"


translate chinese day3_evening_un_a5692cd4:


    "对！那不是她自己！那完全是另外一个人！"


translate chinese day3_evening_un_c579109c:


    th "也许是因为我还不够了解她？"


translate chinese day3_evening_un_92dafc98:


    "我之前就感觉列娜在她的害羞的面纱之下还有秘密。"


translate chinese day3_evening_un_79930909:


    "想要了解一个人，用两天可是远远不够的。"


translate chinese day3_evening_un_c154d46b:


    th "见鬼！怎么办？{w}也许都是我想象出来的！"


translate chinese day3_evening_un_a20cefa7_2:


    "..."


translate chinese day3_evening_un_f4822807:


    "我走向了辅导员的房间。"


translate chinese day3_evening_un_cec9d8ef:


    "整个营地都在休息了，所以没有人打扰我。"


translate chinese day3_evening_un_dd369d3a:


    th "那又怎么了？{w}一个少先队员在夜里就不能散散步了吗？"


translate chinese day3_evening_un_35589041:


    "当我快要走到门口的时候，我听见背后有点动静。"


translate chinese day3_evening_un_c6f6cc81:


    "灌木丛发出悉簌的声响好像有人从那里经过。"


translate chinese day3_evening_un_3bc47b84:


    th "这附近应该没有什么小动物。"


translate chinese day3_evening_un_2bdac6ec:


    th "有人在跟踪我！"


translate chinese day3_evening_un_8591c155:


    "就在这个瞬间我冲向了声音传来的方向跑进灌木丛一探究竟。"


translate chinese day3_evening_un_258661a5:


    "这里太暗了以至于我什么都看不到。"


translate chinese day3_evening_un_d49de170:


    "再往前走是没有意义的，如果有人跟踪我的话，他现在也应该已经走了。"


translate chinese day3_evening_un_f54ea053:


    "我回到了小屋。"


translate chinese day3_evening_un_76db5439:


    "里边的灯光亮着。"


translate chinese day3_evening_un_d336a541:


    th "她看起来好像还没睡。"


translate chinese day3_evening_un_37715fc9:


    mt "谢苗？！你去哪里了！"


translate chinese day3_evening_un_72311896:


    me "什么叫“哪里”？{w}我和列娜在医务室整理药品。我晚上就告诉你了啊。"


translate chinese day3_evening_un_d858aa01:


    mt "真的？那是什么时候？你可是够慢的啊！我半个小时以前去医务室，那里黑着灯，门已经锁了！"


translate chinese day3_evening_un_8125eef3:


    me "这个嘛...呃...我们想出去散散步..."


translate chinese day3_evening_un_a91b818e:


    mt "你出去散步的时候我可是很担心啊！"


translate chinese day3_evening_un_a81f88a1:


    me "唔...对不起...{w}下次我会提前告诉你。"


translate chinese day3_evening_un_140addaa:


    mt "说到做到哟！{w}去睡觉吧！时间不早了！"


translate chinese day3_evening_un_254cc408:


    "我非常同意她的话。"


translate chinese day3_evening_un_f2607f9a:


    th "怎么说今天过的可不容易啊。"


translate chinese day3_evening_un_89fdb957:


    "我累得没有精力回想晚上的事情，而且估计我也想不出什么正经事来..."


translate chinese day3_evening_un_95c84c3a:


    th "最关键的是我完全不知道该怎么处理这些事情。"


translate chinese day3_evening_un_d0119574:


    th "列娜，夏令营，这个世界。"


translate chinese day3_evening_un_d12034ba:


    "我几乎没有用一分钟的时间来思考这个问题的答案。{w}而且我在有意地避免它，好像这个答案会吓到我的感觉。"


translate chinese day3_evening_un_5db7cdc5:


    "还有列娜...{w}我和她在一起的时间比回到“真实世界”还要重要得多。"


translate chinese day3_evening_un_9e50ec2f:


    "我还可以用很长的时间来思考自己有什么事情做错了、什么事情忘了做，可是一阵困倦袭来..."


translate chinese day3_evening_us_8e722b79:


    "斯拉维娅离开了。"


translate chinese day3_evening_us_1a407541:


    "可能在她为我做了那么多之后我不应该拒绝她，可是这种活动真的不适合我。"


translate chinese day3_evening_us_324a06ef:


    "少先队员们继续跳着舞。"


translate chinese day3_evening_us_d21cca78:


    th "至少他们喜欢这个舞会。"


translate chinese day3_evening_us_ea24578a:


    "奥尔加·德米特里耶夫娜也在跳舞。"


translate chinese day3_evening_us_ea82e2f0:


    "我感觉这好像不太合适。{w}辅导员应该在附近维持秩序嘛。"


translate chinese day3_evening_us_78fb937a:


    th "而且她也不是17岁了。"


translate chinese day3_evening_us_f2be2b10:


    "她走向了我，好像感觉到自己的水平被质疑了一样。"


translate chinese day3_evening_us_0f6bc7b5:


    mt "你为什么不跳舞？"


translate chinese day3_evening_us_8717f26e:


    me "我不是太想跳..."


translate chinese day3_evening_us_b0e280b5:


    mt "你自己决定。"


translate chinese day3_evening_us_945c59bc:


    "她狡猾的笑着。"


translate chinese day3_evening_us_31a7a515:


    mt "那我给你准备了一个绝妙的任务！"


translate chinese day3_evening_us_e473fa2f:


    me "一个任务？"


translate chinese day3_evening_us_a1f730b2:


    "什么任务都比跳舞要好。"


translate chinese day3_evening_us_bc92d844:


    mt "你今天你在食堂做的扫除不错，但是...{w}我觉得这还不够。"


translate chinese day3_evening_us_8941bd9d:


    me "诶？？"


translate chinese day3_evening_us_8403b986:


    mt "乌里扬娜!"


translate chinese day3_evening_us_123f5418:


    us "什么?!"


translate chinese day3_evening_us_7f1e0dc0:


    mt "过来！"


translate chinese day3_evening_us_86cbd5c6:


    "乌里扬卡不情愿的走了过来。"


translate chinese day3_evening_us_1e9714fe:


    mt "我看你跳舞跳得已经够多了吧。"


translate chinese day3_evening_us_6c304510:


    us "不，我还没有！"


translate chinese day3_evening_us_eb9a46a9:


    "看她汗流浃背的样子辅导员说的没错。"


translate chinese day3_evening_us_9a131f0d:


    mt "我给你和谢苗一个任务。"


translate chinese day3_evening_us_4b8e25e2:


    us "可是奥尔加·德米特里耶夫娜...！"


translate chinese day3_evening_us_4aca71e6:


    "乌里扬娜恳求着。"


translate chinese day3_evening_us_c588b18c:


    us "现在是跳舞时间！而且已经很晚了..."


translate chinese day3_evening_us_63ea0fa5:


    mt "用不了太长时间。{w}斯拉维娅正在图书室整理图书，但是还没有干完..."


translate chinese day3_evening_us_c4a407cc:


    mt "只给你们剩下几个暑假了。"


translate chinese day3_evening_us_e566dcaf:


    us "可是..."


translate chinese day3_evening_us_c2f28fc6:


    mt "没有可是！！"


translate chinese day3_evening_us_888f7373:


    th "看在上帝的份上..."


translate chinese day3_evening_us_8b0f570d:


    "我不喜欢做义工，可是跳舞..."


translate chinese day3_evening_us_7f594afe:


    me "我准备好了。"


translate chinese day3_evening_us_d0e475bd:


    mt "不错，谢苗！这才是我的好孩子！一个真正的少先队员！你应该向他学习，乌里扬娜！"


translate chinese day3_evening_us_8378599b:


    "乌里扬卡可是不喜欢我这个榜样。"


translate chinese day3_evening_us_88adb859:


    mt "赶紧去！{w}人民等着你的好消息！"


translate chinese day3_evening_us_d172a747:


    us "咱们走着瞧..."


translate chinese day3_evening_us_86a7768d:


    "她咬着牙说道。"


translate chinese day3_evening_us_7cf397bf:


    "晚上的夏令营很漂亮。{w}寂静和安详让我的感到平和。"


translate chinese day3_evening_us_b69e6c0f:


    "让我明白这里还有其他人的只有远远的广场的歌声。"


translate chinese day3_evening_us_38d5d35e:


    "还有刚刚回来的乌里扬娜。"


translate chinese day3_evening_us_79dbaa1d:


    me "你还可以给自己的裙子做一下扫除。"


translate chinese day3_evening_us_a026df63:


    "我笑话她。"


translate chinese day3_evening_us_fb48ad0c:


    us "这都是你的错！"


translate chinese day3_evening_us_74af0838:


    "她重重的喘着气，脸像西红柿一样红，我估计接下来她的耳朵里要喷出蒸汽来了。"


translate chinese day3_evening_us_0ab904a2:


    us "你！这全都是！{w}你的！错！"


translate chinese day3_evening_us_6a9fa49b:


    me "怎么又是我？"


translate chinese day3_evening_us_0f170283:


    us "你怎么不知道闭嘴？"


translate chinese day3_evening_us_21221d76:


    me "那能有什么变化呢？{w}反正她都会让咱们干的。"


translate chinese day3_evening_us_5a606f08:


    us "你..."


translate chinese day3_evening_us_a2d9c3e3:


    "她没有说出话来，只是咬牙切齿。"


translate chinese day3_evening_us_5bbc2e7c:


    me "如果我保持沉默呢？{w}你觉得她会让你继续开心的玩吗？"


translate chinese day3_evening_us_c1625886:


    "乌里扬卡看着我。"


translate chinese day3_evening_us_49ae3aab:


    "她看起来冷静了一些。"


translate chinese day3_evening_us_8fa6a151:


    us "当然！反正你不在乎！{w}你都不会跳舞！"


translate chinese day3_evening_us_9574dd57:


    me "那又怎么样？"


translate chinese day3_evening_us_05b51e5d:


    us "你要是和我跳舞的话...{w}本来可以很有意思的！"


translate chinese day3_evening_us_f51acc4b:


    "她又恢复了“孩子气”模式。"


translate chinese day3_evening_us_b3acf935:


    me "也许吧...{w}不过你也知道事情的结果..."


translate chinese day3_evening_us_1d9ef291:


    "我们没有机会较量一下跳舞的技巧。"


translate chinese day3_evening_us_90a2bbc6:


    "我们来到了图书室，外面已经黑了。"


translate chinese day3_evening_us_f49d96cb:


    "我对奥尔加·德米特里耶夫娜的这个请求有点吃惊...{w}或者应该叫做命令...{w}在半夜整理图书。"


translate chinese day3_evening_us_bc244cfb:


    th "这太奇怪了。"


translate chinese day3_evening_us_a7e2aa20:


    "里面黑漆漆的，开关是坏的，按来按去也只能听到“啪啪”的开关声音。"


translate chinese day3_evening_us_d3f5e62a:


    "也许开关没有问题就是灯不亮。"


translate chinese day3_evening_us_f09b37ef:


    us "等一下，我去找蜡烛。"


translate chinese day3_evening_us_681ab9c1:


    "我正想问问她是怎么知道哪里有蜡烛火柴什么的时候，旁边的桌子上多了亮点亮光。"


translate chinese day3_evening_us_7827470a:


    us "这下好多了！"


translate chinese day3_evening_us_574bbbd5:


    "乌里扬娜对自己很满意的说道。"


translate chinese day3_evening_us_3f38f9a0:


    me "接下来呢？"


translate chinese day3_evening_us_fbf7b967:


    us "什么接下来？"


translate chinese day3_evening_us_c349ed5f:


    "她的脸上充满了问号。"


translate chinese day3_evening_us_050fe076:


    me "我们接下来该做什么？"


translate chinese day3_evening_us_8232daec:


    us "噗...我怎么知道？"


translate chinese day3_evening_us_393056bc:


    "她笑了起来。"


translate chinese day3_evening_us_b57abe01:


    me "真好。"


translate chinese day3_evening_us_6046a48f:


    "我绕着书架走来走去。"


translate chinese day3_evening_us_a5101d38:


    th "奥尔加·德米特里耶夫娜说过还有几个书架..."


translate chinese day3_evening_us_0e2a5ebd:


    "我摸了摸这些书确定上面没有尘土。"


translate chinese day3_evening_us_f637ef9a:


    "看起来有人已经提前把工作完成了。"


translate chinese day3_evening_us_d054667b:


    "我把整个图书室检查里一遍，没有发现需要清理的东西。{w}辅导员同志一定是弄错了。"


translate chinese day3_evening_us_eb36cc9f:


    "突然我听到背后的脚步声。"


translate chinese day3_evening_us_cca19138:


    "老旧的地板一直在咯吱咯吱的响，所以乌里扬娜应该没有机会悄悄接近。"


translate chinese day3_evening_us_08ef6f67:


    us "Boo!"


translate chinese day3_evening_us_468f92d5:


    "我转过身。"


translate chinese day3_evening_us_9e09f5fa:


    me "天啊。吓死我了，拜托..."


translate chinese day3_evening_us_80eb9adc:


    us "哈...关我什么事。"


translate chinese day3_evening_us_e2993c31:


    "她悻悻地转了过去。"


translate chinese day3_evening_us_4ab03e66:


    me "这里看起来很干净，所以..."


translate chinese day3_evening_us_c1661c00:


    "灰暗的光影下一行行旧书在怨愤的看着我。"


translate chinese day3_evening_us_4c1d899a:


    "被遗忘的年代的不知名的作者们..."


translate chinese day3_evening_us_51f94d8f:


    th "我很好奇有没有人会记住他们？{w}热妮娅一定会记得的。"


translate chinese day3_evening_us_b78d5ef3:


    "我确定她什么都会记得的。"


translate chinese day3_evening_us_fce45449:


    us "好了坐下。"


translate chinese day3_evening_us_dc9d3ef6:


    "乌里扬娜递给我一把椅子，{w}我坐了下来。"


translate chinese day3_evening_us_fecfa4ed:


    me "所以说，计划是什么？"


translate chinese day3_evening_us_5d9c94b9:


    us "咱们互相讲故事。{w}咱们有两把椅子，两支蜡烛，所以应该是两个故事。我一个你一个。"


translate chinese day3_evening_us_cd917d11:


    me "好吧。"


translate chinese day3_evening_us_0e57cf1c:


    "我想都没想就同意了。"


translate chinese day3_evening_us_c76d5efb:


    "我一点也不想回去跳舞，而且根据我的经验夏令营的晚上什么没什么事可干。"


translate chinese day3_evening_us_1da6e1e4:


    "而且我有几个肯定会吓到乌里扬娜的鬼故事。"


translate chinese day3_evening_us_a38cf680:


    "那可是个好主意。"


translate chinese day3_evening_us_959cb1f4:


    me "你先来。"


translate chinese day3_evening_us_1549669b:


    us "好啊。"


translate chinese day3_evening_us_906544fe:


    "她找了个舒适的坐姿，胳膊抱着椅背，把蜡烛贴在脸跟前。"


translate chinese day3_evening_us_517ac6ae:



    nvl clear
    "{i}很久以前，在一个偏僻的小村庄，有一个小男孩，只是一个普通的小男孩。他去上学，和孩子们玩耍。他没有什么特别的。{/i}"


translate chinese day3_evening_us_e153dd08:


    "{i}有一天他和朋友打赌说自己走进一座荒废的小屋时不会害怕...{/i}"


translate chinese day3_evening_us_82b72ef3:


    "乌里扬娜停顿了一会儿。"


translate chinese day3_evening_us_4d3eecb3:


    me "所以说那个房子里有什么？"


translate chinese day3_evening_us_cd35d04d:


    us "不要打断我！"


translate chinese day3_evening_us_4d2c229a:


    me "继续编啊~"


translate chinese day3_evening_us_39cf5c52:


    "她撅撅嘴继续说道。"


translate chinese day3_evening_us_f5578357:


    "{i}他们说那个房子里住着一个老巫婆，晚上的时候人们会看到鬼魂。没有人敢肯定，但是大家都很害怕。{/i}"


translate chinese day3_evening_us_f3da10f9:


    "{i}男孩不相信，并且声称自己会在房子里住一晚上。他也是这么做的，但是他第二天早上没有回来。{/i}"


translate chinese day3_evening_us_31aee4ac:


    "{i}后来人们发现他上吊了。{/i}"


translate chinese day3_evening_us_eb137a30:



    nvl clear
    "乌里扬娜又停了下来。"


translate chinese day3_evening_us_fd837679:


    me "真是惊心动魄啊...{w}他是不是用鞋带上吊的啊？"


translate chinese day3_evening_us_abfdf469:


    "我质疑道。"


translate chinese day3_evening_us_306e9012:


    "她又皱起了眉头。"


translate chinese day3_evening_us_6a86fe31:


    me "结束了没？"


translate chinese day3_evening_us_c25bebac:


    us "当然没有啦！"


translate chinese day3_evening_us_81736160:


    "{i}他们埋葬了男孩，就像平常一样，棺材啊什么的。{/i}"


translate chinese day3_evening_us_387cb49c:


    "{i}她的家人和朋友很悲伤，但是却不能让他死而复生。{/i}"


translate chinese day3_evening_us_4498a264:


    "{i}过了几天村子里的人们不断的失踪。没有人知道为什么。他们只是消失到了空气中。{/i}"


translate chinese day3_evening_us_2b80bb65:


    "{i}村子里的人们想要叫警察，不过那个男孩的一个朋友说他见过那个男孩。开始大家不相信他，但是人们还是不断的失踪。{/i}"


translate chinese day3_evening_us_0b0a71f5:



    nvl clear
    "{i}然后大家准备把坟墓挖开，盖子的内侧有挠过的痕迹，但是却没有尸体。{/i}"


translate chinese day3_evening_us_c6ee723c:


    "{i}他去哪儿了？{/i}"


translate chinese day3_evening_us_afd82e5f:


    "乌里扬娜把蜡烛拿开，做了一个吓人的鬼脸。(至少她自己这么觉得。)"


translate chinese day3_evening_us_725f29b0:


    "{i}最后村子里所有的人都消失了，偶尔有人经过巫婆的鬼屋后会说他们看到了两个鬼魂。{/i}"


translate chinese day3_evening_us_76b2fe88:


    nvl clear


translate chinese day3_evening_us_c65a5a14:


    "乌里扬娜吹灭了蜡烛，没有再说话。"


translate chinese day3_evening_us_56417cdb:


    "看起来这个故事结束了。"


translate chinese day3_evening_us_d1bfd0cf:


    me "好诶！"


translate chinese day3_evening_us_bfb0f205:


    "我鼓掌表扬。"


translate chinese day3_evening_us_5bdb65dc:


    me "还达不到普利策奖的高度，不过..."


translate chinese day3_evening_us_d6c3ca28:


    us "你害怕了吗？{w}惊慌了吗？"


translate chinese day3_evening_us_6c2b1b7e:


    me "是啊~啊~啊~我全身都在发抖..."


translate chinese day3_evening_us_f4067405:


    us "算了随便你...{w}你讲讲自己的故事吧。{w}我保证自己不可能退缩！"


translate chinese day3_evening_us_59133844:



    nvl clear
    "我使劲回想，最后决定讲一个几个月之前听博客上一个熟人讲的故事。"


translate chinese day3_evening_us_0864a00e:


    "他很擅长讲故事，至少我喜欢他的风格，看来我胜券在握了。"


translate chinese day3_evening_us_cf924028:


    "我当然没有一字不漏的记住，所以便用自己的语言转述。"


translate chinese day3_evening_us_56bad404:


    "{i}有一个遥远的空间站，在人类文明的边界，紧邻另一个敌对的外星文明。现在是停火的第三个月。{/i}"


translate chinese day3_evening_us_0fe7b509:


    "{i}虽然这很难被叫做战争，在长时间的围困之中仅有少数幸存者。{/i}"


translate chinese day3_evening_us_6dc81529:


    "{i}一方面他们不能扔下据点撤退。另一方面他们知道如果敌人真的进攻的话自己一分钟也活不了。{/i}"


translate chinese day3_evening_us_04d2b110:



    nvl clear
    "{i}绝望这个词用在这里再合适不过了。{/i}"


translate chinese day3_evening_us_68e6fa98:


    "{i}食物已经耗尽，弹药也只够打几发礼炮迎接敌人了。{/i}"


translate chinese day3_evening_us_0f0fc437:


    "{i}只剩下循环系统所提供的空气和水资源还算充足。{/i}"


translate chinese day3_evening_us_420be6e0:


    "{i}这里的人们几个星期也不互相交谈了。可能他们觉得死到临头的时候谈话已经没有什么意义了。{/i}"


translate chinese day3_evening_us_b4d08157:


    "{i}虽然准确的说还有数米厚的装甲保护着他们。{/i}"


translate chinese day3_evening_us_f910168c:


    "{i}这时不如等待救援或是等待敌人的进攻。无论是哪一种结局都可以结束当前的痛苦。{/i}"


translate chinese day3_evening_us_9e98cab6:



    nvl clear
    "{i}现在的处境就像是和一个看不见的对手下棋。现在是最关键的时候，任何一步错棋都会导致毁灭——对敌人或是自己。{/i}"


translate chinese day3_evening_us_6880febd:


    "{i}但是敌人知道吗？他们可能也害怕走错这一步棋。人类可以移动国王，但是也只能前后反复移动。{/i}"


translate chinese day3_evening_us_38c79fcf:


    "{i}任何其他的动作都会导致死无葬身之地。向前一步是进攻，向后一步是防御。如果对手坐在眼前则要容易得多即使是象棋大师也会从眼睛中透露出自己的紧张。{/i}"


translate chinese day3_evening_us_22f0a385:


    "{i}另一方面来说一切都是没有意义的，大家都知道没救了，没有人愿意再做些什么。{/i}"


translate chinese day3_evening_us_35856953:



    nvl clear
    "{i}而当对手坐在面前的时候你会知道他和你自己一样也会犯错误，他也会感到困难。{/i}"


translate chinese day3_evening_us_4973842c:


    "{i}这就是宇宙边缘的这些男女的处境。总部已经好几天没有继续下达命令了。{/i}"


translate chinese day3_evening_us_7c9be39f:


    "{i}虽然最后一条命令没有任何有用的信息，除了让和平常一样的守住据点。{/i}"


translate chinese day3_evening_us_f87c860d:


    "{i}这里的进进退退就像是一个永恒的钟摆。{/i}"


translate chinese day3_evening_us_f34c1a35:


    "{i}在第四个月的开始，指挥官决定撤退。{/i}"


translate chinese day3_evening_us_f2ccbac0:


    "{i}因为他觉得比起形而上的人类理想，自己士兵的生命要更重要。没有人予以评论。{/i}"


translate chinese day3_evening_us_f5762547:



    nvl clear
    "{i}他们也改变不了什么。{/i}"


translate chinese day3_evening_us_0117105e:


    "{i}准备工作很快完成。你可以从一个正在沉没的船上拯救的最重要的财富就是自己的生命。{/i}"


translate chinese day3_evening_us_ce3c3dd4:


    "{i}整个基地的队员都登上了救援飞船。发射序列开始，3,2,1...什么也没有发生。舱门无法打开。{/i}"


translate chinese day3_evening_us_ec3c9d93:


    "{i}他们派工程师去检查故障，但是却一无所获。舱门本应打开，可是第二次依旧失败，第三次也是...{/i}"


translate chinese day3_evening_us_e838b9cb:


    "{i}指挥官命令队员们从脱离舱逃离。可是仍然无法发射，这肯定是技术故障，队员们想要和总部联系却仍旧是徒劳。{/i}"


translate chinese day3_evening_us_a22d8215:



    nvl clear
    "{i}过了几天队长发现雷达上什么也没有。没有行星，没有卫星，没有敌人的飞船，只有无尽的黑暗。{/i}"


translate chinese day3_evening_us_44deaf56:


    "{i}他们一个月内就耗尽了食物。大家都认为已经完了。可是没有人死去，过了一天，一个星期，一个月...{/i}"


translate chinese day3_evening_us_6d909058:


    "{i}好像人类不再需要食物一样。{/i}"


translate chinese day3_evening_us_fe89102b:


    "{i}这是大部分的队员都发疯了。有些人呆在自己的房间里祈祷，有些人来回踱步，有些人想要自杀。{/i}"


translate chinese day3_evening_us_7ed3bc1a:


    "{i}但是用电浆手枪对自己直射，或是跳进水箱，或者直接割腕自杀都没有任何结果。{/i}"


translate chinese day3_evening_us_405821aa:



    nvl clear
    "{i}日子一天天，一年年的过去，没有人还记得时间，发疯的人们恢复了心智，正常的人们又开始发疯，这不断反复。{/i}"


translate chinese day3_evening_us_a8f61a7d:


    "{i}终于，队员们接受了这个事实。{/i}"


translate chinese day3_evening_us_f0789661:


    "{i}他们开始尝试不同的职业：喜剧表演，体育比赛，自己写书自己看...{/i}"


translate chinese day3_evening_us_520553c5:


    "{i}他们组成家庭，又一次次分手。{/i}"


translate chinese day3_evening_us_df10e82a:


    "{i}时间无限的流逝，命运一次次循环。空空如也的雷达告诉他们基地之外只有无尽的空虚，可是基地之内也只有空虚。{/i}"


translate chinese day3_evening_us_dd21be31:


    "{i}最后这些人放弃了生活，只是躺在地上睡觉。开始不是整天整夜的睡觉，但是逐渐的他们学会了让自己昏迷。{/i}"


translate chinese day3_evening_us_134f2ab0:



    nvl clear
    "{i}每个人都有自己的梦，有人回到小时候，有人找到自己曾经失去的挚爱，有的人用手中的炸药捍卫了人类的尊严，有些人只是宇宙中毫无目的的游荡...{/i}"


translate chinese day3_evening_us_d9aa3d87:


    "{i}一艘救生船经过和敌人的几场战斗后降落在了这个基地。登陆小队进入了基地。{/i}"


translate chinese day3_evening_us_0bf36ead:


    "{i}整个基地像是在宇宙空间中漂浮了上千年的破铜烂铁，很久以前反应器就已经停止工作了，墙壁上充满了激光打穿的孔，所有的设备都被毁了。{/i}"


translate chinese day3_evening_us_6b174e6c:


    "{i}他们在各个房间里找到拼死守卫人类前线的战士们的腐烂的骨架。{/i}"


translate chinese day3_evening_us_ab489ab3:


    "{i}只有一条向总部发出的神秘信息暗示了似乎有什么事情不太对...{/i}"


translate chinese day3_evening_us_43b9f5d0:



    nvl clear
    "{i}“救我们！{/i}"


translate chinese day3_evening_us_93ed5913:


    "{i}我们只是想死...”{/i}"


translate chinese day3_evening_us_6b713cd4:


    "我讲完了故事，看了看乌里扬娜。"


translate chinese day3_evening_us_2c2388cc:


    "她的脸隐藏在黑暗中。"


translate chinese day3_evening_us_768354f5:


    me "喏，故事讲完了..."


translate chinese day3_evening_us_e7066533:


    "我熄灭了烛光。"


translate chinese day3_evening_us_3d0f0f54:


    "乌里扬娜突然发出尖叫，扑到了我身上。"


translate chinese day3_evening_us_574fb6aa:


    "我们摔到了地上。"


translate chinese day3_evening_us_a4d2b1c8:


    me "你怎么啦？"


translate chinese day3_evening_us_3005a190:


    us "没什么..."


translate chinese day3_evening_us_3cff4607:


    "她的声音听起来很害怕。"


translate chinese day3_evening_us_871c17c7:


    "看来我的故事中了头奖。"


translate chinese day3_evening_us_c163059f:


    "我精神上鼓励了一下自己。"


translate chinese day3_evening_us_4c250e85:


    "乌里扬卡就没有这么高兴了。"


translate chinese day3_evening_us_355d45cd:


    "她抱着我抽泣着。"


translate chinese day3_evening_us_638d6acb:


    me "你怎么啦，傻瓜？"


translate chinese day3_evening_us_0192e2f2:


    "我拍了拍她的头。"


translate chinese day3_evening_us_6dc624b2:


    me "只是一个故事。{w}不是真的。"


translate chinese day3_evening_us_24578402:


    us "你还有你那个讨厌的故事！"


translate chinese day3_evening_us_00a07cdc:


    "乌里扬娜抱的更紧了。"


translate chinese day3_evening_us_14314e51:


    me "你那么害怕吗？"


translate chinese day3_evening_us_c463eeea:


    us "嗯..."


translate chinese day3_evening_us_d47079d5:


    "说实话我没有预料到她会说的这么直接。"


translate chinese day3_evening_us_0268fff6:


    me "别害怕，什么事都没有。"


translate chinese day3_evening_us_f776f380:


    th "时间会抹平一切伤口，她会冷静下来的。"


translate chinese day3_evening_us_3e0531a1:


    us "听着，谢苗..."


translate chinese day3_evening_us_67c5f2d1:


    me "什么?"


translate chinese day3_evening_us_62db7763:


    us "算了，没什么。"


translate chinese day3_evening_us_dc1b0573:


    "她把脸埋进我的胸口。"


translate chinese day3_evening_us_f8253995:


    "过了好几分钟。"


translate chinese day3_evening_us_a92c5503:


    me "好啦，我明白了...咱们是不是该走了？"


translate chinese day3_evening_us_0cd20c05:


    "我听见{w}乌里扬卡轻轻的呼噜声。"


translate chinese day3_evening_us_61d4bcc5:


    me "嘿！醒醒！"


translate chinese day3_evening_us_094ae4ba:


    th "害怕还能睡着啊？"


translate chinese day3_evening_us_ba0cb0ae:


    me "醒醒啊，我说！"


translate chinese day3_evening_us_71063c36:


    "没有反应。"


translate chinese day3_evening_us_c589ce7a:


    "我试着站起来。"


translate chinese day3_evening_us_20833c6d:


    "乌里扬娜肯定到不了40公斤，不过被这样的分量压在下面..."


translate chinese day3_evening_us_81eadbc5:


    "想站起来真的不容易！"


translate chinese day3_evening_us_2f516faf:


    "如果不是正在呼吸的话你可以认为她已经死了。"


translate chinese day3_evening_us_19979d09:


    "我肯定可以再用力一些..."


translate chinese day3_evening_us_ceb2ee9d:


    th "不过那样她就会醒过来然后又重新开始！"


translate chinese day3_evening_us_c52a9778:


    th "好为难的处境。"


translate chinese day3_evening_us_feb9b84c:


    th "不过还有一个等着她自己醒过来的选项。"


translate chinese day3_evening_us_96e8f382:


    th "听完这样一个故事之后她肯定不会一个好觉睡到天亮吧。"


translate chinese day3_evening_us_dd9bfa6a:


    "我看了看窗外的满天星辰。"


translate chinese day3_evening_us_44c11d50:


    th "我开始想会不会在遥远的地方真的有一个闹鬼的据点..."


translate chinese day3_evening_us_8ac1f25d:


    "我慢慢的闭上了眼睛，过了一会儿我就睡着了..."


translate chinese day3_evening_dv_701cf26a:


    "过了几分钟我走近舞台。"


translate chinese day3_evening_dv_50cb2f7e:


    th "也许接受她的邀请是个坏主意？"


translate chinese day3_evening_dv_13334dce:


    th "光是和朵切芙斯卡娅就很危险..."


translate chinese day3_evening_dv_bdfb9d61:


    th "不过我也不想去舞会就是了。"


translate chinese day3_evening_dv_b864db36:


    th "我在那里能干什么呢？{w}只会让自己出丑..."


translate chinese day3_evening_dv_5815ef70:


    th "不，还是听听阿丽夏弹吉他比较好。"


translate chinese day3_evening_dv_1340350c:


    "走近一点我看到一个人摇晃着双腿坐在舞台边缘。"


translate chinese day3_evening_dv_5ee37cbd:


    dv "所以说你还是来啦。"


translate chinese day3_evening_dv_171149e1:


    "阿丽夏把吉他放在一边跳了下来。"


translate chinese day3_evening_dv_7c8a24e7:


    "她的脸上甚至有一丝欣喜。{w}为另一个人表演总是要比独自练习有意思。"


translate chinese day3_evening_dv_2abbfc44:


    me "如你所见..."


translate chinese day3_evening_dv_64374587:


    dv "我们要干什么呢？"


translate chinese day3_evening_dv_6c4f856c:


    me "什么“什么”？"


translate chinese day3_evening_dv_554549ca:


    dv "我在问你。"


translate chinese day3_evening_dv_c222ae63:


    me "嗯...你想..."


translate chinese day3_evening_dv_e1d0c3f8:


    "我突然感觉这是她的另一个恶作剧，我可能根本听不到她的吉他表演..."


translate chinese day3_evening_dv_c87e1fd0:


    me "我要去睡觉了，拜拜！"


translate chinese day3_evening_dv_4f545b83:


    "我转过身故意的挥挥手。"


translate chinese day3_evening_dv_93b5fc50:


    dv "嘿!"


translate chinese day3_evening_dv_744f6c22:


    "阿丽夏拉住了我的袖子。"


translate chinese day3_evening_dv_ad02f53a:


    me "什么?"


translate chinese day3_evening_dv_18f4b1ad:


    dv "你不想听吗？"


translate chinese day3_evening_dv_ba6356e6:


    me "听什么？你自己都不知道该干什么好。"


translate chinese day3_evening_dv_3ce16a8b:


    dv "我知道啊！"


translate chinese day3_evening_dv_55df167e:


    "她拿起吉他用拨片弹了几下琴弦。"


translate chinese day3_evening_dv_a2494e1f:


    dv "坐在我旁边。"


translate chinese day3_evening_dv_eaadd141:


    "我没有拒绝。"


translate chinese day3_evening_dv_55b9978e:


    th "这样最好。"


translate chinese day3_evening_dv_69e988e0:


    th "现在回去也没什么事要干。"


translate chinese day3_evening_dv_994a7e72:


    "虽然我很想积极的寻找答案，不过作为一个陌生人我只能消极的观察。"


translate chinese day3_evening_dv_6a5d85fb:


    dv "好了，我给你演示一下这首歌怎么弹。"


translate chinese day3_evening_dv_9fb4d40d:


    me "就这样？现在就演示？"


translate chinese day3_evening_dv_7f046cee:


    dv "什么意思嘛？"


translate chinese day3_evening_dv_3eb0fb19:


    me "我已经很久没有摸过吉他了..."


translate chinese day3_evening_dv_22bda204:


    th "她大概能猜到我多长时间没有碰过了，我的技术可以让一只猫笑起来。"


translate chinese day3_evening_dv_87662605:


    dv "这一点也不难！"


translate chinese day3_evening_dv_db47df41:


    "我认真的看着阿丽夏。"


translate chinese day3_evening_dv_2f5dd571:


    "我明显的感觉到她不擅长表达感情。"


translate chinese day3_evening_dv_f464d10c:


    "有几个瞬间我能看到一个普通甚至是友好有爱的女孩。"


translate chinese day3_evening_dv_05afbea0:


    "然后等她又找到了自信，就会变回母老虎。"


translate chinese day3_evening_dv_0d77d9e0:


    th "到底哪个是真正的她呢？"


translate chinese day3_evening_dv_93b5fc50_1:


    dv "嘿!"


translate chinese day3_evening_dv_ad02f53a_1:


    me "什么?"


translate chinese day3_evening_dv_265b67ba:


    dv "别做白日梦了！"


translate chinese day3_evening_dv_94598a92:


    me "啊，喔...抱歉..."


translate chinese day3_evening_dv_a38c32a6:


    dv "所以说，你要看看吗？"


translate chinese day3_evening_dv_dc57eba2:


    me "嗯，是什么歌呢？"


translate chinese day3_evening_dv_4d022e04:


    dv "当然是我早晨弹过的那个！"


translate chinese day3_evening_dv_46559df1:


    me "哇...好吧，我听着呢。"


translate chinese day3_evening_dv_13b39fa1:


    "阿丽夏准备开始弹奏。"


translate chinese day3_evening_dv_ce617998:


    "..."


translate chinese day3_evening_dv_56072db3:


    dv "就是这样了。{w}你都记住了吗？"


translate chinese day3_evening_dv_2a7d102c:


    th "我可能大概懂了，不过想要复现出来就不太可能了。"


translate chinese day3_evening_dv_51584317:


    me "我该怎么说..."


translate chinese day3_evening_dv_52e5d545:


    dv "没关系，熟能生巧。"


translate chinese day3_evening_dv_8f9b4924:


    "她说的没错，不过从小到大练习什么的就做不好。"


translate chinese day3_evening_dv_91dd4d67:


    me "这个..."


translate chinese day3_evening_dv_77bdacb3:


    "我拿起吉他试着弹奏第一个和弦。"


translate chinese day3_evening_dv_59bff6c6:


    "这有点难，我的手指打了结，不怎么听使唤。"


translate chinese day3_evening_dv_008caeac:


    "我原来弹得还可以，不过现在连“蚂蚱”都弹不出来了。"


translate chinese day3_evening_dv_dbeba143:


    dv "嗯?"


translate chinese day3_evening_dv_2074dc26:


    me "等一下..."


translate chinese day3_evening_dv_4145963f:


    dv "看来你不会成为一个摇滚巨星啦。"


translate chinese day3_evening_dv_91623ab5:


    "她善意的笑了笑。"


translate chinese day3_evening_dv_4608b651:


    me "可能这样更好..."


translate chinese day3_evening_dv_f6a527e5:


    "我徒劳的回忆着曲谱。"


translate chinese day3_evening_dv_4366e696:


    me "好吧..."


translate chinese day3_evening_dv_54c3617f:


    "吉他开始发出残忍的声音就像是受伤的恐龙的呻吟。"


translate chinese day3_evening_dv_a4415adc:


    "我的左手不断地偏出右品，右手也不在节奏上。"


translate chinese day3_evening_dv_7c983f00:


    me "这应该算是新式摇滚。"


translate chinese day3_evening_dv_334edc91:


    dv "新式？啊~啊~快给我！"


translate chinese day3_evening_dv_13705a4a:


    "她拿起吉他，琴弦在她的手里发出了完全不同的声音。"


translate chinese day3_evening_dv_11e95cca:


    "我尴尬了很长时间。"


translate chinese day3_evening_dv_b5823702:


    "阿丽夏的技术可能达不到专业水平，不过她在弹这些简单的曲子时很有自信。"


translate chinese day3_evening_dv_195bfa48:


    "她一定练习了很长时间。"


translate chinese day3_evening_dv_561d4770:


    dv "这次明白了吗？"


translate chinese day3_evening_dv_d9ffec9a:


    "她弹完又把吉他递给了我。"


translate chinese day3_evening_dv_c97fadc0:


    me "呃..."


translate chinese day3_evening_dv_e9ad5daa:


    "我这次弹得好一些，不过离阿丽夏的水平还差的很远。"


translate chinese day3_evening_dv_9422d1b3:


    "离各种说的过去的水平都差得远。"


translate chinese day3_evening_dv_7c284714:


    "我明白了自己和她比起来一文不值。"


translate chinese day3_evening_dv_c3b32ce7:


    th "只是弹简单的三和弦！"


translate chinese day3_evening_dv_34a8a8e1:


    dv "虽然这个曲子太简单..."


translate chinese day3_evening_dv_ee31e548:


    "我把吉他放在了一边。"


translate chinese day3_evening_dv_ed1bcf7d:


    "阿丽夏陷入了沉思。"


translate chinese day3_evening_dv_f28fc90f:


    dv "幼儿园小朋友都比你弹得好！"


translate chinese day3_evening_dv_e7c47ec2:


    me "谢谢，{w}我尝试过了！"


translate chinese day3_evening_dv_e131c7ad:


    dv "我发现了！"


translate chinese day3_evening_dv_bd64fe14:


    "她可能在展示自己在吉他上的巨大优势。"


translate chinese day3_evening_dv_a9370cae:


    me "不是每个人都能成为音乐家。"


translate chinese day3_evening_dv_26e899f1:


    dv "这是什么道理呢？..."


translate chinese day3_evening_dv_343fa444:


    "阿丽夏看向天空。"


translate chinese day3_evening_dv_799ffa98:


    dv "不过你肯定不会成为音乐家，这个我可以肯定！"


translate chinese day3_evening_dv_38d5b464:


    me "为什么？"


translate chinese day3_evening_dv_c3abde57:


    dv "你听听你自己的音乐！"


translate chinese day3_evening_dv_43afff04:


    "我有点后悔来这里了。"


translate chinese day3_evening_dv_67787d9f:


    th "我倒是无所谓，可是阿丽夏为什么想要这样做呢？{w}很明显我不是一个很好的吉他手。"


translate chinese day3_evening_dv_8c75e8f3:


    me "显然。{w}不过如果我经过足够的练习..."


translate chinese day3_evening_dv_26bc6b0e:


    dv "所以说，你会练习吗？"


translate chinese day3_evening_dv_968fea51:


    me "不知道...也许会吧。"


translate chinese day3_evening_dv_bca2071d:


    "不过吉他是我最小的问题...{w}和阿丽夏进行愚蠢的争吵还不如想想这个。"


translate chinese day3_evening_dv_67458183:


    "我突然开始自怨自艾起来，想要赶紧离开这个地方。"


translate chinese day3_evening_dv_ca179b6a:


    "这种感觉让我有一种想要说些蠢话的强烈欲望。"


translate chinese day3_evening_dv_3fcbf50b:


    me "好像会有什么改变似的！"


translate chinese day3_evening_dv_1145d96e:


    dv "什么?"


translate chinese day3_evening_dv_c372f019:


    me "我练习吉他！"


translate chinese day3_evening_dv_9d34d000:


    dv "当然了！你肯定是乱弹琴几天然后就丢在一边了。"


translate chinese day3_evening_dv_e7eaa70e:


    "我又中了一箭。"


translate chinese day3_evening_dv_5187f093:


    me "你知道吗?!"


translate chinese day3_evening_dv_35264e84:


    "她应该知道吗？这和她有什么关系吗？"


translate chinese day3_evening_dv_05bd68b6:


    "我是不是因为阿丽夏说出了事实才会有这样的反应？"


translate chinese day3_evening_dv_64d153d7:


    me "你不可能什么都知道怎么做！"


translate chinese day3_evening_dv_b4c62b8a:


    dv "大概是吧...{w}但是你至少应该会做点什么吧..."


translate chinese day3_evening_dv_6528a29b:


    me "所以说你认为我什么也不会吗？"


translate chinese day3_evening_dv_a44772be:


    dv "我去哪里知道啊？"


translate chinese day3_evening_dv_92d27981:


    me "啊，我..."


translate chinese day3_evening_dv_21c57864:


    th "我什么？"


translate chinese day3_evening_dv_380c1152:


    th "我应该告诉她自己的电脑技术吗？{w}或者是我读过的书还有看过的电影？"


translate chinese day3_evening_dv_6660ca80:


    me "这真是毫无意义的对话。"


translate chinese day3_evening_dv_4a20a990:


    dv "是啊。"


translate chinese day3_evening_dv_d48bc21c:


    me "你只是拿我找乐子吧？"


translate chinese day3_evening_dv_cb92aa34:


    dv "而你只是不停地发牢骚吧！"


translate chinese day3_evening_dv_297beca3:


    me "算了！"


translate chinese day3_evening_dv_a08e0ef8:


    "我跳下了舞台，离开了表演区。"


translate chinese day3_evening_dv_8bd551d4:


    dv "你走吧！"


translate chinese day3_evening_dv_11b833b9:


    "我听到阿丽夏向我喊道。"


translate chinese day3_evening_dv_9d84e4e8:


    me "我正在走呢！"


translate chinese day3_evening_dv_2eb0d079:


    "我咬咬牙。"


translate chinese day3_evening_dv_35bec68e:


    "我沿着小路回到了广场。"


translate chinese day3_evening_dv_51f9be11:


    "斯拉维娅正在广场中间，打扫着地面。"


translate chinese day3_evening_dv_d2abd3ca:


    sl "哇，谢苗！你好！我还以为你已经睡觉了呢。"


translate chinese day3_evening_dv_4477db54:


    me "还没...你在干吗？"


translate chinese day3_evening_dv_753b1df5:


    sl "舞会结束后打扫卫生，你为什么没有来呢？"


translate chinese day3_evening_dv_79a8a3cb:


    me "只是..."


translate chinese day3_evening_dv_4bd32d93:


    sl "你去哪里啦？"


translate chinese day3_evening_dv_bbc9f7af:


    me "我..."


translate chinese day3_evening_dv_66b39d2d:


    "我可不想告诉斯拉维娅说在大家愉快的跳舞的时候我在舞台上阿丽夏的面前出丑。"


translate chinese day3_evening_dv_b498ffb4:


    me "散步来着..."


translate chinese day3_evening_dv_527c6bb3:


    sl "好吧，不过你真的应该来的说，真的很有意思。"


translate chinese day3_evening_dv_77b6031e:


    me "很高兴你玩得这么好。"


translate chinese day3_evening_dv_a7c92b66:


    "不知道是因为天气太热还是心情的问题，我今天又是汗流浃背。"


translate chinese day3_evening_dv_c79602cb:


    me "嘿，你知道我可以在哪里洗洗澡吗？"


translate chinese day3_evening_dv_46023748:


    sl "当然，往前走，沿着小路向左转，然后再向右转，你就可以看到一座浴室。"


translate chinese day3_evening_dv_3820771a:


    me "..."


translate chinese day3_evening_dv_56fc315a:


    sl "需要我陪着你吗？"


translate chinese day3_evening_dv_90d3bf06:


    me "不，不用，我会自己找到的！"


translate chinese day3_evening_dv_8a838751:


    "我不想再麻烦她了。"


translate chinese day3_evening_dv_844a11d0:


    "我沿着斯拉维娅指示的小路在森林中穿梭。"


translate chinese day3_evening_dv_7fd553fe:


    "我的整个身体都在发痒。"


translate chinese day3_evening_dv_1ac85161:


    th "我以前怎么没有注意到？"


translate chinese day3_evening_dv_b2840d09:


    "而且最重要的是，还有什么我没有注意到的事情？"


translate chinese day3_evening_dv_19111076:


    th "如果总是这样日复一日我可能一辈子都要在这个世界中度过了..."


translate chinese day3_evening_dv_cd1088c8:


    "我不禁一颤。"


translate chinese day3_evening_dv_ea4df3bd:


    "可能是因为我对这件事居然一点都不紧张。"


translate chinese day3_evening_dv_d95e35f6:


    "就好像是平常的生活一样。"


translate chinese day3_evening_dv_0ba45212:


    "我发现了树林后面的浴室。"


translate chinese day3_evening_dv_4c8601db:


    th "可是为什么少先队员们在这里洗澡呢？"


translate chinese day3_evening_dv_c7b24c86:


    th "奥尔加·德米特里耶夫娜好像说过这里的设备需要维修什么的。"


translate chinese day3_evening_dv_7cf75d85:


    me "这下倒是很有说服力..."


translate chinese day3_evening_dv_c5ff1076:


    "这里有香皂和毛巾所以我迅速洗完然后在户外呼吸着新鲜空气。"


translate chinese day3_evening_dv_9f7fc9a5:


    th "哇，好爽啊，身体上的清洁也可以洗涤心灵..."


translate chinese day3_evening_dv_00142a80:


    "我的焦急心情要么直接消散了要么也不再是困扰我的问题，好像水不仅带走了我身上的尘土，也带走了我的忧虑。"


translate chinese day3_evening_dv_9b68d7c1:


    "我开始打哈欠了，所以我决定返回辅导员的卧室。"


translate chinese day3_evening_dv_8c18ef81:


    "我还没走出几步，突然阿丽夏从草丛中跳了出来。"


translate chinese day3_evening_dv_e58bfb0c:


    dv "你...你在这里干什么？"


translate chinese day3_evening_dv_aa2954ff:


    me "我只是来洗澡。"


translate chinese day3_evening_dv_e2ccd354:


    "她恢复了意识又开始说话。"


translate chinese day3_evening_dv_d87c29ac:


    dv "然后呢?"


translate chinese day3_evening_dv_ad02f53a_2:


    me "什么然后？"


translate chinese day3_evening_dv_410dff3b:


    dv "我怎么知道？！"


translate chinese day3_evening_dv_b7d92302:


    me "你不知道你问什么啊？"


translate chinese day3_evening_dv_0adca3fb:


    "我内心的平静被打破了，只剩下焦虑和疲惫..."


translate chinese day3_evening_dv_e44bfa1d:


    me "好吧，如果你不介意的话..."


translate chinese day3_evening_dv_a5e7a3c6:


    dv "等一下..."


translate chinese day3_evening_dv_f3274282:


    me "又怎么了？"


translate chinese day3_evening_dv_761956ea:


    dv "如果刚才在舞台上你觉得...那个...我不是有意冒犯你的..."


translate chinese day3_evening_dv_1f988280:


    me "还好，我没有生气，我不会玩吉他，很多人都不会嘛。"


translate chinese day3_evening_dv_403f582b:


    "我觉得自己脸上清楚的写着不耐烦，不过我也改变不了什么，我只想对着她把一切都发泄出来。"


translate chinese day3_evening_dv_5b7b1ded:


    dv "就是，你不会！"


translate chinese day3_evening_dv_aee5d595:


    "阿丽夏显然忍住没笑。"


translate chinese day3_evening_dv_d5503c43:


    me "好了，我受够你了！"


translate chinese day3_evening_dv_a3a48b46:


    "她已经超过我的忍耐界限了。"


translate chinese day3_evening_dv_423dbcda:


    "或者说我可能要向她怒吼了...{w}甚至更糟。"


translate chinese day3_evening_dv_f72c72fe:


    dv "等等。"


translate chinese day3_evening_dv_d177607c:


    me "你不能让我自己呆一会儿吗？"


translate chinese day3_evening_dv_cc1a995c:


    "阿丽夏跟着我但是没有试图阻拦我。"


translate chinese day3_evening_dv_0cc8c845:


    dv "我真的不是故意的..."


translate chinese day3_evening_dv_2cebe654:


    "某种隐形的力量让我转过身来。"


translate chinese day3_evening_dv_d09cc761:


    me "好吧，我可以走了吗？"


translate chinese day3_evening_dv_4907e592:


    dv "是的...当然..."


translate chinese day3_evening_dv_664f2019:


    "在月色中阿丽夏看上去很漂亮，她的脸上略过一丝愧疚。"


translate chinese day3_evening_dv_aa3cefba:


    th "如果她能够关注自己的嘴的话..."


translate chinese day3_evening_dv_74c9cbb9:


    dv "你在傻笑什么？"


translate chinese day3_evening_dv_d857ef67:


    th "虽然..."


translate chinese day3_evening_dv_356af705:


    me "没什么。我只是觉得你要是不那么暴躁就好了..."


translate chinese day3_evening_dv_0fd482e1:


    dv "然后呢？又怎么样？"


translate chinese day3_evening_dv_389e70d3:


    "我不应该这么说，每个人都有自己的特点的。"


translate chinese day3_evening_dv_a0eb57ae:


    me "然后就没了。"


translate chinese day3_evening_dv_6f263bd2:


    dv "你把话说完啊！"


translate chinese day3_evening_dv_c27026d4:


    me "后边没有了，我很累了只想去睡觉..."


translate chinese day3_evening_dv_d69aa454:


    dv "站住！"


translate chinese day3_evening_dv_301d2f4b:


    me "别逼我逃跑！"


translate chinese day3_evening_dv_5e3d8340:


    dv "好像你能跑得了似的！"


translate chinese day3_evening_dv_60769081:


    "她说的有道理，经过一天的劳累之后我没什么力气再跑了，而且刚刚洗完澡我也不太想跑步。"


translate chinese day3_evening_dv_ba176b89:


    dv "所以说怎么样？"


translate chinese day3_evening_dv_ad02f53a_3:


    me "什么?"


translate chinese day3_evening_dv_a8a0c403:


    dv "我要是能变得温和一点就好了，然后呢？"


translate chinese day3_evening_dv_ef8f1d34:


    me "你试试就知道了。"


translate chinese day3_evening_dv_279c0b72:


    dv "我为什么要为你这样一个蠢货做这种事？"


translate chinese day3_evening_dv_fb217e62:


    me "那就不要试了，为什么总是和我扯上关系？"


translate chinese day3_evening_dv_772db0f1:


    "我叹了口气，拖着身子走向广场。"


translate chinese day3_evening_dv_227a4155:


    dv "那你呢？"


translate chinese day3_evening_dv_ad02f53a_4:


    me "什么?"


translate chinese day3_evening_dv_dd2be400:


    "我没有回头的回答道。"


translate chinese day3_evening_dv_7bc2ed19:


    dv "就是说，如果我这么做了，你会怎么办？"


translate chinese day3_evening_dv_a70104fb:


    me "不知道，我只是觉得这么说比较容易。"


translate chinese day3_evening_dv_9a1f6cf1:


    dv "蠢货！"


translate chinese day3_evening_dv_779b2571:


    "阿丽夏对着我大喊，然后走向了浴室。"


translate chinese day3_evening_dv_76cd70be:


    "广场上是闪闪发光的德国道路，使用洗发水擦过，有人这么说。"


translate chinese day3_evening_dv_ac1742ca:


    th "还是斯拉维娅！"


translate chinese day3_evening_dv_881163f0:


    th "我还和阿丽夏吵了一架然后没了..."


translate chinese day3_evening_dv_2a8b99c4:


    th "为了什么？"


translate chinese day3_evening_dv_7fd16daf:


    th "好像很必要似的！"


translate chinese day3_evening_dv_ea92f05b:


    th "她的行为举止..."


translate chinese day3_evening_dv_6c061942:


    "在别的情况下我都会觉得阿丽夏喜欢我只是掩盖在她的强硬态度之后。"


translate chinese day3_evening_dv_01f29ad6:


    th "那个现在可不重要！"


translate chinese day3_evening_dv_fa76e542:


    "恋人，爱情，还是什么的..."


translate chinese day3_evening_dv_12677de3:


    th "她可能根本就不是人类！"


translate chinese day3_evening_dv_e9aaa3dd:


    th "可是万一她是个普通人而这一切就发生在现实生活中呢？"


translate chinese day3_evening_dv_92236afc:


    "我打消掉这些想法，前往辅导员的房间，我被困倦包围着。"


translate chinese day3_evening_dv_2579577e:


    "门前好像有一种隐形的阻碍，让我不能走进门去。"


translate chinese day3_evening_dv_71e467af:


    th "奥尔加·德米特里耶夫娜肯定很想知道我去了哪里，为什么不参加舞会。"


translate chinese day3_evening_dv_aabe8f1c:


    th "我肯定会遭到一番严刑拷打..."


translate chinese day3_evening_dv_bcc9746d:


    "想想看阿丽夏和我一起从舞会上消失，就不难知道我们是一起翘掉的。"


translate chinese day3_evening_dv_5df6ba98:


    th "我究竟应不应该告诉她在舞会的时间我们偷偷跑出去弹吉他？"


translate chinese day3_evening_dv_4739fcde:


    th "其实没什么特别的事情..."


translate chinese day3_evening_dv_8a5e9765:


    "我为什么跑出去其实不怎么重要。"


translate chinese day3_evening_dv_0b609861:


    th "所以说..."


translate chinese day3_evening_dv_abdca19a:


    "这时我觉得保持沉默是最好的选择。"


translate chinese day3_evening_dv_bdf7e286:


    th "算了，车到山前必有路。"


translate chinese day3_evening_dv_ed6ae9d5:


    "我叹了口气，拉开了门把手。"


translate chinese day3_evening_dv_e96fefb3:


    "奥尔加·德米特里耶夫娜已经在等着我了，她还穿着一件裙子。"


translate chinese day3_evening_dv_47f7a7d5:


    mt "所以说，谢苗，你去哪里了？"


translate chinese day3_evening_dv_48199bb7:


    me "我..."


translate chinese day3_evening_dv_ae28ee74:


    me "说实话，我刚刚和阿丽夏在舞台上。"


translate chinese day3_evening_dv_baaff23f:


    mt "你们在那里做什么？"


translate chinese day3_evening_dv_0c8efc3f:


    me "弹吉他..."


translate chinese day3_evening_dv_30a21108:


    me "我...{w}只是随便走走。"


translate chinese day3_evening_dv_b166a941:


    mt "你自己吗?"


translate chinese day3_evening_dv_9513cd87:


    me "嗯..."


translate chinese day3_evening_dv_db050801:


    mt "算了，无所谓，不过你为什么没有来舞会呢？"


translate chinese day3_evening_dv_52fe9abf:


    me "说实话，我不太喜欢跳舞..."


translate chinese day3_evening_dv_ff3e3b04:


    mt "那怎么了？{w}这是一项夏令营活动，这意味着每一个少先队员都要参加！"


translate chinese day3_evening_dv_1b595140:


    "她瞪着我。"


translate chinese day3_evening_dv_3607aec3:


    mt "你应该找个地方安静的坐着！"


translate chinese day3_evening_dv_2993aaa0:


    me "抱歉..."


translate chinese day3_evening_dv_ed0914b5:


    "我叹了叹气。"


translate chinese day3_evening_dv_dbc34155:


    mt "好吧。"


translate chinese day3_evening_dv_d14fc77f:


    "辅导员变得温和了一些。"


translate chinese day3_evening_dv_41704f8b:


    mt "赶快去睡觉！"


translate chinese day3_evening_dv_9c792872:


    th "暴风雨过去了。"


translate chinese day3_evening_dv_ce617998_1:


    "..."


translate chinese day3_evening_dv_5649b7bb:


    "我躺在床上，但是却睡不着。"


translate chinese day3_evening_dv_7bb07423:


    "头脑中充满了吉他的旋律，还有阿丽夏..."


translate chinese day3_evening_dv_1a6a274a:


    th "我很好奇，自己究竟能不能成为一名专业的音乐家？"


translate chinese day3_evening_dv_c5ba79b7:


    th "而且我究竟有没有这个愿望？"


translate chinese day3_evening_dv_24a063fd:


    th "我为什么要有这个愿望？那会很有趣，但是也不过如此了。"


translate chinese day3_evening_dv_bc447272:


    th "也许是因为阿丽夏？"


translate chinese day3_evening_dv_66b20a8a:


    th "她就像是一只有活力的吸血鬼，能够把你的血液吸净！"


translate chinese day3_evening_dv_ce617998_2:


    "..."


translate chinese day3_evening_dv_b5247e67:


    "我逐渐闭上了眼睛，不一会儿我就睡着了。"


translate chinese day3_fail_6f4c676f:


    "斯拉维娅回到了舞台上。"


translate chinese day3_fail_5b8f0841:


    "我又坐了一会儿，然后悄悄的溜走，以确定没有人看到我。"


translate chinese day3_fail_a20cefa7:


    "..."


translate chinese day3_fail_ab41d603:


    "经历过如此“成功”的一次舞蹈之后我可不想遇到任何人。"


translate chinese day3_fail_57cdff00:


    "最安静的地方就是这个车站，而且可能再也不会有一辆410路公共汽车到达这里了。"


translate chinese day3_fail_5c5d8e3f:


    "但是！{w}我尖叫了出来。"


translate chinese day3_fail_32f73f0c:


    "我的面前正停着一辆公共汽车。{w}就像是第一天的那一辆。"


translate chinese day3_fail_193dd370:


    "我僵住了。"


translate chinese day3_fail_6770889d:


    me "怎么，什么，怎么，为什么...?"


translate chinese day3_fail_83e56da8:


    "一瞬间我回想起了自己所有关于来到这个夏令营的理论。"


translate chinese day3_fail_8d9231e3:


    "我被自己的的发现惊呆了：这几天我过分的融入了这里的生活，以至于我几乎忘记了这是一个不正常的世界。"


translate chinese day3_fail_dada5103:


    "我呆呆的看着那辆该死的Icarus，然后伸出手扇了自己两巴掌确定这不是幻觉，那辆车还在那里。"


translate chinese day3_fail_28a25175:


    th "如果它在这里，它意味着我该回家了！"


translate chinese day3_fail_b7190439:


    me "撒哟娜拉~少先队员们！"


translate chinese day3_fail_8ff4f863:


    "我冲向大门处..."


translate chinese day3_fail_a0123424:


    "我恢复意识时回到了地上。{w}我的鼻子疼的我直叫唤。"


translate chinese day3_fail_9b2c898b:


    "我站起来试着了解发生了什么情况。"


translate chinese day3_fail_929396d1:


    th "我好像撞到了什么东西..."


translate chinese day3_fail_8d0f3707:


    "公交车摸起来比现实还要现实。"


translate chinese day3_fail_b50ff2e2:


    "我想要伸出手摸摸大门...{w}可是好像有一座隐形的大门。"


translate chinese day3_fail_ca7b4ca3:


    "我被本能的恐惧所包围。"


translate chinese day3_fail_2dda9f46:


    "对于一切事情的恐惧—夏令营，居民们，公共汽车。"


translate chinese day3_fail_6d8da21b:


    th "我到底是怎么来到这里的？"


translate chinese day3_fail_835b1bec:


    th "这是什么见鬼的Icarus？{w}我为什么会遇到这种事情？！"


translate chinese day3_fail_908aa7d1:


    "突然一阵强风袭来。"


translate chinese day3_fail_dea6ec52:


    "我转过身来，发现车轮下面有一个小纸片。"


translate chinese day3_fail_2ef2aa5c:


    "上面写着什么。"


translate chinese day3_fail_3b835d43:


    "{i}你来到这里是有原因的。{/i}"


translate chinese day3_fail_b1b730db:


    "这种糟糕的书写好像很熟悉。"


translate chinese day3_fail_a9d82379:


    th "我肯定在什么地方见到过..."


translate chinese day3_fail_28815dfd:


    "我突然灵光一现！{w}我捡起一块木炭在纸的背面胡乱涂画着。"


translate chinese day3_fail_aa5a386f:


    "完全一样的笔体！"


translate chinese day3_fail_078ccf4d:


    "我清醒了。"


translate chinese day3_fail_e5eddf45:


    th "我从未来给自己写了这条信息！一定是这样的！"


translate chinese day3_fail_b57a15b1:


    th "或者不是。{w}或许是来自过去..."


translate chinese day3_fail_38859e59:


    th "见鬼！我完全不明白！"


translate chinese day3_fail_f958927a:


    "不管怎么说，这是我的字体。"


translate chinese day3_fail_6844d216:


    "想要伪造肯定不难，不过我很确定这是我自己写的。"


translate chinese day3_fail_04f28a5d:


    "我收起那片纸，想要再一次尝试走进门。"


translate chinese day3_fail_e5a11986:


    "隐形的墙壁还在那里。"


translate chinese day3_fail_54febcf3:


    "我围绕着公交车转来转去，到处敲一敲，往里边偷偷看。"


translate chinese day3_fail_8425e955:


    "一切看起来都很寻常。{w}不过现实中却不是。"


translate chinese day3_fail_5386dfcb:


    "很重的石头从草丛中跳起来，看不出一点破损的痕迹。"


translate chinese day3_fail_53e2e58a:


    "一点反应都没有，没有一处划痕。"


translate chinese day3_fail_78354201:


    "我坐在路肩上叹着气。"


translate chinese day3_fail_35da3684:


    "如果思考一下的话，那张纸片在提示着什么..."


translate chinese day3_fail_751d8d30:


    "而且看起来现在的处境不会对我造成伤害。"


translate chinese day3_fail_105fc865:


    mt "谢苗！"


translate chinese day3_fail_07e68b3f:


    th "看起来辅导员正在找我。"


translate chinese day3_fail_06c92db3:


    th "有意思，看看这下她怎么解释这个汽车。{w}她还会不会继续说几天之后才会有公交车来。"


translate chinese day3_fail_c3a1e6ea:


    "我跳起来，跑向奥尔加·德米特里耶夫娜的方向。"


translate chinese day3_fail_d4b7729b:


    me "所以说你怎么解释那个！"


translate chinese day3_fail_dc7df731:


    "我以一副胜利者的姿态神气的指向路边。"


translate chinese day3_fail_9de9961c:


    mt "解释什么？"


translate chinese day3_fail_afc5a641:


    "她惊讶的回答着。"


translate chinese day3_fail_47eaf14e:


    "我转过身去。"


translate chinese day3_fail_26562526:


    "汽车消失了...{w}就像是它突然出现一样。"


translate chinese day3_fail_f0ef1916:


    "胜利的怒吼在我的喉咙中夭折。"


translate chinese day3_fail_0c4809e3:


    mt "到了睡觉时间了，快走！"


translate chinese day3_fail_639b476b:


    me "可是... 可..."


translate chinese day3_fail_6334b91e:


    mt "什么？"


translate chinese day3_fail_be80cde7:


    me "公交车！刚才有一辆公交车！刚刚还在这里呢！"


translate chinese day3_fail_ef1ece89:


    mt "不可能的。"


translate chinese day3_fail_1ee77ef5:


    "她冷静的回答道。"


translate chinese day3_fail_ed2597b4:


    "我盯着奥尔加·德米特里耶夫娜的脸，她要么很善于撒谎，要么什么都没有看见。"


translate chinese day3_fail_65b9f63e:


    "那么就是只有我才看见？"


translate chinese day3_fail_41e67f91:


    th "不会。我刚刚还看到了那辆见鬼的410路。"


translate chinese day3_fail_09631ec6:


    me "别说谎了！"


translate chinese day3_fail_5788b9db:


    "我悄悄地说道。"


translate chinese day3_fail_5afe237b:


    mt "谢苗，我不明白。"


translate chinese day3_fail_c27faba3:


    me "别对我说谎了！这里刚才有一辆公共汽车！是你！是你一直阻止我离开这里！"


translate chinese day3_fail_9e2a7291:


    "我咬着牙，尽量冷静的说。"


translate chinese day3_fail_a34667cc:


    mt "你吓到我了...现在该睡觉了。"


translate chinese day3_fail_f4cdc187:


    th "还是和平常一样，我无法从她这里得到任何答案。"


translate chinese day3_fail_1665b82c:


    "不过说起来我真的很困了。"


translate chinese day3_fail_c0511a05:


    "我快速的超过辅导员，故意的不理她。"


translate chinese day3_fail_01dfa1f8:


    "我睡不着觉，只有那张皱皱巴巴的写着“你来这里是有原因的”的纸条提醒着我之前三天的事情都是真实的。"
# Decompiled by unrpyc: https://github.com/CensoredUsername/unrpyc
