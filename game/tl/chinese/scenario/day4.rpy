
translate chinese day4_std_morning_81731118:


    "伴随着一阵地狱般的巨响，我醒了过来。"


translate chinese day4_std_morning_163f48a5:


    "这响声似乎来自我的意识深处。"


translate chinese day4_std_morning_dfa1885d:


    "不过等我恢复了意识之后，我发现了这响声来自我的闹钟。"


translate chinese day4_std_morning_c8d82a68:


    th "奇怪，这是从哪里来的？为什么在我的床边？"


translate chinese day4_std_morning_51698244:


    "从表上看，现在是7点半。"


translate chinese day4_std_morning_77df9db1:


    "奥尔加·德米特里耶夫娜已经离开了，现在没有人逼着我去参加列队集合仪式。"


translate chinese day4_std_morning_f7391aeb:


    th "所以我可以再睡一会儿。"


translate chinese day4_std_morning_08555663:


    "我闭上了眼睛，不过我的意识似乎已经喝完了早上的咖啡，准备迎接一天的工作了，我得起床了。"


translate chinese day4_std_morning_dae26579:


    th "我应该制定一下计划，再怎么说今天也要有点发现。"


translate chinese day4_std_morning_213e2357:


    "不过我什么也想不出来。"


translate chinese day4_std_morning_dbbf160b:


    th "算了，我得起床清醒清醒。"


translate chinese day4_std_morning_0f23f979:


    "去水房的路上我遇到了热妮娅。"


translate chinese day4_std_morning_2b068e19:


    me "你怎么起的这么早？"


translate chinese day4_std_morning_86a5cb53:


    mz "起得早有什么问题吗？"


translate chinese day4_std_morning_bb2e0646:


    "她像是受到了侮辱一样看着我。"


translate chinese day4_std_morning_08d4c5a9:


    me "没有，没什么，只是好奇。"


translate chinese day4_std_morning_44cce5e2:


    mz "这不关你的事！"


translate chinese day4_std_morning_b3f5b0be:


    "呃，热妮娅即使是心情好的时候也不怎么友好，况且今天她很愤怒。"


translate chinese day4_std_morning_eabc170b:


    "冷水让我振作起来，我头脑中的迷雾逐渐散开，让我重新组织思考。"


translate chinese day4_std_morning_ca15c6d0:


    "奇怪的是，比起担心找到我的那些答案，我更担心的是在食堂找到一个好位置。"


translate chinese day4_std_morning_db0151db:


    "我刷完牙准备离开，突然有一个细小的声音传来。"


translate chinese day4_std_morning_4766e5e2:


    th "可能是松鼠或者是其他的什么动物。"


translate chinese day4_std_morning_3cb11a08:


    "我听到了另一种声音，这一次更远一点。"


translate chinese day4_std_morning_2a6c3c6a:


    "我沿着小路走了十来米，寻找声音的源头。"


translate chinese day4_std_morning_e0c86c0b:


    "没有人，只有清晨的森林。"


translate chinese day4_std_morning_82583edb:


    "我回到了水池，看到未来正在草坪上找什么东西。"


translate chinese day4_std_morning_152eeed8:


    "发现我之后她笑了，跳着向我走来。"


translate chinese day4_std_morning_a1863396:


    mi "哇，你好！早上好！我不小心把牙膏挤得到处都是，现在正在收集中~"


translate chinese day4_std_morning_3243720e:


    "挂满露珠的草丛似乎不会对事态有什么帮助。"


translate chinese day4_std_morning_7f1bbccc:


    me "你确定这是个好主意？"


translate chinese day4_std_morning_fdcbae6e:


    mi "嗯...为什么？我还能做什么？我只剩下这些了..."


translate chinese day4_std_morning_30820f3b:


    "她像一个被抢走了玩具的孩子一样撅起嘴。"


translate chinese day4_std_morning_7ff796f1:


    me "把我的给你吧..."


translate chinese day4_std_morning_fd9fb11a:


    "我从我的包包里寻找着。"


translate chinese day4_std_morning_1ac55bec:


    "我的牙粉没了。"


translate chinese day4_std_morning_4396dacc:


    th "唔...奇怪，我不是刚刚放在这里的吗？{w}我才走开了一分钟就没了？"


translate chinese day4_std_morning_b866f4d7:


    me "听着，我好像是忘带了..."


translate chinese day4_std_morning_b5e2c150:


    "我不想说它凭空消失了。"


translate chinese day4_std_morning_25a1bc7e:


    "考虑到这个女孩如此的敏感，可以想象一件普通的日用品的消失也可以让她避免大脑过热而紧急重启。"


translate chinese day4_std_morning_3f91116b:


    me "好吧，我得走了..."


translate chinese day4_std_morning_165f0267:


    mi "哇，当然，来拜访我们的社团...我是说拜访我，那里只有我自己，但是我们，我是说我..."


translate chinese day4_std_morning_d82a5e6d:


    "未来的声音消失在我身后夏日的清晨中了。"


translate chinese day4_std_morning_0c128c91:


    "回到辅导员卧室，我检查了手机的电量。"


translate chinese day4_std_morning_2f9d1f0c:


    "可能不够一整天了。"


translate chinese day4_std_morning_3d04d563:


    th "当然这派不上太大用场，不过还是..."


translate chinese day4_std_morning_632d44dd:


    "在八十年代想要找到一个充电器意味着要发明出来。"


translate chinese day4_std_morning_f6cc3af9:


    "我准备去吃早饭，这时有人敲门。"


translate chinese day4_std_morning_482a0501:


    "是斯拉维娅。"


translate chinese day4_std_morning_47583a01:


    sl "早上好！{w}你看到奥尔加·德米特里耶夫娜了吗？"


translate chinese day4_std_morning_9e0939cf:


    "我盯着她的胸部，前一天我曾经沉迷的不能自拔。"


translate chinese day4_std_morning_2645a069:


    sl "谢苗?"


translate chinese day4_std_morning_3f835829:


    "斯拉维娅担心的看着我，可是我还是无法从她的胸部上移开视线。"


translate chinese day4_std_morning_a428f2b1:


    sl "有什么问题吗？"


translate chinese day4_std_morning_a21b3e34:


    me "正相反，一切都..."


translate chinese day4_std_morning_65e261bd:


    sl "奥尔加·德米特里耶夫娜...?"


translate chinese day4_std_morning_535da2f1:


    me "不知道，我醒过来的时候她就已经走了。"


translate chinese day4_std_morning_1e0f3ed2:


    "我终于控制住了自己。"


translate chinese day4_std_morning_0d62f1e5:


    sl "好吧，我还有一件事，早饭时再见吧。"


translate chinese day4_std_morning_145891de:


    "斯拉维娅笑着挥挥手离开了。"


translate chinese day4_std_morning_f6cc3af9_1:


    "我正准备去吃早饭，这时有人敲门。"


translate chinese day4_std_morning_26171c75:


    "是列娜。"


translate chinese day4_std_morning_5edd55c2:


    un "唔...早上好..."


translate chinese day4_std_morning_70958877:


    me "嗯...早上好..."


translate chinese day4_std_morning_dab051d6:


    "我稍微向后退。"


translate chinese day4_std_morning_6e7a684a:


    "昨天的记忆还历历在目，不过我真的不想再提了。"


translate chinese day4_std_morning_ed2df426:


    me "你是来找奥尔加·德米特里耶夫娜的吧？"


translate chinese day4_std_morning_736f44dd:


    un "不...{w}不对，是的..."


translate chinese day4_std_morning_db521796:


    "还是平常的那个列娜—害羞又容易不好意思。"


translate chinese day4_std_morning_c62b7542:


    "在码头上的舞蹈...现在只感觉像是一个梦。"


translate chinese day4_std_morning_edd9b25a:


    me "抱歉，她不在这里..."


translate chinese day4_std_morning_98757b47:


    un "嗯...好吧..{w}我先走了..."


translate chinese day4_std_morning_751ac511:


    me "唔..."


translate chinese day4_std_morning_2dd7a526:


    "她离开之后我觉得自己刚刚太冷淡了，决定下一次见面时表现的好一点。"


translate chinese day4_std_morning_07104fa7:


    "不过这个早晨还是充满阳光而美丽的。"


translate chinese day4_std_morning_5a3770b6:


    "阳光让整个夏令营的少先队员们充满力量的开始新的一天。"


translate chinese day4_std_morning_08b429c2:


    "或者，对我来说，浪费一天的精力来弄明白这里到底发生了什么。"


translate chinese day4_std_morning_eaabbc69:


    "食堂附近有一大群人。"


translate chinese day4_std_morning_2260fedb:


    "当然整个营地中最受少先队员们喜爱的还是食堂，不过门廊下为什么聚集了这么多人？"


translate chinese day4_std_morning_f5aef4ac:


    "我走近想要弄清楚发生了什么事。"


translate chinese day4_fail_morning_bd4585a5:


    "我的脑袋里一阵地狱般的巨响把我叫醒。{w}我的头骨好像被从内部击碎一样。"


translate chinese day4_fail_morning_fce68231:


    "我揉揉眼睛，发现了床边的闹钟，一把按掉了它然后继续睡下去。"


translate chinese day4_fail_morning_a20cefa7:


    "..."


translate chinese day4_fail_morning_d941f014:


    "等我睡醒的时候，已经差一刻九点了。"


translate chinese day4_fail_morning_772587d6:


    th "是时候起床了，我可不能错过早饭。"


translate chinese day4_fail_morning_3b1d754c:


    "我直起身，伸个懒腰，然后...{w}突然想起了昨晚的事情！"


translate chinese day4_fail_morning_5ef37d77:


    "我的枕头下面有一块小纸条。"


translate chinese day4_fail_morning_470a435f:


    "“你来这里是有原因的。”"


translate chinese day4_fail_morning_2dc1af34:


    th "我还是不明白，这到底是什么意思...?"


translate chinese day4_fail_morning_50258f02:


    th "我是怎么给自己写一条信息的？"


translate chinese day4_fail_morning_cdc1a9f5:


    th "我为什么不记得了？"


translate chinese day4_fail_morning_f2a17f0f:


    "问题远比答案多。{w}准确的说是完全没有答案。"


translate chinese day4_fail_morning_3d4304d6:


    "我离开了小屋，检查着营地。"


translate chinese day4_fail_morning_25fa9b94:


    th "不，不像是错觉。"


translate chinese day4_fail_morning_b5c3816d:


    "不过这个便签...让所有的事情都变得很复杂。"


translate chinese day4_fail_morning_2b1800da:


    th "我可以相信什么？"


translate chinese day4_fail_morning_5b2a5873:


    "我的肚子在这个时候靠不住的叫了起来。"


translate chinese day4_fail_morning_4c70df13:


    th "就连Mulder探员都不会饿着肚子办案。"


translate chinese day4_fail_morning_eaabbc69:


    "食堂附近意外的有一大群人。"


translate chinese day4_fail_morning_2260fedb:


    "当然整个营地中最受少先队员们喜爱的还是食堂，不过门廊下为什么聚集了这么多人？"


translate chinese day4_fail_morning_f5aef4ac:


    "我走近想要弄清楚发生了什么事。"


translate chinese day4_us_morning_0e224319:


    "我在梦中感觉到了温暖。"


translate chinese day4_us_morning_62c15031:


    "确实有时候会这样，还没有完全清醒的时候，你就能感觉到耀眼的阳光，当你醒来之后还需要眨眨眼来适应早晨的光线。"


translate chinese day4_us_morning_57bf0ff9:


    "是一个很不错的早晨。{w}鸟儿在唱着歌，空气散发着清香，整个世界都在晒着日光浴。"


translate chinese day4_us_morning_51613331:


    "我还想要再睡一会儿，不过有什么不太对劲。"


translate chinese day4_us_morning_32a6b4a1:


    "昨天的事情开始闪现在我的眼前。"


translate chinese day4_us_morning_be5ac746:


    "吓人的还有不吓人的鬼故事，乌里扬娜..."


translate chinese day4_us_morning_a581c654:


    "正在紧紧的抱着我打着呼噜，可不像一个小女孩的样子。"


translate chinese day4_us_morning_a84a89a7:


    "不过我一点也不担心。"


translate chinese day4_us_morning_73427713:


    th "再怎么说，现在还很早，超不过七八点钟。"


translate chinese day4_us_morning_bb3ada36:


    th "谁会这么早来图书馆呢？"


translate chinese day4_us_morning_5c2e2f4e:


    "我向后坐坐，看了看窗外。"


translate chinese day4_us_morning_a3c34756:


    th "再过几分钟太阳就会照射到乌里扬卡的眼睛了。"


translate chinese day4_us_morning_7e2c6b03:


    me "那就是你该睡醒的时候了！"


translate chinese day4_us_morning_70e45333:


    th "有意思，她是怎么抱的这么紧的？"


translate chinese day4_us_morning_a9947f93:


    th "真的，我完全挣脱不开。"


translate chinese day4_us_morning_a26bbdc9:


    "理论上我还可以再坚持一两个小时，不过我听到了脚步声。"


translate chinese day4_us_morning_89be002b:


    th "听起来麻烦找上门来了！"


translate chinese day4_us_morning_05c821d8:


    me "醒醒!听到没？醒醒！"


translate chinese day4_us_morning_e09dbdfe:


    "我小心翼翼的摇着乌里扬娜的肩膀，试图让她醒过来，不过只是徒劳。"


translate chinese day4_us_morning_5fe87263:


    "同时，脚步声越来越近了。"


translate chinese day4_us_morning_ee87d584:


    th "我得不惜任何代价挽回一切！"


translate chinese day4_us_morning_fb5bb4b6:


    "想要站起来是不可能的，我已经试过了，所以我又试着爬行。"


translate chinese day4_us_morning_0178b537:


    "我的动作很像是后备役军人的训练（虽然我没有参加过），一个士兵拖着他负伤的长官逃脱炮火的袭击..."


translate chinese day4_us_morning_385cbf84:


    "长官已经失去了意识，士兵已经精疲力尽，他们的周围布满了铁丝网。"


translate chinese day4_us_morning_29669d4f:


    "我刚刚把乌里扬娜在书架后边藏好，门就打开了。"


translate chinese day4_us_morning_d2052b63:


    "热妮娅站在台阶上。"


translate chinese day4_us_morning_c24e1c91:


    th "她过度敬业，刚刚黎明就来图书馆上班了。"


translate chinese day4_us_morning_61db85c5:


    el "等一下!"


translate chinese day4_us_morning_3417c9cb:


    "我听到了一个熟悉的声音。"


translate chinese day4_us_morning_177ac4cc:


    th "哈？有客人？所以说她来到这里是有原因的？"


translate chinese day4_us_morning_ccf4620f:


    mz "她为什么不在昨天来，或者今天晚一点儿再来？"


translate chinese day4_us_morning_2dfa95a0:


    el "科学不等人。"


translate chinese day4_us_morning_229b38d1:


    mz "科学..."


translate chinese day4_us_morning_49bd4154:


    "是电子小子。"


translate chinese day4_us_morning_dfa36536:


    mz "等一下，我去找找。"


translate chinese day4_us_morning_35e81a10:


    "她向我们两个走过来。"


translate chinese day4_us_morning_1a39bf90:


    "我刚刚能把乌里扬卡转到背上背着她爬过书架就累的瘫在了地上。"


translate chinese day4_us_morning_b01a480f:


    mz "是什么？控制论数学？{w}还是数学控制论...？"


translate chinese day4_us_morning_5e51dcff:


    th "她似乎没有发现我们。"


translate chinese day4_us_morning_266ebd21:


    el "我要了解一下电磁炮理论..."


translate chinese day4_us_morning_dc097374:


    mz "你疯了？我们怎么会有军用的参考书？"


translate chinese day4_us_morning_6335428f:


    el "那完全不是军用好不好..."


translate chinese day4_us_morning_e0071b38:


    "他们沉默了一会儿。"


translate chinese day4_us_morning_861c8f5c:


    el "热妮娅"


translate chinese day4_us_morning_6a6e65d4:


    mz "什么?"


translate chinese day4_us_morning_ab9b10c0:


    el "咱们今晚一起去河边..."


translate chinese day4_us_morning_552a9040:


    mz "做什么?"


translate chinese day4_us_morning_62e815fe:


    el "那个...{w} 只是..."


translate chinese day4_us_morning_7f61d9af:


    mz "我还有很多事...{w}快走吧，你的机器人在等着你呢。"


translate chinese day4_us_morning_3323f7c2:


    el "好吧..."


translate chinese day4_us_morning_1e0523ce:


    "关门的声音就像是电子小子求爱史的墓志铭。"


translate chinese day4_us_morning_67878bf4:


    th "看起来他完全不是一个冷血的机器人。"


translate chinese day4_us_morning_7efc1a56:


    "我悄悄地笑起来。"


translate chinese day4_us_morning_c59fe92e:


    "总之笑什么的不太合适，我们得找机会离开图书馆。"


translate chinese day4_us_morning_89531da4:


    "现在等着热妮娅去吃早饭是最好的解决方案。"


translate chinese day4_us_morning_395caa92:


    "她是个无产阶级工人，不是电子小子的机器人，她还是要吃饭的。"


translate chinese day4_us_morning_5672d57d:


    "同时，乌里扬娜没有要醒的意思。"


translate chinese day4_us_morning_29b73a64:


    th "至少她不再打呼噜了。"


translate chinese day4_us_morning_9a87b38e:


    "图书馆很安静。"


translate chinese day4_us_morning_928c4608:


    "我没有看到热妮娅，不过这种情况还不错。"


translate chinese day4_us_morning_ba7c4e5e:


    "突然间，她的桌子那边出现了一段无法理解的噪音：敲打声，爆破声...{w}音乐响起。"


translate chinese day4_us_morning_70db9b38:


    "苏联国歌？！"


translate chinese day4_us_morning_ba1d06f5:


    th "真好！"


translate chinese day4_us_morning_c14cb5ff:


    "其实本来还不错，如果不是热妮娅，突然跟着唱了起来！"


translate chinese day4_us_morning_c94f8616:


    mz "牢不可破的联盟..."


translate chinese day4_us_morning_75088394:


    "我很羡慕她的爱国热情。"


translate chinese day4_us_morning_06f8fd05:


    "然而她的声音有一些问题，苏联摇滚界肯定不缺热妮娅这么个人物。"


translate chinese day4_us_morning_9877cbdf:


    "国歌结束之后一个陌生的声音开始宣布粮食生产的五年计划超标完成的事情。"


translate chinese day4_us_morning_26511e70:


    "显然是一台收音机。"


translate chinese day4_us_morning_2d3e165e:


    "我用心的听着，说不定里边会播报一些有用的信息。"


translate chinese day4_us_morning_526eb40c:


    "不过在播送玩一些农业成就之后，声音便逐渐消失了。"


translate chinese day4_us_morning_13ff876e:


    th "也许是信号干扰吧。"


translate chinese day4_us_morning_a0c04095:


    "热妮娅站起来朝我们走来。"


translate chinese day4_us_morning_9e8818da:


    "局势变得严重了。"


translate chinese day4_us_morning_356a32e8:


    "我挣扎着想要摆脱乌里扬卡的怀抱。"


translate chinese day4_us_morning_079daa56:


    "我终于摆脱了束缚，但是刚刚的折腾让我精疲力竭，没有力量站起来了。"


translate chinese day4_us_morning_86aec307:


    th "这下得做好最坏的打算开始想几个借口了..."


translate chinese day4_us_morning_a755654d:


    "突然热妮娅停下了脚步。"


translate chinese day4_us_morning_d2163297:


    "看起来热妮娅站在了书架的另一端。"


translate chinese day4_us_morning_f120f183:


    "我听到了翻书的声音。"


translate chinese day4_us_morning_ce9a6b7b:


    th "也许她在找什么东西。"


translate chinese day4_us_morning_9743d34f:


    "她拿走了一本书返回了书桌。"


translate chinese day4_us_morning_f06c5e02:


    "门突然打开了。"


translate chinese day4_us_morning_3cf16294:


    mt "谢苗?!{w}你看见他了吗？"


translate chinese day4_us_morning_5fd5cacf:


    "奥尔加·德米特里耶夫娜上气不接下气。"


translate chinese day4_us_morning_5419bf5d:


    mt "乌里扬娜?!"


translate chinese day4_us_morning_ac7585a7:


    mz "没有..."


translate chinese day4_us_morning_80c6622f:


    "热妮娅吃惊的回答道。"


translate chinese day4_us_morning_519eac8f:


    "接着又是同样大声音的关门声。"


translate chinese day4_us_morning_8adc70f4:


    "看起来大家都在找我们，而且热妮娅马上就要开始搜索整个图书馆了..."


translate chinese day4_us_morning_2dc3e10c:


    "不过幸运的是，就在这个时候，远处的铃声响了，召唤少先队员们去吃早饭。"


translate chinese day4_us_morning_4d1c12f2:


    "热妮娅是一个准时的人，她听到铃声后决定不再磨蹭，马上就离开了，图书馆里只剩下我和乌里扬娜。"


translate chinese day4_us_morning_35353818:


    "现在该决定做什么好了。"


translate chinese day4_us_morning_195ba3f0:


    "这下不怕被抓到了，我对着她的耳朵喊道。"


translate chinese day4_us_morning_e501c3ad:


    me "着火啦！！"


translate chinese day4_us_morning_e9fe52a8:


    "她瞬间跳了起来，向四周看去。"


translate chinese day4_us_morning_a94e04aa:


    "乌里扬娜看见我时很惊讶。"


translate chinese day4_us_morning_ba2cf72c:


    us "你在这里做什么？"


translate chinese day4_us_morning_571b4ce2:


    me "玩过家家。"


translate chinese day4_us_morning_ed803309:


    us "哈?"


translate chinese day4_us_morning_e29d2e60:


    me "别在意...你睡的怎么样？"


translate chinese day4_us_morning_41c2189c:


    us "嗯..."


translate chinese day4_us_morning_feb4bc5f:


    "看起来她还没有完全清醒过来。"


translate chinese day4_us_morning_d2228502:


    me "早饭?"


translate chinese day4_us_morning_41c2189c_1:


    us "嗯..."


translate chinese day4_us_morning_8bceae14:


    "我们离开了图书馆。"


translate chinese day4_us_morning_d5d18ead:


    th "终于!"


translate chinese day4_us_morning_9eb55c9a:


    th "现在我可以确定自己不用再和别人解释为什么我们在图书馆呆了一整夜..."


translate chinese day4_us_morning_71de8971:


    us "那样睡着了，抱歉…"


translate chinese day4_us_morning_6d36c28b:


    me "没关系。"


translate chinese day4_us_morning_9b91052f:


    "我估计自己的声音听起来太没有诚意以至于她怀疑的看着我。"


translate chinese day4_us_morning_4322ee3d:


    us "等一下...{w}你在那里都在干什么？"


translate chinese day4_us_morning_75f4b174:


    me "你不会相信我的。"


translate chinese day4_us_morning_b515de5d:


    us "等一下...{w}就是说..."


translate chinese day4_us_morning_4f6d2aa7:


    "乌里扬卡傻笑着，走开了几步然后喊起来："


translate chinese day4_us_morning_abd7e09d:


    us "我是第一个吃早饭的！"


translate chinese day4_us_morning_74c13f1d:


    me "睡了这么长时间之后这倒是不太令人惊讶..."


translate chinese day4_us_morning_72667b52:


    "我说着，可是乌里扬娜已经跑出了老远，听不到我了。"


translate chinese day4_us_morning_eaabbc69:


    "食堂附近意外的有一大群人。"


translate chinese day4_us_morning_2260fedb:


    "当然整个营地中最受少先队员们喜爱的还是食堂，不过门廊下为什么聚集了这么多人？"


translate chinese day4_us_morning_f5aef4ac:


    "我走近想要弄清楚发生了什么事。"


translate chinese day4_main1_3fe8cd89:


    "好像整个营地的人们都聚集到了这里，有熟悉的女生们，奥尔加·德米特里耶夫娜还有电子小子。"


translate chinese day4_main1_0c95f62a:


    "他们在激动的讨论着什么。"


translate chinese day4_main1_6785b764:


    "我走近了一些。"


translate chinese day4_main1_c74edda5:


    mt "啊，谢苗！"


translate chinese day4_main1_af775fc1:


    mt "你到底去了哪里？{w}我等了你一整夜，然后又找了你一早晨！{w}乌里扬娜告诉我你昨天就离开了图书馆。"


translate chinese day4_main1_9d5d157b:


    "我看看乌里扬卡。{w}她顽皮的笑着。"


translate chinese day4_main1_51149e68:


    me "..."


translate chinese day4_main1_2f165ff9:


    mt "等一下在找你算账！"


translate chinese day4_main1_4c6e2dcd:


    mt "你今天看到舒里克了吗？"


translate chinese day4_main1_3aabe659:


    me "没有，怎么啦？"


translate chinese day4_main1_28e30b6d:


    mt "我们今天一大早就找不到他了。"


translate chinese day4_main1_70b53a62:


    th "消失的少先队员—这倒是一点新鲜事。"


translate chinese day4_main1_547a39ce:


    mt "不过他昨天和你在一起？"


translate chinese day4_main1_1e640e7b:


    "她对电子小子说道。"


translate chinese day4_main1_8fb4fd1c:


    el "是的..."


translate chinese day4_main1_7c91c87b:


    mt "然后你今天起来发现哪里都找不着他人？"


translate chinese day4_main1_62543f01:


    el "不..."


translate chinese day4_main1_30b2a809:


    mt "你为什么不马上来找我？"


translate chinese day4_main1_9f2b5808:


    el "这个...我觉得他可能是早起去洗漱什么的..."


translate chinese day4_main1_a5b6d3cb:


    sl "他昨天提到了什么吗？"


translate chinese day4_main1_f2c87db5:


    "斯拉维娅插话道。"


translate chinese day4_main1_c620bee1:


    el "比如说？"


translate chinese day4_main1_01cfc02e:


    sl "比如说他准备去哪里什么的？"


translate chinese day4_main1_62543f01_1:


    el "没有..."


translate chinese day4_main1_cd7d9ef9:


    me "这有什么可担心的？{w}现在才九点，他可能只是去散散步。"


translate chinese day4_main1_6059e846:


    sl "你不了解舒里克。"


translate chinese day4_main1_453d16cb:


    "她严肃的看着我。"


translate chinese day4_main1_66a4843a:


    me "嗯，是的..."


translate chinese day4_main1_66774de2:


    "不过我没有感觉这件事有什么可疑之处。"


translate chinese day4_main1_fac4579f:


    mt "好啦，别在这里紧张了，咱们快出发去找他吧！"


translate chinese day4_main1_d68ecb78:


    us "他怎么能错过早饭？！"


translate chinese day4_main1_56aa99aa:


    "乌里扬卡偷偷笑了起来。"


translate chinese day4_main1_3fab6262:


    dv "确实！吃饭要紧。"


translate chinese day4_main1_a24b7420:


    "少先队员们进入了食堂。"


translate chinese day4_main1_7a51fac4:


    "不过奥尔加·德米特里耶夫娜拦住了我。"


translate chinese day4_main1_29b74cfc:


    mt "你，谢苗，你呆在这里。"


translate chinese day4_main1_e79d0fd5:


    me "啊?"


translate chinese day4_main1_d124d112:


    mt "你是不是应该解释一下昨晚去哪里了啊？"


translate chinese day4_main1_f883183e:


    me "这个嘛..."


translate chinese day4_main1_597e6c44:


    "这我可是没有想到..."


translate chinese day4_main1_7a46a669:


    "是啊，我肯定会被发现的，早知道应该提前想好几个理由的。"


translate chinese day4_main1_538bd425:


    "但是我忘了..."


translate chinese day4_main1_8f6d4306:


    me "呃，我...{w}我和乌里扬娜在图书馆整理图书，可是她突然把我关在里面自己跑掉了！我只能在里边待到早晨！"


translate chinese day4_main1_a1c3fbc7:


    mt "我今天去了图书馆！{w}可是我没有看见你！"


translate chinese day4_main1_3f76b658:


    th "这我倒是知道。"


translate chinese day4_main1_c0f84c22:


    me "呃...{w}我是悄悄离开的。"


translate chinese day4_main1_41bc7385:


    mt "可是为什么乌里扬娜和我说了完全不同的故事？"


translate chinese day4_main1_2ff7837a:


    th "哇，那是什么故事..."


translate chinese day4_main1_2f2b2114:


    me "你了解她的嘛！"


translate chinese day4_main1_d27d2beb:


    mt "呃，你说的有道理..."


translate chinese day4_main1_d4764288:


    "辅导员停下了一会儿。"


translate chinese day4_main1_fe53b407:


    mt "只是别以为我就相信你了！"


translate chinese day4_main1_f9dd954e:


    "我没那么想..."


translate chinese day4_main1_18c37203:


    mt "好吧，我们等一下再算账，我可记着呢！{w}现在重要的是找到舒里克！"


translate chinese day4_main1_9513cd87:


    me "啊..."


translate chinese day4_main1_7d563950:


    mt "解散..."


translate chinese day4_main1_af8e37ef:


    "我又找不到位置了，我只能坐在阿丽夏和乌里扬娜的旁边。"


translate chinese day4_main1_ed08ccfd:


    us "请坐。"


translate chinese day4_main1_ebb45b6a:


    "她指着旁边的椅子。"


translate chinese day4_main1_04d910fd:


    "我坐了下来。"


translate chinese day4_main1_1a2c041d:


    "阿丽夏看起来和平常差不多，不像是要对我的爽约发脾气的样子。"


translate chinese day4_main1_36580956:


    th "呃，如果她都不想说的话，我为什么要说呢？"


translate chinese day4_main1_b7584acc:


    us "你不准备去拿你的饭吗？"


translate chinese day4_main1_97ba4a43:


    th "对了！{w}我都忘了这回事了。"


translate chinese day4_main1_e2f039bb:


    "今天的饭看起来和昨天的没什么区别，不过让我很有食欲。"


translate chinese day4_main1_ca4c8a18:


    "也许只是因为我太饿了。"


translate chinese day4_main1_f0e3f8e3:


    "或者是我想尽快吃完避免又一次受到乌里扬卡的恶作剧袭击。"


translate chinese day4_main1_15c24207:


    us "你今天想要和我们一起去海滩吗？"


translate chinese day4_main1_18dfa619:


    me "什么时候？"


translate chinese day4_main1_516609bc:


    us "就在早饭之后。"


translate chinese day4_main1_40ae9715:


    "本来我今天的计划不是要休息的，不过不去白不去啊，一边思考着我的答案一边晒着日光浴多好啊。"


translate chinese day4_main1_c1286eed:


    me "当然，白去谁不去..."


translate chinese day4_main1_a64a24e7:


    us "好耶！"


translate chinese day4_main1_b7bd4e35:


    "她友好的笑了起来。"


translate chinese day4_main1_2cb6f965:


    me "我打赌你又在计划下一个恶作剧了。"


translate chinese day4_main1_e4b476c4:


    us "不，完全没有！"


translate chinese day4_main1_dd0d143d:


    "乌里扬娜朝我摆摆手。"


translate chinese day4_main1_6d3417f3:


    dv "这是肯定的！"


translate chinese day4_main1_08dcf315:


    "阿丽夏奸笑着。"


translate chinese day4_main1_232ce05b:


    dv "某些人就是改不了..."


translate chinese day4_main1_27304fd6:


    "我认真的看着阿丽夏，想起来昨晚的事情。"


translate chinese day4_main1_a2cb95d1:


    "她奇怪的举止，还有我们的争吵。"


translate chinese day4_main1_35ddcb27:


    "不过现在她看起来和平常一样了。"


translate chinese day4_main1_c3ed033d:


    th "也许现在没必要和她说什么？"


translate chinese day4_main1_c2e8c7a9:


    "虽然本来我也没打算说。"


translate chinese day4_main1_b56982ca:


    "最后，昨晚的事情显得不再那么尴尬了，就像是一场梦。"


translate chinese day4_main1_6c304510:


    us "怎么会！"


translate chinese day4_main1_1ca23a9b:


    me "我觉得她说的有道理。"


translate chinese day4_main1_53340723:


    us "没有人相信我！"


translate chinese day4_main1_d1036c99:


    "乌里扬卡拿起她的空盘子准备离开。"


translate chinese day4_main1_0a9f12c1:


    us "慢慢吃吧！"


translate chinese day4_main1_39b14315:


    "她的话里充满了肯定——我今天在海滩吃不着什么甜头了。"


translate chinese day4_main1_f86ec2e1:


    "只剩下我和阿丽夏在这里。"


translate chinese day4_main1_37176700:


    me "话说，我觉得自己不应该和你一起走。"


translate chinese day4_main1_2d5beca1:


    dv "那是为什么？"


translate chinese day4_main1_8d93196b:


    me "那个，我有事情要做..."


translate chinese day4_main1_844c800d:


    dv "具体是什么事情呢？"


translate chinese day4_main1_d12f273c:


    "她看透了我的眼睛以至于我一时想不出好的主意。"


translate chinese day4_main1_6517fda2:


    me "我都没有泳装。"


translate chinese day4_main1_da8226ff:


    dv "试试我的。"


translate chinese day4_main1_e10064b1:


    me "你觉得有可能合身吗？"


translate chinese day4_main1_8d7ab4dd:


    dv "试试嘛！"


translate chinese day4_main1_1f304310:


    me "我才不要耽误那个功夫..."


translate chinese day4_main1_3479d749:


    dv "别像个废物！{w}我们会给你找到一些泳裤的！"


translate chinese day4_main1_54a8822f:


    "这些话只是让我更加害怕了。"


translate chinese day4_main1_0547c124:


    dv "在食堂等着我，马上就回来。"


translate chinese day4_main1_505d91ae:


    me "好..."


translate chinese day4_main1_850a4c6e:


    th "等着她也没什么问题吧，不是吗？"


translate chinese day4_main1_6488a41b:


    "吃完早饭后，我走出来，坐在台阶上。"


translate chinese day4_main1_52db742c:


    "少先队员们都有各自的事情，一个个匆匆走过。"


translate chinese day4_main1_6db745af:


    "没有人停下来。{w}甚至没有人往我这里看。"


translate chinese day4_main1_3594b82e:


    "他们似乎不把我当成是外人，而是正相反——是一个少年，他们的好同志。"


translate chinese day4_main1_15995c83:


    "我发现我在想到营地里的人们的时候又是不带什么防备，就像是第一天来这里时一样。"


translate chinese day4_main1_750c0871:


    "阿丽夏确实没过几分钟就回来了。"


translate chinese day4_main1_1d6a18c3:


    dv "准备好了吗？"


translate chinese day4_main1_eaccb1bc:


    me "准备好什么？"


translate chinese day4_main1_82448915:


    "她递给我泳裤...{w}虽然你很难把这个叫做泳裤..."


translate chinese day4_main1_d7b10247:


    "看起来像是装饰着蝴蝶和花瓣的短裤。{w}事实上，{i}就是{/i}短裤。"


translate chinese day4_main1_956c7f92:


    me "我斗胆问一句，你是从哪里搞到这些的？"


translate chinese day4_main1_6993170f:


    dv "害怕不敢穿上了？"


translate chinese day4_main1_009490c9:


    me "这个...{w}我可没有一点想要穿上的意思。"


translate chinese day4_main1_398d5637:


    "她的笑话很有意思，不过我可不想拿自己当笑柄。"


translate chinese day4_main1_f3133a95:


    dv "给我穿上！"


translate chinese day4_main1_8736d85c:


    "她粗暴的说道。"


translate chinese day4_main1_57346f05:


    me "你怎么不穿上试试？{w}我看这泳装的颜色可以让你眼花缭乱！"


translate chinese day4_main1_37b30d8c:


    dv "我们打个赌吧！"


translate chinese day4_main1_6a537964:


    "我没有接受的念头。"


translate chinese day4_main1_1498c76e:


    me "不了，谢谢。"


translate chinese day4_main1_b5ac4c3c:


    dv "好吧，你要么穿上这些泳裤，要么什么都别穿去游泳！"


translate chinese day4_main1_a6fdaa06:


    th "仔细衡量利弊之后，我觉得第二个选项甚至要更好一些。"


translate chinese day4_main1_1b8d0016:


    me "我哪里也不打算去！"


translate chinese day4_main1_b43b3d6a:


    dv "那我就要告诉大家你把这些短裤留在我的卧室！"


translate chinese day4_main1_66ba2af9:


    me "你为什么要那样做？"


translate chinese day4_main1_37160fae:


    dv "我怎么知道？"


translate chinese day4_main1_42be2b6f:


    "她一下子笑了起来。"


translate chinese day4_main1_9efe078e:


    "我不想和阿丽夏争吵，所以决定前往海滩。"


translate chinese day4_main1_14c26ca5:


    "不过不是穿着那些激情的短裤。"


translate chinese day4_main1_74372f49:


    me "我十分钟之内就到。"


translate chinese day4_main1_ea4299c9:


    dv "别迟到！"


translate chinese day4_main1_3dfe79f9:


    "她说完就跑开了。"


translate chinese day4_main1_77b838f6:


    "我跑回辅导员房间去拿一条毛巾，并且希望能找到类似泳裤的东西。"


translate chinese day4_main1_cdc7b7a7:


    "奥尔加·德米特里耶夫娜在房间里等着我。"


translate chinese day4_main1_044559b3:


    mt "谢苗，你听说了关于舒里克的事情了吗？"


translate chinese day4_main1_d45464b9:


    me "和半个小时之前一样..."


translate chinese day4_main1_edefb213:


    "我走向自己的床铺拿起一条毛巾。"


translate chinese day4_main1_d024b380:


    mt "你准备去哪里吗？"


translate chinese day4_main1_cb685aa8:


    me "是啊，去海滩。"


translate chinese day4_main1_850934a0:


    mt "等一下，你有泳裤吗？{w}我记得你好像什么行李都没带..."


translate chinese day4_main1_82f7a16c:


    "奇怪，她当时发现了这件事却没有显得多么惊讶。"


translate chinese day4_main1_5cb822ee:


    me "没..."


translate chinese day4_main1_f01befe8:


    mt "所以说你准备穿什么？"


translate chinese day4_main1_9da88e5d:


    th "好问题，穿什么呢？"


translate chinese day4_main1_7479b07e:


    me "不知道..."


translate chinese day4_main1_22a42eb2:


    mt "等一下。"


translate chinese day4_main1_81a73d46:


    "她走向衣柜，打开了一个抽屉。"


translate chinese day4_main1_b666fe0c:


    "很快她就找出了一个普通的黑色泳裤。"


translate chinese day4_main1_21d21de6:


    th "她从哪里搞到的？{w}更重要的是，为什么？"


translate chinese day4_main1_e922975f:


    "也许是以前的人们忘了带走吧..."


translate chinese day4_main1_b2cd5380:


    "想想这个夏令营的各种奇怪的事情，在奥尔加·德米特里耶夫娜的房间里找到男性服装也不是多么奇怪的事情。"


translate chinese day4_main1_3066598f:


    me "谢谢。"


translate chinese day4_main1_86258ed4:


    "泳裤穿起来很合适。"


translate chinese day4_main1_a20cefa7:


    "..."


translate chinese day4_main1_742830e4:


    "我在房间后边换好了短裤，前往海滩。"


translate chinese day4_main1_cf92b9ce:


    "这里已经聚集了很多少先队员们，不过我只认出了阿丽夏和乌里扬娜。"


translate chinese day4_main1_170351a5:


    us "快过来！"


translate chinese day4_main1_a0ef15ef:


    "我走向她们，坐在沙滩上。"


translate chinese day4_main1_27f5a44d:


    dv "看来你找到了更好的..."


translate chinese day4_main1_99336af1:


    "她看着我的泳裤假笑着。"


translate chinese day4_main1_5e648c4f:


    me "如你所见...."


translate chinese day4_main1_0eed83b2:


    us "去游泳吧！"


translate chinese day4_main1_d1aa6677:


    me "我不太想游。{w}等一会儿吧..."


translate chinese day4_main1_d3a86de3:


    "我不算是个游泳爱好者。"


translate chinese day4_main1_ba1e125e:


    dv "随你便。"


translate chinese day4_main1_a6eddd01:


    "两个女孩子跑进了水中。"


translate chinese day4_main1_521b9f2c:


    th "我为什么要来这里？{w}我为什么没有在寻找答案？"


translate chinese day4_main1_a50d7f70:


    th "我现在应该思考那些事情吗？"


translate chinese day4_main1_a482e6e3:


    "Совёнок看起来很普通。"


translate chinese day4_main1_b9b33704:


    "当然，我来到这里的三天半时间中发生了很多奇怪的事情，不过说起来，其中没有一件是超自然的。{w}尤其是我总是无法获得关于世界的线索。"


translate chinese day4_main1_cc830959:


    "相反，这里发生的一切只是让我更加困惑。"


translate chinese day4_main1_4795bad4:


    th "我还有什么选择？"


translate chinese day4_main1_662e06d0:


    th "我从这里的居民嘴里得不到任何有用的信息。{w}虽然我都怀疑他们知不知道..."


translate chinese day4_main1_4ed7892c:


    th "我应该离开这个地方吗？"


translate chinese day4_main1_8000702a:


    th "不过既然我都不知道自己在什么地方，我到底能走多远呢？"


translate chinese day4_main1_e75df2a1:


    th "结果我的唯一选择就是等待。"


translate chinese day4_main1_4f43f435:


    "过了一会儿女孩儿们回来了。{w}乌里扬娜手里拿着什么东西。"


translate chinese day4_main1_30f4be0c:


    us "看!"


translate chinese day4_main1_06938095:


    "我抬头看到了一只小龙虾。{w}一只普通的小龙虾。"


translate chinese day4_main1_456ae98b:


    "乌里扬卡躺在我的旁边开始折磨起它来。"


translate chinese day4_main1_dd41af79:


    me "放开那个可怜的小动物！"


translate chinese day4_main1_3b3d6cb9:


    us "为什么!{w}只是一只小龙虾啊！"


translate chinese day4_main1_c9e60657:


    me "那又怎么了？{w}它也有权力活着！"


translate chinese day4_main1_ea38a367:


    us "我要剥掉它的爪子然后请食堂的师傅煮做晚餐！"


translate chinese day4_main1_68e2c448:


    me "好像我们没有别的东西吃似的..."


translate chinese day4_main1_4a330faf:


    "我看着阿丽夏。{w}她似乎对乌里扬卡的恶趣味以及可怜的海洋小生命不太感兴趣。"


translate chinese day4_main1_411121e6:


    me "你告诉她！"


translate chinese day4_main1_86658590:


    dv "怎么了？{w}小龙虾就要接受这样的命运！"


translate chinese day4_main1_6a626905:


    "这两个孩子好像小学的自然课程都不怎么认真，而且对大自然毫无怜悯之心..."


translate chinese day4_main1_679b46e1:


    me "给我！"


translate chinese day4_main1_4ec958c5:


    "我从乌里扬卡的手里抢了过来。"


translate chinese day4_main1_4949cf90:


    us "真是的..."


translate chinese day4_main1_80d64677:


    "我有点惊讶她居然没有反抗。"


translate chinese day4_main1_56d514c5:


    "我看着这个小动物的眼睛。"


translate chinese day4_main1_c21fcd5d:


    "它们什么都不说，不过我想如果它们可以说话的话，肯定会非常愤怒，甚至要去世界人权大会去投诉。"


translate chinese day4_main1_70c26240:


    "虽然我很怀疑那会不会有好作用..."


translate chinese day4_main1_4595a707:


    "我把小龙虾带回了河边扔了回去。"


translate chinese day4_main1_53896b88:


    us "没关系，我还能再抓很多，河里有的是。"


translate chinese day4_main1_45927c46:


    "乌里扬卡说道。"


translate chinese day4_main1_972f86d7:


    me "那是..."


translate chinese day4_main1_ca01a7fa:


    "我嘀咕了几句。"


translate chinese day4_main1_b18fe1e2:


    "时间慢慢过去，我有点困了。"


translate chinese day4_main1_357544c9:


    "我睡着了。"


translate chinese day4_main1_0c650a7c:


    "我不记得自己梦到了什么，甚至究竟有没有做梦，只记得醒来的时候有人在摇晃我的肩膀。"


translate chinese day4_main1_3c1fbed3:


    "奥尔加·德米特里耶夫娜站在我的旁边。"


translate chinese day4_main1_517d89d0:


    me "你也来游泳了吗？"


translate chinese day4_main1_fd959960:


    "我没睡醒的问道。"


translate chinese day4_main1_da0111bc:


    mt "不。{w}已经快到了午饭时间，但是我们还是找不到舒里克。"


translate chinese day4_main1_82a4bab8:


    "辅导员站在我面前说道，身上还穿着一件湿透的泳衣。"


translate chinese day4_main1_f95c9fc9:


    me "还有呢？"


translate chinese day4_main1_1df88814:


    mt "我需要你去找他。"


translate chinese day4_main1_7cc521ca:


    me "好像我是整个夏令营里唯一的少先队员似的。"


translate chinese day4_main1_5581e203:


    "我很愤怒。"


translate chinese day4_main1_ccbda48f:


    "这件事越来越清楚了：奥尔加·德米特里耶夫娜让我当她的跑腿员，她的奴隶。{w}反过来也一样..."


translate chinese day4_main1_89e79303:


    mt "我来找你就是希望你帮我。"


translate chinese day4_main1_5aa2346a:


    "为什么一定需要是我呢？"


translate chinese day4_main1_aad225f0:


    "然而，经过了一些心理斗争之后，我同意了。"


translate chinese day4_main1_2186b207:


    "我的肩膀和后背在晒日光浴的时候已经快晒伤了，而且去找舒里克可以让我对营地的地形更加熟悉。"


translate chinese day4_main1_505d91ae_1:


    me "好吧..."


translate chinese day4_main1_b55d2be6:


    "穿着泳裤走来走去好像不太合适，我得先去换衣服。"


translate chinese day4_main1_a20cefa7_1:


    "..."


translate chinese day4_main1_97b8a867:


    "十分钟之后，我站在辅导员的房门外，思考着该去哪里。"


translate chinese day4_busstop_84a6a4dc:


    "车站应该是个好地方。"


translate chinese day4_busstop_310157f5:


    "我突然有了一点奇怪的想法：也许舒里克和我的处境差不多，也在寻找离开的方法，他可能也想坐410路离开。"


translate chinese day4_busstop_ae9e2dee:


    "这是有可能的，如果他也是{i}意外{/i}来到这里的。"


translate chinese day4_busstop_2832326d:


    "虽然这种情况的可能性很小。"


translate chinese day4_busstop_6d68af7d:


    th "不过谁知道呢？"


translate chinese day4_busstop_5a9376e0:


    th "万一公共汽车真的来了呢？..."


translate chinese day4_busstop_794a9280:


    "不过我不怎么相信..."


translate chinese day4_busstop_d2b67202:


    "我在公交车站等了几分钟，确定没有舒里克或者公交车来的迹象，之后又返回了营地。"


translate chinese day4_busstop_3f3e9168:


    "有人从大门后边冲了出来，撞到了我。{w}撞的不是很厉害，只是吓了我一跳。"


translate chinese day4_busstop_548da2a4:


    "未来站在我面前擦着她撞坏的额头。"


translate chinese day4_busstop_37374d4c:


    me "哇，抱歉..."


translate chinese day4_busstop_7459a96f:


    mi "没关系！是我的错！我想要去音乐社团，结果想一首歌想得出神...你知道，歌词啊，还有曲子，我没有意识到自己走到了这里。"


translate chinese day4_busstop_a9298d73:


    mi "所以说你不用道歉！"


translate chinese day4_busstop_03f805f3:


    "她的吐字速度严重超过了我的理解上限。"


translate chinese day4_busstop_513cdea5:


    "我尝试撤退。"


translate chinese day4_busstop_e170ad8d:


    me "好，好...得走了...如此这般..."


translate chinese day4_busstop_206044ed:


    mi "哇，等一下！"


translate chinese day4_busstop_6eecb271:


    "我想要像平常一样偷偷的离开，不过未来抓住了我的胳膊。"


translate chinese day4_busstop_71a0a5fc:


    "吓得我浑身一哆嗦。"


translate chinese day4_busstop_14425216:


    mi "能不能帮帮我呢？只是一点点的？"


translate chinese day4_busstop_80eb41cf:


    "这可不在我的日程之中。"


translate chinese day4_busstop_d90e19df:


    me "这个，我很乐意，不过..."


translate chinese day4_busstop_ffd83896:


    mi "由衷感谢！"


translate chinese day4_busstop_74c53f6a:


    "未来像一只小狗狗一样看着我让我心里发软。"


translate chinese day4_busstop_075057b4:


    "她没有要放手的意思。"


translate chinese day4_busstop_d6c51513:


    me "那么具体是如何帮忙呢？"


translate chinese day4_busstop_855dcd0b:


    mi "你要陪着我，我一个人写不出曲子来，我可以唱歌，演奏，但是无法同时做两件事。"


translate chinese day4_busstop_4c33c840:


    th "我们的大艺术家也有不行的时候啊。"


translate chinese day4_busstop_4942eb5b:


    me "这个，你知道，我什么乐器都不会..."


translate chinese day4_busstop_9f6720fa:


    mi "没关系的，我会教你的！咱们走！"


translate chinese day4_busstop_4ccac507:


    th "反过来说，我也没有什么损失。"


translate chinese day4_busstop_e7714637:


    "然而..."


translate chinese day4_busstop_e2956e63:


    me "我其实应该去找舒里克什么的..."


translate chinese day4_busstop_48f85b22:


    "未来失落的看着我。"


translate chinese day4_busstop_2554171f:


    mi "只要一下下..."


translate chinese day4_busstop_0d2840a0:


    "我不知道该说什么好..."


translate chinese day4_busstop_be6c9d1e:


    "她拉着我走了起来。"


translate chinese day4_busstop_59712563:


    th "也许这样没有好处，不过唯一摆脱她的方法就是甩开她的手。"


translate chinese day4_busstop_0dd57599:


    "但是那样不太礼貌。"


translate chinese day4_busstop_161f1f1a:


    th "反正没什么不好的。{w} 大概..."


translate chinese day4_busstop_82406965:


    "不到一分钟我们就站在了音乐社的门口。"


translate chinese day4_busstop_c75b785f:


    "未来找到了一个吉他。"


translate chinese day4_busstop_d8313998:


    mi "来看看！"


translate chinese day4_busstop_ddd216e1:


    "她坐下来开始演奏起来。"


translate chinese day4_busstop_62735169:


    "我尝试着看清她的指法。{w}旋律很简单。"


translate chinese day4_busstop_074f6095:


    "看起来不太难。"


translate chinese day4_busstop_d28a7a64:


    mi "看清了吗？"


translate chinese day4_busstop_b4dbe06d:


    me "有点。"


translate chinese day4_busstop_39da62e2:


    mi "咱们来试试。"


translate chinese day4_busstop_4bfbb8ba:


    "我拿起吉他开始演奏起来。"


translate chinese day4_busstop_b59ab969:


    "好像不太行得通..."


translate chinese day4_busstop_3865d2b8:


    mi "我再给你演示一次吧。"


translate chinese day4_busstop_045ce299:


    "她比我弹得好得多。"


translate chinese day4_busstop_4db3b5a0:


    "看着未来，我感到好奇。"


translate chinese day4_busstop_ac84947f:


    th "她确实唠叨个没完，有点天真还笨笨的...{w}不过她真的很有音乐天赋。"


translate chinese day4_busstop_1e78833e:


    mi "再试一次。"


translate chinese day4_busstop_dfcc8234:


    "我这次要比上一次好得多。"


translate chinese day4_busstop_763aacd2:


    mi "哇，这次好多了！"


translate chinese day4_busstop_d1046a49:


    "她笑了。"


translate chinese day4_busstop_a0871d04:


    "这并不是很难—实际上只要不断地重复..."


translate chinese day4_busstop_621a4f25:


    th "关键是——要跟上！"


translate chinese day4_busstop_20b61502:


    mi "听我的口令！"


translate chinese day4_busstop_35ec24e9:


    me "好..."


translate chinese day4_busstop_697d9fca:


    mi "准备好了吗？开始！"


translate chinese day4_busstop_52370984:


    "是一首日文歌曲。"


translate chinese day4_busstop_f1a32904:


    "说实话我一句也听不懂，不过未来唱的很好。{w}应该说是，很出色！"


translate chinese day4_busstop_0f7f3a55:


    "她每一个音符，每一句歌词都用心了。"


translate chinese day4_busstop_b6da10b3:


    th "是啊，也许音乐是值得她献出一辈子的东西。"


translate chinese day4_busstop_f7383791:


    "好像不是她选择了音乐，而是音乐选择了她..."


translate chinese day4_busstop_c487d162:


    "过去的这半个小时让我对未来的看法大为改变。"


translate chinese day4_busstop_a20cefa7:


    "..."


translate chinese day4_busstop_128ef1ec:


    mi "哇，谢谢！我的表演还行吗？终于能够和你合作一次了，我自己总是进行的不太好，不是弹错了弦就是唱错了歌词，和你一起非常完美！"


translate chinese day4_busstop_327d8e83:


    mi "你真的很有天赋喔~{w}没有任何准备就能弹成那样..."


translate chinese day4_busstop_5dbd8783:


    th "呃，看来我对她看法的改变有点太草率了。"


translate chinese day4_busstop_5b2f2663:


    me "感谢你的演出！{w}我得走了，回头见！"


translate chinese day4_busstop_df33f13a:


    mi "感谢..."


translate chinese day4_busstop_f1561010:


    "剩下的那部分话被我甩在了门后。"


translate chinese day4_busstop_a9ebca9c:


    "我靠在社团室的外墙上叹着气。"


translate chinese day4_busstop_ab0c1c58:


    "未来的歌声还在我的脑海中飘荡。"


translate chinese day4_forest_cfbff117:


    "显然奥尔加·德米特里耶夫娜和她的少先队员大部队已经找过了营地的每一个角落和缝隙。"


translate chinese day4_forest_5cc420a6:


    "漫游它的整个纵深..."


translate chinese day4_forest_305ab809:


    "所以大概已经没有去食堂或是沙滩的必要了。"


translate chinese day4_forest_bbfd297a:


    "或者是在机器人社团，那是他的第二个家！{w}或者甚至是他的第一个家。"


translate chinese day4_forest_b2a1e220:


    "那么周围的森林更值得我去搜索一番。"


translate chinese day4_forest_50fb2210:


    "我不想在森林里走的太深，不然这下就轮到他们来找我了。"


translate chinese day4_forest_919b405e:


    "我不怎么造访过乡村。"


translate chinese day4_forest_a12c2f29:


    "只是在小时候每个夏天我会到乡下的小屋住一段时间。{w}不过那也是离城市非常近的。"


translate chinese day4_forest_a605f85e:


    "不过在这个夏令营里面有各种我平时根本见不着的东西：环绕的森林，唱歌的鸟儿，清新的空气。"


translate chinese day4_forest_a278fb75:


    "我发现了一块儿草地，然后找到了一个树桩坐了下来。"


translate chinese day4_forest_6853b13e:


    th "这个地方真是恬静..."


translate chinese day4_forest_0617ce76:


    th "不过舒里克究竟跑到哪里去了？"


translate chinese day4_forest_b4222e21:


    "也许他是被别人绑架了也说不定。"


translate chinese day4_forest_ef624150:


    "这个夏令营一点都不正常，所以一两个少先队员的消失事件也没有那么奇怪。"


translate chinese day4_forest_d487dd90:


    th "一个有趣的假设——有没有可能是把我送到这里的那种力量或者类似的其他人做了这件事？"


translate chinese day4_forest_25b671a8:


    "我沉浸在自己的想法之中，没有发现面前的草丛有了些动静。"


translate chinese day4_forest_4e2bb113:


    "我仔细的看着，发现了一只松鼠。"


translate chinese day4_forest_fd1017fc:


    "它谨慎的靠近我，然后仔细的盯着我的手。"


translate chinese day4_forest_aa2cbac9:


    "也许有人以前经常在这个地方喂它们。"


translate chinese day4_forest_871d8c7f:


    me "抱歉，朋友，我可能什么都没有带..."


translate chinese day4_forest_94efe0a6:


    "当然松鼠没有听懂我的话，还是继续坐在那里等着我的奖赏。"


translate chinese day4_forest_5fbbd1e5:


    "我感到很愧疚，因为自己兜里一点面包渣什么的也没有。"


translate chinese day4_forest_3b611457:


    "我发现自己不好意思看着它的眼睛，决定动身离开。"


translate chinese day4_forest_a20cefa7:


    "..."


translate chinese day4_forest_97cf4b53:


    "又溜达了一会儿，我来到了水池旁边。"


translate chinese day4_forest_8563a8ed:


    th "结果他也不在森林里。{w}至少不在这附近的区域。"


translate chinese day4_forest_977ea43c:


    "而再往远处去又有点害怕。"


translate chinese day4_forest_150dfa24:


    "我走近水池，脱掉衬衫想要擦一擦身上，我已经热的湿透了。"


translate chinese day4_forest_cf3e2df3:


    "然而这并没有那么容易。"


translate chinese day4_forest_0fd38ed9:


    th "我没办法进到水池中去，而附近又没有大勺子什么的东西..."


translate chinese day4_forest_9ff095ed:


    "突然我听到背后有脚步声。{w}我转过身来。"


translate chinese day4_forest_fd05bbb8:


    "电子小子朝这边走来。"


translate chinese day4_forest_051049d1:


    el "你在找舒里克吗？"


translate chinese day4_forest_16055c7f:


    me "是的...{w}你呢？"


translate chinese day4_forest_d1214932:


    el "我也是..."


translate chinese day4_forest_959d6609:


    me "听着，你比我更加了解他，他有可能去什么地方？"


translate chinese day4_forest_1cbcc91e:


    el "我完全不知道。"


translate chinese day4_forest_2e34b08a:


    "他失落的回答道。"


translate chinese day4_forest_d074bc24:


    me "呃...{w}我不明白大家为什么都乱着急，他晚上的时候不是应该和你在一起吗，那么他一早晨应该也走不了太远，也许只是去散散步？"


translate chinese day4_forest_5def083f:


    el "你不了解舒里克！"


translate chinese day4_forest_668b3318:


    "电子小子激动的说道。"


translate chinese day4_forest_b25357aa:


    el "他疯狂的献身于他的工作，他的生活就是机械与控制。人们一点不在乎他！完全不在乎！我认为他有无限的可能！我看好他，他是一个铁人！"


translate chinese day4_forest_503848df:


    el "不，甚至是凯旋的英雄！"


translate chinese day4_forest_6b3cf294:


    "那一刻他看起来像是希特勒，在汹涌的人海前演讲。"


translate chinese day4_forest_fe5e3be9:


    "连他的手势都差不多。"


translate chinese day4_forest_ccf562b1:


    me "好吧...{w}所以呢？"


translate chinese day4_forest_b4f43170:


    el "“所以”？你不懂吧，啊？"


translate chinese day4_forest_5cb822ee:


    me "不..."


translate chinese day4_forest_37697e5c:


    el "他在我们的社团投入了全部！投入了一切！一切！"


translate chinese day4_forest_2c080d1a:


    me "所以说因为他不正常的表现感到担心吗？"


translate chinese day4_forest_acffd441:


    el "当然！"


translate chinese day4_forest_2ce86487:


    "看起来电子小子已经冷静下来了。"


translate chinese day4_forest_505d91ae:


    me "Okay..."


translate chinese day4_forest_2c5335a6:


    "他严肃的看着我。"


translate chinese day4_forest_3aee18b1:


    el "你要去洗洗脸吗？"


translate chinese day4_forest_b37f1cb4:


    me "大概...{w}稍微洗洗吧，天气很热。"


translate chinese day4_forest_4fa820b9:


    el "我也是。"


translate chinese day4_forest_2771ee33:


    "他看了看周围。"


translate chinese day4_forest_459c94ac:


    el "真希望附近能有一个水桶或者是大勺，能用来打水。"


translate chinese day4_forest_1403f59c:


    "是的我已经发现了。"


translate chinese day4_forest_01eb5767:


    el "那么开始吧..."


translate chinese day4_forest_fe274fdb:


    "他走向水池，打开了一个水龙头。"


translate chinese day4_forest_6341f283:


    "让我惊讶的是他没有让水直接流下去，而是沿着直角流出。"


translate chinese day4_forest_5dcfd900:


    "这一招可以用来洗洗身上。"


translate chinese day4_forest_0d1796ca:


    "同时电子小子脱掉了衬衫蹲了下去，看起来好像还脱掉了短裤。"


translate chinese day4_forest_746f00c8:


    "我看不清，因为水池正好可以挡住腰部以下的部分。"


translate chinese day4_forest_f589c698:


    "他把水引向自己身上，还开始唱歌。"


translate chinese day4_forest_aabf34aa:


    el "让我们打扫烟囱，打扫打扫..."


translate chinese day4_forest_76b7903e:


    "我惊讶的站着，不知道干什么好。"


translate chinese day4_forest_a7e62300:


    "他好像发现了这一点。"


translate chinese day4_forest_b0d91aa4:


    el "我洗完你也来洗一洗吧。"


translate chinese day4_forest_01fbaabb:


    "哇，我突然感到了解脱！"


translate chinese day4_forest_4c762e8b:


    me "这个...{w}我突然想起来有点儿事，先走啦！"


translate chinese day4_forest_c6ce9432:


    "在电子小子所有的古怪行为中，我刚刚看到的是最严重的。"


translate chinese day4_forest_b530b3f7:


    el "嘿！你怎么了？{w}这种热天洗一个冷水澡多舒服啊！"


translate chinese day4_forest_9308b010:


    me "不，不，不！{w}我不那么想！{w}不管怎么说，我得走了！"


translate chinese day4_forest_b9ecea82:


    "我穿上了衬衫匆匆返回了森林。"


translate chinese day4_forest_eedd04d7:


    th "我很奇怪他遇到了什么事。"


translate chinese day4_house_of_mt_ed200de8:


    th "不过这仍然不是个好主意。"


translate chinese day4_house_of_mt_3cdc1097:


    "如果舒里克藏在营地的某个地方，他现在应该已经被找到了（如果他希望被找到的话）。"


translate chinese day4_house_of_mt_90c7afe2:


    th "所以我不觉得自己能帮得了他。"


translate chinese day4_house_of_mt_afd17b4a:


    "怀着这个想法，我回到了房间，躺在了床上。"


translate chinese day4_house_of_mt_f20ad58c:


    th "如果奥尔加·德米特里耶夫娜发现我在这里可不会有好下场。"


translate chinese day4_house_of_mt_d712f4f6:


    th "算了，够了。我是要做颤抖的虫子还是拥有自己的权利？"


translate chinese day4_house_of_mt_befe5d34:


    "反正我什么也不想做。"


translate chinese day4_house_of_mt_2300b407:


    "今天和前些天一样热，我唯一需要干的事情就是躺在这里等着午饭。"


translate chinese day4_house_of_mt_43e095ca:


    "我正要睡着的时候有人敲门。"


translate chinese day4_house_of_mt_48555f41:


    me "请进。"


translate chinese day4_house_of_mt_482a0501:


    "斯拉维娅站在门口。"


translate chinese day4_house_of_mt_df14250c:


    sl "奥尔加·德米特里耶夫娜不在这里吗？"


translate chinese day4_house_of_mt_5a7a7577:


    me "不在。"


translate chinese day4_house_of_mt_63863ec8:


    sl "那么你在做什么？"


translate chinese day4_house_of_mt_1c3f46c0:


    "她怀疑的问我。"


translate chinese day4_house_of_mt_a57d59c9:


    "我看看自己，躺在床上，然后回答道："


translate chinese day4_house_of_mt_15d05c86:


    me "躺着..."


translate chinese day4_house_of_mt_f8b93764:


    sl "我能看得出来。{w}不过我听说奥尔加·德米特里耶夫娜让你去找舒里克。"


translate chinese day4_house_of_mt_ff471f9a:


    me "啊，是的..."


translate chinese day4_house_of_mt_e26abf96:


    sl "然后呢？"


translate chinese day4_house_of_mt_6715e0fb:


    me "重点是什么？{w}我确定她已经把整个营地翻个底朝天了。"


translate chinese day4_house_of_mt_e06cc00e:


    me "还没过多长时间。{w}咱们为什么要害怕呢？"


translate chinese day4_house_of_mt_04fc551f:


    sl "你知道，有可能会出事..."


translate chinese day4_house_of_mt_edee7f76:


    "斯拉维娅若有所思的看着我。"


translate chinese day4_house_of_mt_b68d7b34:


    sl "起来。"


translate chinese day4_house_of_mt_34c3a4c4:


    me "一定要这样做吗？"


translate chinese day4_house_of_mt_2551395c:


    "我太累了，以至于要去哪里的说法也能吓到我。"


translate chinese day4_house_of_mt_2bb8d573:


    sl "是的！"


translate chinese day4_house_of_mt_90cc9542:


    "斯拉维娅不是那种我能拒绝的了的人。"


translate chinese day4_house_of_mt_6cdf61dd:


    "我不情愿的站起来跟着她走了出去。"


translate chinese day4_house_of_mt_5aaa09bd:


    "我们在门口站了一会儿，享受着夏天的阳光。{w}虽然我要被烤熟了..."


translate chinese day4_house_of_mt_fca62f46:


    me "咱们要去哪里？"


translate chinese day4_house_of_mt_276df626:


    sl "咱们应该到处去看看。"


translate chinese day4_house_of_mt_57a26992:


    th "好主意...好。"


translate chinese day4_house_of_mt_018386dd:


    "我们的第一站是图书馆。"


translate chinese day4_house_of_mt_42ea0267:


    "斯拉维娅走向热妮娅的书桌，和她聊起来。"


translate chinese day4_house_of_mt_188600a4:


    "我在门口站着，我要尽量减少和图书管理员的交流。"


translate chinese day4_house_of_mt_eccb8e77:


    "过了一会儿斯拉维娅回来找我。"


translate chinese day4_house_of_mt_48013d82:


    me "所以说？"


translate chinese day4_house_of_mt_37eba565:


    sl "不行..."


translate chinese day4_house_of_mt_4c3d69ef:


    "她摇摇头。"


translate chinese day4_house_of_mt_e93ab54f:


    th "和我想得一样。"


translate chinese day4_house_of_mt_a20cefa7:


    "..."


translate chinese day4_house_of_mt_79e8cb2c:


    "食堂。{w}离午饭还有一段时间，所以食堂里还没有平常那么多人。"


translate chinese day4_house_of_mt_e2de9d7b:


    "整个建筑的内外都没有人。"


translate chinese day4_house_of_mt_84df38db:


    "当斯拉维娅和食堂的大厨谈话的时候我玩起了调料瓶。"


translate chinese day4_house_of_mt_ae189afc:


    "有一些盐洒了出来。"


translate chinese day4_house_of_mt_f8a8a439:


    "如果我迷信的话就要担心了。"


translate chinese day4_house_of_mt_31901972:


    "奇怪的是，他也不在这里。"


translate chinese day4_house_of_mt_387e3a7a:


    "她可能会意外的在什么地方发现他，比如说在冰箱里..."


translate chinese day4_house_of_mt_a20cefa7_1:


    "..."


translate chinese day4_house_of_mt_d5c5547a:


    "我们计划中的下一站是医务室。"


translate chinese day4_house_of_mt_60f69217:


    "我决定在外边等着斯拉维娅。"


translate chinese day4_house_of_mt_6b61c0c5:


    "没有结果。"


translate chinese day4_house_of_mt_c08b785a:


    "有少先队员在广场上踢着足球。"


translate chinese day4_house_of_mt_b4ae2b53:


    "舒里克在他们之中迷路的可能性不是很大..."


translate chinese day4_house_of_mt_b2c08c50:


    "最后我们来到了社团室。"


translate chinese day4_house_of_mt_38b1dea9:


    me "你觉得他可能在里边吗？我觉得大家这会是大家最先来寻找的地方..."


translate chinese day4_house_of_mt_7827a8db:


    sl "咱们进去看看吧。"


translate chinese day4_house_of_mt_40d2a09a:


    "空空如也。"


translate chinese day4_house_of_mt_2836519c:


    "斯拉维娅打开房门走进另外一个房间。"


translate chinese day4_house_of_mt_f73e0745:


    "我跟着她。"


translate chinese day4_house_of_mt_0ba05a1b:


    "这整个计划都很愚蠢。"


translate chinese day4_house_of_mt_a967e5b1:


    "而且很奇怪，尤其是当斯拉维娅提出这些建议。"


translate chinese day4_house_of_mt_9b88aabc:


    th "我是说，我知道我们有责任什么的...{w}不过他不是显然不在营地里吗？"


translate chinese day4_house_of_mt_13a0360f:


    th "再怎么说，他也没有和我们玩捉迷藏吧？"


translate chinese day4_house_of_mt_c1888233:


    sl "这里也没有人..."


translate chinese day4_house_of_mt_6e164047:


    me "你以为呢？{w}你觉得他会在衣柜里吗？"


translate chinese day4_house_of_mt_dfc5030f:


    sl "嘿!"


translate chinese day4_house_of_mt_6e0f81b2:


    "看来我好像让她生气了。"


translate chinese day4_house_of_mt_bc695e77:


    me "抱歉，抱歉...{w}不过说真的！"


translate chinese day4_house_of_mt_d2e028e4:


    sl "我知道...{w}不过咱们得排除所有的可能性..."


translate chinese day4_house_of_mt_1a2d3a52:


    me "好吧，听着，你实际上是怎么想的？"


translate chinese day4_house_of_mt_1829f925:


    sl "舒里克的失踪事件吗？"


translate chinese day4_house_of_mt_fc30fd2b:


    me "是啊。"


translate chinese day4_house_of_mt_68504b39:


    sl "也许他跑到森林里然后被精灵抓走了。"


translate chinese day4_house_of_mt_ec2c2a7c:


    "她笑着说道。"


translate chinese day4_house_of_mt_579bb939:


    me "真是一个神奇的故事啊..."


translate chinese day4_house_of_mt_adb6e4a6:


    th "不过在这个夏令营里什么不可能呢？"


translate chinese day4_house_of_mt_920eda59:


    sl "是哈...{w}不过现在可不是开玩笑的时间！"


translate chinese day4_house_of_mt_d7a9b9f5:


    me "别灰心！{w}咱们会找到他的！"


translate chinese day4_house_of_mt_4f31da71:


    sl "希望如此..."


translate chinese day4_house_of_mt_c17a722d:


    "斯拉维娅笑了。"


translate chinese day4_house_of_mt_551b3c25:


    sl "嗯，我还有一些事情。"


translate chinese day4_house_of_mt_038a986f:


    me "等会儿见。"


translate chinese day4_house_of_mt_432f59be:


    "她离开了，不过我又看了一会儿机器社团的的衣柜。"


translate chinese day4_house_of_mt_a20cefa7_2:


    "..."


translate chinese day4_house_of_mt_c6af67a5:


    "然而我被斯拉维娅的异想天开感染了，决定也去什么地方找一找。"


translate chinese day4_boathouse_9797429b:


    th "也许舒里克想要在水边捡石头。"


translate chinese day4_boathouse_140891ff:


    th "最糟糕的情况我可能会找到他的尸体..."


translate chinese day4_boathouse_639b643a:


    th "不过我确定肯定不会那样的！"


translate chinese day4_boathouse_ea7057f1:


    "当我穿过广场的时候有人叫住了我。"


translate chinese day4_boathouse_418a6303:


    dv "等一下！"


translate chinese day4_boathouse_3900d745:


    "阿丽夏走过来笑着。{w}我突然感觉到前方有陷阱。"


translate chinese day4_boathouse_2f227a4f:


    dv "你准备去哪里？"


translate chinese day4_boathouse_d6059f36:


    me "我在找舒里克...{w}奥尔加·德米特里耶夫娜要求我去的。"


translate chinese day4_boathouse_ce4ee092:


    dv "那么怎么样？一颗赛艇？"


translate chinese day4_boathouse_9d114d2d:


    "她盯着我的眼睛，我尴尬的扭过头去。"


translate chinese day4_boathouse_cba9cbf0:


    me "那倒没有...{w}不过你懂的，一个失踪的少先队员。"


translate chinese day4_boathouse_36e1c3b7:


    dv "你没有担心这么一件小事吧？"


translate chinese day4_boathouse_cb23542f:


    me "什么意思？"


translate chinese day4_boathouse_b0848313:


    dv "只过了几个小时。{w}也许他只是散步的时候忘记了时间..."


translate chinese day4_boathouse_da12543c:


    "当然我也有这个想法，不过我不想和她说。"


translate chinese day4_boathouse_f7f0bc77:


    me "是啊。{w}不过谁知道到底发生了什么呢..."


translate chinese day4_boathouse_64f4e64e:


    dv "让我帮帮你！"


translate chinese day4_boathouse_16a32560:


    me "唔...帮什么？"


translate chinese day4_boathouse_a3e48491:


    "我保持警惕。"


translate chinese day4_boathouse_01b2df0c:


    dv "帮你找到舒里克啊！"


translate chinese day4_boathouse_beae491d:


    me "哇，可是我没问题的..."


translate chinese day4_boathouse_55688a4e:


    dv "哎呀，来吧"


translate chinese day4_boathouse_f0fb482a:


    "她对我笑着，不过这笑容里肯定有鬼..."


translate chinese day4_boathouse_cd88bad5:


    "本来她的笑容很可爱，不过我总觉得里边藏着什么。"


translate chinese day4_boathouse_b88b8d82:


    me "好吧，如果你一定要坚持的话..."


translate chinese day4_boathouse_4e8dafb2:


    "因为我并不了解她究竟有什么阴谋所以没有什么实质性的理由来拒绝。"


translate chinese day4_boathouse_b5f16424:


    dv "不过走之前我需要回家找点东西。"


translate chinese day4_boathouse_12c4ee4b:


    me "好吧，我等着你。"


translate chinese day4_boathouse_62de1363:


    dv "你在那里站着干什么？{w}我们一起走吧！"


translate chinese day4_boathouse_505d91ae:


    me "好吧..."


translate chinese day4_boathouse_a2980b83:


    "我们来到了阿丽夏的房间。"


translate chinese day4_boathouse_7aa63adb:


    "看起来和其他的房间没什么两样，如果不算上门上的海盗旗的话。"


translate chinese day4_boathouse_e4591d9e:


    dv "顺便说，乌里扬卡是我的室友。"


translate chinese day4_boathouse_53a56040:


    me "OK..."


translate chinese day4_boathouse_36881cd4:


    "我们走进了房间。"


translate chinese day4_boathouse_983dd462:


    "里边的一片混沌让我想起了自己的旧房间。"


translate chinese day4_boathouse_0d66d2a4:


    th "不过我为什么要加上“旧”..."


translate chinese day4_boathouse_225e52c0:


    "总的来说我想象中的女生房间是不一样的：雪白的床单，墙壁，地板和房顶熠熠生辉，洁白无暇。"


translate chinese day4_boathouse_cd30d970:


    th "虽然是想想两个最“模范”的少先队员住在这里..."


translate chinese day4_boathouse_dab5031c:


    "我们沉默的站了一会儿。"


translate chinese day4_boathouse_f744c43e:


    me "你需要拿什么？"


translate chinese day4_boathouse_765cf23a:


    dv "哈?"


translate chinese day4_boathouse_fe1ee079:


    "看起来我打断了阿丽夏的思考。"


translate chinese day4_boathouse_72b1039b:


    dv "啊...{w}事实上不在这里...{w}等一下，我马上回来。"


translate chinese day4_boathouse_0049a2fb:


    "她笑着走了出去。"


translate chinese day4_boathouse_ebc67191:


    th "她今天看起来很不靠谱。"


translate chinese day4_boathouse_7fb057e3:


    "我开始检查整个房间。"


translate chinese day4_boathouse_5dfa3fd6:


    "不知道这里是不是经历过地毯式轰炸还是什么的，总之是一片狼藉。"


translate chinese day4_boathouse_49172fa3:


    th "不过看看这一个少先队员搞出来的糟糕场面，我可以确定自己不是世界上最邋遢的人。"


translate chinese day4_boathouse_b3f9ad2c:


    "我的公寓可是花了几年的时间装修好的！"


translate chinese day4_boathouse_1f000f06:


    "我没有特别的想什么事情，只是向房间的四周看去。"


translate chinese day4_boathouse_76afe60b:


    "苏联艺术海报，书架上有一些书，还有各种居家小工具..."


translate chinese day4_boathouse_76d3f20d:


    "然后我突然意识到事情不对劲！"


translate chinese day4_boathouse_8a607983:


    th "有什么事情要发生了！{w}不好的事情！"


translate chinese day4_boathouse_c1ef0499:


    th "阿丽夏为什么把我自己丢在这里？"


translate chinese day4_boathouse_3eb661ac:


    "有些事情我必须弄明白。"


translate chinese day4_boathouse_9e3a46df:


    "我的第六感告诉我呆在这里很危险。"


translate chinese day4_boathouse_f00b42b5:


    "我回到门口想要把门打开。"


translate chinese day4_boathouse_531d7dd3:


    "锁住了！"


translate chinese day4_boathouse_f67a6003:


    th "真是惊人的发现！{w}她是怎么悄悄的把我锁在里边的？"


translate chinese day4_boathouse_de1fdea1:


    th "我不知道她在想什么，不过我得赶紧离开这个地方！"


translate chinese day4_boathouse_dee27299:


    "我打开一扇窗户，跳了出去。"


translate chinese day4_boathouse_0ed51470:


    th "现在我可以悄悄的离开或者..."


translate chinese day4_boathouse_b69f8c48:


    "我选择了第二个选项，继续等待。"


translate chinese day4_boathouse_e924afbe:


    th "她为什么要把我锁起来？"


translate chinese day4_boathouse_7b372665:


    "我想知道到底发生了什么事，而且还想看看阿丽夏发现屋里没有人的时候的表情。"


translate chinese day4_boathouse_4c2291c3:


    "我躲在房子旁边的草丛中等着。"


translate chinese day4_boathouse_7eae97a5:


    "过了一会儿我听到了脚步声。"


translate chinese day4_boathouse_009f6978:


    "阿丽夏和奥尔加·德米特里耶夫娜一起走来。"


translate chinese day4_boathouse_f9acd723:


    dv "你会亲自看到一切的！"


translate chinese day4_boathouse_bc262665:


    "她打开门走了进去..."


translate chinese day4_boathouse_cc2a5873:


    "过了几秒钟她跑了出来。"


translate chinese day4_boathouse_813cd9cc:


    dv "那个，我..."


translate chinese day4_boathouse_8b502027:


    "阿丽夏的表情好像是刚刚赢得了一场抓刺猬的比赛正准备吹嘘，却发现这并不是一个奥林匹克项目。"


translate chinese day4_boathouse_4195d1ac:


    mt "所以说他消失了？"


translate chinese day4_boathouse_962ac463:


    "奥尔加·德米特里耶夫娜怀疑的问道。"


translate chinese day4_boathouse_5460bb59:


    th "我跳出来之后关上窗户是对的！"


translate chinese day4_boathouse_2178d069:


    dv "那个...{w}你懂的..."


translate chinese day4_boathouse_3f9c9a9a:


    mt "我都知道，朵切芙斯卡娅！"


translate chinese day4_boathouse_fdf3425b:


    mt "你和你的室友，每次都是这样！你们还没玩够？！"


translate chinese day4_boathouse_38429b64:


    dv "但是那是真的！"


translate chinese day4_boathouse_b29a5db5:


    mt "真的？信口开河，没完没了！谢苗在你的房间，谢苗骚扰你，你为什么撒谎？这很糟糕！"


translate chinese day4_boathouse_a48fe9fa:


    "阿丽夏看起来很失落，而且奇怪的是并没有尝试辩解。"


translate chinese day4_boathouse_81dede20:


    "一方面说看起来很有意思，不过另一方面我也为朵切芙斯卡娅感到不好意思。"


translate chinese day4_boathouse_0e804b76:


    "不过这倒是她自找的..."


translate chinese day4_boathouse_4d1f6fb4:


    "辅导员同志训斥完之后就走了。"


translate chinese day4_boathouse_2ef1d6a7:


    "阿丽夏很愤怒。{w}她握紧拳头，脸色通红，看起来要爆炸了。"


translate chinese day4_boathouse_2eb4d7e1:


    "我坐在草丛里悄悄的笑着。"


translate chinese day4_boathouse_7a53a4ae:


    "然而不想知道她是怎么想的，于是从藏身处走了出来，不害怕被揍。"


translate chinese day4_boathouse_c7546dc1:


    me "所以说你到底想和辅导员展示什么？{w}我？"


translate chinese day4_boathouse_d6196049:


    "阿丽夏转过身来惊讶的看着我。"


translate chinese day4_boathouse_60e7b002:


    "过了一会儿，惊讶变成了愤怒。"


translate chinese day4_boathouse_b7d8bf72:


    dv "你！你！"


translate chinese day4_boathouse_8f37a6c9:


    me "我怎么了？"


translate chinese day4_boathouse_f834c3f8:


    "她稍微冷静下来一些。"


translate chinese day4_boathouse_12133f6d:


    dv "我想和你扯平。"


translate chinese day4_boathouse_f0d6d1c8:


    "她咬咬牙。"


translate chinese day4_boathouse_21cbf526:


    me "什么意思？"


translate chinese day4_boathouse_e8988316:


    dv "你打赌输了。{w}所以我要揭穿你跟踪我的事情。"


translate chinese day4_boathouse_0fc6db81:


    th "我倒是很想看看奥尔加·德米特里耶夫娜发现这种场景的时候会怎么处理，不过咱们还是跳过这个话题吧..."


translate chinese day4_boathouse_0686c1b4:


    dv "我要报仇！"


translate chinese day4_boathouse_16b585f8:


    me "为什么？"


translate chinese day4_boathouse_f3add060:


    th "说真的，我对这个女孩做了什么值得报仇的事情？"


translate chinese day4_boathouse_fa0ca1ce:


    dv "你玩牌赢了我！"


translate chinese day4_boathouse_b0dcc84f:


    th "真是一个好理由！{w}说得好..."


translate chinese day4_boathouse_f2e007f0:


    me "我懂了..."


translate chinese day4_boathouse_b6a65aa2:


    dv "因为你输了！"


translate chinese day4_boathouse_32974cc7:


    me "那为什么？"


translate chinese day4_boathouse_15bab5bb:


    dv "因为你不敢和我比赛！"


translate chinese day4_boathouse_3c1b245e:


    me "什么时候？"


translate chinese day4_boathouse_ef0158a3:


    dv "我们玩牌的时候！"


translate chinese day4_boathouse_9d2033ff:


    "真是一个严肃的理由啊..."


translate chinese day4_boathouse_c63ec5a4:


    me "是啊..."


translate chinese day4_boathouse_57d2d760:


    me "也许我应该向你道歉？"


translate chinese day4_boathouse_81684e59:


    "我讽刺道。"


translate chinese day4_boathouse_0eebc5d9:


    dv "少废话！"


translate chinese day4_boathouse_c0d6d356:


    "她走进房间，甩上了门。"


translate chinese day4_boathouse_5e595186:


    "我对阿丽夏不生气。"


translate chinese day4_boathouse_663b2604:


    "我希望是这种情况。"


translate chinese day4_boathouse_d12d5f50:


    "总之，我没有遭到什么报应。"


translate chinese day4_boathouse_2c46efd4:


    "我很幸运——她的尴尬处境让我很快活。"


translate chinese day4_boathouse_5030ce16:


    "嘿嘿，我离开了差点成为我的死亡之谷的阿丽夏的宿舍。"


translate chinese day4_main2_a20cefa7:


    "..."


translate chinese day4_main2_48d0d4e6:


    "我怀着复杂的心情去吃午饭——既有成就感，又有浪费时间的空虚。"


translate chinese day4_main2_4cb70312:


    "食堂人满为患。"


translate chinese day4_main2_58e949cc:


    "我无处可藏，奥尔加·德米特里耶夫娜叫了我。"


translate chinese day4_main2_a4d48910:


    mt "谢苗，跟我们来。"


translate chinese day4_main2_08ad4dba:


    "辅导员，斯拉维娅，还有电子小子围坐在一个四人桌旁。"


translate chinese day4_main2_2acc25a8:


    "我点点头，去取自己的午餐。"


translate chinese day4_main2_523ebbc0:


    "这次我得排一会儿队。"


translate chinese day4_main2_c6c2e073:


    "今天的菜单没有多大变化。{w}至少菜什么的看起来是一样的。"


translate chinese day4_main2_05eb9d8b:


    "等我们都坐下，说完开动了之后，奥尔加·德米特里耶夫娜说道："


translate chinese day4_main2_b397d74c:


    mt "所以说，你在想什么？"


translate chinese day4_main2_c233b6a2:


    me "什么想什么？"


translate chinese day4_main2_67341092:


    mt "我们找舒里克找得焦头烂额，{w}但是到了中午还是毫无线索。"


translate chinese day4_main2_3d1cf378:


    "我注意到这句子好像押韵，不过并不想指出来。"


translate chinese day4_main2_5f3e793a:


    sl "我们找过整个夏令营营地。"


translate chinese day4_main2_46ae85f2:


    el "我找过了附近整片的森林。"


translate chinese day4_main2_cefc6cd9:


    "奥尔加·德米特里耶夫娜看着我。"


translate chinese day4_main2_f0777904:


    me "我也...{w}帮了忙。"


translate chinese day4_main2_ad33fcd7:


    mt "我们要叫警察！"


translate chinese day4_main2_cc3b3354:


    me "也许咱们可以等到晚上？"


translate chinese day4_main2_26be9189:


    "我懒洋洋的问道。"


translate chinese day4_main2_d859378c:


    me "也许他回家了..."


translate chinese day4_main2_29941732:


    mt "那不可能！{w}他家有几千公里远。"


translate chinese day4_main2_51a52fb5:


    me "坐火车？"


translate chinese day4_main2_932e3a79:


    mt "最近的车站..."


translate chinese day4_main2_b7d1c016:


    "她停顿了一下。"


translate chinese day4_main2_61df2540:


    mt "也很远..."


translate chinese day4_main2_35d07915:


    "现在事情越来越有趣了。"


translate chinese day4_main2_3c635a17:


    th "每次谈到我如何离开夏令营的时候这里的人们都会转移话题。"


translate chinese day4_main2_91bfb245:


    me "多远是“远”？"


translate chinese day4_main2_a63beaef:


    mt "很远。"


translate chinese day4_main2_f3bfc63b:


    "辅导员看着我，用眼神示意我禁止提出更多问题。"


translate chinese day4_main2_12bcc6fb:


    sl "所以咱们应该往森林深处走！也许他迷路了。"


translate chinese day4_main2_775f2318:


    el "舒里克一直带着指南针。"


translate chinese day4_main2_3c97ead2:


    "电子小子插话说。"


translate chinese day4_main2_df6e5065:


    "我很好奇在他的魔法背心里还会有什么东西?"


translate chinese day4_main2_feb702c4:


    "如果我在森林深处迷了路，一个指南针可是毫无意义..."


translate chinese day4_main2_0ff502ce:


    mt "警察！{w}我们应该在晚上呼叫警察！"


translate chinese day4_main2_35192e6e:


    th "至少不是现在。"


translate chinese day4_main2_af1d0e2a:


    me "好吧，晚上..."


translate chinese day4_main2_ec9674fa:


    "他们都沉默了。"


translate chinese day4_main2_a0920c3c:


    me "咱们应该在晚上之前可以找到他，还有时间。"


translate chinese day4_main2_97d66c28:


    mt "如果他真的迷路了，咱们可不能再浪费时间了！"


translate chinese day4_main2_24f8e21c:


    sl "咱们无法确定这件事情。"


translate chinese day4_main2_dea475da:


    mt "那么他在哪里？哪里？？"


translate chinese day4_main2_515c8c9d:


    "辅导员说的话有一定的道理，在不知道什么地方躲一天很可疑。"


translate chinese day4_main2_5f25be5b:


    "他那么做有什么理由啊。{w}舒里克给我的感觉是一个严谨的少先队员，乌里扬卡倒是有可能干出那种事情来。"


translate chinese day4_main2_2ddaebf2:


    "所以有理由相信他已经走了。"


translate chinese day4_main2_593ca1bb:


    me "所有能做的事情都已经做了，现在咱们只能等着了。"


translate chinese day4_main2_56f14e0a:


    "斯拉维娅，电子小子和辅导员悲痛的看着我，但是没有说出什么。"


translate chinese day4_main2_a20cefa7_1:


    "..."


translate chinese day4_main2_6aa774a1:


    "我吃完午饭交回了餐盘，然后离开了食堂。"


translate chinese day4_main2_5abbd6f2:


    th "今天才只过了一半而已..."


translate chinese day4_main2_11e5ce88:


    th "现在呢？"


translate chinese day4_main2_19b6f21b:


    "到了午睡时间了。"


translate chinese day4_main2_99bbc067:


    "只有Genda通过他的眼睛看着我。"


translate chinese day4_main2_ab4e08c6:


    "当然他在盯着别的什么地方，不过我总有一种感觉，他好像时不时的在看我。"


translate chinese day4_main2_5f2f4e3e:


    th "我打赌他知道舒里克在什么地方，{w}只是他什么也说不出来..."


translate chinese day4_main2_a3a27538:


    "机器人社团的领导人失踪让我想到，"


translate chinese day4_main2_219aa2d9:


    th "也许这件事和我有什么关系？"


translate chinese day4_main2_2cf5c012:


    cs "噢，少先队员！"


translate chinese day4_main2_08272ac1:


    "护士站在我面前。"


translate chinese day4_main2_4a52b166:


    "我好奇的看着她。"


translate chinese day4_main2_d7304160:


    cs "来医务室代替我的位置，{w}有人受伤了，我需要紧急出诊。"


translate chinese day4_main2_de5dd677:


    me "我？"


translate chinese day4_main2_dacf7660:


    cs "是的，你！{w}拿着钥匙！"


translate chinese day4_main2_f30d0d14:


    "护士小姐把钥匙扔给了我就跑开了。"


translate chinese day4_main2_469e7359:


    th "为什么是我？{w}夏令营里就没有别的人了？还有我具体应该做什么呀？出了事怎么办？"


translate chinese day4_main2_7d9ad10a:


    th "我现在该怎么办，我错过了拒绝的机会..."


translate chinese day4_main2_ab1de823:


    "我在门前犹豫着。"


translate chinese day4_main2_c422bd2d:


    th "一方面可能我没有什么可担心的，可能我在里边待了一会儿她就回来了。"


translate chinese day4_main2_6e8191b6:


    th "可是如果有人需要救治怎么办？{w}腿折了？或者头破了？"


translate chinese day4_main2_78c21f2c:


    "我开始担心很多事情。"


translate chinese day4_main2_72b4eb4d:


    "我希望营地里不要出现比肿个包再严重的伤。"


translate chinese day4_main2_65e07906:


    "不过同时我确定一旦真的出了事儿，自己完全排不上用场。"


translate chinese day4_main2_1e5b77ba:


    th "我连怎么做心肺复苏都不知道。"


translate chinese day4_main2_d02d8b17:


    "桌子上的一份杂志吸引了我的注意。"


translate chinese day4_main2_b5804212:


    th "打发时间的好方法，{w}大概..."


translate chinese day4_main2_ec4a19eb:


    "上面写着“苏联时尚”"


translate chinese day4_main2_e9159fbe:


    "出版日期不见了。"


translate chinese day4_main2_726e419d:


    "不过这不奇怪，这里发生了各种奇怪的事情，也许苏联的杂志就是不显示日期也说不定。"


translate chinese day4_main2_21eab677:


    "油亮的书页上过时的苏联模特看着我。"


translate chinese day4_main2_bb76f93b:


    th "现在没有人穿这样的衣服了。"


translate chinese day4_main2_a026df63:


    "我笑了笑。"


translate chinese day4_main2_f5d07ae7:


    th "我很好奇斯拉维娅会不会觉得这个很时尚。"


translate chinese day4_main2_2453e3b6:


    th "我只能想象她穿着这个出现在我的时代会是怎样的。"


translate chinese day4_main2_864530e1:


    th "想象一下我们俩在大街上手牵手，我穿着带兜帽的大衣，她穿着带花边的贵族长裙。"


translate chinese day4_main2_e7482e77:


    "好像我已经开始想象斯拉维娅在我的世界了...{w}和我在一起...{w}不只是她自己。"


translate chinese day4_main2_19330451:


    th "这件裙子乌里扬娜穿起来应该很合适，这件无袖衫挺适合阿丽夏，这件开襟毛衣和短裙搭配在列娜身上会很好看。"


translate chinese day4_main2_acf18888:


    "要是真的多好..."


translate chinese day4_main2_45c2a3ea:


    "不对，我能看见她们，还能听到，触碰到她们。"


translate chinese day4_main2_b793115e:


    th "可是她们在这里，而我...{w}我并不属于这个地方。"


translate chinese day4_main2_c7ecb5e1:


    th "这里就像是外星球，{w}我只想找个机会逃跑。"


translate chinese day4_main2_34ecfe30:


    th "我只能等着，因为没有什么取决于我..."


translate chinese day4_main2_a734d9d3:


    "我叹叹气，枕着桌子睡着了。"


translate chinese day4_main2_a20cefa7_2:


    "..."


translate chinese day4_main2_ad69b965:


    "我被开门的声音吵醒了。"


translate chinese day4_main2_26171c75:


    "列娜站在门口。"


translate chinese day4_main2_aa82a2c6:


    th "昨天我答应帮她处理医务室的事情来着..."


translate chinese day4_main2_6bb5c0bf:


    "我的脸很快便红了，我转过头假装自己很忙。"


translate chinese day4_main2_fbb88586:


    un "护士不在这里吗？{w}那我等一下再来..."


translate chinese day4_main2_37395644:


    me "我在替她值班！"


translate chinese day4_main2_d87b9ec9:


    th "我既然在这个职位上就要负责到底。"


translate chinese day4_main2_7b38c5ca:


    "虽然我害怕在我的领导下出什么问题。"


translate chinese day4_main2_06a1509d:


    me "有哪里不舒服吗？"


translate chinese day4_main2_4c00f79b:


    "我尝试着做一个最职业的微笑让她不要慌。"


translate chinese day4_main2_69f94ecc:


    un "没什么...{w}只是有点头疼。"


translate chinese day4_main2_2357f708:


    me "看我的！"


translate chinese day4_main2_27d0cf32:


    th "可能需要一些止疼药..."


translate chinese day4_main2_f9569158:


    "我当然不知道应该到哪里去找所以迟迟翻不出来。"


translate chinese day4_main2_649ec83c:


    me "终于！"


translate chinese day4_main2_64f21126:


    "我递给列娜一板安乃近。"


translate chinese day4_main2_082d3571:


    un "谢谢..."


translate chinese day4_main2_d1046a49:


    "她笑了。"


translate chinese day4_main2_38a347b1:


    "我完全没有想到，我失去了现实感，看着她。"


translate chinese day4_main2_3875ba7b:


    un "什么?"


translate chinese day4_main2_cb318c1b:


    "列娜笨拙的转过来。"


translate chinese day4_main2_bec7dcec:


    me "那个，我有个问题，你喜欢这个吗？"


translate chinese day4_main2_6cbc07c6:


    "我不知道自己在想什么，不过还是拿起来那本杂志，指着封面上的开襟毛衣和短裙，我觉得这个很适合列娜的来着。"


translate chinese day4_main2_f9690e49:


    "也许我完全发疯了，想象着这些女孩子们在我的世界里。"


translate chinese day4_main2_50cb5ce9:


    "或者我想要在等着护士的时候消磨时间。"


translate chinese day4_main2_1523dd90:


    "列娜看着那幅图片。"


translate chinese day4_main2_1aa04f80:


    un "还好吧..."


translate chinese day4_main2_bc64bbfd:


    me "这种风格是目前流行的吗？"


translate chinese day4_main2_3c68ea6c:


    un "大概是的..."


translate chinese day4_main2_340ec68e:


    "她有些疑惑，脸有些发红。"


translate chinese day4_main2_9812399d:


    un "为什么问这个呢？"


translate chinese day4_main2_59e7c056:


    th "说真的，为什么呢..."


translate chinese day4_main2_082d3571_1:


    un "谢谢..."


translate chinese day4_main2_dcd2e3df:


    me "不是因为这个，我说的完全是实话！"


translate chinese day4_main2_47092547:


    un "..."


translate chinese day4_main2_c4d1a45e:


    "我们沉默了一段时间。"


translate chinese day4_main2_a417690d:


    me "你的头痛怎么样了？"


translate chinese day4_main2_c1c02f2e:


    un "好多了，感谢。"


translate chinese day4_main2_d1046a49_1:


    "她笑了。"


translate chinese day4_main2_a17b8e3e:


    un "我先走了。"


translate chinese day4_main2_3fc88750:


    me "祝你好运！"


translate chinese day4_main2_7e83005b:


    "列娜离开了，我继续浏览杂志。"


translate chinese day4_main2_a20cefa7_3:


    "..."


translate chinese day4_main2_500f0017:


    "过了一会儿，又有人敲门了。"


translate chinese day4_main2_22afe73c:


    th "看起来医务室还是最热闹的地方。"


translate chinese day4_main2_b588274b:


    "突然我想扮演一下护士的角色...{w}不过是男版的...{w}然后说道："


translate chinese day4_main2_6b33d385:


    me "请进！"


translate chinese day4_main2_573b3dff:


    "门打开了，乌里扬卡走了进来。"


translate chinese day4_main2_5536cc92:


    me "哇！你什么时候学会敲门了？"


translate chinese day4_main2_9ef7bfe7:


    us "你有什么问题吗？"


translate chinese day4_main2_d6ea162b:


    "她皱起了眉头。"


translate chinese day4_main2_396150e8:


    me "哪里疼？"


translate chinese day4_main2_cd1dfac0:


    us "我为什么要告诉你啊？{w}护士在哪里？"


translate chinese day4_main2_7611a63b:


    me "就在你面前。"


translate chinese day4_main2_d2429dec:


    "我自豪的叠起腿，诧异的看着她。"


translate chinese day4_main2_9881e8d1:


    us "那我该走了，{w}就算死掉也比被你治疗好得多。"


translate chinese day4_main2_7f6b716b:


    "她淘气的笑着。"


translate chinese day4_main2_5ae0e9e5:


    me "你都没有让我试一试！"


translate chinese day4_main2_2ccca5b3:


    "乌里扬娜想了一会儿。"


translate chinese day4_main2_2348abed:


    us "不过你可以给我开一些药。"


translate chinese day4_main2_fdf90b79:


    me "你有什么问题？"


translate chinese day4_main2_aa62d7ce:


    "她过了半天才回答。"


translate chinese day4_main2_f32986c4:


    us "肚子很重..."


translate chinese day4_main2_150675b6:


    me "你确定不是脑子很空吗？"


translate chinese day4_main2_3e1d421a:


    "我嘟囔着。"


translate chinese day4_main2_76d30161:


    us "你说什么？"


translate chinese day4_main2_f6290a5b:


    me "没什么，等一下。"


translate chinese day4_main2_f0d4eaf6:


    "我在第一个抽屉里就找到了那些药。"


translate chinese day4_main2_3ae2d123:


    us "谢谢你，医生！"


translate chinese day4_main2_d2b56c39:


    "她开心的笑着。"


translate chinese day4_main2_45c9bd9f:


    "看着乌里扬卡我很奇怪这样乐观积极的孩子怎么会有健康问题。"


translate chinese day4_main2_db45ba49:


    "她凶恶的看着我。"


translate chinese day4_main2_f3f3ce8a:


    us "不，都留给你！"


translate chinese day4_main2_4323f27c:


    "乌里扬卡跑出了医务室，摔上了门。"


translate chinese day4_main2_bd0d0f91:


    "也许我不应该那样说。"


translate chinese day4_main2_4196aef5:


    me "鉴于本地的烹饪水平，这并不奇怪。"


translate chinese day4_main2_f2c43065:


    us "也许 ..."


translate chinese day4_main2_f0d6d1c8:


    "她傻笑着。"


translate chinese day4_main2_5c8b5c7d:


    us "我看你还活蹦乱跳的啊！"


translate chinese day4_main2_6e98e9f8:


    me "我没抱怨..."


translate chinese day4_main2_14f237e5:


    us "再见！"


translate chinese day4_main2_4323f27c_1:


    "乌里扬卡跑出了医务室，摔上了门。"


translate chinese day4_main2_a20cefa7_4:


    "..."


translate chinese day4_main2_dbbcc16f:


    "我又一次沉迷在苏联的时尚潮流中。"


translate chinese day4_main2_a3a786a7:


    "过了好久，护士还是没有回来。"


translate chinese day4_main2_5db604a8:


    "我没有在找舒里克，没有寻找关于自己的线索...{w}我只是坐在这里拿着一本杂志翻来翻去。"


translate chinese day4_main2_900a2743:


    "我还一度觉得这种感觉很好..."


translate chinese day4_main2_f82c0a74:


    th "到目前为止，这里的一切都可以看作是在夏令营的度假，不过如果再过很久还是没有变化，我就有理由担心了。"


translate chinese day4_main2_cbdcd68a:


    "又有什么人在敲门。"


translate chinese day4_main2_7c25eaac:


    th "我是赶上瘟疫大爆发了吗？"


translate chinese day4_main2_482a0501:


    "斯拉维娅站在门口。"


translate chinese day4_main2_23d59ff0:


    sl "哇，你好！护士在这里吗？"


translate chinese day4_main2_7f88d310:


    me "你好。她现在不在，我负责今天这里的事务。"


translate chinese day4_main2_8803d0ca:


    sl "好。{w}我想要..."


translate chinese day4_main2_6e6e30b3:


    "她犹豫了。"


translate chinese day4_main2_ad02f53a:


    me "什么？"


translate chinese day4_main2_23a777ed:


    sl "顺便说，谢苗..."


translate chinese day4_main2_626d4643:


    "斯拉维娅聚精会神的看着我说，"


translate chinese day4_main2_ad02f53a_1:


    me "什么？"


translate chinese day4_main2_809b714a:


    sl "我好像把自己的钥匙弄丢了，你在哪里看见了吗？"


translate chinese day4_main2_c2f310ec:


    th "当然啊。"


translate chinese day4_main2_fe5cc028:


    me "是的，这里。我昨天在食堂发现的，我本来想要还给你但是忘掉了。"


translate chinese day4_main2_7506f80e:


    "我真的很不擅长撒谎——我脸颊变红，眼神游离，手不停晃来晃去。"


translate chinese day4_main2_71a21743:


    me "给你。"


translate chinese day4_main2_01753d9d:


    "我已经准备好迎接她的责骂，不过斯拉维娅接过了钥匙说道："


translate chinese day4_main2_edc3cf97:


    sl "谢谢。"


translate chinese day4_main2_1a6d0231:


    me "所以说你是来做什么呢？"


translate chinese day4_main2_3e086ec1:


    "我需要转换话题。"


translate chinese day4_main2_12a9dd7f:


    me "有哪里疼吗？"


translate chinese day4_main2_87efefbd:


    sl "没有，没关系..."


translate chinese day4_main2_06e847e7:


    th "奇怪。{w}我可没想到像斯拉维娅这样开放的人也会藏着掖着。"


translate chinese day4_main2_0ca10a35:


    me "如果有什么问题就请告诉我好了！{w}我就是因为这个才在这里，治疗疾病。"


translate chinese day4_main2_ed4f432e:


    "我傻笑着。"


translate chinese day4_main2_537386e4:


    sl "不...{w}我是说是，但是不了。"


translate chinese day4_main2_981e60cc:


    "听到这个我觉得一切皆有可能，即使是除以0."


translate chinese day4_main2_d633a8ca:


    me "所以说我能帮到什么？"


translate chinese day4_main2_e2229da3:


    sl "你?{w}我觉得你不行。"


translate chinese day4_main2_d6c3c729:


    "她笑着准备离开，不过突然停下了。"


translate chinese day4_main2_0a917d1d:


    sl "不过...{w}你能出来呆一分钟吗？"


translate chinese day4_main2_9ae266c2:


    th "这有什么？"


translate chinese day4_main2_65bf82fc:


    me "好的..."


translate chinese day4_main2_cf1fe9a1:


    "我走出医务室，靠在墙边。"


translate chinese day4_main2_47737877:


    th "我很奇怪她要做什么不能让我看见？"


translate chinese day4_main2_8ee2b602:


    "过了一会儿，门打开了，斯拉维娅走了出来。"


translate chinese day4_main2_00d76af5:


    "她拿着一个小包裹。"


translate chinese day4_main2_fda5f154:


    me "那是什么？"


translate chinese day4_main2_1e8b4779:


    sl "没什么！"


translate chinese day4_main2_ac04f5d1:


    "她脸红了。"


translate chinese day4_main2_b4db0146:


    sl "谢谢！"


translate chinese day4_main2_c46d7c1e:


    me "不客气..."


translate chinese day4_main2_ec475aa3:


    "斯拉维娅跑开了。"


translate chinese day4_main2_0f420061:


    th "看来我不应该问的。..."


translate chinese day4_main2_3fc88750_1:


    me "祝你好运！"


translate chinese day4_main2_3da1183b:


    "我对她喊道。"


translate chinese day4_main2_309686e8:


    sl "谢谢！"


translate chinese day4_main2_a3a10705:


    "她离开前给了我一个甜甜的微笑。"


translate chinese day4_main2_a20cefa7_5:


    "..."


translate chinese day4_main2_43b742c4:


    "我看了看时间。"


translate chinese day4_main2_12b9bc98:


    "已经越来越晚了，我翻来覆去的看着杂志，不过护士还是不来。"


translate chinese day4_main2_e60481fd:


    "突然门打开了，阿丽夏冲了进来。"


translate chinese day4_main2_8fb93608:


    "她充满压力的看着我。"


translate chinese day4_main2_24b939bd:


    dv "你在这里做什么？"


translate chinese day4_main2_0085f606:


    me "坐着..."


translate chinese day4_main2_9fb02d3f:


    "我如实的回答道。"


translate chinese day4_main2_2b2b99a8:


    dv "那好吧！{w}没有护士更好。"


translate chinese day4_main2_36cead21:


    "她小声说道。"


translate chinese day4_main2_46efe3f1:


    me "你生病了吗？"


translate chinese day4_main2_81684e59:


    "我讽刺的说道。"


translate chinese day4_main2_d1591f00:


    "阿丽夏没有回答，而是靠近了我。"


translate chinese day4_main2_7693d10f:


    dv "让开。"


translate chinese day4_main2_7332342b:


    me "为什么？"


translate chinese day4_main2_b86ba02e:


    dv "所以我可以打开抽屉，这不是明摆着吗？"


translate chinese day4_main2_29a836d9:


    me "做什么？"


translate chinese day4_main2_eed3c5aa:


    "她怒了。"


translate chinese day4_main2_b2a1824d:


    dv "不关你的事。"


translate chinese day4_main2_d0eeeaf6:


    me "那个我算是这里管事的..."


translate chinese day4_main2_60f3d758:


    "她思考了一会儿。"


translate chinese day4_main2_f54686ff:


    dv "那就给我去找活性炭！"


translate chinese day4_main2_f841182c:


    me "肚子疼？"


translate chinese day4_main2_3a950d86:


    dv "是啊。"


translate chinese day4_main2_1fcb3138:


    "她邪恶的笑着。"


translate chinese day4_main2_c5e0f7d5:


    "我感觉事情有点不对劲..."


translate chinese day4_main2_1cfd6748:


    th "不过谁知道呢...{w}万一她真的生病了怎么办？"


translate chinese day4_main2_01822020:


    me "好吧。"


translate chinese day4_main2_e386270a:


    "我打开抽屉，找到了一包活性炭。"


translate chinese day4_main2_8526df6e:


    dv "感谢！"


translate chinese day4_main2_b7cee3ff:


    "她一把夺过跑开了。"


translate chinese day4_main2_952420fb:


    me "说真的，肚子疼？"


translate chinese day4_main2_e6d2dba1:


    dv "真的!"


translate chinese day4_main2_494cb28f:


    me "看起来不像啊..."


translate chinese day4_main2_30d58c19:


    dv "怎么，非要我把午饭全都吐在这里你才信？"


translate chinese day4_main2_5052e4e8:


    me "我不知道用活性炭可以搞什么阴谋，不过我确定你绝对计划着什么！"


translate chinese day4_main2_6e6e30b3_1:


    "她迟疑了一下。"


translate chinese day4_main2_2e830d48:


    dv "如果你说对了呢？"


translate chinese day4_main2_ccdea9ff:


    me "你看!{w}我就知道!{w}我绝对不会给你任何东西！"


translate chinese day4_main2_422e86f8:


    "阿丽夏想要接近抽屉，不过我像马特洛索夫一样堵住了碉堡。"


translate chinese day4_main2_da695243:


    "我不会让她的无理取闹得逞的。"


translate chinese day4_main2_672c3026:


    dv "你随便！我还能从别的地方弄到。"


translate chinese day4_main2_4a53f5ef:


    "她转过身走向出口。"


translate chinese day4_main2_579e0280:


    "她在门口停了下来，回头看看我。"


translate chinese day4_main2_40117237:


    dv "顺便告诉你，辅导员和护士正在运送一个腿折的少先队员。{w}我可不在乎，不过他们好像挺费劲的。"


translate chinese day4_main2_9f4cd3fa:


    me "当然。"


translate chinese day4_main2_9aa2cf7a:


    dv "你自己去看看吧..."


translate chinese day4_main2_0f163ab9:


    "阿丽夏好像在说谎，不过只要我能盯住她..."


translate chinese day4_main2_4ecdd924:


    "我来到门口向外看去，{w}没有人。"


translate chinese day4_main2_c0277722:


    me "呃，到底在哪里？"


translate chinese day4_main2_83ca7233:


    dv "他们还没有走到这里。"


translate chinese day4_main2_4a9b3777:


    "她做了一个无奈的动作。"


translate chinese day4_main2_df8028e5:


    dv "祝你愉快！"


translate chinese day4_main2_eeae4ddf:


    "我回到了桌子旁边，结果发现某一个抽屉被打开了。"


translate chinese day4_main2_d82dccfe:


    th "她是怎么做到的？"


translate chinese day4_main2_26b03227:


    th "我只转过身去几秒钟..."


translate chinese day4_main2_5992178b:


    th "不过用活性炭可以干出什么坏事呢？"


translate chinese day4_main2_a20cefa7_6:


    "..."


translate chinese day4_main2_22f56cd3:


    "还有15分钟到晚饭时间，可是护士还没有回来。"


translate chinese day4_main2_26c2ca6d:


    th "我觉得就这样离开好像不太好。"


translate chinese day4_main2_5cce2231:


    th "再怎么说她还是请我照看的..."


translate chinese day4_main2_259b8764:


    th "我离开了没准发生什么事情..."


translate chinese day4_main2_96201ae8:


    th "虽然说谁想从这里偷东西啊？"


translate chinese day4_main2_084da37b:


    th "除了阿丽夏偷了活性炭..."


translate chinese day4_main2_2f835300:


    "我再次打开杂志，颠来倒去的看着。"


translate chinese day4_main2_c423f302:


    "80年代的时尚不再那么有趣了。"


translate chinese day4_main2_9cba9e2a:


    "我忍不住打了几次呵欠。"


translate chinese day4_main2_b3a0bc3f:


    "已经六点钟了，晚饭已经开始了。"


translate chinese day4_main2_154cf8b8:


    "当然我应该好人做到底，不过我的肚子似乎有一些不同的意见。"


translate chinese day4_main2_fa7b39d4:


    "我站起来走向门口。"


translate chinese day4_main2_172df4fa:


    "门后边有一点沙沙的声音。"


translate chinese day4_main2_0360cdef:


    th "是不是又有谁中了毒，折了腿，或者是什么更严重的？"


translate chinese day4_main2_dfc8fcaa:


    "我重重的叹了口气，拧开了门把手。"


translate chinese day4_main2_4fcedea9:


    "然而外面没有人。"


translate chinese day4_main2_c5913e31:


    "看来只是我的错觉..."


translate chinese day4_main2_63fae2b6:


    "我回到屋里，马上发现医务室里有了点变化。"


translate chinese day4_main2_fc8152bc:


    "准确的说，桌子上出现了一个苹果。。"


translate chinese day4_main2_166e21e4:


    th "可是是从哪里来的？"


translate chinese day4_main2_263f36e7:


    "水果不会从空气直接转化形成吧？"


translate chinese day4_main2_7f119004:


    "虽然在这个夏令营里什么事情都有可能发生..."


translate chinese day4_main2_8c3d5b7c:


    "不过更像是谁拿过来的。"


translate chinese day4_main2_a3f9bd63:


    "也许是列娜想要感谢我。"


translate chinese day4_main2_c9e3d9e8:


    th "想想她那么害羞..."


translate chinese day4_main2_60cc7919:


    th "等一下！{w}她是怎么从我眼皮底下溜过去的？"


translate chinese day4_main2_91c185e1:


    th "再怎么说列娜也不会爬窗户吧！"


translate chinese day4_main2_598572e0:


    th "所以肯定还有什么别的东西..."


translate chinese day4_main2_a705caba:


    "我突然被恐惧所包围。"


translate chinese day4_main2_c2d327f7:


    th "万一苹果下了毒怎么办？！"


translate chinese day4_main2_7cd7e93e:


    "不过我在这个地方孤立无援，如果想杀我明明可以很简单为什么还要这样复杂呢？"


translate chinese day4_main2_dea7837b:


    th "或者这就是夏娃在伊甸园里偷吃的苹果？"


translate chinese day4_main2_74f2512f:


    th "那样的话我可绝对不能吃！"


translate chinese day4_main2_efaf11ed:


    "同一时间我的肚子不争气的叫起来。"


translate chinese day4_main2_0aeb7b69:


    th "然而还要等一会儿才能关掉医务室，走到食堂，搞到饭吃... "


translate chinese day4_main2_1f01a97c:


    "我的胃要自己跑出来吃了。"


translate chinese day4_main2_7e26087d:


    th "应该没什么问题吧，我猜..."


translate chinese day4_main2_49800ad1:


    "我又一次思考这个苹果之前是不是在这个地方。"


translate chinese day4_main2_4d50734a:


    th "它肯定是从什么地方蹦出来的！"


translate chinese day4_main2_9c6489f9:


    th "也许它是从窗户里飞进来的？"


translate chinese day4_main2_20f7d3af:


    "百叶窗微微打开着。"


translate chinese day4_main2_74cca6f2:


    th "所以说肯定有人放在这里的。"


translate chinese day4_main2_ec09e77a:


    th "不过这也没什么..."


translate chinese day4_main2_02c83bbb:


    "最终饥饿感占据了上风，我开始兴致勃勃的咬起那个青苹果。"


translate chinese day4_main2_cab5f253:


    "吃起来很正常。"


translate chinese day4_main2_cd98f167:


    th "虽然我很怀疑最强的毒药有没有味道。"


translate chinese day4_main2_b6572b50:


    th "不过现在考虑那个已经太晚了..."


translate chinese day4_main2_582156c4:


    th "不!{w}即使是普通的苹果，也得洗完再吃！"


translate chinese day4_main2_0b227b4a:


    th "就这样..."


translate chinese day4_main2_f6b3f760:


    "我找到了一个害怕的理由，把苹果扔在了一边。"


translate chinese day4_main2_38770f6e:


    "到了晚饭时间。"


translate chinese day4_main2_13514a11:


    "路上我遇到了电子小子。"


translate chinese day4_main2_9e8d764f:


    me "怎么样？你找到舒里克了吗？"


translate chinese day4_main2_c6d96820:


    el "不，完全没有线索..."


translate chinese day4_main2_0228ef23:


    me "别担心...{w}咱们回找到他的！"


translate chinese day4_main2_257dff95:


    "我试着使他振作起来。"


translate chinese day4_main2_d1dd1c27:


    el "已经过了太长时间了！{w}我继续搜索好了。"


translate chinese day4_main2_6ffcae27:


    me "晚饭怎么办？"


translate chinese day4_main2_2b8d0068:


    el "不，找到舒里克是更要紧的事情！"


translate chinese day4_main2_a1983f0e:


    "他若有所思的念叨着。"


translate chinese day4_main2_0470832e:


    "我一边祝他好运一边离开了。"


translate chinese day4_main2_00f424c0:


    "少先队员们挤在门口。"


translate chinese day4_main2_9219d4ed:


    "我试着加快脚步，尽量不要排到最后，至少今天不要。"


translate chinese day4_main2_29a4258a:


    "我还算幸运——远处的角落里有一张空桌。"


translate chinese day4_main2_5d928b01:


    "我迅速取回晚餐占领了桌子。"


translate chinese day4_main2_dffdc57f:


    "今天的晚饭是煮水果和小面包卷。"


translate chinese day4_main2_eac59148:


    "刚开始吓到我了，不过吃起来还不错。"


translate chinese day4_main2_e2cd40bb:


    "我专心致志的吃着。"


translate chinese day4_main2_c81cbbaa:


    mi "列娜，咱们去那里吧！三个空位！"


translate chinese day4_main2_8bd763eb:


    "列娜和未来站在我的面前。"


translate chinese day4_main2_e140e52b:


    un "这里有人了吗？"


translate chinese day4_main2_0a28548a:


    "列娜疑惑的问着。"


translate chinese day4_main2_82cf5563:


    me "没有，请坐吧。"


translate chinese day4_main2_e8b7b6b4:


    th "当然，我更希望她是自己来的。"


translate chinese day4_main2_082d3571_2:


    un "谢谢你..."


translate chinese day4_main2_f1ccf9dd:


    "列娜刚刚说完，热妮娅就从背后跳了出来。"


translate chinese day4_main2_2ab6d036:


    mz "我要坐在这里，没有别的地方了。"


translate chinese day4_main2_11d6a6d3:


    "她说着就把盘子放到了桌子上，都没有听听我的意见。"


translate chinese day4_main2_46957efa:


    me "当然，别客气..."


translate chinese day4_main2_596d1d03:


    "我悲伤的说着。"


translate chinese day4_main2_6fa187e2:


    mz "什么?!"


translate chinese day4_main2_cfaa423d:


    me "没什么..."


translate chinese day4_main2_1830a553:


    "说实话，我只想要列娜陪着我，虽然说未来和热妮娅也不是很碍事。"


translate chinese day4_main2_eea06034:


    "只是一个话太多一个太傲慢。"


translate chinese day4_main2_1ffd1a45:


    th "不过她们倒是没有危险性，和我知道的某些其他的人相比。"


translate chinese day4_main2_0a276ba6:


    un "哇，我好像忘记了带钥匙..."


translate chinese day4_main2_1236c393:


    mi "别担心，拿我的！"


translate chinese day4_main2_39b90d94:


    "我被未来的精炼回答震撼到了。"


translate chinese day4_main2_b6287b9f:


    me "你们...{w}住在一起吗？"


translate chinese day4_main2_94800851:


    mi "当然！你不知道吗？我们的宿舍是最靠后的，我是说最远的，我是说最后一个。"


translate chinese day4_main2_0d57a898:


    "我都不会惊讶即使有人和我说列娜和斯拉维娅住在一起{w}，甚至是热妮娅，{w}甚至是电子小子。"


translate chinese day4_main2_c29a6bc8:


    th "不过安静害羞的列娜和喋喋不休的未来正好是一对儿...{w}真是令人惊讶！"


translate chinese day4_main2_670ca875:


    mz "你找到舒里克了吗？"


translate chinese day4_main2_40a04971:


    "热妮娅居然为其他人的事情着急，真是奇怪。"


translate chinese day4_main2_5a7a7577:


    me "没呢。"


translate chinese day4_main2_f9068a33:


    mz "他肯定是去村子里边买烟去了，{w}或者是买伏特加。"


translate chinese day4_main2_58ae4528:


    "她喷着鼻子。"


translate chinese day4_main2_e9d77940:


    me "村子？"


translate chinese day4_main2_d535d09a:


    "对话变得有趣了。"


translate chinese day4_main2_86a5cb53:


    mz "村子有什么问题吗？"


translate chinese day4_main2_1490cc40:


    "热妮娅很惊讶。"


translate chinese day4_main2_fd219846:


    me "你是说这附近有一个村庄吗？"


translate chinese day4_main2_429609fe:


    mz "我想是的..."


translate chinese day4_main2_858becb4:


    "她不确定的说道。"


translate chinese day4_main2_31ee18ee:


    "我看着列娜和未来，不过她们正忙着吃饭，没有注意到我父母的对话。"


translate chinese day4_main2_9e200e27:


    me "你是说你不知道确切位置吗？"


translate chinese day4_main2_2c01aece:


    mz "这关我什么事啊。"


translate chinese day4_main2_b265fe1c:


    "热妮娅看着她的菜。"


translate chinese day4_main2_3a65b125:


    me "但是这附近一定有什么。"


translate chinese day4_main2_e76c967c:


    mz "听..."


translate chinese day4_main2_a2516dfe:


    me "正听着呢。"


translate chinese day4_main2_391fb882:


    mz "我不知道！你能让我吃饭吗？"


translate chinese day4_main2_76b07f1d:


    th "看来从她这里什么也问不出来。"


translate chinese day4_main2_b71b185c:


    "虽然她有可能真的什么都不知道..."


translate chinese day4_main2_91e6868c:


    "接下来的时间我们都在听未来疯狂的讲话，我正在悄悄的发疯。"


translate chinese day4_main2_7c2f2de1:


    "显然在我离开食堂之后的第一件事的马上呼吸一口新鲜空气。"


translate chinese day4_main2_ef1b28a4:


    "太阳正在洛神。"


translate chinese day4_main2_153cf25d:


    th "我决定去转转。"


translate chinese day4_main2_2c803352:


    th "我好像找不着什么度过晚上的好方法了，这是个提起我的兴趣的好方法。"


translate chinese day4_main2_441bd7cd:


    "我正在接近广场的时候听到了一声巨响。{w}好像有什么爆炸了。"


translate chinese day4_main2_2c4a9213:


    "我被吓呆了。"


translate chinese day4_main2_9abc32b1:


    th "我在一个自己不知道规则的危险处境里！"


translate chinese day4_main2_e35e500a:


    th "我最好还是跑吧！{w}不过同时我也很好奇..."


translate chinese day4_main2_f6711c85:


    "我本来可能还会继续站在这里，不过有人抓住了我的手。"


translate chinese day4_main2_db010348:


    "是奥尔加·德米特里耶夫娜。"


translate chinese day4_main2_7cc1e65d:


    mt "你在这里站着做什么？{w}快去检查一下发生了什么事！"


translate chinese day4_main2_51893c59:


    me "你有没有能自己做的时候？"


translate chinese day4_main2_df9d9f60:


    "我可怜的求着她。"


translate chinese day4_main2_92fa383c:


    mt "应该不是太严重。{w}希望如此..."


translate chinese day4_main2_723aed67:


    "等我们走到广场的时候那里已经聚集了一大片的少先队员了。"


translate chinese day4_main2_0b603c9e:


    "奥尔加·德米特里耶夫娜推开人群，来到了犯罪现场。"


translate chinese day4_main2_c9d63825:


    "显然有人试着炸毁Genda。"


translate chinese day4_main2_7a693237:


    "不过袭击者失败了，雕塑还戳在那里。"


translate chinese day4_main2_f15dbe9c:


    "爆炸只在基座上留下了一些灰暗的痕迹。"


translate chinese day4_main2_466f41f2:


    mt "好了，这是谁干的？"


translate chinese day4_main2_fe6f2470:


    "她看着拥挤的人群。"


translate chinese day4_main2_588fe4a7:


    "这肯定不是恐怖组织策划的。"


translate chinese day4_main2_dcd82f25:


    "这些人都是来这里围观的。"


translate chinese day4_main2_1da09f52:


    "我注意到了人群之中的乌里扬娜和阿丽夏。{w}而且看起来辅导员也注意到她们了。"


translate chinese day4_main2_fdaff1c2:


    mt "你们两个，过来！"


translate chinese day4_main2_c276a464:


    "她们不情愿的走了过来。"


translate chinese day4_main2_4013aec6:


    us "怎么老是我？"


translate chinese day4_main2_ac0e5120:


    dv "如果你是这么认为的话..."


translate chinese day4_main2_f8a4a1d3:


    mt "让我看看你的手，朵切芙斯卡娅！"


translate chinese day4_main2_0c3815a2:


    dv "有什么问题啊？"


translate chinese day4_main2_a967cbc5:


    "我凑近了看看，发现它们被涂得黑黑的。"


translate chinese day4_main2_6914b9a6:


    mt "现在事情很清楚了...{w}你是用什么做的炸弹？！"


translate chinese day4_main2_74595ca6:


    "这个少年恐怖分子好像正在思考要不要承认自己的罪行，不过她喊了出来："


translate chinese day4_main2_73fdb6c9:


    dv "活性炭，硝石，硫磺！"


translate chinese day4_main2_e3192dfb:


    th "等一下！{w}碳！{w}她从我的医疗箱里边拿走的活性炭！"


translate chinese day4_main2_2d58bb92:


    mt "为什么是雕像呢？{w}这个伟大的人对你做了什么？这个斗士为了维护..."


translate chinese day4_main2_23e3015c:


    dv "顺便说，活性炭是他给我的。"


translate chinese day4_main2_276a218b:


    "她用手指指着我，整个广场上的人都看向了我。"


translate chinese day4_main2_034b00d2:


    "我这时想到，要是策划一次恐怖袭击的话，使用这么小的炸弹岂不是没什么意义吗？"


translate chinese day4_main2_a6f5f832:


    me "这是她偷的！{w}我什么都没做！"


translate chinese day4_main2_978d7670:


    mt "即使是他给你的，我也相信谢苗不会参与你的这种令人作呕的反社会行动。"


translate chinese day4_main2_e7ed10ff:


    me "是！是啊！就是这个意思！"


translate chinese day4_main2_d3ec559b:


    "我表示同意。"


translate chinese day4_main2_a20cefa7_7:


    "..."


translate chinese day4_main2_a20cefa7_8:


    "..."


translate chinese day4_main2_d88f4446:


    "我几乎不能想象辅导员还会训斥她多久如果不是电子小子大喊起来："


translate chinese day4_main2_e6b5d4f7:


    el "我找到了！我找到了！"


translate chinese day4_main2_a09465a1:


    "大家都看过去。"


translate chinese day4_main2_50ceb1d4:


    "他的手中举着一只靴子。"


translate chinese day4_main2_16b80206:


    el "看！"


translate chinese day4_main2_b9a254c5:


    "电子小子兴奋的举过头顶。"


translate chinese day4_main2_11da0634:


    el "是舒里克的靴子。"


translate chinese day4_main2_c764847f:


    mt "冷静！{w}跟我们说说具体是什么情况？"


translate chinese day4_main2_a425fd4f:


    el "在森林里。{w}去往旧营地的路上。"


translate chinese day4_main2_13dffab5:


    "人群开始窃窃私语。"


translate chinese day4_main2_8c7b2ad8:


    all "旧营地...旧营地..."


translate chinese day4_main2_d3e8a3d1:


    mt "你确定？"


translate chinese day4_main2_d0c3eb6a:


    el "绝无差错！!"


translate chinese day4_main2_438450d6:


    me "旧营地有什么特殊的呢？"


translate chinese day4_main2_37372bdb:


    "我加入到对话当中来。"


translate chinese day4_main2_1b61865c:


    mt "其实没什么，特殊的..."


translate chinese day4_main2_49a81686:


    "她结结巴巴的说着。"


translate chinese day4_main2_91c692ef:


    el "夏令营的一个传说是旧营地有一个年轻的辅导员的鬼魂，她爱上了一个少先队员，但是他拒绝了她，结果她自杀了..."


translate chinese day4_main2_417824db:


    us "她用一个菜刀切腹了。{w}第二天那个男孩子被一辆公交车撞了。"


translate chinese day4_main2_98631528:


    "乌里扬娜跑出了人群。"


translate chinese day4_main2_5eb46b8b:


    me "公交车？"


translate chinese day4_main2_94dec8ff:


    "我忍住没问车子的号码。"


translate chinese day4_main2_8e90eaa7:


    el "不过科学没有证实鬼魂的存在，所以咱们没什么好怕的。"


translate chinese day4_main2_2d8001fb:


    mt "总之，得有人去那里看看。"


translate chinese day4_main2_4ab0957c:


    "人群突然开始散开。"


translate chinese day4_main2_957e36e8:


    sl "奥尔加·德米特里耶夫娜，已经到了晚上了，咱们是不是明天再来？"


translate chinese day4_main2_51b9d15d:


    "我转过来，看到了斯拉维娅和列娜。"


translate chinese day4_main2_f8e498d7:


    mt "万一在晚上...{w}万一晚上他出了事怎么办？{w}不行！今天！现在！"


translate chinese day4_main2_ddc6851f:


    me "话说，这是什么地方？"


translate chinese day4_main2_747f2d95:


    "电子小子简单的向我介绍去就营地怎么走，并且给我讲了那些故事。"


translate chinese day4_main2_cb78152f:


    "辅导员仔细的看着我。"


translate chinese day4_main2_3ac0f1ea:


    me "如果你觉得我..."


translate chinese day4_main2_e13e7c4b:


    mt "你是这里唯一的男人。"


translate chinese day4_main2_81abb051:


    "我看了看周围。"


translate chinese day4_main2_db2e005d:


    th "当然…{w}电子小子溜得真快！"


translate chinese day4_main2_0bb42214:


    "我还是不想自己走进林子里。"


translate chinese day4_main2_eba0fa62:


    "如果你问的话，我会..."


translate chinese day4_main2_83894804:


    "我在说什么？{w}为什么她会跟我来呢？"


translate chinese day4_main2_a89ae039:


    th "她会同意吗？"


translate chinese day4_main2_69f37ce2:


    "我看向斯拉维娅。"


translate chinese day4_main2_22e7806d:


    "她没有注意到我。"


translate chinese day4_main2_f88a9c6c:


    th "今天真是不走运..."


translate chinese day4_main2_3bf87393:


    th "看来她的冒险精神不是什么时候都那么旺盛。"


translate chinese day4_main2_3ecb90c0:


    "认为列娜不会害怕的想法很愚蠢。"


translate chinese day4_fail_867359a2:


    me "我要自己去那里吗？"


translate chinese day4_fail_be1d7816:


    "辅导员若有所思。"


translate chinese day4_fail_d7898f7f:


    mt "也许你是对的...{w}咱们应该明天早上再去。"


translate chinese day4_fail_c890c351:


    "我们沉默的站了一会儿，然后就解散了。"


translate chinese day4_fail_6a9a81a0:


    "大家似乎都忘记了阿丽夏和她的恐怖袭击，可能是因为影响太小了吧。"


translate chinese day4_fail_727cde7c:


    "我和辅导员一起回到了宿舍。"


translate chinese day4_fail_d95546c4:


    "我试着思考为什么舒里克会去旧营地。"


translate chinese day4_fail_6b65c5dc:


    "他感兴趣的是机器人，自动机械，不是鬼故事和鬼魂什么的。"


translate chinese day4_fail_9917267e:


    th "这只靴子和那件事有什么关系？"


translate chinese day4_fail_a35e5ae3:


    "我想起怎么等着，等到什么特别的事情发生的时候..."


translate chinese day4_fail_ea2f2219:


    "然后我突然明白了，就是它！"


translate chinese day4_fail_76ef374b:


    me "奥尔加·德米特里耶夫娜我睡觉前打算出去溜达溜达。"


translate chinese day4_fail_11c5c177:


    mt "回来不要太晚喔！"


translate chinese day4_fail_80e4f228:


    me "好的!"


translate chinese day4_fail_e3c48cd1:


    "当我来到广场的时候我觉得在森林里还是什么地方晃来晃去也许没有那么吓人..."


translate chinese day4_fail_04005f5b:


    "不过在黑暗之中...！"


translate chinese day4_fail_9fa06fb8:


    "我不想回到奥尔加·德米特里耶夫娜的宿舍拿个手电。{w}医务室就有一个。"


translate chinese day4_fail_f0d2d342:


    th "幸好我在那里的时候没有完全沉迷于读时尚杂志。"


translate chinese day4_fail_a20cefa7:


    "..."


translate chinese day4_fail_f5100b67:


    "我很快找到了手电筒，不一会儿我就已经在广场上准备出发了。"


translate chinese day4_fail_48a0470e:


    "我需要在进入森林之前做好最后的决定。"


translate chinese day4_fail_4590a9c1:


    "沿着森林的小径，穿过少先队员们的房间来到旧营地..."


translate chinese day4_fail_dc86bdf9:


    "我出发了。"


translate chinese day4_fail_7bd87323:


    "电子小子告诉我说那个建筑是战争刚刚结束的时候建的。"


translate chinese day4_fail_fd3c1cdc:


    "它看起来像是一个幼儿园，或者是什么兵营，看起来装不下现在这么多的人。"


translate chinese day4_fail_e9bbdb04:


    "它已经废弃了二十年了。"


translate chinese day4_fail_a20cefa7_1:


    "..."


translate chinese day4_fail_afeb43da:


    "现在天色完全黑了。..."


translate chinese day4_fail_4b08c931:


    "晚上的森林看起来完全不一样。"


translate chinese day4_fail_00c53d58:


    "神秘的阴影隐藏在树林后面，还有奇怪的沙沙响声以及鸟儿的叫声，大量的树枝阻挡着我的步子。"


translate chinese day4_fail_7944d14b:


    "满月的光照着大地，我决定省着用手电筒的电量，我确定会有用得上的时候的。"


translate chinese day4_fail_acbdce76:


    "我越是往前走，头脑中不好的想法越是不断地出现。"


translate chinese day4_fail_e275989e:


    "我完全不怕黑，也不相信鬼魂还有类似的什么乱七八糟的东西，不过在这个地方经历了种种神奇的经历之后，我开始不断的注意到普通事件里的一样。"


translate chinese day4_fail_f927e7d0:


    "显然晚上的森林让我格外不舒服。"


translate chinese day4_fail_eab673b2:


    "我看到的每一个地方，没一个影子，每一个树丛，看起来都像是奇怪的生物，从恐怖片或者史蒂芬金的小说里跑出来。"


translate chinese day4_fail_2ca15dc2:


    "一个树枝想手臂一样伸过来，想要抓住你的肩膀。树上吊着没有走出森林的人的尸体。每一条沟壑都深不见底。"


translate chinese day4_fail_c6ac6be9:


    "如果我只是在普通的森林里我就不会害怕了吧。"


translate chinese day4_fail_bd7ebd85:


    "不过现在我有无数个理由害怕。"


translate chinese day4_fail_851bae5d:


    "不过虽然如此，我还是有惊无险的来到了旧营地。"


translate chinese day4_fail_a20cefa7_2:


    "..."


translate chinese day4_fail_c9298dc3:


    "树林中的一片空地中，矗立着一座孤零零的老房子。"


translate chinese day4_fail_ca186bf0:


    "墙上的油漆斑斑驳驳，几乎所有的窗户都破掉了，破损的砖石，房顶的大洞像是炸弹轰炸的结果。"


translate chinese day4_fail_c42f0ebd:


    "房屋前面有一个小花园，周围围着一圈生锈的篱笆。"


translate chinese day4_fail_cb36b943:


    "显然它曾经是摆放着旋转木马，秋千，滑梯什么的小运动场。"


translate chinese day4_fail_ba4e4337:


    "时间是把杀猪刀...现在只剩下一堆残损的破铜烂铁。"


translate chinese day4_fail_f3f7ddd1:


    "杂草长到及腰的高度。"


translate chinese day4_fail_c1bb36b5:


    "月光从云层中照射过来，让旧营地发出光辉。"


translate chinese day4_fail_7a22a02d:


    "一切都像是恢复了往日的生机。"


translate chinese day4_fail_96af7e9a:


    "我仿佛听到了孩子们嬉戏的欢笑声，一个严厉的辅导员站在门槛上，抱着一篮子苹果，打扫卫生的老妇人在凉阴中打着瞌睡。"


translate chinese day4_fail_30d755a6:


    "这让我后背一凉。"


translate chinese day4_fail_389b1cbf:


    "当然这只是我的想象，不过旧营地本身看起来就像是浩劫之后的景象，我会看到这些也就没什么惊讶的了。"


translate chinese day4_fail_e652569f:


    "我站在那里很长时间，才决定继续走下去。"


translate chinese day4_fail_654087eb:


    th "总之，我是来这里找舒里克的！"


translate chinese day4_fail_17d0369d:


    th "虽然我还是不明白他到底为什么来这个地方...{w}甚至是他究竟在不在这里..."


translate chinese day4_fail_0e534257:


    th "附近的一只靴子可说明不了什么！"


translate chinese day4_fail_e4b26812:


    "不过如果他没有被狼吃掉的话（这个森林里不知道为什么没有狼）就应该是在这里了。"


translate chinese day4_fail_067a08ab:


    "我小心地穿过庭院，拨开高高的草丛，避开凹凸不平的废弃金属。"


translate chinese day4_fail_4c060fb7:


    "不过我在门口犹豫了。"


translate chinese day4_fail_39f1d886:


    "无法看透的压抑的黑暗从里边注视着我。"


translate chinese day4_fail_443d22e0:


    "即使是明亮的月光也什么都照不亮。"


translate chinese day4_fail_ef3a7e9d:


    "我的大脑本能的告诉我最好的选择是马上逃走。"


translate chinese day4_fail_9aeb4e06:


    "不过我打了打精神，打开了手电筒，跨过了门槛。"


translate chinese day4_fail_bef84d9a:


    "建筑的里面就像是一个普通的幼儿园。"


translate chinese day4_fail_11a53aa3:


    "我记得自己的童年，这里的一切看起来都是那么熟悉。"


translate chinese day4_fail_b863bbfe:


    "卧室里一排排的小床铺，一个大活动室，还有食堂、厕所、公共设施什么的。"


translate chinese day4_fail_10696fa4:


    "这里到处充满了过去孩子们的欢声笑语的印记，手电筒的光线不时地把我的注意力引向一个烂掉的娃娃，一个塑料的玩具车，或是被戳破的橡胶球，还有乱糟糟的玩具士兵的队列。"


translate chinese day4_fail_186331b8:


    "桌子上有一本书，历经了沧桑。"


translate chinese day4_fail_cb2cab2c:


    "我翻了几页。"


translate chinese day4_fail_f381b7fd:


    "是我在童年的时候就读过的一个故事。{w}一个关于战争的故事..."


translate chinese day4_fail_b27a5c2e:


    "敌人毁掉了这个幼儿园，大人和小孩子们都躲在了地下室里，这时炸弹在上面爆炸了。"


translate chinese day4_fail_25b7d2ba:


    "我不记得结局了，不过印象里最后没有人死掉。"


translate chinese day4_fail_81629920:


    "看着其中的描述，各种情感：恐惧，悲伤，对孩子们的同情还有无助的绝望，一切我曾经阅读这个故事时的感情再一次涌现出来。"


translate chinese day4_fail_1c3a4b63:


    "奇怪的是，就在这种情况下，此时此地，我又找到了这本书。"


translate chinese day4_fail_107ba7e9:


    th "也许是有人写了这个剧本，还做了戏服。盖了这个房子，让少先队员们排练这个戏剧。"


translate chinese day4_fail_0d6ce2d3:


    th "说实话他做得很到位，注意到了各种细节。"


translate chinese day4_fail_ffa0fc7b:


    "虽然很久以前这个地方就被遗弃了，不过看起来一切都毫发无损的保存了下来。"


translate chinese day4_fail_e82fe0f4:


    "当然，时间没有放过这个营地，这些年过去，这里肯定被人们不断的拜访过。"


translate chinese day4_fail_67a745c4:


    "这是一个提供给青春期的孩子们绝佳的地方。他们可能在这里沉浸于酒精，爱情...或者干脆为了好玩把所有东西烧个干净。"


translate chinese day4_fail_5cc1d037:


    "电子小子说这个地方充满了传说。{w}关于这里有鬼魂和恶魔的传说..."


translate chinese day4_fail_20237b24:


    "比如说自杀的辅导员的故事。"


translate chinese day4_fail_9f4da515:


    "在{i}我的{/i}世界里这可能难以让人们停下脚步，不过在这里什么都是有可能的。"


translate chinese day4_fail_5ae24796:


    "当然，我的恐惧感没有消失，不过我觉得自己既然来了就干脆做到底。"


translate chinese day4_fail_f267b85f:


    "整个建筑并不大，所以搜索一遍不是很困难。"


translate chinese day4_fail_230cbcfd:


    "不过我没有发现任何舒里克的痕迹。"


translate chinese day4_fail_7e30252f:


    th "也许我应该喊喊他？"


translate chinese day4_fail_14b6dd58:


    th "不过那可能并不是一个好主意。"


translate chinese day4_fail_7fff55da:


    "我失意的坐在门厅的长椅上叹着气。"


translate chinese day4_fail_4d3113b1:


    me "鬼魂啊，你在哪里？"


translate chinese day4_fail_9d6a7df4:


    "我问着这个营地。"


translate chinese day4_fail_7539188b:


    "突然我注意到了角落里的一个小坑。"


translate chinese day4_fail_3f4305c5:


    "一个小小的暗门，成年人似乎都很难钻过去。"


translate chinese day4_fail_1154aac9:


    "而且看起来它好像最近被打开过，上面的灰尘被清理了。"


translate chinese day4_fail_70f0a211:


    "所以说这下我该害怕了。"


translate chinese day4_fail_4b219aad:


    th "那么舒里克走到什么地方去了..."


translate chinese day4_fail_8e98e395:


    th "暗门的后边藏着什么呢？{w}一个地下室？{w}他在这里干什么？"


translate chinese day4_fail_f1c33359:


    "我打开小门，用手电筒照着里面的黑暗。"


translate chinese day4_fail_d6d55fdd:


    "有一个梯子向下延伸了几米，抵达水泥的地面。"


translate chinese day4_fail_f98d9bf1:


    "一条又矮又窄的走廊向前延伸。"


translate chinese day4_fail_da411e3b:


    "我犹豫了很长时间。"


translate chinese day4_fail_b42ebb80:


    "一方面来说，我十分不想下去，不过话说回来，如果舒里克真的在里面..."


translate chinese day4_fail_84882d7a:


    "我一边骂着一边往下走。"


translate chinese day4_fail_ddec5394:


    "下面一点光线也没有，让手电筒在它面前也黯然失色。"


translate chinese day4_fail_30592890:


    "低矮的天花板，沿着墙壁布设着电线，还有金属栏杆后边的坏掉的灯。"


translate chinese day4_fail_43e505f2:


    th "我在什么地方见过这里的一切..."


translate chinese day4_fail_1ed31216:


    "我开始慢慢的向这个地牢的深处移动。"


translate chinese day4_fail_bd147fc2:


    "小路经过了几十米后在一处更高的台阶处停了下来。"


translate chinese day4_fail_af70ec38:


    "很快我来到了一个大金属门面前。"


translate chinese day4_fail_2c241583:


    "上面印着什么标志。"


translate chinese day4_fail_39f74a0e:


    "我凑近观察，发现是一个生化危险的标志。"


translate chinese day4_fail_a60db0bf:


    th "可以理解—如果这是战后建造的营地还一直保留到了70年代的话。"


translate chinese day4_fail_2bd94a6b:


    th "所以只是一个普通的防空洞，没有什么好怕的..."


translate chinese day4_fail_d224dd30:


    "然而这种想法让我更加害怕了。"


translate chinese day4_fail_4b931dd1:


    "我下定决心打开了大门..."


translate chinese day4_fail_d415bd1a:


    "...刚刚走进来大门就伴随着一声巨响关上了。"


translate chinese day4_fail_240fda6a:


    "我开始强烈地发抖，我的双手颤个不停，我的视线变得昏暗。"


translate chinese day4_fail_8247734a:


    "我过了几秒钟才恢复了意识，这个时候我才发现门上安装了一个巨大的弹簧。"


translate chinese day4_fail_9e97fbcb:


    "我发现自己来到了一个宽敞的房间。"


translate chinese day4_fail_0ba401c8:


    "在手电的光线中我发现了一个床位，墙上的一些小柜子还有远处角落里不知道干什么用的设备。"


translate chinese day4_fail_a1458d50:


    "看起来这个防空洞没有投入使用..."


translate chinese day4_fail_21c3af84:


    "没有人来过这里。{w}床铺还被铺好了。"


translate chinese day4_fail_274647ee:


    "这本身就很奇怪..."


translate chinese day4_fail_fd3face8:


    "我谨慎的观察了整个房间。"


translate chinese day4_fail_6d5a6727:


    "柜子里装着防毒面具，生化服，压缩干粮, 各种乱七八糟的小零碎。"


translate chinese day4_fail_76cae0d1:


    "用来计量辐射、气压和温度的仪器。"


translate chinese day4_fail_8c887cf9:


    "简单的说，这就是一个普通的防空洞。"


translate chinese day4_fail_6cf7d00d:


    "我从来没有来过这种地方，不过我想象中大概就是这个样子。"


translate chinese day4_fail_1aa39f51:


    "唯一没有被探索的地方就是房间另一头的大门。"


translate chinese day4_fail_efc3a1da:


    "我拉动把手，不过大门还是紧紧的关着。"


translate chinese day4_fail_24bba245:


    th "如果最近有人来到过这个房间，现在又不在了，那么估计他就在大门的那一边。"


translate chinese day4_fail_b7f23876:


    "我悄悄地说道。"


translate chinese day4_fail_c088b6e1:


    me "舒里克...?"


translate chinese day4_fail_dfffcbac:


    "没有回答。"


translate chinese day4_fail_f8c98a55:


    th "也许他把自己锁在了里面...{w}可是为什么？"


translate chinese day4_fail_234abe88:


    "我不太清楚下一步应该做什么。"


translate chinese day4_fail_d965f701:


    "也许明天带着援军来这里更好，不过我感觉这扇门后边有着我等不及要揭开的秘密。"


translate chinese day4_fail_6f36167b:


    "我在角落里找到了一根撬棍。"


translate chinese day4_fail_88aff48d:


    "我撬进门的底部，想要把它从合页上撬开。"


translate chinese day4_fail_548728ed:


    "奇怪的是，过了一会儿，我就做到了，并不像前一个门那样费劲。"


translate chinese day4_fail_90069d4f:


    "大门拍在了地上。"


translate chinese day4_fail_eb8bd4b5:


    "我深吸了一口气，用手电指向路的前方。"


translate chinese day4_fail_55ed8e88:


    "一条走廊消失在远处，就像是我来到这里的那条小路一样。"


translate chinese day4_fail_a20cefa7_3:


    "..."


translate chinese day4_fail_9276a63c:


    "两侧围墙越来越窄，房顶也越来越低，然而我还是望不到头。"


translate chinese day4_fail_709d42b2:


    "我小心地走着，不停地看着地面，才不至于掉到坑里..."


translate chinese day4_fail_3903f0e0:


    "这个坑直径大约一米半，底部是土而不是水泥混凝土。"


translate chinese day4_fail_9b16cd8e:


    "这个大坑似乎是爆炸产生的。"


translate chinese day4_fail_80434573:


    th "并不是太深，我一会儿可以跳出来。"


translate chinese day4_fail_cf5b6c1b:


    "跳进坑里之后，我发现自己来到了一个像是废旧的矿坑的地方。"


translate chinese day4_fail_1a034f9f:


    "墙体上安装了加固用的木制柱子和横梁。"


translate chinese day4_fail_4bec4ed2:


    "隧道太长了，我的手电筒可能不够用..."


translate chinese day4_fail_a20cefa7_4:


    "..."


translate chinese day4_fail_8e2bc52d:


    "过了几分钟我来到了岔路口。"


translate chinese day4_fail_68ebea2c:


    "再往前走有迷路的危险。"


translate chinese day4_fail_0ef45387:


    "我觉得标记这个位置是一个好主意，所以捡了一块大石头在进入迷宫之前在墙上刻了一个记号。"


translate chinese day4_fail_7dd017ff:


    th "那么..."


translate chinese mine_crossroad_07a69b74:


    "路的前方似乎又有岔路口。"


translate chinese mine_crossroad_ff6d6d5f:


    "看起来又是一个岔路口。"


translate chinese fail_mine_return_to_start_fda95a66:


    "我又回到了自己出发的地方。"


translate chinese fail_mine_return_to_start_d452f00a:


    th "所以说我在某个地方走错了。"


translate chinese fail_mine_return_to_start_2bce21f4:


    th "咱们继续找找吧。"


translate chinese girls_mine_return_to_start_f89efc62:


    "我们回到了出发的地方。"


translate chinese girls_mine_return_to_start_c806e46d:


    th "所以说我们在某个地方走错了。"


translate chinese girls_mine_return_to_start_2bce21f4:


    th "咱们继续找找吧。"


translate chinese fail_mine_coalface_61204914:


    "我几分钟之前来过这里。"


translate chinese fail_mine_coalface_31787369:


    "终于我走出了隧道，来到了一个高高的大厅。"


translate chinese fail_mine_coalface_3ef8c583:


    "虽然说是一个大厅，不过肯定是用来挖矿的。{w}煤矿吧，也许是金矿。"


translate chinese fail_mine_coalface_3c54fe69:


    "墙体被镐和气钻打断。"


translate chinese fail_mine_coalface_3e6cec54:


    "这个地方一片漆黑，所以我只能依赖自己的手电筒了。"


translate chinese fail_mine_coalface_ffe6f7a7:


    th "如果碎掉的话，我可能再也走不出去了..."


translate chinese fail_mine_coalface_58411121:


    "在光线中，我突然发现了一块红色的布。"


translate chinese fail_mine_coalface_98466bb2:


    "红领巾！"


translate chinese fail_mine_coalface_96b3272c:


    "舒里克肯定来到过附近。"


translate chinese fail_mine_coalface_93ec21f7:


    me "舒里克！舒里克！"


translate chinese fail_mine_coalface_f23d45c0:


    "只有我自己的回声回答我。"


translate chinese fail_mine_coalface_cc8806c6:


    th "不过他可能去哪里呢？"


translate chinese fail_mine_coalface_db4b78eb:


    "没有其他的出口了。"


translate chinese fail_mine_coalface_33bf97c7:


    th "也许隧道里还有我没有发现的地方。"


translate chinese fail_mine_coalface_858b6cbe:


    th "所以我应该更仔细的搜索！"


translate chinese fail_mine_halt_61204914:


    "我几分钟之前来到过这里。"


translate chinese fail_mine_halt_739fe800:


    "我来到了一个类似于矿工营地的地方。"


translate chinese fail_mine_halt_9f9a3875:


    "这里到处是各种工具还有头盔，角落里还有一个生锈的矿车。"


translate chinese fail_mine_halt_a2b01ca8:


    "这些东西看起来都非常老旧——更像是20世纪初的东西，不像是上世纪中叶的。"


translate chinese fail_mine_halt_631320fe:


    th "所以说这里真的是一个矿。{w}意味着我可能在里面绕个几年也找不到出去的路。"


translate chinese fail_mine_halt_1efc66da:


    "不过我感觉到出口就在附近，所以继续往前走。"


translate chinese girls_mine_halt_e40597aa:


    "我们几分钟前来到过这里。"


translate chinese girls_mine_halt_498e0e02:


    "我们来到了一个类似于矿工营地的地方。"


translate chinese girls_mine_halt_9f9a3875:


    "这里到处是各种工具还有头盔，角落里还有一个生锈的矿车。"


translate chinese girls_mine_halt_a2b01ca8:


    "这些东西看起来都非常老旧——更像是20世纪初的东西，不像是上世纪中叶的。"


translate chinese girls_mine_halt_631320fe:


    th "所以说这里真的是一个矿。{w}意味着我们可能在里面绕个几年也找不到出去的路。"


translate chinese girls_mine_halt_910be1a3:


    "不过我感觉到出口就在附近，所以继续往前走。"


translate chinese fail_mine_miku_29f8f3fe:


    "我走啊走…"


translate chinese fail_mine_miku_08cda699:


    "好像这些隧道都没有尽头，我好像被困在了弥诺陶洛斯的迷宫中，而且马上就要有可怕的生物从地下冒出来。"


translate chinese fail_mine_miku_6dda7c89:


    "我靠着潮湿的墙面慢慢的向下滑倒..."


translate chinese fail_mine_miku_98a75609:


    th "这该死的舒里克到底在什么地方？"


translate chinese fail_mine_miku_db9b9ede:


    th "而且我已经在这里转了几个小时了。"


translate chinese fail_mine_miku_93a335d8:


    "我的眼睛慢慢的闭上..."


translate chinese fail_mine_miku_154d0a94:


    th "只要几分钟…"


translate chinese fail_mine_exit_c43624ee:


    "在最后一个转弯处，一扇门出现在了黑暗之中。"


translate chinese fail_mine_exit_2236a915:


    "我打开门，走进一个小房间。"


translate chinese fail_mine_exit_50ed1d0a:


    "这里没有人，除了…"


translate chinese fail_mine_exit_6c35e784:


    "…坐在角落里的舒里克！"


translate chinese fail_mine_exit_b01a7f13:


    me "舒里克!"


translate chinese fail_mine_exit_bfe85680:


    "他眼睛里充满恐惧的看着我。"


translate chinese fail_mine_exit_2cfb869c:


    sh "啊啊啊！！不要靠近我！！"


translate chinese fail_mine_exit_f192c172:


    me "舒里克，是我，谢苗！"


translate chinese fail_mine_exit_fe05dcbb:


    sh "我可不会那么容易放弃的！"


translate chinese fail_mine_exit_962a501b:


    "他捡起一根棒子开始挥舞起来！"


translate chinese fail_mine_exit_ce533195:


    me "舒里克，你发什么疯？！冷静！"


translate chinese fail_mine_exit_1a0db18d:


    sh "你先是把我带到这里！然后带我走过这个矿井！然后又伪装成谢苗！"


translate chinese fail_mine_exit_f7d0be86:


    th "他一定是完全发疯了..."


translate chinese fail_mine_exit_95727752:


    me "舒里克！冷静！没有人伪装成我！我真的是我自己！"


translate chinese fail_mine_exit_d566ef9e:


    "我尽量冷静的说道，不过我已经比他还慌了。"


translate chinese fail_mine_exit_c8104f36:


    sh "谢苗?"


translate chinese fail_mine_exit_c9c76fb6:


    "他斜眼看着我。"


translate chinese fail_mine_exit_e5f3e2a0:


    sh "真的是你吗？"


translate chinese fail_mine_exit_6034747a:


    me "是啊！告诉我发生了什么事！你是怎么来到这里的？"


translate chinese fail_mine_exit_d1fc8105:


    "舒里克恢复了呼吸，开始说到："


translate chinese fail_mine_exit_b0096a58:


    sh "我需要给机器人找一些零件。{w}然后我听说在旧的营地那边有一个防空洞...{w}那里可能有一些设备。"


translate chinese fail_mine_exit_400eb8c8:


    sh "所以我早晨起床前往这里..."


translate chinese fail_mine_exit_04e9519a:


    "他停下来一分钟，解决手抖的问题。"


translate chinese fail_mine_exit_c71c2b1c:


    sh "我找到了合适的零件，可是..."


translate chinese fail_mine_exit_3fd0e1a0:


    "我在他的眼睛里读出了本能的恐惧。"


translate chinese fail_mine_exit_3bf32b52:


    sh "那个声音，告诉我往这里走，所以我来到了那个矿井，然后那个声音告诉我往右往左往右往左往右往左..."


translate chinese fail_mine_exit_0c32fa11:


    "他开始歇斯底里的狂笑。"


translate chinese fail_mine_exit_3edbb0d2:


    sh "不过他们现在找不到我了！我在这里！他们找不到我！"


translate chinese fail_mine_exit_8453558b:


    "他又开始挥舞他的棒子。"


translate chinese fail_mine_exit_f1661152:


    me "舒里克...这里谁也没有...我像你一样在矿井中走来走去，不过什么也没有听见。"


translate chinese fail_mine_exit_2c5335a6:


    "他盯着我。"


translate chinese fail_mine_exit_0495a2f5:


    sh "那么你是和他们一伙的！"


translate chinese fail_mine_exit_fabedc6a:


    "舒里克冲向我。"


translate chinese fail_mine_exit_4dcbc4d2:


    "我千钧一发中躲开了"


translate chinese fail_mine_exit_4ca30280:


    "不过手电筒就没有那么幸运了，它吃了舒里克一记重击。"


translate chinese fail_mine_exit_973c06e7:


    "我们回到了完全的黑暗之中。"


translate chinese fail_mine_exit_9978cb5a:


    "这次轮到我发疯了。"


translate chinese fail_mine_exit_6eeebb1a:


    me "你干什么，你个蠢货！！"


translate chinese fail_mine_exit_da6ba350:


    "我向着舒里克的方向挥出拳头，不过没有击中。"


translate chinese fail_mine_exit_68d73b99:


    me "现在咱们要一起死在这里了。"


translate chinese fail_mine_exit_e68105fc:


    "一阵寂静，然后又是一阵狂笑。"


translate chinese fail_mine_exit_222240df:


    sh "没关系...没事...我知道出口在哪里，跟我来！"


translate chinese fail_mine_exit_1b5cf4c1:


    me "怎么跟着你？我神马都看不见！"


translate chinese fail_mine_exit_78e3c7d3:


    sh "跟着我的声音。"


translate chinese fail_mine_exit_05321f51:


    "我没有别的选择。"


translate chinese fail_mine_exit_e7e27795:


    sh "这里..."


translate chinese fail_mine_exit_013b19be:


    "舒里克的声音移动到了远方。"


translate chinese fail_mine_exit_cb4b20ed:


    me "你怎么知道该怎么走？"


translate chinese fail_mine_exit_c98b8e2a:


    sh "我来到过这里..."


translate chinese fail_mine_exit_0cd4b3c9:


    me "那么你为什么不离开呢？"


translate chinese fail_mine_exit_88482955:


    sh "那个声音！...安静！他们能听见咱们！"


translate chinese fail_mine_exit_1e45ae53:


    "理论上我能相信这里有一些超自然现象。"


translate chinese fail_mine_exit_54f45c90:


    "在Совёнок夏令营这只是日常。"


translate chinese fail_mine_exit_06e526d9:


    "不过没有理由着急。"


translate chinese fail_mine_exit_e559fc54:


    "虽然我的手抖得根本停不下来。"


translate chinese fail_mine_exit_e7e27795_1:


    sh "这里..."


translate chinese fail_mine_exit_695378fd:


    "我慢慢跟着他的声音，没有了对时间的感觉。"


translate chinese fail_mine_exit_a5d3cc23:


    "我们可能漫游了几分钟...或者几个小时..."


translate chinese fail_mine_exit_3dae981c:


    "最终远处出现了一点昏暗的灯光。"


translate chinese fail_mine_exit_a370b3b4:


    sh "我们到了.."


translate chinese fail_mine_exit_9a15b181:


    "我们来到了一堵墙。{w}通往自由的道路被房顶上的一个篦子挡住了。"


translate chinese fail_mine_exit_0b2dcdb3:


    "我爬上去从视线中认出了这里是广场的一部分。"


translate chinese fail_mine_exit_49341337:


    me "把我举起来！"


translate chinese fail_mine_exit_56091405:


    "舒里克顺从的做着。"


translate chinese fail_mine_exit_f0562a64:


    "我过分用力的击打着那个井盖，结果弄得手上都是血..."


translate chinese fail_mine_exit_c8bf1db3:


    "不过相应的，盖子打开了。"


translate chinese fail_mine_exit_8a62a2ef:


    "一分钟以后我们回到了地面，在Genda的雕像背后。"


translate chinese fail_mine_exit_aefc775f:


    "当我看到这个篦子的时候可没有想到下面有一个地下墓穴..."


translate chinese fail_mine_exit_96505b67:


    sh "他们会来找你的！"


translate chinese fail_mine_exit_0735175d:


    "我累倒在地上。"


translate chinese fail_mine_exit_a444ac12:


    me "谁?"


translate chinese fail_mine_exit_823b8704:


    sh "他们会来的！"


translate chinese fail_mine_exit_0f567504:


    "舒里克又说了一次就跑掉了..."


translate chinese fail_mine_exit_7a6522f6:


    "我累的要命，什么都想不动了，什么声音，旧营地，防空洞，还有什么什么的。"


translate chinese fail_mine_exit_4d8a8dd9:


    "我几乎是没有意识地回到辅导员的房间的。"


translate chinese fail_mine_exit_6363b20e:


    "但是我还是不能马上去睡觉。"


translate chinese fail_mine_exit_f6e23c0c:


    mt "看看这是谁回来了！"


translate chinese fail_mine_exit_f10bec26:


    me "奥尔加·德米特里耶夫娜..."


translate chinese fail_mine_exit_ea404d77:


    "我疲惫的回答着，想要尽量缩短答案。"


translate chinese fail_mine_exit_1e3c4bff:


    mt "你说过你是去“散散步”，可是你却...！"


translate chinese fail_mine_exit_ffab041d:


    mt "我...我都准备叫警察了！{w}还有..."


translate chinese fail_mine_exit_4370a259:


    me "奥尔加·德米特里耶夫娜，能不能明天再说？我真的很累了。"


translate chinese fail_mine_exit_dfbc9991:


    "辅导员打量着我。"


translate chinese fail_mine_exit_7e1d09c7:


    mt "那好吧..."


translate chinese fail_mine_exit_5344f89f:


    "我有点惊讶她居然答应得如此之快，我决定不要浪费这个机会，迅速关上灯钻进了被窝。"


translate chinese fail_mine_exit_74343bec:


    "然后我的眼前出现了矿井中那个小女孩的身影。"


translate chinese fail_mine_exit_4ae428ab:


    "她是那么真实，让我感觉她就在我的床前。"


translate chinese fail_mine_exit_5fc49513:


    th "不，这是错觉..."


translate chinese fail_mine_exit_16730ca7:


    th "不过我当时为什么不害怕呢，只是和她说话了。"


translate chinese fail_mine_exit_ee91ad9c:


    th "答案都在这里，那个猫耳娘肯定知道些什么。"


translate chinese fail_mine_exit_0011fed6:


    "也许我是被旧营地，矿井还有迷宫什么的折腾的太累了，我的神经就在崩溃边缘。"


translate chinese fail_mine_exit_e12babdb:


    th "我应该问问她…"


translate chinese fail_mine_exit_4ab8d03c:


    th "不过我还有时间…"


translate chinese day4_uv_c936a5db:


    "下一个转弯处手电光照到了一处阴影。"


translate chinese day4_uv_df679a7b:


    "我颤抖着，不敢迈出一步。"


translate chinese day4_uv_52523f03:


    "只有当我想到那个人可能是舒里克的时候我才从僵硬中稍稍恢复。"


translate chinese day4_uv_b01a7f13:


    me "舒里克!"


translate chinese day4_uv_f8c43a30:


    "没有回答…"


translate chinese day4_uv_213cd37d:


    "阴影在角落里消失了，我看到一个模糊的阴影…"


translate chinese day4_uv_59b5788c:


    "有人坐在石头上{w}或者是别的什么…"


translate chinese day4_uv_480f771d:


    "我的腿像长在了地上一样无法移动。"


translate chinese day4_uv_2821e6eb:


    me "你在这里做什么？"


translate chinese day4_uv_51e9a976:


    "我听到了一个可爱的声音："


translate chinese day4_uv_6add52f7:


    uvp "抓老鼠。"


translate chinese day4_uv_88e92f6a:


    "我张开了嘴…{w} …但是什么都没有说出来。"


translate chinese day4_uv_51149e68:


    me "…"


translate chinese day4_uv_c7528539:


    uvp "你在这儿见到过吗？"


translate chinese day4_uv_d695b46b:


    me "没有，不过我确定有很多…"


translate chinese day4_uv_a4cdf691:


    "我用手电照照她的脸…"


translate chinese day4_uv_3abd8a8c:


    "这要么是她的脑袋出了什么问题，要不然就是她的种族有什么问题。她长着耳朵和尾巴，和人类不是完全一样。"


translate chinese day4_uv_157dacf2:


    me "你抓到了多少呢？"


translate chinese day4_uv_02281cca:


    uvp "一只都没有抓到。"


translate chinese day4_uv_3d4f4933:


    "她严肃的回答道。"


translate chinese day4_uv_e2d148b8:


    "她看起来不是很危险，我潜意识里觉得不必害怕她，不过我完全不知道该问她什么，怎么问，她是谁，她在干什么。"


translate chinese day4_uv_5be41974:


    "不过另一方面来说，平常你如果真的在一个废弃的矿井里看到一个人绝对会吓出心脏病的。"


translate chinese day4_uv_11cfe5f6:


    "而且我面前的这个女孩绝对不是这个营地里的少先队员！"


translate chinese day4_uv_3c5fb85c:


    me "你...你是谁？"


translate chinese day4_uv_2372d07d:


    uvp "你大概是在找{i}他{/i}..."


translate chinese day4_uv_2a985733:


    me "谁？"


translate chinese day4_uv_b9a21822:


    uvp "那个男孩..."


translate chinese day4_uv_ea6d6d42:


    me "嗯...{w}是的..."


translate chinese day4_uv_8fd15dcf:


    "我停顿了一下，回答道。"


translate chinese day4_uv_6bae3f57:


    me "不过..."


translate chinese day4_uv_e657d961:


    "女孩子打断了我。"


translate chinese day4_uv_9d150002:


    uvp "他在这里走了很长时间了。{w}我想要和他说话，不过他不肯听。"


translate chinese day4_uv_5c1c58e4:


    me "他现在在哪里呢？"


translate chinese day4_uv_813a06f8:


    uvp "不知道...{w}可能还在附近徘徊吧。"


translate chinese day4_uv_e48430c4:


    "她似乎对这段对话完全不感兴趣。"


translate chinese day4_uv_75223819:


    me "你呢...你是...怎么到这里的？"


translate chinese day4_uv_b4a3ab00:


    uvp "和你一样，从地面上来。"


translate chinese day4_uv_1654f26c:


    me "没有手电？"


translate chinese day4_uv_f2374f2b:


    uvp "为什么呢？"


translate chinese day4_uv_d222fc60:


    "她困惑的看着我。"


translate chinese day4_uv_2250eefe:


    "我完全理解不了。"


translate chinese day4_uv_eec7a97d:


    "手电闪了一下，我吓到了，不停地用手击打它。"


translate chinese day4_uv_a5894410:


    "当它再次亮起来的时候女孩已经不在那里了..."


translate chinese day4_uv_38eb0973:


    th "我不知道她是谁，不过看起来她可以帮助我找到答案。"


translate chinese day4_uv_4173197c:


    "这很奇怪，但是我并不害怕，我觉得那个女孩并不会害我。"


translate chinese day4_uv_9e917b0c:


    "但是我必须找到舒里克！"


translate chinese day4_uv_8288c2fa:


    th "我有一种还会和这个女孩见面的感觉..."


translate chinese day4_uv_53d1fea3:


    "不知道为什么，但是我可以确定。"


translate chinese day4_sl_45708cef:


    sl "我和他去！"


translate chinese day4_sl_678c7ff3:


    mt "很好！一起走要好一些。"


translate chinese day4_sl_238ce8d6:


    me "你确定吗？"


translate chinese day4_sl_30273633:


    "我悄悄的问着斯拉维娅。"


translate chinese day4_sl_ec7a09a0:


    "我想要确定，但是..."


translate chinese day4_sl_bf4362df:


    "她没有回答，只是笑了笑。"


translate chinese day4_sl_3c3e2229:


    "我默默地看着斯拉维娅。"


translate chinese day4_sl_007619f0:


    th "她为什么总是帮助我呢？"


translate chinese day4_sl_d905d2f2:


    mt "非常好！祝你们两个好运。"


translate chinese day4_sl_b904fd9e:


    me "这是很必要的..."


translate chinese day4_sl_ed27161f:


    "辅导员和其他的女生说过再见之后走向她们自己的路。"


translate chinese day4_sl_13a18ea4:


    "我很奇怪...她们竟然在晚上安排...穿过森林...去旧营地的活动...还觉得这很正常。"


translate chinese day4_sl_0bfe3d69:


    th "呃，这也没什么用。"


translate chinese day4_sl_2dbd94e3:


    sl "等一下，我去找一个手电来。"


translate chinese day4_sl_1279b922:


    th "对啊，一个手电..."


translate chinese day4_sl_078dc3eb:


    "我等着斯拉维娅的时候，可是思考一个奇怪的问题。"


translate chinese day4_sl_9047a687:


    th "她为什么这么善良，有同情心，随时准备帮助他人？"


translate chinese day4_sl_78b4cda5:


    th "为什么她愿意和我去一个莫名其妙的地方呢？"


translate chinese day4_sl_6e630e74:


    th "静水流深…"


translate chinese day4_sl_07db949a:


    th "我应该关心一下她吗？"


translate chinese day4_sl_0cebc928:


    sl "我回来了！"


translate chinese day4_sl_f1ebc68b:


    "斯拉维娅笑着递给我手电筒。"


translate chinese day4_sl_ad3d2e0c:


    sl "咱们走吗？"


translate chinese day4_sl_7bd87323:


    "听电子小子说，旧营地是战争刚刚结束的时候修建的。"


translate chinese day4_sl_fd3c1cdc:


    "它看起来像是一个幼儿园，或者是什么兵营，看起来装不下现在这么多的人。"


translate chinese day4_sl_c4f7341a:


    "它已经废弃了二十年了…"


translate chinese day4_sl_a52ca566:


    "现在完全黑下来了。"


translate chinese day4_sl_3868483b:


    "森林好像像我们害怕它一样害怕我们。{w}树木为我们让出小路，繁茂的枝叶静静的看着我们。"


translate chinese day4_sl_59e3e4f7:


    "一只猫头鹰“咕咕”的叫着，不过声音也不大。"


translate chinese day4_sl_6dd60970:


    "我走在斯拉维娅的旁边，并没有感到害怕。"


translate chinese day4_sl_560f6e52:


    "大概在任何其他的情况下走这样的森林小路我都会吓得不行吧。"


translate chinese day4_sl_be70614e:


    "我试着摆脱这些想法。"


translate chinese day4_sl_e29f96bd:


    "虽然这很奇怪，不过斯拉维娅似乎让我的警惕心理稍微降低了一些，让我不那么紧张了。"


translate chinese day4_sl_78eb5981:


    "按照这个势头的话，我大概可以微笑着冲向一头野兽…"


translate chinese day4_sl_0b419961:


    me "你一点也不害怕吧？"


translate chinese day4_sl_63f53767:


    sl "什么意思?"


translate chinese day4_sl_ab195626:


    "斯拉维娅转过来看着我，奇怪的笑着，也许是有一点不解，也许是有一点嘲笑。{w}或者也许只是我看错了。"


translate chinese day4_sl_d91626f2:


    me "那个...夜晚，旧营地..."


translate chinese day4_sl_1bd8d8f4:


    sl "是啊，有一点毛骨悚然的感觉。"


translate chinese day4_sl_cc73ad1f:


    me "只是有一点吗？"


translate chinese day4_sl_1b5187ef:


    sl "你害怕吗？"


translate chinese day4_sl_8e0381aa:


    me "没...是...在这种情况下人本能的会害怕吧？"


translate chinese day4_sl_4891112d:


    "我不想显得自己有多么害怕，但是我也真的不是很害怕，我只是觉得在这种情况下害怕是很正常的。"


translate chinese day4_sl_9f24fbc1:


    "就好像是我的身体不在产生抗体抵御感染了。{w}我害怕不是因为这森林，而是在面对种种奇怪的现象时的反应。"


translate chinese day4_sl_f337bb5a:


    "再说，我也不想显得傻傻的。"


translate chinese day4_sl_01e9adbb:


    sl "嗯。"


translate chinese day4_sl_4fb83426:


    "斯拉维娅又笑了起来，继续走着。"


translate chinese day4_sl_a20cefa7:


    "..."


translate chinese day4_sl_68cdaa0d:


    "被树木和灌木丛阻隔的小径越来越窄了。"


translate chinese day4_sl_42ad9c81:


    "我们越是往森林的深处走，月亮就越是显得模糊。"


translate chinese day4_sl_eb593e12:


    "有一段时间月亮被云彩挡住，我们几乎陷入了彻底的黑暗。"


translate chinese day4_sl_cbdffea8:


    "我紧紧的闭上眼睛，当我再一次睁开的时候，月光又重新洒向了大地。"


translate chinese day4_sl_50e0695f:


    th "在这个时候我才意识到自己是多么的痛恨黑暗！"


translate chinese day4_sl_368ffb97:


    "终于，前面出现了一片空地，几分钟之后我们来到了这块树林环绕的地方。"


translate chinese day4_sl_92efe0b6:


    "一座摇摇欲坠的旧营地建筑，被雨水、锈迹还有虫子蛀蚀得破旧不堪。"


translate chinese day4_sl_fb10b433:


    "破碎的窗户就像是露出充满齿缝的牙的一个笑容，其中充满了死者对生者无声的责备。"


translate chinese day4_sl_b33715a3:


    "一股墓地的雾气笼罩着这里，似乎恶灵，僵尸和魔鬼在其中若隐若现。"


translate chinese day4_sl_28fa12ec:


    "这种景象可以让一般的人犯心脏病。"


translate chinese day4_sl_f4b1d738:


    "如果没有斯拉维娅拉着我的手，我肯定不能正常的站在这里了…"


translate chinese day4_sl_0ab7ae30:


    sl "这样就不会那么害怕了。"


translate chinese day4_sl_06ab0532:


    "她笑了。"


translate chinese day4_sl_9cfcb779:


    "我什么都说不出来，只能紧紧的抓着她的手。"


translate chinese day4_sl_f65f03d8:


    th "我到底在这里干什么？{w}晚上在这里找舒里克？"


translate chinese day4_sl_5cc597fa:


    th "我为什么会同意？{w}正常人绝对不会..."


translate chinese day4_sl_3a771d0d:


    th "但是我现在所处的这个世界绝对不能叫做正常。"


translate chinese day4_sl_f86ea402:


    "我试着劝说自己正在寻找答案，而且情况在变得越来越好。"


translate chinese day4_sl_0ef9ad1a:


    "说实话，过去的四天里没有什么不好的事情发生。而且我也没有什么理由认为会有什么不好的事情发生。"


translate chinese day4_sl_4ddccd16:


    "也许这就是这个世界的法则？人们无法接受没有冒险的生活？"


translate chinese day4_sl_ad3d2e0c_1:


    sl "咱们可以走了吗？"


translate chinese day4_sl_9776d559:


    me "嗯，是的，我想..."


translate chinese day4_sl_0f0cb0fd:


    "但是我们并没有前进。"


translate chinese day4_sl_05bba4b4:


    "斯拉维娅似乎在等着我迈出第一步，但是我站在这里没有逃走已经是极限了。"


translate chinese day4_sl_95f1e61d:


    "月亮再一次从云层后边露出来，照亮了整个建筑。"


translate chinese day4_sl_682e5f18:


    "现在它看起来不再那么像是一个墓地了，不过我大脑中的景象确实越来越清晰了。鬼魂的形象还有呼啸的风吹草动，还有远处不可名状的声音。"


translate chinese day4_sl_0bcb6a42:


    sl "看起来是个好地方..."


translate chinese day4_sl_6cfd5fba:


    "我小心的看着斯拉维娅。"


translate chinese day4_sl_3059e4dc:


    "也许她更加害怕，不过她在试着隐藏起来。{w}我因此变得镇定了一些。"


translate chinese day4_sl_b55719b1:


    me "也许，咱们应该走了？"


translate chinese day4_sl_7b8fbab7:


    sl "是啊..."


translate chinese day4_sl_57efbd93:


    "我小心点迈着步子，尽量不碰到破旧的篱笆，扭曲的旋转木马，还有生锈的滑梯。"


translate chinese day4_sl_031a7b86:


    "有些地方的草丛及腰高，一不小心就有可能受伤。"


translate chinese day4_sl_b0c6ca01:


    "最后我们来到了入口处，我用手电筒指着里面。"


translate chinese day4_sl_ca84f49d:


    "我面前的黑暗比室外还要可怕，至少田野上还有月光。"


translate chinese day4_sl_1da1942d:


    "我凝聚起全身的勇气，跨过了门槛。"


translate chinese day4_sl_2eb693a0:


    "旧营地的内部看起来就像是我小时候的幼儿园，内部的布局，甚至连破损的家具也一样。"


translate chinese day4_sl_c63ec5a4:


    me "那个..."


translate chinese day4_sl_2b54d2f5:


    "我颤抖着，在灯光中发现越来越多的已经消失的孩子们的曾经的欢乐。"


translate chinese day4_sl_69664b95:


    sl "好恐怖。"


translate chinese day4_sl_5eabb736:


    "斯拉维娅现在不仅是攥着我的手，而是抱着我，用力的贴到我身上。"


translate chinese day4_sl_85558387:


    "不过我从她的声音里听不到害怕。"


translate chinese day4_sl_592fe89e:


    me "半夜来这个地方真的好吗？"


translate chinese day4_sl_e330b23b:


    sl "大概不太好。"


translate chinese day4_sl_438cf7fc:


    me "咱们是不是应该往回走了？"


translate chinese day4_sl_251c7d47:


    sl "不！舒里克怎么办？"


translate chinese day4_sl_9a478370:


    "她很惊讶的问道。"


translate chinese day4_sl_6d33a4b2:


    me "嗯，舒里克...我不明白他为什么要来这个地方？也许他根本不在这里？"


translate chinese day4_sl_1a99efc0:


    sl "他还有可能在哪里？"


translate chinese day4_sl_43f9ae8d:


    me "不知道，被狼吃了吧，也许？"


translate chinese day4_sl_290c429f:


    sl "别那么说！"


translate chinese day4_sl_167a71f8:


    "斯拉维娅皱着眉头向后退了一步。"


translate chinese day4_sl_09003bb4:


    "我已经习惯和她的亲近感，被她责备之后有些失落。"


translate chinese day4_sl_ff0e577b:


    me "嗯，好吧好吧... 咱们假设没有狼。"


translate chinese day4_sl_d8ada2af:


    th "不过这可是不确定..."


translate chinese day4_sl_74000c71:


    me "算了，既然都已经到了这里了，就开始搜索吧。"


translate chinese day4_sl_ce617998:


    "..."


translate chinese day4_sl_5b164ba0:


    "我们找遍了旧营地的所有房间：活动室，卧室，食堂，厨房，还有厕所，我甚至还检查了衣柜。"


translate chinese day4_sl_693b20be:


    sl "看!"


translate chinese day4_sl_b212fb3a:


    "当我们回到开始的地方的时候，斯拉维娅从地板上捡起了什么递给我。"


translate chinese day4_sl_0ca58b5a:


    "是一个开了线的破旧娃娃，缺少了一只眼睛还有一只胳膊。"


translate chinese day4_sl_23104825:


    me "怎么了？"


translate chinese day4_sl_c22db2eb:


    sl "很可惜..."


translate chinese day4_sl_c1b9e2f6:


    me "什么？这个娃娃吗？"


translate chinese day4_sl_8b01f8be:


    sl "嗯。"


translate chinese day4_sl_d4817050:


    me "你为什么觉得可惜呢？"


translate chinese day4_sl_95ebdaf5:


    sl "曾经有人玩过，然后丢掉了她，她被遗忘在这个潮湿的废墟中很多很多年..."


translate chinese day4_sl_37bdb5c9:


    "我说不清斯拉维娅是不是认真的。"


translate chinese day4_sl_c14f498f:


    "我觉得现在不是想这些事情的时候。"


translate chinese day4_sl_0d679731:


    "再说了，什么时候适合想这些啊？"


translate chinese day4_sl_3ca74edc:


    "斯拉维娅看起来很失落。"


translate chinese day4_sl_d76f93fa:


    me "嗯。"


translate chinese day4_sl_34900be1:


    "我拿过那个娃娃检查起来。"


translate chinese day4_sl_48de1c46:


    "我记得自己曾经发现过一个玩具狮子，我把它带回家，仔细的清洗干净，就像我其他的玩具一样，虽然它早就应该退休了吧。"


translate chinese day4_sl_3ccc4d70:


    me "你应该把她给乌里扬卡。"


translate chinese day4_sl_c954d69f:


    "斯拉维娅笑了。"


translate chinese day4_sl_3ae7ec76:


    "突然一阵风吹过走廊，发出了惊悚的叫声。"


translate chinese day4_sl_0aa5972c:


    "一道光闪过，我本能的跳了起来，结果绊倒在地上。"


translate chinese day4_sl_7c5d32f6:


    sl "你还好吗？"


translate chinese day4_sl_28fa8946:


    "斯拉维娅跑过来扶我站起来。"


translate chinese day4_sl_e01d393f:


    me "啊，还好..."


translate chinese day4_sl_f92f7071:


    "我绊倒在地板上的一道暗门上。"


translate chinese day4_sl_e28dff1c:


    "很奇怪我们居然没有发现它，它周围的尘土被清理了，不久前曾经打开过。"


translate chinese day4_sl_473e195c:


    me "咱们不会下去吧？"


translate chinese day4_sl_0f373940:


    "我想要提前打断斯拉维娅的提议。"


translate chinese day4_sl_beaacb2a:


    "斯拉维娅悲伤的看着我。"


translate chinese day4_sl_3a450870:


    me "别啊...咱们是在玩矿工游戏吗？"


translate chinese day4_sl_e7af80fb:


    "我一边抱怨着，一边费力的打开门板。"


translate chinese day4_sl_637f8cc9:


    "我的常识在呐喊，告诉我不要这样做；不过我的理智告诉自己，如果这个世界想要杀掉我的话，我早就死了。"


translate chinese day4_sl_7e0c9bc6:


    "不过这是因为斯拉维娅！不然我可是不会下去的！"


translate chinese day4_sl_a133f4fb:


    "沿着两米长的梯子可以下降到下一层的地面。"


translate chinese day4_sl_ceabf4e4:


    "我靠着墙，照着前方向黑暗中延伸的小路。"


translate chinese day4_sl_fb03e6d3:


    me "那是什么？"


translate chinese day4_sl_030f1acd:


    sl "我好像听说过旧营地的防空洞的事情。"


translate chinese day4_sl_28acc263:


    me "防空洞？"


translate chinese day4_sl_d96d518d:


    "嗯，这倒是很合理，考虑到它的建造时间的话。"


translate chinese day4_sl_8017924d:


    me "舒里克为什么要来这个地方呢？"


translate chinese day4_sl_311a3a2f:


    sl "不知道。"


translate chinese day4_sl_a1d6e601:


    th "说起来打开机关的不一定是他，反正最近被打开过，不过谁都没有一个疯狂的发明家更像是嫌疑人了。"


translate chinese day4_sl_0eb09c36:


    th "如果我们不考虑超自然的现象，那么森林里应该没有别的人了。"


translate chinese day4_sl_91db26c0:


    "至少我是这么想的。"


translate chinese day4_sl_c068a23a:


    me "好吧，我先走，如果没有问题你就跟上来。"


translate chinese day4_sl_85ed45c7:


    sl "什么会有问题？"


translate chinese day4_sl_9cb98215:


    "斯拉维娅看起来很害怕，不过这个问题似乎更像是出于礼貌提出的。"


translate chinese day4_sl_60d825fa:


    me "那个，不知道...老鼠吧，比如说，你害怕老鼠吗？"


translate chinese day4_sl_84e0dd1c:


    th "看来我不应该问的。"


translate chinese day4_sl_73542db8:


    "我叹着气，开始向下爬。"


translate chinese day4_sl_ce617998_1:


    "..."


translate chinese day4_sl_2b476ea8:


    "我们牵着手慢慢前进。"


translate chinese day4_sl_e4392fea:


    "在彻底的黑暗之中，手电的光亮不太管用。只是偶尔会照到过去的报纸还有散落在各处的金属碎片。"


translate chinese day4_sl_3c66bee2:


    "突然我感觉墙壁和房顶都在向我压过来，好像这条小路变得更窄了。"


translate chinese day4_sl_3ffd1664:


    "我伸开胳膊，试图测量宽度。"


translate chinese day4_sl_f987b701:


    sl "还有很远吗？"


translate chinese day4_sl_cb8392a3:


    "斯拉维娅难受的问道。"


translate chinese day4_sl_8f2edfc1:


    me "距离哪里？我不清楚。"


translate chinese day4_sl_6f9aa3f1:


    sl "好吧..."


translate chinese day4_sl_af396620:


    "突然不知哪里出现了一座大铁门，矗立在我的面前。"


translate chinese day4_sl_f6458d5d:


    sl "哇..."


translate chinese day4_sl_1135b8b4:


    "斯拉维娅轻轻的哭了。"


translate chinese day4_sl_6d46c180:


    "我动了动把手，它似乎稍微松动了。"


translate chinese day4_sl_a752ccba:


    me "准备好了吗？"


translate chinese day4_sl_8a3c9e52:


    sl "不知道..."


translate chinese day4_sl_49678fc1:


    "虽然说实话，你在这种情况下有什么可以准备的？"


translate chinese day4_sl_4876f13f:


    "轮盘吱吱的转着，经过了几圈之后终于到头了。"


translate chinese day4_sl_a5571da6:


    "我用尽全身的力气拉动铁门，它不情愿的打开了。{w}后面有一个房间..."


translate chinese day4_sl_dfcada65:


    "原来就是传说中的防空洞。"


translate chinese day4_sl_40173f81:


    "当我们走进来的时候灯突然亮了，让我不禁向后退了一步。"


translate chinese day4_sl_2a01283b:


    "这里可能有一些独立的能源，当他们建造这些地下墓穴的时候可能已经过了几百年。"


translate chinese day4_sl_9d89915d:


    "防空洞看起来和我想象中的一样，有各种战时的必需品：箱子里装着生化服，防毒面具，压缩干粮还有水壶，远处还有各种装备：压力表，辐射计数器还有无线电什么的。"


translate chinese day4_sl_867bb71b:


    "不过这个地方好像从来没有用过。{w}甚至是从盖好之后就从来没有人进来过。"


translate chinese day4_sl_0fa153ce:


    "斯拉维娅好奇的看着我。"


translate chinese day4_sl_74d4b2c0:


    me "唔，舒里克肯定不在这里。"


translate chinese day4_sl_2e01cda6:


    sl "他来过吗？"


translate chinese day4_sl_ea7c0036:


    me "不清楚，不过如果他来过的话，不可能就这样凭空消失。"


translate chinese day4_sl_368d74f1:


    "我环视整个房间，发现在我左手边的墙上还有另外一扇门。"


translate chinese day4_sl_156f7cf2:


    me "也许是那里吧。"


translate chinese day4_sl_03dccc13:


    "不过这扇门打不开。"


translate chinese day4_sl_79177436:


    sl "会不会是他从那一边把门闩上了？"


translate chinese day4_sl_869dccdf:


    me "为什么呀？"


translate chinese day4_sl_8a3c9e52_1:


    sl "我不知道..."


translate chinese day4_sl_1f8bda75:


    "不过如果他在里面的话是有可能的。"


translate chinese day4_sl_820a03b7:


    "看起来舒里克好像躲起来了，躲某个人。"


translate chinese day4_sl_dde3d971:


    "这种想法让我很不舒服，我想赶紧到大门的那一边去。"


translate chinese day4_sl_5426c6ac:


    "我用力的推着，不过大门只是嘎吱的响着。"


translate chinese day4_sl_1fecc13d:


    sl "拿着，也许这个可以？"


translate chinese day4_sl_c2d7184d:


    "她递给我一个撬棍。"


translate chinese day4_sl_7b68d30d:


    "我摸索着怎样撬开这扇门。"


translate chinese day4_sl_33d34823:


    me "咱们来试一下。"


translate chinese day4_sl_74880975:


    "我用上所有的力气，把大门从合页上撬开，成功的让大门拍在了地上。"


translate chinese day4_sl_05aa00af:


    "当我在恢复体力的时候斯拉维娅用手电筒指向前方的道路。"


translate chinese day4_sl_00166c91:


    sl "这里有一条和之前差不多的走廊。"


translate chinese day4_sl_a2885c69:


    me "天啊，这个地方是个迷宫。"


translate chinese day4_sl_c6391d27:


    sl "可以走了吗？"


translate chinese day4_sl_079f62e5:


    me "等一下..."


translate chinese day4_sl_41c2ba2c:


    "我坐在一个床铺上，拽下枕头的布套擦着脸。"


translate chinese day4_sl_13ab9fc8:


    me "让我喘口气。"


translate chinese day4_sl_87f6e3eb:


    "斯拉维娅坐在我的旁边开始研究起来地板上的纹理。"


translate chinese day4_sl_48fd6cec:


    me "最好暂时把手电筒关掉。"


translate chinese day4_sl_406417b4:


    sl "哇，是啊..."


translate chinese day4_sl_aba53b88:


    "她稍有些脸红，关掉了手电。"


translate chinese day4_sl_de24b50e:


    me "好恐怖..."


translate chinese day4_sl_12cb2c61:


    "我发现自己没有把刚刚半个小时发生的事情当成真事。"


translate chinese day4_sl_87863660:


    "好像不是发生在我身上的，或者是一个梦一样。"


translate chinese day4_sl_6a0d0b3e:


    "就像是四天以前我刚刚发现自己来到这里时一样。"


translate chinese day4_sl_79ab8f59:


    "是啊我后来开始逐渐的适应这种“现实”。"


translate chinese day4_sl_c2db5cf0:


    "我内心深处想要逃走的想法不再那么急迫了。"


translate chinese day4_sl_a00907f6:


    "不过现在看看：防空洞，森林，旧营地..."


translate chinese day4_sl_3c3e55db:


    "这一切都不像是超自然的东西，不过这一切也都不像是正常的——就像是我原来的生活一样。"


translate chinese day4_sl_7b8fbab7_1:


    sl "是啊..."


translate chinese day4_sl_a4965e91:


    "斯拉维娅表示同意。"


translate chinese day4_sl_00992d89:


    me "你不觉得这一切都有些奇怪吗？"


translate chinese day4_sl_a96c222e:


    sl "具体是什么事情呢？"


translate chinese day4_sl_5aaaf3dd:


    me "就是说咱们晚上坐在这里这件事情啊，正常的话不是应该等到明天早上然后叫警察吗？"


translate chinese day4_sl_33cb1281:


    sl "是啊，应该是的。"


translate chinese day4_sl_5e20fc4d:


    "她若有所思的回答着。"


translate chinese day4_sl_aca724aa:


    me "当然我几乎是自愿加入的，早知道..."


translate chinese day4_sl_e9545c3b:


    "不过“几乎”..."


translate chinese day4_sl_0f83eee7:


    sl "但是你不是想要帮忙吗，那么咱们应该尽快找到舒里克。"


translate chinese day4_sl_6577f1d4:


    me "这种情况下咱们也是他们要找的人。"


translate chinese day4_sl_a336bbb9:


    "斯拉维娅看起来很沮丧。"


translate chinese day4_sl_6e2ae7df:


    me "好啦好啦，我不是那个意思啦，也许舒里克已经回去了，回到营地去了。"


translate chinese day4_sl_c05d1b8c:


    sl "是啊，也许是。"


translate chinese day4_sl_7b411858:


    me "不过咱们还是应该检查一下，对吧？"


translate chinese day4_sl_49cc0ae4:


    sl "当然！"


translate chinese day4_sl_d6679b71:


    "斯拉维娅站起来，向我伸出了手。"


translate chinese day4_sl_50e89832:


    "似乎她不像我一样害怕。"


translate chinese day4_sl_555bb396:


    "虽然我已经厌烦了自己害怕的心情，我现在唯一想的事情就是赶紧把这件事干完。"


translate chinese day4_sl_ce617998_2:


    "...."


translate chinese day4_sl_8d29725f:


    "走廊和来的时候一模一样。"


translate chinese day4_sl_c77a261c:


    "我谨慎的慢慢走着，不停检查着走廊是不是变窄了。"


translate chinese day4_sl_d14ade49:


    sl "看!"


translate chinese day4_sl_9f8002ec:


    "斯拉维娅紧紧的拉着我，指向隧道中间的一个大裂缝。"


translate chinese day4_sl_5197c9ee:


    "足够一个人爬过去了，或者是掉下去。"


translate chinese day4_sl_02b0275d:


    sl "也许..."


translate chinese day4_sl_62129d64:


    "我照向下方，不过似乎除了下面的地面没有什么别的。"


translate chinese day4_sl_642193e3:


    me "我觉得咱们应该检查一下。"


translate chinese day4_sl_3c17ab25:


    "这个洞足够浅，我们一会儿可以爬出来，所以目前应该没有什么害怕的。"


translate chinese day4_sl_bdf18d28:


    "我爬下去，然后帮着斯拉维娅下来。"


translate chinese day4_sl_7a9a5a51:


    "我们好像来到了一个矿井。"


translate chinese day4_sl_171403e3:


    "墙体上安装了加固用的木制柱子和横梁。"


translate chinese day4_sl_3428662f:


    "岩石和泥土不停地压着这个结构，让人感觉十分不靠谱。"


translate chinese day4_sl_a5f51cbc:


    sl "你觉得咱们在什么地方？"


translate chinese day4_sl_ee72963d:


    me "不知道，一个矿井吧，他们可以在这里挖什么？"


translate chinese day4_sl_80b2ad66:


    sl "可能是煤？"


translate chinese day4_sl_0be49839:


    me "可能..."


translate chinese day4_sl_c39ad14a:


    "我们开始慢慢的前进。"


translate chinese day4_sl_29351dec:


    "脚下的石板飞速的闪过，手电的灯光照射在墙壁上的光影不停的跳动。"


translate chinese day4_sl_253c2530:


    sl "这里比上面还要恐怖..."


translate chinese day4_sl_1066065f:


    me "好像是的。"


translate chinese day4_sl_86ad247f:


    "我们越是往前走，我越是觉得舒里克绝对不会在这个地方转来转去！"


translate chinese day4_sl_a28736d8:


    th "他不会是被一只吸血怪抓走了吧？{w}好吧，我们也不能排除那种可能性。"


translate chinese day4_sl_2c6a97fe:


    "后来我们来到了一个岔路口。"


translate chinese day4_sl_11667520:


    sl "现在应该往哪里走？"


translate chinese day4_sl_595d7b8c:


    me "不知道。"


translate chinese day4_sl_2d6b4b1d:


    "我试着往自己的声音里加入一些震惊，不过这我真的不知道。{w}完全不知道：该往哪里走，舒里克在哪里，我们在哪里。"


translate chinese day4_sl_986e3d9c:


    sl "咱们要是迷路了怎么办？"


translate chinese day4_sl_afcce3c5:


    me "啊，是啊，在这里做一个记号吧。"


translate chinese day4_sl_97280ee5:


    "我捡起了一块挺大的石头在木头柱子上画了一个叉。"


translate chinese day4_sl_05bb4d2a:


    me "好了，这下再走到这里的时候就能知道了。"


translate chinese day4_sl_af1d965e:


    sl "好的。"


translate chinese day4_sl_c17a722d:


    "斯拉维娅笑了。"


translate chinese day4_sl_bea7cbc5:


    sl "那么应该往哪里走呢？"


translate chinese day4_sl_ce617998_3:


    "..."


translate chinese sl_mine_coalface_e40597aa:


    "我们来到过这个地方。"


translate chinese sl_mine_coalface_5b4c3c90:


    "最后我们走出了隧道，来到了一个有高高屋顶的大厅。"


translate chinese sl_mine_coalface_3ef8c583:


    "虽然说叫做大厅不太合适，他们过去一定是在这里挖过矿。{w}大概是煤，也许是金子。"


translate chinese sl_mine_coalface_4253b858:


    "墙面有十字镐和气钻凿过的痕迹。"


translate chinese sl_mine_coalface_c718c063:


    "这个地方一片漆黑，所以我们只能依赖手电筒了。"


translate chinese sl_mine_coalface_b97863a3:


    th "如果这里坍塌的话，我们大概是没有机会逃出去的..."


translate chinese sl_mine_coalface_58411121:


    "借着灯光，我看到了角落里的一块红色的布。"


translate chinese sl_mine_coalface_ca0a4f90:


    "是一条红领巾！"


translate chinese sl_mine_coalface_96b3272c:


    "显然舒里克来到过这里！"


translate chinese sl_mine_coalface_93ec21f7:


    me "舒里克！舒里克！"


translate chinese sl_mine_coalface_cc29db24:


    sl "舒里克！舒里克！"


translate chinese sl_mine_coalface_b99be3e2:


    "只有回声回答着我们。"


translate chinese sl_mine_coalface_e27745b6:


    sl "别担心，他一定没事的！"


translate chinese sl_mine_coalface_b894ecef:


    "说实话，我现在最担心的是自己..."


translate chinese sl_mine_coalface_a411960f:


    th "不过他从这里可以去哪儿呢？"


translate chinese sl_mine_coalface_55719814:


    "这个房间没有出口..."


translate chinese sl_mine_coalface_2b95387f:


    th "当然，这个隧道里肯定还有我们没有路过的地方。"


translate chinese sl_mine_coalface_858b6cbe:


    th "我们还需要继续搜索！"


translate chinese sl_mine_exit_e56eea78:


    "又经过一个转弯后，灯光中能看到地板。"


translate chinese sl_mine_exit_233631e4:


    me "哈，至少有了一点发现。"


translate chinese sl_mine_exit_14491239:


    "说真的，我在这隧道里已经转的精疲力尽，只想赶快找到出口。"


translate chinese sl_mine_exit_920627d5:


    sl "后面是什么？"


translate chinese sl_mine_exit_fac00056:


    me "咱们现在看看。"


translate chinese sl_mine_exit_dc9f0445:


    "打开门，那边是一个小房间，要么是一个锅炉房的工具间，要么是防空洞的另一部分。"


translate chinese sl_mine_exit_d4b4511f:


    "地板上扔着空瓶子，墙上画着乱七八糟的壁画。说明在我们之前有人不止一次的来到过这个地方。{w}虽然从这些东西上判断，那应该是很久以前了。"


translate chinese sl_mine_exit_b5d787b8:


    "我晃动着手电，试图照亮房间的各个角落，这时我突然发现了..."


translate chinese sl_mine_exit_7d62949b:


    "舒里克蜷缩在角落里。"


translate chinese sl_mine_exit_a7c6dc11:


    sl "舒里克！"


translate chinese sl_mine_exit_83931b19:


    "斯拉维娅哭喊出来。"


translate chinese sl_mine_exit_54f665b3:


    "不过看起来他好像没有注意到我们。"


translate chinese sl_mine_exit_dd1de183:


    me "你在这里啊，我们找你找了一个晚上了，你来这里干什么啊？！"


translate chinese sl_mine_exit_959f145c:


    "舒里克抬起头，用他迷离的眼睛心不在焉的看着我。"


translate chinese sl_mine_exit_f335a763:


    sh "你是谁？"


translate chinese sl_mine_exit_7393b21b:


    me "什么叫你是谁？快起来跟我们走啊！"


translate chinese sl_mine_exit_5b4d50f1:


    sh "我哪里也不打算去！"


translate chinese sl_mine_exit_50ed4f02:


    "舒里克转过身去继续盯着墙角。"


translate chinese sl_mine_exit_27e3ed3d:


    sh "你想要耍我！你想要让我再进到隧道里绕不出来，是不是！"


translate chinese sl_mine_exit_284e0011:


    "舒里克压低声音恶狠狠的说着。"


translate chinese sl_mine_exit_fe9c97c8:


    me "少废话。"


translate chinese sl_mine_exit_3069c4df:


    "我想要上前把他拉起来，不过斯拉维娅阻止了我。"


translate chinese sl_mine_exit_f9e442d6:


    sl "他不太像他自己。"


translate chinese sl_mine_exit_a13f5a0a:


    me "哈，我也不是我自己！在这种地方转一晚上，肯定已经不正常了。"


translate chinese sl_mine_exit_f98911a4:


    "她摇摇头，开始小心的接近舒里克。"


translate chinese sl_mine_exit_80764f4c:


    sl "没关系，是我，斯拉维娅。"


translate chinese sl_mine_exit_af188dfb:


    sh "真的？"


translate chinese sl_mine_exit_be9d5df3:


    "舒里克看着她，马上开始抽泣起来。"


translate chinese sl_mine_exit_5e2b81c3:


    sl "当然，还能是谁啊？还有那是谢苗，我们来找你了！"


translate chinese sl_mine_exit_0be700a9:


    sh "你真的不是他们吗？"


translate chinese sl_mine_exit_553577fe:


    sl "当然不是！我们是我们！"


translate chinese sl_mine_exit_3002543e:


    "他试着站起来，不过如果不是斯拉维娅的话，他就因为失去平衡摔倒了。"


translate chinese sl_mine_exit_dbdfe385:


    sl "小心，看好脚下！"


translate chinese sl_mine_exit_740d40c9:


    "他们走过来之后，我说："


translate chinese sl_mine_exit_a47d7467:


    me "好了，咱们该走了！"


translate chinese sl_mine_exit_05d7a2e2:


    sh "不行，不能回去..."


translate chinese sl_mine_exit_52f884ab:


    me "回哪里？矿井吗？"


translate chinese sl_mine_exit_2ba450b2:


    sh "是的。"


translate chinese sl_mine_exit_24404a66:


    "舒里克冷静的说道，声音中带着一丝颤抖。"


translate chinese sl_mine_exit_5ac38cf1:


    me "为什么？"


translate chinese sl_mine_exit_798ba618:


    sh "“他们”在那里！"


translate chinese sl_mine_exit_f1b8c285:


    me "他们是谁？"


translate chinese sl_mine_exit_83aa975e:


    sh "那些声音。"


translate chinese sl_mine_exit_777da593:


    me "什么声音？"


translate chinese sl_mine_exit_25a17e79:


    "我要开始失去耐心了。"


translate chinese sl_mine_exit_5d4ebca9:


    "这个地方是不太正常，不过如果真的有谁的话，那么根据恐怖片的理论，他应该早就现身了。"


translate chinese sl_mine_exit_15266f71:


    sl "冷静，舒里克，和我们说说。"


translate chinese sl_mine_exit_aef80036:


    "他深吸了一口气，开始讲道："


translate chinese sl_mine_exit_25825ae0:


    sh "我需要给机器人找一些零件。{w}然后我听说在旧的营地那边有一个防空洞..."


translate chinese sl_mine_exit_3b6c99a8:


    sh "所以我早晨起床前往这里，如果我走得快的话也许能在早饭之前回到营地。"


translate chinese sl_mine_exit_579e2e33:


    sh "我找到了合适的零件。"


translate chinese sl_mine_exit_c03bb0fe:


    "他从口袋里拿出一些灯泡和电线什么的给我们看。"


translate chinese sl_mine_exit_9d133ba2:


    sh "不过接下来我听到防空洞那里有呻吟的声音，我想那可能是有人被困在里面求救，所以..."


translate chinese sl_mine_exit_9ec05142:


    sh "然后就是这个矿井！这个迷宫！还有那个声音，我听不清说的是什么。"


translate chinese sl_mine_exit_b022d1a8:


    "他快要哭出来了，不过斯拉维娅轻轻的拍拍他的头，然后他又继续说："


translate chinese sl_mine_exit_e48ebc1f:


    sh "总之我找到了这个房间，在这里你听不到“他们”..."


translate chinese sl_mine_exit_1eeae244:


    me "所以说你就在这里等着救援？就像是B级片一样？"


translate chinese sl_mine_exit_0f9dcec3:


    "我讽刺的问道。"


translate chinese sl_mine_exit_a783d9b3:


    "斯拉维娅愤怒的看了我一眼。"


translate chinese sl_mine_exit_d5aa4a23:


    me "不管怎么说，咱们赶紧离开这个地方吧！"


translate chinese sl_mine_exit_64d006fa:


    sh "来吧，我知道一条近路！"


translate chinese sl_mine_exit_1f5418a3:


    me "近路？"


translate chinese sl_mine_exit_d3c3faf7:


    sh "是啊，我已经走遍这个地方了。"


translate chinese sl_mine_exit_5fabfeff:


    me "好吧..."


translate chinese sl_mine_exit_a20cefa7:


    "..."


translate chinese sl_mine_exit_31a38a29:


    "舒里克没有撒谎。"


translate chinese sl_mine_exit_8fb267ae:


    "过了不到两分钟，我们就来到了一个井盖下面，穿过篦子可以看到天空。"


translate chinese sl_mine_exit_401125ca:


    me "你为什么自己不出去？"


translate chinese sl_mine_exit_9fd952a9:


    sh "你试试就知道了。"


translate chinese sl_mine_exit_34b807b5:


    "我试着拉动盖子。"


translate chinese sl_mine_exit_59e04ffd:


    th "确实不能这样子打开，大概需要用什么重物..."


translate chinese sl_mine_exit_e1eb6421:


    "我向下看着斯拉维娅和舒里克。"


translate chinese sl_mine_exit_ea249dfd:


    sl "也许可以试试手电筒？"


translate chinese sl_mine_exit_af0c14fb:


    me "手电筒..."


translate chinese sl_mine_exit_b7873ad2:


    "我仔细的检查了一下这个手电筒，它不是上世纪90年代的中国便宜货，是一把结实的苏联货。{w}肯定不如一个榔头，但是如果我够用力的话..."


translate chinese sl_mine_exit_68320b1d:


    "我瞄着它的连接处，挥起手电，重重的砸了上去。"


translate chinese sl_mine_exit_67cc1ddd:


    "运气不错，我听到了什么碎掉的声音，然后掉下来几个小零件。"


translate chinese sl_mine_exit_ce617998:


    "..."


translate chinese sl_mine_exit_fb1e61f2:


    "过了一分钟，我已经躺在雕塑下方的草坪上舒服的呼吸着新鲜的空气了。{w}虽然从矿井里出来以后怎样的空气都会显得新鲜了。"


translate chinese sl_mine_exit_b4b29936:


    sh "好吧，我要走了，多谢..."


translate chinese sl_mine_exit_3598f2d1:


    "舒里克看起来有一点迷惑。"


translate chinese sl_mine_exit_52f39431:


    "我奇怪的看着他，不过没有想出来要说什么，只是挥挥手和他说了再见。"


translate chinese sl_mine_exit_da4e05bd:


    "再说我已经没有力气和他聊什么了。"


translate chinese sl_mine_exit_e0453692:


    th "而且我和他有什么可说的？{w}批评他没有什么用，而且也问不出什么来。"


translate chinese sl_mine_exit_0df46cc3:


    sl "这个晚上真是..."


translate chinese sl_mine_exit_07fd16f1:


    "斯拉维娅坐在我旁边，呆呆的看着月亮。"


translate chinese sl_mine_exit_068c4c7d:


    me "话说，如果有人一个星期之前告诉我会来到一个少先队员夏令营，还有可能钻防空洞..."


translate chinese sl_mine_exit_a2b22e6d:


    sl "但是这很有趣。"


translate chinese sl_mine_exit_c15d505f:


    me "从某种方面来说。"


translate chinese sl_mine_exit_5e8427f4:


    "这确实有点意思。{w}不过也有一些奇怪的事情：在刚刚的几个小时中，本来有可能发生任何事情，结果什么都没有发生。"


translate chinese sl_mine_exit_1ca3789d:


    "多年以后，我只会记得是小时候的一件趣事。{w}如果我活的足够长的话。"


translate chinese sl_mine_exit_fa7152d2:


    me "你怎么认为，你觉得他真的听到什么了吗？"


translate chinese sl_mine_exit_9fee39ea:


    sl "不知道，这是有可能的，不过更像是他自己的想象，你知道，在这种情况下，谁都有可能出现幻觉。"


translate chinese sl_mine_exit_b35d647f:


    "这是一种解释，不过只是一种可能，因为我和斯拉维娅就什么都没有遇到。"


translate chinese sl_mine_exit_defa6d12:


    me "呃，我觉得舒里克可以去看看心理医生。"


translate chinese sl_mine_exit_e8c8afba:


    sl "我看也是！"


translate chinese sl_mine_exit_c954d69f:


    "斯拉维娅笑着。"


translate chinese sl_mine_exit_e086255d:


    sl "好了，该到了睡觉时间了。"


translate chinese sl_mine_exit_8613242c:


    me "嗯。"


translate chinese sl_mine_exit_65810382:


    "我觉得自己应该对斯拉维娅说些什么。{w}我不知道自己是不是想要这样做，不过大概是必要的。"


translate chinese sl_mine_exit_8753f434:


    me "谢谢你！"


translate chinese sl_mine_exit_306a3019:


    sl "关于什么？"


translate chinese sl_mine_exit_d222fc60:


    "她惊讶地看着我。"


translate chinese sl_mine_exit_115b4f29:


    me "如果没有你我做不到的。"


translate chinese sl_mine_exit_22d72bcc:


    sl "没什么啦..."


translate chinese sl_mine_exit_1c771384:


    "斯拉维娅脸红了。"


translate chinese sl_mine_exit_15a32661:


    th "我又欠她的了。"


translate chinese sl_mine_exit_2a83f273:


    th "我很想知道自己该如何回报她，至少是一部分的..."


translate chinese sl_mine_exit_aa269f37:


    "我看着满天繁星，逐渐进入了梦乡。"


translate chinese sl_mine_exit_d50a711e:


    "今天的冒险让我累得没有力气做别的事情了。"


translate chinese sl_mine_exit_7ecbe987:


    me "你知道，在别的情况下，我可能就会向你告白了..."


translate chinese sl_mine_exit_c537a406:


    "我说着，感觉失去了真实感。"


translate chinese sl_mine_exit_0c6574db:


    "思考要比感觉快得多..."


translate chinese sl_mine_exit_7d9eccb6:


    "不过斯拉维娅已经不再这里了。"


translate chinese sl_mine_exit_2e4b7881:


    th "也许这样最好，至少她没有听..."


translate chinese sl_mine_exit_eeffa084:


    th "虽然这很奇怪—为什么她没有说再见就走了？"


translate chinese sl_mine_exit_52ccd478:


    th "算了，我就在这里睡到早晨吧。"


translate chinese sl_mine_exit_c4cf6525:


    "我已经累得不能走回辅导员的房间了...{w}不过灯光还亮着！"


translate chinese sl_mine_exit_b9666e2a:


    th "这说明她还在等着我！"


translate chinese sl_mine_exit_750b01bf:


    th "那就说明我又要面临一场说教了。"


translate chinese sl_mine_exit_f6601815:


    th "在这种情况下是最没有必要的！"


translate chinese sl_mine_exit_d7d24cf6:


    "我完全不想再讨论什么对错。"


translate chinese sl_mine_exit_12c908c6:


    "一把躺椅在我的面前摇晃着，好像在提醒我在这里睡觉，忘掉关于斯拉维娅，舒里克，还有辅导员什么的一切..."


translate chinese sl_mine_exit_19f0e8ea:


    me "你在这里啊..."


translate chinese sl_mine_exit_8a478121:


    "我空洞的说道，盯着自己亲爱的躺椅。"


translate chinese sl_mine_exit_105fc865:


    mt "谢苗!"


translate chinese sl_mine_exit_4b47445a:


    me "是，是...{w}我们找到了舒里克，斯拉维娅明天会和你讲解详情的，现在我只想睡一觉..."


translate chinese sl_mine_exit_014b5f98:


    "她迷惑的看着我，然后说道："


translate chinese sl_mine_exit_7e1d09c7:


    mt "那好吧..."


translate chinese sl_mine_exit_2ab3da5a:


    "我不知道提到斯拉维娅会不会影响到什么，不过半分钟以后，我已经躺在床上了。"


translate chinese sl_mine_exit_c849f321:


    "白天的事情开始回放，但是过了一会儿，我有点头疼，好像我的意识进入了安全模式正在重启..."


translate chinese day4_dv_55e8aea3:


    mt "还有朵切芙斯卡娅和你一起走！"


translate chinese day4_dv_a7bd7fb0:


    "奥尔加·德米特里耶夫娜好像读出了我的心思一样。"


translate chinese day4_dv_34113b9d:


    dv "为什么？"


translate chinese day4_dv_9dcfa902:


    mt "是谁试图炸掉雕塑的？{w}还有是谁试图诬陷谢苗的？"


translate chinese day4_dv_f1c01595:


    "她说的让阿丽夏无言以对。"


translate chinese day4_dv_ed1378b5:


    dv "这下高兴了？"


translate chinese day4_dv_8efa4588:


    "其他人走掉之后，她马上说道。"


translate chinese day4_dv_49e69f9d:


    me "为什么我一定要接受这个任务？{w}是我试图炸掉雕塑的吗？"


translate chinese day4_dv_f505265f:


    "阿丽夏嘟囔着转过身去。"


translate chinese day4_dv_90b40594:


    dv "和你在一起我哪里也不去！"


translate chinese day4_dv_2cbb8397:


    me "哇，别啊。"


translate chinese day4_dv_b5004dfa:


    dv "真好啊！"


translate chinese day4_dv_d1bfd0cf:


    me "太好啦！!"


translate chinese day4_dv_9001d819:


    "我们默默地站在这里，谁也不看谁。."


translate chinese day4_dv_48e0b151:


    "我勇敢的打破沉默。"


translate chinese day4_dv_7b7a2ac8:


    me "就是这些？"


translate chinese day4_dv_7e503b02:


    dv "哪些？"


translate chinese day4_dv_43dded4a:


    me "你想要告诉我的所有事情？"


translate chinese day4_dv_55cf29e5:


    dv "是啊，就是这些！"


translate chinese day4_dv_976566c1:


    me "你是否介意我问问你究竟为什么要炸掉雕塑呢？"


translate chinese day4_dv_288c13a3:


    dv "我很无聊..."


translate chinese day4_dv_f39f8aa5:


    th "啊，是啊，炸掉什么东西可以让你高兴起来！"


translate chinese day4_dv_5628d51f:


    "阿丽夏看起来真的很不情愿的样子。"


translate chinese day4_dv_8d7dd406:


    "我倒是真的想要找到舒里克。"


translate chinese day4_dv_9a84d633:


    "吸引我的不是他，而是旧营地。"


translate chinese day4_dv_e1bb0ffe:


    "出于某种原因，我觉得自己可以在那里找到什么。"


translate chinese day4_dv_19accf37:


    "而且我非常不愿意在晚上独自前往那里..."


translate chinese day4_dv_8d8791e8:


    me "那么你就是害怕了？"


translate chinese day4_dv_2ff41508:


    dv "你这是激将法吗？"


translate chinese day4_dv_7f6b716b:


    "他讽刺的看着我。"


translate chinese day4_dv_f9e95578:


    me "不，我可...{w}那么就祝你好运吧！"


translate chinese day4_dv_2b7ea55e:


    "我转过身往旧营地的方向前进。"


translate chinese day4_dv_4ae306eb:


    "阿丽夏在森林的边上追上了我。"


translate chinese day4_dv_33be1cc8:


    dv "等一下！{w}我和你一起去！"


translate chinese day4_dv_708a9465:


    me "你为什么这么快就屈尊改变了主意呢？"


translate chinese day4_dv_27c62366:


    "我又讽刺起来。"


translate chinese day4_dv_aa4bc8f9:


    dv "这你不需要知道！"


translate chinese day4_dv_eda552ee:


    "她一边喊着一边递给我一个手电筒。"


translate chinese day4_dv_30b5f8c4:


    "这是很明智的。"


translate chinese day4_dv_56fc6798:


    "如果不是阿丽夏我都不知道自己摸黑在那里能干出什么事来。"


translate chinese day4_dv_7bd87323:


    "电子小子告诉我说那个建筑是战争刚刚结束的时候建的。"


translate chinese day4_dv_fd3c1cdc:


    "它看起来像是一个幼儿园，或者是什么兵营，看起来装不下现在这么多的人。(or like a barracks, I expected) and definitely could hold fewer pioneers than today's camp."


translate chinese day4_dv_c4f7341a:


    "已经废弃了将近二十年..."


translate chinese day4_dv_456886a9:


    "夜晚的森林和白天完全不一样。"


translate chinese day4_dv_5d66727e:


    "几个小时之前它还像是一个盲人都不会在其中迷路的小树林。"


translate chinese day4_dv_e7ca9c28:


    "随着夜幕的降临，树木开始疯狂的生长，灌木丛不断地膨胀，填满了一切空地和小路，变成汹涌的森林之海。"


translate chinese day4_dv_30605aad:


    "这幅画最后的一笔是各种野兽，虫鸟的合唱，还有各自的乐器，共同表演着这场精彩的恐怖的夜曲。"


translate chinese day4_dv_a9effafd:


    "我们慢慢的走着，阿丽夏在前面，我在后面跟着尽量不掉队。"


translate chinese day4_dv_52d4466a:


    "她在错误面前总是胸有成竹。{w}绝对不像我，一阵猫头鹰的叫声就可以把我吓得不轻，然后警觉的注视着四周。"


translate chinese day4_dv_97608943:


    "不过显然这里没有什么可害怕的，这个小森林里没有什么野兽，附近除了夏令营里的少先队员们好像没有任何的其他人。"


translate chinese day4_dv_0c72d684:


    "不过恐惧来自我的本能，我尽量小心的走着。{w}谨慎总是要胜过勇气的，我想。"


translate chinese day4_dv_ee008bda:


    "阿丽夏突然停了下来。{w}她停得太突然，我差点撞到她。"


translate chinese day4_dv_15f32c97:


    dv "现在该往哪里走呢？"


translate chinese day4_dv_47e7fadb:


    me "什么？这...一直往前走吧？"


translate chinese day4_dv_7b471372:


    dv "你确定吗？"


translate chinese day4_dv_558379f9:


    "我试图回忆出方向。"


translate chinese day4_dv_1a602959:


    "我们应该走的没错。{w}至少是电子小子指出的那条路。"


translate chinese day4_dv_2a16d041:


    "但是没有地图，没有GPS没有GLONASS，我都不知道自己是不是还在地球上。"


translate chinese day4_dv_4dde0bc7:


    me "咱们还没有转弯，只是一直往前走，就像是电子小子说的..."


translate chinese day4_dv_a44acfbb:


    dv "那么为什么我们还没有走到旧营地？"


translate chinese day4_dv_cd895649:


    me "这我怎么知道？！"


translate chinese day4_dv_492a8d61:


    "阿丽夏粗鲁的谈话方式让我很不爽。"


translate chinese day4_dv_cebebf7d:


    dv "我们已经迷路了吗？"


translate chinese day4_dv_eb6a6442:


    me "你怎么那么说？咱们还没走出一公里呢。"


translate chinese day4_dv_b66acd62:


    dv "那怎么了？"


translate chinese day4_dv_fbb515c6:


    "她明显的变的失落。"


translate chinese day4_dv_023d75be:


    me "这没什么啊！如果你不想前进的话回去就好了。"


translate chinese day4_dv_aeb753f4:


    "阿丽夏失望的看着我，过了一会儿她的表情突然变了。"


translate chinese day4_dv_4a85e25e:


    dv "我可是说到做到！"


translate chinese day4_dv_4fdd6288:


    "她突然转过身去开始轻快的往营地走去。"


translate chinese day4_dv_cacdf009:


    me "祝你好运。"


translate chinese day4_dv_3613cae1:


    "我在后边喊着。"


translate chinese day4_dv_39966305:


    th "然而阿丽夏可能是对的，我们可能已经迷路了。{w}因此独自呆在这里可不是一个好主意..."


translate chinese day4_dv_1f17e8a8:


    "一阵夜间的冷风吹过，我哆嗦了一下。"


translate chinese day4_dv_3fe492ee:


    th "不过跟着她回去会显得很没面子。更糟糕的是会承认她是对的。{w}我可不想承认这个事实！"


translate chinese day4_dv_a158a040:


    "如果不是阿丽夏突然大叫起来我可能一晚上都要在这里做思想斗争了。"


translate chinese day4_dv_5b0d6420:


    "我迅速的跑向她那边。"


translate chinese day4_dv_4739db18:


    "跑了几十米之后我的面前突然出现了一个大坑，不过我在最后关头设法跳了过去。"


translate chinese day4_dv_41315982:


    "不过阿丽夏好像没有成功..."


translate chinese day4_dv_172dcb0f:


    "我正想用手电筒照一照，突然想起来被她拿走了。{w}好吧，这倒是意料之中。"


translate chinese day4_dv_c22eb442:


    me "你还活着吗？"


translate chinese day4_dv_3b4b97c0:


    dv "是啊..."


translate chinese day4_dv_20abb91b:


    "下面传来她嘟嘟囔囔的声音。"


translate chinese day4_dv_f3139a0d:


    me "你砸坏了什么东西没有？"


translate chinese day4_dv_ce4becd7:


    dv "我怎么知道！傻子！"


translate chinese day4_dv_44682451:


    "大洞中一道光闪过，我弯下身来检查它有多深。"


translate chinese day4_dv_dd73bb1b:


    "这个坑有大概三米深，不过下面是一片漆黑。"


translate chinese day4_dv_5d83ec49:


    dv "扶我出去！"


translate chinese day4_dv_a342db42:


    "阿丽夏愤怒的喊着。"


translate chinese day4_dv_ea49f8ff:


    me "你是怎么想象的？"


translate chinese day4_dv_6be52e6b:


    dv "我不知道！你想想办法啊！"


translate chinese day4_dv_71d84df3:


    me "那个，你看到了什么？描述一下。"


translate chinese day4_dv_af7113a7:


    dv "啊，是一个隧道什么的...就是一个隧道。"


translate chinese day4_dv_b949157e:


    "一条隧道？奇怪。"


translate chinese day4_dv_c9a2c926:


    dv "快下来！"


translate chinese day4_dv_285fa6b8:


    me "这就是你最好的主意了？我宁愿跑回营地拿来。"


translate chinese day4_dv_7cc30c48:


    dv "你打算把我一个人扔在这里？"


translate chinese day4_dv_8f7c4481:


    "阿丽夏听起来蛮横无理。"


translate chinese day4_dv_fab07924:


    me "反正你就是自己一个人在下面..."


translate chinese day4_dv_93b5fc50:


    dv "嘿!"


translate chinese day4_dv_e2d0858a:


    "她大叫着，用手电照着我的眼睛。"


translate chinese day4_dv_0f7a1181:


    me "别紧张，又不会有巨魔把你吃了，我去去就回。"


translate chinese day4_dv_66878d88:


    "阿丽夏还在下面喊着什么，不过我听不出来是什么。"


translate chinese day4_dv_7514d7d5:


    "不过我还没走出几步，脚下的地面就开始塌陷。"


translate chinese day4_dv_85879201:


    "我抓住一根树根想要停下来..."


translate chinese day4_dv_de9de6d3:


    "...不过在我的重量下，它折断了，和我一起跌落下来。"


translate chinese day4_dv_ce617998:


    "..."


translate chinese day4_dv_006481b5:


    dv "你还活着吗？"


translate chinese day4_dv_63fdaa9f:


    "我微睁开一只眼，一道光刺过来。"


translate chinese day4_dv_aababdd9:


    me "啊，快移开..."


translate chinese day4_dv_bdc0c338:


    "阿丽夏蹲在旁边兴奋的看着我。"


translate chinese day4_dv_4ddc9f96:


    dv "四条腿有没有受伤？"


translate chinese day4_dv_7479b07e:


    me "不知道..."


translate chinese day4_dv_322546b6:


    "我动了动手脚，似乎都没有问题。"


translate chinese day4_dv_217dc2c0:


    "考虑到这个高度，我没有摔死真是奇迹！"


translate chinese day4_dv_f32b91db:


    "我费力的站起来，检查现场。"


translate chinese day4_dv_e2ce46b6:


    "我们确实在一个长长的走廊中，墙上挂满了电线还有在支架上安装的灯。"


translate chinese day4_dv_88cece4e:


    "在其他的情况下我大概会认为这是一条地铁隧道，不过我不认为«Совёнок»夏令营的基础建设做得这么好..."


translate chinese day4_dv_d359bcda:


    dv "我们在哪里？"


translate chinese day4_dv_c45d86b6:


    me "我不知道，不过咱们必须要走出去！"


translate chinese day4_dv_45db0ef2:


    "爬回塌掉的墙不太可能了，说实话我很奇怪我们是如何从那么高摔下来却一点事没有的。"


translate chinese day4_dv_638df9b4:


    me "看来咱们得找找别的出口了。"


translate chinese day4_dv_c56fe6de:


    dv "那要怎么做？"


translate chinese day4_dv_9d41ebbb:


    "阿丽夏问道，脸色逐渐变得苍白。"


translate chinese day4_dv_7507cfac:


    "她平常的傲慢表情完全消失了。"


translate chinese day4_dv_333f2018:


    me "那个，我猜...这里一定有一个出口的，对吧？"


translate chinese day4_dv_91700607:


    dv "有可能，不过我们应该走哪条路呢？"


translate chinese day4_dv_c59d4b63:


    th "这是一个好问题。"


translate chinese day4_dv_7c3c6bab:


    "一开始看上去往回到营地的方向走是个不错的选择，不过我没有看到那个方向的任何出口。{w}也就是说我们不如试试相反的方向。"


translate chinese day4_dv_8bb4bd4e:


    me "给我！"


translate chinese day4_dv_f3a15952:


    "我从阿丽夏手中抢过手电筒，照向黑暗之中。"


translate chinese day4_dv_c31cbf17:


    me "那边！"


translate chinese day4_dv_ce617998_1:


    "..."


translate chinese day4_dv_7fb53b2d:


    "我们慢慢的前进，而阿丽夏在紧紧的攥着我的手。"


translate chinese day4_dv_bef06bb8:


    "在其他的情况下我可能惊讶，尴尬或者很高兴，不过现在我只想赶紧离开这个地方。"


translate chinese day4_dv_8fa117b1:


    "我的伤还很疼，我听到大脑中血液咚咚的声音，不停地照亮着水泥墙的手电也没有让我们振作起来。"


translate chinese day4_dv_e7613964:


    me "见鬼，还有舒里克..."


translate chinese day4_dv_e0ebb201:


    dv "什么?"


translate chinese day4_dv_acbe50d2:


    me "咱们晚上来森林究竟是干什么？是找舒里克！"


translate chinese day4_dv_f174d829:


    dv "快算了吧。"


translate chinese day4_dv_622bba83:


    me "他也有可能像咱们一样掉了下来。"


translate chinese day4_dv_3d5f8ddf:


    "阿丽夏说的也有道理，现在再说营救舒里克的行动意义不大了，不过如果我们不设法出去的话，就会有营救我们的行动了。"


translate chinese day4_dv_b6c57128:


    "可是我无法让自己忘掉这件事。"


translate chinese day4_dv_fb54620a:


    "好像舒里克就站在我眼前，责备的看着我。"


translate chinese day4_dv_b0dd778f:


    dv "小心！"


translate chinese day4_dv_9d770d3f:


    "阿丽夏喊道。"


translate chinese day4_dv_79b85d9a:


    "我差点撞到一个大金属门..."


translate chinese day4_dv_d0842bb5:


    "一个生化标志引起了我的注意。"


translate chinese day4_dv_a9087c69:


    me "这说明咱们在一个防空洞里..."


translate chinese day4_dv_c11bed91:


    "我悄悄地说道。"


translate chinese day4_dv_aa5fd937:


    dv "是啊，我好像听说过..."


translate chinese day4_dv_017f286b:


    me "你听说过？你刚才怎么不说？"


translate chinese day4_dv_10384b59:


    dv "我怎么知道那个很重要？"


translate chinese day4_dv_7ecde56e:


    me "好吧..."


translate chinese day4_dv_7db5329b:


    "我抓起把手用力的推着。"


translate chinese day4_dv_80cc6fd2:


    "巨大的锈铁不断摩擦着，不过就是一点也不动。"


translate chinese day4_dv_1f8abc27:


    me "别在那里懒洋洋的，快来帮我！"


translate chinese day4_dv_0398ce56:


    "阿丽夏犹豫了一下，然后抓住了把手拉动起来。"


translate chinese day4_dv_6ec0b8eb:


    "最终我们成功地打开了大门。"


translate chinese day4_dv_47dd4261:


    "这个房间可不会被认错，这绝对是一个防空洞。"


translate chinese day4_dv_75eef015:


    "柜子里装着防毒面具，生化服，压缩干粮，各种装备，床铺，还有空气循环系统，有核战争需要的一切。{w}唔，至少能挺过第一波..."


translate chinese day4_dv_c5fd3f99:


    "阿丽夏握着我的胳膊更紧了。"


translate chinese day4_dv_ad02f53a:


    me "什么？"


translate chinese day4_dv_35f94c91:


    dv "什么什么？"


translate chinese day4_dv_04f9ace4:


    "她悄悄抱怨道。"


translate chinese day4_dv_a1e11f2c:


    dv "我好害怕..."


translate chinese day4_dv_958e50a5:


    me "你害怕什么啊？"


translate chinese day4_dv_be3d8921:


    th "是啊，害怕什么啊？"


translate chinese day4_dv_ad914bab:


    th "我们掉进一个废弃的防空洞，在黑乎乎的隧道里寻找出口，{w}对，没有什么好害怕的..."


translate chinese day4_dv_1664c72b:


    "这种想法让我感到一阵寒气，不禁贴紧了阿丽夏。"


translate chinese day4_dv_9f11ce03:


    "她什么也没有说，只是有点脸红，转了过去。"


translate chinese day4_dv_e789012e:


    me "唔，不管怎么说..."


translate chinese day4_dv_e0ebb201_1:


    dv "什么?"


translate chinese day4_dv_c7ebc638:


    me "咱们得继续寻找出口。"


translate chinese day4_dv_3614f1a5:


    dv "嗯，那里还有一扇门。"


translate chinese day4_dv_ded6f407:


    "确实，那里又是一道和刚才差不多的大门。"


translate chinese day4_dv_1695119f:


    "然而，不管我用多大的力气，它总是卡的死死的。"


translate chinese day4_dv_5961a3eb:


    dv "需要帮忙吗？"


translate chinese day4_dv_96fbd823:


    me "不用了，它完全卡住了..."


translate chinese day4_dv_7b6b83d1:


    "我看看周围，在角落里发现了一把撬棍。"


translate chinese day4_dv_a499094f:


    me "好极了!"


translate chinese day4_dv_223ad1b0:


    "阿丽夏呆呆的看着我。"


translate chinese day4_dv_70066e56:


    me "看我的！"


translate chinese day4_dv_b614218a:


    "不过我用了撬棍还是撬不动大门一厘米。"


translate chinese day4_dv_152a52ab:


    "我累的要死，一屁股坐在了床上叹着气。"


translate chinese day4_dv_32250ada:


    me "咱们得往回走了。"


translate chinese day4_dv_11b295cf:


    dv "我们好不容易才走出来！"


translate chinese day4_dv_b3c4b2e1:


    "阿丽夏似乎恢复了一点平常的粗鲁。"


translate chinese day4_dv_f527e7c1:


    me "嘛，你要是想呆在这里也可以，这里有水，有干粮，那台收音机也许还能用..."


translate chinese day4_dv_f67a7d59:


    "她一脸不爽的看着我，但是没有说什么。"


translate chinese day4_dv_0c0902a5:


    "我全身都疼的要命。"


translate chinese day4_dv_0d661aed:


    "知道我放松了一些才体会到那种感觉。"


translate chinese day4_dv_0e1194d0:


    "我腿上一条深深的伤口在不停地流血，我的手也肿了。"


translate chinese day4_dv_d39992ac:


    me "你...还好吗？"


translate chinese day4_dv_4b680097:


    "我问着阿丽夏，也许是出于礼貌，也许是关心她。"


translate chinese day4_dv_aa62d7ce:


    "她过了一会儿才回答我。"


translate chinese day4_dv_413f8d3c:


    dv "这种情况下怎么可能还好？"


translate chinese day4_dv_c71c3992:


    me "不，我是说..."


translate chinese day4_dv_7c28149f:


    "我指着腿上的伤口。"


translate chinese day4_dv_8298da02:


    dv "哇!"


translate chinese day4_dv_16d734ec:


    "阿丽夏喊了出来。"


translate chinese day4_dv_3bf9faff:


    dv "我们需要紧急处理一下！"


translate chinese day4_dv_bee1cd2f:


    me "不用了，我会活下来的。"


translate chinese day4_dv_3044c877:


    dv "别啊！一定得做点什么！不然就会感染然后就得截肢了，我的爷爷在大战中就是这样丢掉一条腿的。"


translate chinese day4_dv_83a7a438:


    "阿丽夏看起来真的很担心所以我决定不去争辩。"


translate chinese day4_dv_21b14d25:


    me "好吧，不过离医务室还有很远。"


translate chinese day4_dv_6c7839f5:


    dv "坚持住。"


translate chinese day4_dv_f08e28b7:


    "她笑了，然后开始翻箱倒柜。"


translate chinese day4_dv_d617f7d1:


    "在防空洞里不难找到药品什么的，很快阿丽夏就带着胜利的表情掏出了一个医药包。"


translate chinese day4_dv_68a3327c:


    dv "别乱动。"


translate chinese day4_dv_8cc97caa:


    me "呃，我尽量。"


translate chinese day4_dv_6db936c0:


    "用带碘酒的药棉撒伤口就像是被烧红的铁棍烫到，我紧握着拳头低声叫着。"


translate chinese day4_dv_cc381102:


    dv "哎呀，哪有那么疼。"


translate chinese day4_dv_73eef208:


    me "就是那么疼好不好！你要不要自己来试一试？！"


translate chinese day4_dv_74a128a2:


    "It was easier with my other wounds and soon enough I looked like a leopard – all covered in brown spots."


translate chinese day4_dv_73aab3cd:


    me "Thanks..."


translate chinese day4_dv_16cc6217:


    dv "Don't you think that was anything special, we just had to treat your injuries!"


translate chinese day4_dv_f505265f_1:


    "阿丽夏抽着鼻子用后背对着我。"


translate chinese day4_dv_1c08f469:


    me "是，是..."


translate chinese day4_dv_ec44416e:


    "我停下了争吵，从床上起来。"


translate chinese day4_dv_9322dc07:


    "短暂的休息一会儿也是对我很有好处的。"


translate chinese day4_dv_ca0fc878:


    me "那个，咱们可以走了吗？"


translate chinese day4_dv_ce617998_2:


    "…"


translate chinese day4_dv_fbd5ef22:


    "阿丽夏抱着我的胳膊，在旁边一步一步的跟着我。"


translate chinese day4_dv_5adb4b2c:


    "我们慢慢的往回走，仔细的检查着房顶上的大洞。"


translate chinese day4_dv_02fac7b0:


    "隧道好像永无止境似的，过了十分钟我们还是找不到掉下来的地方。"


translate chinese day4_dv_9f66e5e7:


    dv "我们迷路了吗？"


translate chinese day4_dv_bf52bfa2:


    "阿丽夏垂头丧气的问道。"


translate chinese day4_dv_3b34fb45:


    me "在森林里可能会迷路..."


translate chinese day4_dv_251da636:


    "我突然感觉这好像和半个小时以前发生的事情一样，所以为了让她打起精神，我充满自信的说道："


translate chinese day4_dv_722c0913:


    me "但是这是一条隧道，你不可能在里面迷路的！"


translate chinese day4_dv_9b43edaf:


    dv "那么我们会到哪里呢？"


translate chinese day4_dv_61395ab8:


    "我的话好像并没有让阿丽夏冷静下来..."


translate chinese day4_dv_070a72a2:


    me "咱们会出去的，总会有一个出口的。"


translate chinese day4_dv_6ed2d8af:


    dv "真的吗?"


translate chinese day4_dv_b0ed4000:


    me "当然了！当年挖隧道的工人们总是要回到地面上去的嘛！"


translate chinese day4_dv_bf62b6f0:


    dv "是啊，大概是这样。"


translate chinese day4_dv_faa0cd99:


    "她好像尽量尝试着像平常一样冒冒失失的，不过似乎不太成功。阿丽夏很显然在黑暗中不停地发抖，她的脸上露出努力的表情。"


translate chinese day4_dv_31843b52:


    me "好吧。咱们继续前进吧！"


translate chinese day4_dv_ce617998_3:


    "..."


translate chinese day4_dv_36099f9d:


    "过了几分钟我发现地面上有一个大洞。{w}看起来像是一个弹坑，足够让我们钻过去。"


translate chinese day4_dv_4453f8b0:


    dv "下面是什么？"


translate chinese day4_dv_7479b07e_1:


    me "我怎么知道..."


translate chinese day4_dv_606bacdd:


    "我跪下来用手电照着下面，湿湿的，还有轨道。"


translate chinese day4_dv_d07b5181:


    me "应该是一个矿井。"


translate chinese day4_dv_47a46c70:


    "阿丽夏不解的看着我。"


translate chinese day4_dv_1e95970b:


    "当然我们可以绕过这个大坑，不过我的直觉告诉我继续沿着这条隧道走也不会有什么——要么是死路要么还是一道打不开的大门。"


translate chinese day4_dv_675becdd:


    "另一方面来说，要在一个矿井中找到出口就太天真了。"


translate chinese day4_dv_4b228483:


    me "咱们可以试试，实在不行就回来，这并不是很高。"


translate chinese day4_dv_a4cb18c4:


    "她的脸上露出痛苦的表情，然后她离开了光线静静的说道："


translate chinese day4_dv_632d7403:


    dv "如果你这么决定的话..."


translate chinese day4_dv_ce617998_4:


    "..."


translate chinese day4_dv_78bd231b:


    "不一会儿我们就来到了矿井中。生锈的轨道在隧道中延伸着，我几乎看到了多年前装的满满的矿车在轨道上吱吱扭扭的行驶。墙上有木头柱子支撑着，房顶上不停的有水滴下来。"


translate chinese day4_dv_0ccb51dd:


    "我不知道这里和上层的防空洞哪里更恐怖一些。我感觉上面有一种末日生存的风格，这里确实一种中世纪的风格。"


translate chinese day4_dv_89403983:


    "不管怎么说我越来越想赶快离开这个是非之地。"


translate chinese day4_dv_710c355f:


    "我们来到矿井之后阿丽夏一声也没出。她只是紧紧的握着我的胳膊在我的旁边走着，让我有点担心。"


translate chinese day4_dv_580235f4:


    me "你还好吗？"


translate chinese day4_dv_e8936f84:


    dv "不好。"


translate chinese day4_dv_a68bf534:


    "她轻轻地说。"


translate chinese day4_dv_fc571a13:


    me "我可以理解，不过你为什么不说出来呢？"


translate chinese day4_dv_3a950d86:


    dv "还好。"


translate chinese day4_dv_d15a76eb:


    "她用那种冷漠的声音回答着。"


translate chinese day4_dv_448d2873:


    me "那还好，因为..."


translate chinese day4_dv_9f378f6c:


    "我很想说“如果你也发疯的话那就会是一场噩梦”。"


translate chinese day4_dv_64368234:


    me "因为咱们会找到出口的，绝对会的。"


translate chinese day4_dv_887ed8a6:


    dv "哈..."


translate chinese day4_dv_2528c1d3:


    "看起来她快要坚持不住了。"


translate chinese day4_dv_24b0636a:


    th "我们应该走的再快点！"


translate chinese day4_dv_0bcb931e:


    "我加快了脚步，又过了几十米，我们来到了一个岔路口。"


translate chinese day4_dv_2ad79933:


    me "这..."


translate chinese day4_dv_36bca938:


    dv "那么，走哪条路呢？"


translate chinese day4_dv_af77dc81:


    "阿丽夏看起来已经完全不担心我们能不能走出去了。"


translate chinese day4_dv_411a0051:


    me "唔，让我想一想..."


translate chinese day4_dv_a6bcb093:


    th "向左还是向右呢？{w}万一我们迷路了怎么办？"


translate chinese day4_dv_4909406f:


    "我决定在墙上画一个记号，万一我们回到了这里就能知道了。"


translate chinese day4_dv_272c439e:


    "我很快在柱子上画了一个叉。"


translate chinese day4_dv_803ee2f2:


    me "好了，咱们可以走了..."


translate chinese dv_mine_coalface_e40597aa:


    "我们已经来过这里了。"


translate chinese dv_mine_coalface_5b4c3c90:


    "最后我们走出了隧道，来到了一个有高高屋顶的大厅。"


translate chinese dv_mine_coalface_3ef8c583:


    "虽然它不太像一个大厅，他们以前一定是在这里挖矿来着。{w}煤矿吧，也许是金矿。"


translate chinese dv_mine_coalface_3c54fe69:


    "墙面有十字镐和气钻凿过的痕迹。"


translate chinese dv_mine_coalface_c718c063:


    "这个地方一片漆黑，所以我们只能依赖手电筒了。"


translate chinese dv_mine_coalface_b97863a3:


    th "如果这里坍塌的话，我们大概是没有机会逃出去的..."


translate chinese dv_mine_coalface_58411121:


    "我在灯光中发现了一块红布。"


translate chinese dv_mine_coalface_ca0a4f90:


    "是一条红领巾！"


translate chinese dv_mine_coalface_96b3272c:


    "舒里克肯定在附近。"


translate chinese dv_mine_coalface_93ec21f7:


    me "舒里克！舒里克！"


translate chinese dv_mine_coalface_f23d45c0:


    "只有回声。"


translate chinese dv_mine_coalface_cb6219ee:


    dv "你喊什么啊？他又听不见..."


translate chinese dv_mine_coalface_858fffec:


    me "总要试一试吧？"


translate chinese dv_mine_coalface_07c129c5:


    "阿丽夏没有回答，只是叹了口气。"


translate chinese dv_mine_coalface_686bf61c:


    "这个大厅没有其他的出口。"


translate chinese dv_mine_coalface_7ea80226:


    th "显然这个隧道里还有我们没有发现的死角..."


translate chinese dv_mine_coalface_858b6cbe:


    th "所以我们得继续搜查！"


translate chinese dv_mine_exit_8daf197d:


    "走过拐角，我沿着灯光看到了一个木门。"


translate chinese dv_mine_exit_599ac8f7:


    me "有啦，终于看见点什么东西了！"


translate chinese dv_mine_exit_16377e5b:


    dv "你觉得这是？"


translate chinese dv_mine_exit_7434af59:


    "阿丽夏看起来恢复了精神。"


translate chinese dv_mine_exit_771aa502:


    me "当然!"


translate chinese dv_mine_exit_e2d76069:


    "说实话我真的不确定门后有没有出口。"


translate chinese dv_mine_exit_f3b9dae9:


    "不过我们反正也没有别的选择。"


translate chinese dv_mine_exit_efd4a253:


    "我拉动了把手..."


translate chinese dv_mine_exit_c4bfa0fb:


    "我们好像来到了一个防空洞的锅炉房或者是工具间。"


translate chinese dv_mine_exit_d9981fe3:


    "地上满是空瓶子，烟头还有其他的各种碎片。"


translate chinese dv_mine_exit_2328f028:


    "这说明附近有出口！"


translate chinese dv_mine_exit_34338260:


    "我用手电的灯光搜索着整个屋子，当灯光照到角落时..."


translate chinese dv_mine_exit_d4831ba9:


    "我看到了舒里克蜷缩在那里。"


translate chinese dv_mine_exit_b01a7f13:


    me "舒里克!"


translate chinese dv_mine_exit_58f9ccc4:


    sh "谁...那是谁？"


translate chinese dv_mine_exit_377c5a6f:


    "他结巴的说着。"


translate chinese dv_mine_exit_4701c46e:


    me "舒里克！我们找了你一个晚上了，你却就是坐在这里！快走吧！"


translate chinese dv_mine_exit_3901fe16:


    sh "我哪里也不去..."


translate chinese dv_mine_exit_6ebe4e41:


    "他的话越来越有力量，直到几乎是喊出来一样。"


translate chinese dv_mine_exit_7979942d:


    sh "不！我哪里也不跟你去！这是幻觉！这不科学！对！这不科学！"


translate chinese dv_mine_exit_5857f574:


    me "什么叫幻觉？！我们就在这里，来救你了啊！"


translate chinese dv_mine_exit_783dbf4d:


    "阿丽夏一直保持着沉默，不过接下来她放开了我的手向前走去。"


translate chinese dv_mine_exit_8bdd7529:


    me "你这是...?"


translate chinese dv_mine_exit_420d89ea:


    "我悄悄的问道。"


translate chinese dv_mine_exit_782e55fe:


    "她犹豫了一下，不过假装没有听见我继续向前走去。"


translate chinese dv_mine_exit_bfea42b5:


    "最终走到那个迷路的少先队员面前，阿丽夏一个巴掌扇在了他的脸上。"


translate chinese dv_mine_exit_3a43352d:


    dv "你！我们为了找你差点挂掉你知不知道！那些森林！还有什么防空洞！隧道！我们摔得鼻青脸肿，你却在这里悠然自得！"


translate chinese dv_mine_exit_af2a4a69:


    "舒里克困惑的看着她。"


translate chinese dv_mine_exit_57632dcb:


    dv "少废话！赶快起来！走你！"


translate chinese dv_mine_exit_7e0c8149:


    sh "不..."


translate chinese dv_mine_exit_fc567eb6:


    "他开始悄悄说道。"


translate chinese dv_mine_exit_7e0c8149_1:


    sh "不..."


translate chinese dv_mine_exit_8f7b3a64:


    "某些我意识边缘的感觉告诉我有什么非常不好的事情就要发生了。"


translate chinese dv_mine_exit_b5bdda9c:


    "它发生了，也许是因为预感和直觉，也许是因为舒里克脸上表情在提示着什么。"


translate chinese dv_mine_exit_94c2b3ff:


    "也许是因为所有的一切。我冲向阿丽夏，灯光突然晃了起来，照在了舒里克的脸上——上面充满了狰狞，愤怒。"


translate chinese dv_mine_exit_495d2be6:


    "他挥舞着一根铁棍，如果不是我的话，他肯定就把阿丽夏的头砸烂了。"


translate chinese dv_mine_exit_0e566744:


    "不过我成功了。"


translate chinese dv_mine_exit_e6910a04:


    "这个瞬间就像是一场梦：女孩儿遥远的呼喊，空洞的撞击声，突然灭掉的灯光还有诡异的寂静，除了舒里克粗重的喘息声。"


translate chinese dv_mine_exit_4aee785c:


    "我过了一会儿才恢复了意识。"


translate chinese dv_mine_exit_de443349:


    me "你丫搞毛啊？"


translate chinese dv_mine_exit_faa7fd6f:


    "我检查了自己的胳膊，还好，舒里克的一击打中了手电筒。{w}所以我们现在陷入了彻底的黑暗之中。"


translate chinese dv_mine_exit_2b53bb97:


    sh "你不会得逞的，你抓不到我！"


translate chinese dv_mine_exit_e324829f:


    "舒里克歇斯底里的笑声消失在了远方。"


translate chinese dv_mine_exit_61203408:


    me "嘿！你去哪儿啊？快回来啊！"


translate chinese dv_mine_exit_e37edfba:


    sh "不到我..."


translate chinese dv_mine_exit_a8eff0da:


    "喊叫声消失在了远方..."


translate chinese dv_mine_exit_5919c4da:


    me "变态啊..."


translate chinese dv_mine_exit_4fd3278c:


    "我试着修理手电筒，没戏了，舒里克把它打成了碎片。然后我突然想到了阿丽夏。"


translate chinese dv_mine_exit_a1e11399:


    me "你还好吗？"


translate chinese dv_mine_exit_2a2977e4:


    "我听到了一阵轻轻的抽泣，接下来的瞬间我感受到了阿丽夏紧紧的怀抱。"


translate chinese dv_mine_exit_0288e8be:


    me "好啦，好啦，没事啦，都过去了哟..."


translate chinese dv_mine_exit_f7c6dd5b:


    th "不过事实上有的是事，不过我需要让阿丽夏回过神来，不然我们可是没有机会或者逃出去的..."


translate chinese dv_mine_exit_e77948eb:


    "我轻轻地爱抚着她的头发。"


translate chinese dv_mine_exit_e7f9499f:


    th "好奇怪，阿丽夏在哭，这不是很好玩的事情吗？{w}可能要在别的场合中..."


translate chinese dv_mine_exit_a1e11f2c:


    dv "我好害怕..."


translate chinese dv_mine_exit_cb460d93:


    me "没关系，我也害怕..."


translate chinese dv_mine_exit_abca858d:


    "我注意到了阿丽夏强烈的转变。"


translate chinese dv_mine_exit_7e138f85:


    th "不过管他呢，谁在这种情况下都会害怕的。{w}况且她还是个女孩儿..."


translate chinese dv_mine_exit_1e55b63f:


    th "不过为什么我不害怕呢？{w}不对，不是害怕。为什么我还能保持理智呢？"


translate chinese dv_mine_exit_4a5234b5:


    "会不会是因为我需要照顾的不只有我自己啊？"


translate chinese dv_mine_exit_8ce5aeb2:


    me "振作起来啦，咱们无论如何都要走出去。"


translate chinese dv_mine_exit_5a998b89:


    dv "等一下，就一会儿..."


translate chinese dv_mine_exit_db7a783b:


    "阿丽夏轻轻的说着，房间被打火机昏暗的光照亮着。"


translate chinese dv_mine_exit_c0d3e816:


    me "那是哪儿来的？"


translate chinese dv_mine_exit_3d4c4201:


    dv "雕塑..."


translate chinese dv_mine_exit_d01e1437:


    "她努力的笑了笑，稍微放松了些。"


translate chinese dv_mine_exit_505d91ae:


    me "好啦..."


translate chinese dv_mine_exit_863a1c4e:


    "我迅速在房间里搜索可以用来做光源的东西。"


translate chinese dv_mine_exit_d0ca8ec6:


    "地板上有一些碎步还有一根鱼竿，有一个瓶子里装的东西问起来像是工业酒精。"


translate chinese dv_mine_exit_c238b970:


    "不管怎么说，很快我的手中出现了一个像是火炬的东西。"


translate chinese dv_mine_exit_93e8f263:


    me "我不知道它能坚持多长时间，所以咱们得快点。"


translate chinese dv_mine_exit_bf5b8ada:


    dv "去哪里？"


translate chinese dv_mine_exit_92ae42e9:


    "阿丽夏已经恢复了理智。"


translate chinese dv_mine_exit_4a1ef41f:


    me "咱们往回走吧，至少防空洞那里还有灯。"


translate chinese dv_mine_exit_ce1ee8c8:


    "她轻轻的点点头，握住了我的手。"


translate chinese dv_mine_exit_ce617998:


    "…"


translate chinese dv_mine_exit_9e59519b:


    "火把烧的很不好，我们每隔一分钟就得重新点燃一次。而且一般人都能看出来我们刚刚用的那块破布坚持不了多长时间。"


translate chinese dv_mine_exit_4b60d852:


    "我越来越害怕我们可能无法走出隧道了，那个打火机好像是指望不上的。"


translate chinese dv_mine_exit_3d40f229:


    "不过我们很快就回到了通往墓穴的大洞，看来我清楚地记得来时的路。"


translate chinese dv_mine_exit_6afe7d5f:


    "我们挣扎着爬上去，几乎是摸黑的爬着。"


translate chinese dv_mine_exit_15f32c97:


    dv "好了，现在该往哪里走？"


translate chinese dv_mine_exit_f590dde1:


    "阿丽夏一边回复着呼吸一边问道。"


translate chinese dv_mine_exit_47bf9d1c:


    me "什么意思？咱们当然是要回到那个充满了柜子的房间了。"


translate chinese dv_mine_exit_ec19d6bf:


    dv "那是哪一条路呢？"


translate chinese dv_mine_exit_5e081fd0:


    "我张开嘴想要回答，不过这才意识到完全不知道说什么好。"


translate chinese dv_mine_exit_61ac798c:


    "这个洞看起来很对称，我们完全看不出方向。"


translate chinese dv_mine_exit_f883183e:


    me "这..."


translate chinese dv_mine_exit_7b1bbd69:


    dv "你不知道吗？"


translate chinese dv_mine_exit_87e8b661:


    "阿丽夏难过的问着我，然后坐在了地上。"


translate chinese dv_mine_exit_5a7a7577:


    me "不知道。"


translate chinese dv_mine_exit_09882a0d:


    "我轻轻的回答道，扑灭了火把以备不测，然后坐在她旁边。"


translate chinese dv_mine_exit_816feedf:


    dv "我们要在这里死掉了吗？"


translate chinese dv_mine_exit_89d773f5:


    "她歪着头搭在我的肩膀上。"


translate chinese dv_mine_exit_c69e1f3a:


    "阿丽夏的声音听起来很平淡，甚至可以说平静，不过我可以感觉到她的身体在颤抖，因为寒冷，或是恐惧，或是两者都有。"


translate chinese dv_mine_exit_15d317c8:


    me "别傻了！事情进展不太顺利，不过离着“死掉”还远着呢...他们早上会来救咱们的，辅导员会叫警察，所以..."


translate chinese dv_mine_exit_a74b8123:


    th "我很愿意相信自己。"


translate chinese dv_mine_exit_4677b777:


    dv "好吧..."


translate chinese dv_mine_exit_751ac511:


    me "是啊..."


translate chinese dv_mine_exit_ce617998_1:


    "…"


translate chinese dv_mine_exit_b162cf42:


    "我失去了时间感..."


translate chinese dv_mine_exit_d3c437c7:


    "我害怕往前走或是往后走，我害怕做出选择，因为往哪一边我们的火把都不够用，在黑暗中走来走去还不如坐在这里等待救援。"


translate chinese dv_mine_exit_2607a8c2:


    "这样我们至少可以留一些能源做紧急用..."


translate chinese dv_mine_exit_af386fc2:


    "突然远方传来一阵乱糟糟的回音。"


translate chinese dv_mine_exit_b03fee0e:


    th "哇，这就是那个紧急事件了！"


translate chinese dv_mine_exit_40ea5406:


    "那绝对是门闩的声音。"


translate chinese dv_mine_exit_7b953cc3:


    me "起来！"


translate chinese dv_mine_exit_a1c23a4f:


    "我使劲拉着阿丽夏，然后颤抖着点燃了火把。"


translate chinese dv_mine_exit_d75c13ce:


    dv "什么？"


translate chinese dv_mine_exit_93c290bf:


    "她失望的问着。"


translate chinese dv_mine_exit_e79963e5:


    me "我不知道，不过似乎有人，咱们得赶紧跑！"


translate chinese dv_mine_exit_ce36a4aa:


    "说实话，我不知道这是不是个好主意。"


translate chinese dv_mine_exit_228b3ef4:


    th "万一舒里克在那里怎么办？{w}而且这已经是我能想出来的最好的情况了！"


translate chinese dv_mine_exit_43f41aac:


    "不过我们没有其他的选择。"


translate chinese dv_mine_exit_5e04ae36:


    "地面上满是石子，我尽量让自己保持平衡，一只手举着火把，一只手拉着阿丽夏。"


translate chinese dv_mine_exit_19c33fc2:


    "她默默的跟着我跑着，重重的喘着气。"


translate chinese dv_mine_exit_cd113210:


    "我真的想回头看看她，不过我没有那个力气，也没有时间。"


translate chinese dv_mine_exit_8f8cca4d:


    "终于大门的轮廓出现在了视线中，我跃进防空洞，准备好面对一切敌人..."


translate chinese dv_mine_exit_208a6c81:


    "不过房子是空的..."


translate chinese dv_mine_exit_60e62f39:


    "我扫视整个房间，试图找到躲起来的怪物，舒里克，或者任何坏蛋，不过怎么看都和我们第一次来到这里时没什么区别。"


translate chinese dv_mine_exit_cf80a68b:


    "除了大门，第二扇大门...{w}是打开的！"


translate chinese dv_mine_exit_9e0f57f2:


    me "刚才有人经过了这里！"


translate chinese dv_mine_exit_1c4ba667:


    "我得意洋洋的说着。"


translate chinese dv_mine_exit_e12061da:


    dv "谁?"


translate chinese dv_mine_exit_f17c350e:


    me "不知道，也许就是舒里克..."


translate chinese dv_mine_exit_b26a8910:


    dv "可是他是如何打开大门的呢？"


translate chinese dv_mine_exit_8a8827fd:


    me "想那么多干嘛？快来吧！"


translate chinese dv_mine_exit_ce617998_2:


    "..."


translate chinese dv_mine_exit_dbf5f587:


    "门后边又是另一条走廊，而且在逐渐上升。"


translate chinese dv_mine_exit_6e4dbe96:


    th "所以说，我们在不断接近地表。"


translate chinese dv_mine_exit_36ebc0c6:


    "我猜对了，又走了几百米之后我被一个通往上方舱门的梯子绊倒。"


translate chinese dv_mine_exit_1c207941:


    "没用多大力气就打开了。"


translate chinese dv_mine_exit_4453f8b0:


    dv "上面有什么？"


translate chinese dv_mine_exit_4164ce88:


    me "别想了，反正比在下面好！"


translate chinese dv_mine_exit_6f888876:


    "我爬上梯子，发现自己被黑暗包围。"


translate chinese dv_mine_exit_6ca992b0:


    "然而这黑暗绝对要比那个古墓巷道轻一些，等我的眼睛适应了一些以后，墙壁的轮廓，梯子，还有通往户外的小屋就都在惨白的月光下显现了。"


translate chinese dv_mine_exit_8aeb5a0d:


    "我帮助阿丽夏向上爬，我们几乎是跳上来的。"


translate chinese dv_mine_exit_681894bf:


    "古墓的出口在一个不是幼儿园就是小学校的废墟里。"


translate chinese dv_mine_exit_24905350:


    dv "这一定是旧营地..."


translate chinese dv_mine_exit_630ee13c:


    "阿丽夏坐在一个秋千上，然后用手背擦着额头上的汗。"


translate chinese dv_mine_exit_0adb5982:


    me "原来如此，所以咱们最终还是找到了它。"


translate chinese dv_mine_exit_ef2764bc:


    "我不禁笑了起来。"


translate chinese dv_mine_exit_2a6404d7:


    dv "你笑什么啊？"


translate chinese dv_mine_exit_76027b11:


    "她皱着眉头，不高兴的撅起嘴来。{w}简单的说，她恢复了平常的自己。"


translate chinese dv_mine_exit_fc154605:


    me "所以说，这里不再那么可怕了吗？"


translate chinese dv_mine_exit_39ef9914:


    dv "我在下面也没有害怕！"


translate chinese dv_mine_exit_2de4d7dc:


    me "哎呦喂~"


translate chinese dv_mine_exit_f8f4d1e6:


    dv "哼，就没有！"


translate chinese dv_mine_exit_66368319:


    me "呃，算了..."


translate chinese dv_mine_exit_0812a5df:


    "我很生气。"


translate chinese dv_mine_exit_bb429265:


    th "我感觉好受伤，如果不是我，阿丽夏还在那个矿井里呆着呢！"


translate chinese dv_mine_exit_25532b53:


    dv "你去哪儿啊？"


translate chinese dv_mine_exit_6773d0b2:


    "她不那么高傲的问我，我则转身慢慢往营地走。"


translate chinese dv_mine_exit_19a037e5:


    me "回去。"


translate chinese dv_mine_exit_93b5fc50:


    dv "嘿!"


translate chinese dv_mine_exit_95ef7c80:


    "阿丽夏跳起来，跟着我并排走。"


translate chinese dv_mine_exit_fbe21e1f:


    "我一度想说一些不友好的话，不过我还不想马上引发下一次口水大战..."


translate chinese dv_mine_exit_860a390f:


    "我们很快找到了刚才走过的路，还有那两个我们掉下去的大洞。"


translate chinese dv_mine_exit_18eb7fb3:


    "离旧营地只有二百米左右。"


translate chinese dv_mine_exit_ce617998_3:


    "..."


translate chinese dv_mine_exit_47e214d8:


    "我在广场停下来，转向阿丽夏。"


translate chinese dv_mine_exit_505d91ae_1:


    me "好吧..."


translate chinese dv_mine_exit_8bfb630c:


    dv "好..."


translate chinese dv_mine_exit_4d3e55f3:


    "她有点动摇，甚至有些迷惑。"


translate chinese dv_mine_exit_2e84db32:


    me "那么..."


translate chinese dv_mine_exit_887ed8a6:


    dv "对啊..."


translate chinese dv_mine_exit_7f338ab0:


    "内心里我想要对她喊，大声责备她，甚至骂她。{w}不过反过来说我还是想进行一次有意义的对话。"


translate chinese dv_mine_exit_a3c6e840:


    "最后我保持了沉默，缓缓的转过身，准备返回辅导员的房间。"


translate chinese dv_mine_exit_5bda040e:


    "不过有什么东西突然打破了夜晚的和谐。{w}我敏锐的听着，原来是打呼噜的..."


translate chinese dv_mine_exit_ff984fa0:


    "...舒里克——他正躺在长椅上睡的正香！"


translate chinese dv_mine_exit_95efcfe1:


    me "嘿!"


translate chinese dv_mine_exit_997fab3e:


    "我叫着还没有走远的阿丽夏。"


translate chinese dv_mine_exit_79d3bbc8:


    me "起来呀，你给我！"


translate chinese dv_mine_exit_94f86bab:


    "舒里克慢慢的恢复了意识，眼神空洞的看着我们。"


translate chinese dv_mine_exit_a5cfef85:


    sh "哇，已经到了早晨了吗？"


translate chinese dv_mine_exit_ec14641b:


    me "早晨？是啊..."


translate chinese dv_mine_exit_565e9484:


    "有时候做的比想的快..."


translate chinese dv_mine_exit_d3e52cd4:


    "当然一切都来源于大脑，然后电流传到中枢，然后再传到身体各处。"


translate chinese dv_mine_exit_e6d884ea:


    "不过有时候潜意识要反应的更快些。{w}因此我已经挥起了手臂，砸在了舒里克的肚子上，这之后我才反应过来。"


translate chinese dv_mine_exit_92f15912:


    "他咳嗽着，想要喘气，在长椅上痛苦的扭曲着身体。"


translate chinese dv_mine_exit_8f6f0153:


    me "你丫的到底怎么回事？"


translate chinese dv_mine_exit_c415cb10:


    "我已经后悔了，我不应该那么鲁莽的..."


translate chinese dv_mine_exit_43efdcda:


    sh "你说...你说什么啊？"


translate chinese dv_mine_exit_40c0b040:


    "舒里克震惊的看着我。"


translate chinese dv_mine_exit_1a18e0c9:


    me "在矿井里啊！"


translate chinese dv_mine_exit_a786120b:


    sh "什么矿井？"


translate chinese dv_mine_exit_d18f8765:


    "他摇摇头，一副完全不明白的表情。"


translate chinese dv_mine_exit_1ccd3e61:


    sh "我为什么在这里？"


translate chinese dv_mine_exit_a031441e:


    sh "你为什么在这里？"


translate chinese dv_mine_exit_3f214d58:


    dv "别扯淡了！"


translate chinese dv_mine_exit_3d3d749e:


    "阿丽夏加入了对话。"


translate chinese dv_mine_exit_e44dae44:


    dv "你在下面差点打死我！现在还装不知道？！"


translate chinese dv_mine_exit_f86b3336:


    sh "怎么回事？"


translate chinese dv_mine_exit_7a20f42c:


    me "你不记得了？"


translate chinese dv_mine_exit_7e0c8149_2:


    sh "不知道啊..."


translate chinese dv_mine_exit_1780b682:


    me "好吧，那么你记得最后一件事是什么？"


translate chinese dv_mine_exit_7843e662:


    "舒里克试图集中注意力。"


translate chinese dv_mine_exit_32afcb7b:


    sh "那个，我今天早晨前往旧营地。我知道我可以在那里找到机器人的零件..."


translate chinese dv_mine_exit_f95c9fc9:


    me "然后呢?"


translate chinese dv_mine_exit_43309355:


    sh "没了...然后我就什么也不记得了，然后我就到这里了..."


translate chinese dv_mine_exit_e94e8b48:


    "我重重的叹了口气然后离开了。"


translate chinese dv_mine_exit_357bf682:


    me "好吧，接着睡吧你..."


translate chinese dv_mine_exit_32b62074:


    dv "嘿，你去哪儿...?"


translate chinese dv_mine_exit_381b0677:


    "我没管阿丽夏跌跌撞撞的跑回奥尔加·德米特里耶夫娜的房间。{w}她留在那里，和舒里克继续争吵。"


translate chinese dv_mine_exit_ce617998_4:


    "..."


translate chinese dv_mine_exit_594f36e4:


    "我坐在躺椅上看着星星。"


translate chinese dv_mine_exit_6b72237f:


    "今天的夜空看起来比往日更亮。"


translate chinese dv_mine_exit_4b4166ca:


    "也许是因为不久之前我的唯一光源就是手电筒还有那个火把..."


translate chinese dv_mine_exit_a3c69ac7:


    "星星绝对要比手电筒和火把亮。{w}我猜多数星星比太阳还要亮...只是离得太远了..."


translate chinese dv_mine_exit_d806349a:


    me "所以说你为什么来了呢？"


translate chinese dv_mine_exit_7fd15fd8:


    "我头也不回的问道，阿丽夏的声音在一个寂静的夜晚可以隔好远听得清清楚楚。"


translate chinese dv_mine_exit_29cec6ff:


    dv "那个，我..."


translate chinese dv_mine_exit_80ecf5fd:


    me "舒里克说了什么？"


translate chinese dv_mine_exit_6a9ad2e5:


    dv "他说他什么也不记得了，说那很不科学，说都是胡扯什么的。"


translate chinese dv_mine_exit_946b1e7d:


    me "我想他可能受到了惊吓真的什么都不记得了吧。"


translate chinese dv_mine_exit_2a5b356a:


    th "我说什么好呢？不久之前我也一样。"


translate chinese dv_mine_exit_b9277d5e:


    th "我好像还在其中，我有失忆症吗？"


translate chinese dv_mine_exit_9a5edc2d:


    me "所以说，你为什么来了呢？"


translate chinese dv_mine_exit_fa9696b2:


    "不过我想我知道答案，这也是为什么我没有去睡觉而是在这里等她。"


translate chinese dv_mine_exit_c9fe61bc:


    dv "那个，我..."


translate chinese dv_mine_exit_4eface95:


    "阿丽夏坐在我旁边。"


translate chinese dv_mine_exit_4e4c10a9:


    dv "有点...想谢谢你...总之，在地下...你...那个，你知道..."


translate chinese dv_mine_exit_92279f92:


    me "不客气。"


translate chinese dv_mine_exit_028267bd:


    "我平静的说着，微微向后靠。"


translate chinese dv_mine_exit_9763a346:


    dv "嗯，那好吧..."


translate chinese dv_mine_exit_cb0a1b3d:


    "她站起来准备离开。"


translate chinese dv_mine_exit_9492db5a:


    me "如果你觉得我对你不满，没有那回事，一切都很正常。"


translate chinese dv_mine_exit_54ef0861:


    dv "我没有那么想！"


translate chinese dv_mine_exit_6f52c0b0:


    "阿丽夏激动了。"


translate chinese dv_mine_exit_712a2ea4:


    me "那就好。"


translate chinese dv_mine_exit_8bfb630c_1:


    dv "好吧..."


translate chinese dv_mine_exit_751ac511_1:


    me "是啊..."


translate chinese dv_mine_exit_a1bcd7f2:


    dv "那么..."


translate chinese dv_mine_exit_1bd407b9:


    me "快走啦！"


translate chinese dv_mine_exit_19ed4ed8:


    "我友好的说着，并且招招手。"


translate chinese dv_mine_exit_eb93b5d8:


    dv "我想走的时候自然会走！"


translate chinese dv_mine_exit_cbd890f2:


    me "所以说你现在不想走吗？"


translate chinese dv_mine_exit_ebd91224:


    dv "想走！"


translate chinese dv_mine_exit_2b9ec1df:


    me "嗯？有什么东西阻碍你离开吗？"


translate chinese dv_mine_exit_82ddf868:


    dv "笨蛋！"


translate chinese dv_mine_exit_c46574f3:


    "阿丽夏跺着脚迅速离开了辅导员的小屋。"


translate chinese dv_mine_exit_b44e66a1:


    "我深吸了一口气站了起来。"


translate chinese dv_mine_exit_d5cb476b:


    "我因为疲劳感觉头晕目眩。"


translate chinese dv_mine_exit_05cac35e:


    th "唔，至少奥尔加·德米特里耶夫娜已经睡了，我不用解释什么..."


translate chinese dv_mine_exit_8e63c1c0:


    "不过好像没那么简单。"


translate chinese dv_mine_exit_69a264ae:


    "辅导员同志站在房间中央，显然准备进行一场持久的谈话。"


translate chinese dv_mine_exit_a38129f5:


    "或者是接受工作汇报。"


translate chinese dv_mine_exit_d8b1d698:


    mt "你想不想解释什么？"


translate chinese dv_mine_exit_132385d6:


    me "怎么啦?{w}而且我们出发找舒里克的时候你怎么不在意？"


translate chinese dv_mine_exit_28247bed:


    mt "什么，你找到他了吗？"


translate chinese dv_mine_exit_ded1e9af:


    "看起来她对我晚归要比对那个失踪的少先队员还要在意。"


translate chinese dv_mine_exit_3385696b:


    me "是啊，我们找到了。{w}话说，你为什么要在黑暗中站在这里？"


translate chinese dv_mine_exit_6334b91e:


    mt "什么?"


translate chinese dv_mine_exit_f72b1120:


    me "我说你为什么站在黑暗中？"


translate chinese dv_mine_exit_3597bffc:


    mt "因为到了睡觉的时间了。"


translate chinese dv_mine_exit_14658aae:


    "我无言以对，虽然我觉得这心情转换有点太快了。"


translate chinese dv_mine_exit_a20cefa7:


    "..."


translate chinese dv_mine_exit_a4baed18:


    "我一头栽在床上，衣服都没有换。"


translate chinese dv_mine_exit_b6a45476:


    th "不过，阿丽夏...{w}阿丽夏..."


translate chinese dv_mine_exit_fff7cafa:


    "不知道该想什么。"


translate chinese dv_mine_exit_f6f64c2c:


    "不是因为最近她表现奇怪。{w}不，相反，她的行为很一致而且可以理解。"


translate chinese dv_mine_exit_b0dedce5:


    "连她回来感谢我也一样。"


translate chinese dv_mine_exit_7bad5d82:


    "这个晚上我一直想着阿丽夏。"


translate chinese dv_mine_exit_8dbd75e9:


    "虽然仔细想想的话也没有什么超自然的。"


translate chinese dv_mine_exit_a701bda4:


    "不舒服，当然，害怕，身后一阵凉气，{w}不过和我来到这里似乎没有什么关系。"


translate chinese dv_mine_exit_b249ce58:


    "这些想法让我慢慢进入了梦乡。"


translate chinese day4_us_aa8cb942:


    us "算我一个！"


translate chinese day4_us_13d9d85c:


    "我惊讶的看着乌里扬娜。"


translate chinese day4_us_2452df7b:


    "她对于探险的渴望好像没有止境。"


translate chinese day4_us_60164174:


    us "你看！夜晚，鬼魂，旧营地——多好！"


translate chinese day4_us_bf708ac9:


    th "一方面来说，这种同伴会带来麻烦，不过另一方面，两个人走不会那么害怕..."


translate chinese day4_us_003878d1:


    mt "完美!"


translate chinese day4_us_af906072:


    "和其他女孩和奥尔加·德米特里耶夫娜说过再见之后，我们出发了。"


translate chinese day4_us_48cc7f3f:


    me "你把这个当成是悠闲地散步吗？"


translate chinese day4_us_e8b3b40c:


    us "是啊，有什么问题吗？"


translate chinese day4_us_94876b55:


    "乌里扬卡咯咯地笑着。"


translate chinese day4_us_b92e40c7:


    me "没什么..."


translate chinese day4_us_ed0914b5:


    "我叹了叹气。"


translate chinese day4_us_2534cd63:


    us "哇，等一下！{w}我得找一下手电筒！"


translate chinese day4_us_cd917d11:


    me "好。"


translate chinese day4_us_ad322fcb:


    "我自己正想提出那个建议。"


translate chinese day4_us_73291f33:


    th "看来我这一晚上不仅要探索一个旧营地，还要照顾一个五脊六兽的熊孩子..."


translate chinese day4_us_3ce9798d:


    "不过既然是乌里扬娜..."


translate chinese day4_us_4da4d2d1:


    th "我得加倍的小心。"


translate chinese day4_us_7bd87323:


    "电子小子告诉我说那个建筑是战争刚刚结束的时候建的。"


translate chinese day4_us_fd3c1cdc:


    "它看起来像是一个幼儿园，或者是什么兵营，看起来装不下现在这么多的人。"


translate chinese day4_us_e9bbdb04:


    "它已经废弃了二十年了…"


translate chinese day4_us_b5d9b351:


    "乌里扬卡蹦蹦跳跳的往前走，好像做游戏一样。"


translate chinese day4_us_5f48e206:


    "我很不爽。"


translate chinese day4_us_71ff7e44:


    "事实上一个人如果在我的处境也会有同样的感受，一个月黑风高夜，各种怪物怪鸟还有我原来的世界什么的..."


translate chinese day4_us_1608aae1:


    "也许我自一个人还要好一些，不用照顾在前面跑来跑去的乌里扬卡。"


translate chinese day4_us_04ecc3b7:


    th "为什么她还不绊个跟头？"


translate chinese day4_us_bd8fd797:


    me "听着，小心点..."


translate chinese day4_us_d4cf3f77:


    us "否则会怎样？"


translate chinese day4_us_0382e70f:


    "她转身太快，让我哆嗦了一下。"


translate chinese day4_us_74e537ca:


    me "没事，你可能伤到自己。"


translate chinese day4_us_aae12bf3:


    us "你在担心我吗？"


translate chinese day4_us_dd02f8d8:


    me "当然啊，在这种情况下，这很正常啊。"


translate chinese day4_us_c0584c4d:


    "乌里扬卡撅撅嘴。"


translate chinese day4_us_17717ee8:


    me "听着..."


translate chinese day4_us_6ba67613:


    "我决定继续我们的对话——这样可以更加放松。"


translate chinese day4_us_9d0c5198:


    me "不过你说旧营地怎么来着？在广场的时候..."


translate chinese day4_us_87d3ffe3:


    us "是啊，那是个很可怕的地方，据说那里的少先队员都死掉了然后变成鬼魂守护着他们的庇护所。"


translate chinese day4_us_7db7b174:


    me "他们是怎么死掉的？"


translate chinese day4_us_3d71a838:


    "她的鬼故事总是有点不太可信。"


translate chinese day4_us_f62f4e9c:


    us "我怎么知道？我那时还没有出生呢？"


translate chinese day4_us_622c73dd:


    me "不过你说得很有信心呢。"


translate chinese day4_us_2585e19d:


    us "我有可靠的情报来源！"


translate chinese day4_us_578a42ac:


    me "那是哪里？从阿丽夏那里来吗？"


translate chinese day4_us_ccb042d0:


    us "不告诉你！"


translate chinese day4_us_fa7e457b:


    me "接下来发生了什么？他们都变成了鬼魂？"


translate chinese day4_us_38197147:


    us "没了..."


translate chinese day4_us_974a0ad3:


    me "什么叫“没了”？"


translate chinese day4_us_2dd06aae:


    us "现在死去的少先队员的鬼魂环绕在营地周围把想要闯入死者的世界的人们带走！"


translate chinese day4_us_7ba98731:


    me "哇！好了，快走！"


translate chinese day4_us_ce617998:


    "..."


translate chinese day4_us_71ed9190:


    "时间慢慢过去，我们逐渐走到森林深处，突然我发现森林变得十分寂静。"


translate chinese day4_us_3e13ee96:


    "好像夜晚的鸟儿都躲藏了起来，虫子都钻到了地下，连风都消失了。"


translate chinese day4_us_25d125dd:


    "我想象月光像扯断的琴弦一样穿透植被。"


translate chinese day4_us_171ac87d:


    "之后我们走出了树林，来到了一片林中空地..."


translate chinese day4_us_609185f4:


    "中间坐落着一座老旧的像是幼儿园一样的房子。"


translate chinese day4_us_56ad6a30:


    "整个像是一个浓雾包围的目的，然后旧营地在中间就像是古老教堂的地下墓穴。"


translate chinese day4_us_883f8d73:


    "据乌里扬卡说少先队员的灵魂在这里漫游，真是像一个大墓地..."


translate chinese day4_us_b8d90de9:


    "我哆嗦了一下，握紧了手电筒。"


translate chinese day4_us_b92c533e:


    me "说实话是一个瘆人的地方..."


translate chinese day4_us_4b95ee74:


    us "哎呀~小鬼~"


translate chinese day4_us_632560ea:


    "她欢快的拍着我的肩膀，让我更加害怕。"


translate chinese day4_us_96025918:


    "我正想往前走，突然月光照射到空地上，照亮了那座破旧的建筑。"


translate chinese day4_us_d383d5fe:


    "在月光下一切显得不再那么老旧：破碎的砖墙，生锈的滑梯和旋转木马，残破的玻璃窗，都变得生动起来。"


translate chinese day4_us_90df7238:


    "我开始想象未知的怪物从森林里钻出来，一个永远被黑暗笼罩的地方..."


translate chinese day4_us_97e4fa69:


    th "我希望他们会像吸血鬼一样害怕光...{w}虽然他们也可能会在满月下变成狼人..."


translate chinese day4_us_bc306349:


    us "你为什么停下来了？"


translate chinese day4_us_1bb0a12c:


    me "我在思考..."


translate chinese day4_us_6a81728a:


    us "思考什么？"


translate chinese day4_us_61fe8457:


    me "舒里克为什么要来这个地方呢？"


translate chinese day4_us_5fd5e401:


    us "我怎么知道？咱们找到他问问不就行了吗？"


translate chinese day4_us_ab70605d:


    me "是啊，显然..."


translate chinese day4_us_60414457:


    "我自言自语的跟着乌里扬卡。"


translate chinese day4_us_2a3004d5:


    "她这下走起来小心的多，不时地看看脚下，偶尔还回头看看背后。"


translate chinese day4_us_39ff0700:


    "正常，有的地方草丛甚至能到她的胸部，谁知道里面埋着什么..."


translate chinese day4_us_87203d37:


    "我们终于来到了大门，乌里扬娜停下来说："


translate chinese day4_us_3110f8c7:


    us "呼~我们来啦！"


translate chinese day4_us_af4772b5:


    me "好像你赢了什么比赛似的，又没人和你比。"


translate chinese day4_us_05780ff1:


    us "你...你简直是..."


translate chinese day4_us_4e7dfaf3:


    me "什么?"


translate chinese day4_us_024305f8:


    us "无聊！"


translate chinese day4_us_f39b4d19:


    "我做了一个不爽的表情然后走进了黑暗之中，让乌里扬娜在前面走太危险了，对她来说很危险，对我也很危险，对人类都很危险。"


translate chinese day4_us_ec2f3403:


    "旧营地的内部让人感觉很压抑，我一度觉得很伤心。{w}也许是对当时生活在这里的人们。"


translate chinese day4_us_e2350d58:


    "这里过去充满了孩子们的欢笑。小孩子们跑来跑去，玩着我已经忘记的游戏，还有一个像是奥尔加·德米特里耶夫娜一样的辅导员维持着纪律。"


translate chinese day4_us_b4495206:


    "现在只是一堆正在生锈的破铜烂铁。"


translate chinese day4_us_30f4be0c:


    us "看!"


translate chinese day4_us_ca4f2dbb:


    "乌里扬卡递给我一个破旧的娃娃，承载着过去的回忆。"


translate chinese day4_us_9bfb0137:


    me "怎么了?"


translate chinese day4_us_3005a190:


    us "没什么..."


translate chinese day4_us_8a70ba7f:


    "她走出了光线，不过我发现她的脸上有一丝难过。"


translate chinese day4_us_4280984e:


    me "就像是墓地一样充满悲伤。"


translate chinese day4_us_614210cb:


    us "可怕！"


translate chinese day4_us_64a3eba4:


    me "我还没有看见什么可怕的东西。"


translate chinese day4_us_fbc2f69e:


    "这里面比外面还要放松一些。"


translate chinese day4_us_a9de51da:


    "这里确实像一座墓地，你走在墓碑旁边，心中很难受，但是不知为何你感觉到自己灵魂的平静。{w}当你躺在旁边的时候..."


translate chinese day4_us_311704a0:


    "我有些发抖的用手电筒照着屋子。{w}没有舒里克的迹象。"


translate chinese day4_us_d6c1b117:


    "不过我到底期望着在这里找到什么呢？他的尸体？"


translate chinese day4_us_7b1fd8de:


    me "看起来他不在这里..."


translate chinese day4_us_36335213:


    us "二层呢？"


translate chinese day4_us_b01a7f13:


    me "舒里克！"


translate chinese day4_us_bf4be854:


    "我大声的喊着，不过只有自己的回声回答着我。"


translate chinese day4_us_1f4ba85c:


    us "舒里克！出来！"


translate chinese day4_us_8a79c954:


    me "看..."


translate chinese day4_us_883708ad:


    us "那也应该检查一下。"


translate chinese day4_us_53c9fe63:


    me "好，好..."


translate chinese day4_us_ce617998_1:


    "..."


translate chinese day4_us_784add86:


    "二楼也没有生命存在的迹象。"


translate chinese day4_us_ae6a6307:


    "我坐在楼梯上，耷拉着脑袋。"


translate chinese day4_us_080b0b24:


    me "接下来该去哪里呢？总不能搜索整个树林吧。我已经困了..."


translate chinese day4_us_66088108:


    us "你怎么跟个怨妇一样啊？"


translate chinese day4_us_8f18aa98:


    "乌里扬卡怒了。"


translate chinese day4_us_8285506c:


    us "抱怨个没完，真是孩子气。"


translate chinese day4_us_f75ea3f6:


    me "啊？这不对吗？"


translate chinese day4_us_d92dc560:


    us "对不对，有什么区别，如果咱们是要找舒里克，就必须找到！"


translate chinese day4_us_04098bfe:


    me "但是怎么找？"


translate chinese day4_us_a43ed67b:


    "我祈求着。"


translate chinese day4_us_78f6e14d:


    us "不知道！某种方法吧！"


translate chinese day4_us_add3ace1:


    "乌里扬卡看起来像是一个严厉的老师在教育淘气的学生。"


translate chinese day4_us_1a536bbc:


    th "可是不是应该反过来吗？"


translate chinese day4_us_30f4be0c_1:


    us "看！"


translate chinese day4_us_0939ec56:


    "我把手电照向她指的方向，看到了一个暗门，被埋在垃圾里。{w}看起来不久之前才被打开过。"


translate chinese day4_us_4e111b28:


    us "他一定是在下面！"


translate chinese day4_us_ede7df57:


    "乌里扬卡冲向舱门，费了好大力气试图打开。"


translate chinese day4_us_92badaea:


    me "他为什么要下去？...不知道，有没有可能是附近的居民什么的？"


translate chinese day4_us_930ac3c9:


    us "不知道..."


translate chinese day4_us_cb4ca95f:


    "乌里扬卡伴随着重重的呼吸回答道。{w}她没能打开暗门。"


translate chinese day4_us_89141735:


    "我开始思考。"


translate chinese day4_us_2f3a091c:


    "舒里克确实很有可能在下面。"


translate chinese day4_us_b73efd8f:


    "我不了解这个世界，为什么这里的居民要按照我的逻辑行动呢？{w}会不会是他试图逃到下面躲避狼群？"


translate chinese day4_us_8e32b775:


    th "这附近有狼群吗？也许是猫头鹰？"


translate chinese day4_us_9dc26b9a:


    me "好吧，我来试试..."


translate chinese day4_us_a11b339c:


    "我绷紧肌肉，拉动小门。"


translate chinese day4_us_93757775:


    "伴随着巨响它倒在了地板上，同时乌里扬卡拿着手电筒向里面照来照去。"


translate chinese day4_us_369cfea8:


    us "是一条隧道！"


translate chinese day4_us_631fa55f:


    me "隧道？"


translate chinese day4_us_b405ffe1:


    "我一下把她拎回来，自己探着头向里面看。"


translate chinese day4_us_d5d98923:


    us "喂!"


translate chinese day4_us_e4344f7f:


    me "好奇心害死猫！"


translate chinese day4_us_8b032119:


    "确实有一条隧道向黑暗中延伸，像是电脑游戏中的地下城。"


translate chinese day4_us_53694ead:


    "一眼看上去没有那么多的危险：没有齐腰的深水，大老鼠，僵尸..."


translate chinese day4_us_4fbc46ec:


    me "好了，咱们趴下去看看，不过要小心！"


translate chinese day4_us_c0841288:


    us "收到！"


translate chinese day4_us_b8db1da9:


    "乌里扬卡喊着。"


translate chinese day4_us_ce617998_2:


    "..."


translate chinese day4_us_c2440a9c:


    "下面好黑，手电筒不太管用。它昏暗的照着水泥墙，房顶上的灯，墙壁上的电线，还有满地的垃圾..."


translate chinese day4_us_34477885:


    "我们慢慢的向前走，我拉着乌里扬卡的手，怕她自己跑掉。"


translate chinese day4_us_f835b5ac:


    "可能也是为了我自己，不想独自面对着这压抑的黑暗。"


translate chinese day4_us_c6aac5b3:


    me "也许咱们应该回去？舒里克不可能走这么远的..."


translate chinese day4_us_951106fa:


    us "如果他有一个手电筒呢？"


translate chinese day4_us_8a8c5c02:


    me "即使他有一个手电筒...他来这里能干什么啊？"


translate chinese day4_us_3e242075:


    us "我不知道啊！你不对前面的未解之谜感兴趣吗？"


translate chinese day4_us_990233c8:


    me "一点都没，我还是更喜欢..."


translate chinese day4_us_54c2445e:


    "我还没有说完，我们的面前突然出现了一座金属门——好像凭空出现一样。"


translate chinese day4_us_87a07145:


    "我马上发现了一个生化危机标志。"


translate chinese day4_us_28acc263:


    me "一个防空洞？"


translate chinese day4_us_5211f0b6:


    "我明白过来。"


translate chinese day4_us_41e59a3d:


    us "大概是的。"


translate chinese day4_us_171958cc:


    me "你听说过关于这个的故事吗？"


translate chinese day4_us_28220b96:


    us "不知道啊，有什么关系吗？"


translate chinese day4_us_4f8c4714:


    me "有什么关系...？万一有辐射怎么办？"


translate chinese day4_us_7ef85af6:


    us "什么辐射？从哪里？"


translate chinese day4_us_38d6ffbb:


    me "哈，你说得对..."


translate chinese day4_us_9bb98464:


    "我试图拧动阀门。"


translate chinese day4_us_8cb0acfd:


    "居然动了，像一个受伤的恐龙一样嚎叫着。"


translate chinese day4_us_8bd067c0:


    "门打开了，乌里扬卡迅速跳了进去。"


translate chinese day4_us_95efcfe1:


    me "嘿！"


translate chinese day4_us_396dea3d:


    "门后面可能就是防空洞的主要房间。"


translate chinese day4_us_deed307a:


    "我的旁边有几张床，远处的墙边还有好多设备，柜子什么的。屋顶的荧光灯照着昏暗的房间。"


translate chinese day4_us_2b06fcd1:


    me "这电是从哪里来的？这里还有备用的发电机吗？"


translate chinese day4_us_3cb76044:


    "我关上了手电筒节省能源。"


translate chinese day4_us_464f6f21:


    "乌里扬卡开始翻箱倒柜，拿出一堆防毒面具，小包包，还有各种工具。"


translate chinese day4_us_1a51a1af:


    me "你就没有什么正经事可以做了吗？"


translate chinese day4_us_0dcdea24:


    us "没了！"


translate chinese day4_us_8263b828:


    "她不爽的看着我。"


translate chinese day4_us_efbfc8f1:


    "我坐在整洁的床铺上，又开始环视四周。"


translate chinese day4_us_44a51345:


    "房间对面又是一扇大门，和进来的那扇一样。"


translate chinese day4_us_cac4f276:


    th "舒里克有没有可能来到过这个地方呢？如果来过的话，那是为什么呢？"


translate chinese day4_us_48e24bcd:


    us "怎么了你？"


translate chinese day4_us_b89cbb7e:


    me "只是有点累..."


translate chinese day4_us_9fb02d3f:


    "我如实的承认了。"


translate chinese day4_us_f062a0a0:


    us "那就歇一会儿！"


translate chinese day4_us_54ad0367:


    "乌里扬卡跳过来推着我的胸口。"


translate chinese day4_us_b9943d3a:


    "我向后踉跄了两步，脑袋撞倒了墙。"


translate chinese day4_us_fc7f611f:


    me "你干什么！"


translate chinese day4_us_8776d0b9:


    "我抓住她的胳膊把她拉过来。"


translate chinese day4_us_53d31d5c:


    "她失去了平衡跌倒在我身上。"


translate chinese day4_us_ed5f24ac:


    us "哎呦!"


translate chinese day4_us_60bb0a79:


    me "这是你先开始的！"


translate chinese day4_us_4b1f34cb:


    "乌里扬卡吐吐舌头站了起来。"


translate chinese day4_us_02aa8fe6:


    me "那么，然后呢？"


translate chinese day4_us_2fa697bf:


    us "那里，那里有另一个人..."


translate chinese day4_us_00a82683:


    me "啊，是啊，舒里克预言要发生核大战，已经提前躲起来了吧。"


translate chinese day4_us_c399a64c:


    us "也许是真的！"


translate chinese day4_us_1759bd02:


    me "也许，这个防空洞离店面太近了，它可以防辐射，不过如果炸弹太近的话......"


translate chinese day4_us_3503d6d7:


    us "你太严肃了吧。"


translate chinese day4_us_fde537c1:


    "她傻笑着。"


translate chinese day4_us_8a58cac1:


    "我真不知道什么时候该和她认真，什么时候该开玩笑。"


translate chinese day4_us_9f4bfb7f:


    "看来我们有代沟..."


translate chinese day4_us_a51509b7:


    th "是多大呢？10年？"


translate chinese day4_us_1ab60ac4:


    "我叹叹气从床上站起来。"


translate chinese day4_us_99949f0f:


    me "好吧，来试试。"


translate chinese day4_us_fad4bb18:


    "不过这扇门可没有第一个那么听话。"


translate chinese day4_us_a23c0e66:


    "不停地嘎吱嘎吱响，但是阀门一厘米也没有动。"


translate chinese day4_us_9da6b067:


    me "好像卡住了..."


translate chinese day4_us_afc3494c:


    us "我来吧！"


translate chinese day4_us_76c86e05:


    "乌里扬卡拿着一根撬棍冲过来，用上全身的重量，虽然一共也没多少分量。"


translate chinese day4_us_d0cada00:


    "这下有门，我帮着她用力，很快大门倒了下来。"


translate chinese day4_us_50d5d9d2:


    "原来合页完全锈死了。"


translate chinese day4_us_0c596808:


    th "看来当时的设计也不是都那么耐久..."


translate chinese day4_us_1df933d0:


    "门后又是一条差不多的走廊。"


translate chinese day4_us_4bb12c41:


    me "不要做愚蠢的事情！"


translate chinese day4_us_99685367:


    "我拉着乌里扬卡走出了房间。"


translate chinese day4_us_ce617998_3:


    "..."


translate chinese day4_us_8ea41ecb:


    "又是一个永无止境的隧道。"


translate chinese day4_us_83e78ba1:


    "隧道的顶部似乎越来越低了，虽然我理智的判断觉得并没有变化。"


translate chinese day4_us_4b91848a:


    "乌里扬卡好像一点也不担心，她一边走着一边哼着歌儿。"


translate chinese day4_us_6ed9af94:


    "这让我更加心烦。"


translate chinese day4_us_7a841c5f:


    me "你好像还玩得挺高兴..."


translate chinese day4_us_83eff6be:


    us "当然了！你不是吗？"


translate chinese day4_us_7a50435b:


    me "不，我完全没有这种感觉！咱们应该马上找到舒里克然后离开这个地方！"


translate chinese day4_us_d877fb1e:


    us "他可能根本就不在这里。"


translate chinese day4_us_94eb0d42:


    me "那为什么...?"


translate chinese day4_us_30f4be0c_2:


    us "看!"


translate chinese day4_us_4f298886:


    "乌里扬卡抢过我的手电筒。"


translate chinese day4_us_bb4d07b2:


    "我们面前有一个几米宽的大洞。"


translate chinese day4_us_3fb17507:


    us "也许他在下面！"


translate chinese day4_us_974d3351:


    "她走过去观察。"


translate chinese day4_us_3e125f8d:


    us "一些轨道..."


translate chinese day4_us_00e5898d:


    "看起来隧道下面是一个矿井。"


translate chinese day4_us_3dd043c8:


    "洞的深度可以让我们爬上来，所以我猜到乌里扬卡要说什么。"


translate chinese day4_us_2e1a85d1:


    us "来吧！"


translate chinese day4_us_70ddfdf6:


    "我想拒绝，不过她已经跳下去了，只剩下我一个人在黑暗之中。"


translate chinese day4_us_95efcfe1_1:


    me "嘿!"


translate chinese day4_us_e539b0ce:


    "我只能跟着她。"


translate chinese day4_us_98c927dd:


    "我不知道他们在这里挖什么，不过这个地方已经被废弃了很久了。木料已经腐烂，铁轨也生锈了，墙壁上露出了内层的泥土。"


translate chinese day4_us_ac3a62f7:


    "整个隧道都很可疑，看起来像是要坍塌然后把我们埋在里面一样。"


translate chinese day4_us_da8e6eb5:


    us "快来啊！"


translate chinese day4_us_e1d9394c:


    "乌里扬卡使劲拉着我的手臂。"


translate chinese day4_us_20e92b26:


    me "去哪儿啊？为什么？舒里克会在那里吗？"


translate chinese day4_us_46a40361:


    us "万一...?!"


translate chinese day4_us_cfc2bc6d:


    "她表情变严肃了。"


translate chinese day4_us_0c35b5c3:


    us "万一他就在这里受伤等待救援，而咱们扔下他回去，让他自生自灭？"


translate chinese day4_us_55b2a7b1:


    "我估计了一下隧道顶的高度，然后又跟着乌里扬卡跑起来。"


translate chinese day4_us_ce617998_4:


    "..."


translate chinese day4_us_b17c36b3:


    "很快我们来到了一个岔路口。"


translate chinese day4_us_830688df:


    us "咱们往右走！"


translate chinese day4_us_c6dd4736:


    me "等一下！"


translate chinese day4_us_36ea64e3:


    "我拉住她的胳膊。"


translate chinese day4_us_5da43ef2:


    us "怎么了？"


translate chinese day4_us_3688cbd4:


    me "万一是死路？或者是迷宫呢？"


translate chinese day4_us_52f35a85:


    us "那个..."


translate chinese day4_us_60f3d758:


    "她想了想。"


translate chinese day4_us_c86b37ad:


    us "那咱们在开始的地方做一个标记吧！"


translate chinese day4_us_8083c22c:


    "乌里扬卡捡起了一块大石头然后在木头柱子上刻了一个叉。"


translate chinese day4_us_f8d84cb9:


    me "你觉得这有帮助吗？"


translate chinese day4_us_3e60cbf2:


    us "有啊！"


translate chinese day4_us_511d63f8:


    th "也许我得选择去哪里，我受不了这个小女孩了。"


translate chinese us_mine_coalface_e40597aa:


    "我们已经来过这里了。"


translate chinese us_mine_coalface_5b4c3c90:


    "最后我们走出了隧道，来到了一个有高高屋顶的大厅。"


translate chinese us_mine_coalface_3ef8c583:


    "虽然它不太像一个大厅，他们以前一定是在这里挖矿来着。{w}煤矿吧，也许是金矿。"


translate chinese us_mine_coalface_3c54fe69:


    "墙面有十字镐和气钻凿过的痕迹。"


translate chinese us_mine_coalface_c718c063:


    "这个地方一片漆黑，所以我们只能依赖手电筒了。"


translate chinese us_mine_coalface_b97863a3:


    th "如果这里坍塌的话，我们大概是没有机会逃出去的…"


translate chinese us_mine_coalface_58411121:


    "在灯光中，我看到了一块红布。"


translate chinese us_mine_coalface_ca0a4f90:


    "是一条红领巾！"


translate chinese us_mine_coalface_96b3272c:


    "舒里克显然来过这里。"


translate chinese us_mine_coalface_93ec21f7:


    me "舒里克！舒里克！"


translate chinese us_mine_coalface_455d9f62:


    us "舒里克!"


translate chinese us_mine_coalface_b99be3e2:


    "只有我们的回声回答着。"


translate chinese us_mine_coalface_15e63d5a:


    me "希望他没事..."


translate chinese us_mine_coalface_f87c925c:


    us "别担心！咱们会找到他的！"


translate chinese us_mine_coalface_a411960f:


    th "但是他可能去哪里了呢？"


translate chinese us_mine_coalface_686bf61c:


    "这个房间没有别的出口。"


translate chinese us_mine_coalface_7ea80226:


    th "所以说隧道里可能还有我们没有去过的地方..."


translate chinese us_mine_coalface_858b6cbe:


    th "看来我们还得继续找他！"


translate chinese us_mine_exit_26c6312e:


    "最后我们在黑暗中发现了一扇旧木门。"


translate chinese us_mine_exit_0e7f3bff:


    us "就是这里了！"


translate chinese us_mine_exit_25f7abbc:


    me "哪里?"


translate chinese us_mine_exit_03e083fb:


    us "哪里？我不知道。"


translate chinese us_mine_exit_e2cbe440:


    "不过她说的有道理，我们终于走出迷宫了。"


translate chinese us_mine_exit_1467f5ac:


    "走过了那么多岔路之后我估计我们已经找不到回去的路了，不过一个矿怎么会没有出口呢？"


translate chinese us_mine_exit_5ac980a3:


    "乌里扬卡打开门，看着眼前的黑暗。"


translate chinese us_mine_exit_6acdac4b:


    me "所以说你不想像平常一样第一个冲出去吗？"


translate chinese us_mine_exit_52f35a85:


    us "这个..."


translate chinese us_mine_exit_01822020:


    me "好吧。"


translate chinese us_mine_exit_02157de8:


    "我跨过门槛。"


translate chinese us_mine_exit_80505c24:


    "门后面又是一个小房间，可能是一个储藏室或者另外一个防空洞。"


translate chinese us_mine_exit_c7136184:


    "这里有瓶子还有烟头，说明之前有人来过。"


translate chinese us_mine_exit_ecb7a6ba:


    "这倒不是多惊人的事实，不过这下我知道这里肯定有另外的出口，因为这里的人们不会和我们走一样的路吧。"


translate chinese us_mine_exit_56d7df1e:


    "光线在屋中扫来扫去，检查着每一个角落，突然它照亮了一个人影..."


translate chinese us_mine_exit_1925a7ca:


    "是舒里克！蜷缩在墙角！"


translate chinese us_mine_exit_72821d25:


    me "嘿！你在这里啊！我们已经找了你一晚上了..."


translate chinese us_mine_exit_212f5a54:


    "不过他好像没有注意到我们，只是坐在那里自言自语。"


translate chinese us_mine_exit_b01a7f13:


    me "舒里克!"


translate chinese us_mine_exit_58f9ccc4:


    sh "谁...那是谁？"


translate chinese us_mine_exit_21a8f802:


    me "什么叫“谁”！是你的救援队！快跟我们走啊！"


translate chinese us_mine_exit_42731173:


    sh "我哪里也不去！"


translate chinese us_mine_exit_7c4bde06:


    "他继续自言自语。"


translate chinese us_mine_exit_71df32d3:


    sh "你又会带着我在隧道里兜圈子，我哪里也不去！我就在这里！你拿我没办法！"


translate chinese us_mine_exit_1882e4f8:


    me "别扯淡了！"


translate chinese us_mine_exit_c5c0b112:


    "他好像发疯了。"


translate chinese us_mine_exit_aeb6a5a7:


    sh "不，不！这次你不会得逞了！"


translate chinese us_mine_exit_8b610151:


    me "你有完没完？！..."


translate chinese us_mine_exit_9a85ecb6:


    "我朝着舒里克走出几步，不过他立刻跳了起来，手里挥舞着一个铁棍。"


translate chinese us_mine_exit_1817dc02:


    sh "别过来！让我自己待着！"


translate chinese us_mine_exit_bf9a997b:


    me "冷静！是我！谢苗！你不认识我了？"


translate chinese us_mine_exit_9f720364:


    sh "谢苗...？不，你不是谢苗！"


translate chinese us_mine_exit_48892acd:


    "我注意到乌里扬卡，刚才还站在我旁边，现在消失了。"


translate chinese us_mine_exit_dd35a3e1:


    sh "你不是谢苗，我要..."


translate chinese us_mine_exit_b64a44f7:


    "在颤抖的灯光中舒里克的手挥舞着棍子消失了，我本能的挡住自己的脑袋。"


translate chinese us_mine_exit_f5b87a6f:


    "什么也没有发生。"


translate chinese us_mine_exit_2533f390:


    "当我睁开眼的时候，舒里克已经消失了，乌里扬卡站在我的旁边，握着那根铁棍咯咯笑着。"


translate chinese us_mine_exit_af36de66:


    us "像一个真正的童子军!"


translate chinese us_mine_exit_4c03fe75:


    me "是啊，童子军..."


translate chinese us_mine_exit_19ad538e:


    "从不知什么地方远远的传来了舒里克丧心病狂的笑声。"


translate chinese us_mine_exit_92bb7ff9:


    us "他跑掉了..."


translate chinese us_mine_exit_d0833b8f:


    me "去他的，他死在这里我也不管了！"


translate chinese us_mine_exit_ff689675:


    "我朝地上吐了口吐沫，然后靠向墙壁。"


translate chinese us_mine_exit_3429bb5b:


    th "如果不是乌里扬娜..."


translate chinese us_mine_exit_c05d018c:


    "我没有准备好！{w}舒里克可能不会杀掉我，但是可能会把我弄成重伤。"


translate chinese us_mine_exit_95c2fa20:


    "重伤着躺在这里和被打死差不多。我们都不知道什么时候救援会到。{w}而且他们究竟能不能穿过这个迷宫找到我们？"


translate chinese us_mine_exit_9eb821b3:


    "我为什么同意来这里的？{w}都是由着她来..."


translate chinese us_mine_exit_85b29f3a:


    us "你看起来好像要杀了谁似的。"


translate chinese us_mine_exit_058d6daa:


    me "如果有人自告奋勇的话。"


translate chinese us_mine_exit_07e9d3d8:


    "乌里扬卡哆嗦了一下。"


translate chinese us_mine_exit_dbe0f3c1:


    me "不，不是你。如果要打打你的屁股很不错，不过不是要杀了你。"


translate chinese us_mine_exit_f0d6d1c8:


    "她傻笑着。"


translate chinese us_mine_exit_b16368a6:


    me "暂时..."


translate chinese us_mine_exit_50d4d0ba:


    us "你！"


translate chinese us_mine_exit_850926ff:


    me "好了，现在该走了，他们明天也许会派来警察，特务还是什么，我可不管了。"


translate chinese us_mine_exit_a661ea5a:


    us "咱们要往回走吗？"


translate chinese us_mine_exit_479f496b:


    "我看看整个房间，又在我的左边看到了一扇门。"


translate chinese us_mine_exit_c2e764e0:


    me "哇。"


translate chinese us_mine_exit_1a153a40:


    "就像是在防空洞的那个大门一样。"


translate chinese us_mine_exit_e592b1be:


    "我拉动了几下把手，但是听到了笨重的嘎吱声。"


translate chinese us_mine_exit_3e37cbde:


    th "要是我带着那根撬棍..."


translate chinese us_mine_exit_3324aa7a:


    us "不管用吗？"


translate chinese us_mine_exit_0c87327f:


    "乌里扬卡失落的问着。"


translate chinese us_mine_exit_5cb822ee:


    me "不行啊..."


translate chinese us_mine_exit_48ef60d6:


    "说实话我已经没有力气了。"


translate chinese us_mine_exit_94802177:


    "放在平时我可能会放下面子求乌里扬娜帮我，或者找一根棍子什么的，不过现在我只想走出这个矿井。"


translate chinese us_mine_exit_85c47979:


    "我真希望这里有个捷径..."


translate chinese us_mine_exit_334e8c15:


    "这意味着我得记得穿过迷宫过来时的路。"


translate chinese us_mine_exit_3486283d:


    me "咱们回去吧。"


translate chinese us_mine_exit_89d3eb13:


    us "好吧。"


translate chinese us_mine_exit_5f443c07:


    "她笑着拉起来我的手。"


translate chinese us_mine_exit_ce617998:


    "..."


translate chinese us_mine_exit_e26fdffd:


    "我们在返回迷宫的路上走得更慢了。"


translate chinese us_mine_exit_db3ba18f:


    "脚下的石子散落着，头顶上不断有水滴滴落。"


translate chinese us_mine_exit_dc5bc7a5:


    "乌里扬卡静静的跟着我。"


translate chinese us_mine_exit_ea63ce77:


    me "发生了什么事吗？"


translate chinese us_mine_exit_5f2c0f51:


    us "什么意思？"


translate chinese us_mine_exit_8a64dac8:


    me "你一分钟不说话就显得很不正常。"


translate chinese us_mine_exit_0242fc7b:


    us "没有，一切正常..."


translate chinese us_mine_exit_c417bbd2:


    "不过肯定有什么不太对劲。"


translate chinese us_mine_exit_a92297a2:


    "一个接一个的岔路。"


translate chinese us_mine_exit_04ad16d0:


    "一分钟以前我确定下一个路口就会看到乌里扬卡画的叉叉，但是我错了。"


translate chinese us_mine_exit_d65de998:


    "我对于自己空间感的自信随着时间不断流失。"


translate chinese us_mine_exit_9e4e4c62:


    me "算了，不管了。"


translate chinese us_mine_exit_554c40e7:


    "我决定聊天来分散自己的注意力。"


translate chinese us_mine_exit_6d9d0fd7:


    us "还好啊，我只是..."


translate chinese us_mine_exit_ad02f53a:


    me "什么？"


translate chinese us_mine_exit_c6444665:


    us "这一点都不正常...舒里克什么的，我们现在堵在了这里。"


translate chinese us_mine_exit_a54e284d:


    me "你救了我，你应该很自豪。"


translate chinese us_mine_exit_ccd2dd89:


    "我尝试着鼓励乌里扬卡，不过似乎没有多大的作用。"


translate chinese us_mine_exit_b1356ab0:


    us "不过如果我没有把他的棒子拿走的话，他可能就不会逃走了。"


translate chinese us_mine_exit_51f70f47:


    me "那有什么意义啊，不管怎么说，咱们都还得走出这个隧道，不是吗？"


translate chinese us_mine_exit_c3e3f337:


    us "是啊，不过..."


translate chinese us_mine_exit_af351568:


    me "一切都没问题的，那个疯子会找到出口的。"


translate chinese us_mine_exit_aa7fa335:


    "这我很确定。"


translate chinese us_mine_exit_ce617998_1:


    "..."


translate chinese us_mine_exit_f7cc90bf:


    "最终我们来到了一条长长的隧道，墙上画这那个大大的叉叉。"


translate chinese us_mine_exit_3daa2b3d:


    "乌里扬卡打起了精神，我们几乎是跑完最后的一段路的。"


translate chinese us_mine_exit_de2c6d73:


    me "你看！"


translate chinese us_mine_exit_b88638a5:


    "满月又一次照耀着我们，旧营地的建筑看起来也不像是凶兆了。{w}尤其是和防空洞和古墓比起来。"


translate chinese us_mine_exit_6bffdd99:


    us "好厉害啊，是不是？"


translate chinese us_mine_exit_53c7724e:


    "乌里扬卡好像又恢复了她平常的活力。"


translate chinese us_mine_exit_edb2bc43:


    me "不知道是不是很厉害，不过咱们是出来了。"


translate chinese us_mine_exit_c6618fc6:


    us "那么咱们应该继续找舒里克吗？"


translate chinese us_mine_exit_67c5f2d1:


    me "啥米？！"


translate chinese us_mine_exit_e113e81a:


    "我无语了，没有说完这句话。"


translate chinese us_mine_exit_3d6467aa:


    me "你疯了吗？咱们已经找到他了，明天辅导员和警察就会冲进去抓住那个野人，拿他做实验。"


translate chinese us_mine_exit_3dc79f49:


    us "那...!"


translate chinese us_mine_exit_6f3fc9d9:


    me "没有那什么！快回营地去！去睡觉！"


translate chinese us_mine_exit_07bb3c51:


    "我迅速离开那个是非之地，把愤怒的乌里扬卡留在了后边。"


translate chinese us_mine_exit_ce617998_2:


    "..."


translate chinese us_mine_exit_680a7e90:


    "不到十分钟，我们就回到了广场。"


translate chinese us_mine_exit_09a35d39:


    me "好了，今天的行动结束！解散！"


translate chinese us_mine_exit_d8af262d:


    "乌里扬卡干净的的敬了个礼，准备离开，突然她叫了起来，挥着手。"


translate chinese us_mine_exit_700adfc4:


    us "看！看！"


translate chinese us_mine_exit_7047bfac:


    "我转过去，看见舒里克躺在一个长椅上。"


translate chinese us_mine_exit_0f53a61e:


    me "我..."


translate chinese us_mine_exit_11b6866f:


    "叫醒他真不容易，好像这个野人已经冬眠了似的。"


translate chinese us_mine_exit_68fdcd41:


    sh "啊？什么？我在哪儿？"


translate chinese us_mine_exit_5d5d525a:


    "他自言自语着。"


translate chinese us_mine_exit_46b3bd18:


    me "你现在想解释解释吗？"


translate chinese us_mine_exit_613a674f:


    sh "解释什么？"


translate chinese us_mine_exit_8cd1cf37:


    us "我们找了你一个晚上了！然后你就用一根铁棍打谢苗同学！然后就逃逸了！"


translate chinese us_mine_exit_c6875964:


    "乌里扬卡在长椅周围跳来跳去，准备爆炸。"


translate chinese us_mine_exit_8e0eb312:


    sh "发生了什么事？我怎么在这里？"


translate chinese us_mine_exit_6e10baeb:


    "舒里克好像恢复了神智。"


translate chinese us_mine_exit_6891b2b4:


    me "你好好解释一下！你是怎么走出矿井的？你到底为什么要去？一切！"


translate chinese us_mine_exit_517dc919:


    sh "什么矿井？"


translate chinese us_mine_exit_2dc7c7d4:


    "他的眼睛里充满了真诚，让我很怀疑。{w}他可能真的失忆了。"


translate chinese us_mine_exit_70974d60:


    me "你这...12个小时去哪里了？"


translate chinese us_mine_exit_bbe83e3a:


    sh "我不知道..."


translate chinese us_mine_exit_1afdf66f:


    "舒里克皱着眉头回忆着。"


translate chinese us_mine_exit_31d31d38:


    sh "我今天早晨前往旧营地。我可以在那里找到机器人的零件..."


translate chinese us_mine_exit_5d2d6c1c:


    "他不解的看着我们。"


translate chinese us_mine_exit_f95c9fc9:


    me "然后呢？"


translate chinese us_mine_exit_80c68aa7:


    sh "没了..."


translate chinese us_mine_exit_a7156351:


    me "你不记得了？"


translate chinese us_mine_exit_f8cef8d3:


    sh "不记得..."


translate chinese us_mine_exit_855db9ee:


    me "好吧。"


translate chinese us_mine_exit_303044ab:


    "我坐在他旁边，向后靠着长椅。"


translate chinese us_mine_exit_6472a1a6:


    "天上的星星闪亮着。"


translate chinese us_mine_exit_fe87209a:


    "他们什么都记得。{w}连舒里克在防空洞做了什么都记得。"


translate chinese us_mine_exit_4ae29059:


    me "创伤后应激障碍。"


translate chinese us_mine_exit_80f898e3:


    us "创伤什么？"


translate chinese us_mine_exit_470d1a54:


    sh "这种症状发生在人们历经重大压力比如灾难之后。"


translate chinese us_mine_exit_ca0e1330:


    "舒里克变得很有智慧的样子。"


translate chinese us_mine_exit_9f48d48f:


    me "你该睡觉了。"


translate chinese us_mine_exit_035cd0d4:


    sh "是的，但是..."


translate chinese us_mine_exit_7241624a:


    me "咱们明天再说。"


translate chinese us_mine_exit_c2ac9dce:


    "舒里克看着我，不过接下来默默的走了。"


translate chinese us_mine_exit_f2233194:


    us "所以说他怎么回事？"


translate chinese us_mine_exit_f96a54b5:


    me "他可能忘记了在矿井里发生的一切。"


translate chinese us_mine_exit_356011d8:


    us "他在说谎！"


translate chinese us_mine_exit_ad59f734:


    me "可是，他为什么要撒谎呢？"


translate chinese us_mine_exit_d58e2a3f:


    us "那么他就...不用解释...为什么他用铁棍袭击你了..."


translate chinese us_mine_exit_858becb4:


    "她不确定的说道。"


translate chinese us_mine_exit_d8eaac03:


    me "看起来不像啊，而且那件事已经过去了啊。"


translate chinese us_mine_exit_c9dba43d:


    us "没有！咱们要查清楚！罪行必须被惩罚！"


translate chinese us_mine_exit_823f9939:


    me "如果这个对你也适用的话，恐怕你已经进去了，或者更糟..."


translate chinese us_mine_exit_0d7a7ad1:


    us "这和你有什么关系啊？我又不会用铁棍打人！"


translate chinese us_mine_exit_2a2a9dda:


    me "那不是故意的。"


translate chinese us_mine_exit_5ee2ab65:


    us "他在撒谎！"


translate chinese us_mine_exit_7fd78ed0:


    me "他也许在撒谎。"


translate chinese us_mine_exit_3aec35b1:


    "我今天累的要死，我才不想管舒里克有没有说谎什么的..."


translate chinese us_mine_exit_87c100aa:


    "似乎他真的什么都不记得了。"


translate chinese us_mine_exit_93078286:


    me "我要去睡觉了。"


translate chinese us_mine_exit_be15443d:


    us "那么..."


translate chinese us_mine_exit_3b19a06b:


    "乌里扬卡跳起来，踮着脚说。"


translate chinese us_mine_exit_e1a87c65:


    us "晚安！"


translate chinese us_mine_exit_b5cc7e1c:


    me "晚安..."


translate chinese us_mine_exit_3f096259:


    "我不知道，她刚刚的表情好像有些特殊，不管它。"


translate chinese us_mine_exit_0ee0f1a6:


    "辅导员正在“家”等着我呢。"


translate chinese us_mine_exit_523cb2dc:


    mt "我以为你不回来了呢。"


translate chinese us_mine_exit_04cc2a2a:


    "我设想了她的各种可能的反应，没想到是这样的。"


translate chinese us_mine_exit_12012f33:


    mt "因为你是和乌里扬卡一起去的。"


translate chinese us_mine_exit_c4c1debf:


    me "你不应该担心我们吗？"


translate chinese us_mine_exit_40fec304:


    mt "为什么呢？你不是没事吗？"


translate chinese us_mine_exit_d950ea90:


    me "哦，那好吧..."


translate chinese us_mine_exit_adda73b6:


    "我已经没有力气和她争论或者思考她为什么这样了。"


translate chinese us_mine_exit_bc6a86e2:


    "我脱掉衣服爬上床。"


translate chinese us_mine_exit_9f489f27:


    th "对于一个人来说太难了。"


translate chinese us_mine_exit_262beff1:


    "在地下城里寻找舒里克应该—专业消防队员干的事情。{w}和乌里扬卡一起—专业疯子干的事情。"


translate chinese us_mine_exit_54005ff3:


    th "不过怎么说呢，挺有意思的。"


translate chinese us_mine_exit_40413c26:


    "我带着笑容睡着了..."


translate chinese day4_un_867359a2:


    me "我不用自己去那里吧？"


translate chinese day4_un_be1d7816:


    "奥尔加·德米特里耶夫娜想了一会儿。"


translate chinese day4_un_db0f7ce7:


    mt "你说得对，咱们一起去吧，明天。"


translate chinese day4_un_c58ce529:


    "刚才的几分钟我看到列娜奇怪的表情。{w}好像她想要说些什么但又不敢说。"


translate chinese day4_un_08dadf4e:


    "少先队员们开始散开，好像他们已经忘了阿丽夏和这个爆炸事件了。"


translate chinese day4_un_958ec37b:


    "连我们的辅导员也很冷静，在犯罪嫌疑人悄悄离开广场的时候—躲在乌里扬卡的后面。"


translate chinese day4_un_63db6e89:


    mt "咱们也应该走了。"


translate chinese day4_un_9513cd87:


    me "是啊..."


translate chinese day4_un_a5b61259:


    "夜幕很快降临了。"


translate chinese day4_un_feec15e3:


    "从太阳开始下山开始到天色完全变黑，你几乎没有多少时间欣赏多彩的夕阳——也许是因为在这个世界吧。"


translate chinese day4_un_04b50f79:


    "现在睡觉还太早了，不过辅导员信誓旦旦的走向宿舍。"


translate chinese day4_un_90487e63:


    me "奥尔加·德米特里耶夫娜，我出去走走。"


translate chinese day4_un_5cb759d8:


    mt "好吧..."


translate chinese day4_un_ed75d184:


    "她仔细的看着我，不过没有找出什么反对的理由，于是耸耸肩一个人走了。"


translate chinese day4_un_e4483483:


    "我回到了广场。"


translate chinese day4_un_0fb3cb29:


    "我不是很想看Genda雕塑的破坏痕迹，它就在整个营地的中间。{w}如果你不知道该去哪儿的话，就可以从这里开始。"


translate chinese day4_un_df68a624:


    "我坐在长椅上望着西方，思考着这个世界的地球是不是也会绕着太阳旋转。{w}或者说这里就没有东西南北的概念？"


translate chinese day4_un_fbfbafbb:


    "很难说。{w}现在我不太容易检查自然定律。"


translate chinese day4_un_e8479859:


    un "嗨..."


translate chinese day4_un_85361757:


    "列娜不知道从哪里出现在了我的旁边。"


translate chinese day4_un_87696cce:


    me "嗨...睡不着吗？"


translate chinese day4_un_efd76d15:


    "她惊讶的看着我。"


translate chinese day4_un_28e80bef:


    me "啊，是啊，现在还很早..."


translate chinese day4_un_c67f7c91:


    un "我可以坐下吗？"


translate chinese day4_un_33712bb6:


    me "当然，请坐。"


translate chinese day4_un_280592ba:


    "我挪动了一下。"


translate chinese day4_un_cb2ccc7b:


    "说一下不太准确，我实际上挪到了椅子边上。"


translate chinese day4_un_626232a6:


    un "多谢。"


translate chinese day4_un_32c8bf7a:


    "列娜坐下来看着天空，好像已经忘了我一样。"


translate chinese day4_un_9c9cf456:


    un "很悲伤..."


translate chinese day4_un_02347a38:


    me "什么悲伤？"


translate chinese day4_un_e7203d10:


    un "舒里克的消失。"


translate chinese day4_un_8ad48a87:


    me "是啊，情况不妙啊。"


translate chinese day4_un_51dccb02:


    "她还是和平常一样冷静沉默，只有在需要说话的时候才会脸红。"


translate chinese day4_un_94abc2fa:


    "这种可能被常人（包括我）当作是尴尬的沉默对于她来说也许没什么。"


translate chinese day4_un_d45c7222:


    "难以想象列娜会和阿丽夏一样为了找到合适的词语憋得满脸通红。"


translate chinese day4_un_b00fea13:


    "我没有办法把她和其他人作比较，她只是做自己就很满足了。"


translate chinese day4_un_029e699d:


    "这意味着我想要和她聊天的愿望是闯入了她的生活吗？"


translate chinese day4_un_5e028b03:


    "不过这个女孩的身上有什么在吸引着我。"


translate chinese day4_un_0ec5dc2a:


    "也许是她的神秘感，而且她绝对不缺少女性的美。"


translate chinese day4_un_309fa209:


    th "这是为什么我还没有被指出是打扰到了她..."


translate chinese day4_un_703b7283:


    me "我确定会找到他的！你怎么跑出一个潜水艇呢？"


translate chinese day4_un_4f2afd96:


    "列娜好像不太喜欢这个笑话。"


translate chinese day4_un_0b8134cd:


    "这个营地在我看来就是一个大潜水艇。"


translate chinese day4_un_58027c9c:


    un "希望如此。"


translate chinese day4_un_8cd244fa:


    me "明天奥尔加·德米特里耶夫娜会叫来警察，到时候就肯定会找到他的！"


translate chinese day4_un_86342ac5:


    un "万一晚上..."


translate chinese day4_un_e57a9cb4:


    "她的表情变得悲伤。"


translate chinese day4_un_74c7f5fa:


    me "万一出了什么事！"


translate chinese day4_un_c95128dc:


    th "晚上，一个人，在森林里...什么事都有可能发生！"


translate chinese day4_un_765a6a1c:


    un "他一定很孤单。"


translate chinese day4_un_3d219146:


    me "没有人逼他去那里啊！"


translate chinese day4_un_6a910ab9:


    un "万一他迷路了怎么办？"


translate chinese day4_un_2791ad53:


    me "他就不应该自己跑进森林的。"


translate chinese day4_un_63cda4c6:


    un "你对他一点都不同情，舒里克可能一个人孤独的坐在那里..."


translate chinese day4_un_dffcef38:


    me "我当然很同情他..."


translate chinese day4_un_7f15ab29:


    "我感觉很羞耻。"


translate chinese day4_un_eda05f52:


    "不管怎么说，列娜是对的，有一个人失踪了。"


translate chinese day4_un_3b718ed7:


    un "晚上什么都有可能发生..."


translate chinese day4_un_5000641d:


    me "咱们不是真的要去找他吧？"


translate chinese day4_un_fac6df53:


    "她没有回答，还是看着远方，最后一点阳光穿过树林，好像在留给人们最后一点温暖。"


translate chinese day4_un_2807b2ef:


    me "你真的觉得夜里在森林转来转去是一个好主意吗？"


translate chinese day4_un_b354da3c:


    un "也许不是。"


translate chinese day4_un_e49a0145:


    "我觉得她心里就是这么想的。"


translate chinese day4_un_2e282ea3:


    "最近我似乎不用说出来就能理解列娜的想法。{w}她似乎能影响我的心理，让我变得支持她。"


translate chinese day4_un_d9cc43d9:


    "列娜的沉默比任何话语都代表着更多的含义。"


translate chinese day4_un_b181f8ef:


    me "他们在白天的时候已经找过他了。"


translate chinese day4_un_2b8cb61b:


    un "到处都找过了吗？"


translate chinese day4_un_03f5817b:


    "她把视线从夕阳转向我。"


translate chinese day4_un_53cb4078:


    me "不知道，我觉得到处都找过了吧。"


translate chinese day4_un_076d7069:


    un "旧营地呢？"


translate chinese day4_un_3727cf80:


    "这是她第一次听起来十分有自信，而不是模棱两可或者冷漠。"


translate chinese day4_un_f405c42e:


    me "在什么地方，我不知道啊。"


translate chinese day4_un_7ce714f3:


    un "电子小子告诉我们了。"


translate chinese day4_un_ce3fa441:


    me "好吧，如果你信任他的话..."


translate chinese day4_un_55fee8b0:


    "我很蠢的咧着嘴笑着，不过列娜还是严肃的看着我。"


translate chinese day4_un_092d430e:


    me "好吧，如果不是太远的话..."


translate chinese day4_un_c90cc27f:


    un "所以说，你真的想去吗？"


translate chinese day4_un_7bb63055:


    th "我当然不想去！"


translate chinese day4_un_6ee034ca:


    me "咱们可以到那里然后马上回来..."


translate chinese day4_un_b993fafe:


    un "好吧。"


translate chinese day4_un_99baefd5:


    "列娜笑了，递给我一个手电筒——不知道从哪里变出来的。"


translate chinese day4_un_34b3d127:


    me "对啊，这个会很实用的..."


translate chinese day4_un_666cb784:


    th "这是不是说她已经提前准备好了？{w}完全没有参考我的想法？"


translate chinese day4_un_bdea403d:


    "我悲剧的叹了叹气，和她一起前往森林。"


translate chinese day4_un_ce617998:


    "..."


translate chinese day4_un_d6e969ac:


    "夜幕降临了。"


translate chinese day4_un_c621431e:


    "我们慢慢地走着，列娜在我的旁边，很近但没有贴在一起。"


translate chinese day4_un_e8646f8d:


    "这很奇怪，不过她似乎一点也不害怕。"


translate chinese day4_un_6f897bb8:


    th "而且她似乎一点也不在乎我们在做什么，好像我们就在看一场别人演的戏一样。"


translate chinese day4_un_99f8ee9b:


    "实际上，电子小子说过旧营地距离并不远，如果我们一直往那边走的话也不太可能会迷路。"


translate chinese day4_un_f0457e58:


    "不过过了几分钟，我觉得我们走的完全不是直线，又过了一会儿，我开始觉得能走出去就是奇迹了。"


translate chinese day4_un_794417f4:


    "我可不想再列娜面前丢脸，所以我信心满满的走着。"


translate chinese day4_un_67236665:


    "森林十分寂静，闪烁的影子和月光，草丛在脚下沙沙作响，树枝在头上悉悉簌簌的扰动。"


translate chinese day4_un_cf17831d:


    "古老的橡树还有新长出的桦树，还有巨大的蘑菇，像是帽子一样。"


translate chinese day4_un_3805c942:


    "不管在什么别的日子里都会是十分美妙的景色。"


translate chinese day4_un_3e7291b0:


    "也许在晚上也一样安全，不过现在每次有风吹来我就会浑身发抖。"


translate chinese day4_un_d5312815:


    un "看。"


translate chinese day4_un_2b42b39e:


    "列娜指向前方，我看到树林中间的一片空隙。"


translate chinese day4_un_c1f40a14:


    "很快我们就来到了一片空地，空地上有一座旧房子，像是一个乡村学校或是一个幼儿园。"


translate chinese day4_un_a547b60d:


    "墙上的漆斑斑驳驳，房顶上有很多洞，像是一次空袭的结果。破碎的窗户盯着我们，让人毛骨悚然。{w}这个景色不是很舒服。"


translate chinese day4_un_fda5c27f:


    "我不记得刚刚自己是如何在脑海中勾画这里的景色的，不过我一看到实景，头脑中的想法全都消失了。"


translate chinese day4_un_af5a1200:


    me "好瘆人啊..."


translate chinese day4_un_a1b1799c:


    "列娜还是静静的站着，不过她的脸上有一丝自然的恐惧。"


translate chinese day4_un_6e55cb5d:


    un "你觉得他在那里吗？"


translate chinese day4_un_7479b07e:


    me "不知道啊..."


translate chinese day4_un_8ae2c27f:


    "如果我是舒里克，那么躲在一个闹鬼的房子里可是下下策。"


translate chinese day4_un_35698e3a:


    un "可以走了吗？"


translate chinese day4_un_63a2b840:


    "我没有回答出来，月亮又一次出来，将冷冷的色彩投射到地面上。"


translate chinese day4_un_8577fae8:


    "——墓地的颜色。"


translate chinese day4_un_29a44ad3:


    "我可以看清远处的树木，还有环绕的雾气，温度像是瞬间降低了一样，让我感到寒冷。"


translate chinese day4_un_9d47d7cc:


    un "你害怕吗？"


translate chinese day4_un_9191d5fa:


    "列娜冷静的问道。"


translate chinese day4_un_8251b3c4:


    me "说真的？"


translate chinese day4_un_245b3c9a:


    "列娜淡淡的笑着，拉起了我的手。"


translate chinese day4_un_9ddc743b:


    "在平常这可能引起我的情感世界的一场风暴，不过现在确实必须的。"


translate chinese day4_un_763505e7:


    "我们慢慢的往建筑走去。"


translate chinese day4_un_ce617998_1:


    "..."


translate chinese day4_un_679ab4c6:


    "穿过操场的时候，我推了一下旋转木马，让它很邪恶的转了半圈。"


translate chinese day4_un_3bade428:


    "列娜哆嗦了一下，握紧了我的手。"


translate chinese day4_un_1dd28790:


    me "抱歉...我可能只是想起了自己的童年。"


translate chinese day4_un_5a29deb9:


    un "你喜欢旋转木马吗？"


translate chinese day4_un_d61e8c95:


    me "是啊...实际上，我不记得了，可能吧，应该大多数孩子都喜欢吧。"


translate chinese day4_un_ad4edfc8:


    un "我不喜欢。"


translate chinese day4_un_5ac38cf1:


    me "Why?"


translate chinese day4_un_200a724b:


    un "我骑着的时候感觉晕晕的。"


translate chinese day4_un_42576b67:


    me "有可能啦，如果你转得太快的话。"


translate chinese day4_un_1f63bde1:


    un "我更喜欢秋千。"


translate chinese day4_un_417eff12:


    me "你荡秋千的话也是有可能晕的！"


translate chinese day4_un_961f1961:


    un "那是怎么回事呢？"


translate chinese day4_un_7479b07e_1:


    me "我不知道..."


translate chinese day4_un_33548823:


    "这段对话分散了我的注意力，让我不再想着这些事情了：关于我，关于舒里克，我们的夜晚探险，还有列娜..."


translate chinese day4_un_efd8ecf9:


    th "总之，这个世界不是那么奇怪。"


translate chinese day4_un_0a11ff96:


    "最后我们终于来到了门口..."


translate chinese day4_un_b6e2f0dc:


    "旧营地内部让我想起了自己的童年。"


translate chinese day4_un_4b790dca:


    "第一眼看上去连屋里的布局都是一样的。"


translate chinese day4_un_b01a7f13:


    me "舒里克！"


translate chinese day4_un_2554ba11:


    un "舒里克！"


translate chinese day4_un_5498d175:


    "如坟墓一般的寂静围绕着我们。{w}连屋外的风声也安静下来了。"


translate chinese day4_un_833aced3:


    me "看起来这里没有人。"


translate chinese day4_un_6bea7733:


    un "不管怎么说也应该检查一下。"


translate chinese day4_un_0edfb52c:


    "列娜的勇气依旧让我惊讶。"


translate chinese day4_un_a85771c8:


    "或者说，她缺少自我保护意识倒是在意料之中。"


translate chinese day4_un_7163af4a:


    "我不知道女孩的这种行为是不是很奇怪。"


translate chinese day4_un_a70602a7:


    me "好吧，来吧..."


translate chinese day4_un_ce617998_2:


    "..."


translate chinese day4_un_6f0ee1a6:


    "我们仔细检查了旧营地，我甚至还检查了阁楼。"


translate chinese day4_un_587c11d8:


    "有来过人的迹象：报纸，空瓶子，各种垃圾，不过不能确定舒里克来过这里。"


translate chinese day4_un_70fc2e8b:


    "我们回到了开始的地方。"


translate chinese day4_un_ed5bdfd6:


    me "咱们接下来该干什么呢？"


translate chinese day4_un_4657d7ce:


    un "我也不知道..."


translate chinese day4_un_c19ecb61:


    "列娜坐在台阶上看着自己的脚。"


translate chinese day4_un_a8e34a9c:


    me "我觉得咱们应该回去了。..."


translate chinese day4_un_c0d7168c:


    "我谨慎的说着。"


translate chinese day4_un_4ca3177a:


    me "已经很晚了...而且就咱们两个的话恐怕要搜索整个森林有些困难。"


translate chinese day4_un_85add91e:


    un "也许你是对的吧。"


translate chinese day4_un_d3d630bf:


    "她看起来很悲伤，让我感觉这次搜索还没有结束。"


translate chinese day4_un_8aa027b2:


    me "是啊！"


translate chinese day4_un_66287c48:


    "我做了一个离开的手势，然后坐在她的旁边。"


translate chinese day4_un_b2005ce0:


    me "咱们应该想到最坏的结果..."


translate chinese day4_un_98fbbb39:


    un "你是说...?"


translate chinese day4_un_a7f8624d:


    me "不，但是...这附近有没有什么野生动物？"


translate chinese day4_un_999aca00:


    un "我不太同意..."


translate chinese day4_un_65356482:


    "列娜马上就镇定下来了。"


translate chinese day4_un_e4ef2e97:


    me "他可能在哪里睡觉吧，也许他早上醒来就回去了呢！"


translate chinese day4_un_6e34d368:


    un "是啊，当然..."


translate chinese day4_un_064ad673:


    "我站起来，开始来回踱步。"


translate chinese day4_un_9ae0b3a2:


    th "我真的想走开，离开这个森林，不过列娜的态度让我走不开。"


translate chinese day4_un_06d65acf:


    "我正想要继续说服她，不过我注意到了地上的什么东西。"


translate chinese day4_un_3fde514e:


    "是一扇暗门，周围有很少的尘土。"


translate chinese day4_un_00a5fafc:


    th "这一定是最近打开过的！"


translate chinese day4_un_2bed846c:


    me "看。"


translate chinese day4_un_39f5c0fa:


    un "你觉得舒里克在里面吗？"


translate chinese day4_un_1bb4e1f7:


    "列娜蹲了下来，拉动了把手。"


translate chinese day4_un_cdff3e66:


    me "也许不是舒里克，但是最近肯定有人动过这里。"


translate chinese day4_un_cb3016d3:


    "我已经开始后悔发现这个小门了。"


translate chinese day4_un_f533d24e:


    un "我们来检查一下吗？"


translate chinese day4_un_a2335b11:


    "暗门不是很重，只需要轻轻一用力。"


translate chinese day4_un_ef14dccc:


    "我用手电筒照了照，看到了一个梯子通往下方。"


translate chinese day4_un_c80ee062:


    me "看起来像是一个地窖..."


translate chinese day4_un_a7796b9d:


    un "下去吧？"


translate chinese day4_un_3d1667de:


    "我看着列娜，试图看透她在想什么。"


translate chinese day4_un_d42ee41a:


    th "她也想乌里扬卡一样热烈的渴望着冒险吗？{w}她的青年精神哪儿去了？"


translate chinese day4_un_e799e894:


    th "也许她只是发疯了..."


translate chinese day4_un_637e04ca:


    "列娜看起来不像是发疯了..."


translate chinese day4_un_d58dfe0e:


    th "不过谁说过她是人类而且你可以用分析人类的方法分析她呢？"


translate chinese day4_un_dfcf67ae:


    "这种想法让我很害怕，不过我并没有太在意。w}我的数百万的想法中还有更重要的——比如，下面有什么！"


translate chinese day4_un_ce617998_3:


    "..."


translate chinese day4_un_bbdc9393:


    "我爬下去向四周看看。"


translate chinese day4_un_3a575910:


    me "没有问题。"


translate chinese day4_un_5222c78f:


    "在我确定没有什么可害怕的之后，我叫着列娜。"


translate chinese day4_un_fe24c71d:


    "我们站在一条长长的隧道中，这下可以确定不是一个地窖。"


translate chinese day4_un_f3fd57d1:


    "这种建筑风格更像是一个克格勃的地牢，或者是地铁的维修通道，我不知道哪一种情况更好一些。"


translate chinese day4_un_00ed8ed5:


    "墙上有数不清的电线，被每隔一米的卡子固定着。还有很多生锈的灯，混凝土的碎片在我们的脚下嘎吱嘎吱的响着。"


translate chinese day4_un_35698e3a_1:


    un "可以走了吗？"


translate chinese day4_un_c6ff2b5b:


    "列娜没有表情。"


translate chinese day4_un_43695a7a:


    me "去哪儿？那里吗？"


translate chinese day4_un_53b04f6d:


    un "是啊。万一舒里克在那里呢？"


translate chinese day4_un_1a763905:


    me "他在那种地方做什么啊？"


translate chinese day4_un_673ea5b0:


    "我今天无法拒绝她，所以我们一起朝着黑暗迈进。"


translate chinese day4_un_ce617998_4:


    "..."


translate chinese day4_un_0431d71e:


    "列娜在旁边一起走着，握着我的手。"


translate chinese day4_un_5e66f0b6:


    "墓穴中的寂静时常被落下的水滴声打断。"


translate chinese day4_un_cdc7cb75:


    "我们慢慢地向前走，也许太慢了，我有一种幽闭恐惧的感觉。"


translate chinese day4_un_7b3a6c14:


    "我咬咬牙，捏紧了手电筒，不过马上又放松了，害怕捏碎了我们唯一的光源。"


translate chinese day4_un_fa5b94fe:


    "列娜保持着沉默，而这沉默比任何语言都响亮，我开始慌了。"


translate chinese day4_un_3b137961:


    me "说点什么吧。"


translate chinese day4_un_9faa94d8:


    un "门。"


translate chinese day4_un_ad02f53a:


    me "什么？"


translate chinese day4_un_9faa94d8_1:


    un "前面有一扇门。"


translate chinese day4_un_72630fc0:


    "她指着前面。"


translate chinese day4_un_934026f6:


    "我们来到了一个印有生化危机标志的大门前。"


translate chinese day4_un_eb8f4603:


    me "看起来像是一个防空洞..."


translate chinese day4_un_f1f136f3:


    un "对了，我听说过。"


translate chinese day4_un_9e352857:


    me "为什么在这里啊？"


translate chinese day4_un_316a211f:


    un "不清楚，也许是因为古巴导弹危机。"


translate chinese day4_un_9ecf579f:


    me "古巴？"


translate chinese day4_un_41b4cc8d:


    "我想想这个营地的大致建造时间，似乎是有道理的。"


translate chinese day4_un_c02a1e81:


    "在这里挖防空洞就像是在Prostokvashino一样，太浅，而且离文明社会太远。{size=-10}（#Простоквашино：某苏联动画片中一个小村子。）{/size}"


translate chinese day4_un_225dc90a:


    "门把手嘎吱嘎吱的响着。"


translate chinese day4_un_4b931dd1:


    "我决定用力..."


translate chinese day4_un_856f240c:


    "我们似乎来到了房间中的生活区。"


translate chinese day4_un_dfdbb734:


    "这里有床铺，箱子，还有科学设备，做了各种预防核战争的准备。"


translate chinese day4_un_a4794e3a:


    "我们还是没有发现任何舒里克的迹象。"


translate chinese day4_un_d5312815_1:


    un "看。"


translate chinese day4_un_f0153e82:


    "列娜拿着一把信号枪笑着。"


translate chinese day4_un_a1e442ec:


    me "用这个干什么？"


translate chinese day4_un_9a220deb:


    un "打怪兽。"


translate chinese day4_un_e23f4f15:


    me "这里没有怪兽。"


translate chinese day4_un_7c7f5007:


    "至少我想要相信。"


translate chinese day4_un_1318a9ad:


    un "如果你这么说的话..."


translate chinese day4_un_6d9f91ff:


    me "我保证！"


translate chinese day4_un_df288870:


    "我不想让她失望所以把枪别在了腰间，也许有能派上用场的时候..."


translate chinese day4_un_f457cf0f:


    "我们又仔细的搜索了一遍房间。"


translate chinese day4_un_7131cb1c:


    "这里有两个出口。{w} 一个是我们进来的门，另一个是左边完全一样的另一扇门。"


translate chinese day4_un_3dda53ec:


    "我一度感觉很兴奋，想要一口气走完迷宫，看看有什么奖品等着我。"


translate chinese day4_un_ae82ae84:


    "不过这可不是一个电脑游戏，也没有存档功能。"


translate chinese day4_un_8be5d580:


    un "也许可以用这个？"


translate chinese day4_un_634ac583:


    "列娜递给我一把大撬棍。"


translate chinese day4_un_ad83807e:


    me "不，我先不用它试一试。"


translate chinese day4_un_c87458f4:


    "然而大门不合我意，我用了半天劲连一毫米都没有动。"


translate chinese day4_un_901746cb:


    me "好吧，递给我。"


translate chinese day4_un_2ece48fa:


    "用撬棍就太容易了。"


translate chinese day4_un_c8250092:


    "最后这个路障倒了下来，砸在了地上。"


translate chinese day4_un_50d5d9d2:


    "合页完全锈死了。"


translate chinese day4_un_13d39abc:


    "我用手电筒照了照，前面还有一个跟刚刚差不多的隧道。"


translate chinese day4_un_35698e3a_2:


    un "我们走？"


translate chinese day4_un_2f236b34:


    "列娜好像一直在催促我。"


translate chinese day4_un_d3520d70:


    me "你这么着急干什么？"


translate chinese day4_un_338e0f1a:


    un "我？我没有啊..."


translate chinese day4_un_340ec68e:


    "她疑惑的脸红了。"


translate chinese day4_un_e6e2a051:


    th "这...{w}她先是什么都不怕，现在又是说了一个词就不知所措了。"


translate chinese day4_un_2968b0cc:


    me "你好像什么都不怕的样子。"


translate chinese day4_un_1c959e2d:


    un "我不知道，有什么好怕的？"


translate chinese day4_un_aa5f81a3:


    un "反正你会保护我..."


translate chinese day4_un_3f6ffa1d:


    "她用几乎听不到的声音又加了一句..."


translate chinese day4_un_a766ab32:


    "所以说列娜很相信我咯..."


translate chinese day4_un_4bac4a4b:


    th "有可能，很傻，很天真，但是有可能。"


translate chinese day4_un_cb8a29df:


    "我知道我谁也保护不了，连我自己也是一样，这个世界没有什么是听我的。"


translate chinese day4_un_72c7c023:


    "把我带到这里的那种力量什么也干不了！"


translate chinese day4_un_e908fa11:


    "这也不是说前面就是死路一条，因为到处都有可能是死路一条。"


translate chinese day4_un_b749d8c0:


    me "走吧！"


translate chinese day4_un_ce617998_5:


    "..."


translate chinese day4_un_04e4c88e:


    "我试图走快一点，列娜似乎一点也不担心，从容的跟着我。"


translate chinese day4_un_6fa018f9:


    "这条隧道和之前的那条完全一样，每一个细节都一模一样。"


translate chinese day4_un_06e7b9dd:


    "没有什么值得惊讶的地方，不过我总是感觉我们在绕圈圈。"


translate chinese day4_un_b5e87c10:


    "我手中的电筒开始闪烁，光线在墙壁上游走，突然它照亮了一个大洞..."


translate chinese day4_un_787a88e3:


    "洞不是很深，我们可以看到下面有铁路。"


translate chinese day4_un_d5a761a9:


    un "下面是什么？"


translate chinese day4_un_2b1cc4e3:


    me "看起来像一个矿井。"


translate chinese day4_un_f533d24e_1:


    un "我们应该去看看吗？"


translate chinese day4_un_403cf625:


    me "为什么不沿着隧道继续前进呢？"


translate chinese day4_un_13849591:


    un "不知道，我觉得我们应该下去。"


translate chinese day4_un_30068944:


    "我粗测了一下它的高度——应该足够我们爬出来。"


translate chinese day4_un_b6d2f56b:


    me "好吧，咱们瞧一瞧。"


translate chinese day4_un_4b5ac164:


    "我跳了下去，然后帮助列娜下来。"


translate chinese day4_un_540f2604:


    "真的是一个矿井。"


translate chinese day4_un_b1af84b4:


    th "我很奇怪他们在这里能挖什么？"


translate chinese day4_un_37b84aa9:


    me "这片地区有什么矿啊？"


translate chinese day4_un_0da946d4:


    un "不知道。"


translate chinese day4_un_7839cf57:


    me "好吧，好二的问题，现在应该已经没有了..."


translate chinese day4_un_b53b2290:


    "我们走进黑暗之中。"


translate chinese day4_un_a7838803:


    "这地方很难走——我不知道该走什么地方——摇摇晃晃的大木板还是凹凸不平的地面。"


translate chinese day4_un_9cee4921:


    "我也没有办法贴着墙走，窄窄的隧道让我不得不沿着轨道走，但是我又不想放开列娜的手。"


translate chinese day4_un_2c6a97fe:


    "最终我们来到了一个岔路口。"


translate chinese day4_un_f604608d:


    me "真好..."


translate chinese day4_un_7b74056c:


    un "我们应该往哪里走？"


translate chinese day4_un_d2b4c033:


    me "哪里？我都不知道咱们到底能不能走出去，尤其是要玩吃豆人游戏的话..."


translate chinese day4_un_9b63fa7b:


    un "玩什么？"


translate chinese day4_un_a4513cee:


    me "不要在意，咱们会迷路的。"


translate chinese day4_un_f05312d6:


    un "万一那里也有个出口呢？"


translate chinese day4_un_99f6233a:


    me "有可能有...万一没有呢？"


translate chinese day4_un_7749b4db:


    un "那么我们应该回去吗？"


translate chinese day4_un_93f7497e:


    "我咬破了嘴唇，狂喊起来："


translate chinese day4_un_b01a7f13_1:


    me "舒里克！"


translate chinese day4_un_3def6bf8:


    "巨大的回声来回穿梭，甚至震掉了墙上的土。"


translate chinese day4_un_a6afb8d5:


    me "看..."


translate chinese day4_un_d25a0ad8:


    un "那我要自己去了。"


translate chinese day4_un_ad02f53a_1:


    me "啥?!"


translate chinese day4_un_ff99c828:


    "我傻笑着。"


translate chinese day4_un_64e96062:


    me "自己？去哪里？"


translate chinese day4_un_e3e62f26:


    un "我必须找到舒里克，他可能..."


translate chinese day4_un_55c7ffde:


    "列娜红着脸低下了头。"


translate chinese day4_un_6003d47f:


    me "不不不，那可不行，如果要去的话，咱们一起去。"


translate chinese day4_un_3f6ce067:


    un "好吧，那就走吧。"


translate chinese day4_un_08bcf5c5:


    "她笑着拉起了我的手。"


translate chinese day4_un_f6728d01:


    th "她是怎么..."


translate chinese day4_un_c9cc1313:


    me "不过首先咱们应该..."


translate chinese day4_un_d8a01162:


    "我拿起一块石头在支撑的木头柱子上刻了一个叉叉。"


translate chinese day4_un_01e8bf07:


    me "现在咱们就知道是从哪里开始的了。"


translate chinese un_mine_coalface_e40597aa:


    "我们来过这里了。"


translate chinese un_mine_coalface_5b4c3c90:


    "最后我们走出了隧道，来到了一个有高高屋顶的大厅。"


translate chinese un_mine_coalface_3ef8c583:


    "虽然它不太像一个大厅，他们以前一定是在这里挖矿来着。{w}煤矿吧，也许是金矿。"


translate chinese un_mine_coalface_3c54fe69:


    "墙面有十字镐和气钻凿过的痕迹。"


translate chinese un_mine_coalface_65a5fdb8:


    "这个地方一片漆黑，所以我们只能依赖手电筒了。"


translate chinese un_mine_coalface_fadb68bd:


    "如果这里坍塌的话，我们大概是没有机会逃出去的..."


translate chinese un_mine_coalface_58411121:


    "我在光线中发现了一块红布。"


translate chinese un_mine_coalface_ca0a4f90:


    "是一条红领巾！"


translate chinese un_mine_coalface_96b3272c:


    "舒里克显然来过这里。"


translate chinese un_mine_coalface_93ec21f7:


    me "舒里克！舒里克！"


translate chinese un_mine_coalface_ca9e3fa1:


    un "舒里克！舒里克！"


translate chinese un_mine_coalface_b99be3e2:


    "只有回声回答着我们。"


translate chinese un_mine_coalface_c1938ec1:


    un "他应该没有走远，既然我们在这里找到了红领巾。"


translate chinese un_mine_coalface_e80273e8:


    "说实话我很想知道“这里”是哪里..."


translate chinese un_mine_coalface_a411960f:


    th "他经过这里还能去哪儿呢？"


translate chinese un_mine_coalface_686bf61c:


    "这个房间没有出口。"


translate chinese un_mine_coalface_7ea80226:


    th "当然隧道里肯定还有我们没有搜索到的地方..."


translate chinese un_mine_coalface_858b6cbe:


    th "这就是说我们得一直找他！"


translate chinese un_mine_exit_0e2f1383:


    "经过了一个转弯之后，灯光中出现了一扇木门。"


translate chinese un_mine_exit_6585ac5f:


    me "至少有了一点发现。..."


translate chinese un_mine_exit_3875ba7b:


    un "什么?"


translate chinese un_mine_exit_81846d80:


    me "至少不是有一个岔路。"


translate chinese un_mine_exit_97b34350:


    un "里面是什么？"


translate chinese un_mine_exit_75b7c7b4:


    me "咱们只能打开看看咯。"


translate chinese un_mine_exit_1f752f8a:


    "我用力的拉开大门。"


translate chinese un_mine_exit_351ceb93:


    "后面有一个看起来像是储藏室的房间。"


translate chinese un_mine_exit_a8afc189:


    "地上满是烟头和空瓶子，墙上有各种涂写的痕迹。"


translate chinese un_mine_exit_9c847221:


    th "也就是说这边还有一个出口！"


translate chinese un_mine_exit_dd71665d:


    "我不愿意相信以前的人们会是和我们走一样的路来到这里。"


translate chinese un_mine_exit_f89bf0e3:


    "悲剧的是，舒里克不在这里。"


translate chinese un_mine_exit_1cc1d82f:


    me "啊..."


translate chinese un_mine_exit_614dca6b:


    "我靠着墙滑到地上。"


translate chinese un_mine_exit_f5df0746:


    me "现在应该已经去过所有地方了吧。"


translate chinese un_mine_exit_c57bbc05:


    un "不是所有地方。"


translate chinese un_mine_exit_a8a4d8de:


    "列娜指着角落里的一扇门。"


translate chinese un_mine_exit_4b1b3ebe:


    "看起来像是和防空洞一样的大门。"


translate chinese un_mine_exit_7208c8c6:


    me "这里可能像你说的一样有一个出口。"


translate chinese un_mine_exit_35698e3a:


    un "可以走了吗？"


translate chinese un_mine_exit_9f46626d:


    me "让我先歇一会儿。"


translate chinese un_mine_exit_b993fafe:


    un "好的。"


translate chinese un_mine_exit_fd0aed6a:


    "列娜做在我的旁边，很近，然后拉起我的手。"


translate chinese un_mine_exit_6e5e2fd8:


    un "没事的。"


translate chinese un_mine_exit_e54b39e9:


    me "什么意思？"


translate chinese un_mine_exit_840b2721:


    un "我们没有找到舒里克。"


translate chinese un_mine_exit_8f9391d8:


    me "咱们应该想一想自己怎么出去。"


translate chinese un_mine_exit_533890b7:


    un "我们会出去的。"


translate chinese un_mine_exit_f9b93501:


    me "是啊，我大概还记得路。"


translate chinese un_mine_exit_6590db25:


    "至少我觉得自己还记得路。"


translate chinese un_mine_exit_e04c5cfb:


    un "我一点都不害怕。"


translate chinese un_mine_exit_f7f16276:


    "她停顿了一会儿后说。"


translate chinese un_mine_exit_69d18c31:


    me "那很好。"


translate chinese un_mine_exit_d0909642:


    un "因为你在我身边。"


translate chinese un_mine_exit_2f2d5a31:


    "突然，门后边有什么声音。"


translate chinese un_mine_exit_debe8be2:


    "我马上跳起来，寻找着可以当作武器的东西。"


translate chinese un_mine_exit_266bb45a:


    "重重的脚步声越来越近。"


translate chinese un_mine_exit_75fc727b:


    "最后，门打开了，舒里克出现在后面。"


translate chinese un_mine_exit_c305b061:


    "我怔住了，看着他。"


translate chinese un_mine_exit_80c1c240:


    sh "你在这里啊！你觉得你可以躲得过我吗？"


translate chinese un_mine_exit_ad02f53a:


    me "啥?"


translate chinese un_mine_exit_f4b92687:


    sh "你觉得我找不到你吗？你错了！"


translate chinese un_mine_exit_5db31dbf:


    "他心智不正常：他的脸扭曲的笑着，他的眼睛在镜片后面露出凶光，这个失踪的少先队员手中握着一根铁棍。"


translate chinese un_mine_exit_6f664776:


    me "你疯了吗？是我们！"


translate chinese un_mine_exit_2e350af4:


    sh "是啊，我可以看出是你们！"


translate chinese un_mine_exit_2bec6403:


    "他靠近了几步。"


translate chinese un_mine_exit_7bb9ce4b:


    "我本能的挡住了列娜。"


translate chinese un_mine_exit_6cb76a93:


    sh "你觉得你可以把我当成傻瓜吗？“向左，向右，向左，向右”然后我走啊走，走啊走..."


translate chinese un_mine_exit_a94436e5:


    "他举起了铁棍。"


translate chinese un_mine_exit_039ca5ae:


    "之后的一切都像是慢动作镜头一样。"


translate chinese un_mine_exit_7e0cb307:


    "舒里克冲向我们。"


translate chinese un_mine_exit_38d16829:


    "我把列娜推开。"


translate chinese un_mine_exit_61257df0:


    "棍子慢慢的砸向我的头..."


translate chinese un_mine_exit_7276c335:


    "...我手中拿着手电筒，慢慢抬起..."


translate chinese un_mine_exit_fb557852:


    "接下来，漆黑一片，急促的呼吸，我的太阳穴咚咚的跳着。寂静——可怕的，沉重的寂静，深深陷入黑暗之中。"


translate chinese un_mine_exit_ee0f0bdf:


    "我动了动手臂，试图摸到墙壁，我感觉有人碰了我。"


translate chinese un_mine_exit_d7fd3730:


    un "别害怕。"


translate chinese un_mine_exit_2233ca2f:


    "我听到了一个熟悉的声音。"


translate chinese un_mine_exit_79b75990:


    me "你在哪儿，你个疯子？！"


translate chinese un_mine_exit_a5efbe41:


    "我喊着。"


translate chinese un_mine_exit_6a0e7f4b:


    un "他走了。"


translate chinese un_mine_exit_90465eb1:


    me "他从哪里走的？去了哪里？"


translate chinese un_mine_exit_a742d1cc:


    "列娜的声音听起来不是很冷静，不过在这种情况下也是正常的。"


translate chinese un_mine_exit_7606245e:


    un "冷静。"


translate chinese un_mine_exit_73e0f4a4:


    "她温柔的抱着我，贴紧我的身体。"


translate chinese un_mine_exit_e2d3e825:


    "我试着恢复平静，调整自己的呼吸，适应这黑暗。"


translate chinese un_mine_exit_8ef5b76e:


    me "咱们接下来应该做什么？"


translate chinese un_mine_exit_6729a574:


    un "你有一把枪。"


translate chinese un_mine_exit_34ee4713:


    me "嗯？我要打谁呢？"


translate chinese un_mine_exit_dae1cf79:


    un "里面装着照明弹。"


translate chinese un_mine_exit_5b8394c2:


    "也许她是对的。"


translate chinese un_mine_exit_e10fcac5:


    "我把枪从腰间掏出来，对着旁边打了一发。"


translate chinese un_mine_exit_e38eb2e8:


    "整个房间都被红光照亮了。"


translate chinese un_mine_exit_c7063a79:


    "信号弹掉在角落里，看起来像是一个烟花。"


translate chinese un_mine_exit_7fae402f:


    un "咱们快走吧，一会儿就要灭掉了。"


translate chinese un_mine_exit_25f7abbc:


    me "去哪儿？"


translate chinese un_mine_exit_fdecf719:


    "列娜指指第二扇门。"


translate chinese un_mine_exit_6d6ad7e8:


    "这扇门不怎么费力就能打开，然后我们又一次进入了黑暗之中..."


translate chinese un_mine_exit_ce617998:


    "..."


translate chinese un_mine_exit_bf010e6a:


    "随着每一秒钟的流逝，信号弹都变得越来越暗。"


translate chinese un_mine_exit_be25fae0:


    "我加快脚步，虽然摔倒了几次，还是没敢慢下来。"


translate chinese un_mine_exit_9fb2c558:


    th "如果它灭掉的话..."


translate chinese un_mine_exit_c6df834b:


    "最后我们看到了光线，然后跟着它来到了一架梯子面前，房顶上有一个盖子，信号弹嘶嘶的燃尽了。"


translate chinese un_mine_exit_fc8c5efd:


    me "有惊无险..."


translate chinese un_mine_exit_6ebe51f8:


    "原来我们正好在Genda雕塑的下方。"


translate chinese un_mine_exit_b28b9392:


    "这个篦子很结实，不过我把手电筒砸坏了之后设法打开了它。"


translate chinese un_mine_exit_63a68c76:


    "到达地表之后我精疲力尽的倒在了草坪上。"


translate chinese un_mine_exit_729724c7:


    me "真糟糕..."


translate chinese un_mine_exit_772c30bb:


    "列娜坐在我的旁边..."


translate chinese un_mine_exit_f5eeaa60:


    me "好可怕..."


translate chinese un_mine_exit_7b02c40e:


    "现在我已经不在乎这个世界什么的了，还有什么410公交车，或者我以前的生活。"


translate chinese un_mine_exit_0cde231e:


    "最诡异的事情是整个这些事情里居然没有一点超自然的现象。"


translate chinese un_mine_exit_d36716df:


    "舒里克发疯了。{w}没有什么奇怪的，要是我也有可能会发疯..."


translate chinese un_mine_exit_b064b37c:


    "列娜温柔的拍着我的头微笑着。"


translate chinese un_mine_exit_97e317a3:


    un "都结束了呦。"


translate chinese un_mine_exit_b46c791f:


    me "我不知道...现在森林里有一个发疯的少先队员诶！或者说是，杀人狂！"


translate chinese un_mine_exit_dd9e978b:


    un "我觉得他会好起来的。"


translate chinese un_mine_exit_2cdd58d5:


    me "好起来？这我可没有那么确定！"


translate chinese un_mine_exit_7fc6d167:


    un "最重要的是我们没事。"


translate chinese un_mine_exit_89d38d04:


    "列娜仍旧笑着。"


translate chinese un_mine_exit_b923095e:


    me "你怎么能这么镇静？"


translate chinese un_mine_exit_79468419:


    un "我和你说过，和你在一起我什么都不害怕。"


translate chinese un_mine_exit_f739c9b0:


    "是啊，刚才我确实救了列娜还有我自己来着。"


translate chinese un_mine_exit_c066a1fa:


    "不过那只是巧合。"


translate chinese un_mine_exit_507b51cd:


    "如果舒里克动作再快一点...{w}或者更疯狂一点..."


translate chinese un_mine_exit_de6f6646:


    me "谢谢。"


translate chinese un_mine_exit_735a4f9a:


    un "不客气。"


translate chinese un_mine_exit_1f9cc7f6:


    me "虽然咱们当时就应该在这里待着来着。"


translate chinese un_mine_exit_332127e8:


    un "也许你是对的。"


translate chinese un_mine_exit_4bde9263:


    "她平静的说着。"


translate chinese un_mine_exit_e41e554d:


    "我真的很困。{w}因为紧张，疲劳，还有现在不知道多晚了..."


translate chinese un_mine_exit_38af4b55:


    "咱们应该去睡觉了。"


translate chinese un_mine_exit_db493765:


    th "我还得站起来走回去...{w}我还没准备好呢。"


translate chinese un_mine_exit_1dff0f5a:


    "我闭上眼睛歇一会儿..."


translate chinese un_mine_exit_ce617998_1:


    "..."


translate chinese un_mine_exit_43cab7ab:


    "星星从遥远的宇宙中看着我。{w}成千上万颗星星。"


translate chinese un_mine_exit_0f63591e:


    "现在它们的光不再显得那么冷了，相反，它们在向我眨着眼，好像在比赛讲童话故事。"


translate chinese un_mine_exit_46e33d3d:


    "讲着很远很远的星系里紫色绒毛的猪仔，讲着飞船消失的小行星带，还有一个勇敢的船长和他无畏的船员们，还有宇宙边疆的无尽的财宝..."


translate chinese un_mine_exit_318652a2:


    th "我很好奇，这是不是一个梦呢？"


translate chinese un_mine_exit_7c64a5d0:


    "我起来一点，发现自己躺在列娜的膝盖上。"


translate chinese un_mine_exit_4e391c09:


    me "我睡了很长时间吗？"


translate chinese un_mine_exit_11be2cb0:


    "我疑惑的问着，不过没有着急起来。"


translate chinese un_mine_exit_60386181:


    un "不知道，我没有带表。"


translate chinese un_mine_exit_0467a4c4:


    "她笑了起来。"


translate chinese un_mine_exit_240fb495:


    me "大概是多长时间呢？"


translate chinese un_mine_exit_71b8ed88:


    un "嗯，大概是二十分钟吧。"


translate chinese un_mine_exit_aba21a0c:


    me "啊...那好吧。"


translate chinese un_mine_exit_adab8aab:


    "我又躺下来，感受到平静，晚上发生的事情似乎正在变得越来越远，我开始忘记它们，就像是刚刚星星对我讲的故事一样。"


translate chinese un_mine_exit_d5ce5430:


    un "舒里克回来了。"


translate chinese un_mine_exit_3d342d5a:


    me "啥米?!"


translate chinese un_mine_exit_abe0afc6:


    "我一下跳了起来。"


translate chinese un_mine_exit_d4879afa:


    un "他就在那里，躺在长椅上睡觉。"


translate chinese un_mine_exit_44f88c82:


    "列娜指着那边的长椅。"


translate chinese un_mine_exit_63d48677:


    me "你是怎么...?"


translate chinese un_mine_exit_fbd96cba:


    un "你刚刚睡的正香，所以没有叫你起来。"


translate chinese un_mine_exit_502b0fe3:


    "我感觉很后怕，不只是因为这种情况很奇怪，这比在矿井里发疯的舒里克还要可怕。"


translate chinese un_mine_exit_c0778843:


    "一个半个小时以前还试图杀了我的疯子，现在躺在长椅上睡觉，她却只是坐在这里？！"


translate chinese un_mine_exit_f1ca775b:


    un "不用担心，他好像..."


translate chinese un_mine_exit_a948a0c5:


    "列娜红着脸。"


translate chinese un_mine_exit_e06c41ca:


    un "他摇摇晃晃的走着，没有朝我们走来，如果我出声的话..."


translate chinese un_mine_exit_0c721333:


    "她要哭出来了。"


translate chinese un_mine_exit_a8068fc6:


    me "没事，别担心。"


translate chinese un_mine_exit_cfe251db:


    "她说的有道理。{w}也许她做的是对的。"


translate chinese un_mine_exit_ae234167:


    "不管怎么说，我们应该马上审问舒里克。"


translate chinese un_mine_exit_15e2c240:


    "我跳起来，走到他睡觉的长椅前，抽了他一巴掌。"


translate chinese un_mine_exit_82e16e17:


    sh "哎呦！"


translate chinese un_mine_exit_99fd0181:


    "他立刻醒了过来。"


translate chinese un_mine_exit_4372f7aa:


    sh "什么鬼？！"


translate chinese un_mine_exit_215fb22b:


    me "你在干什么，你个混蛋？！"


translate chinese un_mine_exit_13919f7d:


    sh "什么？"


translate chinese un_mine_exit_a83772c6:


    "舒里克害怕的呆呆地看着我们。"


translate chinese un_mine_exit_d21f05af:


    me "刚在在下面是怎么回事啊？！"


translate chinese un_mine_exit_c53304b3:


    sh "什么？在哪儿？"


translate chinese un_mine_exit_55aa43d1:


    me "在隧道里，在矿井里，在防空洞里！你发疯了？"


translate chinese un_mine_exit_6ce75e3c:


    sh "我不明白你在说什么..."


translate chinese un_mine_exit_f1ccf3ee:


    "他看了看周围。"


translate chinese un_mine_exit_5b89be3d:


    sh "我为什么在这里？"


translate chinese un_mine_exit_2b4eca13:


    me "你觉得自己应该在哪儿？对，我知道，你应该在精神病院！"


translate chinese un_mine_exit_1cd4852c:


    sh "我去旧营地找机器人的零件..."


translate chinese un_mine_exit_fc07db1d:


    un "你不记得之后的事情了吗？"


translate chinese un_mine_exit_cf2bfb89:


    "列娜走过来问道。"


translate chinese un_mine_exit_b74741dc:


    sh "对，我..."


translate chinese un_mine_exit_82e63599:


    me "别装了。"


translate chinese un_mine_exit_2be01380:


    "我冷静的坐在他旁边。"


translate chinese un_mine_exit_37d99f1d:


    "看起来他真的不是在撒谎。"


translate chinese un_mine_exit_eb7f67e1:


    sh "我完全不明白，这不科学！！"


translate chinese un_mine_exit_925090f3:


    me "谁在乎啊，别以为你能蒙我。"


translate chinese un_mine_exit_ef21e5d1:


    sh "不能就这样失忆..."


translate chinese un_mine_exit_224d6a7e:


    "舒里克自言自语。"


translate chinese un_mine_exit_a8b5b91a:


    un "咱们走吧。"


translate chinese un_mine_exit_3a1bc23d:


    "列娜悄悄的说道。"


translate chinese un_mine_exit_5b76ac8a:


    me "咱们就把他放在这里吗？"


translate chinese un_mine_exit_5ec8c2d4:


    un "他现在不太正常，他需要休息。"


translate chinese un_mine_exit_34e11933:


    me "把这个疯子留在这里太危险了！他会拿个线圈勒死电子小子的！"


translate chinese un_mine_exit_e672ac80:


    un "不会有事的，相信我。"


translate chinese un_mine_exit_fd384959:


    "我没有不相信她的理由。"


translate chinese un_mine_exit_5b31be1d:


    "然而，我也没有相信她的理由。"


translate chinese un_mine_exit_fbd85565:


    th "再说了，谁还在乎啊，我现在只想马上睡觉！"


translate chinese un_mine_exit_505d91ae:


    me "好吧..."


translate chinese un_mine_exit_19c4dce2:


    "我们扔下还在自言自语的舒里克。"


translate chinese un_mine_exit_ce617998_2:


    "..."


translate chinese un_mine_exit_cd10cb1d:


    un "我们到了。"


translate chinese un_mine_exit_1d9b8f65:


    me "什么？到哪儿？"


translate chinese un_mine_exit_41a8b963:


    un "我还要继续走。"


translate chinese un_mine_exit_561828ec:


    "列娜笑了。"


translate chinese un_mine_exit_ac77630c:


    "我一路上什么也没有想，只是默默的跟着她，没有发现我们已经来到了奥尔加·德米特里耶夫娜的门前了。"


translate chinese un_mine_exit_9513cd87:


    me "是啊..."


translate chinese un_mine_exit_8cf9c83d:


    un "今天...谢谢你了。"


translate chinese un_mine_exit_6c156bd2:


    me "没有什么好谢的，咱们能活着回来就好，看看明天舒里克要说什么吧！"


translate chinese un_mine_exit_05ed2d0a:


    un "总之谢谢了..."


translate chinese un_mine_exit_9e3e0847:


    "她神秘的说着，避开了我的目光。"


translate chinese un_mine_exit_e88d2104:


    me "不用谢。"


translate chinese un_mine_exit_6ecba0e3:


    un "好了，我该走了！"


translate chinese un_mine_exit_6b46b235:


    "列娜迅速转过身，朝自己的房间走去。"


translate chinese un_mine_exit_692879ab:


    "我突然觉得事情有些不对劲。"


translate chinese un_mine_exit_3b0e393f:


    th "经过了那么多事情之后，只是“我该走了”吗？"


translate chinese un_mine_exit_864250dc:


    th "这种场合一般都会有不同的台词吧？"


translate chinese un_mine_exit_1a76d291:


    "不过我也不知道自己在期待着什么..."


translate chinese un_mine_exit_90db22de:


    "疲倦再次袭来，我费力的走进宿舍。"


translate chinese un_mine_exit_9dc23163:


    "辅导员正坐在床上..."


translate chinese un_mine_exit_65d8d90c:


    mt "谢苗..."


translate chinese un_mine_exit_5c8a950f:


    "她开始悲伤的说着。"


translate chinese un_mine_exit_3300a99a:


    mt "你去哪里了？"


translate chinese un_mine_exit_4e8fd4d6:


    me "这个...我..."


translate chinese un_mine_exit_18329aed:


    "我准备好迎接一顿责骂，可没有想到这种事情。"


translate chinese un_mine_exit_8644ddd6:


    me "我去找舒里克了。"


translate chinese un_mine_exit_b166a941:


    mt "自己吗?"


translate chinese un_mine_exit_57cf671c:


    me "不，和列娜一起。"


translate chinese un_mine_exit_46b3ba5a:


    me "是啊，一个人。"


translate chinese un_mine_exit_49d63af4:


    mt "那么...{w}舒里克怎么样了？"


translate chinese un_mine_exit_786bc7ef:


    "奥尔加·德米特里耶夫娜好像真的很担心这个失踪的少先队员的命运。"


translate chinese un_mine_exit_c8bc02e9:


    th "这有什么奇怪的？"


translate chinese un_mine_exit_b9bcf031:


    th "辅导员可能处理工作比较草率，不过这并不意味着她是一个无情的人。"


translate chinese un_mine_exit_111b3046:


    me "他还好...{w}正符合在这种情况下的预期。"


translate chinese un_mine_exit_564980ed:


    mt "那就好。{w}去睡觉吧。"


translate chinese un_mine_exit_47b3e3c3:


    me "等一下..."


translate chinese un_mine_exit_29dbd358:


    "她迅速关掉了灯，让我无法继续问下去。"


translate chinese un_mine_exit_9a3906dd:


    th "我应该问吗？"


translate chinese un_mine_exit_f504d3d8:


    th "而且她为什么没有责备我？"


translate chinese un_mine_exit_a20cefa7:


    "..."


translate chinese un_mine_exit_a129a5fd:


    th "而且，列娜..."


translate chinese un_mine_exit_c654370d:


    th "今天发生了什么？"


translate chinese un_mine_exit_c01dd812:


    "我的思绪放慢了脚步..."
# Decompiled by unrpyc: https://github.com/CensoredUsername/unrpyc
