
translate chinese day2_main1_5a2ae8b2:


    "我做了个梦..."


translate chinese day2_main1_fc07b71b:


    "我好像在真空中，周围什么都没有。"


translate chinese day2_main1_895753c3:


    "不只是{i}周围{/i}：我是整个宇宙里唯一一个生物。"


translate chinese day2_main1_0ceef837:


    "好像宇宙回到了大爆炸之前的奇点。"


translate chinese day2_main1_e5affe53:


    "然后马上有什么事情要发生。"


translate chinese day2_main1_1409c490:


    "突然我听到了一个声音。"


translate chinese day2_main1_7451916c:


    "我辨认不清，但是它的感觉很熟悉。"


translate chinese day2_main1_30fbd9d9:


    "那个声音在轻轻地和我说些什么，好像在抚慰着我。"


translate chinese day2_main1_72c99b77:


    "然后我意识到...{w}那是公交车上的奇怪女孩，{w}梦里的那个女孩。"


translate chinese day2_main1_08be4530:


    th "可是她到底想和我说什么？她是谁？"


translate chinese day2_main1_4b4e1333:


    "我醒了。"


translate chinese day2_main1_43e6696c:


    "阳光刺激着我的双眼。"


translate chinese day2_main1_ddaaef10:


    "已经快到中午了。"


translate chinese day2_main1_88d071f4:


    "在床上懒懒的伸个懒腰，打个呵欠，我开始回忆前一天。"


translate chinese day2_main1_062f89ad:


    "几秒钟之内，很多事情从眼前闪过：公共汽车，夏令营，这里的人们。"


translate chinese day2_main1_32bb3fe1:


    th "这可不对。"


translate chinese day2_main1_efb38133:


    "不是现在的处境，不是我在这里的事实—那已经是无法改变的了—而是我对这件事的态度不对了。"


translate chinese day2_main1_7ac9eb97:


    th "我昨天就那样子睡了，还和这里的少先队员们愉快的交谈？甚至还讲了几个笑话？"


translate chinese day2_main1_31cacf90:


    th "我在这种情况下怎么能干得出来？"


translate chinese day2_main1_9f97589a:


    th "我应该是被吓得听到一点细小的声音都会颤抖，并且避免和这些有危险可能性的生物过多接触。"


translate chinese day2_main1_321177f9:


    "昨天的事变得有点模糊了，像是宿醉一样。"


translate chinese day2_main1_0b66f5c8:


    th "这确实像是一个聚会之后的早晨，昨天看起来十分优雅的一切，今天都变成了如同《神曲》一样的鬼畜的画面。"


translate chinese day2_main1_2c6f5607:


    th "不过事情就是这个样子了。"


translate chinese day2_main1_7756bdbc:


    "我当时应该也是审时度势做出了正确的选择吧。"


translate chinese day2_main1_b93eac53:


    "我到处张望，观察自己是不是又被调包到别的地方了，不过这次我还是在这个房间里没有什么变化。"


translate chinese day2_main1_63937111:


    "没有什么特别的事，除了床头的一件制服。"


translate chinese day2_main1_3b552dd7:


    "我试探着折腾着穿上了这件衣服。"


translate chinese day2_main1_d906fb96:


    th "这总比穿着冬装走来走去的好。"


translate chinese day2_main1_207c3b3d:


    th "真想看看自己现在是个什么德行，肯定像个小丑一样吧。"


translate chinese day2_main1_01922da1:


    "我需要一个镜子，{w}再小也得有个镜子。"


translate chinese day2_main1_c8ca898d:


    "我在衣柜上找到了一面镜子。"


translate chinese day2_main1_9fe62ef5:


    me "我勒个..."


translate chinese day2_main1_3e51bea7:


    "我看到了一个没见过的少先队员，吓得后退了一大步。"


translate chinese day2_main1_a7207d27:


    "镜子那一边有一个少先队员！"


translate chinese day2_main1_a5cf122d:


    "他看起来像我，但是不是我！"


translate chinese day2_main1_50576b33:


    "这周的胡茬哪儿去了？我的眼袋，我的萎靡的、疲倦的面部表情去哪儿了？！"


translate chinese day2_main1_b7edc738:


    "看来我不是进入了平行宇宙或者是进行了时间旅行，而是和某个人的意识互换了。"


translate chinese day2_main1_977f3766:


    th "啊，对呀！{w}这种事可是会每天发生的！"


translate chinese day2_main1_7c803d3e:


    "我又离近了仔细观察了半天，结果发现，这个人就是我！"


translate chinese day2_main1_473252f1:


    "只是不是现在的我，而是我在高中或是大学时的样子。"


translate chinese day2_main1_bc978da0:


    th "好吧，这倒是一点发现。"


translate chinese day2_main1_1e928d66:


    th "啊，对呀，{i}人在极端的情况下{/i}确实会忽略掉一个‘房间中的大象。’"


translate chinese day2_main1_78884d1e:


    th "不过辅导员倒是注意到了，昨天还提醒我要用正确的称谓..."


translate chinese day2_main1_e3987898:


    th "啊啊，见鬼去吧！"


translate chinese day2_main1_cbe4d96c:


    th "我怀疑我的外表会不会造成什么影响。"


translate chinese day2_main1_c20955fe:


    "如果钟表没有在说谎的话，早饭早就结束了。"


translate chinese day2_main1_07623b09:


    th "好吧，我会想办法从食堂里弄点吃的。"


translate chinese day2_main1_b1e4da72:


    th "昨天和斯拉维娅不就成功了吗？"


translate chinese day2_main1_0a24a340:


    "这段回忆让我不禁笑了起来。"


translate chinese day2_main1_86af3570:


    "户外的阳光很足，有一点微风。"


translate chinese day2_main1_a7b6773f:


    th "一个美丽的夏日。"


translate chinese day2_main1_d9a6ffae:


    "我已经好几年没有经历过这么舒服的早晨了。"


translate chinese day2_main1_6597a8e7:


    "所有的烦恼都烟消云散了。"


translate chinese day2_main1_be95a8d4:


    "奥尔加·德米特里耶夫娜不知道从哪里出现在了我的面前。"


translate chinese day2_main1_b8d13a2a:


    mt "早上好，谢苗！"


translate chinese day2_main1_32f6d96b:


    me "早上好！"


translate chinese day2_main1_eda54ed8:


    "至少我的早晨是确实不错的，于是我也露出微笑。"


translate chinese day2_main1_53e06c95:


    mt "你昨天才刚刚到，所以我没有叫醒你，不过早饭..."


translate chinese day2_main1_d1895f30:


    mt "没关系，来，拿着这个。"


translate chinese day2_main1_71894711:


    "她递给我纸里包着什么。"


translate chinese day2_main1_db4f4b37:


    "从油渍看上去，里边应该是个三明治之类的。"


translate chinese day2_main1_8aa9a83a:


    me "哇，谢谢。"


translate chinese day2_main1_641d577e:


    mt "去洗洗脸吧！"


translate chinese day2_main1_385627a7:


    "I我准备离开。"


translate chinese day2_main1_22a42eb2:


    mt "等一下。"


translate chinese day2_main1_66afc59e:


    "奥尔加·德米特里耶夫娜冲进房间拿出来一个小包塞给我。"


translate chinese day2_main1_9c319d40:


    "我看到里面有牙刷、香皂、毛巾什么的，没细看。"


translate chinese day2_main1_29289d61:


    mt "少先队员应该保持自己仪表整洁。"


translate chinese day2_main1_1d1b2ba8:


    mt "我来帮你系红领巾，你没有摆正，{w}学会之后你就要自己做了。"


translate chinese day2_main1_66e9a1e6:


    me "一定要这样吗，我正准备洗漱诶..."


translate chinese day2_main1_4a476f09:


    th "是啊，它有可能夹在水龙头上然后勒死我..."


translate chinese day2_main1_e511e399:


    mt "好吧，那就等一会儿。{w}不要忘了到了时间去集合。"


translate chinese day2_main1_8eba6c7d:


    th "集合？然后大家在一面墙前排排站..."


translate chinese day2_main1_a25f95a7:


    me "什么集合？"


translate chinese day2_main1_41981848:


    mt "什么‘什么集合’？！"


translate chinese day2_main1_823e3ce8:


    "她皱起了眉头。"


translate chinese day2_main1_ebb2ca84:


    mt "今天是星期一。"


translate chinese day2_main1_5afc6ea3:


    th "奇怪，我如果没记错的话应该是周日才对..."


translate chinese day2_main1_769920ed:


    th "嘛~~我已经无所谓了..."


translate chinese day2_main1_a24cdacb:


    mt "平常我们要在早饭之前集合，不过今天是星期一，我们要在12点集合。"


translate chinese day2_main1_a89a2c76:


    mt "别迟到！"


translate chinese day2_main1_0de6c041:


    me "好吧，可是在哪里呢？"


translate chinese day2_main1_a56c3145:


    mt "在广场啊，还能是哪里？"


translate chinese day2_main1_49afcaa3:


    "现在不应该吵架。"


translate chinese day2_main1_7ca9038d:


    "我前往‘水房’。"


translate chinese day2_main1_909fa19e:


    "没有独立的卫生间什么的我还可以忍，可是这各种故障的苏联老水龙头实在是不能忍。"


translate chinese day2_main1_fc6f68e6:


    "我不是个斤斤计较的人，不过站在这里我才明白了这些道理。"


translate chinese day2_main1_201b58e0:


    "有些生活中的小东西平常不在意，需要的时候没有才是很烦恼的。"


translate chinese day2_main1_fc32f6b3:


    th "啊，不想了！好像我还有其他选择似的。"


translate chinese day2_main1_e5cc3b97:


    "水很冰手。"


translate chinese day2_main1_7a2b0aea:


    "洗洗手还行，可是其他的项目好像面临着一些问题。"


translate chinese day2_main1_9d2484de:


    "奥尔加·德米特里耶夫娜给我的包里好像没有牙膏。"


translate chinese day2_main1_13ab8033:


    "不用其实也可以，不过毛巾里包着一个小盒。"


translate chinese day2_main1_331eedf5:


    "‘牙粉’。"


translate chinese day2_main1_60d90a2e:


    th "厉害，有一种怀旧的感觉。"


translate chinese day2_main1_43dc525a:


    "我很快就洗漱完毕，这要感谢刺骨的凉水。"


translate chinese day2_main1_86c7f6e3:


    "有人往这边跑过来。"


translate chinese day2_main1_47eaf14e:


    "我转过身。"


translate chinese day2_main1_644b2c05:


    "是斯拉维娅，她穿着一身体操服。"


translate chinese day2_main1_2984b340:


    "这个女孩儿真是穿什么都好看呀：少先队制服、泳装、甚至是宇航服。"


translate chinese day2_main1_396d52a7:


    sl "你好呀！"


translate chinese day2_main1_65d811ca:


    me "偶哈呦...不对，砸上好...早上好！对了..."


translate chinese day2_main1_12f788c2:


    "好流畅的对话。"


translate chinese day2_main1_0ec1deba:


    sl "你怎么没来吃早饭？"


translate chinese day2_main1_9ac34b39:


    me "我睡过了。"


translate chinese day2_main1_a512e5ef:


    "我自豪地说道。"


translate chinese day2_main1_531482a7:


    me "不过奥尔加·德米特里耶夫娜给了我一个三明治。"


translate chinese day2_main1_e3b70370:


    sl "噢，那就好。别忘了去集合！"


translate chinese day2_main1_85279b68:


    me "啊，当然。"


translate chinese day2_main1_ea0e94f5:


    th "好像我会忘了似的。"


translate chinese day2_main1_09688a75:


    sl "不错，我得走了，你好好玩！"


translate chinese day2_main1_06aca64d:


    "她挥手告别然后消失在了小路的转角处。"


translate chinese day2_main1_4ef9a3ba:


    th "看起来还有几分钟就要集合了。"


translate chinese day2_main1_a1bcef4b:


    "我赶快回‘家’放好我的洗漱包包同时吃掉三明治，然后就前往广场。"


translate chinese day2_main1_8fc4fbf5:


    "我一把推开房间大门冲了进去，就像是赶上末班车一样。"


translate chinese day2_main1_8f99475f:


    "我发现这可不是个好主意，因为我看见奥尔加·德米特里耶夫娜在里边..."


translate chinese day2_main1_361b5648:


    "换衣服！"


translate chinese day2_main1_989f5b30:


    "我吓的怔住了，不敢喘气。"


translate chinese day2_main1_6f55abad:


    "辅导员发现了我。"


translate chinese day2_main1_105fc865:


    mt "谢苗！"


translate chinese day2_main1_4d49e3f9:


    "我赶紧往边上看。"


translate chinese day2_main1_c2b62a94:


    mt "没人教过你要敲门吗？出去！"


translate chinese day2_main1_118d6aad:


    th "哎呀，刚才好尴尬。"


translate chinese day2_main1_fa866f87:


    "虽然我很欣赏刚才的风景。"


translate chinese day2_main1_ee01ae30:


    "奥尔加·德米特里耶夫娜很快就跟出来了。"


translate chinese day2_main1_f47b36ab:


    mt "拿着这个，{w}现在它也是你的家了。"


translate chinese day2_main1_c3639b5f:


    "她递给我一把钥匙，{w}我揣到了口袋里。"


translate chinese day2_main1_fcf7f167:


    th "家…"


translate chinese day2_main1_c9d7a8ab:


    th "如果不去想那些灵异事件的话，这个地方倒不是那么可怕，不过要说是家的话..."


translate chinese day2_main1_8bb6d04f:


    th "还是刚来的第二天！"


translate chinese day2_main1_7ab9cde3:


    th "我怀疑会不会有那么一天。"


translate chinese day2_main1_cd306d7c:


    mt "好了，咱们走吧，已经晚了。"


translate chinese day2_main1_dc3be649:


    me "那三明治呢？"


translate chinese day2_main1_a350f4cb:


    mt "路上吃！"


translate chinese day2_main1_9898e071:


    "我们沿着一排小屋前进，我不停地吃，奥尔加·德米特里耶夫娜不停地说，{w}她的嘴巴像抽风一样停不下来..."


translate chinese day2_main1_90484daa:


    "不过我除了吃的什么也不在意。"


translate chinese day2_main1_38e0aff0:


    mt "明白？"


translate chinese day2_main1_b6626098:


    me "哈？"


translate chinese day2_main1_3c5ea71b:


    mt "你根本就没听我说话嘛！"


translate chinese day2_main1_559ab9c6:


    me "对不起…"


translate chinese day2_main1_ff1825ac:


    mt "今天是你开始少先队员生活的第一天！"


translate chinese day2_main1_b2cb81b3:


    mt "你应该努力的去生活！"


translate chinese day2_main1_88b4ce94:


    me "啊，对呀，当然了..."


translate chinese day2_main1_76079f83:


    mt "我很严肃的，少先队员有自己的使命，有很多的责任需要担当：参加社会劳动，帮助低年级的孩子，还有不断学习进取！"


translate chinese day2_main1_7f4c0bdf:


    mt "我们在这里就像是一个大家庭。{w}你也是其中之一。"


translate chinese day2_main1_3f18fd02:


    th "对，之一...{w}要我入党都可以，只要可以不再听这无聊的话。"


translate chinese day2_main1_126fe430:


    mt "我希望在夏令营结束之后，你能留下一份美好的回忆。"


translate chinese day2_main1_c17a76a5:


    mt "一生都会记得的回忆。"


translate chinese day2_main1_9b154f85:


    me "那到底是什么时候结束啊？"


translate chinese day2_main1_3b76cadb:


    mt "你怎么总是问一些笨蛋的问题啊。"


translate chinese day2_main1_adbfd80d:


    th "看来我从她嘴里什么情报也套不出来。"


translate chinese day2_main1_c6e6c275:


    "好遗憾。{w}这个世界看起来挺友好的，不过它从来没有向我介绍{i}它自己{/i}。"


translate chinese day2_main1_edbe22f5:


    th "也许现在我可以不像昨天那么在意了？"


translate chinese day2_main1_cc8bfc43:


    "好像我们之间有个不成文的规定{i}它{/i}不打算伤害我，可是不允许我问问题。"


translate chinese day2_main1_b692bf4b:


    "当然这样的情况还是会令人不爽，可我能有什么办法呢？{w}糟糕的和平也要胜过崇高的斗争。"


translate chinese day2_main1_bfb454f3:


    mt "对你来说最重要的是好好利用在这里度过的时间。"


translate chinese day2_main1_8e4b3ee3:


    me "我尽量。"


translate chinese day2_main1_760779dc:


    "说实话，这对话让我很不耐烦。"


translate chinese day2_main1_cdb44710:


    "我想知道‘这里’是哪里，{w}可..."


translate chinese day2_main1_5fdbda0d:


    "我们来到了广场。"


translate chinese day2_main1_bf8d48ab:


    "少先队员们已经集合起来了。"


translate chinese day2_main1_69eaae74:


    me "还有人没到吗？"


translate chinese day2_main1_d5cf567b:


    mt "嗯，全部到齐了。"


translate chinese day2_main1_f65725c8:


    "她环视勇敢的少先队员们。"


translate chinese day2_main1_e4178ce3:


    mt "去找个地方站着。"


translate chinese day2_main1_2240ebeb:


    th "奇怪。{w}为什么她跟我说没有空余的房间了？"


translate chinese day2_main1_73025a75:


    "老大在宣布这个星期的各种计划的时候我在观察队伍中的各色人等。"


translate chinese day2_main1_6af3ef8e:


    "不远处就是电子小子，再远一点是列娜和斯拉维娅，队尾是乌里扬卡和阿丽夏。"


translate chinese day2_main1_80439bd7:


    th "我见到过的人都在这里。"


translate chinese day2_main1_d2e34ec7:


    "奥尔加·德米特里耶夫娜说到了什么比赛，我则把注意力转移到了雕塑上。"


translate chinese day2_main1_0c15405c:


    th "'Genda'..."


translate chinese day2_main1_89f74fc0:


    "我想不起来哪个革命先驱叫这个名字..."


translate chinese day2_main1_49c36f8b:


    "他的姿势也很奇怪，好像是有一种怀疑的态度，也许是有点不屑，甚至是鄙视的。"


translate chinese day2_main1_ae1cb199:


    th "大概是某个本地的领导吧…"


translate chinese day2_main1_b6ab0cdc:


    sl "又在做白日梦了吗？"


translate chinese day2_main1_cc0015de:


    "斯拉维娅把我拉回了现实。"


translate chinese day2_main1_0ceaa470:


    "奥尔加·德米特里耶夫娜站在跟前。"


translate chinese day2_main1_31af9f80:


    mt "还记得这周的计划吗？"


translate chinese day2_main1_b54906d2:


    me "计划？{w}我不可能会忘的嘛！"


translate chinese day2_main1_003878d1:


    mt "很好！"


translate chinese day2_main1_a7925aca:


    "她看向斯拉维娅。"


translate chinese day2_main1_0e306996:


    mt "你带来了吗？"


translate chinese day2_main1_8b01f8be:


    sl "嗯。"


translate chinese day2_main1_b8640c11:


    "斯拉维娅递给我一张纸片。"


translate chinese day2_main1_5c374a56:


    mt "这是一个清单，每天有四项任务需要完成。"


translate chinese day2_main1_ffebe5e8:


    mt "你在开始之前先选择一个社团，社团室里有几个社团，音乐社团在单独的建筑里。"


translate chinese day2_main1_76437f4d:


    mt "然后，去拜访医务室。"


translate chinese day2_main1_1d88b658:


    mt "最后是图书馆。"


translate chinese day2_main1_f2150eaa:


    mt "记住了？"


translate chinese day2_main1_fc30fd2b:


    me "是的。"


translate chinese day2_main1_a43db633:


    "完成这些人物是个了解这里的好机会，上面列了很多我还没去的地方。"


translate chinese day2_main1_d0722a98:


    mt "那现在就去吧。"


translate chinese day2_main1_abacb833:


    me "午饭怎么办呀？"


translate chinese day2_main1_794c528c:


    mt "不要管那个，我会带来更多三明治的，你干好你的本职工作！"


translate chinese day2_main1_1ef3d9f1:


    sl "祝你好运。"


translate chinese day2_main1_167e9f8c:


    "我还来不及追问他们就都走了。"


translate chinese day2_main1_6866c9c8:


    th "错过了早饭，这下还要再错过午饭。"


translate chinese day2_main1_ce0b356d:


    th "这可不好。"


translate chinese day2_main1_d2a48ffa:


    th "也许我可以搞定？"


translate chinese day2_main1_bb3dc1b6:


    th "午饭下午1点开始，{w}我要是去那里的话就得少去一个地方。"


translate chinese day2_main1_7a51eb9a:


    th "总之去食堂还是太早了！"


translate chinese day2_musclub_5ea37963:


    "音乐社团，是一个较小的单层建筑，和营地其他的房子距离较远。"


translate chinese day2_musclub_69443bca:


    "我打开房门，毫不犹豫的走了进去。"


translate chinese day2_musclub_8cb5bcfe:


    "这里有乐队需要的各种乐器：鼓、吉它还有钢琴什么的。"


translate chinese day2_musclub_aa3ba673:


    "我仔细观察了每个乐器，想要猜一猜它们都是出自哪个年代的，不过突然听到钢琴底下有人在爬的声音。"


translate chinese day2_musclub_babe6137:


    th "一个女孩儿，好像在找什么。"


translate chinese day2_musclub_baf9ce11:


    "她趴在地上好像很有暗示意义的动作让我觉得是不是不应该先开口说话。"


translate chinese day2_musclub_559ab9c6:


    me "抱歉…"


translate chinese day2_musclub_0fe0c850:


    mip "呀啊啊，是谁？"


translate chinese day2_musclub_4391282c:


    "她尝试站起来，却被钢琴挡在了底下。"


translate chinese day2_musclub_250ae924:


    mip "呜..."


translate chinese day2_musclub_645f668d:


    "她总算折腾了出来。"


translate chinese day2_musclub_cc2dd0eb:


    me "对不起，我吓到了你…"


translate chinese day2_musclub_021d4634:


    mip "没关系！哇，你有一个清单，那一定是新来的咯？"


translate chinese day2_musclub_ec29382e:


    me "哈？嗯..."


translate chinese day2_musclub_46ce9eb5:


    mip "我叫作未来。"


translate chinese day2_musclub_2f3d5d1f:


    mi "不，说真的，总是没人相信我，不过这是我的真名呦！我妈妈是日本人，我的爸爸在盖房子的时候遇到…嗯...不是他在盖啦，他是一个工程师…"


translate chinese day2_musclub_9fae511d:


    mi "他在建造一个核电站，或者是一座大坝！一座大桥！或者是不知道什么东西！"


translate chinese day2_musclub_222c3455:


    "她说话超快，一半的音都听不清了。"


translate chinese day2_musclub_ec7942bc:


    me "我叫谢苗。"


translate chinese day2_musclub_d2c6efe6:


    mi "太好了！你想要加入我们的社团吗？现在只有我一个人，不过你来了就是两个人了！你想要玩点什么吗？"


translate chinese day2_musclub_00ccccca:


    "我开始变得不合群的时候买了一把吉它，还练了几个和弦。不过我很快就忘了，自从所有几个小时学不会的东西我都不再学了之后。"


translate chinese day2_musclub_dd80a4a7:


    me "这个，我还没有那样的打算…"


translate chinese day2_musclub_946e96fa:


    mi "没关系的，我会教你。比如说小号，或者是小提琴怎么样？我都玩的很好喔~"


translate chinese day2_musclub_b2395a19:


    "现在和这个乐队女孩争吵可是没有意义的，那只会带来另一段连珠炮。"


translate chinese day2_musclub_3783ff78:


    me "嘿，我会考虑的，那你能不能现在帮我签个字呢？"


translate chinese day2_musclub_b5f95324:


    mi "嗯，嗯，好的，递给我吧，不过一定要来呦~我也会唱歌的说~我可以给你唱一些日本民歌，如果你不喜欢的话也可以唱一些现代的呦~"


translate chinese day2_musclub_1c12d36f:


    me "啊…嗯。多谢，我得走了！"


translate chinese day2_musclub_045fe4ca:


    mi "好的，随时都可以来找我呦~…"


translate chinese day2_musclub_f2b7b54b:


    "那句话没说完的部分留在了房间里。"


translate chinese day2_musclub_2f7d7510:


    "晚上拿一把吉它玩玩还不错，不过有这么个人陪着…"


translate chinese day2_musclub_2c9e7ab2:


    "我转身离开结果正和阿丽夏面对面。"


translate chinese day2_musclub_db45ba49:


    "她用充满怀疑的眼神看着我。"


translate chinese day2_musclub_5c36e901:


    dv "你怎么来了？"


translate chinese day2_musclub_77321a84:


    me "因为那个清单…"


translate chinese day2_musclub_f2265658:


    dv "你找到人签字了？"


translate chinese day2_musclub_9513cd87:


    me "是啊…"


translate chinese day2_musclub_6eb94c13:


    dv "那赶紧走！"


translate chinese day2_musclub_d8a05e0a:


    "阿丽夏走了进去，我也急忙离开了这个地方。"


translate chinese day2_clubs_fe574fd8:


    "我去了社团室。"


translate chinese day2_clubs_a3b49ee7:


    "说实话我一直都不大喜欢课外活动。"


translate chinese day2_clubs_352ba995:


    "我在学校的时候总是想尽办法翘掉额外的课程，在大学的时候也对学生会没什么兴趣。"


translate chinese day2_clubs_2acddae0:


    "拳击、模型、或者是针线活，哪个类型的我都不喜欢。"


translate chinese day2_clubs_edf42e23:


    "所以我来这里只是想完成任务。"


translate chinese day2_clubs_7631c2b8:


    "这里没人。"


translate chinese day2_clubs_68cc9679:


    "这里好像是个机器人爱好者俱乐部：有好多电线和电路板散布在各处，桌子上还摆着个示波器。"


translate chinese day2_clubs_58c977db:


    "另外一间屋子里有点动静，然后两个少先队员出现了。"


translate chinese day2_clubs_40997bda:


    "一个是电子小子，另一个我不认识。"


translate chinese day2_clubs_42afd4b2:


    el "你好呀，谢苗同学！我们等了你很久啦！"


translate chinese day2_clubs_4d5cf736:


    th "他好像对每个人的每一件事都了如指掌..."


translate chinese day2_clubs_3dbd6055:


    me "你为什么要等着我？"


translate chinese day2_clubs_9069fff4:


    el "当然，你是来加入我们的机器人社团的吧？"


translate chinese day2_clubs_1a229aa6:


    "他没有给我机会回答。"


translate chinese day2_clubs_3db9f783:


    el "这是这里的负责人，舒里克。"


translate chinese day2_clubs_b5112857:


    me "我估计这个社团只有你们两个人？"


translate chinese day2_clubs_f6f084ef:


    el "这个嘛，你现在就可以说是三个了嘛。"


translate chinese day2_clubs_c4b2274f:


    "舒里克走上前来，坚定的伸出他的手。"


translate chinese day2_clubs_75eec175:


    "他的脸看起来有点熟悉。"


translate chinese day2_clubs_93f251d0:


    sh "欢迎来到我们的社团！"


translate chinese day2_clubs_751ac511:


    me "哈..."


translate chinese day2_clubs_b9d16cf3:


    el "跟我们来！{w}把这里当成是自己的家吧。"


translate chinese day2_clubs_4b6d8fbc:


    me "额，伙计们，我只是想..."


translate chinese day2_clubs_e600b6dc:


    sh "我们会一直欢迎新人的。"


translate chinese day2_clubs_ac94a513:


    "他这么一说我突然感觉大脑里在回响着苏联国歌。"


translate chinese day2_clubs_8aaf9ff7:


    "很神奇，我到现在还记得那歌词，一年级的时候我把歌词记录在一个练习本上。"


translate chinese day2_clubs_c82f982c:


    me "不、不，我只是想让你们签个字..."


translate chinese day2_clubs_4a94d273:


    el "是啊，你加入我们的社团，我们就给签字~"


translate chinese day2_clubs_1438f764:


    "他咧嘴一笑。"


translate chinese day2_clubs_01f5a268:


    "我正准备面对一场辩论，但是突然有人进来了。"


translate chinese day2_clubs_63509643:


    "我回头看到了斯拉维娅。"


translate chinese day2_clubs_131a6aad:


    sl "啊，谢苗！他们应该没有给你带来困扰吧？"


translate chinese day2_clubs_cf9000b4:


    "她眯起眼睛观察了一下祖国未来的机器人工业。"


translate chinese day2_clubs_f47d1028:


    sl "我认识这两个人——他们可是做得出来的。"


translate chinese day2_clubs_705bab2b:


    me "嗯，是这样的，我只要拿到签名..."


translate chinese day2_clubs_f0bcf20a:


    "我准备从中渔翁得利。"


translate chinese day2_clubs_cdedcb52:


    sl "没问题，给我吧。"


translate chinese day2_clubs_094e2e65:


    "斯拉维娅接过了那张表向舒里克走去。"


translate chinese day2_clubs_ec58ff09:


    sl "快签！"


translate chinese day2_clubs_204589a0:


    sh "别着急，我们还没..."


translate chinese day2_clubs_0e869d11:


    sl "没事了，赶紧签！"


translate chinese day2_clubs_8ceef228:


    "她威胁的眼神让舒里克没有反驳的机会。"


translate chinese day2_clubs_845789bc:


    "他在上边写了一些弯弯扭扭的字，我感谢了斯拉维娅，便心满意足的离开了。"


translate chinese day2_library_89cf3d9c:


    "说实话我挺喜欢读书，不过在这种情况下在图书馆里度日就不太合理了。"


translate chinese day2_library_651b3815:


    "我还是赶紧通过这个检查点吧。"


translate chinese day2_library_3113f296:


    "我走进图书馆的时候，童年的记忆被唤醒了。"


translate chinese day2_library_dc667c83:


    "还能清晰的展现在眼前。"


translate chinese day2_library_c1f61108:


    "我大概七八岁，和妈妈一起在图书馆。"


translate chinese day2_library_c0052ab2:


    "她在浏览我学习的书籍，我坐在角落里翻看各种漫画书。"


translate chinese day2_library_740b504e:


    "那时候我还不明白他们为什么有那么多的书，还有我为什么不能拿走。"


translate chinese day2_library_10b93d8b:


    "那个时候我还没有形成‘公共财产’这样的概念。"


translate chinese day2_library_71daf897:


    "那时候，财产的概念我都是模模糊糊的。"


translate chinese day2_library_26efdcce:


    "这种记忆现在显得更加奇怪了，在这个可能要在三年之内建成‘共产主义’的地方。"


translate chinese day2_library_faaf4baa:


    "这里充满着各种苏联符号，书架上也是各种相关的文学作品。"


translate chinese day2_library_dd1940c8:


    "不过我可不打算看，我现在可是不想阅读马克思哲学作品。"


translate chinese day2_library_86d9517e:


    th "管理员在哪里呀？"


translate chinese day2_library_a5dd78db:


    "我很快就找到了她。"


translate chinese day2_library_075aa233:


    "我仔细看着，{w}短发，厚眼镜，还挺可爱的。"


translate chinese day2_library_74556d4c:


    "她正安静的睡着，我不忍心叫醒她。"


translate chinese day2_library_4112cbd4:


    th "我可以等等，如果过了半个小时她还是不醒，到时候我再..."


translate chinese day2_library_cdaec1c2:


    "只是坐在这里十分无聊，所以我从书架上随便拿了一本书。"


translate chinese day2_library_48a28d8c:


    "作者叔本华，‘作为意志和表象的世界’。"


translate chinese day2_library_78cf63d0:


    "我随便翻开一页开始读起来：（此段为机翻）"


translate chinese day2_library_d7ca3dbf:



    nvl clear
    "一个人的生命，以其无穷的保养，愿望和痛苦，被视为生育行为，即生存意志的绝对索赔的解释和意译;而且，它也是人欠自然他的死亡原因，以及与此债务忧虑认为。"


translate chinese day2_library_cc4bf7fc:


    "这难道不是我们存在涉及有罪证据？"


translate chinese day2_library_26562ac7:


    "无论如何，我们始终存在，不时以死亡为我们的出生付出，我们始终存在，交替承担所有的喜悦和生命的悲哀，因为无论两者可以通过我们没有一定的效果 - 这是一个结果，我们的说的求生意志。"


translate chinese day2_library_4cfe9a45:



    nvl clear
    "因此死亡，这尽管生活中所有的苦难把我们牢牢地把它的恐惧，真的是虚幻的;但正如幻影是吸引我们进入它的冲动。"


translate chinese day2_library_0af732d3:


    "这种吸引力本身可以客观地看作是两个情人之间的相互思念的目光;他们是求生意志最纯粹的表达，它肯定。如何温柔嫩啊！"


translate chinese day2_library_a5a1d951:


    "它希望幸福，宁静的快乐，为自己温和乐趣，为他人，为所有。"


translate chinese day2_library_7e8c00bf:


    "有人在敲门。"


translate chinese day2_library_919288d5:


    "我赶紧把书放了回去。"


translate chinese day2_library_a7a56164:


    th "真是个好习惯啊，敲门。{w}我应该学习学习。"


translate chinese day2_library_4432512e:


    "是列娜。"


translate chinese day2_library_adda1c0b:


    un "哇..."


translate chinese day2_library_1a8f3935:


    me "嗨。"


translate chinese day2_library_2f04a7f9:


    "我保持微笑。"


translate chinese day2_library_ca64d35c:


    un "啊，你好，呃，我只是想要还这本书..."


translate chinese day2_library_04118915:


    "她拿着我昨天看到的那本乱世佳人。"


translate chinese day2_library_b946894f:


    un "哇，热尼娅在睡觉，我等会再来..."


translate chinese day2_library_6ec572fc:


    mz "我醒了。"


translate chinese day2_library_50100392:


    "我转过去惊讶的看着她。"


translate chinese day2_library_3e33ac87:


    "她从桌子那边看着我。"


translate chinese day2_library_4b484fa7:


    mz "你想干嘛？"


translate chinese day2_library_51266558:


    me "我需要你在这里签个名..."


translate chinese day2_library_c7f48bc6:


    mz "拿过来。"


translate chinese day2_library_486518bc:


    "她迅速签完还给了我。"


translate chinese day2_library_eb72bb13:


    "她的表情让我保持安静。"


translate chinese day2_library_9db07b02:


    "列娜去找她还书，我拿了表就走人了。"


translate chinese day2_aidpost_30420f41:


    th "来医务室的必要性？"


translate chinese day2_aidpost_37fed0c1:


    "我的健康没有问题啊，这里的空气十分清新，呼吸起来比平时还要顺畅。"


translate chinese day2_aidpost_513b3994:


    th "不过我就是需要来这里。"


translate chinese day2_aidpost_92c7a992:


    "我进了门。"


translate chinese day2_aidpost_b0f75288:


    th "一个普通的医务室。"


translate chinese day2_aidpost_cbdb3f5c:


    "一个中年女人坐在桌子前。{w}很显然是一个护士。"


translate chinese day2_aidpost_7b8718b9:


    "她在一旁打量着我。"


translate chinese day2_aidpost_7cce673f:


    csp "呃，你好啊...红领巾。"


translate chinese day2_aidpost_f14de383:


    "护士保持着在工作上的注意力。"


translate chinese day2_aidpost_b5fe60d8:


    me "下午好，老师...我有点事..."


translate chinese day2_aidpost_a3450ccf:


    csp "请坐。"


translate chinese day2_aidpost_9fa1bef7:


    "我环视四周。"


translate chinese day2_aidpost_40098e3a:


    csp "坐在沙发上。"


translate chinese day2_aidpost_04d910fd:


    "我坐了下来。"


translate chinese day2_aidpost_89b67c83:


    csp "脱衣服。"


translate chinese day2_aidpost_a9281d57:


    "她的语调没什么变化。"


translate chinese day2_aidpost_f43fc4fb:


    me "为啥...？"


translate chinese day2_aidpost_9e007bed:


    csp "察看病情，听诊，确认你的健康状态，等等。"


translate chinese day2_aidpost_fa5c927d:


    csp "顺便说，我叫薇奥列塔，不过你可以叫我维奥拉。"


translate chinese day2_aidpost_548e0299:


    "她转向我。"


translate chinese day2_aidpost_d4984f4c:


    cs "还等什么？赶紧脱。"


translate chinese day2_aidpost_f4cd982b:


    me "可是我没有健康问题，我只是..."


translate chinese day2_aidpost_c2002e32:


    "我把那张清单递给他。"


translate chinese day2_aidpost_c5b50119:


    cs "等一下。"


translate chinese day2_aidpost_116c6a89:


    "她拿下听诊器好像要给我听听..."


translate chinese day2_aidpost_89132ea0:


    "不过突然有个人敲门。"


translate chinese day2_aidpost_1128b7a1:


    "护士不情愿的说："


translate chinese day2_aidpost_2e8aea9e:


    cs "进来！"


translate chinese day2_aidpost_75d12839:


    "门一下打开了，电子小子进来了。"


translate chinese day2_aidpost_7220ee64:


    el "你好！我踢球的时候摔倒了，额不对，我没事，不过奥尔加·德米特里耶夫娜..."


translate chinese day2_aidpost_5741b364:


    "电子小子的眼睛青了一块。"


translate chinese day2_aidpost_efefe224:


    th "我很怀疑那个是不是踢球受的伤。"


translate chinese day2_aidpost_13a0ecdb:


    cs "坐下，我给你看看。"


translate chinese day2_aidpost_07c0e46a:


    "她说道。"


translate chinese day2_aidpost_8855685b:


    cs "你，把清单递给我。"


translate chinese day2_aidpost_d2d6a28b:


    "护士迅速签完字。"


translate chinese day2_aidpost_42b88054:


    cs "如果受了伤，一定要马上来找我呦~~"


translate chinese day2_aidpost_159b2e86:


    "我决定不去回答就走了出去。"


translate chinese day2_aidpost_9ff8721b:


    th "那个护士肯定不对劲..."


translate chinese day2_dinner_c6f8dc13:


    "总之我决定去吃饭。"


translate chinese day2_dinner_c8dab275:


    th "清单又不会自己走得掉，不过我的肚子可不能等到晚上。"


translate chinese day2_dinner_92787784:


    "我想着这些事情走进了食堂。"


translate chinese day2_dinner_5f1442eb:


    "里边几乎没有人，显然大家都吃完饭了。"


translate chinese day2_dinner_79c74b45:


    "一个友好的食堂阿姨给我递上了不错的饭菜：«Ипрская похлёбка»汤, «от Лаврентия Палыча»炖肉还装饰着十五世纪的煮土豆，还有«Таблица Менделеева»水果酒。"


translate chinese day2_dinner_3f63379f:


    "五星级酒店可不会上这种菜，不过我饿得已经不在乎了。"


translate chinese day2_dinner_0327cad9:


    "跟我平常的通心粉我奶酪的食谱比起来要好得多了。"


translate chinese day2_dinner_a677d27c:


    "我找了个最近的桌子专心吃饭。"


translate chinese day2_dinner_a5341529:


    "我的注意力突然被后背上的一巴掌击溃，差点噎着。"


translate chinese day2_dinner_0d703f5f:


    "乌里扬卡站在我面前洋洋得意。"


translate chinese day2_dinner_8de67373:


    me "我总有一天要噎死你。"


translate chinese day2_dinner_d8dece45:


    us "你要是追得上我就来呀。"


translate chinese day2_dinner_b4cfaed8:


    "她说出来了！"


translate chinese day2_dinner_0a675d44:


    us "你已经失败一次了。"


translate chinese day2_dinner_abd93e7a:


    me "好吧，这次我要偷袭你！"


translate chinese day2_dinner_57a622de:


    us "那不公平！"


translate chinese day2_dinner_fccbe21c:


    me "看看这是谁~公平小姐~"


translate chinese day2_dinner_71fc19fd:


    "我笑了起来。"


translate chinese day2_dinner_e00339d0:


    us "好吧，你在这里等着，我也去弄点吃的。"


translate chinese day2_dinner_822ea040:


    "我还是赶快吃完吧。"


translate chinese day2_dinner_2d5c9aae:


    "可是乌里扬卡不到半分钟就回来了。"


translate chinese day2_dinner_d8e82882:


    "她弄了点烤牛肉和煮土豆。"


translate chinese day2_dinner_b90cc3bd:


    "和我的‘皇家’食谱比起来..."


translate chinese day2_dinner_bff8415e:


    me "你怎么...你是从哪里搞到的？"


translate chinese day2_dinner_2da2c81a:


    us "不知道了吧~"


translate chinese day2_dinner_1d43f87e:


    "她看着我笑了起来露出自己的32颗...或者是多少颗...牙。"


translate chinese day2_dinner_d05806b0:


    th "这我可受不了！"


translate chinese day2_dinner_47797bcf:


    "我从来都不是什么恶作剧大师，而且在学校里我总是被欺负的那一方。"


translate chinese day2_dinner_e27a02d2:


    "不过我一定要想办法报复她一下。"


translate chinese day2_dinner_5dafbcae:


    me "那如果奥尔加·德米特里耶夫娜发现你从食堂偷吃呢？"


translate chinese day2_dinner_d7d48f29:


    us "我可没偷！"


translate chinese day2_dinner_1678d751:


    "她发火了。"


translate chinese day2_dinner_fc822a9d:


    me "我就要这么说啦，不知道她会不会相信你呢？"


translate chinese day2_dinner_88a1ff3e:


    us "她怎么能知道的？"


translate chinese day2_dinner_911579da:


    me "这个嘛，这要跟很多事情有关系..."


translate chinese day2_dinner_ac96beb1:


    us "比如呢？"


translate chinese day2_dinner_1b595140:


    "她看着我的眼睛。"


translate chinese day2_dinner_25334771:


    me "给我拿个小面包来，我要甜的。"


translate chinese day2_dinner_47228a37:


    us "我上哪里给你拿去啊？"


translate chinese day2_dinner_430fbe26:


    me "哼...就是你拿这些东西的地方？"


translate chinese day2_dinner_930035ce:


    "我指着她的盘子。"


translate chinese day2_dinner_35913bdd:


    "她犹豫了一下。"


translate chinese day2_dinner_b9e1270b:


    us "好吧，一个面包！"


translate chinese day2_dinner_0cd3249d:


    us "你要保证不告诉奥尔加·德米特里耶夫娜！"


translate chinese day2_dinner_23e8f891:


    me "我以少先队员的身份发誓！"


translate chinese day2_dinner_0b374dce:


    "她跑向厨房那边，我迅速把胡椒粉全部倒进她的饮料里。"


translate chinese day2_dinner_99986110:


    "我刚刚弄完，这个闲不住的小丫头就回来了。"


translate chinese day2_dinner_5c02fe6a:


    us "拿着，你个骗子！"


translate chinese day2_dinner_d22005bb:


    th "她似乎没有注意到。"


translate chinese day2_dinner_c0146da9:


    me "那么现在我们来比赛喝饮料吧，最后把水果汤喝完的人要去收拾碟子！"


translate chinese day2_dinner_07366c5a:


    us "别犯傻了。"


translate chinese day2_dinner_7f82c019:


    me "傻？看着我。"


translate chinese day2_dinner_78abf728:


    us "我可不会玩这些小孩子的游戏。"


translate chinese day2_dinner_d4d00518:


    me "哎呀，这是谁呀？"


translate chinese day2_dinner_6437d74d:


    "我阴险的笑着。"


translate chinese day2_dinner_8f15074d:


    us "哼，{i}你，{/i}看我的！一、二、三。"


translate chinese day2_dinner_d16b7597:


    "还没等我来得及拿起我的杯子，她已经一股脑的喝光了。"


translate chinese day2_dinner_dc6679ad:


    "仅仅过了一秒钟，她的表情就完全变成了恐惧，她的脸颊变成了红色，眼睛像是要跳出来一样。"


translate chinese day2_dinner_c1908ed9:


    "她一边吐着一边喊着跑向摆着水的桌子。"


translate chinese day2_dinner_053a22e5:


    us "你！你！你..."


translate chinese day2_dinner_b225e29e:


    "我决定不等她就出去，吃着自己的面包偷着乐。"


translate chinese day2_main2_a20cefa7:


    "..."


translate chinese day2_main2_dfe2adc7:


    "终于我搞到了所有的签名，现在只需要给奥尔加·德米特里耶夫娜送回去就行了。"


translate chinese day2_main2_98a12405:


    "她正坐在小屋前面，读着一本书。"


translate chinese day2_main2_bb12859e:


    "不像是她一直标榜的优秀少先队员的形象啊。"


translate chinese day2_main2_281d80a7:


    "我很奇怪她的权力到底有多大，除了责骂乌里扬娜，对我的品德、身体还有意识形态上指指点点？"


translate chinese day2_main2_482db2cf:


    me "给..."


translate chinese day2_main2_7cce4a44:


    "我把自己的清单递给她。"


translate chinese day2_main2_f913543d:


    "她看都没看就揣进了口袋里。"


translate chinese day2_main2_e0ce9392:


    th "真好，原来我可以自己在上面全都签上哪里都不用去。"


translate chinese day2_main2_7b44c8b1:


    mt "不错！那么，你见到了我们的护士了吗？"


translate chinese day2_main2_9513cd87:


    me "是啊..."


translate chinese day2_main2_ee81ac0c:


    "这个问题可是让我背后一阵凉气。"


translate chinese day2_main2_342a300f:


    mt "你加入了哪个社团呢？"


translate chinese day2_main2_8ba55fc5:


    me "我没...我得先想想..."


translate chinese day2_main2_af5748f9:


    mt "太遗憾了，你明天一定要做出选择呦！"


translate chinese day2_main2_02398cce:


    th "噢，当然，当然。"


translate chinese day2_main2_5d6ded1e:


    mt "好了，该吃晚饭了。"


translate chinese day2_main2_b59eddaa:


    th "是时候了，我正开始饿了。"


translate chinese day2_main2_4ff192ad:


    "我和奥尔加·德米特里耶夫娜一起前往食堂。"


translate chinese day2_main2_de8fec0b:


    "我看了看天空发现太阳正在落山。"


translate chinese day2_main2_962703d4:


    "阿丽夏和电子小子正站在门廊下边。..."


translate chinese day2_main2_cb3d2e1e:


    "还有乌里扬娜和斯拉维娅。"


translate chinese day2_main2_30f5e6dc:


    "我听到了他们说的话。"


translate chinese day2_main2_f04cac09:


    dv "不要再叫我二酱，不然我还会再..."


translate chinese day2_main2_a8e77ffd:


    el "我没那么叫啊，那是你听的问题..."


translate chinese day2_main2_689622eb:


    us "他说了！他说了！我听到了！"


translate chinese day2_main2_4c435994:


    el "你当时就没在那里吧！"


translate chinese day2_main2_ab85c187:


    us "我在！我在！我在灌木丛里。"


translate chinese day2_main2_992ca12a:


    sl "你们大伙，算了吧。"


translate chinese day2_main2_75a4c8b7:


    th "所以说电子小子之前受的伤不是踢球导致的。"


translate chinese day2_main2_cdbfd744:


    th "护士水平不错啊，我都看不出来他的黑眼圈了。"


translate chinese day2_main2_03ada258:


    "奥尔加·德米特里耶夫娜走上前来询问这骚动是怎么回事。"


translate chinese day2_main2_4bd88eb6:


    mt "发生了什么事？"


translate chinese day2_main2_b07cfe09:


    sl "阿丽夏和起司科夫..."


translate chinese day2_main2_ecff4d02:


    dv "我什么都没干！"


translate chinese day2_main2_6d1704fc:


    "她无聊的耸耸肩然后走了进去。"


translate chinese day2_main2_f38aed46:


    mt "好吧，现在是晚饭时间。"


translate chinese day2_main2_6c8ce019:


    "我最后走了进去。"


translate chinese day2_main2_84208ced:


    "剩余的座位不多了。"


translate chinese day2_main2_26bbcb23:


    "食堂那边有几个空位在阿丽夏旁边，不过我宁可饿上一周也不想拿我的脑袋换她旁边的一顿饱饭。"


translate chinese day2_main2_c136a510:


    "乌里扬卡旁边也有个位置，不过我可不想去那里吃中国传统的‘爬虫大餐’。"


translate chinese day2_main2_7c6eeade:


    "最后，未来的旁边还有一个位置。"


translate chinese day2_main2_1a652dc4:


    "看来我得自绝后路..."


translate chinese day2_main2_8b9ac1bf:


    me "请问你介意我坐在这里吗？"


translate chinese day2_main2_a9efde1c:


    mi "啊，是的！不对，我是说不，不介意！不，我是说，是的，你可以坐在这里！"


translate chinese day2_main2_04d910fd:


    "我坐了下来。"


translate chinese day2_main2_6ec2d5c6:


    mi "哇，看啊，今天是荞麦，你喜欢荞麦吗？还有鸡肉，啊我不喜欢鸡肉，啊，不是说不喜欢..."


translate chinese day2_main2_a2e3290c:


    mi "不过你要是问我更喜欢哪一个的话，那应该是酸奶油炖肉或者是蔬菜炖肉...不对，也许一个汉堡就行！或者是牛屁股，你喜欢牛屁股吗？"


translate chinese day2_main2_f14d5492:


    me "我没那么挑剔啦。"


translate chinese day2_main2_ea0a7a01:


    "这倒是事实。"


translate chinese day2_main2_af2e7a5f:


    mi "哇，是那样啊，不过至于甜点，这里的甜点可不是太好。我想吃冰淇淋，你喜欢冰淇淋吗？我喜欢‘48戈比’ 还有‘Leningradskoe’的，哎呀，抱歉，我一直自说自话。"


translate chinese day2_main2_9d46cf08:


    mi "也许你更喜欢‘Eskimo’的？"


translate chinese day2_main2_14b91769:


    "这晚餐已经让我无语了，感谢这位大姐。"


translate chinese day2_main2_28655f21:


    "我也不是那种可以忽略别人的谈话的人，{w}即使是她。"


translate chinese day2_main2_c7c2dae1:


    th "我们还是在一个桌子上吃饭的嘛。"


translate chinese day2_main2_f0878075:


    mi "话说，有一次我买了一个甜筒，正吃的时候，你知道怎么了吗？我在那里边发现了一个螺丝！真的是一颗螺丝喔~或者说是螺栓？我是在不太懂这些事情..."


translate chinese day2_main2_791bb7e0:


    mi "螺丝是用改锥来拧的，螺栓是用扳子的对吧？"


translate chinese day2_main2_24441c77:


    th "现在要是有个吃货比赛我估计能得前三名。"


translate chinese day2_main2_6cbc801e:


    me "好了，我要走了，你慢慢吃！"


translate chinese day2_main2_91330d37:


    "我起来走了出去。"


translate chinese day2_main2_a6b59989:


    "未来还在说着什么，不过她的声音被喧闹的少先队员们淹没掉了。"


translate chinese day2_main2_757dd6ab:


    "我出来坐在台阶上，慢慢消化晚饭。"


translate chinese day2_main2_bbb07ad8:


    "我就坐在这里，看着夜幕逐渐降临。"


translate chinese day2_main2_a85b2ea6:


    "白天，这里的一切都充满生机：孩子们高兴的叫着、笑着，追逐嬉戏，愉快的聊天，在水中游泳。"


translate chinese day2_main2_bf72d174:


    "不过天黑之后营地就会完全变了样。"


translate chinese day2_main2_1732123e:


    "白天的喧嚣变成了夜里的寂静，只有偶尔的蛐蛐和夜里活动的鸟儿。"


translate chinese day2_main2_0a2a1026:


    "营地也要休息了。"


translate chinese day2_main2_c328b2c5:


    "每一处阴影里你都能看见什么，也许是一个鬼？或是森林的灵魂？或是一头野兽？最不可能是一个人了。"


translate chinese day2_main2_c347461a:


    "昨晚是这样，今天也一样。"


translate chinese day2_main2_2ac17ce1:


    "本地人过着十分有规律的生活。"


translate chinese day2_main2_3a690eee:


    "白天，营地是他们的；晚上，营地更像是属于大自然的。"


translate chinese day2_main2_0b5df81a:


    "有人碰了我肩膀一下。"


translate chinese day2_main2_47eaf14e:


    "我回头看了看。"


translate chinese day2_main2_49bd4154:


    "是电子小子。"


translate chinese day2_main2_6385785b:


    el "咱们去玩牌吧。"


translate chinese day2_main2_f8ca8757:


    me "牌？"


translate chinese day2_main2_6d492a61:


    el "是啊！我发明了一种新游戏，很不错哦。"


translate chinese day2_main2_bfe132d8:


    me "不错？怎么不错？"


translate chinese day2_main2_ad0877c3:


    el "这个嘛，咱们得先找到牌然后我再跟你说。"


translate chinese day2_main2_3c6dcd82:


    me "那去找啊，有什么问题吗？"


translate chinese day2_main2_2adc32e8:


    el "只有奥尔加·德米特里耶夫娜有，她又不肯给我..."


translate chinese day2_main2_5ac38cf1:


    me "为什么呢？"


translate chinese day2_main2_6c259ad3:


    el "那个，上次我们..."


translate chinese day2_main2_0f60fa5f:


    "奥尔加·德米特里耶夫娜和斯拉维娅来到了门廊。"


translate chinese day2_main2_8831b761:


    el "奥尔加·德米特里耶夫娜！谢苗正想和你讨论一下玩牌的问题。"


translate chinese day2_main2_09a322d4:


    me "实际上..."


translate chinese day2_main2_458888bd:


    mt "为了干什么？"


translate chinese day2_main2_de85b885:


    el "我们发明了一种新游戏！"


translate chinese day2_main2_aa6ae640:


    th "不是咱们，是你。"


translate chinese day2_main2_c7f522d1:


    mt "什么游戏？"


translate chinese day2_main2_3d77030f:


    el "我需要有一副牌才能展示给你。"


translate chinese day2_main2_ee0157c7:


    mt "嗯...我不喜欢这种感觉...{w}不过既然谢苗也支持你，那也许可以吧..."


translate chinese day2_main2_0d6093c8:


    me "说实话..."


translate chinese day2_main2_93e80e0a:


    sl "我们一起去吧，奥尔加·德米特里耶夫娜！"


translate chinese day2_cards_with_sl_a755f898:


    me "如果你不介意的话..."


translate chinese day2_cards_with_sl_547903c7:


    sl "没问题，咱们走吧！"


translate chinese day2_cards_with_sl_956c974d:


    "我们朝着我的小屋走去。"


translate chinese day2_cards_with_sl_2d658ff8:


    "大概走了一半的路的时候斯拉维娅停了下来。"


translate chinese day2_cards_with_sl_ca62c00d:


    sl "对了，我突然想起来了！那副牌在我那里。"


translate chinese day2_cards_with_sl_7c2541af:


    th "哇，这时间选的很巧啊。"


translate chinese day2_cards_with_sl_1b37703b:


    me "那么该往哪里走呢？"


translate chinese day2_cards_with_sl_2015af9e:


    sl "就是沿着这条路了，咱们走。"


translate chinese day2_cards_with_sl_a20cefa7:


    "..."


translate chinese day2_cards_with_sl_c8a2cf84:


    "我们来到了一个小屋前，不过它看起来更像是一辆拖车。"


translate chinese day2_cards_with_sl_113ad468:


    sl "在这里等一下，我马上回来！"


translate chinese day2_cards_with_sl_5fa71576:


    "只用了几秒钟她就回来了。"


translate chinese day2_cards_with_sl_ca656115:


    sl "看！"


translate chinese day2_cards_with_sl_21c26363:


    "一副伤痕累累的牌。"


translate chinese day2_cards_with_sl_24f3bb91:


    me "这上面怎么好像做了很多记号..."


translate chinese day2_cards_with_sl_ccc884da:


    sl "太没有运动员精神了，这还怎么公平的玩？"


translate chinese day2_cards_with_sl_74535a0c:


    "是啊，还不知道规则怎么作弊..."


translate chinese day2_cards_with_sl_ad3d2e0c:


    sl "咱们走吧？"


translate chinese day2_cards_with_sl_304c9144:


    me "走吧。"


translate chinese day2_cards_with_sl_a20cefa7_1:


    "..."


translate chinese day2_cards_with_sl_5316a3b2:


    "在我们返回的路上，我决定再试探着打听打听。"


translate chinese day2_cards_with_sl_6f6dbf74:


    me "你来这里多长时间了？"


translate chinese day2_cards_with_sl_4a08d89f:


    sl "在这个夏令营吗？大概一个星期吧。"


translate chinese day2_cards_with_sl_237356a1:


    me "嗯...那么你是从哪里来的呢？"


translate chinese day2_cards_with_sl_9cbcee67:


    sl "我从北边来。"


translate chinese day2_cards_with_sl_5fd101b5:


    me "那是？"


translate chinese day2_cards_with_sl_3e5e8c80:


    sl "寒冷的北方~"


translate chinese day2_cards_with_sl_943eaf3a:


    "她笑着看着我。"


translate chinese day2_cards_with_sl_a94c6d7f:


    th "看来这里的人再单纯的问题都不会回答的。"


translate chinese day2_cards_with_sl_9256c52d:


    "我试着从另外的角度切入。"


translate chinese day2_cards_with_sl_f2292cea:


    me "你喜欢什么呢？"


translate chinese day2_cards_with_sl_63f53767:


    sl "什么意思？"


translate chinese day2_cards_with_sl_1b25eb7c:


    me "就是说，你的爱好？"


translate chinese day2_cards_with_sl_710d465d:


    sl "哦...我喜欢大自然。"


translate chinese day2_cards_with_sl_fe5ee6e6:


    "奇怪，她今天似乎不太健谈。"


translate chinese day2_cards_with_sl_219d4457:


    me "大自然？嗯...{w} 想要做一个自然历史学家吗？"


translate chinese day2_cards_with_sl_92f9746d:


    sl "更像是个普通的历史学家吧，我对本国历史比较感兴趣。"


translate chinese day2_cards_with_sl_e357f0bd:


    "这倒是挺适合她的。"


translate chinese day2_cards_with_sl_2efeef27:


    "似乎所有的本地人里，只有她没有隐藏什么。"


translate chinese day2_cards_with_sl_0a42064b:


    th "有没有可能她和我一样不知道怎么来到了这里然后不敢对别人随便说什么？"


translate chinese day2_cards_with_sl_300c021f:


    "我决定先试试水。"


translate chinese day2_cards_with_sl_6c6041ec:


    me "你为什么选择这个夏令营呢？"


translate chinese day2_cards_with_sl_a89e29ad:


    sl "不是我选择的，我的父母在他们的单位给我开了证明。"


translate chinese day2_cards_with_sl_8105ae57:


    th "失败..."


translate chinese day2_cards_with_sl_49976930:


    me "那个，如果你能够选择的话呢？"


translate chinese day2_cards_with_sl_baad4a6d:


    sl "这里很不错啊！{w}我不觉得我会选择别的地方，在这里你就像是变成了别的人！"


translate chinese day2_cards_with_sl_e9ca641d:


    "我看起来好像不是这样。"


translate chinese day2_cards_with_sl_d84daee3:


    me "变成了别的人是什么意思？"


translate chinese day2_cards_with_sl_b1f7a78f:


    sl "就是说，这里有很多的可能性，你可以在这里学习，并且认识很多的朋友！"


translate chinese day2_cards_with_sl_fcc6a9dc:


    "现在她说的话像是辅导员一样，有点吓到我了。"


translate chinese day2_cards_with_sl_30054c86:


    "我决定暂时不在问问题了。"


translate chinese day2_cards_with_sl_58bb1834:


    "当我们回来的时候，奥尔加·德米特里耶夫娜说道："


translate chinese day2_cards_with_sl_fc9fa381:


    mt "我刚刚想起来牌应该是在你那里！"


translate chinese day2_cards_with_sl_9446991e:


    sl "没关系，已经拿来了。"


translate chinese day2_cards_with_sl_74619cda:


    mt "不错，不错。"


translate chinese day2_cards_without_sl_587eb799:


    me "我去拿吧。"


translate chinese day2_cards_without_sl_888bedb3:


    mt "好吧，在我的房间里，在书桌的抽屉里。"


translate chinese day2_cards_without_sl_8b243a91:


    "我前往奥尔加·德米特里耶夫娜的房间。"


translate chinese day2_cards_without_sl_9ff4064f:


    th "我到底是为什么会答应啊？"


translate chinese day2_cards_without_sl_ef9c0eef:


    th "不过话说回来，我又有什么选择呢？"


translate chinese day2_cards_without_sl_c56c19ea:


    th "在这里晚上都没什么可干的，这样至少还能玩一玩。"


translate chinese day2_cards_without_sl_a5b9da23:


    "不过我的大脑不断对我唠叨着这可不是玩的时候。"


translate chinese day2_cards_without_sl_4158e5dc:


    th "另一方面来说，这里是一个少先队员的夏令营，那一定意味着什么。"


translate chinese day2_cards_without_sl_c1286124:


    th "即使这是梦境什么的，那这个夏令营也是主要的部分，答案也应该是在营地里而不是森林里。"


translate chinese day2_cards_without_sl_23bd994f:


    "不过就现在的情况来讲，{i}所谓的答案{/i}就是玩牌吧..."


translate chinese day2_cards_without_sl_cf613096:


    "我用自己的钥匙打开了门，走了进去。"


translate chinese day2_cards_without_sl_09b88332:


    "不过奇怪的是，里边并没有纸牌。"


translate chinese day2_cards_without_sl_956232aa:


    "里面充满着杯子、刀子、叉子、盘子，胶带、剪刀、橡胶手套，一段绳子、几个塑料袋，铅笔、还有几只坏掉的钢笔。"


translate chinese day2_cards_without_sl_d96cc0f8:


    "什么都有，就是没有纸牌。"


translate chinese day2_cards_without_sl_c0fa483e:


    th "也许我应该查看一下衣柜？"


translate chinese day2_cards_without_sl_c357b6ff:


    "里面都是奥尔加·德米特里耶夫娜的衣服，不过上方的一个带有钥匙孔的小抽屉吸引了我。"


translate chinese day2_cards_without_sl_d7abae60:


    "我拉了一下，没有打开。"


translate chinese day2_cards_without_sl_394c4aa1:


    th "我很好奇。"


translate chinese day2_cards_without_sl_a9798457:


    th "硬开可是没有好处的，即使纸牌就在这里边。"


translate chinese day2_cards_without_sl_da208a8e:


    "我正准备离开时，突然想起了什么。"


translate chinese day2_cards_without_sl_69cc04fb:


    "斯拉维娅昨天忘的钥匙，还放在这里。"


translate chinese day2_cards_without_sl_505d2b4b:


    th "万一...？"


translate chinese day2_cards_without_sl_7774a647:


    "我犹豫了一下，然后还是挑了一把钥匙往锁头里插过去。"


translate chinese day2_cards_without_sl_5b37b93a:


    "这要是这随便的就能打开就很奇怪了嘛，斯拉维娅为什么会带着奥尔加·德米特里耶夫娜的私人箱子的钥匙啊。"


translate chinese day2_cards_without_sl_87702aa9:


    "不过让我吃惊的是，下一把钥匙很顺畅的滑了进去，我正要拧一拧把手。"


translate chinese day2_cards_without_sl_bd5226c8:


    "突然我背后的大门吱吱作响。"


translate chinese day2_cards_without_sl_82b93712:


    "我跳起来转过身，不过屋里没有人。"


translate chinese day2_cards_without_sl_5ffdfa53:


    th "是风吹的吗？"


translate chinese day2_cards_without_sl_6e22d9c9:


    "我仔细的观察了一下。一个人都没有。"


translate chinese day2_cards_without_sl_82fc9b6c:


    "可能没什么事，但是我还是感觉不自在。"


translate chinese day2_cards_without_sl_a20cefa7:


    "..."


translate chinese day2_cards_without_sl_badefa0c:


    "我甚至还观察了一下屋外的灌木丛有没有人，不过之后还是决定继续探索辅导员同志的黑暗秘密。"


translate chinese day2_cards_without_sl_3d3069e0:


    sl "谢苗，你怎么这么慢？"


translate chinese day2_cards_without_sl_f9a84268:


    me "啊...我..."


translate chinese day2_cards_without_sl_28bdcb47:


    "我的手颤抖着赶快关上了抽屉，拔出钥匙。"


translate chinese day2_cards_without_sl_abc9e8c7:


    "斯拉维娅靠近了一步。"


translate chinese day2_cards_without_sl_45ebf54a:


    sl "啊！我的钥匙！我以为丢了呢，你从哪里找到的？"


translate chinese day2_cards_without_sl_52fd9c46:


    me "在门外的草丛里，刚刚找到的..."


translate chinese day2_cards_without_sl_ab249332:


    "还好我在她看见之前关上了。"


translate chinese day2_cards_without_sl_71f4991e:


    me "我们可以走了吗？"


translate chinese day2_cards_without_sl_9882a00c:


    "我想赶紧离开这个是非之地..."


translate chinese day2_cards_without_sl_9703c95b:


    "我回来之后，奥尔加·德米特里耶夫娜告诉我："


translate chinese day2_cards_without_sl_656cc591:


    mt "抱歉，我突然想起来应该是在斯拉维娅的屋里。{w}她在你过去的时候找到了。"


translate chinese day2_cards_without_sl_899192e2:


    "我看着斯拉维娅，她温柔的笑了。"


translate chinese day2_cards_without_sl_79ea7a49:


    th "好吧，你们不要担心我了..."


translate chinese day2_pre_cards_9f1a8f06:


    "斯拉维娅和奥尔加·德米特里耶夫娜走了进去。"


translate chinese day2_pre_cards_09995e69:


    "我正要进去，有人从后面拉住了我。"


translate chinese day2_pre_cards_08478924:


    "阿丽夏。"


translate chinese day2_pre_cards_d0c76f00:


    "她盯着我，又让我后背一凉，这可不是好现象。"


translate chinese day2_pre_cards_fe95bf48:


    me "你有什么事吗？"


translate chinese day2_pre_cards_fcaa413f:


    "我谨慎的问道。"


translate chinese day2_pre_cards_29ab9a8f:


    dv "你要玩这个脑残的游戏吗？"


translate chinese day2_pre_cards_d14b9495:


    me "呃...对？有什么问题吗？"


translate chinese day2_pre_cards_a364a5cb:


    dv "没有，没事。"


translate chinese day2_pre_cards_bce20dc9:


    "她准备转身离开又回头看看我笑了。"


translate chinese day2_pre_cards_9900fc17:


    dv "所以说，你玩牌？"


translate chinese day2_pre_cards_11e21afc:


    me "玩一点。"


translate chinese day2_pre_cards_f288a1e1:


    "我弄不明白她想干什么。"


translate chinese day2_pre_cards_3b50c11d:


    dv "所以只是玩‘DURAK’？"


translate chinese day2_pre_cards_11670603:


    th "好像你是个扑克牌大师似的..."


translate chinese day2_pre_cards_0624cee5:


    me "嗯，是的，理论上。"


translate chinese day2_pre_cards_7c7e60b4:


    dv "那你就没戏了。"


translate chinese day2_pre_cards_5ac38cf1:


    me "为啥？"


translate chinese day2_pre_cards_de97c6a8:


    dv "因为！"


translate chinese day2_pre_cards_72a60644:


    me "那么你知道规则？"


translate chinese day2_pre_cards_bdbe816e:


    dv "当然！"


translate chinese day2_pre_cards_3ad45daf:


    me "好吧，那你先手就好了。"


translate chinese day2_pre_cards_75b64a69:


    "我不明白这对话有什么意义，于是准备进门。"


translate chinese day2_pre_cards_cbef602e:


    dv "你为什么总想走？"


translate chinese day2_pre_cards_d3fbefa3:


    th "你是有什么话想说？"


translate chinese day2_pre_cards_38f554a8:


    dv "咱们打个赌吗？"


translate chinese day2_pre_cards_1084810b:


    me "你这是什么意思？"


translate chinese day2_pre_cards_abd41233:


    dv "你怎么这么不开窍？赌牌呀，还能是什么？"


translate chinese day2_pre_cards_2020ce91:


    me "那么你想赌什么？"


translate chinese day2_pre_cards_8d15d8fc:


    dv "我会赢！"


translate chinese day2_pre_cards_14abf4d1:


    me "嘛，那是一种可能的结果。"


translate chinese day2_pre_cards_47f0669e:


    "我谨慎的表示同意。"


translate chinese day2_pre_cards_e82a5cb1:


    dv "你害怕了？"


translate chinese day2_pre_cards_7af7e68f:


    me "没有啊...{w}只是不太清楚的时候不想争。"


translate chinese day2_pre_cards_f43da706:


    dv "那么你也不敢冒风险。"


translate chinese day2_pre_cards_7c484d79:


    th "如此敏锐的观察力，我很震惊啊。"


translate chinese day2_pre_cards_9fba406d:


    me "好吧，那我..."


translate chinese day2_pre_cards_0d611ce5:


    dv "不，你不是。"


translate chinese day2_pre_cards_624ed6e0:


    me "又怎么了？"


translate chinese day2_pre_cards_8eadd935:


    "我无力的叹了口气。"


translate chinese day2_pre_cards_e071c864:


    "她用无聊的谈话内容和没有亮点的打赌挑战我的下限。"


translate chinese day2_pre_cards_3f46f6f4:


    dv "如果你不和我打赌的话，我就要告诉大家你勾引我。"


translate chinese day2_pre_cards_3d342d5a:


    me "啥？！"


translate chinese day2_pre_cards_89e2898c:


    dv "你听的没错。"


translate chinese day2_pre_cards_75246721:


    th "这倒是意料之中..."


translate chinese day2_pre_cards_96533413:


    me "别傻了。{w}谁信啊？我只来到这里两天的时间，再有..."


translate chinese day2_pre_cards_9ffc882d:


    dv "你想撞撞运气？"


translate chinese day2_pre_cards_1f560713:


    me "好吧...{w}那么我赢了会怎么样？"


translate chinese day2_pre_cards_8a92943c:


    dv "我就不会和大家说了。"


translate chinese day2_pre_cards_a21dfdaa:


    me "我要是输了呢？"


translate chinese day2_pre_cards_09931ae3:


    dv "脑子又秀逗了？{w}我会告诉大家你勾引我啊，已经说过了嘛。"


translate chinese day2_pre_cards_d4012ccc:


    me "所以说我费半天劲只能证明我没干过什么，而且实际上我真的没有干过？"


translate chinese day2_pre_cards_37b7659b:


    dv "那是你的观点。"


translate chinese day2_pre_cards_17cc9d34:


    "这不是个简单的决定。"


translate chinese day2_pre_cards_742c0e25:


    "一方面说，同意这个就很愚蠢，我不懂这个游戏的规则，对赌博也不在行。"


translate chinese day2_pre_cards_a57f27fe:


    "另一方面来说，她简直是地狱。"


translate chinese day2_pre_cards_286e380f:


    th "再说了，我能相信她吗？"


translate chinese day2_pre_cards_3f4ee582:


    th "我赢了她还是可以说。"


translate chinese day2_pre_cards_7cc3db09:


    dv "那么，你做好决定了吗？"


translate chinese day2_pre_cards_25b3a2ba:


    "我正要回答，突然列娜从我身后过来了。"


translate chinese day2_pre_cards_a07b4105:


    dv "什么？"


translate chinese day2_pre_cards_c93b52ae:


    un "没..."


translate chinese day2_pre_cards_3653acd5:


    "列娜马上又进去了。"


translate chinese day2_pre_cards_dbeba143:


    dv "所以？"


translate chinese day2_pre_cards_369c0b87:


    th "我得后悔一百次..."


translate chinese day2_pre_cards_788e115a:


    me "好吧，我认了。"


translate chinese day2_pre_cards_d1046a49:


    "她笑了。"


translate chinese day2_pre_cards_e541d96d:


    me "不过如果我赢了..."


translate chinese day2_pre_cards_33997702:


    dv "对~对~公平竞争，不许作弊~"


translate chinese day2_pre_cards_8fa3ceb0:


    "阿丽夏转身走进食堂。"


translate chinese day2_pre_cards_e69bd7ba:


    th "我为什么要干这个？"


translate chinese day2_pre_cards_201eb6bf:


    th "就因为她可以随时随地随心所欲的坑我？"


translate chinese day2_pre_cards_5354793c:


    th "既然她决定了..."


translate chinese day2_pre_cards_e331dbe9:


    th "不对，我可不要和你进行这种不可告人的交易。"


translate chinese day2_pre_cards_a4a97e2b:


    me "抱歉了。"


translate chinese day2_pre_cards_bf45c8f3:


    dv "胆小鬼！"


translate chinese day2_pre_cards_3b0ca4e8:


    "她耸了耸肩走了上去，在那之前还瞟了我一眼："


translate chinese day2_pre_cards_9ccfc1da:


    dv "一会儿有你好果子吃的。"


translate chinese day2_pre_cards_46588f42:


    th "好果子..."


translate chinese day2_pre_cards_a83588c3:


    th "如果我做出了错误的决定呢？"


translate chinese day2_pre_cards_c92ad65b:


    "她总是能够把情况变得复杂。"


translate chinese day2_pre_cards_752fe6dc:


    "另一方面，我可不能太鲁莽了。"


translate chinese day2_pre_cards_808392c5:


    "我深深地叹了一口气，跟着她进了门。"


translate chinese day2_cards_6c6e7a1b:


    "里面已经布置好了。"


translate chinese day2_cards_18d88118:


    "周围站着几个少先队员聊着天。"


translate chinese day2_cards_28235b58:


    "饭桌被搬开给选手和观众腾地方。"


translate chinese day2_cards_bd07ee1b:


    "我看了看周围。"


translate chinese day2_cards_1fda7f4b:


    "我看到那边有点情况。"


translate chinese day2_cards_3f899848:


    "我走近了看发现是一张表格。"


translate chinese day2_cards_5f123cf2:


    "上面是选手的名字，也包括我。"


translate chinese day2_cards_a9b3e057:


    me "这是谁想出来的鬼主意？"


translate chinese day2_cards_8b5a8e17:


    "我拍了拍旁边的电子小子。"


translate chinese day2_cards_7d795258:


    el "这当然是您最卑微的仆人。"


translate chinese day2_cards_d353e361:


    "他还和我鞠躬，弄得我浑身不自在。"


translate chinese day2_cards_234f4ba7:


    me "而且为什么我的名字会在上面？"


translate chinese day2_cards_24671a0f:


    "我很失望。"


translate chinese day2_cards_5c08eef1:


    "几秒钟之前我还琢磨着自己可能有机会不参加这个比赛呢，也就省着输了比赛被阿丽夏胡搅蛮缠了。"


translate chinese day2_cards_1c56c3d2:


    "几秒钟之前我还琢磨着自己可能有机会不参加这个比赛呢，也就省着没有打赌被阿丽夏胡搅蛮缠了。"


translate chinese day2_cards_9ca648ce:


    th "现在我的希望破灭了。"


translate chinese day2_cards_8191ef08:


    el "这是纯巧合。"


translate chinese day2_cards_f0331a22:


    th "是啊，巧合，除了我认识每一个参赛者。"


translate chinese day2_cards_dc663958:


    th "还有几十个少先队员站在旁边！"


translate chinese day2_cards_565fd677:


    "我烦的抓耳挠腮。"


translate chinese day2_cards_287334ff:


    "我好像在审讯室一样。"


translate chinese day2_cards_d9135825:


    me "赢了有奖吗？"


translate chinese day2_cards_e0592077:


    "我懒洋洋的问道。"


translate chinese day2_cards_b19ed28a:


    "我想问一些不相关的问题分散自己的注意力。"


translate chinese day2_cards_98766828:


    "电子小子正要回答，乌里扬卡不知道从哪里蹦出来在他周围跳着："


translate chinese day2_cards_fbd0629e:


    us "奖品！奖品！"


translate chinese day2_cards_aa52231b:


    us "我听说有奖品！"


translate chinese day2_cards_2a82cf49:


    me "你知道奥林匹克的价值观是什么吗？"


translate chinese day2_cards_c7b7a3fa:


    us "不懂。"


translate chinese day2_cards_41d394ae:


    me "你长大了就会明白了！"


translate chinese day2_cards_afdea261:


    "她做了个邪恶的表情然后戳了戳电子小子的肋骨。"


translate chinese day2_cards_06a30073:


    us "奖品怎么办啊？"


translate chinese day2_cards_bdf4c52f:


    el "这个...我也不知道。{w}这又不是我说了算。"


translate chinese day2_cards_87bdcd1f:


    "他做了个没有办法的手势。"


translate chinese day2_cards_7da0931b:


    th "不过说真的，你都举办了这个愚蠢的比赛了，至少也准备点巧克力奖牌之类的吧？"


translate chinese day2_cards_b35ebcaf:


    "乌里扬卡突然跳了起来不知道冲向哪里去了。"


translate chinese day2_cards_95df2d6c:


    th "我要是有那么乐观..."


translate chinese day2_cards_806b5dfa:


    me "那么规则是？"


translate chinese day2_cards_c4fe8ebc:


    el "等一下！{w}人还没到齐。"


translate chinese day2_cards_b58585ce:


    "我看了看周围，阿丽夏、斯拉维娅、列娜、未来、还有舒里克都在这里了。"


translate chinese day2_cards_b6cde061:


    me "好像所有人都到了..."


translate chinese day2_cards_7b898ccd:


    el "不是的，热尼娅还没有来！"


translate chinese day2_cards_0d6ad6c9:


    "他很心虚吗？还是只有我这么觉得？"


translate chinese day2_cards_51851d31:


    me "她不在这里，所以呢？"


translate chinese day2_cards_3a674479:


    me "找别人代替一下吧。"


translate chinese day2_cards_d17d3a9a:


    el "不，我不能那样..."


translate chinese day2_cards_8b484ca2:


    "他缓慢的回答道。"


translate chinese day2_cards_1522ed9b:


    "我决定不去探究为什么。"


translate chinese day2_cards_ed082483:


    me "那就去找她还是什么的，我也不知道。"


translate chinese day2_cards_30b1301c:


    mt "他不能去，他是主持人啊。"


translate chinese day2_cards_06d5bcd8:


    "辅导员又不知道从哪里出现了。"


translate chinese day2_cards_8153d6f9:


    el "可是奥尔加·德米特里耶夫娜...！"


translate chinese day2_cards_1d92e913:


    "电子小子哀求道。"


translate chinese day2_cards_4e9a3025:


    mt "谢苗会去的。{w}对吧，谢苗同学？"


translate chinese day2_cards_943eaf3a:


    "她笑着看着我。"


translate chinese day2_cards_6bdedf0e:


    th "当然，反正总是我..."


translate chinese day2_cards_8cc053bc:


    me "她在哪里呀？"


translate chinese day2_cards_3030d7cf:


    mt "我估计是在图书馆。"


translate chinese day2_cards_4366e696:


    me "好吧..."


translate chinese day2_cards_f0078f92:


    "我拖着腿走向门口。"


translate chinese day2_cards_063c8560:


    el "请你快一点！"


translate chinese day2_cards_e98e1cff:


    "这小子到底有什么毛病？"


translate chinese day2_cards_75952aae:


    th "快要天黑了。"


translate chinese day2_cards_ec3b332d:


    "我准备安步当车，所以慢慢往图书馆那里溜达。"


translate chinese day2_cards_7692b3b8:


    "不过我突然遭遇了热尼娅—她坐在广场的长椅上，望着Genda，不孕，不育。"


translate chinese day2_cards_eca593b5:


    me "你在这里干什么？{w}所有人都在找你！"


translate chinese day2_cards_17c805c8:


    mz "如你所见，坐在这里，不孕，不育。"


translate chinese day2_cards_823e3ce8:


    "她皱起眉头。"


translate chinese day2_cards_a15cb848:


    me "那咱们走吧！"


translate chinese day2_cards_085c1aa0:


    mz "我不想去。"


translate chinese day2_cards_a595fb55:


    "她看向一边。"


translate chinese day2_cards_5ac38cf1:


    me "为何？"


translate chinese day2_cards_0a62b9a3:


    mz "我不乐意！"


translate chinese day2_cards_12af78a1:


    "我坐在了她旁边。"


translate chinese day2_cards_9539c19b:


    me "你看，我自己也不喜欢这个比赛，不过我们不能让大家失望啊。"


translate chinese day2_cards_7ae8c79a:


    "这不像是我说出来的话。"


translate chinese day2_cards_f8b4058b:


    "几天之前这种话对我来说还是不可思议的。"


translate chinese day2_cards_377eefc7:


    "热尼娅的脸上充满了惊讶。"


translate chinese day2_cards_59271a84:


    mz "所以说大家都在等着我吗？"


translate chinese day2_cards_6d6c0c61:


    th "我刚才不是这么说的？"


translate chinese day2_cards_fc30fd2b:


    me "是的。"


translate chinese day2_cards_40d696ab:


    mz "那我也不去！"


translate chinese day2_cards_a5db4948:


    "她皱着眉头把脸藏了起来。"


translate chinese day2_cards_65a156a9:


    me "可是为什么？"


translate chinese day2_cards_6cd0f3dd:


    "我挥舞着手臂表示不解。"


translate chinese day2_cards_7311aff6:


    mz "我不知道怎么玩牌..."


translate chinese day2_cards_4473e360:


    me "这有什么啦？{w}我这不一样嘛。"


translate chinese day2_cards_687e7e02:


    mz "那你怎么玩啊？"


translate chinese day2_cards_a7327cc4:


    me "纳尼？难道你只能做在书里学到的事情吗？"


translate chinese day2_cards_976a6d64:


    mz "那当然。"


translate chinese day2_cards_a4614188:


    "她很吃惊。"


translate chinese day2_cards_32c9a33a:


    me "那你要是在北极只能抓企鹅怎么办？"


translate chinese day2_cards_0fa90515:


    mz "北极没有企鹅。"


translate chinese day2_cards_beecbb90:


    "热尼娅笑了。"


translate chinese day2_cards_c2ae7af0:


    me "这没有关系嘛，只是举个栗子。"


translate chinese day2_cards_8d36304d:


    me "来吧，只是玩牌嘛，也不会决定谁的生死。"


translate chinese day2_cards_60f3d758:


    "她在费力的作着思考。"


translate chinese day2_cards_fc98202b:


    mz "我只是不想让大家失望。"


translate chinese day2_cards_3a30e832:


    me "是~是~"


translate chinese day2_cards_edd5c8c1:


    "我玩世不恭的表示同意。"


translate chinese day2_cards_dbd5c99b:


    mz "别到处拾乐！"


translate chinese day2_cards_5b122da1:


    "我没太明白她说的什么意思，不过算了。"


translate chinese day2_cards_78e0f168:


    "显然每个人都有他的弱点。"


translate chinese day2_cards_be072bc7:


    "没过一分钟我们都回到了食堂。"


translate chinese day2_cards_56cd1a43:


    "所有人都看着电子小子。"


translate chinese day2_cards_ff4ddd7c:


    el "那么..."


translate chinese day2_cards_f8f1459d:


    "他清清嗓子。"


translate chinese day2_cards_7ddd8bdc:


    el "每一回合比赛一次。"


translate chinese day2_cards_0de601df:


    el "如果平局，就重新比赛。"


translate chinese day2_cards_a323cae4:


    el "然后输家出局，赢家继续下一回合比赛。"


translate chinese day2_cards_69f4f055:


    el "既然报名的有..."


translate chinese day2_cards_f311836a:


    "他看看我。"


translate chinese day2_cards_e7dd93af:


    el "既然有8个选手，我们只会比赛3个回合。"


translate chinese day2_cards_e2f721ce:


    el "还有问题吗？"


translate chinese day2_cards_5e17549c:


    "人群开始欢呼。"


translate chinese day2_cards_e97014bc:


    us "奖品呢？奖品呢？"


translate chinese day2_cards_1fcf7c30:


    sl "乌里扬娜，停！"


translate chinese day2_cards_3a71b956:


    "斯拉维娅想要去抓住乌里扬卡。"


translate chinese day2_cards_2b37c513:


    us "拿不到奖之前我可不会停的。"


translate chinese day2_cards_038f6c96:


    "看起来这个小女孩一个人的能量就够跃迁到半人马座阿尔法星去。"


translate chinese day2_cards_fbd0629e_1:


    us "奖品！奖品！"


translate chinese day2_cards_fdbf4fb7:


    "她根本停不下来..."


translate chinese day2_cards_7d36e3c2:


    sl "打住！"


translate chinese day2_cards_09337077:


    "斯拉维娅想要说服她。"


translate chinese day2_cards_313ef528:


    "电子小子好像要晕过去了。"


translate chinese day2_cards_6c19d524:


    me "咱们还是赶快开始吧。"


translate chinese day2_cards_fc1804fc:


    "我从容不迫地说道，同时还送给乌里扬卡一句："


translate chinese day2_cards_f0d862c6:


    me "否则你什么奖品也得不到呦~"


translate chinese day2_cards_937ae63e:


    "我的办法似乎奏效了，她去找自己的位置了。"


translate chinese day2_cards_739a28dd:


    "斯拉维娅跟着她走过，给了我一个感谢的笑容。"


translate chinese day2_cards_ea7ae555:


    "少先队员们终于都安静下来了。"


translate chinese day2_cards_34834de5:


    "我走向列娜面前的桌子。"


translate chinese day2_cards_f1ce5d14:


    me "不介意吧？"


translate chinese day2_cards_78e5e8c4:


    "她抬头看着脸红了。"


translate chinese day2_cards_f9967eb5:


    me "别担心，我自己也不知道规则。"


translate chinese day2_cards_4752f785:


    th "我怎么知道是不是‘也’呢..."


translate chinese day2_cards_04d910fd:


    "我坐了下来。"


translate chinese day2_cards_569cc0a5:


    me "结果咱们要比赛第一回合了呢。"


translate chinese day2_cards_f1be5fed:


    un "是的。"


translate chinese day2_cardgame_ea01d133:


    "终于电子小子开始解释规则了。"


translate chinese demo_play_intro_accc2d8a:


    el "仔细看看纸牌。"


translate chinese demo_play_intro_5fa54f4c:


    el "你的面前总共有6张。"


translate chinese demo_play_intro_ccdb8a86:


    th "我希望大家都会数数。"


translate chinese demo_play_intro_283d409f:


    el "现在大家观察观察。"


translate chinese demo_play_intro_5bf90d15:


    "每个人都看过之后，电子小子继续。"


translate chinese demo_play_intro_fb1ad002:


    el "规则和扑克很像。"


translate chinese demo_play_intro_c3658729:


    el "大家应该都知道吧？"


translate chinese demo_play_intro_b18e73e6:


    "我知道规则，不过其他人..."


translate chinese demo_play_intro_5d5160d3:


    el "首先是一张最高的，然后是一对，然后是两对，三对...{w}依此类推，不带顺子。"


translate chinese demo_play_intro_66ab73d4:


    el "第一轮你从对手的牌里选一张拿走。"


translate chinese demo_play_intro_dbcd0300:


    el "你的对手这时可以有两次机会交换自己的牌。"


translate chinese demo_play_intro_d3a6fb9a:


    el "如果不在意的话，他也可以不换。"


translate chinese demo_play_intro_faea44e8:


    el "这里注意对手可以看见被交换的是哪两张牌。"


translate chinese demo_play_intro_d5e89419:


    el "下一步，对手再选一张自己想要的牌。"


translate chinese demo_play_intro_ee59bb67:


    el "如此这般，我想我说的够清楚了吧。"


translate chinese demo_play_intro_3c934234:


    "我好像不太明白。"


translate chinese demo_play_intro_135cb9f4:


    us "我说，爱因斯坦！"


translate chinese demo_play_intro_5b29bd77:


    "乌里扬卡从一边喊道。"


translate chinese demo_play_intro_6cf11f92:


    us "我完全没听懂！"


translate chinese demo_play_intro_3affc0c2:


    el "你一会儿玩的时候就明白了。"


translate chinese demo_play_intro_8dd13e36:


    "电子小子走向摆着表格的桌子，只剩下乌里扬卡独自在风中燃烧。"


translate chinese demo_play_intro_ce1458ea:


    me "你先。"


translate chinese demo_play_intro_881ff0c4:


    "希望我能理解的够快。"


translate chinese demo_play_intro_3dc8ac41:


    "列娜，比平常还要迷惑的样子，伸手指向一张牌。"


translate chinese demo_play_me_defend_1_5bbc93a2:


    "在桌子中间她就停了下来。"


translate chinese demo_play_me_defend_1_c4c9ab6d:


    un "你要..."


translate chinese demo_play_me_defend_1_0f88ba77:


    th "哦，对了，我得保护我的牌。"


translate chinese demo_play_me_defend_1_9592b4e5:


    th "电子小子刚才说的什么来着？"


translate chinese demo_play_me_defend_1_7f329a7a:


    "为了迷惑对手我可以交换纸牌，{w}两次。"


translate chinese demo_play_me_defend_1_30d81d36:


    "我也可以跳过。"


translate chinese demo_play_me_defend_1_a9691ffc:


    "那么我要不要护牌呢？"


translate chinese demo_play_me_defend_1_12313e8c:


    "我也可以直接把她想要的牌给她。"


translate chinese demo_play_me_defend_1_3f7d9aa4:


    "否则列娜也可以改变主意，{w}或者，也可以不改。"


translate chinese demo_play_me_select_1_2c550258:


    "大概有点眉目了，{w}或者至少，稍稍明白一些..."


translate chinese demo_play_me_select_1_ab26be30:


    me "现在轮到我了。"


translate chinese demo_play_me_select_1_6a346d80:


    "我可以把卡片拿回来，也可以选择另一张。"


translate chinese demo_play_rival_defend_10293eac:


    "列娜也可以保护她的纸牌。"


translate chinese demo_play_rival_defend_092d114b:


    "不过只要我仔细看，就可以无视她的防御。"


translate chinese demo_play_after_loop_a4fc9855:


    "我成功了！"


translate chinese day_2_cards_continue_b8733e71:


    "电子小子默默的观察着我们的游戏，并且点头示意。"


translate chinese day_2_cards_continue_f37b7ba7:


    "我们似乎有了一点进展了。"


translate chinese day_2_cards_continue_a335157b:


    el "现在，一局里选手们一共交换三次纸牌，盯紧对手的眼睛，用眼睛射穿他。"


translate chinese day_2_cards_continue_b27ea81d:


    "我笑了起来，‘射穿’......"


translate chinese day_2_cards_continue_ae91c5b2:


    el "你笑什么？"


translate chinese day_2_cards_continue_055701c5:


    me "没，没什么。"


translate chinese day_2_cards_continue_394b559a:


    "我试图保持严肃。"


translate chinese day_2_cards_continue_aa08701b:


    "他盯了我一会儿然后继续。"


translate chinese day_2_cards_continue_3e03a63e:


    el "然后我们亮出牌比比谁的大。"


translate chinese day_2_cards_continue_f0cced6b:


    "电子小子回到了他的表格那里。"


translate chinese un_play_draw_2c705b13:


    el "平局！你们应该再比赛一次！"


translate chinese un_play_win_5df79197:


    "我赢啦！"


translate chinese un_play_win_90577728:


    "说真的，玩这种现编出来的还不是自己编的游戏能赢可是不容易啊。"


translate chinese un_play_win_4dee3b34:


    "但是我赢了！"


translate chinese un_play_win_0b9b056b:


    "虽然说一想到代价是列娜输掉了游戏感觉又变了味儿。"


translate chinese un_play_win_8d4faa34:


    "本来她就不是很自信，现在又输了。"


translate chinese un_play_win_d7bb4c00:


    th "我现在都不好意思看她了。"


translate chinese un_play_win_3f0a42a8:


    "也许我应该故意输掉给她一点自信的说。"


translate chinese un_play_win_08d9c222:


    th "不过我和阿丽夏打了赌..."


translate chinese un_play_win_442b0ff1:


    "M同一时间，电子小子骄傲的宣布第一回合结束。"


translate chinese un_play_win_4c17c888:


    "坚持到第二回合的同学们出现在了列表上。"


translate chinese un_play_win_6ece79aa:


    "半决赛是阿丽夏对阵热尼娅以及乌里扬娜对阵..."


translate chinese un_play_win_aac891f4:


    "我。"


translate chinese un_play_win_482610ae:


    "我为自己灭亡的命运叹了口气。"


translate chinese un_play_win_3e9272c6:


    "乌里扬娜马上在我面前找了个座位！"


translate chinese un_play_win_9d897d0d:


    us "嘿！"


translate chinese un_play_win_2ed6127e:


    "她盯着我笑了起来。"


translate chinese un_play_win_9e5e59e8:


    us "你是怎么样淘汰掉列娜的？"


translate chinese un_play_win_69120a2e:


    us "估计是作弊了吧？"


translate chinese un_play_win_140619d9:


    me "我又不是你。"


translate chinese un_play_win_4f2b481d:


    me "我只是会玩儿罢了。"


translate chinese un_play_win_14e9ade2:


    th "至少这件事得让她弄清楚了。"


translate chinese un_play_win_dd90f754:


    me "那么你是怎么赢了舒里克的呢？"


translate chinese un_play_win_4dc5e73c:


    us "呃..."


translate chinese un_play_win_b7c182de:


    "乌里扬卡挥了挥手，好像很简单似的。"


translate chinese un_play_win_e22a379f:


    us "我威胁要加入他的社团。"


translate chinese un_play_win_dc47b4bb:


    "她又笑了。"


translate chinese un_play_win_da4c608f:


    us "你是不是也要让一让我呀？"


translate chinese un_play_win_9d811be6:


    me "没门儿！"


translate chinese un_play_win_35ba093a:


    us "哈..."


translate chinese un_play_win_502323d6:


    us "那我要自己选择给你哪张牌。"


translate chinese un_play_win_4998c00d:


    me "你到底听没听规则啊？"


translate chinese un_play_win_7625dabe:


    us "我才不管什么鬼规则！"


translate chinese un_play_win_ff6428f7:


    "她看上去完全不在乎。"


translate chinese un_play_win_7a4f4560:


    me "可以，不过我也要选择给你的牌。"


translate chinese un_play_win_0a1ee1fc:


    us "成交！"


translate chinese un_play_win_7ddb7c7e:


    "经历了第一局的胜利，我决定这次孤注一掷走一步险棋。"


translate chinese un_play_win_fa27d833:


    "我本来可以举报什么的，不过我有一种这次要大获全胜的感觉。"


translate chinese un_play_win_09a416bb:


    "我们是打破规则了，不过反正我们俩在一条船上。"


translate chinese un_play_win_05c369b7:


    "我抬头看看电子小子。"


translate chinese un_play_win_d0aaf07c:


    el "半决赛现在开始！"


translate chinese un_play_win_8ce591ef:


    "他发出号令。"


translate chinese un_play_win_352aee2b:


    "我谨慎的检查我的纸牌，确定乌里扬卡没有看到。"


translate chinese us_play_me_defend_2_7232aa89:


    us "嘿，你别晃来晃去的，我看不清楚了！"


translate chinese us_play_me_defend_2_25579ef8:


    "哼嗯..."


translate chinese us_play_draw_2c705b13:


    el "平局！再赛一次！"


translate chinese us_play_win_4c353fb8:


    us "我说！这不公平！"


translate chinese us_play_win_49d03e3a:


    us "你应该让我赢！"


translate chinese us_play_win_fb0127bc:


    "她鼓起脸愤怒的喘着粗气。"


translate chinese us_play_win_6905d85b:


    us "咱们再比一次，不过这次你必须让我赢！"


translate chinese us_play_win_4782a874:


    "整个屋子里的人都听见了。"


translate chinese us_play_win_2286cffa:


    "连电子小子都听见了。"


translate chinese us_play_win_0e9c7f85:


    el "禁止重赛！"


translate chinese us_play_win_2f715ea7:


    "乌里扬娜完全无视他。"


translate chinese us_play_win_615b52f1:


    us "你必须输！！"


translate chinese us_play_win_f6ec2b26:


    me "我可一点不打算跟你再赛一场。"


translate chinese us_play_win_498ef814:


    "我冷静地说道。"


translate chinese us_play_win_a7075cc6:


    us "好吧，那你是想用那种方式？"


translate chinese us_play_win_7c6b0a82:


    me "对，那种方式。"


translate chinese us_play_win_f60b0b79:


    us "那我就散布你骚扰阿丽夏的谣言..."


translate chinese us_play_win_e4705df4:


    "她和我悄悄说到。"


translate chinese us_play_win_32b78b25:


    me "啥米？！"


translate chinese us_play_win_50dfdc11:


    "我靠过去给她一个威胁的眼神。"


translate chinese us_play_win_e2a63898:


    me "所以说你偷听来着？"


translate chinese us_play_win_56390f46:


    us "我只是路过正好听到而已。"


translate chinese us_play_win_6d4b54e2:


    "总之，再赛一场的结果要好一点..."


translate chinese us_play_win_7189cb82:


    th "那种事她肯定干得出来！"


translate chinese us_play_win_0e589c83:


    "我叹了叹气告诉电子小子。"


translate chinese us_play_win_e5983d15:


    me "没有关系，我们可以迅速再比赛一次。"


translate chinese us_play_win_0c8d9dc7:


    el "好吧，你们随便..."


translate chinese us_play_win_d656c5e0:


    "他耸耸肩。"


translate chinese us_play_win_a5e9e043:


    "那么，重赛开始！"


translate chinese us2_play_draw_2c705b13:


    el "平局！你们应该再比赛一次！"


translate chinese us2_play_win_31467604:


    me "小意思。"


translate chinese us2_play_win_0f831176:


    "我一边说着一边斜挎在椅子上。"


translate chinese us2_play_win_57a622de:


    us "这不公平！"


translate chinese us2_play_win_8f3148e1:


    th "希望她不会再要求一次重新比赛！"


translate chinese us2_play_win_dd53341d:


    me "唉，何必呢~~"


translate chinese us2_play_win_e98bfbde:


    "我笑了起来。"


translate chinese us2_play_win_08916ca6:


    us "好吧..."


translate chinese us2_play_win_68edce41:


    "乌里扬娜愤怒的离开了桌子。"


translate chinese us2_play_win_aec6e03a:


    "我仔细研究赛程表，看看下一个和我比赛的是谁。"


translate chinese us2_play_win_1afc5d06:


    "同时阿丽夏也来看了。"


translate chinese us2_play_win_6286b0f8:


    "我假装着傻笑了几声。"


translate chinese us2_play_win_ad1e84f8:


    th "看起来好像我害怕她似的！"


translate chinese us2_play_win_d29e64cc:


    me "恭喜恭喜。"


translate chinese us2_play_win_bb61fa08:


    dv "你会因为胆小推出而后悔的..."


translate chinese us2_play_win_b7a1e7d5:


    th "我已经后悔了..."


translate chinese us2_play_win_a18f9993:


    "不过不是胆小的问题，我是后悔参加这个脑残的游戏。"


translate chinese us2_play_win_c3ff6c62:


    dv "想赢吗？"


translate chinese us2_play_win_58f07f35:


    me "我还等着你兑现自己的诺言呢。"


translate chinese us2_play_win_7807add1:


    dv "好吧，来呀！"


translate chinese dv_play_draw_2c705b13:


    el "平局！你们应该再比赛一次！"


translate chinese day2_main3_84842450:


    "怎么讲输了都不舒服..."


translate chinese day2_main3_c9e7d2f7:


    "至少我还赢过一轮。"


translate chinese day2_main3_1f69a49c:


    "我输给了朵切芙斯卡娅。"


translate chinese day2_main3_e25a1002:


    th "而那是最悲剧的。"


translate chinese day2_main3_183277f1:


    th "明天早上我才能知道她到底干出了什么事情。"


translate chinese day2_main3_00105f86:


    th "她可以在集合的时候捣乱（如果我没有睡过的话）或者是去找奥尔加·德米特里耶夫娜抱怨。"


translate chinese day2_main3_d73b14bb:


    th "她可以在整个营地散布谣言。"


translate chinese day2_main3_d7329552:


    th "最糟糕的事情是大家都只相信她而不相信我。"


translate chinese day2_main3_cc371952:


    "我说不清为什么，不过我100%%确定肯定会这样。"


translate chinese day2_main3_4cda6c6c:


    th "而且这和我打不打赌没有关系，只看我到底赢没赢。"


translate chinese day2_main3_bac15b3e:


    th "如果这是阿丽夏，那么我的下场恐怕只有一种了。"


translate chinese day2_main3_d20a7c98:


    th "明天她和奥尔加·德米特里耶夫娜一说，我就成了大家的笑柄。"


translate chinese day2_main3_3e52af3c:


    th "或者一夜之间传出绯闻。"


translate chinese day2_main3_e1a0fa0c:


    th "最糟糕的事情是大家都只相信她而不相信我。{w}我说不清为什么，不过我100%%确定肯定会这样。"


translate chinese day2_main3_a8cecdb4:


    "我离开了食堂。"


translate chinese day2_main3_f9f946ef:


    "现在睡觉还显得太早了，出去散散步是个不错的主意。"


translate chinese day2_main3_b3b9bfb2:


    th "我应该去哪儿呢？"


translate chinese day2_aidpost_eve_dad1f28b:


    "我就这样跌跌撞撞的溜达到了医务室门前。"


translate chinese day2_aidpost_eve_1b18dca2:


    "即使我需要看医生估计也是心理医生，现在来找我们的护士好像完全说不通啊。"


translate chinese day2_aidpost_eve_a20cefa7:


    "..."


translate chinese day2_square_eve_8c430c9f:


    "我来到了广场前找了个位置坐下。"


translate chinese day2_square_eve_dd9e540e:


    "对着Genda的雕像愣神。"


translate chinese day2_square_eve_a20cefa7:


    "..."


translate chinese day2_beach_eve_4ae280fe:


    "我来到了沙滩。"


translate chinese day2_beach_eve_7700f3ad:


    "我现在心情不佳，不怎么想去游泳，不过我还是来到了水边用手拍了拍水花。"


translate chinese day2_beach_eve_691e5417:


    "水是温温的。"


translate chinese day2_beach_eve_5dc0ba05:


    "似乎在经过一整天的酷热之后还在逐渐降温的过程中。"


translate chinese day2_beach_eve_06f63b4a:


    th "嗯...也许以后有机会我再来游泳吧。"


translate chinese day2_beach_eve_a20cefa7:


    "..."


translate chinese day2_dock_eve_ee198286:


    "我决定到码头那里去。"


translate chinese day2_dock_eve_c4425934:


    "太阳还在地平线上，映出粼粼波光，远处的河流映出红与橙，一片朦朦胧胧。"


translate chinese day2_dock_eve_8572e4d2:


    "水面像是一堆烈火，我盯着看了一会儿，直到那些颜色都逐渐退去。"


translate chinese day2_dock_eve_a20cefa7:


    "..."


translate chinese day2_busstop_eve_62cce665:


    "这几天的事情不断在我的头脑中闪现：让人无语的形式主义清单，垃圾的纸牌比赛..."


translate chinese day2_busstop_eve_73a11ef4:


    "我现在不想说话，不想和谁聊天，我都不想再琢磨我现在的处境。"


translate chinese day2_busstop_eve_6f392ba3:


    "我来到广场找了一个座位，望着Genda的雕塑发着呆。"


translate chinese day2_busstop_eve_a20cefa7:


    "..."


translate chinese day2_busstop_eve_58e66c22:


    "我不知道在这里呆了多久，不过终于还是被蛐蛐的鸣叫带回了现实。"


translate chinese day2_busstop_eve_c4f332c1:


    "我站起身，盲目的瞎逛着。"


translate chinese day2_busstop_eve_f17e0aeb:


    th "公共汽车站..."


translate chinese day2_busstop_eve_301f4821:


    "不知怎么，我走来走去来到了这个地方。"


translate chinese day2_busstop_eve_31ee41ff:


    th "也许潜意识里我一直觉得会有公交车来把我接回现实世界？"


translate chinese day2_busstop_eve_b99b8886:


    th "非常不现实。"


translate chinese day2_busstop_eve_74d34bfa:


    th "反过来说，其实也不是不行？"


translate chinese day2_busstop_eve_39c07ffa:


    "已经暗下来了。"


translate chinese day2_busstop_eve_5daf4589:


    "我只是站在这里看着满天繁星。"


translate chinese day2_busstop_eve_4bb46255:


    "天文学从来没有像宇航学那样吸引我。"


translate chinese day2_busstop_eve_b9df3f0f:


    "最有趣的还是欣赏艺术作品中遥远的星座，星云，和星系，而不是费劲算什么角速度和恒星的质量什么的。"


translate chinese day2_busstop_eve_f721c30a:


    "但是，当然我将能够找到的大熊星座的。"


translate chinese day2_busstop_eve_011b7220:


    "但是如果我在林海雪原里迷了路，唯一可以帮助我走出去的就是生长在树木北侧的苔藓。"


translate chinese day2_busstop_eve_252aa5da:


    "然而，我觉得那样还是不能让我走出森林..."


translate chinese day2_busstop_eve_ffa2f154:


    "过了几分钟我返回了营地。"


translate chinese day2_busstop_eve_a20cefa7_1:


    "..."


translate chinese day2_stage_eve_2c57c848:


    "我决定往北边走，或者至少是我觉得是北边的方向上走。"


translate chinese day2_stage_eve_ac512323:


    "过了几分钟，我远远的看见了一个音乐厅，几排木制的观众席以及一个类似的木制的舞台。"


translate chinese day2_stage_eve_0bd0ce88:


    "我爬上了舞台。"


translate chinese day2_stage_eve_ff3859bd:


    "好多各种各样的音乐装备：扩音器、麦克风的架子甚至还有一台钢琴。"


translate chinese day2_stage_eve_85b7add9:


    "我想象着庞大的观众群呼喊着我的名字，我的眼睛被灯光照得难受。"


translate chinese day2_stage_eve_aa3b726d:


    "想象着手里有一把吉他，表演一段震撼人心的独奏。"


translate chinese day2_stage_eve_7fe7938c:


    "我估计从陌生人的角度看一定很奇怪，一个疯子在台上挥舞着胳膊，像猿人一样蹦来蹦去挤眉弄眼。"


translate chinese day2_stage_eve_dc4028dc:


    th "我希望没人在这里看见我！"


translate chinese day2_stage_eve_a44729f3:


    "想到这个，我赶紧跳了下来匆忙逃跑。"


translate chinese day2_stage_eve_a20cefa7:


    "..."


translate chinese day2_football_eve_bd44db9a:


    "现在踢足球的话太晚了所以球场已经空了。"


translate chinese day2_football_eve_2b9a9e9a:


    "我在此地驻足几分钟，聆听大自然的寂静，发觉甚是无趣，于是转身返程。"


translate chinese day2_football_eve_a20cefa7:


    "..."


translate chinese day2_dv_a4dc0a8d:


    "乌拉~~我赢啦！这下阿丽夏该知道这是谁的地盘了吧！"


translate chinese day2_dv_e284eef4:


    th "我和她打的赌没有白费！"


translate chinese day2_dv_6d9b3054:


    "现在我唯一担心的是阿丽夏会不会因为失败恼羞成怒转而散布谣言。"


translate chinese day2_dv_c21f93b9:


    "我突然有了要庆祝的想法，想要到沙滩那边去戏水。"


translate chinese day2_dv_32525770:


    "说实话我不知道怎么游泳，不过在月光下跳水好像挺诱人的。"


translate chinese day2_dv_e75dca33:


    "我昨天穿了一整天的冬装，都可以直接拧出水来了。"


translate chinese day2_dv_1c3ad5c8:


    "晚上应该没人来这里，所以我脱光衣服只剩一件内裤跳进水里。"


translate chinese day2_dv_90e7fd64:


    th "早知道我就带着泳裤来了。"


translate chinese day2_dv_affe5cb3:


    "平常我也就游个十五二十米，不过今天胜利的喜悦让我想要打破个人纪录。"


translate chinese day2_dv_df18f64c:


    "我游的十分优雅，我欣赏着自己的泳姿，自如地控制着自己的呼吸。"


translate chinese day2_dv_620aa5dd:


    "突然，‘咚！！！’"


translate chinese day2_dv_de055afa:


    "我被一记重击砸向水中。"


translate chinese day2_dv_33564eb7:


    "我开始溺水不过还是设法抓住了一个浮标。"


translate chinese day2_dv_8fc9d4bf:


    "我回头看见了阿丽夏在追着我。"


translate chinese day2_dv_33864d31:


    me "你搞毛啊？"


translate chinese day2_dv_c3a057d6:


    dv "我在干什么？当然是向胜利者致敬啊！"


translate chinese day2_dv_d6c4be35:


    me "那我要是淹死怎么办啊？！"


translate chinese day2_dv_f20a4a5d:


    dv "这个嘛，我会救你的。"


translate chinese day2_dv_8386bb65:


    me "哈...那是当然..."


translate chinese day2_dv_6409b342:


    "呆在这里太危险了，她任何时候都有可能再来暗算我。我用上全身的力气想要往岸边游。"


translate chinese day2_dv_556fac3c:


    "我开始逐渐的回复自己的体力。"


translate chinese day2_dv_f577f3f7:


    "过了一会儿阿丽夏也从水中出来了。"


translate chinese day2_dv_d82da8ae:


    dv "话说，你还游的不错！"


translate chinese day2_dv_869a64b8:


    th "我可不觉得。"


translate chinese day2_dv_4a35be24:


    me "你也是。"


translate chinese day2_dv_64dd4f95:


    dv "那不是显然的吗？"


translate chinese day2_dv_cbc4bb84:


    "我没说话。"


translate chinese day2_dv_9a5b6429:


    dv "你今天赢了我两次。{w}所以抵消你欠我的两次。"


translate chinese day2_dv_ff2a842c:


    th "什么时候欠她的啊？"


translate chinese day2_dv_7d05db3b:


    "阿丽夏好像在认清现实方面有点障碍。"


translate chinese day2_dv_493885e5:


    me "多谢了..."


translate chinese day2_dv_27c62366:


    "我讽刺的说。"


translate chinese day2_dv_01820ffe:


    dv "看来你不像是我原来认为的那种loser..."


translate chinese day2_dv_5b1b7a36:


    "阿丽夏穿的泳装展现了她完美的身材。"


translate chinese day2_dv_84e41e3b:


    "虽然她的缺点不少，不过这个优点也是可以抵消一大部分的诶。"


translate chinese day2_dv_c6ee82bf:


    me "我什么时候变成了loser了？"


translate chinese day2_dv_945c59bc:


    "她讽刺的笑了。"


translate chinese day2_dv_3bbe4c98:


    dv "怎么？你不是？"


translate chinese day2_dv_f837dd45:


    me "当然不是了嘛！"


translate chinese day2_dv_072e019b:


    dv "你怎么证明啊？"


translate chinese day2_dv_fdf9f2c2:


    me "我不用证明什么嘛！"


translate chinese day2_dv_a3b5accf:


    dv "哇，原来如此啊。"


translate chinese day2_dv_516018de:


    "她毫无怒气。"


translate chinese day2_dv_e7ec8520:


    me "是啊，就是如此的..."


translate chinese day2_dv_4a405aad:


    "一阵寂静"


translate chinese day2_dv_1c5b30ab:


    "一阵微风玩弄着落叶，把它们吹向海边又带回来。"


translate chinese day2_dv_00eafdba:


    "阿丽夏还在注视着我，不过看起来好像是，完全忘了我的存在似的。"


translate chinese day2_dv_c4f3bd04:


    me "你好啊，阿丽夏！"


translate chinese day2_dv_2d89497e:


    "她迷离的眼神马上又恢复了平常。"


translate chinese day2_dv_b63997f5:


    dv "总之，拜拜~"


translate chinese day2_dv_293bd21f:


    "她捡起衣服走了。"


translate chinese day2_dv_a20cefa7:


    "..."


translate chinese day2_dv_837271a8:


    "天色晚了不过我决定多呆一会儿，躺在地上看看星星。"


translate chinese day2_dv_95ca3d6a:


    "怎么说我过去也没什么机会这样。"


translate chinese day2_dv_8b8426ec:


    "或者我只是把每个机会都错过去了。"


translate chinese day2_dv_b48dade4:


    th "话说，遥远的星星发出的光线要经过几百万年才能到达我们的视线。..."


translate chinese day2_dv_5739d22f:


    th "我现在能看到的只不过是它们在遥远的过去的某一瞬间。"


translate chinese day2_dv_799ecc91:


    th "现在这个星星已经爆炸消失掉了吧..."


translate chinese day2_dv_1c004364:


    th "等一下！{w}她把我的衣服也拿走了!"


translate chinese day2_dv_bb8c1b2a:


    "我起来看看四周。"


translate chinese day2_dv_ec32c246:


    "绝对是这样没错，阿丽夏把我的少先队员衣服拿走了！"


translate chinese day2_dv_6ccc3e04:


    th "糟糕！"


translate chinese day2_dv_37751890:


    "我还刚刚开始想到她可能没有那么任性... "


translate chinese day2_dv_801f233a:


    th "我需要马上想一个主意。"


translate chinese day2_dv_1c7817b8:


    th "我肯定是可以去找奥尔加·德米特里耶夫娜抱怨一番，不过就穿着这件‘湿了吧唧的大裤衩’..."


translate chinese day2_dv_a11b7a5a:


    "我也不知道阿丽夏住在哪个屋子里。"


translate chinese day2_dv_9aedec39:


    th "而且挨家挨户去敲门可不是个好主意。"


translate chinese day2_dv_faa17736:


    th "也许我该去找斯拉维娅？"


translate chinese day2_dv_691cf254:


    th "哈...对，半夜穿着湿透的内裤...{w}我还应该叼着一只玫瑰花！"


translate chinese day2_dv_1388e5eb:


    "成何体统？！"


translate chinese day2_dv_a6749c0b:


    "总之我得干点什么。"


translate chinese day2_dv_6c982559:


    "我庆幸她走了还没多长时间，所以现在追..."


translate chinese day2_dv_9744eb3a:


    "...赶紧！"


translate chinese day2_dv_1e0acdbc:


    "一眨眼的功夫我就到了广场。"


translate chinese day2_dv_20bc07ef:


    "让我吃惊的是，阿丽夏就坐在长椅上，一副无所事事的样子。"


translate chinese day2_dv_0706b8a9:


    "她已经换了衣服。"


translate chinese day2_dv_679b46e1:


    me "还给我！"


translate chinese day2_dv_e5a0d224:


    dv "没问题啊，拿走吧..."


translate chinese day2_dv_1063c21e:


    "她碎碎念着把衣服还给了我。"


translate chinese day2_dv_51149e68:


    me "..."


translate chinese day2_dv_6cf682fe:


    dv "我可不是专门在这里等着你的哟..."


translate chinese day2_dv_a71ccb66:


    "阿丽夏马上起身回去了。"


translate chinese day2_dv_24bb116e:


    "我愣在原地。"


translate chinese day2_dv_a26ba0eb:


    "没有想到这样的发展。"


translate chinese day2_dv_b327337f:


    th "也许她良心发现了..."


translate chinese day2_dv_4e97c063:


    th "虽然有可能..."


translate chinese day2_dv_afa37a67:


    th "总之我还是离她远点比较好。"


translate chinese day2_dv_8b243a91:


    "我开始往回走。"


translate chinese day2_sl_51d58656:


    "我马上就要走到营地的时候突然听到大门后边有什么动静。"


translate chinese day2_sl_5c128ad7:


    th "这次又是谁...？"


translate chinese day2_sl_01f96367:


    "社团室那边好像有人沿着小路往森林里走去了。"


translate chinese day2_sl_d5b66cdc:


    "因为太暗我只能看见一个隐约的人影。"


translate chinese day2_sl_6368700c:


    "我很奇怪谁大晚上出来晃悠。"


translate chinese day2_sl_5f2c4088:


    th "一个不守规矩的少先队员？嘿嘿嘿..."


translate chinese day2_sl_bb1c83e5:


    "我谨慎的跟踪着那个人影。"


translate chinese day2_sl_707dc7b1:


    "我跟着那个人穿过树林，不过跟来跟去还是跟丢了。"


translate chinese day2_sl_7c11ada4:


    th "我是不是应该回去呢？"


translate chinese day2_sl_da7dcced:


    "树林在眼前分开了，森林中的一片湖水。"


translate chinese day2_sl_9ae5e62c:


    "我看到了斯拉维娅...{w}她轻轻跳着，挥舞着红领巾和衬衫。"


translate chinese day2_sl_e1672e9e:


    "这个情景比这个夏令营还要不可思议。"


translate chinese day2_sl_4b243d51:


    "斯拉维娅看起来是森林的精灵，或是一个仙女。"


translate chinese day2_sl_76f15cf3:


    "她看起来像是一个古希腊的女神。"


translate chinese day2_sl_0ad8db2d:


    "我开始想起来过去看过的所有神学理论。"


translate chinese day2_sl_efc6aadb:


    "这让我想起了泛神论，神是我们周围的一切。"


translate chinese day2_sl_df1b0b99:


    th "也许这不是外星人在作祟或是时间旅行而是天意？"


translate chinese day2_sl_9a178047:


    th "的确，斯拉维娅说过她喜欢自然。"


translate chinese day2_sl_4850f462:


    th "结果她也是个神秘人物？"


translate chinese day2_sl_3565ced7:


    "不过接下来她的衣服都滑落到了地上..."


translate chinese day2_sl_2dc5f1ec:


    "斯拉维娅走进水中，{w}一丝不挂的..."


translate chinese day2_sl_62969ec1:


    "斯拉维娅走进了水中。"


translate chinese day2_sl_3f9652c6:


    "我感觉很惭愧，可是却移不开自己的视线。"


translate chinese day2_sl_62af0d4c:


    "她沾满水珠的皮肤映着银色的月光，让斯拉维娅看起来越发的像是一座古希腊的雕塑了。{w}米洛斯的阿芙洛黛蒂，也许是？"


translate chinese day2_sl_9a56444a:


    "眼前的景象十分神圣，以至于让人不敢有肉体上的欲望，而只是对美的崇高赞颂。"


translate chinese day2_sl_88bcf51a:


    "我注视着斯拉维娅的完美无瑕，完全忘了其他的事情。"


translate chinese day2_sl_1275a4ec:


    "也许这不是地狱而是天堂？"


translate chinese day2_sl_fed951af:


    "我脚下的一根树枝突然不争气的折断了，斯拉维娅转过身，不过应该是看不见我的吧，{w}或者只是我美好的想象。"


translate chinese day2_sl_4f55ff6e:


    "她迅速出来换上了衣服离开了。"


translate chinese day2_sl_4c18ac16:


    "我悄悄地跟着她。"


translate chinese day2_sl_389d811b:


    "斯拉维娅悄悄地穿过树林，巧妙地避开突出的树枝与凹凸不平的地面。"


translate chinese day2_sl_1f1f282b:


    "想要跟上不容易，而且我也不想被抓到。第一偷看是部队的，第二她究竟在这里做什么现在还不清楚。"


translate chinese day2_sl_10a06639:


    "虽然这件事看起来还没有什么疑点—和我来到这个世界没有太大的关系。"


translate chinese day2_sl_8edf9ccd:


    "只是{i}清白{/i}。没什么值得调查的。"


translate chinese day2_sl_c2d72193:


    "我们最后来到了广场。"


translate chinese day2_sl_bfc4d100:


    "斯拉维娅停了下来，转过身看着我。"


translate chinese day2_sl_8f535089:


    sl "你觉得我没有发现你吗？"


translate chinese day2_sl_996aa1aa:


    "我有点迷糊，不过决定故作镇静。"


translate chinese day2_sl_a59596f3:


    me "有多长时间了？"


translate chinese day2_sl_ee6a8067:


    sl "我不太确定..."


translate chinese day2_sl_abc9e8c7:


    "斯拉维娅靠近我。"


translate chinese day2_sl_7a2e9449:


    sl "五分钟吧，大概。"


translate chinese day2_sl_c9c9cf9a:


    me "所以说，连湖水那里都？"


translate chinese day2_sl_44b1529f:


    sl "你在说什么湖啊？"


translate chinese day2_sl_c97fadc0:


    me "这个..."


translate chinese day2_sl_f0f0ce17:


    "斯拉维娅好像真的很吃惊，我不太确定她是在假装不知道还是..."


translate chinese day2_sl_67207812:


    me "算了，不管了。"


translate chinese day2_sl_8d771c48:


    "我决定要表现的绅士一些，什么也没有说。"


translate chinese day2_sl_af1d965e:


    sl "好吧。"


translate chinese day2_sl_b1dd558c:


    "没想到她这么简单的就同意了。"


translate chinese day2_sl_29a99ab6:


    sl "多么美好的夜晚啊。"


translate chinese day2_sl_532e90d7:


    "斯拉维娅坐在长椅上欣赏着夜空。"


translate chinese day2_sl_71dfbb36:


    me "我猜这样的夜晚在这个地方是很平常的事情？"


translate chinese day2_sl_19f9cb53:


    sl "嗯，大概是..."


translate chinese day2_sl_437776df:


    me "为什么不确定呢？"


translate chinese day2_sl_e064e531:


    sl "不知道，我有点想不清楚。"


translate chinese day2_sl_c233b6a2:


    me "想什么？"


translate chinese day2_sl_7ef301f9:


    "她看着我，好像我脸上有什么东西似的，不过一会儿之后又去看星星了。"


translate chinese day2_sl_c55641df:


    sl "有时我会在晚上突然心情特别好...{w}白天充满了工作和琐事，夜晚这里如此寂静。"


translate chinese day2_sl_da5951c8:


    sl "如果没有这些昆虫和鸟儿的鸣叫声，你会有一种和整个宇宙面对面的感觉。"


translate chinese day2_sl_6c369edd:


    "不知道为什么我觉得她不像是会说这么多话的人。"


translate chinese day2_sl_3229d777:


    me "至于我来说，这里实在太安静了。"


translate chinese day2_sl_5606d7d1:


    sl "对吗？"


translate chinese day2_sl_0c5e8544:


    me "是啊，有什么问题吗？"


translate chinese day2_sl_0f20c625:


    sl "嗯..."


translate chinese day2_sl_fcc29073:


    sl "好了！"


translate chinese day2_sl_91f795db:


    "她站起来快速整理了一下裙子。"


translate chinese day2_sl_411ed0f9:


    sl "该睡觉了。。"


translate chinese day2_sl_c3abce40:


    me "晚安！"


translate chinese day2_sl_ee02ee2d:


    "我看着她走远了。"


translate chinese day2_sl_91aa9e4f:


    "我们的对话好像没有任何实质的内容，不过{i}在这里{/i}却显得有很多深邃的含义，在斯拉维娅的旁边。"


translate chinese day2_sl_e881ac0f:


    "好像对我来说这样的寂静也是必要的。让我感觉和宇宙连为一体了。"


translate chinese day2_sl_fe349bbe:


    "我甚至想说是至关重要的—甚至是现在！"


translate chinese day2_sl_a20cefa7:


    "..."


translate chinese day2_sl_a20cefa7_1:


    "..."


translate chinese day2_sl_41f711f9:


    "我不知道在那里呆了多长时间，不过我开始有点困了。"


translate chinese day2_un_197a0aaf:


    "该走了。"


translate chinese day2_un_146e98d1:


    th "我怎么能第一局就输了呢！"


translate chinese day2_un_a3a64f91:


    "我没有理由。"


translate chinese day2_un_5e63525e:


    "这种时候运动场是适合一个人散散心的地方。"


translate chinese day2_un_df7b3cbc:


    th "谁想晚上摸黑踢球呢？"


translate chinese day2_un_579f8206:


    "我坐下来开始回忆刚才发生了什么事情。"


translate chinese day2_un_404f90fe:


    "突然我听见排球场那边有咔嚓咔嚓还是吹口哨的声音。"


translate chinese day2_un_a9cd9809:


    "我转过去发现有人在使劲挥着手。"


translate chinese day2_un_8464c860:


    th "他在和谁打手势啊？"


translate chinese day2_un_1e068335:


    "我惊讶地发现那是列娜。"


translate chinese day2_un_fec5d55a:


    "她把羽毛球扔到空中用力的拿拍子打上去。"


translate chinese day2_un_aa5dee50:


    "不过，说实话，这个，她羽毛球打的很糟糕。"


translate chinese day2_un_3355bd20:


    "我本来想多看一会儿，不过还是决定走上前去。"


translate chinese day2_un_24cfa746:


    "我绕着球场走为了让列娜可以看到我。"


translate chinese day2_un_8c20dbc2:


    "列娜想鹿一样腼腆，所以我决定不要再犯和上次一样的错误。"


translate chinese day2_un_1a8f3935:


    me "嗨！"


translate chinese day2_un_5a070b64:


    "她看了一眼我然后马上把球和牌子藏到了身后。"


translate chinese day2_un_9938563a:


    me "你喜欢打羽毛球吗？"


translate chinese day2_un_9c33c3aa:


    un "没有那么喜欢..."


translate chinese day2_un_dd278e82:


    me "你好像不太掌握要领啊，{w}需要我帮帮忙吗？"


translate chinese day2_un_319450df:


    "说实话我也不是太擅长，不过也大概能达到平均的水平。"


translate chinese day2_un_627e964d:


    me "我给你演示一下。"


translate chinese day2_un_626232a6:


    un "感谢。"


translate chinese day2_un_ac04f5d1:


    "她脸红了。"


translate chinese day2_un_3883f3ff:


    un "我想要加入羽毛球社团，不过好像没什么希望。"


translate chinese day2_un_aa50848d:


    un "本来我今天也不想来的，不过..."


translate chinese day2_un_418d4bde:


    "她抬头看了看我。"


translate chinese day2_un_90c16780:


    un "我玩牌一直不行，不过今天手气一直特别好，所以我想试试羽毛球是不是也能有一样的效果。..."


translate chinese day2_un_7d6095b6:


    "我听到这些话更觉得输给列娜实在是太丢人了。"


translate chinese day2_un_2a1db9ac:


    me "我没想到你对体育运动还挺感兴趣的？"


translate chinese day2_un_a852076f:


    "她又脸红了。"


translate chinese day2_un_0f6c5c86:


    me "哇，抱歉...{w}来吧，我给你演示一下！"


translate chinese day2_un_3d8ebca6:


    "我拿起拍子，将球抛到空中，然后..."


translate chinese day2_un_b356ae1f:


    "用上全身的力气，奋力一击，结果那球一下飞出了围墙快要到森林里去了。"


translate chinese day2_un_9b63169a:


    me "哎呀，抱歉！"


translate chinese day2_un_f1725684:


    "哇，我自己都没想到。"


translate chinese day2_un_5d87ca29:


    un "没关系...{w}虽然说，那是最后一个了..."


translate chinese day2_un_c4393a9f:


    me "最后一个？咱们去找找吧！"


translate chinese day2_un_9d020e39:


    un "还是别去了...{w}森林里...有..."


translate chinese day2_un_d5d9bec0:


    me "有什么？精灵吗？"


translate chinese day2_un_7c8e7385:


    "我笑出了声。"


translate chinese day2_un_8a2da24a:


    un "有可能..."


translate chinese day2_un_8def79fc:


    "好像只有我在笑个不停。"


translate chinese day2_un_b96a2bcb:


    me "没什么可害怕的，来吧！"


translate chinese day2_un_5806cffb:


    un "嗯，如果你陪着我的话..."


translate chinese day2_un_d8c460b7:


    "我们离开了运动场，我开始仔细检查树木。"


translate chinese day2_un_965c33d2:


    "突然间，猫头鹰的一声鸣叫撕开了黑夜的寂静。"


translate chinese day2_un_e9ba05f1:


    "列娜很害怕，她双手紧紧的抓住我，好像在拥抱一样。"


translate chinese day2_un_8bfe647b:


    "好奇怪的感觉。"


translate chinese day2_un_827560fe:


    "如此近距离的感受到一个女孩儿的体温。"


translate chinese day2_un_12cdb438:


    "我心中也感受到一阵温暖。"


translate chinese day2_un_2b91a3d1:


    "我突然有了一种想要保护她的欲望，即使只是面对一只猫头鹰。"


translate chinese day2_un_5e49a6dc:


    "我唯一的愿望就是让现在的拥抱永远持续下去。"


translate chinese day2_un_a89add5c:


    "可是一切终将结束。"


translate chinese day2_un_6c0ae31e:


    "过了一会儿我发现那只猫头鹰在不远处的树枝上对我们叫着。"


translate chinese day2_un_12d3ce3e:


    "它紧紧的抓着那个球。"


translate chinese day2_un_f751a0a0:


    me "你很害怕吗？"


translate chinese day2_un_0905212f:


    un "呜..."


translate chinese day2_un_69e96144:


    me "你看呀！这完全不吓人嘛！"


translate chinese day2_un_f200dfd5:


    "列娜从我背后探出头来。"


translate chinese day2_un_6543e2da:


    un "嗯..."


translate chinese day2_un_efd93d7c:


    me "哇，等一下。"


translate chinese day2_un_ae039cd0:


    "我轻轻的离开了她的怀抱，慢慢接近猫头鹰。"


translate chinese day2_un_2aa9b5ff:


    "开始它好像害怕似的飞起来丢掉了羽毛球。"


translate chinese day2_un_d348cee1:


    "不过还是落在枝头。"


translate chinese day2_un_1ba95990:


    "我成功拿到了球，小心翼翼的把它从鸟的身旁拿走。"


translate chinese day2_un_56edd61c:


    me "看，它挺温顺的！{w}你想不想养它呢？"


translate chinese day2_un_db78f5e4:


    un "也许，下一次...？"


translate chinese day2_un_d01f4370:


    "我把羽毛球递给列娜。"


translate chinese day2_un_3cd7a073:


    un "谢谢。"


translate chinese day2_un_783cf09d:


    "她轻轻的笑了。"


translate chinese day2_un_4d93b5df:


    un "我得走了。"


translate chinese day2_un_0f243eb9:


    me "羽毛球加油喔~"


translate chinese day2_un_f5e7f5a8:


    "列娜又一次笑了起来，匆匆返回营地。"


translate chinese day2_un_4bf89e3c:


    th "她怎么这么可爱啊？！"


translate chinese day2_un_a20cefa7:


    "..."


translate chinese day2_us_ce80d80d:


    "过去的一天的事情不断在我眼前闪现：那份愚蠢的毫无意义的清单，还有那个蛋疼的比赛..."


translate chinese day2_us_5cef682c:


    "今晚我什么也不想干，也不想和谁说话。{w}我现在再也不想调查我的身世还是这个世界了。"


translate chinese day2_us_d9b0b0f8:


    "我往北边走去。{w}至少我觉得那边是北。"


translate chinese day2_us_d66455f1:


    "我从过去就一直有这个习惯——往北边走。"


translate chinese day2_us_a488b614:


    "在我的家乡我也是喜欢北边的几个区，胜过几个南部区。"


translate chinese day2_us_1759ce8e:


    "我也不喜欢去黑海的疗养院什么的，无边无际的森林和田野比沙滩要诱人得多。"


translate chinese day2_us_ac512323:


    "过了几分钟，我远远的看见了一个音乐厅，几排木制的观众席以及一个类似的木制的舞台。"


translate chinese day2_us_0bd0ce88:


    "我爬上了舞台。"


translate chinese day2_us_ff3859bd:


    "好多各种各样的音乐装备：扩音器、麦克风的架子甚至还有一台钢琴。"


translate chinese day2_us_85b7add9:


    "我想象着庞大的观众群呼喊着我的名字，我的眼睛被灯光照得难受。"


translate chinese day2_us_aa3b726d:


    "想象着手里有一把吉他，表演一段震撼人心的独奏。"


translate chinese day2_us_7fe7938c:


    "我估计从陌生人的角度看一定很奇怪，一个疯子在台上挥舞着胳膊，像猿人一样蹦来蹦去挤眉弄眼。"


translate chinese day2_us_dc4028dc:


    th "我希望没人在这里看见我！"


translate chinese day2_us_fa3c839e:


    us "嗨！"


translate chinese day2_us_b65b6fe7:


    "声音从上方传来。"


translate chinese day2_us_997dd5cf:


    "我向上看去，发现乌里扬卡正挂在屋顶的房梁上。"


translate chinese day2_us_89f814b5:


    us "你在这里干什么呢？"


translate chinese day2_us_2ec2a349:


    me "我只是..."


translate chinese day2_us_dab45ac8:


    "否认只是徒劳。"


translate chinese day2_us_cfae0a9c:


    me "你看见了，是不是？"


translate chinese day2_us_7217c832:


    "我沮丧地说，并且转过身去。"


translate chinese day2_us_a7445561:


    us "噢，我看到了你身上被浪费掉的吉他天赋。"


translate chinese day2_us_8f216cfd:


    "我没有说话。"


translate chinese day2_us_57071005:


    us "别皱眉头啊，那多搞笑啊！"


translate chinese day2_us_20f32dda:


    "她咯咯的笑了起来。"


translate chinese day2_us_d0b077c3:


    me "搞笑，是吧？"


translate chinese day2_us_2ff94fc7:


    "我怒了。"


translate chinese day2_us_df6dbf74:


    us "是啊。"


translate chinese day2_us_69b6d036:


    "乌里扬娜冷静的回答道。"


translate chinese day2_us_400c9ff0:


    us "上来呀，伙计。"


translate chinese day2_us_25f7abbc:


    me "上哪里？"


translate chinese day2_us_8b888dc6:


    us "来我这里啊！"


translate chinese day2_us_ae5dfee6:


    me "我才不会去那里！不要尝试说服我！"


translate chinese day2_us_39336f89:


    "不是说我恐高，而是爬那么高到那里完全没有意义嘛！"


translate chinese day2_us_053e61c9:


    us "别啊，只是到这里而已嘛。"


translate chinese day2_us_45d1c061:


    "我觉得自己骨子里有什么地方出错了，可我还是慢慢的往她那边爬过去。"


translate chinese day2_us_d926232c:


    "当我爬到了乌里扬卡下方的时候，她突然喊了起来："


translate chinese day2_us_5736d39d:


    us "接住我！"


translate chinese day2_us_b2c5ecfe:


    "然后就跳了下来..."


translate chinese day2_us_b9b5fa03:


    "我的头脑中瞬间闪现过无数想法。"


translate chinese day2_us_20495ccc:


    th "我怎么接住她？值得尝试吗？她要是死掉了怎么办？她要是砸坏了我的某个零件怎么办？为什么这种事要发生在我的身上？！"


translate chinese day2_us_614bbca7:


    th "那是她自己的问题，这种愚蠢行为的结果没有什么理由连带上我。"


translate chinese day2_us_7c464e77:


    "哇，这一瞬间如此多的想法飞过..."


translate chinese day2_us_54bb57e2:


    "而有的时候多少年都不够琢磨透一件事的。"


translate chinese day2_us_39a5f3d1:


    "最后理性和自我保护的本能获得了胜利，我向后退了一步。"


translate chinese day2_us_818e5d64:


    "乌里扬卡优雅的落地，扑街，然后马上跳起来，目露凶光的看着我。"


translate chinese day2_us_9a98847e:


    us "你，你...为什么不接住我！"


translate chinese day2_us_5181be41:


    me "反正你也没有摔伤..."


translate chinese day2_us_37dbf984:


    "我顾左右而言他。"


translate chinese day2_us_a7d45458:


    us "我要是受伤了呢？"


translate chinese day2_us_c1b7ead1:


    me "可是你没有啊！{w}你怎么回事啊？最近小电影看多了？"


translate chinese day2_us_d3a6c509:


    us "所以说你根本不在乎我？"


translate chinese day2_us_f396e777:


    "她露出了笑容..."


translate chinese day2_us_89c187b4:


    me "呃，在这种情况下...我当然是在乎..."


translate chinese day2_us_c906b0f6:


    us "我真是受宠若惊啊。"


translate chinese day2_us_8de3d1a0:


    me "别想歪了..."


translate chinese day2_us_630ea867:


    us "好吧好吧，我就不再计较纸牌的问题了。"


translate chinese day2_us_0a94a0ea:


    me "但是我..."


translate chinese day2_us_39d27213:


    "我还没来得及说完我的话，乌里扬卡就跳下了舞台消失在了漆黑的夜色中了。"


translate chinese day2_us_bb143e3a:


    th "哼，又是一个小女孩糟糕的恶作剧。"


translate chinese day2_us_b40ec65c:


    th "我当时是在担心她。"


translate chinese day2_us_2b9842d5:


    th "是谁我都会担心的吧..."


translate chinese day2_us_78ddfb9b:


    "我又一次在脑海中诅咒着乌里扬娜，一边往住处返回。"


translate chinese day2_main4_6f1dc765:


    "我今天第一次感觉到我有多累。"


translate chinese day2_main4_3fd94a8a:


    "没有灯光，所以说奥尔加·德米特里耶夫娜肯定已经睡下了。"


translate chinese day2_main4_9390dd47:


    th "这很奇怪，昨天她等着我来着。"


translate chinese day2_main4_aceec196:


    "我走进房间，悄悄地脱掉衣服躺在床上。"


translate chinese day2_main4_2b052db0:


    th "如果仔细想想的话，经过了这一整天，我的处境一点也没有变的明朗。"


translate chinese day2_main4_0abec8be:


    th "事实上，我这一天都在做毫无意义的事情；如果是在现实世界，我根本不会考虑做这些事情。"


translate chinese day2_main4_4b59bf55:


    th "虽然我有的是时间。"


translate chinese day2_main4_0a500eea:


    th "我到底会在这里呆多长时间还是个谜。"


translate chinese day2_main4_381a6c2e:


    th "也许我永远出不去了，也许再过几分钟一切就会结束。"


translate chinese day2_main4_c21f9a60:


    "我懒得再去想过去的事情，关于我是怎么来到这里什么的。"


translate chinese day2_main4_3d823552:


    "这是我很久以来第一次真正感到累，不只是精神上的累，而且身体也同时感到累，心理上的累，还有鬼知道什么也累..."


translate chinese day2_main4_5016abac:


    "我只想让一切的一切都消失掉，{w}我想让这个乱七八糟的世界自己告诉我真相。"


translate chinese day2_main4_ceda5e44:


    "或者至少不用我在里边费好大的力气。"


translate chinese day2_main4_dabe5250:


    th "可是万一我就永远的被囚禁在这里了呢？"


translate chinese day2_main4_92283096:


    th "那么我就得习惯..."


translate chinese day2_main4_410fa16d:


    th "那就...那样了？这我可没准备好...{w}呃..."


translate chinese day2_main4_dcadb353:


    "我的意识在逐渐的流失，我无法再集中注意思考现实世界的问题。"


translate chinese day2_main4_72f8d69d:


    th "也许应该最好等到明天吧..."


translate chinese day2_main4_ba8204f7:


    "我翻了个身睡着了..."
# Decompiled by unrpyc: https://github.com/CensoredUsername/unrpyc
