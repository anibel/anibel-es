
translate chinese prologue_24958abb:


    "我又做那个梦了。"


translate chinese prologue_07dac942:


    "{i}那个{/i} 梦..."


translate chinese prologue_55dfd177:


    "...每天晚上都是如此。"


translate chinese prologue_9ce34629:


    "但到了早晨就会忘掉，像平常一样。"


translate chinese prologue_0c90c0c8:


    "也许这样才好..."


translate chinese prologue_8df02469:


    "只会留下一刹那的记忆----一座半开的大门，似乎要把我带向某个地方，大门的两侧各有一座少先队员的石像。"


translate chinese prologue_deca17df:


    "还有那个奇怪的女孩{w} 一直在问我："


translate chinese prologue_814eff1e:


    dreamgirl "跟我走吗？"


translate chinese prologue_e7b0f130:


    "走...？"


translate chinese prologue_a2e02439:


    "去哪里？"


translate chinese prologue_fe88af01:


    "...又为什么呢？"


translate chinese prologue_4d0aa097:


    "我这到底是在哪儿啊，话说？"


translate chinese prologue_0683e19b:


    "当然，如果这种事情发生在现实中，肯定会吓到我的。"


translate chinese prologue_233a6b9d:


    "不然呢！"


translate chinese prologue_cd46d7f7:


    "但是这只是一个梦。{w} 一个我每晚都会见到的梦。"


translate chinese prologue_af4b467b:


    "这种事一定预示着什么！"


translate chinese prologue_4cd30aa0:


    "你不必完全了解{i}哪里{/i}或是{i}为何{/i}就能明白：有什么事情发生了。"


translate chinese prologue_2e4e4eec:


    "有什么事情，在拼命的吸引我的注意。"


translate chinese prologue_7bd50019:


    "因为周围的这一切都是真的。"


translate chinese prologue_354cb9c5:


    "像是我自己那间公寓一样真实： 我可以推开这扇大门，听到合页吱吱的响，划掉那些斑驳的铁锈，呼吸凉凉的新鲜空气，在夜色中打起寒噤。"


translate chinese prologue_5fb243fe:


    "这些事我可以做到，但是相应的，我得动起来，迈开步子，挥动手臂..."


translate chinese prologue_a6f914fa:


    "不过这是个梦，我能感觉到，但是还有什么，在改变着我的{i}意识{/i}？"


translate chinese prologue_edb5109a:


    "因为在这里的感觉就像是在一个电视带着裂纹的屏幕的另一头，这台电视还在尽力反抗着雪花和条纹，来给观众们展示节目中的每一个细节。"


translate chinese prologue_28f18900:


    "不过画面变得越来越模糊了...{w}我应该马上就要醒来了。"


translate chinese prologue_a20cefa7:


    "..."


translate chinese prologue_14a2c666:


    "也许我应该问问她，{w}那个女孩。"


translate chinese prologue_5da0f47a:


    "她叫什么名字..."


translate chinese prologue_d7b38ee8:


    "或者问问关于星星的事..."


translate chinese prologue_82d77cb1:


    "不过为什么是星星呢？"


translate chinese prologue_51851d8b:


    "我看还不如问问这大门，{w}对了，就是这大门！"


translate chinese prologue_e5f4ee19:


    "她肯定会很吃惊。"


translate chinese prologue_1edb57c8:


    "或者是关于字母ё。"


translate chinese prologue_3234148c:


    "多么神奇的字母= ="


translate chinese prologue_2367bcf1:


    "好像他们不再存在了似的！"


translate chinese prologue_d92e974d:


    "不过，字母、大门和星星跟这个地方有什么关系呢？"


translate chinese prologue_e2138459:


    "因为即使我每天都在做{i}这个{/i}梦，最终却总是会忘掉，所以必须在此时此地找到答案！ "


translate chinese prologue_d5050891:


    "还有，如果你仔细观察，你会发现麦哲伦云..."


translate chinese prologue_e2b591e8:


    "好像我最后会到南半球去？！"


translate chinese prologue_a20cefa7_1:


    "..."


translate chinese prologue_22d0eaf7:


    "在梦中，有一些很小的东西会引起注意：有着奇异颜色的草丛，纷繁的线条，自己扭曲的镜像，反而是真正危险的东西----可以终结一切的危险，显得不再那么重要了。"


translate chinese prologue_26999fe4:


    "这倒是正常，因为在{i}这里{/i}你不会死掉。"


translate chinese prologue_a6da1123:


    "这我可以确定，我已经经历了几百次了。"


translate chinese prologue_e85b8394:


    "既然不会死掉，活着是为了啥米？"


translate chinese prologue_a0462877:


    "我应该问问那个女孩，她应该是本地人，可能会知道吧。"


translate chinese prologue_ee712a8b:


    "对啊！{w}我应该问问她猫头鹰什么的。"


translate chinese prologue_b0865ff2:


    "奇怪的鸟..."


translate chinese prologue_2be63cfe:


    "这都是什么跟什么..."


translate chinese prologue_a20cefa7_2:


    "..."


translate chinese prologue_814eff1e_1:


    dreamgirl "你会跟我走吗？"


translate chinese prologue_db203a62:


    "然后每次我都得回答。"


translate chinese prologue_7ad37ff1:


    "只能如此，否则梦会永无止境，也就无法醒来。"


translate chinese prologue_027137ed:


    "答案总是很难决定。"


translate chinese prologue_34fac5fe:


    "我是谁，我在干什么，她是谁？"


translate chinese prologue_071572e1:


    "而且为什么我的生活要决定于这个答案。"


translate chinese prologue_ec721a0b:


    "也许不是...？"


translate chinese prologue_51d80ee4:


    "这不过是一个梦。"


translate chinese prologue_ca1b37f9:


    "一个梦。"


translate chinese prologue_41cdd4ce:


    "电脑屏幕盯着我，好象活的一样。"


translate chinese prologue_86cfd3d1:


    "有时我真的觉得它有自己的意识，有自己的主意和愿望，有自己的感觉，能感受爱、和痛苦。"


translate chinese prologue_b3960be5:


    "好像我们互相望着，它不是一件工具，它是我自己，是用塑料和塑胶什么的组成的没有生命的我。"


translate chinese prologue_67859f20:


    "从某种程度上来说是这样的，因为我90%%和外界的沟通都是通过它进行的。"


translate chinese prologue_e051da8b:


    "匿名版，时不时的聊一聊，偶尔时ICQ或者Jabber，更偶尔也可能上论坛。"


translate chinese prologue_3f938594:


    "网络电缆那头的人根本不存在！"


translate chinese prologue_5a1a52c8:


    "他们都是电脑的想象力的产物，源代码里的一个错误或是核心的漏洞，获得了自己的生命。"


translate chinese prologue_2728421d:


    "如果有人观察我的外在，这种想法会显得很疯狂吧，要是有个精神科的医生，他肯定会给我开一份复杂的诊断，没准还会被送进精神病院。"


translate chinese prologue_9d607629:


    "没有一点维护和表面上的秩序的小公寓，窗外永远是大都市灰色的风景线，这就是我的生活。"


translate chinese prologue_5823e040:


    "当然，所有这些事情，在开始时其实并不是这个样子。"


translate chinese prologue_7380c69b:


    "我从出生，上学一直都和别人差不多。"


translate chinese prologue_e5f87d9f:


    "我上了一年半载的大学，一直费劲的跟上大家。"


translate chinese prologue_c895b6f5:


    "我干过几个不同的工作，{w}有时效果还不错，有时甚至待遇还很优厚。"


translate chinese prologue_26348b28:


    "但是我总是感觉这不是我自己，而是别的某本传记里的一个人物。"


translate chinese prologue_111ab6cc:


    "我一直没有真正的生活过，它总是在单调的循环。{w}像是电影土拔鼠之日。"


translate chinese prologue_bab8b972:


    "我无法选择如何度日，日子像是邪恶的螺旋，{w}空虚、压抑、扑朔迷离。"


translate chinese prologue_76b2fe88:


    nvl clear


translate chinese prologue_b41ec809:


    "过去的几年里，我就每天的坐在屏幕前生活。"


translate chinese prologue_c39afb80:


    "有时会有一些小零活，有时就要靠父母救济。"


translate chinese prologue_dfb24146:


    "总归是能挺过来。"


translate chinese prologue_ba1a156e:


    "这也不奇怪，大概是因为我的需求都很基本吧。"


translate chinese prologue_9bc39b5c:


    "我很少出门，我和别人的交流也主要是通过网络通信----{i}匿名{/i}论坛，人们没有真实的姓名，出身，和性别。"


translate chinese prologue_a3a04a99:


    "总之是很常见的反社会的生活，{w}也是一种现代社会独有的生活吧。"


translate chinese prologue_ac5e97c3:


    "也许将来会有个知名作家把我的故事写进书里，然后成为当代文学的经典也说不定。{w}也许我得自己动手......"


translate chinese prologue_eea3f384:


    "不过那应该更是自欺欺人了吧，我几年也憋不出一个小故事来。"


translate chinese prologue_6d041340:



    nvl clear
    "我还打算学习很多东西。"


translate chinese prologue_64793d14:


    "画画，没有天赋。{w}编程，很快会腻。{w}外语，太花时间......"


translate chinese prologue_b41193f7:


    "我唯一喜欢做的也就是读书了，不过也称不上是什么学究。"


translate chinese prologue_d0321015:


    "也许在看动画和编点儿网上的小段子上我倒是比较内行。"


translate chinese prologue_acb955ab:


    "如果干那个能拿工资的话，我可能会生活的幸福一些(也不会这么拮据)，不过恐怕还是难以填补我内心的空虚。"


translate chinese prologue_76b2fe88_1:


    nvl clear


translate chinese prologue_8d743298:


    "今天又是典型的失败者的典型的一天。"


translate chinese prologue_4d249ee8:


    "而今天还是我大学同学聚会的日子。"


translate chinese prologue_3d7e70ae:


    "说实话，我真的不想去。"


translate chinese prologue_556a5316:


    "有什么意义呢，我跟他们相处的时间那么短。"


translate chinese prologue_b84fba8f:


    "然而，我还是被一个朋友说服了：少有的几个我一直没有通过网络还保持着联系的老同学。"


translate chinese prologue_a2136874:


    "一个寒冷的夜晚。{w} 车站。静静的等。"


translate chinese prologue_55a843c8:


    "我从来都不太喜欢冬天。{w}虽然酷夏同样不吸引人。"


translate chinese prologue_6c43f774:


    "我只是想不到任何一个理由挑出来一个对我来说特别的季节，对于我这种全天候在家的生活方式来说。"


translate chinese prologue_a43f6da8:


    "公交车今天迟迟不到，让我感觉见了鬼了，想要把最后几百个卢布花掉搭一辆出租车(不知为何我没有打回家的念头)。"


translate chinese prologue_42c6ca3e:


    "我的大脑里又有无数个想法飞过，不过几乎都没什么实际意义。"


translate chinese prologue_ebf71c4c:


    "没有一个想法是那种可以真正下手去做的。"


translate chinese prologue_6b447492:


    "也许我可以去创业？{w}我要去哪里搞到启动资金呢？"


translate chinese prologue_1263d7e9:


    "也许我可以再去做一个办公室白领？{w}不，这可不行！"


translate chinese prologue_ef2e5c25:


    "也许我适合做一名自由职业者？{w}我有什么一技之长能让人看得上我呢？"


translate chinese prologue_42e509b4:


    "我突然回忆起了我的童年，{w}或者说是我的青春...我十五六七岁的时候。"


translate chinese prologue_153757bd:


    "为什么是那些年呢？{w}不知道。"


translate chinese prologue_b3ca007f:


    "我想应该是那个时候，我周围的一切还都显得那么简单。"


translate chinese prologue_74b11ddc:


    "那个时候我能很轻易的作出决定，和现在完全不同。"


translate chinese prologue_a0da84d1:


    "早上起床的时候，我就知道我的这一天该怎么过，然后一直盼望着周末，我可以无忧无虑的玩：玩电脑、踢足球、和朋友们出去瞎逛。"


translate chinese prologue_7160de27:


    "那个时候，到了第二个星期，我都会打起精神继续学习。"


translate chinese prologue_334f2acb:


    "那个时候，没有现在这些烦人的问题，像是“为什么？”“谁在乎？”“有什么用？”之类的。"


translate chinese prologue_272df669:


    "对于今天我这样的人来说，是那么的简单质朴。"


translate chinese prologue_8963d5d3:



    nvl clear
    "无忧无虑的童年啊。{w}正是那个时候我遇到了我初恋的女孩子。"


translate chinese prologue_4a6ac0a3:


    "她的相貌和声音已经从我的记忆中消失了。"


translate chinese prologue_7493b623:


    "留下的只有她的名字，像是一份简历，又伴随着当时我沉浸其中的对她的思念，{w}对她的爱恋、想要呵护她、保护她。"


translate chinese prologue_064fa38f:


    "可惜只有很短的时间。"


translate chinese prologue_c7008ed9:


    "现在我更是不敢想象那种事发生在我的身上。"


translate chinese prologue_f716a97b:


    "我见到女孩子会不知道怎么开口，究竟该说点什么吸引她的注意。"


translate chinese prologue_d631396b:


    "嗯...我很久没有遇到过动心的女孩子了。{w}而且我能到哪里去遇到呢？"


translate chinese prologue_76b2fe88_2:


    nvl clear


translate chinese prologue_52032d61:


    "发动机的巨大响声把我拉回了现实。"


translate chinese prologue_5d936a59:


    "一辆公交车到站了。"


translate chinese prologue_68c6dc22:


    "这有些不正常，我感觉。"


translate chinese prologue_f260960e:


    "算了，无所谓：反正只有410公交走这条路线。"


translate chinese prologue_3cf36c8d:


    "街道上的灯光纷纷掠过，好像它们是在我身体里面闪动一样，尝试着要点燃我没有生命力的灵魂。"


translate chinese prologue_ac957580:


    "也许只是需要唤醒它们而已。"


translate chinese prologue_b9473edc:


    "因为那些感觉已经在我的身体中呆了很长时间了，不断地醒了再睡，睡了又醒。"


translate chinese prologue_59544860:


    "司机播放着十分熟悉的音乐，{w}不过我却没有听。"


translate chinese prologue_53102b62:


    "我透过车窗的雾气，望着过往的车辆。"


translate chinese prologue_0931b53b:


    "因为人们总是着急去哪里，追求着自己想要的东西，被困在自己那小小的世界里，他们怎么可能来关心我？"


translate chinese prologue_5ccdd86f:


    "他们也许自己也有着严重的问题，也许生活很轻松。"


translate chinese prologue_813e6d62:


    "这你没法确定，因为每个人都是不同的，{w}这应该是吧？"


translate chinese prologue_e0c41f97:


    "有时一个人的行动很容易预测，但如果你想透过他的外表观察灵魂的本质，你只会看到难以捉摸的黑暗。"


translate chinese prologue_a20cefa7_3:


    "..."


translate chinese prologue_9f51c5b0:


    "汽车接近市中心，我的想法被繁华的城市灯光打断。"


translate chinese prologue_1029ada9:


    "数不清的广告牌，眼花缭乱的车水马龙，以及成千上万的行人。"


translate chinese prologue_d443aa58:


    "看着看着，我感到有些困倦。"


translate chinese prologue_d56c688b:


    "我稍微闭上眼歇了一会..."
# Decompiled by unrpyc: https://github.com/CensoredUsername/unrpyc
