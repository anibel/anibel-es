
translate chinese epilogue_mi_4273f0c8:


    "因为有的时候梦就是梦。"


translate chinese epilogue_mi_12b67fb5:


    "你不需要上网查询解梦，这样鲁莽的入侵自己的梦境，你也没有必要把它当作是一种标志或是对未来的预测。"


translate chinese epilogue_mi_e27e5ad0:


    "有时候梦就是梦，一个不会发生在现实中的美好的故事，一幅你正在休息的大脑打了草稿，然后用想象力上色的画作。"


translate chinese epilogue_mi_5d3e8602:


    "有时候梦就是梦..."


translate chinese epilogue_mi_f429eb4f:


    "即使分成几个部分。"


translate chinese epilogue_mi_5192a263:


    ma "我已经受够了..."


translate chinese epilogue_mi_b6e6bdf4:


    my "哈?"


translate chinese epilogue_mi_7ac33094:


    "我睁开眼睛，看到一个女孩子出现在我的面前。"


translate chinese epilogue_mi_39748bd3:


    my "你受够什么了?"


translate chinese epilogue_mi_73e7a292:


    ma "一切!"


translate chinese epilogue_mi_0e1ee137:


    my "这可不是别人逼你的!"


translate chinese epilogue_mi_eb1fb061:


    ma "那个...不是那么简单的!"


translate chinese epilogue_mi_d9924f3f:


    my "不，就是那么简单的!"


translate chinese epilogue_mi_fc671583:


    ma "你怎么总是..."


translate chinese epilogue_mi_f98fd3f1:


    mt "别聊天了！来继续工作!"


translate chinese epilogue_mi_f45e1707:


    "一个不容拒绝的声音从远方传来。"


translate chinese epilogue_mi_a3bffb92:


    "公共汽车很快就清空了。"


translate chinese epilogue_mi_ab07d7f4:


    "那个和我吵架的女孩儿像孩子一样绷着脸，蛮横的转过去，跺着脚走向出口。"


translate chinese epilogue_mi_07837af7:


    "她叫玛莎。"


translate chinese epilogue_mi_44f92a18:


    mt "好了，你准备好了吗?"


translate chinese epilogue_mi_514d8a74:


    "一个女人出现了。她的脸上伴有人类命运的痛苦，傲慢，认真，还有疲倦。"


translate chinese epilogue_mi_da181777:


    my "啊...是啊..."


translate chinese epilogue_mi_83cfac39:


    mt "很好!"


translate chinese epilogue_mi_a1a01658:


    mt "那么...{w}开始!"


translate chinese epilogue_mi_86b86796:


    "我都没有发现她下了车，然后一个相机对着我。"


translate chinese epilogue_mi_d2175e55:


    "我闭上了眼睛，对了，按照剧本我应该是睡觉的。"


translate chinese epilogue_mi_726cd530:


    "我得这样子坐多长时间？十秒钟？二十秒钟？"


translate chinese epilogue_mi_5c06941f:


    "应该足够了。"


translate chinese epilogue_mi_44a112d2:


    "我慢慢睁开眼睛大致扫视着汽车。"


translate chinese epilogue_mi_c58cb760:


    "对了，我完全不知道自己在什么地方!"


translate chinese epilogue_mi_c96b254d:


    "这是什么？一辆Icarus？但是为什么？"


translate chinese epilogue_mi_c77489ba:


    "所以说我得做出我至今最为惊讶的表情!"


translate chinese epilogue_mi_28036f42:


    "我应该想象自己坐在马桶上看书，然后突然门打开了，我看见让·雷诺拿着一把枪出现了。"


translate chinese epilogue_mi_1d68d7b6:


    "抱歉，我能不能在这里大号？"


translate chinese epilogue_mi_81e3cf06:


    "他肯定会惊讶的!"


translate chinese epilogue_mi_74e80531:


    "我就是忍不住要大笑起来。"


translate chinese epilogue_mi_92ab1fe0:


    mt "停！停!"


translate chinese epilogue_mi_5aaa9752:


    "导演喊着。"


translate chinese epilogue_mi_f84967b1:


    mt "我受不了了，我们已经拍了一个星期了，但是还是半点儿成果都没有!"


translate chinese epilogue_mi_d4d147e6:


    "她冲了过来。"


translate chinese epilogue_mi_ca068b02:


    mt "这次又怎么了?"


translate chinese epilogue_mi_f4f4dacd:


    my "你知道，我..."


translate chinese epilogue_mi_d82fb8a9:


    "好吧，她不太可能欣赏我和让·雷诺的故事..."


translate chinese epilogue_mi_27f98f9f:


    my "抱歉..."


translate chinese epilogue_mi_399d102b:


    "很难看着她。"


translate chinese epilogue_mi_548b6f7a:


    "不是说我又搞砸了一个镜头，而是我害怕再次笑出来。"


translate chinese epilogue_mi_ed0fe155:


    mt "很好，午间休息!"


translate chinese epilogue_mi_4d90d1cf:


    "导演摘掉了巴拿马帽子然后开始扇风。"


translate chinese epilogue_mi_d7d387fd:


    mt "真是热..."


translate chinese epilogue_mi_2c1992b5:


    sh "再怎么说也是夏天了。"


translate chinese epilogue_mi_1d5a2e57:


    "舒里克突然凭空出现，而且显得很善于观察。"


translate chinese epilogue_mi_193c877e:


    mt "傻子，你最好拿好摄像机!"


translate chinese epilogue_mi_2f643813:


    "舒里克冷静的躲开了扔向他的巴拿马帽子然后返回了摄像机的位置。"


translate chinese epilogue_mi_0781974d:


    "他总是那个样子，沉着冷静，富有智慧，...{w}无聊，迟钝，没有意思。"


translate chinese epilogue_mi_3b0ed2d0:


    "我走出汽车，前往食堂吃口饭。"


translate chinese epilogue_mi_11a4fd66:


    "然后在门口遇到了玛莎..."


translate chinese epilogue_mi_eb88f1fb:


    ma "所以?"


translate chinese epilogue_mi_48975165:


    my "没什么..."


translate chinese epilogue_mi_30261bba:


    ma "怪不得"


translate chinese epilogue_mi_23457c21:


    my "想要吃点什么吗?"


translate chinese epilogue_mi_5a6ffa85:


    "她不理解的看着我。"


translate chinese epilogue_mi_c8eb94cd:


    ma "好了，咱们走。"


translate chinese epilogue_mi_a274cb3e:


    "奇怪，似乎玛莎已经去过食堂了 – 什么，难道她没有时间吃午饭吗?"


translate chinese epilogue_mi_00d4b994:


    "我从背后仔细的观察着她，她的双马尾不停地摇晃着，不时地击打着附近的东西。"


translate chinese epilogue_mi_5b40950c:


    my "你确定头发那个样子很舒服吗?"


translate chinese epilogue_mi_a5498145:


    "我们坐下来以后我问着玛莎。"


translate chinese epilogue_mi_64c890f7:


    ma "有什么问题吗?"


translate chinese epilogue_mi_5e7e8446:


    my "那个，它们不是有点儿...{w}太长了吗?"


translate chinese epilogue_mi_518927bf:


    ma "这是我的角色设定，傻瓜!"


translate chinese epilogue_mi_be22885b:


    my "好像原来短过似的..."


translate chinese epilogue_mi_b990f6b1:


    "她愤怒地看着我，然后继续疯狂的搅拌着她的方便面。"


translate chinese epilogue_mi_afce6747:


    ma "整天吃这个我总有一天要吃出胃炎。"


translate chinese epilogue_mi_a6971a64:


    my "我已经得了..."


translate chinese epilogue_mi_fad116df:


    ma "是啊，我看你是连着道具都一起喝下去了!"


translate chinese epilogue_mi_289391d1:


    my "那又怎么了？告诉你我干这个不拿工钱! 而且很难—领导工作！我不喝相机和胶片!"


translate chinese epilogue_mi_513a7fe4:


    ma "哇，好像你要去争夺奥斯卡奖似的!"


translate chinese epilogue_mi_1e087707:


    my "谁知道呢...谁知道呢..."


translate chinese epilogue_mi_5175d639:


    "我靠在椅子上，点着一支烟。"


translate chinese epilogue_mi_673b46b1:


    ma "我得提醒你一下，这里是一个少年夏令营，你可别忘了!"


translate chinese epilogue_mi_8a8d8f52:


    my "是啊，你有看到孩子吗?"


translate chinese epilogue_mi_6f794cba:


    "玛莎只是抽抽鼻子，没有回答。"


translate chinese epilogue_mi_b3a659cb:


    my "而且，所有伟大的事情都要从一些...比如说毕业论文开始..."


translate chinese epilogue_mi_bbbe361d:


    ma "谁也不会想去拍这样的垃圾!"


translate chinese epilogue_mi_f47e1355:


    "她谨慎的扫视着附近，小心的确认这食堂里只有我们，然后对我说。"


translate chinese epilogue_mi_5cd04ce7:


    ma "只有我们的奥尔加·德米特里耶夫娜做的出来。"


translate chinese epilogue_mi_c4de2e46:


    my "又不是什么人强迫你去做的。"


translate chinese epilogue_mi_143442a9:


    ma "是吗?"


translate chinese epilogue_mi_9912a5af:


    "玛莎略带讽刺的说着。"


translate chinese epilogue_mi_00cde85b:


    ma "好像我有什么选择似的，或者你，还是什么别人..."


translate chinese epilogue_mi_d9a5ec9c:


    my "首先我要告诉你，你也可以提出另一个计划。"


translate chinese epilogue_mi_ba9e80db:


    ma "提出一个计划...什么计划..."


translate chinese epilogue_mi_20caf834:


    "玛莎说话的时候一直不停的往面条里撒盐，而且似乎没有要停下的意思。"


translate chinese epilogue_mi_53e387c4:


    my "我看你喜欢辣的..."


translate chinese epilogue_mi_20f54ffc:


    ma "什么?"


translate chinese epilogue_mi_124641aa:


    "我往她的杯子看去。"


translate chinese epilogue_mi_321a700f:


    ma "糟糕!"


translate chinese epilogue_mi_a30b21a1:


    "她把盐瓶摔在桌子上，然后沮丧的看着自己碗里堆成小山的盐，就好像在和全球变暖进行斗争的冰山一样。"


translate chinese epilogue_mi_f206e4c8:


    ma "哦，天哪..."


translate chinese epilogue_mi_111d10a4:


    "我突然感觉自己有点对不起玛莎。"


translate chinese epilogue_mi_ad4d8e3a:


    "反正我是吃不下去这种垃圾，三明治比这个好吃多了。"


translate chinese epilogue_mi_e6bf5c70:


    my "喏。"


translate chinese epilogue_mi_41d4a8f6:


    "我把自己的碗推过去。"


translate chinese epilogue_mi_7d3aefd1:


    "她没有发言的接受了，然后开始默默的吃起来。"


translate chinese epilogue_mi_5307eb63:


    my "你就不会谢谢我吗?"


translate chinese epilogue_mi_6d330cb9:


    ma "但是这完全是你的错!"


translate chinese epilogue_mi_9536b49c:


    my "什么?"


translate chinese epilogue_mi_c8bad511:


    ma "你让我分心然后...反正是你的错!"


translate chinese epilogue_mi_8c82cb1d:


    my "啊，算了..."


translate chinese epilogue_mi_90823446:


    "玛莎继续默默的吃着。"


translate chinese epilogue_mi_e695e8fb:


    "我抽完烟把烟头戳在桌角。"


translate chinese epilogue_mi_2f0cd122:


    my "你具体是不喜欢哪个方面?"


translate chinese epilogue_mi_62eb8e6c:


    ma "啊，其实还好，如果你习惯的话，多么可爱的面条啊。"


translate chinese epilogue_mi_af79cf2d:


    "我奇怪的看着玛莎，用眼神告诉她，她说的话多么的愚蠢又没有关系。"


translate chinese epilogue_mi_a4712259:


    ma "啊，你是在说电影..."


translate chinese epilogue_mi_e9b89811:


    my "关于那个电影。"


translate chinese epilogue_mi_9e1884cf:


    ma "就说我的人物吧!{w}虽然不介意当作例子，但是这是最糟糕的部分! 你在写的时候到底有没有考虑过?"


translate chinese epilogue_mi_a744ace5:


    "前两天还思考了一下，然后我就开始犯懒了。"


translate chinese epilogue_mi_d3a9b814:


    my "那个，我读过..."


translate chinese epilogue_mi_bf175512:


    ma "怎么了，看起来很正常吗?"


translate chinese epilogue_mi_166834c3:


    my "说什么?"


translate chinese epilogue_mi_90e5468d:


    ma "角色，我的角色!"


translate chinese epilogue_mi_1ebebef3:


    my "有什么问题?"


translate chinese epilogue_mi_dee08048:


    "我决定一直装傻。"


translate chinese epilogue_mi_7d9ee071:


    ma "不要嘲笑我了!"


translate chinese epilogue_mi_25e9c59a:


    "如果表情可以杀人，我一定死得很难看。"


translate chinese epilogue_mi_534485c8:


    my "好了，好了，...{w} 冷静。"


translate chinese epilogue_mi_bc30c075:


    "玛莎一直盯着我。"


translate chinese epilogue_mi_f7f9f25d:


    my "你的角色怎么了? 只是一个普通的迟钝的女孩儿...{w} 反正这样的也很多，时不时?"


translate chinese epilogue_mi_db110686:


    "她的脸变红了，然后变蓝了，然后变绿了。"


translate chinese epilogue_mi_979d0955:


    "我不知道玛莎脸上变色是不是真的是这样，但是真的很吓人。"


translate chinese epilogue_mi_69b1e1a6:


    ma "我说，那真的很吓人。"


translate chinese epilogue_mi_83a2f0c6:


    "她突然说。"


translate chinese epilogue_mi_d122e6e9:


    my "我觉得这很正常...{w}电影的人物，或者是一个小说里的，不可能只有聪明可爱又性感的角色，我们还需要其他辅助的角色。"


translate chinese epilogue_mi_baea95af:


    ma "辅助的，不是迟钝发傻的!"


translate chinese epilogue_mi_b4e96502:


    my "我们现实中也不是就遇不到..."


translate chinese epilogue_mi_8951537d:


    ma "但是我可不是那个样子!"


translate chinese epilogue_mi_429736bb:


    my "但是你可不像是你自己!"


translate chinese epilogue_mi_cb5eb573:


    "她的固执让我无奈。"


translate chinese epilogue_mi_97cf9711:


    ma "人们看完会说：玛莎——傻瓜!"


translate chinese epilogue_mi_9439a255:


    "我捂住脸，把头埋进桌子里。"


translate chinese epilogue_mi_7928ecf8:


    my "好吧，你现在有什么建议?"


translate chinese epilogue_mi_82e7adc3:


    ma "我不知道...这就是不好!"


translate chinese epilogue_mi_b9def702:


    my "如果你一直哭，一直呻吟，事情就会变好吗?"


translate chinese epilogue_mi_499a3ecd:


    ma "我没有在呻吟!"


translate chinese epilogue_mi_4fa7d502:


    my "啊，好像我听不见似的..."


translate chinese epilogue_mi_d6c365e1:


    "呃，我宁愿听不见。"


translate chinese epilogue_mi_51775003:


    ma "你知道吗?!"


translate chinese epilogue_mi_012acf66:


    "我抬头看着发怒的玛莎。"


translate chinese epilogue_mi_5cec30a4:


    my "什么?"


translate chinese epilogue_mi_99d8ebb3:


    ma "你就是一个冷漠的混蛋!"


translate chinese epilogue_mi_7893761a:


    my "您说得对，大小姐!"


translate chinese epilogue_mi_c5902aa6:


    ma "还有..."


translate chinese epilogue_mi_44ce257f:


    "她还没有说完，突然门打开了，一阵兴奋的喊声。"


translate chinese epilogue_mi_c3914184:


    sa "玛莎喵！谢苗君!"


translate chinese epilogue_mi_f994b25d:


    "呃，又来了一个癫狂的疯子..."


translate chinese epilogue_mi_6bdb159c:


    "神啊，我到底做错了什么...?"


translate chinese epilogue_mi_d10088d4:


    "我转过头充满怀疑的看着走进食堂的这个女孩子。"


translate chinese epilogue_mi_9a041eba:


    "萨沙，也是一个主要人物。"


translate chinese epilogue_mi_239efec8:


    "如果我们比较一下萨沙的角色还有她现实中的人，那么萨沙说的这个世界是多么不公平也不是没有意义。"


translate chinese epilogue_mi_92638c32:


    "然而积极的说，我们也不能否定萨沙的表演天才—她在荧屏上真的是一个完全不同的人!"


translate chinese epilogue_mi_c612ee04:


    ma "我跟你说了一千次了，不要叫我玛莎喵!"


translate chinese epilogue_mi_fe3b21aa:


    "玛莎咬牙切齿。"


translate chinese epilogue_mi_8b59578c:


    sa "好吧，那就是玛喵，行不行，行不行?"


translate chinese epilogue_mi_389929aa:


    "萨沙拽过来一把椅子坐在我旁边。"


translate chinese epilogue_mi_a334a043:


    ma "真是疯狂...你这种人是怎么进了大学的...?"


translate chinese epilogue_mi_4b8e4bdb:


    sa "喵！喵！你们在说什么?"


translate chinese epilogue_mi_c8b5ead4:


    "萨沙似乎忽略了玛莎对她职业素养的指责。"


translate chinese epilogue_mi_c7467996:


    my "哦，我们刚刚在讨论玛莎喵的剧本问题。"


translate chinese epilogue_mi_298983e2:


    ma "你等着我要把你打出喵来!"


translate chinese epilogue_mi_84102612:


    "她试图打我的额头，但是我躲开了，结果她的拳头落在了桌子上。"


translate chinese epilogue_mi_f5ad59fd:


    "萨沙马上开始哭泣颤抖起来。"


translate chinese epilogue_mi_9038c77e:


    my "你看到她又多么不满足了吗？一个充满好奇心的年轻人，毫不妥协的，准备开始自我牺牲的巨大工作，为了牺牲自己，拒绝这个物质的世界，为了什么？为了真理！"


translate chinese epilogue_mi_bd2c4c91:


    sa "哇，你好聪明!"


translate chinese epilogue_mi_3ef4a660:


    "萨沙搂着我的肩膀悄悄说着。"


translate chinese epilogue_mi_852d3af8:


    "真遗憾你不是..."


translate chinese epilogue_mi_c573c728:


    ma "不，你已经没有救了..."


translate chinese epilogue_mi_f76258c4:


    my "我就当成是夸我了。"


translate chinese epilogue_mi_e76f1572:


    "玛莎从包包里找出手机，然后看看时间。"


translate chinese epilogue_mi_10fe4a30:


    ma "好了，该走了。"


translate chinese epilogue_mi_1b4f07fe:


    my "你去哪儿？你只在第二天出现来着。"


translate chinese epilogue_mi_fd715212:


    ma "是你该走了，傻瓜！"


translate chinese epilogue_mi_ed7479aa:


    sa "我也是，我也是!"


translate chinese epilogue_mi_b3fcc6a8:


    "萨沙跳来跳去，但是还是揽着我的手臂。"


translate chinese epilogue_mi_12e0f2c5:


    my "我想大概是吧..."


translate chinese epilogue_mi_3cd326b5:


    "我站起来，把椅子退回去然后看着我玛莎。"


translate chinese epilogue_mi_1e6a6efa:


    my "再见。"


translate chinese epilogue_mi_f04f64a5:


    ma "走吧..."


translate chinese epilogue_mi_99aa653e:


    "她哼哼着转了过去。"


translate chinese epilogue_mi_eb124edb:


    sa "我出发了!"


translate chinese epilogue_mi_102232fe:


    ma "你滚开啦..."


translate chinese epilogue_mi_400f3980:


    "我刚刚听到，萨沙就把空的口袋扔了出去..."


translate chinese epilogue_mi_a20cefa7:


    "..."


translate chinese epilogue_mi_cf8c504b:


    "我坐在公交车上，懒洋洋的翻着剧本。"


translate chinese epilogue_mi_5c5ba007:


    "这是哪个傻子写的?"


translate chinese epilogue_mi_f3c1a9e9:


    "啊，是啊，是我..."


translate chinese epilogue_mi_5e46e93b:


    "我把这一打褶皱的册子放在一边，然后盯着窗外。"


translate chinese epilogue_mi_aceecbb8:


    "确实，现在真的很热。"


translate chinese epilogue_mi_d760f21d:


    "即使没有舒里克的显然通知，还是感觉很热。"


translate chinese epilogue_mi_2ef129f9:


    "就说柏油马路吧，它也不在乎剧本上写了昨天还是冬天，那么今天要化还是会化的。"


translate chinese epilogue_mi_9380d7a8:


    "蚱蜢之类的虫子也不会在意，它们只是不停地表演着。"


translate chinese epilogue_mi_856be263:


    "话说，到底是谁负责鸣叫?{w}是蟋蟀还是蚱蜢? 还是知了?{w}不，这种尖叫..."


translate chinese epilogue_mi_64bfb5a6:


    "真是的，我都没有一台上网设备能查阅一下资料..."


translate chinese epilogue_mi_0ef08c24:


    "不过我需要吗？这个剧本已经经过一致的同意，不能继续修改了。"


translate chinese epilogue_mi_a078b3cc:


    "而且再怎么说，谁会在意我们电影里的虫子啊?"


translate chinese epilogue_mi_e5cca34a:


    "虽然说拍一个美国五十年代的恐怖内容也是个好主意。"


translate chinese epilogue_mi_3f1a9553:


    "就像是“我的兄弟是蚱蜢”或者“蟋蟀之日”或者“飞跃蝉巢”..."


translate chinese epilogue_mi_ff14da99:


    "如果我还得在带上五分钟，我宁愿自己变成虫子!"


translate chinese epilogue_mi_578e7bc1:


    "对了，为什么苏联的老汽车里没有空调...?"


translate chinese epilogue_mi_a93c1fe4:


    mt "嘿！你们好！有人在家吗?"


translate chinese epilogue_mi_cc507cf7:


    "一个大大的扩音器指着我，我看到后面的奥尔加·德米特里耶夫娜，她的脸映在燃烧的空气中。"


translate chinese epilogue_mi_f4a4def9:


    my "请把你的信息放在..."


translate chinese epilogue_mi_619302ff:


    "I felt a blow."


translate chinese epilogue_mi_5d7f43c9:


    my "嘿，不要就这样把主角扔在那里！你读了我的指南了吗?"


translate chinese epilogue_mi_ffdbc81e:


    mt "闭嘴！这是在公共汽车里拍的最后一遍!"


translate chinese epilogue_mi_968a77a4:


    "我自己也是这么想的，再怎么说外面也比这个密闭的蒸锅好得多。"


translate chinese epilogue_mi_61c207fa:


    "舒里克的相机举过我。"


translate chinese epilogue_mi_3cf97e79:


    "我演的好像真的一样，愤怒的冲出公交车，瘫倒在路边..."


translate chinese epilogue_mi_d536fac8:


    mt "Cut!"


translate chinese epilogue_mi_cb64514a:


    "奥尔加·德米特里耶夫娜微笑着。"


translate chinese epilogue_mi_fdafbf2c:


    mt "看起来你真想做的时候还是能做到的啊!"


translate chinese epilogue_mi_753f8975:


    "确实动力是必须的。"


translate chinese epilogue_mi_f8e1553e:


    mt "现在呢，见到斯拉维娅。"


translate chinese epilogue_mi_25e7e67f:


    ro "休息一会儿怎么样?.."


translate chinese epilogue_mi_5b25a91f:


    "Router祈求着。"


translate chinese epilogue_mi_59395cbc:


    mt "我们不是来度假的！快去搞你的照相机!"


translate chinese epilogue_mi_0378d59e:


    "他耷拉着脑袋挪回了照相机的位置。"


translate chinese epilogue_mi_36a7ec5f:


    mt "Action!"


translate chinese epilogue_mi_c787cab8:


    "我盯着大门。"


translate chinese epilogue_mi_ef82b877:


    "我看着少先队员的雕塑，然后慢慢向前走着。"


translate chinese epilogue_mi_0b401918:


    "萨沙从门后面出现。"


translate chinese epilogue_mi_078abde8:


    sa "你好，谢苗君！我叫做斯拉维娅！夜露死苦！"


translate chinese epilogue_mi_bf59de13:


    "然后是一段尴尬的沉默。"


translate chinese epilogue_mi_d7a685d0:


    "看起来连蟋蟀们都着迷的看着斯拉维娅，而停止了歌声。"


translate chinese epilogue_mi_916c7ee7:


    mt "我只有一个问题。"


translate chinese epilogue_mi_209e0706:


    "奥尔加·德米特里耶夫娜可怕的冷静的声音突然出现在后面。"


translate chinese epilogue_mi_1c639c9a:


    mt "你读过剧本吗?"


translate chinese epilogue_mi_dfa4c8a5:


    sa "当然啦！但是那个太无聊了！"


translate chinese epilogue_mi_b521b673:


    "现在不管是哭，是骂还是什么的都没有用，我们没有其他的演员了。"


translate chinese epilogue_mi_15068e6a:


    mt "我们在这个营地只有三个星期，你却..."


translate chinese epilogue_mi_3895407a:


    "她坐在导演小椅子上用自己的帽子扇着风。"


translate chinese epilogue_mi_e2203c1f:


    mt "伙计们，你们知不知道如果你们继续这样闹来闹去我们会拍不完啊，你们能不能严肃一点？？好像只有我在乎似的..."


translate chinese epilogue_mi_1b8271ef:


    "棍子对于萨莎没有用，所以我们可能要用胡萝卜。"


translate chinese epilogue_mi_7f682ff5:


    sa "抱歉...我会再试一次的。"


translate chinese epilogue_mi_2f8fa8d8:


    mt "最好是这样。"


translate chinese epilogue_mi_b09d6e1c:


    "我开始想象这个主角，他的大脑即将爆炸，他的思想在神游宇宙，面前突然出现的这个女孩子对他一无所知..."


translate chinese epilogue_mi_6e8305c4:


    "她只是对他打了招呼，然后就带他去见了辅导员..."


translate chinese epilogue_mi_04183b60:


    "我往前走了几步，然后萨沙出现在了大门处。"


translate chinese epilogue_mi_2d51c8a5:


    sa "你一定是新来的吧?"


translate chinese epilogue_mi_e46c798b:


    my "..."


translate chinese epilogue_mi_c4155f1c:


    "我注意到角落里照相机的镜头，尽量装出惊讶的表情。"


translate chinese epilogue_mi_74e29a93:


    sa "如果是这样的，你需要去找我们的辅导员同志..."


translate chinese epilogue_mi_794febdd:


    mt "所以说你可以做到的！!"


translate chinese epilogue_mi_2424e856:


    "奥尔加·德米特里耶夫娜抹抹她额头上的汗珠。"


translate chinese epilogue_mi_01ddb43c:


    mt "我们还要编辑...什么...呃..."


translate chinese epilogue_mi_9b67587c:


    "她看看表。"


translate chinese epilogue_mi_76501b5e:


    mt "好了，今天就到这里吧。"


translate chinese epilogue_mi_8e0afbc8:


    "我虽然算不上是工作狂，但是就算是我也感觉这停的有点太早了，既然我们已经落后进度很久了。"


translate chinese epilogue_mi_31a1857e:


    my "你不觉得..."


translate chinese epilogue_mi_0826aeda:


    mt "是的！但是我觉得已经完全可以了!"


translate chinese epilogue_mi_50a46ecb:


    my "啊，听你的..."


translate chinese epilogue_mi_db0f23eb:


    "我耸耸肩，返回营地。"


translate chinese epilogue_mi_04fdda5e:


    "夜晚在慢慢降临..."


translate chinese epilogue_mi_a20cefa7_1:


    "..."


translate chinese epilogue_mi_5cbd3ea0:


    "今晚我和玛莎，萨沙还有阿丽夏一起吃饭。"


translate chinese epilogue_mi_988b5808:


    "我的面前是一碗炸土豆片还有蘑菇。"


translate chinese epilogue_mi_13434154:


    "作为夏天的一点补贴——花园里新鲜的蔬菜。"


translate chinese epilogue_mi_8aa3e2bb:


    ma "你最好在拍摄的时候也那么卖力。"


translate chinese epilogue_mi_ba41249e:


    my "哇，你看起来更喜欢方便面?"


translate chinese epilogue_mi_36b02c46:


    dv "你在说什么，萨沙可是个大厨师啊!"


translate chinese epilogue_mi_7a3a0d77:


    "阿丽夏嘀咕着。"


translate chinese epilogue_mi_49910a9c:


    ma "没有人告诉过你满嘴食物说话是很不礼貌的吗?"


translate chinese epilogue_mi_ccfd297e:


    dv "你总是在抱怨..."


translate chinese epilogue_mi_1c59bfd0:


    sa "我...我..."


translate chinese epilogue_mi_508e3ad2:


    "萨沙快要哭出来了。"


translate chinese epilogue_mi_2b2f274d:


    ma "噢，天哪...冷静，都很好吃!"


translate chinese epilogue_mi_1b24a1e0:


    sa "喵呜！玛莎喵，你想要让我做什么..."


translate chinese epilogue_mi_2d21996b:


    "玛莎以一种奇怪的表情看着她，让我们觉得不能再继续了。"


translate chinese epilogue_mi_df502e69:


    ma "但是不管怎么样，就这样告诉我..."


translate chinese epilogue_mi_d9f1b919:


    my "我不说!"


translate chinese epilogue_mi_4718ae9f:


    ma "但是你还不知道我要问你什么!"


translate chinese epilogue_mi_2596d838:


    my "我知道..."


translate chinese epilogue_mi_6a9e9866:


    ma "所以说，会怎么样?"


translate chinese epilogue_mi_7eb71829:


    "她骄傲的看着我。"


translate chinese epilogue_mi_11c965cb:


    my "我怎么写出这么垃圾的本子..."


translate chinese epilogue_mi_6ceed182:


    ma "见鬼!"


translate chinese epilogue_mi_5fcef207:


    "阿丽夏和萨沙笑了。"


translate chinese epilogue_mi_a13d417a:


    ma "啊，我看你们两个都没事!"


translate chinese epilogue_mi_c5a0a029:


    dv "这有什么啦？夏天的大自然，海滩，多么惬意!"


translate chinese epilogue_mi_bb713d3d:


    ma "你还有可能去土耳其，你为什么来这里?"


translate chinese epilogue_mi_2dd77e41:


    "阿丽夏呆呆地看着玛莎，然后开始吃起来。"


translate chinese epilogue_mi_7d88023f:


    ma "所以?"


translate chinese epilogue_mi_d0742f96:


    "她用叉子指着我。"


translate chinese epilogue_mi_4ac27a40:


    my "不怕叉子就怕勺，一拳把你打开瓢!"


translate chinese epilogue_mi_2e15dd85:


    ma "现在你告诉我，就说他正常吗?"


translate chinese epilogue_mi_1efc89c2:


    sa "谢苗君超卡哇伊!"


translate chinese epilogue_mi_a16d8757:


    "萨莎抱着我然后点着头。"


translate chinese epilogue_mi_3a95f27d:


    dv "这有什么问题，他让我们有一个假期。"


translate chinese epilogue_mi_1f119ac6:


    ma "我不知道自己在这里干什么。"


translate chinese epilogue_mi_0c881678:


    my "反正没人逼你。"


translate chinese epilogue_mi_7b75257b:


    "我冷冷地说。"


translate chinese epilogue_mi_0bbe7c29:


    ma "啊，如果有人在乎我想要什么多好啊..."


translate chinese epilogue_mi_3d475d4b:


    my "还有你，你一直都在干什么?{w} 你总是唠叨别人，但是根据你的剧本离你登场还有好久，你一定是在准备自己的角色?"


translate chinese epilogue_mi_e5e97d5d:


    "我打断了她。"


translate chinese epilogue_mi_ad5371b1:


    ma "啊，也许我是在准备!"


translate chinese epilogue_mi_a1263512:


    dv "双重标准!"


translate chinese epilogue_mi_6c54bdc2:


    ma "三重!"


translate chinese epilogue_mi_ea3c71bd:


    my "所以你真正在做什么?"


translate chinese epilogue_mi_338067db:


    ma "你还希望我做什么?"


translate chinese epilogue_mi_84d72c1c:


    my "我？没有，只是好奇。"


translate chinese epilogue_mi_45e76bef:


    ma "看书..."


translate chinese epilogue_mi_ec13eb7f:


    "她怯生生的回答着。"


translate chinese epilogue_mi_9a9c5850:


    my "各种关于演员修养的书..."


translate chinese epilogue_mi_07729729:


    ma "行了吧，咱们坐在这不知道什么地方，没有互联网，没有手机信号，什么都没有！{w}如果我没有带驱蚊设备..."


translate chinese epilogue_mi_71495458:


    sa "我带了，喵!"


translate chinese epilogue_mi_8538763c:


    "玛莎的眼神，突然让萨沙退缩了。"


translate chinese epilogue_mi_b9cb210e:


    dv "我就是不明白...一直在这里抱怨，意义何在...? 你本来可以就在家呆着的..."


translate chinese epilogue_mi_010cbc3b:


    ma "啊，我问你了吗!?"


translate chinese epilogue_mi_caa0783d:


    "玛莎生气的站起来，走向出口。"


translate chinese epilogue_mi_5354f480:


    sa "玛莎喵生气了..."


translate chinese epilogue_mi_47d1149e:


    "萨沙沮丧的说。"


translate chinese epilogue_mi_a21ed268:


    dv "哈，如果她现在就是生气的话，那么你们等着快要拍完的时候吧..."


translate chinese epilogue_mi_f23678e8:


    my "少先队员链锯大屠杀Druzhba，是么...?"


translate chinese epilogue_mi_bced139f:


    "我真的不明白为什么玛莎总是各种发怒。"


translate chinese epilogue_mi_587f2ffc:


    "她从一开始就对这些都没有真正的热情，不管是对剧本还是去拍电影。"


translate chinese epilogue_mi_c1ebda1e:


    "但是她还不是那么咄咄逼人。"


translate chinese epilogue_mi_0912ae25:


    "也许是因为天太热了？或者是草丛里的蚱蜢？"


translate chinese epilogue_mi_c7b9f0f9:


    "对了，就是这个，蚱蜢!{w}它们不让我们睡个好觉，简直是地狱!"


translate chinese epilogue_mi_11922441:


    "我真希望我们能有一个按钮，按一下就会释放出毒气，然后..."


translate chinese epilogue_mi_bb476699:


    sa "你在想什么?"


translate chinese epilogue_mi_5fa3b6bf:


    my "蚱蜢!"


translate chinese epilogue_mi_0c3815a2:


    dv "它们有什么问题?"


translate chinese epilogue_mi_45f1bddf:


    my "你应该问它们哪里没有问题。"


translate chinese epilogue_mi_4c2e02b2:


    sa "蚱蜢，不是喵。"


translate chinese epilogue_mi_5b4d3391:


    my "为什么?"


translate chinese epilogue_mi_5ae0bd3f:


    sa "那个，它们太...可怕了..."


translate chinese epilogue_mi_7ea55766:


    my "你难道不喜欢昆虫吗?"


translate chinese epilogue_mi_5c8ae25c:


    sa "不喜欢!"


translate chinese epilogue_mi_a9e9f873:


    my "我也不喜欢..."


translate chinese epilogue_mi_eb6b2adb:


    "压抑的沉默。"


translate chinese epilogue_mi_33c07f3e:


    "不管玛莎在想什么，至少她总能让我们聊下去。{w}即使说的是蚱蜢。"


translate chinese epilogue_mi_b69d5469:


    "我开始想象她谈起对这种怪异的虫子的痛恨，不仅要感染整个世界，还要在半夜叫个不停，打扰你内心的平静还有自在的灵魂..."


translate chinese epilogue_mi_5f4cc6c3:


    "奥尔加·德米特里耶夫娜走了过来。"


translate chinese epilogue_mi_c7675ebb:


    mt "准备好晚上的拍摄了吗?"


translate chinese epilogue_mi_6ba4a203:


    my "晚上还拍摄?我们好像还没有拍完第一个场景呢...{w}而且你还说过今天已经完成了啊..."


translate chinese epilogue_mi_0dc170ff:


    mt "我们要最大限度的利用时间，不然我们将一事无成!"


translate chinese epilogue_mi_a996136e:


    "我不能说自己已经累透了，而且这是我自己的计划，我得展示出一些诚意。"


translate chinese epilogue_mi_78cf53f5:


    my "好吧，咱们要拍什么？"


translate chinese epilogue_mi_2a270e1d:


    mt "晚上和斯拉维娅在林中的镜头。"


translate chinese epilogue_mi_d88ec507:


    sa "纳尼~?"


translate chinese epilogue_mi_f18d55d7:


    "萨沙听说我们在讨论她的镜头于是跳了过来。"


translate chinese epilogue_mi_2fe8b72b:


    mt "求你了..."


translate chinese epilogue_mi_d489d61c:


    "奥尔加·德米特里耶夫娜憔悴的说着。"


translate chinese epilogue_mi_41db4dd8:


    mt "这次千万要跟着剧本走!"


translate chinese epilogue_mi_1897c7ff:


    sa "是!"


translate chinese epilogue_mi_ed85db76:


    mt "你有完没有..."


translate chinese epilogue_mi_ba5e3d3e:


    "她转身离开了。"


translate chinese epilogue_mi_f0d9fc84:


    dv "那个场景是什么?"


translate chinese epilogue_mi_a8c746ea:


    my "你有没有开始读剧本?"


translate chinese epilogue_mi_7ac752e2:


    dv "当然没有!"


translate chinese epilogue_mi_1ac9e5f7:


    "阿丽夏骄傲的说。"


translate chinese epilogue_mi_cea1d5da:


    dv "我只要读过自己上场的部分就没问题啦。"


translate chinese epilogue_mi_31409fb0:


    "我不想和她谈这个关于读过整个剧本的好处的话题。"


translate chinese epilogue_mi_4c582a03:


    my "我偷看着斯拉维娅在林中跳舞..."


translate chinese epilogue_mi_5f43ddfa:


    sa "喵喵！好浪漫!"


translate chinese epilogue_mi_537ab6db:


    my "...然后她一边跳着一边开始脱衣服!"


translate chinese epilogue_mi_65c466a7:


    sa "喵?"


translate chinese epilogue_mi_03d57659:


    "萨莎不理解的看着我。"


translate chinese epilogue_mi_fd1e9efb:


    "过了一会儿她突然明白过来。"


translate chinese epilogue_mi_29afe1be:


    sa "脱...脱衣服?"


translate chinese epilogue_mi_dea901ac:


    my "放轻松，不是完全脱掉。"


translate chinese epilogue_mi_009c29dd:


    sa "那个，如果是你的话..."


translate chinese epilogue_mi_ac04f5d1:


    "她脸红了。"


translate chinese epilogue_mi_e51d66bf:


    "看起来这个女孩子不但最近动画看得太多，而且生下来脑袋就不太灵光。"


translate chinese epilogue_mi_2e162410:


    "奥尔加·德米特里耶夫娜为什么要设计这样的人物?"


translate chinese epilogue_mi_9290596e:


    "对于我来说，我宁愿让玛莎和萨莎饰演的角色调换一下。"


translate chinese epilogue_mi_52b336ff:


    "总之，这是让导演来决定的，而奥尔加·德米特里耶夫娜是我们唯一的导演。"


translate chinese epilogue_mi_8974ad73:


    dv "这有可能是那个吗?"


translate chinese epilogue_mi_2dcb646d:


    sa "什么?"


translate chinese epilogue_mi_f0ac2d1e:


    "萨莎呆呆的看着阿丽夏。"


translate chinese epilogue_mi_449f4ec4:


    my "不会。"


translate chinese epilogue_mi_24bd3c14:


    dv "你这是什么意思，完全没有?"


translate chinese epilogue_mi_604efd9d:


    my "这个场景里没有。"


translate chinese epilogue_mi_f0ac5acd:


    dv "所以以后会有的。"


translate chinese epilogue_mi_26c83201:


    "阿丽夏阴险的笑着。"


translate chinese epilogue_mi_c5d3868a:


    my "如果你这个好奇，不如去读一读剧本!"


translate chinese epilogue_mi_6e91e7e9:


    dv "不，我完全不好奇。"


translate chinese epilogue_mi_f445a737:


    "阿丽夏站了起来。"


translate chinese epilogue_mi_e08e50aa:


    dv "好了，我要走了。"


translate chinese epilogue_mi_79ef6a3f:


    sa "那个，到底是什么，“那个”是什么?"


translate chinese epilogue_mi_1e98cfca:


    my "没什么。你去准备准备，太阳已经快落山了。"


translate chinese epilogue_mi_91e87806:


    sa "喵喵!"


translate chinese epilogue_mi_7cbc1a04:


    "萨莎站起来走向门口。"


translate chinese epilogue_mi_248dbf88:


    "我吃完自己的土豆，然后长长的叹了一口气。"


translate chinese epilogue_mi_80428fb2:


    "嗯...以这个速度我们根本拍不完一半的电影。"


translate chinese epilogue_mi_398ea31a:


    "我想我们可以在最后写上“to be continued”。"


translate chinese epilogue_mi_25434a55:


    "然后变成一个长篇史诗，由三线影院的一群学生创作。"


translate chinese epilogue_mi_b5b110eb:


    "突然我发现，自己不是那么在意..."


translate chinese epilogue_mi_292fb47d:


    sh "准备好了吗?"


translate chinese epilogue_mi_95ff9942:


    "我转过身，看到了舒里克。"


translate chinese epilogue_mi_8b6b532c:


    my "我也没有什么可准备的...{w}你觉得你可以拍出来吗?"


translate chinese epilogue_mi_02458859:


    sh "额...我试试吧..."


translate chinese epilogue_mi_5141016c:


    my "啊，那咱们开始吧。"


translate chinese epilogue_mi_a20cefa7_2:


    "..."


translate chinese epilogue_mi_d4c057a8:


    "晚上终于凉快了下来，应该说是有点太冷了。"


translate chinese epilogue_mi_334bdb32:


    "我坐在一颗树桩上，试图暖和起来，其他人在调试相机还有灯光。"


translate chinese epilogue_mi_c38f964d:


    ma "我为什么要做这些事？我都没有一个角色!"


translate chinese epilogue_mi_8944594a:


    "玛莎在树林中设置着反光板。"


translate chinese epilogue_mi_373f8138:


    mt "还能有谁？我们人手紧张!"


translate chinese epilogue_mi_19ae045d:


    ma "你可以叫更多人来嘛！谁都想参加这种大作的拍摄的！"


translate chinese epilogue_mi_b468297c:


    mt "你又在抱怨吗?"


translate chinese epilogue_mi_f9ea9881:


    "奥尔加·德米特里耶夫娜严厉的看着她。"


translate chinese epilogue_mi_73f6d6b1:


    ma "没...没有，我很好..."


translate chinese epilogue_mi_ee161131:


    "大概只有面对我们的辅导员的时候玛莎可能会犹豫一下。"


translate chinese epilogue_mi_711dadab:


    "我很奇怪这是为什么?"


translate chinese epilogue_mi_a20cefa7_3:


    "..."


translate chinese epilogue_mi_4e70f7d1:


    "舒里克坐在我旁边。"


translate chinese epilogue_mi_7cda594d:


    sh "你怎么看，我们可能在两到三段里解决吗?"


translate chinese epilogue_mi_a45d1642:


    my "我不知道...又不是我能说了算的..."


translate chinese epilogue_mi_61482050:


    "我对萨莎点点头，她正在湖边跳来跳去的，为了即将开始的表演做着热身。"


translate chinese epilogue_mi_fbf62cb1:


    sh "噢，我明白了..."


translate chinese epilogue_mi_9687a380:


    "舒里克悲剧的叹了口气，然后返回照相机处。"


translate chinese epilogue_mi_b1ce9623:


    mt "好了，时间到了。"


translate chinese epilogue_mi_8b3f4a18:


    "我藏在一颗树后。"


translate chinese epilogue_mi_00dabc53:


    mt "开始!"


translate chinese epilogue_mi_84b9f42b:


    "萨莎几乎是舞蹈着从我身边走过。"


translate chinese epilogue_mi_f2d8c1c4:


    "我不得不说有这样的灯光，还有美丽的月光照耀着，她看起来就像是一个呆子。"


translate chinese epilogue_mi_db64b3c4:


    "但是那和我没有什么关系，希望能拍的好一点。"


translate chinese epilogue_mi_686c7490:


    "我悄悄地出现在树后面，然后慢慢的跟着她。"


translate chinese epilogue_mi_a24c889e:


    "萨莎沿着岸边走着，一边欢快的跳着一边解着衬衣的扣子。"


translate chinese epilogue_mi_dac31f5f:


    "事实上看起来妙极了。"


translate chinese epilogue_mi_7dd610b2:


    "我打赌她可以成为默片的头牌。"


translate chinese epilogue_mi_efe25434:


    "很快她消失在了林间，我在沙滩上走了一会儿，然后也跟着她走进了树丛。"


translate chinese epilogue_mi_d536fac8_1:


    mt "Cut!"


translate chinese epilogue_mi_1b2c2d2e:


    "萨莎马上从林间窜了出来。"


translate chinese epilogue_mi_7e7cffad:


    sa "怎么样？怎么样了？"


translate chinese epilogue_mi_78bad556:


    mt "很好，完美！如果你每次都能表演的像这次一样！一次就过！那我们就可以按时完成了！"


translate chinese epilogue_mi_9ae232e3:


    my "你说得好像我们本来不能按时完成似的。"


translate chinese epilogue_mi_e56d1312:


    "我挑剔的说着。"


translate chinese epilogue_mi_ef558f42:


    mt "没问题，咱们能做到的!"


translate chinese epilogue_mi_cfa206cd:


    ma "怎么，这是怎么个意思?"


translate chinese epilogue_mi_fb33457e:


    "玛莎突然不知道从哪里出现在了我的身边。"


translate chinese epilogue_mi_72fd9d16:


    mt "你还想要别的什么吗?"


translate chinese epilogue_mi_d28ef18c:


    ma "我们准备了三个小时，只为了拍半分钟的片段?"


translate chinese epilogue_mi_9e2ea357:


    mt "这是电影！你懂不懂，这是艺术！一个镜头可以用几年时间来拍！一部五分钟的电影可以拍几十年！"


translate chinese epilogue_mi_e60cdb23:


    "奥尔加·德米特里耶夫娜那样子挥舞着手臂，看起来非常搞笑。"


translate chinese epilogue_mi_40ef1a37:


    ma "好吧..."


translate chinese epilogue_mi_11d5d9f7:


    "玛莎自言自语着。"


translate chinese epilogue_mi_ab8a7c84:


    ma "这就完了?"


translate chinese epilogue_mi_438a5ff3:


    mt "是的，今晚就到此为止了。"


translate chinese epilogue_mi_2e22c834:


    ma "晚安，大家!"


translate chinese epilogue_mi_8227612c:


    "她把反光板扔在地上，然后匆匆离开了营地。"


translate chinese epilogue_mi_7b48d8d6:


    "我也很快打了招呼，然后拿上自己的剧本追上她。"


translate chinese epilogue_mi_a20cefa7_4:


    "..."


translate chinese epilogue_mi_333411d8:


    my "嘿!"


translate chinese epilogue_mi_58a251f7:


    "我到了广场才追上玛莎。"


translate chinese epilogue_mi_17c3f085:


    ma "噢，现在你又想干什么?"


translate chinese epilogue_mi_f72fd8e2:


    "玛莎的脸上充满了疲惫。"


translate chinese epilogue_mi_6e8fe88f:


    my "也许你可以和我散散步?"


translate chinese epilogue_mi_cc3d4e3e:


    ma "有这个必要吗?"


translate chinese epilogue_mi_4de002b5:


    "她显然有点误解了。"


translate chinese epilogue_mi_d67add20:


    my "那个，现在，太早了...! 我打赌在城市里这个时间你肯定不会去睡觉的。"


translate chinese epilogue_mi_accf297a:


    ma "城市里...{w} 这里又不是城市，你知道吗。这里度日如年。"


translate chinese epilogue_mi_36f15bc5:


    my "哎呀，来吧，现在还早的很呢!"


translate chinese epilogue_mi_fcbee1a6:


    ma "啊，好吧..."


translate chinese epilogue_mi_943eaf3a:


    "她又恢复了一些精神，微笑着看着我。"


translate chinese epilogue_mi_6d8953b3:


    ma "不要太久喔!"


translate chinese epilogue_mi_443f7100:


    my "是，大小姐!"


translate chinese epilogue_mi_21bb5d23:


    ma "你为什么要一直带着那个?"


translate chinese epilogue_mi_799227a4:


    "她指着我手里的一打纸。"


translate chinese epilogue_mi_58f54863:


    my "嗯，你永远不知道会发生什么..."


translate chinese epilogue_mi_a20cefa7_5:


    "..."


translate chinese epilogue_mi_6299873c:


    "很快，我们来到了沙滩。"


translate chinese epilogue_mi_ee06f3f9:


    ma "好美的月亮!"


translate chinese epilogue_mi_41699bfb:


    "玛莎脱掉衣服冲进河里。"


translate chinese epilogue_mi_9ee4fa9d:


    "我不知道自己在期待着什么，不过她的少先队员制服下面穿着比基尼泳装。"


translate chinese epilogue_mi_ca443530:


    "我着迷的看着她在水中拍打着双腿，带起一片片涟漪。"


translate chinese epilogue_mi_14ea44c3:


    my "所以说你还是可以玩的起来的嘛!"


translate chinese epilogue_mi_91d35e95:


    "我喊着。"


translate chinese epilogue_mi_e2efb894:


    "她生气的看了我一眼，然后走了过来，坐在了我的身边。"


translate chinese epilogue_mi_2a3057e6:


    ma "是啊，最近你好像一直把我当成是一个老圆规。"


translate chinese epilogue_mi_afbc1bb1:


    my "当然不是了...不过你还是一个牢骚鬼。"


translate chinese epilogue_mi_64383958:


    ma "我想我有自己的理由!"


translate chinese epilogue_mi_7d565efc:


    my "不确定...我没有发现什么特别的理由。"


translate chinese epilogue_mi_c7de3691:


    "玛莎站了起来，然后离开了几步。"


translate chinese epilogue_mi_ca00ddd3:


    my "好了啦!"


translate chinese epilogue_mi_ec961d69:


    ma "别这样，一个人不可能这样..."


translate chinese epilogue_mi_7ddbab17:


    my "这样什么?"


translate chinese epilogue_mi_5dc90687:


    ma "你什么都没有看到..."


translate chinese epilogue_mi_67ff3dab:


    "她抱着头悄悄地说着。"


translate chinese epilogue_mi_b9140652:


    ma "你也从来都不理解..."


translate chinese epilogue_mi_b4a3d493:


    my "是啊，我就是这样——冷血，白痴又无聊。"


translate chinese epilogue_mi_8961f09f:


    "玛莎用几乎听不到的音量笑了笑。"


translate chinese epilogue_mi_d58a855b:


    ma "是啊，就是这样..."


translate chinese epilogue_mi_25675418:


    ma "咱们去游泳吧?"


translate chinese epilogue_mi_18eae243:


    my "那边太冷了!"


translate chinese epilogue_mi_7e8feba7:


    ma "时间不会太长的。"


translate chinese epilogue_mi_520e734c:


    my "那...好吧。"


translate chinese epilogue_mi_a20cefa7_6:


    "..."


translate chinese epilogue_mi_9bbf09a7:


    "让人惊讶的是河水很温暖，所以我们确实不会冻着。"


translate chinese epilogue_mi_cb0a184a:


    "我坐在一条不知道谁放在这里的毛巾上，呼吸着夜晚柔软的空气。"


translate chinese epilogue_mi_38d4cee4:


    "玛莎躺在我的前面，把头搭在我的膝盖上休息着。"


translate chinese epilogue_mi_404343d4:


    ma "我打扰到你了吗?"


translate chinese epilogue_mi_6829da13:


    my "没...完全没有..."


translate chinese epilogue_mi_ab837517:


    ma "我只是有点累了。"


translate chinese epilogue_mi_a5dd8896:


    "她直直的盯着我的眼睛，但是我避开了她的目光。"


translate chinese epilogue_mi_b362f783:


    ma "为什么不能和原来一样了?"


translate chinese epilogue_mi_d70ebca4:


    my "怎么?"


translate chinese epilogue_mi_00d724da:


    ma "好啦..."


translate chinese epilogue_mi_02c9d340:


    my "你知道，这是一个关键的决定..."


translate chinese epilogue_mi_ea890fe1:


    ma "但是尽管如此..."


translate chinese epilogue_mi_e9145191:


    "她抱着我的腰，让我感觉更不舒服了。"


translate chinese epilogue_mi_b97bb617:


    ma "话说咱们为什么要来这里?"


translate chinese epilogue_mi_2baba03c:


    my "来拍电影!"


translate chinese epilogue_mi_bb9ef712:


    "我试图用笑话掩盖过去。"


translate chinese epilogue_mi_04598414:


    ma "不过如果不是你，我也就不会上这所大学，...{w}我们可能已经..."


translate chinese epilogue_mi_040d056a:


    "她叹叹气。"


translate chinese epilogue_mi_47224973:


    ma "但是现在..."


translate chinese epilogue_mi_e953effd:


    my "我还是觉得在这里回忆过去不是一个好主意。"


translate chinese epilogue_mi_655cfe62:


    ma "你可以这么想，对我来说，我觉得咱们本没有必要..."


translate chinese epilogue_mi_1f24965d:


    my "但是这是你先提出来的!"


translate chinese epilogue_mi_981566ae:


    ma "我从来没有这样想过!"


translate chinese epilogue_mi_c2f0d527:


    "玛莎怨愤的说。"


translate chinese epilogue_mi_3788408c:


    my "啊，是吗，那么这是我导致的?!"


translate chinese epilogue_mi_5686c565:


    ma "是的，就是你!"


translate chinese epilogue_mi_27ebdb73:


    "她转了过去，但是没有离开我。"


translate chinese epilogue_mi_1221beae:


    my "你总是这样...你不喜欢这个...你不喜欢那个...你为什么不能接受简单的生活？抬头看看吧，现在是美好的夏天，一个美好的夜晚，咱们在拍摄一部经典的电影，事实上，一切都是那么美妙..."


translate chinese epilogue_mi_b4204e75:


    ma "你自己真的那样相信吗?"


translate chinese epilogue_mi_a5b0e9d3:


    "她的这个问题确实有道理"


translate chinese epilogue_mi_bfa1d965:


    my "从一定程度上来说是的，事情不会总是那么理想化的...{w} 但是又有什么糟糕的呢?"


translate chinese epilogue_mi_538b1472:


    ma "一切都很糟糕..."


translate chinese epilogue_mi_1b164615:


    "她好像已经快要哭出来了。"


translate chinese epilogue_mi_96380f1d:


    ma "这个垃圾的夏令营...还有你的剧本。{w} 一切！一切都很糟糕！而且你也不再是一个孩子！你本来可以成家，找到工作..."


translate chinese epilogue_mi_56a282e0:


    my "嘿!{w}我们已经讨论了一千遍了!"


translate chinese epilogue_mi_c7401555:


    ma "是啊，而且因此而分手。"


translate chinese epilogue_mi_04774682:


    my "那是你的观点。一定程度上就是因为你的一切都很糟糕的理论。"


translate chinese epilogue_mi_0ab1e883:


    ma "但是总是想着一切都是那么美好就能真的美好起来似的！只要傻瓜才会见到什么都享受个不停。"


translate chinese epilogue_mi_af9c4714:


    my "我不是享受...应该说我享受所有值得我们享受的东西! 我喜欢我的工作，我觉得拍电影很快乐。"


translate chinese epilogue_mi_9c47bb7a:


    "真的吗?"


translate chinese epilogue_mi_bbfdf3e3:


    ma "就是说你从我这里找不到快乐?"


translate chinese epilogue_mi_39cd6f4b:


    "她起来靠着我的胸膛。"


translate chinese epilogue_mi_995dd1da:


    my "呃，也许我们不应该...?"


translate chinese epilogue_mi_4b4f76bd:


    ma "有什么关系呢？你不想吗?"


translate chinese epilogue_mi_3f27c2c0:


    "我思考不出来应该如何回答。"


translate chinese epilogue_mi_dd178882:


    "一方面来说，我不想揭开过去的伤疤，但是从我的立场来看，我又没有拒绝的权利。"


translate chinese epilogue_mi_113ceef8:


    "而且现在我也不在乎会不会有没睡着的人看到我们..."


translate chinese epilogue_mi_32d9bac4:


    my "但是，为什么呢，然后呢?"


translate chinese epilogue_mi_64fd49e3:


    ma "谁在乎啊..."


translate chinese epilogue_mi_9eb4aadf:


    my "嗯...既然如此..."


translate chinese epilogue_mi_a20cefa7_7:


    "..."


translate chinese epilogue_mi_f1dcbafd:


    "闪耀的月光照亮她裸露的肌肤，凉爽的夏夜还有她身体的温度，粗重的喘息声，窒息的快乐，都在这个瞬间。"


translate chinese epilogue_mi_a20cefa7_8:


    "..."


translate chinese epilogue_mi_71390bb9:


    my "到了睡觉时间了。{w}明天会很累的，奥尔加·德米特里耶夫娜肯定会使劲折腾咱们的。"


translate chinese epilogue_mi_d5b285ba:


    "我慢慢的扣上自己的扣子，然后看着玛莎换衣服。"


translate chinese epilogue_mi_d1464071:


    ma "我不想自己睡..."


translate chinese epilogue_mi_a68bf534:


    "她悄悄地说着。"


translate chinese epilogue_mi_337290ce:


    my "怎么，害怕蚱蜢?"


translate chinese epilogue_mi_4562c02a:


    ma "不是蚱蜢，是蟋蟀，而且我不是害怕它们..."


translate chinese epilogue_mi_42869b08:


    "她避开了我的目光，有点脸红。"


translate chinese epilogue_mi_7ee55b20:


    ma "只是...有点...不太寻常。"


translate chinese epilogue_mi_f7e0940e:


    "这么长时间，可能已经习惯了吧。"


translate chinese epilogue_mi_b1cabb3f:


    "我突然觉得这个观点没有任何问题。{w}尤其是考虑到发生了这么多事情..."


translate chinese epilogue_mi_e9e6e7f9:


    my "好吧..."


translate chinese epilogue_mi_97c79ba2:


    ma "但是在哪儿?"


translate chinese epilogue_mi_19aed9c9:


    "玛莎不解的看着我。"


translate chinese epilogue_mi_c405517a:


    my "有一些没有人的房间。{w}我想咱们可以暂时占用一下。"


translate chinese epilogue_mi_7dc61d1e:


    ma "好吧。"


translate chinese epilogue_mi_ab3f1fe7:


    "她笑着站起来，拉着我的手，然后我们一起走向广场..."


translate chinese epilogue_mi_a20cefa7_9:


    "... （前方剧情涉及恐怖内容，请留意）"


translate chinese epilogue_mi_6c0b31c2:


    "早晨的阳光晃着我的眼睛。"


translate chinese epilogue_mi_cbb4c74c:


    "我没办法伸懒腰，玛莎睡在我的左胳膊上。"


translate chinese epilogue_mi_050d92e1:


    "我费力的把胳膊抽出来，不停地揉着，试图让血液回复流通。"


translate chinese epilogue_mi_d84db049:


    "我摸摸自己的胡茬，伸手找到我的烟。"


translate chinese epilogue_mi_37be2fed:


    "也许昨天我们本不应该过那条线的。"


translate chinese epilogue_mi_15aaeab9:


    "现在她可不会放我一个人走了。"


translate chinese epilogue_mi_583a04fd:


    "大概是的...她就是这样的人。"


translate chinese epilogue_mi_412be39c:


    "但还是会留下不舒服的感觉。"


translate chinese epilogue_mi_66fafbd5:


    "现在只要我给她机会，就肯定会挨一顿骂。"


translate chinese epilogue_mi_7502f7b0:


    "吵架——她的生活里可从来不缺少这个。"


translate chinese epilogue_mi_d81d21a1:


    "我在自己的短裤里找不到烟，觉得应该是忘在自己的房间里了，所以我笨拙的穿上衣服走了出来。"


translate chinese epilogue_mi_a46f5080:


    "天哪，至少下点雨吧，在这种火炉里怎么工作？"


translate chinese epilogue_mi_ee1382fb:


    "而且这日复一日..."


translate chinese epilogue_mi_8aef407b:


    "我揉揉眼睛，拖着沉重的脚步走着，没有注意到有人往我的方向跑来，结果我们撞在了一起。"


translate chinese epilogue_mi_43c329b2:


    "是Router。"


translate chinese epilogue_mi_14d3fbf7:


    ro "你好，谢苗!"


translate chinese epilogue_mi_c54301b0:


    my "你好!"


translate chinese epilogue_mi_c942874a:


    "我对他喊着。"


translate chinese epilogue_mi_95f95256:


    "最后我终于回到了自己的房间，打开了房门。"


translate chinese epilogue_mi_9fc335aa:


    "奥尔加·德米特里耶夫娜在里面，恶狠狠地盯着我。"


translate chinese epilogue_mi_d8b1d698:


    mt "你有没有什么要解释的?"


translate chinese epilogue_mi_cabd0ae8:


    my "怎么了?"


translate chinese epilogue_mi_cd667d28:


    mt "你昨天晚上去哪儿了?!"


translate chinese epilogue_mi_a30d42e4:


    "她的眼神中充满了威胁。"


translate chinese epilogue_mi_71fcb370:


    my "我和玛莎在一起...这怎么了?"


translate chinese epilogue_mi_2e8f7dad:


    "反正大家都知道了，我也没想藏着掖着。"


translate chinese epilogue_mi_3d8b8a51:


    mt "和谁?"


translate chinese epilogue_mi_af66aeeb:


    "奥尔加·德米特里耶夫娜惊讶的看着我。"


translate chinese epilogue_mi_3b56ad7d:


    my "和玛莎。"


translate chinese epilogue_mi_d18d27a7:


    "我冷静的说着。"


translate chinese epilogue_mi_7fb436a3:


    mt "我们这里没有叫玛莎的人。"


translate chinese epilogue_mi_a55c62b2:


    my "这下轮到我吃惊了..."


translate chinese epilogue_mi_c04c6271:


    mt "谢苗，你的行为举止可不像是一个少先队员!"


translate chinese epilogue_mi_22f7322a:


    my "啊，我们还没有开始拍吗？摄影机什么的都在哪里？"


translate chinese epilogue_mi_fbd62a8b:


    "我嬉皮笑脸的。"


translate chinese epilogue_mi_105fc865:


    mt "谢苗!"


translate chinese epilogue_mi_2151a69f:


    my "哎呀..."


translate chinese epilogue_mi_fe1c1f1f:


    "我走到自己的床边，打开抽屉。"


translate chinese epilogue_mi_47e0b7e7:


    "然而，我的书包不在里面。"


translate chinese epilogue_mi_3321c1b4:


    my "呃...你看到我的书包了吗?"


translate chinese epilogue_mi_5c5b691a:


    mt "书包?"


translate chinese epilogue_mi_efd76d15:


    "她又惊讶的看着我。"


translate chinese epilogue_mi_67d1a8d9:


    my "是啊，在这里来着。"


translate chinese epilogue_mi_e6ca34e7:


    mt "但是你来的时候没有带任何行李..."


translate chinese epilogue_mi_b9e79bf2:


    my "什么?"


translate chinese epilogue_mi_ff99c828:


    "我傻笑着。"


translate chinese epilogue_mi_74a4c096:


    mt "而且我还没有忙完!"


translate chinese epilogue_mi_221aaa4e:


    my "奥尔加·德米特里耶夫娜，我明白了，你今天早上心情很好，想要开玩笑，但是..."


translate chinese epilogue_mi_f8da58c6:


    mt "你作为一个少先队员，一个未来的共青团员，居然这样表现，这怎么可能让我心情好?!"


translate chinese epilogue_mi_5a64d54b:


    "少先队员？未来的共青团员？这好像不太对劲。"


translate chinese epilogue_mi_118c25a9:


    my "奥尔加·德米特里耶夫娜，你睡好了吗？你没问题吧？"


translate chinese epilogue_mi_06543492:


    mt "你敢这么和我说话?!"


translate chinese epilogue_mi_6a4d0b42:


    my "呃，你到底怎么回事?"


translate chinese epilogue_mi_eae86bdc:


    mt "总之，你要在半个小时之内去集合，我得去找未来。"


translate chinese epilogue_mi_ad54eb5e:


    "未来？看起来他们好像就是想要恶作剧。"


translate chinese epilogue_mi_443f7100_1:


    my "是!"


translate chinese epilogue_mi_987f165b:


    "我敬了礼然后离开了房间。"


translate chinese epilogue_mi_287b8014:


    "我得去告诉玛莎，跟她讲讲这个笑话。"


translate chinese epilogue_mi_bbbfbbde:


    "我兴奋的推开房门走了进去。"


translate chinese epilogue_mi_b191b3f3:


    my "玛莎，你不会相信他们..."


translate chinese epilogue_mi_7225eb18:


    "玛莎坐在角落里，脸色苍白，抱着双腿。"


translate chinese epilogue_mi_850753d4:


    my "怎么回事?"


translate chinese epilogue_mi_ab6c8876:


    "我冲到她的身边。"


translate chinese epilogue_mi_36e80422:


    ma "列娜走了进来..."


translate chinese epilogue_mi_8143f26a:


    my "列娜？但是她计划三天以后才来..."


translate chinese epilogue_mi_a1cd396c:


    ma "不是我们的列娜...这...只是一个看起来像是列娜的..."


translate chinese epilogue_mi_6e1e7593:


    "玛莎颤抖着，脸上浮现出恐惧的表情。"


translate chinese epilogue_mi_c0db5536:


    my "冷静！慢慢说!"


translate chinese epilogue_mi_adfebabf:


    ma "她不停地说什么列队集合，打扫卫生，图书馆，还有什么音乐社团..."


translate chinese epilogue_mi_4342489d:


    "她变得呼吸急促。"


translate chinese epilogue_mi_e50ade73:


    ma "而且...这不是开玩笑！我说真的!"


translate chinese epilogue_mi_eab4b893:


    "我马上想起了刚刚和奥尔加·德米特里耶夫娜的对话。"


translate chinese epilogue_mi_8877ce04:


    my "好吧，冷静，我还没有明白!"


translate chinese epilogue_mi_dd83c236:


    ma "那不是我们的列娜...而是...你的剧本中的列娜!"


translate chinese epilogue_mi_84cf572d:


    "她用充满泪水的眼睛看着我，我马上明白了这不是开玩笑，我的勇气开始逐渐流失。"


translate chinese epilogue_mi_a944f157:


    my "这太扯淡了！不可能啊..."


translate chinese epilogue_mi_ccbb0e0e:


    "虽然想想和导演的对话..."


translate chinese epilogue_mi_758f8c33:


    ma "不！不！我确定!"


translate chinese epilogue_mi_dfc67743:


    "她投入我的怀抱。"


translate chinese epilogue_mi_ba41a0fe:


    "我试图安慰她。"


translate chinese epilogue_mi_2ea136fe:


    "我们得理智的思考。"


translate chinese epilogue_mi_020471bd:


    my "咱们得理智一些，这只是他们的一个恶作剧，就是这样，我刚刚去找奥尔加·德米特里耶夫娜谈话，然后..."


translate chinese epilogue_mi_9465eed8:


    "她怎么？看起来也很不正常？"


translate chinese epilogue_mi_f5c1c55b:


    "呃，我不能就这样告诉玛莎..."


translate chinese epilogue_mi_6d353af2:


    my "他们最后笑完就会停下来的"


translate chinese epilogue_mi_fd57f767:


    ma "不，真的发生了什么...发生了可怕的事情...我确定..."


translate chinese epilogue_mi_8974dd9f:


    "玛莎在我的怀抱中抽泣着。"


translate chinese epilogue_mi_40789c4e:


    my "好吧，冷静一下，咱们去找他们聊聊。{w}比如说Router，他不像是会讲笑话的人。"


translate chinese epilogue_mi_95c191c3:


    "说时候我现在也很有兴趣，是谁能让这么冷静的玛莎变得歇斯底里。"


translate chinese epilogue_mi_40ef1a37_1:


    ma "好吧..."


translate chinese epilogue_mi_78e86177:


    "她擦干眼泪，坐了起来。"


translate chinese epilogue_mi_40e78541:


    my "冷静一些了吗?"


translate chinese epilogue_mi_0d0c986a:


    ma "尽量..."


translate chinese epilogue_mi_f9eeb73d:


    "玛莎苍白的笑着。"


translate chinese epilogue_mi_cfd5f29b:


    ma "咱们走吧..."


translate chinese epilogue_mi_279dbfa1:


    "我们离开了房间。"


translate chinese epilogue_mi_04e16600:


    ma "去哪儿呢?"


translate chinese epilogue_mi_9164b34c:


    my "奥尔加·德米特里耶夫娜在说什么集合，咱们去广场吧，他们最有可能在那里。"


translate chinese epilogue_mi_499411c9:


    "玛莎拉住了我的手，不过马上又松开了。"


translate chinese epilogue_mi_00660cbc:


    "我没有看着她，因为我已经知道她的表情了，我们就这样前往了广场。"


translate chinese epilogue_mi_a20cefa7_10:


    "..."


translate chinese epilogue_mi_20361ed5:


    "队列很拥挤。"


translate chinese epilogue_mi_eb1f0748:


    "我连一半的人都不认识，单是这件事就已经很奇怪了。"


translate chinese epilogue_mi_e02d4ff6:


    "奥尔加·德米特里耶夫娜试图让他们全都站好。"


translate chinese epilogue_mi_c44b27a4:


    "我不再闲逛，而是直接朝她走去。"


translate chinese epilogue_mi_1ddcb8c6:


    my "奥尔加·德米特里耶夫娜，你知道..."


translate chinese epilogue_mi_9c242b70:


    mt "啊，还有未来也在这里!"


translate chinese epilogue_mi_0d425fdb:


    "她打断了我。"


translate chinese epilogue_mi_0f83334b:


    mt "很好，我们在集合结束以后再说。"


translate chinese epilogue_mi_809254d9:


    my "你在说什么集合...{w}你玩的还不够吗？天哪，这已经一点也不好玩了！"


translate chinese epilogue_mi_4de002b5_1:


    "她困惑的看着我。"


translate chinese epilogue_mi_a82eeb20:


    mt "你在说什么?"


translate chinese epilogue_mi_be2c12c6:


    my "就是那个!"


translate chinese epilogue_mi_5427ae40:


    "玛莎小心的从我背后探出头看着。"


translate chinese epilogue_mi_7609eb24:


    ma "奥尔加·德米特里耶夫娜，真的已经不好笑了..."


translate chinese epilogue_mi_b8ebaa25:


    mt "我不明白你们两个..."


translate chinese epilogue_mi_47e141b6:


    my "咱们严肃点说拍片的事情!"


translate chinese epilogue_mi_deb210f9:


    mt "拍什么?"


translate chinese epilogue_mi_ea1863fe:


    "我严肃的看着她，第一次感觉到她可能并不是在开玩笑。"


translate chinese epilogue_mi_e369f105:


    "奥尔加·德米特里耶夫娜从来就不是一个好演员，所以不太可能把这种惊讶又不解的感觉装得这么像..."


translate chinese epilogue_mi_f385c640:


    "真是一团糟..."


translate chinese epilogue_mi_55374b08:


    my "好吧，咱们假设...告诉我你的版本。"


translate chinese epilogue_mi_717e5682:


    mt "什么的版本?"


translate chinese epilogue_mi_0da1d3c7:


    "她还在不停的眨着眼睛。"


translate chinese epilogue_mi_f33afa6e:


    my "我明白了。"


translate chinese epilogue_mi_1af119c3:


    "我拉住玛莎的手然后离开了广场。"


translate chinese epilogue_mi_3dccb76f:


    ma "嘿，等一下，你去哪儿?"


translate chinese epilogue_mi_e5c4f40a:


    my "咱们得找一个没有这么发疯的人。"


translate chinese epilogue_mi_4742eb97:


    "奥尔加·德米特里耶夫娜在我们背后喊着，但是我没有管她。"


translate chinese epilogue_mi_a20cefa7_11:


    "..."


translate chinese epilogue_mi_c718bb72:


    "很快我们来到了社团活动室。"


translate chinese epilogue_mi_03304259:


    "根据剧本应该就在这里。"


translate chinese epilogue_mi_448b06ba:


    my "咱们走吧。"


translate chinese epilogue_mi_f81758d9:


    "玛莎没有回答，只是默默地跟着我。"


translate chinese epilogue_mi_bd1a2f8e:


    "我猛地打开门，然后冲了进去。"


translate chinese epilogue_mi_d2107504:


    "Router如我所料就在这里。"


translate chinese epilogue_mi_7feccd3d:


    my "怎么回事?"


translate chinese epilogue_mi_dc50fc49:


    ro "啊，谢苗，你还不是一个人！你好！你想要加入我们的社团吗?"


translate chinese epilogue_mi_b9c61f4c:


    "看起来他也有点不正常。"


translate chinese epilogue_mi_c2ed7553:


    my "你为什么没有去集合?"


translate chinese epilogue_mi_81684e59:


    "我怀疑的问。"


translate chinese epilogue_mi_e3b581e3:


    ro "呃...{w}那个，我这里还有点事。"


translate chinese epilogue_mi_db19aca1:


    my "啊，随便你，只要告诉我这里发生了什么事情。"


translate chinese epilogue_mi_a9ea475f:


    ro "我有错过什么吗?"


translate chinese epilogue_mi_bb291d76:


    "就像奥尔加·德米特里耶夫娜一样，他完全不解的看着我们。"


translate chinese epilogue_mi_3286aba0:


    my "为什么他们都好像真的在一个少先队夏令营里做少先队员?"


translate chinese epilogue_mi_50edcb0a:


    ro "难道不是这样的嘛?"


translate chinese epilogue_mi_08a831fd:


    my "是这样?"


translate chinese epilogue_mi_95e46bc6:


    ro "那个，事实上..."


translate chinese epilogue_mi_3a9f1644:


    my "电影怎么办?"


translate chinese epilogue_mi_5fe1cb2a:


    ro "什么电影?"


translate chinese epilogue_mi_48f2e671:


    my "电影...咱们正在拍的电影!"


translate chinese epilogue_mi_71aef04d:


    ro "谢苗，我真的不知道你在说什么..."


translate chinese epilogue_mi_114b3078:


    "知道现在我才发现这里的人都叫我谢苗..."


translate chinese epilogue_mi_3af97c5c:


    "好吧，萨沙也经常叫我谢苗君..."


translate chinese epilogue_mi_2bdf9616:


    "不过那只是开玩笑，但是现在..."


translate chinese epilogue_mi_a28e10ab:


    "这不是什么太重要的事情，但是我的名字绝对不是谢苗!"


translate chinese epilogue_mi_fc0ed84b:


    "我恐惧的向后退着，绊倒了玛莎。"


translate chinese epilogue_mi_4a4eea7d:


    "她短促的尖叫了一声。"


translate chinese epilogue_mi_179c9e45:


    ro "还有你未来..."


translate chinese epilogue_mi_1a34a91c:


    "我没有听..."


translate chinese epilogue_mi_2264c5bc:


    "我拉住玛莎的手跑了出去。"


translate chinese epilogue_mi_65d0e61a:


    "我知道公交车站才停下。"


translate chinese epilogue_mi_90269e81:


    "我转过身，看到玛莎在不停的喘着粗气。"


translate chinese epilogue_mi_e8deb2c6:


    "我放开了她的手，坐在了路边。"


translate chinese epilogue_mi_2a47862d:


    my "看来咱们两个有麻烦了..."


translate chinese epilogue_mi_9ddc03d3:


    ma "..."


translate chinese epilogue_mi_f91617dc:


    my "你还没有明白吗，这里所有的人都按照我写的剧本行动!"


translate chinese epilogue_mi_e0b753af:


    "她先是呆呆的看着我，然后反应过来:"


translate chinese epilogue_mi_50ba87dc:


    ma "是啊，有道理...但是，为什么呢?"


translate chinese epilogue_mi_44093b8e:


    my "我也不知道啊..."


translate chinese epilogue_mi_8389024e:


    ma "那么，咱们应该怎么办?"


translate chinese epilogue_mi_326ad2f9:


    my "还是不知道啊..."


translate chinese epilogue_mi_a35f3a53:


    "玛莎看起来快要哭出来了。"


translate chinese epilogue_mi_d4708852:


    my "别啊，别这样，我真的没有办法了，这种情况下我怎么会有准备好的办法呢？"


translate chinese epilogue_mi_01180556:


    ma "我明白，但是..."


translate chinese epilogue_mi_f59e1b10:


    my "再怎么说，这也有可能真的是一个恶作剧...不过已经过头了。"


translate chinese epilogue_mi_63ef8b13:


    ma "但是万一...?"


translate chinese epilogue_mi_10030a3d:


    my "那，如果...那样我们就得将计就计。"


translate chinese epilogue_mi_3e730633:


    "我叹叹气，匆匆站了起来。"


translate chinese epilogue_mi_6f413eb8:


    my "咱们至少去吃点东西啊。{w}反正不管这里发生着什么，总是还要提供伙食的。"


translate chinese epilogue_mi_ee84d14d:


    "玛莎淡淡的微笑了一下，然后跟着我。"


translate chinese epilogue_mi_a20cefa7_12:


    "..."


translate chinese epilogue_mi_4cb70312:


    "食堂人满为患。"


translate chinese epilogue_mi_08c3283d:


    "即使在集合的时候我也注意到这里的人比昨天多很多。"


translate chinese epilogue_mi_e35a16d4:


    "附近的孩子们真的都来这里了吗？来一个已经废弃的夏令营？"


translate chinese epilogue_mi_039e411f:


    "不，关于这点真的是有问题的..."


translate chinese epilogue_mi_c99f77e3:


    "不过还是会分发食物，所以我和玛莎拿了吃的，坐在没有人的地方。"


translate chinese epilogue_mi_fb2c31f0:


    "我专心的吃着，试图不去看周围的人们。"


translate chinese epilogue_mi_67b9e789:


    "然而这没有什么帮助，不久还是有人走了过来。"


translate chinese epilogue_mi_02f457e1:


    "我看到了列娜。"


translate chinese epilogue_mi_0ece5a44:


    un "可以吗...?"


translate chinese epilogue_mi_a86cd0ce:


    "她静静的问着。"


translate chinese epilogue_mi_29741375:


    ma "当然，请便!"


translate chinese epilogue_mi_790d5a8d:


    "我还没有反应过来，玛莎已经拉出了一把椅子邀请列娜坐下来。"


translate chinese epilogue_mi_dac35953:


    un "今天早上你不是你自己..."


translate chinese epilogue_mi_94cc4744:


    "她担心的说着。"


translate chinese epilogue_mi_684d739f:


    ma "嗯...没有睡好..."


translate chinese epilogue_mi_f8d7c8a1:


    "玛莎尴尬的笑了笑。"


translate chinese epilogue_mi_6c544c99:


    "列娜看了我一眼，然后匆匆移开了目光。"


translate chinese epilogue_mi_7bc737ed:


    my "所以说怎么样了?"


translate chinese epilogue_mi_c3c1a949:


    "我问了一个更笼统的问题。"


translate chinese epilogue_mi_b993fafe:


    un "还好。"


translate chinese epilogue_mi_52d92c19:


    my "我明白了..."


translate chinese epilogue_mi_26eb2fcd:


    "我完全不知道我们可以和她讨论什么。"


translate chinese epilogue_mi_30741e46:


    "而且列娜看起来十分诡异，平常充满活力的欢乐的列娜现在居然这么害羞。"


translate chinese epilogue_mi_827f779d:


    un "你听说斯拉维娅的事情了吗?"


translate chinese epilogue_mi_7abdce27:


    my "发生了什么?"


translate chinese epilogue_mi_0c42474d:


    un "那个，从昨天开始就没有人看见她...{w}好像你们两个先是消失...不过你现在在这里...那斯拉维娅..."


translate chinese epilogue_mi_d0cb10a2:


    "她每一句话都经过了长长的停顿。"


translate chinese epilogue_mi_f9611d76:


    "我还是不能理解，要么他们实在是会演，要么我面前的根本就不是列娜。"


translate chinese epilogue_mi_449f4ec4_1:


    my "不。"


translate chinese epilogue_mi_81d2b836:


    ma "我们还没有看见她。"


translate chinese epilogue_mi_69a8f246:


    un "嗯，希望她最后能回来。"


translate chinese epilogue_mi_9b627c74:


    my "是啊，我们也是这么想的。"


translate chinese epilogue_mi_6d56d6ac:


    "斯拉维娅...{w}萨沙确实大白天的都有可能迷路——这倒是不奇怪。"


translate chinese epilogue_mi_531f4fb8:


    "我刚刚吃完这碗像胶皮一样的麦片粥，突然奥尔加·德米特里耶夫娜挥着手走进了食堂。"


translate chinese epilogue_mi_3d70faed:


    "她毫无表情的看着周围，然后向我们的桌子走来。"


translate chinese epilogue_mi_0bab5229:


    mt "噢，你们在这里啊！咱们去谈谈吧!"


translate chinese epilogue_mi_762bfea5:


    "她用眼神示意。"


translate chinese epilogue_mi_af771b64:


    "看来反抗是没有意义的，我对玛莎点点头，然后跟着奥尔加·德米特里耶夫娜走了。"


translate chinese epilogue_mi_45d3dfab:


    "我们出门后，她严厉的问着我:"


translate chinese epilogue_mi_ec4d970a:


    mt "所以说你昨天晚上在哪儿?"


translate chinese epilogue_mi_97132f75:


    my "管你什么...?"


translate chinese epilogue_mi_8e0229dd:


    "我耸耸肩。"


translate chinese epilogue_mi_349b0aba:


    mt "这就是我的事，你知道斯拉维娅失踪了吗?"


translate chinese epilogue_mi_4f716193:


    my "刚刚听列娜说。"


translate chinese epilogue_mi_e6a0d896:


    mt "对！我去找她了，然后...找到了她!"


translate chinese epilogue_mi_b34ccd5d:


    "她似乎在强忍住眼泪。"


translate chinese epilogue_mi_16fc9de2:


    my "那是好消息啊。"


translate chinese epilogue_mi_9175e7a8:


    mt "来，跟我来看看。"


translate chinese epilogue_mi_001b9dd0:


    "她低声说着，然后往森林走去。"


translate chinese epilogue_mi_01ce7e3e:


    "我看看玛莎。"


translate chinese epilogue_mi_e0d7e61d:


    ma "也许咱们不应该去?"


translate chinese epilogue_mi_13269186:


    my "有什么问题吗?"


translate chinese epilogue_mi_ab7331fb:


    "然而我的内心也是这么想的。"


translate chinese epilogue_mi_a14a2f57:


    ma "那好吧...但是我怕...。"


translate chinese epilogue_mi_7851bc23:


    my "没关系的..."


translate chinese epilogue_mi_bef6e9f9:


    "我试图笑笑，但是即使没有镜子我也知道自己的笑容很假，让人难受。"


translate chinese epilogue_mi_c390a24d:


    "很快我们来到了森林中的一块空地。"


translate chinese epilogue_mi_77b14888:


    mt "那里..."


translate chinese epilogue_mi_0498059b:


    "奥尔加·德米特里耶夫娜指向一棵树。"


translate chinese epilogue_mi_477f49be:


    "我往前走了几步，看到一条人类的手臂。{w}事实上它看起来更像是被撕裂下来的。"


translate chinese epilogue_mi_6e403fb6:


    "玛莎从我背后探出头，然后恐惧的大叫起来。"


translate chinese epilogue_mi_3d6cfafe:


    my "别看。!"


translate chinese epilogue_mi_efa05797:


    "她哭了出来，冲进了奥尔加·德米特里耶夫娜的怀抱。"


translate chinese epilogue_mi_3352219f:


    "我费了好大的力气去树后面看看，确认那确实是斯拉维娅。"


translate chinese epilogue_mi_a45eb573:


    "各种器官摆在地上，胳膊，腿，分散在各处，胸腔的碎肉，头发凌乱的脑袋，还有深陷的明亮的眼睛。"


translate chinese epilogue_mi_6ae474eb:


    "我想要吐出来，然后本能的移开了目光。"


translate chinese epilogue_mi_7cb2ad73:


    my "这是谁干的...?"


translate chinese epilogue_mi_a0c3cc82:


    mt "我怎么知道?"


translate chinese epilogue_mi_bd343a2b:


    "奥尔加·德米特里耶夫娜用不太寻常的冷静的声音说着。"


translate chinese epilogue_mi_2cd46005:


    mt "整个营地中，你是唯一一个没有不在场证明的人。"


translate chinese epilogue_mi_9ef4463b:


    "玛莎继续哭着，现在趴在我的肩膀上。"


translate chinese epilogue_mi_8746139c:


    my "什么，你真的觉得我们可能做出这种...事情?!"


translate chinese epilogue_mi_76171197:


    "我愤怒的盯着她。"


translate chinese epilogue_mi_2deb3a6a:


    "现在这里到底是发生了什么已经不重要了，到底是恶作剧还是开玩笑什么的——有一个人被谋杀了!"


translate chinese epilogue_mi_4cc32fd1:


    "一个我们认识的人，不管是叫斯拉维娅还是萨沙!"


translate chinese epilogue_mi_9b82e676:


    mt "嗯，没有..."


translate chinese epilogue_mi_c0ab7802:


    "她犹豫了。"


translate chinese epilogue_mi_9c0b8144:


    mt "但是总是有人干的!"


translate chinese epilogue_mi_19cc5970:


    my "我想说应该是某种野生动物，大概是狼之类的。"


translate chinese epilogue_mi_5b070411:


    mt "附近根本没有狼..."


translate chinese epilogue_mi_0b9bc04a:


    my "所以你觉得一个人能做出这种事?!"


translate chinese epilogue_mi_e504c1f3:


    mt "反正不能怪其他的人..."


translate chinese epilogue_mi_9d5abb5c:


    my "不，这完全说不通，真的..."


translate chinese epilogue_mi_03e931a3:


    "我紧紧的抱着玛莎。"


translate chinese epilogue_mi_32a03025:


    "她的抽泣声稍微轻了一些。"


translate chinese epilogue_mi_4385d694:


    mt "总之，我们需要报警。"


translate chinese epilogue_mi_d61b9628:


    my "当然了！快去吧!"


translate chinese epilogue_mi_a20cefa7_13:


    "..."


translate chinese epilogue_mi_978dd8c8:


    "城镇中心距离这里很远...警察不会很快来的..."


translate chinese epilogue_mi_dad64e6b:


    "夜幕逐渐降临。"


translate chinese epilogue_mi_209abbb1:


    "我坐在广场上，看着日落。"


translate chinese epilogue_mi_cd30d9b9:


    "玛莎在奥尔加·德米特里耶夫娜的房间里睡觉。"


translate chinese epilogue_mi_6eda1825:


    "这件事刺激到了她，可能比我们要严重很多，因为各种原因..."


translate chinese epilogue_mi_a5a8139e:


    "我现在主要有两个想法：第一，这里发生了什么；第二，谁杀了斯拉维娅。{w}或者萨沙。不管怎么说，是谁杀的?"


translate chinese epilogue_mi_a645528a:


    "第一个问题可以先不去管它。{w}但是第二个我也是毫无头绪。"


translate chinese epilogue_mi_b9e6fa5a:


    "虽然很可疑，但是被野生动物攻击的说法还是最合理的。"


translate chinese epilogue_mi_919c8c4f:


    "我不管这附近有没有狼，还有可能是一头熊，或者獾...但是绝对不是人!"


translate chinese epilogue_mi_a5f91356:


    "没有人可以不借助工具弄成那样。"


translate chinese epilogue_mi_252102f2:


    "那种伤口显然又是徒手撕裂导致的。"


translate chinese epilogue_mi_44e8a885:


    "虽然说一个疯狂的人类也可能干出这种疯狂的事情..."


translate chinese epilogue_mi_e0069242:


    "但是这里的人谁能做的出来呢？Router?列娜？或者奥尔加·德米特里耶夫娜？"


translate chinese epilogue_mi_a4cb3fed:


    "不，这很愚蠢。"


translate chinese epilogue_mi_d3f93a18:


    "但是即使是这样，除了他们还是有很多的孩子。"


translate chinese epilogue_mi_b10ee23b:


    "我要去审问他们每一个人吗？不会吧，那应该是警察的工作!"


translate chinese epilogue_mi_b9939397:


    "那还有什么好怕的呢？也许我不是自己害怕，是为了玛莎..."


translate chinese epilogue_mi_b2e7370c:


    "对了，我得看看她怎么样了!"


translate chinese epilogue_mi_ca00c08d:


    "我正要起来，突然我看到了什么人。"


translate chinese epilogue_mi_4a224c84:


    "我回过头，看到了列娜。"


translate chinese epilogue_mi_86d435bf:


    un "啊，我没想打扰到你..."


translate chinese epilogue_mi_7851bc23_1:


    my "没关系的..."


translate chinese epilogue_mi_c67f7c91:


    un "我可以坐在这里吗?"


translate chinese epilogue_mi_3cdc5a84:


    my "当然，请坐吧。"


translate chinese epilogue_mi_e29e5371:


    "她简单的整理一下裙子，然后坐下来看着夕阳。"


translate chinese epilogue_mi_3b7b5e4d:


    un "这太糟糕了..."


translate chinese epilogue_mi_4170725c:


    my "是啊，没有什么愉悦的。"


translate chinese epilogue_mi_7d25e023:


    un "你怎么看，是谁做的呢?"


translate chinese epilogue_mi_4612d3fc:


    my "我怎么知道呢？肯定不是我。"


translate chinese epilogue_mi_4f66a37f:


    un "不，不，不是我怀疑你什么的!"


translate chinese epilogue_mi_e77f4d13:


    "她滑稽的摆着手。"


translate chinese epilogue_mi_09ae1847:


    my "那真的太感谢了。"


translate chinese epilogue_mi_851fed46:


    "我苦笑。"


translate chinese epilogue_mi_41c9cd8c:


    un "但是那一定是一个非常邪恶残忍的人做的。"


translate chinese epilogue_mi_6fa72eff:


    my "嗯，也不是完全必要..."


translate chinese epilogue_mi_d713736e:


    "列娜怀疑的看着我。"


translate chinese epilogue_mi_c2861722:


    my "也许只是一种错觉，反正在日常生活中，精神分裂的人可能平时看起来也很正常。"


translate chinese epilogue_mi_b9867686:


    un "你是说...?"


translate chinese epilogue_mi_0ae367b8:


    "她有点惊讶的长大了嘴巴。"


translate chinese epilogue_mi_dbe54858:


    un "可能是我们之中的一个人干的...?"


translate chinese epilogue_mi_ee63da1c:


    my "我不是很确定...但是嫌疑人的范围是很大的。"


translate chinese epilogue_mi_a69b68b1:


    un "不...孩子们不能那样做的...不可能的..."


translate chinese epilogue_mi_e69771c0:


    my "总之要等警察来判断了..."


translate chinese epilogue_mi_a4f042ce:


    "我们又默默的坐了一会儿。"


translate chinese epilogue_mi_1918988a:


    "我准备离开，列娜悄悄地对我说:"


translate chinese epilogue_mi_090aef2c:


    un "你是不是可能..."


translate chinese epilogue_mi_b9e79bf2_1:


    my "什么?"


translate chinese epilogue_mi_4745db66:


    "她脸上浮现出不太正常的笑容。"


translate chinese epilogue_mi_7b836a93:


    "魔鬼的微笑——我是这么觉得。"


translate chinese epilogue_mi_298ef616:


    un "会不会是你杀了斯拉维娅?"


translate chinese epilogue_mi_a4b858b3:


    my "当然不是!"


translate chinese epilogue_mi_683f39d5:


    un "但是你们两个关系不是很好..."


translate chinese epilogue_mi_a6716552:


    my "为什么这么想？我们的关系一直不错的。"


translate chinese epilogue_mi_82a95096:


    "显然列娜说的不是我。"


translate chinese epilogue_mi_a34c7a38:


    "准确的说不是昨天的我。{w}也不是叫做萨沙的那个斯拉维娅。"


translate chinese epilogue_mi_536e6181:


    "这个我可以确定。"


translate chinese epilogue_mi_a0e48a85:


    my "不，那不是我!"


translate chinese epilogue_mi_0f6c6eab:


    un "好了，好了，冷静，我没有指控你..."


translate chinese epilogue_mi_ba4db5a4:


    "她看着我，眼睛似乎发红。"


translate chinese epilogue_mi_7f322486:


    "也许只是映着夕阳的颜色..."


translate chinese epilogue_mi_0ed60aab:


    my "我很高兴你能明白。"


translate chinese epilogue_mi_8dc0ae33:


    un "但是你刚刚还说所有人都有嫌疑呢。"


translate chinese epilogue_mi_09973a1a:


    my "我没有让你怀疑所有人！我只是说，我们还没有找到凶手，目前..."


translate chinese epilogue_mi_cb7116cc:


    un "噢，我明白了，希望我们不会是下一个受害者!"


translate chinese epilogue_mi_a73c91c7:


    "列娜大声的说着，然后大笑起来。"


translate chinese epilogue_mi_e7ba1f7e:


    "她的反应让我坐立不安。"


translate chinese epilogue_mi_ccdf7460:


    un "好了，该走了。"


translate chinese epilogue_mi_7bec5d07:


    "她优雅的站起来，离开了广场。"


translate chinese epilogue_mi_d1c55a8c:


    "我决定不再闲逛，直接前往奥尔加·德米特里耶夫娜的房间。"


translate chinese epilogue_mi_a20cefa7_14:


    "..."


translate chinese epilogue_mi_50fa59ee:


    "现在已经完全天黑了..."


translate chinese epilogue_mi_ee446fb5:


    "玛莎还在睡觉，我不想弄醒她，所以悄悄的爬上去待在她的旁边。"


translate chinese epilogue_mi_e4f4f44e:


    "我们的导演（据本地人讲是辅导员）不见了踪影。"


translate chinese epilogue_mi_87554d1b:


    "如果她现在进来的话，那么事情会很难办。"


translate chinese epilogue_mi_1cd7d952:


    "不管怎么说，这有什么关系呢？已经有一个人被杀了！还要管这种琐事做什么!"


translate chinese epilogue_mi_8f3d22b2:


    "而且不管怎么说我们还是要弄清楚的，大家为什么都要装成是少先队员?"


translate chinese epilogue_mi_9ef4ae27:


    "我总觉得，在我们找到凶手以前这个大型恶作剧是不会结束的。"


translate chinese epilogue_mi_f240c5d7:


    "真正的问题是，哪个是原因，哪个是结果...?"


translate chinese epilogue_mi_a59fd30e:


    "是大家都在装导致斯拉维娅被杀，还是说斯拉维娅被杀导致大家都在装..."


translate chinese epilogue_mi_c6188591:


    "见鬼！我完全糊涂了，而且在这里开动脑筋也不会有什么结果。"


translate chinese epilogue_mi_90f7eafc:


    "大门轻轻的打开了。"


translate chinese epilogue_mi_cdaff57d:


    "我谨慎的往黑暗中看去。"


translate chinese epilogue_mi_108f56f1:


    "经过了一会儿难熬的时间，结果没有人进来。"


translate chinese epilogue_mi_05be7ae4:


    "躺下等着也没有什么意义。"


translate chinese epilogue_mi_080212dd:


    "刚刚可能只是风吹的，但是晚上这么晚了，大家都睡了..."


translate chinese epilogue_mi_68b1a2b9:


    "我站起来，在黑暗中拿起自己的裤子，向周围看着。"


translate chinese epilogue_mi_7d8bf151:


    "这里可是没有什么能用来自卫的东西。"


translate chinese epilogue_mi_63056596:


    "但是我至少得试试。"


translate chinese epilogue_mi_e04f66bc:


    "我在房间里翻来翻去，绊倒在柜子里一个上锁的抽屉上。"


translate chinese epilogue_mi_40adda07:


    "我是很想找到一把枪或者至少是一个什么撬棍。不过我也知道没什么可能。"


translate chinese epilogue_mi_55acc250:


    "但是它上锁一定是有理由的!"


translate chinese epilogue_mi_10eae588:


    "我把柜子拉出来，检查了一下抽屉的具体位置，然后准确的踢了下去。"


translate chinese epilogue_mi_07f9ee90:


    "我听到了三合板碎裂的声音，然后有什么东西散落了出来。"


translate chinese epilogue_mi_db4073e9:


    "内裤...好多内裤...火柴。"


translate chinese epilogue_mi_d0a78133:


    "呃，可能只是为了方便。"


translate chinese epilogue_mi_c88871a5:


    "我把火柴装进口袋，同时我突然听见外面有东西在挠门，好像是什么动物。"


translate chinese epilogue_mi_9795e900:


    "我确定如果这就是那个杀人凶手，我恐怕是没有希望获胜了..."


translate chinese epilogue_mi_274d3fca:


    "但是也没准就是一个本地的“少先队员”。"


translate chinese epilogue_mi_6e7c4600:


    "只是发疯了，或者一开始就是个疯子..."


translate chinese epilogue_mi_3d71d05e:


    "不管怎么说，我应该还是可以战胜这里的孩子的。{w}即使他带着刀..."


translate chinese epilogue_mi_760217c6:


    "想到这个武器，我感觉很害怕。"


translate chinese epilogue_mi_0c0e46bd:


    "不行，我不能这么自大。"


translate chinese epilogue_mi_c7ea3de8:


    "如果我真的遇到了有刀的攻击者，我必须躲起来锁好门等待救援!"


translate chinese epilogue_mi_2466c05f:


    "但是..."


translate chinese epilogue_mi_c189c152:


    "又是一阵响声，门稍微打开了一点。"


translate chinese epilogue_mi_1ad9f73d:


    "我闭上眼睛，然后猛地打开门，用上全身的力量。"


translate chinese epilogue_mi_d2ae2e3c:


    "我听到一声尖叫，然后吓呆的阿丽夏。"


translate chinese epilogue_mi_e13ab7da:


    my "哇，是你啊..."


translate chinese epilogue_mi_606c5803:


    "我松了口气。"


translate chinese epilogue_mi_93228cc9:


    my "别吓我呀，快要给我吓出心脏病来了！"


translate chinese epilogue_mi_63ed4b6c:


    "她站起来掸掸身上的尘土，愧疚的看着我。"


translate chinese epilogue_mi_77613451:


    dv "抱歉..."


translate chinese epilogue_mi_91d22172:


    my "还好，只是...晚上自己在营地里走动太危险了，在那种事情发生的时候..."


translate chinese epilogue_mi_2a7a8a3b:


    "我停顿了一下。"


translate chinese epilogue_mi_f9d90a33:


    "'我说的好像还在发生..."


translate chinese epilogue_mi_126375f0:


    my "我是说，发生了那种事情..."


translate chinese epilogue_mi_a3a258d8:


    dv "我就是因为这个来的..."


translate chinese epilogue_mi_0fc06d24:


    "阿丽夏盯着脚下，有点颤抖。"


translate chinese epilogue_mi_2517d987:


    dv "已经很晚了，但是乌里扬卡还没有回来。"


translate chinese epilogue_mi_71da9f7d:


    "她抬起头，我看到泪水顺着她的脸颊流了下来。"


translate chinese epilogue_mi_31b3f8f3:


    my "她可能去哪里呢?"


translate chinese epilogue_mi_f8590b39:


    "我也不是真的多么关心，但是我得也控制住局势。"


translate chinese epilogue_mi_b82284e4:


    "很明显晚上在营地里没有什么可干的，如果有电的话大家应该都会打开电脑看电影什么的，如果没有大家可能都在烛光下看书。"


translate chinese epilogue_mi_32d70287:


    "独自赏月应该不在此列..."


translate chinese epilogue_mi_bf433c67:


    dv "我不知道..."


translate chinese epilogue_mi_0709f70a:


    my "好吧，咱们先冷静的回顾一下..."


translate chinese epilogue_mi_23b8137e:


    "我走到桌子旁，捡起手表。"


translate chinese epilogue_mi_26bbbc79:


    "已经半夜了，大家应该都回到宿舍了。"


translate chinese epilogue_mi_65e215d0:


    "一个恐怖的念头突然出现在了我的心中，奥尔加·德米特里耶夫娜在哪里...?"


translate chinese epilogue_mi_2f7416f2:


    my "咱们出去说吧。我不想吵醒玛...未来..."


translate chinese epilogue_mi_11889848:


    "我走到楼梯的时候开始思考。"


translate chinese epilogue_mi_5be4562e:


    "阿丽夏站在我旁边表情痛苦的攥着手，盯着逐渐升起的月亮。"


translate chinese epilogue_mi_203b67d9:


    dv "我们得叫警察。"


translate chinese epilogue_mi_cb6ef91a:


    my "已经叫了。"


translate chinese epilogue_mi_fb4190cc:


    dv "那是只是斯拉维娅，现在还有乌里扬娜..."


translate chinese epilogue_mi_e63e04ce:


    my "奥尔加·德米特里耶夫娜呢，你看见她了吗?"


translate chinese epilogue_mi_b8e9ed52:


    dv "没有...怎么了，难道她也...?"


translate chinese epilogue_mi_b680e150:


    "阿丽夏哭了出来。"


translate chinese epilogue_mi_c0fe2e06:


    my "我们还什么都不知道，但是..."


translate chinese epilogue_mi_01f7b9c3:


    "不管怎么说，这里正在发生着什么事情!"


translate chinese epilogue_mi_06d2a65f:


    "不管是他们在掩饰着什么还是这个世界发生了什么变化，我现在还是很有机会逃离这一切的。"


translate chinese epilogue_mi_d6e99617:


    "所以我应该小心的尝试生存..."


translate chinese epilogue_mi_d8fea1da:


    "“生存”，这个词语突然像是一拳击在我的后脑。"


translate chinese epilogue_mi_aa6ca21f:


    "“生存”，我昨天还完全没有考虑这样事情呢..."


translate chinese epilogue_mi_1b3de5ca:


    "安静的夏夜笼罩着大地，没有一点声音，没有风声，没有草丛小动物的活动，也没有树枝悉悉簌簌。什么声音也没有。"


translate chinese epilogue_mi_0bbb84bd:


    "只有宿舍房间反射的月光，天空中零星的云彩，还有无法穿透的黑暗，有人说，当你注视着深渊的时候，深渊也在注视着你。"


translate chinese epilogue_mi_845dfb1b:


    "还有蚱蜢，好像在轮班倒的DJ!"


translate chinese epilogue_mi_77894e87:


    "他们总是在岗..."


translate chinese epilogue_mi_f03db694:


    "我还得找点儿杀虫剂，如果我能干掉一小股敌军，至少在房间附近可以清静一些。"


translate chinese epilogue_mi_ae29cb48:


    "当然我不可能杀光他们..."


translate chinese epilogue_mi_6e9aee0c:


    "什么玩意儿，虫子永远也杀不光..."


translate chinese epilogue_mi_18e03cde:


    dv "你能听见吗...?"


translate chinese epilogue_mi_bdaf90db:


    "阿丽夏坐下来紧紧的靠着我，她的身体在不停地颤抖。"


translate chinese epilogue_mi_b9e79bf2_2:


    my "什么?"


translate chinese epilogue_mi_21f9a60f:


    "我紧张的听着。"


translate chinese epilogue_mi_a969edc9:


    "好像有人在慢慢的往房间这边走。"


translate chinese epilogue_mi_d7c542e6:


    "在黑暗中我什么也看不清，但是每一秒钟声音都越来越清楚。"


translate chinese epilogue_mi_53d2a1ff:


    my "好吧，别慌...他不敢袭击我们两个人..."


translate chinese epilogue_mi_9e7e2c33:


    "虽然这个我也不是很确定。"


translate chinese epilogue_mi_62e9f0fe:


    "我的手本能的开始找能用来自卫的东西。"


translate chinese epilogue_mi_93b5fc50:


    dv "嘿!"


translate chinese epilogue_mi_ec37960b:


    my "抱歉..."


translate chinese epilogue_mi_e042b998:


    "很快我找到了椅子底下的一根水管。"


translate chinese epilogue_mi_58a4016f:


    "没什么大用，太粗了，而且挥不动，不过也要比徒手强一些。"


translate chinese epilogue_mi_a37831a4:


    "一看到这个大怪物的身体可能所有人都会吓尿。{w}更有可能是从笑声中..."


translate chinese epilogue_mi_1b95dc17:


    "我什么也做不了，我们就是坐以待毙，所以我宁愿冲向敌人。"


translate chinese epilogue_mi_9bcff4c0:


    "我不能说自己突然充满了涌起，其实应该是正好相反，我吓的快要尿裤子了。"


translate chinese epilogue_mi_b9ce7582:


    "但是我好像真的别无他法..."


translate chinese epilogue_mi_f55c7e59:


    "我深吸一口气，想暗处走去。"


translate chinese epilogue_mi_439b90ad:


    "很快我看到这个人更多的轮廓。{w}不是很高，奇怪的头发，衬衫..."


translate chinese epilogue_mi_86e127e7:


    "那一定是列娜!"


translate chinese epilogue_mi_ef6c8558:


    "我放松了一些。"


translate chinese epilogue_mi_af67a563:


    my "你这么晚在这里做什么?"


translate chinese epilogue_mi_ef1f0ae2:


    un "我...我..."


translate chinese epilogue_mi_2c21a7bc:


    "列娜的脸上都是泪痕。"


translate chinese epilogue_mi_448b06ba_1:


    my "来吧。"


translate chinese epilogue_mi_a6d8a438:


    "我把她带到阿丽夏旁边。"


translate chinese epilogue_mi_5b1b0f13:


    "列娜恢复正常以后，她开始讲述发生的事情。"


translate chinese epilogue_mi_41ff22a7:


    un "我就独自坐着，已经很晚了...但是未来还不知道在哪里...但是我...还有斯拉维娅发生的事情... 我..."


translate chinese epilogue_mi_3019d555:


    "眼前的这个列娜和之前的似乎完全无法协调，几个小时以前列娜在广场上露出恶魔般的笑容，眼中还充满着凶光。"


translate chinese epilogue_mi_772757c7:


    "也许这只是我的想象...{w}也许不是..."


translate chinese epilogue_mi_97352ac0:


    my "话说，你知不知道乌里扬娜和奥尔加·德米特里耶夫娜去了哪儿?"


translate chinese epilogue_mi_3932a02b:


    "她惊讶的看着我。"


translate chinese epilogue_mi_74ae6a8d:


    un "没有，怎么了...她们也失踪了吗?"


translate chinese epilogue_mi_52f6ea86:


    my "嗯，我们现在还不能确定..."


translate chinese epilogue_mi_4297ae4e:


    dv "谢苗..."


translate chinese epilogue_mi_5b9b3c9e:


    "我一开始还没有发现她是在叫我。"


translate chinese epilogue_mi_366fbd9e:


    my "哈?"


translate chinese epilogue_mi_d6783e26:


    dv "你不觉得营地里有点过于安静了吗?"


translate chinese epilogue_mi_9c6951b5:


    my "晚上大家都在睡觉啊，不是吗...?"


translate chinese epilogue_mi_09a7148c:


    "我竖起耳朵，确实除了这些虫子的叫声就没有别的声音了。"


translate chinese epilogue_mi_ae49ef98:


    "什么声音也没有!"


translate chinese epilogue_mi_c86f665e:


    my "这个地方平常是什么样的?"


translate chinese epilogue_mi_bdbb2826:


    un "安静..."


translate chinese epilogue_mi_7e596111:


    "列娜悄悄的说。"


translate chinese epilogue_mi_100a8b2f:


    dv "安静，但是没有这么安静。"


translate chinese epilogue_mi_20845d8d:


    "我不能让女孩儿们紧张。"


translate chinese epilogue_mi_0b7cd763:


    "虽然我自己已经要吓坏了。"


translate chinese epilogue_mi_c62e4545:


    "但是真的是有原因的：活人突然消失，凶残的谋杀，朋友到最后一直都在假装是少先队员。"


translate chinese epilogue_mi_aa1ba754:


    "我已经快要坚持不住了..."


translate chinese epilogue_mi_d00c5bcd:


    my "好吧，咱们理智的分析一下。"


translate chinese epilogue_mi_fcc1f8e9:


    "女孩儿们有点疑惑的看着我。"


translate chinese epilogue_mi_630f46ee:


    my "两个人不可能没有任何痕迹的凭空消失!"


translate chinese epilogue_mi_6b952a33:


    un "也就是说..."


translate chinese epilogue_mi_480a55d0:


    my "不，这并不意味着什么!"


translate chinese epilogue_mi_8fff31b7:


    "突然我们听到房间后面的树丛里有鬼鬼祟祟的声音。"


translate chinese epilogue_mi_ee4a8cbd:


    "我还没来得及拿起我的管子，Router就出现了。"


translate chinese epilogue_mi_b7ab4b8d:


    ro "噢，你们在这里啊!"


translate chinese epilogue_mi_6d30a2a8:


    "他好像完全不在乎这里发生的可怕的事情。"


translate chinese epilogue_mi_2986d3b8:


    my "天哪，你能不能不要吓我们！女孩儿们都要犯心脏病了！"


translate chinese epilogue_mi_776c97bc:


    "虽然我并不知道到底谁更害怕—她们还是我。"


translate chinese epilogue_mi_43b66a40:


    ro "是啊，抱歉..."


translate chinese epilogue_mi_38116f5e:


    my "你怎么来了?"


translate chinese epilogue_mi_d2c06568:


    ro "你看到舒里克了吗?"


translate chinese epilogue_mi_b84b7fde:


    my "没有..."


translate chinese epilogue_mi_75fdeb9e:


    "好像这几个小时里半个夏令营的人都消失了。"


translate chinese epilogue_mi_a5873d1b:


    ro "而且这附近太安静了..."


translate chinese epilogue_mi_b01d3ea3:


    my "我们已经注意到这一点了!"


translate chinese epilogue_mi_763991f9:


    ro "到处都没有光，只有寂静..."


translate chinese epilogue_mi_c4497e2f:


    my "你不要在这里制造紧张气氛了！我已经很不舒服了!"


translate chinese epilogue_mi_a90284ea:


    ro "我在做什么？我什么也没做啊..."


translate chinese epilogue_mi_d4f9df56:


    "他露出神秘的微笑，让我一惊。"


translate chinese epilogue_mi_569deb9a:


    my "好了，你们呆在这里，我去搜查一下整个夏令营!"


translate chinese epilogue_mi_c87950e7:


    un "自己?"


translate chinese epilogue_mi_8ab3285c:


    "是啊，这可能不是个好主意...我应该先动动脑子再说话。"


translate chinese epilogue_mi_c90f163e:


    my "呃..."


translate chinese epilogue_mi_862e0a1f:


    dv "我和你一起去!"


translate chinese epilogue_mi_0f4ce8dc:


    my "好吧，你呆在这里，别忘了玛...未来在房间里。"


translate chinese epilogue_mi_785bce8b:


    ro "是，遵命!"


translate chinese epilogue_mi_ee6a930d:


    "真是傻子，如果他在别的情况下跟我那样傻笑..."


translate chinese epilogue_mi_a20cefa7_15:


    "..."


translate chinese epilogue_mi_a20cefa7_16:


    "..."


translate chinese epilogue_mi_982fd05a:


    "过了半个小时，我们来到了广场。"


translate chinese epilogue_mi_9112918f:


    "所有的房间都是空的，似乎整个夏令营里只有我们五个。"


translate chinese epilogue_mi_594f012c:


    "我拿着自己的管子，一瘸一拐的走着。"


translate chinese epilogue_mi_5326720a:


    "一开始我还完全不在乎来着，但是逐渐的我发现即使是背着一包羽毛，如果一直走下去也是会累坏的。"


translate chinese epilogue_mi_0feb0847:


    dv "大家究竟都在哪里..."


translate chinese epilogue_mi_f5ac9909:


    "阿丽夏好像还比较镇定。{w}刚刚发现一个个房间都是空的那个时候的恐惧逐渐消失了。"


translate chinese epilogue_mi_d5218c9a:


    my "至少我们可以确定这和斯拉维娅的事件有所不同!"


translate chinese epilogue_mi_59c3611a:


    dv "为什么?"


translate chinese epilogue_mi_b7312eb9:


    my "你想想...尸体在哪里?"


translate chinese epilogue_mi_06fb3902:


    "我悄悄的说，怕吓到她。"


translate chinese epilogue_mi_4b68666f:


    dv "但是大家都在哪儿呢?"


translate chinese epilogue_mi_bb984b76:


    my "消失了...?"


translate chinese epilogue_mi_d01110d6:


    "这个解释一点说服力也没有...{w}但是说起来过去的24小时里发生的各种事情都让人难以置信。"


translate chinese epilogue_mi_7c45825b:


    dv "他们不能就那样的...消失了..."


translate chinese epilogue_mi_d63d25c0:


    my "现在咱们什么也做不了，明天又是新的一天。"


translate chinese epilogue_mi_72f142cf:


    "我试图笑笑。"


translate chinese epilogue_mi_ba81381a:


    my "去找他们吧!"


translate chinese epilogue_mi_593113c9:


    dv "嗯..."


translate chinese epilogue_mi_01f64c2f:


    "阿丽夏有点不太确定的回答着。"


translate chinese epilogue_mi_413d048d:


    "我们又走了几步，突然又听到背后的草丛里沙沙的声响。"


translate chinese epilogue_mi_848599fc:


    "我马上转过身，警惕的观察着。"


translate chinese epilogue_mi_4b8f2dde:


    "阿丽夏仅仅的攥着我的手，弄得我很疼，我也紧紧的握住管子。"


translate chinese epilogue_mi_e0ac0d35:


    dv "谁在...那里...?"


translate chinese epilogue_mi_326ad2f9_1:


    my "不知道..."


translate chinese epilogue_mi_6e7af154:


    "我挤眉弄眼，还是看不清楚。"


translate chinese epilogue_mi_333411d8_1:


    my "嘿!"


translate chinese epilogue_mi_ffe234c7:


    "我不知道接下来应该做什么。"


translate chinese epilogue_mi_c42d06d1:


    "那个声音突然消失了，我们也回到了这个由蟋蟀占领的普通的夜晚。"


translate chinese epilogue_mi_52152cc5:


    my "会不会是什么野生动物..."


translate chinese epilogue_mi_fefef18f:


    "我有点担心的问着。"


translate chinese epilogue_mi_42a83dbb:


    dv "野生...动物?"


translate chinese epilogue_mi_42e4e478:


    "她更加用力的拉住我的手。"


translate chinese epilogue_mi_049c7fdc:


    dv "但是附近并没有野生动物!"


translate chinese epilogue_mi_7996282d:


    my "呃，比如说一只松鼠什么的...我也不知道..."


translate chinese epilogue_mi_066810ef:


    dv "松鼠?"


translate chinese epilogue_mi_6ae4ca96:


    my "是啊，一只松鼠。"


translate chinese epilogue_mi_b95f9fd2:


    dv "嗯，也许是..."


translate chinese epilogue_mi_f87596bf:


    my "我们可以走了吗?"


translate chinese epilogue_mi_3b4b97c0:


    dv "嗯..."


translate chinese epilogue_mi_a20cefa7_17:


    "..."


translate chinese epilogue_mi_e258274a:


    "过了一会儿，我们逐渐接近奥尔加·德米特里耶夫娜的房间。"


translate chinese epilogue_mi_103eb1f8:


    "Router和列娜，一直保持着沉默，在看见我们的时候有点发抖。"


translate chinese epilogue_mi_4e1a88c8:


    ro "怎么样了?"


translate chinese epilogue_mi_5b845686:


    my "没有人。"


translate chinese epilogue_mi_dcb7a8c7:


    un "什么叫...没有人?"


translate chinese epilogue_mi_699e5734:


    dv "大家都不见了..."


translate chinese epilogue_mi_ca44847e:


    "阿丽夏带着泪水说着。"


translate chinese epilogue_mi_200d2256:


    "女孩儿们抱在了一起，我坐在Router旁边，终于把那根可恶的管子扔在了地上。"


translate chinese epilogue_mi_e985afc7:


    ro "就这样?"


translate chinese epilogue_mi_6a716cb7:


    my "就这样..."


translate chinese epilogue_mi_f347bf4c:


    ro "怎么回事?"


translate chinese epilogue_mi_1659118a:


    my "我真的不知道...也许是被外星人诱拐了?"


translate chinese epilogue_mi_8303b1d0:


    ro "这讲不通啊!"


translate chinese epilogue_mi_eefcb0ea:


    my "想要探案吗，夏洛克?"


translate chinese epilogue_mi_e44817f8:


    ro "这，有可能还是同一个疯子干的..."


translate chinese epilogue_mi_0b807932:


    my "你不觉得这对于一个人来说有点?"


translate chinese epilogue_mi_32b76f96:


    ro "有道理...但是这是谁...什么时候?"


translate chinese epilogue_mi_a29f386e:


    my "这也有可能是一个糟糕的恶作剧..."


translate chinese epilogue_mi_b5d68c97:


    ro "斯拉维娅也是吗？那也是一个恶作剧吗?"


translate chinese epilogue_mi_0b63e0e1:


    my "不，她绝对是被谋杀了。"


translate chinese epilogue_mi_a1f1a290:


    ro "谁干的？"


translate chinese epilogue_mi_2c5335a6:


    "他眯起眼睛盯着我。"


translate chinese epilogue_mi_f1897fd9:


    my "我怎么知道！也许就是你!"


translate chinese epilogue_mi_262d7cae:


    "我开始变得紧张。"


translate chinese epilogue_mi_37e35222:


    ro "或者是你!"


translate chinese epilogue_mi_699cf6d5:


    my "肯定不是我。"


translate chinese epilogue_mi_9da52b6d:


    ro "你怎么证明?"


translate chinese epilogue_mi_e3275cbc:


    my "我为什么要向你证明?"


translate chinese epilogue_mi_d7c7a76d:


    ro "只有你没有不在场证明!"


translate chinese epilogue_mi_9bb0e846:


    my "怎么，你有?"


translate chinese epilogue_mi_4bae00e6:


    ro "我晚上在房间里，舒里克可以证明!"


translate chinese epilogue_mi_76243b78:


    my "当然，但是舒里克现在不知道哪里去了..."


translate chinese epilogue_mi_daca0afa:


    "Router咬牙切齿的转了过去。"


translate chinese epilogue_mi_71d9105b:


    ro "总之我有不在场证明。我不用证明我自己!"


translate chinese epilogue_mi_3b810b08:


    my "相信自己很好...我也有不在场证明，我一直和未来在一起，她可以证明！"


translate chinese epilogue_mi_c814e87f:


    ro "你这个证人选的不太好..."


translate chinese epilogue_mi_094c7520:


    my "跟你的差不多!"


translate chinese epilogue_mi_74d118df:


    "如果没有列娜突然插话，我们不知道还有争论多久。"


translate chinese epilogue_mi_0284f524:


    un "男孩们，不要这样..."


translate chinese epilogue_mi_bf352153:


    my "你说得对，抱歉..."


translate chinese epilogue_mi_44cacce6:


    ro "是啊..."


translate chinese epilogue_mi_400b9c27:


    "确实，现在这个时机不太好。"


translate chinese epilogue_mi_9b909de6:


    my "总之我们应该在一起..."


translate chinese epilogue_mi_a7fd75a5:


    dv "这可以让我们不会一个个消失吗...?"


translate chinese epilogue_mi_0046a8da:


    "阿丽夏心虚的问着。"


translate chinese epilogue_mi_70bbf125:


    my "我不知道消失什么的怎么回事，但是这样确实可以让咱们生存的几率大一些!"


translate chinese epilogue_mi_4e5b4343:


    "我看了一眼表，快到四点了!{w}太阳马上就要出来了。"


translate chinese epilogue_mi_709706dd:


    my "好了，伙计们，该睡觉了，不然大家白天就睁不开眼了。"


translate chinese epilogue_mi_46466e3f:


    un "但是..."


translate chinese epilogue_mi_35fb6c26:


    my "没有但是!"


translate chinese epilogue_mi_71ed3f9b:


    "没有人反驳，我们就这样静静的走进房间。"


translate chinese epilogue_mi_cf4f8759:


    ma "哈？发生了什么？.."


translate chinese epilogue_mi_b6b544d2:


    "我们好像吵醒了玛莎。"


translate chinese epilogue_mi_1addbba1:


    my "没事，继续睡吧..."


translate chinese epilogue_mi_688bd005:


    "但是她已经坐起来看着我们的小聚会。"


translate chinese epilogue_mi_bb72e793:


    ro "那个，你看..."


translate chinese epilogue_mi_13d057b3:


    un "他们都走了！消失了！不见了！"


translate chinese epilogue_mi_ae15ba1a:


    "列娜突然哭了出来，然后抱着玛莎的脖子。"


translate chinese epilogue_mi_a35f21fb:


    ma "什么？怎么回事？你能不能说清楚一点？"


translate chinese epilogue_mi_a22c92b6:


    my "嗯，长话短说..."


translate chinese epilogue_mi_3a6935fd:


    "我揉揉脖子然后到口袋里找烟，但是没有找到，我失望的叹叹气:"


translate chinese epilogue_mi_ccc382cd:


    my "看起来夏令营里只有咱们五个人了..."


translate chinese epilogue_mi_e8e8d6ba:


    ro "就是这个房间里的所有人!"


translate chinese epilogue_mi_1f6dec3c:


    my "感谢你，大队长！你第一个站岗！"


translate chinese epilogue_mi_6c21d46f:


    ro "什么?"


translate chinese epilogue_mi_c12f9eb4:


    my "没什么，你觉得所有人同时睡觉很安全吗?"


translate chinese epilogue_mi_f1b10303:


    ro "嗯，不是，但是..."


translate chinese epilogue_mi_d54d0a6f:


    my "就是说啊!"


translate chinese epilogue_mi_20db871e:


    "我把管子递给他，然后慢慢接近列娜和玛莎。"


translate chinese epilogue_mi_db756b30:


    my "那个，如果你不介意的话..."


translate chinese epilogue_mi_1f2560d6:


    "我脱掉靴子，躺在女生身边，面朝墙。"


translate chinese epilogue_mi_844b71c4:


    ma "但是怎么..."


translate chinese epilogue_mi_4576c932:


    my "怎么...睡觉!"


translate chinese epilogue_mi_03f7efc1:


    "我太累了，不论是身体上的，还是精神上的，我已经累得说不出话来了。"


translate chinese epilogue_mi_1dff787e:


    "我只想赶紧睡觉..."


translate chinese epilogue_mi_e3998c81:


    "玛莎的悄悄话，列娜的抽泣，还有阿丽夏的声音，都融合成一首摇篮曲，我就这样睡着了。"


translate chinese epilogue_mi_a20cefa7_18:


    "..."


translate chinese epilogue_mi_0540bbef:


    "我被人用很大的力气摇醒。"


translate chinese epilogue_mi_df4372df:


    "Router摇晃着我的肩膀。"


translate chinese epilogue_mi_b9e79bf2_3:


    my "啥?"


translate chinese epilogue_mi_fc10bec9:


    ro "该你了!"


translate chinese epilogue_mi_3e837789:


    my "我睡了多长时间?"


translate chinese epilogue_mi_49aa35d6:


    ro "大概一个小时。"


translate chinese epilogue_mi_dab8428f:


    my "才一个小时？你继续..."


translate chinese epilogue_mi_2ddefabc:


    ro "我已经睁不开眼了..."


translate chinese epilogue_mi_d3a7a6c6:


    "我们的Router不是永动机..."


translate chinese epilogue_mi_e9e6e7f9_1:


    my "好吧..."


translate chinese epilogue_mi_44cfa98c:


    "我费力的坐起来，跳过互相拥抱着的列娜和玛莎。"


translate chinese epilogue_mi_7f4ec5bf:


    "房间看起来稍微明亮了一些。"


translate chinese epilogue_mi_edfb1f7d:


    my "早晨到了..."


translate chinese epilogue_mi_44cacce6_1:


    ro "是啊..."


translate chinese epilogue_mi_e2db42a2:


    "Router试图爬到我的位置，但是我抓住他的衣领，把他扔在了地上，然后扔给他一条毯子还有一个枕头。"


translate chinese epilogue_mi_004e3a87:


    ro "所以说你可以..."


translate chinese epilogue_mi_4d46ae5d:


    my "你不行!"


translate chinese epilogue_mi_402485e7:


    "不过话说回来，他为什么不行...算了，现在那个不重要。"


translate chinese epilogue_mi_12e4b3cb:


    "我把椅子拉到门边，坐下来等着。"


translate chinese epilogue_mi_1fa1a644:


    "我现在就是在等着。"


translate chinese epilogue_mi_e34cb82f:


    "随时都可能有一个疯子砸掉门板冲进来... 或者这个夏令营里最后的几个人会消失...{w}或者我会消失..."


translate chinese epilogue_mi_c3a2c9f0:


    "不管是怎么回事，肯定要出什么事。"


translate chinese epilogue_mi_cc495d0c:


    "Router在大声的打着呼噜，女孩儿们在安全的睡着，然后我也开始打瞌睡。"


translate chinese epilogue_mi_5b9c71e5:


    "我更加用力的握着水管，金属冰凉的感觉让我清醒了一些。"


translate chinese epilogue_mi_01c45f1f:


    "我不能睡着!"


translate chinese epilogue_mi_cb07ce8b:


    "我现在是他们唯一的希望了，也是我自己的希望..."


translate chinese epilogue_mi_746b4d5c:


    "但是要反抗起来太困难了，只要几分钟..."


translate chinese epilogue_mi_7d4bb7ad:


    "我的眼睛好像有自己的生命，不停的张张合合。"


translate chinese epilogue_mi_31518315:


    "门后面有动静。不，应该是我想象出来的，应该是蟋蟀!"


translate chinese epilogue_mi_10788c07:


    "它们还想干什么...?"


translate chinese epilogue_mi_b1e7167b:


    "也许就是这些虫子，他们像蝗虫过境一样把大家都吃干净..."


translate chinese epilogue_mi_0284a5b0:


    "我开始想象自己被虫子分食的景象...{w}我的大脑中出现一阵恶心的噩梦。"


translate chinese epilogue_mi_e4e546d0:


    "它们的腿，翅膀，头，眼睛... 充满黑暗!"


translate chinese epilogue_mi_b49efc2e:


    "眼睛里映出深渊。"


translate chinese epilogue_mi_2aa219d6:


    "你只要看着这些蚱蜢的眼睛，然后你就再也变不回自己了..."


translate chinese epilogue_mi_ce617998:


    "..."


translate chinese epilogue_mi_ed851b67:


    "就是这样，我在逃跑，它们在跟着我，他们有成千上万，上百万!"


translate chinese epilogue_mi_e0b1f94a:


    "我在费力的跋山涉水!"


translate chinese epilogue_mi_e424f733:


    "他们不在乎，他们有翅膀!{w}是啊，那些污秽的翅膀，震动瘆人的响声。"


translate chinese epilogue_mi_e39b3c47:


    "可能它们根本不需要牙齿，它们只要不停的扇着翅膀，就能带来世界末日！"


translate chinese epilogue_mi_ba3e9f48:


    "不行，我受不了了，我掉了下去，我用双手捂住耳朵..."


translate chinese epilogue_mi_38997562:


    "求你们了，来个人..."


translate chinese epilogue_mi_ce617998_1:


    "..."


translate chinese epilogue_mi_c7d7a7a5:


    "我被头上的疼痛感惊醒。"


translate chinese epilogue_mi_9d40e697:


    "我睁开眼，发现自己瘫在地上。"


translate chinese epilogue_mi_3529a363:


    "我好像睡着了，然后从椅子上掉了下来..."


translate chinese epilogue_mi_4a584bb7:


    "现在是九点钟。"


translate chinese epilogue_mi_26659c8d:


    "见鬼！我一口气睡了五个小时，这段时间不定发生什么事情！"


translate chinese epilogue_mi_fb4b0b46:


    "我惊恐的四处张望，似乎一切正常。"


translate chinese epilogue_mi_13e9f254:


    "呼...好险。"


translate chinese epilogue_mi_f74f70f2:


    "我把所有人叫醒。"


translate chinese epilogue_mi_18abd84b:


    ro "该我了吗...?"


translate chinese epilogue_mi_70ab00a2:


    my "你睡太多了！我一直在这里站岗..."


translate chinese epilogue_mi_6f311047:


    "这是谎言，但是就说我从蚱蜢的魔爪下逃命可不怎么样。"


translate chinese epilogue_mi_bcc18214:


    ro "抱歉..."


translate chinese epilogue_mi_3d2c6bb8:


    "Router耷拉着脑袋。"


translate chinese epilogue_mi_5f6d175a:


    my "别担心..."


translate chinese epilogue_mi_7a8ced10:


    ma "啊，我真的好饿。"


translate chinese epilogue_mi_6f990f62:


    "玛莎伸了个懒腰，然后可爱的看着我。"


translate chinese epilogue_mi_49c498a0:


    ma "还有你萌为什么都在这里?"


translate chinese epilogue_mi_bae123b1:


    un "你什么都不记得了吗?"


translate chinese epilogue_mi_7556a267:


    ma "不记得，怎么了?"


translate chinese epilogue_mi_699e5734_1:


    dv "大家都消失了..."


translate chinese epilogue_mi_e38a29fe:


    "阿丽夏阴郁的说。"


translate chinese epilogue_mi_20f54ffc_1:


    ma "说什么?"


translate chinese epilogue_mi_70a3cd5c:


    dv "大家都不见了...都凭空消失了..."


translate chinese epilogue_mi_d79f8f4c:


    "玛莎看看窗外，没有再说什么，她似乎已经想起来了。"


translate chinese epilogue_mi_6cc461d6:


    my "那个...应该找点儿东西吃!"


translate chinese epilogue_mi_0f0c21bc:


    "没有人反驳。"


translate chinese epilogue_mi_a624eeb3:


    "It不是说我多么馋，但是保持身体正常运转还是必要的。"


translate chinese epilogue_mi_a20cefa7_19:


    "..."


translate chinese epilogue_mi_f13a4b32:


    "很快我们就来到了食堂，到处搜刮各种能吃的东西。"


translate chinese epilogue_mi_a9c1d18e:


    ma "想要让我做点儿什么吗?"


translate chinese epilogue_mi_22f7f7dc:


    "玛莎悄悄的问。"


translate chinese epilogue_mi_f862db93:


    my "到午饭时再说吧，现在还好。"


translate chinese epilogue_mi_1356cf00:


    "她失望的看着我。"


translate chinese epilogue_mi_695687ee:


    ro "好吧，所以咱们要做什么?"


translate chinese epilogue_mi_c3a696ad:


    my "咱们要离开这里。"


translate chinese epilogue_mi_13582c80:


    un "但是警察..."


translate chinese epilogue_mi_a12d2714:


    my "咱们要离开这里!"


translate chinese epilogue_mi_5254c792:


    "我固执的重复着。"


translate chinese epilogue_mi_e1353452:


    dv "要怎么做呢?"


translate chinese epilogue_mi_06988cc4:


    my "公交车。"


translate chinese epilogue_mi_c6062e4b:


    dv "这个可能性可是不大，很久才有一辆的..."


translate chinese epilogue_mi_688cf4d4:


    my "至少附近应该有时刻表之类的东西。"


translate chinese epilogue_mi_d1c129fd:


    "我试图回忆自己有没有在什么地方看见过这种东西，似乎是没有..."


translate chinese epilogue_mi_989754e9:


    my "那咱们就不行，反正哪里都比坐在这里好!"


translate chinese epilogue_mi_91dba49d:


    un "但是还有很远的距离..."


translate chinese epilogue_mi_5d634972:


    my "咱们有可能可以搭到便车。"


translate chinese epilogue_mi_8d827cef:


    ro "在这种荒郊野外？..."


translate chinese epilogue_mi_37160b7d:


    my "你别瞎想了!"


translate chinese epilogue_mi_df404933:


    "虽然说现在好像正是时候..."


translate chinese epilogue_mi_d94b9a8c:


    ro "我没有瞎想，但是..."


translate chinese epilogue_mi_dec09dcb:


    my "行了，到此为止。"


translate chinese epilogue_mi_739f8c27:


    dv "我们到达城市以后要怎么说?"


translate chinese epilogue_mi_3893f203:


    my "告诉他们事实—咱们来拍电影..."


translate chinese epilogue_mi_85899043:


    ro "什么电影?"


translate chinese epilogue_mi_c2b1462d:


    my "好了，别闹了，行不行?"


translate chinese epilogue_mi_4cb424ce:


    "我掩面叹气。"


translate chinese epilogue_mi_aa01c424:


    my "昨天可能还有点意思，但是你现在不要这样了!"


translate chinese epilogue_mi_535db658:


    ro "我没明白..."


translate chinese epilogue_mi_861228df:


    my "你还是想说，你来这里不是来拍电影，而是真的作为一个少先队员?"


translate chinese epilogue_mi_4c950402:


    dv "我完全不明白..."


translate chinese epilogue_mi_c4b533d9:


    "阿丽夏惊讶的看着大家。"


translate chinese epilogue_mi_f3ebcebc:


    dv "但是我们就是少先队员..."


translate chinese epilogue_mi_0b861b18:


    ma "什么少先队员，天哪?!"


translate chinese epilogue_mi_8fdf19ba:


    "一直默不作声的玛莎突然加入了对话。"


translate chinese epilogue_mi_b017a002:


    ma "现在是21世纪了，你们这些少先队员!"


translate chinese epilogue_mi_2eff6d6b:


    dv "二十..."


translate chinese epilogue_mi_8eeb0718:


    ro "一?"


translate chinese epilogue_mi_3e7184e9:


    "看来这里真的一团糟..."


translate chinese epilogue_mi_6a191ff8:


    my "所以你们是说...你们...咱们...只是普通的少先队员，来到这里度假？"


translate chinese epilogue_mi_d30e024c:


    "一阵沉默。"


translate chinese epilogue_mi_5f2878d9:


    dv "那个... 是啊..."


translate chinese epilogue_mi_01ce7e3e_1:


    "我看着玛莎。"


translate chinese epilogue_mi_9f98315f:


    "她似乎也不知道该怎么办。"


translate chinese epilogue_mi_91e2a42f:


    "我对她指了指门口。"


translate chinese epilogue_mi_547acb5a:


    my "抱歉..."


translate chinese epilogue_mi_296d110b:


    "我走到外面说:"


translate chinese epilogue_mi_9397443d:


    my "你怎么看?"


translate chinese epilogue_mi_697b0ee3:


    ma "嗯..."


translate chinese epilogue_mi_20e75221:


    "她坐着思想者的动作。"


translate chinese epilogue_mi_8d1032a5:


    ma "他们好像真的相信，而且知道昨天咱们还是未来和谢苗。"


translate chinese epilogue_mi_b74846e4:


    my "啊..."


translate chinese epilogue_mi_94ce0421:


    ma "我觉得他们和你更熟悉!"


translate chinese epilogue_mi_b78f3341:


    my "好了啦!"


translate chinese epilogue_mi_e5e97d5d_1:


    "我打断了她。"


translate chinese epilogue_mi_2e070fe6:


    my "你没有个更可行的理论吗?"


translate chinese epilogue_mi_bc6cebcc:


    ma "我还完全没有什么理论...我还没有什么时间思考这件事..."


translate chinese epilogue_mi_b4d52943:


    my "我也是..."


translate chinese epilogue_mi_f6aad95c:


    ma "但是这里发生的事情，不只是少先队员什么的，谋杀，还有所有人都消失这些状况应该是更重要的！"


translate chinese epilogue_mi_c7c73a32:


    "I looked closely at Masha."


translate chinese epilogue_mi_fb6ae8d4:


    "我很好奇她是怎么在这种情况下保持冷静的，我是说我也尝试着保持冷静，但是..."


translate chinese epilogue_mi_144a299a:


    ma "也就是说..."


translate chinese epilogue_mi_b9e79bf2_4:


    my "什么?"


translate chinese epilogue_mi_1a88a249:


    ma "这件事咱们之后再考虑。"


translate chinese epilogue_mi_7b4c77c1:


    my "但是至少猜测一下?"


translate chinese epilogue_mi_40161182:


    ma "我觉得这和很多人突然消失有关系...一两个还可以，但是这么多人..."


translate chinese epilogue_mi_ad288150:


    my "我同意。"


translate chinese epilogue_mi_14d957a2:


    ma "可能是这个现象影响到了他们。"


translate chinese epilogue_mi_71666196:


    my "也许是吧..."


translate chinese epilogue_mi_1677698d:


    ma "也可以相信我们现在是在什么平行世界中。"


translate chinese epilogue_mi_0466237c:


    my "那个，我愿意相信任何理论..."


translate chinese epilogue_mi_63797f2e:


    ma "那么咱们目前就这样想吧"


translate chinese epilogue_mi_a27cf3ba:


    my "好吧..."


translate chinese epilogue_mi_f60fe5df:


    ma "咱们走吧?"


translate chinese epilogue_mi_16ade63c:


    my "你先走吧，我去去就回。"


translate chinese epilogue_mi_d0722fc6:


    ma "一个人太危险了..."


translate chinese epilogue_mi_23baa87a:


    my "说什么，有什么危险的，大白天，大家又都在这里。"


translate chinese epilogue_mi_5a3ed4bf:


    ma "好吧。"


translate chinese epilogue_mi_036327ef:


    "她惨淡的笑了笑，然后返回了食堂。"


translate chinese epilogue_mi_57d8a15d:


    "但是她的行为也很奇怪。"


translate chinese epilogue_mi_c1100f55:


    "似乎过去的几天改变了大家，包括玛莎..."


translate chinese epilogue_mi_deca8d3b:


    "昨天——震惊，害怕，发抖；今天——；沉着，冷静，理智。"


translate chinese epilogue_mi_8cd31d45:


    "见鬼！她比我还有自信!"


translate chinese epilogue_mi_55e9d52c:


    "是因为睡了一个好觉吗？不，绝对是她也有问题!"


translate chinese epilogue_mi_968e3ce8:


    "当然她没有向其他人一样认为是少先队员，但是..."


translate chinese epilogue_mi_750bd51e:


    "如果他们身上发生了什么，那么我身上可能也发生了什么..."


translate chinese epilogue_mi_1df2225f:


    "不！不可能，一切都很正常..."


translate chinese epilogue_mi_5c2c6e71:


    "我反复检查自己的身体，并没有发现什么不正常的地方。"


translate chinese epilogue_mi_361e4259:


    "好吧，我要保持冷静..."


translate chinese epilogue_mi_b72dfe95:


    "我站起来准备返回食堂，突然听到背后的声音。"


translate chinese epilogue_mi_45e11c59:


    "好像有人在叫我。"


translate chinese epilogue_mi_20953507:


    "我走下门廊，往前几步。"


translate chinese epilogue_mi_9f8b3255:


    "有人在树后面偷偷看着我。"


translate chinese epilogue_mi_a19d3e12:


    "是乌里扬卡，不，不是她，是一个很像她的东西..."


translate chinese epilogue_mi_a9617350:


    "我吓得定住了。"


translate chinese epilogue_mi_30c877bb:


    "她的脸有一半被撕裂成碎肉垂下来，她龇着牙的嘴巴不停的颤抖好像在笑一样，充满血丝的眼睛，在眼窝外面悬吊着，突然把瞳孔转向我。"


translate chinese epilogue_mi_70b8148d:


    "我停止了呼吸，好像现在就要因为恐惧而死。"


translate chinese epilogue_mi_e5b6bc80:


    us "谢苗..."


translate chinese epilogue_mi_b5deb76e:


    "像乌里扬娜的东西叫了我。"


translate chinese epilogue_mi_5da5e5ae:


    "我的双腿要坚持不住了。"


translate chinese epilogue_mi_85dad370:


    "我竭尽全力不要瘫掉，然后喊出来，逃跑..."


translate chinese epilogue_mi_c9e0ed8c:


    "但是我的意识已经抛弃的我的肉体，只剩下一个布娃娃，一个傀儡。"


translate chinese epilogue_mi_118f2951:


    "再过一秒钟我就要..."


translate chinese epilogue_mi_bfc45034:


    ma "嘿!"


translate chinese epilogue_mi_dca1260c:


    "我本能的转过去看，看到了玛莎站在门廊上，然后又本能的回头看“乌里扬娜”。"


translate chinese epilogue_mi_5007383a:


    "但是她消失了..."


translate chinese epilogue_mi_d04071e3:


    "玛莎好像注意到了我死人一样的表情，一瞬间就冲了过来。"


translate chinese epilogue_mi_49a38e33:


    ma "怎么了？发生了什么？"


translate chinese epilogue_mi_1c8f07f7:


    my "我刚刚...看到了乌里扬娜...在那儿..."


translate chinese epilogue_mi_02db51d2:


    "我指着那棵树。"


translate chinese epilogue_mi_f7f58c4b:


    "被屠杀的斯拉维娅看起来令人毛骨悚然..."


translate chinese epilogue_mi_c633ec22:


    "但是她至少是死掉的，没有在大白天满脸血肉走来走去，而且也没有叫我的名字（虽然说严格意义上那不是我的名字。）"


translate chinese epilogue_mi_3c3e173c:


    "我最后一滴力气也把我抛弃了，我双膝跪地摊在地上。"


translate chinese epilogue_mi_d3701ebe:


    ma "什么？哪里？"


translate chinese epilogue_mi_5217a1fa:


    "玛莎摇晃着我的肩膀。"


translate chinese epilogue_mi_b957be9d:


    my "不是她...不是那个我认识的..."


translate chinese epilogue_mi_9bf74aca:


    ma "清醒一点!"


translate chinese epilogue_mi_409bc2a3:


    "我感觉到一个巴掌打在我的脸上，我疑惑的看着玛莎。"


translate chinese epilogue_mi_32cd69c5:


    "我的脸颊很烫，但是我也开始恢复意识了。"


translate chinese epilogue_mi_e5d90224:


    my "乌里扬娜在那里，她的脸...{w}简单的说，是一个僵尸，是的，僵尸乌里扬娜，这已经是我能给出的最合理的解释了！"


translate chinese epilogue_mi_f159f577:


    "玛莎十分惊讶的看着我。"


translate chinese epilogue_mi_ddba048a:


    ma "这里什么都有可能发生..."


translate chinese epilogue_mi_0f8b803f:


    my "如果你没有亲眼看到她..."


translate chinese epilogue_mi_cbdc4e80:


    ma "谢天谢地我没有看到！来吧，我给你找点水！"


translate chinese epilogue_mi_a20cefa7_20:


    "..."


translate chinese epilogue_mi_33498a6c:


    "我和大家详细的说明了刚刚看到的情况。"


translate chinese epilogue_mi_3eab3cd4:


    ro "我只能告诉你们一件事，科学无法解释这里发生的一切!"


translate chinese epilogue_mi_db0c1724:


    dv "科学？这里完全疯狂了！撒旦之力...真希望这里能有一个教堂，我可以点燃一支蜡烛..."


translate chinese epilogue_mi_6a561d69:


    "列娜什么都没有说，只是任凭自己哭泣。"


translate chinese epilogue_mi_1a7c1a1e:


    "我差不多完全恢复了，开始能正常的思考。"


translate chinese epilogue_mi_0a4cb391:


    my "现在已经明确了，我们需要离开这里!"


translate chinese epilogue_mi_83ad4a9c:


    ro "嗯，我不是一个真正的科学家...更不要说是什么超自然学家，不过如果我记得没错的话，所有的这些恶魔还有灵魂都被绑定在一个固定的位置。因此，如果我们有办法离开这个地方..."


translate chinese epilogue_mi_f6b9796e:


    ma "那是一个很好的方法!"


translate chinese epilogue_mi_be002e4f:


    "玛莎怀疑的冷笑着。"


translate chinese epilogue_mi_ad288150_1:


    my "我同意。"


translate chinese epilogue_mi_605106a3:


    "我没有在意她，表示同意。"


translate chinese epilogue_mi_f8f4d1e6:


    dv "是啊!"


translate chinese epilogue_mi_bd8d303c:


    "列娜几乎让人看不见的点点头。"


translate chinese epilogue_mi_375aadac:


    my "好吧，那就这么决定了!"


translate chinese epilogue_mi_0505734e:


    "我一拳砸在桌子上，心中充满了激情。"


translate chinese epilogue_mi_ae1a89b2:


    ma "等一下，我们好像没有全都同意吧..."


translate chinese epilogue_mi_cd0d8c85:


    my "为什么要犹豫！？我可不想再见到那样的东西!"


translate chinese epilogue_mi_a7480fc6:


    ma "咱们要去哪里？你知不知道去城镇中心需要多长时间?"


translate chinese epilogue_mi_e0790bc4:


    my "我去，你以为那高速公路上没有一辆车经过？轿车，公交车，反正咱们总会遇到人的!"


translate chinese epilogue_mi_26c4382d:


    ro "我没有那么确定..."


translate chinese epilogue_mi_b9590f03:


    my "你觉得呆在这里更好?"


translate chinese epilogue_mi_ee0e2d60:


    ro "我也没那么说..."


translate chinese epilogue_mi_59962f16:


    un "这里太可怕了，尤其是晚上..."


translate chinese epilogue_mi_e2ce5d0c:


    "列娜悄悄的说着。"


translate chinese epilogue_mi_8690ec90:


    ma "那也不能说明咱们应该就去什么地方!"


translate chinese epilogue_mi_3cd95137:


    "玛莎大声喊着。"


translate chinese epilogue_mi_395daf08:


    "我眯起眼睛怀疑的看着玛莎，她绝对发生了什么变化。"


translate chinese epilogue_mi_c748f801:


    "也许我应该单独和她谈谈。"


translate chinese epilogue_mi_c2ecd2a9:


    "也许不应该这样？可能她也是..."


translate chinese epilogue_mi_a8506c23:


    my "好吧，咱们来投票吧!"


translate chinese epilogue_mi_f2f360c8:


    "我充满自信的说着，虽然我还是有疑问。"


translate chinese epilogue_mi_f988541d:


    "万一玛莎说的是对的怎么办？万一我们不应该漫无目的的闲逛？"


translate chinese epilogue_mi_0c03293b:


    "我们既不知道夏令营大门的外面有什么等着我们，也不知道继续呆在这里还有没有可能发生更可怕的事情..."


translate chinese epilogue_mi_40ef1a37_2:


    ma "好吧..."


translate chinese epilogue_mi_b53b2a58:


    "她耸耸肩。"


translate chinese epilogue_mi_0a36094e:


    "但是我还是不想在这里地方呆着!"


translate chinese epilogue_mi_5f20a77f:


    my "离开!"


translate chinese epilogue_mi_2b34d1a9:


    ma "留下"


translate chinese epilogue_mi_f4738552:


    dv "我们试试吧，也许真的运气很好遇到顺风车也说不定。"


translate chinese epilogue_mi_5bf88fe1:


    ro "离开..."


translate chinese epilogue_mi_a7287f96:


    "所有人都看着列娜。"


translate chinese epilogue_mi_151b8dfc:


    un "我不想留在这里..."


translate chinese epilogue_mi_4e16488f:


    "看来我怎么想意义也不大。"


translate chinese epilogue_mi_1521e234:


    "那走吧!"


translate chinese epilogue_mi_e130e273:


    my "走吧!"


translate chinese epilogue_mi_3059b01f:


    "我跳了起来，但是其他人都低着头坐在原地。"


translate chinese epilogue_mi_d04a0580:


    my "既然咱们已经决定了..."


translate chinese epilogue_mi_bc561ca0:


    ma "那个，既然已经决定了..."


translate chinese epilogue_mi_7b696593:


    "玛莎慢慢的站起来看着我。"


translate chinese epilogue_mi_5638fcf6:


    dv "我们的东西怎么办?"


translate chinese epilogue_mi_97d81d27:


    "阿丽夏尴尬的问。"


translate chinese epilogue_mi_445fb36b:


    ro "你真的觉得那些东西现在很重要?"


translate chinese epilogue_mi_ea7b081f:


    "Router嘀咕着。"


translate chinese epilogue_mi_31933f01:


    dv "好吧， 有道理..."


translate chinese epilogue_mi_75139ff0:


    "至于我，我的东西已经都不知道消失在什么地方了..."


translate chinese epilogue_mi_a20cefa7_21:


    "..."


translate chinese epilogue_mi_ede53218:


    "没过几分钟我们就来到了公交车站旁边。"


translate chinese epilogue_mi_6049b2ea:


    ma "城镇在那边。"


translate chinese epilogue_mi_38c29669:


    "玛莎指向远方的地平线。"


translate chinese epilogue_mi_ec1be65f:


    my "前天是在那里..."


translate chinese epilogue_mi_fbd62a8b_1:


    "我咧嘴笑着。"


translate chinese epilogue_mi_67d59d3c:


    ro "对啊，就是那边。"


translate chinese epilogue_mi_4264eb79:


    "Router进行了确认。"


translate chinese epilogue_mi_283c52cb:


    "我们发了一会儿呆，然后慢慢的朝着目的地的方向前进。"


translate chinese epilogue_mi_a20cefa7_22:


    "..."


translate chinese epilogue_mi_a20cefa7_23:


    "..."


translate chinese epilogue_mi_97183294:


    "夏天的太阳简直要把柏油路烤焦。"


translate chinese epilogue_mi_c0fe229c:


    "几百米以外的景色都被炙烤的空气扭曲了。"


translate chinese epilogue_mi_f47addae:


    "我身上的汗浸个不停，感觉自己就要中暑了。"


translate chinese epilogue_mi_770d11e5:


    "我们确实应该多带点儿水出来，不过刚刚我们一心想的就是要离开那个鬼夏令营。"


translate chinese epilogue_mi_4d43cd01:


    "我最后感觉要失去意识了。"


translate chinese epilogue_mi_7aadfff4:


    "只有蚱蜢没完没了的颤音让我还暂时保持着清醒。"


translate chinese epilogue_mi_c7b1fe99:


    "当然了！这酷夏的正午可是它们最喜欢的了。"


translate chinese epilogue_mi_3e8862d2:


    "他们可是一点也不在乎天气多么闷热。"


translate chinese epilogue_mi_9a28a9d2:


    "相反，它们感觉好极了，这就是它们末日交响曲的最佳幕景..."


translate chinese epilogue_mi_13fa06ef:


    "也许我们真的应该待在夏令营里也说不定。"


translate chinese epilogue_mi_abedaf26:


    "或者我们至少也应该晚一点出来。"


translate chinese epilogue_mi_5a9c692b:


    "虽然半夜在不知名的乡间小路散步也不是什么好主意..."


translate chinese epilogue_mi_7ea693c4:


    "我背后传来沉闷的哭声。"


translate chinese epilogue_mi_3c7e799a:


    "几十米开外，阿丽夏勉强的撑住列娜，让她不要倒在地上。"


translate chinese epilogue_mi_aec78f4c:


    "我马上冲了过去。"


translate chinese epilogue_mi_1d810d31:


    my "怎么回事?"


translate chinese epilogue_mi_ea512261:


    dv "她昏过去了!"


translate chinese epilogue_mi_ad5a6d31:


    "我帮忙把列娜抬到路边，至少这里还有一点树荫。"


translate chinese epilogue_mi_ac1911e9:


    ma "这就说明..."


translate chinese epilogue_mi_6a8b4df5:


    "玛莎摊在草丛里阴沉的说。"


translate chinese epilogue_mi_916de10c:


    ma "我警告过你了!"


translate chinese epilogue_mi_348aff25:


    my "好吧，但是现在说这个有什么用啊..."


translate chinese epilogue_mi_b117497a:


    "我们已经这样走了几个小时了，所以返回夏令营不太可能了，不过很快就要到晚上了，到时可能会凉快一些。"


translate chinese epilogue_mi_faf254ea:


    ro "但是我们还是一辆车都没有见到..."


translate chinese epilogue_mi_864e06fe:


    "我转过身看着Router。{w}他脱掉自己的衬衫，拧出里面的汗水。"


translate chinese epilogue_mi_7ba1bf38:


    "阿丽夏尝试着让列娜复苏。"


translate chinese epilogue_mi_38622444:


    my "如果是中暑的话..."


translate chinese epilogue_mi_f913a1de:


    dv "够了!"


translate chinese epilogue_mi_c8f00552:


    "她对我喊着。"


translate chinese epilogue_mi_25c82915:


    "如果她中暑了的话，我们恐怕是哪儿也去不了了。"


translate chinese epilogue_mi_961bbfe5:


    "我向前走了几步，用手遮住阳光，向远方望去。"


translate chinese epilogue_mi_fdbd24c7:


    "一望无际的道路，除了树还是树，没有文明的痕迹。"


translate chinese epilogue_mi_3444bdc0:


    "好像我们不仅仅是整个夏令营里幸存的人类，而且还是整个世界仅存的人类。"


translate chinese epilogue_mi_c43dec15:


    "虽然如果你考虑一下前几天所见到的东西也不难理解。"


translate chinese epilogue_mi_3915eb70:


    "我回去找大家，然后坐在木头堆上。"


translate chinese epilogue_mi_46d4b9bb:


    my "咱们可能要去前面侦察一下。"


translate chinese epilogue_mi_4c4c5aba:


    "我看着Router。"


translate chinese epilogue_mi_750d0f10:


    my "去看看前面有什么..也许可以发现什么线索...你们先在这里等着。"


translate chinese epilogue_mi_5a93ab85:


    dv "但是..."


translate chinese epilogue_mi_1ef5403c:


    my "她现在哪里也去不了。"


translate chinese epilogue_mi_2ff95471:


    "列娜闭着眼睛躺着，困难的呼吸着。"


translate chinese epilogue_mi_202c17a8:


    ma "好吧，走吧。"


translate chinese epilogue_mi_5dd3dee9:


    "玛莎冷静的说。"


translate chinese epilogue_mi_3b32e781:


    "我突然想起来自己还准备和她谈话来着，但是现在肯定不是很好的时机。"


translate chinese epilogue_mi_5175cd14:


    my "咱们走吧?"


translate chinese epilogue_mi_250c1da5:


    "Router耸耸肩，然后跟上了我。"


translate chinese epilogue_mi_a20cefa7_24:


    "..."


translate chinese epilogue_mi_f402d84a:


    "我们就这样走了十五分钟，女孩们被远远地甩在了后面。"


translate chinese epilogue_mi_9aa33312:


    ro "你怎么看，咱们有机会见到人吗?"


translate chinese epilogue_mi_a3533961:


    "他低声问着。"


translate chinese epilogue_mi_9ef048f4:


    my "我不知道!"


translate chinese epilogue_mi_987deb91:


    "我很厌烦的回答着，我现在完全不想谈话。"


translate chinese epilogue_mi_0a3b55b3:


    "田野延伸向天边，其中零星的点缀着森林和山丘。"


translate chinese epilogue_mi_e710db3b:


    "这种典型的平静的景色，让人十分焦虑。"


translate chinese epilogue_mi_68224858:


    "如果我们掉进了地狱，我们随时可能被袭击，那样还可以做好迎击的准备。"


translate chinese epilogue_mi_ebc7f617:


    "现在我们就像是一个无名画家画出来的中央俄罗斯的夏天里，或者是什么差不多的地方..."


translate chinese epilogue_mi_b17071ab:


    "这些都只能让我们更加害怕..."


translate chinese epilogue_mi_6ae2ce2f:


    ro "看!"


translate chinese epilogue_mi_631bd070:


    "Router喊了起来。"


translate chinese epilogue_mi_5b9ca325:


    "我眨眨眼睛，发现道路有个转弯。"


translate chinese epilogue_mi_d049f728:


    my "这有什么啦?"


translate chinese epilogue_mi_0d705a9c:


    ro "那边一定有什么东西!"


translate chinese epilogue_mi_7e25d624:


    my "你为什么这么想?"


translate chinese epilogue_mi_15aa17c6:


    ro "那个，因为有转弯啊，咱们一直都在走直线..."


translate chinese epilogue_mi_ae92f472:


    my "好吧，那去看看吧。"


translate chinese epilogue_mi_36b49d85:


    "过了几分钟，我们来到了...{w}小猫头鹰夏令营的大门..."


translate chinese epilogue_mi_971b05e7:


    "我双手掩面瘫倒在路边。"


translate chinese epilogue_mi_0fd2ac9d:


    "我没有感到害怕，相反我现在没有任何感觉，只有疲劳还有无尽的空虚。"


translate chinese epilogue_mi_bb76d279:


    ro "但是...这是...怎么回事?!"


translate chinese epilogue_mi_9840e224:


    "Router不停的说着。"


translate chinese epilogue_mi_429571ae:


    my "不知道...{w} 不过这有什么区别呢？跟过去这几天发生的事情相比，这个也没有奇怪到什么程度。"


translate chinese epilogue_mi_550ecf00:


    "我感觉精疲力尽。"


translate chinese epilogue_mi_9f5fe769:


    "Router的嘴唇在颤抖，他的脸上浮现出死亡般的恐惧。"


translate chinese epilogue_mi_2bf4d850:


    my "咱们回去找女孩儿们吧。"


translate chinese epilogue_mi_ca7fc9e6:


    "我没有等他，就这样返回了我们分开的位置。"


translate chinese epilogue_mi_a20cefa7_25:


    "..."


translate chinese epilogue_mi_9c57ac94:


    ma "找到什么了吗?"


translate chinese epilogue_mi_407fb42e:


    "玛莎吃着什么不知名的草莓问着。"


translate chinese epilogue_mi_e5032065:


    my "找到了...一个夏令营..."


translate chinese epilogue_mi_25d0d643:


    ma "什么夏令营?"


translate chinese epilogue_mi_efd76d15_1:


    "她惊讶的说。"


translate chinese epilogue_mi_11995523:


    my "我们的夏令营..."


translate chinese epilogue_mi_20f54ffc_2:


    ma "什么?"


translate chinese epilogue_mi_31b8751f:


    "玛莎现在看起来好傻，我没有忍住笑了出来。"


translate chinese epilogue_mi_855bda49:


    ma "但是你们在往前走，没有往回走啊。"


translate chinese epilogue_mi_c6243ca7:


    my "是在前面...小猫头鹰夏令营在前面。"


translate chinese epilogue_mi_7ae81d9e:


    ma "嘿，别开玩笑了!"


translate chinese epilogue_mi_8e302a5d:


    "她喊着。"


translate chinese epilogue_mi_c483e315:


    my "我是很严肃的..."


translate chinese epilogue_mi_5a93ab85_1:


    dv "但是..."


translate chinese epilogue_mi_01ffaf07:


    "阿丽夏说。"


translate chinese epilogue_mi_ab26c060:


    "她坐在草坪上，怀抱着列娜的脑袋，让她安心睡觉。"


translate chinese epilogue_mi_8b206af0:


    my "是啊，就是这样..."


translate chinese epilogue_mi_20dcd489:


    "我说完坐在地上。"


translate chinese epilogue_mi_bf48949e:


    ma "好吧，那咱们往那里走!"


translate chinese epilogue_mi_65d91c76:


    "我焦躁的看着玛莎，意识到自己不能再忍下去了。"


translate chinese epilogue_mi_de289d79:


    my "我说，你怎么回事?"


translate chinese epilogue_mi_9af1ef36:


    ma "你什么意思?"


translate chinese epilogue_mi_6e540bea:


    "她的表情似乎说明我们现在无法进行平静的对话。"


translate chinese epilogue_mi_ebb6fb85:


    my "昨天你还吓得要死，但是今天却一副理所当然的样子。"


translate chinese epilogue_mi_825a0cff:


    ma "但是恐慌有什么意义呢?"


translate chinese epilogue_mi_783cf09d:


    "她露出一丝几乎看不出来的微笑。"


translate chinese epilogue_mi_f484fdd8:


    my "如果我不认识你..."


translate chinese epilogue_mi_c6b1f443:


    "我咬着嘴唇，默默地走了。"


translate chinese epilogue_mi_db691b48:


    "但是说实话，我真的很了解她吗？我们是在一起很多年，但是..."


translate chinese epilogue_mi_e6e88a9f:


    "如果仔细想想，我一直更在乎自己。{w}或者说，我一直相信，她应该感觉很满足。"


translate chinese epilogue_mi_d1b363f3:


    "而且她对我总是那样，对一切都是一样的，即使是分手的时候，好像她不是一个人，只是我最喜欢的衣服。"


translate chinese epilogue_mi_cf143826:


    "我完全不了解她的内心，因为我从来就没有关心过!"


translate chinese epilogue_mi_63ac3909:


    "因此她现在的反应并不比我们遇到的其他事情奇怪。"


translate chinese epilogue_mi_2ff98e18:


    "如果想想两天以前在沙滩上的谈话..."


translate chinese epilogue_mi_2969b51f:


    "可能她真的比我要更坚强。"


translate chinese epilogue_mi_b28f3085:


    ma "嘿！你到底有没有听我说话?!"


translate chinese epilogue_mi_e9afa089:


    my "啊，抱歉，我走神了，发生了这么多事..."


translate chinese epilogue_mi_bcebe051:


    "她严厉的看着我。"


translate chinese epilogue_mi_3508c9b1:


    ma "咱们去营地吧，或者是什么别的地方，如果那个还是咱们出发时的营地的话..."


translate chinese epilogue_mi_a27cf3ba_1:


    my "好吧..."


translate chinese epilogue_mi_6352bf59:


    "其实我们也没有更多的选择，继续在烈日下行走只会导致更多的伤亡..."


translate chinese epilogue_mi_7241af63:


    ma "但是咱们得带着她"


translate chinese epilogue_mi_a516c62b:


    my "嗯...等一下，我来试试..."


translate chinese epilogue_mi_af54c066:


    "我在附近的空地见到两根解释的木棍。"


translate chinese epilogue_mi_e3e90986:


    "我回来以后命令Router脱掉衬衫。"


translate chinese epilogue_mi_9efba5a0:


    "他先是试图反抗，但是终于还是意识到这是徒劳。"


translate chinese epilogue_mi_914261b2:


    "我也脱掉自己的衬衫，然后制作出一个类似担架的东西。"


translate chinese epilogue_mi_4a929c62:


    ma "这个能坚持住吗?"


translate chinese epilogue_mi_0f7b726d:


    "玛莎开始表示质疑，不过她尝试了一下，确认了结实的程度然后微微一笑。"


translate chinese epilogue_mi_044b0bcd:


    my "可以的，至少可以坚持到营地..."


translate chinese epilogue_mi_a20cefa7_26:


    "..."


translate chinese epilogue_mi_5aba3686:


    "在这种天气下行走很是困难，但是抬着一个人就更困难了。"


translate chinese epilogue_mi_0c231086:


    "有好几次我几乎要晕过去，清醒过来发现自己快要摔倒了。"


translate chinese epilogue_mi_3c08fc74:


    "Router感觉更糟糕，玛莎和阿丽夏总是和他换位置。"


translate chinese epilogue_mi_b8ecaed9:


    "但是我的自尊心让我不去请求她们帮忙。"


translate chinese epilogue_mi_8915be50:


    "当然，这样很愚蠢，但是我就是忍不住。"


translate chinese epilogue_mi_539b7522:


    "这是我的又一个缺点，还有我的冷漠和漫不经心——即使是对自己所爱的人也是一样。"


translate chinese epilogue_mi_f81c0c30:


    "最终我们终于来到了大门处，我筋疲力尽的倒在草丛中，一座不知道是谁的少先队员雕塑面前。"


translate chinese epilogue_mi_867157a6:


    ma "真的是咱们的夏令营..."


translate chinese epilogue_mi_bdc084f3:


    "玛莎嘀咕着。"


translate chinese epilogue_mi_0947ef24:


    ma "确实看起来是一样的。"


translate chinese epilogue_mi_8fa854c5:


    dv "我们得把她送到房间里。"


translate chinese epilogue_mi_01ffaf07_1:


    "阿丽夏轻轻地说。"


translate chinese epilogue_mi_e31180f6:


    my "是啊..."


translate chinese epilogue_mi_c4f3ee83:


    "我费力的站起来，准备最后冲刺。"


translate chinese epilogue_mi_a20cefa7_27:


    "..."


translate chinese epilogue_mi_6ba66183:


    "我的选择显然是奥尔加·德米特里耶夫娜的房间。"


translate chinese epilogue_mi_15104e27:


    "至少我们昨天晚上成功的度过了，{w}至少我们还活着..."


translate chinese epilogue_mi_59348943:


    "我和Router一下子倒在床上，女孩儿们把列娜的衣服脱掉。"


translate chinese epilogue_mi_0a658695:


    ma "不要看!"


translate chinese epilogue_mi_72499cac:


    "玛莎严肃的说。"


translate chinese epilogue_mi_6731b709:


    my "也没有什么可看的..."


translate chinese epilogue_mi_6eb72cdb:


    "我抽抽鼻子转了过去，使劲的戳了Router的肋骨，让他学我。"


translate chinese epilogue_mi_e2b706b0:


    ma "我们去找点儿水，可能也要翻一翻食堂..."


translate chinese epilogue_mi_db46de88:


    my "自己去太危险了..."


translate chinese epilogue_mi_f76f1de1:


    ma "想要一起来吗?"


translate chinese epilogue_mi_fde537c1:


    "她笑着。"


translate chinese epilogue_mi_833e3637:


    my "不用了...你至少带上那根管子。"


translate chinese epilogue_mi_fefef18f_1:


    "我不太确定的说着。"


translate chinese epilogue_mi_f8f2e420:


    ma "你真的觉得这个有用吗?"


translate chinese epilogue_mi_6450d358:


    "我重重的叹了口气，闭上了眼睛。"


translate chinese epilogue_mi_59f84e9e:


    my "那...小心一点吧..."


translate chinese epilogue_mi_cf8ac78f:


    "门关上了。"


translate chinese epilogue_mi_4eaf6646:


    ro "你睡着了?"


translate chinese epilogue_mi_0fe78771:


    "Router对我说着悄悄话。"


translate chinese epilogue_mi_863570d0:


    my "能睡得着吗..."


translate chinese epilogue_mi_a7be59ec:


    "我突然听到列娜从旁边的床上传来的呻吟。"


translate chinese epilogue_mi_d9d00dfc:


    "我倚着胳膊肘看着她。"


translate chinese epilogue_mi_af832e65:


    "列娜在毛毯中不停的辗转反侧，颤抖着。"


translate chinese epilogue_mi_efc1edac:


    "突然间，她睁开眼睛看着我。"


translate chinese epilogue_mi_ca6611ca:


    "她的脸上浮现出魔鬼般邪恶的笑容。{w}和我昨天晚上看到的一样..."


translate chinese epilogue_mi_3eb7004f:


    un "很热啊，不是吗?"


translate chinese epilogue_mi_f1709333:


    "她大笑起来。"


translate chinese epilogue_mi_51df1151:


    "我感到心里流过一股寒气，说不出话来。"


translate chinese epilogue_mi_8199e005:


    my "你... 你..."


translate chinese epilogue_mi_6c21d46f_1:


    ro "什么?"


translate chinese epilogue_mi_586ee385:


    "Router在睡梦中问着。"


translate chinese epilogue_mi_f2047f00:


    "我马上转过去。"


translate chinese epilogue_mi_8aab4a10:


    my "看!"


translate chinese epilogue_mi_fe583ccc:


    "我伸手指向列娜的床。"


translate chinese epilogue_mi_6c21d46f_2:


    ro "什么?"


translate chinese epilogue_mi_58d3724b:


    "他起来看看，然后又过来问我。"


translate chinese epilogue_mi_49b544cf:


    "我转过去看到列娜平静的睡着。"


translate chinese epilogue_mi_866a81a4:


    my "你刚刚没有听到吗?"


translate chinese epilogue_mi_6c21d46f_3:


    ro "什么?"


translate chinese epilogue_mi_9cb45ed8:


    my "她...列娜醒了过来...然后说..."


translate chinese epilogue_mi_0717c0bb:


    ro "啊，你也中暑了吧..."


translate chinese epilogue_mi_4d9e42bd:


    "他转过去面朝着墙。"


translate chinese epilogue_mi_ea5146f0:


    "但是我确定我绝对看见了而且听见了..."


translate chinese epilogue_mi_0c0d3615:


    "好吧，也有可能确实我是中暑了..."


translate chinese epilogue_mi_42499ac9:


    "但是这看起来都和那个时候特别像。这不是列娜!"


translate chinese epilogue_mi_ef3c872b:


    "至少不是我认识的列娜！她好像被魔鬼控制了一样!"


translate chinese epilogue_mi_8209af39:


    "我躺下来，拉过来一条被子蒙在头上。"


translate chinese epilogue_mi_dead6866:


    "我真的想要睡觉，但是一想到我现在如果睡着的话就有可能再也醒不过来了，我还是尽量保持着自己清醒。"


translate chinese epilogue_mi_a600ff76:


    "我不知道我这样呆了多长时间，每一点细小的动静都让我颤抖，但是最后，门突然打开了。"


translate chinese epilogue_mi_3cdfabf7:


    "我从缝隙里偷偷看着，看到了阿丽夏和玛莎拿着一桶水还有一个大袋子。"


translate chinese epilogue_mi_f6d3ba26:


    ma "我看你已经有点疯了。"


translate chinese epilogue_mi_2e3f558d:


    my "是啊，有点..."


translate chinese epilogue_mi_24d62045:


    "我颤抖的回答着，试图笑一笑。"


translate chinese epilogue_mi_3657be4d:


    my "一切正常吗?"


translate chinese epilogue_mi_57a73d0f:


    ma "好像是这样的。"


translate chinese epilogue_mi_fb21cabf:


    "我又看了看列娜，但是她还是在平静的睡着。"


translate chinese epilogue_mi_bfd0137f:


    "也许真的就是我的幻觉?"


translate chinese epilogue_mi_7f889c60:


    "确实在这个精神经受着极端压力的情况下我是有可能看到什么不正常的东西的..."


translate chinese epilogue_mi_074f63d2:


    "我坐起来找了个杯子盛了一点水。"


translate chinese epilogue_mi_543f3a10:


    my "所以说咱们接下来要做什么?"


translate chinese epilogue_mi_98b3a8c1:


    ma "咱们暂时只能呆在这里了。"


translate chinese epilogue_mi_2ceeb90b:


    dv "但是奥尔加·德米特里耶夫娜已经叫了警察..."


translate chinese epilogue_mi_b75cedb9:


    "阿丽夏咕哝着。"


translate chinese epilogue_mi_9eab3900:


    ma "是啊，但是在那之后所有人都消失了。"


translate chinese epilogue_mi_0b0513f7:


    my "不管怎么说，等到列娜清醒过来。..."


translate chinese epilogue_mi_d162c751:


    "虽然说如果她醒过来还是那个样子的话，我还是宁愿她一直在这里睡着..."


translate chinese epilogue_mi_5d8b05ff:


    "阿丽夏躺在列娜的床上，玛莎坐在我的旁边。"


translate chinese epilogue_mi_09de083e:


    ma "睡一会儿吧。"


translate chinese epilogue_mi_4719c4e5:


    "她轻轻的抚摸着我的额头。"


translate chinese epilogue_mi_9c51ff5e:


    my "经历了这么多事你真的能睡着吗..."


translate chinese epilogue_mi_239bed8a:


    ma "我们会保持警戒的。你看起来那么累!"


translate chinese epilogue_mi_3c89501d:


    "我想她应该是对的。"


translate chinese epilogue_mi_8a2a3476:


    my "好吧，但是只要稍微休息一下，在晚上以前叫醒我。"


translate chinese epilogue_mi_43c3e662:


    "她没有说话，只是笑了笑。"


translate chinese epilogue_mi_b70a39b4:


    my "保证!"


translate chinese epilogue_mi_17742508:


    ma "我保证!"


translate chinese epilogue_mi_1ce87451:


    "玛莎悄悄的说道。"


translate chinese epilogue_mi_ba17b8a7:


    "我闭上眼睛，马上陷入了梦境之中。"


translate chinese epilogue_mi_a20cefa7_28:


    "..."


translate chinese epilogue_mi_934c20c6:


    "它们又来了—那些蚱蜢。"


translate chinese epilogue_mi_beaa4cc0:


    "这次是它们的国王—— 巨型的沼泽绿色怪兽，巨大的翅膀可以比得上飞机，还有巨大的黑眼睛。"


translate chinese epilogue_mi_f3c2c459:


    "它在我面前张牙舞爪，还噼噼啪啪的说着什么。"


translate chinese epilogue_mi_507578a0:


    "我完全不理解它们的语言，但是从语调上听起来肯定不是早间新闻那种好消息。"


translate chinese epilogue_mi_e7fe809f:


    "我想要逃走，但是它用邪恶的爪子牢牢地抓住了我。"


translate chinese epilogue_mi_4977fdd5:


    "我听见自己的骨头碎裂的声音，我的肋骨折断了，将我的各种内脏刺烂捅破。"


translate chinese epilogue_mi_7da28214:


    "我感觉有上百个锤子砸我的头骨，我的视线逐渐模糊，然后突然间新鲜的血肉喷泉汹涌的喷洒了一地..."


translate chinese epilogue_mi_a20cefa7_29:


    "..."


translate chinese epilogue_mi_48f17ebc:


    "我一身冷汗的跳了起来，我的梦还在不停的回响。"


translate chinese epilogue_mi_f92e8047:


    "我用了几秒钟的时间才恢复了自己的视觉。"


translate chinese epilogue_mi_6b7f9fb7:


    "我疯狂的看着周围，发现现在已经是晚上了，外面已经完全天黑。"


translate chinese epilogue_mi_d897367d:


    "还好，我们昨天没有关灯..."


translate chinese epilogue_mi_1d5cb079:


    "玛莎在我旁边睡着，Router在地上蜷着身子。"


translate chinese epilogue_mi_95cddf67:


    "列娜躺在旁边的床上，紧紧的裹在被子里。"


translate chinese epilogue_mi_52614955:


    "但是阿丽夏不见了。"


translate chinese epilogue_mi_333411d8_2:


    my "嘿!"


translate chinese epilogue_mi_f5a3eeec:


    "我猛地叫醒玛莎。"


translate chinese epilogue_mi_20f54ffc_3:


    ma "什么?"


translate chinese epilogue_mi_6f181833:


    "她慢慢的恢复了意识。"


translate chinese epilogue_mi_66e200e7:


    my "我没有叫你?!"


translate chinese epilogue_mi_cd89b571:


    ma "抱歉...我睡着了..."


translate chinese epilogue_mi_c592cc74:


    "她脸上愧疚的表情让我很难受。"


translate chinese epilogue_mi_4ab7a146:


    "我不应该靠她，至少应该自己定好闹铃..."


translate chinese epilogue_mi_95dc5bb2:


    "我叫醒Router。"


translate chinese epilogue_mi_10190a88:


    my "阿丽夏在哪里?"


translate chinese epilogue_mi_e77cd9de:


    ma "我不知道..."


translate chinese epilogue_mi_4b1dca0c:


    "玛莎担心的说着。"


translate chinese epilogue_mi_bb853559:


    my "谁知道呢？我们都睡着了!"


translate chinese epilogue_mi_8b687bf3:


    ma "那个，她先是在这里睡觉，然后..."


translate chinese epilogue_mi_e01ba3e1:


    "她的眼睛充满了歉意。"


translate chinese epilogue_mi_ed2f886e:


    my "然后你睡着了!"


translate chinese epilogue_mi_f5196784:


    "她摇摇头，叹气。"


translate chinese epilogue_mi_20b8375c:


    my "太好了，真的太好了!"


translate chinese epilogue_mi_0a0f07b3:


    "我一拳打在桌子上，让玛莎跳了起来。"


translate chinese epilogue_mi_b0069244:


    my "好吧，你坐在这里，我们去找她!"


translate chinese epilogue_mi_020772e0:


    ma "自己...?"


translate chinese epilogue_mi_49838f90:


    "她的脸上突然出现了恐惧的表情。"


translate chinese epilogue_mi_550b76e2:


    my "自己！不是你告诉我的没必要紧张的嘛?!"


translate chinese epilogue_mi_93f232b2:


    ma "是啊，但是那个..."


translate chinese epilogue_mi_ff3b375f:


    my "好了！我要走了!"


translate chinese epilogue_mi_c8d6b6d8:


    ma "但是..."


translate chinese epilogue_mi_42faecaa:


    my "还是说你真的想和列娜呆在这里?"


translate chinese epilogue_mi_96cd1356:


    "玛莎没有说话。"


translate chinese epilogue_mi_68e74430:


    ro "自己去太危险了..."


translate chinese epilogue_mi_2dca72b2:


    "我发现Router似乎已经完全清醒了过来。"


translate chinese epilogue_mi_81495ac1:


    my "她自己在这儿待着不危险吗?"


translate chinese epilogue_mi_8d4fc7d5:


    "我觉得继续争吵没有什么意义，于是拿上我的管子，朝门口走去。"


translate chinese epilogue_mi_f498c94c:


    "不管怎么说，既然人们都可以凭空消失，那么不管周围有什么人，自己都不怎么安全。"


translate chinese epilogue_mi_3bd941f9:


    "当然阿丽夏也有可能就是去上厕所了什么的。"


translate chinese epilogue_mi_cfe4baf3:


    "虽然说看到她害怕的样子她应该会叫醒玛莎或者等到明天早上..."


translate chinese epilogue_mi_9ce03507:


    "反正不可能自己在营地里走!"


translate chinese epilogue_mi_979b3743:


    "我攥攥拳，拉动把手，往门外走去。"


translate chinese epilogue_mi_e426c290:


    "我扫视着四周..."


translate chinese epilogue_mi_97b95495:


    "我看到了阿丽夏...{w} 高高的吊在房前的树上..."


translate chinese epilogue_mi_03acb541:


    "我的太阳穴一阵剧痛，我一下子瘫倒在地上。"


translate chinese epilogue_mi_19e8325e:


    "这种恐惧感十分强烈，我感觉自己的心脏被绑上了链条。"


translate chinese epilogue_mi_c71ffa7a:


    "我受到了惊吓，勉强爬回了房间，关上了门。"


translate chinese epilogue_mi_78bb1682:


    ma "怎么回事?!"


translate chinese epilogue_mi_3c910ff4:


    "玛莎冲了过来。"


translate chinese epilogue_mi_3d6cfafe_1:


    my "别看!"


translate chinese epilogue_mi_36ea64e3:


    "我拉住她的胳膊。"


translate chinese epilogue_mi_6f6e2c1b:


    my "别出去!"


translate chinese epilogue_mi_39555e25:


    ma "怎么啦?!"


translate chinese epilogue_mi_ce032298:


    "她想要去开门，我试图阻止她，但是我身体一阵虚弱，只发出了悲惨的呻吟。"


translate chinese epilogue_mi_f0dd9ecf:


    "玛莎打开门，发出了一声尖叫。"


translate chinese epilogue_mi_0e2c4200:


    "我用上最后的力气，一脚踢上了门。"


translate chinese epilogue_mi_fd6cf8ba:


    "玛莎昏了过去，倒在了我的怀里。"


translate chinese epilogue_mi_9b50f023:


    "我把她抱到床上，在Router的旁边。"


translate chinese epilogue_mi_8e5b2243:


    "看他的表情，似乎是对于整件事情完全不在意。"


translate chinese epilogue_mi_c545824e:


    "我们的喧嚣打扰了列娜，她坐起来茫然的看着我们。"


translate chinese epilogue_mi_012448da:


    un "出了什么事...?"


translate chinese epilogue_mi_6c39322b:


    "她悄悄的问着。"


translate chinese epilogue_mi_3f27c2c0_1:


    "我不知道应该怎么回答。"


translate chinese epilogue_mi_7a995e66:


    "我感觉自己的嗓子有种要喊出来的冲动，但是最终还是遏制住了，只是发出了一阵叹息。"


translate chinese epilogue_mi_ed8a0e3f:


    "列娜有点恐惧，但是并不了解究竟发生了什么。"


translate chinese epilogue_mi_e35e5625:


    my "不要...出门..."


translate chinese epilogue_mi_f9065ef9:


    "I moaned."


translate chinese epilogue_mi_657108f7:


    "她和玛莎不一样，没有反驳。"


translate chinese epilogue_mi_cca21832:


    "接着又是一阵压抑的沉默。"


translate chinese epilogue_mi_761c4b2e:


    "连蚱蜢都不再鸣叫似乎就是为了加重我们这里的紧张气氛。"


translate chinese epilogue_mi_1bafb56b:


    "玛莎还躺在我的膝盖上，Router在床的角落里不停的哆嗦，列娜在我的面前，脸上充满了绝望。"


translate chinese epilogue_mi_3874b4a4:


    "我不敢眨眼睛的盯着面前的大门，等着它突然打开的瞬间..."


translate chinese epilogue_mi_d8767c7a:


    "我的恐惧不能实在的表现出来，疯子，怪物，外星人...{w}是关于未知，黑暗，还有空虚的恐惧。"


translate chinese epilogue_mi_ce811c64:


    "我可能会像斯拉维娅一样被撕碎，或者像乌里扬娜一样变成僵尸，但是我都没有害怕。"


translate chinese epilogue_mi_170b2003:


    "我害怕的是自己会突然消失，一点儿痕迹不留的离开这个世界..."


translate chinese epilogue_mi_e374bab2:


    "玛莎无声的哭了出来。"


translate chinese epilogue_mi_4fce6de4:


    ma "我刚刚做了噩梦..."


translate chinese epilogue_mi_908757cc:


    my "这不是梦..."


translate chinese epilogue_mi_bb759ffc:


    "她大大的睁开眼睛，恐惧的看着我。"


translate chinese epilogue_mi_198e3f4c:


    ma "那... 那里?!"


translate chinese epilogue_mi_3d6cfafe_2:


    my "别看!"


translate chinese epilogue_mi_741958cd:


    "我一边喊着，一边迅速的抱住了她。"


translate chinese epilogue_mi_b2cd5a0c:


    "列娜大声哭了起来，跑过来扑到我的胸前。"


translate chinese epilogue_mi_bd486098:


    "在痛苦的沉默中，时间一分分过去。"


translate chinese epilogue_mi_282e0eee:


    ma "那...咱们应该怎么办呢?"


translate chinese epilogue_mi_10a44f66:


    "玛莎带着眼泪说着。"


translate chinese epilogue_mi_b44a635d:


    "她现在没有那么自信了。虽然说我们都是一样的状态..."


translate chinese epilogue_mi_777a7686:


    "时间不断地流逝，但是什么也没有发生。"


translate chinese epilogue_mi_af951e50:


    "我们得作出决定。"


translate chinese epilogue_mi_104c4b70:


    "我不能说自己在看见那个以后终于恢复了理智，但是我感觉自己更加清醒了一些。"


translate chinese epilogue_mi_bf92a608:


    "但是有一件事我可以确认，我们不能再在这个地方待着了！不是因为这里最危险..."


translate chinese epilogue_mi_b1129cb1:


    "只是一直呆在阿丽夏的尸体旁边我受不了。"


translate chinese epilogue_mi_81f3f410:


    "相信对于女孩儿们来说更是想要呕吐..."


translate chinese epilogue_mi_5ec6958d:


    my "咱们必须走了..."


translate chinese epilogue_mi_5788b9db:


    "我悄悄的说。"


translate chinese epilogue_mi_2f93c801:


    "我感觉到玛莎和列娜更加用力的抱着我。"


translate chinese epilogue_mi_7cabd0eb:


    ro "必须..."


translate chinese epilogue_mi_48701c6a:


    "Router不在颤抖了，然后站了起来。"


translate chinese epilogue_mi_32912129:


    my "你感觉怎么样?"


translate chinese epilogue_mi_3329e7d9:


    "我问列娜。"


translate chinese epilogue_mi_d00d3d0f:


    "她什么也没有说。"


translate chinese epilogue_mi_95fe422d:


    my "你可以走路吗?"


translate chinese epilogue_mi_3c68ea6c:


    un "大概可以..."


translate chinese epilogue_mi_5d37a806:


    "我轻轻的摆脱了她们的怀抱。"


translate chinese epilogue_mi_f90fc978:


    my "咱们得跑了，晚上还要容易一些。"


translate chinese epilogue_mi_d58a29ab:


    ma "但是咱们..."


translate chinese epilogue_mi_8f19793e:


    "玛莎试图做出简单的反抗。"


translate chinese epilogue_mi_73866edf:


    my "是啊，我知道咱们是决定留下来了，但是...咱们得离开这个地方!"


translate chinese epilogue_mi_951f9710:


    "我转向Router。"


translate chinese epilogue_mi_ef2e4fda:


    my "你应该知道...她们不应该看到的。"


translate chinese epilogue_mi_a91e5c2f:


    "他微微的点了点头。"


translate chinese epilogue_mi_2aa79f94:


    "虽然Router不知道阿丽夏到底怎么了，但是根据他自己的联想可能受到的惊吓不亚于我。"


translate chinese epilogue_mi_7037899d:


    my "好吧，咱们拉起手一起走，我在最前面，然后是玛莎，然后是列娜，然后是你。"


translate chinese epilogue_mi_6b024c96:


    "我看着女孩儿们。"


translate chinese epilogue_mi_dd76c6a4:


    "她们在低着头抽泣。"


translate chinese epilogue_mi_549e20f2:


    my "准备好了吗?"


translate chinese epilogue_mi_c5d5810d:


    "没有人回答—这个问题也没有什么实际意义，谁也不可能准备好的。"


translate chinese epilogue_mi_35423a3b:


    "我拉住玛莎的手，严肃的看着列娜和Router。"


translate chinese epilogue_mi_f91862ca:


    my "好了，现在你们闭上眼睛，然后不管遇到什么事情都不要挣开，好吗?"


translate chinese epilogue_mi_44cacce6_2:


    ro "嗯..."


translate chinese epilogue_mi_f7a66aa8:


    "列娜点点头。"


translate chinese epilogue_mi_7983c203:


    "我拉住玛莎的手，一脚踹开门往外走去。"


translate chinese epilogue_mi_eec4cf2b:


    my "注意脚下!"


translate chinese epilogue_mi_f298ccdd:


    "等我们出来的时候，我瞥了一眼阿丽夏。"


translate chinese epilogue_mi_bb16154d:


    "这次我没有被吓趴下，虽然我的心脏重重的跳个不停，我感觉晕眩恶心想要吐出来。"


translate chinese epilogue_mi_0573dd61:


    "她在距离地面几米的地方，被红领巾绑住了喉咙。"


translate chinese epilogue_mi_cdda0258:


    "突出的眼球，伸出来的舌头，身体已经开始腐烂，好像她不是几个小时以前死掉的，而是至少死掉好几天了。"


translate chinese epilogue_mi_c76687ee:


    "像是受到了非人的痛苦的表情凝固在她的脸上。"


translate chinese epilogue_mi_a33a94a5:


    "很明显她不可能是对自己这样下手。"


translate chinese epilogue_mi_788c07e6:


    "天哪，这到底是谁干的，难道我在地狱...?"


translate chinese epilogue_mi_aa8da201:


    "我加快了步伐，离开这篇痛苦的景色。"


translate chinese epilogue_mi_a20cefa7_30:


    "..."


translate chinese epilogue_mi_912b3321:


    "我到了广场才让他们睁开眼睛。"


translate chinese epilogue_mi_d2d78eec:


    ma "她...还在那里吗?"


translate chinese epilogue_mi_55b4c0e5:


    "玛莎没有放开我的手，突然问道。"


translate chinese epilogue_mi_413cf259:


    "我只是点点头。"


translate chinese epilogue_mi_8c308047:


    un "谁?! 阿丽夏?!"


translate chinese epilogue_mi_c6b1fe3f:


    "列娜哭喊着。"


translate chinese epilogue_mi_6e61e470:


    "玛莎抱住列娜试图安抚她。"


translate chinese epilogue_mi_5d58c3b8:


    ro "她怎么了?"


translate chinese epilogue_mi_54683d6b:


    "Router用颤抖的声音问着。"


translate chinese epilogue_mi_50c8a92f:


    my "你们不会想知道的。"


translate chinese epilogue_mi_290e56ee:


    "我就站在这里看着月亮。"


translate chinese epilogue_mi_ac9a276c:


    "列娜还在哭着，Router不停的来回踱步，玛莎掩面坐着。"


translate chinese epilogue_mi_f077aeea:


    my "好了，到时间了..."


translate chinese epilogue_mi_54ac6199:


    "说完没有人动。"


translate chinese epilogue_mi_727a919f:


    my "咱们不能待在这里!"


translate chinese epilogue_mi_38a7da59:


    "我找到玛莎，拉住她的手。"


translate chinese epilogue_mi_2178548f:


    "她用充满泪水的眼睛看着我，点了点头。"


translate chinese epilogue_mi_891afcb1:


    "我们继续往车站走去..."


translate chinese epilogue_mi_a20cefa7_31:


    "..."


translate chinese epilogue_mi_d6e969ac:


    "夜幕降临了。"


translate chinese epilogue_mi_2b88b9e3:


    "我注意到路上的蚱蜢都很安静。"


translate chinese epilogue_mi_4d1b313b:


    "它们安静了一会儿，然后又恢复了那烦人的歌声，但是这次声音稍微小了一点，可以说是有了一些礼貌。"


translate chinese epilogue_mi_24889ac6:


    "白天的喧嚣变成了交响乐队的小夜曲。"


translate chinese epilogue_mi_af1d202f:


    "它们有没有可能了解这里发生了什么...?"


translate chinese epilogue_mi_5a24d3d5:


    "当然，如果我们假设它们在地狱里也是蚱蜢的话..."


translate chinese epilogue_mi_efbb6c03:


    "我们来到车站，停了下来，犹豫着。"


translate chinese epilogue_mi_b2bc2a02:


    ma "咱们要去哪里呢?"


translate chinese epilogue_mi_e3229b58:


    "玛莎抹着眼泪问着。"


translate chinese epilogue_mi_1ca97523:


    my "好吧，咱们已经试过了往右走，这次试试左边。"


translate chinese epilogue_mi_bf0ef4ba:


    ma "但是那边没有城镇..."


translate chinese epilogue_mi_c9afa297:


    my "几天以前右边还没有夏令营的..."


translate chinese epilogue_mi_b4e45382:


    un "也许咱们不应该..."


translate chinese epilogue_mi_49be4260:


    "列娜低声抽泣着。"


translate chinese epilogue_mi_f2ab6c8a:


    my "咱们没有选择...我再也不在这里待着了!"


translate chinese epilogue_mi_93a07a2a:


    "我们正准备离开这里，突然听见远远的有什么动静。"


translate chinese epilogue_mi_bb6a2944:


    "我仔细看着，发现了有人沿着公路往这边来。"


translate chinese epilogue_mi_cbc33115:


    ro "一辆公交车!"


translate chinese epilogue_mi_2b6b2a9a:


    "Router充满喜悦的大喊着。"


translate chinese epilogue_mi_89a22708:


    my "闭嘴，你个傻子！那不是公共汽车！是有人！"


translate chinese epilogue_mi_6f0bbf9c:


    "我教训着他。"


translate chinese epilogue_mi_aea41fc4:


    ma "很多人..."


translate chinese epilogue_mi_c67e1ffd:


    "玛莎焦虑的说着。"


translate chinese epilogue_mi_f5f53cd8:


    "现在事情已经很清楚了，我们一秒钟也不能呆在这里了。"


translate chinese epilogue_mi_353f7742:


    "我转过身， 发现另外一个方向也有很多人在不断接近我们。喧嚣声越来越大。"


translate chinese epilogue_mi_a3e8eadb:


    "蚱蜢的鸣叫... 但是不是它们平常的叫声，现在声音更大，更加扭曲，好像加上了扩音器一样。"


translate chinese epilogue_mi_1f4d46ef:


    my "跑!"


translate chinese epilogue_mi_c34e863e:


    "我喊着，但是没有人动。"


translate chinese epilogue_mi_b6e2867e:


    my "啊，见鬼!"


translate chinese epilogue_mi_f6db9d2f:


    "我拉起玛莎和列娜疯狂的往营地跑回去。"


translate chinese epilogue_mi_f19be6a7:


    "女孩儿们恍惚的不知所措，所以我真的就是在拽着她们。"


translate chinese epilogue_mi_fae82ef7:


    "我冲到了广场，停下来喘口气，我这时才意识到Router被扔在了后面。"


translate chinese epilogue_mi_b1c873bb:


    ma "咱们得回去找他!"


translate chinese epilogue_mi_41f008ff:


    "玛莎一边喊着一边往车站跑。"


translate chinese epilogue_mi_a900b142:


    my "你疯了?!"


translate chinese epilogue_mi_f149054d:


    "我拉住她，强迫她停了下来。"


translate chinese epilogue_mi_c95121c1:


    my "你想要让咱们全躺在那里?!"


translate chinese epilogue_mi_3d0e96c9:


    "列娜不停的颤抖着，我拉着她的手，自己也跟着颤抖起来。"


translate chinese epilogue_mi_27fc3c78:


    my "一直跑!"


translate chinese epilogue_mi_3a8faa33:


    ma "去哪儿?!"


translate chinese epilogue_mi_d149a305:


    my "不知道，去树林里!"


translate chinese epilogue_mi_b84b36c8:


    "现在回到奥尔加·德米特里耶夫娜的房间可不太好。"


translate chinese epilogue_mi_2e4f2c35:


    "我的自我保护本能告诉我也不应该停留在开阔地带。"


translate chinese epilogue_mi_4215b313:


    "我们冲向森林。"


translate chinese epilogue_mi_4716024f:


    "我紧紧的抓住女孩儿们的手。"


translate chinese epilogue_mi_0e01cbac:


    "她们艰难的跑着，尤其是摇摇晃晃的列娜，她跌倒了几次，但是我很快就扶她起来，大声喊着鼓励她，然后拉着她继续跑。"


translate chinese epilogue_mi_b9aff25b:


    "穿过树林是最艰难的一部分。"


translate chinese epilogue_mi_30941c88:


    "虽然满月就挂在空中，但是我还是经常撞上树枝，跌进坑里。"


translate chinese epilogue_mi_90e827b2:


    "荨麻刮着我的腿，枝叶不停打着我的脸，划得很疼。"


translate chinese epilogue_mi_2795f897:


    "我的心脏扑通扑通的跳着，我的每一次呼吸都会让肺部疼痛，大脑中的汹涌的血液好像可能会让我的头爆炸。"


translate chinese epilogue_mi_5379c1e1:


    "我开始失去知觉，只是靠腿自己带着往前走。"


translate chinese epilogue_mi_1df107da:


    "最后我们终于来到了一片空地，停了下来。"


translate chinese epilogue_mi_2a7f7693:


    "我们都精疲力竭，女孩儿们跌倒在地上，我坐在一个树桩上。"


translate chinese epilogue_mi_274d0040:


    "他们是谁？人？鬼？或者什么其他的？"


translate chinese epilogue_mi_a42af439:


    "还有那个像蚱蜢一样的鸣叫声是怎么回事?"


translate chinese epilogue_mi_53300ec8:


    "我在夜色中无法看清，但是这可能是最好的情况了。"


translate chinese epilogue_mi_b8132fa2:


    "我恢复了知觉以后朝女孩儿们走去。"


translate chinese epilogue_mi_1b7c0a78:


    "列娜像死人一样躺在地上，好像完全没有了生气。"


translate chinese epilogue_mi_9cc83bd6:


    "玛莎抱膝坐着，不停的晃动着。"


translate chinese epilogue_mi_60b21c95:


    my "我们得做点儿什么..."


translate chinese epilogue_mi_38896a66:


    "我坐在她旁边说着。"


translate chinese epilogue_mi_51e97271:


    ma "太晚了...咱们会死掉的..."


translate chinese epilogue_mi_5a9cdae9:


    "她用空虚的声音说着"


translate chinese epilogue_mi_226dce17:


    my "咱们还活着，这说明还是有希望的。"


translate chinese epilogue_mi_0a647cc5:


    ma "不，现在没有了...全完了..."


translate chinese epilogue_mi_872ed3da:


    "我抱着她，紧紧的搂住她。"


translate chinese epilogue_mi_85ff07ec:


    my "还没有..."


translate chinese epilogue_mi_a20cefa7_32:


    "..."


translate chinese epilogue_mi_f54fc1f7:


    "我坐下来，盯着远处的森林。"


translate chinese epilogue_mi_cb4ba357:


    "现在已经几乎听不到蚱蜢的声音了。"


translate chinese epilogue_mi_dda9e6e8:


    "如果不是听到玛莎的呼吸声，我可能觉得自己已经死了。"


translate chinese epilogue_mi_661c87b5:


    ma "原谅我..."


translate chinese epilogue_mi_c1e898ec:


    "她悄悄的说着。"


translate chinese epilogue_mi_9a8df94b:


    my "为什么?"


translate chinese epilogue_mi_ae39ac0b:


    ma "一切...因为我是这个样子...因为所有咱们没有做到的事情..."


translate chinese epilogue_mi_256eea8e:


    my "现在时机不太合适..."


translate chinese epilogue_mi_0fd16807:


    "我温柔的说着。"


translate chinese epilogue_mi_2b77fde2:


    ma "咱们没有什么时间了...所以我觉得这可能是时候了。"


translate chinese epilogue_mi_403d5760:


    "她抬头看着我，惨淡的笑着。"


translate chinese epilogue_mi_4b94df60:


    ma "请你原谅我，如果我一直是那么自私的话..."


translate chinese epilogue_mi_d16187bb:


    "我看着她的眼睛，她清澈的眼睛，我感觉有一滴热泪沿着脸颊流了下来。"


translate chinese epilogue_mi_2110e4c0:


    my "不，我才应该抱歉..."


translate chinese epilogue_mi_a70dec20:


    "我现在完全不知道说什么好。"


translate chinese epilogue_mi_a84db421:


    "我对她做错了很多事，让她这么痛苦，在最后的时刻，除了一句对不起我竟然什么别的话都想不出来。"


translate chinese epilogue_mi_852082f5:


    "我对自己的无能感到愤怒，还有我不能保护自己唯一亲爱的人。"


translate chinese epilogue_mi_bf98df2f:


    my "你知道，我一直爱你..."


translate chinese epilogue_mi_f538b416:


    ma "我知道..."


translate chinese epilogue_mi_a68bf534_1:


    "她悄悄地说着。"


translate chinese epilogue_mi_7a73e50c:


    "玛莎又笑了，虽然她的脸不停的颤抖，脸上一直挂着热泪。"


translate chinese epilogue_mi_8cbe7e05:


    my "而且我还爱着你..."


translate chinese epilogue_mi_f538b416_1:


    ma "我知道..."


translate chinese epilogue_mi_6a903ceb:


    "我受不了了—我想要转过去，闭上眼睛..."


translate chinese epilogue_mi_8e9b5288:


    "我承受不了最后这一刻的软弱!"


translate chinese epilogue_mi_d1d01520:


    "我低下身，用力的亲吻着她。"


translate chinese epilogue_mi_99de19ed:


    "时间静止了。{w}这个受诅咒的夏令营，漆黑的夜晚，这些地狱般的鬼蚱蜢终于消失了!"


translate chinese epilogue_mi_b67167b0:


    "整个宇宙都消失了，只剩下我们两个..."


translate chinese epilogue_mi_ce617998_2:


    "..."


translate chinese epilogue_mi_daa91f05:


    "我觉得这一切都是一个噩梦，树林就要继续沙沙作响。"


translate chinese epilogue_mi_79516c27:


    "远处的植被颤动了几下，几个模糊的黑影出现了。"


translate chinese epilogue_mi_57818ffd:


    "我还是看不清楚，但是我确定这就是我们在公交车站看到的东西。"


translate chinese epilogue_mi_aa083e71:


    "我的整个身体都绝望的呆住了，我害怕的不敢呼吸。"


translate chinese epilogue_mi_58343c59:


    ma "所以这就是我们的结局了..."


translate chinese epilogue_mi_d8b53ec6:


    "玛莎静静的说着，紧紧的抓着我。"


translate chinese epilogue_mi_6bd68c39:


    "不，这不是结局!"


translate chinese epilogue_mi_198fcb2e:


    "突然间我明白了，我要为了她战斗到最后一滴血。"


translate chinese epilogue_mi_1139d9cb:


    "只要有一点活下去的希望!"


translate chinese epilogue_mi_c7c16c8a:


    "我跳起来，猛地拉住她，冲向列娜。"


translate chinese epilogue_mi_118fa0c9:


    "她像是死了一样躺在那里好像已经完全不知道发生了什么。"


translate chinese epilogue_mi_29d61edf:


    "我开始摇晃她的肩膀。"


translate chinese epilogue_mi_97a2b531:


    my "起来!"


translate chinese epilogue_mi_7e5612ad:


    "没有反应。"


translate chinese epilogue_mi_fefd014c:


    "我使劲扇了她几下，她发出一阵低沉的呻吟。"


translate chinese epilogue_mi_09ccb2a2:


    my "起来！快!"


translate chinese epilogue_mi_27ba2774:


    "我拽起列娜的时候她完全不解的看着我。"


translate chinese epilogue_mi_1f4d46ef_1:


    my "咱们快跑!"


translate chinese epilogue_mi_e900bc14:


    "我拉住她们的手，然后漫无目的的跑着，越快越好。"


translate chinese epilogue_mi_7a43a1d3:


    "我们不停的摔倒，站起来，穿过厚厚的丛林，最终，声音消失了。只剩下我们还有吵闹的蚱蜢。"


translate chinese epilogue_mi_1464e77a:


    "远处有灯光闪现。"


translate chinese epilogue_mi_a20cefa7_33:


    "..."


translate chinese epilogue_mi_2bf52109:


    "很快我们来到了一块完全被树丛包围的空地。这里有一座像是幼儿园的建筑。"


translate chinese epilogue_mi_74cb7d4d:


    my "那是什么?"


translate chinese epilogue_mi_46835ec1:


    "我结巴的问着。"


translate chinese epilogue_mi_809c3b70:


    un "这是旧营地的建筑..."


translate chinese epilogue_mi_0cb7a1ff:


    "列娜有气无力的说着。"


translate chinese epilogue_mi_bb7631a3:


    "她好像也渐渐的恢复神智了。"


translate chinese epilogue_mi_7c3c1f20:


    "我犹豫的停了下来。"


translate chinese epilogue_mi_153ace4d:


    "这太幼稚了，觉得这里可能有避难所什么的，以现在的情况来说我们在哪里都不会真正安全的。"


translate chinese epilogue_mi_aa5cdda1:


    "另一方面来说，我们就在崩溃的边缘，尤其是女孩儿们。"


translate chinese epilogue_mi_4015903e:


    "我踌躇的往校舍那里前进，当我背后出现嘶嘶的响声时，我惊慌的突然转过身。"


translate chinese epilogue_mi_84eb76e0:


    "这种噪音越来越大，然后我们突然听到了巨大的鸣叫声。"


translate chinese epilogue_mi_1aaec2c1:


    "我用上超人的力量转过来。"


translate chinese epilogue_mi_e9aee5ea:


    "我后面有一群人...{w}一群小女孩儿。"


translate chinese epilogue_mi_dc80e866:


    "或者说，一群乌里扬娜..."


translate chinese epilogue_mi_fddbd92f:


    "她们都是我今天早晨看到的那个样子——被撕裂的脸还有邪恶扭曲的笑容。"


translate chinese epilogue_mi_9e72eb1c:


    "她们都发出着恐怖的声音，像是蚱蜢的鸣叫，让我的心脏感觉寒冷的恶魔之音。"


translate chinese epilogue_mi_52648267:


    "我们就呆在原地，一步也迈不开。"


translate chinese epilogue_mi_b22355a1:


    "突然间，其中一个乌里扬娜走了出来，拿出来...{w}Router的头。"


translate chinese epilogue_mi_f58f26f9:


    "痛苦的恐惧永远停留在了他的脸上。"


translate chinese epilogue_mi_67e4b031:


    "她开始说话的时候我几乎吓得晕了过去。"


translate chinese epilogue_mi_24eb79a9:


    us "你好，谢苗！最近怎么样?"


translate chinese epilogue_mi_64c1dacf:


    "她的声音就像是打穿我的大脑的子弹，我尖叫着，拉着两个女孩儿跑进了旧校舍。"


translate chinese epilogue_mi_85b7581c:


    "我必须承认，那个时候我完全忘记了玛莎和列娜。"


translate chinese epilogue_mi_6fa07538:


    "也许她们也在尖叫，也许她们吓得说不出话来，我都不知道。"


translate chinese epilogue_mi_e3a0e8e4:


    "现在最重要的事情是离这些怪物越远越好。"


translate chinese epilogue_mi_2da23a26:


    "我们逃进了建筑里，突然间地面消失不见了我们掉进了黑暗之中..."


translate chinese epilogue_mi_a20cefa7_34:


    "..."


translate chinese epilogue_mi_a20cefa7_35:


    "..."


translate chinese epilogue_mi_6c66893a:


    "我在完全的黑暗中醒了过来。"


translate chinese epilogue_mi_a490ddde:


    "我的整个身体都疼的要命，尤其是我的腿，但是不知怎么我还是靠着墙站了起来。"


translate chinese epilogue_mi_20b70770:


    "乌里扬娜的声音已经听不见了，我这才稍微平静了一些。"


translate chinese epilogue_mi_7f05575c:


    "我用虚弱的声音问着:"


translate chinese epilogue_mi_02b36d0b:


    my "玛莎... 列娜..."


translate chinese epilogue_mi_4fc4bd86:


    "我听到附近有动静，有什么东西抓住了我的腿。"


translate chinese epilogue_mi_23250185:


    "我本能的跳开了，不过同时下面传来一阵虚弱的声音。"


translate chinese epilogue_mi_5854ece9:


    ma "咱们还活着吗...?"


translate chinese epilogue_mi_00cfd6e5:


    my "好像是的。"


translate chinese epilogue_mi_a313a700:


    "我帮助玛莎站起来。"


translate chinese epilogue_mi_c8171111:


    "突然间，几米之外出现了亮光。"


translate chinese epilogue_mi_97874b9d:


    "我靠着墙颤抖着，不过突然意识到那是列娜，她打开了手电，她是从哪里弄来的？"


translate chinese epilogue_mi_9341d7f5:


    my "你是怎么...?"


translate chinese epilogue_mi_70effa3b:


    "列娜一瘸一拐的往这边走来。"


translate chinese epilogue_mi_8f0774a8:


    un "我昨天带的，以防万一..."


translate chinese epilogue_mi_956b67a0:


    "我接过手电筒往四周照一照。"


translate chinese epilogue_mi_1fddf6ec:


    "我们在一个隧道里，墙上布满了电线，天花板上有没开的电灯。"


translate chinese epilogue_mi_64a7a549:


    ma "咱们在哪里?"


translate chinese epilogue_mi_365e331e:


    my "不知道...看起来好像是一个防空洞..."


translate chinese epilogue_mi_cf6427db:


    "我抬头看到了一个吓人的大洞，透过这个洞可以勉强看到上面的旧校舍的走廊。"


translate chinese epilogue_mi_9b78bde9:


    "可能是地板年久失修，于是我们掉了下来。"


translate chinese epilogue_mi_02fb066a:


    "看上去好像我们没办法爬上去的，太高了，没有什么可抓的东西。"


translate chinese epilogue_mi_b5a405a0:


    my "那里是什么呢？往深处走？"


translate chinese epilogue_mi_bbde6bdd:


    "我没有期待着答案。"


translate chinese epilogue_mi_ec980db8:


    ma "我不知道...要看看才知道。"


translate chinese epilogue_mi_2264a05e:


    "我照着前面，试探地走着。"


translate chinese epilogue_mi_bfec5f9c:


    "我摔得鼻青脸肿，但是姑且还能走得了。"


translate chinese epilogue_mi_4ef21e5f:


    un "啊..."


translate chinese epilogue_mi_bc66543c:


    "列娜在后面哭喊了起来。"


translate chinese epilogue_mi_1d810d31_1:


    my "怎么回事?"


translate chinese epilogue_mi_4bad9ddd:


    un "我好像扭伤脚踝了..."


translate chinese epilogue_mi_d2070307:


    "我真的很期待那种“别管我，你们先走”之类的台词，但是显然没什么希望。"


translate chinese epilogue_mi_7f15c298:


    my "好吧，靠在我身上。"


translate chinese epilogue_mi_b462a513:


    "我走向列娜让她靠在我的肩膀上。"


translate chinese epilogue_mi_2d5feb7b:


    "很快石墙变成了木墙，我们好像来到了一个矿井之中。"


translate chinese epilogue_mi_ff0e72ed:


    ma "咱们休息一下吧!"


translate chinese epilogue_mi_347e63c2:


    "玛莎求着我。"


translate chinese epilogue_mi_fd1bdbfc:


    "我小心的放下列娜然后坐了下来。"


translate chinese epilogue_mi_b1f368d2:


    my "嗯，至少那些怪物不见了..."


translate chinese epilogue_mi_710a57f3:


    ma "但是能保持多久呢...?"


translate chinese epilogue_mi_6450d358_1:


    "我叹了口气，闭上眼睛。"


translate chinese epilogue_mi_b587d808:


    ma "你觉得会发生什么?"


translate chinese epilogue_mi_49190220:


    "玛莎的声音在颤抖，但是她还是充满自信。"


translate chinese epilogue_mi_7bfb3264:


    "似乎她已经在疲倦中耗尽了所有的力气，没法再害怕了。"


translate chinese epilogue_mi_576daca7:


    my "我不知道...而且说实话，我也不想知道!"


translate chinese epilogue_mi_18e5c515:


    ma "但是...咱们...要出去吗?"


translate chinese epilogue_mi_6731092c:


    my "当然!"


translate chinese epilogue_mi_0ffb9fba:


    "我看不见，但是我知道她在笑着。"


translate chinese epilogue_mi_e5153f6b:


    "突然手电筒的灯光灭掉了。"


translate chinese epilogue_mi_1c74b1ee:


    "我不停的拍打着，终于矿井的墙壁再次被照亮，然后我马上发现有什么事情不太对劲！"


translate chinese epilogue_mi_df6b624b:


    "列娜不见了!"


translate chinese epilogue_mi_6ec38792:


    my "列娜消失了..."


translate chinese epilogue_mi_9db6b90d:


    "我悄悄说。"


translate chinese epilogue_mi_20f54ffc_4:


    ma "什么?"


translate chinese epilogue_mi_cfb3fc4f:


    "玛莎拽着我的袖口，仔细盯着列娜刚刚坐过的地方。"


translate chinese epilogue_mi_a4a54acd:


    ma "怎么回事？究竟怎么回事？"


translate chinese epilogue_mi_72683e3e:


    "她又开始哭起来。"


translate chinese epilogue_mi_83e118dc:


    ma "咱们回死掉的...咱们真的会死掉的！"


translate chinese epilogue_mi_6fca0341:


    "我也十分想哭。"


translate chinese epilogue_mi_0b13bed1:


    "黑暗逐渐开始包裹住我们。"


translate chinese epilogue_mi_784f3191:


    "手电的灯光开始逐渐消失，随之而去的还有我们幸存的希望。"


translate chinese epilogue_mi_57aee98a:


    my "起来吧，该走了，电池很快就要用光了。"


translate chinese epilogue_mi_ed94e63d:


    "几分钟以后，我们来到了一个岔路口。"


translate chinese epilogue_mi_dd3ac820:


    my "这什么玩意儿，难道是一个大迷宫?"


translate chinese epilogue_mi_2eb0d079:


    "我暗自骂着。"


translate chinese epilogue_mi_0238f96f:


    ma "也许咱们应该回去了?"


translate chinese epilogue_mi_a595d43b:


    my "不行，那可不行。"


translate chinese epilogue_mi_a3a11b35:


    "玛莎更加用力的拉着我。"


translate chinese epilogue_mi_4ea6e0a7:


    ma "那去哪里?"


translate chinese epilogue_mi_326ad2f9_2:


    my "我不知道..."


translate chinese epilogue_mi_f7db3185:


    "但是我们要做出选择。"


translate chinese epilogue_mi_d59a939e:


    "我决定往右走。"


translate chinese epilogue_mi_ab9caf40:


    "很快我们又来到了一个岔路口。"


translate chinese epilogue_mi_5dea2762:


    ma "这次去哪儿呢?"


translate chinese epilogue_mi_7a1ec301:


    my "咱们保持同样的选择吧..."


translate chinese epilogue_mi_3507d86b:


    "我们走了好久，已经没有力气继续了。"


translate chinese epilogue_mi_5afc66a6:


    "我非常渴，只想舔舔墙上的水珠。"


translate chinese epilogue_mi_9e241c26:


    ma "咱们...坐下来...休息一会儿..."


translate chinese epilogue_mi_367b10bc:


    my "不行，咱们必须继续走!"


translate chinese epilogue_mi_493613af:


    ma "但是只要一会儿..."


translate chinese epilogue_mi_84b04f60:


    "玛莎痛苦的央求着我。"


translate chinese epilogue_mi_e9e6e7f9_2:


    my "好吧..."


translate chinese epilogue_mi_54517287:


    "她低下头，搭着我的肩膀。"


translate chinese epilogue_mi_3c3d66ba:


    "手电闪动的越来越频繁了，我决定把它先关闭来节约电能。"


translate chinese epilogue_mi_58343c59_1:


    ma "就这样了..."


translate chinese epilogue_mi_ce617998_3:


    "..."


translate chinese epilogue_mi_2ffc89fb:


    "我不知道过了多久。"


translate chinese epilogue_mi_245bdc9c:


    "我发疯了一样想要睡觉。"


translate chinese epilogue_mi_fd42b024:


    "我们是怎么相见的..."


translate chinese epilogue_mi_29183e7d:


    "那是很久以前了..."


translate chinese epilogue_mi_1f88cd59:


    my "你还记得吗...?"


translate chinese epilogue_mi_0be4039d:


    "我悄悄问。"


translate chinese epilogue_mi_b6f57941:


    ma "什么...?"


translate chinese epilogue_mi_059b8596:


    "玛莎的声音有点颤抖。"


translate chinese epilogue_mi_d00cf9c5:


    my "咱们是怎么相见的..."


translate chinese epilogue_mi_bfd44c3c:


    ma "嗯..."


translate chinese epilogue_mi_4d08847b:


    "她想要笑出来，但是开始咳嗽。"


translate chinese epilogue_mi_c09c2b0c:


    ma "我想要睡一会儿，可以吗?"


translate chinese epilogue_mi_db20ce9f:


    my "只有一会儿，不然你又要睡过去一整天了..."


translate chinese epilogue_mi_8229d523:


    ma "但是，你会叫我起来吗...?"


translate chinese epilogue_mi_5e3404c1:


    my "当然..."


translate chinese epilogue_mi_2f0abbfd:


    "我吻了她一下，闭上了眼睛。"


translate chinese epilogue_mi_20a50466:


    "混沌离我越来越近但是我不在乎，我回到那个时候..."


translate chinese epilogue_mi_a20cefa7_36:


    "..."


translate chinese epilogue_mi_a20cefa7_37:


    "..."


translate chinese epilogue_mi_dc909087:


    "我浑身疼的要命..."


translate chinese epilogue_mi_ccf84f7d:


    "我的大脑也不能平静的休息，因为的神经末梢传递着无数的信号。"


translate chinese epilogue_mi_ca2bcd31:


    "我坐起来，叫醒了玛莎。"


translate chinese epilogue_mi_20c2627d:


    "不管怎么说，我们打算活下来..."


translate chinese epilogue_mi_d737a002:


    "我在这个矿井里多长时间了...?"


translate chinese epilogue_mi_48f216c9:


    "一个又一个岔路，一条又一条隧道。"


translate chinese epilogue_mi_2c9bce0d:


    "玛莎一直在低声说着什么。"


translate chinese epilogue_mi_c8ad0429:


    "向左...向右...向左...向右..."


translate chinese epilogue_mi_d9534268:


    "我好像再也不能征服下一个路口了..."


translate chinese epilogue_mi_b8aa830c:


    "不过接下来灯光照亮了一条小路，我们来到了一个开放的房间。"


translate chinese epilogue_mi_853bf27c:


    ma "看!"


translate chinese epilogue_mi_8060d8b7:


    "玛莎指向黑暗中的什么东西。"


translate chinese epilogue_mi_c367b132:


    "我照亮那里，发现了一条红领巾。"


translate chinese epilogue_mi_a8f16511:


    ma "这不是..."


translate chinese epilogue_mi_6d40bd4c:


    my "不！快走！"


translate chinese epilogue_mi_cd9d1345:


    "最后我们离开了迷宫，来到了一个小房间。"


translate chinese epilogue_mi_6fffd30f:


    "地上满是烟头和空瓶子，墙上有各种涂写的痕迹。"


translate chinese epilogue_mi_c51d3130:


    my "至少景象有点变化。"


translate chinese epilogue_mi_9da1463d:


    "我叹叹气。"


translate chinese epilogue_mi_a2d6a840:


    "手电快要坚持不住了，我得每隔10秒钟就摇一摇，来保持电量。"


translate chinese epilogue_mi_ccc4b1b1:


    "玛莎无力的倒在地上。"


translate chinese epilogue_mi_7ddfed17:


    my "咱们不能停下！我确定附近有出口！"


translate chinese epilogue_mi_e98886ba:


    ma "你怎么知道...?"


translate chinese epilogue_mi_3a8d7079:


    "我看着她。"


translate chinese epilogue_mi_36c6417c:


    "她显然一点力气也没有了—玛莎在挣扎着，她随时都有可能崩溃。"


translate chinese epilogue_mi_b87232f7:


    my "我就是知道。"


translate chinese epilogue_mi_046fd47c:


    "我笑笑，试图鼓励她。"


translate chinese epilogue_mi_fdd1561c:


    ma "嗯...那么..."


translate chinese epilogue_mi_2d188ff6:


    "她靠着墙，费力的站起来。"


translate chinese epilogue_mi_d86bb40d:


    my "在这里坐一会儿，我去前面看看..."


translate chinese epilogue_mi_d2de1f1d:


    ma "我害怕..."


translate chinese epilogue_mi_811b3088:


    "玛莎颤抖着。"


translate chinese epilogue_mi_376a198b:


    my "我不会走远的!"


translate chinese epilogue_mi_600c1cfd:


    "她充满希望的抬头看着我，几乎察觉不出的点了点头。"


translate chinese epilogue_mi_e3edc783:


    "我看看四周，发现了一道铁门。"


translate chinese epilogue_mi_44b1af16:


    "我试着打开它，但是就是白费功夫，它已经完全锈住了。"


translate chinese epilogue_mi_c3fa1cc1:


    "呃，要是那根管子在这里多好..."


translate chinese epilogue_mi_2f004e19:


    "但是没有别的选择了!"


translate chinese epilogue_mi_a331b385:


    "我顶住墙，用上全身的力量压动把手。{w}我的肌肉暴起，满头大汗，眼前发黑..."


translate chinese epilogue_mi_d24acfc5:


    "但是大门没有移动，只是发出了一阵低沉的轰鸣。"


translate chinese epilogue_mi_3fd97b24:


    my "没关系，等一下..."


translate chinese epilogue_mi_e0d77cc3:


    "我喘了口气，对玛莎喊着。"


translate chinese epilogue_mi_b203e2e6:


    my "我得找一根棍子。"


translate chinese epilogue_mi_cbec9827:


    "我跑向玛莎，弯下身来对她说。"


translate chinese epilogue_mi_f977f9ea:


    my "门后面一定有出口!"


translate chinese epilogue_mi_ad4d86a9:


    ma "呜..."


translate chinese epilogue_mi_21d01a89:


    "她有气无力的看着我。"


translate chinese epilogue_mi_c0cfd78b:


    "我跑出房间，到隧道里寻找有没有适合的东西。"


translate chinese epilogue_mi_55ef4138:


    "手电筒要坚持不住了，它最后闪烁了一下，然后终于灭掉了..."


translate chinese epilogue_mi_f94d2e95:


    "我慌张的寻找回去的路。"


translate chinese epilogue_mi_e071baa4:


    "然而我不断的撞墙。"


translate chinese epilogue_mi_c391fe01:


    my "玛莎！玛莎!"


translate chinese epilogue_mi_80d8e32e:


    "我呼喊着，希望她能听到我的声音。"


translate chinese epilogue_mi_1b079b4b:


    "突然我的背后有动静，一道光射过来，在地上映出我的影子，向无穷的黑暗中延伸。"


translate chinese epilogue_mi_03cecdb6:


    "我被吓得钉在了原地。"


translate chinese epilogue_mi_67aec98b:


    "我听到了一个好像很熟悉的声音。"


translate chinese epilogue_mi_2e95fbdc:


    un "怎么了，谢苗，迷路了?"


translate chinese epilogue_mi_b5c21e08:


    "我慢慢的转过身，看到列娜拿着一束火把。"


translate chinese epilogue_mi_19ff5743:


    "她的脸上带着邪恶的非人的笑容。"


translate chinese epilogue_mi_d7da412d:


    un "我来找你了!"


translate chinese epilogue_mi_c256bf58:


    "这是我昨天晚上在广场见到的列娜。"


translate chinese epilogue_mi_2f54a629:


    "在奥尔加·德米特里耶夫娜的房间里见到的那个列娜。"


translate chinese epilogue_mi_48cecf56:


    "是列娜，同时，也不是列娜..."


translate chinese epilogue_mi_363ad07d:


    "这个动物更像是“乌里扬娜”..."


translate chinese epilogue_mi_cdd889d8:


    un "我来找你了!"


translate chinese epilogue_mi_9148c42c:


    my "你... 你... 你是谁?"


translate chinese epilogue_mi_28b5ea24:


    "我挤出一句话来。"


translate chinese epilogue_mi_50e370da:


    un "是列娜啊！你不认识我了吗?"


translate chinese epilogue_mi_a0df0b59:


    "她又爆发出撒旦的笑声。"


translate chinese epilogue_mi_32ddb218:


    un "你和她们享乐的时候忘记我了吗?"


translate chinese epilogue_mi_84b9a0ea:


    my "我...我..."


translate chinese epilogue_mi_cfc352ff:


    "我的话卡在嗓子里。"


translate chinese epilogue_mi_3198ebd7:


    un "没关系！现在没有别人了！只有你我!"


translate chinese epilogue_mi_47c53372:


    my "所以说...这都是你干的...?"


translate chinese epilogue_mi_ef814bbe:


    un "Bingo！当然不是所有的事情..."


translate chinese epilogue_mi_1346f417:


    "她脸上一度浮现出愧疚，但是有咧嘴笑了起来。"


translate chinese epilogue_mi_ba906862:


    un "斯拉维娅是第一个！她太烦人了！她总是可靠小姐，勤奋，刻苦，哪里都有她，谁都同意，恶心！我撕掉她的手臂的时候怎么哭了！"


translate chinese epilogue_mi_c255c3bb:


    "她的嘴开始颤抖，然后不停的流着口水，还翻着白眼。"


translate chinese epilogue_mi_a183bd23:


    un "然后是阿丽夏，“一个个烦死了，我不需要任何人”现在她自己平静的挂在树上。当然把她挂到上面去不太容易，但是效果很好，听听你们的惨叫！像是杀猪！"


translate chinese epilogue_mi_34cb066a:


    "列娜脸上充满病态的淫笑。"


translate chinese epilogue_mi_16487ed9:


    un "好了，只要再去解决了未来那个小婊子，就结束了!"


translate chinese epilogue_mi_4879df12:


    "她从背后掏出来一把沾满血的刀。"


translate chinese epilogue_mi_4fd3d6cb:


    un "那个，实际上..."


translate chinese epilogue_mi_604a0a8b:


    "看起来了她已经发疯了。."


translate chinese epilogue_mi_3c2cd797:


    un "我不知道乌里扬娜那是怎么回事...她还有其他的少先队员不是我干掉的，但是那个现在真的重要吗？"


translate chinese epilogue_mi_9daeafd9:


    "她摇一摇火把，然后火光瞬间就消失了。"


translate chinese epilogue_mi_7e9959b4:


    un "等一会儿，我马上回来!"


translate chinese epilogue_mi_006d380a:


    "她疯狂的笑声从背后传来。"


translate chinese epilogue_mi_eb5af906:


    "就是说她去找玛莎了!"


translate chinese epilogue_mi_6bbab6e5:


    "我必须马上行动!"


translate chinese epilogue_mi_64fdda0c:


    "但是我完全不知道该往哪里走，黑暗让我完全迷失了方向。"


translate chinese epilogue_mi_b632416a:


    "见鬼!"


translate chinese epilogue_mi_c66c511c:


    "我沿着自己设想的列娜的路线走着。"


translate chinese epilogue_mi_0eae989f:


    "我走了几步就撞到墙上，我试图慢一点走，结果绊倒在什么东西上，摔倒疼的我大叫。"


translate chinese epilogue_mi_999e1188:


    "我试图站起来，发现好像扭到了左脚..."


translate chinese epilogue_mi_4b8b6c31:


    "我想要爬行，但是效果不怎么样，矿井的地上都是各种尖尖的石头，生锈的铁轨还有破碎的木板。"


translate chinese epilogue_mi_a3fa3479:


    "只走出一小段距离，我的胳膊已经血肉模糊，而且尘土让我睁不开眼睛。"


translate chinese epilogue_mi_2d13ba42:


    "我从口袋里摸出火柴。"


translate chinese epilogue_mi_47fd5c48:


    "我想要划着一根火柴，但是总是不成功。"


translate chinese epilogue_mi_10e3d62a:


    my "冷静...冷静..."


translate chinese epilogue_mi_d09690e2:


    "我念叨着。"


translate chinese epilogue_mi_0187584b:


    "最后在我尝试了五次以后隧道终于被这点微弱的火光照亮了。"


translate chinese epilogue_mi_58360f3f:


    "我马上寻找回去的路。"


translate chinese epilogue_mi_fc8120bd:


    "结果没有过几秒钟我就回到了玛莎呆的房间。"


translate chinese epilogue_mi_8c229ba9:


    "最后一根火柴用掉了，但是我正好看到列娜，她举起砍刀向玛莎砍去。"


translate chinese epilogue_mi_9c2f56ee:


    "我大喊着，冲过去，扑在她们身上。"


translate chinese epilogue_mi_deada465:


    "我的手撞倒了什么东西，我落到地上，开始疯狂的砸烂周围的各种东西。"


translate chinese epilogue_mi_67c9b16b:


    "我的攻击大部分都没有命中，但是当我打到一个软的东西以后，我听到了一声呻吟然后是死一般的寂静。"


translate chinese epilogue_mi_3be834f5:


    "我翻过身，粗重的呼吸着，贪婪的吸入干干的空气。"


translate chinese epilogue_mi_22b173a8:


    my "玛莎?"


translate chinese epilogue_mi_805bb83c:


    "我喊着。"


translate chinese epilogue_mi_a4178908:


    "我听到几声啜泣。"


translate chinese epilogue_mi_0019cb32:


    my "你还活着吗?"


translate chinese epilogue_mi_268d1f74:


    ma "嗯..."


translate chinese epilogue_mi_e3a26dcc:


    "她悄悄说。"


translate chinese epilogue_mi_d1ec164c:


    my "她碰到你了吗?"


translate chinese epilogue_mi_ab472d5e:


    ma "没..."


translate chinese epilogue_mi_055645cd:


    "我开始在黑暗中寻找，很快找到了一束火把还有打火机。"


translate chinese epilogue_mi_3401954a:


    "无规律的影子开始在墙上跳动，我看到列娜瘫在地上，她好像还活着。"


translate chinese epilogue_mi_3aa51417:


    "我准备结果了她。"


translate chinese epilogue_mi_e3f3c64d:


    ma "别..."


translate chinese epilogue_mi_f72c4a89:


    "玛莎爬过来拉住我的腿。"


translate chinese epilogue_mi_be4dbf99:


    my "但是我们不能就这样..."


translate chinese epilogue_mi_e3f3c64d_1:


    ma "不必了..."


translate chinese epilogue_mi_71ab98c7:


    "当然不能都怪列娜..."


translate chinese epilogue_mi_9218c96e:


    "但是她就是一个魔鬼!"


translate chinese epilogue_mi_e8f60892:


    "我不能让这样一个人（不知道还能不能叫做人）活着。"


translate chinese epilogue_mi_2046e79a:


    "但是当我看到玛莎的时候我知道我不能这样做...就是不能..."


translate chinese epilogue_mi_1c94cf88:


    "我捡起砍刀，在手上扭曲着，我觉得可以做一个很好的棍子。"


translate chinese epilogue_mi_56ceedde:


    "我走到门边，塞到轮子上，然后用最大的力量拉动。"


translate chinese epilogue_mi_6904897c:


    "大门伴随着一声脆响打开了，新鲜的空气涌了进来。"


translate chinese epilogue_mi_92b62876:


    my "我们得救了..."


translate chinese epilogue_mi_44e2cc8f:


    "我回去找玛莎，我们搀扶着走出了房间，扔下列娜一个人昏迷着躺在那里。"


translate chinese epilogue_mi_800c5a39:


    "前面是一条走廊，尽头有梯子通往上方的盖子。"


translate chinese epilogue_mi_c6d7deb7:


    "我爬上去，用全身的力气拉动。"


translate chinese epilogue_mi_379678b6:


    "盖子突然间砸了下来，我们得以爬出。"


translate chinese epilogue_mi_790139a0:


    "我精疲力尽的倒在草坪上，看着周围。"


translate chinese epilogue_mi_b402b18c:


    "还是这个广场，还是这个营地..."


translate chinese epilogue_mi_144666f0:


    "东方红色的黎明，点燃了遥远的森林。"


translate chinese epilogue_mi_698a2bad:


    "最后的星星还在向我眨眼，它们完全不知道这个地狱里发生了什么。"


translate chinese epilogue_mi_9f24a4f9:


    "玛莎躺在我的肩膀上，用颤抖的双手指向天空。"


translate chinese epilogue_mi_58999707:


    ma "希望我能在那里..."


translate chinese epilogue_mi_a7b77934:


    "虽然我们离开了那个地牢，但是还远没有得救，营地里僵尸乌里扬娜在四处游荡，发疯的列娜躺在地底下，而且谁知道我们在这里还会遇到什么事情。"


translate chinese epilogue_mi_26d8a756:


    ma "我真的很想睡觉..."


translate chinese epilogue_mi_3b7315ab:


    "我不知道该怎么回答。"


translate chinese epilogue_mi_85ad9044:


    "因为我不知道我们应该往哪里跑，我不知道哪里是安全的。"


translate chinese epilogue_mi_d036f439:


    "不知道如何返回我们的世界..."


translate chinese epilogue_mi_7956f114:


    my "睡吧..."


translate chinese epilogue_mi_bc298dba:


    "我轻轻拍着玛莎的头。"


translate chinese epilogue_mi_c4c2efec:


    ma "我说，这个世界真是个很好的电影素材啊!"


translate chinese epilogue_mi_33b4df45:


    "她笑出了声。"


translate chinese epilogue_mi_5f9f4f73:


    "我惊恐的看着她。"


translate chinese epilogue_mi_887f26c3:


    "玛莎在微笑着。"


translate chinese epilogue_mi_bc65e8e5:


    "但是不是列娜那样的邪恶的微笑，是纯洁，诚实，也许有些孩子气的微笑。"


translate chinese epilogue_mi_9de01d70:


    "她所有的疲倦都消失了，眼睛里闪烁着光芒。"


translate chinese epilogue_mi_7122db66:


    my "你...你怎么了?"


translate chinese epilogue_mi_70580914:


    "我几乎说不出话来。"


translate chinese epilogue_mi_8ec442e1:


    ma "一切都很好!"


translate chinese epilogue_mi_bd43e6c0:


    "她又躺在我的肩上。"


translate chinese epilogue_mi_9d008617:


    ma "那是最后一幕!"


translate chinese epilogue_mi_35287c9d:


    "我感觉自己的力量在逐渐消失。"


translate chinese epilogue_mi_23661344:


    "我的视线逐渐变暗，我的意识飘向远方。"


translate chinese epilogue_mi_f491b17d:


    "我静静的躺着，只能听到玛莎的呼吸。"


translate chinese epilogue_mi_b2f531df:


    "没有蚱蜢的鸣叫—好像今天都死光了一样，只有玛莎平静的呼吸。"


translate chinese epilogue_mi_99fe2e9b:


    "还有她的微笑..."


translate chinese epilogue_mi_a20cefa7_38:


    "..."


translate chinese epilogue_mi_a20cefa7_39:


    "..."


translate chinese epilogue_mi_7ffeaac0:


    "我挣扎着睁开眼睛。"


translate chinese epilogue_mi_9af7bec5:


    "水中倒映着美丽的月光，在波浪中不断跳动，晚风轻轻的吹来，还有远处隐约的猫头鹰的叫声。"


translate chinese epilogue_mi_ea099931:


    "我看着熟睡的玛莎，盖着我的衬衣安静的睡着。"


translate chinese epilogue_mi_654b8602:


    "这个梦啊..."


translate chinese epilogue_mi_38e7da2f:


    "我的手机显示着时间——三点!"


translate chinese epilogue_mi_846c7ecb:


    "我们最好回去了，不然奥尔加·德米特里耶夫娜又会让我们..."


translate chinese epilogue_mi_d3fe6684:


    "我刚刚决定叫玛莎起来，但是注意到她旁边的剧本还有钢笔。"


translate chinese epilogue_mi_6c272b09:


    "我翻了几页，感到惊恐——这就是刚刚我的梦中发生的事情！"


translate chinese epilogue_mi_bbc14d5c:


    "我想要呕吐，眼前一黑。"


translate chinese epilogue_mi_fb974fc0:


    "玛莎翻过身，睁开眼看着我。"


translate chinese epilogue_mi_5c118b9c:


    ma "你醒了?"


translate chinese epilogue_mi_aa1fbd9c:


    my "这个...是你写的嘛?"


translate chinese epilogue_mi_ef225471:


    "我磕磕巴巴的说着。"


translate chinese epilogue_mi_41585d2c:


    ma "是啊，有什么问题吗?"


translate chinese epilogue_mi_9ad49ae0:


    "我不知道怎么回答，我就是不知道这是怎么回事。"


translate chinese epilogue_mi_efefdbbe:


    ma "你睡着的样子好可爱，我不想叫你起来，而蚱蜢的鸣叫声很烦人，所以..."


translate chinese epilogue_mi_d1046a49:


    "她笑了。"


translate chinese epilogue_mi_eb681808:


    my "等一下，咱们在这里？也就是说，咱们现在不是少先队员，是在拍电影什么的，时不时?"


translate chinese epilogue_mi_fed3ba60:


    ma "是啊，当然了。"


translate chinese epilogue_mi_d6404691:


    "玛莎不解的问着我。"


translate chinese epilogue_mi_75cc25f6:


    "也许是我醒过来读了她写的内容然后又睡着了...?"


translate chinese epilogue_mi_0e0ceee1:


    ma "你出了一身汗!"


translate chinese epilogue_mi_4b123fd7:


    "她翻着少先队员制服的口袋，找出一条手帕。{w}典型的俄国式手帕，还绣着彩花。"


translate chinese epilogue_mi_a7d1b840:


    "我接过来，心里感到一阵温暖，就是自己亲爱的人送给自己礼物时候的那种温暖。"


translate chinese epilogue_mi_f60fe5df_1:


    ma "咱们可以走了吗?"


translate chinese epilogue_mi_99462d82:


    "玛莎开始穿衣服。"


translate chinese epilogue_mi_665516ab:


    my "嗯...走吧..."


translate chinese epilogue_mi_839949ee:


    "我费劲的站起来，往河水那边走去。"


translate chinese epilogue_mi_fd5cfac0:


    "冷冷的河水让我活了过来，那个梦不再显得那么真实了。"


translate chinese epilogue_mi_2c25b971:


    "不管怎么说，那就是一个梦啊，不是吗?"


translate chinese epilogue_mi_9dd5fe3e:


    "我们回去的路上，玛莎拉着我的手，没完没了的说着各种各样的事情。"


translate chinese epilogue_mi_b3e9120f:


    "我们来到广场。"


translate chinese epilogue_mi_0ef867d4:


    ma "我希望这些生物至少今天晚上可以让我睡好..."


translate chinese epilogue_mi_e59f817f:


    "她若有所思的说着。"


translate chinese epilogue_mi_15c31469:


    my "谁?"


translate chinese epilogue_mi_914e5307:


    ma "那些蚱蜢!"


translate chinese epilogue_mi_fbeb45b9:


    my "啊...是啊，那样就好了..."


translate chinese epilogue_mi_8425d0a6:


    ma "好吧，我要走了..."


translate chinese epilogue_mi_2d38f6b9:


    "她热情的看着我，转过去准备离开。"


translate chinese epilogue_mi_77ececca:


    my "等等!"


translate chinese epilogue_mi_15f28b4d:


    "我拉住她的手，把她拉过来，用力的亲吻着她。"


translate chinese epilogue_mi_3d50e8ce:


    "因为有的时候，梦就是梦..."


translate chinese epilogue_mi_d792d7ba:


    "而现实就是现实..."
# Decompiled by unrpyc: https://github.com/CensoredUsername/unrpyc
