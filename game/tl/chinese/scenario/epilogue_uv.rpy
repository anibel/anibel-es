
translate chinese epilogue_uv_7e594d31:


    "很有趣，看起来很普通的事情也可以影响到一个人。"


translate chinese epilogue_uv_07e4edc8:


    "比如说儿童有很多患上OCD（强迫症）的，在走路的时候要准确的踩在格子里。"


translate chinese epilogue_uv_466ed2a9:


    "这好像没什么，但是如果不能很快好起来，就会出大问题，比如说一个人只能低着头走路，然后被车撞到…"


translate chinese epilogue_uv_a2969cc6:


    "我没有得过这种病，但是我的生活中经常有很小的事情酿成大祸。"


translate chinese epilogue_uv_da2ea9fd:


    "真的，在时间和空间的边缘的一个少先队夏令营里碰到奇怪的猫耳娘有什么了？...?"


translate chinese epilogue_uv_a337cb21:


    "不像是什么很大的事情。"


translate chinese epilogue_uv_9c59ff6c:


    "虽然说我还觉的整个世界就只有这个夏令营了，在这个礼拜以前根本没有我自己。"


translate chinese epilogue_uv_9a051ead:


    "所以一个猫耳娘是这个世界的标志性要素。"


translate chinese epilogue_uv_a79e039c:


    "她只是又一个角色，不是好的，也不是坏的。"


translate chinese epilogue_uv_9dda5d32:


    "但是在我的灵魂深处我知道不是那样的。"


translate chinese epilogue_uv_389909c0:


    "营地里所有人看上去都很正常的，有时候有点太正常了，没有任何可圈可点之处。"


translate chinese epilogue_uv_f8f578c5:


    "天啦，你都可以说这里的生活很无聊。"


translate chinese epilogue_uv_581326c5:


    "但是现在..."


translate chinese epilogue_uv_2486bb90:


    "我的头要爆炸了，所以我睁开眼睛跳了起来。"


translate chinese epilogue_uv_33e7bf55:


    "辅导员的房间和平常一样。"


translate chinese epilogue_uv_e0cf3746:


    "窗外有一只小鸟单调的叫个不停，想要让我眼睛里的血管也都跟着她唱起来一样。"


translate chinese epilogue_uv_13ad0098:


    "现在是11点钟。"


translate chinese epilogue_uv_82f332bb:


    "奥尔加·德米特里耶夫娜为什么没有叫我起来，虽然说这已经无所谓了..."


translate chinese epilogue_uv_2677a9c2:


    "反正我睡得够久。"


translate chinese epilogue_uv_10e06630:


    "但是我感觉自己还没有完全恢复。"


translate chinese epilogue_uv_bc948b8d:


    "整个世界好像都在嘲笑我，外面明亮的太阳，微风对茂盛的森林说着悄悄话，这个烦人的鸟又带来一些朋友，简直是想要扰乱我的思想。"


translate chinese epilogue_uv_7c50b983:


    "水池的凉水还是冷得刺骨。"


translate chinese epilogue_uv_bc68213e:


    "我洗完脸发现没有带刷牙的工具…"


translate chinese epilogue_uv_99507a71:


    "奇怪我没有把自己忘了"


translate chinese epilogue_uv_252255bb:


    "一天这样开始实在是糟糕到一定程度了。"


translate chinese epilogue_uv_4e71467e:


    "那个时候我还觉得这是我在夏令营里经历的最糟糕的一天。"


translate chinese epilogue_uv_6808eaed:


    "虽然说也没有多少选择：一百多个小时，都来不及形成真正家的感觉。"


translate chinese epilogue_uv_65142f6d:


    "家...{w}这个词语让我很痛苦。"


translate chinese epilogue_uv_2899cae6:


    "我真的要永远待在这里吗？但是为什么呢？..."


translate chinese epilogue_uv_cb64a07a:


    "我无助的掩面坐在草坪上。"


translate chinese epilogue_uv_0b1b3e6f:


    "突然间我想起了那个女孩儿。{w}我很好奇还会有多少这种神奇的人物?"


translate chinese epilogue_uv_5f2574e3:


    pi "真是一个好天气啊?"


translate chinese epilogue_uv_96275f4a:


    "我都不想抬头。"


translate chinese epilogue_uv_197a972e:


    me "是啊，太好了..."


translate chinese epilogue_uv_e290bdf2:


    "我保持着同一个姿势回答着。"


translate chinese epilogue_uv_94fa158a:


    pi "这么讽刺我干什么?"


translate chinese epilogue_uv_0f3afb25:


    me "讽刺?"


translate chinese epilogue_uv_fd41eafe:


    "我惊讶的抬头，但是附近没有人。"


translate chinese epilogue_uv_f4ec4bfa:


    "是我的幻觉吗？不对，我绝对听见有人说话..."


translate chinese epilogue_uv_e99cb8c8:


    "难道我疯了?"


translate chinese epilogue_uv_0ec9dbdb:


    "那个地下矿井，舒里克，神秘的声音，猫耳娘，还有更多的声音…{w}这里有能确定的事情吗?!"


translate chinese epilogue_uv_c66393c3:


    "我一拳砸在水池上，然后马上疼得跳了起来，一边揉着手一边诅咒着。"


translate chinese epilogue_uv_43ab9e79:


    me "尼玛!"


translate chinese epilogue_uv_ad63e7a6:


    "突然间，有人从草丛里出来，或者说是什么东西..."


translate chinese epilogue_uv_bd8c5549:


    uvp "你好!"


translate chinese epilogue_uv_cf471270:


    "是昨天的女孩儿。"


translate chinese epilogue_uv_401e4c6c:


    me "好..."


translate chinese epilogue_uv_faae32c5:


    "我不知道说什么好。"


translate chinese epilogue_uv_a12cedc0:


    th "我不知道从她身上能期待什么？"


translate chinese epilogue_uv_206da196:


    "她可能是地狱使者，被派过来干掉我的。{w}只是还没有下手..."


translate chinese epilogue_uv_1f57fbc9:


    "猫耳娘看着我。"


translate chinese epilogue_uv_1330e6b3:


    uvp "疼吗?"


translate chinese epilogue_uv_59cc3474:


    "她看着我的手。"


translate chinese epilogue_uv_9513cd87:


    me "嗯..."


translate chinese epilogue_uv_87af141e:


    "我怯生生的回答着。"


translate chinese epilogue_uv_2fea0ff6:


    uvp "怎么了?"


translate chinese epilogue_uv_038a9e0d:


    me "什么怎么了?"


translate chinese epilogue_uv_09987e32:


    uvp "为什么会这样?"


translate chinese epilogue_uv_45f8c00f:


    me "因为我怒了..."


translate chinese epilogue_uv_f6c23508:


    "我完全不知道应该怎么和她谈话。"


translate chinese epilogue_uv_812a95dc:


    "可能一句话，或者一个动作的错误就可能要了我的命。"


translate chinese epilogue_uv_d817b84a:


    uvp "因为什么怒了?"


translate chinese epilogue_uv_550c69d7:


    "女孩儿完全没有露出一点攻击性——她好像只是想要调查清楚发生了什么事情。"


translate chinese epilogue_uv_bd67e015:


    me "对自己愤怒。"


translate chinese epilogue_uv_39c66d97:


    uvp "我明白了。"


translate chinese epilogue_uv_783cf09d:


    "她淡淡的笑了笑。"


translate chinese epilogue_uv_4d984aa7:


    me "听着...{w}我不知道应该怎么说，但是我一定要问问..."


translate chinese epilogue_uv_98d00e80:


    "我停下来重振精神..."


translate chinese epilogue_uv_99970e7f:


    me "你是谁...?"


translate chinese epilogue_uv_72e1758a:


    "女孩儿呆呆地看着我。"


translate chinese epilogue_uv_606596a1:


    uvp "我，我是我，这是显然的嘛!"


translate chinese epilogue_uv_a9a44f6b:


    "她充满自信的回答着。"


translate chinese epilogue_uv_a343b282:


    me "这，这是，但是…{w}那个...我应该怎么说呢?{w}你应该知道，平常很少见到带着猫耳朵的人类，是吧？"


translate chinese epilogue_uv_3cea47dd:


    "猫耳娘仔细的检查了自己的身体零件，然后问道："


translate chinese epilogue_uv_41fa5d91:


    uvp "为什么?"


translate chinese epilogue_uv_7a2286cb:


    "我完全糊涂了。"


translate chinese epilogue_uv_ed40cefe:


    "最开始我还觉得有必要害怕，但是现在她更像是一个好人而不是可怕的怪兽。"


translate chinese epilogue_uv_6ed8834b:


    me "因为在我的世界里..."


translate chinese epilogue_uv_cf33afc2:


    "我确信自己不应该对她隐瞒。"


translate chinese epilogue_uv_60290bff:


    me "在我的世界里，只有童话中才会有这种事情。"


translate chinese epilogue_uv_1911cc51:


    uvp "什么是童话?"


translate chinese epilogue_uv_acab5d31:


    me "那个...{w} 就是现实中不可能发生的那种故事。"


translate chinese epilogue_uv_822aded0:


    uvp "所以我是不存在的吗?"


translate chinese epilogue_uv_63d3671f:


    "女孩儿笑了。"


translate chinese epilogue_uv_c54c6597:


    me "我都不确定“我”是不是存在。"


translate chinese epilogue_uv_f0d6ba43:


    uvp "但是我可以摸到你，所以你是存在的。"


translate chinese epilogue_uv_fd243037:


    "我想想她要摸我，就感觉不舒服。"


translate chinese epilogue_uv_f31233fd:


    me "那个，就说我是存在的吧..."


translate chinese epilogue_uv_d7fa0877:


    "我后退一步。"


translate chinese epilogue_uv_a630f25b:


    uvp "哦，好吧!"


translate chinese epilogue_uv_c453d5a6:


    "女孩儿微笑着，摇动着尾巴。"


translate chinese epilogue_uv_3baee6cb:


    "好尴尬的沉默。"


translate chinese epilogue_uv_fb3cf2d4:


    "我在这个夏令营的时光就像是在梦中。"


translate chinese epilogue_uv_6f42630e:


    "有时候你睡着了然后发现你大脑里发生的各种事情只是自己的想象，然后你只要希望…"


translate chinese epilogue_uv_d5887cda:


    "我过去的五天是这么想的，在我的灵魂深处总是有这种想法。"


translate chinese epilogue_uv_3eed1590:


    "但是似乎一切都是发生在现实之中的。{w}至少我都不能醒来…"


translate chinese epilogue_uv_c4904eec:


    me "我只是想要明白..."


translate chinese epilogue_uv_ad2b8ad1:


    uvp "什么?"


translate chinese epilogue_uv_6842d279:


    "女孩儿还在笑着。"


translate chinese epilogue_uv_913229f6:


    me "我是怎么来的...{w}还有我要怎么离开这里。"


translate chinese epilogue_uv_ab49bee4:


    "突然我听到路边有动静，于是往那边看看。"


translate chinese epilogue_uv_efa22042:


    "斯拉维娅和热妮娅正往水池走去。"


translate chinese epilogue_uv_bd0cee6c:


    me "还有你..."


translate chinese epilogue_uv_8391e4f1:


    "但是女孩儿已经消失了。"


translate chinese epilogue_uv_84dc2808:


    me "你看见了吗，看见了吗?!"


translate chinese epilogue_uv_d436b474:


    "好像我要找一个证人来确认这个生物的存在一样。{w}这样我才能确定自己不是发疯了。"


translate chinese epilogue_uv_e48273b0:


    "真是讽刺—在奇幻世界发疯...{w}虽然说自从来到了这里就已经一只脚踏进疯人院了。"


translate chinese epilogue_uv_fd3163fc:


    "就算我现在醒过来，我也不能完全确定这就是一个梦。"


translate chinese epilogue_uv_bf07f552:


    sl "看见什么?"


translate chinese epilogue_uv_fcc1f8e9:


    "女孩儿们呆呆的看着我。"


translate chinese epilogue_uv_aeb84719:


    me "我旁边的人..."


translate chinese epilogue_uv_4569b623:


    "我都说不出话来了。"


translate chinese epilogue_uv_300fbab5:


    "所以说是幻觉呵?{w}所以我真的..."


translate chinese epilogue_uv_d9eb3a0e:


    mz "你应该去检查一下大脑了，真的!"


translate chinese epilogue_uv_8c01a57f:


    "热妮娅离开了。{w}斯拉维娅抱歉的笑着，然后匆匆跟了上去。"


translate chinese epilogue_uv_bda4e78f:


    "所以这就是那么简单吗...?"


translate chinese epilogue_uv_34121b4d:


    "我现在只有一个愿望—在这个噩梦里还有人相信我不是疯子!"


translate chinese epilogue_uv_965e44b3:


    "这个夏令营还有里面的少先队员都没有昨天那么真实了。"


translate chinese epilogue_uv_cf1121e2:


    "而且一个礼拜以前我还过着一个普通的男子的普通的生活，但是现在..."


translate chinese epilogue_uv_cd153fa8:


    "也许这些事情让我觉得一切都正常有序?{w}就是说，出现在这里很正常。"


translate chinese epilogue_uv_baf7b7ad:


    "现在我最后的一点常识仅仅是因为这个猫耳娘在向大家证明我不是疯子了。"


translate chinese epilogue_uv_2c9d53b3:


    "看，你根本不准再!{w}是我在你的故事里，不是你在我的里面。"


translate chinese epilogue_uv_0ef30d5d:


    "我要爆炸了。"


translate chinese epilogue_uv_9ee7c0ba:


    "如果没有晚饭的信号，我可能要往图书馆跑去，查找脑叶切断术操作指南。"


translate chinese epilogue_uv_9cefc21f:


    "食堂还是和平常一样拥挤，有些事情是永远不会变的。"


translate chinese epilogue_uv_aea05e6b:


    "我第一次以不同的角度看着这些少先队员们。"


translate chinese epilogue_uv_8946754d:


    "的确，为什么事情都这么...正确？"


translate chinese epilogue_uv_aaec030d:


    "好像是有人照着网上的资料设计了这个少先队夏令营，而根本没有亲自来看看。"


translate chinese epilogue_uv_824ea295:


    "就像是一部苏联老电影，严厉的辅导员，模范的少先队员。{w}甚至还有乌里扬娜。{w}甚至还有阿丽夏..."


translate chinese epilogue_uv_9b5ba514:


    "都在合理的范围之内。"


translate chinese epilogue_uv_fd4fc82b:


    "所以那个奇怪的女孩儿到底是哪里来的?"


translate chinese epilogue_uv_8c4c3dff:


    "她好像就是修复员手抖时抹到画上的一笔颜料。"


translate chinese epilogue_uv_a17c46ba:


    "电子小子突然坐在我旁边。"


translate chinese epilogue_uv_1acf0704:


    el "你好!"


translate chinese epilogue_uv_ede75644:


    "他跟我嬉皮笑脸。"


translate chinese epilogue_uv_401e4c6c_1:


    me "好..."


translate chinese epilogue_uv_0f55d42c:


    el "你在思考什么?"


translate chinese epilogue_uv_9553ce6a:


    me "只是"


translate chinese epilogue_uv_af68277f:


    "现在跟别人坦诚的告白肯定没有什么错了..."


translate chinese epilogue_uv_97647879:


    me "你有没有见到有奇怪的人?"


translate chinese epilogue_uv_f20e3f00:


    el "奇怪?"


translate chinese epilogue_uv_ed10791f:


    me "那个{w}不完全是人类。"


translate chinese epilogue_uv_5aa34a60:


    el "没明白。"


translate chinese epilogue_uv_765c7d3f:


    "电子小子笑了。"


translate chinese epilogue_uv_4594de55:


    me "一个有猫耳还有尾巴的女孩儿。"


translate chinese epilogue_uv_8d388b1a:


    "我进行了简短的描述。"


translate chinese epilogue_uv_552282c3:


    el "女孩儿还有猫耳和尾巴..?"


translate chinese epilogue_uv_ba9b1a5b:


    "他开始思考起来。"


translate chinese epilogue_uv_50507c2c:


    el "你是怎么了解她的？"


translate chinese epilogue_uv_4925911e:


    me "就说我见到她了吧。"


translate chinese epilogue_uv_d7d4400d:


    "我们的对话越来越有意思了。"


translate chinese epilogue_uv_ed9cb410:


    el "只是说说还是你真的见到了?"


translate chinese epilogue_uv_9b84c34b:


    me "这个很重要吗?"


translate chinese epilogue_uv_3f421fba:


    "电子小子严肃的看着我。"


translate chinese epilogue_uv_d8483462:


    el "这里有猫娘的传说，她偷吃食物，偷各种小东西，而且躲避人类。"


translate chinese epilogue_uv_b9b6131d:


    me "真的，一个传说?"


translate chinese epilogue_uv_34955a96:


    me "这个女孩儿长什么样?"


translate chinese epilogue_uv_abf7efe2:


    el "我没有亲眼见过，他们说她有猫耳和尾巴，就跟你说的一样。"


translate chinese epilogue_uv_5587f86a:


    me "还有什么?"


translate chinese epilogue_uv_537d8131:


    el "那个，这是个很古老的传说，别的我也不知道了。"


translate chinese epilogue_uv_c93b36d7:


    me "你觉得谁了解的更多一些?"


translate chinese epilogue_uv_f4cf968f:


    el "问问奥尔加·德米特里耶夫娜吧，她已经来过这个夏令营很多次了，甚至她还不是辅导员的时候就来过这里。"


translate chinese epilogue_uv_3118d7bc:


    me "好吧，谢谢。"


translate chinese epilogue_uv_e316c160:


    "我站起来去找辅导员。"


translate chinese epilogue_uv_e6413ca6:


    "我敢打赌她一定是吃完饭后准备午睡。"


translate chinese epilogue_uv_03870281:


    "可能奥尔加·德米特里耶夫娜是这个理想中的夏令营图景中唯一格格不入的元素——她不像是一个模范辅导员。"


translate chinese epilogue_uv_8014570c:


    "我打开门，充满自信的走进去。"


translate chinese epilogue_uv_b95f1f60:


    "的确，她正躺在床上看书。"


translate chinese epilogue_uv_743fc179:


    mt "谢苗...又偷懒了?"


translate chinese epilogue_uv_22f38a3f:


    me "呃，是的，其实我有问题要问你。"


translate chinese epilogue_uv_8822fa3d:


    "她看着我，犹豫了一下。"


translate chinese epilogue_uv_79e62762:


    "可能我并不应该这么直接?"


translate chinese epilogue_uv_c6104e29:


    "我好像突然触发了自己的防火墙，让我说不出猫耳娘的事情。"


translate chinese epilogue_uv_f6bdf29b:


    "但是我很善于绕弯子，顾左右而言他。"


translate chinese epilogue_uv_ba8beeb1:


    "这才仅仅用了四天！虽然我感觉我一辈子都在做这种事情…"


translate chinese epilogue_uv_c5d9d3db:


    me "奥尔加·德米特里耶夫娜，我当时在吃午饭...{w}和电子小子谈话。{w}他告诉我这里有一个猫娘传说。"


translate chinese epilogue_uv_a59cb894:


    me "他还说你可能了解的更多..."


translate chinese epilogue_uv_1c523a1a:


    mt "你要知道这个做什么?"


translate chinese epilogue_uv_a1ac3a05:


    "辅导员直勾勾地盯着我。"


translate chinese epilogue_uv_048f18e0:


    me "只是好奇！我对这种事情很有兴趣!"


translate chinese epilogue_uv_90c72986:


    "我试图笑笑，但是好像没有什么用处。"


translate chinese epilogue_uv_c224c61a:


    mt "我们的夏令营是非常正常的。"


translate chinese epilogue_uv_f5092004:


    "当然了，当然是正常的了!{w} 我可不想遇到你所说的不正常，那肯定都是疯子还有僵尸。"


translate chinese epilogue_uv_02e02e2f:


    "小猫头鹰的噩梦…"


translate chinese epilogue_uv_50ca27d3:


    me "是，我明白...{w}电子小子说你以前来过这个地方。"


translate chinese epilogue_uv_0e31da79:


    mt "对..."


translate chinese epilogue_uv_d4764288:


    "辅导员在思考什么事情。"


translate chinese epilogue_uv_d290e285:


    mt "不过确实是有一个传说。{w}就是说附近的森林里有一个猫娘偷吃食物还偷走一些小东西，她害怕人，如果有人不小心撞见她，她就会逃跑的。"


translate chinese epilogue_uv_388bac12:


    "这跟我早晨见到的完全不一样嘛。"


translate chinese epilogue_uv_633cf645:


    "那个女孩儿更像是观察研究我，想要弄明白什么，不像是害怕。"


translate chinese epilogue_uv_9696cc5e:


    me "那你..."


translate chinese epilogue_uv_4d18c559:


    mt "我见过她吗?"


translate chinese epilogue_uv_bc6747d3:


    "奥尔加·德米特里耶夫娜打断了我。"


translate chinese epilogue_uv_df6f468a:


    mt "唔嗯，我没见过，但是有几次很接近了。{w}房间里瓶子，牛奶，牙粉一类的东西都神秘的消失了。{w}一点痕迹都不留。"


translate chinese epilogue_uv_91589aaf:


    "我已经完全忘了这里的少先队员们其实都不存在，或者只听令于辅导员。"


translate chinese epilogue_uv_1d9fdb2a:


    mt "我很好奇这些东西都跑到哪儿去了。{w}但是结果也没有找到她在这里的线索..."


translate chinese epilogue_uv_b102f359:


    "她笑了笑，好像在提示我对话结束了。"


translate chinese epilogue_uv_26a5c68f:


    me "如果有呢?"


translate chinese epilogue_uv_13679a66:


    mt "你怎么对这个这么感兴趣?"


translate chinese epilogue_uv_149896fd:


    me "为什么不能感兴趣呢?"


translate chinese epilogue_uv_3c4868cf:


    "虽然我还有很多折磨大脑的东西。"


translate chinese epilogue_uv_2c3aefa4:


    mt "我不知道..."


translate chinese epilogue_uv_983cc84e:


    "辅导员又思考了几秒钟，然后又恢复了她的{i}夏令营之母{/i}的状态。"


translate chinese epilogue_uv_4b9bfbfc:


    mt "我说，你好像想完全偷懒!{w}去找点事情做..."


translate chinese epilogue_uv_76e6e018:


    "她停下来伸着懒腰，结果露出了一些她这个年纪完全不应该有的皱纹。"


translate chinese epilogue_uv_6d49ea8f:


    mt "快去...干你的事儿去!"


translate chinese epilogue_uv_31709fb0:


    "我叹叹气，离开了。"


translate chinese epilogue_uv_7ed6de8c:


    "不管怎么说，哪个少先队员都没有真的见过这个猫耳娘，他们都是听到的“传说”。"


translate chinese epilogue_uv_4dbdaf04:


    "可能我应该找到她!"


translate chinese epilogue_uv_2dc25f6f:


    "我已经从今天早晨的震惊之中恢复了过来，这些关于存在的问题已经不再折磨我的大脑了，所以我也差不多可以找一个合适的位置了。"


translate chinese epilogue_uv_d50f0920:


    "但是这很必要因为我还有很多问题：我是怎么来到这里的，为什么，还有这是什么地方?"


translate chinese epilogue_uv_4c7c2569:


    "猫娘就是整个事件的催化剂。"


translate chinese epilogue_uv_72f94960:


    "但是我不想思考什么平行宇宙的复杂理论，或者什么外星人啊，活体实验啊。"


translate chinese epilogue_uv_f5c40cb4:


    "在夏令营里经过了最初的几个小时以后我就烦了。"


translate chinese epilogue_uv_213d5389:


    "可能我应该抛下这些理论，去考虑一下实际的情况。"


translate chinese epilogue_uv_1c978c35:


    "即使我假设这个世界不管我怎么想都继续存在的话，我到达这里也是解释不清的。{w}还不如讨论一下我在这里具体会遇到什么事更靠谱一些。"


translate chinese epilogue_uv_1c050fb2:


    "除非我可以得到有力的证据，不然我只能假设自己就是永远的被困在这里了。"


translate chinese epilogue_uv_385b5708:


    "躺椅随着我的动作发出悦耳的吱呀声，丁香枝划着我的脸，早晨鸟儿烦人的叫声也已经消失不见了，显然是因为夏天太热了。"


translate chinese epilogue_uv_a3843ea8:


    "树顶就像是中世纪的城堡，埋没在无边无际的森林中。"


translate chinese epilogue_uv_6f43b891:


    "这些房间，广场，Genda的雕塑，还有社团活动室都是一些村民的住屋，包围着千年的碉堡，秘密的隐藏在林中的空地。"


translate chinese epilogue_uv_641e9777:


    "然后这里的所有人，跟我一样的人，或者猫娘那样的，只是某个隐藏的天才的仆人..."


translate chinese epilogue_uv_cab4416d:


    "如果营地是在山顶上就好多了。"


translate chinese epilogue_uv_6da72447:


    "因为一千年前堡垒总是建在山顶上。"


translate chinese epilogue_uv_17159f54:


    "可能那个人真的什么也不怕。{w}但是那样的话他又不可能一直这么躲在树丛里。"


translate chinese epilogue_uv_c913f529:


    "我叹叹气，闭上了眼睛。"


translate chinese epilogue_uv_01d78855:


    "我的意识逐渐抛弃了我，很快整个世界陷入了黑暗。"


translate chinese epilogue_uv_ce617998:


    "…"


translate chinese epilogue_uv_1380294c:


    "然后又一次，在我的眼前，像是一张巨大的地毯一样—森林边的公路，还有无边无际的田野。"


translate chinese epilogue_uv_6ebd931c:


    "公共汽车不知道去了哪里，把我认识的所有东西，我热爱的讨厌的全都扔掉了。"


translate chinese epilogue_uv_a25e5688:


    "一个不太熟悉的女孩儿靠着我，跟我低声耳语着什么。"


translate chinese epilogue_uv_aa083268:


    "我对她说的很感兴趣。"


translate chinese epilogue_uv_7e91da95:


    "但是我听不见，我从侧边什么都能看清就是看不清她的脸。"


translate chinese epilogue_uv_a12c5467:


    "什么都能听见，就是听不见她说话。"


translate chinese epilogue_uv_064711df:


    "什么都能感觉到，就除了我自己的人生..."


translate chinese epilogue_uv_e65ed0cf:


    "我醒来了，感觉好像窒息一样，大口的呼吸着空气。"


translate chinese epilogue_uv_c51374b9:


    "我的呼吸恢复了正常，然后又感觉到难以忍受的口渴。"


translate chinese epilogue_uv_9dcbb59a:


    "但是...这都是什么梦？还有那个女孩儿到底是谁？"


translate chinese epilogue_uv_e8dd80d0:


    "又是这些问题..."


translate chinese epilogue_uv_4dfb5dfb:


    "可能我已经死掉了，来到了地狱。"


translate chinese epilogue_uv_934cbd27:


    "一个人可以感觉到自己生命结束了然后前往下一个阶段吗?"


translate chinese epilogue_uv_8908c3e8:


    "思考这种事情我的大脑要爆炸了。"


translate chinese epilogue_uv_f58c6165:


    "我做了什么，为什么这样对我？{w}我只是一个普通人，不是一个哲学家，不是神学家，不是科学家。"


translate chinese epilogue_uv_197dea6a:


    "我也不想搞清楚这宇宙的运行规律什么的。{w}我对生活的要求就是平静的活着，当所有事情都结束以后就放手而去。"


translate chinese epilogue_uv_8d9ab812:


    "但是，我却被卡在了这里!"


translate chinese epilogue_uv_666232c3:


    "我的眼睛里充满了绝望的泪水。"


translate chinese epilogue_uv_451a9255:


    "为什么总是这样?!"


translate chinese epilogue_uv_64cbaa90:


    "我开始觉得如果自己在深入思考一下，可能就真正理解“物质的本质”了，或者是完全发疯..."


translate chinese epilogue_uv_2773c75d:


    "终极原因都是这辆公交车，我走进去睡着了，然后..."


translate chinese epilogue_uv_e44cd0ef:


    "然后有一个女孩儿，可能是这里的少先队员，也可能就是那个猫耳娘!"


translate chinese epilogue_uv_9e1bfd29:


    "嗯，有什么不可以的，我一点原因也排除不掉，所以我现在在这里..."


translate chinese epilogue_uv_b758af67:


    "我的想法停了下来，像风滚草一样不停地跳动着，同时防止耳朵掉下。"


translate chinese epilogue_uv_833a0181:


    "然后...就没了。"


translate chinese epilogue_uv_31afad13:


    "现在我在这里，是地狱，还是天堂，还是炼狱，无所谓了。{w} 看起来我得在这里度过永恒的时间了..."


translate chinese epilogue_uv_d5cd5ce9:


    "不知道为了什么我就看准了这个理论。"


translate chinese epilogue_uv_092fb80e:


    "很奇怪，因为我没有考虑一个精神上的生物没有基本的人类的感知，视觉，听觉，触觉。"


translate chinese epilogue_uv_c0c03087:


    "而且第一眼看上去我和上个礼拜没有什么区别。{w}嗯，除了年轻了几岁以外。"


translate chinese epilogue_uv_2470ab86:


    "那么到底发生了什么悲惨的事情了呢?"


translate chinese epilogue_uv_e3c414c9:


    "我都愿意再去思考一遍这个问题!"


translate chinese epilogue_uv_7d31568f:


    "不管怎么说，找到工作也不太难，我也可以暂时住在临时收容所里..."


translate chinese epilogue_uv_5e133ffb:


    "我现在17岁，我还很健康，我的大脑很健康，我浑身充满了能量。{w}我还想获得什么成就..."


translate chinese epilogue_uv_7c5d0d68:


    "但是到底是什么阻止着我？我原来真的那么老吗？"


translate chinese epilogue_uv_89b8aeef:


    "我想是一支老旧的步枪一样卡壳了，如果没有人帮助我的话，我可能都会陷入这对自我的思考中。"


translate chinese epilogue_uv_f9d80d04:


    "幸运的是，列娜突然出现在了辅导员的房间旁边。"


translate chinese epilogue_uv_bd946546:


    un "你好。"


translate chinese epilogue_uv_dde1f147:


    "我不情愿的抬头看着她。"


translate chinese epilogue_uv_401e4c6c_2:


    me "你好..."


translate chinese epilogue_uv_805ea37c:


    un "那个...我..."


translate chinese epilogue_uv_080288a3:


    "她停下来看着别处，脸红红的。"


translate chinese epilogue_uv_6f6c98f9:


    un "没事，我可以坐在这里吗?"


translate chinese epilogue_uv_66a21e69:


    me "当然"


translate chinese epilogue_uv_d407d0dc:


    "列娜坐在了椅子边缘。"


translate chinese epilogue_uv_2cd0045b:


    un "你在思考什么?"


translate chinese epilogue_uv_62f30bbf:


    me "试图理解世界运行的规律。"


translate chinese epilogue_uv_bb75283f:


    un "进展怎么样?"


translate chinese epilogue_uv_b84d3377:


    "她严肃的问着。"


translate chinese epilogue_uv_73e269e8:


    me "不太好...{w}可能是人类无法理解的。"


translate chinese epilogue_uv_c520fb7c:


    un "那，你可能不应该去想那个?"


translate chinese epilogue_uv_e6abf674:


    me "那我应该怎么办呢？我必须这样做。"


translate chinese epilogue_uv_b672514d:


    un "为什么?"


translate chinese epilogue_uv_2376d290:


    "说真的，为什么?"


translate chinese epilogue_uv_f0d30338:


    "这是什么紧急事件吗?"


translate chinese epilogue_uv_c1b551f0:


    "我以前好像也不怎么在乎那种问题的。"


translate chinese epilogue_uv_fb3cd06a:


    "到底是什么发生了这么戏剧性的变化?{w}我现在也不会去想，而且生活也会发生某种变化，但是这到底有什么问题?"


translate chinese epilogue_uv_1f42124c:


    "如果没有选择的话..."


translate chinese epilogue_uv_8473c25d:


    me "我不知道，好像没有什么是我可以决定的。"


translate chinese epilogue_uv_4ac82be2:


    "列娜好像想说点什么，但是没有说出来。"


translate chinese epilogue_uv_701bd2e2:


    me "你可能是想要找奥尔加·德米特里耶夫娜?"


translate chinese epilogue_uv_f3db2cab:


    un "是啊...{w}不对，{w}那个，说实话..."


translate chinese epilogue_uv_ad02f53a:


    me "怎么了?"


translate chinese epilogue_uv_e3352c88:


    un "有时候我也有这种感觉。"


translate chinese epilogue_uv_cb23542f:


    me "那是什么意思?"


translate chinese epilogue_uv_8c2a3988:


    un "就是这些都不受我自己控制。"


translate chinese epilogue_uv_8f838576:


    "我认真的看着她。"


translate chinese epilogue_uv_dade88e8:


    "她低垂着眼睛，紧张的看着地上的花朵，它们洋溢着太阳一样真诚的黄色。"


translate chinese epilogue_uv_e499d007:


    un "那个，你知道...{w}这些花朵—它们就是这样生长着，然后我折下一朵，然后就完了!"


translate chinese epilogue_uv_845b4bf2:


    "她简直要哭出来了。{w}这不像是我印象中的列娜。"


translate chinese epilogue_uv_a3df48c6:


    me "嗯，是啊...{w}说实话，我真的不知道说什么好。"


translate chinese epilogue_uv_e07cb63d:


    un "嗯，我们也一样，被折断下来，然后..."


translate chinese epilogue_uv_54d3c161:


    "她激动的往广场跑去，连一声再见也没有说。"


translate chinese epilogue_uv_8e47f89e:


    "确实，列娜就像是其他的少先队员一样，好像真的一样。{w}不完全是我的想象，不是一个木偶，不是电脑角色。"


translate chinese epilogue_uv_7a418be8:


    "几个小时以前我还觉得这里真实存在的只有我一个人，{w}可能还有那个猫耳娘。"


translate chinese epilogue_uv_b456282f:


    "希望我能找到她。"


translate chinese epilogue_uv_0bcb04b7:


    "我好像出于某种灵魂缺失状态，我的感觉都在正常运转，但是我还是嫉妒渴望去了解自己为什么在这里。"


translate chinese epilogue_uv_72ae7014:


    "就好像是对身份认同的渴望，找到在这里世界里属于我的位置，决定那些至关重要的东西，就像是基本的食物需求。"


translate chinese epilogue_uv_5c3cb8c8:


    "这可能是对一个人最严酷的惩罚，有理解某种东西的渴望，却没有能力。渴望改变什么，但是没有机会，竭力追求什么却做不到。"


translate chinese epilogue_uv_837013f3:


    "我从来没有严肃的思考过，我为什么存在于这个世界，我能在这个世界上生存多长时间，我必须要做什么，接下来会发生什么。"


translate chinese epilogue_uv_075f5465:


    "现在，在我遇到了另外的这个世界以后，我只能想到这些事情。"


translate chinese epilogue_uv_40e0457e:


    "我觉得如果我可能逃离这里，我一定会回到自己平静的生活。"


translate chinese epilogue_uv_250cbd1f:


    "但是我回不去了..."


translate chinese epilogue_uv_4b055169:


    "面对这样一个不可能完成的任务真的很难。"


translate chinese epilogue_uv_2d594681:


    "如果我可以自己选择自己的命运，我绝对不会坐这辆车，来到这个夏令营，遇到这些女孩儿，我会继续待在自己的公寓里面对着电脑屏幕。"


translate chinese epilogue_uv_fbae2843:


    "你不能要求一些他能力以外的东西。"


translate chinese epilogue_uv_b711afb2:


    "即使是一辈子在矿井里工作也比硬要自己理解那些不可能理解的东西好!"


translate chinese epilogue_uv_e0d958d5:


    "反正没有别的办法。"


translate chinese epilogue_uv_b3b8f58b:


    "当然，也可以勇敢的放弃思考，接受崭新的生活。"


translate chinese epilogue_uv_785611e4:


    "但是这样忽略自己周围的一切可是和人类的本性相违背的。"


translate chinese epilogue_uv_16a336ea:


    "如果我被指控一项莫须有的罪名，我当然会反抗，试图证明自己的清白。"


translate chinese epilogue_uv_37f26571:


    "不管家是什么样子，也没有谁会扔下自己的家跑去坐监狱。"


translate chinese epilogue_uv_90c2fe99:


    "现在我就是被强制的驱离自家送到这个监狱。"


translate chinese epilogue_uv_4c3a0f63:


    "虽然这里没有围墙没有铁栅栏，但是我没有要求这些，我只想离开这个地方回家去。"


translate chinese epilogue_uv_bb5f8ea5:


    "唯一可以离开这里的方法就是把自己吊在刚好跳起来够不着的天花板上。"


translate chinese epilogue_uv_c0218e20:


    "然后附近什么东西也没有，也没有人帮忙。"


translate chinese epilogue_uv_d6449651:


    "只要几厘米，几厘米就可以让我远离这一切..."


translate chinese epilogue_uv_5c3993ed:


    "可能还是回不了家，但是至少我可以离开这个监狱!"


translate chinese epilogue_uv_37c1ea83:


    "这就是我过去的五天里一直渴望的事情..."


translate chinese epilogue_uv_a20cefa7:


    "…"


translate chinese epilogue_uv_9565e04f:


    "夜幕逐渐降临。"


translate chinese epilogue_uv_cac0bbd5:


    "晚饭把我扔下了，而且辅导员有好几次停下来想要跟我说什么。"


translate chinese epilogue_uv_2a186eff:


    "但是我已经在另外一个世界里了—一个牢房中，钥匙绑在用阿里阿德涅的丝线系的戈尔迪厄斯之结上。"


translate chinese epilogue_uv_a308a72b:


    "不一会儿，营地里的灯光都关掉了，蟋蟀们开始悲凉的鸣叫。"


translate chinese epilogue_uv_49f57976:


    "远处猫头鹰以每分钟37次的频率叫着。"


translate chinese epilogue_uv_e5c6354e:


    "我坐下来按照同样的频率敲击着，试图跟上它的节奏。"


translate chinese epilogue_uv_58b687db:


    "突然间附近的草丛有一阵响声，一直松鼠叼着坚果爬上树。{w}它一定是着急回家呢..."


translate chinese epilogue_uv_be1bbc12:


    "夏夜还是像平常一样吸引人，但是一点也没有影响到我。"


translate chinese epilogue_uv_9a927fac:


    "我的大脑设备可能还有我的灵魂都需要严格的保养，不能只是粗略的维修和烤漆。"


translate chinese epilogue_uv_cf53bd0d:


    "我决定去散散步。"


translate chinese epilogue_uv_978811c4:


    "想想发现我已经在这个躺椅上待了一整天了。{w}我都睡了一小觉了..."


translate chinese epilogue_uv_56500c49:


    "营地里的人们都累了，整个营地都在睡觉。"


translate chinese epilogue_uv_b459cef6:


    "在队员的房间附近穿行，经过Genda广场，森林里的小路，这些就像是小孩子的连环画。{w}整个世界就像是一张画。"


translate chinese epilogue_uv_bc0e1e5a:


    "那也不是我昨天想的事情..."


translate chinese epilogue_uv_abe4b874:


    "原因是一个长着动物耳朵的奇怪女孩儿，一个猫娘，虽然也可以说是动物。"


translate chinese epilogue_uv_3e0e6364:


    "好像我来到这里以后有了一种防御机制，让我不去寻找答案，或者了解现在的情况。"


translate chinese epilogue_uv_0b56ea8e:


    "永远都有更重要的事情，比如说错过列队集合，午饭是找到舒服的作为，从辅导员身边溜走..."


translate chinese epilogue_uv_4dfa815d:


    "除了思考我怎么离开的事情，别的什么都行..."


translate chinese epilogue_uv_1e2d6285:


    "我坐在树桩上，闭上眼睛。"


translate chinese epilogue_uv_d3f7944b:


    "我的大脑中出现了一百幅图像，其中有一条道路，一辆公交车，还有那个女孩儿。{w}对，有着动物耳朵的女孩儿。{w}她在公交车里!"


translate chinese epilogue_uv_3d57006a:


    "我马上睁开眼睛发现她就在我面前。"


translate chinese epilogue_uv_30954eab:


    "开始想是一个梦或者幻觉，但是突然间一阵凉风让我重新回到现实。"


translate chinese epilogue_uv_91d91e6b:


    "奇怪的女孩儿坐在那里往蘑菇上撒着糖。"


translate chinese epilogue_uv_51149e68:


    me "…"


translate chinese epilogue_uv_08781329:


    uvp "这样可以长得更好。"


translate chinese epilogue_uv_b9daa03b:


    "她一脸严肃的说着。"


translate chinese epilogue_uv_a444ac12:


    me "谁?"


translate chinese epilogue_uv_70580914:


    "我费劲的说着。"


translate chinese epilogue_uv_d9e00995:


    uvp "当然是蘑菇了，还能有谁!"


translate chinese epilogue_uv_7a3eab5d:


    me "蘑菇...?"


translate chinese epilogue_uv_246a2b06:


    uvp "对呀，蘑菇。"


translate chinese epilogue_uv_2a236b27:


    me "你为什么需要这些蘑菇?"


translate chinese epilogue_uv_de01302f:


    uvp "吃啊！不然要蘑菇干什么!"


translate chinese epilogue_uv_5942e96a:


    "女孩儿撅起嘴。"


translate chinese epilogue_uv_8213e247:


    "我真的不知道怎么和她聊天，因为我确定这个长着猫耳的女孩儿把我带到了这里来，而我和一个高等生物交朋友可能也不会有什么好处。"


translate chinese epilogue_uv_c5d78fa3:


    uvp "你还有糖吗?"


translate chinese epilogue_uv_58838469:


    "她突然问我。"


translate chinese epilogue_uv_941e5272:


    me "我不知道...没有。你要那个做什么?"


translate chinese epilogue_uv_3fe32671:


    uvp "什么做什么！我已经告诉你了嘛！给蘑菇用啊。{w}那样可以长得更好。"


translate chinese epilogue_uv_82ed9336:


    "不管怎么说，这个女孩儿看起来很普通。"


translate chinese epilogue_uv_8261b96b:


    "当然平常你不会见到长着猫耳的人类，但是除了这一点..."


translate chinese epilogue_uv_abb3995c:


    me "我可以拿来一些..."


translate chinese epilogue_uv_b283d28e:


    "我不知道自己为什么这么说，可能只是为了赢得她的信任。"


translate chinese epilogue_uv_b18f4a9e:


    "只要她不善于读心术的话。"


translate chinese epilogue_uv_cb5b23ec:


    uvp "没关系的，已经够用了。"


translate chinese epilogue_uv_65bf82fc:


    me "好"


translate chinese epilogue_uv_b4cd8a03:


    uvp "还有呢?"


translate chinese epilogue_uv_ad02f53a_1:


    me "什么?"


translate chinese epilogue_uv_f81f2044:


    uvp "你为什么来?"


translate chinese epilogue_uv_a91b9930:


    "她这是什么意思？来到这片丛林还是来到这个夏令营还是来到这个世界...?"


translate chinese epilogue_uv_1602edd7:


    "我的大脑中充满了各种奇怪的假设，要爆炸了。"


translate chinese epilogue_uv_7b2d5c2d:


    "只要再多一点，一切就都能清楚了。"


translate chinese epilogue_uv_171177e4:


    "不，答案和解释不会凭空出现，请注意，门要关了，已满员，下一站是..."


translate chinese epilogue_uv_af4e6296:


    "火车离开了，我丢掉了行李呆呆地站在站台上，在零下二十度的寒风中穿着一件薄薄的夹克。"


translate chinese epilogue_uv_49c71752:


    me "我只是散散步!"


translate chinese epilogue_uv_6ea6f839:


    "这些话空空的在我的大脑中回响。"


translate chinese epilogue_uv_0ab3f93c:


    "好像中世纪的海盗，独自面对上百个敌人。{w}没有疑问，没有悔恨，只有视死如归的信念！"


translate chinese epilogue_uv_9c5fc54a:


    uvp "我知道了..."


translate chinese epilogue_uv_d37bab0a:


    "女孩儿心不在焉的回答着，然后又继续开始工作。"


translate chinese epilogue_uv_35d04d84:


    me "是你吗？当时，在车上？"


translate chinese epilogue_uv_7d58e7f3:


    uvp "我..."


translate chinese epilogue_uv_a307d27a:


    "她继续面对着蘑菇，心不在焉的说着。"


translate chinese epilogue_uv_d147d16f:


    me "你为什么把我带到这里来?"


translate chinese epilogue_uv_e03d589f:


    uvp "我没有带你。"


translate chinese epilogue_uv_8b05cf4e:


    me "那是谁?"


translate chinese epilogue_uv_5dcae342:


    uvp "我不知道。"


translate chinese epilogue_uv_7d60f019:


    me "但是你当时在公交车上吗?"


translate chinese epilogue_uv_63ea92fa:


    uvp "在。"


translate chinese epilogue_uv_9bfb0137:


    me "然后呢?"


translate chinese epilogue_uv_ad2b8ad1_1:


    uvp "什么?"


translate chinese epilogue_uv_0225570c:


    "对她喊叫是很危险的，但是我还是没有忍住。"


translate chinese epilogue_uv_afdca9b7:


    me "什么！什么？！快告诉我！"


translate chinese epilogue_uv_0fca1ec3:


    uvp "你问了什么?"


translate chinese epilogue_uv_ea44f8ab:


    "好像她一点也不烦恼。"


translate chinese epilogue_uv_9af412a5:


    me "你是谁?"


translate chinese epilogue_uv_efd76d15:


    "她惊讶的看着我。"


translate chinese epilogue_uv_ec74d78b:


    uvp "我是我!"


translate chinese epilogue_uv_834c0fe5:


    me "好吧...{w}你从哪里来?"


translate chinese epilogue_uv_f09a8f60:


    uvp "我不知道...我一直都在这里。"


translate chinese epilogue_uv_29be4a1c:


    me "什么叫一直?{w}你肯定是从哪里来的啊?"


translate chinese epilogue_uv_4de354e1:


    uvp "我不知道..."


translate chinese epilogue_uv_064c8591:


    "我怀疑了半天面前这个人是不是全知全能的人。"


translate chinese epilogue_uv_d676fe96:


    "这个女孩儿是有点特别，（准确的说就是非常特别）她那时和我一起在公交车里，但是可能她跟我来到这里并没有关系。一切皆有可能..."


translate chinese epilogue_uv_e4a40e96:


    me "你都知道什么?"


translate chinese epilogue_uv_4fd84444:


    uvp "我知道怎样为冬天储存粮食。"


translate chinese epilogue_uv_9d48f5b9:


    "她甜蜜的笑着。"


translate chinese epilogue_uv_3881d52a:


    me "储存什么?"


translate chinese epilogue_uv_0ace2a6c:


    uvp "那个，冬天嘛，很冷的..."


translate chinese epilogue_uv_e5d08bc9:


    uvp "我还没有见到本地的冬天话说。"


translate chinese epilogue_uv_a7966ed3:


    me "如果你一直在这里为什么一直见不到?"


translate chinese epilogue_uv_5dcae342_1:


    uvp "我不知道。"


translate chinese epilogue_uv_1b595140:


    "她认真的看着我。"


translate chinese epilogue_uv_8c4a8a38:


    uvp "不管怎么说！你太无聊了！"


translate chinese epilogue_uv_7d21a175:


    me "我？！"


translate chinese epilogue_uv_a8361046:


    uvp "对啊，你。"


translate chinese epilogue_uv_ef114c48:


    me "我怎么无聊了?"


translate chinese epilogue_uv_1169b08a:


    uvp "你的那些愚蠢的问题。"


translate chinese epilogue_uv_1373dd1c:


    me "与批判...?{w}嗯，我突然从自己的世界被拽到了这里，我的问题还是愚蠢的问题？！"


translate chinese epilogue_uv_d5ed42a0:


    uvp "对。"


translate chinese epilogue_uv_9087bb2a:


    "她平静的回答着。"


translate chinese epilogue_uv_cee893c3:


    me "那你觉得什么问题不愚蠢？"


translate chinese epilogue_uv_22217845:


    uvp "比如说去哪里弄到糖。"


translate chinese epilogue_uv_c71c2028:


    me "在食堂啊，还能是哪儿?!{w}足够你偷的了，据说你好像很擅长那个。"


translate chinese epilogue_uv_b1cb93f2:


    uvp "我不偷东西!"


translate chinese epilogue_uv_624d137a:


    "猫耳娘生气了。"


translate chinese epilogue_uv_5c53c3a4:


    uvp "我是有借有还的!"


translate chinese epilogue_uv_b6908ceb:


    me "有没有还...{w}我也不在乎！我只想知道自己是怎么来到这个地方的还有为什么！我只想回去！"


translate chinese epilogue_uv_c0d6abbb:


    uvp "我对那个不感兴趣。"


translate chinese epilogue_uv_45da52b8:


    "女孩儿站起来抖了抖身上。"


translate chinese epilogue_uv_3ab93971:


    me "但是我很感兴趣！{w}如果这些事情不都怪你，那你一定知道这些事情的答案！"


translate chinese epilogue_uv_6832591f:


    me "总要有人知道的..."


translate chinese epilogue_uv_a4748218:


    "她认真的看着我，然后笑了。"


translate chinese epilogue_uv_310acf5d:


    uvp "你好有趣!"


translate chinese epilogue_uv_32c4ee43:


    "我彻底的发怒了。"


translate chinese epilogue_uv_acb541be:


    me "别笑话我了！你！我受够了！为什么是我？！我就比别人差？我可没想做这些“有意思”的事情，让我一个人冷静一下，我谁也没有打扰！也没有伤害到谁！我对你做什么了？走开吧，让我一个人冷静一下！"


translate chinese epilogue_uv_10514786:


    "我的整个身体颤抖起来，眼前的一切都暗了下去。"


translate chinese epilogue_uv_da5992ef:


    uvp "你说完了吗?"


translate chinese epilogue_uv_1faf32bf:


    "她悄悄地问。"


translate chinese epilogue_uv_59558a54:


    "我想要把她揍扁，看看她是不是也是血肉之躯。"


translate chinese epilogue_uv_bef37e18:


    uvp "好吧，咱们走吧。"


translate chinese epilogue_uv_477fc689:


    me "去哪儿?"


translate chinese epilogue_uv_d1be1fae:


    uvp "你会知道的。"


translate chinese epilogue_uv_e23148b5:


    "我随着她的指引跟上去。"


translate chinese epilogue_uv_ce617998_1:


    "…"


translate chinese epilogue_uv_f63b1b8b:


    "过了一会儿我开始怀疑我们是不是在兜圈子。"


translate chinese epilogue_uv_97a205cc:


    "最后我决定自己检查一下，就在明显的地方放了一块糖纸。"


translate chinese epilogue_uv_a6536d01:


    "结果我们很快又经过了这里。"


translate chinese epilogue_uv_03e5b22a:


    me "等等。"


translate chinese epilogue_uv_9d8a1030:


    "我不敢抓她的手，所以只是用言语叫住她。"


translate chinese epilogue_uv_ad2b8ad1_2:


    uvp "什么?"


translate chinese epilogue_uv_58955638:


    me "你带我去哪儿?"


translate chinese epilogue_uv_77a8b489:


    uvp "我说了你会知道的!"


translate chinese epilogue_uv_5b06e11d:


    me "但是咱们在兜圈子。"


translate chinese epilogue_uv_c379a4f1:


    uvp "那怎么了?"


translate chinese epilogue_uv_20b0f9b4:


    me "什么叫那怎么了？你总是兜圈子怎么可能去别的地方!"


translate chinese epilogue_uv_c5a6ae3e:


    uvp "但是你自己说了只是散散步..."


translate chinese epilogue_uv_ab154d49:


    "这个女孩儿很奇怪，没有逻辑，甚至有点傲慢，好像嘲讽我还很高兴似的。"


translate chinese epilogue_uv_4c94d3cf:


    me "你不跟我说清楚这是怎么回事我一步也不走。"


translate chinese epilogue_uv_d8d14dde:


    uvp "好吧..."


translate chinese epilogue_uv_077824e8:


    "她停了下来，我靠在树上。"


translate chinese epilogue_uv_b3106bae:


    uvp "我就是无聊啊，总是一样的重复来重复去...{w}但是最近发生了很多新鲜的事情，现在..."


translate chinese epilogue_uv_2827a713:


    me "现在怎么了?"


translate chinese epilogue_uv_3bdae7a5:


    uvp "现在我可以和你聊天。"


translate chinese epilogue_uv_d1046a49:


    "她笑了。"


translate chinese epilogue_uv_60f5333f:


    me "你以前做不到吗?"


translate chinese epilogue_uv_020b0a78:


    uvp "那个，我当然可以了，但是..."


translate chinese epilogue_uv_29f71b5c:


    me "发生了什么?"


translate chinese epilogue_uv_5dcae342_2:


    uvp "我不知道。"


translate chinese epilogue_uv_9ffdf555:


    me "纳尼怎么会觉得发生了什么?"


translate chinese epilogue_uv_60a48766:


    uvp "我有这种感觉。"


translate chinese epilogue_uv_0657f52c:


    me "感觉...?"


translate chinese epilogue_uv_0b4ccb3b:


    "我无助的叹叹气。"


translate chinese epilogue_uv_a531a3e7:


    "可能她只是一个从精神病院跑出来的孩子，不是什么精灵?"


translate chinese epilogue_uv_2c88fada:


    me "我说，咱们严肃一点。"


translate chinese epilogue_uv_2df5ed7c:


    uvp "啊，好吧!"


translate chinese epilogue_uv_92199a18:


    "女孩儿认真的看着我。"


translate chinese epilogue_uv_8e9d069a:


    me "你是谁，这里发生了什么?"


translate chinese epilogue_uv_b9debf29:


    uvp "我不知道!"


translate chinese epilogue_uv_7a944f53:


    "她回答的时候脸上总是一样的表情。"


translate chinese epilogue_uv_be4b5d36:


    me "谁知道呢?"


translate chinese epilogue_uv_b9debf29_1:


    uvp "我不知道!"


translate chinese epilogue_uv_007d8828:


    me "好吧，我要去睡觉了。"


translate chinese epilogue_uv_b364eb6c:


    "我没有力气再说下去了，也不想在听她瞎扯或者是在森林里转圈圈。"


translate chinese epilogue_uv_a7f2edd7:


    "即使她知道那些答案，也可以让我回到自己的世界，那也不是今天。{w}今天已经够了!"


translate chinese epilogue_uv_d878403f:


    "我要是继续在这里待着，可能就不用再回去了。"


translate chinese epilogue_uv_89b1c070:


    uvp "晚安。"


translate chinese epilogue_uv_4e7cba70:


    "女孩儿热情的说着。"


translate chinese epilogue_uv_5c1ed51e:


    me "完了?"


translate chinese epilogue_uv_e6b99436:


    uvp "你还想要什么？你不是准备去睡觉了吗。"


translate chinese epilogue_uv_e4426188:


    "她说得对..."


translate chinese epilogue_uv_994a9bf3:


    me "好吧，你至少应该有个名字吧?"


translate chinese epilogue_uv_c07ebab2:


    uvp "名字?"


translate chinese epilogue_uv_ff627021:


    me "对啊，名字!"


translate chinese epilogue_uv_4de354e1_1:


    uvp "我不知道..."


translate chinese epilogue_uv_be4b5d36_1:


    me "谁知道呢?"


translate chinese epilogue_uv_b9debf29_2:


    uvp "我不知道!"


translate chinese epilogue_uv_8225d824:


    me "好吧，那我应该怎么称呼你?"


translate chinese epilogue_uv_4de354e1_2:


    uvp "我不知道..."


translate chinese epilogue_uv_6942194c:


    me "但是一个人...任何世界上的东西都有名字的..."


translate chinese epilogue_uv_4de002b5:


    "她不理解的看着我。"


translate chinese epilogue_uv_08ce5481:


    me "那好吧，你就叫尤莉娅!"


translate chinese epilogue_uv_7b802b26:


    uvp "尤莉娅..."


translate chinese epilogue_uv_69f1b777:


    "我不知道自己为什么选择了这个名字，只是首先出现在了我的脑海中。"


translate chinese epilogue_uv_b9209126:


    uvp "好吧。"


translate chinese epilogue_uv_d2b56c39:


    "她开心的笑着。"


translate chinese epilogue_uv_7d1b4f7a:


    uv "现在我得走了!"


translate chinese epilogue_uv_61d7e5e8:


    "女孩儿挥动着耳朵消失在森林里。"


translate chinese epilogue_uv_6e5f9416:


    "我慢慢拖动着双腿返回房间。"


translate chinese epilogue_uv_fa5ab919:


    "现在我好像在什么悲喜剧里。"


translate chinese epilogue_uv_2bbab062:


    "高等人不可能是幼儿园水平的笨蛋，难道有可能吗？"


translate chinese epilogue_uv_a774658e:


    "当然他们和我们的世界观可能不太相同..."


translate chinese epilogue_uv_e30b61a2:


    "我实在太累了，无法完全理解最近发生的事情。"


translate chinese epilogue_uv_a70e7a5c:


    "答案可能就在我眼前，我需要伸出手臂。{w}也可能帮助我拿到钥匙。"


translate chinese epilogue_uv_674811f4:


    "但是我怎么能在这种情况下保持冷静呢?"


translate chinese epilogue_uv_1e24e1ff:


    "可能我只是累了...真的累了。"


translate chinese epilogue_uv_634747fb:


    "奥尔加·德米特里耶夫娜在门口等着我。"


translate chinese epilogue_uv_87b272e3:


    mt "终于！你这大晚上的跑去哪里了？"


translate chinese epilogue_uv_19d8b6a9:


    "我认真的看着辅导员。"


translate chinese epilogue_uv_2dc57d68:


    "我很好奇她是不是真的?{w}我对尤莉娅一点都不怀疑。{w}她可能不是一个人，但是她是真实存在的。"


translate chinese epilogue_uv_afc131f8:


    "但是奥尔加·德米特里耶夫娜呢，还有其他的少先队员呢?"


translate chinese epilogue_uv_234e69a7:


    me "话说，我看到了那个猫耳娘。"


translate chinese epilogue_uv_975e5089:


    mt "是啊，当然..."


translate chinese epilogue_uv_393056bc:


    "她讥笑着。"


translate chinese epilogue_uv_e7cbadb2:


    me "我说，你要是不相信，我也不强求。"


translate chinese epilogue_uv_f0a7d1a8:


    mt "我不想相信你，我也不相信你!"


translate chinese epilogue_uv_099b3cc6:


    me "这是你的选择。"


translate chinese epilogue_uv_339a89c8:


    "我疲惫的说着，径直往屋里走去。"


translate chinese epilogue_uv_6f09955a:


    mt "谢苗，明天..."


translate chinese epilogue_uv_48e685fd:


    "我没有听见她后面说的什么。"


translate chinese epilogue_uv_bd727701:


    "只有一件重要的事情，到底这里的事我说了算不算。"


translate chinese epilogue_uv_07b2b5de:


    "我应该继续竭力找那把钥匙呢，还是说那个猫耳娘就是我一直以来在寻找的答案?"


translate chinese epilogue_uv_e8bc9d3a:


    "也许唯一剩下的工作就是理解?"


translate chinese epilogue_uv_949ae8dc:


    "没有人说过答案就是那么明显的用你看得懂的语言大字标在那里的!"


translate chinese epilogue_uv_5c917e85:


    "虽然我可能一直都知道那些答案，只是还需要去理解...?"


translate chinese epilogue_uv_86ab4f38:


    "万一这些都是假的，我只是在做梦呢?"


translate chinese epilogue_uv_30cf5a85:


    "万一这些都是我想象出来的呢?"


translate chinese epilogue_uv_d1653018:


    "可能没有笼子，没有监狱，我的家变成了夏令营，要是就在灯上，我的想象中还出现了各种伪装的人类。"


translate chinese epilogue_uv_b8815151:


    "不对...我不是巫师，不是祭司也不是神..."


translate chinese epilogue_uv_8b291a5a:


    "伏尔泰说幻觉是乐趣之首，可能这也是我最成功的技能。"


translate chinese epilogue_uv_0eb0dd52:


    "我的想法相互缠绕，计划着明天的一切，但是安全机制断开了电源，没有像上次一样把我的大脑格式化。"


translate chinese epilogue_uv_3c841c5b:


    "我被一种恶魔般的声音惊醒，结果发现是我的闹铃。"


translate chinese epilogue_uv_5ef84dab:


    "我终于找到那个按钮，按了下去。"


translate chinese epilogue_uv_d7e76fb1:


    me "奥尔加·德米特里耶夫娜才早上七点，干什么?"


translate chinese epilogue_uv_4e6b16ec:


    "我对着对面的床说着。"


translate chinese epilogue_uv_45634255:


    "但是那里没有人，辅导员可能已经走了。"


translate chinese epilogue_uv_a3857818:


    "我费力的站起来，照着镜子，{w}一个普通的少先队员。"


translate chinese epilogue_uv_9ae770c4:


    "如果有人给我看我在里面的少先队员合照，我可能都认不出来自己。"


translate chinese epilogue_uv_3880648b:


    "太阳照耀着大地，大自然好像在嘲笑我。"


translate chinese epilogue_uv_1580acda:


    "我慢慢的往水池走去。"


translate chinese epilogue_uv_d341469e:


    "路上我只见到了几个少先队员，他们都在赶路。"


translate chinese epilogue_uv_d056bcc0:


    "奇怪他们这么早都去哪里啊...?"


translate chinese epilogue_uv_7a6be6b8:


    "冷冷的清水让我清醒了一些，我开始回忆起前几天的事。"


translate chinese epilogue_uv_28ae18f5:


    "唔...我得去找尤莉娅，而且这次得礼貌一些。"


translate chinese epilogue_uv_ceb6fa03:


    "如果你和一个人类说话的时候，按照自己的世界观都搭不上话，那一个完全的外星人不就更不可能了吗?"


translate chinese epilogue_uv_a1eafc73:


    "外星人和猫耳娘为什么要按照我的世界观来行动呢?"


translate chinese epilogue_uv_7a1f73f3:


    "他们的世界观可能是完全不同的，人类对她们来说可能只是蚂蚁。"


translate chinese epilogue_uv_79206dac:


    "杀死一只蚊子我们也不觉得怎么样，所以他们有什么理由注意到我的这些小问题?"


translate chinese epilogue_uv_995e66c0:


    "虽然说完全有可能是反过来的情况...{w}而且可能也不是尤莉娅的问题..."


translate chinese epilogue_uv_a1d45f75:


    "附近的草丛里有动静，我又看到了熟悉的耳朵。"


translate chinese epilogue_uv_fc936817:


    me "我看见你了。"


translate chinese epilogue_uv_bd81185d:


    "尤莉娅不太情愿的离开了藏身处。"


translate chinese epilogue_uv_1e212057:


    uv "哎呀..."


translate chinese epilogue_uv_94f2abe5:


    "她抽着鼻子，撅起嘴巴。"


translate chinese epilogue_uv_0117c38f:


    me "你是在监视我吗?"


translate chinese epilogue_uv_5774bd7a:


    uv "对啊，不行吗?"


translate chinese epilogue_uv_b3e42019:


    me "唔，也许可以吧，反正我也想找你。"


translate chinese epilogue_uv_58bb260d:


    uv "真的吗？我也是。"


translate chinese epilogue_uv_0d6456d9:


    me "关于什么事?"


translate chinese epilogue_uv_d8362ad8:


    "我很好奇。"


translate chinese epilogue_uv_8d36f7b7:


    uv "这里发生了一些奇怪的事情..."


translate chinese epilogue_uv_df3a694c:


    me "奇怪？什么事情？"


translate chinese epilogue_uv_c6f62156:


    uv "不知道...但是...所有人都和平常的表现不太一样。绝对发生了什么事。{w}先是你，然后所有的其他人..."


translate chinese epilogue_uv_c17e7890:


    me "你在说什么？我不明白。"


translate chinese epilogue_uv_f1645aa5:


    uv "我不知道，我就是能感觉到...{w}而且我能感觉到的事情，总是会发生!"


translate chinese epilogue_uv_0b2a7882:


    "尤莉娅看起来有点沮丧甚至有点害怕。"


translate chinese epilogue_uv_3202acd0:


    "这个时候她完全不像是什么高等生物。"


translate chinese epilogue_uv_5756b2ff:


    me "好吧，咱们一起调查一下吧。"


translate chinese epilogue_uv_311b83b6:


    uv "不对，不应该有人看见我，过一个小时过来，暂时先..."


translate chinese epilogue_uv_57b757e5:


    "道路上传来了重重的脚步声。有人在跑着。"


translate chinese epilogue_uv_316ba4bb:


    "我转过来，看到了斯拉维娅。"


translate chinese epilogue_uv_25cb17a4:


    "尤莉娅如我所料消失在了森林中。"


translate chinese epilogue_uv_a87c71d7:


    me "你好。"


translate chinese epilogue_uv_5154e89b:


    "我微笑着。"


translate chinese epilogue_uv_cc470635:


    sl "嗯，你好..."


translate chinese epilogue_uv_9ee3c581:


    "她看起来很紧张，脸颊红红的，眼睛不停的闪烁着，辫子也乱糟糟的。"


translate chinese epilogue_uv_54314043:


    sl "谢苗，那个...{w}跟我来，你会知道的!"


translate chinese epilogue_uv_1449335c:


    me "怎么了?!"


translate chinese epilogue_uv_e2810698:


    "她的表情中充满了恳求，让我不想再去辩解。"


translate chinese epilogue_uv_68329b9d:


    "她拉住我的手，跑向广场，好像整个夏令营的人都在这里了。"


translate chinese epilogue_uv_70843b37:


    "我们走过拐角，穿过那群盯着Genda 的少先队员们..."


translate chinese epilogue_uv_041893cc:


    "远处，原来是森林和田野的地方，开始慢慢的出现了一座城市!"


translate chinese epilogue_uv_9ecdd8e3:


    "刚开始很模糊，好像是用烟雾堆砌成的，然后慢慢的变得坚硬起来，好像从雾中诞生。"


translate chinese epilogue_uv_2ae80add:


    "天空之城——我马上这么想到。"


translate chinese epilogue_uv_39cee6c6:


    "但是不知道是奇怪还是正常，这只是一个普通的城市，就和我们国家别的城市一样。"


translate chinese epilogue_uv_d6dcd9e8:


    "甚至可以说是太普通了，有点无聊。"


translate chinese epilogue_uv_808be7d4:


    "如果你乘车路过或者坐飞机经过的时候完全不会注意到的城市。"


translate chinese epilogue_uv_f1408521:


    "但是在此时此地，夏令营里，另一个维度的广大田野和森林中，仅有的生物是这里的少先队员，松鼠还有蚊子..."


translate chinese epilogue_uv_2444ccc9:


    "连猫耳娘跟这个比起来都不奇怪了!"


translate chinese epilogue_uv_2412f0b1:


    "斯拉维娅不停的使劲攥着我的手。"


translate chinese epilogue_uv_22d16f07:


    "少先队员们在我们身边窃窃私语，好像不敢大声说话，害怕那个幻象会突然消失，或者是完全相反的，变得更加真实，把小猫头鹰夏令营压在底下。"


translate chinese epilogue_uv_727cd6f7:


    "但是城市就待在那里，好像是峡谷里耸立的白色悬崖。"


translate chinese epilogue_uv_4a0ef863:


    "没什么特别的，不管是摩天大楼，还是什么地标建筑，只有不同颜色的房子，工厂，医院，还有一些这类的东西。"


translate chinese epilogue_uv_c096e4e5:


    "从这么远的距离看不清人和汽车，但是我确定这个城市有它自己的生活，和我们的夏令营是独立的。"


translate chinese epilogue_uv_26629e57:


    "我很好奇那里有没有人爬到屋顶上用望远镜看着我们，试图看清广场上熙熙攘攘的人群?"


translate chinese epilogue_uv_ca6dbdb2:


    "果真远处的高楼上有什么东西闪着光。"


translate chinese epilogue_uv_58def530:


    "等一下...{w}望远镜!"


translate chinese epilogue_uv_2215b7ef:


    "根据的我推测这里距离城市大概有两到三公里，所以我要想弄清楚情况的话，就需要一个望远镜。"


translate chinese epilogue_uv_part2_62ac5531:


    "我跑去找奥尔加·德米特里耶夫娜。"


translate chinese epilogue_uv_part2_be449a3c:


    me "你也看到了吗?"


translate chinese epilogue_uv_part2_92063617:


    mt "对啊，当然。"


translate chinese epilogue_uv_part2_c155417f:


    "她心神不宁的说着。"


translate chinese epilogue_uv_part2_064c2399:


    me "我需要望远镜!"


translate chinese epilogue_uv_part2_6bfa8108:


    "辅导员停下了目光，不太理解的看着我。"


translate chinese epilogue_uv_part2_238336f0:


    mt "望远镜...{w}对了，去机器人社团，他们那里应该有的。"


translate chinese epilogue_uv_part2_e0d4808f:


    "我马上往社团楼冲过去。"


translate chinese epilogue_uv_part2_6db36340:


    "电子小子和舒里克还在和往常一样修修补补，好像对外面发生的奇怪现象一无所知。"


translate chinese epilogue_uv_part2_b752e3f7:


    me "望远镜！快!"


translate chinese epilogue_uv_part2_7a052c21:


    "我尽量保持自己的呼吸喊着。"


translate chinese epilogue_uv_part2_7d73585c:


    el "什么?"


translate chinese epilogue_uv_part2_da743cb2:


    me "你没看见吗?{w} 你就跟猫头鹰一样坐在这里鼓捣机器人!{w} 你有没有望远镜?!"


translate chinese epilogue_uv_part2_d081d1f8:


    "我一个字一个字说着好像刚刚学会说话的洞穴人。"


translate chinese epilogue_uv_part2_e68ec0c8:


    sh "你能冷静的解释一下到底是怎么回事吗?"


translate chinese epilogue_uv_part2_94ede6ba:


    me "没时间解释了，赶紧找到望远镜跟我去广场!"


translate chinese epilogue_uv_part2_42e74a51:


    "两个年轻的自动化专家没有和我争吵，不到一分钟我们就来到了少先队员中间，我拿起望远镜仔细的观察着远处的城市。"


translate chinese epilogue_uv_part2_ea239d21:


    "第一眼看上去好像没有什么特别的，给我一种我的时代的感觉，21世纪初的样式：广告牌，卫星接收器，浮夸的商店招牌，现代车辆..."


translate chinese epilogue_uv_part2_5a52abce:


    mt "给我!"


translate chinese epilogue_uv_part2_4fe722fb:


    "辅导员从我手中顺走了望远镜。"


translate chinese epilogue_uv_part2_11af697f:


    uv "所以说这就是你的世界吗?"


translate chinese epilogue_uv_part2_d815dd97:


    "我转过来看到尤莉娅站在我的旁边。"


translate chinese epilogue_uv_part2_b876afc0:


    me "你说没有人可以看见你..."


translate chinese epilogue_uv_part2_4726bdfc:


    uv "是啊，但是..."


translate chinese epilogue_uv_part2_60f3d758:


    "她陷入了沉思。"


translate chinese epilogue_uv_part2_15c6c678:


    uv "现在没有人看见我。"


translate chinese epilogue_uv_part2_6ee3ebcd:


    "少先队员们没有注意到奇怪的猫耳娘，都在专注的看着远处的城市。"


translate chinese epilogue_uv_part2_1e313794:


    me "你知道这是什么吗...还有为什么?"


translate chinese epilogue_uv_part2_c34ebf47:


    uv "不，不知道。"


translate chinese epilogue_uv_part2_9697f09d:


    "她停顿了一下说道。"


translate chinese epilogue_uv_part2_9ca446d1:


    uv "以前从来没有过这种事情！"


translate chinese epilogue_uv_part2_ee658045:


    me "那里是什么?"


translate chinese epilogue_uv_part2_65321879:


    uv "那个，你来了，又走了，然后又来了..."


translate chinese epilogue_uv_part2_7f19b333:


    me "你那时都在做什么?"


translate chinese epilogue_uv_part2_74ba4a64:


    uv "我跟你说过啦，我在准备冬天的粮食!"


translate chinese epilogue_uv_part2_f8af74e9:


    "我完全不明白了，难道尤莉娅完全不觉得现在的情况很奇怪吗，还是说她很擅长表演？"


translate chinese epilogue_uv_part2_65fa2c6e:


    "但是又有谁会搞这么一出就为了给我看呢?{w}我现在不过是一只笼子里的小白鼠!"


translate chinese epilogue_uv_part2_9da8e58a:


    me "我不相信，你应该知道什么的..."


translate chinese epilogue_uv_part2_a6cdc653:


    mt "谢苗。"


translate chinese epilogue_uv_part2_1781e744:


    "我转向辅导员。"


translate chinese epilogue_uv_part2_e8f4b983:


    mt "我不知道这都是怎么回事，但是很奇怪。{w}咱们应该叫警察!"


translate chinese epilogue_uv_part2_04f49c36:


    "奥尔加·德米特里耶夫娜的反应很正常，就像是一个普通人面对这种事件时的反应。"


translate chinese epilogue_uv_part2_dd2c1757:


    "我回去找尤莉娅想要得意的告诉她这个消息，但是她已经消失了。"


translate chinese epilogue_uv_part2_13051a08:


    mt "这能是什么?"


translate chinese epilogue_uv_part2_97006f10:


    "辅导员低声说着，谁也没有提到。"


translate chinese epilogue_uv_part2_a35ec295:


    "我不知道我是不是应该都跟她说了，自己来自未来什么的..."


translate chinese epilogue_uv_part2_04d1fe96:


    "我怎么能确定那些建筑就是来自于我的时代呢?"


translate chinese epilogue_uv_part2_8097992c:


    "会不会只是另一种幻象，然后这些少先队员们的表现也包括其中呢？{w}但是如果不是呢...?"


translate chinese epilogue_uv_part2_ff886f0c:


    me "这个..."


translate chinese epilogue_uv_part2_f58236c0:


    "我没有再说出话来。"


translate chinese epilogue_uv_part2_310e737e:


    us "厉害！厉害!"


translate chinese epilogue_uv_part2_b833fa40:


    "乌里扬卡不知道从哪里突然出现大喊着。"


translate chinese epilogue_uv_part2_da11e595:


    us "咱们过去看看!"


translate chinese epilogue_uv_part2_6b336dbd:


    dv "呃，对，他们一定等着我们呢!"


translate chinese epilogue_uv_part2_e7b79051:


    sl "等一下，咱们应该先考虑一下！"


translate chinese epilogue_uv_part2_2ca18f35:


    "列娜走了过来，但是什么也没有说。"


translate chinese epilogue_uv_part2_35ebcb3b:


    sl "谢苗，你怎么看?"


translate chinese epilogue_uv_part2_e522b546:


    "斯拉维娅热切的看着我，好像在等待着一个立即可以得出的结论。"


translate chinese epilogue_uv_part2_5de47c68:


    "不过说真的为什么我说的话就很重要?"


translate chinese epilogue_uv_part2_d822ea1f:


    "什么让我和他们不同，让我说的话也变得重要了?"


translate chinese epilogue_uv_part2_6cd79bd9:


    "我在这里是一个新人，这有什么重要的？这些事情对我来说比他们可奇怪得多。"


translate chinese epilogue_uv_part2_5c1e6f2d:


    me "我不知道，的确，咱们应该亲自去看看...{w}看起来也不太远。"


translate chinese epilogue_uv_part2_13848e7c:


    mt "但是很危险!"


translate chinese epilogue_uv_part2_850d94f4:


    un "难道坐在这里就不危险了?"


translate chinese epilogue_uv_part2_67db1ed2:


    "列娜不好意思的说着。辅导员还有所有的少先队员都不解的看着她。"


translate chinese epilogue_uv_part2_d17d5e4a:


    un "那个，我是说...{w}如果那座城市突然出现，那么还有可能发生其他的事情，然后我们..."


translate chinese epilogue_uv_part2_b104c7ae:


    "她好像完全糊涂了，没有再说下去。"


translate chinese epilogue_uv_part2_d1a7ad29:


    dv "嗯，她说得对。"


translate chinese epilogue_uv_part2_1086056b:


    "阿丽夏懒洋洋的说着。"


translate chinese epilogue_uv_part2_d2fa28f9:


    dv "我真的不知道这些都是怎么回事，但是我不能就坐在这里干等着!{w}所以如果还有人..."


translate chinese epilogue_uv_part2_af0e79a1:


    us "还有我!"


translate chinese epilogue_uv_part2_bc284071:


    "乌里扬娜打断了她。"


translate chinese epilogue_uv_part2_399ecda1:


    sl "那，到时候怎么办？如果真的只是幻象呢？"


translate chinese epilogue_uv_part2_73dd7efd:


    me "那我们也没有任何损失。"


translate chinese epilogue_uv_part2_98c7b6d0:


    "我对这里的人们热衷于寻找真相有点惊讶，因为之前他们会竭力避免涉及到我来到这个世界的话题。"


translate chinese epilogue_uv_part2_c28ab8e4:


    "也许这是谜中的谜?{w} 就好像两个封闭的球，我是在外壳里，所以这里的事情他们都很明白但是我却不能理解，但是那个城市是交叉地带。"


translate chinese epilogue_uv_part2_1de398c6:


    "但是如果是真的，那么我应该是在边界上，并不了解其他的。"


translate chinese epilogue_uv_part2_a1204b82:


    "奥尔加·德米特里耶夫娜很怀疑。"


translate chinese epilogue_uv_part2_a6810ea8:


    "所有人都在等着她的准许或者是严厉的批评。"


translate chinese epilogue_uv_part2_24b65eee:


    "没什么奇怪的：在夏令营的生活里，像是瑞士的手表一样有秩序，她就是君主，就是统治者，但是当小猫头鹰的边界突然扩展，出现了外部的对手，她的权力就一下子受到了限制，她开始慌了，她的王位开始动摇。"


translate chinese epilogue_uv_part2_15a8d7cc:


    "这是我找回自信的开始!"


translate chinese epilogue_uv_part2_24f6e173:


    me "我觉得咱们应该去看看!"


translate chinese epilogue_uv_part2_f47733f4:


    "我应该在那里坐多长时间，尝试寻找答案?! 去哪儿寻找? 在食堂? 辅导员的房间? Genda雕塑?"


translate chinese epilogue_uv_part2_02724db8:


    "我在这里没有什么可做的，尤其是有这么好的机会的时候!"


translate chinese epilogue_uv_part2_ebcff3e9:


    mt "但是..."


translate chinese epilogue_uv_part2_a5b8262d:


    "奥尔加·德米特里耶夫娜不好意思的拒绝着。"


translate chinese epilogue_uv_part2_63fc37d3:


    me "如果咱们已经死了或者什么的...?"


translate chinese epilogue_uv_part2_1770827f:


    "我必须鼓动大家但是还没有决定是不是要把自己的情况都讲出来。"


translate chinese epilogue_uv_part2_94700582:


    un "那是什么...死了?"


translate chinese epilogue_uv_part2_664ee673:


    "列娜的脸上浮现出恐惧的表情。"


translate chinese epilogue_uv_part2_4eb2832a:


    me "我不是说真的，但是什么都有可能发生，所以必须要考虑到这种情况!"


translate chinese epilogue_uv_part2_d9abbf18:


    "列娜开始哭了起来，斯拉维娅安慰着她。"


translate chinese epilogue_uv_part2_862ecd27:


    us "这样就更有意思了!"


translate chinese epilogue_uv_part2_5ce4cf42:


    "乌里扬卡就是这样，连死亡都不能成为自己沉着冷静的理由。"


translate chinese epilogue_uv_part2_58c57031:


    dv "我们不管怎样都得去!"


translate chinese epilogue_uv_part2_d7b7c1f7:


    "阿丽夏欢呼着。"


translate chinese epilogue_uv_part2_1f1e9ea8:


    "我转过来看着舒里克和电子小子。"


translate chinese epilogue_uv_part2_ad26e5b2:


    el "那个...我们最好留在这里，进行观察，还有计算..."


translate chinese epilogue_uv_part2_855db9ee:


    me "我明白了。"


translate chinese epilogue_uv_part2_8f02b6c0:


    "我也没期待着这两位能做出什么贡献。"


translate chinese epilogue_uv_part2_f785cd61:


    mt "那你可以开始准备你需要的东西..."


translate chinese epilogue_uv_part2_acef7f53:


    "奥尔加·德米特里耶夫娜开始忙起来。"


translate chinese epilogue_uv_part2_4c76d45a:


    mt "手电筒，厚衣服，无线电，你们社团里有无线电吗?"


translate chinese epilogue_uv_part2_69e4a0cc:


    "两个社员表示肯定。"


translate chinese epilogue_uv_part2_ce617998:


    "…"


translate chinese epilogue_uv_part2_ea5dec1f:


    "过了半个小时，调查小组就在广场集结好了：我，阿丽夏，列娜，斯拉维娅，乌里扬娜。"


translate chinese epilogue_uv_part2_811b54d3:


    "我拿着一个很重的背包，里面装着衣服，手电筒还有配给，手里还拿着短波步话机。"


translate chinese epilogue_uv_part2_d8c59b44:


    "我背着这些东西当然也可以坚持的下来，但是为什么不把这些东西分发给大家?"


translate chinese epilogue_uv_part2_bc26005b:


    me "那个，你知道..."


translate chinese epilogue_uv_part2_850ca1bb:


    "我把背包仍在地上对辅导员说着。"


translate chinese epilogue_uv_part2_e33faa0d:


    me "我是这里唯一的男人，你这样让我没法工作了，每个人应该分着带自己的东西!"


translate chinese epilogue_uv_part2_f9473cf8:


    "女孩儿们犹豫了。"


translate chinese epilogue_uv_part2_556ce63a:


    "乌里扬娜首先走了过来， 一边抱怨着一边拿起手电筒和一件大衣。"


translate chinese epilogue_uv_part2_3cd3be0e:


    "其他人也跟着拿起来。"


translate chinese epilogue_uv_part2_492c5c1a:


    "我都没有意料到自己会这么横，也没有意料到她们会这样就同意了。"


translate chinese epilogue_uv_part2_7f3e103d:


    "看来连这里的少先队员们遇到严重的问题时也会比较理智的。"


translate chinese epilogue_uv_part2_ced536c8:


    "过了几分钟我们来到了夏令营的大门。"


translate chinese epilogue_uv_part2_544fcb0f:


    mt "希望你们能够明白我不能和你们一起去...{w}我需要照顾其他人还有..."


translate chinese epilogue_uv_part2_3372f455:


    me "没关系，你不用解释的。"


translate chinese epilogue_uv_part2_e2da94f6:


    "我没有期待着奥尔加·德米特里耶夫娜能帮上什么忙，而且她说的话也有道理。"


translate chinese epilogue_uv_part2_6626bed3:


    mt "祝你们好运，别忘了你们带着无线电!"


translate chinese epilogue_uv_part2_ce617998_1:


    "…"


translate chinese epilogue_uv_part2_61cd3a24:


    "夏日无情的炙烤着我们。"


translate chinese epilogue_uv_part2_89211204:


    "背包里的厚毛衣简直是嘲讽，我愿意拿衣服还有手电还有无线电来换辅导员的巴拿马帽子还有一瓶水。"


translate chinese epilogue_uv_part2_6d59a79c:


    "为什么没有人想到要带着瓶子?"


translate chinese epilogue_uv_part2_9dff38be:


    dv "呃..."


translate chinese epilogue_uv_part2_a8e7c03d:


    "每隔几分钟阿丽夏身上就会少一件衣服。"


translate chinese epilogue_uv_part2_d5817008:


    dv "咱们休息一会儿吧!"


translate chinese epilogue_uv_part2_90621c2d:


    "我停下脚步，遮住眼睛看着远处的城市，看起来就在我们面前。"


translate chinese epilogue_uv_part2_3103df8d:


    "好像一点进展都没有。"


translate chinese epilogue_uv_part2_162c5d13:


    us "为什么不直接朝着它前进?"


translate chinese epilogue_uv_part2_1678d751:


    "乌里扬娜生气的问着。"


translate chinese epilogue_uv_part2_b1c773b0:


    me "怎么前进？穿过森林和原野？"


translate chinese epilogue_uv_part2_ded4003e:


    us "我不管，那样也比绕路好！"


translate chinese epilogue_uv_part2_7adb53b7:


    sl "走公路会更快的，看，那里转弯了。"


translate chinese epilogue_uv_part2_8b245306:


    "斯拉维娅想要和她讲道理。"


translate chinese epilogue_uv_part2_857c395f:


    dv "听你的..."


translate chinese epilogue_uv_part2_d6c4b72d:


    "阿丽夏扑通的躺在路边的草丛中。"


translate chinese epilogue_uv_part2_b413fa09:


    dv "我要休息一会儿。"


translate chinese epilogue_uv_part2_ee90e64d:


    "我们坐在电线杆旁边的一小块空地上。"


translate chinese epilogue_uv_part2_37702a41:


    "我拿出望远镜看着远处的城市。"


translate chinese epilogue_uv_part2_4f498b93:


    "那些高层大楼还在遥远的闪着光。"


translate chinese epilogue_uv_part2_b58174dc:


    "我们为什么一直无法接近，还是说热空气造成了光学的假象?"


translate chinese epilogue_uv_part2_6ead5d25:


    "附近的草丛里有动静，我又看到了熟悉的一对儿耳朵。"


translate chinese epilogue_uv_part2_f948448a:


    me "等一下，我马上回来..."


translate chinese epilogue_uv_part2_af746624:


    "我在灌木丛中向尤莉娅招手，直到我感觉已经拉开足够的距离以后才开口说话。"


translate chinese epilogue_uv_part2_f2554bb0:


    me "所以说你在监视我们?"


translate chinese epilogue_uv_part2_bb30009f:


    uv "嗯，当然了，我也感觉很有意思!"


translate chinese epilogue_uv_part2_721821a4:


    me "啊，我知道了，而且你真的不知道前面有什么，时不时?"


translate chinese epilogue_uv_part2_2590e0ab:


    uv "当然了!"


translate chinese epilogue_uv_part2_06ab0532:


    "她笑了。"


translate chinese epilogue_uv_part2_90fa743d:


    me "你也不知道我们为什么无法接近那座城市?"


translate chinese epilogue_uv_part2_cce2dbb5:


    uv "我知道。"


translate chinese epilogue_uv_part2_b528e2bc:


    "尤莉娅冷静的说。"


translate chinese epilogue_uv_part2_da0567b3:


    me "那为什么?"


translate chinese epilogue_uv_part2_9f61aef1:


    uv "因为你无法离开这个地方。"


translate chinese epilogue_uv_part2_aad4d61a:


    "我重重的叹了一口气，然后靠在树上。"


translate chinese epilogue_uv_part2_34d58fa0:


    me "现在是不是该告诉一切了?"


translate chinese epilogue_uv_part2_60f3d758_1:


    "她好像很困惑。"


translate chinese epilogue_uv_part2_beaccc1b:


    uv "以前不一样的。你来了...然后又走了，然后又来了。不管你怎么走都是一样的，那个城市，是你原来的世界..."


translate chinese epilogue_uv_part2_c89b97c8:


    me "你为什么总是说那是我的世界?"


translate chinese epilogue_uv_part2_c01f7fd7:


    uv "因为那不属于这里!"


translate chinese epilogue_uv_part2_1c70a91b:


    "尤莉娅恼怒了。"


translate chinese epilogue_uv_part2_43d1686d:


    me "这里是什么地方，你能不能说清楚一点?"


translate chinese epilogue_uv_part2_056a2259:


    "我当然没有对她生气，这个猫耳娘好像没有隐瞒什么重要的东西，她可能只是不明白问题或者不知道答案。"


translate chinese epilogue_uv_part2_9abf8151:


    uv "我不知道。"


translate chinese epilogue_uv_part2_5a9a2edc:


    me "好吧，咱们一步一步来，你在这里多长时间了?"


translate chinese epilogue_uv_part2_af20e0a2:


    uv "我一直在这里。"


translate chinese epilogue_uv_part2_8a11ab08:


    me "一直这个说法对于人类来说太复杂了。{w}虽然说你可能并不是人类。"


translate chinese epilogue_uv_part2_89141735:


    "我也糊涂了。"


translate chinese epilogue_uv_part2_2d026c0d:


    me "我在这里干什么?"


translate chinese epilogue_uv_part2_48657b0c:


    uv "你也一直在这里。"


translate chinese epilogue_uv_part2_03adb46e:


    me "那怎么可能？我是复制了很多个自己吗？因为我一点印象也没有啊。"


translate chinese epilogue_uv_part2_79ba8a6b:


    uv "那个，是你也不是你，有很多个你。"


translate chinese epilogue_uv_part2_ddf16d12:


    me "那你是只有一个吗?"


translate chinese epilogue_uv_part2_819d8828:


    uv "对。"


translate chinese epilogue_uv_part2_41c98a55:


    "尤莉娅热情的笑着，不停的摇着尾巴。"


translate chinese epilogue_uv_part2_5de7b061:


    me "你一个人面对所有平行世界的我吗？"


translate chinese epilogue_uv_part2_04cf541e:


    uv "大概是这个样子。"


translate chinese epilogue_uv_part2_5d95f80c:


    me "所以说，你是...全知全能的人?"


translate chinese epilogue_uv_part2_0ce08539:


    "我没有想到更合适的词语。"


translate chinese epilogue_uv_part2_6ccdf245:


    uv "我不知道。"


translate chinese epilogue_uv_part2_f55362eb:


    me "你有超自然的能力吗？你会飞吗？会扔火球吗？会瞬移吗？"


translate chinese epilogue_uv_part2_e4b3a5ba:


    uv "我可以储存冬天的粮食。"


translate chinese epilogue_uv_part2_3d4f4933:


    "她很严肃的说着。"


translate chinese epilogue_uv_part2_4e34062e:


    me "那就当成是“不能”吧...{w}还有，这一切的目的...是什么呢?"


translate chinese epilogue_uv_part2_6ccdf245_1:


    uv "我不知道。"


translate chinese epilogue_uv_part2_8eabed28:


    "如果我开始还算冷静，可能经过这些“我不知道”以后我就要发怒了。"


translate chinese epilogue_uv_part2_997bc03d:


    me "你知道..."


translate chinese epilogue_uv_part2_2a454c26:


    sl "谢苗!"


translate chinese epilogue_uv_part2_5f087d0d:


    "我听到斯拉维娅喊我。"


translate chinese epilogue_uv_part2_2e9e672c:


    "等我再回头看尤莉娅的时候，她已经消失了。"


translate chinese epilogue_uv_part2_d9f91079:


    me "你又..."


translate chinese epilogue_uv_part2_4a436c24:


    "我咬牙切齿的回去找女孩儿们。"


translate chinese epilogue_uv_part2_ce617998_2:


    "…"


translate chinese epilogue_uv_part2_a204f5f2:


    "我们已经走了几个小时了，但是那座城市还是在那个位置。"


translate chinese epilogue_uv_part2_a9df582b:


    "好像我们连一米的进展都没有。"


translate chinese epilogue_uv_part2_ca6e81ce:


    us "我就说了应该直接往那个方向走!"


translate chinese epilogue_uv_part2_d2785117:


    dv "闭嘴！我不明白..."


translate chinese epilogue_uv_part2_540edccf:


    un "有没有可能真的是幻觉?"


translate chinese epilogue_uv_part2_36d8976c:


    "列娜突然说话了。"


translate chinese epilogue_uv_part2_b103b945:


    me "不是吧，沙漠里的海市蜃楼走着走着就会消失的，大概..."


translate chinese epilogue_uv_part2_d83880ec:


    dv "那现在怎么办?"


translate chinese epilogue_uv_part2_a5fdb009:


    "阿丽夏发疯的说。"


translate chinese epilogue_uv_part2_5a93ad2a:


    dv "我已经受够了，我们回去吧!"


translate chinese epilogue_uv_part2_17dea409:


    sl "你要想走就走吧，没有人拦着你!"


translate chinese epilogue_uv_part2_a4fde9ae:


    "斯拉维娅和她吵了起来。"


translate chinese epilogue_uv_part2_7e935856:


    "每个人都惊讶的看着她，听到我们的模范少先队员说出这样的话实在是不平常啊。"


translate chinese epilogue_uv_part2_1413dbd3:


    sl "你怎么总是这么不满意？你不喜欢，就滚回去！"


translate chinese epilogue_uv_part2_f392c23f:


    dv "好像我求着你似的..."


translate chinese epilogue_uv_part2_b75cedb9:


    "阿丽夏有些迟疑。"


translate chinese epilogue_uv_part2_ad0de36d:


    un "啊，她说得对!"


translate chinese epilogue_uv_part2_a9448a42:


    "列娜突然又加入了对话。"


translate chinese epilogue_uv_part2_0a78c8a6:


    un "我已经受够了你的牢骚了!"


translate chinese epilogue_uv_part2_610dac36:


    "乌里扬娜害怕的躲在我的身后。"


translate chinese epilogue_uv_part2_3f49c33b:


    dv "好，你总是要和别人形成对比！可以，在我的衬托下你显得更高大了！"


translate chinese epilogue_uv_part2_f9318593:


    un "好吧，那看你受不受得了这个！"


translate chinese epilogue_uv_part2_75eeb62b:


    "列娜朝阿丽夏冲过去，但是斯拉维娅站在她们中间。"


translate chinese epilogue_uv_part2_cc189774:


    sl "行了，你们停下!"


translate chinese epilogue_uv_part2_d1a7ebe0:


    us "住手!"


translate chinese epilogue_uv_part2_1689ea1f:


    "乌里扬卡哭喊着。"


translate chinese epilogue_uv_part2_dfa2690e:


    "我不知道在这种情况下应该做什么，但是我不能让她们打起来。"


translate chinese epilogue_uv_part2_71564beb:


    "但是不需要我干涉，尤莉娅突然不知道从哪里跳了出来，愤怒的吼叫着。"


translate chinese epilogue_uv_part2_12b53b2f:


    uv "不应该是这样的，这样不对！你们不能这样做！你们不是为了这样的!"


translate chinese epilogue_uv_part2_24b87964:


    "女孩儿后退了一步，阿丽夏尖叫起来，斯拉维娅做出了准备打架的姿势，列娜不知道从哪里掏出一把刀。"


translate chinese epilogue_uv_part2_405c714f:


    sl "你是谁?"


translate chinese epilogue_uv_part2_cd104290:


    un "这是谁?"


translate chinese epilogue_uv_part2_852891ee:


    dv "这是什么?"


translate chinese epilogue_uv_part2_cdd96158:


    "我不知道她指的是尤莉娅还是我。"


translate chinese epilogue_uv_part2_457e2c5c:


    me "等等..."


translate chinese epilogue_uv_part2_6f6f163b:


    "我站在她们和猫耳娘之间。"


translate chinese epilogue_uv_part2_0b28dad0:


    me "我可以解释清楚。"


translate chinese epilogue_uv_part2_893718f4:


    "这就像是找借口...{w}向谁，为了什么？她才应该把事情都说清楚！"


translate chinese epilogue_uv_part2_37d7b496:


    "女孩儿们放松了一些。"


translate chinese epilogue_uv_part2_314afab1:


    me "那个...你们都有没有听说猫耳娘的故事，这个就是..."


translate chinese epilogue_uv_part2_6905fe00:


    "尤莉娅继续发出嘶嘶声，但是声音小了一些。"


translate chinese epilogue_uv_part2_1b195105:


    sl "她知道。"


translate chinese epilogue_uv_part2_2ff596a2:


    "斯拉维娅冷冷地说。"


translate chinese epilogue_uv_part2_3183db72:


    sl "她知道这个城市是什么，她什么都知道!"


translate chinese epilogue_uv_part2_f4f28a92:


    me "她什么也不知道。"


translate chinese epilogue_uv_part2_6ccdf245_2:


    uv "我什么都不知道。"


translate chinese epilogue_uv_part2_b1ca2485:


    "尤莉娅确认了一下。"


translate chinese epilogue_uv_part2_6c17c857:


    dv "你好好问她了吗?"


translate chinese epilogue_uv_part2_2b82084e:


    "阿丽夏站起来掰着关节。"


translate chinese epilogue_uv_part2_293737cc:


    me "你在干什么？！停下！先是公交车，然后是夏令营，然后是你，她，还有那个见鬼的城市!"


translate chinese epilogue_uv_part2_f8b31659:


    "我开始喊了起来，咬着牙，可能眼睛里还闪动着火光。"


translate chinese epilogue_uv_part2_b458ed0a:


    me "够了，够了！我已经受够了这些不确定了！你要么马上告诉我这里到底是怎么回事，不然我一步也不走！你杀了我我也不走。"


translate chinese epilogue_uv_part2_86355b31:


    me "你觉得我什么都忍得了？我不管这里发生了什么，假装没事一样，但是一切事情都是有限度的..."


translate chinese epilogue_uv_part2_0ca24177:


    "我结束了自己的喊叫，大口喘着气。"


translate chinese epilogue_uv_part2_92f97560:


    "我的太阳穴不停的跳动着，整个世界一度变得黑暗，然后又充满了各种颜色。"


translate chinese epilogue_uv_part2_0bf17dcb:


    "女孩儿们惊讶甚至害怕的看着我。"


translate chinese epilogue_uv_part2_a5c34365:


    "尤莉娅不再嘶嘶的叫了，又开始冷漠的看着周围的一切。"


translate chinese epilogue_uv_part2_505d91ae:


    me "好吧..."


translate chinese epilogue_uv_part2_f9b7b6f4:


    "我说完决定继续沿着道路往城市走，然后好像我走一步，城市就往后退一步。"


translate chinese epilogue_uv_part2_58aebb5d:


    "没有人跟着我..."


translate chinese epilogue_uv_part2_ce617998_3:


    "…"


translate chinese epilogue_uv_part2_9db18933:


    "太阳开始落下，在东边还能看到，在雾气中逐渐消失。"


translate chinese epilogue_uv_part2_97bdb375:


    "我看不清楚究竟是在什么地方，因为前面的田野被森林挡住了，但是高楼大厦还是可以看见就像是透景画。"


translate chinese epilogue_uv_part2_9811a37e:


    "这个城市就这样沉默的矗立在那里，好像漂浮在空中一样，低头责备我试图接近的渺小的尝试。"


translate chinese epilogue_uv_part2_899035c2:


    "根据我的计算，我走的距离已经远远超过两三公里了，也就是理论上到城市的距离，我停下来在林中休息，傍晚的凉风拯救了我。"


translate chinese epilogue_uv_part2_3c799697:


    "但是城市可能根本就是不存在的，我可能就是在轮子里跑个不停的仓鼠，累的要死但是目的地其实在笼子外面..."


translate chinese epilogue_uv_part2_a6ba1110:


    "我想起昨天钥匙挂在天花板下面的那种想法。"


translate chinese epilogue_uv_part2_1fd60c00:


    "这个现在对我很有用！可能根本没有答案，我已经太累了...{w}而且还和女孩儿们发了一通脾气。"


translate chinese epilogue_uv_part2_e14ad1c1:


    "啊，真要命!"


translate chinese epilogue_uv_part2_86264158:


    "我坐在路边，卷起一阵尘土，呛得我咳嗽起来。"


translate chinese epilogue_uv_part2_ab8b4683:


    "我的眼里充满泪水。"


translate chinese epilogue_uv_part2_d0c5e298:


    "当我再次睁开眼睛的时候，尤莉娅站在我的面前。"


translate chinese epilogue_uv_part2_71403012:


    me "猫确实比人类有优势，跑起来快很多。"


translate chinese epilogue_uv_part2_aeea0184:


    uv "你要去哪里?"


translate chinese epilogue_uv_part2_663c6ac0:


    "她没有管我说什么，问道。"


translate chinese epilogue_uv_part2_b0e31945:


    me "我就要去那里!"


translate chinese epilogue_uv_part2_90c9d82c:


    "我挥挥手，指着城市的方向。"


translate chinese epilogue_uv_part2_37fe8f5c:


    uv "可是为什么呢?"


translate chinese epilogue_uv_part2_0ee3d648:


    me "其他人在哪里?"


translate chinese epilogue_uv_part2_fa0bf095:


    uv "她们回营地去了。"


translate chinese epilogue_uv_part2_855db9ee_1:


    me "好吧。"


translate chinese epilogue_uv_part2_740dbe23:


    uv "所以说你为什么要去那里?"


translate chinese epilogue_uv_part2_b51017bc:


    me "因为..."


translate chinese epilogue_uv_part2_89141735_1:


    "我糊涂了。"


translate chinese epilogue_uv_part2_369bcbed:


    me "还能干什么？我坐在这里会怎么样？"


translate chinese epilogue_uv_part2_d06bfe4a:


    uv "不怎么样。"


translate chinese epilogue_uv_part2_73cd06af:


    me "我就是这个意思。"


translate chinese epilogue_uv_part2_6af42011:


    "尤莉娅认真的看着我，然后坐在我旁边。"


translate chinese epilogue_uv_part2_479c6fcf:


    uv "我想要帮忙..."


translate chinese epilogue_uv_part2_520e4d28:


    "她突然悄悄地说。"


translate chinese epilogue_uv_part2_b020cad8:


    me "那就帮忙啊!"


translate chinese epilogue_uv_part2_80f6bbf6:


    uv "我不知道要怎么做..."


translate chinese epilogue_uv_part2_583e4d51:


    me "跟我说说那个城市，还有这个世界！我为什么在这里？我要怎么回去？"


translate chinese epilogue_uv_part2_bfefff38:


    uv "我不知道...{w}好吧，我是说...{w}一直就是这样，你坐车来学校，在这里呆一个星期，然后不管怎么样，一切都会重复。然后，有很多个你...都是...不一样的。"


translate chinese epilogue_uv_part2_1eebdf41:


    me "不一样的，是什么意思？"


translate chinese epilogue_uv_part2_99fb7c6c:


    uv "不像是你！只有你是这样的，我才能这样和你聊天，其他人不明白..."


translate chinese epilogue_uv_part2_0180ce19:


    me "他们不明白什么?"


translate chinese epilogue_uv_part2_37b06b96:


    uv "他们有时候消失。"


translate chinese epilogue_uv_part2_be1ebd85:


    "尤莉娅没管我的问题，继续说着。"


translate chinese epilogue_uv_part2_ad623b81:


    uv "应该是这样的，我不知道为什么，但是应该是这样的。他们七天以后不会再来，但是会有新的来。每个人刚刚到这里的时候都是一样的，几乎是一样的。"


translate chinese epilogue_uv_part2_a74fdca1:


    uv "过了一段时间会有区别，尤其是他们明白时间会一遍遍重复以后。"


translate chinese epilogue_uv_part2_43def3e2:


    me "这是说我就注定要不停的绕圈?"


translate chinese epilogue_uv_part2_0e00b743:


    uv "我不知道，这次不太一样。"


translate chinese epilogue_uv_part2_32193853:


    me "为什么要发生这种事情？其他人都是谁？"


translate chinese epilogue_uv_part2_4de002b5:


    "她不解的看着我。"


translate chinese epilogue_uv_part2_a5fc162e:


    uv "但是他们都是你..."


translate chinese epilogue_uv_part2_67914ffa:


    me "什么叫我"


translate chinese epilogue_uv_part2_660e4ce1:


    uv "你还有所有其他的人..."


translate chinese epilogue_uv_part2_8f96fdab:


    "她烦恼起来，试图找到合适的词语。"


translate chinese epilogue_uv_part2_cf6949e5:


    uv "你们都是一样的!"


translate chinese epilogue_uv_part2_781b326f:


    me "所以他们是我克隆出来的还是什么?"


translate chinese epilogue_uv_part2_dc13f3f4:


    uv "克隆是什么?"


translate chinese epilogue_uv_part2_b2232755:


    me "复制品，就像是你为了冬天储藏的蘑菇里有两片完全一样的!"


translate chinese epilogue_uv_part2_2b8c7e9e:


    uv "没有完全一样的蘑菇。"


translate chinese epilogue_uv_part2_2cde1e69:


    "尤莉娅严肃的说着。"


translate chinese epilogue_uv_part2_262a8646:


    me "你想象有一样的!"


translate chinese epilogue_uv_part2_2bb025d9:


    uv "一样的蘑菇...可能...我不知道。"


translate chinese epilogue_uv_part2_5d277179:


    me "天哪，简直是噩梦..."


translate chinese epilogue_uv_part2_84ad17c2:


    "我把头靠在膝盖上，闭上了眼睛。"


translate chinese epilogue_uv_part2_19e9263c:


    "我们就这样沉默了几分钟。"


translate chinese epilogue_uv_part2_893bd690:


    "我真的想知道尤莉娅是怎么想的。"


translate chinese epilogue_uv_part2_f9abbf96:


    "不管怎么说，即使她真的什么也不知道，她至少也应该有点想法吧。"


translate chinese epilogue_uv_part2_8f211b5f:


    me "好吧，我应该怎么办?"


translate chinese epilogue_uv_part2_d1215d87:


    "我严肃的问。"


translate chinese epilogue_uv_part2_4de002b5_1:


    "她又不解的看着我。"


translate chinese epilogue_uv_part2_cad610a0:


    me "你建议我怎么办?"


translate chinese epilogue_uv_part2_9abf8151_1:


    uv "我不知道。"


translate chinese epilogue_uv_part2_48f8fcee:


    me "如果你是我，你会怎么办?"


translate chinese epilogue_uv_part2_4b09c0b5:


    uv "我...?{w}是你...?"


translate chinese epilogue_uv_part2_8495dc4f:


    uv "但是那不可能嘛!"


translate chinese epilogue_uv_part2_05864633:


    "尤莉娅大声笑了起来。"


translate chinese epilogue_uv_part2_f798a921:


    uv "你是你我是我啊!"


translate chinese epilogue_uv_part2_615ead6d:


    me "你想象一下！你有自己的思想，你可以想象!"


translate chinese epilogue_uv_part2_1fbe04a5:


    uv "那个...我什么都不会改变。"


translate chinese epilogue_uv_part2_5ac38cf1:


    me "为什么?"


translate chinese epilogue_uv_part2_3f04a191:


    uv "因为事情就是这样的，这样才不会有麻烦。"


translate chinese epilogue_uv_part2_c080aa07:


    me "所以就待在营地里什么也不干等着无穷的轮回?"


translate chinese epilogue_uv_part2_0e8e1d0f:


    uv "对。"


translate chinese epilogue_uv_part2_8face0e3:


    "我真的不同意，真的。"


translate chinese epilogue_uv_part2_ff9f8675:


    "只要我还能做得到，我就要行动起来!"


translate chinese epilogue_uv_part2_e41273e8:


    "我站起来往远处的城市走去，在远远的暮霭中还隐约可见。"


translate chinese epilogue_uv_part2_ce617998_4:


    "…"


translate chinese epilogue_uv_part2_a10a51f7:


    "最后道路突然转弯，刚刚树后面的楼房都消失了，乡间小路的景色换成了工业郊区。"


translate chinese epilogue_uv_part2_f021e5ae:


    "已经天黑了，天上散布着点点繁星，在我站的地方还有遥远的星星，城市的上方都是明亮的窗户和路灯。"


translate chinese epilogue_uv_part2_47d682d4:


    "寂静黑暗的夜晚占据了我的背后，只被夜间的鸟儿打断，还有草丛的声音和虫子的叫声， 我面前的人工蚁冢一秒钟也不愿停下来眨眨眼睛。"


translate chinese epilogue_uv_part2_29ef898b:


    "街道看起来更加陌生，空空的，好像被废弃了，没有车也没有人。"


translate chinese epilogue_uv_part2_c4ec3c82:


    "像是连接两个世界的狭窄通道。"


translate chinese epilogue_uv_part2_6b67a347:


    "尤莉娅一直在我的旁边跟着。"


translate chinese epilogue_uv_part2_62569e6a:


    me "你怎么在这里?"


translate chinese epilogue_uv_part2_420d89ea:


    "我悄悄的问。"


translate chinese epilogue_uv_part2_686e7c65:


    uv "我必须和你在一起。"


translate chinese epilogue_uv_part2_5ac38cf1_1:


    me "为什么?"


translate chinese epilogue_uv_part2_29bfa65a:


    uv "奇怪，城市一直都是在我们的右边，现在却在我们面前。"


translate chinese epilogue_uv_part2_6ab05308:


    "她又忽略了我的问题。"


translate chinese epilogue_uv_part2_d2669c69:


    me "也没有比别的事情奇怪。{w}我至少走了十几公里了，但是还是那么远!"


translate chinese epilogue_uv_part2_cf738eeb:


    uv "远?"


translate chinese epilogue_uv_part2_44fcb674:


    me "咱们可能永远也到不了!"


translate chinese epilogue_uv_part2_3b18e37c:


    "我停下来开始想象那里到底有什么。"


translate chinese epilogue_uv_part2_fe135b74:


    "那个城市和我的家乡很相似吗？都是什么样的建筑呢？住着什么人呢...?"


translate chinese epilogue_uv_part2_55dee337:


    "我是怎么得出那里有人的结论的？这就不能是一个幻觉吗，这个世界的恶作剧那样吗？"


translate chinese epilogue_uv_part2_959e8bb8:


    "连尤莉娅都说她是第一次见到这些！"


translate chinese epilogue_uv_part2_60491530:


    "突然我的对讲机发出了噪音，奥尔加·德米特里耶夫娜焦躁的声音响了起来："


translate chinese epilogue_uv_part2_7e5a4c32:


    mt "谢苗！有没有收到！谢苗！"


translate chinese epilogue_uv_part2_e81bfa10:


    "过了一会儿我按下按钮回答："


translate chinese epilogue_uv_part2_fc30fd2b:


    me "收到。"


translate chinese epilogue_uv_part2_027e452f:


    mt "谢苗，发生了很多事情！立刻回来！!"


translate chinese epilogue_uv_part2_d64c8037:


    me "什么？怎么了！"


translate chinese epilogue_uv_part2_39ad62fd:


    "我没什么兴趣的回答着。"


translate chinese epilogue_uv_part2_3e4d60db:


    "辅导员，少先队员，整个夏令营都在后面，很远，可不仅仅是几公里了。"


translate chinese epilogue_uv_part2_ce29470a:


    "过去几天发生的事情都失去了意义，就像你遗忘噩梦一样。"


translate chinese epilogue_uv_part2_095e79da:


    "现在有一个真实的世界，就在我的面前...{w}我没有其他的选择，只能相信，在我抵达那座城市的时候一切都会结束。"


translate chinese epilogue_uv_part2_9f2bee1d:


    mt "谢苗，快！没时间解释了，这是灾难..."


translate chinese epilogue_uv_part2_63619bef:


    "电流声越来越大，改过了辅导员的声音。"


translate chinese epilogue_uv_part2_e46e5542:


    uv "你要去吗?"


translate chinese epilogue_uv_part2_dbcf576f:


    "尤莉娅担心的问着。"


translate chinese epilogue_uv_part2_b6d2279b:


    me "哪里？回去？不去，为什么？"


translate chinese epilogue_uv_part2_f27606ef:


    uv "但是..."


translate chinese epilogue_uv_part2_44fa2fc7:


    me "你想让我回去吗?"


translate chinese epilogue_uv_part2_1b462d8f:


    uv "我不知道，你是怎么想的?"


translate chinese epilogue_uv_part2_0e3de8d0:


    "说真的我是怎么想的?"


translate chinese epilogue_uv_part2_4efd7251:


    "前往城市，我这一天都在干什么？或者是更大的问题，试图离开这个世界？"


translate chinese epilogue_uv_part2_79ceb492:


    "万一营地那里真的发生什么严重的问题了呢?"


translate chinese epilogue_uv_part2_75a77f12:


    "我开始头疼起来。"


translate chinese epilogue_uv_part2_cf213262:


    "斯拉维娅，列娜，乌里扬娜，阿丽夏..."


translate chinese epilogue_uv_part2_6f66ce9d:


    "我为什么要关心？这样想行不行？"


translate chinese epilogue_uv_part2_5163e5b3:


    "但是，真的，我还是在乎的!"


translate chinese epilogue_uv_part2_48ba4ca4:


    "我最近和她们那么亲密，我真的已经适应了夏令营的生活了!"


translate chinese epilogue_uv_part2_3aad34f9:


    "但是城市会怎么样？会不会像当时出现一样突然消失？"


translate chinese epilogue_uv_part2_39c34ac9:


    "万一一切就像尤莉娅说的一样呢...?{w}不对，我不能照着那样做。"


translate chinese epilogue_uv_part2_f53442e6:


    "对讲机只发出嘶嘶的声音，但是我还没关，怕随时都有可能有声音。"


translate chinese epilogue_uv_part2_37a46732:


    uv "所以说，你是怎么决定的?"


translate chinese epilogue_uv_part2_44b18e64:


    "我必须作出决定。"


translate chinese epilogue_uv_city_959e0edc:


    "这小猫头鹰，真见鬼！{w}没有夏令营，没有少先队员！{w}我怎么到这里的也不重要，我的首要任务是离开这里，马上！"


translate chinese epilogue_uv_city_54348e42:


    me "我要往前走。"


translate chinese epilogue_uv_city_588dbb53:


    "我耸耸肩，表现出不在乎的样子。"


translate chinese epilogue_uv_city_78170aac:


    "尤莉娅严肃的瞪着我，不过还是往前走了两步，好像在邀请我跟她走。"


translate chinese epilogue_uv_city_2c36965d:


    uv "那个，你要走了吗?"


translate chinese epilogue_uv_city_ce617998:


    "…"


translate chinese epilogue_uv_city_7b1bb487:


    "这好像是我们今天第一次开始接近城市，好像有个一百米。"


translate chinese epilogue_uv_city_fcfa0bc9:


    "我已经确定自己可以接近了，突然后面想起了汽车的喇叭声..."


translate chinese epilogue_uv_city_16335221:


    "一辆公交车，一辆Icarus快速向我们驶来，灯光照亮了黑夜。"


translate chinese epilogue_uv_city_c896c1bf:


    "我站到路边。"


translate chinese epilogue_uv_city_fd94b862:


    me "那是..."


translate chinese epilogue_uv_city_f05fcf4f:


    "尤莉娅不在旁边。"


translate chinese epilogue_uv_city_84bca6ed:


    "她是怎么这样神出鬼没的？"


translate chinese epilogue_uv_city_421162ac:


    "公交车在我旁边停了下来，门打开了，然后..."


translate chinese epilogue_uv_city_8e42683f:


    "...奥尔加·德米特里耶夫娜走了出来!"


translate chinese epilogue_uv_city_58184ba9:


    mt "谢苗！你怎么不回来？我都跟你说了!"


translate chinese epilogue_uv_city_d0bc54d8:


    me "我也想问你了！你说有灾难，但是现在..."


translate chinese epilogue_uv_city_5cb759d8:


    mt "好吧..."


translate chinese epilogue_uv_city_89ab1420:


    "她突然软了下来。"


translate chinese epilogue_uv_city_49846fd4:


    mt "上车。"


translate chinese epilogue_uv_city_ff029072:


    me "什么...？那城市怎么办？"


translate chinese epilogue_uv_city_a770878a:


    mt "我们就是要去看看!"


translate chinese epilogue_uv_city_fb7ddfe9:


    "我犹豫了。"


translate chinese epilogue_uv_city_135f17b8:


    "确实，以公交车的速度很有可能到达城市，但是，辅导员的表现太奇怪了。"


translate chinese epilogue_uv_city_cb631dc4:


    "反正我们也是往相同的方向走..."


translate chinese epilogue_uv_city_71d3b8c8:


    me "好吧"


translate chinese epilogue_uv_city_164e884d:


    "我表示同意，坐上了公交车。"


translate chinese epilogue_uv_city_ce617998_1:


    "…"


translate chinese epilogue_uv_city_cf9891a5:


    "Icarus颠簸的往神秘城市的方向走去。"


translate chinese epilogue_uv_city_12d134cd:


    "要我说这太慢了，我感觉很累，所以闭上了眼睛。"


translate chinese epilogue_uv_city_c8562488:


    "那尤莉娅怎么办？我把她扔在那里了吗？"


translate chinese epilogue_uv_city_9b99b80c:


    "好像小猫头鹰夏令营的所有少先队员全都上车了！"


translate chinese epilogue_uv_city_3d477727:


    "可能她想对我说什么..."


translate chinese epilogue_uv_city_22a67c2b:


    "但是我对睡神的抵抗完全是徒劳，没过几分钟后排座位就响起了我的呼噜声。"


translate chinese epilogue_uv_city_742d6e3a:


    "一切都结束了！还是没结束?"


translate chinese epilogue_uv_city_ef1df980:


    "我特别想出去，不愿意睡着。"


translate chinese epilogue_uv_city_77be2dc1:


    "疲劳是理由吗?"


translate chinese epilogue_uv_city_3eb6b561:


    "可能不是吧，我好像吃了镇定剂。{w}人不能就这样失去意识，我也没有嗜睡症的病史!"


translate chinese epilogue_uv_city_eeb3a5ac:


    "我的思绪汹涌又沉着，像是一条宽阔的河流。"


translate chinese epilogue_uv_city_1aaadbd7:


    "没有人影响它们的飞行，因为我在睡觉..."


translate chinese epilogue_uv_city_0a064f5d:


    "但是我好像是清醒的!"


translate chinese epilogue_uv_city_73eda9e0:


    "我准备睁开眼睛，然后..."


translate chinese epilogue_uv_city_7027362e:


    "老旧，破损的天花板从上面看着我，一条大大的裂缝好像威胁着我二楼随时都有可能掉下来。"


translate chinese epilogue_uv_city_8c1abb55:


    "电脑的风扇平静的转着，但是很焦躁。无数的灰尘在空中舞蹈。"


translate chinese epilogue_uv_city_d103a6b9:


    "窗外的风呼号着，裹挟着雪花敲打着玻璃窗。"


translate chinese epilogue_uv_city_0c034f2c:


    "明亮的冬日的月亮，在夜空中眨着眼睛。"


translate chinese epilogue_uv_city_416493f0:


    "我的头很重很晕，嘴里有一种很糟糕的味道，好像是烟灰缸伴随着伏特加的味道。"


translate chinese epilogue_uv_city_fd751d18:


    "我没有马上明白过来。"


translate chinese epilogue_uv_city_ff227b7b:


    "我昨天打算去哪儿，然后..."


translate chinese epilogue_uv_city_6b1de54a:


    "我做了一个奇怪的梦，梦里有那个奇怪的夏令营，少先队员，辅导员还有猫耳娘还有遥远的城市。"


translate chinese epilogue_uv_city_9715094f:


    "我总是忘掉自己梦的结尾，我们到达城市了吗?"


translate chinese epilogue_uv_city_c814cbd9:


    "等一下，那真的是一个梦吗?"


translate chinese epilogue_uv_city_5ecfb0de:


    "我几乎在那个夏令营里待了一个礼拜!"


translate chinese epilogue_uv_city_aabf4b52:


    "然而我的表似乎不同意——看起来才过了十二个小时。"


translate chinese epilogue_uv_city_1845cce3:


    "我在床上坐起来试图回忆。"


translate chinese epilogue_uv_city_890c3f51:


    "我的感觉和心情都很真实，但是梦里发生的事情就比较模糊了。"


translate chinese epilogue_uv_city_a9378da6:


    "我记得女孩儿们，她们的泪水和笑脸，记得我们一起经历的快乐和悲伤，但是她们的相貌好像都蒙上了阴影。"


translate chinese epilogue_uv_city_079e3950:


    "然后还有一个什么矿井，防空洞，一个小岛还有出发去哪里。{w}还有见了鬼的蚱蜢，总是打扰我的好梦!"


translate chinese epilogue_uv_city_4da0715a:


    "还有斯拉维娅，列娜，阿丽夏，乌里扬娜，还有玛莎...{w}还有尤莉娅!"


translate chinese epilogue_uv_city_f2e3ea45:


    "但是还有别的，还有梦之外的事情。我以后的生活，好像是一种新的存在。"


translate chinese epilogue_uv_city_71c0a0b1:


    "对，我醒过来好几次，在醒着的时候遇到这些女孩儿，然后一切又都重新开始..."


translate chinese epilogue_uv_city_f4b5c749:


    "我的大脑真的承受不了这么大的信息量，我的记忆要溢出了。我每秒钟都在忘掉什么，我无法理解什么是真的什么不是。"


translate chinese epilogue_uv_city_f51c0761:


    "夏令营的所有事情混合在一起成为了一幅奇幻的图画，几百个不同的艺术家，几百种不同的风格。"


translate chinese epilogue_uv_city_4455d2a3:


    "感觉的碎片，情感的阴影，一丝丝的记忆，我的生命分崩离析然后又重新聚合在一起， 结果就是这个我和乌里扬卡一起在旧营地找到的破旧的娃娃，{w}等等，还是说跟斯拉维娅?!"


translate chinese epilogue_uv_city_dd9d01a3:


    "我的眼前浮现出那天晚上寻找舒里克的各种细节，但是我想不起那个时候和我一起去的女孩儿的脸。"


translate chinese epilogue_uv_city_d15cc5f5:


    "{i}梦{/i}每时每刻都变得越来越模糊，会不会很快就被忘掉呢?{w}我不能让那种事情发生！我必须记住！"


translate chinese epilogue_uv_city_0a28bc9e:


    "即使没有细节，即使没有相貌，可能连名字也没有..."


translate chinese epilogue_uv_city_c64f8bb5:


    "我确定这不只是一个梦！{w}是一种更重要的东西..."


translate chinese epilogue_uv_city_ece79837:


    "我的脑袋里响起一种声音，这不是我的声音，是别人的。"


translate chinese epilogue_uv_city_7d1637b6:


    "最开始声音很轻，很远，但是不断的变大，很快我几乎能听清说的是什么了。"


translate chinese epilogue_uv_city_6c11e880:


    "但是也可能只是我的想象。一次脑电脉冲的信息比一千本书还要多！"


translate chinese epilogue_uv_city_18d6a6f0:


    "我想起那个梦里我还没有完成的事情。"


translate chinese epilogue_uv_city_a59d3cd1:


    "城市？不对，我已经到达城市了。"


translate chinese epilogue_uv_city_e19d7211:


    "尤莉娅!"


translate chinese epilogue_uv_city_a529acd3:


    "猫耳娘的身影出现在我的灵魂深处，她愤怒的盯着我，耳朵滑稽的动着。"


translate chinese epilogue_uv_city_8ec1f570:


    "这不是梦吗...?我在夏令营的一个礼拜不是梦吗?!"


translate chinese epilogue_uv_city_7f69f2b6:


    "我在屋里踱步，盯着窗外，外面的世界和昨天一样。"


translate chinese epilogue_uv_city_f65a8fd8:


    "我就处在发疯的边缘!"


translate chinese epilogue_uv_city_e729f7fe:


    "门铃的响声打破了房间里的寂静。"


translate chinese epilogue_uv_city_52f1355e:


    "谁会在这个时候造访?!"


translate chinese epilogue_uv_city_d1f773e3:


    "开始我不想开门，甚至有点害怕，但是我觉得这可能是一个很好的机会来检验这是不是一个梦。"


translate chinese epilogue_uv_city_6dcbff83:


    "梦里和现实里的人们是完全不一样的!"


translate chinese epilogue_uv_city_e05fd94e:


    "我打开门就知道了!"


translate chinese epilogue_uv_city_118a9de7:


    "我两步冲到门口，没有从猫眼看就打开了门..."


translate chinese epilogue_uv_city_cc41fdb9:


    "女孩儿们站在门口...{w}夏令营里的女孩子们!"


translate chinese epilogue_uv_city_91ccfacc:


    "但是她们发生了一些变化。"


translate chinese epilogue_uv_city_e2eac827:


    "我不记得梦里她们样貌的细节了，但是斯拉维娅的发型...{w}乌里扬卡也变高了。"


translate chinese epilogue_uv_city_85b6f90b:


    "沉默持续了好长时间，我呆在原地，完全愣住了，没有任何感情。"


translate chinese epilogue_uv_city_c9f8a031:


    "女孩儿们好像一点也不惊讶..."


translate chinese epilogue_uv_city_51149e68:


    me "…"


translate chinese epilogue_uv_city_9104ac12:


    sl "嗨!"


translate chinese epilogue_uv_city_f64f0819:


    "斯拉维娅开心的说着。"


translate chinese epilogue_uv_city_aa88bb70:


    us "你怎么跟个笨蛋似的站在这里?!"


translate chinese epilogue_uv_city_517f79f3:


    "乌里扬娜喊起来。"


translate chinese epilogue_uv_city_0296ee39:


    me "你们怎么..."


translate chinese epilogue_uv_city_3109432e:


    "我感觉话就在嘴边，但是说出来却语无伦次。"


translate chinese epilogue_uv_city_0a8eba07:


    "我又好多问题要问!"


translate chinese epilogue_uv_city_2e4ca1c2:


    un "等等，女孩儿们，不能这样吓到人家!"


translate chinese epilogue_uv_city_58da8a51:


    "我马上想起了列娜，想起了我们在夏令营结束以后的生活..."


translate chinese epilogue_uv_city_7418a938:


    "还有夜晚和斯拉维娅在公交车站的对话。"


translate chinese epilogue_uv_city_34324030:


    "还有乌里扬娜，我们在大学的相遇。"


translate chinese epilogue_uv_city_4135852b:


    "还有阿丽夏出现在我的音乐会上。"


translate chinese epilogue_uv_city_68c476d7:


    "甚至还有另一个世界的玛莎。"


translate chinese epilogue_uv_city_e64bad3c:


    "但是怎么...?"


translate chinese epilogue_uv_city_a5ba39bf:


    dv "嗯，长话短说..."


translate chinese epilogue_uv_city_368e3ebe:


    "阿丽夏开始随便的说起来。"


translate chinese epilogue_uv_city_bf9f2b06:


    dv "别惊讶，你觉得你是唯一一个吗...?"


translate chinese epilogue_uv_city_ff147745:


    ma "在小猫头鹰。"


translate chinese epilogue_uv_city_e9b81946:


    dv "在小猫头鹰，对，你不是唯一一个!"


translate chinese epilogue_uv_city_9a17ab72:


    us "我们也在那里!"


translate chinese epilogue_uv_city_3465113b:


    me "但是你们怎么...一起在这里..."


translate chinese epilogue_uv_city_c643907f:


    sl "关键是每个人都有自己的夏令营，我们有我们的，你有你的，在我们的夏令营里你表现的不太一样。"


translate chinese epilogue_uv_city_5307db81:


    "斯拉维娅有点尴尬的说着。"


translate chinese epilogue_uv_city_e63f8ffa:


    us "真的不一样！你为了那个面包追我，我几乎逃不了!"


translate chinese epilogue_uv_city_8512ffc9:


    ma "看起来我们比你更早的记起了这些事情，也意识到这不是梦。"


translate chinese epilogue_uv_city_0fb77060:


    me "但是，这是怎么回事...只过了一天啊。"


translate chinese epilogue_uv_city_def4bb08:


    un "不，要长的多。最初我也觉得这只是梦，不过接下来那里的人变得更真实，你也是..."


translate chinese epilogue_uv_city_9fe29739:


    "她深深地叹了口气。"


translate chinese epilogue_uv_city_d14e992a:


    un "还有那些恐怖的..."


translate chinese epilogue_uv_city_d04d005c:


    "好像列娜随时都要哭出来了。"


translate chinese epilogue_uv_city_58742dd8:


    dv "不要说了，我跟你说了一千遍了那不是他，只是另外一个谢苗!"


translate chinese epilogue_uv_city_1c47284d:


    me "什么意思，另外一个谢苗...?"


translate chinese epilogue_uv_city_1ace01ff:


    sl "你要知道有很多梦，很多个夏令营，每个人都有自己的，每个里面你的表现都是不一样的，有时候就是那样的..."


translate chinese epilogue_uv_city_57819faa:


    "她悲伤的看着列娜说:"


translate chinese epilogue_uv_city_d2a2bfc1:


    sl "你的很多化身告诉我其他世界的事情...但是我们在那里只是娃娃，是预先设定好的角色。"


translate chinese epilogue_uv_city_37bdf439:


    us "我不是什么娃娃!"


translate chinese epilogue_uv_city_1678d751:


    "乌里扬娜怒了。"


translate chinese epilogue_uv_city_2800154c:


    dv "当然我们受不了了！我们开始寻找出口，然后有一次我进入了未来的世界。"


translate chinese epilogue_uv_city_f109e0d1:


    ma "别那么叫我!"


translate chinese epilogue_uv_city_42898380:


    me "但是...我记得...那里的夏令营完全不一样，我们在拍电影，然后..."


translate chinese epilogue_uv_city_1b96ba71:


    ma "有什么区别，不管怎么说梦都会重复的。"


translate chinese epilogue_uv_city_51149e68_1:


    me "…"


translate chinese epilogue_uv_city_a5cd4aee:


    dv "然后我们设法把我们五个的世界连在了一起，然后事情就简单了!"


translate chinese epilogue_uv_city_ab0f8bf4:


    "阿丽夏笑了，我认出了那个笑容。"


translate chinese epilogue_uv_city_87568219:


    sl "最后的事情就是要找到你。"


translate chinese epilogue_uv_city_b4cf4929:


    me "找到我？为什么？"


translate chinese epilogue_uv_city_988a30f2:


    "我不是针对那个!"


translate chinese epilogue_uv_city_43800bc3:


    "一切最后都会回到这个地方，小猫头鹰，少先队员，辅导员，女孩儿们..."


translate chinese epilogue_uv_city_d37fcc88:


    me "尤莉娅呢?"


translate chinese epilogue_uv_city_71927170:


    un "谁?"


translate chinese epilogue_uv_city_f9f2a365:


    me "尤莉娅，猫耳娘，你们没有见到吗?"


translate chinese epilogue_uv_city_2bdb69db:


    sl "对，确实有人提到来着..."


translate chinese epilogue_uv_city_e3f80252:


    me "怎么可以...?"


translate chinese epilogue_uv_city_d69ea088:


    "我还是因为离开尤莉娅感觉很愧疚。"


translate chinese epilogue_uv_city_09622045:


    dv "就是这样，我们每个人都只有一个世界，但是你有很多个。"


translate chinese epilogue_uv_city_367090df:


    sl "然后我们意识到只有你连接着现实世界。"


translate chinese epilogue_uv_city_e0a04c2e:


    me "你们是怎么理解的?"


translate chinese epilogue_uv_city_e29b42f5:


    ma "那个少先队员告诉我们的，就是你说已经在梦中待了很长时间的那个。"


translate chinese epilogue_uv_city_d180f7cd:


    us "那个奇怪的人，可能已经完全发疯了！"


translate chinese epilogue_uv_city_c08f848b:


    "我没法直接理解他们说的。"


translate chinese epilogue_uv_city_6bdc8ef5:


    me "好吧，等等..."


translate chinese epilogue_uv_city_670ed473:


    "我开始整理自己的思路。"


translate chinese epilogue_uv_city_4220c762:


    me "就是说那个夏令营不是梦？事情已经发生很久了？有很多有你我的夏令营？但是你们都有一个梦，而我有很多个？不行，我实在是不能理解。"


translate chinese epilogue_uv_city_0e688c6f:


    un "大概是这样的。"


translate chinese epilogue_uv_city_1f90b603:


    us "别绷着个脸，你太没劲了！很有意思的！比如说我就从来没有去过少先队夏令营，我觉得你应该也没有去过。"


translate chinese epilogue_uv_city_156e1688:


    me "但是这后面都是谁？又为了什么呢？"


translate chinese epilogue_uv_city_b1c7cc7f:


    sl "所以我们才来了——就是为了弄清楚。"


translate chinese epilogue_uv_city_0eeb4092:


    me "但是你们是怎么找到我的?"


translate chinese epilogue_uv_city_a5ee8aa0:


    "我突然感觉怀疑甚至恐惧。"


translate chinese epilogue_uv_city_85f93595:


    "乌里扬娜马上捏了我一下。"


translate chinese epilogue_uv_city_38b5e0e1:


    me "哎呦!"


translate chinese epilogue_uv_city_47abcbd1:


    us "怕你觉得自己在做梦!"


translate chinese epilogue_uv_city_50ea836a:


    ma "不难。"


translate chinese epilogue_uv_city_6b8e3cb6:


    dv "如果想要跟你的复制品聊天的话，其实他们可能说出很多东西来。"


translate chinese epilogue_uv_city_0ad194ed:


    un "或者强迫他们说..."


translate chinese epilogue_uv_city_6be3f064:


    "列娜悄悄的补充道。"


translate chinese epilogue_uv_city_a67950e2:


    sl "所以说我们首先在现实生活中相遇，然后我们来找你。"


translate chinese epilogue_uv_city_778a81fa:


    us "迎接你的客人们!"


translate chinese epilogue_uv_city_76517ead:


    me "不可能吧..."


translate chinese epilogue_uv_city_22a5a4b0:


    "眨眼间我的人生就完全变了样。"


translate chinese epilogue_uv_city_5d555141:


    "显然我没有记得这个梦里所有的细节，但是我也觉得那个夏令营其实要更大一些。"


translate chinese epilogue_uv_city_056a21ff:


    "它让我们相聚，先是在那里，然后是在这里。"


translate chinese epilogue_uv_city_5573410f:


    "只有尤莉娅的问题还存在。{w}但是我可能知道答案吗?{w}可能我只是忘了，就跟我忘掉很多其他的事情一样。"


translate chinese epilogue_uv_city_087cd72d:


    me "好吧，希望你们能具体的跟我说说。"


translate chinese epilogue_uv_city_49cc0ae4:


    sl "当然!"


translate chinese epilogue_uv_city_cfc15b56:


    dv "我们就是为了这个来的。"


translate chinese epilogue_uv_city_5034671b:


    me "好吧，请进，别都站在门口..."


translate chinese epilogue_uv_city_d5bf3b5e:


    me "茶，咖啡，跳舞?"


translate chinese epilogue_uv_city_0dd900f4:


    "我试图冷静下来。"


translate chinese epilogue_uv_city_01827c2a:


    "列娜瞪了我一眼。"


translate chinese epilogue_uv_city_a722bd37:


    me "好吧，好吧，跳舞可以等等..."


translate chinese epilogue_uv_city_d99864b1:


    "每个故事都有自己的开始和结尾。"


translate chinese epilogue_uv_city_4d69c034:


    "每个故事都有梗概，摘要，目录，关键词，序章和尾声。"


translate chinese epilogue_uv_city_754a4f66:


    "所有的书如果你重复去读，总会发现一些新的细节。"


translate chinese epilogue_uv_city_39cdaa6f:


    "但是所有的书都有最后一页，当我们读完的时候我们会把它放回架子上。"


translate chinese epilogue_uv_city_611077c4:


    "为了明天能够开启一个新的..."


translate chinese epilogue_uv_ulya_01822020:


    me "好吧。"


translate chinese epilogue_uv_ulya_4569b623:


    "我强迫自己说话。"


translate chinese epilogue_uv_ulya_8f6b1a03:


    uv "什么?"


translate chinese epilogue_uv_ulya_0726abef:


    "尤莉娅很惊讶。"


translate chinese epilogue_uv_ulya_0459b6ff:


    me "好吧，咱们回去。{w}你难道没有期待这个答案吗?"


translate chinese epilogue_uv_ulya_3aa0974f:


    "她狡猾的笑着。"


translate chinese epilogue_uv_ulya_13afe092:


    uv "没有，为什么这么想?"


translate chinese epilogue_uv_ulya_94105ae3:


    me "因为你说过了七天所有的事情都会重复，然后我无法离开这里..."


translate chinese epilogue_uv_ulya_1a2b07d6:


    uv "所以咱们要去弄清楚!"


translate chinese epilogue_uv_ulya_4c07374f:


    me "弄清楚？咱们不应该首先去弄清楚那个城市吗？"


translate chinese epilogue_uv_ulya_b4e02fac:


    uv "这要你自己决定了。"


translate chinese epilogue_uv_ulya_8f838576:


    "我看着她。"


translate chinese epilogue_uv_ulya_5b06f4d4:


    "尤莉娅好像变了，但是我不知道具体是什么。"


translate chinese epilogue_uv_ulya_1c771708:


    me "我已经决定了..."


translate chinese epilogue_uv_ulya_42765c89:


    "我自言自语。"


translate chinese epilogue_uv_ulya_ba638d57:


    uv "那我们应该走了!"


translate chinese epilogue_uv_ulya_1bc90a19:


    "猫耳娘穿过田野，自信的往森林里走去。"


translate chinese epilogue_uv_ulya_219f2350:


    me "嘿，等等，你去哪儿?!"


translate chinese epilogue_uv_ulya_88d9a691:


    uv "这样更快。"


translate chinese epilogue_uv_ulya_bb93d8ba:


    me "快...?"


translate chinese epilogue_uv_ulya_0c925c52:


    "我的大脑中思绪万千，都汇成了一个想法，可能不是什么很友好的想法。"


translate chinese epilogue_uv_ulya_e9681ae3:


    me "所以说你一直..."


translate chinese epilogue_uv_ulya_1402ff26:


    "我一步追上她，抓住了她的尾巴。"


translate chinese epilogue_uv_ulya_b96afcb5:


    me "...你一直都知道？为什么不直接从这里走？不就省得绕弯走一天了吗？！"


translate chinese epilogue_uv_ulya_6a9c4595:


    "尤莉娅愤怒的露出牙看着我。我放开她的尾巴。"


translate chinese epilogue_uv_ulya_7d489e88:


    uv "不!{w}我是说，我可能知道，但是你去那里没什么事可做！"


translate chinese epilogue_uv_ulya_9af1bc42:


    me "在那个城市里?"


translate chinese epilogue_uv_ulya_0e8e1d0f:


    uv "对。"


translate chinese epilogue_uv_ulya_5ac38cf1:


    me "为什么?"


translate chinese epilogue_uv_ulya_a755a034:


    uv "我不知道，不应该那样，那样不对，我也说不清，反正就是这样！"


translate chinese epilogue_uv_ulya_e46208f1:


    "我转过身看着遥远的灯光。"


translate chinese epilogue_uv_ulya_27c22bd1:


    "我很奇怪事情还会变得更糟糕吗?"


translate chinese epilogue_uv_ulya_1d8e7c07:


    "不是说现在已经多么糟糕，但是如果那个城市不消失呢？而且夏令营里到底发生了什么？奥尔加·德米特里耶夫娜什么意思？我已经决定了之后应不应该还继续犹豫？"


translate chinese epilogue_uv_ulya_e744e646:


    me "好吧，咱们走..."


translate chinese epilogue_uv_ulya_ce617998:


    "…"


translate chinese epilogue_uv_ulya_164351d0:


    "我们穿过森林，好几次穿过田野。"


translate chinese epilogue_uv_ulya_cadcee8c:


    "尤莉娅好像对什么很满意。"


translate chinese epilogue_uv_ulya_d7695174:


    me "你可以解释一下你怎么了吗?"


translate chinese epilogue_uv_ulya_a5018e29:


    uv "一切都很不一样。"


translate chinese epilogue_uv_ulya_e1c6333b:


    me "因为这和以前不太一样，...{w}对我已经听说过了，永远重复的七天。"


translate chinese epilogue_uv_ulya_e6d7190e:


    "她只是笑了笑。"


translate chinese epilogue_uv_ulya_a86bf17a:


    me "我很奇怪营地那里到底发生了什么...?"


translate chinese epilogue_uv_ulya_311c443d:


    uv "看!"


translate chinese epilogue_uv_ulya_1ac599e9:


    "远远地可以看到营地大门，在树林后面。"


translate chinese epilogue_uv_ulya_7dc8253b:


    "我们加快了脚步。"


translate chinese epilogue_uv_ulya_ce617998_1:


    "…"


translate chinese epilogue_uv_ulya_bc110277:


    "显然出了什么问题！小猫头鹰夏令营被遗弃了。"


translate chinese epilogue_uv_ulya_35d98a27:


    "可以假设少先队员们都去睡觉了，但是除了路灯一点灯光都没有。而且为什么奥尔加·德米特里耶夫娜还没有带着防爆小组等着我们?"


translate chinese epilogue_uv_ulya_f8c2ea35:


    "她在无线电上说发生了什么事..."


translate chinese epilogue_uv_ulya_af8150cd:


    "我马上被一种焦虑感笼罩。{w}我看着Genda..."


translate chinese epilogue_uv_ulya_f10b5d1e:


    me "城市！没有城市了!"


translate chinese epilogue_uv_ulya_3dcb7711:


    "尤莉娅好像也很惊讶，真的很惊讶。所以我没有去质问她。"


translate chinese epilogue_uv_ulya_0b9c8da0:


    "我回到辅导员的房间，停下来犹豫着。"


translate chinese epilogue_uv_ulya_187b5819:


    "我的眼前闪现出可能发生的事情。"


translate chinese epilogue_uv_ulya_9b3de332:


    "不过我很快回过神。"


translate chinese epilogue_uv_ulya_27c40073:


    me "这，这没有道理啊!"


translate chinese epilogue_uv_ulya_d4106a37:


    "没有什么能证明这个科幻片正在变成血腥的惊悚片。"


translate chinese epilogue_uv_ulya_14081a40:


    "我坚决的拉动把手。"


translate chinese epilogue_uv_ulya_4b142943:


    "里面没有人..."


translate chinese epilogue_uv_ulya_990400df:


    "那我们不能再待在这里了!"


translate chinese epilogue_uv_ulya_1b3b6813:


    "我冲向乌里扬娜和阿丽夏的房间。"


translate chinese epilogue_uv_ulya_c80ffad8:


    "我径直上前去敲门..."


translate chinese epilogue_uv_ulya_7254fc56:


    "但是只有一片静寂。"


translate chinese epilogue_uv_ulya_01d1d520:


    "我在周围走着，看着窗户里面。"


translate chinese epilogue_uv_ulya_b0b2c18b:


    "空的。"


translate chinese epilogue_uv_ulya_c18e9490:


    "好吧，这并不说明什么问题...他们可能只是去什么地方了！"


translate chinese epilogue_uv_ulya_4921445e:


    "我跑去食堂。"


translate chinese epilogue_uv_ulya_0633fc4a:


    "可能大家都想去吃点夜宵..."


translate chinese epilogue_uv_ulya_d1faa6ec:


    "然而门锁上了。"


translate chinese epilogue_uv_ulya_7f2ca87a:


    "我回到广场，精疲力尽的倒在长椅上。"


translate chinese epilogue_uv_ulya_6e6530d1:


    uv "跑够了?"


translate chinese epilogue_uv_ulya_522d9099:


    "尤莉娅静静的跟着我，没有兴趣的问着。"


translate chinese epilogue_uv_ulya_e3b9d717:


    "好吧，对了，我为什么要紧张呢...?"


translate chinese epilogue_uv_ulya_fc975872:


    "不过万一真的发生了什么..."


translate chinese epilogue_uv_ulya_5d937815:


    uv "那个，我饿了，我要去弄点吃的来。"


translate chinese epilogue_uv_ulya_dc61ff3f:


    "我没有反对，瞬间尤莉娅就消失在黑夜中。"


translate chinese epilogue_uv_ulya_92c8e965:


    "这要么是什么讨厌的恶作剧，要么是大家真的都消失了！"


translate chinese epilogue_uv_ulya_f6778e37:


    "这个时间本身不比我突然来到这里更奇怪，也不必那个突然出现又突然消失的城市奇怪，但是..."


translate chinese epilogue_uv_ulya_4fef03bd:


    "我都已经习惯了这个夏令营还有这里的各种少先队员了，但是现在又突然..."


translate chinese epilogue_uv_ulya_dacde494:


    "我终于要适应自己新的生活的时候突然又出事了!"


translate chinese epilogue_uv_ulya_70a36eec:


    "我掩面诅咒着。"


translate chinese epilogue_uv_ulya_98d346b6:


    "我可以再这样坐在这里哭上一会儿，不过..."


translate chinese epilogue_uv_ulya_14c6fc57:


    "一声巨响几乎把我的鼓膜震破，好像打雷一样。"


translate chinese epilogue_uv_ulya_f0593949:


    "怎么回事？天上也没有云。"


translate chinese epilogue_uv_ulya_d7f3983d:


    "我睁开眼睛，看到有人站在我的面前。"


translate chinese epilogue_uv_ulya_7561217b:


    me "你... 你..."


translate chinese epilogue_uv_ulya_c0870411:


    "我结巴的说着。"


translate chinese epilogue_uv_ulya_8e73390d:


    "晴朗的天空突然出现了，闪电挡住了面前这个人的脸。"


translate chinese epilogue_uv_ulya_52297f6e:


    pi "谢苗，怎么一脸悲剧的表情?"


translate chinese epilogue_uv_ulya_d4875625:


    me "你是谁?"


translate chinese epilogue_uv_ulya_a5efbe41:


    "我喊起来。"


translate chinese epilogue_uv_ulya_ae1f4d5e:


    pi "冷静，你没必要这么紧张。会死掉很多细胞的。"


translate chinese epilogue_uv_ulya_163770ab:


    "我唯一的想法就是逃跑，但是我的身体没有响应。"


translate chinese epilogue_uv_ulya_8aa5c206:


    "闪电还在继续打着，让我没有机会看清这个人的脸。"


translate chinese epilogue_uv_ulya_a4822e0d:


    "不过我确定我以前在哪里听过他的声音。"


translate chinese epilogue_uv_ulya_0b34c373:


    me "你是谁?"


translate chinese epilogue_uv_ulya_ca57930f:


    "我更加低声的说。"


translate chinese epilogue_uv_ulya_06a6a402:


    pi "可能你还有更多合适的问题?"


translate chinese epilogue_uv_ulya_20e14b1f:


    me "大家都去那儿了?"


translate chinese epilogue_uv_ulya_7e18cb0c:


    pi "他们已经完成了他们的角色，我们已经不需要他们了。"


translate chinese epilogue_uv_ulya_7b84c73a:


    me "角色？什么角色？"


translate chinese epilogue_uv_ulya_32e5b62e:


    pi "他们的角色。"


translate chinese epilogue_uv_ulya_71a264d4:


    me "那城市呢?"


translate chinese epilogue_uv_ulya_387afb71:


    pi "什么城市?"


translate chinese epilogue_uv_ulya_da03225b:


    "我不知道他是突然有点惊讶还是我听错了。"


translate chinese epilogue_uv_ulya_1cb27603:


    me "城市，那里!"


translate chinese epilogue_uv_ulya_6546a605:


    "我伸手指着Genda。"


translate chinese epilogue_uv_ulya_88b778cd:


    pi "啊，你是说..."


translate chinese epilogue_uv_ulya_6f2d5bcd:


    "他沉默了一会儿。"


translate chinese epilogue_uv_ulya_53131bc2:


    pi "这是你的错!"


translate chinese epilogue_uv_ulya_d3bd0899:


    me "我不明白。"


translate chinese epilogue_uv_ulya_b8796610:


    pi "因为你没有更加努力的尝试!"


translate chinese epilogue_uv_ulya_fa68beb0:


    "少先队员提高了音量。"


translate chinese epilogue_uv_ulya_d7e55f45:


    me "我...我..."


translate chinese epilogue_uv_ulya_a98feb47:


    pi "你需要思考！思考！你来到这里多长时间了？五天？六天？二十六天？"


translate chinese epilogue_uv_ulya_e0eaa37b:


    me "六天...大概..."


translate chinese epilogue_uv_ulya_1190dcda:


    pi "六天？为什么六天？你确定？你为什么确定？因为你数过黎明和黄昏的数量了？还是看了手机上的日期？"


translate chinese epilogue_uv_ulya_7479b07e:


    me "我不知道..."


translate chinese epilogue_uv_ulya_aa8531fb:


    "少先队员发疯了，我完全不知道他在说什么。"


translate chinese epilogue_uv_ulya_9e98cfe6:


    pi "六天...好吧，就说是六天。"


translate chinese epilogue_uv_ulya_62a72b45:


    "他冷静的说着。"


translate chinese epilogue_uv_ulya_c747fdad:


    pi "你以前没有来过这里吗?"


translate chinese epilogue_uv_ulya_5cb822ee:


    me "没有..."


translate chinese epilogue_uv_ulya_94259d94:


    pi "这你也可以确定吗?"


translate chinese epilogue_uv_ulya_7479b07e_1:


    me "我不知道..."


translate chinese epilogue_uv_ulya_24167162:


    pi "如果我说你以前来过呢?"


translate chinese epilogue_uv_ulya_016bd72c:


    "我完全不知道如何应对，就像是第一天一样，我好像发生了什么变化，害怕，恐惧。"


translate chinese epilogue_uv_ulya_6656d4b0:


    me "但是我不记得了..."


translate chinese epilogue_uv_ulya_3ab912f7:


    pi "你为什么要记住?"


translate chinese epilogue_uv_ulya_695225db:


    "他又沉默了，好像在思考。"


translate chinese epilogue_uv_ulya_76244013:


    pi "但是你真的不记得了，好吧，那我告诉你。"


translate chinese epilogue_uv_ulya_6a29747a:


    "我在雷声中很难听清他说的什么。"


translate chinese epilogue_uv_ulya_2d849aac:


    "闪电让我的眼睛里充满了泪水，但是我不敢眨眼，生怕少先队员又消失。"


translate chinese epilogue_uv_ulya_c440a75f:


    "他真的有可能告诉我我是怎么来的。"


translate chinese epilogue_uv_ulya_b17892d5:


    pi "这不是你第一次来，我不知道你来了多少次了，但是没有关系。"


translate chinese epilogue_uv_ulya_e453e3db:


    me "但是为什么？我什么也不记得。"


translate chinese epilogue_uv_ulya_c23e302b:


    pi "你不可能记得！不可能!"


translate chinese epilogue_uv_ulya_419b652c:


    "他喊起来。"


translate chinese epilogue_uv_ulya_44b2f075:


    me "抱歉..."


translate chinese epilogue_uv_ulya_10dc660b:


    "我决定不打断他。"


translate chinese epilogue_uv_ulya_4d99f82b:


    pi "对，这不是你第一次来这里..."


translate chinese epilogue_uv_ulya_f48ef569:


    "他冷静下来继续说。"


translate chinese epilogue_uv_ulya_2a0f20ef:


    pi "但是这次很特殊，这会是你最后一次来这里。"


translate chinese epilogue_uv_ulya_89fb5012:


    "我不能理解他这个“最后一次”的定义是什么。"


translate chinese epilogue_uv_ulya_be736c58:


    "我可以离开这里返回我的正常生活吗？还是说我会死在这里...?"


translate chinese epilogue_uv_ulya_6563fe0f:


    pi "嗯，那只是我的选择。"


translate chinese epilogue_uv_ulya_b2744c5a:


    "他又沉默了很长时间。"


translate chinese epilogue_uv_ulya_57996fa1:


    "最后我决定问他一个问题。"


translate chinese epilogue_uv_ulya_b45a54a4:


    me "这些你都是怎么知道的?"


translate chinese epilogue_uv_ulya_ea18c8d8:


    pi "怎么？可以说，我是你的难兄难弟，..."


translate chinese epilogue_uv_ulya_3f1ecfd9:


    "附近又响起了爆炸声，闪电好像击中了几十米以外的一棵树。"


translate chinese epilogue_uv_ulya_ab12a71f:


    "我马上抱着头趴到椅子下面。"


translate chinese epilogue_uv_ulya_fe32bb4a:


    "我不知道自己躺了多长时间，但是等我恢复过来的时候雷电已经结束了。"


translate chinese epilogue_uv_ulya_5deace4a:


    "我睁开眼睛..."


translate chinese epilogue_uv_ulya_b956f2ed:


    "尤莉娅站在刚刚少先队员的位置。"


translate chinese epilogue_uv_ulya_8c9a0417:


    uv "所以你见到你的哥们儿了。"


translate chinese epilogue_uv_ulya_9ef50886:


    "她笑着，递给我一只手。{w}我很震惊，不知道干什么好，只能继续躺着。"


translate chinese epilogue_uv_ulya_22e36827:


    uv "你还想在那里待多长时间?"


translate chinese epilogue_uv_ulya_b4517927:


    "我费劲的站起来。"


translate chinese epilogue_uv_ulya_66d35f4b:


    me "那是谁?"


translate chinese epilogue_uv_ulya_e4fd6319:


    "我宁愿相信尤莉娅也不愿意相信那个少先队员。"


translate chinese epilogue_uv_ulya_b558a3fc:


    uv "那个，怎么说呢..."


translate chinese epilogue_uv_ulya_60f3d758:


    "她开始思考。"


translate chinese epilogue_uv_ulya_7f1538dd:


    uv "就是说，你不是唯一一个人。准确的说，你在这里是唯一一个人，但是...还有更多夏令营。有很多。还有更多的你，也有很多。"


translate chinese epilogue_uv_ulya_1a2e4c0f:


    "我什么也没明白。"


translate chinese epilogue_uv_ulya_2560e52e:


    uv "有的人可以出现在其他地方，比如说那个少先队员。"


translate chinese epilogue_uv_ulya_b888bb40:


    me "你都知道，但是一直都不告诉我?"


translate chinese epilogue_uv_ulya_eab3261b:


    uv "你又没问。"


translate chinese epilogue_uv_ulya_b53b2a58:


    "她耸耸肩。"


translate chinese epilogue_uv_ulya_23e3138a:


    me "我应该害怕他吗?"


translate chinese epilogue_uv_ulya_2feb68a6:


    uv "我觉得不用...虽然..."


translate chinese epilogue_uv_ulya_1adb473f:


    me "到底怎么?"


translate chinese epilogue_uv_ulya_221d3699:


    "我开始焦虑起来。"


translate chinese epilogue_uv_ulya_089ed32b:


    uv "我很怀疑他...能不能...变成实体。不过你要小心他。"


translate chinese epilogue_uv_ulya_5b247995:


    "我突然接受了好多信息，可能需要一个礼拜才能消化。"


translate chinese epilogue_uv_ulya_c8cd06b9:


    "虽然大概都记住了，但是这些信息好像并没有什么用。"


translate chinese epilogue_uv_ulya_565ca87a:


    me "所以说有很多像我一样的人..."


translate chinese epilogue_uv_ulya_0e8e1d0f_1:


    uv "对。"


translate chinese epilogue_uv_ulya_8e1e107d:


    me "但是我...我们是怎么来到这里的?"


translate chinese epilogue_uv_ulya_9f91c876:


    uv "这我也没有一个准确的答案，我只知道是你来到了这里，你一个人。"


translate chinese epilogue_uv_ulya_03009af2:


    "尤莉娅现在看起来完全不像是那个往蘑菇上撒糖的奇怪猫耳娘了。"


translate chinese epilogue_uv_ulya_84949186:


    "现在正相反，她的眼中充满了自信，甚至有点傲慢。完全没有了之前的幼稚。"


translate chinese epilogue_uv_ulya_321365d8:


    me "那么，你是谁?"


translate chinese epilogue_uv_ulya_739f08ea:


    uv "我?"


translate chinese epilogue_uv_ulya_fc30fd2b:


    me "对啊。"


translate chinese epilogue_uv_ulya_1227e8ef:


    uv "说实话，我也不知道..."


translate chinese epilogue_uv_ulya_c459add4:


    "我张着嘴看着她。"


translate chinese epilogue_uv_ulya_8579e67d:


    me "怎么...你也不知道?"


translate chinese epilogue_uv_ulya_6aad0d0b:


    uv "我唯一记得的事情就是我在这里。别的我一点也不记得了。就好像什么也没有似的。我必须看着你...所有的你，而且我同时存在于各个夏令营里。"


translate chinese epilogue_uv_ulya_ffc4f24e:


    "我从震惊中恢复过来。"


translate chinese epilogue_uv_ulya_5561955a:


    me "然后你还不知道自己是谁?"


translate chinese epilogue_uv_ulya_4fa8ffca:


    uv "对了!"


translate chinese epilogue_uv_ulya_ec2c2a7c:


    "她笑了。"


translate chinese epilogue_uv_ulya_a6440285:


    me "但是那不可能啊，我可以了解失忆症...但是你这样的神一样的人不可能得这种病啊。"


translate chinese epilogue_uv_ulya_104bc8ca:


    uv "谁告诉你我是神一样的人?"


translate chinese epilogue_uv_ulya_d4713cb0:


    me "你不是吗?"


translate chinese epilogue_uv_ulya_079e4740:


    uv "实际上我什么也不是。"


translate chinese epilogue_uv_ulya_ef49cb55:


    me "但是你不知道...也看不到?"


translate chinese epilogue_uv_ulya_5659cee4:


    uv "我知道一些，我也看到了一些..."


translate chinese epilogue_uv_ulya_7530075b:


    me "那么会...你明白的..."


translate chinese epilogue_uv_ulya_f1760393:


    "我不知道怎么叙述我的问题。"


translate chinese epilogue_uv_ulya_5dd4b838:


    uv "不，通常来说过了七天你就会离开夏令营，或者说...消失。然后会来一个新的然后一切都开始重复...夏令营的数量是固定了。"


translate chinese epilogue_uv_ulya_9ca0629f:


    me "我们以前说过这个，消失...去哪里?"


translate chinese epilogue_uv_ulya_9abf8151:


    uv "我不知道。"


translate chinese epilogue_uv_ulya_fbf20f76:


    me "那不可能！你跟我说了那么多..."


translate chinese epilogue_uv_ulya_65f552e4:


    uv "你觉得我在和你隐瞒什么吗?"


translate chinese epilogue_uv_ulya_5a7a7577:


    me "没有。"


translate chinese epilogue_uv_ulya_50bd82b0:


    "我无话可说。"


translate chinese epilogue_uv_ulya_aeafe187:


    "我觉得我不应该质疑她的话...至少目前是这样..."


translate chinese epilogue_uv_ulya_c0c55507:


    me "也许他们不是消失了，只是重新开始一圈?"


translate chinese epilogue_uv_ulya_0c75893a:


    uv "不能排除这种情况，但是我觉得不像是这样的。"


translate chinese epilogue_uv_ulya_231065b3:


    me "那么现在我身上要发生什么事情了?"


translate chinese epilogue_uv_ulya_bf4facf7:


    uv "这个我也解释不了。但是我确定这个要发生了，而且这个是不太平常的事情。这次情况不太一样。"


translate chinese epilogue_uv_ulya_b37bbbdc:


    me "那个少先队员也说到了这个..."


translate chinese epilogue_uv_ulya_de6bbaa6:


    uv "可能他知道的更多一些。"


translate chinese epilogue_uv_ulya_c9bf7d2f:


    me "但是我要怎么理解这些东西呢?"


translate chinese epilogue_uv_ulya_e98d5cf2:


    "我深深的叹了口气，然后抱着头。"


translate chinese epilogue_uv_ulya_039b4986:


    uv "我觉得咱们很快就可以弄明白的。"


translate chinese epilogue_uv_ulya_36f8874d:


    me "好吧...但是，你到底是谁?"


translate chinese epilogue_uv_ulya_dd718d6c:


    "尤莉娅撅起了嘴。"


translate chinese epilogue_uv_ulya_e75258a6:


    uv "我之前说过了!"


translate chinese epilogue_uv_ulya_40dd1aca:


    me "所以你真的失忆了?"


translate chinese epilogue_uv_ulya_710e003a:


    uv "没有..."


translate chinese epilogue_uv_ulya_60f3d758_1:


    "她思考了一会儿。"


translate chinese epilogue_uv_ulya_012d131e:


    uv "可能我完全没有记忆。在这个夏令营出现之前是没有我的，我是不存在的。"


translate chinese epilogue_uv_ulya_a337cbd5:


    me "但..."


translate chinese epilogue_uv_ulya_cd5a786f:


    "我没有说完。"


translate chinese epilogue_uv_ulya_e545f709:


    "考虑到这里发生的各种事情，很多我只能做出猜测。"


translate chinese epilogue_uv_ulya_43398793:


    "这就是说我遇到一个猫耳女孩儿没有什么奇怪的。"


translate chinese epilogue_uv_ulya_fd6b4e40:


    me "你是一个观察者什么的吗?"


translate chinese epilogue_uv_ulya_23eb114f:


    uv "大概吧...{w}我觉得这是一个很合适的词语。"


translate chinese epilogue_uv_ulya_55692694:


    me "那么所有的人在演完自己的角色以后就消失?"


translate chinese epilogue_uv_ulya_f9e487df:


    uv "你在说什么?"


translate chinese epilogue_uv_ulya_2a654aba:


    me "那个少先队员说的..."


translate chinese epilogue_uv_ulya_645769e4:


    uv "角色..."


translate chinese epilogue_uv_ulya_f5cac582:


    "她很滑稽的动着耳朵，让我忍不住想笑。"


translate chinese epilogue_uv_ulya_73acaf44:


    uv "有一点，这里只有你是真的，我是说你，那个少先队员还有其他的..."


translate chinese epilogue_uv_ulya_1b32cfea:


    me "那是怎么?"


translate chinese epilogue_uv_ulya_e2e98778:


    uv "我告诉你了！我不知道！你可以假设我带着有限的知识来到这里，没有别的了。有些事情我知道的不比你多，有的事情我都不知道。"


translate chinese epilogue_uv_ulya_505d91ae:


    me "好吧..."


translate chinese epilogue_uv_ulya_2520f29d:


    "她的话好像很有道理。"


translate chinese epilogue_uv_ulya_7a5d2ca6:


    "如果在这种情况下还有“道理”这种东西的话..."


translate chinese epilogue_uv_ulya_644ea9c2:


    me "那你觉得下一步应该怎么办?"


translate chinese epilogue_uv_ulya_d348b8ed:


    uv "等等看事情会怎样发展。"


translate chinese epilogue_uv_ulya_009e7e71:


    me "对啊，你也什么都做不了..."


translate chinese epilogue_uv_ulya_3e1d421a:


    "我自言自语着。"


translate chinese epilogue_uv_ulya_c6510216:


    uv "不管怎么说，现在是晚饭时间!"


translate chinese epilogue_uv_ulya_e2e98a90:


    "她开心的笑着，摇着尾巴。"


translate chinese epilogue_uv_ulya_9a80cd18:


    me "你没去找什么东西吗...?"


translate chinese epilogue_uv_ulya_8634c528:


    uv "我什么也没找到!{w}我没有时间，我看到闪电就马上回来了。"


translate chinese epilogue_uv_ulya_fe33debe:


    uv "走吧!"


translate chinese epilogue_uv_ulya_badab978:


    "她往食堂的方向走去。"


translate chinese epilogue_uv_ulya_5d070f50:


    "她最好跟我呆在一起。"


translate chinese epilogue_uv_ulya_7f3217c7:


    "那里可以也没有少先队员..."


translate chinese epilogue_uv_ulya_9fbc5272:


    "如果不止有我可以看见他，那就没必要质疑我的精神问题，虽然我最近一直比较担忧。"


translate chinese epilogue_uv_ulya_ce617998_2:


    "…"


translate chinese epilogue_uv_ulya_d74a75da:


    "尤莉娅坐在桌边看着我。"


translate chinese epilogue_uv_ulya_3513c03c:


    uv "所以?"


translate chinese epilogue_uv_ulya_ad02f53a:


    me "什么?"


translate chinese epilogue_uv_ulya_fe7e1a41:


    uv "晚饭。"


translate chinese epilogue_uv_ulya_9de3289a:


    me "对了，晚饭?"


translate chinese epilogue_uv_ulya_9a2e9e53:


    "我打着呵欠，已经过了午夜了。"


translate chinese epilogue_uv_ulya_78d7a896:


    me "对了，你是怎么打开食堂的?"


translate chinese epilogue_uv_ulya_195ecce2:


    uv "总得有点手艺才行!"


translate chinese epilogue_uv_ulya_1812f423:


    "没必要继续这个话题了。"


translate chinese epilogue_uv_ulya_53b7f24a:


    me "现在可能最好该睡觉了吧?"


translate chinese epilogue_uv_ulya_11e4ef3b:


    uv "现在，做饭。"


translate chinese epilogue_uv_ulya_01ef5a6e:


    "她好像独裁者一样的口吻。"


translate chinese epilogue_uv_ulya_de5dd677:


    me "我?"


translate chinese epilogue_uv_ulya_d7494819:


    uv "没看见有别人啊。"


translate chinese epilogue_uv_ulya_c530b477:


    me "好...好吧..."


translate chinese epilogue_uv_ulya_17968fb5:


    "我慢慢的往厨房走去。"


translate chinese epilogue_uv_ulya_f36628c2:


    "因为整个这段关于食物的对话让我也很饿。"


translate chinese epilogue_uv_ulya_30f02899:


    "我在冰箱里找到一盒鸡蛋，然后对尤莉娅说："


translate chinese epilogue_uv_ulya_8908e489:


    me "煎鸡蛋怎么样?"


translate chinese epilogue_uv_ulya_fd28a4ac:


    uv "嗯... 可以."


translate chinese epilogue_uv_ulya_da13cf98:


    "我开始做饭。"


translate chinese epilogue_uv_ulya_cba09001:


    "现在事情很有趣了..."


translate chinese epilogue_uv_ulya_1b2b5e44:


    "我身上发生了各种奇怪的令人不解的事情，但是我还是站在这里做着晚饭。{w}希望能做好。"


translate chinese epilogue_uv_ulya_3e302763:


    "这是俄国特色..."


translate chinese epilogue_uv_ulya_2e254a1c:


    "不管怎么说，我都确定不会发生什么不好的事情。"


translate chinese epilogue_uv_ulya_05c7e10a:


    "而且如果发生什么坏事的话尤莉娅会保护我的。"


translate chinese epilogue_uv_ulya_ee05990f:


    "虽然她说她不懂什么，但是在关键的时候我只能靠她。{w}因为没有其他人了。"


translate chinese epilogue_uv_ulya_cd9b2003:


    "昨天我还什么都做不了，但是今天我至少稍微明白了到底是怎么回事，但是现在..."


translate chinese epilogue_uv_ulya_8445a01a:


    "我们只能舒服的坐下来等着安装完成。"


translate chinese epilogue_uv_ulya_4559a5ad:


    "我加上碎干酪，蛋黄酱，盐还有胡椒粉然后把这一堆糊糊倒进锅里。"


translate chinese epilogue_uv_ulya_db55b061:


    "我还找到了一节香肠还有一块面包。"


translate chinese epilogue_uv_ulya_360e960d:


    "很快我们就坐在一起，静静的吃着煎蛋了。"


translate chinese epilogue_uv_ulya_3d29c745:


    uv "好吃!"


translate chinese epilogue_uv_ulya_09de456b:


    me "没什么特别的...标准的单身汉晚餐。"


translate chinese epilogue_uv_ulya_f569df04:


    uv "我从来没有试过。"


translate chinese epilogue_uv_ulya_666aed83:


    me "可能比蘑菇还有坚果好一些?"


translate chinese epilogue_uv_ulya_7e790588:


    uv "够了。"


translate chinese epilogue_uv_ulya_6b572d3d:


    "尤莉娅生气的回答着。"


translate chinese epilogue_uv_ulya_5ffa82a0:


    "吃完以后我说："


translate chinese epilogue_uv_ulya_9e70d737:


    me "跟我讲讲你的故事...虽然你可能没有什么可讲的"


translate chinese epilogue_uv_ulya_2661c728:


    uv "可能。"


translate chinese epilogue_uv_ulya_8b90ba81:


    me "你在这里多长时间了?"


translate chinese epilogue_uv_ulya_e0e4e466:


    uv "不知道，很久了..."


translate chinese epilogue_uv_ulya_16b7a3e5:


    me "但是你主要是人吗?"


translate chinese epilogue_uv_ulya_0db604b4:


    uv "对，很有可能!"


translate chinese epilogue_uv_ulya_47949dcf:


    me "但是你怎么可能同时出现在不同的地方...?"


translate chinese epilogue_uv_ulya_f1ee5506:


    uv "很难解释，我自己也不太懂。就假设你面前的是唯一的我吧。"


translate chinese epilogue_uv_ulya_ba6a7718:


    "这种假设可以接受。"


translate chinese epilogue_uv_ulya_57f0d6d3:


    "虽然说有很多个尤莉娅也是这个世界的一个谜，但是肯定不是最关键的问题。"


translate chinese epilogue_uv_ulya_eef3f95b:


    "至少我是这么想的。"


translate chinese epilogue_uv_ulya_7b8d131e:


    me "好吧，咱们假设...你对这个世界了解什么?"


translate chinese epilogue_uv_ulya_22fde84e:


    uv "我会读书写字，如果你说的是这个的话。"


translate chinese epilogue_uv_ulya_ec2c2a7c_1:


    "她笑了起来。"


translate chinese epilogue_uv_ulya_9c18fd82:


    me "不，不是那个意思，你知道这个夏令营在什么地方吗?"


translate chinese epilogue_uv_ulya_710e003a_1:


    uv "不知道..."


translate chinese epilogue_uv_ulya_b8b45cc7:


    me "那你应该知道我不是这里的人...不属于这个时间，不属于这个世界。"


translate chinese epilogue_uv_ulya_f754bafd:


    uv "嗯，我知道，但是我不知道具体的情况。"


translate chinese epilogue_uv_ulya_a05ef932:


    me "那如果我问你现在是哪一年..."


translate chinese epilogue_uv_ulya_35d6ab99:


    uv "我无法回答。"


translate chinese epilogue_uv_ulya_a2964860:


    "我深吸一口气，看着窗外。"


translate chinese epilogue_uv_ulya_df22040a:


    uv "总体上来说，你可以认为有的东西我比你懂得多，有的比你少..."


translate chinese epilogue_uv_ulya_8adc0e91:


    me "那咱们应该做什么?"


translate chinese epilogue_uv_ulya_b2dad4f2:


    uv "就跟我之前跟你说的一样，就等等。"


translate chinese epilogue_uv_ulya_d5ef5aac:


    me "等等...具体要做什么?"


translate chinese epilogue_uv_ulya_045a7a56:


    uv "你想要做什么?"


translate chinese epilogue_uv_ulya_89b0220f:


    me "我不知道...但是请原谅我，我不喜欢爬树。"


translate chinese epilogue_uv_ulya_f8b7d908:


    uv "好吧，好吧，那么你能不能跟我讲讲你自己?"


translate chinese epilogue_uv_ulya_256129cd:


    me "现在不行。"


translate chinese epilogue_uv_ulya_7c1ea423:


    uv "好吧，那现在呢?"


translate chinese epilogue_uv_ulya_6ae279d3:


    me "我不知道，就坐下等吧，"


translate chinese epilogue_uv_ulya_64a7a78d:


    uv "那太没劲了!"


translate chinese epilogue_uv_ulya_fc71db18:


    me "没看出有别的选择。"


translate chinese epilogue_uv_ulya_305b323e:


    uv "那就走吧...来吧..."


translate chinese epilogue_uv_ulya_60f3d758_2:


    "她思考了一会儿。"


translate chinese epilogue_uv_ulya_088f9ff2:


    "我认真的看着尤莉娅。"


translate chinese epilogue_uv_ulya_d8abb286:


    "她好像真的觉得这里发生的事情都很有意思。"


translate chinese epilogue_uv_ulya_3de491d4:


    "而且这里的事情在她看来一点都不奇怪。"


translate chinese epilogue_uv_ulya_2d96fef7:


    "从她的角度来看没有什么不平常的事情。"


translate chinese epilogue_uv_ulya_f43abfa2:


    me "你应该知道，这里发生的事让我感觉很陌生，我都不明白自己现在为什么没有躲在床底下不停的颤抖。"


translate chinese epilogue_uv_ulya_2e5a2d95:


    uv "结果你还很勇敢。"


translate chinese epilogue_uv_ulya_6b9955f7:


    "我什么也没有说，只是笑了笑。"


translate chinese epilogue_uv_ulya_cf3d8309:


    uv "我有一个主意!"


translate chinese epilogue_uv_ulya_8f838576_1:


    "我兴致勃勃的看着她。"


translate chinese epilogue_uv_ulya_83a6f072:


    uv "啊不，我又忘了..."


translate chinese epilogue_uv_ulya_802fc6e1:


    "尤莉娅挠着自己的猫耳朵。"


translate chinese epilogue_uv_ulya_9b21e657:


    uv "啊！想起来了！咱们去游泳吧!"


translate chinese epilogue_uv_ulya_e229c5a7:


    "现在我不太相信的看着她。"


translate chinese epilogue_uv_ulya_1c6063a9:


    me "可能还是去睡觉比较好吧?"


translate chinese epilogue_uv_ulya_4ef6df25:


    uv "哎呀行了!"


translate chinese epilogue_uv_ulya_a03f69df:


    "她撅起嘴。"


translate chinese epilogue_uv_ulya_b5792957:


    "确实，今天很热，我浑身都浸满了汗，这么热的时候只有干净了才容易睡着。"


translate chinese epilogue_uv_ulya_304c9144:


    me "好吧，咱们走。"


translate chinese epilogue_uv_ulya_ce617998_3:


    "…"


translate chinese epilogue_uv_ulya_645846e3:


    "过了几分钟，我们来到了沙滩。"


translate chinese epilogue_uv_ulya_7e0cced3:


    "我懒洋洋的躺在沙滩上看着小河。"


translate chinese epilogue_uv_ulya_a1e9fb5f:


    "我突然感觉精疲力尽。"


translate chinese epilogue_uv_ulya_c42573a4:


    me "那个，我不太喜欢游泳。"


translate chinese epilogue_uv_ulya_1b88e3de:


    uv "你是说你不知道怎么游泳?"


translate chinese epilogue_uv_ulya_de0a7798:


    "尤莉娅坐在我旁边。"


translate chinese epilogue_uv_ulya_5ea21bc9:


    me "那个，不是我不知道...{w}反正不重要！少先队员还好，但是怎么可能整个城镇都消失了呢！"


translate chinese epilogue_uv_ulya_cee1d719:


    uv "我告诉你了，我不知道！咱们去游泳吧!"


translate chinese epilogue_uv_ulya_58f195ab:


    me "我不想游..."


translate chinese epilogue_uv_ulya_c07212df:


    "我不想暴露自己游泳很烂的事实。"


translate chinese epilogue_uv_ulya_356ba22a:


    uv "哎呀，来吧，你可以在岸边玩水!"


translate chinese epilogue_uv_ulya_d577923b:


    me "好吧，等等...我去找我的泳裤。"


translate chinese epilogue_uv_ulya_066199aa:


    "但是尤莉娅好像没有听见我...然后开始脱衣服。"


translate chinese epilogue_uv_ulya_8f7a778e:


    me "啊。你...那个..."


translate chinese epilogue_uv_ulya_8f6b1a03_1:


    uv "什么?"


translate chinese epilogue_uv_ulya_62b43e76:


    me "你要这么游泳吗?"


translate chinese epilogue_uv_ulya_4e4ac7db:


    uv "对啊，有什么问题吗?"


translate chinese epilogue_uv_ulya_2be3a365:


    "我马上转了过去。"


translate chinese epilogue_uv_ulya_66bb80fd:


    me "你不感觉不好意思吗?"


translate chinese epilogue_uv_ulya_addfa65b:


    "我尽量冷静的说，但是我因为脸红得发烫不敢看她，倒不是因为出于礼貌。"


translate chinese epilogue_uv_ulya_2a7fa430:


    uv "对谁不好意思？你？"


translate chinese epilogue_uv_ulya_ec2c2a7c_2:


    "她笑了。"


translate chinese epilogue_uv_ulya_5187f093:


    me "你知道吗!"


translate chinese epilogue_uv_ulya_b82d4c86:


    "我转过来，尤莉娅已经在水里，没到了脖子。"


translate chinese epilogue_uv_ulya_059a514d:


    uv "过来!"


translate chinese epilogue_uv_ulya_eb9cb597:


    "我觉得自己就穿着内裤的话也没有什么问题，我脱掉衣服进入水中。"


translate chinese epilogue_uv_ulya_1c12c14a:


    me "呃呃呃...好冷!"


translate chinese epilogue_uv_ulya_6252b6e3:


    uv "动起来就暖和了！像我这样!"


translate chinese epilogue_uv_ulya_831ca62f:


    "她开始跳起来，在水中挥着手。"


translate chinese epilogue_uv_ulya_12554f6d:


    "我还是感觉很尴尬。"


translate chinese epilogue_uv_ulya_21c8990f:


    uv "你变红了。"


translate chinese epilogue_uv_ulya_85e87f2d:


    "尤莉娅兴奋地说。"


translate chinese epilogue_uv_ulya_c105b60f:


    me "当然了！我的旁边有一个裸体的女孩儿...至少是一个类似于人的生物。"


translate chinese epilogue_uv_ulya_cc8ea77e:


    uv "然后呢?"


translate chinese epilogue_uv_ulya_36ed3616:


    me "这还不够吗!"


translate chinese epilogue_uv_ulya_8e053d85:


    "我转过身，同一时间，我感觉尤莉娅抱住了我。"


translate chinese epilogue_uv_ulya_846952dc:


    me "这...这也是你的程序的一部分吗?"


translate chinese epilogue_uv_ulya_7b9304db:


    "我明白自己在完全瞎扯，但是我的大脑中完全没有什么更有意义的话了。"


translate chinese epilogue_uv_ulya_8f6b1a03_2:


    uv "什么?"


translate chinese epilogue_uv_ulya_00ee5007:


    me "那个...你知道..."


translate chinese epilogue_uv_ulya_0a634eed:


    "突然我意识到自己是被一个全裸的女神抱住了。"


translate chinese epilogue_uv_ulya_ef505b03:


    "我的大脑中开始出现各种错误，缺少系统资源，然后蓝屏了。"


translate chinese epilogue_uv_ulya_d4c218f1:


    "我感觉两腿发软，开始往水底下沉，尤莉娅拉住了我。"


translate chinese epilogue_uv_ulya_7035f158:


    uv "你没必要淹自己吧!"


translate chinese epilogue_uv_ulya_51d45ef4:


    "她继续抱着我，更用力的抱着我..."


translate chinese epilogue_uv_ulya_91df6206:


    me "你准备怎么做?"


translate chinese epilogue_uv_ulya_be5ea40d:


    uv "游泳啊，当然了!"


translate chinese epilogue_uv_ulya_2dc1a8de:


    me "所以说你没有暗示什么吗?"


translate chinese epilogue_uv_ulya_7e490fd7:


    "我试图冷静的解释，但是好像效果不太好。"


translate chinese epilogue_uv_ulya_69d9c023:


    uv "比如说什么?"


translate chinese epilogue_uv_ulya_39165438:


    me "嗯...即使你不太记得，你还是很理性...{w}所以你应该可以明白。"


translate chinese epilogue_uv_ulya_1c394603:


    uv "嗯，我不知道..."


translate chinese epilogue_uv_ulya_54a2bc36:


    "她狡猾的说着。"


translate chinese epilogue_uv_ulya_1bdbd74a:


    "我突然感觉有什么东西缠住了我的大腿。"


translate chinese epilogue_uv_ulya_9ff69ab7:


    "我试图逃开，但是尤莉娅仅仅的抱住了我。"


translate chinese epilogue_uv_ulya_055bd048:


    "而且在水里做这些动作和在陆地上是完全不一样的。"


translate chinese epilogue_uv_ulya_c7494045:


    uv "只不过是我的尾巴!"


translate chinese epilogue_uv_ulya_1efb85e5:


    me "尾巴...你不觉得你有尾巴就很奇怪吗?"


translate chinese epilogue_uv_ulya_18295dff:


    uv "什么？你不喜欢我的尾巴吗？如果你踩在上面可是会疼的!"


translate chinese epilogue_uv_ulya_aff91d3f:


    me "等一等..."


translate chinese epilogue_uv_ulya_f608ae54:


    "我停下来开始回忆刚才的想法。"


translate chinese epilogue_uv_ulya_df14bfd6:


    me "所以你也看过那个?"


translate chinese epilogue_uv_ulya_3ec3958b:


    uv "哪个?"


translate chinese epilogue_uv_ulya_1c1b1ab5:


    me "你刚刚引用的那句话。"


translate chinese epilogue_uv_ulya_e2b99f78:


    uv "那个只是..."


translate chinese epilogue_uv_ulya_c12e5015:


    "她突然停下来开始思考。"


translate chinese epilogue_uv_ulya_04b40d70:


    uv "人们不都是那么说的嘛...?{w}一种谚语吧。"


translate chinese epilogue_uv_ulya_7b85edf4:


    me "所以你不知道这是哪里来的？"


translate chinese epilogue_uv_ulya_ec8b60db:


    uv "不知道...谁在乎啊!"


translate chinese epilogue_uv_ulya_fdd16430:


    "尤莉娅笑了。"


translate chinese epilogue_uv_ulya_31308d65:


    "她的尾巴继续探索着我的身体。"


translate chinese epilogue_uv_ulya_c62be51e:


    "有没有可能尤莉娅还记得一些她来这个夏令营以前的事情?"


translate chinese epilogue_uv_ulya_1c773453:


    "也许那些记忆被隐藏在什么地方了。"


translate chinese epilogue_uv_ulya_135f2864:


    "她通过某种方式了解到了那句话!"


translate chinese epilogue_uv_ulya_144b5de1:


    "当然她有可能是听某个少先队员说的..."


translate chinese epilogue_uv_ulya_8d8c4af3:


    "但是那个动画片在这个时代不是还没有上映吗?"


translate chinese epilogue_uv_ulya_0191cc8f:


    "唯一的结论是是尤莉娅可能也是这个夏令营里的一个囚犯，就像我一样。{w}不过更神秘一些。"


translate chinese epilogue_uv_ulya_89471135:


    "一切都自然的发生了。"


translate chinese epilogue_uv_ulya_750ac835:


    "虽然说这样做有点危险，但是我还是一个充满活力的男人啊..."


translate chinese epilogue_uv_ulya_3fb50882:


    "在这个时候尤莉娅不像是一个神，而只是我怀中的少女。"


translate chinese epilogue_uv_ulya_a20cefa7:


    "…"


translate chinese epilogue_uv_ulya_d70204eb:


    "我抱着睡着的尤莉娅回到我的房间，她真的好轻。"


translate chinese epilogue_uv_ulya_a4ad8aa8:


    "我把她放在床上，躺在旁边，闭上了眼睛..."


translate chinese epilogue_uv_ulya_e40d9ea3:


    "但是我一直睡不着，我感觉很渴。"


translate chinese epilogue_uv_ulya_e74c6a6a:


    "所以我很安静的下了床，穿上裤子出来。"


translate chinese epilogue_uv_ulya_4e13b8c2:


    "在去水池的路上我开始回想这一切。"


translate chinese epilogue_uv_ulya_1bab0291:


    "我不在乎这对不对，这不是主要问题。"


translate chinese epilogue_uv_ulya_d913a13b:


    "我觉得自己在那个情况下没有选择。"


translate chinese epilogue_uv_ulya_edf2e8e3:


    "可能的结果更加重要。"


translate chinese epilogue_uv_ulya_77a3bfd6:


    "即使她和我不太一样，她和幕后的那个人更近，也只是因为她了解的更多。"


translate chinese epilogue_uv_ulya_ec7e2bac:


    "也就是说...在这个夏令营里胡乱猜想是一个很困难的任务，现在我能确定了。"


translate chinese epilogue_uv_ulya_ce617998_4:


    "…"


translate chinese epilogue_uv_ulya_53310946:


    "我解决了自己的口渴，哼着皮尔金的曲子，突然有人叫住了我。"


translate chinese epilogue_uv_ulya_71c2dd40:


    pi "你怎么...?"


translate chinese epilogue_uv_ulya_b03222c9:


    "我一下跳了起来，看到了那个少先队员的轮廓。"


translate chinese epilogue_uv_ulya_5e973abc:


    "黑暗中我看不清他的脸。"


translate chinese epilogue_uv_ulya_7b78cd99:


    me "所以说...又是你?"


translate chinese epilogue_uv_ulya_9837d783:


    pi "我是谁？不是说我是哪个你见过的人，而是说我究竟是什么人?"


translate chinese epilogue_uv_ulya_d3bd0899_1:


    me "我不明白。"


translate chinese epilogue_uv_ulya_1b8a1e41:


    "我记得尤莉娅说过他无法对我造成身体上的伤害，然后我冷静了一些。"


translate chinese epilogue_uv_ulya_2a3661e7:


    pi "啊算了。说起来，我看到你在那里干什么了！"


translate chinese epilogue_uv_ulya_98007ecd:


    "他的话让我坐立不安。"


translate chinese epilogue_uv_ulya_5cfa92d6:


    "不管现在的情况多么神奇，在你亲密的时候被抓住总是很失望。"


translate chinese epilogue_uv_ulya_73a1fd25:


    me "没有人告诉过你偷看别人是不对的吗?"


translate chinese epilogue_uv_ulya_77ddb932:


    pi "你这人太没意思。"


translate chinese epilogue_uv_ulya_c45ede0e:


    me "你以为呢..."


translate chinese epilogue_uv_ulya_346c99a6:


    "我稍微转过身，试图看清他的脸。"


translate chinese epilogue_uv_ulya_772467b0:


    pi "那个猫耳朵的跟你怎么说的?"


translate chinese epilogue_uv_ulya_02007f83:


    me "她有名字!"


translate chinese epilogue_uv_ulya_e1e64156:


    "我很依恋尤莉娅，所以马上站出来反击。"


translate chinese epilogue_uv_ulya_cba6f894:


    pi "名字？什么名字？上帝？多面神？"


translate chinese epilogue_uv_ulya_25dedf39:


    me "尤莉娅..."


translate chinese epilogue_uv_ulya_fefef18f:


    "我有点不太确定的说着。"


translate chinese epilogue_uv_ulya_cf46188f:


    pi "尤莉娅?!"


translate chinese epilogue_uv_ulya_73e6bc8d:


    "他疯了一样的笑着。"


translate chinese epilogue_uv_ulya_b58f3a60:


    pi "谁给她起的名字？是你吧？"


translate chinese epilogue_uv_ulya_9bfb401d:


    me "好吧，告诉我你想要什么，然后滚开！!"


translate chinese epilogue_uv_ulya_c16c1f2a:


    pi "好了，好了，不要生气，陛下!"


translate chinese epilogue_uv_ulya_dfa62206:


    "他奇怪的鞠躬。"


translate chinese epilogue_uv_ulya_4e88db3f:


    "这景象太荒唐，让我开始发抖。"


translate chinese epilogue_uv_ulya_ba66f329:


    pi "所谓的尤莉娅跟你说什么了?"


translate chinese epilogue_uv_ulya_2dd45817:


    "我觉得自己没必要隐瞒最近发现的事情。"


translate chinese epilogue_uv_ulya_a8058b44:


    me "她告诉我还有其他的夏令营还有你和我一样。还有过了七天我们就会离开不再回来。"


translate chinese epilogue_uv_ulya_708e2129:


    "和他在一条船上让我很不爽，但是事实就是事实。"


translate chinese epilogue_uv_ulya_8d323880:


    pi "对了，大概就是这样的，她怎么介绍自己的?"


translate chinese epilogue_uv_ulya_99d4c76e:


    me "你觉得这和你有关系吗？而且你自己完全可以弄清楚这些事情的。"


translate chinese epilogue_uv_ulya_23fad338:


    pi "对，我了解，我和她有过接触。"


translate chinese epilogue_uv_ulya_afb9b3c2:


    "他的语调好恶心，让我不想继续对话。"


translate chinese epilogue_uv_ulya_63e70168:


    me "好吧，如果你没有什么可说的了..."


translate chinese epilogue_uv_ulya_e52964ff:


    pi "不不，等一等，我还没有说完。"


translate chinese epilogue_uv_ulya_4b2daec2:


    me "又怎么了?"


translate chinese epilogue_uv_ulya_c5eeecce:


    pi "想要我告诉你她隐瞒了什么吗?"


translate chinese epilogue_uv_ulya_cd917d11:


    me "讲。"


translate chinese epilogue_uv_ulya_97dc66db:


    "我确定尤莉娅没有对我隐瞒什么，但是对我来说任何信息都是有用的。"


translate chinese epilogue_uv_ulya_5f0edde5:


    pi "“不再回来”...不不不，他们会回来的，你也会回来的，至少你就曾经回来过。"


translate chinese epilogue_uv_ulya_787e2d21:


    me "我不明白。"


translate chinese epilogue_uv_ulya_bc5bc614:


    pi "你已经来过这里了，所以七天以后不管你是坐上公交车还是在森林里睡觉，反正你都会再次在410路上醒过来。"


translate chinese epilogue_uv_ulya_5c4cab43:


    me "怎么...?"


translate chinese epilogue_uv_ulya_666d09fb:


    pi "她告诉你咱们只是消失，对吧?"


translate chinese epilogue_uv_ulya_7fe9a557:


    "我没有回答。"


translate chinese epilogue_uv_ulya_de006685:


    pi "嗯，我不能确定她是不是说谎，可能她的确不了解这些，但是我很确定！你和我已经多次来这里了！我当然来过这里很多次，但是没有关系。"


translate chinese epilogue_uv_ulya_c310b5dd:


    me "好吧，但是我为什么一点也不记得?"


translate chinese epilogue_uv_ulya_b00c1e22:


    pi "你回头会想起来的，我就是这样的。"


translate chinese epilogue_uv_ulya_70d504c6:


    me "然后你说我无法逃离这里?"


translate chinese epilogue_uv_ulya_f4dc0758:


    pi "正是。"


translate chinese epilogue_uv_ulya_916a3a3b:


    me "我记得你说今天发生的事情是头一次对吗?"


translate chinese epilogue_uv_ulya_8761e86f:


    pi "对，是这样。"


translate chinese epilogue_uv_ulya_76023e32:


    me "那你怎么解释这个?"


translate chinese epilogue_uv_ulya_86cd0059:


    pi "我不管！你觉得会有什么发生变化？不可能!"


translate chinese epilogue_uv_ulya_8d1f0c27:


    me "咱们走着瞧吧。"


translate chinese epilogue_uv_ulya_2117dd83:


    "我坚决的转过身走开了。"


translate chinese epilogue_uv_ulya_aba701a6:


    "让我惊讶的是少先队员居然没有试图拦住我。"


translate chinese epilogue_uv_ulya_021b03c0:


    "我回到房间，找到尤莉娅，小心的抚摸着她的尾巴。"


translate chinese epilogue_uv_ulya_9364ca36:


    uv "你又和他聊天了。"


translate chinese epilogue_uv_ulya_e861403d:


    me "你怎么知道的?"


translate chinese epilogue_uv_ulya_105c8aa8:


    "我感觉很奇怪。"


translate chinese epilogue_uv_ulya_4632840a:


    uv "我猜的，他在你一个人的时候才会出现。"


translate chinese epilogue_uv_ulya_9513cd87:


    me "对..."


translate chinese epilogue_uv_ulya_cc8ea77e_1:


    uv "然后呢?"


translate chinese epilogue_uv_ulya_4d5b668d:


    me "他说没有人会消失，大家都会回来。"


translate chinese epilogue_uv_ulya_92ef5f8d:


    "尤莉娅没有回答。"


translate chinese epilogue_uv_ulya_3581e356:


    me "是这样的吗?"


translate chinese epilogue_uv_ulya_7f260d43:


    uv "跟你说实话，我不知道..."


translate chinese epilogue_uv_ulya_407f9e91:


    "我坐在她旁边开始思考。"


translate chinese epilogue_uv_ulya_ce617998_5:


    "…"


translate chinese epilogue_uv_ulya_b838dd19:


    "我不知道自己坐了多长时间，但是我开始犯困。"


translate chinese epilogue_uv_ulya_c69a0894:


    uv "咱们去看星星吧。"


translate chinese epilogue_uv_ulya_304c9144_1:


    me "好吧。"


translate chinese epilogue_uv_ulya_fd40ac42:


    "我很想睡觉，但是有感觉很失落，希望清新的空气可以让我清醒一些。"


translate chinese epilogue_uv_ulya_63937d5a:


    "过了几分钟我们来到了广场。"


translate chinese epilogue_uv_ulya_0734e246:


    "我坐在长椅上，尤莉娅躺在我的膝盖上望着天空。"


translate chinese epilogue_uv_ulya_5c9504e1:


    uv "好美啊..."


translate chinese epilogue_uv_ulya_9513cd87_1:


    me "是啊..."


translate chinese epilogue_uv_ulya_4900029d:


    "我们就这样静静的坐了很久。"


translate chinese epilogue_uv_ulya_ceb783e7:


    me "明天会发生什么呢?"


translate chinese epilogue_uv_ulya_62cb2255:


    uv "什么意思?"


translate chinese epilogue_uv_ulya_ec806ff9:


    me "嗯，明天是第七天...所以该离开了。"


translate chinese epilogue_uv_ulya_22006ea3:


    uv "你想要离开吗?"


translate chinese epilogue_uv_ulya_6343bef6:


    "我突然清醒过来，我听到过很多次这个问题。"


translate chinese epilogue_uv_ulya_56494f6b:


    me "根据你们两个说的话来判断，就算我不想走也没用。"


translate chinese epilogue_uv_ulya_72be10d8:


    uv "那咱们就走吧。"


translate chinese epilogue_uv_ulya_d1046a49:


    "她笑了。"


translate chinese epilogue_uv_ulya_dab936c8:


    me "你会和我一起走吗?"


translate chinese epilogue_uv_ulya_167bb087:


    uv "当然了，经过了这些事，你一定要娶我喔~"


translate chinese epilogue_uv_ulya_ec3468be:


    me "我感觉没有哪个登记处会承认人类和猫耳娘结婚。"


translate chinese epilogue_uv_ulya_158aae48:


    uv "那就找一个。"


translate chinese epilogue_uv_ulya_39fba389:


    me "好吧好吧，那么明天会发生什么?"


translate chinese epilogue_uv_ulya_e652c25e:


    uv "晚上公交车会来..."


translate chinese epilogue_uv_ulya_aa9c1d3b:


    me "这个你确定吗?"


translate chinese epilogue_uv_ulya_21e652c5:


    uv "当然了！总是会来的。"


translate chinese epilogue_uv_ulya_f95c9fc9:


    me "然后呢?"


translate chinese epilogue_uv_ulya_6c27c947:


    uv "然后咱们坐上去。"


translate chinese epilogue_uv_ulya_f4eb646d:


    me "然后呢?"


translate chinese epilogue_uv_ulya_733740cb:


    uv "你怎么总是问这些愚蠢的问题啊?!"


translate chinese epilogue_uv_ulya_1c70a91b:


    "尤莉娅抗议道。"


translate chinese epilogue_uv_ulya_55b0a935:


    uv "我怎么知道?"


translate chinese epilogue_uv_ulya_c25ae0cb:


    me "那就猜猜吧!"


translate chinese epilogue_uv_ulya_a7a02427:


    uv "然后...咱们一起回到你的世界。"


translate chinese epilogue_uv_ulya_1f7d095b:


    me "那就是..."


translate chinese epilogue_uv_ulya_2a7a8a3b:


    "我停下了。"


translate chinese epilogue_uv_ulya_f20e460f:


    me "到时候看吧。"


translate chinese epilogue_uv_ulya_9da1463d:


    "我叹叹气。"


translate chinese epilogue_uv_ulya_510df513:


    me "那那无数个你呢?"


translate chinese epilogue_uv_ulya_4e31d83e:


    uv "我只是在这里看起来是这样的，对吧？如果过去的话我会很普通的。"


translate chinese epilogue_uv_ulya_50cdb6bd:


    me "当然，要猫耳还有尾巴..."


translate chinese epilogue_uv_ulya_6c4c0225:


    "尤莉娅打得我肋骨很痛。"


translate chinese epilogue_uv_ulya_1a723568:


    me "嘿！我只是开玩笑!"


translate chinese epilogue_uv_ulya_cc66d7ef:


    uv "可能没有尾巴。"


translate chinese epilogue_uv_ulya_2a65d4fd:


    "她悲伤的说着。"


translate chinese epilogue_uv_ulya_a97d3385:


    me "不，不！我喜欢，别那样想!"


translate chinese epilogue_uv_ulya_5cd38101:


    "她没有说什么，只是闭上了眼睛。"


translate chinese epilogue_uv_ulya_412334c0:


    uv "那么在那里是什么感觉呢...?"


translate chinese epilogue_uv_ulya_8459d4e8:


    me "不太一样——我只能这么说了，有的东西要好一点，有的东西差一点，我们有互联网..."


translate chinese epilogue_uv_ulya_a026df63:


    "我笑起来。"


translate chinese epilogue_uv_ulya_1f38f517:


    uv "那我在那里可以做什么?"


translate chinese epilogue_uv_ulya_59625bdc:


    me "嗯...我不知道...车到山前必有路。"


translate chinese epilogue_uv_ulya_4eb5d154:


    uv "好吧..."


translate chinese epilogue_uv_ulya_ce617998_6:


    "…"


translate chinese epilogue_uv_ulya_c5c1cbec:


    "我们就这样坐了很长时间，讨论各种傻傻的事情。"


translate chinese epilogue_uv_ulya_21be8af1:


    "最后尤莉娅说:"


translate chinese epilogue_uv_ulya_c4f8d6ec:


    uv "咱们去睡觉吧!"


translate chinese epilogue_uv_ulya_805f21bb:


    me "嗯，时间正合适!"


translate chinese epilogue_uv_ulya_0c0452b3:


    "我感到一阵疲倦的浪潮。"


translate chinese epilogue_uv_ulya_43478fd5:


    uv "明天咱们该收拾东西了。"


translate chinese epilogue_uv_ulya_f89ed32e:


    me "但是我没有什么可收拾的。"


translate chinese epilogue_uv_ulya_e5d2ff76:


    uv "好吧，没什么可收拾的...但是你知道我...有！"


translate chinese epilogue_uv_ulya_e473fa2f:


    me "比如说?"


translate chinese epilogue_uv_ulya_8ffd00b8:


    "我善意的咯咯笑着。"


translate chinese epilogue_uv_ulya_00bce6ba:


    "我真的想象不出来她准备带什么。"


translate chinese epilogue_uv_ulya_4716ebb6:


    uv "比如说我冬天的储备！苹果！坚果！蘑菇!"


translate chinese epilogue_uv_ulya_779061ff:


    me "你真的觉得你需要那些东西吗?"


translate chinese epilogue_uv_ulya_bf9129e5:


    uv "当然了!"


translate chinese epilogue_uv_ulya_2cb5dbd1:


    "她跳起来严肃认真的看着我。"


translate chinese epilogue_uv_ulya_83ff6f89:


    uv "我要去睡觉了，你爱干什么干什么!"


translate chinese epilogue_uv_ulya_dde67e73:


    "尤莉娅说完精神抖擞的往辅导员的房间走去。"


translate chinese epilogue_uv_ulya_197dfa8a:


    me "嘿，我没有..."


translate chinese epilogue_uv_ulya_a0076c62:


    "我跟着她跑过去。"


translate chinese epilogue_uv_ulya_ce617998_7:


    "…"


translate chinese epilogue_uv_ulya_e426ad6a:


    "走进房间以后尤莉娅马上脱掉衣服，钻进被子里然后蒙上了脑袋。"


translate chinese epilogue_uv_ulya_beb6a4da:


    me "我没有那个意思，如果你想要带上自己的补给那就带上好了。"


translate chinese epilogue_uv_ulya_cf6c57e8:


    "没准那些东西还有用呢，我那种处境不一定会发生什么事。"


translate chinese epilogue_uv_ulya_e70508ab:


    uv "我只是开玩笑的!"


translate chinese epilogue_uv_ulya_122cb80a:


    "她偷偷探出头说着。"


translate chinese epilogue_uv_ulya_c41d257d:


    uv "现在——睡觉!"


translate chinese epilogue_uv_ulya_54256e31:


    "尤莉娅下达了指令。"


translate chinese epilogue_uv_ulya_b03955ac:


    "我不在乎，所以我脱掉衣服，躺在她的身边，我们相拥入睡..."


translate chinese epilogue_uv_ulya_3b5be134:


    "我做了一个奇怪的梦。"


translate chinese epilogue_uv_ulya_fe1efafe:


    "当然我在这个夏令营里就很奇怪了，已经比各种噩梦还要奇怪了。"


translate chinese epilogue_uv_ulya_57a861c9:


    "但是这个梦感觉这么真实..."


translate chinese epilogue_uv_ulya_6627fa0f:


    "然后我醒过来的时候却什么也想不起来。"


translate chinese epilogue_uv_ulya_ecbc37b5:


    "只有一点模糊的印象，一条长长的公路，老旧的生锈的汽车，摇动的营地大门，倒塌的少先队员雕塑，还有废弃的广场。"


translate chinese epilogue_uv_ulya_54f753f7:


    "我来回穿过小猫头鹰想要找什么人。但是我却不记得那到底是谁。"


translate chinese epilogue_uv_ulya_ef297c74:


    "我醒过来的时候，太阳已经开始落山了。"


translate chinese epilogue_uv_ulya_30bf0240:


    "一道光线照亮了屋子里的灰尘，映射出彩虹的各种颜色。"


translate chinese epilogue_uv_ulya_75ec2ab9:


    "草丛上的反射出现了各种轮廓：从一个角度看上去像是一点阳光，另一个角度看上去像是一个海盗挥动着自己的弯刀。"


translate chinese epilogue_uv_ulya_7e0b301d:


    "我懒洋洋的伸着懒腰打了个呵欠。"


translate chinese epilogue_uv_ulya_5bd995a8:


    "我转过身，看到尤莉娅在我身边平静的睡着。"


translate chinese epilogue_uv_ulya_85ea5c99:


    uv "早上好!"


translate chinese epilogue_uv_ulya_b5c033aa:


    "尤莉娅没有睁开眼睛对我说着。"


translate chinese epilogue_uv_ulya_658bead9:


    me "已经很晚了。"


translate chinese epilogue_uv_ulya_f7e9404b:


    uv "你醒来才叫早晨。"


translate chinese epilogue_uv_ulya_4bfd9907:


    me "就跟我原来一样。"


translate chinese epilogue_uv_ulya_4db5c195:


    uv "你现在不一样了?"


translate chinese epilogue_uv_ulya_42efed39:


    me "唔...不知道...我上个星期都是早晨起来的。"


translate chinese epilogue_uv_ulya_837b5c70:


    "这也不完全对，我也不是太阳升起就起床，而且经常错过早晨的集合。"


translate chinese epilogue_uv_ulya_f98565f1:


    uv "你想说你已经改变了?"


translate chinese epilogue_uv_ulya_d8e2c47f:


    "她笑着。"


translate chinese epilogue_uv_ulya_3c6b3cb2:


    me "我觉得不管是谁遇到我这种情况都会改变的，至少会有一点。"


translate chinese epilogue_uv_ulya_07fe3436:


    "对我来说很自然。"


translate chinese epilogue_uv_ulya_4514ec9d:


    uv "对啊，有可能，我不反对。"


translate chinese epilogue_uv_ulya_90c7a728:


    "接下来是一段长长的沉默。"


translate chinese epilogue_uv_ulya_82c716ab:


    "你十分想要说点什么，这种沉默简直是严刑。{w}沉默比得上任何话语。"


translate chinese epilogue_uv_ulya_eb401598:


    me "好吧，咱们今天要离开了?"


translate chinese epilogue_uv_ulya_2fd40082:


    "最后我问道。"


translate chinese epilogue_uv_ulya_ef2303bf:


    uv "我觉得是反正。"


translate chinese epilogue_uv_ulya_d68b5157:


    me "你觉得？难道你不确定？"


translate chinese epilogue_uv_ulya_6510370b:


    uv "不完全确定，这是头一次这样。"


translate chinese epilogue_uv_ulya_8f9e293b:


    me "你是说少先队员都消失什么的。"


translate chinese epilogue_uv_ulya_b6003605:


    uv "不只是这个..."


translate chinese epilogue_uv_ulya_ec3291c7:


    "她坐起来开始穿衣服。"


translate chinese epilogue_uv_ulya_0fbf0070:


    "我着迷的看着她，眼睛都不舍得转动一下。"


translate chinese epilogue_uv_ulya_836b34d3:


    uv "你看什么？快起来！该收拾东西了!"


translate chinese epilogue_uv_ulya_2c312632:


    me "你的浆果和蘑菇?"


translate chinese epilogue_uv_ulya_bf9129e5_1:


    uv "当然了!"


translate chinese epilogue_uv_ulya_6935bf62:


    "尤莉娅两手叉腰生气的看着我。"


translate chinese epilogue_uv_ulya_1bc3348a:


    uv "要不然我的补给都怎么办?"


translate chinese epilogue_uv_ulya_f7e824d2:


    me "我是不想..."


translate chinese epilogue_uv_ulya_acb58b15:


    "说实话我才不在乎。"


translate chinese epilogue_uv_ulya_e0aa82ad:


    "准确的说对于这种没有返程的旅行，储存那么多补给究竟有什么意义。{w}反正我们是没有回程票了。"


translate chinese epilogue_uv_ulya_ce617998_8:


    "…"


translate chinese epilogue_uv_ulya_cd0c429e:


    "过了几分钟我们又站在了相同的空地，一天以前尤莉娅在这里往蘑菇上撒糖。"


translate chinese epilogue_uv_ulya_2dad5b75:


    "她跑到一棵树后面，扒开枝叶，翻出来一个大包。"


translate chinese epilogue_uv_ulya_f3f023d1:


    me "这都是..."


translate chinese epilogue_uv_ulya_0877fc5a:


    uv "这还没完，这里只是最必须的。"


translate chinese epilogue_uv_ulya_6e778374:


    "我不敢讲出原来打算说的话，所以只好保持沉默。"


translate chinese epilogue_uv_ulya_e2dea26f:


    "包包里都是蘑菇，浆果，苹果，坚果，全都混在了一起。"


translate chinese epilogue_uv_ulya_a3d7b55b:


    "我很快开始质疑人类食用这类食物的安全性。"


translate chinese epilogue_uv_ulya_d9056b1a:


    uv "拿着。"


translate chinese epilogue_uv_ulya_1331401f:


    "尤莉娅轻松的笑着，一边指向一个包包。"


translate chinese epilogue_uv_ulya_59d43f12:


    me "那个，希望你明白我没法..."


translate chinese epilogue_uv_ulya_ece439a7:


    uv "不不，你很强壮，看看你多大!"


translate chinese epilogue_uv_ulya_7430ffbb:


    "她用手比划着她和我的身高差距。"


translate chinese epilogue_uv_ulya_018dab19:


    "我试图举起包包，感觉至少得有30公斤。"


translate chinese epilogue_uv_ulya_3e3eb9ad:


    me "那个我说，咱们拿一部分吧，反正也吃不了这么多。"


translate chinese epilogue_uv_ulya_dbcde99e:


    "我想要糊弄一下。"


translate chinese epilogue_uv_ulya_a0fad7c3:


    uv "为什么这些地方冬天很长的。"


translate chinese epilogue_uv_ulya_957d4b84:


    me "这你怎么知道的?"


translate chinese epilogue_uv_ulya_c5cc1a96:


    uv "我猜的。"


translate chinese epilogue_uv_ulya_b07beae2:


    "我露出愁眉苦脸的表情。"


translate chinese epilogue_uv_ulya_1d232d3e:


    me "我顶多可以扛到广场，可怜可怜我吧!"


translate chinese epilogue_uv_ulya_58312911:


    "尤莉娅思考了一下。"


translate chinese epilogue_uv_ulya_fd4177df:


    uv "那就扔掉一些浆果吧!"


translate chinese epilogue_uv_ulya_85411cb9:


    "这方法真好，把最轻的东西扔掉。"


translate chinese epilogue_uv_ulya_a6d25ea4:


    "然而我没有反驳。"


translate chinese epilogue_uv_ulya_eb11115c:


    me "我没感觉轻了多少。"


translate chinese epilogue_uv_ulya_c1d08cf6:


    "我弄完嘟囔着。"


translate chinese epilogue_uv_ulya_89f72f94:


    uv "我不管!"


translate chinese epilogue_uv_ulya_b51aad30:


    "我叹叹气，倒在袋子旁边。"


translate chinese epilogue_uv_ulya_6e976e33:


    me "那咱们轮流拿。"


translate chinese epilogue_uv_ulya_eb28f6df:


    uv "但是这个太重了!"


translate chinese epilogue_uv_ulya_62526242:


    "尤莉娅喊起来。"


translate chinese epilogue_uv_ulya_75383d27:


    me "我就说..."


translate chinese epilogue_uv_ulya_2de9ff55:


    uv "我说对我来说太重了。"


translate chinese epilogue_uv_ulya_8894ed1f:


    me "好像我就觉得多轻似的..."


translate chinese epilogue_uv_ulya_f848c25b:


    "我继续嘟囔着，没打算让她听见。"


translate chinese epilogue_uv_ulya_cf87a84d:


    uv "这对你就是很轻!"


translate chinese epilogue_uv_ulya_545fb987:


    "看来我不能嘀咕她的猫耳。"


translate chinese epilogue_uv_ulya_9744fa26:


    me "后咯，咱们等一会儿再来，现在先去搞点吃的!"


translate chinese epilogue_uv_ulya_a4e53ca8:


    uv "好吧... "


translate chinese epilogue_uv_ulya_de229a07:


    "她笑着对我伸出了手。"


translate chinese epilogue_uv_ulya_ce617998_9:


    "…"


translate chinese epilogue_uv_ulya_95c684b4:


    "食堂里充满了炸土豆的味道。"


translate chinese epilogue_uv_ulya_d7b798a9:


    "尤莉娅在我身边跳来跳去看着我做饭。"


translate chinese epilogue_uv_ulya_eb7752aa:


    "说实话我这样一点也不爽。"


translate chinese epilogue_uv_ulya_c91f29dc:


    "我只想马上吃到饭，不想费劲去准备材料，做水，包饺子..."


translate chinese epilogue_uv_ulya_1fed802b:


    "所以我总吃快餐还有三明治什么的。"


translate chinese epilogue_uv_ulya_d48b29e9:


    uv "你做得很好嘛!"


translate chinese epilogue_uv_ulya_4cb72b17:


    me "我跟你说这个没什么特殊的嘛。"


translate chinese epilogue_uv_ulya_fec78416:


    uv "但...但是..."


translate chinese epilogue_uv_ulya_576e6d7a:


    "她试图辩解。"


translate chinese epilogue_uv_ulya_f256b99d:


    uv "但是...反正你做的好!"


translate chinese epilogue_uv_ulya_ff60c702:


    me "你喜欢我很开心。"


translate chinese epilogue_uv_ulya_5e40bb44:


    uv "我当然喜欢了!"


translate chinese epilogue_uv_ulya_920e0bc4:


    "看看尤莉娅大快朵颐的样子，她可能真的喜欢我的厨艺。"


translate chinese epilogue_uv_ulya_31397184:


    uv "你担心吗?"


translate chinese epilogue_uv_ulya_482b5f41:


    me "先吃完再说话！"


translate chinese epilogue_uv_ulya_96b2f2bb:


    uv "鲁莽!"


translate chinese epilogue_uv_ulya_9e367860:


    me "你在相互尊敬的社会中要有礼貌。"


translate chinese epilogue_uv_ulya_442d1b23:


    uv "那我们这里是什么社会?"


translate chinese epilogue_uv_ulya_13170ccd:


    "尤莉娅摊开手笑着。"


translate chinese epilogue_uv_ulya_86f5a471:


    "对，食堂空空的就像是整个夏令营。"


translate chinese epilogue_uv_ulya_68468e81:


    me "你觉得呢，他们都去那儿了?"


translate chinese epilogue_uv_ulya_61bc63e5:


    uv "我上哪儿知道去..."


translate chinese epilogue_uv_ulya_164ed80e:


    "她叉起最后一块放到嘴里。"


translate chinese epilogue_uv_ulya_13a24fe6:


    uv "你想他们吗?"


translate chinese epilogue_uv_ulya_1d8c00d5:


    me "有点。"


translate chinese epilogue_uv_ulya_8f18ceca:


    uv "我呢，我可比流浪狗好多了!"


translate chinese epilogue_uv_ulya_21b90d49:


    "又引用了一句话，这次我没太注意。"


translate chinese epilogue_uv_ulya_081edf35:


    me "考虑到你是一只猫，你引用的很正确啊。"


translate chinese epilogue_uv_ulya_1072caf5:


    uv "你觉得孤独吗?"


translate chinese epilogue_uv_ulya_616608b4:


    me "唔...我不是一个人。"


translate chinese epilogue_uv_ulya_682c5cba:


    uv "那也只有咱们两个..."


translate chinese epilogue_uv_ulya_9237bb74:


    me "但是如果没有发生这些事情，咱们可能..."


translate chinese epilogue_uv_ulya_3a69bd1e:


    "我停了下来。"


translate chinese epilogue_uv_ulya_aebf8ccd:


    me "所以咱们在其他的世界里没有这么..."


translate chinese epilogue_uv_ulya_7a792120:


    uv "亲密?"


translate chinese epilogue_uv_ulya_a0ab92e9:


    "尤莉娅帮我说完了。"


translate chinese epilogue_uv_ulya_fc30fd2b_1:


    me "对。"


translate chinese epilogue_uv_ulya_55d14e50:


    uv "不是的。"


translate chinese epilogue_uv_ulya_4cb51946:


    me "奇怪。"


translate chinese epilogue_uv_ulya_fd80ccaf:


    uv "为什么?"


translate chinese epilogue_uv_ulya_e7a7b1ad:


    me "因为有很多个夏令营，按照你说的话，如果根据概率..."


translate chinese epilogue_uv_ulya_c508ce25:


    me "就是说过去的两天里发生的事情真的是不同寻常的..."


translate chinese epilogue_uv_ulya_ffa95805:


    uv "你说得好像坏事一样。"


translate chinese epilogue_uv_ulya_f837dd45:


    me "当然不是了!"


translate chinese epilogue_uv_ulya_c9b0b0be:


    "我感觉血液冲上大脑。"


translate chinese epilogue_uv_ulya_7651e2ab:


    me "就是奇怪，这里有很多奇怪的事情，所以我总用这个词，你有点太挑剔了。"


translate chinese epilogue_uv_ulya_0834f49a:


    uv "谁？我？不可能!"


translate chinese epilogue_uv_ulya_ce3666e5:


    "她笑了。"


translate chinese epilogue_uv_ulya_a3c453d6:


    me "没有，你真的很挑剔，尤其是说话的时候!"


translate chinese epilogue_uv_ulya_81f608cc:


    uv "我那样干什么?"


translate chinese epilogue_uv_ulya_48537224:


    me "我不知道，你自己应该更清楚。"


translate chinese epilogue_uv_ulya_47309df6:


    "好像不管我说什么她都有准备好的答案，好像她知道我要说什么似的。"


translate chinese epilogue_uv_ulya_457518f0:


    me "算了吧...咱们走吧。"


translate chinese epilogue_uv_ulya_fd5ba6b4:


    uv "去哪儿?"


translate chinese epilogue_uv_ulya_3096cd01:


    "我意识到自己绝对是说错了话，她肯定要拉着我回去拿那个大袋子。"


translate chinese epilogue_uv_ulya_149eedf2:


    me "那个，里公交车来这里还有很长时间，所以..."


translate chinese epilogue_uv_ulya_cc52b4f1:


    uv "啊，对了，我的补给!"


translate chinese epilogue_uv_ulya_a1d07ef9:


    "我挠着下巴跟着她，真是重大失败!"


translate chinese epilogue_uv_ulya_ce617998_10:


    "…"


translate chinese epilogue_uv_ulya_1c0485da:


    "扛着二十五公斤的东西可不轻松啊。"


translate chinese epilogue_uv_ulya_6712960c:


    "即使距离很短也一样。"


translate chinese epilogue_uv_ulya_c17bf6e0:


    "我隔五十米就要停下来休息一下。"


translate chinese epilogue_uv_ulya_75fc6814:


    "尤莉娅在一边兴奋的为我加油，简直是添乱。"


translate chinese epilogue_uv_ulya_0e135c3b:


    "我不知道自己是不是可以得一个“猫耳娘补给包短距运输比赛”的奖牌。不过过了半个小时这个大包还是终于运送到了公交车站。"


translate chinese epilogue_uv_ulya_087877b3:


    "我瘫倒在袋子旁边。"


translate chinese epilogue_uv_ulya_b0d3f9c0:


    me "行了..."


translate chinese epilogue_uv_ulya_e76aa14c:


    uv "你真是英雄!"


translate chinese epilogue_uv_ulya_72d66e35:


    "我浑身疼的难受，肌肉刺痛，额头的汗水流到眼睛里辣辣的。"


translate chinese epilogue_uv_ulya_fa5868f8:


    "我不知道自己应该得到什么奖，但是我感觉这个比赛就像是残奥会，而我是作为智力障碍人群参赛的。"


translate chinese epilogue_uv_ulya_fd9fe70f:


    uv "看，公交车来了!"


translate chinese epilogue_uv_ulya_3d342d5a:


    me "什么?!"


translate chinese epilogue_uv_ulya_c00b6b38:


    "我跳了起来，我的疲倦感瞬间消失了。"


translate chinese epilogue_uv_ulya_50978573:


    "确实远处有什么东西。"


translate chinese epilogue_uv_ulya_b941dcba:


    "我眯起眼睛看到了一辆Icarus。"


translate chinese epilogue_uv_ulya_bedf97cb:


    "可这是为什么?"


translate chinese epilogue_uv_ulya_42a3ce3a:


    "尤莉娅说得到了晚上才会来。"


translate chinese epilogue_uv_ulya_eb16fe39:


    "而且我以为是LiAZ大巴。"


translate chinese epilogue_uv_ulya_ce26c4df:


    "很快汽车停在我们面前打开了门。"


translate chinese epilogue_uv_ulya_6de31db1:


    "司机的位置是空空的，但是我也不是很惊讶，这里已经发生了很多奇怪的事情了。"


translate chinese epilogue_uv_ulya_13faf4dc:


    uv "来吗?"


translate chinese epilogue_uv_ulya_3a532c56:


    "尤莉娅轻松的跳了上去对我挥挥手。"


translate chinese epilogue_uv_ulya_a7b876ab:


    me "你不觉得这公交车无人驾驶很奇怪吗?"


translate chinese epilogue_uv_ulya_943ed485:


    uv "唔..."


translate chinese epilogue_uv_ulya_60f3d758_3:


    "她思考了一下。"


translate chinese epilogue_uv_ulya_0375339e:


    uv "当然，这是很奇怪，但是你都说了这里有很多奇怪的事情!"


translate chinese epilogue_uv_ulya_b1d8564c:


    "她这个说得对。"


translate chinese epilogue_uv_ulya_8dec57e5:


    "不管怎么说，我也没有选择。"


translate chinese epilogue_uv_ulya_6d318d6a:


    "不管我是上车还是留在这里我都不知道接下来会发生什么事情。"


translate chinese epilogue_uv_ulya_e63e6485:


    "但是上车的话我还至少有希望离开这里。"


translate chinese epilogue_uv_ulya_db41b274:


    "我叹叹气，用最后一口气把大袋子抬上汽车，然后自己也爬了上来。"


translate chinese epilogue_uv_ulya_ce617998_11:


    "…"


translate chinese epilogue_uv_ulya_e6421b37:


    "我们做了大概半个小时。"


translate chinese epilogue_uv_ulya_43dae48d:


    "尤莉娅一直嘟囔不停，但是我并没有注意听。"


translate chinese epilogue_uv_ulya_ca1945f3:


    "我仔细的看着路过的景色。{w} 森林，田野，河流，森林，田野......完全没有什么可疑的。"


translate chinese epilogue_uv_ulya_f8021fd2:


    "一方面这让我充满希望，但是另一方面也可能说明一切都不会变化，这个世界好像一切都那么慢，有自己的规则。"


translate chinese epilogue_uv_ulya_302508d2:


    "至少几天以前是这样的。"


translate chinese epilogue_uv_ulya_a66cfed7:


    "虽然说我只待了一个礼拜就得出结论不太好..."


translate chinese epilogue_uv_ulya_ec0eddf1:


    uv "好了！现在我确定一切都会好起来了!"


translate chinese epilogue_uv_ulya_8f838576_2:


    "我认真的看着她。"


translate chinese epilogue_uv_ulya_6a4e1b53:


    "一对猫耳，长长的尾巴，有节奏的前后摆动着，稍有点长的尖牙，尤莉娅就像是童话故事里的人物。"


translate chinese epilogue_uv_ulya_ad15388e:


    "我们可以在到了现实世界以后再讨论她怎么办。"


translate chinese epilogue_uv_ulya_1eeff60a:


    "我好像变成了跳动的血管，随时都有可能爆裂开。"


translate chinese epilogue_uv_ulya_a487c0e8:


    "我等待着...等待着什么时候这一切都会迎来结束的时候。"


translate chinese epilogue_uv_ulya_8bce2f18:


    "结果还不一定是怎么样的，对我来说最重要的是不要在这之前就发疯。"


translate chinese epilogue_uv_ulya_2cfc3b5a:


    "我已经几乎紧张到极限了。"


translate chinese epilogue_uv_ulya_61cf1417:


    "但是自己又没有办法分散注意力。"


translate chinese epilogue_uv_ulya_8bb8720a:


    uv "嘿！你在听我说话吗?"


translate chinese epilogue_uv_ulya_7401b3c5:


    me "哈？抱歉，我走神了..."


translate chinese epilogue_uv_ulya_da7fd880:


    uv "哼，一直都这样!"


translate chinese epilogue_uv_ulya_366345bd:


    "她撅起嘴。"


translate chinese epilogue_uv_ulya_e11c2266:


    me "现在是关键时刻，你知道吗...咱们可以离开这个夏令营，所以对我来说是一个关键的时刻。"


translate chinese epilogue_uv_ulya_b2811648:


    uv "唔，对我来说也是一个关键的时刻，但是我一点也不担心。"


translate chinese epilogue_uv_ulya_d44a746c:


    me "你这可不能比。"


translate chinese epilogue_uv_ulya_6afd4281:


    uv "什么意思?"


translate chinese epilogue_uv_ulya_b9ed7480:


    me "我曾经住在一个完全不同的世界里，所以我有自己的生活拿来和这里对比，但是你可以说就是出生在这里的。"


translate chinese epilogue_uv_ulya_f5055106:


    uv "没有啊，我不是!"


translate chinese epilogue_uv_ulya_cfc51cbb:


    me "你怎么知道的?"


translate chinese epilogue_uv_ulya_3a5cf8cd:


    uv "我就是知道，我知道那个..."


translate chinese epilogue_uv_ulya_ce617998_12:


    "…"


translate chinese epilogue_uv_ulya_a850329e:


    "公交车经过路中的坑，不停颠簸，让我开始颤抖。"


translate chinese epilogue_uv_ulya_4512f1df:


    "方向盘自动的转着，很快我就不再注意没有司机这件事了。"


translate chinese epilogue_uv_ulya_ad87b7fb:


    "我的眼睛不自觉的闭上了..."


translate chinese epilogue_uv_ulya_5fabbbed:


    "有个女孩儿在我面前弯下腰。"


translate chinese epilogue_uv_ulya_02d2b574:


    "她温柔的对我耳语着什么..."


translate chinese epilogue_uv_ulya_4fc9e3f4:


    "我感觉头上被猛击了一下。"


translate chinese epilogue_uv_ulya_53a6f078:


    uv "嘿，别停下!"


translate chinese epilogue_uv_ulya_ba101d53:


    me "我打扰到你了吗?"


translate chinese epilogue_uv_ulya_2d838a38:


    "我心烦意乱的揉着脸问道。"


translate chinese epilogue_uv_ulya_8e590003:


    uv "你没听我说话!"


translate chinese epilogue_uv_ulya_f7b19f66:


    me "如果咱们逃离这里的话会有很多时间聊天的。"


translate chinese epilogue_uv_ulya_b61f7d2c:


    uv "如...如果没有逃出去呢?"


translate chinese epilogue_uv_ulya_f4a33a5c:


    me "那咱们时间就更多了。"


translate chinese epilogue_uv_ulya_5db31537:


    "我想笑笑，但是想到自己可能离不开这里很可怕，结果只是傻子一样咧了咧嘴。"


translate chinese epilogue_uv_ulya_13b1a056:


    uv "如果咱们不逃走就更好了，我不知道在你的世界里有什么，但是你肯定不会再注意我了!"


translate chinese epilogue_uv_ulya_959ccee3:


    me "不会那样的，我保证!"


translate chinese epilogue_uv_ulya_67844860:


    "我真诚的抱住她。"


translate chinese epilogue_uv_ulya_7853c29e:


    uv "真的真的吗?"


translate chinese epilogue_uv_ulya_c7d80c45:


    me "真的是真的!"


translate chinese epilogue_uv_ulya_ae142e7e:


    "尤莉娅靠着我。"


translate chinese epilogue_uv_ulya_134e4880:


    uv "你会听我的话吗?"


translate chinese epilogue_uv_ulya_771aa502:


    me "当然!"


translate chinese epilogue_uv_ulya_9eb6accf:


    uv "你会永远听我的吗?"


translate chinese epilogue_uv_ulya_8288e06d:


    me "当然!"


translate chinese epilogue_uv_ulya_06fb0086:


    "她坐好，直直的看着我的眼睛。"


translate chinese epilogue_uv_ulya_9e863299:


    uv "那好吧，我原谅你!"


translate chinese epilogue_uv_ulya_4e48f9de:


    "公交车里充满了欢笑声。"


translate chinese epilogue_uv_ulya_c74b3c37:


    uv "但是你可要小心了，别想骗我喔!"


translate chinese epilogue_uv_ulya_45c72629:


    me "你想要签一个协议吗?"


translate chinese epilogue_uv_ulya_63cdfc3b:


    uv "婚姻契约吗?"


translate chinese epilogue_uv_ulya_7b2837ac:


    me "我反正又没有那些财产..."


translate chinese epilogue_uv_ulya_1c13dd09:


    uv "但是我有!"


translate chinese epilogue_uv_ulya_55396f10:


    "尤莉娅指指地上的大包。"


translate chinese epilogue_uv_ulya_621c7bd3:


    me "那好吧..."


translate chinese epilogue_uv_ulya_c9689908:


    "我试图回想起自己买过什么东西。"


translate chinese epilogue_uv_ulya_b16647b1:


    me "这个!"


translate chinese epilogue_uv_ulya_96266e14:


    "我掏出自己的手机，但是已经没电了。"


translate chinese epilogue_uv_ulya_b3557c43:


    uv "这什么?"


translate chinese epilogue_uv_ulya_b0ef2520:


    "尤莉娅拿起来好奇的玩着。"


translate chinese epilogue_uv_ulya_589cf2f8:


    me "在我的世界里很有用的东西，你可以和很远地方的人对话。"


translate chinese epilogue_uv_ulya_63b9af07:


    uv "哇，那很好，这个可以!"


translate chinese epilogue_uv_ulya_bd622277:


    "她把手机放在座位上。"


translate chinese epilogue_uv_ulya_90bff781:


    uv "成交？我的冬天补给还有你的...那个!"


translate chinese epilogue_uv_ulya_01822020_1:


    me "成交。"


translate chinese epilogue_uv_ulya_4a22ceaf:


    uv "那么，现在咱们就是夫妇了!"


translate chinese epilogue_uv_ulya_945c59bc:


    "她顽皮的笑着。"


translate chinese epilogue_uv_ulya_3118ae12:


    me "嘿，等等..."


translate chinese epilogue_uv_ulya_d41d1cae:


    "我突然想起来这段对话开始说的是婚姻契约。"


translate chinese epilogue_uv_ulya_8d32181d:


    me "不管怎么说，没有官方认证..."


translate chinese epilogue_uv_ulya_934db5ab:


    uv "到了你的世界我们就去官方记录...那什么男人没有老婆就像是过年没有饺子..."


translate chinese epilogue_uv_ulya_f5e62b0b:


    me "如果咱们到了那里，当然..."


translate chinese epilogue_uv_ulya_a540e1eb:


    "尤莉娅没说什么，只是睁开眼睛往前看着。"


translate chinese epilogue_uv_ulya_9a70d2e5:


    "我跟着她的目光，然后发现公交车里充满了人!"


translate chinese epilogue_uv_ulya_08aceedd:


    "他们都穿着少先队制服，所有人看起来都跟那个少先队员一样。"


translate chinese epilogue_uv_ulya_b9a82855:


    "没有人说话，他们都看着我。"


translate chinese epilogue_uv_ulya_0638a60a:


    "我看不见他们的脸，他们的衣服可以看见，但是脸不知道哪里来了一道阴影巧妙的都给罩住了。"


translate chinese epilogue_uv_ulya_c09707fb:


    "好像他们不像一般人有脑袋，相应的那个地方有一个黑洞。"


translate chinese epilogue_uv_ulya_131aff14:


    "我开始颤抖，背后开始起鸡皮疙瘩。"


translate chinese epilogue_uv_ulya_1d2d7a9b:


    "尤莉娅攥着我的手。"


translate chinese epilogue_uv_ulya_71296442:


    "我用很大的力气才回头看她。"


translate chinese epilogue_uv_ulya_e0da3ec4:


    "她也很害怕。"


translate chinese epilogue_uv_ulya_13859e61:


    "如果我只是自己的话可能就要昏过去了吧，但是现在我还有自己的责任。"


translate chinese epilogue_uv_ulya_2c2b16f4:


    me "你...是...谁..."


translate chinese epilogue_uv_ulya_4569b623_1:


    "我艰难的咬着牙。"


translate chinese epilogue_uv_ulya_9074e15e:


    "少先队员依旧可怕的保持着沉默。"


translate chinese epilogue_uv_ulya_d76ff1af:


    "我感觉难以呼吸，我使劲吸气，但是大脑还是缺氧，结果自己昏过去一段时间。"


translate chinese epilogue_uv_ulya_ede926b3:


    uv "你是谁？！你想要什么？！"


translate chinese epilogue_uv_ulya_60f8aa06:


    "我回复意识以后听到尤莉娅在大喊。"


translate chinese epilogue_uv_ulya_2b71358d:


    "坐在第一排的一个少先队员走过来。"


translate chinese epilogue_uv_ulya_8de32484:


    pi "你好谢苗，我们也决定回家去。"


translate chinese epilogue_uv_ulya_2dbb2a67:


    all "对...对..."


translate chinese epilogue_uv_ulya_bbf8617d:


    "公交车里异口同声。"


translate chinese epilogue_uv_ulya_b2f2241f:


    pi "我们会陪着你。"


translate chinese epilogue_uv_ulya_faae32c5:


    "我不知道说什么好。"


translate chinese epilogue_uv_ulya_43efcd48:


    "这是我在这里遇到的最奇怪的情况。"


translate chinese epilogue_uv_ulya_a067eaab:


    "不管是尤莉娅还是那个城市，还是消失的少先队员还是面前站着的这个奇怪的人都比不上这个..."


translate chinese epilogue_uv_ulya_af6577bb:


    uv "我问你问题了!"


translate chinese epilogue_uv_ulya_b9e0f9fd:


    "尤莉娅好像还能正常的说出话来。{w}可以说她还是特殊的生物。"


translate chinese epilogue_uv_ulya_9ce8487e:


    pi "我重复一遍，我们只是回家去。"


translate chinese epilogue_uv_ulya_5913ba9c:


    "他的声音听起来很疯狂。"


translate chinese epilogue_uv_ulya_ecaa6f59:


    pi "我们打扰到你了吗?"


translate chinese epilogue_uv_ulya_7cde2b94:


    all "打扰...打扰..."


translate chinese epilogue_uv_ulya_116fe00f:


    uv "你想要什么?"


translate chinese epilogue_uv_ulya_728d6df5:


    pi "什么也不要。"


translate chinese epilogue_uv_ulya_09008b6b:


    all "不要...不要..."


translate chinese epilogue_uv_ulya_9f89e0ef:


    uv "我不相信你！你肯定有自己的目的!"


translate chinese epilogue_uv_ulya_021210b3:


    pi "你想多了，小动物!"


translate chinese epilogue_uv_ulya_6bd9eaf0:


    uv "我是动物，你是什么，鬼？影子？"


translate chinese epilogue_uv_ulya_1ee124eb:


    pi "也许是，但是鬼也有自己权利。"


translate chinese epilogue_uv_ulya_d438c040:


    uv "你有权滚开!"


translate chinese epilogue_uv_ulya_f5fb328d:


    "尤莉娅的声音震得我发晕，但是少先队员们都没有反应。"


translate chinese epilogue_uv_ulya_98717eb9:


    pi "看来我们会留下来。"


translate chinese epilogue_uv_ulya_955f7cb8:


    "他恶毒的说。"


translate chinese epilogue_uv_ulya_8e29f23e:


    me "但是你是怎么？还有其他人..."


translate chinese epilogue_uv_ulya_ee804b19:


    "最后我终于鼓足勇气说了出来。"


translate chinese epilogue_uv_ulya_21629221:


    pi "很简单，我的朋友！集体意识？听说过吗？"


translate chinese epilogue_uv_ulya_7b70b2c1:


    me "这...怎么了?"


translate chinese epilogue_uv_ulya_24b9de6e:


    pi "每个人都想同时祝你好运，就是这样。"


translate chinese epilogue_uv_ulya_e60d1472:


    all "好运... 好运..."


translate chinese epilogue_uv_ulya_b80d896b:


    pi "可以说我们就是因此存在。"


translate chinese epilogue_uv_ulya_9fab6928:


    me "但是接下来会发生什么?"


translate chinese epilogue_uv_ulya_8da302f0:


    pi "说实话我不知道。"


translate chinese epilogue_uv_ulya_c8ba98b9:


    uv "出去!"


translate chinese epilogue_uv_ulya_f00f1e41:


    "尤莉娅喊了起来。"


translate chinese epilogue_uv_ulya_7f35eb53:


    pi "冷静，你——冷静!"


translate chinese epilogue_uv_ulya_94f08d02:


    pi "你看..."


translate chinese epilogue_uv_ulya_fbc96e29:


    "他又提到了我。"


translate chinese epilogue_uv_ulya_d743b5e5:


    pi "我不想隐瞒，你大有机会离开这里，我不知道为什么，但是你打破了循环，准确的说是所有人的循环。所以我们有机会不再循环这七天的故事。"


translate chinese epilogue_uv_ulya_db8702b3:


    uv "他们不会循环，只是会死掉!"


translate chinese epilogue_uv_ulya_f3e7b92e:


    pi "没那么简单，我们为什么不能自我保护?"


translate chinese epilogue_uv_ulya_6747b594:


    pi "可以，对吧?"


translate chinese epilogue_uv_ulya_a3c4358b:


    "他问着少先队员们。"


translate chinese epilogue_uv_ulya_11042548:


    all "可以...可以..."


translate chinese epilogue_uv_ulya_7d1b8fb3:


    "我听到他们的回应。"


translate chinese epilogue_uv_ulya_fdc1027f:


    pi "所以我们决定陪着你。"


translate chinese epilogue_uv_ulya_d3565aac:


    pi "现在咱们可能会一起离开，或者一起消失，或者一起重新开始循环。"


translate chinese epilogue_uv_ulya_71af10ce:


    uv "万一我们就因你死掉?!"


translate chinese epilogue_uv_ulya_96f68338:


    pi "你不会自己死的，我们光荣的死去。"


translate chinese epilogue_uv_ulya_861f9a54:


    all "光荣...光荣..."


translate chinese epilogue_uv_ulya_57fefea4:


    pi "还是说你觉得我们的生命..."


translate chinese epilogue_uv_ulya_446df447:


    "他笑了..."


translate chinese epilogue_uv_ulya_f303d177:


    pi "嗯，如果我们叫生命的话..."


translate chinese epilogue_uv_ulya_a123f077:


    all "生命...生命..."


translate chinese epilogue_uv_ulya_651e05d0:


    pi "他们不如你重要吗?"


translate chinese epilogue_uv_ulya_ec2d2644:


    "尤莉娅不知道怎么回答。"


translate chinese epilogue_uv_ulya_20807278:


    pi "你不要以为我们全都在这里了，其实大部分这里装不下但是也都可以回去了。"


translate chinese epilogue_uv_ulya_5d22fc9e:


    uv "你... 你..."


translate chinese epilogue_uv_ulya_88523b73:


    "尤莉娅咬着牙。"


translate chinese epilogue_uv_ulya_f501ead7:


    pi "太晚了，我建议你冷静下来，享受这趟旅程。一会儿会有服务人员端上水果。"


translate chinese epilogue_uv_ulya_2b07621f:


    "他阴险的笑着，返回了他的位置。"


translate chinese epilogue_uv_ulya_3d9eb82e:


    "尤莉娅跳了起来，但是我制止了她。"


translate chinese epilogue_uv_ulya_83d116b4:


    uv "让我过去！我要干掉他!"


translate chinese epilogue_uv_ulya_c6ddc0c1:


    me "没必要...你能做什么？能有什么好处？"


translate chinese epilogue_uv_ulya_6e1e8524:


    "她惊讶的看着我，然后坐下了。"


translate chinese epilogue_uv_ulya_30c0cbc9:


    uv "所以呢，咱们就这样坐以待毙?"


translate chinese epilogue_uv_ulya_e2f12d3a:


    me "要不还能做什么?"


translate chinese epilogue_uv_ulya_9cc2138c:


    uv "我不知道，但是咱们不能就这样一直等着..."


translate chinese epilogue_uv_ulya_a77d3972:


    me "营地里有一件事我可以确定——很少有东西是我可以决定的。实际上完全没有我能决定的事情。尤其是现在。尤其是在这里。我不知道怎么让他们离开，但是我都知道，这样做了也不会有什么改变。"


translate chinese epilogue_uv_ulya_a78021fb:


    uv "可能你说得对吧..."


translate chinese epilogue_uv_ulya_c9107c2d:


    "尤莉娅悲伤的说着。"


translate chinese epilogue_uv_ulya_065ddf5d:


    me "我们只能等等，不会很久的..."


translate chinese epilogue_uv_ulya_476454b2:


    "她靠着我的肩膀。"


translate chinese epilogue_uv_ulya_ce617998_13:


    "…"


translate chinese epilogue_uv_ulya_46ee7ab5:


    "我们已经走了很久，好像永远会这样走下去。{w}太阳已经落山了，夜晚已经降临。"


translate chinese epilogue_uv_ulya_f2b3c332:


    "我感觉度日如年。"


translate chinese epilogue_uv_ulya_1d001f7b:


    "少先队员们保持着死一般的寂静，那个十分傲慢的人都没有开始说话。"


translate chinese epilogue_uv_ulya_63c82e69:


    "“大音希声”？！"


translate chinese epilogue_uv_ulya_a6d65f6d:


    "只要轻轻的拍拍手都能让我的鼓膜爆裂。"


translate chinese epilogue_uv_ulya_27bb93cc:


    "我和尤莉娅也这样沉默着。"


translate chinese epilogue_uv_ulya_b8a45f9a:


    "今天什么都说过了，没有什么别的了。"


translate chinese epilogue_uv_ulya_8a17dcd9:


    "我感觉十分空虚，我的头像是灌了铅，我好像已经死了一样，但是上天已经抛弃了我，所以我永恒的时间只能在这里和这些少先队员一起度过。"


translate chinese epilogue_uv_ulya_d8530ed5:


    "他们真的都是我的难兄难弟吗?"


translate chinese epilogue_uv_ulya_ee366b28:


    "他们也一样来到这里待一个礼拜然后离开吗...?{w}然后他们也重新开始...?"


translate chinese epilogue_uv_ulya_be5a413d:


    "也许对我来说也一样?"


translate chinese epilogue_uv_ulya_4231c343:


    "一切都有可能，现在一切都有可能了..."


translate chinese epilogue_uv_ulya_ce617998_14:


    "…"


translate chinese epilogue_uv_ulya_79c1defa:


    "我听到尤莉娅轻轻的呼噜声，她睡着了。"


translate chinese epilogue_uv_ulya_7ac9cad9:


    "她一定压力太大了。"


translate chinese epilogue_uv_ulya_bbfcefb4:


    "也可能她大喊的时候太累了。"


translate chinese epilogue_uv_ulya_6a4ba34d:


    "不管怎么说，在睡着的时候迎来自己的结局还是不错的。"


translate chinese epilogue_uv_ulya_8c9884a6:


    "现在我已经确定这就是结局了。"


translate chinese epilogue_uv_ulya_413ab6f9:


    "在悬崖上被敌人重重包围之时还以为相信着希望实在是没有意义。"


translate chinese epilogue_uv_ulya_c1ca73df:


    "即使真的有很小的机会获救也不会去想的。"


translate chinese epilogue_uv_ulya_9ad0b36a:


    "也许我这样也不对，换个人也能比我更乐观也说不定..."


translate chinese epilogue_uv_ulya_9d4ef48c:


    "我只是需要走出最后一步。"


translate chinese epilogue_uv_ulya_1802ad39:


    "但是为什么呢？这里没有楼梯，只有电梯，随时都有可能带我前往结局。"


translate chinese epilogue_uv_ulya_e8c2c59c:


    "我闭上眼睛，然后马上失去了意识。"


translate chinese epilogue_uv_ulya_3d844e6d:


    "我不知道这会持续一个小时还是持续到永远..."


translate chinese epilogue_uv_ulya_1c9980b4:


    "空虚大概不太适合思考。"


translate chinese epilogue_uv_ulya_e6517a54:


    "什么都没有的时候，世界没有了存在的意义，意识也变得虚无缥缈。"


translate chinese epilogue_uv_ulya_c43fb6ec:


    "很难想象到底什么时候才会结束，人类可能永远无法发现。"


translate chinese epilogue_uv_ulya_040c5913:


    "我在黑暗中恢复了意识，我重新感觉到自己是一个人，周围什么都没有，不管是周围还是我的内心。"


translate chinese epilogue_uv_ulya_c8cdb262:


    "我好像从宇宙中消失了，但是我没有死，和死掉完全不一样，我好像从三维世界回到了一维世界，整个世界收缩成了一个点。"


translate chinese epilogue_uv_ulya_cd386160:


    "那个点就是我，就是一切，我什么都是，什么也不是，然而我的意识，我的记忆，我的灵魂也都收缩成了一点。"


translate chinese epilogue_uv_ulya_05274ca7:



    nvl clear
    "我不害怕，可能是没有必要，可能是我还没有理解。"


translate chinese epilogue_uv_ulya_c8872b3a:


    "这里没有时间的概念。"


translate chinese epilogue_uv_ulya_8f5b6bf9:


    "所以我无法计算出自己已经在这种空虚中待了多长时间，然后过了一个小时或者是一年，我听到了轻轻的声音。我听不清具体说了什么，但是我确定是对我说的。"


translate chinese epilogue_uv_ulya_592b8e1f:


    bush "谢苗... 谢苗..."


translate chinese epilogue_uv_ulya_e79d0fd5:


    me "喂?"


translate chinese epilogue_uv_ulya_051cae6f:


    "我像打电话一样回答着。"


translate chinese epilogue_uv_ulya_20edba06:


    bush "见到你很高兴。"


translate chinese epilogue_uv_ulya_a66b5a14:


    "这声音好像离我很近。"


translate chinese epilogue_uv_ulya_e7620f07:



    nvl clear
    me "…"


translate chinese epilogue_uv_ulya_0f765061:


    bush "其实咱们已经认识很久了。"


translate chinese epilogue_uv_ulya_0b34c373_1:


    me "你是谁?"


translate chinese epilogue_uv_ulya_336dcb9a:


    bush "我就是你。"


translate chinese epilogue_uv_ulya_5ff017df:


    me "我死了吗...?"


translate chinese epilogue_uv_ulya_58a18029:


    bush "没有，不是那样的!"


translate chinese epilogue_uv_ulya_380cc324:


    "这声音好像很沉着，像是和老朋友谈话一样。"


translate chinese epilogue_uv_ulya_2de7bf97:


    me "我怎么了?"


translate chinese epilogue_uv_ulya_60e86f67:


    bush "你走到了尽头。"


translate chinese epilogue_uv_ulya_a7f92d54:


    me "什么走到了尽头?"


translate chinese epilogue_uv_ulya_6b49f2aa:


    bush "你成功的离开了夏令营。"


translate chinese epilogue_uv_ulya_aa801636:



    nvl clear
    "夏令营...对了，我想起来了，小猫头鹰夏令营，我突然想起了过去的七天，我想起自己和尤莉娅一起离开。"


translate chinese epilogue_uv_ulya_72917697:


    me "那这里是..."


translate chinese epilogue_uv_ulya_66bc60d2:


    bush "尤莉娅?"


translate chinese epilogue_uv_ulya_fc30fd2b_2:


    me "对。"


translate chinese epilogue_uv_ulya_e4066547:


    bush "她就在这里。"


translate chinese epilogue_uv_ulya_6af67ff0:


    "我往四周看看，但是什么也没有。"


translate chinese epilogue_uv_ulya_dfae19f4:


    me "我看不见她。"


translate chinese epilogue_uv_ulya_31083230:


    bush "你不能去看，你应该去感觉。"


translate chinese epilogue_uv_ulya_98f21fc2:


    "我一点也不紧张，好像被麻醉了。"


translate chinese epilogue_uv_ulya_fb31367b:



    nvl clear
    me "我...什么也...感觉不到。"


translate chinese epilogue_uv_ulya_44de386e:


    bush "你只需记起来..."


translate chinese epilogue_uv_ulya_be28a3d4:


    "说完，几千幅图片，影像，味道，思想，逐渐的浮现。夏令营！我确实在那里！还有斯拉维娅，列娜，阿丽夏，乌里扬娜，玛莎...然后呢？可是这是怎么回事？我记得自己回到了真实世界，然后我在公交车站遇到了斯拉维娅...不对，我在大学里遇到了乌里扬娜...不对，我在音乐会上遇到了阿丽夏！...但是我已经和列娜生活了很多年...不，我曾经和玛莎经历了完全不同的人生，这怎么可能，我回忆起了五次人生的具体细节。但是一切都是同时发生的... 我也确定是真实的!"


translate chinese epilogue_uv_ulya_63fe795d:



    nvl clear
    me "怎么...?"


translate chinese epilogue_uv_ulya_243def4b:


    bush "对啊，这些都发生在你的身上。"


translate chinese epilogue_uv_ulya_04098bfe:


    me "可是到底是怎么?"


translate chinese epilogue_uv_ulya_b9df3187:


    bush "你离开了夏令营，不止这一次，然后你回到了你自己普通的生活，然后又回到夏令营..."


translate chinese epilogue_uv_ulya_d749d691:


    me "所以说是我死掉了然后又复活了吗?"


translate chinese epilogue_uv_ulya_e7832e23:


    bush "不不，可以说这些都是同时发生的。"


translate chinese epilogue_uv_ulya_3aad82ef:


    me "我不明白..."


translate chinese epilogue_uv_ulya_a1dd0efd:


    bush "不必明白。"


translate chinese epilogue_uv_ulya_dd924d72:


    me "那些少先队员们都是什么人?"


translate chinese epilogue_uv_ulya_bb23dee2:


    bush "那些人也是你。"


translate chinese epilogue_uv_ulya_51149e68:


    me "…"


translate chinese epilogue_uv_ulya_3beec0f4:


    bush "你的其他面。"


translate chinese epilogue_uv_ulya_d3bd0899_2:


    me "我不明白。"


translate chinese epilogue_uv_ulya_f0aeb1ea:



    nvl clear
    bush "每个人都是无穷的宇宙。你记得这五种人生因为它们都是你自己做的选择，那些你只是做了不同的选择，就是这么简单!"


translate chinese epilogue_uv_ulya_3ad789cf:


    "我感觉没这么简单。"


translate chinese epilogue_uv_ulya_dc739d10:


    me "那这个夏令营是什么?"


translate chinese epilogue_uv_ulya_50953e17:


    bush "你可以认为是试验场。"


translate chinese epilogue_uv_ulya_0c39b064:


    me "那你是谁?"


translate chinese epilogue_uv_ulya_eb217c40:


    bush "我就是你，是很久以后的你。我在你的人生之上，可以说是最高的谢苗。"


translate chinese epilogue_uv_ulya_82187263:


    me "你就是这一切的原因?"


translate chinese epilogue_uv_ulya_85323ec4:


    bush "不完全正确，这一切不都是因为我才发生的，但是我可以控制这些事件。"


translate chinese epilogue_uv_ulya_b1002e1e:


    me "可是为什么...?"


translate chinese epilogue_uv_ulya_8d4e756e:


    bush "你难道反对吗?"


translate chinese epilogue_uv_ulya_8ec353e5:


    "我陷入了沉思，进入了模糊的状态。"


translate chinese epilogue_uv_ulya_7479b07e_2:


    me "我不知道..."


translate chinese epilogue_uv_ulya_ec67e222:



    nvl clear
    bush "如果你选择可以选择，在你所有事情都记得的时候，你愿意回到一开始忘掉一切吗?"


translate chinese epilogue_uv_ulya_7479b07e_3:


    me "我不知道..."


translate chinese epilogue_uv_ulya_b81c8065:


    "我还没准备好回答这么困难的问题。"


translate chinese epilogue_uv_ulya_f9d2f152:


    me "不管怎么说，尤莉娅在哪儿?"


translate chinese epilogue_uv_ulya_e4066547_1:


    bush "她就在这里。"


translate chinese epilogue_uv_ulya_de833a1f:


    me "哪里?"


translate chinese epilogue_uv_ulya_273c343b:


    bush "在你身体里面。"


translate chinese epilogue_uv_ulya_05dc229b:


    me "那是什么?"


translate chinese epilogue_uv_ulya_f7dbb331:


    bush "尤莉娅是你的内心，你的意识形成了一个具象。"


translate chinese epilogue_uv_ulya_ad9639d3:


    "我无法理解他的话。"


translate chinese epilogue_uv_ulya_40ec5fc2:



    nvl clear
    bush "你不用理解，我已经说了。"


translate chinese epilogue_uv_ulya_a337cbd5_1:


    me "但是..."


translate chinese epilogue_uv_ulya_7b47e97d:


    "我感觉我想念她。"


translate chinese epilogue_uv_ulya_017e0287:


    bush "别害怕，她会一直和你在一起的。"


translate chinese epilogue_uv_ulya_ea951d31:


    "我觉得我昨天还不会希望这样呢。"


translate chinese epilogue_uv_ulya_c7814762:


    bush "最难的事情是做到内心的平静，而不是和敌人之间的和平，而你做到了，如果你想要爱她，首先要会爱自己。"


translate chinese epilogue_uv_ulya_51149e68_1:


    me "…"


translate chinese epilogue_uv_ulya_4f767ea3:


    bush "好了，差不多了，我该离开了。"


translate chinese epilogue_uv_ulya_3d6b4645:


    me "等等！会发生什么?"


translate chinese epilogue_uv_ulya_3c28c280:


    bush "你会回去。"


translate chinese epilogue_uv_ulya_08b0cbb8:


    "我已经不明白什么是真的什么不是了，到底有多少个世界..."


translate chinese epilogue_uv_ulya_25f7abbc:


    me "去哪儿?"


translate chinese epilogue_uv_ulya_73290988:


    bush "这只决定于你自己。"


translate chinese epilogue_uv_ulya_3aad82ef_1:


    me "我不明白。"


translate chinese epilogue_uv_ulya_e8aa7b7a:


    bush "现在你要记住所有不同的可能。"


translate chinese epilogue_uv_ulya_040643f7:



    nvl clear
    "他停下来很长时间，我没敢说话，怕不小心打断到他。"


translate chinese epilogue_uv_ulya_1813dbe0:


    bush "所以你至少有五个选项。"


translate chinese epilogue_uv_ulya_ca002c69:


    me "我会...都记得吗?"


translate chinese epilogue_uv_ulya_05b44d12:


    bush "那要你自己决定。"


translate chinese epilogue_uv_ulya_67c8ae4f:


    "他的话里有了一点情感，可能是欣喜，也可能是不满。"


translate chinese epilogue_uv_ulya_2569d811:


    bush "别了。"


translate chinese epilogue_uv_ulya_a337cbd5_2:


    me "可是..."


translate chinese epilogue_uv_ulya_76b858e0:


    "我感觉这个声音可能再也不会和我说话了。"


translate chinese epilogue_uv_ulya_76b2fe88:


    nvl clear


translate chinese epilogue_uv_ulya_ce617998_15:


    "…"


translate chinese epilogue_uv_ulya_8011f019:


    "时间不断的流逝，我必须自己决定，如果真的像他说的一样，我只能祈祷了。"


translate chinese epilogue_uv_ulya_9f15618d:


    "我已经完全冷静下来，你甚至可以说是打起精神来了，我确定我已经离开了那个被诅咒的夏令营，而且我的前方是光明的未来，我不知道，但是我记得！"


translate chinese epilogue_uv_ulya_40f78451:


    "但是我应该怎么选择呢..."


translate chinese epilogue_uv_ulya_1424cf31:



    nvl clear
    "我闭上眼睛，想象到斯拉维娅对我笑着，她总是乐于帮助我，而且从来不会责备我，和她在一起总是很舒心很安全，然后在公交车站的相遇…我猜自己可能从来没有想过在真实世界中和她在一起，但那一定是很精彩的生活！"


translate chinese epilogue_uv_ulya_c62fe1c5:



    nvl clear
    "多年以来我一直把列娜当作掌上明珠，我甚至记得和她在一起的所有时光。我记得我们的孩子，记得那些喜悦与痛苦的时光。这一切我都记得，实在是难以拒绝。"


translate chinese epilogue_uv_ulya_b374bc69:



    nvl clear
    "或者是阿丽夏，弹起她心爱的吉他，和她在一起永远不会寂寞，也许那是我梦想的生活，放荡不羁的生活？也许那是我一直梦想中的女孩儿，一个热情的假小子？我印象中的阿丽夏狡猾的笑着。"


translate chinese epilogue_uv_ulya_a8c0d905:



    nvl clear
    "乌里扬娜长大后也会是一个很好的女孩儿。当然在夏令营时我还没有把她当作是女人… 她当然有自己的缺点，但是她似乎还有一种魔力，深深的吸引着我。"


translate chinese epilogue_uv_ulya_248b80e1:



    nvl clear
    "当然还有未来...另一个世界的玛莎，她也曾是我的挚爱 。可能在夏令营的时光只是一段小插曲，和其他世界线的我完全不同，但那样也好，我有完全不同的选择，新的开始，一个好像我一直认识的人。"


translate chinese epilogue_uv_ulya_1f451166:



    nvl clear
    "不过我不会再见到尤莉娅了，还是感觉很遗憾..."


translate chinese epilogue_uv_ulya_1805ca4b:


    uv "你当然会见到了!"


translate chinese epilogue_uv_ulya_be7a8c7c:


    "她的声音在我的脑袋里响了起来。"


translate chinese epilogue_uv_ulya_3e8460dd:


    me "什么？你在哪里？"


translate chinese epilogue_uv_ulya_8b87fadc:


    uv "我就在这里，就像他说的一样。"


translate chinese epilogue_uv_ulya_23a24352:


    me "你知道?"


translate chinese epilogue_uv_ulya_2c864d61:


    uv "不知道，来到这里我才知道的。"


translate chinese epilogue_uv_ulya_bf6c77a3:


    me "这样可以吗？你不会想我吗？"


translate chinese epilogue_uv_ulya_938ec256:


    uv "当然不会了！我会一直和你在一起！"


translate chinese epilogue_uv_ulya_51149e68_2:


    me "…"


translate chinese epilogue_uv_ulya_9f6f75fc:


    uv "你还记得你的诺言吗?"


translate chinese epilogue_uv_ulya_ad02f53a_1:


    me "什么诺言?"


translate chinese epilogue_uv_ulya_989f34dd:


    uv "就是说你会和我永远在一起!"


translate chinese epilogue_uv_ulya_9513cd87_2:


    me "嗯..."


translate chinese epilogue_uv_ulya_25c487ea:


    uv "别想食言喔，我会一直盯着你的!"


translate chinese epilogue_uv_ulya_19a7bed2:



    nvl clear
    "我不知道自己会不会还像那样跟她说话，有人可能会因此发疯，但是现在我很高兴。"


translate chinese epilogue_uv_ulya_601ae3ae:


    me "嗯，我不会忘记的。"


translate chinese epilogue_uv_ulya_b9d89c90:


    uv "那你要做出选择。"


translate chinese epilogue_uv_ulya_8b728ec7:


    "我感觉到尤莉娅消失了，藏在了我意识的迷宫里。"


translate chinese epilogue_uv_ulya_3d5436bd:


    me "好了，到时间了!"


translate chinese epilogue_uv_ulya_867f9349:


    "我紧紧的闭上眼睛然后..."


translate chinese epilogue_uv_ulya_76b2fe88_1:


    nvl clear


translate chinese epilogue_uv_ulya_ce617998_16:


    "…"


translate chinese epilogue_uv_ulya_442941ca:


    "……"


translate chinese epilogue_uv_ulya_b010f1b9:


    "………"


translate chinese epilogue_uv_ulya_04c1f529:


    "... 明亮的阳光刺激着我的眼睛。."


translate chinese epilogue_uv_ulya_a7f58933:


    "没什么可惊讶的，反正是夏天嘛。"


translate chinese epilogue_uv_ulya_e544a3a0:


    "又是那个奇怪的梦。"


translate chinese epilogue_uv_ulya_8dcc0023:


    "为什么我最近总是梦到这个?"


translate chinese epilogue_uv_ulya_7a5325b0:


    "也许是有原因的!"


translate chinese epilogue_uv_ulya_4a2fcf60:


    "不过到底为什么?"


translate chinese epilogue_uv_ulya_0212e193:


    "少先队夏令营，女孩儿们，辅导员..."


translate chinese epilogue_uv_ulya_19d5f5ed:


    "我从来没去过夏令营。"


translate chinese epilogue_uv_ulya_e018ccaf:


    "可能你只是不记得了?"


translate chinese epilogue_uv_ulya_00b53382:


    "很有趣..."


translate chinese epilogue_uv_ulya_51119d14:


    "但是如果?"


translate chinese epilogue_uv_ulya_49a0f6ca:


    "相信我，我从三岁以来的事情都记得清清楚楚。"


translate chinese epilogue_uv_ulya_22e170fb:


    "再往前可能我还不太清醒。"


translate chinese epilogue_uv_ulya_b1a60837:


    "即使是这样的，这些也不是偶然的。"


translate chinese epilogue_uv_ulya_413b28d9:


    "为什么不是偶然的?"


translate chinese epilogue_uv_ulya_6eb9c6a9:


    "有人每天晚上在梦里寻找黑猫，但是什么也没有发生!"


translate chinese epilogue_uv_ulya_e04a3f3a:


    "这不是你自己编的吗?"


translate chinese epilogue_uv_ulya_1db77900:


    "我什么也没有编，这是我最近读到的。"


translate chinese epilogue_uv_ulya_85928523:


    "黑猫是一件事，这么详细的梦是另一回事。"


translate chinese epilogue_uv_ulya_d2a78cd5:


    "我没感觉有什么区别。"


translate chinese epilogue_uv_ulya_21380b7e:


    "好吧，那么那个声音怎么回事?"


translate chinese epilogue_uv_ulya_1b443b08:


    "什么声音?"


translate chinese epilogue_uv_ulya_c90f6cbf:


    "别装傻。"


translate chinese epilogue_uv_ulya_9087d0df:


    "没打算装傻。"


translate chinese epilogue_uv_ulya_ac53bb9c:


    "让你做出选择的那个声音。"


translate chinese epilogue_uv_ulya_fb191570:


    "好吧，一个声音，然后呢?"


translate chinese epilogue_uv_ulya_8b52ac40:


    "有原因的。"


translate chinese epilogue_uv_ulya_2edc20f2:


    "但是我没有别的印象了，我的梦到那里就结束了。"


translate chinese epilogue_uv_ulya_8545d90e:


    "可能是你醒来的时候忘掉了吧?"


translate chinese epilogue_uv_ulya_82f9910a:


    "可能是，但是..."


translate chinese epilogue_uv_ulya_355a343b:


    "但是什么?"


translate chinese epilogue_uv_ulya_09d75c72:


    "这是说你已经做出了选择。"


translate chinese epilogue_uv_ulya_e1a2e98c:


    "我都不记得选择。"


translate chinese epilogue_uv_ulya_6dd1edbf:


    "难道有过选择?"


translate chinese epilogue_uv_ulya_4f6b1ca8:


    "应该有的!"


translate chinese epilogue_uv_ulya_5d58786f:


    "你面对重要的事情都不怎么认真，一直都这样!"


translate chinese epilogue_uv_ulya_9ccde1c2:


    "哇，天哪，那么重要!"


translate chinese epilogue_uv_ulya_a58e1faa:


    "跟你说话实在是浪费时间..."


translate chinese epilogue_uv_ulya_a20cefa7_1:


    "..."


translate chinese epilogue_uv_ulya_bcd935d1:


    "最近我发现自己好像有一个分裂的人格。"


translate chinese epilogue_uv_ulya_9923c087:


    "自己的内心独白是很正常的，但是内心对话..."


translate chinese epilogue_uv_ulya_ce76dee3:


    "不过还没有到得病的成都，因为我在和自己说话而不是另外一个人格。"


translate chinese epilogue_uv_ulya_357f3a65:


    "可能是我的什么潜意识..."


translate chinese epilogue_uv_ulya_6ace32a6:


    "不过那都不重要了!"


translate chinese epilogue_uv_ulya_aa527417:


    "我翻了个身，阳光依旧照耀着我的眼睛。"


translate chinese epilogue_uv_ulya_85ea5c99_1:


    uv "早上好!"


translate chinese epilogue_uv_ulya_e1ffc0eb:


    sl "早上好!"


translate chinese epilogue_uv_ulya_83a3b49d:


    dv "早上好!"


translate chinese epilogue_uv_ulya_5567bace:


    un "早上好!"


translate chinese epilogue_uv_ulya_cb60f8f8:


    us "早上好!"


translate chinese epilogue_uv_ulya_e8f1372a:


    ma "早上好!"


translate chinese epilogue_uv_ulya_cfb99cb8:


    dreamgirl "早上好!"


translate chinese epilogue_uv_ulya_0a6d11b5:


    me "早上好!"


translate chinese epilogue_uv_ulya_348dbde8:


    "我回答道。"


translate chinese epilogue_uv_ulya_bc8dedc6:


    "选择...我有过选择吗...?"


translate chinese epilogue_uv_ulya_d99864b1:


    "每个故事都有自己的开始和结尾。"


translate chinese epilogue_uv_ulya_4d69c034:


    "每个故事都有梗概，摘要，目录，关键词，序章和尾声。"


translate chinese epilogue_uv_ulya_754a4f66:


    "不管什么书，只要你再去读一遍，总会发现新的细节的。"


translate chinese epilogue_uv_ulya_39cdaa6f:


    "但是每一本书都有最后一页，当我们读完的时候，就会放回到书架上。"


translate chinese epilogue_uv_ulya_611077c4:


    "为了明天能够打开新的一本书..."
# Decompiled by unrpyc: https://github.com/CensoredUsername/unrpyc
