
translate chinese epilogue_main_7a98964b:


    "我全身充满疼痛感。"


translate chinese epilogue_main_7c0ecf1c:


    "尤其是我的太阳穴。"


translate chinese epilogue_main_28766e67:


    "好像它们要碎成几千片，让风灌进我空空的脑袋。"


translate chinese epilogue_main_1f5640c9:


    "如果我没有睁开眼睛可能连五秒钟也受不了了。"


translate chinese epilogue_main_4d647778:


    "我在“某个地方”，我说不清是具体什么地方。"


translate chinese epilogue_main_81be611c:


    "我感觉云山雾罩的。"


translate chinese epilogue_main_0c4d877e:


    "一种介于清醒和不清醒之间，而且有清晰的记得自己的梦的状态。"


translate chinese epilogue_main_e050dc5d:


    "我正准备坐车去什么地方，突然有一个小女孩找上了我，快速的说着什么。"


translate chinese epilogue_main_5d58d05f:


    "我完全听不懂，但是小女孩看起来很沮丧。"


translate chinese epilogue_main_6bdcd82e:


    "我没有理解她需要什么。"


translate chinese epilogue_main_21564d96:


    "时间不停地流逝，她还是不停的说着。"


translate chinese epilogue_main_bb180213:


    "感觉越来越烦了。"


translate chinese epilogue_main_23d9757b:


    "我想要让她停下来或者歇一会儿，但是我失败了，可能是因为我没有说出什么或者我说的话没有到达她那里。"


translate chinese epilogue_main_14eb51b4:


    "我看不见她的脸，我只是听着，看着，听着，看着..."


translate chinese epilogue_main_685c283d:


    "可能对我还不是很重要。"


translate chinese epilogue_main_f176077d:


    "你当然不会注意到一个打扰你睡觉的蚊子的长相。"


translate chinese epilogue_main_79c81bc5:


    "你也不能注意到它拍打翅膀的频率还有口器的斜率。"


translate chinese epilogue_main_065ff797:


    "这个女孩只是那些打扰你集中精神或是睡觉的几百万种噪音之一..."


translate chinese epilogue_main_66488078:


    "她说的越是大声，我越是不能理解。"


translate chinese epilogue_main_829ffeb0:


    "公共汽车的车厢，老旧的座椅，不太平整的地板，生锈的车顶，破碎的车窗，所有东西都和她一起飘走了。"


translate chinese epilogue_main_1281b344:


    "然后我感到一种独特的解脱。"


translate chinese epilogue_main_de7746b8:


    "真正的汽车和女孩我已经不在意了，对我来说不过是一个虫子。"


translate chinese epilogue_main_6f529678:


    "现在我在这里完全陷入一片空寂的梦境..."


translate chinese epilogue_main_ed7f45f7:


    "梦境可以是一种实体，不一定是一个噩梦，但是也不是很舒服。"


translate chinese epilogue_main_4ad85600:


    "我醒过来揉揉脸，准备去洗漱。"


translate chinese epilogue_main_2ebebacc:


    "一个很阴沉的早晨。"


translate chinese epilogue_main_6695bb75:


    "我找不到自己的洗漱包包，但是没有也可以凑合一下。"


translate chinese epilogue_main_66d354ed:


    "穿衣服很困难，我的手不停的发抖。"


translate chinese epilogue_main_571f5d50:


    "我看着镜子里的自己，查看着两个星期的胡子。"


translate chinese epilogue_main_c4b53f19:


    "好吧，也许奥尔加·德米特里耶夫娜有刮胡刀..."


translate chinese epilogue_main_602cb68c:


    "知道现在我才突然意识到自己不在一个少先队夏令营里!"


translate chinese epilogue_main_f8f2d3b4:


    "我回到了自己的公寓!"


translate chinese epilogue_main_7132ec6d:


    "我离开自己的房间，走进了走廊，而不是宿舍外的森林!"


translate chinese epilogue_main_d7d976c7:


    "我的心中笼罩着惊讶，失望，甚至是恐慌。"


translate chinese epilogue_main_f8f1fe90:


    "怎么?!"


translate chinese epilogue_main_4b63500a:


    "我坐在床上抱着头，试图回忆起昨天的事情。"


translate chinese epilogue_main_c0692396:


    "公交车，夏令营的最后一天。"


translate chinese epilogue_main_28a24bd1:


    "我睡着了...然后我醒来的时候，就在家了。"


translate chinese epilogue_main_e6a0051c:


    "从某种程度上来说这都不合逻辑。"


translate chinese epilogue_main_9b3b626a:


    "最初的惊讶已经消失了。."


translate chinese epilogue_main_2b7cb0bd:


    "再怎么说我突然回到了家也没有突然出现在一个80年代夏令营那么让人吃惊。"


translate chinese epilogue_main_94b8b09a:


    "前两天的事情马上浮现在我的脑海中。"


translate chinese epilogue_main_4cdec556:


    "那个神秘的少先队员，他的话..."


translate chinese epilogue_main_16f67736:


    "但是我是怎么回到现实的?"


translate chinese epilogue_main_302cfd4b:


    "根据他的话我会永远的卡在夏令营中无法离开!"


translate chinese epilogue_main_da19ba98:


    "而且我还不是一个人。"


translate chinese epilogue_main_c8c85310:


    "车站的那个少先队员，还有复杂的矛盾，有出口的说法。"


translate chinese epilogue_main_d51d1680:


    "是不是说我找到了出口，然后成功的离开了，但是究竟是怎么?"


translate chinese epilogue_main_2d5435f1:


    "不管怎么说我都不知道自己是应该喜悦还是悲伤，在过去的一个星期里，我已经习惯了这种内心独白，深入的自我剖析，所以我只是不能接受，并不是自己的分析到底发生了什么事。"


translate chinese epilogue_main_797ee454:


    "他告诉我是第一个在第一次循环时就发现了其他人的人，但是这意味着什么吗?"


translate chinese epilogue_main_6d0aec47:


    "不管怎么说他的理论瞬间就崩溃了!"


translate chinese epilogue_main_117f74f8:


    "现在我得想想自己应该做什么。"


translate chinese epilogue_main_f5b5600e:


    "我当然得喜悦了！我终于回到了现实。"


translate chinese epilogue_main_9d077188:


    "也许过去的七天只是一个梦。"


translate chinese epilogue_main_ef3f3b75:


    "真的没有任何证据证明我曾经到达过那里。"


translate chinese epilogue_main_6bf2c0b3:


    "我没有见过自己的少先队员服装，我变成了过去的模样。{w}我的手机在桌子上充满了电。"


translate chinese epilogue_main_9854060f:


    "但是我不能掩盖自己的记忆。{w}梦里是不会发生这么真实的一系列事情的。"


translate chinese epilogue_main_e2ba07bf:


    "我还具体的记得这一个星期以来的各种事情。"


translate chinese epilogue_main_b42bf578:


    "也许我整个过程都在昏迷?"


translate chinese epilogue_main_7db54ad1:


    "我释出一段讽刺的笑声。"


translate chinese epilogue_main_37cb3d33:


    "不，那也不是一个合理的选择。"


translate chinese epilogue_main_59c846f5:


    "那么...就这样好好的结束了吗?"


translate chinese epilogue_main_fdf1c653:


    "夏令营中的最后几个小时突然出现在了我的脑海中。"


translate chinese epilogue_main_376c1a1c:


    "的确我没有期望着离开，不管是从那个现实还是那个夏令营，我已经准备好再来一个星期了。{w}或者再来，再来..."


translate chinese epilogue_main_bff86508:


    "我可能会接受自己的命运。"


translate chinese epilogue_main_4a86a5af:


    "而且发生了那么多事情我还能怎么做呢?"


translate chinese epilogue_main_c6b1ea80:


    "我吐出命运的一口气，费力的起床，然后坐在电脑旁。"


translate chinese epilogue_main_0dba5f6f:


    "很奇怪，从这上面看，我只是离开了14个小时，而不是一整个星期。"


translate chinese epilogue_main_295da44b:



    nvl clear
    "即时通讯出现了一条新消息..."


translate chinese epilogue_main_8ffdf5b6:


    "{i}嗨，谢苗...昨天，太棒了，回头见：）{/i}"


translate chinese epilogue_main_da755414:


    "我的大学同学，他邀请我去参加那个我本来准备去的聚会。"


translate chinese epilogue_main_6c9bac4f:


    "突然我明白了过来，头痛，晕晕的感觉，宿醉。"


translate chinese epilogue_main_393d03d9:


    "我昨天确实参加了聚会，喝了好多酒什么的...但是那些少先队员都是从哪里来的，那些记忆，那些情感?"


translate chinese epilogue_main_dc6e0b16:



    nvl clear
    "这些都无法理解，让我变得疯狂，我开始肮脏的咒骂，试图拔起自己的头发，用拳头打着键盘，知道所有的键都掉了以后才停止。"


translate chinese epilogue_main_f36bf9ba:


    "我为什么要在乎，当一段梦境或者幻视结束的时候本没有什么可担心的，而且大家一般都会高兴的，难道是我已经准备好在那里住下了，所以当我回到家的时候反倒不适应了？"


translate chinese epilogue_main_5f5b219a:



    nvl clear
    "然后一个简单的想法出现了，我可能只是发疯了..."


translate chinese epilogue_main_2500664b:


    "确实，发疯的人可能会看到像现实一样的东西。而且我又发疯时候的所有症状：我从来不离开自己的公寓，从来不进行社交，我有很多心理问题，好吧，这解释了很多问题!"


translate chinese epilogue_main_18e12dde:



    nvl clear
    "我的黑暗的笑声回响在整个房间。"


translate chinese epilogue_main_92e8ada7:


    "我想该到了我去看医生的时候了，或者就选择放弃，心甘情愿进精神病院?"


translate chinese epilogue_main_636725da:


    me "一匹马，我的王国换一匹马!"


translate chinese epilogue_main_5f0e3c02:


    "我一会儿哭一会儿笑。"


translate chinese epilogue_main_ecb4a2ab:


    me "不要和她争吵，她是个疯子!"


translate chinese epilogue_main_56059efa:


    "我的邻居咚咚的敲着墙，让我冷静下来一些。"


translate chinese epilogue_main_5eb2202b:



    nvl clear
    "如果我飞得太高，可能只有脑叶切断术能救得了我。"


translate chinese epilogue_main_ce617998:


    "..."


translate chinese epilogue_main_258159ac:


    "我就这样呆呆地坐了几个小时，什么也没做，什么也没想，只是盯着电脑屏幕，像睡着了一样，最后我终于打开了几个页面，有什么新闻，F5，F5！"


translate chinese epilogue_main_81eac3b9:


    "好像上个星期的事情都完全没有发生一样，来吧， 为了自己的幻觉悲伤值得吗? 是真的吗？有很多记忆。看起来像真的吗？是的，特别像，有证据吗？没有。"


translate chinese epilogue_main_6e5d6669:



    nvl clear
    "结论很明显，我的未来也是，我一直怀疑自己现在的生活没有什么好处，我没有习惯这样一种突然的事件，不过管他呢?我得准备好自己下一次发疯。"


translate chinese epilogue_main_ce617998_1:


    "..."


translate chinese epilogue_main_76b2fe88:


    nvl clear


translate chinese epilogue_main_8e39de8c:


    "夜晚，各式各样的记忆碎片打断了我的思考和幻觉，出人意料的，屏幕上突然出现了一个消息窗口。"


translate chinese epilogue_main_b336ac6e:


    message "嗨，你最近怎么样?"


translate chinese epilogue_main_e3463832:


    "是一个标准的开头。"


translate chinese epilogue_main_e7ce88cd:


    me "正常。"


translate chinese epilogue_main_931ab60c:


    "人物信息是空的，连昵称都没有，只有几位数字，只是看这几位数字可是看不出来对面是谁的，不是吗？当然，有一天都会不一样的，人们会被数字代码标识，还有几个字节的个性，几位的感情..."


translate chinese epilogue_main_dd99d86f:



    nvl clear
    message "安全回家了吗?"


translate chinese epilogue_main_387fcb02:


    "可能是昨天聚会上的人。"


translate chinese epilogue_main_830dea84:


    me "好像是的。"


translate chinese epilogue_main_707b84c6:


    message "你还记得我昨天告诉你的事情吧?"


translate chinese epilogue_main_f5ebe454:


    me "那个...可能忘了吧。"


translate chinese epilogue_main_62a3a2c6:


    "长长的停顿之后又有一条信息。"


translate chinese epilogue_main_ca8f6404:



    nvl clear
    message "我明白了。"


translate chinese epilogue_main_998b1726:


    "说真的我都不在乎这是谁，他要干什么。我还可以容忍他保持在我的联系人里，除非他骚扰我。"


translate chinese epilogue_main_33de6e67:


    message "那好吧，我们会再次见面的，一定会的。"


translate chinese epilogue_main_f5239078:


    "是啊，我很期待，啊..."


translate chinese epilogue_main_c92f92b9:



    nvl clear
    me "好吧。"


translate chinese epilogue_main_e24c9d81:


    message "再见!"


translate chinese epilogue_main_0e18a85a:


    "我没有回答。"


translate chinese epilogue_main_f8d54840:


    "这奇怪又乱七八糟的一天终于慢慢的要结束了，我上床后，又一次想起了那些让我感觉那么真实的事情，我明天醒来会在哪里？那些幻觉还会再来吗？不会吧，可能我都会忘了的。"


translate chinese epilogue_main_95611f9f:



    nvl clear
    "奇怪，但是现在我真的觉得发生的一切都是真的，那只是一段虚幻的，变形的幻想游戏，奇幻的东西，而这里才是真实的，不是那么方便，不如自己所料，让你惊讶，但是却是现实! "


translate chinese epilogue_main_fb80d3eb:


    "我带着这样的想法睡着了。"


translate chinese epilogue_main_ce617998_2:


    "..."


translate chinese epilogue_main_76b2fe88_1:


    nvl clear


translate chinese epilogue_main_c07a412b:


    "我的生活一天一天的回到了正常，我也开始忘记那一个星期的故事，再怎么说也并没有发生什么坏事。即使那就是真的，又有什么么？只是在一个少先队夏令营里待了七天，我没有被杀，没有被绑架去做人体实验，没有被洗脑，为什么现在我还要纠结原因呢?"


translate chinese epilogue_main_d7bc5196:



    nvl clear
    "如果真的是幻觉，我只能期待着不要再发生了。"


translate chinese epilogue_main_76b2fe88_2:


    nvl clear


translate chinese epilogue_main_1c95a7cd:


    "我还是继续待在家中，我的电脑是自己唯一的伴侣，键盘是我同外界交流的唯一渠道，F5再次成为我生活中的主要情节。如果想想的话真的没有那么糟糕。"


translate chinese epilogue_main_76b2fe88_3:


    nvl clear


translate chinese epilogue_main_88474bfc:


    "然后有一天，屏幕上又一次出现了那熟悉的几位数。"


translate chinese epilogue_main_68c6e51f:


    message "嘿!"


translate chinese epilogue_main_a87c71d7:


    me "你好。"


translate chinese epilogue_main_15fd667b:


    message "最近怎么样?"


translate chinese epilogue_main_e7ce88cd_1:


    me "还好。"


translate chinese epilogue_main_c56a1499:


    "也许我认识他，但是无所谓..."


translate chinese epilogue_main_fcdab43c:



    nvl clear
    message "最近有什么事吗?"


translate chinese epilogue_main_dd5093d8:


    me "没什么。"


translate chinese epilogue_main_d166e979:


    message "稳定第一?"


translate chinese epilogue_main_dd69c3a1:


    me "可以这么说。"


translate chinese epilogue_main_e43109d5:


    message "哦，我明白了。"


translate chinese epilogue_main_e99ffbdc:


    "我以为这段对话已经结束了，但是他似乎不这么想。"


translate chinese epilogue_main_efe11ea9:



    nvl clear
    message "所以说什么都没有改变吗?"


translate chinese epilogue_main_8dd0767d:


    me "没有，应该有改变吗?"


translate chinese epilogue_main_6fa5b1d2:


    message "你不要觉得那是很正常的事情！当然也不是谁都会遇到这种事情..."


translate chinese epilogue_main_1084810b:


    me "你到底在说什么?"


translate chinese epilogue_main_3c014c6c:


    message "你还没有明白吗?"


translate chinese epilogue_main_67b3ef9f:


    "我开始明白了。"


translate chinese epilogue_main_a160c0c8:



    nvl clear
    me "我更想听你的解释。"


translate chinese epilogue_main_ba495f57:


    message "你不记得夏令营吗?"


translate chinese epilogue_main_907853ad:


    "这像是晴天霹雳，那几天的事情突然闪现在我眼前，我的手不停的颤抖，额头出了冷汗，我惊恐的回头，想象着自己可能会遇到外星人，鬼怪，死神，但是我的房间里除了我自己什么也没有，我跑到走廊，检查了厨房还有卫生间，还是没有人，最后，我回到电脑前，同一条消息还在这里闪动着，就好像一条普通的消息一样。"


translate chinese epilogue_main_56c768f9:



    nvl clear
    me "你是谁?"


translate chinese epilogue_main_b69f1bc8:


    "我的手不停的颤抖，让我难以按下按键。"


translate chinese epilogue_main_56d58b67:


    message "哇，所以说你已经回忆起来了?"


translate chinese epilogue_main_30227704:


    "我的幻觉还在继续? 还是说我根本就没有能离开夏令营? 还是说这是一个梦，还是..."


translate chinese epilogue_main_54a73f72:


    me "我不知道该说什么。"


translate chinese epilogue_main_3a4edaaa:


    "我诚实的承认着，总之我觉得这个实体 (我只能想出来这个词语) 不需要任何网络就能和我交谈。"


translate chinese epilogue_main_bbccea89:



    nvl clear
    message "嗯，你只要安安静静的，一切都会解释明白的。XD"


translate chinese epilogue_main_a073eb1b:


    "最后的笑脸看起来像是魔鬼一样，伸出他变形的魔爪要吞噬了我。"


translate chinese epilogue_main_e1d6b6aa:


    me "我这是怎么了?"


translate chinese epilogue_main_d850b239:


    "我终于下了决心问了这个问题。"


translate chinese epilogue_main_5e3cf096:


    message "没有，没事，你只是在正常的生活。"


translate chinese epilogue_main_39faba7a:


    me "但是那个夏令营...那是真的吗?"


translate chinese epilogue_main_93656c09:


    message "那你觉得呢?"


translate chinese epilogue_main_4515fb38:


    me "我不太确定。"


translate chinese epilogue_main_83608308:


    message "如果你觉得是真的，那就是真的；如果你觉得不是，那就不是。就是这么简单。"


translate chinese epilogue_main_d1986394:



    nvl clear
    "这完全不简单!"


translate chinese epilogue_main_a87d627d:


    me "接下来会发生什么?"


translate chinese epilogue_main_9db6b90d:


    "我悄悄说着。"


translate chinese epilogue_main_ab2d839e:


    message "不用担心。"


translate chinese epilogue_main_7b4418d5:


    "看起来他真的不用互联网就能听见我说话，我的身体被恐惧感牢牢控制住，我闭上眼，团成一个球。"


translate chinese epilogue_main_dfa8be82:


    me "为什么，你为什么这么对我?"


translate chinese epilogue_main_4d3bcab6:


    "我要发疯了，各种词语不受控制的从我的嘴里出来，各种毫无意义的毫无逻辑的词语。"


translate chinese epilogue_main_9ee3c724:



    nvl clear
    me "为什么？为什么是我？为什么？原谅我！我什么都没有做！"


translate chinese epilogue_main_ae870164:


    "过了几分钟我终于有了勇气睁开眼睛看看显示器。"


translate chinese epilogue_main_148f12c7:


    message "你就是一切事情的原因。"


translate chinese epilogue_main_3f769b22:


    "消息窗口冷冷的闪动着。"


translate chinese epilogue_main_5f3254a2:


    me "那然后呢?"


translate chinese epilogue_main_a1f21fcf:


    message "嗯，你很快就会明白的！ :)"


translate chinese epilogue_main_76b2fe88_4:


    nvl clear


translate chinese epilogue_main_74902b45:


    "我的眼前陷入黑暗，耳朵里充满的噪音，我开始感到晕眩，我觉得自己的灵魂要离开身体了，前几天的事情变得模模糊糊，好像它们是发生在别人的身上而不是我自己，然后夏令营的记忆开始消失，好像有个人在用橡皮擦掉铅笔记录的回忆。过了一会儿，我什么也感觉不到了。我好像来到了一片意识的荒原。"


translate chinese epilogue_main_f7590cd3:


    "如果有人这个时候看到我，他们大概会看到我在笑着..."


translate chinese epilogue_main_a20cefa7:


    "..."


translate chinese epilogue_main_529298f6:


    me "呃...我好像卡在这里了。"


translate chinese epilogue_main_6f3ba371:


    "我敲击着举报垃圾邮件按钮，关掉窗口，然后打开浏览器，看了看表。"


translate chinese epilogue_main_612c456e:


    "该走了，不然我就该迟到了..."


translate chinese epilogue_main_a20cefa7_1:


    "..."


translate chinese epilogue_un_bad_0da63856:


    "有的时候现实变得完全没有意义。"


translate chinese epilogue_un_bad_6693cd84:


    "有时候你精神上的痛苦胜过一切。"


translate chinese epilogue_un_bad_092e6505:


    "即使世界终结了，你也不会注意到。{w}一把大刀刺入你的胸中，你也不会注意到。{w}即使是在地狱中永远饱受炙烤也没有感觉。"


translate chinese epilogue_un_bad_92427898:


    "不管怎么说，还有更重要的事情..."


translate chinese epilogue_un_bad_ab4e3f76:


    "当我睁开眼睛时，我意识到有什么事情不对劲。"


translate chinese epilogue_un_bad_089c801c:


    "我用了一段时间才明白过来。"


translate chinese epilogue_un_bad_2638c66a:


    "最后我意识到自己不是在公交车上，而是...在自己的老公寓里。"


translate chinese epilogue_un_bad_7c7bf187:


    "好吧，这倒是意料之中的。"


translate chinese epilogue_un_bad_724c6d60:


    "就好像我用了一个星期的时间认真的备考，然后，在最后的关键时刻，伟大的失败了。"


translate chinese epilogue_un_bad_c9575f91:


    "而这个伟大的的失败就是我返回了原来的世界..."


translate chinese epilogue_un_bad_95da6ae5:


    "本来应该是现实，但是我却没感觉比小猫头鹰夏令营真实多少。"


translate chinese epilogue_un_bad_f7d76256:


    "难怪，现实是你能感觉到，触摸到的。{w}那些东西当然都是具备的。"


translate chinese epilogue_un_bad_85abf9cd:


    "那个世界连最小的细节都是真实的。有的时候我真的感觉我过去的生活才是幻觉。"


translate chinese epilogue_un_bad_f527260f:


    "现在我又得开始回忆自己的存在。"


translate chinese epilogue_un_bad_459b2ee7:


    "为什么?{w}我好像是在汽车全速奔驰的时候被扔了出去一样。"


translate chinese epilogue_un_bad_fd341608:


    "在我摔得鼻青脸肿，躺在路中间的时候，汽车扬长而去，带走我所有的希望。"


translate chinese epilogue_un_bad_b99b2263:


    "列娜..."


translate chinese epilogue_un_bad_6f0311f7:


    "她的形象清晰的浮现在我眼前，让我忍不住痛哭。"


translate chinese epilogue_un_bad_ae8f3acd:


    "不——我想要狂喊，使劲抓着头发，用拳头砸墙，然后继续发疯一样喊叫。"


translate chinese epilogue_un_bad_f7554966:


    "然而我的灵魂还是那么空虚。"


translate chinese epilogue_un_bad_c0d3b99f:


    "我徒劳的寻找着对她的悔恨，可怜，痛苦，但是已经所剩无几。"


translate chinese epilogue_un_bad_18791a54:


    "我就躺在这里，盯着天花板..."


translate chinese epilogue_un_bad_53f73d1e:


    "我完全不在乎自己到底是为什么，是怎么回来的。"


translate chinese epilogue_un_bad_5e02e068:


    "反正，谁在乎自己是怎么把灵魂出卖给恶魔的呢？还有各种法律程序?"


translate chinese epilogue_un_bad_db394f22:


    "更重要的是结果。而这就是我得到的结果。"


translate chinese epilogue_un_bad_5e75b8df:


    "不，这不是说我回到了现实!{w}如果我到达了那辆公交车的终点，也不会有任何的改变的。"


translate chinese epilogue_un_bad_53be3572:


    "结果就是列娜身上发生的一切，原因在于我，这一切我都十分确定。"


translate chinese epilogue_un_bad_a3b352cb:


    "再怎么说，她也不可能凭空做出那种事情...{w} 不，列娜绝不会那样的！所以绝对是我的错!"


translate chinese epilogue_un_bad_7f92c544:


    "知道自己是别人的死因，带着这种罪恶，想要活下去真的是一种负担。"


translate chinese epilogue_un_bad_1b453803:


    "好像就是我亲自拿着刀子划开她的手腕，亲眼看着她死掉的过程。然后就这样离开..."


translate chinese epilogue_un_bad_bd552dae:


    "当然，我当时什么也做不了，但是我感觉自己是一个胆小鬼。"


translate chinese epilogue_un_bad_1f993547:


    "不，比那个还不是东西。"


translate chinese epilogue_un_bad_0c858154:


    "这些真的重要吗?"


translate chinese epilogue_un_bad_c763d1a6:


    "我竟然还能冷静的思考这些问题，我因为这个就诅咒自己。"


translate chinese epilogue_un_bad_d14867bb:


    "我现在应该责备自己，然后为列娜哀悼...{w} 但是我什么也做不到，如果我不能回去的话... "


translate chinese epilogue_un_bad_f7620350:


    "我开始颤抖，我全身都在颤抖，好像不能呼吸一样。"


translate chinese epilogue_un_bad_7a23b79f:


    "自我保护的本能战胜了愧疚，我步履蹒跚的走向厨房，吃掉一些镇定剂。"


translate chinese epilogue_un_bad_8325eee3:


    "像我一样具有反社会人格的往往都在柜子里面有这种储备。"


translate chinese epilogue_un_bad_06a8cbb9:


    "我吃掉半板，然后返回房间打开电脑，我受不了这样的寂静。"


translate chinese epilogue_un_bad_561abd10:


    "我随机打开了一首歌曲，结果发现这应该是电脑上最压抑的一首曲子了。"


translate chinese epilogue_un_bad_2233adb0:


    "但是我就是停不下来，这种噪音可以让我平静一些。"


translate chinese epilogue_un_bad_5fa296f2:


    "我需要决定以后的生活要怎么度过。"


translate chinese epilogue_un_bad_d453809f:


    "有一件事我可以确定，这一切，夏令营里发生的一切，我突然出现在那里，我又突然回来，我全都不在乎。"


translate chinese epilogue_un_bad_a36af85e:


    "还有，我也不在乎这些事情到底是为了什么。"


translate chinese epilogue_un_bad_6fb09a75:


    "我的心中只有列娜。"


translate chinese epilogue_un_bad_081d7480:


    "那个曾经的列娜。"


translate chinese epilogue_un_bad_cc0ad615:


    "当然也有可能那只是一个梦，而她根本就不是真人。"


translate chinese epilogue_un_bad_810ea4a2:


    "不过那样说来，我们的现实世界也有可能是某个人疯狂的幻想。"


translate chinese epilogue_un_bad_0403a983:


    "如果人们在现实世界里饱受失去爱人的痛苦，为什么我在那个世界失去列娜就变成疯狂的幻觉了?!"


translate chinese epilogue_un_bad_ff643764:


    "我自己亲眼所见，我感到痛苦，恐惧..."


translate chinese epilogue_un_bad_88638ccf:


    "见鬼！对我来说那种现实还一直持续着。"


translate chinese epilogue_un_bad_9dedbadc:


    "永远持续下去!"


translate chinese epilogue_un_bad_e2eb86dd:


    "而且我确定自己不是在做白日梦!"


translate chinese epilogue_un_bad_e27cd146:


    "虽然说那要是一个梦该有多好啊..."


translate chinese epilogue_un_bad_59ef3021:


    "扩音器里传来吉他的悲凉旋律，{w}好像是一首安魂曲，专门奏给我听的..."


translate chinese epilogue_un_bad_cb6f7baf:


    "现在我就要背负着这份罪恶或者吗!"


translate chinese epilogue_un_bad_034f1df0:


    "我的思想还不是那么完美，而且一个完美的人也忍受不了这样的负担！"


translate chinese epilogue_un_bad_167e249d:


    "而且我已经感觉自己要发疯了!"


translate chinese epilogue_un_bad_14cf330d:


    "我想要压抑住这些想法，不是想要逃避，只是让自己休息一下。"


translate chinese epilogue_un_bad_6ff084a7:


    "只要一分钟就好..."


translate chinese epilogue_un_bad_41d75612:


    "但是没有用，我的不断被痛苦裹挟。{w}我已经开始从身体上感觉到了。{w}不过身体上的总是要比精神上的轻一些..."


translate chinese epilogue_un_bad_04d0ffcc:


    "我跌倒在地板上，像婴儿一样滚来滚去。"


translate chinese epilogue_un_bad_d629c94e:


    "大脑中血液汹涌，好像随时都有可能爆裂开。"


translate chinese epilogue_un_bad_ddf2c20b:


    "我撞到了桌子，一根蜡烛从纸堆下面滚了出来。"


translate chinese epilogue_un_bad_a52bd6cd:


    "小小的，20厘米长，几乎折了90度，但是还保持着原来的形状，灯芯微微凸出。"


translate chinese epilogue_un_bad_210e40a9:


    "我找到一个打火机，点亮了蜡烛。"


translate chinese epilogue_un_bad_510f67d0:


    "以此来纪念列娜..."


translate chinese epilogue_un_bad_55f5fa34:


    "也许在另外一个世界里她过的能好一些..."


translate chinese epilogue_un_bad_ce617998:


    "…"


translate chinese epilogue_un_bad_be1b0bed:


    "我坐在地板上看着，蜡滴在我的手指上。"


translate chinese epilogue_un_bad_a54003ea:


    "我感觉不到疼痛，可能是我的神经已经不能继续传递信号了吧。"


translate chinese epilogue_un_bad_ef0a06ae:


    "火焰让我平静了一些，我盯着火苗，什么都没有想。"


translate chinese epilogue_un_bad_160075f5:


    "最后，终于获得了一点平静..."


translate chinese epilogue_un_bad_ce617998_1:


    "..."


translate chinese epilogue_un_bad_f07d7c82:


    "蜡烛烧掉了一半。"


translate chinese epilogue_un_bad_bf7d5546:


    "突然间，我感觉自己的生命就像是这蜡烛一样。"


translate chinese epilogue_un_bad_f97deac3:


    "不只是我的，任何人都一样。"


translate chinese epilogue_un_bad_7d4f29f6:


    "我们生来都是完整的。"


translate chinese epilogue_un_bad_a017ae8e:


    "但是接下来没准会发生什么事，可能突然刮来一阵风，手突然颤抖，灯芯可能烧断...{w}然后生命就会提前结束。"


translate chinese epilogue_un_bad_2ebac29f:


    "但是，每一根蜡烛都是不同的!"


translate chinese epilogue_un_bad_550851ec:


    "就像是这个——3卢布；那个两倍粗的——7卢布。更大的，像铁锹的木柄一样粗的——70卢布。"


translate chinese epilogue_un_bad_986feabd:


    "我不知道列娜的蜡烛是提前熄灭了呢，还是比别人的小呢?"


translate chinese epilogue_un_bad_d3c73c95:


    "虽然说我很怀疑还有没有比我的还小的..."


translate chinese epilogue_un_bad_0a488632:


    "我在手里转动着蜡烛。"


translate chinese epilogue_un_bad_06a3d48f:


    "多么有趣，我随时都可以把蜡烛吹灭，然后就完了..."


translate chinese epilogue_un_bad_1c8c0f19:


    "但是，生活不是蜡，你不能把两个小的捏在一起变成大的。"


translate chinese epilogue_un_bad_fe0373f7:


    "我很乐意把我自己剩下的部分送给列娜。"


translate chinese epilogue_un_bad_038be31f:


    "给任何更值得的人!"


translate chinese epilogue_un_bad_317fb567:


    "我留着有什么用呢？既不烫手，也不温暖，也很难照亮房间。"


translate chinese epilogue_un_bad_d887153f:


    "坦率的说，简直就是浪费蜡，浪费氧气..."


translate chinese epilogue_un_bad_7bf48038:


    "我吹灭了蜡烛。"


translate chinese epilogue_un_bad_87239164:


    "这个动作没有唤起任何感情。"


translate chinese epilogue_un_bad_aed890bf:


    "我慢慢的站起来前往浴室。"


translate chinese epilogue_un_bad_ce617998_2:


    "…"


translate chinese epilogue_un_bad_a61ee024:


    me "沿着走，而不是穿过去，大家都穿过去，你却..."


translate chinese epilogue_un_bad_50614f77:


    "列娜的身影又出现在了我的眼前。"


translate chinese epilogue_un_bad_efd90378:


    "她在微笑着。"


translate chinese epilogue_un_bad_8c835c15:


    me "我们一定会再次见面的...我真的很抱歉..."


translate chinese epilogue_un_bad_9ee997b0:


    "温暖的水，影子还有死去的梦让我平静了许多。"


translate chinese epilogue_un_bad_16861ec1:


    "可能我真的从来就没有去过那个夏令营。"


translate chinese epilogue_un_bad_487e661d:


    "或者我根本就没有回来。"


translate chinese epilogue_un_bad_1c0fe513:


    "但是现在已经不重要了..."


translate chinese epilogue_un_bad_cb89588c:


    "我几乎感受到了列娜的拥抱。"


translate chinese epilogue_un_bad_c4d43861:


    me "一切都会好起来的...{w}我们会一起坐上车前往目的地...{w} 没有人会找到我们...{w}我们在那里永远幸福..."


translate chinese epilogue_un_bad_7e153b32:


    "我开始失去力量，逐渐沉到水中去。"


translate chinese epilogue_un_bad_8c835c15_1:


    me "我们一定会再见面的...原谅我... "


translate chinese epilogue_un_bad_ce617998_3:


    "..."


translate chinese epilogue_un_good_ced219ed:


    un "醒醒!"


translate chinese epilogue_un_good_2e7081b6:


    "我逐渐恢复了意识。"


translate chinese epilogue_un_good_451af48e:


    "我好像穿过云层向下坠落。"


translate chinese epilogue_un_good_6188d36b:


    "我越来越接近地面了，但是我不害怕，我开始慢下来，温暖迎接着我..."


translate chinese epilogue_un_good_456fff17:


    "我睁开眼睛。"


translate chinese epilogue_un_good_9e2c1e5b:


    un "你要把一辈子都睡过去了!"


translate chinese epilogue_un_good_ed8bfd35:


    "列娜严肃的看着我。"


translate chinese epilogue_un_good_0bd39427:


    me "啊？什么？咱们已经到了吗...?"


translate chinese epilogue_un_good_79937daa:


    un "到哪儿?"


translate chinese epilogue_un_good_211eccb4:


    "我看看四周。"


translate chinese epilogue_un_good_5efc1f6b:


    "长椅，广场，Genda..."


translate chinese epilogue_un_good_aedb5b04:


    "怎么?{w}我正在公交车上，前往城镇中心的路上。"


translate chinese epilogue_un_good_576a105f:


    un "你怎么像看到鬼一样看着我?"


translate chinese epilogue_un_good_8bb49e0e:


    me "但是咱们...我为什么还在这里?!"


translate chinese epilogue_un_good_a5d4a4e2:


    "我有一瞬间非常恐惧。"


translate chinese epilogue_un_good_0113bcd7:


    "我难道永远无法离开这个地方了?"


translate chinese epilogue_un_good_973d03cf:


    me "咱们...一起在...公交车上..."


translate chinese epilogue_un_good_5f6db029:


    un "在公交车上?"


translate chinese epilogue_un_good_ef4c9ffe:


    me "昨天..."


translate chinese epilogue_un_good_77a0ecbc:


    un "昨天?"


translate chinese epilogue_un_good_18b26cc6:


    me "是的，昨天，你不记得了吗?"


translate chinese epilogue_un_good_d6dd39b3:


    "列娜开始思考。"


translate chinese epilogue_un_good_5bd0cf6d:


    un "昨天...{w}我完全不记得那种事情，可能是你做的梦吧。"


translate chinese epilogue_un_good_7bef23f0:


    "想象这里的各种幻觉，那有可能是梦中梦吧。"


translate chinese epilogue_un_good_5f1d7a74:


    me "好吧，咱们慢慢说..."


translate chinese epilogue_un_good_54beb87a:


    "我试图理智的思考。"


translate chinese epilogue_un_good_b23cd5f9:


    me "昨天夏令营结束，所有的少先队员都离开了，但是咱们留了下来...{w}然后咱们坐上公交车前往城镇中心。"


translate chinese epilogue_un_good_a3e6cada:


    "列娜非常困惑不解。"


translate chinese epilogue_un_good_5a05f88f:


    un "你在开玩笑吗?"


translate chinese epilogue_un_good_5d17104b:


    me "当然没有啊!"


translate chinese epilogue_un_good_be6c71e6:


    un "今天是最后一天。"


translate chinese epilogue_un_good_56a8dc53:


    me "等等...{w}那么我在这里呆了多长时间?"


translate chinese epilogue_un_good_71c0ad20:


    "列娜开始掰着手指头数起来。"


translate chinese epilogue_un_good_45719f1b:


    un "好像是...七天。"


translate chinese epilogue_un_good_1163b74b:


    me "不是八天?"


translate chinese epilogue_un_good_0262d06c:


    un "是七天，绝对是七天!"


translate chinese epilogue_un_good_7b2a2cf9:


    "我疲倦的叹了口气，捂着脸。"


translate chinese epilogue_un_good_90cfa1fe:


    "好吧，又多出一条，这个夏令营的神秘事件。"


translate chinese epilogue_un_good_85181956:


    me "那...好吧。虽然我觉得咱们今天也无法离开这个夏令营。"


translate chinese epilogue_un_good_b672514d:


    un "为什么?"


translate chinese epilogue_un_good_4d730f88:


    me "第六感。"


translate chinese epilogue_un_good_8d11582d:


    un "你今天有点奇怪。"


translate chinese epilogue_un_good_5b132458:


    me "可能是吧，我有自己的理由..."


translate chinese epilogue_un_good_9d577beb:


    un "想告诉我吗?"


translate chinese epilogue_un_good_8f838576:


    "我认真的看着她。"


translate chinese epilogue_un_good_e86e77ad:


    me "我想。"


translate chinese epilogue_un_good_5ce3e082:


    "当然了，有何不可?"


translate chinese epilogue_un_good_d2bb6bf4:


    "我完全有机会离开这个夏令营的时候，都完全失败了..."


translate chinese epilogue_un_good_87127cb1:


    "现在还能失去什么呢?"


translate chinese epilogue_un_good_0ee8c6fb:


    me "那么我应该从哪里开始...?"


translate chinese epilogue_un_good_9e69a9ba:


    un "从最初开始。"


translate chinese epilogue_un_good_a3854fdc:


    me "对啊，当然...{w} 那个，我出生，上学，结婚...{w} 呃... 嗯，那个，我还没有结婚..."


translate chinese epilogue_un_good_a20e9f2e:


    un "你还有时间。"


translate chinese epilogue_un_good_dce47469:


    me "是啊，但是...{w}那个...简单的说，我不是这个世界的人。"


translate chinese epilogue_un_good_97c2c800:


    "看看她的表情，我确定她完全没有听懂。"


translate chinese epilogue_un_good_1072a05d:


    me "我是很意外的来到这里的，我都不知道为什么。"


translate chinese epilogue_un_good_7887dd77:


    un "那你的世界是什么样子的?"


translate chinese epilogue_un_good_c2b423ab:


    "列娜严肃的问着。"


translate chinese epilogue_un_good_d1d836af:


    me "那个...{w}其实是一样的，只是要比你的晚了二十年。"


translate chinese epilogue_un_good_fb5752ef:


    un "你们有宇宙飞船了吗?"


translate chinese epilogue_un_good_f2fa462b:


    me "要说那个问题的话，其实你们也有啊。{w}但是我们还没有在那个领域取得什么突破性的进展。"


translate chinese epilogue_un_good_6b84b606:


    un "好像很有意思!"


translate chinese epilogue_un_good_bac480a2:


    "我说不清列娜究竟是真的很严肃还是在和我开玩笑。"


translate chinese epilogue_un_good_632121f6:


    me "我过去生活在一个完全不同的地方，在遥远的北方，从景观上来说是一个大城市。"


translate chinese epilogue_un_good_94abe78c:


    un "我从来没有去过大城市。"


translate chinese epilogue_un_good_e8e0a840:


    me "也许那样才好。"


translate chinese epilogue_un_good_9790301a:


    un "你可以带我去你家吗?"


translate chinese epilogue_un_good_96768537:


    "她似乎很认真。"


translate chinese epilogue_un_good_7e5baac7:


    me "好吧，我都不确定现在到底有没有那所房子..."


translate chinese epilogue_un_good_8d8e7fcf:


    un "哎呀!"


translate chinese epilogue_un_good_e49a9950:


    "列娜撅起嘴。"


translate chinese epilogue_un_good_481c2e78:


    me "嗯，好吧，带你去..."


translate chinese epilogue_un_good_df776ecb:


    un "要怎么去呢?"


translate chinese epilogue_un_good_cf4b3576:


    me "有天晚上我坐着公交车，410路，等我醒来的时候已经到这里了。{w}除此之外就都不知道了。"


translate chinese epilogue_un_good_39b2719c:


    un "410路，就是在夏令营门口有站的!"


translate chinese epilogue_un_good_57ebfa1e:


    me "对啊，但是我的那个410路运行在不同的时间地点。"


translate chinese epilogue_un_good_1c2133a9:


    un "你打算回去吗?"


translate chinese epilogue_un_good_78a5a750:


    "我注意到列娜的表情似乎有一点失望。"


translate chinese epilogue_un_good_ad816568:


    me "我都不知道怎么才能..."


translate chinese epilogue_un_good_cf0505aa:


    un "如果你知道呢?"


translate chinese epilogue_un_good_f078db34:


    me "这是个很难回答的问题。"


translate chinese epilogue_un_good_be29202a:


    un "怎么了?"


translate chinese epilogue_un_good_72d6612e:


    me "没什么...{w}那个，我才刚刚适应在这里生活，最初到这里的那一个小时，我简直是..."


translate chinese epilogue_un_good_5f555b1e:


    un "没关系，你还有时间去想。"


translate chinese epilogue_un_good_b1d14b10:


    "有时间想?"


translate chinese epilogue_un_good_5540c8a2:


    "好像列娜虽然相信了我，但是只是觉得这是个麻烦。"


translate chinese epilogue_un_good_59c33efd:


    me "不管怎么说，我说了不算。{w}也许把我带到这里的人可以再把我送回去。"


translate chinese epilogue_un_good_c17e95b3:


    un "我相信人一生任何事情都是有原因的。{w} 所以你来到这里也是有原因的。"


translate chinese epilogue_un_good_1b2ffe6d:


    me "也许是这样的..."


translate chinese epilogue_un_good_5db9f7be:


    un "如果你还没有回去...那一定是这里需要你。"


translate chinese epilogue_un_good_da6af767:


    me "嗯，这个颇有道理。"


translate chinese epilogue_un_good_9a80266d:


    mt "嘿，该收拾东西了!"


translate chinese epilogue_un_good_9719a190:


    "我好像听到了辅导员的声音远远的。"


translate chinese epilogue_un_good_7ef75530:


    un "听见了吗?"


translate chinese epilogue_un_good_2bf6ead1:


    me "我没什么可装的..."


translate chinese epilogue_un_good_c09b5abb:


    un "我有啊！那就在公交车那里等我吧!"


translate chinese epilogue_un_good_86be10c0:


    me "你想让我帮忙吗?"


translate chinese epilogue_un_good_8fa9fd07:


    un "谢谢你，但是不必了。"


translate chinese epilogue_un_good_22f7b476:


    "列娜笑着跑开了。"


translate chinese epilogue_un_good_6c54000c:


    "我还震惊的坐在长椅上。"


translate chinese epilogue_un_good_c391d6da:


    "我的大脑一片混沌，各种想法翻来覆去。"


translate chinese epilogue_un_good_094c98fd:


    "一方面来说，我如果能离开这里，比起我突然来到这里要好得多，但是..."


translate chinese epilogue_un_good_a33f4f66:


    "怎么办？我应该怎么办？如果我再试试的话，我真的有可能离开这里吗?"


translate chinese epilogue_un_good_675b9b96:


    "不过紧张有什么用?{w}反正现在我能确定自己什么主都做不了。"


translate chinese epilogue_un_good_70c222fc:


    "可能我就坐在后面看看风景就行了?"


translate chinese epilogue_un_good_ef2764bc:


    "我不禁笑了起来。"


translate chinese epilogue_un_good_b2d4b2d6:


    "更别说我现在还有留在这里的理由。{w}那就是列娜。"


translate chinese epilogue_un_good_3ed77c37:


    "也许她是对的，我来到这里是有原因的。"


translate chinese epilogue_un_good_4a181a23:


    "反正就算我统计出所有自己来到这里的理由，也还是会有下一个。"


translate chinese epilogue_un_good_ce005bce:


    "我要是一直分析个不停，肯定会疯掉的。"


translate chinese epilogue_un_good_251ac201:


    "也许我应该做出选择了?"


translate chinese epilogue_un_good_72f1add5:


    "可能这个世界就没有那么糟糕。{w}尤其是我终于有了让我生活下去的目标。"


translate chinese epilogue_un_good_994914a4:


    "很模糊，但是还是有的!"


translate chinese epilogue_un_good_8ae8f091:


    "现在我还有力量可以让它变得更强大！"


translate chinese epilogue_un_good_88e5892d:


    "我一边想着一边前往奥尔加·德米特里耶夫娜的房间准备收拾自己的那几件廉价的行李然后永远的离开这个夏令营。"


translate chinese epilogue_un_good_ce617998:


    "..."


translate chinese epilogue_un_good_752fc388:



    nvl clear
    "..."


translate chinese epilogue_un_good_2000e780:


    "你可能觉得奇怪，但是我们最后终于来到了城镇中心，我在车上睡着了，我醒来以后，跑下车，呼吸着新鲜的空气，然后我突然意识到这长长的一个星期终于结束了!"


translate chinese epilogue_un_good_a8aed4e9:


    "我最后终于逃离了这个夏令营，我度过的时间似乎远远超过7天… 但是现在一切都结束了。"


translate chinese epilogue_un_good_33d7cf72:


    "那些事情都疯狂的流失掉。"


translate chinese epilogue_un_good_b7912630:


    "没有时间再去分析，再去寻找答案，而且渐渐的我开始忘掉那个夏令营的事情。"


translate chinese epilogue_un_good_6383c486:


    "刚刚进入一个陌生的环境，我就和他人一样，完全迷糊了。我没有什么身份证，甚至没有出生证明，没有生存的本领，电脑操作员或者接线员这种职业根本就没人需要。"


translate chinese epilogue_un_good_b54c638a:



    nvl clear
    "列娜回归了正常的生活，她毕业以后进入大学，夏令营在苏联南部，跟我想的一样，列娜住在一个人口十万的城镇里，从我们的城镇中心去那里，坐公交车的话很方便。"


translate chinese epilogue_un_good_c7376b62:


    "城镇没有什么特殊的，一个大工厂，很多5层的楼房，郊区的小木屋，七点关门的杂货店，还有一个超市，购物狂的伊甸园，不管是冬天的橡胶鞋，女式的羊皮大衣还是男式的麝鼠皮帽都应有尽有。"


translate chinese epilogue_un_good_02306dd9:


    "以前我可能拼命的逃离这样的地方，但是现在这是我的家。在欧洲20世纪已经很少有饥荒出现了，我设法找到了工作，我在工厂里做车工助手，也在宿舍里搞到了位置。"


translate chinese epilogue_un_good_e8a877a1:



    nvl clear
    "这份工作开始显得很难，但是逐渐的我越来越满意。也许一部分是因为可以自己去买食物。时间不断的流逝，我慢慢的升职，最终成为了整个班的领导。"


translate chinese epilogue_un_good_6bd80c6f:


    "我的同事都很吃惊我有这样的才能还有毅力，我自己也很惊讶。列娜毕业了，我则夜夜把着计算器，设计收支表格，最后决定向列娜求婚。"


translate chinese epilogue_un_good_8de1f0df:


    "我不记得她有没有话很长时间考虑，有没有建议签署婚姻协议，或者因为没有嫁妆而抗议过，但是很快我们一起搬进了工厂旁边的集体公寓，这个花掉了我工资的三分之二。"


translate chinese epilogue_un_good_ce390941:



    nvl clear
    "应该感谢苏联的免费教育，亦或是我妻子的坚持，我在本地的工业大学开始学习。"


translate chinese epilogue_un_good_467cd7ed:


    "最初我还抗议，表示一个真正的无产阶级没必要像一个知识分子一样，不过后来我还是意识到有一张文凭还是更容易生存。"


translate chinese epilogue_un_good_d5213709:


    "因为以前更高的求学背景，现在学习起来并不难，国家发生了很大的变化，虽然我一直知道，但是事情发生的时候还是很突然。"


translate chinese epilogue_un_good_5f973fa2:



    nvl clear
    "你可以想象一下在山上遇到雪崩，再怎么准备，最后还是会把你埋在下面，工厂都进行了私有化，然后都关门了。"


translate chinese epilogue_un_good_7676e0eb:


    "我兼职做一名司机，开着我岳父的戈比老爷车，真的不怎么赚钱，我甚至都想要去小猫头鹰夏令营做一名辅导员了，这时列娜的一个远方的亲戚去世，留给我们一套房子，在俄罗斯中部。"


translate chinese epilogue_un_good_c490afc8:


    "我们经过讨论之后决定搬过去，我们在一个赫鲁晓夫楼拥挤的厨房里，看着天鹅湖，迎接了90年代，在我从事兼职赚了一点小钱以后我们决定搬到大一点的城市去，当然我选择了自己出生地..."


translate chinese epilogue_un_good_b16966f7:



    nvl clear
    "我们决定卖掉房子搞投资，我们一度变得很富裕，购买各种貂皮大衣还有国外的名车，在高档餐厅吃饭，去国外旅游。和别人一样，那个时候我有机会起来。"


translate chinese epilogue_un_good_082d8313:


    "我的生意是建材的经销，这个生意很赚钱因为那个时候人们在高档住宅上投入很多钱。"


translate chinese epilogue_un_good_200309d6:


    "也许生活还能这样继续一段时间，但是90年代被称为放纵的90年代也是有原因的。在经历的敲诈和腐败的影响以后，我被迫带着空空的冰箱，空空的钱包还有嘴里的血离开。"


translate chinese epilogue_un_good_f5d9692f:



    nvl clear
    "这个时候我想起来自己来自未来的这个优势。苏联时代不允许的赌博场所不断涌现，我马上跑去在我知道的所有事件上下注，然后我有遇到了新的打击，各种比赛的赢家都和我的印象不一样，连94年的世界杯冠军都不是巴西。"


translate chinese epilogue_un_good_353ba4d9:


    "我看电视的时候还一切正常，但是当我把口袋里最后几块钱掏出来赌博的时候就会出现各种各样的冷门。"


translate chinese epilogue_un_good_ea090535:


    "除此之外我还追求过政治顾问的职位，但是没有关系的话你是不会进入相关的领域的，我不知道自己还要受多少这样的煎熬，结果在这个时候，我和列娜有了一个孩子。"


translate chinese epilogue_un_good_c402b437:



    nvl clear
    "我开始急切的寻找各种可能的手段来补贴家用。我想起来父亲的一些老朋友，然后再扮演着自己的亲戚，在银行了做了一个初级数据分析，同时我又尝试读下我的文凭（准确的说应该是毕业）。"


translate chinese epilogue_un_good_0c3a6b1b:


    "时间过得很慢，我们赚的钱只够填饱肚子的，经常在班上住下，挤出时间复习考试，列娜在家看孩子，我在这个时候开始写作，现在感觉好像昨天一样。我想起自己午夜坐在老旧的386面前，在毁灭战士里疯狂的消灭怪兽。"


translate chinese epilogue_un_good_fa74f193:


    "我想要好好睡一觉但是电脑游戏可以让我解脱，当我过关时，脑海中满是这几年来发生的事情，对于一个人来说太多了，我决定把自己身上发生的事情都写下来。"


translate chinese epilogue_un_good_65ff89ca:



    nvl clear
    "我打开文字软件，写了二百个字，想要明天再写，但是我第二天就忘了，下个礼拜也没有想起来。"


translate chinese epilogue_un_good_a55760f5:


    "过了一个月，列娜在电脑上打东西的时候我才想起来，我开始质疑自己到底能不能完成。"


translate chinese epilogue_un_good_a0fb86f8:


    "而且，描写自己是很困难的... 这件事情可以暂时搁置，迎接2000年的时候我已经是部门主管，搬家的时候我发现了自己老旧的386，想起了曾经想要写自传的想法。"


translate chinese epilogue_un_good_b64fc8f9:



    nvl clear
    "我想了，好蠢，但是过了一会儿，我打开笔记本，记录下一个没有理想的年轻人，在外星球上找到自我的故事。糟糕的格式和这糟糕的剧情很搭配。"


translate chinese epilogue_un_good_06d5c1a8:


    "我把故事给妻子看，她笑了，但是鼓励我继续。"


translate chinese epilogue_un_good_62204972:


    "过了一年，我的故事已经装满了一个抽屉，我感觉很奇怪，因为我全天时间都在工作，回到家也是在陪家人，不过我还是做到了。"


translate chinese epilogue_un_good_16103041:



    nvl clear
    "那时我想要发表自己的作品，和大家一样，大部分都被拒绝了，但是有几篇还是过审了，我没有拿到稿费，但是我的作品有人读了！有人会觉得很有意思。"


translate chinese epilogue_un_good_d17d0576:


    "时间慢慢的流逝，我们又有了第二个孩子，闲暇时间我开始写小说，但是没有写我的生活，夏令营，等等，我觉得那是命运。"


translate chinese epilogue_un_good_bc5da8f3:


    "谁知道如果我没有突然出现在小猫头鹰夏令营的门口，又会发生什么事情呢?"


translate chinese epilogue_un_good_d7be6fd0:


    "过了一年，我的小说写完了，在我烦过所有的出版商以后，我终于和一家小公司达成了一致。一个大胡子穿着破烂的粗花呢夹克的的编辑老头跟我说："


translate chinese epilogue_un_good_d813e73f:



    nvl clear
    "'你的水平很一般，但是想法很好，你需要多读书，多读些书..."


translate chinese epilogue_un_good_d13fddf7:


    "我一生都记着这句话，我兑现和朋友们的约定，把书分发给他们，列娜读完说很有意思，可能是怕我伤心吧。"


translate chinese epilogue_un_good_78ec671a:


    "时间过得很快，很快就要到我突然离开正常生活回到那个夏令营的时候了，现在我是一个成功的作家（至少从卖出多少书来说是这样）。"


translate chinese epilogue_un_good_e0db8497:



    nvl clear
    "今天的午夜是一个新的开始，明天就是那个我已经等待了几十年的日子。"


translate chinese epilogue_un_good_2cd0045b:


    un "你在想什么?"


translate chinese epilogue_un_good_83704a72:


    "我睁开眼睛看着列娜。"


translate chinese epilogue_un_good_8e609a05:


    "她在摇椅旁边的小桌上摆了一小杯茶。"


translate chinese epilogue_un_good_72199a1c:


    "火焰不慌不忙的烧着，外面则刮着暴雪。"


translate chinese epilogue_un_good_932e1068:


    "我把自己裹在毯子里，看着列娜。"


translate chinese epilogue_un_good_f07f7371:


    me "你知道今天是什么日子吗?"


translate chinese epilogue_un_good_6b22e1e1:


    un "不知道，什么?"


translate chinese epilogue_un_good_e152372b:


    me "你还记得我在夏令营的时候是怎么和你说的嘛?"


translate chinese epilogue_un_good_049b021f:


    un "不记得了。"


translate chinese epilogue_un_good_13821bda:


    "也难怪，这些年我也一直没有提起这件事情。"


translate chinese epilogue_un_good_1814dbd5:


    me "你还记得我跟你说我不是这个世界的人吗?"


translate chinese epilogue_un_good_d6dd39b3_1:


    "列娜停了下来。"


translate chinese epilogue_un_good_9ebd484f:


    un "嗯，好像是...对啊..."


translate chinese epilogue_un_good_3ed0a2cc:


    me "今天就是那个日子，我坐上了410路，然后醒来之后出现在那个夏令营的那一天。"


translate chinese epilogue_un_good_ffd733f0:


    un "所以这是纪念日吗?"


translate chinese epilogue_un_good_561828ec:


    "列娜笑了。"


translate chinese epilogue_un_good_ca6a2d01:


    me "是啊，算是吧..."


translate chinese epilogue_un_good_58a9940e:


    un "是一个很悲伤的纪念吗?"


translate chinese epilogue_un_good_0d7f2c30:


    me "没有啊，完全不是!"


translate chinese epilogue_un_good_4013090d:


    un "我已经不记得那个时候的事情了..."


translate chinese epilogue_un_good_e68e962b:


    me "那样才好，可能..."


translate chinese epilogue_un_good_dd47136a:


    "我填了一些柴火，然后拿起来杯子。"


translate chinese epilogue_un_good_429c57e7:


    me "也许我可以写一本小说，你看看就能想起来了。"


translate chinese epilogue_un_good_b63153d6:


    un "你觉得那样好吗?"


translate chinese epilogue_un_good_aed0e1bf:


    "列娜的表情变得严肃了起来。"


translate chinese epilogue_un_good_1b89c45a:


    "确实，我曾经向自己保证再也不写那段故事。"


translate chinese epilogue_un_good_2141272e:


    me "嗯，的确现在发生的事情都是为了咱们两个。"


translate chinese epilogue_un_good_e5f761b0:


    un "我也是这么想的。"


translate chinese epilogue_un_good_82c3c74a:


    "我叹叹气，看着火苗。"


translate chinese epilogue_un_good_b5944be5:


    me "你知道，这就像是蜡烛，有的人短一些，有的人长一些，随时都有可能灭掉。"


translate chinese epilogue_un_good_76bc0c24:


    un "那咱们的生活就像是这个火炉。"


translate chinese epilogue_un_good_d76f93fa:


    me "嗯，是啊。"


translate chinese epilogue_un_good_5c7f770a:


    "我看着她，微笑着。"


translate chinese epilogue_un_good_1064aa29:


    me "我要再睡一会儿。"


translate chinese epilogue_un_good_9271b549:


    "列娜亲吻了我，然后飘渺迷离的盯着火苗。"


translate chinese epilogue_un_good_18bde327:


    "我闭上了眼睛。"


translate chinese epilogue_un_good_b130e182:


    "我开始慢慢的落向什么地方。"


translate chinese epilogue_un_good_da5a6c7c:


    "不是一个梦，像是温暖的以太，把我包裹起来。"


translate chinese epilogue_un_good_ce617998_1:


    "..."


translate chinese epilogue_us_f08a51bb:


    "好像我完全没有睡觉。"


translate chinese epilogue_us_b10d729b:


    "往往是这样的，好像眨眼之间，就已经到了早晨。"


translate chinese epilogue_us_f8bebe71:


    "我用力的打着呵欠，下巴差点疼起来。"


translate chinese epilogue_us_ab939c0c:


    "有点不对劲..."


translate chinese epilogue_us_2a6b0fba:


    "不是有点—是完全不对劲!"


translate chinese epilogue_us_7c5d345f:


    "我回到了我的公寓。"


translate chinese epilogue_us_d3ab1684:


    "但是...怎么回事，这怎么可能？?"


translate chinese epilogue_us_b97b6ee6:


    "我开始慌了，我在屋子里跑来跑去试图冷静下来。"


translate chinese epilogue_us_32fd8672:


    "身体上的劳累往往可以平静一个人的心灵。"


translate chinese epilogue_us_2c5d0412:


    "我的大脑空空的只剩下恐惧，脑海中似乎回荡着什么歌声，也许是祈祷，也许是什么没有关系的，不能让我冷静也可能让我不那么紧张。"


translate chinese epilogue_us_1645c72d:


    "过了一个小时我瘫倒在地上，死死的盯着天花板。"


translate chinese epilogue_us_80b0da80:


    "好像我哪里都没有去，古老的吊灯还在天花板上看着我，石灰的碎片还有碎掉的壁纸都没有掉落下来。"


translate chinese epilogue_us_8d04f2aa:


    "这真的是一个梦吗...?"


translate chinese epilogue_us_69c49704:


    "但是不可能！真的不可能!"


translate chinese epilogue_us_afbfd5dd:


    "我在那个夏令营待了整整一个星期。"


translate chinese epilogue_us_164fbde5:


    "我绝对去过那里，从最初抵达，到最后离开，一切都那么真实，如果只是幻觉的话不会那么真实。"


translate chinese epilogue_us_ca2a0b33:


    "我站起来去厨房倒了一杯水。"


translate chinese epilogue_us_d0a7feb4:


    "我的太阳穴还是不停的涌动着，但是至少没有刚开始那么害怕了，至少能休息一会儿。"


translate chinese epilogue_us_c3ffb486:


    "我集中精神思考着那时的细节，那是离开夏令营的时候。"


translate chinese epilogue_us_7136c5b9:


    "夜晚，颠簸的公共汽车，脏乱的窗户，少先队员..."


translate chinese epilogue_us_2ce6d3ca:


    "我都没有想过可以回来。"


translate chinese epilogue_us_8dcc331d:


    "不管怎么说我已经开始计划到达城镇中心以后的生活了。"


translate chinese epilogue_us_d8532661:


    "或者?"


translate chinese epilogue_us_0babffab:


    me "见鬼!"


translate chinese epilogue_us_0e363452:


    "我愤怒的拔着自己的头发。"


translate chinese epilogue_us_5058105d:


    me "我想不起来了!"


translate chinese epilogue_us_60547951:


    "最后几个小时的事情就好像雷诺阿的印象派作品模模糊糊。"


translate chinese epilogue_us_3302407b:


    me "但是也没有那么糟糕。而且有什么糟糕的，应该说现在这样简直是完美!"


translate chinese epilogue_us_d0f0488f:


    me "我终于离开了那个倒霉的营地，回到家，现在最重要的事情是不要再去了，这是有可能的!"


translate chinese epilogue_us_ce617998:


    "..."


translate chinese epilogue_us_f896d7c0:


    me "但是也没有什么好怕的，我只是看到了什么，幻觉!"


translate chinese epilogue_us_4d149f2f:


    me "现代科学不允许这样的事情存在!"


translate chinese epilogue_us_f4546a9f:


    "我大声笑着。"


translate chinese epilogue_us_33c6265f:


    "我内心有一个声音想要制止我嘴上停不下来的腹泻，但是没有成功。"


translate chinese epilogue_us_27fabe99:


    "好像我的想法和语言完全独立运转了。"


translate chinese epilogue_us_4a73860c:


    "我的大脑告诉我要平静的思考分析，但是我的舌头只是不停的发出各种没有意义的声音来平息自己的紧张情绪。"


translate chinese epilogue_us_94206733:


    "最后我终于安静了下来，我拉开了窗帘。"


translate chinese epilogue_us_a5c5546b:


    "城市的夜景和一个星期以前完全一样。"


translate chinese epilogue_us_450bbc84:


    "这个景色至少让我的心情稍稍恢复了平静。"


translate chinese epilogue_us_533137c4:


    "不管怎么说，如果一切正常，没有什么超自然的事情发生，那不就是一个梦嘛？"


translate chinese epilogue_us_94950be4:


    "现在只有两个选择。"


translate chinese epilogue_us_06d20129:


    "我承认这只是一场梦，然后冷静下来。{w}或者我接受那些都是真的。"


translate chinese epilogue_us_193a19e2:


    "不管我选择哪一个，我都无法得到答案。"


translate chinese epilogue_us_adf47948:


    "很有意思，我在那里呆了一周，尽全力寻找答案却找不到，现在我突然回来了，该怎么办?"


translate chinese epilogue_us_62a5f438:


    "神秘的面纱还没有揭开，却不断涌现出更多的问题。"


translate chinese epilogue_us_2b19ebd4:


    "最后我躺在床上，没过多久就打起呼噜..."


translate chinese epilogue_us_ce617998_1:


    "…"


translate chinese epilogue_us_54d920bf:



    nvl clear
    "自从我回来以后已经过了很久，我的生活也发生了很大的变化。"


translate chinese epilogue_us_bd3a1548:


    "回来的第一个礼拜，我不停的思考，总结发生的各种事情，画图表进行分析，甚至准备去看心理医生。"


translate chinese epilogue_us_9462ce27:


    "结果什么也没有发生，我只是一个普通人，这些事情已经完全超出我的容量了。"


translate chinese epilogue_us_6101364c:


    "如果一个洞穴人突然出现在21世纪，他都可以比我理解的事情多。"


translate chinese epilogue_us_7ec6a1be:



    nvl clear
    "最开始他会觉得手机把人的声音发送到很远的距离是上帝制造的伟大的奇迹。"


translate chinese epilogue_us_29d72188:


    "但是最后总会习惯的。"


translate chinese epilogue_us_e0d7c1dc:


    "可能他上了小学或者中学之后都可以试着理解具体的工作原理了。"


translate chinese epilogue_us_82144eab:


    "这是一个很夸张的例子，跟我身上发生的事情一样神奇。"


translate chinese epilogue_us_65950373:


    "我怀疑自己有没有机会真正了解到底这件事情是怎么发生的，而且我如果再次遇到这件事情我受不受得了。"


translate chinese epilogue_us_9bdf285f:



    nvl clear
    "不过还是有一个问题一直困扰我——为什么是我?"


translate chinese epilogue_us_7b47c1ee:


    "我做了什么，导致如此的好运（或者厄运）?"


translate chinese epilogue_us_5535f094:


    "科幻小说的故事经常发生在普通人身上，但是那都是偶然的。"


translate chinese epilogue_us_716452f4:


    "但是我认为这些都是有原因的。比如小说里一个人物被放到1000年前。"


translate chinese epilogue_us_6a21d9ba:



    nvl clear
    "但是没有人认识他，都觉得他是一个疯子。"


translate chinese epilogue_us_9087af0c:


    "但是我呢？夏令营里的人们都认识我。"


translate chinese epilogue_us_7d414027:


    "也许是随机选择出来做研究？那样这就更值得问为什么了。"


translate chinese epilogue_us_ce617998_2:


    "..."


translate chinese epilogue_us_4284aa16:



    nvl clear
    "找不到答案，我只能回到自己普通的生活。"


translate chinese epilogue_us_28ca60d9:


    "但是现在我不只是一天到晚坐在电脑前狂按F5了，我有了新的兴趣，我想要继续读书。"


translate chinese epilogue_us_e0d94309:


    "不是为了找到工作什么的，只是因为我想起来自己大学第一年的生活是那么的有趣。"


translate chinese epilogue_us_ddf05860:


    "和同学们聊天，没日没夜的狂欢，和我这几年没有激情的生活完全不一样。"


translate chinese epilogue_us_9b73f6c1:


    "一位伟人说过，读书太多，用脑太少，会懒于思考。"


translate chinese epilogue_us_6a62e6c6:



    nvl clear
    "一个普通人不会因为懒惰在这里坐着呆着，真正的原因是没有有意思的东西。"


translate chinese epilogue_us_b4927a3f:


    "比如说，我不喜欢针线活，所以我就是一个懒惰的人了吗?"


translate chinese epilogue_us_67291156:


    "但是懒得翻开书这种感觉我明白，虽然我喜欢看书，但是想到大部头几千页我就..."


translate chinese epilogue_us_68cd3f08:


    "即使很有意思，我可能也要放一放，现在我只想拿起一本书，但是我这么懒..."


translate chinese epilogue_us_83bbcd46:


    "我不知道什么影响了我，但是我有一半的时间开始做有意义的事情了。"


translate chinese epilogue_us_01e63747:



    nvl clear
    "有时候我觉得那是夏令营带给我的。"


translate chinese epilogue_us_6b3df02c:


    "这个事实不能否认，在夏令营的那几天我一直在进行社交活动，解决各种事件，这在以前是完全不能想象的。"


translate chinese epilogue_us_c9700033:


    "另一方面来说仅仅一个星期还不足以把一个人的性格改变的完全变样。"


translate chinese epilogue_us_9deeccfb:


    "尤其是像我这样固执的人。"


translate chinese epilogue_us_ae21b9f8:



    nvl clear
    "但是给出一个方向还是很简单的。"


translate chinese epilogue_us_cfcb2a60:


    "但是我觉得对于我来说也是不太可能的。"


translate chinese epilogue_us_7d323ccc:


    "不管怎么说，我很享受这一切，也就不太细究其原因了。"


translate chinese epilogue_us_ae7018a0:


    "要是谁中了头奖，怎么可能还去思考为什么这个问题？"


translate chinese epilogue_us_ce617998_3:


    "..."


translate chinese epilogue_us_b60d6130:



    nvl clear
    "夏天我重新进入大学，学期在秋季重新开始。"


translate chinese epilogue_us_f8eaaeec:


    "我以自己都想象不到的热情参加着所有的课程，讨论，考试，等等。"


translate chinese epilogue_us_792db99f:


    "我游刃有余的参加各种活动，虽然我比他们大，但是基本上不会影响到我。"


translate chinese epilogue_us_cae314c1:


    "也许是因为我的个性一直都不太成熟，这个我也说不清。"


translate chinese epilogue_us_6135d2ed:



    nvl clear
    "经过了很多年以后，与人交谈的乐趣，终于再度出现。"


translate chinese epilogue_us_aafbc9f6:


    "和他人交谈其实没什么，原本我感觉生活就是一片阴暗压抑的，现在也重新发出的光芒。"


translate chinese epilogue_us_71461a28:


    "有时候我感觉自己变成了玩具店里一排排的玩具。"


translate chinese epilogue_us_e2123de9:


    "但是在这家店里，所有坏掉的玩具都被扔到小黑屋里，少一只胳膊的泰迪熊啊，看起来像是个微波炉的变形金刚啊。"


translate chinese epilogue_us_bfbc90e1:



    nvl clear
    "过去我曾经和它们在一起。"


translate chinese epilogue_us_b863937a:


    "他们可能被捐走，或者送到孤儿院，我只能继续在垃圾堆里。"


translate chinese epilogue_us_1be16bf7:


    "所以我的生活发生了这么大的变化我高兴还来不及呢!"


translate chinese epilogue_us_ce617998_4:


    "..."


translate chinese epilogue_us_be1991b7:


    "这是这个学期最困难的课程的最后一节课。"


translate chinese epilogue_us_ead34c7c:


    "大部分学生都不喜欢。"


translate chinese epilogue_us_18d7148a:


    "因为课程很难，老师的话很难听懂吧。"


translate chinese epilogue_us_31f9553d:


    "但是我竟然对这些数据和表格很感兴趣。"


translate chinese epilogue_us_65f71ceb:


    "我用它进行各种计算，得出对一切事物的看法。"


translate chinese epilogue_us_9f752038:


    "没有什么数字可以逃过我的法眼，都被我一一悉心整理。"


translate chinese epilogue_us_f15dbc2c:


    "每一位数都被放在相应的空格里，工作服都有相应的用处。"


translate chinese epilogue_us_620a5afb:


    "有人挖战壕，有人向前进，有人在用线性回归计算射程。"


translate chinese epilogue_us_bb1b122e:


    odn "你在想什么?"


translate chinese epilogue_us_ec822f80:


    "我不太情愿的停下手中的工作看着同学。"


translate chinese epilogue_us_51f07f25:


    me "就是做笔记嘛。"


translate chinese epilogue_us_6f6e3c30:


    odn "这有什么！回去看教材就好了!"


translate chinese epilogue_us_a2c7fb06:


    me "教材我也会回去看的。"


translate chinese epilogue_us_d93f9cb9:


    odn "你总是让我很吃惊。"


translate chinese epilogue_us_baaee44b:


    me "有什么吃惊的?"


translate chinese epilogue_us_41e93016:


    odn "你准备拿一等学位，是不是?"


translate chinese epilogue_us_21b742e8:


    me "没想过..."


translate chinese epilogue_us_096e8101:


    odn "你别告诉我你真的觉得有意思!"


translate chinese epilogue_us_ef73bda8:


    "我刚刚面对着一堆数字情不自禁的微笑。"


translate chinese epilogue_us_56f957c4:


    odn "你都不需要这些东西!"


translate chinese epilogue_us_93d43576:


    me "都有用的，至少对于总体的发展来说。"


translate chinese epilogue_us_a5b0bb2a:


    "同学讽刺的笑了笑。"


translate chinese epilogue_us_38f2115a:


    me "我打赌你一辈子都没有读完过一本书。"


translate chinese epilogue_us_d6011e93:


    odn "那怎么了?"


translate chinese epilogue_us_f13d64cb:


    "他挑衅道。"


translate chinese epilogue_us_fcfd9bb3:


    me "只是在陈述一个事实。"


translate chinese epilogue_us_c7273c00:


    odn "你总是云山雾罩的。"


translate chinese epilogue_us_69ed5808:


    "他说的有道理，虽然我找回了自己的社交技能，但是我还经常做白日梦。"


translate chinese epilogue_us_33265fad:


    me "你说得好像是一件坏事一样!"


translate chinese epilogue_us_39ec8d69:


    odn "那不读书也没有什么不好!"


translate chinese epilogue_us_6ba8578f:


    me "人类不能只有物质的欲望。"


translate chinese epilogue_us_ef353f7b:


    "我嘲讽他。"


translate chinese epilogue_us_da783828:


    odn "啊，你真是充满希望!"


translate chinese epilogue_us_ce617998_5:


    "..."


translate chinese epilogue_us_5c98c047:


    "课程快要结束了，我开始计划接下来的事情。"


translate chinese epilogue_us_26180779:


    "需要去买点东西，还有计划要完工送给客户，还要做笔记..."


translate chinese epilogue_us_44324923:


    "晚上我就可以随便的看点什么。"


translate chinese epilogue_us_75613726:


    "除非有人找我有事，不然我就可以和朋友混一混。"


translate chinese epilogue_us_21b43907:


    odn "啊，还有五分钟..."


translate chinese epilogue_us_5620292c:


    "我看看手机，发现从我返回现实世界已经过了一年了。"


translate chinese epilogue_us_c2222e39:


    "我的灵魂获得了平静。"


translate chinese epilogue_us_f6b9bef5:


    "我现在都不怎么想到当时的事情了。"


translate chinese epilogue_us_4510141f:


    "但是那种事情也不是可以轻易忘记的。"


translate chinese epilogue_us_da6f1a84:


    "而且一般的事情，过了很久以后可能都只剩下一些碎片，但是小猫头鹰夏令营的那些日子不同。"


translate chinese epilogue_us_c7712fd2:


    "我记得所有的细节，刚刚下车时的恐惧，第一天充满惊奇，认识了那么多人，乌里扬卡的恶作剧，就拿那个扮鬼的恶作剧来说吧。"


translate chinese epilogue_us_3b043f92:


    "苏联最好的喜剧演员也会羡慕的!"


translate chinese epilogue_us_1133bb6a:


    "那些少先队员们，他们是最真实的人，他们天真无邪的笑着!"


translate chinese epilogue_us_88f6794d:


    "如果能在现实世界中见到乌里扬卡就好了。"


translate chinese epilogue_us_5fdf8a75:


    "她可能不总是那么完美，她简直是多动症，也没有什么礼貌，但是..."


translate chinese epilogue_us_ad5fa5b0:


    "这样真诚热情的孩子平常也是不多见的。"


translate chinese epilogue_us_957cd999:


    "也许她给了我这份能量..."


translate chinese epilogue_us_732f5a0a:


    "结果她只能远远的躲在后面。"


translate chinese epilogue_us_66623e13:


    "当然是一个梦，是我美好的想象。"


translate chinese epilogue_us_59330073:


    "不过虚拟的人物，小说的主角，电影中的人物，人们还是会随着他们的境遇而不时落泪。"


translate chinese epilogue_us_41ee4507:


    "对我来说一样，如果我的表现有何不同的话，这个故事一定会有一个更温暖的结局的。"


translate chinese epilogue_us_e8ee1985:


    "不过我确定乌里扬卡在那个世界也一定会幸福的。"


translate chinese epilogue_us_10bf53a2:


    "我真的无法想象她能有连续几分钟失落那样的情况。她乐观的精神会让她一直前进，不管是多么困难的情况。"


translate chinese epilogue_us_5432fef5:


    "我很好奇她长大以后会是什么样子..."


translate chinese epilogue_us_ce617998_6:


    "…"


translate chinese epilogue_us_78903e24:


    "铃声响了，我的同学站起来热情的看着我。"


translate chinese epilogue_us_320bd80d:


    odn "嗯，再见了...我回来可以抄你的笔记吗?"


translate chinese epilogue_us_22448110:


    me "如果你自己一直知道记录的话就不会这么烦恼了。"


translate chinese epilogue_us_0c8f943d:


    odn "所以说到底行不行啊?"


translate chinese epilogue_us_9f5895ba:


    me "好吧，好吧。"


translate chinese epilogue_us_31844f25:


    odn "万岁!"


translate chinese epilogue_us_3e06fe65:


    "他欢乐的离开了教室。"


translate chinese epilogue_us_0912f764:


    "我慢慢的收好自己的书和笔记本，然后环顾着教室四周。"


translate chinese epilogue_us_c11e20a5:


    "我一度感觉自己可以闻到空气中知识的味道，就像是在甲板上大海的味道一样。"


translate chinese epilogue_us_d5ee5f01:


    "成千上万的学生们来到这里来求学，学会新的知识，忘掉原本会的知识。"


translate chinese epilogue_us_4f2c8f9f:


    "有些人整堂课都在睡觉，有些人在认真的记录笔记，像我一样，但是没有人是完全冷漠的。"


translate chinese epilogue_us_36afd483:


    "没有那种拉上窗帘眼睛死死的盯着电脑屏幕的人。"


translate chinese epilogue_us_a3f5dd2b:


    "我怀着要好好度过这一天的志向离开了教室。"


translate chinese epilogue_us_303f415b:


    "铃声响了，我的同学站起来，对我说了再见，然后离开了。"


translate chinese epilogue_us_7de17a9f:


    "我开始收拾自己的书和笔记本，突然听到前面有谁的声音。"


translate chinese epilogue_us_ec6b06a5:


    usg "请问，这里是34号教室吗?"


translate chinese epilogue_us_446ad0da:


    me "门上有标志的。"


translate chinese epilogue_us_e87f24be:


    usg "没有!"


translate chinese epilogue_us_a7b9d37d:


    "女孩儿好像有点生气。"


translate chinese epilogue_us_974f3678:


    me "那肯定是有人摘掉了。"


translate chinese epilogue_us_faab6aa2:


    usg "所以这里就是34号教室?"


translate chinese epilogue_us_fc30fd2b:


    me "对啊。"


translate chinese epilogue_us_18a6d4fc:


    "我终于收拾好所有的东西，准备站起来离开。"


translate chinese epilogue_us_053ad9b8:


    "这个女孩儿看起来特别熟悉。"


translate chinese epilogue_us_04dca07f:


    me "咱们有没有...?"


translate chinese epilogue_us_41fdb5dd:


    usg "是啊，我也有这个感觉。"


translate chinese epilogue_us_f2f12dc1:


    "她好像非常惊讶。"


translate chinese epilogue_us_94093acc:


    me "但是我想不起来自己可能在哪里见过你了。"


translate chinese epilogue_us_6c0a1771:


    usg "我也是..."


translate chinese epilogue_us_8cc9c861:


    me "你是几年级?"


translate chinese epilogue_us_7aad28e6:


    usg "一年级。"


translate chinese epilogue_us_b8aea2a0:


    me "哇，还是新人啊..."


translate chinese epilogue_us_fb117d69:


    "女孩儿微笑着。"


translate chinese epilogue_us_080a02d2:


    usg "我的未来还在前方!"


translate chinese epilogue_us_c49f13e1:


    me "好像我没有未来似的..."


translate chinese epilogue_us_48cc1cb9:


    usg "你呢?"


translate chinese epilogue_us_dd578b4d:


    me "四年级。"


translate chinese epilogue_us_cfefc74e:


    usg "那个，在这里学习很困难吗?"


translate chinese epilogue_us_8f48e6be:


    me "第一年是有点。"


translate chinese epilogue_us_05e57566:


    usg "我就知道!"


translate chinese epilogue_us_1e8379d1:


    "她沮丧的说着。"


translate chinese epilogue_us_04f0c0cd:


    me "但是会越来越容易的...而且你习惯了以后就很容易了。"


translate chinese epilogue_us_638e6427:


    usg "有时候我太懒了..."


translate chinese epilogue_us_3685e98e:


    me "嗯，是有这种情况。"


translate chinese epilogue_us_da4ba352:


    usg "你还有大一时候的东西吗?{w}如果你能借给我的话可能我应付考试会容易一些。"


translate chinese epilogue_us_123037fe:


    "显然我什么都没有了，我大一还是很久以前的事情呢。"


translate chinese epilogue_us_934f55fc:


    "但是我突然有一种强烈的欲望想要她的电话号码。"


translate chinese epilogue_us_14b5f47a:


    me "嗯，可能有...{w}但是我要去找找。"


translate chinese epilogue_us_463c9a51:


    usg "太棒了!"


translate chinese epilogue_us_5274a2e2:


    "我们交换了电话号码。"


translate chinese epilogue_us_96f1754d:


    me "我叫谢苗。你呢?"


translate chinese epilogue_us_f46d0d3c:


    usg "乌里扬娜。"


translate chinese epilogue_us_fcd06166:


    "我终于想起了这个女孩儿到底像谁!"


translate chinese epilogue_us_8a86e6b2:


    "真的就是乌里扬卡，只是大了几岁而已!"


translate chinese epilogue_us_919cb1ad:


    "我瞬间不知道说什么好。"


translate chinese epilogue_us_9f328817:


    us "怎么了？只是一个普通的名字啊！是列宁的名字啊!"


translate chinese epilogue_us_89c17322:


    "她撅起嘴来。"


translate chinese epilogue_us_f5b985ac:


    me "没有...只是，你参加过少先队夏令营吗?"


translate chinese epilogue_us_4af695ee:


    us "我小的时候...问这个干什么?"


translate chinese epilogue_us_cf8568cf:


    "眨眼间，小猫头鹰夏令营的各种事情开始浮现在眼前。"


translate chinese epilogue_us_63854d93:


    "那不是梦!"


translate chinese epilogue_us_cba25f57:


    us "不过只是夏令营，并不是少先队的..."


translate chinese epilogue_us_903a86f3:


    "乌里扬娜又说了一句。"


translate chinese epilogue_us_9445daad:


    me "所以你没有穿少先队制服吗?"


translate chinese epilogue_us_c3110504:


    us "红领巾吗？没有啦!"


translate chinese epilogue_us_56d4e39c:


    "她大声笑着。"


translate chinese epilogue_us_3649081a:


    us "但是为什么问这个呢?"


translate chinese epilogue_us_8f44567c:


    "我都想告诉她，但是怕她把我当成神经病。"


translate chinese epilogue_us_6b2b31a6:


    "不管怎么说这个孩子比我印象中的乌里扬卡要大不少。"


translate chinese epilogue_us_ae985c6d:


    "也许只是长的像..."


translate chinese epilogue_us_3cd4f58f:


    me "我曾经做过一个梦...我觉得我好像见过你。那种既视感。"


translate chinese epilogue_us_d1a9fc56:


    us "既视...?"


translate chinese epilogue_us_ec20dd52:


    me "就是那种似曾相识的感觉。"


translate chinese epilogue_us_abc2ab7a:


    us "既然你提起了这件事情..."


translate chinese epilogue_us_ce43a5b5:


    "她变得严肃起来。"


translate chinese epilogue_us_78382c80:


    us "我也觉得自己在什么地方见过你。{w} 绝对见过!{w}也许就是你刚刚说的..."


translate chinese epilogue_us_e8c05531:


    me "真是有趣的巧合啊，是不是?"


translate chinese epilogue_us_2170a31a:


    us "我不知道...总之，很好！我会给你打电话的!"


translate chinese epilogue_us_5de584b6:


    "她笑着跑了出去。"


translate chinese epilogue_us_0dcbddab:


    "相貌，举止，谈吐...不管怎么看，她就是乌里扬卡!"


translate chinese epilogue_us_894a40f8:


    "这就是说那个梦成真了?"


translate chinese epilogue_us_23b134c2:


    "这可真是一件好事!"


translate chinese epilogue_us_9e9cd5d5:


    "不管怎么说，我又能见到这个欢乐的女孩子了!"


translate chinese epilogue_dv_9bc20c27:


    "好像我变老了..."


translate chinese epilogue_dv_2be57040:


    "一天已经结束了，但是我还是感觉晕晕的。"


translate chinese epilogue_dv_6be0ab43:


    "我用力尝试着，但是还是睁不开眼。"


translate chinese epilogue_dv_99485000:


    "我很好奇，城镇中心到底有什么?{w} 我要怎么生存?{w} 也许并没有那么糟糕?"


translate chinese epilogue_dv_df8947d0:


    "我好，我确实不知道从哪里开始，不过话说回来，这个世界也并不是那么危险，这里的东西我都熟悉，大家都说俄语..."


translate chinese epilogue_dv_cdc9d191:


    "也许不会有什么坏事...?"


translate chinese epilogue_dv_bf9b8fdd:


    "我突然感觉非常难受。"


translate chinese epilogue_dv_910fe671:


    "见鬼，我以为昨天晚上就过去了呢!"


translate chinese epilogue_dv_098084b9:


    "我凭着本能找到厕所。"


translate chinese epilogue_dv_80b1d831:


    "过了一会儿，等我干完了自己的脏活儿，我回到了房间。"


translate chinese epilogue_dv_7d131181:


    "然后我意识到..."


translate chinese epilogue_dv_7b407504:


    "我回到了自己的公寓!"


translate chinese epilogue_dv_04cada73:


    "不过没有多老..."


translate chinese epilogue_dv_bdfdf404:


    "好像我离开了一个礼拜之后一点变化也没有。"


translate chinese epilogue_dv_e99eda04:


    "都没有多少尘土。"


translate chinese epilogue_dv_49074c47:


    "键盘的位置，显示器的角度，茶杯的污渍，一切都是原来的样子。"


translate chinese epilogue_dv_47eb912e:


    "理论上讲我这样的男人不应该注意到这种事情!"


translate chinese epilogue_dv_55584c51:


    "但是整个房间连最小的细节都吸引着我，好像闪烁着不同的颜色一样。"


translate chinese epilogue_dv_46f98924:


    "袜子都挂在当时的位置，被子也和我离开时一样的摊在床上，脏脏的窗户还是映照着相同的街景。"


translate chinese epilogue_dv_76d36664:


    "我徒劳的试图变得恐惧。"


translate chinese epilogue_dv_185107f9:


    "也许是灾难后创伤?"


translate chinese epilogue_dv_dc9713bf:


    "不管怎么说，几个小时以前我都没有考虑过可能返回现实世界。"


translate chinese epilogue_dv_ce3c1db8:


    "少先队夏令营，女孩儿们，阿丽夏，神秘的城镇中心，如果我没有回到自己的房间，可能就去了那里吧，我都开始觉得那些是真实的了。"


translate chinese epilogue_dv_3b4fb89e:


    "也许我前往小猫头鹰夏令营的奇妙之旅只是一场梦?"


translate chinese epilogue_dv_bb1b5b1b:


    "我开始在房间里来回踱步。{w}我背着手反复思考着发生的各种事情。"


translate chinese epilogue_dv_05941cbe:


    "我只能确定一件事情，就是我突然出现在了那里，又突然返回了自己家。"


translate chinese epilogue_dv_b0e545e2:


    "没有任何理由和逻辑。"


translate chinese epilogue_dv_c2f030da:


    "也许有某种解释，但是也不是一般的人类可以理解的了的。"


translate chinese epilogue_dv_54e489ca:


    "到底是神秘的外形力量，还是谁的意念控制，还是政府做的实验，都一点线索也没有。"


translate chinese epilogue_dv_94c0ae0c:


    "如果我在夏令营里找不到线索，那这里就更没有可能弄清楚了。"


translate chinese epilogue_dv_f55be16f:


    "就好像是在烧成灰烬的房间里寻找真相。"


translate chinese epilogue_dv_077e08fa:


    "即使可以确认罪行，所有的证据也都已经被毁了。"


translate chinese epilogue_dv_be44e243:


    "我要是一个法医的话可能还有戏，但是..."


translate chinese epilogue_dv_dea456b1:


    "我手头仅有的证据就是这一个星期的记忆。"


translate chinese epilogue_dv_9362d886:


    "但是这不只是一个梦，我可以确定!"


translate chinese epilogue_dv_5bfae656:


    "我的宿醉绝对是真的，我的感情，感觉，那些都是真的，我可以确定。"


translate chinese epilogue_dv_a78cb617:


    "所以说我真的去过那里，在小猫头鹰夏令营，和辅导员，少先队员们，还有阿丽夏。"


translate chinese epilogue_dv_a2226b0b:


    "回忆起她我不断的叹息。"


translate chinese epilogue_dv_42975286:


    "如果能和她一起走多好..."


translate chinese epilogue_dv_78c33026:


    "等一下!{w}我这是怎么回事?!{w}我回到家应该高兴啊!"


translate chinese epilogue_dv_db34fc11:


    "难道不应该...?"


translate chinese epilogue_dv_0b94b031:


    "我现在就是不能很好的估计形势。"


translate chinese epilogue_dv_2f3f1dc7:


    "我不知道自己是应该因为终于实现了回家的心愿而高兴还是本来应该享受在那里的生活。"


translate chinese epilogue_dv_d091ab38:


    "我看到桌子上有一个5卢布的硬币，我拿起来毫无意义的扔着，一边数着正反面的数目。"


translate chinese epilogue_dv_ba6c1813:


    "很久以前，好像在学校里听说硬币扔非常多次以后并不是一比一的。"


translate chinese epilogue_dv_b79ab5f5:


    "可能只是硬币本身的形状的问题吧。"


translate chinese epilogue_dv_0906061c:


    "我扔出来的结果是70比71."


translate chinese epilogue_dv_a3037b78:


    "所以那个理论说错了...{w}但是我也没有权利反驳。"


translate chinese epilogue_dv_6766bb1d:


    "我想起自己的童年，第一次听到“尾巴”这个说法的时候。"


translate chinese epilogue_dv_b5103a20:


    "不知怎么我总是会想到松鼠。"


translate chinese epilogue_dv_0dc3131c:


    "可能是有点道理的，松鼠的确是有尾巴的。"


translate chinese epilogue_dv_31eac3c5:


    "这个结论现在听起来有点奇怪，但是也找不到什么理由来反驳。"


translate chinese epilogue_dv_ba47df6c:


    "所以说松鼠比头多吗?"


translate chinese epilogue_dv_736afd2f:


    "但是需要想到松鼠也是有头的，..."


translate chinese epilogue_dv_80525988:


    "我扯着自己的头发，叹叹气。"


translate chinese epilogue_dv_73fcc8eb:


    "现在不是思考这些乱七八糟东西的时候!"


translate chinese epilogue_dv_51221043:


    "不过话说回来，现在是干什么的时候，我到底能干得了什么?"


translate chinese epilogue_dv_4c4bbb47:


    "如果我一个礼拜都没有注意寻找过答案，那现在思考又有什么用呢?"


translate chinese epilogue_dv_bc27a2ca:


    "也许我就应该开始自己正常的生活...?"


translate chinese epilogue_dv_6ce763b8:


    "“正常的生活”这个词语让我很不舒服。"


translate chinese epilogue_dv_3ceed523:


    "我为什么用了这个说法，为什么没有说“过去的”“原本的”?"


translate chinese epilogue_dv_4e6dae94:


    "可能是我觉得那是自己习惯的吧。"


translate chinese epilogue_dv_1e1bd7d8:


    "就像有人说的，习惯就成了自然。"


translate chinese epilogue_dv_87ae0d50:


    "上周所有的事情都开始浮现在我的面前。"


translate chinese epilogue_dv_1614d0af:


    "我真的那么想留在那里吗?"


translate chinese epilogue_dv_b3b1bf62:


    "一个星期够奥尔加·德米特里耶夫娜对我的指指点点吗，还包邮乌里扬娜的恶作剧，阿丽夏的讽刺?"


translate chinese epilogue_dv_612f35d1:


    "我没有答案，现在我已经错过了。"


translate chinese epilogue_dv_07307489:


    "虽然我确定自己再也不能返回。"


translate chinese epilogue_dv_da4fdd9d:


    "我都说不清把我带到那里的那种力量是不是只想耍我..."


translate chinese epilogue_dv_06277978:


    "有可能。"


translate chinese epilogue_dv_eccd0d95:


    "现在我只有这个，一段我一生都会记得的历险。"


translate chinese epilogue_dv_b8699e2f:


    "见鬼！多少人有过这种经历，即使是梦里都很难有吧!"


translate chinese epilogue_dv_e7a9c5b6:


    "最主要的是这只是一个普通的少先队夏令营。"


translate chinese epilogue_dv_41392bfa:


    "本来很普通，但是我突然到了那里就不太普通了。"


translate chinese epilogue_dv_4f464055:


    "不管怎么说，那也不是一生中最糟糕的经历，而且还有可能是最美妙的一次经历!"


translate chinese epilogue_dv_1a6feebe:


    "遗憾的是已经结束了，但是什么事情都会有结尾。"


translate chinese epilogue_dv_aa9a36c1:


    "这样结束也不错，谁知道还有可能发生什么事?"


translate chinese epilogue_dv_ce617998:


    "…"


translate chinese epilogue_dv_4bbd270b:



    nvl clear
    "随着时间的流逝，夏令营里发生的事情越来越像是一部小说。"


translate chinese epilogue_dv_5faded9b:


    "但是我并没有忘记，相反我记得所有的细节。"


translate chinese epilogue_dv_2a6f7dd3:


    "我还记得小时候在被子里面用手电筒照着宇宙飞船的故事看。"


translate chinese epilogue_dv_ba6313e4:


    "有时候我没法起床上学，父母总是批评我，但是夜晚降临以后我还是会小心的翻动书页欣赏里面的每一处细节。"


translate chinese epilogue_dv_b9465d76:



    nvl clear
    "对于少先队夏令营我也有差不多的感受，那种现实，在现在看来更像是一本小说。"


translate chinese epilogue_dv_84be21a5:


    "但是我觉得这样很不错，到最后，主要情节都记住了，而究竟是通过什么媒介也就不重要了。"


translate chinese epilogue_dv_a20cefa7:


    "…"


translate chinese epilogue_dv_0775f137:



    nvl clear
    "现在每一辆公交车都会让我同时感到恐惧和希望。"


translate chinese epilogue_dv_41f919cf:


    "开始我经常在410路公交车附近无意义的踱步，我有好几次在同一把椅子上睡着。"


translate chinese epilogue_dv_c066fcc3:


    "但是什么都没有发生，连一点超自然的影子都没有。"


translate chinese epilogue_dv_a3314542:


    "时间不断的流逝，我也停止了这种奇怪的行为。"


translate chinese epilogue_dv_ce617998_1:


    "…"


translate chinese epilogue_dv_32ab7558:



    nvl clear
    "我的生活改变了!"


translate chinese epilogue_dv_d3186f16:


    "就这样过了几个星期，我从角落里找出了自己的吉他。"


translate chinese epilogue_dv_77eccfd0:


    "我弹了几个和弦，想到了阿丽夏，对我来说，她和吉他是分不开的。"


translate chinese epilogue_dv_56c6309a:


    "开始进展不太顺利，可能我真的没有音乐天赋，我开始看自学书籍，还有视频。"


translate chinese epilogue_dv_f6f2a4ed:


    "不过所有的努力都不再是我的负担了，我就这样不停的练习着。"


translate chinese epilogue_dv_30a27c7e:


    "而且不管是什么乐器，只要练习就能不断进步。"


translate chinese epilogue_dv_56ead4c3:


    "过了半年时间，我发现自己已经取得了一些进展，至少可以流畅的弹出自己喜欢的曲子了。"


translate chinese epilogue_dv_07a11c4d:



    nvl clear
    "我对自己的进展很满意，决定组建自己的乐队。"


translate chinese epilogue_dv_47a62878:


    "我还不知道自己想要弹奏怎样风格的音乐。"


translate chinese epilogue_dv_ede9ef2c:


    "这本来对我来说是不可能的。"


translate chinese epilogue_dv_c41adf0a:


    "我得开始和陌生人交流! 万一我们太不一样呢? 而且我怎么和别人讲出自己的思想呢?"


translate chinese epilogue_dv_2510a2ef:


    "但是结果都很容易。"


translate chinese epilogue_dv_41b1dc0a:



    nvl clear
    "我的伙伴没有那么吓人，我们的爱好也很相似。"


translate chinese epilogue_dv_367522a6:


    "不管怎么说我们很相似，所以目标上的问题还是很好解决的。"


translate chinese epilogue_dv_03fe9757:


    "很快我们成立了乐队。"


translate chinese epilogue_dv_7c51a82e:


    "我突然发现了自己的作曲才能。"


translate chinese epilogue_dv_6e2dd77c:


    "所以我开始夜以继日的谱写歌曲，交给乐队的伙伴们。"


translate chinese epilogue_dv_2e540bca:



    nvl clear
    "很快我们进行了自己的演出。"


translate chinese epilogue_dv_1ab31e80:


    "一个四流社团，场地和我的卧室差不多，观众好多都不太清醒，但是我很高兴。"


translate chinese epilogue_dv_dc28a178:


    "因为这是我们朝着成功迈出的第一步!"


translate chinese epilogue_dv_b6db561e:


    "但是之后过了很久都没有人邀请我们。"


translate chinese epilogue_dv_ac45d183:


    "我们费了很大力气录了三张专辑发布在了网上，粉丝也实在是没有什么热情，但是我一点都不伤心，这只是开始。"


translate chinese epilogue_dv_b178ebc9:



    nvl clear
    "有人可能觉得我们的乐队只是一个休闲娱乐的，但是我是认真的对待的。"


translate chinese epilogue_dv_74103c40:


    "因为乐队是我建立的，曲子也都是我写的。"


translate chinese epilogue_dv_78c27567:


    "我对自己的作品要负责人，我把休息时间都用来练习，过了一年以后我已经可以自豪的说自己不是世界上最糟糕的吉他手了。"


translate chinese epilogue_dv_1a44fbb5:



    nvl clear
    "然后突然开始流行了起来了。"


translate chinese epilogue_dv_425eb152:


    "我们每周都举行演出，也可以拿到工资了，虽然只有每个人几百卢布但是我不在乎，因为有人欣赏我们的作品，愿意花钱来听，这才是最重要的。（虽然说我们往往是为其他的乐队做暖场）"


translate chinese epilogue_dv_ce617998_2:


    "…"


translate chinese epilogue_dv_6e06d90f:


    "我坐在屏幕前，折腾着一把吉他。"


translate chinese epilogue_dv_5d68b7b8:


    "即将到来的演出在电脑的日历上用大大的记号标了出来。"


translate chinese epilogue_dv_b8653b2e:


    "这让我起了一身鸡皮疙瘩。"


translate chinese epilogue_dv_42802d30:


    "这是我们第一次作为主角演出。{w}我们还要有演出专辑。"


translate chinese epilogue_dv_89bb111d:


    "我很好奇到底会卖出多少光盘呢?{w}CD的时代已经过去了，但是总还是有一些爱好者的。"


translate chinese epilogue_dv_78b39126:


    "有的人可能为了支持乐队还是会买的。"


translate chinese epilogue_dv_67aef320:


    "我又确认了一下日期。{w}就是我从小猫头鹰回来整整一年之后。"


translate chinese epilogue_dv_299e685a:


    "显示屏上映着我的笑容。"


translate chinese epilogue_dv_ed82539c:


    "不知道阿丽夏会不会喜欢我的音乐，如果她也在这里的话，不过大概是不可能的!"


translate chinese epilogue_dv_1c7acd8a:


    "她喜欢经典的苏联摇滚，不过谁知道呢..."


translate chinese epilogue_dv_1e250ea6:


    "我弹奏了几个复杂的形式，感觉很满意，我觉得自己已经超过了“乌拉尔”吉他的水平还有当时很多的吉他手。"


translate chinese epilogue_dv_663b0360:


    "我很好奇如果没有经历过这些历险我有没有可能达到今天的高度?{w}好像是不太可能的..."


translate chinese epilogue_dv_4e2974c6:


    "这也没有什么奇怪的。很多人经历了极端的事情以后都会重新思考他们的人生。"


translate chinese epilogue_dv_82a1b7c9:


    "当然，那时也没有发生什么危险的事情，心理危机比生命危机好得多。"


translate chinese epilogue_dv_3c4da4a0:


    "我又排练了一下自己的曲子，然后把吉他放回盒子，收拾好所有的东西，走向出口。"


translate chinese epilogue_dv_199c3750:


    "外面很冷，就像那天一样..."


translate chinese epilogue_dv_e801f24b:


    "我慢慢来到了410路公交车站。"


translate chinese epilogue_dv_55024429:


    "真是巧啊，我正是要坐这辆车前往俱乐部。"


translate chinese epilogue_dv_11dda01e:


    "我的心沉了下去，我开始倒数。"


translate chinese epilogue_dv_a0a306c4:


    "很快公交车抵达了，打开门吸入了几位乘客。"


translate chinese epilogue_dv_fbe14004:


    "和那天一样，冬天的夜晚，即将入睡的城市，还有不时闪烁的车头灯让人保持些许清醒。"


translate chinese epilogue_dv_688e4dad:


    "距离市中心越近，路上的人越多。"


translate chinese epilogue_dv_ca09470b:


    "有的人在欢乐的交谈着，有些人观看着精品店的橱窗，匆忙的跑着，对周围的事情一点都不在意。"


translate chinese epilogue_dv_25cb0d1d:


    "我今天一点都不想再回去那个夏令营了。{w}也许以后有机会吧，但是今天就算了!"


translate chinese epilogue_dv_535c0d02:


    "我一定要完成演出。"


translate chinese epilogue_dv_9b124b7d:


    "我的目标很明确，其他的都要让开!"


translate chinese epilogue_dv_ce617998_3:


    "…"


translate chinese epilogue_dv_3ddfa4cf:


    "这个俱乐部显然要大得多，但是这时还是空空的，{w}现在正在声音检查。"


translate chinese epilogue_dv_633c3b22:


    "我透过扩音器听不清鼓的声音，我急切的想要告诉调音师傅。"


translate chinese epilogue_dv_d28a96c6:


    "调音完成，各种设备已就绪，节目单准备好了，可以开始了吗？"


translate chinese epilogue_dv_ab38f79d:


    "今天很特殊，我们不是来做暖场的，这次是别人为我们暖场，我们是压轴的。"


translate chinese epilogue_dv_2dc1b4a6:


    "听到第一个乐队的演奏时，我注意到他们还有好多需要改进的地方。"


translate chinese epilogue_dv_e867ef2d:


    "不过话说回来，我们不久以前也就是那个意思。"


translate chinese epilogue_dv_3561335d:


    "距离我们的表演越来越近了，我越来越紧张。"


translate chinese epilogue_dv_b07e6f10:


    "天哪，我简直就是在不停的发抖!"


translate chinese epilogue_dv_4daf3bc3:


    "我决定不再听下一个乐队的表演，而是在更衣间等着。"


translate chinese epilogue_dv_d6ae003b:


    "看看我的伙计们的表情，他们似乎也和我一样紧张。"


translate chinese epilogue_dv_00e13f2c:


    "我们沉默了，其实我们也没有什么可说的。"


translate chinese epilogue_dv_8986e3df:


    "我们每个人早就熟悉了自己的工作了。"


translate chinese epilogue_dv_a7cff533:


    "只有紧张可以影响我们，但是我们已经有了经验，所以一切都没有问题的。{w}至少我是这样安慰自己的。"


translate chinese epilogue_dv_ce617998_4:


    "…"


translate chinese epilogue_dv_da0729f4:


    "现在，终于，时间到了!"


translate chinese epilogue_dv_40b909bb:


    "我不记得我们是怎么上台和观众打招呼了。"


translate chinese epilogue_dv_a65dc811:


    "我们的第一声和弦在扩音器中出现的时候，我终于恢复了清醒。"


translate chinese epilogue_dv_58bfcc73:


    "我在等候时着急的温暖自己的手指，但是现在它们流利的在琴弦之间游走，好像被施了魔法一样。"


translate chinese epilogue_dv_15afa28b:


    "我突然闪过了一个想法——为什么我连一个很小的错误都没有犯呢?"


translate chinese epilogue_dv_76c871e1:


    "我在家练习还有排练的时候都没有想过。"


translate chinese epilogue_dv_b60a105d:


    "但是当我站在观众面前的时候感觉完全不一样了。"


translate chinese epilogue_dv_a8ba8d2a:


    "内心深处我总是觉得自己的手指马上就要滑落...{w}谢天谢地这种想法没有影响到我的演出!"


translate chinese epilogue_dv_9819f44a:


    "我们的演出马上就要结束了，该到了我个人独奏了，我为此已经练习了很久。"


translate chinese epilogue_dv_e621a17f:


    "我的紧张情绪消失了，我演奏的比排练时还要好。"


translate chinese epilogue_dv_ed12c375:


    "我们的演出终于结束了，观众中爆发出了掌声。"


translate chinese epilogue_dv_552e80e1:


    "我还有乐队成员深深的鞠躬。"


translate chinese epilogue_dv_4ff24831:


    "他们大喊着“再来一个”，但是我们是一个刚刚成立的乐队，我们已经把所有的歌曲都演奏了一遍了。"


translate chinese epilogue_dv_ce617998_5:


    "…"


translate chinese epilogue_dv_485788f0:


    "我们十分兴奋的在更衣室拥抱着，大厅里都是打开香槟的声音。"


translate chinese epilogue_dv_6ab4e9e3:


    "更让我高兴的是，我们的专辑都卖光了。"


translate chinese epilogue_dv_1e7ce85a:


    "我终于也做成了一点事情!"


translate chinese epilogue_dv_a744e489:


    "要预测我们乐队的未来还很不容易。"


translate chinese epilogue_dv_7c912a9c:


    "但是有意见事情是确定的，我终于找到了可以投入一生的爱好。"


translate chinese epilogue_dv_701ac863:


    "而且这个爱好可以让不止我一个人开心。"


translate chinese epilogue_dv_32dffc6c:


    "我现在可能是世界上最高兴的人!{w}至少这是我生命中最开心的时刻。"


translate chinese epilogue_dv_ce617998_6:


    "…"


translate chinese epilogue_dv_abdb027b:


    "观众们陆续离开，我们的设备也都收拾好了，我等着其他人一起回家。"


translate chinese epilogue_dv_290d0099:


    "我看着自己一个小时以前演奏的那个舞台。"


translate chinese epilogue_dv_46ed0349:


    "欣喜和兴奋之情再度充满我的心房。"


translate chinese epilogue_dv_b9afffc3:


    "我确定这不会是我最后一次登上这个舞台。"


translate chinese epilogue_dv_d3215206:


    "也许未来我还可以登上更大的舞台。谁知道呢？{w}现在一切皆有可能!"


translate chinese epilogue_dv_1e17703e:


    "我和同伴们纷纷道别，然后跑到公交车站，很幸运的赶上了410路公交车。"


translate chinese epilogue_dv_748039c5:


    "里面没有人，我找了一个司机后面的位置，就和那个时候一样。收音机里传来一些熟悉的旋律。"


translate chinese epilogue_dv_5ef8eb1d:


    "也许未来的某一天这里也能播放我的音乐。"


translate chinese epilogue_dv_6570bb77:


    "这和一年前的事情有些类似之处。"


translate chinese epilogue_dv_76a32943:


    "现在一切都是可知的，都是通过努力得来的。"


translate chinese epilogue_dv_9a35ddf5:


    "我已经是一个不同的人了。"


translate chinese epilogue_dv_674125ad:


    dvg "嘿!"


translate chinese epilogue_dv_1b10feb5:


    "我转过身，看到一个女孩儿站在附近。"


translate chinese epilogue_dv_cf74f4a4:


    "她的声音不是很有礼貌，也不是太过冒犯。"


translate chinese epilogue_dv_c2b1de8c:


    dvg "我喜欢你的表演!"


translate chinese epilogue_dv_73aab3cd:


    me "谢谢..."


translate chinese epilogue_dv_6b60c623:


    "俱乐部了很黑，我看不清她的脸，但是我确定是一个我很熟悉的面孔。"


translate chinese epilogue_dv_bd0cee6c:


    me "你是..."


translate chinese epilogue_dv_1d56b639:


    "我上前一步。"


translate chinese epilogue_dv_531bb64d:


    "真的！是阿丽夏!"


translate chinese epilogue_dv_e3137c54:


    "我呆在原地。"


translate chinese epilogue_dv_6a3ab760:


    dvg "我怎么了?"


translate chinese epilogue_dv_5b8f2192:


    me "没什么...只是觉得以前见过你。"


translate chinese epilogue_dv_ba143499:


    "我还是不要直接转移到少先队夏令营的话题。"


translate chinese epilogue_dv_6fa8d283:


    "不管怎么说这个女孩儿可能只是看起来很像..."


translate chinese epilogue_dv_6827cc68:


    "而且她很显然是第一次见到我，或者是装作这样..."


translate chinese epilogue_dv_0f19819d:


    dvg "嗯，有一些道理..."


translate chinese epilogue_dv_ae3e36fe:


    "她眯起眼睛端详着我。"


translate chinese epilogue_dv_5a477656:


    me "我叫谢苗！"


translate chinese epilogue_dv_0c70048e:


    "我使劲笑着，伸出手。"


translate chinese epilogue_dv_9cd81f75:


    dvg "其实我知道。"


translate chinese epilogue_dv_c0d3e816:


    me "怎么?"


translate chinese epilogue_dv_5c2b6b63:


    dvg "在网上看到的。"


translate chinese epilogue_dv_021e0faf:


    "啊，是啊，这当然了嘛。"


translate chinese epilogue_dv_339fce09:


    me "那你是...?"


translate chinese epilogue_dv_be5fc071:


    dvg "阿丽夏..."


translate chinese epilogue_dv_1ebb64bd:


    "她有点困惑的回答着。"


translate chinese epilogue_dv_04cf27b7:


    "所以我猜对了，但是到什么程度呢...?"


translate chinese epilogue_dv_2ec84ac1:


    me "我说，你小时候有没有参加过少先队夏令营?"


translate chinese epilogue_dv_ed23c5e1:


    dv "没...没有...{w}你问这个干什么?"


translate chinese epilogue_dv_e1ab806a:


    me "只是...{w}我觉得自己在那里见过你。"


translate chinese epilogue_dv_98abf1e8:


    dv "我都没有去过你怎么会见到我的?"


translate chinese epilogue_dv_ec2c2a7c:


    "她笑了起来。"


translate chinese epilogue_dv_85d7f47f:


    dv "哈哈，你们这些音乐家都很奇怪!"


translate chinese epilogue_dv_7bd57e71:


    me "是啊。"


translate chinese epilogue_dv_98885eac:


    dv "那个...我只是想说你的音乐还不错。"


translate chinese epilogue_dv_0c7cecbc:


    "阿丽夏准备离开。"


translate chinese epilogue_dv_dadc6a74:


    "我觉得自己有必要说点什么。"


translate chinese epilogue_dv_48603599:


    me "我们的下一场演出，你会来吗?"


translate chinese epilogue_dv_8e1cb694:


    dv "什么时候?"


translate chinese epilogue_dv_37503a95:


    "说实话我也不知道。"


translate chinese epilogue_dv_e56b5b6f:


    me "很快..."


translate chinese epilogue_dv_26dbb3bd:


    dv "嗯，有可能...到时候告诉我!"


translate chinese epilogue_dv_3b03af3a:


    me "嗯..."


translate chinese epilogue_dv_1cf7fbad:


    "我们交换了通讯地址。"


translate chinese epilogue_dv_b256ad93:


    dv "那就回头见了!"


translate chinese epilogue_dv_f57169e1:


    "她挥挥手离开了。"


translate chinese epilogue_dv_1e45c266:


    "我不知道她是不是就是我在夏令营见到的那个阿丽夏。"


translate chinese epilogue_dv_9835dc5e:


    "缺少证据，任何理论都无法成立。"


translate chinese epilogue_dv_c0c56290:


    "但是那个我现在不在乎，因为我重新找到了自我，既是由于自己的音乐，而且还见到了阿丽夏。"


translate chinese epilogue_dv_b44b240b:


    me "总之，我一定会给她打电话的，到时候..."


translate chinese epilogue_dv_7bdf75da:


    "我笑笑，背上吉他前往410路公交车站。"


translate chinese epilogue_sl_d4b0f999:


    "有些梦让你不愿意醒来。"


translate chinese epilogue_sl_63d02893:


    "好像在温暖的河上漂流，前往地平线的那一头，惬意的观察着云层中的整个世界。"


translate chinese epilogue_sl_f9b4fc62:


    "过去已经扔到了身后，不会再折磨你的灵魂。{w}未来就在这里，你需要伸出手摸到。"


translate chinese epilogue_sl_410a9fd5:


    "不管前面有什么等着我，自身平静而开心的接受这个世界是最重要的。"


translate chinese epilogue_sl_aa0f75b6:


    "我一直相信宇宙曾经是这样的，未来可能还会变成这样。"


translate chinese epilogue_sl_38f385ca:


    "我们的一生只是一瞬，对宇宙来说，我们的存在只是一秒，恒星的一生是一分钟，那些星系则是一个小时。"


translate chinese epilogue_sl_9c9a7284:


    "但是这些加在一起也到不了一天。"


translate chinese epilogue_sl_bd631aa1:


    "没有人会在乎怎样度过每一秒钟，有人会在乎如何珍惜河流中的某一滴水吗?"


translate chinese epilogue_sl_1f69fba6:


    "我睁开眼睛，惬意的伸着懒腰。"


translate chinese epilogue_sl_1210b0d8:


    "我不想站起来，感觉自己很舒服的裹在被子里。"


translate chinese epilogue_sl_590f6c27:


    "不管怎么说，我不必去任何地方。..."


translate chinese epilogue_sl_be7b0f0b:


    "我也没有什么计划。"


translate chinese epilogue_sl_003b132f:


    "为什么不能睡觉呢?"


translate chinese epilogue_sl_bd93e9e9:


    "我翻过身盯着墙上的旧壁纸。."


translate chinese epilogue_sl_0a1ee678:


    "我很好奇他们上一次整修还是什么时候，而且为什么木头墙上会贴上壁纸啊?"


translate chinese epilogue_sl_433cc26c:


    "我大脑中的铃声突然响起。"


translate chinese epilogue_sl_771e1b84:


    "我把被子扔开，激动的跳起来。"


translate chinese epilogue_sl_9b6e0d96:


    "对了，我回到了自己的公寓。"


translate chinese epilogue_sl_2f2ad9f9:


    "最初我惊呆了。"


translate chinese epilogue_sl_18a8a5cb:


    "这种冲击过于强大，让我一度不会思考。"


translate chinese epilogue_sl_ea18891a:


    "我就这样站着盯着电脑屏幕。"


translate chinese epilogue_sl_047fe1ee:


    "我不会思考了，我甚至忘记了呼吸。"


translate chinese epilogue_sl_3c6ecad1:


    "终于，我开始慢慢的恢复清醒。"


translate chinese epilogue_sl_fb89f263:


    "到底发生了什么?{w}我在车上睡着了，然后到达了城镇中心？{w}好像是那样的..."


translate chinese epilogue_sl_aa71a533:


    "斯拉维娅就坐在我旁边。然后我就回来了..."


translate chinese epilogue_sl_9762c4e5:


    "似乎是我在夏令营的时候对那里的生活太熟悉了，以至于突然回到现代世界我都不知道该干什么好。"


translate chinese epilogue_sl_a44e9759:


    "不管怎么说，最初我只是想要回家..."


translate chinese epilogue_sl_b4b3948b:


    "最后一切都像是糟糕的电影，我确实回来了，但是非常震惊。"


translate chinese epilogue_sl_af8879d8:


    "我一点也不害怕，反倒应该说是很有意思...也有点失落。"


translate chinese epilogue_sl_29f47261:


    "我都已经决定和斯拉维娅开始新的生活了，准备把我的过去都扔下了——屈辱，痛苦，自我反省，没有做完的事情，还有未来的计划。"


translate chinese epilogue_sl_57794fd0:


    "我觉得在那里的新生活不会比现实世界的更差，至少。"


translate chinese epilogue_sl_5c585492:


    "但是现在已经回不去了..."


translate chinese epilogue_sl_e0b5fc71:


    "反过来说，如果一个礼拜以前有人告诉我可以把我送到那样的世界去，我绝对不会相信，眼睛都不会眨一下的。"


translate chinese epilogue_sl_21b9702d:


    "那这种事情连续发生两次又有什么奇怪的呢?"


translate chinese epilogue_sl_d390fb44:


    "我拖着沉重的步伐走到厨房，接了一杯水回来。"


translate chinese epilogue_sl_b6e3af01:


    "冰冷的清水带有严重漂白粉味道，可以让我清醒一些。"


translate chinese epilogue_sl_e5e5f203:


    "我现在得决定下一步做什么。"


translate chinese epilogue_sl_41ef88b1:


    "我突然发现自己不能忍受这种安静。"


translate chinese epilogue_sl_80b426bb:


    "我打开电脑，随便点开一首歌曲，冷静了一些。"


translate chinese epilogue_sl_a0bf71cd:


    "说实话，我还能做什么?{w}最近我发现了这个真理，没有什么是我自己可以决定的。"


translate chinese epilogue_sl_a90c22ac:


    "某个人凭借自己的力量把我送到那个世界然后又送回来。"


translate chinese epilogue_sl_35575ac5:


    "在夏令营待的这一个星期我什么答案也没有找到。"


translate chinese epilogue_sl_c9372110:


    "现在再做推测又有什么用...?"


translate chinese epilogue_sl_d4e2d25c:


    "我应该把它当成是噩梦一样忘掉就好!"


translate chinese epilogue_sl_32fbcaae:


    "可能只是幻觉，我现在不在乎。"


translate chinese epilogue_sl_9e130725:


    "但是斯拉维娅让我没办法马上忘掉那个世界。"


translate chinese epilogue_sl_6d445c91:


    "我记起了她的微笑还有我们在一起的时光。"


translate chinese epilogue_sl_4ef0184b:


    "从第一天的见面到晚上在森林中的约会，还有营地的分别。"


translate chinese epilogue_sl_51bc49e7:


    "我的心脏跳个不停，让我的身体很难受。"


translate chinese epilogue_sl_36b7144d:


    "我看到她的第一眼就喜欢上了她。{w}那么善良，有爱，又善于理解我..."


translate chinese epilogue_sl_98add8a2:


    "就像是那个动画的女主角一样，叫什么名字来着? 帮助抑郁的反社会倾向的男主角的。（难道是指的欢迎加入NHK？= =）"


translate chinese epilogue_sl_264c0834:


    "仔细想想，现实生活中怎么可能有这种人!"


translate chinese epilogue_sl_5aa379da:


    "斯拉维娅不要求任何回报，不需要鼓励，对于自己的默默付出从来不要表扬。"


translate chinese epilogue_sl_0a40af6e:


    "她就是她自己——一个不可能在现实生活中出现的女孩儿。"


translate chinese epilogue_sl_c52a2b6d:


    "再想想清楚，她就是这样一个人。"


translate chinese epilogue_sl_bcc4565b:


    "看来我是做了一个有一个星期之久的梦。"


translate chinese epilogue_sl_057f727b:


    "少先队夏令营，苏联的青少年，辅导员，温暖的夏夜，篝火晚会，儿童游戏，最质朴的娱乐，一个持续一个星期的瞬间，一个永远的夏天。"


translate chinese epilogue_sl_dccb98b2:


    "但是这些不只是我看到的，我还去了那里，亲自参加其中!"


translate chinese epilogue_sl_d77755f6:


    "我的眼睛不争气的充满了眼泪。{w}不是绝望的眼泪，是忧愁的眼泪。"


translate chinese epilogue_sl_d1d01df5:


    "即使一切都真的结束了，我也还是经历了很多人做梦都见不多的事情。"


translate chinese epilogue_sl_dc042d8c:


    "斯拉维娅的身影在我的脑海中越来越明亮。"


translate chinese epilogue_sl_75314796:


    "我真想让她和我一起走..."


translate chinese epilogue_sl_b649e1a2:


    "可能我现在还什么都不能给她，但是我还有我的人生。"


translate chinese epilogue_sl_4d7ca51b:


    "我觉得自己从这些事情里学到了很多事情。"


translate chinese epilogue_sl_0b2cd74c:


    "我不禁觉得这些太完美了，好像是提前写好的一样。"


translate chinese epilogue_sl_ad88d4c0:


    "一个失败者，经历了奇妙的事件，然后脱胎换骨了..."


translate chinese epilogue_sl_eab34488:


    "我觉得这不太可能了，至少今生不太可能了..."


translate chinese epilogue_sl_a1f94223:


    "我在一天或者一周以后会成为当时那样的人吗?"


translate chinese epilogue_sl_e3775951:


    "这是最现实的事情。"


translate chinese epilogue_sl_84316324:


    "有一件事可以确定，那就是我再也不能回去了..."


translate chinese epilogue_sl_ce617998:


    "…"


translate chinese epilogue_sl_dfcf3261:



    nvl clear
    "距离我从小猫头鹰夏令营回来已经过去了一个月了，准确的说是距离我醒来。"


translate chinese epilogue_sl_65020c5a:


    "我继续着自己孤独的生活，继续上网，出门只去商店，我也意料之中的又适应了这种生活。"


translate chinese epilogue_sl_5b8761f3:


    "显然忧愁和压抑的时候也会出现但是也不会比平常长，除非是睡觉的时候，我回到那个夏令营，那个夏天..."


translate chinese epilogue_sl_c49c4047:


    "但是一旦醒来我就试图忘记这些想法，做白日梦有什么意义呢?"


translate chinese epilogue_sl_7bbf60a6:



    nvl clear
    "即使发生奇迹，它们也是独立发生的，不管怎么说，甜这个字也不会真的让你有甜的感觉的。"


translate chinese epilogue_sl_9a22d402:


    "但是我还是有了一些改变，过去，我从来不去思考未来，不会在意自己还能活多长时间，是一个星期还是40年，但是现在我可以乐观的看待这些事情了。"


translate chinese epilogue_sl_5c734fc5:


    "我不是想要改变什么或者变成完全不同的人，但是这个世界似乎更有道理了。没有变的多么友好，但是更容易理解了。"


translate chinese epilogue_sl_d59449dc:


    "以前有些事情我就是做不到，觉得他们发生都是有道理的，但是现在我觉得生活很简单，一切都会好起来的!"


translate chinese epilogue_sl_e1feb5f7:



    nvl clear
    "至少事情发生了我也可以忍受，我开始露出笑容，不再一天24小时一副苦大仇深的表情。"


translate chinese epilogue_sl_a49c2c8d:


    "连我的朋友们（当然是在网上的）也注意到了我的变化，他们也觉得这样很好。"


translate chinese epilogue_sl_cffd8fe2:


    "现在我觉得自己可以继续下去了，感谢小猫头鹰夏令营。{w}如果没有他们我是做不到的..."


translate chinese epilogue_sl_ce617998_1:


    "…"


translate chinese epilogue_sl_76b2fe88:


    nvl clear


translate chinese epilogue_sl_61db16fc:


    "那是一月初。"


translate chinese epilogue_sl_0d80cbdc:


    "整个城市还没有从新年中恢复过来，白天街道也是空空的，更不要说晚上了，街上仅有的几个行人看起来也是匆匆忙忙的，为了不知道什么事情赶着路。"


translate chinese epilogue_sl_f357f57f:


    "我没有这种理由，我只是想随便逛逛，我还是需要不时地获得一些新鲜空气的。"


translate chinese epilogue_sl_6c6bdb78:


    "冬天的夜晚是最适合一个人的。"


translate chinese epilogue_sl_8cf56a1b:



    nvl clear
    "我一直觉得大隐隐于市，真正的独居在纷乱的人群中而不是在大沙漠，大平原或者是大雪山。"


translate chinese epilogue_sl_761bb15b:


    "在人群中，言语中，思想中，愿望中，每个人都有自己的方向。"


translate chinese epilogue_sl_2ffddc18:


    "我在其中自由的漫步，无欲无求。"


translate chinese epilogue_sl_c5c605e3:


    "也许它们是朝着不同方向的向量？ 在笛卡尔坐标系下不会相遇。"


translate chinese epilogue_sl_e35f54cf:



    nvl clear
    "这里还是一样的，同样的人在跑步，他们朝着一个方向跑着，我则在另外的方向上。"


translate chinese epilogue_sl_6ec15f5e:


    "但是我觉得孤独吗？过去，也许是的，但是现在，已经不是了。"


translate chinese epilogue_sl_ad5015f4:


    "在灯火通明的城市中，在车水马龙的噪音中，我享受着寂静的交响乐!"


translate chinese epilogue_sl_c26cab4f:


    "但是现在情况有所不同了，街道上只有我一个人，所以现在不只是我是自己一个人，整个世界都是。"


translate chinese epilogue_sl_804dc10a:



    nvl clear
    "在沙漠里做一粒沙子和在大海中做一滴水是不一样的!"


translate chinese epilogue_sl_cc6ceeaf:


    "在人群中没有人注意我，但是现在透过窗户偷偷看着我的人还有开车路过的人会想：他为什么不回家？也许是有什么急事？也许他发疯了？也许他是一个酒鬼？..."


translate chinese epilogue_sl_87c3c17d:


    "我不喜欢特立独行，所以并没有反社会这个选项，对我来说。"


translate chinese epilogue_sl_1e8b6cba:


    "但是现在情况不一样了。"


translate chinese epilogue_sl_c230e647:



    nvl clear
    "好像我在向所有人说：看！我也可以开心的！你需要你的电视还有热巧克力，但是我只需要雪，夜晚，一个黑暗的房间，还有一个无聊的显示器，而且我一点也不比你糟糕！"


translate chinese epilogue_sl_53d3760b:


    "我的内心感到悲凉，但是是一种宁静的悲凉。"


translate chinese epilogue_sl_bb50df60:


    "我真的需要那些他们有我没有的东西吗?"


translate chinese epilogue_sl_7d88b51c:


    "也许我有一些完全更有价值的东西?"


translate chinese epilogue_sl_ce617998_2:


    "…"


translate chinese epilogue_sl_950f1be7:


    "我没有注意到自己怎么就来到了410路公交车站。"


translate chinese epilogue_sl_7535addf:


    me "这让我回忆起了很多事情..."


translate chinese epilogue_sl_8da3fa57:


    "我坐下来翻着口袋，想要找出香烟来。{w}然而我的口袋是空的。"


translate chinese epilogue_sl_a54cf057:


    me "好吧..."


translate chinese epilogue_sl_fd5a0f69:


    "我揉了揉扔进了垃圾桶。"


translate chinese epilogue_sl_a81c77bf:


    me "这可能对我的健康有益！"


translate chinese epilogue_sl_a97808f5:


    "天空中一颗孤零零的小星星闪烁着。"


translate chinese epilogue_sl_264e6e2b:


    "我今天才在网上看到，一个壮观的天文现象已经停止了，我们要想再次见到就要等几十年以后了。"


translate chinese epilogue_sl_e61de771:


    "也许这颗星星也已经爆炸了，我们看到的只是它曾经的辉煌？星际边缘的废弃的星球。"


translate chinese epilogue_sl_7ad4fe45:


    "然而，事情为什么会发生?"


translate chinese epilogue_sl_28b32374:


    "是因为它就在那里，可以触摸到?"


translate chinese epilogue_sl_c62c06d1:


    "还是因为我们相信它存在?"


translate chinese epilogue_sl_604959ba:


    "最开始感觉答案很简单，但是..."


translate chinese epilogue_sl_13a6a7c7:


    "即使这颗星星已经消失，我们还是可以看到它的光芒。"


translate chinese epilogue_sl_21b7ddca:


    "也许它指引着什么人走出了白雪皑皑的森林，给了他希望，给了他温暖。"


translate chinese epilogue_sl_19248a5c:


    "一个不知道什么时候爆炸的星星都能做得到这些事情吗?{w} 有没有可能几十亿人都相信一个根本不存在的东西?"


translate chinese epilogue_sl_311843a1:


    "但是相信本身不会创造出任何实际的东西，反正我就没有见过。"


translate chinese epilogue_sl_ce617998_3:


    "…"


translate chinese epilogue_sl_8844dc3d:


    "雪越下越大，现在已经是暴风雪了。"


translate chinese epilogue_sl_d1e598ce:


    "我的马车会变成南瓜吗?"


translate chinese epilogue_sl_f2165086:


    "也许我从小猫头鹰回来也就意味着我的灰姑娘的故事结束了?"


translate chinese epilogue_sl_ced18126:


    "即使是这样，这真的是一个悲剧的结局吗?"


translate chinese epilogue_sl_9f4f4f69:


    "为什么，任何故事都会有一点真实的东西的。"


translate chinese epilogue_sl_f7a557f1:


    "我想起斯拉维娅，站起来，抖掉身上的雪，然后慢慢走向黑夜。"


translate chinese epilogue_sl_1eaf8011:


    "我感觉不到痛苦，感觉不到悔恨。"


translate chinese epilogue_sl_aabe0924:


    "也许这只是那篇故事的下一个章节，虽然我当时那么想把这个故事继续下去。"


translate chinese epilogue_sl_498f7c4e:


    "既然它已经发生了一次了，为什么不能再发生一次呢?"


translate chinese epilogue_sl_8ebdfc5e:


    "而且如果我在现实中见到斯拉维娅呢...?"


translate chinese epilogue_sl_a20cefa7:


    "…"


translate chinese epilogue_sl_8074d8af:


    "我准备离开，但是突然从雪夜中出现了一个人，跑到了车站下面。"


translate chinese epilogue_sl_96af921b:


    "显然是一个女孩儿，还梳着长长的辫子。"


translate chinese epilogue_sl_81772974:


    "她背对着我，所以我看不见她的脸。"


translate chinese epilogue_sl_501a8b3a:


    slg "抱歉，请问最后一班车已经走了吗?"


translate chinese epilogue_sl_454f3f2b:


    "她问我，但是没有转过来。"


translate chinese epilogue_sl_14528e2c:


    "她这么晚还能去哪里?"


translate chinese epilogue_sl_0a4debed:


    "410路一定是被诅咒的路线。"


translate chinese epilogue_sl_6e4ac050:


    "不但把人带到另一个现实里去，而且你最需要它的时候，它总是不来。"


translate chinese epilogue_sl_451ccc51:


    me "还有一趟车，就在午夜之后。"


translate chinese epilogue_sl_d722e8b7:


    slg "谢谢你。"


translate chinese epilogue_sl_839f9131:


    "她微笑着说。"


translate chinese epilogue_sl_58d296bc:


    "我坐下来看着这个女孩儿。"


translate chinese epilogue_sl_09bca910:


    "最后她说："


translate chinese epilogue_sl_ffb89fb4:


    slg "你这么晚了要去哪里呢?"


translate chinese epilogue_sl_a945e3a8:


    "她真的觉得这个很有意思吗?"


translate chinese epilogue_sl_68e9d064:


    me "散散步而已..."


translate chinese epilogue_sl_f2f10b4d:


    "我若有所思的回答着。"


translate chinese epilogue_sl_9eb3ac16:


    me "你呢...你有什么重要的事情吗?"


translate chinese epilogue_sl_a8743c64:


    slg "嗯，其实没有...我准备回家。"


translate chinese epilogue_sl_591089ca:


    me "你住在哪里?"


translate chinese epilogue_sl_9a483b3c:


    "我不禁问道。"


translate chinese epilogue_sl_f9bd2f63:


    slg "很远的地方..."


translate chinese epilogue_sl_99a29ab7:


    "她回答的很模糊。"


translate chinese epilogue_sl_64f7f202:


    me "我就住在附近。"


translate chinese epilogue_sl_a7590312:


    slg "这是个好地方。"


translate chinese epilogue_sl_c75aa891:


    "要我说这儿可是最差劲的了。"


translate chinese epilogue_sl_263611b4:


    me "我没有什么选择..."


translate chinese epilogue_sl_13f66e78:


    slg "你不喜欢吗?"


translate chinese epilogue_sl_24cdca72:


    me "不知道，我现在已经习惯了。"


translate chinese epilogue_sl_837a1356:


    slg "我觉得人们不管在哪里都是可以开心起来的!"


translate chinese epilogue_sl_907b6d20:


    me "也许是这样吧..."


translate chinese epilogue_sl_2612f632:


    slg "嗯，你想想看!{w}连北极都会住人，{w}撒哈拉沙漠里都会有人! 还有很多地方，人们自己才是最重要的!"


translate chinese epilogue_sl_a82636de:


    me "确实很有道理。"


translate chinese epilogue_sl_76f34f02:


    "我的评论也很哲学，我迷迷糊糊的笑着。"


translate chinese epilogue_sl_f55284f9:


    slg "你听起来不太真诚。"


translate chinese epilogue_sl_724d66a5:


    me "为什么这么说，女士?"


translate chinese epilogue_sl_8baa587f:


    "她静静的笑着。"


translate chinese epilogue_sl_de9d3a74:


    slg "我觉得你是一个好人!"


translate chinese epilogue_sl_b4d548da:


    "然后她转过来，..."


translate chinese epilogue_sl_7f9ddde9:


    "我这才明白为什么她的声音，她的背影还有一切都那么熟悉。"


translate chinese epilogue_sl_5d2bdfb2:


    "斯拉维娅站在我面前。"


translate chinese epilogue_sl_d7bb50f0:


    "我一度不会说话了，但是只有一会儿。"


translate chinese epilogue_sl_30204e6b:


    "可能这只是一个看起来和我梦中的斯拉维娅很像的一个女孩儿，也可能是非常强烈的既视感..."


translate chinese epilogue_sl_7c085c9f:


    "这个城市不是夏令营，我也没有穿着少先队制服，现在也不是夏天。"


translate chinese epilogue_sl_a32736d4:


    "我试图证明这是幻觉，问了一个问题。"


translate chinese epilogue_sl_82ad3926:


    me "我们以前见过吗?"


translate chinese epilogue_sl_6ac4a2e6:


    "她仔细打量着我，然后笑了。"


translate chinese epilogue_sl_38e86f4c:


    slg "好像没有...但是你看起来很熟悉。"


translate chinese epilogue_sl_40a6ca02:


    me "您...你小时候有没有参加过少先队夏令营？"


translate chinese epilogue_sl_73927ac0:


    slg "当然没有!"


translate chinese epilogue_sl_ce3666e5:


    "她笑了。"


translate chinese epilogue_sl_c7fd5288:


    slg "我出生的时候就已经解散了的。"


translate chinese epilogue_sl_0d9f82ca:


    "啊，对了..."


translate chinese epilogue_sl_2d02b5b6:


    slg "不过，我最近做了一个梦。"


translate chinese epilogue_sl_60f3d758:


    "她思考了一下。"


translate chinese epilogue_sl_83d4403a:


    me "我也是，我是说一个少先队夏令营。"


translate chinese epilogue_sl_fe5d4dde:


    slg "咱们也许是在那里见面的吧。"


translate chinese epilogue_sl_e26e8716:


    "女孩儿严肃的说着。"


translate chinese epilogue_sl_fd914013:


    me "有可能。"


translate chinese epilogue_sl_4c079f2d:


    slg "对了，我叫斯拉维娅，其实我的全名叫斯拉维娅娜，但是大家都叫我斯拉维娅，你也可以这么叫!"


translate chinese epilogue_sl_b4002489:


    "我本来想要说我知道，但是我停顿了一下。"


translate chinese epilogue_sl_b54dd65c:


    "不管怎么说，这件事为什么很重要吗，现在斯拉维娅站在这个魔法的公交车站前面对着我?"


translate chinese epilogue_sl_a4217a2f:


    "有些梦你就是不想醒来!"


translate chinese epilogue_sl_94f8232c:


    me "我叫谢苗..."


translate chinese epilogue_sl_e62d08e6:


    sl "好可爱的名字。"


translate chinese epilogue_sl_6170b881:


    me "也是S开头的。"


translate chinese epilogue_sl_8b01f8be:


    sl "是啊。"


translate chinese epilogue_sl_8b7f6f51:


    me "你住的很远吗?"


translate chinese epilogue_sl_af4e649f:


    sl "嗯，也不是..."


translate chinese epilogue_sl_7cfc34fb:


    "她告诉我那个地区。"


translate chinese epilogue_sl_f14fd67f:


    me "我觉得那应该是南方。"


translate chinese epilogue_sl_70f37a53:


    sl "为什么?"


translate chinese epilogue_sl_e902ce8b:


    me "不知道...可能是你看起来像是少先队员。"


translate chinese epilogue_sl_90481d5f:


    sl "你还想着少先队员哪!"


translate chinese epilogue_sl_c954d69f:


    "斯拉维娅笑了。"


translate chinese epilogue_sl_ee42356b:


    me "如果不是因为这个咱们可能根本不会见面的。"


translate chinese epilogue_sl_70f37a53_1:


    sl "为什么?"


translate chinese epilogue_sl_cfc2bc6d:


    "她的表情严肃了起来。"


translate chinese epilogue_sl_e8b9f9a3:


    me "只是...开玩笑...{w}听着，你相信世界是因为我们相信才会存在的嘛?"


translate chinese epilogue_sl_17172b5c:


    sl "那好像是主观唯心主义..."


translate chinese epilogue_sl_bbc1f2e4:


    "她陷入了思考。"


translate chinese epilogue_sl_eb6cea32:


    me "上次我可能距离这个哲学学校很近。"


translate chinese epilogue_sl_5095dd33:


    sl "嗯，不太懂...我这些科目学得不太好。"


translate chinese epilogue_sl_e15fe545:


    me "你是学什么的呢?"


translate chinese epilogue_sl_d1046a49:


    "她笑了。"


translate chinese epilogue_sl_719e7c45:


    sl "别的专业。"


translate chinese epilogue_sl_7f708c46:


    me "这不是当然的嘛!"


translate chinese epilogue_sl_d1aa5e5c:


    sl "但是咱们在刚刚见面几分钟诶!"


translate chinese epilogue_sl_79ac6bd7:


    me "但是感觉已经好久了。"


translate chinese epilogue_sl_1cc42161:


    sl "我好像也有这种感觉..."


translate chinese epilogue_sl_cc1284f3:


    "一段尴尬的沉默。"


translate chinese epilogue_sl_644f4e1f:


    "雪逐渐停了，但是公交车还是没有来。"


translate chinese epilogue_sl_d86d06b8:


    "我看看表，认定这辆车是不会来了。"


translate chinese epilogue_sl_6a9e8821:


    "斯拉维娅好像更冷了，她的脸颊冻得通红，不停的摩擦着双手试图暖和起来。"


translate chinese epilogue_sl_161b1e49:


    me "今天好像没有410路了呢。"


translate chinese epilogue_sl_1555b86b:


    sl "好像是呢..."


translate chinese epilogue_sl_1e8379d1:


    "她伤心的说着。"


translate chinese epilogue_sl_732bddc6:


    me "如果你愿意的话..."


translate chinese epilogue_sl_8fe9063e:


    "我集中注意力，继续说下去。"


translate chinese epilogue_sl_e07c37b3:


    sl "什么?"


translate chinese epilogue_sl_beb50f2a:


    me "你可以来我家..."


translate chinese epilogue_sl_4f36139c:


    sl "但是咱们才刚刚见面诶。"


translate chinese epilogue_sl_0467a4c4:


    "她笑了。"


translate chinese epilogue_sl_bd3e21d9:


    "她好像一点也不害怕。"


translate chinese epilogue_sl_6b12c510:


    me "是啊，我明白，但是外面很冷..."


translate chinese epilogue_sl_d03f0acf:


    sl "你不害怕吗?"


translate chinese epilogue_sl_c93fe8e6:


    "斯拉维娅好像和我开着玩笑。"


translate chinese epilogue_sl_10f87319:


    me "我？我应该害怕吗?"


translate chinese epilogue_sl_b71c6525:


    sl "谁知道呢...谁知道呢..."


translate chinese epilogue_sl_58f5f795:


    me "我只是提出建议，没有想过那些其他的..."


translate chinese epilogue_sl_c99f65be:


    "我开始试图解释。"


translate chinese epilogue_sl_1ee2e52a:


    sl "我明白!"


translate chinese epilogue_sl_ec2c2a7c:


    "她又笑了。"


translate chinese epilogue_sl_0cc7ab77:


    sl "如果你不介意的话，当然..."


translate chinese epilogue_sl_7ec51fd8:


    "虽然看起来不太可能，但是她的脸更红了。"


translate chinese epilogue_sl_71f4991e:


    me "可以走了吗?"


translate chinese epilogue_sl_c1235c80:


    sl "咱们再等一会儿吧，也许会来也说不定呢。"


translate chinese epilogue_sl_71d3b8c8:


    me "好吧。"


translate chinese epilogue_sl_ce617998_4:


    "…"


translate chinese epilogue_sl_42acf572:


    sl "你空闲时间都干些什么?"


translate chinese epilogue_sl_ac2505d0:


    "过了一会儿斯拉维娅问我。"


translate chinese epilogue_sl_cebb61ed:


    me "我...那个...在家里工作，算是。"


translate chinese epilogue_sl_03bc366f:


    "我不能就这样告诉她我什么也不做。"


translate chinese epilogue_sl_36ffb439:


    sl "很不错!"


translate chinese epilogue_sl_75f4e79a:


    me "为什么?"


translate chinese epilogue_sl_d74e7a4a:


    sl "不知道...{w}你有很多空闲时间，而且哪里都不用去，这样..."


translate chinese epilogue_sl_b78c6e26:


    me "你呢?"


translate chinese epilogue_sl_b620325b:


    "我想要转换话题。"


translate chinese epilogue_sl_c42d7da0:


    sl "我在上学。"


translate chinese epilogue_sl_606db6d9:


    me "专攻民族学学位?"


translate chinese epilogue_sl_01f26b1d:


    sl "那是什么?"


translate chinese epilogue_sl_38a71a20:


    "斯拉维娅严肃的问着。"


translate chinese epilogue_sl_a52d1d58:


    me "不重要..."


translate chinese epilogue_sl_5bca72ca:


    sl "我想要做一名生态学家。"


translate chinese epilogue_sl_f19cb2e9:


    me "嗯，是一个值得自豪的职业..."


translate chinese epilogue_sl_ac8018d5:


    "我们还谈论了各种事情，但是公交车还是不来。"


translate chinese epilogue_sl_8231d531:


    "已经一点多了，时间过得很快。"


translate chinese epilogue_sl_e3127a21:


    "我觉得斯拉维娅明白我的意思了。"


translate chinese epilogue_sl_ad3d2e0c:


    sl "咱们走吧?"


translate chinese epilogue_sl_06ab0532:


    "她笑着。"


translate chinese epilogue_sl_a6a0a962:


    "我站起来朝自己家走去。"


translate chinese epilogue_sl_312acdec:


    "她小心的抱着我的胳膊，两眼看着我。"


translate chinese epilogue_sl_ae697d22:


    sl "你知道吗，我确定，和去年比起来，一切都会更好的!"


translate chinese epilogue_sl_a20cefa7_1:


    "…"
# Decompiled by unrpyc: https://github.com/CensoredUsername/unrpyc
