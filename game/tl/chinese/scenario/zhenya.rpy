
translate chinese zhenya_route_4060be9e:


    "请稍后再回来..."


translate chinese zhenya_route_c9a1f434:


    "我的世界是空空的，和我自己一样。"


translate chinese zhenya_route_f3335cb0:


    "我毁灭我自己，变成一个世界，一个空虚的世界毁灭了我..."


translate chinese zhenya_route_37955148:


    "我相信自己的手中有谁的命运，有毁灭他人的力量。"


translate chinese zhenya_route_408b289a:


    "但是我只是在黑暗中前行，徒劳的躲藏着一点点光明，光明只是在制造阴影。"


translate chinese zhenya_route_2d048c95:


    "每一处阴影都是我没有面部的投射，完全黑暗的阴云，唯一的目的就是吞噬我。"


translate chinese zhenya_route_d09f25e0:


    "我躲着他们，陷入进去，他们变成了我，我也变成了他们，最后已经不知道到底谁在镜子那边。"


translate chinese zhenya_route_ce617998:


    "..."


translate chinese zhenya_route_5685d9fa:


    "虽然在这种情况下这不只是一个失败者的陈旧格言。"


translate chinese zhenya_route_34acc943:


    "这是事实。"


translate chinese zhenya_route_162b28bf:


    "即使以前不是这样，即使世界之前有许多，我的生命也可以被称作生命。"


translate chinese zhenya_route_48f0c7b7:


    "因为他们都在一个瞬间结束了。"


translate chinese zhenya_route_99b70cac:


    "先是这个见鬼的公交车，然后是一群欢乐的少先队员走来走去，像我一样..."


translate chinese zhenya_route_5f95db12:


    "然后一遍一遍，像是各种面孔，情感杂糅的万花筒。"


translate chinese zhenya_route_2c11ec4c:


    "但是谁还在乎?!"


translate chinese zhenya_route_f4cffaa7:


    "如果一个人死了，他会因为没有时间做什么，还有没能做得了的事情后悔，而不是真正的生命结束的样子。"


translate chinese zhenya_route_1c0707ba:


    "但是我是在这些死后的灵魂待的地方吗...?"


translate chinese zhenya_route_7eb975bc:


    "而且所有的事情都有可能不一样!"


translate chinese zhenya_route_61b04b14:


    "结局都是一样的，虽然道路可能是不一样的，可能有自己的选择，这都没有关系。"


translate chinese zhenya_route_11e39e69:


    "曾经是这样的..."


translate chinese zhenya_route_5cfa9630:


    "所以这一切都是什么时候改变的呢?"


translate chinese zhenya_route_3136eb3d:


    "不同的事件，生命线，不同人的命运似乎缠结在一起，最后变成一条绳子让你自己去吊死!"


translate chinese zhenya_route_b9305951:


    "这一切都是什么时候改变的?"


translate chinese zhenya_route_bc26a936:


    "肯定不是一天，一小时，肯定是快到我看不出来。"


translate chinese zhenya_route_ce617998_1:


    "..."


translate chinese zhenya_route_37e5def2:


    "夏日的阳光照射着我的眼睛。"


translate chinese zhenya_route_f2bda4fc:


    "夏天——谁想得到呢，真是惊喜!"


translate chinese zhenya_route_7ebc85db:


    "但是昨天还是冬天...是的吧?"


translate chinese zhenya_route_7368e9d8:


    "我记得在司机的箱子里有香烟和火柴。"


translate chinese zhenya_route_a20a3b61:


    "深吸一口气让我感觉晕晕的，薄荷醇糟糕的气味让我大脑开始颤抖。"


translate chinese zhenya_route_4fc8a5c5:


    "\"Cosmos\"，出口。"


translate chinese zhenya_route_3468df1f:


    "他们当时做出口的东西做得很好来着，不像现在!"


translate chinese zhenya_route_006b2395:


    "核反应器，步枪，香烟，还有共产主义。"


translate chinese zhenya_route_f59ec965:


    "很多共产主义，在盒子里，箱子里，包装里，在集装货柜里，在飞机货仓里，在货运列车里，甚至在太空飞船里！"


translate chinese zhenya_route_389b751d:


    "还有更多的共产主义——向非洲的俾格米人，南美的印加人，新几内亚的巴布亚人..."


translate chinese zhenya_route_e9153e64:


    "我觉得我们也可以给南极的企鹅输送一些。"


translate chinese zhenya_route_00e16e71:


    "呃，我都说胡话了!"


translate chinese zhenya_route_8c8441f3:


    "抽完第三支烟，我盯着大门等待着。"


translate chinese zhenya_route_3b0dacde:


    "我都不需要表，我的大脑里有一台不会出错的钟。"


translate chinese zhenya_route_df514e2b:


    "现在，只要再等一会儿..."


translate chinese zhenya_route_5e5b6ce1:


    "其中一扇门吱吱的打开了，后面一个女孩儿探出身子，往周围看着，然后她注意到了我。"


translate chinese zhenya_route_b58b784f:


    "她一边往这边走一边使劲的吸气，这热空气显得有点难受，但是她很快笑着说："


translate chinese zhenya_route_7625c414:


    slp "你好，你一定是新的..."


translate chinese zhenya_route_8433d564:


    "我大喊着站起来走向营地，完全忽略掉她。"


translate chinese zhenya_route_6a3f1549:


    "这曾经很搞笑，然后很有意思，现在则是习惯了。"


translate chinese zhenya_route_fe493094:


    "愚蠢但是非做不可，像是往不同的口袋里装不同的东西。"


translate chinese zhenya_route_6761052d:


    "比如说我就不能在右边的口袋里装手机，因为..."


translate chinese zhenya_route_268e7fb4:


    "我怎么知道为什么!"


translate chinese zhenya_route_c21732cf:


    "这件事也一样：在早晨惹一个女孩儿就是一个很好的开始!"


translate chinese zhenya_route_68c3b30c:


    "我在路上遇到了另外一个少先队员的女孩儿。"


translate chinese zhenya_route_47974352:


    "准确的说她试图击打我的后背，但是我即使的跳开了，然后盯着她看，让这个可怜的女孩儿不得不去做其他的事情。"


translate chinese zhenya_route_4db95486:


    "辅导员的房间被紫丁香的瀑布包围着...{w}就不能淹死吗!"


translate chinese zhenya_route_8df4ea96:


    "巨大的摔门声影响到主人看书了。"


translate chinese zhenya_route_e969b169:


    "但是我从来都不在乎辅导员在看什么书。"


translate chinese zhenya_route_8aa38276:


    "我没有脱衣服，一屁股坐在床上，穿着靴子把脚搭在床板上。"


translate chinese zhenya_route_dc44d1f9:


    mtp "你以为你在干什么？！还有你是谁？！"


translate chinese zhenya_route_c982b4e5:


    "辅导员不爽的问着。"


translate chinese zhenya_route_2045575e:


    pi "你闭嘴，女人。"


translate chinese zhenya_route_bb30f1ca:


    "我懒洋洋的说着。"


translate chinese zhenya_route_13e9df52:


    "她惊呆了，说不出话来。"


translate chinese zhenya_route_fea18646:


    "我总是这样，每次都是这样。"


translate chinese zhenya_route_bed25db3:


    pi "说真的，告诉我，你怎么还没找到一个男人？你多大了？二十五？快三十了？你应该明白在这个年纪应该干什么！"


translate chinese zhenya_route_8920f5d5:


    "房间里一阵寂静。"


translate chinese zhenya_route_91056f46:


    pi "你不知道？那我告诉你—谁尼玛会想要你这样的人？！"


translate chinese zhenya_route_c092ab59:


    "辅导员的房间里回响着巨大的笑声，甚至像是马的嘶鸣，外面有熟悉的旋律。"


translate chinese zhenya_route_46e84c94:


    "我有好几次试图弄清楚晚餐的铃声是从什么地方播放的，所以这样扩音器里就能播放一些更欢快的曲子了。"


translate chinese zhenya_route_94ec9f26:


    "但是我没有成功，好像这天使般的声音不需要什么设备就能播放。"


translate chinese zhenya_route_66275b7d:


    "我去，不管了..."


translate chinese zhenya_route_432de457:


    "比如说，现在我就不在乎整个夏令营会不会都听到我的笑声...笑声..."


translate chinese zhenya_route_60662672:


    "但是对谁?"


translate chinese zhenya_route_31e1abdf:


    "绝对不是我自己!"


translate chinese zhenya_route_8b953e88:


    "我笑只是因为这是对付无聊的有效武器。"


translate chinese zhenya_route_18084d09:


    "笑一分钟可以让你多活五分钟来着?"


translate chinese zhenya_route_40c9f60e:


    "对我没什么用，我是事实永生的，只是需要法律证明一下。"


translate chinese zhenya_route_4517b112:


    mtp "你怎么能这么对长辈说话?!"


translate chinese zhenya_route_ffdabf91:


    pi "亲爱的，咱们能换一种说法吗？我已经听腻了，这样“你在做什么”好了，该你了!"


translate chinese zhenya_route_1489a42c:


    mtp "什么...?"


translate chinese zhenya_route_45586a8d:


    pi "不是什么，是你在做什么？"


translate chinese zhenya_route_fd5c0a1b:


    pi "我就会回答“谢谢你，和平常一样糟糕。”或者说“感谢你的关心。”"


translate chinese zhenya_route_09e93866:


    mtp "出去，我要叫警察了!"


translate chinese zhenya_route_06ad96de:


    pi "怎么叫？用信鸽？电话可不能用。"


translate chinese zhenya_route_d7289d90:


    mtp "对，但是...我..."


translate chinese zhenya_route_4c828f77:


    "辅导员颤抖起来。"


translate chinese zhenya_route_a996a0b3:


    pi "不过..."


translate chinese zhenya_route_89436b87:


    "我伸手掏着口袋，左边的，拿出手机甩给她。"


translate chinese zhenya_route_9ea2f617:


    pi "这个，试试。"


translate chinese zhenya_route_39fe9bde:


    "辅导员接住手机，还在手中跳来跳去差点掉在地上。"


translate chinese zhenya_route_d9306537:


    mtp "这...是什么?"


translate chinese zhenya_route_d5c62521:


    pi "电话。"


translate chinese zhenya_route_a411088b:


    "我简短的回答，然后闭上了眼睛。"


translate chinese zhenya_route_5072b3b6:


    pi "就是那个，“铃铃铃”响的。"


translate chinese zhenya_route_7af41437:


    mtp "你脑子有病吗?"


translate chinese zhenya_route_b216930d:


    "辅导员好像终于恢复了意识。"


translate chinese zhenya_route_1fdb7186:


    pi "你呢?"


translate chinese zhenya_route_2d95eff3:


    mtp "我不知道这里发生了什么..."


translate chinese zhenya_route_6015a2a3:


    pi "你看过土拔鼠之日吗？没有。总之，如果你的图书馆里有精神分析的书籍，我可能能对自己的精神状态进行详细的评估。所以我现在只能模糊的告诉你：对，我亲爱的，我有病！"


translate chinese zhenya_route_2c220ab9:


    "然后又是恐怖的魔鬼的笑声。"


translate chinese zhenya_route_ba3723c7:


    "辅导员昏了过去，手机从手中滑落，砸在了床边，滚了过来。"


translate chinese zhenya_route_3340d8cc:


    "我马上捡起来放回口袋里。"


translate chinese zhenya_route_d9c4ae96:


    pi "但是不是爱你。"


translate chinese zhenya_route_cf7d0875:


    "我睁开一只眼睛看着。"


translate chinese zhenya_route_44422a26:


    "有人害羞的敲着门，然后门打开了，有一个少先队员女孩儿站在门口..."


translate chinese zhenya_route_b5682e52:


    pi "见鬼!"


translate chinese zhenya_route_275798b7:


    "我神奇的从床上跳了下来。"


translate chinese zhenya_route_50556e32:


    pi "刀呢？落在家里了？"


translate chinese zhenya_route_efe5e7d5:


    "女孩儿困惑的看着辅导员。"


translate chinese zhenya_route_71e1b5fb:


    mtp "这是..."


translate chinese zhenya_route_b95f6ce4:


    "她叹叹气，然后倒在了床上。"


translate chinese zhenya_route_292e2dbc:


    "实际上我知道辅导员不害怕我，她的设定里没有害怕。"


translate chinese zhenya_route_90ba2fd2:


    "她可以变得很生气—对，很恐怖!"


translate chinese zhenya_route_a3ac6997:


    "但是过几分钟她就会停下来的。"


translate chinese zhenya_route_9318d2f9:


    "我也一样...如果我在她的位置...!"


translate chinese zhenya_route_38974101:


    unp "我一定来的不合适..."


translate chinese zhenya_route_0c34c18c:


    "女孩儿不好意思的说着，眼睛看着地下，准备离开。"


translate chinese zhenya_route_e83f62de:


    pi "别不好意思啊!"


translate chinese zhenya_route_23634e10:


    "我热情的喊着，招呼她过来。"


translate chinese zhenya_route_b1c5fa5a:


    pi "坐下来，就像在家里一样!"


translate chinese zhenya_route_613c7491:


    unp "不，我..."


translate chinese zhenya_route_aed8f0f1:


    pi "我说坐下!"


translate chinese zhenya_route_825bc5ee:


    "我咆哮道。"


translate chinese zhenya_route_bef00662:


    "现在这个女孩儿可以表现出恐怖，害怕，恐惧。"


translate chinese zhenya_route_bdc40866:


    "她慢慢的走过来，坐在床边。"


translate chinese zhenya_route_d117b542:


    pi "所以有什么好消息吗...?"


translate chinese zhenya_route_ce617998_2:


    "..."


translate chinese zhenya_route_59f848eb:


    "我跟别的孩子一样，从小父母就告诉我们不能给陌生人随便开门。"


translate chinese zhenya_route_6b1d45d4:


    "还有不知道什么人叫我的时候，不要跟着一起去不知道什么地方。"


translate chinese zhenya_route_c713fb4d:


    "但是那是童年了..."


translate chinese zhenya_route_d74f573a:


    "但是现在所有的事情都决定于我们是否相信陌生人!"


translate chinese zhenya_route_2385d8a9:


    "好像一辈子已经过去了，我有时间变老，如果不是身体上的就是精神上的。"


translate chinese zhenya_route_28349501:


    "我忘记了冬天，雪，冷，晚上的黑暗，还有泥泞的雪水。"


translate chinese zhenya_route_1e4f4c53:


    "城市里也一样，一刻也不停下来，总是跑来跑去熏着污浊的天空，有生锈的烟囱和汽车的人们精疲力竭，停留在遥远的过去，就像是老相册褪色的一页，被当作没用的东西扔进储藏室。"


translate chinese zhenya_route_fe1f361c:


    mz "嘿!"


translate chinese zhenya_route_b0b68304:


    "附近坐着的一个女孩儿让我分心了。"


translate chinese zhenya_route_5cbff5c8:


    mz "你又走神了？还在想那个吗？"


translate chinese zhenya_route_c233b6a2:


    me "什么?"


translate chinese zhenya_route_b00e703f:


    "她没有冒犯的意思，我也只是仪式性的问问，我已经知道答案了。"


translate chinese zhenya_route_a2aa1362:


    mz "你知道。"


translate chinese zhenya_route_1032aca8:


    "我们坐在图书馆的屋顶上，双腿在房檐边荡着。"


translate chinese zhenya_route_95ebb22e:


    "热妮娅向后靠了靠，她的头发在风中飘荡着。"


translate chinese zhenya_route_541d0415:


    "阳光照射着我们，让我们眯起眼睛，但没有那么眩目，更像是天鹅绒一般的温暖，柔和的阳光轻轻的包裹着你的身体。"


translate chinese zhenya_route_76bb3a52:


    me "不...{w}我是说我不知道，我想。{w}最近我经常说不清楚自己在想什么，我的大脑里都是一片模模糊糊的想法。"


translate chinese zhenya_route_d47cc9b7:


    "我沉默了一段时间，看着热妮娅。"


translate chinese zhenya_route_62828675:


    "她看起来不仅对我们的对话毫无兴趣，而且对周围的一切也都没什么感觉。"


translate chinese zhenya_route_75c0b8b5:


    "好像这个女孩儿对什么都无所谓似的，夏天，太阳，微风，青春..."


translate chinese zhenya_route_344c7b57:


    "好像这些对她就够了，其他的都是没用的琐事。"


translate chinese zhenya_route_b71363c7:


    me "总之，你知道我在说什么！你为什么还要问?"


translate chinese zhenya_route_7e36ced4:


    mz "因为人是社会生物，我们就应该这样交流。"


translate chinese zhenya_route_9551e44d:


    "她几乎让人看不出的微微笑了一下。"


translate chinese zhenya_route_c7fab65f:


    me "啊，对，看看谁在说话！等等，我要去给你颁奖“80年代最能交流少先队员奖”。"


translate chinese zhenya_route_0754d52f:


    mz "我呢？我和别人不一样，我在这个夏令营里有具体的义务，我是集体中备受尊敬的人，甚至是无可替代，和你正相反！"


translate chinese zhenya_route_440c2112:


    "热妮娅笑着嘲讽我，眼睛闪着光。"


translate chinese zhenya_route_1f5dde14:


    me "义务，对，啊..."


translate chinese zhenya_route_1dcd62cf:


    "我捂着脸靠在后面。"


translate chinese zhenya_route_f3cb88e5:


    me "说真的你能听见自己说话？义务？集体？"


translate chinese zhenya_route_fe1f361c_1:


    mz "嘿!"


translate chinese zhenya_route_8118435a:


    "热妮娅严肃的说着，然后砸了一下我的肚子。"


translate chinese zhenya_route_ea196633:


    me "噢呜。"


translate chinese zhenya_route_09cd5cef:


    "我故意呻吟着然后愤怒的看着热妮娅。"


translate chinese zhenya_route_f115c4df:


    me "我要是掉下去怎么办？摔伤了呢？啊？"


translate chinese zhenya_route_9da7dc7e:


    mz "那将是非常不幸的!"


translate chinese zhenya_route_1a6118f5:


    me "这里都没有地方去上保险!"


translate chinese zhenya_route_91ca94c2:


    mz "对，也没有人负责给你赔偿金。"


translate chinese zhenya_route_25d2cb1e:


    me "对，没人..."


translate chinese zhenya_route_d2c7a5fb:


    "我揉揉脑袋，呆呆的笑着说:"


translate chinese zhenya_route_1a377d44:


    me "你呢?"


translate chinese zhenya_route_cb595887:


    mz "我..."


translate chinese zhenya_route_4f246bc0:


    "热妮娅思考了一下。"


translate chinese zhenya_route_192326c0:


    mz "那个，可能，虽然我觉得食堂可能不接受美元、欧元、信用卡、或者起码是卢布，这里可能也没有货币兑换，即使有，谁知道回事什么比率，我可不想吃通货膨胀的亏！"


translate chinese zhenya_route_e6444b06:


    me "让他们给你黄金!"


translate chinese zhenya_route_c4101076:


    mz "行了吧你!"


translate chinese zhenya_route_852f8481:


    "她扑过来，试图胳肢我，拧我的肉，把我打倒，或者所有这些。"


translate chinese zhenya_route_ce617998_3:


    "..."


translate chinese zhenya_route_197ea717:


    "我咯咯笑着离开辅导员的房间。"


translate chinese zhenya_route_dcebf79c:


    "我可能也不应该从这种少先队员女孩儿这里期待什么。"


translate chinese zhenya_route_6077816f:


    "我也没有，有什么理由?"


translate chinese zhenya_route_92d37e44:


    "这已经都知道了，我扮演自己的角色，就像是独角戏。"


translate chinese zhenya_route_c1f15886:


    "真是讽刺，一个演员的喜剧，只有一个观众的剧院，只有一个人的城市，只有一个生物的宇宙?"


translate chinese zhenya_route_851ebb7b:


    "或者是加引号的，生物？或者是斜体字的？你想要哪种讣告？想要带花边的嘛？要大写的吗？要什么字体？要亮面纸还是磨砂纸？"


translate chinese zhenya_route_7e9e0418:


    "哈，又来了!"


translate chinese zhenya_route_f619af25:


    "路上有一个小女孩儿欢快的跳着。"


translate chinese zhenya_route_023d8bad:


    "也许这次我应该从她开始?"


translate chinese zhenya_route_77521125:


    "我记得食堂里有很多肉钩..."


translate chinese zhenya_route_c3e020a3:


    "或者我应该...?"


translate chinese zhenya_route_4e76737d:


    usp "你是谁?"


translate chinese zhenya_route_244be5e6:


    "她走过来脱口而出。"


translate chinese zhenya_route_90ff5d0b:


    pi "我是夜晚降临的恶鬼？呜啊！"


translate chinese zhenya_route_6f0e525a:


    "我生动的伸出胳膊表演着。"


translate chinese zhenya_route_6c1f8778:


    usp "这一点也不吓人。"


translate chinese zhenya_route_3d43d484:


    pi "不吓人？啊，只是暂时的..."


translate chinese zhenya_route_8d8338a5:


    "我咧嘴笑着，然后继续往前走。"


translate chinese zhenya_route_e10b6d74:


    usp "嘿!"


translate chinese zhenya_route_e8057229:


    "她跟着我喊着。"


translate chinese zhenya_route_ce6c5674:


    usp "等等。"


translate chinese zhenya_route_a3ba21f2:


    "女孩儿追上来拉住我的手。"


translate chinese zhenya_route_a593fff2:


    pi "别碰我!"


translate chinese zhenya_route_842697d6:


    "我恶心的喊着，然后猛地抽出手，结果这个小少先队员飞了起来，落在了灌木丛里。"


translate chinese zhenya_route_911de195:


    usp "哎呦，好疼!"


translate chinese zhenya_route_b7ea271a:


    "她哭了起来。"


translate chinese zhenya_route_59a139cf:


    "奇怪。现在这真的很奇怪了。"


translate chinese zhenya_route_8e30c7d5:


    "这个可从来没有发生过。"


translate chinese zhenya_route_d16c2c64:


    "当然这一天重复了那么多次，已经数不过来了..."


translate chinese zhenya_route_fe8f03bf:


    "重点不是女孩儿飞进了草丛里，而是不应该有新的东西出现!"


translate chinese zhenya_route_a884f083:


    "即使是这么不起眼的事情，有人可能这么觉得，这也可能比最一开始还要关键！"


translate chinese zhenya_route_0abd66bb:


    "我不知道为什么，但是真的已经好久没有新的事情出现了！"


translate chinese zhenya_route_57abc691:


    "当然这个世界不只是没有灵魂的机器，但是我应该熟悉所有的剧本的！"


translate chinese zhenya_route_d9f0d80c:


    "而且我说所有的时候，就是所有！！"


translate chinese zhenya_route_03df3bc1:


    "几分钟以前进入辅导员房间的本来可能不是那个害羞的女孩儿，而是我在大门那里遇到的，而且我可能准备好了。"


translate chinese zhenya_route_00375a72:


    "或者是那个在后背上用大拇指打招呼的傲慢的少先队员，我也准备好了。"


translate chinese zhenya_route_68cb4caf:


    "或者是这个爱哭的小孩儿，我也是..."


translate chinese zhenya_route_6c0f5a1e:


    "但是，就在这个时刻，现在发生的事情我没有准备好!"


translate chinese zhenya_route_7a13a065:


    "而且我没有准备好迎接这么大的变化!"


translate chinese zhenya_route_3d9d35bf:


    "女孩儿继续哭着，憎恨的看着我。"


translate chinese zhenya_route_7083b974:


    usp "你！都是你的错！"


translate chinese zhenya_route_faf5a9ef:


    pi "好了啦，你哭什么啊..."


translate chinese zhenya_route_47520c7a:


    "我犹豫着走向她然后伸出我的手。"


translate chinese zhenya_route_87036d00:


    "女孩儿一度露出疑惑的表情，不过还是抓住了我的手，然后瞬间站了起来。"


translate chinese zhenya_route_4106821b:


    usp "你应该感觉耻辱？哪有这样的—你不应该欺负小孩！"


translate chinese zhenya_route_cd7eeef3:


    pi "耻辱?"


translate chinese zhenya_route_14af5cda:


    "我重复了一遍，我完全没有想到。"


translate chinese zhenya_route_f699ca55:


    pi "为什么？因为你？为什么？"


translate chinese zhenya_route_fbb480b9:


    usp "你应该这样!"


translate chinese zhenya_route_07964120:


    "她生气的往辅导员房间那里跑去。"


translate chinese zhenya_route_6082bc94:


    "去抱怨吧，可能是，我不知道。"


translate chinese zhenya_route_312c4c2e:


    "我真的不知道！"


translate chinese zhenya_route_c0b16db9:


    "真的已经很久很久没有发生过这种事情了!"


translate chinese zhenya_route_a12e16eb:


    "几分钟以前我还能说得出来每个少先队员都在干什么，但是现在怎么办？"


translate chinese zhenya_route_c8849df7:


    "哈!"


translate chinese zhenya_route_ce617998_4:


    "..."


translate chinese zhenya_route_6e093196:


    "晚饭时辅导员走过来。"


translate chinese zhenya_route_8fd2dc1f:


    mtp "刚刚很尴尬..."


translate chinese zhenya_route_3dcba555:


    "她一边思考着一边说道。"


translate chinese zhenya_route_9b4a0f57:


    mtp "你的父母给我打电话，然后..."


translate chinese zhenya_route_56a89359:


    mtp "但是你本来可以做得更好的！可以好好的做自我介绍！"


translate chinese zhenya_route_8aa18321:


    "她说着表情变了。"


translate chinese zhenya_route_61a3b78e:


    mtp "但是没有关系，我们会让你成为一个模范的少先队员的。"


translate chinese zhenya_route_051e0836:


    "以前的话我会具体跟她说说少先队员还有模范是怎么一回事，但是现在我不太想说，不管是对她还是对别人。"


translate chinese zhenya_route_d6438854:


    mtp "那好吧，开始你应该先见见你的同事们。"


translate chinese zhenya_route_22da79ff:


    "辅导员挥挥手，邀请别人过来，有两个女孩儿走了过来。"


translate chinese zhenya_route_f05e1e1c:


    mtp "好了，享受你们的晚餐吧，我还有其他事情，先走了。"


translate chinese zhenya_route_7ca779be:


    "她有点不太肯定的说着，然后离开了。"


translate chinese zhenya_route_430dffff:


    mip "你好，你是新来的对吧？你喜欢音乐吗？加入我们的音乐社团吧！虽然现在只有我，但是如果你也加入的话就有两个人了，会很有趣的，不是吗？"


translate chinese zhenya_route_96dfe183:


    "我没有看她，继续认真的吃着。"


translate chinese zhenya_route_001db536:


    mzp "算了吧，没看出来他是个脑残吗。"


translate chinese zhenya_route_e662491f:


    "如果第一个女孩儿确实有点傻，那第二个都可以和网上的不爽猫相比了。"


translate chinese zhenya_route_e94109d2:


    "那个什么...?"


translate chinese zhenya_route_8fcc767a:


    "我以前从来没做过，只是现在突然想到了。"


translate chinese zhenya_route_33bd02a0:


    "嗯，既然我现在也不知道未来的事情，那为什么不试一下?"


translate chinese zhenya_route_e95a8512:


    "我悄悄地站起来，拿起水果汤的被子，倒在第二个少先队员的头上。"


translate chinese zhenya_route_264353ba:


    pi "祝你胃口大开。"


translate chinese zhenya_route_6e8444c6:


    "我冷漠的总结了一下自己的破坏行为然后往食堂门口走去。"


translate chinese zhenya_route_1741b8aa:


    "女孩儿哇哇的哭起来，喘息着，呻吟着，但是没有阻止我。"


translate chinese zhenya_route_58fe7b4e:


    "现在我最想在其他的地方，从外面观察整个世界，从我经验的高度看看其他人怎么受苦。"


translate chinese zhenya_route_d425dea5:


    "但是今天没有这样的地方。"


translate chinese zhenya_route_d58c2a5b:


    "我在门口遇到了大门那里的那个女孩儿，我试图绕过去，但是..."


translate chinese zhenya_route_16321643:


    "脸上被打了一巴掌。"


translate chinese zhenya_route_56937f22:


    "这个声音迅速传遍食堂，不停的回荡着。"


translate chinese zhenya_route_ccd4b759:


    "瞬间周围都沉默了。"


translate chinese zhenya_route_7d790b8b:


    "不到一秒钟的时间我的手攥成拳头，指甲按进了手掌里，手上的皮肤几乎要破裂..."


translate chinese zhenya_route_f644afb1:


    "我准备打死这个婊子..."


translate chinese zhenya_route_d8822251:


    "可能我因为一点事情迟疑了一下，那个被我扔进草丛里的小少先队员，突然不知道从哪里出现了，伸出胳膊挡在我们中间，用小小的身体保护着后面的女孩儿。"


translate chinese zhenya_route_3dead8bb:


    "结果就这样结束了..."


translate chinese zhenya_route_8ce3cecf:


    "我松开拳头，失去了力量，我低着头从她们身边走过，迅速离开了食堂。"


translate chinese zhenya_route_6d636a33:


    "她们真的比我更强吗?"


translate chinese zhenya_route_b0c195bf:


    "现在怎么会发生这种事情，以前的话，我会..."


translate chinese zhenya_route_125b75d4:


    "没有什么事情阻止过我。"


translate chinese zhenya_route_74647d36:


    "揍一个女孩儿也没什么的！我干过很多更恶劣的事情。"


translate chinese zhenya_route_e33546f5:


    "这个世界好像没通知我就发生改变了!"


translate chinese zhenya_route_ce617998_5:


    "..."


translate chinese zhenya_route_e85ecdd6:


    "烟圈慢慢飘向Genda。"


translate chinese zhenya_route_3865b2a1:


    "这一包已经快抽完了，这个地方也搞不到烟，我只能再等一周了..."


translate chinese zhenya_route_68877b02:


    "当然我可以去找自闭症兄弟，拿来一瓶伏特加然后喝醉，但是有什么区别呢?"


translate chinese zhenya_route_632060af:


    "现在决定起来好复杂，那时曾经多么简单..."


translate chinese zhenya_route_514f9a11:


    "哈，我难道已经像那个失败者一样思考了？！不行，不能这样！"


translate chinese zhenya_route_8236fc12:


    dvp "还有吗?"


translate chinese zhenya_route_96e9ab02:


    "又一个少先队员坐在我旁边。"


translate chinese zhenya_route_3b3e0f18:


    pi "什么?"


translate chinese zhenya_route_b7ca5301:


    "我不解的问道。"


translate chinese zhenya_route_eb197aa4:


    "她指指我的烟。"


translate chinese zhenya_route_b5c90a40:


    pi "吸烟有害健康!"


translate chinese zhenya_route_a96c9711:


    dvp "看看这是谁说的。"


translate chinese zhenya_route_473574eb:


    "她咧嘴笑着。"


translate chinese zhenya_route_b861b089:


    pi "我的是橡胶的，你想确认一下吗?"


translate chinese zhenya_route_3c4c2708:


    dvp "不用了，我相信。"


translate chinese zhenya_route_698cea6f:


    "我递给她一根烟还有火。"


translate chinese zhenya_route_b7315f93:


    dvp "你今天很酷...嗯..."


translate chinese zhenya_route_62f34328:


    "她咳嗽完说道。"


translate chinese zhenya_route_0adf4ebc:


    dvp "还有辅导员在房子里，我听到了，还有在晚饭的时候，至少现在我不是营地最大的耻辱了"


translate chinese zhenya_route_f427a0c4:


    "我小心的看着她。"


translate chinese zhenya_route_0cc3480e:


    "这个少先队员很平常的抽着烟，洋洋得意的样子，好像整个世界已经承认了她这样做的合法性。"


translate chinese zhenya_route_01e0ab71:


    pi "还有你，我看你是只想着自己。"


translate chinese zhenya_route_8e823649:


    dvp "什么？你说什么呢？我...没有..."


translate chinese zhenya_route_1f8ac785:


    "她的表情变了，她甚至有点紧张还呛到了自己。"


translate chinese zhenya_route_126ad096:


    pi "但是你说的对，你绝对不是这里最差劲的人，我很了解我自己的。"


translate chinese zhenya_route_32eb32a4:


    dvp "你说话太...好...酷了!"


translate chinese zhenya_route_df5a5407:


    "现在她这么表扬我，我可是很高兴啊。"


translate chinese zhenya_route_ed866e5b:


    "或者...?"


translate chinese zhenya_route_ecd96042:


    pi "听着..."


translate chinese zhenya_route_c1b91905:


    "我开始傻笑着。"


translate chinese zhenya_route_3ed5b05c:


    pi "你想一起去喝点什么吗?"


translate chinese zhenya_route_48a95115:


    dvp "你有吗?"


translate chinese zhenya_route_63d5e899:


    "少先队员的眼睛亮了。"


translate chinese zhenya_route_f90a9790:


    pi "我总可以给我们的好同志找到一些!"


translate chinese zhenya_route_ce617998_6:


    "..."


translate chinese zhenya_route_b4f43180:


    "虽然这开始像是脏兮兮的报纸上印着的糟糕的小说，但是结果却完全不一样。"


translate chinese zhenya_route_217e4924:


    "记忆开始缠在一起变得模模糊糊，像是一条黄色的砖路，通向前方的绿宝石之城。"


translate chinese zhenya_route_70fa3abc:


    "虽然桃乐茜长大去卖淫，因为她遇到了这片神奇的土地，也没有努力学习。"


translate chinese zhenya_route_e2cae742:


    "胆小的狮子，沉迷于冰毒，在警匪枪战中被射杀。"


translate chinese zhenya_route_94326c75:


    "现在铁皮人的生活很好，他成了一个杀手。"


translate chinese zhenya_route_1ffdfc71:


    "而稻草人一点都没变，他找到了一个稳定但是工资不高的小工作，每天从九点到六点，缺少更多的机会。"


translate chinese zhenya_route_8549b557:


    "可能前方根本就没有什么神奇的城市。"


translate chinese zhenya_route_cddcc89f:


    "一条没有尽头的道路，也没有终点和意义。"


translate chinese zhenya_route_c0fbc843:


    "你知道，那个时候他真的是在和我说这些，试图警告我，结果我没有听，结果成了唯一一个困在这魔比斯环的人。"


translate chinese zhenya_route_03818c7f:


    "她坐在旁边不爽的斜眼看着我。"


translate chinese zhenya_route_488f7b42:


    mz "嘿！我说！大地在召唤你!"


translate chinese zhenya_route_226ec868:


    me "好吧，怎么了？我就不能思考一下？"


translate chinese zhenya_route_509526cd:


    mz "思考应该会有结果，你有没有结果了?"


translate chinese zhenya_route_7062bae2:


    me "唔，我有什么?"


translate chinese zhenya_route_b1e74574:


    mz "无果!"


translate chinese zhenya_route_9524b395:


    me "无果也是一种结果嘛!"


translate chinese zhenya_route_83479432:


    "热妮娅使劲掐着我的肩膀，然后躲开了。"


translate chinese zhenya_route_ce617998_7:


    "..."


translate chinese zhenya_route_67b69ec7:


    "然后她出现了。"


translate chinese zhenya_route_274f7aad:


    "不，热妮娅...热妮娅们一直都在夏令营里，但是那个时候我第一次看到我的热妮娅。"


translate chinese zhenya_route_23e4b0e5:


    "可能我不记得这之前和之后都发生了什么，但是那几天的时光会永远留在我的记忆里！"


translate chinese zhenya_route_ce617998_8:


    "..."


translate chinese zhenya_route_4f00cb19:


    "我已经意识到这不是我的僵硬的世界，意识到我从一种永恒里掉入另一种永恒。"


translate chinese zhenya_route_8ac15cc8:


    "好像一个瞬间两个星球在我的面前交汇。"


translate chinese zhenya_route_27a7f3cc:


    "如果我晚一秒钟它们也会互相远离的，以惊人的速度相互离开。"


translate chinese zhenya_route_71441e64:


    "这个世界就跟之前的一样孤独。"


translate chinese zhenya_route_3c572706:


    "但是重要的是它这次不一样!"


translate chinese zhenya_route_16cc93e0:


    "这里有我不熟悉的事情。"


translate chinese zhenya_route_b518e850:


    "这个真的很重要，对我很重要!"


translate chinese zhenya_route_b1f78b5b:


    "比如说热妮娅不是坐在图书馆里而是食堂的门口。"


translate chinese zhenya_route_56aa7a6d:


    "发生了这么多事情以后我真的不能忍受讽刺。"


translate chinese zhenya_route_c8d5ca9e:


    pi "嗨。"


translate chinese zhenya_route_1364bea0:


    "我走近说，但是还是保持着安全距离。"


translate chinese zhenya_route_0df38c29:


    "热妮娅抬起头看着我，她的表情看上去好像没有期待在这里遇到谁。"


translate chinese zhenya_route_7c7aa84b:


    mzp "嗨..."


translate chinese zhenya_route_e68a1186:


    "她的声音也不像平常，不过我可不会把图书管理员的声音弄错！"


translate chinese zhenya_route_63994ee4:


    me "想要提前吃饭吗?"


translate chinese zhenya_route_065c7777:


    mzp "没有...我不知道...什么时候吃晚饭来着?"


translate chinese zhenya_route_37c6ffc1:


    "她嘟囔着。"


translate chinese zhenya_route_cfdaa1d8:


    me "什么叫什么时候？"


translate chinese zhenya_route_9e2a8fe8:


    "她的回答让我有点恍惚，我微笑着，身体里流过一丝寒气。"


translate chinese zhenya_route_b1f8319d:


    mzp "啊，抱歉，我当然知道晚饭是什么时候，但是..."


translate chinese zhenya_route_a334b9dd:


    "热妮娅完全糊涂了，然后盯着地面。"


translate chinese zhenya_route_4f43b195:


    "看起来她现在完全不想待在这里，但是似乎还没有决心离开。"


translate chinese zhenya_route_490c71fc:


    "这很奇怪，但是并没有吓到我，相反我很兴奋，因为终于发生了一些不太一样的事情！"


translate chinese zhenya_route_9933d8d3:


    pi "你还好吗?"


translate chinese zhenya_route_396cd3e5:


    "这种情况下这应该是最糟糕的问题了。"


translate chinese zhenya_route_abee5180:


    "我能期待什么样的回答呢?"


translate chinese zhenya_route_12f3639c:


    mz "嗯，谢谢。"


translate chinese zhenya_route_9508d41b:


    me "为什么?"


translate chinese zhenya_route_be9c5dd6:


    mz "我不知道，因为担心吧。"


translate chinese zhenya_route_5242e190:


    me "那个我只是..."


translate chinese zhenya_route_f50a4f7c:


    "她的脸上蒙上一层不满的表情，不过很快消失了，然后热妮娅慢慢的起来了。"


translate chinese zhenya_route_8a961b45:


    mz "我得走了。"


translate chinese zhenya_route_35daecd9:


    me "等等，你不是准备去食堂吗?"


translate chinese zhenya_route_6e2024d8:


    mz "我还有事，一会儿再来。"


translate chinese zhenya_route_505d91ae:


    me "好吧..."


translate chinese zhenya_route_2a4f9fde:


    "我没有阻止她，只是跟着她走。"


translate chinese zhenya_route_44b86c7f:


    "天知道上次发生这种事情是多久以前了!"


translate chinese zhenya_route_850d89b8:


    "可能只有最开始的几次..."


translate chinese zhenya_route_92e3d697:


    "热妮娅走到广场，然后迅速前往图书馆。"


translate chinese zhenya_route_f6928ff9:


    "她一直没有回头，所以我也不用躲开。"


translate chinese zhenya_route_38cda274:


    "当大门关上的时候我愣在了原地。"


translate chinese zhenya_route_7bb3941c:


    "接下来怎么办？我应该进去吗？但是进去了说什么好呢？"


translate chinese zhenya_route_ddf58ea4:


    "不用是个侦探也能看得出来我一直在跟踪她。"


translate chinese zhenya_route_f287617d:


    "电子小子不知道从哪里突然出现了，他打断了我的思考。"


translate chinese zhenya_route_3fa63434:


    elp "怎么，拿着清单来了吗?"


translate chinese zhenya_route_8e512950:


    pi "嗯？啊，对，决定来图书馆看看。"


translate chinese zhenya_route_d848bc68:


    elp "好了，我就不打扰你了。"


translate chinese zhenya_route_10ebed14:


    "他说完就准备离开了。"


translate chinese zhenya_route_b0b2d0e2:


    pi "等等。你不觉得最近热妮娅有点...呃...奇怪吗？"


translate chinese zhenya_route_257fa208:


    "电子小子仔细的打量着我。"


translate chinese zhenya_route_dcb8b7df:


    elp "发生什么事了?"


translate chinese zhenya_route_ac5548d9:


    me "没有，没什么..."


translate chinese zhenya_route_0ba7fa61:


    "我好像在编造借口，让我感觉很自责。我为什么要编造借口呢？"


translate chinese zhenya_route_1c8d2b4a:


    pi "我只是问问！怎么，我都不能问了？！"


translate chinese zhenya_route_e121c03e:


    elp "没有...当然可以.."


translate chinese zhenya_route_12d357d2:


    "他不好意思的说着。"


translate chinese zhenya_route_9c4cad90:


    elp "她可能确实有点奇怪。我觉得过去几天里我已经注意到了一些情况。"


translate chinese zhenya_route_d889c13c:


    pi "真的？这种奇怪的感觉有什么现象？"


translate chinese zhenya_route_40ac813e:


    elp "我说不太清，但是她平常要更有自信，那样的感觉，你觉得咱们是不是应该跟她谈谈？"


translate chinese zhenya_route_c090949c:


    "我本来想说“你可别去”，不过还是忍住了。"


translate chinese zhenya_route_49ac3d86:


    pi "算了算了，可能她只是那种心情。"


translate chinese zhenya_route_094c845e:


    elp "什么心情?"


translate chinese zhenya_route_d6ae043c:


    pi "没什么！那个—你动动脑子，你不是我们未来的科学家吗?"


translate chinese zhenya_route_fd51a2f5:


    "我突然间回答道。"


translate chinese zhenya_route_e1d27f94:


    "电子小子开始思考起来，好像在思考整个宇宙的命运一样，然后突然明白了。"


translate chinese zhenya_route_8734c583:


    elp "好吧。"


translate chinese zhenya_route_c2c38b1e:


    "我伸出胳膊，低着头向他示意自己已经没事了，他可以继续搞自己的机器人了，而且越快越好。"


translate chinese zhenya_route_809c2d88:


    "当然他反应有点慢，不过这次他马上就理解了。"


translate chinese zhenya_route_5c848e46:


    "电子小子离开后我坐了下来，后背靠着图书馆旁边的大树，在窗户里面看不到的角度坐好然后开始思考。"


translate chinese zhenya_route_71cfb74b:


    "准确的说应该是试图思考，因为根本没有足够的信息供我分析。"


translate chinese zhenya_route_1627ebc3:


    "你房间里的壁纸从白色变成黑色，你会感觉惊讶，恐惧，你会认为你不知道的时候被人重新涂了颜色，你会很生气..."


translate chinese zhenya_route_26e9bd8b:


    "但是那些都是感情。"


translate chinese zhenya_route_14f931ad:


    "那种情况的一个事实就是在桌子上放一张纸条写着“亲爱的，我觉得最好准备好迎接永恒所以我把内部换成了更合适的感觉。”"


translate chinese zhenya_route_90a606a4:


    "现在我手里有的东西：我的后面，无数次的循环，现在，图书管理员的奇怪举止，前方，还不确定。"


translate chinese zhenya_route_8b53b821:


    "没有任何理论的任何基础。"


translate chinese zhenya_route_bf4b9a89:


    "而且如果是的话我也是各种理论的大师，虽然那是过去的事情了，但是那个时候我开始不再关心自己在这个永久的夏令营里的未来和命运了。"


translate chinese zhenya_route_0bf3ea31:


    "哈，行了，又来了!"


translate chinese zhenya_route_f04073f4:


    "不管我怎么决定，这都不像是我!"


translate chinese zhenya_route_dc78bf83:


    "我不那样思考，可能也永远不会那样思考。"


translate chinese zhenya_route_21ad4eef:


    "就好像有人把外语词放进我的大脑里，我来到了外星世界就不是我自己了吗？"


translate chinese zhenya_route_d0ea6aef:


    "草丛在我的脚下涌动，暴露了我的行踪，我潜伏到图书馆外面试图看清里面发生了什么事。"


translate chinese zhenya_route_56b6bff2:


    "热妮娅坐在一张桌子后面，直直的盯着前面。"


translate chinese zhenya_route_2a36c9b6:


    "都没有拿着书——我的脑海中突然出现了一个想法。"


translate chinese zhenya_route_2e2c0707:


    "她看起来不知所措，好像被毁灭了一样，就好像有人知道自己只能活几个小时了一样。"


translate chinese zhenya_route_928e244d:


    "我真希望我能告诉她一切都会好起来的，只要再过五天，所有的事情都会开始重复..."


translate chinese zhenya_route_2ebd4d06:


    "但是这怎么能算是好起来？？"


translate chinese zhenya_route_8b397834:


    "就好像告诉一个人他再过五天就要死了，但是不用担心，因为之后再过七天他就可以再死一次了！"


translate chinese zhenya_route_443724fc:


    "过了将近十分钟了，但是热妮娅纹丝不动，有时候我都感觉她都没有在呼吸。"


translate chinese zhenya_route_416886f8:


    "现在我开始明白这不只是奇怪，这是“那个”，把我送到这个新世界的东西。"


translate chinese zhenya_route_48b5e529:


    "我只要想好自己应该说什么。"


translate chinese zhenya_route_f942d401:


    "我可以义无反顾的走上前去，给她讲讲我的故事..."


translate chinese zhenya_route_a563b120:


    mtp "啊，你在这里啊!"


translate chinese zhenya_route_d863b12a:


    "辅导员站在我身后。"


translate chinese zhenya_route_364c8304:


    "我突然转过身，烦躁的说："


translate chinese zhenya_route_ec8eb135:


    pi "你干嘛?"


translate chinese zhenya_route_e5143970:


    "就像我自己的一贯作风。"


translate chinese zhenya_route_6f62e389:


    mtp "你在这里犹豫什么啊？你忘了你也需要在图书馆获得签名吗？"


translate chinese zhenya_route_18359698:


    "辅导员好像一点也没有注意到我的语气。"


translate chinese zhenya_route_b9be1462:


    "跟平常不太一样，像是很久以前的一次。"


translate chinese zhenya_route_4959d934:


    "我的眼前出现了自己第一次来到这个夏令营时的景象，那些孩子，当时我还感觉他们都是真人，而不是摆设。"


translate chinese zhenya_route_780f294e:


    pi "那个，你看..."


translate chinese zhenya_route_fdd19e6d:


    mtp "我都不想听——晚饭以前必须拿到!"


translate chinese zhenya_route_6f46c062:


    pi "当然我正准备去见..."


translate chinese zhenya_route_5228d6ae:


    mtp "那就快去!"


translate chinese zhenya_route_5e43c0de:


    "奥尔加·德米特里耶夫娜目送我走到门口，我都不需要决定下一步做什么了，辅导员帮我决定了。"


translate chinese zhenya_route_0dda31b4:


    "图书馆闻起来就像是书里的尘土，老旧的苏联家具还有一点潮湿的空气。"


translate chinese zhenya_route_653614f9:


    "热妮娅坐在那里，完全没有注意到我，直到我关上门她才回过神来。"


translate chinese zhenya_route_c95e0c61:


    pi "我们又见面了。"


translate chinese zhenya_route_e5989bb1:


    "我迟疑的说着。"


translate chinese zhenya_route_91f4ad81:


    "不管怎么说我已经习惯了和这里的少先队员对话的方式了，想要改变不太容易。"


translate chinese zhenya_route_c9281090:


    "这个见鬼的辅导员又没有给我机会准备，我真想拿她的帽子勒死她！"


translate chinese zhenya_route_b83eb7ab:


    "热妮娅没有回答，继续小心的看着我。"


translate chinese zhenya_route_8c8749a0:


    pi "我那个什么来这里找你签字..."


translate chinese zhenya_route_195cbb9a:


    mzp "好吧，给我。"


translate chinese zhenya_route_addf2dc6:


    "她没有感情的说着。"


translate chinese zhenya_route_3abc9bf6:


    "我开始在口袋里掏来掏去然后意识到..."


translate chinese zhenya_route_d14407b5:


    "不对，我绝对出了什么问题！"


translate chinese zhenya_route_28462b26:


    "经历了这么多我应该已经熟悉了，但是我却跟一个一年生一样!{w}准确的说像是一个刚刚来到这个世界的新人！"


translate chinese zhenya_route_057d924f:


    pi "我好像弄丢了..."


translate chinese zhenya_route_86695cb0:


    "我沮丧的说着。"


translate chinese zhenya_route_a2dd85af:


    mzp "那可不好。"


translate chinese zhenya_route_acd3da05:


    "她还是用那种冷漠枯燥的声音回答着。"


translate chinese zhenya_route_369b2b04:


    "我在这种叠加态呆住了，如果我现在转身就走会怎么样？"


translate chinese zhenya_route_404fa019:


    "但是我又不知怎么有了决心。"


translate chinese zhenya_route_53cfe566:


    pi "我能问你个问题吗"


translate chinese zhenya_route_e9ebecff:


    mzp "什么问题?"


translate chinese zhenya_route_c0ce709a:


    "说真的，什么问题?"


translate chinese zhenya_route_7fcb23e3:


    "“你有没有觉得食堂最近的饭盐好像放少了？”“现在晚上很冷啊，是不是？”“对了，你知不知道我是一个未来的外星人？”..."


translate chinese zhenya_route_283f6bd3:


    pi "你还好吗？我感觉你最近心情不太好呢。"


translate chinese zhenya_route_632a1baf:


    "热妮娅看起来来了兴趣。"


translate chinese zhenya_route_1a27f095:


    mzp "但是咱们也不熟。"


translate chinese zhenya_route_eb7317c7:


    pi "嗯，是啊，只是...我昨天看到你然后觉得..."


translate chinese zhenya_route_d9cd8027:


    "我卷入了越来越多的谎话当中。"


translate chinese zhenya_route_e2013459:


    "一直不能讲实话让我越来越自闭，词汇量也开始降低。"


translate chinese zhenya_route_ccc2d340:


    mzp "我还看到你昨天在夏令营里跑来跑去，但是咱们还是不熟。"


translate chinese zhenya_route_65f82f5f:


    pi "没有，咱们是不熟，但是... 我听斯拉维娅说过你，你们是室友吧?"


translate chinese zhenya_route_46665132:


    mzp "她跟你说什么了?"


translate chinese zhenya_route_07d63e82:


    "热妮娅开始一点点的流露出内心的情感。"


translate chinese zhenya_route_f3d36d37:


    pi "没什么。"


translate chinese zhenya_route_b3d08fc6:


    "我无助的叹叹气，然后陷进了门口的椅子里。"


translate chinese zhenya_route_b26c8325:


    "窗外各种鸟儿唱着歌，蟋蟀不停的鸣叫，还有不知道什么地方的流水声，还有远处孩子们愉快的笑声。"


translate chinese zhenya_route_7deb2005:


    "但是那些都在那里，图书馆的窗外，房间里面只有我们两个，热妮娅和我，准确的说，是一个看起来像是热妮娅的女孩儿。"


translate chinese zhenya_route_0fb35f2f:


    "没有害怕的感觉，但是可以说是好奇。"


translate chinese zhenya_route_79a8f2f4:


    pi "咱们假设我不是自己。"


translate chinese zhenya_route_eae3d435:


    mzp "那你是谁?"


translate chinese zhenya_route_3797aeb4:


    "我都感觉有点生气她竟然不知道我到底是谁。"


translate chinese zhenya_route_7b7b5828:


    "为什么，我！哪个？那个？对，就是我嘛！"


translate chinese zhenya_route_6fa6e3c2:


    "很多个世界的少先队员都在流传着我的故事。"


translate chinese zhenya_route_33615421:


    "但是在这个世界的这个热妮娅好像一点也不在乎我是谁还有我要干什么，本来她已经开始感兴趣的对话又变的没有意义。"


translate chinese zhenya_route_49aeaf7e:


    pi "我看着像谁?"


translate chinese zhenya_route_bebec5bf:


    "热妮娅朝我扫了一眼。"


translate chinese zhenya_route_1704c62e:


    mzp "一个少先队员?"


translate chinese zhenya_route_d6914ece:


    pi "啊，对啊，一个少先队员，很有道理！!"


translate chinese zhenya_route_fea0eb84:


    "我苦笑着。"


translate chinese zhenya_route_7f7b1369:


    mzp "我没有开玩笑。"


translate chinese zhenya_route_37114254:


    pi "就是我看起来像自己，你不像。"


translate chinese zhenya_route_261dc859:


    mzp "我不像我自己?"


translate chinese zhenya_route_4a9dd7d0:


    pi "你不像你自己，天哪!"


translate chinese zhenya_route_9fc4efee:


    "最后的进攻开始了。"


translate chinese zhenya_route_9ca493ef:


    mzp "在什么意义下?"


translate chinese zhenya_route_62482817:


    "她感觉很惊讶。"


translate chinese zhenya_route_dd75e4f3:


    pi "在你不是热妮娅的意义下，我是说你不是这里的图书管理员！"


translate chinese zhenya_route_332c7b34:


    mzp "那我是谁？"


translate chinese zhenya_route_b749f9b4:


    "她看着旁边，好像在书架上找东西。"


translate chinese zhenya_route_6f01edd9:


    pi "我不知道，所以才问的。"


translate chinese zhenya_route_d9435aaa:

    mzp "如果这又是一个玩笑的话..."


translate chinese zhenya_route_916dd930:


    pi "玩笑?"


translate chinese zhenya_route_7dbdbf2d:


    mzp "你知道，大家都知道了..."


translate chinese zhenya_route_340ec68e:


    "她很扭捏的样子，脸有点泛红。"


translate chinese zhenya_route_27b31d2a:


    pi "我没有。"


translate chinese zhenya_route_d191e01e:


    mzp "我椅子上的图钉...我去找一本书的时候，阿丽夏..."


translate chinese zhenya_route_29245267:


    "热妮娅盯着地下，眼睛里充满了泪水。"


translate chinese zhenya_route_5dabba5b:


    pi "没有，我没做过那种事情。"


translate chinese zhenya_route_0eb20230:


    "不过如果想想的话，以前确实可能发生过，阿丽夏经常恶作剧。"


translate chinese zhenya_route_9375f9d2:


    "但是那个时候我肯定没有注意到。"


translate chinese zhenya_route_be40d38a:


    "热妮娅的反应肯定也不一样。"


translate chinese zhenya_route_945283c8:


    "阿丽夏..."


translate chinese zhenya_route_ce617998_9:


    "..."


translate chinese zhenya_route_3c8c8c92:


    dvp "躲开!"


translate chinese zhenya_route_e3f1127e:


    "这个少先队员女孩儿一边系着扣子，一边嘀咕着什么，嘴几乎没有动。"


translate chinese zhenya_route_b5f2f446:


    "机器人社团笼罩着月光，桌子上有一个伏特加的瓶子，还有洒出来的一滩痕迹。"


translate chinese zhenya_route_b53b097a:


    "我的脸颊很烫，大脑不停的转着，但是我都不能理解这是怎么回事，可能不仅仅是酒精的作用。"


translate chinese zhenya_route_81275182:


    dvp "你要是觉得我是那样的人，那你可以去死了!"


translate chinese zhenya_route_e04b78e8:


    "女孩儿的手指不听使唤，扣子不停的跳来跳去，但是就是无法扣到合适的位置去。"


translate chinese zhenya_route_965504ff:


    pi "对啊，啊?"


translate chinese zhenya_route_4dd24508:


    "我生气了，抓住她的手，把她按在桌子上，然后用牙咬开她的衬衫。"


translate chinese zhenya_route_ba65372b:


    pi "你想怎么样？像你这样？这不是反问，回答我！"


translate chinese zhenya_route_93a9420e:


    "我什么也不想改变，这样挺合适！"


translate chinese zhenya_route_f2b8909a:


    "我就在我自己习惯的世界过着自己习惯的生活，这样挺好！"


translate chinese zhenya_route_d1d1c36c:


    "我需要新鲜的事情做什么？！"


translate chinese zhenya_route_5db0b7d7:


    pi "你想要，我知道！跟一个陌生人认识了五分钟就狂灌伏特加，这不是你吗？去你的吧，该怎么就怎么样！"


translate chinese zhenya_route_b1c84289:


    "我要失去控制了，我自己要失控了，这个世界要失控了..."


translate chinese zhenya_route_b99db903:


    dvp "放手，很疼!"


translate chinese zhenya_route_28eb9141:


    "女孩儿哭了起来。"


translate chinese zhenya_route_0da4bcdf:


    "但是我还是紧紧地攥着她，过了一会儿她就没了力气，闭上了眼睛，只是不时的流出滚烫的眼泪。"


translate chinese zhenya_route_78f90845:


    pi "看，现在好多了吧。"


translate chinese zhenya_route_8572d521:


    "我粗暴的抓着她，大喘着粗气。"


translate chinese zhenya_route_6ca28273:


    pi "因为你是我的！你们都是我的！整个世界都是我的！"


translate chinese zhenya_route_74cdf889:


    "我突然失去了兴趣，甚至有点恶心。"


translate chinese zhenya_route_c3d0fb09:


    pi "我不需要你—这里还有更好的！"


translate chinese zhenya_route_a834cad6:


    "我放开她，然后疲倦的滑倒在地上。"


translate chinese zhenya_route_7a01683b:


    "这个女孩儿没有动，只是继续抽泣着。"


translate chinese zhenya_route_1887db4f:


    pi "我想要哪个女孩儿都可以！你听见了吧！然后我就要拿剁肉刀，然后...你以为以前没发生过吗？"


translate chinese zhenya_route_9bd59498:


    "我疯狂的喊起来。"


translate chinese zhenya_route_35794ca1:


    pi "你觉得你能让我做什么？这是我的世界，你听见了？我的！你们都不存在，我在和自己说话！"


translate chinese zhenya_route_c9bc6d68:


    pi "别哭了！"


translate chinese zhenya_route_6b0ebc18:


    "我失去了最后一点理智，跳起来把全身的重量压在少先队员身上。"


translate chinese zhenya_route_0c326074:


    "她好像已经快要晕过去了。"


translate chinese zhenya_route_d1490e2d:


    "然后我弯下腰对她悄悄说着："


translate chinese zhenya_route_db4eee40:


    pi "你知道我和你做过什么吗？那么多次了...想听听吗？现在就给我听着，我要说了！"


translate chinese zhenya_route_d47d3ccb:


    "我突然慌了。"


translate chinese zhenya_route_748351d4:


    "自从最一开始已经过了很久了，但是其中的每一秒钟我都“活着”，我能控制自己，控制这个世界，还有之前的很多个世界。"


translate chinese zhenya_route_ba6bab34:


    "我这时第一次感觉自己要消失了，自己要离开了！"


translate chinese zhenya_route_2f2f0661:


    "怎么可能，是我，你不懂？你不记得了？"


translate chinese zhenya_route_1eb562e0:


    dvp "你是窝囊废..."


translate chinese zhenya_route_c7cfef75:


    "女孩儿对我悄悄说着，好像来自另外一个世界一样。"


translate chinese zhenya_route_1ac9eb61:


    dvp "一个可悲的废物，什么也不是，我想要可怜你，但是对着空虚的躯壳也没什么能可怜的。"


translate chinese zhenya_route_fd0f9179:


    "我恐惧的跳了起来。"


translate chinese zhenya_route_7f9a4127:


    "少先队员的脸上充满扭曲的憎恶的表情，眼泪已经消失了，就好像从来没有过一样。"


translate chinese zhenya_route_8eccaba8:


    "她站起来往我这边走了几步，然后停下。 "


translate chinese zhenya_route_ce617998_10:


    "..."


translate chinese zhenya_route_01793405:


    mz "你为什么跟我说这个?"


translate chinese zhenya_route_455e39f2:


    "热妮娅懒洋洋的翻过一页书。"


translate chinese zhenya_route_c9efc482:


    "水面浮动着涟漪，我们头顶的桦树叶不停的摇动，正午的阳光照耀着我们。"


translate chinese zhenya_route_3dd25e16:


    "空地里的鸟儿欢快的歌唱着，草丛里的小虫子争先恐后的往我的腿上爬。"


translate chinese zhenya_route_b92f4659:


    pi "你为什么在读这本书？别告诉我这是第一次!"


translate chinese zhenya_route_0584c94e:


    mz "我喜欢，就像是你喜欢叙述，我喜欢读书。"


translate chinese zhenya_route_b801ccf7:


    "在另一边河岸的沙滩上，少先队员们愉快的嬉戏着，奥尔加·德米特里耶夫娜在河水中追着乌里扬卡，我还看到了斯拉维娅金色的头发。{w}她是在这里游泳吗？"


translate chinese zhenya_route_bf61ccfa:


    mz "那怎么了?"


translate chinese zhenya_route_1d91a5d4:


    pi "什么?"


translate chinese zhenya_route_9efbba46:


    mz "然后发生了什么?"


translate chinese zhenya_route_1ecf5995:


    pi "我以为你不感兴趣呢。"


translate chinese zhenya_route_17ce821c:


    mz "但是你很有兴趣。"


translate chinese zhenya_route_beecbb90:


    "热妮娅笑了。"


translate chinese zhenya_route_ce617998_11:


    "..."


translate chinese zhenya_route_e2ddd404:


    "她朝我吐了口吐沫走了出去，甩上了大门。"


translate chinese zhenya_route_ec4fada4:


    "剩下我自己站在那里。"


translate chinese zhenya_route_4d9c3879:


    "什么，这就完了?"


translate chinese zhenya_route_d02cc081:


    "我一度等待着，甚至很有耐心的等待着。"


translate chinese zhenya_route_b963c577:


    "但是时间太久了，我都不太记得了..."


translate chinese zhenya_route_aa69bc1b:


    "那现在呢?"


translate chinese zhenya_route_fb8066ab:


    "那些娃娃，那些没有灵魂的娃娃破坏了我自己的人生...?{w}就那样，一天之内就...?"


translate chinese zhenya_route_6072604a:


    "我抚摸着自己的脸颊，摸到了湿湿的东西。"


translate chinese zhenya_route_ce617998_12:


    "..."


translate chinese zhenya_route_3690f9af:


    "夜幕降临整个营地，遮挡着这里的少先队员，遮挡住他们的诡计和憎恨。"


translate chinese zhenya_route_a97066dd:


    "我就坐在机器人社团里，抱膝摇来摇去。"


translate chinese zhenya_route_78432774:


    "可能那个少先队员女孩儿要回来了，反正她还可以更进一步的羞辱我。{w}或者是更糟糕的事情..."


translate chinese zhenya_route_b724315a:


    "但是随着时间的流逝只有虫鸟的鸣叫声慢慢褪去。"


translate chinese zhenya_route_5dafa38c:


    "很快就要到黎明了，整个营地都会醒来，然后突然开始这让我很不熟悉的生活。"


translate chinese zhenya_route_9b97de77:


    "算了，管他呢..."


translate chinese zhenya_route_f388c609:


    "我有点费力的站起来，看着桌子上的那瓶伏特加。"


translate chinese zhenya_route_f56a8f65:


    "最好就现在就昏过去然后明天跟什么事没有一样醒过来，把今天的事情都忘了，回到自己熟悉的能预测所有人行动的世界去。"


translate chinese zhenya_route_7f6ffa46:


    "这都是从那个小女孩儿开始的..."


translate chinese zhenya_route_48e2be22:


    "她以前从来没哭过。"


translate chinese zhenya_route_930e94bf:


    "也许是她哭过但是我没有注意到，那说明什么又?"


translate chinese zhenya_route_69a2c6d6:


    "什么？难道要同情那个小屁孩，变得跟她们一样？"


translate chinese zhenya_route_039e2cb1:


    "不行我要振作起来!"


translate chinese zhenya_route_d565336d:


    "我离开了机器人社团，使劲关上门，然后呼吸着夜晚清新的空气。"


translate chinese zhenya_route_18c48e62:


    "奇怪，不像平常那样没有生气。"


translate chinese zhenya_route_786313e6:


    "那我应该去哪儿?"


translate chinese zhenya_route_c86ccfda:


    "我看追上那个女孩儿然后报复一下吐我一脸这件事很好!"


translate chinese zhenya_route_2d3dd531:


    "我绝不饶恕这种行为！"


translate chinese zhenya_route_cac4acc5:


    "广场上只有一个少先队员坐在长椅上。"


translate chinese zhenya_route_1eaf6c8a:


    "我刚开始没有在意，谁知道这里会有多少弱智？不过他朝我喊着："


translate chinese zhenya_route_b5f924fc:


    pi2 "嘿，谢苗!"


translate chinese zhenya_route_a0e4c394:


    "你他妈叫谁谢苗？我暗自骂道。"


translate chinese zhenya_route_e16dba53:


    "但是这个少先队员一点也没有冷静下来:"


translate chinese zhenya_route_4a9d1da0:


    pi2 "你在找谁吧？但是她没往那边走。"


translate chinese zhenya_route_020a3c0b:


    "我僵在了原地，但是马上转过身朝他走去。"


translate chinese zhenya_route_b26f9b1a:


    pi "你看见她了?"


translate chinese zhenya_route_faffbf87:


    pi2 "那当然!"


translate chinese zhenya_route_e9213e07:


    "他脸上弥漫着令人作呕的笑容，他呲牙咧嘴，野蛮凶残，显示出强者的优越感。"


translate chinese zhenya_route_dd90be7e:


    pi "那就感觉说，我可没时间玩游戏!"


translate chinese zhenya_route_0035a9a8:


    pi2 "等等，你这么着急干什么，咱们坐下来谈一谈。"


translate chinese zhenya_route_1d25a44c:


    pi "我跟你没什么好谈的。"


translate chinese zhenya_route_d656bbab:



    "少先队员思考了一会儿，然后摸摸口袋，拿出来一包“宇宙”牌香烟递给我。"


translate chinese zhenya_route_bfadd262:



    pi2 "要不要?"


translate chinese zhenya_route_990a64cb:


    pi "这...你从哪儿搞到的?"


translate chinese zhenya_route_da5e25e1:


    "我可不记得普通的少先队员还有烟!"


translate chinese zhenya_route_35ea6809:


    "虽然说过去的这24个小时里发生的事情实在太多..."


translate chinese zhenya_route_49df45e9:


    "我慢慢抽出一根，然后把整包递回去。"


translate chinese zhenya_route_9075cb57:



    "他拿起来在手里转了几圈然后又递给了我。"


translate chinese zhenya_route_ef0baff0:


    pi2 "拿着吧，我不抽了。"


translate chinese zhenya_route_7302d938:


    "又是那个肮脏的笑容!"


translate chinese zhenya_route_b9b224aa:


    "不过算了，我得去找到那个无礼的贱货! "


translate chinese zhenya_route_e5239908:


    pi "所以呢？告诉我她去哪儿了!"


translate chinese zhenya_route_b0bacf80:


    pi2 "你要那个可怜的女孩儿干什么？在宇宙的尺度上她是那么渺小，可能根本就不存在。"


translate chinese zhenya_route_6e286439:


    "他思考了一会儿然后继续说。"


translate chinese zhenya_route_9a27b7d6:


    pi2 "对你来说也一样，你确定自己真的存在吗？即使存在，你确定自己的存在是真实的吗？也许你从前真的存在过但是那样的日子已经一去不复返了呢？"


translate chinese zhenya_route_0c2d21db:


    pi2 "或者说你会出现在未来，现在我面前的只是一个娃娃?"


translate chinese zhenya_route_7b1709f6:


    "“娃娃”这个词在我的大脑中爆炸了，好像用.45口径的柯尔特打碎我的脑壳，溅出一地的脑浆。"


translate chinese zhenya_route_3851cbd0:


    "对，明天该斯拉娃打扫...我内心里不由自主的傻笑着。"


translate chinese zhenya_route_aef5fe7f:


    pi "你是谁?"


translate chinese zhenya_route_797cca45:


    "那个傲慢的往我脸上吐吐沫的女孩儿就和我的脑浆一起飞走了。"


translate chinese zhenya_route_b49e136e:


    pi "你是谁，我问你呢!"


translate chinese zhenya_route_3286fb9d:


    "我拽住他的红领巾，用力太大，发出了撕裂的声音。"


translate chinese zhenya_route_d4433ae7:


    "但是少先队员只是一直笑着。"


translate chinese zhenya_route_522e4cdd:


    pi2 "冷静，别那么担心，在这里不可能会死掉的，你知道的，你已经试过很多次了。"


translate chinese zhenya_route_393c873a:


    "我恐惧的远离他旁边。"


translate chinese zhenya_route_d92d4988:


    "这句话...我已经听过很多次了！"


translate chinese zhenya_route_de5bcfba:


    "不只是这样——我一直这样对自己说！"


translate chinese zhenya_route_aff47745:


    "在很久以前...我的某一次曾经的人生。"


translate chinese zhenya_route_c313ce95:


    pi2 "因为你就是谢苗。我也是谢苗。你就是我。"


translate chinese zhenya_route_530a8a5f:


    "长椅和那个少先队员突然间远离了我，好像我从过山车上掉下去似的，但是那个恶毒的笑容还留在我的眼前。"


translate chinese zhenya_route_6ecb5b42:


    "周围的世界被裹在黑暗之中..."


translate chinese zhenya_route_ce617998_13:


    "..."


translate chinese zhenya_route_b052fa58:


    me "好吧，然后我消失了。"


translate chinese zhenya_route_9b867c85:


    mz "就那样?"


translate chinese zhenya_route_0482fa2e:


    "热妮娅一直盯着书，但是这个故事好像也引起了她的兴趣。"


translate chinese zhenya_route_8b0f35ac:


    me "怎么？及时退场是一项重要的技能，你要知道，说起来我可是这项技术的大师啊！"


translate chinese zhenya_route_eab0814c:


    mz "对，你什么事都是大师..."


translate chinese zhenya_route_57d8f4cb:


    "她放下书，拥抱着我。"


translate chinese zhenya_route_257596c3:


    "斯拉娃显然在我们的前方游泳，她看到了我们，然后转过身往岸边游去。"


translate chinese zhenya_route_6258eed6:


    me "等等，我都没到关键部分呢!"


translate chinese zhenya_route_a2112eb3:


    me "反正不在这里..."


translate chinese zhenya_route_3b51a4ca:


    "我不好意思的离开她。"


translate chinese zhenya_route_ce05b572:


    "热妮娅显然不喜欢这样，往后退了一下。"


translate chinese zhenya_route_02d00f0e:


    mz "我已经听腻了你的故事了，每次都是一样的，没完没了！这次换一下吧，你听听我的故事。"


translate chinese zhenya_route_b401bc94:


    me "我很好奇是什么故事?"


translate chinese zhenya_route_43f0b201:


    mz "比如说我怎么来到这里的!"


translate chinese zhenya_route_6a25d8e4:


    me "我都听了一百遍了。"


translate chinese zhenya_route_46e49702:


    mz "我听你的都听了两百遍了!"


translate chinese zhenya_route_a20cefa7:


    "..."


translate chinese zhenya_route_7372fd7b:


    "对热妮娅来说小猫头鹰的生活不是从我们的见面还有那张清单开始的。"


translate chinese zhenya_route_776cce52:


    "因为我开始自己在这里的生活的时间永远比她晚一个星期。"


translate chinese zhenya_route_6b6478fd:


    mz "你知道时间停止的感觉吗？你想死却死不掉的感觉？"


translate chinese zhenya_route_edad5831:


    me "啊，对，像是在这里，在这个夏令营?"


translate chinese zhenya_route_0766c187:


    mz "不，在这里还要简单，人们更诚实，更善良，没有真正的恶人，没有过“那时”的憎恨，你不记得了吗？"


translate chinese zhenya_route_cbc4bb84:


    "我没有回答。"


translate chinese zhenya_route_0b199ede:


    mz "我可以读书，和人们聊天，有人需要我，有人把我当作朋友，我有自己的职责..."


translate chinese zhenya_route_72a4670a:


    "热妮娅好像已经要哭出来了。"


translate chinese zhenya_route_360485e5:


    mz "好了，我们这么... 不管怎么说，这次因为410公交车的震动把我吵醒的，你坐过那辆车吗？"


translate chinese zhenya_route_b91f0b42:


    "我点点头。"


translate chinese zhenya_route_ed2494b4:


    mz "我没坐过，我们的城市里甚至都没有三位数的公交车。"


translate chinese zhenya_route_13c9611e:


    mz "我不明白发生了什么，然后身边坐着的女孩儿突然对我说：“你好，我是斯拉娃。”"


translate chinese zhenya_route_73f9c19e:


    mz "斯拉娃？什么斯拉娃？哪个斯拉娃？"


translate chinese zhenya_route_3d09c388:


    mz "我当然很糊涂了，但是没有表现得那么明显，然后我们来到了夏令营，然后事情就越来越乱..."


translate chinese zhenya_route_385c7f99:


    me "然后这些就又一次发生了..."


translate chinese zhenya_route_9d45a0d2:


    mz "然后又一次..."


translate chinese zhenya_route_be100ad4:


    "热妮娅叹叹气。"


translate chinese zhenya_route_8110044b:


    mz "而且我已经见到你很多次了，你和斯拉娃，和阿丽夏，和列娜..."


translate chinese zhenya_route_a150cf10:


    "她咬咬牙。"


translate chinese zhenya_route_66a94d6b:


    me "但是那我，已经一百次了，然后你又开始新的!"


translate chinese zhenya_route_09769e7d:


    mz "你，不是你... 你们都半斤八两！“我”也在那里是不是，但是那可不是真正的我！"


translate chinese zhenya_route_cf5fe3d9:


    me "就是这样！要分辨出真货也没有那么难!"


translate chinese zhenya_route_2a487144:


    mz "对，因为假的每次都换成新的，真的没有换。"


translate chinese zhenya_route_931c85d9:


    me "完全不是那样的吧..."


translate chinese zhenya_route_90aa467c:


    mz "好像你什么都知道似的!"


translate chinese zhenya_route_a6c61dc1:


    me "这有什么关系?!"


translate chinese zhenya_route_b9e0fc87:


    "我又仅仅的抱住热妮娅，然后闭上眼睛。"


translate chinese zhenya_route_a20cefa7_1:


    "..."


translate chinese zhenya_route_f38c94d0:


    "我醒了过来，浑身是汗，心脏狂跳不止。"


translate chinese zhenya_route_95485d35:


    "正在升起的太阳热得让人受不了，虽然看样子才六七点而已。"


translate chinese zhenya_route_7c0f70f5:


    "到底怎么了？那个邪恶的少先队员，怎么回事？"


translate chinese zhenya_route_b8fea11b:


    "可能只是梦？只是幻觉？"


translate chinese zhenya_route_6d64ec4f:


    "我现在要相信了，我什么都要相信了!"


translate chinese zhenya_route_b1ec62eb:


    "有人慢慢的穿过广场向我走来。"


translate chinese zhenya_route_54e2fa21:


    "我往那边看着，发现是辅导员。"


translate chinese zhenya_route_f2c53477:


    mtp "所以说你又来了?"


translate chinese zhenya_route_a339aad1:


    "她靠过来，威胁的说道。"


translate chinese zhenya_route_d619304f:


    "这让我站起来往后退了一步。"


translate chinese zhenya_route_00676949:


    mtp "我还觉得昨天是误解...我是无语了，那样对待一个女孩子！"


translate chinese zhenya_route_7fbc5e40:


    "她在说什么?"


translate chinese zhenya_route_a6084c5b:


    "什么女孩儿，她到底要我干什么？"


translate chinese zhenya_route_0790d314:


    "至少我们的辅导员同志看起来和平常没有什么区别，这还让我冷静了一些。"


translate chinese zhenya_route_fe2f533c:


    mtp "所以呢，回答我！你以为你这次逃得了吗？我要叫警察，我要上报全联盟共青团中央总书记!"


translate chinese zhenya_route_e0de5854:


    "她太卖力气，都开始喘了起来。"


translate chinese zhenya_route_7ca78ad4:


    mtp "在我的夏令营里，我要，我要，你...!"


translate chinese zhenya_route_b745da71:


    "反正辅导员这种举止倒是吓不倒我。"


translate chinese zhenya_route_71972c56:


    "但是我感觉不自在，我还在回忆昨天晚上的事情，在喝醉的时候做了一些不能饶恕的很不道德的事情。"


translate chinese zhenya_route_563df633:


    "奇怪的是我根本不在乎面前狂喊的辅导员。"


translate chinese zhenya_route_17ef50dc:


    pi "闭嘴行不行！好像着火了一样！你懂不懂什么是问题？一个少先队员差点被强暴？你脑子是不是有问题？"


translate chinese zhenya_route_e625c257:


    "我咬咬牙，威胁的看了辅导员一眼，她顿时闪过一丝恐惧。"


translate chinese zhenya_route_88d9cfee:


    pi "我跟你讲讲真正的问题怎么样？让你不再沉浸在自己想象的小世界里？！"


translate chinese zhenya_route_7eb5bc4b:


    pi "算了！滚吧，你就是在浪费我的时间！"


translate chinese zhenya_route_156bb973:


    "不过话说回来，我从来都不缺少这种时间..."


translate chinese zhenya_route_cf548d39:


    "我的大脑中又闪过一个可怕的想法：万一所有事情都像现在一样怎么办...?"


translate chinese zhenya_route_af5d6b63:


    "万一我在这个世界永远的失控怎么办？"


translate chinese zhenya_route_1073c912:


    "所有的女孩儿都会开始往我脸上吐吐沫然后辅导员又要像教训电子小子一样教训我...?"


translate chinese zhenya_route_6efa0286:


    "那可是比死掉还糟糕的!"


translate chinese zhenya_route_4764ccbb:


    pi "反正我要走了..."


translate chinese zhenya_route_2b1ad4f4:


    "辅导员还沉浸在震惊之中，我朝她冲过去，任凭两腿把我带向不知道的地方。"


translate chinese zhenya_route_96ba2c48:


    "但是说真的，死亡对于我自己来说是什么？"


translate chinese zhenya_route_717b489b:


    "我好像已经很久没有理解这个概念了。"


translate chinese zhenya_route_d4c4a4be:


    "如果我一直无法消失，一直在这个循环中重复的话，死亡已经抛弃了我吗？"


translate chinese zhenya_route_8a3a5440:


    "没有了力量，前方只有不确定？"


translate chinese zhenya_route_7e0405f4:


    "但是那比任何“普通”的人的命运还要糟糕！"


translate chinese zhenya_route_735eed4b:


    "他们的生活有不好的结局，你也不能够存档。"


translate chinese zhenya_route_1ea7a991:


    "我的生活没有结局，而且可以存档..."


translate chinese zhenya_route_07e40943:


    "他们会管这种生活叫什么？"


translate chinese zhenya_route_02cd0d67:


    "如果我还记得一个普通人的生活..."


translate chinese zhenya_route_d9801473:


    "他们可能管这个叫地狱！"


translate chinese zhenya_route_3da4e67a:


    "然后这一次循环，连没有灵魂的娃娃都可以羞辱我，这次应该是最后一次了。"


translate chinese zhenya_route_fc47ce7d:


    "如果可能的话，我已经准备好发疯了。"


translate chinese zhenya_route_4db8f15d:


    "所有曾经消失的想法突然间都回来了，凝聚成一个原始的情感。"


translate chinese zhenya_route_f7601ba0:


    "最后谁是对的?"


translate chinese zhenya_route_14e500e2:


    "我一直劝自己说自己是对的，他们是错的，因为我活着，他们在追逐空虚的幻象的时候已经消失了。"


translate chinese zhenya_route_aa9cc9b6:


    "他们消失了，我留下了。"


translate chinese zhenya_route_6aec9b7e:


    "最后谁得到了好的一面...?"


translate chinese zhenya_route_a0734101:


    pi2 "哈哈。"


translate chinese zhenya_route_18a14147:


    "附近响起熟悉的窃笑声。"


translate chinese zhenya_route_ca7a3d8c:


    pi2 "你今天好像明白了很多事情啊！"


translate chinese zhenya_route_5a75dcad:


    pi "我知道你是谁。"


translate chinese zhenya_route_ad7879c5:


    "我的声音很冷静，但是心脏以惊人的速度狂跳不止。"


translate chinese zhenya_route_aa40ee3d:


    pi2 "哦，是吗，我倒是很想听听。"


translate chinese zhenya_route_7c80e143:


    pi "你和我一样，一个少先队员，但是是来自另一次循环的！"


translate chinese zhenya_route_dc8a84af:


    pi2 "但是你不是觉得在谢苗坐车离开以后这里就没有其他人了吗？你觉得这无穷的夏令营里只有你自己！"


translate chinese zhenya_route_8249e6b5:


    pi "我有可能犯错..."


translate chinese zhenya_route_900da79c:


    "虽然很不想承认，但是这是有可能的。"


translate chinese zhenya_route_3cf57bf2:


    pi2 "你？犯错？为什么？那不可能啊！"


translate chinese zhenya_route_2138916b:


    pi "随便你，你来做什么？只是来嘲笑我以前犯过的错误吗？"


translate chinese zhenya_route_28216e14:


    pi2 "你说得好像...自己很愧疚似的。"


translate chinese zhenya_route_c7603b8c:


    pi "我没感觉愧疚，而且这也跟你没关系，你就回答我的问题!"


translate chinese zhenya_route_69b5c4c3:


    pi2 "啊，可能你说得对..."


translate chinese zhenya_route_aa53adf9:


    "少先队员思考了一会儿。"


translate chinese zhenya_route_8c7036b9:


    pi2 "虽然说嘲笑痛苦的人是不道德的!"


translate chinese zhenya_route_6d7221cb:


    "我突然间很想一拳头打在这家伙脸上！"


translate chinese zhenya_route_c1143dcc:


    "总之，我是一个颤抖的生物，还是有这个权利?!"


translate chinese zhenya_route_21d61111:


    "谁允许他在我的世界里搞他的那一套的?!"


translate chinese zhenya_route_a902c6d7:


    pi "滚开!"


translate chinese zhenya_route_2323c424:


    "我喊出来，用上全身的力气朝他脸上一击..."


translate chinese zhenya_route_26c40b41:


    "但是我的手穿过一层雾，什么也没有打到，反倒是我自己失去了平衡，重重的摔倒在地上。"


translate chinese zhenya_route_ce617998_14:


    "..."


translate chinese zhenya_route_392741f8:


    "疼痛减轻一些后，我发现有人站在附近。"


translate chinese zhenya_route_eac3bbc2:


    "果然，草丛里有一个讨厌的小女孩儿看着我。"


translate chinese zhenya_route_15b8af29:


    usp "你这是自作自受！"


translate chinese zhenya_route_f02f9218:


    "她跟我吐舌头。"


translate chinese zhenya_route_319a4b06:


    usp "面对自己的同龄人的时候就没有那么神气了吧!"


translate chinese zhenya_route_8a70a572:


    pi "啊你..."


translate chinese zhenya_route_4cc464f6:


    "这真是没有意料到，我都不知道说什么好了。"


translate chinese zhenya_route_3bfb5da7:


    pi "你面前一个活人突然消失你就不觉得很奇怪吗?"


translate chinese zhenya_route_754e98b4:


    usp "嗯...世界上总会有一些奇怪的事情，可能是魔术吧，无所谓了，总之你是自作自受！"


translate chinese zhenya_route_2a81e5c3:


    "我受不了这些侮辱，跳起来冲向这个女孩儿。"


translate chinese zhenya_route_4794ec9c:


    "女孩儿巧妙的逃跑了，我却在树林里浪费着时间不停的躲闪着划在脸上的树枝，还有坑坑洼洼的地面。"


translate chinese zhenya_route_f8154d9c:


    "磕了浑身的包还把手划得都是血，我最后终于在广场追上了她。"


translate chinese zhenya_route_1435d693:


    pi "怎么样，你也没那么快啊是不是，这可跟从什么笨蛋手中逃跑不一样吧!"


translate chinese zhenya_route_7a05ead8:


    usp "放开我！"


translate chinese zhenya_route_3f110a9b:


    "女孩儿哭了起来。"


translate chinese zhenya_route_c8a389f1:


    "突然我感觉背后有一个力量推了我一下，我眼前的一切都飘动起来，然后飞速的摔到了地上。"


translate chinese zhenya_route_7cb62aa8:


    dvp "所以你连孩子都不放过?"


translate chinese zhenya_route_fc1209ed:


    slp "就是他，抓住他!"


translate chinese zhenya_route_ecefc9dc:


    "肚子上又是一拳..."


translate chinese zhenya_route_3161f0e8:


    "我本能的用胳膊抱着头。"


translate chinese zhenya_route_ad729ea8:


    mip "这下神气不起来了吧?"


translate chinese zhenya_route_a66487fa:


    "又是一拳..."


translate chinese zhenya_route_c834ca35:


    "我已经快要失去意识了，我艰难的支撑着自己滚到了一边去。"


translate chinese zhenya_route_edbb6f89:


    "血液流到我的眼睛里，打我的少先队员都变成了红色。"


translate chinese zhenya_route_3b6c1c0e:


    mtp "这样教育你可能不是最好的方法，但是肯定是最有效的。"


translate chinese zhenya_route_00b08032:


    "天知道辅导员从哪里又出现了。"


translate chinese zhenya_route_a6717d6b:


    pi "你干什么？！够了..."


translate chinese zhenya_route_a20cefa7_2:


    "..."


translate chinese zhenya_route_e42400d8:


    mz "你读过堂吉诃德吗?"


translate chinese zhenya_route_e52631bf:


    "热妮娅坐在我旁边，悠闲地晃着腿。"


translate chinese zhenya_route_1b25a65d:


    me "嗯，但是已经不太记得讲的什么了。"


translate chinese zhenya_route_0a5efd62:


    mz "风磨！你的故事都针对风磨。（？？）"


translate chinese zhenya_route_955295f8:


    me "求求你，什么磨..."


translate chinese zhenya_route_9af55b9b:


    mz "即使没有磨，你也要弄出一个来，接着续。"


translate chinese zhenya_route_17eb90aa:


    me "你这说的我好像受虐狂似的!"


translate chinese zhenya_route_3805b28b:


    mz "你不是吗?"


translate chinese zhenya_route_b47b6b13:


    "她狡猾的笑着。"


translate chinese zhenya_route_f1b872bc:


    "热妮娅...没了她我怎么办？"


translate chinese zhenya_route_520a3a56:


    "有时候我觉得如果最后我能够获得奖励的话，其实在这个夏令营里受的苦也不算什么。"


translate chinese zhenya_route_fc9436ca:


    "我把她拉过来亲吻着她。"


translate chinese zhenya_route_d79bf9f9:


    mz "等等，你干什么...要是有人看见怎么办？"


translate chinese zhenya_route_a064965c:


    "热妮娅红着脸反抗着。"


translate chinese zhenya_route_6d038d67:


    me "你平常不这么在乎的啊。"


translate chinese zhenya_route_c19c6b50:


    mz "没有，但是...好吧，我刚才说到哪儿了？"


translate chinese zhenya_route_d663f3b1:


    me "风磨?"


translate chinese zhenya_route_86c65e85:


    mz "对...我是说不对！你还不能安定下来，就跟那些普通人一样坐着公交车走..."


translate chinese zhenya_route_4933f750:


    me "也就见不到你了。"


translate chinese zhenya_route_f8936c3d:


    mz "好像你以前见过我似的，总是斯拉娃，列娜，阿丽夏..."


translate chinese zhenya_route_b7edee7e:


    "她有点生气。"


translate chinese zhenya_route_05457ff9:


    me "看，这不就是说我都做对了吗!"


translate chinese zhenya_route_5034d5ab:


    mz "去你的吧！大男子主义还有后宫！"


translate chinese zhenya_route_c4265473:


    me "咱们别说过去了，面前还有整个世界呢!"


translate chinese zhenya_route_a20cefa7_3:


    "..."


translate chinese zhenya_route_b56763f0:


    pi3 "嘿！你还活着吗?"


translate chinese zhenya_route_2847cd45:


    "我脸上被倒上了冷水，我费劲的睁开眼睛。"


translate chinese zhenya_route_35df17a9:


    "有一个少先队员靠在我旁边。"


translate chinese zhenya_route_a02937cb:


    pi "怎么又是你...她们没把我弄死，你还决定帮助我，你来吧，我不怕，来吧！"


translate chinese zhenya_route_7b0d9a0b:


    "我的内心还感觉有点高兴，这个罪恶的循环终于要结束了。"


translate chinese zhenya_route_cac3b2d5:


    pi3 "不对，你不明白！我...我不是他！"


translate chinese zhenya_route_c5c56f07:


    pi "那你是谁?"


translate chinese zhenya_route_1804c309:


    "我已经不在乎了，现在死了最好，结束这一次循环，开始下一次！"


translate chinese zhenya_route_83dddfb4:


    "我从来没有这么期待过自己循环的人生。"


translate chinese zhenya_route_dffa8938:


    "对他们来说最重要的是遵守长久以来制定好的规则。"


translate chinese zhenya_route_0022762c:


    pi3 "你不明白吗...?"


translate chinese zhenya_route_f7f100c4:


    "少先队员痛苦的说着。"


translate chinese zhenya_route_277c650c:


    pi "不，我不再玩这些游戏了!"


translate chinese zhenya_route_43104af6:


    "真的，他们没有决定把我放在那个蠢货的位置上吗?"


translate chinese zhenya_route_d65516a5:


    "在这个游戏里我还是什么新手？在我的服务器里可不行！"


translate chinese zhenya_route_595caacc:


    pi "而且不管怎么说，我都不在乎!"


translate chinese zhenya_route_6304a54d:


    "我试图站起来，但是疼痛让我呻吟着被迫靠在树桩上。"


translate chinese zhenya_route_ef375c44:


    "虽然说如果让我在这该死的循环里再待上五天半我可能还有妥协一下。"


translate chinese zhenya_route_1b5fc009:


    pi "好吧，我不管你是谁，你为什么在这里，但是如果你想要帮我的话我也不会拒绝。"


translate chinese zhenya_route_a0e4cf55:


    "如果他表里如一的话那我还是可以相信他一下的。"


translate chinese zhenya_route_b82cc3b2:


    pi3 "当然..."


translate chinese zhenya_route_b3db7e6e:


    pi "那就给我弄点喝的东西来。"


translate chinese zhenya_route_87e7c94d:


    "少先队员马上跳起来然后消失在了树林里。"


translate chinese zhenya_route_d7251a80:


    pi "他是往食堂跑去了吗？真是个傻子。"


translate chinese zhenya_route_ce617998_15:


    "..."


translate chinese zhenya_route_4526b54e:


    "时间过得很慢。"


translate chinese zhenya_route_eb8d24cc:


    "我把手机弄丢了，可能是挨打的时候掉的，也可能之前就已经丢了，反正我无法判断那个少先队员离开了多长时间了。"


translate chinese zhenya_route_93a3cf35:


    "但是不管怎么说他也应该回来了。"


translate chinese zhenya_route_0748b652:


    "我的伤口疼的难以忍受，我好像开始发烧了。{w}会不会是伤口感染了？"


translate chinese zhenya_route_5eb365a4:


    "这可是会出人命的！"


translate chinese zhenya_route_8d439469:


    "至少她们可以打死我。"


translate chinese zhenya_route_caf72da0:


    "但是她们没有打死我，我只能在这里一个人痛苦的死去。"


translate chinese zhenya_route_6c406b5e:


    "等等！"


translate chinese zhenya_route_2f974ad0:


    "下次我都告诉你！"


translate chinese zhenya_route_ee043568:


    "对死亡的恐惧不存在因为一个人只能被可怕的东西吓到。"


translate chinese zhenya_route_3f52e9b2:


    "对我来说死亡反倒是一种解脱。"


translate chinese zhenya_route_ec79852e:


    "虽然说我也不太相信，下一次这一切可能都会重复，这意味着我要自己奋斗！"


translate chinese zhenya_route_6b91e2c7:


    "就跟最开始的几次一样。"


translate chinese zhenya_route_1103dcdf:


    "做什么都行，别让我跟那个人似的哭个不停。"


translate chinese zhenya_route_3b8c1c5e:


    "我勉强的站起来一瘸一拐的往营地走去。"


translate chinese zhenya_route_dad64e6b:


    "天色越来越暗了。"


translate chinese zhenya_route_d6a7f5b9:


    "我应该问问那个少先队员在我被打昏过去以后到底发生了什么。"


translate chinese zhenya_route_331a0e55:


    "可能他们在找我?"


translate chinese zhenya_route_73be3703:


    "或者她们觉得我已经死了?"


translate chinese zhenya_route_e64dfe95:


    "不管是哪种情况我都要小心。"


translate chinese zhenya_route_a32be48c:


    "现在我在永无止境的循环里学会的各种技巧就显得十分重要了。"


translate chinese zhenya_route_fbda2b81:


    "但是只有一条胳膊一条腿完整的残废估计也没法打架..."


translate chinese zhenya_route_a20cefa7_4:


    "..."


translate chinese zhenya_route_df258bf7:


    "第一眼看上去广场空空的，没有少先队员，没有鸟，没有附近森林里居住的松鼠。"


translate chinese zhenya_route_4ceb9268:


    "空气中弥漫着恐怖的寂静，就像是战斗开始之前的间歇，或者是人类和动物都避而远之的雷区。"


translate chinese zhenya_route_db1b0a8d:


    "一步走错就会粉身碎骨。"


translate chinese zhenya_route_1625539c:


    "要是这么简单就好了..."


translate chinese zhenya_route_e4410ee1:


    "但是我不能这样等着，我得去医务室清理伤口，用一些止痛药还有抗生素。"


translate chinese zhenya_route_131ab1ed:


    "最初的几步很艰难，倒不只是因为打斗时受的伤。"


translate chinese zhenya_route_51b712f7:


    "我终于走到了尽头，走过窄窄的小径，来到医务室门前，我已经开始折腾门锁了，突然又听到草丛里簌簌的声音。"


translate chinese zhenya_route_fcb1e9ab:


    "绝对不是动物或者鸟，那是人类!"


translate chinese zhenya_route_5b291f33:


    "虽然我知道那些疯狂的少先队员不会躲起来，她们很疯狂。"


translate chinese zhenya_route_d979f39e:


    pi "我不知道谁躲在哪里，但是你最好赶紧出来，不然..."


translate chinese zhenya_route_58a3975f:


    "不然什么?"


translate chinese zhenya_route_7a18852d:


    "在这种情况下我的威胁可能连那个傲慢的小姑娘都不如!"


translate chinese zhenya_route_d8883300:


    unp "是我..."


translate chinese zhenya_route_7e66674c:


    "害羞的少先队员走了出来。"


translate chinese zhenya_route_caa65ec6:


    pi "啊，是你?"


translate chinese zhenya_route_621736c0:


    "我讽刺的问。"


translate chinese zhenya_route_d3c58e8d:


    pi "你自己？其他人呢？虽然说你自己的话就好办了..."


translate chinese zhenya_route_f45fe05b:


    unp "没有没有...如果我打扰到了你的话我可以离开。"


translate chinese zhenya_route_c572cc9a:


    pi "算了吧，反正事情也不会再坏到哪儿去。"


translate chinese zhenya_route_81356c94:


    "我还在徒劳的折腾着门锁。"


translate chinese zhenya_route_85f66ba6:


    "当然要是有个牙签什么的我撬锁无压力，但是我得有正常的手指头，但是我现在血肉模糊，关节错位..."


translate chinese zhenya_route_ff0038ce:


    "我现在顶多能拿着队旗挥舞一下。"


translate chinese zhenya_route_9cacc36a:


    "本身就是愚蠢的教训，这里的少先队员又可以强迫我做。（？？）"


translate chinese zhenya_route_fb78244d:


    unp "也许我可以帮帮忙?"


translate chinese zhenya_route_8f5ff1f3:


    pi "怎么帮？精神上的鼓励？"


translate chinese zhenya_route_5ac801c4:


    unp "我有那个..."


translate chinese zhenya_route_c68a86cc:


    "她话没说完，然后递给我一把钥匙。"


translate chinese zhenya_route_1537f606:


    pi "哪儿来的?"


translate chinese zhenya_route_92afb189:


    "对，我没有意料到，但是也没有什么好吃惊的，我还想着别的事儿呢。"


translate chinese zhenya_route_5e6a0cab:


    unp "我从斯拉娃那里拿来的。"


translate chinese zhenya_route_09d5b397:


    pi "就这样？你是想说你没告诉她要做什么用，跟她要，结果就给你了？"


translate chinese zhenya_route_28020918:


    unp "那个，我说我身体不舒服，但是又不想麻烦她，就借一下钥匙。"


translate chinese zhenya_route_a224ce8d:


    pi "对啊，好借口。阴险，奸诈，狡猾，他们说每个厉害的男人背后都有一个厉害的女人。"


translate chinese zhenya_route_9a2c43a2:


    "我想起那个少先队员给我的第二包“宇宙”牌香烟。"


translate chinese zhenya_route_bb832dfd:


    "那里面的烟结果都烂了，但是我还是成功的找到了几根完好的。"


translate chinese zhenya_route_c56adbe2:


    pi "好吧，很有用，你很聪明。"


translate chinese zhenya_route_7f5ee04f:


    unp "谢谢。"


translate chinese zhenya_route_7346cea3:


    "女孩儿几乎让人察觉不到地笑了一下。"


translate chinese zhenya_route_3bfa5470:


    "如果她真的没有在背后藏着一把刀呢...?{w}说真的这些想法都是哪儿来的?!"


translate chinese zhenya_route_4a5fdb28:


    "在这个世界里假设谁无辜从来都不能成功，尤其是现在认为所有人都先验的是坏人更好一些。"


translate chinese zhenya_route_bfa3d62d:


    "我谨慎的打量着这个女孩儿。"


translate chinese zhenya_route_e2bce231:


    unp "什么...？有什么问题吗？"


translate chinese zhenya_route_f40a37e9:


    pi "我就是不明白，你最好告诉我——你还正常吗？"


translate chinese zhenya_route_f6774088:


    unp "我不明白你什么意思..."


translate chinese zhenya_route_6bda23f8:


    "女孩儿显然有点失望。"


translate chinese zhenya_route_80fdc055:


    "好吧，至少她还假装好人，我应该利用这一点，要让我自己包扎所有的伤口不太容易。"


translate chinese zhenya_route_4280f591:


    pi "别在意，如果你真的想要帮忙的话..."


translate chinese zhenya_route_ce617998_16:


    "..."


translate chinese zhenya_route_74cd6038:


    "医务室被房顶的荧光灯照的很明亮。"


translate chinese zhenya_route_ecfceff8:


    "我勉强的爬上沙发，试图脱掉衬衣。"


translate chinese zhenya_route_7aaf2429:


    unp "我帮你吧。"


translate chinese zhenya_route_d7c5f11f:


    pi "好吧，帮帮我。"


translate chinese zhenya_route_3df4d52c:


    "我肚子两边都是惨不忍睹的伤口还有淤青的肿块。"


translate chinese zhenya_route_bbafbb40:


    "我肯定折了几根肋骨，或者还有几根没折。"


translate chinese zhenya_route_6dc32eb8:


    "每一点小小的动作都伴随着疼痛的响应。"


translate chinese zhenya_route_3f894e04:


    pi "啊啊啊！你就不能再小心一点吗?"


translate chinese zhenya_route_d9ee0e49:


    unp "抱歉。"


translate chinese zhenya_route_f99441c3:


    "女孩儿开始帮我脱衣服，甚至有点热心。"


translate chinese zhenya_route_47c8c54f:


    "最后我只剩一条短裤。"


translate chinese zhenya_route_b9d2f671:


    unp "等等，得用碘消消毒！"


translate chinese zhenya_route_80692109:


    pi "好吧，然后我看着跟一头棕熊似的？算了吧，还是用过氧化氢吧，在抽屉里。"


translate chinese zhenya_route_259d7d24:


    "女孩儿没有争辩。"


translate chinese zhenya_route_edaff1f2:


    "但是不管是双氧水还是碘液——都疼的要命啊！！"


translate chinese zhenya_route_208736c6:


    "我必须做出非人的努力才能不叫出来。"


translate chinese zhenya_route_e6e5397a:


    unp "疼吗?"


translate chinese zhenya_route_62a68b8d:


    pi "你觉得呢?!"


translate chinese zhenya_route_b462f311:


    "我暗自咬着牙。"


translate chinese zhenya_route_5d12af1f:


    "然后女孩儿开始帮我缠绷带。"


translate chinese zhenya_route_c4e8acee:


    "每次要缠绕身体的时候，她都几乎趴在我身上。"


translate chinese zhenya_route_a847a4e2:


    unp "好了!"


translate chinese zhenya_route_c8bb1a19:


    "列娜好像终于完成了任务一样说着。"


translate chinese zhenya_route_64313f9e:


    "她做的有点过了，我这么觉得。"


translate chinese zhenya_route_909f6f6a:


    pi "嗯，好多了。"


translate chinese zhenya_route_6cdbc3d5:


    "倒不是我真的感觉舒服了，但是我相信自己很快就能好起来，到不了明天。"


translate chinese zhenya_route_98db5fc7:


    "但是那要到明天。"


translate chinese zhenya_route_ed2b786e:


    "我试图站起来往药房走，但是我突然感觉头晕，结果又坐在了沙发上。"


translate chinese zhenya_route_4d858608:


    pi "给我安乃近还有...还有...那个..."


translate chinese zhenya_route_521a590f:


    "我真的忘了医药箱的库存了吗?"


translate chinese zhenya_route_ed40cc8c:


    "我伸手摸摸额头，很热。"


translate chinese zhenya_route_cc8f0f79:


    pi "抗破伤风的，忘了叫什么了，在说明里有。"


translate chinese zhenya_route_c61d4c44:


    "女孩儿匆匆开始寻找，然后抱过来一堆五颜六色的药丸。"


translate chinese zhenya_route_38a2fca3:


    pi "这都是什么?"


translate chinese zhenya_route_cb469a4d:


    unp "你说的嘛..."


translate chinese zhenya_route_27722bff:


    "这个当然有风险，但是也不会糟糕到哪儿去了。"


translate chinese zhenya_route_f93b1c3f:


    pi "然后用水冲。"


translate chinese zhenya_route_1cbefc66:


    "我吞掉药丸，然后摊在沙发上。"


translate chinese zhenya_route_7e492c7f:


    "女孩儿坐在旁边耐心的看着我。"


translate chinese zhenya_route_45d6e425:


    "她到底想要什么?"


translate chinese zhenya_route_c822b5dc:


    "这种悬念让我很累。"


translate chinese zhenya_route_434288d8:


    "我只想睡觉，可以暂时忘掉这个疯狂的世界。{w}再好一点，在另外一个世界醒过来！"


translate chinese zhenya_route_58911e68:


    "让一切结束吧，让他们结束我吧!"


translate chinese zhenya_route_67aba564:


    "或者是这个女孩儿..."


translate chinese zhenya_route_0f6e53e3:


    pi "你打算在这里坐多长时间?"


translate chinese zhenya_route_1691f095:


    unp "如果我打扰到了你的话我可以离开。"


translate chinese zhenya_route_6e477f3d:


    pi "没有，没有。"


translate chinese zhenya_route_57115d23:


    "突然我想到了一个疯狂但是也不是毫无意义的想法！"


translate chinese zhenya_route_5857b475:


    pi "你甚至可以帮我..."


translate chinese zhenya_route_2cf6cc0c:


    "女孩儿好奇的看着我。"


translate chinese zhenya_route_f7b1fdd6:


    pi "看到那个罐子了吗?"


translate chinese zhenya_route_e2b2bbba:


    "我挣扎着指向那个装满药品的罐子。"


translate chinese zhenya_route_b0d079f8:


    pi "现在从里面抽出一整个针管的药。"


translate chinese zhenya_route_a20cefa7_5:


    "..."


translate chinese zhenya_route_30fbce41:


    mz "你是认真的吗?"


translate chinese zhenya_route_46df7f73:


    "热妮娅的脸变得很烫，她的声音听起来不仅是震惊，而且害怕！"


translate chinese zhenya_route_763ea9a0:


    me "安静!"


translate chinese zhenya_route_f35360cc:


    mz "不行，你肯定是疯了!"


translate chinese zhenya_route_e4748fd7:


    me "怎么了？最次来讲新的轮回也要开始了，没有人会记得的。"


translate chinese zhenya_route_bb4cc545:


    mz "我会记得——这就够了！"


translate chinese zhenya_route_39419e54:


    me "所以你的答案就是不行?"


translate chinese zhenya_route_c6cacbe6:


    mz "我的答案是别欺负我了!"


translate chinese zhenya_route_af512106:


    "她试图站起来，但是我仅仅的抓住了她的手。"


translate chinese zhenya_route_fe1f361c_2:


    mz "嘿!"


translate chinese zhenya_route_3d06e455:


    "热妮娅没有挣脱，只是笑着扑到了我身上。"


translate chinese zhenya_route_96a92f4a:


    mz "你真的没开玩笑吗？万一咱们真的离开这里了呢？"


translate chinese zhenya_route_3b02a616:


    me "你很久没有说过那个问题了。"


translate chinese zhenya_route_7761d947:


    mz "我没说过，但是我现在说了，如果在现实世界中你想要忘记，假装自己什么都没说，那..."


translate chinese zhenya_route_d2551cd3:


    me "但是不管是在这里还是现实世界我的话都是真诚的!"


translate chinese zhenya_route_5f3b8e22:


    mz "即使是这样也不太容易相信啊，这么突然..."


translate chinese zhenya_route_0af1865c:


    me "这有什么突然的？咱们已经互相认识多久了？我已经不记得是哪次循环了。"


translate chinese zhenya_route_618d05b1:


    mz "我还在数着..."


translate chinese zhenya_route_3129f2d7:


    "热妮娅悄悄地说着，然后紧紧的抱着我。"


translate chinese zhenya_route_ce617998_17:


    "..."


translate chinese zhenya_route_c4a91b1d:


    "女孩儿顺从的把针管灌满无色的液体递给我。"


translate chinese zhenya_route_0fda54f1:


    pi "不对，你什么时候见过病人自己给自己打针的?!"


translate chinese zhenya_route_27de15a6:


    unp "但是我不知道怎么..."


translate chinese zhenya_route_954d7da0:


    pi "这没什么难的。"


translate chinese zhenya_route_23affce2:


    "只是对我来说这个世界只有一个禁忌——自杀。"


translate chinese zhenya_route_0cffd3d6:


    "我已经准备好被少先队员们打死或者是被注射过大剂量的药品，但是自己杀死自己..."


translate chinese zhenya_route_cee6910e:


    "不知道为什么我就是觉得不能这样，虽然一直也说不清楚到底是怎么回事。"


translate chinese zhenya_route_7742b1d1:


    "也许是过去的二十四个小时里对未知的恐惧，或者只是自我保护的本能。"


translate chinese zhenya_route_d3f91d78:


    "反正如果我被谁杀掉的话马上就会在新的一次轮回中醒过来，但是现在都准备好了!"


translate chinese zhenya_route_be57257f:


    "现在唯一的问题是这个女孩儿不愿意帮助我结束生命。"


translate chinese zhenya_route_75512fb4:


    unp "不行，我...万一弄错了怎么办？最好还是叫护士来！我马上回来！"


translate chinese zhenya_route_f085d7c6:


    "我现在最不需要的就是护士了!"


translate chinese zhenya_route_12645575:


    pi "等等!"


translate chinese zhenya_route_58ec471e:


    "我撑住自己，抓住她的手。"


translate chinese zhenya_route_9f0b1268:


    pi "别担心，我已经试过上百次了，我教你就行！"


translate chinese zhenya_route_8d3cf7f8:


    "女孩儿犹豫了。"


translate chinese zhenya_route_80585789:


    pi "先拿上止血带..."


translate chinese zhenya_route_150c2823:


    "等等，她为什么要用止血带，那是采血的时候用的啊。"


translate chinese zhenya_route_43e5513f:


    pi "好吧，不用止血带。"


translate chinese zhenya_route_dcfbef84:


    "女孩儿又把它放下了。"


translate chinese zhenya_route_12202aea:


    "我把左手放在边上让她更方便一些。"


translate chinese zhenya_route_1ce043ab:


    unp "可能咱们根本不应该这样吧?"


translate chinese zhenya_route_81d5325a:


    pi "我一点感觉都不会有的。"


translate chinese zhenya_route_574f5a8b:


    "她的手不停的颤抖，针头左右摇晃着。"


translate chinese zhenya_route_8ef32db4:


    "如果她没有扎到血管里那就热闹了。"


translate chinese zhenya_route_5bc3003e:


    "用联盟医疗包进行安乐死可能不是太稳定的娱乐方式..."


translate chinese zhenya_route_4f6d90df:


    unp "别那么看着我。"


translate chinese zhenya_route_5df5d92e:


    "女孩儿突然说。"


translate chinese zhenya_route_1ab9aef2:


    pi "我怎么看你了？别分心！"


translate chinese zhenya_route_11f22c5a:


    unp "我做不到..."


translate chinese zhenya_route_9b411c3c:


    pi "好吧..."


translate chinese zhenya_route_de7ff5a4:


    "我叹了口气，然后把胳膊抬走了。"


translate chinese zhenya_route_be9f07ea:


    pi "算了吧，我睡一觉就好了，别忘了走的时候关灯锁门。"


translate chinese zhenya_route_28fb42e2:


    "女孩儿顺从的往出口走去，但是在半路上站住了。"


translate chinese zhenya_route_d40a0991:


    unp "你真的很痛苦吗?"


translate chinese zhenya_route_cd157731:


    pi "是啊，但是有什么呢？我忍着就好了。"


translate chinese zhenya_route_a609ce96:


    unp "我能做得到...只要...你闭上眼睛。"


translate chinese zhenya_route_15ac0b0f:


    pi "所以我闭上眼睛，又会有什么改变?"


translate chinese zhenya_route_b895e892:


    unp "对我来说容易一些，看起来好像你在睡觉一样。"


translate chinese zhenya_route_5e2e3ab6:


    "她微笑着。"


translate chinese zhenya_route_1c1458d1:


    pi "好吧，我闭上眼睛就是了。"


translate chinese zhenya_route_2fdb022d:


    "一点也不可怕嘛。"


translate chinese zhenya_route_9f19ad89:


    "女孩儿走过来拿起针筒。"


translate chinese zhenya_route_790ba682:


    unp "所以?"


translate chinese zhenya_route_53a2d83d:


    pi "对，对。"


translate chinese zhenya_route_2ffa5e6c:


    "我闭上眼睛，然后大脑中突然闪过一个想法：我为什么这么容易的就相信她了?"


translate chinese zhenya_route_587bd764:


    "就一个小时以前在门口的时候我的想法还完全不一样呢！"


translate chinese zhenya_route_db889481:


    "有什么力量把我压在沙发上，让我不能移动。这个贱人!"


translate chinese zhenya_route_00986cf7:


    "女孩儿拿着止血带靠了过来。"


translate chinese zhenya_route_2737718a:


    "过了一秒钟它被绑在我的脖子上。"


translate chinese zhenya_route_047476be:


    pi "你什么...?"


translate chinese zhenya_route_34a7e6ca:


    "我喘息着。"


translate chinese zhenya_route_a0fcf8d6:


    "即使这就是我一直渴望的死亡，也不应该是这样的，不应该是被止血带勒死！"


translate chinese zhenya_route_465ef323:


    "女孩儿越来越用力，她的表情扭曲了：恶鬼一般的笑容，凹陷的眼睛睁得大大的，甚至闪着光。"


translate chinese zhenya_route_ff6a97ff:


    "我毫无意义的尝试着挣脱，但是没有成功。"


translate chinese zhenya_route_6496742f:


    "我开始出现缺氧血症，眼前的世界变得模糊，但是那扭曲的笑容已经被刻在我的眼睛里！"


translate chinese zhenya_route_65581f39:


    "在我一时的边缘我听到开门的声音，恶鬼般的女孩儿放开了手，另一个女孩儿靠在我的身边。"


translate chinese zhenya_route_b2ada642:


    slp "咱们走！快！"


translate chinese zhenya_route_ada3d630:


    "一个熟悉的声音对我悄悄说着。"


translate chinese zhenya_route_9ce82d61:


    "有人帮我站了起来，我艰难的走出了医务室。"


translate chinese zhenya_route_ce617998_18:


    "..."


translate chinese zhenya_route_871a5dfa:


    "我们慢慢的走着，不时地撞到各种障碍上。"


translate chinese zhenya_route_f59b944c:


    "我不停的昏过去又醒过来，但是一直没法抬头，只能看见脚下的石子路，从医务室通往广场，然后是落满了树叶的地面。"


translate chinese zhenya_route_5ee85201:


    "最后我们终于停了下来，我挣扎着看着四周。"


translate chinese zhenya_route_404bac47:


    "好像是那个少先队员带我来的空地..."


translate chinese zhenya_route_39d09361:


    pi "我今天就一直是循环..."


translate chinese zhenya_route_b92aee43:


    "我试图吸气，但是沉重的咳嗽着，我的手背上都是血。"


translate chinese zhenya_route_b080400f:


    slp "来，喝这个。"


translate chinese zhenya_route_43a9d8cf:


    "女孩儿递给我一个冷的瓶子。"


translate chinese zhenya_route_9e06cea6:


    "我猛喝了几口然后看着我的救命恩人。"


translate chinese zhenya_route_8a6b8e6c:


    "她看上去还是那个熟悉的女孩儿，但是给人的感觉有点不一样。"


translate chinese zhenya_route_8cfe0426:


    "不管怎么说，她的脸蛋和微笑更相配，而不是这样冷冷的决心。"


translate chinese zhenya_route_c196cab9:


    "我都不知道该说什么好..."


translate chinese zhenya_route_966ef311:


    "我在医务室的时候放松了警惕，结果被那个疯子差点干掉，现在开始我更得小心了！"


translate chinese zhenya_route_ca5f5721:


    "虽然我身体上的疼痛让我难以忍受。"


translate chinese zhenya_route_631179f6:


    pi "这是什么...?"


translate chinese zhenya_route_9e9814e8:


    slp "嘘，不要说话，会变糟的！我都知道。"


translate chinese zhenya_route_2e7e3777:


    "女孩儿小心的看着我，好像在等着我的答案。"


translate chinese zhenya_route_70c9b40f:


    slp "你的事情我都知道，我也是...真的。"


translate chinese zhenya_route_efee23f6:


    pi "你怎么可能是真的？！我昨天已经见过你了！我一点儿也没感觉出来你哪里是真的！"


translate chinese zhenya_route_7ac3698b:


    "我又开始咯血。"


translate chinese zhenya_route_2513e664:


    pi "这也是你的玩笑?"


translate chinese zhenya_route_b033ae17:


    "我虚弱的继续说着。"


translate chinese zhenya_route_e83d1d1c:


    pi "你就不能直接杀了我，对吧？你要先折磨我，把我逼疯？！"


translate chinese zhenya_route_e3e89799:


    "然后我意识到这些事情都发生过...只是我们的角色反了过来。"


translate chinese zhenya_route_3e56f2df:


    slp "没有没有，不是那样的！你理解错了，昨天不是我！哎呀我这解释的，你自己没明白。"


translate chinese zhenya_route_39ffacba:


    "女孩儿皱起了眉头。"


translate chinese zhenya_route_8b5427ea:


    slp "你没有遇到过自己的复制品吗?"


translate chinese zhenya_route_0ae95c2e:


    "她说的很有道理，我竟然无言以对。"


translate chinese zhenya_route_8321ad79:


    pi "那好吧...你为什么要这么做？为什么要帮我？"


translate chinese zhenya_route_7d3ad2c5:


    slp "因为你和我是一样的，是真的，这不是很正常的嘛？你如果是我不也会这么做吗？"


translate chinese zhenya_route_ec9da8a5:


    "要是她知道我之前做过什么..."


translate chinese zhenya_route_218c2c76:


    "如果她没有撒谎呢"


translate chinese zhenya_route_b6ba807e:


    "我在这个世界里的确遇到过我之外的人!"


translate chinese zhenya_route_e309eca4:


    pi "感谢，如果是这样的话..."


translate chinese zhenya_route_a5f76af6:


    "女孩儿没有说话，只是笑了笑。"


translate chinese zhenya_route_ad54904e:


    pi "但是我还是不能理解你是...{w}我以前遇到过你很多次，但是她们都是假的。"


translate chinese zhenya_route_c3c67cfd:


    slp "说来话长啊，现在没有时间了！"


translate chinese zhenya_route_bf9c30b9:


    pi "没有时间，你着急去什么地方吗？"


translate chinese zhenya_route_7bd2bccb:


    slp "不只是我！很快咱们就都能逃离这里了！"


translate chinese zhenya_route_794059c2:


    pi "你是说离开这一次循环前往新的?"


translate chinese zhenya_route_48a94368:


    slp "不是不是，我是说离开这个夏令营！"


translate chinese zhenya_route_c6ae4acd:


    pi "什么意思...?"


translate chinese zhenya_route_ec482ad2:


    "我开始傻笑着。"


translate chinese zhenya_route_3bf95c39:


    "我早就放弃了还有出口的想法。"


translate chinese zhenya_route_35c07165:


    slp "你别告诉我你想永远待在这里？"


translate chinese zhenya_route_4faa5838:


    pi "那个我其实已经在这里度过了永恒的时间了..."


translate chinese zhenya_route_c0cdbc2d:


    slp "我可告诉你了喔。"


translate chinese zhenya_route_c4dddafe:


    pi "等等，你理解错了..."


translate chinese zhenya_route_895a960c:


    "不过为什么错了呢？"


translate chinese zhenya_route_4d5b7b29:


    "我准备好回到现实世界了吗?"


translate chinese zhenya_route_b5a196bc:


    "现实世界对我来说已经完全陌生了，我已经把这个夏令营当成自己的家了。"


translate chinese zhenya_route_a6c7a702:


    "但是，这家已成了一片废墟 - 被掠夺和烧毁！"


translate chinese zhenya_route_429f73ec:


    "我可以重新整修吗？"


translate chinese zhenya_route_0a336b4a:


    slp "嗯？所以你想要什么？ "


translate chinese zhenya_route_108a6cc5:


    pi "好吧，但是怎么做？如果你理解我的处境，你一定觉得有必要跟我解释一下..."


translate chinese zhenya_route_438e567e:


    slp "好吧，但是现在不行，我们得先确定没有人追!"


translate chinese zhenya_route_a50296a4:


    pi "等等..."


translate chinese zhenya_route_79498c62:


    "但是少先队员没有听我说话，很快她就消失在了丛林中。"


translate chinese zhenya_route_3d661175:


    "真好..."


translate chinese zhenya_route_2d6f3c2a:


    mz "但是我不会缝!"


translate chinese zhenya_route_72ebe9e8:


    me "真是遗憾...我们管家的重大疏忽!"


translate chinese zhenya_route_7b17d3b9:


    mz "哎呦，看看这是谁说的！你以为你要拿什么做戒指？是不是要用线圈代替啊？"


translate chinese zhenya_route_a4eed316:


    me "我有几个主意..."


translate chinese zhenya_route_c59c53ff:


    mz "说吧，我听着呢。"


translate chinese zhenya_route_84f14225:


    "热妮娅微笑着看着我，好像在坐等我失败。"


translate chinese zhenya_route_205070b7:


    "然而我还没有见到她的时候就已经学会了很多手艺，所以打造一个戒指不成问题。"


translate chinese zhenya_route_c07bca8e:


    "唯一的问题是工具，在这里找不到打造珠宝的工具。"


translate chinese zhenya_route_69c44bc3:


    "我没办法在一个星期内把东西都准备好..."


translate chinese zhenya_route_1ca203ea:


    "如果没有机器人社团的话！"


translate chinese zhenya_route_2551c817:


    "在他们的线路板上搜刮一下，绝对能找到够两个戒指用的金箔。"


translate chinese zhenya_route_0866bdde:


    "而且让社团的同学忙活几天肯定也不成问题，而且我以前还试过！"


translate chinese zhenya_route_bb3c0a72:


    me "惊喜！"


translate chinese zhenya_route_6091a866:


    mz "即使是这样..."


translate chinese zhenya_route_b4a932b1:


    "热妮娅有点不好意思。"


translate chinese zhenya_route_b50a6e08:


    mz "过一个礼拜就会消失的，你知道咱们什么也带不走吧。"


translate chinese zhenya_route_8d6f2a2a:


    me "那怎么了？对你来说哪个更重要，两块破铜烂铁还是...？"


translate chinese zhenya_route_e5b74069:


    mz "没有，当然不是..."


translate chinese zhenya_route_47c8c285:


    me "但是如果咱们要弄得正式一点的话，那裙子也得..."


translate chinese zhenya_route_53695934:


    "热妮娅的眼睛放着光。"


translate chinese zhenya_route_05ebce7a:


    "套装没有问题我知道夏令营里好几个人有。还有暗灰色的。"


translate chinese zhenya_route_6208daa1:


    "夏天的连衣裙很适合热妮娅，婚纱是她的主意！{w}她很正常的马上拒绝了。"


translate chinese zhenya_route_5d04b965:


    me "用网眼纱!"


translate chinese zhenya_route_6c3efbe4:


    mz "你是认真的嘛?"


translate chinese zhenya_route_658e4271:


    "她好像已经不想吵了。"


translate chinese zhenya_route_b98d0d5d:


    me "没有...但是你总可以问问女孩儿们，那样最好了。"


translate chinese zhenya_route_c02501ad:


    "热妮娅红着脸撅起嘴，转了过去。"


translate chinese zhenya_route_fdf97a8a:


    mz "那你就自己去问！记得带上尺子，因为你要自己穿！"


translate chinese zhenya_route_d13b52c7:


    mz "你怎么觉得我能做得到，我的话就直接去找斯拉娃说，你能不能帮我缝一件婚纱？"


translate chinese zhenya_route_6b63784c:


    me "你就说是...服装比赛!"


translate chinese zhenya_route_209d81c1:


    me "不行，那个有点傻..."


translate chinese zhenya_route_4e958dc2:


    me "那就是...化装舞会！"


translate chinese zhenya_route_6a0bdf61:


    "热妮娅有点惊讶，但是没有跟我吵。"


translate chinese zhenya_route_148134b8:


    me "I'我去和辅导员说，别担心，而且也没有什么不好意思的，我们让电子小子和舒里克表演太空行走，未来Cosplay列宁..."


translate chinese zhenya_route_f1b9bc99:


    me "然后咱们就可以结婚！"


translate chinese zhenya_route_15cdfe60:


    mz "那你还想扮演什么历史上的角色吗？"


translate chinese zhenya_route_33626f7e:


    me "天哪，这有什么？选择叶卡捷琳娜二世还有彼得三世都可以！"


translate chinese zhenya_route_341cca1a:


    mz "啊，亲爱的，你的命运真是不值得羡慕啊！"


translate chinese zhenya_route_8796d51d:


    "热妮娅咯咯笑着。"


translate chinese zhenya_route_aea00f45:


    me "没关系，只是举个例子，你看你都已经开始开玩笑了。"


translate chinese zhenya_route_323de6e5:


    mz "不过，那还是有点..."


translate chinese zhenya_route_c8e1c467:


    me "相信我!"


translate chinese zhenya_route_91039827:


    mz "好吧，如果你觉得这样可以的话..."


translate chinese zhenya_route_ce617998_19:


    "..."


translate chinese zhenya_route_333e14d0:


    "快要到黎明了，我发烧越来越严重了。"


translate chinese zhenya_route_21698494:


    "看起来“真正”的少先队员姑娘迷路了，我又被扔在森林里了..."


translate chinese zhenya_route_4bde977b:


    "不过这次我也没有寄太大希望，只要一切都能回到正轨就行，我也没有打算回到现实。"


translate chinese zhenya_route_d802c7b0:


    "也许在另一边只有几个小时，也许有几个世纪，但是我都已经不在乎了。"


translate chinese zhenya_route_f694e34b:


    "我像是一个刚刚苏醒的植物人，不解地看着周围的一切。"


translate chinese zhenya_route_18fc7c9f:


    "地平线燃烧着，向天空中投射出一道极光。"


translate chinese zhenya_route_f25fa47d:


    "也许不只是营地里的人改变了这个世界呢？"


translate chinese zhenya_route_cc66a0fd:


    "再过几分钟这道火光就要把我还有整个夏令营都吞噬掉！"


translate chinese zhenya_route_f7aa9307:


    "我真希望“真的”少先队员能回来！"


translate chinese zhenya_route_a51237ea:


    "就算在一个新世界生活也比在这里烤化了强！"


translate chinese zhenya_route_0cac00e1:


    "而且那里也不会有人知道我在这里发生了什么！"


translate chinese zhenya_route_d3d6bce6:


    "还有我都做过什么..."


translate chinese zhenya_route_95cdb0f9:


    pi2 "你好像在思考着什么？"


translate chinese zhenya_route_64f4c9df:


    "附近出现了熟悉的声音。我的声音..."


translate chinese zhenya_route_9a21a38f:


    pi "你要干什么?"


translate chinese zhenya_route_c8492839:


    "我感觉很吃惊，甚至有点害怕，我已经受够了这些发疯的少先队员了，我也都不思考我的替身的问题了。"


translate chinese zhenya_route_dc0d54d6:


    "不过，我的“替身”？...我什么时候开始这么叫他们了？"


translate chinese zhenya_route_1fbbac28:


    "对，对我来说总有我和他们，我们没有共同点，即使我们都在一起..."


translate chinese zhenya_route_9f450679:


    pi2 "也许你是在思考“这一切是从什么时候开始不对劲的”？"


translate chinese zhenya_route_449bd8e5:


    "这个他说错了，我没有想那个问题。"


translate chinese zhenya_route_43ab8e00:


    pi2 "用各种疑问折磨你自己，询问自己永远找不到答案的问题？"


translate chinese zhenya_route_f91eeb18:


    pi "我只要都结束就行了！你在这里帮不上忙！"


translate chinese zhenya_route_33691edf:


    pi2 "所有事情都早已结束了，如果你是这个意思的话..."


translate chinese zhenya_route_4a413495:


    pi "哦？是吗，我完全没有注意到。"


translate chinese zhenya_route_2212509c:


    pi2 "只有你！那天在公交车上你做了错误的决定，然后死了。"


translate chinese zhenya_route_22952e7f:


    "我仔细的看看自己的身体，很疼，说明我还活着！"


translate chinese zhenya_route_2d38d332:


    pi "我看起来不像是一个死人！"


translate chinese zhenya_route_8258081a:


    pi2 "你以为都像是电影里那么简单吗？你还记的自己的最后一口气吗？记得生命离开自己的时候？"


translate chinese zhenya_route_c9d54b47:


    pi "我不明白你什么意思。那就是说我在现实中已经死了！"


translate chinese zhenya_route_92313099:


    pi2 "不！你曾经有机会，但是你没有抓住，结果你陷入了自己的地狱，自己的潜意识中。"


translate chinese zhenya_route_6aee1bdf:


    pi "啊对，我也发现了美洲，但是我不知道!"


translate chinese zhenya_route_e8ac9853:


    pi2 "你知道，但是你不知道只有你自己在这里了，而且这里没有出口!"


translate chinese zhenya_route_5eb47f22:


    pi "这我知道..."


translate chinese zhenya_route_7c4f3efc:


    "至少我是这么想的。"


translate chinese zhenya_route_4e47c222:


    pi "那你呢?"


translate chinese zhenya_route_4822c2b3:


    pi2 "我不在这里！我只是你的幻觉！"


translate chinese zhenya_route_24b2d9d5:


    "森林里回荡着他恶心的笑声。"


translate chinese zhenya_route_0870ec03:


    "这真是很熟悉，但是为什么要不停的重复呢?"


translate chinese zhenya_route_ed1d079e:


    "他们说祸不单行是真的吗?"


translate chinese zhenya_route_9ec16ea9:


    pi "别客气..."


translate chinese zhenya_route_724dc14a:


    "我闭上眼睛，甚至转了过去。"


translate chinese zhenya_route_997dc7d7:


    pi3 "别那样对他，他也曾经属于咱们！曾经..."


translate chinese zhenya_route_bebf711e:


    "这个熟悉的声音有点不同。"


translate chinese zhenya_route_5b2a770e:


    "现在我面前有两个少先队员站在这里。"


translate chinese zhenya_route_8f95cdc6:


    pi2 "你要以毒攻毒!"


translate chinese zhenya_route_a6e86815:


    pi3 "他已经经历了够多了！"


translate chinese zhenya_route_c8ef17e5:


    pi2 "还不够！"


translate chinese zhenya_route_0c2864de:


    "他们吵个不停，现实已经失去了意义。"


translate chinese zhenya_route_2f5de2b0:


    pi3 "看看，他已经失去意识了！"


translate chinese zhenya_route_044d2365:


    pi2 "没什么特别的，会过去的，就像我们在那个故事里告诉乌里扬卡“疯狂会恢复理智，正常会发疯。”"


translate chinese zhenya_route_67371292:


    "然后又是那邪恶的笑声。"


translate chinese zhenya_route_f0b0ba2b:


    pi2 "你觉得这是他第一次?"


translate chinese zhenya_route_102a2a69:


    pi3 "不是?"


translate chinese zhenya_route_e27da733:


    pi2 "我觉得不是，他觉得自己什么都记得，每个少先队员干什么都记得，但是他忘掉太多了，可能都忘记了一些非常重要的东西..."


translate chinese zhenya_route_c19cedfa:


    uv2 "行了，够了!"


translate chinese zhenya_route_c600ee72:


    "这两个人突然让开了，然后猫耳娘出现了..."


translate chinese zhenya_route_cf99867f:


    "我对她一直比较谨慎，因为我感觉她不会带来什么好事情。"


translate chinese zhenya_route_ea619637:


    "而且她给我的内心带来原始的恐惧，就像是对黑暗的恐惧。"


translate chinese zhenya_route_1bc7b239:


    "我也从来不相信她的故事。{w}比如说她是我们的潜意识什么的！"


translate chinese zhenya_route_628e7777:


    "都是胡扯!"


translate chinese zhenya_route_c0c70154:


    uv2 "你没看见他多么糟糕吗！没有意义！你这..."


translate chinese zhenya_route_9229cca9:


    "她伸手指着那个邪恶的少先队员。"


translate chinese zhenya_route_cd25bf7c:


    uv2 "快去医务室吃药！"


translate chinese zhenya_route_9f2460f9:


    pi2 "什么药...?"


translate chinese zhenya_route_f3f86c57:


    "他有点吃惊。"


translate chinese zhenya_route_654f75d1:


    uv2 "我不知道！你不是这里最聪明的吗？你去弄清楚！"


translate chinese zhenya_route_136154fe:


    "少先队员无力的叹叹气然后往营地那边走去。"


translate chinese zhenya_route_e04e4c0e:


    uv2 "然后你..."


translate chinese zhenya_route_8f4741a0:


    "她又看看“好的”少先队员。"


translate chinese zhenya_route_9d637256:


    uv2 "你...你也去...找点事做。说真的你哭哭啼啼对事情一点好处也没有！"


translate chinese zhenya_route_1187e1a0:


    pi3 "但是我只想帮帮忙..."


translate chinese zhenya_route_ea0da183:


    uv2 "你没帮上忙！快走吧！"


translate chinese zhenya_route_151f70d8:


    "她挥挥手，那种表情让我突然有了一种感觉：就像是不爽猫！"


translate chinese zhenya_route_85e516ba:


    "少先队员没有反驳，跟着他的分身走了。"


translate chinese zhenya_route_5cd1f1b6:


    pi "现在怎么办? "


translate chinese zhenya_route_c0cfa3ab:


    "我没有什么激情的问着。"


translate chinese zhenya_route_6dd0b4ac:


    "我已经被骗了好几次了，我不会再相信谁了，尤其是她！"


translate chinese zhenya_route_698655c7:


    "我可从来没有想过夏令营里这么多人，最后来帮我的是这个猫耳娘！"


translate chinese zhenya_route_01ee43cb:


    "即使只是言语上的，不管怎么说她记得以前的事情，记得我们在公交车上争吵。"


translate chinese zhenya_route_4eddc54b:


    pi "你觉得你还可以再把我当傻子耍？"


translate chinese zhenya_route_eb468b81:


    "我用上全身的力气起来，我的眼前因为疼痛而一片漆黑。"


translate chinese zhenya_route_968edac1:


    "但是我要去哪儿呢?"


translate chinese zhenya_route_5c0580b2:


    "我应该自己带着针管..."


translate chinese zhenya_route_7bea4bf4:


    "我为什么决定在这里自杀是最后的结局?"


translate chinese zhenya_route_6e9bb08c:


    uv2 "嘿!"


translate chinese zhenya_route_f5f49f8f:


    "猫耳娘跳过来扶着我。"


translate chinese zhenya_route_4991f6ed:


    pi "你...你真想让我相信你会帮我吗？"


translate chinese zhenya_route_2c0c4fc1:


    uv2 "你以为我在做什么呢？我一直都在帮你啊。"


translate chinese zhenya_route_74481647:


    pi "啊，对啊..."


translate chinese zhenya_route_b7427c21:


    "我又瘫倒在地上叹着气。"


translate chinese zhenya_route_de2d25b8:


    "我在这样的状态下哪儿也去不了。"


translate chinese zhenya_route_19691844:


    pi "所以你的少先队员们什么时候回来?.."


translate chinese zhenya_route_f0748a5e:


    uv2 "很快..."


translate chinese zhenya_route_0ba6f1f4:


    "她模糊的回答着。"


translate chinese zhenya_route_0a87d3a3:


    pi "好吧，咱们就坐在这里等着吧，反正我也不会再失去什么了..."


translate chinese zhenya_route_a20cefa7_6:


    "..."


translate chinese zhenya_route_e9c63299:


    "整个夏令营都在广场上集合了。"


translate chinese zhenya_route_034dc053:


    "乌里扬卡追着电子小子试图抢走他的纸板做的航天员头盔。"


translate chinese zhenya_route_4f376572:


    "太空行走的场景也不是没有什么好玩的地方，Genda成了临时演员，舒里克在关键的时刻在上面绊倒了。"


translate chinese zhenya_route_48adf945:


    "还好这个距离比近地轨道要近很多而且像是伴随着“Трава у дома”这首歌曲。"


translate chinese zhenya_route_457c4564:


    mt "别跑了！你是不是找死!"


translate chinese zhenya_route_4bd9c425:


    "奥尔加·德米特里耶夫娜在嚎叫着。"


translate chinese zhenya_route_b6eca189:


    "我不知道她更关心哪个，是机器人社团还是这里全年都没有事件发生的图景？"


translate chinese zhenya_route_0b44a339:


    "热妮娅在旁边拽着我的袖子，但是我沉迷在眼前的景色里，没有马上注意到。"


translate chinese zhenya_route_ce73fe66:


    mz "所以说!"


translate chinese zhenya_route_ad02f53a:


    me "什么?"


translate chinese zhenya_route_173b40bd:


    mz "你确定咱们要这么做吗?"


translate chinese zhenya_route_c62cea6e:


    me "但是这是你自己想要的诶!"


translate chinese zhenya_route_5d729a3c:


    mz "I我什么也没想要！是你提议的，然后我同意了..."


translate chinese zhenya_route_bbd48daa:


    me "咱们已经谈了上百次了，而且你也知道这不是真的。"


translate chinese zhenya_route_8dad8ac4:


    mz "所以不是真的，是吗?"


translate chinese zhenya_route_cb64fecf:


    "她可怕的说着。"


translate chinese zhenya_route_2cf45de0:


    me "我不是这个意思...只是因为咱们在这个世界里，所以不管做什么，过了几天所有人都会忘掉的。"


translate chinese zhenya_route_26238f0b:


    mz "对，但是我会记住的！"


translate chinese zhenya_route_070599c2:


    me "所以你是因为谁不好意思的？是周围的人还是你自己？"


translate chinese zhenya_route_e9fca54d:


    "热妮娅红着脸，没有说话。"


translate chinese zhenya_route_aed58c1a:


    mt "好了，去换衣服吧！"


translate chinese zhenya_route_40fea5f9:


    "奥尔加·德米特里耶夫娜向我们下达命令。"


translate chinese zhenya_route_a752ccba:


    me "准备好了吗?"


translate chinese zhenya_route_38c18bf5:


    "热妮娅点点头。"


translate chinese zhenya_route_ce617998_20:


    "..."


translate chinese zhenya_route_53d34154:


    "十分钟以后少先队夏令营里罕见的景象就要上演了。"


translate chinese zhenya_route_a219887e:


    "哦，我在说什么，这对整个小猫头鹰来说都是很罕见的！"


translate chinese zhenya_route_907927ec:


    "我也感觉很奇怪，在一群少先队员中穿着老旧的苏联礼服。"


translate chinese zhenya_route_3903c48c:


    "当然不是燕尾服什么的，但是也要比短裤还有T恤好一些。"


translate chinese zhenya_route_941c499b:


    "热妮娅则穿着匆忙赶制的看起来像是婚纱的衣服。"


translate chinese zhenya_route_159608c4:


    "如果是别的时候我可能说她看起来很蠢，但是现在可不一样。"


translate chinese zhenya_route_9d9d5e6c:


    "现在这条裙子看起来比任何浮华的服装店里卖的衣服都要漂亮。"


translate chinese zhenya_route_f29ba8f0:


    mt "所以你们现在在表演什么，我忘了?"


translate chinese zhenya_route_567dd942:


    "辅导员对我悄悄说着。"


translate chinese zhenya_route_4fdd0140:


    me "奥尔加·德米特里耶夫娜，有什么关系呢！咱们开始吧!"


translate chinese zhenya_route_025b7514:


    mt "好吧..."


translate chinese zhenya_route_de8c4bfc:


    "热妮娅像一个龙虾一样红，不知道眼睛该看哪里。"


translate chinese zhenya_route_a876aab3:


    "她好像很想看着我。"


translate chinese zhenya_route_0e1a4307:


    me "一切都会好起来的。"


translate chinese zhenya_route_5788b9db:


    "我悄悄的说。"


translate chinese zhenya_route_ff4fa4d8:


    "未来在人群中欢快的笑着，好像这是她自己的婚礼一样，有点二，但是很真诚。"


translate chinese zhenya_route_3c5ef511:


    "列娜和平常一样脸红红的，但是还是认真的看着我们。"


translate chinese zhenya_route_d006862e:


    "阿丽夏看起来好像很不情愿的被带到这里来似的，要不然她就去弹吉他或者睡觉了。"


translate chinese zhenya_route_7b0dcbe4:


    "乌里扬卡倒是玩的很开心：小孩子很容易开心啊。"


translate chinese zhenya_route_002294ae:


    "斯拉娃看上去真的为我们开心，她欣慰的笑着。"


translate chinese zhenya_route_fc45d68b:


    mz "咱们快点吧。"


translate chinese zhenya_route_337d23ad:


    "好像再慢一点热妮娅就要不好意思死了。"


translate chinese zhenya_route_01822020:


    me "好吧。"


translate chinese zhenya_route_280f303c:


    me "我们准备好了。"


translate chinese zhenya_route_ab16cb35:


    "我朝辅导员点点头。"


translate chinese zhenya_route_27d5273b:


    "现在我得说他们在婚礼上经常说的话。"


translate chinese zhenya_route_daa4ab51:


    "当然我不知道具体应该说什么，而且还成功的把自己准备好的词都忘掉了。"


translate chinese zhenya_route_4208fbed:


    me "我发誓..."


translate chinese zhenya_route_9eb7ae3d:


    "我的声音开始颤抖。"


translate chinese zhenya_route_145c5153:


    me "不论年老还是生病..."


translate chinese zhenya_route_100598bd:


    "不对，有问题。"


translate chinese zhenya_route_32f2392a:


    us "你在嘀咕什么啊！我们完全听不见啊！"


translate chinese zhenya_route_1770bf88:


    "乌里扬卡激动的喊着。"


translate chinese zhenya_route_2676b165:


    dv "你安静，不然把你换上去。"


translate chinese zhenya_route_8155be36:


    "这好像很管用，乌里扬娜马上安静下来了。"


translate chinese zhenya_route_90d25991:


    me "嗯，那是..."


translate chinese zhenya_route_3b342837:


    "一分钟以前我还可以说自己非常冷静呢，但是现在我一句话也说不出来。"


translate chinese zhenya_route_cca9bafd:


    "我只能猜测热妮娅是什么感觉。"


translate chinese zhenya_route_1e527a61:


    "我鼓起勇气。"


translate chinese zhenya_route_1d086fbd:


    me "我发誓和你永远在一起，不论是好是坏，富有还是贫穷，疾病还是健康，直到死亡将我们分离!"


translate chinese zhenya_route_7aaa9384:


    "一直吵闹个不停的少先队员们突然安静了。"


translate chinese zhenya_route_b4112c36:


    "实在是太安静，让我都能感觉到自己心脏的跳动。"


translate chinese zhenya_route_6d361550:


    "热妮娅继续站着，她的眼睛盯着地面。"


translate chinese zhenya_route_5d3c27fa:


    me "嗯，现在到你了!"


translate chinese zhenya_route_7b26ab8b:


    "她深吸一口气。"


translate chinese zhenya_route_7f141d67:


    mz "我答应嫁给你，不论贫穷还是疾病。行了！"


translate chinese zhenya_route_5c93db98:


    "人群爆发出笑声。"


translate chinese zhenya_route_f65f12c2:


    "虽然对我来说热妮娅的话显得很严肃。"


translate chinese zhenya_route_fde7233b:


    mz "快结束吧。"


translate chinese zhenya_route_e0f4c4c3:


    me "还有“现在你可以亲吻新郎”呢？"


translate chinese zhenya_route_1a014c13:


    "我开玩笑的问着。"


translate chinese zhenya_route_8492c157:


    "热妮娅眼睛闪着光，好像说清楚这种情况下即使不接吻也没问题。"


translate chinese zhenya_route_c8b5fcd7:


    "我觉得她可能没错，而且我们也有很多时间。"


translate chinese zhenya_route_a718aa85:


    mt "很好！感谢大家！"


translate chinese zhenya_route_f52b105b:


    "奥尔加·德米特里耶夫娜呼喊着，电子小子拿着照相机走了过来。"


translate chinese zhenya_route_4f2d1d7d:


    "他看上去好像宁可拿着一根撬棍。"


translate chinese zhenya_route_6ddf4f60:


    el "有很多个瞬间可以留念..."


translate chinese zhenya_route_e70a6fe5:


    mz "天哪别!"


translate chinese zhenya_route_a312257a:


    "热妮娅哭号。"


translate chinese zhenya_route_1b473258:


    me "来吧，为了合影。"


translate chinese zhenya_route_b402f943:


    "她犹豫了一会儿，不过好像还是笑了。"


translate chinese zhenya_route_cd93bb8a:


    me "很好！比预想的还要好!"


translate chinese zhenya_route_cd8ad1b8:


    "这是第一次热妮娅盯着我的眼睛说"


translate chinese zhenya_route_bcd11e4b:


    mz "是啊，大概是。"


translate chinese zhenya_route_ce617998_21:


    "..."


translate chinese zhenya_route_0852e54f:


    "我在一天快要过去时才醒过来。"


translate chinese zhenya_route_0bb1d15b:


    "高烧已经褪去，我在右手上看到了一个注射的痕迹。"


translate chinese zhenya_route_532743cd:


    "猫耳娘真的帮助我了吗...?"


translate chinese zhenya_route_5ca624bb:


    "经过过去的几天我都已经不相信这里还有好人了。"


translate chinese zhenya_route_762d0ac9:


    "我费劲的站起来往营地走去。"


translate chinese zhenya_route_7dac7415:


    "反正我在森林里也没有什么事可做。"


translate chinese zhenya_route_67da5e6b:


    "我又一次开始思考，我必须要忍受，我要忍到下一次轮回，我的身体可以恢复，精神也可以恢复。"


translate chinese zhenya_route_d272d59d:


    "不要绝望，不要变得跟那个人似的！"


translate chinese zhenya_route_85b36826:


    "我就是死也不想那样活着！"


translate chinese zhenya_route_a20cefa7_7:


    "..."


translate chinese zhenya_route_3a421f90:


    "背着包包的少先队员在广场蹦蹦跳跳。"


translate chinese zhenya_route_cb975bdd:


    "辅导员站在雕塑旁边双手叉腰很正式的喊着。"


translate chinese zhenya_route_7b2c05b0:


    "我僵在了原地不知道该干什么。"


translate chinese zhenya_route_f64d961c:


    "这个世界在不停地变化，昨天她们还打算杀我呢，但是现在呢？"


translate chinese zhenya_route_f711d58a:


    mtp "嘿，谢苗!"


translate chinese zhenya_route_303ef93d:


    "辅导员威胁的对我喊着。"


translate chinese zhenya_route_058c857c:


    pi "什么？我？你是在叫我吗？"


translate chinese zhenya_route_98ac5311:


    mtp "你怎么还没有收拾？"


translate chinese zhenya_route_f8ea68a4:


    pi "收拾？我马上..."


translate chinese zhenya_route_5268cef9:


    mtp "今天是最后一天了！你又忘了？快去收拾！"


translate chinese zhenya_route_4267b07b:


    "我不想和她争吵，我没有力气，而且从很久以前我就不再惊讶了。"


translate chinese zhenya_route_6c4fab7d:


    pi "你看我这样行吗？"


translate chinese zhenya_route_e9b3d6f4:


    "我指指自己一身的伤还有破烂的衣服。"


translate chinese zhenya_route_4eb3fa9a:


    mtp "你怎么了?"


translate chinese zhenya_route_ee50b2d9:


    "辅导员真的很奇怪。"


translate chinese zhenya_route_3c6b857f:


    mtp "你是在森林里奔跑然后摔倒了吗？"


translate chinese zhenya_route_077afa71:


    pi "你在说什么？什么森林？我为什么要在森林里跑？"


translate chinese zhenya_route_64416fdd:


    mtp "好了，不关我的事...我们再过半个小时就要出发了，你要是不来就要再待一个礼拜了。"


translate chinese zhenya_route_fde537c1:


    "她傻笑着。"


translate chinese zhenya_route_a5626efa:


    "再待一个礼拜..."


translate chinese zhenya_route_7cbd0db3:


    "不对，这绝对超出我的计划了！"


translate chinese zhenya_route_8cc96c2a:


    "这里总会出现愚蠢的离开的方法！"


translate chinese zhenya_route_af3b5ab5:


    "我慢慢往公交车站走去。"


translate chinese zhenya_route_ad26bb5a:


    "整个夏令营的人都在车站前拥挤着。"


translate chinese zhenya_route_a27f23dc:


    "所有人都在等着辅导员，反正她的话就是命令嘛。"


translate chinese zhenya_route_37559c26:


    unp "啊，你怎么了?.."


translate chinese zhenya_route_9673889c:


    "医务室那个女孩儿充满母性的看着我。"


translate chinese zhenya_route_0b9afc20:


    slp "你还好吗？需要绷带吗？"


translate chinese zhenya_route_dd4b010c:


    "那就够吗？我需要夹板!"


translate chinese zhenya_route_3f25e7ab:


    dvp "肯定掉坑里哭个不停。"


translate chinese zhenya_route_662ce80f:


    "哼..."


translate chinese zhenya_route_2872693b:


    usp "被狼追上怎么办？!"


translate chinese zhenya_route_12528bfb:


    "狼只有可能是这里的少先队员..."


translate chinese zhenya_route_222786fb:


    "我安静的坐在路边捂着脸。"


translate chinese zhenya_route_aef7bae3:


    "I我都不想思考明天会发生什么事情，我是会进入新的轮回还是就此永久结束。"


translate chinese zhenya_route_cfa4c35a:


    "或者说前面还有什么等着我...?"


translate chinese zhenya_route_ce617998_22:


    "..."


translate chinese zhenya_route_cae10f7f:


    "我醒来的时候热妮娅不再旁边。"


translate chinese zhenya_route_65b807df:


    "一定是去处理个人的事情了。"


translate chinese zhenya_route_f9aba6a3:


    "天哪，她为什么需要这个少先队员游戏?!"


translate chinese zhenya_route_90bc68a6:


    "尤其是在结婚之夜!"


translate chinese zhenya_route_9a8da883:


    "我舒服的伸着懒腰。"


translate chinese zhenya_route_9d92e2de:


    "至少我可以给她个惊喜，去森林里采集花朵。"


translate chinese zhenya_route_9b419756:


    "虽然算不上什么贵重的礼物，但是我知道她喜欢百合。"


translate chinese zhenya_route_523c3f6b:


    "我可能永远无法理解为什么女人喜欢花朵..."


translate chinese zhenya_route_d823504c:


    "荨麻或者豚草就不行?"


translate chinese zhenya_route_04a18347:


    "它们也是植物，而且还都是野生的，不是种在盆里什么的。"


translate chinese zhenya_route_206496b3:


    "不过既然每次我给她送花的时候热妮娅都会灿烂的对我笑，我又需要在乎什么呢?"


translate chinese zhenya_route_1919fcf8:


    "当然可能并不只是花的缘故..."


translate chinese zhenya_route_f7f98ab4:


    "我拿着花束走进图书馆，大声的打开门。"


translate chinese zhenya_route_d823e1d6:


    "阿丽夏坐在管理员的位置上，很阴沉的翻着杂志。"


translate chinese zhenya_route_4bc48b58:


    dv "哇，新婚丈夫出现了。"


translate chinese zhenya_route_38e73ed3:


    "她愁眉苦脸的嘟囔着。"


translate chinese zhenya_route_b3705464:


    me "你好，热妮娅去哪儿了...?"


translate chinese zhenya_route_b20b9d5a:


    dv "不晓得。她叫我替她坐一会儿然后就走了。"


translate chinese zhenya_route_9edc69e2:


    "啊，对啊，我的热妮娅甚至可以劝得动阿丽夏。."


translate chinese zhenya_route_21e4d285:


    me "好吧，那我就..."


translate chinese zhenya_route_59986aee:


    dv "而且看起来她一点幸福的表情都没有。"


translate chinese zhenya_route_23858456:


    "阿丽夏在我背后说着。"


translate chinese zhenya_route_6b3f4d60:


    "热妮娅去哪儿了?"


translate chinese zhenya_route_48ccafa2:


    "也许她也想给我一个惊喜?"


translate chinese zhenya_route_c1260382:


    "不对，那不是她的风格，可能只是去吃早饭了吧。"


translate chinese zhenya_route_aa30435e:


    "我前往，不对，跑向食堂。"


translate chinese zhenya_route_b11e70d8:


    "少先队员们欢乐的喧闹着，大厅里充满了碟子敲击的交响乐，连平时很阴沉的厨师阿姨都很欢乐。"


translate chinese zhenya_route_0b93c284:


    "我发现了斯拉娃，然后走过去找她。"


translate chinese zhenya_route_94ff2553:


    me "早上好，有没有看到热妮娅?"


translate chinese zhenya_route_a850b845:


    "斯拉娃惊讶的看着我，然后不好意思的移开了目光。"


translate chinese zhenya_route_ddb1c9c0:


    sl "我觉得她可能去公交车站了。"


translate chinese zhenya_route_89d4ed9c:


    me "公交车站？为什么？"


translate chinese zhenya_route_16a22502:


    sl "我不知道...你自己去问她吧。快点的话你还能赶上。"


translate chinese zhenya_route_2ef037d1:


    "去公交车站？去公交车站！"


translate chinese zhenya_route_27f648fd:


    "不到一分钟我冲过了大门..."


translate chinese zhenya_route_1baec8fb:


    "我看到热妮娅站在道路中间，望着远方的地平线，穿过平原和森林，追寻着明亮的夏日。"


translate chinese zhenya_route_f15d9551:


    "她脸上有种奇怪的表情，悲伤，期望，痛苦混合在一起，而且看起来不太一样，我感觉自己不认识她。"


translate chinese zhenya_route_581fe4cb:


    "更好？不对，我还不如相信我明天可以回到现实世界。"


translate chinese zhenya_route_d91dba14:


    "可能以前更好？"


translate chinese zhenya_route_924ac1c6:


    "那个少先队员好像说我忘记了很多东西..."


translate chinese zhenya_route_95e3c63e:


    "但是具体是什么呢?"


translate chinese zhenya_route_867fc00d:


    me "嘿，我一直在找你。"


translate chinese zhenya_route_8e4d71a5:


    "听起来很愚蠢，我的声音在颤抖，我必须得说点什么，所以我假装笑了笑，尽量友好的说。"


translate chinese zhenya_route_8df9005a:


    mz "你想要怎样?"


translate chinese zhenya_route_ac521edd:


    "热妮娅冷冷的回答着。"


translate chinese zhenya_route_fba62ae7:


    me "我起来发现你不见了，所以"


translate chinese zhenya_route_ff81478a:


    "热妮娅转了过来，她的眼睛闪着光。"


translate chinese zhenya_route_01ebb44b:


    mz "我要走了。"


translate chinese zhenya_route_f2dc887f:


    "她冷静地说着。"


translate chinese zhenya_route_25f7abbc:


    me "去哪儿?"


translate chinese zhenya_route_86c8528c:


    mz "回家。"


translate chinese zhenya_route_fb5e4a90:


    me "但是公交车不会来这里啊!"


translate chinese zhenya_route_e03488f6:


    "公交车？什么鬼？！"


translate chinese zhenya_route_75e90b09:


    mz "它们会来的。"


translate chinese zhenya_route_93c27914:


    me "这个，我给你做的..."


translate chinese zhenya_route_2fab920e:


    "我递给她那束花，结果瞬间就到了地上。"


translate chinese zhenya_route_293d1e5d:


    mz "我不要你的东西！你！昨天...我不知道发生了什么，但是肯定都是你的错！"


translate chinese zhenya_route_66e6b111:


    me "什么是我的错?"


translate chinese zhenya_route_40de19bd:


    mz "所有的事情！这个见鬼的婚礼，晚上发生的事情..."


translate chinese zhenya_route_e218d65f:


    me "但是我们同意了的...你自己说的！你忘了几个星期以前？"


translate chinese zhenya_route_f22f02db:


    mz "我几个星期以前还不认识你!"


translate chinese zhenya_route_0afb7a8c:


    "对，都是那个时候开始的。"


translate chinese zhenya_route_89eaae80:


    "或者说继续的，我不在乎了！"


translate chinese zhenya_route_8de561c3:


    "我觉得我可以幸福的和热妮娅在一起，但是她突然就消失了，就跟她来的时候一样。"


translate chinese zhenya_route_47016d9b:


    "热妮娅没有坐在图书馆，而是在食堂门口—那已经是很久以前的事情了。"


translate chinese zhenya_route_a49f0e42:


    "我曾经失去过信念，我放弃了人类的身份，然后我遇到了她，我感觉生活给了我第二次机会，尽管是在这个奇怪的世界上。"


translate chinese zhenya_route_e96d834d:


    "不过，她又走了，然后在无尽的循环之中我忘记了一切，我回到了遇到她以前的状态，我不再是我自己了…"


translate chinese zhenya_route_8209fb60:


    "可能“他”说对了？..."


translate chinese zhenya_route_713b6223:


    me "你在说什么？我们已经认识了...{w}我已经不记得那是多长时间了！"


translate chinese zhenya_route_53223da6:


    mz "我不认识你，我也不想认识你！"


translate chinese zhenya_route_5dc1718e:


    me "等等，咱们..."


translate chinese zhenya_route_4892f21d:


    "我突然感觉很虚弱，我跪在了地上。"


translate chinese zhenya_route_2cc03992:


    "手指上的戒指感觉很烫，甚至是身体上的感觉。"


translate chinese zhenya_route_4d5dd590:


    mz "这个，拿着!"


translate chinese zhenya_route_66517b70:


    "热妮娅掏出来什么东西扔给我。"


translate chinese zhenya_route_35c23299:


    mz "我要去走走!"


translate chinese zhenya_route_f4572a11:


    "我扭曲着订婚戒指嚎啕大哭。"


translate chinese zhenya_route_f6b5b83d:


    "我从来没有这样哭过，在这个世界里没有过，在之前的生活中也没有过..."


translate chinese zhenya_route_11c27875:


    "然后一切都结束了。"


translate chinese zhenya_route_2b610b28:


    "永远的。"


translate chinese zhenya_route_8c05f068:


    "我可能还留有一些人性，所以我决定把这一切都忘掉，为了不让自己发疯。"


translate chinese zhenya_route_2a64dbfe:


    "但是现在我的所有记忆都在这里了，同时伴随着痛苦，可能没有那么强烈，随着时间的流逝在逐渐变弱，但是还是在撕裂着我的心。"


translate chinese zhenya_route_f7d9e51c:


    "我好像被撕成碎片，唯一剩下的一片就是还有一点意识的。"


translate chinese zhenya_route_4f7b01ff:


    "不是一个，不是半个，只是一点巧合，在这个完美的世界里的一个小错误。"


translate chinese zhenya_route_302b08db:


    pi4 "别伤心，哥们儿!"


translate chinese zhenya_route_58fabdec:


    "一个少先队员坐在我旁边。"


translate chinese zhenya_route_90087846:


    pi "又是你?"


translate chinese zhenya_route_4f5d7a29:


    pi4 "我谁?"


translate chinese zhenya_route_00129a4d:


    pi "有什么区别？你，他，那个，这个，都一样嘛!"


translate chinese zhenya_route_4c2f272f:


    "他没有回答，只是不停地乐着。"


translate chinese zhenya_route_10c3756d:


    pi "你又来嘲笑我了吗？可能那次你要是杀了我更好。"


translate chinese zhenya_route_32babbd7:


    pi4 "但是你都记得。"


translate chinese zhenya_route_cce9bc51:


    pi "我都记得，所以才很糟糕！"


translate chinese zhenya_route_56a872ce:


    pi4 "你是一个错误。"


translate chinese zhenya_route_a3eb2c09:


    pi "什么的错我是？阻止你离开那个猫儿女孩儿？我知道公交车上是你!"


translate chinese zhenya_route_a2bffe06:


    "少先队员的脸上还是没有表情。"


translate chinese zhenya_route_6e23c048:


    pi4 "可能是，可能不是，有什么区别?"


translate chinese zhenya_route_2b5b71d1:


    pi "就是说你...出去了?"


translate chinese zhenya_route_71961912:


    pi "然后回来只是为了嘲笑我?"


translate chinese zhenya_route_e1828c1c:


    pi4 "你不也是这么做的嘛?"


translate chinese zhenya_route_9ebc18c6:


    pi "别跟我扯这个!"


translate chinese zhenya_route_d6d21cef:


    "我跪着，看着热妮娅一步步离开。"


translate chinese zhenya_route_b7251b6d:


    "她走过拐角，我无法挽回她..."


translate chinese zhenya_route_28c9f488:


    "不管怎么说，我知道这不是热妮娅，我真正的热妮娅昨天消失了，这只是一个没有灵魂的木偶。{w}但是看起来真的一样..."


translate chinese zhenya_route_03273ca5:


    "这个故事不能无限的持续下去!"


translate chinese zhenya_route_c7744369:


    "这个世界真的是地狱，我就是恶魔。{w}而恶魔是无法获得真正的幸福的。"


translate chinese zhenya_route_e23c07ec:


    pi "就告诉我一件事。"


translate chinese zhenya_route_330cac58:


    "我打破了沉默。"


translate chinese zhenya_route_d964d736:


    pi "这一切都是为什么?"


translate chinese zhenya_route_64fb9aaa:


    pi4 "你从最一开始有一个机会，你本来可以做得完全不一样，很多人做对了，和你还有一些其他的人不太一样。"


translate chinese zhenya_route_a49737fe:


    pi "啊，对，他们都愉快的离开了这里然后和他们的娃娃愉快的生活在“真实”世界里！"


translate chinese zhenya_route_fbd62a8b:


    "我讥笑着。"


translate chinese zhenya_route_0d254476:


    pi4 "那你觉得自己的生活更好?"


translate chinese zhenya_route_89deef73:


    "他强调了一下“生活”这个词。"


translate chinese zhenya_route_d8d4112a:


    pi "如果你是...如果你是把热妮娅从我身边夺走的人..."


translate chinese zhenya_route_46c6c1d3:


    "我的内心开始愤怒，我想要把他暴揍致死!"


translate chinese zhenya_route_fe54ef92:


    "但是我不知道他是不是实体。"


translate chinese zhenya_route_a15e6140:


    "但是那又能有什么变化呢?"


translate chinese zhenya_route_523cc556:


    pi4 "你和其他的女孩儿可以快乐的生活的，这是她们来到这里的原因。"


translate chinese zhenya_route_a68dd09f:


    "我没有想到这样的答案。"


translate chinese zhenya_route_ac729750:


    pi "但是她们都不是真的！真正的人是我...你...还有...热妮娅..."


translate chinese zhenya_route_e55f7952:


    pi4 "那只是对你来说，在你的世界里，你知道在这里有很多的现实，你都可以选择不同的命运，可能只有一个谢苗，但是这个夏令营给了他不同的选择。"


translate chinese zhenya_route_0248499f:


    pi4 "有的人设法离开了，然后快乐的生活着，也有的人留在了这里，比如说你。"


translate chinese zhenya_route_ab00125a:


    pi "你可以告诉我这是什么地方吗？还有你是谁？"


translate chinese zhenya_route_755b6861:


    pi4 "你可以认为我是辅导员。"


translate chinese zhenya_route_80725814:


    "少先队员对我微笑了一下，然后没有任何嘲讽或者嘲笑的意思，消失了。"


translate chinese zhenya_route_45f9e91b:


    "听到他的话我感觉轻松了一些。"


translate chinese zhenya_route_2ce94d4c:


    "这个世界突然被卷入浓雾，然后我的眼前出现了一道光明。"


translate chinese zhenya_route_9c6336fb:


    "我看了看四周，发现少先队员们已经消失了，公交车也一样。"


translate chinese zhenya_route_7fbcf6f6:


    "又怎么了?"


translate chinese zhenya_route_48843b81:


    "大门静静的发出吱呀的响声，我躲在雕塑后面。"


translate chinese zhenya_route_66517463:


    "谢苗走到了公交车站，其中一个谢苗，但是他脸上痛苦的表情很熟悉。"


translate chinese zhenya_route_07e4236c:


    "我已经早就忘了，我自己也能是那个样子。"


translate chinese zhenya_route_cb5e1464:


    "他盯着公路看了一会儿，然后又动身返回营地了。"


translate chinese zhenya_route_2016aa78:


    pi "别相信他。"


translate chinese zhenya_route_2eea460b:


    "我冲动的说着。"


translate chinese zhenya_route_0a2309cb:


    me "你是谁?!"


translate chinese zhenya_route_3e4dff80:


    "谢苗看起来很惊讶。"


translate chinese zhenya_route_d6271c95:


    pi "站住！别过来!"


translate chinese zhenya_route_c41b6a4c:


    me "好吧，我就待在这里吧..."


translate chinese zhenya_route_9f354a24:


    pi "你看见他了吗？跟他说过话了吗？"


translate chinese zhenya_route_3edfb9a5:


    me "你在说什么?"


translate chinese zhenya_route_65856116:


    pi "你懂的..."


translate chinese zhenya_route_9513cd87:


    me "嗯..."


translate chinese zhenya_route_e3fcbbd3:


    pi "他跟你说什么了?"


translate chinese zhenya_route_e88413fc:


    me "其实没什么..."


translate chinese zhenya_route_5d3e0c2a:


    pi "他给你建议了吗？告诉你去哪儿？威胁你了吗？"


translate chinese zhenya_route_3a4fb558:


    me "没有，倒是没有...他看起来很奇怪，但是没有什么别的了..."


translate chinese zhenya_route_7896c94c:


    pi "你要知道，他可能不是一个人，或者说他可能是一个人，但是你还会遇到其他看起来差不多的少先队员。"


translate chinese zhenya_route_0effa2c7:


    me "你呢？你是谁？你在躲什么？"


translate chinese zhenya_route_18d9b086:


    pi "你会明白的...总有一天...只要记住，最重要的事情是找到出口！"


translate chinese zhenya_route_5adb12a4:


    "风变得很硬，然后同样一道闪光又出现在了我的面前。"


translate chinese zhenya_route_c1d579b7:


    "然后突然间，少先队员还有公交车都出现了。"


translate chinese zhenya_route_affe749c:


    "我不知道自己为什么要说这些..."


translate chinese zhenya_route_72e87799:


    "我觉得应该这样。"


translate chinese zhenya_route_175b483f:


    "如果我还记得懊悔是什么感觉，那么我现在就是这种感觉。"


translate chinese zhenya_route_6da9f141:


    mz "少先队员要时刻准备着!"


translate chinese zhenya_route_174cdd90:


    "一个熟悉的声音突然出现，就像是晴天霹雳，然后熟悉的背后一记重击，解除了我的紧张。"


translate chinese zhenya_route_9b924f7d:


    "热妮娅站在我面前。"


translate chinese zhenya_route_3c83f193:


    "我马上发现这是真正的热妮娅—不是木偶！"


translate chinese zhenya_route_00a94cb1:


    pi "但是...怎么..."


translate chinese zhenya_route_7f9e842d:


    "我只能说出这些话。"


translate chinese zhenya_route_ccbc9e0b:


    mz "我不知道..."


translate chinese zhenya_route_e0c9575f:


    "她笑着，但是声音在不停地颤抖着。"


translate chinese zhenya_route_a0d084f7:


    mz "醒来之后我到了另外一个世界，我以为我失去你了，但是经过了很多我已经忘记的循环..."


translate chinese zhenya_route_fc62eb93:


    pi "抱歉。"


translate chinese zhenya_route_5788b9db_1:


    "我悄悄的说。"


translate chinese zhenya_route_c6ad8ebd:


    pi "原谅我。"


translate chinese zhenya_route_514806e6:


    mz "没什么。但是“他”出现了，他说可以带我来找你，我开始不太相信，但是...我回来了！"


translate chinese zhenya_route_a283dd49:


    "我冲上前去抱住热妮娅。"


translate chinese zhenya_route_f3cdb758:


    pi "原谅我！我忘记了...忘记了一切，忘记了你。我对你做了很多不好的事情，真的很抱歉！"


translate chinese zhenya_route_f64a5bd9:


    mz "呜呜，你要憋死我了！"


translate chinese zhenya_route_70e18faf:


    "她挣脱了我的怀抱。"


translate chinese zhenya_route_d95a0aeb:


    mz "但是我回来了，所以一切都没有问题的!"


translate chinese zhenya_route_97688933:


    "她喊着眼泪说着。"


translate chinese zhenya_route_c21de771:


    pi "你怎么会这么冷静?"


translate chinese zhenya_route_25a697d5:


    mz "因为我一直知道咱们会在一起的。"


translate chinese zhenya_route_af19cecb:


    "热妮娅抬起右手，我看到她的手指上，用百合花编成的戒指。"


translate chinese zhenya_route_d16e8b07:


    pi "抱歉，我把自己的弄丢了..."


translate chinese zhenya_route_7f2fc500:


    mz "没关系，还能买一个新的!"


translate chinese zhenya_route_06ab0532:


    "她笑了。"


translate chinese zhenya_route_5608eebc:


    mtp "嘿，快上车，你们俩!"


translate chinese zhenya_route_7199c752:


    "奥尔加·德米特里耶夫娜很不满的喊着。"


translate chinese zhenya_route_ce617998_23:


    "…"


translate chinese zhenya_route_5fa8d405:


    "公交车一路颠簸的驶向城镇中心。{w}或者是其他的某个地方!"


translate chinese zhenya_route_8e91937c:


    pi "你知道，我真的不会相信这一切都要结束了。"


translate chinese zhenya_route_bb22c22e:


    "过去的一个礼拜，我经历了太多事情..."


translate chinese zhenya_route_51ff3d8a:


    mz "你又一次来到了最有趣的部分!"


translate chinese zhenya_route_7730d48a:


    "热妮娅一脸愠色。"


translate chinese zhenya_route_0db5e9a1:


    pi "你不会想和我交换位置的。"


translate chinese zhenya_route_7ba78cc4:


    mz "我可以想象，但是这总要比坐在图书馆里好得多！"


translate chinese zhenya_route_661c1c64:


    pi "你对有趣的事情总是有奇怪的见解..."


translate chinese zhenya_route_2c6f48c5:


    mz "你想过了吗?"


translate chinese zhenya_route_3a9e0089:


    pi "什么?"


translate chinese zhenya_route_745bf00d:


    mz "明天会发生什么？咱们在现实世界还是会见面的！"


translate chinese zhenya_route_9c5be399:


    pi "哦，对了，我来把你的地址记下来。"


translate chinese zhenya_route_902c41be:


    mz "啊，对啊，所以你现在就可以给我寄信了!"


translate chinese zhenya_route_d2a39874:


    "我笑了。"


translate chinese zhenya_route_7d57dcd6:


    mz "我会记住的，快说吧。"


translate chinese zhenya_route_5317f0d7:


    "我们交换了地址和电话号码。"


translate chinese zhenya_route_a4e5d71a:


    "奇怪的是我们以前没有想到过。"


translate chinese zhenya_route_0161af1e:


    "我估计是我们从来没有想过要离开这里，也不想回想起自己过去的生活。"


translate chinese zhenya_route_af2214a1:


    mz "你觉得会怎样？咱们只是睡着吗？"


translate chinese zhenya_route_db2a144a:


    "热妮娅紧握着我的手，把整个身体靠过来。"


translate chinese zhenya_route_2d69ac30:


    pi "希望是这样，但是最后这也不是最糟糕的!"


translate chinese zhenya_route_909b2197:


    mz "可以说是最好的了。"


translate chinese zhenya_route_5464c3dd:


    "热妮娅打了个呵欠，然后闭上了眼睛。"


translate chinese zhenya_route_c7d14696:


    mz "做个好梦吧，再见了。"


translate chinese zhenya_route_d0f0885a:


    "我没有说什么，只是继续看着窗外飞过的风景。"


translate chinese zhenya_route_1f47ecf1:


    "小猫头鹰夏令营被扔在了后面，我可能再也不会回到那里了。"


translate chinese zhenya_route_e536a6fa:


    "嗯，这次我很确定。"


translate chinese zhenya_route_900f8104:


    "但是它永远都会是我生活的一部分。"


translate chinese zhenya_route_e1e4cc09:


    "我在那里改变了许多。"


translate chinese zhenya_route_96ece0c6:


    "和在那里比起来，我人生仅剩的五六十年还要短得多，但是我一定要做一个大写的人！"


translate chinese zhenya_route_f8c0c4d9:


    mz "对啊，你可能不是什么好人，就像那个少先队员说的一样..."


translate chinese zhenya_route_41d7cf0b:


    "热妮娅睡着了还在嘀咕着。"


translate chinese zhenya_route_ad02f53a_1:


    me "什么?"


translate chinese zhenya_route_1bdc5a24:


    mz "没什么，睡觉吧!"


translate chinese zhenya_route_43560fef:


    "她一边笑着，一边掐了我一下。"


translate chinese zhenya_route_ce617998_24:


    "…"


translate chinese zhenya_route_5ca93524:


    "我很好奇别人在我的位置的话会有什么感觉?"


translate chinese zhenya_route_3882f569:


    "失去意识，焦虑，紧张?"


translate chinese zhenya_route_6cfbc782:


    "我回到自己过去的公寓，但是完全不认识了。"


translate chinese zhenya_route_b1bb61b3:


    "对啊，就像是回到自己很多年以前住过的屋子，所有的东西都在应该在的位置，但是你就是一点也不认识了，或者是觉得就应该是这样。"


translate chinese zhenya_route_2ed037cd:


    "我已经忘了我离开这个世界是哪天了，（我也不确定是哪一年了）但是窗外还是冬天，说明这里还没有经过很长时间。"


translate chinese zhenya_route_817af00a:


    "对啊，我不再是17岁了，但是我也不是我真实的年龄！"


translate chinese zhenya_route_e2e4abf4:


    "结果一切都好，没有什么意外?"


translate chinese zhenya_route_7703e5b3:


    "热妮娅..."


translate chinese zhenya_route_cab53a73:


    "我冲到桌子旁边，赶紧把电话号码和地址记下来。"


translate chinese zhenya_route_ae449981:


    "我当然不会忘掉的，但是有个备份总要好一些。"


translate chinese zhenya_route_827b177a:


    "好了，现在该怎么办?"


translate chinese zhenya_route_dee0474e:


    "这个世界到底是怎么运行的?"


translate chinese zhenya_route_d061d06a:


    "这里的法律完全不一样，我一点也不懂。"


translate chinese zhenya_route_5ac00794:


    "我就像是一个新生儿，但是却要过大人的生活!"


translate chinese zhenya_route_c294b867:


    "但是那个等等再说吧，现在，我得去找热妮娅！!"


translate chinese zhenya_route_103e53ea:


    "有人按响了门铃..."


translate chinese zhenya_route_8bd0d3e5:


    "人类。真正的人，不是少先队员演员..."


translate chinese zhenya_route_e9e7cdfa:


    "我得去开门，不然还能怎么样？我要过正常人的生活。"


translate chinese zhenya_route_c230ba61:


    "我慢慢的走到门口，插入钥匙，转动把手，然后愣住了。"


translate chinese zhenya_route_2f9f442b:


    mz "喂！你还想让我站多久？"


translate chinese zhenya_route_3b74cebc:


    "热妮娅..."


translate chinese zhenya_route_6b1df7af:


    "她看起来就跟在夏令营时一样。"


translate chinese zhenya_route_8a5df7ac:


    me "但是...怎么回事？?"


translate chinese zhenya_route_120e3d2a:


    mz "你记得我总是比你提前一个星期来到夏令营吗？这次我也提前一个星期回来了。"


translate chinese zhenya_route_2b81f411:


    "我忘记了小猫头鹰，忘记了真实世界，最重要的是，我们在一起了！"


translate chinese zhenya_route_85e2092d:


    "我拉起热妮娅朝屋里跑去。"


translate chinese zhenya_route_529e952d:


    mz "所以说咱们就要站在这里吗?"


translate chinese zhenya_route_7b813b45:


    me "啊，抱歉..."


translate chinese zhenya_route_c7fa01d1:


    "热妮娅轻松的跳上地板。"


translate chinese zhenya_route_0edeb144:


    mz "等等...咱们现在就要做，等一下可能没有时间了。"


translate chinese zhenya_route_7cfc6c8e:


    "她笑了笑，然后开始在包包里面翻找。"


translate chinese zhenya_route_cf917bd8:


    mz "伸手。"


translate chinese zhenya_route_d4a01b6d:


    "热妮娅在我的手指上放了一个戒指。"


translate chinese zhenya_route_9ede4597:


    "就跟在夏令营那时一样，很普通。"


translate chinese zhenya_route_6862cd26:


    "在昏暗的灯光下闪闪发光。"


translate chinese zhenya_route_5880720c:


    me "我应该买戒指来着..."


translate chinese zhenya_route_ee43c51d:


    mz "看看你这装修水平你好像没有那个经济能力啊。"


translate chinese zhenya_route_6d386883:


    "我们真诚的一起笑着。"


translate chinese zhenya_route_037f0871:


    "窗外飘着真正的雪花，白色的，晶莹的，黑色的夜晚被城市成千上万的灯光还有歌声覆盖，告诉我们还活着，还在一起。"


translate chinese zhenya_route_8e128b77:


    "我轻轻用手抚摸着热妮娅的脸颊。{w}温暖，真实..."


translate chinese zhenya_route_6abeee24:


    me "你这次不会消失了吧?"


translate chinese zhenya_route_871af259:


    mz "你最好相信！你还欠我戒指钱呢!"


translate chinese zhenya_route_04ae1cfa:


    "我抱紧热妮娅，从未如此热情的亲吻着她......（完）@Volgaski(o゜▽゜)o☆"
# Decompiled by unrpyc: https://github.com/CensoredUsername/unrpyc
