
translate chinese day5_main1_aa4d323e:


    "我们用尽了力气跑着..."


translate chinese day5_main1_11656716:


    "就像是在拯救自己宝贵的生命一样。"


translate chinese day5_main1_5edff0f6:


    "就像是一个即将毁灭的人，即使知道自己死路一条，仍旧为了自己的命运而战... "


translate chinese day5_main1_5db27d0a:


    "我用力的关上了身后的金属大门。"


translate chinese day5_main1_92ce41cd:


    th "我完全不知道这是在多深的地方，这个防空洞能不能经受住核弹的打击，不过我们没有其他的地方可以躲了..."


translate chinese day5_main1_4c9141de:


    "她紧紧的握着我的手。"


translate chinese day5_main1_6f240b82:


    me "别害怕..."


translate chinese day5_main1_81e0185f:


    "房顶不停的掉落着碎片，墙壁不住的颤抖。"


translate chinese day5_main1_43320cff:


    "我已经做好了最坏的打算。"


translate chinese day5_main1_eabbd6cf:


    "不过你是永远不可能准备好死亡的..."


translate chinese day5_main1_aceafbaa:


    "不过突然间，一片寂静，比爆炸还要响。"


translate chinese day5_main1_e7df2349:


    dreamgirl "也许是说再见的时候了..."


translate chinese day5_main1_1f90d83e:


    "她抽泣着。"


translate chinese day5_main1_8ba5cb54:


    "我想要安慰她，不过发现自己什么都做不了。"


translate chinese day5_main1_9513cd87:


    me "是啊..."


translate chinese day5_main1_b0692838:


    dreamgirl "知道，我..."


translate chinese day5_main1_337804c3:


    "一阵巨大的轰鸣声几乎撕破了我的鼓膜。"


translate chinese day5_main1_0ac01129:


    "我似乎在一片坠落的天花板下，但是并没有受伤。"


translate chinese day5_main1_02f7c13f:


    "我唯一想做的就是让她不要走..."


translate chinese day5_main1_893465aa:


    "我一身冷汗的醒过来，急促的喘着气。"


translate chinese day5_main1_8d045bac:


    "我用了很久才恢复了神智。"


translate chinese day5_main1_1d678e2c:


    me "是一个梦…{w}只是一个梦…"


translate chinese day5_main1_85574277:


    "然而我的大脑拒绝接受这个事实。"


translate chinese day5_main1_cba74cb0:


    th "刚刚和我一起的女孩是谁？"


translate chinese day5_main1_92ca5a14:


    "我死死的抓着她的手..."


translate chinese day5_main1_d2a550ca:


    "无奈的是我完全无法回忆起她来。"


translate chinese day5_main1_cfe2aec8:


    "表针指着十点过几分。"


translate chinese day5_main1_e1d34af1:


    "我逐渐恢复了意识——我的肚子强烈的抗议着。"


translate chinese day5_main1_4e2d25a2:


    me "好吧，战争就是战争，但是午饭要照常吃！"


translate chinese day5_main1_f33a92de:


    "原来奥尔加·德米特里耶夫娜不在这里，她一定是决定不叫我起床。"


translate chinese day5_main1_c0506319:


    th "呃，感谢伟大的辅导员！"


translate chinese day5_main1_d29a68d9:


    "经过了昨天的冒险我需要休息一下。"


translate chinese day5_main1_fc576f62:


    "昨天晚上模模糊糊的回忆我不想再记起来。"


translate chinese day5_main1_358f1320:


    "现在更重要的是吃饭...{w}还有洗漱！"


translate chinese day5_main1_16f93484:


    th "对了！"


translate chinese day5_main1_c02083da:


    th "因为一个少先队员必须保持仪表整洁。"


translate chinese day5_main1_15333fca:


    "即使不是少先队员我也会同意这个原则的（虽然说实际上我就不是）。"


translate chinese day5_main1_f50cd452:


    "前往水池的路上我遇到了电子小子。"


translate chinese day5_main1_7e4abd77:


    "他挥着手朝我跑过来。"


translate chinese day5_main1_5237cfbf:


    el "早上好！感谢你找到了舒里克！没有他我都不知道怎么办才好..."


translate chinese day5_main1_b97e3fd5:


    me "没什么..."


translate chinese day5_main1_dab051d6:


    "我有点不好意思。"


translate chinese day5_main1_611aba0e:


    el "真的，别不好意思，这个国家应该感谢她的英雄！"


translate chinese day5_main1_04210531:


    me "那么舒里克怎么样呢？"


translate chinese day5_main1_c4c5697f:


    "经过昨天的疯狂之后我觉得这样的问题是十分必要的。"


translate chinese day5_main1_805f0715:


    el "当然！不错！唯一的问题是他什么都不记得了..."


translate chinese day5_main1_859bc359:


    me "就这样？"


translate chinese day5_main1_2850cdb5:


    "我一点也不惊讶！"


translate chinese day5_main1_4819f710:


    el "他说他昨天去了旧营地，然后今天早上在他的床上醒了过来。{w}我是说他完全不记得两件事之间发生的事了。"


translate chinese day5_main1_5ec2624e:


    me "我明白了，那好吧…"


translate chinese day5_main1_caccdbd7:


    el "你错过了早饭吧？来我们的社团吧！我们需要你！我们有一些特殊的东西。"


translate chinese day5_main1_89ac8d3c:


    "电子小子很诡异的笑了笑。"


translate chinese day5_main1_94afa350:


    me "谢谢，我大概会去的..."


translate chinese day5_main1_7f29f4c2:


    "总之我得先洗洗脸。"


translate chinese day5_main1_c6f70531:


    el "我们等着你呦！"


translate chinese day5_main1_641f39b8:


    "他挥着手继续自己的活计了。"


translate chinese day5_main1_c64ccad8:


    "水池附近没有其他人。"


translate chinese day5_main1_127f580b:


    "令人惊讶的是，今天的水很暖和。"


translate chinese day5_main1_375200fb:


    th "应该是已经被加热过了吧..."


translate chinese day5_main1_b4c71998:


    "洗过脸之后我发现想要洗身上的其他部分不是太容易。"


translate chinese day5_main1_e7b1f5de:


    th "也许我应该去洗澡..."


translate chinese day5_main1_540ebe5d:


    th "不过既然这里没有别的人..."


translate chinese day5_main1_242cdd8f:


    "我拧动水龙头让水流平行地面流出，然后开始脱衣服。"


translate chinese day5_main1_8e855925:


    th "万一有人看到我怎么办？"


translate chinese day5_main1_6f67ff49:


    th "我就迅速洗完，穿上衣服..."


translate chinese day5_main1_315b7c24:


    "让我的手和脸感觉温暖的水，浇在身上时冰的要命。"


translate chinese day5_main1_a862e7c2:


    "整个洗澡的过程持续了不到十秒钟，我就开始擦干身上。"


translate chinese day5_main1_203544b1:


    "不过我还是没有成功的做到——小路上传来了人声！"


translate chinese day5_main1_8c7c1ae8:


    "我的头脑中闪现出唯一的办法——我抓起来衣服躲在草丛里。"


translate chinese day5_main1_374a9333:


    "过了一会儿阿丽夏和乌里扬娜出现在了水池前。"


translate chinese day5_main1_150b3ccc:


    dv "你自己就可以的，把我带过来干什么啊？"


translate chinese day5_main1_88bcfdf2:


    us "这有什么啊？"


translate chinese day5_main1_cc876a45:


    dv "好吧，让我…"


translate chinese day5_main1_657eb718:


    "我偷偷看着，发现她们都涂满了红漆。"


translate chinese day5_main1_2c5cbf0a:


    th "这怎么回事...我很奇怪她们是怎么做到的？"


translate chinese day5_main1_066f749f:


    "阿丽夏打开阀门开始给乌里扬娜擦身体。"


translate chinese day5_main1_fe4eae47:


    dv "把胸罩摘掉！"


translate chinese day5_main1_99ac44aa:


    us "万一有人看见怎么办...?"


translate chinese day5_main1_fc7e06ef:


    dv "什么，难道你那里有什么可看的吗？"


translate chinese day5_main1_473574eb:


    "她傻笑着。"


translate chinese day5_main1_6f871fea:


    us "好啦...快一点！"


translate chinese day5_main1_dd98f289:


    "确实没有什么可看的，不过我还是仔细的看着。"


translate chinese day5_main1_dcd32ae7:


    "遗憾的是她们背对着我。"


translate chinese day5_main1_89e896b4:


    "过了一会儿阿丽夏设法洗掉了所有的漆。"


translate chinese day5_main1_55cf29e5:


    dv "完了！"


translate chinese day5_main1_38773291:


    us "谢谢！"


translate chinese day5_main1_d53c1a0e:


    dv "不客气…"


translate chinese day5_main1_924b8a95:


    "阿丽夏懒洋洋的回答着。"


translate chinese day5_main1_c473bf91:


    us "听着，让我试试你的..."


translate chinese day5_main1_68b5468a:


    "她指着阿丽夏的胸罩。"


translate chinese day5_main1_82a80785:


    dv "肯定不会适合你的…"


translate chinese day5_main1_f348d116:


    us "哼，反正我要试一试…"


translate chinese day5_main1_19b2ba93:


    dv "不过那里…"


translate chinese day5_main1_ba15cbf1:


    us "那里没有人吧？"


translate chinese day5_main1_b8352a08:


    "乌里扬娜向我这边狡猾的看着。"


translate chinese day5_main1_eba695a3:


    "我很确定隔着灌木丛她肯定看不见我…"


translate chinese day5_main1_fe7431ac:


    dv "虽然说这…"


translate chinese day5_main1_3bd56665:


    "乌里扬卡没有再听，而是熟练地解开了阿丽夏的胸罩。"


translate chinese day5_main1_cc3da604:


    "好吧，这下我得承认，我有可看的了！"


translate chinese day5_main1_7f273933:


    "我屏住呼吸看着两个女孩子在水池前追来追去。"


translate chinese day5_main1_e09372ff:


    "阿丽夏用手遮住了胸部让我看不见什么。"


translate chinese day5_main1_d0252e13:


    "我向前靠了一些，绊倒在一块石头上，从灌木丛中滚了出来…"


translate chinese day5_main1_2341b367:


    "阿丽夏和乌里扬娜愣住了，看着我。"


translate chinese day5_main1_ef3d3ea6:


    "我惭愧的试图遮住自己。"


translate chinese day5_main1_11a555c2:


    "这幅画面持续了几秒钟，然后阿丽夏迅速拿起了自己的上衣穿了上去。"


translate chinese day5_main1_b7d8bf72:


    dv "你！你！"


translate chinese day5_main1_ec010716:


    "她的脸从红变紫。{w}看起来她可以在任何时候发生爆炸。"


translate chinese day5_main1_4d760247:


    "我现在只想分解成原子然后离开震中越远越好！"


translate chinese day5_main1_97e02b14:


    us "他一直都坐在那里！"


translate chinese day5_main1_470f0c0e:


    th "所以说她看到我了…"


translate chinese day5_main1_b7d8bf72_1:


    dv "你！你！"


translate chinese day5_main1_3a1dcf49:


    me "我...那个...不小心...你懂的..."


translate chinese day5_main1_a9f31aea:


    "阿丽夏向我冲了过来。"


translate chinese day5_main1_4ffa3601:


    "我一手捂着屁股一手拿着衣服跑进了树林。"


translate chinese day5_main1_9ba510d2:


    "看起来我当时的唯一办法不太合适…"


translate chinese day5_main1_20ff3f6f:


    "我头也不回的跑着。"


translate chinese day5_main1_e7dc840b:


    "然后停下几秒钟喘口气。"


translate chinese day5_main1_0dab6283:


    "似乎她没有在追了。"


translate chinese day5_main1_544faf65:


    th "所以我成功的逃了出来！"


translate chinese day5_main1_49bd14a5:


    "不过是建立在全身划伤，脚划出血的代价上的——我跑的时候来不及穿上鞋。"


translate chinese day5_main1_244a0a02:


    "我坐在一个树桩上叹着气…"


translate chinese day5_main1_ada2f8dd:


    "过了一会儿，我穿好了衣服，离开了森林。"


translate chinese day5_main1_01337998:


    th "我需要决定接下来做什么！"


translate chinese day5_main1_0004b659:


    th "我的脚在流血，所以我应该直接去医务室！"


translate chinese day5_main1_21e25d14:


    th "不过另一方面，我的肚子也等不及了。"


translate chinese day5_main1_bc7d5816:


    th "也许我可以接受电子小子的邀请？"


translate chinese day5_main1_ee8b56d6:


    th "或者去食堂也许能侥幸找到一些吃的...?"


translate chinese day5_dining_hall_c113591d:


    "我总是注意自己的健康。{w}忍无可忍的情况下更是如此。"


translate chinese day5_dining_hall_5b254b74:


    "现在我可以走了，我的脚也不再那么疼。"


translate chinese day5_dining_hall_1068ff00:


    "所以我的脚好了，然而森林里的狼因为饥饿可能会出来。"


translate chinese day5_dining_hall_29600c1e:


    th "少先队员们肯定不会吃光所有东西的！"


translate chinese day5_dining_hall_16cc44b9:


    th "至少会有一些鸡蛋，香肠，最差的情况也应该还剩下一些面包吧！"


translate chinese day5_dining_hall_daf1231e:


    "食堂周围出奇的寂静让我犹豫了一会儿。"


translate chinese day5_dining_hall_d065f713:


    "这难道不是少先队员们一天三次追求幸福的地方吗，难道不是这个夏天的沙漠中的绿洲吗，难道不是一个测试各种未知的事物对未成年人影响的化学实验室吗？"


translate chinese day5_dining_hall_01ef2d8a:


    "现在这个建筑更像是一座被守军遗弃的堡垒，被胡格诺教徒抛弃的拉罗谢尔。"


translate chinese day5_dining_hall_d00d888a:


    "只要走进来，武士的灵魂就会把你包围…"


translate chinese day5_dining_hall_ff4b2359:


    "食堂本身看起来和平常并没有什么不同。"


translate chinese day5_dining_hall_59713482:


    "几乎是空空如也，除了…{w}未来，正在打扫桌子。"


translate chinese day5_dining_hall_c404fc22:


    "看到之后我马上转身打算悄悄溜走，不过我没有成功…"


translate chinese day5_dining_hall_44d5aaf7:


    mi "嗨，谢苗！你来这里吃饭了吗？你没有吃早饭吧！我是说，我没有看见你。也许是你来了但是我没有看见你。不过你来了就好！"


translate chinese day5_dining_hall_479c6b10:


    me "嗯，嗨…{w}呃，是啊，我…不知道还有没有吃的剩下…"


translate chinese day5_dining_hall_a44fc8b3:


    mi "什么都没有了！你得等午饭了！你，不来帮帮吗，我还得做卫生…"


translate chinese day5_dining_hall_810f39c8:


    me "为什么啊？"


translate chinese day5_dining_hall_780b4277:


    mi "你是什么意思？"


translate chinese day5_dining_hall_d946b833:


    "她撅起嘴好像生气了。"


translate chinese day5_dining_hall_d0d699dd:


    mi "总要有人做啊！总会轮到你的！"


translate chinese day5_dining_hall_c0778825:


    th "谢谢，但是算了…"


translate chinese day5_dining_hall_6933143e:


    me "好吧，我明白了…"


translate chinese day5_dining_hall_9b0aa1ca:


    "我准备离开，不过未来好像停不下来。"


translate chinese day5_dining_hall_c78b2cc2:


    mi "所以说你会不会帮我？"


translate chinese day5_dining_hall_a4480274:


    "我不知道为什么就同意了。"


translate chinese day5_dining_hall_79c948da:


    "这种事总是发生，不经意间脱口而出之后又会用很长时间反思当时为什么那么做。"


translate chinese day5_dining_hall_2f281a85:


    "你想来想去…{w}就是总不清楚自己为什么那么做了。"


translate chinese day5_dining_hall_d93feb70:


    "这是我擦桌子一张接一张的时候心里想的事。"


translate chinese day5_dining_hall_68f21c4b:


    mi "话说，我又写了一首新歌，你要不要听一听？"


translate chinese day5_dining_hall_209b83b3:


    th "完全没有兴趣。"


translate chinese day5_dining_hall_0270e5da:


    mi "不对…"


translate chinese day5_dining_hall_60f3d758:


    "她陷入思考。"


translate chinese day5_dining_hall_85b5c92a:


    mi "一边做卫生一边唱歌有些困难...我过一会儿再唱！"


translate chinese day5_dining_hall_3aeb2d0f:


    "未来露出了释然的微笑。"


translate chinese day5_dining_hall_3b03af3a:


    me "是啊，当然..."


translate chinese day5_dining_hall_6eaf9733:


    mi "昨天你救了舒里克好厉害啊！今天一大早整个夏令营都在讨论这件事情！"


translate chinese day5_dining_hall_992a60d0:


    th "我感觉就像是一个英雄一样…"


translate chinese day5_dining_hall_668b75c5:


    me "其实没有什么。"


translate chinese day5_dining_hall_57cc9875:


    mi "我是说真的！我半夜可不敢走进黑暗的森林…去旧营地，你听说过那个传说吗？一个辅导员在那里开枪自杀…"


translate chinese day5_dining_hall_3bb17a2c:


    th "之前她们说是上吊自杀..."


translate chinese day5_dining_hall_52e843c6:


    mi "总的来说实在是太可怕了！"


translate chinese day5_dining_hall_b22bc996:


    me "是啊，大概是这样的。"


translate chinese day5_dining_hall_f15c1e3c:


    "我试图集中注意力打扫卫生。"


translate chinese day5_dining_hall_235ecfcf:


    "这让我比预期的更早的结束了工作。"


translate chinese day5_dining_hall_7a81d8e3:


    me "现在结束了！"


translate chinese day5_dining_hall_07ea95a8:


    mi "谢谢!"


translate chinese day5_dining_hall_25fab848:


    "距离午饭还有很长时间，所以我决定散散步。"


translate chinese day5_dining_hall_90a17a70:


    mi "好吧…"


translate chinese day5_dining_hall_b2673fad:


    "看起来我让未来失望了。"


translate chinese day5_dining_hall_f3c8b2f3:


    th "但是有什么办法呢，这就是生活…"


translate chinese day5_dining_hall_9d2e50ce:


    "离开食堂以后我坐在门口的长椅上叹着气。"


translate chinese day5_dining_hall_7e6cef9d:


    "我的脚还有些疼，虽然比之前要好一些，但是我的肚子里还是什么都没有…"


translate chinese day5_dining_hall_29ebce26:


    "距离午饭还有很长时间，所以我决定散散步。"


translate chinese day5_dining_hall_549330fd:


    "我随便找了一个方向——大概可以说是“前方”。"


translate chinese day5_dining_hall_8983ed56:


    "结果我发现自己来到了广场。"


translate chinese day5_dining_hall_283be7ae:


    "不难想到Genda纪念碑是整个营地的中心，也是一个零公里里程碑…"


translate chinese day5_dining_hall_356059a5:


    "我坐在长椅上开始思考。"


translate chinese day5_dining_hall_85bc341b:


    th "已经过去了四天，但是我还对离开这里的方法没有一点头绪。"


translate chinese day5_dining_hall_37f3c603:


    th "这段时间确实发生了一些灵异的事件，不过仔细想想的话全都可以用逻辑解释清楚。"


translate chinese day5_dining_hall_9cc8af14:


    th "每一件事情拿出来放到日常生活中都说得过去…"


translate chinese day5_dining_hall_ee9a7a1a:


    th "日常生活，这个词语放在这里失去了原本的意义。"


translate chinese day5_dining_hall_e5526803:


    th "这里的环境，人们的行为，他们说的话，没有一点是正常的。"


translate chinese day5_dining_hall_66dcd718:


    th "过去的四天里我的世界观受到重击，先是在肚子上，然后是一记上勾拳不是把我撂倒就是让我崩溃。"


translate chinese day5_dining_hall_7b0e3b31:


    th "有时候我不明白自己为什么那样做那样说…"


translate chinese day5_dining_hall_19f2dbbc:


    th "实际上，我明白，但是不是直接的。"


translate chinese day5_dining_hall_ac9be890:


    th "这种事后的思考，并不能帮助我改变自己的行动，变得更加正常或者是更加适合情境。"


translate chinese day5_dining_hall_bc9e662f:


    th "发生在我身上的真实越来越少了。"


translate chinese day5_dining_hall_85fdbea0:


    th "如果我第一天到这里时唯一想要的是如何离开这里的话，现在我想的只是哪里能弄到吃的，怎样翘掉早晨的集合训话还有阿丽夏抱怨我的什么的话怎么和辅导员解释…"


translate chinese day5_dining_hall_f2b21e4a:


    th "这些事情对于我都很重要！"


translate chinese day5_dining_hall_770314f2:


    th "一天一天，这样的日常琐事让我对这个世界和女孩子什么的思考变得越来越黯然失色。"


translate chinese day5_dining_hall_4ba0ec94:


    th "但是我什么也做不了，因为我只是忘记…"


translate chinese day5_dining_hall_5c46142a:


    th "就像是我们意识不到自己在呼吸一样，我每一天都更多的加入这里人们的日常生活中而且还意识不到。"


translate chinese day5_dining_hall_95d43b05:


    th "我已经准备好做一个普通的少先队员了…"


translate chinese day5_dining_hall_6614ca63:


    me "不!{w}这不对!"


translate chinese day5_dining_hall_216fd396:


    "我大声喊着，抽打着自己的脸。"


translate chinese day5_dining_hall_8d33c464:


    "突然扩音器中的铃声响起，召集少先队员们去吃午饭。"


translate chinese day5_dining_hall_f23afdfa:


    me "终于!"


translate chinese day5_dining_hall_436b6b08:


    "我扔下自己刚刚只能让Genda感兴趣的想法，蹦蹦跳跳的跑向食堂…"


translate chinese day5_aidpost_c1caf0d4:


    "我总是注意自己的健康。{w}疼的时候更是如此。"


translate chinese day5_aidpost_b92e3272:


    "所以我毫不犹豫的走向了医务室。"


translate chinese day5_aidpost_6e2d6c49:


    "但是就在大门前我停了下来，好像有什么力量阻止着我。"


translate chinese day5_aidpost_f49a6790:


    "我没有紧急需要的时候绝对不想见到护士同志，不过说起来我昨天可是对医务室做了很大贡献啊。"


translate chinese day5_aidpost_9cb3955d:


    "S所以从某种程度上说她欠我一个人情…"


translate chinese day5_aidpost_41de82b8:


    "我打开了门。"


translate chinese day5_aidpost_8e557000:


    cs "你好啊，少先队员！"


translate chinese day5_aidpost_bb4c809d:


    me "你好..."


translate chinese day5_aidpost_0f095ac2:


    cs "很抱歉我昨天回来的太晚了…"


translate chinese day5_aidpost_fa9efa41:


    me "没有关系…"


translate chinese day5_aidpost_e2736162:


    cs "你怎么来了？{w}你生病了吗？"


translate chinese day5_aidpost_945c59bc:


    "她狡猾的微笑着。"


translate chinese day5_aidpost_f6d4c30b:


    me "呃，这…{w}我的脚有些…"


translate chinese day5_aidpost_a10407c0:


    cs "有些什么？"


translate chinese day5_aidpost_3f4d5019:


    me "脚。"


translate chinese day5_aidpost_be55d6e6:


    "我做了一个愚蠢的回答。"


translate chinese day5_aidpost_ee52ce7f:


    cs "好吧，给我看看咯。"


translate chinese day5_aidpost_ebe6fcf2:


    "我坐在沙发上，脱掉自己的靴子。"


translate chinese day5_aidpost_387cc55b:


    cs "你是怎么做到的？"


translate chinese day5_aidpost_97c64dc4:


    "我最好对于水池发生的事情保持沉默。"


translate chinese day5_aidpost_2fcc5991:


    me "这…"


translate chinese day5_aidpost_db1556e6:


    cs "没关系{w}等一下…"


translate chinese day5_aidpost_37b10e56:


    "她从抽屉中找到一个小药瓶，拿出一个刻着十字的药丸。"


translate chinese day5_aidpost_7603637e:


    me "这是做什么的？"


translate chinese day5_aidpost_8f1a1f4c:


    "我被药丸的大小和形状惊到了，一般都是分成两半的而不是四瓣。"


translate chinese day5_aidpost_8040e280:


    cs "这是为了让你的腿不必截肢！"


translate chinese day5_aidpost_3d342d5a:


    me "啥?!"


translate chinese day5_aidpost_91d22182:


    cs "没什么，不要害怕…{w} 少先队员!{w}吃掉它你就会好起来。"


translate chinese day5_aidpost_9fe20b07:


    me "这个药片…{w}为什么是这个样子的？"


translate chinese day5_aidpost_7e2d8a95:


    cs "这是给不想口服的病人准备的。{w}我们用改锥拧进去。"


translate chinese day5_aidpost_7ccd5058:


    "我没敢问她是在哪里拧的。"


translate chinese day5_aidpost_c2372b85:


    cs "现在试着忍一下。"


translate chinese day5_aidpost_5c6ac9cb:


    "她拿出一团药棉，蘸上碘液。"


translate chinese day5_aidpost_ad181873:


    "我准备迎接地狱的刑罚…"


translate chinese day5_aidpost_4b4491c1:


    me "呜啊啊啊啊啊啊啊…"


translate chinese day5_aidpost_1e0476d1:


    "我低声呻吟着。"


translate chinese day5_aidpost_ad1b9cb0:


    "不过我对于这种方法杀菌效果的信任比那个奇怪的药丸高得多，所以我只能忍痛了。"


translate chinese day5_aidpost_4e9c1b77:


    cs "全部完成了！"


translate chinese day5_aidpost_4eb5e7c1:


    "我穿上靴子试图走动。"


translate chinese day5_aidpost_920fc608:


    "我的脚还在疼，不过那种撕心裂肺的疼痛已经过去了。"


translate chinese day5_aidpost_61477e60:


    me "谢谢！"


translate chinese day5_aidpost_f328eb53:


    cs "不客气！随时来...拜访哦！"


translate chinese day5_aidpost_40d35bf5:


    "我说了再见，离开了医务室。"


translate chinese day5_aidpost_c4d64e48:


    "我的健康问题解决了！"


translate chinese day5_aidpost_132ab340:


    th "现在是吃饭时间了！"


translate chinese day5_clubs_6f918f97:


    "我已经饿得不行了。"


translate chinese day5_clubs_c113591d:


    "我总是注意自己的健康。{w}疼的时候更是如此。"


translate chinese day5_clubs_f98258e8:


    "现在我可以走了，我的脚也不再那么疼。"


translate chinese day5_clubs_ff3100c2:


    th "我的脚会逐渐好起来，不过我的肚子可是等不及了…"


translate chinese day5_clubs_52edf477:


    "我决定去蹭电子小子。"


translate chinese day5_clubs_13624c4d:


    "我了解这些少先队员们，食堂里一块腐烂的面包都不会剩下的…"


translate chinese day5_clubs_2035d522:


    th "而且机器人社团总是某种程度上欠我的。"


translate chinese day5_clubs_c6f1a614:


    "我抱着这种想法来到了社团大楼。"


translate chinese day5_clubs_e8ed57a1:


    "然后听到里面传来尖叫的声音。"


translate chinese day5_clubs_d8f147f1:


    "我尝试仔细听，可是什么也没有听出来。"


translate chinese day5_clubs_5b5a4f32:


    sh "给我！"


translate chinese day5_clubs_6c5410b6:


    us "不！"


translate chinese day5_clubs_d25dd8ba:


    "乌里扬卡拿着一个线圈在屋里跑来跑去，后面追着电子小子。"


translate chinese day5_clubs_12e6be7b:


    me "伙计们，很抱歉打扰你们，不过…"


translate chinese day5_clubs_5b5a4f32_1:


    sh "给我！"


translate chinese day5_clubs_1217af9c:


    "他们一点都没有注意我。"


translate chinese day5_clubs_6c5410b6_1:


    us "不给！"


translate chinese day5_clubs_87a4498e:


    "舒里克疯狂的追逐着，在我旁边不小心摔倒了，差点把我撞倒。"


translate chinese day5_clubs_95efcfe1:


    me "嘿!"


translate chinese day5_clubs_37ace03e:


    "乌里扬卡欢快的在屋里绕着圈。"


translate chinese day5_clubs_ec25da0f:


    th "我很奇怪她要这个电线有什么用？"


translate chinese day5_clubs_81589db2:


    "同时，社团领导人在经历了昨天的疯狂之后看起来甚至更加平静了。{w}甚至可以是神清气爽。"


translate chinese day5_clubs_2ddd0bed:


    "不过这对于他抓住乌里扬卡倒是没有什么帮助，她又小又灵活，而且还跑得快。"


translate chinese day5_clubs_075ee6b7:


    "她可以很容易的把他绕晕…"


translate chinese day5_clubs_95efcfe1_1:


    me "嘿!"


translate chinese day5_clubs_0b5f4462:


    "乌里扬卡向我跑过来，躲在我后面。"


translate chinese day5_clubs_692dfc67:


    "舒里克离开了房间，摔门而去。"


translate chinese day5_clubs_8c929878:


    th "他好像怒了…"


translate chinese day5_clubs_25606905:


    me "嘿，你怎么回事？！"


translate chinese day5_clubs_b73a1dab:


    us "你抓不到我！"


translate chinese day5_clubs_575bed8e:


    "她看着电子小子，还吐着舌头，他正在观察着这个脑残的游戏。"


translate chinese day5_clubs_67bf1dec:


    el "谢苗，从她手里抢走电线！"


translate chinese day5_clubs_3cfac947:


    me "不过你为什么需要那个电线？难道你们没有足够的吗？"


translate chinese day5_clubs_65fffe51:


    "她试图抓住躲在我后面的乌里扬卡。"


translate chinese day5_clubs_3a233083:


    "他往左，她往右，他往右，她往左…"


translate chinese day5_clubs_db785e8c:


    "最后我厌烦了，巧妙的从乌里扬卡手中顺走了电线。"


translate chinese day5_clubs_7a0f81d3:


    us "还给我！还给我！"


translate chinese day5_clubs_25ebf834:


    "她愤怒的喊着。"


translate chinese day5_clubs_bb517af5:


    me "不，我不会给的！你不要捣乱了！"


translate chinese day5_clubs_aac11710:


    "我把电线举过头顶，所以以乌里扬卡的身高完全摸不到。"


translate chinese day5_clubs_d2db29cb:


    el "谢谢！"


translate chinese day5_clubs_44f47ba7:


    us "好吧！"


translate chinese day5_clubs_99aa653e:


    "她抽着鼻子走开了。"


translate chinese day5_clubs_66d3549f:


    me "你原本为什么需要这个？"


translate chinese day5_clubs_7d1c8c94:


    us "那和你没有关系…"


translate chinese day5_clubs_c68d93e1:


    "乌里扬卡狡猾的看着我。"


translate chinese day5_clubs_88702a1e:


    us "你想让我告诉大家…"


translate chinese day5_clubs_960701df:


    "我用手堵住了她的嘴然后把她拽向了出口。"


translate chinese day5_clubs_d0245af1:


    me "好了，咱们得走了…"


translate chinese day5_clubs_c000d8e7:


    "我傻笑着对电子小子说。"


translate chinese day5_clubs_4d17bd21:


    el "那么你来这里是为了什么？"


translate chinese day5_clubs_7525ab2b:


    "一走到外面，我就放开了不停踢着的乌里扬卡。"


translate chinese day5_clubs_1828214b:


    me "你听好，你要知道那是一个意外！...而且即使如此，那也是因为你！"


translate chinese day5_clubs_86dba4fb:


    us "我什么也不知道！{w}事实就是事实：你当时在偷看我们！"


translate chinese day5_clubs_d226b66c:


    us "啊哈哈哈!{w}奥尔加·德米特里耶夫娜会怎么想…"


translate chinese day5_clubs_df680674:


    "我一方面根本不在乎辅导员怎么想，不过另一方面事情对我太不利了，我不应该让自己陷入那样的困境。"


translate chinese day5_clubs_97ad7b66:


    me "好吧，也许咱们可以做一个交易…"


translate chinese day5_clubs_6532261a:


    us "唔嗯..."


translate chinese day5_clubs_60f3d758:


    "她开始思考。"


translate chinese day5_clubs_9c1e8275:


    us "我知道！"


translate chinese day5_clubs_747f9ddc:


    "我预期到了最坏的情况。"


translate chinese day5_clubs_e97a33bb:


    us "你把电线带给我！"


translate chinese day5_clubs_47728b3d:


    me "但是你要它做什么？"


translate chinese day5_clubs_fc2357ec:


    us "我有自己的需要！"


translate chinese day5_clubs_2fca2e42:


    me "好吧，假设我弄到了…{w}你不会告诉别人吧？"


translate chinese day5_clubs_6f158bac:


    us "我以少先队员向你保证！"


translate chinese day5_clubs_85b6f436:


    "我不太相信她。"


translate chinese day5_clubs_88068e4d:


    "不过反过来说，只是一个线圈嘛，试一试又何妨？"


translate chinese day5_clubs_91dd4d67:


    me "那好吧…"


translate chinese day5_clubs_a64a24e7:


    us "成交!"


translate chinese day5_clubs_fee7bf01:


    me "不过你一定要知道…"


translate chinese day5_clubs_f85583fb:


    us "是是是，当然！"


translate chinese day5_clubs_0d425fdb:


    "她打断了我。"


translate chinese day5_clubs_b2bf8ea2:


    me "在这里等着我。"


translate chinese day5_clubs_a4e889e3:


    "我说着回到了社团活动室。"


translate chinese day5_clubs_0af71d51:


    me "不，没有电线给你，已经够了！"


translate chinese day5_clubs_1d6fcf81:


    us "那我就会把一切说出来！"


translate chinese day5_clubs_5da9850d:


    me "反正你也会说出来！或者阿丽夏…"


translate chinese day5_clubs_80eb9adc:


    us "啊啊，走开！"


translate chinese day5_clubs_6d0fed3e:


    me "啊，是，好像是我的错一样…"


translate chinese day5_clubs_a18bb399:


    us "就是，你在看着我们。"


translate chinese day5_clubs_69a83100:


    me "没有，我没看！"


translate chinese day5_clubs_2719b638:


    "不过不管你怎么说，我确实是看了…"


translate chinese day5_clubs_76442aba:


    us "阿丽夏不这么想。"


translate chinese day5_clubs_bdc70299:


    me "她有很多事情和我观点不同。"


translate chinese day5_clubs_d927617d:


    us "而且奥尔加·德米特里耶夫娜也不会高兴的。"


translate chinese day5_clubs_a36b990a:


    me "你知道吗？！"


translate chinese day5_clubs_6ea78a01:


    "我开始失去耐心了。"


translate chinese day5_clubs_07f22fdf:


    me "如果你想说的话，现在就去说吧！都是我的错！什么都是我的错！全球变暖，经济危机，全都是我的错！"


translate chinese day5_clubs_7843e56a:


    us "哇，别冲动，我只是...开玩笑啦。"


translate chinese day5_clubs_ba72b31c:


    me "开玩笑...?"


translate chinese day5_clubs_578ce4ba:


    "我突然意识到我有些过分了。"


translate chinese day5_clubs_07630dae:


    me "你的笑话太愚蠢！我每次都得干什么——猜测你的话是不是笑话？"


translate chinese day5_clubs_df6dbf74:


    us "是啊。"


translate chinese day5_clubs_fde537c1:


    "她傻笑着。"


translate chinese day5_clubs_0f1412bc:


    us "那样更加有趣。"


translate chinese day5_clubs_eac89d90:


    "乌里扬卡转过身跑向广场，一边挥着手道别。"


translate chinese day5_clubs_beff659f:


    th "反正没有办法和她和谐相处…"


translate chinese day5_clubs_01a2b4fe:


    "我叹着气回到了机器人社团。"


translate chinese day5_clubs_ee53a93e:


    "电子小子正在全神贯注的只做什么东西。"


translate chinese day5_clubs_6768144c:


    me "我现在完事了，你不是保证过给我什么…"


translate chinese day5_clubs_72b8e1d5:


    "我的声音有一点谄媚的感觉，让我很不爽。"


translate chinese day5_clubs_cda5bf38:


    me "当然我没有要求必须如此，不过…"


translate chinese day5_clubs_73232f80:


    el "马上。"


translate chinese day5_clubs_819d22c9:


    "他离开工作，到抽屉里拿出了一个小面包还有一个三角形的酸牛奶。"


translate chinese day5_clubs_10c6e757:


    el "请。"


translate chinese day5_clubs_73aab3cd:


    me "感谢…"


translate chinese day5_clubs_c321210e:


    "我正忙着吃的时候，电子小子毫不分心的专注在自己的机器上。"


translate chinese day5_clubs_88d66003:


    "他正在向我从乌里扬卡那里拿回来的线上缠线圈。"


translate chinese day5_clubs_c90aa8d9:


    th "她想要的是电线！"


translate chinese day5_clubs_b12c3bd0:


    me "这是什么？"


translate chinese day5_clubs_ce6e33a1:


    el "电感线圈。"


translate chinese day5_clubs_01d8c6fc:


    me "电关系安全？"


translate chinese day5_clubs_a1512ad1:


    el "加入我们的社团，你都会学会的！"


translate chinese day5_clubs_99c684ef:


    "他看着我猥琐的笑着。"


translate chinese day5_clubs_232d02c3:


    me "我会想想的…"


translate chinese day5_clubs_8e38bd64:


    "当然我是不会加入的，不过看在他给我吃的份上客气一下。"


translate chinese day5_clubs_0560d317:


    el "顺便说，我还有别的东西…"


translate chinese day5_clubs_f39fd8a3:


    me "那是…"


translate chinese day5_clubs_73232f80_1:


    el "等一下。"


translate chinese day5_clubs_ebaab1b0:


    "他走进旁边的屋子里拿出来一个小包裹。"


translate chinese day5_clubs_1dfcdcd0:


    el "铛铛！"


translate chinese day5_clubs_6ba4e579:


    "然后递给我。"


translate chinese day5_clubs_4b293ede:


    "里面有一大瓶“红牌伏特加”。"


translate chinese day5_clubs_4e5a25f1:


    me "啊，我明白了，不过还是早晨…"


translate chinese day5_clubs_7cc7ef1a:


    th "或者是电子小子有那种“早晨喝醉，全天放假”的座右铭？"


translate chinese day5_clubs_9479eb17:


    el "你在说什么？{w}我不是说喝掉它，我是说要清洗镜片！"


translate chinese day5_clubs_5e972698:


    th "清洗镜片，啊…{w}里面…"


translate chinese day5_clubs_f2e007f0:


    me "好…"


translate chinese day5_clubs_9991da5b:


    "我把包裹还给他。"


translate chinese day5_clubs_a5f87a00:


    me "对了，你可以借我用一下这个线圈吗？"


translate chinese day5_clubs_ce0d2801:


    el "你用这个做什么？"


translate chinese day5_clubs_c97fadc0:


    me "那个…"


translate chinese day5_clubs_d6323f95:


    "也许我事先应该想好理由..."


translate chinese day5_clubs_98fe77ae:


    me "就是需要嘛。"


translate chinese day5_clubs_09b8008a:


    el "不过没有它不行啊。"


translate chinese day5_clubs_68f1c3d2:


    me "真的吗？那好吧..."


translate chinese day5_clubs_ba73ed1f:


    th "我不能强迫他给我吧？"


translate chinese day5_clubs_cd9f16d5:


    me "那我走了…"


translate chinese day5_clubs_642cd44e:


    el "随时欢迎你！"


translate chinese day5_clubs_bdd3ba78:


    me "当然…"


translate chinese day5_clubs_c5676147:


    th "我想喝…"


translate chinese day5_clubs_212a81b3:


    "出来以后我很疑惑他为什么要给我那瓶酒…"


translate chinese day5_clubs_3c6ad981:


    "乌里扬卡马上跑了过来。"


translate chinese day5_clubs_1fb33640:


    us "然后呢？有没有弄来？"


translate chinese day5_clubs_1eaad051:


    me "没..."


translate chinese day5_clubs_07a5780c:


    us "那是你自己的问题！"


translate chinese day5_clubs_03515da3:


    me "你真的需要这个线圈吗...?"


translate chinese day5_clubs_b934057e:


    us "不过试试总是有好处的嘛！"


translate chinese day5_clubs_ad02f53a:


    me "啥?"


translate chinese day5_clubs_d8362ad8:


    "我很吃惊。"


translate chinese day5_clubs_a10e2b46:


    us "阿丽夏都会说出来的！"


translate chinese day5_clubs_a4c6b91e:


    "乌里扬卡笑着。"


translate chinese day5_clubs_d1d0d845:


    us "她很生气。"


translate chinese day5_clubs_078810d4:


    me "那是…"


translate chinese day5_clubs_3e1d421a:


    "我自言自语。"


translate chinese day5_clubs_fa77eb7d:


    us "好了，再见。"


translate chinese day5_clubs_3861dc96:


    "她挥挥手跑开了。"


translate chinese day5_clubs_50e6fe96:


    th "好了，我又处在尴尬的境地。"


translate chinese day5_clubs_5bd8bf7d:


    th "不是第一次…"


translate chinese day5_clubs_bedd20be:


    "突然我的肚子又叫了起来…"


translate chinese day5_clubs_9a611cea:


    "小面包和酸奶的加餐当然是不错，不过还不够填满我的肚子。"


translate chinese day5_clubs_8b9210c2:


    "幸运的是我听到了铃响，召集少先队员们吃饭了。"


translate chinese day5_main2_c9f98d7e:


    th "新的一天才刚刚开始，而我已经经历了这么多挫折…"


translate chinese day5_main2_9dff93e1:


    th "不过我做到了，现在我有充足的理由吃饱饭！"


translate chinese day5_main2_b855c58c:


    "我今天不是最后一个，所以可以自由选择自己的位置。"


translate chinese day5_main2_5a8c068e:


    "午饭有豌豆汤，土豆泥还有鱼。"


translate chinese day5_main2_e5fed9f2:


    "不过让人失望的是我不吃鱼，所以能吃到的能量可能要少一些。"


translate chinese day5_main2_9a2a6cb4:


    "很快斯拉维娅和列娜来到了我的桌子。"


translate chinese day5_main2_99c96681:


    sl "可以吗？"


translate chinese day5_main2_b7bd4e35:


    "她友好的笑着。"


translate chinese day5_main2_bcdef80b:


    me "诶?{w}当然！"


translate chinese day5_main2_566b45f6:


    "我站起来给她拽过来一把椅子。"


translate chinese day5_main2_7b602452:


    me "请坐!"


translate chinese day5_main2_cafd2e9e:


    "我现在心情很好。"


translate chinese day5_main2_538f35f4:


    un "开动咯…"


translate chinese day5_main2_62d27eaf:


    "然后列娜开始盯着我，而且持续了好一段时间，不过在她发现自己的样子之后又把注意力放回了盘子。"


translate chinese day5_main2_3c5ee540:


    me "我开动啦。"


translate chinese day5_main2_952025ed:


    sl "你今天有什么计划吗？"


translate chinese day5_main2_5a7a7577:


    me "没。"


translate chinese day5_main2_c0d912bb:


    "我诚实的回答着，因为我真的没有什么计划。{w}除了“寻找答案”，不过那个是既定政策。"


translate chinese day5_main2_8df8d875:


    sl "你想和我们一起划船到岛上去吗？"


translate chinese day5_main2_c16c4c12:


    th "岛上...{w}嗯，我好像从码头看到过。"


translate chinese day5_main2_7332342b:


    me "为什么？"


translate chinese day5_main2_ea29c6fe:


    sl "奥尔加·德米特里耶夫娜让我们去采摘草莓，岛上有很多，而且非常美味！"


translate chinese day5_main2_679ff36d:


    "我看到斯拉维娅的表情就知道肯定非常好吃。"


translate chinese day5_main2_f29bf018:


    me "草莓...{w}摘草莓干什么呢？"


translate chinese day5_main2_2abd657f:


    sl "我也不知道，不过这是个好主意！"


translate chinese day5_main2_5ccbf2e6:


    th "嗯，是啊。{w}而且我还没有去过岛上。"


translate chinese day5_main2_1066065f:


    me "好吧，当然。"


translate chinese day5_main2_a20cefa7:


    "..."


translate chinese day5_main2_f2b9563d:


    "没过几分钟，我们就已经来到了码头。"


translate chinese day5_main2_a8b1bc59:


    sl "好了，船在这里。{w}等一下，我去找几只桨。"


translate chinese day5_main2_86c42a71:


    "只剩下我和列娜面面相觑。"


translate chinese day5_main2_1745ad04:


    me "你喜欢草莓吗？"


translate chinese day5_main2_3898ad76:


    un "嗯...不太喜欢，不过很好吃。"


translate chinese day5_main2_561828ec:


    "列娜笑着。"


translate chinese day5_main2_f2e007f0:


    me "这样啊..."


translate chinese day5_main2_3a3284a7:


    "我不知道如何把对话进行下去。"


translate chinese day5_main2_858a16c5:


    "如果斯拉维娅不回来的话我们可以一言不发的坐到晚上。"


translate chinese day5_main2_ca656115:


    sl "给你！"


translate chinese day5_main2_60c4cc55:


    "她递给我一副很大的桨。"


translate chinese day5_main2_4ab1b169:


    me "啊...好..."


translate chinese day5_main2_d8f75e4c:


    "我们登上船，放开绳子，向水中划去。"


translate chinese day5_main2_ff778a3e:


    me "那么咱们具体是去哪里？"


translate chinese day5_main2_7c19e280:


    sl "就在那！"


translate chinese day5_main2_199f5847:


    "她伸手指向那个小岛。"


translate chinese day5_main2_adf7269e:


    sl "那个小岛叫做“最近小岛”。"


translate chinese day5_main2_97833beb:


    th "我很奇怪什么样的船长会起一个这样原始的名字？{w}不过这个小岛确实离岸边不远。"


translate chinese day5_main2_0fd8390d:


    me "是，船长！"


translate chinese day5_main2_c5a9758a:


    th "要是我知道前面有什么等着我就好了…"


translate chinese day5_main2_bb189b0d:


    "我不算是一个有经验的桨手，我也就划过一两次船。"


translate chinese day5_main2_dfd96964:


    "那个小岛只有大概五百米远，不过由于我的“技术”问题，我们走的是“之”字形。"


translate chinese day5_main2_c1ed01f4:


    "划到一般的位置我的胳膊疼的不行，所以我放下船桨休息一会儿。"


translate chinese day5_main2_cf757ee3:


    me "啊...{w}别的地方就没有草莓了吗？{w}我是说更好拿的地方。"


translate chinese day5_main2_31c67473:


    sl "可是最好吃的在这里。"


translate chinese day5_main2_683509ba:


    "斯拉维娅迷惑的看着我。"


translate chinese day5_main2_6506e675:


    un "你自己划可以吗？"


translate chinese day5_main2_e1e2ebb7:


    "列娜不像斯拉维娅一样，她马上就明白了。"


translate chinese day5_main2_b9dbf1a1:


    me "啊...{w}没事的..."


translate chinese day5_main2_7f474b89:


    "总之我不能让一个瘦弱的女孩子帮我。"


translate chinese day5_main2_255dc29b:


    "剩下的路我一直注意维持自己的生命。"


translate chinese day5_main2_b4c0cce9:


    "斯拉维娅和列娜在讨论着什么，不过我没有听——那对于我负担太重了。"


translate chinese day5_main2_f0934032:


    "最后我们终于抵达了。"


translate chinese day5_main2_d6e51afd:


    "我累倒在岸上，看着后面的码头。"


translate chinese day5_main2_9289c81b:


    "看起来那么远，让我感觉自己是第一个在月球上看到地球升起的人一样。"


translate chinese day5_main2_8f83b289:


    sl "给你！"


translate chinese day5_main2_e7fdbd78:


    "斯拉维娅递给我一个篮子。"


translate chinese day5_main2_799068d0:


    "这是一个很小的岛，仅仅方圆一百多米。"


translate chinese day5_main2_b15dff1c:


    "这里看起来更像是一个枫树林，整个地表都被枫树覆盖了。"


translate chinese day5_main2_5f826482:


    "一片平静的绿色海洋在我们的脚下散布开来，伴随着微风中飘动的落叶。"


translate chinese day5_main2_2e1cc3e7:


    "这个岛就像是世外桃源。"


translate chinese day5_main2_6ec442f5:


    th "怪不得最好吃的草莓都长在这里。"


translate chinese day5_main2_7568fc9b:


    sl "咱们得分头采集，这样可以更快些。"


translate chinese day5_main2_d76f93fa:


    me "啊，是吧。"


translate chinese day5_main2_1f4c91db:


    un "不过只有两个篮子。"


translate chinese day5_main2_f6237fab:


    "列娜小心的说着。"


translate chinese day5_main2_e2c244d1:


    sl "啊，你说得对，我的错！"


translate chinese day5_main2_c1db23a3:


    un "所以说我们要怎么分开呢？"


translate chinese day5_main2_94c61009:


    me "让我和你走吧。"


translate chinese day5_main2_23fc5b72:


    un "我们走吧…"


translate chinese day5_main2_7f370439:


    sl "嗯，那好吧！"


translate chinese day5_main2_235efb86:


    "斯拉维娅提起了另外一个篮子向小岛的另一端走去。"


translate chinese day5_main2_b17829b3:


    me "那个?"


translate chinese day5_main2_3875ba7b:


    un "那个?"


translate chinese day5_main2_71f4991e:


    me "咱们走吗?"


translate chinese day5_main2_37fef1ca:


    un "嗯。"


translate chinese day5_main2_561828ec_1:


    "列娜微笑着。"


translate chinese day5_main2_e937d3e9:


    me "那个，注意喔，一个也不能落下！"


translate chinese day5_main2_51487473:


    un "你也是。"


translate chinese day5_main2_a20cefa7_1:


    "..."


translate chinese day5_main2_a704bf84:


    "现在是收获时间。"


translate chinese day5_main2_afe45067:


    "这里的草莓确实好吃。"


translate chinese day5_main2_de5b3003:


    "如果我不赶快停下的话恐怕就都让我吃光了。"


translate chinese day5_main2_f8b9b72c:


    th "这里的草莓不像是野生的，倒像是花园里人工种的，又大又红，看来我们没有白来。"


translate chinese day5_main2_bbcae5a7:


    "列娜紧跟着我，因为我们只有一个篮子。"


translate chinese day5_main2_ac634faa:


    "我就像是一个采蘑菇的孩子，仔细的检查着每一个草莓，小心的扒开草丛。"


translate chinese day5_main2_b7921614:


    un "嗯，你干的比我好得多..."


translate chinese day5_main2_65640e4e:


    me "真的吗？说真的我都不太相信啊。"


translate chinese day5_main2_0f83bc7d:


    un "不，你真的很棒啊。"


translate chinese day5_main2_435f0faa:


    "篮子已经半满了。"


translate chinese day5_main2_c88a4757:


    me "你很喜欢大自然啊，不是吗？"


translate chinese day5_main2_f1be5fed:


    un "是啊。"


translate chinese day5_main2_5d5a082e:


    "眩目的太阳光照射着我，让我一瞬间睁不开眼。"


translate chinese day5_main2_b6151df9:


    "我坐下来，靠在一颗树边。"


translate chinese day5_main2_7587bc3f:


    me "不过这里还真是很美啊！"


translate chinese day5_main2_9b60c62c:


    "列娜坐在我的旁边。{w}我们坐的很近，手肘碰到了一起。"


translate chinese day5_main2_46b74af0:


    un "是啊！"


translate chinese day5_main2_4bfe22ae:


    "我们就坐在这里享受着，时间好像停止了一样。"


translate chinese day5_main2_9c76c985:


    "微风吹动着树叶，虫子在草丛中跳动着，远处的水面上映出点点阳光。"


translate chinese day5_main2_78fd2536:


    "列娜把头打在我的肩膀上。"


translate chinese day5_main2_d1d0b93e:


    "开始我很惊讶，不过接下来听到列娜平静的呼吸声，我觉得这样很好。"


translate chinese day5_main2_bbbf378c:


    "也许她困了，想要稍稍休息一下。"


translate chinese day5_main2_464ee2c5:


    "我就坐在这里呆呆的冷了几分钟。"


translate chinese day5_main2_9dcce076:


    "不过接下来我的脑海中闪过这些词语。"


translate chinese day5_main2_4bf43e20:


    th "列娜。{w}好近。{w}在睡着。{w}好温暖。{w}温柔。{w}感觉..."


translate chinese day5_main2_3a8d7079:


    "我盯着她。"


translate chinese day5_main2_a5887389:


    "她脸上的表情如此宁静，让我感觉她到了另一个更好的世界。"


translate chinese day5_main2_f950a1bd:


    "我不知道如果我没有听见斯拉维娅的喊话会怎么样。"


translate chinese day5_main2_2295bc5f:


    sl "谢苗！列娜！"


translate chinese day5_main2_fdadda66:


    "我来回摇着头，列娜也开始醒了。"


translate chinese day5_main2_c0ebc5f2:


    "她睁开眼睛呆呆的看着我。"


translate chinese day5_main2_badffbcb:


    me "睡得还好吗？"


translate chinese day5_main2_d0ef9648:


    un "哈?"


translate chinese day5_main2_8d04e459:


    "列娜突然想到自己倒在我的肩膀上睡着了，脸变得红扑扑的。"


translate chinese day5_main2_be58af4f:


    un "哇...抱歉..."


translate chinese day5_main2_6d36c28b:


    me "没关系。"


translate chinese day5_main2_2eefbc0c:


    "斯拉维娅向我们走过来，所以列娜马上准备起来。"


translate chinese day5_main2_fde84cf5:


    sl "你们摘到了多少？"


translate chinese day5_main2_da3a9368:


    "我叹叹气。"


translate chinese day5_main2_633224a4:


    sl "这不是很多啊。…"


translate chinese day5_main2_380af4e6:


    "她的筐里堆起了小山。"


translate chinese day5_main2_e384ce23:


    sl "算了，反正已经够了，咱们返程吧！"


translate chinese day5_main2_7cd2b80a:


    "我拿起了篮子，我们走回了小船。"


translate chinese day5_main2_790a16f6:


    "我不想自己在这里走，我希望斯拉维娅可以和我一起走。"


translate chinese day5_main2_e6680b45:


    me "啊，这很简单，我拿一个篮子，你们两个拿一个篮子。"


translate chinese day5_main2_97bf0978:


    sl "不，我跟你一起走吧。"


translate chinese day5_main2_c17a722d:


    "斯拉维娅笑了。"


translate chinese day5_main2_35ec24e9:


    me "好吧..."


translate chinese day5_main2_f8089075:


    "我有点惊讶，但是也有点高兴。"


translate chinese day5_main2_46439482:


    "列娜似乎一点也不生气。"


translate chinese day5_main2_a20cefa7_2:


    "..."


translate chinese day5_main2_a704bf84_1:


    "收获开始了。"


translate chinese day5_main2_afe45067_1:


    "这里的草莓确实很好吃。"


translate chinese day5_main2_de5b3003_1:


    "如果我不赶紧停下的话，恐怕很快就会被我吃完的。"


translate chinese day5_main2_f8b9b72c_1:


    th "这些草莓不像是野生的，倒像是家里种的。"


translate chinese day5_main2_11dc0364:


    "斯拉维娅在我的旁边走着，因为我们只有一个篮子。"


translate chinese day5_main2_ac634faa_1:


    "我就像是一个采蘑菇的孩子，仔细的检查着每一个草莓，小心的扒开草丛。"


translate chinese day5_main2_9f273fda:


    sl "主意。"


translate chinese day5_main2_eebba4df:


    "我把一整片草莓落在了后面。"


translate chinese day5_main2_cd47bc4e:


    me "啊，抱歉。"


translate chinese day5_main2_705bea28:


    sl "没关系。"


translate chinese day5_main2_9db1f947:


    me "你一定喜欢在这里吧？你很喜欢大自然。"


translate chinese day5_main2_49cc0ae4:


    sl "当然了。"


translate chinese day5_main2_c17a722d_1:


    "斯拉维娅笑了。"


translate chinese day5_main2_0854ae45:


    sl "这让我想起来我家，那里也有很多的桦树。"


translate chinese day5_main2_f32c544b:


    "她看着远方。"


translate chinese day5_main2_14e38355:


    me "那个，我一直想问，总的来说你喜欢什么？"


translate chinese day5_main2_ed0e2cff:


    me "你好像7x24小时工作个不停一样。"


translate chinese day5_main2_7db509b3:


    sl "呃"


translate chinese day5_main2_60f3d758:


    "她陷入思考。"


translate chinese day5_main2_63626b5d:


    sl "我不太确定。{w}很多事情我都喜欢。"


translate chinese day5_main2_882dd540:


    me "嗯，可以理解，不过？"


translate chinese day5_main2_9698acdd:


    sl "我喜欢缝纫和编织，那样的活儿…"


translate chinese day5_main2_f22d26a4:


    "斯拉维娅从口袋里拿出一条红领巾。{w}上面绣着红黄绿色的花。"


translate chinese day5_main2_aef33dd6:


    "它们复杂的混合在了一起，形成一幅完整的图案。"


translate chinese day5_main2_576c9c96:


    "一个典型的俄国红领巾。"


translate chinese day5_main2_fa5e2895:


    "我看着它想象着斯拉维娅穿着沙拉芬，坐在一个破旧的房子前的长椅上，一群孩子在周围跑来跑去。"


translate chinese day5_main2_9918c29c:


    me "很可爱。"


translate chinese day5_main2_8c1bfef0:


    sl "谢谢！{w}那就送给你做礼物吧！"


translate chinese day5_main2_95e49024:


    "这个提议让我很不好意思。"


translate chinese day5_main2_08a40647:


    me "这怎么好意思…"


translate chinese day5_main2_127a57de:


    sl "不，拿着它！"


translate chinese day5_main2_72fe7131:


    "我又看看那条红领巾，把它装进口袋里。"


translate chinese day5_main2_73aab3cd:


    me "谢谢…"


translate chinese day5_main2_a20cefa7_3:


    "…"


translate chinese day5_main2_ff2ae452:


    "这里有好多草莓，才过了半个小时，我们就装了满满一筐。"


translate chinese day5_main2_fb9748eb:


    me "看起来我们完成了…"


translate chinese day5_main2_e498e9bd:


    sl "是啊，咱们采到了这么多，应该已经够了。"


translate chinese day5_main2_f110cff6:


    "我们回到船上时列娜还没有回来。"


translate chinese day5_main2_b852b10d:


    sl "她一个人的话可能需要的时间长一些。"


translate chinese day5_main2_ff886d6d:


    me "是啊…"


translate chinese day5_main2_94972c05:


    "我看着河流。"


translate chinese day5_main2_827d1834:


    "平静的水面上只有太阳光在闪耀着。"


translate chinese day5_main2_29dcd13b:


    sl "你在想什么？"


translate chinese day5_main2_51eebe65:


    me "没什么…{w}你呢？"


translate chinese day5_main2_7be9fd97:


    sl "我...?{w}假期结束怎么办呢？咱们都要离开这里回家去…"


translate chinese day5_main2_08224f21:


    sl "我还会见到在这里遇到的人们吗？{w}我会再遇到你吗...?"


translate chinese day5_main2_e932afd0:


    "她看着我的眼睛里充满的难过。"


translate chinese day5_main2_bffe94f9:


    "列娜不知道从什么地方出现了。"


translate chinese day5_main2_bfcc6d0c:


    un "哇，你们已经到...这里了。"


translate chinese day5_main2_26b9d76b:


    "她给我们看看一满筐的草莓。"


translate chinese day5_main2_e2c68eb7:


    sl "好极了，现在咱们可以回去了！"


translate chinese day5_main2_3ea9d288:


    "斯拉维娅的表情和她的一番话还在我的心头萦绕。"


translate chinese day5_main2_9b1d3958:


    "难过和伤心不是她的风格。"


translate chinese day5_main2_8bcb5251:


    th "她是不是一直在一副快乐的面具下藏着这种痛苦呢？"


translate chinese day5_main2_54c1d08a:


    "我不知道也没有办法弄清楚。"


translate chinese day5_main2_e3a29d10:


    th "也许以后吧…"


translate chinese day5_main2_4d28e778:


    "回去用的时间少一些，我专心在划船上。"


translate chinese day5_main2_1012dd1b:


    "我只想活着回去，我的手划了几下桨就疼的不行。"


translate chinese day5_main2_d6437409:


    "系好船以后，我完全没有力气了。"


translate chinese day5_main2_ba4a18aa:


    "斯拉维娅和列娜弯下腰问我。"


translate chinese day5_main2_379cb129:


    sl "如果太费劲的话，你早就可以说出来的啊！"


translate chinese day5_main2_31b01f67:


    un "是啊"


translate chinese day5_main2_bb2ef35c:


    me "别担心，还好…{w}我只要躺一会儿就行…"


translate chinese day5_main2_50def11b:


    sl "好吧，请你把这两个篮子带给奥尔加·德米特里耶夫娜，我们还有其他的事情要做。"


translate chinese day5_main2_85279b68:


    me "好吧，当然。"


translate chinese day5_main2_6d720424:


    "为了不用起来我什么都答应。"


translate chinese day5_main2_3bfb15a5:


    "斯拉维娅把篮子放在我的旁边离开了，一边和列娜愉快的讨论着什么。"


translate chinese day5_main2_cb288325:


    me "反正最难的部分已经结束了…"


translate chinese day5_main2_258341cc:


    "这是我在提起这些篮子之前想的…"


translate chinese day5_main2_f1fdfa2c:


    "虽然这两筐水果没有多重，但是在刚刚划过船之后它们感觉就像是两袋水泥。"


translate chinese day5_main2_331ec88f:


    "结果前往辅导员房间的路显得格外的长，我每隔50米就要停下来休息。"


translate chinese day5_main2_3f67afab:


    "抵达以后我费力的坐在椅子上。"


translate chinese day5_main2_30582463:


    me "奥尔加·德米特里耶夫娜！我有礼物给你！"


translate chinese day5_main2_dfffcbac:


    "没有回答。"


translate chinese day5_main2_a11cb528:


    "我设法爬进房间。"


translate chinese day5_main2_40d2a09a:


    "里面没有人。"


translate chinese day5_main2_d189a7a2:


    me "如果你不要的话…"


translate chinese day5_main2_c3f0764a:


    "我躺下来睡着了。"


translate chinese day5_main2_8bc92ca0:


    "我做了一个奇怪的梦。"


translate chinese day5_main2_f12d326b:


    "我用力的划着船，后面巨大的草莓追着我。"


translate chinese day5_main2_b573ff1d:


    "我满脸汗水，越来越费力的划着，然而大草莓还是要追上我了。"


translate chinese day5_main2_371f84cd:


    "他们张开大嘴露出满嘴牙。"


translate chinese day5_main2_57cc90da:


    th "等一下...草莓有牙？！"


translate chinese day5_main2_bed90e86:


    mt "…谢苗！谢苗！"


translate chinese day5_main2_4b4e1333:


    "我醒了。"


translate chinese day5_main2_821dbe9f:


    "奥尔加·德米特里耶夫娜在旁边摇晃着我。"


translate chinese day5_main2_4f14ab7e:


    mt "看来是大丰收啊。"


translate chinese day5_main2_f0de0091:


    mt "好吧，不过不止这些！"


translate chinese day5_main2_b635cee7:


    th "说真的，我还期待着一会儿能美美的休息一下呢…"


translate chinese day5_main2_6091ae1b:


    mt "你知道这些草莓是做什么的吗？"


translate chinese day5_main2_230628fb:


    me "不懂…"


translate chinese day5_main2_f1e8b4cf:


    "我真是城市。"


translate chinese day5_main2_035bcc73:


    mt "我们要做一个蛋糕！"


translate chinese day5_main2_f2e007f0_1:


    me "我明白了…"


translate chinese day5_main2_918f0332:


    th "嗯，这说得过去。"


translate chinese day5_main2_cf6837ea:


    mt "为了纪念救回舒里克。{w}都是为了感谢你！"


translate chinese day5_main2_e8bfedc9:


    "看来采摘草莓还远远不是最后一件事。"


translate chinese day5_main2_b125d822:


    th "不过请告诉我，如果是一个为了感谢我的庆祝活动，为什么我要亲自累个半死的准备啊？.."


translate chinese day5_main2_443723fe:


    me "嗯，是…"


translate chinese day5_main2_cb1beef9:


    mt "所以说，我有一个重要的任务交给你！"


translate chinese day5_main2_a8dc8e24:


    mt "我们找不到酵母面粉和糖。{w}但是在晚饭之前需要全部准备齐。"


translate chinese day5_main2_1cdc0576:


    me "也就是说那些制作蛋糕的人自己搞不定吗？"


translate chinese day5_main2_13e8802b:


    "我不爽的问着。"


translate chinese day5_main2_4cb23232:


    mt "他们当然不行！{w}他们都很忙的！{w}你是整个营地里唯一一个闲着的人！"


translate chinese day5_main2_c4eaf48d:


    "她的话有一部分正确，但是并没有什么帮助。"


translate chinese day5_main2_aefacb3e:


    "再说这些话就像是一颗子弹射中我。"


translate chinese day5_main2_05b86f04:


    mt "所以写下来吧！{w}你会在医务室找到酵母，面粉在图书馆，糖在社团活动室。"


translate chinese day5_main2_42814c09:


    me "等，等一…"


translate chinese day5_main2_e9da3bbd:


    mt "我很着急！祝你好运！"


translate chinese day5_main2_0457a719:


    "她狡猾的笑着离开了。"


translate chinese day5_main2_8824565a:


    th "这个营地里有很多奇怪的地方…{w}酵母在医务室？这我可以忍...{w}面粉在图书馆？！"


translate chinese day5_main2_dd117348:


    th "还有糖…{w}这已经超出我的理解能力了！"


translate chinese day5_main2_d71f77ee:


    "我往地上吐了口唾沫。"


translate chinese day5_main2_6a2ac63d:


    me "我不想相信这个！你骗人！"


translate chinese day5_main2_2b37191e:


    "感觉像是有一群绿巨人围着我笑话我。"


translate chinese day5_main2_b150a7df:


    th "也许我就不管这个蛋糕了...?"


translate chinese day5_main2_f58ed270:


    "我思考了会儿"


translate chinese day5_main2_c815c3c0:


    th "如果奥尔加·德米特里耶夫娜生气的话恐怕结果不会太好。"


translate chinese day5_main2_7cca4369:


    th "这会让我在这里的生活还有我寻找答案的过程变得复杂。"


translate chinese day5_main2_515e2ec4:


    th "看来我没有选择…"


translate chinese day5_aidpost_2_71caa85a:


    th "我感觉自己最近来医务室太频繁了。"


translate chinese day5_aidpost_2_7a9eec06:


    th "不过我还能做什么呢？"


translate chinese day5_aidpost_2_af5fbbdb:


    "我叹叹气，敲着门。"


translate chinese day5_aidpost_2_e9203c64:


    cs "请进！"


translate chinese day5_aidpost_2_e6e52c0a:


    "护士用平淡的声音回答着。"


translate chinese day5_aidpost_2_b9a6c45b:


    me "奥尔加·德米特里耶夫娜让我来这里找..."


translate chinese day5_aidpost_2_5286c4cf:


    "我稍微犹豫了一下。"


translate chinese day5_aidpost_2_68451631:


    me "酵母..."


translate chinese day5_aidpost_2_2d204cc8:


    cs "啊，好的。"


translate chinese day5_aidpost_2_6d77584d:


    "她善良的笑了。"


translate chinese day5_aidpost_2_524e70a4:


    cs "只是我没有任何…少先队员。"


translate chinese day5_aidpost_2_e8bf1827:


    me "什么？她说…"


translate chinese day5_aidpost_2_48f8061d:


    cs "嗯，我有一些，不过用光了。"


translate chinese day5_aidpost_2_62e70926:


    "我都没想问她是怎么有的。"


translate chinese day5_aidpost_2_55f0b627:


    cs "别担心，你可以拿一些阿司匹林。"


translate chinese day5_aidpost_2_624f6a17:


    th "那个倒是有用。"


translate chinese day5_aidpost_2_ec7a55c9:


    me "我要从哪里...?"


translate chinese day5_aidpost_2_ed0914b5:


    "我叹叹气。"


translate chinese day5_aidpost_2_19a3e4d7:


    cs "拿着！"


translate chinese day5_aidpost_2_4fb9e776:


    "她打开抽屉拿出一个小瓶。"


translate chinese day5_aidpost_2_3a770cd2:


    "我仔细看了看，是“奥斯坦金”啤酒"


translate chinese day5_aidpost_2_51149e68:


    me "..."


translate chinese day5_aidpost_2_94e2a40d:


    cs "怎么啦？{w}啤酒也是发酵的东西嘛。"


translate chinese day5_aidpost_2_8fb93608:


    "她深深的凝望着我。"


translate chinese day5_aidpost_2_97a20ab2:


    cs "没有人会发现的！"


translate chinese day5_aidpost_2_51d589e0:


    "她说得对，不过这一切都十分荒谬让我不知道说什么好。"


translate chinese day5_aidpost_2_d9f00730:


    me "你…确定？"


translate chinese day5_aidpost_2_a61a2efe:


    cs "当然！"


translate chinese day5_aidpost_2_d76d86ac:


    me "那好吧…"


translate chinese day5_aidpost_2_fe10b3f1:


    "这个瓶子肯定放不进我的裤兜。"


translate chinese day5_aidpost_2_73aab3cd:


    me "啊，谢谢了..."


translate chinese day5_aidpost_2_e7fd1586:


    "我不好意思的嘟囔着，离开了医务室。"


translate chinese day5_aidpost_2_942696fd:


    th "啤酒可以代替酵母。"


translate chinese day5_aidpost_2_a2b4dc7e:


    th "我有限的生物知识倒是可以接受，不过…"


translate chinese day5_aidpost_2_4997ec0f:


    "拿着这个瓶子走来走去显得很二，所以我决定把它藏在辅导员的房间。"


translate chinese day5_aidpost_2_aeaa1520:


    "不过我得在没有人注意的情况下放进去。"


translate chinese day5_aidpost_2_f988b661:


    "我用衬衫罩住啤酒。"


translate chinese day5_aidpost_2_b2e65042:


    "本来一切都很顺利，不过斯拉维娅突然叫我去广场。"


translate chinese day5_aidpost_2_9b22a2b9:


    "事实上她从背后突然出现，吓了我一跳。"


translate chinese day5_aidpost_2_99a001ee:


    sl "怎么样？"


translate chinese day5_aidpost_2_ad02f53a:


    me "什么怎么样？"


translate chinese day5_aidpost_2_28a39c69:


    sl "你寻找配料啊。"


translate chinese day5_aidpost_2_ed34a1cf:


    me "哇，你已经知道了啊…"


translate chinese day5_aidpost_2_2bb8d573:


    sl "是的！"


translate chinese day5_aidpost_2_c17a722d:


    "斯拉维娅笑了。"


translate chinese day5_aidpost_2_7de85321:


    me "还好啦…"


translate chinese day5_aidpost_2_f7e1cdf8:


    "我回答着，试图掩饰自己的慌乱。"


translate chinese day5_aidpost_2_9d5782dc:


    sl "那里面是什么？"


translate chinese day5_aidpost_2_c76c023d:


    "她指指从我的衬衫里突出的瓶子。"


translate chinese day5_aidpost_2_5652d5eb:


    me "啊，这个…"


translate chinese day5_aidpost_2_bda7946b:


    th "被她抓到了！"


translate chinese day5_aidpost_2_4c420dcf:


    me "哇，没什么..."


translate chinese day5_aidpost_2_478e684d:


    "我红着脸傻笑着。"


translate chinese day5_aidpost_2_d96f5cdf:


    me "该走了..."


translate chinese day5_aidpost_2_bc62fb23:


    "我几乎是在跑，把满脸问号的斯拉维娅留在后面。"


translate chinese day5_aidpost_2_74475de8:


    th "她是一个不爱乱问问题的人真是太好了。"


translate chinese day5_aidpost_2_bd1cc79d:


    "不过这个夏令营里总是有一些喜欢指手画脚的人。"


translate chinese day5_aidpost_2_0616ad18:


    "穿过少先队员的宿舍时，我遇到了乌里扬娜。"


translate chinese day5_aidpost_2_397ec628:


    us "你里面在藏着什么？"


translate chinese day5_aidpost_2_07c1adb6:


    "她厚颜无耻的问道。"


translate chinese day5_aidpost_2_6ada69b4:


    "我觉得没有否认的必要，所以我挑衅的回答道："


translate chinese day5_aidpost_2_38b6a274:


    me "这和你无关，我奉命向指挥部送信。"


translate chinese day5_aidpost_2_1ebafc67:


    us "那一定是一个大...{w}信..."


translate chinese day5_aidpost_2_fd58cad9:


    "我正把瓶子放在腰部所以有一点尴尬。"


translate chinese day5_aidpost_2_d8a23d43:


    us "你需要帮忙吗？"


translate chinese day5_aidpost_2_5fc5692c:


    me "我自己没问题！"


translate chinese day5_aidpost_2_5a4c3675:


    "我充满自信的走过去，继续我的行程。"


translate chinese day5_aidpost_2_fbcbd186:


    "让我惊讶的是她没有继续问什么，也没有追我。"


translate chinese day5_aidpost_2_1266f2bc:


    "奥尔加·德米特里耶夫娜的房间里没有人，所以我成功的把瓶子藏在了我的枕头底下。"


translate chinese day5_aidpost_2_4e945031:


    "回到室外以后我放松的叹口气。"


translate chinese day5_aidpost_2_74665f46:


    "说真的我都没想到自己会这么重视一瓶啤酒！好像回到中学时一样。"


translate chinese day5_aidpost_2_af3e0460:


    th "不过现在安全了，这是好事..."


translate chinese day5_aidpost_2_ceb963db:


    th "如果有人发现了，我也可以说不是我的—我可以轻易的从自己丰富的经验中找出一个理由。"


translate chinese day5_clubs_2_bb74ce24:


    "我今天似乎经历了比前些天加起来还要多的事情。"


translate chinese day5_clubs_2_7fddb993:


    "结果去社团室找糖的时候我都忘了思考这是一个多么奇怪的事情了。"


translate chinese day5_clubs_2_ebe5438a:


    "舒里克和电子小子正在充满热情的建造着什么。"


translate chinese day5_clubs_2_2e2f3c1b:


    "他们忙的过于投入，没有发现我。"


translate chinese day5_clubs_2_2b02462e:


    "我凑近看看。"


translate chinese day5_clubs_2_3af379d1:


    "是某种机器人，或者至少是一部分。"


translate chinese day5_clubs_2_729ff343:


    "而且这个机器人是个女性，还有两个动物耳朵。"


translate chinese day5_clubs_2_a869bd00:


    "我不想乱讲这个机器人又创造历史什么的话。"


translate chinese day5_clubs_2_c69b15fb:


    "虽然看起来很厉害，不过我还是质疑这个机器人能不能占领地球。"


translate chinese day5_clubs_2_81faf816:


    "不过他们似乎更享受过程。"


translate chinese day5_clubs_2_76cfb844:


    "我也有这种爱好，不过没好意思和他们说。"


translate chinese day5_clubs_2_d63ce887:


    "不过另一方面来说他们不怕失败，不在乎别人的看法。"


translate chinese day5_clubs_2_ff329017:


    th "哇，我似乎真的在拿他们和一些权威的专家进行比较。…"


translate chinese day5_clubs_2_b90af56a:


    me "嗨。"


translate chinese day5_clubs_2_853a9b87:


    "我心虚的和他们打了个招呼。"


translate chinese day5_clubs_2_5c419eaf:


    sh "哇！谢苗！快进来，很高兴见到你！"


translate chinese day5_clubs_2_d193794e:


    th "我已经进来了…"


translate chinese day5_clubs_2_2bd5ad91:


    sh "那个，十分抱歉，我什么都不记得了，那个…"


translate chinese day5_clubs_2_95e7fcc0:


    me "别在意，没关系！"


translate chinese day5_clubs_2_f3cf77af:


    el "您怎么来啦？"


translate chinese day5_clubs_2_e7ed1aae:


    "电子小子“热情的”迎接我。"


translate chinese day5_clubs_2_f069bcd3:


    "有时我觉得他在讽刺别人。"


translate chinese day5_clubs_2_88b7e8e0:


    me "糖。{w}我需要糖。"


translate chinese day5_clubs_2_e4fdc3d2:


    "我的脑海中不知为什么浮现出了一个小人说“金子，我们需要更多金子！”这样的情景。"


translate chinese day5_clubs_2_9965d584:


    el "我们有。"


translate chinese day5_clubs_2_9b27ce05:


    "电子小子冷静地说。"


translate chinese day5_clubs_2_edb7828d:


    sh "你为什么需要这个？"


translate chinese day5_clubs_2_6a1f6f30:


    "我觉得自己不应该把蛋糕的事情告诉他，那样就没有意思了。"


translate chinese day5_clubs_2_cea669a0:


    me "我不知道...奥尔加·德米特里耶夫娜让我拿的..."


translate chinese day5_clubs_2_f7be46a6:


    el "好吧，等一下。"


translate chinese day5_clubs_2_c1165691:


    "电子小子消失在了门的另一边。"


translate chinese day5_clubs_2_72678105:


    me "你们为什么在这里有糖？不是应该在食堂里吗？"


translate chinese day5_clubs_2_c5831f8c:


    sh "上次食物卡车到的时候这是最后卸下来的。"


translate chinese day5_clubs_2_8afaf429:


    sh "因为我们这栋楼距离门口最近所以他们决定放在这里..."


translate chinese day5_clubs_2_9b463aae:


    th "这很有道理..."


translate chinese day5_clubs_2_bb104bbc:


    "门打开了，电子小子抱着一个大口袋走了出来。"


translate chinese day5_clubs_2_d24fcbc5:


    "我不知道那个蛋糕会有多大，不过用这么多糖就没有必要了..."


translate chinese day5_clubs_2_30543032:


    me "呃，谢谢，不过我应该不需要这么多..."


translate chinese day5_clubs_2_776a0c07:


    el "不过我们要放在哪里呢？"


translate chinese day5_clubs_2_961b180e:


    "电子小子惊讶的看着我。"


translate chinese day5_clubs_2_590bb4f8:


    el "我们没有地方放了，你不是需要嘛，就拿走好啦。"


translate chinese day5_clubs_2_ec12a3f5:


    th "看来他之前的微笑不是没有原因的。"


translate chinese day5_clubs_2_0cff3fa1:


    me "也许你可以帮我运送一下？实在比较远…"


translate chinese day5_clubs_2_40b931b1:


    el "我们很忙。"


translate chinese day5_clubs_2_07640196:


    "他指指手中的机器人。"


translate chinese day5_clubs_2_c06e6e9a:


    "我看看舒里克，反正他欠我人情。"


translate chinese day5_clubs_2_d7fa97fa:


    "他犹豫了一下然后不好意思的向一边看过去。"


translate chinese day5_clubs_2_9b5a3058:


    "我叹了一口气，拿起袋子走向门口。"


translate chinese day5_clubs_2_d9acf4d6:


    me "总之谢谢了…"


translate chinese day5_clubs_2_d52a2c3a:


    "我对自己说道。"


translate chinese day5_clubs_2_256429b1:


    "不过我没有成功的移动太远。"


translate chinese day5_clubs_2_87117e72:


    "过了仅仅二十米左右我就不得不放下来休息。"


translate chinese day5_clubs_2_0fd93e15:


    "我不清楚它有多重，不过大概有二十千克。"


translate chinese day5_clubs_2_4099b982:


    "不过这里距离食堂也只有二百米左右。"


translate chinese day5_clubs_2_e147e5e5:


    "但是即使是这个距离对我来说也实在是太远了。"


translate chinese day5_clubs_2_d88fce53:


    "我不停地冲刺，然后休息，这时我听到背后的声音："


translate chinese day5_clubs_2_73f17541:


    un "也许我可以帮帮你？"


translate chinese day5_clubs_2_1cd2d1a1:


    "我看到列娜在我的面前。"


translate chinese day5_clubs_2_5cbd7cac:


    me "我不觉得你可以…"


translate chinese day5_clubs_2_2fb0f57d:


    "我感觉好讽刺…"


translate chinese day5_clubs_2_be079536:


    un "我可以找来一个手推车。"


translate chinese day5_clubs_2_dbff5c0c:


    th "手推车...我为什么自己没有想到？！"


translate chinese day5_clubs_2_02b2a515:


    me "好的，那太好了！"


translate chinese day5_clubs_2_a8585d26:


    un "等一下，我很快就回来！"


translate chinese day5_clubs_2_47fe246e:


    "她笑着向广场跑去。"


translate chinese day5_clubs_2_e8fbb4d7:


    th "要是没有她我会怎么样啊..."


translate chinese day5_clubs_2_8efa90a2:


    th "列娜也不总是那样害羞胆小的。"


translate chinese day5_clubs_2_89141735:


    "我思考起来。"


translate chinese day5_clubs_2_952b5881:


    "她和平常完全不一样，脸上没有害羞的表情，相反只是自信和笑容。"


translate chinese day5_clubs_2_017038ff:


    "这不是一件太大的事，不过是列娜帮的我…"


translate chinese day5_clubs_2_a1ed0501:


    "过了几分钟她推过来一个相当小的小车。"


translate chinese day5_clubs_2_3fed3b65:


    "我把袋子放上去。"


translate chinese day5_clubs_2_3066598f:


    me "谢谢。"


translate chinese day5_clubs_2_69777dad:


    un "不客气…"


translate chinese day5_clubs_2_42f94d04:


    "她红着脸看着地下。"


translate chinese day5_clubs_2_3ecb53f0:


    th "哇，平常的列娜回来了！"


translate chinese day5_clubs_2_ab1d28c0:


    un "那么我要走了…"


translate chinese day5_clubs_2_45858c8d:


    me "好的，再见！再次谢谢你！"


translate chinese day5_clubs_2_3da1183b:


    "我向她喊着。"


translate chinese day5_clubs_2_d45c5c2b:


    "有时候我感觉列娜的身体里生活着完全不同的两个人。"


translate chinese day5_clubs_2_0b282078:


    "但是第二个，自信，快乐，有点大胆的列娜只在她对我说话的时候出现。"


translate chinese day5_clubs_2_9a4a089a:


    th "或者又是我想多了吗...?"


translate chinese day5_clubs_2_fce0897d:


    "我想自己如果能很快找到这些配料会更好，所以我推着车直接前往辅导员那里。"


translate chinese day5_library_2_d8c69e27:


    "如果前两个储存食材的地点还有一点道理的话，那么在图书馆里的面粉实在是说不通。"


translate chinese day5_library_2_94112c60:


    "我仔细想了想谁会为了什么把面粉放在那里，可是还是想不通。"


translate chinese day5_library_2_328028d5:


    "按照热妮娅的性格，我得提前敲门才行。"


translate chinese day5_library_2_9b83bcec:


    mz "请进。"


translate chinese day5_library_2_ccef914a:


    "热妮娅从自己的眼镜后面偷看着我。"


translate chinese day5_library_2_6705d775:


    mz "你想要什么？"


translate chinese day5_library_2_e39034b2:


    me "呃...{w}不要多想，不过我需要…"


translate chinese day5_library_2_511a0256:


    "我不想看起来像是一个傻子一样所以小心的解释了一下。"


translate chinese day5_library_2_c2c36c71:


    me "我需要面粉，奥尔加·德米特里耶夫娜告诉我在这里，我知道这很奇怪但是..."


translate chinese day5_library_2_16f45806:


    me "我被派来找你，为了庆祝拯救舒里克制作蛋糕。"


translate chinese day5_library_2_e3838181:


    mz "是的，我这里有面粉，这有什么奇怪的吗？"


translate chinese day5_library_2_80c6622f:


    "热妮娅惊讶地回答着。"


translate chinese day5_library_2_f64f4e8b:


    "这个时候我感觉自己头部被重击一拳，完全不能理解发生了什么。"


translate chinese day5_library_2_079c3fed:


    th "面粉在图书馆...是啊，有什么奇怪的呢？..."


translate chinese day5_library_2_e29b49ed:


    th "我们在异想世界里，我就是爱丽丝，我得吃下魔法的蘑菇才能回去..."


translate chinese day5_library_2_fe1f361c:


    mz "嘿！"


translate chinese day5_library_2_ca8c1883:


    me "啊？什么？"


translate chinese day5_library_2_5c270e5d:


    "我在做白日梦。"


translate chinese day5_library_2_e683043b:


    mz "在这里等着，我马上回来。"


translate chinese day5_library_2_660215b2:


    "她消失在暑假后面，我只好在这里等着。"


translate chinese day5_library_2_c8ef07cc:


    "过了一会儿，一阵暗门打开的声音引起了我的注意。"


translate chinese day5_library_2_21111af0:


    me "嘿，你需要帮忙吗？"


translate chinese day5_library_2_91d35e95:


    "我大声问着。"


translate chinese day5_library_2_644e33dc:


    mz "我没问题！"


translate chinese day5_library_2_47919150:


    "热妮娅向我嚎叫着。"


translate chinese day5_library_2_dcc13160:


    mz "她似乎在地下室里，所以我得等一会儿了。"


translate chinese day5_library_2_d08df322:


    me "很好很好..."


translate chinese day5_library_2_0a82bf01:


    "过了好几分钟，热妮娅还是没有回来。"


translate chinese day5_library_2_83502309:


    "我开始担心了，突然图书馆大门砰地打开了，阿丽夏冲了进来。"


translate chinese day5_library_2_c46172f4:


    "她看见我在这里也很惊讶。"


translate chinese day5_library_2_cdb9fe3e:


    dv "你在这里干什么？"


translate chinese day5_library_2_3df6714e:


    me "不允许我在这里吗？"


translate chinese day5_library_2_30a65a09:


    "我粗鲁的对她说。"


translate chinese day5_library_2_7ebd37d9:


    "阿丽夏似乎不知道怎么办才好。"


translate chinese day5_library_2_f1713752:


    dv "哈...谁在乎啊...?"


translate chinese day5_library_2_866ddbdc:


    "她吸着鼻子走向热妮娅的桌子。"


translate chinese day5_library_2_de1f890a:


    me "那么你为什么来这里呢？"


translate chinese day5_library_2_6184e4df:


    "阿丽夏好像已经准备好说些什么了，但是又突然改变了主意，转过去把手藏在后面。"


translate chinese day5_library_2_fe41f14c:


    me "还一本书吗？"


translate chinese day5_library_2_91b7e642:


    "我说出了头脑中蹦出来的第一件事。"


translate chinese day5_library_2_2d3c80f4:


    dv "这跟你没有关系..."


translate chinese day5_library_2_eb262731:


    "她好像犹豫了一下然后回答道。"


translate chinese day5_library_2_1c67eb95:


    me "那是什么书？"


translate chinese day5_library_2_991d9a14:


    "阿丽夏沉默了。"


translate chinese day5_library_2_d8028619:


    me "哎呀，让我看看嘛！我很好奇“危险勿靠近”同学在看什么？"


translate chinese day5_library_2_2d3c80f4_1:


    dv "跟你没有关系…"


translate chinese day5_library_2_f7c41790:


    "她的话越来越没有底气了。"


translate chinese day5_library_2_42f650e4:


    me "好吧好吧，我就不坚持了…"


translate chinese day5_library_2_76998bd7:


    "事实上我很感兴趣。"


translate chinese day5_library_2_66eefb60:


    "而且看见她手里拿着书我感觉很搞笑。"


translate chinese day5_library_2_a36340e5:


    "这里如果有电视电脑能看电影的话，那些肯定是更适合这样一个女孩子的。"


translate chinese day5_library_2_30470e07:


    "不过她却拿着一本书…"


translate chinese day5_library_2_a1a458f1:


    "我的好奇心获得了胜利，趁着阿丽夏注意力没有集中，我冲上去，一把夺过了那本书。"


translate chinese day5_library_2_97a3e89b:


    dv "呀！"


translate chinese day5_library_2_c9e0274f:


    "她叫了起来。"


translate chinese day5_library_2_a1b7c998:


    "接下来的几秒钟里她的表情让我怀疑自己是不是做出了正确的决定。"


translate chinese day5_library_2_38429226:


    th "如果我要死掉也要明明白白的死掉！"


translate chinese day5_library_2_a0da6e68:


    "我拿着一本《乱世佳人》。"


translate chinese day5_library_2_8d03e8c1:


    "就是列娜那天晚上在长椅上看的书。"


translate chinese day5_library_2_29dc2955:


    "我过于惊讶，一度忘记了自己的死期将至。"


translate chinese day5_library_2_6958082e:


    me "很有趣吗？"


translate chinese day5_library_2_3b4b97c0:


    dv "是啊"


translate chinese day5_library_2_eb7dd1a8:


    "阿丽夏毫无热情的红着脸回答着。"


translate chinese day5_library_2_621c7bd3:


    me "那好吧…"


translate chinese day5_library_2_75b79b0a:


    "我把书还给她。"


translate chinese day5_library_2_df52def3:


    "阿丽夏把书扔在桌子上头也不回的跑了出去。"


translate chinese day5_library_2_c3a92102:


    th "所以说她也是有人类的情感的，她也是一个女孩。"


translate chinese day5_library_2_fe404d31:


    "回顾了一下刚刚发生的事情，我发现其实没有什么奇怪的。"


translate chinese day5_library_2_e499eebb:


    "好奇心害死猫。"


translate chinese day5_library_2_0dc343a6:


    th "不管怎么说，那是阿丽夏，这件事本来有可能变成一团糟，而且我还有活要干。"


translate chinese day5_library_2_ab719773:


    dv "好吧，我等一会儿再来..."


translate chinese day5_library_2_24b51b37:


    "阿丽夏没有回头的离开了图书馆。"


translate chinese day5_library_2_89141735:


    "这让我开始思考。"


translate chinese day5_library_2_72ba180f:


    th "这本书是关于什么的，让她那么不好意思？"


translate chinese day5_library_2_4521510a:


    th "阿丽夏不好意思本身就很奇怪，而且还是因为一本书…"


translate chinese day5_library_2_10c14b59:


    th "不过现在猜来猜去有什么意思。"


translate chinese day5_library_2_4160881a:


    "终于，地下传来了热妮娅的呻吟声，响彻整个图书馆。"


translate chinese day5_library_2_bece771e:


    mz "拿着！"


translate chinese day5_library_2_98368a17:


    "我穿过书架，来到了一身是汗的图书管理员面前她的旁边放着一个袋子。"


translate chinese day5_library_2_7959d521:


    th "嗯，下面也许是什么储藏室…"


translate chinese day5_library_2_553525de:


    me "谢谢!"


translate chinese day5_library_2_821c2037:


    "我拿起袋子离开了图书馆。"


translate chinese day5_library_2_089e3478:


    "幸好这个袋子不是很沉，我没有花多大力气就回到了房间。"


translate chinese day5_main3_8645b244:


    "终于我集齐了所有需要的东西。"


translate chinese day5_main3_4f706171:


    "我把糖，面粉还有草莓什么都装在一起。"


translate chinese day5_main3_ee5f67e3:


    "为了以防万一，我还是把啤酒藏在衣服底下。"


translate chinese day5_main3_f702be22:


    "一天快要结束了，所以我得快一点，烤蛋糕需要一段时间。"


translate chinese day5_main3_1546fb97:


    "当然我更想躺下来彻底的放松一下，不过我不能让奥尔加·德米特里耶夫娜失望啊。"


translate chinese day5_main3_050e260b:


    "确实，在经历了这么多程序之后，我觉得自己有责任把这件事做好。"


translate chinese day5_main3_bfc08830:


    "我在广场停下来喘口气。"


translate chinese day5_main3_dcabd394:


    "倒不是因为这个推车有多重，只是现在任何运动都会让我浑身疼痛。"


translate chinese day5_main3_fc8d210b:


    "我坐在长椅上闭上眼睛。"


translate chinese day5_main3_5dad0356:


    dreamgirl "那是什么？"


translate chinese day5_main3_4999a965:


    "我不太在乎那是谁，大概只是一个好奇的少先队员女孩想要了解一下一个陌生的同伴。"


translate chinese day5_main3_cb23542f:


    me "你在说什么？"


translate chinese day5_main3_b037ba1a:


    "我吊儿郎当的问道。"


translate chinese day5_main3_d00d3d0f:


    "她没有回答。"


translate chinese day5_main3_b9035f5a:


    me "那些是做蛋糕的材料，你喜欢蛋糕吗？"


translate chinese day5_main3_3d6bef45:


    dreamgirl "我不知道..."


translate chinese day5_main3_159dd052:


    me "什么，你没有吃过蛋糕吗？"


translate chinese day5_main3_3d6bef45_1:


    dreamgirl "我不知道..."


translate chinese day5_main3_9ee429fd:


    "显然这个女孩没有明白我的意思，不过现在这不重要。"


translate chinese day5_main3_7285209b:


    "我对这个对话不是很有兴趣，我现在很累，不想思考外界的奇怪问题。"


translate chinese day5_main3_6978c07f:


    me "我明白了..."


translate chinese day5_main3_0a00a780:


    me "一会儿来食堂尝一尝吧。"


translate chinese day5_main3_f1668a0e:


    dreamgirl "真的？"


translate chinese day5_main3_dae36352:


    me "真的。"


translate chinese day5_main3_5b24c110:


    dreamgirl "是用什么做的呢？"


translate chinese day5_main3_2a985733:


    me "什么？"


translate chinese day5_main3_fad6f3d4:


    "我平淡的问道。"


translate chinese day5_main3_52a831f5:


    dreamgirl "那些...蛋糕！"


translate chinese day5_main3_46d149a2:


    me "那个...面粉，糖，各种材料..."


translate chinese day5_main3_61fefa63:


    th "这就是个奇怪的问题了，她不知道蛋糕是什么做的吗？"


translate chinese day5_main3_0fa8d81f:


    dreamgirl "这些你都有了吗？"


translate chinese day5_main3_c8b42142:


    me "是啊，大概是的。"


translate chinese day5_main3_749fd519:


    dreamgirl "还有糖？"


translate chinese day5_main3_e2a3a4fa:


    me "还有糖..."


translate chinese day5_main3_b206627b:


    dreamgirl "你可以借给我一点吗？"


translate chinese day5_main3_7332342b:


    me "做什么？"


translate chinese day5_main3_9d2d461d:


    "我想可能是太多了。"


translate chinese day5_main3_15d24d3a:


    "突然一阵风吹来，让我本能的抓住手推车，然后睁开了眼睛。"


translate chinese day5_main3_0cf7617c:


    "不过这里没有人。"


translate chinese day5_main3_195802d1:


    th "我是在做白日梦吗？"


translate chinese day5_main3_0ee6c06e:


    "不过我注意到糖口袋被打开了，并且倒出了一些。"


translate chinese day5_main3_2fb04556:


    th "是不是她被风吓到然后跑开了呢...?"


translate chinese day5_main3_5e73ac52:


    "整理好袋子之后，我站起来继续我的草莓之路。"


translate chinese day5_main3_d3d42b9c:


    "食堂附近一个人都没有——确实，晚饭还有一个小时。"


translate chinese day5_main3_1b6a6dde:


    "我把手推车运到后门然后卸货给食堂的厨师。"


translate chinese day5_main3_17448fb7:


    "她应该已经知道这个消息了，而且她不爽的看着我，不知道烤一个蛋糕需要多长时间，不过似乎她得抓紧了。"


translate chinese day5_main3_088c8385:


    "剩下的时间我只想休息。"


translate chinese day5_main3_294475af:


    "简单的说我太累了，所以干脆坐在台阶上等着。"


translate chinese day5_main3_89ce1a6f:


    "我的眼睛自己就闭上了，我累的没有注意到有人过来，知道有人拍了我的肩膀。"


translate chinese day5_main3_a0a72914:


    mi "嗨!"


translate chinese day5_main3_7629afc5:


    "未来站在我的面前。"


translate chinese day5_main3_b5cc7e1c:


    me "啊..."


translate chinese day5_main3_6aa1252d:


    "我不需要镜子就知道自己脸上不耐烦的表情。"


translate chinese day5_main3_11ad29bb:


    mi "啊，抱歉，我一定打扰到你了..."


translate chinese day5_main3_df78914f:


    me "没关系，我只是坐在这里。"


translate chinese day5_main3_618d7741:


    mi "啊，那好吧！"


translate chinese day5_main3_1af920ee:


    "未来喜形于色。"


translate chinese day5_main3_3f7156f6:


    mi "我来吃晚饭，我觉得时间已经到了，但是表上显示的现在还太早了，但是我还是想要检查一下，也许是我的表坏了呢。"


translate chinese day5_main3_bf5ddeb1:


    mi "啊，不是表，表错不了，我是说会不会是我看错表了呢..."


translate chinese day5_main3_3c421d62:


    "她好像陷入了迷糊状态，正在停下来思考。"


translate chinese day5_main3_7fe5f005:


    me "距离晚饭还有半个小时。"


translate chinese day5_main3_8824776e:


    mi "那太好了，如果你不介意的话，就让我在这里和你一起等吧？"


translate chinese day5_main3_203abf9f:


    "说实话我很介意。"


translate chinese day5_main3_05c9aec0:


    me "那个，你知道的，我有一些事情..."


translate chinese day5_main3_e44244b3:


    "我迅速站起来离开，像平常一样无视未来。"


translate chinese day5_main3_b094f8aa:


    "过了一分钟我来到了广场，找到了一个好地方可以休息着等着食堂开饭。"


translate chinese day5_main3_da5773ae:


    "这是我这一整天里第一次有这种舒服的感觉。"


translate chinese day5_main3_de22be40:


    "我对于这些没有意义的烦人事不是很恼火，不，是很愤怒！"


translate chinese day5_main3_8b26ac2a:


    "我已经放弃思考我为什么在这里了。"


translate chinese day5_main3_9e4b97f1:


    "我也不在意如何离开了。"


translate chinese day5_main3_07d4e30c:


    "让我愤怒的是辅导员总是给我布置一些奇怪的毫无意义的任务，让我像个傻瓜一样。"


translate chinese day5_main3_bd134020:


    th "如果这是什么外星人的小把戏那么他们也该看看心理医生了。"


translate chinese day5_main3_13370091:


    "我咬咬牙，攥紧自己的拳头。"


translate chinese day5_main3_8ea911ef:


    th "而且最烦人的是所有的事都像是自然发生的！"


translate chinese day5_main3_e2dfefc7:


    th "幸好我不需要拿着一两吨的糖和面，不过那样我也没有办法！"


translate chinese day5_main3_12a8d8d9:


    th "我是说别的情况可能更糟…"


translate chinese day5_main3_60e71618:


    us "你在和谁生气啊？"


translate chinese day5_main3_4a8fcfb4:


    "乌里扬卡站在我的面前奇怪的笑着。"


translate chinese day5_main3_90b46808:


    me "其实没有谁…"


translate chinese day5_main3_12a0290a:


    "我恍惚的回答着。"


translate chinese day5_main3_4758c0c0:


    "但是我的拳头暴露了自己。"


translate chinese day5_main3_be723785:


    me "就这样…"


translate chinese day5_main3_40bd06ac:


    us "好吧好吧，你决定…{w}你最好告诉我为什么一整天拿着各种袋子跑来跑去！"


translate chinese day5_main3_d8e5e6d0:


    me "我必须这样做。"


translate chinese day5_main3_d20f4795:


    "我不情愿的回答着。"


translate chinese day5_main3_a0086905:


    us "我觉得那是吃的。"


translate chinese day5_main3_726459ba:


    me "也许是的…"


translate chinese day5_main3_28b2648c:


    "乌里扬娜正准备说什么，突然铃响了，呼唤少先队员们去吃饭。"


translate chinese day5_main3_f1fc33c2:


    "我放松的叹了口气，跑向食堂，把乌里扬卡扔在了后面。"


translate chinese day5_main3_1d7b62f5:


    mt "感谢你，谢苗！"


translate chinese day5_main3_9508d41b:


    me "感谢什么？"


translate chinese day5_main3_92e433aa:


    "辅导员友好的对我笑着。"


translate chinese day5_main3_f601ce01:


    mt "当然是为了蛋糕的事情了！"


translate chinese day5_main3_707cf821:


    me "啊，是啊..."


translate chinese day5_main3_e512b012:


    "这个时候我才明白“不用谢”是什么意思。"


translate chinese day5_main3_374f0622:


    mt "你没有告诉任何人吗？这应该是一个惊喜！"


translate chinese day5_main3_9513cd87:


    me "是啊..."


translate chinese day5_main3_6a31846d:


    mt "好孩子！现在去吃完饭吧！"


translate chinese day5_main3_937d7bb8:


    "奥尔加·德米特里耶夫娜挥挥手，指着食堂方向。"


translate chinese day5_main3_5452915f:


    "我小心的走过食堂，寻找着座位。"


translate chinese day5_main3_f6bdde48:


    "结果有很多空余的座位，我可以找到一个人的地方。"


translate chinese day5_main3_2061d557:


    "晚饭有鱼和土豆泥。"


translate chinese day5_main3_062530c8:


    th "真是不幸，我又要饿肚子了，而且我们中午不是刚刚吃过鱼吗...?"


translate chinese day5_main3_0e40ce73:


    th "今天是吃鱼日吗？！"


translate chinese day5_main3_ce79b819:


    "我推开盘子趴在桌子上。"


translate chinese day5_main3_d13c0f93:


    "不过很快有人走了过来。"


translate chinese day5_main3_44b645c0:


    sl "嘿，你还好吗？"


translate chinese day5_main3_84662d7a:


    me "还好..."


translate chinese day5_main3_35836741:


    "我一动不动的回答着。"


translate chinese day5_main3_cb89aafa:


    sl "累了吗？"


translate chinese day5_main3_bf1c2987:


    me "是啊，有点..."


translate chinese day5_main3_17c764f2:


    sl "那太不幸了。"


translate chinese day5_main3_81e2449f:


    "斯拉维娅严肃的说着。"


translate chinese day5_main3_17c6b21c:


    me "当然…"


translate chinese day5_main3_4fb291b0:


    sl "你知道咱们吃过晚饭之后还要远足的吧？你准备好所有的东西了吗？"


translate chinese day5_main3_25632213:


    me "什么？去哪儿？"


translate chinese day5_main3_9830e8f7:


    "我马上睁开眼，抬起来头。"


translate chinese day5_main3_bf312592:


    "列娜站在斯拉维娅的旁边。"


translate chinese day5_main3_cd82f264:


    sl "远足…"


translate chinese day5_main3_a4614188:


    "她很惊讶。"


translate chinese day5_main3_1acd2585:


    sl "你不知道吗？"


translate chinese day5_main3_5cb822ee:


    me "不..."


translate chinese day5_main3_63edcced:


    "我抱着头又趴在了桌子上。"


translate chinese day5_main3_72338241:


    th "要是我可以陷到地里去…"


translate chinese day5_main3_34d8cd0a:


    "女孩子们沉默着。"


translate chinese day5_main3_cfa6c6ec:


    "我自己思考了一会儿。"


translate chinese day5_main3_cdac6a32:


    "也许我就可以这样的待到晚饭以后，不过奥尔加·德米特里耶夫娜的声音从食堂对面传来。"


translate chinese day5_main3_0b4ca722:


    mt "孩子们!为了庆祝营救舒里克的活动，我们为你们准备了这个蛋糕！"


translate chinese day5_main3_639a506c:


    "我漫不经心的抬起头，不过除了少先队员们的背影什么也看不到。"


translate chinese day5_main3_c6fcef15:


    mt "等一下，等一下…"


translate chinese day5_main3_a783f6df:


    th "跟我一点关系都没有！我救了舒里克，我收集了各种原料…"


translate chinese day5_main3_6400b884:


    th "好像就应该这样似的。"


translate chinese day5_main3_df46be92:


    th "哈，对辅导员有所期待真是可笑。"


translate chinese day5_main3_6ab86cd0:


    sl "咱们走吧，不然就什么都没有了。"


translate chinese day5_main3_c17a722d:


    "斯拉维娅笑着。"


translate chinese day5_main3_a8b5b91a:


    un "走。"


translate chinese day5_main3_c1956a30:


    "列娜表示同意。"


translate chinese day5_main3_3b03af3a:


    me "是啊，当然..."


translate chinese day5_main3_a2f9d7eb:


    "我拖着身子站起来。"


translate chinese day5_main3_f95eb2e4:


    "当我们接近的时候，奥尔加·德米特里耶夫娜刚刚把蛋糕放在桌子上。"


translate chinese day5_main3_9e9a688f:


    mt "那么现在..."


translate chinese day5_main3_902ab03d:


    "辅导员还没来得及弄完，乌里扬卡就冲了过去。"


translate chinese day5_main3_44ef2910:


    "她在被拖走之前设法咬掉了好几口。"


translate chinese day5_main3_3ec376bc:


    "她不停地踢着，喊叫着。"


translate chinese day5_main3_72ace9f9:


    "我呆呆的看着这幅画面：阿丽夏笑着，列娜用手蘸着奶油，周围是愤怒的少先队员们。"


translate chinese day5_main3_7f98185e:


    "我感觉自己不在这个世界里，好像我闭上眼睛再睁开就能回到我自己公寓的小屋里。"


translate chinese day5_main3_f569f26d:


    "我眨了眨眼，但是什么都没有发生，只有吵闹声更加烦人了。"


translate chinese day5_main3_87959490:


    mt "乌里扬娜！你过分了！过分了！"


translate chinese day5_main3_34125318:


    us "我...{w}我只是..."


translate chinese day5_main3_6429cb93:


    "嗯，这种行为即使对于她来说也有些过了。"


translate chinese day5_main3_d677809e:


    "舒里克加入了对话—或者说是审判？"


translate chinese day5_main3_58d5ebe0:


    sh "奥尔加·德米特里耶夫娜，请原谅她，我们只是想庆祝我回来，并不是什么大事..."


translate chinese day5_main3_a6820f71:


    "他犹豫了一下。"


translate chinese day5_main3_57d1ba21:


    mt "那没有关系！"


translate chinese day5_main3_aac2b1f6:


    "辅导员转向乌里扬娜。"


translate chinese day5_main3_33bbaf19:


    mt "还有你，我今天要好好修理修理你，好让你学会正确的行为举止！"


translate chinese day5_main3_935bc568:


    us "啊，随便你！"


translate chinese day5_main3_99aa653e:


    "她转过身来。"


translate chinese day5_main3_cd01cddf:


    mt "你今晚不会和我们一起远足！"


translate chinese day5_main3_2babf74f:


    us "好像我想去似的！"


translate chinese day5_main3_54e5777a:


    "我真想和乌里扬卡交换一下，不过谁知道呢..."


translate chinese day5_main3_038c58cb:


    "如果我提前知道的话，我就是第一个上去打碎蛋糕的人！"


translate chinese day5_main3_a4bcbbe9:


    "又过了几分钟，少先队员们开始逐渐散开。"


translate chinese day5_main3_c997a5fb:


    mt "你也要准备好，再过半个小时就要集合了。"


translate chinese day5_main3_80aff84c:


    "我直直的看着辅导员，想要通过眼神告诉她我的立场。"


translate chinese day5_main3_a89a2c76:


    mt "别晚了！"


translate chinese day5_main3_fd82295b:


    "我前往出口的时候乌里扬卡正坐在桌子上。"


translate chinese day5_main3_94be5d8c:


    me "所以说你为什么那样做？"


translate chinese day5_main3_4d3873eb:


    "她看起来很失落，不过这也是有原因的。"


translate chinese day5_main3_500104f1:


    us "我想要那样做。"


translate chinese day5_main3_e8371c45:


    "乌里扬娜粗鲁的回答着。"


translate chinese day5_main3_017bb18f:


    me "所以你现在高兴了吗？"


translate chinese day5_main3_2ddb5b95:


    us "当然！还有你！祝你远足幸运！"


translate chinese day5_main3_72d5c9fe:


    "她淘气的笑着跑出了食堂。"


translate chinese day5_main3_b53a9fd1:


    th "哼，幸运一些也没有什么坏处..."


translate chinese day5_main3_a9aa1a31:


    "去辅导员房间的路上，我的大脑中一直回响着“路要考自己走”的话。"


translate chinese day5_main3_831c71df:


    "不知道为什么我就是不想吵架或者装病什么的。"


translate chinese day5_main3_f85ee780:


    "今晚的事情让我学会了顺从，虽然我经常遇到这些没有道理的事情。"


translate chinese day5_main3_4da475d5:


    "当我进门的时候，我有了一点想法。"


translate chinese day5_main3_96c41623:


    th "事实上，我应该准备什么呢？"


translate chinese day5_main3_9679533e:


    th "衣服？我反正也没有什么衣服，不过总之我忘了问这个远足是不是会持续一整个晚上什么的。"


translate chinese day5_main3_fd2a6187:


    "我想不出什么更好的主意，所以我拿上了刚刚来到这里时穿的毛衣抵御可能的寒冷，直接奔着广场去了。"


translate chinese day5_main3_ca17b192:


    "几乎整个营地的人都在这里了，虽然比奥尔加·德米特里耶夫娜指示的时间还要早上十分钟。"


translate chinese day5_main3_1e295624:


    "我在边缘的位置停下来耐心的等着。"


translate chinese day5_main3_fd3eb8d2:


    "夜晚降临了。"


translate chinese day5_main3_d20ed5d2:


    th "从外面看的话真是一幅可笑的画面：一群站得整整齐齐的少先队员们，等着铜人Genda发号施令。"


translate chinese day5_main3_018d282a:


    th "这一切都发生在潮红的日落中。"


translate chinese day5_main3_b7cd9dd7:


    "接下来就是他带领着戴着红领巾的少先队员们冲锋陷阵…"


translate chinese day5_main3_18d7dd2e:


    "不过事实上说话的是奥尔加·德米特里耶夫娜。"


translate chinese day5_main3_553eaa56:


    mt "看来大家都在这里了...很好！"


translate chinese day5_main3_a8c428b5:


    "我今天实在太累所以只能听着辅导员讲话。"


translate chinese day5_main3_443f99aa:


    mt "现在我们要开始远足了！"


translate chinese day5_main3_747aae61:


    mt "关键时刻少先队员要懂得互相帮助！"


translate chinese day5_main3_fc6094ea:


    mt "我们要学会合作！"


translate chinese day5_main3_fc0d9c17:


    "人群中有悄悄话传来传去，说远足的目的地是不远处的森林。"


translate chinese day5_main3_038d0ddf:


    "不知道为什么我也是这样想的。"


translate chinese day5_main3_81d86d37:


    mt "我们会两人一组行动，所以如果你还没有找到伴，现在最好开始行动了！"


translate chinese day5_main3_30d15b67:


    "少先队员们马上开始动起来。"


translate chinese day5_main3_ae8616af:


    "看起来我好像是唯一一个没有同伴的人。"


translate chinese day5_main3_a9a03604:


    "斯拉维娅在和奥尔加·德米特里耶夫娜热情的讨论着什么，列娜和未来，电子小子，当然是和舒里克。"


translate chinese day5_main3_aea0ad16:


    th "哎，一个人走也未必是坏事。"


translate chinese day5_main3_105fc865:


    mt "谢苗!"


translate chinese day5_main3_4e47d15f:


    "辅导员的声音让我惊醒。"


translate chinese day5_main3_92f6d31e:


    "我不情愿的走过去。"


translate chinese day5_main3_8dfde425:


    mt "你好像没有找到同伴。"


translate chinese day5_main3_d3242212:


    me "看起来是这样的..."


translate chinese day5_main3_d1a7c5c7:


    mt "那么你就去找热妮娅吧——她也是自己。"


translate chinese day5_main3_27ad27f5:


    "我感受到一种独特的孤独。"


translate chinese day5_main3_b7994624:


    "所以我不得不和一个给我钱也不想在一起的图书管理员在一起？"


translate chinese day5_main3_b73326a6:


    th "虽然说我们现在是在一条船上了..."


translate chinese day5_main3_d17a396e:


    "我慢慢接近热妮娅。"


translate chinese day5_main3_6818c128:


    me "我想咱们要在一起..."


translate chinese day5_main3_c25dd9ca:


    "她看着我。"


translate chinese day5_main3_0af22cb1:


    mz "你觉得我很高兴吗？！"


translate chinese day5_main3_44ba86f4:


    "热妮娅严肃的说着。"


translate chinese day5_main3_5122493d:


    me "你为何要高兴？"


translate chinese day5_main3_d7b5efd3:


    "我天真的问道。"


translate chinese day5_main3_d0a8dd78:


    mz "算了，你最好闭嘴。"


translate chinese day5_main3_f4dcd652:


    th "这样真是好..."


translate chinese day5_main3_b28b669b:


    "她转过去继续跟着队伍。"


translate chinese day5_main3_e32f998e:


    "我仍然没有发现结对走的好处。"


translate chinese day5_main3_34fb045a:


    "总之我们走的是一条痕迹清晰的小路，这样的地方就算是想要迷路也很难。"


translate chinese day5_main3_d1f6b652:


    "而且我们已经走了半个小时，然而并没有向森林深处走多远，相反我们好像是在绕圈子。"


translate chinese day5_main3_946c8923:


    "不过想想领队是奥尔加·德米特里耶夫娜，这次的旅程就像是霍比特人前往魔多..."


translate chinese day5_main3_97574c1e:


    "我按照热妮娅的意思和她保持一定的距离。"


translate chinese day5_main3_41f63256:


    "这位图书管理员似乎很满意。"


translate chinese day5_main3_6afab6eb:


    me "我说，你不知道咱们什么时候能够抵达吗？"


translate chinese day5_main3_67e23fdb:


    mz "咱们什么？"


translate chinese day5_main3_88370e84:


    me "咱们在哪里扎营啊？"


translate chinese day5_main3_059aa439:


    mz "你不明白这次的远足活动不是为了扎营！而是为了远足本身！"


translate chinese day5_main3_97b64a58:


    "是啊，看来我对于远足一点也不了解..."


translate chinese day5_main3_4587e3c3:


    me "你大概是对的，不过…"


translate chinese day5_main3_bb86bd36:


    mz "我不知道！"


translate chinese day5_main3_75f80dfe:


    "她愤怒的回答着，然后加快了脚步。"


translate chinese day5_main3_133cde0a:


    "我追上她问道："


translate chinese day5_main3_e4b066d7:


    me "我说，你为什么总是..."


translate chinese day5_main3_17313088:


    "我想说“刻薄”，不过停顿了一下。"


translate chinese day5_main3_6687b26f:


    me "我没有对你做任何坏事，而且我也不会做的！"


translate chinese day5_main3_efd76d15:


    "她惊讶的看着我。"


translate chinese day5_main3_296b44b5:


    mz "什么？!"


translate chinese day5_main3_231d5bf5:


    me "不善交际...有一点...也许是因为我？"


translate chinese day5_main3_da119946:


    mz "算了别再说那些愚蠢的话了！"


translate chinese day5_main3_8b13fe9b:


    me "如你所愿..."


translate chinese day5_main3_39a4936f:


    "我决定剩下的时间不再和她争论。"


translate chinese day5_main3_a20cefa7:


    "..."


translate chinese day5_main3_66653ce7:


    "最后奥尔加·德米特里耶夫娜终于决定结束这无间道。"


translate chinese day5_main3_f2e5e7d4:


    mt "该停下了。"


translate chinese day5_main3_e9d2363e:


    "T这里原来是森林中的一大片空地，还有临时凉棚，烧过的营火。"


translate chinese day5_main3_d3f88135:


    "显然这个远足是这个夏令营的传统。"


translate chinese day5_main3_e664fb77:


    "我和其他的男孩子一样陪派出去寻找柴火。"


translate chinese day5_main3_5463f5b6:


    "这不需要很长的时间因为到处都是树枝什么的。"


translate chinese day5_main3_dcb20eaf:


    "最后奥尔加·德米特里耶夫娜用一些旧报纸点燃了营火。"


translate chinese day5_main3_df660aed:


    "我很想知道上面写了什么，不过除了苏联的标志什么也看不出来。"


translate chinese day5_main3_44bc05c0:


    "少先队员们开始坐下来聊天。"


translate chinese day5_main3_0b4fac65:


    "看来这项活动的终极目的达到了。"


translate chinese day5_main3_c751793a:


    th "唯一缺少的就是一大锅鱼汤，铝壶装的伏特加，还有吉他。"


translate chinese day5_main3_a7859a66:


    "不过就算是现在突然出现这些东西我也不会奇怪的。"


translate chinese day5_main3_29dcd13b:


    sl "你在想什么？"


translate chinese day5_main3_a6893681:


    "斯拉维娅坐在我的旁边。"


translate chinese day5_main3_60a79f40:


    me "啊，没什么特别的，只是在享受远足。"


translate chinese day5_main3_27c62366:


    "我讽刺的说道。"


translate chinese day5_main3_5e83eb9a:


    sl "你好像不是很高兴啊。"


translate chinese day5_main3_f8cc5c73:


    me "我没有高兴的跳起来，还真对不起啊。"


translate chinese day5_main3_8f1735d3:


    sl "好啦，我不会烦你了。"


translate chinese day5_main3_70009934:


    "她在我这里坐了一会儿，不过在发现我没有那个心情之后就离开了，留下我一个人在这里自省。"


translate chinese day5_main3_81fad12b:


    "我现在唯一想做的事情就是躺下睡觉，然而取而代之的是呛人的烟气还有乱哄哄的少先队员们。"


translate chinese day5_main3_9aaac94d:


    "他们欢呼雀跃，谈笑风生，享受着仲夏夜。"


translate chinese day5_main3_2803f4ab:


    "我看着空地的那一边列娜和阿丽夏吵得很凶。"


translate chinese day5_main3_d93c3f94:


    "“凶”和列娜在我看来完全不能相容。"


translate chinese day5_main3_193e4c57:


    "看起来斯拉维娅在结束我们的对话以后去了不知道什么地方。"


translate chinese day5_main3_37441d60:


    "电子小子和舒里克正在绞尽脑汁说服奥尔加·德米特里耶夫娜什么事情。"


translate chinese day5_main3_3257d53a:


    "好像只有我不属于这里。"


translate chinese day5_main3_a4f8dc02:


    th "不过说起来我为什么要在乎呢？"


translate chinese day5_main3_cb94daa6:


    "我要做的就是看着营火。"


translate chinese day5_main3_c8d1a036:


    th "有一种说法就是一个人可以永远的看着它，就像是看着流水一样。"


translate chinese day5_main3_68d47823:


    "不过那里似乎还有别的东西…"


translate chinese day5_main3_1d9e5974:


    th "那是什么？"


translate chinese day5_main3_29414842:


    mt "一个人可以永远的看着三样东西：燃烧的火焰，流动的清水，还有正在工作的别人。"


translate chinese day5_main3_903e62de:


    "辅导员把我从白日梦中拉了回来。"


translate chinese day5_main3_31151d59:


    mt "谢苗，你不觉得现在休息太早了吗？"


translate chinese day5_main3_0a50a8a2:


    me "但是我还需要做什么？"


translate chinese day5_main3_435b54b7:


    "我真的不知道奥尔加·德米特里耶夫娜想让我做什么。"


translate chinese day5_main3_2c3aefa4:


    mt "我不知道…"


translate chinese day5_main3_2ebab91c:


    "她停顿了一下。"


translate chinese day5_main3_3e0f4283:


    mt "不过如果要做的话，那就毫不犹豫地去做。"


translate chinese day5_main3_fa103e1d:


    "她意义不明的笑着，然后去给篝火添了一些树枝。"


translate chinese day5_main3_8045c9cb:


    "说完这些话以后我完全确定她把我当成是苦工。"


translate chinese day5_main3_5d460c3e:


    "或者至少是免费劳动力，这个本质上没有什么区别的说法。"


translate chinese day5_main3_5618085a:


    "我叹叹气，用手托着脑袋，盼望着今天的苦工已经干完…"


translate chinese day5_main3_a582c672:


    "有人拍了拍我的肩膀。"


translate chinese day5_main3_008bd100:


    "我抬头看看，发现是电子小子和舒里克。"


translate chinese day5_main3_132aeab6:


    me "你们想要什么？"


translate chinese day5_main3_b037ba1a_1:


    "我懒洋洋的问着。"


translate chinese day5_main3_a3f9f456:


    el "别伤心！"


translate chinese day5_main3_8afd5934:


    me "还有什么更有意思的事情吗？"


translate chinese day5_main3_a2ade9c9:


    sh "你看，我们在讨论机器人社团的发展问题…"


translate chinese day5_main3_13f2497e:


    sh "问题就在这里了，我们需要更多人手，如果你…"


translate chinese day5_main3_a6820f71_1:


    "他犹豫了。"


translate chinese day5_main3_ac13c40b:


    th "“发展”这个词语和他们可是不相容的。"


translate chinese day5_main3_b77b4d76:


    "我什么也没有说，然后看了看周围的少先队员们。"


translate chinese day5_main3_eb10acd3:


    sh "嗯?"


translate chinese day5_main3_b9a3cb6a:


    me "我没有时间....{w}你们没有发现我从来都是在忙活辅导员的差事吗？"


translate chinese day5_main3_74081b6b:


    sh "是啊，我想你是对的，今天的事情很尴尬啊，关于乌里扬娜。"


translate chinese day5_main3_a3bf9ccf:


    "我惊讶的看着他。"


translate chinese day5_main3_3e7b5ad7:


    th "舒里克好像为了蛋糕的事情很自责。"


translate chinese day5_main3_8043b470:


    me "是啊…"


translate chinese day5_main3_5bb39a9d:


    "少先队员们都在这里了，但是我好像看不到斯拉维娅的身影。"


translate chinese day5_main3_35484a41:


    sh "我觉得她生我的气了…"


translate chinese day5_main3_ad02f53a:


    me "谁?"


translate chinese day5_main3_fad6f3d4_1:


    "我不经意的问着。"


translate chinese day5_main3_948b648b:


    sh "乌里扬娜。{w}也许我应该道歉？"


translate chinese day5_main3_7fe19203:


    me "不，那不是你的错…"


translate chinese day5_main3_53bddd68:


    "我们静静的坐了一会儿，然后我站起来说："


translate chinese day5_main3_37f21c35:


    me "我的腿有点麻了，我得走走。"


translate chinese day5_main3_d7826409:


    "他们没有回答。"


translate chinese day5_main3_d9cf7a94:


    "我围着营地绕着圈子，发现辅导员盯着我盯得很紧。"


translate chinese day5_main3_3b6642f3:


    "看来她等不及要给我派一些任务了。"


translate chinese day5_main3_0724ca3e:


    "我在哪里都找不到斯拉维娅。"


translate chinese day5_main3_a3f3fccb:


    th "也许我应该去找找她？"


translate chinese day5_main3_84b23eff:


    "另一方面我每次想到乌里扬卡失望的表情就感觉不舒服。"


translate chinese day5_main3_d76fc57e:


    th "也许这次的远足不是那么有意思，不过自己坐在这里也不见得好多少。"


translate chinese day5_main3_016c7fc0:


    "不过现在我哪里也不想去…"


translate chinese day5_main3_3e703e80:


    "我确定斯拉维娅没有问题，所以我就在这里待着了。"


translate chinese day5_main3_d37bbb9f:


    th "不过另一方面我为什么要管呢？"


translate chinese day5_main3_f6cc82fd:


    th "我觉得今天已经够了。"


translate chinese day5_main3_c58cdcde:


    "我回到座位上等待着这次活动的结束，几乎可以感觉到辅导员落在我身上的目光。"


translate chinese day5_main3_5b5fdf66:


    "最后她站起来说道："


translate chinese day5_main3_85ade4d1:


    mt "现在咱们玩Cities!"


translate chinese day5_main3_79d3e6a7:


    "我不反对这个游戏。"


translate chinese day5_main3_9432dd18:


    th "不过显然因为这个我们的远足活动会变得更长…"


translate chinese day5_main3_6f4a9363:


    "少先队员们围着营火坐下来。"


translate chinese day5_main3_1dff3b67:


    "我注意到列娜和阿丽夏，他们坐在我对面的树桩上。"


translate chinese day5_main3_f58cf444:


    th "看起来一切都还好。"


translate chinese day5_main3_7db4f674:


    "几分钟以前我还有完全不同的感觉呢。"


translate chinese day5_main3_9f3fc574:


    th "不过一切皆有可能…"


translate chinese day5_main3_74340d5d:


    "我倒是很想问问她们在吵些什么，不过现在不太可能了，我现在只感觉越来越累。"


translate chinese day5_main3_6615be77:


    "我的大脑空空的。"


translate chinese day5_main3_99ab0d84:


    "说得准确一些，我的头很沉，没有地方装那些想法了。"


translate chinese day5_main3_e447cec9:


    "平常我的大脑就像是高速公路，无数的想法在上面飞奔然后撞个稀烂，但是现在它就像是一条隐蔽的林中小路，除非是特殊的情况，否则就完全不通了。"


translate chinese day5_main3_3b240839:


    "斯拉维娅没有回来。"


translate chinese day5_main3_b00a6ffe:


    th "也许她有什么事？"


translate chinese day5_main3_5147a728:


    "不过说起来现在并没有办法找她。"


translate chinese day5_main3_3f2cf4ef:


    mt "好了，咱们开始吧！莫斯科！"


translate chinese day5_main3_dad4cc7b:


    "少先队员们开始接城市的名字。"


translate chinese day5_main3_3f3598ba:


    "最后轮到我了。"


translate chinese day5_main3_58df1a5a:


    "我仔细听着，试着找到我要说的第一个字母。"


translate chinese day5_main3_d1d876fd:


    me "阿尔汉格尔斯克。"


translate chinese day5_main3_f374397d:


    "我们玩了好几轮。"


translate chinese day5_main3_0759a0f8:


    "每一个新的名字都让我越来越难以记住之前提到的东西。"


translate chinese day5_main3_e27c9e7a:


    "我的注意力完全淹没在了这些大都市还有小乡村的名字中了。"


translate chinese day5_main3_b45788c7:


    mt "谢苗！谢苗！轮到你了！"


translate chinese day5_main3_b4c8d23c:


    "奥尔加·德米特里耶夫娜把我带回了现实。"


translate chinese day5_main3_60202c5c:


    me "啊，抱歉...刚刚的最后一个名字是什么？"


translate chinese day5_main3_cb702f2c:


    mt "你又在在白日梦了！是塞瓦斯托波尔。"


translate chinese day5_main3_25856564:


    me "好吧，那我就说伦敦..."


translate chinese day5_main3_9affe25e:


    mt "已经说过了。"


translate chinese day5_main3_bad80ec1:


    me "那好吧..."


translate chinese day5_main3_63447575:


    "我陷入了沉思。世界上以“L”开头的城市名字很多，不过我现在一个也想不起来。"


translate chinese day5_main3_b6595b35:


    me "利物浦？"


translate chinese day5_main3_6bbc1d1e:


    mt "有了！"


translate chinese day5_main3_b9a2a957:


    me "洛杉矶？"


translate chinese day5_main3_4c5275f0:


    mt "啊，终于！"


translate chinese day5_main3_e7e84af8:


    "她不屑的看着我，然后继续。"


translate chinese day5_main3_fca355af:


    "我实在是想不出下一个这样的名字了，不过幸运的是这是最后一轮了。"


translate chinese day5_main3_4e97d333:


    mt "好吧，今天就到这里了，时间不早了，咱们回去吧！"


translate chinese day5_main3_99742b57:


    "我放松的叹了口气。"


translate chinese day5_main3_5e1a747b:


    "回去的路上我们自由的走着。"


translate chinese day5_main3_d3fc10d2:


    "夜晚降临了，一个寻常的普通的夜晚。"


translate chinese day5_main3_46ada0f2:


    "是那种普通的黑夜，星星月亮都没有什么特殊的感觉，鸟叫，虫鸣，都没有什么特殊之处。"


translate chinese day5_main3_a20cefa7_1:


    "..."


translate chinese day5_main3_604b0d7d:


    "没过几分钟所有的少先队员们就都站在广场上了。"


translate chinese day5_main3_5e7a97d2:


    "现在已经很晚了，而且大家都很疲惫，所以队站得并不好。"


translate chinese day5_main3_26bc47b6:


    "在我看来这更像是一支维京人的部队刚刚打完了胜仗，只想刚快回家而不是怎样站出整齐的队列。"


translate chinese day5_main3_86ecb47a:


    "不过别人看起来也许更像是一支打了败仗的部队，耗尽最后的力气苟延残喘的回到了家乡。"


translate chinese day5_main3_75fc1be9:


    mt "感谢大家！现在去睡觉吧！已经很晚了。"


translate chinese day5_main3_539072ed:


    "少先队员们散开了，我也跟着辅导员走着。"


translate chinese day5_main3_ca1d4f17:


    mt "咱们也该走了！"


translate chinese day5_main3_006c39f4:


    "我们在路上保持着沉默。"


translate chinese day5_main3_79ed0be1:


    mt "全体睡觉！"


translate chinese day5_main3_899c3f81:


    "她说完关上了灯。"


translate chinese day5_main3_ae190c31:


    "我辗转反侧，回忆着一整天的事情。"


translate chinese day5_main3_8212c1e8:


    "我虽然终于可以休息了，但是那种担心说错了什么的心情却挥之不去..."


translate chinese day5_main3_a6f64aad:


    "这种好像没有完成什么的感觉让我很难受。"


translate chinese day5_main3_9cd093b9:


    "现在大概是两点钟。"


translate chinese day5_main3_7962d319:


    "坏事总会结束的..."


translate chinese day5_main3_d3e44219:


    "我睡着了。"


translate chinese day5_dv_b3a56e2b:


    "所有的这些美妙中只有列娜和阿丽夏与众不同。"


translate chinese day5_dv_9494f7ea:


    "阿丽夏和别人吵成那个样子倒是很正常。"


translate chinese day5_dv_49a0c8bc:


    "不过听到列娜提高声音说话..."


translate chinese day5_dv_9844ee70:


    "我慢慢的靠近，试图听清她们说了什么。"


translate chinese day5_dv_6ca87438:


    dv "不，你听我说！"


translate chinese day5_dv_76e80067:


    un "随便你怎么想！该说的我都说了！"


translate chinese day5_dv_d1acba6b:


    th "看起来很严重。"


translate chinese day5_dv_7d833908:


    "我尝试着不去注意她们，结果我就站在那里什么也没有听进去。"


translate chinese day5_dv_65a0ca80:


    dv "没有什么可想的，所有的一切都很清楚！"


translate chinese day5_dv_08837eb1:


    dv "别想糊弄我，我可是很了解你的！"


translate chinese day5_dv_85d1fa2f:


    un "对啊，你什么都知道，你为什么自己不去告诉他？"


translate chinese day5_dv_cf4dd80f:


    "列娜很生气，这本身就很奇怪，虽然我不知道她们在吵什么。"


translate chinese day5_dv_7dfc2afa:


    dv "那和你没有关系！"


translate chinese day5_dv_12152c43:


    "阿丽夏扭过头，然后看到了我。"


translate chinese day5_dv_b91f8b82:


    "不一会儿，列娜也看到了我。"


translate chinese day5_dv_aeac4053:


    "两个女孩子不解的站在那里。"


translate chinese day5_dv_a22d002f:


    dv "你在...{w}偷听?!"


translate chinese day5_dv_e808d0e5:


    me "我？不不不，我只是...路过..."


translate chinese day5_dv_61947ed2:


    "我努力做出最好的笑容，不过似乎不太见效。"


translate chinese day5_dv_f829601e:


    dv "对了，今天他偷看我！"


translate chinese day5_dv_31e62164:


    "阿丽夏阴险的笑着。"


translate chinese day5_dv_74229088:


    "列娜疑惑的看着我。"


translate chinese day5_dv_57da622c:


    th "首先，她为什么要相信这个恶魔的话？"


translate chinese day5_dv_d122756b:


    me "我没有偷看！{w}你很清楚那是一个事故！意外！要怪就怪乌里扬卡！"


translate chinese day5_dv_f4cf67d8:


    dv "那当然——你去和警察说去吧！"


translate chinese day5_dv_b0cbdad9:


    th "看来我的借口不太管用。"


translate chinese day5_dv_93816b7f:


    dv "你喜欢我的胸部吗？"


translate chinese day5_dv_169666e8:


    "我感到一阵寒气。"


translate chinese day5_dv_8bfbf78c:


    un "那是...{w}真的吗?"


translate chinese day5_dv_b17dc3c0:


    "列娜哀求的看着我。"


translate chinese day5_dv_78824aab:


    me "那个，那是一个意外...我什么都没有看到。"


translate chinese day5_dv_7394ce16:


    dv "你没有？好吧，那我再给你看看！"


translate chinese day5_dv_a55bd08a:


    "阿丽夏愤怒的叫着。"


translate chinese day5_dv_a03136b3:


    "我不知道该说什么好。"


translate chinese day5_dv_c67e9088:


    dv "你看，我说吧！"


translate chinese day5_dv_709e3a50:


    un "不... 不..."


translate chinese day5_dv_0d581cb1:


    "列娜开始自言自语，然后一下子跑开了。"


translate chinese day5_dv_87a2f256:


    me "等一下，怎么回事？！"


translate chinese day5_dv_3da1183b:


    "我喊叫着追她。"


translate chinese day5_dv_0cd43818:


    dv "你看看你干了什么好事，那个孩子在哭！"


translate chinese day5_dv_31e62164_1:


    "她又露出阴险的微笑。"


translate chinese day5_dv_fab61109:


    me "首先让她不高兴的不是我，其次，我完全不知道你们在吵什么！"


translate chinese day5_dv_2b19dcc7:


    "我快要失去耐心了。"


translate chinese day5_dv_b578411d:


    dv "我为什么要告诉你？"


translate chinese day5_dv_65ea82d5:


    "她好像觉得我们的对话结束了，准备离开。"


translate chinese day5_dv_0c737116:


    "我粗暴的抓住了她的手。"


translate chinese day5_dv_6a189dfc:


    "阿丽夏害怕的看着我，但是什么也没有说。"


translate chinese day5_dv_277c9533:


    "我很生气！很生气！"


translate chinese day5_dv_f6618979:


    "首先我因为列娜为了我伤心而十分生气。"


translate chinese day5_dv_7c8a30ee:


    "第二，我被阿丽夏的傲慢激怒了。"


translate chinese day5_dv_513493ad:


    "第三，我很累，所以见到什么都想生气。"


translate chinese day5_dv_2bf8f738:


    me "你高兴了？"


translate chinese day5_dv_9c13f0cc:


    "我指着列娜逃掉的方向。"


translate chinese day5_dv_240840c7:


    me "你只会伤害无辜的少女。"


translate chinese day5_dv_f71a0fac:


    "她看起来很害怕，很难过，而且很困惑。"


translate chinese day5_dv_050fb154:


    dv "她，她可以伤害任何人，你不了解她！"


translate chinese day5_dv_73f6779b:


    me "我只是不了解她，但是我确定她不会伤害任何人！那是你！"


translate chinese day5_dv_03cd505c:


    "我们沉默了一会儿。"


translate chinese day5_dv_60d9b7af:


    "我攥着阿丽夏的手，不知道该干什么。"


translate chinese day5_dv_47ae85aa:


    "我突然想到了一个方法。"


translate chinese day5_dv_370eed20:


    me "你要去找她道歉！"


translate chinese day5_dv_f9f3ed76:


    dv "为什么是我？"


translate chinese day5_dv_fd492212:


    "阿丽夏想要向平常一样傲慢的应付过去，不过这次不行了。"


translate chinese day5_dv_7b8ee948:


    me "“因为我说了！”"


translate chinese day5_dv_983d54f9:


    "我拒绝接受任何理由，并且把她拉到篝火一边。"


translate chinese day5_dv_ff2ed8eb:


    me "奥尔加·德米特里耶夫娜，抱歉，她有一些个人问题，我们得提前离开。"


translate chinese day5_dv_7d32c910:


    "辅导员同志想要反驳，不过我没有管她，拉着阿丽夏前往列娜离开的方向。"


translate chinese day5_dv_a69e912e:


    "过了一会儿，我放开了她的手。"


translate chinese day5_dv_e687fb2b:


    me "你有什么想说的吗？"


translate chinese day5_dv_552eae04:


    "阿丽夏的脸上已经没有了傲慢。"


translate chinese day5_dv_5f5a4508:


    dv "如果你非要让我道歉的话，我就和你一起去，不过我没有什么可道歉的，我只是说出了事实。"


translate chinese day5_dv_f99126ea:


    me "咱们一会儿看看。"


translate chinese day5_dv_a767512c:


    "我打断了她。"


translate chinese day5_dv_29f6f60c:


    "我们一路上谁也没有说话，就这样走回了营地。"


translate chinese day5_dv_9cee08fa:


    "我们来到广场，我问阿丽夏："


translate chinese day5_dv_2442aa77:


    me "咱们接下来应该往哪里走？"


translate chinese day5_dv_37160fae:


    dv "我怎么会知道？"


translate chinese day5_dv_65c09781:


    me "刚刚是谁说过她很了解列娜？我吗？！"


translate chinese day5_dv_571e60de:


    "阿丽夏犹豫了。"


translate chinese day5_dv_8a9865ff:


    dv "那个，她可能在岛上，她想要一个人的时候就会到那里去。"


translate chinese day5_dv_a499094f:


    me "很好!"


translate chinese day5_dv_c8bd92fc:


    "我们前往码头。"


translate chinese day5_dv_cffd7334:


    "在我磨磨蹭蹭的找桨还有折腾绳子的时候，天逐渐变黑了。"


translate chinese day5_dv_07f664e2:


    "有一艘船不见了。"


translate chinese day5_dv_e631a0c0:


    me "好了，今天是你的幸运日，你有机会锻炼自己的二头肌，三头肌还有各种肌肉。"


translate chinese day5_dv_27c62366:


    "我讽刺的说道。"


translate chinese day5_dv_25a535c0:


    dv "你说真的？"


translate chinese day5_dv_883a35cc:


    "阿丽夏害怕的看着我。"


translate chinese day5_dv_183b833e:


    th "哇，可能这有点太难了，即使对她来说..."


translate chinese day5_dv_5b893c52:


    me "好吧，我不会让你划船的，当然..."


translate chinese day5_dv_1b5cc40c:


    "这听起来可一点没有自信。"


translate chinese day5_dv_36f1c607:


    "这次划起桨来可不是那么容易了。"


translate chinese day5_dv_a5ba40b7:


    "这不奇怪，我今天已经用完了一年的划船指标。"


translate chinese day5_dv_968f6af6:


    "我在河中间停下来喘口气。"


translate chinese day5_dv_ee0bd590:


    "营地被夜晚笼罩，一个普通的夜晚。"


translate chinese day5_dv_46ada0f2:


    "是那种普通的黑夜，星星月亮都没有什么特殊的感觉，鸟叫，虫鸣，都没有什么特殊之处。"


translate chinese day5_dv_4b0e60fa:


    "我盯着漆黑的夜晚，想要从中找到小岛的影子。"


translate chinese day5_dv_b4ddcd02:


    me "你是怎么了解她的？"


translate chinese day5_dv_3f85db61:


    "阿丽夏稍微颤抖了一下。"


translate chinese day5_dv_e3340992:


    dv "我们是一起长大的。"


translate chinese day5_dv_0b791288:


    "她犹豫了一下回答道。"


translate chinese day5_dv_feaeb3c3:


    me "然后在同一个夏令营，真是奇妙的巧合啊！"


translate chinese day5_dv_826081ef:


    "所有这些问题和谜都出现在了我的头脑中。"


translate chinese day5_dv_4cdcce8a:


    th "见鬼我现在就可以把她扔下去淹死。"


translate chinese day5_dv_8a51e645:


    th "我可以要求答案。"


translate chinese day5_dv_01dabe85:


    th "不过那样可能很危险除非我知道自己在做什么。"


translate chinese day5_dv_d79d8ce6:


    "不过我感觉自己这次有主动权。"


translate chinese day5_dv_9c1029af:


    me "我问你，这是什么夏令营？"


translate chinese day5_dv_acae4786:


    "阿丽夏呆呆的看着我。"


translate chinese day5_dv_38f0a76d:


    me "咱们在哪儿？我为什么在这里？"


translate chinese day5_dv_27b39f33:


    "她保持沉默。"


translate chinese day5_dv_46287afa:


    me "回答我！"


translate chinese day5_dv_91d35e95:


    "我喊了起来。"


translate chinese day5_dv_3f5fc648:


    dv "嘿，你怎么了？如果你想要让我对列娜道歉的话，我就去道歉好了..."


translate chinese day5_dv_6579c7ab:


    "我这个时候感觉自己真是愚蠢。"


translate chinese day5_dv_7113a55a:


    th "她可能和那件事...完全没关。"


translate chinese day5_dv_7ebb2614:


    th "而且，一个人也不太可能那样诚实的撒谎。"


translate chinese day5_dv_481f6bb0:


    "阿丽夏害怕的表情是另一个相信她的理由。"


translate chinese day5_dv_44b2f075:


    me "抱歉..."


translate chinese day5_dv_613ae970:


    "我只想出了这个词语。"


translate chinese day5_dv_2283e3ea:


    "不过我还是想要保持自己的优势。"


translate chinese day5_dv_8b39987d:


    "最后的几米很费劲。"


translate chinese day5_dv_1fa7fb2a:


    "我累极了，跳下船，躺在冰冷的草地上。"


translate chinese day5_dv_4942088d:


    "阿丽夏站在旁边看着我。"


translate chinese day5_dv_395406fd:


    "我觉得她要说点什么讽刺我的话，不过她还是呆呆的站在原地，所以我只好认为她受到的惊吓还没有过去。"


translate chinese day5_dv_7de75851:


    "虽然这里很暗，不过要藏在我家的小房间也比藏在这里容易。"


translate chinese day5_dv_8f34fcaa:


    "而且列娜肯定在我们渡河的时候听到了，所以现在再问阿丽夏问题就不太合适了，虽然我还有几个问题。"


translate chinese day5_dv_17717ee8:


    me "听着..."


translate chinese day5_dv_3b8d3ec7:


    "我看着她，但是什么也没有看见，天很黑，我又没有猫的眼睛。"


translate chinese day5_dv_0940755d:


    me "咱们找到列娜以后你告诉她你无意伤害她的感情，今天早晨发生的事情全都是误会，好吗？"


translate chinese day5_dv_cb693d1d:


    "我的声音没有几分钟以前那么有自信了，不过倒还是足够的。"


translate chinese day5_dv_64847fff:


    "阿丽夏没有说什么，不过看表情应该是同意了。"


translate chinese day5_dv_89f2dd53:


    "我站起来，我们一起去找列娜。"


translate chinese day5_dv_9ca3b435:


    "她的船系在岛屿一端的尖柱上。"


translate chinese day5_dv_a5b3f875:


    me "她很聪明啊，从那么远看不到船。"


translate chinese day5_dv_3cc8517a:


    "我自言自语着。"


translate chinese day5_dv_afb89a5f:


    me "这也就是说她在树丛里。走吧！"


translate chinese day5_dv_90bebd35:


    "我朝着森林里走去，不过阿丽夏还站在原地。"


translate chinese day5_dv_889111c0:


    me "怎么了？{w}我还以为咱们都说好了呢？..."


translate chinese day5_dv_f2f597bd:


    dv "呃...{w}不是...{w}只是...{w}那里面很黑..."


translate chinese day5_dv_8d507ec8:


    "我需要睁大眼睛才能看清她的脸。"


translate chinese day5_dv_28c1ce27:


    me "这个林子比一个花园大不了多少，有什么可害怕的？"


translate chinese day5_dv_da6c23b0:


    "阿丽夏没有回答。"


translate chinese day5_dv_f4740084:


    me "我看我还不如把你撂在这儿，谁知道你又有什么打算？！"


translate chinese day5_dv_a1bcd7f2:


    dv "那个..."


translate chinese day5_dv_6239b879:


    "她走过来贴近我。"


translate chinese day5_dv_0127153c:


    dv "我们这样子走..."


translate chinese day5_dv_9a59280f:


    "她拉住我的手。"


translate chinese day5_dv_18180057:


    "这我倒是没想到。"


translate chinese day5_dv_09c023e9:


    "我的脸红的发烫，我控制不住自己的呼吸。"


translate chinese day5_dv_729fa06e:


    "这种情况下随便开口恐怕没有什么好处，所以我还是保持沉默吧。"


translate chinese day5_dv_140f9a5e:


    "我们慢慢的走进了树林。"


translate chinese day5_dv_dbdafe82:


    "我慢慢的控制住自己的情绪。"


translate chinese day5_dv_f8e9e2d5:


    me "没事的，我们白天才来过这里采草莓，这里没有什么，只是一片桦树林..."


translate chinese day5_dv_a6dda4a2:


    me "白天的时候这里真的很漂亮。"


translate chinese day5_dv_29bf0ac9:


    "我停顿了一下，然后补充道。"


translate chinese day5_dv_2c4d16f2:


    "阿丽夏没有看着我，她的眼睛盯着远处的什么。"


translate chinese day5_dv_57ad0457:


    me "算了，反正是你的事情..."


translate chinese day5_dv_dbfec409:


    "过了一会儿她指着我们的前方。"


translate chinese day5_dv_65ced1f6:


    dv "她在那里。"


translate chinese day5_dv_5460ca51:


    "我向黑暗中看去，但是几乎什么都看不见。"


translate chinese day5_dv_9e040406:


    "当我们走近之后，我发现列娜坐在树下，她在哭着。"


translate chinese day5_dv_41ec3a67:


    un "你们怎么来了？"


translate chinese day5_dv_563f0f0a:


    "她流着泪问着。"


translate chinese day5_dv_8fc2a6d9:


    un "还有你..."


translate chinese day5_dv_0b024121:


    "她看着阿丽夏，没有说完。"


translate chinese day5_dv_86522e17:


    "阿丽夏放开了我的手。"


translate chinese day5_dv_498638c2:


    me "那个，你看..."


translate chinese day5_dv_9eb817ac:


    dv "我只是想对于自己的傲慢和无礼道歉..."


translate chinese day5_dv_272bb3ad:


    "阿丽夏打断了她，她的声音又恢复了平常的态度。"


translate chinese day5_dv_7fc953c2:


    "也许是因为她不想在列娜面前示弱。"


translate chinese day5_dv_0ed5b186:


    un "真是完美的道歉啊！说一套做一套啊！"


translate chinese day5_dv_9b03effe:


    un "你真是聪明，说我做的不对，然后你再做同样的事情！"


translate chinese day5_dv_991e4d49:


    "列娜一发不可收拾的喊起来。"


translate chinese day5_dv_d2b96176:


    dv "你完全理解错了..."


translate chinese day5_dv_04d782d6:


    un "我信我自己！"


translate chinese day5_dv_35da24e2:


    me "呃，那个，你们看...{w}你们又吵了起来，而且和我有关系，但是我完全不明白啊！"


translate chinese day5_dv_c501f81c:


    un "和他解释解释。"


translate chinese day5_dv_6b44c45f:


    "列娜冷笑着。"


translate chinese day5_dv_921308b4:


    dv "没有什么可解释的..."


translate chinese day5_dv_006275ed:


    un "也许你应该让我说？"


translate chinese day5_dv_319d4fcb:


    dv "你随便！"


translate chinese day5_dv_184d37fc:


    "她把胳膊架在胸前，然后转过身去。"


translate chinese day5_dv_a958d2ed:


    un "那好吧，谢苗...{w}因为我想要追你，她很不满。"


translate chinese day5_dv_c52b3095:


    "持续了一分钟的沉默。"


translate chinese day5_dv_e8d3e7f3:


    "列娜似乎不打算继续解释，我则不停的分析着大量的信息。"


translate chinese day5_dv_149b3064:


    me "你什么意思？"


translate chinese day5_dv_885d4f38:


    un "就是我所说的。"


translate chinese day5_dv_220fc882:


    dv "不是那么回事，我也不是那个意思。"


translate chinese day5_dv_f15e5159:


    "阿丽夏插话。"


translate chinese day5_dv_bbd6ebea:


    un "当然，你总是这样..."


translate chinese day5_dv_6771fae9:


    "列娜一下子又哭了起来。"


translate chinese day5_dv_7a91f6b6:


    me "等一下，这怎么回事...?"


translate chinese day5_dv_99a3bbc7:


    th "所以说...列娜喜欢我吗？"


translate chinese day5_dv_b711211f:


    un "还有你！你！你为了证明你自己就拉着他的手！"


translate chinese day5_dv_56b5ad1b:


    th "什么？！所以说阿丽夏也喜欢我？！不对，不可能，阿丽夏喜欢谁，这说不通啊..."


translate chinese day5_dv_428cd21d:


    dv "只是这里太暗了..."


translate chinese day5_dv_72b459e3:


    un "这才像你。{w}那么傲慢，总是试着控制别人，但是轮到你自己的时候..."


translate chinese day5_dv_da6c23b0_1:


    "阿丽夏没有回答。"


translate chinese day5_dv_d66e57b9:


    "我觉得自己有必要说点什么缓解一下气氛。"


translate chinese day5_dv_246e6c69:


    me "嘿，等一下，也许咱们都理解错了..."


translate chinese day5_dv_443723c9:


    dv "这和你没有关系..."


translate chinese day5_dv_b386505f:


    "阿丽夏的声音听起来很远。"


translate chinese day5_dv_013ab4b3:


    me "从她们的对话听起来，这完全和我有关系。"


translate chinese day5_dv_cb70eba8:


    un "你问问她是怎么想的？为什么不让我追！"


translate chinese day5_dv_f7e5e4be:


    th "我不觉得列娜真的是“追”我。"


translate chinese day5_dv_91cb39e7:


    dv "我再说一遍，我..."


translate chinese day5_dv_19fe9001:


    un "我们已经听够了你的故事了！"


translate chinese day5_dv_eae66d30:


    "列娜说完跑向了黑暗之中。"


translate chinese day5_dv_d1bcdf75:


    "我愣在原地，不知道该做什么好。"


translate chinese day5_dv_f72d2ca7:


    th "如果我去追列娜，在我追到她之后该说什么？"


translate chinese day5_dv_f0495ba4:


    th "况且我都不知道到底发生了什么。"


translate chinese day5_dv_09af90a3:


    th "我毫无来由莫名其妙的安慰她只会让她更加伤心。"


translate chinese day5_dv_87ab0698:


    me "大概应该让她一个人冷静一下..."


translate chinese day5_dv_e5989bb1:


    "那听起来不是很自信。"


translate chinese day5_dv_448ae357:


    dv "随便。"


translate chinese day5_dv_c19759e7:


    "阿丽夏一言不发的回答了船上。"


translate chinese day5_dv_a20cefa7:


    "..."


translate chinese day5_dv_9525b0c0:


    "回到码头的路程又是一通折腾，我不得不每隔十米二十米就停下来休息。"


translate chinese day5_dv_7231fc65:


    "阿丽夏盯着河水，没有看我。"


translate chinese day5_dv_2ace92a8:


    "看来这是这个夏令营中第一件超出我的理解能力的事情。"


translate chinese day5_dv_5248806b:


    "虽然这本质上是日常事件。"


translate chinese day5_dv_c998baac:


    "我前往旧营地的那天晚上也没有这样的困惑。"


translate chinese day5_dv_0fc4d70a:


    th "而且如果仔细观察的话，目前没有任何奇怪的地方。"


translate chinese day5_dv_74caf240:


    th "列娜很害羞，甚至是内向，但是她也是人，也有感情。"


translate chinese day5_dv_6b02e0fc:


    th "所以说她的表现也很正常。"


translate chinese day5_dv_8096860d:


    "虽然我不知道具体是怎么回事，不过我确定女孩儿们如果没有什么事的话是不会吵成那个样子的。"


translate chinese day5_dv_e82ce761:


    "看起来我就是那个原因。"


translate chinese day5_dv_b467b940:


    "并且这是整个事件最奇怪的部分。"


translate chinese day5_dv_b1b2da01:


    me "抱歉，事情这样的发展，要是我没有偷听你们的对话..."


translate chinese day5_dv_6a0b3208:


    "我急迫的想要道歉。"


translate chinese day5_dv_443f185c:


    dv "这完全不是你的错。"


translate chinese day5_dv_560d31b2:


    "她漫不经心的说道。"


translate chinese day5_dv_d59d8325:


    dv "你只不过是在特殊的时间特殊的地点碰巧触动了那个开关。"


translate chinese day5_dv_3aad82ef:


    me "我不是很明白..."


translate chinese day5_dv_e1d83f9f:


    dv "当然了，你现在也不需要明白，不过你之后会明白的。"


translate chinese day5_dv_1b595140:


    "她认真的看着我。"


translate chinese day5_dv_fca3e3d8:


    me "见鬼！为什么总有这种事情发生？！"


translate chinese day5_dv_635de124:


    "不管是在中学还是大学，我总是麻烦事的首要目标。"


translate chinese day5_dv_455102c5:


    "有人用堵死大门？被抓到的是我。"


translate chinese day5_dv_f91c8482:


    "有人打架了？被怀疑的是我。"


translate chinese day5_dv_9e87993c:


    "考试挂掉？好吧这个是我自己的问题。"


translate chinese day5_dv_66fe6755:


    "就连我的父母也宁可怪我也不会相信是什么不可抗力。"


translate chinese day5_dv_6130f8de:


    "有时候我觉得我确实可以吸引麻烦，就像是墨菲定律一样。"


translate chinese day5_dv_c8a1ebf7:


    "因为这个我一直尝试着避免麻烦。"


translate chinese day5_dv_ffc93cf9:


    "从今天看来，我肯定是不太擅长..."


translate chinese day5_dv_7e2b13fc:


    dv "你似乎有一种气质，你总是吸引别人的注意。"


translate chinese day5_dv_194f815a:


    "我惊讶的看着阿丽夏，她在笑着。"


translate chinese day5_dv_8a97cc6a:


    th "她好像可以读透我的心思一样..."


translate chinese day5_dv_df9b37de:


    me "大部分是坏事。"


translate chinese day5_dv_127fc146:


    dv "谁知道呢..."


translate chinese day5_dv_3fed2b26:


    "她盯着河水，继续漫不经心的说道。"


translate chinese day5_dv_4676d969:


    dv "真是一个很美丽的夜晚，不是吗？"


translate chinese day5_dv_a4b71fa9:


    me "如果真要说的话，和平常没有什么两样。"


translate chinese day5_dv_223e723d:


    "我燃尽了。"


translate chinese day5_dv_7e23ddcf:


    dv "我就是想说这个，普通的日子，普通的人，普通的事，这就是我们的辅导员最喜欢看到的事情。"


translate chinese day5_dv_3145d815:


    dv "她应该知道她最喜欢的少先队员是一个偷看女孩子洗澡的变态！"


translate chinese day5_dv_9821dca6:


    "我的内心似乎又中了一箭..."


translate chinese day5_dv_c606f827:


    me "你不是认真的吧？"


translate chinese day5_dv_aa6b4182:


    dv "什么...我当然是认真的啦！"


translate chinese day5_dv_d4711875:


    "她似乎完全不是在开玩笑。"


translate chinese day5_dv_6fc6b848:


    me "像是你的风格，不过现在这样合适吗？"


translate chinese day5_dv_6d61f732:


    dv "你不是应该划船吗？"


translate chinese day5_dv_afbb39ba:


    "我刚刚发现我们的船正漂浮在水面上。"


translate chinese day5_dv_0934364c:


    me "等等!{w}不要回避我的问题！"


translate chinese day5_dv_78b06dfc:


    dv "我们先回到营地去吧...{w}我要思考一下。"


translate chinese day5_dv_20705088:


    "我可不想一整晚都在船上度过，所以我加快了速度。"


translate chinese day5_dv_1b0374d0:


    "我们终于抵达了码头。"


translate chinese day5_dv_ffb36c55:


    "我感觉好累，一方面是因为划船，另一方面是阿丽夏说要告发我偷看的事情。"


translate chinese day5_dv_d45a2e04:


    "我了解辅导员，我大致可以猜出她会干出什么事情..."


translate chinese day5_dv_cc9ba692:


    me "所以说你真的要去找奥尔加·德米特里耶夫娜吗？"


translate chinese day5_dv_f74976c4:


    dv "为什么不去呢？"


translate chinese day5_dv_3aa0974f:


    "她超顽皮的笑着。"


translate chinese day5_dv_6c9ff79e:


    me "已经是晚上了，她可能已经睡觉了。"


translate chinese day5_dv_34c6ac1d:


    dv "谁知道呢，没准她还醒着..."


translate chinese day5_dv_e2560be6:


    me "哎呀，你不是知道那只是一个意外吗！"


translate chinese day5_dv_43cdac3a:


    dv "我不知道..."


translate chinese day5_dv_dec618ce:


    "她挥挥手走向营地。"


translate chinese day5_dv_86a32f18:


    "我用尽全身的力气，跳起来，追上了她。"


translate chinese day5_dv_44ba1020:


    "开始我想要抓住她的胳膊，不过后来觉得经过今天发生的这些事情还是不要为好。"


translate chinese day5_dv_079f62e5:


    me "等一下..."


translate chinese day5_dv_a4b6ed32:


    "我迈着沉重的脚步，勉强跟着她。"


translate chinese day5_dv_86a0035c:


    me "咱们来谈一谈。"


translate chinese day5_dv_4f27c86c:


    dv "有什么需要谈的吗？"


translate chinese day5_dv_1eeed204:


    "我们静静的走了一会儿。"


translate chinese day5_dv_a7a06c6d:


    "至少她走的不算太快，让我可以心怀感激的跟上。"


translate chinese day5_dv_5495a454:


    me "嘿，我能不能做什么让你不去揭发我的事情？"


translate chinese day5_dv_ddf334ae:


    dv "不知道，不过，嗯...有一件事情..."


translate chinese day5_dv_b3e9120f:


    "我们来到了广场。"


translate chinese day5_dv_6378d82b:


    me "所以说是什么？"


translate chinese day5_dv_21c6f737:


    dv "那个比如说，不要再去追列娜..."


translate chinese day5_dv_0e345e20:


    th "天啊，她为什么认为我在追列娜？！"


translate chinese day5_dv_434f8b3b:


    "我开始发脾气了。"


translate chinese day5_dv_496ee797:


    me "你在胡说什么？！刚才吵架就是因为你，现在又说这些没用的！我受够了！"


translate chinese day5_dv_4c611d30:


    me "我没有追她！"


translate chinese day5_dv_922b2e90:


    "阿丽夏静静的站着，被月光照亮的脸上露出了不满和怨恨。"


translate chinese day5_dv_dbf16f4b:


    dv "你以为我不知道你看她的眼神吗？"


translate chinese day5_dv_b9cdb4df:


    me "我是怎么看的？"


translate chinese day5_dv_0ba7b0db:


    dv "就是那样！"


translate chinese day5_dv_d2b04ecc:


    me "什么样？"


translate chinese day5_dv_5f616c8b:


    dv "你自己清楚！"


translate chinese day5_dv_df0640da:


    "她移开目光，但还在原地站着。"


translate chinese day5_dv_41e33f1a:


    me "你别再编故事了行不行，如果你真的停不下来的话，就讲给自己听！"


translate chinese day5_dv_b8304cd4:


    me "不要牵扯到别人，你扯到我就算了，现在你又扯到列娜了！"


translate chinese day5_dv_a6dd47ba:


    "我真的怒了，但是阿丽夏没有回答。"


translate chinese day5_dv_73cc2124:


    "我们陷入了沉默，只剩下阿丽夏的抽泣声。"


translate chinese day5_dv_1564df3a:


    me "越来越糟了...你怎么哭上了？你发疯了吗？"


translate chinese day5_dv_7a6449b3:


    "我失望的团起手。"


translate chinese day5_dv_1a648809:


    "我可以理解列娜为什么哭，不过看到阿丽夏哭实在是不能理解..."


translate chinese day5_dv_e14b3828:


    "我都没有听到她的辩解。"


translate chinese day5_dv_f6741270:


    "在别的时候我肯定会非常吃惊，不过现在我很累。"


translate chinese day5_dv_6615be77:


    "我的大脑一片空白。"


translate chinese day5_dv_99ab0d84:


    "准确的说，是沉沉的，完全转不动。"


translate chinese day5_dv_e447cec9:


    "如果我的大脑原来是一条车水马龙的高速公路的话，现在只是一条蜿蜒曲折罕有人至的林间小道。"


translate chinese day5_dv_c1d95e71:


    "阿丽夏还是不说话，至少她现在不哭了。"


translate chinese day5_dv_3c584237:


    me "如果你真的不爽的话，就直接去找奥尔加·德米特里耶夫娜揭露我吧，如果那样你就能好受的话..."


translate chinese day5_dv_bd4f7e30:


    dv "好吧，我不会那样做的..."


translate chinese day5_dv_8e49058e:


    "她轻声说着，转向了我。"


translate chinese day5_dv_f381d5cd:


    "眼泪已经不见了，不过她的脸还是充满了痛苦。"


translate chinese day5_dv_3e47cc5e:


    dv "我只是感觉很伤心..."


translate chinese day5_dv_e6e0ffb2:


    me "什么事情让你伤心了？"


translate chinese day5_dv_b037ba1a:


    "我疲倦的问道。"


translate chinese day5_dv_8926ae0c:


    dv "总是那样。她总是得到人们的注目，我总是被推到一边。"


translate chinese day5_dv_5637fae3:


    "我有点不能理解，只好继续听下去。"


translate chinese day5_dv_1f586e25:


    th "希望她能放过我。"


translate chinese day5_dv_c1141a62:


    me "不是那样的。你也很吸引人啊，人们也不会忽视你啊，我就不会的。"


translate chinese day5_dv_5ec2967a:


    "我睁开眼睛认真的看着她。"


translate chinese day5_dv_8d8f127d:


    "她的脸上充满了惊喜和期待。"


translate chinese day5_dv_455fafb6:


    me "你看?"


translate chinese day5_dv_694b9cc2:


    dv "你...你能不能..."


translate chinese day5_dv_cee25919:


    "突然间，她的声音变得十分温柔，让我..."


translate chinese day5_dv_20588e11:


    "阿丽夏脸颊有些微红，她避开了我的目光。"


translate chinese day5_dv_48f34051:


    dv "那个...{w}你真的觉得我和她一样有魅力吗...？"


translate chinese day5_dv_0250a659:


    "我想说“比她还要好”之类的话，不过还是打住了。"


translate chinese day5_dv_693741eb:


    me "是的..."


translate chinese day5_dv_dab6d6c3:


    "她好像没有发现我说的话一点都没有诚意。"


translate chinese day5_dv_1d46b2fc:


    dv "好了，该睡觉了！"


translate chinese day5_dv_a412e07f:


    "阿丽夏突然充满活力的说道。"


translate chinese day5_dv_ff8903e2:


    "现在她又恢复成自己了。"


translate chinese day5_dv_b2e53022:


    dv "明天见。"


translate chinese day5_dv_c3c87c93:


    "她挥挥手跑开了。"


translate chinese day5_dv_99742b57:


    "我解脱般的叹了口气。"


translate chinese day5_dv_d688f7f1:


    th "好了，今天终于结束了。"


translate chinese day5_dv_334bf905:


    "我剩余的力气都消耗在冲向宿舍的路上了。"


translate chinese day5_dv_d055c80a:


    "屋里没有灯光，所以我瞧瞧的换了衣服躺在床上。"


translate chinese day5_dv_b0e44a4b:


    th "不过我还是感觉很有趣，阿丽夏到底是什么意思？"


translate chinese day5_dv_c93f9c04:


    th "还有列娜...?"


translate chinese day5_dv_2def8e8d:


    "我完全不明白发生了什么，好像我们被吸进了一个漩涡一样。"


translate chinese day5_dv_9b5148d6:


    th "不过接下来会发生什么呢？"


translate chinese day5_dv_801e05ca:


    th "我到哪里去呢？"


translate chinese day5_dv_a20cefa7_1:


    "..."


translate chinese day5_sl_608d1f0d:


    "我很好奇斯拉维娅去了哪里？"


translate chinese day5_sl_3ff2c172:


    "我只想马上离开这个鬼地方，不然也要找个地方自己呆一会儿，否则少先队员小鬼们还有辅导员又要给我下达新的任务了。"


translate chinese day5_sl_2c4efb4d:


    "我找准时机逃到了森林里。"


translate chinese day5_sl_d3fc10d2:


    "夜幕降临了，一个普通的夜晚。"


translate chinese day5_sl_46ada0f2:


    "是那种普通的黑夜，星星月亮都没有什么特殊的感觉，鸟叫，虫鸣，都没有什么特殊之处。"


translate chinese day5_sl_42be4cb2:


    "我漫无目的地散着步，同时保持自己离营地不太远。"


translate chinese day5_sl_89eb1213:


    "在那边还有遇到乌里扬卡的危险，那可是比奥尔加·德米特里耶夫娜还要糟糕的灾难。"


translate chinese day5_sl_4d903dc8:


    "我坐在一棵倒下来的树上思考着。"


translate chinese day5_sl_905d8f87:


    "为什么这一切都发生在我的身上？为什么我到了什么地方都会遇到这种愚蠢的事情？"


translate chinese day5_sl_f58b1a89:


    th "即使我突然出现在这个世界中，我面对的事情仍旧不是让我做实验的牺牲品啊，什么事件的受害者什么的。"


translate chinese day5_sl_013538e9:


    th "不行，我得躲起来，以防遇到发疯的辅导员还有她的传令兵..."


translate chinese day5_sl_99250f31:


    "天空上的星星闪亮着。"


translate chinese day5_sl_d9e32f7f:


    th "也许月光不单单照亮着我，还照亮着我远方的家..."


translate chinese day5_sl_1b24971b:


    "我的胸中感到一阵痛苦。"


translate chinese day5_sl_3ef36634:


    "我的大脑中浮想出了自己的小公寓，一种难受的感觉从肚子传到喉咙。"


translate chinese day5_sl_86074a46:


    "这不象是一种感觉，刚想是一种回忆。"


translate chinese day5_sl_30f40a77:


    "刚刚的发生的事情比我之前的五年时间还要长。"


translate chinese day5_sl_d5ded2d1:


    "现在我真的不确定是不是应该走回去。"


translate chinese day5_sl_87600ecf:


    "我只剩下一个问题了，我是怎么来到这里的。"


translate chinese day5_sl_e3fa1442:


    "我没有用多长时间寻找这个问题的答案。"


translate chinese day5_sl_cba9268c:


    "我的精力都浪费在日常的琐事之中了。"


translate chinese day5_sl_2e35c234:


    "现在为了能保证我在这里的正常生活，我需要对这个地方有一定的了解。"


translate chinese day5_sl_6534fa35:


    "一只被关在笼子里的夜莺都有机会了解它为什么被关在那个地方，并且还应该有决定自己是否出来的权利..."


translate chinese day5_sl_7e84e611:


    "也许我还会继续在人类的大义中沉思一段时间，不过我听到有人来的声音，是奥尔加·德米特里耶夫娜和少先队员们。"


translate chinese day5_sl_a025a4ba:


    th "看起来他们在继续远足..."


translate chinese day5_sl_b7831d31:


    "我迅速的回到营地。"


translate chinese day5_sl_20e62a68:


    th "我不想回到辅导员的宿舍，因为那样肯定会被骂的，而且在这里等着肯定也没有好处。"


translate chinese day5_sl_607f0110:


    "很快我来到了沙滩上。"


translate chinese day5_sl_5f1f337b:


    "我不是一个人，沙滩上还有一件少先队员的制服..."


translate chinese day5_sl_4048e221:


    "不过看起来没有人在水里游泳。"


translate chinese day5_sl_274f5b11:


    "我正在思考这又是谁的恶作剧什么的时候，突然背后有一个声音叫我。"


translate chinese day5_sl_6a030984:


    sl "翘掉远足了吗？"


translate chinese day5_sl_51149e68:


    me "..."


translate chinese day5_sl_19be3aa3:


    sl "我以为你还在树林里呢。"


translate chinese day5_sl_74e314f2:


    "我转过身来，看到斯拉维娅穿着比基尼。"


translate chinese day5_sl_ec56f2f6:


    me "抱歉，我打扰到你了。"


translate chinese day5_sl_c3af5591:


    sl "没关系，我这里已经快要结束了。"


translate chinese day5_sl_0bcdd9c2:


    me "你为什么想要在晚上游泳呢？"


translate chinese day5_sl_bea64ff8:


    sl "嗯，这是不允许的吗？"


translate chinese day5_sl_d1046a49:


    "她笑着。"


translate chinese day5_sl_ff30fec3:


    me "那个，不是，只是，奥尔加·德米特里耶夫娜不会介意吗，你提前离开什么的...?"


translate chinese day5_sl_ddc20430:


    sl "不过你不也是这样做的吗！"


translate chinese day5_sl_72b67782:


    "斯拉维娅看了我一眼。"


translate chinese day5_sl_fe32f248:


    me "是啊，是这样的..."


translate chinese day5_sl_3be00ada:


    "我坐下来看着河水。"


translate chinese day5_sl_9cd3b7aa:


    me "你不喜欢远足吧？"


translate chinese day5_sl_d22b7e79:


    sl "不，不是因为那个，我只是想要一个人呆一会儿。"


translate chinese day5_sl_a21bded2:


    me "看来我还是打扰到了你啊。"


translate chinese day5_sl_97ab3600:


    sl "没有，还好。"


translate chinese day5_sl_4a2c94f6:


    me "这不像是你啊。"


translate chinese day5_sl_b31d38bf:


    sl "你在说什么？"


translate chinese day5_sl_dbe426b0:


    me "就是说，那样子离开..."


translate chinese day5_sl_51aa3969:


    sl "嗯，我又不是一个预先编好程序的机器人。"


translate chinese day5_sl_ec2c2a7c:


    "她笑了。"


translate chinese day5_sl_078b05d1:


    me "是啊，有道理..."


translate chinese day5_sl_3ca9afdd:


    "我还是很迷糊，而且自己的疲倦感越来越明显。"


translate chinese day5_sl_6615be77:


    "我的大脑一片空白。"


translate chinese day5_sl_99ab0d84:


    "准确的说，是沉沉的，完全转不动。"


translate chinese day5_sl_e447cec9:


    "如果我的大脑原来是一条车水马龙的高速公路的话，现在只是一条蜿蜒曲折罕有人至的林间小道。"


translate chinese day5_sl_0458ec4c:


    "所以我就把自己想到的第一件事说了出来。"


translate chinese day5_sl_00992d89:


    me "你不觉得这里很奇怪吗？"


translate chinese day5_sl_706bd1ea:


    sl "奇怪?"


translate chinese day5_sl_15ae2aa5:


    me "这里什么都会发生，就像是一个少先队员夏令营的典范。"


translate chinese day5_sl_51310c74:


    me "当然，这不是说我对此有很多了解，但是这就和我想象的一样。"


translate chinese day5_sl_b31d38bf_1:


    sl "你在说些什么啊？"


translate chinese day5_sl_f3118ca2:


    "她一脸窘迫的看着我。"


translate chinese day5_sl_143220ed:


    me "你没有那种自己不属于这里的感觉吗？"


translate chinese day5_sl_311a3a2f:


    sl "我不知道。"


translate chinese day5_sl_f4af8a55:


    me "准确的说，自己完全不属于这里，自己的家远在千里之外，甚至是宇宙之中。"


translate chinese day5_sl_fb1ab657:


    sl "我不明白你在说什么..."


translate chinese day5_sl_042de057:


    me "我们如此相似..."


translate chinese day5_sl_5653f3b3:


    "我躺下来看着群星。"


translate chinese day5_sl_b18f0221:


    me "如果我告诉你说，我是一个来自未来的外星人，你会怎么想？"


translate chinese day5_sl_33d5edca:


    sl "真的吗？"


translate chinese day5_sl_a0cddebc:


    "斯拉维娅严肃的问着。"


translate chinese day5_sl_399fd2dd:


    me "那个，咱们先假设是这样的，我应该如何回到未来呢？"


translate chinese day5_sl_8068cf2d:


    sl "你真的想要回去吗？"


translate chinese day5_sl_5dcae275:


    th "是啊，我跟她所有的对话最后都会以相同的莫名其妙的方式结束。"


translate chinese day5_sl_02fb630c:


    th "看起来她在给我摆出一个留下来的选项，几乎是坚持这样做的。"


translate chinese day5_sl_5bb9832a:


    me "那个，就说我不太确定吧。{w}那里所有的东西都像是我的家，对我来说要熟悉的多。"


translate chinese day5_sl_ba3caafa:


    me "总的来说，所有的东西我都了解，所有的情况我都能应付。"


translate chinese day5_sl_9d70ab7f:


    me "而在这里则是相反的情况，基本上所有的事情都让我很吃惊，所有的，一切，都是那么不同。"


translate chinese day5_sl_863dcd94:


    sl "真的有那么糟糕吗？"


translate chinese day5_sl_56eeb493:


    me "我不能说是糟糕，大概是应该是，不熟悉，不清楚，有些事情很难改变，即使是和我性格相近的人。"


translate chinese day5_sl_83493bf7:


    sl "但是你真正想要的是什么？"


translate chinese day5_sl_51fffd9d:


    me "要回答这个问题，我首先需要知道自己在“哪里”。"


translate chinese day5_sl_aaa90d0e:


    sl "那就去找出来啊！"


translate chinese day5_sl_3b021425:


    me "如果有那么简单就好了..."


translate chinese day5_sl_0b1c1ea9:


    sl "但是有什么难的呢？"


translate chinese day5_sl_88ed05d2:


    me "一切都很困难！我都不知道从哪里开始...而且经常有事情分心！"


translate chinese day5_sl_84f92ed7:


    sl "你说得很认真，就像是真的一样！"


translate chinese day5_sl_ec2c2a7c_1:


    "她又笑了起来。"


translate chinese day5_sl_183aaa3f:


    me "谁知道呢..."


translate chinese day5_sl_c95262b6:


    "接下来是一段长长的沉默。斯拉维娅打了个喷嚏。"


translate chinese day5_sl_1beeb1c2:


    me "祝你健康。"


translate chinese day5_sl_309686e8:


    sl "谢谢！"


translate chinese day5_sl_d0e6cc2c:


    me "晚上游泳可不是一个好主意，你可能会生病的，赶快回到屋里去吧。"


translate chinese day5_sl_c393724a:


    sl "没关系，我宁可和你在这里呆一会儿，让我换一下衣服。"


translate chinese day5_sl_fedfb810:


    "我有一种受宠若惊的感觉，不过..."


translate chinese day5_sl_85c017d1:


    me "咱们走吧，我陪着你。"


translate chinese day5_sl_d70f58e2:


    "不过我们还没有走出几步，斯拉维娅就抓住了我的胳膊。"


translate chinese day5_sl_75ba3164:


    me "怎么回事？"


translate chinese day5_sl_d161ce4e:


    sl "啊，我突然感觉头晕..."


translate chinese day5_sl_473c8882:


    "我摸摸她的额头，很烫。"


translate chinese day5_sl_de3adeeb:


    "我用手感觉温度没有什么准，不过这次还是挺明显的。"


translate chinese day5_sl_8323f78a:


    me "我跟你说过了啊！"


translate chinese day5_sl_1ba9f4d6:


    sl "阿嚏！"


translate chinese day5_sl_a15cb848:


    me "来吧，来吧。"


translate chinese day5_sl_4d435875:


    sl "不...我可能会感染到热妮娅，咱们还是去医务室吧。"


translate chinese day5_sl_44076421:


    me "你晚上一个人在医务室干什么？"


translate chinese day5_sl_06c202f0:


    sl "这怎么不行，如果你不去的话，我自己去好了。"


translate chinese day5_sl_2cc25ffd:


    "她放开了我的手，准备离开。"


translate chinese day5_sl_a2051ad9:


    me "别伤心，穿上这个，天很冷。"


translate chinese day5_sl_b04f271d:


    "我递给她我的毛衣，本来是我自己给远足预备的。"


translate chinese day5_sl_16c00715:


    th "终于做了点实际的事情，好孩子啊。"


translate chinese day5_sl_309686e8_1:


    sl "感谢!"


translate chinese day5_sl_6275b191:


    "她披上毛衣，露出温柔，关怀的表情，让我无法继续争论下去。"


translate chinese day5_sl_9fbebf7b:


    me "好吧，如你所愿，我们来到了医务室！"


translate chinese day5_sl_02e4ba3b:


    "过了五分钟，我们站在医务室的门口，斯拉维娅找到了正确的钥匙。"


translate chinese day5_sl_a22a6123:


    "我觉得这个想法很傻，没有人因为普通的感冒死掉的。"


translate chinese day5_sl_84768b52:


    "至少在上个世纪不太可能。"


translate chinese day5_sl_097ff7d4:


    "我找不到任何应该在医务室待下的理由。"


translate chinese day5_sl_3a4dbc67:


    "最后斯拉维娅打开了房门，然后靠在了我的身上。"


translate chinese day5_sl_36203cca:


    sl "我还是感觉有些头晕。"


translate chinese day5_sl_e5045fc4:


    "她不好意思的说道。"


translate chinese day5_sl_4461e513:


    "斯拉维娅躺在了床上，我坐在了旁边的椅子上。"


translate chinese day5_sl_48f0ab25:


    me "不过，我说啊，一整晚就躺在医务室可是..."


translate chinese day5_sl_7da35453:


    th "再怎么说热妮娅也可以给你打一杯水什么的，她也不会感染上的，她又年轻，又健康。"


translate chinese day5_sl_7f80fc24:


    sl "没关系的，我不想麻烦别人，而且护士明天就回来。"


translate chinese day5_sl_3d2e3b21:


    "我突然间想象了一下如果是我自己处在斯拉维娅的位置上，在医务室呆一晚上..."


translate chinese day5_sl_460094c3:


    "感觉背后一阵寒风吹来。"


translate chinese day5_sl_3deaf8e7:


    me "我说，也许我可以陪你呆着？..."


translate chinese day5_sl_a3c61bd4:


    "我欠斯拉维娅很多人情，而且说实话我都不想离开。"


translate chinese day5_sl_afee0832:


    sl "为什么呢？一切都没有问题的，你应该去休息一下。"


translate chinese day5_sl_aa120605:


    me "我还是觉得..."


translate chinese day5_sl_e9c90895:


    sl "没有关系的。"


translate chinese day5_sl_353eced4:


    "我一度想过..."


translate chinese day5_sl_82680eaa:


    "当然她在这里也不会发生什么，不过我还是在这里陪着她更能放松下来。"


translate chinese day5_sl_9fbe4547:


    me "我觉得..."


translate chinese day5_sl_2b08672e:


    sl "哎呀！"


translate chinese day5_sl_0d7f306b:


    "斯拉维娅好像生气了。"


translate chinese day5_sl_9f3f4e67:


    me "你不会赶我吧。"


translate chinese day5_sl_2e9b0de6:


    "我狡猾地笑着。"


translate chinese day5_sl_a8c6f6ab:


    sl "好吧，不过如果你也得上感冒的话，那就只好怪你自己咯！"


translate chinese day5_sl_7d705a39:


    "我小小的庆祝了一下自己的胜利。"


translate chinese day5_sl_96286888:


    sl "那么，咱们应该做什么？"


translate chinese day5_sl_9a9c2e41:


    "抽屉里应该有一些纸牌。"


translate chinese day5_sl_14a83ddb:


    me "Durak?"


translate chinese day5_sl_bf89e18e:


    "事实上除了那个和扑克，别的我也都不会。"


translate chinese day5_sl_b58c757b:


    sl "好吧。"


translate chinese day5_sl_403a4e5b:


    "我们玩了很长时间，我完全忘记了时间。"


translate chinese day5_sl_5a3cb568:


    "我们聊着有趣的话题，医务室中充满了笑声。"


translate chinese day5_sl_653470e9:


    "斯拉维娅看起来状态很好。"


translate chinese day5_sl_f248ccf9:


    "然后，午夜降临了..."


translate chinese day5_sl_1bd398ca:


    sl "该睡觉了。"


translate chinese day5_sl_6ded9a67:


    me "大概是的..."


translate chinese day5_sl_70dd0c01:


    sl "不过你应该去哪里呢？"


translate chinese day5_sl_8fb93608:


    "她看着我。"


translate chinese day5_sl_51906baa:


    sl "哎呀，你不应该坐在这里陪着我的。"


translate chinese day5_sl_b550b4b7:


    me "反正也..."


translate chinese day5_sl_617f93da:


    th "我现在站着都能睡着了。"


translate chinese day5_sl_848c7c43:


    "斯拉维娅看了我一会儿，然后拉上了窗帘。"


translate chinese day5_sl_df84d49a:


    sl "晚安！"


translate chinese day5_sl_8de47ec6:


    me "安。"


translate chinese day5_sl_b551d5e5:


    "我趴下来，然后立刻就睡着了。"


translate chinese day5_sl_56adc4c2:


    "虽然我很累，但是睡得并不好，更像是打盹。"


translate chinese day5_sl_c443185a:


    "我的意识不断地离开我然后再回来。"


translate chinese day5_sl_b0ea066f:


    "出现幻觉的最佳状态..."


translate chinese day5_sl_c118b5ae:


    "410公交车从房间里面开了过去，屋顶下起来雪，我的电脑显示器突然从桌子上飞了起来。"


translate chinese day5_sl_196f6c57:


    "墙纸离开了墙面， 垃圾离开了地板，还有我自己的床，出现在了角落里。"


translate chinese day5_sl_d7272335:


    "我又回到了自己的小公寓。"


translate chinese day5_sl_7fc90acf:


    "风景变成了我的家乡。"


translate chinese day5_sl_bcd4a1a8:


    "我好像穿过了草原上的游牧民族。"


translate chinese day5_sl_3624e130:


    "然后黑暗再一次降临，永远的黑暗。"


translate chinese day5_sl_b5fa9f77:


    "我突然惊醒，一身冷汗。"


translate chinese day5_sl_fe12675a:


    "斯拉维娅担心的看着我。"


translate chinese day5_sl_60108fae:


    me "只是一个噩梦..."


translate chinese day5_sl_15aba1cf:


    "我试着笑出来。"


translate chinese day5_sl_1c6cbe1a:


    sl "你在大声叫着..."


translate chinese day5_sl_6c7c2069:


    me "叫着什么?"


translate chinese day5_sl_f9a31b17:


    sl "我不知道，听不出来。"


translate chinese day5_sl_8be56802:


    "表针显示现在才0:30"


translate chinese day5_sl_11adbbfc:


    sl "谢苗。"


translate chinese day5_sl_b6626098:


    me "哈?"


translate chinese day5_sl_5ef3b66e:


    sl "我冷..."


translate chinese day5_sl_564a8986:


    "斯拉维娅颤抖着摩擦着毛衣的袖子。"


translate chinese day5_sl_a0d2cda7:


    me "等一下，我去找一个毯子。"


translate chinese day5_sl_2b495447:


    "我翻遍一个个箱子，暗自骂着这个医务室为什么没有一件给病人盖的东西。"


translate chinese day5_sl_6ab83d4c:


    me "抱歉，这里什么都没有..."


translate chinese day5_sl_640c2a39:


    me "等一下，我跑去找一下。"


translate chinese day5_sl_97419f60:


    sl "不用了，你可以和我一起躺在这里吗？那样会暖和很多。"


translate chinese day5_sl_c849a05b:


    "她很严肃的说着。"


translate chinese day5_sl_68527d19:


    "她的声音听起来那么可怜让我止住了呼吸。"


translate chinese day5_sl_22609679:


    me "你确定...那样...可以吗?"


translate chinese day5_sl_ed91b7e2:


    sl "所以说，不行吗?"


translate chinese day5_sl_07ed6d6a:


    "她红着脸问着。"


translate chinese day5_sl_7edd72b2:


    me "没有啊，为什么..."


translate chinese day5_sl_d3c57f8c:


    "我脱掉鞋子躺在沙发上，尽量靠近一边。"


translate chinese day5_sl_f211b57a:


    "斯拉维娅缓缓的抱住了我，然后把放在了我的胸口。"


translate chinese day5_sl_c61518ac:


    sl "嗯，这样好多了。"


translate chinese day5_sl_2481134c:


    "她咕噜咕噜的叫着。"


translate chinese day5_sl_6d3a10e3:


    "我不知道说些什么好，所以只能静静的躺着。"


translate chinese day5_sl_eb090c68:


    me "很好..."


translate chinese day5_sl_00a08a43:


    sl "谢谢你。"


translate chinese day5_sl_9508d41b:


    me "为什么?"


translate chinese day5_sl_f0f90483:


    "斯拉维娅一直闭着眼，好像随时都有可能睡着。"


translate chinese day5_sl_bca81bd5:


    sl "因为在这里。"


translate chinese day5_sl_9a94410c:


    me "还好啦..."


translate chinese day5_sl_d663f86e:


    th "刚刚她还打算让我离开。"


translate chinese day5_sl_fc7dc843:


    sl "没有，真的。"


translate chinese day5_sl_b320bfee:


    me "那就，不客气。"


translate chinese day5_sl_7a6acecb:


    "她什么都没有说，不过我觉得她应该笑了。"


translate chinese day5_sl_77f7eb26:


    me "就像他们说的，随时联系！"


translate chinese day5_sl_c5ee54e6:


    sl "你好善良，你非常关心别人。"


translate chinese day5_sl_1a49756a:


    me "嗯，我尽量。"


translate chinese day5_sl_a0e4fdf2:


    sl "你是一个很好的朋友。"


translate chinese day5_sl_d1052636:


    me "朋友？嗯，也许是吧..."


translate chinese day5_sl_29a4e2ac:


    "不知为何这个词语让我很难受。"


translate chinese day5_sl_3b03b28d:


    "另一方面来说并没有什么理由让斯拉维娅把我当成是什么超过朋友的人..."


translate chinese day5_sl_a2344f49:


    th "那都是些什么想法啊?"


translate chinese day5_sl_df7e98e4:


    me "你也是..."


translate chinese day5_sl_954ffd8b:


    "我就是找不到合适的词语。"


translate chinese day5_sl_7411ac41:


    th "什么叫“你也是”啊？我们希望进一步密切的合作？"


translate chinese day5_sl_ada6764b:


    th "或者说，是的，很高兴和你做朋友，咱们一起去玩耍吧？"


translate chinese day5_sl_ecd07382:


    "斯拉维娅就在我的身边躺着，让我实在难以把持住，我不能逃跑，不能躲起来，即使不去看她的眼睛我也知道她能够看穿我。"


translate chinese day5_sl_a549e991:


    sl "你有些痛苦？"


translate chinese day5_sl_98db6f67:


    me "没有，我很好。"


translate chinese day5_sl_ef63c01d:


    sl "我能看得出来。"


translate chinese day5_sl_2d91a0c5:


    me "那个，也许吧..."


translate chinese day5_sl_5c6c0b54:


    sl "你想要说一说吗？"


translate chinese day5_sl_99ee26fe:


    me "那个...不是太想说，说实话，没有什么可说的。"


translate chinese day5_sl_45f565d7:


    sl "那么你真的不知道自己为什么失落吗？"


translate chinese day5_sl_dab7f3be:


    me "是啊，也许是这样的吧。"


translate chinese day5_sl_e47336fd:


    sl "那么究竟为什么失落呢？"


translate chinese day5_sl_c954d69f:


    "斯拉维娅笑了起来。"


translate chinese day5_sl_3ae34e17:


    me "是啊，不应该啊。"


translate chinese day5_sl_dbfe4f1e:


    sl "那太好了！"


translate chinese day5_sl_6dd013b2:


    "她抱得更紧了。"


translate chinese day5_sl_970eac9a:


    me "现在不冷了吗?"


translate chinese day5_sl_ad99cfcc:


    sl "我没事，如果你不舒服的话，请告诉我。"


translate chinese day5_sl_5c1a0ecc:


    me "没事，我很好。"


translate chinese day5_sl_ed54fb3b:


    sl "就像是那个时候？在船上的时候？"


translate chinese day5_sl_1339e6eb:


    "我的肌肉似乎还记得那时的伤痛，开始用疼痛感提醒我。"


translate chinese day5_sl_b21be332:


    me "完全不一样。"


translate chinese day5_sl_8c47f676:


    sl "好吧，好吧。"


translate chinese day5_sl_044e54c2:


    "她顽皮的笑着，稍稍抬起了头。"


translate chinese day5_sl_b78463d7:


    sl "所以说，一切都还好吗？"


translate chinese day5_sl_f0ccc081:


    me "是啊，很好。"


translate chinese day5_sl_719f7d6e:


    "我开始担心了，斯拉维娅肯定想要什么。"


translate chinese day5_sl_0793c467:


    "想要让我干什么或者说什么。"


translate chinese day5_sl_bd971270:


    th "但是是什么...?"


translate chinese day5_sl_24ef8389:


    sl "你确定吗?"


translate chinese day5_sl_626d142f:


    me "是啊...{w}大概是。"


translate chinese day5_sl_440927ac:


    sl "那好吧，晚安!"


translate chinese day5_sl_3c5ee540:


    me "晚安。"


translate chinese day5_sl_a20cefa7:


    "..."


translate chinese day5_sl_a7b8346c:


    "过了一会儿，她睡着了。"


translate chinese day5_sl_dbce3303:


    "我查看了一下角落里的钟表，现在还只有1点钟，也就是说我得一直保持这个姿势到早晨。"


translate chinese day5_sl_0dd8c20c:


    "这不完全是我的胳膊变麻的问题。"


translate chinese day5_sl_dd2a66dd:


    "只是说，距离斯拉维娅这么近，很危险。"


translate chinese day5_sl_39c41ef8:


    "我的大脑被各种想法袭击。"


translate chinese day5_sl_c93104db:


    th "她真的觉得这种情况很正常嘛?"


translate chinese day5_sl_a20cefa7_1:


    "..."


translate chinese day5_sl_9719a2d8:


    "就在我思考如果我们被发现的话会多么糟糕的时候，大门突然打开了，奥尔加·德米特里耶夫娜冲了进来。"


translate chinese day5_sl_ede80479:


    "我的心脏一度停止了跳动。"


translate chinese day5_sl_96da4eb5:


    mt "谢苗..."


translate chinese day5_sl_9142f8f6:


    "辅导员用一种魔鬼般阴沉的声音说着。"


translate chinese day5_sl_b51939f9:


    mt "我找了你好久，你提前离开了森林，没有回到宿舍睡觉，现在你还在勾引我们最好的少先队员!"


translate chinese day5_sl_5e2bbb18:


    "斯拉维娅突然醒了过来。"


translate chinese day5_sl_3872252b:


    "她困倦的看着辅导员，然后突然清醒过来，明白了发生了什么情况。"


translate chinese day5_sl_110c77e8:


    sl "奥尔加·德米特里耶夫娜！不是你想象的那个样子，只是我生病了，让谢苗同学陪着我。"


translate chinese day5_sl_764cea7c:


    sl "然后我有点冷，让他回到自己的房间去。"


translate chinese day5_sl_a4985f09:


    mt "哦，是啊，你生病了？"


translate chinese day5_sl_7b8fbab7:


    sl "是的..."


translate chinese day5_sl_4aa379e0:


    "斯拉维娅恭敬地回答着。"


translate chinese day5_sl_dc4f5852:


    mt "那么你必须得到治疗！"


translate chinese day5_sl_bcebe051:


    "她恐吓的说道。"


translate chinese day5_sl_c2745a0c:


    mt "还有你！站起来跟着我！"


translate chinese day5_sl_a7b2aa21:


    "我知道争论没有意义，所以乖乖的站起来跟着辅导员，没有再看斯拉维娅一眼。"


translate chinese day5_sl_3e2db886:


    "我们一言不发的来到了广场。"


translate chinese day5_sl_fb0b3f24:


    "奥尔加·德米特里耶夫娜邪恶的看着我，说道:"


translate chinese day5_sl_d702b0ee:


    mt "该回家了！我得和你进行一次严肃的对话！"


translate chinese day5_sl_f725c780:


    "我一路上都感觉很不舒服。"


translate chinese day5_sl_8551a9d4:


    "辅导员静静的跟着我。"


translate chinese day5_sl_0a646630:


    "我觉得自己又一次陷入了谁都看得出来不对劲的尴尬境地。"


translate chinese day5_sl_68e9f070:


    th "哎... 没什么可讨论的..."


translate chinese day5_sl_7dbd4345:


    "如果我是在奥尔加·德米特里耶夫娜的位置上呢?{w}两个少先队员在医务室抱在一起睡觉。{w}实在想不出有什么理由辩解。"


translate chinese day5_sl_25df23e4:


    "最糟糕的事情是，这些事情总是发生在我的身上!"


translate chinese day5_sl_ce7660c4:


    "我总是觉得自己擅长分析情况，不过我真的得应用到实战中..."


translate chinese day5_sl_f40fe17c:


    "我们回到了宿舍，辅导员还在继续审问我。"


translate chinese day5_sl_8237174c:


    mt "你还想不想辩解?"


translate chinese day5_sl_49ae3aab:


    "她看起来似乎冷静了一些。"


translate chinese day5_sl_8ba11b89:


    "至少她的这一方面也显露了出来，她是一个冲动的女人，不过也可以很快的冷静下来。"


translate chinese day5_sl_512994b2:


    me "斯拉维娅已经把一切都告诉你了..."


translate chinese day5_sl_43521b0d:


    mt "你觉得我会相信吗?"


translate chinese day5_sl_49fe0ea8:


    me "我不强迫你去相信，不过我也不会编出什么，因为那就是事实。"


translate chinese day5_sl_5fb08a35:


    "她看着我，然后说："


translate chinese day5_sl_5be2bfc3:


    mt "我明天再决定怎么处理你。"


translate chinese day5_sl_9f74c45c:


    "辅导员关上了灯，我没有再费力去脱衣服，只是卷上了毯子，然后面朝着墙。"


translate chinese day5_sl_68e31fbe:


    th "奇怪，为什么只是责备我？再怎么说，那里也不只有我自己。"


translate chinese day5_sl_5e211841:


    th "斯拉维娅当然是少先队员的模范，不过..."


translate chinese day5_sl_bfacbe72:


    "不，我并不是想要责备她，我只是觉得自己总是处在这种讨厌的处境很不爽。"


translate chinese day5_sl_d8ca9160:


    "不由自主的，没有任何恶意..."


translate chinese day5_sl_53374338:


    "我还可以在那里自怨自艾好长时间，不过疲劳战胜了我，于是我睡了过去。"


translate chinese day5_us_8f2500e8:


    "我那晚一直没有想乌里扬娜。"


translate chinese day5_us_4754cc34:


    th "那个蛋糕的事情，好像从来没有发生过一样..."


translate chinese day5_us_d4e9d4ea:


    "虽然我还是记得她气愤的，失望的表情。"


translate chinese day5_us_760ba5e1:


    "虽然在其他的情况下我肯定不会去找她，不过现在我倒是有了一个很好的离开这里的理由。"


translate chinese day5_us_afda6c4c:


    "因为担心奥尔加·德米特里耶夫娜又会给我布置新的任务，所以我没有告诉她，只是偷偷的离开了森林。"


translate chinese day5_us_d6e969ac:


    "夜幕降临了。"


translate chinese day5_us_fbf53d12:


    "我感谢辅导员没有过度的研究这片土地，让我距离文明只有短短的几百米远。"


translate chinese day5_us_94c6d083:


    "再来一段森林中的探险可不怎么样。"


translate chinese day5_us_57b7303c:


    "很快我来到了广场，开始犹豫。"


translate chinese day5_us_6f52cdaf:


    th "这不会看起来很傻吗?"


translate chinese day5_us_b54027f4:


    th "我要和乌里扬卡说什么呢，再说了，我到底是为什么要去找她啊?"


translate chinese day5_us_4b80dcb9:


    "我的大脑满满的，已经没有空间进行更多的思考了。"


translate chinese day5_us_e447cec9:


    "如果我的大脑原来是一条车水马龙的高速公路的话，现在只是一条蜿蜒曲折罕有人至的林间小道。"


translate chinese day5_us_cd65d7a1:


    "所以与其三思，不如说干就干。"


translate chinese day5_us_5365a619:


    th "不过我应该去哪里呢？去她的房间吗？如果她不在那里呢？"


translate chinese day5_us_cb5fdfc2:


    "突然间，有什么东西撞到了我，让我摔倒在地面上，幸好我还有时间伸出手臂，不然就要把鼻子撞烂了..."


translate chinese day5_us_759e5578:


    us "抓到你啦!"


translate chinese day5_us_447bdfb5:


    "我迅速站起来，看到乌里扬卡站在我的面前。"


translate chinese day5_us_1b093c3a:


    me "你干什么?!"


translate chinese day5_us_88297410:


    "我实在忍不住不喊出来。"


translate chinese day5_us_259f22bf:


    us "谁让你发呆的!"


translate chinese day5_us_2f6a59ef:


    "她邪恶的回答着。"


translate chinese day5_us_056f2d76:


    me "要是砸坏了我的鼻子怎么办？或者摔坏了我的胳膊？"


translate chinese day5_us_78d01ae3:


    "我更加冷静的说道。"


translate chinese day5_us_7de3b85d:


    us "那个，反正都没有发生。"


translate chinese day5_us_e4e8e714:


    me "你看，你为什么总是取笑我?"


translate chinese day5_us_515f902a:


    "我已经开始后悔担心她孤独的决定了。"


translate chinese day5_us_6ee354fc:


    me "今天对你的惩罚还不够吗？你就不能让人省省心？"


translate chinese day5_us_91d3b1af:


    us "这都是你的错。"


translate chinese day5_us_2c703153:


    "乌里扬娜脸上的笑容消失了。"


translate chinese day5_us_9afe61c9:


    me "什么，是我的错?"


translate chinese day5_us_4c55e5c0:


    us "一切!"


translate chinese day5_us_590ed0a7:


    me "你是说总是该怪我吗?"


translate chinese day5_us_551306e9:


    us "是!"


translate chinese day5_us_184d37fc:


    "她在胸前挥舞着手臂，然后跑开了。"


translate chinese day5_us_eb6f6f93:


    me "好..."


translate chinese day5_us_ad3d6bce:


    "我突然想尽快离开这个是非之地。"


translate chinese day5_us_4ef07a9a:


    me "不过我是为了你才回来的..."


translate chinese day5_us_b7ef3111:


    us "什么?"


translate chinese day5_us_2940996c:


    me "我觉得大家都去远足，只留下你在这里，可能会很寂寞..."


translate chinese day5_us_7625dabe:


    us "我才不在乎!"


translate chinese day5_us_1272c1de:


    "她兴奋的喊着。"


translate chinese day5_us_36434b99:


    us "如果真的是那样，你打算怎么做?"


translate chinese day5_us_149b3064:


    me "你什么意思?"


translate chinese day5_us_5db74333:


    us "我是说如果你是因为我回到这里!"


translate chinese day5_us_5a38be43:


    "这我倒是没想到。"


translate chinese day5_us_1206b412:


    th "虽然说我不太在乎自己是为什么回来的..."


translate chinese day5_us_b8ee6c9c:


    me "经历过这样的见面后我什么也不想做。"


translate chinese day5_us_457fd234:


    us "那么选择权在我了!"


translate chinese day5_us_f8556bbb:


    "乌里扬娜兴奋的说道，然后开始思考。"


translate chinese day5_us_36baf507:


    "我看着她，然后开始失去耐心:"


translate chinese day5_us_83506e3c:


    me "听着，如果你又打算..."


translate chinese day5_us_ba2766af:


    us "我知道！咱们打扮成鬼一起去吓唬其他人！"


translate chinese day5_us_9c7c48b0:


    th "我怎么也不觉得这很好笑。"


translate chinese day5_us_6a2846ee:


    me "够了..."


translate chinese day5_us_532041e9:


    "我懒洋洋的说着，不过突然停了下来。"


translate chinese day5_us_146e3f57:


    th "也许那并不是那么差的主意。"


translate chinese day5_us_49be75d5:


    "而且，等她想清楚，准备好的时候，可能辅导员已经带着少先队员们回来了。"


translate chinese day5_us_b8d157c1:


    "如果我是因为乌里扬娜回来的，那么我应该继续下去。"


translate chinese day5_us_22634694:


    me "也许你是对的。"


translate chinese day5_us_b7ef3111_1:


    us "什么?"


translate chinese day5_us_2dbdc7b7:


    "她大大的睁着眼睛看着我。"


translate chinese day5_us_1b7f7977:


    us "你就这样同意了吗?"


translate chinese day5_us_4bd5340f:


    me "呃，这有什么奇怪的，而且如果你一定要..."


translate chinese day5_us_29659e99:


    "她仔细看了我一会儿，然后:"


translate chinese day5_us_7601cc90:


    us "太好了!{w}那么咱们需要床单！要打扮的像一点，不是吗?"


translate chinese day5_us_907b6d20:


    me "大概是吧..."


translate chinese day5_us_03325ca7:


    us "你去找吧!"


translate chinese day5_us_3d4a0aa6:


    "乌里扬卡专横的宣布道。"


translate chinese day5_us_310cd775:


    me "你觉得我应该到哪里去找？"


translate chinese day5_us_a2c60a47:


    us "当然是从你的房间里拿了！"


translate chinese day5_us_cdd74e25:


    me "我感觉不是那么当然..."


translate chinese day5_us_acbf1354:


    us "所以说，你不想去吗?"


translate chinese day5_us_044b5473:


    "她的表情突然阴沉了下来。"


translate chinese day5_us_69eeffd7:


    me "好吧，好吧!"


translate chinese day5_us_48beb79c:


    "我突然想起来奥尔加·德米特里耶夫娜的柜子里好像有富裕的，所以应该没有问题..."


translate chinese day5_us_a4951fb8:


    "我们接近宿舍，我对乌里扬娜说:"


translate chinese day5_us_153ceca2:


    me "在这里等着我。"


translate chinese day5_us_34c369b7:


    us "是，长官!"


translate chinese day5_us_30bcaace:


    "她好像很正式的向我敬礼。"


translate chinese day5_us_d9777aab:


    "乌里扬卡好像完全沉浸到这个游戏里了。"


translate chinese day5_us_900dfc81:


    "我很快找到了两张白色的床单。"


translate chinese day5_us_03e5f1cc:


    th "很可惜它们要被弄脏了..."


translate chinese day5_us_71a21743:


    me "来，拿着。"


translate chinese day5_us_5d11f8d4:


    "我把一张床单递给她。"


translate chinese day5_us_55a5bd1b:


    us "这张太大了。"


translate chinese day5_us_2a2dcaaf:


    "乌里扬卡摆弄了一下之后说道。"


translate chinese day5_us_243ba3e8:


    th "想想她的身高，这也不难理解。"


translate chinese day5_us_99c46342:


    me "递给我。"


translate chinese day5_us_3fae3b47:


    "我把床单折成一半，然后又递给她。"


translate chinese day5_us_e2dcfcce:


    us "好多了，跟我来!"


translate chinese day5_us_ef43e763:


    "她把白布套在头上，然后跑进了森林。"


translate chinese day5_us_9e8ba2ed:


    me "等等!"


translate chinese day5_us_69531d20:


    "我跟了上去。"


translate chinese day5_us_880b3622:


    "没过几分钟，我们就来到了少先队员们露营的地点，并且躲在树丛里。"


translate chinese day5_us_4e62ae4d:


    "现在很明显这个本来无害的活动有点变味儿。"


translate chinese day5_us_4f51f0fe:


    "本来我确定乌里扬卡在露营结束以前不会准备好，不过现在我们俩就这样套着床单，猫在树丛里，看起来一点也不可怕，反而很好笑。"


translate chinese day5_us_8f4b438d:


    us "准备好！听我口令!"


translate chinese day5_us_451e8da8:


    me "等一下，等一下，我其实是开玩笑的，你想想，这其实对你没有好处，只会在大庭广众之下被抓住!"


translate chinese day5_us_16b3df32:


    me "我也会..."


translate chinese day5_us_f9d44a76:


    us "决不后退，决不投降！你准备好了吗，听我口令倒数三个数!"


translate chinese day5_us_0f768398:


    "我的大脑中瞬间闪过各种可能的后果，数量并不多。"


translate chinese day5_us_3f1c9f2d:


    "第一种，我和乌里扬卡从树丛里冲出来，少先队员们开始大笑，然后我们被辅导员严厉的批评。"


translate chinese day5_us_31114dd5:


    "然后乌里扬卡被处以夏令营里的极刑。"


translate chinese day5_us_3b066783:


    "第二种，我呆在这里，和其他人一起，看着乌里扬卡怎么在空地上瞎跑。"


translate chinese day5_us_6d29669b:


    "她还是被处以极刑，我则相对好过。"


translate chinese day5_us_30ea56d4:


    "第三种，我尽一切可能阻止她，然后谁也不会出丑，但是她会感觉受到侮辱。"


translate chinese day5_us_52605bb2:


    "本来可以更加顺利的，不过我的这些想法都需要超过三秒钟的时间，而乌里扬卡没有给我那么多的时间，我只能眼睁睁的看着她野兽般咆哮着冲了出去。"


translate chinese day5_us_d63ede28:


    "如我所料，少先队员们都大笑了起来，甚至还有人从木材堆上摔下来在地上打滚。"


translate chinese day5_us_733c66fc:


    "我试着救场，大声喊叫起来，让乌里扬卡听不到他们的声音:"


translate chinese day5_us_f0a46cde:


    me "傻瓜！快回来！"


translate chinese day5_us_37e33704:


    "我不知道是我的提示见效了，还是乌里扬卡自己意识到计划失败了，她迅速冲了回来，躲在了树丛中。"


translate chinese day5_us_95318591:


    "我毫不犹豫的跟着她。"


translate chinese day5_us_d75de63e:


    "这样的结局也许可以让她不被再一次惩罚。"


translate chinese day5_us_a78b707a:


    "幸好我没有加入她的滑稽表演。"


translate chinese day5_us_97b3825e:


    th "现在我得找到乌里扬卡。"


translate chinese day5_us_56432bce:


    "结果这并没有多难，她没有跑远。"


translate chinese day5_us_db6a8922:


    "乌里扬娜坐在一棵树下...哭着。"


translate chinese day5_us_70edb764:


    "我站在原地，犹豫着。"


translate chinese day5_us_73c62008:


    "当然这种情况完全在意料之中，不过我还没有想好该如何安慰她。"


translate chinese day5_us_a3275ce4:


    "而且我是主动回去营地的，还同意了加入她的计划..."


translate chinese day5_us_f8f658a7:


    "然后事情只是变得更糟糕。"


translate chinese day5_us_0e2f3120:


    "而且我还累的要死。"


translate chinese day5_us_311d70c7:


    "这个时候我只想让这一切都消失，只想闭上眼，然后睁开的时候已经到达另外一个地方。"


translate chinese day5_us_ce999c85:


    "最好是一个安静的地方。"


translate chinese day5_us_67119f08:


    "不过我的视线中，哭泣的乌里扬娜让我必须采取行动。"


translate chinese day5_us_c414404b:


    "我走过去，坐在她的旁边。"


translate chinese day5_us_82d52896:


    me "那个，你到底是在期待什么呢...?"


translate chinese day5_us_262b65eb:


    "我很哲学的问道。"


translate chinese day5_us_8d394e00:


    me "结局肯定会是这样的。"


translate chinese day5_us_1b26a454:


    us "这一切都怪你！都怪你!"


translate chinese day5_us_5f31ce4d:


    "乌里扬卡哭喊着。"


translate chinese day5_us_a508b663:


    me "所以说如果我和你一起冲出去能有什么改变？不过是咱们两个一起被嘲笑罢了。"


translate chinese day5_us_177d555a:


    us "你总是这个样子！总是!"


translate chinese day5_us_df7cba70:


    "她哭的声音越来越大，然后突然冲过来用拳头砸我，她并没有用力。"


translate chinese day5_us_34d398f8:


    "她更像是想要挣脱自己的绝望，而不是真的想要打我。"


translate chinese day5_us_d2af7ffb:


    me "冷静下来!"


translate chinese day5_us_a0e82e49:


    "我坚定地说着。"


translate chinese day5_us_5f0944f0:


    "她停了下来，然后抱着我。"


translate chinese day5_us_a8064fda:


    us "也许不会改变什么，不过如果咱们两个一起冲出去的话我会感觉好得多。"


translate chinese day5_us_6fd205af:


    "我不知道该说什么好，只能轻轻的拍着她的头。"


translate chinese day5_us_2e4814b2:


    us "我可以这样子多呆一会儿吗?"


translate chinese day5_us_10409023:


    me "嗯。"


translate chinese day5_us_d624d515:


    "这个时候她不像是一个核反应炉，而只是我的一个小妹妹，把事情搞得很糟而已。"


translate chinese day5_us_7cb9453a:


    "我完全不责备她，相反我也觉得自己太在乎计划是不是成功了。"


translate chinese day5_us_42868526:


    me "好啦，好啦，咱们下次会吓到他们的!"


translate chinese day5_us_c463eeea:


    us "是啊..."


translate chinese day5_us_a38f6822:


    "我不知道我们就这样沉默的呆了多长时间。"


translate chinese day5_us_3dcc62a7:


    "乌里扬娜不再哭了，我不想打扰她，她自己就冷静下来了。"


translate chinese day5_us_8f418ac5:


    "不过我也没有在森林里呆一整晚的计划。"


translate chinese day5_us_2bb4e019:


    me "嘿，咱们回到营地去吧。"


translate chinese day5_us_4d1b85a8:


    "我轻轻的摇动着她的肩膀，不过她并没有回应。"


translate chinese day5_us_17489859:


    "乌里扬卡睡着了。"


translate chinese day5_us_95efcfe1:


    me "嘿!"


translate chinese day5_us_17edece2:


    "我用力的摇晃着，还是没有反应。"


translate chinese day5_us_1161aa63:


    "我都想哭了。"


translate chinese day5_us_c56aa18d:


    th "为什么这种事情总是发生在我的身上?"


translate chinese day5_us_f58b1a89:


    th "我突然出现在不知道是哪里的少先队夏令营，不过我没有被用作实验对象，没有糊里糊涂的加入星际战争，像是科幻小说那样。"


translate chinese day5_us_e29a567a:


    th "相反，我要在森林里抱着一个睡觉的小女孩。"


translate chinese day5_us_5c588563:


    th "下次我宁愿参加实验..."


translate chinese day5_us_4cc5c895:


    "我站起来，把睡着的乌里扬卡放在背上。"


translate chinese day5_us_8e5b9b1b:


    "也许可以用某种办法叫醒她，但是首先我不想去做，而且在这个时候也不差什么。"


translate chinese day5_us_3521c810:


    "还好她不是很重..."


translate chinese day5_us_c048facc:


    "我在广场停了下来，把乌里扬卡放在长椅上，自己赶紧歇一会儿。"


translate chinese day5_us_66f6667a:


    "即使只是一个小女孩，要我背这么长时间也是受不了的。"


translate chinese day5_us_99250f31:


    "天上的星星闪亮着。"


translate chinese day5_us_d9e32f7f:


    th "也许它们不只照亮着我和乌里扬卡，可能还有我出生的城市，还有我过去的家..."


translate chinese day5_us_1b24971b:


    "我的胸口好像感到一阵疼痛。"


translate chinese day5_us_3ef36634:


    "我的眼前呈现出了过去的房间，然后从肚子到嗓子感到一阵痛楚。"


translate chinese day5_us_86074a46:


    "不，这不是我的视觉，只是过去的一点回忆。"


translate chinese day5_us_30f40a77:


    "因为虽然如此，我在这里的五天时间比过去的好几年更像是真正的生活。"


translate chinese day5_us_d5ded2d1:


    "现在我真的不知道是不是想要回去。"


translate chinese day5_us_f0e63108:


    "只有一个问题还在困扰着我——我到底是怎么来到这里的。"


translate chinese day5_us_e3fa1442:


    "我直到现在还没有怎么想过这个问题。"


translate chinese day5_us_cba9268c:


    "我的思想完全被日常的琐事占领。"


translate chinese day5_us_2e35c234:


    "现在为了能够在这里正常的生活，我得对这个地方增强了解。"


translate chinese day5_us_6534fa35:


    "只是即使是一只夜莺也有权利知道自己是被谁关在这里，并且决定自己是否要留下来..."


translate chinese day5_us_849ef2d5:


    "我不知道自己还会在这种想法里沉浸多长时间，不过乌里扬卡响亮的呼噜声把我带回了现实。"


translate chinese day5_us_d9d3457b:


    me "这么小的人，呼噜声却这么大..."


translate chinese day5_us_c4195e16:


    "我叹叹气，再次把乌里扬娜背起来，朝她的房间进发。"


translate chinese day5_us_a20cefa7:


    "..."


translate chinese day5_us_12b2a39f:


    "我没有向阿丽夏解释的想法，所以我只是把乌里扬卡放在了房门外，悄悄门，然后马上就离开了。"


translate chinese day5_us_cf46738c:


    "我带着复杂的情感回到了奥尔加·德米特里耶夫娜的房间。"


translate chinese day5_us_1531a117:


    "一方面来说，我做了自己想要做的事情—安慰乌里扬卡。"


translate chinese day5_us_6aec4a07:


    "另一方面..."


translate chinese day5_us_d05290b8:


    "我手里的床单更像是紧身衣。"


translate chinese day5_us_bb449b79:


    "我慢慢的打开门走进去。"


translate chinese day5_us_105fc865:


    mt "谢苗!"


translate chinese day5_us_b3e320ef:


    "奥尔加·德米特里耶夫娜坐在桌子旁边，看起来她似乎在等着我。"


translate chinese day5_us_79427e67:


    mt "你有没有什么想要解释的?"


translate chinese day5_us_377d0453:


    me "那个，请不要再责备她，全都是我的错!"


translate chinese day5_us_47603f81:


    "出乎自己意料的，我就这样成为了一个英雄。"


translate chinese day5_us_3ed3fd1f:


    "我的舌头比大脑反应快。"


translate chinese day5_us_8c77f545:


    "也许这是我一直不为人知的品质。"


translate chinese day5_us_16f0739b:


    "人性和相对的常识。"


translate chinese day5_us_4354ab86:


    mt "真的?"


translate chinese day5_us_cce98559:


    "我准备好人做到底。"


translate chinese day5_us_0b7ff73e:


    me "那个，是我找到的床单，当时我也站在树后面。"


translate chinese day5_us_cc347b04:


    mt "什么树后面?"


translate chinese day5_us_0193dcd6:


    "辅导员惊讶的看着我。"


translate chinese day5_us_ee5de7cc:


    mt "所以说你用床单做什么?"


translate chinese day5_us_220e2848:


    "我意识到了自己的错误。"


translate chinese day5_us_bc2d73a5:


    me "所以说你没有说森林的事情吗?"


translate chinese day5_us_5afe237b:


    mt "谢苗，我不知道你在说什么。"


translate chinese day5_us_13ed3d89:


    mt "我本来想要问问你在远足的时候突然消失去做什么了，不过我现在对你的床单的故事也很感兴趣。"


translate chinese day5_us_87f8ffa1:


    th "可是她还有其他人都没看到乌里扬卡愚蠢的表演？那是不可能的!"


translate chinese day5_us_e65fa514:


    th "哄堂大笑的少先队员们不可能只是我的想象。"


translate chinese day5_us_f41a46e2:


    me "奥尔加·德米特里耶夫娜，我是认真的，你难道没有看到谁披着床单想你冲过去吗?"


translate chinese day5_us_de5231d6:


    mt "所以说，那是你?"


translate chinese day5_us_8fb93608:


    "她靠近了看着我。"


translate chinese day5_us_c20bb56d:


    me "不，那不是我..."


translate chinese day5_us_85c3d025:


    th "难道她看不出身高的不同吗?"


translate chinese day5_us_32446a1f:


    mt "不过你拿着床单..."


translate chinese day5_us_a73acf7d:


    me "是的..."


translate chinese day5_us_d874bb71:


    "我不知道她是把我当成傻子，还是真的不知道发生了什么。"


translate chinese day5_us_3a23059a:


    me "奥尔加·德米特里耶夫娜，就当这个对话从来没有发生过吧，我太累了。"


translate chinese day5_us_d034fa83:


    mt "好吧，去睡觉吧。"


translate chinese day5_us_24edbca1:


    "让我吃惊的是，她同意了。"


translate chinese day5_us_db925e9e:


    "当然我很吃惊，不过我决定将计就计，于是抱起床单躺在床上。"


translate chinese day5_us_4c4a4071:


    "但是我睡不着。"


translate chinese day5_us_8d27541d:


    "没有什么想法，我的头很疼，但是我就是睡不着。"


translate chinese day5_us_9db6bada:


    "我辗转反侧，回忆着一整天的事情。"


translate chinese day5_us_e7fb5b3f:


    "我闭上眼睛，想要让它们消失，但是做不到。"


translate chinese day5_us_e227a4a1:


    "突然间，我听到窗户上的一阵敲击声。"


translate chinese day5_us_f9ca50cb:


    "奥尔加·德米特里耶夫娜好像已经睡着了。"


translate chinese day5_us_ce13ac06:


    "我穿上衣服走出来。"


translate chinese day5_us_a35885f7:


    "乌里扬卡站在我的面前，顽皮的笑着。"


translate chinese day5_us_92e87dbb:


    us "你是怎么把我运回来的，是不是很难啊?"


translate chinese day5_us_f0de0297:


    me "还好吧，你来做什么?"


translate chinese day5_us_047a7a48:


    "我虽然睡不着，但是床是我唯一思念的地方，所以乌里扬卡的造访没有让我很开心。"


translate chinese day5_us_c198f325:


    us "她怎么样，你有没有被批评?"


translate chinese day5_us_be6d9222:


    me "没有，总之是糊弄过去了..."


translate chinese day5_us_4275b6c0:


    us "那太好了！我总是运气很好!"


translate chinese day5_us_d0119426:


    me "那是..."


translate chinese day5_us_645ce19d:


    "我的眼皮开始打架，看来这次是真的坚持不住了。"


translate chinese day5_us_cdcc908e:


    me "看，我非常累了..."


translate chinese day5_us_0f78845d:


    us "很快的，闭上眼。"


translate chinese day5_us_e0f3d01c:


    "这很简单，我也不想知道她要做什么，也许是想要逃跑吧。"


translate chinese day5_us_5cbb3956:


    "我闭上了眼睛。"


translate chinese day5_us_2bd7350e:


    "我感觉嘴唇被轻轻的亲吻的一下。"


translate chinese day5_us_5d1fc15b:


    "我睁开眼睛，不过发现乌里扬卡已经跑开了，同时向我挥着手。"


translate chinese day5_us_1d1a05f7:


    "我呆呆的站着，什么也喊不出来。"


translate chinese day5_us_74510c40:


    "我不知道自己站了多长时间，不过最终夜晚的寒风终止了我的恍惚。"


translate chinese day5_us_78b47b21:


    "我颤抖着回到床上。"


translate chinese day5_us_eff30d1d:


    "我本来还想把这件事想清楚，不过我的身体好像听了眼睛的指挥，我还没有明白过来就已经睡着了..."


translate chinese day5_un_b3a56e2b:


    "只有列娜和阿丽夏站在这里。"


translate chinese day5_un_9494f7ea:


    "当然阿丽夏像这样和别人吵架是很正常的。"


translate chinese day5_un_49a0c8bc:


    "不过听到列娜高声的争吵..."


translate chinese day5_un_9844ee70:


    "我悄悄的走近，想要弄清楚发生了什么。"


translate chinese day5_un_6ca87438:


    dv "不，你听我说!"


translate chinese day5_un_8c9cf7c5:


    un "我什么也不听!"


translate chinese day5_un_8eaadb05:


    dv "真的？那好!"


translate chinese day5_un_4e3a9f81:


    "阿丽夏转过身，然后看到了我。"


translate chinese day5_un_0961f894:


    "刚开始她肯定没有反应过来，不过接下来..."


translate chinese day5_un_7f5542bc:


    dv "所以说，你在偷听!"


translate chinese day5_un_12887db1:


    me "什么？我？没有！"


translate chinese day5_un_712bd43f:


    "她开始仔细的观察我，我逐渐后退。"


translate chinese day5_un_ee80f2e5:


    "而列娜还是站在原地。"


translate chinese day5_un_607f19ad:


    dv "好了，好了，都听着!"


translate chinese day5_un_c992f6a8:


    "阿丽夏停下来，转向列娜。"


translate chinese day5_un_6aa66cea:


    dv "对咯，你知道他今天偷看我来着吗?"


translate chinese day5_un_3875ba7b:


    un "什么?"


translate chinese day5_un_91748180:


    dv "对，他偷看来着，而且全都看到了!"


translate chinese day5_un_f328f2ed:


    un "那，那是真的吗?"


translate chinese day5_un_52e59e20:


    "列娜扭曲着手臂，看了我一眼，然后又看向地面，好像失去了和世界的联系。"


translate chinese day5_un_127f1428:


    me "不，不，没有发生那样的事情!"


translate chinese day5_un_806ed8e6:


    "我愤怒的看了阿丽夏一眼，走向列娜。"


translate chinese day5_un_32dec18e:


    dv "你为什么要否认呢？我都看到了!"


translate chinese day5_un_246d6902:


    me "这更加糟糕!"


translate chinese day5_un_05eee1a3:


    dv "那么你还坚持说没有看到吗?"


translate chinese day5_un_5bf1c5fc:


    "我想了一秒钟。"


translate chinese day5_un_756d591a:


    th "不过说真的我为什么要在列娜的面前竭力维持自己的清白？"


translate chinese day5_un_9cc4691d:


    th "我怎么知道她是怎么想的?"


translate chinese day5_un_cd4b1ccb:


    me "没有那回事!"


translate chinese day5_un_b7eb3fd9:


    "列娜无力的看着阿丽夏然后又充满期望的看着我。"


translate chinese day5_un_71c04b4b:


    "继续看着那双眼睛变得很困难。"


translate chinese day5_un_0d6ab95e:


    me "没有那回事..."


translate chinese day5_un_cacc8d60:


    "我又心虚的重复了一遍。"


translate chinese day5_un_ba1e125e:


    dv "算了，随便你。"


translate chinese day5_un_e36048c5:


    "阿丽夏从我的身后冷笑着。"


translate chinese day5_un_c6c151b9:


    dv "你自己决定相信谁。"


translate chinese day5_un_97aaae01:


    "她向列娜说道，然后转身走向篝火。"


translate chinese day5_un_50793df0:


    un "是真的吗?"


translate chinese day5_un_56ad8efd:


    "我感觉很不舒服，就像是一条鱼离开了水，只想赶快结束这一切。"


translate chinese day5_un_ba176c8b:


    th "说实在的我为什么要和她解释呢?"


translate chinese day5_un_5156bbe7:


    me "嗯，是真的怎么了?"


translate chinese day5_un_5dc693b2:


    "我大大咧咧的看着列娜，不过注意到夕阳照在她的脸上映射出一滴眼泪。"


translate chinese day5_un_e6d47aad:


    me "不，我是说..."


translate chinese day5_un_f203e664:


    un "你不需要撒谎。"


translate chinese day5_un_0c5b6d1e:


    "她擦掉眼泪，试图露出微笑。"


translate chinese day5_un_2060fc9e:


    un "再怎么说，这也和我没有关系。"


translate chinese day5_un_f32b3a36:


    me "不，为什么..."


translate chinese day5_un_f3c734aa:


    th "不过这是为什么?"


translate chinese day5_un_35b0d3f6:


    me "可是那只是一场意外，由于乌里扬卡导致的，意外，只此而已。"


translate chinese day5_un_6e34d368:


    un "是啊，当然..."


translate chinese day5_un_85bc2437:


    me "是真的!"


translate chinese day5_un_e71ea824:


    un "我相信你。"


translate chinese day5_un_89436b45:


    "列娜说什么都没有感情，好像都和她没有关系似的。"


translate chinese day5_un_76748f71:


    "我几乎要相信她了，不过几秒钟之前她的眼泪..."


translate chinese day5_un_14dd805c:


    me "看上去可不是这样的。"


translate chinese day5_un_5c9d8120:


    un "那是什么啊，是一个游戏吗?"


translate chinese day5_un_29ff2cb1:


    "列娜轻轻的说道，不过她的声音里有一丝愤怒。"


translate chinese day5_un_0a753ae6:


    un "你真的需要让我相信吗？而且就算我相信了又能怎样？"


translate chinese day5_un_ce5015a9:


    me "我不想让你相信任何事情，我只是想让你知道我是无辜的..."


translate chinese day5_un_ffacfd31:


    un "我为什么要在乎?!"


translate chinese day5_un_f78c17ba:


    "列娜喊了起来。"


translate chinese day5_un_a7f39912:


    "我背对着篝火，但是可以想象到大家投向这里的目光。"


translate chinese day5_un_fd952043:


    un "你如果想要偷看谁的话，就去看吧！为什么要和我扯上关系?"


translate chinese day5_un_f8882e89:


    me "我没有..."


translate chinese day5_un_427acce6:


    "不过怎么看我都没有任何胜利的希望，就像是一个犯了错误的小孩子..."


translate chinese day5_un_b24ff123:


    un "你随便！我不知道!"


translate chinese day5_un_5cd48f53:


    "列娜转过身去，往营地走。"


translate chinese day5_un_6f3fa70b:


    "我没有尝试阻止她，现在看来，那样做也不太合适。"


translate chinese day5_un_5307c03e:


    "她正处在临界点上，这样做不会管用的。"


translate chinese day5_un_c03bbe86:


    th "她什么时候变成那样子了...?"


translate chinese day5_un_37a8623c:


    "我想着这些事情，坐在篝火旁边。"


translate chinese day5_un_ce617998:


    "..."


translate chinese day5_un_dbeba143:


    dv "所以?"


translate chinese day5_un_a07446cb:


    "阿丽夏坐在边上问道。"


translate chinese day5_un_ad02f53a:


    me "什么?"


translate chinese day5_un_1a47cd1e:


    dv "不太成功啊，是不是?"


translate chinese day5_un_2abbfc44:


    me "你都看到了..."


translate chinese day5_un_3792f79f:


    "我一边捅着煤一边愤怒的说道。"


translate chinese day5_un_e73a96b8:


    "现在完全变暗了。"


translate chinese day5_un_87200809:


    "眼前的景象就像是童年的噩梦，漆黑的森林中突然冒出鬼怪，就连老鼠都会变成妖怪。"


translate chinese day5_un_3c849f9b:


    "我倒不是很害怕，经过昨天旧营地的地牢之后所有的事情都不同了..."


translate chinese day5_un_d74fcdd1:


    "月亮照耀着勇敢的少先队，寂静的夜晚酝酿着拂晓。"


translate chinese day5_un_e9c4c23a:


    dv "就跟我想的一样，虽然已经想到了。"


translate chinese day5_un_fda262d8:


    me "可是为什么...?"


translate chinese day5_un_aef6d994:


    "我终于找到了一块很大的燃料，投入火堆中后溅起了一米高的焰浪。"


translate chinese day5_un_77bf3519:


    me "列娜经常像那个样子吗?"


translate chinese day5_un_185b84dd:


    dv "怎么?"


translate chinese day5_un_56fc9a67:


    me "朝别人大喊大叫，我感觉她平时不是这个样子的啊。"


translate chinese day5_un_989f2bee:


    dv "她也是人，就像其他人一样。"


translate chinese day5_un_93c166ce:


    "阿丽夏傻笑着。"


translate chinese day5_un_32fca04b:


    dv "好像你以前没有注意到似的。"


translate chinese day5_un_0f99a0a2:


    me "没有注意到什么?"


translate chinese day5_un_5e9fa3e4:


    dv "她。"


translate chinese day5_un_e54b39e9:


    me "什么意思?"


translate chinese day5_un_b08ce089:


    dv "我是说这就是她的本来面目。"


translate chinese day5_un_ec069979:


    me "什么叫本来面目？"


translate chinese day5_un_2e4631df:


    dv "啊，你太笨了! "


translate chinese day5_un_34dc294a:


    me "你把话说完！"


translate chinese day5_un_44462a0c:


    dv "她不是那个样子的，不是你原先想象的那个样子的。"


translate chinese day5_un_95364e9c:


    me "那么她到底是?"


translate chinese day5_un_6fbef0db:


    dv "好了，我受不了了..."


translate chinese day5_un_3f845aea:


    "阿丽夏站起来，想要离开。"


translate chinese day5_un_bd114f34:


    "我什么都没有说，只是坐在这里，看着寂静的森林。"


translate chinese day5_un_1c95b08d:


    dv "顺便告诉你，如果你想要找她的话，她应该在小岛上。"


translate chinese day5_un_278fc1b6:


    me "什么小岛?"


translate chinese day5_un_5f02cbe1:


    dv "你今天采集草莓的小岛。"


translate chinese day5_un_b91fd3d7:


    me "她在那里做什么?"


translate chinese day5_un_d7920d68:


    dv "啊，你真是个傻子!"


translate chinese day5_un_42e0fed6:


    "阿丽夏跺着脚，坐在篝火对面。"


translate chinese day5_un_ff34e775:


    "我一定是没有注意到什么。"


translate chinese day5_un_6615be77:


    "我的大脑一片空白。"


translate chinese day5_un_ff8730b2:


    "准确的说，是沉沉的，完全转不动。"


translate chinese day5_un_e447cec9:


    "如果我的大脑原来是一条车水马龙的高速公路的话，现在只是一条蜿蜒曲折罕有人至的林间小道。"


translate chinese day5_un_643db9b6:


    "我试图想清发生了什么事情，不过我好像就在撞击一面隐形的墙。"


translate chinese day5_un_0877776f:


    "而且我的内心深处没有一点反应，好像这件事是发生在别的什么人身上一样。"


translate chinese day5_un_8d9c0b6a:


    "我向列娜证明自己清白的努力失败了，不过那有必要吗?"


translate chinese day5_un_dc1bef5c:


    "列娜哭了，跑开了，然后呢?"


translate chinese day5_un_0845afe0:


    "我一点感觉也没有，我应该有感觉吗?"


translate chinese day5_un_7c882346:


    "我咬着嘴唇，咬出了血，然后站了起来。"


translate chinese day5_un_bbae1908:


    "我受不了在这里坐着，周围一群少先队员。"


translate chinese day5_un_5ba34865:


    th "我要离开，到远处去，到什么地方，到自己的空间。"


translate chinese day5_un_fbc3bc79:


    "我看准辅导员走神的瞬间，一溜烟消失在森林中。"


translate chinese day5_un_a20cefa7:


    "..."


translate chinese day5_un_6219e64c:


    "已经很久没有下雨了。"


translate chinese day5_un_c6749284:


    th "不过我完全不知道这个地方会不会下雨。"


translate chinese day5_un_5d4ba34b:


    th "真是愚蠢，植物当然需要潮湿的环境。"


translate chinese day5_un_cb1b2ded:


    "夜晚比下午的空气要清新，不过还没有完全冷却下来，我的大脑也还没有冷静下来。"


translate chinese day5_un_40724f26:


    "突然我意识到我想要游泳。"


translate chinese day5_un_489cb8db:


    "这是很正常的感觉，因为天真的很热，我全身都是汗，一天也下不去。"


translate chinese day5_un_1056c0d4:


    "我甚至注意到我已经接近了码头。"


translate chinese day5_un_d177ba32:


    th "为什么不是沙滩?"


translate chinese day5_un_9bb6a06d:


    "我又想到了列娜，我一直试图忘掉的东西又一次全都出现了。"


translate chinese day5_un_afb26968:


    "显然我是被自己的潜意识带到码头的。"


translate chinese day5_un_17e84712:


    th "不过我内心想要什么?"


translate chinese day5_un_4f586330:


    th "和她道歉？向她解释？不像。"


translate chinese day5_un_eb0d22d4:


    "我只是想要看到她的一些反应，听到她说“我理解你”然后微笑。"


translate chinese day5_un_304c60e6:


    "然后我才能把自己从这些感觉中解脱出来…"


translate chinese day5_un_a78ee8f1:


    "在这个世界里我也很渴望被谁理解。"


translate chinese day5_un_21fa3a03:


    "我解开了一条船，推入水中，然后拿起了船桨。"


translate chinese day5_un_ce617998_1:


    "..."


translate chinese day5_un_1607c44f:


    "这次要省力一些，虽然我的手臂还是很疼，不过我设法学到了一些划船的技巧。"


translate chinese day5_un_134ecd94:


    "河水在小船的下面，好像柔软透明的轻纱。"


translate chinese day5_un_cdd6fc1f:


    "月光射入水底，让人几乎能看到。"


translate chinese day5_un_1b592616:


    "我开始划船。"


translate chinese day5_un_ce617998_2:


    "..."


translate chinese day5_un_9f7443d1:


    "出人意料的是，这里和白天看起来没有什么不同。"


translate chinese day5_un_f47c3547:


    "这里的夜晚往往都和白天完全不同的，是一个只属于黑夜的世界，一个神秘的世界。"


translate chinese day5_un_b94320d8:


    "我慢慢的在岛上转着，寻找着列娜来的时候划的船。"


translate chinese day5_un_35778f50:


    "脚下的草丛沙沙的响着，岸边的浪花循回往复。"


translate chinese day5_un_575aa0ca:


    "水面上的凉风吹来，让树林充满了生机。"


translate chinese day5_un_0bba232e:


    "我就沉浸在这幅美妙的景色之中，然后突然绊倒在什么上面。"


translate chinese day5_un_44e820c3:


    "是一条小船。"


translate chinese day5_un_0c66a774:


    th "是啊，她不可能是游泳过来的。"


translate chinese day5_un_c486b958:


    "我朝着小岛的中心走去。"


translate chinese day5_un_ce617998_3:


    "..."


translate chinese day5_un_b2eec26c:


    "走过几百米之后，我突然听到树林里一阵响声。"


translate chinese day5_un_4dcf727f:


    un "不要过来..."


translate chinese day5_un_5f75988c:


    "列娜轻轻说着。"


translate chinese day5_un_51149e68:


    me "..."


translate chinese day5_un_70edb764:


    "我犹豫了。"


translate chinese day5_un_0ca1cc9c:


    un "不要过来。"


translate chinese day5_un_78558274:


    "她提高了声音。"


translate chinese day5_un_3efb73c2:


    me "你怎么知道是我的?"


translate chinese day5_un_edfa793d:


    "虽然她并没有说她知道是我。"


translate chinese day5_un_65bf82fc:


    me "好吧..."


translate chinese day5_un_f4fd5ba0:


    "我靠在树上，试图不往后面看。"


translate chinese day5_un_ad697c77:


    me "阿丽夏告诉我你会在这里。"


translate chinese day5_un_ca19d4ba:


    un "然后呢?"


translate chinese day5_un_801934ec:


    "我很难理解列娜现在的情感。"


translate chinese day5_un_d382dd22:


    "她的声音很平稳，虽然我可以感觉到她的不耐烦。我难以理解她为什么会生气。"


translate chinese day5_un_13bf83a1:


    me "这有些尴尬。"


translate chinese day5_un_7a2f384f:


    "我尽力避免毫无意义的道歉，不过似乎很困难。"


translate chinese day5_un_e5bf1202:


    un "那就是你来这里的唯一原因吗?"


translate chinese day5_un_e7f8d1d4:


    me "不...那个，我也不知道。"


translate chinese day5_un_3c94e6e3:


    un "你不知道，但是还是来了?"


translate chinese day5_un_fc30fd2b:


    me "是啊。"


translate chinese day5_un_334f3921:


    un "你不应该来的。"


translate chinese day5_un_839ef62e:


    me "为什么啊？当然，如果我打扰到了你..."


translate chinese day5_un_c9d68cca:


    un "你为什么要跟踪我?"


translate chinese day5_un_48199bb7:


    me "我..."


translate chinese day5_un_4a3d8c67:


    "她说得对，看起来是这个样子的，而且我确实被她所吸引。"


translate chinese day5_un_84f2a0fc:


    me "你不应该这样想。"


translate chinese day5_un_1adc8b36:


    un "我应该怎样想?"


translate chinese day5_un_595d7b8c:


    me "我不知道。"


translate chinese day5_un_8d18dd8b:


    un "我只能根据你的行为来判断。"


translate chinese day5_un_9044e6ea:


    me "我真的不知道...也许我应该走了。"


translate chinese day5_un_8477995f:


    "我很困惑，而且呆在她的旁边很难受。"


translate chinese day5_un_984ea120:


    un "那为什么？既然你都已经来了..."


translate chinese day5_un_44251c8f:


    "她的声音里似乎有一点轻佻。"


translate chinese day5_un_01822020:


    me "好吧。"


translate chinese day5_un_77825d23:


    un "然后呢?"


translate chinese day5_un_ad02f53a_1:


    me "啥?"


translate chinese day5_un_e9197f7e:


    un "阿丽夏和你说什么了?"


translate chinese day5_un_668b75c5:


    me "没有什么特殊的。"


translate chinese day5_un_8bf5923b:


    un "我明白了。"


translate chinese day5_un_fc30fd2b_1:


    me "是啊。"


translate chinese day5_un_4924952d:


    un "那好吧。"


translate chinese day5_un_751ac511:


    me "对..."


translate chinese day5_un_40151fe6:


    "我们沉默了一段时间。"


translate chinese day5_un_b8d98c47:


    un "既然你都来了，那就跟我说点什么吧。"


translate chinese day5_un_e3b565dd:


    me "那个，我不知道..."


translate chinese day5_un_8ee362ac:


    un "比如说，今天早晨到底发生了什么。"


translate chinese day5_un_2ea0a340:


    me "什么发生了什么?"


translate chinese day5_un_5b543368:


    un "这你知道。"


translate chinese day5_un_142ff09f:


    me "你自己好像不想谈这个。"


translate chinese day5_un_7af3f67c:


    un "我原来不想谈，但是现在想了!"


translate chinese day5_un_ea56876b:


    "列娜的声音有些颤抖。"


translate chinese day5_un_2fc43074:


    "我已经完全不明白到底发生了什么了。"


translate chinese day5_un_968df1e5:


    th "也许躲在树丛里的不是她?"


translate chinese day5_un_a8a3ea5d:


    un "别看!"


translate chinese day5_un_7c18f5e8:


    me "好吧，好吧...可是到底怎么了"


translate chinese day5_un_d893718b:


    un "没什么，别看就好。"


translate chinese day5_un_67742710:


    me "好吧，听你的。"


translate chinese day5_un_c928b72a:


    un "所以说，什么都没有发生吗?"


translate chinese day5_un_a336db31:


    me "那个，其实发生了一些事情...不过那只是一次愚蠢的意外!"


translate chinese day5_un_4f1a98c1:


    un "那么你为什么担心我知道这件事情?"


translate chinese day5_un_7cd08f32:


    me "我没有担心!"


translate chinese day5_un_0594d317:


    me "怎么都是你自己!"


translate chinese day5_un_505ae84f:


    "我有点恼怒的大声喊道。"


translate chinese day5_un_acb49e13:


    un "什么我自己？"


translate chinese day5_un_f2ac40dd:


    me "你为什么要关心这件事?"


translate chinese day5_un_3537de2e:


    un "谁说我关心这件事了?"


translate chinese day5_un_2c8023fc:


    me "那又怎么了?"


translate chinese day5_un_62592cb0:


    un "没什么..."


translate chinese day5_un_3d26df77:


    "她悄悄的说道，然后沉默了。"


translate chinese day5_un_e6ab9030:


    "这样的对话可没个完。"


translate chinese day5_un_1826deee:


    th "列娜什么也不说，我又什么也猜不到。"


translate chinese day5_un_ae93dffd:


    me "抱歉，我觉得这是我的错，只是我没有办法理解..."


translate chinese day5_un_962ad609:


    un "为什么..."


translate chinese day5_un_ad02f53a_2:


    me "什么?"


translate chinese day5_un_68ca672f:


    un "你为什么要道歉，为所有事道歉，即使是你没有做的事情？"


translate chinese day5_un_a337cbd5:


    me "可..."


translate chinese day5_un_0f57e6b5:


    "也许她是对的，但是我也没有别的办法，我觉得自己愧对大家。"


translate chinese day5_un_336e6bce:


    "这样他们就不会再怨恨我，我也可以消除误会。"


translate chinese day5_un_7259241d:


    un "总之，那是你的事情!"


translate chinese day5_un_339b95c7:


    "列娜生气了。"


translate chinese day5_un_ca9b074e:


    un "道歉，找借口，管我什么事...?"


translate chinese day5_un_607b607b:


    me "好吧，我是清白的，我没有犯任何错误，这怎么了?"


translate chinese day5_un_1e693cd1:


    un "为什么总是和我过不去?"


translate chinese day5_un_6478a1d7:


    me "不知道...我就是这么想的。"


translate chinese day5_un_6355a504:


    un "噢！你就是这么想的!"


translate chinese day5_un_0467a4c4:


    "她笑了。"


translate chinese day5_un_27b88dd3:


    un "你好像多么了解我似的。"


translate chinese day5_un_51149e68_1:


    me "..."


translate chinese day5_un_cbc4bb84:


    "我没有回答。"


translate chinese day5_un_af01240b:


    un "走吧..."


translate chinese day5_un_850cca06:


    "过了一会儿，列娜说道。"


translate chinese day5_un_a09db615:


    "我没有走，我迈不开步子，就是迈不开。"


translate chinese day5_un_d2f6ef68:


    "我这个时候什么都不想，不想解释，也不求她理解。"


translate chinese day5_un_8ef1b6f9:


    "这一切都让我好累。"


translate chinese day5_un_5ac38cf1:


    me "为什么?"


translate chinese day5_un_f174e90e:


    un "走吧..."


translate chinese day5_un_e3a26dcc:


    "她悄悄的说道。"


translate chinese day5_un_1457a622:


    me "我不想走。"


translate chinese day5_un_380bd525:


    un "那我走。"


translate chinese day5_un_bf0dc08a:


    me "咱们一起走吧。"


translate chinese day5_un_049b021f:


    un "不。"


translate chinese day5_un_35d834dd:


    me "你还要生气多久？这里所有的人都很正常，除了你！"


translate chinese day5_un_3fddb229:


    "我不太在乎自己说了什么，好像是别的人说出来的。"


translate chinese day5_un_5dedc88b:


    un "你真的什么都不懂，阿丽夏说得对。"


translate chinese day5_un_4bf0d4a0:


    me "不懂什么?"


translate chinese day5_un_20163f63:


    un "别管了..."


translate chinese day5_un_0b35a130:


    "我闭上眼睛开始思考。"


translate chinese day5_un_475b9270:


    th "不行，我不能走，我不明白。"


translate chinese day5_un_abacf06a:


    th "而且从什么时候开始我不再在乎自己为什么来到这个世界，又该怎么回去?"


translate chinese day5_un_d8c918ef:


    th "什么时候我唯一在乎的事情是列娜，还有其他人怎么看待我?"


translate chinese day5_un_56543886:


    th "这不像是我自己，这很愚蠢，这很不合适。"


translate chinese day5_un_aed8e1b2:


    me "总的来说，我什么也没有做，也不想纠缠自己没有做的事情!"


translate chinese day5_un_dfffcbac:


    "没有回答。"


translate chinese day5_un_4183115b:


    me "嘿！你听到我说话了吗?"


translate chinese day5_un_d7d3178a:


    "我最后决定看看谁藏在树后面。"


translate chinese day5_un_6807a309:


    th "她走了!"


translate chinese day5_un_e9ad8eab:


    "我赶快跑回列娜的小船那里，不过她已经离开很远，快要抵达码头了。"


translate chinese day5_un_8f338516:


    me "算了!"


translate chinese day5_un_8494703d:


    "我一边喊着一边沿着岸边走着。"


translate chinese day5_un_ce617998_4:


    "..."


translate chinese day5_un_2c35439c:


    "返程时倒是很顺利。."


translate chinese day5_un_9d79bcf3:


    "不过我的手臂还是疼的要命，而且眼睛几乎睁不开了。"


translate chinese day5_un_dd481fe9:


    "也许这样的身体和精神压力有点过大了。"


translate chinese day5_un_961bdcdb:


    "我慢慢的走回去，朝着辅导员宿舍的方向，心理什么也没有想。"


translate chinese day5_un_793d9878:


    "有人在广场叫我。"


translate chinese day5_un_93b5fc50:


    dv "嘿!"


translate chinese day5_un_53356b4d:


    "阿丽夏跑了过来。"


translate chinese day5_un_2cf26385:


    dv "你去过小岛了吗？"


translate chinese day5_un_9513cd87:


    me "是啊..."


translate chinese day5_un_d331deef:


    "说实话我什么都不想告诉她，不过我已经没有力气撒谎了。"


translate chinese day5_un_9c8b084d:


    dv "进展怎么样?"


translate chinese day5_un_e5f72f43:


    me "你不要管...我累了，想要睡觉。"


translate chinese day5_un_a7c99a15:


    dv "哎呀，告诉我啦!"


translate chinese day5_un_48f402f2:


    "她做着令人反感的鬼脸，把我气得发抖。"


translate chinese day5_un_3f72c7b7:


    me "关你什么事?!"


translate chinese day5_un_12b03082:


    dv "那个，我只是..."


translate chinese day5_un_5449a6fc:


    "她失望的嘀咕着。"


translate chinese day5_un_cc04c95f:


    me "管好你自己的事情!"


translate chinese day5_un_ea222abb:


    "我厉声说道，然后加快了自己的步伐。"


translate chinese day5_un_206f4c0d:


    "阿丽夏没有试图阻止我。"


translate chinese day5_un_1e4d5480:


    "可是让人恼火的是，奥尔加·德米特里耶夫娜还没有回来，我自己又忘了带钥匙。"


translate chinese day5_un_325046a9:


    th "我要是丢了怎么办?"


translate chinese day5_un_b5d9b711:


    "我只能等着。"


translate chinese day5_un_95440e29:


    "我扑通的坐在椅子上，闭上眼睛。"


translate chinese day5_un_3eda7fc1:


    "我的心感觉很沉重，我的灵魂有一种被撕裂的感觉。"


translate chinese day5_un_71767923:


    "不时地还有已经死掉的感觉，就像是掉进了地狱一样。"


translate chinese day5_un_ef43a5c3:


    th "而事实上，我不但没有离开这里的希望，反而越来越陷入这个夏令营中烦人的事务。"


translate chinese day5_un_f6d61e07:


    "就好像我从来没有过以前真实的生活一样。"


translate chinese day5_un_6a8003ef:


    "好像我一直对别人的想法很感兴趣似的..."


translate chinese day5_un_0730defb:


    th "糟糕！我不应该在乎!"


translate chinese day5_un_226088e0:


    th "为什么就在这里？为什么就在这个时候？"


translate chinese day5_un_701c073f:


    "我会想起列娜哭泣的脸。"


translate chinese day5_un_79fc3a70:


    th "是啊，也许别人的想法不在乎，不过我在乎列娜... "


translate chinese day5_un_195bd715:


    "我远远的听到脚步声，然后奥尔加·德米特里耶夫娜出现了。"


translate chinese day5_un_fb7d3b52:


    "她看着我，好像想要说些什么，不过接下来叹了一口气，打开了房门。"


translate chinese day5_un_f73e0745:


    "我跟着她。"


translate chinese day5_un_62928289:


    "模糊的身影，碎片的回忆，以及各种感觉，在我的头脑中盘旋。"


translate chinese day5_un_da141c38:


    "过了很久我也想不出自己在什么地方干什么事情。"


translate chinese day5_un_621dba00:


    "唯一的解脱就是去睡觉..."
# Decompiled by unrpyc: https://github.com/CensoredUsername/unrpyc
