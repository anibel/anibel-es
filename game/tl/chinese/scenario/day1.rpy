
translate chinese day1_e9644438:


    "不久，突然感觉阳光刺着我的眼。"


translate chinese day1_8e82fc0a:


    "我刚开始并没有注意，以为只是还没有睡醒罢了。"


translate chinese day1_44c308e2:


    "我不经思考的向车门走去。"


translate chinese day1_ccc28a84:


    me "糟糕，我好像坐过站了。"


translate chinese day1_aee379b4:


    "但是这里没有门..."


translate chinese day1_d42615e7:


    "我环顾四周，发现这辆车不再是一辆破旧的LiAZ，而是变成了Icarus还是全新的！"


translate chinese day1_03cecdb6:


    "我震惊了..."


translate chinese day1_3665cd10:


    th "这怎么{w} ，怎么回事？{w}难道这里是死后的世界？"


translate chinese day1_2df08a3e:


    th "还是说我被绑架了？"


translate chinese day1_a23b7265:


    th "不对，我肯定是死了。"


translate chinese day1_a2eb760d:


    "我疯狂的抽自己嘴巴，然后以头抢地..."


translate chinese day1_892f12f3:


    th "这下清楚了：要么我还活着，要么人死了之后还有知觉。"


translate chinese day1_b55f45ec:


    th "可这到底是怎么回事？"


translate chinese day1_8b1a1045:


    th "也许我睡的时间太长了，车已经到了停车场？"


translate chinese day1_fa8f47fe:


    th "然后呢，他们把我扛到另一辆车里接着睡？"


translate chinese day1_05f4c741:


    "我急忙出去打算观察一下周围的环境。"


translate chinese day1_7fd7201a:


    "到处是绿色的植被：路旁高高的草丛，树林，野花。"


translate chinese day1_0c35f127:


    th "夏天！{w}这怎么回事？{w}刚才还是冬天的说..."


translate chinese day1_ae3344e7:


    "我的头疼的要命，{w}好像要爆炸了一样。"


translate chinese day1_2208f558:


    "慢慢的，我开始想起一些片段。"


translate chinese day1_8d858ed9:


    "消失在远方的公路；森林，平原，田野，湖水，接着还是森林。"


translate chinese day1_786f868f:


    th "我觉得我是在睡觉，那我怎么会记得这些事情？"


translate chinese day1_2f605107:


    "然后是...{w}一条裂缝..."


translate chinese day1_5fabbbed:


    "有哪个女孩靠过来。"


translate chinese day1_73437152:


    "她在我耳边轻声细语。"


translate chinese day1_c2d5fb61:


    "然后又是一道裂缝..."


translate chinese day1_2e96e275:


    "然后我醒来就到了这里。"


translate chinese day1_c8d2bde3:


    th "那么，那个奇怪的女孩是谁呢？"


translate chinese day1_a1a6b8d6:


    th "或许那是个梦？"


translate chinese day1_ebfa0491:


    "不知怎么想起她之后我的心情逐渐平静了一些，我感到内心里涌出一股暖流。"


translate chinese day1_fde77cc5:


    th "有没有可能是她把我带到了这里？"


translate chinese day1_9bacf66f:


    th "我需要找到她！"


translate chinese day1_a35e9118:


    th "我必须离开这个鬼地方。"


translate chinese day1_378336bd:


    "我叽里咕噜跑来跑去，抓耳挠腮琢磨该往哪个方向前进，最后我决定往公共汽车开来的方向跑去。"


translate chinese day1_a20cefa7:


    "..."


translate chinese day1_a20cefa7_1:


    "..."


translate chinese day1_723ee7f4:


    "运动起来之后，头脑能变得清醒，让我能对周围的判断更加明白一些。"


translate chinese day1_cb806649:


    "不过那只是美好的想象，我现在坐在路边，大口喘着粗气，让夏天的热气流过自己的喉咙。"


translate chinese day1_6d244e52:


    "不过话说回来，这一通疯跑到底还是有点作用的：心中的恐惧感渐渐淡去了。"


translate chinese day1_adb77bc9:


    th "也许我真的就是在做梦？"


translate chinese day1_93fa4347:


    "不过想起来刚才在车上的自残场景，我又马上否定了这个想法。"


translate chinese day1_fae72ec6:


    me "我既没有在做梦，也没有死掉..."


translate chinese day1_36f36703:


    "一条小路穿过田野，延伸到远方，{w}就是我一直在做的那个梦。"


translate chinese day1_21bd3c65:


    th "我现在一定离家很远。"


translate chinese day1_41a25460:


    "不只是昨天的冬天到今天的夏天的变化。"


translate chinese day1_fe462b7c:


    "而是整个环境的变化。"


translate chinese day1_610fd3ae:


    th "当然，夏天就是这样的：像是充满绿色生机的火炉，但是这里的一切似乎都不是完全的生命体。"


translate chinese day1_b66d48e9:


    "这里的景色就像是19世纪的俄罗斯风景画。"


translate chinese day1_d7a6969b:


    "花草、灌木丛都过于茂盛了，能遮挡住后面的树林..."


translate chinese day1_6482df8a:


    "树木本身也有些奇怪...{w}树林离得很远，但是那些树好像排成了密集的队形，准备向田野这边进军。"


translate chinese day1_46d39e23:


    "我屏住呼吸向刚刚的公共汽车望去，——远的几乎看不见了。"


translate chinese day1_d0f34e2d:


    th "真是好一通跑..."


translate chinese day1_5a5230cb:


    "恐惧再一次袭上心头。"


translate chinese day1_52a8e81e:


    th "啊，那些电线...{w}也就是说这里一定有人！"


translate chinese day1_a53379f8:


    "但是那又{i}意味着{/i}什么？"


translate chinese day1_8d395e64:


    "话又说回来了，根本什么用都没有嘛。{w} 难道地狱里就不能有电线了吗？"


translate chinese day1_fbe4386c:


    "在火堆上炙烤罪人？那是上个世纪的风格..."


translate chinese day1_c88e5ea3:


    "我已经到达了极限，接下来，要么是完全疯掉，要么是试着理解这里到底在发生什么事情。"


translate chinese day1_3cbef58c:


    th "趁着现在还有选择的余地，我还是赶紧选择第二项为妙..."


translate chinese day1_28b9b20f:


    "我开始慢慢的返回公交车的位置。"


translate chinese day1_9aeca35d:


    th "嗯...吓人也是必然的嘛。"


translate chinese day1_d5a8e5e1:


    th "但是我在广袤的田野上能有什么线索呢，还是回去找那个见了鬼的破铜烂铁比较靠谱一点。"


translate chinese day1_a20cefa7_2:


    "..."


translate chinese day1_001631c6:


    th "我应该仔细调查这附近。"


translate chinese day1_3e09185c:


    "一座砖墙，大门上立着一块牌子写着“Совёнок”。两边是少先队员的雕塑，还有一个路牌写着410路公共汽车。"


translate chinese day1_59b146e5:


    me "今天的旅途让我疲惫不堪。"


translate chinese day1_e98bfbde:


    "我开始傻笑起来。"


translate chinese day1_0fa2263b:


    "人在极限情况下会变得不正常。"


translate chinese day1_db78176c:


    th "这就是我现在的状态。"


translate chinese day1_95a9a169:


    "这个地方不像是被废弃的，大门上没有锈迹，墙面也没有损坏。"


translate chinese day1_3ba35159:


    me "'Совёнок'…"


translate chinese day1_33b4e80e:


    th "这是指的神马？"


translate chinese day1_94f91cd7:


    th "从这两座少先队员的雕像来看，这里应该是一座夏令营。{w} 而且好像还在活动中？！"


translate chinese day1_deb4632a:


    th "当然嘛，事情就是这样的，讲道理的话，也讲不出个所以然..."


translate chinese day1_b987229b:


    th "奇怪的女孩、被掉包的汽车、还有这夏令营。"


translate chinese day1_33930872:


    "成千上万个不同的理论在我的心中奔腾...从被外星人绑架到被人催眠，从出现幻觉到时空穿越..."


translate chinese day1_d28308d8:


    "一个比一个糟糕的情况，不过要让我选某一个还不是那么容易选出。"


translate chinese day1_c83682b4:


    "然后我突然想到：我可以试着打个电话！"


translate chinese day1_30421d7d:


    "我掏出手机找到通讯录里的第一个号码就拨了出去。"


translate chinese day1_a5e58fe8:


    "但是手机屏幕上没有信号强度的指示，取而代之的是一个大大的叉叉。"


translate chinese day1_19146449:


    th "好吧，可能这个鸟不拉屎的地方没有信号。"


translate chinese day1_0d3882c7:


    th "不对呀，我不可能是唯一一个来到这里的人！"


translate chinese day1_c52f433f:


    th "公交车又不是汽车人..."


translate chinese day1_9191dff2:


    "我翻来覆去的检查了一遍整个汽车，以确定它不是一个幻象。"


translate chinese day1_841adb1f:


    "底盘有点脏...到处都有点锈，掉了色的喷漆，磨掉一层的轮胎—不对呀，这绝对是一辆很普通的Icarus公交车。"


translate chinese day1_6109f212:


    th "额...就是那种你一睡过了就会把你带到什么不可思议的地方的公交......"


translate chinese day1_0343ddfb:


    "我苦笑了一下。"


translate chinese day1_d4028078:


    "这应该是情不自禁的偶然一笑，{w}因为这可不是幽默细胞爆发的好时机啊..."


translate chinese day1_17403436:


    th "可是这鬼司机去哪儿了？"


translate chinese day1_7530ded5:


    "我谨慎的坐在路边的倒鸭子上等着。"


translate chinese day1_a20cefa7_3:


    "..."


translate chinese day1_4bc2caf0:


    "我没有什么耐心。"


translate chinese day1_ede35d71:


    "我已经焦虑到要发疯的极限了。"


translate chinese day1_c8bc0b5a:


    "这种情况下，任何人的反应都会是差不多的。"


translate chinese day1_0b02c10b:


    "外星人和平行宇宙什么的从我的大脑中消失不见了，剩下的只有无尽的空虚和黑暗。"


translate chinese day1_33b8346c:


    th "这就是我的末日吗？这就是我的{i}人生{/i}终点吗?"


translate chinese day1_0818ff10:


    th "可是我还有好多的心愿，好多事情在等着我去体验的说..."


translate chinese day1_405f9fbf:


    "我被这种可怕的想法淹没，这一定就是我的结局了。"


translate chinese day1_97d1733b:


    th "可这是为什么？"


translate chinese day1_6e7e69ad:


    th "这不公平！我可还算不上是什么‘世界上最邪恶的人’吧..."


translate chinese day1_a72fb0ee:


    th "老天啊，你这是为什么..."


translate chinese day1_69065e88:


    "泪水灼烧着我的眼睛，我全身拧作一团开始在地上打滚。"


translate chinese day1_4629954b:


    me "为毛？为什么？为啥是我！"


translate chinese day1_6f37277c:


    "可是只有那两位沉默的雕塑能够听见我的呐喊，还有栖于枝头的鸟儿，而且它马上就飞走了，还用自己的鸟语叫着，好像在嘲笑哪个傻瓜打搅了它的午睡。"


translate chinese day1_893c0214:


    "只剩下我抽抽搭搭地独自在风中凌乱......"


translate chinese day1_a20cefa7_4:


    "..."


translate chinese day1_1b527b73:


    "又过了一段时间，我又逐渐打起精神来。"


translate chinese day1_659aa794:


    "头脑稍稍又清醒了一些，好像恐惧给了我一点喘口气的时间。"


translate chinese day1_dba992b6:


    th "说来说去，如果有谁要杀了我，那是为了什么呢？"


translate chinese day1_1a34ee7c:


    th "这也不像是个实验。"


translate chinese day1_56b2c610:


    th "如果这真的是什么疯狂的巧合的话，那也就没有什么危险了。"


translate chinese day1_cc407c1e:


    th "总之，目前来说还没有什么危险。"


translate chinese day1_35abcf62:


    "紧张感也就很快消散了。"


translate chinese day1_bbe3feaf:


    "当然，我的太阳穴还在咚咚直响，手也还抖个不停，但至少我又能正常思考了。"


translate chinese day1_007b1ddb:


    th "现在这种情况，我根本什么都做不了嘛。{w}所以随便的行动肯定不会有好结果的。"


translate chinese day1_607957e4:


    th "在任何事实发生之前，胡乱猜想都是没有意义的。"


translate chinese day1_cea13f7e:


    th "在这里闲逛也未必能有所帮助。"


translate chinese day1_f68fbb50:


    "这个夏令营看起来是附近唯一可能有人的地方，所以我打算进门一探究竟，可不等我动身......"


translate chinese day1_cb43b355:


    "一个女孩儿在门口出现了..."


translate chinese day1_c9771ed6:


    "...穿着少先队员的制服。"


translate chinese day1_76dc3c1c:


    th "这次我的大脑正常工作了。"


translate chinese day1_c454f340:


    th "可是一个21世纪的少先队员..."


translate chinese day1_55a53f71:


    th "而且，一个女孩儿，{i}在这儿{/i}？..."


translate chinese day1_a0dd748b:


    "我又僵住了。"


translate chinese day1_888fb514:


    "我的心中回响着唯一一个念头：{w}跑啊！！离开这鬼地方，这汽车，大门，雕塑，还有那见鬼的‘小憩’的鸟。"


translate chinese day1_1a518774:


    "跑啊跑，飘啊飘，像星星眨眼，向银河招手~~"


translate chinese day1_8abc0b99:


    "变成一道脉冲，用残留射线征服星辰大海！"


translate chinese day1_b53ca9d4:


    "不管往哪里跑，只要能{b}离开{/b}这个鬼地方！"


translate chinese day1_d158f7e2:


    "与此同时，女孩儿慢慢接近，对我微笑着。"


translate chinese day1_3fbcaafd:


    "我不禁注意到她漂亮的脸蛋，虽然我还在不停地发抖。"


translate chinese day1_01e37be4:


    "人类本能可以独立于意识，当5%%的大脑功能用来理智思考的时候，剩下的95%%在全力的支持着{i}生命本能{/i}而且特别是内分泌系统。"


translate chinese day1_b619432f:


    "我拼命的想要把大脑中的各种想法理出一个头绪，让不断蹦出来的百科全书的词条停下，虽然我的思路还是像某个蹩脚的犯罪小说里英雄的男主角没完没了的独白一样停不下来。"


translate chinese day1_159ede02:


    "漂亮的斯拉夫小姑娘，两条长长的辫子像是一捆捆的草，蓝蓝的眼睛像是能把人吸进去一样。"


translate chinese day1_ebdc09cc:


    slp "你好，刚到吗？"


translate chinese day1_a009dbf2:


    "我惊讶的说不出话来了。"


translate chinese day1_b5aec161:


    "女孩看上去只是一个普通人，我却一时不知道该表达什么才好。"


translate chinese day1_fff6f9eb:


    th "现在想跑已经晚了..."


translate chinese day1_880e4e10:


    slp "我说了什么不对的话吗？"


translate chinese day1_06176d1f:


    "费了一番力气后："


translate chinese day1_13317d7b:


    me "嗯... 啊..."


translate chinese day1_c44cb8e0:


    slp "嗯？"


translate chinese day1_d51bc1bf:


    me "啊，不，我是说...呃，{i}我是刚刚到这里{/i}."


translate chinese day1_c0a65fae:


    "回答的有些着急，让我开始嘀咕是不是一不小心说漏了什么。"


translate chinese day1_13317d7b_1:


    me "嗯... 啊..."


translate chinese day1_3c13f21e:


    slp "好的，欢迎~~"


translate chinese day1_6d77584d:


    "她朝我活泼的笑起来。"


translate chinese day1_82d0a78d:


    "奇怪，怎么看都只是一个普通的女孩。"


translate chinese day1_31e75c3d:


    th "呃...我不应该回到汽车这里的，还是田野和森林更好一些..."


translate chinese day1_c2c61eee:


    th "但是我接下来应该做什么呢—假装她是人类和她搭讪，或者是伺机逃跑，或者呢？"


translate chinese day1_85e4f294:


    "我听着自己的心跳剧烈的跳着，好像头上的血管要炸裂开了一样，只要我再一激动，就会溅到女孩一身的脑浆......"


translate chinese day1_26d0580a:


    slp "什么那么好笑啊？"


translate chinese day1_5b1e985b:


    "女孩打量着我。"


translate chinese day1_632c64ff:


    "我感到一股电流穿过自己的脊柱，让膝盖不停地颤抖。"


translate chinese day1_fa942807:


    me "没... 没什么..."


translate chinese day1_81fa1f0f:


    slp "那太好了~"


translate chinese day1_35ff2f5f:


    th "好？有什么好的？"


translate chinese day1_9d000438:


    "这个瞬间我有些自暴自弃了：去他的吧，我不管什么公交车了，不管什么冬天和夏天了，我只想赶紧脱掉这件让我浑身难受的毛衣，别的...还不如省省心。"


translate chinese day1_1ab1c37a:


    me "话说，也许你知道...?"


translate chinese day1_d0ebceb7:


    slp "你应该去找我们的辅导员，她会告诉你所有情况的。"


translate chinese day1_3399b38f:


    slp "看，{w} 一直往前走到广场，然后左转，你会看到几座小屋。"


translate chinese day1_f40c35be:


    "她指着那座大门说这些，好像我知道这后边有什么似的。"


translate chinese day1_54f0a60f:


    slp "那个，你找人问问奥尔加·德米特里耶夫娜在哪里就行。"


translate chinese day1_17993887:


    me "我… 呃…"


translate chinese day1_81c28b1c:


    slp "明白？"


translate chinese day1_a1e6972e:


    "当然没有..."


translate chinese day1_2f0e0a31:


    slp "我现在得走了。"


translate chinese day1_77cc4ba5:


    "女孩朝我挥挥手，然后消失在了大门的那一头。"


translate chinese day1_bc447943:


    "似乎，她没有把我当成是...{w}什么奇怪的人。"


translate chinese day1_07ef85cb:


    "而且这整个的灵异事件只是发生在了我身上，这里其他的一切都很正常..."


translate chinese day1_2d3f952e:


    th "辅导员，少先队制服…"


translate chinese day1_cf1c4a81:


    th "这...他们是在这里再现过去的历史吗？"


translate chinese day1_bce4a681:


    th "希望不要在这广场上看到一辆装甲车上站着列宁......"


translate chinese day1_1b36af68:


    th "不过，那种事现在也不会让我多么惊讶了..."


translate chinese day1_39014c31:


    "独自站了一会儿之后，我朝着营地走去。"


translate chinese day1_6f92edab:


    "向前走了大概50米，左侧一座单层的平房出现在眼前，{w}房门一侧挂着牌子，写道：CLUBS"


translate chinese day1_b20957b2:


    "我正准备上前去..."


translate chinese day1_93faf713:


    "突然房门打开了，一位矮个子的女生走了出来。"


translate chinese day1_bc2ffd42:


    "她可爱的脸庞让我感觉她背负着人类的命运，忍受着全世界的痛苦。"


translate chinese day1_5b8bfb48:


    "看到我的瞬间，女孩儿好像害怕一样不敢动弹。"


translate chinese day1_82b60add:


    "我也一样，僵在那里不知道该干什么好 – 是先上去问好还是等着她主动接近，{w}活着还是干脆跑了算了..."


translate chinese day1_d6de2681:


    "虽然我的自我保护的本能不断提示我，选择这最后一个选项。（我还是愿意相信这是自我保护）"


translate chinese day1_2cebe5d7:


    "这不是最差的本能，也已经是失去理智了。"


translate chinese day1_63473f65:


    "这种本能要是和理智推理赌博一把的话，估计结局是已经注定了。"


translate chinese day1_a971bc7a:


    "而那些理智的推理（至少表面上是这样）又在不断地提醒我，这个女孩儿可是人畜无害呦。"


translate chinese day1_9b858553:


    "突然间，有人从附近的灌木丛里跳了出来。"


translate chinese day1_b978b533:


    "一个穿着鲜艳的红色T恤的女孩子，上面还印有'苏联'的字样。"


translate chinese day1_7ebe0cfb:


    th "真是那个时代的再现啊。"


translate chinese day1_42c9a02c:


    "离远一点观察她感觉好矮，应该是比另外那两个少先队员都要小吧。"


translate chinese day1_c52afe92:


    "最终我还是决定接近她们，不过'苏联娘'(我在脑海里这样称呼她)跃向那个女孩说了些什么，还夸张的挥着手。"


translate chinese day1_b63ce3ba:


    "接着那个女孩露出了不解的表情，并且目光向地面看过去。"


translate chinese day1_d44e3c53:


    "我本想继续观察这有趣的对话，‘苏联娘’突然从裤兜里掏出了不知什么东西在另一个孩子面前晃来晃去。"


translate chinese day1_258e8119:


    "这{i}不知什么东西{/i}原来是一只蚂蚱。"


translate chinese day1_ed6083c2:


    unp "呀啊啊啊啊啊啊啊啊！！！！！！！！"


translate chinese day1_87779d4a:


    "女孩尖叫起来。"


translate chinese day1_f91880c6:


    "肯定不是很喜欢昆虫什么的，因为她突然逃走了，跑向了那个应该是列宁在那里宣传革命的地方。"


translate chinese day1_03448d77:


    "就是说，跑向了广场..."


translate chinese day1_ebc4605c:


    "‘苏联娘’朝我调皮的一笑，然后也追着跑了过去。"


translate chinese day1_38e92acb:


    th "哼嗯...真好。"


translate chinese day1_1c924fd9:


    th "我到了一个自己绝对不知道的地方，而且这里还有很多小孩扮演少先队员......"


translate chinese day1_3f084620:


    th "而且，据我观察，这地方离家不知要有几千里路，{w}这完全可能是另一种现实，来世？"


translate chinese day1_b8844662:


    "而这确实是{i}现实{/i}..."


translate chinese day1_bd1b7173:


    "周围的一切看上去都像是真的，(虽然有一种过度装饰的感觉)以至于我开始质疑我的上辈子是不是就是一场梦。"


translate chinese day1_947b07b9:


    th "那么，我现在应该做什么呢？"


translate chinese day1_a20cefa7_5:


    "..."


translate chinese day1_a48ce857:


    "我踢着脚下的碎石，眼睛随意的向那个‘CLUBS’那边看去。"


translate chinese day1_52f502cd:


    th "要等我做好下一步的决定估计还需要一些时间。"


translate chinese day1_6f416834:


    "这时我又想起了刚才在草地上打滚的情景..."


translate chinese day1_487a7790:


    "好丢脸..."


translate chinese day1_34312b93:


    "也许这是另外一种本能：当自己用来哭闹和自怨自艾的能量都被用尽的时候，身体就会自动的进入休眠或者是调用储存起来的能量。"


translate chinese day1_8ff0ae14:


    "很显然我选择了第二种选项，因为突然冒出来的决心让我想弄明白明白是怎么回事。"


translate chinese day1_6aea12ff:


    "那么为了做好这一点，我需要表现的有个人样 — 像个人类：保留着代表{i}我自己的世界{/i}的尊严。"


translate chinese day1_267d5adc:


    "我沿着小路往左转，右手边是一排小屋，显然是少先队员们的住处。"


translate chinese day1_772090f6:


    "不过说实话，看上去这些小屋还挺温馨的。"


translate chinese day1_5ff36d96:


    "虽然我出生在苏联时期，但是从来没有加入过那些少年组织，没有进过少先队，甚至也没有做过小十月主义者。"


translate chinese day1_d869f630:


    "我想象的少先队夏令营和这种情景有点区别：高大的营房，长长的大通铺，每天六点钟拉响警报集合，一分钟做好准备，然后广场上列队集合。"


translate chinese day1_3a1335b5:


    th "诶，不对~{w}会不会是我跟别的东西弄混了...？"


translate chinese day1_9eee971e:


    "突然有什么从背后撞了我！"


translate chinese day1_bb359f8d:


    "我踉跄几步，但还算保持住了平衡，准备转过身来为生存决一死战！"


translate chinese day1_57666fa8:


    "但是只是另外一个女孩子而已。"


translate chinese day1_ee51804d:


    "我惊讶地张着嘴。"


translate chinese day1_68b22888:


    dvp "把你的下巴收起来。"


translate chinese day1_143f62c0:


    "我闭上了嘴。"


translate chinese day1_c1b4c8b4:


    "同样款式的少先队服，不过她穿着的方式，怎么说呢，好叛逆啊。"


translate chinese day1_f689de87:


    "跟我遇到的其他几个女孩儿一样，她其实也挺可爱，不过她过于傲慢的态度却会让人丧失和她谈下去的信心。"


translate chinese day1_5ecab483:


    dvp "你，新来的？"


translate chinese day1_51149e68:


    me "…"


translate chinese day1_dc8deb21:


    dvp "好啊，再见！"


translate chinese day1_df07b66c:


    "她威胁似的瞟了我一眼离开了。"


translate chinese day1_be998354:


    "我等着她消失在拐角处...{w} 谁知道她还可能整出点什么事来！"


translate chinese day1_310f2718:


    "最有趣的事情就是即使是这个很不友好的女孩儿，在我看来也肯普通。她给人感觉并不是那么有杀气。"


translate chinese day1_dbdad37c:


    "顶多是会照着你的鼻子来一拳。"


translate chinese day1_78546616:


    "最后我终于来到了广场上。"


translate chinese day1_79587a2a:


    "这里倒是没有站在装甲车上的列宁，虽然经历了这些乱七八糟的事之后即使遇到了也不稀奇。"


translate chinese day1_b624fcdd:


    "取而代之的是一座某位同志的雕塑，底座上写着'GENDA'。"


translate chinese day1_4f2ef086:


    th "一定是党内的大人物。"


translate chinese day1_2f3a0b1e:


    "旁边有一些小的长椅。"


translate chinese day1_f324eeda:


    th "这里感觉还不错。"


translate chinese day1_9d30be31:


    th "对了那个女孩告诉我往哪里走来着？{w}往左还是往右啊。"


translate chinese day1_c6de1218:


    th "向左，向右，向左，向右…"


translate chinese day1_58ada7be:


    th "话说我究竟为什么要去那里呀...?"


translate chinese day1_bd699a39:


    th "啊，对了，我是决定要假装成是普通人来着..."


translate chinese day1_833eb5f3:


    th "那么，我就往右走吧！"


translate chinese day1_4870b408:


    "穿过一片小树林..."


translate chinese day1_be1bac02:


    "我来到了一座码头跟前。"


translate chinese day1_66caa1a6:


    th "刚才肯定是走错了。"


translate chinese day1_a02d94f5:


    slp "嘿，你走错了！"


translate chinese day1_0a8fd9cf:


    "我转向声音那边。"


translate chinese day1_ea065f04:


    "我遇到的第一个女孩儿就站在眼前。"


translate chinese day1_4725a971:


    slp "我跟你怎么说的呀？在广场那里应该左转的嘛，不是吗？"


translate chinese day1_59abc400:


    "她换上了泳装。"


translate chinese day1_e85f6b79:


    slp "啊，忘了自我介绍了，{w}我叫斯拉维娅。"


translate chinese day1_2fa68520:


    slp "实际上我的全名是斯拉维亚娜，不过大家都叫我斯拉维娅，{w}所以你也可以这么叫。"


translate chinese day1_6e37a835:


    me "嗯...好吧..."


translate chinese day1_0cc735df:


    "我还有些疑惑不解，一时没有想出更有意义的答案。"


translate chinese day1_4c5321be:


    sl "那么，你叫什么名字呢？"


translate chinese day1_c5787aa3:


    "好像她可以看透我的心..."


translate chinese day1_5cf26729:


    me "嗯...呃...我叫...谢苗。"


translate chinese day1_e2552123:


    sl "你好啊，谢苗。"


translate chinese day1_a89521fc:


    sl "好吧，我这里要结束了。{w}你可以等一下我吗，我去换一下衣服，然后咱们一起去找奥尔加·德米特里耶夫娜，同意？"


translate chinese day1_65bf82fc:


    me "同意..."


translate chinese day1_43560fad:


    "说完这些话她跑开了，我坐在码头上踢着水花。"


translate chinese day1_90942777:


    "我穿着厚重的冬天大皮靴，不过在这种天气下把脚弄湿也没什么吧。"


translate chinese day1_5c4ff44b:


    "而且，这还能让我稍微冷静下来。"


translate chinese day1_ea94e5d1:


    "我望着河水，大脑飞速地处理着刚才发生的各种事情。"


translate chinese day1_24d93bb8:


    th "如果这是什么阴谋，也太友好了吧..."


translate chinese day1_5edac208:


    th "不对，这看起来更像是一个随机事件。"


translate chinese day1_044eac6b:


    th "一个完全无法理解的随机事件。"


translate chinese day1_a733d584:


    sl "咱们走吧？"


translate chinese day1_3d8e6458:


    "斯拉维娅又穿着少先队服站在了我的身边。"


translate chinese day1_85ccdc6d:


    me "嗯，走吧…"


translate chinese day1_1ea0d875:


    th "我到这里没多长时间，可在这些人中，她算是看起来最不可疑的了。"


translate chinese day1_efe66139:


    th "可是，这个事实本身就很可疑！"


translate chinese day1_b3e9120f:


    "我们走到了广场。"


translate chinese day1_73d6913a:


    "苏联娘和刚才打了我的女孩子在那里追着跑。"


translate chinese day1_805a8066:


    th "那是某种游戏吗？"


translate chinese day1_6c5b8d8e:


    sl "乌里扬娜，跑够了没有？我要告诉奥尔加·德米特里耶夫娜了。"


translate chinese day1_e2f0dae9:


    us "是，长官！"


translate chinese day1_0bfd5bdf:


    "我决定先不要和斯拉维娅打听关于当地居民安全的问题。"


translate chinese day1_5280f6b1:


    th "最好还是先会见一下这位奥尔加·德米特里耶夫娜吧。"


translate chinese day1_1cd246d8:


    th "听上去她是这里管事的。"


translate chinese day1_3337c244:


    "我们走过一排看上去都差不多的小屋，有的看起来像是大号的酒桶，还有的像是储藏室。"


translate chinese day1_598416c3:


    "最后，斯拉维娅在一座稍小一点的屋前停了下来。"


translate chinese day1_4ce282c2:


    "看起来像是一件艺术作品：淡去的油漆，分布着零星破损的外墙，在阳光下闪闪发光，百叶窗微微打开，缓缓地随风摆动，两侧生长着繁茂的紫色树丛。"


translate chinese day1_9ba44673:


    "破败的屋子好像要被紫色的天鹅绒淹没一般， 不可思议的紫色，作为其中的主力，无情的包围着辅导员的小屋。"


translate chinese day1_3dc6042e:


    sl "你在这里愣着干什么？快走啊。"


translate chinese day1_cc0015de:


    "斯拉维娅打断了我的白日梦。"


translate chinese day1_25a8e409:


    mt_voice "... 还有不要再戏弄列娜了..."


translate chinese day1_23aa893b:


    "蕾娜？！（礼奈？！）"


translate chinese day1_368e2a32:


    th "好像有人在里面。"


translate chinese day1_42c5f640:


    "没错，过了一会儿门打开了，是乌里扬娜，她又带着淘气的笑容跑了过去。"


translate chinese day1_9b6e58bf:


    "双马尾女孩儿跟着走了出来。"


translate chinese day1_01fca3c6:


    sl "别在意，列娜！"


translate chinese day1_0d446d18:


    th "所以说她的名字叫做列娜...{w}还好不是蕾娜的说..."


translate chinese day1_92afc216:


    un "可是我不..."


translate chinese day1_a3f0580d:


    "她话没有说完就红着脸跑向了广场。"


translate chinese day1_a653101d:


    "不知为何我想跟着看看她，可是这时斯拉维娅对我说："


translate chinese day1_84aaac50:


    sl "跟我来。"


translate chinese day1_9e8e7cd3:


    "我们进入了小木屋。"


translate chinese day1_4a8e191a:


    "房间内部的陈设和我想象差不多：两张床、几把椅子、一张书桌、一个衣柜、还有地上铺着的简朴的地毯。"


translate chinese day1_c87f01c5:


    "没什么特别的，像家一样温暖舒适，虽然整洁程度上和我自己的房间有一拼。"


translate chinese day1_f9471bdb:


    "窗户那里站着一个大概25岁的女孩子。"


translate chinese day1_1529b84e:


    th "大自然赐予了她天使一样的身材和美貌..."


translate chinese day1_0aa01509:


    th "现在至少在这个魔界里有一件事能让我打起精神来：此地似乎盛产漂亮的小姑娘..."


translate chinese day1_80b7fab7:


    mtp "你终于来了！{w}太好了！{w} 我的名字叫做奥尔加·德米特里耶夫娜，我是夏令营的辅导员。"


translate chinese day1_81f67e23:


    me "幸会，我叫谢苗。"


translate chinese day1_2a6869f0:


    "我尽量装出一副没事的样子回答问题。"


translate chinese day1_b93477a9:


    "她往前凑了凑。"


translate chinese day1_9a812b52:


    mt "我们打早上就盼着你来呢。"


translate chinese day1_b7364cea:


    me "你们，在等着我？"


translate chinese day1_ca77693f:


    mt "是的，当然了！"


translate chinese day1_dda106bc:


    me "请问下一班公共汽车几点到呀，你看，我... "


translate chinese day1_fec151d0:


    mt "你需要坐下一班车吗？"


translate chinese day1_8f1ac2ed:


    th "哇，对喔，我为什么要坐..."


translate chinese day1_5a1280bd:


    th "看来我不能问的太直接：那样往往得不到想要的答案。"


translate chinese day1_79c3f7b2:


    th "我越来越这么怀疑。"


translate chinese day1_8d50c9cf:


    me "没啥，只是好奇..."


translate chinese day1_1725efc3:


    me "话说，我们的具体位置是哪里呀？{w}我们的寄信地址，我是说。"


translate chinese day1_21adf436:


    me "我想给父母写封信报个平安。"


translate chinese day1_efb897d9:


    "我总觉得如果死马当成活马医，跟他们瞎白话白话，也许就能问出点什么隐情也说不定。"


translate chinese day1_5a58e925:


    mt "这样啊，可是你的父母半个小时之前刚刚打来电话，托我向你问好呢。"


translate chinese day1_41180a2e:


    th "好吧，这可不在计划之内。..."


translate chinese day1_350d74d5:


    me "那么，我能给他们打电话吗？我出门之前有点事情忘了说。"


translate chinese day1_44f6bf31:


    mt "不行。"


translate chinese day1_9d48f5b9:


    "她很自然地莞尔一笑。"


translate chinese day1_9bddf507:


    me "为什么呀？"


translate chinese day1_c40fd752:


    mt "我们这里没有电话。"


translate chinese day1_51149e68_1:


    me "......"


translate chinese day1_729f518a:


    me "那我爸妈是怎么打的电话呀？"


translate chinese day1_120ecdbc:


    mt "我刚刚从城镇中心回来，是在那里和他们谈的话呦。"


translate chinese day1_434cc4a4:


    th "好吧，原来如此。"


translate chinese day1_68be55cd:


    me "那能不能问一下我要怎样去到城里呢？"


translate chinese day1_44f6bf31_1:


    mt "不行的。"


translate chinese day1_89d38d04:


    "他还是那样笑着。"


translate chinese day1_5ac38cf1:


    me "为什么呀？"


translate chinese day1_06cd3c2d:


    mt "因为下一班车一个星期以后才有。"


translate chinese day1_a3268632:


    "我决定不再询问辅导员同志今天早晨是如何往返的了，反正也得不到什么答案。"


translate chinese day1_bd84df53:


    "刚才的整个对话过程中，斯拉维娅都在旁边默默地等着，好像我们的对话没有一点奇怪的地方。"


translate chinese day1_869d68e4:


    mt "哦，对了，得给你找个制服。"


translate chinese day1_75c6d686:


    th "我可是绝对没有要穿上少先队员短裤的意思，还要戴上红领巾！？..."


translate chinese day1_94a1d364:


    th "不过大夏天穿着冬装也不是个好主意。"


translate chinese day1_f4df3367:


    me "嗯...谢谢..."


translate chinese day1_00ec5ab7:


    th "话说我在这么热的天气里穿着厚棉袄和靴子居然没有人觉得奇怪，这好像很诡异......"


translate chinese day1_13e0bfb3:


    mt "OK~我要去忙了，你四处转转熟悉熟悉周围的环境吧！{w}别忘了晚上回来吃饭呦~"


translate chinese day1_8af1d414:


    "说完她就走出了小屋。"


translate chinese day1_3be70f85:


    "不对，不是‘走’，应该说是跑出去的。"


translate chinese day1_733c1098:


    "结果屋里只有我和斯拉维娅两人。"


translate chinese day1_4c0d1dce:


    sl "我也得走了，有些活得干。"


translate chinese day1_0a470c4f:


    sl "四处转转吧！{w}晚上见咯。"


translate chinese day1_d47d9534:


    th "如果这不是暗藏着什么杀机，那么这现实也好，别的什么也好，还有斯拉维娅，这里真是越来越吸引我了。"


translate chinese day1_a20cefa7_6:


    "..."


translate chinese day1_a28a58fe:


    "我今天头一次发现这鬼地方真是热的要命。"


translate chinese day1_3aa2905a:


    "虽然，这明显要怪我的冬装。"


translate chinese day1_6224b6ad:


    "我脱掉大衣扔到床上，然后还有我的毛衣，现在只穿一件衬衫。"


translate chinese day1_1302b288:


    th "哇，这下痛快多了！"


translate chinese day1_0650308f:


    "现在我能做的也就是听她们的四处转一转。"


translate chinese day1_faf01b13:


    th "然后尽量观察观察。"


translate chinese day1_abe359aa:


    "穿过当地的住宅区，迎面走过来一个少先队员。"


translate chinese day1_19166f7e:


    "而且居然是男的，原来这个梦幻的国度里也是有男性的呀。"


translate chinese day1_6e392819:


    elp "你好，你是新来的，叫做谢苗对吧。"


translate chinese day1_f4234049:


    me "你是怎么...？"


translate chinese day1_eb3ac837:


    elp "大家都知道啦，对了我叫作电子小子，现实版的哦，这么叫我就行。"


translate chinese day1_1ab8b0e8:


    th "电子小子...现实版，这个世界已经疯狂的让我无语了。"


translate chinese day1_f2e007f0:


    me "好吧..."


translate chinese day1_13475f83:


    el "乌里扬卡管我叫起司酱。"


translate chinese day1_f005cc88:


    me "起司酱？"


translate chinese day1_dd47efcd:


    th "面包上的？"


translate chinese day1_8966f24f:


    el "因为我的姓是起斯科夫。（注：原文Сыроежкой、подосиновиком、Сыроежкин）"


translate chinese day1_f2e007f0_1:


    me "哈..."


translate chinese day1_ab73c55e:


    el "我带你转转吧！"


translate chinese day1_c45870d5:


    "因为自己在陌生的地方瞎转太费时间了，所以我接受了他的邀请。"


translate chinese day1_901746cb:


    me "好吧，咱们走。"


translate chinese day1_e555f27e:


    "结果我们又来到了广场上。"


translate chinese day1_6bd8e03f:


    th "好像这个夏令营没有别的地方似的..."


translate chinese day1_ac68c5e6:


    "列娜正坐在长椅上看书，{w}电子小子很神气的走上前去。"


translate chinese day1_03a7db73:


    el "你好啊，列娜，来见见我们的新人，谢苗同学。"


translate chinese day1_ebd7111d:


    "他兴致勃勃地引出话题。"


translate chinese day1_f37aca17:


    me "你好。{w}嗯...从某种程度上我们已经见过面了......"


translate chinese day1_31b01f67:


    un "是的…"


translate chinese day1_51f9c6e8:


    "她的目光离开书本，看向了我，然后脸突然红了，急忙低下头接着看书，好像不知道我们还站在这里似的。"


translate chinese day1_0e6973ff:


    el "好吧，我们接着走。"


translate chinese day1_e71595cb:


    "我起初觉得这样的初次见面说的话略少，不过后来一想这样其实比较好。{w}电子小子活力四射的电波和列娜不大对的上。"


translate chinese day1_304c9144:


    me "走吧。"


translate chinese day1_d2c497e2:


    "我们接下来走到一个建筑前面，这个我一眼就能看出来是食堂。"


translate chinese day1_ac073a3b:


    el "那么这个是..."


translate chinese day1_e84fdadf:


    me "这我知道，你们在这里食用有机食物。"


translate chinese day1_06114188:


    el "大概是那个样子…"


translate chinese day1_4eab803f:


    "食堂的门廊下，刚刚背后偷袭我的少女站在那里。"


translate chinese day1_2cd75b17:


    "这人让我的愉悦心情瞬间......没了。"


translate chinese day1_2eef73b2:


    th "说真的现在也不是跟这货耍宝的时候，虽然他比较滑稽。"


translate chinese day1_88b653a6:


    th "我还是先弄清楚什么都是什么，或者至少弄清我在哪里。"


translate chinese day1_84bfeb9f:


    el "那个人，那边那个，叫做阿丽夏·朵切芙斯卡娅，遇到她可得小心点。"


translate chinese day1_36dc9fcc:


    "他跟我说起悄悄话。"


translate chinese day1_c065e234:


    el "不要叫她二酱，她不喜欢那个名字..."


translate chinese day1_242c3a55:


    dv "你刚才说我什么？"


translate chinese day1_c941236c:


    "她肯定是听见了。"


translate chinese day1_7cbd6d0d:


    "眨眼之间，阿丽夏已经冲到了我们跟前。"


translate chinese day1_7810c210:


    el "哇呀，这里交给你啦..."


translate chinese day1_8a6dfaab:


    "电子小子溜了。"


translate chinese day1_68ae01db:


    "我觉得独自面对这个暴力女可是不妙，于是也跟着溜了。"


translate chinese day1_87908dda:


    "跑到广场时，看不到他了。"


translate chinese day1_f5ecd77a:


    "不过朵切芙斯卡娅也没有追我呀。"


translate chinese day1_efa7a730:


    th "不过我还是别要尝试叫那个名字吧。{w}即使想也不要想..."


translate chinese day1_d436642f:


    "等我平缓了气息，我很奇怪刚才为什么那样。"


translate chinese day1_50af9809:


    th "好吧，她是个暴力...女...{w}可是我为什么要跑呢？"


translate chinese day1_cd458724:


    "找不到答案，我坐下来看着Genda的雕塑。"


translate chinese day1_2d7b339e:


    "阿丽夏从身旁掠过，低声吼道："


translate chinese day1_1823fa4f:


    dv "“等会儿找你算账”。"


translate chinese day1_35feebce:


    me "找我算账？我做错什么了啊？！"


translate chinese day1_7eff5e99:


    "我的话被加上了一个愧疚的笑容。"


translate chinese day1_8fa3ff1e:


    th "可我有什么可愧疚的？..."


translate chinese day1_9dbfd6f2:


    "她没有回答，接着追电子小子。"


translate chinese day1_a20cefa7_7:


    "..."


translate chinese day1_eb85f5b2:


    th "看起来晚饭之前我得自己消磨消磨时间了。"


translate chinese day1_c1af6492:


    "我决定往东走。{w}至少是在现实世界里那边是东。"


translate chinese day1_990e43f3:


    "不一会儿，我来到了一个足球场。"


translate chinese day1_55a5886e:


    "一场比赛踢得正热闹。"


translate chinese day1_6e3f39b7:


    th "估计在这里看一会儿也没什么坏处。"


translate chinese day1_80a62586:


    "我从小一直到十几岁足球都踢得不错，甚至还想过要去踢职业的，不过几个比较严重的伤让我不想再冒险走这条不可靠的路。"


translate chinese day1_ac5ae99c:


    "踢球的孩子们年龄不等，有个十岁左右的男孩儿，还有个十四岁左右的女孩儿，还有..."


translate chinese day1_f462954c:


    th "一个女孩儿...{w}哇，那是乌里扬娜。"


translate chinese day1_5e68b29c:


    th "好吧，她也踢踢球，也没什么可吃惊的。"


translate chinese day1_1ae034aa:


    th "不过她好像总是有使不完的劲儿。"


translate chinese day1_0286167f:


    "我站的离场地挺远，不过她还是发现我了。"


translate chinese day1_f85a8374:


    us "嗨，你！"


translate chinese day1_77bd833d:


    "乌里扬卡朝我喊着。"


translate chinese day1_afc02b59:


    us "想不想来玩？"


translate chinese day1_3f27c2c0:


    "我不知道该怎么回答。"


translate chinese day1_7fb785ba:


    th "一方面来说，在球场上跑个十来分钟也没什么。"


translate chinese day1_b2114442:


    th "不过另一方面，我走的每一步都有可能成为最后一步。"


translate chinese day1_be98ba5d:


    "不过怎么说，我的着装还是非常不适合上场的。"


translate chinese day1_0b1fdc5d:


    "穿着冬天的靴子和长裤在夏天踢球，估计用不了一会儿我就洗澡了。"


translate chinese day1_16639bbb:


    th "而不穿鞋不穿裤子比赛又不符合伦理..."


translate chinese day1_91f30c65:


    me "下回吧，好不好呀。"


translate chinese day1_1cdbc40f:


    "我大声喊着回话，然后转身离开了。"


translate chinese day1_317df2d8:


    "乌里扬卡的喊叫声马上传来了：说我的裤子还是椅子，还是说我是伪娘什么的..."


translate chinese day1_a20cefa7_8:


    "..."


translate chinese day1_4b9550bc:


    "夜幕逐渐降临，让我在经历了一整天毫无意义的折腾之后十分疲乏。"


translate chinese day1_a20cefa7_9:


    "..."


translate chinese day1_2d76a94d:


    "我回到广场，坐到长椅上无力的叹着气。"


translate chinese day1_c7f14958:


    th "我还是就坐在这里等着吃晚饭吧。{w}怎么说也是不饿肚子的时候容易干活嘛。"


translate chinese day1_6face009:


    th "他们这里给饭吃吧？不会有错吧？"


translate chinese day1_6efc8181:


    th "话说这事很奇怪，最最基本的生理需求也能让人停止思考，或是奋斗什么的。"


translate chinese day1_8098381c:


    th "比如说我现在很饿，就不怎么去想我现在在哪里的问题了。"


translate chinese day1_8e0062ff:


    th "伟人们也会有这种习惯吗？"


translate chinese day1_11ca5f98:


    th "如果是这样，那斯巴达克斯是怎么领导奴隶起义的..."


translate chinese day1_6d0083af:


    th "我只能得出我不是什么伟人的结论，这跟我在一个大机器里作的是哪一个小零件就没什么关系了，不管是这个社会，还是什么科幻的母体，还是这个奇怪的夏令营。"


translate chinese day1_31836b32:


    "灯杆上扩音器的铃声打断了我的思考。"


translate chinese day1_bbaa6f9b:


    th "这一定是晚饭的铃声！"


translate chinese day1_d1afa489:


    "我马上前往食堂，还好我知道它在哪里。"


translate chinese day1_60f3a365:


    "奥尔加·德米特里耶夫娜站在门廊下。"


translate chinese day1_3181ce0b:


    "我靠近了盯着她看，表现出一副期待的样子。"


translate chinese day1_620995d5:


    "她也看了我一会儿，然后过来问我："


translate chinese day1_e4e46775:


    mt "谢苗，你等什么呀，赶快进来吧。"


translate chinese day1_3db38509:


    th "我想跟着她进去应该没事吧。"


translate chinese day1_8646eca1:


    "我的肚子这次很支持我。"


translate chinese day1_20d3b7df:


    "我们俩走了进去。"


translate chinese day1_d5cccf64:


    "这个食堂看起来像是一个...{w}食堂..."


translate chinese day1_5a204f67:


    "我曾经去参观过一个工厂的食堂...{w}这个看起来一模一样，不过要更干净更现代化一些。"


translate chinese day1_7f269d4e:


    "金属的椅子和饭桌，墙上和地板上打光的瓷砖，简朴又偶尔有些破损的餐具。"


translate chinese day1_2b4a4a89:


    th "大概一个少先队夏令营的食堂应该就是这样的吧。"


translate chinese day1_865ed000:


    mt "谢苗，等一下，我们去给你找个椅子…"


translate chinese day1_fed0ccad:


    "她环顾四周。"


translate chinese day1_cbc89c54:


    mt "朵切芙斯卡娅，你站住！"


translate chinese day1_bccd2e3b:


    "奥尔加·德米特里耶夫娜在阿丽夏经过的时候叫住了她。"


translate chinese day1_e0ebb201:


    dv "什么？"


translate chinese day1_686395a7:


    mt "你这衣服怎么回事啊？"


translate chinese day1_f893781b:


    dv "这有什么问题吗？"


translate chinese day1_6c59a28a:


    "的确她的衣服比较...叛逆"


translate chinese day1_7f4c6c16:


    mt "换一件漂亮干净的衣服，现在，马上！"


translate chinese day1_aa5269ba:


    dv "是...是..."


translate chinese day1_02da0b4a:


    "阿丽夏整理了一下衬衫走过去，路过时还瞪了我一眼。"


translate chinese day1_a1e6af5d:


    mt "那么，让你坐哪里好呢..."


translate chinese day1_30ea3e74:


    "空位置不多啊。"


translate chinese day1_986412cf:


    mt "坐在那里吧，乌里扬娜的旁边！"


translate chinese day1_2cfe56a5:


    me "嗯... 我..."


translate chinese day1_385a7396:


    mt "没问题，饭已经端上来了。"


translate chinese day1_07918b6c:


    "我只有接受了。"


translate chinese day1_ff014804:


    "当然端上来的肉饼里有可能被下了箭毒什么的也说不定...然后是土豆泥上被大方的涂上砒霜调料，水果汤被换成抗冻液..."


translate chinese day1_1dcade22:


    "可它们看起来那么好吃，让我都没有计划抵抗。"


translate chinese day1_d5d98923:


    us "嘿！"


translate chinese day1_087d8e36:


    me "干毛？"


translate chinese day1_4b969ae9:


    "我对坐在旁边的乌里扬卡回答的不怎么礼貌。"


translate chinese day1_bcac0437:


    us "你怎么不和我们踢球啊？"


translate chinese day1_ee0ece72:


    me "因为我的衣服啊。"


translate chinese day1_ac6bee46:


    "我说着，边指向问题的源头。"


translate chinese day1_762f9dc6:


    us "喔...好吧，吃饭吧。"


translate chinese day1_71c21715:


    "然而，已经没什么可以吃的了：我的肉饼从盘子里消失了！"


translate chinese day1_2623f571:


    th "这只有可能是她..."


translate chinese day1_f9c55ec5:


    th "不对，准确的说，除了乌里扬娜谁也做不出这种事来！"


translate chinese day1_58e7978c:


    me "把我的肉饼还给我！"


translate chinese day1_0f31d85b:


    us "在一个大家庭里，愣神就意味着挨饿呦~{w}一不小心一个肉饼就没啦~"


translate chinese day1_ea7db8e5:


    me "我告诉你还给我啦！"


translate chinese day1_3e16ffc9:


    "我气势汹汹的准备抬起手来..."


translate chinese day1_39e0f78f:


    us "你看，我这里根本没有啊！"


translate chinese day1_8fb93759:


    "确实，乌里扬卡的盘子是空的；这小兔崽子吃的和顺的一样快。"


translate chinese day1_bf4d7f64:


    us "别着急，我们会有办法的~"


translate chinese day1_c2895463:


    "她拿起我的盘子跑开了。"


translate chinese day1_4c2a1209:


    "现在追她应该是没有意义的：如果他们想要下毒的话，没有必要这么麻烦。"


translate chinese day1_4bb06742:


    "过了一分钟，乌里扬卡端着热气腾腾的肉饼回来了。"


translate chinese day1_39cd2cfa:


    us "送给饿着肚子的人~"


translate chinese day1_73aab3cd:


    me "谢谢…"


translate chinese day1_dfe4386e:


    "我可不会再说什么了。"


translate chinese day1_e425e9ee:


    "我已经饿得顾不上怀疑食物的成分了。{w}我叉起肉饼就是..."


translate chinese day1_2158fc2f:


    th "这啥？！{w}有虫子！{w}还不是普通的虫子！{w}一只毛毛虫！{w}有那么多条腿而且还在蠕动！"


translate chinese day1_27e62654:


    "盘子掉在地上摔成了碎片，我的腿还被椅子砸伤了。"


translate chinese day1_c1ea30fa:


    "我从小就不喜欢昆虫，刚才那饭碗里蠕动的虫子...简直就是噩梦！！"


translate chinese day1_670b85c3:


    me "你个小兔崽子..."


translate chinese day1_cdba60b2:


    "乌里扬卡好像早有预谋，已经跑到了门口，好像刚刚听了一段搞笑段子一样笑话我。"


translate chinese day1_a0076c62:


    "我跟着她冲了过去。"


translate chinese day1_4a7f4c33:


    "我们冲出了食堂。"


translate chinese day1_51f77987:


    "我们相隔不过几米，我感觉自己应该能够抓住这个小女孩。"


translate chinese day1_c0e0bceb:


    "我们穿过了广场..."


translate chinese day1_8d3b0cc2:


    "经过了CLUBS小屋..."


translate chinese day1_f7567198:


    "跑进了森林小径。"


translate chinese day1_d50859c6:


    "我开始大口喘气休息。"


translate chinese day1_80f73680:


    th "我应该戒烟了…"


translate chinese day1_1178684b:


    "乌里扬卡在转角处消失了。"


translate chinese day1_b24a1f41:


    th "不可能，让她溜了！"


translate chinese day1_c255b412:


    th "怎么可能！"


translate chinese day1_7a8d7835:


    "我停下来重新调整呼吸。"


translate chinese day1_a20cefa7_10:


    "..."


translate chinese day1_fd3eb8d2:


    "天色越来越暗了。"


translate chinese day1_b570941e:


    th "我好像迷路了…"


translate chinese day1_bce9bc24:


    th "在树林里过夜可不是一个好主意，我还是赶快回到营地去吧。"


translate chinese day1_bbe1aefc:


    "可是，我完全不知道该怎么走！"


translate chinese day1_985a9376:


    th "嗯，这种时候要随机选择..."


translate chinese day1_a20cefa7_11:


    "..."


translate chinese day1_a20cefa7_12:


    "..."


translate chinese day1_f8583ab1:


    "我在森林里兜了半天圈子，甚至还想过大喊救命，不过终于还是看到了树林那边营地的围墙。"


translate chinese day1_7ad8ae02:


    th "一切都恢复正常了。"


translate chinese day1_7f4df294:


    me "公交车走了..."


translate chinese day1_6cfccad8:


    "我默默地自言自语。"


translate chinese day1_dbfa4d22:


    "话说过来：公交嘛，不可能总是停在同一个地方。"


translate chinese day1_42ed0653:


    "说又说回去：汽车又不会自己走，总要有个人开车吧！"


translate chinese day1_dcc5269b:


    th "应该...是这样的吧？"


translate chinese day1_ba1dfa38:


    "这个世界的所有东西都是表面上很普通的，但是都可以有两种解释：一个普通、真实的日常解释，或者是一个充满隐情的解释。"


translate chinese day1_b678b61b:


    "很有可能司机师傅就是下车去弄点吃的，然后我就在这段时间下了车..."


translate chinese day1_ec1d1437:


    th "怎么说我也不应该在这个地方呆着呀。"


translate chinese day1_9aa1fa90:


    "公交车是不是会无人驾驶算是个问题，不过更重要的是我到底是怎么到这里的。"


translate chinese day1_2b9ab305:


    "还有{i}这里{/i}到底是哪里..."


translate chinese day1_5aa31516:


    "田野和树林向远方延伸，我无法从它们那里获得答案。"


translate chinese day1_a20cefa7_13:


    "..."


translate chinese day1_13255b43:


    "奇怪的、不可理解的异想世界。{w}可是从各种意义上来讲这个世界一点都不吓人。"


translate chinese day1_c83e85c5:


    "要么是我的求生本能罢工了，要么是这里无忧无虑的环境把我催眠了，让我不在乎几个小时之前发生的事情。"


translate chinese day1_e9c8c9dc:


    "虽然我估计也没什么体力接着担心什么了。"


translate chinese day1_ab64ceb2:


    "我只是想要平静，休息休息，等有了精神才有可能继续寻找想要的答案。"


translate chinese day1_c260705c:


    "不过那可是需要花点时间了..."


translate chinese day1_5cfa0252:


    th "那么我现在该干什么呢？不知道现在是不是能歇着的时候..."


translate chinese day1_a20cefa7_14:


    "..."


translate chinese day1_b2e9fb9d:


    "天色完全黑下来了，无论如何还是在营地里过夜比较安全。"


translate chinese day1_46aa5cbc:


    "我正准备回去，突然有人悄悄从背后接近我。"


translate chinese day1_ac5eb104:


    sl "你好呀，这么晚了在这里做什么？"


translate chinese day1_51149e68_2:


    me "..."


translate chinese day1_245b70c4:


    "斯拉维娅站在我面前，我被吓了一跳。"


translate chinese day1_ec2df0cc:


    sl "所以说你没抓住乌里扬娜呢~"


translate chinese day1_d1046a49:


    "她笑了。"


translate chinese day1_5e1fc6b8:


    "我无力的点点头，叹了叹气。"


translate chinese day1_dd08f845:


    sl "可以理解啦。{w}从来没有人追上过她。"


translate chinese day1_476d94f1:


    th "对，她是个火箭女孩。{w}她的能量应该用在一些更有意义的事情上..."


translate chinese day1_d1722b3c:


    sl "你一定饿了吧，一直没有吃上饭..."


translate chinese day1_d0dae46d:


    "对啊，我完全忘了肚子饿这件事了；不过她一提起来，我肚子又开始咕咕叫起来，把我给出卖了。"


translate chinese day1_c17a722d:


    "斯拉维娅笑了。"


translate chinese day1_4cbb01a1:


    sl "咱们走吧。"


translate chinese day1_e01dee92:


    me "什么？食堂还开着吗？"


translate chinese day1_40c17de2:


    sl "没关系，我有钥匙。"


translate chinese day1_36955eb7:


    me "钥匙？"


translate chinese day1_ab83b32e:


    sl "是啊，我有这个夏令营所有设施的钥匙。"


translate chinese day1_9080c90e:


    me "你从哪里搞到的呀？"


translate chinese day1_ca97714d:


    sl "嗯...我应该算是辅导员的助手吧。"


translate chinese day1_7e4b1b8c:


    me "这样啊。{w}那么，咱们走吧。"


translate chinese day1_63a3eb4c:


    "一个我无法拒绝的提案。"


translate chinese day1_00f9254f:


    "我们走到广场的时候，斯拉维娅突然停下了脚步。"


translate chinese day1_7f5d338c:


    sl "抱歉，我得先和我的室友说一声，她平常特别准时，我怕她会担心。"


translate chinese day1_518df0ed:


    sl "你去食堂吧，我马上就到，好吗？"


translate chinese day1_505d91ae:


    me "好吧..."


translate chinese day1_32ac72cc:


    "我没想到这么晚了食堂还有人。"


translate chinese day1_b6cef34a:


    "那人在绝望的撬着门锁..."


translate chinese day1_18f36ecf:


    "我没多想什么就走了上去。"


translate chinese day1_d706aa5c:


    "入侵者原来是阿丽夏。"


translate chinese day1_e81739e5:


    th "我想我应该保持距离... "


translate chinese day1_bbe1ef0f:


    "她目不转睛的看了我一会儿，然后说道："


translate chinese day1_a872d369:


    dv "别光看着，来搭把手啊。"


translate chinese day1_e54b39e9:


    me "啥意思？"


translate chinese day1_9736714c:


    dv "帮我开门啊！"


translate chinese day1_7332342b:


    me "为啥？"


translate chinese day1_c67591a7:


    dv "因为我想弄点面包奶酪什么的，晚上没吃饱！"


translate chinese day1_8d3f7a19:


    me "嗯...{w}那样好不好啊？"


translate chinese day1_1381f7ca:


    dv "你自己不饿？{w}乌里扬卡害得你晚上没怎么吃好吧？"


translate chinese day1_f396e777:


    "她笑着嘲讽我。"


translate chinese day1_ad73a68c:


    th "呜...是这样......"


translate chinese day1_c43ed392:


    me "没事啦，斯拉维娅正准备过来然后..."


translate chinese day1_c8b0074b:


    dv "纳尼？！"


translate chinese day1_ca8046cd:


    th "我好像不应该说这件事。"


translate chinese day1_e7b745f9:


    dv "我先撤了！"


translate chinese day1_5f47ed77:


    dv "咱们走着瞧！{w}你今天已经惹了我两次了！"


translate chinese day1_a180b25b:


    "说完阿丽夏就消失在了黑夜中。"


translate chinese day1_efb5548d:


    th "第一次是什么时候啊？"


translate chinese day1_a20cefa7_15:


    "..."


translate chinese day1_696c481c:


    "斯拉维娅很快就来了。"


translate chinese day1_8c608cb1:


    sl "没发生什么事吧？"


translate chinese day1_d5cf1edd:


    me "没事啊，问这干什么？"


translate chinese day1_18938c76:


    sl "没事，随便问问。"


translate chinese day1_dc129c74:


    "不跟她说阿丽夏的事情为好..."


translate chinese day1_b5544ae3:


    me "没事，挺好的。"


translate chinese day1_944a6c8e:


    "我瞬间回答了出来，有一种谎言的感觉......"


translate chinese day1_6d7067b9:


    sl "那么，咱们走吧？"


translate chinese day1_3792d81a:


    "至于斯拉维娅，她好像什么都没有注意到。{w}至少装成什么都不知道..."


translate chinese day1_44f2c4ea:


    "我们进入了食堂。"


translate chinese day1_c8f556b6:


    sl "等一下，我去找点东西。"


translate chinese day1_d09b3412:


    "我坐在椅子上摇着尾巴等我的救世主..."


translate chinese day1_8775f75b:


    "我的晚饭很简单：几个小面包，还有一杯酸奶。"


translate chinese day1_c6c983c8:


    th "一点也不奇怪：饥饿的少先队员们早就把好吃的东西都抢光了吧。"


translate chinese day1_37e90611:


    "不过，这也比我平常的食谱好得多了。"


translate chinese day1_d5866868:


    "斯拉维娅坐在桌子对面看着我吃饭。"


translate chinese day1_fbb73cfa:


    me "我脸上有东西吗？"


translate chinese day1_178aa33f:


    sl "没有，只是…"


translate chinese day1_d1046a49_1:


    "她笑了。"


translate chinese day1_dc9c1b81:


    sl "那么，你在夏令营的第一天过得怎么样？"


translate chinese day1_6a05d51d:


    me "嗯...我不知道该怎么说..."


translate chinese day1_b12cf159:


    "对于一个刚刚来到了另一个世界的人来说，不管是问他食堂的饭好不好吃还是辅导员漂不漂亮，自己的房间是不是舒适，都不太好回答。"


translate chinese day1_46db5ea6:


    sl "没关系，你很快就会习惯的。"


translate chinese day1_68e00554:


    "斯拉维娅深沉的望着窗外..."


translate chinese day1_b05e0e4e:


    th "说实话我也没打算熟悉这里，不过她并不知道..."


translate chinese day1_b9ffb640:


    th "或者至少是她想让我这么想。"


translate chinese day1_2c50ec15:


    me "不过总的来说，这里感觉还不错。"


translate chinese day1_443c7288:


    "我得想法打破这冷场的状态。"


translate chinese day1_17d13602:


    sl "是吗？"


translate chinese day1_a271f10e:


    "她没什么兴趣的回问道。"


translate chinese day1_1b986acc:


    me "是啊，这个地方这么..."


translate chinese day1_d4bc890b:


    "我本来想说‘怀旧’，不过还是忍住没说。"


translate chinese day1_edbbb7b2:


    "说起来对我来说是这样的。{w}可对他们来说可能只是一种普通的生活方式。"


translate chinese day1_78a8ca85:


    "{i}生活{/i}这个词用在这里很合适..."


translate chinese day1_789035fa:


    sl "这么什么？"


translate chinese day1_00577787:


    "她认真的看着我，{w}好像我说的话会影响一个很重要的决定一样。"


translate chinese day1_7c2d154d:


    me "嗯...怎么说呢，温馨啊，这里很温馨呢~"


translate chinese day1_c1307b0b:


    sl "应该是吧。"


translate chinese day1_78a2fe3f:


    "她又笑了起来。"


translate chinese day1_e349590f:


    sl "很高兴你这么想。"


translate chinese day1_5ac38cf1_1:


    me "为什么？"


translate chinese day1_17e99afb:


    sl "那个，不是所有的人都喜欢这里呢..."


translate chinese day1_1a377d44:


    me "那你呢？"


translate chinese day1_1995267f:


    sl "我？"


translate chinese day1_fc30fd2b:


    me "对。"


translate chinese day1_031c751c:


    sl "我喜欢这个地方，这里很棒。"


translate chinese day1_5c30029f:


    me "那么，你不必在乎别人怎么想。"


translate chinese day1_879638a9:


    sl "嗯...我没有那么担心啦~"


translate chinese day1_c954d69f:


    "斯拉维娅笑了起来。"


translate chinese day1_72c6e2b1:


    "我们的对话好像已经偏离了预定的轨道..."


translate chinese day1_1a712a21:


    sl "而你担心你自己..."


translate chinese day1_1d09f697:


    me "真的？为什么这么说？"


translate chinese day1_8b1f703e:


    sl "嗯...当别人专心吃饭的时候..."


translate chinese day1_07129b8a:


    me "对不起。"


translate chinese day1_705bea28:


    sl "没关系的。"


translate chinese day1_02cdc4c3:


    "我感觉坚持不住对这个女孩保持警觉。"


translate chinese day1_ded79e77:


    "可是为什么是她，不是这里其他的人？"


translate chinese day1_e53b428c:


    "每个人给我的感觉都是那么普通。{w}严格意义上的{i}普通{/i}，过于普通以至于一股寒气沿着我的脊梁流进骨髓！"


translate chinese day1_51e2809e:


    "普通，不像是一个左手拿着电钻，右手低音炮的邻居。"


translate chinese day1_ab3da39e:


    "不像是经常在地铁上、公交上经常看到的人。"


translate chinese day1_15d26ee0:


    "不像是坐在旁边的办公桌前的同事。"


translate chinese day1_29ee93d2:


    "也不像是一个比别人更加坚持己见的朋友。"


translate chinese day1_82012fff:


    "她们都很{b}普通{/b}—就像我期待的那样—有自己的缺点，没有超能力。"


translate chinese day1_c821ace7:


    "而且斯拉维娅还有点...{w}可爱？"


translate chinese day1_7f68049d:


    "我偷偷看了她一眼，不知道该说什么好。"


translate chinese day1_1ec8deab:


    sl "对不起，我本来想带着你到处认识一下可是自己却提前先走了。"


translate chinese day1_567b2330:


    me "我...自己认识的也还不错吧，大概都转到了。"


translate chinese day1_2e601a49:


    sl "你确定什么都没有错过吗？"


translate chinese day1_efd47648:


    "她笑得很开朗，让我有点不好意思，稍稍移开了视线。"


translate chinese day1_cdbd4a1c:


    me "这个嘛，我也不清楚啦，我第一天到这儿啊。"


translate chinese day1_122de170:


    sl "好吧，你都去了哪里了？"


translate chinese day1_875830b6:


    me "我看到了广场、这个食堂、还有足球场..."


translate chinese day1_76985cc0:


    sl "那海滩怎么样？"


translate chinese day1_b11fae54:


    me "只是远远的看了一下。"


translate chinese day1_f5ac888c:


    sl "你应该去那里嘛！或者咱们一起去！"


translate chinese day1_d5688542:


    me "嗯，好吧，一起..."


translate chinese day1_446ce20c:


    "她的纯真有点吓着我了，不过我再一想，万一这里发生的一切都是本来的样子，所有对我来说奇怪的东西对他们只是很平常的怎么办？"


translate chinese day1_6578880f:


    "也许我只是时空旅行到了过去？"


translate chinese day1_5410f15c:


    "啊，那样很多事情就解释清楚了。"


translate chinese day1_aad73e6b:


    me "我能问一个很二的问题吗？"


translate chinese day1_fadcaef0:


    sl "不行。"


translate chinese day1_7b5c12bd:


    "斯拉维娅笑着站了起来。"


translate chinese day1_c5c451d5:


    sl "有点晚了...你能自己找到奥尔加·德米特里耶夫娜的住处吗？"


translate chinese day1_9fef43a7:


    me "当然啦，不过为什么要去那里？"


translate chinese day1_821bd937:


    sl "她要安排你跟谁在一起。"


translate chinese day1_7332342b_1:


    me "为了啥？"


translate chinese day1_bfddcddb:


    "估计这个问题很傻，因为斯拉维娅咯咯地笑了起来。"


translate chinese day1_c070ca19:


    sl "你得有住的地方，不是吗？"


translate chinese day1_4405a6cf:


    me "是有道理..."


translate chinese day1_2455141e:


    sl "好吧，那我回去了，{w}晚安！"


translate chinese day1_0a0db17f:


    me "安..."


translate chinese day1_ca407fc5:


    th "她走的这么匆忙，有些奇怪..."


translate chinese day1_c6b742a3:


    "门锁上的一串钥匙引起了我的注意。"


translate chinese day1_08ccb5b7:


    "我想去找斯拉维娅，可是她住在哪里呀？"


translate chinese day1_3aa2ad8d:


    "这么晚挨个屋子去敲门不是个好主意。"


translate chinese day1_9356c1f1:


    th "我还是把它拿走吧，谁知道晚上会发生什么事。"


translate chinese day1_56811613:


    "这个想法让我不禁有点哆嗦，我才是那个需要警惕的人啊。"


translate chinese day1_5802aa2b:


    th "另一方面来说，我拿着这钥匙有什么用啊..."


translate chinese day1_0eb5a1f8:


    "夜晚，虽然昏暗，但是一点也不寂静：你可以听到叫个不停的蛐蛐儿，夜里歌唱的鸟儿还有树木枝叶的窸窣。"


translate chinese day1_ee63d9fd:


    "听斯拉维娅的去找辅导员吧，这个主意还不错。"


translate chinese day1_b358634e:


    "不知道为什么，不过不知名的社会主义缔造者的铜像让我有了干劲儿。"


translate chinese day1_e66f685d:


    "我坐在海滩上开始回忆今天发生的事情。{w}我的干劲也只能干出这些事了。"


translate chinese day1_2719b81b:


    "这里比食堂那边明亮许多，还有磨磨蹭蹭的少先队员匆匆跑过，所以这个地方显得一点也不吓人。"


translate chinese day1_2dc0eb74:


    th "公共汽车，夏令营，女孩子…"


translate chinese day1_3aff8100:


    "我实在太累，想不出这么多事的答案。"


translate chinese day1_4caea1ce:


    "我注意到一阵几乎听不见的沙沙声。"


translate chinese day1_f2d26be1:


    "我有点发抖的往那边看过去。"


translate chinese day1_6993b49f:


    th "一个女孩。{w}在看书。"


translate chinese day1_2807e1fc:


    th "是列娜。"


translate chinese day1_1bdc01a9:


    "我决定过去搭个话。"


translate chinese day1_7ccf917f:


    "她是我到这里遇到的人里唯一一个没怎么说过话的。"


translate chinese day1_7b241855:


    me "嗨，你在干嘛？"


translate chinese day1_ac772bf2:


    "列娜被吓得跳了起来。"


translate chinese day1_ea263cee:


    me "抱歉，我不是有意的！"


translate chinese day1_62592cb0:


    un "没事..."


translate chinese day1_ad42b83a:


    "她脸红了，又开始低头看自己的书。"


translate chinese day1_c43d3dfb:


    me "所以说你在看什么书？"


translate chinese day1_f3297c42:


    "封面上写着‘乱世佳人’。"


translate chinese day1_53009119:


    me "是好书啊…"


translate chinese day1_626232a6:


    un "谢谢。"


translate chinese day1_87f26837:


    th "说实话我没看过那本书，不过感觉这种文学的书和她挺般配。"


translate chinese day1_21b3e47f:


    "列娜好像没有兴趣继续对话下去。"


translate chinese day1_f85b2b40:


    me "那个，如果我打扰到你的话..."


translate chinese day1_049b021f:


    un "没有。"


translate chinese day1_62091b7c:


    "她一边看着书一边回答道。"


translate chinese day1_13ee5d36:


    me "我能坐在你的旁边呆一会儿吗？"


translate chinese day1_41cfffd5:


    un "为什么？"


translate chinese day1_e92b6e6e:


    th "是啊，为什么？"


translate chinese day1_a0c74c5a:


    "可能只是因为我太累了，而且有个伴总比独自呆着有意思。"


translate chinese day1_bf12dc37:


    "也许是我想了解一下她。"


translate chinese day1_0864712b:


    "我仔细的盯着列娜。"


translate chinese day1_07aeaaac:


    th "不对，不像是这样的..."


translate chinese day1_3102ef10:


    me "好吧，我不知道...不行吗？"


translate chinese day1_3b683375:


    un "可以..."


translate chinese day1_d783747c:


    me "不过如果我打扰到你的话..."


translate chinese day1_ab09f879:


    un "不，没事。"


translate chinese day1_858edfda:


    me "我可以离开的，只要你说就好..."


translate chinese day1_0d42a600:


    un "没问题的。"


translate chinese day1_f517812c:


    me "那好吧..."


translate chinese day1_98f71c0d:


    "我小心的坐在长椅的边上。"


translate chinese day1_0fcded02:


    "结束了这段让人紧张的对话，我已经不想再绷紧神经坐在这里了，可是现在站起来就走也不太合适。"


translate chinese day1_61ec364d:


    me "刚才不在状态...啊哈哈哈......"


translate chinese day1_cf8bd28b:


    "列娜什么也没有回答。"


translate chinese day1_8caec5df:


    "我把自己弄的像一个傻瓜一样。"


translate chinese day1_60dd740c:


    "我打赌乌里扬卡要是在这里的话肯定话笑话我。"


translate chinese day1_e1927099:


    me "你喜欢这里吗？"


translate chinese day1_610b2212:


    "我想起了斯拉维娅的问题，觉得这应该很适合引起话题。"


translate chinese day1_f1be5fed:


    un "嗯。"


translate chinese day1_783cf09d:


    "她淡淡的笑了。"


translate chinese day1_23729c98:


    me "I我大概也喜欢吧..."


translate chinese day1_b5a17bee:


    "列娜显然不像斯拉维娅一样擅长在没什么意义的话题上长时间的寒暄。"


translate chinese day1_141ec821:


    "不过她有些地方还是挺吸引人的。"


translate chinese day1_f8e0c3e7:


    "就像是仲秋雨夜，玻璃上一瞬间的倒影，让你想要转过身去在视野尽头的阴影里寻找。"


translate chinese day1_04705c56:


    "虽然你分辨不清、也未必认识，可是还是被深深地吸引..."


translate chinese day1_9ee89722:


    "列娜还在读她的书，没有在意我的存在。"


translate chinese day1_62582949:


    "我不打算再去问她关于夏令营或是这个世界的问题。"


translate chinese day1_a1e73db2:


    me "令人陶醉的夜晚..."


translate chinese day1_5ef3d7d4:


    un "嗯..."


translate chinese day1_76d86f28:


    th "究竟是为什么要和她聊天呢？"


translate chinese day1_69ef7bda:


    un "时间差不多了，我该走了…"


translate chinese day1_45544479:


    me "是挺晚了…"


translate chinese day1_2629037f:


    un "晚安。"


translate chinese day1_fab13f1e:


    me "安…"


translate chinese day1_e5d71cd0:


    "这个女孩不知道哪里有点奇怪。"


translate chinese day1_482011b5:


    th "第一眼看上去绝对是一个害羞的守纪律的少先队员女孩…"


translate chinese day1_efa2bb54:


    "列娜的秘密也在我的夏令营未解之谜的清单上占了一个位置。"


translate chinese day1_a20cefa7_16:


    "..."


translate chinese day1_047c6176:


    th "悠闲的晚上，没事可做…"


translate chinese day1_8b243a91:


    "我走向奥尔加·德米特里耶夫娜的小屋。"


translate chinese day1_76db5439:


    "房间里的灯亮着。"


translate chinese day1_01c690bf:


    mt "你好，谢苗！{w}你太慢了！"


translate chinese day1_ccd4c421:


    me "是啊…{w}我去营地四处散了散步。"


translate chinese day1_6e6b09c8:


    mt "好吧。{w}你睡在这儿。"


translate chinese day1_d0ba333c:


    "她指向其中一张床。"


translate chinese day1_54ddf89e:


    me "就在这儿...？"


translate chinese day1_07acf34e:


    "我有点吃惊。"


translate chinese day1_0cc846e0:


    mt "是啊，有什么问题吗？{w}我们没有空余的宿舍了。"


translate chinese day1_e5c2c901:


    "辅导员笑了，我想应该主要是出于礼貌吧。"


translate chinese day1_e6f03e6e:


    mt "你知道你应该做一个像样的少先队员吧。"


translate chinese day1_6d7c696f:


    "她强调了‘像样的’这几个字。"


translate chinese day1_fa9f2c55:


    me "对... 当然..."


translate chinese day1_c2a35634:


    "我有一刻陷入了沉思。"


translate chinese day1_8edc3e00:


    me "你不会在意吗，妹子？"


translate chinese day1_d642ee60:


    "她奇怪地看着我，{w}眼睛里似乎有些杀气？"


translate chinese day1_a63ad949:


    mt "一个少先队员应该尊重年长的人！"


translate chinese day1_8492f5e5:


    "奥尔加·德米特里耶夫娜严肃的说。"


translate chinese day1_4adb9df1:


    me "是啊，没人说不是啊…"


translate chinese day1_343b3a4f:


    "我继续念念不休，还是没明白到底发生了什么事。"


translate chinese day1_1c5c9771:


    mt "你不还是...?"


translate chinese day1_8fb93608:


    "她盯着我。"


translate chinese day1_4b041b53:


    "在这种目光之下即使是最厉害的矮人工匠大师从最深的地牢里铸出的秘银剑也会融化。"


translate chinese day1_8e8da635:


    me "我啥？{w}什么事，宝贝儿？"


translate chinese day1_50297304:


    "疑惑不解加上有点心烦让我提高了声音。"


translate chinese day1_20f9f9be:


    mt "你要用合适的词语称呼成年人！"


translate chinese day1_0bdad8e7:


    th "对，这个地方是有很多奇怪的事情…{w}不过这个女孩比我大不了几岁..."


translate chinese day1_d2336ee2:


    th "可能还比我小..."


translate chinese day1_c447246d:


    "不过我决定还是不要争吵—虽然几分钟之前我还不会叫她是大人，我不得不承认她的作风还是挺硬派的。"


translate chinese day1_6a31f527:


    "我现在的处境不适合吵架。"


translate chinese day1_28205106:


    me "听从您的吩咐，老师。"


translate chinese day1_9fde8ff9:


    "我念叨着。"


translate chinese day1_c0fad5bd:


    mt "这就好多了嘛！{w}这才是一个好的少先队员应该具有的素质。"


translate chinese day1_fc0afa91:


    mt "现在到了睡觉时间。"


translate chinese day1_cee3bfd9:


    "说实话，我既不是什么正经的少先队员，也不是什么没素质的少先队员。"


translate chinese day1_06cf444d:


    "我昨天还不是少先队员呢..."


translate chinese day1_e62f6767:


    th "不过现在哪里有选择。"


translate chinese day1_a3857c58:


    "‘我们会开出一个你无法拒绝的条件。’我估计奥尔加·德米特里耶夫娜会用上这个格言的。"


translate chinese day1_fe66fa24:


    "躺在床上闭上眼才发现自己今天好累..."


translate chinese day1_80860151:


    "我的脑袋像是有个锤子在砸一样，叮叮当当的。"


translate chinese day1_af68a167:


    "而且更像是机械车间，而不是高精尖的半导体工厂。"


translate chinese day1_95ae7d8f:


    "那辆公交车一闪而过..."


translate chinese day1_de155e12:


    "还有立着雕像的广场..."


translate chinese day1_4d51c966:


    "充满着少先队员的食堂...{w}还有乌里扬卡幸灾乐祸的表情。"


translate chinese day1_169054b7:


    "斯拉维娅..."


translate chinese day1_06f01a7c:


    "列娜..."


translate chinese day1_753d87f9:


    "甚至回忆到阿丽夏也没有多少不好的感觉。"


translate chinese day1_1de79ea9:


    th "没准我来这里是一件好事？"
# Decompiled by unrpyc: https://github.com/CensoredUsername/unrpyc
