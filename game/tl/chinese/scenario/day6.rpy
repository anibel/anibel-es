
translate chinese day6_main_8ae28bb6:


    "我不知道奥尔加·德米特里耶夫娜什么时候叫我起来，不过在那之前我就体会到死亡的感觉。"


translate chinese day6_main_8d76b64a:


    "我的整个身体都很疼，头晕晕的，大脑也转不动。"


translate chinese day6_main_d8a5165b:


    mt "谢苗！快起来！再不起来你就要错过早饭了！而且！更重要的—集合！"


translate chinese day6_main_1950340c:


    "看起来辅导员想要给我安排新的一天的任务。"


translate chinese day6_main_10ee39fe:


    "我随便拿起什么东西盖住头，转过身去面对墙。"


translate chinese day6_main_fd3267f5:


    th "奥尔加·德米特里耶夫娜只不过会动动嘴皮功夫。"


translate chinese day6_main_511dc216:


    "至少那个时候我是想这样相信的。"


translate chinese day6_main_a5a51a86:


    mt "赶紧给我起来，不然..."


translate chinese day6_main_7898d95e:


    "不然什么？我正想自豪的问出来，不过还是克制住了。"


translate chinese day6_main_181f6035:


    "不是因为害怕，只是因为我太累了。"


translate chinese day6_main_45358519:


    th "说真的，她能干什么?"


translate chinese day6_main_53306e73:


    th "在集合的时候批评我？把我的照片贴在墙上？"


translate chinese day6_main_2308a0d4:


    th "或者拿我做外星实验？"


translate chinese day6_main_ffaac6cf:


    th "啊，那个我也准备好了，只要让我睡上一会儿就好。"


translate chinese day6_main_4547c60b:


    mt "好吧...不过如果你错过集合...!"


translate chinese day6_main_68fee20c:


    "门被摔上了，困意又一次袭来。"


translate chinese day6_main_a20cefa7:


    "..."


translate chinese day6_main_4dec4f4d:


    "等我睡醒的时候太阳很晃眼。"


translate chinese day6_main_8f17d018:


    "根据我快要坚持不住的手机显示，现在已经下午一点钟了。"


translate chinese day6_main_9409b38e:


    "奇怪的是我感到神清气爽，又是一个好天气。"


translate chinese day6_main_6142f969:


    "我伸了个懒腰，走向水房。"


translate chinese day6_main_5b04907d:


    th "我睡过了早饭，不过午饭就要开始了，所以不必担心像昨天一样挨饿。"


translate chinese day6_main_7052ffcc:


    "半路上我看到了一个长相奇怪又熟悉的人。"


translate chinese day6_main_1593a66d:


    "不过我走得太快没有来得及看，等我回头的时候他已经走到路口了。"


translate chinese day6_main_609cc4c0:


    "水池附近没有其他人。"


translate chinese day6_main_8bf50f95:


    "大家大概都因为辅导员的任务而忙的不可开交。"


translate chinese day6_main_81c75e91:


    "我迅速的洗漱，准备离开的时候听到对面的水声。"


translate chinese day6_main_ca1b7d89:


    "对面站着一位少先队员。"


translate chinese day6_main_8ce7bba1:


    "我看不清他的脸，不过看起来像是刚刚路过的那个人。"


translate chinese day6_main_003b19dd:


    pi "你在干什么，太阳这么好!"


translate chinese day6_main_75daf906:


    "我抬头看看天，用手遮住阳光。"


translate chinese day6_main_725738da:


    me "是啊，没什么特殊的..."


translate chinese day6_main_6ef01be6:


    "他背后的阳光很刺眼，让我很难看清他的脸。"


translate chinese day6_main_cdb07297:


    pi "辅导员今天很生气，很生气!"


translate chinese day6_main_cb4b580d:


    "他的声音听起来也很诡异的熟悉。"


translate chinese day6_main_2fa452e8:


    me "是啊，她总是那个样子..."


translate chinese day6_main_c0d7b8c0:


    pi "嗯，你最好知道..."


translate chinese day6_main_319ce924:


    "我试图回想以前有没有听过这个声音。"


translate chinese day6_main_efa43c61:


    pi "那再见了..."


translate chinese day6_main_7c10e79b:


    "他快速关掉了水龙头，前往树林。"


translate chinese day6_main_fe539829:


    "我本来想要追上去看看，不过很快打消了这个主意，我可不想浪费这么好的一天搞没有意义又累人的调查。"


translate chinese day6_main_de1f880f:


    "大自然是多么美好，树枝在摇动，微风吹拂着小草，鸟儿在休息，森林和田野在阳光下舒展。"


translate chinese day6_main_010548de:


    th "我不知道现在是几月份，不过看起来像是盛夏。"


translate chinese day6_main_fc0175bf:


    "我回想起童年夏天的欢乐。"


translate chinese day6_main_c43f265a:


    th "儿时的游戏，有那么多..."


translate chinese day6_main_a11227aa:


    th "我可不会拒绝玩打仗游戏或者捉迷藏什么的。"


translate chinese day6_main_bb2be5d8:


    th "或者用沙子建造一座城堡，在里面放满塑料士兵。"


translate chinese day6_main_a20cefa7_1:


    "..."


translate chinese day6_main_d2032a24:


    "我坐在广场上等待着午餐，看起来很快了。"


translate chinese day6_main_aac9dcb2:


    "少先队员们不断在我身边经过。"


translate chinese day6_main_2be25cd9:


    "不过总是我不认识的人，阿丽夏，乌里扬娜，列娜，斯拉维娅都没有见到。"


translate chinese day6_main_e960bd40:


    "我的大脑里出现了各种无意义的想法，不过我还是不想浪费这样一个美妙的下午。"


translate chinese day6_main_fa556551:


    th "谁想拒绝这样有爱的太阳呢?"


translate chinese day6_main_fe4d7ca8:


    th "我肯定不会!"


translate chinese day6_main_86f706e5:


    "我看着GENDA."


translate chinese day6_main_ad19e3a6:


    "他在像平常一样冥想。"


translate chinese day6_main_32b69a4b:


    th "他现在不会想不明白了。"


translate chinese day6_main_4eb2c759:


    "我想起来前几天的迷惑和恐惧。"


translate chinese day6_main_e55e0216:


    th "那些都离我那么远。"


translate chinese day6_main_d7baf9b3:


    th "我到底能不能离开这里...?"


translate chinese day6_main_0330e53e:


    "这已经不再那么重要了。"


translate chinese day6_main_83657a58:


    th "也许我已经死了。{w}这就是最后一站，请下车..."


translate chinese day6_main_ecb0d206:


    pi "你在想什么?"


translate chinese day6_main_a9c9e2a5:


    "我抬起头，又看到了之前的那个少先队员。"


translate chinese day6_main_323f65fe:


    "我还是看不清他的脸，因为太阳的原因。"


translate chinese day6_main_91a6c9e2:


    me "你知道啊，绳命..."


translate chinese day6_main_6af5686f:


    "我本以为他会坐在这里，不过他只是站在原地，稍稍侧过脸，让我彻底失去看清他真面目的机会。"


translate chinese day6_main_a0e278fd:


    me "我说，咱们之前见过吗..."


translate chinese day6_main_80d5b78e:


    pi "这么说吧，你知道我是谁。"


translate chinese day6_main_fb74ce55:


    me "但是我不知道。"


translate chinese day6_main_b5b3bfa4:


    "我诚实的笑着。"


translate chinese day6_main_e0782e42:


    pi "在这里，你不知道。"


translate chinese day6_main_b8948d3e:


    "他回答得很短。"


translate chinese day6_main_f2e007f0:


    me "我知道了..."


translate chinese day6_main_79123fc6:


    "不是我不想说，是我完全不知道该说什么好。"


translate chinese day6_main_50b86b79:


    "我的灵魂如此平静，对此一点都不着急。"


translate chinese day6_main_f6e6d257:


    pi "你第一次来?"


translate chinese day6_main_232e0bb1:


    "他问了一个问题，不过语气像是等待着肯定的回答。"


translate chinese day6_main_1703d110:


    me "是啊... 你呢?"


translate chinese day6_main_43d309aa:


    pi "我?"


translate chinese day6_main_00b5ddd1:


    "他停顿了几秒钟。"


translate chinese day6_main_8d5ee990:


    pi "不，不是第一次...{w}可以说我从童年开始每年都来。"


translate chinese day6_main_df6d3916:


    "这个答案让我很有兴趣。"


translate chinese day6_main_e7458b35:


    me "那...{w}这里以前是什么样子的...?"


translate chinese day6_main_d64a6b28:


    pi "总是这个样子...{w}总是一样的，奥尔加·德米特里耶夫娜作为辅导员，同样的少先队员们，同样的队列集合，还有同样的诡异事件。"


translate chinese day6_main_bd473b02:


    "我一度觉得这是自己在说话。"


translate chinese day6_main_fe8d0158:


    me "有意思..."


translate chinese day6_main_4f1daffa:


    pi "好像每一..."


translate chinese day6_main_a6820f71:


    "他犹豫了一下。"


translate chinese day6_main_1bda58b4:


    pi "年...{w}都会有越来越多的有意思的事情发生，然后你也会越来越了解这个地方。"


translate chinese day6_main_cb23542f:


    me "你在说什么?"


translate chinese day6_main_225bea57:


    "这段对话让我很好奇。"


translate chinese day6_main_b46780c4:


    th "遗憾的是我一直看不清这个少先队员的脸。"


translate chinese day6_main_aa6afd6a:


    pi "嗯，每一个夏令营的活动都让我回想起上一次。"


translate chinese day6_main_831f663f:


    "他冷静的说道。"


translate chinese day6_main_5f789b50:


    me "大概是吧...{w}我这是第一次。"


translate chinese day6_main_2015f3db:


    pi "看起来是这样。"


translate chinese day6_main_531b4fd4:


    "少先队员笑了。"


translate chinese day6_main_cdab879e:


    pi "但是看起来也不会是最后一次。"


translate chinese day6_main_93624028:


    me "那个，这里很有意思，但是...{w}不过你也知道有人说，没有一个地方像是..."


translate chinese day6_main_aefb5c47:


    pi "家，但是你还是要回去!"


translate chinese day6_main_d12dd680:


    "现在我完全确定这个家伙在对我隐瞒着什么。"


translate chinese day6_main_3a65de66:


    "准确的说他和这里其他的人们太不一样了。"


translate chinese day6_main_b22130bc:


    me "你什么意思，你是说我会永远困在这里了吗?"


translate chinese day6_main_8a6dbb06:


    "我清晰的说着每一个字。"


translate chinese day6_main_70a4fcf8:


    "少先队员没有时间回答了，午饭铃声响了。"


translate chinese day6_main_65eca27b:


    "我转过头去看着扩音器，等我回过头来的时候，他已经消失了。"


translate chinese day6_main_7136b652:


    "我的大脑中出现了各种可能性，不过很快又都消失了，开始回想这个夏令营的正常之处。"


translate chinese day6_main_d654b2a7:


    "再怎么说这五天里也没有发生什么超自然的事情。"


translate chinese day6_main_fbd5c1ad:


    "而且这里的一切看起来都是那么的正常，甚至是有一点无聊。"


translate chinese day6_main_d6045179:


    th "也许我只是误解了他说的话?"


translate chinese day6_main_070cce50:


    "我一边想着，一边前往食堂，期待着一顿大餐。"


translate chinese day6_main_e0e16d60:


    "有时候这里的午餐就像是饥荒时候的汤屋。"


translate chinese day6_main_74f00562:


    "少先队员们推来推去，连爬带滚，试图抢到最前面还有最好的座位。"


translate chinese day6_main_d065bf1d:


    "我冷静的站在后边等我的食物供给。"


translate chinese day6_main_10ad7f24:


    "今天的午饭是окрошка(不怎么喜欢)还有土豆加上肉排。"


translate chinese day6_main_57891974:


    "我坐在角落里想象着自己平静的享受午餐的情景。"


translate chinese day6_main_74749cbe:


    "我的桌子距离厨房最远，我虔诚的祈祷少先队员们不会发现。"


translate chinese day6_main_c3b3dadd:


    "或者最后发现。"


translate chinese day6_main_92a1900b:


    "然而，当我开始吃主食的时候，斯拉维娅，舒里克还有电子小子突然出现了。"


translate chinese day6_main_252cd706:


    sl "可以吗?"


translate chinese day6_main_f0cecb70:


    "我没有用来反对的理由。"


translate chinese day6_main_8f9071f9:


    me "当然..."


translate chinese day6_main_2b21bf5c:


    "出乎意料，午餐一直很安静，就连电子小子也没有像平常一样闹腾。"


translate chinese day6_main_3763e444:


    "我终于吃完了，满足的咂着舌头。"


translate chinese day6_main_8ef508e6:


    me "我说，你们有没有见过那个人…我今天早晨见到他，他那个…"


translate chinese day6_main_59b52b32:


    "我突然发现自己不知道怎么形容。"


translate chinese day6_main_01a7d78d:


    me "那个跟我差不多的打扮，差不多的着装..."


translate chinese day6_main_789d0b9f:


    sl "你这样的描述让人很难判断啊。"


translate chinese day6_main_45a469ba:


    "斯拉维娅笑了。"


translate chinese day6_main_b0d50ab6:


    el "我们大概有半个夏令营长的是那个样子的。"


translate chinese day6_main_06cde8a8:


    "好吧，他们说得对。"


translate chinese day6_main_eaba70e6:


    sh "你为什么也问这个呢?"


translate chinese day6_main_30d66e9b:


    me "我今天见到他，但是看起来我好像从来没有在这里看到他。"


translate chinese day6_main_58193b9c:


    el "在食堂里找找吧！我觉得他可不会错过午饭!"


translate chinese day6_main_11b8b706:


    th "我为什么没有想到?!"


translate chinese day6_main_44cf71eb:


    me "是啊，好吧，慢慢吃吧，朋友们!"


translate chinese day6_main_f11b8b6b:


    "我站起来，慢慢的沿着通道走着。"


translate chinese day6_main_be7f7c50:


    th "列娜和热妮娅坐在那边，我朝她们友好的笑笑。"


translate chinese day6_main_0a699e1d:


    th "阿丽夏和乌里扬娜正在笑着争吵什么。"


translate chinese day6_main_58fae8be:


    th "奥尔加·德米特里耶夫娜被围在少先队员的中间，幸好她还没有发现我。"


translate chinese day6_main_73c2f32d:


    th "已经没有空座了，不过还是找不到早晨的那个人。"


translate chinese day6_main_bf019437:


    th "这情况越来越有意思了..."


translate chinese day6_main_2814bfb0:


    th "看来我在这里找不到他了。"


translate chinese day6_main_da518989:


    th "也许他已经吃过午饭了?"


translate chinese day6_main_75e20332:


    "我朝着出口走去。"


translate chinese day6_main_c68ce9bc:


    "外面好热，感觉只要离开凉阴整个人就会化掉一样。"


translate chinese day6_main_9f9676f7:


    "我突然很想睡觉。"


translate chinese day6_main_be0bca85:


    "我打了个呵欠，准备前往奥尔加·德米特里耶夫娜的宿舍。"


translate chinese day6_main_c150c0d1:


    "她看起来暂时还不会回来，那么我就可以美美的睡上几个钟头。"


translate chinese day6_main_207c30bb:


    th "总之我可不想出什么别的情况，尤其在这个温度下。"


translate chinese day6_main_ee0c51ab:


    "宿舍中天堂般的凉爽成了我的解脱。"


translate chinese day6_main_95912f70:


    "我脱掉衣服跳上床。"


translate chinese day6_main_3da3d910:


    "屋外的蟋蟀懒洋洋的叫着，风吹动着窗帘，我沉浸在梦中..."


translate chinese day6_main_a20cefa7_2:


    "..."


translate chinese day6_main_cd340537:


    "敲门声把我惊醒，虽然不是很猛，但是一直持续着。"


translate chinese day6_main_cc72393f:


    "我不情愿的走过去开门。"


translate chinese day6_main_ae0d3213:


    "奇怪的是门口没有人。"


translate chinese day6_main_2a6b67de:


    "我揉揉眼睛走到外面。还是没有人。"


translate chinese day6_main_c567b577:


    th "也许是一个梦?"


translate chinese day6_main_ca2d8082:


    "已经四点了。"


translate chinese day6_main_ce2e12fc:


    "我感觉很累。"


translate chinese day6_main_b2353364:


    th "我也许不应该在下午睡觉，至少我知道要发生什么!"


translate chinese day6_main_111fd3b2:


    "我穿上靴子，锁上门离开了房间。"


translate chinese day6_main_45ecefad:


    th "不过去哪里呢，距离晚餐很近了。"


translate chinese day6_main_ff6f5b1a:


    "我突然想要清醒一下，于是前往了沙滩。"


translate chinese day6_main_9f56ddfa:


    "这边天黑的很早，太阳已经快要落下去了。"


translate chinese day6_main_100dfcab:


    "我斜着眼看着天空中的大火球。"


translate chinese day6_main_dc571f95:


    pi "怎么回事?"


translate chinese day6_main_c4ddbe2a:


    "我哆嗦了一下。"


translate chinese day6_main_1a9fc9dc:


    "这个奇怪的人又一次出现在了我的面前，而且又一次由于光线照射我看不清他的脸。"


translate chinese day6_main_9330aefd:


    "我被晃到了眼睛，所以一度只能看清周围物体的轮廓。"


translate chinese day6_main_f4ab4a03:


    me "你在跟踪我吗?"


translate chinese day6_main_ba4fcc35:


    pi "没有，我只是路过。"


translate chinese day6_main_99107c75:


    "他冷静的回答道。"


translate chinese day6_main_7cf46969:


    me "那你应该知道!"


translate chinese day6_main_b73b893a:


    pi "所以你谁也没有看见吗?"


translate chinese day6_main_b4c5d120:


    me "是啊。"


translate chinese day6_main_c9d6ddfd:


    "我擦了擦眼睛，但是没有用。"


translate chinese day6_main_efd14637:


    me "为什么在午饭的时候没有看见你?"


translate chinese day6_main_9c68f611:


    "我直接的问道。"


translate chinese day6_main_7b536ae4:


    pi "我不饿。"


translate chinese day6_main_b703fc38:


    "他笑了。"


translate chinese day6_main_61bd5e66:


    "我眨了几下眼，然后他又神奇的消失了。"


translate chinese day6_main_eec03d52:


    me "这尼玛到底怎么回事?!"


translate chinese day6_main_9b11e809:


    "现在我确定那个人有些奇怪。"


translate chinese day6_main_76509461:


    th "他肯定和这里的各种事情都有关系!"


translate chinese day6_main_47c82893:


    th "我得去弄清楚。"


translate chinese day6_main_96aea495:


    th "但是我得首先确定这不是我的错觉，这个少先队员不是一个普通的本地人。"


translate chinese day6_main_c9a31bdf:


    "沙滩上人很多，好像整个夏令营的人们都来到了这里。"


translate chinese day6_main_a3ae7c9c:


    th "是啊，外面太热了。"


translate chinese day6_main_d272a3e6:


    "奥尔加·德米特里耶夫娜稍稍拉开一点距离站在旁边，监视着少先队员们。"


translate chinese day6_main_7806ecc0:


    "正当我觉得来到这里可能不是一个好主意的时候，她发现了我。"


translate chinese day6_main_105fc865:


    mt "嘿，谢苗!"


translate chinese day6_main_245e3bea:


    mt "你集合的时候没有到场，而且今天一整天你都在闲逛!"


translate chinese day6_main_3f27c2c0:


    "我不知道该怎么回答。"


translate chinese day6_main_75ee262f:


    mt "你到底想要怎么成为一个优秀的少先队员...?"


translate chinese day6_main_28cd78c0:


    "她说的温柔了一些。"


translate chinese day6_main_b900c6f1:


    me "奥尔加·德米特里耶夫娜，在夏令营中，...{w}有没有一个和我很像的少先队员?"


translate chinese day6_main_5faf7021:


    "她看起来很惊讶。"


translate chinese day6_main_ee235fec:


    mt "也许吧，我不知道...{w}你为什么这么问?"


translate chinese day6_main_92ba045e:


    me "啊，我只是好奇..."


translate chinese day6_main_db5e08e9:


    mt "我们这里有很多男孩子。"


translate chinese day6_main_7227b86f:


    me "好吧，算了..."


translate chinese day6_main_2bbb0cb6:


    "有人在河那边叫了起来，奥尔加·德米特里耶夫娜往那个方向跑了过去。"


translate chinese day6_main_a1ecea1f:


    th "哇，看起来我躲过了一劫。"


translate chinese day6_main_89b6f9be:


    "我坐在沙滩上看着戏水的少先队员们。"


translate chinese day6_main_88733a8a:


    "过了几分钟，乌里扬卡走了过来。"


translate chinese day6_main_f62bc905:


    us "怎么心情不好?"


translate chinese day6_main_600e482e:


    me "没有。"


translate chinese day6_main_371d353a:


    us "那你为什么自己坐在这里?"


translate chinese day6_main_1bb0a12c:


    me "我在思考..."


translate chinese day6_main_6a81728a:


    us "思考什么?"


translate chinese day6_main_df1dac3f:


    "我决定给她讲讲自己的故事。"


translate chinese day6_main_6da88382:


    me "想象一下你遇到了一个奇怪的人，然后你需要弄清楚到底他什么奇怪。"


translate chinese day6_main_95250360:


    us "简单！我就直接去问问就好了!"


translate chinese day6_main_ce3666e5:


    "她笑了。"


translate chinese day6_main_f9b97b16:


    me "那是很简单，但是他不会回答的。"


translate chinese day6_main_01b8a656:


    us "你怎么知道？你问过了吗?"


translate chinese day6_main_4e9618f8:


    me "没有，不过那很明显..."


translate chinese day6_main_03ccbb4d:


    "乌里扬卡没有回答，而是做在我的旁边长叹一声。."


translate chinese day6_main_bb0c418e:


    us "我一直在游泳。"


translate chinese day6_main_98a645b3:


    me "如果他们知道什么，但是就是不告诉你呢?"


translate chinese day6_main_09228ed5:


    us "想办法让他们说啊!"


translate chinese day6_main_65e449ac:


    th "可是要怎样...?"


translate chinese day6_main_9fa67ce4:


    "我们又聊了一会儿，然后我站起来道别，便离开了。"


translate chinese day6_main_1da41780:


    th "现在应该去找那个神秘的少先队员然后询问一番了。"


translate chinese day6_main_b30b8dd4:


    "第一个地方是公交车站。"


translate chinese day6_main_560be1b3:


    th "再怎么说，这是一切开始的地方!"


translate chinese day6_main_38c39f41:


    "道路像平常一样空空的。{w}只有偶尔的尘土的漩涡。"


translate chinese day6_main_4a8cd2f2:


    "我正准备回去，突然听到有人说悄悄话。"


translate chinese day6_main_aa3f5830:


    pi "不要相信他!"


translate chinese day6_main_c3c01734:


    "有人躲在雕塑后面。"


translate chinese day6_main_2475793e:


    "一个少先队员背对着我。"


translate chinese day6_main_c32fda68:


    "他的声音听起来还是那样诡异的熟悉。"


translate chinese day6_main_0a2309cb:


    me "你是谁?!"


translate chinese day6_main_8c7ee3c8:


    "我靠近了几步。"


translate chinese day6_main_d6271c95:


    pi "站住！不要靠近!"


translate chinese day6_main_f0d11be8:


    "我下意识的站住了。"


translate chinese day6_main_68a3cebc:


    "我的内心告诉自己不应该和他吵架。"


translate chinese day6_main_c41b6a4c:


    me "好吧，我站住了..."


translate chinese day6_main_9f354a24:


    pi "你看到他了吗？和他说话了吗?"


translate chinese day6_main_ae9a3d04:


    "他紧张的问着。"


translate chinese day6_main_3edfb9a5:


    me "你在说什么?"


translate chinese day6_main_f3effee3:


    "他穿着一件少先队员的衣服。"


translate chinese day6_main_65856116:


    pi "你知道是谁..."


translate chinese day6_main_08a2904f:


    "我当然知道!{w}他在说我之前见到的少先队员！"


translate chinese day6_main_9513cd87:


    me "是。"


translate chinese day6_main_424c8856:


    "我沉默了一会儿，然后回答道。"


translate chinese day6_main_e3fcbbd3:


    pi "他和你说了什么?"


translate chinese day6_main_137679fe:


    "少先队员恳求的说道。"


translate chinese day6_main_e88413fc:


    me "其实没说什么..."


translate chinese day6_main_5d3e0c2a:


    pi "他给你什么建议了吗？让你去干什么了吗？威胁你?"


translate chinese day6_main_3a4fb558:


    me "没有，没有那样的事情，他是看起来很奇怪，不过没有其他的事情了..."


translate chinese day6_main_7896c94c:


    pi "要知道他可能不是一个人！或者说他是一个人但是你会看到很多和他差不多的人！"


translate chinese day6_main_0effa2c7:


    me "那么你呢？你是谁？你在躲着谁?"


translate chinese day6_main_18d9b086:


    pi "你会明白的...{w}一段时间以后...{w}只要记住，最重要的是找到出口!"


translate chinese day6_main_ea099937:


    "突然刮起了一阵风，卷起漫天树叶，一个袋子罩在了我的头上。"


translate chinese day6_main_3b1e051a:


    "当我回头看雕像的时候，那个少先队员已经消失了。"


translate chinese day6_main_cb002221:


    "就在这个瞬间我感到了恐惧，真正的，几乎可以触摸到的恐惧。"


translate chinese day6_main_5eb7b77f:


    "我想起来刚刚来到这里时的恐惧。"


translate chinese day6_main_2ad95628:


    "不过那个时候一切看起来还很友好，现在这个夏令营已经露出了獠牙，准备吃了我了。"


translate chinese day6_main_a7216041:


    "前方是未知的..."


translate chinese day6_main_6c246aae:


    "我起了一身的鸡皮疙瘩，我的喉咙一阵干渴，手臂不停地颤抖，我试图忘记这些回到营地。"


translate chinese day6_main_8983704d:


    "已经是晚饭时间了，但是我还不想去食堂。"


translate chinese day6_main_c10e6db8:


    "见到第二个少先队员之后我已经吓坏了。"


translate chinese day6_main_3f3d68fb:


    th "我都不知道他们是谁。"


translate chinese day6_main_d902b9fb:


    th "虽然现在什么都没有发生，但是我不能相信他们!"


translate chinese day6_main_94d63846:


    "我来到了社团活动室，不过我意识到这可能不是很合适，这里很有可能遇到什么人，这可不是我现在想干的事。"


translate chinese day6_main_fdbf6225:


    "我几乎是跑车冲向了森林。"


translate chinese day6_main_35b23e96:


    th "一边是少先队员，准确的说是两个少先队员。"


translate chinese day6_main_d208b9b7:


    th "另一边是夏令营里看起来一个比一个正常的本地人。"


translate chinese day6_main_8ae0dc11:


    "我的放任自流政策遇到了空前的压力。"


translate chinese day6_main_0b638a1b:


    "现在很明显，我得做点什么。"


translate chinese day6_main_5fd8c6ed:


    th "但是究竟是...?"


translate chinese day6_main_5225748d:


    "大家都在吃完饭，所以我有机会偷偷前往奥尔加·德米特里耶夫娜的房间。"


translate chinese day6_main_f9f20d48:


    "我准备穿上来的时候的衣服，然后带上手机。"


translate chinese day6_main_346b48a6:


    th "我现在想不出一个好的计划，所以目前还是应该躲在森林里。"


translate chinese day6_main_1840851c:


    "我加快脚步走进建筑。"


translate chinese day6_main_e7710e4b:


    "我的东西都在它们应该在的位置。"


translate chinese day6_main_b5791e64:


    "我迅速的拿起来，准备装进口袋里，但是突然注意到屏幕上显示着什么。"


translate chinese day6_main_4eda2e3c:


    "信息窗口被打开了。"


translate chinese day6_main_3d971385:


    "上面写着:"


translate chinese day6_main_a645ed9e:


    "“你错了，谢苗！你错了！”"


translate chinese day6_main_d3429bef:


    "我的灵魂好像离开了自己的身体。"


translate chinese day6_main_a9617350:


    "我愣在了原地。"


translate chinese day6_main_8962b7ae:


    "我在不停地发抖，我的血液不停地涌动，我的大脑要爆炸了。"


translate chinese day6_main_4e8980f6:


    "我至少过了一分钟才回过神来。"


translate chinese day6_main_b2d0ecc1:


    th "也许是奥尔加·德米特里耶夫娜写上去的，也许是一个少先队员。"


translate chinese day6_main_5b297fa5:


    th "虽然他们没有见过这种产品但是想要弄明白可能也不难。"


translate chinese day6_main_98fe4021:


    th "不过除了我没有人会知道我把手机藏在哪儿!"


translate chinese day6_main_2d7a0195:


    "这件事简直是压倒骆驼的最后一根稻草。"


translate chinese day6_main_d3412ceb:


    "我冲出门去，再也不想回来。"


translate chinese day6_main_a20cefa7_3:


    "…"


translate chinese day6_main_d6e969ac:


    "夜晚降临了夏令营。"


translate chinese day6_main_8e9948b3:


    "我在树林里坐了几个小时，因为每一点微小的声音而颤抖。"


translate chinese day6_main_a921011f:


    "我选择的这个位置距离小路很远，所以想要找到我应该不容易。"


translate chinese day6_main_b808e2cc:


    "我的大脑中正在思考着各种方案，冲出去，杀光每一个人..."


translate chinese day6_main_ecefb337:


    "不过反过来想我这样的反应可能也有点过度了..."


translate chinese day6_main_f4c17cd7:


    th "我还是觉得这里发生的事情一点也不正常，这种感觉越来越强，然而我一直没有证据。"


translate chinese day6_main_09d97100:


    "我本来可能就这样在这里呆到早晨，被各种想法淹没，不过我似乎听到了脚步声..."


translate chinese day6_main_34d72ebd:


    pi "所以说你决定逃跑了吗?"


translate chinese day6_main_c9a36b10:


    "我转过身， 不过在黑暗中看不清少先队员的脸。"


translate chinese day6_main_06562ffd:


    "不过我很确定那就是他。"


translate chinese day6_main_cb29da41:


    "我那个时候还不害怕。"


translate chinese day6_main_c09ac6c3:


    "更加准确地说我太累了，所以遇到什么事情也都害怕不起来了。"


translate chinese day6_main_0a666ca8:


    me "我不觉得这是逃跑。"


translate chinese day6_main_6f0a1453:


    "...我慢慢的回答。"


translate chinese day6_main_2f8ebf8c:


    pi "哈？那这是什么？"


translate chinese day6_main_4ca5f33e:


    me "战略撤退..."


translate chinese day6_main_871ad7d7:


    pi "太好了!"


translate chinese day6_main_1141c237:


    "他爆发出一阵笑声。"


translate chinese day6_main_32251891:


    me "我说，你为什么不跟我说说这里发生了什么，你是谁?"


translate chinese day6_main_2f1ec6d2:


    pi "门口的那个人跟你说什么?"


translate chinese day6_main_794b660f:


    "他好像完全没有听见我的问题。"


translate chinese day6_main_07ceaf40:


    me "他说不要相信你。"


translate chinese day6_main_bc2b8f99:


    "我撒谎了。"


translate chinese day6_main_8f076af1:


    "不过另一方面我觉得那就是他的意思。"


translate chinese day6_main_b22a7523:


    pi "是啊，他总是那个样子，逃跑..."


translate chinese day6_main_85e1a38c:


    "我从他的话中听出了不屑。"


translate chinese day6_main_49ff021e:


    me "我不知道你在演什么戏，不过我可不陪你演!"


translate chinese day6_main_f1508df8:


    pi "哎呀，有何不可呢...再怎么说，你是明星啊!"


translate chinese day6_main_565563b0:


    "我看不清他的脸，不过我可以确定他在笑。"


translate chinese day6_main_26e04ee0:


    me "那给我讲讲我的剧情!"


translate chinese day6_main_e4cb38e3:


    pi "你懂的，刚开始，一切看起来都是那么的平静。"


translate chinese day6_main_6ca36315:


    pi "然后我逃跑，变得疯狂，然后拷打他们，询问事实!"


translate chinese day6_main_a667d35b:


    "他疯狂地笑着。"


translate chinese day6_main_5eb260fc:


    "我开始发抖。"


translate chinese day6_main_8b9a92c3:


    pi "但是这毫无意义!"


translate chinese day6_main_6b9446af:


    "...少先队员冷静下来一些说道。"


translate chinese day6_main_2a471045:


    pi "毫无意义...{w}然后我注意到..."


translate chinese day6_main_a7d4acbc:


    pi "最开始我只是听见他们的声音，在远方，还有自己的大脑中。"


translate chinese day6_main_3dc38f3b:


    pi "然后模糊的身影出现了，他们慢慢出现实体。"


translate chinese day6_main_c695ff8a:


    pi "最后他们来到了我的世界，我可以触摸到，可以把他介绍给其他的少先队员..."


translate chinese day6_main_81e2b75f:


    pi "而且他们都不一样！你知道什么意思吗？不一样!"


translate chinese day6_main_54c1072c:


    "他开始喊叫。"


translate chinese day6_main_634696b6:


    pi "然后这一次次的发生，没完没了，你得试着熟悉，不过..."


translate chinese day6_main_b4bb03eb:


    pi "然后我学会了怎么进入他们的世界，怎么和他们交流，然后我发现我们有很多!"


translate chinese day6_main_62a7ca22:


    pi "今天你至少还遇到一个。"


translate chinese day6_main_7e8f7bb7:


    "他沉默了。"


translate chinese day6_main_f4b1a463:


    "我不知道该问些什么好，而且又不想打断他的故事，所以我什么也没有说。"


translate chinese day6_main_0c7f5431:


    "过了一分钟他又继续了。"


translate chinese day6_main_bb9de06b:


    pi "当然这没有那么简单，而且这也不总是可能的。"


translate chinese day6_main_7fd20c4d:


    pi "只有在特殊的情况下，比如说当你有强烈的情感的时候。"


translate chinese day6_main_fb2863a3:


    "我突然回忆起我第一次见到他的时候，是我第一次没有任何感情的时候。"


translate chinese day6_main_f7c370d3:


    pi "包括你感觉快乐的时候..."


translate chinese day6_main_75932a4e:


    "好像他可以看到我的想法一样。"


translate chinese day6_main_505d91ae:


    me "好吧..."


translate chinese day6_main_88477eb9:


    me "你是说有很多的平行世界，我在其中可能会被你或者车站那里的那个人代替掉...?"


translate chinese day6_main_ee412b1d:


    pi "是啊，大概是那个样子..."


translate chinese day6_main_7c3f9b3e:


    "他的答案完全没有让我惊讶。"


translate chinese day6_main_04ff5586:


    "结果这里发生的事情没有那么超出人类的理解界限。"


translate chinese day6_main_b263087c:


    me "不过你说所有的事情都会再次发生..."


translate chinese day6_main_c5256338:


    pi "是的，再次发生。"


translate chinese day6_main_90478ae9:


    me "那么之后会怎么样?"


translate chinese day6_main_af16c541:


    pi "你会从头开始，在车上醒来，遇到奥尔加·德米特里耶夫娜，女孩子们..."


translate chinese day6_main_eea0fa70:


    me "不过如果这件事发生在你的身上，为什么也会发生在我的身上?"


translate chinese day6_main_cae5da7b:


    pi "这发生在所有人的身上!"


translate chinese day6_main_25c5c857:


    "他又丧心病狂的笑了起来。"


translate chinese day6_main_571d1544:


    me "那么你已经遇到了多少次循环了?"


translate chinese day6_main_7768eb2b:


    pi "我已经不再数了，开始我还试着记住，现在大概几百次了吧..."


translate chinese day6_main_7927bc7d:


    "不难理解，怪不得这个人现在不太正常。"


translate chinese day6_main_72d6794a:


    me "不过你找到了出去的路，不是吗?"


translate chinese day6_main_ab51e1d2:


    "他没有回答。"


translate chinese day6_main_dfec187b:


    me "那就是在车站前的那个人说的。"


translate chinese day6_main_e710f157:


    pi "因为他什么都不懂!"


translate chinese day6_main_332b374a:


    "...他喊了起来。"


translate chinese day6_main_31847263:


    pi "因为他就像你一样，一直逃跑!"


translate chinese day6_main_42cd9689:


    me "那么你建议我怎么做?"


translate chinese day6_main_d5f8131d:


    pi "如果我有建议的话，我就不会站在这里和你说话了。"


translate chinese day6_main_3a64a45c:


    me "好吧，不过你尝试过，不是吗?"


translate chinese day6_main_5887898e:


    pi "你试着理解我说的话了吗，既然我都已经呆了很久了?"


translate chinese day6_main_c276f7a2:


    "...他兴奋的问道。"


translate chinese day6_main_c38656e1:


    me "这个嘛，很明显..."


translate chinese day6_main_94935db0:


    pi "是啊，我曾经尝试着逃跑，离开这里。"


translate chinese day6_main_6bc52dbe:


    pi "那里..."


translate chinese day6_main_ee7633ba:


    "他指着森林。"


translate chinese day6_main_57f82592:


    pi "...什么都没有，{w}只有树。{w}我尝试过，走了好几天，然后晕倒过去，醒过来又一次出现在公交车里。"


translate chinese day6_main_00d4b823:


    pi "沿着道路走也一样，无穷无尽。"


translate chinese day6_main_24fd33fb:


    pi "和别人谈话也是没有意义的，你应该已经知道了。"


translate chinese day6_main_fb6303a8:


    me "是的。"


translate chinese day6_main_677a1d30:


    "...我打断到。"


translate chinese day6_main_ea337e24:


    pi "即使你直接的进行解释，最好也是被当成一个傻子。"


translate chinese day6_main_a3fc2851:


    me "如果你试着呆在公交车里呢?"


translate chinese day6_main_75d11f10:


    pi "同样没有意义，你总会被找到的。"


translate chinese day6_main_3ba30819:


    pi "我甚至尝试过一只呆在那里，不过我睡着了，然后一切由从头开始。"


translate chinese day6_main_472d9432:


    me "那么你能不能尝试自己着车离开呢?"


translate chinese day6_main_5782e23f:


    pi "没有钥匙，我又不会偷车。"


translate chinese day6_main_9e6c1787:


    me "被诅咒的循环..."


translate chinese day6_main_8712b475:


    pi "就是!"


translate chinese day6_main_d39e8b29:


    "接下来是一段长久的沉默。"


translate chinese day6_main_1d014408:


    "Finally, I asked:"


translate chinese day6_main_99ff1407:


    me "这里的人们呢，没有感觉可疑吗?!"


translate chinese day6_main_92239f1a:


    pi "可疑?"


translate chinese day6_main_bbf472a0:


    "又是笑声。"


translate chinese day6_main_b33ce14d:


    pi "唯一可疑的是不管你怎么说他们，他们都只是愣着看着你!"


translate chinese day6_main_24929dab:


    pi "那些人...我开始也害怕他们。"


translate chinese day6_main_a250d056:


    pi "然后我拿他们做实验，我发现他们不是人，只是人偶!"


translate chinese day6_main_74deb4fc:


    pi "你可以简单的预测出他们的行动或者语言!"


translate chinese day6_main_de26d074:


    me "所以没有必要害怕他们?"


translate chinese day6_main_e38b3c64:


    pi "唯一需要怕的是你..."


translate chinese day6_main_47ff30a4:


    "...他悄悄的说。"


translate chinese day6_main_9180ae03:


    pi "你知道人类的骨头会在拉伸的时候发出奇怪的响声吗...?"


translate chinese day6_main_642dbde1:


    th "他好像终于失去了理智。"


translate chinese day6_main_d97fd181:


    me "我说，这我都懂，可是..."


translate chinese day6_main_c5e21a03:


    pi "别的人也觉得这有点过了..."


translate chinese day6_main_88c33a5e:


    pi "不过没有关系，除了你别人都不是真的!"


translate chinese day6_main_90713cb4:


    "少先队员突然沉默了。"


translate chinese day6_main_8520460f:


    me "不，我不同意。"


translate chinese day6_main_e8e26d07:


    pi "你可能在想，为什么我没有自己发现这一切，为什么我没有尝试去其他的城镇？"


translate chinese day6_main_7c6f498a:


    me "是的，我的确有这种想法。"


translate chinese day6_main_691f28f6:


    pi "为什么我没有怀疑，为什么我没有去寻找答案，而是去干了别的事情？"


translate chinese day6_main_2c057d1c:


    me "嗯...是..."


translate chinese day6_main_fa8bd2c9:


    pi "别想了！那很正常！我们第一次来到这里的时候都是这样的。"


translate chinese day6_main_a5bbe860:


    me "不过你至今到底见过多少人呢？"


translate chinese day6_main_aa579a81:


    pi "不是那么多。"


translate chinese day6_main_447e6d41:


    "少先队员陷入思考。"


translate chinese day6_main_dcc4b12f:


    pi "大概十个人左右吧，不过我确定还有更多的人！"


translate chinese day6_main_011e2d8a:


    me "那么，大家都是这个样子的吗?"


translate chinese day6_main_65c1d01f:


    pi "事实上是的，只有具体的细节有所不同，总的来说都是一样的—逃不出去！"


translate chinese day6_main_01815b85:


    pi "而且顺便告诉你，你和大部分人都一样!"


translate chinese day6_main_509c847e:


    pi "唯一的区别是这些事情你都从我这里听说了，而不是通过自己的试验得来。"


translate chinese day6_main_e03c0d9a:


    "我不知道自己是不是应该感谢他..."


translate chinese day6_main_4b5045fd:


    me "那么现在应该干什么?"


translate chinese day6_main_f3d36d37:


    pi "什么也不做。"


translate chinese day6_main_d9259528:


    "...他简短的回答着。"


translate chinese day6_main_36cf3956:


    "我闭上眼睛陷入了沉思。{w}这是一个严重的错误。"


translate chinese day6_main_0925d989:


    "等我睁开眼的时候，一个人都没有了，就像上一次一样。"


translate chinese day6_main_6d094282:


    th "好了，现在我知道是怎么回事了，不过他说的这些话怎样变成逃离这里的方法呢?"


translate chinese day6_main_387743f2:


    th "是的，我不是一个人，是的，这里的一切都在不停的重演，可是这些都是为什么呢？"


translate chinese day6_main_ad7a645c:


    th "而且最重要的——如何离开这里...?"


translate chinese day6_main_f75ae07b:


    "我从对话中听出来唯一有用的话就是我不必害怕这里的人们。"


translate chinese day6_main_a7aff17c:


    "不过这个结论对我很重要，因为回去睡个好觉总比睡在森林里强得多。"


translate chinese day6_main_2c3edc12:


    "我叹了一口气，拿起我的东西，前往营地。"


translate chinese day6_main_89090f90:


    "在我往回走的路上，我们之间的对话不停地在我的头脑中闪现。"


translate chinese day6_main_b3935654:


    th "为什么我没有询问他手机上消息的问题，还有其他的各种暗示什么的..."


translate chinese day6_main_222e2152:


    th "也许等我发现一些具体的线索之后，就可以得出一些结论。"


translate chinese day6_main_bd2b9931:


    "然而我不觉得我们之间的谈话会就此结束。"


translate chinese day6_main_168f0e75:


    "辅导员的房间亮着灯。"


translate chinese day6_main_535a1938:


    "我打开房门，优雅的走了进去。"


translate chinese day6_main_f6e23c0c:


    mt "你终于回来了!"


translate chinese day6_main_30eab556:


    "...辅导员愤怒的说道。"


translate chinese day6_main_5ad08176:


    me "奥尔加·德米特里耶夫娜，我太累了，咱们还是不要开批斗会了吧。"


translate chinese day6_main_54224d71:


    mt "你因为什么累?"


translate chinese day6_main_2e9daa0c:


    me "因为各种事情!"


translate chinese day6_main_df4d1541:


    "...我粗鲁的挥舞着手臂。"


translate chinese day6_main_0193dcd6:


    "辅导员惊讶的看着我。"


translate chinese day6_main_33eccc5f:


    "我没有脱衣服就躺在了床上。"


translate chinese day6_main_307422bd:


    mt "谢苗，一个优秀的少先队员不应该这样!"


translate chinese day6_main_b8a5fff1:


    me "那应该怎样?"


translate chinese day6_main_dccb57d2:


    mt "不是这样..."


translate chinese day6_main_9a8666dc:


    "辅导员说话卡住了，好像在选择正确的词句。"


translate chinese day6_main_fd3b5cff:


    mt "应该尊重长辈!"


translate chinese day6_main_56af3994:


    me "我很尊重您..."


translate chinese day6_main_105fc865_1:


    mt "谢苗!"


translate chinese day6_main_8b538c95:


    "我语气中的讽刺意味显然被发现了。"


translate chinese day6_main_87be519f:


    me "我现在要睡觉了。"


translate chinese day6_main_f10bfcfd:


    mt "等一下，我..."


translate chinese day6_main_b616628c:


    me "对了，离这里最近的小镇有多远？公交车什么时候抵达？我该如何离开这里？"


translate chinese day6_main_a2cc86f7:


    "...我用一种神奇的冷静的声音问着。"


translate chinese day6_main_68f9d7af:


    "没有回答，也不应该有回答。"


translate chinese day6_main_d16f1af1:


    "我说这些只是为了让她走开。"


translate chinese day6_main_6cba906b:


    me "你为什么不说话了?"


translate chinese day6_main_3dfe0def:


    mt "我累了，咱们明天再说吧。"


translate chinese day6_main_03e11cff:


    "辅导员起来关掉了灯。"


translate chinese day6_main_e49c22a1:


    th "是的，正如那个人所说。"


translate chinese day6_main_0d4a118f:


    th "我奇怪自己为什么和他说话的时候那么冷静?"


translate chinese day6_main_c64fd468:


    th "我应该是害怕的打着哆嗦啊..."


translate chinese day6_main_c516c7b0:


    "那个少先队员不怎么靠谱，不过我有一种感觉，他应该不会伤害我。"


translate chinese day6_main_7af9b647:


    "正当我想要在思考一些别的什么的时候，疲倦让我沉睡了过去。"


translate chinese day6_un_f1700078:


    "有人摇晃我的肩膀，把我弄醒了。"


translate chinese day6_un_1a840eb3:


    "我感觉睁开眼很困难，所以只是呻吟着。"


translate chinese day6_un_6b65ae8f:


    mt "快起床！你要错过集合了!"


translate chinese day6_un_df156736:


    "我一意识到奥尔加·德米特里耶夫娜想要让我做什么还有现在的时间，就翻过身去。"


translate chinese day6_un_cb9038f3:


    "我现在很累，我想掐她，为什么不让我休息！"


translate chinese day6_un_05acfb3e:


    mt "谢苗！快起来!"


translate chinese day6_un_ceeafad7:


    "我用上全身的力气，睁开眼睛，坐了起来。"


translate chinese day6_un_1e98f221:


    me "奥尔加·德米特里耶夫娜...{w}我懂，但是我昨天很累..."


translate chinese day6_un_e57c744c:


    me "就今天，我能不能多睡一会儿?"


translate chinese day6_un_10659a77:


    "我开始恳求。"


translate chinese day6_un_215606b1:


    mt "这不可能！集合是每个人的义务，而且你已经错过几次了！"


translate chinese day6_un_703bafc8:


    "我的头木木的，想不出反驳的理由。"


translate chinese day6_un_f9754f3e:


    "没过几分钟，我们已经站在广场了。"


translate chinese day6_un_22b326f3:


    "我在不停的打瞌睡，能够站着不倒已经很困难了，辅导员说的话我一句也没有听见。"


translate chinese day6_un_df687591:


    "大部分少先队员们都和我情况差不多。"


translate chinese day6_un_58d2f9bd:


    "电子小子不停的打着哈欠，阿丽夏睡眼惺忪，只有乌里扬娜充满了活力，就像平常一样。"


translate chinese day6_un_316f08e7:


    "我揉揉眼睛，看看我们的队列，没有列娜的身影。"


translate chinese day6_un_ecc81dcb:


    th "很奇怪。{w}她应该是一个乖巧又刻苦的女孩子，不像是会错过这种活动的人。"


translate chinese day6_un_3d4c4b35:


    th "不过话说回来，昨天她经历了太多，她可能现在很失落..."


translate chinese day6_un_f49ecdba:


    "虽然这样的行为很奇怪。"


translate chinese day6_un_9512b0f6:


    "我怀疑真正的她不是她想要让大家了解的那个样子，不过也没有想到转变居然如此剧烈。"


translate chinese day6_un_f2c60111:


    "列娜，神奇的让我想起了阿丽夏，她可是经常十分粗鲁。"


translate chinese day6_un_e4d65836:


    "现在我都不知道怎么面对她了，我都不敢靠近了。"


translate chinese day6_un_ff2fa3df:


    "最终，集合结束了，少先队员们前去吃早饭。"


translate chinese day6_un_20de4020:


    "未来在食堂附近找到我。"


translate chinese day6_un_b0f1c022:


    mi "谢苗，早上好！晚上睡得怎么样？有没有做梦？早上感觉如何？准备好吃早饭了吗？"


translate chinese day6_un_8d857931:


    "她就像平常一样说个不停，外带一个可爱的微笑。"


translate chinese day6_un_e7ce88cd:


    me "我还好。"


translate chinese day6_un_56b00510:


    "我懒洋洋的回答着。"


translate chinese day6_un_ec604917:


    mi "今天的天气很阴沉，好像就要下雨一样，对了今天大家都是这样阴沉的！也许是发生了什么不好的事情但是都没有告诉我吗？"


translate chinese day6_un_aafa05de:


    mi "你能想象吗！有什么大事发生了，大家都知道，但是只有我不知道，可是..."


translate chinese day6_un_bb18abf7:


    me "不要担心，你不会错过世界末日的。"


translate chinese day6_un_27c62366:


    "我挖苦了一下。"


translate chinese day6_un_57d2c9aa:


    mi "哈?"


translate chinese day6_un_94555fb6:


    "她好像沉浸在自己的独白中。"


translate chinese day6_un_a7320a03:


    me "如果你错过了，我会想办法让你知道的..."


translate chinese day6_un_c63fd243:


    me "没什么，享受你的午餐吧。"


translate chinese day6_un_63855039:


    "我前往食堂，取得我每日必须的糖类，蛋白质还有脂肪。"


translate chinese day6_un_88b41ad3:


    "让我惊讶的是，远处的角落里竟然还有一个座位。"


translate chinese day6_un_c21d2e19:


    "我坐下来，并且做着坚定地逐客表情，告诉大家除非有紧急情况，否则不要接近我。"


translate chinese day6_un_b5a8b072:


    "我只是想要肚子坐在这里思考，而且内心清静可以让我不是那么困。"


translate chinese day6_un_c0c4e707:


    sl "嗨，介意吗?"


translate chinese day6_un_3501408a:


    "我没有注意到斯拉维娅站在这里。"


translate chinese day6_un_d80e4062:


    th "奇怪..."


translate chinese day6_un_9513cd87:


    me "可以..."


translate chinese day6_un_d20f4795:


    "我犹豫了一秒钟以后回答道。"


translate chinese day6_un_59a6b536:


    sl "心情不好?"


translate chinese day6_un_997c151d:


    me "有一点。"


translate chinese day6_un_e4d522d4:


    sl "发生了什么事吗?"


translate chinese day6_un_b97e3fd5:


    me "没什么..."


translate chinese day6_un_87297b64:


    sl "你不想说的话可以不用说的。"


translate chinese day6_un_cbce3f49:


    me "跟你没什么可说的。"


translate chinese day6_un_6488c1ac:


    "我们继续安静的吃饭，直到我问道："


translate chinese day6_un_79ee911e:


    me "你昨晚去了哪里?"


translate chinese day6_un_e1994127:


    sl "我？哦，我只是想要自己呆一会儿。"


translate chinese day6_un_4a2c94f6:


    me "那不像你啊。"


translate chinese day6_un_f59ecd73:


    "我感觉自己有了一点儿活力。"


translate chinese day6_un_c6212685:


    sl "真的吗？那好吧...我只是觉得这样阴沉不像是你。"


translate chinese day6_un_0139b861:


    th "也许她是对的。"


translate chinese day6_un_d560542b:


    "我平常在很糟糕的情况下也都尽力表现出积极的一面。"


translate chinese day6_un_e4c10516:


    "因为我是一个乐观的人，所以一直尝试着不让心情变差。"


translate chinese day6_un_24507d3d:


    "保持我原来的心情，这很正常，不过一旦忧郁的心情找上你，整个世界就都变的不一样了。"


translate chinese day6_un_6ab05865:


    me "也许吧。"


translate chinese day6_un_0565c647:


    "斯拉维娅吃完饭了，我还在不停的喝粥。"


translate chinese day6_un_d23277e3:


    sl "我要走了。"


translate chinese day6_un_e4eadbc7:


    me "对了，你有没有看到列娜?"


translate chinese day6_un_e00071aa:


    sl "没有为什么问这个呢?"


translate chinese day6_un_2283b18c:


    me "早晨集合的时候没有看到她，这很奇怪，不像是她。"


translate chinese day6_un_5d832dbe:


    sl "也许你是对的，不过我觉得这不是什么大事。"


translate chinese day6_un_be2f234f:


    me "是啊，当然，我只是很好奇..."


translate chinese day6_un_2da87938:


    "我又坐了一会儿，然后没等吃完早饭就离开了。"


translate chinese day6_un_fdfb7569:


    th "今天的天气很阴沉，我来到这里之后第一次遇到这样的天气。"


translate chinese day6_un_f69f6bef:


    "我对这里的印象应该是只有到了晚上才能感到些许凉爽的。"


translate chinese day6_un_d29d26f8:


    "看来即使是在这里天气也是会变化的。"


translate chinese day6_un_d31c084d:


    th "好像和我的心情非常匹配..."


translate chinese day6_un_df21ce17:


    th "看起来这个世界的造物主很会讲故事。"


translate chinese day6_un_b83c2795:


    "有的时候我真的想把注意力集中到自己的问题上，完全不管其他人的事情。"


translate chinese day6_un_6752541e:


    "只靠自己的思考得出这到底是外星人，还是平行宇宙，还是什么秘密的实验..."


translate chinese day6_un_f82edbaf:


    "选出一个就完事了!"


translate chinese day6_un_89aa71e4:


    "不再被一个接一个的想法所困扰，只要有我自己的一个想法。"


translate chinese day6_un_78520b75:


    th "不过这是不可能的。"


translate chinese day6_un_4b4dd244:


    th "我几乎不了解任何事情，这里也没有什么特殊的事情发生在我的身上。"


translate chinese day6_un_a0795afe:


    "这里的确发生过一切奇怪的事情，不过它们在正常的世界中是有可能发生的。"


translate chinese day6_un_35362687:


    "就这样思考着，我发现自己来到了410公交车站，夏令营的大门口。"


translate chinese day6_un_4aa5ce10:


    th "没有答案，没有提示，没有线索..."


translate chinese day6_un_4fa5905f:


    "我漫无目的的走到哪儿算哪儿。"


translate chinese day6_un_f9c294f6:


    "在居住区几乎一个人也没有，十分寂静。"


translate chinese day6_un_6a121a11:


    "当电子小子出现在下一个路口的时候我更加惊讶了。"


translate chinese day6_un_a3828cd7:


    "我想要叫住他，不过停下了，他看起来十分自信的朝着不知道什么方向走过去。"


translate chinese day6_un_49160043:


    th "奇怪，这也不像是他自己。"


translate chinese day6_un_4fa01f29:


    th "而且我要和他说些什么，我还得开始一段对话..."


translate chinese day6_un_7e840313:


    th "我很绝望。"


translate chinese day6_un_42f4c858:


    "然而，跟踪他，看看他到底去哪儿可能很有意思。"


translate chinese day6_un_de5de174:


    "我小的时候很喜欢玩间谍游戏，现在是实战的时候了!"


translate chinese day6_un_5a5a09fe:


    "我没有鬼鬼祟祟的到处躲藏，而是普通的悄悄跟在后面。"


translate chinese day6_un_6b9662bf:


    "很快我们来到了图书馆，电子小子敲敲门走了进去。"


translate chinese day6_un_ad78315b:


    "我躲在一棵大树后面等着。"


translate chinese day6_un_a20cefa7:


    "…"


translate chinese day6_un_c1a2fbe8:


    "他消失了很长时间。"


translate chinese day6_un_2cee12ba:


    th "也许这个主意很愚蠢，而且他去图书馆有什么奇怪的呀，也许只是想要找本书读读?"


translate chinese day6_un_41a8466c:


    th "他走得很快？为什么？"


translate chinese day6_un_539d4165:


    th "也许他接下来还有事情要做..."


translate chinese day6_un_5b0195d4:


    "我的思路被摔门的声音打断了。"


translate chinese day6_un_66094c34:


    "我看看图书馆，发现了电子小子，正在逃跑，热妮娅在后面追着:"


translate chinese day6_un_07d3d267:


    mz "我再也不想听见，也不想见到你!"


translate chinese day6_un_d825c319:


    "他们从我面前经过，不过没有发现我。"


translate chinese day6_un_17db73bb:


    "整个事件看起来都非常搞笑，我决定去弄清楚到底发生了什么。"


translate chinese day6_un_8bbec564:


    th "我很奇怪电子小子是去哪里，那样奇怪的跑?"


translate chinese day6_aidpost_5f9ed739:


    "也许是往医务室?"


translate chinese day6_aidpost_ceb48884:


    th "不，那太愚蠢了..."


translate chinese day6_dinner_e22dfa0f:


    "躲在食堂里应该是一个好主意。"


translate chinese day6_dinner_78f58283:


    "不过他也不在这里。"


translate chinese day6_after_map_e40ce72e:


    th "是的，显然他应该直接奔向了机器人活动室。"


translate chinese day6_after_map_fcae6aa1:


    "我没有敲门就走了进去，不过里面没有人。"


translate chinese day6_after_map_249528f8:


    me "电子小子，是我!"


translate chinese day6_after_map_71f484c6:


    "隔壁房间传来了脚步声，电子小子出现了。"


translate chinese day6_after_map_13e9141f:


    el "你好谢苗，我只是..."


translate chinese day6_after_map_930b325a:


    "他的眼中充满了愧疚，身上全都是汗。"


translate chinese day6_after_map_1e590c6b:


    me "做运动，冲刺。"


translate chinese day6_after_map_3573e50d:


    el "我...你看见了?"


translate chinese day6_after_map_594616b0:


    "他好像遇到末日一样。"


translate chinese day6_after_map_ccf2f1f7:


    me "是啊，完全是一个巧合，所以我停下来问问你到底发生了什么?"


translate chinese day6_after_map_603882a4:


    el "真的没有什么..."


translate chinese day6_after_map_61b29886:


    th "是啊，逃离一个愤怒的图书管理员，没什么特殊的..."


translate chinese day6_after_map_0a1f52d0:


    me "你可以告诉我。"


translate chinese day6_after_map_260966c7:


    "我友好的笑了笑。"


translate chinese day6_after_map_e53cf926:


    el "真的？你不会告诉..."


translate chinese day6_after_map_88f39b4b:


    me "当然！打死我也不说!"


translate chinese day6_after_map_8636977a:


    "我好像有点夸张了，不过他似乎还是相信了。"


translate chinese day6_after_map_ab8f7115:


    el "好吧。"


translate chinese day6_after_map_a4e10992:


    "他深吸了一口气。"


translate chinese day6_after_map_4b01cbdb:


    el "那个，我从第一天就喜欢热妮娅..."


translate chinese day6_after_map_3763685c:


    "我差点笑得在地上打滚，不过还是忍住了，不过后面的几句话还是没有听见。"


translate chinese day6_after_map_7d9d9a52:


    el "...然后我决定了，接下来的事情你都看到了..."


translate chinese day6_after_map_58f9de3f:


    th "真正的勇士。"


translate chinese day6_after_map_b8456c80:


    me "那个，你...只要更加努力，一定会成功的!"


translate chinese day6_after_map_1a0d5f6e:


    "我拍了拍他的肩膀，努力忍住不笑出来。"


translate chinese day6_after_map_3ebcd212:


    el "感谢你的支持。"


translate chinese day6_after_map_d5ffee81:


    "他苦笑着。"


translate chinese day6_after_map_3b5c973e:


    me "好了，我得走了，我还有事。"


translate chinese day6_after_map_6e47a0d1:


    "我飞速冲出活动室，大笑起来。"


translate chinese day6_after_map_a3fe05d4:


    th "不过想一想的话，电子小子和热妮娅真的是一对儿。"


translate chinese day6_after_map_5ea74e09:


    th "他们性格挺和的，热妮娅居然拒绝了他..."


translate chinese day6_after_map_9c84def6:


    "我走向广场继续思考着这件事情。"


translate chinese day6_after_map_4c7202cc:


    th "但是电子小子不是这样的傻瓜..."


translate chinese day6_after_map_27837c94:


    th "他知道结果但还是义无反顾。或者他真的傻到一定程度了?"


translate chinese day6_after_map_b874d159:


    th "不管怎么说，你都可以感受到他的诚意。"


translate chinese day6_after_map_d0e49694:


    th "一个更有想法的人会准备上几天，几星期来思考手段还有结果什么的，我大概就是这样的。"


translate chinese day6_after_map_16546c05:


    th "我应该是这样做过的。"


translate chinese day6_after_map_604e72aa:


    th "不过他就那样说出来了，肯定是不成功，不过也许有可能有不同的结果..."


translate chinese day6_after_map_69934023:


    "这件事让我很忧愁，本来不会是这样的。"


translate chinese day6_after_map_db39b8db:


    "食堂里人很多。{w}夏令营里的人们似乎经过早上的压抑以后开始不断的出现了。"


translate chinese day6_after_map_583f6bbf:


    "也许是因为太阳出来了，也许是因为我在电子小子那里的时候又发生了什么。"


translate chinese day6_after_map_252e9ca8:


    "只有乌里扬娜和阿丽夏旁边的位置是空的。"


translate chinese day6_after_map_09b90b56:


    "我坚定地走过去，再怎么说吃饭还是很重要的。"


translate chinese day6_after_map_ec04d37b:


    me "我可以坐在这里吗?"


translate chinese day6_after_map_b4f1aecf:


    dv "坐吧。"


translate chinese day6_after_map_57fa2035:


    us "你怎么这么忧郁?"


translate chinese day6_after_map_35ce6c29:


    me "是你太兴奋了，我只是在试图保持宇宙之中的平衡。"


translate chinese day6_after_map_a064eabe:


    "乌里扬卡咯咯笑着。"


translate chinese day6_after_map_8c8bd74b:


    "奇怪的是，女孩们没有在意我，而只是在讨论她们自己的事情。"


translate chinese day6_after_map_cc02e7a3:


    "刚开始我觉得这很好，不过接下来我感觉这是她们没有注意到我。"


translate chinese day6_after_map_2b3338c6:


    me "对了，列娜在哪里?"


translate chinese day6_after_map_11255df4:


    "我一直在想别的，差点把她忘了。"


translate chinese day6_after_map_3e69c35f:


    dv "我不知道。"


translate chinese day6_after_map_d4d17245:


    "阿丽夏心不在焉的说着。"


translate chinese day6_after_map_84eb34c3:


    me "她还没有出现吗?"


translate chinese day6_after_map_f355226b:


    dv "你都看到了..."


translate chinese day6_after_map_3fd95c7c:


    "我环视着整个食堂，没有她的身影。"


translate chinese day6_after_map_9fcb4846:


    me "没有人有她的消息吗?"


translate chinese day6_after_map_e8936f84:


    dv "没有。"


translate chinese day6_after_map_ae30c1d7:


    me "你不觉得很奇怪吗?"


translate chinese day6_after_map_c1788a1d:


    us "有什么奇怪的，她可能在看书，睡觉或者什么的。"


translate chinese day6_after_map_01b5873a:


    me "你好像在说你自己。"


translate chinese day6_after_map_f3820ec1:


    dv "这和你没有关系吧?"


translate chinese day6_after_map_c501a590:


    "阿丽夏很生气的打断。"


translate chinese day6_after_map_61d482ed:


    me "如果有一个人失踪的话..."


translate chinese day6_after_map_37715109:


    dv "舒里克失踪的时候你可没有这么着急!"


translate chinese day6_after_map_74053528:


    me "那可不一样。"


translate chinese day6_after_map_0e2114f3:


    dv "是吗？为什么?"


translate chinese day6_after_map_1b8b414c:


    "我没有答案，呆呆的看了阿丽夏一会儿之后，继续吃我的饭。"


translate chinese day6_after_map_517d0fc9:


    "她没有继续对话。"


translate chinese day6_after_map_a20cefa7:


    "…"


translate chinese day6_after_map_b686c259:


    "午饭结束了，我没有说话的走出了食堂。"


translate chinese day6_after_map_9d0e81e1:


    th "列娜消失了，我现在应该做什么?"


translate chinese day6_after_map_be3ae4b1:


    th "话说回来，我为什么要做什么？我在哪里，这都是些什么人?"


translate chinese day6_after_map_25de3652:


    th "谁也弄不清列娜真正的面目。"


translate chinese day6_after_map_eeb3cd87:


    th "也许这一切都并不存在，我还忙什么呢...?"


translate chinese day6_after_map_fd2a5fac:


    th "然而对我来说，列娜就是列娜，我第一天来到这里的时候看到的乖巧的小女孩。"


translate chinese day6_after_map_7edba5d0:


    th "她奇怪的举止也无法改变我对她的感觉。"


translate chinese day6_after_map_22235f9d:


    th "我都不知道自己是不是还存在，所以至少在这个世界看起来还很有逻辑的时候，我还是按照它的规则做事吧!"


translate chinese day6_after_map_fd5f28b2:


    "我迅速回到辅导员的宿舍。"


translate chinese day6_after_map_c028139c:


    "奥尔加·德米特里耶夫娜正躺在床上看书。"


translate chinese day6_after_map_1246e12e:


    me "你知道列娜在哪里吗?"


translate chinese day6_after_map_017acf74:


    mt "不知道你问这个干什么?"


translate chinese day6_after_map_07ba9e65:


    me "她人不见了！早饭和午饭的时候都看不到她!"


translate chinese day6_after_map_9dd49ab7:


    mt "所以呢?"


translate chinese day6_after_map_4de002b5:


    "她呆呆的看着我。"


translate chinese day6_after_map_1c44d4fc:


    me "什么叫所以呢？舒里克消失的时候整个夏令营从早晨就开始搜索！"


translate chinese day6_after_map_fe72fec3:


    mt "我不明白你的意思。"


translate chinese day6_after_map_6fa16493:


    "辅导员又变得很奇怪了，她的反应非常不正常。"


translate chinese day6_after_map_ad45a807:


    me "你觉得这很正常吗？那么她在哪里呢？"


translate chinese day6_after_map_9c19b5ad:


    mt "我不知道。"


translate chinese day6_after_map_53405852:


    "奥尔加·德米特里耶夫娜冷静的说着。"


translate chinese day6_after_map_b8029b05:


    me "太过分了!"


translate chinese day6_after_map_6ea78a01:


    "我忍不住要发脾气。"


translate chinese day6_after_map_8009c859:


    mt "问问未来，反正她是列娜的室友。"


translate chinese day6_after_map_c6b76c70:


    th "那是一个好主意，因为我在这里显然得不到更多的答案。"


translate chinese day6_after_map_b424b5bf:


    "我离开了房间，摔上门，前往未来和列娜的房间。"


translate chinese day6_after_map_b83fcbd4:


    "还好，这个乐队女孩曾经告诉过我她们的房间在什么地方，所以我过了一分钟就来到了列娜的门前。"


translate chinese day6_after_map_df84c43e:


    "我应该敲门，不过不知道为什么我做不到。"


translate chinese day6_after_map_3bda9055:


    "深呼吸几次以后，我终于敲响了大门。"


translate chinese day6_after_map_c7ed3d71:


    mi "请进!"


translate chinese day6_after_map_3417c9cb:


    "我听到了一个熟悉的声音。"


translate chinese day6_after_map_7299645f:


    me "你好...你知道列娜在哪里吗?"


translate chinese day6_after_map_02794ad5:


    mi "不知道...我今天没有看见她，你在找她吧?"


translate chinese day6_after_map_374cfeae:


    me "你不觉得这很奇怪吗?"


translate chinese day6_after_map_9d504cc9:


    "现在我觉得这里的人们在向我隐藏关于列娜的真相。"


translate chinese day6_after_map_2f93b124:


    mi "那个我觉得她是去了哪里，可是我自己的事情太多，我准备早饭，午饭，乐队，帮忙，各种，..."


translate chinese day6_after_map_f7d66ec2:


    me "好吧，我明白了，那么昨天呢？昨天是不是正常的呢？"


translate chinese day6_after_map_e9cf8521:


    mi "那个，她昨天回来的很晚，然后马上就去睡觉了，我没有看出什么特殊的地方。"


translate chinese day6_after_map_86efd052:


    "这里也没有任何的线索。"


translate chinese day6_after_map_3066598f:


    me "谢谢。"


translate chinese day6_after_map_d9b28d3a:


    "我有些粗鲁的说道，然后离开了。"


translate chinese day6_after_map_6130e681:


    "我现在觉得消失掉的列娜是这个地方唯一的有生命的人，我一定要找到她!"


translate chinese day6_after_map_e739b113:


    "然而只靠我自己大概是不可能的，所以我前去找外援。"


translate chinese day6_after_map_a35b54c4:


    th "谁最有可能帮助我？斯拉维娅!"


translate chinese day6_after_map_a328c184:


    "我觉得她可能在进行清扫工作，也就是说在广场。"


translate chinese day6_after_map_b89225c8:


    "我前往了广场。"


translate chinese day6_after_map_15d046bb:


    "我的第六感没有让我失望。"


translate chinese day6_after_map_1a8f3935:


    me "你好!"


translate chinese day6_after_map_9104ac12:


    sl "嗨!"


translate chinese day6_after_map_2c385c8f:


    me "你有没有看到列娜?"


translate chinese day6_after_map_e00071aa:


    sl "没有，为什么问这个?"


translate chinese day6_after_map_c72e7da6:


    me "自从今天早晨就没有人再看到过她，不管是早饭，午饭..."


translate chinese day6_after_map_cffc6886:


    sl "奇怪。"


translate chinese day6_after_map_ec0bd519:


    me "我也觉得，说好听了是奇怪。你能帮我一起找她吗?"


translate chinese day6_after_map_ce25ce10:


    sl "啊，抱歉，等一会儿好吗，我现在需要打扫..."


translate chinese day6_after_map_19e9e137:


    "就像是晴天霹雳。"


translate chinese day6_after_map_96afba80:


    "我向后退开几步，然后跑开了。"


translate chinese day6_after_map_235cdb53:


    th "不！那不是她！好像有什么别的人代替了她！这个夏令营里所有的人都是这样。"


translate chinese day6_after_map_7012273e:


    th "到底发生了什么？最奇怪的是，这些都和我没有关系，而是和列娜有关!"


translate chinese day6_after_map_e760a29d:


    th "也许她是和我差不多的方式来到了这里?"


translate chinese day6_after_map_83f4ce09:


    th "对啊!{w}所以她才大部分时间都很安静的。"


translate chinese day6_after_map_105dafff:


    th "不对，等一下...关于她认识阿丽夏的问题...?"


translate chinese day6_after_map_bf8e6d5c:


    th "啊，不对，说那个也没有用。"


translate chinese day6_after_map_cb18b97c:


    "我的大脑要爆炸了。"


translate chinese day6_after_map_915d90ea:


    "等我喘过气来，我已经来到了公交车站前，我坐在倒鸭子上，捂着脸。"


translate chinese day6_after_map_cbafac0b:


    th "如果以前有什么情况的时候我都帮不上忙，那个时候也都不是什么大事，可是现在情况不同了。"


translate chinese day6_after_map_a980985a:


    th "隔岸观火和置身战斗之中的感觉是不一样的。"


translate chinese day6_after_map_a20cefa7_1:


    "…"


translate chinese day6_after_map_fff50899:


    "我就这么坐着。"


translate chinese day6_after_map_f3fefe18:


    "时间一点点的过去，太阳开始落山。"


translate chinese day6_after_map_62739121:


    th "也许晚饭已经开始了。"


translate chinese day6_after_map_3edce0f9:


    th "虽然不太一样了，但是我还是不想去吃。"


translate chinese day6_after_map_41cc0c56:


    "我站起来，拖着沉重的脚步返回营地。"


translate chinese day6_after_map_7b58f3a3:


    th "就像平常一样，唯一的事情就是等待。"


translate chinese day6_after_map_a4552563:


    "我决定前往沙滩。"


translate chinese day6_after_map_a2ad895b:


    th "大家都在吃完饭的时候，我可以独自坐在这里思考。"


translate chinese day6_after_map_4bbcd690:


    th "虽然我不知道有什么可以思考的?"


translate chinese day6_after_map_6de0d6ca:


    "然而我的期望破灭了。"


translate chinese day6_after_map_9fc97989:


    "我看到热妮娅在海滩上，让我很吃惊。"


translate chinese day6_after_map_eb39fdd5:


    me "你也来这里了吗?"


translate chinese day6_after_map_47641ee9:


    "她从眼睛后面看着我。"


translate chinese day6_after_map_43937691:


    mz "你觉得我不是人吗?"


translate chinese day6_after_map_d9f75732:


    me "不，我不是那个意思。"


translate chinese day6_after_map_ed1dc0ad:


    mz "那你是什么意思?"


translate chinese day6_after_map_562a22e9:


    me "没...{w}对了，你有没有看到列娜?"


translate chinese day6_after_map_c48a46a8:


    mz "没有。"


translate chinese day6_after_map_f2e007f0:


    me "好吧..."


translate chinese day6_after_map_4e3a8dcb:


    mz "你想干什么?"


translate chinese day6_after_map_2dfb23fa:


    me "那个，只是从昨天晚上开始就没有人看到她了。"


translate chinese day6_after_map_e8b5756a:


    mz "你觉得会有人在这个夏令营里迷路吗?"


translate chinese day6_after_map_56d4e39c:


    "她大声笑着。"


translate chinese day6_after_map_cf3d96a9:


    me "舒里克成功的迷路了..."


translate chinese day6_after_map_a4dfd52d:


    mz "那是特殊的情况...{w}那两个活宝什么都干得出来。"


translate chinese day6_after_map_6ebc2860:


    "我马上想起了早晨的事件。"


translate chinese day6_after_map_340d0570:


    me "告诉我...{w}你早晨为什么在追电子小子?"


translate chinese day6_after_map_9889b720:


    "热妮娅有些不自在。"


translate chinese day6_after_map_63dcdb44:


    mz "这和你没有关系。"


translate chinese day6_after_map_efa19ecb:


    me "我只是问问。"


translate chinese day6_after_map_54df90eb:


    mz "因为他是一个蠢货!"


translate chinese day6_after_map_36d3e865:


    "她说完转了过去。"


translate chinese day6_after_map_33d14fd3:


    me "也许你不应该对他那么严厉。"


translate chinese day6_after_map_82514e53:


    "我也不知道自己是在帮电子小子说话还是单纯的为了把对话继续下去。"


translate chinese day6_after_map_16cd0044:


    mz "那我应该怎么对待他?"


translate chinese day6_after_map_5f7ad39e:


    me "那个，给他一个机会嘛...这种事情需要很大的勇气的。"


translate chinese day6_after_map_b2adba60:


    mz "你说得好像是什么特殊的成就似的...你自己能做到吗?"


translate chinese day6_after_map_846fcae4:


    "我思考了一会儿。"


translate chinese day6_after_map_e05aaca3:


    me "我不知道...我还没有合适的时机。"


translate chinese day6_after_map_a9c3105e:


    mz "但愿你早点找到那个时机。"


translate chinese day6_after_map_a7ce299f:


    "热妮娅冷淡的说道，然后走向了食堂。"


translate chinese day6_after_map_a6b3e5a5:


    "我坐在沙滩上想着。"


translate chinese day6_after_map_e55707cf:


    th "是啊，电子小子能做的出来，我是不是也能做到呢，这是个问题。"


translate chinese day6_after_map_182cea07:


    th "如果我能找到机会...{w}但是那是什么时候呢?"


translate chinese day6_after_map_df7d6c66:


    th "预测一件未来的事情是很简单的，只需要一些简单的想法。"


translate chinese day6_after_map_20124702:


    th "但是大部分情况下都和实际情况不符。"


translate chinese day6_after_map_7fad78b3:


    th "一个最简单的时间也可以全盘毁掉你的计划。"


translate chinese day6_after_map_04ab0520:


    th "如果你一定要等到万事俱备的时候再行动，很有可能最后什么也得不到。"


translate chinese day6_after_map_ab503b46:


    th "因此，对于热妮娅的问题，唯一正确的答案是“不”"


translate chinese day6_after_map_60b33880:


    th "不是一个模模糊糊的不太确定的答案，而是一个明确的“不”。"


translate chinese day6_after_map_7889693d:


    th "简单的“是”“否”"


translate chinese day6_after_map_f4bb7bbe:


    "我理解起来总是很困难。"


translate chinese day6_after_map_90e26c1c:


    "相反，我的答案往往都是“也许”“可能”“大概”“差不多”“我试试”..."


translate chinese day6_after_map_a20cefa7_2:


    "…"


translate chinese day6_after_map_e51168a0:


    "我一直沉浸在自己的思考之中，没有意识到黑夜已经笼罩了整个夏令营。"


translate chinese day6_after_map_3aa3c67c:


    "平常我肯定就去睡觉了，晚上去寻找列娜也不是一个好主意。"


translate chinese day6_after_map_d8482e89:


    "我慢慢的站起来，漫无目的的走着。"


translate chinese day6_after_map_c84711ef:


    "很快，我来到了运动场。"


translate chinese day6_after_map_97ec2962:


    "我站在这里呆了一会儿，等我准备离开的时候，突然听到了一些动静。"


translate chinese day6_after_map_cfc15ee6:


    th "啪嗒的声音...{w}听起来很熟悉!"


translate chinese day6_after_map_32a97e95:


    "我跑向排球场地，看到了列娜。她不断尝试着用拍子打到毽球。"


translate chinese day6_after_map_59811faa:


    "我震惊的站了很长时间。"


translate chinese day6_after_map_550358a1:


    "我的大脑空空的，只是感觉很高兴，高兴因为我找到了她，再一次见到了她。"


translate chinese day6_after_map_f86b9a25:


    "最终我恢复了意识，向前走去..."


translate chinese day6_after_map_d092d462:


    "不过，没走几步，我就停了下来。"


translate chinese day6_after_map_cf3bc831:


    th "我现在应该说什么呢？很高兴见到你？你去哪儿了，我好担心？"


translate chinese day6_after_map_de715a93:


    th "经过昨天的对话以后，我觉得她不太可能想和我说话。"


translate chinese day6_after_map_e0f727b2:


    th "万一列娜问我为什么要找她？为什么担心？"


translate chinese day6_after_map_4baf4e5a:


    th "我自己也不知道，也许是因为她消失了很长时间吧。"


translate chinese day6_after_map_db5327a9:


    th "也许其他的熟人失踪我也会有同样的感觉的。"


translate chinese day6_after_map_9e166d2c:


    th "也许我昨天如果表现的好一些的话，她今天就不会消失的。"


translate chinese day6_after_map_b1d2df25:


    th "我拿不太准，但是尽量尝试着找到合适的说法..."


translate chinese day6_after_map_5c5017b8:


    "我又向前走了一步，然后又停了下来。"


translate chinese day6_after_map_36e92b6c:


    th "也许，大概，可能，不确定..."


translate chinese day6_after_map_fac2cdca:


    th "这些词语不停的出现在我的生命中..."


translate chinese day6_after_map_ee8a9b71:


    th "毫不真诚!"


translate chinese day6_after_map_10d6f972:


    th "可是为什么？"


translate chinese day6_after_map_6179f9a3:


    th "我需要作出决定！最后的决定!"


translate chinese day6_after_map_beab36a3:


    th "虽然 ...{w}有两个最简单的词语“是”和“否”。"


translate chinese day6_after_map_bc795975:


    "最后，这一切都清楚了!"


translate chinese day6_after_map_40942432:


    "我走近运动场，笑着说:"


translate chinese day6_after_map_1a8f3935_1:


    me "嘿!"


translate chinese day6_after_map_f0d43f46:


    "她转过来看着我。"


translate chinese day6_after_map_e8479859:


    un "嗨..."


translate chinese day6_after_map_0974d8de:


    me "你今天缺勤了。"


translate chinese day6_after_map_abad00d7:


    un "是的...我在四处散步。"


translate chinese day6_after_map_158d2b03:


    "她说的一切都十分平静，不害羞，不尴尬，应该说没有感情。"


translate chinese day6_after_map_9cac764a:


    me "我们很担心你。"


translate chinese day6_after_map_a9e86fe4:


    "虽然唯一一个担心的人是我。"


translate chinese day6_after_map_334f3921:


    un "不必了。"


translate chinese day6_after_map_696f7e51:


    me "但是你不能...你不能就那样消失啊!"


translate chinese day6_after_map_f1268435:


    "我笑了笑，尽量让自己看起来不是在责备。"


translate chinese day6_after_map_608904a9:


    un "我不觉得谁会在意的。"


translate chinese day6_after_map_d1150751:


    me "我在意。"


translate chinese day6_after_map_b672514d:


    un "为什么?"


translate chinese day6_after_map_24fb8611:


    "我从她的眼睛里看到了惊讶。"


translate chinese day6_after_map_e8345567:


    me "因为...因为这样不对!"


translate chinese day6_after_map_d982cf8e:


    "我怎么尝试着变得诚实，都不对。"


translate chinese day6_after_map_4738c0dc:


    un "啊，好吧...我不会这样做了。"


translate chinese day6_after_map_0f363f3f:


    "她似乎对我们的对话不感兴趣。"


translate chinese day6_after_map_ec43a852:


    "接下来是一段沉默。"


translate chinese day6_after_map_615bc5a9:


    "我不知道接下来说什么好，而列娜似乎对这沉默感到很满足。"


translate chinese day6_after_map_6215010e:


    me "玩得怎么样?"


translate chinese day6_after_map_d0fd1810:


    "我终于说出话来，我指着球拍。"


translate chinese day6_after_map_15f9eed8:


    un "不是很..."


translate chinese day6_after_map_6bc45b3c:


    me "如果你想的话，我..."


translate chinese day6_after_map_8fa9fd07:


    un "不，不必了。"


translate chinese day6_after_map_afff794c:


    "她走到长椅旁，放下来球和球拍。"


translate chinese day6_after_map_505d91ae:


    me "好吧..."


translate chinese day6_after_map_7e92ea41:


    "说实话我没有意料到这样的反应。"


translate chinese day6_after_map_3cebee2a:


    un "想要一起看看星星吗?"


translate chinese day6_after_map_3b03af3a:


    me "可以啊，当然..."


translate chinese day6_after_map_76efef38:


    "我坐在她的旁边。"


translate chinese day6_after_map_a39c7cb2:


    "列娜马上开始研究起天空。"


translate chinese day6_after_map_67f740f6:


    "星星点点的亮光出现在我们的头顶，一些很亮，另一些则几乎看不见。"


translate chinese day6_after_map_e0e85a4c:


    "我从来没有理解那些看星星的人看出来什么。"


translate chinese day6_after_map_f99d691b:


    "从地球上看，它们只是小小的两点，大部分人都对那些星体没有什么了解。"


translate chinese day6_after_map_543454aa:


    "不过对于我来说看星星和盯着一堵墙没有什么本质的区别，一颗颗星星就像是墙上的坑坑洼洼。"


translate chinese day6_after_map_e767efc0:


    "微星的夜空。"


translate chinese day6_after_map_6ff78afb:


    me "你看到了什么?"


translate chinese day6_after_map_1663efd6:


    un "星星..."


translate chinese day6_after_map_60e76402:


    "她抬着头神秘的说着。"


translate chinese day6_after_map_cdb6a705:


    me "我也看到了。{w}可是它们有什么特殊的吗?"


translate chinese day6_after_map_47fad4d7:


    un "我不知道...{w}它们好像在和我说话，那里还有其他的人们住着，他们有自己的生活，也许比我们的还要好，他们抬头看着夜空，也能看到你和我。"


translate chinese day6_after_map_b51220d8:


    me "遥远的星星的声音..."


translate chinese day6_after_map_73da0b9c:


    th "我已经从什么地方听到了。"


translate chinese day6_after_map_548f7ac2:


    un "可以那么说。"


translate chinese day6_after_map_092f8254:


    me "很有趣的理论。"


translate chinese day6_after_map_1b701aca:


    un "不，不，这种理论很平常的。"


translate chinese day6_after_map_8603b4ab:


    "列娜看着我。"


translate chinese day6_after_map_33f7f134:


    "在昏暗的月光下，我看到一滴眼泪沿着她的脸颊流下来。{w}或者至少我是这么觉得..."


translate chinese day6_after_map_92d22845:


    me "我觉得我是一个更现实的人。"


translate chinese day6_after_map_15c1d9fb:


    "她什么也没有说，只是继续看着天空。"


translate chinese day6_after_map_a6f3957e:


    me "不过如果仔细想想的话，确实，满天繁星，无限的宇宙，无数的星系，真的很迷人!"


translate chinese day6_after_map_94f87d77:


    "我的声音兴奋的有点不自然。"


translate chinese day6_after_map_51cb2968:


    un "你不必说这些。"


translate chinese day6_after_map_cb23542f:


    me "说什么?"


translate chinese day6_after_map_0ed52f26:


    un "所有的...这一切..."


translate chinese day6_after_map_23571ed6:


    me "不，这些星星，真的很漂亮..."


translate chinese day6_after_map_61cd4196:


    un "你为什么来这里?"


translate chinese day6_after_map_1ed10390:


    "现在她肯定实在哭了"


translate chinese day6_after_map_4cd41dd4:


    me "我...我在找你。"


translate chinese day6_after_map_41cfffd5:


    un "为什么?"


translate chinese day6_after_map_9432886b:


    me "我也不知道为什么！我就是来了!"


translate chinese day6_after_map_4cd400a2:


    un "你找到我了，高兴了吗?"


translate chinese day6_after_map_f883183e:


    me "那个..."


translate chinese day6_after_map_5eca3ce7:


    "我自言自语。"


translate chinese day6_after_map_90b68121:


    un "你为什么不去找阿丽夏?"


translate chinese day6_after_map_9f078a0b:


    me "这和她有什么关系?"


translate chinese day6_after_map_53406a5d:


    un "你是说这和她没有关系吗?"


translate chinese day6_after_map_214e0fd5:


    me "是的，我就是这个意思！你为什么又提起这个了，我以为咱们都说清了呢？"


translate chinese day6_after_map_d2905358:


    "当然我们没有说清，准确的说，我们甚至都没有开始。"


translate chinese day6_after_map_3fd16509:


    "但是我还是不明白这一切的必要性。"


translate chinese day6_after_map_a5ef42ee:


    un "你觉得我自己想说吗?"


translate chinese day6_after_map_3ae4ee1d:


    me "不想说，是为什么呢?"


translate chinese day6_after_map_f20d2d44:


    un "因为...{w} 因为...{w}因为你!"


translate chinese day6_after_map_fd549485:


    me "因为我什么?"


translate chinese day6_after_map_e358f2de:


    un "你和阿丽夏..."


translate chinese day6_after_map_4fa9efef:


    me "我和阿丽夏什么?{w}我们没有关系…我们之间什么都没有!"


translate chinese day6_after_map_bdc08ff0:


    un "别撒谎了..."


translate chinese day6_after_map_50c351a6:


    "列娜转了过去，悄悄的哭着。"


translate chinese day6_after_map_73766a33:


    th "为什么她这么重视?"


translate chinese day6_after_map_dcecdc26:


    me "你不相信我的话，我还能说什么?"


translate chinese day6_after_map_6706f668:


    un "告诉我事实!"


translate chinese day6_after_map_61fa1f64:


    me "我已经告诉你了!"


translate chinese day6_after_map_aa3b5bb0:


    un "那就去找她吧!"


translate chinese day6_after_map_0aa84a7b:


    me "我为什么要去找她？我哪里也不想去!"


translate chinese day6_after_map_5041432a:


    un "你为什么要坐在这里折磨我?"


translate chinese day6_after_map_80e9ae79:


    me "天啊，我哪里折磨你了?"


translate chinese day6_after_map_cf8bd28b:


    "列娜没有回答。"


translate chinese day6_after_map_f59f13df:


    me "我找了你一整天了，因为我担心你！我来这里是因为你在这里，我坐在这里是因为我想要在这里！你为什么不明白？"


translate chinese day6_after_map_70497417:


    "她停止了哭泣。"


translate chinese day6_after_map_dd41c6da:


    un "是真的吗?"


translate chinese day6_after_map_771aa502:


    me "当然!"


translate chinese day6_after_map_2630532a:


    un "我对你来说意味着什么吗?"


translate chinese day6_after_map_97962bdb:


    "经历过之前那么多的对话，这个问题还是把我问住了。"


translate chinese day6_after_map_ea6d6d42:


    me "那个..{w}是的..."


translate chinese day6_after_map_dba41e75:


    un "你应该听听自己的说的什么!"


translate chinese day6_after_map_2844b0b9:


    "她一边哭着，一边生气的离开运动场。"


translate chinese day6_after_map_9e8ba2ed:


    me "等等!"


translate chinese day6_after_map_ea828f9c:


    "我追上她，拉住她的手，但是她挣脱开了。"


translate chinese day6_after_map_6afc93f8:


    un "别碰我!"


translate chinese day6_after_map_5ad79041:


    "现在列娜变成了一个完全不同的人."


translate chinese day6_after_map_c1e1344b:


    "我不能说她是有进攻性，还是意志坚定。{w}她对自己做的事完全没有疑义。"


translate chinese day6_after_map_42af3d5b:


    me "等一下！我说什么了？那都是真的!"


translate chinese day6_after_map_96759709:


    un "把你这些废话都留给她吧!"


translate chinese day6_after_map_78f10d19:


    "我又追了上去，不过她恶狠狠的看着我，让我没敢反驳。"


translate chinese day6_after_map_26abaae4:


    me "你想想我是什么感觉！你做了好多傻事，背负罪恶的是我!"


translate chinese day6_after_map_3abf163e:


    un "你为什么要在乎?"


translate chinese day6_after_map_18650fcd:


    me "我应该!"


translate chinese day6_after_map_c2fbebf3:


    "她停了下来，看着我。"


translate chinese day6_after_map_3b50af60:


    un "这都是谎言!"


translate chinese day6_after_map_861f4d27:


    "我愤怒的用一根铁棒砸着脑袋。"


translate chinese day6_after_map_999750cd:


    un "你就打死自己吧!"


translate chinese day6_after_map_8da3ba3c:


    "我见过列娜的很多面，但是没有这么残酷无情的。"


translate chinese day6_after_map_1db6eb05:


    me "等一下，咱们冷静的谈一下!"


translate chinese day6_after_map_019acfa8:


    un "我们没有任何可谈的!"


translate chinese day6_after_map_b44c2c2d:


    "她穿过足球场地，我徒劳的跟着她。"


translate chinese day6_after_map_e6426ab5:


    th "我不知道自己为什么要这么做？证明自己是对的?"


translate chinese day6_after_map_db9e6092:


    th "我就不会被误解了？我在她的眼中的形象高大了许多?"


translate chinese day6_after_map_666d032c:


    th "还是什么其他的原因?"


translate chinese day6_after_map_2fd24a5d:


    "不管是什么，在那个时候我只是觉得这样很必要。"


translate chinese day6_after_map_b3e9120f:


    "我们来到了广场。"


translate chinese day6_after_map_197a0325:


    "列娜走得很快，我跟着她很困难。"


translate chinese day6_after_map_3cdd393e:


    "我得说些什么，我的头脑中有各种想法，但是没有一个合适的。"


translate chinese day6_after_map_9e8ba2ed_1:


    me "等等!"


translate chinese day6_after_map_ba080751:


    "没有回答。"


translate chinese day6_after_map_70cd7a9e:


    me "听着..."


translate chinese day6_after_map_ba080751_1:


    "没有回答。"


translate chinese day6_after_map_d175e20e:


    me "你能不能停下!?"


translate chinese day6_after_map_ba080751_2:


    "没有回答。"


translate chinese day6_after_map_a3922398:


    me "咱们回惊醒整个营地的!"


translate chinese day6_after_map_b23d87cc:


    un "怎么了？有什么用？"


translate chinese day6_after_map_e688e51e:


    "奇怪，这个时候我更担心我们的行为而不是列娜。"


translate chinese day6_after_map_54f55e48:


    "她停了下来。"


translate chinese day6_after_map_673b1993:


    un "感谢你陪着我，现在我要一个人走了。"


translate chinese day6_after_map_53d3d956:


    "列娜讽刺的说着。"


translate chinese day6_after_map_9a44e183:


    me "你还没有听我说!"


translate chinese day6_after_map_3a8fc4c1:


    un "对我来说，我已经听过了，听过好多次了!"


translate chinese day6_after_map_14f0e6dc:


    me "所以你到底想要什么?"


translate chinese day6_after_map_fd132569:


    un "我？从你这里？什么也不想要!"


translate chinese day6_after_map_a1a64448:


    "她听起来更加自信了，她的眼中没有疑问。"


translate chinese day6_after_map_2f879a85:


    me "我能说什么，能做什么...?{w} 见鬼!"


translate chinese day6_after_map_ed3bf88e:


    "我都要哭出来了。"


translate chinese day6_after_map_4b3132f4:


    un "别这么敏感，什么都没有发生。"


translate chinese day6_after_map_f6b5a1cc:


    me "什么都没有发生？！肯定发生了什么!"


translate chinese day6_after_map_488ea261:


    un "你不注意我也不会有任何的损失。"


translate chinese day6_after_map_6750969b:


    me "让我决定!"


translate chinese day6_after_map_de950caa:


    un "不，说真的，为什么?{w}你有很多事情可以做，说真的，我不需要。"


translate chinese day6_after_map_9f0f0f1e:


    me "如果你不需要，为什么还是总提到阿丽夏?"


translate chinese day6_after_map_d67764cb:


    "列娜的表情变了。"


translate chinese day6_after_map_11f8377a:


    un "这和你没有关系！你明白没有!?"


translate chinese day6_after_map_5fab1698:


    me "没有，现在这就是我的事情!"


translate chinese day6_after_map_52a66644:


    "她看起来想要杀人一样。"


translate chinese day6_after_map_6a9541dc:


    un "你为什么这么烦我？你有她了？就去找她吧！她不会拒绝的，我跟你保证。"


translate chinese day6_after_map_17a396ba:


    "列娜疯狂的喊着，她的头发散乱了，她的脸变得通红，她的眼睛充满了血丝。"


translate chinese day6_after_map_d2b2d0c2:


    me "冷静！我为什么要去找她？！你为什么那么想？我现在就在这里，哪里也不去！"


translate chinese day6_after_map_87b3deb0:


    "她看起来稍微冷静了一些。"


translate chinese day6_after_map_3a8b8317:


    dv "这里怎么回事?"


translate chinese day6_after_map_166adabe:


    "我转过身，看见阿丽夏懒洋洋的嚼着一块面包。"


translate chinese day6_after_map_9bba946f:


    th "错误的时间，错误的地点，简直是教科书一般。"


translate chinese day6_after_map_10e07777:


    "我吃惊的愣住了，什么也说不出来。"


translate chinese day6_after_map_41dceae9:


    "不过列娜更聪明。"


translate chinese day6_after_map_f335a36d:


    un "传球，接着!"


translate chinese day6_after_map_e2e97404:


    "阿丽夏看起来很惊讶，显然她没有听到全部的对话。"


translate chinese day6_after_map_7cac8762:


    dv "什么?"


translate chinese day6_after_map_cfaec870:


    un "我说接着。"


translate chinese day6_after_map_32fc49d5:


    "没过一会儿，列娜又变成了平常的自己，冷静，沉着。"


translate chinese day6_after_map_20cf849d:


    dv "我要接着什么?"


translate chinese day6_after_map_0216fe85:


    un "他!"


translate chinese day6_after_map_3f535c7c:


    "列娜一脸厌恶的指着我。"


translate chinese day6_after_map_21610fc5:


    un "他一直抱怨自己多么的爱你，没有你就活不下去！"


translate chinese day6_after_map_e0ebb201:


    dv "啥?"


translate chinese day6_after_map_61fafefa:


    "阿丽夏瞪大了眼睛。"


translate chinese day6_after_map_5135656c:


    me "不...列娜只是在开玩笑。"


translate chinese day6_after_map_d9758ca6:


    "我苦笑着。"


translate chinese day6_after_map_9849559e:


    un "为什么？这就是事实。"


translate chinese day6_after_map_6a9072d6:


    me "听着，什么事情都有度。"


translate chinese day6_after_map_f25c750f:


    "我悄悄的说道。"


translate chinese day6_after_map_bd03c02c:


    me "你想要有受害者吗?"


translate chinese day6_after_map_a94aa304:


    un "怎么了？我早就告诉你去找她。"


translate chinese day6_after_map_0ece655b:


    dv "我不知道你们在说什么，不过不要带上我!"


translate chinese day6_after_map_c981c313:


    un "你不知道?"


translate chinese day6_after_map_51cf0a90:


    "列娜平静的说着，不过可以听出她的怒火。"


translate chinese day6_after_map_73a43983:


    un "又是这样?!"


translate chinese day6_after_map_16082370:


    "阿丽夏害怕的看着她。"


translate chinese day6_after_map_e75b572c:


    dv "听着，我懂，那个...我真的，一点也没有想，和他一点关系也没有!"


translate chinese day6_after_map_efca6f44:


    un "你不知道?!"


translate chinese day6_after_map_75b8e738:


    "列娜跳了过去，我还没有反应过来，她已经一记重拳打在阿丽夏的脸上。"


translate chinese day6_after_map_c097944c:


    "如果只是打耳光我还可以理解，可是这样的拳头可是会把人的下巴打碎啊!"


translate chinese day6_after_map_1bd51a91:


    "阿丽夏摔倒了，好像失去了意识。"


translate chinese day6_after_map_91c4ce70:


    th "我应该怎么办?"


translate chinese day6_after_map_660ef966:


    "我决定隔岸观火。"


translate chinese day6_after_map_7596bb80:


    th "可是我怎么可能冷漠的看着?"


translate chinese day6_after_map_41d6bd2d:


    th "我是一个真实的人吗?!"


translate chinese day6_after_map_1b093c3a:


    me "你在做什么?!"


translate chinese day6_after_map_dc606069:


    "我跑向摔倒的孩子，查看她是不是还活着。"


translate chinese day6_after_map_5e987c49:


    me "你疯了吗？如果你想要让我离开，让我闭嘴，那好！但是这里已经快要变成一个疯人院了，你是不是得穿一件紧身衣才能防止其他人受伤?!"


translate chinese day6_after_map_0710111a:


    "列娜站了起来，握紧了拳头。"


translate chinese day6_after_map_246bc5b9:


    "这样的一击甚至可以把手砸坏。"


translate chinese day6_after_map_8bf70748:


    un "你... 你...{w} 你什么都不懂!"


translate chinese day6_after_map_8373e0fe:


    "她的眼泪在不停的打转，然后从脸颊上滑落，她飞跑着逃离了广场。"


translate chinese day6_after_map_3ccb1990:


    "在我带阿丽夏离开的时候好像还能听见列娜的哭声。"


translate chinese day6_after_map_b0111b39:


    "最终她终于恢复了意识。"


translate chinese day6_after_map_605ea41f:


    me "你还活着吗？一切都还好吗？"


translate chinese day6_after_map_f537afe9:


    "阿丽夏左右移动着嘴巴。"


translate chinese day6_after_map_898a6e8c:


    dv "我会活着的..."


translate chinese day6_after_map_d84b48c5:


    "我帮她站起来。"


translate chinese day6_after_map_64e51ae5:


    dv "我跟你说过她了..."


translate chinese day6_after_map_6146f956:


    me "现在不要管那个了！你得赶快去医务室!"


translate chinese day6_after_map_93df0dc6:


    dv "医务室要关门了，我现在得去睡一觉，明天早晨再去。"


translate chinese day6_after_map_3e4756aa:


    me "好吧，我跟你一起去。"


translate chinese day6_after_map_2807c5a8:


    dv "我无法拒绝。"


translate chinese day6_after_map_853a9bed:


    "阿丽夏说着，一边尝试着对我笑笑。"


translate chinese day6_after_map_e82487ee:


    "我们一路上都沉默着。"


translate chinese day6_after_map_705d46ac:


    "她可能说不了话，或者不知道说什么好，这一切对我都是一个巨大的打击。"


translate chinese day6_after_map_08c54854:


    "我发现在一出我是主角的戏剧里，我只能作为一名观众出现..."


translate chinese day6_after_map_f6503a89:


    "阿丽夏关上门之后我又盯着她的房间看了好长时间。"


translate chinese day6_after_map_7775e100:


    th "经历了这一切之后，我到底应该怎么做?"


translate chinese day6_after_map_d541a4e0:


    "疲倦袭来，我得去睡觉了。"


translate chinese day6_after_map_f4e9f6bc:


    th "我觉得以列娜现在的状态没有办法和她讲道理。"


translate chinese day6_after_map_0b692d89:


    "我完全累坏了，瘫倒在床上睡了过去。"


translate chinese day6_us_932d85c4:


    "我的早晨可不怎么样，说实话，醒来以后我感觉很糟糕。"


translate chinese day6_us_44869d66:


    "房间里洒满了阳光，窗前有鸟儿欢快的叫着。"


translate chinese day6_us_3d1fd928:


    "我很不情愿的坐起来伸个懒腰。"


translate chinese day6_us_26166937:


    "我开始慢慢回忆起前一天的事情。"


translate chinese day6_us_ba99f4d8:


    "游览活动，愚蠢的扮鬼游戏，乌里扬卡睡着还有..."


translate chinese day6_us_8eb6fcb7:


    "... 一个吻!"


translate chinese day6_us_9ab1bc4f:


    "我马上脸红了起来。{w}这实在是令人吃惊，我呆住了一段时间。"


translate chinese day6_us_89d1c6c1:


    th "主要的问题是现在我应该怎样对待她?{w} 第一眼看上去什么奇怪的事情都没有发生，但是..."


translate chinese day6_us_b9f2d616:


    th "再说，这个夏令营里她最不像是能干出这种事的孩子。"


translate chinese day6_us_7b429d44:


    "对我来说她最多是一个小妹妹。"


translate chinese day6_us_d11fbcdb:


    "但是对她来说，我可能不只是一个大哥哥..."


translate chinese day6_us_b3cdc0ae:


    "虽然说，也许这些都是我自己臆想出来的，可能这个吻里并没有包含着什么特殊的意味?"


translate chinese day6_us_22c8c3a5:


    th "乌里扬卡绝对做的出来这种出格的举动。{w} 还能很容易的把简单的事情变得复杂。"


translate chinese day6_us_bd31a746:


    "最终我决定知道我面对面的见到她以前暂时先不要想这个问题了。"


translate chinese day6_us_b202d75c:


    "我看看表，已经十一点了。{w}也就是说奥尔加·德米特里耶夫娜没有叫我起来吃早饭。"


translate chinese day6_us_4dc8c2d9:


    "不过我没有什么可担心的，距离午饭还有好长时间。"


translate chinese day6_us_aca3d21b:


    "我拿起自己的小包包，前去洗漱。"


translate chinese day6_us_3701bba9:


    "我在半路上没有见到任何人。"


translate chinese day6_us_8ddbdf53:


    th "可能少先队员们都忙着自己的事情呢吧。"


translate chinese day6_us_a5a2f280:


    "水池这里也没有人烟。"


translate chinese day6_us_9c48b593:


    "只过了五分钟，我就再一次站在辅导员的门前，思考着接下来应该做什么。"


translate chinese day6_us_a5889f74:


    "现在并没有什么值得做的，我可以就这样子待到午饭时间，不过一个熟悉的声音把我从神游中叫了回来。"


translate chinese day6_us_9104ac12:


    sl "嗨!"


translate chinese day6_us_d058f15f:


    "我抬起头，看到斯拉维娅在向我甜甜的笑着。"


translate chinese day6_us_a87c71d7:


    me "你好。"


translate chinese day6_us_452193c9:


    sl "你又睡过了吗?"


translate chinese day6_us_6581dd6b:


    me "似乎是这样的..."


translate chinese day6_us_269ed985:


    sl "那可不好呀..."


translate chinese day6_us_472ae0f8:


    me "也许是吧... 你这是准备去哪里?"


translate chinese day6_us_dd46fbd4:


    "实际上我对斯拉维娅今天的日程安排不怎么感兴趣，不过我还是想要继续我们的对话。"


translate chinese day6_us_619b2083:


    sl "那个，我有些事要做。"


translate chinese day6_us_f2e007f0:


    me "我明白了..."


translate chinese day6_us_54d766e4:


    "之后我们都没有说话，她准备离开，这时我突然问她:"


translate chinese day6_us_bdfd5ce1:


    me "愿意在这里坐一会儿吗?"


translate chinese day6_us_ba1218a0:


    sl "当然。"


translate chinese day6_us_97a08ca0:


    "斯拉维娅坐在了旁边。"


translate chinese day6_us_d1c22b75:


    sl "有什么事情在困扰着你，不是吗?"


translate chinese day6_us_78b966f1:


    me "没有啊，你为什么这么想?"


translate chinese day6_us_9ac0f8e4:


    sl "从你的脸上就能看得出来。"


translate chinese day6_us_ca162b6d:


    "她温柔的笑着。"


translate chinese day6_us_a6ff12a6:


    me "也不是..."


translate chinese day6_us_f22c4266:


    "虽然说斯拉维娅可能是对的，确实有什么在困扰着我，是的，就是昨天晚上发生的事情。"


translate chinese day6_us_60656899:


    me "那个，你有没有兄弟姐妹?"


translate chinese day6_us_bb4d3f88:


    sl "有。"


translate chinese day6_us_b8863b83:


    me "你和他们相处的怎么样呢?"


translate chinese day6_us_85a61fa8:


    sl "还不错。"


translate chinese day6_us_6d77584d:


    "她爽朗的笑着。"


translate chinese day6_us_9aed200b:


    sl "你呢?"


translate chinese day6_us_5cb822ee:


    me "我没有兄弟姐妹..."


translate chinese day6_us_96096e29:


    "我停顿了一下，然后补充道。"


translate chinese day6_us_cfdfe577:


    sl "那为什么问这个问题?"


translate chinese day6_us_0f0d016f:


    me "不知道，只是..."


translate chinese day6_us_f36a24a2:


    me "你总是能理解他们吗?"


translate chinese day6_us_e7a9578f:


    sl "我会不断地尝试。"


translate chinese day6_us_0f15cfb0:


    me "有时候我实在不明白小孩子们是怎么想的..."


translate chinese day6_us_7519a16d:


    "我突然停了下来。"


translate chinese day6_us_b771fb63:


    th "斯拉维娅不知道我比看上去的还要大几岁，对她来说我还是个孩子。"


translate chinese day6_us_5baee7ee:


    me "那些比我小的..."


translate chinese day6_us_70f37a53:


    sl "为什么?"


translate chinese day6_us_f4a5584e:


    me "那个，我不是总能跟上他们的想法，有时候他们的行为不怎么合逻辑。"


translate chinese day6_us_257da516:


    sl "你不是不久之前还和他们一样大吗?"


translate chinese day6_us_efd76d15:


    "她惊讶的看着我。"


translate chinese day6_us_80ee985d:


    me "这倒是啦..."


translate chinese day6_us_de2e65e6:


    "但是对我来说真的是好长一段时间了，尤其对于乌里扬卡来说，已经是代沟了。"


translate chinese day6_us_b77a46d6:


    sl "简单啊，想想那个时候的自己。"


translate chinese day6_us_774a19b3:


    me "我不太一样..."


translate chinese day6_us_3e1005b1:


    "至少在我看来是这样的。"


translate chinese day6_us_457d02d4:


    sl "如果是这样，那很不错啊!"


translate chinese day6_us_c63c3cdc:


    "斯拉维娅又笑了起来。"


translate chinese day6_us_71b0154f:


    me "你呢？你也做过一些很傻的事情吗？"


translate chinese day6_us_d85b7a18:


    sl "这要看咱们说的是多大年纪了。"


translate chinese day6_us_79e03e6b:


    me "那个，就说，十四岁吧..."


translate chinese day6_us_a25fc2aa:


    sl "嗯，发生了很多不同的事情..."


translate chinese day6_us_5e20fc4d:


    "她若有所思的回答着。"


translate chinese day6_us_0536f6a9:


    me "那很好啦，可是对我的事情没有什么帮助啊..."


translate chinese day6_us_d97a7610:


    sl "也许就应该顺其自然吧?"


translate chinese day6_us_963937db:


    me "也许是这样的..."


translate chinese day6_us_f0a4acf4:


    "斯拉维娅站起来，向我道了别然后离开了，我闭上了眼睛，又进入了梦乡。"


translate chinese day6_us_a20cefa7:


    "..."


translate chinese day6_us_c8a36022:


    "午饭的铃声把我惊醒。"


translate chinese day6_us_33663426:


    "虽然铃音离我有很远的距离，我还是自动的蹦起来，及时赶到了食堂。"


translate chinese day6_us_c789b795:


    "我就是在这里遇到了乌里扬卡。"


translate chinese day6_us_da3a622a:


    us "你好!"


translate chinese day6_us_d2510e4c:


    me "啊... 好..."


translate chinese day6_us_886b57c4:


    us "我已经吃过了!"


translate chinese day6_us_9bc7d8f8:


    "她开心的笑着，然后跑开了。"


translate chinese day6_us_33ab27ac:


    "我没有阻止她，而且我也没有什么话可说。"


translate chinese day6_us_ec659793:


    "我取来自己的午饭，准备朝我喜欢的小角落走的时候，突然有人抓住了我的胳膊。"


translate chinese day6_us_04f58a0e:


    dv "坐在这里!"


translate chinese day6_us_f294c7bb:


    "阿丽夏的声音好像在威胁，同时似乎也充满了请求。"


translate chinese day6_us_29f71b5c:


    me "发生了什么?"


translate chinese day6_us_73ab0e4f:


    dv "我有些事情要和你说。"


translate chinese day6_us_cb7dcf26:


    "她的声音不再那么强硬了。"


translate chinese day6_us_505d91ae:


    me "好吧..."


translate chinese day6_us_12af78a1:


    "我坐在她的旁边。"


translate chinese day6_us_c233b6a2:


    me "说什么呢?"


translate chinese day6_us_c1a63ca7:


    dv "你昨天在不在...{w}当她傻瓜似的披着床单扮鬼的时候?"


translate chinese day6_us_ae83cf7a:


    th "和阿丽夏说谎没有什么意义。"


translate chinese day6_us_627457bb:


    me "是啊，我在那里..."


translate chinese day6_us_3cf2f629:


    dv "这...这..."


translate chinese day6_us_56d4e39c:


    "她笑出了声。"


translate chinese day6_us_bf0fda5f:


    dv "你为什么这么做？"


translate chinese day6_us_d35441c0:


    "阿丽夏突然又变得严肃起来。"


translate chinese day6_us_4954bdac:


    me "你怎么不去问问她?"


translate chinese day6_us_6153932c:


    dv "不，我在问你，你得告诉我!"


translate chinese day6_us_fccc1215:


    me "我不知道!"


translate chinese day6_us_ccbb1994:


    "我带着怒气的说道。"


translate chinese day6_us_bd86e9cc:


    me "你是不是也要告诉我，奥尔加·德米特里耶夫娜怎么没有管?"


translate chinese day6_us_c46902a7:


    dv "她怎么管?"


translate chinese day6_us_2552c9ac:


    me "这你不知道？？..."


translate chinese day6_us_84ea8c71:


    "我怀疑的看着她。"


translate chinese day6_us_a306c7bc:


    dv "那个，她就是不知道那是谁啊。"


translate chinese day6_us_65256f6f:


    "好吧，我需要承认，奥尔加·德米特里耶夫娜的推理水平实在是有待进步。"


translate chinese day6_us_251f6e0f:


    dv "然后...是不是你把她带了回来?"


translate chinese day6_us_311e429f:


    me "不是啊，是莎士比亚的小精灵。"


translate chinese day6_us_12b4f273:


    "我讽刺她。"


translate chinese day6_us_1e5bc39c:


    me "那到底有什么问题啊?"


translate chinese day6_us_b3b67d03:


    dv "没有什么问题..."


translate chinese day6_us_2e06d203:


    "阿丽夏看着自己的盘子，开始认真的吃起来。"


translate chinese day6_us_6cc1bbe6:


    dv "还有...{w}她跑去了哪里，然后心情超好的回来..."


translate chinese day6_us_f95c9fc9:


    me "然后呢?"


translate chinese day6_us_79b4bb58:


    dv "你就没有点线索吗?"


translate chinese day6_us_37ad7929:


    me "就算是我有，又和你有什么关系啊?"


translate chinese day6_us_8fb93608:


    "她瞪了我一下。"


translate chinese day6_us_304d61e3:


    dv "嗯，我很在意。"


translate chinese day6_us_3856e844:


    th "我还真没想到她会关心自己以外的人。"


translate chinese day6_us_df1ea57f:


    me "如果你一定要知道的话，那么没有发生那样的事情..."


translate chinese day6_us_744c373a:


    dv "不，不是关于那个..."


translate chinese day6_us_6d91ce66:


    "阿丽夏看都没看我的回答着。"


translate chinese day6_us_9e028cbf:


    "也许谈到这个话题不太明智。"


translate chinese day6_us_4db446fd:


    dv "对了，“那样”的事，是什么事？"


translate chinese day6_us_3ffb4733:


    me "什么意思?"


translate chinese day6_us_ae504ccb:


    dv "你刚刚说的嘛!"


translate chinese day6_us_357ddbfd:


    me " 我刚刚说了什么?"


translate chinese day6_us_bb9ef712:


    "我试图把她绕迷糊。"


translate chinese day6_us_93884669:


    dv "嘿... 听我说，我盯着你呢！"


translate chinese day6_us_465a1be8:


    me "啊，我好害怕啊!"


translate chinese day6_us_46433dd4:


    "我拿起了盘子，离开了饭桌。"


translate chinese day6_us_868ec380:


    "我对阿丽夏的话一点也不害怕，虽然听起来好像在威胁我。"


translate chinese day6_us_f2a61bc9:


    "事实上她这么关心乌里扬卡让我很感兴趣..."


translate chinese day6_us_3a2303e3:


    "在这么多的少先队员之中，她最不像是一个会关心别人的人了。"


translate chinese day6_us_523908a6:


    "我走出食堂，来到了真正的大熔炉中。{w}太阳严酷的照射着我们，没有人可以在地表停留。"


translate chinese day6_us_5a8af8ba:


    "我得找一个阴凉的地方。"


translate chinese day6_us_8816790a:


    "沙滩看起来是最合适的地方。"


translate chinese day6_us_7ffbd155:


    "奇怪的是，沙滩上没什么人。"


translate chinese day6_us_d78798fb:


    "也许大部分的少先队员们都被炎热的天气吓傻了吧，或者他们吃完饭想要午睡一会儿。"


translate chinese day6_us_539fca33:


    "我没有带泳裤来，所以便坐在一把遮阳伞下面看着游泳的人群。"


translate chinese day6_us_aee79133:


    "附近没有我认识的人，这很好，这样我就可以坐下来冷静的思考。"


translate chinese day6_us_6339194e:


    th "自从我来到这里已经过了五天半，到底有什么改变呢?"


translate chinese day6_us_e1327ac0:


    th "这里发生了很多奇怪的但是又没有什么危害的事情，总的来说，这就是一个少先队员夏令营。"


translate chinese day6_us_becbdeab:


    "这让我更加害怕了。"


translate chinese day6_us_1d93d559:


    th "接下来要发生什么?"


translate chinese day6_us_e2f93f20:


    th "新的一天，新的一周?还会有什么未知的事情？{w}我已经没有家可回了。"


translate chinese day6_us_efe9c612:


    th "也许这个时候我还没有出生，或者更糟糕的情况..."


translate chinese day6_us_07aa477a:


    "我思考了几分钟之后，回到了昨天晚上。"


translate chinese day6_us_b188d2fb:


    "奇怪，之前我还更加担心来着。"


translate chinese day6_us_5818c1ec:


    "一方面我想要把情况弄清楚，另一方面我又不想遇到乌里扬卡，怕没事也说出点儿事来。"


translate chinese day6_us_d6d9a6e5:


    "我闭上眼睛开始休息。"


translate chinese day6_us_a20cefa7_1:


    "..."


translate chinese day6_us_3615e0f8:


    "当我醒来的时候，看到列娜在我的旁边。"


translate chinese day6_us_4be9f1c5:


    me "你在这里做了多长时间了?"


translate chinese day6_us_84bd5d09:


    "我惊讶的问着。"


translate chinese day6_us_267018a5:


    un "不是太长。"


translate chinese day6_us_74e632e7:


    "奇怪的是她没有把我弄醒。"


translate chinese day6_us_ea63ce77:


    me "发生了什么事吗?"


translate chinese day6_us_7d85bdfc:


    "不知道为什么，可是我的第六感告诉我她想要说些什么但是又不太感兴趣。"


translate chinese day6_us_50dd705e:


    un "没有，你呢?"


translate chinese day6_us_8cd2864d:


    me "没有。"


translate chinese day6_us_cc4a1ab6:


    un "你确定吗?"


translate chinese day6_us_771aa502:


    me "当然!"


translate chinese day6_us_2f04a7f9:


    "我笑了。"


translate chinese day6_us_d9b000a0:


    un "那么你想要说什么?"


translate chinese day6_us_0f1517b5:


    me "你怎么觉得我要说些什么?"


translate chinese day6_us_cdbc06cf:


    un "看起来象是这样的。"


translate chinese day6_us_dd0d8a90:


    me "不...其实没有。"


translate chinese day6_us_a5f21b45:


    "一段长长的沉默。"


translate chinese day6_us_1458865a:


    me "你有弟弟妹妹吗?"


translate chinese day6_us_50dd705e_1:


    un "没有，你呢?"


translate chinese day6_us_8cd2864d_1:


    me "没有。"


translate chinese day6_us_9e52f44a:


    un "那为什么这么问呢?"


translate chinese day6_us_d659d744:


    me "有时候我觉得自己完全不了解小孩子们。"


translate chinese day6_us_959d479a:


    un "我也是。"


translate chinese day6_us_f41e692e:


    "列娜一点表情没有。"


translate chinese day6_us_898b12b0:


    un "但是可能就应该是这样子的。"


translate chinese day6_us_2eb02c21:


    me "那是什么意思？我应该怎么对待他们呢？"


translate chinese day6_us_c80e177b:


    un "你觉得怎样合适就怎样做好了。"


translate chinese day6_us_096e3a82:


    me "啊，就是这个问题嘛—我不知道什么是对的，什么是错的。"


translate chinese day6_us_0263b300:


    un "你越是多想，越是后悔。"


translate chinese day6_us_c256f910:


    me "唔，那有道理，可是帮不上忙啊..."


translate chinese day6_us_f82ea479:


    "我有点惊讶，列娜说出这么有深度的话。"


translate chinese day6_us_9437b7c4:


    "她说的话可能是出于礼貌，但是..."


translate chinese day6_us_5e0eb09d:


    me "你是说我应该按照自己的第一感觉去做吗?"


translate chinese day6_us_60bde9d8:


    "我笑了。"


translate chinese day6_us_61e40971:


    un "有这样的说法，你的第一感觉就是对的。"


translate chinese day6_us_8500866b:


    me "你相信吗?"


translate chinese day6_us_cb890890:


    un "我不知道..."


translate chinese day6_us_e6ff543f:


    "我们又坐了一会儿，然后她站了起来。"


translate chinese day6_us_ccdf7460:


    un "嗯，我得走了。"


translate chinese day6_us_ce5c1ffe:


    me "感谢你和我的交谈!"


translate chinese day6_us_584c5532:


    un "别客气!"


translate chinese day6_us_d7a43efd:


    "列娜淡淡地说。"


translate chinese day6_us_7c4da760:


    "我躺下来看着燃烧的太阳。"


translate chinese day6_us_d79d6192:


    th "我的第一感觉，是吗？万一我的第一感觉是回吻她？"


translate chinese day6_us_9d9f68a5:


    th "而且我那个时候的第一感觉是什么啊?"


translate chinese day6_us_e288f243:


    "我努力的思考着，但是没有任何效果，我的大脑中没有出现任何想法。"


translate chinese day6_us_7e684fb8:


    "不过我只能听从列娜的建议，因为这一切都发生的太快了。"


translate chinese day6_us_a20cefa7_2:


    "…"


translate chinese day6_us_28dd5f2a:


    "太阳开始缓缓地降落，我也站起来准备离开，因为我的肚子告诉自己即使是置身于这样精彩的故事之中也是需要正常吃饭的。"


translate chinese day6_us_793d9878:


    "有人在广场叫住了我。"


translate chinese day6_us_b4ae4166:


    "是电子小子，他上气不接下气的跑过来说:"


translate chinese day6_us_45391550:


    el "谢苗..."


translate chinese day6_us_9aa9ea53:


    me "是啊，那应该是我。"


translate chinese day6_us_caf0b8c7:


    el "拿着。"


translate chinese day6_us_5e22b544:


    "他递给我了一个什么钥匙。"


translate chinese day6_us_51c2c17c:


    me "这是什么?"


translate chinese day6_us_24914d26:


    el "这是我们社团的钥匙。"


translate chinese day6_us_9a940e9d:


    me "我要这个干什么?"


translate chinese day6_us_9fd6666c:


    el "奥尔加·德米特里耶夫娜这样交代的...{w}因为我今天不会再回来了。"


translate chinese day6_us_64143d09:


    me "那就给舒里克嘛。"


translate chinese day6_us_23cd6835:


    el "他有自己的事情。"


translate chinese day6_us_483eac77:


    me "那你自己拿着嘛。"


translate chinese day6_us_74e85ae4:


    el "这不行。"


translate chinese day6_us_5ac38cf1:


    me "为什么?"


translate chinese day6_us_7a4a266c:


    el "你拿着就好了。"


translate chinese day6_us_c8e2439f:


    me "我要这个干什么啊？拿来做什么？"


translate chinese day6_us_83054bac:


    el "你之后把它还给我就行了。"


translate chinese day6_us_17717ee8:


    me "我说..."


translate chinese day6_us_34709e3e:


    el "啊，我该走了!"


translate chinese day6_us_a5cf9eb3:


    "他把钥匙塞在我的手里然后就离开了。"


translate chinese day6_us_378693dd:


    th "不管你怎么看，他都是很奇怪..."


translate chinese day6_us_ca9eac02:


    "不过我没有太在意，只是把钥匙放在了口袋里。"


translate chinese day6_us_3f520ea8:


    "我在晚饭之前就来到了食堂。"


translate chinese day6_us_b5e4650e:


    "幸运的是这里已经开门了，不过晚饭还并没有准备好。"


translate chinese day6_us_cc81d257:


    "我在角落里找了一个自己喜欢的位置，然后专注的玩起一根牙签。"


translate chinese day6_us_2a72bf60:


    "很快我感觉自己旁边坐了一个人。"


translate chinese day6_us_94e4a1c1:


    "我抬起眼睛，看到了奥尔加·德米特里耶夫娜。"


translate chinese day6_us_e4be0ed5:


    "我们就这样互相盯着看了一会儿。"


translate chinese day6_us_fdb677a4:


    me "你需要什么吗?"


translate chinese day6_us_4c09a32f:


    "我最后打破了沉默。"


translate chinese day6_us_acc07115:


    mt "这个，谢苗，这些我都懂，但是..."


translate chinese day6_us_ad02f53a:


    me "什么?"


translate chinese day6_us_7c87b14a:


    "我吃惊的问着。"


translate chinese day6_us_70193a9b:


    mt "你以为我不知道你昨天晚上失踪了吗?"


translate chinese day6_us_a96d8927:


    th "'失踪'这个描述太夸张了吧，{w}我觉得自己只是出去转了一会儿。"


translate chinese day6_us_e7288eb6:


    mt "嗯，怎么了?"


translate chinese day6_us_1e5bc39c_1:


    me "有什么问题吗?"


translate chinese day6_us_421df7f4:


    mt "我本来觉得没什么问题，不过还是决定检查一下。"


translate chinese day6_us_8782263a:


    "辅导员开始慢慢皱起了眉头。"


translate chinese day6_us_6100ad4b:


    me "那么你检查了没有?"


translate chinese day6_us_27f521dd:


    "我还是猜不出来她在想什么。"


translate chinese day6_us_6b3e0c5d:


    mt "是的，我检查了！这...对于一名少先队员来说......是不能容忍的！尤其是和她!"


translate chinese day6_us_7e6d6b00:


    "她的脸憋得通红。"


translate chinese day6_us_443c0493:


    mt "我都不想听你假惺惺的理由!"


translate chinese day6_us_fd11f26f:


    "我逐渐对于她说的事情有了一点模糊的概念。"


translate chinese day6_us_185a8790:


    me "所以说，到底发生了什么，你能不能解释一下呢?"


translate chinese day6_us_6ca85047:


    mt "你现在还假装无辜，你！和乌里扬娜!"


translate chinese day6_us_cae6c168:


    "现在这一切都变得十分清晰了。"


translate chinese day6_us_07246779:


    me "我有两件事要告诉你，第一..."


translate chinese day6_us_262d7cae:


    "我来了精神。"


translate chinese day6_us_b3aa00a8:


    me "没有发生那种事情，而且那和我一点关系没有。"


translate chinese day6_us_ca25a37e:


    mt "你觉得..."


translate chinese day6_us_4ff3e72d:


    me "第二..."


translate chinese day6_us_a3058a6d:


    "我粗鲁的打断了她。"


translate chinese day6_us_7de2af8d:


    me "谁跟你说的?"


translate chinese day6_us_da2aca8e:


    "事实上，答案十分明显。"


translate chinese day6_us_231ed06e:


    "乌里扬卡肯定不会那样做，如果她想要恶作剧的话，可能早就做了，而且根据我这几天的相处，我越来越了解她了。"


translate chinese day6_us_83723d62:


    th "这就导致了只有一种可能性..."


translate chinese day6_us_d1eaca67:


    mt "这有什么关系?"


translate chinese day6_us_4d01563d:


    me "不，不！有关系!"


translate chinese day6_us_e5fc00e7:


    mt "你能不能解释一下自己的行为呢?"


translate chinese day6_us_703eeb00:


    "奥尔加·德米特里耶夫娜在我的攻势下似乎后退了一些。"


translate chinese day6_us_3d718732:


    me "没有什么可解释的!"


translate chinese day6_us_9829aaef:


    "我迅速站起来，前往食堂的出口。"


translate chinese day6_us_19709348:


    mt "谢苗，等一下..."


translate chinese day6_us_653b8455:


    "我听到了辅导员在后面无力的语无伦次的辩解。"


translate chinese day6_us_2080cf3d:


    "如果你面对她的时候够果断，她反而会软下来。"


translate chinese day6_us_4d5ffb3d:


    "所以现在我得把事情和朵切芙斯卡娅说清楚。"


translate chinese day6_us_50e7f679:


    "如果她的恶作剧没有直接的关系到我的利益，我还可以忍受。"


translate chinese day6_us_abec8829:


    "仔细想想的话，我甚至还开始不再把她当成一个坏蛋。"


translate chinese day6_us_412c8322:


    "我在广场遇到了斯拉维娅。"


translate chinese day6_us_c8aa9582:


    me "你知道阿丽夏在哪里吗?"


translate chinese day6_us_b3c273bf:


    sl "是啊，有什么事吗?"


translate chinese day6_us_18c5db17:


    me "告诉我!"


translate chinese day6_us_5ac2d11c:


    "我的声音显然有点高了，不过似乎她没有在意。"


translate chinese day6_us_26d880cd:


    sl "她在音乐社团。"


translate chinese day6_us_95bfa391:


    "斯拉维娅说得对，我在音乐社团看到了她，不过她和未来在一起。"


translate chinese day6_us_8a7949da:


    me "你是不是要解释一下?"


translate chinese day6_us_ba464770:


    "我没有讲客套话。"


translate chinese day6_us_a36d4b17:


    dv "没有，你怎么这么想?"


translate chinese day6_us_01ffd8fc:


    "阿丽夏可爱的笑着。"


translate chinese day6_us_c01cc6ab:


    mi "啊，谢苗，你好! 真高兴你也在这里! 也许咱们三个可以玩点什么? 我有一首新歌，它非常有意思! 你一定会喜欢的，我保证!"


translate chinese day6_us_90c9fd46:


    mi "咱们也可以找一些老一点的..."


translate chinese day6_us_6dae2579:


    "未来打断了谈话。"


translate chinese day6_us_0259c598:


    me "你能不能..."


translate chinese day6_us_57d2c9aa:


    mi "什么?"


translate chinese day6_us_90f2e3fb:


    "她看着我的眼神好像一个什么都不明白的小孩子，我现在明白她的脑袋里都是什么了——小兔子，小熊， 猫咪，但是完全没有智慧。"


translate chinese day6_us_b05f8c8c:


    me "我得和阿丽夏谈谈。"


translate chinese day6_us_7cbb1d69:


    "我用不容置疑的声音说道。"


translate chinese day6_us_94b93a6a:


    mi "好吧，等一会儿..."


translate chinese day6_us_980963c1:


    "她看起来很失望..."


translate chinese day6_us_6cdf2111:


    th "但是我不在乎!"


translate chinese day6_us_1cb52a30:


    me "所以..."


translate chinese day6_us_0b01579b:


    dv "我不知道你什么意思。"


translate chinese day6_us_c11d0305:


    "阿丽夏无礼的说道。"


translate chinese day6_us_f16ef830:


    dv "总之我得走了!"


translate chinese day6_us_17bb7bcb:


    "她转过去准备离开。"


translate chinese day6_us_75807571:


    "我粗鲁的抓住了她的手腕。"


translate chinese day6_us_ba86d4d5:


    dv "你干什么?!"


translate chinese day6_us_e5fbd716:


    "眼前的女孩子有点害怕。"


translate chinese day6_us_27478259:


    me "别这么着急!"


translate chinese day6_us_97c863d3:


    "她停下来转向我。"


translate chinese day6_us_c644793a:


    me "我不是来杀你的..."


translate chinese day6_us_30c1f806:


    "我的话听起来像是开玩笑，不过阿丽夏好像吓到了。"


translate chinese day6_us_ec416668:


    me "谁告诉辅导员乌里扬卡晚上来找我的?"


translate chinese day6_us_3e69c35f:


    dv "我不知道。"


translate chinese day6_us_e657f196:


    "她很可怜的说道。"


translate chinese day6_us_c3effd86:


    me "咱们早晨说过，不是吗?"


translate chinese day6_us_0d3c2df7:


    "她什么也没有说，但是还是充满恐惧的看着我。"


translate chinese day6_us_45e343a5:


    me "更重要的是，你讲这些谎言的意义何在?"


translate chinese day6_us_ad8d4b66:


    dv "我真的不知道你在说什么!"


translate chinese day6_us_5dbc6805:


    "她的眼睛里充满了泪水。"


translate chinese day6_us_7e881d63:


    "突然我觉得我好像被闪电击中。"


translate chinese day6_us_97da91c1:


    th "万一她是清白的呢？为什么我马上想到她？{w}其他人也很有可能在晚上看到我们。"


translate chinese day6_us_8cce8ea8:


    th "这我完全说不清楚，乌里扬卡自己都有可能说出去。"


translate chinese day6_us_b77a7f8b:


    me "你确定?"


translate chinese day6_us_09073ec6:


    "我严厉的问着。"


translate chinese day6_us_3a950d86:


    dv "是的。"


translate chinese day6_us_cea34798:


    "我思考了一会儿，这段时间够阿丽夏逃走了。"


translate chinese day6_us_b2d3e6d4:


    th "现在唯一的解决办法是和奥尔加·德米特里耶夫娜谈谈。"


translate chinese day6_us_7149be6f:


    "我在食堂的门廊下找到了她。"


translate chinese day6_us_37e4ac0e:


    "这个时候太阳几乎已经消失在了地平线以下。"


translate chinese day6_us_636e78e7:


    mt "你终于还是回来了..."


translate chinese day6_us_8c1c36bb:


    "她说道。"


translate chinese day6_us_1a590f52:


    me "奥尔加·德米特里耶夫娜，你能不能诚实的告诉我，是谁跟你说的?"


translate chinese day6_us_61a9bb1f:


    me "我刚刚和朵切芙斯卡娅说过，不是她，乌里扬卡不可能自己去做，那么到底是谁呢?"


translate chinese day6_us_373aadf9:


    "说实话我自己也不知道到底为什么要了解这个问题。"


translate chinese day6_us_7e4fe70a:


    th "我本来对阿丽夏发火，但是..."


translate chinese day6_us_d1318244:


    mt "这个嘛..."


translate chinese day6_us_6e6e30b3:


    "她犹豫了。"


translate chinese day6_us_eeb01990:


    mt "奇怪的女孩...{w} 但是这也..."


translate chinese day6_us_09fba842:


    me "奇怪的女孩?"


translate chinese day6_us_7d0924ac:


    mt "是啊...看起来不是我们夏令营的，似乎。"


translate chinese day6_us_80770549:


    me "她为什么跟你说这个?"


translate chinese day6_us_336d97d4:


    mt "这我怎么知道?!"


translate chinese day6_us_e6e7decd:


    me "奥尔加·德米特里耶夫娜，这..."


translate chinese day6_us_5c589d2a:


    "我深吸了一口气，离开了。"


translate chinese day6_us_ed919e5f:


    th "她为什么要撒这么明显的谎?"


translate chinese day6_us_483b4ef7:


    th "还是说她在保护谁，那个人是...糟糕我想不出来！"


translate chinese day6_us_4366e696:


    me "好吧..."


translate chinese day6_us_76102a17:


    "我低声说道，然后快速离开了食堂。"


translate chinese day6_us_25f025cf:


    "我完全不想再继续听奥尔加·德米特里耶夫娜的下一条谎言。"


translate chinese day6_us_e2e7f765:


    th "我们所谓的“辅导员”..."


translate chinese day6_us_b46ab80b:


    "我漫无目的的走着，沉浸在自己的想法里。"


translate chinese day6_us_390dbdba:


    us "你是在巡逻吗?"


translate chinese day6_us_16e9259d:


    "我听到身后充满活力的声音。"


translate chinese day6_us_fb91294d:


    "是乌里扬娜。"


translate chinese day6_us_31ff7fd5:


    me "没什么特别的，只是..."


translate chinese day6_us_c6821b44:


    "看来我找不出告密者了，而且昨天的事情也被我压下去了，所以我决定不再想这件事。"


translate chinese day6_us_09d23ad2:


    us "你晚上有什么计划吗?"


translate chinese day6_us_5203c9d1:


    me "没有吧..."


translate chinese day6_us_29fcfde5:


    "经过我刚刚手忙脚乱的调查，我现在面对阿丽夏和乌里扬卡的时候总感觉有一点愧疚。"


translate chinese day6_us_8eec6359:


    "而且面对乌里扬卡的愧疚感完全不知道是为了什么。"


translate chinese day6_us_876ae211:


    us "那么我有一个建议。"


translate chinese day6_us_094d6b91:


    me "什么?"


translate chinese day6_us_288bb33e:


    us "我从家里带来了一盘录像带..."


translate chinese day6_us_6d3b56e5:


    "她的眼睛狡黠的闪动着。"


translate chinese day6_us_f95c9fc9_1:


    me "然后?"


translate chinese day6_us_2b9ac9b1:


    th "我都想不起来自己上一次见到录像带是什么时候了。"


translate chinese day6_us_b54f9cfb:


    us "里面有一部很有趣的电影!"


translate chinese day6_us_33f86965:


    me "附近哪里有VCR?"


translate chinese day6_us_9b9a6c28:


    "我想起来了回忆中的这个词语，想要顺便测试一下我对于自己被放在什么时间的猜测是不是准确。"


translate chinese day6_us_635b72e6:


    us "当然是在机器人社团!"


translate chinese day6_us_4ce76f25:


    me "那里什么也没有。"


translate chinese day6_us_449236f5:


    "我充满自信的说着。"


translate chinese day6_us_6fdda8ad:


    us "后面的房间呢?"


translate chinese day6_us_fc831b50:


    "她说得对，有可能在那里。"


translate chinese day6_us_2d3bdcfb:


    me "有可能，不过现在太晚了，那里都关门了。"


translate chinese day6_us_ba96048e:


    "我面对乌里扬卡时的愧疚感让我没有直接拒绝她。"


translate chinese day6_us_d82bc37c:


    us "咱们可以翻窗户!"


translate chinese day6_us_17cfa934:


    "她淘气的笑着。"


translate chinese day6_us_75cef782:


    me "这个...你要知道..."


translate chinese day6_us_6533f6ed:


    us "也许你是对的，咱们要是有钥匙就好了..."


translate chinese day6_us_2b00c58d:


    "乌里扬娜陷入了沉思。"


translate chinese day6_us_71a7833d:


    th "但是我有!{w} 在我的口袋里。"


translate chinese day6_us_433d2c22:


    "我刚刚想了起来。"


translate chinese day6_us_4e580acc:


    me "不过既然你提到了！我有钥匙!"


translate chinese day6_us_001cfbf1:


    "下次我最好过过脑子再说话。"


translate chinese day6_us_24a62d16:


    us "很好！我回去拿录像带来，你在这里等着。"


translate chinese day6_us_8b7b1283:


    "我还没来得及张嘴，她就消失了。"


translate chinese day6_us_983ee57d:


    "我竟然巧妙的在合适的时间地点拿到了钥匙，乌里扬卡竟然一点也不奇怪。"


translate chinese day6_us_b974419e:


    "不管怎么说，我得决定自己下一步怎么做。"


translate chinese day6_us_d21cc0d2:


    "最正确的选择就是不跟她去任何地方，但是我内心的愧疚感..."


translate chinese day6_us_dddd87f0:


    th "虽然看起来和她一起看一部电影也没有什么问题。"


translate chinese day6_us_1b5d4998:


    th "不过反过来说，即使是乌里扬卡做出的最无害的事情，最后也可能变得乱成一团，鸡飞狗跳。"


translate chinese day6_us_f97a40c8:


    "我已经开始忘记今天晚上发生了什么了，自己试图发现真相的愚蠢尝试..."


translate chinese day6_us_a20cefa7_3:


    "…"


translate chinese day6_us_1dc2a2a7:


    "没过几分钟，她就回来了。"


translate chinese day6_us_bcf9c5aa:


    us "有别的想法?"


translate chinese day6_us_df7954e6:


    "我看着乌里扬娜，感觉有点不舒服。"


translate chinese day6_us_1c3f5164:


    us "咱们可以走了吗?"


translate chinese day6_us_a4c1c729:


    me "那个，听着...我觉得你的注意...不是那么的...安全。"


translate chinese day6_us_e1f1e8c6:


    me "最后又会悲剧的。"


translate chinese day6_us_5d3c20b3:


    us "你害怕了?"


translate chinese day6_us_bcadcefa:


    me "没有。"


translate chinese day6_us_7714aa0d:


    me "我是一个成年人，我对这种游戏不感兴趣。"


translate chinese day6_us_35cd0fa9:


    us "成年人?"


translate chinese day6_us_ec2c2a7c:


    "她笑了起来。"


translate chinese day6_us_359cc1c1:


    th "确实，我忘了自己看起来顶多十七岁。"


translate chinese day6_us_9a385a27:


    us "你怎么会这么想?"


translate chinese day6_us_6f30efaf:


    "想要承认很困难，但是我无话可说。"


translate chinese day6_us_0c18ac79:


    "想想也是，我到这里这么长时间，没有体现出自己作为成年人的一点长处，经验还是什么冷静的分析能力。"


translate chinese day6_us_98eb7069:


    "虽然另一个问题是，我在自己的生活中，到底有没有过那些长处?"


translate chinese day6_us_242a5814:


    th "我也觉得自己可以想到一些，可是..."


translate chinese day6_us_7a9e2886:


    me "好吧，走吧。"


translate chinese day6_us_c52254db:


    "我对乌里扬卡的话很不爽:"


translate chinese day6_us_537e718c:


    me "在你看来，一个成年人到底是怎么样的?"


translate chinese day6_us_1998fdba:


    us "会承担责任，会考虑后果，不仅会照顾自己，也会照顾他人。"


translate chinese day6_us_27bca7e1:


    me "这些话描述你肯定不合适。"


translate chinese day6_us_bb9ef712_1:


    "我尝试着笑话她。"


translate chinese day6_us_c23eb093:


    us "反正我也没有装成那样子。"


translate chinese day6_us_ddb5934e:


    "乌里扬娜笑着。"


translate chinese day6_us_7efaef44:


    "是啊，她说的对，她所说的那几点我没有一个能做到。"


translate chinese day6_us_31f38e05:


    "我一直觉得过了17岁就是大人了。"


translate chinese day6_us_35c738f1:


    "结果我和其他人没有任何的区别，只要我想，我完全可以变成和她们一样的人。"


translate chinese day6_us_bd922584:


    th "这是错误的吗?"


translate chinese day6_us_0a98cbd7:


    "事实上，不对。在这个夏令营里我试图变得理智，行为得体!"


translate chinese day6_us_53d1df42:


    th "但是如果连乌里扬卡都这么说... "


translate chinese day6_us_bc130c9d:


    th "那我还怎么离开这里?!"


translate chinese day6_us_cc350778:


    "我们走进门，乌里扬卡花了一段时间才找到电灯的开关。"


translate chinese day6_us_ded0fd48:


    us "Presto!"


translate chinese day6_us_5e0cd80f:


    me "干得漂亮!"


translate chinese day6_us_77e18087:


    "我夸张的拍着手。"


translate chinese day6_us_ad0cb0ae:


    us "嘿！不要笑话我!"


translate chinese day6_us_e16a3b7b:


    "她撅起嘴。"


translate chinese day6_us_80d646aa:


    me "好吧，接下来是什么?"


translate chinese day6_us_3671b8c0:


    us "这里!"


translate chinese day6_us_d42abb00:


    "乌里扬娜走到下一个房间的门前，犹豫了一小会儿，然后打开了门。"


translate chinese day6_us_724ee48f:


    "里面没有什么空间，这里似乎就是用来储藏的。"


translate chinese day6_us_7c05bc4a:


    "各种各样的箱子堆出了一个小山峰，散乱在各处的电子设备让人想起科学家的头脑风暴。远处的书架说明这里不是一个普通的储藏室或者是什么垃圾堆。"


translate chinese day6_us_d39afe89:


    "我的右边有一台带有放映机的电视。"


translate chinese day6_us_a3c51985:


    "有趣的是，这不是苏联产的，看样子像是日本的。"


translate chinese day6_us_62c4c780:


    "这倒是不奇怪，在那个时候（至少对我来说是那个时候），已经开始有进口产品了。"


translate chinese day6_us_8bf0de68:


    us "看我怎么跟你说的!"


translate chinese day6_us_709cc631:


    "乌里扬卡胜利的笑着。"


translate chinese day6_us_ff471f9a:


    me "是啊，是啊..."


translate chinese day6_us_6c9f5d6f:


    us "你以前见过这种东西吗?"


translate chinese day6_us_756a29e3:


    me "见过..."


translate chinese day6_us_46586735:


    "也许我也有一台VCR，大概就在我家的某个夹层里。"


translate chinese day6_us_cd67e98a:


    us "你看起来一点也不惊讶。"


translate chinese day6_us_95322256:


    me "我为什么要惊讶?"


translate chinese day6_us_2c209cc5:


    us "一台日本产的电视和放映机! 你好像经常见到一样!"


translate chinese day6_us_69f8f8a0:


    me "嗯...最近见到的不多了。"


translate chinese day6_us_78674bdc:


    "我懒洋洋的说着。"


translate chinese day6_us_7505e340:


    us "你看起来好压抑啊..."


translate chinese day6_us_469db7d9:


    us "算了，不管了。拿好这个!"


translate chinese day6_us_4e5aa691:


    "她递给我一盘磁带，我翻来覆去的检查着，但是没有看到任何标签一类的东西。"


translate chinese day6_us_fb03e6d3:


    me "所以说这里面演的什么?"


translate chinese day6_us_d23e2c34:


    us "我也不知道。"


translate chinese day6_us_9137ce1a:


    "乌里扬卡呆呆的说着。"


translate chinese day6_us_3bb74eea:


    us "但是我确定肯定是很有意思的东西!"


translate chinese day6_us_4121ba2c:


    "很快屏幕上出现了有名的美国电影公司的标志。"


translate chinese day6_us_7331bf96:


    us "这里，这里，看!"


translate chinese day6_us_4653fbcc:


    "乌里扬卡几乎有点坐立不安。"


translate chinese day6_us_1b32e37e:


    me "我正看着呢..."


translate chinese day6_us_f7aa1f59:


    "过了几分钟，我发现录像带中是一部八十年代的电影，一个未来的机器人被送到过去，杀死某个人，所以一个英雄就不会出生了... "


translate chinese day6_us_f027422b:


    "巧合，有趣，你可以说我就是从未来来的人。"


translate chinese day6_us_5a80f756:


    th "所以说？我的任务就是杀掉这里的人们中的一个？"


translate chinese day6_us_aa60fb72:


    "我忍不住笑了起来。"


translate chinese day6_us_686edcc3:


    us "什么那么好笑啊?"


translate chinese day6_us_1a66e4d6:


    "乌里扬娜不爽的看着我。"


translate chinese day6_us_3c5e2cfc:


    me "啊，不，没什么。"


translate chinese day6_us_b1a006e1:


    us "看起来你好像不喜欢这个电影一样!"


translate chinese day6_us_731f7b7d:


    me "电影还好，我已经看过很多次了。"


translate chinese day6_us_433e89df:


    us "那怎么可能?"


translate chinese day6_us_41c9ba10:


    "她充满惊讶的瞪着大大的眼睛。"


translate chinese day6_us_78ab486a:


    me "那个...{w} 我的朋友给了我录像带。"


translate chinese day6_us_6532261a:


    us "嗯..."


translate chinese day6_us_e2092d77:


    "乌里扬卡仔细的看着我。"


translate chinese day6_us_a9137dac:


    me "总之，仔细看吧！"


translate chinese day6_us_e868da76:


    "我指着电视屏幕。"


translate chinese day6_us_993a1e1e:


    "当电影演到一半的时候，我似乎听到打开前门的声音。"


translate chinese day6_us_a2f7bc76:


    "有人说当一种感官不好的时候，另一种感官就被调动起来了，我看东西几乎是半瞎，所以我的听力非常好。"


translate chinese day6_us_0eebb0ce:


    me "关灯!"


translate chinese day6_us_9db6b90d:


    "我悄悄说着。"


translate chinese day6_us_b7ef3111:


    us "什么?"


translate chinese day6_us_c18f0f1a:


    me "把灯关上!"


translate chinese day6_us_39642f61:


    "乌里扬卡发现现在不是和我争吵的时候，然后冲向开关。{w} 同时，我按下了暂停按钮。"


translate chinese day6_us_955fefc7:


    "我听到隔壁房间的脚步声还有地面上闪烁的光线。"


translate chinese day6_us_bf71caa2:


    th "我很奇怪这种时候谁想潜入机器人社团?"


translate chinese day6_us_de372b78:


    mt "他们不在这里。"


translate chinese day6_us_d099a808:


    "是奥尔加·德米特里耶夫娜的声音。"


translate chinese day6_us_9b04210e:


    "很快大门被关上了，我放松的叹着气。"


translate chinese day6_us_505772f4:


    me "看到没有？都是因为你!"


translate chinese day6_us_b7ef3111_1:


    us "什么?"


translate chinese day6_us_50398fc3:


    "乌里扬卡惊讶的看着我。"


translate chinese day6_us_bd77af7a:


    me "她要不然晚上还能来这里干什么!肯定是在找咱们！"


translate chinese day6_us_2405d879:


    me "阿丽夏可能并不在乎你失踪了，但是我和辅导员住在同一间宿舍!"


translate chinese day6_us_60f3d758:


    "她思考了一会儿。"


translate chinese day6_us_2072b7ef:


    us "嗯，怎么了?"


translate chinese day6_us_e4a7abab:


    me "怎么了...? 你怎么到处找麻烦？你好像喜欢一样？！"


translate chinese day6_us_10573fa6:


    us "这样很有意思啊!"


translate chinese day6_us_08fab8ce:


    me "你一直被批评责备很有意思？！"


translate chinese day6_us_0e694a7f:


    us "敢于冒险，才能得到回报!"


translate chinese day6_us_7a6bb0f1:


    "她欢快的笑着。"


translate chinese day6_us_ff350d77:


    me "再怎么说也是有原则的，到现在为止你还没有受到严厉的惩罚，不过谁知道呢!最后的结局可能很糟！"


translate chinese day6_us_43d44c0e:


    us "你这碎碎念像个老太太一样!"


translate chinese day6_us_3db311fc:


    "她生气的说着。"


translate chinese day6_us_23e36b36:


    me "谁刚刚才说的我不像是个成年人?"


translate chinese day6_us_cff7648a:


    us "别管了，咱们把电影看完吧。"


translate chinese day6_us_d07a4ff2:


    "我决定不和她吵架。"


translate chinese day6_us_e7c03f19:


    th "最后我们还是要被奥尔加·德米特里耶夫娜列入黑名单，早一个小时，晚一个小时—有关系吗?"


translate chinese day6_us_e487a8f7:


    "每到恐怖的片段，乌里扬卡都尖叫着握住我的手。"


translate chinese day6_us_0958d1fb:


    "我对这个电影没有那么感兴趣，不过还是假装看得很有意思。"


translate chinese day6_us_12d2bbe0:


    "除了邪恶的机器人，还有一个好人从未来回到过去帮助他们。"


translate chinese day6_us_9a8a93f1:


    th "也许我不是来到这里残酷无情的进行杀戮，相反是来保护什么?"


translate chinese day6_us_a140a31c:


    th "阻止一件事，比如说，阻止某个人犯下错误..."


translate chinese day6_us_2d270f7c:


    th "这个想法很有趣，但是却没有任何的依据。"


translate chinese day6_us_dd48914d:


    th "我没有霰弹枪，没有皮夹克，没有霸道的摩托车..."


translate chinese day6_us_30190641:


    th "我甚至都没有墨镜!"


translate chinese day6_us_8d2a1158:


    us "看，看!"


translate chinese day6_us_e99c1fdf:


    "画面上出现的下一个画面上，汽车的追逐战，枪战，爆炸..."


translate chinese day6_us_7d3685bb:


    me "你觉得好人会赢吗?"


translate chinese day6_us_d6b61a8f:


    "当然，我知道答案。"


translate chinese day6_us_6c856ecd:


    us "当然了！好人总是会赢的!"


translate chinese day6_us_15e55f1b:


    "她严肃的说着。"


translate chinese day6_us_90e45f3b:


    "如果我们考虑一下乌里扬卡经常成功的进行恶作剧的话，“好人”这个词语的意思很有问题了。"


translate chinese day6_us_3bc8c3ba:


    us "等等！你已经看过了。"


translate chinese day6_us_0886d374:


    me "是啊，我只是想要了解一下你的想法..."


translate chinese day6_us_1a8f9a59:


    "很快电影结束了。"


translate chinese day6_us_8b49353c:


    "在最后时刻，乌里扬卡在房间里跑来跑去，试图一点不错过的看完经典的镜头。"


translate chinese day6_us_1c0e27c2:


    us "哈!"


translate chinese day6_us_80984763:


    "当片尾的字幕出现的时候，她放松的舒了一口气。"


translate chinese day6_us_c874b643:


    me "喜欢吗?"


translate chinese day6_us_ae076645:


    us "这需要问吗？！当然喜欢了!"


translate chinese day6_us_f2e007f0_1:


    me "好..."


translate chinese day6_us_8b59d260:


    us "而且我能看出你不怎么喜欢。"


translate chinese day6_us_c4bf4ad4:


    "她斜眼看着我。"


translate chinese day6_us_09b227d4:


    me "嗯，总之这是一部给小孩子看的电影。"


translate chinese day6_us_f868e368:


    us "啊... 真的?"


translate chinese day6_us_ed8eb672:


    me "嗯...{w} 是啊，怎么了?"


translate chinese day6_us_565373fe:


    us "没什么。"


translate chinese day6_us_9827d8ef:


    "她叹了口气，靠在一堆箱子上。"


translate chinese day6_us_46f6e5ee:


    me "大概到了睡觉时间。"


translate chinese day6_us_b1586995:


    us "我累了，我哪里也不要去。"


translate chinese day6_us_b8e7bf20:


    me "你请便吧，我要走了。"


translate chinese day6_us_ba7d98ab:


    us "嘿，等一下！"


translate chinese day6_us_80abfaf2:


    "乌里扬卡马上跳起来抓住我的手。"


translate chinese day6_us_38a6668d:


    us "你要把我一个人丢在这里?"


translate chinese day6_us_4cad9cd9:


    me "呃，如果你愿意的话，我可以把你背回房间?"


translate chinese day6_us_fefef18f:


    "我不太确定的说。"


translate chinese day6_us_3f9a7fc5:


    "那个我已经体验过了。"


translate chinese day6_us_b1823081:


    us "不！我要在这里睡觉!"


translate chinese day6_us_94133a62:


    "她在箱子里翻来翻去，找到了一些毯子和枕头。"


translate chinese day6_us_4bff6f94:


    me "又是这样的剧情..."


translate chinese day6_us_25f24811:


    "我叹叹气。"


translate chinese day6_us_10c6982a:


    me "你想想，奥尔加·德米特里耶夫娜已经在找咱们了，如果一晚上不回去的话..."


translate chinese day6_us_443d35ef:


    us "首先，她是在找你，这和我没有关系。"


translate chinese day6_us_be3315b6:


    "乌里扬卡笑着。"


translate chinese day6_us_fc9e5e8f:


    me "即使是那样，到时候你也会被抓到的!"


translate chinese day6_us_ff3c9f13:


    us "你怎么这么想？而且即使我被抓到也没关系，我已经习惯了!"


translate chinese day6_us_7eb76897:


    me "你怎么这么固执，这里距离你的房间只有几百米。"


translate chinese day6_us_737dc545:


    "而且乌里扬娜从来没有累的时候。"


translate chinese day6_us_4c76cf23:


    us "我累了!"


translate chinese day6_us_3a4d70a0:


    "她把自己卷在毯子里，转过身开始大声的假装打呼噜。"


translate chinese day6_us_6cacb19d:


    me "好吧，明天见。"


translate chinese day6_us_8f11bfa0:


    "我试图离开，不过乌里扬卡又一次抓住了我的手。"


translate chinese day6_us_69f4e5e6:


    me "好吧，又怎么了?"


translate chinese day6_us_8627abec:


    "她什么也没有说，她的眼神看起来好像被我吓到了一样。"


translate chinese day6_us_feabf980:


    "好吧，我完全不知道自己应该怎么做了。"


translate chinese day6_us_873663f3:


    th "我们的录像带历险记最开始就很愚蠢，现在更是让我完全不能理解了。"


translate chinese day6_us_2ba6c73f:


    "我不知道她的逻辑是怎样的，不知道她有什么动机。"


translate chinese day6_us_0ffe8ded:


    "然而，虽然是这样，我还是无法说不。"


translate chinese day6_us_08b7211c:


    "有什么阻止着我，也许是使命感，同情感，对孩子的耐心..."


translate chinese day6_us_849cfae0:


    me "好吧，你到底想怎样?"


translate chinese day6_us_f1881724:


    us "和我一起睡觉!"


translate chinese day6_us_9a9afc69:


    "我好像在哪里见过这种场景。"


translate chinese day6_us_b01cd732:


    me "好吧，咱们假设..."


translate chinese day6_us_4645bec9:


    "我关掉灯，在她身边躺下来。"


translate chinese day6_us_ca6c74b8:


    "还好现在是夏天，不然睡在凉地板上可不是个好主意。"


translate chinese day6_us_2ed1402b:


    th "没有床垫就更不必提了。"


translate chinese day6_us_376cd472:


    us "你怎么这么容易就同意了?"


translate chinese day6_us_12bc6723:


    "我可以从她的声音里听到怨恨。"


translate chinese day6_us_87a707b8:


    me "你这么要求的，所以我同意了。"


translate chinese day6_us_e8799476:


    "事实上，我认为乌里扬卡很快就会睡着，然后我就可以像往常一样把她抱回去。"


translate chinese day6_us_7e1e0002:


    th "总的来说这算是一个不错的计划，虽然我还是得负重跑步。"


translate chinese day6_us_08c4f608:


    us "嘿，跟我聊天!"


translate chinese day6_us_b1158062:


    me "你快睡觉！你自己喊着要睡觉的。"


translate chinese day6_us_0bf1db7d:


    us "快说话!"


translate chinese day6_us_c233b6a2_1:


    me "说什么?"


translate chinese day6_us_c034a3dd:


    us "跟我讲讲你自己有趣的故事，你的生活。"


translate chinese day6_us_0668a617:


    me "我的生活可没有那么丰富多彩。"


translate chinese day6_us_2776dad8:


    us "什么都回忆不起来?"


translate chinese day6_us_97091cb6:


    th "就算有什么，和她讲也不是一个好主意。"


translate chinese day6_us_67eea6e1:


    me "什么也没有。"


translate chinese day6_us_bb57e7a2:


    us "不可能那么糟糕!"


translate chinese day6_us_9779d985:


    "我转向乌里扬娜。"


translate chinese day6_us_691925cb:


    "她瞪着圆圆的眼睛，望着天花板。"


translate chinese day6_us_e0f7ef7d:


    me "可能的，相信我。"


translate chinese day6_us_22602e79:


    us "但是那太无聊了!"


translate chinese day6_us_ba42ab02:


    me "有时是..."


translate chinese day6_us_0e03ae69:


    "她沉默了一会儿，然后说:"


translate chinese day6_us_77b3d75b:


    us "但是在这里你玩得很高兴!"


translate chinese day6_us_c10011a5:


    me "啊，那是实话!"


translate chinese day6_us_89bea129:


    "我笑了。"


translate chinese day6_us_563002a7:


    us "你会记得这个夏令营吗?"


translate chinese day6_us_601cfdfa:


    th "首先我得离开这里。"


translate chinese day6_us_d06923c2:


    me "当然啦，我会记得。"


translate chinese day6_us_21d8894d:


    us "还有其他的呢?"


translate chinese day6_us_50b56044:


    me "会的..."


translate chinese day6_us_f4bcf7be:


    us "还有我?"


translate chinese day6_us_aa854ae9:


    me "还有你..."


translate chinese day6_us_93a0d0b4:


    "我用平淡的语气说着，说实话我没有仔细思考她说的话。"


translate chinese day6_us_e73afb35:


    us "我也会..."


translate chinese day6_us_a60d2254:


    "乌里扬娜悄悄说着。"


translate chinese day6_us_3c6c3d52:


    "我突然感觉到她钻进了我的怀中，还伸出手抱住我的脖子。"


translate chinese day6_us_9f8d7b41:


    me "舒服了吗?"


translate chinese day6_us_0162dcba:


    us "嗯。"


translate chinese day6_us_acaae73d:


    "她自言自语道。"


translate chinese day6_us_2f598c90:


    me "你没有找到更好的地方吗?"


translate chinese day6_us_f1d4b5e2:


    "她轻轻摇着头。"


translate chinese day6_us_1943e342:


    th "好吧，就让她那样躺着吧。"


translate chinese day6_us_47e54bbf:


    th "她可能更快的睡着。"


translate chinese day6_us_a20cefa7_4:


    "…"


translate chinese day6_us_94caab83:


    "大概过了五分钟，我试图拍拍她的肩膀看看有没有睡着。"


translate chinese day6_us_b7ef3111_2:


    us "什么?"


translate chinese day6_us_322c760e:


    "她没有抬头的问着。"


translate chinese day6_us_16a426b8:


    "我注意到她的声音有些不同。"


translate chinese day6_us_f075d0eb:


    me "你为什么不睡觉?"


translate chinese day6_us_a63d8c58:


    us "你说呢!"


translate chinese day6_us_a20cefa7_5:


    "…"


translate chinese day6_us_28e2eb35:


    "时间很不给力，过得很慢。"


translate chinese day6_us_d499dbc6:


    "我决定多等十分钟再检查看看。"


translate chinese day6_us_8efc776c:


    "但是现在度日如年..."


translate chinese day6_us_7d75fee5:


    "我的眼睛慢慢的闭上，我挣扎着让自己不要睡着。"


translate chinese day6_us_d580dc80:


    "我使劲眨着眼睛，结果发现自己已经快要失去意识了。"


translate chinese day6_us_f9df56b2:


    th "也许我可以闭一会儿眼睛，只是一分钟的话没什么大不了的嘛?"


translate chinese day6_us_6abdb446:


    "这些想法在我的头脑中闪过。"


translate chinese day6_us_b8b42840:


    "这之后我便渐渐的..."


translate chinese day6_sl_662a5ccc:


    "我没有睡好。"


translate chinese day6_sl_84b3863a:


    "我总是隔一两分钟就醒过来一次，然后又再次睡着。"


translate chinese day6_sl_8076c467:


    "我也没有做很多梦，只有一些诡异的影像，估计到了早晨就会忘掉了。"


translate chinese day6_sl_b893b471:


    "奥尔加·德米特里耶夫娜叫我起来。"


translate chinese day6_sl_93a88bb1:


    mt "快起来!"


translate chinese day6_sl_c96aaf8e:


    "我揉揉眼睛，迷迷糊糊的看着她。"


translate chinese day6_sl_f6a4fbf2:


    me "现在几点了?"


translate chinese day6_sl_42495f59:


    mt "够晚了!"


translate chinese day6_sl_ea0df862:


    "我全身都很疼，脑袋像个保龄球一样沉。"


translate chinese day6_sl_e419b053:


    mt "快点起来，然后去洗漱!"


translate chinese day6_sl_e9440aad:


    "我顺从的站起来，拿起洗漱包包，走出房门。"


translate chinese day6_sl_4a70bb48:


    "我现在不想和辅导员说话。"


translate chinese day6_sl_f672dc60:


    th "显然我面临着一顿严厉的批评。"


translate chinese day6_sl_1c2ef58a:


    th "但是我要尽量的往后拖。"


translate chinese day6_sl_06732be2:


    "反正我一直是这个风格。"


translate chinese day6_sl_36a0fcdc:


    "我尝试着不要站着睡着，终于来到了水池旁。{w} 冷冷的水让我恢复了一些意识。"


translate chinese day6_sl_ae8a5bf9:


    th "也许我应该去找斯拉维娅？我还有一些时间。"


translate chinese day6_sl_a714f4e1:


    th "去对她说对不起..."


translate chinese day6_sl_7daba806:


    th "不过反过来说我为什么要道歉啊，这是她的主意。"


translate chinese day6_sl_1398f30a:


    th "我到底做了什么错事?"


translate chinese day6_sl_8e04721c:


    th "除了一次又一次的把自己置于这种愚蠢的境地..."


translate chinese day6_sl_3d8cc6b9:


    "我深吸一口气，返回奥尔加·德米特里耶夫娜的房间，准备坚决的证明自己的清白，或者至少表明自己的立场。"


translate chinese day6_sl_cb36cd72:


    "但是房间是空的。"


translate chinese day6_sl_efc1096d:


    "我坐在床上用手捂着脸。"


translate chinese day6_sl_6a13efce:


    th "我得想出一个理由，一个说得过去的理由。"


translate chinese day6_sl_114a09ac:


    th "如果她不相信事实，当然这也不全怪她，我如果在她的位置上可能也会做出错误的判断..."


translate chinese day6_sl_e967e0d0:


    "我无所事事。"


translate chinese day6_sl_ae02f020:


    "我就坐在这里，看着太阳慢慢升起。"


translate chinese day6_sl_f25133e7:


    "门悄悄的打开了，辅导员走了进来。"


translate chinese day6_sl_3a4485f1:


    me "奥尔加·德米特里耶夫娜，我得跟你解释解释..."


translate chinese day6_sl_cf0c0005:


    mt "不必了..."


translate chinese day6_sl_c5e1ce92:


    "我惊讶的看着她。"


translate chinese day6_sl_79c7318e:


    mt "我都明白!"


translate chinese day6_sl_91b3c16d:


    me "你明白事情不是你看到的那个样子了?"


translate chinese day6_sl_6889d390:


    mt "为什么，怎么看就是那个样子!{w} 我不想责备你或者斯拉维娅，我都明白，青春期，性激素，各种各种..."


translate chinese day6_sl_47b5cf5f:


    "我叹着气，又把脸枕到手上。"


translate chinese day6_sl_93a58ce6:


    me "我到底要怎么说才能让你相信我?"


translate chinese day6_sl_c8c68b8a:


    mt "什么都不用说，我已经说过了—我全都明白。"


translate chinese day6_sl_8ba64c1f:


    me "你什么也不明白..."


translate chinese day6_sl_b459e062:


    "我嘀咕着。"


translate chinese day6_sl_f9270db9:


    mt "好了，去集合吧!"


translate chinese day6_sl_106dc472:


    "我站起来，懒洋洋的跟着她。"


translate chinese day6_sl_dd234e0f:


    me "我能不能去找她聊聊，至少?"


translate chinese day6_sl_b71e2412:


    mt "当然不行。"


translate chinese day6_sl_7318b2da:


    "辅导员惊讶的看着我，好像我的问题问得很不合适。"


translate chinese day6_sl_c3fad59f:


    mt "我已经警告护士不要让你进去了。"


translate chinese day6_sl_c2e10946:


    me "那我是不是至少能了解一下斯拉维娅的情况?"


translate chinese day6_sl_a809ce74:


    mt "嗯，看起来斯拉维娅确实是生病了。"


translate chinese day6_sl_66a3dff1:


    "我没能问问到底有多严重，我们已经来到了广场。"


translate chinese day6_sl_90404e3b:


    mt "去站到你自己的位置上。"


translate chinese day6_sl_2dca417a:


    "我站在了电子小子旁边。"


translate chinese day6_sl_4f3f853d:


    el "所以说，昨天发生了什么?"


translate chinese day6_sl_ae5c7f77:


    "他悄悄的对我说。"


translate chinese day6_sl_cb23542f:


    me "你在说什么?"


translate chinese day6_sl_e1649ec8:


    el "就是说，你突然消失了。"


translate chinese day6_sl_9553ce6a:


    me "嗯，没什么特别的..."


translate chinese day6_sl_ca0c06f1:


    th "让整个夏令营都知道我们的事情...那太可怕了。"


translate chinese day6_sl_3490597b:


    th "而且既然辅导员都误解我们，那么其他人都会怎么想..."


translate chinese day6_sl_b37413a9:


    "虽然我开始觉得这好像也不是完全的错。"


translate chinese day6_sl_e6dd2114:


    "整个集合的活动包括点名，还有宣布今天澡堂维修而不能使用的消息。"


translate chinese day6_sl_244fe891:


    "最后，少先队员们开心的聊着天前往食堂，我无语的跟着他们。"


translate chinese day6_sl_ad482dd4:


    "说实话，我没有吃饭的心情。"


translate chinese day6_sl_09b5d477:


    "在食堂门口晃了一会儿以后，我离开了那里。"


translate chinese day6_sl_39e8f495:


    "我也不知道这是怎么回事，不过医务室神奇的出现在了我的面前。"


translate chinese day6_sl_1a778ee7:


    "我的自动导航模式把我带到了这里。"


translate chinese day6_sl_770ba296:


    "我盯着大门，完全不知所措。"


translate chinese day6_sl_703f77b3:


    th "反正他们也不会让我进去的...现在应该干什么呢?"


translate chinese day6_sl_bf506fe5:


    th "我应该等着奥尔加·德米特里耶夫娜的判决吗，还是等着夏令营里的每一个人都知道我们的事情?"


translate chinese day6_sl_917651b4:


    th "那更加糟糕!{w}各种奇怪的眼光盯着我..."


translate chinese day6_sl_45615f79:


    "那是不可避免的..."


translate chinese day6_sl_6a4721bc:


    th "不，我并没有犯什么错误，但是在这个奇妙扭曲的社会里，这种行为不是被认为是罪恶的，就是被看成背叛行为。"


translate chinese day6_sl_2f9f3e25:


    th "也就是说我反正也不会再失去什么了!"


translate chinese day6_sl_991e1a72:


    "我毫不犹豫的敲响了门。"


translate chinese day6_sl_89ac2598:


    cs "等一下!"


translate chinese day6_sl_01d2073c:


    "我拉动着把手，但是无法打开。"


translate chinese day6_sl_13207c20:


    th "嗯，锁上了?"


translate chinese day6_sl_e26ebd50:


    "接下来我听到了钥匙拧动的声音，然后护士出现了。"


translate chinese day6_sl_a5eeb60e:


    cs "早上好，少先队员..."


translate chinese day6_sl_32f6d96b:


    me "你好!"


translate chinese day6_sl_df3861de:


    "我充满自信的回答着。"


translate chinese day6_sl_0e5cbc98:


    cs "嗯，你知道你被禁止进入医务室了吗?"


translate chinese day6_sl_1654d9af:


    me "是啊...{w} 但是如果你可以..."


translate chinese day6_sl_e5ee009f:


    cs "不...这样可不行啊!"


translate chinese day6_sl_7665ceec:


    "她开玩笑地说着。"


translate chinese day6_sl_a786531b:


    cs "辅导员的命令就是命令。{w}如果你要问我的话，我自己并没有发现你有什么不道德的行为，不过奥尔加·德米特里耶夫娜..."


translate chinese day6_sl_8b820e8f:


    me "也许我可以做点什么...?"


translate chinese day6_sl_5d8ff9ca:


    cs "不，不行的。"


translate chinese day6_sl_cb795a40:


    "我愤怒的看着她，然后转过身，假装地慢慢移动着。"


translate chinese day6_sl_34d50336:


    "我不知道接下来应该做什么..."


translate chinese day6_sl_2e89c23b:


    "我就这样往前走着，用力击打着眼前的大树。"


translate chinese day6_sl_a20cefa7:


    "..."


translate chinese day6_sl_a2e15f1e:


    "直到我的手疼得不能再打，嘴唇被咬出了血，我的怒气才渐渐平息。"


translate chinese day6_sl_1947d5ea:


    "有意思的是我在这个夏令营的神奇遭遇现在已经变成了完全的日常琐事。{w}一出就像是我的存在一样好笑的戏剧。"


translate chinese day6_sl_82da43b7:


    "然后我又一次做错了什么，想错了什么，接下来便发生了什么我无法预料的事情..."


translate chinese day6_sl_a51631b5:


    th "好吧，就让一切…"


translate chinese day6_sl_c0292f4c:


    "食堂附近没有人。"


translate chinese day6_sl_c23a417d:


    "我放松的找了一个角落里的位置，期望着没有人发现我。"


translate chinese day6_sl_ac6511c6:


    "心不在焉的往肚子里填充着我的早饭，我正要离开的时候未来向我的桌子冲过来。"


translate chinese day6_sl_337999bd:


    mi "啊，早上好，谢苗，我今天有点起晚了...我可以坐在你旁边吗?"


translate chinese day6_sl_705add58:


    "她直接就坐了下来。"


translate chinese day6_sl_4b92e09f:


    me "那个，我实际上正准备离开的..."


translate chinese day6_sl_579406ce:


    mi "那个是真的吗，你和斯拉维娅，那个...来着？我是说，你懂我的意思。"


translate chinese day6_sl_5b30cd59:


    "她贴紧了看着我，不停地眨着眼睛。"


translate chinese day6_sl_b9e0f0b2:


    me "谁跟你说的?"


translate chinese day6_sl_e3364d6a:


    "好吧，谣言传的是很快，但是这也太快了!"


translate chinese day6_sl_331ec19b:


    "我开始慢慢的发怒。"


translate chinese day6_sl_f4fd2d5d:


    mi "我是从热妮娅那里听来的，她大概是听起斯科夫说的。他是听奥尔加·德米特里耶夫娜说的。你不要误会喔，这和我没有什么关系，如果你不想回答的话，就不要说了！但是如果你不是很在意的话..."


translate chinese day6_sl_a3ccfa9b:


    me "你说的对，我不想回答。"


translate chinese day6_sl_1203e79e:


    "我站起来，快速朝出口走去。"


translate chinese day6_sl_23fd5e58:


    "今天天气很热。{w}这让我的愤怒加剧，因为我讨厌热天。"


translate chinese day6_sl_57084d4c:


    "然而现在任何事情都可能让我发疯。{w}但是现在我得决定应该如何控制住自己。"


translate chinese day6_sl_50ccb861:


    "我不想遇见任何人，所以我前往森林，在那里我可以一个人静静的思考。"


translate chinese day6_sl_1d5b76c1:


    "来到了一片林中空地，我坐在一个树桩上，捡起了一根树枝，开始敲打着玩了起来。"


translate chinese day6_sl_4104b753:


    th "如果没有什么是我可以决定的，那么我唯一可以做的就是等待。"


translate chinese day6_sl_8763d3f6:


    th "但...{w}说真的我可以做什么?"


translate chinese day6_sl_6c70ad8a:


    th "对医务室发起突击?{w} 就算我成功了，接下来呢?"


translate chinese day6_sl_630e01c1:


    th "试图让奥尔加·德米特里耶夫娜恢复理智？那不太可能..."


translate chinese day6_sl_3b57bdd9:


    "最有意思的是，整个诡异的事件现在变成了一件日常的糟糕情况。{w}如果是在我的世界里发生的话，应该不会有任何问题。"


translate chinese day6_sl_115eb200:


    "天啊，究竟有什么问题啊，就算是我真的和那个孩子睡觉了，又能有什么问题?"


translate chinese day6_sl_964f46c9:


    "就算是我真的只有十七岁，就算是没有什么糟糕的事情，我也只是会更加尴尬一些，并没有什么别的..."


translate chinese day6_sl_f32a5b64:


    th "现在呢？我不是自己命运的主人..."


translate chinese day6_sl_46c5eccc:


    "我曾经是过吗?{w}在我一辈子里，有没有一件事情是完全按照我的意愿的?"


translate chinese day6_sl_a9996173:


    "我究竟有没有到达过自己追求的极限?"


translate chinese day6_sl_b7175618:


    th "现在我一旦达到了以后，我又坚持不住了..."


translate chinese day6_sl_19ac515c:


    th "也许从最开始我就是一个观众?{w} 我神秘的出现在这个夏令营，所有的学生，发生在我身上的事情..."


translate chinese day6_sl_ee7dab86:


    th "这些只是一个奇怪的游戏，我只是其中的一个棋子。"


translate chinese day6_sl_4f0726c7:


    th "好吧，如果是那样，我也要把自己的角色演好!"


translate chinese day6_sl_8af8d5f1:


    "我站起来，准备返回营地。"


translate chinese day6_sl_e773fa2d:


    "我的计划还没有想好，但是我已经决定了!"


translate chinese day6_sl_06b4910d:


    "辅导员的房间里没有人。"


translate chinese day6_sl_be72f150:


    "我搜索着整个房间，找出了一个睡袋，我的冬季服装。还有一些其他的我找到的东西。"


translate chinese day6_sl_e5172865:


    "我走出来，直接去找阿丽夏。"


translate chinese day6_sl_5959997f:


    dv "准备远足?"


translate chinese day6_sl_a90c5245:


    me "是啊，去远足。"


translate chinese day6_sl_df3861de_1:


    "我又重复了一次。"


translate chinese day6_sl_07a7866b:


    dv "那好吧，小心!"


translate chinese day6_sl_79ac8364:


    "她狡猾的对我笑着，然后继续走掉了。"


translate chinese day6_sl_885cf3e5:


    "我加快了前往医务室的脚步。"


translate chinese day6_sl_8c0459e9:


    th "护士也是人，所以她最后还是会去吃饭的。"


translate chinese day6_sl_c3cfebc5:


    "我躲在灌木丛里。"


translate chinese day6_sl_a20cefa7_1:


    "..."


translate chinese day6_sl_32d9f799:


    "时间过得很快。{w}我终于做出了决定。"


translate chinese day6_sl_aa94e82e:


    "我还没有想好第二步，不过我不管。"


translate chinese day6_sl_5236e874:


    th "最重要的是我顺其自然，要发生什么就发生什么。"


translate chinese day6_sl_15377a38:


    "最后，护士走出了医务室。"


translate chinese day6_sl_0c4f3316:


    "她向外看看，发现没有危险，用钥匙锁上，然后快速离开了。"


translate chinese day6_sl_808818b1:


    "当她走到路口的时候，我来到窗前，不停的敲着门。"


translate chinese day6_sl_7d8aa336:


    "斯拉维娅很快出现了。"


translate chinese day6_sl_0e4abe5d:


    sl "谢苗，你在这里做什么，不是被禁止..."


translate chinese day6_sl_3ce9b29e:


    me "你感觉怎么样?"


translate chinese day6_sl_6d6acda2:


    sl "我还好…{w}但是奥尔加·德米特里耶夫娜让我在这里多呆一天。"


translate chinese day6_sl_d1eb8d48:


    th "她和我说的完全不一样啊!"


translate chinese day6_sl_e00f006d:


    me "好吧，出来吧。"


translate chinese day6_sl_8df31520:


    "我充满自信的说着。"


translate chinese day6_sl_ba36c8c0:


    sl "你在说什么？为什么啊？"


translate chinese day6_sl_fb77ae46:


    "她害怕的问着。"


translate chinese day6_sl_7a4ad964:


    me "快走！我会和你解释的!"


translate chinese day6_sl_44e59b45:


    "她看了我一会儿，不过没有继续争论，打开了窗户。"


translate chinese day6_sl_c0792059:


    "我帮助她跳下来。"


translate chinese day6_sl_05a7a65b:


    sl "好了，接下来呢?"


translate chinese day6_sl_b749d8c0:


    me "走吧!"


translate chinese day6_sl_4a86fb6e:


    "我拉住她的手，往森林跑去。"


translate chinese day6_sl_2754ebb3:


    "过了几分钟，斯拉维娅停了下来。"


translate chinese day6_sl_aa07a769:


    sl "谢苗，咱们到底在做什么?"


translate chinese day6_sl_70ae8bb8:


    me "怎么了，你真的觉得这样没问题吗?"


translate chinese day6_sl_149bd1f8:


    sl "不是啊，但是我觉得这样会更糟糕啊。"


translate chinese day6_sl_8c2bd012:


    me "为什么？咱们本来有机会把事情弄好的!"


translate chinese day6_sl_9ee0cfcb:


    sl "好吧，但是接下来会怎么样?"


translate chinese day6_sl_936b630d:


    "我还没有想过。"


translate chinese day6_sl_40898a23:


    "所有的事情到目前为止都不错，但是我对于接下来的部分没有一点计划。"


translate chinese day6_sl_6bf1e9fc:


    me "看!"


translate chinese day6_sl_b46c28f2:


    "我打开包包，给她看我从辅导员房间里拿的东西。"


translate chinese day6_sl_e26abf96:


    sl "然后呢?"


translate chinese day6_sl_0fa153ce:


    "斯拉维娅疑惑的看着我。"


translate chinese day6_sl_ccf6bd0d:


    me "不知道...{w} 咱们在森林里呆一段时间...{w} 告诉他们咱们有自己的想法!"


translate chinese day6_sl_c23e3fba:


    sl "那太愚蠢了，而且很孩子气!"


translate chinese day6_sl_59c4f9d9:


    me "好吧，如果你不想的话..."


translate chinese day6_sl_d4d98b56:


    "她失望的看着我。"


translate chinese day6_sl_eb5caabf:


    sl "都听你的..."


translate chinese day6_sl_ecece0fc:


    "这话让我起鸡皮疙瘩。"


translate chinese day6_sl_0834429f:


    th "'都听你的'..."


translate chinese day6_sl_6428f6b8:


    "从来没有人对我这么说过。"


translate chinese day6_sl_c747ecee:


    me "不管怎么说，这是现在最好的决定了!"


translate chinese day6_sl_6f9aa3f1:


    sl "好吧"


translate chinese day6_sl_ecfe56f5:


    "我把睡袋摆在地上，然后我们坐在上面。"


translate chinese day6_sl_032a8535:


    sl "所以咱们应该做什么?"


translate chinese day6_sl_7479b07e:


    me "我不知道…"


translate chinese day6_sl_83657023:


    "我还没有经历过这样的彻底失败。"


translate chinese day6_sl_6f0e498c:


    "我不是后悔自己的选择，我是真的不知道接下来该做什么。"


translate chinese day6_sl_9d1d5c5c:


    me "你有什么建议吗?"


translate chinese day6_sl_8cefafd9:


    sl "我觉得最好找点吃的...你有带什么吃的东西吗?"


translate chinese day6_sl_a5ff15c9:


    th "这正是我忘记的事情..."


translate chinese day6_sl_5cb822ee:


    me "没有..."


translate chinese day6_sl_0080cf26:


    "我慢慢的回答着。"


translate chinese day6_sl_6f9aa3f1_1:


    sl "那好吧..."


translate chinese day6_sl_0b938548:


    "我们静静的坐着。"


translate chinese day6_sl_e893c99b:


    th "我很好奇他们会不会来找我们？不过即使他们来找也不太可能会成功，森林太大了。"


translate chinese day6_sl_e870b5fc:


    th "但是，吃的...{w} 到了晚上我也会饿的..."


translate chinese day6_sl_55e17cab:


    me "好吧，呆在这里，我马上回来。"


translate chinese day6_sl_29684dd8:


    sl "你去哪儿啊?"


translate chinese day6_sl_b8a8c899:


    me "去找吃的!"


translate chinese day6_sl_a81ddd10:


    sl "也许咱们应该一起去吧?"


translate chinese day6_sl_a428b9a5:


    me "不必了...我马上就回来!"


translate chinese day6_sl_775467e0:


    "我迅速回到营地，把斯拉维娅一个人留在了森林里。"


translate chinese day6_sl_bbc0779a:


    "我很确定她会在树林里等着我...我希望自己也能对回去的路这么确定就好了..."


translate chinese day6_sl_d8d6f4b9:


    th "即使他们还没有开始搜索我们，也肯定发现斯拉维娅不见了，所以我还是保持警惕为好。"


translate chinese day6_sl_60719f55:


    th "现在每一个少先队员对于我来说都是一个威胁，不只是那些认识我的人，发生了绑架事件之后肯定大家都知道了。"


translate chinese day6_sl_90bcafae:


    "虽然迅速抵达食堂很容易，但是我却不知道下一步做什么了。"


translate chinese day6_sl_566963d3:


    th "我不能跟什么都没有发生似的走进去..."


translate chinese day6_sl_4a1751e3:


    us "你是在躲着谁吗?"


translate chinese day6_sl_e9efaf89:


    "我的心脏差点跳出来。"


translate chinese day6_sl_6377c405:


    "乌里扬卡站在我的后面。"


translate chinese day6_sl_a1efee9e:


    us "饿了吗?"


translate chinese day6_sl_c4b83f7b:


    "她盯着我。"


translate chinese day6_sl_9ba98794:


    us "说话呀，怎么这么沉默?"


translate chinese day6_sl_d6658e34:


    "我一时大脑一片空白，什么也说不出来。"


translate chinese day6_sl_961f4581:


    us "所以说你为什么来这里?"


translate chinese day6_sl_1a3f56c5:


    us "哦，真的...?"


translate chinese day6_sl_e7ec8520:


    me "是啊，就这样..."


translate chinese day6_sl_b7e4de4c:


    us "好吧，马上回来!"


translate chinese day6_sl_30a664f4:


    "让我惊讶的是，乌里扬卡没有问任何尴尬的问题。"


translate chinese day6_sl_e2f777eb:


    "一眨眼的功夫她就钻进了食堂的大门。"


translate chinese day6_sl_a977e8fd:


    "我不知道自己是不是应该相信她，还是在她带着增援回来之前赶紧逃跑...?"


translate chinese day6_sl_ab25b248:


    "不过乌里扬卡没有一会儿就拿着一个大塑料袋出来了，打断了我的思考。"


translate chinese day6_sl_9a7ad094:


    us "接着!"


translate chinese day6_sl_b493cbf1:


    "袋子里是很多小面包，还有几盒酸牛奶。"


translate chinese day6_sl_65a156a9:


    me "可是为什么?"


translate chinese day6_sl_336a06df:


    us "那个...因为...勇敢...而且...很厉害!"


translate chinese day6_sl_945c59bc:


    "她狡猾的笑着。"


translate chinese day6_sl_83d1e252:


    us "替我向斯拉维娅问好!"


translate chinese day6_sl_233fc538:


    "乌里扬卡挥着手消失了。"


translate chinese day6_sl_f6f60dcb:


    "我不解的站在原地，不过过了一会儿还是动身返回森林了。"


translate chinese day6_sl_2ae59a66:


    "这完全不像是乌里扬卡，但是说实话我很感谢她。"


translate chinese day6_sl_a3e91759:


    "结果我靠自己的力量根本弄不到食物..."


translate chinese day6_sl_f703ec01:


    "快要回到树林的时候我发现斯拉维娅还是原来的姿势坐在睡袋上。"


translate chinese day6_sl_482db2cf:


    me "给..."


translate chinese day6_sl_8dadd6b7:


    "我把袋子递给她。"


translate chinese day6_sl_e7830fb4:


    sl "好快!"


translate chinese day6_sl_0b70dfeb:


    "她激动的说着。"


translate chinese day6_sl_254d4d53:


    me "嗯，我得到了援助..."


translate chinese day6_sl_af5c4880:


    "斯拉维娅有点疑惑的看着我，但是没有继续追问。"


translate chinese day6_sl_1ec45799:


    "我们一言不发的匆匆吃着。"


translate chinese day6_sl_563de47a:


    "吃完我满意的叹口气。"


translate chinese day6_sl_af6fb71f:


    me "真的很不错啊。"


translate chinese day6_sl_8b01f8be:


    sl "是啊。"


translate chinese day6_sl_a4965e91:


    "斯拉维娅表示同意。"


translate chinese day6_sl_eacac508:


    sl "咱们现在应该做什么?"


translate chinese day6_sl_d60537c4:


    me "你有什么建议吗?"


translate chinese day6_sl_1210502c:


    sl "关于什么的?"


translate chinese day6_sl_6a46b67c:


    me "至少告诉我怎么解决辅导员。"


translate chinese day6_sl_29040378:


    sl "我不知道...{w}而且你确定..."


translate chinese day6_sl_748c8dfc:


    me "是的!"


translate chinese day6_sl_e5e97d5d:


    "我打断了她。"


translate chinese day6_sl_ccf4cd89:


    me "这是一种抗议，她应该知道她不能控制我..."


translate chinese day6_sl_2a7a8a3b:


    "我结结巴巴的说着。"


translate chinese day6_sl_704441dd:


    me "...我...干各种事情!"


translate chinese day6_sl_4d7af59e:


    "我好像说了什么愚蠢的话。"


translate chinese day6_sl_97d14a00:


    "即使我是奥尔加·德米特里耶夫娜跑腿的差使，斯拉维娅..."


translate chinese day6_sl_f9d7b013:


    sl "你好有趣!"


translate chinese day6_sl_b7bd4e35:


    "她可爱的笑着。"


translate chinese day6_sl_ee9341f5:


    sl "咱们可以去找她谈谈，但是如果你愿意就在这里坐着，我也不反对。"


translate chinese day6_sl_6c4c506e:


    me "我试着和她谈过..."


translate chinese day6_sl_c77d0992:


    sl "你确定方法得当吗?"


translate chinese day6_sl_32522388:


    th "怎样是方法得当?"


translate chinese day6_sl_002325fc:


    "说实话我不知道答案。"


translate chinese day6_sl_33279267:


    "这种尴尬的谈话对我来说实在是太难了。"


translate chinese day6_sl_28d5f3e2:


    "我确定如果我有这种能力的话，事情可能就不会这样发展了。"


translate chinese day6_sl_6650b9b5:


    "当然我很害怕，我怪自己，我还是没有自信能做好..."


translate chinese day6_sl_7479b07e_1:


    me "我不知道..."


translate chinese day6_sl_c954d69f:


    "斯拉维娅笑了出来。"


translate chinese day6_sl_3d342d5a:


    me "什么?!"


translate chinese day6_sl_31892cbe:


    sl "没什么...我只是觉得自己像是一个公主被王子从巫婆的邪恶城堡中救了出来。"


translate chinese day6_sl_8f838576:


    "我小心的看着她。"


translate chinese day6_sl_e89f228b:


    "虽然这一切听起来都很浪漫，但是我感觉斯拉维娅刚刚的话都说不通。"


translate chinese day6_sl_97cde972:


    th "不过总的来说，奥尔加·德米特里耶夫娜是一个邪恶的老巫婆...这是很有道理的!"


translate chinese day6_sl_88e6b2de:


    me "是啊...证据很明显。"


translate chinese day6_sl_f51cd4bc:


    "我清楚地说着。"


translate chinese day6_sl_3915d229:


    sl "而且公主们在这种情况下没有什么选择。"


translate chinese day6_sl_aabed66d:


    "斯拉维娅说了一句隐晦的话。"


translate chinese day6_sl_1cabe26b:


    th "没有选择？那不可能啊!"


translate chinese day6_sl_e89a598d:


    th "我才是那个没有选择的人—坐在这里干等着出现一个奇迹把我带回原来的世界。"


translate chinese day6_sl_63017633:


    th "斯拉维娅和我有什么共同点吗？她是一个出色的少先队员，聪明又漂亮。"


translate chinese day6_sl_c7da1f43:


    "她有成百上千的选择!"


translate chinese day6_sl_db88a5ef:


    th "也许这就是我一直以来的梦想—一个美丽的女孩子，不仅做在我的旁边和我愉快的聊天，而且还完全指望着我!"


translate chinese day6_sl_a49c6922:


    th "但是这是好事还是坏事？我接下来应该怎么办？"


translate chinese day6_sl_18e9a332:


    "问题远比答案多。"


translate chinese day6_sl_9a4e7e0b:


    "我连担起自己的责任都做不到，还如何担起我们两个人的责任...?"


translate chinese day6_sl_afb4e267:


    "过去我宁可自己躲起来等着一切事情变好。"


translate chinese day6_sl_3057669a:


    "但是现在我不能这么做。"


translate chinese day6_sl_d1c57701:


    "不只是因为只有天知道我现在在什么地方，什么时间。"


translate chinese day6_sl_2b586af9:


    me "你应该知道，我可能是错的。"


translate chinese day6_sl_0fa153ce_1:


    "斯拉维娅疑惑地看着我。"


translate chinese day6_sl_3b122786:


    me "咱们坐在这里什么也解决不了，你说得对，这样太愚蠢，太孩子气了"


translate chinese day6_sl_82f6ffb1:


    sl "那么咱们应该怎么办?"


translate chinese day6_sl_b749d8c0_1:


    me "咱们走吧!"


translate chinese day6_sl_91946be4:


    "我们站起来，我牵着斯拉维娅的手，一边拿着睡袋准备返回营地。"


translate chinese day6_sl_98346aa3:


    "很快我们回到了广场，奥尔加·德米特里耶夫娜站在那里好像在等着我们。"


translate chinese day6_sl_57e9c38f:


    mt "好久不见!"


translate chinese day6_sl_7b0b0261:


    "她愤怒的说道。"


translate chinese day6_sl_bee8d492:


    mt "你有没有什么要解释的?"


translate chinese day6_sl_a0bfe3f6:


    "我冷静的回答着。"


translate chinese day6_sl_ccda0fcf:


    "我讨厌的回答着。"


translate chinese day6_sl_7f368bf8:


    mt "哦，真的吗?{w} 好吧，你觉得这一切都很正常？很好..."


translate chinese day6_sl_7ab0fbc2:


    "她暂停了一会儿，又转向斯拉维娅。"


translate chinese day6_sl_b7ba9ce3:


    mt "回到你的房间去。我知道这件事不怪你。"


translate chinese day6_sl_e290e93e:


    "斯拉维娅没有移动，她只是低下了头，她和我拉手更用力了。"


translate chinese day6_sl_1ed1a34c:


    mt "斯拉维娅!"


translate chinese day6_sl_da106b03:


    me "如你所见，她不想走。"


translate chinese day6_sl_c53ecc7f:


    mt "你到底怎么勾引她的?"


translate chinese day6_sl_e5c8bdf9:


    me "天哪，我什么都没说!"


translate chinese day6_sl_262d7cae:


    "我开始不耐烦了。"


translate chinese day6_sl_57062f56:


    me "你为什么觉得我们做错了事？我们什么都没有做!"


translate chinese day6_sl_9b7d56c6:


    mt "好像我没有看见似的!"


translate chinese day6_sl_f0d6d1c8:


    "她奸笑着。"


translate chinese day6_sl_dcebf1f8:


    me "你看见什么了?"


translate chinese day6_sl_a5f17bb1:


    "她停顿了一会儿，然后又继续说道:"


translate chinese day6_sl_17e9592b:


    mt "我已经看见了足够的事情。"


translate chinese day6_sl_d3772757:


    me "哇，你真会总结！在NKVD工作?"


translate chinese day6_sl_4e67b316:


    mt "你不要这么无礼!"


translate chinese day6_sl_5f44af84:


    me "我还没开始想呢。"


translate chinese day6_sl_d808c8a3:


    "我讽刺着。"


translate chinese day6_sl_3e824ff1:


    mt "好了，咱们的对话结束了，我不想再进行这个无意义的辩论，你被禁足了，一直到活动结束为止。"


translate chinese day6_sl_34787a27:


    me "真的吗？那么你要怎么做到呢？"


translate chinese day6_sl_e8cb9456:


    mt "我要把你锁在房间里!"


translate chinese day6_sl_9f712cc6:


    me "如果我反抗呢?"


translate chinese day6_sl_af66aeeb:


    "奥尔加·德米特里耶夫娜惊讶的看着我。"


translate chinese day6_sl_aafc257a:


    "我刚刚意识到我们周围聚集了一大群人。"


translate chinese day6_sl_a198bf9e:


    "我可以看到人群中有列娜，阿丽夏还有乌里扬娜。"


translate chinese day6_sl_46cac45a:


    mt "你说“反抗”是什么意思?"


translate chinese day6_sl_e7f3c77f:


    "辅导员慢慢的说着。"


translate chinese day6_sl_563e55f9:


    me "就是字面上的意思。"


translate chinese day6_sl_3a35e669:


    mt "但是你不能这样做！一个真正的少先队员..."


translate chinese day6_sl_2f21154b:


    me "也就是说我不是真正的少先队员!"


translate chinese day6_sl_680ddca2:


    "奥尔加·德米特里耶夫娜停住了，好像正在整理她的思绪。"


translate chinese day6_sl_08cd4a35:


    mt "你觉得我不能强制你么?"


translate chinese day6_sl_832f577a:


    me "你要怎么做呢?"


translate chinese day6_sl_30d5768e:


    "我大声笑着。"


translate chinese day6_sl_b2ab1d1c:


    th "除了什么神奇的超自然力量意外，辅导员现在真的没有什么强制性的力量。"


translate chinese day6_sl_25d8873c:


    mt "你知道吗!?"


translate chinese day6_sl_ad02f53a:


    me "什么?"


translate chinese day6_sl_c051b56f:


    "奥尔加·德米特里耶夫娜奥尔加·德米特里耶夫娜好像已经明白自己输掉了，但是为了维护自己在营地里的声誉，还是不肯放弃。"


translate chinese day6_sl_91d81026:


    mt "我...要给你的学校写信，让你不能加入共青团，不能加入共产党!"


translate chinese day6_sl_4a0430d0:


    "我不知道该如何回应这么强大的威胁。"


translate chinese day6_sl_32847c55:


    mt "接受吧，你没有选择!"


translate chinese day6_sl_4f1d9777:


    "一只保持沉默的斯拉维娅，突然介入了我们的对话:"


translate chinese day6_sl_0fbed59a:


    sl "奥尔加·德米特里耶夫娜你为什么觉得自己是对的？为什么一切都要怪谢苗？到底发生了什么值得你这么做的事情？你究竟明不明白自己是为了什么惩罚我们？"


translate chinese day6_sl_63d41120:


    sl "而且如果是其他人站在我的位置，你会做出相同的反应吗?"


translate chinese day6_sl_d6544d57:


    mt "斯拉维娅，你应该知道..."


translate chinese day6_sl_133f5bcb:


    sl "不用说了，我都知道，我知道谢苗没有犯任何错误!我知道你总是带有偏见的指责他！没有任何理由就批评他，即使他已经很努力了！"


translate chinese day6_sl_e25671be:


    sl "虽然有的时候结果不是那么好，但是我知道他已经尽力了，你要因为他做了他自己就批评他吗?"


translate chinese day6_sl_9a7a49a4:


    "我完全被斯拉维娅的独白震慑到了，过了好久我都没有意识到她究竟说的是什么。"


translate chinese day6_sl_adaf79ec:


    "她的话都从我的耳边飞了过去。"


translate chinese day6_sl_c51c54a3:


    "她的声音可以让我睡着，把我带到远方，远离这个邪恶的女人，这个糟糕的夏令营，治愈我受伤的心灵。"


translate chinese day6_sl_90afe97b:


    mt "好吧，我不知道还能对你怎么办..."


translate chinese day6_sl_a9bda318:


    "奥尔加·德米特里耶夫娜好像冷静了一些。"


translate chinese day6_sl_8d10714b:


    mt "但是我不支持你的行为!"


translate chinese day6_sl_60c09bca:


    "她转过身，穿过人群，返回了她的房间。"


translate chinese day6_sl_64e8ffbe:


    "我期待着想电影中一样的掌声，但是并没有，人群开始逐渐散去，这件事似乎结束了。"


translate chinese day6_sl_b164096b:


    "乌里扬卡在向我狡猾的眨着眼。"


translate chinese day6_sl_3e600926:


    sl "那个，你知道吗，咱们似乎做的有点过了。"


translate chinese day6_sl_2ffca3fd:


    "我看着斯拉维娅，她正在笑着。"


translate chinese day6_sl_431c06db:


    me "没有，完全没有!{w} 你说得完全正确，要是我，我也会这样和她辩论到深夜，可是我思考了一个星期也没有相处合适的词语!"


translate chinese day6_sl_a441fb1c:


    sl "哎呀，算了啦..."


translate chinese day6_sl_d3368ac8:


    me "什么？那可是实话呀!"


translate chinese day6_sl_092ec533:


    "她盯着我的眼睛，笑了起来。"


translate chinese day6_sl_28b09c2f:


    sl "我只是本能的说出了心理想说的话。结果就是这样了。"


translate chinese day6_sl_2266dea3:


    me "第一感觉总是对的，有句话这么说。"


translate chinese day6_sl_e330b23b:


    sl "大概是这样的吧。"


translate chinese day6_sl_c13a9458:


    "我刚刚注意到我们还一直牵着手。"


translate chinese day6_sl_cc04a8ea:


    "我感觉有点尴尬，想要把手抽出来，但是斯拉维娅紧紧的抓着我。{w}这让我有点出冷汗。"


translate chinese day6_sl_0b0dc7e3:


    sl "咱们应该去哪里?"


translate chinese day6_sl_6ba416ef:


    "我可不想回到我的房间，说真的我完全不想在夏令营里待着。"


translate chinese day6_sl_e59ccd54:


    me "如果我说咱们返回到森林里去，你觉得怎么样呢?"


translate chinese day6_sl_21f85f08:


    "斯拉维娅惊讶的看着我。"


translate chinese day6_sl_54f96f7f:


    me "这个，你知道嘛，有些时候..."


translate chinese day6_sl_05b93dff:


    sl "好吧，咱们走吧!"


translate chinese day6_sl_1d885fd1:


    "我没有意料到她会这么快的答应，让我不知道怎么说完自己的话。"


translate chinese day6_sl_d574deca:


    "我正想跟她说说自己想了好久的事情—当你离开自己的舒适区的时候，至少持续一段时间。"


translate chinese day6_sl_59c2b914:


    "我以前经常这样做，去国外的地区，或者到其他的城市。"


translate chinese day6_sl_abd50e2b:


    "新的地点和景色让我知道自己的生活不仅是那间小公寓的四面墙壁。"


translate chinese day6_sl_e1e86b71:


    "还有比我做得更好或者更糟的人。"


translate chinese day6_sl_0d93e329:


    "这样我自己的问题就没有那么严重了。"


translate chinese day6_sl_b18f3f5e:


    "我突然笑了。"


translate chinese day6_sl_e07c37b3:


    sl "什么?"


translate chinese day6_sl_41274eff:


    me "没什么，我只是想起了...算了，咱们走吧!"


translate chinese day6_sl_3529c624:


    "我们就这样牵着手回到了森林。"


translate chinese day6_sl_359460da:


    "我们又来到了那块空地，似乎就是我们一个小时以前来过的，也许是另外一块，我看不出区别。"


translate chinese day6_sl_817547ad:


    sl "这里?"


translate chinese day6_sl_0fa153ce_2:


    "斯拉维娅疑惑地看着我。"


translate chinese day6_sl_e4bedbb9:


    me "不，咱们再往深处走吧。"


translate chinese day6_sl_af1d965e:


    sl "好的。"


translate chinese day6_sl_5261c24b:


    "又走了半个小时，我们来到了一个小湖边。"


translate chinese day6_sl_66a1624c:


    me "真是一个美丽的地方!"


translate chinese day6_sl_9bb4b00c:


    sl "你说得对..."


translate chinese day6_sl_3605a2b3:


    "斯拉维娅神秘的笑着。"


translate chinese day6_sl_b45d6866:


    me "那么我建议咱们就在这里休息吧!"


translate chinese day6_sl_e54e1256:


    sl "很好的地方。"


translate chinese day6_sl_4f7b9290:


    "我把睡袋放在地上，然后坐了下来。"


translate chinese day6_sl_ddd76d2d:


    "斯拉维娅也坐了下来。"


translate chinese day6_sl_b7cba703:


    me "咱们接下来要做什么?"


translate chinese day6_sl_48ee8384:


    sl "我不知道...是你说的要来森林..."


translate chinese day6_sl_21a837aa:


    me "是啊..."


translate chinese day6_sl_2e80f38f:


    th "我又要作出决定了，但是还完全不知道应该做什么好。"


translate chinese day6_sl_d3c3f841:


    th "我们可以聊天，但是谈什么呢...?"


translate chinese day6_sl_428107ab:


    sl "哎呀，别这么不好意思啊!"


translate chinese day6_sl_c954d69f_1:


    "斯拉维娅笑了。"


translate chinese day6_sl_66f97779:


    me "我没有…"


translate chinese day6_sl_033b4661:


    sl "我能看得出来!"


translate chinese day6_sl_a63fa39d:


    me "那个，只是因为..."


translate chinese day6_sl_7bf46dbb:


    sl "没关系。"


translate chinese day6_sl_0fa7e23d:


    me "听你的。"


translate chinese day6_sl_b9b338a4:


    "我叹叹气，看着脚下。"


translate chinese day6_sl_9269f3f7:


    sl "也许你可以讲讲你自己的故事?"


translate chinese day6_sl_59929532:


    me "那个，其实没有什么好讲的..."


translate chinese day6_sl_ac0560c7:


    sl "一个人的生活不可能什么有意思的事情都没有!"


translate chinese day6_sl_9eeceb0e:


    me "为什么呢？我就是典型的..."


translate chinese day6_sl_dec3abfe:


    sl "只是你..."


translate chinese day6_sl_60f3d758:


    "她犹豫了。"


translate chinese day6_sl_8e71c1af:


    sl "你是不是有什么事情不太想说?"


translate chinese day6_sl_8b677452:


    me "比如说什么?"


translate chinese day6_sl_762cefe1:


    sl "我不知道...{w}比如说你可能隐藏着一些什么?"


translate chinese day6_sl_869dccdf:


    me "我为什么要隐藏呢?"


translate chinese day6_sl_446d678d:


    sl "我怎么知道？你更明白嘛。"


translate chinese day6_sl_7dff98c4:


    me "那个{w}我也不…"


translate chinese day6_sl_3c9fa231:


    sl "你说得好像..."


translate chinese day6_sl_d2b04ecc:


    me "怎么?"


translate chinese day6_sl_94474f8e:


    sl "不对!"


translate chinese day6_sl_f7fcc72d:


    "斯拉维娅又笑了。"


translate chinese day6_sl_ee707346:


    "我确定除了她，别的人都不会这么冷静的。"


translate chinese day6_sl_2d42f5c8:


    me "好吧，所以你想让我谈谈什么呢?"


translate chinese day6_sl_50dc5f53:


    sl "嗯，跟我讲讲你的生活，任何有趣的事情都可以!"


translate chinese day6_sl_e9233663:


    "我现在可以确定自己的生活里没有任何有趣的事情。"


translate chinese day6_sl_dfb2a4e4:


    me "好吧，让我想想..."


translate chinese day6_sl_8a84040d:


    "我做了一个很聪明的表情。"


translate chinese day6_sl_40e09782:


    me "我还是小孩子的时候，大概五六岁吧，在我家乡下的房子边有一座小房子。"


translate chinese day6_sl_6642f6b7:


    me "木板墙，油毡瓦，打仗的时候可以当作防空洞。"


translate chinese day6_sl_23e95695:


    me "有一天我就和谁坐在这里，然后...掉了下来。"


translate chinese day6_sl_4ea8163e:


    me "后面还有很多床草莓。"


translate chinese day6_sl_78f3b806:


    me "就在我掉下来的时候我想到，死神找上我来了，我的人生在自己的眼前闪现，虽然只有六年，但是…"


translate chinese day6_sl_a269facd:


    me "所以我掉在这些床上，但是没有什么感觉，因为是软软的。"


translate chinese day6_sl_b0563737:


    me "这个故事就是这样..."


translate chinese day6_sl_c954d69f_2:


    "斯拉维娅笑了。"


translate chinese day6_sl_0ea44879:


    sl "但是那太傻了!"


translate chinese day6_sl_1c6992b3:


    me "我那时可不那么觉得。"


translate chinese day6_sl_320e2edd:


    sl "现在呢?"


translate chinese day6_sl_851543b3:


    me "我不知道...应该是吧..."


translate chinese day6_sl_3aa2017b:


    sl "好吧，再和我说说别的!"


translate chinese day6_sl_26ec147e:


    me "你还是会说我傻的..."


translate chinese day6_sl_e1a615ef:


    sl "这次不会了!"


translate chinese day6_sl_1cc1d82f:


    me "唔..."


translate chinese day6_sl_384065ad:


    "我又开始了思考。"


translate chinese day6_sl_d9e5ad4e:


    me "还有一次，我从一座桥上掉到了池塘里，差点淹死。"


translate chinese day6_sl_745d222d:


    sl "你的故事都很..."


translate chinese day6_sl_75767d26:


    "斯拉维娅有点生气的说道。"


translate chinese day6_sl_c0ec9074:


    me "我跟你说过了啦，没有什么可说的，所以我把第一个想到的故事就和你说了。"


translate chinese day6_sl_3eb56ac0:


    sl "啊，对了，说起来，湖!"


translate chinese day6_sl_b0c344be:


    "斯拉维娅看着水面。"


translate chinese day6_sl_3049af19:


    sl "咱们去游泳吧？"


translate chinese day6_sl_fcde635e:


    me "我不是太喜欢，而且我都没有带任何适合游泳的衣服。"


translate chinese day6_sl_81cd01eb:


    sl "快来吧!"


translate chinese day6_sl_6db70b0c:


    "她露出迷人的笑容。"


translate chinese day6_sl_e8fc1211:


    me "嗯...我不是很确定。"


translate chinese day6_sl_b7400b08:


    "当然我很想同意，但是我犹豫了。"


translate chinese day6_sl_72436a13:


    sl "来吧!"


translate chinese day6_sl_41c34d45:


    "斯拉维娅一边笑着一边往湖边跑去，同时解开上衣。"


translate chinese day6_sl_a62eafb3:


    me "嗯...唔..."


translate chinese day6_sl_5b6dae07:


    "我站起来往她的方向走。"


translate chinese day6_sl_e09166ac:


    "当我走近的时候，斯拉维娅已经在水中拍打着浪花，她所有的衣服摆在了岸上。"


translate chinese day6_sl_87701f3c:


    sl "过来!"


translate chinese day6_sl_7e260440:


    me "呃，那个...我不太确定..."


translate chinese day6_sl_5eca3ce7:


    "我嘀咕着。"


translate chinese day6_sl_12241ecd:


    sl "快来吧！很好玩!"


translate chinese day6_sl_6217020f:


    "她好像对我的存在一点不尴尬。"


translate chinese day6_sl_d8852a28:


    "我还想转过去来着。"


translate chinese day6_sl_6bf6e07b:


    th "问题是，到底怎样才是合适的？如果斯拉维娅觉得没关系而我觉得尴尬的话，会不会不太尊重她？"


translate chinese day6_sl_a7e3e304:


    th "但是那不是最重要的问题!"


translate chinese day6_sl_4fe83cce:


    me "我没有带我的泳装来..."


translate chinese day6_sl_96942526:


    "我觉得这不是最合适的理由。"


translate chinese day6_sl_cbfa9c42:


    sl "我也没有带呀…"


translate chinese day6_sl_475bb393:


    "斯拉维娅更轻的说着。"


translate chinese day6_sl_8160a6c3:


    sl "别害怕。"


translate chinese day6_sl_8cac9455:


    me "我没有！我没有什么好害怕的..."


translate chinese day6_sl_6dfc8e2e:


    sl "那就进来吧。"


translate chinese day6_sl_3f27c2c0:


    "我不知道该说什么。"


translate chinese day6_sl_1a6a0ccb:


    "我想要顺从，但是其他的一切都不是这样的，不是这样自发的。"


translate chinese day6_sl_3ef72e17:


    th "而且说真的，为什么斯拉维娅对我这么好?{w}还是另有隐情?{w} 那我真的不明白!"


translate chinese day6_sl_d88264e4:


    sl "你自己决定!"


translate chinese day6_sl_1e8379d1:


    "她失望的说着。"


translate chinese day6_sl_7cd8cd17:


    sl "那就去把火点着吧。"


translate chinese day6_sl_7332342b:


    me "为什么呢?"


translate chinese day6_sl_936f6483:


    sl "因为!{w}我出去的时候，需要把身上的水烤干啊!"


translate chinese day6_sl_6829db90:


    me "噢，对了...好的..."


translate chinese day6_sl_d608686c:


    "我迅速回到睡袋那里，在路上捡起各种树枝。"


translate chinese day6_sl_fb820884:


    "收集木头不是什么问题，但是要如何点着呢...?"


translate chinese day6_sl_c0609f2f:


    "我没有火柴也没有打火机。"


translate chinese day6_sl_eee42951:


    "我像其他上过学的成年人一样都知道用一根棍子摩擦另一根什么的可以打出火花，但是我从来没有试过。{w}好吧，我完全不会。"


translate chinese day6_sl_e5ff5614:


    "我开始用树枝慢慢的钻起来。"


translate chinese day6_sl_2723a1cb:


    "幸运的是，树枝很干。"


translate chinese day6_sl_27361633:


    "我本来很怀疑这样行不行，但是过了一会儿，一小缕烟雾升起，然后出现了小小的火苗，持续了一段时间。"


translate chinese day6_sl_f5d403da:


    "现在我至少知道这样是可以生火的。"


translate chinese day6_sl_75c51157:


    "我又继续折腾了二十来分钟，不过最终还是在睡袋旁边点起了一小团火焰。"


translate chinese day6_sl_b893bafe:


    "我慢慢的向里面添加着柴火。"


translate chinese day6_sl_b50189b8:


    "原来是这样的，人是可以适应任何环境的，所以也许我可以在林海雪原里幸存。"


translate chinese day6_sl_2545e174:


    "我还沉浸在自己关于生存的思考中的时候，斯拉维娅突然出现在了我的上方。"


translate chinese day6_sl_14c08f52:


    sl "哇！干得不错嘛。"


translate chinese day6_sl_ad95378a:


    "我抬起头，然后马上移开了视线。"


translate chinese day6_sl_190c6df5:


    "她穿着一件衬衫，但是湿湿的，所以几乎完全可见。"


translate chinese day6_sl_79ca2420:


    "在湖水中这不是什么问题，但是距离这么近就很不一样了。"


translate chinese day6_sl_9fe63324:


    sl "跟着你不会走丢。"


translate chinese day6_sl_e7783d47:


    "斯拉维娅坐下来，靠着我的肩膀。"


translate chinese day6_sl_12a92a61:


    sl "希望能尽快烤干。"


translate chinese day6_sl_4baac495:


    "不知道为什么我也这么觉得。"


translate chinese day6_sl_4166759d:


    sl "所以，还有什么?"


translate chinese day6_sl_ad02f53a_1:


    me "什么?"


translate chinese day6_sl_ce38d8f8:


    "我混乱的问着。"


translate chinese day6_sl_d43342bf:


    sl "再和我说说别的!"


translate chinese day6_sl_77005469:


    me "我真的不知道了..."


translate chinese day6_sl_f574621f:


    "我觉得有东西堵在了嗓子眼，肯定没法讲故事。"


translate chinese day6_sl_a1ced71a:


    sl "你突然变得好僵硬。"


translate chinese day6_sl_d82bbbfe:


    th "真的吗，为什么呢?"


translate chinese day6_sl_09834bc7:


    me "没什么，只是..."


translate chinese day6_sl_64fb8da4:


    "我觉得自己没法再这么平常的坐在这里了。."


translate chinese day6_sl_10e70ee5:


    "我的大脑被动物的本能所笼罩。"


translate chinese day6_sl_e909c247:


    me "你觉得...这很正常吗?"


translate chinese day6_sl_e07c37b3_1:


    sl "嗯?"


translate chinese day6_sl_877d42ed:


    me "这样坐在这里。"


translate chinese day6_sl_0da077c9:


    sl "这有什么关系?"


translate chinese day6_sl_b060c097:


    "看起来斯拉维娅完全不明白..."


translate chinese day6_sl_7d63ae9b:


    me "那个，你...你真的不明白...?"


translate chinese day6_sl_1fd5a2a5:


    "我无力的叹叹气。"


translate chinese day6_sl_86c2396f:


    sl "嗯...我不是很确定..."


translate chinese day6_sl_2295ea06:


    "她的声音听起来有点淘气。"


translate chinese day6_sl_ba65d0ea:


    sl "你怎么觉得?"


translate chinese day6_sl_a15c0346:


    me "那个，我...我..."


translate chinese day6_sl_75404f9d:


    sl "也许咱们没必要忍着?"


translate chinese day6_sl_b31684a0:


    "她的话在我的头脑中闪过。"


translate chinese day6_sl_69908825:


    th "她这是在允许我!"


translate chinese day6_sl_618577b1:


    "我们之间发生的美妙的事情，在六天前我还完全不敢想象。"


translate chinese day6_sl_f6734dd5:


    "也许这就是我来到这个夏令营的原因?"


translate chinese day6_sl_64e5abef:


    sl "我好开心!"


translate chinese day6_sl_9513cd87:


    me "嗯..."


translate chinese day6_sl_a2d3eaed:


    "我们就这样躺在一起呆了好久。"


translate chinese day6_sl_add6d7aa:


    sl "有点冷了。"


translate chinese day6_sl_62823e20:


    me "等一下。"


translate chinese day6_sl_3a7548c7:


    "我打开了睡袋，我们一起钻了进去。"


translate chinese day6_sl_d5fe4f59:


    "斯拉维娅把头放在我的肩膀上。"


translate chinese day6_sl_0cc68dfe:


    sl "我可以打个盹吗，我感觉很累..."


translate chinese day6_sl_3b03af3a:


    me "当然..."


translate chinese day6_sl_755c0af5:


    "很快她的呼吸变得平稳。"


translate chinese day6_sl_3634f3f3:


    "微风吹动着我们周围的树叶，身旁的草丛在不停摇动着，头顶上小小的飞虫时不时的环绕着。"


translate chinese day6_sl_0ece9306:


    "突然我意识到这是我这辈子见到的最美丽的景色。"


translate chinese day6_sl_b249ce58:


    "我一边想着一边进入了梦乡。"


translate chinese day6_sl_ce617998:


    "…"


translate chinese day6_sl_a20cefa7_2:


    "..."


translate chinese day6_sl_dcbe279a:


    "我睁开眼睛，看到橡树的树冠呼啸着。"


translate chinese day6_sl_4ec20237:


    "一阵寒风冻到刺骨。"


translate chinese day6_sl_35f2530a:


    "一轮满月凝视着我—夜幕已经降临。"


translate chinese day6_sl_b7d4386e:


    "我说不清现在是什么时间，不过那并不重要。"


translate chinese day6_sl_41242fed:


    "斯拉维娅还在熟睡着。"


translate chinese day6_sl_3df19969:


    "我把睡袋小心的向上提了提，帮她盖好。然后穿上衣服站了起来。"


translate chinese day6_sl_5666a2a3:


    "我很渴，现在也没有别的选项。"


translate chinese day6_sl_7a95c9e6:


    th "而且我感觉这里的河水应该没有毒。"


translate chinese day6_sl_1829982a:


    th "我们并不在切尔诺贝利附近。"


translate chinese day6_sl_fab822c4:


    th "至少我是乐意这样相信的。"


translate chinese day6_sl_45db8adc:


    "我捧起河水，把脸埋进去。"


translate chinese day6_sl_c60407cd:


    "很好喝，就像是泉水，而不是河水。"


translate chinese day6_sl_1f3eb0be:


    "河水中，我的倒影被月光照得亮亮的。"


translate chinese day6_sl_023f70e4:


    "看起来好像在微笑。"


translate chinese day6_sl_342dc487:


    th "奇怪，我都没有注意到..."


translate chinese day6_sl_0a38142d:


    "心满意足以后，我往回走着，打算一觉睡到天亮..."


translate chinese day6_sl_855eb22e:


    "...然而突然附近有树枝折断的声音。"


translate chinese day6_sl_f17137d4:


    "我转过身看看发生了什么，看到了树后面好像有人。"


translate chinese day6_sl_1d2d3e1b:


    me "谁在那里?"


translate chinese day6_sl_323cb2fa:


    "我担心的问着。"


translate chinese day6_sl_3a866645:


    FIXME_voice "没必要那样喊。"


translate chinese day6_sl_c555bb93:


    "这声音听起来很熟悉。"


translate chinese day6_sl_7ccaacb2:


    "我走近一步，看到了列娜。"


translate chinese day6_sl_2821e6eb:


    me "你在这里做什么?"


translate chinese day6_sl_8543722f:


    un "散步。"


translate chinese day6_sl_2864c058:


    me "这个时候?!"


translate chinese day6_sl_920912b6:


    un "怎么，不行吗?"


translate chinese day6_sl_b9c06d63:


    me "不，但是太奇怪了。"


translate chinese day6_sl_28dbd9ca:


    "我靠在树上打量着她。"


translate chinese day6_sl_2d954428:


    th "她来到这里是有原因的。"


translate chinese day6_sl_a5f26668:


    th "而且我们勉强才走到这里，要是“散步”不太可能找到这个地方!"


translate chinese day6_sl_9c114242:


    th "而且即使是在晚上也能清晰地看见斯拉维娅在几步之外睡着..."


translate chinese day6_sl_f95c9fc9:


    me "然后呢?"


translate chinese day6_sl_3875ba7b:


    un "什么?"


translate chinese day6_sl_35f65f56:


    me "你不是要说些什么吗?"


translate chinese day6_sl_38f1c7fb:


    un "我应该说吗?"


translate chinese day6_sl_87e50db3:


    me "这要看你了。"


translate chinese day6_sl_4b0db65e:


    un "我不明白你在说什么。"


translate chinese day6_sl_ffb6e8f5:


    me "好吧...{w}你在这里呆了一段时间了吗?"


translate chinese day6_sl_4ae58fde:


    un "嗯，没那么长时间..."


translate chinese day6_sl_8b16db24:


    me "是多长呢?"


translate chinese day6_sl_dc72cd9c:


    un "不长!"


translate chinese day6_sl_2406c221:


    "列娜强烈的回应着。"


translate chinese day6_sl_93ecd6c4:


    me "好吧...{w}就这些了吗?"


translate chinese day6_sl_0da946d4:


    un "我不知道。"


translate chinese day6_sl_e7e5f09c:


    "我们静静的站了一会儿。"


translate chinese day6_sl_1350bc39:


    un "也许你该回去，斯拉维娅会冻着的。"


translate chinese day6_sl_65796b62:


    me "所以你看见了?"


translate chinese day6_sl_2fe9320f:


    un "要想不看见很难。"


translate chinese day6_sl_bd831b71:


    me "如果你觉得..."


translate chinese day6_sl_17b466c5:


    un "我应该这么觉得，对吧？我看这已经很清楚了吧。"


translate chinese day6_sl_8c370f90:


    me "大概是吧...{w}所以你不再说点什么了吗?"


translate chinese day6_sl_1ab636d8:


    un "还要说什么?{w}经过了今天你在广场的表演，已经不必再说些什么了。"


translate chinese day6_sl_d7ec763f:


    me "那不是表演...{w}我们只是..."


translate chinese day6_sl_e767eb14:


    th "我好像在向她辩解。"


translate chinese day6_sl_5e5d8d97:


    me "反正也和你没有关系!"


translate chinese day6_sl_80519381:


    un "是啊。{w}这是你自己的事。{w}你只要小心一点就行了。"


translate chinese day6_sl_1084810b:


    me "什么意思?"


translate chinese day6_sl_f1725204:


    un "如果一切都来得太容易的话，你可能不会珍惜，不过你不应该那样。"


translate chinese day6_sl_787e2d21:


    me "我没明白。"


translate chinese day6_sl_30f3651c:


    un "嗯，比如说斯拉维娅，这次很神奇，哈...然后你一次又一次的重复，然后你就厌烦了。"


translate chinese day6_sl_f14d3856:


    "列娜停顿了一下。"


translate chinese day6_sl_9e6298f4:


    un "我就是这个意思，你要懂得珍惜，即使你毫不费力的得到她。"


translate chinese day6_sl_6d7c4722:


    me "如果你觉得我只是玩玩的话，你就错了。"


translate chinese day6_sl_44fccd69:


    un "我没有那么想，但是那是有可能的。"


translate chinese day6_sl_2b60d758:


    me "再说了!{w}你是不是不应该再插手别人的事情了?"


translate chinese day6_sl_356d8152:


    un "我只是给你建议。"


translate chinese day6_sl_94c6774e:


    me "我好像没有跟你要过。"


translate chinese day6_sl_7967d505:


    un "我觉得你应该对你正在做的事情以及未来的计划有更多的自觉，这样以后就不会被悔恨所折磨。"


translate chinese day6_sl_54eac6ad:


    me "我说，你现在越来越像是威胁我!"


translate chinese day6_sl_12de8fbf:


    th "嗯，就是..."


translate chinese day6_sl_5361e5b6:


    un "嗯，也许是吧..."


translate chinese day6_sl_71411d8c:


    "列娜神秘的说着。"


translate chinese day6_sl_766131be:


    me "所以你..."


translate chinese day6_sl_cad298e3:


    un "不，完全没有..."


translate chinese day6_sl_d8b4d5a6:


    un "准确的说，不是现在。"


translate chinese day6_sl_7dd3d65d:


    "她悄悄地说着。"


translate chinese day6_sl_51cf710a:


    "列娜消失在了森林的深处。"


translate chinese day6_sl_5f2d5d23:


    "我没有阻止她。"


translate chinese day6_sl_a4493a46:


    "她走远的时候，我发现她的手中有什么东西闪亮着月光。"


translate chinese day6_sl_77992443:


    th "真是一个奇怪的女孩..."


translate chinese day6_sl_4850ebe2:


    "我叹了口气，回去找斯拉维娅。"


translate chinese day6_sl_bd5a6473:


    "她温暖的身体挡住了寒冷的黑夜。"


translate chinese day6_sl_8fbd4ba7:


    "希望我也让斯拉维娅暖和起来了..."


translate chinese day6_dv_c15cbee2:


    "我醒的很早，奥尔加·德米特里耶夫娜还在睡觉，我伸了个懒腰，坐了起来。"


translate chinese day6_dv_4f2d9d90:


    "神奇的是我感觉还不错，虽然从理论上来讲经过了昨天的时间我今天睡一整天都不为过。"


translate chinese day6_dv_2702e092:


    "为了不惊醒辅导员，我拿好自己的洗漱装备，悄悄地走出了房门。"


translate chinese day6_dv_0cbfa63c:


    "我忘了看表，不过看起来似乎是早晨六七点钟。"


translate chinese day6_dv_1e1204ff:


    "整个夏令营还在沉睡。"


translate chinese day6_dv_1857932a:


    "夜间的露水从草丛上滑落，地表弥漫着一层雾气，太阳刚刚穿过树顶，还完全感觉不到暖意，就像是我家对面的那盏荧光灯。"


translate chinese day6_dv_62f242c5:


    "我深吸了一口凉凉的空气，心想即使是我这样的夜猫子有时候也是可以享受一下清晨的。"


translate chinese day6_dv_1fc263e9:


    "营地的“街道”当然是空空的。"


translate chinese day6_dv_6b08ba9d:


    "这里的水总是寒冷刺骨，但是今天就像是从南极运来的似的，{w}我的手完全失去知觉了..."


translate chinese day6_dv_07d87721:


    "结束了卫生项目之后我决定在附近散散步。"


translate chinese day6_dv_c8815fd4:


    th "奥尔加·德米特里耶夫娜还在睡觉，早饭还没准备好，所以没什么可干的..."


translate chinese day6_dv_e44d834c:


    "我来到广场，看着Genda。"


translate chinese day6_dv_37cdd1ec:


    th "那个人不管白天晚上都在那里——站岗!"


translate chinese day6_dv_4b83221f:


    "我对自己的笑话很满意，然后动身前往船坞。"


translate chinese day6_dv_714f4f97:


    "我一边走着一边哼着歌，我很确定自己是唯一一个醒着的人。"


translate chinese day6_dv_24dad558:


    "让我惊讶的是，码头那里有人，把脚放在水里悠闲的坐着。"


translate chinese day6_dv_b284c8b7:


    "阿丽夏。"


translate chinese day6_dv_b3358970:


    "我走上前去，站住，不知道该说什么。"


translate chinese day6_dv_31a54a49:


    "然后她突然转过来对我说。"


translate chinese day6_dv_39426f9a:


    dv "在这里站了很长时间了吗?"


translate chinese day6_dv_5cb822ee:


    me "没..."


translate chinese day6_dv_6373b237:


    dv "你为什么没有睡觉?"


translate chinese day6_dv_b78c6e26:


    me "那你呢?"


translate chinese day6_dv_bc3de2ca:


    "她转了回去，继续盯着河水。"


translate chinese day6_dv_48bb9e3b:


    th "看来我不是今天唯一一个心情很好的人。"


translate chinese day6_dv_ed9a68b7:


    "至少她今天不是那么无礼。"


translate chinese day6_dv_186c0eae:


    dv "睡不着。"


translate chinese day6_dv_f2e007f0:


    me "嗯..."


translate chinese day6_dv_c203a783:


    dv "昨天...{w} 你都搞定了吧?"


translate chinese day6_dv_ae56873f:


    me "你指的什么?"


translate chinese day6_dv_5a2cc2c6:


    dv "天啊!"


translate chinese day6_dv_0239f53c:


    th "我的回答似乎不太算数。"


translate chinese day6_dv_f6787280:


    me "好吧，是的..."


translate chinese day6_dv_bc2b8f99:


    "我撒谎了。"


translate chinese day6_dv_db8773f6:


    dv "好吧...很好..."


translate chinese day6_dv_c8722ed0:


    "她突然盯着我的眼睛笑了。"


translate chinese day6_dv_114bfae2:


    dv "那么我今天为你订好了计划!"


translate chinese day6_dv_0b273a12:


    me "呃?{w}我能不能问问是什么?"


translate chinese day6_dv_d0e6c06e:


    dv "你不必知道!"


translate chinese day6_dv_0ba7958b:


    me "那个，既然我是其中的一部分..."


translate chinese day6_dv_0ac4aace:


    dv "到时候你会知道的。"


translate chinese day6_dv_67da3356:


    me "至少给我一点提示行不行?"


translate chinese day6_dv_9f6452c6:


    dv "不行!"


translate chinese day6_dv_58acf86d:


    "她站起来，向我走过来。"


translate chinese day6_dv_7c3046d7:


    "我有点害怕，让出了路。"


translate chinese day6_dv_225e8e60:


    dv "别做傻事!"


translate chinese day6_dv_3af4c20c:


    "阿丽夏一边说着一边跑开了。"


translate chinese day6_dv_f1425e57:


    th "我很奇怪她是什么意思？还有她到底有什么计划...?"


translate chinese day6_dv_544d6ac4:


    "我不想成为炸毁雕塑恐怖分子中的一员..."


translate chinese day6_dv_9ede4c1b:


    "而且昨天在岛上和列娜对话之后，我仍然心神不宁。"


translate chinese day6_dv_8241f1ad:


    th "她可能还是很生气，而且我没能跟她解释清楚。"


translate chinese day6_dv_4184783d:


    th "虽然就算是给我机会我可能也说不出个所以然。"


translate chinese day6_dv_0566e7d0:


    "我对一切都感到愧疚，但是又说不清具体是什么。"


translate chinese day6_dv_7ea39484:


    "夏令营的人们快要醒来了..."


translate chinese day6_dv_9af1e38e:


    "距离早餐至少还有一个小时，但是我完全不想回到辅导员的宿舍，所以我就在食堂这里等着。"


translate chinese day6_dv_c58fe4ea:


    th "至少这一次我能第一个冲进去!"


translate chinese day6_dv_25d2dc92:


    "我坐在台阶上看着太阳升起。"


translate chinese day6_dv_75d19de5:


    "有时候看着新的一天来临也是一件幸福的事。"


translate chinese day6_dv_cd46fe4c:


    "我现在心境祥和......没有了平常那些让自己心烦意乱的事情。"


translate chinese day6_dv_743da966:


    "我就这样沐浴着天边的星星的光芒。"


translate chinese day6_dv_cfe6a6d0:


    "在别的情况下我甚至会说自己很开心。"


translate chinese day6_dv_5492e5b8:


    un "早上好！你今天起的很早啊..."


translate chinese day6_dv_3edc6eb9:


    "我抬起头看到列娜站在我的面前，笑着。"


translate chinese day6_dv_0c9902bf:


    me "你也是啊..."


translate chinese day6_dv_f6ff66c7:


    "昨天晚上的事情在我眼前闪过。"


translate chinese day6_dv_17717ee8:


    me "我说..."


translate chinese day6_dv_5aa4cd1c:


    un "如果是关于昨天的事情，就请不要说了!"


translate chinese day6_dv_cd5b1943:


    "她强硬的打断了我。"


translate chinese day6_dv_54f0d7cf:


    un "我不知道自己怎么了...那完全是...都忘掉吧!"


translate chinese day6_dv_8fb93608:


    "她看着我。"


translate chinese day6_dv_b9aa504e:


    "列娜比平常看起来自信的多。"


translate chinese day6_dv_0b610250:


    me "要是有那么简单就好了..."


translate chinese day6_dv_96b90470:


    un "这没有什么复杂的!"


translate chinese day6_dv_1a20f786:


    "她坐在我旁边。"


translate chinese day6_dv_6fd2c686:


    "我们离得太近了，让我有一点尴尬，但是并不像挪开。"


translate chinese day6_dv_b6d571c6:


    un "我有时会和阿丽夏吵架。"


translate chinese day6_dv_77ff46df:


    th "是啊，我明白。"


translate chinese day6_dv_025de025:


    me "我明白...{w}但是这次，是因为我。"


translate chinese day6_dv_a9702184:


    un "谁知道呢..."


translate chinese day6_dv_069a2ec8:


    "她回答完陷入了沉思。"


translate chinese day6_dv_a4255212:


    me "所以说...那个，...我感觉很尴尬。"


translate chinese day6_dv_99d1db6f:


    me "也许真的就是我的责任...{w}或者说，我不是想推脱责任..."


translate chinese day6_dv_5d2a7a44:


    "我就像是一个向老师辩解的小学生。"


translate chinese day6_dv_b0c06763:


    un "好吧，别这样！就像是我说的，没有什么严重的问题!"


translate chinese day6_dv_bd04f9b5:


    me "如果你这么说的话..."


translate chinese day6_dv_bd742d8d:


    un "你更应该想想从现在开始应该怎么做..."


translate chinese day6_dv_dc477843:


    me "不能重蹈覆辙!"


translate chinese day6_dv_9b902801:


    "我捂住脸，表现出一副迷惑不解的样子，还有感觉很不舒服。"


translate chinese day6_dv_bc39bcd1:


    me "你能不能解释得更清楚一些?"


translate chinese day6_dv_41cfffd5:


    un "为什么?"


translate chinese day6_dv_67a6d56e:


    "她推脱道。"


translate chinese day6_dv_ebf71d2d:


    me "你看，如果我不知道自己到底哪里做错了的话，我是没有办法改正的!{w}我没办法判断怎样按照你说的做。"


translate chinese day6_dv_f2f9031f:


    un "我觉得你会明白的..."


translate chinese day6_dv_86da72f4:


    me "我是很愿意相信，但是..."


translate chinese day6_dv_27475010:


    "食堂的大门突然砰地打开了，奥尔加·德米特里耶夫娜从里面冲了出来。"


translate chinese day6_dv_03906774:


    mt "这么早就饿了?"


translate chinese day6_dv_759d5254:


    "她开玩笑的说着。"


translate chinese day6_dv_5567bace:


    un "早上好!"


translate chinese day6_dv_4fb197ff:


    "列娜愉快地说着。她站起来向里走着。"


translate chinese day6_dv_35698e3a:


    un "走吧?"


translate chinese day6_dv_1c08f469:


    me "嗯，嗯..."


translate chinese day6_dv_d2b198a7:


    "我们还没说完，但是少先队员们不知道从哪里冲出来把我们淹没了。等我取到自己的早餐的时候，列娜已经坐了下来，周围还有未来、热妮娅和斯拉维娅。悲剧的是，他们的桌子没有多余的椅子了。"


translate chinese day6_dv_b59dc367:


    "我感觉很失落，故意没有和大家问好，然后走向了自己最爱的小角落。"


translate chinese day6_dv_64b85b7a:


    "我吃着自己的麦片粥（今天有些嚼不烂），用凉茶冲下去，然后开始看着其他人。..."


translate chinese day6_dv_9224c445:


    "似乎所有的少先队员都在这里了。"


translate chinese day6_dv_4b50e0bf:


    "有些人在聊天，还有些，像电子小子和舒里克，在专心的咀嚼着，还有的在到处张望，就像我。"


translate chinese day6_dv_3ddb5562:


    "我正准备离开，突然乌里扬卡冲到我的桌子前，端着她满满的一盘食物。"


translate chinese day6_dv_0a9f12c1:


    us "吃好!"


translate chinese day6_dv_eb56d1ba:


    "她嘟囔着。"


translate chinese day6_dv_ceffc1ce:


    me "又是偷来的?"


translate chinese day6_dv_25072178:


    "我盯着她的盘子问道。"


translate chinese day6_dv_93b62c6f:


    us "正在成长的身体需要很多能量!"


translate chinese day6_dv_3c9d580c:


    me "是啊，你是应该长一长..."


translate chinese day6_dv_9db6b90d:


    "我嘀咕着。"


translate chinese day6_dv_b7ef3111:


    us "什么?"


translate chinese day6_dv_19ce3442:


    "我觉得她应该没有听到。"


translate chinese day6_dv_84662d7a:


    me "没什么..."


translate chinese day6_dv_75e20332:


    "我往外走。"


translate chinese day6_dv_a2b6719e:


    us "这么快就要走了吗?"


translate chinese day6_dv_afe56f0b:


    "她沮丧的喊着。{w}我没有回答。"


translate chinese day6_dv_2f41a894:


    "我应该把洗漱工具放回去，我不想一整天带着。"


translate chinese day6_dv_8dc289f1:


    th "话说，我今天有什么事可干?"


translate chinese day6_dv_ae9fb3ad:


    "我什么也想不到。"


translate chinese day6_dv_409f5623:


    "我今天的心情感觉有人要突然出现邀请我去旅行。"


translate chinese day6_dv_f321b160:


    "我离开了辅导员的宿舍，正踌躇的时候，听到了一个声音在叫我。"


translate chinese day6_dv_2a454c26:


    sl "谢苗!"


translate chinese day6_dv_c50dbd79:


    "是斯拉维娅。"


translate chinese day6_dv_8543a313:


    sl "忙吗?"


translate chinese day6_dv_5cb822ee_1:


    me "没有..."


translate chinese day6_dv_0b8f24fb:


    sl "你可以帮帮我吗?"


translate chinese day6_dv_3df40f08:


    th "当然..."


translate chinese day6_dv_3e0102a8:


    "在这么好的日子里我可不想再去寻找什么答案了。"


translate chinese day6_dv_32de2538:


    th "花费几个小时也不会对我的处境有多大改变。"


translate chinese day6_dv_3eeb5def:


    me "什么事?"


translate chinese day6_dv_ac32d9f9:


    sl "你应该去机器人社团看看...我不确定到底是怎么回事，他们会跟你解释清楚的。"


translate chinese day6_dv_6954719a:


    th "又是那些悲剧的发明家..."


translate chinese day6_dv_65bf82fc:


    me "好吧..."


translate chinese day6_dv_309686e8:


    sl "感谢!"


translate chinese day6_dv_0167daaa:


    "她开心地笑着，然后跑开了。"


translate chinese day6_dv_dd9995b4:


    "所以说我旅行的重点是充满谎言的敌人，邪恶的电子小子博士，还有恶魔教授舒里克。{w} 化身博士海德先生..."


translate chinese day6_dv_1ad8199f:


    "我坚定地推开门走了进去。"


translate chinese day6_dv_45391550:


    el "谢苗..."


translate chinese day6_dv_1d3a15b7:


    "电子小子有点不舒服的说道，放下自己正在用改锥拧着的神奇机器，抬头看着我。"


translate chinese day6_dv_81505d19:


    me "斯拉维娅派我来的。"


translate chinese day6_dv_e75769fe:


    el "我知道。"


translate chinese day6_dv_f95c9fc9:


    me "然后呢?"


translate chinese day6_dv_772dd018:


    el "什么?"


translate chinese day6_dv_7c46f700:


    "第二扇门打开了，舒里克走了进来。"


translate chinese day6_dv_839ded9e:


    sh "谢苗。"


translate chinese day6_dv_cf64a48c:


    "他用和舒里克一样的风格打着招呼。"


translate chinese day6_dv_1e1be213:


    me "从早晨就是我自己..."


translate chinese day6_dv_27c62366:


    "我开着玩笑。"


translate chinese day6_dv_ccd5a1e0:


    me "所以说你们需要我做什么?"


translate chinese day6_dv_782ffade:


    el "我们有一项重要的任务要交给你!"


translate chinese day6_dv_a2516dfe:


    me "我正听着呢。"


translate chinese day6_dv_74f9f6cb:


    sh "昨天护士来了。"


translate chinese day6_dv_c8c1467c:


    "他停了下来，好像不想继续说了一样。"


translate chinese day6_dv_f95c9fc9_1:


    me "所以?"


translate chinese day6_dv_58c13b19:


    "我谨慎的问着。"


translate chinese day6_dv_cce022c6:


    el "所以我们有一项重要的任务要交给你。"


translate chinese day6_dv_63eb63d4:


    me "那个我已经知道了..."


translate chinese day6_dv_c8416a10:


    sh "你需要交给她一个包裹。"


translate chinese day6_dv_0997405a:


    me "你们自己搞不定吗？医务室就在那里。"


translate chinese day6_dv_c40a9bb9:


    "我讽刺着他们。"


translate chinese day6_dv_b6cbf345:


    el "不行的。"


translate chinese day6_dv_47d017ef:


    "电子小子惊讶的说着。"


translate chinese day6_dv_379f1dd8:


    th "真是一对奇怪的夫妇。"


translate chinese day6_dv_11a96bf4:


    me "我明白了...{w}不会又是一个那么重的大袋子吧?"


translate chinese day6_dv_f1caa021:


    el "不，不，不是那样的!"


translate chinese day6_dv_f4166d68:


    "他笑了。"


translate chinese day6_dv_0031a3ab:


    me "好吧..."


translate chinese day6_dv_1888335c:


    "我小心的同意了。"


translate chinese day6_dv_3a378445:


    sh "等一下。"


translate chinese day6_dv_4ef4398f:


    "舒里克消失在了门那边，然后很快拿着一个小纸包走了出来。"


translate chinese day6_dv_b9fafe93:


    sh "小心一点。"


translate chinese day6_dv_d9c0add8:


    me "好吧，好吧。"


translate chinese day6_dv_abb01373:


    "我拿起包裹，感觉他们不是在移交天国之门的钥匙就是在传递奥运火炬。"


translate chinese day6_dv_c3714a4b:


    me "我可以走了吗?"


translate chinese day6_dv_89559a2a:


    el "当然。"


translate chinese day6_dv_a193f014:


    "电子小子很吓人的说着。"


translate chinese day6_dv_98287c05:


    sh "只要小心一点!"


translate chinese day6_dv_d7cdf7b2:


    "舒里克央求着。"


translate chinese day6_dv_9dd454a1:


    "我走出来仔细的检查着这个包裹。"


translate chinese day6_dv_e18730b6:


    "他们没说不让我打开。"


translate chinese day6_dv_2c50bd85:


    th "虽然说要这么做的话还是隐蔽一点好。"


translate chinese day6_dv_42b9bac1:


    "我快速走到广场，然后在确定没有人跟踪我之后，前往了森林。"


translate chinese day6_dv_32712073:


    "我犹豫不决的站了一会儿，盯着这个包裹。"


translate chinese day6_dv_76fdd246:


    th "我很奇怪这里面到底是什么，什么那么重要?{w}他们为什么自己不能送去?"


translate chinese day6_dv_b468a82a:


    "我有很多疑问。"


translate chinese day6_dv_fb32ffc9:


    "我打开了包裹，看到了一瓶“红牌伏特加”。"


translate chinese day6_dv_420166b4:


    th "这有什么啊，少先队员们给护士带去一瓶伏特加。{w}或者说是我干的，但是这很正常啊。"


translate chinese day6_dv_87a11b40:


    "我把瓶子包回去，然后挠挠头。"


translate chinese day6_dv_d78595da:


    th "这不是奇怪，这是愚蠢。"


translate chinese day6_dv_72e21fc3:


    th "让我想不到什么不脑残的解释。"


translate chinese day6_dv_93a5c183:


    "我又站了一会儿，心想不能放过这么好的一个机会，于是往辅导员的宿舍走去。"


translate chinese day6_dv_521a35e9:


    "还好，奥尔加·德米特里耶夫娜不在，我悄悄走了进去，然后翻动着抽屉里的东西。"


translate chinese day6_dv_e0aa3f01:


    "很快伏特加被灌到了水瓶里，里面本来装着浇花的水。"


translate chinese day6_dv_089fc8e9:


    "计划成功以后我蹦蹦跳跳的走向医务室。"


translate chinese day6_dv_1e5e0588:


    th "这有什么错误？他们差点坑了我！如果让辅导员看见..."


translate chinese day6_dv_ef0f0a3b:


    th "还有其他人吗?"


translate chinese day6_dv_2fc32561:


    th "我自己也可以做到的..."


translate chinese day6_dv_cc444139:


    th "所以说绝对没什么问题!"


translate chinese day6_dv_c8139598:


    "我用上全身的力气敲响医务室的大门。"


translate chinese day6_dv_5d7721f0:


    cs "你好...少先队员!"


translate chinese day6_dv_99266ac9:


    me "你好! 我拿来了一个包裹，给你的储备...呃...放到你的橱柜。"


translate chinese day6_dv_a2debb84:


    "我有点过于欣快，已经开始语无伦次了。"


translate chinese day6_dv_a9d81cd2:


    cs "啊..."


translate chinese day6_dv_c2732a68:


    "她说着。"


translate chinese day6_dv_7d07968d:


    cs "递给我。"


translate chinese day6_dv_aecf76f3:


    "我把包裹递给她。"


translate chinese day6_dv_b68868ff:


    "护士看都没看就塞到了抽屉里。"


translate chinese day6_dv_bda69ee6:


    cs "你没有打开过吧?"


translate chinese day6_dv_f837dd45:


    me "当然没有!"


translate chinese day6_dv_3d78efca:


    "我脱口而出。"


translate chinese day6_dv_aedf1019:


    cs "好孩子。"


translate chinese day6_dv_76aba977:


    me "好吧，那么我要..."


translate chinese day6_dv_40e2f3e6:


    cs "走吧，走吧...少先队员."


translate chinese day6_dv_4aaea0be:


    "我关上门，叹了口气。"


translate chinese day6_dv_23623d22:


    th "我可以用最简单的方式缓解压力，我反正可以很容易的把责任推到那两个疯狂发明家身上!"


translate chinese day6_dv_0cd6a776:


    "我来到了广场。表针指向正午，这意味着午餐。"


translate chinese day6_dv_e4fa05b8:


    "我坐在长椅上，决定铃声不响坚决不动。"


translate chinese day6_dv_c6cc3d58:


    "少先队员们从我身边跑过，谈笑风生。"


translate chinese day6_dv_94906696:


    th "是啊，这真是一个好天气。"


translate chinese day6_dv_cdd7919b:


    "我看看广场那边，列娜正向我走来。"


translate chinese day6_dv_f3d744ae:


    un "正在休息吗?"


translate chinese day6_dv_6b635703:


    me "大概是这样的..."


translate chinese day6_dv_b8aa5db1:


    "她坐了下来，看着天空。"


translate chinese day6_dv_2c5438ac:


    un "天气真不错，是不是啊?"


translate chinese day6_dv_31663314:


    me "是啊..."


translate chinese day6_dv_fde4d309:


    "有一点尴尬，列娜和平常不太一样，她不是那么害羞，她脸也没有变红，也很有自信。"


translate chinese day6_dv_c20baf86:


    me "所以说当时的对话..."


translate chinese day6_dv_8891daf5:


    un "我已经把一切都告诉你了。"


translate chinese day6_dv_8cd8b52e:


    me "是啊，但是..."


translate chinese day6_dv_3cd3c424:


    un "忘了吧。"


translate chinese day6_dv_48f1666a:


    me "如果你一定要这么说的话..."


translate chinese day6_dv_a4f042ce:


    "我们又安静的坐了一会儿。"


translate chinese day6_dv_2c070d08:


    me "可是我做不到..."


translate chinese day6_dv_dd328974:


    un "那么对于阿丽夏，你怎么看?"


translate chinese day6_dv_d22f0a40:


    "她忽略掉我的话，继续问道。"


translate chinese day6_dv_591ad32f:


    me "我怎么看阿丽夏?"


translate chinese day6_dv_86e16e26:


    "我重复了一遍，陷入了沉思。"


translate chinese day6_dv_943e93f2:


    me "我真不知道啊...为什么?"


translate chinese day6_dv_caa0bbb4:


    un "你对昨天过于担心。"


translate chinese day6_dv_633b4e6d:


    me "是啊，当然了...但是和这个有什么关系啊?"


translate chinese day6_dv_a7c0899d:


    un "这有直接的关系!"


translate chinese day6_dv_ce3666e5:


    "她笑了。"


translate chinese day6_dv_9b6ef66d:


    me "咱们假设..."


translate chinese day6_dv_235d1b4e:


    un "怎么了?"


translate chinese day6_dv_e7450e08:


    me "那个，她..."


translate chinese day6_dv_2350d104:


    "我又开始思考。"


translate chinese day6_dv_111d6d4f:


    th "说真的我到底是怎么想的?"


translate chinese day6_dv_d2293c40:


    me "那个，她...有的时候确实是...比较傲慢。"


translate chinese day6_dv_ab97ad5d:


    un "是的。"


translate chinese day6_dv_a0062ec1:


    th "我还能说什么?{w} 既然现在提起了这个问题，我想说我完全不了解阿丽夏。{w}有时候她的态度确实让我很不爽。"


translate chinese day6_dv_c86c4e80:


    me "如果你想问我对她是不是有成见，那么是没有的。"


translate chinese day6_dv_631a37e5:


    me "我是说，她有的时候会控制不住自己，但是每个人都有那样的时候。..."


translate chinese day6_dv_4c144815:


    "列娜盯着我看着。"


translate chinese day6_dv_93207c79:


    un "真的?"


translate chinese day6_dv_97bb8cf6:


    me "我说错什么了吗?"


translate chinese day6_dv_7bc1d385:


    un "没有，并没有..."


translate chinese day6_dv_544a3a75:


    "她又一次大声的笑了出来，这时午饭的铃声响了起来。"


translate chinese day6_dv_5aac5414:


    un "好了，再见!"


translate chinese day6_dv_7c13b2c0:


    "我没有拦住她，虽然我还有很多问题想要问她。"


translate chinese day6_dv_42970b85:


    th "但是我要怎么问呢？我到底应不应该问呢？"


translate chinese day6_dv_3869395e:


    "我站起来，缓慢的移动到食堂。"


translate chinese day6_dv_14e6034a:


    "只剩下几个位置了，所以我只能和电子小子和舒里克坐在一起。"


translate chinese day6_dv_497bae1a:


    el "你有没有交货?"


translate chinese day6_dv_9513cd87:


    me "嗯..."


translate chinese day6_dv_40231c6f:


    "我懒洋洋的回答着，一边用叉子杵着荞麦粥。"


translate chinese day6_dv_807920a2:


    el "没发生什么事情吗?"


translate chinese day6_dv_a0c4afad:


    me "没什么...{w}里面到底是什么啊?"


translate chinese day6_dv_01cfac1d:


    el "你没有打开吧?"


translate chinese day6_dv_2c5335a6:


    "他盯着我看。"


translate chinese day6_dv_5cb822ee_2:


    me "没..."


translate chinese day6_dv_fa45ce2d:


    el "太好了!"


translate chinese day6_dv_385405d4:


    "吃完饭后，我离开了食堂。"


translate chinese day6_dv_bebd5afd:


    "我突然打起了呵欠。"


translate chinese day6_dv_2984a468:


    th "这时睡个午觉应该是个好主意。"


translate chinese day6_dv_f4822807:


    "我前往辅导员的宿舍。"


translate chinese day6_dv_b85dfb0f:


    "盛夏的烈日像是要把一切都晒化了一样。"


translate chinese day6_dv_4e7b8856:


    "没有地方可以躲藏，任何的平面都反射着阳光，如果抬头，你会变盲。"


translate chinese day6_dv_4d2a4c1c:


    "我用尽最后的力气，转动着门把手，走进房门，然后瘫倒在床上。"


translate chinese day6_dv_aefac231:


    "我没有期待着房间里能有多么凉爽，不过还是要比外面好多了。"


translate chinese day6_dv_802ba619:


    "我惬意的在床上舒展开身体。"


translate chinese day6_dv_799e8d13:


    "奇怪的是，我睡不着，我的大脑中是各种想法。"


translate chinese day6_dv_601d58f7:


    "一会儿是关于我怎么来到这里的理论。{w} 一会儿又是这里发生了什么的理论。{w} 一会儿又是这里的事务的真面目的理论。"


translate chinese day6_dv_8152340b:


    "我突然想起来列娜说的，我对于阿丽夏怎么看。"


translate chinese day6_dv_baeb4f56:


    th "说真的，我怎么看待她?{w} 我怎么看待一个我刚刚认识了几天的人?{w} 我可以根据她的几个特定的行为举止就做出判断吗?"


translate chinese day6_dv_4881f3b0:


    th "虽然说她的行为..."


translate chinese day6_dv_a026df63:


    "我笑了起来。"


translate chinese day6_dv_5492f5b1:


    "对于雕塑爆炸事件，各个组织都会得出类似的结论的。"


translate chinese day6_dv_27abcde7:


    th "不过，我...{w} 不在乎?"


translate chinese day6_dv_e0e83cfe:


    th "也许我什么都不应该想?"


translate chinese day6_dv_79c3d71e:


    th "一个人可以对一个没有意思的人感兴趣吗?"


translate chinese day6_dv_32ee260b:


    th "不，这太荒谬了..."


translate chinese day6_dv_7e8c00bf:


    "有人在敲门。"


translate chinese day6_dv_6b33d385:


    me "请进!"


translate chinese day6_dv_d96562ca:


    "我实在太懒得起来了，所以我只是稍微坐了起来。"


translate chinese day6_dv_cc6d016b:


    "门打开了，我看到了阿丽夏。真是一个巧合啊..."


translate chinese day6_dv_e8401a0f:


    "她停了下来，盯着我看了一会儿。"


translate chinese day6_dv_344347eb:


    me "我猜你是在找辅导员?"


translate chinese day6_dv_6a8dcc12:


    th "我是怎么想的?"


translate chinese day6_dv_e8936f84:


    dv "不是。"


translate chinese day6_dv_e2960fb5:


    me "那有什么事?"


translate chinese day6_dv_a09125aa:


    dv "我在找你。"


translate chinese day6_dv_5e777c99:


    "她看起来很平静，不是很狂躁。"


translate chinese day6_dv_15c7a20d:


    "一般来说我不会害怕也不会在意她的这种表现的。"


translate chinese day6_dv_7332342b:


    me "为什么?"


translate chinese day6_dv_7d4b9078:


    dv "我想要把一切弄清楚。"


translate chinese day6_dv_0ed62df2:


    "她坐在我对面的床上。"


translate chinese day6_dv_ae56873f_1:


    me "具体是什么?"


translate chinese day6_dv_4cf6f6d7:


    dv "我想要澄清我们之间的误会。"


translate chinese day6_dv_32c1cb65:


    me "好吧，我很支持你!"


translate chinese day6_dv_d6f5f041:


    dv "你今天和列娜说过话了吗?"


translate chinese day6_dv_fc30fd2b:


    me "是的。"


translate chinese day6_dv_21a68fd8:


    dv "关于什么?"


translate chinese day6_dv_96467507:


    me "其实没说什么..."


translate chinese day6_dv_3e9127d1:


    dv "你在说谎。"


translate chinese day6_dv_e9a6a178:


    "阿丽夏看起来很僵硬也很担心。{w}她翘起腿，咬着嘴唇。"


translate chinese day6_dv_6300bc11:


    dv "你应该知道吧...不管是什么时候，出了问题我总是最先被怀疑的..."


translate chinese day6_dv_0288549d:


    me "反正犯错的总是你。"


translate chinese day6_dv_c41aa4a5:


    "我没过脑子就说了出来。"


translate chinese day6_dv_69364372:


    "她的眼中闪过了什么，不过还是继续说着:"


translate chinese day6_dv_f0c03665:


    dv "也许是吧...{w} 但是这次...{w} 我和列娜, 我们总是那样 – 即使完全是她引起的，最后要责备的还是我!"


translate chinese day6_dv_63fdbbbc:


    me "所以你是说列娜...{w}事实上我不太明白你在说什么。"


translate chinese day6_dv_60dc8a1d:


    dv "你从来都不明白，是不是!?"


translate chinese day6_dv_d1fbe814:


    "她变得狂躁了。"


translate chinese day6_dv_c727773e:


    th "奇怪了，我感觉自己还没有愚蠢到那个程度。"


translate chinese day6_dv_3ce5fd8b:


    me "解释一下。"


translate chinese day6_dv_25156efc:


    dv "你知道列娜喜欢你吧?"


translate chinese day6_dv_1aa433e1:


    "她说完脸红了。"


translate chinese day6_dv_5cb822ee_3:


    me "不..."


translate chinese day6_dv_e0299b09:


    "我诚实的回答着。"


translate chinese day6_dv_37a6038e:


    dv "好吧，现在你知道了..."


translate chinese day6_dv_9ecfc70c:


    me "现在知道了..."


translate chinese day6_dv_ae829ad1:


    "屋子里一阵尴尬的沉默..."


translate chinese day6_dv_62543cf8:


    "我还在理解着她说的话，阿丽夏似乎又不知道该说什么好。"


translate chinese day6_dv_768d0b51:


    th "列娜喜欢我，我可能猜得出来吗...不太可能。"


translate chinese day6_dv_49ebd22b:


    "我的直觉一向很准确，但是到了关于我自己的感受的时候就完了。"


translate chinese day6_dv_23bbf0e9:


    "最奇怪的事情是，我竟然毫不犹豫的表示理解。"


translate chinese day6_dv_23eb34e0:


    "我现在正听着阿丽夏说话，来不及想别的。"


translate chinese day6_dv_4b721f97:


    dv "她觉得我要把你抢走!{w} 她总是那么想，你知道吗？即使事情是完全反过来的! 每个人都注意着她，因为她是一个天使，没有人注意我!"


translate chinese day6_dv_61d7ee92:


    dv "对她们来说，我就是一个可以抱住哭的人..."


translate chinese day6_dv_3bc3b19b:


    "阿丽夏开始喊了起来。"


translate chinese day6_dv_095c18d7:


    me "你看，我...{w}别伤心，我不是那么想的!"


translate chinese day6_dv_ecfa445c:


    dv "当然..."


translate chinese day6_dv_c2c94b69:


    "我还是不知道什么让她这么诚实。{w}我也不知道该如何回应。{w} 而且我一点都不担心。"


translate chinese day6_dv_c62c09a6:


    "我好像是在看一部电视剧一样，所有的故事都是在屏幕的那头。"


translate chinese day6_dv_ef389ffb:


    me "我明白，都是因为我...{w}但是列娜今天说没关系了。"


translate chinese day6_dv_5eb2b915:


    dv "她就是那样！你应该知道她其实不是看起来的那个样子!"


translate chinese day6_dv_5b3e26db:


    th "我开始思考。"


translate chinese day6_dv_650270d4:


    me "如果我能做的了什么..."


translate chinese day6_dv_323fb304:


    dv "告诉我..."


translate chinese day6_dv_092b3b93:


    "阿丽夏抬起头，我这才注意到她的眼睛里充满了泪水。"


translate chinese day6_dv_8c4f1a22:


    "我的心中有什么激荡了起来。{w}我不能再把自己当作是一个局外人那样处理了。"


translate chinese day6_dv_c7e864ed:


    dv "你喜欢列娜吗？还是喜欢我？"


translate chinese day6_dv_da3f2db4:


    "我应该提前预料到这个问题的，但是我却没有准备好。"


translate chinese day6_dv_84e48168:


    me "就是那个..."


translate chinese day6_dv_a0eda2ae:


    th "列娜人很好...{w} 但是阿丽夏说的也很有道理。"


translate chinese day6_dv_a769edd0:


    "而且她说的时候是当真的。"


translate chinese day6_dv_a4fad65f:


    me "我对你们都不太了解...{w}这样的问题实在是不好直接回答。"


translate chinese day6_dv_9a3e6f7f:


    "如果要我说不的话，可能我是会说的，在需要的场合。"


translate chinese day6_dv_73ec7242:


    "但是我只有一次回答的机会。"


translate chinese day6_dv_56680e86:


    "但是我又不知道该如何选择。"


translate chinese day6_dv_f673f8e3:


    dv "你想要让这件事不了了之。"


translate chinese day6_dv_80322d80:


    me "当然，但是..."


translate chinese day6_dv_32cbd9c4:


    dv "又是但是?"


translate chinese day6_dv_3ea35b71:


    me "为什么又是?"


translate chinese day6_dv_4c7ded3a:


    dv "为什么为什么?"


translate chinese day6_dv_f21d1a90:


    me "我说，咱们这样得不出任何结论..."


translate chinese day6_dv_3baacd9d:


    dv "好啊，你倒是回答啊!"


translate chinese day6_dv_db0ec79f:


    "她似乎又恢复了平常的傲慢。"


translate chinese day6_dv_d1744488:


    me "我需要时间..."


translate chinese day6_dv_f097abfb:


    dv "那你就回去想吧!"


translate chinese day6_dv_b750f4ae:


    "阿丽夏站了起来，然后生气的往外走。"


translate chinese day6_dv_5783bb1b:


    dv "别忘了晚上我们还有计划!"


translate chinese day6_dv_70813d40:


    "她在门槛上笑着说。"


translate chinese day6_dv_d7d9b64d:


    "我还没来得及说什么，门就关上了。"


translate chinese day6_dv_94e61f1e:


    th "计划? 什么计划?"


translate chinese day6_dv_76c05f84:


    "这次的对话对我影响很大。"


translate chinese day6_dv_37b565c4:


    th "所以说，列娜喜欢我..."


translate chinese day6_dv_0e2e48a7:


    th "这个新闻已经很令人震惊了，但是我还必须做出选择，而且不管我怎么选，总是有人要伤心。"


translate chinese day6_dv_7e235707:


    th "要是我能两个都拒绝就好..."


translate chinese day6_dv_e7446732:


    "虽然我本来就不喜欢拒绝这个词。"


translate chinese day6_dv_92bca9c0:


    th "如果我可以只用一个不字回答就好了。"


translate chinese day6_dv_4cf63875:


    "但是我知道我做不到。"


translate chinese day6_dv_1aaf9536:


    th "在我和阿丽夏谈过之后，我知道一切都无法回到过去了。"


translate chinese day6_dv_0b14c784:


    "没有办法，没有机会了，我必须做出选择..."


translate chinese day6_dv_0fa075d4:


    th "既然“不”不是一个选择，那么我必须找一个更加积极的说法，如果我可以接受的话。"


translate chinese day6_dv_66117c1c:


    "但是很显然的是，有一个选择不会让所有人都满意，但是是我一心期待的。"


translate chinese day6_dv_0086716d:


    "那就是列娜，她聪明可爱，又善良。"


translate chinese day6_dv_6cd9e693:


    "阿丽夏没有什么吸引我的。"


translate chinese day6_dv_be0923e0:


    "门突然打开了，奥尔加·德米特里耶夫娜走了进来。"


translate chinese day6_dv_0bae5ad4:


    mt "又在磨洋工了！"


translate chinese day6_dv_fe458b8c:


    me "只是休息一下，外面太热了..."


translate chinese day6_dv_2ebe60a9:


    mt "起来去帮助斯拉维娅，她在四处找人。"


translate chinese day6_dv_25f7abbc:


    me "在哪里?"


translate chinese day6_dv_7a91aa07:


    "我懒洋洋的问着。"


translate chinese day6_dv_a18b435d:


    mt "在运动场那里。"


translate chinese day6_dv_5c421508:


    "我不情愿的出发了。"


translate chinese day6_dv_ee607f36:


    "不过我确实应该呼吸一下新鲜空气。"


translate chinese day6_dv_a7fe7928:


    "运动场有很多人，有人在踢球，有人在打羽毛球，更小的孩子们则在追逐玩耍。"


translate chinese day6_dv_2a454c26_1:


    sl "谢苗!"


translate chinese day6_dv_876e414e:


    "斯拉维娅向我走来。"


translate chinese day6_dv_bf8808dd:


    me "你找我帮忙了吗?"


translate chinese day6_dv_ecb3efc7:


    "我装模作样的问着。"


translate chinese day6_dv_b793031f:


    sl "嗯...{w}对了，你有没有医务室的钥匙?"


translate chinese day6_dv_5a7a7577:


    me "没有。"


translate chinese day6_dv_9babb1c2:


    th "话说为什么问我?"


translate chinese day6_dv_93bd5577:


    sl "你最近经常在那里嘛。"


translate chinese day6_dv_ac1c31df:


    th "她说的有道理..."


translate chinese day6_dv_5cb822ee_4:


    me "没有..."


translate chinese day6_dv_234b5492:


    sl "也许你知道谁有?"


translate chinese day6_dv_3119e7b5:


    me "不知道。为什么?"


translate chinese day6_dv_da46fa34:


    sl "护士今天来找我借钥匙，你知道我有所有的钥匙，她说她自己的找不到了。"


translate chinese day6_dv_4070ac03:


    me "是不是她自己弄丢了?"


translate chinese day6_dv_0fe6b15a:


    "我耸耸肩。"


translate chinese day6_dv_1197e9fc:


    sl "不，她说不会的。"


translate chinese day6_dv_85cd4b82:


    me "我不知道，好吧?"


translate chinese day6_dv_35b300f9:


    sl "好吧。"


translate chinese day6_dv_0049a2fb:


    "她笑着跑开了。"


translate chinese day6_dv_52ec3723:


    "我回去辅导员的房间，准备补补觉。"


translate chinese day6_dv_02ad66d4:


    "还好奥尔加·德米特里耶夫娜不在这里，我瘫在床上，失去了意识。"


translate chinese day6_dv_a20cefa7:


    "..."


translate chinese day6_dv_4df22cd5:


    "我不记得自己梦到什么，但是感觉发生了很多事，下午睡午觉总是这样。"


translate chinese day6_dv_ea3f5806:


    "我折腾着站起来看看表，现在七点多一点。"


translate chinese day6_dv_53b55b1b:


    th "这不奇怪，我都有可能睡过去一整天。"


translate chinese day6_dv_23d4a9c7:


    th "虽然说我还是错过了晚饭..."


translate chinese day6_dv_31e3afb8:


    "这让我很不爽，我走向了门口。"


translate chinese day6_dv_ee3bcd9a:


    "我在门前停了下来，思考着下一步该做什么。"


translate chinese day6_dv_df471347:


    "我不知道如果没有人叫我的话，我还会这样昏昏沉沉的在门口站多久。"


translate chinese day6_dv_280c3ef3:


    "斯拉维娅迅速向门口走来。"


translate chinese day6_dv_1e187d3c:


    sl "谢苗..."


translate chinese day6_dv_eef5cfb9:


    "她喘着气问着。"


translate chinese day6_dv_fce6b834:


    sl "你确定你没有医务室的钥匙吗?"


translate chinese day6_dv_cafb72c1:


    me "当然没有，我要那个有什么用?"


translate chinese day6_dv_af3f3208:


    sl "那你知道谁拿了吗?"


translate chinese day6_dv_cddb9533:


    me "不知道，为什么?"


translate chinese day6_dv_a19bb019:


    sl "那个...有些东西消失了。"


translate chinese day6_dv_8b677452:


    me "什么东西?"


translate chinese day6_dv_b9cf32d1:


    sl "重要的..."


translate chinese day6_dv_eba68a85:


    "她停顿了一下。"


translate chinese day6_dv_28ce78b4:


    sl "药品。"


translate chinese day6_dv_eb5ac138:


    th "所以这都是因为丢了一些药？虽然说..."


translate chinese day6_dv_89141735:


    "我仔细想着。"


translate chinese day6_dv_19d82568:


    th "我的房间里有个瓶子...不知道是不是她说的?"


translate chinese day6_dv_bbccfea0:


    "我试图让自己保持镇定。"


translate chinese day6_dv_065dee42:


    me "好吧，我不知道，抱歉..."


translate chinese day6_dv_6f9aa3f1:


    sl "好吧..."


translate chinese day6_dv_280be8fe:


    "斯拉维娅失望的说着。"


translate chinese day6_dv_e22d0105:


    "我看着她的样子，思考着情况到底有多么糟糕，我是不是要完蛋了。"


translate chinese day6_dv_dffe083c:


    th "一方面来说，那瓶伏特加...确实是我偷的。"


translate chinese day6_dv_935f06ca:


    th "但是她又说“消失”，是自己消失，不是外界导致的。"


translate chinese day6_dv_383cfe31:


    th "虽然她不太可能说出来医务室里的一瓶伏特加被偷走了。"


translate chinese day6_dv_910fce4e:


    th "所以说的方式没什么问题。"


translate chinese day6_dv_1072ee7f:


    "我突然感觉很害怕。"


translate chinese day6_dv_73f94476:


    th "万一他们发现是我干的呢?"


translate chinese day6_dv_65b4be16:


    th "即使不是瓶子的事，他们搜索的时候还是会找到。"


translate chinese day6_dv_dea23e04:


    "我得行动起来了!{w}现在最主要的任务是销毁证据!"


translate chinese day6_dv_bea31ad1:


    "我把瓶子揣起来往森林跑去。{w}现在最好的选择是把它埋起来。"


translate chinese day6_dv_27c88b77:


    "我花了一段时间才找到一个好地方，没有发现夜幕已经降临了。"


translate chinese day6_dv_768e9627:


    "一个熟悉的声音吓了我一跳。"


translate chinese day6_dv_93b5fc50:


    dv "嘿!"


translate chinese day6_dv_66bf0ead:


    "我急忙转过身去，装出一副平常的样子。"


translate chinese day6_dv_dbd3bc52:


    dv "所以说，你在这里干什么?"


translate chinese day6_dv_385b3fe9:


    me "哦，我只是在散步..."


translate chinese day6_dv_405fbe4e:


    "我的声音还算冷静，但是我知道我失败了，我骗不过阿丽夏。"


translate chinese day6_dv_4529043a:


    dv "自己?半夜?在树林里?"


translate chinese day6_dv_4e1558a6:


    me "是啊... 有什么问题吗?"


translate chinese day6_dv_fcd85f45:


    dv "没什么，完全没有问题。"


translate chinese day6_dv_1b595140:


    "她看着我。"


translate chinese day6_dv_76aba977_1:


    me "好吧，那我..."


translate chinese day6_dv_c250ee11:


    dv "等一下!{w}你没有忘掉什么吗?"


translate chinese day6_dv_01f23cac:


    me "没有吧。"


translate chinese day6_dv_4daa4260:


    dv "我们说好了的!"


translate chinese day6_dv_c233b6a2:


    me "关于什么?"


translate chinese day6_dv_59e88892:


    dv "今天晚上!"


translate chinese day6_dv_1a41c767:


    "她似乎说得对，好像有这么一回事。"


translate chinese day6_dv_d72e579c:


    me "是啊，但是你看..."


translate chinese day6_dv_afd1c304:


    dv "我不管，我们走!"


translate chinese day6_dv_d3dbfb33:


    "她往前走着，招呼我跟上。"


translate chinese day6_dv_2a3aebf5:


    "我想，争吵是没有必要的，我的任何举动都有可能让自己暴露。"


translate chinese day6_dv_fe0a8a5c:


    th "然后..."


translate chinese day6_dv_1faec534:


    "我叹着气，跟着她。"


translate chinese day6_dv_b5027457:


    "我们过了几分钟来到了她的房间。"


translate chinese day6_dv_4d608c0d:


    "我在路上一直试图丢掉那瓶伏特加，但是阿丽夏和我距离太近，让我一直没有机会。"


translate chinese day6_dv_dbeba143:


    dv "嗯?"


translate chinese day6_dv_1b595140_1:


    "她盯着我。"


translate chinese day6_dv_d6f22291:


    me "嗯什么?"


translate chinese day6_dv_e3e21bbe:


    "我过于紧张，冒了一身冷汗。"


translate chinese day6_dv_2ffa378a:


    "我现在想到的唯一一件事就是调整瓶子的角度，让阿丽夏看不出来。"


translate chinese day6_dv_988e7f2d:


    dv "你说什么?"


translate chinese day6_dv_a4388070:


    me "关于什么?"


translate chinese day6_dv_3614794f:


    "阿丽夏思考着。"


translate chinese day6_dv_8c1b9d53:


    dv "好吧，你说得对，我把你带到这里来，应该是我做决定。"


translate chinese day6_dv_505d91ae:


    me "好吧..."


translate chinese day6_dv_dea40394:


    "我略表同意。"


translate chinese day6_dv_92e1ccd1:


    dv "等一下..."


translate chinese day6_dv_cb6c3ebb:


    "她伸手在床下面找着...{w}然后拿出来一瓶“红牌”伏特加!"


translate chinese day6_dv_0ab734f4:


    "我震惊了。"


translate chinese day6_dv_3ff96290:


    dv "很惊讶吧?"


translate chinese day6_dv_36d2fce0:


    th "当然!{w}不是被这件事本身，而是这瓶伏特加就是我早晨带到医务室的那瓶。"


translate chinese day6_dv_1d557f44:


    "里面浑浊的液体告诉了我这一点。"


translate chinese day6_dv_4d1c9363:


    dv "所以?"


translate chinese day6_dv_13181c03:


    "阿丽夏挑逗的看着我。"


translate chinese day6_dv_ad02f53a:


    me "什么?"


translate chinese day6_dv_9a78a547:


    dv "来点儿吗?"


translate chinese day6_dv_235f9753:


    "我都不知道该说什么好。"


translate chinese day6_dv_d54f3e81:


    th "她拿的只是一瓶脏水，真正的伏特加在我的腰带上。"


translate chinese day6_dv_8d2027c8:


    me "好吧，我有点..."


translate chinese day6_dv_77dfc065:


    dv "别告诉我你不喝!"


translate chinese day6_dv_9859878b:


    "阿丽夏撅起嘴。"


translate chinese day6_dv_d2ea3d33:


    me "我不知道...不，只是..."


translate chinese day6_dv_c9e23ee7:


    dv "那好吧！等一下!"


translate chinese day6_dv_f816894f:


    "不一会儿，阿丽夏取出了两个小杯子，然后把它们灌满。"


translate chinese day6_dv_3a7f0ff5:


    dv "来吧!"


translate chinese day6_dv_bdc908c8:


    "她说完，递给我一杯，然后我还没来得及说什么她就干了另一杯。"


translate chinese day6_dv_31c05c43:


    dv "呜啊!"


translate chinese day6_dv_8ad16087:


    "她喘着气说。"


translate chinese day6_dv_17c04f77:


    "她马上明白了过来..."


translate chinese day6_dv_1df814f8:


    dv "这是什么?!"


translate chinese day6_dv_e54a6b95:


    "她开始往外吐。"


translate chinese day6_dv_c73f3fec:


    dv "他们管这个叫伏特加?!"


translate chinese day6_dv_420183fc:


    "我实在忍不住笑了起来。"


translate chinese day6_dv_b2fde6e2:


    dv "你在笑什么?"


translate chinese day6_dv_6858883c:


    "阿丽夏红着脸问着。"


translate chinese day6_dv_9ed6914d:


    dv "我问你，什么那么好笑?!"


translate chinese day6_dv_6c56b254:


    me "真的很好笑嘛。"


translate chinese day6_dv_0c423888:


    dv "你很开心？你把自己的喝了，我们看看是不是很有趣!"


translate chinese day6_dv_2488dfae:


    "我可不想喝，但是为了不让她怀疑:"


translate chinese day6_dv_7150554e:


    me "所以说是什么味道?"


translate chinese day6_dv_a0c02db8:


    dv "像腐败的水。"


translate chinese day6_dv_e0350d8e:


    "阿丽夏失落的说。"


translate chinese day6_dv_8d978d04:


    dv "我只是想要优雅的休息一会儿，结果..."


translate chinese day6_dv_84e0b485:


    me "优雅的休息..."


translate chinese day6_dv_4243032b:


    "我重复了一遍，笑弯了腰。"


translate chinese day6_dv_43e1408d:


    dv "嘿，别笑了!"


translate chinese day6_dv_df3fece3:


    "阿丽夏一边喊着，一边袭击我。"


translate chinese day6_dv_5fb59874:


    "她试图打我或者咬我什么的，或者是干掉我，我抓住了她的手，结果她只能愤怒的在原地喘着气。"


translate chinese day6_dv_88ce415c:


    "本来没有什么事，但是阿丽夏不小心滑倒了。"


translate chinese day6_dv_550fc70f:


    "我放开了她的手，结果她重重的压在我的身上，砸到了我的鼻子。"


translate chinese day6_dv_3d629c43:


    "我痛苦的嚎叫着，然后把她扔了出去。"


translate chinese day6_dv_bc81e122:


    dv "哇，我们这是发现了什么?"


translate chinese day6_dv_6d5303d4:


    "当我发现她拿到了瓶子的时候，我一点都不疼了。"


translate chinese day6_dv_679b46e1:


    me "还给我!"


translate chinese day6_dv_7be7066b:


    "我试图抢回瓶子，但是阿丽夏巧妙的躲开了，然后逃到屋子的另一端。"


translate chinese day6_dv_8e960ce3:


    dv "我们走着瞧，哈哈!"


translate chinese day6_dv_f8e2a6f3:


    "她马上打开了瓶盖儿，喝了一大口。"


translate chinese day6_dv_652453a6:


    "瞬间她的脸就变红了，要爆炸了。"


translate chinese day6_dv_bc888301:


    dv "这...这是什么?"


translate chinese day6_dv_17c09957:


    "她恢复了意识，用沙哑的声音说着。"


translate chinese day6_dv_a4a2a1fa:


    me "这是真正的伏特加。"


translate chinese day6_dv_2f3f0691:


    "我暴露了。"


translate chinese day6_dv_38cbafd3:


    dv "但是你是从哪里...?"


translate chinese day6_dv_c5adf9aa:


    me "你觉得自己从医务室偷到的那瓶是真的吗?"


translate chinese day6_dv_cac1539b:


    "她点点头。"


translate chinese day6_dv_009a5837:


    me "嗯，这个瓶子是真的，但是里面的东西..."


translate chinese day6_dv_a99785e2:


    dv "哇，你很聪明啊!"


translate chinese day6_dv_eadf401a:


    "她已经语无伦次了。"


translate chinese day6_dv_ec4582f5:


    "阿丽夏走到窗边，把脏水倒掉，换成我的伏特加。"


translate chinese day6_dv_3a7f0ff5_1:


    dv "来吧!"


translate chinese day6_dv_413213a1:


    me "我不想喝!"


translate chinese day6_dv_09bcc5f4:


    th "说真的现在不太合适。"


translate chinese day6_dv_d7cfce4d:


    dv "快喝！不然我要告诉大家是你偷的!"


translate chinese day6_dv_348ce313:


    me "但是是你!"


translate chinese day6_dv_b144a585:


    dv "但是伏特加在你的手里!"


translate chinese day6_dv_20df12ec:


    me "这你可证明不了!"


translate chinese day6_dv_8e3540a1:


    dv "你觉得有必要吗?"


translate chinese day6_dv_da707ec9:


    "阿丽夏有意的说着。"


translate chinese day6_dv_edfca763:


    "我盯着瓶子，思考着应该怎么办。"


translate chinese day6_dv_8e14b6e7:


    th "现在重要的是甩掉那瓶伏特加，只有一个办法..."


translate chinese day6_dv_a1cabb40:


    "我深吸一口气，连续喝掉了一杯。"


translate chinese day6_dv_a5f5245d:


    "我后悔了。"


translate chinese day6_dv_257940d2:


    me "有没有...什么吃的...可以压一压?"


translate chinese day6_dv_4e45e644:


    dv "等一下!"


translate chinese day6_dv_0e2d45d5:


    "阿丽夏兴奋的从抽屉里拿出来一大块火腿。"


translate chinese day6_dv_da1a8792:


    "我撕下来一条咬了起来。"


translate chinese day6_dv_acf11e47:


    me "呜..."


translate chinese day6_dv_9c8b084d:


    dv "怎么样?"


translate chinese day6_dv_bdd0a304:


    me "哈...我可不是每天都这样大口喝酒的!"


translate chinese day6_dv_2919a3a5:


    "我真不知道自己是怎么轻易答应的。"


translate chinese day6_dv_9c8b084d_1:


    dv "好吧，感觉怎么样?"


translate chinese day6_dv_183e652e:


    me "像是...像是伏特加味儿的大大泡泡糖!"


translate chinese day6_dv_e5ec287e:


    dv "好吧，该再来一轮了!"


translate chinese day6_dv_ece6c281:


    "她又打开了瓶子给我们一人倒上半杯。"


translate chinese day6_dv_947b745d:


    me "你不觉得已经差不多了吗?"


translate chinese day6_dv_9e0a34e8:


    dv "你在说什么啊!"


translate chinese day6_dv_dc7ac58a:


    me "好吧，既然咱们已经开始了..."


translate chinese day6_dv_8778e051:


    "酒精已经开始起作用了，我拿起杯子一口干了。"


translate chinese day6_dv_28cad17a:


    "阿丽夏显然不是很有经验，她似乎遇到了一些麻烦。"


translate chinese day6_dv_84399dec:


    dv "啊...这是怎么..."


translate chinese day6_dv_5f08e9f7:


    "她看起来已经醉了。"


translate chinese day6_dv_17750322:


    me "你没把握就不要碰!"


translate chinese day6_dv_3f99c8fe:


    "我开心的说着，继续给自己满上。"


translate chinese day6_dv_02260da3:


    "内心深处我似乎发觉有些多了，但是箭在弦上不得不发..."


translate chinese day6_dv_4ff95f61:


    dv "我怎么样?"


translate chinese day6_dv_0cb8369f:


    "阿丽夏已经精神不正常了。"


translate chinese day6_dv_5097c292:


    me "你还没有喝够吗?"


translate chinese day6_dv_a4506eb9:


    dv "不可能!"


translate chinese day6_dv_d009fa63:


    "她把瓶子抢过去，开始直接喝起来。"


translate chinese day6_dv_ea0035fe:


    me "嘿！咱们有杯子好不好!"


translate chinese day6_dv_99af8ac3:


    dv "去你的!"


translate chinese day6_dv_6dee2c25:


    "她疯狂的喊着笑着。"


translate chinese day6_dv_b13a1ec7:


    dv "好爽!"


translate chinese day6_dv_f6dadb64:


    "她喘着气说着。"


translate chinese day6_dv_bddbfd2a:


    me "是啊，感觉还不错。"


translate chinese day6_dv_49f40434:


    "我看看瓶子，我们已经干掉三分之二了。"


translate chinese day6_dv_4fc941de:


    "我绝对已经喝醉了，阿丽夏更加糟糕。"


translate chinese day6_dv_c365b5cf:


    th "已经不剩什么了!"


translate chinese day6_dv_f1f22231:


    "我叹了口气，给自己倒了一杯，给阿丽夏倒了半杯。"


translate chinese day6_dv_1ddb42b6:


    dv "没了!"


translate chinese day6_dv_ddc629f8:


    "说完她把瓶子从窗户扔了出去。"


translate chinese day6_dv_4037402e:


    me "他们找到怎么办?"


translate chinese day6_dv_31a41270:


    dv "管那个干什么!"


translate chinese day6_dv_b4d6e8a8:


    "我们都大笑了起来。"


translate chinese day6_dv_1e6410a5:


    me "致在场的所有女士!"


translate chinese day6_dv_f9f12001:


    "我哼哼着喝掉了。"


translate chinese day6_dv_1db3dad4:


    dv "致所有的男士!"


translate chinese day6_dv_4a63f2cd:


    "她接着我的话说。"


translate chinese day6_dv_04ff19b4:


    dv "我有点晕晕的..."


translate chinese day6_dv_96ca161a:


    "她解开了衬衫的几个扣子。"


translate chinese day6_dv_8f838576:


    "我仔细的看着她。"


translate chinese day6_dv_4fce3e9d:


    th "她一直是这么漂亮的吗？还是说我现在看谁都这么好看...?"


translate chinese day6_dv_49ff7c76:


    "不管是怎么样，我都没法移开自己的视线。"


translate chinese day6_dv_11513fad:


    dv "你再这么看要把我看穿了!"


translate chinese day6_dv_93c166ce:


    "阿丽夏笑了。"


translate chinese day6_dv_44b2f075:


    me "抱歉..."


translate chinese day6_dv_e18f175c:


    "我很尴尬的看着地面。"


translate chinese day6_dv_59610270:


    dv "你要是感兴趣的话就看吧。"


translate chinese day6_dv_06bac448:


    "我睁开眼睛，看到她还在继续解开扣子。"


translate chinese day6_dv_f506e4ea:


    "我一时语塞。这个时候谁能说的出话来?"


translate chinese day6_dv_a20cefa7_1:


    "..."


translate chinese day6_dv_1f2532e5:


    "接下来该发生的事情发生了。"


translate chinese day6_dv_ec3e3dbf:


    "我现在想不出怎样做是对的。"


translate chinese day6_dv_3c562aeb:


    "我也不想，我感觉很好。"


translate chinese day6_dv_a20cefa7_2:


    "..."


translate chinese day6_dv_4d03f762:


    "灯泡烧坏了，或者是谁关掉了，总之屋里黑了下来。"


translate chinese day6_dv_0a606dd1:


    "我看着她的房间，想起来什么。"


translate chinese day6_dv_422a1788:


    me "乌里扬娜在哪里?"


translate chinese day6_dv_126253a8:


    dv "怎么你害怕有人进来看到吗?"


translate chinese day6_dv_2dc438e7:


    "阿丽夏好心的笑着。"


translate chinese day6_dv_2d3ba9c4:


    me "有可能..."


translate chinese day6_dv_ae2894c9:


    dv "我叫她到别的女孩的房间休息。"


translate chinese day6_dv_4ff54a12:


    me "等一下，所以说你...{w}都是计划好的吗?"


translate chinese day6_dv_8c03dbe0:


    dv "嗯，当然没有计划到这个..."


translate chinese day6_dv_938d5502:


    "她微笑着看着我，闭上了眼睛。"


translate chinese day6_dv_c63ec5a4:


    me "嗯..."


translate chinese day6_dv_6c190789:


    "我盯着天花板看着，过了一会儿我听到了平静的呼吸声—阿丽夏睡着了。"


translate chinese day6_dv_1899453f:


    "我突然感觉头疼。"


translate chinese day6_dv_8fbfda03:


    "最好的选择是和她一样睡一觉，我还没有来得及想清楚就已经睡着了。"
# Decompiled by unrpyc: https://github.com/CensoredUsername/unrpyc
