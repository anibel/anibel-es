
translate chinese day7_main_aa6af803:


    "明亮的阳光刺激着我的眼睛。"


translate chinese day7_main_5c43311e:


    "我慵懒的伸着懒腰，爬下床。"


translate chinese day7_main_f254f75a:


    "现在是下午两点。"


translate chinese day7_main_b36d3471:


    th "看来昨天我确实累坏了，得多花一些时间才能恢复。"


translate chinese day7_main_c1bcf764:


    "我在房间里散着步，思考着今天该做些什么。"


translate chinese day7_main_d7fae9fa:


    "显然和那名奇怪的少先队员聊过天以后我在这个营地的生活将会完全不同。"


translate chinese day7_main_f80b4755:


    th "如果一切都和他说的一样，那么我有很多时间。"


translate chinese day7_main_6bd2f897:


    "我拿起我的清洁工具走出门。"


translate chinese day7_main_e37820fc:


    "我刚刚走出几步，突然有人撞到我的后背。"


translate chinese day7_main_fe998b08:


    "我转过身，看到了阿丽夏。"


translate chinese day7_main_b3e42438:


    dv "嘿，小心点!"


translate chinese day7_main_0ffdf7aa:


    "她冷淡的说着，然后就走开了。"


translate chinese day7_main_17ea660a:


    "她的手里提着一个什么袋子..."


translate chinese day7_main_dc306e1e:


    th "啊，管他呢，反正又是这个疯狂的夏令营里的普通的一天。"


translate chinese day7_main_2e63ec7e:


    "用冷冷的水洗脸，让自己变得更加清醒。"


translate chinese day7_main_e3cedd9c:


    "洗漱之后我感觉好了一些。"


translate chinese day7_main_c32db707:


    "突然间，我感觉到了希望，不是我可以安全的离开这里的希望..."


translate chinese day7_main_79994a07:


    "而是，我不相信一切像那个家伙说的那么糟糕。"


translate chinese day7_main_7efc0341:


    pi "早上好..."


translate chinese day7_main_215b97bf:


    "我听到了树林里隐约的声音。"


translate chinese day7_main_091593a0:


    "有人站在树的后面。"


translate chinese day7_main_70958877:


    me "好..."


translate chinese day7_main_6bedc27f:


    pi "你准备好了吗?"


translate chinese day7_main_eaccb1bc:


    me "准备好干什么?"


translate chinese day7_main_a93dcec8:


    "我又看了一眼，似乎这个人是昨天在公交车站看到的那个。"


translate chinese day7_main_3d1d50e7:


    "这倒是意料之中，所以我十分清醒的和他对话。"


translate chinese day7_main_c6c61387:


    pi "你不相信那个人吧，是不是?"


translate chinese day7_main_cb23542f:


    me "你在说什么?"


translate chinese day7_main_74acfe46:


    pi "他说的一切..."


translate chinese day7_main_fe4e16b8:


    me "很好，那么你关于这里的一切的观点是什么呢?"


translate chinese day7_main_71a0a00c:


    "我决定把这里的现实当成是电影一样对待。"


translate chinese day7_main_e427bce7:


    th "希望这能让我更加自由的活动。"


translate chinese day7_main_4ad2a811:


    pi "这里有出口，一定有的!"


translate chinese day7_main_ad9f7f99:


    "他兴奋的说着。"


translate chinese day7_main_9da02d58:


    me "我不知道，但是我倒是愿意相信。"


translate chinese day7_main_6c7b7a89:


    pi "只要你..."


translate chinese day7_main_2a454c26:


    sl "谢苗!"


translate chinese day7_main_47eaf14e:


    "我转过身。"


translate chinese day7_main_5d2bdfb2:


    "斯拉维娅就站在我的旁边。"


translate chinese day7_main_177f4329:


    sl "你在和谁讲话?"


translate chinese day7_main_32d6e567:


    me "呃...没谁...{w}自言自语而已。"


translate chinese day7_main_85f46292:


    "我觉得不太应该跟她说外星人和平行宇宙什么的。"


translate chinese day7_main_eb65a9c2:


    th "反正她应该也不知道..."


translate chinese day7_main_f35f8815:


    sl "你已经准备好了吗?"


translate chinese day7_main_6f63a38c:


    me "准备？再来一次远足？"


translate chinese day7_main_f7b781d4:


    sl "不!{w}今天是整个活动的最后一天。"


translate chinese day7_main_0657e28f:


    me "什么...?"


translate chinese day7_main_955f98b3:


    "我脸上露出傻笑。"


translate chinese day7_main_8ecccc0f:


    sl "晚上会有公交车来到这里，咱们要走了。"


translate chinese day7_main_82b7666b:


    me "尼玛..."


translate chinese day7_main_128e12c4:


    th "我已经准备好接受惊天大逆转了，但是这..."


translate chinese day7_main_f4182ba6:


    "这就是刚刚那个少先队员说的事情吗?"


translate chinese day7_main_2e7eb2bb:


    th "也许是的。"


translate chinese day7_main_7bea6fbb:


    th "显然我得从头再来了。"


translate chinese day7_main_1358eee6:


    th "但是这次我什么都知道了!"


translate chinese day7_main_08625009:


    me "我还没有收拾...虽然说我也没有什么收拾的。"


translate chinese day7_main_6f9aa3f1:


    sl "好吧..."


translate chinese day7_main_a04a9677:


    "斯拉维娅移开了目光。"


translate chinese day7_main_fe79dc7e:


    "她好像想要说些什么，但是犹豫了。"


translate chinese day7_main_9a860373:


    sl "嗯，到时候再见吧!"


translate chinese day7_main_279eeea1:


    me "唔..."


translate chinese day7_main_0f8792e5:


    th "至少我应该问问我们到底什么时候离开..."


translate chinese day7_main_adbb8b50:


    "带着这个疑问我来到了广场。"


translate chinese day7_main_dd4f3123:


    th "附近一定有我认识的人吧!"


translate chinese day7_main_a261ff3b:


    "但是...只有Genda在广场里等着我。"


translate chinese day7_main_588af67a:


    th "也许所有的少先队员们都在收拾东西。"


translate chinese day7_main_6ae3edb7:


    "我在广场找了一个座位坐了下来，呆呆地望着天空。"


translate chinese day7_main_76f33ef6:


    th "结果今天我得收拾好东西在车上失去意识，然后醒来的时候回到第一天..."


translate chinese day7_main_2163cc41:


    "天地陷入寂静。"


translate chinese day7_main_a7963ac8:


    "夏日...太阳像是在天空中停住了，动物都在午睡，晚间的凉风还在积攒能量。"


translate chinese day7_main_7bc45999:


    "我突然想到自己不但错过了午饭，而且早饭也没有吃。"


translate chinese day7_main_6274d258:


    "现在去食堂搜索估计是没有用的，少先队员们离开之前肯定都搜刮干净了。"


translate chinese day7_main_fbfba1db:


    "我挠挠头，回到了自己的房间。"


translate chinese day7_main_e56fa1ff:


    th "桌子的抽屉里肯定得有点可供食用的东西!"


translate chinese day7_main_ea19c1fa:


    "我走到门口的时候有人叫住了我。"


translate chinese day7_main_2faacefc:


    "舒里克和电子小子正在往这边跑着。"


translate chinese day7_main_63a9aae8:


    el "你已经收拾好了吗?"


translate chinese day7_main_129cce15:


    me "收拾好了吗..."


translate chinese day7_main_cc5ffa9b:


    "我模仿着他。"


translate chinese day7_main_18e33553:


    el "你过去的这几天不太像你自己..."


translate chinese day7_main_6e4c96dc:


    me "怎么突然说这个?"


translate chinese day7_main_40962235:


    el "我怎么知道，你自己明白的..."


translate chinese day7_main_55493947:


    me "我是说，你怎么会关心?"


translate chinese day7_main_481d2b70:


    el "没有为什么..."


translate chinese day7_main_10cfbea3:


    sh "一个真正的少先队员应当把同志们的困难当作自己的困难!"


translate chinese day7_main_ff16defd:


    "我怀疑的看着他。"


translate chinese day7_main_13d9478e:


    me "感谢你的关心，我很好。"


translate chinese day7_main_88d04331:


    "结果抽屉里有一块腐败的面包还有一块熏香肠。"


translate chinese day7_main_26352937:


    "我愉快的吃着，然后又喝了一口奥尔加·德米特里耶夫娜大概是用来浇花的水。"


translate chinese day7_main_c67840a3:


    "吃完之后，有人敲门。"


translate chinese day7_main_48555f41:


    me "请进。"


translate chinese day7_main_ed42b147:


    "乌里扬卡冲了进来。"


translate chinese day7_main_bb2205bc:


    us "哇，只有你!"


translate chinese day7_main_3db311fc:


    "她失望的说着。"


translate chinese day7_main_b9fcdeff:


    me "你期待着见到谁？一个马戏团的狗熊？"


translate chinese day7_main_94876b55:


    "乌里扬卡笑了起来。"


translate chinese day7_main_da46daf5:


    us "奥尔加·德米特里耶夫娜在哪儿?"


translate chinese day7_main_595d7b8c:


    me "不知道。"


translate chinese day7_main_8e0229dd:


    "我耸耸肩。"


translate chinese day7_main_301d3861:


    us "为什么不知道?"


translate chinese day7_main_2f877f3b:


    me "我不知道就是不知道嘛...{w}你想要找她干什么?"


translate chinese day7_main_756d3f6a:


    us "走之前得问一些事。"


translate chinese day7_main_c1537911:


    me "好吧，如果我见到她我会告诉她的。"


translate chinese day7_main_95cb8133:


    us "对了，你为什么还没有收拾?"


translate chinese day7_main_3178b41b:


    me "好像我有什么可以收拾的似的..."


translate chinese day7_main_fa77eb7d:


    us "那好吧，拜拜!"


translate chinese day7_main_84ce9118:


    "她狡猾地笑着，甩上门离开了。"


translate chinese day7_main_b0f8ec72:


    "奇怪，没有人觉得这突然的结束很奇怪吗？"


translate chinese day7_main_80d609d4:


    th "为什么我是唯一一个并没有期待着的?"


translate chinese day7_main_e2fab788:


    th "好像大家都很关心我有没有收拾东西?"


translate chinese day7_main_fa546b6a:


    th "而且大家都不担心这是我们最后一次见面了吗...?"


translate chinese day7_main_dcef1d84:


    "昨天那个人说这里的一切都是假的，我突然想到。"


translate chinese day7_main_de05ab05:


    th "好吧，现在我准备好相信这个理论了。"


translate chinese day7_main_d7317467:


    "我找到早饭的抽屉里还有一堆家庭用品。"


translate chinese day7_main_d43240e6:


    "我拿起一根铅笔还有一片纸，检查了一下，然后装进了口袋。"


translate chinese day7_main_8dcd7fa6:


    th "以备不测!"


translate chinese day7_main_39f8f65c:


    "我可没有兴趣围观其他少先队员们收拾东西，结果自己不知不觉就睡着了。"


translate chinese day7_main_a20cefa7:


    "..."


translate chinese day7_main_59194f62:


    "有什么人说话的声音把我吵醒了。"


translate chinese day7_main_9e6959c5:


    "一个熟悉的少先队员就坐在对面，背对着我。"


translate chinese day7_main_937c66f2:


    "我已经很熟悉他了。"


translate chinese day7_main_d1240714:


    me "我说，你怎么总是隐藏着自己的脸?"


translate chinese day7_main_cc040a11:


    pi "因为你不应该看到。"


translate chinese day7_main_0fa7e23d:


    me "如果你这么说的话..."


translate chinese day7_main_9ca20712:


    "我不应该卷入争吵。"


translate chinese day7_main_c1b4c0f1:


    me "所以说这次你又要揭露什么?"


translate chinese day7_main_600f0c98:


    pi "你已经知道今天是夏令营的最后一天了吧?"


translate chinese day7_main_fc30fd2b:


    me "是啊。"


translate chinese day7_main_6800c2ff:


    pi "而且你已经和那个人说过了吗?"


translate chinese day7_main_fc30fd2b_1:


    me "是啊。"


translate chinese day7_main_cf3c4c1e:


    pi "那么，他和你怎么说?"


translate chinese day7_main_4a46ef3d:


    me "没什么特别的，他说这里有出口。"


translate chinese day7_main_6beb007e:


    "少先队员笑了起来。"


translate chinese day7_main_caf2ecdd:


    pi "是啊，我很久以前也曾经相信。"


translate chinese day7_main_f6352aef:


    me "现在呢?"


translate chinese day7_main_52ab222f:


    pi "现在？我曾经有过自己的生活..."


translate chinese day7_main_867d892e:


    "他停顿了一段时间。"


translate chinese day7_main_015d5bda:


    pi "不过那是很久很久以前了，我已经不记得了。"


translate chinese day7_main_499952ad:


    pi "未来总是一样的，不停地重复，没完没了，你怎么知道现在是什么时候？"


translate chinese day7_main_50a2b7a7:


    me "好吧，虽然我不想说，但是...我目前还没有像你一样在无限的时间里失去自我。"


translate chinese day7_main_09c7c654:


    pi "哦，没有关系，该来的总会来的!"


translate chinese day7_main_54556d0f:


    "他像魔鬼一般的笑起来。"


translate chinese day7_main_2ede7e21:


    me "我只有一件事不明白，你到这里来是干什么?"


translate chinese day7_main_ed700d2d:


    pi "我？其实没有什么。"


translate chinese day7_main_ed4c078d:


    me "那么你到底为什么来?"


translate chinese day7_main_239cb75b:


    pi "因为只有你，他还有像我们一样的人是真实的人。"


translate chinese day7_main_cce08df0:


    "虽然他一直这么说，但是我还是很难相信这里的大家都是一群木偶。"


translate chinese day7_main_4529ad5a:


    me "你确定自己说的对吗?"


translate chinese day7_main_6fa19435:


    pi "说的什么对?"


translate chinese day7_main_311f2261:


    me "一切...?"


translate chinese day7_main_1e5d88ee:


    pi "无所谓对错，我就是来到了这里，你也是一样。"


translate chinese day7_main_8b97aebf:


    me "我说，我对你的玄学已经糊涂了。"


translate chinese day7_main_d4c1382a:


    "我也很奇怪自己为什么可以冷静的和他聊天。"


translate chinese day7_main_0e575d19:


    th "好了，都在这里了，这个夏令营所有的疯狂和奇幻。"


translate chinese day7_main_265d8152:


    th "还有—我来到这里的原因。"


translate chinese day7_main_377ed3b6:


    th "还有—我寻找已久的答案!"


translate chinese day7_main_0a0561bd:


    "一方面，我的行动很理智。"


translate chinese day7_main_d9e03d7d:


    "在我还完全迷糊的时候，这个人滔滔不绝的白话着，虽然说他说个没完也不一定能说出个所以然。"


translate chinese day7_main_8a6c765f:


    "那么听他的到底有什么好处?"


translate chinese day7_main_6a19df1b:


    pi "不，不！你很快就会明白一切的。"


translate chinese day7_main_5022bc8a:


    "有人在敲门。"


translate chinese day7_main_482a0501:


    "是斯拉维娅。"


translate chinese day7_main_fad161d6:


    me "你来找奥尔加·德米特里耶夫娜吗?"


translate chinese day7_main_37eba565:


    sl "不..."


translate chinese day7_main_7e6970f3:


    me "那请进吧。"


translate chinese day7_main_c073a9b1:


    "我确定少先队员已经消失了。"


translate chinese day7_main_8cf78390:


    "我发现自己是对的。"


translate chinese day7_main_6a9ff616:


    "斯拉维娅坐在了床上，我靠着衣柜，站在角落里。"


translate chinese day7_main_c8621acb:


    "她显然有点紧张。"


translate chinese day7_main_ea63ce77:


    me "发生了什么事吗?"


translate chinese day7_main_c18a9682:


    sl "没什么...只是...今天是最后一天..."


translate chinese day7_main_165306ee:


    me "嗯，我已经知道了...总比不知道的好。"


translate chinese day7_main_6a46b661:


    sl "嗯，我想也是...我是说...咱们以后大概再也不会见面了吧。"


translate chinese day7_main_2aa28af8:


    me "有人说世界很小。"


translate chinese day7_main_aaf30b84:


    sl "但是也许你可以给我你的地址，我可以给你写信?"


translate chinese day7_main_f178bf3d:


    th "我倒是想，要是我知道就好了。"


translate chinese day7_main_9e6bb37c:


    me "那个...咱们反过来怎么样？你给我你的地址，我回家以后就会给你写信的。"


translate chinese day7_main_305689ee:


    sl "但是你为什么不想把自己的地址给我呢?"


translate chinese day7_main_a91117ee:


    me "嗯...我们正要搬家了，所以说...最好还是我给你写信吧。"


translate chinese day7_main_f734a826:


    "我傻笑起来，试图让自己的故事听起来更加可信。"


translate chinese day7_main_596c8eed:


    sl "啊，好吧...没关系..."


translate chinese day7_main_a4cfd165:


    "斯拉维娅站起来，准备离开。"


translate chinese day7_main_98609165:


    me "嘿，等一下，地址呢?"


translate chinese day7_main_a53cdaac:


    sl "等等再说吧。"


translate chinese day7_main_318e3c4f:


    "她的脸上露出一丝悲伤。"


translate chinese day7_main_1e9c65d7:


    "我刚刚关上了门，突然听到背后挖苦的声音。"


translate chinese day7_main_7fcab065:


    pi "哼，高兴了吗？你让一个女孩儿伤心了。"


translate chinese day7_main_7700b04d:


    me "我怎么伤她心了？给她写信？给乡下的老奶奶写信？"


translate chinese day7_main_a75b7aef:


    me "还是说我要给她一个可能还没有盖起来的房子的地址?"


translate chinese day7_main_87494573:


    pi "那和我有什么关系？这是你的世界，不是我的，反正我在自己的世界里想办法解决了。"


translate chinese day7_main_febd1fcf:


    "最后一句话让我不安。"


translate chinese day7_main_997bc03d:


    me "你知道..."


translate chinese day7_main_5e237961:


    "我没有说完，又有人敲门了。"


translate chinese day7_main_6b33d385:


    me "请进!"


translate chinese day7_main_50581468:


    "乌里扬卡冲了进来。"


translate chinese day7_main_a406d12c:


    me "为何如此匆忙，大小姐?"


translate chinese day7_main_3ea49673:


    "我夸张的鞠着躬。"


translate chinese day7_main_4081a71f:


    us "我？我只是..."


translate chinese day7_main_440da628:


    "她的眼睛警惕的四处张望，脸颊变得红红的。"


translate chinese day7_main_02437b85:


    us "只是想说再见!"


translate chinese day7_main_47d964b9:


    me "会有时间的，反正咱们都要做一辆公交车。"


translate chinese day7_main_6a55610a:


    us "嗯，你说得对，但是在所有人面前有点不好意思。"


translate chinese day7_main_7420944b:


    me "哇，原来你还有不好意思的时候啊?"


translate chinese day7_main_60bde9d8:


    "我笑了起来。"


translate chinese day7_main_3dc79f49:


    us "呃!"


translate chinese day7_main_366345bd:


    "她撅起嘴。"


translate chinese day7_main_a18522d8:


    us "我只是想说你不是一个大坏蛋...而且，你是一个很酷的家伙!"


translate chinese day7_main_7dc51ce4:


    "她的话让我很惊讶。"


translate chinese day7_main_f2486061:


    me "好吧，谢谢...{w}跟你一起玩也很高兴。"


translate chinese day7_main_4a74cafa:


    us "好吧，我说完了!"


translate chinese day7_main_519137f4:


    "她冲了出去，同时甩上了门。"


translate chinese day7_main_d76f8bb2:


    pi "嘿，没有想到吗?"


translate chinese day7_main_b78c6e26:


    me "你想到了?"


translate chinese day7_main_c233607a:


    pi "我告诉过你了，这和我没有关系。"


translate chinese day7_main_e15ce1bd:


    me "但是你好像知道她要说什么似的。"


translate chinese day7_main_5de5d3d5:


    pi "也许是的...也许不是..."


translate chinese day7_main_b6653187:


    me "你来这里就是嘲笑我的?"


translate chinese day7_main_6ea78a01:


    "我怒了。"


translate chinese day7_main_e7918564:


    pi "那个我也想过。"


translate chinese day7_main_4d6f2bc2:


    me "那你告诉我为什么在这里晃来晃去?"


translate chinese day7_main_54b22d30:


    me "如果我需要一个人对我评头论足，那也应该是一个专业的心理医生!"


translate chinese day7_main_3b8fbfa4:


    pi "你觉得经历了这么多我还不够资格吗?"


translate chinese day7_main_e499cb70:


    me "我的观点是经过了这么多你已经完全发疯了。"


translate chinese day7_main_a9edc4dd:


    pi "心理医生都是疯子..."


translate chinese day7_main_eb56bdcb:


    me "是的，但是疯子不都是心理医生。"


translate chinese day7_main_4fdeb53f:


    "他笑了出来。"


translate chinese day7_main_28f25b65:


    pi "幽默，很乐观!说真的你的笑话很没劲。{w} 你在笑什么？你在笑你自己!"


translate chinese day7_main_7e656166:


    me "我说，如果这个世界和你没有关系，你还是去你自己的世界吧。"


translate chinese day7_main_8cd37642:


    pi "那么你在你的世界有什么要做的吗?"


translate chinese day7_main_18cd1dcb:


    "少先队员反驳我。"


translate chinese day7_main_06f26021:


    me "这不用说，我会弄明白的...收拾收拾，离开夏令营..."


translate chinese day7_main_925fc2f3:


    pi "然后呢?"


translate chinese day7_main_9791c0b4:


    me "然后呢...这我怎么知道!"


translate chinese day7_main_f05adfcc:


    me "不管你信不信，反正我是信了。我从来没有遇到这种情况。"


translate chinese day7_main_7109a0ab:


    pi "看起来你好像忘记了自己不能离开这个夏令营了。"


translate chinese day7_main_a4f8c07f:


    th "是啊，这件事他说得对，大概..."


translate chinese day7_main_c8fda9f8:


    me "你失败了，不代表我也会失败!"


translate chinese day7_main_f038ee18:


    pi "是，谁也比不过你。"


translate chinese day7_main_cc46f69e:


    "有人在敲门，声音很轻，我几乎没有听见。"


translate chinese day7_main_67e1f584:


    me "糟糕，又是谁?!"


translate chinese day7_main_c78e5705:


    "我喊道:"


translate chinese day7_main_6b33d385_1:


    me "请进!"


translate chinese day7_main_abbe1f0a:


    "但是门并没有开。"


translate chinese day7_main_c5b18002:


    "所以我自己拉动着把手。"


translate chinese day7_main_26171c75:


    "列娜正站在门口。"


translate chinese day7_main_40993023:


    "看起来我真的吓到她了。"


translate chinese day7_main_200803e8:


    me "啊，抱歉，我真的不是故意的...你是来找奥尔加·德米特里耶夫娜的吧？"


translate chinese day7_main_049b021f:


    un "不。"


translate chinese day7_main_ed81b2e1:


    "她盯着地面说着。"


translate chinese day7_main_84eb3967:


    me "那么?"


translate chinese day7_main_5f2d59cb:


    th "列娜想要干什么?"


translate chinese day7_main_2526c703:


    me "进来吧。"


translate chinese day7_main_b516a337:


    "她走了进来，在房间中央犹豫着。"


translate chinese day7_main_e22a9c2a:


    me "你不想坐下来吗?"


translate chinese day7_main_d6169916:


    "我指向一张床。"


translate chinese day7_main_2d050cb5:


    "列娜又犹豫了一会儿，但是还是坐了下来。"


translate chinese day7_main_ea63ce77_1:


    me "发生了什么事吗?"


translate chinese day7_main_72b9a973:


    un "没有，只是..."


translate chinese day7_main_918776a6:


    "她悄悄的看了我一眼，但是脸唰的红了，然后移开了视线。"


translate chinese day7_main_bacbd43d:


    un "这个!"


translate chinese day7_main_989da459:


    "列娜拿出来了什么东西递给我。"


translate chinese day7_main_23db2f78:


    "我震惊了—是我的手机。"


translate chinese day7_main_27205bdb:


    me "但是...你是从哪里找到的...?"


translate chinese day7_main_8836199f:


    un "我在森林里找到的..."


translate chinese day7_main_e109c728:


    me "好吧，但是你怎么知道这是我的?"


translate chinese day7_main_2bc078c7:


    un "有个男孩子这样告诉我的..."


translate chinese day7_main_75088da6:


    me "你以前见过他吗?"


translate chinese day7_main_da35aa0b:


    un "不知道，我看不清他的脸，但是他穿着少先队员的制服。"


translate chinese day7_main_c773393c:


    "一切都清楚了。"


translate chinese day7_main_82b1e370:


    me "你不奇怪这是什么吗?"


translate chinese day7_main_30490fdf:


    "我看了看屏幕，还有电，所以列娜不会觉得这只是一块塑料。"


translate chinese day7_main_4f35044c:


    un "我不知道，一个什么游戏..."


translate chinese day7_main_a3eea1e0:


    me "啊，你说得对..."


translate chinese day7_main_577b13f1:


    "我马上从菜单里找到贪吃蛇打开。"


translate chinese day7_main_19416c28:


    me "给你吧！当作纪念品!"


translate chinese day7_main_afefddf2:


    un "啊，这怎么行，我不能..."


translate chinese day7_main_2ed4ff25:


    "列娜朝我摆手。"


translate chinese day7_main_f4b1f89a:


    me "拿着吧，我的家里还有好多。"


translate chinese day7_main_cdda1bfc:


    "她又推脱了一会儿，不过最终还是接受了。"


translate chinese day7_main_c67a4136:


    un "我拿这个可以做什么?"


translate chinese day7_main_9216d06a:


    me "用按键控制上下左右，你需要吃到小球但是避免撞到自己的尾巴。"


translate chinese day7_main_1239b2a3:


    un "哇，好有趣!"


translate chinese day7_main_d1046a49:


    "她笑了。"


translate chinese day7_main_1aa9d6c3:


    un "感谢！但是我没有什么给你的东西，太不好意思了。"


translate chinese day7_main_58e414f2:


    me "不用的，谢谢你。"


translate chinese day7_main_d4366f58:


    un "不，那怎么行!"


translate chinese day7_main_2b2d9484:


    "她听起来更有自信了一些。"


translate chinese day7_main_39665fa5:


    un "再怎么说今天是最后一天了。"


translate chinese day7_main_9513cd87:


    me "是啊..."


translate chinese day7_main_f93613f2:


    un "我希望我们能够再次见面..."


translate chinese day7_main_e16f1ab0:


    me "会的。"


translate chinese day7_main_77f2eecc:


    un "那我有一个礼物给你。"


translate chinese day7_main_5109b055:


    me "那是什么呢?"


translate chinese day7_main_03f55ca9:


    un "闭上眼睛。"


translate chinese day7_main_7fdc006d:


    "我照办了。"


translate chinese day7_main_deebb2e3:


    un "保证我让你打开的时候才能睁开!"


translate chinese day7_main_71d3b8c8:


    me "好吧。"


translate chinese day7_main_b472b19d:


    un "不行，你得保证!"


translate chinese day7_main_1f17061d:


    me "好吧好吧，我保证!"


translate chinese day7_main_3e527e8e:


    "不一会儿，我感觉脸颊被轻轻的亲了一下。"


translate chinese day7_main_7aaa79b3:


    "我真的想睁开眼睛，但是我向她保证了..."


translate chinese day7_main_d8b768ba:


    un "睁开!"


translate chinese day7_main_d6d054a1:


    "房间是空空的。"


translate chinese day7_main_188f31b1:


    me "这孩子..."


translate chinese day7_main_026f195c:


    "我不知道还有什么别的可说的。"


translate chinese day7_main_9d8b1602:


    pi "所以说，感觉怎么样，大哥?"


translate chinese day7_main_56c91cad:


    "我看到那个少先队员坐在列娜刚刚的位置，阴险的笑着。"


translate chinese day7_main_0802a5ba:


    me "你就搞这种恶作剧?"


translate chinese day7_main_82d1e669:


    pi "我？恶作剧？你真的被一个可爱的女孩子亲了一下好吗..."


translate chinese day7_main_20d94de6:


    "我真的想要揍他一顿，但是我都不能确定他是不是真人。"


translate chinese day7_main_9183379b:


    me "我估计这还不是你的最后一次恶作剧吧?"


translate chinese day7_main_fcc63fd6:


    "我冷静的说着。"


translate chinese day7_main_e02cb0bc:


    pi "谁知道呢，谁知道呢...这不是很有意思吗?"


translate chinese day7_main_d2749b3c:


    "少先队员大声笑着，可能只有莫里亚蒂教授会这样笑，预想着他邪恶计划的成功。"


translate chinese day7_main_7a033c20:


    me "你是觉得很有意思，我被你玩可不觉得有意思。"


translate chinese day7_main_33573f9b:


    pi "放轻松，伙计。已经快结束了 ，还有你的最后一圈!"


translate chinese day7_main_e2a097db:


    pi "等你再折腾个十几次，可能就需要中场休息一下，但是到时候你可能就不需要我了。"


translate chinese day7_main_15b2c48a:


    me "好像我现在很需要你似的。"


translate chinese day7_main_d7caa3c1:


    pi "哇，这么忘恩负义..."


translate chinese day7_main_417a57f0:


    me "你还有完没完!"


translate chinese day7_main_6c7b7a89_1:


    pi "如果你..."


translate chinese day7_main_5f318380:


    me "行了快闭嘴吧!"


translate chinese day7_main_0d2ff854:


    pi "不明白..."


translate chinese day7_main_6d00754d:


    me "闭嘴!!!"


translate chinese day7_main_2d7f3c73:


    "我喊的声音太大，四墙震动起来。"


translate chinese day7_main_348c1e05:


    "前门突然打开了，阿丽夏冲了进来。"


translate chinese day7_main_69acda8f:


    dv "是我疯掉了吗，还是别的人?"


translate chinese day7_main_93c290bf:


    "她受到惊吓的问着。"


translate chinese day7_main_dd69c3a1:


    me "是的，你可以这么说。"


translate chinese day7_main_a172912c:


    "我愤怒的说着。"


translate chinese day7_main_b7182058:


    dv "你在喊什么?"


translate chinese day7_main_5c9c623e:


    me "我想喊。"


translate chinese day7_main_7efa97c5:


    "我已经明白了，这件事要么是电子小子引发的，要么会被他令人反感的评头论足。"


translate chinese day7_main_f20d0c38:


    dv "你是发疯了吗还是什么?"


translate chinese day7_main_a4579566:


    "阿丽夏走进来斜躺在床上。"


translate chinese day7_main_bec896af:


    me "请问你有什么需要帮助?"


translate chinese day7_main_d28f8e57:


    dv "只是来转转...但是你喊了起来..."


translate chinese day7_main_0a9c213a:


    me "你也没有转啊。"


translate chinese day7_main_f58c07db:


    dv "我没事可做，行李已经收拾好了，好无聊..."


translate chinese day7_main_af374609:


    me "嗯，嗯..."


translate chinese day7_main_e9100bc5:


    dv "如果你觉得我来找你是因为..."


translate chinese day7_main_7386d3f8:


    "她突然愤怒的看着我，然后把脸转了过去。"


translate chinese day7_main_2e5dec60:


    dv "那之后我真不应该来!"


translate chinese day7_main_8e7c35a6:


    me "什么之后？我还什么都没说呢。"


translate chinese day7_main_84a9b240:


    dv "当然！你只是在想!"


translate chinese day7_main_7179f364:


    me "现在你还会读心术?"


translate chinese day7_main_9b855aa7:


    dv "不用读心，都写在你脸上了。"


translate chinese day7_main_e18b878d:


    "从我的脸上除了疲劳和愤怒应该看不出什么别的。"


translate chinese day7_main_7b79a718:


    me "你读出了什么?"


translate chinese day7_main_359525c1:


    dv "这不关你的事!"


translate chinese day7_main_4dfbba46:


    me "又不是我非要让你看的。"


translate chinese day7_main_ee0160d3:


    dv "少来！我愿意看什么就看什么，你管不着。"


translate chinese day7_main_7a6ef78d:


    me "好吧，天哪!"


translate chinese day7_main_5a25a62a:


    "再怎么说阿丽夏在这里陪着我也比神秘少先队员好。"


translate chinese day7_main_35fda676:


    "我躺下来闭上眼睛。"


translate chinese day7_main_09b65c63:


    "阿丽夏过了几分钟才开口说话。"


translate chinese day7_main_35834dca:


    dv "你真的什么也不想说吗?"


translate chinese day7_main_e473fa2f:


    me "比如说?"


translate chinese day7_main_81291356:


    dv "像是今天都是最后一天了。"


translate chinese day7_main_029a43e7:


    me "所以你很高兴?"


translate chinese day7_main_87a35bd8:


    dv "唔..."


translate chinese day7_main_17f57844:


    "她不确定的嘀嘀咕咕着。"


translate chinese day7_main_3930aa38:


    dv "大家都准备离开了。"


translate chinese day7_main_eef28ca7:


    me "多好，终于结束了。"


translate chinese day7_main_6589795d:


    dv "这就没了?"


translate chinese day7_main_aa5aa16f:


    me "你还想听别的?"


translate chinese day7_main_493ea103:


    dv "你不喜欢这里吗?"


translate chinese day7_main_d0b8151b:


    "她的声音和平常不太一样。"


translate chinese day7_main_26cff5fe:


    me "我见过更好的地方。"


translate chinese day7_main_f853a1e5:


    dv "我...你个蠢货，跟你说话就是浪费时间。"


translate chinese day7_main_739017d2:


    "她站起来向门口走去。"


translate chinese day7_main_cfa74516:


    me "好吧，也祝你好运!"


translate chinese day7_main_d6ac8c0a:


    "阿丽夏转过来，脸上写满了愤怒。"


translate chinese day7_main_546c756b:


    dv "你就不能说说你想我什么的吗?!"


translate chinese day7_main_0b12d1b2:


    me "当然啦。"


translate chinese day7_main_db268193:


    dv "废物!"


translate chinese day7_main_06efabf4:


    "她重重的摔上了门。"


translate chinese day7_main_ac6d49df:


    me "傻子!"


translate chinese day7_main_19e291ef:


    "当然阿丽夏没有听见我的最后一句话。"


translate chinese day7_main_3f9dd422:


    pi "真是一个放荡不羁的妞儿啊，是不是?"


translate chinese day7_main_71dbf37a:


    me "跟你半斤八两。"


translate chinese day7_main_fe52d02d:


    pi "这么说，跟你也是。"


translate chinese day7_main_dec36634:


    me "当然，把我跟你比。"


translate chinese day7_main_cc9559a7:


    "我讽刺着他。"


translate chinese day7_main_f5e0300a:


    pi "但是有什么区别，咱们的处境是一样的，我只是比你来的早了一点而已，说实话早了很多..."


translate chinese day7_main_224b03df:


    me "然后你已经完全发疯了。"


translate chinese day7_main_c299cbd0:


    pi "可想而知。"


translate chinese day7_main_6a69d0f4:


    "他沙哑的笑声让我很不爽。"


translate chinese day7_main_69abf7b9:


    me "我说，我有没有说过你很适合当一个演员？你演汉尼拔很合适!"


translate chinese day7_main_f93c3816:


    me "尤其在你把自己定义为心理医生以后。"


translate chinese day7_main_5521fd0f:


    pi "我会考虑的，但是现在我必须走了，后会有期。"


translate chinese day7_main_658edb67:


    me "滚开!"


translate chinese day7_main_7b4acbfa:


    "我看向他，但是他已经消失了。"


translate chinese day7_main_f23afdfa:


    me "哈，终于!"


translate chinese day7_main_bc9abf02:


    th "不过话说回来，他来这里做什么?"


translate chinese day7_main_0da09332:


    "有可能只有我们这样的是这里仅有的几个真正的人，但是我还是不想相信。"


translate chinese day7_main_01d888f8:


    "他说的话就好像是一个什么复杂的游戏。"


translate chinese day7_main_0c605775:


    "好像他不断的给我提示，让我发现更多线索，破解神秘的剧情。"


translate chinese day7_main_801bab94:


    th "真遗憾他失败了，因为我什么也想不出来..."


translate chinese day7_main_a20cefa7_1:


    "..."


translate chinese day7_main_bee56547:


    "时间好像过得很慢，但是已经五点钟了。"


translate chinese day7_main_891514f3:


    "这很正常，如果你一直一秒一秒的数着，那么每一分钟过的都很慢，但是如果你沉浸在别的事情中，那么一天很快就会过去。"


translate chinese day7_main_941b1a51:


    "我决定开始收拾。"


translate chinese day7_main_fb69cac0:


    th "他们可能突然丢下我跑掉..."


translate chinese day7_main_511be532:


    "我把所有的冬装收拾到一个包包里，装好以后我正准备躺到床上歇一会儿，突然奥尔加·德米特里耶夫娜走了进来。"


translate chinese day7_main_d8e5d2bb:


    mt "哇，看来你已经收拾好了，那么咱们出发吧!"


translate chinese day7_main_ce6ffaad:


    "我极不情愿的站起来，拿起自己的行李跟着她。"


translate chinese day7_main_54c4948c:


    th "我真的不在乎接下来会发生什么。"


translate chinese day7_main_5af78794:


    th "我一直怀疑这个世界里没有任何我可以决定的事情，最近的事情更是证明了我的这个判断。"


translate chinese day7_main_06390870:


    th "我可能明天在410路公交车中醒来，或者我根本不会醒来了。"


translate chinese day7_main_5826caff:


    th "就这样，就这么简单。"


translate chinese day7_main_1a9d1f7c:


    th "在这里呆着没有意义，也没有任何可以逃跑的地方。"


translate chinese day7_main_8dc6c9ca:


    th "我唯一的选择就是和大家一起去公交车上，去往未知的前方。"


translate chinese day7_main_e4adaddf:


    th "这就是我一个星期以来的经历，在漆黑的小路上往复，找不到来时的路，也找不到出口。"


translate chinese day7_main_0d0d1634:


    "我们就要抵达大门了，突然我听见有人喊我的名字。"


translate chinese day7_main_592b8e1f:


    bush "谢苗 谢苗"


translate chinese day7_main_f779d1e3:


    me "奥尔加·德米特里耶夫娜，抱歉，等我一下。"


translate chinese day7_main_65032a00:


    mt "好吧，不过快点，不然我们要先走了!"


translate chinese day7_main_18687800:


    "我朝着声音传来的方向走去。"


translate chinese day7_main_592b8e1f_1:


    bush "谢苗...谢苗..."


translate chinese day7_main_36c554a3:


    "我趟过厚厚的草丛，来到森林的小路。"


translate chinese day7_main_52117956:


    "这声音不知道是从哪里传来的，一会儿是在树后面，一会儿是在我的身后。"


translate chinese day7_main_8d4918eb:


    "也许就是那些穿越平行宇宙的外星人，虽然我好像从来没有见过他们。"


translate chinese day7_main_f24bb417:


    bush "我们时间不多了。"


translate chinese day7_main_fd341de6:


    me "我正听着呢。"


translate chinese day7_main_6b2e53ca:


    bush "我知道你已经和他还有他接触过了。"


translate chinese day7_main_fc30fd2b_2:


    me "是啊。"


translate chinese day7_main_6c48903a:


    bush "我知道他们和你说了什么，不要问我怎么知道的。"


translate chinese day7_main_53a56040:


    me "好吧。"


translate chinese day7_main_0e7f1fb9:


    bush "不过你必须知道我知道的事情...像咱们一样的人可不止几十个...说实话，可能有上千个..."


translate chinese day7_main_85f19512:


    bush "但是有很多人走出去了。"


translate chinese day7_main_3cf4cc40:


    "我试着理解他说过的话。"


translate chinese day7_main_5c937cbe:


    me "但是你为什么还在这里?"


translate chinese day7_main_df98d116:


    bush "我留了下来。"


translate chinese day7_main_7332342b:


    me "为什么?"


translate chinese day7_main_0f22f523:


    bush "帮助其他人离开这里!"


translate chinese day7_main_d52f7ebb:


    th "哇，真是博爱..."


translate chinese day7_main_fc7d44e9:


    me "我为什么要相信你，你已经是第三个人了...{w}说实话我都不知道你是不是我自己的幻觉。"


translate chinese day7_main_6972bbdc:


    bush "这不是相信不相信的问题..."


translate chinese day7_main_ca1e4751:


    me "那是什么的问题?"


translate chinese day7_main_be914630:


    bush "是关于选择的问题!"


translate chinese day7_main_ac9043c1:


    bush "把这个夏令营当作一个巨大的迷宫，你需要做出几个正确的选择才能离开这里。"


translate chinese day7_main_bc5b6d3b:


    me "好吧，我怎么知道哪个选择是对的?"


translate chinese day7_main_58019608:


    bush "你会知道的...跟我来..."


translate chinese day7_main_1878bc08:


    me "去哪里?"


translate chinese day7_main_4a75165b:


    bush "去第二次轮回。"


translate chinese day7_main_34fc2c23:


    me "等一下...{w}那个少先队员说我第二次会从明天开始。"


translate chinese day7_main_ac491a56:


    bush "他在撒谎!"


translate chinese day7_main_1386fd85:


    "说话的人提高了声音。"


translate chinese day7_main_73da8c64:


    bush "他已经完全发疯了，他也想让别人疯掉!"


translate chinese day7_main_20f48057:


    me "所以说如果我不跟你一起走，而是做上了公交车，我就完蛋了?"


translate chinese day7_main_8422eef3:


    bush "我不知道..."


translate chinese day7_main_c5e0555a:


    me "那你为什么这么说?"


translate chinese day7_main_f2ac56d7:


    bush "没有人跟他说过话以后还走出去。"


translate chinese day7_main_50e4f830:


    me "我说，我为什么要相信你?"


translate chinese day7_main_aa1b7016:


    "我很不爽。"


translate chinese day7_main_817f4941:


    th "那个少先队员只是不停地讽刺我，但是这个少先队员说的却令人担心。"


translate chinese day7_main_9bad067c:


    th "我不知道自己应该相信哪一个人。"


translate chinese day7_main_e1e025fb:


    th "这就像是让一个盲人猜测屋子里的灯是不是亮着的。"


translate chinese day7_main_355517e6:


    th "他真的只能猜，比如说我..."


translate chinese day7_main_068aa355:


    bush "快点，时间不多了!"


translate chinese day7_main_aff91d3f:


    me "嘿，等一下..."


translate chinese day7_main_77d3e654:


    bush "做出你的选择—你到底想不想离开!?"


translate chinese day7_main_f6c00c2d:


    me "好吧，咱们走。"


translate chinese day7_main_3976ac4c:


    th "不管怎么说他听起来比那个疯狂的少先队员更可信一些。"


translate chinese day7_main_f59377cf:


    th "当然这个解决方法可能是致命的，但是另外一个也不见得好。"


translate chinese day7_main_220c95fc:


    "我只能在黑暗中开一枪..."


translate chinese day7_main_2715a6e1:


    me "所以咱们去哪儿?"


translate chinese day7_main_b3a4212e:


    bush "咱们已经在那儿了..."


translate chinese day7_main_9c4ed245:


    "我的视力开始模糊，我的意识不断地远离自己..."


translate chinese day7_main_300388db:


    me "不，不行，我不能在几分钟之内就完全相信你。"


translate chinese day7_main_92441a59:


    th "至少我更了解那个家伙，他说的话更有道理。"


translate chinese day7_main_026e81de:


    bush "你会后悔的..."


translate chinese day7_main_c9f8c4d3:


    "这些话听起来像是来自另一个世界。"


translate chinese day7_main_c80400a7:


    "声音似乎消失了。"


translate chinese day7_main_99e05e2b:


    "当然，我不能确定自己的选择是不是正确。"


translate chinese day7_main_f36decb5:


    "这是有时间限制的摸黑选择。"


translate chinese day7_main_f9e8cad7:


    th "不管怎么说，我会弄清楚的。"


translate chinese day7_main_2c7b78d5:


    "没过一会儿，我又回到了大家面前。"


translate chinese day7_main_709d3e7d:


    mt "大家都在这里了吗?"


translate chinese day7_main_14167bf5:


    "奥尔加·德米特里耶夫娜开始说着。"


translate chinese day7_main_044ab0d8:


    mt "你们就要离开夏令营了，在分别的时候我有几句话要说。"


translate chinese day7_main_00123e37:


    "她看起来很紧张，不知道该说什么好。"


translate chinese day7_main_019ed77a:


    mt "希望你们在以后的日子中记住在这个夏令营度过的日子，保留着对小猫头鹰夏令营的美好印象。"


translate chinese day7_main_b48fe53b:


    mt "希望你们至少长大了一些，交到了新朋友...{w}明年...再来。"


translate chinese day7_main_42256d78:


    "辅导员看向了旁边，看起来她在努力止住眼中的泪水。"


translate chinese day7_main_80682609:


    "我没想到她也能这么多愁善感。"


translate chinese day7_main_f4683d53:


    "虽然她的讲话还是像平常一样完全就是扯淡。"


translate chinese day7_main_09939b29:


    "少先队员们开始喧闹着挤上公交车。"


translate chinese day7_main_4bd26034:


    "我决定停下脚步，正式的和这座夏令营说一声再见。"


translate chinese day7_main_38f849e4:


    "突然间，我想扔硬币。"


translate chinese day7_main_b4d64ac3:


    "当然这里没有喷泉，我也不想回去，但是我就是想试试自己从来没有试过的迷信仪式。"


translate chinese day7_main_f881a31d:


    "我掏了掏自己的裤兜，只找到了几张糖纸，一根铅笔还有一块纸片。"


translate chinese day7_main_68eaeb3b:


    "我拿着一会儿，然后蹲下来，趴在地上写着。"


translate chinese day7_main_470a435f:


    "“你来这里是有原因的。”"


translate chinese day7_main_4bd38202:


    "我感觉自己很傻，把纸条扔到了汽车下面，然后上了车。"


translate chinese day7_main_d20ef26b:


    "我在中间找到了一个位置。"


translate chinese day7_main_92e96785:


    "但是奇怪的是，大家都是俩俩一对儿的坐着，只有我是自己。"


translate chinese day7_main_c59d06a0:


    th "不过这有什么关系呢?"


translate chinese day7_main_a0b475c4:


    th "我要么会消失，要么会从头再来。"


translate chinese day7_main_a20cefa7_2:


    "..."


translate chinese day7_main_b9800de2:


    "公交车慢慢的朝中心走去，一路上颠颠簸簸。"


translate chinese day7_main_60b0a3f1:


    "漆黑的夜晚透过车上老旧的窗户什么也看不见。"


translate chinese day7_main_6cd4a42b:


    "反正我对外面的风景也完全不在乎，我就坐在这里等着将要发生的事情。"


translate chinese day7_main_637f8d4a:


    "这是很久以来我的大脑第一次什么也不想。"


translate chinese day7_main_1b370f01:


    "我身边的少先队员们很开心的享受着这段旅程。"


translate chinese day7_main_eac2b791:


    "乌里扬卡和阿丽夏在玩牌。"


translate chinese day7_main_18b1408e:


    "列娜在看书，斯拉维娅在睡觉。"


translate chinese day7_main_8be4c497:


    "未来在徒劳的尝试着和热妮娅说话，热妮娅努力的尝试着不要开始杀人。"


translate chinese day7_main_5400ac8b:


    "电子小子和舒里克和平常一样在制造着什么东西。"


translate chinese day7_main_5ae9bf47:


    "我是唯一一个被所有人忽略的。"


translate chinese day7_main_6035b571:


    th "这可能是有点牵强的感觉，我在夏令营里的时候行惯性的认为整个宇宙是围着我转的。"


translate chinese day7_main_5ed13ddb:


    th "可能那时真的从某种程度上来说是的，但是现在我就是一个外星人，一个晶体中异类的分子。"


translate chinese day7_main_a20cefa7_3:


    "..."


translate chinese day7_main_d602b53d:


    "我不知道我们在公路上走了多远，但是我已经越来越困了。"


translate chinese day7_main_7f8e5aa9:


    "我竭尽全力的同睡神进行抗争，想要尽量的醒着。"


translate chinese day7_main_8f87d7c9:


    th "再怎么说，今天可能就是我生命的最后一天了。"


translate chinese day7_main_2fc046f6:


    th "我为了最后几个小时无意义的生命而挣扎..."


translate chinese day7_main_a20cefa7_4:


    "..."


translate chinese day7_main_f6b7f9ad:


    "我突然想起了那一小片纸还有上面的文字。"


translate chinese day7_main_30775a01:


    th "糟糕，我以前见过的!"


translate chinese day7_main_e730bb3c:


    th "我怎么能这么马虎..."


translate chinese day7_main_7a69a252:


    th "也许是这些少先队员们，还有各种说法让我迷糊了..."


translate chinese day7_main_cba5d1e3:


    "我的大脑中充满了各种问题，要爆炸了。"


translate chinese day7_main_ef6d6baa:


    "然后身体上的疲劳还有精神上的疲劳最终胜出，我慢慢的睡了过去..."


translate chinese day7_un_28b2bda0:


    "不知怎么我在自己的公寓里醒了过来。"


translate chinese day7_un_a32e02ac:


    "这没有引起我任何的特殊情感—只是我无聊生活的新一天。"


translate chinese day7_un_7d869d50:


    "我起床爬到电脑桌前。"


translate chinese day7_un_ec63c684:


    "任务栏上闪动着一个消息窗口。"


translate chinese day7_un_4e652ca0:


    "我坐下来，盯着显示器。"


translate chinese day7_un_e275ca5a:


    th "有人给我写了这条消息，这意味着有什么人还需要我。"


translate chinese day7_un_3b3f4f87:


    "上面只显示着一个词语“醒来”。"


translate chinese day7_un_002780e6:


    th "他们想让我干什么？位置地址，这是垃圾邮件吗？再有这个醒来是什么意思...?"


translate chinese day7_un_315e078b:


    th "我不在睡觉了。"


translate chinese day7_un_bd7d69e4:


    "窗口还在不停闪动着。"


translate chinese day7_un_9ba4d0e0:


    "我试图关掉它，但是失败了，它们不停的出现，上面都写着相同的词语。"


translate chinese day7_un_c5553e17:


    me "你到底想让我干什么?!"


translate chinese day7_un_a5efbe41:


    "我喊了起来。"


translate chinese day7_un_39ba5ebd:


    "很快整个屏幕都充满了这则相同的消息。"


translate chinese day7_un_395338c2:


    "我再也受不了了，抓起来砸在了墙上。"


translate chinese day7_un_61465614:


    "电话响了。"


translate chinese day7_un_7411cb8f:


    th "奇怪，这会是谁？"


translate chinese day7_un_191851cc:


    "我拿起听筒，听到了“醒来”。"


translate chinese day7_un_4f1bf480:


    "无数的声音在喊着——男人，女人，孩子..."


translate chinese day7_un_92dce532:


    dreamgirl "醒醒!"


translate chinese day7_un_e6cd0617:


    "我把电话摔在地上，但是它没有摔碎，而是继续响着。"


translate chinese day7_un_b6784bd0:


    "没过一会儿，整个房间都是陌生人。"


translate chinese day7_un_1fe77d6f:


    "他们抓住我的胳膊，盯着我的眼睛喊着，喊着..."


translate chinese day7_un_92dce532_1:


    dreamgirl "醒来!"


translate chinese day7_un_9a3170d8:


    "我一身冷汗的从床上跳了起来。"


translate chinese day7_un_658777a7:


    th "哈...好可怕的梦..."


translate chinese day7_un_f61bd817:


    "我过了好几分钟才回过神，然后回头看看表。"


translate chinese day7_un_0718c34d:


    th "已经下午五点了，我到底睡了多长时间?!"


translate chinese day7_un_cd800d7f:


    "昨天的事情开始闪现在我的眼前。{w} 寻找列娜，糟糕的对话，阿丽夏被打倒..."


translate chinese day7_un_1a354f35:


    th "我真的得和她谈谈，也许经过了一个晚上她能冷静一些。"


translate chinese day7_un_3f9b887f:


    "我穿好衣服，走出房间，犹豫了一会儿，然后前往列娜的宿舍。"


translate chinese day7_un_8e4dce15:


    th "我应该说什么？怎么开口？我不能只是说“你好吗，我路过这里”..."


translate chinese day7_un_061fbae3:


    th "跟她讲道理也不太可能。"


translate chinese day7_un_7e701d1f:


    "我没有想到什么具体的解决办法，敲响了门。没有人回答。"


translate chinese day7_un_6506b798:


    "我又敲了一次，还是没有人。"


translate chinese day7_un_89013315:


    th "我拧动门把手，奇怪，没有锁。"


translate chinese day7_un_1ff94a41:


    "然而里面没有人。"


translate chinese day7_un_fc00e421:


    th "也就是说，列娜不在这里。"


translate chinese day7_un_f9d3aec1:


    th "我去找点吃的，我可能会在食堂看见她。"


translate chinese day7_un_ba6405b9:


    "沿着宿舍区行进的时候，我一个少先队员都没看见。"


translate chinese day7_un_ee95ab11:


    th "这也很奇怪，平常这个时候都很热闹的。"


translate chinese day7_un_1ffe59a2:


    "广场上也没有人。"


translate chinese day7_un_1f9572c5:


    "来到食堂门口，我真的开始担心了，已经到了晚饭时间，但是这里完全没有饿着肚子的少先队员们排队。"


translate chinese day7_un_b2fcee60:


    "希望这只是...食堂关门!"


translate chinese day7_un_71d41a98:


    "看起来白天发生了什么。"


translate chinese day7_un_17d7007b:


    "我在门口坐下来开始思考。"


translate chinese day7_un_df3b23b2:


    "所有的少先队员们都消失了，没有任何警告。"


translate chinese day7_un_51b3e31f:


    th "虽然说我来到这里也不是什么正常的事情，但是这一个星期以来，还没有遇到完全解释不了的事情。"


translate chinese day7_un_2ea6120c:


    th "有很多奇怪的事情，但是..."


translate chinese day7_un_3825b75c:


    me "也许他们都躲在那个堡垒里面..."


translate chinese day7_un_c25b3dcc:


    "我出声的笑了起来。"


translate chinese day7_un_8bc30be9:


    "虽然这一切都很神秘，但是我却完全不紧张。"


translate chinese day7_un_484a3495:


    un "不，只是他们都离开了。"


translate chinese day7_un_2df970b0:


    "我的心脏差点跳出来。列娜站在我旁边。"


translate chinese day7_un_6e7d7566:


    me "你...你...不要这样偷偷接近别人啊！差点给我吓出心脏病!"


translate chinese day7_un_be58af4f:


    un "抱歉..."


translate chinese day7_un_1ee77ef5:


    "她冷静的说着。"


translate chinese day7_un_43dff079:


    "现在我才反应过来，“离开”..."


translate chinese day7_un_bfcc9a41:


    me "等一下，等一下，什么叫“离开”了?!"


translate chinese day7_un_0f9ef342:


    un "夏令营已经结束了。"


translate chinese day7_un_9c158afb:


    me "辅导员呢?"


translate chinese day7_un_e24368b3:


    un "她要在城里处理一些事情，所以和她们一起走了。"


translate chinese day7_un_a04aa514:


    me "好吧，你呢?"


translate chinese day7_un_5e51765d:


    un "我留了下来。"


translate chinese day7_un_9895a7c7:


    "列娜非常冷静的说着，好像这些事情都很正常似的。"


translate chinese day7_un_ae14ffa6:


    me "怎么？为什么?"


translate chinese day7_un_0128f27c:


    un "有什么问题吗？夏令营结束了，孩子们动身回家，这有那里不对吗？"


translate chinese day7_un_630033b9:


    me "可是是什么时候决定的?"


translate chinese day7_un_35db2fba:


    un "你不会相信的，是在第一天，而且奥尔加·德米特里耶夫娜在集合的时候讲过了。"


translate chinese day7_un_06a6d311:


    "是啊，我参加了集合的时候，最不可能做的事就是听奥尔加·德米特里耶夫娜的讲话。"


translate chinese day7_un_a3b16791:


    me "那为什么没有任何人和我说?"


translate chinese day7_un_fd89f04c:


    "我等待离开这个夏令营的机会等了好久，终于等到的时候，我却睡过去了。"


translate chinese day7_un_325ed1b8:


    th "好吧，那真是我自己的风格。"


translate chinese day7_un_49d22b71:


    un "我告诉他们的。"


translate chinese day7_un_b7baf6a3:


    me "即使你这么说，奥尔加...等一下，什么?!"


translate chinese day7_un_49d22b71_1:


    un "这是我的请求。"


translate chinese day7_un_0dc9f9e8:


    "列娜充满自信的说着。"


translate chinese day7_un_ee2b3274:


    me "那个，让他们把我扔下也是你的主意?"


translate chinese day7_un_f1be5fed:


    un "嗯。"


translate chinese day7_un_65cf65b0:


    me "你不觉得那有点..."


translate chinese day7_un_38a6d40e:


    me "{size=+10}完全错误吗？！{/size}"


translate chinese day7_un_8807f63e:


    "我开始喊着。"


translate chinese day7_un_1a28e6a0:


    un "没有，这是很正常的。"


translate chinese day7_un_55f389f6:


    "我仔细看着列娜。"


translate chinese day7_un_13e71061:


    "从一开始她的脸上就没有任何表情。"


translate chinese day7_un_5a1b2d40:


    th "我面前站着的这个女孩似乎完全不是那种会害怕蟋蟀，读到浪漫小说就会害羞脸红的类型。"


translate chinese day7_un_a981adad:


    me "好吧，我懂了。"


translate chinese day7_un_14f2eabe:


    th "看来这就是和我来到这里相关的事情了。"


translate chinese day7_un_0b34c373:


    me "你是谁?"


translate chinese day7_un_0dd1a163:


    un "我? 今天早晨我是列娜。"


translate chinese day7_un_0586ca54:


    "她耐心的回答着。"


translate chinese day7_un_40c0bfd0:


    me "是吗，我是擎天柱，九百万年前出生在苏格兰高地。"


translate chinese day7_un_ebd88aea:


    "我这个时候对这些都完全不害怕。"


translate chinese day7_un_ddce0400:


    "我只是很愤怒。"


translate chinese day7_un_5d53b103:


    un "幸会，我是列娜。"


translate chinese day7_un_6b20d365:


    me "好吧，咱们严肃一点，我觉得你说的完全是不可能的。"


translate chinese day7_un_17a451e0:


    me "今天结果是最后一天，但是我之前完全不知道，辅导员和大家一起走了，把我扔在这里，只是因为你的请求。"


translate chinese day7_un_367d9ad6:


    me "你不觉得这完全不正常吗?"


translate chinese day7_un_4d7961b1:


    un "也许是的..."


translate chinese day7_un_e3b83b37:


    me "你是不是向我隐瞒着什么?"


translate chinese day7_un_515af32c:


    un "也许是的..."


translate chinese day7_un_cdd77ebf:


    me "那你就老实交代!"


translate chinese day7_un_7540469a:


    un "我已经告诉你了。"


translate chinese day7_un_cc572823:


    "我捂住脸，深吸了一口气。"


translate chinese day7_un_db58107e:


    me "好吧，那么我现在到底应该做什么?"


translate chinese day7_un_0da946d4:


    un "我不知道。"


translate chinese day7_un_fd0ec1e9:


    me "那么你要做什么?"


translate chinese day7_un_fe61a603:


    un "我也不知道，这要看情况。"


translate chinese day7_un_aefb602a:


    me "这就是情况。"


translate chinese day7_un_185c618b:


    "我愤愤不平的挥着手。"


translate chinese day7_un_b723e2c5:


    un "你为什么这么在乎?"


translate chinese day7_un_8f9ecda9:


    "她的脸上第一次充满了好奇。"


translate chinese day7_un_e7810ecb:


    me "还能怎样？开心？"


translate chinese day7_un_0005cbde:


    un "没有，或许...但是这是你自己想要的。"


translate chinese day7_un_c9cfb0f4:


    me "我？你疯了!"


translate chinese day7_un_30630964:


    un "昨天谁说的他想要和我在一起?"


translate chinese day7_un_5cc1cbcf:


    me "这有什么关系?"


translate chinese day7_un_741edec3:


    "我假装不明白，不过还是意识到她到底想要说什么了。"


translate chinese day7_un_15749306:


    un "不就是这样吗!"


translate chinese day7_un_3b03af3a:


    me "是啊，当然..."


translate chinese day7_un_c11bed91:


    "我嘀咕着。"


translate chinese day7_un_5a52a4ba:


    me "好吧，我现在是和你一起在这里，但是这还是没有解释发生了什么。"


translate chinese day7_un_4d28d31b:


    un "总会有方法达到目的的。"


translate chinese day7_un_3eac24c4:


    me "然后你...这就是..."


translate chinese day7_un_f1be5fed_1:


    un "是的。"


translate chinese day7_un_e7732512:


    "她的脸上露出了笑容。"


translate chinese day7_un_ac9ef530:


    me "呃，如果我一开始就知道你这么狡猾..."


translate chinese day7_un_4be26182:


    "说实话，我很喜欢这种情况——也说不上是喜欢，但是我很好奇。"


translate chinese day7_un_9f8b7dc3:


    th "当然，没有和大家一起离开夏令营很遗憾，但是我们有了一次促膝长谈的机会。"


translate chinese day7_un_a2a35aab:


    th "也就是说我有机会认识到真正的她。"


translate chinese day7_un_9b37382f:


    "我想要的答案也许就在这里而不是大家都去的城市里。"


translate chinese day7_un_6f985085:


    me "好吧，咱们现在要做什么呢?"


translate chinese day7_un_493d793b:


    un "咱们?"


translate chinese day7_un_d1046a49:


    "她笑了。"


translate chinese day7_un_2a23e310:


    me "是啊，咱们。{w} 咱们是唯一呆在这里的人。{w} 食堂已经关门了，所以我很担心还有没有食物。"


translate chinese day7_un_46a920a9:


    me "当然，奥尔加·德米特里耶夫娜还会回来，但是..."


translate chinese day7_un_0efd4712:


    un "有什么关系?"


translate chinese day7_un_3d127dd4:


    me "好吧，没有关系。有什么建议吗?"


translate chinese day7_un_bdb81d42:


    un "嗯，你想要什么?"


translate chinese day7_un_cd14a0d6:


    "她淘气的笑着。"


translate chinese day7_un_291eb303:


    me "我想要找点吃的...然后，我也不知道了。"


translate chinese day7_un_4af71b46:


    un "好吧，我们去找点吃的。"


translate chinese day7_un_25f7abbc:


    me "去哪里?"


translate chinese day7_un_98dfebb8:


    un "我的房间里应该还有。"


translate chinese day7_un_8d0f1bba:


    me "好吧，那就走吧。"


translate chinese day7_un_58427f30:


    "我们来到了列娜的房间，在抽屉里搜刮着。"


translate chinese day7_un_2fe68f0d:


    un "嗯，有一些饼干，你想要来一些吗?"


translate chinese day7_un_3e7a1ceb:


    "她递给我开了半包的饼干。"


translate chinese day7_un_d37f7637:


    me "哇， 好可爱..."


translate chinese day7_un_27c62366:


    "我冷笑着。"


translate chinese day7_un_008527ca:


    un "为什么?"


translate chinese day7_un_5cbbe2b2:


    me "饼干...就像是我小时候..."


translate chinese day7_un_34ea9cb7:


    un "可以想象。"


translate chinese day7_un_0ac292f9:


    "她微笑着坐在我旁边。"


translate chinese day7_un_f71c90f1:


    me "嗯?"


translate chinese day7_un_3cc85031:


    "我满嘴饼干问着。"


translate chinese day7_un_3875ba7b:


    un "什么?"


translate chinese day7_un_afcb638f:


    me "所以说咱们要做什么?"


translate chinese day7_un_4657d7ce:


    un "我不知道..."


translate chinese day7_un_8063e0c4:


    "她伤感的看着窗外。"


translate chinese day7_un_c52a621b:


    un "可是你想要什么呢?"


translate chinese day7_un_57ffc5a8:


    me "我..."


translate chinese day7_un_bce388b7:


    th "说真的，我到底想要什么?"


translate chinese day7_un_f7a69265:


    "时间过去了半个小时，我唯一的想法就是离开这个鬼夏令营。"


translate chinese day7_un_b783c75e:


    "不过现在，在列娜的身边，似乎有什么改变了。"


translate chinese day7_un_7f75f03b:


    me "嗯...我也不知道..."


translate chinese day7_un_f67c85bb:


    un "努力想一想。"


translate chinese day7_un_6d6679e5:


    "她靠近了一些，盯着我的眼睛。"


translate chinese day7_un_b97ac4d9:


    me "那个，我...你知道..."


translate chinese day7_un_5036ab46:


    "我感觉脸发烫，大脑中充满了各种想法。"


translate chinese day7_un_619d5ebf:


    "不只是因为有一个想要什么的女孩子坐得离我很近，而且因为这个女孩子是列娜。"


translate chinese day7_un_28a06992:


    un "嗯?"


translate chinese day7_un_1b595140:


    "她注视着我。"


translate chinese day7_un_51149e68:


    me "…"


translate chinese day7_un_9e509f53:


    me "列娜，我..."


translate chinese day7_un_3875ba7b_1:


    un "什么?"


translate chinese day7_un_4800ea90:


    "说她看起来不像是自己没有什么意义。"


translate chinese day7_un_aa2b3fa8:


    "我对她的这种变化并不感到害怕，我害怕的是自己在这种情况下到底可以做什么。"


translate chinese day7_un_48199bb7:


    me "我..."


translate chinese day7_un_5bf65010:


    un "你不想和我在一起吗...?"


translate chinese day7_un_609ebc3b:


    "她悄悄的在我耳边说，勾引着我。"


translate chinese day7_un_2dc0d4db:


    me "是...是啊!"


translate chinese day7_un_99cb3823:


    un "那有什么问题吗?"


translate chinese day7_un_5e5a445c:


    me "你应该知道，这太...而且我还有其他的问题..."


translate chinese day7_un_9307c403:


    un "什么问题?"


translate chinese day7_un_182b9bc9:


    "她严肃了起来。"


translate chinese day7_un_f0e8eea2:


    me "那个...{w} 各种... 问题..."


translate chinese day7_un_df31a10a:


    un "你不会喜欢...?"


translate chinese day7_un_d49ccc82:


    "列娜笑了。"


translate chinese day7_un_5c51ac23:


    me "不，你怎么会那么想!"


translate chinese day7_un_eca1df05:


    "我好像更加脸红了。"


translate chinese day7_un_99cb3823_1:


    un "那有什么问题?"


translate chinese day7_un_2f2888eb:


    me "你确定想要吗?"


translate chinese day7_un_feb3b3d8:


    "我尝试变换话题。"


translate chinese day7_un_ba874842:


    "再怎么说，一个星期以前我还在过自己的正常生活，每天都一样的和女孩子完全没有关系的生活。"


translate chinese day7_un_f3bf26c2:


    "现在我在夏令营里，坐在旁边的列娜很明显的在暗示着什么。"


translate chinese day7_un_398b3c5d:


    th "我现在应该怎么办?"


translate chinese day7_un_31b01f67:


    un "嗯..."


translate chinese day7_un_a1cfdd9e:


    "她皱起眉头。"


translate chinese day7_un_57db0c16:


    un "当然了，有什么问题?"


translate chinese day7_un_ae17a426:


    me "只是我..."


translate chinese day7_un_3f78e394:


    "我的举止看起来都不像是十七岁（虽然我看起来是），好像还要小好多一样。"


translate chinese day7_un_d314a10c:


    un "我也一样..."


translate chinese day7_un_d1046a49_1:


    "她笑了。"


translate chinese day7_un_3179bb68:


    "我得决定接下来做什么—反正少先队员们都离开了。"


translate chinese day7_un_43e408be:


    "我得寻找答案。"


translate chinese day7_un_21a442ca:


    "再怎么说我应该知道这个女孩儿连性格都能很容易的改变..."


translate chinese day7_un_1d5c58e7:


    "但是现在我唯一的想法就是“这是什么鬼”!'"


translate chinese day7_un_a755f898:


    me "那个，如果你不在意的话..."


translate chinese day7_un_5cbb3956:


    "我闭上了眼睛。"


translate chinese day7_un_ca7b7ca9:


    "她什么也没有说，只是笑了笑着，然后靠近了一些。"


translate chinese day7_un_0684f7af:


    "我们的嘴唇轻轻贴合，久久的接吻。"


translate chinese day7_un_5233788b:


    "这个瞬间我忘记了一切：我忘记了这个奇怪的夏令营，忘记了已经离开的少先队员们，忘掉了我们疯疯癫癫的辅导员..."


translate chinese day7_un_4c273632:


    "我忘记了我所有的过去和未来，如果我还有的话。"


translate chinese day7_un_bddb8f96:


    "现在我只在乎列娜，她柔软的双唇，她涌入我心底的温暖，燃烧着我的灵魂。"


translate chinese day7_un_d35a905d:


    "我轻轻的推开她。"


translate chinese day7_un_3c8c14d4:


    me "等一下...{w}你不觉得...这有点太快了吗?"


translate chinese day7_un_b672514d:


    un "为什么?"


translate chinese day7_un_d1317e4f:


    "她睁着一双大眼睛看着我，要把我吸进去一样。"


translate chinese day7_un_ef40802e:


    un "你很想要，不是吗?"


translate chinese day7_un_9513cd87:


    me "我..."


translate chinese day7_un_3cb55656:


    "我悄悄说着。"


translate chinese day7_un_a20cefa7:


    "..."


translate chinese day7_un_67d22f65:


    "我感觉自己真心的喜欢列娜，这个瞬间我只想紧紧的抱着她，不想让她离开。"


translate chinese day7_un_b59d5f4a:


    "她也一定有相同的感觉。"


translate chinese day7_un_94abf778:


    "我们融为一体，时间飞速流逝着。"


translate chinese day7_un_a20cefa7_1:


    "..."


translate chinese day7_un_739a1875:


    "当我醒来的时候，外面已经黑下来了。"


translate chinese day7_un_23b365c0:


    "我起来穿上衣服，在屋里走着。"


translate chinese day7_un_f75f09af:


    th "那是什么?{w} 只是动物本能或者什么其他的?"


translate chinese day7_un_601aa36f:


    th "不，这是一个错误！完全错误!"


translate chinese day7_un_98050cd0:


    th "我被困在这个夏令营里了，我失去了离开的机会。而且跟我在一起的这个女孩可能有双重人格还有双相障碍。"


translate chinese day7_un_8f52dae4:


    "我看着正在睡觉的列娜。"


translate chinese day7_un_728cd1d4:


    th "不管怎么说，她是那么美丽。"


translate chinese day7_un_eebc563a:


    "我们之间发生的事不停的在我的大脑中回放，让我感到一阵凉气。"


translate chinese day7_un_7858e570:


    th "唔，我不知道这对不对，但是如果让我回到那时候再选择一次的话，可能还是同样的结果。"


translate chinese day7_un_ec183f94:


    "我开心的坐在列娜的床边。"


translate chinese day7_un_bad_022c8614:


    "快到十点钟了。"


translate chinese day7_un_bad_1b57fb49:


    th "大概到了起床时间了。"


translate chinese day7_un_bad_bfb63a52:


    "我轻轻的推了推列娜的肩膀。"


translate chinese day7_un_bad_c09b685c:


    "她睁开了眼睛。"


translate chinese day7_un_bad_e7c35764:


    me "早上好，好吧，实际上是晚上好。"


translate chinese day7_un_bad_e0a6620a:


    un "你好。"


translate chinese day7_un_bad_c861a148:


    "她温柔的笑着。"


translate chinese day7_un_bad_5c767700:


    me "该起床了，小懒虫。"


translate chinese day7_un_bad_ab39f43d:


    un "你很着急吗?"


translate chinese day7_un_bad_33e2c702:


    me "嗯，没有...但是现在营地里只有咱们两个人..."


translate chinese day7_un_bad_ca19d4ba:


    un "怎么了?"


translate chinese day7_un_bad_1b595140:


    "她认真的看着我。"


translate chinese day7_un_bad_5e428d20:


    me "那个，没什么...{w}奥尔加·德米特里耶夫娜会回来吗?"


translate chinese day7_un_bad_7144803b:


    un "那对你来说很重要吗?"


translate chinese day7_un_bad_cd66338b:


    "她的表情严肃了起来。"


translate chinese day7_un_bad_9e8dd32e:


    me "呃，没有食物的话，咱们都要死在这里了。"


translate chinese day7_un_bad_e0269484:


    "我笑了起来。"


translate chinese day7_un_bad_2a02ff5d:


    un "那么你可以走了。"


translate chinese day7_un_bad_6556b37c:


    "她转了过去，看着墙。"


translate chinese day7_un_bad_b48b240c:


    me "但是我要怎么离开呢?"


translate chinese day7_un_bad_4b07d0b6:


    un "当然是坐公交车了。"


translate chinese day7_un_bad_20781912:


    me "这里没有公交车。"


translate chinese day7_un_bad_ebc83514:


    un "那你觉得这里有一个410路站点是干什么用的?"


translate chinese day7_un_bad_6821f908:


    me "说实话，我不知道。"


translate chinese day7_un_bad_45c95a7d:


    un "我告诉奥尔加·德米特里耶夫娜我需要和你一起处理一些紧急的事情，会晚一点过去。"


translate chinese day7_un_bad_ad02f53a:


    me "什么?"


translate chinese day7_un_bad_1ffdefb7:


    "我好像被闪电击中。"


translate chinese day7_un_bad_cca86205:


    "我不知道自己应该对哪件事情更惊讶—是这里会有公交车经过，还是辅导员会允许两个少先队员肚子留在营地里。"


translate chinese day7_un_bad_b97307f7:


    un "就是这样。"


translate chinese day7_un_bad_53a970d1:


    me "你是说咱们可以离开吗?"


translate chinese day7_un_bad_d6ccd123:


    un "可以啊，没有人拦着你。"


translate chinese day7_un_bad_89256079:


    "她完全一动不动的说着这些话，甚至有点吓人。"


translate chinese day7_un_bad_526a1798:


    me "好吧，抱歉，只是今天发生的事情实在是太令人惊讶了。"


translate chinese day7_un_bad_654ececc:


    un "你几个小时以前可没有这么惊讶。"


translate chinese day7_un_bad_039f555e:


    th "我可能说错了什么...说了完全错误的话。"


translate chinese day7_un_bad_b47ccf80:


    me "那个，别生气...{w}咱们不会就在这里等到世界末日吧，所以说如果能够离开..."


translate chinese day7_un_bad_cf8bd28b:


    "列娜什么都没有说。"


translate chinese day7_un_bad_4cd94bec:


    "我看着她的背影，思考着她在想什么。"


translate chinese day7_un_bad_1385de95:


    un "好吧!"


translate chinese day7_un_bad_3fec7cdf:


    "她兴奋的大声说着，犹豫了一下，然后穿好衣服。"


translate chinese day7_un_bad_d9dd4daf:


    un "去收拾好你的东西，十分钟以后到广场去等我!"


translate chinese day7_un_bad_a2242d63:


    "列娜靠过来，热情的亲吻着我。"


translate chinese day7_un_bad_01822020:


    me "好的。"


translate chinese day7_un_bad_86defb2c:


    "我离开了她的房间，跑向辅导员的房间。"


translate chinese day7_un_bad_6ba4f013:


    "说真的我其实没有什么可收拾的。"


translate chinese day7_un_bad_d7b1f94e:


    "我把我的冬装收进包里，装好自己的手机，然后前往广场。"


translate chinese day7_un_bad_a20cefa7:


    "..."


translate chinese day7_un_bad_623bcbe0:


    "已经过了十五分钟了，但是列娜还是没有来。"


translate chinese day7_un_bad_b4641094:


    "我想她的行李可能比较多，所以需要更长时间。"


translate chinese day7_un_bad_a20cefa7_1:


    "..."


translate chinese day7_un_bad_66dd03fe:


    "可是过了半个小时她还是没有来，让我有点担心。"


translate chinese day7_un_bad_46e4b02b:


    "我还没有明白过来的时候，我的双腿就把我带到了列娜的门前。"


translate chinese day7_un_bad_e0851da8:


    "我推开门，看到列娜躺在床上。"


translate chinese day7_un_bad_1d0de26a:


    "她泡在血泊之中，床单，被子都浸透了鲜血，地板湿漉漉的，列娜的手臂上有一条长长的裂痕。"


translate chinese day7_un_bad_e25e3383:


    "我冲过去摇动着她的肩膀。"


translate chinese day7_un_bad_815e4974:


    me "列娜！列娜！为什么?!"


translate chinese day7_un_bad_630ef49b:


    "她还有一些意识。"


translate chinese day7_un_bad_e89187e1:


    un "你好啊，谢苗。"


translate chinese day7_un_bad_777137e4:


    "无力的微笑凝固在她的嘴唇上。"


translate chinese day7_un_bad_9a95d0f0:


    me "等一下，不要昏过去啊! 我来想办法! 听着，一切都会好的！你不会死的!"


translate chinese day7_un_bad_200625b6:


    "好吧，我自己也没有相信—列娜的伤口从肘部延长到手腕。"


translate chinese day7_un_bad_e56517e3:


    "伤口很深，当我在广场等着她的时候，她已经流了很多血了。"


translate chinese day7_un_bad_df119e7d:


    "现在即使是救护车来了可能也无力回天，而且在这个与世隔绝空无一人的夏令营里，列娜幸存的几率不大于零..."


translate chinese day7_un_bad_ac6d49df:


    me "傻瓜!?"


translate chinese day7_un_bad_57856a8b:


    "我紧紧的抱着她。"


translate chinese day7_un_bad_6d0fc2fd:


    "我的眼泪不断的流着，消失在她的头发之间。"


translate chinese day7_un_bad_1b1fd4fe:


    "我一生都没有这样哭过。"


translate chinese day7_un_bad_ce1f1f67:


    me "傻瓜！为什么要沿着切！别人都是割开，但是你却...!"


translate chinese day7_un_bad_0ab6f7e7:


    un "对不起...{w}已经这样了..."


translate chinese day7_un_bad_5820dcba:


    "她虚弱的喃喃细语。"


translate chinese day7_un_bad_3492de92:


    me "可是为什么？！为什么？！"


translate chinese day7_un_bad_3a8a8420:


    un "我累了...太累了..."


translate chinese day7_un_bad_c2ddb952:


    "列娜安静了下来。"


translate chinese day7_un_bad_abd71de2:


    "I我看着她的眼睛——她还有意识，但是最后一点摇曳的生命也在逐渐消失。"


translate chinese day7_un_bad_19198e2a:


    un "我太累了...{w}带着面具...{w}忍受着...{w}我只是想和你在一起...{w}但是你也离开了我..."


translate chinese day7_un_bad_b1c8ef56:


    me "我哪里都没有去！我就在这里！为什么！你做了什么?!"


translate chinese day7_un_bad_0b0df940:


    un "对不起..."


translate chinese day7_un_bad_68679f3d:


    "我哭得一句话都说不出来。"


translate chinese day7_un_bad_d2f8b2b7:


    un "对不起...{w}我们会...{w}再见的..."


translate chinese day7_un_bad_359cbafc:


    "我紧紧的抱着她。"


translate chinese day7_un_bad_3824bfbf:


    "列娜的呼吸越来越虚弱，慢慢的消失了......"


translate chinese day7_un_bad_4664516e:


    "我受到了惊吓，从床边跳了下来。"


translate chinese day7_un_bad_912ad41d:


    "我眼前一片黑暗，我的心脏疯狂的跳动着，我看到眼前沾满凝血的匕首。"


translate chinese day7_un_bad_d7fd24dc:


    "我把它握在手中。"


translate chinese day7_un_bad_8f9c66f6:


    "刀刃在距离我的手腕几毫米的地方停了下来..."


translate chinese day7_un_bad_8a75b802:


    th "为什么？这有什么意义呢？"


translate chinese day7_un_bad_9c8a7ab2:


    "我呆呆地坐在这里看着列娜。"


translate chinese day7_un_bad_ff9eb876:


    me "不，你没有死。"


translate chinese day7_un_bad_47037f84:


    "我疯狂地笑着。"


translate chinese day7_un_bad_675c0b56:


    me "起来啊，小懒虫，该起床了!"


translate chinese day7_un_bad_31c290d0:


    "我摇动着她的肩膀，轻轻地说着。"


translate chinese day7_un_bad_123bb3db:


    "但是列娜没有醒来。"


translate chinese day7_un_bad_c1836b70:


    me "我...我做了什么?!"


translate chinese day7_un_bad_7f565988:


    "我冲出房间，疯狂的跑着。"


translate chinese day7_un_bad_90732d43:


    "我不知道过了多长时间，直到自己累得再也跑不动，瘫倒在地上。"


translate chinese day7_un_bad_8c7dae16:


    "可怕的寂静包围着我，只有头顶的星星默默地责备着我。"


translate chinese day7_un_bad_9e1de710:


    "就是列娜昨天欣赏过的星星..."


translate chinese day7_un_bad_a5b7f2b5:


    "又一阵痛苦几乎将我撕开。"


translate chinese day7_un_bad_c2891e48:


    th "为什么？！她为什么要这样做？！我要去哪里？！我根本没有要扔下她！"


translate chinese day7_un_bad_871cc013:


    "直到这个时候我才意识到她对我有多么重要。"


translate chinese day7_un_bad_08f145b6:


    "我意识到虽然她的性格那样的怪异，但是今天发生的一切，我们的那一段时光，她已经成了我生命中不能替代的那个人了。"


translate chinese day7_un_bad_daac228f:


    "然后我一听到那个公交车，马上就把她扔在了一边。"


translate chinese day7_un_bad_824a58dd:


    "确实这无法解释她的行为，但是我要怎样才能不再想着她？..."


translate chinese day7_un_bad_ee990b86:


    "我躺了很久，看着星星。"


translate chinese day7_un_bad_a20cefa7_2:


    "..."


translate chinese day7_un_bad_51f4d2a1:


    "树枝在宁静的夜晚轻轻摇动，植物才不会在乎我的感受。"


translate chinese day7_un_bad_a33a26f0:


    "熟悉的景象。"


translate chinese day7_un_bad_3256dae3:


    "我抑制住眼泪，返回营地。"


translate chinese day7_un_bad_e2267550:


    "一切都和昨天，和一周前一样...{w}广场，Genda的雕塑... 少先队员的宿舍...列娜的宿舍..."


translate chinese day7_un_bad_5dd67afb:


    "我的内心几乎崩溃。"


translate chinese day7_un_bad_d4c3458a:


    "内心的痛苦可能随时都会把我撕成碎片。"


translate chinese day7_un_bad_56824cd3:


    "我跪在地上，用拳头砸着地面，直到我的双手完全浸满血渍。"


translate chinese day7_un_bad_54b67798:


    th "要是我能早一点明白过来...{w} 只要提前一点，该有多好..."


translate chinese day7_un_bad_3a000f1c:


    th "她是那么...即使是最微小的一点暗示也能让我明白。"


translate chinese day7_un_bad_425b26e9:


    "我这才明白列娜已经死掉了，同时死掉的还有我的内心。"


translate chinese day7_un_bad_dd13b05c:


    "也许是我最重要的部分..."


translate chinese day7_un_bad_4212818d:


    "过了一会儿我回过神来，站在她的房间。"


translate chinese day7_un_bad_a42b463d:


    "血迹已经干了，不再反射着月光。"


translate chinese day7_un_bad_51c253d9:


    "我坐到床前，紧挨着列娜的尸体。"


translate chinese day7_un_bad_3eddd6fc:


    "我很害怕来到这里，但是我觉得有必要告诉她什么。"


translate chinese day7_un_bad_07109f1d:


    me "对不起。"


translate chinese day7_un_bad_7cd29b00:


    "我开始说到。"


translate chinese day7_un_bad_eefec37d:


    me "现在已经太晚了，当然，不过如果你能听见的话，请你记住，我会永远爱你，直到我死去!"


translate chinese day7_un_bad_73cd14af:


    "这是实话。"


translate chinese day7_un_bad_ab86e84f:


    me "抱歉，我没有在乎你的感受。抱歉，一直只是考虑我自己，所有的事我都对不起你...{w}死掉的应该是我，不是你。"


translate chinese day7_un_bad_41e2281b:


    "我用被子盖住她的尸体，然后慢慢离开了房间。"


translate chinese day7_un_bad_e845b484:


    "我在公交车站前恢复了意识。"


translate chinese day7_un_bad_38f3ab16:


    me "所以想要逃跑啊，你个人渣?"


translate chinese day7_un_bad_02a0d7a5:


    "我暗自对着自己说。"


translate chinese day7_un_bad_5684a1f5:


    th "我忍受不了多在这个夏令营呆一分钟。"


translate chinese day7_un_bad_cfc84616:


    th "列娜再也不会回来了，我无法承受自己所做的一切。{w}我只想等着带我离开这里的公交车。"


translate chinese day7_un_bad_fb75b41c:


    "我一点儿也不在意接下来的一小时还是一天会发生什么。"


translate chinese day7_un_bad_8a6bbc18:


    "我不在乎答案，不在乎我是怎么来到这里的..."


translate chinese day7_un_bad_1167379e:


    "我看到远远的一束灯光。{w}我竟然一点都不惊讶。"


translate chinese day7_un_bad_b2ca87a4:


    "没过多久我已经坐在一辆没有人的410路公交车上，透过一扇破旧的窗户看着漆黑的夜景。"


translate chinese day7_un_bad_238ed2ad:


    "我的内心一片空白。"


translate chinese day7_un_bad_ee87ed80:


    "所有让我们成为人类的东西——感觉，情感，愿望，痛苦，我都丢在那个夏令营里了。"


translate chinese day7_un_bad_27cdd9e3:


    "现在我拥有的一切就是这黑夜还有这辆公交车。"


translate chinese day7_un_bad_26c4841e:


    "没有未来，没有现在。"


translate chinese day7_un_bad_eb03b956:


    "如果我明天死掉，也不过意味着又一个人类的身体不再存在，真正的我，几个小时以前，已经在那里消失了。"


translate chinese day7_un_bad_a20cefa7_3:


    "..."


translate chinese day7_un_bad_6bb89a09:


    "我不知道过去了多长时间，一阵疲倦袭来。"


translate chinese day7_un_bad_75b8b39e:


    "我不准备再去斗争，反正不管我醒着还是睡着也都没有什么区别。"


translate chinese day7_un_bad_61f61677:


    "我再也坚持不住了..."


translate chinese day7_un_good_1e14927c:


    "我完全不想叫列娜起来。"


translate chinese day7_un_good_0f009ba2:


    "一种说不出的享受，看着她睡着的样子。"


translate chinese day7_un_good_cb08a804:


    "人们说睡着的表情揭示出真正的内心。"


translate chinese day7_un_good_655fad3e:


    "我听说某个古老的文明有这种仪式：在结婚之前，一个女子要看着自己的未婚夫睡觉，看三个晚上。"


translate chinese day7_un_good_d9e8de3e:


    "然后再去决定是不是要结婚。"


translate chinese day7_un_good_0d6d6fe6:


    "这么想的话，我感觉列娜身上只有善良，仁慈，还有孩子般的天真。"


translate chinese day7_un_good_d1a6079c:


    th "这真的是昨天打倒阿丽夏的那个女孩子吗?"


translate chinese day7_un_good_416ed528:


    "算了，不管怎么说，事情已经发生了。"


translate chinese day7_un_good_82dadd76:


    "我已经见过那样的列娜，她刚刚见面时的腼腆，在岛上发疯，面对阿丽夏时的愤怒，还有今天对我的热情..."


translate chinese day7_un_good_aa392382:


    th "一个普通人怎么会有这么多完全相反的性格?"


translate chinese day7_un_good_1a2112d1:


    th "也许她真的是双重，三重，甚至四重人格?"


translate chinese day7_un_good_a20cefa7:


    "..."


translate chinese day7_un_good_b36c6fa6:


    "我不知道自己看着列娜睡觉又多长时间了。{w}也许只有几分钟，也许已经几个小时。"


translate chinese day7_un_good_d8d3fddd:


    "最后她终于醒来了。"


translate chinese day7_un_good_c02c0710:


    un "早上好。"


translate chinese day7_un_good_42545412:


    "列娜温柔的微笑着。"


translate chinese day7_un_good_74f1716a:


    un "过来。"


translate chinese day7_un_good_794fe748:


    "她坐起来拥抱着我。"


translate chinese day7_un_good_39d29f37:


    "我没有反抗的扑在了床上。"


translate chinese day7_un_good_5b22e8ef:


    un "你太棒了..."


translate chinese day7_un_good_d8e711d5:


    "她在我耳边轻轻说着，亲吻着我的脖子。"


translate chinese day7_un_good_a6b25fe7:


    me "你也是..."


translate chinese day7_un_good_124fb462:


    "但是我的声音有点犹豫。"


translate chinese day7_un_good_a2aed382:


    un "还想再来吗?"


translate chinese day7_un_good_c57f6bf1:


    "她调皮的说着。"


translate chinese day7_un_good_03e5b22a:


    me "等一下。"


translate chinese day7_un_good_d35a905d:


    "我温柔的把她推开。"


translate chinese day7_un_good_05b8f5af:


    me "你知道，这件事本来很简单。咱们刚刚见面的时候，你看起来像是一个人，然后你看起来像是另外一个人，然后又是另外一个..."


translate chinese day7_un_good_4e07d0a8:


    me "我只是不能理解你到底是怎样的人!"


translate chinese day7_un_good_916ed086:


    me "再看看你是怎么对阿丽夏的..."


translate chinese day7_un_good_16158fef:


    un "又是她..."


translate chinese day7_un_good_32fed232:


    "列娜蒙上被子转了过去。"


translate chinese day7_un_good_9a7e66a7:


    un "你不喜欢我们做的吗？你还想和她再做一次？我不会拦着你了，来拿我们两个比较吧，我把她的地址给你，你说到底谁好！或者我们三个人一起来..."


translate chinese day7_un_good_9c1b1178:


    "她好像没有在说我和她。"


translate chinese day7_un_good_fa7c02f2:


    "好像在说陌生人。"


translate chinese day7_un_good_6d308520:


    me "好吧，又是这样。"


translate chinese day7_un_good_9da1463d:


    "我叹着气。"


translate chinese day7_un_good_7ba67c69:


    me "你今天应该已经明白，我最不想提起的人就是阿丽夏。"


translate chinese day7_un_good_302fa0d4:


    un "好吧，你说得对。"


translate chinese day7_un_good_a1a1f8ca:


    "她转过来微笑着，紧紧的抱着我。"


translate chinese day7_un_good_9aed7767:


    me "等一下...{w}我还是没有明白..."


translate chinese day7_un_good_41cfffd5:


    un "为什么?"


translate chinese day7_un_good_485071ca:


    me "我不能只是...{w}而且，是的！我很在乎，所以我真的想要知道你到底是怎样的人。"


translate chinese day7_un_good_91c10784:


    un "我就是我。"


translate chinese day7_un_good_24e34fe5:


    "她还是那样无精打采的说着。"


translate chinese day7_un_good_b91c9203:


    "我的情感的刹车要失灵了。"


translate chinese day7_un_good_bf337784:


    th "不过我得想办法冷静下来。"


translate chinese day7_un_good_1c3e144c:


    "我站起来坐到对面的床上。"


translate chinese day7_un_good_aed0636a:


    un "好吧..."


translate chinese day7_un_good_0dadd0ec:


    "列娜失落的说着，然后开始穿衣服..."


translate chinese day7_un_good_52eb706a:


    me "我不想问很多，只是有几个问题。"


translate chinese day7_un_good_6e7d937c:


    un "等一会儿，现在该走了。"


translate chinese day7_un_good_8dd5c477:


    "她冷冷的打断了我。"


translate chinese day7_un_good_1d9b8f65:


    me "什么？去哪儿？"


translate chinese day7_un_good_3149b337:


    un "什么去哪儿？当然是去城镇里!"


translate chinese day7_un_good_55d5b035:


    me "但是你告诉我..."


translate chinese day7_un_good_36460bd7:


    un "好吧，我是隐瞒了一些事情，事实上我跟奥尔加·德米特里耶夫娜说我们还有紧急的事情需要处理，所以会晚一点到。"


translate chinese day7_un_good_8a5bbf40:


    me "咱们要怎么去呢?"


translate chinese day7_un_good_d8473fed:


    un "当然是坐公交车。"


translate chinese day7_un_good_2966b125:


    me "这里没有公交车。"


translate chinese day7_un_good_fbd62a8b:


    "我龇牙笑着。"


translate chinese day7_un_good_c78f7540:


    un "什么叫没有公交车？当然有啦！410路啊。"


translate chinese day7_un_good_9491336f:


    "我不知道自己是不是应该相信，现在事态有点失去控制了，但是我不想吵架。"


translate chinese day7_un_good_65bf82fc:


    me "好~吧~..."


translate chinese day7_un_good_0897efd9:


    "我谨慎的回答着。"


translate chinese day7_un_good_a45e6739:


    un "去收拾东西吧，然后过十分钟到广场上等我。"


translate chinese day7_un_good_505d91ae:


    me "好吧..."


translate chinese day7_un_good_345e129a:


    "我没有再问，离开了房间。"


translate chinese day7_un_good_63363cb0:


    "实际上我基本上没有什么可收拾的。"


translate chinese day7_un_good_a692e759:


    "我只是把自己的冬装收进包包里，然后就动身前往广场了。"


translate chinese day7_un_good_9c54c577:


    "列娜已经挎着一个旅行包等着我了。"


translate chinese day7_un_good_8aac883d:


    me "你好像没有带很多行李。"


translate chinese day7_un_good_0562d03f:


    un "这样就足够了。"


translate chinese day7_un_good_06ab0532:


    "她微笑着。"


translate chinese day7_un_good_7373b9f3:


    me "让我帮你拿吧。"


translate chinese day7_un_good_735a4f9a:


    un "请。"


translate chinese day7_un_good_f78985a9:


    "虽然她的包几乎没有重量，但是有一点骑士精神总归是好的。"


translate chinese day7_un_good_e265a3a1:


    "我们慢慢地走着，列娜不停地讲着笑话，各种八卦，有趣的故事，笑个不停。"


translate chinese day7_un_good_0dc27b80:


    th "现在我真的不认识坐在我身边的这个人了。{w}她还是我一个星期以前见到的那个女孩子吗？还是我今天早晨见到的那个女孩子吗？"


translate chinese day7_un_good_4cd8fa22:


    "到达车站的时候，我拿出手机查看时间。{w}出人意料的是，居然还有一点儿电。"


translate chinese day7_un_good_e43133b4:


    "快到晚上十一点了。"


translate chinese day7_un_good_8207f0bf:


    th "这个时间还有公交车吗?"


translate chinese day7_un_good_928da381:


    un "哇，那个是什么?"


translate chinese day7_un_good_d83a6ae8:


    "列娜好奇的问着。"


translate chinese day7_un_good_1ebb2d15:


    me "唔，那个，只是玩具...拿着吧，给你的礼物。"


translate chinese day7_un_good_f12d0ee9:


    un "谢谢!"


translate chinese day7_un_good_b6aed7ee:


    "她微笑着拿走了电话。"


translate chinese day7_un_good_a217d0cd:


    un "要怎么玩?"


translate chinese day7_un_good_48e8a886:


    me "你自己会弄明白的，很简单。"


translate chinese day7_un_good_bb460699:


    "总之一个手机在这里完全无用。"


translate chinese day7_un_good_a20cefa7_1:


    "..."


translate chinese day7_un_good_43fb154a:


    "我们又等了大概半个小时，列娜还在不停地谈着。"


translate chinese day7_un_good_21403291:


    "好吧，我必须承认她的故事很有意思，和她呆在一起很舒服，不过这公共汽车到底在哪里?"


translate chinese day7_un_good_71f2297f:


    me "所以说关于..."


translate chinese day7_un_good_7b804235:


    "我还没有说完，突然看到了远远的光亮。"


translate chinese day7_un_good_083d605d:


    un "啊，来了!"


translate chinese day7_un_good_7f656032:


    "列娜激动地说着。"


translate chinese day7_un_good_429932d4:


    "公交车缓缓地把我们带离小猫头鹰夏令营，我只是想要相信我们不必再回来。"


translate chinese day7_un_good_c996f2ed:


    "窗户那边漆黑的夜晚让我们什么都看不清，道路，森林，原野，事实上，它们可能都已经不存在了，我们只是穿越在真空的时空隧道里前往未知的世界。"


translate chinese day7_un_good_c2afe0a0:


    "不管怎么说，我一点也不在乎，我只是专心的听着列娜的故事。"


translate chinese day7_un_good_357a77a9:


    "也许她今天一天说过的话已经超过了过去的总和。"


translate chinese day7_un_good_9bbf9c21:


    me "你知道..."


translate chinese day7_un_good_b756babd:


    "我最终打断了她。"


translate chinese day7_un_good_726d51ff:


    me "我还是不明白..."


translate chinese day7_un_good_a604848c:


    un "什么事情?"


translate chinese day7_un_good_06ab0532_1:


    "她微笑着。"


translate chinese day7_un_good_cbb2f3bd:


    me "你刚开始那么犹豫腼腆，两个词都说不出来，但是现在...这样..."


translate chinese day7_un_good_fcc06fdd:


    un "这很重要吗?"


translate chinese day7_un_good_f8a3ddee:


    me "是的，对我来说是的!"


translate chinese day7_un_good_88bb1318:


    un "好吧..."


translate chinese day7_un_good_d92703f2:


    "她回答之前深吸了一口气。"


translate chinese day7_un_good_440c732d:


    un "你应该能感觉出来，我一直都是我们第一次见到时的那个样子...在公共场合。"


translate chinese day7_un_good_736c9df5:


    un "我从小就无法按照自己想要的方式生活，所以我不得不带上面具..."


translate chinese day7_un_good_b7d1c016:


    "她沉默了。"


translate chinese day7_un_good_0ff7e242:


    un "虽然...我们还是不要太深究了!"


translate chinese day7_un_good_61aec53e:


    "列娜笑着，挽着我的手臂，紧紧的投入我的怀中。"


translate chinese day7_un_good_def14ee4:


    me "好吧，我明白了..."


translate chinese day7_un_good_8e15585f:


    "对她来说可能确实很难谈起这个问题。"


translate chinese day7_un_good_c1c67ccd:


    "总之，即使是这么简短的回答，我还是了解了很多。"


translate chinese day7_un_good_6d3e0546:


    me "但是...我能确定你不会再变成那个样子了吗...?{w} 你不会再躲进自己的面具之中，不会再那样生气?"


translate chinese day7_un_good_daaa0105:


    un "那取决于你了。"


translate chinese day7_un_good_bf744da8:


    "列娜狡猾的笑着。"


translate chinese day7_un_good_1a37d158:


    "我现在才明白我开始了一场勇敢者的游戏。"


translate chinese day7_un_good_62d42734:


    th "一方面我观察了一个星期到底自己是在什么世界里，我没有地方可去，什么都不明白，见鬼!"


translate chinese day7_un_good_8ab802ed:


    th "另一方面，这个女孩子显然对我如此重要..."


translate chinese day7_un_good_5c4556e4:


    "我几乎都不用想自己对列娜是不是有感情。"


translate chinese day7_un_good_0baeb136:


    "我只是想在她的身边，看着她，听着她说话，我喜欢她本来的样子。"


translate chinese day7_un_good_fa770e07:


    "我向自己确认，我面前的这个人真的就是列娜。"


translate chinese day7_un_good_10603e2c:


    "现在我的这些想法，关于我过去的生活，我如何离奇的出现在了80年代的夏令营中..."


translate chinese day7_un_good_fc2a9652:


    th "现在不是想这些的时候!"


translate chinese day7_un_good_8b4d1244:


    th "我现在在她的旁边感觉很幸福，我哪里也不想去，也不想要什么答案，我不愿意想明天会发生什么事情!"


translate chinese day7_un_good_2b03d3cf:


    me "如果让我决定的话，我只想相信你会一直是这个样子，因为我喜欢这个样子的你!"


translate chinese day7_un_good_f3f46bb0:


    un "只要我和你在一起，我就会一直这样!"


translate chinese day7_un_good_6dd013b2:


    "她更用力的抱着我。"


translate chinese day7_un_good_a20cefa7_2:


    "..."


translate chinese day7_un_good_46979785:


    "我不知道我们在公路上走了多远，但是列娜已经逐渐消停下来了。"


translate chinese day7_un_good_9a1a8533:


    "她把头搭在我的肩膀上，还在讲着一个故事，说她的猫进入一个睡觉机器，晕晕的遇到了末日。"


translate chinese day7_un_good_a7bfc79d:


    "我自己也开始不停地打瞌睡。"


translate chinese day7_un_good_ad535286:


    me "不过说起来，你为什么要打阿丽夏。?"


translate chinese day7_un_good_20b153f9:


    "她靠近了，严肃地看着我。"


translate chinese day7_un_good_bb6fada3:


    un "我一直都讨厌那个小婊子！!"


translate chinese day7_un_good_bab1178b:


    "列娜大声笑着，钻进我的肩膀。"


translate chinese day7_un_good_a20cefa7_3:


    "..."


translate chinese day7_un_good_a3e8e773:


    "我尽力的试图不要睡着，未知的前方在等待着我们，在公共汽车的下一个转弯处。{w}我的面前也许是崭新的生活，或者是我们童话的结局——然后我就会被埋葬在破旧的棺材里，扔到自己的老公寓里。"


translate chinese day7_un_good_115465f3:


    "但是我快要输掉这场战斗了，睡神召唤出无数的怪物在疲倦，劳累，孤独和犹豫的指挥下，向我袭来。"


translate chinese day7_un_good_349e1321:


    "我无法战胜天启的四大骑士，最后我睡着了。"


translate chinese day7_us_a1b824b0:


    "在一个小女孩的怀抱中睡着，这一定不是生命中最差的事情。"


translate chinese day7_us_85a98e77:


    "现在门打开了，有武装人员向我们冲过来。"


translate chinese day7_us_b1bd846f:


    "我听不清他们到底在喊什么，但是他们显然来者不善。"


translate chinese day7_us_a52663fb:


    "不，我一点也不害怕，反而是有点尴尬，甚至是羞耻。"


translate chinese day7_us_dea9e5b0:


    "当我睁开眼睛的时候我用了一段时间才搞清楚自己在什么地方。"


translate chinese day7_us_aea7d683:


    "这里很暗，只有地面上一点模糊的灯光。"


translate chinese day7_us_7be5da68:


    th "他们为什么要建造没有窗户的储藏室?"


translate chinese day7_us_22623e57:


    th "虽然这是显然的...但是他们为什么要在这里放一台电视还有VCR?"


translate chinese day7_us_b5ec365a:


    "我暗自诅咒着设计这座房子的建筑师，摇动着乌里扬卡的肩膀。"


translate chinese day7_us_8cb5ffe6:


    me "快起来啦!"


translate chinese day7_us_80c5bc91:


    "她伸伸懒腰，因为我们躺得很近，所以即使是在黑暗中我也能清晰的看见她的眼睛。"


translate chinese day7_us_6d998213:


    us "啥...? 让我睡觉!"


translate chinese day7_us_04545e87:


    "乌里扬娜转了过去，但是我紧紧的抓住了她的肩膀。"


translate chinese day7_us_77910b1b:


    me "我不知道现在几点了，但是别管了，咱们现在离开这里最好。"


translate chinese day7_us_bc4db99c:


    us "别啊，等一会儿。"


translate chinese day7_us_eaa8c8b3:


    "她半睡半醒的轻声说着。"


translate chinese day7_us_6089f3a9:


    me "我说起来，快走啦!"


translate chinese day7_us_56ce9d85:


    "我轻易的把她拎了起来。"


translate chinese day7_us_47c27e5d:


    us "呜咕~"


translate chinese day7_us_c0371831:


    "乌里扬卡悲痛的嚎叫着。"


translate chinese day7_us_748c57a1:


    "我开始寻找电灯开关，不过突然听到了脚步声。"


translate chinese day7_us_027c68b0:


    "我的心脏沉了下去。"


translate chinese day7_us_f9b338df:


    el "怎么这么早?"


translate chinese day7_us_8023c69c:


    sh "我们有一卡车的工作呢，不能再晚了! 你知道我们要在离开之前干完这些活计。"


translate chinese day7_us_324f3179:


    el "好吧，好吧..."


translate chinese day7_us_918870bf:


    "看来我们精力充沛的两个电子技术宅天还没亮就来了。"


translate chinese day7_us_555a7855:


    me "安静!"


translate chinese day7_us_413ae5b6:


    "我对乌里扬卡悄悄说着。"


translate chinese day7_us_7d2c0a44:


    us "这是什么..."


translate chinese day7_us_a7fcd40d:


    "她没有说完，我堵住了她的嘴。"


translate chinese day7_us_5f863e08:


    el "好吧，这我都明白，但是咱们至少也得等到早饭!"


translate chinese day7_us_ac12d769:


    sh "啊，你还有别的事情?"


translate chinese day7_us_62543f01:


    el "没有..."


translate chinese day7_us_6fe0e4ad:


    "电子小子似乎有一点犹豫。"


translate chinese day7_us_94943518:


    sh "你今天不想一大早去图书馆吗?"


translate chinese day7_us_f9592fdf:


    el "一点这个想法也没有..."


translate chinese day7_us_bc927332:


    "他焦躁的说着。"


translate chinese day7_us_357f75f0:


    sh "是啊，如果你这么说的话。"


translate chinese day7_us_327920bd:


    "很快他们开始了工作，我听到锤子砸下来的声音，机器的响声还有电流之类的声音。"


translate chinese day7_us_2ca031ea:


    "电子小子和舒里克在讨论他们自己的事情，所以我也没有在意。"


translate chinese day7_us_81c60560:


    "我对他们打算什么时候离开这里更感兴趣。"


translate chinese day7_us_28532eec:


    th "很快，早饭时间就要到了。但是如果考虑到舒里克的热情..."


translate chinese day7_us_e1c95826:


    us "放开我!"


translate chinese day7_us_417eb8f9:


    "乌里扬卡终于挣脱开了，不过没有提高声音。"


translate chinese day7_us_00c87776:


    us "咱们为什么不能直接走?"


translate chinese day7_us_6c39322b:


    "她悄悄的问着。"


translate chinese day7_us_60ae17bd:


    me "你说呢——你觉得这没有问题?"


translate chinese day7_us_38311340:


    us "有什么问题?"


translate chinese day7_us_9ca91be8:


    me "好吧，我们一整晚都在这里..."


translate chinese day7_us_2bcd45b7:


    th "想想我把门锁上了，他们会怎么看。"


translate chinese day7_us_cbf4636b:


    us "所以呢?"


translate chinese day7_us_ba0e85e5:


    me "什么所以呢?"


translate chinese day7_us_1fd5a2a5:


    "我疲倦的叹着气。"


translate chinese day7_us_2c1c12f9:


    me "相信我！我知道那对你没有什么关系！但是那对我有关系!"


translate chinese day7_us_8b8ddb79:


    us "好吧，好吧，咱们继续在这里躲着。"


translate chinese day7_us_dcea2b7f:


    "乌里扬娜不爽的回答着。"


translate chinese day7_us_c441e2a5:


    "我还打算继续呆一段时间，但是显然前门打开了，有人走了进来。"


translate chinese day7_us_ea040539:


    sl "早上好啊，伙计们!"


translate chinese day7_us_073bef57:


    el "早上好!"


translate chinese day7_us_c6c5df39:


    sh "你好!"


translate chinese day7_us_c50dbd79:


    "是斯拉维娅。"


translate chinese day7_us_36054e3a:


    sl "我只是想问你有没有正好带着胶带?"


translate chinese day7_us_a716e0cd:


    sh "我们有...在某个地方..."


translate chinese day7_us_1758adf3:


    "舒里克若有所思的回答着。"


translate chinese day7_us_837638ee:


    el "啊，去里屋储藏室找找吧!"


translate chinese day7_us_d9880b2f:


    "这话让我起了一身鸡皮疙瘩，我紧紧的抓住门把手。"


translate chinese day7_us_a3e6a343:


    "斯拉维娅走了过来试图开门，但是我在里面用尽全身的力气挡住。"


translate chinese day7_us_9daed956:


    sl "被锁上了。"


translate chinese day7_us_338f90c7:


    el "不可能，我们从来不会锁那扇门!"


translate chinese day7_us_96e3c2da:


    sh "让我试试。"


translate chinese day7_us_9a13cece:


    "舒里克使劲的拉动着门把手，但是什么也没有发生，虽然我用了好大力气才维持住。"


translate chinese day7_us_28d6326b:


    sh "啊，卡住了，帮我一把!"


translate chinese day7_us_8e62bdbd:


    "过了一会儿，他们和电子小子一起用力拧动把手。"


translate chinese day7_us_3c57cbd3:


    "我用上毕生的力量，赌上自己的性命，握紧了把手，然后我坚持不住放弃了。"


translate chinese day7_us_4a18af52:


    "大门砰的一下打开了，刺眼的阳光让我什么也看不见，也没有看见斯拉维娅、舒里克和电子小子惊讶的表情，直到过了好几秒钟才恢复过来。"


translate chinese day7_us_0cb3a629:


    el "呃... 早上好!"


translate chinese day7_us_9b07033b:


    us "好..."


translate chinese day7_us_db8c0cef:


    "乌里扬卡站在我的身后，我不知道她是什么表情，但是从声音里能感觉到有点不好意思。"


translate chinese day7_us_f3289a98:


    sh "你在这里做什么?"


translate chinese day7_us_e4baa3e6:


    "舒里克好像一点也不奇怪似的问着。"


translate chinese day7_us_b0e31fb0:


    me "额...说实话，我们是来这里看电影的，乌里扬卡带来一盘录像带，然后我们借用你们的VCR..."


translate chinese day7_us_f20523f5:


    "舒里克一脸怀疑的看着。"


translate chinese day7_us_2affcd83:


    "我轻轻的推了推乌里扬卡，然后她开始播放那部电影。"


translate chinese day7_us_a303a91b:


    el "那是什么电影?"


translate chinese day7_us_05d05fbf:


    "电子小子脸上似乎闪过一丝看不清的笑容。"


translate chinese day7_us_8f6c89a0:


    me "只是一个普通的电影，啊，是惊悚片，最新的惊悚片!"


translate chinese day7_us_816f2706:


    "我在思考他们在想什么，让我感觉很愤怒。"


translate chinese day7_us_8ccefc46:


    me "如果你觉得...我们...完全不是那样的!"


translate chinese day7_us_48e6007e:


    "我指指乌里扬娜。"


translate chinese day7_us_d2a8f0f0:


    "她严肃的看着我。"


translate chinese day7_us_82fab5b1:


    sl "没有人指责你任何事..."


translate chinese day7_us_f3e5d2d3:


    "斯拉维娅没有看着我回答着..."


translate chinese day7_us_3129c89c:


    me "至少这里有一个理智的人!"


translate chinese day7_us_cc54c296:


    sl "不过..."


translate chinese day7_us_7dd3d65d:


    "她轻声说着。"


translate chinese day7_us_ad02f53a:


    me "什么?"


translate chinese day7_us_47020024:


    us "一切都正如他所说。"


translate chinese day7_us_29d44d62:


    "乌里扬娜加入了对话。"


translate chinese day7_us_0313b033:


    us "我们只是在看电影，然后我感觉很困...然后..."


translate chinese day7_us_daef1297:


    el "我们什么也没有想，只是一个愚蠢的处境，行了吧..."


translate chinese day7_us_e3a1d5a1:


    "电子小子试图用笑话缓解气氛。"


translate chinese day7_us_a17efb6c:


    sl "我觉得这件事应该交给奥尔加·德米特里耶夫娜判断。"


translate chinese day7_us_2ff596a2:


    "斯拉维娅冷静地说。"


translate chinese day7_us_04e9a59a:


    me "喂，等一下，为什么要把辅导员扯进来?"


translate chinese day7_us_9f99e4ab:


    sl "不然还能是谁?"


translate chinese day7_us_2b3752c2:


    me "但是你看，我们在说实话!"


translate chinese day7_us_180a5d04:


    sl "我没有权力判断..."


translate chinese day7_us_2d71c574:


    me "见鬼！那还能是谁！？你们自己都看得清清楚楚!"


translate chinese day7_us_97f0d6ac:


    sl "这要让辅导员来决定。"


translate chinese day7_us_11681176:


    "斯拉维娅平静的说着，然后便离开了。"


translate chinese day7_us_b93ba56b:


    me "我说等一下!"


translate chinese day7_us_279248eb:


    "我纵身一跃挡住了她的去路。"


translate chinese day7_us_04377996:


    me "听着!"


translate chinese day7_us_4c8b51f3:


    sl "这和你没有关系..."


translate chinese day7_us_a74cf5b3:


    "斯拉维娅试图避开我的目光。"


translate chinese day7_us_a9ab19d6:


    th "似乎她也对这个情况感觉有点尴尬。"


translate chinese day7_us_5b85d78d:


    sl "我只是有义务…"


translate chinese day7_us_c7f85ff8:


    me "你对谁有义务？你为什么要这么做？"


translate chinese day7_us_a6fdad47:


    sl "因为..."


translate chinese day7_us_60e65045:


    "她不知道该说什么好。"


translate chinese day7_us_aca23380:


    me "所以说，你不能走，你不用跟任何人说这些事！"


translate chinese day7_us_37eba565:


    sl "不..."


translate chinese day7_us_c72cecb4:


    "她紧张的说着，不过接下来抬起头，严肃的注视着我。"


translate chinese day7_us_19d8e410:


    sl "抱歉，谢苗。"


translate chinese day7_us_fcafb3be:


    us "放开她，让她走吧。"


translate chinese day7_us_9a93eedf:


    "我转过头看着乌里扬娜，这个工夫她已经冲出来了。"


translate chinese day7_us_b93ba56b_1:


    me "等一下！等一下!"


translate chinese day7_us_c3edfb53:


    "我对她喊着，但是没有用。"


translate chinese day7_us_10f920b2:


    th "阻止斯拉维娅去见辅导员也是没有意义的，如果她执意要去，我也是没有办法的。"


translate chinese day7_us_f94a034f:


    us "反正她什么也证明不了。"


translate chinese day7_us_9367d115:


    "乌里扬卡咯咯地笑着。"


translate chinese day7_us_271eed32:


    me "谁在乎她到底能不能证明啊，话说你到底明白不明白自己是什么处境?"


translate chinese day7_us_de872d99:


    me "不管遇到什么事，你已经注定是有罪的了，而且这次又是这种特殊的情况..."


translate chinese day7_us_4becc777:


    us "那就咱们都有责任吧。"


translate chinese day7_us_5345aad1:


    "她淘气的傻笑着。"


translate chinese day7_us_0c53b718:


    me "就是..."


translate chinese day7_us_767711c4:


    "我离开了房间，坐在楼梯上。"


translate chinese day7_us_ca5d2b01:


    el "那个...我们会和你站在一边，如果需要的话，是不是，舒里克?"


translate chinese day7_us_f7843a33:


    sh "是吧...我没有明白到底发生了什么，但是应该没有什么值得担心的。"


translate chinese day7_us_06498067:


    el "好吧，我们要去吃饭了!"


translate chinese day7_us_4c751a5e:


    "很快我们就看不见他们了。"


translate chinese day7_us_b451f12c:


    us "咱们也去吃饭吧!"


translate chinese day7_us_2dcff881:


    "乌里扬卡兴奋的说着。"


translate chinese day7_us_1cbe0f5f:


    me "你脑子里只有吃!"


translate chinese day7_us_439a68cf:


    us "要不然呢？在这哭个不停？那也不能解决问题。"


translate chinese day7_us_42da4d87:


    "至少她在这一点上还是对的。"


translate chinese day7_us_87f2529e:


    th "我们只能等着辅导员的决定。"


translate chinese day7_us_52e891ef:


    me "咱们先走吧..."


translate chinese day7_us_e5f23980:


    "我们前往食堂。"


translate chinese day7_us_e64b885e:


    "整个营地的少先队员们都来到了这里，除了奥尔加·德米特里耶夫娜和斯拉维娅。"


translate chinese day7_us_29b1cc7a:


    th "我想这也许是最好的情况。"


translate chinese day7_us_eabb99b9:


    us "你在想什么?"


translate chinese day7_us_30a8c2b2:


    "乌里扬卡一边吃着饭一边兴奋地问我。"


translate chinese day7_us_df42402e:


    me "还是那件事。"


translate chinese day7_us_682ca4a2:


    us "哎呀，别再担心啦!"


translate chinese day7_us_25e396e3:


    me "也许对你来说没什么..."


translate chinese day7_us_7ac5a652:


    th "不过，说真的，到底有什么可担心的?"


translate chinese day7_us_dd7a2cb9:


    th "每件事都可以从不同的角度来看，特别是如果你有什么理由的话..."


translate chinese day7_us_126beeed:


    us "算了吧，最坏能有什么后果?"


translate chinese day7_us_44eab9f2:


    me "你知道，咱们的辅导员，她有点怪。"


translate chinese day7_us_1a257db4:


    us "呃...好吧，但是咱们没有做任何事，那种事情!"


translate chinese day7_us_6ad7fada:


    me "我希望她也可以相信..."


translate chinese day7_us_edf1846c:


    "我和乌里扬卡的关系最近戏剧性的改变了。"


translate chinese day7_us_88166c1e:


    "刚开始我只觉得她是一个淘气又不懂礼貌的孩子，但是现在我开始发现她性格中好的一面。"


translate chinese day7_us_f3d379ac:


    "虽然不是很多..."


translate chinese day7_us_f6c5d224:


    "现在就当所有的事情都要完美的解决的时候，一场不能避免的和辅导员的对决就要开始了。"


translate chinese day7_us_da1b7a68:


    th "也许这是有点难以理解，但是斯拉维娅的表现只是因为惊讶..."


translate chinese day7_us_a1bda0c8:


    mt "吃的还好吗?"


translate chinese day7_us_2515daf1:


    "奥尔加·德米特里耶夫娜突然出现在了我的面前，她凶狠的看着我。"


translate chinese day7_us_df6dbf74:


    us "是的。"


translate chinese day7_us_ea23ce21:


    mt "你想不想解释一下?"


translate chinese day7_us_00c2b734:


    me "解释什么?"


translate chinese day7_us_1a0200f5:


    mt "比如说你是怎么碰巧进入机器人社团的储藏室？你从哪里拿到的钥匙？为什么去那儿？"


translate chinese day7_us_40981815:


    "我想起了自己拿到这个被诅咒的钥匙的时刻。"


translate chinese day7_us_03126c7a:


    th "我应该问问电子小子那是怎么回事。"


translate chinese day7_us_75eb26f7:


    me "如果斯拉维娅和你说了的话..."


translate chinese day7_us_581b4f29:


    us "我们在看电影!"


translate chinese day7_us_483db5e3:


    "乌里扬娜严肃地说。"


translate chinese day7_us_7996582c:


    mt "整个晚上?"


translate chinese day7_us_17187bef:


    "辅导员讽刺的说。"


translate chinese day7_us_8882dbb1:


    us "然后我就睡着了，谢苗陪着我来着。"


translate chinese day7_us_e8218d47:


    mt "所以你们没有机会回到自己的房间去吗?"


translate chinese day7_us_d3ad4e8d:


    us "一点也没有!"


translate chinese day7_us_5f234a66:


    mt "你要说什么?"


translate chinese day7_us_db73f201:


    me "这听起来虽然很愚蠢，但是她说的是对的。"


translate chinese day7_us_73f28212:


    mt "你觉得我会相信?"


translate chinese day7_us_5232e44c:


    me "呃，这可是事实..."


translate chinese day7_us_28860bdc:


    mt "我没法指控你什么..."


translate chinese day7_us_f9fe8dfb:


    "辅导员慢慢的开始说。"


translate chinese day7_us_78c9d534:


    mt "不过从另一方面来说，这整个事件都不正常，少先队员不可以这样做！{w}而且你们的故事里矛盾太多！"


translate chinese day7_us_c7395410:


    me "我们明白了..."


translate chinese day7_us_64ed00c4:


    "我没有反抗的回答着。"


translate chinese day7_us_ed3d54bd:


    mt "我要做出如下的处罚：乌里扬娜，你被禁足了，你要在自己的房间里呆着不许出来，之后我再决定怎么处理你。"


translate chinese day7_us_2b2111cc:


    "我注意着乌里扬卡，出乎我的意料，她似乎一点也不失落。"


translate chinese day7_us_b346132a:


    mt "我觉得这不是你的错误，但是公平的说..."


translate chinese day7_us_9ac1f10f:


    us "太好了！你知道到哪里去找我!"


translate chinese day7_us_21586975:


    "她马上站起来往出口跑去。"


translate chinese day7_us_601dcc29:


    "奥尔加·德米特里耶夫娜没有试图拦住她。"


translate chinese day7_us_f86f5ead:


    mt "她总是制造麻烦！这次还把你..."


translate chinese day7_us_463bcf35:


    me "我不明白她把我怎么样了，而且你看起来对她太苛刻了。"


translate chinese day7_us_86c24609:


    mt "你到哪儿去找一个跟她一样的少先队员!"


translate chinese day7_us_9b510cae:


    "辅导员笑了起来。"


translate chinese day7_us_d8a59e1a:


    me "当她表现不好被禁足那是另外一件事，但是现在..."


translate chinese day7_us_b0a68be7:


    mt "事实上我不太清楚到底发生了什么，但是关键的是我要让你好好做人! 而这个情况很可疑！十分可疑！"


translate chinese day7_us_e6d5b5bb:


    me "那你要关她多久啊?"


translate chinese day7_us_2c3aefa4:


    mt "我不知道..."


translate chinese day7_us_c8efaa8d:


    "奥尔加·德米特里耶夫娜思考了一段时间。"


translate chinese day7_us_aa5a8210:


    mt "今天就应该离开了，但是在这种情况下..."


translate chinese day7_us_015aa4b8:


    me "什么？什么离开？"


translate chinese day7_us_e5e97d5d:


    "我跳了起来。"


translate chinese day7_us_2a286c8b:


    mt "夏令营结束了，今天是最后一天。"


translate chinese day7_us_d3383672:


    me "啥...?"


translate chinese day7_us_ca88a665:


    "我只相处了这么一句答案。"


translate chinese day7_us_95d81bde:


    th "最后一天...就是说，我终于可以离开这个该死的夏令营了!"


translate chinese day7_us_148a37fc:


    th "也许我受难的日子要结束了，马上就可以回到正常的生活了?"


translate chinese day7_us_d94530d9:


    me "但是为什么这么突然?"


translate chinese day7_us_484d29a2:


    mt "怎么突然？我在集合的时候讲过!"


translate chinese day7_us_b7dab458:


    "她讲的很有道理，在列队集合的时候我不是在睡觉就是东张西望，反正没有认真听讲。"


translate chinese day7_us_230b0b30:


    me "具体是什么时候?"


translate chinese day7_us_f2dd2ae4:


    mt "五点钟，不要忘了做好准备!"


translate chinese day7_us_b8beaef4:


    th "我基本上没有什么可收拾的。"


translate chinese day7_us_d6c3fd46:


    "她站了起来，拿起盘子，准备离开。"


translate chinese day7_us_10a9cce9:


    me "乌里扬娜会怎么样?"


translate chinese day7_us_24e7f6f2:


    mt "我告诉过你了，我不知道，她大概会晚一些离开。"


translate chinese day7_us_a1e9eded:


    me "怎么？不和大家一起？"


translate chinese day7_us_4859697b:


    mt "嗯，是的。"


translate chinese day7_us_9e33a049:


    me "那很正常吗?"


translate chinese day7_us_cb6d083f:


    "我很惊讶。"


translate chinese day7_us_9ee92636:


    mt "这有什么问题吗?"


translate chinese day7_us_e037db1b:


    th "我不知道有什么问题，不过这确实是很奇怪。"


translate chinese day7_us_4cedb800:


    mt "好了，我还有工作。"


translate chinese day7_us_a20cefa7:


    "..."


translate chinese day7_us_a075466c:


    "我漫无目的的盛着粥，已经凉了好久了。"


translate chinese day7_us_6d8e9a1c:


    th "离开这里，我有可能离开这个鬼地方了!"


translate chinese day7_us_d58d3bbd:


    th "但是说起来，乌里扬娜..."


translate chinese day7_us_ddc689f2:


    "我感觉愧疚。"


translate chinese day7_us_0b1acb99:


    th "结果她被惩罚了，而我没有，这不太公平。"


translate chinese day7_us_2b277b74:


    "不过我倒不是想和她有难同当，但是我觉得她自己被关在那里很可怜。"


translate chinese day7_us_4de1adb3:


    th "距离出发还有很长时间，应该可以趁机把事情搞清楚。"


translate chinese day7_us_743c2089:


    "我决定首先和斯拉维娅谈谈。"


translate chinese day7_us_90ef6742:


    th "我希望她已经冷静下来了。"


translate chinese day7_us_246c591f:


    "我习惯在广场看见斯拉维娅了，所以我毫不犹豫地前往那里。"


translate chinese day7_us_21e83890:


    th "为什么是那里？因为绝大多数情况下我都是在那里看见她。"


translate chinese day7_us_b960cd1c:


    "但是Genda的领域内没有任何人烟。"


translate chinese day7_us_bab2fcfe:


    "我呆呆地盯着雕塑看了一会儿，然后前往图书馆。"


translate chinese day7_us_645d78b5:


    "热妮娅还是很有可能知道自己的邻居在什么地方的。"


translate chinese day7_us_d20a5a40:


    "我敲了敲门，然后走了进去。（根据我的经验，这个环节是不可忽略的。）"


translate chinese day7_us_a715c94f:


    "热妮娅的注意力从书本转移到了我这里。"


translate chinese day7_us_6705d775:


    mz "你想要什么?"


translate chinese day7_us_b3e89296:


    me "发生了什么？为什么这种态度？我连进来都不行了？"


translate chinese day7_us_3f35d5c7:


    mz "怎么？你为了进来而进来吗？我很怀疑。"


translate chinese day7_us_ff471f9a:


    me "那个，不是..."


translate chinese day7_us_dd5de5c4:


    "马列经典不是我最喜欢的著作。"


translate chinese day7_us_e0d0e8a3:


    me "我想要知道斯拉维娅在哪里。"


translate chinese day7_us_41ebc836:


    mz "你为什么想知道?"


translate chinese day7_us_166c7947:


    "她好像很确定我们的对话已经结束了，开始继续看起书。"


translate chinese day7_us_9c2b514a:


    me "这个嘛，既然我问了，那肯定是有理由的。"


translate chinese day7_us_4101fa65:


    mz "应该在码头。"


translate chinese day7_us_6dc67cf3:


    "热妮娅冷淡的回答着。"


translate chinese day7_us_73aab3cd:


    me "谢谢..."


translate chinese day7_us_0faa275e:


    "了解到需要的信息以后我马上离开了这座怨恨的城堡。"


translate chinese day7_us_da95a21f:


    th "热妮娅绝对精神不正常，至少和我对不上电波。"


translate chinese day7_us_ba64d8f0:


    "码头附近一些少先队员在拉动着小船，还有一些拿着船桨和绳子跑着。"


translate chinese day7_us_e721198a:


    "我仔细观察了一下，发现了斯拉维娅，她离我很远，坐在水边。"


translate chinese day7_us_f191de9e:


    me "正在打扫吗?"


translate chinese day7_us_757f4d47:


    "我问了一个最中立的问题。"


translate chinese day7_us_8b01f8be:


    sl "是的。"


translate chinese day7_us_4e965a2e:


    "她没有回头的回答着。"


translate chinese day7_us_b3a03b6f:


    me "听着，我..."


translate chinese day7_us_89748db4:


    sl "...想要说乌里扬卡的事情吗?"


translate chinese day7_us_ff471f9a_1:


    me "嗯，是啊..."


translate chinese day7_us_5b802725:


    "事实上，我也不知道该说什么好。{w} 斯拉维娅发现了我们在储藏室里，不知道理解了什么，然后全都告诉了辅导员。"


translate chinese day7_us_70278229:


    "如果仔细想想的话，现在已经不再是她的问题了。"


translate chinese day7_us_752bcc5c:


    th "再说现在去找奥尔加·德米特里耶夫娜谈话也完全是浪费时间。"


translate chinese day7_us_64efd5bd:


    "也许我只是想要证明斯拉维娅的推理，证明她是清白的。"


translate chinese day7_us_0483babd:


    sl "所以你想说什么?"


translate chinese day7_us_eb47c7e6:


    me "那个，乌里扬卡被惩罚了。也许她不会和咱们一起离开了。"


translate chinese day7_us_ecd25c83:


    sl "正常。"


translate chinese day7_us_b4a66582:


    me "我只是想和你说，没有发生任何特殊的事情。"


translate chinese day7_us_ff2ee0f7:


    sl "说实话，我什么也不知道，我只能把了解到的一切都告诉她。"


translate chinese day7_us_c1375773:


    me "所以你说了，但是这对谁有好处?"


translate chinese day7_us_3cc8517a:


    "我自言自语。"


translate chinese day7_us_dd320952:


    sl "当然我也不确定这样对不对..."


translate chinese day7_us_6175a110:


    "她没有自信的说着。"


translate chinese day7_us_b41d6a32:


    me "好吧，反正已经过去了...你觉得有没有可能把乌里扬卡从监禁中解救出来?"


translate chinese day7_us_4d657d7b:


    sl "你太过担心她了。"


translate chinese day7_us_3568a2ad:


    "斯拉维娅最后看着我笑了。"


translate chinese day7_us_c5c058f9:


    me "不是关心她...是正义!"


translate chinese day7_us_b89f54ec:


    "我有点糊涂的时候，终于找到的正确的答案。"


translate chinese day7_us_822ca264:


    sl "你是了解我们的辅导员的。"


translate chinese day7_us_df7535dd:


    me "是的，这我承认。"


translate chinese day7_us_ce7c946a:


    sl "等一等，她最后会冷静下来的。"


translate chinese day7_us_44dec0d8:


    "是啊，我觉得这是最好的决定了。"


translate chinese day7_us_c8c804ac:


    me "嗯，你说得对。"


translate chinese day7_us_26dcea4e:


    "我站在她旁边沉默了一会儿。"


translate chinese day7_us_f37824e2:


    "斯拉维娅似乎不是很想继续聊下去。"


translate chinese day7_us_e2cc6a60:


    "还是有一些事情没有解决好，但是我不想就这样尴尬下去，所以我不再保持这样紧张。"


translate chinese day7_us_2e597f6a:


    me "那好吧，我要走了。"


translate chinese day7_us_5f596122:


    sl "再见。"


translate chinese day7_us_d1046a49:


    "她微笑着。"


translate chinese day7_us_7177cc70:


    "我在广场中央停下来思索着。"


translate chinese day7_us_733af708:


    th "距离启程还有好长时间，我也没什么事可做。"


translate chinese day7_us_0477db27:


    th "就在昨天我觉得自己完全被困在这里，有的是时间的时候，我还感觉要快点想出办法来。"


translate chinese day7_us_f5c36e19:


    th "但是现在我只有五个小时了，却没有一点该做什么好的想法。"


translate chinese day7_us_60d55935:


    "我决定去找乌里扬卡。"


translate chinese day7_us_cfa9bf4e:


    th "再怎么说，虽然她被禁止出门，但是没有禁止我去找她。"


translate chinese day7_us_63ca30ce:


    "我很有礼貌的敲敲门。"


translate chinese day7_us_37c07e8b:


    us "我不欢迎你!"


translate chinese day7_us_61815909:


    "里面传来一阵非常不友好的声音。"


translate chinese day7_us_4c56125d:


    "我转动门把手走了进去。"


translate chinese day7_us_824d3de5:


    me "你好，囚犯!"


translate chinese day7_us_70b26771:


    us "哇，是你..."


translate chinese day7_us_743d2def:


    "乌里扬卡失望的说着。"


translate chinese day7_us_96cb823a:


    me "什么，我是唯一一个不被欢迎的人?"


translate chinese day7_us_15aba1cf:


    "我试着笑出来。"


translate chinese day7_us_467b6ff9:


    us "你怎么来了?"


translate chinese day7_us_43e44e64:


    me "嗯，我觉得你自己在这里可能很无聊。"


translate chinese day7_us_e7312d00:


    us "我很好!"


translate chinese day7_us_c33a6926:


    me "阿丽夏在哪里?"


translate chinese day7_us_67ce2945:


    us "你看，反正不在这里。"


translate chinese day7_us_8a6c21b7:


    me "怎么这么生气？早上你心情明明很好的。"


translate chinese day7_us_76aad34e:


    us "生气？我？"


translate chinese day7_us_d54c78b5:


    me "反正不是我..."


translate chinese day7_us_621db685:


    us "你是闲得没事做才来到这里的，是不是?"


translate chinese day7_us_0d9da0d6:


    me "啊，我的错..."


translate chinese day7_us_18e7bfa2:


    "我像演戏一样的叹着气，抱着头。"


translate chinese day7_us_2259a5d2:


    us "你坐这儿吧。"


translate chinese day7_us_83c16d37:


    "我坐在对面的床上。"


translate chinese day7_us_f9c0308a:


    us "跟我说点什么。"


translate chinese day7_us_36e3343b:


    me "咱们想想怎么和奥尔加·德米特里耶夫娜证明咱们是清白的!"


translate chinese day7_us_26f39e54:


    us "我是清白的。"


translate chinese day7_us_e9d754a4:


    "乌里扬娜指出我的错误。"


translate chinese day7_us_b1a66e30:


    us "你跟这件事情完全没有关系。"


translate chinese day7_us_d9150933:


    me "好吧，那就这样。"


translate chinese day7_us_03f66e24:


    us "但是为什么?"


translate chinese day7_us_86d84474:


    th "不好回答的问题。"


translate chinese day7_us_ecae36f6:


    "我们好像互换了角色，我提出了一个愚蠢的观点，她却保持了理智。"


translate chinese day7_us_873c6ad1:


    me "啊，就是因为咱们没有做错任何事情啊!"


translate chinese day7_us_56f9ef5d:


    us "这到底和你有什么关系？我也只是被关几个小时的禁闭，然后咱们就要离开了。"


translate chinese day7_us_442bf024:


    "她倒在床上，仰头看着天花板。"


translate chinese day7_us_3838f56d:


    me "好吧，是啊，但是..."


translate chinese day7_us_5093b270:


    "我尝试着让她高兴起来，但是目前看来很失败。"


translate chinese day7_us_14e075c5:


    me "你想做点什么吗?"


translate chinese day7_us_6494556f:


    us "已经到了午饭时间了。"


translate chinese day7_us_43b742c4:


    "我看了看表。"


translate chinese day7_us_ddedc7ac:


    me "啊，是啊。"


translate chinese day7_us_07a52ab7:


    us "让我来交给你一个工作，既然我自己不能出去，请你帮我去找点吃的来。"


translate chinese day7_us_6ae432cd:


    me "是!"


translate chinese day7_us_a0ee099d:


    "我敬了个礼，然后匆匆离开了房间。"


translate chinese day7_us_7b5685fb:


    "最近我开始觉得乌里扬卡终于开始懂事了。"


translate chinese day7_us_56f958e0:


    "也许是因为这个惩罚让她明白了很多，也许是因为别的。"


translate chinese day7_us_c059b51b:


    "我对她的态度也改变了。"


translate chinese day7_us_2a9a781a:


    "原先她干的坏事只是让我不爽，但是现在我也开始对她感到同情。"


translate chinese day7_us_c2b62820:


    "再怎么说，我也曾经是一个小孩子。"


translate chinese day7_us_2326b742:


    th "如果有人教给她什么事情是对的，什么事不对的，也许她犯的很多错误都是可以避免的。"


translate chinese day7_us_e9f2b2e6:


    "我在食堂里和不愿意给我双份的师傅吵了起来。"


translate chinese day7_us_749be311:


    "不过大家都知道乌里扬娜被关禁闭的事情，所以最后我劝说的力量战胜了食堂的规定。"


translate chinese day7_us_9c84b285:


    "很快我已经和乌里扬娜坐在房间里贪婪的吞食着肉丸子还有土豆。"


translate chinese day7_us_19b4f87b:


    me "就像是最后的晚餐。"


translate chinese day7_us_e718f922:


    us "你在说什么?"


translate chinese day7_us_076ef871:


    me "啊，每一个死刑犯都有机会许一个愿，那我的愿望就是这样一顿晚饭。"


translate chinese day7_us_6532261a:


    us "唔..."


translate chinese day7_us_1d2850e0:


    "让我惊讶的是这顿饭很好吃。"


translate chinese day7_us_1357b107:


    me "你最后的愿望是什么?"


translate chinese day7_us_8a3e0f25:


    us "那个...不被处死啊，当然的了!"


translate chinese day7_us_ec2c2a7c:


    "她笑了起来。"


translate chinese day7_us_4e8385e3:


    me "这你做不到的。"


translate chinese day7_us_d8734f33:


    us "为什么？如果许什么愿都可以的话。"


translate chinese day7_us_d1a58061:


    me "这个，你可以许愿，但是是有一定的限制的。"


translate chinese day7_us_1b7bd7e3:


    us "也就是说不是什么都可以。"


translate chinese day7_us_ba513381:


    me "那好吧，不是什么都可以..."


translate chinese day7_us_2bca74af:


    us "那就没劲了。"


translate chinese day7_us_a220139e:


    me "反正我一开始就没觉得做一个死刑犯多么有意思。"


translate chinese day7_us_e98bfbde:


    "我笑了。"


translate chinese day7_us_389622ca:


    us "我不知道，我没有经历过。"


translate chinese day7_us_0cf85698:


    "不过如果想想的话，这就是我的处境。"


translate chinese day7_us_8c75e136:


    th "这个夏令营就是我的牢房，再过几个小时我就要被抓起来面对最后的审判，面对前方的未知，就像是死亡一样。"


translate chinese day7_us_72658116:


    th "唯一的不同是我又更多选择，如果我不想参加列队集合，我可以不去。"


translate chinese day7_us_9e77a60a:


    me "你接下来要做什么?"


translate chinese day7_us_5f2c0f51:


    us "什么意思?"


translate chinese day7_us_c9d0b0d9:


    me "那个，夏令营结束以后。"


translate chinese day7_us_efd76d15:


    "她惊讶的看着我。"


translate chinese day7_us_85f9af1a:


    us "当然是回去上学了。"


translate chinese day7_us_133710bb:


    th "是啊，只有对我来说离开这个夏令营才是一个巨大的障碍，一段人生的结束，崭新生活的开始。"


translate chinese day7_us_cbeb66da:


    "一个星期以前我还完全不能理解自己离开了普通的生活，被放到了一个天知道什么地方。"


translate chinese day7_us_40e7e33b:


    "然后又是这些事情..."


translate chinese day7_us_74755571:


    th "总的来说，我现在的感觉不是害怕，恐惧，而是一种模糊的不确定感。"


translate chinese day7_us_cdd166bb:


    us "你呢?"


translate chinese day7_us_6aabcfa6:


    me "我...那个，我得找点事做。"


translate chinese day7_us_94d9cae2:


    us "找点事?"


translate chinese day7_us_42be2b6f:


    "她突然大笑起来。"


translate chinese day7_us_90305e74:


    me "是啊，这有什么问题吗?"


translate chinese day7_us_a5f6da24:


    us "你应该去马戏团当一个小丑!"


translate chinese day7_us_5ac38cf1:


    me "为什么?"


translate chinese day7_us_79d4d869:


    us "别人看到你的时候笑得停不下来。"


translate chinese day7_us_ed762271:


    me "但是为什么?"


translate chinese day7_us_1c0cb259:


    us "你总是像一个殉道者，俄罗斯的弥赛亚。"


translate chinese day7_us_67f0f7a7:


    "好吧，她说的有一些道理。"


translate chinese day7_us_bb751cb7:


    me "我有我自己的原因。"


translate chinese day7_us_e0608260:


    "我自言自语的看着窗户。"


translate chinese day7_us_96bedb40:


    us "什么原因?"


translate chinese day7_us_e9cde24f:


    me "各种...你怎么这么感兴趣?"


translate chinese day7_us_b72e8852:


    us "你忘了我是一个小孩子了吗?"


translate chinese day7_us_3aa0974f:


    "她淘气的笑着。"


translate chinese day7_us_31faa27f:


    me "嗯，跟你呆半个小时就是煎熬。"


translate chinese day7_us_7c214dde:


    us "看这是谁说的！你就是一个讨厌鬼!"


translate chinese day7_us_1aff923f:


    me "你怎么会这么想?"


translate chinese day7_us_ebf04f0a:


    us "你总是试图看透别人的灵魂，试图发现什么，分析周围的所有人。"


translate chinese day7_us_1a13d024:


    "我惊讶的看着乌里扬娜。"


translate chinese day7_us_3637341f:


    "我没想到一个小女孩能做出这么成熟的判断。"


translate chinese day7_us_5730768c:


    me "...还有呢?"


translate chinese day7_us_38197147:


    us "没了..."


translate chinese day7_us_0d76fe54:


    me "至少我知道自己该干什么，我也不会被禁足。"


translate chinese day7_us_b9e39a7d:


    us "那是偶然情况。"


translate chinese day7_us_7f6b716b:


    "她傻笑着。"


translate chinese day7_us_a7b22250:


    me "是啊，当然!"


translate chinese day7_us_26a5c7be:


    us "如果昨天晚上..."


translate chinese day7_us_5e054826:


    "她停顿了一下。"


translate chinese day7_us_fc2d031c:


    me "'昨天晚上什么?"


translate chinese day7_us_0cdc580c:


    us "没什么..."


translate chinese day7_us_7a4616c7:


    me "别啊，把话说完啊!"


translate chinese day7_us_b58aec3f:


    "她正要说下去，突然门外有脚步声，不一会儿奥尔加·德米特里耶夫娜就出现了。"


translate chinese day7_us_aef6989a:


    mt "啊，你在这里，这更好了。"


translate chinese day7_us_6553b668:


    "她好像有点糊涂，不知所云。"


translate chinese day7_us_cb8b6ba5:


    mt "那个，我又考虑了一下早晨的事情，似乎没有搞清楚，但是应该也不是大事，所以乌里扬娜，我不再关你禁闭了。"


translate chinese day7_us_0cd3e52f:


    me "你要是从一开始就醒悟了多好。"


translate chinese day7_us_901a0e98:


    "我嘀咕了几声。"


translate chinese day7_us_574528c5:


    mt "你说了什么吗?"


translate chinese day7_us_3c5e2cfc:


    me "没，没有。"


translate chinese day7_us_73c573c4:


    mt "我们快要出发了，快去收拾你的东西。"


translate chinese day7_us_8af1d414:


    "她说完就离开了房间。"


translate chinese day7_us_4456980d:


    us "你看吧!"


translate chinese day7_us_c63ec5a4:


    me "是啊..."


translate chinese day7_us_9da1463d:


    "我叹了口气。"


translate chinese day7_us_2d5f3035:


    me "你要收拾东西吗?"


translate chinese day7_us_f2cfbfb7:


    us "大概是吧...你呢?"


translate chinese day7_us_bad_7d11a631:


    me "嗯，我也要去收拾东西。"


translate chinese day7_us_bad_cf3fffbb:


    "她没有回答，所以我离开了房间。"


translate chinese day7_us_bad_953ac80a:


    th "哇，看来我在这个夏令营里最后的任务也结束了，对乌里扬卡的判决也被撤销了。"


translate chinese day7_us_bad_044c011d:


    th "现在我唯一的事情就是准备好离开这个地方。"


translate chinese day7_us_bad_782e9fd9:


    "我揉揉眼睛，走进了辅导员的房间。"


translate chinese day7_us_bad_9bc29860:


    th "我有什么需要收拾的东西吗?"


translate chinese day7_us_bad_cae3ae11:


    "我把自己的冬装收进包里，然后坐在了床上。"


translate chinese day7_us_bad_b02e82a9:


    "我被一种痛苦的感觉折磨，也许是既视感，感觉自己忘了什么，但是就是想不起来。"


translate chinese day7_us_bad_6288575d:


    th "这里的一切都结束以后，我还是没有找到答案，所以我还得到别的地方寻找答案。"


translate chinese day7_us_bad_1aa8b92f:


    th "如果我根本无法离开的话，我因为什么来到这里，怎么来到这里还有没有关系?{w}而且看起来我什么也决定不了。"


translate chinese day7_us_bad_41fa4bd9:


    "原来一切看起来都是那么简单。"


translate chinese day7_us_bad_5a9fed74:


    "是啊，我没有什么前途，但是我周围的一切都很清楚。"


translate chinese day7_us_bad_3a418180:


    "而我在这里呆了一个星期，遇到的事情比过去很多年都多。"


translate chinese day7_us_bad_a20cefa7:


    "..."


translate chinese day7_us_bad_f98266b4:


    "时间指向五点钟。"


translate chinese day7_us_bad_d924adad:


    "我拿起书包匆匆前往车站，生怕他们扔下我!"


translate chinese day7_us_bad_abfd8483:


    "结果公交车就在那里等着，还有所有的少先队员。"


translate chinese day7_us_bad_090f0c1d:


    mt "大家都在这里了吗?"


translate chinese day7_us_bad_14167bf5:


    "奥尔加·德米特里耶夫娜问道。"


translate chinese day7_us_bad_044ab0d8:


    mt "你们就要离开夏令营了，在分别的时候我有几句话要说。"


translate chinese day7_us_bad_00123e37:


    "她显然有点紧张。"


translate chinese day7_us_bad_019ed77a:


    mt "我希望在夏令营结束之后，你能留下一份美好的回忆。"


translate chinese day7_us_bad_b48fe53b:


    mt "同时希望你们有所进步，学习到一些知识，交到好朋友，还有...明年再来。"


translate chinese day7_us_bad_42256d78:


    "辅导员转了过去，看起来她在忍住眼泪。"


translate chinese day7_us_bad_80682609:


    "没想到她还这么动感情。"


translate chinese day7_us_bad_f4683d53:


    "虽然她的讲话听起来还是扯淡。"


translate chinese day7_us_bad_592d05f0:


    "少先队员们陆续登上公共汽车。"


translate chinese day7_us_bad_5176ce5d:


    "我在人群中寻找乌里扬卡。"


translate chinese day7_us_bad_422a1788:


    me "乌里扬娜在哪里?"


translate chinese day7_us_bad_2dd2f5e0:


    mt "她不走了。"


translate chinese day7_us_bad_0012312c:


    "辅导员做了简短的回答。"


translate chinese day7_us_bad_ae14ffa6:


    me "怎么？为什么？"


translate chinese day7_us_bad_945c2cb1:


    mt "她被禁足了。"


translate chinese day7_us_bad_55986632:


    me "但是你说..."


translate chinese day7_us_bad_f037a948:


    mt "她又犯错了。"


translate chinese day7_us_bad_149b3064:


    me "什么意思?"


translate chinese day7_us_bad_76b8fe45:


    mt "快点上车，不然他们就要扔下你了!"


translate chinese day7_us_bad_a8a52ea3:


    th "确实我是最后一个。"


translate chinese day7_us_bad_4b08df28:


    me "等一下..."


translate chinese day7_us_bad_8d6e2c28:


    mt "快点!"


translate chinese day7_us_bad_ef663ab0:


    "她不停地推着我，我决定不和她吵。"


translate chinese day7_us_bad_4c34caf5:


    th "又是这样，我不能总对她负责，如果她一直这么固执，她还是要自己面对后果的!"


translate chinese day7_us_bad_92535a77:


    "我坐上了最后的座位。"


translate chinese day7_us_bad_d1b5df44:


    "没有人和我坐在一起，我觉得没有关系。"


translate chinese day7_us_bad_06550b87:


    "不过过了一会儿阿丽夏靠了过来。"


translate chinese day7_us_bad_fddf23ae:


    dv "乌里扬卡在哪儿?"


translate chinese day7_us_bad_bd9299cf:


    me "留在营地里了.."


translate chinese day7_us_bad_69139e02:


    dv "为什么?"


translate chinese day7_us_bad_ba766f00:


    me "被禁足了..."


translate chinese day7_us_bad_ec5a83b6:


    "我毫无兴趣的回答着。"


translate chinese day7_us_bad_81056a1f:


    dv "因为什么?"


translate chinese day7_us_bad_7479b07e:


    me "不知道..."


translate chinese day7_us_bad_86e1bfff:


    dv "怎么，你没有询问真相吗?"


translate chinese day7_us_bad_d762c781:


    me "值得吗?"


translate chinese day7_us_bad_72474ea4:


    dv "你应该去的。"


translate chinese day7_us_bad_b0739623:


    me "为了什么，如果你想去你可以去，我们还没走远。"


translate chinese day7_us_bad_aa5269ba:


    dv "好，好..."


translate chinese day7_us_bad_ee6019f2:


    "她说完回到了自己的座位。"


translate chinese day7_us_bad_1ff20cee:


    "斯拉维娅和热妮娅坐在前面兴奋的讨论着什么，列娜，未来，舒里克和电子小子坐在附近玩牌。阿丽夏还有别的女孩子在看杂志。"


translate chinese day7_us_bad_e72a9bb8:


    "我大概是唯一一个没事可做的人。"


translate chinese day7_us_bad_f2666b16:


    "我发现自己还是对乌里扬卡感到愧疚。"


translate chinese day7_us_bad_0e4c5769:


    th "但是我还能做什么呢？如果我继续和她呆在一起，我可能会错过自己唯一的离开这里的机会..."


translate chinese day7_us_bad_05830ef4:


    th "在我目前的处境，这样玩儿火可是很危险的!"


translate chinese day7_us_bad_13511f52:


    th "到此为止了!"


translate chinese day7_us_bad_f86e1ee0:


    "不过我还是会想起她。{w} 都是我的错，因为我是一个胆小鬼，干了坏事。{w}所有和我立场相同的人都..."


translate chinese day7_us_bad_654261be:


    th "但是为什么呢？为什么我要为了她去冒险呢？"


translate chinese day7_us_bad_8ce59d82:


    "要回答这个问题则更加困难。"


translate chinese day7_us_bad_7415ba73:


    "我在这里的时候对她还有其他人都很了解。"


translate chinese day7_us_bad_a7064953:


    th "最后我觉得我要对乌里扬卡负责!"


translate chinese day7_us_bad_abd762d2:


    th "但是我现在想这个问题是为了什么?"


translate chinese day7_us_bad_69057b86:


    th "我要离开这个夏令营了，我迈向了位置的世界。"


translate chinese day7_us_bad_79fbcf58:


    th "从现在开始，我得假设这是一个外星世界，而且对我很不友好。"


translate chinese day7_us_bad_dd2f1e94:


    th "而且我指不上任何人。"


translate chinese day7_us_bad_d80fce90:


    "虽然说这种情况对我来说也并不是那么陌生，我以前就一直是自己一个人..."


translate chinese day7_us_bad_a20cefa7_1:


    "..."


translate chinese day7_us_bad_e1a1a085:


    "小猫头鹰夏令营被远远地甩在了后面，夜幕降临在这个位置的大地上。这辆公共汽车就像是漂浮在一片漆黑的大洋上，森林和田野的送来一阵阵波浪。"


translate chinese day7_us_bad_9c63431a:


    "不管怎么说，我现在最不在意的就是外面的情况了，我正陷入了沉思。"


translate chinese day7_us_bad_ef6dcef8:


    "我感觉自己有什么没有做完的事情。"


translate chinese day7_us_bad_bdc41394:


    "虽然现在我没有办法回去了。"


translate chinese day7_us_bad_3dfd000d:


    th "好事，坏事，很快都会被忘记，只剩下我在这里的存在，我过去生活的结束，新的生活的开始。"


translate chinese day7_us_bad_0bf16c1e:


    th "总的来说，其实没有什么花哨的东西，只是从从未来穿越到了80年代的一个少先队夏令营..."


translate chinese day7_us_bad_cd6651e8:


    th "我实际上更加关心前方等待着我的是什么。"


translate chinese day7_us_bad_6e3dcb07:


    th "我现在没有地方可去了，我没有亲人，没有朋友，没有钱，没有家。"


translate chinese day7_us_bad_2560e7b0:


    th "我很快就要和这些少先队员们分开了，我可能再也不会记得他们了，他们呢？可能也不会再记得我了。"


translate chinese day7_us_bad_34c047f7:


    th "对他们来说可能没什么，我也不是一个从未来穿越回来的人，只是一个小男孩，一个同龄人..."


translate chinese day7_us_bad_a20cefa7_2:


    "..."


translate chinese day7_us_bad_790bebae:


    "这条路看起来无穷无尽。{w}大部分的少先队员已经睡着了，只有我还在同睡神战斗。"


translate chinese day7_us_bad_55c5b97a:


    "闯入未知的领域时，保持清醒总是最好的，但是未知似乎掌握着关键，时间的流逝。"


translate chinese day7_us_bad_59d8bd33:


    "过了一分钟，一个小时，什么都没有发生。然后你的神经再也无法绷紧，终于缴械投降..."


translate chinese day7_us_good_0f8e9855:


    me "我好像没什么可收拾的。"


translate chinese day7_us_good_fa7387e7:


    "真的，这是事实。"


translate chinese day7_us_good_07b57da2:


    us "那么你能帮帮我吗?"


translate chinese day7_us_good_011dac7d:


    me "当然啦，有什么不可以?"


translate chinese day7_us_good_9e6ddcf0:


    "她开始从衣柜里翻出自己的衣物，垛在床上。"


translate chinese day7_us_good_16c1c561:


    me "小心，你会全搞砸的。"


translate chinese day7_us_good_a5b3f692:


    us "没关系，我回家去洗!"


translate chinese day7_us_good_8821f1ec:


    "T恤，短裙，短裤，连衣裙，鞋子，运动鞋，内衣...这堆衣服越来越高。"


translate chinese day7_us_good_1b010f5f:


    me "你真是自己带过来的?"


translate chinese day7_us_good_2312e712:


    "考虑到乌里扬卡的体型，感觉这不太可能。"


translate chinese day7_us_good_fc656e44:


    us "是啊，当然了!"


translate chinese day7_us_good_ce3666e5:


    "她乐着。"


translate chinese day7_us_good_968191a9:


    us "来帮我一把。"


translate chinese day7_us_good_907c8b48:


    "我们往大包里塞着衣服。"


translate chinese day7_us_good_7128760a:


    "我本来试图小心翼翼的装，但是发现这样毫无意义，于是也开始使劲往里塞。"


translate chinese day7_us_good_e48f1d3a:


    "最后床上的衣服完全消失了，我们居然还设法拉上了拉索。"


translate chinese day7_us_good_848e1ff0:


    us "好了，收工了。"


translate chinese day7_us_good_9513cd87:


    me "啊..."


translate chinese day7_us_good_153e49e8:


    "我看了看表，距离出发还有四十分钟。"


translate chinese day7_us_good_05577344:


    us "你看，挺有意思的。"


translate chinese day7_us_good_cb23542f:


    me "说什么?"


translate chinese day7_us_good_150ba4fa:


    us "就是说，过去的这一个星期。"


translate chinese day7_us_good_75e0e5f1:


    me "啊，是啊..."


translate chinese day7_us_good_f2170060:


    "我心不在焉的应和着。"


translate chinese day7_us_good_734b6aae:


    us "你没有这种感觉吗?"


translate chinese day7_us_good_665c7c6a:


    me "为什么...?"


translate chinese day7_us_good_68413a8d:


    us "听起来有点不是真心的。"


translate chinese day7_us_good_b2352a58:


    me "好吧，我得承认我还没有那么嗨。"


translate chinese day7_us_good_14cb44fc:


    us "但是有什么你不喜欢的呢?"


translate chinese day7_us_good_5839d87f:


    th "这实在是一言难尽啊..."


translate chinese day7_us_good_d2eaf470:


    me "你看...有一些，怎么说呢，我没有预料到的情况。"


translate chinese day7_us_good_2ee7e228:


    us "啊，你太没劲了..."


translate chinese day7_us_good_0c55cf94:


    "乌里扬卡说完转了过去。"


translate chinese day7_us_good_34c3e0de:


    me "好吧，你到底期待着什么呢?"


translate chinese day7_us_good_cac3332a:


    us "听起来你好像没有任何要回忆的？"


translate chinese day7_us_good_b654b924:


    me "嗯，确实有挺多可以回忆的事情。"


translate chinese day7_us_good_5154e89b:


    "我苦笑着。"


translate chinese day7_us_good_ac91a0ae:


    us "是啊，我就是这个意思!"


translate chinese day7_us_good_e24147c1:


    "她仔细看着我，让我有点不舒服。"


translate chinese day7_us_good_f3274282:


    me "又怎么了?"


translate chinese day7_us_good_f68e9feb:


    us "我呢，你会记得我吗?"


translate chinese day7_us_good_39fd95ae:


    me "当然了，所有人..."


translate chinese day7_us_good_7b24e7b5:


    us "所有人..."


translate chinese day7_us_good_d445de56:


    me "还有你，最重要的是你!"


translate chinese day7_us_good_55ab6c4e:


    "我把手放在胸前敬礼。"


translate chinese day7_us_good_7827470a:


    us "这好多了!"


translate chinese day7_us_good_f3bc15be:


    th "然而，我和她之间真的有什么值得回忆的事情吗?"


translate chinese day7_us_good_37b15a62:


    th "是啊，我们一起经历了很多冒险，但是乌里扬娜对我来说是什么人?{w}从事实上来讲只是一个烦人的熊孩子..."


translate chinese day7_us_good_b532bb7b:


    th "话说回来，我现在真的那么在乎自己的处境吗?{w} 和刚刚来到这里时相比，情况已经有了很大的变化，现在我感兴趣的只是事情下一步会怎么发展。"


translate chinese day7_us_good_2104e6d9:


    th "是啊，未知的前方可能不是那么美好的，但是不得不说还是很激动人心的。"


translate chinese day7_us_good_78852785:


    th "反正我到底能不能回到自己的世界，决定权也不在我。"


translate chinese day7_us_good_119d1deb:


    th "也就是说我没有任何的选择，我必须适应这里的生活。"


translate chinese day7_us_good_3c4827d3:


    "其中的一部分生活，就在我的面前坐着，哈哈大笑。"


translate chinese day7_us_good_77a7cf13:


    me "话说，你好像不像我刚开始觉得那么傻。"


translate chinese day7_us_good_0f029e42:


    us "你有这种想法就很奇怪啊！"


translate chinese day7_us_good_3db311fc:


    "她有点生气的说着。"


translate chinese day7_us_good_2824e5e8:


    me "只是开个玩笑。"


translate chinese day7_us_good_27972fbd:


    us "你的玩笑..."


translate chinese day7_us_good_96ba4ae7:


    me "好吧，快到时间了。"


translate chinese day7_us_good_89d3eb13:


    us "咱们走吧。"


translate chinese day7_us_good_5b0783c9:


    "乌里扬卡高兴的说着，指着她的大包。"


translate chinese day7_us_good_f2e007f0:


    me "啊..."


translate chinese day7_us_good_379e3d71:


    "我背起来，这份量几乎要把我压弯。"


translate chinese day7_us_good_fd618572:


    "我庆幸公交车站距离这里只有几百米。"


translate chinese day7_us_good_6ef81a93:


    "我猛地把她的包裹甩到车上，然后去拿自己那个不值一提的包包。"


translate chinese day7_us_good_fee09454:


    "过了不一会儿，所有的少先队员们都在这里集合了。"


translate chinese day7_us_good_090f0c1d:


    mt "大家都到齐了吗?"


translate chinese day7_us_good_14167bf5:


    "奥尔加·德米特里耶夫娜开始说到。"


translate chinese day7_us_good_044ab0d8:


    mt "你们就要离开夏令营了，在分别的时候我有几句话要说。"


translate chinese day7_us_good_00123e37:


    "她显然有点紧张。"


translate chinese day7_us_good_019ed77a:


    mt "我希望在夏令营结束之后，你能留下一份美好的回忆。"


translate chinese day7_us_good_b48fe53b:


    mt "希望你们至少长大了一些，交到了新朋友......明年，想着再来。"


translate chinese day7_us_good_42256d78:


    "辅导员转了过去，看起来她在忍住眼泪。"


translate chinese day7_us_good_6a707c88:


    "我没想到她也能这么多愁善感。但是她说的话我倒是同意。"


translate chinese day7_us_good_6888cb6b:


    "也许这是我第一次没有把她的话当作耳旁风。"


translate chinese day7_us_good_7400c0ae:


    "很快大家都坐上车了。我坐在最后一排，和乌里扬卡挨在一起。"


translate chinese day7_us_good_e3f34bf2:


    "斯拉维娅和热妮娅在第一排，然后稍微靠后一点的是列娜，未来，电子小子，还有舒里克，他们在玩牌。{w}阿丽夏独自在前面一排坐着，垂头丧气的，没有人和她坐在一起。"


translate chinese day7_us_good_69fc4c0b:


    me "这让你的邻居有点尴尬..."


translate chinese day7_us_good_7c056817:


    us "没关系，你帮我收拾东西而且还背过来的。"


translate chinese day7_us_good_8f838576:


    "我认真的看着她。"


translate chinese day7_us_good_fb2f522b:


    th "看起来这个小恶魔对我的态度好像发生了很大的转变。"


translate chinese day7_us_good_607b9495:


    th "是因为我...是她的朋友吗?{w} 一个对我来说已经早就失去意义的词语。"


translate chinese day7_us_good_ebd06a5c:


    th "我记不得自己的熟人，什么时候认识他们，都有谁。{w} 就连在学校的时候..."


translate chinese day7_us_good_ef14ed90:


    th "现在竟然有人把我当成朋友..."


translate chinese day7_us_good_d54d3387:


    th "不管怎么说，这些对我来说到底意味着什么?"


translate chinese day7_us_good_d742ed4f:


    "我更擅长思考人类的大问题，抽象的遥远的未来前景，而不是自己的琐事。"


translate chinese day7_us_good_70b35443:


    "而且确实我在这个夏令营里的时候，成功的和乌里扬卡做了好朋友。"


translate chinese day7_us_good_dadbd740:


    "她唤起了这些在我身上消失已久的感情。"


translate chinese day7_us_good_5cbb42fa:


    "这就是同志和朋友。"


translate chinese day7_us_good_d53e2161:


    "我微笑着揉乱她的头发。"


translate chinese day7_us_good_d8ad57a4:


    us "这是干什么?"


translate chinese day7_us_good_d946b833:


    "她撅起嘴。"


translate chinese day7_us_good_616b5736:


    me "没什么!"


translate chinese day7_us_good_36244ee4:


    us "变态!"


translate chinese day7_us_good_9ddb8918:


    us "我长大以后，你会娶我吗?"


translate chinese day7_us_good_8288e06d:


    me "当然!"


translate chinese day7_us_good_969c2b47:


    us "我可记住了呦!"


translate chinese day7_us_good_3362bb2d:


    me "好~好~"


translate chinese day7_us_good_6fc26df0:


    us "咱们去找他们一起玩牌吧?"


translate chinese day7_us_good_3fbaf00b:


    me "走吧!"


translate chinese day7_us_good_60a78af5:


    "我们围着一个当作牌桌的旅行箱。"


translate chinese day7_us_good_72f8664f:


    "很快阿丽夏也加入了我们。"


translate chinese day7_us_good_f9ee83c7:


    "我们一路欢声笑语，讲着笑话，这种感觉就是幸福。{w}就在此时此地的简单的幸福。"


translate chinese day7_us_good_3105c441:


    "就在这个瞬间，我在这里交到的朋友，他们已经远比我找到离开这里的方法还要重要。"


translate chinese day7_us_good_32e7a990:


    th "说起来，我还应不应该设法回去呢?"


translate chinese day7_us_good_a20cefa7:


    "..."


translate chinese day7_us_good_07039b05:


    "天逐渐黑了下来，游戏已经结束，大家都回到了座位上。"


translate chinese day7_us_good_b2a3af4b:


    "我不知道到达城镇中心到底需要多长时间，但是现在感觉就像是永恒一样。"


translate chinese day7_us_good_5fd5c146:


    "透过车窗，只有漆黑的夜在注视着我，好像要吞噬掉整个世界，只剩下这辆Icarus的空壳。"


translate chinese day7_us_good_8d851fd8:


    "总之，周围的情况是我最不关心的，我很享受这个瞬间。"


translate chinese day7_us_good_4ba60f47:


    th "看起来这种现实是非常正常的。"


translate chinese day7_us_good_7e661857:


    th "而且我如何来到这里这种问题真的还重要吗？我变成了另外一个人，交到了新的朋友。"


translate chinese day7_us_good_eabb99b9:


    us "你在想什么?"


translate chinese day7_us_good_94622b60:


    me "生命。"


translate chinese day7_us_good_c8e68f82:


    us "想的怎么样?"


translate chinese day7_us_good_d1bfd0cf:


    me "生命是如此的精彩!"


translate chinese day7_us_good_fae4f64a:


    "乌里扬卡悄悄笑了起来。"


translate chinese day7_us_good_f4357af5:


    th "我没有地方可去，所以我可以自由选择自己的道路。"


translate chinese day7_us_good_9e5ffe85:


    th "是啊，我们马上就要分别，我和其中的大部分人可能再也不会见面，但是我们还会是好朋友的!"


translate chinese day7_us_good_d097bca7:


    "我的内心涌起一阵暖流。"


translate chinese day7_us_good_0a73f977:


    "我好像回到了童年时代。"


translate chinese day7_us_good_76539999:


    "乌里扬娜把脑袋搭在我的肩膀上，很快就睡着了。"


translate chinese day7_us_good_a20cefa7_1:


    "..."


translate chinese day7_us_good_481a603e:


    "有时候你会觉得很累，不一定是因为工作或者伤心，也有可能是享受，欢乐。"


translate chinese day7_us_good_8cffdf27:


    "有的时候你可能还想继续，但是你的灵魂渴望平静，你的身体渴望休息。"


translate chinese day7_us_good_59cfc807:


    "我面带微笑的睡着了。"


translate chinese day7_dv_34b34bb6:


    "有人在追我..."


translate chinese day7_dv_2fb6d076:


    "也许他们不是人，只是一些黑影。"


translate chinese day7_dv_56a7165e:


    "我跑着，跑着...绊倒在什么东西上，喘不过气来。"


translate chinese day7_dv_c534a505:


    "我全身都被原始的恐惧充满。"


translate chinese day7_dv_f6c12f28:


    "接下来就没有了记忆..."


translate chinese day7_dv_a20cefa7:


    "..."


translate chinese day7_dv_81b5abc9:


    "我睁开肿胀的眼睛，感到被阳光刺痛。"


translate chinese day7_dv_66a25612:


    "我的嘴里有一种奇怪的味道，浑身都疼的要命，头昏脑胀，好像昨天有人喝了很多…"


translate chinese day7_dv_130627c2:


    "等我逐渐恢复意识以后，我开始记起昨天的事情。"


translate chinese day7_dv_961cb5de:


    "我确定我昨天兴奋的和阿丽夏干了一瓶伏特加..."


translate chinese day7_dv_955e4b23:


    "我试图站起来，但是有什么压住了我的左臂。"


translate chinese day7_dv_169141ea:


    "是在安详的睡着的阿丽夏。"


translate chinese day7_dv_fed4fffc:


    "全裸..."


translate chinese day7_dv_53e612f9:


    "我开始想起昨天发生的各种事情..."


translate chinese day7_dv_2d377264:


    "恐惧很快被欣快所占据。"


translate chinese day7_dv_d8bc19f3:


    "我懒洋洋的躺在床上，享受着宿醉的早晨。"


translate chinese day7_dv_94b6a4c7:


    "过了一会儿阿丽夏醒了过来。"


translate chinese day7_dv_75b94b19:


    "我轻轻的亲着她说:"


translate chinese day7_dv_0a6d11b5:


    me "早上好!"


translate chinese day7_dv_117eed62:


    "她呆呆地看着我，然后跳了起来:"


translate chinese day7_dv_b7d8bf72:


    dv "你！你!"


translate chinese day7_dv_08572abe:


    me "我什么?"


translate chinese day7_dv_4f57be91:


    "她突然意识到自己是全裸的，于是抢走一张床单裹在身上。"


translate chinese day7_dv_0c9248c1:


    dv "你昨天..."


translate chinese day7_dv_8a5850eb:


    "她发出唏嘘。"


translate chinese day7_dv_0e8d2401:


    me "但是那似乎是你的主意..."


translate chinese day7_dv_8b988724:


    dv "我喝醉了!"


translate chinese day7_dv_bb4b7abd:


    me "我也是，你还能做什么..."


translate chinese day7_dv_725e3ccf:


    "阿丽夏愤怒地看着我，不过过了一会儿坐在了我的旁边。"


translate chinese day7_dv_40473235:


    dv "好吧，过去了就过去吧..."


translate chinese day7_dv_238ce8d6:


    me "你确定?"


translate chinese day7_dv_0c27c20a:


    "我做起来想要拥抱她。"


translate chinese day7_dv_48a6403e:


    dv "别..."


translate chinese day7_dv_bb97ed04:


    "她悄悄说着，脸有些泛红。"


translate chinese day7_dv_5ac38cf1:


    me "为什么?"


translate chinese day7_dv_9e177d54:


    dv "因为..."


translate chinese day7_dv_f374d34a:


    "阿丽夏疯狂的摇着头。"


translate chinese day7_dv_339d121b:


    dv "因为已经四点了!"


translate chinese day7_dv_a6f35be5:


    me "然后怎么了？别告诉我你会迟到什么的?"


translate chinese day7_dv_3daf3890:


    dv "他们会让下咱们自己走的!"


translate chinese day7_dv_25f7abbc:


    me "走?"


translate chinese day7_dv_350637f6:


    dv "今天是最后一天!"


translate chinese day7_dv_09586d84:


    me "什么最后一天?"


translate chinese day7_dv_141c5aec:


    "我完全糊涂的问着。"


translate chinese day7_dv_9d1223cd:


    dv "夏令营的最后一天!"


translate chinese day7_dv_3d342d5a:


    me "什么?!"


translate chinese day7_dv_4b58184b:


    "本来已经好转的头疼突然又一次袭来。"


translate chinese day7_dv_24c7fd3d:


    dv "别告诉我你不知道?"


translate chinese day7_dv_975a5f59:


    "阿丽夏惊讶的问。"


translate chinese day7_dv_1b078565:


    me "所以你真的知道。"


translate chinese day7_dv_0f45ad2e:


    dv "啊，是啊...但是我不知怎么的就给忘了。"


translate chinese day7_dv_e1f38c76:


    me "咱们现在应该干什么?"


translate chinese day7_dv_3490638a:


    "实际上这不是我最关心的事情。"


translate chinese day7_dv_e3a67172:


    th "如果今天大家要一起离开这个夏令营，那么我也终于可以离开这个鬼地方了!"


translate chinese day7_dv_3b3335bc:


    dv "好吧，我不知道。"


translate chinese day7_dv_516b334e:


    me "也许还赶得上!"


translate chinese day7_dv_713e5769:


    "我跳了起来，迅速穿上衣服。"


translate chinese day7_dv_1252f291:


    "这显得有点困难，尤其是在经过了昨天晚上疯狂的放纵之后。"


translate chinese day7_dv_9acc6e59:


    me "你还在等什么?"


translate chinese day7_dv_b1376477:


    "阿丽夏没有回答，但是也开始穿衣服。"


translate chinese day7_dv_d7fddf78:


    "过了一分钟我们站在了荒凉的广场。"


translate chinese day7_dv_4c7430c4:


    me "咱们快去辅导员的房间!"


translate chinese day7_dv_3a7fd3f3:


    "我找出自己的钥匙打开了门，但是里面没有人。"


translate chinese day7_dv_a20cefa7_1:


    "..."


translate chinese day7_dv_fc48590b:


    "我们检查了整个营地，发现只有我们两个。"


translate chinese day7_dv_a20cefa7_2:


    "..."


translate chinese day7_dv_732ccffa:


    "我回到广场，双手掩面，感觉自己完蛋了。"


translate chinese day7_dv_16a845b1:


    "我仍然感觉很不舒服，但是现在我还得解决一个严重的问题。"


translate chinese day7_dv_4f9d4e08:


    me "他们可能会去哪儿?"


translate chinese day7_dv_6a9c9a91:


    dv "嗯，大概是去了城镇中心。"


translate chinese day7_dv_e462bbf9:


    me "你知道那是在哪里吗?"


translate chinese day7_dv_eb9b0712:


    dv "大概知道。"


translate chinese day7_dv_b5ff223e:


    me "步行去那里可能吗?"


translate chinese day7_dv_3e69c35f:


    dv "不知道。"


translate chinese day7_dv_f85f3acb:


    me "如果坐车的话要多长时间?"


translate chinese day7_dv_6ac4fbb2:


    dv "几个小时。"


translate chinese day7_dv_6373dd16:


    "我迅速思考了一下折合成公里大概是多远。"


translate chinese day7_dv_5039789f:


    "考虑到苏联的汽车工业水平，还有公路保养水平，大概可以估计出走着去还是有戏的。"


translate chinese day7_dv_78543246:


    "如果阿丽夏知道确切的位置的话。"


translate chinese day7_dv_98084336:


    "而且对我来说很难作出决定。"


translate chinese day7_dv_cfd1657b:


    "但是我们也不能呆在这里，独自两个人，在一个没有人烟的夏令营..."


translate chinese day7_dv_ce8c5196:


    "在现实生活中这绝对是很恐怖的，而且我又没办法不把这里当作现实。"


translate chinese day7_dv_304c9144:


    me "咱们走吧。"


translate chinese day7_dv_621eb081:


    dv "去哪里?"


translate chinese day7_dv_17368c07:


    me "去城镇中心!"


translate chinese day7_dv_0ff80729:


    dv "你疯了吗?!"


translate chinese day7_dv_948f6b23:


    me "你要呆在这里吗?"


translate chinese day7_dv_a094d465:


    dv "当然了，他们会发现少两个人然后回来找我们的。"


translate chinese day7_dv_296ed7ce:


    me "你不觉得他们出发的时候没有发现就很奇怪吗?"


translate chinese day7_dv_d8bfa1da:


    dv "啊，是啊，但是..."


translate chinese day7_dv_60f3d758:


    "她开始思考。"


translate chinese day7_dv_efc9fc55:


    dv "但是就凭我们两个走..."


translate chinese day7_dv_e708673b:


    me "但是你说你知道方向!"


translate chinese day7_dv_0db84079:


    dv "我知道..."


translate chinese day7_dv_33c04461:


    "她没有信心的回答着。"


translate chinese day7_dv_d232a207:


    me "还想什么？也许咱们会在路上遇到公交车的。"


translate chinese day7_dv_8f93dbc3:


    th "这我自己都不太信，但是如果这个夏令营真的有一个出口的话，那么这个办法应该是比较靠谱的。"


translate chinese day7_dv_69c97e52:


    dv "我不知道...随你怎么说..."


translate chinese day7_dv_1488dd24:


    "她迷迷糊糊的继续说着。"


translate chinese day7_dv_79601b2c:


    me "那就开始收拾你的东西吧，过半个小时在这里等我。"


translate chinese day7_dv_3a7b0668:


    "我跑向奥尔加·德米特里耶夫娜的房间。"


translate chinese day7_dv_47b0c421:


    "是啊，没什么可收拾的。"


translate chinese day7_dv_9e214029:


    "我把冬装收进包里，正要走的时候，突然有了一个有趣的想法。"


translate chinese day7_dv_1d1dfe83:


    th "我要这些东西有什么用?"


translate chinese day7_dv_7f3b0a96:


    th "现在还是夏天，我为什么要带着这些厚衣服呢，而且我还不知道自己要走多远。"


translate chinese day7_dv_294dbace:


    "考虑到这些，我把手机装进口袋，就径直前往广场了。"


translate chinese day7_dv_68674ae5:


    "我得等阿丽夏二十分钟。"


translate chinese day7_dv_d5afc1dc:


    "我的宿醉好了一些，让我能更集中的回忆昨天晚上的事情。"


translate chinese day7_dv_ee82b163:


    th "她对我意味着什么?"


translate chinese day7_dv_391b61de:


    th "只是酒后乱性还是什么别的?"


translate chinese day7_dv_dac5d5ff:


    th "我说不清阿丽夏是怎么，我连自己都说不清。"


translate chinese day7_dv_515b15e3:


    "我想起自己和她的交往，刚开始她是怎么取笑我，那么傲慢又自负..."


translate chinese day7_dv_4ec52090:


    "昨晚的事情不像单单是补偿。"


translate chinese day7_dv_93eb0c2d:


    th "我已经确定了阿丽夏并不是她表面上看起来的那个样子，但是她真正的性格到底是什么样的?"


translate chinese day7_dv_7d2a5519:


    th "而且我对她到底有没有感觉呢？我确实感觉同情，有一点爱慕，但是还有没有其他的?"


translate chinese day7_dv_b0325c5c:


    "我的思考被阿丽夏打断了。"


translate chinese day7_dv_a303f6cc:


    dv "来，拿着。"


translate chinese day7_dv_77743949:


    "她递给我一个大书包。"


translate chinese day7_dv_4907682a:


    me "这是什么?"


translate chinese day7_dv_ff782f2a:


    "我怀疑的问着。"


translate chinese day7_dv_e780e2ad:


    dv "我的行李。"


translate chinese day7_dv_aae0b9eb:


    me "怎么就这么点儿?"


translate chinese day7_dv_4eaa9948:


    dv "用多少拿多少。"


translate chinese day7_dv_5491b40f:


    me "你看...{w} 我们不知道需要走多远，所以只要带需要的东西就好了?"


translate chinese day7_dv_44f79413:


    dv "怎么？还要我把这些扔在这里？"


translate chinese day7_dv_15d90504:


    "阿丽夏嗤之以鼻的说着。"


translate chinese day7_dv_206f97a8:


    "首先，我不明白她怎么又这么多东西，再有，里面有什么重要的玩意儿?"


translate chinese day7_dv_8d0625c6:


    me "但是我要履行骑士精神，是吧?"


translate chinese day7_dv_bdbe816e:


    dv "那当然了!"


translate chinese day7_dv_3aa0974f:


    "她自豪的笑着。"


translate chinese day7_dv_8d91232a:


    "我估计了一下大概有多重。"


translate chinese day7_dv_278bae91:


    th "我想大概有十公斤。{w}我能背几公里，但是再远可能就无能为力了..."


translate chinese day7_dv_9887bbde:


    me "你应该明白我们拿着这个走不了多远。"


translate chinese day7_dv_d3b04189:


    dv "那个，我们尽量吧..."


translate chinese day7_dv_a7bd2a57:


    me "你是说我过一会儿可以扔出去一些东西?"


translate chinese day7_dv_7ec4ae09:


    dv "等会儿再说。"


translate chinese day7_dv_521b5dfc:


    "她狡猾的转动着眼珠回答着。"


translate chinese day7_dv_9886068f:


    me "好吧，我就照你说的办。"


translate chinese day7_dv_91ff7f5a:


    th "总之，既然是我在背着，那么我也有权利往下扔。"


translate chinese day7_dv_a07a03c0:


    "就这样，我们的旅程开始了。"


translate chinese day7_dv_92f1963f:


    "至少过了一个小时我才开始感觉累。"


translate chinese day7_dv_31827d05:


    "没想到我这么持久。"


translate chinese day7_dv_948d6304:


    "要么是这个书包很轻，要么是昨天晚上的运动锻炼了我的肌肉。"


translate chinese day7_dv_3e917655:


    "我在路边停下，把阿丽夏的东西扔在了地上。"


translate chinese day7_dv_e4dfaea7:


    dv "你在干什么?"


translate chinese day7_dv_6932f262:


    "她显然很生气。"


translate chinese day7_dv_6557adca:


    me "你想不想自己背?"


translate chinese day7_dv_b2bffcd8:


    dv "当然不想了!"


translate chinese day7_dv_ddbf8003:


    me "如果是那样的话，我需要休息一下。"


translate chinese day7_dv_5ced9ae4:


    "最激进的做法是把重的东西都扔掉，只留下最重要的，但是我好像没有那么冲动。"


translate chinese day7_dv_a87b4b28:


    me "你一路上一言不发..."


translate chinese day7_dv_47e8d127:


    "阿丽夏呆呆地看着，一条公路延伸向远方。"


translate chinese day7_dv_2895df22:


    dv "说什么?"


translate chinese day7_dv_e807ab10:


    me "不知道，但是你好像什么也没有发生一样。"


translate chinese day7_dv_b9934743:


    dv "发生了什么吗?"


translate chinese day7_dv_46ec1ab9:


    "她心不在焉的问着。"


translate chinese day7_dv_256912af:


    me "那个...你知道..."


translate chinese day7_dv_6330e388:


    dv "歇够了吗？我们走吧!"


translate chinese day7_dv_45b6c18e:


    "阿丽夏迅速向前走去。"


translate chinese day7_dv_1d8b65b9:


    me "喂，等一下!"


translate chinese day7_dv_02244c64:


    "我背起书包追她。"


translate chinese day7_dv_a20cefa7_3:


    "..."


translate chinese day7_dv_8af27249:


    "我们一直这样沉默的走着，阿丽夏在前，我在后。"


translate chinese day7_dv_d9f48ac6:


    "我想聊聊，但是不知道怎么开口。"


translate chinese day7_dv_f9d9d427:


    "她似乎在明确的暗示我昨天晚上的事情是一个巨大的错误。"


translate chinese day7_dv_c81f3a7e:


    "至少对我来说..."


translate chinese day7_dv_e4773f56:


    "虽然在我看来没有什么不该发生的事。"


translate chinese day7_dv_bd664ace:


    "在夏令营里的最后几天，我真的被阿丽夏吸引住了。{w}我自认为她也挺喜欢我的。"


translate chinese day7_dv_9f1719bd:


    th "那有什么问题嘛?"


translate chinese day7_dv_5f62b20e:


    th "也许她很不爽因为这是一夜情?"


translate chinese day7_dv_9bbf9c21:


    me "你知道..."


translate chinese day7_dv_00dae805:


    "她转过来看着我。"


translate chinese day7_dv_6afc1bb3:


    me "如果我做错了什么..."


translate chinese day7_dv_6812f447:


    dv "你什么都没有做错。"


translate chinese day7_dv_a31a4d9f:


    me "但是..."


translate chinese day7_dv_904f584f:


    dv "结果就是这样。"


translate chinese day7_dv_953edcaa:


    me "所以这只是意外?"


translate chinese day7_dv_3ae4e7bf:


    dv "那我可没说。"


translate chinese day7_dv_b233eb86:


    me "那你说什么了?"


translate chinese day7_dv_20a5d791:


    "我总是得不到答案，有点不高兴，所以我提高了声音。"


translate chinese day7_dv_60992bdb:


    me "你总是各种暗示，让人完全不明白！你怎么不像原来一样直率的说出来!"


translate chinese day7_dv_86f37595:


    dv "人是会变的..."


translate chinese day7_dv_47359365:


    "她慢慢的往前走着。"


translate chinese day7_dv_ad786d68:


    me "变? 你这叫“变”?{w} 你就像一个完全不同的人了现在！就像列娜一样！"


translate chinese day7_dv_1647718e:


    "阿丽夏停了下来，但是没有转身。"


translate chinese day7_dv_e9b62978:


    dv "别拿我们作比较...听到没有！别比较!"


translate chinese day7_dv_03ee962e:


    "她声音很小，但是话语中充满了怒气。"


translate chinese day7_dv_d3383672:


    me "什么...?"


translate chinese day7_dv_b6ed7be6:


    "我想后退了一步。"


translate chinese day7_dv_98eb301b:


    dv "你总是怪我，但是你自己呢？你总是看着她!现在你从我这儿都能看出她的影子了..."


translate chinese day7_dv_3837a7d9:


    me "等等，那是..."


translate chinese day7_dv_c011ba0d:


    dv "那你昨天来找我干什么？你不是也能去找她你知道..."


translate chinese day7_dv_3b51cf39:


    me "知道什么?"


translate chinese day7_dv_ffc2234f:


    dv "知道我在等你!"


translate chinese day7_dv_cdd661a7:


    "看起来阿丽夏要承认起来不是那么容易的，但是我刚开始没有太在意。"


translate chinese day7_dv_760c9675:


    me "那个，昨天你自己说让我去的，所以..."


translate chinese day7_dv_0433bfa6:


    dv "和那个没有关系!"


translate chinese day7_dv_d1db8c32:


    "阿丽夏开始抽泣，用手捂住了脸。"


translate chinese day7_dv_613a279f:


    "我完全被吓到了。"


translate chinese day7_dv_5bf38ff0:


    "我仍旧不明白她在暗示什么，所以这对话没法正常进行。"


translate chinese day7_dv_aa4f4033:


    me "抱歉...{w}原谅我..."


translate chinese day7_dv_a9505b4f:


    dv "别道歉了!"


translate chinese day7_dv_e506eadf:


    me "但是如果我真的做错了什么..."


translate chinese day7_dv_1a4b9468:


    dv "那道歉还有什么意义，如果你知道自己做错了，从最一开始就别做不就行了嘛。"


translate chinese day7_dv_732b1419:


    me "但是我不知道怎么做!"


translate chinese day7_dv_bdcc6431:


    "阿丽夏抬起头看着我。"


translate chinese day7_dv_b4bb6756:


    "她的脸上充满了痛苦，让我不敢和她对视。"


translate chinese day7_dv_8a74fc28:


    "我们就这样站着..."


translate chinese day7_dv_5fcaf917:


    "很快她就恢复正常了。"


translate chinese day7_dv_1b7149e8:


    dv "好吧，算了，我们继续走吧。"


translate chinese day7_dv_7f90898b:


    "一般来说我就算想要忘掉都不可能，但是我又不知道说什么好，所以只能跟着她。"


translate chinese day7_dv_a20cefa7_4:


    "..."


translate chinese day7_dv_bf9809fa:


    "太阳逐渐降下了地平线。"


translate chinese day7_dv_546ddc40:


    "我陷入深深地沉思中，甚至没有感觉到身上的重担。"


translate chinese day7_dv_7d33e08e:


    "我得说点儿什么，做点什么。"


translate chinese day7_dv_29a605f9:


    th "阿丽夏在期待着呢，至少要说出来..."


translate chinese day7_dv_ee5b037a:


    th "但是我要怎么开始呢?"


translate chinese day7_dv_3522dad7:


    "我已经熟悉了简单的生活。"


translate chinese day7_dv_26ce155f:


    "所有的任务都只需要简单的几步就能完成。"


translate chinese day7_dv_bb97fe3b:


    "相比于完成任务的冗长步骤，我更倾向于沉浸在未来的成功虚拟的喜悦之中。"


translate chinese day7_dv_99fc5064:


    "而且，说实话我觉得真要在现实生活中我是不可能遇到这种情况的。"


translate chinese day7_dv_6fb63bc0:


    "显然我向往已久这种有人在乎我的生活。"


translate chinese day7_dv_ff2cf0a2:


    "但是现在我的面前就有这样一个人，我却无动于衷。"


translate chinese day7_dv_79e93d21:


    "我不能因为不知道接下来会发生什么就告诉阿丽夏“是”或“否”。"


translate chinese day7_dv_cb230162:


    "天色变暗。"


translate chinese day7_dv_31cc1e11:


    me "我想我们应该在这附近野营。"


translate chinese day7_dv_683a8ca7:


    dv "前面不太远了。"


translate chinese day7_dv_3e4e35f3:


    me "你确定吗？而且即使是真的，也最好是在白天走。"


translate chinese day7_dv_19dee4e1:


    dv "听你的吧..."


translate chinese day7_dv_8cef92aa:


    "她冷冷地说。"


translate chinese day7_dv_2da2cbd9:


    "我们在树林的边缘停了下来。"


translate chinese day7_dv_e627ef17:


    me "你有什么有用的东西吗?"


translate chinese day7_dv_2e4b2df9:


    "我指指背包。"


translate chinese day7_dv_c665ba01:


    dv "看看。"


translate chinese day7_dv_62531094:


    "大部分是各种衣物，但是在最底下我找到了一本杂志还有火柴，足够生火了。"


translate chinese day7_dv_55d331e0:


    "很快我们就坐在树桩上烤着火了。"


translate chinese day7_dv_64dedd42:


    "我到现在才明白我已经离开了那个鬼夏令营。"


translate chinese day7_dv_cef2d4ff:


    th "我没有找到任何答案，相反，事情变得越来越复杂了，我觉得自己就要在这个世界度过余生了。"


translate chinese day7_dv_584626de:


    th "如果不考虑各种科幻元素，我现在显然是在80年代而且是南部。"


translate chinese day7_dv_c66b43bf:


    th "然而我到底是怎么来的已经不重要了，所以我得尝试着去适应。"


translate chinese day7_dv_0cc0cf06:


    th "我得从我身边的这个女孩子开始。"


translate chinese day7_dv_48a428eb:


    me "所以说等咱们到了城镇中心以后要做什么呢?"


translate chinese day7_dv_9eca505b:


    dv "给父母打电话啊!"


translate chinese day7_dv_94184036:


    "阿丽夏轻松的笑着，她倒是有人可打。"


translate chinese day7_dv_f4eb646d:


    me "然后呢?"


translate chinese day7_dv_e639a729:


    dv "然后没了。"


translate chinese day7_dv_e54b39e9:


    me "什么意思?"


translate chinese day7_dv_35919e4b:


    dv "那个，各回各家。"


translate chinese day7_dv_22dddcd1:


    "我一点也不惊讶，我只是没有想过。"


translate chinese day7_dv_5c1ed51e:


    me "没了?"


translate chinese day7_dv_964c391d:


    dv "难道还有什么?"


translate chinese day7_dv_ee102f21:


    me "我不知道...发生了这么多事..."


translate chinese day7_dv_be621763:


    dv "但是什么也没有发生!"


translate chinese day7_dv_bdfc5195:


    "她开心的说着。"


translate chinese day7_dv_7a16e55f:


    me "你这是什么意思?"


translate chinese day7_dv_5788094d:


    dv "你有机会找到列娜的。"


translate chinese day7_dv_7540e341:


    "我很惊讶，但是阿丽夏很友好的说着。"


translate chinese day7_dv_1deed426:


    me "好像我需要列娜似的!"


translate chinese day7_dv_bad_3bbe4c98:


    dv "难道不是吗?"


translate chinese day7_dv_bad_af26aec3:


    "说真的，我现在最不愿意想的人就是列娜。"


translate chinese day7_dv_bad_3d1847ad:


    me "不。"


translate chinese day7_dv_bad_7f24994f:


    dv "那么，也许是我?"


translate chinese day7_dv_bad_eb6f47e6:


    "我认真的看着阿丽夏，她在邪恶的笑着。"


translate chinese day7_dv_bad_3674b5bc:


    th "刚刚她还在抹眼泪..."


translate chinese day7_dv_bad_86e80d63:


    me "你呢?"


translate chinese day7_dv_bad_ff946cd9:


    "我回避了问题。"


translate chinese day7_dv_bad_50f50099:


    dv "你怎么看我?"


translate chinese day7_dv_bad_ae44c7f6:


    me "我不知道..."


translate chinese day7_dv_bad_0c350a0a:


    dv "你就不能直接说出来?"


translate chinese day7_dv_bad_90c7a728:


    "一段寂静。"


translate chinese day7_dv_bad_ec6e249c:


    th "阿丽夏想要一个直接的答案，但是我自己都想不清楚。"


translate chinese day7_dv_bad_785620ea:


    th "这个少先队夏令营，还有这个想让我说出点什么的女孩子。"


translate chinese day7_dv_bad_5dc514e5:


    me "你看..."


translate chinese day7_dv_bad_a3d0602e:


    "然后我真的明白了。"


translate chinese day7_dv_bad_cff69fc4:


    "我明白现在不是逃避，顾左右而言他。"


translate chinese day7_dv_bad_881eda46:


    "我几乎要闯出来了，暴风雨已经过去了!"


translate chinese day7_dv_bad_ef1f22d6:


    "或者说现在是暴风雨即将到来之前的平静。"


translate chinese day7_dv_bad_fd117b88:


    me "你看...你期待的事情，几乎是不可能的。"


translate chinese day7_dv_bad_a80e7dec:


    dv "看起来什么事情对于你来说都是不可能的。"


translate chinese day7_dv_bad_393056bc:


    "她愤愤不平的说着。"


translate chinese day7_dv_bad_bc3ffa54:


    me "这说起来很奇怪，但是是真的，我不知道自己身上会发生什么... 不仅是说明天，而是连下一秒都是！"


translate chinese day7_dv_bad_1183f209:


    dv "那又能怎么样？你能消失到空气里？"


translate chinese day7_dv_bad_b9c3fa63:


    me "很有可能。"


translate chinese day7_dv_bad_d5f6c4f2:


    "她狂笑不止。"


translate chinese day7_dv_bad_ac122a30:


    dv "我说，我各种理由都猜到了，但是完全没有想到你的话。"


translate chinese day7_dv_bad_0fdb45a6:


    me "这些不是理由，听我说..."


translate chinese day7_dv_bad_1bc70a2f:


    "阿丽夏似乎没有打断的意思。"


translate chinese day7_dv_bad_8de689be:


    me "那个，我并不属于这个地方，{w}我不是说这个夏令营，而是整个这个世界。{w} 你们对于我来说都是奇怪的不能理解的事务。"


translate chinese day7_dv_bad_2ee7cffc:


    dv "你对于我们来说也是。"


translate chinese day7_dv_bad_2a05cd1b:


    me "我都不知道你是不是来自我的星球或者现实世界。"


translate chinese day7_dv_bad_cb2bf9d2:


    dv "有完没完..."


translate chinese day7_dv_bad_117896c8:


    "阿丽夏懒洋洋地说着。"


translate chinese day7_dv_bad_340880cb:


    me "你想想睡醒以后在一个完全不同的地方醒过来。{w} 不仅是不同的地方，还是不同的时代。"


translate chinese day7_dv_bad_c0004c2a:


    me "这就发生在我的身上!{w}我正经住在21世纪，距离这里很远的一个城市！"


translate chinese day7_dv_bad_79a7b35b:


    me "我比现在看起来的要大一些，我有自己的生活，不管是好是坏，那是我自己的生活。"


translate chinese day7_dv_bad_e281434b:


    me "然后有一天我坐着公交车，睡着了，醒来的时候已经来到了这里。"


translate chinese day7_dv_bad_0743d894:


    me "而且变成了一个十七岁的少年。"


translate chinese day7_dv_bad_560a2626:


    "阿丽夏惊讶的看着我。"


translate chinese day7_dv_bad_3439946a:


    dv "你觉得我会相信你?"


translate chinese day7_dv_bad_be346c4b:


    me "我不知道，这要看你...我说出这些不是那么容易的。"


translate chinese day7_dv_bad_82b2a360:


    dv "你真是一个故事大师。"


translate chinese day7_dv_bad_41aa7bd4:


    me "等一下..."


translate chinese day7_dv_bad_de80ec27:


    "我翻出自己的手机给她看。"


translate chinese day7_dv_bad_f584d6ef:


    "让我惊讶的是居然还有电。"


translate chinese day7_dv_bad_15c145d7:


    me "你见过这个吗？你有吗？"


translate chinese day7_dv_bad_45f81bbc:


    "我把它递给阿丽夏。"


translate chinese day7_dv_bad_56878f3a:


    "阿丽夏有点烦躁的乱按着。"


translate chinese day7_dv_bad_aa4e170d:


    dv "好吧...就是某种外国玩具，怎么了？"


translate chinese day7_dv_bad_135e53c8:


    me "不是玩具，这是一个电话。"


translate chinese day7_dv_bad_99ad611b:


    dv "电线呢，听筒呢?"


translate chinese day7_dv_bad_0467a4c4:


    "她笑了。"


translate chinese day7_dv_bad_2f7003bc:


    me "不需要，遗憾的是，我无法向你展示怎么使用因为在你的时代这个东西还没有发明出来。"


translate chinese day7_dv_bad_1e3907df:


    dv "是啊哈哈..."


translate chinese day7_dv_bad_d45cda8e:


    "她怀疑的说着，又把我的手机拿了回去。"


translate chinese day7_dv_bad_1b521e0d:


    me "总之，相不相信你自愿，{w}我和你说这些只是为了解释我的存在。"


translate chinese day7_dv_bad_48d8149b:


    me "我还是不知道一个小时以后会发生什么..."


translate chinese day7_dv_bad_4741fcda:


    dv "好了，你不需要再说了，也不用做出什么决定。"


translate chinese day7_dv_bad_3a7fe15b:


    "她站起来，添了一些火。"


translate chinese day7_dv_bad_b6877455:


    me "如果我对你..."


translate chinese day7_dv_bad_b4919381:


    dv "没有，一切正常。"


translate chinese day7_dv_bad_3e1254a0:


    "她冷冷地说。"


translate chinese day7_dv_bad_39cad26b:


    dv "我明白，你是一个未来的访客，所以像我这样的普通女孩没法吸引你。"


translate chinese day7_dv_bad_71b71623:


    me "你理解错了..."


translate chinese day7_dv_bad_cac50a1b:


    dv "没有，我没有，这很可信，很有逻辑，很令人信服。"


translate chinese day7_dv_bad_d0bafdfc:


    dv "谁都会相信。"


translate chinese day7_dv_bad_6facbf02:


    me "你还是觉得我在编造理由?"


translate chinese day7_dv_bad_9f0b84db:


    "阿丽夏保持沉默。"


translate chinese day7_dv_bad_2284906f:


    me "还是说我完全发疯了?"


translate chinese day7_dv_bad_d558b838:


    dv "你什么都干得出来。"


translate chinese day7_dv_bad_9514ab02:


    me "你为什么要这样?"


translate chinese day7_dv_bad_258b2342:


    "我悲伤的说着。"


translate chinese day7_dv_bad_30e02149:


    dv "我怎么了，我什么也没有做错。"


translate chinese day7_dv_bad_5345aad1:


    "她邪恶的笑着。"


translate chinese day7_dv_bad_0039dd59:


    me "你想想如果这些都是真的，我说这些都是为了什么..."


translate chinese day7_dv_bad_a51804eb:


    dv "我想过了，我理解了..."


translate chinese day7_dv_bad_2534d25c:


    "我还想说点什么，但是路上响起了喇叭声。"


translate chinese day7_dv_bad_99a62d84:


    "路边有一辆公共汽车，一辆普通的LiAZ。"


translate chinese day7_dv_bad_a3ea7899:


    "很奇怪，我们没有注意到它的靠近。"


translate chinese day7_dv_bad_0725f998:


    th "终于！解脱!"


translate chinese day7_dv_bad_d5a384e6:


    dv "我们可以走了吗?"


translate chinese day7_dv_bad_53da10dd:


    "阿丽夏毫无感情的说着。"


translate chinese day7_dv_bad_4bfb0b4b:


    me "好啊，当然了!"


translate chinese day7_dv_bad_963da71b:


    "我拿起书包。"


translate chinese day7_dv_bad_eb09504e:


    dv "我拿得了。"


translate chinese day7_dv_bad_376173e1:


    "她抢走了书包。"


translate chinese day7_dv_bad_3fd88211:


    "显然对她来说太沉了，但是我没有拒绝。"


translate chinese day7_dv_bad_b5b9d022:


    "很快我们平静的坐在汽车的后座上。"


translate chinese day7_dv_bad_994c7297:


    "我的内心充满了各种情感，一方面我因为得以重返文明世界而狂喜，另一方面，我因为和阿丽夏没有讲完的对话而感到愧疚。"


translate chinese day7_dv_bad_26ebea5b:


    me "抱歉，我这样...你本来有所期待..."


translate chinese day7_dv_bad_af909b39:


    dv "我对你没有期待。"


translate chinese day7_dv_bad_8cef92aa:


    "她依旧冷冷的说着。"


translate chinese day7_dv_bad_89272480:


    me "我只是想让你明白...而且说起我对你的感觉，我喜欢你，真的!"


translate chinese day7_dv_bad_64464ef0:


    dv "听到这个好高兴啊..."


translate chinese day7_dv_bad_bb653366:


    "她的声音听起来没那么高兴。"


translate chinese day7_dv_bad_65ecf8a3:


    me "我是说真的，我不知道接下来会发生什么，但是此时此地..."


translate chinese day7_dv_bad_0507e954:


    dv "我猜你昨晚更喜欢我。"


translate chinese day7_dv_bad_3b5928b5:


    me "不，你误会了..."


translate chinese day7_dv_bad_68ac3caa:


    dv "我们是不是趁着司机没有主意继续做啊?"


translate chinese day7_dv_bad_cddb4809:


    me "你冷静一下好吗！我等没有想过!"


translate chinese day7_dv_bad_4e1d6150:


    dv "有什么关系？你想要，我也不在乎，反正我对你也只有这个用处..."


translate chinese day7_dv_bad_30b2cc2a:


    me "阿丽夏等等..."


translate chinese day7_dv_bad_e4d66988:


    dv "好了，我要睡觉了。"


translate chinese day7_dv_bad_fe96cf03:


    "她斜靠在座位上，闭上了眼睛。"


translate chinese day7_dv_bad_079f62e5:


    me "等一下..."


translate chinese day7_dv_bad_b5173cb6:


    dv "我在睡觉！等会儿再说!"


translate chinese day7_dv_bad_af5a03bc:


    "我没有反驳。"


translate chinese day7_dv_bad_699d1594:


    "总之是需要睡一觉来冷静一下，阿丽夏也能冷静一下。"


translate chinese day7_dv_bad_6304447d:


    "虽然说我急切的想要用她明白的方式解释清楚。"


translate chinese day7_dv_bad_71df3cde:


    "但是我退缩了。"


translate chinese day7_dv_bad_1a5a841d:


    "然而我内心仍然十分痛苦。"


translate chinese day7_dv_bad_a20cefa7:


    "..."


translate chinese day7_dv_bad_4a65de63:


    "车上只有我们。"


translate chinese day7_dv_bad_7d4a72bc:


    "车外是无穷的黑暗，偶尔可以看到一棵树，一条小河，或者是电线塔。"


translate chinese day7_dv_bad_0041c3e7:


    "但是整体的景象和夏令营里的没有多大的区别，也许整个世界就是一块圆，我们只不过是在绕圈圈?"


translate chinese day7_dv_bad_401b2186:


    "反正我也不在乎外面有什么，我死死地盯着地板，思考着明天的事情。"


translate chinese day7_dv_bad_594edbed:


    th "明天会发生什么？这个城市，这个星球，这个时代，统统是陌生的..."


translate chinese day7_dv_bad_30bdeb77:


    th "和阿丽夏进一步的对话，虽然她已经对我完全失望了..."


translate chinese day7_dv_bad_a20cefa7_1:


    "..."


translate chinese day7_dv_bad_4eeeabc5:


    "等待的过程比知道结果还要难熬。"


translate chinese day7_dv_bad_85334293:


    "而且已经要发生的事情也不会让你再感觉害怕，只是让你心累。"


translate chinese day7_dv_bad_ab33fd37:


    "汽车驶向未知的前方，我抵抗不住困意，渐渐睡着了。"


translate chinese day7_dv_good_b6233cde:


    dv "我不会那么肯定。"


translate chinese day7_dv_good_7a0d2a29:


    me "我肯定，让我自己做决定吧! "


translate chinese day7_dv_good_cf61573a:


    dv "什么？你什么时候开始做这种傻事了？"


translate chinese day7_dv_good_d0b801cc:


    me "那个..."


translate chinese day7_dv_good_a586fdb0:


    "我突然想起来现在的年龄。"


translate chinese day7_dv_good_7e088224:


    me "十七年前!"


translate chinese day7_dv_good_301b1799:


    dv "好吧，你厉害...说真的，我完全没有注意到这个。"


translate chinese day7_dv_good_80e71ea5:


    me "你应该注意到什么?"


translate chinese day7_dv_good_0ed90974:


    dv "就是你可以毫不怀疑的说出一件事!"


translate chinese day7_dv_good_012bdaba:


    "她这一点说的很对。"


translate chinese day7_dv_good_570d60e3:


    me "那么你到底想要听到什么?"


translate chinese day7_dv_good_fa78d9d9:


    dv "你知道!"


translate chinese day7_dv_good_24d8e0d2:


    "这个暗示游戏没完没了。"


translate chinese day7_dv_good_7dc7d107:


    me "如果你想知道我对你是什么感觉，我可以告诉你，我自己也不清楚。"


translate chinese day7_dv_good_1abe160f:


    dv "我就是这个意思!"


translate chinese day7_dv_good_e51d106b:


    "阿丽夏转了过去。"


translate chinese day7_dv_good_667cf5e8:


    me "我还能做什么？这就是事实!"


translate chinese day7_dv_good_421779c8:


    dv "那我能做什么?"


translate chinese day7_dv_good_30e22da3:


    me "我不指望你做什么!"


translate chinese day7_dv_good_4c06e27c:


    dv "是吗？你确定？"


translate chinese day7_dv_good_eee643db:


    me "是啊..."


translate chinese day7_dv_good_218d5a60:


    "我停顿了一会儿，回答道。"


translate chinese day7_dv_good_a89de161:


    dv "那你这七天里都在做什么?"


translate chinese day7_dv_good_cbe79a70:


    me "我在做什么?"


translate chinese day7_dv_good_6010fd21:


    dv "你在问吧，是不是?"


translate chinese day7_dv_good_014b09c2:


    me "我问什么？为了什么？你说话就不能正常一点？我又不会读心术!"


translate chinese day7_dv_good_8a8032f7:


    dv "如果你想明白，你是会明白的..."


translate chinese day7_dv_good_206c4e41:


    me "你不解释我怎么明白!"


translate chinese day7_dv_good_0ec98f8c:


    dv "好吧，我们的对话毫无意义..."


translate chinese day7_dv_good_42c9c2a4:


    "她站起来向公路走去。"


translate chinese day7_dv_good_b9225b9a:


    me "别!"


translate chinese day7_dv_good_b0133881:


    "我完全失控了，我突然站起来，冲上去，拉住她的手，把她拽了回来。"


translate chinese day7_dv_good_05c5777e:


    "但是我没有计算好需要用多大力气，结果我们摔倒了，把阿丽夏压在了下面。"


translate chinese day7_dv_good_01ae0d80:


    dv "怎么昨天晚上还不够？还想要？"


translate chinese day7_dv_good_18438f67:


    "她的脸有些发热，眼睛闪烁着。"


translate chinese day7_dv_good_444788e7:


    "我有点心烦意乱，我放开她的手，但是没有马上起来。"


translate chinese day7_dv_good_db56aa2f:


    me "我只是想要知道，我从哪里做错了?"


translate chinese day7_dv_good_dd60046c:


    dv "你为什么总觉得自己哪里做错了?"


translate chinese day7_dv_good_7de960a6:


    me "因为你是这么想的!"


translate chinese day7_dv_good_dcad2da3:


    dv "谁跟你说的？我没有!"


translate chinese day7_dv_good_981eb02b:


    me "但是我能看得出来!"


translate chinese day7_dv_good_e3bdc4b7:


    dv "你什么都能看出来，是不是?{w}你看了看，得出结论，认为自己不应该干涉？应该保持低调..."


translate chinese day7_dv_good_7351a8db:


    me "我用自己的理智得出的结论!"


translate chinese day7_dv_good_77d7b000:


    dv "你确定你的理智是对的?"


translate chinese day7_dv_good_5c588803:


    "我不知道怎么回答。"


translate chinese day7_dv_good_40534c30:


    "我们就这样躺在这里。"


translate chinese day7_dv_good_b315c215:


    "我完全糊涂了，阿丽夏看起来也不想继续我们的对话。"


translate chinese day7_dv_good_121b1f59:


    "最后，她推开了我，然后站了起来。"


translate chinese day7_dv_good_c09a48f6:


    dv "那你为什么现在和我...在这里?"


translate chinese day7_dv_good_359b6740:


    me "因为所有人都离开了，只剩下咱们两个..."


translate chinese day7_dv_good_826048a2:


    dv "我不是说那个问题..."


translate chinese day7_dv_good_4f930f50:


    me "那个，因为我喜欢你..."


translate chinese day7_dv_good_58e259d9:


    "我终于直接的说了出来。"


translate chinese day7_dv_good_3d42189c:


    dv "你确定吗？列娜呢？{w}她总是在你旁边..."


translate chinese day7_dv_good_f4da743f:


    dv "也许要搞定她太困难了!我就简单了，手指一挥，我就在你床上了！"


translate chinese day7_dv_good_176694f4:


    th "我不觉得有那么简单..."


translate chinese day7_dv_good_f37462ea:


    me "你不要诬陷人!"


translate chinese day7_dv_good_2a75e18b:


    dv "你什么都没说，所以现在有自由空间！但是你还想着好多人!"


translate chinese day7_dv_good_2563ff87:


    "好吧，这一点她说得对。"


translate chinese day7_dv_good_d5c105bf:


    me "我完全没有想过那种事情!"


translate chinese day7_dv_good_2e22f90c:


    dv "所以我要相信你?"


translate chinese day7_dv_good_ccfdaf24:


    me "你不用相信，我又不会强制你!{w}但这就是事实，我不会对你编故事!"


translate chinese day7_dv_good_fc9f6ff5:


    dv "要用行动证明!"


translate chinese day7_dv_good_30792c5e:


    th "她到底期待着什么?"


translate chinese day7_dv_good_37530dee:


    me "好吧，咱们可以再做一次..."


translate chinese day7_dv_good_dae3bc9a:


    "我傻笑着。"


translate chinese day7_dv_good_d835fa5b:


    "我都不知道自己在这种情况下怎么还敢开这种玩笑。"


translate chinese day7_dv_good_d43bb4d6:


    dv "当然啊，来吧!"


translate chinese day7_dv_good_7a01cb1d:


    "阿丽夏平静的说着，走到篝火旁边，然后开始脱衣服。"


translate chinese day7_dv_good_af45c70b:


    "我跳起来，冲过去拉住她的手。"


translate chinese day7_dv_good_0d1b5397:


    me "你干什么？！我不是那个意思!"


translate chinese day7_dv_good_ed6b05b6:


    dv "怎么？有什么关系？我对你不就是用来干这个的吗。"


translate chinese day7_dv_good_53efb905:


    me "当然不是啊，你理解错了!"


translate chinese day7_dv_good_d81dcc94:


    dv "那我应该怎么理解?!"


translate chinese day7_dv_good_633e9a1b:


    "她抽出了自己的胳膊，然后开始哭了起来。"


translate chinese day7_dv_good_a184da49:


    "我在她身边坐在下来，抱紧了她，轻轻的拍着她的头。"


translate chinese day7_dv_good_7c4d73aa:


    me "没关系，一切都会好起来的，别担心..."


translate chinese day7_dv_good_28b62007:


    dv "别管我!"


translate chinese day7_dv_good_be68715b:


    "她哭喊着，但是没有尝试挣脱。"


translate chinese day7_dv_good_8971dc3e:


    me "那个，我在来到这个夏令营以前的生活很不容易，我只是还没有习惯这里的生活...我不知道在这种情况下应该怎么办。"


translate chinese day7_dv_good_da2cd44a:


    me "我一直都是自己一个人，没有朋友，没有喜欢的人。"


translate chinese day7_dv_good_ee9729c2:


    me "我只是忘记了如何去爱，这对于我来说太难了。"


translate chinese day7_dv_good_70bdd2cd:


    me "只是...把这一切都抛弃掉很容易，然后现在这里的生活...太不一样了，让我很不习惯。"


translate chinese day7_dv_good_f83fac68:


    me "也不是说我就是一个自负的混蛋...虽然我知道自己看上去很像..."


translate chinese day7_dv_good_18509af9:


    me "我只是不能马上反应过来..."


translate chinese day7_dv_good_cc722f9f:


    "阿丽夏没有回答，只是继续哭着。"


translate chinese day7_dv_good_49a9ce42:


    me "但是有一件事情我是可以确定的，我和列娜之间没有任何事情！{w} 她只是一个朋友，我没有往别的方面想过。"


translate chinese day7_dv_good_a5aef048:


    me "而且我需要你，不只是你想的那个方面！"


translate chinese day7_dv_good_9e669319:


    me "我只是不知道怎样和你解释清楚，这里面有很多责任...{w} 你看，我现在的处境，我都不知道一个小时以后自己身上会发生什么事情..."


translate chinese day7_dv_good_48c0bf6b:


    "阿丽夏好像完全听不进去。"


translate chinese day7_dv_good_cd717fa3:


    th "好吧，我应该自豪，我让一个女孩子哭了，哪个女孩儿呢? 阿丽夏，这个一眼看上去一点也不浪漫的孩子!"


translate chinese day7_dv_good_06e93473:


    me "你知道，我不能为咱们两个作出决定。{w} 我只能说，我自己已经想明白了，我可以和你在一起，支持你，保护你!"


translate chinese day7_dv_good_6ef95196:


    me "但是你...你为什么需要我?"


translate chinese day7_dv_good_4a9891cd:


    me "我的灵魂很空虚，我不知道我的人生是为了什么。{w} 而且，我今天是在这里，但是明天我可能就不见了..."


translate chinese day7_dv_good_6196bca7:


    "阿丽夏停止了哭声。"


translate chinese day7_dv_good_64da3e76:


    dv "你不知道我很久以前就做好了那个决定了吗?"


translate chinese day7_dv_good_2e84db32:


    me "那么..."


translate chinese day7_dv_good_5b7bc846:


    "她用充满泪光的眼睛看着我。"


translate chinese day7_dv_good_1ca244f1:


    "我似乎一度从中看到了幸福。"


translate chinese day7_dv_good_7419452b:


    "阿丽夏拥抱着我，轻轻的亲吻着我。"


translate chinese day7_dv_good_0f399005:


    "这是我一生中最美好的吻。"


translate chinese day7_dv_good_794a0ba9:


    "没有昨天晚上那么有激情，但是却充满了温柔，信任和爱，融入了她的心中，让我从她的嘴唇，她的怀抱中感受到温暖。"


translate chinese day7_dv_good_fe659328:


    "我忘记了一切，什么夏令营，什么我的过去，还有不久之前还在折磨我的各种疑问。"


translate chinese day7_dv_good_e3bf8cd7:


    "她就是我需要的一切，永远!"


translate chinese day7_dv_good_63829ee5:


    "也许这个吻还能持续下去，但是突然一阵汽笛声响起。"


translate chinese day7_dv_good_48538083:


    "我转过身，看到一辆老旧的LiAZ，样式很普通。"


translate chinese day7_dv_good_377572e1:


    "我们就这样躺着，看了一会儿。"


translate chinese day7_dv_good_1a224f5a:


    "不一会儿，司机师傅下了车招呼我们。"


translate chinese day7_dv_good_f2e98a50:


    FIXME_voice "嘿，你们这一对儿小情侣，到底走不走啊？这可是去城镇的最后一辆车了！"


translate chinese day7_dv_good_a640ab70:


    dv "我们可以走吗?"


translate chinese day7_dv_good_60c6ccbe:


    "阿丽夏开心的说着。"


translate chinese day7_dv_good_4b4b5248:


    "我站起来，背上书包，拉着阿丽夏的手跑了起来。"


translate chinese day7_dv_good_74424fe8:


    "过了一会儿，我们舒服的坐在了汽车的后座上。"


translate chinese day7_dv_good_b0a142e5:


    "司机在座位上喊着什么，但是我没有听。"


translate chinese day7_dv_good_7e7c36fa:


    th "一艘救生船，载着我们前往新的生活!"


translate chinese day7_dv_good_37a9c52a:


    "我看着阿丽夏，我十分激动的想要说些什么，继续篝火旁的对话。她安静的睡着了，靠在我的肩膀上。"


translate chinese day7_dv_good_099b09a8:


    "我没有叫醒她，没有必要，我们还有很多时间。"


translate chinese day7_dv_good_4a65de63:


    "汽车里只有我们两个。"


translate chinese day7_dv_good_22a0d0c3:


    "汽车走得很快，远远地甩下了我过去的生活，还有我在小猫头鹰夏令营生活的这七天。"


translate chinese day7_dv_good_0a03607e:


    "我在这七天里的经历比我过去的很多年还要丰富，{w}而前方的未来又有多少故事在等待着我呢?"


translate chinese day7_dv_good_200afadf:


    "普通的乡村景色不是很吸引我，我只是看着阿丽夏平静的睡着。"


translate chinese day7_dv_good_83fb48fe:


    "我的心里充满了温暖和平静，我也终于不再思考个不停。"


translate chinese day7_dv_good_a20cefa7:


    "..."


translate chinese day7_dv_good_0ec09f2e:


    "但是幸福是很脆弱的，很多东西都有可能打破它，比如说时间。"


translate chinese day7_dv_good_6b42fada:


    "你的幸福持续的时间越长，它就越是变得平常，接下来便又会出现新的问题。"


translate chinese day7_dv_good_7ba8dcd0:


    "我感觉很好，但是累计的疲劳让我慢慢陷入睡眠..."


translate chinese day7_sl_d19d9343:


    "我在第一缕阳光的照射下就被冻醒了。"


translate chinese day7_sl_05d73391:


    "显然睡袋里还很暖和，而且还没有来得及覆盖上白霜，但是对于我来说也是十分新奇。"


translate chinese day7_sl_8df959d7:


    "我小心的离开了睡袋，避免弄醒斯拉维娅。"


translate chinese day7_sl_01b6883d:


    "清晨清新的空气一扫我的倦意，昨晚的宁静又一次袭上心头。"


translate chinese day7_sl_fa705823:


    "我想这是我一生中最快乐的时光。"


translate chinese day7_sl_78603168:


    "亲密，温柔，爱情，激情，短短的这几个小时里，这些感情环绕着我。"


translate chinese day7_sl_6b037955:


    "我看着斯拉维娅，她是那么美丽。{w}她的脸蛋是那么让人舒心，充满平静。"


translate chinese day7_sl_2b82da62:


    th "我想只有天使才会这样睡觉..."


translate chinese day7_sl_f50d77d4:


    "我不想把她吵醒，我只想坐在这里看着清晨的到来。"


translate chinese day7_sl_3e13649b:


    "大自然...{w}清澈的露水，柔和的晨风，飘舞的落叶，还有斑驳的晨曦，这些昨天对我来说还像是外星球一样的东西。"


translate chinese day7_sl_470e2440:


    "我从前认识的森林都是混凝土的高楼大厦。"


translate chinese day7_sl_485f4671:


    "而且如果有人告诉我有这种东西可以享受的话，虽然这里有美丽的风景和宁静的大自然，但是没有电脑，没有互联网，我是绝对不会相信他们的。"


translate chinese day7_sl_e1ffc0eb:


    sl "早上好!"


translate chinese day7_sl_69f37ce2:


    "我看着斯拉维娅。"


translate chinese day7_sl_987561b6:


    me "早上好！你睡得怎么样?"


translate chinese day7_sl_3b4389a0:


    sl "非常好"


translate chinese day7_sl_46eff4e1:


    me "我也是。"


translate chinese day7_sl_4932445c:


    "她探起身，亲吻着我。"


translate chinese day7_sl_b7a6a5b1:


    "这个像是永恒一般的吻，好像把我吸了进去一样。"


translate chinese day7_sl_4925608a:


    sl "应该吃早饭了。"


translate chinese day7_sl_fab71e09:


    "她最后放开了我说着。"


translate chinese day7_sl_1066065f:


    me "是啊，我想是的。"


translate chinese day7_sl_78042d2e:


    sl "你知道现在几点了吗?"


translate chinese day7_sl_1e9c08b9:


    me "不知道，但是应该还早。"


translate chinese day7_sl_1f18c165:


    sl "希望食堂已经开门了，我超~级~饿。"


translate chinese day7_sl_54aee577:


    me "我也是。"


translate chinese day7_sl_e0880e8b:


    "我们简单的收拾好东西，牵着手走向食堂。"


translate chinese day7_sl_b9e72e01:


    "食堂确实开门了，而且很奇怪的是，我们是最先来的。"


translate chinese day7_sl_79df6cca:


    "早餐不是那么丰盛，这也不奇怪：燕麦粥，两个鸡蛋，干面包，还有兑了很多水的茶。"


translate chinese day7_sl_b27dc208:


    me "话说，我一直不喜欢这里的饭菜。"


translate chinese day7_sl_5e13855b:


    sl "说实话，我也不喜欢。"


translate chinese day7_sl_5e6d7806:


    "斯拉维娅笑了。"


translate chinese day7_sl_e83d08e1:


    me "但是没有选择...{w}而且饿着肚子吃起来还不错。"


translate chinese day7_sl_7b2eabb4:


    sl "是的!"


translate chinese day7_sl_f9be2c84:


    "跟我不一样，她吃起来很小心。"


translate chinese day7_sl_37662204:


    "我还是像平常一样吃了一身。"


translate chinese day7_sl_bfeafc61:


    sl "你总是这样吃饭吗?"


translate chinese day7_sl_72cb68c0:


    me "这有什么问题吗?"


translate chinese day7_sl_3cc85031:


    "我满嘴食物的问道。"


translate chinese day7_sl_e79d08b4:


    "她只是笑了笑。"


translate chinese day7_sl_df14ec0d:


    me "啊...也不总是这样的，但是我非常饿的时候...{w}当然我还可以再文明一些。"


translate chinese day7_sl_50f431fa:


    th "好吧，只是我可以尝试一下。"


translate chinese day7_sl_1108cd46:


    sl "没关系，这没什么。"


translate chinese day7_sl_fc08829f:


    "很快我们吃完了早饭，走向了出口。"


translate chinese day7_sl_68728339:


    "我刚刚注意到我们是食堂里唯一的两个人，突然奥尔加·德米特里耶夫娜出现了。"


translate chinese day7_sl_33a6f7b8:


    mt "啊，是你们...那个，我不会问你们晚上在哪里过夜...所以，那个..."


translate chinese day7_sl_08429d75:


    "辅导员显然有点不好意思。"


translate chinese day7_sl_e173004d:


    sl "早上好，奥尔加·德米特里耶夫娜!"


translate chinese day7_sl_e1614169:


    "斯拉维娅大方的笑着。"


translate chinese day7_sl_96bc1657:


    mt "是啊，早上好...早上好...{w}好吧，想着别忘了收拾东西。"


translate chinese day7_sl_16698d4c:


    sl "我们不会的！"


translate chinese day7_sl_996ab8e2:


    me "那是什么意思？又是远足吗？"


translate chinese day7_sl_02f1b760:


    "我们离开食堂后我问斯拉维娅。"


translate chinese day7_sl_e5708664:


    sl "不，今天是最后一天，你忘了?"


translate chinese day7_sl_09586d84:


    me "什么最后一天?"


translate chinese day7_sl_86874628:


    sl "夏令营的最后一天!"


translate chinese day7_sl_5a084819:


    "可以说她的话让我目瞪口呆。"


translate chinese day7_sl_8f86373c:


    me "怎么？为什么？"


translate chinese day7_sl_88194eef:


    sl "怎么这么惊讶？你不知道？在早上集合的时候说过的。"


translate chinese day7_sl_2d769d89:


    "但是即使是列队集合的时候我也只是站着睡觉而已。"


translate chinese day7_sl_5cb822ee:


    me "没有..."


translate chinese day7_sl_54f37a06:


    sl "现在你知道了!"


translate chinese day7_sl_cfb1c9d5:


    "斯拉维娅哈哈笑了起来。"


translate chinese day7_sl_536cdcc1:


    me "那该做什么...?"


translate chinese day7_sl_70f1dc1a:


    "她的话搅得我心烦意乱。"


translate chinese day7_sl_22659260:


    th "一方面我可能终于可以离开这个被诅咒的夏令营了。"


translate chinese day7_sl_a8307773:


    th "另一方面，我才刚刚找到了那些宝贵的东西，现在就要我放手?"


translate chinese day7_sl_63f53767:


    sl "你是什么意思?"


translate chinese day7_sl_869602ed:


    me "啊，没什么...什么时候出发?"


translate chinese day7_sl_4e45ffe9:


    sl "在晚上，大概是五点到六点。"


translate chinese day7_sl_431ec0f7:


    me "啊，还有很长时间!"


translate chinese day7_sl_818ed3e4:


    sl "是啊，但是我还有事情要做，而且我得去找热妮娅，她可能会比较担心的，我一整个晚上都没有回去。"


translate chinese day7_sl_c62f6955:


    th "是啊，她就是会这样担心的一个人..."


translate chinese day7_sl_142536ec:


    sl "午饭时见!"


translate chinese day7_sl_412b31af:


    "她轻轻的亲了我的脸颊然后跑开了。"


translate chinese day7_sl_9f3c503e:


    "我继续站在食堂的入口处。"


translate chinese day7_sl_cdfd20d4:


    "我什么事也没有，也没有什么可收拾的，我需要做的就是一直到晚上之前在营地里转来转去，思来想去..."


translate chinese day7_sl_23106771:


    th "要可以让我这十个小时里大脑空空什么也不想，我愿意做任何事。"


translate chinese day7_sl_630dd74a:


    "一个声音让我醒了过来。"


translate chinese day7_sl_1acf0704:


    el "你好啊!"


translate chinese day7_sl_e7fb612f:


    "电子小子站在我面前，像平常一样开心。"


translate chinese day7_sl_b5cc7e1c:


    me "你好..."


translate chinese day7_sl_d60a1514:


    "我心不在焉的回答着。"


translate chinese day7_sl_206f44a2:


    el "你知道吗，整个夏令营都在讨论你的事情!"


translate chinese day7_sl_d57bdfe4:


    me "我觉得我知道为什么..."


translate chinese day7_sl_1c122ee3:


    el "所以说...你已经做过了?"


translate chinese day7_sl_c8bd0473:


    "他一边问一边乐。"


translate chinese day7_sl_a19056f4:


    me "你觉得这和你有什么关系吗?"


translate chinese day7_sl_e84597be:


    el "啊，没有，我只是..."


translate chinese day7_sl_ad94eafa:


    me "那就闭嘴!"


translate chinese day7_sl_5c46a06d:


    "我抛下他自己走向广场。"


translate chinese day7_sl_dc17db89:


    th "他怎么总是想法的让大家生气?{w}虽然说也许只是让我生气..."


translate chinese day7_sl_7036dfea:


    "我完全沉浸在自己的想法里，没有注意到四周，知道未来撞上我才发现。"


translate chinese day7_sl_d8625dc6:


    mi "啊，抱歉，我应该小心一点才是！我刚刚一直在想事情，没有注意到你，这种事情确实会发生，啊，对了，你好!"


translate chinese day7_sl_38926eb8:


    "我扫了她一眼，然后继续走着。"


translate chinese day7_sl_425d7917:


    mi "啊，谢苗，等一下! 斯拉维娅怎么样了，告诉我，这太有意思了! 好像，整个夏令营里只有我不知道，不，不是我多么有好奇心，是大家都这么的严肃..."


translate chinese day7_sl_933d1b5a:


    "要说她不是好奇，那可是违背了宇宙发展的规律的。"


translate chinese day7_sl_f6740215:


    me "没有什么特别的。"


translate chinese day7_sl_1e997695:


    "我没有回头的回答着。"


translate chinese day7_sl_dc2db981:


    mi "好吧，如果你不想说..."


translate chinese day7_sl_52f2af91:


    "我尝试着让自己不被外界所干扰，所以未来的话我完全没有听进去。"


translate chinese day7_sl_2d7ae10b:


    "很快广场的景色变成的沙滩。"


translate chinese day7_sl_5e0ecadd:


    "幸运的是，这里还没有被苏联钻石少年占领。（译：意义不明= =，求指导）"


translate chinese day7_sl_0f673e81:


    "我在沙滩上找了一块地方坐了下来。"


translate chinese day7_sl_98d846ec:


    th "为什么整个夏令营都对我和斯拉维娅的关系这么感兴趣?"


translate chinese day7_sl_bb84fa35:


    th "在我自己的世界里，没有人会在乎的，至少不会说出来。"


translate chinese day7_sl_c310401c:


    th "至少你听不见他们背着你说的事情。"


translate chinese day7_sl_0d2a8813:


    "我现在还忍得了八卦，只要他们不会打扰我的个人生活。"


translate chinese day7_sl_5db83ec4:


    "太阳越升越高，而我的不停的打着瞌睡。"


translate chinese day7_sl_948fae16:


    "我的眼睛已经闭上了，突然我听到了有人坐在了我的旁边。"


translate chinese day7_sl_08478924:


    "是阿丽夏。"


translate chinese day7_sl_6a87787c:


    dv "祝贺你!"


translate chinese day7_sl_75ba0772:


    me "什么?"


translate chinese day7_sl_caa43468:


    dv "你不知道吗?"


translate chinese day7_sl_7a835092:


    th "是啊，不难猜测..."


translate chinese day7_sl_321eb6b8:


    me "说真的，我很烦，也很讨厌这个...{w} 谁都要说一说，有完没有!"


translate chinese day7_sl_e5a5925c:


    dv "你还能怎么样？你的事情在夏令营里已经过线了！我都不知道怎么形容了!"


translate chinese day7_sl_ec2c2a7c:


    "她笑了。"


translate chinese day7_sl_513d88a1:


    dv "像那样对待辅导员! 连乌里扬卡都做不到，我可能也是吧..."


translate chinese day7_sl_687ba52e:


    "我不知道怎么回答，不知道这是赞赏还是嘲讽。"


translate chinese day7_sl_9eb3d677:


    me "你知道吗，你太小题大做了，这对于我来说没什么的。"


translate chinese day7_sl_b241d169:


    me "而且我不明白你们为什么要对我指指点点，{w}你们不觉得这完全只是我自己的事情吗?"


translate chinese day7_sl_e9cbda60:


    dv "也许是吧...大概是吧..."


translate chinese day7_sl_576ab57f:


    "阿丽夏神秘的说着。"


translate chinese day7_sl_fc97acab:


    "我们又沉默的坐了一会儿。"


translate chinese day7_sl_749ec097:


    "我就想要让她赶紧离开，但是阿丽夏就坐在这里看着沙滩上的少先队员们，没有一点要离开的意思。"


translate chinese day7_sl_0ee3b46c:


    dv "那么你接下来要做什么呢?"


translate chinese day7_sl_cb23542f:


    me "你在说什么?"


translate chinese day7_sl_518e3571:


    dv "今天是最后一天了。"


translate chinese day7_sl_7321b637:


    me "啊，所以呢?"


translate chinese day7_sl_9772f413:


    dv "大家都会离开的。"


translate chinese day7_sl_23104825:


    me "所以呢?"


translate chinese day7_sl_8503bc50:


    dv "斯拉维娅也会离开的。"


translate chinese day7_sl_38f8adbc:


    me "你也会离开的，所以呢?"


translate chinese day7_sl_26679ce6:


    dv "你得分手了。"


translate chinese day7_sl_6474973e:


    th "我已经明白了，谢谢。"


translate chinese day7_sl_467d3cb1:


    me "当然。"


translate chinese day7_sl_3030a38d:


    dv "你怎么看?"


translate chinese day7_sl_46d67e60:


    me "怎么看，我又不能一直呆在这里。"


translate chinese day7_sl_cbf330c2:


    dv "当然不行啊，但是怎么，你要让她走还是怎么的?"


translate chinese day7_sl_1ecab4b2:


    me "不...我是说，我还能做什么?"


translate chinese day7_sl_fd11f974:


    "实话说，我也不知道下一步怎么办。{w}我的脑袋乱糟糟的。"


translate chinese day7_sl_2aefe5ec:


    th "我可以离开这里，这是这一个星期以来最重要的事情。但是现在我居然要回答一个更加困难的问题。"


translate chinese day7_sl_4892f4fc:


    th "对于苏联的孩子们来说，夏令营的故事如何结束，（我的故事真的是一个夏天的故事？）{w}他们就回家了，然后就没有然后了..."


translate chinese day7_sl_78491662:


    th "他们的家可能距离上千公里。{w} 对我来说简直是不同的世界。"


translate chinese day7_sl_a5e6cfbd:


    th "我看一般人几个小时里可是想不出一个很好的解决方案的。"


translate chinese day7_sl_31914dbe:


    dv "好像我知道似的，这要靠你自己想!"


translate chinese day7_sl_2da8076e:


    "阿丽夏笑着站了起来，跑向了河边。"


translate chinese day7_sl_2b4a56da:


    "距离午饭还有半个小时，所以我慢慢的往食堂走去。"


translate chinese day7_sl_c60c50bf:


    "少先队员们还没有完全占领这里，所以我可以找到一个僻静的角落，虽然说遗憾的是，不是我最喜欢的那个角落。"


translate chinese day7_sl_03bda289:


    "我正要开始吃饭，突然有人在我旁边粗鲁的拉出椅子坐了下来。"


translate chinese day7_sl_d7e5ecca:


    us "你好啊，感觉怎么样?"


translate chinese day7_sl_556f4ec1:


    "是乌里扬卡，而且是异常礼貌版的。"


translate chinese day7_sl_892a8819:


    me "我直到刚刚还很好的。"


translate chinese day7_sl_27c62366:


    "我讽刺道。"


translate chinese day7_sl_11569ff2:


    us "好啦，快告诉我，你要准备怎么做?"


translate chinese day7_sl_59c7266e:


    me "吃饭。"


translate chinese day7_sl_9da1463d:


    "我叹了口气。"


translate chinese day7_sl_10e5fe18:


    us "我不是说那个问题。夏令营结束了，你和斯拉维娅要分开了，你要怎么办呢?"


translate chinese day7_sl_5c9ad26b:


    th "好像我的未来生活是这里的人们关心的最重要的大事。"


translate chinese day7_sl_af94a60d:


    me "我说，这到底有什么重要的?"


translate chinese day7_sl_f13441a0:


    us "只是好奇!"


translate chinese day7_sl_15ec1554:


    me "好奇什么？我都不会问你明天会做什么，后天做什么，一个月以后做什么。你怎么这么关心别人的私生活？？"


translate chinese day7_sl_5b024fb2:


    us "我没有要打扰你的意思。"


translate chinese day7_sl_c6e7d0a8:


    "乌里扬娜生气地说着。"


translate chinese day7_sl_85f844bf:


    us "我对那种事情不是很在行..."


translate chinese day7_sl_7b60b058:


    me "这说对了，你就是个熊孩子。"


translate chinese day7_sl_81615f37:


    us "你有话说，你是这里唯一的大人!"


translate chinese day7_sl_7c3e569d:


    me "我有什么话说?"


translate chinese day7_sl_71f29a50:


    us "你们要不停地写信，然后明年继续参加这个夏令营?"


translate chinese day7_sl_e2283e70:


    th "写信..."


translate chinese day7_sl_33774013:


    "这太神奇了。"


translate chinese day7_sl_7e748919:


    "我不是说信纸信封什么的仪式，我是说我可能会失去斯拉维娅，也许是一年，也许是永远..."


translate chinese day7_sl_946147fa:


    us "怎么了?"


translate chinese day7_sl_e67f8463:


    sl "我可以坐在这里吗?"


translate chinese day7_sl_cf0d6604:


    "我抬起头，看到斯拉维娅端着盘子走了过来。"


translate chinese day7_sl_5f86c688:


    us "好了，我该走了。"


translate chinese day7_sl_b80fdba5:


    "乌里扬卡马上冲到了食堂的另一个角落。"


translate chinese day7_sl_4d6b2adc:


    sl "你们在说什么?"


translate chinese day7_sl_1ec6c417:


    me "你知道的，我们还能说什么?"


translate chinese day7_sl_c17a722d:


    "斯拉维娅笑了。"


translate chinese day7_sl_750701bd:


    me "解决了你的事情了?"


translate chinese day7_sl_0dad5262:


    sl "是的，现在没有事儿了，我可以帮你收拾东西。"


translate chinese day7_sl_7c158bc0:


    me "我没有那么多东西好收拾..."


translate chinese day7_sl_4bb84a95:


    sl "好吧，那么你可以帮我收拾。"


translate chinese day7_sl_71d3b8c8:


    me "好的。"


translate chinese day7_sl_852ac74c:


    "这是和她安静的谈话的好机会。{w}虽然我一点儿也不知道该和她谈什么。"


translate chinese day7_sl_0494524b:


    "我们很快吃完了饭，前往斯拉维娅的房间。"


translate chinese day7_sl_33094fdb:


    "一切都是那么整洁而干净，不像是奥尔加·德米特里耶夫娜的房间一样。"


translate chinese day7_sl_31d68c71:


    sl "啊，我不知道该从何谈起..."


translate chinese day7_sl_917a3612:


    "她拿出一个旅行包，然后开始翻箱倒柜。"


translate chinese day7_sl_1b0f9bba:


    "我这个时候坐在床上整理着自己的思绪。"


translate chinese day7_sl_1cdd2e37:


    me "你关于离开夏令营以后有没有计划呢?"


translate chinese day7_sl_af11ccdd:


    sl "那个，..."


translate chinese day7_sl_943eaf3a:


    "她抬起头，对着我笑了笑。"


translate chinese day7_sl_01aec217:


    sl "我会返回学校。"


translate chinese day7_sl_2c057d1c:


    me "啊...是啊..."


translate chinese day7_sl_5c7b7c48:


    sl "你呢?"


translate chinese day7_sl_79f50df9:


    th "我呢？我好像没有再去上高中的计划..."


translate chinese day7_sl_2260325d:


    me "我？我也是吧，大概。"


translate chinese day7_sl_31433173:


    me "虽然我不太想回去自己城市。"


translate chinese day7_sl_52dd0423:


    th "严肃地说，即使我想要回去，我都不知道怎么才能回去。"


translate chinese day7_sl_70f37a53:


    sl "为什么?"


translate chinese day7_sl_04bffd62:


    me "就是说，我在那里没有事可做，也没有人等着我。"


translate chinese day7_sl_a6afd81b:


    sl "但是你的父母呢?"


translate chinese day7_sl_dd0b7f61:


    me "我的父母...他们现在不在那里。"


translate chinese day7_sl_7a41296e:


    sl "但是他们在哪里呢?"


translate chinese day7_sl_848f65f9:


    me "他们算是，在国外工作。"


translate chinese day7_sl_0857dec3:


    sl "哇，你真的很幸运!"


translate chinese day7_sl_d2b56c39:


    "她开心的笑着。"


translate chinese day7_sl_5ac38cf1:


    me "为什么?"


translate chinese day7_sl_347ac558:


    sl "他们可以给你送来国外的东西什么的。"


translate chinese day7_sl_36322c82:


    th "我不觉得这样就是幸运了。"


translate chinese day7_sl_ce4797d9:


    me "那个，总之..."


translate chinese day7_sl_6a5f7148:


    sl "别沮丧!"


translate chinese day7_sl_aa9df60c:


    th "她好像真的不明白我在说什么。"


translate chinese day7_sl_496c0518:


    me "我没有沮丧。"


translate chinese day7_sl_61615b2e:


    "这是真的。"


translate chinese day7_sl_926dd5d8:


    sl "那是怎么回事呢?"


translate chinese day7_sl_56cbe60a:


    me "很多事情..."


translate chinese day7_sl_b8748488:


    sl "比如说?"


translate chinese day7_sl_eecf1d19:


    me "那个，比如说..."


translate chinese day7_sl_95862924:


    "我正要开始吐露自己过去的生活，还有对斯拉维娅的思念，突然门砰地打开了，热妮娅冲了进来。"


translate chinese day7_sl_d59f2277:


    mz "啊，你在这里。"


translate chinese day7_sl_c58c9392:


    sl "是啊，我们在收拾行李。"


translate chinese day7_sl_dcbd9d98:


    mz "我明白了...还好我没有过十分钟以后再来，不然就要打扰你的“收拾”了。"


translate chinese day7_sl_afb6e684:


    "斯拉维娅红着脸，慌张的继续打包自己的衣物。"


translate chinese day7_sl_67bf96a7:


    mz "怎么了，英勇的情夫?"


translate chinese day7_sl_146cea46:


    "这说法让我感觉很别扭。"


translate chinese day7_sl_ad02f53a:


    me "什么?"


translate chinese day7_sl_95bc0f12:


    mz "没什么..."


translate chinese day7_sl_c7bb77e9:


    "她盯着我看了一会儿。"


translate chinese day7_sl_f9da3a8f:


    mz "小心！现在你的决定可不止会影响到你自己。"


translate chinese day7_sl_60a03249:


    "我没有回答，然后热妮娅就离开了我们的房间。"


translate chinese day7_sl_404c3ae1:


    sl "她是什么意思?"


translate chinese day7_sl_9ec962ea:


    me "可能永远无法猜透她的心思。"


translate chinese day7_sl_f277b278:


    sl "但还是..."


translate chinese day7_sl_2922f709:


    me "那个...我想亲自和你谈谈。"


translate chinese day7_sl_b7c51fee:


    th "我能开始，但是..."


translate chinese day7_sl_d00ac2ba:


    sl "关于什么?"


translate chinese day7_sl_b40211ef:


    "斯拉维娅认真的看着我。"


translate chinese day7_sl_4ee92ca9:


    me "就是说，今天咱们就要离开了，是吗?"


translate chinese day7_sl_5ede84c0:


    sl "是啊。"


translate chinese day7_sl_cb5149bf:


    me "咱们要返回自己的城市了。"


translate chinese day7_sl_be32f309:


    sl "嗯，是的。"


translate chinese day7_sl_774c66dd:


    me "我什么时候还能再次见到你?"


translate chinese day7_sl_bbc1f2e4:


    "斯拉维娅开始思考起来。"


translate chinese day7_sl_8a3c9e52:


    sl "我不知道..."


translate chinese day7_sl_05cb701f:


    me "而且我也不知道..."


translate chinese day7_sl_30ac20c5:


    sl "但是你可以给我写信啊!"


translate chinese day7_sl_6f3d442d:


    "她微笑着，但是她的笑容没有让我安心。"


translate chinese day7_sl_f8d768e0:


    me "是啊，当然，但是，你知道，那是不一样的。"


translate chinese day7_sl_926dd5d8_1:


    sl "那是什么?"


translate chinese day7_sl_d9c6a876:


    me "我可以和你一起走吗?"


translate chinese day7_sl_a2800813:


    "我决定尝试着直白的说出来。"


translate chinese day7_sl_aa131597:


    sl "那你要在那里做什么?"


translate chinese day7_sl_f4e53387:


    "斯拉维娅好像没有很吃惊，但是有点伤脑筋的感觉。"


translate chinese day7_sl_2f7570dd:


    sl "学习，我的父母...人们会怎么说?"


translate chinese day7_sl_7768528c:


    "我不太在乎别人的说法，而且说实在的，他们不怎么关注我。"


translate chinese day7_sl_6e0d3350:


    me "那个，咱们总是可以找到办法的!"


translate chinese day7_sl_c7e0fd92:


    sl "什么办法?"


translate chinese day7_sl_baa95abd:


    me "咱们会想出来的!"


translate chinese day7_sl_5d12fc48:


    sl "谢苗，咱们太年轻了..."


translate chinese day7_sl_0acece57:


    th "事实上，我并不是..."


translate chinese day7_sl_2325fa76:


    me "那怎么了?"


translate chinese day7_sl_655480b8:


    sl "也许日后..."


translate chinese day7_sl_870d8d45:


    me "你需要多长时间？一年？两年？五年？"


translate chinese day7_sl_742d5557:


    sl "你说得好像...我不知道..."


translate chinese day7_sl_dafbd76d:


    "她有点糊涂了。"


translate chinese day7_sl_c39dd65b:


    me "所以这里发生的一切都只是仲夏夜之“梦”?"


translate chinese day7_sl_262d7cae:


    "我有点生气了。"


translate chinese day7_sl_2732ce31:


    sl "没有！当然不是，只是..."


translate chinese day7_sl_ad02f53a_1:


    me "什么?"


translate chinese day7_sl_d9d64c2f:


    sl "我还没有准备好讨论这个事情，咱们以后再说吧。"


translate chinese day7_sl_ffe0dbb0:


    me "什么是以后，咱们只有几个小时就要出发了。"


translate chinese day7_sl_86719bd3:


    "斯拉维娅什么都没有说，只是继续整理着衣物。"


translate chinese day7_sl_e6f46fe1:


    "我预想到了她各种的反应，但是没有想到居然是这样。"


translate chinese day7_sl_31110d51:


    "事实上，她只是把我推开了。"


translate chinese day7_sl_c4036085:


    th "结果她并不需要维持我们的关系。{w}所以说，我对于她来说并没有那么重要。"


translate chinese day7_sl_bad_adf2ccc7:


    me "你看，我还是不明白。"


translate chinese day7_sl_bad_90097fb1:


    sl "什么?"


translate chinese day7_sl_bad_2034de62:


    "斯拉维娅没有回头的问着。"


translate chinese day7_sl_bad_dbb5b4ae:


    me "你为什么不想..."


translate chinese day7_sl_bad_3c8f9ea1:


    sl "你还能怎么办?"


translate chinese day7_sl_bad_2cddef76:


    me "那个，我可以找个工作，租一间公寓。"


translate chinese day7_sl_bad_e867c7b6:


    sl "你会做什么？你至少也得有个什么学位证书?"


translate chinese day7_sl_bad_c6d4d90d:


    th "事实上是有的，就在我衣柜里放着..."


translate chinese day7_sl_bad_ae7c378c:


    me "那不重要!"


translate chinese day7_sl_bad_af407039:


    sl "为什么?"


translate chinese day7_sl_bad_74f68a72:


    me "只要你有目标，你就可以战胜困难，而且这些困难也没有那么巨大。"


translate chinese day7_sl_bad_a9c0b671:


    sl "那只是你的异想天开。"


translate chinese day7_sl_bad_54740717:


    me "也许你是对的，但是不管怎么说，我就是不能理解你的反应!{w}咱们如果离开的话，那就那样了，就结束了..."


translate chinese day7_sl_bad_0f3b42d7:


    sl "我没有那么说。"


translate chinese day7_sl_bad_590152a9:


    me "但是这很明显。"


translate chinese day7_sl_bad_fb896de0:


    sl "你这是从哪儿想出来的!?"


translate chinese day7_sl_bad_544fc2bc:


    "她喊着。"


translate chinese day7_sl_bad_ca84e284:


    "也许这是我第一次见到这样的斯拉维娅。"


translate chinese day7_sl_bad_0d2c9366:


    me "好吧，我换一种说法，我只是不想回到自己原来的地方...{w} 然后只能和你打电话，期待着有一天可以和你见面。"


translate chinese day7_sl_bad_dc38e5e1:


    sl "那对我来说也很难。"


translate chinese day7_sl_bad_7512585c:


    me "所以有什么问题呢?"


translate chinese day7_sl_bad_380f8d60:


    sl "你要知道，不是什么事情都是咱们可以做主..."


translate chinese day7_sl_bad_cfeb918e:


    me "谁做主?!"


translate chinese day7_sl_bad_30abaa79:


    th "虽然没人知道她比我说的对多少，但是如果我真的可以做主的话，我也不会出现在这里。"


translate chinese day7_sl_bad_87a8fd14:


    sl "有社会的规范，道德，法律，伦理等等。"


translate chinese day7_sl_bad_d80f315d:


    me "这些有什么关系，这很重要吗?!"


translate chinese day7_sl_bad_cdfc45ff:


    sl "可能对你来说不是那么重要，但是对我很重要。"


translate chinese day7_sl_bad_2a579a0f:


    me "好吧，本来我可以接受你的行为准则，但是现在还有更重要的东西。"


translate chinese day7_sl_bad_2952e1d9:


    sl "也许是...但是我不能那么突然的..."


translate chinese day7_sl_bad_3d0a9e16:


    me "你没有那么多时间思考。"


translate chinese day7_sl_bad_e18fa2db:


    "我停下来思考着。"


translate chinese day7_sl_bad_a86972c4:


    me "我只能交出我自己...这也是我唯一能交的出来的。"


translate chinese day7_sl_bad_fbcc5341:


    me "再说，我都不知道一个小时以后我身上会发生什么事情，但是我知道不管发生什么，如果我想要重新开始，我要和你开始。"


translate chinese day7_sl_bad_7c100b18:


    sl "你说得太美好了，但是你想想我的感受!{w}对你来说那么简单，但是我有我一直以来的生活，就那样把一切都改变掉..."


translate chinese day7_sl_bad_34cae222:


    me "我也曾经有自己的生活..."


translate chinese day7_sl_bad_adf03656:


    "我温柔的说着。"


translate chinese day7_sl_bad_e07c37b3:


    sl "什么?"


translate chinese day7_sl_bad_3a389e87:


    me "我是说这不可避免。{w}改变就是生活的一部分，你无法逃开的。"


translate chinese day7_sl_bad_235fca6e:


    "斯拉维娅陷入了沉思她拿着背包站着，然后坐在我的身边，头搭在我的肩膀上。"


translate chinese day7_sl_bad_91e26f2b:


    sl "好吧，我相信你，如果你这么说的话，那就这样吧!"


translate chinese day7_sl_bad_ae141d61:


    "这个瞬间我简直感到完美的幸福。"


translate chinese day7_sl_bad_e2ff5a09:


    th "虽然一个星期以前我还在过着完全不同的生活，生活在不同的世界里，但是这无所谓。{w}那些事情都已经消失，对我来说反倒是无法想象的奇怪事物了。"


translate chinese day7_sl_bad_f903c358:


    "我都不知道自己怎么才能觉得那是正常的。"


translate chinese day7_sl_bad_eb2a107a:


    th "现在我已经不太在乎到底是因为什么我来到这里还有怎么来的了。"


translate chinese day7_sl_bad_3548661d:


    th "现在我不再寻找什么答案了。{w}我为什么要找呢，它们不就在我的面前吗?"


translate chinese day7_sl_bad_5a380cf2:


    th "现在我的面前有一天长长的道路，我和自己所爱的人牵着手一起走着。{w}而向后看才是最不合理的。"


translate chinese day7_sl_bad_5734f7c1:


    "我可以就这样一直坐着也不看时间，但是斯拉维娅看了看表，然后在我耳边轻轻说着："


translate chinese day7_sl_bad_b0527f28:


    sl "到时间了。"


translate chinese day7_sl_bad_2df15734:


    me "是啊，是的。{w}你先去车站，我去辅导员的房间拿上自己的东西。"


translate chinese day7_sl_bad_e0c8585e:


    sl "你这样太没有调理了，你应该提前收拾好的。"


translate chinese day7_sl_bad_5e6d7806:


    "斯拉维娅笑了。"


translate chinese day7_sl_bad_34d1b774:


    me "反正我也没有什么可收拾的..."


translate chinese day7_sl_bad_a4b3d627:


    "我离开房间，跑向奥尔加·德米特里耶夫娜的宿舍。"


translate chinese day7_sl_bad_2be7619b:


    "辅导员站在门口，显然是在等着我。"


translate chinese day7_sl_bad_6cda25dd:


    mt "我还以为你不会来了呢..."


translate chinese day7_sl_bad_1a154f74:


    "我没有回答，只是冷冰冰的走进房间，拿上自己的冬装，装进背包里就离开了。"


translate chinese day7_sl_bad_d5902b51:


    mt "来吧，到时间了。"


translate chinese day7_sl_bad_db5dcd8e:


    "过了一会儿我们就来到了车站，和其他少先队员一起。"


translate chinese day7_sl_bad_709d3e7d:


    mt "大家都在这里了吗?"


translate chinese day7_sl_bad_14167bf5:


    "奥尔加·德米特里耶夫娜开始说道。"


translate chinese day7_sl_bad_044ab0d8:


    mt "你们就要离开夏令营了，在分别的时候我有几句话要说。"


translate chinese day7_sl_bad_00123e37:


    "她显然有点紧张，不知道说什么。"


translate chinese day7_sl_bad_019ed77a:


    mt "希望你们在以后的日子中记住在这个夏令营度过的日子，保留着对小猫头鹰夏令营的美好印象。"


translate chinese day7_sl_bad_b48fe53b:


    mt "希望你们至少长大了一些，交到了新朋友，记得...明年再来。"


translate chinese day7_sl_bad_42256d78:


    "辅导员转了过去，看起来她在忍住眼泪。"


translate chinese day7_sl_bad_6a707c88:


    "没想到她还这么动感情，但是她说的我倒是都同意。"


translate chinese day7_sl_bad_45cf8a7c:


    "也许这是我第一次没有把她的话当作耳旁风。"


translate chinese day7_sl_bad_e64058b6:


    "我可能会想念这个夏令营，甚至是奥尔加·德米特里耶夫娜。"


translate chinese day7_sl_bad_53f18694:


    th "虽然说如果这是一段崭新生活的开始的话，我为什么不再来呢?"


translate chinese day7_sl_bad_6eb2ae8d:


    "小猫头鹰被远远地甩在了后面，我迎接着新的生活，和斯拉维娅一起。"


translate chinese day7_sl_bad_d27ec580:


    "月亮在闪耀着，和白天一样明亮，我们越过辽阔的、披着灰色衣装的田野，远远的还装点着暗绿色的森林。"


translate chinese day7_sl_bad_8d851fd8:


    "总之我完全不在乎外面的景色，我很享受这个瞬间。"


translate chinese day7_sl_bad_5fe75290:


    "斯拉维娅和我坐在最后一排，一同看着其他的少先队员们。"


translate chinese day7_sl_bad_9a99d836:


    "乌里扬卡在最前面耍宝，她跑来跑去大喊大叫，列娜在看一本书，阿丽夏在睡觉。"


translate chinese day7_sl_bad_86689381:


    th "这个世界看起来十分正常。"


translate chinese day7_sl_bad_f27eb605:


    th "如果一切这么美好的话，其实我怎么来到这里什么的都是不重要的。我变成了一个不同的人，我遇到了斯拉维娅。"


translate chinese day7_sl_bad_29dcd13b:


    sl "你在想什么?"


translate chinese day7_sl_bad_94622b60:


    me "生命。"


translate chinese day7_sl_bad_0bb42e8e:


    sl "想的怎么样?"


translate chinese day7_sl_bad_1495be07:


    me "很好!"


translate chinese day7_sl_bad_2fd33210:


    "她温柔的笑着。"


translate chinese day7_sl_bad_9bbf9c21:


    me "你知道..."


translate chinese day7_sl_bad_8b01c678:


    sl "能不能等一下，这辆汽车摇晃得我想要睡觉，我需要先休息一下。"


translate chinese day7_sl_bad_4bfb0b4b:


    me "当然!"


translate chinese day7_sl_bad_909286be:


    "她把头靠在我的肩膀上，很快就睡着了。"


translate chinese day7_sl_bad_a20cefa7:


    "..."


translate chinese day7_sl_bad_fec04684:


    "即将到来的，在城镇中心开始的，崭新的生活，这一点也没有让我烦躁。"


translate chinese day7_sl_bad_e23b4758:


    "当你在世界的顶端时，小小的琐事是不会打扰到你的。"


translate chinese day7_sl_bad_d8cf6937:


    "我一感到疲倦就安心的睡着了，完全不去担心。"


translate chinese day7_sl_good_3fe69130:


    me "你只是觉得这对你很复杂!"


translate chinese day7_sl_good_c966fd52:


    sl "这看起来对你来说也很困难，但是不知道为什么你一点也不担心。"


translate chinese day7_sl_good_8f6ce43c:


    me "如果我告诉你说我根本没有家可回，说从上个星期一开始我就失去了自己原来的生活，我就只能从这里开始?"


translate chinese day7_sl_good_e1b3f8e0:


    sl "我不知道...这听起来没什么道理。"


translate chinese day7_sl_good_bc56b5b9:


    me "是的！但是这是真的。"


translate chinese day7_sl_good_217f925e:


    sl "然后呢...请你解释一下。"


translate chinese day7_sl_good_53896a36:


    me "我是意外来到这个夏令营的!{w}事实上我来自另外的时代，也许还是另外一个世界！"


translate chinese day7_sl_good_f88606a6:


    me "我来自二十一世纪初。"


translate chinese day7_sl_good_41a98fea:


    me "而且我比看起来还要大一些。"


translate chinese day7_sl_good_4a22576a:


    me "我都不太清楚自己是怎么到达这里的。"


translate chinese day7_sl_good_08ff2d6a:


    "斯拉维娅看着我，但是什么都没有说。"


translate chinese day7_sl_good_b62b586c:


    "这种尴尬一直持续着。"


translate chinese day7_sl_good_05affea5:


    "我等着她笑话我，说我有病，或者更加糟糕的。"


translate chinese day7_sl_good_f40ae6c7:


    sl "你为什么要告诉我这个?"


translate chinese day7_sl_good_31da2fb6:


    "她最后低声问着我。"


translate chinese day7_sl_good_db105195:


    me "我想要让你理解我的处境。"


translate chinese day7_sl_good_3bb29744:


    sl "但是...这听起来很愚蠢。"


translate chinese day7_sl_good_4c64048c:


    me "当然了！如果我是你的话，我也会这么认为。"


translate chinese day7_sl_good_48a19f73:


    sl "所以你想要什么?"


translate chinese day7_sl_good_53d3fd1d:


    me "我不知道...我只是觉得我得告诉你事实。"


translate chinese day7_sl_good_837efc0e:


    sl "我没有说你说谎，或者你疯了，但是希望你理解，我真的无法接受。"


translate chinese day7_sl_good_2e320730:


    me "是啊，我理解...我只是想要让你更容易理解我做出这些事情的原因。"


translate chinese day7_sl_good_5673e9a6:


    sl "说实话，我没有理解。"


translate chinese day7_sl_good_0257709a:


    "我无奈的叹着气。"


translate chinese day7_sl_good_96e94f68:


    sl "我的观点没有改变。"


translate chinese day7_sl_good_2ed72018:


    me "但是..."


translate chinese day7_sl_good_0848ad63:


    sl "你最好走了，快去收拾你的东西吧，不然就来不及了。"


translate chinese day7_sl_good_c744b4ed:


    me "那还有..."


translate chinese day7_sl_good_07c08f1b:


    sl "咱们等一会儿再说。"


translate chinese day7_sl_good_d9f15fcb:


    "我不打算争吵。"


translate chinese day7_sl_good_a1fa17bf:


    th "如果斯拉维娅在我说出了一切之后还是没有改变想法的意思，那么我继续施压也是没有意义的。"


translate chinese day7_sl_good_00aaa21e:


    "我伤心的往奥尔加·德米特里耶夫娜的房间挪去。"


translate chinese day7_sl_good_f08c18e7:


    "辅导员站在宿舍门口，好像在等着我。"


translate chinese day7_sl_good_959e190e:


    mt "你怎么这么阴沉？发生了什么事？"


translate chinese day7_sl_good_2ba695f3:


    me "啊，没什么。我只是来收拾东西。"


translate chinese day7_sl_good_3e5d4e16:


    mt "哎呀，说出来吧，你可能会感觉好些。"


translate chinese day7_sl_good_44d67563:


    me "那个...你觉得我跟别人说的都是事实，但是她却都不相信我?"


translate chinese day7_sl_good_1bcba649:


    mt "你完全确定自己在说明事实吗?"


translate chinese day7_sl_good_7468f5f7:


    me "绝对!"


translate chinese day7_sl_good_b06f64e7:


    mt "他们可能只是需要时间来思考。"


translate chinese day7_sl_good_d796d61f:


    me "但是已经没有时间了。"


translate chinese day7_sl_good_c9524a2a:


    mt "那我就不知道了。"


translate chinese day7_sl_good_4a9b3777:


    "她无奈的耸耸肩。"


translate chinese day7_sl_good_22cd2b19:


    me "嗯，最后终于出了点事。"


translate chinese day7_sl_good_312eb4e2:


    "我把自己的冬装收进包里，然后前往公交车站。"


translate chinese day7_sl_good_7e15aae1:


    "如果前几天我看到一辆Icarus停在车站等着少先队员们排队离开，我一定超级高兴。"


translate chinese day7_sl_good_9eebbd94:


    "但是现在我却享受不起来。"


translate chinese day7_sl_good_5b7603c0:


    "距离出发还有很长时间。"


translate chinese day7_sl_good_8ef889cc:


    "我坐在路肩上抱着头。"


translate chinese day7_sl_good_d37b9e02:


    un "怎么这么伤心?"


translate chinese day7_sl_good_c32db60d:


    "一个熟悉的声音让我停止了沉思。"


translate chinese day7_sl_good_9553ce6a:


    me "只是..."


translate chinese day7_sl_good_c1368442:


    un "和斯拉维娅吵架了吗?"


translate chinese day7_sl_good_dd69c3a1:


    me "可以这么说。"


translate chinese day7_sl_good_88021e16:


    un "为什么呢?"


translate chinese day7_sl_good_1b550b25:


    me "她不相信我是一个未来的外星人。"


translate chinese day7_sl_good_abcb2c8d:


    "我尴尬的笑着。"


translate chinese day7_sl_good_312181fb:


    un "我也不会相信的。"


translate chinese day7_sl_good_5ffaa56d:


    "列娜严肃的说着。"


translate chinese day7_sl_good_82ba88b1:


    me "如果我是你的话，我也不会相信的。"


translate chinese day7_sl_good_f8d26a13:


    un "没关系的，你们会和好的!"


translate chinese day7_sl_good_3b40e91b:


    me "是啊，但是已经没有多长时间了..."


translate chinese day7_sl_good_1e436020:


    un "干什么?"


translate chinese day7_sl_good_3782f67a:


    me "做出正确的选择..."


translate chinese day7_sl_good_a20cefa7:


    "..."


translate chinese day7_sl_good_a48849f7:


    "过了一会儿整个夏令营的孩子们都聚集到车站附近。"


translate chinese day7_sl_good_a2beb057:


    mt "大家都在这里了吗?"


translate chinese day7_sl_good_14167bf5:


    "奥尔加·德米特里耶夫娜说道。"


translate chinese day7_sl_good_044ab0d8:


    mt "你们就要离开夏令营了，在分别的时候我有几句话要说。"


translate chinese day7_sl_good_00123e37:


    "她显然有点紧张，不知道说什么。"


translate chinese day7_sl_good_019ed77a:


    mt "希望你们在以后的日子中记住在这个夏令营度过的日子，保留着对小猫头鹰夏令营的美好印象。"


translate chinese day7_sl_good_b48fe53b:


    mt "希望你们至少长大了一些，交到了新朋友，记得...明年再来。"


translate chinese day7_sl_good_42256d78:


    "辅导员转了过去，看起来她在忍住眼泪。"


translate chinese day7_sl_good_80682609:


    "没想到她会这么动感情。"


translate chinese day7_sl_good_27a36bac:


    "虽然她的话听起来还是扯淡。"


translate chinese day7_sl_good_e115ea83:


    th "是啊，我和斯拉维娅在这里相遇了，但是现在我们遇到了困难。接下来会发生什么还完全不清楚。"


translate chinese day7_sl_good_e477e76c:


    "所以我也不在乎自己是不是会想念这个地方还有奥尔加·德米特里耶夫娜，不太可能..."


translate chinese day7_sl_good_388908da:


    "当少先队员们开始上车的时候，我穿过人群，找到斯拉维娅。"


translate chinese day7_sl_good_2be46bfb:


    me "咱们坐在一起吧。"


translate chinese day7_sl_good_b58c757b:


    sl "好的。"


translate chinese day7_sl_good_9087bb2a:


    "她低声说着。"


translate chinese day7_sl_good_6fa79838:


    "我甚至觉得自己看到了她脸上微笑的影子。"


translate chinese day7_sl_good_d2835e2a:


    "汽车穿过田野，森林，小溪，丘陵..."


translate chinese day7_sl_good_341bc523:


    "我感觉这里像自己的家乡一样熟悉。"


translate chinese day7_sl_good_c8980b8f:


    "不管怎么说，我现在最不在意的就是外面的情况了，我正陷入了沉思。我在思考着如何开始我们的对话。"


translate chinese day7_sl_good_6c7e0e00:


    "我们就坐在这里，也许有几个小时，但是我的所有尝试都被一个简单的回答敷衍了事。"


translate chinese day7_sl_good_f10d9132:


    me "忘了我之前说的话吧，我不想解释，也不想证明自己是对的。{w}只是现在对我最重要的是你。"


translate chinese day7_sl_good_572579b8:


    sl "我明白。"


translate chinese day7_sl_good_1b595140:


    "她仔细的看着我。"


translate chinese day7_sl_good_8e0333a3:


    me "虽然我有时让人无法理解，{w}请不要在意，现在我最重要的就是你! 而且永远是你。"


translate chinese day7_sl_good_552f93e2:


    me "我知道自己行动起来太不经考虑，我的计划太冒险，但是咱们可以一起努力，你和我!"


translate chinese day7_sl_good_0fb1f792:


    "斯拉维娅继续盯着我。"


translate chinese day7_sl_good_0907c839:


    sl "你明不明白这很困难？明不明白你不能闭着眼睛往黑暗中跑，明不明白有太多的困难，咱们根本解决不了？"


translate chinese day7_sl_good_da9a63e8:


    me "是的，我都明白，但是为了你，我都准备好了。"


translate chinese day7_sl_good_b8586c1f:


    "她没有说话，只是轻轻的亲吻了我一下，然后把头搭在我的肩膀上。"


translate chinese day7_sl_good_62038221:


    "这绝对是同意!"


translate chinese day7_sl_good_44708f32:


    "我不想再打扰她了，就让她睡吧。"


translate chinese day7_sl_good_e8f795a9:


    "关键是斯拉维娅终于理解我了，理解她对我有多么重要，能明白我能为了她做任何事情。"


translate chinese day7_sl_good_24228837:


    "即使我做得太过了，太早了，她也能把我放在合适的道路上。{w} 我们会一起前进的道路!"


translate chinese day7_sl_good_a20cefa7_1:


    "..."


translate chinese day7_sl_good_7aa62860:


    "夜晚逐渐掌握了统治权，汽车上的人们纷纷开始睡觉。"


translate chinese day7_sl_good_5d1c29d3:


    "开始我还在同睡神抗争，但是既然我的未来已经如此清晰，我为什么还要抗争?"


translate chinese day7_sl_good_c01a1705:


    "我的眼睛稍微闭上了一会儿..."
# Decompiled by unrpyc: https://github.com/CensoredUsername/unrpyc
