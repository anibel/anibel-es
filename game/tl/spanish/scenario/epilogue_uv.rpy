
translate spanish epilogue_uv_7e594d31:


    "Es interesante cómo supuestamente unos simples hechos pueden afectar a una persona."


translate spanish epilogue_uv_07e4edc8:


    "Por ejemplo, hay tales acontecimientos extendidos de TOC (trastorno obsesivo compulsivo) entre los niños, como cuando paseas por la calle y tienes que pisar precisamente en las baldosas, evitando las juntas."


translate spanish epilogue_uv_466ed2a9:


    "No es que sea algo relevante, pero si uno no se mantiene al margen a tiempo... olvidándolo o esforzándose uno mismo en pisar sobre las baldosas tal cual... podría volverse un problema grave en el futuro.{w} Por ejemplo, uno podría estar mirando el suelo y ser atropellado por un vehículo..."


translate spanish epilogue_uv_a2969cc6:


    "Nunca tuve algo así como TOC, pero son las nimiedades de mi vida las que ha menudo comportan problemas."


translate spanish epilogue_uv_da2ea9fd:


    "De verdad, ¿qué hay de malo en conocer a una extraña muchacha con orejas de gata en un campamento de pioneros en los confines del espacio y el tiempo, o posiblemente incluso entre mundos...?"


translate spanish epilogue_uv_a337cb21:


    "No parece gran cosa."


translate spanish epilogue_uv_9c59ff6c:


    "Aunque, quizás me acostumbre a que realmente nada existió para mí esta semana. Sólo había este campamento y esta vida."


translate spanish epilogue_uv_9a051ead:


    "Y ése es el motivo por el cual algo como una muchacha felina era algo normal, tal como iban las cosas en este país de las maravillas."


translate spanish epilogue_uv_a79e039c:


    "Ella es sólo otro personaje en esta actuación, ni mejor ni peor que cualquier otro."


translate spanish epilogue_uv_9dda5d32:


    "Pero algo profundo en mi alma me decía que no era así."


translate spanish epilogue_uv_389909c0:


    "Todos los lugareños del campamento parecían normales, a veces demasiado normales, sin nada en absoluto que fuera destacable de ellos."


translate spanish epilogue_uv_f8f578c5:


    "¡Por Dios!{w} Hasta podrías decir que mi vida aquí era simplemente aburrida."


translate spanish epilogue_uv_581326c5:


    "Y ahora..."


translate spanish epilogue_uv_2486bb90:


    "Mi cabeza estaba a punto de estallar de tantos pensamientos, así que abrí mis ojos y me levanté de golpe de la cama."


translate spanish epilogue_uv_33e7bf55:


    "La cabaña de la líder del campamento era igual que de costumbre."


translate spanish epilogue_uv_e0cf3746:


    "Afuera de la ventana un pájaro piaba monótonamente, tratando de reventar los capilares de mis ojos con su canto."


translate spanish epilogue_uv_13ad0098:


    "El reloj marcaba las once de la madrugada."


translate spanish epilogue_uv_82f332bb:


    "¿Por qué no me despertó Olga Dmitrievna?{w} Aunque da igual..."


translate spanish epilogue_uv_2677a9c2:


    "Dormí bastante al menos."


translate spanish epilogue_uv_10e06630:


    "Sin embargo no me sentí completamente recuperado."


translate spanish epilogue_uv_bc948b8d:


    "El mundo entero me hacía reír... el brillante sol de fuera, la brisa soplando ligeramente, el suave susurro de las copas de los árboles, y este asqueroso pájaro que ha traído con él a sus compañeros, obviamente tratando de arruinar mi serenidad."


translate spanish epilogue_uv_7c50b983:


    "El agua de los lavabos era helada como de costumbre."


translate spanish epilogue_uv_bc68213e:


    "Mientras me aseaba mi cara, me di cuenta de que olvidé mi cepillo de dientes, los polvos para dientes y la toalla en la cabaña..."


translate spanish epilogue_uv_99507a71:


    "Qué raro que no me olvidara mi cabeza..."


translate spanish epilogue_uv_252255bb:


    "No podía imaginarme un peor comienzo para el día."


translate spanish epilogue_uv_4e71467e:


    "En ese momento pensaba que era el peor día de todos los que había transcurrido en el campamento."


translate spanish epilogue_uv_6808eaed:


    "Aunque tampoco habían muchas opciones: poco más de un centenar de horas, no suficiente tiempo como para echar raíces de un hogar."


translate spanish epilogue_uv_65142f6d:


    "Hogar...{w} Esa palabra golpeaba dolorosamente en mi cabeza."


translate spanish epilogue_uv_2899cae6:


    "¡¿Me tengo que quedar para siempre aquí?!{w} ¿Pero por qué, cómo, para qué...?"


translate spanish epilogue_uv_cb64a07a:


    "Me senté con impotencia en la hierba y enterré mi rostro entre mis manos."


translate spanish epilogue_uv_0b1b3e6f:


    "Repentinamente me acordé de aquella muchacha.{w} Me pregunto cuántos personajes fantásticos más conoceré aquí..."


translate spanish epilogue_uv_5f2574e3:


    pi "Un día maravilloso, ¿no?"


translate spanish epilogue_uv_96275f4a:


    "No me podía ni siquiera molestar en alzar mi cabeza."


translate spanish epilogue_uv_197a972e:


    me "Claro, podría no ser mejor..."


translate spanish epilogue_uv_e290bdf2:


    "Respondí, manteniendo la misma posición."


translate spanish epilogue_uv_94fa158a:


    pi "¿Por qué eres tan sarcástico?"


translate spanish epilogue_uv_0f3afb25:


    me "¿Sarcástico?"


translate spanish epilogue_uv_fd41eafe:


    "Estaba sorprendido y miré arriba, pero no había nadie en los aledaños."


translate spanish epilogue_uv_f4ec4bfa:


    "¿Acaso es mi imaginación?{w} No, estoy seguro que lo escuché..."


translate spanish epilogue_uv_e99cb8c8:


    "¿Tal vez estoy perdiendo la cordura?"


translate spanish epilogue_uv_0ec9dbdb:


    "Las minas bajo tierra, Shurik, las voces, la muchacha felina, más voces...{w} ¡¿Es que acaso hay algo aquí de lo que pueda estar seguro?!"


translate spanish epilogue_uv_c66393c3:


    "Di un fuerte y doloroso puñetazo en los lavabos e inmediatamente me puse en pie, reteniendo mi mano y maldiciendo."


translate spanish epilogue_uv_43ab9e79:


    me "¡Maldita sea!"


translate spanish epilogue_uv_ad63e7a6:


    "De repente, alguien surgió de los arbustos frente a mí.{w} O alguna cosa..."


translate spanish epilogue_uv_bd8c5549:


    uvp "¡Hola!"


translate spanish epilogue_uv_cf471270:


    "Era la muchacha de ayer."


translate spanish epilogue_uv_401e4c6c:


    me "Hola..."


translate spanish epilogue_uv_faae32c5:


    "No sabía qué decir."


translate spanish epilogue_uv_a12cedc0:


    th "No tenía ni idea de qué esperar de ella."


translate spanish epilogue_uv_206da196:


    "Probablemente ella sea una bestia del infierno y haya venido aquí para poner fin a mi vida.{w} Eso sería así, desde luego, si únicamente no hubiera terminado ya..."


translate spanish epilogue_uv_1f57fbc9:


    "La muchacha felina se quedó mirándome fijamente."


translate spanish epilogue_uv_1330e6b3:


    uvp "¿Duele?"


translate spanish epilogue_uv_59cc3474:


    "Ella miraba mi mano."


translate spanish epilogue_uv_9513cd87:


    me "Sí..."


translate spanish epilogue_uv_87af141e:


    "Contesté tímidamente."


translate spanish epilogue_uv_2fea0ff6:


    uvp "¿Entonces por qué?"


translate spanish epilogue_uv_038a9e0d:


    me "¿Por qué el porqué?"


translate spanish epilogue_uv_09987e32:


    uvp "¿Por qué le diste un puñetazo?"


translate spanish epilogue_uv_45f8c00f:


    me "Pues porque estaba enfadado..."


translate spanish epilogue_uv_f6c23508:


    "Estaba completamente inseguro sobre cómo debería hablarle."


translate spanish epilogue_uv_812a95dc:


    "Quizá una palabra incorrecta, una frase o un gesto podrían ser fatales para mí."


translate spanish epilogue_uv_d817b84a:


    uvp "¿Enojado con quién?"


translate spanish epilogue_uv_550c69d7:


    "Pero la muchacha no mostraba ningún signo de hostilidad... parecía que estaba simplemente preocupada con lo que ocurría."


translate spanish epilogue_uv_bd67e015:


    me "Conmigo mismo."


translate spanish epilogue_uv_39c66d97:


    uvp "Ya veo."


translate spanish epilogue_uv_783cf09d:


    "Sonrió vagamente."


translate spanish epilogue_uv_4d984aa7:


    me "Escucha...{w} No sé cómo debería decirlo, pero debo preguntarte..."


translate spanish epilogue_uv_98d00e80:


    "Me mantuve callado por un instante, para reunir fuerzas."


translate spanish epilogue_uv_99970e7f:


    me "¿Quién eres tú...?"


translate spanish epilogue_uv_72e1758a:


    "La muchacha me miraba perpleja."


translate spanish epilogue_uv_606596a1:


    uvp "Soy yo, ¡por supuesto!"


translate spanish epilogue_uv_a9a44f6b:


    "Dijo con seguridad."


translate spanish epilogue_uv_a343b282:


    me "No, no tengo dudas sobre eso, pero...{w} ¿Cómo debería decirlo...?{w} No conoces un humano con orejas de gata y cola cada día, ya sabes."


translate spanish epilogue_uv_3cea47dd:


    "La muchacha felina examinó cuidadosamente las mencionadas partes del cuerpo y preguntó:"


translate spanish epilogue_uv_41fa5d91:


    uvp "¿Por qué?"


translate spanish epilogue_uv_7a2286cb:


    "Estaba totalmente confundido."


translate spanish epilogue_uv_ed40cefe:


    "Si en los primeros minutos pensé que era necesario estar preocupado, de ser muy cuidadoso, ahora ella resultaba ser más bien una critura espiritualmente bondadosa que un monstruo hostil."


translate spanish epilogue_uv_6ed8834b:


    me "Porque en mi mundo..."


translate spanish epilogue_uv_cf33afc2:


    "Estaba completamente convencido de que no debía esconderle nada."


translate spanish epilogue_uv_60290bff:


    me "En mi mundo esto solamente sucede en los cuentos de hadas."


translate spanish epilogue_uv_1911cc51:


    uvp "¿Qué es un cuento de hadas?"


translate spanish epilogue_uv_acab5d31:


    me "Bueno...{w} Es una historia que no puede ocurrir en la realidad."


translate spanish epilogue_uv_822aded0:


    uvp "Entonces, ¿realmente no existo?"


translate spanish epilogue_uv_63d3671f:


    "La muchacha se rio."


translate spanish epilogue_uv_c54c6597:


    me "No estoy seguro ni de que {i}yo{/i} exista..."


translate spanish epilogue_uv_f0d6ba43:


    uvp "Pero puedo tocarte... así que existes."


translate spanish epilogue_uv_fd243037:


    "Tan sólo pensar el hecho de que ella me pudiera tocar, me hizo sentir incómodo."


translate spanish epilogue_uv_f31233fd:


    me "Bueno, digamos que existo, sí..."


translate spanish epilogue_uv_d7fa0877:


    "Di un paso atrás."


translate spanish epilogue_uv_a630f25b:


    uvp "¡Guay!"


translate spanish epilogue_uv_c453d5a6:


    "La muchacha sonrió y agitó su cola."


translate spanish epilogue_uv_3baee6cb:


    "Hubo un silencio incómodo."


translate spanish epilogue_uv_fb3cf2d4:


    "Todo el tiempo que paso en este campamento, es como si viviera en un sueño."


translate spanish epilogue_uv_6f42630e:


    "A veces cuando duermes te das cuenta de que todo lo que ocurre en tu cerebro es sólo producto de tu imaginación, y sólo necesitas desearlo..."


translate spanish epilogue_uv_d5887cda:


    "Había pensado eso durante los últimos cincos días, este pensamiento existía constantemente en alguna profunda parte de mi alma."


translate spanish epilogue_uv_3eed1590:


    "Aun así, pareciera que todo sucede en realidad.{w} Por lo menos, no podía simplemente despertarme..."


translate spanish epilogue_uv_c4904eec:


    me "Solamente quiero comprender..."


translate spanish epilogue_uv_ad2b8ad1:


    uvp "¿Qué?"


translate spanish epilogue_uv_6842d279:


    "La muchacha seguía sonriendo."


translate spanish epilogue_uv_913229f6:


    me "Cómo llegué aquí...{w} Y cómo salir de aquí."


translate spanish epilogue_uv_ab49bee4:


    "De repente, escuché voces por la carretera así que me di la vuelta."


translate spanish epilogue_uv_efa22042:


    "Slavya y Zhenya se acercaban lentamente a los lavabos."


translate spanish epilogue_uv_bd0cee6c:


    me "Y tú..."


translate spanish epilogue_uv_8391e4f1:


    "Pero la muchacha había desaparecido."


translate spanish epilogue_uv_84dc2808:


    me "¡¿Lo visteis, lo habéis visto?!"


translate spanish epilogue_uv_d436b474:


    "Como si necesitara que alguien me confirmara la existencia de esta criatura.{w} Así me puedo convencer de que no me estoy volviendo loco."


translate spanish epilogue_uv_e48273b0:


    "Qué ironía... volverse loco en la Tierra de Oz...{w} Aunque justo aparecer aquí ya es un billete para el manicomio."


translate spanish epilogue_uv_fd3163fc:


    "E incluso aunque me despertara justo ahora mismo, aun así no estaría muy seguro de si todo fue un sueño."


translate spanish epilogue_uv_bf07f552:


    sl "¿Ver el qué?"


translate spanish epilogue_uv_fcc1f8e9:


    "Las muchachas me miraron perplejas."


translate spanish epilogue_uv_aeb84719:


    me "Alguien a mi lado..."


translate spanish epilogue_uv_4569b623:


    "Apenas podía hacerme explicar."


translate spanish epilogue_uv_300fbab5:


    "Entonces fue una ilusión, ¿no?{w} Así que en realidad..."


translate spanish epilogue_uv_d9eb3a0e:


    mz "¡En serio! Deberías ir a hacerte examinar tu cabeza."


translate spanish epilogue_uv_8c01a57f:


    "Zhenya resopló y me sobrepasó de largo.{w} Slavya sonrió disculpándose y se dio prisa tras ella."


translate spanish epilogue_uv_bda4e78f:


    "¿Es así de simple...?"


translate spanish epilogue_uv_34121b4d:


    "Sólo quería una cosa ahora... ¡que alguien de esta pesadilla creyera que todavía estoy cuerdo!"


translate spanish epilogue_uv_965e44b3:


    "Todo este campamento y sus pioneros no parecían tan reales como ayer."


translate spanish epilogue_uv_cf1121e2:


    "Al final, hace menos de una semana atrás vivía una vida completamente normal de un hombre corriente de su época, y ahora..."


translate spanish epilogue_uv_cd153fa8:


    "¿Quizá todo lo que sucedió me hizo pensar en que todo lo que ocurre es como debería ser?{w} Eso es lo normal... sólo ser, existir aquí."


translate spanish epilogue_uv_baf7b7ad:


    "Y ahora los restos de mi sentido común se aferran desesperadamente a esta muchacha con aspecto de gata con esas orejas, tratando de demostrar a los lugareños de que no estoy loco."


translate spanish epilogue_uv_2c9d53b3:


    "Lo ves... ¡tú no existes, tú!{w} Soy yo en tu cuento, no tú en el mío..."


translate spanish epilogue_uv_0ef30d5d:


    "Mi cabeza estaba a punto de estallar."


translate spanish epilogue_uv_9ee7c0ba:


    "Si no hubiera sido por la señal para ir a comer, tal vez habría ido corriendo a la biblioteca en busca de libros de medicina con instrucciones para lobotomía."


translate spanish epilogue_uv_9cefc21f:


    "La cantina estaba llena como de costumbre.{w} Algunas cosas nunca cambian."


translate spanish epilogue_uv_aea05e6b:


    "Por primera vez miré a todos los pioneros locales desde un ángulo ligeramente distinto."


translate spanish epilogue_uv_8946754d:


    "Desde luego, ¿por qué todo es tan...{w} correcto?"


translate spanish epilogue_uv_aaec030d:


    "Como si alguien hubiera creado este campamento de pioneros recopilando cuidadosamente material de internet, como si ni tan siquiera se hubiese molestado en visitar dicho lugar por sí mismo."


translate spanish epilogue_uv_824ea295:


    "Era justo como una vieja película soviética... la estricta líder del campamento, los ejemplares pioneros.{w} Incluso Ulyana.{w} E incluso Alisa..."


translate spanish epilogue_uv_9b5ba514:


    "Todo dentro de lo habitual."


translate spanish epilogue_uv_fd4fc82b:


    "Entonces, ¿de dónde surge esa extraña muchacha?"


translate spanish epilogue_uv_8c4c3dff:


    "Ella es como una mancha de pintura que sencillamente sucedió en el cuadro a causa de las temblorosas manos de un restaurador chapucero."


translate spanish epilogue_uv_a17c46ba:


    "Electronik repentinamente se sentó a mi lado."


translate spanish epilogue_uv_1acf0704:


    el "¡Qué hay!"


translate spanish epilogue_uv_ede75644:


    "Sonrió con malicia."


translate spanish epilogue_uv_401e4c6c_1:


    me "Ey..."


translate spanish epilogue_uv_0f55d42c:


    el "¿En qué estás pensando?"


translate spanish epilogue_uv_9553ce6a:


    me "Sólo..."


translate spanish epilogue_uv_af68277f:


    "No había nada de malo en absoluto en hablar con franqueza con alguien.{w} No lo había..."


translate spanish epilogue_uv_97647879:


    me "¿Has visto alguien raro por aquí?"


translate spanish epilogue_uv_f20e3f00:


    el "¿Raro?"


translate spanish epilogue_uv_ed10791f:


    me "Bueno...{w} Que no sea humano del todo."


translate spanish epilogue_uv_5aa34a60:


    el "No te entiendo muy bien."


translate spanish epilogue_uv_765c7d3f:


    "Electronik se rio."


translate spanish epilogue_uv_4594de55:


    me "Una muchacha con orejas de gata y una cola."


translate spanish epilogue_uv_8d388b1a:


    "Dije en pocas palabras."


translate spanish epilogue_uv_552282c3:


    el "¿Una muchacha con orejas y una cola...?"


translate spanish epilogue_uv_ba9b1a5b:


    "Comenzó a reflexionar."


translate spanish epilogue_uv_50507c2c:


    el "¿Cómo la conociste a ella?"


translate spanish epilogue_uv_4925911e:


    me "Bueno, asumamos que la vi."


translate spanish epilogue_uv_d7d4400d:


    "Esta conversación comenzaba a ponerse interesante."


translate spanish epilogue_uv_ed9cb410:


    el "¿Vamos a suponerlo o realmente la viste?"


translate spanish epilogue_uv_9b84c34b:


    me "¿De verdad eso es tan importante?"


translate spanish epilogue_uv_3f421fba:


    "Elektronik me miraba fijamente con seriedad."


translate spanish epilogue_uv_d8483462:


    el "Hay una leyenda aquí acerca de una muchacha felina. Ella roba alimentos, objetos pequeños en general y evita encontrarse con la gente."


translate spanish epilogue_uv_b9b6131d:


    me "En serio.{w} ¿Una leyenda?"


translate spanish epilogue_uv_34955a96:


    me "¿Y qué aspecto tiene esa muchacha?"


translate spanish epilogue_uv_abf7efe2:


    el "Yo nunca la he visto.{w} Dicen que tiene una cola y orejas, tal como describiste."


translate spanish epilogue_uv_5587f86a:


    me "¿Y qué dicen?"


translate spanish epilogue_uv_537d8131:


    el "Bueno, esta leyenda es antigua y no sé mucho de ella."


translate spanish epilogue_uv_c93b36d7:


    me "¿Sabes quién puede saber más de esta leyenda?"


translate spanish epilogue_uv_f4cf968f:


    el "Pregúntale a Olga Dmitrievna... ella ha estado en varias ocasiones en este campamento, incluso antes de ser líder de campamento."


translate spanish epilogue_uv_3118d7bc:


    me "Muy bien, gracias."


translate spanish epilogue_uv_e316c160:


    "Me puse en pie y fui a buscar a la líder del campamento."


translate spanish epilogue_uv_e6413ca6:


    "Seguro que decide tomarse una siesta tras comer."


translate spanish epilogue_uv_03870281:


    "Quizás Olga Dmitrievna era el único elemento que, de alguna forma, estaba fuera de lugar en el cuadro de un campamento ideal... ciertamente, ella no era un modelo ejemplar de líder de campamento."


translate spanish epilogue_uv_8014570c:


    "Abrí la puerta y entré con confianza en la cabaña."


translate spanish epilogue_uv_b95f1f60:


    "Y por supuesto, allí estaba ella tumbada en la cama leyendo un libro, tal como esperaba."


translate spanish epilogue_uv_743fc179:


    mt "Semyon... ¿Otra vez desanimado?"


translate spanish epilogue_uv_22f38a3f:


    me "En realidad sí, pero da igual.{w} Quería preguntarte una cosa."


translate spanish epilogue_uv_8822fa3d:


    "Se me quedó mirando fijamente y dudó por un instante."


translate spanish epilogue_uv_79e62762:


    "¿Tal vez no debería ser tan directo con todas las cosas?"


translate spanish epilogue_uv_c6104e29:


    "Era como si mi cortafuegos interno se hubiera activado y, sencillamente, no pudiera contarle a la líder del campamento que había visto una muchacha felina hoy."


translate spanish epilogue_uv_f6bdf29b:


    "Quizá me las apaño como para darlo a entender, evitando preguntas directas, manteniendo callado lo demás."


translate spanish epilogue_uv_ba8beeb1:


    "¡Solamente cuatro días!{w} Aunque me parece haber estado haciendo esto toda mi vida..."


translate spanish epilogue_uv_c5d9d3db:


    me "Olga Dmitrievna, estaba en la comida...{w} y hablé con Elektronik.{w} Y me explicó una leyenda acerca de una muchacha con aspecto de gata, que supuestamente vive aquí."


translate spanish epilogue_uv_a59cb894:


    me "También me dijo que tú sabrías más..."


translate spanish epilogue_uv_1c523a1a:


    mt "¿Para qué quieres saber eso?"


translate spanish epilogue_uv_a1ac3a05:


    "La líder del campamento me hizo una aguda mirada y se puso en pie."


translate spanish epilogue_uv_048f18e0:


    me "¡Por curiosidad!{w} ¡Me gustan estas cosas!"


translate spanish epilogue_uv_90c72986:


    "Traté de sonreír, pero no parecía que la cosa fuera a mejor."


translate spanish epilogue_uv_c224c61a:


    mt "Tenemos un campamento completamente normal."


translate spanish epilogue_uv_f5092004:


    "¡Seguro que sí!{w} ¡Completamente normal!{w} No me quiero ni imaginar lo que tú llamarías como «anormal»... Supongo que algo con zombis y maníacos."


translate spanish epilogue_uv_02e02e2f:


    "«Pesadilla en Sovyonok Street»..."


translate spanish epilogue_uv_50ca27d3:


    me "Sí, lo entiendo, pero...{w} Electronik dijo que tú habías estado antes en este lugar."


translate spanish epilogue_uv_0e31da79:


    mt "Sí..."


translate spanish epilogue_uv_d4764288:


    "La líder del campamento estaba reflexionando sobre alguna cosa."


translate spanish epilogue_uv_d290e285:


    mt "Sin embargo, hay por supuesto una leyenda.{w} Que en alguna parte en los bosques de los alrededores vive una extraña muchacha felina, a veces robando alimentos y pequeños objetos de las cabañas. Le asusta la gente y si por casualidad alguien la ve, sale huyendo."


translate spanish epilogue_uv_388bac12:


    "Eso era bastante diferente de lo que vi esta mañana."


translate spanish epilogue_uv_633cf645:


    "Aquella muchacha estaba observándome, estudiándome, tratando de comprenderme, para nada estaba asustada de mí."


translate spanish epilogue_uv_9696cc5e:


    me "Y tú..."


translate spanish epilogue_uv_4d18c559:


    mt "¿Que si la he visto?"


translate spanish epilogue_uv_bc6747d3:


    "Olga Dmitrievna me interrumpió."


translate spanish epilogue_uv_df6f468a:


    mt "No, no la he visto, pero estuve bastante cerca de verla en diversas ocasiones.{w} Botellas, leche, polvo para dientes y muchas otras cosas desaparecían de mi cabaña a veces.{w} Desaparecían misteriosamente sin dejar rastro alguno."


translate spanish epilogue_uv_91589aaf:


    "Me había olvidado por completo del hecho por el cual todos los pioneros de aquí en realidad no existían, y sólo escuchaba atentamente a la líder del campamento."


translate spanish epilogue_uv_1d9fdb2a:


    mt "Tenía curiosidad de adónde desaparecían las cosas.{w} Pero al final, no hay evidencias de su existencia..."


translate spanish epilogue_uv_b102f359:


    "Ella sonrió como si diera a entender que la conversación había terminado."


translate spanish epilogue_uv_26a5c68f:


    me "¿Y si la hubiera?"


translate spanish epilogue_uv_13679a66:


    mt "¿Por qué estás tan interesado en esto?"


translate spanish epilogue_uv_149896fd:


    me "¿Quién no estaría interesado en algo así...?"


translate spanish epilogue_uv_3c4868cf:


    "A pesar de que todavía tenía muchas cosas por las que devanarme los sesos."


translate spanish epilogue_uv_2c3aefa4:


    mt "No lo sé..."


translate spanish epilogue_uv_983cc84e:


    "La líder del campamento se quedó pensativa por un segundo y luego retornó a su habitual estado de {i}Madre de todos los campamentos{/i}."


translate spanish epilogue_uv_4b9bfbfc:


    mt "Bueno, ¡parece que has decidido tomártelo con calma!{w} Haz algo ya..."


translate spanish epilogue_uv_76e6e018:


    "Hizo una pausa y estiró el cuerpo de tal forma que las arrugas que no deberían estar ahí a su edad, aparecieron."


translate spanish epilogue_uv_6d49ea8f:


    mt "Venga... ¡A tus asuntos!"


translate spanish epilogue_uv_31709fb0:


    "Tan sólo suspiré y salí fuera."


translate spanish epilogue_uv_7ed6de8c:


    "En definitiva, nadie entre los pioneros había visto jamás una muchacha con orejas de gata, sólo habían escuchado «leyendas»."


translate spanish epilogue_uv_4dbdaf04:


    "En fin, ¡probablemente debería buscarla!"


translate spanish epilogue_uv_2dc25f6f:


    "Ya me había recuperado del suceso impactante de esta mañana, y los pensamientos existenciales dejaron de calentarme la cabeza por ahora, así que era posible para mí tomar una decisión razonable, más o menos."


translate spanish epilogue_uv_d50f0920:


    "Era necesario porque todavía tenía muchas preguntas: ¿Cómo llegué hasta aquí? ¿Por qué motivo? ¿El porqué? ¿Y dónde se encuentra el «aquí»?"


translate spanish epilogue_uv_4c7c2569:


    "Y la muchacha felina es el catalizador de todo."


translate spanish epilogue_uv_72f94960:


    "Pero no quería empezar a teorizar otra vez sobre mundos paralelos, alienígenas, experimentos y el más allá."


translate spanish epilogue_uv_f5c40cb4:


    "Ya me alimenté de todo esto justo tras mis primeras horas en este campamento."


translate spanish epilogue_uv_213d5389:


    "Tal vez sólo necesito dejar de lado las teorías y echar un vistazo en la realidad física."


translate spanish epilogue_uv_1c978c35:


    "Incluso si supongo que este mundo existe a pesar de mi voluntad y tengo que vivir en él, mi llegada aquí es inexplicable.{w} Es más provechoso concentrarse en estudiar este mundo y en analizar los posibles hechos que me puedan ocurrir aquí."


translate spanish epilogue_uv_1c050fb2:


    "Haré eso hasta que se demuestre lo contrario. Hasta entonces, asumamos que estoy atrapado aquí.{w} Para un largo tiempo, si no para siempre."


translate spanish epilogue_uv_385b5708:


    "La tumbona chirriaba melodiosamente con cada uno de mis movimientos, las brancas de lilo intentaban golpearme en la cara y los desagradables pájaros matutinos estaban callados por un rato, aparentemente cansados del calor veraniego."


translate spanish epilogue_uv_a3843ea8:


    "Las copas de los árboles parecían como puntiagudos picos de una almena en una muralla de un castillo medieval perdido en un bosque sin límites."


translate spanish epilogue_uv_6f43b891:


    "Y estas cabañas, la plaza, el monumento de Genda y el edificio de clubs son sólo una villa de plebeyos, apiñados cerca de los muros mileniales de una lúgubre fortaleza, escondida en una arboleda."


translate spanish epilogue_uv_641e9777:


    "Y toda la gente de aquí... sean personas como yo o muchachas con aspecto de gata... sólo somos siervos de un malvado genio escondido ahí fuera..."


translate spanish epilogue_uv_cab4416d:


    "Sería mucho más fácil si el campamento estuviera en la cima de una montaña."


translate spanish epilogue_uv_6da72447:


    "Porque hace miles de años atrás el mejor lugar para fortificarse era siempre algún lugar elevado."


translate spanish epilogue_uv_17159f54:


    "Probablemente allí donde {i}alguien{/i} no tendría nada que temer.{w} Pero en dicho caso no se escondería con sumo cuidado detrás de las copas de los árboles."


translate spanish epilogue_uv_c913f529:


    "Suspiré y cerré mis ojos."


translate spanish epilogue_uv_01d78855:


    "Mi consciencia empezaba lentamente a abandonarme y, pronto, el mundo entero se desvaneció en tinieblas."


translate spanish epilogue_uv_ce617998:


    "..."


translate spanish epilogue_uv_1380294c:


    "Y de nuevo, frente a mis ojos, como una alfombra sin fin, la carretera, rodeada por bosques y campos."


translate spanish epilogue_uv_6ebd931c:


    "El autobús se dirige a algún lugar, dejando atrás todo lo que conocí, lo que amé y lo que odié."


translate spanish epilogue_uv_a25e5688:


    "Una muchacha desconocida se inclina sobre mí y me susurra dulcemente algo en el oído."


translate spanish epilogue_uv_aa083268:


    "En realidad estoy interesado en lo que ella me dice."


translate spanish epilogue_uv_7e91da95:


    "Pero no puedo escucharla... Puedo verlo todo a mi lado salvo su rostro."


translate spanish epilogue_uv_a12c5467:


    "Y escuchar todo salvo sus palabras."


translate spanish epilogue_uv_064711df:


    "Y sentirlo todo excepto mi propia vida..."


translate spanish epilogue_uv_e65ed0cf:


    "Me desperté sintiendo como si me estuviera asfixiando y comencé a absorber avariciosamente el aire."


translate spanish epilogue_uv_c51374b9:


    "Mi aliento pronto volvió a la normalidad y una intolerable sed se apoderó de mí."


translate spanish epilogue_uv_9dcbb59a:


    "Pero... ¿De qué iba todo ese sueño?{w} ¿Y quién era esa muchacha?"


translate spanish epilogue_uv_e8dd80d0:


    "Estas preguntas otra vez..."


translate spanish epilogue_uv_4dfb5dfb:


    "Quizás ya me he muerto y estoy en el infierno."


translate spanish epilogue_uv_934cbd27:


    "¿Puede un ser humano sentir que su vida se ha acabado y que sólo puede moverse al siguiente plano de la existencia?"


translate spanish epilogue_uv_8908c3e8:


    "Mi cabeza estaba a punto de estallar con semejantes pensamientos cósmicos."


translate spanish epilogue_uv_f58c6165:


    "¿Qué hice para merecer esto?{w} Solamente soy un tipo normal y corriente, no un filósofo, ni un teólogo y tampoco un científico."


translate spanish epilogue_uv_197dea6a:


    "Y no quiero comprender la razón de ser de las cosas, la esencia del universo.{w} No pido nada más que simplemente vivir mi vida tranquilamente, y luego dormitar en un sueño eterno cuando todo se haya acabado."


translate spanish epilogue_uv_8d9ab812:


    "Pero no... ¡Tan sólo estoy atrapado aquí!"


translate spanish epilogue_uv_666232c3:


    "Lágrimas de frustración brotaron de mis ojos."


translate spanish epilogue_uv_451a9255:


    "¡¿Y por qué siempre es así?!"


translate spanish epilogue_uv_64cbaa90:


    "Comencé a pensar que si continuaba reflexionando más, acabaría por comprender {i}la Esencia de Todas las Cosas{/i} o perdería la razón completamente."


translate spanish epilogue_uv_2773c75d:


    "Básicamente, todo empezó con un autobús... Entré, me dormí y..."


translate spanish epilogue_uv_e44cd0ef:


    "Había una muchacha.{w} Quizás una de los pioneros locales, ¡tal vez incluso aquella con orejas de gata!"


translate spanish epilogue_uv_9e1bfd29:


    "Bueno, ¿por qué no?{w} No puedo descartar ninguna idea.{w} Así que estoy aquí ahora..."


translate spanish epilogue_uv_b758af67:


    "Mis pensamientos se detuvieron. Un cardo ruso daba vueltas adelante y atrás en mis sesos, rebotando alrededor del cerebro y manteniendo mis oídos sin deteriorarse."


translate spanish epilogue_uv_833a0181:


    "Y... eso es todo."


translate spanish epilogue_uv_31afad13:


    "Ahora estoy aquí... en el infierno, en los cielos, en el pugatorio o adonde quiera que sea.{w} Parece que me voy a pasar el resto de la eternidad en este lugar..."


translate spanish epilogue_uv_d5cd5ce9:


    "Por algún motivo me obsesioné con esta exacta teoría."


translate spanish epilogue_uv_092fb80e:


    "Es extraño que no me preocupara por el hecho de que un ser espiritual no tiene sentidos humanos básicos... la respiración, el oído y el tacto."


translate spanish epilogue_uv_c0c03087:


    "En definitiva, a primera vista no había nada de distinto en mí respecto a mi yo de hace una semana atrás.{w} Bueno, excepto por ser un par de años más joven."


translate spanish epilogue_uv_2470ab86:


    "Entonces, ¿qué hay de malo en todo lo que sucedió?"


translate spanish epilogue_uv_e3c414c9:


    "¡Podría considerar incluso esto como una segunda oportunidad!"


translate spanish epilogue_uv_7d31568f:


    "Después de todo, no es tan difícil conseguir un trabajo, podría vivir en una habitación de alquiler en una pensión..."


translate spanish epilogue_uv_5e133ffb:


    "Tengo diecisiete años ahora... todavía estoy sano, mi cerebro está en excelente estado, mi energía está a tope.{w} Puedo lograr algo..."


translate spanish epilogue_uv_7c5d0d68:


    "¿Pero qué fue lo que me impidió hacerlo en el pasado?{w} ¿Acaso era tan mayor entonces?"


translate spanish epilogue_uv_89b8aeef:


    "Me atasqué como un viejo rifle y, sin una mano ajena, no me habría escapado de este círculo vicioso."


translate spanish epilogue_uv_f9d80d04:


    "Afortunadamente, Lena apareció pronto por la cabaña de la líder del campamento."


translate spanish epilogue_uv_bd946546:


    un "Hola."


translate spanish epilogue_uv_dde1f147:


    "Alcé mi mirada de mala gana y me quedé mirándola fijamente por unos segundos."


translate spanish epilogue_uv_401e4c6c_2:


    me "Hola..."


translate spanish epilogue_uv_805ea37c:


    un "Uhmm... Yo..."


translate spanish epilogue_uv_080288a3:


    "Se detuvo, apartó la mirada y se ruborizó."


translate spanish epilogue_uv_6f6c98f9:


    un "En fin, no importa.{w} ¿Puedo sentarme?"


translate spanish epilogue_uv_66a21e69:


    me "Claro."


translate spanish epilogue_uv_d407d0dc:


    "Lena se posó en el borde de la tumbona."


translate spanish epilogue_uv_2cd0045b:


    un "¿En qué estás pensando?"


translate spanish epilogue_uv_62f30bbf:


    me "Tratando de entender las leyes de este mundo."


translate spanish epilogue_uv_bb75283f:


    un "¿Y qué tal te va?"


translate spanish epilogue_uv_b84d3377:


    "Me preguntó con aspecto serio."


translate spanish epilogue_uv_73e269e8:


    me "No muy bien...{w} Quizás están más allá de la comprensión humana."


translate spanish epilogue_uv_c520fb7c:


    un "Pues entonces, tal vez no deberías molestarte..."


translate spanish epilogue_uv_e6abf674:


    me "¿Pero qué debería hacer?{w} Simplemente tengo que hacerlo."


translate spanish epilogue_uv_b672514d:


    un "¿Por qué?"


translate spanish epilogue_uv_2376d290:


    "En serio... ¿por qué?"


translate spanish epilogue_uv_f0d30338:


    "¿Acaso es de alguna vital urgencia?"


translate spanish epilogue_uv_c1b551f0:


    "Realmente en el pasado no me molestaba en pensar cosas así, vivía más o menos en paz."


translate spanish epilogue_uv_fb3cd06a:


    "¿Y ahora qué es lo que ha cambiado tan drásticamente?{w} No pensaré ahora en ello tampoco y viviré de alguna forma en circunstancias distintas, pero ¿y qué tiene eso de malo?"


translate spanish epilogue_uv_1f42124c:


    "Si no hay elección..."


translate spanish epilogue_uv_8473c25d:


    me "No lo sé.{w} Es que parece que nada depende de mí."


translate spanish epilogue_uv_4ac82be2:


    "Era como si Lena quisiera decir alguna cosa, pero no dijo nada."


translate spanish epilogue_uv_701bd2e2:


    me "¿Viniste probablemente para ver a Olga Dmitrievna?"


translate spanish epilogue_uv_f3db2cab:


    un "Sí...{w} Quiero decir... no.{w} Pero, para ser honesta..."


translate spanish epilogue_uv_ad02f53a:


    me "¿Qué pasa?"


translate spanish epilogue_uv_e3352c88:


    un "A veces me siento de esa forma también."


translate spanish epilogue_uv_cb23542f:


    me "¿Qué quieres decir?"


translate spanish epilogue_uv_8c2a3988:


    un "Que nada depende de mí."


translate spanish epilogue_uv_8f838576:


    "La miré con atención."


translate spanish epilogue_uv_dade88e8:


    "Estaba sentada con una mirada apesadumbrada, nerviosa jugueteando con un puñado de flores... eran amarillas como los apasionados rayos del sol veraniego."


translate spanish epilogue_uv_e499d007:


    un "Bueno, ya sabes...{w} Estas flores, por ejemplo... estaban creciendo por el mero hecho de hacerlo, hasta que las arranqué del suelo, ¡y eso es todo!"


translate spanish epilogue_uv_845b4bf2:


    "Ella estaba a punto literalmente de romper a llorar.{w} No se ajustaba a la imagen que tenía de Lena."


translate spanish epilogue_uv_a3df48c6:


    me "Bueno, vale...{w} Para ser sincero, no sé en verdad qué decir."


translate spanish epilogue_uv_e07cb63d:


    un "Bueno, nosotros somos así... nos arrancan y luego..."


translate spanish epilogue_uv_54d3c161:


    "Se puso en pie súbitamente y salió corriendo en dirección a la plaza, sin decirme adiós."


translate spanish epilogue_uv_8e47f89e:


    "Desde luego, Lena, como el resto de pioneros, parecía ser real.{w} No un producto de mi imaginación, ni una muñeca de trapo, ni tampoco un «personaje no controlable»."


translate spanish epilogue_uv_7a418be8:


    "Pocas horas antes estaba seguro de que era el único que existía aquí.{w} Y posiblemente aquella muchacha con orejas de gata."


translate spanish epilogue_uv_b456282f:


    "Desearía poder hallarla..."


translate spanish epilogue_uv_0bcb04b7:


    "Estoy en un estado de algo así como de privación espiritual... todos mis sentidos funcionan apropiadamente, pero todavía tengo una necesidad imperante de saber por qué estoy aquí."


translate spanish epilogue_uv_72ae7014:


    "Como si se hubiera vuelto vital necesitar una identidad, encontrar mi lugar en este mundo, determinar su naturaleza, leyes y mecanismos, tal como la necesidad de alimentarse y descansar."


translate spanish epilogue_uv_5c3cb8c8:


    "Probablemente sea el peor castigo para un humano... tener el deseo de comprender alguna cosa, pero no la aptitud. El deseo de cambiar algo, pero no tener la oportunidad. Esforzarse por alguna cosa, pero ser incapaz de lograrlo."


translate spanish epilogue_uv_837013f3:


    "Nunca lo había pensado antes en serio... ¿por qué me encuentro en este mundo, cuánto tiempo transcurriré aquí, qué es lo que tengo que hacer y qué será lo siguiente?"


translate spanish epilogue_uv_075f5465:


    "Ahora, tras encontrarme en este otro mundo, no me queda nada salvo pensar por mi existencia en este."


translate spanish epilogue_uv_40e0457e:


    "Supongo que si es posible escapar de aquí, retornaré a mi tranquila vida donde no tengo tales problemas."


translate spanish epilogue_uv_250cbd1f:


    "Pero no retornaré..."


translate spanish epilogue_uv_4b055169:


    "Es muy duro afrontar una tarea que no puede ser completada."


translate spanish epilogue_uv_2d594681:


    "Si pudiera escoger mi destino, nunca me quedaría con este autobús, con este campamento, con estas muchachas... Me quedaría allí, en mi piso, frente a la pantalla."


translate spanish epilogue_uv_fbae2843:


    "No puedes demandar más de lo que una persona puede dar de sí."


translate spanish epilogue_uv_b711afb2:


    "Es mejor pasarte el resto de tu vida trabajando en una mina, ¡que tratando de entender algo que tu cabeza sencillamente no puede asimilar!"


translate spanish epilogue_uv_e0d958d5:


    "Al fin y al cabo, no hay otra forma."


translate spanish epilogue_uv_b3b8f58b:


    "Por supuesto, es posible hacer un esfuerzo valiente, tratar de no pensar, tratar de aceptar esta realidad y comenzar una vida nueva."


translate spanish epilogue_uv_785611e4:


    "Pero no es parte de la naturaleza humana ser descuidado con todo lo que ocurre a tu alrededor."


translate spanish epilogue_uv_16a336ea:


    "Si fuera acusado de un crimen que no hubiera cometido, desde luego que me resistiría para demostrar mi inocencia."


translate spanish epilogue_uv_37f26571:


    "Nadie preferiría vivir en una prisión en vez de en casa, independientemente de como sea ese hogar."


translate spanish epilogue_uv_90c2fe99:


    "Y ahora soy yo quien ha sido expulsado del mundo en el que solía vivir a esta «cárcel»."


translate spanish epilogue_uv_4c3a0f63:


    "A pesar de que no tiene límites, aunque no haya muros o barrotes en sus ventanas, sigue siendo una cárcel... Yo no pedí esto, y mi único deseo ahora mismo es volver a casa."


translate spanish epilogue_uv_bb5f8ea5:


    "Y la llave para escapar de aquí se encuentra colgada justo un poco más arriba de lo que puedo alcanzar con un salto."


translate spanish epilogue_uv_c0218e20:


    "Y no hay nada que sea de ayuda cerca... ni sillas, ni taburetes, ni tan siquiera una pequeña piedra que pudiera ser puesta bajo mi pie.{w} Y nadie que me pueda socorrer."


translate spanish epilogue_uv_d6449651:


    "Unos pocos centímetros. Tan sólo unos pocos centímetros me separan de la libertad, de comprenderlo todo..."


translate spanish epilogue_uv_5c3993ed:


    "Tal vez sea imposible volver a casa, ¡pero al menos seré capaz de escapar de esta cárcel!"


translate spanish epilogue_uv_37c1ea83:


    "Eso es exactamente lo que he querido durante estos últimos cinco días..."


translate spanish epilogue_uv_a20cefa7:


    "..."


translate spanish epilogue_uv_9565e04f:


    "La noche descendió sobre el campamento."


translate spanish epilogue_uv_cac0bbd5:


    "La cena transcurrió sin mí, la líder del campamento se detuvo en varias ocasiones frente a la tumbona, tratándome de explicar algo."


translate spanish epilogue_uv_2a186eff:


    "Pero yo ya estaba en otro mundo... el mundo de mi jaula con una llave, atada por un nudo gordiano al hilo de Ariadna."


translate spanish epilogue_uv_a308a72b:


    "No era muy tarde cuando la luz de la cabaña fue apagada y los grillos empezaron con su lamentable quejido por todas partes."


translate spanish epilogue_uv_49f57976:


    "En algún lugar a lo lejos, una lechuza estaba ululando a una velocidad de 37 ululatos por minuto."


translate spanish epilogue_uv_e5c6354e:


    "Estaba sentado y repiqueaba rítmicamente, tratando de acompañarlo."


translate spanish epilogue_uv_58b687db:


    "De repente los arbustos crujieron cerca, y una ardilla con una nuez en sus dientes trepó por un árbol.{w} Debe tener prisa por encontrarse con su familia..."


translate spanish epilogue_uv_be1bbc12:


    "El encanto de la noche veraniega era tan embriagadora como siempre, pero no me afectaba de ninguna manera."


translate spanish epilogue_uv_9a927fac:


    "Los mecanismos de mi cerebro y, probablemente, de mi alma necesitaban un serio mantenimiento, no simplemente una reparación chapucera de la carrocería y un trabajo de pintura nueva."


translate spanish epilogue_uv_cf53bd0d:


    "Decidí dar un paseo."


translate spanish epilogue_uv_978811c4:


    "Porque si lo piensas bien, he estado sentado en esta tumbona casi todo el día.{w} Incluso hice una pequeña siesta..."


translate spanish epilogue_uv_56500c49:


    "Todos en el campamento estaban cansados, estaban durmiendo."


translate spanish epilogue_uv_b459cef6:


    "El paseo entre las cabañas de los pioneros, la plaza con la estatua de Genda, el sendero del bosque, todo eso estaba cambiando como dibujos de un libro para niños.{w} También todo el mundo parecía un retrato."


translate spanish epilogue_uv_bc0e1e5a:


    "Y eso no es lo que estaba pensando justo ayer..."


translate spanish epilogue_uv_abe4b874:


    "Y la razón de ello fue una extraña muchacha con orejas de animal, una muchacha felina (aunque ella podría ser igual que cualquier otro animal)."


translate spanish epilogue_uv_3e0e6364:


    "Es como si hubiera un mecanismo de defensa que se activó tras aparecer aquí, que no me hubiera dejado «buscar respuestas» o «tener una percepción de la situación»."


translate spanish epilogue_uv_0b56ea8e:


    "Siempre habían cosas más importantes que hacer, tales como perderse la formación, conseguir un asiento cómodo para comer o escabullirse de la líder del campamento."


translate spanish epilogue_uv_4dfa815d:


    "Cualquier cosa antes que pensar por qué estoy aquí y cómo puedo escapar.{w} Cualquiera..."


translate spanish epilogue_uv_1e2d6285:


    "Me senté en un tocón y cerré mis ojos."


translate spanish epilogue_uv_d3f7944b:


    "Un millón de imágenes aparecieron por mi cabeza de golpe y, entre ellas, había una carretera, un autobús y aquella muchacha.{w} Sí, la muchacha con orejas de animal.{w} ¡Era ella la que estaba en el autobús!"


translate spanish epilogue_uv_3d57006a:


    "Rápidamente abrí mis ojos y la vi frente a mí."


translate spanish epilogue_uv_30954eab:


    "Al principio era como un sueño o una alucinación, pero una repentina ráfaga de un frío viento me trajo de vuelta a la realidad."


translate spanish epilogue_uv_91d91e6b:


    "La extraña muchacha estaba sentada y esparciendo azúcar por encima de una seta."


translate spanish epilogue_uv_51149e68:


    me "..."


translate spanish epilogue_uv_08781329:


    uvp "Crecen mejor de esta manera."


translate spanish epilogue_uv_b9daa03b:


    "Dijo con una cara seria."


translate spanish epilogue_uv_a444ac12:


    me "¿Quién?"


translate spanish epilogue_uv_70580914:


    "Pronuncié con dificultad."


translate spanish epilogue_uv_d9e00995:


    uvp "Las setas, ¡qué si no!"


translate spanish epilogue_uv_7a3eab5d:


    me "¿Setas...?"


translate spanish epilogue_uv_246a2b06:


    uvp "Sí, setas."


translate spanish epilogue_uv_2a236b27:


    me "¿Para qué necesitas esas setas?"


translate spanish epilogue_uv_de01302f:


    uvp "¡Para comérmelas por supuesto!{w} ¡Para qué si no ibas a necesitar setas!"


translate spanish epilogue_uv_5942e96a:


    "La muchacha hizo un puchero."


translate spanish epilogue_uv_8213e247:


    "Simplemente no sabía cómo iniciar una conversasción con ella, porque estaba seguro de que esa cosa con extraño aspecto de muchacha con orejas de animal me había traído hasta aquí, y no creo que hacer amistad con una critura superior fuera muy buena idea."


translate spanish epilogue_uv_c5d78fa3:


    uvp "¿Tienes más azúcar?"


translate spanish epilogue_uv_58838469:


    "Preguntó de repente."


translate spanish epilogue_uv_941e5272:


    me "No lo sé... No.{w} ¿Para qué la necesitas?"


translate spanish epilogue_uv_3fe32671:


    uvp "¡¿Qué quieres decir con que «para qué»?!{w} Te lo dije... ¡para las setas!{w} Crecen mejor de esa manera."


translate spanish epilogue_uv_82ed9336:


    "A pesar de todo, la muchacha parecía totalmente normal."


translate spanish epilogue_uv_8261b96b:


    "Desde luego, no se ve a menudo una humana con orejas y cola de animal (las cuales parecían de verdad), pero aparte de eso..."


translate spanish epilogue_uv_abb3995c:


    me "Puedo traerte un poco..."


translate spanish epilogue_uv_b283d28e:


    "No sé por qué dije eso, tal vez sólo quería ganarme su confianza."


translate spanish epilogue_uv_b18f4a9e:


    "En tanto que leer la mente no fuera una de sus habilidades."


translate spanish epilogue_uv_cb5b23ec:


    uvp "Está bien, no necesito más. Hay suficiente."


translate spanish epilogue_uv_65bf82fc:


    me "Bien..."


translate spanish epilogue_uv_b4cd8a03:


    uvp "¿Y?"


translate spanish epilogue_uv_ad02f53a_1:


    me "¿Qué?"


translate spanish epilogue_uv_f81f2044:


    uvp "¿Por qué viniste?"


translate spanish epilogue_uv_a91b9930:


    "¿Qué quiere decir?{w} ¿Llegar a este claro o llegar a este campamento?{w} ¿O a esta vida...?"


translate spanish epilogue_uv_1602edd7:


    "Mi cabeza estaba a punto de estallar debido a los pensamientos existencialistas."


translate spanish epilogue_uv_7b2d5c2d:


    "Sólo un poquito más...{w} y todo se volvería cristalino como el agua."


translate spanish epilogue_uv_171177e4:


    "No, «las respuestas» y «la comprensión» no surgen de la nada. Retiremos los dispositivos de seguridad... precaución, las puertas se cierran, no caben más personas en el tren, la siguiente parada es..."


translate spanish epilogue_uv_af4e6296:


    "El tren se fue y yo me quedé en la plataforma sin mi equipaje, vistiendo una delgada chaqueta en los helados veinte grados bajo cero."


translate spanish epilogue_uv_49c71752:


    me "¡Solamente estaba dando un paseo!"


translate spanish epilogue_uv_6ea6f839:


    "Las palabras resonaron huecas en mi cabeza."


translate spanish epilogue_uv_0ab3f93c:


    "Me sentí como un vikingo de la Edad Media que se enfrentaba solo contra cientos de enemigos.{w} Sin dudas, sin remordimientos, ¡únicamente una voluntad de hierro para morir con honor!"


translate spanish epilogue_uv_9c5fc54a:


    uvp "Ya veo..."


translate spanish epilogue_uv_d37bab0a:


    "La muchacha contestó sin mucho interés y volvió a su trabajo."


translate spanish epilogue_uv_35d04d84:


    me "¿Eras tú? ¿Antes allí en el autobús?"


translate spanish epilogue_uv_7d58e7f3:


    uvp "Yo..."


translate spanish epilogue_uv_a307d27a:


    "Respondió perezosamente, sin despegarse de las setas."


translate spanish epilogue_uv_d147d16f:


    me "¿Por qué me traíste aquí?"


translate spanish epilogue_uv_e03d589f:


    uvp "Yo no te traje."


translate spanish epilogue_uv_8b05cf4e:


    me "¿Entonces quién?"


translate spanish epilogue_uv_5dcae342:


    uvp "No lo sé."


translate spanish epilogue_uv_7d60f019:


    me "Pero... ¿tú estabas en el autobús?"


translate spanish epilogue_uv_63ea92fa:


    uvp "Estaba."


translate spanish epilogue_uv_9bfb0137:


    me "¿Y?"


translate spanish epilogue_uv_ad2b8ad1_1:


    uvp "¿Qué?"


translate spanish epilogue_uv_0225570c:


    "Chillarle era peligroso, pero no podía evitarlo."


translate spanish epilogue_uv_afdca9b7:


    me "¡¿Cómo que {i}qué{/i}?! ¡Respóndeme!"


translate spanish epilogue_uv_0fca1ec3:


    uvp "¿Has preguntado algo?"


translate spanish epilogue_uv_ea44f8ab:


    "Es como si esta conversación no le molestara en absoluto."


translate spanish epilogue_uv_9af412a5:


    me "¿Quién eres tú?"


translate spanish epilogue_uv_efd76d15:


    "Me miró sorprendida."


translate spanish epilogue_uv_ec74d78b:


    uvp "¡Yo soy yo!"


translate spanish epilogue_uv_834c0fe5:


    me "Vale...{w} ¿De dónde eres?"


translate spanish epilogue_uv_f09a8f60:


    uvp "No lo sé...{w} Siempre he estado aquí."


translate spanish epilogue_uv_29be4a1c:


    me "¿Qué quieres decir con... siempre?{w} Debes haber llegado de alguna parte, ¿no?"


translate spanish epilogue_uv_4de354e1:


    uvp "No lo sé..."


translate spanish epilogue_uv_064c8591:


    "Dudé por un instante de que tuviera frente a mí una criatura omnipotente."


translate spanish epilogue_uv_d676fe96:


    "Por supuesto, esta muchacha es un poco extraordinaria (para ser más precisos, absolutamente extraordinaria) y ella estuvo conmigo en el autobús antes, pero quizás ella no tiene nada que ver con mi aparición aquí. Cualquier cosa es posible..."


translate spanish epilogue_uv_e4a40e96:


    me "¿Qué sabes?"


translate spanish epilogue_uv_4fd84444:


    uvp "Sé cómo hacer suministros para el invierno."


translate spanish epilogue_uv_9d48f5b9:


    "Sonrió con dulzura y naturalidad."


translate spanish epilogue_uv_3881d52a:


    me "¿Qué suministros?"


translate spanish epilogue_uv_0ace2a6c:


    uvp "¡Bueno, ya sabes! ¡Para el invierno!{w} Podría hacer frío..."


translate spanish epilogue_uv_e5d08bc9:


    uvp "Aunque nunca he visto los inviernos locales."


translate spanish epilogue_uv_a7966ed3:


    me "¿Cómo puede ser que no los hayas visto, si siempre has estado aquí?"


translate spanish epilogue_uv_5dcae342_1:


    uvp "No lo sé."


translate spanish epilogue_uv_1b595140:


    "Me miró atentamente."


translate spanish epilogue_uv_8c4a8a38:


    uvp "¡Además!{w} ¡Me estás aburriendo!"


translate spanish epilogue_uv_7d21a175:


    me "¡¿Yo?!"


translate spanish epilogue_uv_a8361046:


    uvp "Sí, tú."


translate spanish epilogue_uv_ef114c48:


    me "¿Cómo puedes estar aburrida de mí?"


translate spanish epilogue_uv_1169b08a:


    uvp "Tus estúpidas preguntas."


translate spanish epilogue_uv_1373dd1c:


    me "¿Estúpidas...?{w} Bueno, fui arrastrado de mi auténtico mundo, traído aquí, ¡¿y ahora mis preguntas son estúpidas?!"


translate spanish epilogue_uv_d5ed42a0:


    uvp "Sí."


translate spanish epilogue_uv_9087bb2a:


    "Respondió con calma."


translate spanish epilogue_uv_cee893c3:


    me "Y bien, ¿qué preguntas no considerarías estúpidas?"


translate spanish epilogue_uv_22217845:


    uvp "Dónde se puede conseguir azúcar, por ejemplo."


translate spanish epilogue_uv_c71c2028:


    me "En la cantina, ¡¿dónde si no?!{w} Hay montones que podrás robar, eres bien conocida por hacer eso, según he escuchado."


translate spanish epilogue_uv_b1cb93f2:


    uvp "¡Yo no robo!"


translate spanish epilogue_uv_624d137a:


    "La muchacha con orejas de animal estaba enojada."


translate spanish epilogue_uv_5c53c3a4:


    uvp "¡Lo tomo prestado!{w} ¡De todas formas se recupera!"


translate spanish epilogue_uv_b6908ceb:


    me "Se recupere o no...{w} ¡Me trae sin cuidado! ¡Quiero entender cómo y por qué estoy aquí! ¡Y quiero volver!"


translate spanish epilogue_uv_c0d6abbb:


    uvp "No estoy interesada en eso."


translate spanish epilogue_uv_45da52b8:


    "La muchacha se puso en pie y se sacudió."


translate spanish epilogue_uv_3ab93971:


    me "¡Pero yo estoy interesado!{w} Si tú no eres el responsable de todo, ¡entonces debes saber las respuestas!"


translate spanish epilogue_uv_6832591f:


    me "Alguien tiene que saberlo a fin de cuentas..."


translate spanish epilogue_uv_a4748218:


    "Ella me miró atentamente y se rio."


translate spanish epilogue_uv_310acf5d:


    uvp "¡Eres divertido!"


translate spanish epilogue_uv_32c4ee43:


    "Perdí mi paciencia completamente."


translate spanish epilogue_uv_acb541be:


    me "¡Deja de tomarme el pelo! ¡Tú! ¡Estoy harto de ti! ¡¿Por qué yo precisamente?! ¡¿Acaso soy peor que cualquier otro?! ¡Nunca pedí algo «divertido» como todo esto! ¡Déjame solo! ¡Nunca molesté a nadie! ¡No herí a nadie! ¡¿Qué te hice a ti?! ¡Piérdete! ¡Déjame vivir en paz!"


translate spanish epilogue_uv_10514786:


    "Todo mi cuerpo se estremeció temblando, la sangre latía en mi cabeza como un torrente, todo se oscureció frente a mis ojos."


translate spanish epilogue_uv_da5992ef:


    uvp "¿Ya has soltado toda la mala leche?"


translate spanish epilogue_uv_1faf32bf:


    "Preguntó en voz baja."


translate spanish epilogue_uv_59558a54:


    "Estaba a punto de zurrarla.{w} Por lo menos así comprobaría si era de carne y hueso."


translate spanish epilogue_uv_bef37e18:


    uvp "Muy bien, vamos."


translate spanish epilogue_uv_477fc689:


    me "¿Dónde...?"


translate spanish epilogue_uv_d1be1fae:


    uvp "Ya verás."


translate spanish epilogue_uv_e23148b5:


    "La muchacha me hizo un gesto para que la siguiera y se adentró en las profundidades del bosque."


translate spanish epilogue_uv_ce617998_1:


    "..."


translate spanish epilogue_uv_f63b1b8b:


    "Tras un rato comencé a sospechar que estábamos caminando en círculos."


translate spanish epilogue_uv_97a205cc:


    "Finalmente, decidí comprobarlo poniendo un envoltorio de caramelo en un lugar destacable."


translate spanish epilogue_uv_a6536d01:


    "Afirmativo, pronto nos lo cruzamos otra vez."


translate spanish epilogue_uv_03e5b22a:


    me "Espera."


translate spanish epilogue_uv_9d8a1030:


    "No me atreví a agarrarla de la mano, así que intenté detenerla verbalmente."


translate spanish epilogue_uv_ad2b8ad1_2:


    uvp "¿Qué?"


translate spanish epilogue_uv_58955638:


    me "¿Dónde me estás llevando?"


translate spanish epilogue_uv_77a8b489:


    uvp "¡Te dije que lo verías!"


translate spanish epilogue_uv_5b06e11d:


    me "Pero estamos caminando en círculos."


translate spanish epilogue_uv_c379a4f1:


    uvp "¿Y qué?"


translate spanish epilogue_uv_20b0f9b4:


    me "¿Qué quieres decir con «y qué»?{w} ¡No irás a ninguna parte caminando en círculos!"


translate spanish epilogue_uv_c5a6ae3e:


    uvp "Pero si ya lo dijiste tú mismo, que solamente estabas dando un paseo..."


translate spanish epilogue_uv_ab154d49:


    "La muchacha parecía rara, ilógica, incluso arrogante, como si disfrutara en tomarme el pelo."


translate spanish epilogue_uv_4c94d3cf:


    me "No pienso moverme ni un centímetro más hasta que me expliques qué sucede."


translate spanish epilogue_uv_d8d14dde:


    uvp "Vale..."


translate spanish epilogue_uv_077824e8:


    "Se detuvo y se apoyó contra un árbol grande."


translate spanish epilogue_uv_b3106bae:


    uvp "¡Sólo estaba aburrida! Es siempre lo mismo una y otra vez...{w} Pero recientemente muchas cosas nuevas han ocurrido y ahora..."


translate spanish epilogue_uv_2827a713:


    me "¿Y ahora qué?"


translate spanish epilogue_uv_3bdae7a5:


    uvp "Ahora puedo hablar contigo."


translate spanish epilogue_uv_d1046a49:


    "Ella sonrió."


translate spanish epilogue_uv_60f5333f:


    me "¿Y no podías hacerlo antes?"


translate spanish epilogue_uv_020b0a78:


    uvp "Bueno, ciertamente podía.{w} Tal vez..."


translate spanish epilogue_uv_29f71b5c:


    me "¿Y qué ocurrió?"


translate spanish epilogue_uv_5dcae342_2:


    uvp "No lo sé."


translate spanish epilogue_uv_9ffdf555:


    me "Pues entonces, en primer lugar, ¿por qué crees que alguna cosa ha sucedido?"


translate spanish epilogue_uv_60a48766:


    uvp "Tengo un presentimiento."


translate spanish epilogue_uv_0657f52c:


    me "Un presentimiento, ¿eh...?"


translate spanish epilogue_uv_0b4ccb3b:


    "Hice un suspiro inútil."


translate spanish epilogue_uv_a531a3e7:


    "¿Quizás ella sólo es una lunática que se escapó del psiquiátrico local y no una criatura omnipotente?"


translate spanish epilogue_uv_2c88fada:


    me "Venga ya, seamos serios."


translate spanish epilogue_uv_2df5ed7c:


    uvp "¡Vale, está bien!"


translate spanish epilogue_uv_92199a18:


    "La muchacha me miró atentamente."


translate spanish epilogue_uv_8e9d069a:


    me "¿Quién eres tú y que está sucediendo aquí?"


translate spanish epilogue_uv_b9debf29:


    uvp "¡No lo sé!"


translate spanish epilogue_uv_7a944f53:


    "Respondió con la misma expresión de antes en su rostro."


translate spanish epilogue_uv_be4b5d36:


    me "¿Y quién lo sabe?"


translate spanish epilogue_uv_b9debf29_1:


    uvp "¡No lo sé!"


translate spanish epilogue_uv_007d8828:


    me "Muy bien. Pues me voy a dormir."


translate spanish epilogue_uv_b364eb6c:


    "No tenía fuerzas para hablar sobre ello, para escuchar sus absurdos, ni para caminar en círculos por el bosque."


translate spanish epilogue_uv_a7f2edd7:


    "Incluso aunque ella supiera las respuestas y pudiera retornarme a mi verdadero mundo y todo eso, hoy no sería.{w} ¡Así que ya basta por hoy!"


translate spanish epilogue_uv_d878403f:


    "Si me quedo un segundo más aquí, no habrá nadie para volver."


translate spanish epilogue_uv_89b1c070:


    uvp "Buenas noches."


translate spanish epilogue_uv_4e7cba70:


    "Dijo la muchacha educadamente."


translate spanish epilogue_uv_5c1ed51e:


    me "¿Eso es todo?"


translate spanish epilogue_uv_e6b99436:


    uvp "¿Qué quieres si no? Te vas a dormir."


translate spanish epilogue_uv_e4426188:


    "Ella tenía razón..."


translate spanish epilogue_uv_994a9bf3:


    me "Vale. ¿Tienes al menos un nombre?"


translate spanish epilogue_uv_c07ebab2:


    uvp "¿Nombre?"


translate spanish epilogue_uv_ff627021:


    me "¡Sí, un nombre!"


translate spanish epilogue_uv_4de354e1_1:


    uvp "No lo sé..."


translate spanish epilogue_uv_be4b5d36_1:


    me "¿Y quién lo sabe?"


translate spanish epilogue_uv_b9debf29_2:


    uvp "¡No lo sé!"


translate spanish epilogue_uv_8225d824:


    me "Vale. ¿Y entonces cómo debería llamarte?"


translate spanish epilogue_uv_4de354e1_2:


    uvp "No lo sé..."


translate spanish epilogue_uv_6942194c:


    me "Pero una persona... todas las cosas en el mundo deben tener un nombre, no hay otra forma..."


translate spanish epilogue_uv_4de002b5:


    "Me miraba incomprensivamente."


translate spanish epilogue_uv_08ce5481:


    me "Muy bien pues, ¡serás Yulya!"


translate spanish epilogue_uv_7b802b26:


    uvp "Yulya..."


translate spanish epilogue_uv_69f1b777:


    "No sé por qué elegí ese nombre. Quizá sea sólo lo primero que se me pasó por la mente."


translate spanish epilogue_uv_b9209126:


    uvp "Está bien."


translate spanish epilogue_uv_d2b56c39:


    "Sonrió alegremente."


translate spanish epilogue_uv_7d1b4f7a:


    uv "¡Y ahora me tengo que ir!"


translate spanish epilogue_uv_61d7e5e8:


    "La muchacha movió sus orejas rápidamente y desapareció en el bosque."


translate spanish epilogue_uv_6e5f9416:


    "Lentamente arrastré mis pies de vuelta al campamento."


translate spanish epilogue_uv_fa5ab919:


    "Ahora parecía estar en alguna especie de tragicomedia."


translate spanish epilogue_uv_2bbab062:


    "Las criaturas superiores no pueden ser tan idiotas al nivel de un parvulito, ¿verdad?"


translate spanish epilogue_uv_a774658e:


    "Aunque también es posible que no perciban el mundo de la misma forma en que lo hacemos nosotros..."


translate spanish epilogue_uv_e30b61a2:


    "Estaba muy cansado. No podía comprender totalmente todo lo que me había sucedido recientemente."


translate spanish epilogue_uv_a70e7a5c:


    "Las respuestas podrían estar justo frente a mí, solamente tenía que estirar mi brazo.{w} Podría ser ese taburete que me salvara, aquel que me socorrería para alcanzar la llave."


translate spanish epilogue_uv_674811f4:


    "¿Y cómo podría estar tan tranquilo en semejante situación?"


translate spanish epilogue_uv_1e24e1ff:


    "Quizás sólo estoy cansado...{w} Muerto de cansancio."


translate spanish epilogue_uv_634747fb:


    "Olga Dmitrievna me esperaba en la entrada de su cabaña."


translate spanish epilogue_uv_87b272e3:


    mt "¡Por fin!{w} ¿Dónde estabas vagando a las tantas de la noche?"


translate spanish epilogue_uv_19d8b6a9:


    "Miré con dificultades a la líder del campamento."


translate spanish epilogue_uv_2dc57d68:


    "Me pregunto si ella es real...{w} No tengo ningún género de dudas acerca de esa muchacha, Yulya.{w} Puede que ella no sea una humana, pero existe."


translate spanish epilogue_uv_afc131f8:


    "¿Y Olga Dmitrievna? ¿Y los otros pioneros?"


translate spanish epilogue_uv_234e69a7:


    me "Sabes, vi aquella muchacha felina."


translate spanish epilogue_uv_975e5089:


    mt "Claro, por supuesto..."


translate spanish epilogue_uv_393056bc:


    "Ella soltó una risita."


translate spanish epilogue_uv_e7cbadb2:


    me "Bueno, no tienes por qué creerme si no quieres."


translate spanish epilogue_uv_f0a7d1a8:


    mt "¡Ni quiero, ni te creo!"


translate spanish epilogue_uv_099b3cc6:


    me "Es tu decisión."


translate spanish epilogue_uv_339a89c8:


    "Dije con voz agotada y me abrí paso hacia el interior de la cabaña."


translate spanish epilogue_uv_6f09955a:


    mt "Semyon, mañana..."


translate spanish epilogue_uv_48e685fd:


    "No escuché el resto de sus palabras."


translate spanish epilogue_uv_bd727701:


    "Sólo había una cosa que comprender... tanto si hay algo que dependa de mí como si no."


translate spanish epilogue_uv_07b2b5de:


    "¿Debía seguir buscando desesperadamente aquella llave, o es la muchacha con aspecto de gata la respuesta que estuve buscando?"


translate spanish epilogue_uv_e8bc9d3a:


    "¿Tal vez la única cosa que queda es entenderlo?"


translate spanish epilogue_uv_949ae8dc:


    "¡Nadie dijo que la pista fuera escrita sencillamente en tu propio idioma!"


translate spanish epilogue_uv_5c917e85:


    "A pesar de todo, ¿quizá siempre supe aquellas «respuestas» y solamente tenía que comprender su significado...?"


translate spanish epilogue_uv_86ab4f38:


    "¿Y si nada es real y solamente estoy soñando?"


translate spanish epilogue_uv_30cf5a85:


    "¿Y si sólo me lo imaginé todo acerca de «hallar respuestas», sobre «comprenderlos»?"


translate spanish epilogue_uv_d1653018:


    "Quizás no hay jaula, no hay cárcel y la llave ya estaba colgando arriba en la desgastada bombilla de mi habitación, la cual se ha convertido en un campamento de pioneros, habitado por personas imaginarias pero bastante vivas, las cuales parecen ser muy comprensibles y valiosas."


translate spanish epilogue_uv_b8815151:


    "Bueno, no...{w} No soy un hechicero, ni un oráculo, ni tampoco Dios..."


translate spanish epilogue_uv_8b291a5a:


    "Voltaire una vez dijo: «La ilusión es el primero de todos los placeres». Tal vez mi habilidad más exitosa fueran mis ilusiones."


translate spanish epilogue_uv_0eb0dd52:


    "Líneas de pensamientos conectadas unas con otras, preparadas para aguantar hasta la mañana siguiente, pero el dispositivo de seguridad hizo su trabajo nuevamente y desconectó todo el sistema, en vez de formatear mi cerebro como la última vez."


translate spanish epilogue_uv_3c841c5b:


    "Fui despertado por un terrible ruido en mi cabeza.{w} Resultó ser la alarma del reloj."


translate spanish epilogue_uv_5ef84dab:


    "Habiendo hallado su botón con dificultad, la apagué."


translate spanish epilogue_uv_d7e76fb1:


    me "Olga Dmitrievna, son las siete de la mañana, ¿por qué tan pronto?"


translate spanish epilogue_uv_4e6b16ec:


    "Dije en dirección hacia la otra cama, abriendo mis ojos."


translate spanish epilogue_uv_45634255:


    "Pero no había nadie ahí.{w} La líder del campamento ya debió irse a algún lugar."


translate spanish epilogue_uv_a3857818:


    "Me alcé con dificultades y me miré en el espejo.{w} Un pionero común, nada más."


translate spanish epilogue_uv_9ae770c4:


    "Si alguien me mostrara fotografías de mí entre los lugareños, nunca me reconocería a mí mismo."


translate spanish epilogue_uv_3880648b:


    "El sol brillaba iluminando fuera, la naturaleza parecía estar riéndose de mí."


translate spanish epilogue_uv_1580acda:


    "Lentamente caminé hasta los lavabos."


translate spanish epilogue_uv_d341469e:


    "Me encontré con pocos pioneros de camino, los cuales obviamente tenían prisa."


translate spanish epilogue_uv_d056bcc0:


    "Qué raro. ¿Adónde van tan pronto...?"


translate spanish epilogue_uv_7a6be6b8:


    "La fría agua me resfrescó un poco y todos los recuerdos de los últimos días inundaron mi cabeza."


translate spanish epilogue_uv_28ae18f5:


    "Bueno, debería encontrar a Yulya y tratar de ser un poco más educado y formal esta vez."


translate spanish epilogue_uv_ceb6fa03:


    "Después de todo, si no puedes comprender al prójimo cuando lo juzgas en base a tu propia cosmovisión e ideas, ¿entonces cómo puedes conversar con algo que es completamente ajeno?"


translate spanish epilogue_uv_a1eafc73:


    "¿Por qué deberían los alienígenas o las muchachas felinas seguir el mismo código moral y extraer los mismos razonamientos y conclusiones que yo?"


translate spanish epilogue_uv_7a1f73f3:


    "Puede que perciban el universo de una manera completamente diferente... y que los humanos sólo sean hormigas para ellos."


translate spanish epilogue_uv_79206dac:


    "Al igual que nosotros no daríamos importancia en matar a un problemático mosquito, ¿por qué deberían ellos prestarme atención a mí y a mis pequeños problemas?"


translate spanish epilogue_uv_995e66c0:


    "A pesar de que podría ser totalmente lo contrario...{w} Y Yulya puede no ser culpable en esto..."


translate spanish epilogue_uv_a1d45f75:


    "Hubo un crujido en los arbustos cercanos y vi un par de orejas familiares."


translate spanish epilogue_uv_fc936817:


    me "Te veo."


translate spanish epilogue_uv_bd81185d:


    "Yulya abandonó a regañadientes su escondite."


translate spanish epilogue_uv_1e212057:


    uv "Venga ya..."


translate spanish epilogue_uv_94f2abe5:


    "Resopló haciendo un puchero."


translate spanish epilogue_uv_0117c38f:


    me "¿Me estás vigilando?"


translate spanish epilogue_uv_5774bd7a:


    uv "Sí. ¿No está permitido?"


translate spanish epilogue_uv_b3e42019:


    me "Bueno, probablemente esté permitido.{w} Quería hablar contigo igualmente."


translate spanish epilogue_uv_58bb260d:


    uv "¿En serio?{w} Yo también."


translate spanish epilogue_uv_0d6456d9:


    me "¿Sobre qué?"


translate spanish epilogue_uv_d8362ad8:


    "Quise conocer."


translate spanish epilogue_uv_8d36f7b7:


    uv "Algo extraño está sucediendo aquí..."


translate spanish epilogue_uv_df3a694c:


    me "¿Extraño? ¿Qué?"


translate spanish epilogue_uv_c6f62156:


    uv "No lo sé...{w} Es sólo que... todos se comportan de forma distinta de lo habitual. Algo ha ocurrido.{w} Primero tú, luego todos los demás..."


translate spanish epilogue_uv_c17e7890:


    me "¿De qué estás hablando?{w} No te entiendo."


translate spanish epilogue_uv_f1645aa5:


    uv "No lo sé, solamente lo presiento y...{w} cuando presiento algo, ¡siempre ocurre!"


translate spanish epilogue_uv_0b2a7882:


    "Yulya parecía abatida e incluso un poco asustada."


translate spanish epilogue_uv_3202acd0:


    "En este momento, ella no parece en absoluto ningún tipo de criatura superior."


translate spanish epilogue_uv_5756b2ff:


    me "Vale, investiguémoslo juntos."


translate spanish epilogue_uv_311b83b6:


    uv "No, ¡nadie debería verme! Vuelve aquí dentro de una hora, hasta entonces..."


translate spanish epilogue_uv_57b757e5:


    "El sonido de unas pisadas fuertes provenían de la carretera, de alguien corriendo."


translate spanish epilogue_uv_316ba4bb:


    "Me di la vuelta y vi a Slavya aproximándose."


translate spanish epilogue_uv_25cb17a4:


    "Yulya desapareció en el bosque, tal como esperaba."


translate spanish epilogue_uv_a87c71d7:


    me "Hola."


translate spanish epilogue_uv_5154e89b:


    "Sonreí."


translate spanish epilogue_uv_cc470635:


    sl "Ey, hola..."


translate spanish epilogue_uv_9ee3c581:


    "Tenía aspecto de estar muy inquieta, con sus mejillas sonrojadas, sus ojos destellando y sus trenzas desordenadas."


translate spanish epilogue_uv_54314043:


    sl "Semyon, es...{w} ¡Ven conmigo, lo verás con tus propios ojos!"


translate spanish epilogue_uv_1449335c:


    me "¡¿Qué sucedió?!"


translate spanish epilogue_uv_e2810698:


    "El rostro de Slavya tenía tal expresión de súplica que opté por no discutir."


translate spanish epilogue_uv_68329b9d:


    "Ella me agarró de la mano y me arrastró hacia la plaza, todo el campamento estaba probablemente allí."


translate spanish epilogue_uv_70843b37:


    "En cuanto giramos por la última esquina, miré más alla de la multitud de pioneros que estaban quietos en la dirección de Genda y allí estaba..."


translate spanish epilogue_uv_041893cc:


    "En la distancia, donde antes habían campos y bosques, ¡una ciudad comenzó lentamente a surgir!"


translate spanish epilogue_uv_9ecdd8e3:


    "Al principio estaba borrosa, como si fuera hecha de humo, pero luego se volvió más sólida, como si surgiera de una niebla."


translate spanish epilogue_uv_2ae80add:


    "Sky City... inmediatamente me vino a la mente la Ciudad Celestial."


translate spanish epilogue_uv_39cee6c6:


    "Pero, extraño o no, no había nada de especial en ello, tan sólo una ciudad normal más... hay muchas de estas en el territorio de nuestro basto país."


translate spanish epilogue_uv_d6dcd9e8:


    "Podrías incluso decir que demasiado normal, casi aburrida."


translate spanish epilogue_uv_808be7d4:


    "Una ciudad a la que nunca le prestarías atención al circular con un vehículo o al sobrevolarla con una avioneta."


translate spanish epilogue_uv_f1408521:


    "Pero ver semejante cosa aquí y ahora, en el campamento, perdida en los bastos campos y bosques de otra dimensión, donde la única vida que hay son los pioneros, ardillas y mosquitos del lugar..."


translate spanish epilogue_uv_2444ccc9:


    "¡Incluso la muchacha felina no es tan rara comparado con esto!"


translate spanish epilogue_uv_2412f0b1:


    "Slavya seguía apretando mi mano estrechamente."


translate spanish epilogue_uv_22d16f07:


    "Los pioneros susurraban a nuestro alrededor, como si estuvieran asustados de alzar más la voz para hablar, asustados de que dicha ilusión desapareciera, o incluso lo contrario... que se volviera más real, partiendo bajo tierra el pequeño campamento de Sovyonok."


translate spanish epilogue_uv_727cd6f7:


    "Pero la ciudad permaneció en su lugar, como un risco blanco por encima de un valle."


translate spanish epilogue_uv_4a0ef863:


    "No había nada de especial en ello, ni rascacielos gigantes, ni tampoco construcciones arquitectónicas significativas, únicamente casas de diferentes colores y distintos números de pisos, edificios industriales y un hospital o algo así."


translate spanish epilogue_uv_c096e4e5:


    "No podías distinguir a la gente o los vehículos desde esta distancia, pero estaba seguro de que la ciudad tenía su propia vida pululando, independientemente de nuestro campamento."


translate spanish epilogue_uv_26629e57:


    "Me pregunto si alguien se puso en pie en un tejado y nos está mirando por medio de unos prismáticos, tratando de divisar la multitud de pioneros en la plaza..."


translate spanish epilogue_uv_ca6dbdb2:


    "Desde luego, parecía que algo destellaba lejos desde uno de los edificios más altos."


translate spanish epilogue_uv_58def530:


    "Pero espera un minuto...{w} ¡Prismáticos!"


translate spanish epilogue_uv_2215b7ef:


    "De acuerdo a mis estimaciones, debían haber unos dos o tres kilómetros hasta la ciudad, así que si quiero comprender algo ahora mismo, entonces necesito unos prismáticos o un catalejo."


translate spanish epilogue_uv_part2_62ac5531:


    "Corrí hasta Olga Dmitrievna."


translate spanish epilogue_uv_part2_be449a3c:


    me "¿Ves eso también?"


translate spanish epilogue_uv_part2_92063617:


    mt "Sí, por supuesto."


translate spanish epilogue_uv_part2_c155417f:


    "Respondió como si estuviera tensa."


translate spanish epilogue_uv_part2_064c2399:


    me "¡Necesito unos prismáticos! ¡O un catalejo!"


translate spanish epilogue_uv_part2_6bfa8108:


    "La líder del campamento dejó de contemplar la lejana ciudad por un instante y me miró desconcertada."


translate spanish epilogue_uv_part2_238336f0:


    mt "Prismáticos...{w} Sí. Ve al club de cibernética. Debería haber algo."


translate spanish epilogue_uv_part2_e0d4808f:


    "Salí corriendo como un rayo en dirección al edificio de clubs."


translate spanish epilogue_uv_part2_6db36340:


    "Electronik y Shurik estaban ocupados con algo como de costumbre, tenían aspecto de no saber nada del extraño suceso que ocurría en el campamento."


translate spanish epilogue_uv_part2_b752e3f7:


    me "¡Unos primásticos! ¡Rápido!"


translate spanish epilogue_uv_part2_7a052c21:


    "Les grité tratando de no perder mi aliento."


translate spanish epilogue_uv_part2_7d73585c:


    el "¿A qué viene esto?"


translate spanish epilogue_uv_part2_da743cb2:


    me "¿No lo habéis visto?{w} Ahh, al infierno, ¡estáis aquí sentados con vuestros robots como lechuzas!{w} ¡¿Tenéis unos prismáticos o un catalejo?!"


translate spanish epilogue_uv_part2_d081d1f8:


    "Pronuncié cada una de las palabras como si fuera un cavernícola que justo había aprendido a hablar."


translate spanish epilogue_uv_part2_e68ec0c8:


    sh "¿Puedes calmarte y explicarnos de qué va todo esto?"


translate spanish epilogue_uv_part2_94ede6ba:


    me "No hay tiempo para explicaciones, ¡dame los prismáticos y vente rápido hacia la plaza!"


translate spanish epilogue_uv_part2_42e74a51:


    "Los jóvenes cibernéticos no siguieron discutiendo conmigo, y en un minuto estuvimos con los demás pioneros en tanto usaba los prismáticos para examinar cuidadosamente la ciudad la cual apareció Dios sabe donde."


translate spanish epilogue_uv_part2_ea239d21:


    "A primera vista no había nada de especial en ella y me dio la impresión de ser de mi época, de principios del siglo XXI: carteles de publicidad, antenas parabólicas vía satélite, letreros de tiendas pomposas, vehículos {i}modernos{/i}..."


translate spanish epilogue_uv_part2_5a52abce:


    mt "¡Dámelos!"


translate spanish epilogue_uv_part2_4fe722fb:


    "La líder del campamento me arrebató los prismáticos de mis manos."


translate spanish epilogue_uv_part2_11af697f:


    uv "Así que, ¿ése es tú mundo?"


translate spanish epilogue_uv_part2_d815dd97:


    "Me giré y vi a Yulya a mi lado."


translate spanish epilogue_uv_part2_b876afc0:


    me "Dijiste que nadie debería verte..."


translate spanish epilogue_uv_part2_4726bdfc:


    uv "Sí, pero..."


translate spanish epilogue_uv_part2_60f3d758:


    "Se quedó ensimismada."


translate spanish epilogue_uv_part2_15c6c678:


    uv "Nadie me ve ahora mismo."


translate spanish epilogue_uv_part2_6ee3ebcd:


    "Los pioneros no prestaban atención a la extraña muchacha con orejas de gata, demasiado ocupados contemplando la lejana ciudad."


translate spanish epilogue_uv_part2_1e313794:


    me "¿Sabes qué es... y por qué?"


translate spanish epilogue_uv_part2_c34ebf47:


    uv "N-no."


translate spanish epilogue_uv_part2_9697f09d:


    "Contestó tras una breve pausa."


translate spanish epilogue_uv_part2_9ca446d1:


    uv "¡No había sucedido algo así antes! Para nada."


translate spanish epilogue_uv_part2_ee658045:


    me "¿Y qué hace ahí?"


translate spanish epilogue_uv_part2_65321879:


    uv "Bueno. Tú viniste, luego te fuiste, luego volviste de nuevo..."


translate spanish epilogue_uv_part2_7f19b333:


    me "¿Y qué haces todo este tiempo?"


translate spanish epilogue_uv_part2_74ba4a64:


    uv "Te lo dije antes, ¡estaba preparando suministros para el invierno!"


translate spanish epilogue_uv_part2_f8af74e9:


    "No podía comprender nada en absoluto, ¿acaso de verdad no considera Yulya que esta situación sea ni un poquito rara, o simplemente ella está disimulándolo muy bien?"


translate spanish epilogue_uv_part2_65fa2c6e:


    "Sin embargo, ¿quién me pondría en semejante espectáculo sólo para mí?{w} ¡No soy más que un conejito de indias en una jaula!"


translate spanish epilogue_uv_part2_9da8e58a:


    me "No te creo.{w} Deberías saber alguna cosa..."


translate spanish epilogue_uv_part2_a6cdc653:


    mt "Semyon."


translate spanish epilogue_uv_part2_1781e744:


    "Me giré hacia la líder del campamento."


translate spanish epilogue_uv_part2_e8f4b983:


    mt "No tengo ni idea de qué va todo esto, pero parece muy extraño.{w} ¡Deberíamos llamar a la policía!"


translate spanish epilogue_uv_part2_04f49c36:


    "Olga Dmitrievna reaccionó de una manera habitual, como una persona normal haría en dicha situación."


translate spanish epilogue_uv_part2_dd2c1757:


    "Me di la vuelta hacia Yulya para informarla triunfantemente sobre ello, pero la muchacha felina ya había desaparecido."


translate spanish epilogue_uv_part2_13051a08:


    mt "¿Qué puede ser?"


translate spanish epilogue_uv_part2_97006f10:


    "La líder del campamento lo dijo en voz baja, sin dirigirse a nadie."


translate spanish epilogue_uv_part2_a35ec295:


    "No sabía si debería explicarle lo mío, sobre el futuro, acerca de viajar al pasado..."


translate spanish epilogue_uv_part2_04d1fe96:


    "¿Cómo sé si esa ciudad es realmente de mi época?"


translate spanish epilogue_uv_part2_8097992c:


    "¿Podría ser tan sólo otra ilusión y el comportamiento de los pioneros ser tan sólo parte de ello?{w} ¿Y si no lo es...?"


translate spanish epilogue_uv_part2_ff886f0c:


    me "Esto..."


translate spanish epilogue_uv_part2_f58236c0:


    "No supe decir mucho más."


translate spanish epilogue_uv_part2_310e737e:


    us "¡Guay! ¡Guay!"


translate spanish epilogue_uv_part2_b833fa40:


    "Gritaba Ulyana, apareciendo de la nada."


translate spanish epilogue_uv_part2_da11e595:


    us "¡Vayámonos a comprobarlo!"


translate spanish epilogue_uv_part2_6b336dbd:


    dv "Oh, claro. ¡Nos deben estar esperando!"


translate spanish epilogue_uv_part2_e7b79051:


    sl "Esperad un momento. ¡Primero deberíamos pensarlo bien!"


translate spanish epilogue_uv_part2_2ca18f35:


    "Lena se acercó a nosotros, pero no dijo nada."


translate spanish epilogue_uv_part2_35ebcb3b:


    sl "¿Qué crees, Semyon?"


translate spanish epilogue_uv_part2_e522b546:


    "Slavya me preguntó y se quedó mirándome fijamente, como si esperase una respuesta inmediata."


translate spanish epilogue_uv_part2_5de47c68:


    "En realidad, ¿por qué debería importar lo que crea o piense sobre eso?"


translate spanish epilogue_uv_part2_d822ea1f:


    "¿Cuál es la diferencia entre ellos y yo que me haga saber más de todo esto?"


translate spanish epilogue_uv_part2_6cd79bd9:


    "¿Acaso importa que sea nuevo en este mundo? Las cosas que están sucediendo son tan opacas para mí como para cualquier otro lugareño."


translate spanish epilogue_uv_part2_5c1e6f2d:


    me "¡No tengo ni idea! Desde luego, vayámonos a verlo...{w} No parece estar muy lejos."


translate spanish epilogue_uv_part2_13848e7c:


    mt "¡Pero es peligroso!"


translate spanish epilogue_uv_part2_850d94f4:


    un "¿No es también peligroso estar sentado aquí?"


translate spanish epilogue_uv_part2_67db1ed2:


    "Dijo Lena tímidamente. Todos los pioneros y la líder del campamento la miraron desconcertados."


translate spanish epilogue_uv_part2_d17d5e4a:


    un "Bueno, quiero decir...{w} Si esa ciudad ha aparecido tan repentinamente, entonces otra cosa podría ocurrir también, y nosotros..."


translate spanish epilogue_uv_part2_b104c7ae:


    "Parecía estar confundiéndose cada vez más así que dejó de hablar."


translate spanish epilogue_uv_part2_d1a7ad29:


    dv "Bueno, ella tiene razón."


translate spanish epilogue_uv_part2_1086056b:


    "Dijo Alisa con voz perezosa."


translate spanish epilogue_uv_part2_d2fa28f9:


    dv "Ciertamente no comprendo nada de lo que está ocurriendo, ¡pero no me voy a quedar sentada a esperar hasta que algo distinto suceda!{w} Así que, si nadie más va ir..."


translate spanish epilogue_uv_part2_af0e79a1:


    us "¡Y yo, y yo!"


translate spanish epilogue_uv_part2_bc284071:


    "Ulyana la interrumpió."


translate spanish epilogue_uv_part2_399ecda1:


    sl "Bueno, ¿entonces qué? ¿Y si sólo es una ilusión?"


translate spanish epilogue_uv_part2_73dd7efd:


    me "Pues no nos perdemos nada."


translate spanish epilogue_uv_part2_98c7b6d0:


    "Estaba un poco sorprendido por la actitud de urgencia de los lugareños por hallar la naturaleza de este fenómeno, porque con anterioridad habían estado evitando cualquier conversación relacionada con mi aparición en este mundo."


translate spanish epilogue_uv_part2_c28ab8e4:


    "¿Quizás es un misterio dentro de un misterio?{w} Como dos esferas cerradas, estamos dentro del cascarón de una diferente de las dos, donde todo está claro para ellos pero es completamente ajeno para mí, y la ciudad que es tan sólo la intersección de ambas esferas."


translate spanish epilogue_uv_part2_1de398c6:


    "Sin embargo, si es verdad, entonces debería estar cerca de los límites, desconociendo todo sobre una y la otra."


translate spanish epilogue_uv_part2_a1204b82:


    "Olga Dmitrievna estaba indecisa."


translate spanish epilogue_uv_part2_a6810ea8:


    "Todos esperaban a su aprobación o su estricta prohibición."


translate spanish epilogue_uv_part2_24b65eee:


    "No tiene nada de sorprendente: en la vida del campamento, organizado como un reloj suizo, ella era una tirana y déspota, pero cuando los límites de Sovyonok se expandieron y un misterioso oponente apareció más allá de ellos, ella no podía salvaguardar las riendas del poder en sus manos, comenzó a entrar en pánico, se sentía inquieta en el trono en el cual perdía su encanto."


translate spanish epilogue_uv_part2_15a8d7cc:


    "¡Puede que este sea el momento adecuado para empezar a comportarse con confianza!"


translate spanish epilogue_uv_part2_24f6e173:


    me "¡Creo que deberíamos ir y comprobarlo!"


translate spanish epilogue_uv_part2_f47733f4:


    "¡¿Cuánto tiempo más debería estar aquí sentado, tratando de buscar {i}algunas{/i} respuestas?! ¿Dónde las encontraré? ¿En la cantina? ¿En la cabaña de la líder del campamento? ¿Cerca de la estatua de Genda?"


translate spanish epilogue_uv_part2_02724db8:


    "No tengo nada que hacer aquí, ¡especialmente cuando semejante oportunidad se me cruza!"


translate spanish epilogue_uv_part2_ebcff3e9:


    mt "Pero..."


translate spanish epilogue_uv_part2_a5b8262d:


    "Olga Dmitrievna se opuso reservadamente."


translate spanish epilogue_uv_part2_63fc37d3:


    me "¿Y si ya estamos muertos o algo así...?"


translate spanish epilogue_uv_part2_1770827f:


    "Tenía que motivar a los demás de alguna forma, pero escogí no explicarles nada acerca de mí."


translate spanish epilogue_uv_part2_94700582:


    un "¿Cómo que... muertos?"


translate spanish epilogue_uv_part2_664ee673:


    "Una expresión de terror se dibujó en el rostro de Lena."


translate spanish epilogue_uv_part2_4eb2832a:


    me "No estoy hablando literalmente, ¡pero cualquier cosa podría ocurrir! ¡No podemos explicar con certeza qué es lo que está ocurriendo aquí, por lo que cualquier teoría tiene que ser considerada!"


translate spanish epilogue_uv_part2_d9abbf18:


    "Lena comenzó a llorar y Slavya trató de consolarla."


translate spanish epilogue_uv_part2_862ecd27:


    us "¡Es más divertido así!"


translate spanish epilogue_uv_part2_5ce4cf42:


    "Y aquí está Ulyana, para la cual incluso la muerte no es suficiente motivo como para comportarse con calma."


translate spanish epilogue_uv_part2_58c57031:


    dv "¡Deberíamos ir en cualquier caso!"


translate spanish epilogue_uv_part2_d7b7c1f7:


    "Dijo Alisa alegremente."


translate spanish epilogue_uv_part2_1f1e9ea8:


    "Me di la vuelta y vi a Shurik y Electronik."


translate spanish epilogue_uv_part2_ad26e5b2:


    el "Bueno... Es mejor que permanezcamos aquí, tratando de medir, hacer cálculos..."


translate spanish epilogue_uv_part2_855db9ee:


    me "Ya veo."


translate spanish epilogue_uv_part2_8f02b6c0:


    "No esperaba ninguna ayuda de estos dos."


translate spanish epilogue_uv_part2_f785cd61:


    mt "Pues deberías prepararte, prepararte para todo lo que puedas necesitar..."


translate spanish epilogue_uv_part2_acef7f53:


    "Olga Dmitrievna comenzó a atarearse por ahí."


translate spanish epilogue_uv_part2_4c76d45a:


    mt "Linternas, ropas que abriguen, un walkie-talkie. ¿Tenéis un walkie-talkie en el club?"


translate spanish epilogue_uv_part2_69e4a0cc:


    "Se dirigió a los cibernéticos.{w} Asintieron afirmativamente."


translate spanish epilogue_uv_part2_ce617998:


    "..."


translate spanish epilogue_uv_part2_ea5dec1f:


    "En media hora el «Equipo de Investigación» se reunió en la plaza: yo, Alisa, Lena, Slavya y Ulyana."


translate spanish epilogue_uv_part2_811b54d3:


    "Se me dio una mochila bastante pesada, con ropa de invierno empaquetada, linternas, otras cosas necesarias y en mis manos sostenía un walkie-talkie de onda corta."


translate spanish epilogue_uv_part2_d8c59b44:


    "Ciertamente podría apañármelas para caminar esos dos o tres kilómetros portándola en mis hombros, ¿pero por qué no distribuir los objetos entre todos?"


translate spanish epilogue_uv_part2_bc26005b:


    me "Bueno, sabéis que..."


translate spanish epilogue_uv_part2_850ca1bb:


    "Tiré al suelo la mochila y miré a la líder del campamento."


translate spanish epilogue_uv_part2_e33faa0d:


    me "¡Soy el único hombre aquí y me inmovilizáis! ¡Cada uno debería llevar lo suyo!"


translate spanish epilogue_uv_part2_f9473cf8:


    "Las muchachas dudaron."


translate spanish epilogue_uv_part2_556ce63a:


    "La primera en acercarse a la mochila fue Ulyana. Ella cogió una linterna y una chaqueta de invierno, quejándose."


translate spanish epilogue_uv_part2_3cd3be0e:


    "Las demás siguieron su ejemplo."


translate spanish epilogue_uv_part2_492c5c1a:


    "No me esperaba ese descaro por mi parte, y me esperaba incluso menos de ellas de aceptarlo sin rechistar."


translate spanish epilogue_uv_part2_7f3e103d:


    "Incluso los pioneros locales pueden tratar de comportarse razonablemente en situaciones críticas."


translate spanish epilogue_uv_part2_ced536c8:


    "Tras un par de minutos estábamos a las puertas del campamento."


translate spanish epilogue_uv_part2_544fcb0f:


    mt "Espero que comprendáis que no puedo ir con vosotros...{w} Tengo que cuidar de los demás y..."


translate spanish epilogue_uv_part2_3372f455:


    me "Está bien. No tienes por qué explicarte."


translate spanish epilogue_uv_part2_e2da94f6:


    "No contaba con la ayuda de Olga Dmitrievna, ya que a fin de cuentas sus palabras eran ciertas."


translate spanish epilogue_uv_part2_6626bed3:


    mt "¡Buena suerte!{w} ¡Y no olvidéis que podéis poneros en contacto!"


translate spanish epilogue_uv_part2_ce617998_1:


    "..."


translate spanish epilogue_uv_part2_61cd3a24:


    "El sol de verano nos abrasaba sin piedad."


translate spanish epilogue_uv_part2_89211204:


    "El jersey de invienro en la mochila era como una broma de mal gusto. Lo daría junto con las linternas e incluso con el walkie-talkie a cambio del sombrero de panamá y una botella de agua."


translate spanish epilogue_uv_part2_6d59a79c:


    "¿Y por qué a nadie se le ocurrió traer una botella de agua?"


translate spanish epilogue_uv_part2_9dff38be:


    dv "Oh..."


translate spanish epilogue_uv_part2_a8e7c03d:


    "Alisa llevaba cada vez menos y menos ropas puestas encima con cada minuto que transcurría."


translate spanish epilogue_uv_part2_d5817008:


    dv "¡Descansemos!"


translate spanish epilogue_uv_part2_90621c2d:


    "Me detuve lentamente, me puse la palma de mi mano para hacerme sombra en los ojos y miré en dirección hacia la ciudad, la cual estaba justo frente a nosotros."


translate spanish epilogue_uv_part2_3103df8d:


    "No parecía que nos hubiéramos acercado ni un centímetro."


translate spanish epilogue_uv_part2_162c5d13:


    us "¿Por qué no vamos recto hasta ahí?"


translate spanish epilogue_uv_part2_1678d751:


    "Ulyana estaba enojada."


translate spanish epilogue_uv_part2_b1c773b0:


    me "¿Y eso cómo? ¿Recto a través de los bosques y campos?"


translate spanish epilogue_uv_part2_ded4003e:


    us "¡Me da igual! ¡Incluso así sería mejor que tomar rodeos!"


translate spanish epilogue_uv_part2_7adb53b7:


    sl "Sería más rápido si fuéramos por la carretera. ¡Mirad! Hay una curva por {i}ahí{/i}."


translate spanish epilogue_uv_part2_8b245306:


    "Slavya trató de razonar con ella."


translate spanish epilogue_uv_part2_857c395f:


    dv "Haced como queráis..."


translate spanish epilogue_uv_part2_d6c4b72d:


    "Alisa se desplomó en la hierba de la cuneta."


translate spanish epilogue_uv_part2_b413fa09:


    dv "Tomaré un descanso."


translate spanish epilogue_uv_part2_ee90e64d:


    "Nos instalamos a la sombra de un pequeño arbusto cerca del tendido eléctrico."


translate spanish epilogue_uv_part2_37702a41:


    "Cogí los prismáticos y miré hacia la ciudad."


translate spanish epilogue_uv_part2_4f498b93:


    "Los edificios de varios pisos todavía brillaban en la distancia."


translate spanish epilogue_uv_part2_b58174dc:


    "¿Por qué no nos estamos acercando? ¿O acaso es el calor el que está creando una ilusión óptica?"


translate spanish epilogue_uv_part2_6ead5d25:


    "Un arbusto cercano crujió ligeramente. Me giré y vi un par de orejas de gata sobresaliendo de allí."


translate spanish epilogue_uv_part2_f948448a:


    me "Esperad, vuelvo en seguida..."


translate spanish epilogue_uv_part2_af746624:


    "Habiendo caminado por los arbustos, le hice un gesto a Yulya para que me siguiera y habláramos, sólo tras asegurarme de que teníamos suficiente distancia respecto a los demás, de forma que no nos vieran hablar."


translate spanish epilogue_uv_part2_f2554bb0:


    me "¿Así que nos estás vigilando?"


translate spanish epilogue_uv_part2_bb30009f:


    uv "Bueno, así es. ¡También estoy interesada!"


translate spanish epilogue_uv_part2_721821a4:


    me "Ah, ya veo.{w} Y seguramente no sabes qué es lo que nos aguarda allí, ¿verdad?"


translate spanish epilogue_uv_part2_2590e0ab:


    uv "¡Claro que no!"


translate spanish epilogue_uv_part2_06ab0532:


    "Sonrió."


translate spanish epilogue_uv_part2_90fa743d:


    me "¿Y no tienes idea de por qué no nos estamos acercando a la ciudad?"


translate spanish epilogue_uv_part2_cce2dbb5:


    uv "En realidad sí."


translate spanish epilogue_uv_part2_b528e2bc:


    "Yulya contestó con tranquilidad."


translate spanish epilogue_uv_part2_da0567b3:


    me "¿Entonces por qué?"


translate spanish epilogue_uv_part2_9f61aef1:


    uv "Porque no podéis abandonar este lugar."


translate spanish epilogue_uv_part2_aad4d61a:


    "Suspiré profundamente como de costumbre y me apoyé contra el solitario árbol."


translate spanish epilogue_uv_part2_34d58fa0:


    me "¿Quizás es el momento oportuno para explicármelo todo?"


translate spanish epilogue_uv_part2_60f3d758_1:


    "Se quedó ensimismada."


translate spanish epilogue_uv_part2_beaccc1b:


    uv "Antes era distinto.{w} Viniste y luego... luego te fuiste y otra vez viniste. E incluso por mucho que te alejaste, siempre era lo mismo. Y aquella ciudad... es tu mundo..."


translate spanish epilogue_uv_part2_c89b97c8:


    me "¿Por qué siempre te refieres a él como {i}mi{/i} mundo?"


translate spanish epilogue_uv_part2_c01f7fd7:


    uv "¡Porque no forma parte de este lugar!"


translate spanish epilogue_uv_part2_1c70a91b:


    "Yulya estaba exasperada."


translate spanish epilogue_uv_part2_43d1686d:


    me "¿Y qué lugar es este? ¿Puedes ser más clara?"


translate spanish epilogue_uv_part2_056a2259:


    "No estaba enfadado con ella en absoluto... no me parecía que la muchacha felina estuviera ocultando algo importante, sino que ella tan sólo no comprendía mis preguntas o no sabía las respuestas."


translate spanish epilogue_uv_part2_9abf8151:


    uv "No lo sé."


translate spanish epilogue_uv_part2_5a9a2edc:


    me "Muy bien. Hagámoslo paso a paso.{w} ¿Cuánto tiempo llevas estando aquí?"


translate spanish epilogue_uv_part2_af20e0a2:


    uv "Siempre."


translate spanish epilogue_uv_part2_8a11ab08:


    me "«Siempre» es una definición demasiado compleja para un humano.{w} A pesar de que tú probablemente no seas ni humana."


translate spanish epilogue_uv_part2_89141735:


    "Me ensimismé en mis pensamientos."


translate spanish epilogue_uv_part2_2d026c0d:


    me "¿Y qué estoy haciendo aquí?"


translate spanish epilogue_uv_part2_48657b0c:


    uv "Tú también has estado aquí siempre."


translate spanish epilogue_uv_part2_03adb46e:


    me "¿Cómo es eso posible? ¿Acaso me he duplicado? Porque no recuerdo nada de eso."


translate spanish epilogue_uv_part2_79ba8a6b:


    uv "Bueno. Es como si fueras tú y tampoco lo fueras. Hay muchos de ti."


translate spanish epilogue_uv_part2_ddf16d12:


    me "¿Y tú eres la única?"


translate spanish epilogue_uv_part2_819d8828:


    uv "Soy la única."


translate spanish epilogue_uv_part2_41c98a55:


    "Yulya sonrió afablemente y agitó su cola."


translate spanish epilogue_uv_part2_5de7b061:


    me "¿Una para todos mis {i}yo paralelos{/i}?"


translate spanish epilogue_uv_part2_04cf541e:


    uv "Algo así."


translate spanish epilogue_uv_part2_5d95f80c:


    me "Entonces, eres como... ¿omnipotente?"


translate spanish epilogue_uv_part2_0ce08539:


    "No encontré mejor palabra para decirlo."


translate spanish epilogue_uv_part2_6ccdf245:


    uv "No lo sé."


translate spanish epilogue_uv_part2_f55362eb:


    me "¿Tienes alguna habilidad sobrenatural? ¿Puedes volar, lanzar bolas de fuego o teletransportarte?"


translate spanish epilogue_uv_part2_e4b3a5ba:


    uv "Puedo hacer suministros para el invierno."


translate spanish epilogue_uv_part2_3d4f4933:


    "Respondió con un tono serio."


translate spanish epilogue_uv_part2_4e34062e:


    me "Supongamos que eso es un «no».{w} ¿Y cuál es el propósito de... todo esto?"


translate spanish epilogue_uv_part2_6ccdf245_1:


    uv "No lo sé."


translate spanish epilogue_uv_part2_8eabed28:


    "Si no fuera porque soy más bien tranquilo, tras todos estos «No lo sé» hace rato que habría perdido la paciencia."


translate spanish epilogue_uv_part2_997bc03d:


    me "Sabes..."


translate spanish epilogue_uv_part2_2a454c26:


    sl "¡Semyon!"


translate spanish epilogue_uv_part2_5f087d0d:


    "Escuché a Slavya llamándome."


translate spanish epilogue_uv_part2_2e9e672c:


    "Me giré y cuando volví a mirar en el lugar en el que se encontraba Yulya, ya había desaparecido."


translate spanish epilogue_uv_part2_d9f91079:


    me "Ahí va otra vez..."


translate spanish epilogue_uv_part2_4a436c24:


    "Rechiné mis dientes y fui de vuelta con las muchachas."


translate spanish epilogue_uv_part2_ce617998_2:


    "..."


translate spanish epilogue_uv_part2_a204f5f2:


    "Habíamos estado caminando por varias horas, pero la ciudad seguía permaneciendo en el mismo lugar."


translate spanish epilogue_uv_part2_a9df582b:


    "No estábamos acercándonos a la ciudad ni un solo metro, ni tan siquiera se alejaba de nosotros."


translate spanish epilogue_uv_part2_ca6e81ce:


    us "¡Ya os dije que deberíamos ir en línea recta!"


translate spanish epilogue_uv_part2_d2785117:


    dv "¡Cállate!{w} No lo entiendo..."


translate spanish epilogue_uv_part2_540edccf:


    un "¿Podría ser en verdad una ilusión?"


translate spanish epilogue_uv_part2_36d8976c:


    "Dijo Lena, quien no pronunció ni una sola palabra."


translate spanish epilogue_uv_part2_b103b945:


    me "No, incluso una ilusión en un desierto desaparece si te acercas lo bastante a ella.{w} Probablemente..."


translate spanish epilogue_uv_part2_d83880ec:


    dv "¿Y ahora qué?"


translate spanish epilogue_uv_part2_a5fdb009:


    "Dijo Alisa, enfadada."


translate spanish epilogue_uv_part2_5a93ad2a:


    dv "Estoy harta y cansada de esto. ¡Volvamos!"


translate spanish epilogue_uv_part2_17dea409:


    sl "Ve, si quieres. ¡Nadie te lo impide!"


translate spanish epilogue_uv_part2_a4fde9ae:


    "Slavya le gruñió."


translate spanish epilogue_uv_part2_7e935856:


    "Todos le miraron sorprendidos. Era realmente raro escuchar tales palabras de una pionera ejemplar."


translate spanish epilogue_uv_part2_1413dbd3:


    sl "¿Por qué siempre estás tan insatifecha con todo? Si no te gusta... ¡mueve tu culo de vuelta!"


translate spanish epilogue_uv_part2_f392c23f:


    dv "¿Acaso te he preguntado...?"


translate spanish epilogue_uv_part2_b75cedb9:


    "Dijo Alisa insegura."


translate spanish epilogue_uv_part2_ad0de36d:


    un "Bueno, ¡ella tiene razón!"


translate spanish epilogue_uv_part2_a9448a42:


    "Lena intervino en la conversación."


translate spanish epilogue_uv_part2_0a78c8a6:


    un "¡Estoy harta de tus lloriqueos!"


translate spanish epilogue_uv_part2_610dac36:


    "Ulyana se asustó y se escondió detrás de mí."


translate spanish epilogue_uv_part2_3f49c33b:


    dv "¡Oh, tú sólo estás aquí para aparentar estar mejor frente a alguien a costa de otra! ¡Claro que sí! ¡Es más fácil conmigo estando en segundo plano!"


translate spanish epilogue_uv_part2_f9318593:


    un "Anda creéme... ¡no serás capaz de aguantarte en pie tras lo que te haré ahora!"


translate spanish epilogue_uv_part2_75eeb62b:


    "Lena se lanzó contra Alisa, pero Slavya se interpuso entre ambas."


translate spanish epilogue_uv_part2_cc189774:


    sl "Demonios, ¡no, primero dejadme acabar!"


translate spanish epilogue_uv_part2_d1a7ebe0:


    us "¡Deteneos!"


translate spanish epilogue_uv_part2_1689ea1f:


    "Ulyana chilló entre lágrimas."


translate spanish epilogue_uv_part2_dfa2690e:


    "No tenía ni idea de qué hacer en tal situación, pero no podía dejarlas que se pelearan."


translate spanish epilogue_uv_part2_71564beb:


    "Sin embargo, no tuve que intervenir, Yulya saltó de la nada y se interpuso entre las muchachas, siseando encolerizada."


translate spanish epilogue_uv_part2_12b53b2f:


    uv "¡No debería ser así! ¡Esto no está bien! ¡No debéis comportaros así! ¡No es vuestro propósito!"


translate spanish epilogue_uv_part2_24b87964:


    "Las muchachas dieron un respingo atrás. Alisa chilló, Slavya adoptó un tipo de postura de combate y Lena estaba sosteniendo un cuchillo que sacó de Dios sabe donde."


translate spanish epilogue_uv_part2_405c714f:


    sl "¿Quién eres tú?"


translate spanish epilogue_uv_part2_cd104290:


    un "¿Quién es ésa?"


translate spanish epilogue_uv_part2_852891ee:


    dv "¿Qué es eso?"


translate spanish epilogue_uv_part2_cdd96158:


    "No sabría decir si se dirigía a mí o a Yulya."


translate spanish epilogue_uv_part2_457e2c5c:


    me "Espera..."


translate spanish epilogue_uv_part2_6f6f163b:


    "Me quedé entre ellas y la muchacha felina."


translate spanish epilogue_uv_part2_0b28dad0:


    me "Puedo explicarlo todo."


translate spanish epilogue_uv_part2_893718f4:


    "Es como si me inventara excusas...{w} ¿Para quién? ¿Y para qué? ¡Ella debería explicarlo todo!"


translate spanish epilogue_uv_part2_37d7b496:


    "Las muchachas se relajaron un poco."


translate spanish epilogue_uv_part2_314afab1:


    me "Bueno... ¿Habéis escuchado alguna vez la leyenda de la muchacha felina? Pues es esta..."


translate spanish epilogue_uv_part2_6905fe00:


    "Yulya seguía siseando, pero con voz más baja."


translate spanish epilogue_uv_part2_1b195105:


    sl "Lo sabe."


translate spanish epilogue_uv_part2_2ff596a2:


    "Dijo Slavya fríamente."


translate spanish epilogue_uv_part2_3183db72:


    sl "Ella sabe qué es la ciudad. ¡Ella lo sabe todo!"


translate spanish epilogue_uv_part2_f4f28a92:


    me "Ella no sabe nada."


translate spanish epilogue_uv_part2_6ccdf245_2:


    uv "No sé nada."


translate spanish epilogue_uv_part2_b1ca2485:


    "Confirmó Yulya."


translate spanish epilogue_uv_part2_6c17c857:


    dv "¿Le has preguntado apropiadamente?"


translate spanish epilogue_uv_part2_2b82084e:


    "Alisa se puso en pie y crujió sus nudillos."


translate spanish epilogue_uv_part2_293737cc:


    me "¡¿Qué demonios estás haciendo?! ¡Para ya! ¡Primero el autobús, el campamento, luego tú, luego ella y ahora esa maldita ciudad!"


translate spanish epilogue_uv_part2_f8b31659:


    "Comencé a gritar, apretando los dientes, probablemente con chispas saltando de mis ojos."


translate spanish epilogue_uv_part2_b458ed0a:


    me "¡Basta ya! ¡Ya tuve bastante con la maldita incerteza! ¡O explicas lo que está ocurriendo aquí, o no me moveré ni un solo centímetro más! ¡Mátame si quieres, me trae sin cuidado!"


translate spanish epilogue_uv_part2_86355b31:


    me "¿Crees que puedo tolerarlo todo? ¿Que haré ver que nada está sucediendo y que no comprendo nada, independientemente de lo que hagas? ¡No me importa! Todo tiene un límite..."


translate spanish epilogue_uv_part2_0ca24177:


    "Terminé mi sermón más calmado, habiendo perdido mi aliento de gritar."


translate spanish epilogue_uv_part2_92f97560:


    "La sangre palpitaba en mis sienes, el mundo se volvió más oscuro por un instante y entonces, luego, se pintó por sí mismo de colores inimaginables."


translate spanish epilogue_uv_part2_0bf17dcb:


    "Las muchachas me miraban con sorpresa e incluso con miedo."


translate spanish epilogue_uv_part2_a5c34365:


    "Yulya dejó de sisear y de nuevo volvió a tener aspecto de ser indiferente a todo lo que le rodeaba."


translate spanish epilogue_uv_part2_505d91ae:


    me "Muy bien..."


translate spanish epilogue_uv_part2_f9b7b6f4:


    "Declaré y empecé a caminar a través de la carretera en dirección a la ciudad, la cual se movía un paso más lejos por cada paso que daba."


translate spanish epilogue_uv_part2_58aebb5d:


    "Nadie me siguió..."


translate spanish epilogue_uv_part2_ce617998_3:


    "..."


translate spanish epilogue_uv_part2_9db18933:


    "El sol comenzó a ponerse más allá del horizonte y la ciudad, todavía visible en el este, se hacía cada vez más y más pequeña absorbida por la niebla."


translate spanish epilogue_uv_part2_97bdb375:


    "No sabría decir dónde se hallaba realmente ya que los campos estaban interrumpidos por bosques, pero sus edificios todavía se alzaban por encima de ellos como un elemento central de un diorama."


translate spanish epilogue_uv_part2_9811a37e:


    "La ciudad permanecía muda, como si sobrevolara el mundo por encima, mirando abajo con un reproche silencioso a mis triviales intentos de alcanzarla."


translate spanish epilogue_uv_part2_899035c2:


    "De acuerdo a mis cálculos, había caminado más de dos o tres kilómetros, lo cual era la distancia teórica hasta la ciudad. Me detuve a descansar en la sombra un par de veces, el frío del atardecer se volvió mi salvador."


translate spanish epilogue_uv_part2_3c799697:


    "Sin embargo, podría no haber ninguna ciudad y estoy simplemente correteando como un hámster en una rueda, sintiendo el cansancio de la distancia recorrida, pero con mi destino en alguna parte más allá de la jaula..."


translate spanish epilogue_uv_part2_a6ba1110:


    "Recordé los pensamientos de ayer sobre la llave colgando justo bajo el techo."


translate spanish epilogue_uv_part2_1fd60c00:


    "¡Me serían verdaderamente útiles ahora! Tal vez no haya respuestas para nada. Estoy demasiado cansado como para pensar en ello...{w} Además de romper encolerizado con las muchachas."


translate spanish epilogue_uv_part2_e14ad1c1:


    "Ah, ¡déjalo!"


translate spanish epilogue_uv_part2_86264158:


    "Me senté al lado de la carretera, levantando una pequeña tormenta de polvo en el suelo, la cual me cubrió y me hizo toser."


translate spanish epilogue_uv_part2_ab8b4683:


    "Las lágrimas llenaron mis ojos."


translate spanish epilogue_uv_part2_d0c5e298:


    "Cuando los abrí de nuevo, Yulya estaba frente a mí."


translate spanish epilogue_uv_part2_71403012:


    me "Parece que ahí tienes que tener ventajas siendo una gata, puedes moverte más rápidamente que los humanos."


translate spanish epilogue_uv_part2_aeea0184:


    uv "¿A dónde vas?"


translate spanish epilogue_uv_part2_663c6ac0:


    "Me preguntó, sin prestar atención a mis palabras."


translate spanish epilogue_uv_part2_b0e31945:


    me "¡Estoy yendo justo ahí!"


translate spanish epilogue_uv_part2_90c9d82c:


    "Moví una mano en la dirección de la ciudad."


translate spanish epilogue_uv_part2_37fe8f5c:


    uv "¿Pero por qué?"


translate spanish epilogue_uv_part2_0ee3d648:


    me "¿Dónde están los demás?"


translate spanish epilogue_uv_part2_fa0bf095:


    uv "Se volvieron al campamento."


translate spanish epilogue_uv_part2_855db9ee_1:


    me "Ya veo."


translate spanish epilogue_uv_part2_740dbe23:


    uv "Y bien, ¿por qué estás yendo ahí?"


translate spanish epilogue_uv_part2_b51017bc:


    me "Porque..."


translate spanish epilogue_uv_part2_89141735_1:


    "Me quedé ensimismado."


translate spanish epilogue_uv_part2_369bcbed:


    me "¿Qué hay que hacer si no? ¿Qué sucederá si solamente me siento aquí?"


translate spanish epilogue_uv_part2_d06bfe4a:


    uv "Nada."


translate spanish epilogue_uv_part2_73cd06af:


    me "Eso es lo que quiero decir."


translate spanish epilogue_uv_part2_6af42011:


    "Yulya me miró atentamente y se sentó a mi lado."


translate spanish epilogue_uv_part2_479c6fcf:


    uv "Me gustaría ayudarte..."


translate spanish epilogue_uv_part2_520e4d28:


    "Dijo de repente en voz baja."


translate spanish epilogue_uv_part2_b020cad8:


    me "¡Pues ayuda!"


translate spanish epilogue_uv_part2_80f6bbf6:


    uv "No sé cómo..."


translate spanish epilogue_uv_part2_583e4d51:


    me "¡Cuéntame algo sobre la ciudad, sobre este mundo! ¿Por qué estoy aquí? ¿Cómo vuelvo?"


translate spanish epilogue_uv_part2_bfefff38:


    uv "No lo sé...{w} Bueno, quiero decir...{w} Siempre es así: llegas al campamento con un autobús, transcurres una semana aquí y, luego, independientemente del resultado, todo vuelve a repetirse. Y... y... hay muchos de ti, todos ellos son diferentes."


translate spanish epilogue_uv_part2_1eebdf41:


    me "¿Qué quieres decir con {i}diferentes{/i}?"


translate spanish epilogue_uv_part2_99fb7c6c:


    uv "¡Que no son como tú! Tú eres el único que es así, ese es el motivo por el que puedo hablarte y los demás no pueden entenderme..."


translate spanish epilogue_uv_part2_0180ce19:


    me "¿Qué no te entienden?"


translate spanish epilogue_uv_part2_37b06b96:


    uv "Desaparecen a veces."


translate spanish epilogue_uv_part2_be1ebd85:


    "Yulya seguía hablando, ignorando mi pregunta."


translate spanish epilogue_uv_part2_ad623b81:


    uv "Así es como deberían ser las cosas. No tengo ni idea de por qué, pero deberían. No retornan aquí tras los siete días, sino que otros nuevos llegan que son como los anteriores. Cada uno de ellos es igual al principio, casi igual."


translate spanish epilogue_uv_part2_a74fdca1:


    uv "Las diferencias surgen tras un tiempo. Especialmente cuando comprenden que todo debería repetirse una y otra vez."


translate spanish epilogue_uv_part2_43def3e2:


    me "¿Significa eso que estoy destinado a caminar en círculos también?"


translate spanish epilogue_uv_part2_0e00b743:


    uv "No lo sé. Esta vez es diferente."


translate spanish epilogue_uv_part2_32193853:


    me "¿Por qué siempre ocurre lo mismo? ¿Quiénes son esos {i}otros{/i}?"


translate spanish epilogue_uv_part2_4de002b5:


    "Me miró desconcertada."


translate spanish epilogue_uv_part2_a5fc162e:


    uv "Pero son tú..."


translate spanish epilogue_uv_part2_67914ffa:


    me "¿Qué quieres decir con {i}yo{/i}?"


translate spanish epilogue_uv_part2_660e4ce1:


    uv "Tú y todos los demás..."


translate spanish epilogue_uv_part2_8f96fdab:


    "Ella se quejó, tratando de encontrar las palabras adecuadas."


translate spanish epilogue_uv_part2_cf6949e5:


    uv "¡Todos vosotros sois iguales!"


translate spanish epilogue_uv_part2_781b326f:


    me "¿Entonces son clones míos o algo así?"


translate spanish epilogue_uv_part2_dc13f3f4:


    uv "¿Qué son «clones»?"


translate spanish epilogue_uv_part2_b2232755:


    me "Copias.{w} ¡Como dos de tus setas iguales que preparas para el invierno!"


translate spanish epilogue_uv_part2_2b8c7e9e:


    uv "No hay dos setas idénticas."


translate spanish epilogue_uv_part2_2cde1e69:


    "Dijo Yulya con seriedad."


translate spanish epilogue_uv_part2_262a8646:


    me "¡Imagínate que las hay!"


translate spanish epilogue_uv_part2_2bb025d9:


    uv "Setas idénticas...{w} Quizá... No lo sé."


translate spanish epilogue_uv_part2_5d277179:


    me "Dios mío, esto es una pesadilla..."


translate spanish epilogue_uv_part2_84ad17c2:


    "Agaché mi cabeza entre mis rodillas y cerré los ojos."


translate spanish epilogue_uv_part2_19e9263c:


    "Estábamos sentados en silencio desde hacía varios minutos."


translate spanish epilogue_uv_part2_893bd690:


    "Realmente quería comprender qué pasaba por la cabeza de Yulya."


translate spanish epilogue_uv_part2_f9abbf96:


    "Después de todo, incluso si ella de verdad no sabía nada, pues por lo menos debería tener algunas ideas sobre lo que estaba sucediendo."


translate spanish epilogue_uv_part2_8f211b5f:


    me "Vale. ¿Qué deberíamos hacer?"


translate spanish epilogue_uv_part2_d1215d87:


    "Le pregunté con voz seria."


translate spanish epilogue_uv_part2_4de002b5_1:


    "Ella me miró con aspecto desconcertado otra vez."


translate spanish epilogue_uv_part2_cad610a0:


    me "¿Qué me aconsejarías que hiciera?"


translate spanish epilogue_uv_part2_9abf8151_1:


    uv "No lo sé."


translate spanish epilogue_uv_part2_48f8fcee:


    me "¿Qué harías en mi lugar?"


translate spanish epilogue_uv_part2_4b09c0b5:


    uv "¿Yo...?{w} ¿En tú lugar...?"


translate spanish epilogue_uv_part2_8495dc4f:


    uv "¡Pero eso nunca ocurrirá!"


translate spanish epilogue_uv_part2_05864633:


    "Yulya se rio a carcajadas."


translate spanish epilogue_uv_part2_f798a921:


    uv "¡Tú eres tú, y yo soy yo!"


translate spanish epilogue_uv_part2_615ead6d:


    me "¡Sólo imagínatelo! Tienes cabeza así que... ¡deberías poder imaginártelo!"


translate spanish epilogue_uv_part2_1fbe04a5:


    uv "Bueno...{w} No cambiaría nada."


translate spanish epilogue_uv_part2_5ac38cf1:


    me "¿Por qué no?"


translate spanish epilogue_uv_part2_3f04a191:


    uv "Porque es como tienen que ser las cosas, es mejor y así es menos problemático."


translate spanish epilogue_uv_part2_c080aa07:


    me "Bien pues, ¿es menos problemático quedarse en este campamento sentado a esperar las eternas repeticiones?"


translate spanish epilogue_uv_part2_0e8e1d0f:


    uv "Sí."


translate spanish epilogue_uv_part2_8face0e3:


    "No podía estar de acuerdo con eso. Simplemente no podía."


translate spanish epilogue_uv_part2_ff9f8675:


    "En tanto algo esté al alcance de mi poder para cambiarlo, o incluso sólo para intentarlo, ¡tengo que actuar!"


translate spanish epilogue_uv_part2_e41273e8:


    "Me alcé del suelo y marché hacia la distante ciudad, visible en los rayos carmesíes de un lejano crespúsculo."


translate spanish epilogue_uv_part2_ce617998_4:


    "..."


translate spanish epilogue_uv_part2_a10a51f7:


    "La carretera tomó una repentina curva al final. Los edificios, los cuales se alzaban antes por encima de los árboles, desaparecieron de mi vista y el paisaje rural frente a mí fue reemplazado por periferia industrial."


translate spanish epilogue_uv_part2_f021e5ae:


    "Ya era oscuro. Estrellas brillantes diseminadas llenaban el cielo, todavía visibles desde donde estaba pero desapareciendo por encima de la ciudad, la cual estaba repleta de luces lejanas de ventanas y farolas."


translate spanish epilogue_uv_part2_47d682d4:


    "Detrás de mí reinaba una silenciosa y oscura noche, interrumpida solamente por el gorjeo de un durmiente pájaro nocturno, el crujido de la hierba y el monótono zumbido de los insectos. Mientras que frente a mí había un hormiguero humano que no podía callarse ni un solo minuto, parpadeando felizmente con miles de luces y con gritos de millones de voces."


translate spanish epilogue_uv_part2_29ef898b:


    "La carretera que caminaba parecía extraña. Estaba vacía, como desierta, sin usar por vehículos o personas."


translate spanish epilogue_uv_part2_c4ec3c82:


    "Como un sendero estrecho, conduciendo lejos de {i}este{/i} mundo a otro."


translate spanish epilogue_uv_part2_6b67a347:


    "Yulya caminó callada a mi lado todo este rato."


translate spanish epilogue_uv_part2_62569e6a:


    me "¿Por qué estás aquí?"


translate spanish epilogue_uv_part2_420d89ea:


    "Le pregunté en voz baja."


translate spanish epilogue_uv_part2_686e7c65:


    uv "Tengo que estar contigo."


translate spanish epilogue_uv_part2_5ac38cf1_1:


    me "¿Por qué?"


translate spanish epilogue_uv_part2_29bfa65a:


    uv "Es extraño. La ciudad siempre estuvo en su lugar y ahora está frente a nosotros."


translate spanish epilogue_uv_part2_6ab05308:


    "Ella ignoró mi pregunta."


translate spanish epilogue_uv_part2_d2669c69:


    me "No más raro que todo lo demás.{w} ¡He caminado como diez o veinte kilómetros y aun así sigue estando lejos!"


translate spanish epilogue_uv_part2_cf738eeb:


    uv "¿Lejos?"


translate spanish epilogue_uv_part2_44fcb674:


    me "¡Puede que nunca la alcancemos!"


translate spanish epilogue_uv_part2_3b18e37c:


    "Me detuve y traté de imaginarme qué me aguardaba ahí."


translate spanish epilogue_uv_part2_fe135b74:


    "¿Es esta ciudad similar a la mía? ¿Qué tipo de arquitectura es esta? ¿Quién vive ahí...?"


translate spanish epilogue_uv_part2_55dee337:


    "¿Y por qué he llegado a la conclusión de que hay gente ahí? ¿Acaso no es sólo otra ilusión, una trampa más de este enfermo mundo?"


translate spanish epilogue_uv_part2_959e8bb8:


    "¡Incluso Yulya dijo que ella ve por primera vez todo esto!"


translate spanish epilogue_uv_part2_60491530:


    "Repentinamente, el walkie-talkie que todavía tenía encima hizo un ruido desagradable y la voz agitada de Olga Dmitrievna dijo:"


translate spanish epilogue_uv_part2_7e5a4c32:


    mt "Semyon, ¿me copias? ¡Semyon!"


translate spanish epilogue_uv_part2_e81bfa10:


    "Tras un instante, apreté el botón y contesté."


translate spanish epilogue_uv_part2_fc30fd2b:


    me "Sí."


translate spanish epilogue_uv_part2_027e452f:


    mt "¡Semyon, están pasando muchas cosas! ¡Vuelve enseguida!"


translate spanish epilogue_uv_part2_d64c8037:


    me "¿Qué? ¿Qué es? ¿Qué ocurre?"


translate spanish epilogue_uv_part2_39ad62fd:


    "Pregunté sin mucho interés."


translate spanish epilogue_uv_part2_3e4d60db:


    "La líder del campamento, los pioneros, todo el campamento había sido dejado muy atrás, no solamente unos doce kilómetros, sino mucho más."


translate spanish epilogue_uv_part2_ce29470a:


    "Todo lo sucedido durante los últimos días perdió cualquier significado, como una pesadilla que empiezas a olvidar."


translate spanish epilogue_uv_part2_095e79da:


    "Ahora, cuando haya un mundo de verdad frente a mí...{w} No tengo otra opción salvo creer que cuando alcance esa ciudad, todo terminará."


translate spanish epilogue_uv_part2_9f2bee1d:


    mt "¡Semyon, date prisa! ¡No hay tiempo para explicártelo! Es un desastre..."


translate spanish epilogue_uv_part2_63619bef:


    "La estática se hizo más grande, ahogando la voz de la líder del campamento."


translate spanish epilogue_uv_part2_e46e5542:


    uv "¿Irás?"


translate spanish epilogue_uv_part2_dbcf576f:


    "Preguntó Yulya nerviosa."


translate spanish epilogue_uv_part2_b6d2279b:


    me "¿Dónde? ¿Volver allí? No, ¿por qué lo haría?"


translate spanish epilogue_uv_part2_f27606ef:


    uv "Pero..."


translate spanish epilogue_uv_part2_44fa2fc7:


    me "¿Es que quieres que vuelva?"


translate spanish epilogue_uv_part2_1b462d8f:


    uv "No lo sé. ¿Qué quieres tú?"


translate spanish epilogue_uv_part2_0e3de8d0:


    "En serio. ¿Qué es lo que quiero?"


translate spanish epilogue_uv_part2_4efd7251:


    "Llegar a esa ciudad... ¿no es lo que estuve haciendo todo el día? O algo mucho más relevante... ¿escaparme de este mundo?"


translate spanish epilogue_uv_part2_79ceb492:


    "¿Y si en realidad algo malo ha sucedido allí?"


translate spanish epilogue_uv_part2_75a77f12:


    "Ese pensamiento me dio una sensación de dolor en mi cabeza."


translate spanish epilogue_uv_part2_cf213262:


    "Alguna cosa les ha sucedido a Slavya, a Lena, a Ulyana, incluso a Alisa..."


translate spanish epilogue_uv_part2_6f66ce9d:


    "¿Y por qué la cuestión de «¿Por qué me iba importar?» no me surgía ahora?"


translate spanish epilogue_uv_part2_5163e5b3:


    "Pero... sí. ¡Claro que me preocupaba!"


translate spanish epilogue_uv_part2_48ba4ca4:


    "He tenido mucha relación con ellas recientemente, ¡realmente casi me acostumbré a esta vida del campamento!"


translate spanish epilogue_uv_part2_3aad34f9:


    "¿Pero que le ocurrirá a la ciudad? ¿Desaparecerá tan repentinamente como apareció?"


translate spanish epilogue_uv_part2_39c34ac9:


    "¿Y si todo va como Yulya lo describió...?{w} No, no debería permitirlo."


translate spanish epilogue_uv_part2_f53442e6:


    "El walkie-talkie siseó, pero no quería apagarlo, por si acaso escuchaba algo otra vez."


translate spanish epilogue_uv_part2_37a46732:


    uv "Y bien, ¿te has decidido?"


translate spanish epilogue_uv_part2_44b18e64:


    "Tenía que escoger."


translate spanish epilogue_uv_city_959e0edc:


    "¡Maldito sea Sovyonok!{w} ¡No hay campamento, ni pioneros!{w} Y realmente no importa por qué llegué aquí. Mi prioridad es escapar, ¡rápido!"


translate spanish epilogue_uv_city_54348e42:


    me "Seguiré adelante."


translate spanish epilogue_uv_city_588dbb53:


    "Me encogí de hombros, tratando de expresar el mínimo interés."


translate spanish epilogue_uv_city_78170aac:


    "Yulya se me quedó mirando fijamente por un rato, pero luego dio un par de pasos adelante, como invitándome a seguirla."


translate spanish epilogue_uv_city_2c36965d:


    uv "Bueno, ¿vas a ir?"


translate spanish epilogue_uv_city_ce617998:


    "..."


translate spanish epilogue_uv_city_7b1bb487:


    "Parecía que por primera vez en todo el día nos estábamos acercando un poco a la ciudad, como unos cientos de metros o así."


translate spanish epilogue_uv_city_fcfa0bc9:


    "Estaba casi seguro de que la alcanzaría, entonces escuché el sonido de una bocina de vehículo detrás de mí..."


translate spanish epilogue_uv_city_16335221:


    "Un autobús, un Icarus, se nos aproximó rápidamente, atravesando la oscura noche con sus luces."


translate spanish epilogue_uv_city_c896c1bf:


    "Me aparté a la cuneta."


translate spanish epilogue_uv_city_fd94b862:


    me "Eso es..."


translate spanish epilogue_uv_city_f05fcf4f:


    "Yulya no estaba cerca."


translate spanish epilogue_uv_city_84bca6ed:


    "¿Cómo se las apaña para aparecer y desaparecer tan imperceptiblemente?"


translate spanish epilogue_uv_city_421162ac:


    "El autobús se detuvo lentamente a mi lado, la puerta se abrió y..."


translate spanish epilogue_uv_city_8e42683f:


    "¡...Olga Dmitrievna salió fuera!"


translate spanish epilogue_uv_city_58184ba9:


    mt "¡Semyon! ¿Por qué no viniste? ¡Te lo pedí!"


translate spanish epilogue_uv_city_d0bc54d8:


    me "¡Yo te podía pedir lo mismo! Me dijiste que un desastre había sucedido y ahora estás aquí..."


translate spanish epilogue_uv_city_5cb759d8:


    mt "Vale..."


translate spanish epilogue_uv_city_89ab1420:


    "De repente se suavizó."


translate spanish epilogue_uv_city_49846fd4:


    mt "Sube al autobús."


translate spanish epilogue_uv_city_ff029072:


    me "¿Qué...? ¿Y qué hay de la ciudad?"


translate spanish epilogue_uv_city_a770878a:


    mt "Y allí es dónde estamos yendo... ¡para verlo por nosotros mismos!"


translate spanish epilogue_uv_city_fb7ddfe9:


    "Dudé."


translate spanish epilogue_uv_city_135f17b8:


    "Por un lado, iba a ser más rápido ir en autobús, esta carretera debería conducir hasta la ciudad. Por otra parte, las acciones de la líder del campamento parecían raras."


translate spanish epilogue_uv_city_cb631dc4:


    "Estamos yendo en la misma dirección por ahora de todos modos..."


translate spanish epilogue_uv_city_71d3b8c8:


    me "De acuerdo."


translate spanish epilogue_uv_city_164e884d:


    "Acepté y me subí al autobús."


translate spanish epilogue_uv_city_ce617998_1:


    "..."


translate spanish epilogue_uv_city_cf9891a5:


    "El Icarus iba en la dirección de la misteriosa ciudad, rebotando entre los baches."


translate spanish epilogue_uv_city_12d134cd:


    "¡Era demasiado lento para mi gusto! Estaba tan cansado que mis ojos empezaron a cerrarse."


translate spanish epilogue_uv_city_c8562488:


    "¿Y qué pasa con Yulya?{w} ¿Sencillamente la he dejado sola atrás?"


translate spanish epilogue_uv_city_9b99b80c:


    "¡Parecía que todos los pioneros que habían estado en Sovyonok se habían subido a este autobús!"


translate spanish epilogue_uv_city_3d477727:


    "Tal vez ella quería decirme algo..."


translate spanish epilogue_uv_city_22a67c2b:


    "Sin embargo, a pesar de mi lucha contra Morfeo, mi fuerte ronquido empezó a sonar en un par de minutos desde el asiento de atrás."


translate spanish epilogue_uv_city_742d6e3a:


    "¡Ahora todo se acabó!{w} ¿O no?"


translate spanish epilogue_uv_city_ef1df980:


    "Quería escapar desesperadamente y no rendirme al sueño."


translate spanish epilogue_uv_city_77be2dc1:


    "¿Es el agotamiento la causa?"


translate spanish epilogue_uv_city_3eb6b561:


    "Probablemente no. Es como si hubiera tomado una dosis de sedante.{w} ¡Las personas no pueden perder la conciencia de esta forma y nunca padecer de narcolepsia!"


translate spanish epilogue_uv_city_eeb3a5ac:


    "Mis pensamientos fluyeron rápida y calmadamente, como un ancho río."


translate spanish epilogue_uv_city_1aaadbd7:


    "Nada interfería en su vuelo, porque estaba durmiendo..."


translate spanish epilogue_uv_city_0a064f5d:


    "Pero a la vez, ¡sentía que estaba consciente!"


translate spanish epilogue_uv_city_73eda9e0:


    "Estaba a punto de abrir mis ojos cuando..."


translate spanish epilogue_uv_city_7027362e:


    "Un antiguo y agrietado techo me estaba mirando desde arriba. Una gran grieta lo dividía en dos partes a lo largo, como si amenazara en desplomar el piso de arriba sobre mí."


translate spanish epilogue_uv_city_8c1abb55:


    "El ventilador de la computadora sonaba tranquilamente, pero forzado. Miles de motas de polvo bailaban bellamente en el aire."


translate spanish epilogue_uv_city_d103a6b9:


    "Se escuchaba el llanto del viento fuera de la ventana, lamiendo el vidrio cubierto de hielo y luego alejándose volando, haciendo revolotear los copos de nieve a su paso."


translate spanish epilogue_uv_city_0c034f2c:


    "La brillante y helada luna observaba desde lo alto a {i}mi{/i} mundo, parpadeando alegremente."


translate spanish epilogue_uv_city_416493f0:


    "Había un zumbido resonando pesadamente en mi cabeza y me sentí mareado. Un malgusto desagradable inundaba mi boca, como si me hubiera tragado las sobras de un cenicero y enjuagado posteriormente con una botella de vodka."


translate spanish epilogue_uv_city_fd751d18:


    "No me percaté de golpe."


translate spanish epilogue_uv_city_ff227b7b:


    "Estaba intentando llegar a alguna parte ayer y luego..."


translate spanish epilogue_uv_city_6b1de54a:


    "Y tuve un extraño sueño sobre un verano, un campamento, pioneros y su líder de campamento, sobre una extraña muchacha felina y una ciudad distante."


translate spanish epilogue_uv_city_9715094f:


    "Siempre olvido el final de mis sueños.{w} ¿Llegué a la ciudad o no?"


translate spanish epilogue_uv_city_c814cbd9:


    "Espera un segundo...{w} ¿Realmente fue un sueño?"


translate spanish epilogue_uv_city_5ecfb0de:


    "¡Transcurrí casi una semana en ese campamento!"


translate spanish epilogue_uv_city_aabf4b52:


    "Sin embargo, el reloj no estaba de acuerdo conmigo... según él, tan sólo habían pasado doce horas."


translate spanish epilogue_uv_city_1845cce3:


    "Me puse en pie sentado en la cama y traté de recordar."


translate spanish epilogue_uv_city_890c3f51:


    "Mis sensaciones y emociones eran realmente vívidas, pero los hechos de aquel sueño estaban cubiertos por una niebla."


translate spanish epilogue_uv_city_a9378da6:


    "Recordé las muchachas, sus lágrimas y sus sonrisas, recordé el júbilo y la tristeza que experimentamos juntos, pero sus rostros y cuerpos estaban ocultos por una sombra."


translate spanish epilogue_uv_city_079e3950:


    "Y había un tipo de mina, un refugio antiaéreo y luego una isla y una partida.{w} ¡Y aquellos malditos saltamontes, los cuales estorbaban mi sueño!"


translate spanish epilogue_uv_city_4da0715a:


    "Y Slavya, y Lena, y Alisa, y Ulyana, y Masha...{w} ¡y Yulya!"


translate spanish epilogue_uv_city_f2e3ea45:


    "Pero había otra cosa, algo más allá del sueño. Mi vida {i}posterior{/i}, supuestamente como un nuevo plano de existencia."


translate spanish epilogue_uv_city_71c0a0b1:


    "Sí, me desperté montones de veces, conocí aquellas muchachas mientras estaba {i}despierto{/i} en este mundo, y luego todo volvió a empezar de nuevo..."


translate spanish epilogue_uv_city_f4b5c749:


    "Esta inmensa cantidad de información apenas podía acomodarse en mi cabeza, mi memoria estaba sobresaturada. Olvidaba una cosa a cada segundo y no podía comprender qué era real y qué no lo era."


translate spanish epilogue_uv_city_f51c0761:


    "Todos los hechos de aquel campamento se ajustaron en una única y fantástica imagen, un collage hecho de cientos de trabajos por cientos de artistas con diferentes estilos."


translate spanish epilogue_uv_city_4455d2a3:


    "Fragmentos de sentimientos, sombras de emociones, pedazos de recuerdos. Mi vida fue echa jirones y torpemente cosida de nuevo, y el resultado fue como esa muñeca de trapo desfigurada que hallé en el antiguo campamento junto a Ulyana.{w} Espera... ¡¿o fue con Slavya?!"


translate spanish epilogue_uv_city_dd9d01a3:


    "Aquella búsqueda nocturna por Shurik surgió ante mí con todo lujo de detalles, pero no podía recordar el rostro de la muchacha que me acompañaba en aquella ocasión."


translate spanish epilogue_uv_city_d15cc5f5:


    "{i}El sueño{/i} se volvía más borroso a cada instante. ¿Pronto será olvidado?{w} ¡No puedo dejar que eso ocurra! ¡Debo recordarlo!"


translate spanish epilogue_uv_city_0a28bc9e:


    "Incluso sin detalles, incluso sin rostros, tal vez incluso sin nombres..."


translate spanish epilogue_uv_city_c64f8bb5:


    "¡Estoy seguro de que no es sólo un sueño!{w} Es algo muy grande, algo muy importante para mí..."


translate spanish epilogue_uv_city_ece79837:


    "Una voz comenzó a sonar en mi cabeza y no era la mía, era la de otro."


translate spanish epilogue_uv_city_7d1637b6:


    "Al principio era una voz floja y lejana, pero iba creciendo en volumen a cada instante, pronto casi podía figurarme qué palabras eran."


translate spanish epilogue_uv_city_6c11e880:


    "Pero probablemente era sólo mi imaginación. ¡Un impulso eléctrico del cerebro contenía más información que miles de libros!"


translate spanish epilogue_uv_city_18d6a6f0:


    "Recordé algo de aquel sueño que dejé a medias."


translate spanish epilogue_uv_city_a59d3cd1:


    "¿La ciudad?{w} No, la había alcanzado."


translate spanish epilogue_uv_city_e19d7211:


    "¡Yulya!"


translate spanish epilogue_uv_city_a529acd3:


    "Desde las profundidades de mi consciencia surgió la imagen de la muchacha con aspecto de gata. Ella se me quedaba mirando fijamente, furiosa, y movía sus orejas de una manera graciosa."


translate spanish epilogue_uv_city_8ec1f570:


    "¿No fue un sueño...? ¡¿No fue acaso un sueño la semana que transcurrí en el campamento?!"


translate spanish epilogue_uv_city_7f69f2b6:


    "Caminé dando vueltas por la habitación, mirando a través de la ventana. Fuera el mundo estaba igual que {i}ayer{/i}."


translate spanish epilogue_uv_city_f65a8fd8:


    "¡Estaba al borde de volverme loco!"


translate spanish epilogue_uv_city_e729f7fe:


    "El silencio de mi piso se vio perturbado por el timbre de la campanilla de la puerta."


translate spanish epilogue_uv_city_52f1355e:


    "¡¿Quién puede ser a estas horas?!"


translate spanish epilogue_uv_city_d1f773e3:


    "Al principio no quise abrir la puerta. Estaba incluso asustado, pero luego pensé que era una buena oportunidad para comprobarlo: ¿era {i}esto{/i} un sueño?"


translate spanish epilogue_uv_city_6dcbff83:


    "¡Las personas son totalmente distintas en los sueños respecto a la realidad!"


translate spanish epilogue_uv_city_e05fd94e:


    "¡Abriré la puerta y lo descubriré!"


translate spanish epilogue_uv_city_118a9de7:


    "Me las apañé para llegar hasta la entrada en un par de brincos, estiré del pomo de la puerta sin mirar siquiera por la mirilla, y..."


translate spanish epilogue_uv_city_cc41fdb9:


    "Las muchachas estaban frente a la puerta...{w} ¡Las muchachas del campamento!"


translate spanish epilogue_uv_city_91ccfacc:


    "Pero alguna cosa había cambiado en ellas."


translate spanish epilogue_uv_city_e2eac827:


    "No recordaba cada detalle de sus aspectos en el sueño. Sin embargo, el corte de cabello de Slavya...{w} Y Ulyana que parecía más alta."


translate spanish epilogue_uv_city_85b6f90b:


    "Un profundo silencio enrareció el ambiente por un instante. Estaba de piedra, mis pensamientos congelados, sin temor u otra emoción."


translate spanish epilogue_uv_city_c9f8a031:


    "Las muchachas no parecían estar muy sorprendidas..."


translate spanish epilogue_uv_city_51149e68:


    me "..."


translate spanish epilogue_uv_city_9104ac12:


    sl "¡Hola!"


translate spanish epilogue_uv_city_f64f0819:


    "Dijo Slavya alegremente."


translate spanish epilogue_uv_city_aa88bb70:


    us "¡¿Por qué os quedáis quietas como tontas?!"


translate spanish epilogue_uv_city_517f79f3:


    "Exclamó Ulyana."


translate spanish epilogue_uv_city_0296ee39:


    me "Cómo habéis..."


translate spanish epilogue_uv_city_3109432e:


    "Tenía las palabras en la punta de la lengua, pero no podía articular ni una para preguntarles."


translate spanish epilogue_uv_city_0a8eba07:


    "¡Tenía tantas cosas que preguntar de golpe!"


translate spanish epilogue_uv_city_2e4ca1c2:


    un "Tranquilizaos, muchachas. ¡No deberíais darle un susto a este hombre!"


translate spanish epilogue_uv_city_58da8a51:


    "Y recordé a Lena de inmediato, me acordé de nuestra vida tras el campamento..."


translate spanish epilogue_uv_city_7418a938:


    "Y de Slavya, aquella conversación por la noche en la parada de autobús."


translate spanish epilogue_uv_city_34324030:


    "Y de Ulyana. Nuestro encuentro en la universidad."


translate spanish epilogue_uv_city_4135852b:


    "Y de Alisa en mi concierto."


translate spanish epilogue_uv_city_68c476d7:


    "E incluso de Masha en nuestro mundo."


translate spanish epilogue_uv_city_e64bad3c:


    "¿Pero cómo...?"


translate spanish epilogue_uv_city_a5ba39bf:


    dv "Sí, bueno. En resumidas cuentas..."


translate spanish epilogue_uv_city_368e3ebe:


    "Alisa empezó a explicar indiferentemente."


translate spanish epilogue_uv_city_bf9f2b06:


    dv "No te sorprendas tanto. ¿Creías que eras el único...?"


translate spanish epilogue_uv_city_ff147745:


    ma "En Sovyonok."


translate spanish epilogue_uv_city_e9b81946:


    dv "Sí, sí, en Sovyonok. ¡No eras el único!"


translate spanish epilogue_uv_city_9a17ab72:


    us "¡Nosotras también estuvimos!"


translate spanish epilogue_uv_city_3465113b:


    me "Pero como habéis... todas juntas, aquí..."


translate spanish epilogue_uv_city_c643907f:


    sl "La cuestión es que cada una de nosotras tenía su propio campamento y que tú tenías el tuyo. Quiero decir que en el nuestro te comportabas... de manera diferente."


translate spanish epilogue_uv_city_5307db81:


    "Slavya parecía incómoda."


translate spanish epilogue_uv_city_e63f8ffa:


    us "¡Realmente muy diferente! ¡Me perseguiste con aquel pastel, apenas pude escaparme!"


translate spanish epilogue_uv_city_8512ffc9:


    ma "Resultó que recordamos todo lo que hiciste y comprendimos que no eran simples sueños."


translate spanish epilogue_uv_city_0fb77060:


    me "Espera, pero cómo puede ser... Únicamente transcurrió un día."


translate spanish epilogue_uv_city_def4bb08:


    un "No, lleva ya mucho tiempo. Al principio también pensé eso, que sólo tuve sueños sobre el campamento, pero luego la gente de {i}allí{/i} comenzó a volverse más real. Tú también empezaste..."


translate spanish epilogue_uv_city_9fe29739:


    "Ella suspiró profundamente."


translate spanish epilogue_uv_city_d14e992a:


    un "Y aquellos horrores..."


translate spanish epilogue_uv_city_d04d005c:


    "Ahora parecía que Lena se echara a llorar en cualquier segundo."


translate spanish epilogue_uv_city_58742dd8:


    dv "¡Déjalo ya! Te dije mil veces que ése no era él, ¡que era otro Semyon!"


translate spanish epilogue_uv_city_1c47284d:


    me "¿Qué significa eso de «otro Semyon»...?"


translate spanish epilogue_uv_city_1ace01ff:


    sl "Verás, hay muchos sueños, muchos campamentos, cada uno tiene su propia gente. Y en cada uno de ellos tú te comportas de forma distinta. En algunos es así de..."


translate spanish epilogue_uv_city_57819faa:


    "Ella miró a Lena con tristeza y continuó:"


translate spanish epilogue_uv_city_d2a2bfc1:


    sl "Muchas de tus reencarnaciones nos explicaron sobre la existencia de otros campamentos y otras versiones de nosotras... Pero tan sólo éramos muñecas allí, actuando conforme a roles predeterminados."


translate spanish epilogue_uv_city_37bdf439:


    us "¡No soy ninguna muñeca!"


translate spanish epilogue_uv_city_1678d751:


    "Ulyana estaba enfadada."


translate spanish epilogue_uv_city_2800154c:


    dv "Nos acabamos cansando de aquello, ¡desde luego! Empezamos a buscar una salida y una vez llegué al mundo de Miku."


translate spanish epilogue_uv_city_f109e0d1:


    ma "¡No me llames así!"


translate spanish epilogue_uv_city_42898380:


    me "Pero... recuerdo... que el campamento era completamente diferente allí, estábamos rodando una película y..."


translate spanish epilogue_uv_city_1b96ba71:


    ma "¿Qué diferencia hay? En cualquier caso el sueño se repitió."


translate spanish epilogue_uv_city_51149e68_1:


    me "..."


translate spanish epilogue_uv_city_a5cd4aee:


    dv "Bueno, entonces nos las apañamos para conectar el resto de mundos en uno para nosotras cinco. Y a partir de ahí, ¡fue coser y cantar!"


translate spanish epilogue_uv_city_ab0f8bf4:


    "Alisa sonrió y reconoció una sonrisa."


translate spanish epilogue_uv_city_87568219:


    sl "La única cosa pendiente era encontrarte a ti."


translate spanish epilogue_uv_city_b4cf4929:


    me "¿Encontrarme? ¿Por qué?"


translate spanish epilogue_uv_city_988a30f2:


    "¡De todos modos tampoco estaba en contra de ello!"


translate spanish epilogue_uv_city_43800bc3:


    "¡Por fin todo encajaba en su lugar! Sovyonok, los pioneros, la líder del campamento, estas muchachas..."


translate spanish epilogue_uv_city_d37fcc88:


    me "¿Y qué hay de Yulya?"


translate spanish epilogue_uv_city_71927170:


    un "¿Quién?"


translate spanish epilogue_uv_city_f9f2a365:


    me "Yulya, la muchacha felina. ¿No la visteis?"


translate spanish epilogue_uv_city_2bdb69db:


    sl "Sí, alguien mencionó alguna cosa sobre eso..."


translate spanish epilogue_uv_city_e3f80252:


    me "¿Cómo puede ser que...?"


translate spanish epilogue_uv_city_d69ea088:


    "Todavía me sentía culpable por abandonar a Yulya."


translate spanish epilogue_uv_city_09622045:


    dv "Así es como va. Cada una de nosotras tenía únicamente un mundo propio, y tú tenías muchos de ellos."


translate spanish epilogue_uv_city_367090df:


    sl "Y entonces comprendimos que solamente tú estabas conectado con la realidad."


translate spanish epilogue_uv_city_e0a04c2e:


    me "¿Cómo comprendisteis eso?"


translate spanish epilogue_uv_city_e29b42f5:


    ma "El pionero nos lo explicó. Quiero decir tu yo que afirmaba haber estado en aquel sueño por mucho tiempo."


translate spanish epilogue_uv_city_d180f7cd:


    us "¡Un tipo raro! ¡Probablemente se volvió loco de atar!"


translate spanish epilogue_uv_city_c08f848b:


    "No podía asimilar directamente en mi mente todo lo que me estaban diciendo."


translate spanish epilogue_uv_city_6bdc8ef5:


    me "Vale, esperad..."


translate spanish epilogue_uv_city_670ed473:


    "Recopilé todas mis ideas y las valoré."


translate spanish epilogue_uv_city_4220c762:


    me "¿Entonces aquel campamento no fue un sueño? ¿Todo estuvo sucediendo desde hace mucho tiempo? ¿Estábamos en muchos {i}campamentos{/i} tanto yo como vosotras? ¿Pero cada una de vosotras tenía uno y yo tenía muchos...? No, no lo comprendo."


translate spanish epilogue_uv_city_0e688c6f:


    un "Eso es correcto."


translate spanish epilogue_uv_city_1f90b603:


    us "Deja ya de ser hosco. ¡Eres muy aburrido! ¡Fue muy divertido! Por ejemplo, ¡nunca había visitado un campamento de pioneros! Tú probablemente tampoco."


translate spanish epilogue_uv_city_156e1688:


    me "¿Pero quién estaba detrás de todo esto? ¿Y por qué?"


translate spanish epilogue_uv_city_b1c7cc7f:


    sl "Por eso vinimos... para averiguarlo."


translate spanish epilogue_uv_city_0eeb4092:


    me "¿Pero cómo me hallasteis?"


translate spanish epilogue_uv_city_a5ee8aa0:


    "Repentinamente, sentí duda e incluso temor."


translate spanish epilogue_uv_city_85f93595:


    "Ulyana me pellizcó de golpe."


translate spanish epilogue_uv_city_38b5e0e1:


    me "¡Ay!"


translate spanish epilogue_uv_city_47abcbd1:


    us "¡Sólo por si acaso pensabas que estabas soñando!"


translate spanish epilogue_uv_city_50ea836a:


    ma "No fue difícil."


translate spanish epilogue_uv_city_6b8e3cb6:


    dv "Tus copias podían ser verdaderamente muy conversadoras, si querían."


translate spanish epilogue_uv_city_0ad194ed:


    un "O si las forzabas..."


translate spanish epilogue_uv_city_6be3f064:


    "Añadió Lena en voz baja."


translate spanish epilogue_uv_city_a67950e2:


    sl "Así que primero nos conocimos en la vida real, y luego vinimos a ti."


translate spanish epilogue_uv_city_778a81fa:


    us "¡Dale la bienvenida a tus invitadas!"


translate spanish epilogue_uv_city_76517ead:


    me "Increíble..."


translate spanish epilogue_uv_city_22a5a4b0:


    "Mi vida cambió en un abrir y cerrar de ojos."


translate spanish epilogue_uv_city_5d555141:


    "Obviamente no recuerdo este sueño, o los sueños como las muchachas dijeron, con todo lujo de detalles, pero también sentí que el campamento era algo más grande."


translate spanish epilogue_uv_city_056a21ff:


    "Nos reunió a todos juntos, primero {i}allí{/i} y ahora {i}aquí{/i}."


translate spanish epilogue_uv_city_5573410f:


    "Solamente la cuestión de Yulya continuaba pendiente.{w} ¿Pero puedo saber la respuesta?{w} Podría haberla olvidado, justo como olvidé otras tantas cosas."


translate spanish epilogue_uv_city_087cd72d:


    me "Vale. Espero que me contéis todo con detalle."


translate spanish epilogue_uv_city_49cc0ae4:


    sl "¡Claro!"


translate spanish epilogue_uv_city_cfc15b56:


    dv "Para eso estamos aquí."


translate spanish epilogue_uv_city_5034671b:


    me "Bueno, adelante. No os quedéis aquí en la entrada..."


translate spanish epilogue_uv_city_d5bf3b5e:


    me "¿Té, café, baile?"


translate spanish epilogue_uv_city_0dd900f4:


    "Añadí, tratando de relajarme y recuperar mis sentidos."


translate spanish epilogue_uv_city_01827c2a:


    "Los ojos de Lena me fulminaron."


translate spanish epilogue_uv_city_a722bd37:


    me "Está bien, vale, nos ahorramos el baile para más tarde..."


translate spanish epilogue_uv_city_d99864b1:


    "Cada historia tiene su principio y su final."


translate spanish epilogue_uv_city_4d69c034:


    "Cada historia tiene su lienzo, su sinopsis, su contenido, sus momentos clave, su prólogo y su epílogo."


translate spanish epilogue_uv_city_754a4f66:


    "Y no hay libro que, tras releerse, no fuera a revelar nuevos detalles de los que antes no te percataras."


translate spanish epilogue_uv_city_39cdaa6f:


    "Pero todo libro tiene una última página. Y una vez la hemos pasado, ponemos el libro en la estantería."


translate spanish epilogue_uv_city_611077c4:


    "Sólo para abrir uno nuevo mañana..."


translate spanish epilogue_uv_ulya_01822020:


    me "Vale."


translate spanish epilogue_uv_ulya_4569b623:


    "Reuní fuerzas para hablar."


translate spanish epilogue_uv_ulya_8f6b1a03:


    uv "¿Qué?"


translate spanish epilogue_uv_ulya_0726abef:


    "Yulya estaba sorprendida."


translate spanish epilogue_uv_ulya_0459b6ff:


    me "De acuerdo, retornemos.{w} ¿No esperabas esta respuesta?"


translate spanish epilogue_uv_ulya_3aa0974f:


    "Sonrió astutamente."


translate spanish epilogue_uv_ulya_13afe092:


    uv "No. ¿Por qué crees eso?"


translate spanish epilogue_uv_ulya_94105ae3:


    me "Porque siempre dices que todo se repite por sí mismo tras cada siete días, y que no puedo escapar de aquí..."


translate spanish epilogue_uv_ulya_1a2b07d6:


    uv "¡Pues comprobémoslo juntos!"


translate spanish epilogue_uv_ulya_4c07374f:


    me "Oh, ¿comprobarlo?{w} ¿Para empezar no deberíamos haber comprobado esa ciudad?"


translate spanish epilogue_uv_ulya_b4e02fac:


    uv "Depende de ti escoger."


translate spanish epilogue_uv_ulya_8f838576:


    "La miré atentamente."


translate spanish epilogue_uv_ulya_5b06f4d4:


    "Algo parecía haber cambiado en Yulya, pero no podía comprender qué era exactamente."


translate spanish epilogue_uv_ulya_1c771708:


    me "Ya he decidido..."


translate spanish epilogue_uv_ulya_42765c89:


    "Murmuré."


translate spanish epilogue_uv_ulya_ba638d57:


    uv "¡Pues deberíamos irnos!"


translate spanish epilogue_uv_ulya_1bc90a19:


    "La muchacha felina se abrió camino a través de los campos en dirección al bosque con confianza."


translate spanish epilogue_uv_ulya_219f2350:


    me "Ey, espera, ¡¿a dónde vas?!"


translate spanish epilogue_uv_ulya_88d9a691:


    uv "Llevará menos tiempo."


translate spanish epilogue_uv_ulya_bb93d8ba:


    me "¿Menos tiempo...?"


translate spanish epilogue_uv_ulya_0c925c52:


    "Mis pensamientos comenzaron a dar vueltas en mi cabeza furiosamente, fundiéndose en uno, el cual probablemente no sería uno agradable."


translate spanish epilogue_uv_ulya_e9681ae3:


    me "Entonces todo este tiempo..."


translate spanish epilogue_uv_ulya_1402ff26:


    "La atrapé en un par de zancadas y la agarré por la cola."


translate spanish epilogue_uv_ulya_b96afcb5:


    me "¿...lo sabías todo este tiempo?{w} ¿Que podía cortar por este atajo? ¡¿Sin tener que vagar durante todo el día?!"


translate spanish epilogue_uv_ulya_6a9c4595:


    "Yulya siseó encolerizada y me mostró los dientes. Solté su cola."


translate spanish epilogue_uv_ulya_7d489e88:


    uv "¡No!{w} Quiero decir, puede que lo sepa, ¡pero no se te ha perdido nada allí!"


translate spanish epilogue_uv_ulya_9af1bc42:


    me "¿En la ciudad?"


translate spanish epilogue_uv_ulya_0e8e1d0f:


    uv "Sí."


translate spanish epilogue_uv_ulya_5ac38cf1:


    me "¿Por qué?"


translate spanish epilogue_uv_ulya_a755a034:


    uv "No lo sé. No debería ser de esa forma. ¡No es lo correcto! No sé explicarlo, ¡pero lo sé!"


translate spanish epilogue_uv_ulya_e46208f1:


    "Me di la vuelta y miré a las lejanas luces."


translate spanish epilogue_uv_ulya_27c22bd1:


    "Me pregunto si las cosas podrían ir a peor..."


translate spanish epilogue_uv_ulya_1d8e7c07:


    "No es que todo vaya realmente mal ahora mismo, pero ¿y si la ciudad no desaparece? ¿Y qué ocurre en el campamento? ¿Qué significaban las palabras de Olga Dmitrievna? ¿Debería seguir dudando, si ya he escogido qué hacer?"


translate spanish epilogue_uv_ulya_e744e646:


    me "Muy bien, vayamos..."


translate spanish epilogue_uv_ulya_ce617998:


    "..."


translate spanish epilogue_uv_ulya_164351d0:


    "Caminamos a través del bosque casi todo el camino, cruzando los bordes del bosque y los campos en pocas ocasiones."


translate spanish epilogue_uv_ulya_cadcee8c:


    "Yulya parecía estar extremadamente satisfecha con algo."


translate spanish epilogue_uv_ulya_d7695174:


    me "¿Me podrías explicar qué pasa contigo?"


translate spanish epilogue_uv_ulya_a5018e29:


    uv "Todo es tan atípico..."


translate spanish epilogue_uv_ulya_e1c6333b:


    me "Porque antes no era así, todo se repite por sí mismo tras siete días...{w} Claro, ya he escuchado eso antes."


translate spanish epilogue_uv_ulya_e6d7190e:


    "Ella tan sólo me sonrió en respuesta."


translate spanish epilogue_uv_ulya_a86bf17a:


    me "Me pregunto que habrá ocurrido en el campamento..."


translate spanish epilogue_uv_ulya_311c443d:


    uv "¡Mira!"


translate spanish epilogue_uv_ulya_1ac599e9:


    "Las puertas del campamento eran visibles a la distancia, más allá de los árboles."


translate spanish epilogue_uv_ulya_7dc8253b:


    "Comenzamos a caminar más rápido."


translate spanish epilogue_uv_ulya_ce617998_1:


    "..."


translate spanish epilogue_uv_ulya_bc110277:


    "¡Estaba claro que algo iba mal! Sovyonok parecía abandonado."


translate spanish epilogue_uv_ulya_35d98a27:


    "Podría asumir que los pioneros se fueron a la cama, pero no habrían luces salvo las farolas de las calles. ¿Y por qué no está aquí Olga Dmitrievna esperándonos con un grupo para riesgos biológicos?"


translate spanish epilogue_uv_ulya_f8c2ea35:


    "Ella dijo por el walkie-talkie que algo estaba ocurriendo..."


translate spanish epilogue_uv_ulya_af8150cd:


    "De golpe una extraña ansiedad me abrumó.{w} Miré a Genda y por ahí..."


translate spanish epilogue_uv_ulya_f10b5d1e:


    me "¡La ciudad! ¡No hay ninguna ciudad!"


translate spanish epilogue_uv_ulya_3dcb7711:


    "Yulya también parecía sorprendida, sorprendida de verdad. Así que no le asalté con preguntas."


translate spanish epilogue_uv_ulya_0b9c8da0:


    "Habiendo llegado a la cabaña de la líder del campamento, me detuve dudando por un instante."


translate spanish epilogue_uv_ulya_187b5819:


    "Ante mí vinieron las imágenes de los posibles hechos."


translate spanish epilogue_uv_ulya_9b3de332:


    "Sin embargo, rápidamente me rehice."


translate spanish epilogue_uv_ulya_27c40073:


    me "¡Esto es algo absurdo!"


translate spanish epilogue_uv_ulya_d4106a37:


    "No había nada que corroborara la idea de esta historia de ciencia ficción volviéndose en una de suspense sangriento."


translate spanish epilogue_uv_ulya_14081a40:


    "Con determinación, estiré del pomo de la puerta para entrar en la cabaña."


translate spanish epilogue_uv_ulya_4b142943:


    "No había nadie dentro..."


translate spanish epilogue_uv_ulya_990400df:


    "Bueno, ¡pues no tiene sentido quedarse aquí!"


translate spanish epilogue_uv_ulya_1b3b6813:


    "Salí corriendo hacia la cabaña de Ulyana y Alisa."


translate spanish epilogue_uv_ulya_c80ffad8:


    "Sin pensármelo, llamé a la puerta..."


translate spanish epilogue_uv_ulya_7254fc56:


    "Pero solamente el silencio me respondió."


translate spanish epilogue_uv_ulya_01d1d520:


    "Caminé alrededor de la cabaña y miré por la ventana."


translate spanish epilogue_uv_ulya_b0b2c18b:


    "Estaba vacía."


translate spanish epilogue_uv_ulya_c18e9490:


    "Bueno, eso no significa nada...{w} ¡Puede que se hayan ido a alguna parte!"


translate spanish epilogue_uv_ulya_4921445e:


    "Corrí hasta la cantina."


translate spanish epilogue_uv_ulya_0633fc4a:


    "Quizá todo el mundo pensó en cenar tarde..."


translate spanish epilogue_uv_ulya_d1faa6ec:


    "Sin embargo, la puerta estaba cerrada."


translate spanish epilogue_uv_ulya_7f2ca87a:


    "Volví a la plaza y me desplomé en un banco, exhausto."


translate spanish epilogue_uv_ulya_6e6530d1:


    uv "Basta ya de correr, ¿te parece?"


translate spanish epilogue_uv_ulya_522d9099:


    "Yulya, quien me siguió callada, me lo pidió sin mucho interés."


translate spanish epilogue_uv_ulya_e3b9d717:


    "Bueno, es verdad. ¿Por qué entré en pánico...?"


translate spanish epilogue_uv_ulya_fc975872:


    "Aunque, y si en realidad algo había sucedido..."


translate spanish epilogue_uv_ulya_5d937815:


    uv "Bueno, estoy hambrienta. Voy a ir a buscar algo para comer."


translate spanish epilogue_uv_ulya_dc61ff3f:


    "No me opuse, en un instante Yulya desapareció silenciosamente en la noche."


translate spanish epilogue_uv_ulya_92c8e965:


    "Tanto si esa era alguna broma estúpida (lo cual era difícil de creer), ¡como si realmente todos han desaparecido a algún lugar!"


translate spanish epilogue_uv_ulya_f6778e37:


    "El suceso no es que sea mucho más raro de lo que es mi llegada aquí, o aquella misteriosa ciudad la cual se mostró y luego se desvaneció, pero..."


translate spanish epilogue_uv_ulya_4fef03bd:


    "Estaba acostumbrado a este campamento y a sus lugareños. Y ahora, semejante giro de 180 grados..."


translate spanish epilogue_uv_ulya_dacde494:


    "¡Estaba más o menos adaptado a mis nuevas circunstancias y ahora ocurre algo otra vez!"


translate spanish epilogue_uv_ulya_70a36eec:


    "Maldecí y me cubrí la cara con mis manos."


translate spanish epilogue_uv_ulya_98d346b6:


    "Probablemente me habría quedado sentado ahí durante varios minutos hasta que las lágrimas empezaran a caer, pero..."


translate spanish epilogue_uv_ulya_14c6fc57:


    "Mis tímpanos casi se rompieron por un estruendo.{w} Sonó como un trueno."


translate spanish epilogue_uv_ulya_f0593949:


    "¿Pero de dónde venía? No había ni una nube en el cielo."


translate spanish epilogue_uv_ulya_d7f3983d:


    "Abrí mis ojos y vi alguien frente a mí."


translate spanish epilogue_uv_ulya_7561217b:


    me "Tú... tú..."


translate spanish epilogue_uv_ulya_c0870411:


    "Pronuncié, tartamudeando."


translate spanish epilogue_uv_ulya_8e73390d:


    "Una tormenta retumbaba en el despejado cielo, los relámpagos destelleantes oscurecían el rostro del hombre que estaba frente a mí."


translate spanish epilogue_uv_ulya_52297f6e:


    pi "¿A qué viene esa cara triste, Semyon?"


translate spanish epilogue_uv_ulya_d4875625:


    me "¿Quién eres?"


translate spanish epilogue_uv_ulya_a5efbe41:


    "Grité."


translate spanish epilogue_uv_ulya_ae1f4d5e:


    pi "Cálmate, no tienes que ponerte tan tenso. Las células nerviosas no se regeneran, ya sabes."


translate spanish epilogue_uv_ulya_163770ab:


    "Mi primera idea fue correr, pero mi cuerpo no respondió a dicha idea."


translate spanish epilogue_uv_ulya_8aa5c206:


    "El relámpago todavía estaba destelleando brillantemente, y no había manera de ver la cara del pionero con claridad."


translate spanish epilogue_uv_ulya_a4822e0d:


    "Sin embargo, estaba seguro de haber oído esa voz con anterioridad."


translate spanish epilogue_uv_ulya_0b34c373:


    me "¿Quién eres tú?"


translate spanish epilogue_uv_ulya_ca57930f:


    "Pregunté con un tono tranquilo."


translate spanish epilogue_uv_ulya_06a6a402:


    pi "¿Quizás tengas preguntas más apropiadas?"


translate spanish epilogue_uv_ulya_20e14b1f:


    me "¿A dónde han desaparecido todos?"


translate spanish epilogue_uv_ulya_7e18cb0c:


    pi "Ellos ya han realizado sus papeles y ya no los necesitamos más."


translate spanish epilogue_uv_ulya_7b84c73a:


    me "¿Papeles? ¿Qué papeles?"


translate spanish epilogue_uv_ulya_32e5b62e:


    pi "Sus partes."


translate spanish epilogue_uv_ulya_71a264d4:


    me "¿Y qué pasa con la ciudad?"


translate spanish epilogue_uv_ulya_387afb71:


    pi "¿Qué ciudad?"


translate spanish epilogue_uv_ulya_da03225b:


    "No estaba seguro de si había detectado un rasgo de sorpresa en su voz, o simplemente me lo imaginé."


translate spanish epilogue_uv_ulya_1cb27603:


    me "La ciudad. ¡Allí!"


translate spanish epilogue_uv_ulya_6546a605:


    "Moví mi mano en dirección a Genda."


translate spanish epilogue_uv_ulya_88b778cd:


    pi "Ah, estás hablando de..."


translate spanish epilogue_uv_ulya_6f2d5bcd:


    "Se quedó en silencio por un instante."


translate spanish epilogue_uv_ulya_53131bc2:


    pi "¡Es tu culpa!"


translate spanish epilogue_uv_ulya_d3bd0899:


    me "No lo entiendo."


translate spanish epilogue_uv_ulya_b8796610:


    pi "¡Es porque no lo intentaste con suficiente esfuerzo!"


translate spanish epilogue_uv_ulya_fa68beb0:


    "El pionero alzó su voz."


translate spanish epilogue_uv_ulya_d7e55f45:


    me "Yo... yo..."


translate spanish epilogue_uv_ulya_a98feb47:


    pi "¡Sólo tienes que pensar! ¡Solamente piensa! ¿Cuánto tiempo has estado aquí? ¿Cinco días? ¿Seis? ¿O tal vez veintiseis?"


translate spanish epilogue_uv_ulya_e0eaa37b:


    me "Seis... probablemente..."


translate spanish epilogue_uv_ulya_1190dcda:


    pi "¿seis? ¿Por qué seis? ¿Estás seguro de que son seis? ¿Por qué estás seguro? ¿Porque contaste el número de amaneceres y de albas? ¿O porque miraste la fecha en tu telefóno móvil?"


translate spanish epilogue_uv_ulya_7479b07e:


    me "No lo sé..."


translate spanish epilogue_uv_ulya_aa8531fb:


    "El pionero frente a mí perdió su paciencia. No tenía ni idea de qué estaba hablando."


translate spanish epilogue_uv_ulya_9e98cfe6:


    pi "Seis... Vale, dejemos que sean seis."


translate spanish epilogue_uv_ulya_62a72b45:


    "Dijo tranquilamente."


translate spanish epilogue_uv_ulya_c747fdad:


    pi "¿Y no has estado antes aquí?"


translate spanish epilogue_uv_ulya_5cb822ee:


    me "No..."


translate spanish epilogue_uv_ulya_94259d94:


    pi "¿Estás seguro sobre eso también?"


translate spanish epilogue_uv_ulya_7479b07e_1:


    me "No lo sé..."


translate spanish epilogue_uv_ulya_24167162:


    pi "¿Y si te digo que tú ya has estado aquí antes?"


translate spanish epilogue_uv_ulya_016bd72c:


    "No tenía ni la más mínima idea de cómo reaccionar a sus palabras. Algo me estaba sucediendo, como el primer día aquí, algo inexplicable, terrorífico y tal vez peligroso."


translate spanish epilogue_uv_ulya_6656d4b0:


    me "Pero no lo recuerdo..."


translate spanish epilogue_uv_ulya_3ab912f7:


    pi "¿Y por qué tienes que acordarte?"


translate spanish epilogue_uv_ulya_695225db:


    "Se quedó callado, parecía ensimismado."


translate spanish epilogue_uv_ulya_76244013:


    pi "Aun así, realmente no te acuerdas. Bueno, entonces te lo explicaré."


translate spanish epilogue_uv_ulya_6a29747a:


    "Apenas podía escuchar sus palabras debido al ruido de los truenos."


translate spanish epilogue_uv_ulya_2d849aac:


    "Mis ojos se estaban humedeciendo a causa de los destellos de los relámpagos, pero estaba asustado de que con sólo pestañear le diera la oportunidad de desaparecer a este pionero."


translate spanish epilogue_uv_ulya_c440a75f:


    "Puede que realmente me cuente cómo llegué hasta aquí."


translate spanish epilogue_uv_ulya_b17892d5:


    pi "Ésta no es la primera vez que has estado aquí. No sé cuántas vueltas has dado, pero no importan."


translate spanish epilogue_uv_ulya_e453e3db:


    me "¿Pero por qué? No me acuerdo de nada."


translate spanish epilogue_uv_ulya_c23e302b:


    pi "¡No deberías recordarlo! ¡No deberías!"


translate spanish epilogue_uv_ulya_419b652c:


    "Me gritó."


translate spanish epilogue_uv_ulya_44b2f075:


    me "Lo siento..."


translate spanish epilogue_uv_ulya_10dc660b:


    "Elegí no interrumpirle."


translate spanish epilogue_uv_ulya_4d99f82b:


    pi "Sí, no es tu primera vez aquí..."


translate spanish epilogue_uv_ulya_f48ef569:


    "Continuó con una voz calmada."


translate spanish epilogue_uv_ulya_2a0f20ef:


    pi "Pero esta vez es especial. Ésta será la última vez para ti."


translate spanish epilogue_uv_ulya_89fb5012:


    "No podía comprender cuál era la definción de las palabras «última vez»."


translate spanish epilogue_uv_ulya_be736c58:


    "¿Escaparía de aquí y retornaría a mi vida normal? ¿O significaba que moriría aquí...?"


translate spanish epilogue_uv_ulya_6563fe0f:


    pi "Bueno, solamente es mi opinión."


translate spanish epilogue_uv_ulya_b2744c5a:


    "Se calló por un largo rato."


translate spanish epilogue_uv_ulya_57996fa1:


    "Al fin decidí hacerle una pregunta."


translate spanish epilogue_uv_ulya_b45a54a4:


    me "¿Cómo sabes todo esto?"


translate spanish epilogue_uv_ulya_ea18c8d8:


    pi "¿Cómo? Digamos que soy tu compañero de fatigas. Y..."


translate spanish epilogue_uv_ulya_3f1ecfd9:


    "Una explosión se escuchó cerca. Era como si un relámpago hubiera caído sobre un árbol a tan sólo unas docenas de metros lejos de mí."


translate spanish epilogue_uv_ulya_ab12a71f:


    "Instintivamente me cubrí las manos y me amagué debajo del banco."


translate spanish epilogue_uv_ulya_fe32bb4a:


    "No sé cuánto tiempo llevaba ahí, pero para cuando me había recobrado ligeramente del susto, los truenos se detuvieron."


translate spanish epilogue_uv_ulya_5deace4a:


    "Abrí mis ojos..."


translate spanish epilogue_uv_ulya_b956f2ed:


    "Yulya estaba donde el pionero había estado antes."


translate spanish epilogue_uv_ulya_8c9a0417:


    uv "Vaya, así que conociste a tu colega."


translate spanish epilogue_uv_ulya_9ef50886:


    "Me sonrió y me ofreció su mano.{w} Conmocionado, no sabía qué hacer y seguí quieto agachado."


translate spanish epilogue_uv_ulya_22e36827:


    uv "¿Cuánto tiempo más te piensas pasar ahí?"


translate spanish epilogue_uv_ulya_b4517927:


    "Me puse en pie con dificultad."


translate spanish epilogue_uv_ulya_66d35f4b:


    me "¿Quién era ése?"


translate spanish epilogue_uv_ulya_e4fd6319:


    "Si tuviera que escoger lo menos malo, pues creería antes en Yulya que en el misterioso pionero."


translate spanish epilogue_uv_ulya_b558a3fc:


    uv "Bueno, cómo lo diría..."


translate spanish epilogue_uv_ulya_60f3d758:


    "Se quedó ensimismada."


translate spanish epilogue_uv_ulya_7f1538dd:


    uv "Sabes, no eres el único. Para ser más exactos, eres el único aquí, pero... Hay muchos más campamentos. Muchos de ellos. Y hay muchos iguales como tú. Muchos de ellos también."


translate spanish epilogue_uv_ulya_1a2e4c0f:


    "No comprendí nada."


translate spanish epilogue_uv_ulya_2560e52e:


    uv "Algunos de ellos pueden aparecer en otros lugares. Como este pionero, por ejemplo."


translate spanish epilogue_uv_ulya_b888bb40:


    me "Entonces tú lo sabías todo eso, ¿y no me explicaste nada?"


translate spanish epilogue_uv_ulya_eab3261b:


    uv "No me lo preguntaste."


translate spanish epilogue_uv_ulya_b53b2a58:


    "Se encogió de hombros."


translate spanish epilogue_uv_ulya_23e3138a:


    me "¿Debería preocuparme de él?"


translate spanish epilogue_uv_ulya_2feb68a6:


    uv "No creo que debieras... Aunque..."


translate spanish epilogue_uv_ulya_1adb473f:


    me "¿Debería o no?"


translate spanish epilogue_uv_ulya_221d3699:


    "Comenzaba a estar ansioso."


translate spanish epilogue_uv_ulya_089ed32b:


    uv "Dudo que él pueda tomar una... forma física...{w} Pero deberías ir con cuidado con él."


translate spanish epilogue_uv_ulya_5b247995:


    "Había recibido tanta información de golpe que necesitaría más de una semana para asimilarla toda."


translate spanish epilogue_uv_ulya_c8cd06b9:


    "A pesar de tener todo eso en mente, comprendí que esta información tampoco explicaba nada."


translate spanish epilogue_uv_ulya_565ca87a:


    me "Entonces hay muchas personas como yo..."


translate spanish epilogue_uv_ulya_0e8e1d0f_1:


    uv "Sí."


translate spanish epilogue_uv_ulya_8e1e107d:


    me "¿Pero cómo llegué... llegamos aquí?"


translate spanish epilogue_uv_ulya_9f91c876:


    uv "No tengo una respuesta exacta para esa pregunta. Sólo sé que fuiste tú quien llegó aquí. Solamente una persona llegó aquí, para ser más precisos."


translate spanish epilogue_uv_ulya_03009af2:


    "Ahora Yulya no parecía en absoluto aquella muchacha extraña con orejas de animal que esparcía azúcar sobre las setas."


translate spanish epilogue_uv_ulya_84949186:


    "Ella era exactamente todo lo contrario. Había confianza en sus ojos e incluso algo de arrogancia, y ningún rastro de su anterior inmadurez."


translate spanish epilogue_uv_ulya_321365d8:


    me "¿Quién eres tú, pues?"


translate spanish epilogue_uv_ulya_739f08ea:


    uv "¿Yo?"


translate spanish epilogue_uv_ulya_fc30fd2b:


    me "Sí."


translate spanish epilogue_uv_ulya_1227e8ef:


    uv "Sinceramente, no tengo ni idea..."


translate spanish epilogue_uv_ulya_c459add4:


    "Me quedé mirándola con la boca abierta."


translate spanish epilogue_uv_ulya_8579e67d:


    me "Cómo es eso... ¿no tienes ni idea?"


translate spanish epilogue_uv_ulya_6aad0d0b:


    uv "La única cosa que recuerdo es que yo estaba aquí. No recuerdo nada más. Como si no hubiera nada. Debo vigilarte... todos tus yo, y existo en todos los campamentos simultáneamente."


translate spanish epilogue_uv_ulya_ffc4f24e:


    "Tras recuperarme de mi conmoción, continué."


translate spanish epilogue_uv_ulya_5561955a:


    me "¿Y no sabes quién eres?"


translate spanish epilogue_uv_ulya_4fa8ffca:


    uv "¡Bingo!"


translate spanish epilogue_uv_ulya_ec2c2a7c:


    "Ella sonrió."


translate spanish epilogue_uv_ulya_a6440285:


    me "¡Pero eso es imposible! Bueno, comprendería si fuera amnesia... Pero todas las criaturas todopoderosas no pueden padecerlo."


translate spanish epilogue_uv_ulya_104bc8ca:


    uv "¿Y quién te dijo que yo fuera todopoderosa?"


translate spanish epilogue_uv_ulya_d4713cb0:


    me "¿No lo eres?"


translate spanish epilogue_uv_ulya_079e4740:


    uv "En realidad soy incapaz de nada. No puedo interferir con el fluir natural de los acontecimientos."


translate spanish epilogue_uv_ulya_ef49cb55:


    me "¿Pero no lo sabes... o lo intuyes?"


translate spanish epilogue_uv_ulya_5659cee4:


    uv "Sé un poquito de algo. Intuyo un poquito de algo..."


translate spanish epilogue_uv_ulya_7530075b:


    me "Y qué pasará... Bueno, ya sabes..."


translate spanish epilogue_uv_ulya_f1760393:


    "No tengo ni idea de cómo formular la pregunta."


translate spanish epilogue_uv_ulya_5dd4b838:


    uv "Nada. Normalmente, transcurren siete días y uno de ti abandona el campamento. Incluso diría que... desaparece. Luego otro nuevo viene y todo se repite... El número de campamentos es constante."


translate spanish epilogue_uv_ulya_9ca0629f:


    me "Hablamos sobre esto anteriormente.{w} Desaparece... ¿dónde?"


translate spanish epilogue_uv_ulya_9abf8151:


    uv "No lo sé."


translate spanish epilogue_uv_ulya_fbf20f76:


    me "¡Eso no puede ser posible! Me has dicho muchas veces que..."


translate spanish epilogue_uv_ulya_65f552e4:


    uv "¿Crees que te estoy ocultando alguna cosa?"


translate spanish epilogue_uv_ulya_5a7a7577:


    me "No."


translate spanish epilogue_uv_ulya_50bd82b0:


    "Dije agotado."


translate spanish epilogue_uv_ulya_aeafe187:


    "Supongo que no debería dudar de sus palabras.{w} Por lo menos por ahora..."


translate spanish epilogue_uv_ulya_c0c55507:


    me "Tal vez no desaparecen, vuelven a repetirlo otra vez..."


translate spanish epilogue_uv_ulya_0c75893a:


    uv "No puedo descartar esa posibilidad, pero no lo creo."


translate spanish epilogue_uv_ulya_231065b3:


    me "¿Y qué me está ocurriendo a mí ahora pues?"


translate spanish epilogue_uv_ulya_bf4facf7:


    uv "Tampoco puedo explicarlo. Pero sé del cierto que esto tenía que suceder. Y esto es algo fuera de lo normal. El curso de los acontecimientos es diferente esta vez."


translate spanish epilogue_uv_ulya_b37bbbdc:


    me "El pionero dijo algo así también..."


translate spanish epilogue_uv_ulya_de6bbaa6:


    uv "Puede que sepa algo más que yo."


translate spanish epilogue_uv_ulya_c9bf7d2f:


    me "¿Pero cómo debería entender todo esto?"


translate spanish epilogue_uv_ulya_e98d5cf2:


    "Suspiré profundamente y me cubrí la cara con mis manos."


translate spanish epilogue_uv_ulya_039b4986:


    uv "Creo que lo entenderemos muy pronto."


translate spanish epilogue_uv_ulya_36f8874d:


    me "Vale... Pero en fin, ¿quién eres pues?"


translate spanish epilogue_uv_ulya_dd718d6c:


    "Yulya hizo un puchero con sus labios."


translate spanish epilogue_uv_ulya_e75258a6:


    uv "¡Te lo dije antes!"


translate spanish epilogue_uv_ulya_40dd1aca:


    me "Pues entonces, ¿tienes pérdida de memoria?"


translate spanish epilogue_uv_ulya_710e003a:


    uv "No..."


translate spanish epilogue_uv_ulya_60f3d758_1:


    "Pensó por un instante."


translate spanish epilogue_uv_ulya_012d131e:


    uv "En realidad diría que no tengo recuerdos, probablemente. No había un yo anterior a este campamento... No existía."


translate spanish epilogue_uv_ulya_a337cbd5:


    me "Pero..."


translate spanish epilogue_uv_ulya_cd5a786f:


    "No terminé mi oración."


translate spanish epilogue_uv_ulya_e545f709:


    "Teniendo en cuenta todo lo que ocurre aquí, había mucho de lo que únicamente sólo podía conjeturar."


translate spanish epilogue_uv_ulya_43398793:


    "Y eso significa que no debería sorprenderme por una muchacha con orejas de animal."


translate spanish epilogue_uv_ulya_fd6b4e40:


    me "¿Eres algo así como un observador aquí?"


translate spanish epilogue_uv_ulya_23eb114f:


    uv "Probablemente...{w} Creo que esa palabra es la más adecuada."


translate spanish epilogue_uv_ulya_55692694:


    me "¿Y qué hay de todos los lugareños que desaparecieron tras realizar su papel?"


translate spanish epilogue_uv_ulya_f9e487df:


    uv "¿De qué estás hablando?"


translate spanish epilogue_uv_ulya_2a654aba:


    me "El pionero dijo eso..."


translate spanish epilogue_uv_ulya_645769e4:


    uv "Papeles..."


translate spanish epilogue_uv_ulya_f5cac582:


    "Ella agitó sus orejas de una forma graciosa, haciéndome sonreír."


translate spanish epilogue_uv_ulya_73acaf44:


    uv "Hay algo en ello. Porque el único real aquí eres tú... Quiero decir tú, ese pionero y los otros tú."


translate spanish epilogue_uv_ulya_1b32cfea:


    me "¿Cómo es eso?"


translate spanish epilogue_uv_ulya_e2e98778:


    uv "Te lo dije... ¡No lo sé! Puedes aceptar que aparecí aquí con un conocimiento limitado, nada más. Hay cosas de las que no estoy segura, y otras cosas de las que no sé mucho más que tú."


translate spanish epilogue_uv_ulya_505d91ae:


    me "Está bien..."


translate spanish epilogue_uv_ulya_2520f29d:


    "Por una parte, sus palabras parecían bastante razonables."


translate spanish epilogue_uv_ulya_7a5d2ca6:


    "Si no pudiera hablar con lógica en dicha situación..."


translate spanish epilogue_uv_ulya_644ea9c2:


    me "¿Y qué sugieres hacer ahora?"


translate spanish epilogue_uv_ulya_d348b8ed:


    uv "Esperar y ver cómo las cosas se desarrollan."


translate spanish epilogue_uv_ulya_009e7e71:


    me "Sí, no puedes cambiar nada..."


translate spanish epilogue_uv_ulya_3e1d421a:


    "Murmuré."


translate spanish epilogue_uv_ulya_c6510216:


    uv "De todas formas, ¡es la hora cenar!"


translate spanish epilogue_uv_ulya_e2e98a90:


    "Ella sonrió alegremente y agitó su cola."


translate spanish epilogue_uv_ulya_9a80cd18:


    me "¿No fuiste a buscar alguna cosa...?"


translate spanish epilogue_uv_ulya_8634c528:


    uv "¡No encontré nada!{w} No tuve tiempo, vi un relámpago y volví de inmediato."


translate spanish epilogue_uv_ulya_fe33debe:


    uv "¡Vamos!"


translate spanish epilogue_uv_ulya_badab978:


    "Se fue en dirección a la cantina."


translate spanish epilogue_uv_ulya_5d070f50:


    "Mejor que se quedara a mi lado."


translate spanish epilogue_uv_ulya_7f3217c7:


    "Puede que no hubiera ni un solo pionero..."


translate spanish epilogue_uv_ulya_9fbc5272:


    "Si no fuera el único que pudiera verle, pues entonces tendría menos motivos para dudar de mi cordura, la cual no estaba muy segura últimamente."


translate spanish epilogue_uv_ulya_ce617998_2:


    "..."


translate spanish epilogue_uv_ulya_d74a75da:


    "Yulya se sentó en una de las mesas y me miró."


translate spanish epilogue_uv_ulya_3513c03c:


    uv "¿Y bien?"


translate spanish epilogue_uv_ulya_ad02f53a:


    me "¿Qué?"


translate spanish epilogue_uv_ulya_fe7e1a41:


    uv "La cena."


translate spanish epilogue_uv_ulya_9de3289a:


    me "Sí, ¿qué pasa con ella?"


translate spanish epilogue_uv_ulya_9a2e9e53:


    "Bostecé.{w} Era más de medianoche."


translate spanish epilogue_uv_ulya_78d7a896:


    me "Por cierto, ¿cómo abriste la cantina?"


translate spanish epilogue_uv_ulya_195ecce2:


    uv "¡Tienes que tener habilidad!"


translate spanish epilogue_uv_ulya_1812f423:


    "Mejor no continuar con este tema."


translate spanish epilogue_uv_ulya_53b7f24a:


    me "¿Quizás es mejor irse a dormir?"


translate spanish epilogue_uv_ulya_11e4ef3b:


    uv "Cocina. Ahora."


translate spanish epilogue_uv_ulya_01ef5a6e:


    "Me dijo con voz dictatorial."


translate spanish epilogue_uv_ulya_de5dd677:


    me "¿Yo?"


translate spanish epilogue_uv_ulya_d7494819:


    uv "No veo a nadie más."


translate spanish epilogue_uv_ulya_c530b477:


    me "Va... vale..."


translate spanish epilogue_uv_ulya_17968fb5:


    "Lentamente caminé hacia la cocina."


translate spanish epilogue_uv_ulya_f36628c2:


    "A causa de hablar de comer de repente sentí hambre."


translate spanish epilogue_uv_ulya_30f02899:


    "Encontré un paquete de huevos en una nevera y llamé a Yulya:"


translate spanish epilogue_uv_ulya_8908e489:


    me "Ey, ¿que tal unos huevos revueltos?"


translate spanish epilogue_uv_ulya_fd28a4ac:


    uv "Bueno... quizá."


translate spanish epilogue_uv_ulya_da13cf98:


    "Empecé a cocinar."


translate spanish epilogue_uv_ulya_cba09001:


    "Qué gracia cómo se desarrollaban las cosas..."


translate spanish epilogue_uv_ulya_1b2b5e44:


    "Acontecimientos extraños e incomprensibles me están sucediendo y, aun así, aquí estoy haciendo la cena.{w} Deseando lo mejor."


translate spanish epilogue_uv_ulya_3e302763:


    "Típica característica rusa, sí..."


translate spanish epilogue_uv_ulya_2e254a1c:


    "No obstante, estoy bastante seguro de que nada malo ocurrirá."


translate spanish epilogue_uv_ulya_05c7e10a:


    "Y en caso de problemas inesperados, Yulya me protegerá."


translate spanish epilogue_uv_ulya_ee05990f:


    "Aunque ella diga que no sabe mucho, en un momento crítico puedo realmente confiar en ella.{w} Sencillamente no hay nadie más con quien contar."


translate spanish epilogue_uv_ulya_cd9b2003:


    "Justo ayer nada dependía de mí, pero al fin tuve una mínima idea de qué estaba sucediendo. Sin embargo, ahora..."


translate spanish epilogue_uv_ulya_8445a01a:


    "La única cosa que quedaba por hacer era ponerse cómodo en una silla y esperar a que el espectáculo finalizara."


translate spanish epilogue_uv_ulya_4559a5ad:


    "Añadí queso rayado, mayonesa, sal y pimienta, y vertí la bien mezclada masa en una cazuela."


translate spanish epilogue_uv_ulya_db55b061:


    "También encontré un salami en la nevera y una hogaza de pan en la estantería próxima."


translate spanish epilogue_uv_ulya_360e960d:


    "Pronto estuvimos sentados en silencio, comiendo los huevos revueltos."


translate spanish epilogue_uv_ulya_3d29c745:


    uv "¡Rico, rico!"


translate spanish epilogue_uv_ulya_09de456b:


    me "No es nada especial... Cena normal y corriente de soltero."


translate spanish epilogue_uv_ulya_f569df04:


    uv "Nunca probé nada como esto."


translate spanish epilogue_uv_ulya_666aed83:


    me "¿Probablemente mejor que las setas y las nueces?"


translate spanish epilogue_uv_ulya_7e790588:


    uv "Ya basta."


translate spanish epilogue_uv_ulya_6b572d3d:


    "Yulya contestó enojada."


translate spanish epilogue_uv_ulya_5ffa82a0:


    "Cuando terminamos de cenar dije:"


translate spanish epilogue_uv_ulya_9e70d737:


    me "Cuéntame algo sobre ti... A pesar de que... probablemente no tengas nada que contar."


translate spanish epilogue_uv_ulya_2661c728:


    uv "Probablemente."


translate spanish epilogue_uv_ulya_8b90ba81:


    me "¿Cuánto tiempo llevas aquí?"


translate spanish epilogue_uv_ulya_e0e4e466:


    uv "No lo sé. Mucho tiempo..."


translate spanish epilogue_uv_ulya_16b7a3e5:


    me "¿Pero eres casi humana?"


translate spanish epilogue_uv_ulya_0db604b4:


    uv "Bueno, ¡seguramente!"


translate spanish epilogue_uv_ulya_47949dcf:


    me "¿Entonces cómo puedes estar en diversos lugares a la vez...?"


translate spanish epilogue_uv_ulya_f1ee5506:


    uv "Es difícil de explicar, ni yo misma lo entiendo muy bien. Asumamos que aquí y ahora frente a ti sólo soy la única."


translate spanish epilogue_uv_ulya_ba6a7718:


    "Podía aceptar tal supuesto."


translate spanish epilogue_uv_ulya_57f0d6d3:


    "Al final, aunque el hecho de que Yulya tuviera muchas caras era solamente un misterio más entre muchos otros que ocurrían aquí, no era desde luego el más importante."


translate spanish epilogue_uv_ulya_eef3f95b:


    "Al menos eso era lo que pensaba."


translate spanish epilogue_uv_ulya_7b8d131e:


    me "Bueno, asumamos eso... ¿Y qué conoces del mundo?"


translate spanish epilogue_uv_ulya_22fde84e:


    uv "Puedo leer y escribir, si eso es lo que quieres decir."


translate spanish epilogue_uv_ulya_ec2c2a7c_1:


    "Ella se rio."


translate spanish epilogue_uv_ulya_9c18fd82:


    me "No, no eso exactamente. ¿Sabes dónde está localizado este campamento?"


translate spanish epilogue_uv_ulya_710e003a_1:


    uv "No..."


translate spanish epilogue_uv_ulya_b8b45cc7:


    me "Entonces, tal vez sepas que yo no soy de aquí... no de esta época... no de esta realidad."


translate spanish epilogue_uv_ulya_f754bafd:


    uv "Bueno, lo sé, pero no todos los detalles."


translate spanish epilogue_uv_ulya_a05ef932:


    me "Y si te pregunto qué año es ahora..."


translate spanish epilogue_uv_ulya_35d6ab99:


    uv "No puedo responder."


translate spanish epilogue_uv_ulya_a2964860:


    "Di una profunda respiración y miré fijamente a través de la ventana."


translate spanish epilogue_uv_ulya_df22040a:


    uv "Por lo general, puedes aceptar que hay cosas que sé mejor que tú, y que hay otras que sé menos..."


translate spanish epilogue_uv_ulya_8adc0e91:


    me "¿Qué deberíamos hacer?"


translate spanish epilogue_uv_ulya_b2dad4f2:


    uv "Tal como te dije antes... solamente esperar."


translate spanish epilogue_uv_ulya_d5ef5aac:


    me "Esperar... ¿Y qué vamos a hacer?"


translate spanish epilogue_uv_ulya_045a7a56:


    uv "¿Qué es lo que quieres hacer?"


translate spanish epilogue_uv_ulya_89b0220f:


    me "No lo sé... Pero discúlpame, no me siento como para trepar árboles."


translate spanish epilogue_uv_ulya_f8b7d908:


    uv "Vale, vale. ¿Entonces tal vez me explicarás algo sobre ti?"


translate spanish epilogue_uv_ulya_256129cd:


    me "Ahora no."


translate spanish epilogue_uv_ulya_7c1ea423:


    uv "Como desees... ¿Pues entonces qué?"


translate spanish epilogue_uv_ulya_6ae279d3:


    me "No lo sé. Tal vez sólo estar sentados y esperar. No me importa."


translate spanish epilogue_uv_ulya_64a7a78d:


    uv "¡Pero eso es muy aburrido!"


translate spanish epilogue_uv_ulya_fc71db18:


    me "No veo otra alternativa."


translate spanish epilogue_uv_ulya_305b323e:


    uv "Pues vámonos... venga..."


translate spanish epilogue_uv_ulya_60f3d758_2:


    "Ella pensó por un rato."


translate spanish epilogue_uv_ulya_088f9ff2:


    "Miré con entusiasmo a Yulya."


translate spanish epilogue_uv_ulya_d8abb286:


    "Es como si ella realmente encontrase que todo lo que aconteciera fuera divertido."


translate spanish epilogue_uv_ulya_3de491d4:


    "Además, la situación actual no le resultaba extraña."


translate spanish epilogue_uv_ulya_2d96fef7:


    "De acuerdo con ella, no había nada de anómalo en ello."


translate spanish epilogue_uv_ulya_f43abfa2:


    me "Sabes, todo lo que ocurre no me parece bastante normal. Incluso no comprendo el porqué no me hallo oculto en algún lugar bajo mi cama, temblando con cada ruido."


translate spanish epilogue_uv_ulya_2e5a2d95:


    uv "Resulta que eres valiente."


translate spanish epilogue_uv_ulya_6b9955f7:


    "No dije nada salvo sonreír meramente."


translate spanish epilogue_uv_ulya_cf3d8309:


    uv "¡Tengo una idea!"


translate spanish epilogue_uv_ulya_8f838576_1:


    "La miré con interés."


translate spanish epilogue_uv_ulya_83a6f072:


    uv "No, la olvidé otra vez..."


translate spanish epilogue_uv_ulya_802fc6e1:


    "Yulya se rascó sus orejas de gata."


translate spanish epilogue_uv_ulya_9b21e657:


    uv "¡Ah! ¡Ahora me acuerdo! ¡Vayámonos a nadar!"


translate spanish epilogue_uv_ulya_e229c5a7:


    "Ahora la miraba sin dar crédito."


translate spanish epilogue_uv_ulya_1c6063a9:


    me "¿Quizás no sea mejor irse a dormir?"


translate spanish epilogue_uv_ulya_4ef6df25:


    uv "¡Oh, venga ya!"


translate spanish epilogue_uv_ulya_a03f69df:


    "Ella hizo un puchero."


translate spanish epilogue_uv_ulya_b5792957:


    "Desde luego, el día había sido caluroso y estaba empapado en sudor. Es más fácil caer dormido con este calor cuando estás limpio."


translate spanish epilogue_uv_ulya_304c9144:


    me "Vale. Vamos."


translate spanish epilogue_uv_ulya_ce617998_3:


    "..."


translate spanish epilogue_uv_ulya_645846e3:


    "Pocos minutos después estábamos en la playa."


translate spanish epilogue_uv_ulya_7e0cced3:


    "Me estiré perezosamente en la arena y miré hacia el río."


translate spanish epilogue_uv_ulya_a1e9fb5f:


    "Inmediatamente me sentí completamente exhausto."


translate spanish epilogue_uv_ulya_c42573a4:


    me "Sabes, en realidad no me gusta nadar."


translate spanish epilogue_uv_ulya_1b88e3de:


    uv "¿Quieres decir que no sabes nadar?"


translate spanish epilogue_uv_ulya_de0a7798:


    "Yulya se sentó a mi lado."


translate spanish epilogue_uv_ulya_5ea21bc9:


    me "Bueno, no es que no sepa...{w}. ¡De todas formas eso no importa! Los pioneros desaparecieron... bueno, pero ¿cómo podría desaparecer una ciudad entera?"


translate spanish epilogue_uv_ulya_cee1d719:


    uv "Te lo dije antes... No lo sé.{w} ¡Vayamos a nadar!"


translate spanish epilogue_uv_ulya_58f195ab:


    me "No quiero..."


translate spanish epilogue_uv_ulya_c07212df:


    "Nunca me gustó demostrar que era un pésimo nadador."


translate spanish epilogue_uv_ulya_356ba22a:


    uv "¡Oh, venga ya! ¡Para! ¡Puedes darte un chapuzón en el agua cerca de la orilla!"


translate spanish epilogue_uv_ulya_d577923b:


    me "Vale... Espera, voy a buscar mi bañador."


translate spanish epilogue_uv_ulya_066199aa:


    "Pero Yulya no pareció escucharme y...{w} comenzó a desnudarse."


translate spanish epilogue_uv_ulya_8f7a778e:


    me "Ehhm. Tú... ehhh..."


translate spanish epilogue_uv_ulya_8f6b1a03_1:


    uv "¿Qué?"


translate spanish epilogue_uv_ulya_62b43e76:


    me "¿Vas a nadar así?"


translate spanish epilogue_uv_ulya_4e4ac7db:


    uv "Sí, ¿qué pasa?"


translate spanish epilogue_uv_ulya_2be3a365:


    "Me giré a tiempo."


translate spanish epilogue_uv_ulya_66bb80fd:


    me "¿Es que no tienes vergüenza?"


translate spanish epilogue_uv_ulya_addfa65b:


    "Traté de hablar tan relajadamente como pudiera, pero el motivo por el que no la miré era porque mi rostro estaba ardiendo, más que por cortesía."


translate spanish epilogue_uv_ulya_2a7fa430:


    uv "¿Vergüenza de quién? ¿De ti?"


translate spanish epilogue_uv_ulya_ec2c2a7c_2:


    "Ella se rio."


translate spanish epilogue_uv_ulya_5187f093:


    me "¡Ya sabes qué!"


translate spanish epilogue_uv_ulya_b82d4c86:


    "Me di la vuelta. Yulya ya estaba en el agua hasta el cuello."


translate spanish epilogue_uv_ulya_059a514d:


    uv "¡Ven aquí!"


translate spanish epilogue_uv_ulya_eb9cb597:


    "Pensé que nada malo iba a acontecer si me quedaba con la ropa interior puesta. Me desnudé y entré en el agua."


translate spanish epilogue_uv_ulya_1c12c14a:


    me "Brrr... ¡Está fría!"


translate spanish epilogue_uv_ulya_6252b6e3:


    uv "¡Muévete un poco y entrarás en calor! ¡Como yo!"


translate spanish epilogue_uv_ulya_831ca62f:


    "Ella comenzó a saltar y mover sus manos entre el agua."


translate spanish epilogue_uv_ulya_12554f6d:


    "Todavía me sentía incómodo."


translate spanish epilogue_uv_ulya_21c8990f:


    uv "Te has puesto rojo."


translate spanish epilogue_uv_ulya_85e87f2d:


    "Dijo Yulya excitada."


translate spanish epilogue_uv_ulya_c105b60f:


    me "Por supuesto que lo estoy. Tengo a mi lado una muchacha desnuda... o por lo menos una humanoide hembra."


translate spanish epilogue_uv_ulya_cc8ea77e:


    uv "¿Y?"


translate spanish epilogue_uv_ulya_36ed3616:


    me "¡Ya basta!"


translate spanish epilogue_uv_ulya_8e053d85:


    "Me di la vuelta y, en el mismo instante, sentí como Yulya me abrazaba."


translate spanish epilogue_uv_ulya_846952dc:


    me "Esto... ¿forma parte también de tus planes?"


translate spanish epilogue_uv_ulya_7b9304db:


    "Comprendí que estaba diciendo tonterías, pero no me vino nada a la cabeza que tuviera significado."


translate spanish epilogue_uv_ulya_8f6b1a03_2:


    uv "¿Qué?"


translate spanish epilogue_uv_ulya_00ee5007:


    me "Bueno es que... sabes..."


translate spanish epilogue_uv_ulya_0a634eed:


    "Repentinamente, me di cuenta de que detrás de mí me estaba abrazando una criatura omnipotente desnuda, la cual era capaz de existir en múltiples realidades."


translate spanish epilogue_uv_ulya_ef505b03:


    "Mi sesos al instante estaban sobresaturados por errores debido a la falta de recursos del sistema. Y ahí va... un pantallazo azul de la muerte."


translate spanish epilogue_uv_ulya_d4c218f1:


    "Mis piernas cedían, y empecé a hundirme bajo el agua.{w} Yulya me sostuvo."


translate spanish epilogue_uv_ulya_7035f158:


    uv "¡No hay necesidad de que te me ahogues!"


translate spanish epilogue_uv_ulya_51d45ef4:


    "Ella continuaba abrazándome.{w} Le sentí estrechándome más fuerte..."


translate spanish epilogue_uv_ulya_91df6206:


    me "¿Qué vas a hacer?"


translate spanish epilogue_uv_ulya_be5ea40d:


    uv "¡Nadar, pues claro!"


translate spanish epilogue_uv_ulya_2dc1a8de:


    me "¿Entonces quieres decir que no estás proponiendo nada?"


translate spanish epilogue_uv_ulya_7e490fd7:


    "Traté de decirlo con una expresión relajada, pero no funcionó muy bien."


translate spanish epilogue_uv_ulya_69d9c023:


    uv "Por ejemplo, ¿como qué?"


translate spanish epilogue_uv_ulya_39165438:


    me "Bueno... a pesar de que no recuerdes mucho, esencialmente te comportas bastante razonablemente...{w} Así que... deberías entenderlo."


translate spanish epilogue_uv_ulya_1c394603:


    uv "Bueno, no sé..."


translate spanish epilogue_uv_ulya_54a2bc36:


    "Dijo astutamente."


translate spanish epilogue_uv_ulya_1bdbd74a:


    "De repente sentí alguna cosa retorciéndose alrededor de mis muslos."


translate spanish epilogue_uv_ulya_9ff69ab7:


    "Traté de quitármelo de encima, pero Yulya me sostuvo prietamente."


translate spanish epilogue_uv_ulya_055bd048:


    "Además, las físicas de los movimientos en el agua eran muy diferentes en tierra."


translate spanish epilogue_uv_ulya_c7494045:


    uv "¡Tan sólo es mi cola!"


translate spanish epilogue_uv_ulya_1efb85e5:


    me "Una cola... ¿No te parece nada raro que tengas una?"


translate spanish epilogue_uv_ulya_18295dff:


    uv "¿Qué, no te gusta mi cola? ¡Si me pisas la cola, entonces acabarás haciéndote daño!"


translate spanish epilogue_uv_ulya_aff91d3f:


    me "Espera un segundo..."


translate spanish epilogue_uv_ulya_f608ae54:


    "Me detuve por un instante, tratando de componer la oración que justo había pensado."


translate spanish epilogue_uv_ulya_df14bfd6:


    me "¿Así que has visto ese anime también?"


translate spanish epilogue_uv_ulya_3ec3958b:


    uv "¿Qué anime?"


translate spanish epilogue_uv_ulya_1c1b1ab5:


    me "Ese que has citado."


translate spanish epilogue_uv_ulya_e2b99f78:


    uv "Sólo es..."


translate spanish epilogue_uv_ulya_c12e5015:


    "De repente ella se detuvo y reflexionó."


translate spanish epilogue_uv_ulya_04b40d70:


    uv "¿No dice eso la gente...?{w} Algún tipo de dicho."


translate spanish epilogue_uv_ulya_7b85edf4:


    me "Así que no sabes de dónde viene..."


translate spanish epilogue_uv_ulya_ec8b60db:


    uv "No... ¡Y a quién le importa!"


translate spanish epilogue_uv_ulya_fdd16430:


    "Yulya se rio."


translate spanish epilogue_uv_ulya_31308d65:


    "Su cola continuaba indagando mi cuerpo."


translate spanish epilogue_uv_ulya_c62be51e:


    "¿Podría ser que Yulya todavía tuviera recuerdos acerca de su vida anterior al campamento?"


translate spanish epilogue_uv_ulya_1c773453:


    "Quizás ocultaron algo o lo suprimieron."


translate spanish epilogue_uv_ulya_135f2864:


    "¡Ella obtuvo esa cita de alguna parte!"


translate spanish epilogue_uv_ulya_144b5de1:


    "Por supuesto, tal vez ella la habría escuchado de alguno de los pioneros..."


translate spanish epilogue_uv_ulya_8d8c4af3:


    "¿Pero no es algo temprano para que exista dicho manganime?"


translate spanish epilogue_uv_ulya_0191cc8f:


    "La única conclusión posible es que Yulya sea una prisionera en este campamento, igual que yo.{w} Sólo que más misteriosa."


translate spanish epilogue_uv_ulya_89471135:


    "Lo que ocurrió, sucedió por sí mismo."


translate spanish epilogue_uv_ulya_750ac835:


    "Incluso si lo que iba hacer era peligroso, ¡al fin y al cabo soy un hombre de sangre caliente!"


translate spanish epilogue_uv_ulya_3fb50882:


    "En ese momento Yulya no era un ser todopoderoso para mí, tenía entre mis brazos a una muchacha normal y corriente."


translate spanish epilogue_uv_ulya_a20cefa7:


    "..."


translate spanish epilogue_uv_ulya_d70204eb:


    "Llevé la durmiente Yulya a mi cabaña.{w} Era sorprendentemente ligera."


translate spanish epilogue_uv_ulya_a4ad8aa8:


    "La dejé sobre una cama, me derrumbé a su lado y cerré los ojos..."


translate spanish epilogue_uv_ulya_e40d9ea3:


    "Pero todos mis intentos para dormir fueron en vano.{w} Sentí una terrible sed."


translate spanish epilogue_uv_ulya_e74c6a6a:


    "En silencio, con tal de no despertar a Yulya, me levanté de la cama, me puse los pantalones cortos y salí fuera."


translate spanish epilogue_uv_ulya_4e13b8c2:


    "De camino a los lavabos pensé en todo lo que aconteció."


translate spanish epilogue_uv_ulya_1bab0291:


    "No me preocupaba si estaba bien o mal. No, esa no era la cuestión importante."


translate spanish epilogue_uv_ulya_d913a13b:


    "Resolví que no tenía otra opción en aquella situación."


translate spanish epilogue_uv_ulya_edf2e8e3:


    "Lo más importante eran las posibles consecuencias."


translate spanish epilogue_uv_ulya_77a3bfd6:


    "Incluso si Yulya era un poco diferente de mí, podríamos decir que ella estaba más cerca de quien (o que) estaba tras todo esto.{w} Tan sólo porque ella sabe más que yo."


translate spanish epilogue_uv_ulya_ec7e2bac:


    "Lo cual significa que...{w} Además, aceptar ciegamente o extraer conclusiones sobre algo, sigue siendo una tarea difícil en este campamento, de eso estaba seguro por ahora."


translate spanish epilogue_uv_ulya_ce617998_4:


    "..."


translate spanish epilogue_uv_ulya_53310946:


    "Sacié mi sed y me aseé. Iba a volver hacia la cabaña de la líder del campamento, canturreando una canción de «Peer Gynt» de Grieg, cuando alguien me llamó."


translate spanish epilogue_uv_ulya_71c2dd40:


    pi "¿Cómo lo hiciste...?"


translate spanish epilogue_uv_ulya_b03222c9:


    "Di un respingo, me giré y vi la silueta de aquel pionero."


translate spanish epilogue_uv_ulya_5e973abc:


    "No había ni la más mínima oportunidad de distinguir su rostro en esta oscuridad."


translate spanish epilogue_uv_ulya_7b78cd99:


    me "¿Así que eres... tú otra vez?"


translate spanish epilogue_uv_ulya_9837d783:


    pi "¿Quién «yo»? No en el sentido de que yo sea el que hayas visto ya, sino en el sentido de que... ¿quién, de hecho, soy yo?"


translate spanish epilogue_uv_ulya_d3bd0899_1:


    me "No te entiendo."


translate spanish epilogue_uv_ulya_1b8a1e41:


    "Recordé que Yulya me explicó que él no puede hacerme daño físico alguno, así que me tranquilicé un poco."


translate spanish epilogue_uv_ulya_2a3661e7:


    pi "Oh, da igual.{w} A parte de eso, te vi antes cómo te lo pasabas."


translate spanish epilogue_uv_ulya_98007ecd:


    "Eso me puso la piel de gallina."


translate spanish epilogue_uv_ulya_5cfa92d6:


    "No importa cuán fantástica sea una situación, siempre es frustrante cuando te atrapan haciendo algo tan íntimo."


translate spanish epilogue_uv_ulya_73a1fd25:


    me "¿No te han explicado alguna vez que no es cortés espiar a la gente?"


translate spanish epilogue_uv_ulya_77ddb932:


    pi "Estás molesto."


translate spanish epilogue_uv_ulya_c45ede0e:


    me "No. Por qué iba a estarlo..."


translate spanish epilogue_uv_ulya_346c99a6:


    "Resoplé y me medio giré hacia él, tratando en vano de ver su rostro."


translate spanish epilogue_uv_ulya_772467b0:


    pi "¿Y qué te ha contado la de las orejas de gata?"


translate spanish epilogue_uv_ulya_02007f83:


    me "¡Ella tiene un nombre!"


translate spanish epilogue_uv_ulya_e1e64156:


    "Sentía un fuerte vínculo con Yulya, por lo que no dudé en defenderla."


translate spanish epilogue_uv_ulya_cba6f894:


    pi "¿Un nombre? ¿Y cuál es? ¿El todopoderoso omnisciente? ¿Jano el de las dos caras?"


translate spanish epilogue_uv_ulya_25dedf39:


    me "Yulya..."


translate spanish epilogue_uv_ulya_fefef18f:


    "Dije un poco inseguro."


translate spanish epilogue_uv_ulya_cf46188f:


    pi "¡¿Yulya?!"


translate spanish epilogue_uv_ulya_73e6bc8d:


    "Comenzó a reírse como un caballo relinchando."


translate spanish epilogue_uv_ulya_b58f3a60:


    pi "¿Y quién se lo puso? Tú, probablemente."


translate spanish epilogue_uv_ulya_9bfb401d:


    me "Está bien, ¡cuéntame lo que sepas y piérdete!"


translate spanish epilogue_uv_ulya_c16c1f2a:


    pi "Vale, vale, ¡Su Majestad! ¡Tan sólo no te enojes!"


translate spanish epilogue_uv_ulya_dfa62206:


    "Se inclinó torpemente."


translate spanish epilogue_uv_ulya_4e88db3f:


    "La vista era tan grotesca que incluso me dio escalofríos."


translate spanish epilogue_uv_ulya_ba66f329:


    pi "¿Y qué te explicó la llamada Yulya?"


translate spanish epilogue_uv_ulya_2dd45817:


    "No tenía ningún sentido que ocultara la información que descubrí recientemente."


translate spanish epilogue_uv_ulya_a8058b44:


    me "Ella me contó que habían muchos otros campamentos. Que tú eres igual que yo. Y que tras siete días nos vamos y nunca volvemos de nuevo."


translate spanish epilogue_uv_ulya_708e2129:


    "Me era desagradable estar en el mismo barco que él, pero los hechos son los hechos."


translate spanish epilogue_uv_ulya_8d323880:


    pi "Sí, eso es correcto. ¿Y te explicó algo sobre ella?"


translate spanish epilogue_uv_ulya_99d4c76e:


    me "¿Crees que es asunto tuyo? Además, seguro que lo sabrás todo sin mí."


translate spanish epilogue_uv_ulya_23fad338:


    pi "Sí, sé una cosa. Ya lidié con ella antes."


translate spanish epilogue_uv_ulya_afb9b3c2:


    "Su tono era tan desagradable que no quería ni continuar esta conversación."


translate spanish epilogue_uv_ulya_63e70168:


    me "Bueno, si no tienes nada más que..."


translate spanish epilogue_uv_ulya_e52964ff:


    pi "¡No, espera un segundo! No he terminado aun."


translate spanish epilogue_uv_ulya_4b2daec2:


    me "¿Y ahora qué?"


translate spanish epilogue_uv_ulya_c5eeecce:


    pi "¿Quieres que te explique lo que ella te está ocultando?"


translate spanish epilogue_uv_ulya_cd917d11:


    me "Adelante."


translate spanish epilogue_uv_ulya_97dc66db:


    "Estaba seguro de que Yulya no tenía secretos conmigo, pero en mi situación cualquier información podría ser de utilidad."


translate spanish epilogue_uv_ulya_5f0edde5:


    pi "¿«Y nunca volvemos de nuevo»...? No, ¡sí que vuelven! Y tú vuelves... Al menos lo hiciste antes."


translate spanish epilogue_uv_ulya_787e2d21:


    me "No te entiendo."


translate spanish epilogue_uv_ulya_bc5bc614:


    pi "Ya te he dicho que tú ya has estado aquí. Así que tras siete días, da igual si te vas en un autobús o si te duermes en el bosque... volverás a encontrarte sentado en el 410 de nuevo."


translate spanish epilogue_uv_ulya_5c4cab43:


    me "¿Pero cómo...?"


translate spanish epilogue_uv_ulya_666d09fb:


    pi "Ella te explicó que tan sólo desaparecemos, ¿cierto?"


translate spanish epilogue_uv_ulya_7fe9a557:


    "No respondí."


translate spanish epilogue_uv_ulya_de006685:


    pi "Bueno, no puedo estar seguro de si miente. Quizás ella no lo sabe todo, ¡pero lo que dije es verdad! Tú y yo hemos estado aquí muchas veces. Desde luego, yo he estado aquí más veces, pero eso no importa."


translate spanish epilogue_uv_ulya_c310b5dd:


    me "Vale, ¿entonces por qué no me acuerdo de nada?"


translate spanish epilogue_uv_ulya_b00c1e22:


    pi "Te acordarás de todos ellos más tarde. Eso es lo que me ocurrió a mí."


translate spanish epilogue_uv_ulya_70d504c6:


    me "¿Y me estás contando que no hay salida de aquí?"


translate spanish epilogue_uv_ulya_f4dc0758:


    pi "Así es."


translate spanish epilogue_uv_ulya_916a3a3b:


    me "Recuerdo que me dijiste que lo que sucedía hoy no había ocurrido nunca antes, ¿cierto?"


translate spanish epilogue_uv_ulya_8761e86f:


    pi "Es verdad. Ésta es la primera vez."


translate spanish epilogue_uv_ulya_76023e32:


    me "¿Y cómo puedes explicar eso?"


translate spanish epilogue_uv_ulya_86cd0059:


    pi "¡No puedo! ¿Acaso esperas que algo vaya a cambiar? ¡No hay forma!"


translate spanish epilogue_uv_ulya_8d1f0c27:


    me "Eso ya lo veremos."


translate spanish epilogue_uv_ulya_2117dd83:


    "Decididamente me di la vuelta y me alejé caminando."


translate spanish epilogue_uv_ulya_aba701a6:


    "Para mi sorpresa, el pionero no trató tampoco de detenerme."


translate spanish epilogue_uv_ulya_021b03c0:


    "Retorné a la cabaña y me encontré a Yulya, peinándose cuidadosamente su cola."


translate spanish epilogue_uv_ulya_9364ca36:


    uv "Estuviste hablando con él otra vez."


translate spanish epilogue_uv_ulya_e861403d:


    me "¿Cómo lo sabes?"


translate spanish epilogue_uv_ulya_105c8aa8:


    "Por un instante tuve dudas."


translate spanish epilogue_uv_ulya_4632840a:


    uv "Solamente lo deduzco. Él aparece supuestamente cuando estás solo."


translate spanish epilogue_uv_ulya_9513cd87:


    me "Sí..."


translate spanish epilogue_uv_ulya_cc8ea77e_1:


    uv "¿Y?"


translate spanish epilogue_uv_ulya_4d5b668d:


    me "Me contó que nadie desaparece. Que «nosotros» volvemos de nuevo."


translate spanish epilogue_uv_ulya_92ef5f8d:


    "Yulya no me contestó nada."


translate spanish epilogue_uv_ulya_3581e356:


    me "¿Es eso así?"


translate spanish epilogue_uv_ulya_7f260d43:


    uv "A decir verdad, no lo sé..."


translate spanish epilogue_uv_ulya_407f9e91:


    "Me senté en la cama a su lado y empecé a pensar."


translate spanish epilogue_uv_ulya_ce617998_5:


    "..."


translate spanish epilogue_uv_ulya_b838dd19:


    "No sé cuánto tiempo estuvimos sentados ahí, pero pronto comencé a dormirme."


translate spanish epilogue_uv_ulya_c69a0894:


    uv "Vayamos a ver las estrellas."


translate spanish epilogue_uv_ulya_304c9144_1:


    me "Vayámonos."


translate spanish epilogue_uv_ulya_fd40ac42:


    "Quería dormir urgentemente, pero a la vez sentía una sensación de falta de plenitud, así que esperaba que el aire fresco aclararía mis pensamientos."


translate spanish epilogue_uv_ulya_63937d5a:


    "Pocos minutos después estábamos en la plaza."


translate spanish epilogue_uv_ulya_0734e246:


    "Me senté en el banco, y Yulya puso su cabeza en mi regazo y comenzó a mirar fijamente el cielo."


translate spanish epilogue_uv_ulya_5c9504e1:


    uv "¿No es bello?"


translate spanish epilogue_uv_ulya_9513cd87_1:


    me "Lo es..."


translate spanish epilogue_uv_ulya_4900029d:


    "Estuvimos sentados en silencio durante bastante rato."


translate spanish epilogue_uv_ulya_ceb783e7:


    me "¿Y qué nos ocurrirá mañana?"


translate spanish epilogue_uv_ulya_62cb2255:


    uv "¿Qué quieres decir?"


translate spanish epilogue_uv_ulya_ec806ff9:


    me "Bueno, mañana es el séptimo día... Así que... es la hora de partir."


translate spanish epilogue_uv_ulya_22006ea3:


    uv "¿Tú quieres irte?"


translate spanish epilogue_uv_ulya_6343bef6:


    "De repente fui golpeado por la realidad al darme cuenta. Había escuchado antes esa pregunta muchas veces."


translate spanish epilogue_uv_ulya_56494f6b:


    me "De acuerdo contigo y él, incluso aunque no quisiera, debo irme."


translate spanish epilogue_uv_ulya_72be10d8:


    uv "Pues vayámonos."


translate spanish epilogue_uv_ulya_d1046a49:


    "Ella sonrió."


translate spanish epilogue_uv_ulya_dab936c8:


    me "¿Vendrás conmigo?"


translate spanish epilogue_uv_ulya_167bb087:


    uv "¡Por supuesto que sí! ¡Después de todo esto, tienes que casarte conmigo claramente!"


translate spanish epilogue_uv_ulya_ec3468be:


    me "No conozco ni un solo registro civil que acepte registrar bodas con muchachas felinas."


translate spanish epilogue_uv_ulya_158aae48:


    uv "Pues buscaremos uno."


translate spanish epilogue_uv_ulya_39fba389:


    me "Vale, ahora en serio, ¿qué sucederá mañana?"


translate spanish epilogue_uv_ulya_e652c25e:


    uv "Al atardecer el autobús vendrá..."


translate spanish epilogue_uv_ulya_aa9c1d3b:


    me "¿Estás segura de eso?"


translate spanish epilogue_uv_ulya_21e652c5:


    uv "¡Absolutamente segura! Siempre viene."


translate spanish epilogue_uv_ulya_f95c9fc9:


    me "¿Y?"


translate spanish epilogue_uv_ulya_6c27c947:


    uv "Y subiremos."


translate spanish epilogue_uv_ulya_f4eb646d:


    me "¿Y luego?"


translate spanish epilogue_uv_ulya_733740cb:


    uv "¡¿Por qué me estás preguntando todas estas estúpidas preguntas?!"


translate spanish epilogue_uv_ulya_1c70a91b:


    "Protestó Yulya."


translate spanish epilogue_uv_ulya_55b0a935:


    uv "¿Cómo voy a saberlo?"


translate spanish epilogue_uv_ulya_c25ae0cb:


    me "Bueno, ¡haz conjeturas!"


translate spanish epilogue_uv_ulya_a7a02427:


    uv "Y... volveremos a tu mundo de vuelta."


translate spanish epilogue_uv_ulya_1f7d095b:


    me "Eso sería..."


translate spanish epilogue_uv_ulya_2a7a8a3b:


    "Me detuve."


translate spanish epilogue_uv_ulya_f20e460f:


    me "Ya veremos."


translate spanish epilogue_uv_ulya_9da1463d:


    "Suspiré."


translate spanish epilogue_uv_ulya_510df513:


    me "¿Y qué hay de ti y de tus varios yo?"


translate spanish epilogue_uv_ulya_4e31d83e:


    uv "Únicamente me veo tal como {i}aquí{/i}, ¿no? Allí seré igual que siempre."


translate spanish epilogue_uv_ulya_50cdb6bd:


    me "Claro, con orejas de gata y una cola..."


translate spanish epilogue_uv_ulya_6c4c0225:


    "Yulya me dio un golpe doloroso en las costillas."


translate spanish epilogue_uv_ulya_1a723568:


    me "¡Ey! ¡Sólo bromeaba!"


translate spanish epilogue_uv_ulya_cc66d7ef:


    uv "Tal vez sin una cola."


translate spanish epilogue_uv_ulya_2a65d4fd:


    "Dijo con tristeza."


translate spanish epilogue_uv_ulya_a97d3385:


    me "¡No, no! ¡Me gusta! ¡No pienses algo así!"


translate spanish epilogue_uv_ulya_5cd38101:


    "Ella no dijo nada, tan sólo cerró sus ojos."


translate spanish epilogue_uv_ulya_412334c0:


    uv "¿Y cómo es {i}allí{/i}...?"


translate spanish epilogue_uv_ulya_8459d4e8:


    me "Es diferente... esa es la mejor manera de decirlo. Algunas cosas son mejores, otras son peores. Tenemos internet..."


translate spanish epilogue_uv_ulya_a026df63:


    "Hice una risita."


translate spanish epilogue_uv_ulya_1f38f517:


    uv "¿Y qué voy a hacer allí?"


translate spanish epilogue_uv_ulya_59625bdc:


    me "Bueno, no lo sé... Resolvamos los problemas en cuanto los tengamos."


translate spanish epilogue_uv_ulya_4eb5d154:


    uv "Muy bien..."


translate spanish epilogue_uv_ulya_ce617998_6:


    "..."


translate spanish epilogue_uv_ulya_c5c1cbec:


    "Estuvimos sentados por un largo rato, hablando sobre todo tipo cosas absurdas."


translate spanish epilogue_uv_ulya_21be8af1:


    "Finalmente Yulya dijo:"


translate spanish epilogue_uv_ulya_c4f8d6ec:


    uv "¡Vamos a dormir!"


translate spanish epilogue_uv_ulya_805f21bb:


    me "¡Sí, buena idea!"


translate spanish epilogue_uv_ulya_0c0452b3:


    "Sentí una ola de terrible cansancio."


translate spanish epilogue_uv_ulya_43478fd5:


    uv "Mañana será la hora de empacar las cosas."


translate spanish epilogue_uv_ulya_f89ed32e:


    me "Pero no tengo nada que empacar."


translate spanish epilogue_uv_ulya_e5d2ff76:


    uv "Anda qué bien... él no tiene nada que empacar... Pero sabes qué... ¡Yo sí!"


translate spanish epilogue_uv_ulya_e473fa2f:


    me "¿Por ejemplo?"


translate spanish epilogue_uv_ulya_8ffd00b8:


    "Me reí nerviosamente de forma natural."


translate spanish epilogue_uv_ulya_00bce6ba:


    "Sencillamente no me podía imaginar qué tenía que empacar."


translate spanish epilogue_uv_ulya_4716ebb6:


    uv "¿Y qué hay de mis suministros para invierno? ¡Las manzanas! ¡Las nueces! ¡Las setas!"


translate spanish epilogue_uv_ulya_779061ff:


    me "¿De verdad crees que necesitarás todo eso?"


translate spanish epilogue_uv_ulya_bf9129e5:


    uv "¡Por supuesto!"


translate spanish epilogue_uv_ulya_2cb5dbd1:


    "Se puso de pie y me miró severamente."


translate spanish epilogue_uv_ulya_83ff6f89:


    uv "¡Voy a dormir y tú puedes hacer lo que quieras!"


translate spanish epilogue_uv_ulya_dde67e73:


    "Con estas palabras, Yulya se marchó raudamente hacia la cabaña de la líder del campamento."


translate spanish epilogue_uv_ulya_197dfa8a:


    me "Ey, no quise..."


translate spanish epilogue_uv_ulya_a0076c62:


    "La seguí rápidamente."


translate spanish epilogue_uv_ulya_ce617998_7:


    "..."


translate spanish epilogue_uv_ulya_e426ad6a:


    "Tras entrar en la cabaña Yulya se desnudó de inmediato, se escurrió dentro de la cama y se tapó con la sábana hasta la cabeza."


translate spanish epilogue_uv_ulya_beb6a4da:


    me "No quise decir nada de eso... Si quieres recoger tus suministros, pues recojámoslos."


translate spanish epilogue_uv_ulya_cf6c57e8:


    "A fin de cuentas, nos podrían ser útiles, especialmente en mi situación donde nada podía ocurrir."


translate spanish epilogue_uv_ulya_e70508ab:


    uv "¡Sólo estaba bromeando!"


translate spanish epilogue_uv_ulya_122cb80a:


    "Se asomó bajo la sábana y me sacó su lengua."


translate spanish epilogue_uv_ulya_c41d257d:


    uv "Y ahora... ¡a dormir!"


translate spanish epilogue_uv_ulya_54256e31:


    "Ordenó Yulya."


translate spanish epilogue_uv_ulya_b03955ac:


    "No me importó, así que me desnudé, me tumbé a su lado y nos dormimos abrazados juntos..."


translate spanish epilogue_uv_ulya_3b5be134:


    "Tuve un extraño sueño."


translate spanish epilogue_uv_ulya_fe1efafe:


    "No, claro que estar en este campamento era extremadamente raro por sí mismo, incomparable con cualquier otro sueño nocturno."


translate spanish epilogue_uv_ulya_57a861c9:


    "Sin embargo, en este sueño todo parecía tan real que..."


translate spanish epilogue_uv_ulya_6627fa0f:


    "Pero cuando me desperté, ya no podía recordar nada."


translate spanish epilogue_uv_ulya_ecbc37b5:


    "Solamente habían unos recuerdos vagos de una larga carretera de tierra, un antiguo autobús oxidado, unas puertas desvencijadas de un campamento, unas estatuas derrumbadas de pioneros y una plaza abandonada."


translate spanish epilogue_uv_ulya_54f753f7:


    "Corrí a través de Sovyonok a un lado y a otro, como si tratara de buscar a alguien.{w} Pero a quién, seguía siendo un misterio para mí."


translate spanish epilogue_uv_ulya_ef297c74:


    "Cuando me desperté, el sol ya se estaba poniendo."


translate spanish epilogue_uv_ulya_30bf0240:


    "Un brillante rayo de sol estaba iluminando el polvo que flotaba a la deriva en el silencio de la habitación, pintándolo con todos los colores del arco iris."


translate spanish epilogue_uv_ulya_75ec2ab9:


    "Los reflejos en los vidrios tomaban distintos contornos: si miras desde un ángulo sólo es un punto de luz solar, y si miras desde otro es un pirata blandiendo su alfanje."


translate spanish epilogue_uv_ulya_7e0b301d:


    "Me estiré perezosamente y bostecé."


translate spanish epilogue_uv_ulya_5bd995a8:


    "Al darme la vuelta hacia el otro lado vi a Yulya, durmiendo tranquilamente junto a mí."


translate spanish epilogue_uv_ulya_85ea5c99:


    uv "¡Buenos días!"


translate spanish epilogue_uv_ulya_b5c033aa:


    "Dijo Yulya, sin abrir sus ojos."


translate spanish epilogue_uv_ulya_658bead9:


    me "Ya es de día."


translate spanish epilogue_uv_ulya_f7e9404b:


    uv "La mañana comienza cuando te despiertas."


translate spanish epilogue_uv_ulya_4bfd9907:


    me "Hablas de la misma forma que hacía yo antes."


translate spanish epilogue_uv_ulya_4db5c195:


    uv "¿Y bien, cambiaste de idea?"


translate spanish epilogue_uv_ulya_42efed39:


    me "Bueno, no lo sé... Durante la última semana me acostumbré a levantarme por la mañana."


translate spanish epilogue_uv_ulya_837b5c70:


    "Eso no era del todo cierto, porque no siempre me desperté con el amanecer y a menudo me perdí las formaciones."


translate spanish epilogue_uv_ulya_f98565f1:


    uv "¿Quieres decir que cambiaste?"


translate spanish epilogue_uv_ulya_d8e2c47f:


    "Ella hizo una risita nerviosa."


translate spanish epilogue_uv_ulya_3c6b3cb2:


    me "Creo que todo hombre en mi lugar habría cambiado, al menos un poco."


translate spanish epilogue_uv_ulya_07fe3436:


    "Me parece bastante natural."


translate spanish epilogue_uv_ulya_4514ec9d:


    uv "Sí, quizá. No lo discutiré."


translate spanish epilogue_uv_ulya_90c7a728:


    "Un largo silencio siguió."


translate spanish epilogue_uv_ulya_82c716ab:


    "Ese tipo de silencio que no te tortura provocando la necesidad de decir algo con tal de romper esa opresiva incomodidad.{w} Este silencio podía decir mucho más que las palabras."


translate spanish epilogue_uv_ulya_eb401598:


    me "Bueno, ¿partimos hoy?"


translate spanish epilogue_uv_ulya_2fd40082:


    "Pregunté al fin."


translate spanish epilogue_uv_ulya_ef2303bf:


    uv "Eso creo."


translate spanish epilogue_uv_ulya_d68b5157:


    me "¿Crees? ¿Quieres decir que tampoco estás segura?"


translate spanish epilogue_uv_ulya_6510370b:


    uv "No completamente. Esto está ocurriendo por primera vez."


translate spanish epilogue_uv_ulya_8f9e293b:


    me "Te refieres a la desaparición de los pioneros."


translate spanish epilogue_uv_ulya_b6003605:


    uv "No sólo eso..."


translate spanish epilogue_uv_ulya_ec3291c7:


    "Salió fuera de la cama y empezó a vestirse."


translate spanish epilogue_uv_ulya_0fbf0070:


    "La miré fascinado, incapaz de apartar la mirada."


translate spanish epilogue_uv_ulya_836b34d3:


    uv "¿Qué estás mirando? ¡Levántate! ¡Es la hora de empacar!"


translate spanish epilogue_uv_ulya_2c312632:


    me "¿Quieres decir tus frutas del bosque y tus setas?"


translate spanish epilogue_uv_ulya_bf9129e5_1:


    uv "¡Por supuesto!"


translate spanish epilogue_uv_ulya_6935bf62:


    "Yulya se puso con los brazos en jarras y me miró con resentimiento."


translate spanish epilogue_uv_ulya_1bc3348a:


    uv "¿Es que quieres que pierda todos mis suministros?"


translate spanish epilogue_uv_ulya_f7e824d2:


    me "Desde luego que no..."


translate spanish epilogue_uv_ulya_acb58b15:


    "A decir verdad, realmente me importaban un pimiento."


translate spanish epilogue_uv_ulya_e0aa82ad:


    "Para ser más exactos, simplemente no le encuentro sentido almacenar alimentos para un viaje de un día.{w} Al menos, claramente, sin billete de vuelta."


translate spanish epilogue_uv_ulya_ce617998_8:


    "..."


translate spanish epilogue_uv_ulya_cd0c429e:


    "Pocos minutos más tarde, estábamos en el mismo claro del bosque, allí donde el anterior día Yulya estuvo esparciendo azúcar en una seta."


translate spanish epilogue_uv_ulya_2dad5b75:


    "Fue tras un árbol, apartó las hojas y extrajo un pesado saco."


translate spanish epilogue_uv_ulya_f3f023d1:


    me "Es todo esto..."


translate spanish epilogue_uv_ulya_0877fc5a:


    uv "No es todo. Esto es solamente lo que es necesario."


translate spanish epilogue_uv_ulya_6e778374:


    "No sabía cómo decir lo que estaba pensando de una manera agradable, así que escogí no decir nada."


translate spanish epilogue_uv_ulya_e2dea26f:


    "El saco estaba lleno de setas, frutas del bosque, manzanas, nueces... todo mezclado."


translate spanish epilogue_uv_ulya_a3d7b55b:


    "Inmediatamente tuve serias dudas de su idoneidad para consumo humano de semejante surtido."


translate spanish epilogue_uv_ulya_d9056b1a:


    uv "Agárralo."


translate spanish epilogue_uv_ulya_1331401f:


    "Dijo Yulya, señalando el saco con una relajada sonrisa."


translate spanish epilogue_uv_ulya_59d43f12:


    me "Bueno, espero que entiendas que no seré capaz de..."


translate spanish epilogue_uv_ulya_ece439a7:


    uv "No, no, tú eres fuerte. ¡Mira que inmenso eres!"


translate spanish epilogue_uv_ulya_7430ffbb:


    "Ella empleó sus manos para indicar la diferencia de nuestras estaturas."


translate spanish epilogue_uv_ulya_018dab19:


    "Traté de cargar con el saco.{w} Pesaba al menos treinta kilogramos."


translate spanish epilogue_uv_ulya_3e3eb9ad:


    me "Mira, llevemos solamente unos pocos... no seremos capaces de comernos todo esto igualmente."


translate spanish epilogue_uv_ulya_dbcde99e:


    "Intentaba hacer trampa."


translate spanish epilogue_uv_ulya_a0fad7c3:


    uv "¿Por qué no? Los inviernos son largos en estas tierras."


translate spanish epilogue_uv_ulya_957d4b84:


    me "¿Cómo podrías saber eso?"


translate spanish epilogue_uv_ulya_c5cc1a96:


    uv "Lo supongo."


translate spanish epilogue_uv_ulya_b07beae2:


    "Traté de hacer la mayor mueca de dolor que pude."


translate spanish epilogue_uv_ulya_1d232d3e:


    me "Puedo lograrlo hasta la plaza como mucho. ¡Ten piedad!"


translate spanish epilogue_uv_ulya_58312911:


    "Yulya reflexionó un poco."


translate spanish epilogue_uv_ulya_fd4177df:


    uv "Vale, ¡pues tira las frutas del bosque!"


translate spanish epilogue_uv_ulya_85411cb9:


    "Qué gran solución... deshacerme de la cosa menos pesada."


translate spanish epilogue_uv_ulya_a6d25ea4:


    "Aun así, no me opuse y metí la mano dentro del saco."


translate spanish epilogue_uv_ulya_eb11115c:


    me "No puedo decir que se haya hecho más ligero."


translate spanish epilogue_uv_ulya_c1d08cf6:


    "Dije cuando terminé con las frutas del bosque."


translate spanish epilogue_uv_ulya_89f72f94:


    uv "¡No quiero oír más!"


translate spanish epilogue_uv_ulya_b51aad30:


    "Suspiré y me hundí impotente en el suelo cerca del saco."


translate spanish epilogue_uv_ulya_6e976e33:


    me "Pues entonces carguemos con él por turnos."


translate spanish epilogue_uv_ulya_eb28f6df:


    uv "¡Pero pesa mucho!"


translate spanish epilogue_uv_ulya_62526242:


    "Exclamó Yulya."


translate spanish epilogue_uv_ulya_75383d27:


    me "Eso es lo que trato de decirte..."


translate spanish epilogue_uv_ulya_2de9ff55:


    uv "Quiero decir que pesa mucho para mí."


translate spanish epilogue_uv_ulya_8894ed1f:


    me "Como si fuera ligero para mí..."


translate spanish epilogue_uv_ulya_f848c25b:


    "Murmuré para mí mismo tratando de evitar que Yulya lo escuchara."


translate spanish epilogue_uv_ulya_cf87a84d:


    uv "¡Es liviano para ti!"


translate spanish epilogue_uv_ulya_545fb987:


    "Me parece que no debería subestimar sus orejas de gata."


translate spanish epilogue_uv_ulya_9744fa26:


    me "Vale, pues volvamos aquí más tarde, ¡por ahora sería bueno que comiéramos algo!"


translate spanish epilogue_uv_ulya_a4e53ca8:


    uv "Bueno, quizá... "


translate spanish epilogue_uv_ulya_de229a07:


    "Sonrió y me ofreció su mano."


translate spanish epilogue_uv_ulya_ce617998_9:


    "..."


translate spanish epilogue_uv_ulya_95c684b4:


    "La cantina olía a patatas fritas."


translate spanish epilogue_uv_ulya_d7b798a9:


    "Yulya daba vueltas a mi alrededor, mientras observaba como cocinaba."


translate spanish epilogue_uv_ulya_eb7752aa:


    "A decir verdad, nunca hallé ningún placer en especial en hacer esto."


translate spanish epilogue_uv_ulya_c91f29dc:


    "Siempre quise empezar a comer inmediatamente, sin esperar a que todos los ingredientes estuvieran preparados, el agua hervir, las albondigas flotar..."


translate spanish epilogue_uv_ulya_1fed802b:


    "Ese es la razón por la cual estoy acostumbrado a comer sándwiches o comida rápida."


translate spanish epilogue_uv_ulya_d48b29e9:


    uv "¡Lo haces muy bien!"


translate spanish epilogue_uv_ulya_4cb72b17:


    me "Te dije que no tiene nada de especial."


translate spanish epilogue_uv_ulya_fec78416:


    uv "Pero... Pero..."


translate spanish epilogue_uv_ulya_576e6d7a:


    "Ella trató de encontrar palabras."


translate spanish epilogue_uv_ulya_f256b99d:


    uv "Pero.. ¡Lo sigues haciendo muy bien de todas maneras!"


translate spanish epilogue_uv_ulya_ff60c702:


    me "Me alegro de que te guste."


translate spanish epilogue_uv_ulya_5e40bb44:


    uv "¡Desde luego que me gusta!"


translate spanish epilogue_uv_ulya_920e0bc4:


    "A juzgar por como Yulya estaba engulliendo las patatas, realmente disfrutaba mi receta básica de cocina."


translate spanish epilogue_uv_ulya_31397184:


    uv "¿Estás preocupado?"


translate spanish epilogue_uv_ulya_482b5f41:


    me "Mastica primero, luego habla."


translate spanish epilogue_uv_ulya_96b2f2bb:


    uv "¡Qué bobo!"


translate spanish epilogue_uv_ulya_9e367860:


    me "Debes comportarte educadamente en sociedad."


translate spanish epilogue_uv_ulya_442d1b23:


    uv "¿Y en qué tipo de sociedad estamos aquí?"


translate spanish epilogue_uv_ulya_13170ccd:


    "Yulya extendió sus manos a los lados y se rio."


translate spanish epilogue_uv_ulya_86f5a471:


    "Eso era verdad, la cantina estaba vacía.{w} Como el resto del campamento."


translate spanish epilogue_uv_ulya_68468e81:


    me "¿Qué crees, a dónde fueron a parar los demás?"


translate spanish epilogue_uv_ulya_61bc63e5:


    uv "Cómo voy a saberlo..."


translate spanish epilogue_uv_ulya_164ed80e:


    "Ella pinchó el último pedazo con su tenedor y, diestramente, lo tiró a su boca."


translate spanish epilogue_uv_ulya_13a24fe6:


    uv "¿Les añoras?"


translate spanish epilogue_uv_ulya_1d8c00d5:


    me "Algo así."


translate spanish epilogue_uv_ulya_8f18ceca:


    uv "¡¿Y yo qué?! ¡Soy mejor que un perro callejero!"


translate spanish epilogue_uv_ulya_21b90d49:


    "Otra cita...{w} No presté mucha atención en esta ocasión."


translate spanish epilogue_uv_ulya_081edf35:


    me "Puesto que eres algún tipo de gata, sin duda tienes tu propias citas, refranes y dichos."


translate spanish epilogue_uv_ulya_1072caf5:


    uv "¿Te sientes solo?"


translate spanish epilogue_uv_ulya_616608b4:


    me "Bueno, no estoy solo aquí."


translate spanish epilogue_uv_ulya_682c5cba:


    uv "Sólo nosotros dos..."


translate spanish epilogue_uv_ulya_9237bb74:


    me "Pero si no hubiera sucedido lo que ocurrió, nosotros tal vez..."


translate spanish epilogue_uv_ulya_3a69bd1e:


    "Me detuve por un instante."


translate spanish epilogue_uv_ulya_aebf8ccd:


    me "Entonces, en otras realidades tú y yo nunca hubiéramos estado..."


translate spanish epilogue_uv_ulya_7a792120:


    uv "¿Juntos?"


translate spanish epilogue_uv_ulya_a0ab92e9:


    "Finalizó Yulya por mí."


translate spanish epilogue_uv_ulya_fc30fd2b_1:


    me "Sí."


translate spanish epilogue_uv_ulya_55d14e50:


    uv "No."


translate spanish epilogue_uv_ulya_4cb51946:


    me "Es extraño."


translate spanish epilogue_uv_ulya_fd80ccaf:


    uv "¿Por qué?"


translate spanish epilogue_uv_ulya_e7a7b1ad:


    me "Bueno, porque hay muchos campamentos, tal como dijiste, y de acuerdo a la teoría de la probabilidad..."


translate spanish epilogue_uv_ulya_c508ce25:


    me "Lo cual significa que todo lo que aconteció durante los últimos dos días es realmente algo excepcional..."


translate spanish epilogue_uv_ulya_ffa95805:


    uv "Lo dices como si fuera malo."


translate spanish epilogue_uv_ulya_f837dd45:


    me "¡Por supuesto que no!"


translate spanish epilogue_uv_ulya_c9b0b0be:


    "Sentí que la sangre se me subía a la cabeza."


translate spanish epilogue_uv_ulya_7651e2ab:


    me "Solamente extraño. Hay muchas cosas extrañas aquí. Ese es el motivo por el cual uso esa palabra tan a menudo. De todas formas, eres quisquillosa."


translate spanish epilogue_uv_ulya_0834f49a:


    uv "¿Quién? ¡¿Yo?! ¡Nunca!"


translate spanish epilogue_uv_ulya_ce3666e5:


    "Se rio."


translate spanish epilogue_uv_ulya_a3c453d6:


    me "¡No, en serio eres quisquillosa! ¡Sobre todo con las palabras!"


translate spanish epilogue_uv_ulya_81f608cc:


    uv "¿Y por qué haría eso?"


translate spanish epilogue_uv_ulya_48537224:


    me "No me lo puedo ni imaginar. Tú lo sabrás mejor que yo."


translate spanish epilogue_uv_ulya_47309df6:


    "Era como si por cada uno de mis argumentos ella ya tuviera una respuesta preparada. Como si supiera por adelantado lo que diría."


translate spanish epilogue_uv_ulya_457518f0:


    me "Olvídalo... ¿Nos vamos yendo?"


translate spanish epilogue_uv_ulya_fd5ba6b4:


    uv "¿Dónde?"


translate spanish epilogue_uv_ulya_3096cd01:


    "Me di cuenta al decirlo que fue un gran error, ahora me arrastraría otra vez con aquel saco."


translate spanish epilogue_uv_ulya_149eedf2:


    me "Bueno... hay bastante tiempo hasta que el autobús llegue. Así que podríamos..."


translate spanish epilogue_uv_ulya_cc52b4f1:


    uv "¡Oh, claro! ¡Los suministros!"


translate spanish epilogue_uv_ulya_a1d07ef9:


    "Me rasqué el mentón y la seguí. ¡Qué fallo!"


translate spanish epilogue_uv_ulya_ce617998_10:


    "..."


translate spanish epilogue_uv_ulya_1c0485da:


    "Cargar con cerca de veinticinco kilogramos no es fácil."


translate spanish epilogue_uv_ulya_6712960c:


    "Incluso en una distancia corta."


translate spanish epilogue_uv_ulya_c17bf6e0:


    "Me detuve cada cincuenta metros a descansar."


translate spanish epilogue_uv_ulya_75fc6814:


    "Yulya me daba prisas y me animaba, pero sólo hacía que las cosas fueran a peor."


translate spanish epilogue_uv_ulya_0e135c3b:


    "No sé si merecería una medalla en alguna competición rara de carga-en-corta-distancia-de-sacos-de-suministros-de-muchacha-felina, pero una hora más tarde el saco fue transportado hasta la parada de autobús."


translate spanish epilogue_uv_ulya_087877b3:


    "Me derrumbé junto a él, exhausto."


translate spanish epilogue_uv_ulya_b0d3f9c0:


    me "Ya está..."


translate spanish epilogue_uv_ulya_e76aa14c:


    uv "¡Eres todo un héroe! Lo ves, ¡no era para tanto!"


translate spanish epilogue_uv_ulya_72d66e35:


    "Mi cuerpo entero me dolía, mis músculos ardían y ríos de sudor fluían por mi frente, molestándome en los ojos."


translate spanish epilogue_uv_ulya_fa5868f8:


    "No sé qué hice para merecerme esto, pero esta competición fue más como unas Olimpiadas para gente con retraso mental... aunque ganara, todavía era un retrasado."


translate spanish epilogue_uv_ulya_fd9fe70f:


    uv "¡Mira! ¡El autobús está llegando!"


translate spanish epilogue_uv_ulya_3d342d5a:


    me "¡¿Qué?!"


translate spanish epilogue_uv_ulya_c00b6b38:


    "Me puse en pie. Mi fatiga se desvaneció al instante."


translate spanish epilogue_uv_ulya_50978573:


    "Efectivamente algo se podía ver de lejos."


translate spanish epilogue_uv_ulya_b941dcba:


    "Forcé mis ojos y vi un Icarus."


translate spanish epilogue_uv_ulya_bedf97cb:


    "¿Pero cómo?"


translate spanish epilogue_uv_ulya_42a3ce3a:


    "Yulya me dijo que llegaría casi por la noche."


translate spanish epilogue_uv_ulya_eb16fe39:


    "Además, me esperaba un autobús LiAZ."


translate spanish epilogue_uv_ulya_ce26c4df:


    "Pronto el autobús se detuvo frente a nosotros y la puerta se abrió, invitándonos a entrar."


translate spanish epilogue_uv_ulya_6de31db1:


    "El asiento del conductor estaba vacío, pero realmente no me sorprendía. Cosas más extrañas habían ocurrido aquí."


translate spanish epilogue_uv_ulya_13faf4dc:


    uv "¿Vienes?"


translate spanish epilogue_uv_ulya_3a532c56:


    "Yulya saltó fácilmente en el primer escalón y me hizo un gesto para que la siguiera."


translate spanish epilogue_uv_ulya_a7b876ab:


    me "¿No te parece raro que los autobuses vayan solos por sí mismos?"


translate spanish epilogue_uv_ulya_943ed485:


    uv "Bueno..."


translate spanish epilogue_uv_ulya_60f3d758_3:


    "Ella reflexionó sobre eso."


translate spanish epilogue_uv_ulya_0375339e:


    uv "Desde luego, es extraño, ¡pero tú mismo dijiste que hay montones de cosas extrañas aquí!"


translate spanish epilogue_uv_ulya_b1d8564c:


    "Sí, tenía razón en eso."


translate spanish epilogue_uv_ulya_8dec57e5:


    "De todas formas, no tenía elección."


translate spanish epilogue_uv_ulya_6d318d6a:


    "En cualquier caso, tanto si me quedaba como si me iba con el autobús, no sabía con certeza qué sería lo siguiente."


translate spanish epilogue_uv_ulya_e63e6485:


    "Pero en el segundo caso, por lo menos tenía la ilusión y esperanza de escapar de este campamento."


translate spanish epilogue_uv_ulya_db41b274:


    "Suspiré y, con el último esfuerzo, cargué con el saco hasta el interior del autobús."


translate spanish epilogue_uv_ulya_ce617998_11:


    "..."


translate spanish epilogue_uv_ulya_e6421b37:


    "Estuvimos viajando alrededor de una hora."


translate spanish epilogue_uv_ulya_43dae48d:


    "Yulya estaba constantemente charlando sobre alguna cosa, pero yo no la escuchaba."


translate spanish epilogue_uv_ulya_ca1945f3:


    "Con una mirada estudiaba cuidadosamente los panoramas que pasaban a través de las ventanas.{w} Los bosques, los campos, el río, más bosques, más campos... Nada absolutamente sospechoso."


translate spanish epilogue_uv_ulya_f8021fd2:


    "Por un lado, esto me daba algo de esperanza, pero por el otro lado era señal de que al final nada cambiaría, porque en este mundo todo parece ir lento y calculado, siguiendo sus asentadas reglas."


translate spanish epilogue_uv_ulya_302508d2:


    "Al menos así era hace pocos días."


translate spanish epilogue_uv_ulya_a66cfed7:


    "Aunque quizá no debería sacar conclusiones habiendo estado solamente una semana..."


translate spanish epilogue_uv_ulya_ec0eddf1:


    uv "¡Bueno! ¡Ahora estoy segura de que todo irá bien!"


translate spanish epilogue_uv_ulya_8f838576_2:


    "La miré atentamente."


translate spanish epilogue_uv_ulya_6a4e1b53:


    "Orejas de gata, una larga cola que agitaba rítmicamente a un lado y a otro, y colmillos ligeramente más largos. Yulya parecía un personaje de un cuento de hadas."


translate spanish epilogue_uv_ulya_ad15388e:


    "Ya decidiremos qué haces con ella cuando estemos en el mundo real más tarde."


translate spanish epilogue_uv_ulya_1eeff60a:


    "Me estaba convirtiendo en una gran vena palpitante, a punto de estallar en cualquier instante."


translate spanish epilogue_uv_ulya_a487c0e8:


    "Estaba esperando...{w} aguardando en cómo iba a terminar esto."


translate spanish epilogue_uv_ulya_8bce2f18:


    "El resultado final podía ser cualquier cosa. Para mí lo importante era no volverme loco antes de tiempo."


translate spanish epilogue_uv_ulya_2cfc3b5a:


    "Mis nervios estaban a flor de piel hasta el punto en que traté de no pensar en nada más."


translate spanish epilogue_uv_ulya_61cf1417:


    "Pero era imposible distraerme."


translate spanish epilogue_uv_ulya_8bb8720a:


    uv "¡Ey! ¿Me estás escuchando?"


translate spanish epilogue_uv_ulya_7401b3c5:


    me "¿Eh? No, disculpa, estaba sumido en pensamientos..."


translate spanish epilogue_uv_ulya_da7fd880:


    uv "Bueno, ¡como siempre!"


translate spanish epilogue_uv_ulya_366345bd:


    "Ella hizo un puchero."


translate spanish epilogue_uv_ulya_e11c2266:


    me "Es que ahora es un momento muy importante, sabes... Podemos escapar... escapar de este campamento. Así que es un momento muy importante para mí."


translate spanish epilogue_uv_ulya_b2811648:


    uv "Bueno, también es importante para mí, pero no estoy preocupada."


translate spanish epilogue_uv_ulya_d44a746c:


    me "Es que simplemente no puedes ver esto en contexto."


translate spanish epilogue_uv_ulya_6afd4281:


    uv "¿Qué quieres decir?"


translate spanish epilogue_uv_ulya_b9ed7480:


    me "Antes vivía en un mundo diferente... el verdadero, aparentemente. Así que tengo algo con lo que comparar este campamento. Y tú, digamos, que naciste aquí."


translate spanish epilogue_uv_ulya_f5055106:


    uv "¡Yo no nací ahí!"


translate spanish epilogue_uv_ulya_cfc51cbb:


    me "¿Cómo lo sabes?"


translate spanish epilogue_uv_ulya_3a5cf8cd:


    uv "¡Simplemente lo sé! Lo sé..."


translate spanish epilogue_uv_ulya_ce617998_12:


    "..."


translate spanish epilogue_uv_ulya_a850329e:


    "El autobús rebotaba sobre los baches con cuidado, moviéndome más y más."


translate spanish epilogue_uv_ulya_4512f1df:


    "El volante se giraba por sí solo y pronto dejé de percatarme de la ausencia de un conductor."


translate spanish epilogue_uv_ulya_ad87b7fb:


    "Mis ojos empezaron a cerrarse y me dormité..."


translate spanish epilogue_uv_ulya_5fabbbed:


    "Una muchacha se inclinaba sobre mí."


translate spanish epilogue_uv_ulya_02d2b574:


    "Ella me susurraba suavemente algo en mi oído...{w} Tan tiernamente que..."


translate spanish epilogue_uv_ulya_4fc9e3f4:


    "Sentí un fuerte golpe en mi cabeza."


translate spanish epilogue_uv_ulya_53a6f078:


    uv "¡Ey, no te duermas!"


translate spanish epilogue_uv_ulya_ba101d53:


    me "¿Acaso te molesto?"


translate spanish epilogue_uv_ulya_2d838a38:


    "Le pregunté enojado, frotándome donde me magulló."


translate spanish epilogue_uv_ulya_8e590003:


    uv "¡No me estás escuchando!"


translate spanish epilogue_uv_ulya_f7b19f66:


    me "Si salimos de aquí, tendremos mucho tiempo para conversar."


translate spanish epilogue_uv_ulya_b61f7d2c:


    uv "Y si... y si... ¿Y si no escapamos?"


translate spanish epilogue_uv_ulya_f4a33a5c:


    me "Pues entonces tendremos mucho más tiempo."


translate spanish epilogue_uv_ulya_5db31537:


    "Traté de sonreír, pero el simple pensamiento de que no pudiéramos abandonar este campamento me asustó mucho, así que hice una sonrisita estúpida."


translate spanish epilogue_uv_ulya_13b1a056:


    uv "¡Es mejor si no nos escapamos! Porque en tu mundo... No sé qué esperar de allí, pero... ¡No me prestarás nada de atención!"


translate spanish epilogue_uv_ulya_959ccee3:


    me "¡Lo haré, lo prometo!"


translate spanish epilogue_uv_ulya_67844860:


    "La abracé sinceramente."


translate spanish epilogue_uv_ulya_7853c29e:


    uv "¿De verdad de la buena?"


translate spanish epilogue_uv_ulya_c7d80c45:


    me "¡De verdad de la buena!"


translate spanish epilogue_uv_ulya_ae142e7e:


    "Yulya se apoyó contra mí."


translate spanish epilogue_uv_ulya_134e4880:


    uv "¿Y me escucharás?"


translate spanish epilogue_uv_ulya_771aa502:


    me "¡Desde luego!"


translate spanish epilogue_uv_ulya_9eb6accf:


    uv "¿Y siempre estarás de acuerdo conmigo?"


translate spanish epilogue_uv_ulya_8288e06d:


    me "¡Claro!"


translate spanish epilogue_uv_ulya_06fb0086:


    "Se apartó y me miró a los ojos."


translate spanish epilogue_uv_ulya_9e863299:


    uv "Pues bien, ¡te perdono!"


translate spanish epilogue_uv_ulya_4e48f9de:


    "Una risa a carcajadas llenó toda la cabina del autobús."


translate spanish epilogue_uv_ulya_c74b3c37:


    uv "Ándate con cuidado, no te olvides de eso, ¡y no me engañes!"


translate spanish epilogue_uv_ulya_45c72629:


    me "¿Quieres firmar un contrato o un negocio?"


translate spanish epilogue_uv_ulya_63cdfc3b:


    uv "¿Cómo un matrimonio?"


translate spanish epilogue_uv_ulya_7b2837ac:


    me "No tengo dote para eso de todas formas."


translate spanish epilogue_uv_ulya_1c13dd09:


    uv "¡Pero yo sí!"


translate spanish epilogue_uv_ulya_55396f10:


    "Yulya indicó el saco, tirado en el asiento del pasillo."


translate spanish epilogue_uv_ulya_621c7bd3:


    me "Bueno, pues..."


translate spanish epilogue_uv_ulya_c9689908:


    "Traté de acordarme de algo que hubiera comprado con mi dinero."


translate spanish epilogue_uv_ulya_b16647b1:


    me "¡Ahí va!"


translate spanish epilogue_uv_ulya_96266e14:


    "Saqué mi teléfono móvil de mi bolsillo.{w} A estas alturas ya estaba descargado."


translate spanish epilogue_uv_ulya_b3557c43:


    uv "¿Qué es eso?"


translate spanish epilogue_uv_ulya_b0ef2520:


    "Preguntó Yulya con curiosidad, agarrando el teléfono móvil."


translate spanish epilogue_uv_ulya_589cf2f8:


    me "Es una cosa muy útil en mi mundo... puedes comunicarte a distancia con la gente."


translate spanish epilogue_uv_ulya_63b9af07:


    uv "¡Guau, eso es realmente genial! ¡Excelente, eso bastará!"


translate spanish epilogue_uv_ulya_bd622277:


    "Ella dejó el teléfono móvil en el asiento."


translate spanish epilogue_uv_ulya_90bff781:


    uv "¿Bastará? Mis suministros para el invierno son para mí y para ti.... ¡serán eso!"


translate spanish epilogue_uv_ulya_01822020_1:


    me "Vale, de acuerdo."


translate spanish epilogue_uv_ulya_4a22ceaf:


    uv "Muy bien, ¡ahora somos marido y esposa!"


translate spanish epilogue_uv_ulya_945c59bc:


    "Ella sonrió astutamente."


translate spanish epilogue_uv_ulya_3118ae12:


    me "Ey, espera..."


translate spanish epilogue_uv_ulya_d41d1cae:


    "De repente recordé que toda esta conversación surgió por hablar de un negocio de matrimonio."


translate spanish epilogue_uv_ulya_8d32181d:


    me "De todas formas, sin estar en un registro civil..."


translate spanish epilogue_uv_ulya_934db5ab:


    uv "Cuando estemos en tu mundo iremos a inscribir nuestro matrimonio. Un hombre sin esposa es como una cocina sin cuchillo."


translate spanish epilogue_uv_ulya_f5e62b0b:


    me "Si llegamos, desde luego..."


translate spanish epilogue_uv_ulya_a540e1eb:


    "Yulya no dijo nada, sólo estaba mirando fijamente delante con sus ojos abiertos como platos."


translate spanish epilogue_uv_ulya_9a70d2e5:


    "Seguí su mirada y me di cuenta de que... ¡el autobús estaba repleto de personas!"


translate spanish epilogue_uv_ulya_08aceedd:


    "Todos ellos estaban llevando uniformes de pioneros y todos tenían el mismo aspecto que aquel pionero."


translate spanish epilogue_uv_ulya_b9a82855:


    "Nadie dijo ni una palabra. Todos ellos miraban callados en mi dirección."


translate spanish epilogue_uv_ulya_0638a60a:


    "No podía ver sus caras. Las ropas, la estatura... sí, pero sus rostros estaban cubiertos en sombras que parecían salidas de la nada."


translate spanish epilogue_uv_ulya_c09707fb:


    "Era como si en lugar de tener sus cabezas como las personas normales, tuvieran miniagujeros negros."


translate spanish epilogue_uv_ulya_131aff14:


    "Empecé a tener escalofríos y a mi espalda se le puso la piel de gallina."


translate spanish epilogue_uv_ulya_1d2d7a9b:


    "Yulya apretó mi mano."


translate spanish epilogue_uv_ulya_71296442:


    "Con gran esfuerzo me obligué a mirarla."


translate spanish epilogue_uv_ulya_e0da3ec4:


    "Ella también estaba muy asustada."


translate spanish epilogue_uv_ulya_13859e61:


    "Si hubiera estado solo en ese momento, probablemente me hubiera desmayado.{w} Pero justo ahora era responsable no sólo de mi propia vida."


translate spanish epilogue_uv_ulya_2c2b16f4:


    me "Quiénes.. sois... vosotros..."


translate spanish epilogue_uv_ulya_4569b623_1:


    "Dije con agallas a duras penas."


translate spanish epilogue_uv_ulya_9074e15e:


    "Los pioneros mantuvieron su terrorífico silencio."


translate spanish epilogue_uv_ulya_d76ff1af:


    "Se volvió difícil respirar. Estaba jadeando por falta de aire, ya que mi cerebro rechazaba trabajar con bajo oxígeno y perdí la conciencia por un instante."


translate spanish epilogue_uv_ulya_ede926b3:


    uv "¡¿Quiénes sois vosotros?! ¡¿Qué queréis?!"


translate spanish epilogue_uv_ulya_60f8aa06:


    "Escuché el grito de Yulya justo cuando recuperé mis sentidos."


translate spanish epilogue_uv_ulya_2b71358d:


    "Un pionero de los primeros asientos se puso en pie y caminó hasta nosotros."


translate spanish epilogue_uv_ulya_8de32484:


    pi "¡Hola, Semyon! Hemos decidido volver a «casa» también."


translate spanish epilogue_uv_ulya_2dbb2a67:


    all "También... también..."


translate spanish epilogue_uv_ulya_bbf8617d:


    "Un coro de voces llenó el autobús."


translate spanish epilogue_uv_ulya_b2f2241f:


    pi "Te haremos compañía."


translate spanish epilogue_uv_ulya_faae32c5:


    "No sabía qué decir."


translate spanish epilogue_uv_ulya_43efcd48:


    "Esta fue la situación más extraña que experimenté en el campamento."


translate spanish epilogue_uv_ulya_a067eaab:


    "Ni Yulya, ni la ciudad, ni la desaparición de los pioneros, ni esta rara persona frente a mí, ninguno era comparable con esto..."


translate spanish epilogue_uv_ulya_af6577bb:


    uv "¡Te hice una pregunta!"


translate spanish epilogue_uv_ulya_b9e0f9fd:


    "Parece que Yulya todavía era capaz de hablar con claridad.{w} No era una sorpresa. A fin de cuentas ella todavía seguía siendo, podríamos decir, una extraordinaria criatura."


translate spanish epilogue_uv_ulya_9ce8487e:


    pi "Repito. Tan sólo volvemos a casa."


translate spanish epilogue_uv_ulya_5913ba9c:


    "Su voz sonaba frenética."


translate spanish epilogue_uv_ulya_ecaa6f59:


    pi "¿Acaso te molestamos?"


translate spanish epilogue_uv_ulya_7cde2b94:


    all "Molestamos... molestamos..."


translate spanish epilogue_uv_ulya_116fe00f:


    uv "¿Qué es lo que quieres de nosotros?"


translate spanish epilogue_uv_ulya_728d6df5:


    pi "Nada en absoluto."


translate spanish epilogue_uv_ulya_09008b6b:


    all "Nada... nada..."


translate spanish epilogue_uv_ulya_9f89e0ef:


    uv "¡No te creo! ¡Sé que hay una razón por la cual estás haciendo todo esto!"


translate spanish epilogue_uv_ulya_021210b3:


    pi "¡Sólo son imaginaciones tuyas, animal!"


translate spanish epilogue_uv_ulya_6bd9eaf0:


    uv "Tal vez sea un animal. ¿Pero qué eres tú? ¿Un fantasma, una sombra?"


translate spanish epilogue_uv_ulya_1ee124eb:


    pi "Quizás. Los fantasmas también tienen sus derechos."


translate spanish epilogue_uv_ulya_d438c040:


    uv "¡Tienes derecho a irte al infierno!"


translate spanish epilogue_uv_ulya_f5fb328d:


    "Gritó Yulya con tanta fuerza que mis oídos resonaban. Pero ni el pionero ni sus compañeros reaccionaron."


translate spanish epilogue_uv_ulya_98717eb9:


    pi "Supongo que nos quedaremos."


translate spanish epilogue_uv_ulya_955f7cb8:


    "Dijo maliciosamente."


translate spanish epilogue_uv_ulya_8e29f23e:


    me "¿Pero cómo lo lograste? Y todos los otros..."


translate spanish epilogue_uv_ulya_ee804b19:


    "Al fin reuní las fuerzas para hablar."


translate spanish epilogue_uv_ulya_21629221:


    pi "¡Es muy simple, querido amigo! Consciencia colectiva. ¿Sentiste hablar de ello?"


translate spanish epilogue_uv_ulya_7b70b2c1:


    me "Y... ¿bien?"


translate spanish epilogue_uv_ulya_24b9de6e:


    pi "Cada uno de nosotros quiso desearte suerte simultáneamente, eso es todo."


translate spanish epilogue_uv_ulya_e60d1472:


    all "Suerte... suerte..."


translate spanish epilogue_uv_ulya_b80d896b:


    pi "En persona, diría. Ése es el motivo por el cual estamos aquí."


translate spanish epilogue_uv_ulya_9fab6928:


    me "Pero, ¿y qué sucederá ahora?"


translate spanish epilogue_uv_ulya_8da302f0:


    pi "Sinceramente, no tengo ni idea."


translate spanish epilogue_uv_ulya_c8ba98b9:


    uv "¡Lárgate!"


translate spanish epilogue_uv_ulya_f00f1e41:


    "Le gritó Yulya otra vez."


translate spanish epilogue_uv_ulya_7f35eb53:


    pi "Relájate... ¡cálmate!"


translate spanish epilogue_uv_ulya_94f08d02:


    pi "Verás..."


translate spanish epilogue_uv_ulya_fbc96e29:


    "Se dirigió a mí otra vez."


translate spanish epilogue_uv_ulya_d743b5e5:


    pi "No quiero ocultarte nada. Tienes una buena oportunidad para escapar ahora. No sé exactamente por qué, pero tus acciones rompieron el bucle. Los bucles, para ser más precisos, todos nuestros bucles. Y hay una posibilidad de que no tengamos que abandonar cada siete días el campamento para volver el octavo, despertándonos en este autobús."


translate spanish epilogue_uv_ulya_db8702b3:


    uv "¡Pues no vuelvas! ¡Sólo muérete!"


translate spanish epilogue_uv_ulya_f3e7b92e:


    pi "No es tan sencillo. ¿Y por qué no íbamos a tener un sentido de supervivencia?"


translate spanish epilogue_uv_ulya_6747b594:


    pi "Deberíamos, ¿verdad?"


translate spanish epilogue_uv_ulya_a3c4358b:


    "Les preguntó a los pioneros."


translate spanish epilogue_uv_ulya_11042548:


    all "Deberíamos... deberíamos..."


translate spanish epilogue_uv_ulya_7d1b8fb3:


    "Escuché sus respuestas."


translate spanish epilogue_uv_ulya_fdc1027f:


    pi "Así que decidimos hacerte compañía."


translate spanish epilogue_uv_ulya_d3565aac:


    pi "Ahora, o nos vamos de aquí juntos, o bien nos desvanecemos para siempre juntos. O empezamos otro bucle. Las tres opciones nos valen."


translate spanish epilogue_uv_ulya_71af10ce:


    uv "¡¿Y si nos morimos por vuestra culpa?!"


translate spanish epilogue_uv_ulya_96f68338:


    pi "Bueno, no miraréis solos. Moriremos con honor."


translate spanish epilogue_uv_ulya_861f9a54:


    all "Honor... honor..."


translate spanish epilogue_uv_ulya_57fefea4:


    pi "O crees que nuestras vidas..."


translate spanish epilogue_uv_ulya_446df447:


    "Él comenzó a reír..."


translate spanish epilogue_uv_ulya_f303d177:


    pi "Bueno, a lo que llamemos vidas..."


translate spanish epilogue_uv_ulya_a123f077:


    all "Vidas... vidas..."


translate spanish epilogue_uv_ulya_651e05d0:


    pi "¿Acaso no son tan importantes como las vuestras?"


translate spanish epilogue_uv_ulya_ec2d2644:


    "Yulya no sabía cómo responder."


translate spanish epilogue_uv_ulya_20807278:


    pi "Y no te pienses que todos nosotros estamos aquí. De hecho, muchos de nosotros no cabían, ya que no habían suficientes billetes para todos."


translate spanish epilogue_uv_ulya_5d22fc9e:


    uv "Tú... tú..."


translate spanish epilogue_uv_ulya_88523b73:


    "Yulya siseó."


translate spanish epilogue_uv_ulya_f501ead7:


    pi "Es demasiado tarde. Te sugiero que te tranquilices y disfrutes del vuelo. Las azafatas te ofrecerán comida y bebida en un par de minutos."


translate spanish epilogue_uv_ulya_2b07621f:


    "Sonrió y se fue a su lugar."


translate spanish epilogue_uv_ulya_3d9eb82e:


    "Yulya se puso en pie, pero la detuve."


translate spanish epilogue_uv_ulya_83d116b4:


    uv "¡Suéltame! ¡Lo tumbaré!"


translate spanish epilogue_uv_ulya_c6ddc0c1:


    me "No es necesario... ¿Qué le vas hacer? ¿Qué hará de bien?"


translate spanish epilogue_uv_ulya_6e1e8524:


    "Ella me miró sorprendida, pero se sentó."


translate spanish epilogue_uv_ulya_30c0cbc9:


    uv "Entonces qué, ¿estás sugiriendo que no hagamos nada y tan sólo esperemos?"


translate spanish epilogue_uv_ulya_e2f12d3a:


    me "¿Y qué podemos hacer?"


translate spanish epilogue_uv_ulya_9cc2138c:


    uv "No lo sé, pero no deberíamos estar simplemente sentados..."


translate spanish epilogue_uv_ulya_a77d3972:


    me "He aprendido una cosa en este campamento... muy pocas cosas dependen de mí. En realidad, nada en absoluto. Especialmente ahora. Especialmente aquí. No tengo ni la más mínima idea de cómo sacarlos de aquí, pero incluso aunque lo supiera, estoy seguro de que nada cambiaría, de que nada afectaría el resultado final."


translate spanish epilogue_uv_ulya_a78021fb:


    uv "Puede que estés en lo cierto..."


translate spanish epilogue_uv_ulya_c9107c2d:


    "Dijo Yulya con una voz triste."


translate spanish epilogue_uv_ulya_065ddf5d:


    me "Solamente podemos esperar, pues.{w} No llevará mucho tiempo..."


translate spanish epilogue_uv_ulya_476454b2:


    "Ella apoyó su cabeza sobre mi hombro."


translate spanish epilogue_uv_ulya_ce617998_13:


    "..."


translate spanish epilogue_uv_ulya_46ee7ab5:


    "Hemos estado viajando por un largo rato, parecía una eternidad.{w} El sol se puso y se hizo de noche."


translate spanish epilogue_uv_ulya_f2b3c332:


    "Lo segundos se hacían horas, los minutos... años."


translate spanish epilogue_uv_ulya_1d001f7b:


    "Los pioneros permanecían en un silencio sepulcral, e incluso aquel, el arrogante, ni tan siquiera intentó empezar una conversación."


translate spanish epilogue_uv_ulya_63c82e69:


    "El silencio era más ruidoso que cualquier grito."


translate spanish epilogue_uv_ulya_a6d65f6d:


    "Una palmada floja hubiera sido suficiente como para hacer que mis tímpanos explotaran."


translate spanish epilogue_uv_ulya_27bb93cc:


    "Yo y Yulya estuvimos en silencio también."


translate spanish epilogue_uv_ulya_b8a45f9a:


    "Hoy ya se había dicho todo, no quedaban más palabras."


translate spanish epilogue_uv_ulya_8a17dcd9:


    "Me sentía totalmente vacío. Mi cabeza pesaba tanto como plomo y ya me parecía estar casi muerto, pero Dios y el Diablo justo se habían olvidado de mí, así que tendría que pasarme el resto de mi eternidad solo con esta muchedumbre silenciosa de pioneros."


translate spanish epilogue_uv_ulya_d8530ed5:


    "¿Son en realidad mis colegas en la desgracia?"


translate spanish epilogue_uv_ulya_ee366b28:


    "¿De verdad vinieron a esta campamento también, pasaron una semana aquí y luego se fueron...?{w} ¿Y luego lo repitieron todo otra vez...?"


translate spanish epilogue_uv_ulya_be5a413d:


    "¿Quizá me aguardaría lo mismo a mí, tal como dijo el pionero?"


translate spanish epilogue_uv_ulya_4231c343:


    "Todo es posible.{w} Todo es posible ahora..."


translate spanish epilogue_uv_ulya_ce617998_14:


    "..."


translate spanish epilogue_uv_ulya_79c1defa:


    "Escuché un ronquido flojo de Yulya... se quedó dormida."


translate spanish epilogue_uv_ulya_7ac9cad9:


    "Todo este estrés debe haber sido demasiado para ella."


translate spanish epilogue_uv_ulya_bbfcefb4:


    "O tal vez consumió toda su energía gritando."


translate spanish epilogue_uv_ulya_6a4ba34d:


    "En cualquier caso, es mejor afrontar el final mientras se duerme."


translate spanish epilogue_uv_ulya_8c9884a6:


    "Ahora estaba completamente seguro de que éste era el final."


translate spanish epilogue_uv_ulya_413ab6f9:


    "Es estúpido aferrarse a la esperanza mientras se está al borde de un precipicio, rodeado por innumerables enemigos."


translate spanish epilogue_uv_ulya_c1ca73df:


    "E incluso aunque hubiera una pequeñísima posibilidad de ser rescatados, nunca pensarías en ella de verdad."


translate spanish epilogue_uv_ulya_9ad0b36a:


    "Tal vez no debería ser así, y otra persona en mi lugar habría sido más optimista..."


translate spanish epilogue_uv_ulya_9d4ef48c:


    "Tan sólo tenía que dar el último paso."


translate spanish epilogue_uv_ulya_1802ad39:


    "Aunque, ¿por qué?{w} No hay escaleras, sino unas escaleras mecánicas, me llevarán al final de todos modos."


translate spanish epilogue_uv_ulya_e8c2c59c:


    "Cerré mis ojos y mi consciencia me abandonó de golpe.{w} Me quedé dormido."


translate spanish epilogue_uv_ulya_3d844e6d:


    "No sé si duró por una hora o por una eternidad..."


translate spanish epilogue_uv_ulya_1c9980b4:


    "El vacío no es el mejor lugar para los pensamientos."


translate spanish epilogue_uv_ulya_e6517a54:


    "Cuando no hay nada, la esencia del mundo normal desaparece. Al final, los pensamientos también forman parte de ello, sólo que con un carácter inmaterial."


translate spanish epilogue_uv_ulya_c43fb6ec:


    "Es difícil imaginar dónde termina la existencia. Una persona no es capaz de imaginárselo."


translate spanish epilogue_uv_ulya_040c5913:


    "Recuperé la consciencia en una completa oscuridad. Para ser precisos, recuperé mi habilidad para percibirme como una persona. No había otra cosa, ni a mi alrededor ni dentro de mí."


translate spanish epilogue_uv_ulya_c8cdb262:


    "Era como si hubiera cesado de existir, hubiera sido borrado del universo. Pero no estaba muerto, no. Era totalmente distinto, era como si me hubiera desplazado de las tres dimensiones del espacio a una única dimensión. El mundo entero comprimido al tamaño de un punto."


translate spanish epilogue_uv_ulya_cd386160:


    "Ese punto era yo, y era todo. Yo era todo y, a la vez, no era nada. Sin embargo, mi consciencia, mis recuerdos y mi alma también fueron a parar a ese pequeño sendero."


translate spanish epilogue_uv_ulya_05274ca7:



    nvl clear
    "No estaba preocupado. Quizá porque no había necesidad o tal vez porque todavía no podía comprender lo que sucedía."


translate spanish epilogue_uv_ulya_c8872b3a:


    "Toda noción de tiempo se perdió aquí. Sencillamente, no había lugar aquí para dicho término."


translate spanish epilogue_uv_ulya_8f5b6bf9:


    "Ése es el motivo por el cual es imposible decir cuánto {i}tiempo{/i} estuve aquí en este vacío. Finalmente, tras una hora o un año, comencé a escuchar una voz muy floja. No podía entender qué palabras eran, pero era como si yo mismo hablara. En realidad, no había otra posibilidad."


translate spanish epilogue_uv_ulya_592b8e1f:


    bush "Semyon... Semyon..."


translate spanish epilogue_uv_ulya_e79d0fd5:


    me "¿Sí?"


translate spanish epilogue_uv_ulya_051cae6f:


    "Contesté como si respondiera en un teléfono."


translate spanish epilogue_uv_ulya_20edba06:


    bush "Encantado de conocerte."


translate spanish epilogue_uv_ulya_a66b5a14:


    "La voz sonaba muy familiar."


translate spanish epilogue_uv_ulya_e7620f07:



    nvl clear
    me "..."


translate spanish epilogue_uv_ulya_0f765061:


    bush "Nos conocemos el uno al otro desde hace mucho tiempo a pesar de todo."


translate spanish epilogue_uv_ulya_0b34c373_1:


    me "¿Quién eres tú?"


translate spanish epilogue_uv_ulya_336dcb9a:


    bush "Soy tú."


translate spanish epilogue_uv_ulya_5ff017df:


    me "¿Me he muerto...?"


translate spanish epilogue_uv_ulya_58a18029:


    bush "No. ¡Nada de eso!"


translate spanish epilogue_uv_ulya_380cc324:


    "La voz parecía tranquila, como si le hablara a un viejo amigo."


translate spanish epilogue_uv_ulya_2de7bf97:


    me "¿Qué me está sucediendo?"


translate spanish epilogue_uv_ulya_60e86f67:


    bush "Has alcanzado el final del camino."


translate spanish epilogue_uv_ulya_a7f92d54:


    me "¿Qué camino?"


translate spanish epilogue_uv_ulya_6b49f2aa:


    bush "Lograste abandonar el campamento."


translate spanish epilogue_uv_ulya_aa801636:



    nvl clear
    "El campamento... ¡Sí, lo recuerdo! ¡El campamento pionero Sovyonok! Repentinamente me acordé de los últimos siete días. Me acordé de cómo me estaba yendo de allí, junto a Yulya."


translate spanish epilogue_uv_ulya_72917697:


    me "Dónde está..."


translate spanish epilogue_uv_ulya_66bc60d2:


    bush "¿Yulya?"


translate spanish epilogue_uv_ulya_fc30fd2b_2:


    me "Sí."


translate spanish epilogue_uv_ulya_e4066547:


    bush "Ella está justo aquí."


translate spanish epilogue_uv_ulya_6af67ff0:


    "Miré alrededor, pero solamente estaba rodeado por el vacío."


translate spanish epilogue_uv_ulya_dfae19f4:


    me "No la veo."


translate spanish epilogue_uv_ulya_31083230:


    bush "No deberías verla, deberías sentirla."


translate spanish epilogue_uv_ulya_98f21fc2:


    "No estaba tenso para nada, como si me estuviera recuperando de la anestesia."


translate spanish epilogue_uv_ulya_fb31367b:



    nvl clear
    me "No la siento... no siento nada."


translate spanish epilogue_uv_ulya_44de386e:


    bush "Sólo tienes que acordarte..."


translate spanish epilogue_uv_ulya_be28a3d4:


    "Tras estas palabras, miles de representaciones, de imágenes, de olores, de ideas reaparecieron en mi mente. ¡El campamento! ¡Estuve allí de verdad! Con Slavya, con Lena, con Alisa, con Ulyana, con Masha... Y luego... ¿Pero cómo? Me acuerdo claramente que lo abandoné y retorné al mundo real. Y que luego me encontré con Slavya en la parada de autobús... No, me encontré con Ulyana en la universidad. ¡No, me encontré con Alisa en un concierto! ¡Pero viví junto a Lena durante muchos años! No, tuve una vida completamente diferente junto a Masha. ¿Cómo es posible todo eso? Recuerdo acontecimientos de casi cinco vidas enteras hasta el más pequeño de sus detalles. Pero todo ocurrió simultáneamente... ¡Y estoy seguro de que todo fue real!"


translate spanish epilogue_uv_ulya_63fe795d:



    nvl clear
    me "¿Cómo...?"


translate spanish epilogue_uv_ulya_243def4b:


    bush "Sí, todo ello te ocurrió realmente."


translate spanish epilogue_uv_ulya_04098bfe:


    me "¿Pero cómo?"


translate spanish epilogue_uv_ulya_b9df3187:


    bush "Te fuiste del campamento. No solamente una vez. Luego viviste tu vida normal y corriente. Luego volvías y te ibas de nuevo..."


translate spanish epilogue_uv_ulya_d749d691:


    me "Entonces, ¿es como si hubiera muerto y me hubiesen revivido?"


translate spanish epilogue_uv_ulya_e7832e23:


    bush "No. Puedes creer que todo te sucedió simultáneamente."


translate spanish epilogue_uv_ulya_3aad82ef:


    me "No lo comprendo..."


translate spanish epilogue_uv_ulya_a1dd0efd:


    bush "Tampoco lo necesitas."


translate spanish epilogue_uv_ulya_dd924d72:


    me "¿Y quiénes eran aquellos otros pioneros?"


translate spanish epilogue_uv_ulya_bb23dee2:


    bush "Ellos también son tú."


translate spanish epilogue_uv_ulya_51149e68:


    me "..."


translate spanish epilogue_uv_ulya_3beec0f4:


    bush "Las otras caras de ti."


translate spanish epilogue_uv_ulya_d3bd0899_2:


    me "No lo comprendo."


translate spanish epilogue_uv_ulya_f0aeb1ea:



    nvl clear
    bush "Cada persona es un universo infinito. Sus propias acciones determinan cómo transcurrirá su vida. Te acuerdas de aquellas cinco veces porque fueron ocasiones en las que escogiste opciones que te conducieron a escapar del campamento. Tus otras réplicas actuaron de alguna manera diferente. Así es... ¡así de sencillo!"


translate spanish epilogue_uv_ulya_3ad789cf:


    "Para mí no era tan sencillo."


translate spanish epilogue_uv_ulya_dc739d10:


    me "¿Y qué es este campamento?"


translate spanish epilogue_uv_ulya_50953e17:


    bush "Puedes considerarlo un terreno de pruebas."


translate spanish epilogue_uv_ulya_0c39b064:


    me "¿Y quién eres tú?"


translate spanish epilogue_uv_ulya_eb217c40:


    bush "Soy tú, pero de una época muy posterior. No del tiempo tal como lo ves. Estoy más allá y por encima de todos vosotros. Puedes considerarme el Semyon primigenio."


translate spanish epilogue_uv_ulya_82187263:


    me "¿Entonces tú eres la razón de todo esto?"


translate spanish epilogue_uv_ulya_85323ec4:


    bush "Eso no es exactamente cierto. Todo no transcurrió de acuerdo a mi voluntad. Pero puedo controlar estos acontecimientos."


translate spanish epilogue_uv_ulya_b1002e1e:


    me "¿Pero por qué...?"


translate spanish epilogue_uv_ulya_8d4e756e:


    bush "¿Estás en contra?"


translate spanish epilogue_uv_ulya_8ec353e5:


    "Me perdí entre mis pensamientos. Por lo menos tanto como fuera posible estarlo en semejante estado de oscuridad como el mío."


translate spanish epilogue_uv_ulya_7479b07e_2:


    me "No lo sé..."


translate spanish epilogue_uv_ulya_ec67e222:



    nvl clear
    bush "Si te dieran una opción ahora, cuando lo sabes y recuerdas todo, ¿decidirías realmente volver atrás al principio y simplemente olvidarlo todo?"


translate spanish epilogue_uv_ulya_7479b07e_3:


    me "No lo sé..."


translate spanish epilogue_uv_ulya_b81c8065:


    "No estaba preparado para responder a semejante cuestión tan difícil."


translate spanish epilogue_uv_ulya_f9d2f152:


    me "De todas formas, ¿dónde está Yulya?"


translate spanish epilogue_uv_ulya_e4066547_1:


    bush "Ella está justo aquí."


translate spanish epilogue_uv_ulya_de833a1f:


    me "¿Dónde?"


translate spanish epilogue_uv_ulya_273c343b:


    bush "Dentro de ti."


translate spanish epilogue_uv_ulya_05dc229b:


    me "¿Cómo es eso?"


translate spanish epilogue_uv_ulya_f7dbb331:


    bush "Yulya es tu mundo interior, tu conciencia la cual tomó una forma."


translate spanish epilogue_uv_ulya_ad9639d3:


    "No podía comprender sus palabras."


translate spanish epilogue_uv_ulya_40ec5fc2:



    nvl clear
    bush "No necesitas intentar entenderlo, como ya dije."


translate spanish epilogue_uv_ulya_a337cbd5_1:


    me "Pero..."


translate spanish epilogue_uv_ulya_7b47e97d:


    "Sentí que la echaba de menos."


translate spanish epilogue_uv_ulya_017e0287:


    bush "No te preocupes. Ella siempre estará contigo."


translate spanish epilogue_uv_ulya_ea951d31:


    "No creo que quisiera precisamente esta unión justo ayer."


translate spanish epilogue_uv_ulya_c7814762:


    bush "La cosa más difícil es tener paz interior, no la paz con un enemigo. Y tú lo has logrado. Si quieres amarla, tienes que aprender a amarte a ti mismo."


translate spanish epilogue_uv_ulya_51149e68_1:


    me "..."


translate spanish epilogue_uv_ulya_4f767ea3:


    bush "Bueno, ya es suficiente para ti. Es la hora de que me vaya."


translate spanish epilogue_uv_ulya_3d6b4645:


    me "¡Espera! ¿Qué me sucederá?"


translate spanish epilogue_uv_ulya_3c28c280:


    bush "Volverás."


translate spanish epilogue_uv_ulya_08b0cbb8:


    "Todavía no comprendía qué era real y qué no lo era. O cuántas realidades habían..."


translate spanish epilogue_uv_ulya_25f7abbc:


    me "¿A dónde?"


translate spanish epilogue_uv_ulya_73290988:


    bush "Depende solamente de ti."


translate spanish epilogue_uv_ulya_3aad82ef_1:


    me "No lo entiendo."


translate spanish epilogue_uv_ulya_e8aa7b7a:


    bush "Ahora ya recordaste todas las posibles variaciones de los acontecimientos."


translate spanish epilogue_uv_ulya_040643f7:



    nvl clear
    "Se detuvo por un largo rato y no me atreví a decir nada, con tal de no interrumpirle sin querer."


translate spanish epilogue_uv_ulya_1813dbe0:


    bush "Pues bien, tienes al menos cinco opciones."


translate spanish epilogue_uv_ulya_ca002c69:


    me "¿Recordaré todo... esto?"


translate spanish epilogue_uv_ulya_05b44d12:


    bush "Eso depende de ti."


translate spanish epilogue_uv_ulya_67c8ae4f:


    "Un ápice de emoción apareció en su voz por un instante, tal vez júbilo o quizá insatisfacción."


translate spanish epilogue_uv_ulya_2569d811:


    bush "Adiós."


translate spanish epilogue_uv_ulya_a337cbd5_2:


    me "Pero..."


translate spanish epilogue_uv_ulya_76b858e0:


    "Podía sentir que la voz nunca más me volvería a hablar."


translate spanish epilogue_uv_ulya_76b2fe88:


    nvl clear


translate spanish epilogue_uv_ulya_ce617998_15:


    "..."


translate spanish epilogue_uv_ulya_8011f019:


    "El tiempo transcurrió. Traté de escoger qué hacer. Si todo es como él dijo, entonces sólo tengo que desearlo."


translate spanish epilogue_uv_ulya_9f15618d:


    "Por entonces ya me había calmado completamente, e incluso podrías decir que se me habían levantado los ánimos un poco. Estaba totalmente seguro de que me había escapado de aquel campamento encantado. Además, un brillante futuro me aguardaba. ¡No lo supe, lo recordé!"


translate spanish epilogue_uv_ulya_40f78451:


    "Pero qué debería escoger..."


translate spanish epilogue_uv_ulya_1424cf31:



    nvl clear
    "Cerré los ojos y me imaginé con claridad a Slavya sonriéndome. Ella siempre estaba dispuesta a ayudarme, y nunca me regañó por nada. Siempre estaba tranquilo y seguro con ella. Y aquel encuentro en la parada de autobús... Supongo que nunca pensé que estaría con ella en la vida real. Aunque eso sería asombroso, ¡estoy seguro!"


translate spanish epilogue_uv_ulya_c62fe1c5:



    nvl clear
    "A través de muchos años creció en mí un verdadero aprecio por Lena. En realidad recuerdo todo aquel tiempo transcurrido junto a ella. Recuerdo a nuestros niños, recuerdo los momentos de alegría y de pena, lo recuerdo todo. Es muy duro rechazar todo eso."


translate spanish epilogue_uv_ulya_b374bc69:



    nvl clear
    "¿O Alisa, abrazando dulcemente su guitarra? Nunca es aburrido junto a ella. Quizá sea la vida que siempre soñé... simplemente despreocupada y divertida. Tal vez ésa sea la muchacha que deseé... como una verdadera compañera. La imagen de Alisa en mis pensamientos sonreía astutamente."


translate spanish epilogue_uv_ulya_a8c0d905:



    nvl clear
    "La Ulyana madura parecía también una buena opción. Desde luego, antes en el campamento no la traté como una mujer, pero ahora... Obviamente, ella tiene sus desventajas, pero a la vez había una sensación de magia en ella, algo atractivo."


translate spanish epilogue_uv_ulya_248b80e1:



    nvl clear
    "Y por supuesto Miku... Masha en la otra vida. La estimaba de verdad. Quizá antes {i}allí{/i} en el campamento, sólo sea un pequeño episodio y puedo ser una persona completamente diferente de mí mismo respecto de los otros caminos, ¡pero eso es lo mejor! Nuevas oportunidades, nuevas posibilidades y aquella persona que amo, una persona que aparentemente conocí toda mi vida."


translate spanish epilogue_uv_ulya_1f451166:



    nvl clear
    "Pero sigue siendo triste que nunca volveré a ver a Yulya..."


translate spanish epilogue_uv_ulya_1805ca4b:


    uv "¡Sin duda que lo harás!"


translate spanish epilogue_uv_ulya_be7a8c7c:


    "Su voz sonó en mi cabeza."


translate spanish epilogue_uv_ulya_3e8460dd:


    me "¿Qué? ¿Dónde estás?"


translate spanish epilogue_uv_ulya_8b87fadc:


    uv "Estoy justo aquí. Tal como él dijo."


translate spanish epilogue_uv_ulya_23a24352:


    me "¿Lo sabías?"


translate spanish epilogue_uv_ulya_2c864d61:


    uv "No. Lo comprendí solamente cuando llegamos aquí."


translate spanish epilogue_uv_ulya_bf6c77a3:


    me "¿Estás bien así? ¿No me echarás de menos?"


translate spanish epilogue_uv_ulya_938ec256:


    uv "¡Por supuesto que no! ¡Siempre estaré contigo!"


translate spanish epilogue_uv_ulya_51149e68_2:


    me "..."


translate spanish epilogue_uv_ulya_9f6f75fc:


    uv "¿Te acuerdas de tu promesa?"


translate spanish epilogue_uv_ulya_ad02f53a_1:


    me "¿Qué promesa?"


translate spanish epilogue_uv_ulya_989f34dd:


    uv "¡Que siempre estarás conmigo!"


translate spanish epilogue_uv_ulya_9513cd87_2:


    me "Sí..."


translate spanish epilogue_uv_ulya_25c487ea:


    uv "¡Ni se te ocurra romperla! ¡Te estaré vigilando!"


translate spanish epilogue_uv_ulya_19a7bed2:



    nvl clear
    "No sé ni tan siquiera si estaré hablando con ella así en otra ocasión. Tal vez uno podría volverse loco con eso, pero ahora mismo estaba feliz."


translate spanish epilogue_uv_ulya_601ae3ae:


    me "Muy bien. No la olvidaré."


translate spanish epilogue_uv_ulya_b9d89c90:


    uv "Pues entonces solamente tienes que tomar una decisión."


translate spanish epilogue_uv_ulya_8b728ec7:


    "Sentí que Yulya desaparecía, desvaneciéndose en el laberinto de mi mente."


translate spanish epilogue_uv_ulya_3d5436bd:


    me "Bueno, ¡es la hora!"


translate spanish epilogue_uv_ulya_867f9349:


    "Cerré mis ojos con fuerza y..."


translate spanish epilogue_uv_ulya_76b2fe88_1:


    nvl clear


translate spanish epilogue_uv_ulya_ce617998_16:


    "..."


translate spanish epilogue_uv_ulya_442941ca:


    "......"


translate spanish epilogue_uv_ulya_b010f1b9:


    "........."


translate spanish epilogue_uv_ulya_04c1f529:


    "...mis ojos fueron golpeados por un brillante rayo de luz de sol."


translate spanish epilogue_uv_ulya_a7f58933:


    "No tiene nada de sorprendente, al fin y al cabo es verano."


translate spanish epilogue_uv_ulya_e544a3a0:


    "Es ese extraño sueño de nuevo."


translate spanish epilogue_uv_ulya_8dcc0023:


    "¿Por qué lo comencé a ver tan a menudo recientemente?"


translate spanish epilogue_uv_ulya_7a5325b0:


    "¡Quizás haya una razón!"


translate spanish epilogue_uv_ulya_4a2fcf60:


    "Aun así, ¿por qué?"


translate spanish epilogue_uv_ulya_0212e193:


    "Un campamento de pioneros, algunas muchachas, la líder del campamento..."


translate spanish epilogue_uv_ulya_19d5f5ed:


    "Nunca había estado en el campamento."


translate spanish epilogue_uv_ulya_e018ccaf:


    "¿Quizás solamente no lo recuerdas?"


translate spanish epilogue_uv_ulya_00b53382:


    "Muy gracioso..."


translate spanish epilogue_uv_ulya_51119d14:


    "¿Pero y si estuve?"


translate spanish epilogue_uv_ulya_49a0f6ca:


    "Creéme, me acuerdo perfectamente de todo lo que me ha sucedido desde que tengo tres años."


translate spanish epilogue_uv_ulya_22e170fb:


    "Y antes de eso es porque estaba en una edad sin consciencia."


translate spanish epilogue_uv_ulya_b1a60837:


    "Y aunque eso fuera verdad, todo esto no es fortuito."


translate spanish epilogue_uv_ulya_413b28d9:


    "¿Por qué no es fortuito?"


translate spanish epilogue_uv_ulya_6eb9c6a9:


    "Hay gente que ve un gato negro en sus sueños cada noche, ¡y nada sucede!"


translate spanish epilogue_uv_ulya_e04a3f3a:


    "¿Te lo acabas de inventar?"


translate spanish epilogue_uv_ulya_1db77900:


    "No me invento nada. Lo leí recientemente."


translate spanish epilogue_uv_ulya_85928523:


    "Un gato negro es una cosa y un detallado sueño es otra."


translate spanish epilogue_uv_ulya_d2a78cd5:


    "No veo mucha diferencia."


translate spanish epilogue_uv_ulya_21380b7e:


    "Vale. ¿Pues qué dirías sobre aquella voz?"


translate spanish epilogue_uv_ulya_1b443b08:


    "¿Qué voz?"


translate spanish epilogue_uv_ulya_c90f6cbf:


    "No te hagas el tonto."


translate spanish epilogue_uv_ulya_9087d0df:


    "No lo pretendía tampoco."


translate spanish epilogue_uv_ulya_ac53bb9c:


    "El que te dijo que hicieras una elección."


translate spanish epilogue_uv_ulya_fb191570:


    "Bueno, una voz. ¿Y qué?"


translate spanish epilogue_uv_ulya_8b52ac40:


    "Había un motivo."


translate spanish epilogue_uv_ulya_2edc20f2:


    "Pero no recuerdo nada más, el sueño se acaba ahí."


translate spanish epilogue_uv_ulya_8545d90e:


    "¿Quizá te olvidaste cuando te despertaste?"


translate spanish epilogue_uv_ulya_82f9910a:


    "Tal vez, pero..."


translate spanish epilogue_uv_ulya_355a343b:


    "¿Pero qué?"


translate spanish epilogue_uv_ulya_09d75c72:


    "Eso significa que en realidad hiciste una elección."


translate spanish epilogue_uv_ulya_e1a2e98c:


    "Ni tan siquiera me acuerdo de las opciones."


translate spanish epilogue_uv_ulya_6dd1edbf:


    "¿Había alguna?"


translate spanish epilogue_uv_ulya_4f6b1ca8:


    "¡Deberían haberlas!"


translate spanish epilogue_uv_ulya_5d58786f:


    "Ni tan siquiera tratas de ser serio con las cosas importantes, ¡como siempre!"


translate spanish epilogue_uv_ulya_9ccde1c2:


    "¡Dios mío! ¡Es tan importante!"


translate spanish epilogue_uv_ulya_a58e1faa:


    "No tiene sentido hablar contigo..."


translate spanish epilogue_uv_ulya_a20cefa7_1:


    "..."


translate spanish epilogue_uv_ulya_bcd935d1:


    "Recientemente, comencé a darme cuenta de que tengo algo así como una doble personalidad."


translate spanish epilogue_uv_ulya_9923c087:


    "Un monólogo interior es, desde luego, un fenómeno normal para cualquier persona, pero cuando se vuelve en un diálogo..."


translate spanish epilogue_uv_ulya_ce76dee3:


    "Bueno, todavía está lejos de ser un caso clínico, a fin de cuentas me hablo conmigo mismo y no con otra personalidad dentro de mi mente."


translate spanish epilogue_uv_ulya_357f3a65:


    "Quizá con mi propia subconsciencia..."


translate spanish epilogue_uv_ulya_6ace32a6:


    "Sin embargo, ¡eso no es importante!"


translate spanish epilogue_uv_ulya_aa527417:


    "Me di la vuelta hacia el otro lado.{w} El brillante sol aun iluminaba frente mis ojos."


translate spanish epilogue_uv_ulya_85ea5c99_1:


    uv "¡Buenos días!"


translate spanish epilogue_uv_ulya_e1ffc0eb:


    sl "¡Buenos días!"


translate spanish epilogue_uv_ulya_83a3b49d:


    dv "¡Buenos días!"


translate spanish epilogue_uv_ulya_5567bace:


    un "¡Buenos días!"


translate spanish epilogue_uv_ulya_cb60f8f8:


    us "¡Buenos días!"


translate spanish epilogue_uv_ulya_e8f1372a:


    ma "¡Buenos días!"


translate spanish epilogue_uv_ulya_cfb99cb8:


    dreamgirl "¡Buenos días!"


translate spanish epilogue_uv_ulya_0a6d11b5:


    me "¡Buenos días!"


translate spanish epilogue_uv_ulya_348dbde8:


    "Contesté."


translate spanish epilogue_uv_ulya_bc8dedc6:


    "Elección... ¿Acaso tengo alguna elección...?"


translate spanish epilogue_uv_ulya_d99864b1:


    "Cada historia tiene su principio y su final."


translate spanish epilogue_uv_ulya_4d69c034:


    "Cada historia tiene su lienzo, su sinopsis, su contenido, sus momentos clave, su prólogo y su epílogo."


translate spanish epilogue_uv_ulya_754a4f66:


    "Y no hay libro que, tras releerse, no fuera a revelar nuevos detalles de los que antes no te percataras."


translate spanish epilogue_uv_ulya_39cdaa6f:


    "Pero todo libro tiene una última página. Y una vez la hemos pasado, ponemos el libro en la estantería."


translate spanish epilogue_uv_ulya_611077c4:


    "Sólo para abrir uno nuevo mañana..."
# Decompiled by unrpyc: https://github.com/CensoredUsername/unrpyc
