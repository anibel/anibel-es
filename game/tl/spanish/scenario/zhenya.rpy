
translate spanish zhenya_route_4060be9e:


    "Por favor regrese más tarde ..."


translate spanish zhenya_route_c9a1f434:



    "Mi mundo está vacío, tan vacío como lo estoy yo."


translate spanish zhenya_route_f3335cb0:



    "Me destruí a mí mismo para convertirme en un mundo entero, un mundo de vacío que me destruyó..."


translate spanish zhenya_route_37955148:



    "Creí que tenía el destino de alguien en mis manos, el poder para acabar con las vidas de los otros."


translate spanish zhenya_route_408b289a:



    "Pero solamente estuve vagando en la oscuridad, escondiéndome en vano de cualquier rayo de luz nuevo, cada uno de los cuales sólo engendraba sombras vacías."


translate spanish zhenya_route_2d048c95:



    "Cada sombra era mi reflejo anónimo, una nube de oscuridad absoluta con un único propósito: consumirme."


translate spanish zhenya_route_d09f25e0:



    "Me estaba escondiendo de ellas, fundiéndome con ellas, ellas se convertían en mí y yo me estaba convirtiendo en ellas y, finalmente, ya no importó nada quién de nosotros estaba al otro lado del espejo."


translate spanish zhenya_route_ce617998:



    "..."


translate spanish zhenya_route_5685d9fa:



    "Aunque en este caso no es simplemente una frase cliché o el reflejo de un vulgar perdedor."


translate spanish zhenya_route_34acc943:



    "Es un hecho."


translate spanish zhenya_route_162b28bf:



    "Incluso a pesar de que éste no fuera el anterior caso, incluso a pesar de que antes los mundos fueran muchos y a mi vida se le pudiera llamar vida."


translate spanish zhenya_route_48f0c7b7:



    "Porque todo se terminó en un solo instante."


translate spanish zhenya_route_99b70cac:



    "Primero fue este maldito autobús... otra vez, ¡por doquier!... y una muchedumbre de aquella alegre algarabía de {i}pioneros{/i} por alrededor, iguales que yo..."


translate spanish zhenya_route_5f95db12:



    "Y luego una vez y otra, como un vívido caleidoscopio de rostros, acontecimientos, emociones y sentimientos."


translate spanish zhenya_route_2c11ec4c:



    "¡¿Pero a quién demonios le importa ya?!"


translate spanish zhenya_route_f4cffaa7:



    "Sabes, si un hombre ha muerto él lamentará lo que no tuvo tiempo de hacer, lo que no hizo, lo que no dijo, más que cómo exactamente terminó su curso vital."


translate spanish zhenya_route_1c0707ba:



    "¿Pero acaso me hallo en uno de esos lugares donde las almas van a parar tras la muerte...?"


translate spanish zhenya_route_7eb975bc:



    "¡Y todo podría haber sido diferente también!"


translate spanish zhenya_route_61b04b14:



    "No importa que el final sea siempre uno y el mismo, si el camino hacia él puede variar... depende de mi elección."


translate spanish zhenya_route_11e39e69:



    "Lo hizo antes..."


translate spanish zhenya_route_5cfa9630:



    "Así que, ¿cuándo cambió todo?"


translate spanish zhenya_route_3136eb3d:



    "Las versiones diferentes de los acontecimientos, las líneas de la vida, el destino de la gente parecía entrelazarse en diversas ocasiones, ¡acabando por ser algo así como una cuerda maldita con la cual solamente puedes estrangularte a ti mismo!"


translate spanish zhenya_route_b9305951:



    "Así que, ¿cuándo cambió todo?"


translate spanish zhenya_route_bc26a936:



    "Seguramente no en un solo día, no en una sola hora, pero ciertamente tan rápido que no tuve incluso tiempo para darme cuenta."


translate spanish zhenya_route_ce617998_1:



    "..."


translate spanish zhenya_route_37e5def2:



    "Los rayos del sol veraniego brillaron en mis ojos."


translate spanish zhenya_route_f2bda4fc:



    "Uno de verano... bueno, quién lo iba a decir, ¡qué sorpresa!"


translate spanish zhenya_route_7ebc85db:



    "Pero fue invierno ayer...{w} ¿Realmente era ayer?"


translate spanish zhenya_route_7368e9d8:



    "Sé que hay un paquete de cigarrillos y una caja de cerillas guardadas en la guantera del conductor."


translate spanish zhenya_route_a20a3b61:



    "Mi cabeza daba vueltas de las respiraciones profundas que inhalé y el desagradable olor a mentol nublaba mi cerebro con un escalofrío."


translate spanish zhenya_route_4fc8a5c5:



    "«Cosmos», para exportar."


translate spanish zhenya_route_3468df1f:



    "Entonces podían hacer rematadamente bien cosas para exportar, ¡a diferencia de ahora!"


translate spanish zhenya_route_006b2395:



    "Reactores nucleares, rifles, cigarrillos y comunismo."


translate spanish zhenya_route_f59ec965:



    "Cantidad de comunismo: en las cajas, en embalajes, en contenedores, en las bodegas de los buques de carga, en los transportes de carga de los aviones, en los vagones de bienes de los trenes, ¡incluso en las naves espaciales!"


translate spanish zhenya_route_389b751d:



    "Incluso más comunismo... hacia los pigmeos de África, los incas de Sur América, los papuanos de Nueva Guinea..."


translate spanish zhenya_route_e9153e64:



    "Y supongo que también podemos soltar un paquete para los pingüinos de la Antártida."


translate spanish zhenya_route_00e16e71:



    "¡Puaj, estoy delirando!"


translate spanish zhenya_route_8c8441f3:



    "Terminando con el tercer cigarrillo, me quedé mirando fijamente las puertas y comencé a esperar."


translate spanish zhenya_route_3b0dacde:



    "Tampoco necesito un reloj... hay un cronómetro interno que no falla funcionando en mi cabeza."


translate spanish zhenya_route_df514e2b:



    "Ahora, sólo un poquito más de tiempo..."


translate spanish zhenya_route_5e5b6ce1:



    "Una de las puertas chirrió desagradablemente y se abrió un poco, tras ella se asomó una muchacha en uniforme de pionera, la cual miró alrededor y se percató de mí."


translate spanish zhenya_route_b58b784f:



    "Al acercarse caminando ella aspiró el denso y caliente aire, parecía estar un poco sorprendida, pero inmediatamente sonrió y dijo:"


translate spanish zhenya_route_7625c414:



    slp "Hola, debes ser el nuevo..."


translate spanish zhenya_route_8433d564:



    "Grité, me levanté y me fui al campamento, ignorándola."


translate spanish zhenya_route_6a3f1549:



    "Fue hilarante una vez, luego fue divertido, ahora es solamente un hábito."


translate spanish zhenya_route_fe493094:



    "Un estúpido pero obsesivo hábito, como poner diferentes cosas en diferentes bolsillos."


translate spanish zhenya_route_6761052d:



    "Yo, por ejemplo, no puedo llevar un teléfono en mi bolsillo derecho porque..."


translate spanish zhenya_route_268e7fb4:



    "¡Al cuerno si lo sé!"


translate spanish zhenya_route_c21732cf:



    "Y éste es el mismo asunto: ¡insultar a una muchacha por la mañana es un buen comienzo para el día!"


translate spanish zhenya_route_68c3b30c:



    "Me encontré con otra muchacha pionera por el camino."


translate spanish zhenya_route_47974352:



    "Para ser más exactos, ella trató de golpearme la espalda pero la esquivé a tiempo, y la miré de tal forma que pareció repentinamente producirle a la pobre muchacha la urgente necesidad de irse a hacer otras cosas más importantes."


translate spanish zhenya_route_4db95486:



    "La cabaña de la líder del campamento estaba inundada por una tormenta de lilos...{w} Ojalá se hundiera ya, ¡maldita sea!"


translate spanish zhenya_route_8df4ea96:



    "El fuerte portazo de la puerta distrajo al ocupante de la lectura de algún libro."


translate spanish zhenya_route_e969b169:



    "No, la verdad es que nunca me preocupé por lo que la líder del campamento lee."


translate spanish zhenya_route_8aa38276:



    "Sin desvestirme, salté sobre la cama y estiré mis pies con sus botas de invierno sobre el pie de cama."


translate spanish zhenya_route_dc44d1f9:



    mtp "¡¿Qué crees que estás haciendo?! Y ya de paso, ¡¿quién eres tú?!"


translate spanish zhenya_route_c982b4e5:



    "Preguntó la líder del campamento, resentida genuinamente."


translate spanish zhenya_route_2045575e:



    pi "Cierra el pico, mujer."


translate spanish zhenya_route_bb30f1ca:



    "Dije perezosamente."


translate spanish zhenya_route_13e9df52:



    "Ella se quedó petrificada, incapaz de decir una sola palabra."


translate spanish zhenya_route_fea18646:



    "Siempre es así, cada maldita vez."


translate spanish zhenya_route_bed25db3:



    pi "Mejor aun, explícame, ¿por qué no has encontrado todavía un hombre para ti? ¿Qué edad tienes, veinticinco? ¿Más de veinticinco? ¡Ya deberías saber cómo funciona el mundo a esa edad!"


translate spanish zhenya_route_8920f5d5:



    "Por un instante un pesado silencio se adueñó de la habitación."


translate spanish zhenya_route_91056f46:



    pi "¿No lo sabes? Te lo explicaré pues... ¡¿quién demonios querría estar con alguien como tú?!"


translate spanish zhenya_route_c092ab59:



    "La casa de la líder del campamento resonó con una carcajada sonora, o incluso más bien como un relinchar de caballo, mientras una melodía familiar sonaba fuera."


translate spanish zhenya_route_46e84c94:



    "En un par de ocasiones traté de descubrir de dónde provenía la música para la cena, así que en vez de ello los altavoces reproducirían algo más o menos alegre en el campamento."


translate spanish zhenya_route_94ec9f26:



    "No lo pensé... parece que esta melodía angelical no necesitaba tampoco de un reproductor, ni cables ni electricidad."


translate spanish zhenya_route_66275b7d:



    "Pues al infierno con ello..."


translate spanish zhenya_route_432de457:



    "Por ejemplo, ahora mismo no estaría en contra de que todo el campamento escuchara mis risas, me escuchara reír... reír..."


translate spanish zhenya_route_60662672:



    "¿Pero de quién?"


translate spanish zhenya_route_31e1abdf:



    "No de mí mismo, ¡no!"


translate spanish zhenya_route_8b953e88:



    "Simplemente me rio porque reírse es un maravilloso mecanismo de defensa y una manera de distraer el aburrimiento."


translate spanish zhenya_route_18084d09:



    "¿Cuánto te proporciona un minuto de risa? ¿Cinco minutos de vida?"


translate spanish zhenya_route_40c9f60e:



    "Bueno no tiene sentido conmigo, soy de hecho inmortal, tan sólo necesito una certificación notarial."


translate spanish zhenya_route_4517b112:



    mtp "¡¿Cómo te atreves a hablarle así a tus mayores?!"


translate spanish zhenya_route_ffdabf91:



    pi "Olya, querida, cambia de aires ya, ¿vale? Sinceramente, ¡estoy harto de escuchar lo mismo una y otra vez! Vamos a intentarlo de esta forma: «Hola, ¿qué tal te va?». Ves, ¡no es tan difícil! Vale, ¡ahora tu turno!"


translate spanish zhenya_route_1489a42c:



    mtp "¿Qué...?"


translate spanish zhenya_route_45586a8d:



    pi "No {i}qué{/i}, es «Hola, ¿qué tal te va?»."


translate spanish zhenya_route_fd5c0a1b:



    pi "Y te contestaré: «Gracias, mal como siempre». Y quizás incluso añada: «¡Gracias por tus palabras!»."


translate spanish zhenya_route_09e93866:



    mtp "Largo de aquí, ¡llamaré a la policía!"


translate spanish zhenya_route_06ad96de:



    pi "¿Cómo? ¿Con una paloma mensajera? El teléfono no funciona."


translate spanish zhenya_route_d7289d90:



    mtp "Sí, pero... yo..."


translate spanish zhenya_route_4c828f77:



    "La líder del campamento titubeó."


translate spanish zhenya_route_a996a0b3:



    pi "Aunque..."


translate spanish zhenya_route_89436b87:



    "Rebusqué en mi bolsillo, el izquierdo, saqué mi teléfono y se lo tiré."


translate spanish zhenya_route_9ea2f617:



    pi "Ahí tienes, inténtalo."


translate spanish zhenya_route_39fe9bde:



    "La líder del campamento cogió el teléfono; le resbaló de las manos en varias ocasiones y casi se le cae al suelo."


translate spanish zhenya_route_d9306537:



    mtp "¿Qué... es esto?"


translate spanish zhenya_route_d5c62521:



    pi "Un teléfono."


translate spanish zhenya_route_a411088b:



    "Di una respuesta breve y cerré mis ojos."


translate spanish zhenya_route_5072b3b6:



    pi "Esa cosa, ya sabes: «Ring-ring. ¿Quién es? ¿El destino? Estaba esperando tu llamada»."


translate spanish zhenya_route_7af41437:



    mtp "¿Estás mal de la cabeza?"


translate spanish zhenya_route_b216930d:



    "La líder del campamento parecía haber comenzado al fin a recuperar sus sentidos."


translate spanish zhenya_route_1fdb7186:



    pi "¿Y tú?"


translate spanish zhenya_route_2d95eff3:



    mtp "No sé qué está sucediendo aquí..."


translate spanish zhenya_route_6015a2a3:



    pi "¿Has visto la película «El Día de la Marmota»? No lo hiciste. Bueno da igual. Si esa biblioteca vuestra tuviera algunos libros sobre psiquiatría, podría ser capaz de contestarte acerca de mi condición psiquiátrica con más seguridad. Por ello, por ahora, sólo puedo decírtelo como un aficionado: sí, querida Olya, ¡estoy enfermo!"


translate spanish zhenya_route_2c220ab9:



    "Y de nuevo la ruidosa y endemoniada carcajada."


translate spanish zhenya_route_ba3723c7:



    "La líder del campamento estaba paralizada y soltó el teléfono de su mano. Cayó del borde de la cama y rebotó hasta mí."


translate spanish zhenya_route_3340d8cc:



    "Recogí el teléfono móvil al instante y lo guardé en mi bolsillo."


translate spanish zhenya_route_d9c4ae96:



    pi "Pero no estoy enamorado de ti."


translate spanish zhenya_route_cf7d0875:



    "Añadí, abriendo un ojo."


translate spanish zhenya_route_44422a26:



    "Alguien tímidamente llamó a la puerta, luego se abrió y una muchacha pionera apareció en las escaleras de la entrada..."


translate spanish zhenya_route_b5682e52:



    pi "¡Oh maldita sea!"


translate spanish zhenya_route_275798b7:



    "Salté de la cama melodramáticamente."


translate spanish zhenya_route_50556e32:



    pi "¿Y dónde está la cuchilla? ¿Olvidada en casa?"


translate spanish zhenya_route_efe5e7d5:



    "La muchacha observaba a la líder del campamento confundida."


translate spanish zhenya_route_71e1b5fb:



    mtp "Este es..."


translate spanish zhenya_route_b95f6ce4:



    "Comenzó ella, luego suspiró y se encogió profundamente en la cama contraria."


translate spanish zhenya_route_292e2dbc:



    "En realidad sabía que la líder del campamento no está asustada de mí, el miedo sencillamente no está incluido en su programa."


translate spanish zhenya_route_90ba2fd2:



    "Ella puede estar resentida... claro, tanto como quieras; imitar el miedo... ¡lo más seguro!"


translate spanish zhenya_route_a3ac6997:



    "Pero un par de minutos pasarán y ella se retirará, dejándome solo."


translate spanish zhenya_route_9318d2f9:



    "Si fuera líder de campamento...{w} ¡Si estuviera en su lugar...!"


translate spanish zhenya_route_38974101:



    unp "Es un mal momento..."


translate spanish zhenya_route_0c34c18c:



    "Parloteó tímidamente la muchacha, apartando su mirada al suelo y con disposición para marcharse."


translate spanish zhenya_route_e83f62de:



    pi "Por qué, ¡no seas tímida!"


translate spanish zhenya_route_23634e10:



    "Exclamé con hospitalidad, indicando con un gesto cerca de mí."


translate spanish zhenya_route_b1c5fa5a:



    pi "Siéntate ahora, ¡haz como en casa!"


translate spanish zhenya_route_613c7491:



    unp "No, yo..."


translate spanish zhenya_route_aed8f0f1:



    pi "¡Que te sientes he dicho!"


translate spanish zhenya_route_825bc5ee:



    "Ladré."


translate spanish zhenya_route_bef00662:



    "Ahora {i}esta{/i} muchacha puede ciertamente mostrar verdadero temor, miedo e incluso terror."


translate spanish zhenya_route_bdc40866:



    "Lentamente ella se aproximó a mi cama y se sentó, aferrándose al pie de cama."


translate spanish zhenya_route_d117b542:



    pi "Y bien, ¿tienes algo bueno que contarme...?"


translate spanish zhenya_route_ce617998_2:



    "..."


translate spanish zhenya_route_59f848eb:



    "A mí, al igual que cualquier otro crío, mis padres me enseñaron desde la infancia que no debería abrirle la puerta a los extraños."


translate spanish zhenya_route_6b1d45d4:



    "Y también que mejor no me fuera a vete a saber dónde con alguien desconocido."


translate spanish zhenya_route_c713fb4d:



    "Pero aquello fue en mi infancia..."


translate spanish zhenya_route_d74f573a:



    "¡Y ahora literalmente todo depende tanto si confío como si no en un extraño!"


translate spanish zhenya_route_2385d8a9:



    "Pareciera como si toda una vida ya se hubiera ido y yo hubiese tenido tiempo para envejecer mentalmente, si no físicamente."


translate spanish zhenya_route_28349501:



    "Y olvidé lo que el invierno es, la nieve, el frío, el atardecer oscuro, un viento que da escalofríos hasta los huesos y una eterna turbia aguanieve."


translate spanish zhenya_route_1e4f4c53:



    "Lo mismo vale para la ciudad, sin detenerse por un minuto, siempre corriendo hacia algún lugar, fumigando el sucio cielo y la gente con sus rastros de las chimeneas deterioradas y los vehículos agotados. Permaneció en el pasado distante, como las borrosas páginas de un viejo álbum de fotografías que ha sido tirado en cuarto trastero como innecesario."


translate spanish zhenya_route_fe1f361c:



    mz "¡Ey!"


translate spanish zhenya_route_b0b68304:



    "La voz de la muchacha sentada cerca me distrajo de estos pensamientos."


translate spanish zhenya_route_5cbff5c8:



    mz "¿Otra vez sigues obsesionado? ¿Sigues pensando sobre {i}eso{/i}?"


translate spanish zhenya_route_c233b6a2:



    me "¿Sobre qué?"


translate spanish zhenya_route_b00e703f:



    "Ella no quería ofenderme y pregunté otra vez por el mero hecho de hacerlo... ya sabía la respuesta."


translate spanish zhenya_route_a2aa1362:



    mz "Ya sabes."


translate spanish zhenya_route_1032aca8:



    "Estábamos sentados en el tejado de la biblioteca, nuestras piernas colgaban por el borde."


translate spanish zhenya_route_95ebb22e:



    "Zhenya se inclinó atrás un poco, su cabello se agitó bellamente con el viento."


translate spanish zhenya_route_541d0415:



    "El radiante sol estaba brillando en nuestros ojos, haciéndonos entrecerrarlos. Pero no era cegador, más bien como una suave calidez, y parecía envolver tu cuerpo en el calor agradable de un día soleado."


translate spanish zhenya_route_76bb3a52:



    me "No...{w} Quiero decir que no lo sé. Supongo.{w} Últimamente no siempre puedo decir lo que pienso. Tan sólo tengo un montón de pensamientos en mi cabeza."


translate spanish zhenya_route_d47cc9b7:



    "Me quedé callado por un instante y miré a Zhenya."


translate spanish zhenya_route_62828675:



    "Ella parecía no solamente no estar interesada en nuestra conversación, sino en todo lo que le ocurría a su alrededor en general."


translate spanish zhenya_route_75c0b8b5:



    "Como si a esta muchacha le pareciera bien todo: el verano, el sol, el agradable viento, nuestra juventud..."


translate spanish zhenya_route_344c7b57:



    "Como si esto fuera suficiente para ella para ser feliz y lo demás fueran solamente absurdas trivialidades."


translate spanish zhenya_route_b71363c7:



    me "En resumen, ¡entiendes lo que te digo! ¿Por qué sigues preguntándolo?"


translate spanish zhenya_route_7e36ced4:



    mz "Porque el humano es una criatura social, se supone que se comunica con otros como él."


translate spanish zhenya_route_9551e44d:



    "Ella sonrió casi imperceptiblemente."


translate spanish zhenya_route_c7fab65f:



    me "Claro, cierto, ¡mira quién habla! Espera aquí, iré a buscar el premio del «Pionero Más Comunicativo del año mil novecientos ochenta y tantos»."


translate spanish zhenya_route_0754d52f:



    mz "¿Y yo qué? Yo, a diferencia de otras personas, ¡tengo unas responsabilidades bastante específicas en este campamento! Soy una respetable miembro de nuestro colectivo, ¡incluso irremplazable, en cierto sentido! ¡A diferencia de ti!"


translate spanish zhenya_route_440c2112:



    "Los ojos de Zhenya destellaban mientras sonreía con sorna."


translate spanish zhenya_route_1f5dde14:



    me "Responsabilidades, sí claro..."


translate spanish zhenya_route_1dcd62cf:



    "Me cubrí los ojos con la palma y me incliné apoyándome atrás."


translate spanish zhenya_route_f3cb88e5:



    me "En serio, ¿te puedes escuchar a ti misma? ¡Responsabilidades! ¡Colectivo!"


translate spanish zhenya_route_fe1f361c_1:



    mz "¡Ey!"


translate spanish zhenya_route_8118435a:



    "Dijo Zhenya severamente y me dio un empujón en el vientre."


translate spanish zhenya_route_ea196633:



    me "Ohh."


translate spanish zhenya_route_09cd5cef:



    "Gemí más bien melodramáticamente y le hice una mirada iracunda en su dirección."


translate spanish zhenya_route_f115c4df:



    me "¿Y si me hubiera caído? ¿Me hubiese herido? ¿Eh?"


translate spanish zhenya_route_9da7dc7e:



    mz "¡Eso sería algo muy desafortunado!"


translate spanish zhenya_route_1a6118f5:



    me "¡No hay tampoco ningún lugar donde conseguir un seguro de vida aquí!"


translate spanish zhenya_route_91ca94c2:



    mz "Claro, y no habría nadie que pagara la indemnización luego."


translate spanish zhenya_route_25d2cb1e:



    me "Bueno, nadie es..."


translate spanish zhenya_route_d2c7a5fb:



    "Me rasqué detrás de la cabeza, sonreí tontamente y dije:"


translate spanish zhenya_route_1a377d44:



    me "¿Y tú qué?"


translate spanish zhenya_route_cb595887:



    mz "Yo..."


translate spanish zhenya_route_4f246bc0:



    "Zhenya pensó por un segundo."


translate spanish zhenya_route_192326c0:



    mz "Bueno, quizá, aunque no creo que la cantina acepte dólares, euros, tarjetas de crédito o, como mínimo, rublos rusos. Y probablemente tampoco hay aquí ninguna casa de cambio de moneda. E incluso si la hubiera, quién sabe cómo va la tasa de cambio. ¡No quiero padecer la inflación, ya sabes!"


translate spanish zhenya_route_e6444b06:



    me "¡Pide que te paguen en oro!"


translate spanish zhenya_route_c4101076:



    mz "¡Oro, qué tonto!"


translate spanish zhenya_route_852f8481:



    "Ella se lanzó contra mí, tratando de hacerme bien cosquillas, apretujarme, aplastarme, o bien todo lo anterior."


translate spanish zhenya_route_ce617998_3:



    "..."


translate spanish zhenya_route_197ea717:



    "Al irme de la cabaña de la líder del campamento me estaba riendo entre dientes."


translate spanish zhenya_route_dcebf79c:



    "Probablemente no debería haber esperado nada de la conversación con esta muchacha pionera."


translate spanish zhenya_route_6077816f:



    "Y no lo hice, ¿por qué lo haría?"


translate spanish zhenya_route_92d37e44:



    "Ahora todo se sabe desde hace mucho tiempo... Soy capaz de tocar esta partitura por mí mismo, como el monólogo de un miserable."


translate spanish zhenya_route_c1f15886:



    "Qué ironía... ¿Una interpretación con un actor, en un teatro con un espectador, en una ciudad con un ciudadano, en un universo con un solo ser vivo?"


translate spanish zhenya_route_851ebb7b:


    "¿O «vivir», entre comillas? ¿O tal vez {i}vivir{/i}, en cursiva? ¿Qué tipo de obituario preferirías? ¿Con volutas o sin ellas? ¿Con mayúsculas o sin ellas? ¿Qué tipo de carácter preferirías? ¿Debería ser el papel glaseado o mate?"


translate spanish zhenya_route_7e9e0418:



    "¡Puaj, otra vez!"


translate spanish zhenya_route_f619af25:



    "Camino abajo estaba dando saltitos una alegre muchachita."


translate spanish zhenya_route_023d8bad:



    "¿Debería quizá comenzar con ella esta vez?"


translate spanish zhenya_route_77521125:



    "Recuerdo que había espléndidos ganchos de carnicero en la cantina..."


translate spanish zhenya_route_c3e020a3:



    "¿O no debería...?"


translate spanish zhenya_route_4e76737d:



    usp "¿Y tú quién eres?"


translate spanish zhenya_route_244be5e6:



    "Espetó ella, acercándoseme."


translate spanish zhenya_route_90ff5d0b:



    pi "¡Soy el terror que se deja caer por la noche! ¡Buu!"


translate spanish zhenya_route_6f0e525a:



    "Efusivamente extendí mis brazos y me lancé amenazadoramente sobre ella."


translate spanish zhenya_route_6c1f8778:



    usp "Eso no asusta nada."


translate spanish zhenya_route_3d43d484:



    pi "¿No asusta? Bueno, por ahora bastará..."


translate spanish zhenya_route_8d8338a5:



    "Sonreí y me adelanté."


translate spanish zhenya_route_e10b6d74:



    usp "¡Ey!"


translate spanish zhenya_route_e8057229:



    "Gritó tras de mí."


translate spanish zhenya_route_ce6c5674:



    usp "Espera."


translate spanish zhenya_route_a3ba21f2:



    "La muchacha me atrapó y me agarró de mi mano."


translate spanish zhenya_route_a593fff2:



    pi "¡No me toques!"


translate spanish zhenya_route_842697d6:



    "Chillé con aversión y me sacudí liberando mi mano a la fuerza, así que la joven pionera salió volando del camino y se cayó en los arbustos."


translate spanish zhenya_route_911de195:



    usp "¡Ay, eso duele!"


translate spanish zhenya_route_b7ea271a:



    "Comenzó a llorar."


translate spanish zhenya_route_59a139cf:



    "Qué extraño.{w} Esto es {i}realmente{/i} extraño."


translate spanish zhenya_route_8e30c7d5:



    "No creo que algo así haya ocurrido nunca antes."


translate spanish zhenya_route_d16c2c64:



    "Por supuesto, este día se ha repetido por sí mismo tantas veces ya, que es imposible contarlas..."


translate spanish zhenya_route_fe8f03bf:



    "Y la cuestión realmente no es que la muchacha haya salido despedida hasta los arbustos, ¡es que no debería ser {i}algo{/i} nuevo!"


translate spanish zhenya_route_a884f083:



    "Incluso semejante insignificante suceso, como uno podría pensar, ¡puede resultar ser más importante que todas las anteriores {i}veces{/i}!{w} ¡Que la primera {i}vez{/i}!"


translate spanish zhenya_route_0abd66bb:



    "No sé por qué, ¡tan sólo siento que sencillamente hace demasiado tiempo desde que {i}algo{/i} nuevo sucedió!"


translate spanish zhenya_route_57abc691:



    "Desde luego, este mundo no es comparable a un mecanismo monótono que hace su trabajo desalmadamente, ¡sino que todas las variaciones del guión me son familiares!"


translate spanish zhenya_route_d9f0d80c:



    "¡Y cuando digo {i}todas{/i}, quiero decir {b}todas{/b}!"


translate spanish zhenya_route_03df3bc1:



    "Hace un par de minutos atrás en la cabaña de la líder del campamento, podría haber venido no la tímida pionera sino la muchacha que me encontré en las puertas del campamento... y estaría preparado."


translate spanish zhenya_route_00375a72:



    "O la muchacha pionera arrogante que saluda a los extraños con un golpe en la espalda... y estaría preparado."


translate spanish zhenya_route_68cb4caf:



    "O incluso esta pequeña llorica... y aun así estaría preparado..."


translate spanish zhenya_route_6c0f5a1e:



    "Pero ahora, justo en este momento, ¡algo está sucediendo para lo que no estoy preparado!"


translate spanish zhenya_route_7a13a065:



    "¡Y por encima de todo no estoy preparado para las sorpresas!"


translate spanish zhenya_route_3d9d35bf:



    "La muchacha continuaba llorando, mirándome con sus ojos llenos de ira y resentimiento."


translate spanish zhenya_route_7083b974:



    usp "¡Tú! ¡Es todo culpa tuya!"


translate spanish zhenya_route_faf5a9ef:



    pi "Venga ya, por qué estás llorando..."


translate spanish zhenya_route_47520c7a:



    "Caminé hasta ella con vacilación y le ofrecí mi mano."


translate spanish zhenya_route_87036d00:



    "Por un instante la muchacha hizo una cara desconcertada, pero luego me agarró y se puso en pie con un estirón."


translate spanish zhenya_route_4106821b:



    usp "Te sentiste avergonzado, ¿no? Ésa es la cuestión, ¡no puedes abusar de los pequeños!"


translate spanish zhenya_route_cd7eeef3:



    pi "¿Avergonzado?"


translate spanish zhenya_route_14af5cda:



    "Repetí, mis pensamientos estaban lejos de aquí."


translate spanish zhenya_route_f699ca55:



    pi "¿Por qué? ¿Por ti? ¿Por qué lo estaría?"


translate spanish zhenya_route_fbb480b9:



    usp "¡Pero deberías!"


translate spanish zhenya_route_07964120:



    "Frunció el ceño y salió corriendo hacia la cabaña de la líder del campamento."


translate spanish zhenya_route_6082bc94:



    "Para quejarse, quizá... no lo sé."


translate spanish zhenya_route_312c4c2e:



    "¡Realmente no lo sé!"


translate spanish zhenya_route_c0b16db9:



    "¡Por primera vez en mucho tiempo, mucho tiempo, algo como esto ha sucedido!"


translate spanish zhenya_route_a12e16eb:



    "Hace tan solamente un par de minutos atrás, podía decir con seguridad con qué estaban ocupados cada uno de los pioneros en este singular momento.{w} ¿Y ahora qué?"


translate spanish zhenya_route_c8849df7:



    "¡Puaj!"


translate spanish zhenya_route_ce617998_4:



    "..."


translate spanish zhenya_route_6e093196:



    "A la hora de comer se me acercó la líder del campamento."


translate spanish zhenya_route_8fd2dc1f:



    mtp "Aquello resultó ser incómodo..."


translate spanish zhenya_route_3dcba555:



    "Comenzó a decir mientras ordenaba sus ideas."


translate spanish zhenya_route_9b4a0f57:



    mtp "Tus padres me llamaron y..."


translate spanish zhenya_route_56a89359:



    mtp "¡Pero podrías haberte comportado más apropiadamente! ¡Por lo menos presentarte desde un principio!"


translate spanish zhenya_route_8aa18321:



    "Dijo en tanto su expresión cambió."


translate spanish zhenya_route_61a3b78e:



    mtp "Pero eso no es nada, haremos un pionero ejemplar de ti."


translate spanish zhenya_route_051e0836:



    "Antes le habría explicado a ella en detalle tanto sobre los pioneros como la ejemplificación, pero ahora mismo simplemente no tenía deseo de hablar... ni con ella, ni con nadie."


translate spanish zhenya_route_d6438854:



    mtp "Pues muy bien. Para empezar deberías conocer a tus camaradas."


translate spanish zhenya_route_22da79ff:



    "La líder del campamento hizo una señal con su mano, invitando a alguien a la mesa y dos muchachas se nos aproximaron."


translate spanish zhenya_route_f05e1e1c:



    mtp "Bueno, disfruta tu comida, tengo más cosas que hacer."


translate spanish zhenya_route_7ca779be:



    "Dijo insegura y se marchó dejando solas a las pioneras conmigo."


translate spanish zhenya_route_430dffff:



    mip "Hola, eres nuevo, ¿verdad? ¿Te gusta la música? ¡Únete a nuestro club de música! Aunque solamente estoy yo ahora mismo, pero contigo seremos dos, será divertido, ¿no crees?"


translate spanish zhenya_route_96dfe183:



    "No la miré y continué comiendo atentamente."


translate spanish zhenya_route_001db536:



    mzp "Déjalo solo, no ves que es como un tarado."


translate spanish zhenya_route_e662491f:



    "Si la primera muchacha era solamente un poco tonta, la segunda la compararía con la gata cascarrabias de internet."


translate spanish zhenya_route_e94109d2:



    "Mmm, ¿y si...?"


translate spanish zhenya_route_8fcc767a:



    "En realidad nunca he hecho esto antes, sencillamente nunca pasó por mi mente."


translate spanish zhenya_route_33bd02a0:



    "Bueno, si no sé los acontecimientos futuros ahora, pues... ¿por qué no darle una oportunidad?"


translate spanish zhenya_route_e95a8512:



    "Me levanté en silencio, cogí el vaso de compota y se lo vertí por encima de la cabeza a la segunda pionera."


translate spanish zhenya_route_264353ba:



    pi "Bon appétit."


translate spanish zhenya_route_6e8444c6:



    "Resumí este acto de vandalismo con una voz insensible y me dirigí hacia la salida de la cantina."


translate spanish zhenya_route_1741b8aa:



    "La muchacha dio un alarido, chilló, resopló e incluso gimió de alguna manera con voz ronca, pero no trató de detenerme."


translate spanish zhenya_route_58fe7b4e:



    "Justo ahora querría estar en alguna otra parte, mirar al mundo desde fuera, observar el sufrimiento de los demás desde las alturas de mi experiencia."


translate spanish zhenya_route_d425dea5:



    "Pero hoy día ya no hay esos lugares."


translate spanish zhenya_route_d58c2a5b:



    "En la entrada choqué con aquella primera muchacha que encontré en las puertas, traté de rodearla, pero..."


translate spanish zhenya_route_16321643:



    "Recibí una hiriente bofetada en la cara."


translate spanish zhenya_route_56937f22:



    "El sonido inmediatamente resonó por la cantina, con su eco rebotando en el tejado."


translate spanish zhenya_route_ccd4b759:



    "En un momento, hubo un completo silencio."


translate spanish zhenya_route_7d790b8b:



    "En menos de un segundo mi mano se cerró en un puño, las uñas hundiéndose en la palma, la piel de mis nudillos se estiró casi hasta el punto de romperse..."


translate spanish zhenya_route_f644afb1:



    "Iba a darle un puñetazo a esta zorra..."


translate spanish zhenya_route_d8822251:



    "Tal vez fui lento por una fracción de segundo... entre nosotros, como si saliera de la tierra, saltó la pequeña pionera que lancé a los arbustos, quien extendió sus manos a ambos lados, protegiendo su amiga con su cuerpo pequeño."


translate spanish zhenya_route_3dead8bb:



    "Y ése fue el final de eso..."


translate spanish zhenya_route_8ce3cecf:



    "Dejé de cerrar mi puño, mi fuerza de voluntad se fue, agaché mi cabeza, me escurrí entre ellas y abandoné la cantina."


translate spanish zhenya_route_6d636a33:



    "¿Acaso realmente resultaban ser más fuertes que yo?"


translate spanish zhenya_route_b0c195bf:



    "¿Por qué incluso sucede algo como esto?{w} Antes habría..."


translate spanish zhenya_route_125b75d4:



    "Incluso el porqué «habría»... nada me detuvo tantas veces."


translate spanish zhenya_route_74647d36:



    "¡Golpear a una muchacha no era nada!{w} Hice peores cosas."


translate spanish zhenya_route_e33546f5:



    "¡Es como si este mundo hubiese cambiado sin informarme!"


translate spanish zhenya_route_ce617998_5:



    "..."


translate spanish zhenya_route_e85ecdd6:



    "Los anillos de humo del cigarrillo flotaban lentamente hacia Genda."


translate spanish zhenya_route_3865b2a1:



    "El paquete ya está casi vacío y no hay otro lugar donde conseguir tabaco por aquí, tendré que esperar otra semana..."


translate spanish zhenya_route_68877b02:



    "Obviamente podría ir con los hermanos autistas, coger la botella de vodka y emborracharme, ¿pero qué cambiaría eso?"


translate spanish zhenya_route_632060af:



    "Decisiones, tan complicadas ahora y tan simples entonces..."


translate spanish zhenya_route_514f9a11:



    "Puaj, ¡¿comencé a pensar como aquel maldito perdedor ya?!{w} ¡No, nunca!"


translate spanish zhenya_route_8236fc12:



    dvp "¿Tienes alguno más?"


translate spanish zhenya_route_96e9ab02:



    "Otra vez una muchacha pionera se sentó junto a mí."


translate spanish zhenya_route_3b3e0f18:



    pi "¿Qué?"


translate spanish zhenya_route_b7ca5301:



    "Pregunté confundido."


translate spanish zhenya_route_eb197aa4:



    "Ella señaló el cigarrillo."


translate spanish zhenya_route_b5c90a40:



    pi "¡Fumar es perjudicial para tu salud!"


translate spanish zhenya_route_a96c9711:



    dvp "Mira quién fue hablar."


translate spanish zhenya_route_473574eb:



    "Ella sonrió."


translate spanish zhenya_route_b861b089:



    pi "La mía es de goma. ¿Quieres comprobarlo?"


translate spanish zhenya_route_3c4c2708:



    dvp "No hace falta, lo daré por cierto."


translate spanish zhenya_route_698cea6f:



    "Le di un cigarrillo y le di fuego."


translate spanish zhenya_route_b7315f93:



    dvp "Hiciste algo guay hoy... bueno..."


translate spanish zhenya_route_62f34328:



    "Dijo tras dejar de toser."


translate spanish zhenya_route_0adf4ebc:



    dvp "Y la líder del campamento en su choza... lo escuché. Y en la cena. ¡Por lo menos ahora ya no soy la mayor desgracia del campamento!"


translate spanish zhenya_route_f427a0c4:



    "La miré atentamente."


translate spanish zhenya_route_0cc3480e:



    "La pionera estaba fumando el cigarrillo esporádicamente y sonriendo tan complacida como si el mundo entero estuviera al tanto de su corrección."


translate spanish zhenya_route_01e0ab71:



    pi "Y tú, veo, sólo piensas en ti misma."


translate spanish zhenya_route_8e823649:



    dvp "¿Qué? ¿De qué estás hablando? Yo... no..."


translate spanish zhenya_route_1f8ac785:



    "Su expresión cambió, incluso entró un poco en pánico y se asfixió con el humo."


translate spanish zhenya_route_126ad096:



    pi "Pero tienes razón. Tú nunca fuiste la peor de todos aquí. Aunque no estoy tan seguro sobre mí mismo."


translate spanish zhenya_route_32eb32a4:



    dvp "Estás hablando... tan... bueno... ¡descaradamente!"


translate spanish zhenya_route_df5a5407:



    "Ahora, su alabanza era ciertamente todo lo que necesitaba."


translate spanish zhenya_route_ed866e5b:



    "¿O, quizá...?"


translate spanish zhenya_route_ecd96042:



    pi "Escucha..."


translate spanish zhenya_route_c1b91905:



    "Empecé, haciendo una de esas sonrisas estudiadas."


translate spanish zhenya_route_3ed5b05c:



    pi "Ya que estamos en ello, ¿te apetecería un encuentro para echar un trago, qué dices?"


translate spanish zhenya_route_48a95115:



    dvp "¿Tienes alguna bebida?"


translate spanish zhenya_route_63d5e899:



    "Los ojos de la pionera destellaron."


translate spanish zhenya_route_f90a9790:



    pi "¡Siempre puedo hallar algo para una buena camarada!"


translate spanish zhenya_route_ce617998_6:



    "..."


translate spanish zhenya_route_b4f43180:



    "Aunque todo comenzó como en una novela pésima que se publica por capítulos en un periódico amarillista, resultó ser completamente diferente."


translate spanish zhenya_route_217e4924:



    "Los recuerdos se mezclaban juntos en una continua neblina de un tono rozijo anaranjado, como en una carretera de ladrillos amarillos conduciendo hacia la ciudad Esmeralda."


translate spanish zhenya_route_70fa3abc:



    "A pesar de que Dorothy creció un poquito y se gana la vida prostituyéndose, porque en vez de estudiar diligentemente ella estaba totalmente atrapada por los sueños de una tierra mágica."


translate spanish zhenya_route_e2cae742:



    "El cobarde león se quedó enganchado a las metanfetaminas y fue disparado en un tiroteo por la policía."


translate spanish zhenya_route_94326c75:



    "Ahora, el Hombre de Hojalata tenía una buena vida... se ha vuelto un sicario."


translate spanish zhenya_route_1ffdfc71:



    "Y el Espantapájaros incluso no cambió en absoluto, consiguió por sí mismo un trabajo estable y mal pagado en una oficina con un práctico horario entre las nueve y las seis, y con ausencia de carrera profesional."


translate spanish zhenya_route_8549b557:



    "Probablemente no había ninguna ciudad mágica delante para empezar."


translate spanish zhenya_route_cddcc89f:



    "Una carretera sin fin, sin destino o significado."


translate spanish zhenya_route_c0fbc843:



    "Ya sabes, en aquel momento realmente {i}él{/i} estaba hablando sobre algo como esto, trató de avisarme, por lo menos. No estuve escuchando y como consecuencia acabé como el único atrapado en esta maldita banda de Moebius."


translate spanish zhenya_route_03818c7f:



    "Ella estaba sentada cerca haciendo una miradita con disgusto."


translate spanish zhenya_route_488f7b42:



    mz "¡Ey! ¡Hola, hola, aquí la Tierra!"


translate spanish zhenya_route_226ec868:



    me "Vale, ¿qué pasa? ¿Es que no puedo tener un momento para pensar?"


translate spanish zhenya_route_509526cd:



    mz "Los pensamientos deberían conducir hacia un resultado, ¿y qué es lo que has conseguido?"


translate spanish zhenya_route_7062bae2:



    me "Bueno, ¿qué he conseguido?"


translate spanish zhenya_route_b1e74574:



    mz "¡Careces de resultados!"


translate spanish zhenya_route_9524b395:



    me "¡Carecer de resultados sigue siendo un resultado!"


translate spanish zhenya_route_83479432:



    "Zhenya me pellizcó dolorosamente en el hombro y se alejó un metro."


translate spanish zhenya_route_ce617998_7:



    "..."


translate spanish zhenya_route_67b69ec7:



    "Y luego ella apareció."


translate spanish zhenya_route_274f7aad:



    "No, Zhenya... las Zhenyas siempre han estado en Sovyonok, pero en aquel momento vi mi Zhenya por primera vez."


translate spanish zhenya_route_23e4b0e5:



    "Quizá no recuerdo qué sucedió antes y qué ocurrió luego tras eso, ¡pero aquellos pocos días siempre permanecerán en mi memoria!"


translate spanish zhenya_route_ce617998_8:



    "..."


translate spanish zhenya_route_4f00cb19:



    "Ya me he dado cuenta claramente de que éste no es mi mundo congelado, comprendí que una posterior eternidad que tuve otra vez, como la que tuve antes, acabó por desmoronarse sobre otra."


translate spanish zhenya_route_8ac15cc8:



    "Sucedió accidentalmente... como si por un segundo las órbitas de los dos planetas se cruzaran y únicamente yo tuviera un instante."


translate spanish zhenya_route_27a7f3cc:



    "Si hubiera estado un segundo más tarde se habrían ido volando la una de la otra por siempre, alejándose más y más con estupenda velocidad."


translate spanish zhenya_route_71441e64:



    "Este mundo es tan solitario y vacío como lo era el anterior."


translate spanish zhenya_route_3c572706:



    "¡Pero la cosa más importante en él es que es diferente!"


translate spanish zhenya_route_16cc93e0:



    "Y hay cosas aquí que me son desconocidas."


translate spanish zhenya_route_b518e850:



    "¡Oh, esto es de verdad importante, es muy importante para mí!"


translate spanish zhenya_route_b1f78b5b:



    "Por ejemplo, Zhenya sentada no en la biblioteca, sino en el porche de la cantina."


translate spanish zhenya_route_56aa7a6d:



    "Tras todo lo que ocurrió, no era capaz de hacer sarcasmo."


translate spanish zhenya_route_c8d5ca9e:



    pi "Hola."


translate spanish zhenya_route_1364bea0:



    "Dije en tanto me acercaba, pero aun así deteniéndome a una distancia prudencial."


translate spanish zhenya_route_0df38c29:



    "Zhenya alzó su mirada hacia mí. Su rostro dejaba entrever que no esperaba encontrarse nadie aquí."


translate spanish zhenya_route_7c7aa84b:



    mzp "Hola..."


translate spanish zhenya_route_e68a1186:



    "Y su voz no sonaba como de costumbre... ¡no me equivocaría con la entonación de la bibliotecaría con cualquiera otra!"


translate spanish zhenya_route_63994ee4:



    me "¿Decidiste cenar más temprano?"


translate spanish zhenya_route_065c7777:



    mzp "No... No lo sé... ¿Cuándo cenamos otra vez?"


translate spanish zhenya_route_37c6ffc1:



    "Dijo con labia."


translate spanish zhenya_route_cfdaa1d8:



    me "¿Qué quieres decir con «cuándo cenamos»?"


translate spanish zhenya_route_9e2a8fe8:



    "Su respuesta me dejó con estupor y rompí con una sonrisa, a pesar de que un escalofrío recorrió mi cuerpo entero."


translate spanish zhenya_route_b1f8319d:



    mzp "Ah, no, lo siento, por supuesto que sé cuando es la cena, es solamente que... bueno..."


translate spanish zhenya_route_a334b9dd:



    "Zhenya estaba completamente confusa y miraba fijamente sus pies."


translate spanish zhenya_route_4f43b195:



    "Parecía que la última cosa que quisiera ahora mismo fuera estar aquí, pero no tuviera suficiente resolución para marcharse."


translate spanish zhenya_route_490c71fc:



    "Era extraño, pero no me asustaba para nada... por el contrario, estaba feliz porque algo nuevo, más allá del alcance de la rutina, ¡ha ocurrido!"


translate spanish zhenya_route_9933d8d3:



    pi "¿Te encuentras bien?"


translate spanish zhenya_route_396cd3e5:



    "Hice la pregunta más estúpida posible en dicha situación."


translate spanish zhenya_route_abee5180:



    "¿Qué esperaba escuchar como respuesta?"


translate spanish zhenya_route_12f3639c:



    mz "Sí, gracias."


translate spanish zhenya_route_9508d41b:



    me "¿Por qué gracias?"


translate spanish zhenya_route_be9c5dd6:



    mz "No lo sé. Bueno, por estar preocupado."


translate spanish zhenya_route_5242e190:



    me "Bueno yo sólo..."


translate spanish zhenya_route_f50a4f7c:



    "Un leve matiz de disgusto apareció en su rostro, pero desapareció inmediatamente y Zhenya lentamente se levantó."


translate spanish zhenya_route_8a961b45:



    mz "Me tengo que ir."


translate spanish zhenya_route_35daecd9:



    me "Espera, ¿pero no ibas a ir a la cantina?"


translate spanish zhenya_route_6e2024d8:



    mz "Vendré más tarde, tengo otras cosas que hacer."


translate spanish zhenya_route_505d91ae:



    me "Vale..."


translate spanish zhenya_route_2a4f9fde:



    "No la detuve, pero decidí seguirla."


translate spanish zhenya_route_44b86c7f:



    "¡Dios sabe cuánto tiempo hace desde que no me ha ocurrido algo como eso!"


translate spanish zhenya_route_850d89b8:



    "Tal vez únicamente en las primeras veces..."


translate spanish zhenya_route_92e3d697:



    "Zhenya llegó a la plaza y rápidamente se fue en dirección a la biblioteca."


translate spanish zhenya_route_f6928ff9:



    "No se daba la vuelta al caminar, así que no tuve que estar escondiéndome mucho."


translate spanish zhenya_route_38cda274:



    "Cuando la puerta se cerró tras ella, me quedé de piedra dudando."


translate spanish zhenya_route_7bb3941c:



    "¿Y bien, y ahora qué? ¿Debería llamar y entrar? ¿Pero qué le contaré?"


translate spanish zhenya_route_ddf58ea4:



    "Uno no necesitaría ser un detective para darse cuenta de que he estado siguiéndola."


translate spanish zhenya_route_f287617d:



    "Fui sacado de mis cavilaciones por Electronik, quien apareció frente a mí salido de la nada."


translate spanish zhenya_route_3fa63434:



    elp "¿Qué, viniste a pasar la lista?"


translate spanish zhenya_route_8e512950:



    pi "¿Eh? Claro, tan sólo decidí visitar la biblioteca."


translate spanish zhenya_route_d848bc68:



    elp "Lo entiendo, no te distraeré pues."


translate spanish zhenya_route_10ebed14:



    "Dijo mientras se iba a ir ya."


translate spanish zhenya_route_b0b2d0e2:



    pi "Espera.{w} ¿No crees que últimamente Zhenya ha estado, bueno... comportándose de forma extraña, supongo?"


translate spanish zhenya_route_257fa208:



    "Electronik me miró atentamente, incluso como valorando."


translate spanish zhenya_route_dcb8b7df:



    elp "¿Ocurrió algo?"


translate spanish zhenya_route_ac5548d9:



    me "No, nada..."


translate spanish zhenya_route_0ba7fa61:



    "Era como si estuviera inventándome excusas y fuera preso de una sensación de insignificancia... ¡por qué debería inventarme excusas con él!"


translate spanish zhenya_route_1c8d2b4a:



    pi "¡Solamente pregunto! ¡¿Qué pasa, es que no puedo hacer preguntas ya?!"


translate spanish zhenya_route_e121c03e:



    elp "No, desde luego que puedes..."


translate spanish zhenya_route_12d357d2:



    "Dijo con nervios."


translate spanish zhenya_route_9c4cad90:



    elp "Quizás ella ha estado un poco rara. Supongo que me he percatado de algo de eso hace un par de días."


translate spanish zhenya_route_d889c13c:



    pi "¿De verdad? ¿Y de qué manera se ha manifestado esa rareza?"


translate spanish zhenya_route_40ac813e:



    elp "No estoy seguro, está mucho más segura de sí misma que de costumbre, o alguna cosa. ¿Crees que deberíamos hablar con ella?"


translate spanish zhenya_route_c090949c:



    "Iba a decirle «ciertamente no deberías», pero me abstuve."


translate spanish zhenya_route_49ac3d86:



    pi "Vale, olvídalo. Quizá solamente está de ese humor."


translate spanish zhenya_route_094c845e:



    elp "¿Qué humor?"


translate spanish zhenya_route_d6ae043c:



    pi "¡Que no hay humor! {i}Ése{/i}... supongo, ¿acaso no eres uno de nuestros futuros científicos?"


translate spanish zhenya_route_fd51a2f5:



    "Respondí súbitamente."


translate spanish zhenya_route_e1d27f94:



    "Electronik pensó por un instante... como si ponderara el destino del mundo... y luego asintió comprensivamente."


translate spanish zhenya_route_8734c583:



    elp "Vale."


translate spanish zhenya_route_c2c38b1e:



    "Alcé los brazos un poco y agaché la cabeza, indicando que si no tenía más asuntos que tratar conmigo, pues que sería mejor para él volver a seguir construyendo robots, y hacerlo cuanto antes fuera posible."


translate spanish zhenya_route_809c2d88:



    "Por supuesto, fue un poco lento, pero esta vez me comprendió a la primera."


translate spanish zhenya_route_5c848e46:



    "Cuando Electronik se marchó me senté, mi espalda se apoyó contra el árbol cerca de la biblioteca, de tal forma que no podía ser visto desde las ventanas y empecé a pensar."


translate spanish zhenya_route_71cfb74b:



    "Para ser más específicos, traté de pensar, porque desastrosamente no habían suficientes hechos para un análisis comprensivo de la situación."


translate spanish zhenya_route_1627ebc3:



    "Cuando el empapelado de la pared de tu habitación cambia del blanco al negro puedes estar sorprendido, asustado, asumir que ellos lo han recoloreado sin tu conocimiento, enfadarte con el que lo hizo..."


translate spanish zhenya_route_26e9bd8b:



    "Pero todas ellas son emociones."


translate spanish zhenya_route_14f931ad:



    "Un hecho en dicha situación sería una nota en la mesa diciendo algo así como «querido, pensé que sería mejor estar preparados para la eternidad de antemano, así que cambié el tono de los interiores por uno más adecuado»."


translate spanish zhenya_route_90a606a4:



    "Y qué he tenido en mis manos hasta este momento: detrás de mí... una enorme cantidad de ciclos y bucles; ahora... el extraño comportamiento de la bibliotecaria; y al frente... lo desconocido."


translate spanish zhenya_route_8b53b821:



    "No exactamente una sólida base sobre la que construir teorías de cualquier tipo."


translate spanish zhenya_route_bf4b9a89:



    "¡Oh, y era un maestro de las teorías, si es que lo era! Sin embargo todo eso fue en el pasado, porque en algún momento dejé de preocuparme completamente sobre por qué exactamente estoy destinado a pasarme una eternidad en este campamento."


translate spanish zhenya_route_0bf3ea31:



    "¡Puaj, otra vez!"


translate spanish zhenya_route_f04073f4:



    "E independientemente de lo que escoja, ¡todo esto no es como yo!"


translate spanish zhenya_route_dc78bf83:



    "No creo en eso y probablemente nunca lo hice."


translate spanish zhenya_route_21ad4eef:



    "Es como si alguien hubiera puesto palabras extrañas en mi cabeza. ¿Dejé de ser yo mismo cuando llegué a un mundo extraño?"


translate spanish zhenya_route_d0ea6aef:



    "La hierba crujía traicioneramente bajo mis pies, alertando de mi presencia. Estaba entrando a hurtadillas en la biblioteca, tratando de descubrir qué sucedía en su interior."


translate spanish zhenya_route_56b6bff2:



    "Zhenya estaba sentada tras una mesa, mirando hacia delante."


translate spanish zhenya_route_2a36c9b6:



    "Incluso sin... una idea se me pasó por mi cabeza."


translate spanish zhenya_route_2e2c0707:



    "Se le veía perpleja, quizá desafortunada, como alguien a quien se le dijo que le quedan tan sólo unas pocas horas de vida."


translate spanish zhenya_route_928e244d:



    "Desearía decirle que todo está bien, que en cinco días todo se volverá a repetir otra vez, y luego otra..."


translate spanish zhenya_route_2ebd4d06:



    "¿Pero cómo puede incluso {i}estar bien{/i} eso?"


translate spanish zhenya_route_8b397834:



    "Es como contarle a un hombre enfermo que él morirá en cinco días, pero que no debería preocuparse porque en siete días tras eso, ¡volverá a morirse de nuevo!"


translate spanish zhenya_route_443724fc:



    "Cerca de diez minutos transcurrieron pero Zhenya ni se movió. A veces incluso me parecía que ella ni respiraba."


translate spanish zhenya_route_416886f8:



    "Ahora empecé a darme cuenta con seguridad que eso no era simplemente raro, ¡esto {i}lo{/i} es! ¡La cosa que me lanzó en este nuevo mundo!"


translate spanish zhenya_route_48b5e529:



    "Solamente tenía que escoger por dónde comenzar."


translate spanish zhenya_route_f942d401:



    "Podría preguntarle sin rodeos, explicarle mi historia..."


translate spanish zhenya_route_a563b120:



    mtp "¡Ah, aquí estás!"


translate spanish zhenya_route_d863b12a:



    "La líder del campamento estaba detrás mío."


translate spanish zhenya_route_364c8304:



    "Me giré bruscamente y dije irritado:"


translate spanish zhenya_route_ec8eb135:



    pi "¿Qué es lo que quieres?"


translate spanish zhenya_route_e5143970:



    "Como siempre, como solía hacerlo."


translate spanish zhenya_route_6f62e389:



    mtp "¿Para qué estás indeciso aquí? ¿Olvidaste que necesitas una firma de la biblioteca también?"


translate spanish zhenya_route_18359698:



    "La líder del campamento parecía no prestar atención a mis palabras y a mi tono."


translate spanish zhenya_route_b9be1462:



    "No como de costumbre, pero como hace mucho tiempo atrás."


translate spanish zhenya_route_4959d934:



    "Junto a mis recuerdos de mi primera aparición en este campamento me vinieron las ya olvidadas imágenes de los lugareños... aquellos que juzgué ser personas en aquel momento, no simples decoraciones."


translate spanish zhenya_route_780f294e:



    pi "Es sólo que, verás..."


translate spanish zhenya_route_fdd19e6d:



    mtp "No quiero ni escucharlo. ¡Consigue todas las firmas antes de la cena!"


translate spanish zhenya_route_6f46c062:



    pi "Desde luego, solamente iba a ver..."


translate spanish zhenya_route_5228d6ae:



    mtp "¡Pues ve!"


translate spanish zhenya_route_5e43c0de:



    "Olga Dmitrievna me siguió hasta la puerta con su mirada, así que no tuve que escoger qué hacer... la líder del campamento me {i}ayudó{/i}."


translate spanish zhenya_route_0dda31b4:



    "La biblioteca olía como a polvo de libro, a antiguos muebles soviéticos y a un poquitín de humedad."


translate spanish zhenya_route_653614f9:



    "Zhenya estaba sentada y no me prestaba atención, y pareció que solamente recuperó los sentidos cuando cerré la puerta."


translate spanish zhenya_route_c95e0c61:



    pi "Hola de nuevo."


translate spanish zhenya_route_e5989bb1:



    "Comencé con dudas."


translate spanish zhenya_route_91f4ad81:



    "Después de todo, estaba acostumbrado a cierta rutina de comportamiento y comunicación con los lugareños del campamento y era difícil cambiar las formas."


translate spanish zhenya_route_c9281090:



    "¡Y esta líder de campamento no me dejó prepararme tampoco!{w} ¡Desearía estrangularla con su panamá como siempre!"


translate spanish zhenya_route_b83eb7ab:



    "Zhenya no contestó, pero siguió mirándome con cautela."


translate spanish zhenya_route_8c8749a0:



    pi "Vine para algo así como tener firmada la lista..."


translate spanish zhenya_route_195cbb9a:



    mzp "Vale, trae aquí."


translate spanish zhenya_route_addf2dc6:



    "Respondió impasiblemente."


translate spanish zhenya_route_3abc9bf6:



    "Empecé a hurgar en mis bolsillos y entonces me di cuenta..."


translate spanish zhenya_route_d14407b5:



    "No, ¡tiene que estar sucediéndome alguna cosa completamente errónea!"


translate spanish zhenya_route_28462b26:



    "Debería estar acostumbrado a todo después de todo este tiempo, ¡y me estoy comportando como uno de primer grado!{w} Más concretamente... como un {i}primerizo de temporada{/i} en los términos de este mundo."


translate spanish zhenya_route_057d924f:



    pi "Creo que la perdí..."


translate spanish zhenya_route_86695cb0:



    "Dije con cara molesta."


translate spanish zhenya_route_a2dd85af:



    mzp "Muy mal."


translate spanish zhenya_route_acd3da05:



    "Respondió con la misma sosa y desinteresada voz."


translate spanish zhenya_route_369b2b04:



    "Me quedé de piedra en algún tipo de superposición, si me doy la vuelta y me voy ahora, luego... ¿luego qué?"


translate spanish zhenya_route_404fa019:



    "Pero en aquel momento la determinación provino de quién sabe donde."


translate spanish zhenya_route_53cfe566:



    pi "¿Puedo hacerte una pregunta?"


translate spanish zhenya_route_e9ebecff:



    mzp "¿Qué pregunta?"


translate spanish zhenya_route_c0ce709a:



    "De verdad, ¿cuál?"


translate spanish zhenya_route_7fcb23e3:



    "«¿No crees que han estado desalando la comida de la cantina últimamente?», «Las noches se han vuelto bastante frías, ¿verdad?», «Oh, por cierto, ¿sabes que soy un visitante del futuro?»..."


translate spanish zhenya_route_283f6bd3:



    pi "¿Te encuentras bien? Últimamente parece que estás molesta con alguna cosa."


translate spanish zhenya_route_632a1baf:



    "El rostro de Zhenya mostró un vistazo de interés."


translate spanish zhenya_route_1a27f095:



    mzp "Pero no nos conocemos el uno al otro."


translate spanish zhenya_route_eb7317c7:



    pi "Sí, claro, es sólo que... Te vi ayer y pensé..."


translate spanish zhenya_route_d9cd8027:



    "Me iba enredando más y más en mentiras."


translate spanish zhenya_route_e2013459:



    "La incapacidad para decir la verdad me conducía lejos hacia el autismo y la reducción de mi vocabulario."


translate spanish zhenya_route_ccc2d340:



    mzp "También te vi ayer, corriendo por el campamento, pero aun así no nos conocemos."


translate spanish zhenya_route_65f82f5f:



    pi "No, no nos conocemos pero yo... me habló de ti Slavya, ¡claro! Sois compañeras de habitación, ¿verdad?"


translate spanish zhenya_route_46665132:



    mzp "¿Y qué te contó ella?"


translate spanish zhenya_route_07d63e82:



    "Poco a poco Zhenya estaba empezando a emerger de su condición contemplativa."


translate spanish zhenya_route_f3d36d37:



    pi "Nada."


translate spanish zhenya_route_b3d08fc6:



    "Dejé ir un suspiro inútil y me dejé caer pesadamente sobre una silla cerca de la puerta."


translate spanish zhenya_route_b26c8325:



    "Afuera de las ventanas muchos de los pájaros estaban cantando sus canciones, los grillos grillaban, en alguna parte el agua chapoteaba y, bien lejos, se escuchaban las alegres risas de los niños."


translate spanish zhenya_route_7deb2005:



    "Pero todo eso estaba... más allá de los muros de la biblioteca. Aquí dentro solamente estamos nosotros dos, yo y Zhenya... o para ser más precisos, una muchacha que se parece a Zhenya."


translate spanish zhenya_route_0fb35f2f:



    "No había miedo, sino más bien asombro."


translate spanish zhenya_route_79a8f2f4:



    pi "Asumamos que yo, no soy yo."


translate spanish zhenya_route_eae3d435:



    mzp "¿Entonces quién eres?"


translate spanish zhenya_route_3797aeb4:



    "Por un segundo incluso me sentí insultado de que ella no supiera quien soy."


translate spanish zhenya_route_7b7b5828:



    "¡Por qué yo! Oh, ese yo, ¿ése? Sí, sí, ¡ese tipo!"


translate spanish zhenya_route_6fa6e3c2:



    "¡Los pioneros de muchos mundos han contado leyendas sobre mí!"


translate spanish zhenya_route_33615421:



    "Pero esta concreta Zhenya en este específico mundo, parece que no tuviese ni idea de quién soy y qué estoy haciendo aquí, y nuestra conversación que parecía haber empezado a interesarle se volvió en un inútil parloteo."


translate spanish zhenya_route_49aeaf7e:



    pi "¿A qué me parezco?"


translate spanish zhenya_route_bebec5bf:



    "Zhenya me echó un vistazo."


translate spanish zhenya_route_1704c62e:



    mzp "¿A un pionero?"


translate spanish zhenya_route_d6914ece:



    pi "Sí, claro, un pionero, eso es lógico, ¡bonita broma!"


translate spanish zhenya_route_fea0eb84:



    "Sonreí enérgicamente."


translate spanish zhenya_route_7f7b1369:



    mzp "No estaba bromeando."


translate spanish zhenya_route_37114254:



    pi "Es sólo que yo me veo como soy, y tú no."


translate spanish zhenya_route_261dc859:



    mzp "¿Que no me veo como yo misma?"


translate spanish zhenya_route_4a9dd7d0:



    pi "¡Que no te ves como tú misma, por Dios!"


translate spanish zhenya_route_9fc4efee:



    "El asalto final ha comenzado."


translate spanish zhenya_route_9ca493ef:



    mzp "¿En qué sentido?"


translate spanish zhenya_route_62482817:



    "Se asombró."


translate spanish zhenya_route_dd75e4f3:



    pi "¡En el sentido de que tú no eres Zhenya! ¡No, quiero decir que no eres la bibliotecaria local!"


translate spanish zhenya_route_332c7b34:



    mzp "¿Pues quién soy?"


translate spanish zhenya_route_b749f9b4:



    "Movió su mirada hacia un lado, como si mirara hacia algo en las estanterías."


translate spanish zhenya_route_6f01edd9:



    pi "No lo sé, por eso pregunto."


translate spanish zhenya_route_d9435aaa:



    mzp "Si ésta es otra broma..."


translate spanish zhenya_route_916dd930:



    pi "¿Broma?"


translate spanish zhenya_route_7dbdbf2d:



    mzp "Bueno, ya sabes, todo el mundo lo sabe..."


translate spanish zhenya_route_340ec68e:



    "Ella se avergonzó y se ruborizó."


translate spanish zhenya_route_27b31d2a:



    pi "No, no lo sé."


translate spanish zhenya_route_d191e01e:



    mzp "Lo de la chincheta en mi silla... Me fui a por un libro y mientras lo buscaba, Alisa..."


translate spanish zhenya_route_29245267:



    "Zhenya se quedó mirando al suelo, sus ojos se humedecieron con lágrimas."


translate spanish zhenya_route_5dabba5b:



    pi "No, no escuché nada sobre eso."


translate spanish zhenya_route_0eb20230:



    "Aunque si lo piensas, algo así estaba sucediendo en anteriores ocasiones... Alisa decidió gastarle una broma."


translate spanish zhenya_route_9375f9d2:



    "Es tan sólo que no le presté ninguna atención a ello entonces."


translate spanish zhenya_route_be40d38a:



    "Y Zhenya reaccionó de manera distinta también, creo."


translate spanish zhenya_route_945283c8:



    "Alisa..."


translate spanish zhenya_route_ce617998_9:



    "..."


translate spanish zhenya_route_3c8c8c92:



    dvp "¡Déjame en paz!"


translate spanish zhenya_route_e3f1127e:



    "Murmuró la muchacha pionera mientras se abrochaba su camisa, apenas moviendo su boca."


translate spanish zhenya_route_b5f2f446:



    "El club de cibernética estaba bañado por la brillante luz lunar, en la mesa había una botella vacía con un pulcro charco de vodka derramado alrededor."


translate spanish zhenya_route_b53b097a:



    "Mis mejillas estaban ardiendo y la cabeza me daba tantas vueltas que incluso no podía comprender por qué exactamente. Pero difícilmente sólo por el alcohol."


translate spanish zhenya_route_81275182:



    dvp "Si te crees que soy de ésas, ¡pues ya te puedes ir al infierno!"


translate spanish zhenya_route_e04b78e8:



    "Los dedos de la muchacha no parecían escucharla, el botón estaba saltando hacia atrás y hacia delante, pero rechazaba volver a su apropiado lugar."


translate spanish zhenya_route_965504ff:



    pi "¿Claaaaaro?"


translate spanish zhenya_route_4dd24508:



    "Perdí mi paciencia, agarré la pionera por las manos, la empujé contra la mesa y le arranqué la camisa abriéndosela con mis dientes."


translate spanish zhenya_route_ba65372b:



    pi "¿Pues cómo entonces? Pues cómo eres tú, ¡te estoy preguntando! No es una pregunta retórica... ¡respóndeme!"


translate spanish zhenya_route_93a9420e:



    "No quería cambiar nada, ¡todo encajaba conmigo!"


translate spanish zhenya_route_f2b8909a:



    "¡Vivía mi vida habitual, en mi mundo habitual, haciendo lo que quería!"


translate spanish zhenya_route_d1d1c36c:



    "¡¿Por qué demonios necesitaría esta novedad?!"


translate spanish zhenya_route_5db0b7d7:



    pi "¡Quieres esto, lo sé! ¡Dar unos tragos de vodka con un extraño cinco minutos después de conocerlo! ¿Que no es así como eres? ¡A la mierda con todos vosotros! ¡Haced como siempre fue!"


translate spanish zhenya_route_b1c84289:



    "Estaba perdiendo el control sobre mí mismo, sobre la situación, sobre el propio mundo..."


translate spanish zhenya_route_b99db903:



    dvp "¡Suéltame, me haces daño!"


translate spanish zhenya_route_28eb9141:



    "La muchacha estaba llorando."


translate spanish zhenya_route_0da4bcdf:



    "Pero la estuve sosteniendo firmemente y, en un minuto, aflojó y cerró los ojos, únicamente con unas esporádicas lágrimas calientes y saladas brotando de ella."


translate spanish zhenya_route_78f90845:



    pi "Lo ves ahora, eso está mucho mejor."


translate spanish zhenya_route_8572d521:



    "La estaba sobando por todas partes bruscamente, respirando rápida y ruidosamente en ello."


translate spanish zhenya_route_6ca28273:



    pi "¡Porque eres mía! ¡Porque todas sois mías aquí! ¡Este mundo entero es mío!"


translate spanish zhenya_route_74cdf889:



    "Pero la muchacha no se estaba resistiendo y, repentinamente, perdí todo interés, incluso me sentí disgustado."


translate spanish zhenya_route_c3d0fb09:



    pi "Y no te necesito, ¡hay otras mejores aquí!"


translate spanish zhenya_route_a834cad6:



    "La solté y me caí al suelo, exhausto."


translate spanish zhenya_route_7a01683b:



    "La pionera no se movió, solamente continuó sollozando en silencio."


translate spanish zhenya_route_1887db4f:



    pi "¡Puedo tener a cualquier muchacha de aquí! Cualquiera, ¡me escuchas! Y luego cogeré el cuchillo de carnicero y... ¿Crees que no ha sucedido antes?"


translate spanish zhenya_route_9bd59498:



    "Le estaba gritando."


translate spanish zhenya_route_35794ca1:



    pi "¡¿Crees que puedes decirme lo que debo hacer?! ¡Éste es mi mundo, me escuchas, mío! ¡Todos vosotros ni existís! ¡Hablo conmigo mismo!"


translate spanish zhenya_route_c9bc6d68:



    pi "¡Deja de lloriquear!"


translate spanish zhenya_route_6b0ebc18:



    "Perdí los últimos ápices de control sobre mí mismo, me puse en pie y me apoyé con todo mi peso sobre la muchacha."


translate spanish zhenya_route_0c326074:



    "Parecía estar cerca de desmayarse ya."


translate spanish zhenya_route_d1490e2d:



    "Me incliné hacia delante y le susurré en su oído:"


translate spanish zhenya_route_db4eee40:



    pi "¿Sabes lo que he estado haciendo contigo? Tantas veces... ¿Quieres escucharlo? ¡Oh no, escucha ahora y te lo contaré!"


translate spanish zhenya_route_d47d3ccb:



    "Repentinamente me entró el pánico."


translate spanish zhenya_route_748351d4:



    "Ha pasado un largo e infinito tiempo desde el preciso {i}comienzo{/i}, pero con cada segundo de ello he estado «viviendo», era consciente de mí mismo como persona, estaba controlando la situación en este mundo... y mucho más antes de ello."


translate spanish zhenya_route_ba6bab34:



    "Y para ser la primera vez que tenía una sensación de estar desapareciendo, ¡de que en cualquier instante me esfumaría!"


translate spanish zhenya_route_2f2f0661:



    "¿Cómo puede ser eso, soy YO, no lo ves? ¿No te acuerdas...?"


translate spanish zhenya_route_1eb562e0:



    dvp "Eres un debilucho..."


translate spanish zhenya_route_c7cfef75:



    "El susurro de la muchacha me llegó como si fuera de otro mundo."


translate spanish zhenya_route_1ac9eb61:



    dvp "Un patético debilucho, un don nadie. Me compadecería, pero no te puedes compadecer de una cosa que está vacía."


translate spanish zhenya_route_fd0f9179:



    "Me aparté de ella aterrorizado."


translate spanish zhenya_route_7f9a4127:



    "En el rostro de la pionera se había quedado congelada una mueca de repulsión, las lágrimas desaparecieron como si nunca hubieran estado ahí."


translate spanish zhenya_route_8eccaba8:



    "Se levantó de la mesa, dio un par de pasos hacia mí y se detuvo."


translate spanish zhenya_route_ce617998_10:



    "..."


translate spanish zhenya_route_01793405:



    mz "¿Por qué me cuentas a mí todo esto?"


translate spanish zhenya_route_455e39f2:



    "Preguntó Zhenya perezosamente, pasando una página."


translate spanish zhenya_route_c9efc482:



    "Las ondas cruzaban a través del agua, por encima nuestro se mecían las hojas de un abedul, con brillantes rayos de luz del fatigoso sol de medio día resplandeciendo a través de las hojas."


translate spanish zhenya_route_3dd25e16:



    "Los pájaros estaban cantando ruidosamente en la arboleda, y los pequeños insectos escondidos entre la hierba alta, estaban ajetreados saltando sobre mis piernas."


translate spanish zhenya_route_b92f4659:



    pi "Bueno, ¿por qué estás leyendo ese libro? ¡No me digas que es la primera vez!"


translate spanish zhenya_route_0584c94e:



    mz "Me gusta. Verás, a ti te gusta narrar y a mí me gusta leer."


translate spanish zhenya_route_b801ccf7:



    "En la otra orilla, en la playa, los pioneros estaban divirtiéndose, Olga Dmitrievna estaba corriendo tras Ulyana y en medio del río distinguí la dorada cabeza de Slavya.{w} ¿Podía estar ella nadando aquí?"


translate spanish zhenya_route_bf61ccfa:



    mz "¿Y bien?"


translate spanish zhenya_route_1d91a5d4:



    pi "¿Qué?"


translate spanish zhenya_route_9efbba46:



    mz "¿Qué sucedió luego?"


translate spanish zhenya_route_1ecf5995:



    pi "Pensé que no estabas interesada."


translate spanish zhenya_route_17ce821c:



    mz "Tú lo estás, a pesar de todo."


translate spanish zhenya_route_beecbb90:



    "Sonrió Zhenya."


translate spanish zhenya_route_ce617998_11:



    "..."


translate spanish zhenya_route_e2ddd404:



    "Ella me escupió en la cara y se fue caminando fuera, cerrando de un portazo tras de ella."


translate spanish zhenya_route_ec4fada4:



    "Y me dejó solo ahí."


translate spanish zhenya_route_4d9c3879:



    "¿Qué? ¿Es este... el fin?"


translate spanish zhenya_route_d02cc081:



    "Hasta cierto punto lo estaba esperando, quizá incluso con impaciencia."


translate spanish zhenya_route_b963c577:



    "Pero fue hace tanto tiempo que ya ni lo recuerdo..."


translate spanish zhenya_route_aa69bc1b:



    "¿Y ahora qué?"


translate spanish zhenya_route_fb8066ab:



    "Estos muñecos, ¿estos desalmados muñecos destruyeron mi mundo que he estado construyendo en toda mi vida...?{w} ¿De esa forma, en un solo día...?"


translate spanish zhenya_route_6072604a:



    "Pasé mi mano por encima de mi mejilla y vi mis dedos cubiertos de algo húmedo."


translate spanish zhenya_route_ce617998_12:



    "..."


translate spanish zhenya_route_3690f9af:



    "La noche había descendido hacia rato sobre el campamento, escondiendo a los pioneros locales bajo su manto, ocultando su malicia y su astucia."


translate spanish zhenya_route_a97066dd:



    "Y estaba tan sólo sentado en el club de cibernética, mis brazos rodeando mis rodillas, lentamente meciéndome a lado y lado."


translate spanish zhenya_route_78432774:



    "Tal vez esa muchacha pionera volverá otra vez ahora, a fin de cuentas podría humillarme con mucha más saña.{w} O incluso hacerme peores cosas..."


translate spanish zhenya_route_b724315a:



    "Pero el tiempo transcurrió y únicamente había el cantar de los pájaros nocturnos y el grillar de los grillos silenciándose."


translate spanish zhenya_route_5dafa38c:



    "Y el alba está cerca también, el campamento se despertará entonces y comenzará a vivir su vida, la cual se había vuelto repentinamente tan extraña para mí."


translate spanish zhenya_route_9b97de77:



    "Bueno da igual, al cuerno con ello..."


translate spanish zhenya_route_f388c609:



    "Me puse en pie con algo de dificultad y miré a la botella vacía de vodka en la mesa."


translate spanish zhenya_route_f56a8f65:



    "Sería fantástico perder el conocimiento ahora y despertarme mañana como si nada hubiera ocurrido, olvidar lo de hoy, ¡aparecer en un mundo donde suelo estar acostumbrado a los comportamientos de los pioneros por ser familiares y predecibles!"


translate spanish zhenya_route_7f6ffa46:



    "Y todo comenzó con aquella muchachita..."


translate spanish zhenya_route_48e2be22:



    "Ella nunca había llorado antes."


translate spanish zhenya_route_930e94bf:



    "¿O quizás ella lo hizo y simplemente nunca presté atención? Incluso si eso es verdad, ¿y qué?"


translate spanish zhenya_route_69a2c6d6:



    "¿Qué? ¿Conviértete en un debilucho, siéntete mal por esa pequeña mierda, ponte a {i}su{/i} nivel, en fin?"


translate spanish zhenya_route_039e2cb1:



    "No, ¡tengo que reponerme!"


translate spanish zhenya_route_d565336d:



    "Abandoné el club de cibernética, cerrando de un fuerte portazo, y aspirando el fresco aire nocturno."


translate spanish zhenya_route_18c48e62:



    "Qué extraño, no se sentía tan artificial como habitualmente."


translate spanish zhenya_route_786313e6:



    "¿Y bien, a dónde debería ir?"


translate spanish zhenya_route_c86ccfda:



    "¡Supongo que atrapar a esa muchacha pionera y tomar venganza por el escupitajo en la cara merecería la pena!"


translate spanish zhenya_route_2d3dd531:



    "Antes no habría olvidado nunca semejante insulto, ¡y no lo olvidaré ahora!"


translate spanish zhenya_route_cac4acc5:



    "No había ni una sola alma en la plaza aparte de un pionero sentado en un banco."


translate spanish zhenya_route_1eaf6c8a:



    "No le presté atención al principio... quién sabe cuántos retrasados moran aquí... pero él me llamó:"


translate spanish zhenya_route_b5f924fc:



    pi2 "¡Ey, Semyon!"


translate spanish zhenya_route_a0e4c394:



    "¡A quién diantres le llamas Semyon, maldita sea!{w} Siseé en voz baja."


translate spanish zhenya_route_e16dba53:



    "Pero el pionero no se tranquilizaba:"


translate spanish zhenya_route_4a9d1da0:



    pi2 "¿Debes estar buscando a alguien? Pero ella no se fue por ahí."


translate spanish zhenya_route_020a3c0b:



    "Me quedé de piedra, pero inmediatamente me giré y rápidamente caminé hasta él."


translate spanish zhenya_route_b26f9b1a:



    pi "¿Entonces la viste?"


translate spanish zhenya_route_faffbf87:



    pi2 "¡Por supuesto que la vi!"


translate spanish zhenya_route_e9213e07:



    "Una sonrisa desagradable apareció en su rostro, casi una sonrisita, una maleducada, expresando únicamente la superioridad de uno frente a sus oponentes."


translate spanish zhenya_route_dd90be7e:



    pi "Pues explícalo con detalle, ¡aquí no estamos jugando a las adivinanzas!"


translate spanish zhenya_route_0035a9a8:



    pi2 "Espera, ¿para qué tienes tanta prisa? Siéntate, hablemos con calma para empezar."


translate spanish zhenya_route_1d25a44c:



    pi "No tengo nada de qué hablar contigo."


translate spanish zhenya_route_d656bbab:



    "El pionero pensó por un instante, luego hurgó en los bolsillos de sus pantalones cortos, sacó un paquete de «Cosmos» y me lo tendió."


translate spanish zhenya_route_bfadd262:



    pi2 "¿Quieres alguno?"


translate spanish zhenya_route_990a64cb:



    pi "Esto... ¿dónde lo conseguiste?"


translate spanish zhenya_route_da5e25e1:



    "¡No recuerdo que tuvieran cigarrillos los pioneros de siempre!"


translate spanish zhenya_route_35ea6809:



    "Aunque han ocurrido tantas cosas en las últimas veinticuatro horas que nunca antes habían sucedido..."


translate spanish zhenya_route_49df45e9:



    "Saqué lentamente uno de los cigarrillos y le devolví el paquete al extraño pionero."


translate spanish zhenya_route_9075cb57:



    "Le estuvo dando vueltas entre sus manos por un rato y luego me lo tiró de vuelta."


translate spanish zhenya_route_ef0baff0:



    pi2 "Qué va, quédatelo, me largo."


translate spanish zhenya_route_7302d938:



    "¡Una vez más esa soberbia sonrisa!"


translate spanish zhenya_route_b9b224aa:



    "Al infierno con esa, ¡necesito atrapar a aquella insolente zorra!"


translate spanish zhenya_route_e5239908:



    pi "¿Y bien? ¡Dime dónde se fue!"


translate spanish zhenya_route_b0bacf80:



    pi2 "¿Para qué necesitas a esa pobre muchacha? Ella es insignificantemente pequeña comparada con el Universo y tal vez incluso ni existe en éste."


translate spanish zhenya_route_6e286439:



    "Pensó por un instante, luego continuó."


translate spanish zhenya_route_9a27b7d6:



    pi2 "Lo mismo vale para ti. ¿Estás seguro de que existes? E incluso si es así, ¿de que tu existencia aquí y ahora es la verdadera? ¿Quizá realmente existías antes, y ahora ya no?"


translate spanish zhenya_route_0c2d21db:



    pi2 "¿O existirás en el futuro y por ahora tan sólo es un muñeco lo que está frente a mí?"


translate spanish zhenya_route_7b1709f6:



    "La palabra «muñeco» me ardió dolorosamente en mi cabeza, como si una colt de calibre .45 me volara la tapa de los sesos, esparciendo el contenido del cráneo por los suelos."


translate spanish zhenya_route_3851cbd0:



    "Claro, luego sería el turno de Slavya para limpiarlo por la mañana...{w} Sin querer hice una sonrisa tonta para mí mismo."


translate spanish zhenya_route_aef5fe7f:



    pi "¿Quién eres?"


translate spanish zhenya_route_797cca45:



    "La muchacha insolente que escupió en mi cara hacía rato que se fue volando de mi cabeza junto con mi cerebro."


translate spanish zhenya_route_b49e136e:



    pi "Quién eres, ¡te estoy preguntando!"


translate spanish zhenya_route_3286fb9d:



    "Le agarré por el pañuelo del cuello y le estiré hacia mí, con tal fuerza que el maldito trapo rojo hizo un ruido y casi se rasga."


translate spanish zhenya_route_d4433ae7:



    "Pero el pionero solamente sonreía."


translate spanish zhenya_route_522e4cdd:



    pi2 "Tranquilízate, no te preocupes tanto. Es imposible morir aquí... ya deberías saberlo, hiciste eso cientos de veces."


translate spanish zhenya_route_393c873a:



    "Me aparté de él aterrorizado."


translate spanish zhenya_route_d92d4988:



    "Esta frase...{w} ¡La he escuchado ciertamente antes!"


translate spanish zhenya_route_de5bcfba:



    "No, no de esa forma... ¡La he dicho yo mismo!"


translate spanish zhenya_route_aff47745:



    "Hace mucho tiempo, en mi anterior... una de mis anteriores vidas."


translate spanish zhenya_route_c313ce95:



    pi2 "Porque tú eres Semyon.{w} Yo soy Semyon. Porque tú eres yo."


translate spanish zhenya_route_530a8a5f:



    "El banco y el pionero bruscamente se alejaban cientos de metros de mí, como si me estuviera cayendo de una montaña rusa, únicamente su desgradable sonrisita permanecía frente a mis ojos."


translate spanish zhenya_route_6ecb5b42:



    "Y luego el mundo fue cubierto por las tinieblas..."


translate spanish zhenya_route_ce617998_13:



    "..."


translate spanish zhenya_route_b052fa58:



    me "Bueno, y entonces desaparecí."


translate spanish zhenya_route_9b867c85:



    mz "¿Así tal cual?"


translate spanish zhenya_route_0482fa2e:



    "Zhenya mantenía sus ojos pegados en el libro, pero la historia parecía empezarle a interesar."


translate spanish zhenya_route_8b0f35ac:



    me "¿Por qué no? La partida puntual es una habilidad genial, ya sabes, ¡y estoy hecho un maestro de ella si lo soy!"


translate spanish zhenya_route_eab0814c:



    mz "Claro, eres un maestro de todo..."


translate spanish zhenya_route_57d8f4cb:



    "Ella dejó el libro y me abrazó."


translate spanish zhenya_route_257596c3:



    "Slavya, quien estaba claramente nadando en nuestra dirección, pareció darse cuenta de ello y se dio la vuelta hacia la playa."


translate spanish zhenya_route_6258eed6:



    me "¡Espera! ¡Aun no llegué a la mejor parte!"


translate spanish zhenya_route_a2112eb3:



    me "Y no aquí, después de todo..."


translate spanish zhenya_route_3b51a4ca:



    "Tímidamente me aparté de ella."


translate spanish zhenya_route_ce05b572:



    "A Zhenya claramente no le gustó eso y se alejó de mí."


translate spanish zhenya_route_02d00f0e:



    mz "Estoy cansada de estas historias tuyas... ¡todas son siempre iguales! Déjame contarte la mía para variar."


translate spanish zhenya_route_b401bc94:



    me "Me pregunto sobre qué..."


translate spanish zhenya_route_43f0b201:



    mz "¡Cómo llegué hasta aquí, por ejemplo!"


translate spanish zhenya_route_6a25d8e4:



    me "Ya la he escuchado cientos de veces."


translate spanish zhenya_route_46e49702:



    mz "¡Y yo he escuchado las tuyas doscientas veces!"


translate spanish zhenya_route_a20cefa7:



    "..."


translate spanish zhenya_route_7372fd7b:



    "Para Zhenya, Sovyonok no comenzó con nuestro encuentro y la hoja de lista."


translate spanish zhenya_route_776cce52:



    "Porque siempre tenía, tengo y tendré {i}mi{/i} primer día de la temporada después de que ella haya pasado ya una semana en el campamento."


translate spanish zhenya_route_6b6478fd:



    mz "¿Conoces esa sensación cuando el tiempo se ha detenido? ¿Cuando no puedes morir, incluso si de verdad quieres?"


translate spanish zhenya_route_edad5831:



    me "Bueno, claro. ¿Como aquí, en este campamento?"


translate spanish zhenya_route_0766c187:



    mz "No, aquí siempre fue fácil para mí. Las personas se comportan de forma más honesta, apropiada, nadie incordia a alguien, no hay nadie, por lo menos, que sea malvado como lo eran entonces en todas partes, {i}antes{/i}, ¿te acuerdas?"


translate spanish zhenya_route_cbc4bb84:



    "No respondí."


translate spanish zhenya_route_0b199ede:



    mz "Simplemente puedo leer libros, hablar con la gente, alguien me necesita, alguien me considera una amiga, tengo responsabilidades..."


translate spanish zhenya_route_72a4670a:



    "Parecía que Zhenya estuviera a punto de romper a llorar."


translate spanish zhenya_route_360485e5:



    mz "Está bien, por qué me pongo... De todas maneras, esta vez tan sólo me desperté por las excesivas sacudidas del autobús 410. ¿Has cogido alguna vez ese autobús?"


translate spanish zhenya_route_b91f0b42:



    "Asentí."


translate spanish zhenya_route_ed2494b4:



    mz "Bueno yo nunca lo hice, nosotros no tenemos ni tan siquiera líneas de autobús con tres dígitos en nuestra ciudad."


translate spanish zhenya_route_13c9611e:



    mz "Todavía no comprendí realmente qué había sucedido y la muchacha sentada a mi lado me dice: «Hola, mi nombre es Slavya»."


translate spanish zhenya_route_73f9c19e:



    mz "¿Slavya? ¿Qué Slavya? ¿Quién es Slavya?"


translate spanish zhenya_route_3d09c388:



    mz "Desde luego estaba confundida, pero traté de no mostrarme así. Entonces llegamos al campamento y las cosas empezaron a ponerse feas..."


translate spanish zhenya_route_385c7f99:



    me "Y luego volvió a suceder de nuevo..."


translate spanish zhenya_route_9d45a0d2:



    mz "Y luego otra vez..."


translate spanish zhenya_route_be100ad4:



    "Zhenya suspiró."


translate spanish zhenya_route_8110044b:



    mz "Y te he visto muchas veces. A ti con Slavya, con Alisa, con Lena..."


translate spanish zhenya_route_a150cf10:



    "Ella apretó los dientes en silencio."


translate spanish zhenya_route_66a94d6b:



    me "¡Pero ése no era yo! ¡Ya hemos tratado eso cientos de veces, ¡y ahora vuelves a empezar otra vez!"


translate spanish zhenya_route_09769e7d:



    mz "Tú, tú no... ¡sois todos como dos gotas de agua! «Yo» estuve también en tus campamentos, ¿correcto? ¡Pero ella no era como mi yo real!"


translate spanish zhenya_route_cf5fe3d9:



    me "¡Exacto! ¡Distinguir el original del falso no es tan difícil como te crees!"


translate spanish zhenya_route_2a487144:



    mz "Sí, porque los falsos se renuevan cada vez y los originales se quedan."


translate spanish zhenya_route_931c85d9:



    me "Eso no es del todo cierto..."


translate spanish zhenya_route_90aa467c:



    mz "¡Como si lo supieras todo!"


translate spanish zhenya_route_a6c61dc1:



    me "¡¿Qué importa?!"


translate spanish zhenya_route_b9e0fc87:



    "Abracé a Zhenya estrechamente y cerré mis ojos."


translate spanish zhenya_route_a20cefa7_1:



    "..."


translate spanish zhenya_route_f38c94d0:



    "Me desperté cubierto de sudor, mi corazón latiendo a punto de explotar."


translate spanish zhenya_route_95485d35:



    "El amanecer del sol era insoportablemente caluroso, a pesar de que no era mucho más tarde de las seis o las siete de la mañana a juzgar por el aspecto."


translate spanish zhenya_route_7c0f70f5:



    "¿Qué demonios me ocurrió?{w} El siniestro pionero... ¿qué fue aquello?"


translate spanish zhenya_route_b8fea11b:



    "¿Tal vez solamente fue un sueño, una alucinación?"


translate spanish zhenya_route_6d64ec4f:



    "Ahora mismo estoy preparado para creerme eso...{w} ¡En cualquier cosa!"


translate spanish zhenya_route_b1ec62eb:



    "Alguien se acercaba caminando lentamente hacia mí cruzando la plaza."


translate spanish zhenya_route_54e2fa21:



    "Me quedé mirando fijamente y reconocí a la líder del campamento."


translate spanish zhenya_route_f2c53477:



    mtp "¿Ya estamos otra vez así?"


translate spanish zhenya_route_a339aad1:



    "Me dijo amenazadoramente, inclinándose sobre mí."


translate spanish zhenya_route_d619304f:



    "Eso me hizo levantarme del banco y dar un paso atrás."


translate spanish zhenya_route_00676949:



    mtp "Y creí que lo de ayer era solamente un malentendido, y tú... ¡Estoy sin palabras! ¡Hacerle algo así a una muchacha!"


translate spanish zhenya_route_7fbc5e40:



    "¿De qué está hablando?"


translate spanish zhenya_route_a6084c5b:



    "¡¿Qué muchacha, qué tiene que ver conmigo maldita sea?!"


translate spanish zhenya_route_0790d314:



    "Por lo menos la líder del campamento se veía como de costumbre, eso me calmó un poco."


translate spanish zhenya_route_fe2f533c:



    mtp "¿Y bien? ¡Respóndeme ahora! ¿Crees que te saldrás con la tuya esta vez? Llamaré a la policía, ¡incluso lo pondré en conocimiento a la Secretaría de la Organización de la Unión del Komsomol!"


translate spanish zhenya_route_e0de5854:



    "Ella estaba soplando tanto que incluso se puso roja y empezó a jadear."


translate spanish zhenya_route_7ca78ad4:



    mtp "¡En mi campamento! ¡Sí, tú! ¡Te voy a...!"


translate spanish zhenya_route_b745da71:



    "Por norma general, este comportamiento de la líder del campamento no me sorprendía ni una pizca."


translate spanish zhenya_route_71972c56:



    "Pero me sentí inquieto porque mi mente todavía estaba retornando a los acontecimientos de la pasada noche, dejando la huella deprimente y dolorosa de una sensación cercana a la vergüenza sobre alguna cosa hecha durante mi borrachera."


translate spanish zhenya_route_563df633:



    "La cosa más extraña era que no me preocupaba de los fuertes gritos de la líder del campamento a poco más de un metro de mí."


translate spanish zhenya_route_17ef50dc:



    pi "Cállate ya, ¡¿vale?! ¡Gritar así lo empeora! ¿Tú qué sabes si hay un problema? ¿Crees que tienes un problema? Oh-oh-oh, una pionera fue casi violada, ¡qué desastre!"


translate spanish zhenya_route_e625c257:



    "Apreté mis dientes e hice una mirada amenazadora a la líder del campamento que por un instante le produció miedo en su rostro."


translate spanish zhenya_route_88d9cfee:



    pi "¡¿Qué tal si te cuento los problemas {i}reales{/i}?! ¡No esos de tu pequeño mundo imaginario!"


translate spanish zhenya_route_7eb5bc4b:



    pi "Aun así, ¡vete al infierno! ¡Sólo eres una pérdida de tiempo!"


translate spanish zhenya_route_156bb973:



    "Por otra parte, tengo más que suficiente tiempo de ése..."


translate spanish zhenya_route_cf548d39:



    "Una horrorosa idea se me cruzó por la mente de repente: ¿qué pasa si todo va a ser siempre {i}así{/i} ahora...?"


translate spanish zhenya_route_af5d6b63:



    "¿Qué pasaría si pierdo el control sobre este mundo para siempre?"


translate spanish zhenya_route_1073c912:



    "¿Cualquier muchacha pionera sería capaz de escupirme en mi cara cuando quisiera y la líder del campamento me tendría que regañar como a Electronik...?"


translate spanish zhenya_route_6efa0286:



    "¡Pero eso es peor que la muerte!"


translate spanish zhenya_route_4764ccbb:



    pi "De todas formas, me tengo que ir..."


translate spanish zhenya_route_2b1ad4f4:



    "Empujé a la líder del campamento quien todavía no se había recuperado del trastorno, y me fui a dondequiera que me llevasen mis piernas."


translate spanish zhenya_route_96ba2c48:



    "En serio, ¿qué es la muerte personalmente para mí?"


translate spanish zhenya_route_717b489b:



    "Parece como se hiciera mucho tiempo desde que dejé de comprender el concepto básico de la idea."


translate spanish zhenya_route_d4c4a4be:



    "Porque si no puedo desaparecer, si estoy destinado a vivir en la misma semana de siempre en este maldito campamento una y otra vez, ¿entonces quizá la muerte me abandonó?"


translate spanish zhenya_route_8a3a5440:



    "¿Abandonándome la autoridad que tengo sobre el mundo, aguardándome la incertidumbre más adelante?"


translate spanish zhenya_route_7e0405f4:



    "¡Pero eso es incluso peor que el destino de cualquier humano {i}normal{/i}!"


translate spanish zhenya_route_735eed4b:



    "Su vida es un juego con un final infeliz, sin capacidad para guardar tu progreso."


translate spanish zhenya_route_1ea7a991:



    "Mi vida es un juego sin ningún final con la capacidad para guardar..."


translate spanish zhenya_route_07e40943:



    "¿Cómo la llamarían {i}ellos{/i} a semejante vida?"


translate spanish zhenya_route_02cd0d67:



    "Si sólo pudiera recordar cómo es... ser un hombre normal..."


translate spanish zhenya_route_d9801473:



    "¡Probablemente ellos lo llamarían un infierno!"


translate spanish zhenya_route_3da4e67a:



    "Y este particular bucle, donde incluso las muñecas desalmadas me humillan, es su último ciclo."


translate spanish zhenya_route_fc47ce7d:



    "Estaba preparado para perder mi cabeza, si solamente pudiera."


translate spanish zhenya_route_4db8f15d:



    "Todos los pensamientos que había estado conteniendo diligentemente, surgieron repentinamente con fuerza nueva, retornando sentimientos olvidados hace mucho tiempo."


translate spanish zhenya_route_f7601ba0:



    "¿Y quién resulta tener razón al final?"


translate spanish zhenya_route_14e500e2:



    "Siempre me estaba convenciendo a mí mismo de que tenía razón, que {i}ellos{/i} no, porque yo estoy vivo y {i}ellos{/i} desaparecían mientras perseguía ilusiones vacías."


translate spanish zhenya_route_aa9cc9b6:



    "Pero {i}ellos{/i} desaparecían y yo me quedaba."


translate spanish zhenya_route_6aec9b7e:



    "¿Y al final quién se quedó la mejor parte...?"


translate spanish zhenya_route_a0734101:



    pi2 "Je je je."


translate spanish zhenya_route_18a14147:



    "Una risita familiar se escucó cerca."


translate spanish zhenya_route_ca7a3d8c:



    pi2 "¡Parece que has comprendido algo hoy!"


translate spanish zhenya_route_5a75dcad:



    pi "Sé quien eres."


translate spanish zhenya_route_ad7879c5:



    "Mi voz sonaba relajada, pero mi corazón estaba latiendo intensamente como nunca antes."


translate spanish zhenya_route_aa40ee3d:



    pi2 "Oh, ¿en serio? Sería interesante escucharlo."


translate spanish zhenya_route_7c80e143:



    pi "Tú eres igual que yo, un pionero, ¡pero de otro bucle!"


translate spanish zhenya_route_dc8a84af:



    pi2 "¿Pero no creíste siempre que los {i}otros{/i} no lo eran más cuando Semyon se bajó del autobús {i}aquella{/i} vez? ¡Ahora creíste que estabas solo en todos estos campamentos sin fin!"


translate spanish zhenya_route_8249e6b5:



    pi "Podría estar equivocado..."


translate spanish zhenya_route_900da79c:



    "Por muy duro que fuera para mí admitirlo, esa posibilidad existía."


translate spanish zhenya_route_3cf57bf2:



    pi2 "¿Tú? ¿Equivocado? ¡Por qué, eso no puede ser!"


translate spanish zhenya_route_2138916b:



    pi "Da igual. ¿Qué necesitas? ¿Tan sólo viniste para burlarte por lo que antes estuve haciendo?"


translate spanish zhenya_route_28216e14:



    pi2 "Lo dices como si...{w} Te sientes culpable."


translate spanish zhenya_route_c7603b8c:



    pi "No me siento culpable y de todas formas, ¡eso no es de tu incumbencia! Simplemente responde a la pregunta."


translate spanish zhenya_route_69b5c4c3:



    pi2 "Bueno, quizá tengas razón..."


translate spanish zhenya_route_aa53adf9:



    "El pionero pensó por un rato."


translate spanish zhenya_route_8c7036b9:



    pi2 "¡Aunque es un pecado reírse de los desdichados!"


translate spanish zhenya_route_6d7221cb:



    "¡Repentinamente sentí un notable deseo de quitarle de un puñetazo esa sonrisa desagradable de su rostro!"


translate spanish zhenya_route_c1143dcc:



    "A fin de cuentas, ¡¿acaso soy una criatura temblorosa, o soy una con derechos?!"


translate spanish zhenya_route_21d61111:



    "¡¿Quién le permitió a {i}él{/i} imponer sus formas en mi mundo?!"


translate spanish zhenya_route_a902c6d7:



    pi "¡Piérdete!"


translate spanish zhenya_route_2323c424:



    "Vociferé, me puse en pie y lancé mi puñetazo hacia él con tanta fuerza como pude..."


translate spanish zhenya_route_26c40b41:



    "Pero mi mano simplemente atravesó una neblina que desapareció en el aire y perdí mi equilibrio, cayendo dolorosamente en el suelo."


translate spanish zhenya_route_ce617998_14:



    "..."


translate spanish zhenya_route_392741f8:



    "Cuando el dolor se calma un poco, me siento como si alguien estuviera cerca."


translate spanish zhenya_route_eac3bbc2:



    "Y ciertamente, desde los arbutos estaba echando un vistazo una pícara muchachita pionera."


translate spanish zhenya_route_15b8af29:



    usp "¡Tienes lo que te mereces!"


translate spanish zhenya_route_f02f9218:



    "Ella sacó su lengua fuera."


translate spanish zhenya_route_319a4b06:



    usp "¡Supongo que no eres tan valiente con tus iguales!"


translate spanish zhenya_route_8a70a572:



    pi "Oh tú..."


translate spanish zhenya_route_4cc464f6:



    "Fue tan inesperado que incluso olvidé lo que quería decir."


translate spanish zhenya_route_3bfb5da7:



    pi "¿No te sorprende en absoluto que un hombre sencillamente se haya disuelto en la nada justo frente a ti?"


translate spanish zhenya_route_754e98b4:



    usp "Bueno...{w} A veces pasa. ¿Quizás es algún truco? No importa de todas formas, ¡te ganaste lo que te buscabas!"


translate spanish zhenya_route_2a81e5c3:



    "No queriendo tolerar por más tiempo estos insultos, bruscamente me puse en pie y me lancé contra ella."


translate spanish zhenya_route_4794ec9c:



    "Pero la muchacha era diestra corriendo a través de los árboles, mientras que yo tenía que perder unos preciosos segundos esquivando las brancas que me cortaban la cara, las lomas prominentes, así como las invisibles zanjas."


translate spanish zhenya_route_f8154d9c:



    "Consiguiendo un par de chichones y unos arañazos en mis ensangrentadas manos, finalmente la atrapé en la plaza."


translate spanish zhenya_route_1435d693:



    pi "¿Qué, no eres tan rápida después de todo, eh? ¡No es tan fácil como correr con algunos retrasados!"


translate spanish zhenya_route_7a05ead8:



    usp "¡Suéltame! ¡Déjame!"


translate spanish zhenya_route_3f110a9b:



    "La muchacha pionera estaba llorando."


translate spanish zhenya_route_c8a389f1:



    "De repente sentí un fuerte golpe en mi espalda, todo me dio vueltas frente a mí y me vine al suelo con tremenda velocidad."


translate spanish zhenya_route_7cb62aa8:



    dvp "¿Así que ahora te van las niñas también?"


translate spanish zhenya_route_fc1209ed:



    slp "¡Eso es, sujetadlo!"


translate spanish zhenya_route_ecefc9dc:



    "Un golpe en el vientre..."


translate spanish zhenya_route_3161f0e8:



    "Instintivamente me cubrí la cabeza con mis brazos."


translate spanish zhenya_route_ad729ea8:



    mip "¡¿No te diviertes ahora, eh?!"


translate spanish zhenya_route_a66487fa:



    "Otro golpe..."


translate spanish zhenya_route_c834ca35:



    "Llegados a ese punto estaba preparado para perder la conciencia, pero, reuniendo fuerzas de flaqueza, me alcé con mis codos y rodé varios metros hacia un lado."


translate spanish zhenya_route_edbb6f89:



    "La sangre estaba brotando hasta mis ojos, convirtiendo la muchedumbre de pioneros que me golpeaban en una espesa niebla roja."


translate spanish zhenya_route_3b6c1c0e:



    mtp "Éste no es, desde luego, el mejor método educativo, pero sí uno bastante efectivo, creo."


translate spanish zhenya_route_00b08032:



    "Vete a saber de dónde diantres provenía la voz de la líder del campamento."


translate spanish zhenya_route_a6717d6b:



    pi "¡¿Qué estáis haciendo?! Ya basta..."


translate spanish zhenya_route_a20cefa7_2:



    "..."


translate spanish zhenya_route_e42400d8:



    mz "¿Leíste Don Quijote de la Mancha?"


translate spanish zhenya_route_e52631bf:



    "Zhenya estaba sentada a mi lado, balanceando alegremente sus pies."


translate spanish zhenya_route_1b25a65d:



    me "Claro, pero a penas me acuerdo ahora de qué iba."


translate spanish zhenya_route_0a5efd62:



    mz "¡Los molinos de viento! Todas tus historias son como luchas con los molinos de viento."


translate spanish zhenya_route_955295f8:



    me "Qué molinos, por favor..."


translate spanish zhenya_route_9af55b9b:



    mz "Y aunque no haya ningún molino, seguro que construirás uno porque sencillamente tienes que luchar contra algo."


translate spanish zhenya_route_17eb90aa:



    me "¡Lo dices como si fuera algún tipo de masoquista!"


translate spanish zhenya_route_3805b28b:



    mz "¿No lo eres?"


translate spanish zhenya_route_b47b6b13:



    "Ella sonrió astutamente."


translate spanish zhenya_route_f1b872bc:



    "Zhenya...{w} ¿Qué haría sin ella?"


translate spanish zhenya_route_520a3a56:



    "A veces pienso que todo lo que me sucedía en este campamento no era tan malo, si al final me esperaba una recompensa."


translate spanish zhenya_route_fc9436ca:



    "La empujé hacia mí y la besé."


translate spanish zhenya_route_d79bf9f9:



    mz "Espera, qué estás... ¿Y si alguien nos ve?"


translate spanish zhenya_route_a064965c:



    "Zhenya se ruborizó y dudó, pero más para aparentar que otra cosa."


translate spanish zhenya_route_6d038d67:



    me "Eso no te importa a menudo."


translate spanish zhenya_route_c19c6b50:



    mz "No, pero...{w} De todas formas, ¿por dónde iba?"


translate spanish zhenya_route_d663f3b1:



    me "¿Los molinos de viento?"


translate spanish zhenya_route_86c65e85:



    mz "Claro... ¡Quiero decir no! ¡Quiero decir que aun no puedes acomodarte! Como si te hubieras montado en aquel autobús aquella vez como las personas {i}normales{/i}..."


translate spanish zhenya_route_4933f750:



    me "Y no te hubiera conocido."


translate spanish zhenya_route_f8936c3d:



    mz "¡Como si me hubieses conocido antes! Siempre Slavya, Alisa, Lena..."


translate spanish zhenya_route_b7edee7e:



    "Se ofendió."


translate spanish zhenya_route_05457ff9:



    me "Ves, ¡eso significa que hice todo bien!"


translate spanish zhenya_route_5034d5ab:



    mz "¡Vete al infierno! ¡El macho alfa con un harén, mis narices!"


translate spanish zhenya_route_c4265473:



    me "Dejemos de hablar del pasado, ¡tenemos toda la vida por delante!"


translate spanish zhenya_route_a20cefa7_3:



    "..."


translate spanish zhenya_route_b56763f0:



    pi3 "¡Ey! ¿Estás bien?"


translate spanish zhenya_route_2847cd45:



    "El agua fría salpicaba en mi rostro y con dificultad, logré abrir mis ojos tapados."


translate spanish zhenya_route_35df17a9:



    "Sobre mí se inclinaba... {w} un pionero."


translate spanish zhenya_route_a02937cb:



    pi "Tú otra vez... ¿Qué, {i}ellas{/i} no terminaron conmigo y decidiste venir a rematarme? Pues bien adelante, ¡manos a la obra!"


translate spanish zhenya_route_7b0d9a0b:



    "En alguna parte dentro de mí estaba incluso feliz de dicho giro de los acontecimientos... ¡por fin este ciclo vicioso se rompe en pedazos!"


translate spanish zhenya_route_cac3b2d5:



    pi3 "¡No, no lo comprendes! No soy...{w} ¡No soy él!"


translate spanish zhenya_route_c5c56f07:



    pi "¿Pues quién eres?"


translate spanish zhenya_route_1804c309:



    "Ya no me importaba, sería tan bonito morir ahora, ¡terminar este bucle e irme a uno nuevo!"


translate spanish zhenya_route_83dddfb4:



    "¡Supongo que nunca antes quise tanto continuar con los bucles interminables de mi {i}vida{/i}!"


translate spanish zhenya_route_dffa8938:



    "La única cosa importante para ellos es obedecer las normas hace tiempo establecidas."


translate spanish zhenya_route_0022762c:



    pi3 "¿No lo comprendes...?"


translate spanish zhenya_route_f7f100c4:



    "Dijo el pionero con angustia."


translate spanish zhenya_route_277c650c:



    pi "No, no, no, ¡no voy a jugar más a estos juegos!"


translate spanish zhenya_route_43104af6:



    "En serio, ¿decidieron ponerme en las circunstancias de {i}ese{/i} idiota?"


translate spanish zhenya_route_d65516a5:



    "¿Acaso es esto algún tipo de videojuego y soy un novato que está verde para ellos?{w} ¡No en mi propio servidor!"


translate spanish zhenya_route_595caacc:



    pi "De todas formas, ¡no me importa!"


translate spanish zhenya_route_6304a54d:



    "Traté de levantarme, pero el dolor me hizo gemir y apoyarme contra el tronco más cercano."


translate spanish zhenya_route_ef375c44:



    "Aunque, si me tengo que pasar cinco días y medio más en este maldito bucle, obviamente tendré que hacer ciertos compromisos."


translate spanish zhenya_route_1b5fc009:



    pi "Vale, no me importa quién eres o por qué estás aquí, pero si de verdad me quieres ayudar, no me opondré."


translate spanish zhenya_route_a0e4cf55:



    "Si de verdad es quien dice ser, entonces puedo esperar ayuda de él."


translate spanish zhenya_route_b82cc3b2:



    pi3 "Sí, por supuesto..."


translate spanish zhenya_route_b3db7e6e:



    pi "Traéme algo para beber pues."


translate spanish zhenya_route_87e7c94d:



    "El pionero se puso en pie rápidamente y desapareció tras los árboles."


translate spanish zhenya_route_d7251a80:



    pi "¿Se fue corriendo hacia la cantina? Qué imbécil."


translate spanish zhenya_route_ce617998_15:



    "..."


translate spanish zhenya_route_4526b54e:



    "El tiempo transcurría traicioneramente lento."


translate spanish zhenya_route_eb8d24cc:



    "Perdí el teléfono... bien durante la pelea, o bien incluso antes de eso, así que no podía decir cuánto tiempo pasó desde el momento en que el pionero se fue a por agua."


translate spanish zhenya_route_93a3cf35:



    "Pero en cualquier caso ya debería haber vuelto."


translate spanish zhenya_route_0748b652:



    "Mis heridas estaban doliendo insoportablemente, parecía que estaba a punto de coger fiebre.{w} ¿Podría ser una infección?"


translate spanish zhenya_route_5eb365a4:



    "¡Podría morir sin más!"


translate spanish zhenya_route_8d439469:



    "¡Por lo menos podrían haberme pateado hasta morir!"


translate spanish zhenya_route_caf72da0:



    "Pero no... tengo que permanecer aquí y esperar hasta el final agonizando."


translate spanish zhenya_route_6c406b5e:



    "¡Esperádme!"


translate spanish zhenya_route_2f974ad0:



    "La próxima vez os enseñaré, ¡os enseñaré a todos!"


translate spanish zhenya_route_ee043568:



    "El miedo a la muerte cesó de existir para mí ya que, sencillamente, uno solamente puede temer lo que le asusta."


translate spanish zhenya_route_3f52e9b2:



    "En mi caso la muerte sería una salvación."


translate spanish zhenya_route_ec79852e:



    "Aunque difícilmente podría creer eso, lo más probable es que la siguiente vez todo vuelva a repetirse, lo cual significa que tendré que enfrentarme, que tendré que luchar por mí mismo, ¡por mi vida!"


translate spanish zhenya_route_6b91e2c7:



    "Igual que las primeras veces."


translate spanish zhenya_route_1103dcdf:



    "Hacerlo todo para no volverme un quejica y un mocoso como {i}aquel{/i} otro."


translate spanish zhenya_route_3b8c1c5e:



    "Apenas logrando ponerme en pie, tropecé yendo hacia el campamento."


translate spanish zhenya_route_dad64e6b:



    "Estaba oscureciendo."


translate spanish zhenya_route_d6a7f5b9:



    "Debería haberle preguntado probablemente al pionero qué sucedió tras desvanecerme durante la paliza que recibí en la plaza."


translate spanish zhenya_route_331a0e55:



    "¿Quizá me están buscando?"


translate spanish zhenya_route_73be3703:



    "¿O creen que ya morí?"


translate spanish zhenya_route_e64dfe95:



    "En cualquier caso debería ir con cuidado."


translate spanish zhenya_route_a32be48c:



    "Ahora mismo las diversas habilidades conseguidas durante el tiempo que transcurrí en todos estos bucles sin fin me serían de utilidad."


translate spanish zhenya_route_fbda2b81:



    "Pero un manco lisiado difícilmente puede ser un buen luchador..."


translate spanish zhenya_route_a20cefa7_4:



    "..."


translate spanish zhenya_route_df258bf7:



    "A primera vista la plaza parecía vacía... sin pioneros, sin pájaros, ni una de las ardillas que habitaban abundantemente el bosque por los alrededores del campamento."


translate spanish zhenya_route_4ceb9268:



    "Y el aire estaba sonando con un peligroso silencio, como la calma antes de la tormenta, o como un campo de minas bordeado tanto por gente como por bestias."


translate spanish zhenya_route_db1b0a8d:



    "Da un paso y saltarás en pedazos."


translate spanish zhenya_route_1625539c:



    "Si de verdad sólo fuera tan fácil..."


translate spanish zhenya_route_e4410ee1:



    "Pero no puedo quedarme quieto y esperar... tengo que ir a la enfermería, limpiarme y vendarme las heridas, tomarme unos analgésicos y antibióticos."


translate spanish zhenya_route_131ab1ed:



    "Dar los primeros pasos por la plaza eran difíciles de hacer, más causada por la agobiante tensión que por mis heridas de guerra."


translate spanish zhenya_route_51b712f7:



    "De alguna manera me arrastré hasta llegar allí, caminé a través del estrecho camino, alcancé la enfermería y ya había empezado pelearme con la cerradura cuando, de repente, escuché un crujido familiar en los arbustos cercanos."


translate spanish zhenya_route_fcb1e9ab:



    "Con toda certeza que eso no es una bestia o un pájaro, ¡eso es un humano!"


translate spanish zhenya_route_5b291f33:



    "A pesar de todo, comprendí bien que los violentos pioneros no se esconderían... ellos actuarían al descubierto ahora."


translate spanish zhenya_route_d979f39e:



    pi "No sé quién está escondiéndose ahí, pero será mejor que salgas, o si no..."


translate spanish zhenya_route_58a3975f:



    "¿O si no qué?"


translate spanish zhenya_route_7a18852d:



    "¡Bajo estas condiciones a duras penas puedo suponer una amenaza más grave que la arrogante muchachita!"


translate spanish zhenya_route_d8883300:



    unp "Sólo soy yo..."


translate spanish zhenya_route_7e66674c:



    "La tímida pionera casi susurraba, proviniendo de su escondite."


translate spanish zhenya_route_caa65ec6:



    pi "Oh, ¿eres tú?"


translate spanish zhenya_route_621736c0:



    "Pregunté sarcásticamente."


translate spanish zhenya_route_d3c58e8d:



    pi "¿Sola? ¿Y dónde están los demás? Aunque la verdad, tú sola te bastarás..."


translate spanish zhenya_route_f45fe05b:



    unp "No, yo...{w} Puedo irme si te estoy molestando."


translate spanish zhenya_route_c572cc9a:



    pi "Quédate, no puede ir a peor."


translate spanish zhenya_route_81356c94:



    "Estaba liado en vano con la cerradura."


translate spanish zhenya_route_85f66ba6:



    "Desde luego, no era difícil para mí abrirla incluso con un mondadientes o una cerilla, pero para eso se necesitan unos dedos sanos, mientras que los míos están completamente cubiertos de cardenales y arañazos, cada segunda articulación dislocada."


translate spanish zhenya_route_ff0038ce:



    "La acción más compleja de la que soy capaz ahora, es sostener una bandera pionera y distraídamente ondearla por encima de mi cabeza."


translate spanish zhenya_route_9cacc36a:



    "Una ridícula época pasada por sí misma, pero los pioneros locales pueden obligarme a algo peor."


translate spanish zhenya_route_fb78244d:



    unp "¿Quizá pueda ayudarte?"


translate spanish zhenya_route_8f5ff1f3:



    pi "¿Cómo? ¿Con apoyo moral?"


translate spanish zhenya_route_5ac801c4:



    unp "Tengo una..."


translate spanish zhenya_route_c68a86cc:



    "Se detuvo a media oración y sacó un puñado de llaves acercándomelas."


translate spanish zhenya_route_1537f606:



    pi "¿Cómo?"


translate spanish zhenya_route_92afb189:



    "Sí, no me esperaba algo como esto, pero no estaba realmente sorprendido... tenía otras cosas en mente."


translate spanish zhenya_route_5e6a0cab:



    unp "Las cogí de Slavya."


translate spanish zhenya_route_09d5b397:



    pi "¿Así sin más? ¿Me estás diciendo que ella te las dio sin ni tan siquiera preguntarte por qué las necesitabas?"


translate spanish zhenya_route_28020918:



    unp "Bueno, le dije que no me sentía muy bien y que no quería molestarla, así que podría ir yo misma a la enfermería."


translate spanish zhenya_route_a224ce8d:



    pi "Claro, buen pretexto. Picardia, astucia y excusa. Dicen que detrás de todo gran hombre, hay una una gran mujer."


translate spanish zhenya_route_9a2c43a2:



    "Me acordé del segundo paquete de «Cosmos», aquel que el pionero me regaló."


translate spanish zhenya_route_bb832dfd:



    "Los cigarrillos resultaron estar considerablemente arrugados, pero aun así me las apañé para encontrar un par de ellos intactos."


translate spanish zhenya_route_c56adbe2:



    pi "Vale, las llaves serán de gran utilidad, eso fue algo inteligente por tu parte."


translate spanish zhenya_route_7f5ee04f:



    unp "Gracias."


translate spanish zhenya_route_7346cea3:



    "La muchacha sonrió casi imperceptiblemente."


translate spanish zhenya_route_3bfa5470:



    "¿Y si realmente no está ocultando un cuchillo detrás suyo...?{w} ¡¿A pesar de todo por qué me vienen incluso estos pensamientos?!"


translate spanish zhenya_route_4a5fdb28:



    "La presunción de inocencia nunca había funcionado en este mundo desde hace mucho, especialmente ahora a juzgar que todo el mundo es hostil {i}a priori{/i} sería lo más acertado pensarlo."


translate spanish zhenya_route_bfa3d62d:



    "Miré cuidadosamente a la muchacha de arriba a abajo."


translate spanish zhenya_route_e2bce231:



    unp "¿Qué...? ¿Pasa algo conmigo?"


translate spanish zhenya_route_f40a37e9:



    pi "Tampoco lo sé. Será mejor si me cuentas... ¿va todo bien o no?"


translate spanish zhenya_route_f6774088:



    unp "No te entiendo..."


translate spanish zhenya_route_6bda23f8:



    "La muchacha pionera estaba claramente avergonzada y molesta."


translate spanish zhenya_route_80fdc055:



    "Oh bueno, mientras ella hace ver por lo menos que va {i}bien{/i}, debería usarla. Sería complicado vendarme todas mis heridas por mí mismo, con los brazos rígidos."


translate spanish zhenya_route_4280f591:



    pi "No importa. Si de verdad quieres ayudarme..."


translate spanish zhenya_route_ce617998_16:



    "..."


translate spanish zhenya_route_74cd6038:



    "La enfermería estaba iluminada brillantemente por la lámpara de fluorescente que colgaba del techo."


translate spanish zhenya_route_ecfceff8:



    "Con algo de dificultad trepó hasta el diván y traté de quitarme la camisa."


translate spanish zhenya_route_7aaf2429:



    unp "Déjame ayudarte."


translate spanish zhenya_route_d7c5f11f:



    pi "De acuerdo, adelante."


translate spanish zhenya_route_3df4d52c:



    "Los costados derecho e izquierdo alrededor de mi vientre estaban enrojecidos, con magulladuras con un terrible aspecto."


translate spanish zhenya_route_bbafbb40:



    "Debo haberme roto varias costillas, o tal vez es al revés y solo me quedan unas pocas sin romperse."


translate spanish zhenya_route_6dc32eb8:



    "Cada instante resonaba con dolor por dentro:"


translate spanish zhenya_route_3f894e04:



    pi "¡Aaaargh! ¿Podrías tener más cuidado?"


translate spanish zhenya_route_d9ee0e49:



    unp "Lo siento."


translate spanish zhenya_route_f99441c3:



    "La pionera empezó a desnudarme, casi con entusiasmo."


translate spanish zhenya_route_47c8c54f:



    "Finalmente me quedé sólo con mis pantalones cortos."


translate spanish zhenya_route_b9d2f671:



    unp "Espera, espera, que tenemos que limpiar las heridas con yodo, ¡desinfectarlas!"


translate spanish zhenya_route_80692109:



    pi "Claro, sí bueno, ¿así me vería como un oso pardo? No gracias, hagámoslo con peróxido de hidrógeno... está en el cajón del escritorio."


translate spanish zhenya_route_259d7d24:



    "La pionera no discutió."


translate spanish zhenya_route_edaff1f2:



    "Aunque sea el peróxido de hidrógeno o el yodo... ¡picará igualmente lo mismo!"


translate spanish zhenya_route_208736c6:



    "Tuve que hacer un esfuerzo sobrehumano para no gritar fuerte."


translate spanish zhenya_route_e6e5397a:



    unp "¿Duele?"


translate spanish zhenya_route_62a68b8d:



    pi "¡¿A ti que te parece?!"


translate spanish zhenya_route_b462f311:



    "Siseé apretando los dientes."


translate spanish zhenya_route_5d12af1f:



    "Luego la pionera comenzó a vendarme."


translate spanish zhenya_route_c4e8acee:



    "Cada vez que ella tenía que recoger la venda desde el otro sitio, ella estaba casi apretándose contra mí."


translate spanish zhenya_route_a847a4e2:



    unp "¡Ya está!"


translate spanish zhenya_route_c8bb1a19:



    "Dijo Lena con la sensación de haber hecho un buen trabajo."


translate spanish zhenya_route_64313f9e:



    "Está sobreactuando... se me pasó por la cabeza."


translate spanish zhenya_route_909f6f6a:



    pi "Sí, así está mejor."


translate spanish zhenya_route_6cdbc3d5:



    "No es que me sintiera realmente mejor... más bien estaba más seguro de que estaría casi curado como muy tarde mañana."


translate spanish zhenya_route_98db5fc7:



    "Pero eso será mañana."


translate spanish zhenya_route_ed2b786e:



    "Traté de levantarme para caminar hasta el armario de fármacos, pero mi cabeza repentinamente empezó a dar vueltas y me tuve que sentar en el diván de nuevo."


translate spanish zhenya_route_4d858608:



    pi "Traéme analgin y...{w} y...{w} bueno, eso..."


translate spanish zhenya_route_521a590f:



    "¿De verdad que olvidé el contenido del armario de medicina local?"


translate spanish zhenya_route_ed40cc8c:



    "Alcé una mano hacia mi frente... estaba caliente."


translate spanish zhenya_route_cc8f0f79:



    pi "Algo para el tétanos, no sé. Está escrito ahí, en las instrucciones."


translate spanish zhenya_route_c61d4c44:



    "La muchacha se dio prisa y pronto retornó con un montón de pastillas multicolores."


translate spanish zhenya_route_38a2fca3:



    pi "¿Qué es eso?"


translate spanish zhenya_route_cb469a4d:



    unp "Como pediste..."


translate spanish zhenya_route_27722bff:



    "Estaba tomando un riesgo desde luego, pero claramente no podía ir a peor."


translate spanish zhenya_route_f93b1c3f:



    pi "Y agua para engullirlas."


translate spanish zhenya_route_1cbefc66:



    "Me tragué las pastillas y apenas logré estirarme encima del diván."


translate spanish zhenya_route_7e492c7f:



    "La muchacha pionera se sentó en la silla cercana y empezó a mirarme pacientemente."


translate spanish zhenya_route_45d6e425:



    "¿Qué más necesita ella?"


translate spanish zhenya_route_c822b5dc:



    "El suspense era agotador."


translate spanish zhenya_route_434288d8:



    "Sólo quería dormir, quedarme frito y olvidarme de este mundo de locos por lo menos durante un rato.{w} O mejor aun... ¡despertarme en otro!"


translate spanish zhenya_route_58911e68:



    "¡Dejémosle que terminen conmigo lo que empezaron!"


translate spanish zhenya_route_67aba564:



    "O esta tímida muchacha..."


translate spanish zhenya_route_0f6e53e3:



    pi "¿Y cuánto más vas a estar sentada aquí?"


translate spanish zhenya_route_1691f095:



    unp "Si te molesto, me puedo ir."


translate spanish zhenya_route_6e477f3d:



    pi "No, no me molestas."


translate spanish zhenya_route_57115d23:



    "De repente, ¡se me ocurrió una idea salvaje e insensata!"


translate spanish zhenya_route_5857b475:



    pi "Puedes incluso ayudarme..."


translate spanish zhenya_route_2cf6cc0c:



    "La muchacha me hizo una mirada inquisitiva. "


translate spanish zhenya_route_f7b1fdd6:



    pi "Coge ese frasco, ¿lo ves?"


translate spanish zhenya_route_e2b2bbba:



    "A duras penas pude alzar una mano y señalar la medicina requerida."


translate spanish zhenya_route_b0d079f8:



    pi "Ahora llena una jeringa de eso."


translate spanish zhenya_route_a20cefa7_5:



    "..."


translate spanish zhenya_route_30fbce41:



    mz "¿Lo dices en serio?"


translate spanish zhenya_route_46df7f73:



    "El rostro de Zhenya estaba ardiendo y su voz no sonaba tampoco sorprendida... ¡asustada!"


translate spanish zhenya_route_763ea9a0:



    me "¡Bastante!"


translate spanish zhenya_route_f35360cc:



    mz "No, ¡debiste estar fuera de tus cabales!"


translate spanish zhenya_route_e4748fd7:



    me "¿Qué importancia tiene? Incluso en el peor caso, el nuevo bucle está en un par de días, nadie lo recordará tampoco."


translate spanish zhenya_route_bb4cc545:



    mz "Yo lo recordaré, ¡ya basta!"


translate spanish zhenya_route_39419e54:



    me "¿Entonces tu respuesta es un no?"


translate spanish zhenya_route_c6cacbe6:



    mz "¡Mi respuesta es que dejes de intimidarme!"


translate spanish zhenya_route_af512106:



    "Trató de levantarse, pero la agarré por las manos firmemente."


translate spanish zhenya_route_fe1f361c_2:



    mz "¡Ey!"


translate spanish zhenya_route_3d06e455:



    "En vez de tratar de liberarse, Zhenya se rio y se apretó junto a mí."


translate spanish zhenya_route_96a92f4a:



    mz "¿De verdad que no bromeas? ¿Y qué pasa si nos vamos de aquí después de todo?"


translate spanish zhenya_route_3b02a616:



    me "Hace rato que no has hablado de eso."


translate spanish zhenya_route_7761d947:



    mz "¡No lo hice, pero ahora lo hago! Si en el mundo real te esperas sencillamente olvidar y disimular que no dijiste nada ahora, pues..."


translate spanish zhenya_route_d2551cd3:



    me "¡No, mis palabras son sinceras tanto aquí como en el mundo real!"


translate spanish zhenya_route_5f3b8e22:



    mz "Aun así...{w} ¡Es difícil de creer! Tan repentinamente..."


translate spanish zhenya_route_0af1865c:



    me "¿Qué tiene de repentino? ¿Hace cuánto tiempo que nos hemos conocido el uno al otro? ¡Hace tiempo que perdí la cuenta de los bucles!"


translate spanish zhenya_route_618d05b1:



    mz "Todavía los cuento a pesar de todo..."


translate spanish zhenya_route_3129f2d7:



    "Dijo Zhenya en voz baja y me abrazó incluso estrechándome más."


translate spanish zhenya_route_ce617998_17:



    "..."


translate spanish zhenya_route_c4a91b1d:



    "La muchacha obedientemente rellenó la jeringa con el líquido transparente e incoloro y me la dio."


translate spanish zhenya_route_0fda54f1:



    pi "No, ¡¿cuándo viste que un paciente se diera una inyección a sí mismo?!"


translate spanish zhenya_route_27de15a6:



    unp "Pero no sé cómo hacerlo..."


translate spanish zhenya_route_954d7da0:



    pi "No tiene nada de difícil."


translate spanish zhenya_route_23affce2:



    "Es sólo que para mí en este mundo todavía había un solo y único tabú: el suicidio."


translate spanish zhenya_route_0cffd3d6:



    "Estaba preparado para (e incluso quería) morir a manos de pioneros locos o de una inyección letal, pero acabar con mi propia vida..."


translate spanish zhenya_route_cee6910e:



    "Por alguna razón parecía que no debería, no podía tampoco explicar por qué y nunca traté de verificarlo."


translate spanish zhenya_route_7742b1d1:



    "Quizá todo era a causa de ese miedo a la incertidumbre que se había apoderado de mí en las últimas veinticuatro horas, o tal vez todo es mucho más sencillo y la razón es el simple instinto de supervivencia."


translate spanish zhenya_route_d3f91d78:



    "En cualquier caso, estaba seguro de que si me mata alguien, me despertaré inmediatamente en un nuevo bucle, ¡estoy listo para cualquier cosa!"


translate spanish zhenya_route_be57257f:



    "El único problema era el hecho de que esta muchacha no iba a ayudarme con mi descanso eterno voluntario."


translate spanish zhenya_route_75512fb4:



    unp "No, yo...{w} ¿Y si hago algo mal? ¡Es mejor que avise a la enfermera! Iré, volveré pronto."


translate spanish zhenya_route_f085d7c6:



    "¡La enfermera era la última cosa que necesitaba justo aquí y ahora!"


translate spanish zhenya_route_12645575:



    pi "¡Espera!"


translate spanish zhenya_route_58ec471e:



    "A duras penas me alcé sobre uno de mis codos y le agarré de una mano."


translate spanish zhenya_route_9f0b1268:



    pi "No te preocupes, ¡lo he hecho cientos de veces y te enseñaré a ti también! No tiene nada de complicado."


translate spanish zhenya_route_8d3cf7f8:



    "La muchacha titubeó."


translate spanish zhenya_route_80585789:



    pi "Primero coge el torniquete..."


translate spanish zhenya_route_150c2823:



    "Espera qué, por qué iría a necesitar ella el torniquete, se usa para contener la sangre."


translate spanish zhenya_route_43e5513f:



    pi "Vale, no cojas el torniquete."


translate spanish zhenya_route_dcfbef84:



    "La muchacha pionera dejó el torniquete en la mesa cerca de mí."


translate spanish zhenya_route_12202aea:



    "Moví mi mano izquierda hacia el lado para hacerlo más accesible para ella."


translate spanish zhenya_route_1ce043ab:



    unp "¿Tal vez no deberíamos a fin de cuentas...?"


translate spanish zhenya_route_81d5325a:



    pi "No sentiré tampoco nada."


translate spanish zhenya_route_574f5a8b:



    "Sus manos temblaban, la aguja se movía a izquierda y derecha."


translate spanish zhenya_route_8ef32db4:



    "Si ella falla con la vena no será tan grave."


translate spanish zhenya_route_5bc3003e:



    "La eutanasia por medio de las medicinas de un paquete médico soviético es un lujo precario tal cual..."


translate spanish zhenya_route_4f6d90df:



    unp "No me mires así."


translate spanish zhenya_route_5df5d92e:



    "Dijo la muchacha repentinamente."


translate spanish zhenya_route_1ab9aef2:



    pi "¿Cómo te estoy mirando? ¡No te distraigas!"


translate spanish zhenya_route_11f22c5a:



    unp "No puedo hacerlo así..."


translate spanish zhenya_route_9b411c3c:



    pi "Oh ya estamos..."


translate spanish zhenya_route_de7ff5a4:



    "Hice un suspiro inútil y tiré apartando mi mano de ella."


translate spanish zhenya_route_be9f07ea:



    pi "Vale, olvídalo. Tan sólo dormiré y estaré bien mañana. ¡No te olvides de apagar las luces y de cerrar la puerta cuando te vayas!"


translate spanish zhenya_route_28fb42e2:



    "La muchacha se dirigió hacia la salida obedientemente, pero se detuvo a medio camino."


translate spanish zhenya_route_d40a0991:



    unp "¿De verdad que te duele mucho?"


translate spanish zhenya_route_cd157731:



    pi "¡En serio! ¿Qué importa ahora? Lo aguantaré."


translate spanish zhenya_route_a609ce96:



    unp "Podría hacerlo...{w} Solamente si cierras tus ojos."


translate spanish zhenya_route_15ac0b0f:



    pi "Y si los cierro, ¿qué cambiará?"


translate spanish zhenya_route_b895e892:



    unp "Será más fácil para mí. Como si durmieras."


translate spanish zhenya_route_5e2e3ab6:



    "Sus ojos sonreían."


translate spanish zhenya_route_1c1458d1:



    pi "De acuerdo, los cerraré."


translate spanish zhenya_route_2fdb022d:



    "No parece algo que dé miedo."


translate spanish zhenya_route_9f19ad89:



    "La muchacha caminó hasta mí y cogió la jeringa."


translate spanish zhenya_route_790ba682:



    unp "¿Y bien?"


translate spanish zhenya_route_53a2d83d:



    pi "Sí, sí."


translate spanish zhenya_route_2ffa5e6c:



    "Cerré mis ojos y en el mismo instante se me pasó una idea por mi cabeza: ¿por qué confío incluso tan ciegamente en ella?"


translate spanish zhenya_route_587bd764:



    "Hace justo media hora antes, cuando estábamos en el porche, ¡estaba pensando muy distinto!"


translate spanish zhenya_route_db889481:



    "Algo me apretó contra el diván con una fuerza que me hizo incapaz de mover mis brazos o piernas.{w} ¡Será zorra!"


translate spanish zhenya_route_00986cf7:



    "La muchacha pionera con el torniquete en sus manos se puso encima mío."


translate spanish zhenya_route_2737718a:



    "Un segundo después lo apretó en mi cuello."


translate spanish zhenya_route_047476be:



    pi "¿Qué estás...?"


translate spanish zhenya_route_34a7e6ca:



    "Respiré con dificultad."


translate spanish zhenya_route_a0fcf8d6:



    "Incluso si ésta es la muerte que ansiaba que hiciera, no debería ser así, ¡no siendo estrangulado con un torniquete! ¡No! ¡Espera, no estoy preparado!"


translate spanish zhenya_route_465ef323:



    "La pionera lo estaba apretando más fuerte, con una mueca de locura en su rostro: la boca sonriendo con una sonrisa endemoniada, los ojos hundidos abiertos ampliamente e incluso pareciendo que brillaban con una luz de forma no natural."


translate spanish zhenya_route_ff6a97ff:



    "Traté en vano de liberarme... las heridas y la fatiga no me lo permitían."


translate spanish zhenya_route_6496742f:



    "Ahí va la falta de sangre, todo se vuelve borroso frente a mis ojos. Los cierro, ¡pero la huella de esa demente mueca está todavía presente en mis párpados!"


translate spanish zhenya_route_65581f39:



    "Al borde de mi consciencia escucho el chirrido de la puerta al abrirse, la pionera endiablada deja de apretarme y luego el rostro de otra muchacha ya se inclina sobre mí."


translate spanish zhenya_route_b2ada642:



    slp "¡Vayámonos! ¡Ahora!"


translate spanish zhenya_route_ada3d630:



    "La voz familiar me susurra."


translate spanish zhenya_route_9ce82d61:



    "Alguien me ayuda a levantarme, me apoyo sobre esa persona y me alejo de la enfermería tropezando."


translate spanish zhenya_route_ce617998_18:



    "..."


translate spanish zhenya_route_871a5dfa:



    "Íbamos lentamente, a menudo tropezando y chocando levemente con alguna cosa."


translate spanish zhenya_route_f59b944c:



    "Me desmayaba y volvía en sí todo el rato, pero no podía alzar mi cabeza y solamente estaba mirando a mis pies: la piedra del pavimento frente a la enfermería dio paso al asfalto de la plaza, luego a la tierra cubierta de hojas caídas y a espinas de abeto."


translate spanish zhenya_route_5ee85201:



    "Al fin nos detuvimos y con dificultad me dejé caer en el suelo y miré alrededor."


translate spanish zhenya_route_404bac47:



    "Parecía el mismo claro al que me arrastró la pionera para..."


translate spanish zhenya_route_39d09361:



    pi "Hoy estoy caminando en malditos círculos..."


translate spanish zhenya_route_b92aee43:



    "Traté de sonreír burlonamente, pero tosí inmediatamente; el dorso de mi mano estaba cubierta de sangre."


translate spanish zhenya_route_b080400f:



    slp "Ten, bebe."


translate spanish zhenya_route_43a9d8cf:



    "La muchacha me pasó una botella fresca."


translate spanish zhenya_route_9e06cea6:



    "Apenas di un par de tragos y miré a mi salvadora."


translate spanish zhenya_route_8a6b8e6c:



    "Parece ser la misma muchacha conocida, pero algo estaba fuera de su lugar respecto con su imagen habitual."


translate spanish zhenya_route_8cfe0426:



    "A fin de cuentas, una sonrisa se dibujaba en esta muchacha pionera antes que la expresión de fría determinación que estaba fija en su rostro."


translate spanish zhenya_route_c196cab9:



    "Tampoco sabía muy bien qué decir..."


translate spanish zhenya_route_966ef311:



    "Ya me he equivocado una vez con aquella maníaca en la enfermería al relajarme por un instante, ¡ahora tengo que estar al acecho hasta el final esta vez!"


translate spanish zhenya_route_ca5f5721:



    "A pesar de que el insoportable dolor en todo mi cuerpo y la cabeza que me estaba dando vueltas, no me permitían concentrarme ni por un momento."


translate spanish zhenya_route_631179f6:



    pi "¿Qué es todo esto...?"


translate spanish zhenya_route_9e9814e8:



    slp "Shh, no hace falta hablar, ¡que te pondrás peor! Lo sé todo."


translate spanish zhenya_route_2e7e3777:



    "Dijo la muchacha y me miró atentamente como si, a diferencia de lo que dijera, ella estuviera esperando una respuesta."


translate spanish zhenya_route_70c9b40f:



    slp "Lo sé todo acerca de ti. En realidad yo también soy... real."


translate spanish zhenya_route_efee23f6:



    pi "¡¿Cómo diantres vas a ser real?! Ya nos conocimos ayer, ¡y no he notado nada de {i}real{/i} en ti!"


translate spanish zhenya_route_7ac3698b:



    "Tosí sangre otra vez."


translate spanish zhenya_route_2513e664:



    pi "¿Acaso ésta es otra de tus bromas?"


translate spanish zhenya_route_b033ae17:



    "Cotinué con tranquilidad."


translate spanish zhenya_route_e83d1d1c:



    pi "No puedes simplemente matarme, ¿cierto? ¡¿Me tienes que torturar, burlarte de mí?! ¡¿Es que quieres que me vuelva loco?!"


translate spanish zhenya_route_e3e89799:



    "Y entonces me di cuenta de que todo esto ya ha sucedido una vez...{w} aunque nuestros roles estaban intercambiados."


translate spanish zhenya_route_3e56f2df:



    slp "¡No, no, no es así en absoluto! ¡Estás equivocado! Tu no estuviste hablando conmigo ayer. Oh, para qué me explico, ¡es como si no te comprendieras a ti mismo!"


translate spanish zhenya_route_39ffacba:



    "La muchacha pionera frunció el ceño."


translate spanish zhenya_route_8b5427ea:



    slp "¿Acaso no encontraste tus propias copias?"


translate spanish zhenya_route_0ae95c2e:



    "Definitivamente había tal lógica en sus palabras que no podía hallar un defecto directamente."


translate spanish zhenya_route_8321ad79:



    pi "Bueno, tal vez...{w} ¿Y para qué necesitas hacer todo esto? Quiero decir, ¿por qué me ayudas?"


translate spanish zhenya_route_7d3ad2c5:



    slp "Porque tú eres igual que yo, ¡{i}real{/i}! ¿No es natural? ¿No harías lo mismo en mi lugar?"


translate spanish zhenya_route_ec9da8a5:



    "Si solamente supiera lo que he hecho antes..."


translate spanish zhenya_route_218c2c76:



    "¡¿Pero y si no está mintiendo?!"


translate spanish zhenya_route_b6ba807e:



    "¡A fin de cuentas antes conocí otra {i}gente{/i} a parte de mí en este mundo!"


translate spanish zhenya_route_e309eca4:



    pi "Gracias, si eso..."


translate spanish zhenya_route_a5f76af6:



    "La muchacha no contestó nada, sólo sonrió."


translate spanish zhenya_route_ad54904e:



    pi "Pero todavía no comprendo cómo lo hiciste...{w} Quiero decir que te he conocido a «ti» un millar de veces antes y todas ellas eran bots."


translate spanish zhenya_route_c3c67cfd:



    slp "¡Es difícil de explicar y no tenemos tiempo ahora!"


translate spanish zhenya_route_bf9c30b9:



    pi "¿Que no hay tiempo? ¿Tienes prisa para algo?"


translate spanish zhenya_route_7bd2bccb:



    slp "¡No sólo yo! ¡Pronto escaparemos todos de aquí!"


translate spanish zhenya_route_794059c2:



    pi "¿Quieres decir de este bucle a otro nuevo?"


translate spanish zhenya_route_48a94368:



    slp "¡No, no! ¡Quiero decir de este campamento para siempre!"


translate spanish zhenya_route_c6ae4acd:



    pi "¿Qué quieres decir...?"


translate spanish zhenya_route_ec482ad2:



    "Hice una sonrisa tonta."


translate spanish zhenya_route_3bf95c39:



    "La mera idea de que hubiera una salida de este lugar, ahora me había parecido absurda tras tanto tiempo."


translate spanish zhenya_route_35c07165:



    slp "¿No me digas que quieres quedarte aquí para siempre?"


translate spanish zhenya_route_4faa5838:



    pi "Bueno, ya he estado aquí siempre de alguna forma..."


translate spanish zhenya_route_c0cdbc2d:



    slp "Yo lo propongo... y tú lo rechazas, vale."


translate spanish zhenya_route_c4dddafe:



    pi "No, espera, me has malentendido..."


translate spanish zhenya_route_895a960c:



    "¿Aunque en realidad por qué «maletendido»?"


translate spanish zhenya_route_4d5b7b29:



    "¿Estoy realmente preparado para volver al mundo {i}real{/i}?"


translate spanish zhenya_route_b5a196bc:



    "Ahora me resulta ajeno y hostil, como si este campamento se hubiera vuelto mi hogar."


translate spanish zhenya_route_a6c7a702:



    "Pero ese hogar está en ruinas... ¡asaltado y quemado!"


translate spanish zhenya_route_429f73ec:



    "¿Seré capaz de construirlo de nuevo?"


translate spanish zhenya_route_0a336b4a:



    slp "¿Sí? ¿Pues qué es lo que quieres?"


translate spanish zhenya_route_108a6cc5:



    pi "De acuerdo, ¿pero ahora? No sugerirás que confíe en tu palabra, ¿verdad? Si de verdad comprendieras mi situación, no declinarías explicarte..."


translate spanish zhenya_route_438e567e:



    slp "Está bien, pero no ahora... ¡tenemos que asegurarnos que no nos buscan!"


translate spanish zhenya_route_a50296a4:



    pi "Espera..."


translate spanish zhenya_route_79498c62:



    "Pero la pionera no me estaba escuchando y, un segundo después, ella desapareció entre la vívida vegetación."


translate spanish zhenya_route_3d661175:



    "Lo que faltaba..."


translate spanish zhenya_route_2d6f3c2a:



    mz "¡Pero no sé coser!"


translate spanish zhenya_route_72ebe9e8:



    me "Qué lástima... ¡Qué gran descuido por parte de nuestra ama de casa!"


translate spanish zhenya_route_7b17d3b9:



    mz "¡Oh, mira quién fue hablar! ¿De qué vas a hacer los anillos? ¿Planeas acaso sencillamente enrollar un cable de la bobina de inducción alrededor de tu dedo?"


translate spanish zhenya_route_a4eed316:



    me "Tengo un par de ideas..."


translate spanish zhenya_route_c59c53ff:



    mz "Bueno, te escucho."


translate spanish zhenya_route_84f14225:



    "Zhenya me miró con cuidado y sonrió, anticipando mi fracaso."


translate spanish zhenya_route_205070b7:



    "Sin embargo, incluso antes de conocerla había adquirido algunas variadas y diferentes habilidades, construir para ambos un par de anillos no sería muy difícil para mí."


translate spanish zhenya_route_c07bca8e:



    "La única cuestión es el equipo... no puedes hallar instrumentos de orfebrería en un campamento."


translate spanish zhenya_route_69c44bc3:



    "Así que simplemente no tendría tiempo para prepararlo todo en una sola semana..."


translate spanish zhenya_route_1ca203ea:



    "¡Si no fuera por el club de cibernética!"


translate spanish zhenya_route_2551c817:



    "Si rebuscas entre la mayor parte del equipo almacenado allí, claramente podrías encontrar suficiente dorado para dos simples anillos."


translate spanish zhenya_route_0866bdde:



    "Y mantener ocupados a los cibernéticos lugareños con alguna cosa por un par de días no sería seguramente ningún problema, ¡especialmente considerando que no es mi primera vez!"


translate spanish zhenya_route_bb3c0a72:



    me "¡Qué sorpresa!"


translate spanish zhenya_route_6091a866:



    mz "Aun así..."


translate spanish zhenya_route_b4a932b1:



    "Zhenya se avergonzó."


translate spanish zhenya_route_b50a6e08:



    mz "En una semana desaparecerán. Sabes que no podemos llevarnos nada con nosotros."


translate spanish zhenya_route_8d6f2a2a:



    me "¿Y qué? ¿Qué es más importante para ti, dos piezas de metal o...?"


translate spanish zhenya_route_e5b74069:



    mz "No, claro que no..."


translate spanish zhenya_route_47c8c285:



    me "Pero si lo vamos hacer de la forma en que debería hacerse, entonces la ropa también..."


translate spanish zhenya_route_53695934:



    "Los ojos de Zhenya destellaron."


translate spanish zhenya_route_05ebce7a:



    "No había problemas con la ropa, lo sé con seguridad ya que varios tipos en el campamento tienen calzoncillos y una chaqueta, tal vez incluso de color gris oscuro."


translate spanish zhenya_route_6208daa1:



    "Un vestido de verano se vería estupendo en Zhenya, ¡pero el vestido de bodas era su idea al fin y al cabo!{w} El cual ella rechazó inmediatamente como era de esperar."


translate spanish zhenya_route_5d04b965:



    me "¡Usa un tul!"


translate spanish zhenya_route_6c3efbe4:



    mz "¿Lo dices en serio?"


translate spanish zhenya_route_658e4271:



    "Parece que ya está cansada de discutir."


translate spanish zhenya_route_b98d0d5d:



    me "No... ¡Pero siempre puedes pedírselo a una de las muchachas! Creo que ésa es la mejor opción."


translate spanish zhenya_route_c02501ad:



    "Zhenya se puso colorada, hizo un puchero y se dio la vuelta."


translate spanish zhenya_route_fdf97a8a:



    mz "¡Pues ve a pedir uno tú mismo! ¡Y toma las medidas oportunas tuyas ya que vestirás uno!"


translate spanish zhenya_route_d13b52c7:



    mz "Cómo te imaginas que podría hacer eso, simplemente me acerco caminando a, digamos, Slavya y le pido: «¿Podrías por favor coserme un vestido de boda para mí?»."


translate spanish zhenya_route_6b63784c:



    me "Y por qué no dices que es para...{w} ¡Para una competición de disfraces!"


translate spanish zhenya_route_209d81c1:



    me "No, eso es una tontería..."


translate spanish zhenya_route_4e958dc2:



    me "¡Para un baile de disfraces!"


translate spanish zhenya_route_6a0bdf61:



    "Zhenya estaba sorprendida pero no empezó a discutir inmediatamente."


translate spanish zhenya_route_148134b8:



    me "Le llevaré la idea a la líder del campamento, ¡no te preocupes! ¡Y no habrá motivo para ser vergonzosos! Una boda como parte de la actuación. Haremos que Shurik y Electronik hagan una actuación del primer paso de la humanidad en el espacio, Miku hará un cosplay de Lenin en un vehículo blindado..."


translate spanish zhenya_route_f1b9bc99:



    me "¡Y tú y yo nos casaremos!"


translate spanish zhenya_route_15cdfe60:



    mz "¿Y sobre qué acontecimiento quieres hacer esta reconstrucción histórica?"


translate spanish zhenya_route_33626f7e:



    me "Dios mío, ¿qué importa? ¡Podrías ser incluso el matrimonio de Catalina II y Pedro III!"


translate spanish zhenya_route_341cca1a:



    mz "¡Oooh, pues qué destino poco envidiable te espera, querido!"


translate spanish zhenya_route_8796d51d:



    "Zhenya sonrió burlonamente."


translate spanish zhenya_route_aea00f45:



    me "No importa, ¡sólo es un ejemplo! Ves, ya estás bromeando."


translate spanish zhenya_route_323de6e5:



    mz "Aun así, eso es algo..."


translate spanish zhenya_route_c8e1c467:



    me "¡Confía en mí!"


translate spanish zhenya_route_91039827:



    mz "Vale, si crees que todo funcionará..."


translate spanish zhenya_route_ce617998_19:



    "..."


translate spanish zhenya_route_333e14d0:



    "Empezó a atardecer, la fiebre hizo acto de presencia y parecía incluso hacerse más intensa."


translate spanish zhenya_route_21698494:



    "Aparentemente, la muchacha pionera «real» se perdió en alguna parte del bosque... parece que se han deshecho de mí otra vez..."


translate spanish zhenya_route_4bde977b:



    "Aunque no fui de mucha ayuda en esta ocasión, tan sólo quería que todo volviera a ser como era, no volver a la {i}realidad{/i}."


translate spanish zhenya_route_d802c7b0:



    "Quizá por otra parte solamente pasó una hora, tal vez siglos, ya no importa."


translate spanish zhenya_route_f694e34b:



    "Me siento como un hombre comatoso que se acaba de despertar y no puede reconocer el mundo a su alrededor."


translate spanish zhenya_route_18fc7c9f:



    "El horizonte estaba ardiendo con un resplandor brillante escarlata, bañando el cielo con una aurora en llamas."


translate spanish zhenya_route_f25fa47d:



    "¿Y si no fueran solamente los lugareños los que habían cambiado en este mundo?"


translate spanish zhenya_route_cc66a0fd:



    "En pocos minutos los rayos del resplandeciente sol llegarán hasta aquí y me quemarán, ¡junto a este maldito Sovyonok!"


translate spanish zhenya_route_f7aa9307:



    "¡Desearía que la muchacha pionera «real» volviera!"


translate spanish zhenya_route_a51237ea:



    "¡Es mejor decidirse por un mundo {i}nuevo{/i} que arder vivo en uno conocido!"


translate spanish zhenya_route_0cac00e1:



    "¡Además de porque nadie allí sabrá algo de lo que me sucedió aquí!"


translate spanish zhenya_route_d3d6bce6:



    "Sobre todo lo que hice..."


translate spanish zhenya_route_95cdb0f9:



    pi2 "Pareces ensimismado con pensamientos profundos sobre algo."


translate spanish zhenya_route_64f4c9df:



    "Una voz familiar sonó cerca.{w} Mi voz..."


translate spanish zhenya_route_9a21a38f:



    pi "¿Qué quieres?"


translate spanish zhenya_route_c8492839:



    "Estaba sorprendido por su aparición e incluso un poco asustado... tuve tantos problemas con los pioneros encolerizados que incluso había dejado de pensar acerca de mis dobles."


translate spanish zhenya_route_dc0d54d6:



    "Aunque, ¿«mis dobles»...?{w} ¿Cuánto tiempo hace desde la última vez que los llamé así?"


translate spanish zhenya_route_1fbbac28:



    "Correcto, por mí siempre estuve {i}yo{/i} y estaban {i}ellos{/i}... no tenemos nada en común, somos personas distintas, incluso si fuimos uno en algún determinado punto..."


translate spanish zhenya_route_9f450679:



    pi2 "¿Quizás estés pensando «cuándo se estropeó todo»?"


translate spanish zhenya_route_449bd8e5:



    "En eso él estaba equivocado... No estaba pensando en eso."


translate spanish zhenya_route_43ab8e00:



    pi2 "¿Atormentándote con dudas, preguntándote a ti mismo cuestiones a las que no hallas respuestas?"


translate spanish zhenya_route_f91eeb18:



    pi "¡Solamente quiero acabar con todo esto! ¡Y tu presencia no ayuda en nada a ello!"


translate spanish zhenya_route_33691edf:



    pi2 "Todo terminó hace mucho tiempo, si eso es a lo que te refieres..."


translate spanish zhenya_route_4a413495:



    pi "¿En serio? No es precisamente tan evidente."


translate spanish zhenya_route_2212509c:



    pi2 "¡Para todos salvo para ti! Aquel día en el autobús tomaste la decisión incorrecta y moriste."


translate spanish zhenya_route_22952e7f:



    "Miré por encima de mi cuerpo con cuidado.{w} Me dolía, ¡lo cual significa que estoy vivo!"


translate spanish zhenya_route_2d38d332:



    pi "¡No tengo aspecto de ser un hombre muerto!"


translate spanish zhenya_route_8258081a:



    pi2 "¿Crees que es tan sencillo, como en las películas? ¿Quieres recordar el instante en que la vida te abandonó, recordar tu último aliento? ¿El momento de la transición? ¿Los primeros instantes en el nuevo nivel de existencia?"


translate spanish zhenya_route_c9d54b47:



    pi "No te entiendo de verdad. ¡Pues eso significa que ya he muerto entonces, en {i}realidad{/i}!"


translate spanish zhenya_route_92313099:



    pi2 "¡No! Se te ha dado la oportunidad pero no la usaste y acabaste en tu propio y particular infierno, ¡un limbo si así lo deseas!"


translate spanish zhenya_route_6aee1bdf:



    pi "Oh, mírate inventando la rueda, ¡como si no lo supiera!"


translate spanish zhenya_route_e8ac9853:



    pi2 "Lo sabías, ¡pero no sabías que aquí estás solo ahora y que no hay salida!"


translate spanish zhenya_route_5eb47f22:



    pi "Eso lo sabía..."


translate spanish zhenya_route_7c4f3efc:



    "O por lo menos lo suponía."


translate spanish zhenya_route_4e47c222:



    pi "¿Y bien? ¿Qué hay de ti pues?"


translate spanish zhenya_route_4822c2b3:



    pi2 "¡No estoy aquí! ¡Soy solamente tu alucinación!"


translate spanish zhenya_route_24b2d9d5:



    "El bosque resonó con su desagradable carcajada."


translate spanish zhenya_route_0870ec03:



    "Todo es dolorosamente familiar, pero ¿por qué se repite ahora?"


translate spanish zhenya_route_ed1d079e:



    "¿Es verdad lo que dicen que cuando hay problemas nunca vienen solos?"


translate spanish zhenya_route_9ec16ea9:



    pi "Pues que sean bienvenidos..."


translate spanish zhenya_route_724dc14a:



    "Cerré mis ojos e incluso me di la vuelta para asegurarme."


translate spanish zhenya_route_997dc7d7:



    pi3 "No lo trates de esa manera, ¡él es uno de nosotros también! O por lo menos lo fue una vez..."


translate spanish zhenya_route_bebf711e:



    "La voz familiar sonó un poco diferente."


translate spanish zhenya_route_5b2a770e:



    "Ahora dos pioneros estaban en el claro frente a mí."


translate spanish zhenya_route_8f95cdc6:



    pi2 "¡Tienes que combatir el fuego con fuego!"


translate spanish zhenya_route_a6e86815:



    pi3 "¡Él ya ha tenido bastante guerra!"


translate spanish zhenya_route_c8ef17e5:



    pi2 "¡No diría que aun sea suficiente!"


translate spanish zhenya_route_0c2864de:



    "Estaban discutiendo mientras que la realidad dejó de tener sentido para mí."


translate spanish zhenya_route_2f5de2b0:



    pi3 "Míralo, ¡ya perdió la cabeza para siempre!"


translate spanish zhenya_route_044d2365:



    pi2 "¡Eso no tiene nada de especial, ya se le pasará! Como en aquella historia que le contamos a Ulyana, original de un amigo inexistente: «Los locos recuperaron la cordura, y los cuerdos se volvieron locos»."


translate spanish zhenya_route_67371292:



    "Y luego aquella carcajada endiablada otra vez."


translate spanish zhenya_route_f0b0ba2b:



    pi2 "¿Crees que ésta es su primera vez?"


translate spanish zhenya_route_102a2a69:



    pi3 "¿No lo es?"


translate spanish zhenya_route_e27da733:



    pi2 "¡Lo sé con seguridad que no lo es! Él piensa que recuerda todo, que está preparado para todo, ¡conoce el comportamiento de cada pionero de antemano! ¡Pero se olvidó de mucho! Quizá se olvidó de la cosa más importante..."


translate spanish zhenya_route_c19cedfa:



    uv2 "¡Basta ya!"


translate spanish zhenya_route_c600ee72:



    "Repentinamente ambos se hicieron a un lado y la orejas de gata se adelantó..."


translate spanish zhenya_route_cf99867f:



    "Siempre fui algo receloso con ella porque pensé que no me podía esperar nada bueno de ella."


translate spanish zhenya_route_ea619637:



    "Además, ella me causaba algo en lo profundo de mi interior, algún tipo de miedo primario y animal, como el miedo a la oscuridad."


translate spanish zhenya_route_1bc7b239:



    "¡Y tampoco me creí nunca sus historias!{w} Como aquella en que ella es nuestra subconsciencia, ¡su parte oculta!"


translate spanish zhenya_route_628e7777:



    "¡Qué absurdo!"


translate spanish zhenya_route_c0c70154:



    uv2 "¿Es que no veis lo mal que está? ¡No sois útiles! Como tú..."


translate spanish zhenya_route_9229cca9:



    "Ella indicó con su dedo al pionero «malvado»."


translate spanish zhenya_route_cd25bf7c:



    uv2 "¡Daos prisa hacia la enfermería a por pastillas!"


translate spanish zhenya_route_9f2460f9:



    pi2 "¿Qué pastillas...?"


translate spanish zhenya_route_f3f86c57:



    "Él estaba un poco sorprendido."


translate spanish zhenya_route_654f75d1:



    uv2 "¡No lo sé! ¿No eres el más inteligente de los presentes? ¡Apáñatelas!"


translate spanish zhenya_route_136154fe:



    "El pionero suspiró inútilmente y se arrastró en dirección hacia el campamento."


translate spanish zhenya_route_e04e4c0e:



    uv2 "Y tú..."


translate spanish zhenya_route_8f4741a0:



    "Ella miró al «bueno»."


translate spanish zhenya_route_9d637256:



    uv2 "Y tú...{w} bueno... {w} ¡encuentra algo que hacer! Sinceramente, tus quejas me ponen enferma, ¡solo empeoran las cosas!"


translate spanish zhenya_route_1187e1a0:



    pi3 "Pero solamente quería ayudar..."


translate spanish zhenya_route_ea0da183:



    uv2 "¡Pero no estás ayudando! ¡Así que vete!"


translate spanish zhenya_route_151f70d8:



    "Ella se despidió con la mano e hizo una expresión que tuve una idea por mi cabeza: ¡como la gata cascarrabias!"


translate spanish zhenya_route_85e516ba:



    "El pionero no discutió y siguió a su doble."


translate spanish zhenya_route_5cd1f1b6:



    pi "¿Y ahora qué?"


translate spanish zhenya_route_c0cfa3ab:



    "Pregunté sin mucho entusiasmo."


translate spanish zhenya_route_6dd0b4ac:



    "He sido engañado más de una vez, así que no voy a creerla ahora.{w} ¡Especialmente a ella!"


translate spanish zhenya_route_698655c7:



    "Nunca habría pensado que de todos los lugareños, sería la orejas de gata la que viniera a ayudarme."


translate spanish zhenya_route_01ee43cb:



    "Aunque fuera sólo con palabras. Después de todo, ella se acuerda de las anteriores veces, recuerda nuestra discusión en el autobús."


translate spanish zhenya_route_4eddc54b:



    pi "¿Crees que serás capaz de tomarme el pelo otra vez?"


translate spanish zhenya_route_eb468b81:



    "Reuniendo los últimos restos de mis fuerzas, me alcé, mis ojos se nublaban de dolor."


translate spanish zhenya_route_968edac1:



    "¿Pero adónde iré?"


translate spanish zhenya_route_5c0580b2:



    "Debí haberme inyectado la jeringa yo mismo..."


translate spanish zhenya_route_7bea4bf4:



    "¿Por qué pensé que un suicidio {i}aquí{/i} es el fin?"


translate spanish zhenya_route_6e9bb08c:



    uv2 "¡Ey!"


translate spanish zhenya_route_f5f49f8f:



    "La orejas de gata saltó a mi lado de un brinco y me sostuvo, así no me caería."


translate spanish zhenya_route_4991f6ed:



    pi "Tú...{w} ¡¿De verdad quieres hacerme creer que me estás ayudando?!"


translate spanish zhenya_route_2c0c4fc1:



    uv2 "¿Y qué te piensas que estoy haciendo ahora? ¡A fin de cuentas siempre os ayudo {i}a todos{/i}!"


translate spanish zhenya_route_74481647:



    pi "Sí, claro..."


translate spanish zhenya_route_b7427c21:



    "Me derrumbé en el suelo otra vez y dejé ir un profundo suspiro."


translate spanish zhenya_route_de2d25b8:



    "En realidad no hay ningún lugar al que ir en estas condiciones."


translate spanish zhenya_route_19691844:



    pi "Y bien, ¿cuándo retornarán los pioneros...?"


translate spanish zhenya_route_f0748a5e:



    uv2 "Pronto..."


translate spanish zhenya_route_0ba6f1f4:



    "Contestó vagamente."


translate spanish zhenya_route_0a87d3a3:



    pi "Vale, sentémonos, esperemos, no tengo nada más que perder..."


translate spanish zhenya_route_a20cefa7_6:



    "..."


translate spanish zhenya_route_e9c63299:



    "Todo el campamento se reunió en la plaza."


translate spanish zhenya_route_034dc053:



    "Ulyana estaba persiguiendo a Electronik en un intento vano por arrebatarle su abollado casco de cosmonauta de cartón."


translate spanish zhenya_route_4f376572:



    "La escena del primer paso del hombre en el espacio no fue sin algunos incidentes divertidos: el monumento de Genda sirvió de cohete improvisado para los cibernéticos y Shurik se las arregló para tropezar y caer en el momento más crucial."


translate spanish zhenya_route_48adf945:



    "Es bueno que la distancia hasta el suelo fuera significativamente más corta que la órbita más próxima a la tierra, y se encontró como en la canción... con la «hierba del hogar»."


translate spanish zhenya_route_457c4564:



    mt "¡Deja de correr! ¡Que te vas a matar!"


translate spanish zhenya_route_4bd9c425:



    "Se lamentaba Olga Dmitrievna."


translate spanish zhenya_route_b6eca189:



    "Tampoco sé con certeza qué le preocupaba más en dicha situación... si los cibernéticos o la imagen del campamento que no tuviera ningún accidents los 365 días del año desde el comienzo de los tiempos."


translate spanish zhenya_route_0b44a339:



    "Zhenya estaba cerca y estiraba de mi manga, pero estaba demasiado distraído con el espectáculo y no me di cuenta directamente."


translate spanish zhenya_route_ce73fe66:



    mz "¡Bueno!"


translate spanish zhenya_route_ad02f53a:



    me "¿Qué?"


translate spanish zhenya_route_173b40bd:



    mz "¿Estás seguro que deberíamos hacer esto?"


translate spanish zhenya_route_c62cea6e:



    me "¡Pero tú misma querías esto!"


translate spanish zhenya_route_5d729a3c:



    mz "¡No quería nada!{w} Tú solamente lo sugeriste y yo estuve de acuerdo..."


translate spanish zhenya_route_bbd48daa:



    me "Ya hemos discutido esto cientos de veces. Y sabes perfectamente que eso no es de verdad."


translate spanish zhenya_route_8dad8ac4:



    mz "Oh, así que no es de verdad, ¿eh?"


translate spanish zhenya_route_cb64fecf:



    "Dijo ella con pavor."


translate spanish zhenya_route_2cf45de0:



    me "Eso no es lo que quiero decir... Estamos simplemente en {i}este{/i} mundo, no lo olvides. En un par de días todo se repetirá y nadie recordará esto."


translate spanish zhenya_route_26238f0b:



    mz "Cierto, ¡pero yo sí!"


translate spanish zhenya_route_070599c2:



    me "¿Entonces por quién estás más incómoda? ¿Por aquellos de tu alrededor, o por ti misma?"


translate spanish zhenya_route_e9fca54d:



    "Zhenya se ruborizó y no dijo nada."


translate spanish zhenya_route_aed58c1a:



    mt "Bien, ¡idos a cambiar ya!"


translate spanish zhenya_route_40fea5f9:



    "Nos ordenó Olga Dmitrievna."


translate spanish zhenya_route_a752ccba:



    me "¿Preparada?"


translate spanish zhenya_route_38c18bf5:



    "Zhenya asintió."


translate spanish zhenya_route_ce617998_20:



    "..."


translate spanish zhenya_route_53d34154:



    "Diez minutos más tarde una vista extraña para un pionero habitual del campamento se podía ver en la plaza."


translate spanish zhenya_route_a219887e:



    "Anda ya, pero que estoy diciendo, ¡era extraña incluso para Sovyonok!"


translate spanish zhenya_route_907927ec:



    "Y me sentía raro también en medio de una muchedumbre de pioneros en un antiguo disfraz soviético de otra persona de antaño."


translate spanish zhenya_route_3903c48c:



    "No un traje de frac formal desde luego, pero aun así mejor que los pantalones cortos y una camisa de manga corta."


translate spanish zhenya_route_941c499b:



    "Mientras que Zhenya estaba con una vestimenta de un fugaz parecido razonable a un vestido de bodas."


translate spanish zhenya_route_159608c4:



    "En cualquier otra situación diría que se veía ridícula, pero no ahora."


translate spanish zhenya_route_9d9d5e6c:



    "Ahora este vestido me parecía más bello que cualquier atuendo de una pomposa tienda de moda."


translate spanish zhenya_route_f29ba8f0:



    mt "¿Y qué escena estáis interpretando ahora? ¿Acaso me olvidé?"


translate spanish zhenya_route_567dd942:



    "Me susurró al oído la líder del campamento."


translate spanish zhenya_route_4fdd0140:



    me "Olga Dmitrievna, ¿qué importa ya? ¡Empecemos!"


translate spanish zhenya_route_025b7514:



    mt "De acuerdo..."


translate spanish zhenya_route_de8c4bfc:



    "Zhenya estaba roja como un tomate, sin saber dónde mirar con sus ojos."


translate spanish zhenya_route_a876aab3:



    "Ahora parecía querer intensamente mirarme."


translate spanish zhenya_route_0e1a4307:



    me "Todo irá bien."


translate spanish zhenya_route_5788b9db:



    "Dije con voz baja."


translate spanish zhenya_route_ff4fa4d8:



    "Miku estaba sonriendo alegremente entre la muchedumbre de pioneros, como si fuera su propia boda... un poco boba tal vez, pero honestamente."


translate spanish zhenya_route_3c5ef511:



    "Lena se ruborizaba como de costumbre, pero a veces haciéndonos miradas prudentes."


translate spanish zhenya_route_d006862e:



    "En toda la mirada de Alisa se intuía que ella había sido arrastrada aquí contra su propia voluntad, que de haber podido quedarse tocando la guitarra o simplemente durmiendo lo habría hecho."


translate spanish zhenya_route_7b0dcbe4:



    "A pesar de todo Ulyana estaba claramente intrigada con todo el espectáculo: los niños no necesitan mucho para ser felices."


translate spanish zhenya_route_002294ae:



    "En cuanto a Slavya, parecía estar muy feliz por nosotros y sonreía amablemente."


translate spanish zhenya_route_fc45d68b:



    mz "Pongamos fin a esto cuanto antes."


translate spanish zhenya_route_337d23ad:



    "Parece que por poco más, Zhenya se moriría de vergüenza."


translate spanish zhenya_route_01822020:



    me "Vale."


translate spanish zhenya_route_280f303c:



    me "Estamos preparados."


translate spanish zhenya_route_ab16cb35:



    "Asentí a la líder del campamento."


translate spanish zhenya_route_27d5273b:



    "Ahora tenía que decir lo que habitualmente se dice en las bodas."


translate spanish zhenya_route_daa4ab51:



    "Por supuesto, no me sabía las palabras exactas y me las apañé para olvidarme completamente de todo lo que había preparado de antemano."


translate spanish zhenya_route_4208fbed:



    me "Prometo..."


translate spanish zhenya_route_9eb7ae3d:



    "Comencé con la voz temblorosa."


translate spanish zhenya_route_145c5153:



    me "En la enfermadad y en la vejez..."


translate spanish zhenya_route_100598bd:



    "No, así no es como era."


translate spanish zhenya_route_32f2392a:



    us "¡Qué murmuras! ¡No te escuchamos nada!"


translate spanish zhenya_route_1770bf88:



    "Vociferó alegremente Ulyana."


translate spanish zhenya_route_2676b165:



    dv "Tranquilízate, o te pondremos en el lugar de Zhenya."


translate spanish zhenya_route_8155be36:



    "La amenaza tuvo un efecto inmediato y Ulyana se calló."


translate spanish zhenya_route_90d25991:



    me "Bueno, eso es..."


translate spanish zhenya_route_3b342837:



    "Hace sólo un minuto podría haber jurado que estaba absolutamente calmado, ahora no podía ni pronunciar una palabra."


translate spanish zhenya_route_cca9bafd:



    "Sólo podía suponer lo que Zhenya estaba sintiendo."


translate spanish zhenya_route_1e527a61:



    "Reuní mi coraje."


translate spanish zhenya_route_1d086fbd:



    me "Prometo estar junto a ti en lo bueno y en lo malo, para la riqueza y la pobreza, en la enfermedad y en la salud, ¡hasta que la muerte nos separe!"


translate spanish zhenya_route_7aaa9384:



    "Los pioneros que aclamaban con alegría hasta ahora, pararon de hablar inmediatamente."


translate spanish zhenya_route_b4112c36:



    "Se estaba con tanto silencio que podía escuchar mi corazón latiendo furiosamente."


translate spanish zhenya_route_6d361550:



    "Zhenya estaba quieta, sus ojos se fijaron en el suelo."


translate spanish zhenya_route_5d3c27fa:



    me "¡Bueno, ahora tú!"


translate spanish zhenya_route_7b26ab8b:



    "Respiró profundamente llenando sus pulmones y dijo a carrerilla:"


translate spanish zhenya_route_7f141d67:



    mz "Estoy de acuerdo en casarme contigo, prometo estar en la enfermedad y en la pobreza. ¡Ahí tienes!"


translate spanish zhenya_route_5c93db98:



    "La muchedumbre explotó a carcajadas."


translate spanish zhenya_route_f65f12c2:



    "Aunque para mí las palabras de Zhenya resultaron bastante serias."


translate spanish zhenya_route_fde7233b:



    mz "Venga terminad ya."


translate spanish zhenya_route_e0f4c4c3:



    me "¿Y qué hay del «y ahora puedes besar a la novia»?"


translate spanish zhenya_route_1a014c13:



    "Pregunté con un tono juguetón."


translate spanish zhenya_route_8492c157:



    "Los ojos de Zhenya destellearon, dejando claro que esta situación era suficientemente buena incluso sin besos."


translate spanish zhenya_route_c8b5fcd7:



    "Supongo que ella tiene razón, ya tendremos tiempo para eso de todas formas."


translate spanish zhenya_route_a718aa85:



    mt "¡Buen trabajo! ¡Gracias a todos!"


translate spanish zhenya_route_f52b105b:



    "Olga Dmitrievna estaba chillando. Electronik se adelantó con una cámara."


translate spanish zhenya_route_4f2d1d7d:



    "Se le veía que estaría mucho más contento sosteniendo con sus manos un pasamanos en su lugar."


translate spanish zhenya_route_6ddf4f60:



    el "Un par de tomas para la posteridad..."


translate spanish zhenya_route_e70a6fe5:



    mz "¡No diantres!"


translate spanish zhenya_route_a312257a:



    "Le desgañitó Zhenya."


translate spanish zhenya_route_1b473258:



    me "Venga ya, para el álbum de familia."


translate spanish zhenya_route_b402f943:



    "Ella dudó un poco, pero hizo un parecido de sonrisa después de todo."


translate spanish zhenya_route_cd93bb8a:



    me "¡Todo fue como debía ir! ¡Mejor de lo esperado!"


translate spanish zhenya_route_cd8ad1b8:



    "Por primera vez en todo este tiempo Zhenya me miró directamente a los ojos, sonrió y dijo:"


translate spanish zhenya_route_bcd11e4b:



    mz "Claro, supongo."


translate spanish zhenya_route_ce617998_21:



    "..."


translate spanish zhenya_route_0852e54f:



    "Me desperté cuando el día estaba casi llegando a su fin."


translate spanish zhenya_route_0bb1d15b:



    "La fiebre se había calmado y hallé una marca de inyección en mi mano derecha."


translate spanish zhenya_route_532743cd:



    "¿Realmente la orejas de gata me ayudó...?"


translate spanish zhenya_route_5ca624bb:



    "Al principio no podía ni creerme que hubiera alguien aquí capaz de un buen acto tras todo lo que ocurrió en los últimos días."


translate spanish zhenya_route_762d0ac9:



    "Con cierta dificultad me levanté y me dirigí hacia el campamento."


translate spanish zhenya_route_7dac7415:



    "Después de todo, no tengo nada que hacer claramente en el bosque y no parece que pueda ir a peor."


translate spanish zhenya_route_67da5e6b:



    "Una vez y otra un pensamiento se me pasaba por la cabeza... tengo que aguantar, tengo que vivir durante esta semana y luego será más fácil. Me recuperaré físicamente y habrá algún tiempo incluso para recuperarme mentalmente."


translate spanish zhenya_route_d272d59d:



    "Tan sólo no desesperes, no abandones, ¡no seas como {i}él{/i}!"


translate spanish zhenya_route_85b36826:



    "¡Antes moriría que vivir así!"


translate spanish zhenya_route_a20cefa7_7:



    "..."


translate spanish zhenya_route_3a421f90:



    "Los pioneros con mochilas, bolsas y paquetes estaban correteando allí y allá por la plaza."


translate spanish zhenya_route_cb975bdd:



    "La líder del campamento estaba cerca de Genda como un pequeño monumento, con los brazos en jarras y vociferando formidablemente."


translate spanish zhenya_route_7b2c05b0:



    "Me quedé quieto de piedra, sin saber qué esperarme."


translate spanish zhenya_route_f64d961c:



    "{i}Este{/i} mundo está en continuo movimiento, cambia justo frente a mis ojos: hace tan sólo ayer estaban a punto de matarme, ¿y hoy qué?"


translate spanish zhenya_route_f711d58a:



    mtp "¡Ey, Semyon!"


translate spanish zhenya_route_303ef93d:



    "La líder del campamento me gritó y me miró de forma intimidatoria."


translate spanish zhenya_route_058c857c:



    pi "¿Quién, yo? ¿Me lo dices a mí?"


translate spanish zhenya_route_98ac5311:



    mtp "¿Por qué no has empacado aun?"


translate spanish zhenya_route_f8ea68a4:



    pi "¿Empacado? No estoy preparado para empacar..."


translate spanish zhenya_route_5268cef9:



    mtp "¡Hoy es el último día de la temporada! ¡¿Ya te olvidaste otra vez?! ¡Vete a empacar!"


translate spanish zhenya_route_4267b07b:



    "No quería discutir con ella... sencillamente no tenía fuerzas... y dejé de estar sorprendido hace ya mucho tiempo, así que me salí por la tangente:"


translate spanish zhenya_route_6c4fab7d:



    pi "¿Está bien que me vea así?"


translate spanish zhenya_route_e9b3d6f4:



    "Y señalé mis ropas rasgadas y las magulladuras de mi cuerpo."


translate spanish zhenya_route_4eb3fa9a:



    mtp "¿Qué te sucedió?"


translate spanish zhenya_route_ee50b2d9:



    "Se extrañó sinceramente la líder del campamento."


translate spanish zhenya_route_3c6b857f:



    mtp "¿Fuiste corriendo por el bosque y te caíste?"


translate spanish zhenya_route_077afa71:



    pi "¿De qué demonios estás hablando? ¿Qué bosque? ¿Y por qué iría a necesitar correr a través del bosque?"


translate spanish zhenya_route_64416fdd:



    mtp "De acuerdo, no es asunto mío... Partimos en media hora, ¡si llegas tarde te quedarás para otra temporada!"


translate spanish zhenya_route_fde537c1:



    "Sonrió burlonamente."


translate spanish zhenya_route_a5626efa:



    "«Te quedarás para otra temporada»..."


translate spanish zhenya_route_7cbd0db3:



    "¡No, eso está fuera de mis planes!"


translate spanish zhenya_route_8cc96c2a:



    "De todas maneras, un nueva forma de escapar de aquí apareció, ¡aun siendo en semejantes estúpidas circunstancias!"


translate spanish zhenya_route_af3b5ab5:



    "Lentamente me arrastré hacia la parada de autobús."


translate spanish zhenya_route_ad26bb5a:



    "Todo el campamento se amontonaba frente al autobús."


translate spanish zhenya_route_a27f23dc:



    "Todo el mundo está esperando a la líder del campamento... a fin de cuentas, seguir las normas contradice sus propias normas."


translate spanish zhenya_route_37559c26:



    unp "Oh, ¿qué te sucedió...?"


translate spanish zhenya_route_9673889c:



    "La pionera de la enfermería inquirió maternalmente."


translate spanish zhenya_route_0b9afc20:



    slp "¿Te encuentras bien? ¿Necesitas una venda?"


translate spanish zhenya_route_dd4b010c:



    "¡Porqué dejarlo pasar, ofréceme un entablillado!"


translate spanish zhenya_route_3f25e7ab:



    dvp "Debe haberse caído en una zanja y lloró desconsoladamente."


translate spanish zhenya_route_662ce80f:



    "Seguro, mira quién fue a hablar..."


translate spanish zhenya_route_2872693b:



    usp "¡Y si los lobos lo hubieran atrapado!"


translate spanish zhenya_route_12528bfb:



    "Únicamente si esos lobos fueran pioneros locales..."


translate spanish zhenya_route_222786fb:



    "Estaba sentado tranquilamente en la cuneta, agaché mi cabeza entre mis manos."


translate spanish zhenya_route_aef7bae3:



    "Tampoco quería pensar sobre lo que sucedería mañana, tanto si acababa en un nuevo bucle como si terminaba de una vez por todas."


translate spanish zhenya_route_cfa4c35a:



    "¿O quizás hay alguna cosa aguardándome más adelante, algo...{w} mejor...?"


translate spanish zhenya_route_ce617998_22:



    "..."


translate spanish zhenya_route_cae10f7f:



    "Cuando me desperté Zhenya no estaba cerca."


translate spanish zhenya_route_65b807df:



    "Debe haber ido a ocuparse de algún asunto personal."


translate spanish zhenya_route_f9aba6a3:



    "Por Dios, ¡¿por qué necesita todo este juego de pioneros?!"


translate spanish zhenya_route_90bc68a6:



    "¡Especialmente en la noche de bodas!"


translate spanish zhenya_route_9a8da883:



    "Me estiré y bostecé con gusto."


translate spanish zhenya_route_9d92e2de:



    "Oh bueno, por lo menos puedo sorprenderla... cogeré algunas flores en el bosque."


translate spanish zhenya_route_9b419756:



    "No tanto como para un regalo, pero sé que le gusta el Lirio de los Valles."


translate spanish zhenya_route_523c3f6b:



    "Nunca pude entender por qué le gustan las flores a las mujeres..."


translate spanish zhenya_route_d823504c:



    "¿Por qué entoces no las ortigas o las brancas ursinas?"


translate spanish zhenya_route_04a18347:



    "Eso son plantas también y son bonitas en sus respectivos hábitats, no en macetas o en floreros."


translate spanish zhenya_route_206496b3:



    "Aunque, ¿qué importa si cada vez que le regalé flores a Zhenya su rostro se iluminó con una radiante sonrisa?"


translate spanish zhenya_route_1919fcf8:



    "Por supuesto, puede que no haya sido debido a las flores solamente..."


translate spanish zhenya_route_f7f98ab4:



    "Con un ramo en mis manos me deslicé dentro de la biblioteca, dejando abierta la puerta con ruido."


translate spanish zhenya_route_d823e1d6:



    "Detrás de la mesa de la biblioteca estaba sentada Alisa, con aspecto melancólico y hojeando alguna revista."


translate spanish zhenya_route_4bc48b58:



    dv "Oh, el recién casado se ha dejado ver."


translate spanish zhenya_route_38e73ed3:



    "Murmuró, mirándome hoscamente."


translate spanish zhenya_route_b3705464:



    me "Ey, ¿dónde está Zhenya...?"


translate spanish zhenya_route_b20b9d5a:



    dv "No sé. Me pidió que me quedara sentada aquí en su lugar y se fue a alguna parte."


translate spanish zhenya_route_9edc69e2:



    "Bueno, sí, mi Zhenya puede incluso convencer a Alisa."


translate spanish zhenya_route_21e4d285:



    me "De acuerdo, pues yo..."


translate spanish zhenya_route_59986aee:



    dv "Y por alguna razón no tenía en absoluto cara de estar alegre."


translate spanish zhenya_route_23858456:



    "Dijo Alisa a mis espaldas."


translate spanish zhenya_route_6b3f4d60:



    "¿Dónde se fue Zhenya?"


translate spanish zhenya_route_48ccafa2:



    "¿Quizá también quiso hacerme un regalo sorpresa?"


translate spanish zhenya_route_c1260382:



    "No, las sorpresas no son lo suyo, debe ser solamente la hora del desayuno."


translate spanish zhenya_route_aa30435e:



    "Fui, no, corrí hasta la cantina."


translate spanish zhenya_route_b11e70d8:



    "Los pioneros estaban haciendo una alegre algarabía, los platos repiqueaban de la misma alegre manera, incluso la habitual señora triste de la cocina hoy estaba inusualmente alegre."


translate spanish zhenya_route_0b93c284:



    "Me percaté de Slavya y caminé hasta a ella."


translate spanish zhenya_route_94ff2553:



    me "Hola, ¿has visto a Zhenya?"


translate spanish zhenya_route_a850b845:



    "Slavya me miró con sorpresa, luego se avergonzó y apartó los ojos."


translate spanish zhenya_route_ddb1c9c0:



    sl "Creo que se fue a la parada de autobús."


translate spanish zhenya_route_89d4ed9c:



    me "¿La parada de autobús? ¿Por qué?"


translate spanish zhenya_route_16a22502:



    sl "No lo sé...{w} Mejor se lo preguntas a ella. Todavía puedes llegar a tiempo si te das prisa."


translate spanish zhenya_route_2ef037d1:



    "¿A la parada de autobús? ¡A la parada de autobús!"


translate spanish zhenya_route_27f648fd:



    "En un minuto corrí cruzando las puertas del campamento..."


translate spanish zhenya_route_1baec8fb:



    "Y vi a Zhenya de pie en la carretera, su mirada dirigida hacia alguna parte lejos en el horizonte, más allá de las llanuras y los bosques, cerca del radiante sol veraniego."


translate spanish zhenya_route_f15d9551:



    "Una expresión extraña estaba en su rostro, tristeza, premonición y angustia mezcladas todas juntas, pero a la vez Zhenya se veía tan diferente...{w} No la reconocía."


translate spanish zhenya_route_581fe4cb:



    "¿Mejor? No, puedo creerme incluso que eso sea menos que el hecho de que mañana retornaré al mundo {i}real{/i}."


translate spanish zhenya_route_d91dba14:



    "Tal vez, ¿era mejor antes?"


translate spanish zhenya_route_924ac1c6:



    "Pareciera que esa pionera me estuviera diciendo que olvidé muchas cosas..."


translate spanish zhenya_route_95e3c63e:



    "¿Pero qué exactamente?"


translate spanish zhenya_route_867fc00d:



    me "Ey, te estaba buscando."


translate spanish zhenya_route_8e4d71a5:



    "Eso sonó ridículo y mi voz estaba temblando... tan sólo tenía que decir algo, así que hice una sonrisa falsa y traté de hablar de una forma amistosa."


translate spanish zhenya_route_8df9005a:



    mz "¿Qué es lo que quieres?"


translate spanish zhenya_route_ac521edd:



    "Respondió Zhenya fríamente."


translate spanish zhenya_route_fba62ae7:



    me "Me desperté y te habías ido, así que..."


translate spanish zhenya_route_ff81478a:



    "Zhenya se dio la vuelta con sus ojos brillando."


translate spanish zhenya_route_01ebb44b:



    mz "Me voy."


translate spanish zhenya_route_f2dc887f:



    "Dijo con una voz calmada."


translate spanish zhenya_route_25f7abbc:



    me "¿Dónde?"


translate spanish zhenya_route_86c8528c:



    mz "A casa."


translate spanish zhenya_route_fb5e4a90:



    me "¡Pero los autobuses no llegan aquí!"


translate spanish zhenya_route_e03488f6:



    "¿Autobuses? Qué demonios."


translate spanish zhenya_route_75e90b09:



    mz "Llegan."


translate spanish zhenya_route_93c27914:



    me "Aquí tienes, hice esto por ti..."


translate spanish zhenya_route_2fab920e:



    "Le di un ramo de flores, el cual acabó en el mismo instante en el suelo."


translate spanish zhenya_route_293d1e5d:



    mz "¡No necesito nada de ti! Tú, ayer... ¡No quiero saber nada de ti! No quiero saber qué sucedió, ¡pero estoy segura que fue todo culpa tuya!"


translate spanish zhenya_route_66e6b111:



    me "¿Qué fue mi culpa?"


translate spanish zhenya_route_40de19bd:



    mz "¡Todo! Esta estúpida boda, y lo que sucedió durante la noche..."


translate spanish zhenya_route_e218d65f:



    me "Pero estábamos de acuerdo... ¡Tú misma lo querías! ¿Recuerdas, hace un par de semanas atrás?"


translate spanish zhenya_route_f22f02db:



    mz "¡Ni tan siquiera te conocía hace un par de semanas atrás!"


translate spanish zhenya_route_0afb7a8c:



    "Claro, todo comenzó entonces."


translate spanish zhenya_route_89eaae80:



    "O {i}continuó{i}... ¡Ya no me importa más!"


translate spanish zhenya_route_8de561c3:



    "Decidí que podía ser feliz junto a Zhenya, pero ella desapareció igual que apareció... repentinamente."


translate spanish zhenya_route_47016d9b:



    "{i}Zhenya, sentada no en la biblioteca, sino en el porche de la cantina{/i}... fue hace tanto tiempo..."


translate spanish zhenya_route_a49f0e42:



    "Perdí mi esperanza una vez, dejé de ser humano, pero luego la conocí y fue casi como si la vida me diera una segunda oportunidad, a pesar de ser en este mundo extraño."


translate spanish zhenya_route_e96d834d:



    "Pero entonces se fue, y durante los ciclos sin fin lo olvidé todo. Volví a estar como lo estaba antes de conocerla por primera vez. Dejé de ser yo mismo..."


translate spanish zhenya_route_8209fb60:



    "¿Quizás {i}él{/i} tuviera razón...?"


translate spanish zhenya_route_713b6223:



    me "¿Qué dices...? Nos conocemos desde hace...{w} ¡Tampoco sé cuánto tiempo hace que nos conocemos ambos!"


translate spanish zhenya_route_53223da6:



    mz "No te conozco, ¡y tampoco quiero conocerte!"


translate spanish zhenya_route_5dc1718e:



    me "Pero espera, nosotros..."


translate spanish zhenya_route_4892f21d:



    "De repente la debilidad me golpeó y me derrumbé sobre mis rodillas, sólo para evitar caerme sobre mi propio rostro."


translate spanish zhenya_route_2cc03992:



    "Mi anillo se sentía muy cálido en mi dedo. Casi podía sentirlo arder físicamente."


translate spanish zhenya_route_4d5dd590:



    mz "¡Ten, cógelo!"


translate spanish zhenya_route_66517b70:



    "Zhenya cogió algo de su bolsillo y me lo tiró."


translate spanish zhenya_route_35c23299:



    mz "¡Me voy a dar un paseo!"


translate spanish zhenya_route_f4572a11:



    "Le di vueltas al delgado anillo de compromiso en mis manos y empecé a llorar."


translate spanish zhenya_route_f6b5b83d:



    "Nunca antes he llorado así. No en este mundo, no en el anterior..."


translate spanish zhenya_route_11c27875:



    "Y todo terminó."


translate spanish zhenya_route_2b610b28:



    "Para siempre."


translate spanish zhenya_route_8c05f068:



    "Supongo que algo humano permaneció en mí, así que decidí olvidarlo todo, con el fin de no volverme completamente loco."


translate spanish zhenya_route_2a64dbfe:



    "Pero ahora todos los recuerdos estaban aquí, y con ellos vino el dolor... no era tan malo, se había debilitado lentamente a través del tiempo, pero aun así me partía en dos."


translate spanish zhenya_route_f7d9e51c:



    "Casi como si fuera roto en pedazos y el único que me quedase fuera aquel que retuviera de alguna forma mi consciencia."


translate spanish zhenya_route_4f7b01ff:



    "Ni todo, ni parte, únicamente una triste coincidencia, un error aceptable en la perfecta ecuación de este mundo."


translate spanish zhenya_route_302b08db:



    pi4 "¡No estés tan triste, hermano!"


translate spanish zhenya_route_58fabdec:



    "Algún pionero estaba sentado a mi lado."


translate spanish zhenya_route_90087846:



    pi "¿Otra vez tú?"


translate spanish zhenya_route_4f5d7a29:



    pi4 "¿Quién yo?"


translate spanish zhenya_route_00129a4d:



    pi "¿Qué diferencia hay? Tú, él, aquel otro, este otro... ¡todos os parecéis!"


translate spanish zhenya_route_4c2f272f:



    "No me respondió.{w} Hizo un risita nerviosa."


translate spanish zhenya_route_10c3756d:



    pi "¿Viniste aquí para burlarte de mí? Hubiera sido mejor que me matarás aquella vez."


translate spanish zhenya_route_32babbd7:



    pi4 "Pero tú recordaste todo."


translate spanish zhenya_route_cce9bc51:



    pi "¡Lo recordé! ¡Y es incluso peor a causa de ello!"


translate spanish zhenya_route_56a872ce:



    pi4 "Tú eres el único culpable aquí."


translate spanish zhenya_route_a3eb2c09:



    pi "¿Culpable de qué? ¿Por tratar de impedir que partieras con la orejas de gata? ¡Sé que eras tú quien estaba en ese autobús!"


translate spanish zhenya_route_a2bffe06:



    "Aun así seguía sin haber rastro de emociones en el rostro del pionero."


translate spanish zhenya_route_6e23c048:



    pi4 "Quizá lo fueses, o quizá no... ¿qué más da ahora?"


translate spanish zhenya_route_2b5b71d1:



    pi "Eso significa que tú...{w} ¿lograste escapar?"


translate spanish zhenya_route_71961912:



    pi "¿Y volviste sólo para burlarte de mí?"


translate spanish zhenya_route_e1828c1c:



    pi4 "¿No hiciste acaso tú lo mismo?"


translate spanish zhenya_route_9ebc18c6:



    pi "Deja ya de darme lecciones morales, ¡por el amor de Dios!"


translate spanish zhenya_route_d6d21cef:



    "Estaba de rodillas y miraba a Zhenya, quien lentamente se estaba yendo más y más lejos."


translate spanish zhenya_route_b7251b6d:



    "Y por allí giró por la esquina. No puedo hacerla volver..."


translate spanish zhenya_route_28c9f488:



    "De todas maneras, sabía que ésta no era Zhenya... mi Zhenya de verdad desapareció ayer, ésta era tan sólo una muñeca desalmada que se le parecía.{w} Pero era eso, solamente se le parecía..."


translate spanish zhenya_route_03273ca5:



    "¡El cuento no puede durar para siempre!"


translate spanish zhenya_route_c7744369:



    "Este mundo es un infierno de verdad, y soy el demonio.{w} Y los demonios no pueden alcanzar la verdadera felicidad."


translate spanish zhenya_route_e23c07ec:



    pi "Solamente dime una cosa."


translate spanish zhenya_route_330cac58:



    "Rompí el silencio."


translate spanish zhenya_route_d964d736:



    pi "¿Por qué todo es así?"


translate spanish zhenya_route_64fb9aaa:



    pi4 "Tuviste una opción justo desde el principio, una elección... podrías haber hecho todo de forma totalmente diferente. Muchos lo hicieron correctamente... y otros lo hicieron como tú, y como tantos otros."


translate spanish zhenya_route_a49737fe:



    pi "Claro, cierto. ¡Todos ellos se fueron y ahora están viviendo felizmente con sus muñecas en el mundo {i}real{/i}!"


translate spanish zhenya_route_fbd62a8b:



    "Sonreí."


translate spanish zhenya_route_0d254476:



    pi4 "¿Crees que tu vida es mejor?"


translate spanish zhenya_route_89deef73:



    "Él enfatizó claramente la palabra «vida»."


translate spanish zhenya_route_d8d4112a:



    pi "Si eres tú...{w} Si eres tú quien me apartó a Zhenya de mí..."


translate spanish zhenya_route_46c6c1d3:



    "La rabia estaba creciendo en mi interior, ¡quería abalanzarme sobre él y golpearle hasta matarlo!"


translate spanish zhenya_route_fe54ef92:



    "Pero no estaba nada seguro de que existiera en el mundo físico."


translate spanish zhenya_route_a15e6140:



    "Pero incluso así, ¿qué habría cambiado?"


translate spanish zhenya_route_523cc556:



    pi4 "Podrías haber sido feliz con cualquier otra muchacha, esa es la razón por la que están aquí."


translate spanish zhenya_route_a68dd09f:



    "No me esperaba semejante respuesta."


translate spanish zhenya_route_ac729750:



    pi "¡Pero ninguna de ellas es real! Los únicos reales somos yo... tú... todos nosotros...{w} Zhenya..."


translate spanish zhenya_route_e55f7952:



    pi4 "Eso solamente para ti, en {i}tu mundo{/i}. Sabes que hay muchas realidades y en cada una de ellas, tú escoges tu propia creencia. Quizá solo hubiera un Semyon, pero este campamento le dio múltiples posibilidades."


translate spanish zhenya_route_0248499f:



    pi4 "Alguien se las apañó para escapar y ahora vive felizmente, tal como dijiste. Alguien... está atrapado, como tú."


translate spanish zhenya_route_ab00125a:



    pi "¿Puedes decirme qué lugar es éste? ¿Quién eres tú?"


translate spanish zhenya_route_755b6861:



    pi4 "Puedes pensar en mí como en el líder del campamento."


translate spanish zhenya_route_80725814:



    "El pionero hizo una sonrisa sin rasgo alguno de burla o ironía... y se desvaneció."


translate spanish zhenya_route_45f9e91b:



    "Me sentí un poco inquieto tras escuchar sus palabras."


translate spanish zhenya_route_2ce94d4c:



    "El mundo se envolvió de repente en niebla, y un brillante destello apareció frente a mí."


translate spanish zhenya_route_9c6336fb:



    "Miré a mi alrededor y me di cuenta de que los pioneros se fueron, así como el autobús."


translate spanish zhenya_route_7fbcf6f6:



    "¿Y ahora qué?"


translate spanish zhenya_route_48843b81:



    "Las puertas chirriaron silenciosamente y me oculté tras el monumento del pionero."


translate spanish zhenya_route_66517463:



    "Semyon caminó hasta la parada de autobús... uno de los Semyons, pero su expresión facial era dolorosamente familiar."


translate spanish zhenya_route_07e4236c:



    "He olvidado hace tiempo que yo puedo verme exactamente así."


translate spanish zhenya_route_cb5e1464:



    "Se quedó mirando la carretera por un rato y se dirigió hacia el campamento."


translate spanish zhenya_route_2016aa78:



    pi "¡No confíes en él!"


translate spanish zhenya_route_2eea460b:



    "Dije en un impulso."


translate spanish zhenya_route_0a2309cb:



    me "¡¿Quién eres?!"


translate spanish zhenya_route_3e4dff80:



    "Semyon miró sorprendido."


translate spanish zhenya_route_d6271c95:



    pi "¡Detente! ¡No te acerques más!"


translate spanish zhenya_route_c41b6a4c:



    me "De acuerdo, me quedaré aquí..."


translate spanish zhenya_route_9f354a24:



    pi "¿Le has visto? ¿Has hablado con él?"


translate spanish zhenya_route_3edfb9a5:



    me "¿De quién siquiera estás hablando?"


translate spanish zhenya_route_65856116:



    pi "Ya sabes quien..."


translate spanish zhenya_route_9513cd87:



    me "Sí..."


translate spanish zhenya_route_e3fcbbd3:



    pi "¿Qué te dijo?"


translate spanish zhenya_route_e88413fc:



    me "En realidad, nada..."


translate spanish zhenya_route_5d3e0c2a:



    pi "¿Te dio un consejo? ¿Te dijo qué hacer? ¿Te amenazó?"


translate spanish zhenya_route_3a4fb558:



    me "No, nada de eso...{w} Seguro, me pareció raro, pero nada más..."


translate spanish zhenya_route_7896c94c:



    pi "¡Recuerda que puede no estar solo! O aun más probable, está solo pero puede que te encuentres más pioneros que se parezcan a él."


translate spanish zhenya_route_0effa2c7:



    me "¿Y qué hay de ti? ¿Quién eres? ¿De quién te escondes?"


translate spanish zhenya_route_18d9b086:



    pi "Lo entenderás...{w} En su debido momento...{w} Sólo recuerda, ¡lo más importante es encontrar la salida!"


translate spanish zhenya_route_5adb12a4:



    "El viento se volvió realmente fuerte por un instante, y el mismo destello apareció frente a mí."


translate spanish zhenya_route_c1d579b7:



    "Y de nuevo, en un instante, el autobús y los pioneros aparecieron una vez más."


translate spanish zhenya_route_affe749c:



    "No sé para que le dije nada..."


translate spanish zhenya_route_72e87799:



    "Simplemente creí que era lo correcto."


translate spanish zhenya_route_175b483f:



    "Si sólo recordara lo que el remordimiento y el arrepentimiento eran, entonces posiblemente eso sería lo que estoy sintiendo."


translate spanish zhenya_route_6da9f141:



    mz "¡Un pionero debería estar siempre preparado!"


translate spanish zhenya_route_174cdd90:



    "Una voz familiar sonó como un trueno en medio de un cielo claro, y un golpe familiar en la espalda me sacó de mi estado catatónico en el que estaba."


translate spanish zhenya_route_9b924f7d:



    "Zhenya estaba de pie frente a mí."


translate spanish zhenya_route_3c83f193:



    "Al instante supe que ésta era Zhenya, ¡no la muñeca!"


translate spanish zhenya_route_00a94cb1:



    pi "Pero cómo..."


translate spanish zhenya_route_7f9e842d:



    "Ésas fueron las únicas palabras que pude susurrar."


translate spanish zhenya_route_ccbc9e0b:



    mz "No lo sé..."


translate spanish zhenya_route_e0c9575f:



    "Ella sonrió, pero su voz estaba temblando."


translate spanish zhenya_route_a0d084f7:



    mz "Antes, tras la boda, me desperté en un mundo diferente...{w} Pensé que te había perdido. Y luego... y luego hubo tantos bucles que casi olvidé..."


translate spanish zhenya_route_fc62eb93:



    pi "Lo siento."


translate spanish zhenya_route_5788b9db_1:



    "Dije en voz baja."


translate spanish zhenya_route_c6ad8ebd:



    pi "Perdóname por todo."


translate spanish zhenya_route_514806e6:



    mz "No pasó nada. Entonces {i}él{/i} vino y dijo que podría guiarme hasta el mundo en el que estás. Al principio no le creía, pero...{w} ¡aquí estoy!"


translate spanish zhenya_route_a283dd49:



    "Me abalancé hacia delante y abracé a Zhenya."


translate spanish zhenya_route_f3cdb758:



    pi "¡Perdóname! Lo olvidé... todo, ¡te olvidé! Te he hecho tantas cosas malas. No lo creía, no tuve esperanza. ¡Lo siento!"


translate spanish zhenya_route_f64a5bd9:



    mz "Uff, ¡me vas a asfixiar!"


translate spanish zhenya_route_70e18faf:



    "Ella se liberó de mis brazos."


translate spanish zhenya_route_d95a0aeb:



    mz "Pero ahora estoy aquí, ¡así que todo está bien!"


translate spanish zhenya_route_97688933:



    "Dijo entre lágrimas."


translate spanish zhenya_route_c21de771:



    pi "¿Cómo puedes estar tan tranquila?"


translate spanish zhenya_route_25a697d5:



    mz "Porque yo siempre supe que estaríamos juntos."


translate spanish zhenya_route_af19cecb:



    "Zhenya alzó su mano derecha, y en su dedo le vi un anillo que estaba tejido con Lirios de los Valles."


translate spanish zhenya_route_d16e8b07:



    pi "He perdido el mío, lo siento..."


translate spanish zhenya_route_7f2fc500:



    mz "Está bien, ¡compraremos uno nuevo!"


translate spanish zhenya_route_06ab0532:



    "Ella sonrió."


translate spanish zhenya_route_5608eebc:



    mtp "Ey, subid al autobús, ¡vosotros dos!"


translate spanish zhenya_route_7199c752:



    "Olga Dmitrievna estaba chillando con descontento."


translate spanish zhenya_route_ce617998_23:



    "..."


translate spanish zhenya_route_5fa8d405:



    "El autobús se sacudía con los baches tranquilamente mientras se dirigía lentamente hacia el distrito central...{w} ¡o dondequiera que fuese realmente!"


translate spanish zhenya_route_8e91937c:



    pi "Sabes, no puedo creer que todo vaya a terminar."


translate spanish zhenya_route_bb22c22e:



    "He pasado tantas cosas en la última semana..."


translate spanish zhenya_route_51ff3d8a:



    mz "Y una vez más, ¡conseguiste la mejor parte!"


translate spanish zhenya_route_7730d48a:



    "Se enojó Zhenya."


translate spanish zhenya_route_0db5e9a1:



    pi "No creo que quisieras cambiarte en mi lugar."


translate spanish zhenya_route_7ba78cc4:



    mz "¡Me lo imagino! Pero aun así es más interesante que estar sentada en la biblioteca durante días hasta el final."


translate spanish zhenya_route_661c1c64:



    pi "Tienes ideas raras sobre lo que es interesante..."


translate spanish zhenya_route_2c6f48c5:



    mz "¿Pensaste en ello?"


translate spanish zhenya_route_3a9e0089:



    pi "¿Sobre qué?"


translate spanish zhenya_route_745bf00d:



    mz "¿Qué sucederá... mañana? ¡Todavía tenemos que conocernos de alguna forma en el mundo real!"


translate spanish zhenya_route_9c5be399:



    pi "Oh, claro, déjame que tome nota de tu dirección."


translate spanish zhenya_route_902c41be:



    mz "Sí, claro, ¡así me puedes enviar una carta ahora mismo!"


translate spanish zhenya_route_d2a39874:



    "Sonreí."


translate spanish zhenya_route_7d57dcd6:



    mz "La memorizaré, adelante."


translate spanish zhenya_route_5317f0d7:



    "Intercambiamos números y direcciones."


translate spanish zhenya_route_a4e5d71a:



    "Es extraño que no tuviéramos esta idea desde un principio."


translate spanish zhenya_route_0161af1e:



    "Asumí que ni siquiera nunca esperaría escapar de este mundo, y simplemente no quisimos acordarnos de nuestras vidas anteriores."


translate spanish zhenya_route_af2214a1:



    mz "¿Cómo crees que será? ¿Cómo siempre? ¿Nos quedamos dormidos?"


translate spanish zhenya_route_db2a144a:



    "Zhenya estrechó mi mano y apretó su cuerpo entero contra el mío."


translate spanish zhenya_route_2d69ac30:



    pi "Eso espero. Pero al final, ¡esto no es lo peor!"


translate spanish zhenya_route_909b2197:



    mz "Diría que uno de los mejores."


translate spanish zhenya_route_5464c3dd:



    "Zhenya bostezó y cerró los ojos."


translate spanish zhenya_route_c7d14696:



    mz "Dulces sueños. Nos vemos."


translate spanish zhenya_route_d0f0885a:



    "No dije nada y tan sólo continué mirando a través de la ventana el panorama por el que circulábamos."


translate spanish zhenya_route_1f47ecf1:



    "El campamento Sovyonok se quedaba atrás, y no retornaré allí."


translate spanish zhenya_route_e536a6fa:



    "Sí, estaba seguro de eso."


translate spanish zhenya_route_900f8104:



    "Pero siempre será una parte de mi vida."


translate spanish zhenya_route_e1e4cc09:



    "He cambiado ahí, he cambiado múltiples veces."


translate spanish zhenya_route_96ece0c6:



    "Comparado con el tiempo transcurrido ahí, unos simples 50-60 años que he permanecido en el mundo real son solamente un breve instante, ¡pero me aseguraré de vivirlos como un humano de verdad!"


translate spanish zhenya_route_f8c0c4d9:



    mz "Sí, probablemente seas malo, tal como dijo aquel pionero."


translate spanish zhenya_route_41d7cf0b:



    "Murmuró Zhenya en su sueño."


translate spanish zhenya_route_ad02f53a_1:



    me "¿Qué?"


translate spanish zhenya_route_1bdc5a24:



    mz "Nada, ¡duérmete ya!"


translate spanish zhenya_route_43560fef:



    "Se rio y me pellizcó dolorosamente."


translate spanish zhenya_route_ce617998_24:



    "…"


translate spanish zhenya_route_5ca93524:



    "Me pregunto qué sentían todos en mi lugar..."


translate spanish zhenya_route_3882f569:



    "¿La misma pérdida de conciencia en los primeros momentos, el sentimiento de ansiedad, de pánico?"


translate spanish zhenya_route_6cfbc782:



    "Volví a mi antiguo piso, pero no reconocía nada de él."


translate spanish zhenya_route_b1bb61b3:



    "Sí, se sentía definitivamente como estar en una casa en la que viviste hace muchos años atrás... todo parece estar en su correcto lugar, pero no recuerdas nada, piensas simplemente que así es como se supone que es."


translate spanish zhenya_route_2ed037cd:



    "Ya he olvidado el día en que desaparecí de este mundo (y tampoco estoy muy seguro acerca del año), pero es invierno fuera, y eso significa que no mucho tiempo había pasado {i}aquí{/i}."


translate spanish zhenya_route_817af00a:



    "Claro, ya no tengo 17 años, ¡pero tampoco me gusta mi edad {i}real{/i}!"


translate spanish zhenya_route_e2e4abf4:



    "En fin, parece que todo fue bien, ¿mi retorno no tuvo ninguna inesperada consecuencia?"


translate spanish zhenya_route_7703e5b3:



    "Zhenya..."


translate spanish zhenya_route_cab53a73:



    "Corrí deprisa hasta la mesa y escribí la dirección y el número de teléfono con manos nerviosas."


translate spanish zhenya_route_ae449981:



    "Obviamente no sería capaz de olvidarme de ellos, pero sería mejor tener una copia por si acaso."


translate spanish zhenya_route_827b177a:



    "Y ahora, ¿qué?"


translate spanish zhenya_route_dee0474e:



    "¿Cómo funciona este mundo de todas formas?"


translate spanish zhenya_route_d061d06a:



    "Las leyes son completamente diferentes aquí...{w} ninguna de las cuales, para ser sinceros, nunca he comprendido."


translate spanish zhenya_route_5ac00794:



    "Y ahora soy como un recién nacido, ¡obligado a vivir una vida de adulto!"


translate spanish zhenya_route_c294b867:



    "Pero ahorrémonos eso para después, por ahora... ¡necesito hallar a Zhenya!"


translate spanish zhenya_route_103e53ea:



    "Alguien tocó el timbre de la puerta."


translate spanish zhenya_route_8bd0d3e5:



    "Humanos.{w} Humanos reales, no pioneros..."


translate spanish zhenya_route_e9e7cdfa:



    "Tengo que abrir la puerta, ¿qué otra cosa si no puedo hacer? Finalmente he retornado, tengo que vivir en este mundo ahora."


translate spanish zhenya_route_c230ba61:



    "Lentamente me acerqué caminando hasta la puerta, giré la llave de la cerradura, agarré la maneta y me quedé de piedra."


translate spanish zhenya_route_2f9f442b:



    mz "¿Y bien? ¿Por cuánto tiempo debería permanecer esperando en la entrada?"


translate spanish zhenya_route_3b74cebc:



    "Abrí la puerta de un estirón y vi a Zhenya..."


translate spanish zhenya_route_6b1df7af:



    "Se veía tal y como en el campamento."


translate spanish zhenya_route_8a5df7ac:



    me "Pero...{w} ¿cómo?"


translate spanish zhenya_route_120e3d2a:



    mz "¿Recuerdas que siempre llegaba al campamento una semana antes que tú? Pues retorné una semana antes."


translate spanish zhenya_route_2b81f411:



    "Me olvidé de Sovyonok, me olvidé del mundo real, la única cosa que importa es... ¡que estamos juntos!"


translate spanish zhenya_route_85e2092d:



    "Agarré a Zhenya y cargué con ella hasta la habitación."


translate spanish zhenya_route_529e952d:



    mz "Bueno, ¿sólo vamos a estar de pie por aquí?"


translate spanish zhenya_route_7b813b45:



    me "Ah, lo siento..."


translate spanish zhenya_route_c7fa01d1:



    "Zhenya saltó livianamente en el suelo."


translate spanish zhenya_route_0edeb144:



    mz "Espera un poquito... Tenemos que hacerlo ahora mismo, puede que no tengamos tiempo para eso luego."


translate spanish zhenya_route_7cfc6c8e:



    "Ella sonrió pícara y comenzó a rebuscar entre su cartera."


translate spanish zhenya_route_cf917bd8:



    mz "Dame tu mano."


translate spanish zhenya_route_d4a01b6d:



    "Extendí mi brazo hacia ella y Zhenya me puso un anillo en mi dedo."


translate spanish zhenya_route_9ede4597:



    "Uno muy sencillo... como aquel del campamento."


translate spanish zhenya_route_6862cd26:



    "En la tenue luz de una lámpara polvorienta del techo, un anillo de oro brillaba en su dedo."


translate spanish zhenya_route_5880720c:



    me "Creo que yo debería haber comprado los anillos..."


translate spanish zhenya_route_ee43c51d:



    mz "Bueno, a juzgar por el aspecto de tu cueva, no creo que tengas el dinero para ello."


translate spanish zhenya_route_6d386883:



    "Nos reímos, sinceramente de corazón."


translate spanish zhenya_route_037f0871:



    "Nieve de verdad estaba cayendo lentamente afuera de la ventana. Blanca y bella nieve, con la oscuridad de la noche que se deslizaba entre miles y miles de luces de la gran ciudad, la cual cantaba con cientos de voces, recordándonos que aun seguíamos vivos y que todavía estábamos juntos."


translate spanish zhenya_route_8e128b77:



    "Acaricié suavemente con mi mano la mejilla de Zhenya.{w} Cálida, real..."


translate spanish zhenya_route_6abeee24:



    me "¿No desaparecerás esta vez?"


translate spanish zhenya_route_871af259:



    mz "¡Mejor que te lo creas! ¡Aun me debes mi dinero por los anillos!"


translate spanish zhenya_route_04ae1cfa:


    "Abracé a Zhenya y la besé como nunca antes..."
# Decompiled by unrpyc: https://github.com/CensoredUsername/unrpyc
