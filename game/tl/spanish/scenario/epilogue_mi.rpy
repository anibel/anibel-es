
translate spanish epilogue_mi_4273f0c8:


    "Porque en ocasiones un sueño solamente es un sueño."


translate spanish epilogue_mi_12b67fb5:


    "Uno tendría que estar mirando en internet, buscando una interpretación de un sueño que invadió tu mundo de ensueño de semejante burda manera. Uno no debe asumir que eso es una señal, una profecía o una predicción del futuro."


translate spanish epilogue_mi_e27e5ad0:


    "En ocasiones un sueño solamente es un sueño, una bella historia que sencillamente no va a ocurrir en tu vida real, un cuento de hadas esbozado por tu descansado cerebro y pintado por tu imaginación."


translate spanish epilogue_mi_5d3e8602:


    "En ocasiones un sueño solamente es un sueño..."


translate spanish epilogue_mi_f429eb4f:


    "Incluso si se sueña en diversas partes."


translate spanish epilogue_mi_5192a263:


    ma "Maldita sea, estoy muy cansado de todo esto..."


translate spanish epilogue_mi_b6e6bdf4:


    my "¿Ein?"


translate spanish epilogue_mi_7ac33094:


    "Levanté mi mirada y vi una muchacha colgando encima mío."


translate spanish epilogue_mi_39748bd3:


    my "¿De qué estás cansado exactamente?"


translate spanish epilogue_mi_73e7a292:


    ma "¡De todo!"


translate spanish epilogue_mi_0e1ee137:


    my "No es como si alguien te hubiera obligado a hacer esto... ¡o como si te estuviera obligando!"


translate spanish epilogue_mi_eb1fb061:


    ma "Bueno...{w} ¡No es tan sencillo!"


translate spanish epilogue_mi_d9924f3f:


    my "Oh, ¡y tanto que es muy sencillo!"


translate spanish epilogue_mi_fc671583:


    ma "Maldita sea, siempre tú..."


translate spanish epilogue_mi_f98fd3f1:


    mt "¡Para ya de charlar! ¡Vamos venga, a trabajar!"


translate spanish epilogue_mi_f45e1707:


    "Una voz que no admitía excusas provino de lejos."


translate spanish epilogue_mi_a3bffb92:


    "El autobús se quedó vacío en un instante."


translate spanish epilogue_mi_ab07d7f4:


    "La muchacha con la que estaba discutiendo que se enfurruñó como una niña, se giró de golpe, repiqueteando los tacones y se dirigió a la salida."


translate spanish epilogue_mi_07837af7:


    "Su nombre era Masha."


translate spanish epilogue_mi_44f92a18:


    mt "Vale, ¿preparados?"


translate spanish epilogue_mi_514d8a74:


    "Una mujer apareció. Su rostro expresaba simultáneamente la agonía por el destino de toda la humanidad, arrogancia, concentración y fatiga."


translate spanish epilogue_mi_da181777:


    my "Claro... Seguro..."


translate spanish epilogue_mi_83cfac39:


    mt "¡Excelente!"


translate spanish epilogue_mi_a1a01658:


    mt "Pues...{w} ¡Acción!"


translate spanish epilogue_mi_86b86796:


    "No me di cuenta cuando ella se fue del autobús que una cámara apareció, apuntándome a mí."


translate spanish epilogue_mi_d2175e55:


    "Cerré mis ojos... está bien, debía estar adormecido de acuerdo con el guión."


translate spanish epilogue_mi_726cd530:


    "¿Cuánto tiempo debo quedarme sentado así? ¿Diez segundos? ¿Veinte?"


translate spanish epilogue_mi_5c06941f:


    "Eso debería bastar."


translate spanish epilogue_mi_44a112d2:


    "Lentamente abrí mis ojos y empecé a inspeccionar el autobús con una mirada ignorante."


translate spanish epilogue_mi_c58cb760:


    "Eso es, ¡no tengo ni la más mínima idea de dónde estoy!"


translate spanish epilogue_mi_c96b254d:


    "¡¿Qué es esto?! ¿Un Icarus? ¿Pero por qué?"


translate spanish epilogue_mi_c77489ba:


    "¡Así que tenía que poner la mayor cara de sorpresa que jamás haya hecho!"


translate spanish epilogue_mi_28036f42:


    "Debería imaginarme que estoy sentado en un inodoro, leyendo un libro y entonces, de repente, la puerta se abre de golpe y veo a Jean Reno con una escopeta."


translate spanish epilogue_mi_1d68d7b6:


    "Perdóneme, ¿no cree usted que -perdone por mi falta de francés- estoy plantando un pino aquí?"


translate spanish epilogue_mi_81e3cf06:


    "¡Seguramente estaría sorprendido!"


translate spanish epilogue_mi_74e80531:


    "Sencillamente no podría sobrellevarlo y rompería a carcajadas."


translate spanish epilogue_mi_92ab1fe0:


    mt "¡Detente! ¡Detente!"


translate spanish epilogue_mi_5aaa9752:


    "Chilló la directora."


translate spanish epilogue_mi_f84967b1:


    mt "¡Es una atrocidad! ¡Ya llevamos filmando como una semana, pero todavía no tenemos nada!"


translate spanish epilogue_mi_d4d147e6:


    "Saltó a mi lado."


translate spanish epilogue_mi_ca068b02:


    mt "¿Qué pasa esta vez?"


translate spanish epilogue_mi_f4f4dacd:


    my "Sabes, yo..."


translate spanish epilogue_mi_d82fb8a9:


    "Bueno, a duras penas apreciaría mi historia sobre Jean Reno..."


translate spanish epilogue_mi_27f98f9f:


    my "Lo siento..."


translate spanish epilogue_mi_399d102b:


    "Era difícil mirarla a sus ojos."


translate spanish epilogue_mi_548b6f7a:


    "Yo no parecía estar muy preocupado por hacer mal otra toma, sino principalmente porque me preocupaba romper en carcajadas de nuevo."


translate spanish epilogue_mi_ed0fe155:


    mt "Muy bien, ¡descanso!"


translate spanish epilogue_mi_4d90d1cf:


    "Nuestra directora se quitó el sombrero de panamá y empezó a abanicarse con él."


translate spanish epilogue_mi_d7d387fd:


    mt "Hace mucha calor..."


translate spanish epilogue_mi_2c1992b5:


    sh "Bueno, es verano después de todo."


translate spanish epilogue_mi_1d5a2e57:


    "Shurik apareció de la nada, demostrando ser bastante observador."


translate spanish epilogue_mi_193c877e:


    mt "¡Pues claro, cabeza cuadrada! ¡Mejor que te ocupes de la cámara!"


translate spanish epilogue_mi_2f643813:


    "Shurik tranquilamente esquivó el sombrero de panamá que le fue lanzado y se fue a su cámara."


translate spanish epilogue_mi_0781974d:


    "Él siempre es así... calmado, sabio y...{w} aburrido, soso y bastante predecible."


translate spanish epilogue_mi_3b0ed2d0:


    "Me salí del autobús y me dirigí hacia la cantina para probar bocado."


translate spanish epilogue_mi_11a4fd66:


    "Y corrí hacia Masha hasta las puertas..."


translate spanish epilogue_mi_eb88f1fb:


    ma "¿Y bien?"


translate spanish epilogue_mi_48975165:


    my "Nada..."


translate spanish epilogue_mi_30261bba:


    ma "No me sorprende."


translate spanish epilogue_mi_23457c21:


    my "¿Quieres comer algo?"


translate spanish epilogue_mi_5a6ffa85:


    "Me miró fijamente por un rato con una mirada desconcertada."


translate spanish epilogue_mi_c8eb94cd:


    ma "Vale, vamos."


translate spanish epilogue_mi_a274cb3e:


    "Qué raro, parece que Masha ya haya visitado la cantina... ¿qué es? ¿es que no tuvo tiempo para almorzar?"


translate spanish epilogue_mi_00d4b994:


    "La examiné pensativamente desde detrás... sus dos coletas eran realmente largas, colgaban de lado a lado, chocando allí y allá con cualquier cosa que se encontraran en su camino."


translate spanish epilogue_mi_5b40950c:


    my "¿Estás segura de que estás cómoda con tu cabello de esa forma?"


translate spanish epilogue_mi_a5498145:


    "Le pregunté a Masha cuando tomamos asiento en una mesa."


translate spanish epilogue_mi_64c890f7:


    ma "¿Qué problema hay?"


translate spanish epilogue_mi_5e7e8446:


    my "Bueno, son algo...{w} largas, ¿no?"


translate spanish epilogue_mi_518927bf:


    ma "Es una parte de mi personaje, ¡cretino!"


translate spanish epilogue_mi_be22885b:


    my "Como si fueran más cortas antes de eso..."


translate spanish epilogue_mi_b990f6b1:


    "Me lanzó una mirada furiosa y procedió violentamente a mezclar el contenido de su paquete de fideos instantáneos."


translate spanish epilogue_mi_afce6747:


    ma "Un día voy a coger gastritis gracias a esta dieta."


translate spanish epilogue_mi_a6971a64:


    my "Ya lo capto..."


translate spanish epilogue_mi_fad116df:


    ma "Oh, claro, ¡apuesto a que ya te has tragado todos los refrigerios de esta gente!"


translate spanish epilogue_mi_289391d1:


    my "¿Y qué tiene de malo?{w} No cobro por este trabajo, ¡ya lo sabes! ¡Y es duro... el papel protagonista, después de todo! ¡No me bebo las cámaras o las cintas!"


translate spanish epilogue_mi_513a7fe4:


    ma "Oh, ¡como si fueras a por el Óscar!"


translate spanish epilogue_mi_1e087707:


    my "Quién sabe... quién sabe..."


translate spanish epilogue_mi_5175d639:


    "Me recliné en la silla y encendí un cigarrillo."


translate spanish epilogue_mi_673b46b1:


    ma "Sólo para tu información, esto es un campamento de niños, ¡por si lo has olvidado!"


translate spanish epilogue_mi_8a8d8f52:


    my "Claro, ¿ves algún crío por los alrededores?"


translate spanish epilogue_mi_6f794cba:


    "Masha sólo resopló y no contestó."


translate spanish epilogue_mi_b3a659cb:


    my "Y, por cierto, todos los grandes tuvieron que empezar por algo...{w} Algunos de ellos, por ejemplo, comenzaron con un proyecto de grado..."


translate spanish epilogue_mi_bbbe361d:


    ma "¡Ni un solo humano cuerdo jamás pensaría en rodar semejante porquería!"


translate spanish epilogue_mi_f47e1355:


    "Observó con cautela a su alrededor con una mirada intimidante, asegurándose de que nadie excepto nosotros estaba en la cantina, y prosiguió."


translate spanish epilogue_mi_5cd04ce7:


    ma "Solamente nuestra Olga Dmitrievna podría."


translate spanish epilogue_mi_c4de2e46:


    my "Nadie te está obligando a hacerlo."


translate spanish epilogue_mi_143442a9:


    ma "Oh, ¿en serio?"


translate spanish epilogue_mi_9912a5af:


    "Dijo lentamente Masha con voz amargada."


translate spanish epilogue_mi_00cde85b:


    ma "Como si hubiera tenido opción. O tú. O cualquier otro..."


translate spanish epilogue_mi_d9a5ec9c:


    my "Bueno, para empezar, podrías haber propuesto otro tema."


translate spanish epilogue_mi_ba9e80db:


    ma "Proponer otro tema...{w} Qué tema..."


translate spanish epilogue_mi_20caf834:


    "Todo este tiempo Masha había estado esparciendo sal por encima de su taza de fideos, y no parecía que fuera a parar."


translate spanish epilogue_mi_53e387c4:


    my "Veo que te gusta salado..."


translate spanish epilogue_mi_20f54ffc:


    ma "¿Qué?"


translate spanish epilogue_mi_124641aa:


    "Dirigí mi mirada a su taza."


translate spanish epilogue_mi_321a700f:


    ma "¡Maldita sea!"


translate spanish epilogue_mi_a30b21a1:


    "Ella soltó el salero de golpe sobre la mesa y, desgraciada, dejó caer su mirada en la pequeña montaña de sal que, como un iceberg luchando contra el calentamiento global, se resistía a disolverse en el caldo."


translate spanish epilogue_mi_f206e4c8:


    ma "Oh venga ya..."


translate spanish epilogue_mi_111d10a4:


    "De repente lo sentía por Masha."


translate spanish epilogue_mi_ad4d8e3a:


    "De todos modos, apenas podía digerir esta basura... los sándwiches son mucho mejores."


translate spanish epilogue_mi_e6bf5c70:


    my "Ten."


translate spanish epilogue_mi_41d4a8f6:


    "Le empujé mi tazón en su dirección."


translate spanish epilogue_mi_7d3aefd1:


    "Lo agarró en silencio y comenzó a comer."


translate spanish epilogue_mi_5307eb63:


    my "¿No vas a agradecérmelo?"


translate spanish epilogue_mi_6d330cb9:


    ma "¡Pero todo es culpa tuya!"


translate spanish epilogue_mi_9536b49c:


    my "¿Qué exactamente?"


translate spanish epilogue_mi_c8bad511:


    ma "Tu me distraíste y yo...{w} De todas formas, ¡es tu culpa!"


translate spanish epilogue_mi_8c82cb1d:


    my "Ah, da igual..."


translate spanish epilogue_mi_90823446:


    "Masha permaneció comiendo en silencio."


translate spanish epilogue_mi_e695e8fb:


    "Terminé de fumarme el cigarrillo y lo apagué aplastándolo en la esquina de la mesa."


translate spanish epilogue_mi_2f0cd122:


    my "¿Qué es exactamente lo que no te gusta?"


translate spanish epilogue_mi_62eb8e6c:


    ma "Bueno, en realidad son buenos, si te acostumbras a ellos... sabrosos fideos."


translate spanish epilogue_mi_af79cf2d:


    "Me quedé mirando a Masha de tal forma que demostró cuán tonta e irrelevante era su respuesta."


translate spanish epilogue_mi_a4712259:


    ma "Ah, estás hablando de la película..."


translate spanish epilogue_mi_e9b89811:


    my "Sobre la película."


translate spanish epilogue_mi_9e1884cf:


    ma "¡Coge mi personaje, por ejemplo!{w} Aunque no te importara usarlo como ejemplo, ¡es la peor parte del guión! ¿Acaso lo leíste cuando fue escrito?"


translate spanish epilogue_mi_a744ace5:


    "Por primera vez en estos dos días, la pereza me invadió."


translate spanish epilogue_mi_d3a9b814:


    my "Bueno, lo he leído..."


translate spanish epilogue_mi_bf175512:


    ma "Y entonces qué, ¿te parece normal a ti?"


translate spanish epilogue_mi_166834c3:


    my "¿Exactamente qué?"


translate spanish epilogue_mi_90e5468d:


    ma "El personaje, ¡mi personaje!"


translate spanish epilogue_mi_1ebebef3:


    my "¿Qué problema hay con él?"


translate spanish epilogue_mi_dee08048:


    "Decidí hacerme el idiota hasta el final."


translate spanish epilogue_mi_7d9ee071:


    ma "¡Para ya de burlarte de mí!"


translate spanish epilogue_mi_25e9c59a:


    "Si pudiera matarme, mi muerte habría sido una de las más dolorosas."


translate spanish epilogue_mi_534485c8:


    my "Está bien, vale...{w} Cálmate."


translate spanish epilogue_mi_bc30c075:


    "Masha fijó su mirada en mí."


translate spanish epilogue_mi_f7f9f25d:


    my "¿Qué pasa con tu personaje? Solamente es una muchacha retrasada normal y corriente...{w} Hay muchas por ahí igualmente, ¿no?"


translate spanish epilogue_mi_db110686:


    "Al principio se puso roja, luego azul y finalmente se puso verde."


translate spanish epilogue_mi_979d0955:


    "No estoy seguro de que su cambio de colores en su rostro durante un fracción de segundo fuera realmente así, pero de todas formas Masha parecía terrible."


translate spanish epilogue_mi_69b1e1a6:


    ma "Sabes, eso es desagradable."


translate spanish epilogue_mi_83a2f0c6:


    "Dijo repentinamente tras relajarse."


translate spanish epilogue_mi_d122e6e9:


    my "Creo que es perfectamente normal...{w} El reparto de una película -o un libro, en su caso- no puede consistir únicamente en personajes simpáticos, inteligentes y guapos. También necesitamos papeles secundarios."


translate spanish epilogue_mi_baea95af:


    ma "Secundarios, ¡pero no retrasados!"


translate spanish epilogue_mi_b4e96502:


    my "Como si no nos los encontráramos en la vida real..."


translate spanish epilogue_mi_8951537d:


    ma "¡Pero yo no soy así!"


translate spanish epilogue_mi_429736bb:


    my "¡Pero no estás interpretándote a ti misma!"


translate spanish epilogue_mi_cb5eb573:


    "Su tozudez me empezaba a poner de los nervios."


translate spanish epilogue_mi_97cf9711:


    ma "La gente lo verá y dirá: ¡Masha es una retrasada!"


translate spanish epilogue_mi_9439a255:


    "Cubrí mi cara con mis manos y apoyé mi cabeza encima de la mesa."


translate spanish epilogue_mi_7928ecf8:


    my "Vale, ¿y qué sugieres ahora?"


translate spanish epilogue_mi_82e7adc3:


    ma "No lo sé...{w} ¡Es que todo es pésimo!"


translate spanish epilogue_mi_b9def702:


    my "¿Mejorará si te pones tan sólo a llorar y a quejarte?"


translate spanish epilogue_mi_499a3ecd:


    ma "¡No me estoy quejando!"


translate spanish epilogue_mi_4fa7d502:


    my "Oh, como si no lo pudiera escuchar..."


translate spanish epilogue_mi_d6c365e1:


    "Bueno, preferiría no poder escucharlo."


translate spanish epilogue_mi_51775003:


    ma "¡¿Sabes qué?!"


translate spanish epilogue_mi_012acf66:


    "Abrí un ojo y alcé la vista a la encolerizada Masha."


translate spanish epilogue_mi_5cec30a4:


    my "¿Qué?"


translate spanish epilogue_mi_99d8ebb3:


    ma "¡Sólo eres un bastardo insensible!"


translate spanish epilogue_mi_7893761a:


    my "¡Como tú digas, mademoiselle!"


translate spanish epilogue_mi_c5902aa6:


    ma "Y..."


translate spanish epilogue_mi_44ce257f:


    "No pudo terminar de acabar la frase... la puerta se abrió de golpe, y un grito de alegría nos alcanzó."


translate spanish epilogue_mi_c3914184:


    sa "¡Masha-nyan! ¡Semyon-kun!"


translate spanish epilogue_mi_f994b25d:


    "Oh, ahí va otra loca de remate..."


translate spanish epilogue_mi_6bdb159c:


    "Oh Señor, ¿qué hice mal...?"


translate spanish epilogue_mi_d10088d4:


    "Giré mi cabeza y miré escépticamente a la muchacha que corría por la cantina."


translate spanish epilogue_mi_9a041eba:


    "Sasha.{w} Una de los protagonistas."


translate spanish epilogue_mi_239efec8:


    "Si comparamos la Sasha de la vida real con el personaje que va a interpretar, pues entonces las palabras de Masha sobre la injusticia de este mundo no parecen tan desafortunadas."


translate spanish epilogue_mi_92638c32:


    "Sin embargo, por el lado bueno, uno no puede ignorar el talento para actuar de Sasha... ¡Realmente se vuelve una persona totalmente diferente frente a la pantalla!"


translate spanish epilogue_mi_c612ee04:


    ma "Te he dicho miles de veces que... ¡no me llames nunca Masha-nyan!"


translate spanish epilogue_mi_fe3b21aa:


    "Masha apretó sus dientes."


translate spanish epilogue_mi_8b59578c:


    sa "Vale, ¡pues será Mashyan! ¿Puedo, puedo?"


translate spanish epilogue_mi_389929aa:


    "Sasha agarró de un arrebato la silla, la movió a mi lado y se sentó junto a mí."


translate spanish epilogue_mi_a334a043:


    ma "Bicho raro chiflado...{w} En primer lugar, ¿cómo puede entrar en la universidad gente como esa...?"


translate spanish epilogue_mi_4b8e4bdb:


    sa "¡Nya! ¡Nya! ¿De qué habláis?"


translate spanish epilogue_mi_c8b5ead4:


    "Sasha pareció ignorar la opinión de Masha sobre su inadecuada profesionalidad."


translate spanish epilogue_mi_c7467996:


    my "Oh, solamente estábamos tratando exactamente qué no le gusta a Masha-nyan de nuestro guión."


translate spanish epilogue_mi_298983e2:


    ma "¡Sólo espera hasta que te arranque el nyan a ti también!"


translate spanish epilogue_mi_84102612:


    "Trató de lanzarme un puñetazo en mi frente, pero lo esquivé y su puño golpeó sobre la mesa."


translate spanish epilogue_mi_f5ad59fd:


    "Sasha soltó un breve lloro y empezó a temblar."


translate spanish epilogue_mi_9038c77e:


    my "¿Ves cuán insatisfecha está? Una mente curiosa joven, que no busca ningún compromiso, preparada para un largo y meticuloso trabajo, para abnegarse y rechazar el frágil mundo material. ¿Y todo para qué? ¡Por la verdad!"


translate spanish epilogue_mi_bd2c4c91:


    sa "Guau, ¡eres muy inteligente!"


translate spanish epilogue_mi_3ef4a660:


    "Ronroneó Sasha y se abrazó a mi brazo."


translate spanish epilogue_mi_852d3af8:


    "Qué lástima, tú no lo eres..."


translate spanish epilogue_mi_c573c728:


    ma "Nooo, no tienes remedio..."


translate spanish epilogue_mi_f76258c4:


    my "Me tomaré eso como un cumplido."


translate spanish epilogue_mi_e76f1572:


    "Masha agarró su teléfono móvil de su bolsillo y comprobó la hora."


translate spanish epilogue_mi_10fe4a30:


    ma "Vale, hora de irse."


translate spanish epilogue_mi_1b4f07fe:


    my "¿Dónde te vas? Sólo apareces a partir del día dos."


translate spanish epilogue_mi_fd715212:


    ma "Eres tú quien debería irse, ¡cretino!"


translate spanish epilogue_mi_ed7479aa:


    sa "¡Yo también, yo también!"


translate spanish epilogue_mi_b3fcc6a8:


    "Sasha saltaba, todavía pegada a mi brazo."


translate spanish epilogue_mi_12e0f2c5:


    my "Supongo pues..."


translate spanish epilogue_mi_3cd326b5:


    "Me levanté, puse la silla bajo la mesa y miré hacia Masha."


translate spanish epilogue_mi_1e6a6efa:


    my "Nos vemos."


translate spanish epilogue_mi_f04f64a5:


    ma "Oh, ves yendo..."


translate spanish epilogue_mi_99aa653e:


    "Ella resopló y se dio la vuelta."


translate spanish epilogue_mi_eb124edb:


    sa "¡Ittekimasu!"


translate spanish epilogue_mi_102232fe:


    ma "Veteyasu..."


translate spanish epilogue_mi_400f3980:


    "Justo cuando eso alcanzó mi oído, Sasha me pasó de largo dejando atrás paquetes de fideos vacíos..."


translate spanish epilogue_mi_a20cefa7:


    "..."


translate spanish epilogue_mi_cf8c504b:


    "Estaba sentado en el autobús, ojeando el guión perezosamente."


translate spanish epilogue_mi_5c5ba007:


    "¿Pero qué clase de idiota escribió esto?"


translate spanish epilogue_mi_f3c1a9e9:


    "Oh, claro, ese sería yo..."


translate spanish epilogue_mi_5e46e93b:


    "Dejé a un lado el montón de hojas impresas mal grapadas y fijé mi vista fuera de la ventana."


translate spanish epilogue_mi_aceecbb8:


    "Desde luego, realmente hacía calor."


translate spanish epilogue_mi_d760f21d:


    "Incluso sin Shurik haciendo de Capitán Obvio... hacía calor."


translate spanish epilogue_mi_2ef129f9:


    "Por ejemplo, tomemos el asfalto. No le importa un pimiento que justo ayer era invierno y de acuerdo al guión... ya sabes qué tendría que estar fundiéndose justo ahora mismo."


translate spanish epilogue_mi_9380d7a8:


    "Y a los saltamontes no les importa... ellos siguen practicando."


translate spanish epilogue_mi_856be263:


    "Por cierto, ¿cuál de ellos estridula?{w} ¿Son los grillos o los saltamontes? ¿O las cigarras?{w} Qué va, esos chillan..."


translate spanish epilogue_mi_64bfb5a6:


    "Maldita sea, no tengo ni internet a mano para un rápido vistazo..."


translate spanish epilogue_mi_0ef08c24:


    "Aunque para qué... El guión está de acuerdo con la aprobación y no puede ser editado ya."


translate spanish epilogue_mi_a078b3cc:


    "Y de todas formas, ¿quién se iba a preocupar de los saltamontes en nuestra película?"


translate spanish epilogue_mi_e5cca34a:


    "Aunque, sería una espléndida idea para rodar un cortometraje de terror, propio del espíritu de los años cincuenta de las películas de terror de serie B americanas."


translate spanish epilogue_mi_3f1a9553:


    "Como, por ejemplo, «Mi primo es un Saltamontes». O «El Día del Grillo». «Un Vuelo Sobre la Guarida de la Cigarra»..."


translate spanish epilogue_mi_ff14da99:


    "Si me tengo que pasar cinco minutos más aquí, ¡yo mismo me voy a convertir en un insecto!"


translate spanish epilogue_mi_578e7bc1:


    "Oh, ¿por qué no hay aire acondicionado en los antiguos autobuses soviéticos...?"


translate spanish epilogue_mi_a93c1fe4:


    mt "¡Ey! ¡Hola! ¿Alguien en casa?"


translate spanish epilogue_mi_cc507cf7:


    "Un altavoz me apuntaba y justo detrás de él reconocí el rostro de Olga Dmitrievna a través del aire fundiéndose."


translate spanish epilogue_mi_f4a4def9:


    my "Por favor deja el mensaje después de..."


translate spanish epilogue_mi_619302ff:


    "Sentí un golpe."


translate spanish epilogue_mi_5d7f43c9:


    my "¡Ey! ¡No puedes agredir al protagonista de esta manera! ¿Acaso has leído mi prueba piloto?"


translate spanish epilogue_mi_ffdbc81e:


    mt "¡Cállate!{w} ¡Ésta tiene que ser la última toma del autobús!"


translate spanish epilogue_mi_968a77a4:


    "Quería más que nadie eso mismo... después de todo, es mejor estar fuera que atrapado en este mueve-derrite-hombres."


translate spanish epilogue_mi_61c207fa:


    "La cámara de Shurik estaba suspendida por encima mío."


translate spanish epilogue_mi_3cf97e79:


    "Actué como si realmente estuviera desesperado sobre lo que estaba ocurriendo, encolerizado crucé el autobús, corriendo afuera y por sus alrededores y, finalmente, derrumbándome en la acera..."


translate spanish epilogue_mi_d536fac8:


    mt "¡Cortad!"


translate spanish epilogue_mi_cb64514a:


    "Olga Dmitrievna sonrió con aprobación."


translate spanish epilogue_mi_fdafbf2c:


    mt "Bueno, ¡parece que eres capaz de hacerlo cuando realmente lo quieres!"


translate spanish epilogue_mi_753f8975:


    "Pues claro, resulta que la voluntad es la cosa más importante."


translate spanish epilogue_mi_f8e1553e:


    mt "Y ahora... el encuentro con Slavya."


translate spanish epilogue_mi_25e7e67f:


    ro "¿Un descanso tal vez...?"


translate spanish epilogue_mi_5b25a91f:


    "Suplicó Router."


translate spanish epilogue_mi_59395cbc:


    mt "¡No estamos aquí para unas vacaciones! ¡Venga, a tu cámara!"


translate spanish epilogue_mi_0378d59e:


    "Bajó su cabeza y se arrastró hasta su lugar."


translate spanish epilogue_mi_36a7ec5f:


    mt "¡Acción!"


translate spanish epilogue_mi_c787cab8:


    "Me quedé mirando las puertas del campamento."


translate spanish epilogue_mi_ef82b877:


    "Sostuve mi mirada en las estatuas de los pioneros y di unos cuantos pasos adelante."


translate spanish epilogue_mi_0b401918:


    "Sasha surgió de detrás de las puertas."


translate spanish epilogue_mi_078abde8:


    sa "¡Hola, Semyon-kun! ¡Mi nombre es Slavya! ¡Yoroshiku-ne!"


translate spanish epilogue_mi_bf59de13:


    "Un estupefacto silencio descendió sobre todo el equipo."


translate spanish epilogue_mi_d7a685d0:


    "Pareció que incluso los saltamontes, repentinamente, detuvieron su canción y alzaron su mirada a Sasha con fascinación."


translate spanish epilogue_mi_916c7ee7:


    mt "Sólo tengo una pregunta para ti."


translate spanish epilogue_mi_209e0706:


    "La terrible calmada voz de Olga Dmitrievna nos alcanzó desde detrás de mí."


translate spanish epilogue_mi_1c639c9a:


    mt "¿Te has leído siquiera el guión?"


translate spanish epilogue_mi_dfa4c8a5:


    sa "¡Claro que lo hice, pero es que era tan aburrido...!"


translate spanish epilogue_mi_b521b673:


    "No tenía sentido ponerse a llorar, sudar, volverse histérico... no podríamos encontrar ninguna otra actriz por aquí."


translate spanish epilogue_mi_15068e6a:


    mt "Solamente tenemos tres semanas en este campamento y tú..."


translate spanish epilogue_mi_3895407a:


    "Ella se sentó en su silla de director y comenzó a abanicarse con su sombrero de panamá."


translate spanish epilogue_mi_e2203c1f:


    mt "Muchachos, ¡no entendéis que no lo conseguiremos a tiempo si continuamos dando rodeos así! ¡Todavía tenemos mucho material que rodar! ¿Podríais ser capaces al menos de ser un poco más serios? Es como si fuera la única que se preocupa..."


translate spanish epilogue_mi_1b8271ef:


    "Un palo ciertamente no funcionará con Sasha, así que tal vez deberíamos intentarlo con una zanahoría en su lugar."


translate spanish epilogue_mi_7f682ff5:


    sa "Lo siento...{w} Lo intentaré de nuevo."


translate spanish epilogue_mi_2f8fa8d8:


    mt "Será mejor."


translate spanish epilogue_mi_b09d6e1c:


    "Me resultaba difícil de imaginar al protagonista inspeccionando un entorno desconocido, mientras millones de teorías vuelan a través de su mente. Él está trastornado, no sabe muy bien qué pensar, su cerebro está a punto de explotar, su consciencia comienza a errar y... ahí va... una muchacha, una pionera promedio, aparece y ella no tiene ni la más mínima idea de lo que le ha sucedido a él."


translate spanish epilogue_mi_6e8305c4:


    "Ella tan sólo saluda al protagonista y lo envía directo a ver a la líder del campamento..."


translate spanish epilogue_mi_04183b60:


    "Avanzo unos cuantos pasos y Sasha aparece de detrás de las puertas."


translate spanish epilogue_mi_2d51c8a5:


    sa "¿Debes ser nuevo aquí?"


translate spanish epilogue_mi_e46c798b:


    my "..."


translate spanish epilogue_mi_c4155f1c:


    "Capté la vista de las lentes de la cámara apuntándome desde el rabillo de mi ojo e intenté, con esfuerzos, hacer una verdadera cara de sorpresa."


translate spanish epilogue_mi_74e29a93:


    sa "Si es así, tendrías que ir a ver a nuestra líder del campamento..."


translate spanish epilogue_mi_794febdd:


    mt "¡Ves, puedes hacerlo! ¡Si realmente lo quieres!"


translate spanish epilogue_mi_2424e856:


    "Olga Dmitrievna se limpió la sudor de su frente."


translate spanish epilogue_mi_01ddb43c:


    mt "¡Me voy a morir antes de tiempo con todas esas cursilerías vuestras!{w} Y aun tenemos que hacer la edición... Jah..."


translate spanish epilogue_mi_9b67587c:


    "Comprobó su reloj."


translate spanish epilogue_mi_76501b5e:


    mt "Vale, digamos que hicimos el día."


translate spanish epilogue_mi_8e0afbc8:


    "Difícilmente me llamaría un adicto al trabajo, pero incluso yo sentí que era demasiado pronto para parar, puesto que ya íbamos mal de tiempo respecto con lo previsto."


translate spanish epilogue_mi_31a1857e:


    my "No crees que..."


translate spanish epilogue_mi_0826aeda:


    mt "¡Lo creo! ¡Pero me siento descompuesta!"


translate spanish epilogue_mi_50a46ecb:


    my "Eh, como quieras..."


translate spanish epilogue_mi_db0f23eb:


    "Me encogí de hombros y me dirigí por el campamento."


translate spanish epilogue_mi_04fdda5e:


    "El ocaso se acercaba..."


translate spanish epilogue_mi_a20cefa7_1:


    "..."


translate spanish epilogue_mi_5cbd3ea0:


    "Esta noche estaba cenando con Masha, Sasha y Alisa."


translate spanish epilogue_mi_988b5808:


    "Había un gran cuenco lleno de patatas fritas troceadas con setas justo frente a mí."


translate spanish epilogue_mi_13434154:


    "Una de las ventajas de verano... vegetales frescos de la huerta."


translate spanish epilogue_mi_8aa3e2bb:


    ma "Mostraste mucho esfuerzo durante el rodaje."


translate spanish epilogue_mi_ba41249e:


    my "Oh, ¿parece que prefieres fideos instantáneos antes que esto?"


translate spanish epilogue_mi_36b02c46:


    dv "Pero qué decís, ¡Sasha es una gran cocinera!"


translate spanish epilogue_mi_7a3a0d77:


    "Murmuró Alisa."


translate spanish epilogue_mi_49910a9c:


    ma "¿No te dijeron nunca que es de mala educación hablar con la boca llena?"


translate spanish epilogue_mi_ccfd297e:


    dv "Y tú siempre te estás quejando..."


translate spanish epilogue_mi_1c59bfd0:


    sa "Yo... Yo..."


translate spanish epilogue_mi_508e3ad2:


    "Sasha estaba a punto de romper a llorar."


translate spanish epilogue_mi_2b2f274d:


    ma "Oh Señor...{w} ¡Venga ya, calmarse! ¡Todo es delicioso!"


translate spanish epilogue_mi_1b24a1e0:


    sa "¡Nyaa! Masha-nyan, quieres que haga algo..."


translate spanish epilogue_mi_2d21996b:


    "Masha le hizo una mirada tan sucia que fue obvio incluso para Sasha que no debería insistir."


translate spanish epilogue_mi_df502e69:


    ma "Pero de todas formas, dime sinceramente..."


translate spanish epilogue_mi_d9f1b919:


    my "¡No lo haré!"


translate spanish epilogue_mi_4718ae9f:


    ma "¡Pero si no sabes qué te voy a pedir!"


translate spanish epilogue_mi_2596d838:


    my "Lo sé..."


translate spanish epilogue_mi_6a9e9866:


    ma "Bien, ¿y qué va a ser?"


translate spanish epilogue_mi_7eb71829:


    "Me miró fijamente con una mirada triunfante."


translate spanish epilogue_mi_11c965cb:


    my "Por qué se me ocurrió semejante basura de guión..."


translate spanish epilogue_mi_6ceed182:


    ma "¡Maldita sea!"


translate spanish epilogue_mi_5fcef207:


    "Alisa y Sasha se rieron."


translate spanish epilogue_mi_a13d417a:


    ma "Oh, ¡ya veo que os parece bien a las dos!"


translate spanish epilogue_mi_c5a0a029:


    dv "¿Qué pasa? Es verano, estamos rodeados por la naturaleza, hay una playa aquí, ¡todo parece genial!"


translate spanish epilogue_mi_bb713d3d:


    ma "También podrías visitar Turquía, ¿por qué viniste aquí?"


translate spanish epilogue_mi_2dd77e41:


    "Alisa miró a Masha inexpresivamente y comenzó a pinchar en su plato."


translate spanish epilogue_mi_7d88023f:


    ma "¿Y bien?"


translate spanish epilogue_mi_d0742f96:


    "Ella apuntó su tenedor hacia mí."


translate spanish epilogue_mi_4ac27a40:


    my "Teme no al tenedor sino a la cuchara, ¡un sólo golpe y tu cráneo se partirá!"


translate spanish epilogue_mi_2e15dd85:


    ma "Ahora decidme... ¿acaso él es normal?"


translate spanish epilogue_mi_1efc89c2:


    sa "¡Semyon-kun es kawaii!"


translate spanish epilogue_mi_a16d8757:


    "Sasha se aferró a mi brazo y asintió vigorosamente."


translate spanish epilogue_mi_3a95f27d:


    dv "¿Y qué tiene de malo? Nos ha dado unas vacaciones gratis."


translate spanish epilogue_mi_1f119ac6:


    ma "No entiendo qué estoy haciendo yo aquí."


translate spanish epilogue_mi_0c881678:


    my "Bueno, nadie te está reteniendo aquí."


translate spanish epilogue_mi_7b75257b:


    "Dije fríamente."


translate spanish epilogue_mi_0bbe7c29:


    ma "Oh, si solamente lo que quisiera importara..."


translate spanish epilogue_mi_3d475d4b:


    my "Y tú, ¿qué has estado haciendo todo este tiempo?{w} Solamente estás agobiando a los demás y todavía queda mucho tiempo para tu aparición de acuerdo al guión. Debes prepararte para tu papel, ¿eh?"


translate spanish epilogue_mi_e5e97d5d:


    "Le interrumpí."


translate spanish epilogue_mi_ad5371b1:


    ma "Oh, ¡quizás me estoy preparando!"


translate spanish epilogue_mi_a1263512:


    dv "¡Doble rasero!"


translate spanish epilogue_mi_6c54bdc2:


    ma "¡Triple, joder!"


translate spanish epilogue_mi_ea3c71bd:


    my "Y bien, ¿qué estas haciendo en verdad?"


translate spanish epilogue_mi_338067db:


    ma "Bueno, ¿qué esperarías que hiciera yo?"


translate spanish epilogue_mi_84d72c1c:


    my "¿Yo? Nada. Curiosidad."


translate spanish epilogue_mi_45e76bef:


    ma "Leer libros..."


translate spanish epilogue_mi_ec13eb7f:


    "Respondió tímidamente."


translate spanish epilogue_mi_9a9c5850:


    my "Y todo libro para perfeccionar la actuación..."


translate spanish epilogue_mi_07729729:


    ma "Para ya, ¿vale? Estamos sentados aquí, dejados de la mano de Dios... no hay internet, no hay señal de teléfono. ¡No hay nada de nada!{w} Si no me hubiera traído mi repelente de mosquitos conmigo..."


translate spanish epilogue_mi_71495458:


    sa "¡Yo lo traje, nya!"


translate spanish epilogue_mi_8538763c:


    "Masha le hizo tal mirada que Sasha de repente empezó a encogerse más de lo habitual."


translate spanish epilogue_mi_b9cb210e:


    dv "No te entiendo... ¿Qué sentido tiene estar quejándose todo el rato...? Podrías haberte quedado en casa..."


translate spanish epilogue_mi_010cbc3b:


    ma "Ah, ¡¿te pregunté a ti?!"


translate spanish epilogue_mi_caa0783d:


    "Masha se levantó de golpe y se dirigió a la salida."


translate spanish epilogue_mi_5354f480:


    sa "Masha-nyan está enfadada..."


translate spanish epilogue_mi_47d1149e:


    "Dijo Sasha con un tono consternado."


translate spanish epilogue_mi_a21ed268:


    dv "Eh, si se «enfada» sólo ahora, pues espera a ver cómo se pone hacia el final del rodaje..."


translate spanish epilogue_mi_f23678e8:


    my "La matanza con motosierra de la pionera con una «Druzhba», eh..."


translate spanish epilogue_mi_bced139f:


    "Tenía mucha curiosidad, realmente no podía comprender el porqué Masha siempre se pone como una loca."


translate spanish epilogue_mi_587f2ffc:


    "Está claro que desde buen comienzo no tenía ningún entusiasmo real, ni por el guión, ni por la sola idea de rodar una película en un campamento de pioneros dejado de la mano de Dios."


translate spanish epilogue_mi_c1ebda1e:


    "Pero aun así, ella no era así de hostil."


translate spanish epilogue_mi_0912ae25:


    "¿Quizá sea el calor el culpable?{w} ¿O los saltamontes?"


translate spanish epilogue_mi_c7b9f0f9:


    "¡Claro, eso es! ¡Los saltamontes!{w} ¡Nos están dando la tabarra en vez de sueño!"


translate spanish epilogue_mi_11922441:


    "Desearía que tuviéramos tan sólo un botón para liberar algún tipo de gas venenoso... y presto..."


translate spanish epilogue_mi_bb476699:


    sa "¿Qué estás pensando?"


translate spanish epilogue_mi_5fa3b6bf:


    my "¡Sobre los saltamontes!"


translate spanish epilogue_mi_0c3815a2:


    dv "¿Qué hay de malo con ellos?"


translate spanish epilogue_mi_45f1bddf:


    my "También podrías preguntar «¿qué hay de bueno con ellos?»."


translate spanish epilogue_mi_4c2e02b2:


    sa "Saltamontes... ellos no lo son nya."


translate spanish epilogue_mi_5b4d3391:


    my "¿Por qué?"


translate spanish epilogue_mi_5ae0bd3f:


    sa "Bueno, son muy... terribles..."


translate spanish epilogue_mi_7ea55766:


    my "¿No te gustan en general los insectos?"


translate spanish epilogue_mi_5c8ae25c:


    sa "¡Nop!"


translate spanish epilogue_mi_a9e9f873:


    my "Yo tampoco..."


translate spanish epilogue_mi_eb6b2adb:


    "Un pesado silencio se hizo."


translate spanish epilogue_mi_33c07f3e:


    "No importa qué se le ocurre por la cabeza a Masha, al menos siempre puede mantener viva la conversación.{w} Incluso si sólo es de saltamontes."


translate spanish epilogue_mi_b69d5469:


    "Me la imaginé hablando sobre su odio contra estas crituras infernales, las cuales no sólo osaban plagar este mundo ya imperfecto, sino que también cantaban a lo largo de toda la noche, interrumpiendo a su vez tu tranquilo descanso y tu apacible alma..."


translate spanish epilogue_mi_5f4cc6c3:


    "Olga Dmitrievna se aproximó a nuestra mesa."


translate spanish epilogue_mi_c7675ebb:


    mt "¿Preparados para el rodaje nocturno?"


translate spanish epilogue_mi_6ba4a203:


    my "¿Rodaje nocturno? No hemos ni completado las primeras escenas aun...{w} Y también mencionaste que por hoy ya habíamos terminado..."


translate spanish epilogue_mi_0dc170ff:


    mt "¡Tenemos que aprovechar al máximo el tiempo! ¡O si no, no lo conseguiremos!"


translate spanish epilogue_mi_a996136e:


    "No podía decir que estuviera completamente muerto de cansancio y, por otro lado, tenía que mostrar al menos un destello de compromiso para el éxito de mi propio proyecto."


translate spanish epilogue_mi_78cf53f5:


    my "Muy bien. ¿Qué vamos a rodar?"


translate spanish epilogue_mi_2a270e1d:


    mt "La escena nocturna con Slavya en el bosque."


translate spanish epilogue_mi_d88ec507:


    sa "¿Nani-nani?"


translate spanish epilogue_mi_f18d55d7:


    "Sasha escuchó que hablábamos de su personaje y saltó a incorporarse."


translate spanish epilogue_mi_2fe8b72b:


    mt "Pero por favooooooor..."


translate spanish epilogue_mi_d489d61c:


    "Comenzó Olga Dmitrievna con voz fatigada."


translate spanish epilogue_mi_41db4dd8:


    mt "¡Ateneos al guión esta vez!"


translate spanish epilogue_mi_1897c7ff:


    sa "¡Wakarimashita!"


translate spanish epilogue_mi_ed85db76:


    mt "Y para ya con eso..."


translate spanish epilogue_mi_ba5e3d3e:


    "Apretó sus dientes, se dio la vuelta y nos dejó."


translate spanish epilogue_mi_f0d9fc84:


    dv "¿De qué va esa escena?"


translate spanish epilogue_mi_a8c746ea:


    my "Pero, ¿te has leído acaso el guión?"


translate spanish epilogue_mi_7ac752e2:


    dv "¡Por supuesto que no!"


translate spanish epilogue_mi_1ac9e5f7:


    "Dijo Alisa orgullosa."


translate spanish epilogue_mi_cea1d5da:


    dv "Me basta con leerme solamente las escenas en que aparezco."


translate spanish epilogue_mi_31409fb0:


    "No iba a incordiarla con conversaciones inútiles sobre la necesidad de captar la idea general para poder comprender plenamente un personaje."


translate spanish epilogue_mi_4c582a03:


    my "Estoy contemplando el baile de Slavya desde un lado del lago en los bosques..."


translate spanish epilogue_mi_5f43ddfa:


    sa "¡Nya-nya! ¡Qué romántico!"


translate spanish epilogue_mi_537ab6db:


    my "¡...y ella se desnuda de mientras!"


translate spanish epilogue_mi_65c466a7:


    sa "¿Nya?"


translate spanish epilogue_mi_03d57659:


    "Sasha me miró fijamente con una mirada de incomprensión."


translate spanish epilogue_mi_fd1e9efb:


    "Cayó en la cuenta del significado de mis palabras tras unos segundos."


translate spanish epilogue_mi_29afe1be:


    sa "De... ¿desnuda?"


translate spanish epilogue_mi_dea901ac:


    my "Relájate, no del todo."


translate spanish epilogue_mi_009c29dd:


    sa "Bueno, si es contigo..."


translate spanish epilogue_mi_ac04f5d1:


    "Se ruborizó."


translate spanish epilogue_mi_e51d66bf:


    "Claro, parece que esta muchacha no ha estado viendo mucho anime recientemente, pero tampoco es que sea muy inteligente de nacimiento."


translate spanish epilogue_mi_2e162410:


    "¿Por qué Olga Dmitrievna seleccionó el papel de los personajes de esta forma?"


translate spanish epilogue_mi_9290596e:


    "Si por mí fuera, sería mejor el papel de Slavya interpretado por Masha, y el papel de Miku para Sasha."


translate spanish epilogue_mi_52b336ff:


    "De todas formas, es el director quien decide, y Olga Dmitrievna es la única directora que tenemos."


translate spanish epilogue_mi_8974ad73:


    dv "Por casualidad, ¿habrá de {i}aquello{/i}?"


translate spanish epilogue_mi_2dcb646d:


    sa "¿Qué?"


translate spanish epilogue_mi_f0ac2d1e:


    "Sasha le hizo una mirada en blanco a Alisa."


translate spanish epilogue_mi_449f4ec4:


    my "No."


translate spanish epilogue_mi_24bd3c14:


    dv "Qué dices, ¿nada de nada?"


translate spanish epilogue_mi_604efd9d:


    my "No en esta escena."


translate spanish epilogue_mi_f0ac5acd:


    dv "Ah, vale, entonces {i}harás aquello{/i} más tarde."


translate spanish epilogue_mi_26c83201:


    "Alisa mostró una aguda sonrisa."


translate spanish epilogue_mi_c5d3868a:


    my "Si tanto te llama la curiosidad, ¡ya deberías de haberte leído el guión!"


translate spanish epilogue_mi_6e91e7e9:


    dv "Qué va, no soy tan curiosa."


translate spanish epilogue_mi_f445a737:


    "Alisa se levantó de su silla."


translate spanish epilogue_mi_e08e50aa:


    dv "Bueno, me voy."


translate spanish epilogue_mi_79ef6a3f:


    sa "Ejem, ¿de qué iba todo esto? ¿Qué es «eso»?"


translate spanish epilogue_mi_1e98cfca:


    my "No es nada.{w} Vete preparando... el sol casi se ha puesto ya."


translate spanish epilogue_mi_91e87806:


    sa "¡Nya-nya!"


translate spanish epilogue_mi_7cbc1a04:


    "Sasha se levantó y se fue corriendo a la salida."


translate spanish epilogue_mi_248dbf88:


    "Acabé con mi plato de patatas y solté un profundo suspiro."


translate spanish epilogue_mi_80428fb2:


    "Bueno, a este ritmo no lograremos rodar ni la mitad de la película."


translate spanish epilogue_mi_398ea31a:


    "Supongo que tan sólo podemos poner «continuará» en el final."


translate spanish epilogue_mi_25434a55:


    "Y convertirlo todo en una épica saga, creada por unos estudiantes de una escuela de cine y televisión local de tres al cuarto."


translate spanish epilogue_mi_b5b110eb:


    "De repente comprendí eso, en verdad, no me preocupaba más..."


translate spanish epilogue_mi_292fb47d:


    sh "¿Preparado?"


translate spanish epilogue_mi_95ff9942:


    "Me di la vuelta y capté la vista de Shurik."


translate spanish epilogue_mi_8b6b532c:


    my "No tengo nada que hacer...{w} ¿Crees que te puedes apañar con este rodaje nocturno?"


translate spanish epilogue_mi_02458859:


    sh "Oh... De alguna manera..."


translate spanish epilogue_mi_5141016c:


    my "Vale, pues adelante."


translate spanish epilogue_mi_a20cefa7_2:


    "..."


translate spanish epilogue_mi_d4c057a8:


    "Al fin se volvía más frío al atardecer.{w} Demasiado frío, diría."


translate spanish epilogue_mi_334bdb32:


    "Estaba sentado en un tocón e intentaba mantenerme caliente con mi delgada blusa, mientras los demás estaban alineando cámaras y luces."


translate spanish epilogue_mi_c38f964d:


    ma "De todas formas, ¿por qué debería estar ocupándome de estos asuntos? ¡No tengo nada que ver con esta escena!"


translate spanish epilogue_mi_8944594a:


    "Masha estaba poniendo una tela retro-reflectante de luz entre los árboles."


translate spanish epilogue_mi_373f8138:


    mt "¿Quién si no? ¡Siempre estamos escasos de mano de obra!"


translate spanish epilogue_mi_19ae045d:


    ma "Bueno, ¡podrías traer más gente para esto! Todo el mundo estaría feliz de poder participar en la producción de semejante obra maestra."


translate spanish epilogue_mi_b468297c:


    mt "¿Te estás quejando de algo?"


translate spanish epilogue_mi_f9ea9881:


    "Olga Dmitrievna le hizo una mirada amenazadora."


translate spanish epilogue_mi_73f6d6b1:


    ma "N... no del todo, está bien..."


translate spanish epilogue_mi_ee161131:


    "Nuestra directora era probablemente la única persona con la cual Masha dudaba llevarle la contraria."


translate spanish epilogue_mi_711dadab:


    "Me pregunto por qué..."


translate spanish epilogue_mi_a20cefa7_3:


    "..."


translate spanish epilogue_mi_4e70f7d1:


    "Shurik se sentó a mi lado."


translate spanish epilogue_mi_7cda594d:


    sh "¿Qué crees, hay oportunidades de rodarla en dos o tres tomas?"


translate spanish epilogue_mi_a45d1642:


    my "No tengo ni idea... No depende de mí..."


translate spanish epilogue_mi_61482050:


    "Asentí en dirección a Sasha. Estaba saltando arriba y abajo con energía por la zona del lago, calentándose para su papel."


translate spanish epilogue_mi_fbf62cb1:


    sh "Oh, ya veo..."


translate spanish epilogue_mi_9687a380:


    "Shurik vomitó un suspiro de condenado y se dirigió hacia su cámara."


translate spanish epilogue_mi_b1ce9623:


    mt "¡Muy bien! Es la hora."


translate spanish epilogue_mi_8b3f4a18:


    "Me escondí tras un árbol."


translate spanish epilogue_mi_00dabc53:


    mt "¡Cámara! ¡Acción!"


translate spanish epilogue_mi_84b9f42b:


    "Sasha caminó más allá de mí, casi bailando."


translate spanish epilogue_mi_f2d8c1c4:


    "No pude dejar de notar que dada toda la extrailuminación, combinada con la luz lunar, ella parecía estúpida."


translate spanish epilogue_mi_db64b3c4:


    "Pero eso es asunto del responsable de la cámara... probablemente sea lo mejor."


translate spanish epilogue_mi_686c7490:


    "Lentamente surgí de detrás del árbol y la seguí."


translate spanish epilogue_mi_a24c889e:


    "Sasha iba lentamente a lo largo de la orilla, casi como flotando por el suelo, deshaciéndose de su pañuelo y desabrochándose su camisa por el camino."


translate spanish epilogue_mi_dac31f5f:


    "De hecho, en realidad parecía asombroso."


translate spanish epilogue_mi_7dd610b2:


    "Apuesto que podría ser una gran actriz del cine mudo."


translate spanish epilogue_mi_efe25434:


    "Más bien pronto, Sasha desapareció en el bosque. Esperé unos instantes para distanciarme de la orilla y luego comencé a seguirla."


translate spanish epilogue_mi_d536fac8_1:


    mt "¡Cortad!"


translate spanish epilogue_mi_1b2c2d2e:


    "Sasha inmediatamente salió corriendo de detrás de los árboles."


translate spanish epilogue_mi_7e7cffad:


    sa "¿Y bien, y bien? ¿Qué tal?"


translate spanish epilogue_mi_78bad556:


    mt "¡Excelente! ¡Perfecto! Si tan sólo pudieras actuar siempre así... ¡en la primera toma! ¡Entonces lo tendríamos todo acabado a tiempo!"


translate spanish epilogue_mi_9ae232e3:


    my "Lo dices como si no fuéramos a hacerlo a tiempo."


translate spanish epilogue_mi_e56d1312:


    "Remarqué escépticamente."


translate spanish epilogue_mi_ef558f42:


    mt "Lo haremos, ¡apuesta que sí!"


translate spanish epilogue_mi_cfa206cd:


    ma "Ejem, ¿qué quieres decir? ¿Eso es todo?"


translate spanish epilogue_mi_fb33457e:


    "Masha de repente apareció junto a mí como si de la nada."


translate spanish epilogue_mi_72fd9d16:


    mt "¿Quieres algo?"


translate spanish epilogue_mi_d28ef18c:


    ma "¿Malgastamos como tres horas preparando sólo un minuto de metraje?"


translate spanish epilogue_mi_9e2ea357:


    mt "¡Es el cine! ¡Cine, ya sabes! ¡Arte! ¡Uno podría pasarse años para cazar una toma! ¡Uno podría pasarse rodando durante décadas para una película de cinco minutos!"


translate spanish epilogue_mi_e60cdb23:


    "Olga Dmitrievna tenía un aspecto particularmente gracioso, agitando sus brazos de esa forma."


translate spanish epilogue_mi_40ef1a37:


    ma "Claro..."


translate spanish epilogue_mi_11d5d9f7:


    "Murmuró Masha con voz agotada."


translate spanish epilogue_mi_ab8a7c84:


    ma "¿Eso es todo?"


translate spanish epilogue_mi_438a5ff3:


    mt "Claro, eso es todo por esta noche."


translate spanish epilogue_mi_2e22c834:


    ma "Pues entonces buenas noches, ¡a todos!"


translate spanish epilogue_mi_8227612c:


    "Ella tiró aquella pieza de retro-reflector de luz al suelo, y se dio prisa hacia el campamento."


translate spanish epilogue_mi_7b48d8d6:


    "Dije rápidamente mi adiós a todos, agarré un impreso del guion y salí corriendo tras ella."


translate spanish epilogue_mi_a20cefa7_4:


    "..."


translate spanish epilogue_mi_333411d8:


    my "¡Ey!"


translate spanish epilogue_mi_58a251f7:


    "Fue en la plaza donde la atrapé."


translate spanish epilogue_mi_17c3f085:


    ma "Oh, ¿y ahora qué quieres?"


translate spanish epilogue_mi_f72fd8e2:


    "Un aspecto de cansancio extremo se veía en el rostro de Masha."


translate spanish epilogue_mi_6e8fe88f:


    my "Quizá... ¿quizá quieras dar un pequeño paseo antes de irte a dormir?"


translate spanish epilogue_mi_cc3d4e3e:


    ma "¿Por qué querría?"


translate spanish epilogue_mi_4de002b5:


    "Me miraba aparentemente confundida."


translate spanish epilogue_mi_d67add20:


    my "Bueno, es sólo que...{w} ¡aun es muy pronto! Apuesto a que tampoco pensarías en irte a dormir a esta hora en la ciudad."


translate spanish epilogue_mi_accf297a:


    ma "En la ciudad...{w} Esto no es la ciudad, ¿sabes? Tres días aquí cuentan como uno."


translate spanish epilogue_mi_36f15bc5:


    my "Oh, ¡venga ya! ¡Todavía es pronto!"


translate spanish epilogue_mi_fcbee1a6:


    ma "Mmmm, está bien..."


translate spanish epilogue_mi_943eaf3a:


    "Me hizo una mirada alegre y sonrió."


translate spanish epilogue_mi_6d8953b3:


    ma "¡Pero no por mucho rato!"


translate spanish epilogue_mi_443f7100:


    my "¡Sí, señora!"


translate spanish epilogue_mi_21bb5d23:


    ma "¿Por qué cargas con eso todo el tiempo?"


translate spanish epilogue_mi_799227a4:


    "Ella indicaba al montón de papeles que sostenía en mis manos."


translate spanish epilogue_mi_58f54863:


    my "Bueno, nunca sabes qué puede ocurrirte..."


translate spanish epilogue_mi_a20cefa7_5:


    "..."


translate spanish epilogue_mi_6299873c:


    "En breve llegamos a la playa."


translate spanish epilogue_mi_ee06f3f9:


    ma "¡Que luna más preciosa!"


translate spanish epilogue_mi_41699bfb:


    "Masha comenzó a desnudarse y salió corriendo hacia el río."


translate spanish epilogue_mi_9ee4fa9d:


    "No sé qué era lo que esperaba, ya que ella tenía un bikini bajo su uniforme de pionera."


translate spanish epilogue_mi_ca443530:


    "Fascinado, la miré fijamente como sus piernas se zambullían en el agua, las cuales causaban ondas con salpicaduras a su alrededor."


translate spanish epilogue_mi_14ea44c3:


    my "¡Al fin, aun puedes divertirte!"


translate spanish epilogue_mi_91d35e95:


    "Le grité."


translate spanish epilogue_mi_e2efb894:


    "Me lanzó una mirada de ofendida, se acercó y se sentó junto a mí."


translate spanish epilogue_mi_2a3057e6:


    ma "Pues claro, es como si hubieras pensado recientemente que soy solamente una vieja bruja."


translate spanish epilogue_mi_afbc1bb1:


    my "Claro que no lo eres...{w} Pero aun así eres una terrible quejica."


translate spanish epilogue_mi_64383958:


    ma "¡Supongo que tengo mis motivos!"


translate spanish epilogue_mi_7d565efc:


    my "No tan claro... En particular no veo ninguno."


translate spanish epilogue_mi_c7de3691:


    "Masha se levantó y se apartó unos pocos pasos de mí."


translate spanish epilogue_mi_ca00ddd3:


    my "¡Oh, venga ya!"


translate spanish epilogue_mi_ec961d69:


    ma "¡No «venga ya» conmigo! Sólo es que... una no pueda ser como... como un..."


translate spanish epilogue_mi_7ddbab17:


    my "¿Como qué?"


translate spanish epilogue_mi_5dc90687:


    ma "Nunca lo ves..."


translate spanish epilogue_mi_67ff3dab:


    "Puso su cabeza abajo entre sus rodillas y añadió en voz baja:"


translate spanish epilogue_mi_b9140652:


    ma "Y tú nunca los entiendes..."


translate spanish epilogue_mi_b4a3d493:


    my "Claro, ese soy yo... sin corazón, tonto y soso."


translate spanish epilogue_mi_8961f09f:


    "Masha hizo una risa apenas audible."


translate spanish epilogue_mi_d58a855b:


    ma "Sep, exactamente..."


translate spanish epilogue_mi_25675418:


    ma "¿Nadamos?"


translate spanish epilogue_mi_18eae243:


    my "¡Hace frío dentro!"


translate spanish epilogue_mi_7e8feba7:


    ma "No llevará mucho rato."


translate spanish epilogue_mi_520e734c:


    my "Oh... vale."


translate spanish epilogue_mi_a20cefa7_6:


    "..."


translate spanish epilogue_mi_9bbf09a7:


    "El agua era sorprendentemente cálida, por lo que, desde luego, era imposible que nos fuéramos a congelar."


translate spanish epilogue_mi_cb0a184a:


    "Me senté en la toalla que alguien había dejado atrás y respiré profundamente el suave aire nocturno."


translate spanish epilogue_mi_38d4cee4:


    "Masha se tumbó frente a mí y descansaba su cabeza en mi regazo."


translate spanish epilogue_mi_404343d4:


    ma "¿Te molesto?"


translate spanish epilogue_mi_6829da13:


    my "N... no para nada..."


translate spanish epilogue_mi_ab837517:


    ma "Sólo me siento algo cansada."


translate spanish epilogue_mi_a5dd8896:


    "Me miraba directamente a mis ojos, pero aparté mi mirada."


translate spanish epilogue_mi_b362f783:


    ma "¿Por qué no pueden ser las cosas como eran antes?"


translate spanish epilogue_mi_d70ebca4:


    my "¿Cómo?"


translate spanish epilogue_mi_00d724da:


    ma "Venga ya..."


translate spanish epilogue_mi_02c9d340:


    my "Aquello fue una decisión compartida, lo sabes..."


translate spanish epilogue_mi_ea890fe1:


    ma "Pero no obstante..."


translate spanish epilogue_mi_e9145191:


    "Ella puso sus brazos rodeando mi cintura, y me sentí aun más incómodo."


translate spanish epilogue_mi_b97bb617:


    ma "¿Por qué vinimos aquí en primer lugar?"


translate spanish epilogue_mi_2baba03c:


    my "¡Para rodar una película!"


translate spanish epilogue_mi_bb9ef712:


    "Traté de desviar el tema con una broma."


translate spanish epilogue_mi_04598414:


    ma "Aun así si no fuera por ti, primero de todo, no habría ni tan siquiera ido a esa escuela...{w} Podríamos haber..."


translate spanish epilogue_mi_040d056a:


    "Suspiró."


translate spanish epilogue_mi_47224973:


    ma "Pero ahora..."


translate spanish epilogue_mi_e953effd:


    my "Aun así creo que remover el pasado en este contexto, no es muy buena idea."


translate spanish epilogue_mi_655cfe62:


    ma "Puedes pensarlo. En cuanto a mí, todavía creo que no lo deberíamos haber..."


translate spanish epilogue_mi_1f24965d:


    my "¡Pero tú fuiste la primera que lo quiso en primer lugar!"


translate spanish epilogue_mi_981566ae:


    ma "¡Nunca lo quise!"


translate spanish epilogue_mi_c2f0d527:


    "Dijo Masha con resentimiento."


translate spanish epilogue_mi_3788408c:


    my "¡Oh, claro! Entonces, ¡¿fui yo quien insistió?!"


translate spanish epilogue_mi_5686c565:


    ma "¡Sí, fuíste tú!"


translate spanish epilogue_mi_27ebdb73:


    "Si dio la vuelta, pero no se separó de mí."


translate spanish epilogue_mi_1221beae:


    my "Siempre eres así... No te gusta esto... No te gusta aquello... ¿Por qué no puedes adoptar una forma humilde de tomarte la vida? Echa un vistazo a tu alrededor, ¡es un verano espléndido! ¡Es una noche bonita! ¡Estamos rodando una destacable película! De hecho, todo es genial..."


translate spanish epilogue_mi_b4204e75:


    ma "¿De verdad te crees todo eso?"


translate spanish epilogue_mi_a5b0e9d3:


    "Su pregunta era, desde luego, una que merecía hacerse."


translate spanish epilogue_mi_bfa1d965:


    my "Lo creo... algo así. No siempre todo va a ser ideal...{w} ¿Pero qué tiene de malo ello?"


translate spanish epilogue_mi_538b1472:


    ma "Todo es malo..."


translate spanish epilogue_mi_1b164615:


    "Parecía estar preparada para romper a llorar."


translate spanish epilogue_mi_96380f1d:


    ma "Este estúpido campamento... Ese guión tuyo.{w} ¡Todo! ¡Todo es malo! ¡Y tú ya no eres un niño! Ya podrías haber iniciado una familia, tener un empleo..."


translate spanish epilogue_mi_56a282e0:


    my "¡Ey!{w} ¡Eso lo discutimos como miles de veces!"


translate spanish epilogue_mi_c7401555:


    ma "Claro... Y rompimos por culpa de eso exactamente."


translate spanish epilogue_mi_04774682:


    my "Esa fue tu iniciativa, recuérdalo. Precisamente a causa de tu actitud de «todo es malo»."


translate spanish epilogue_mi_0ab1e883:


    ma "¡Oh, pero tú estabas en plan «todo es genial» para superarlo!{w} Sólo los idiotas se regocijan por todo lo que les rodea."


translate spanish epilogue_mi_af9c4714:


    my "No me estoy regocijando... O, mejor dicho, ¡me regocijo de aquellas cosas que valen la pena regocijarse de ellas! Me gusta mi trabajo. Disfruto con las películas."


translate spanish epilogue_mi_9c47bb7a:


    "Lo hago, ¿verdad?"


translate spanish epilogue_mi_bbfdf3e3:


    ma "¿Significa eso que no disfrutas conmigo?"


translate spanish epilogue_mi_39cd6f4b:


    "Se levantó y se apoyó contra mi pecho."


translate spanish epilogue_mi_995dd1da:


    my "Ejem, tal vez no deberíamos..."


translate spanish epilogue_mi_4b4f76bd:


    ma "¿Qué pasa? ¿No te apetece?"


translate spanish epilogue_mi_3f27c2c0:


    "No podía decidirme en cómo debería contestarle."


translate spanish epilogue_mi_dd178882:


    "Por una parte, no quería reabrir viejas heridas, pero por otra... Comprendí claramente que no estaba en una posición como para rechazarla."


translate spanish epilogue_mi_113ceef8:


    "Y ahora mismo, no me preocupaba si alguien que no estuviera durmiendo pudiera vernos..."


translate spanish epilogue_mi_32d9bac4:


    my "¿Pero por qué? ¿Y luego qué?"


translate spanish epilogue_mi_64fd49e3:


    ma "Qué más da..."


translate spanish epilogue_mi_9eb4aadf:


    my "Bueno... En ese caso..."


translate spanish epilogue_mi_a20cefa7_7:


    "..."


translate spanish epilogue_mi_f1dcbafd:


    "La resplandeciente luz lunar se reflejaba en las gotitas de agua en su desnuda piel, la pegajosa arena, la fresca de la noche veraniega y el calor de su cuerpo, la respiración agitada y el jadeo de placeres... todo transcurrió en un breve momento."


translate spanish epilogue_mi_a20cefa7_8:


    "..."


translate spanish epilogue_mi_71390bb9:


    my "Supongo que ya es la hora de dormir.{w} Será un día duro mañana... definitivamente, Olga Dmitrievna nos exigirá mucho."


translate spanish epilogue_mi_d5b285ba:


    "Estaba abrochándome lentamente mi camisa mientras observaba a Masha vestirse."


translate spanish epilogue_mi_d1464071:


    ma "No quiero dormir sola..."


translate spanish epilogue_mi_a68bf534:


    "Murmuró en voz baja."


translate spanish epilogue_mi_337290ce:


    my "Qué, ¿asustada de los saltamontes?"


translate spanish epilogue_mi_4562c02a:


    ma "No son los saltamontes, ¡son los grillos! Y no estoy asustada de ellos..."


translate spanish epilogue_mi_42869b08:


    "Apartó su mirada y se ruborizó un poco."


translate spanish epilogue_mi_7ee55b20:


    ma "Es sólo que... bueno... no estoy acostumbrada."


translate spanish epilogue_mi_f7e0940e:


    "Podrías haberte acostumbrado a ello, después de todo este tiempo."


translate spanish epilogue_mi_b1cabb3f:


    "De repente pensé que no había nada de malo en esta idea.{w} Especialmente dado todo lo que había sucedido..."


translate spanish epilogue_mi_e9e6e7f9:


    my "Vale..."


translate spanish epilogue_mi_97c79ba2:


    ma "¿Pero dónde?"


translate spanish epilogue_mi_19aed9c9:


    "Masha me hizo una mirada inquisitiva."


translate spanish epilogue_mi_c405517a:


    my "Hay un par de cabañas no ocupadas, reservadas para escenas posteriores.{w} Bueno, podríamos usarlas de mientras, supongo."


translate spanish epilogue_mi_7dc61d1e:


    ma "Vamos."


translate spanish epilogue_mi_ab3f1fe7:


    "Sonrió, se levantó, me agarró por el brazo y, lentamente, nos dirigimos hacia la plaza..."


translate spanish epilogue_mi_a20cefa7_9:


    "..."


translate spanish epilogue_mi_6c0b31c2:


    "Un irresistible rayo de luz matutino cegador golpeó en mis ojos."


translate spanish epilogue_mi_cbb4c74c:


    "No era capaz de estirarme... Masha estaba durmiendo en mi brazo izquierdo."


translate spanish epilogue_mi_050d92e1:


    "Apenas me las apañé para quitar mi extremidad de debajo su cabeza y lo masajeé durante un rato, tratando de recuperar la circulación de la sangre."


translate spanish epilogue_mi_d84db049:


    "Froté mi incipiente barba, alcancé mis pantalones cortos buscando mis cigarrillos."


translate spanish epilogue_mi_37be2fed:


    "Quizás no deberíamos haber llegado ayer tan lejos, después de todo."


translate spanish epilogue_mi_15aaeab9:


    "Ahora seguramente no me dejaría solo."


translate spanish epilogue_mi_583a04fd:


    "Bueno, probablemente no...{w} Eso no está en su naturaleza."


translate spanish epilogue_mi_412be39c:


    "Pero un mal regusto aun permanecerá."


translate spanish epilogue_mi_66fafbd5:


    "Y ahora tiene un argumento para reprochármelo, si es que le doy la oportunidad de ello."


translate spanish epilogue_mi_7502f7b0:


    "Aun así {i}otro{/i} argumento... como si estuviera corta de ellos."


translate spanish epilogue_mi_d81d21a1:


    "Incapaz de encontrar los cigarrillos en mis pantalones cortos, pensé que debí haberlos dejado atrás en mi cabaña, por lo que me vestí torpemente y salí fuera."


translate spanish epilogue_mi_a46f5080:


    "Oh Señor, al menos haz que llueva... es imposible trabajar en semejante horno."


translate spanish epilogue_mi_ee1382fb:


    "Y así es desde el día a la noche, y de la noche al día..."


translate spanish epilogue_mi_8aef407b:


    "Frotándome los ojos, me arrastré por la senda y no me percaté de que alguien venía justo corriendo hasta que lo tuve frente a mí."


translate spanish epilogue_mi_43c329b2:


    "Era Router."


translate spanish epilogue_mi_14d3fbf7:


    ro "¡Hola Semyon!"


translate spanish epilogue_mi_c54301b0:


    my "¡Qué tal!"


translate spanish epilogue_mi_c942874a:


    "Le grité tras él."


translate spanish epilogue_mi_95f95256:


    "Finalmente llegué a la cabaña, abrí la puerta y entré."


translate spanish epilogue_mi_9fc335aa:


    "Olga Dmitrievna estaba dentro.{w} Me atravesó con su mirada."


translate spanish epilogue_mi_d8b1d698:


    mt "¿Querrías explicarte?"


translate spanish epilogue_mi_cabd0ae8:


    my "¿Qué ocurre?"


translate spanish epilogue_mi_cd667d28:


    mt "¡¿Dónde te has pasado toda la noche?!"


translate spanish epilogue_mi_a30d42e4:


    "Tenía un aspecto más que amenazador."


translate spanish epilogue_mi_71fcb370:


    my "Estaba con Masha... ¿Y cuál es el problema?"


translate spanish epilogue_mi_2e8f7dad:


    "Después de todo, todo el mundo ya lo sabe, así que no tiene ningún sentido estar escondiéndolo."


translate spanish epilogue_mi_3d8b8a51:


    mt "¿Con quién?"


translate spanish epilogue_mi_af66aeeb:


    "Olga Dmitrievna me hizo una mirada de sorpresa."


translate spanish epilogue_mi_3b56ad7d:


    my "Con Masha."


translate spanish epilogue_mi_d18d27a7:


    "Repetí con tranquilidad."


translate spanish epilogue_mi_7fb436a3:


    mt "No hay ninguna Masha aquí."


translate spanish epilogue_mi_a55c62b2:


    my "Ahora es mi turno para sorprenderme..."


translate spanish epilogue_mi_c04c6271:


    mt "Semyon, ¡tu comportamiento es impropio de un verdadero pionero!"


translate spanish epilogue_mi_22f7322a:


    my "Ah, ¿que ya estamos rodando? ¿Pero dónde están las cámaras?"


translate spanish epilogue_mi_fbd62a8b:


    "Sonreí."


translate spanish epilogue_mi_105fc865:


    mt "¡Semyon!"


translate spanish epilogue_mi_2151a69f:


    my "Oh, venga ya..."


translate spanish epilogue_mi_fe1c1f1f:


    "Me dirigí a mi cama y abrí el cajón de la mesita."


translate spanish epilogue_mi_47e0b7e7:


    "Sin embargo, mi mochila no estaba allí."


translate spanish epilogue_mi_3321c1b4:


    my "Errr... ¿Has visto mi mochila?"


translate spanish epilogue_mi_5c5b691a:


    mt "¿Una mochila?"


translate spanish epilogue_mi_efd76d15:


    "Me miraba sorprendida."


translate spanish epilogue_mi_67d1a8d9:


    my "Claro. Estaba aquí."


translate spanish epilogue_mi_e6ca34e7:


    mt "Pero llegaste aquí sin equipaje..."


translate spanish epilogue_mi_b9e79bf2:


    my "¿Qué?"


translate spanish epilogue_mi_ff99c828:


    "Hice una sonrisa tonta."


translate spanish epilogue_mi_74a4c096:


    mt "¡Y todavía no he terminado!"


translate spanish epilogue_mi_221aaa4e:


    my "Olga Dmitrievna, entiendo que estás de buen humor esta mañana y que tienes buena disposición para bromear, pero..."


translate spanish epilogue_mi_f8da58c6:


    mt "¡¿Cómo puedo sentirme bien cuando un pionero, un futuro miembro del Komsomol, se comporta así?!"


translate spanish epilogue_mi_5a64d54b:


    "¿Un pionero? ¿Un miembro del Komsomol? Algo está yendo definitivamente mal."


translate spanish epilogue_mi_118c25a9:


    my "Olga Dmitrievna, ¿has dormido bien? ¿Te encuentras bien?"


translate spanish epilogue_mi_06543492:


    mt "¡¿Cómo te atreves a hablarme de ese modo?!"


translate spanish epilogue_mi_6a4d0b42:


    my "Ejem, ¿qué problema tienes?"


translate spanish epilogue_mi_eae86bdc:


    mt "Resumiendo, ¡tienes que estar en la formación en media hora! En cuanto a mí, todavía tengo que encontrar a Miku."


translate spanish epilogue_mi_ad54eb5e:


    "¿Miku?{w} Parece que nos quieran gastar una broma."


translate spanish epilogue_mi_443f7100_1:


    my "¡A sus órdenes, mi Señora!"


translate spanish epilogue_mi_987f165b:


    "La saludé y marché hacia fuera de la cabaña."


translate spanish epilogue_mi_287b8014:


    "Tengo que ir hasta Masha y contárselo... compartir unas risas con ella."


translate spanish epilogue_mi_bbbfbbde:


    "Empujé vigorosamente la puerta de la cabaña para abrirla y corrí hacia dentro."


translate spanish epilogue_mi_b191b3f3:


    my "Masha, no te vas a creer lo que ellos han..."


translate spanish epilogue_mi_7225eb18:


    "Masha estaba sentada en un rincón de una cama con su rostro completamente pálido, rodeando con sus brazos las piernas."


translate spanish epilogue_mi_850753d4:


    my "¿Qué sucedió?"


translate spanish epilogue_mi_ab6c8876:


    "Salté hasta su lado."


translate spanish epilogue_mi_36e80422:


    ma "Lena entró..."


translate spanish epilogue_mi_8143f26a:


    my "¿Lena?{w} Pero si no vendrá hasta dentro de tres días más..."


translate spanish epilogue_mi_a1cd396c:


    ma "No era nuestra Lena... {i}Ésta{/i}...{w} Era algo que se le parecía..."


translate spanish epilogue_mi_6e1e7593:


    "Masha estaba temblando, y tenía una expresión de terror en su rostro."


translate spanish epilogue_mi_c0db5536:


    my "¡Tranquilízate! ¡Tómatelo con calma!"


translate spanish epilogue_mi_adfebabf:


    ma "Estaba hablando sobre algún tipo de formación, limpiar los suelos, sobre la biblioteca, sobre el club de música..."


translate spanish epilogue_mi_4342489d:


    "Su respiración se volvió agitada."


translate spanish epilogue_mi_e50ade73:


    ma "Y... y...{w} ¡no era una broma! ¡Estoy segura de ello!"


translate spanish epilogue_mi_eab4b893:


    "Inmediatamente recordé la reciente conversación con Olga Dmitrievna."


translate spanish epilogue_mi_8877ce04:


    my "Vale, ¡cálmate! ¡Todavía no entendí nada!"


translate spanish epilogue_mi_dd83c236:


    ma "No era nuestra Lena... Era...{w} ¡la Lena de tu guión!"


translate spanish epilogue_mi_84cf572d:


    "Alzó sus ojos llenos de lágrimas hacia mí y comprendí en seguida que esto no era una broma, mi valentía se desvaneció."


translate spanish epilogue_mi_a944f157:


    my "¡Eso es ridículo! No puede ser..."


translate spanish epilogue_mi_ccbb0e0e:


    "Aunque al recordar la conversación con la directora..."


translate spanish epilogue_mi_758f8c33:


    ma "¡No! ¡No! ¡Estoy segura!"


translate spanish epilogue_mi_dfc67743:


    "Se me abalanzó sobre mis brazos."


translate spanish epilogue_mi_ba41a0fe:


    "Traté de tranquilizarla."


translate spanish epilogue_mi_2ea136fe:


    "Tenemos que pensar con lógica."


translate spanish epilogue_mi_020471bd:


    my "¡Tenemos que pensar con lógica! Solamente están tratando de gastarnos una broma, ¡eso es todo! Justo estuve hablando con Olga Dmitrievna y ella..."


translate spanish epilogue_mi_9465eed8:


    "¿Y ella qué? ¿Que también me pareció diferente que de costumbre?"


translate spanish epilogue_mi_f5c1c55b:


    "Qué va, no le puedo decir eso a Masha..."


translate spanish epilogue_mi_6d353af2:


    my "Al final, se reíran y lo dejarán estar."


translate spanish epilogue_mi_fd57f767:


    ma "No, algo ha sucedido... algo horrible...{w} Estoy segura de ello..."


translate spanish epilogue_mi_8974dd9f:


    "Masha estaba sollozando en mis brazos."


translate spanish epilogue_mi_40789c4e:


    my "Vale, acabemos con esto. Hablemos con alguien.{w} Por ejemplo, con Router... seguramente él no sea parte importante de esta comedia."


translate spanish epilogue_mi_95c191c3:


    "A decir verdad, llegados a este punto estaba interesado en hallar qué tipo de broma era ésta, una que puede arrastrar a la histeria a Masha, quien siempre está relajada y preparada para todo."


translate spanish epilogue_mi_40ef1a37_1:


    ma "De acuerdo..."


translate spanish epilogue_mi_78e86177:


    "Se limpió las lágrimas y se levantó de la cama."


translate spanish epilogue_mi_40e78541:


    my "¿Más tranquila ahora?"


translate spanish epilogue_mi_0d0c986a:


    ma "Lo intento..."


translate spanish epilogue_mi_f9eeb73d:


    "Masha sonrió ligeramente."


translate spanish epilogue_mi_cfd5f29b:


    ma "Vayámonos..."


translate spanish epilogue_mi_279dbfa1:


    "Salimos de la cabaña."


translate spanish epilogue_mi_04e16600:


    ma "¿Adónde?"


translate spanish epilogue_mi_9164b34c:


    my "Olga Dmitrievna estaba hablando sobre algún tipo de formación. Vayamos a la plaza... lo más probable es que estén allá."


translate spanish epilogue_mi_499411c9:


    "Masha me agarró de la mano, pero la dejó ir al momento."


translate spanish epilogue_mi_00660cbc:


    "Preferí no mirar en su dirección porque ya sabía qué expresión tenía en su rostro, y tan sólo me dirigí hacia la plaza."


translate spanish epilogue_mi_a20cefa7_10:


    "..."


translate spanish epilogue_mi_20361ed5:


    "La formación (si ése es el término correcto para referirse a la gente amontonada alrededor del monumento) estaba bastante abarrotada."


translate spanish epilogue_mi_eb1f0748:


    "No reconocía ni la mitad de la gente que se reunía aquí y, ese hecho, era extremadamente raro de por sí."


translate spanish epilogue_mi_e02d4ff6:


    "Olga Dmitrievna trató de alinearlos."


translate spanish epilogue_mi_c44b27a4:


    "Preferí no detenerme y fui hacia ella."


translate spanish epilogue_mi_1ddcb8c6:


    my "Olga Dmitrievna, sabes..."


translate spanish epilogue_mi_9c242b70:


    mt "¡Vaya! ¡Y Miku está aquí también!"


translate spanish epilogue_mi_0d425fdb:


    "Me interrumpió."


translate spanish epilogue_mi_0f83334b:


    mt "¡Excelente! Hablaremos tras la formación."


translate spanish epilogue_mi_809254d9:


    my "De qué formación estás hablando...{w} Ya basta de jugar a este juego. Por el amor de Dios, ¡no tiene gracia!"


translate spanish epilogue_mi_4de002b5_1:


    "Me miró confundida."


translate spanish epilogue_mi_a82eeb20:


    mt "¿De qué hablas?"


translate spanish epilogue_mi_be2c12c6:


    my "¡Sobre eso!"


translate spanish epilogue_mi_5427ae40:


    "Masha observó con cautela desde detrás de mí."


translate spanish epilogue_mi_7609eb24:


    ma "Olga Dmitrievna, en verdad que no hace gracia..."


translate spanish epilogue_mi_b8ebaa25:


    mt "No os entiendo a ambos..."


translate spanish epilogue_mi_47e141b6:


    my "¡En serio, volvamos a seguir rodando!"


translate spanish epilogue_mi_deb210f9:


    mt "¿Qué rodaje?"


translate spanish epilogue_mi_ea1863fe:


    "La miré con atención y, por primera vez, me di cuenta de que tal vez ella no estaba bromeando."


translate spanish epilogue_mi_e369f105:


    "Olga Dmitrievna nunca fue una actriz decente, así que dudo que esa expresión de sorpresa e incomprensión sea falsa..."


translate spanish epilogue_mi_f385c640:


    "Qué mal..."


translate spanish epilogue_mi_55374b08:


    my "Vale, asumámoslo...{w} Cuéntanos tu versión."


translate spanish epilogue_mi_717e5682:


    mt "¿La versión de qué?"


translate spanish epilogue_mi_0da1d3c7:


    "Ella todavía parpadeaba con sus ojos mientras observaba mi boca."


translate spanish epilogue_mi_f33afa6e:


    my "Ya entiendo."


translate spanish epilogue_mi_1af119c3:


    "Agarré a Masha por su brazo y me alejé de la plaza."


translate spanish epilogue_mi_3dccb76f:


    ma "¡Ey, esperad! ¿Adónde vais?"


translate spanish epilogue_mi_e5c4f40a:


    my "Tenemos que encontrar a alguien menos loco."


translate spanish epilogue_mi_4742eb97:


    "Los gritos de Olga Dmitrievna nos siguieron, pero no presté atención."


translate spanish epilogue_mi_a20cefa7_11:


    "..."


translate spanish epilogue_mi_c718bb72:


    "Bastante pronto llegamos al edificio de clubs."


translate spanish epilogue_mi_03304259:


    "De acuerdo con el guión, tendría que estar justo aquí."


translate spanish epilogue_mi_448b06ba:


    my "Vamos."


translate spanish epilogue_mi_f81758d9:


    "Masha no respondió, solamente siguió mi orden."


translate spanish epilogue_mi_bd1a2f8e:


    "Abrí la puerta de golpe y caminé hacia dentro."


translate spanish epilogue_mi_d2107504:


    "Router estaba ahí, tal como esperaba."


translate spanish epilogue_mi_7feccd3d:


    my "¿Qué sucede?"


translate spanish epilogue_mi_dc50fc49:


    ro "¡Oh, Semyon! ¡Y no estás solo! ¡Qué tal! ¿Has venido para unirte a nuestro club?"


translate spanish epilogue_mi_b9c61f4c:


    "Parecía que también había algo raro en él."


translate spanish epilogue_mi_c2ed7553:


    my "¿Por qué no estás en la formación?"


translate spanish epilogue_mi_81684e59:


    "Pregunté con picardía."


translate spanish epilogue_mi_e3b581e3:


    ro "Ejem...{w} Bueno, tengo algo que acabar aquí."


translate spanish epilogue_mi_db19aca1:


    my "Ah, da igual, cuéntanos qué está ocurriendo aquí."


translate spanish epilogue_mi_a9ea475f:


    ro "¿Me estoy perdiendo algo?"


translate spanish epilogue_mi_bb291d76:


    "Al igual que Olga Dmitrievna, él me miraba fijamente sin señal alguna de comprensión."


translate spanish epilogue_mi_3286aba0:


    my "¿Por qué diantres todos se comportan como si fueran pioneros de verdad en un campamento de pioneros real?"


translate spanish epilogue_mi_50edcb0a:


    ro "¿Es que no es ese supuesto?"


translate spanish epilogue_mi_08a831fd:


    my "¿Lo es?"


translate spanish epilogue_mi_95e46bc6:


    ro "Bueno, en realidad..."


translate spanish epilogue_mi_3a9f1644:


    my "¿Y qué hay de la película?"


translate spanish epilogue_mi_5fe1cb2a:


    ro "¿Qué película?"


translate spanish epilogue_mi_48f2e671:


    my "La película... ¡La que estamos filmando!"


translate spanish epilogue_mi_71aef04d:


    ro "Semyon, de verdad que no comprendo de qué estás hablando..."


translate spanish epilogue_mi_114b3078:


    "Sólo ahora me percaté de que, todo el mundo de por aquí, me estaba llamando Semyon..."


translate spanish epilogue_mi_3af97c5c:


    "Bueno, Sasha siempre me estaba llamando también «Semyon-kun»..."


translate spanish epilogue_mi_2bdf9616:


    "Pero aquello era una broma claramente, mientras que aquí..."


translate spanish epilogue_mi_a28e10ab:


    "No es que sea un gran problema, ¡pero claramente mi nombre no es Semyon!"


translate spanish epilogue_mi_fc0ed84b:


    "Di un paso atrás horrorizado y tropecé con Masha."


translate spanish epilogue_mi_4a4eea7d:


    "Ella dio un pequeño chillido."


translate spanish epilogue_mi_179c9e45:


    ro "Y tú, Miku..."


translate spanish epilogue_mi_1a34a91c:


    "No estaba escuchando..."


translate spanish epilogue_mi_2264c5bc:


    "Tomé la mano de Masha en un arrebato y salí corriendo fuera."


translate spanish epilogue_mi_65d0e61a:


    "Me detuve solamente en la parada de autobús."


translate spanish epilogue_mi_90269e81:


    "Mientras me giraba a mi alrededor, vi a Masha recuperando el aliento."


translate spanish epilogue_mi_e8deb2c6:


    "Solté su mano y me senté al borde de la acera."


translate spanish epilogue_mi_2a47862d:


    my "Parece que ambos estamos en apuros..."


translate spanish epilogue_mi_9ddc03d3:


    ma "..."


translate spanish epilogue_mi_f91617dc:


    my "¿No lo entiendes? ¡Todo el mundo aquí se comporta exactamente de la misma forma en como lo escribí en el guión!"


translate spanish epilogue_mi_e0b753af:


    "Ella me miró fijamente de forma inexpresiva durante un rato, luego se recuperó:"


translate spanish epilogue_mi_50ba87dc:


    ma "Mmmm, una conjetura verosímil...{w} Pero espera... ¿por qué?"


translate spanish epilogue_mi_44093b8e:


    my "Como si lo supiera..."


translate spanish epilogue_mi_8389024e:


    ma "Entonces, ¿qué vamos a hacer?"


translate spanish epilogue_mi_326ad2f9:


    my "No lo sé..."


translate spanish epilogue_mi_a35f3a53:


    "Masha estaba al borde de romper en lágrimas."


translate spanish epilogue_mi_d4708852:


    my "¡Oh, para ya! ¡De verdad que no tengo ni idea! ¿Cómo voy a tener una solución hecha a medida para esta situación?"


translate spanish epilogue_mi_01180556:


    ma "Lo comprendo, pero..."


translate spanish epilogue_mi_f59e1b10:


    my "Después de todo, aun queda la posibilidad de que todo esto sea una broma absurda...{w} Bueno, una de muy mal gusto."


translate spanish epilogue_mi_63ef8b13:


    ma "Pero, ¿y si...?"


translate spanish epilogue_mi_10030a3d:


    my "Bueno, si... Luego ya actuaremos de acuerdo con las circunstancias."


translate spanish epilogue_mi_3e730633:


    "Suspiré y me levanté rápidamente."


translate spanish epilogue_mi_6f413eb8:


    my "Vayamos a comer algo al menos.{w} Creo que lo que sea que esté sucediendo aquí, la comida será servida igualmente."


translate spanish epilogue_mi_ee84d14d:


    "Masha me hizo una apenas distinguible sonrisa y me siguió."


translate spanish epilogue_mi_a20cefa7_12:


    "..."


translate spanish epilogue_mi_4cb70312:


    "La cantina estaba atiborrada."


translate spanish epilogue_mi_08c3283d:


    "Incluso en la formación ya me di cuenta de que había mucha más gente que la que había ayer."


translate spanish epilogue_mi_e35a16d4:


    "¿De verdad que todos estos niños han venido aquí? ¿A un campamento abandonado?"


translate spanish epilogue_mi_039e411f:


    "No, hay algo que realmente no va bien..."


translate spanish epilogue_mi_c99f77e3:


    "Aun así, la comida se estaba distribuyendo, por lo que tanto yo como Masha cogimos una bandeja y nos sentamos separados el uno del otro."


translate spanish epilogue_mi_fb2c31f0:


    "Comí atentamente, tratando de no alzar mi cabeza del plato y no mirar a las personas que me rodeaban."


translate spanish epilogue_mi_67b9e789:


    "Sin embargo, no me sirvió de mucho... en un par de minutos alguien se asomó cerca de nuestra mesa."


translate spanish epilogue_mi_02f457e1:


    "Apenas alcé mi mirada que vi a Lena."


translate spanish epilogue_mi_0ece5a44:


    un "¿Puedo...?"


translate spanish epilogue_mi_a86cd0ce:


    "Preguntó en voz baja."


translate spanish epilogue_mi_29741375:


    ma "¡Claro, adelante!"


translate spanish epilogue_mi_790d5a8d:


    "Mientras pensaba cómo debería responder, Masha apartó una silla de su lado e invitó a Lena con un gesto a sentarse."


translate spanish epilogue_mi_dac35953:


    un "No parecías tú misma esta madrugada..."


translate spanish epilogue_mi_94cc4744:


    "Dijo con ansiedad."


translate spanish epilogue_mi_684d739f:


    ma "Bueno... Me levanté de la cama con el pie izquierdo..."


translate spanish epilogue_mi_f8d7c8a1:


    "Masha se rió nerviosamente."


translate spanish epilogue_mi_6c544c99:


    "Lena me hizo una mirada y luego rápidamente la apartó."


translate spanish epilogue_mi_7bc737ed:


    my "Y bien, ¿qué tal?"


translate spanish epilogue_mi_c3c1a949:


    "Empecé con preguntas más bien genéricas."


translate spanish epilogue_mi_b993fafe:


    un "Bien."


translate spanish epilogue_mi_52d92c19:


    my "Ya veo..."


translate spanish epilogue_mi_26eb2fcd:


    "No tenía ni la más remota idea de qué podríamos hablar con ella."


translate spanish epilogue_mi_30741e46:


    "Además, desde luego que Lena se le veía bastante rara... normalmente suele ser muy divertida y optimista, ahora en cambio parece extremadamente tímida y reservada."


translate spanish epilogue_mi_827f779d:


    un "¿Has escuchado algo de Slavya?"


translate spanish epilogue_mi_7abdce27:


    my "¿Qué sucedió?"


translate spanish epilogue_mi_0c42474d:


    un "Bueno, nadie la ha visto desde ayer...{w} Primero desaparecisteis vosotros dos... Pero ahora estáis aquí... Y Slavya está..."


translate spanish epilogue_mi_d0cb10a2:


    "Dejaba ir largas pausas entre sus frases."


translate spanish epilogue_mi_f9611d76:


    "Todavía no lo entiendo... tanto si están actuando perfectamente bien, como si la criatura sentada delante nuestro no es nuestra Lena."


translate spanish epilogue_mi_449f4ec4_1:


    my "Nop."


translate spanish epilogue_mi_81d2b836:


    ma "No la hemos visto."


translate spanish epilogue_mi_69a8f246:


    un "Bueno, espero que vuelva de un momento a otro."


translate spanish epilogue_mi_9b627c74:


    my "Claro, esperemos que sí."


translate spanish epilogue_mi_6d56d6ac:


    "Slavya...{w} Desde luego, Sasha podría perderse a plena luz del día... eso no es muy sorprendente."


translate spanish epilogue_mi_531f4fb8:


    "Estaba acabando de comer los copos de avena que eran como de goma, cuando repentinamente Olga Dmitrievna salió corriendo a través de la cantina, moviendo sus manos."


translate spanish epilogue_mi_3d70faed:


    "Miraba alrededor sin emoción y, tras verme, se dirigió directamente a nuestra mesa a paso rápido."


translate spanish epilogue_mi_0bab5229:


    mt "¡Oh, aquí estáis! ¡Vamos a tener una charla!"


translate spanish epilogue_mi_762bfea5:


    "Susurró en voz baja frente a mi cara."


translate spanish epilogue_mi_af771b64:


    "Resultó que ponerle excusas no valdría la pena, así que me levanté, asentí hacia Masha y nos fuimos tras Olga Dmitrievna."


translate spanish epilogue_mi_45d3dfab:


    "Tras estar fuera, se dio la vuelta de golpe y dijo:"


translate spanish epilogue_mi_ec4d970a:


    mt "Y bien, ¿dónde estuvistéis la pasada noche?"


translate spanish epilogue_mi_97132f75:


    my "¿Debería importarte...?"


translate spanish epilogue_mi_8e0229dd:


    "Me encogí de hombros."


translate spanish epilogue_mi_349b0aba:


    mt "¡Debería! ¿Sabéis que Slavya está desaparecida?"


translate spanish epilogue_mi_4f716193:


    my "Justo escuchamos eso de Lena."


translate spanish epilogue_mi_e6a0d896:


    mt "¡Exactamente! Fui a buscarla y...{w} ¡la hallé!"


translate spanish epilogue_mi_b34ccd5d:


    "Parecía que estaba desesperadamente conteniendo sus lágrimas."


translate spanish epilogue_mi_16fc9de2:


    my "Eso son buenas noticias pues."


translate spanish epilogue_mi_9175e7a8:


    mt "Vamos, dejadme que os lo muestre."


translate spanish epilogue_mi_001b9dd0:


    "Dijo en voz baja y se dirigió en dirección al bosque."


translate spanish epilogue_mi_01ce7e3e:


    "Miré a Masha."


translate spanish epilogue_mi_e0d7e61d:


    ma "Tal vez, no deberíamos..."


translate spanish epilogue_mi_13269186:


    my "¿Qué hay de malo?"


translate spanish epilogue_mi_ab7331fb:


    "Sin embargo, mi voz interior estaba de acuerdo con ella."


translate spanish epilogue_mi_a14a2f57:


    ma "PUes muy bien... Pero estoy preocupada."


translate spanish epilogue_mi_7851bc23:


    my "Todo irá bien..."


translate spanish epilogue_mi_bef6e9f9:


    "Traté de sonreír, pero incluso sin mirarme en un espejo, era obvio que mi sonrisa era tan falsa que me ponía enfermo."


translate spanish epilogue_mi_c390a24d:


    "Pronto, llegamos a un pequeño claro."


translate spanish epilogue_mi_77b14888:


    mt "Ahí..."


translate spanish epilogue_mi_0498059b:


    "Olga Dmitrievna señaló a un árbol."


translate spanish epilogue_mi_477f49be:


    "Di unos cuantos pasos y, repentinamente, detecté un brazo humano mutilado.{w} De hecho, no estaba ni mutilado, más bien había sido arrancado."


translate spanish epilogue_mi_6e403fb6:


    "Masha miró desde detrás de mis hombros y chilló aterrorizada de inmediato."


translate spanish epilogue_mi_3d6cfafe:


    my "¡No mires!"


translate spanish epilogue_mi_efa05797:


    "Rompió a llorar y se precipitó en los brazos de Olga Dmitrievna."


translate spanish epilogue_mi_3352219f:


    "Hice un esfuerzo considerable: tenía que ver detrás del árbol, para asegurarme de que efectivamente era Slavya."


translate spanish epilogue_mi_a45eb573:


    "Varias partes del cuerpo yacían ahí: un brazo, una pierna, quebradas en diferentes partes, un trozo de carne del pecho, una cabeza con cabello apelmazado por la sangre y unos ojos vidriosos profundamente hundidos."


translate spanish epilogue_mi_6ae474eb:


    "Sentí como si fuera a vomitar e instintivamente aparté mi mirada."


translate spanish epilogue_mi_7cb2ad73:


    my "¿Quién hizo esto...?"


translate spanish epilogue_mi_a0c3cc82:


    mt "¿Cómo voy a saberlo?"


translate spanish epilogue_mi_bd343a2b:


    "Dijo Olga Dmitrievna con una sorprendente voz fría como el mármol."


translate spanish epilogue_mi_2cd46005:


    mt "En todo el campamento, los únicos que no tenéis una coartada sois vosotros dos."


translate spanish epilogue_mi_9ef4463b:


    "Masha continuaba llorando, ahora en mi hombro."


translate spanish epilogue_mi_8746139c:


    my "¡¿Qué?! ¡¿En serio crees que íbamos a poder hacer... esto?!"


translate spanish epilogue_mi_76171197:


    "La miré furiosamente."


translate spanish epilogue_mi_2deb3a6a:


    "Ahora no importaba mucho qué estaba ocurriendo aquí... bromas absurdas o algo más serio... ¡una persona ha sido asesinada!"


translate spanish epilogue_mi_4cc32fd1:


    "Una persona que conocíamos.{w} Slavya o Sasha... ¡es indiferente!"


translate spanish epilogue_mi_9b82e676:


    mt "Bueno, no..."


translate spanish epilogue_mi_c0ab7802:


    "Dudó."


translate spanish epilogue_mi_9c0b8144:


    mt "¡Pero alguien tuvo que hacerlo!"


translate spanish epilogue_mi_19cc5970:


    my "Diría que algunos animales salvajes. Probablemente lobos."


translate spanish epilogue_mi_5b070411:


    mt "Nunca han habido lobos por los alrededores..."


translate spanish epilogue_mi_0b9bc04a:


    my "Y bien, ¡¿crees que un humano podría haberlo hecho?!"


translate spanish epilogue_mi_e504c1f3:


    mt "Bueno, no hay nadie más a quien culpar..."


translate spanish epilogue_mi_9d5abb5c:


    my "No, no tiene ningún sentido... No puede ser..."


translate spanish epilogue_mi_03e931a3:


    "Abracé más estrechamente a Masha."


translate spanish epilogue_mi_32a03025:


    "Su sollozo se volvió silencioso."


translate spanish epilogue_mi_4385d694:


    mt "De todas formas, tenemos que llamar a la policía."


translate spanish epilogue_mi_d61b9628:


    my "¡Por supuesto que sí! ¡Vamos!"


translate spanish epilogue_mi_a20cefa7_13:


    "..."


translate spanish epilogue_mi_978dd8c8:


    "El distrito central está bastante lejos del campamento...{w} La policia no llegará pronto..."


translate spanish epilogue_mi_dad64e6b:


    "Oscurecía."


translate spanish epilogue_mi_209abbb1:


    "Estaba sentado en la plaza, contemplando el ocaso."


translate spanish epilogue_mi_cd30d9b9:


    "Masha estaba durmiendo en la cabaña de Olga Dmitrievna."


translate spanish epilogue_mi_6eda1825:


    "Le trastornó a ella mucho más que a nosotros.{w} Por muchos motivos..."


translate spanish epilogue_mi_a5a8139e:


    "Tenía dos incógnitas principales en mente: la primera, qué ocurría aquí; y la segunda, quién asesinó a Slavya.{w} O Sasha. Da igual, ¿quién es el asesino?"


translate spanish epilogue_mi_a645528a:


    "La primera de todas se podía posponer por ahora.{w} Pero no tenía ni remota idea de cómo encajar la segunda."


translate spanish epilogue_mi_b9e6fa5a:


    "A pesar de toda la confusión, la conclusión por la cual ella habría sido violentada por bestias salvajes, parecía ser la explicación más razonable."


translate spanish epilogue_mi_919c8c4f:


    "No me preocupa si aquí hay o no lobos...{w} ¡Podría ser hasta un oso! O un tejón... ¡Pero ciertamente no un humano!"


translate spanish epilogue_mi_a5f91356:


    "Nadie podría haber realizado algo como eso sin herramientas de más."


translate spanish epilogue_mi_252102f2:


    "Y el tipo de heridas dejaban claro que la desafortunada víctima había sido descuartizada a manos desnudas (o con garras)."


translate spanish epilogue_mi_44e8a885:


    "Aunque es cierto que un humano encolerizado puede llegar a tener casi fuerza sobrehumana..."


translate spanish epilogue_mi_e0069242:


    "Pero de toda la gente reunida aquí, ¿quién podría ser capaz de algo así? ¿Router? ¿Lena? ¿Tal vez la propia Olga Dmitrievna?"


translate spanish epilogue_mi_a4cb3fed:


    "No, eso es estúpido."


translate spanish epilogue_mi_d3f93a18:


    "Pero aun y así, había muchos más niños a parte de ellos en la cantina y en la plaza."


translate spanish epilogue_mi_b10ee23b:


    "¿Pero acaso en serio voy a interrogar a cada uno de ellos?{w} ¡Ése es el trabajo de la policía!"


translate spanish epilogue_mi_b9939397:


    "Entonces, ¿de qué hay que preocuparse? Quizá no estoy preocupado por mí...{w} Sino por Masha..."


translate spanish epilogue_mi_b2e7370c:


    "Correcto, ¡necesito comprobar qué tal le va!"


translate spanish epilogue_mi_ca00c08d:


    "Estaba a punto de levantarme cuando, de repente, sentí la presencia de alguien."


translate spanish epilogue_mi_4a224c84:


    "Miré atrás y vi a Lena."


translate spanish epilogue_mi_86d435bf:


    un "Oh, no quería asustarte..."


translate spanish epilogue_mi_7851bc23_1:


    my "No pasa nada..."


translate spanish epilogue_mi_c67f7c91:


    un "¿Puedo sentarme aquí?"


translate spanish epilogue_mi_3cdc5a84:


    my "Sí, siéntate, por favor."


translate spanish epilogue_mi_e29e5371:


    "Se ajustó su falda con un pulcro movimiento, se sentó y comenzó a contemplar el ocaso."


translate spanish epilogue_mi_3b7b5e4d:


    un "Qué triste es todo esto..."


translate spanish epilogue_mi_4170725c:


    my "Claro, no hay nada de qué disfrutar."


translate spanish epilogue_mi_7d25e023:


    un "¿Qué crees? ¿Quién podría haber hecho esto?"


translate spanish epilogue_mi_4612d3fc:


    my "¿Cómo podría saberlo? Ciertamente, yo no."


translate spanish epilogue_mi_4f66a37f:


    un "¡No, no! ¡No es que sospeche de ti o algo así!"


translate spanish epilogue_mi_e77f4d13:


    "Comenzó a mover sus manos de una forma graciosa."


translate spanish epilogue_mi_09ae1847:


    my "Bueno, gracias por eso."


translate spanish epilogue_mi_851fed46:


    "Sonreí afectado con tristeza."


translate spanish epilogue_mi_41c9cd8c:


    un "Pero debe haber sido hecho por alguien muy cruel, una persona muy malvada."


translate spanish epilogue_mi_6fa72eff:


    my "Bueno, no necesariamente..."


translate spanish epilogue_mi_d713736e:


    "Lena me miró confundida."


translate spanish epilogue_mi_c2861722:


    my "Quizás fue una persona excepcionalmente engañosa. Después de todo, en el transcurso del día a día, hasta los maníacos pueden ser personas simpáticas."


translate spanish epilogue_mi_b9867686:


    un "Entonces, ¿sugieres que...?"


translate spanish epilogue_mi_0ae367b8:


    "Sus ojos se abrieron y apretó su mandíbula un poco."


translate spanish epilogue_mi_dbe54858:


    un "¿Que podría haber sido hecho por cualquiera de nosotros...?"


translate spanish epilogue_mi_ee63da1c:


    my "Bueno, no estoy afirmando eso, está claro... Pero el radio de sospechosos es bastante grande."


translate spanish epilogue_mi_a69b68b1:


    un "No... Ninguno de los niños podría hacer aquello... Ninguno..."


translate spanish epilogue_mi_e69771c0:


    my "De todas maneras, es asunto de la policía ocuparse de ello..."


translate spanish epilogue_mi_a4f042ce:


    "Nos pasamos más tiempo sentados en silencio."


translate spanish epilogue_mi_1918988a:


    "Estaba a punto de irme, cuando Lena repentinamente susurró:"


translate spanish epilogue_mi_090aef2c:


    un "O podrías tú..."


translate spanish epilogue_mi_b9e79bf2_1:


    my "¿Qué?"


translate spanish epilogue_mi_4745db66:


    "Una sonrisa poco natural apareció en su rostro."


translate spanish epilogue_mi_7b836a93:


    "Una sonrisa diabólica... creo."


translate spanish epilogue_mi_298ef616:


    un "¿Podrías haber asesinado tú a Slavya?"


translate spanish epilogue_mi_a4b858b3:


    my "¡Por supuesto que no!"


translate spanish epilogue_mi_683f39d5:


    un "Pero vosotros dos no os llevabáis muy bien..."


translate spanish epilogue_mi_a6716552:


    my "¿Por qué piensas así? Nos llevábamos bien."


translate spanish epilogue_mi_82a95096:


    "De repente se hizo claro que Lena, más bien no hablaba de mí."


translate spanish epilogue_mi_a34c7a38:


    "Para ser más exactos, no el yo de ayer.{w} Y no la Slavya que su nombre es Sasha."


translate spanish epilogue_mi_536e6181:


    "Estaba seguro de eso."


translate spanish epilogue_mi_a0e48a85:


    my "No, ¡no fui yo!"


translate spanish epilogue_mi_0f6c6eab:


    un "Vale, vale, relájate, no te estoy acusando..."


translate spanish epilogue_mi_ba4db5a4:


    "Me hizo una mirada y, por un segundo, sus ojos me parecieron rojos."


translate spanish epilogue_mi_7f322486:


    "Tal vez sólo fuera un reflejo del ocaso..."


translate spanish epilogue_mi_0ed60aab:


    my "Me alegra que lo comprendas correctamente."


translate spanish epilogue_mi_8dc0ae33:


    un "Pero fuiste tú quien dijo que todo el mundo era un posible sospechoso."


translate spanish epilogue_mi_09973a1a:


    my "¡No te dije que «todo el mundo sea un posible sospecho»! ¡Quise decir que no sabemos quien es el criminal! Aun..."


translate spanish epilogue_mi_cb7116cc:


    un "Oh, ya veo. Bueno, ¡espero no convertirme en la siguiente víctima!"


translate spanish epilogue_mi_a73c91c7:


    "Lena lo dijo en voz alta y rompió a carcajadas."


translate spanish epilogue_mi_e7ba1f7e:


    "La manera en que lo hizo me dio muy mala espina."


translate spanish epilogue_mi_ccdf7460:


    un "Vale, es hora de irse."


translate spanish epilogue_mi_7bec5d07:


    "Se levantó y se fue rauda de la plaza."


translate spanish epilogue_mi_d1c55a8c:


    "Preferí no entretenerme y me dirigí hacia la cabaña de Olga Dmitrievna."


translate spanish epilogue_mi_a20cefa7_14:


    "..."


translate spanish epilogue_mi_50fa59ee:


    "Ahora se estaba completamente oscuro..."


translate spanish epilogue_mi_ee446fb5:


    "Masha todavía estaba durmiendo, así que decidí no despertarla y, silenciosamente, me puse a su lado."


translate spanish epilogue_mi_e4f4f44e:


    "Nuestra directora (o la líder del campamento, como los lugareños seguían refiriéndose a ella) no estaba a la vista."


translate spanish epilogue_mi_87554d1b:


    "Si ella entrara ahora, estaría completamente incómoda.{w} Bueno, puede, por lo menos."


translate spanish epilogue_mi_1cd7d952:


    "De todos modos, ¿qué más da? Una persona ha sido asesinada... ¡no es momento para tales nimiedades!"


translate spanish epilogue_mi_8f3d22b2:


    "Y de todas formas, todavía tenemos que seguir buscando qué ocurre... ¿por qué todo el mundo sigue comportándose como si fueran pioneros?"


translate spanish epilogue_mi_9ef4ae27:


    "Me parece que hasta que no esté claro quién es el verdadero criminal, toda esta ridícula pantomima no terminará."


translate spanish epilogue_mi_f240c5d7:


    "La verdadera cuestión era, ¿cuál es la causa y cuál es el efecto...?"


translate spanish epilogue_mi_a59fd30e:


    "Tanto si todo el mundo por estar fingiendo a causado el asesinato de Slavya (todavía me negaba a aceptar que Sasha fuera la víctima, por lo que me decía a mí mismo que debía ser Slavya), como si Slavya fue muerta a causa de que todo el mundo estuviera fingiendo..."


translate spanish epilogue_mi_c6188591:


    "¡Maldita sea! Estoy completamente perdido, y tales cavilaciones no me van a llevar a ninguna parte."


translate spanish epilogue_mi_90f7eafc:


    "La puerta chirrió ligeramente."


translate spanish epilogue_mi_cdaff57d:


    "Con cautela, me alcé un poco con mis codos y miré detenidamente en la oscuridad."


translate spanish epilogue_mi_108f56f1:


    "Transcurrieron unos instantes como una tortura, pero al final nadie entró."


translate spanish epilogue_mi_05be7ae4:


    "Bueno, supongo que no tiene utilidad estar estirado y esperar."


translate spanish epilogue_mi_080212dd:


    "Ahora mismo probablemente sea sólo una ráfaga de viento o algo así, pero la noche se ha hecho tan tarde, ahora que todos están durmiendo..."


translate spanish epilogue_mi_68b1a2b9:


    "Me levanté, agarré mis pantalones en la oscuridad y miré por los alrededores."


translate spanish epilogue_mi_7d8bf151:


    "Era bastante improbable que encontrara algo aquí que pudiera servirme, digamos, como autodefensa."


translate spanish epilogue_mi_63056596:


    "Pero al menos tenía que intentarlo."


translate spanish epilogue_mi_e04f66bc:


    "Hurgando por la cabaña, tropecé con un cajón cerrado dentro de un armario."


translate spanish epilogue_mi_40adda07:


    "Pues claro, me encantaría encontrar ahí un arma o al menos algún tipo de pasamanos pesado. Sin embargo, comprendí totalmente que en realidad no debería aspirar a ninguno."


translate spanish epilogue_mi_55acc250:


    "¡Pero tenía que estar cerrado por algún motivo!"


translate spanish epilogue_mi_10eae588:


    "Aparté el armario de la pared, estimé dónde estaría exactamente la parte trasera del cajón, y le di una patada aguda y precisa con mi pierna (de manera que no despertaría a Masha)."


translate spanish epilogue_mi_07f9ee90:


    "El contrachapado se rompió, y algunas cosas se desparramaron delante del cajón."


translate spanish epilogue_mi_db4073e9:


    "Ropa interior...{w} Más ropa interior...{w} Cerillas."


translate spanish epilogue_mi_d0a78133:


    "Oh, para tenerlas a mano."


translate spanish epilogue_mi_c88871a5:


    "Guardé la caja de cerillas en mi bolsillo y, al mismo instante, escuché unos ruidos extraños afuera tras la puerta, como si un animal salvaje la estuviera arañando."


translate spanish epilogue_mi_9795e900:


    "Seguro que si ese es el verdadero criminal, no tendré ni una sola oportunidad frente a él..."


translate spanish epilogue_mi_274d3fca:


    "Pero uno no debería desechar la posibilidad de que fuera obra de un «pioпero» local."


translate spanish epilogue_mi_6e7c4600:


    "El cual se habría vuelto loco o que, sencillamente, ya estaba demente desde un principio..."


translate spanish epilogue_mi_3d71d05e:


    "De todas maneras, aun así debería poder dejar fuera de lugar a cualquiera de los lugareños.{w} Incluso aunque se me acerquen con un cuchillo..."


translate spanish epilogue_mi_760217c6:


    "Al pensar en un arma se me puso la piel de gallina."


translate spanish epilogue_mi_0c0e46bd:


    "No, ¡no debería ser tan confiado!"


translate spanish epilogue_mi_c7ea3de8:


    "Si me encuentro en una escaramuza con alguien que, por ejemplo, está armado con un cuchillo, ¡lo que debo hacer es cerrar la puerta haciendo una barricada en la entrada y esperar a ser auxiliado!"


translate spanish epilogue_mi_2466c05f:


    "Probablemente no podrías hacerte con un arma de fuego aquí...{w} Aunque..."


translate spanish epilogue_mi_c189c152:


    "La puerta chirrió una vez más al abrirse como un centímetro."


translate spanish epilogue_mi_1ad9f73d:


    "Cerré mis ojos por un segundo y violentamente la abrí, reuniendo todas mis fuerzas."


translate spanish epilogue_mi_d2ae2e3c:


    "Escuché un grito ahogado y vi a Alisa, con los brazos y piernas estiradas del susto en la entrada."


translate spanish epilogue_mi_e13ab7da:


    my "Oh, sólo eres tú..."


translate spanish epilogue_mi_606c5803:


    "Expiré un suspiro de alivio."


translate spanish epilogue_mi_93228cc9:


    my "¡No asustes a la gente de esa forma! ¿Es que me quieres dar un ataque de corazón?"


translate spanish epilogue_mi_63ed4b6c:


    "Se levantó, se quitó el polvo de la ropa lentamente, y me echó un vistazo con mirada de culpable."


translate spanish epilogue_mi_77613451:


    dv "Lo siento..."


translate spanish epilogue_mi_91d22172:


    my "Está bien. Sólo que... Podría ser peligroso caminar sola por el campamento durante la noche... con todo lo que está ocurriendo..."


translate spanish epilogue_mi_2a7a8a3b:


    "Me detuve en seco."


translate spanish epilogue_mi_f9d90a33:


    "«Ocurriendo» implica un proceso en marcha.{w} Como si todavía algo estuviera sucediendo..."


translate spanish epilogue_mi_126375f0:


    my "Quiero decir, ocurrió..."


translate spanish epilogue_mi_a3a258d8:


    dv "Por eso vine..."


translate spanish epilogue_mi_0fc06d24:


    "Alisa miraba al suelo y temblaba ligeramente."


translate spanish epilogue_mi_2517d987:


    dv "Ya es muy tarde, pero Ulyana aun no se le ha visto."


translate spanish epilogue_mi_71da9f7d:


    "Alzó su mirada hacia mí, y vi lágrimas cayendo por sus mejillas."


translate spanish epilogue_mi_31b3f8f3:


    my "¿Dónde podría haber ido?"


translate spanish epilogue_mi_f8590b39:


    "No es que estuviera realmente preocupado, pero le pregunté ya que sentí que había que tranquilizar la situación."


translate spanish epilogue_mi_b82284e4:


    "Estaba perfectamente claro que no había nada en absoluto que hacer en este campamento por la noche... si hubiera electricidad disponible, la gente vería películas en sus computadoras portátiles, si no estarían solamente leyendo libros iluminados por velas."


translate spanish epilogue_mi_32d70287:


    "Vagar bajo la luna no debería estar en esa lista..."


translate spanish epilogue_mi_bf433c67:


    dv "No lo sé..."


translate spanish epilogue_mi_0709f70a:


    my "Vale, pensemos en ello sin armar jaleo..."


translate spanish epilogue_mi_23b8137e:


    "Caminé hasta la mesita y cogí el reloj."


translate spanish epilogue_mi_26bbbc79:


    "Es medianoche.{w} Obviamente, todo el mundo debería estar ya en sus cabañas a estas horas."


translate spanish epilogue_mi_65e215d0:


    "Una idea horrible me vino a la mente de repente.{w} ¿Dónde está Olga Dmitrievna...?"


translate spanish epilogue_mi_2f7416f2:


    my "Salgamos fuera y hablemos ahí. No quiero que despertemos a Ma... Miku..."


translate spanish epilogue_mi_11889848:


    "En cuanto salimos fuera, tomé lugar en las escaleras de la entrada y comencé a pensar."


translate spanish epilogue_mi_5be4562e:


    "Alisa permanecía a mi lado, frotándose las manos con un aspecto de dolor en su rostro mientras veía el ascenso de la luna llena."


translate spanish epilogue_mi_203b67d9:


    dv "Tenemos que llamar a la policía."


translate spanish epilogue_mi_cb6ef91a:


    my "Ya les hemos llamado."


translate spanish epilogue_mi_fb4190cc:


    dv "Pero entonces sólo era Slavya, y ahora también es Ulyana..."


translate spanish epilogue_mi_e63e04ce:


    my "Y Olga Dmitrievna. ¿La has visto?"


translate spanish epilogue_mi_b8e9ed52:


    dv "No... Pero por qué, ¿ella también...?"


translate spanish epilogue_mi_b680e150:


    "Alisa rompió a llorar."


translate spanish epilogue_mi_c0fe2e06:


    my "Todavía no sabemos nada, pero..."


translate spanish epilogue_mi_01f7b9c3:


    "Pero no obstante, ¡está claro que algo ocurre aquí!"


translate spanish epilogue_mi_06d2a65f:


    "Incluso si ellos no simulan ser pioneros y algo ha ocurrido realmente en este mundo, ahora mismo podría tener una buena oportunidad de escaparme sin hallar respuestas a mis cuestiones."


translate spanish epilogue_mi_d6e99617:


    "Por lo que tengo que concentrarme en sobrevivir..."


translate spanish epilogue_mi_d8fea1da:


    "«Sobrevivir»...{w} La palabra punzaba como un dolor agudo en la parte posterior de la cabeza."


translate spanish epilogue_mi_aa6ca21f:


    "«Sobrevivir»...{w} Justo ayer no necesitaba prestar ni la más mínima consideración a algo como eso..."


translate spanish epilogue_mi_1b3de5ca:


    "La tranquila noche veraniega estaba reinando sobre todo a nuestro alrededor... ni una ráfaga de viento, ni el crugir de la hierba o un susurro de las hojas agitándose se podía escuchar.{w} Nada."


translate spanish epilogue_mi_0bbb84bd:


    "Solamente el reflejo de la luz lunar en las ventanas de las cabañas, escasas nubes en el cielo y aun así una impenetrable oscuridad alrededor... dicen que cuando miras largo tiempo a un abismo, el abismo también mira dentro de ti."


translate spanish epilogue_mi_845dfb1b:


    "¡Y los saltamontes!{w} ¡Estos malditos bichos estaban haciendo sesión doble de DJ!"


translate spanish epilogue_mi_77894e87:


    "Como si estuvieran constantemente en deber... cuando el día se acaba, la noche llega, el que no ha durmido suficiente, comienza.{w} Luego la noche llega a su final y..."


translate spanish epilogue_mi_f03db694:


    "Tengo que encontrar algún insecticida... al menos puedo limpiar un par de zonas, con algo de suerte tendría algo de espacio con tranquilidad alrededor de la cabaña."


translate spanish epilogue_mi_ae29cb48:


    "Seguro, puedo destruirlos a todos..."


translate spanish epilogue_mi_6e9aee0c:


    "Qué diantres, probablemente hay más de ellos que humanos en todo el mundo..."


translate spanish epilogue_mi_18e03cde:


    dv "¿Puedes escucharlo...?"


translate spanish epilogue_mi_bdaf90db:


    "Alisa se sentó y se hizo un ovillo junto a mí.{w} Todo su cuerpo entero estaba temblando."


translate spanish epilogue_mi_b9e79bf2_2:


    my "¿Qué?"


translate spanish epilogue_mi_21f9a60f:


    "Afiné mi oído."


translate spanish epilogue_mi_a969edc9:


    "Parecía que alguien estaba caminando lentamente por el sendero hasta nuestra cabaña."


translate spanish epilogue_mi_d7c542e6:


    "No podía figurarme quién era en la oscuridad, pero los pasos se volvieron más y más audibles a cada segundo."


translate spanish epilogue_mi_53d2a1ff:


    my "Vale, no te pongas nerviosa... No se atreverá a atacar a dos de nosotros..."


translate spanish epilogue_mi_9e7e2c33:


    "Aunque no estaba muy seguro de ese extremo."


translate spanish epilogue_mi_62e9f0fe:


    "Mis manos instintivamente comenzaron a buscar algo con lo que pudiera defenderme."


translate spanish epilogue_mi_93b5fc50:


    dv "¡Ey!"


translate spanish epilogue_mi_ec37960b:


    my "Lo siento..."


translate spanish epilogue_mi_e042b998:


    "Bien pronto hallé una pieza de una tubería de agua bajo el banco."


translate spanish epilogue_mi_58a4016f:


    "No es que sea de gran ayuda, desde luego... es muy gruesa e incómoda para manejarla adecuadamente, pero aun así es mejor que tener las manos vacías."


translate spanish epilogue_mi_a37831a4:


    "¡La mera vista de esta monstruosidad hubiera hecho que cualquiera se meara encima!{w} Más bien de risa..."


translate spanish epilogue_mi_1b95dc17:


    "No hay nada que pueda hacer... éramos tan buenos como unos patos sentados aquí, así que lo mejor era dirigirse hacia el enemigo."


translate spanish epilogue_mi_9bcff4c0:


    "No podría decir que estuviera hinchado de coraje y valor, de hecho todo lo contrario, estaba preso del terror."


translate spanish epilogue_mi_b9ce7582:


    "Simplemente parecía que no había otra opción..."


translate spanish epilogue_mi_f55c7e59:


    "Respiré profundamente y lentamente me adentré en la oscuridad."


translate spanish epilogue_mi_439b90ad:


    "Bien pronto pude figurarme características de la silueta del extraño.{w} No muy alto, con un peinado extraño, una falda..."


translate spanish epilogue_mi_86e127e7:


    "¡Ésa tenía que ser Lena!"


translate spanish epilogue_mi_ef6c8558:


    "Bajé mi guarda."


translate spanish epilogue_mi_af67a563:


    my "¿Qué estás haciendo a estas horas de la noche?"


translate spanish epilogue_mi_ef1f0ae2:


    un "Yo... yo..."


translate spanish epilogue_mi_2c21a7bc:


    "El rostro de Lena estaba manchado por las lágrimas."


translate spanish epilogue_mi_448b06ba_1:


    my "Venga, ven."


translate spanish epilogue_mi_a6d8a438:


    "La acompañé hasta Alisa."


translate spanish epilogue_mi_5b1b0f13:


    "En cuanto Lena recobró su compostura, trató de explicar lo que sucedió."


translate spanish epilogue_mi_41ff22a7:


    un "Estaba sentada sola y... ya era bastante tarde y... pero Miku todavía estaba perdida... además... lo que ocurrió con Slavya... y yo..."


translate spanish epilogue_mi_3019d555:


    "Aun no me podía acostumbrar a esta imagen de Lena... una muchacha sollozando sentada frent a mí... con la otra que vi en la plaza hace pocas horas, con la sonrisa diabólica y las llamas del infierno en sus ojos."


translate spanish epilogue_mi_772757c7:


    "Tal vez sólo fue mi imaginación...{w} O quizá no..."


translate spanish epilogue_mi_97352ac0:


    my "Por casualidad, ¿sabes dónde están Ulyana y Olga Dmitrievna?"


translate spanish epilogue_mi_3932a02b:


    "Ella alzó sus sorprendidos ojos hacia mí."


translate spanish epilogue_mi_74ae6a8d:


    un "No, por qué... ¿también están desaparecidas?"


translate spanish epilogue_mi_52f6ea86:


    my "Bueno, por ahora no estamos seguros..."


translate spanish epilogue_mi_4297ae4e:


    dv "Semyon..."


translate spanish epilogue_mi_5b9b3c9e:


    "Al principio, no me di cuenta de que se estaba dirigiendo a mí."


translate spanish epilogue_mi_366fbd9e:


    my "¿Eh?"


translate spanish epilogue_mi_d6783e26:


    dv "¿No crees que se está demasiado tranquilo en el campamento?"


translate spanish epilogue_mi_9c6951b5:


    my "Es de noche, todo el mundo está durmiendo, ¿no...?"


translate spanish epilogue_mi_09a7148c:


    "Afiné mi oído.{w} Afirmativo, no habían sonidos aparte de esos malditos saltamontes."


translate spanish epilogue_mi_ae49ef98:


    "Quiero decir, ¡ningún ruido de nada!"


translate spanish epilogue_mi_c86f665e:


    my "¿Cómo es habitualmente este lugar?"


translate spanish epilogue_mi_bdbb2826:


    un "Silencioso..."


translate spanish epilogue_mi_7e596111:


    "Susurró Lena."


translate spanish epilogue_mi_100a8b2f:


    dv "Silencioso, pero no esta tranquilidad."


translate spanish epilogue_mi_20845d8d:


    "No podía dejar que las muchachas entraran en pánico."


translate spanish epilogue_mi_0b7cd763:


    "Aunque comenzaba a asustarme."


translate spanish epilogue_mi_c62e4545:


    "A pesar de que habían muchos motivos para ello: humanos desapareciendo en la nada, asesinatos horribles, amigos que simulaban ser pioneros incluso en estas circunstancias."


translate spanish epilogue_mi_aa1ba754:


    "Comenzaba a perder la calma..."


translate spanish epilogue_mi_d00c5bcd:


    my "Vale, pensemos racionalmente."


translate spanish epilogue_mi_fcc1f8e9:


    "Las muchachas me hicieron miradas inquisitivas."


translate spanish epilogue_mi_630f46ee:


    my "¡La gente no puede desaparecer sin dejar ningún rastro!"


translate spanish epilogue_mi_6b952a33:


    un "Eso significa..."


translate spanish epilogue_mi_480a55d0:


    my "Nop, ¡eso no significa nada!"


translate spanish epilogue_mi_8fff31b7:


    "Repentinamente escuchamos un crugir en los arbustos detrás de la cabaña."


translate spanish epilogue_mi_ee4a8cbd:


    "Antes incluso de que pudiera agarrar mi tubería, Router emergió de la oscuridad."


translate spanish epilogue_mi_b7ab4b8d:


    ro "¡Ah, aquí estáis!"


translate spanish epilogue_mi_6d30a2a8:


    "No parecía estar al corriente de todo lo que estaba ocurriendo alrededor."


translate spanish epilogue_mi_2986d3b8:


    my "¡Oh Señor, haz que la gente deje ya de aparecer de esta forma! ¡Las muchachas van a tener un ataque de corazón a este ritmo!"


translate spanish epilogue_mi_776c97bc:


    "Aunque no estaba seguro de quién tenía más miedo... si ellas o yo."


translate spanish epilogue_mi_43b66a40:


    ro "Claro, pido disculpas..."


translate spanish epilogue_mi_38116f5e:


    my "¿Por qué viniste?"


translate spanish epilogue_mi_d2c06568:


    ro "¿Habéis visto a Shurik?"


translate spanish epilogue_mi_b84b7fde:


    my "No..."


translate spanish epilogue_mi_75fdeb9e:


    "Es como si la mitad del campamento simplemente hubiera desaparecido en las últimas horas."


translate spanish epilogue_mi_a5873d1b:


    ro "Además, se está demasiado tranquilo por aquí..."


translate spanish epilogue_mi_b01d3ea3:


    my "¡Ya nos dimos cuenta de eso!"


translate spanish epilogue_mi_763991f9:


    ro "No hay ni una luz, un silencio mortecino..."


translate spanish epilogue_mi_c4497e2f:


    my "Oh, ¡no empieces a azuzar más tensión! ¡Ya me siento mal!"


translate spanish epilogue_mi_a90284ea:


    ro "¿Qué estoy haciendo? No hago nada..."


translate spanish epilogue_mi_d4f9df56:


    "Esbozó una sonrisa misteriosa que me dio escalofríos."


translate spanish epilogue_mi_569deb9a:


    my "Vale, ¡quédate aquí y yo ire a explorar por los aledaños del campamento!"


translate spanish epilogue_mi_c87950e7:


    un "¿Solo?"


translate spanish epilogue_mi_8ab3285c:


    "Claro, eso probablemente no era exactamente la idea más brillante...{w} Tengo que pensar primero, luego hablar."


translate spanish epilogue_mi_c90f163e:


    my "Ejem..."


translate spanish epilogue_mi_862e0a1f:


    dv "¡Iré contigo!"


translate spanish epilogue_mi_0f4ce8dc:


    my "Vale, ¡entonces los demás quedaos aquí! Y no os olvidéis de que Ma... Miku está todavía durmiendo en la cabaña."


translate spanish epilogue_mi_785bce8b:


    ro "¡A sus órdenes!"


translate spanish epilogue_mi_ee6a930d:


    "¡Qué cretino! Si me hiciera esa sonrisa de zopenco en cualquiera otra situación..."


translate spanish epilogue_mi_a20cefa7_15:


    "..."


translate spanish epilogue_mi_a20cefa7_16:


    "..."


translate spanish epilogue_mi_982fd05a:


    "Aproximadamente media hora después, nuestras pesquisas nos llevaron a la plaza."


translate spanish epilogue_mi_9112918f:


    "Todas las cabañas estaban vacías...{w} Resultaba que tan sólo quedábamos cinco de nosotros en el campamento."


translate spanish epilogue_mi_594f012c:


    "Caminé con dificultad con aquella tubería mía."


translate spanish epilogue_mi_5326720a:


    "Al principio resultó ser bastante ligera, pero tras cargarla por un tiempo, aprendí que uno puede agotarse incluso cargando con una bolsa de plumas."


translate spanish epilogue_mi_0feb0847:


    dv "Dónde demonios están todos..."


translate spanish epilogue_mi_f5ac9909:


    "Alisa parecía estar más o menos tranquila.{w} El terror de los primeros minutos, cuando íbamos de una cabaña vacía a otra, ya había pasado."


translate spanish epilogue_mi_d5218c9a:


    my "Bueno, al menos podemos estar seguros de que no es el mismo supuesto... ¡tal como le ocurrió a Slavya!"


translate spanish epilogue_mi_59c3611a:


    dv "¿Y eso por qué?"


translate spanish epilogue_mi_b7312eb9:


    my "Sólo piensa... ¿Dónde están los cuerpos?"


translate spanish epilogue_mi_06fb3902:


    "Dije en un susurro, temeroso de asustarla."


translate spanish epilogue_mi_4b68666f:


    dv "Pero y entonces, ¿dónde está todo el mundo?"


translate spanish epilogue_mi_bb984b76:


    my "¿Desaparecidos...?"


translate spanish epilogue_mi_d01110d6:


    "Una explicación difícilmente creíble...{w} Pero si lo piensas, nada de lo que ha sucedido durante las últimas veinticuatro horas ha sido creíble."


translate spanish epilogue_mi_7c45825b:


    dv "No pueden haberse ido... así sin más..."


translate spanish epilogue_mi_d63d25c0:


    my "No parece que algo dependiera de nosotros ahora. Y, en cualquier caso, mañana será otro día."


translate spanish epilogue_mi_72f142cf:


    "Traté de poner una sonrisa."


translate spanish epilogue_mi_ba81381a:


    my "¡Volvamos de nuevo con los demás!"


translate spanish epilogue_mi_593113c9:


    dv "Claro..."


translate spanish epilogue_mi_01f64c2f:


    "Estuvo de acuerdo Alisa con una pizca de indecisión."


translate spanish epilogue_mi_413d048d:


    "Solamente habíamos dado un par de pasos, cuando un ruido se escuchó desde los arbustos de detrás."


translate spanish epilogue_mi_848599fc:


    "Al instante me di la vuelta y miré con atención hacia la fuente de origen del ruido."


translate spanish epilogue_mi_4b8f2dde:


    "Alisa se aferró a mi mano tan fuerte que me hizo daño, y yo con la otra la estreché más sobre la tubería."


translate spanish epilogue_mi_e0ac0d35:


    dv "¿Quién... va...?"


translate spanish epilogue_mi_326ad2f9_1:


    my "Ni idea..."


translate spanish epilogue_mi_6e7af154:


    "Forcé mi vista pero aun así no podía adivinar nada en la oscuridad."


translate spanish epilogue_mi_333411d8_1:


    my "¡Ey!"


translate spanish epilogue_mi_ffe234c7:


    "No tenía ni idea de qué hacer."


translate spanish epilogue_mi_c42d06d1:


    "El ruido de crujidos de repente se detuvo, y la noche retornó a su habitual jugueteo de estridulaciones."


translate spanish epilogue_mi_52152cc5:


    my "Podría ser algún animal salvaje..."


translate spanish epilogue_mi_fefef18f:


    "Me preguntaba con un sentimiento de inseguridad."


translate spanish epilogue_mi_42a83dbb:


    dv "¿Animales... salvajes?"


translate spanish epilogue_mi_42e4e478:


    "Ella me apretó la mano aun más estrechamente."


translate spanish epilogue_mi_049c7fdc:


    dv "¡Pero si no hay animales salvajes por aquí!"


translate spanish epilogue_mi_7996282d:


    my "Ehmmm, una ardilla, por ejemplo, no lo sé..."


translate spanish epilogue_mi_066810ef:


    dv "¿Una ardilla?"


translate spanish epilogue_mi_6ae4ca96:


    my "Claro, una ardilla."


translate spanish epilogue_mi_b95f9fd2:


    dv "Bueno, tal vez..."


translate spanish epilogue_mi_f87596bf:


    my "¿Debemos ir?"


translate spanish epilogue_mi_3b4b97c0:


    dv "Claro..."


translate spanish epilogue_mi_a20cefa7_17:


    "..."


translate spanish epilogue_mi_e258274a:


    "Un minuto después, estábamos aproximándonos a la cabaña de Olga Dmitrievna."


translate spanish epilogue_mi_103eb1f8:


    "Router y Lena permanecían en silencio, se dieron un ligero sobresalto al vernos."


translate spanish epilogue_mi_4e1a88c8:


    ro "Y bien, ¿cómo fue?"


translate spanish epilogue_mi_5b845686:


    my "No hay ni un alma."


translate spanish epilogue_mi_dcb7a8c7:


    un "¿Qué quieres decir conque... no hay ni un alma?"


translate spanish epilogue_mi_699e5734:


    dv "Que todo el mundo se fue..."


translate spanish epilogue_mi_ca44847e:


    "Dijo Alisa entre lágrimas."


translate spanish epilogue_mi_200d2256:


    "Las muchachas se abalanzaron entre sus brazos, y yo me senté al lado de Router, soltando al fin aquella aberrante tubería al suelo."


translate spanish epilogue_mi_e985afc7:


    ro "¿Es así de simple?"


translate spanish epilogue_mi_6a716cb7:


    my "Así de simple..."


translate spanish epilogue_mi_f347bf4c:


    ro "¿Pero cómo?"


translate spanish epilogue_mi_1659118a:


    my "Realmente no lo sé... ¿Quizás es una abducción alienígena?"


translate spanish epilogue_mi_8303b1d0:


    ro "¡Qué tontería!"


translate spanish epilogue_mi_eefcb0ea:


    my "¿Quieres jugar a ser detective, Sherlock?"


translate spanish epilogue_mi_e44817f8:


    ro "Bueno, podría ser el mismo maníaco..."


translate spanish epilogue_mi_0b807932:


    my "Algo así... como que es muy difícil para que sea solamente un hombre, ¿no lo crees también?"


translate spanish epilogue_mi_32b76f96:


    ro "Supongo que tienes razón... Pero quién... ¿qué puede ser?"


translate spanish epilogue_mi_a29f386e:


    my "Todavía podría resultar ser algún tipo de broma estúpida..."


translate spanish epilogue_mi_b5d68c97:


    ro "¿Y Slavya también? ¿Todo eso también fue una estúpida broma?"


translate spanish epilogue_mi_0b63e0e1:


    my "No, ella fue asesinada por alguien definitivamente."


translate spanish epilogue_mi_a1f1a290:


    ro "¿Pero quién?"


translate spanish epilogue_mi_2c5335a6:


    "Me escrutó con una mirada."


translate spanish epilogue_mi_f1897fd9:


    my "¡No tengo ni idea! ¡Tal vez fuiste tú!"


translate spanish epilogue_mi_262d7cae:


    "Comenzaba a perder los nervios."


translate spanish epilogue_mi_37e35222:


    ro "¡O tú!"


translate spanish epilogue_mi_699cf6d5:


    my "Yo no, eso seguro."


translate spanish epilogue_mi_9da52b6d:


    ro "¿Cómo puedes demostrarlo?"


translate spanish epilogue_mi_e3275cbc:


    my "¿Por qué demonios debería darte explicaciones a ti?"


translate spanish epilogue_mi_d7c7a76d:


    ro "¡Eres el único sin una coartada!"


translate spanish epilogue_mi_9bb0e846:


    my "Ah, así que tú tienes una coartada, ¿eh?"


translate spanish epilogue_mi_4bae00e6:


    ro "Estaba en mi cabaña por la noche... ¡Shurik puede probarlo!"


translate spanish epilogue_mi_76243b78:


    my "Seguro que sí, pero Shurik está temporalmente no disponible..."


translate spanish epilogue_mi_daca0afa:


    "Router apretó los dientes y se dio la vuelta."


translate spanish epilogue_mi_71d9105b:


    ro "De todas formas, soy el único con una coartada aquí. ¡No tengo que dar explicaciones a nadie! ¡Estoy seguro de mí mismo!"


translate spanish epilogue_mi_3b810b08:


    my "Es genial creer en ti mismo... ¡También yo estoy seguro de mí mismo! Y Miku puede demostrar que tengo razón... Estaba con ella todo el rato."


translate spanish epilogue_mi_c814e87f:


    ro "No precisamente el mejor testigo..."


translate spanish epilogue_mi_094c7520:


    my "¡Tan bueno como el tuyo!"


translate spanish epilogue_mi_74d118df:


    "No tengo ni idea de cuánto rato habríamos estado discutiendo de esa manera, si Lena no hubiese intervenido en la conversación."


translate spanish epilogue_mi_0284f524:


    un "Muchachos, venga, ya basta..."


translate spanish epilogue_mi_bf352153:


    my "Tienes razón, lo siento..."


translate spanish epilogue_mi_44cacce6:


    ro "Claro..."


translate spanish epilogue_mi_400b9c27:


    "Desde luego, ahora no era el mejor momento para estas cosas."


translate spanish epilogue_mi_9b909de6:


    my "En cualquier caso, deberíamos permanecer juntos..."


translate spanish epilogue_mi_a7fd75a5:


    dv "¿Ayudaría a evitar que desaparezcamos...?"


translate spanish epilogue_mi_0046a8da:


    "Preguntó Alisa tímidamente."


translate spanish epilogue_mi_70bbf125:


    my "No estoy seguro sobre todo ese asunto de las desapariciones, ¡pero al menos debería ayudarnos a permanecer vivos!"


translate spanish epilogue_mi_4e5b4343:


    "Eché un vistazo a mi reloj... ¡son casi las cuatro en punto!{w} El sol aparecerá pronto."


translate spanish epilogue_mi_709706dd:


    my "¡Muy bien, equipo! Es la hora de recoger el trigo, o acabaremos siendo eliminados."


translate spanish epilogue_mi_46466e3f:


    un "Pero..."


translate spanish epilogue_mi_35fb6c26:


    my "¡No hay peros!"


translate spanish epilogue_mi_71ed3f9b:


    "Nadie se opuso y entramos en silencio en la cabaña."


translate spanish epilogue_mi_cf4f8759:


    ma "¿Eh? ¿Qué ocurre...?"


translate spanish epilogue_mi_b6b544d2:


    "Parece que despertamos a Masha."


translate spanish epilogue_mi_1addbba1:


    my "Todo va bien, sigue durmiendo..."


translate spanish epilogue_mi_688bd005:


    "Pero ya estaba sentada en la cama y mirándonos fijamente de forma inexpresiva a nuestro pequeño grupo."


translate spanish epilogue_mi_bb72e793:


    ro "Bueno, verás, hay..."


translate spanish epilogue_mi_13d057b3:


    un "¡Ellos se fueron! ¡Desaparecieron! ¡Todo el mundo!"


translate spanish epilogue_mi_ae15ba1a:


    "Lena rompió a llorar y se arrojó con sus brazos rodeando el cuello de Masha."


translate spanish epilogue_mi_a35f21fb:


    ma "¿Qué? ¿Qué está sucediendo? ¿Podéis hablar con más claridad?"


translate spanish epilogue_mi_a22c92b6:


    my "Bueno, una larga historia que en resumen..."


translate spanish epilogue_mi_3a6935fd:


    "Me crugí el cuello y compulsivamente alcancé los cigarrillos de mi bolsillo. No hallando ninguno, expiré un suspiro de frustración y dije:"


translate spanish epilogue_mi_ccc382cd:


    my "Parece que solamente quedan cinco de nosotros en este campamento..."


translate spanish epilogue_mi_e8e8d6ba:


    ro "¡Querrás decir en esta habitación!"


translate spanish epilogue_mi_1f6dec3c:


    my "¡Qué alegría! ¡Capitán Obvio ha vuelto! ¡Sigues siendo el primero en darte cuenta!"


translate spanish epilogue_mi_6c21d46f:


    ro "¿Eh?"


translate spanish epilogue_mi_c12f9eb4:


    my "¡Oh, nada! ¿Creéis de verdad que es seguro que durmamos todos a la vez?"


translate spanish epilogue_mi_f1b10303:


    ro "Bueno, no lo es... pero..."


translate spanish epilogue_mi_d54d0a6f:


    my "¡Exacto!"


translate spanish epilogue_mi_20db871e:


    "Le di la tubería y, lentamente, me acerqué a la cama donde Lena estaba aun sollozando en los brazos de Masha."


translate spanish epilogue_mi_db756b30:


    my "Bueno, si no os importa..."


translate spanish epilogue_mi_1f2560d6:


    "Me quité las botas, me estiré dejando a un lado a las muchachas y me giré cara a la pared."


translate spanish epilogue_mi_844b71c4:


    ma "Pero cómo..."


translate spanish epilogue_mi_4576c932:


    my "Lo que sea... ¡A dormir!"


translate spanish epilogue_mi_03f7efc1:


    "Estaba muy exhausto... tanto físicamente y, más importante, emocionalmente... que era incapaz de hacer algo, pensar en algo, comunicarme con alguien."


translate spanish epilogue_mi_1dff787e:


    "Estaba desesperado por dormir un poco...{w} Demasiado desesperado..."


translate spanish epilogue_mi_e3998c81:


    "Los susurros de Masha, los sollozos de Lena, la voz de Alisa... todo aquello se fundió de alguna manera en una apacible nana, y así caí dormido."


translate spanish epilogue_mi_a20cefa7_18:


    "..."


translate spanish epilogue_mi_0540bbef:


    "Fui despertado a la fuerza por alguien, que hacía sus mejores intentos sacudiéndome."


translate spanish epilogue_mi_df4372df:


    "Sacudiéndome físicamente, eso es... Router estaba inclinado sobre mí y me empujaba por mis hombros."


translate spanish epilogue_mi_b9e79bf2_3:


    my "¿Queeé?"


translate spanish epilogue_mi_fc10bec9:


    ro "¡Es tu turno!"


translate spanish epilogue_mi_3e837789:


    my "¿Cuánto rato he estado durmiendo?"


translate spanish epilogue_mi_49aa35d6:


    ro "Como una hora."


translate spanish epilogue_mi_dab8428f:


    my "¿Solamente una hora? Aguanta un poco más..."


translate spanish epilogue_mi_2ddefabc:


    ro "Ya me estoy adormeciendo..."


translate spanish epilogue_mi_d3a7a6c6:


    "Claro, ¡una fuente de infinita energía no le haría ningún mal a nuestro Router!{w} Aunque tampoco me iría mal a mí también..."


translate spanish epilogue_mi_e9e6e7f9_1:


    my "Oh, está bien..."


translate spanish epilogue_mi_44cfa98c:


    "Me alcé con esfuerzo considerable, dejando atrás a Lena y Masha quienes dormían abrazadas la una con la otra, salté abajo al suelo."


translate spanish epilogue_mi_7f4ec5bf:


    "La habitación parecía ligeramente iluminada."


translate spanish epilogue_mi_edfb1f7d:


    my "El amanecer se acerca..."


translate spanish epilogue_mi_44cacce6_1:


    ro "Claro..."


translate spanish epilogue_mi_e2db42a2:


    "Router trató de llegar a mi lugar, pero le agarré por el cuello y le tiré al suelo, dándole una sábana y una almohada."


translate spanish epilogue_mi_004e3a87:


    ro "Vaya, tú puedes..."


translate spanish epilogue_mi_4d46ae5d:


    my "¡Y tú no puedes!"


translate spanish epilogue_mi_402485e7:


    "Vamos a pensar, ¿por qué no puede él...?{w} De todas maneras, no es tan importante ahora."


translate spanish epilogue_mi_12e4b3cb:


    "Agarré la silla, la arrastré hasta la puerta, me senté y empecé a esperar."


translate spanish epilogue_mi_1fa1a644:


    "«Anticipación» parecía ser la mejor palabra para describir lo que estaba sintiendo."


translate spanish epilogue_mi_e34cb82f:


    "En cualquier momento, la puerta se abriría de golpe y un maníaco rompería dentro de la habitación... O los últimos lugareños de este campamento desaparecerían...{w} O yo desaparecería..."


translate spanish epilogue_mi_c3a2c9f0:


    "Y en cualquier caso, algo ocurriría... sentí eso profundamente."


translate spanish epilogue_mi_cc495d0c:


    "Router estaba roncando a pierna suelta, las muchachas dormían firmemente, y yo estaba empezando a adormilarme también."


translate spanish epilogue_mi_5b9c71e5:


    "Aferré con más fuerza la tubería... la sensación del frío metal contra mi piel desnuda, de alguna forma me recuperó los sentidos."


translate spanish epilogue_mi_01c45f1f:


    "¡No debo dormirme!"


translate spanish epilogue_mi_cb07ce8b:


    "Soy su única esperanza ahora.{w} Y la mía también..."


translate spanish epilogue_mi_746b4d5c:


    "Pero es tan difícil resistirse...{w} Con tan solamente un par de minutos..."


translate spanish epilogue_mi_7d4bb7ad:


    "Mis ojos parecían tener su propia vida, abriéndose y cerrándose espontáneamente, en tanto mi mente divagaba."


translate spanish epilogue_mi_31518315:


    "Un crujido provino de detrás de la puerta.{w} No, debo haber imaginado eso... ¡serán sólo esos malditos saltamontes!"


translate spanish epilogue_mi_10788c07:


    "¿Hasta dónde están dispuestos a...?"


translate spanish epilogue_mi_b1e7167b:


    "¡¿Quizá son los saltamontes los culpables?! Cayeron sobre el campamento como una plaga de langostas y devoraron a todo el mundo..."


translate spanish epilogue_mi_0284a5b0:


    "Una visión de mí mismo siendo devorado por saltamontes surgió en mi mente...{w} Una desagradable tormenta se estaba destilando en mi cabeza."


translate spanish epilogue_mi_e4e546d0:


    "Sus largas patas, sus alas, una cabeza enorme y sus ojos... ¡llenos de tinieblas!"


translate spanish epilogue_mi_b49efc2e:


    "Ojos que reflejaban el abismo."


translate spanish epilogue_mi_2aa219d6:


    "Tan sólo observa un saltamontes en sus ojos... y ya no volverás a ser el mismo nunca más..."


translate spanish epilogue_mi_ce617998:


    "..."


translate spanish epilogue_mi_ed851b67:


    "Así es como sucede... ¡estoy corriendo, alejándome y ellos van detrás de mí! Hay miles de ellos, ¡millones!"


translate spanish epilogue_mi_e0b1f94a:


    "Voy saltando desesperadamente los agujeros y barrancos, ¡escalando montes y rocas!"


translate spanish epilogue_mi_e424f733:


    "A ellos les da igual... ¡tienen alas!{w} Claro, estas asquerosas alas que hacen ese sucio sonido."


translate spanish epilogue_mi_e39b3c47:


    "Probablemente no necesitarían sus dientes de todos modos... solamente tienen que agitar estas horribles alas más y más rápido, invocando una ola de sonido del apocalipsis."


translate spanish epilogue_mi_ba3e9f48:


    "¡No!{w} ¡Ni tan siquiera puedo tapar ese infernal ruido! Estoy cayendo, desperadamente trato de cubrirme los oídos con mis manos..."


translate spanish epilogue_mi_38997562:


    "¡Por favor, deteneos! Que alguien..."


translate spanish epilogue_mi_ce617998_1:


    "..."


translate spanish epilogue_mi_c7d7a7a5:


    "Me desperté con un dolor agudo en la parte posterior de la cabeza."


translate spanish epilogue_mi_9d40e697:


    "Abriendo mis ojos, me di cuenta de que estaba tumbado en el suelo."


translate spanish epilogue_mi_3529a363:


    "Resulta que debí dormirme y me caí de la silla..."


translate spanish epilogue_mi_4a584bb7:


    "Eran las nueve en punto."


translate spanish epilogue_mi_26659c8d:


    "Dios maldita sea, ¡es como si me hubiera dormido cinco horas seguidas!{w} ¡Cualquier cosa puede haber ocurrido!"


translate spanish epilogue_mi_fb4b0b46:


    "Horrorizado, me di unas palmaditas y, aparentemente, todo estaba en su lugar."


translate spanish epilogue_mi_13e9f254:


    "Ufff... Por poco..."


translate spanish epilogue_mi_f74f70f2:


    "Fui a despertar a nuestro equipo."


translate spanish epilogue_mi_18abd84b:


    ro "¿Ya es mi turno...?"


translate spanish epilogue_mi_70ab00a2:


    my "¡Dormiste demasiado! Mientras estaba aquí sentado... toda la noche..."


translate spanish epilogue_mi_6f311047:


    "Obviamente, eso era una mentira, pero contarle que me pasé la noche huyendo para salvar mi vida de unos saltamontes, definitivamente no era una opción."


translate spanish epilogue_mi_bcc18214:


    ro "Lo siento..."


translate spanish epilogue_mi_3d2c6bb8:


    "Router bajó su cabeza."


translate spanish epilogue_mi_5f6d175a:


    my "No te preocupes..."


translate spanish epilogue_mi_7a8ced10:


    ma "Ahhmm, estoy terriblemente hambrienta."


translate spanish epilogue_mi_6f990f62:


    "Masha se estiró y me hizo una dulce sonrisa."


translate spanish epilogue_mi_49c498a0:


    ma "¿Y qué hacéis todos aquí?"


translate spanish epilogue_mi_bae123b1:


    un "¿No te acuerdas de nada?"


translate spanish epilogue_mi_7556a267:


    ma "Nop, ¿qué pasa?"


translate spanish epilogue_mi_699e5734_1:


    dv "Todos desaparecieron..."


translate spanish epilogue_mi_e38a29fe:


    "Dijo Alisa con pesimismo."


translate spanish epilogue_mi_20f54ffc_1:


    ma "¿Qué dices?"


translate spanish epilogue_mi_70a3cd5c:


    dv "Que todo el mundo desapareció... se desvaneció en la nada..."


translate spanish epilogue_mi_d79f8f4c:


    "Masha observó a través de la ventana y no dijo nada.{w} Parece que ahora lo recordó."


translate spanish epilogue_mi_6cc461d6:


    my "Bueno... ¡Ir a por algo de comer sería, desde luego, una buena idea!"


translate spanish epilogue_mi_0f0c21bc:


    "Nadie se iba a negar."


translate spanish epilogue_mi_a624eeb3:


    "No es que estuviera de humor para comer, igual que cualquier otro, pero últimamente todos sabían que teníamos que estar huyendo."


translate spanish epilogue_mi_a20cefa7_19:


    "..."


translate spanish epilogue_mi_f13a4b32:


    "Bien pronto, estábamos sentados en la cantina, desayunando en silencio lo que fuera que pudiéramos hallar."


translate spanish epilogue_mi_a9c1d18e:


    ma "¿Queréis que cocine alguna cosa?"


translate spanish epilogue_mi_22f7f7dc:


    "Preguntó en voz baja Masha."


translate spanish epilogue_mi_f862db93:


    my "Esperemos hasta la hora de comer. Por ahora está bien, reposemos."


translate spanish epilogue_mi_1356cf00:


    "Me hizo una mirada decepcionada."


translate spanish epilogue_mi_695687ee:


    ro "Vale, y bien, ¿qué vamos hacer?"


translate spanish epilogue_mi_c3a696ad:


    my "Tenemos que escapar de aquí."


translate spanish epilogue_mi_13582c80:


    un "Pero la policía..."


translate spanish epilogue_mi_a12d2714:


    my "¡Tenemos que escapar de aquí!"


translate spanish epilogue_mi_5254c792:


    "Repetí tercamente."


translate spanish epilogue_mi_e1353452:


    dv "¿Pero cómo?"


translate spanish epilogue_mi_06988cc4:


    my "El autobús."


translate spanish epilogue_mi_c6062e4b:


    dv "Es bastante improbable que alguno llegue. No suelen venir a menudo..."


translate spanish epilogue_mi_688cf4d4:


    my "Al menos, debería de haber algún cartel de horarios por ahí."


translate spanish epilogue_mi_d1c129fd:


    "Traté de recordar si había visto el mapa del autobús o el cartel de horarios en alguna parte.{w} Probablemente no..."


translate spanish epilogue_mi_989754e9:


    my "¡Entonces iremos a pie! ¡Cualquier cosa es mejor que permanecer aquí!"


translate spanish epilogue_mi_91dba49d:


    un "Pero es una larga caminata..."


translate spanish epilogue_mi_5d634972:


    my "Bueno, podríamos ir a dedo, autoestop."


translate spanish epilogue_mi_8d827cef:


    ro "Aquí, ¿en medio de la nada? Lo dudo..."


translate spanish epilogue_mi_37160b7d:


    my "¡Oh, parad ya con ese drama!"


translate spanish epilogue_mi_df404933:


    "Aunque pensándolo bien, era el momento idóneo para hacer un drama..."


translate spanish epilogue_mi_d94b9a8c:


    ro "No estoy siendo dramático, pero..."


translate spanish epilogue_mi_dec09dcb:


    my "Ya está, fin de la historia."


translate spanish epilogue_mi_739f8c27:


    dv "¿Qué vamos a decir cuando lleguemos a la ciudad?"


translate spanish epilogue_mi_3893f203:


    my "Les contaremos la verdad... que vinimos a rodar una película y luego..."


translate spanish epilogue_mi_85899043:


    ro "¿Qué película?"


translate spanish epilogue_mi_c2b1462d:


    my "Vale, dejad ya de hacer el tonto, ¿de acuerdo?"


translate spanish epilogue_mi_4cb424ce:


    "Enterré mi rostro en mis manos, sintiéndome de capa caída."


translate spanish epilogue_mi_aa01c424:


    my "Incluso si ayer pudo ser gracioso (aunque en realidad no lo fue), ¡ya podéis parar ahora!"


translate spanish epilogue_mi_535db658:


    ro "No te entiendo..."


translate spanish epilogue_mi_861228df:


    my "Oh, ¿todavía vas a seguir simulando que viniste aquí, no para rodar una película, sino para hacer el papel de un pionero?"


translate spanish epilogue_mi_4c950402:


    dv "No comprendo nada..."


translate spanish epilogue_mi_c4b533d9:


    "Alisa contemplaba a todos de un lado a otro con su mirada sorprendida."


translate spanish epilogue_mi_f3ebcebc:


    dv "Pero nosotros somos pioneros..."


translate spanish epilogue_mi_0b861b18:


    ma "¡¿Pero qué pioneros por el amor de Dios?!"


translate spanish epilogue_mi_8fdf19ba:


    "Masha, que había permanecido en silencio todo el rato, se entrometió."


translate spanish epilogue_mi_b017a002:


    ma "¡Estamos en el siglo XXI! ¡Claro que sí, pioneros!"


translate spanish epilogue_mi_2eff6d6b:


    dv "Veinti..."


translate spanish epilogue_mi_8eeb0718:


    ro "¿Uno?"


translate spanish epilogue_mi_3e7184e9:


    "Parece que estamos, claramente, en un auténtico desastre..."


translate spanish epilogue_mi_6a191ff8:


    my "Y bien, ¿vais a decir todos que estáis en... bueno, que somos... pioneros normales que llegaron aquí, para pasar unas vacaciones de una semana o así?"


translate spanish epilogue_mi_d30e024c:


    "Un pesado silencio se hizo."


translate spanish epilogue_mi_5f2878d9:


    dv "Bueno... sí..."


translate spanish epilogue_mi_01ce7e3e_1:


    "Le hice un vistazo a Masha."


translate spanish epilogue_mi_9f98315f:


    "Es como si ella tampoco supiera cómo reaccionar."


translate spanish epilogue_mi_91e2a42f:


    "La señalé, y le indiqué la puerta."


translate spanish epilogue_mi_547acb5a:


    my "Disculpadnos por un segundo..."


translate spanish epilogue_mi_296d110b:


    "Habiendo salido fuera, le pregunté:"


translate spanish epilogue_mi_9397443d:


    my "¿Qué te parece?"


translate spanish epilogue_mi_697b0ee3:


    ma "Bueno..."


translate spanish epilogue_mi_20e75221:


    "Adoptó la pose del antiguo Pensador griego."


translate spanish epilogue_mi_8d1032a5:


    ma "Es como si creyeran genuinamente en eso. Y unos compañeros, llamados Semyon y Miku, estaban en nuestro lugar hasta ayer."


translate spanish epilogue_mi_b74846e4:


    my "Algo así..."


translate spanish epilogue_mi_94ce0421:


    ma "Bueno, ¡supongo que tú estás más familiarizado con ellos que yo!"


translate spanish epilogue_mi_b78f3341:


    my "¡Oh, venga ya!"


translate spanish epilogue_mi_e5e97d5d_1:


    "Le interrumpí."


translate spanish epilogue_mi_2e070fe6:


    my "¿Podrías tener alguna teoría más factible?"


translate spanish epilogue_mi_bc6cebcc:


    ma "Aun no tengo ni una teoría... no sé todavía qué pensar..."


translate spanish epilogue_mi_b4d52943:


    my "Yo tampoco..."


translate spanish epilogue_mi_f6aad95c:


    ma "Pero, de todas formas, ¡todo el infierno se está desatando aquí incluso sin tener en cuenta los pioneros! ¡Asesinatos! ¡Docenas de personas desapareciendo sin rastro! Eso tiene que ser nuestra prioridad más importante ahora."


translate spanish epilogue_mi_c7c73a32:


    "Miré de cerca a Masha."


translate spanish epilogue_mi_fb6ae8d4:


    "Me pregunto cómo puede ella mantener la mente tan fría en semejantes circunstancias... Por supuesto, yo también trato de mantener la calma, pero..."


translate spanish epilogue_mi_144a299a:


    ma "Eso significa..."


translate spanish epilogue_mi_b9e79bf2_4:


    my "¿Qué?"


translate spanish epilogue_mi_1a88a249:


    ma "Dejemos este asunto para más tarde."


translate spanish epilogue_mi_7b4c77c1:


    my "Pero, ¿y al menos echando a ojo?"


translate spanish epilogue_mi_40161182:


    ma "Supongo que de alguna forma tiene que estar conectado con las desapariciones. Es más que probable asumir que una... o incluso dos o tres... los humanos no podrían hacerlo con tanta gente a la vez..."


translate spanish epilogue_mi_ad288150:


    my "Estoy de acuerdo."


translate spanish epilogue_mi_14d957a2:


    ma "Puede ser que este fenómeno de alguna forma les haya afectado a ellos."


translate spanish epilogue_mi_71666196:


    my "Podría ser..."


translate spanish epilogue_mi_1677698d:


    ma "De todas maneras, es más fácil creer en eso que, por ejemplo, creer que estamos en una realidad paralela o algo así."


translate spanish epilogue_mi_0466237c:


    my "Sabes, por ahora estoy dispuesto a creer cualquier cosa..."


translate spanish epilogue_mi_63797f2e:


    ma "Pues por ahora, aceptemos esta teoría."


translate spanish epilogue_mi_a27cf3ba:


    my "Vale..."


translate spanish epilogue_mi_f60fe5df:


    ma "¿Nos vamos?"


translate spanish epilogue_mi_16ade63c:


    my "Ve delante, vuelvo en un minuto."


translate spanish epilogue_mi_d0722fc6:


    ma "Es peligroso estar por ahí solo..."


translate spanish epilogue_mi_23baa87a:


    my "En serio, ¿qué es tan peligroso? A plena luz del día y todo el mundo por aquí."


translate spanish epilogue_mi_5a3ed4bf:


    ma "Vale."


translate spanish epilogue_mi_036327ef:


    "Me hizo una sonrisa apenas visible, y volvió hacia la cantina."


translate spanish epilogue_mi_57d8a15d:


    "Aun así, su comportamiento seguía siendo bastante raro."


translate spanish epilogue_mi_c1100f55:


    "¡Es como si los últimos pocos días hubieran cambiado todo y a todos!{w} Incluyendo a Masha..."


translate spanish epilogue_mi_deca8d3b:


    "Ayer... trastornada, con miedo y temblando. Hoy... fría y con razonamiento sensato."


translate spanish epilogue_mi_8cd31d45:


    "¡Maldita sea! ¡Incluso ella está más segura de sí misma que yo!"


translate spanish epilogue_mi_55e9d52c:


    "¿Puede ser que sea simplemente el haber dormido bien?{w} No, ¡también hay algo que va mal con ella!"


translate spanish epilogue_mi_968e3ce8:


    "Obviamente, ella no cree ser un pionero como los demás, pero aun así..."


translate spanish epilogue_mi_750bd51e:


    "Si alguna cosa les ha ocurrido a ellos, entonces quizá también me pueda suceder a mí..."


translate spanish epilogue_mi_1df2225f:


    "¡No! No, no puede ser, todo parece ir bien..."


translate spanish epilogue_mi_5c2c6e71:


    "Volví a revisar mi cuerpo y no encontré nada fuera de lo normal."


translate spanish epilogue_mi_361e4259:


    "Vale, tengo que mantener la calma y autocontrolarme..."


translate spanish epilogue_mi_b72dfe95:


    "Me levanté y estaba a punto de retornar a la cantina cuando, de repente, escuché una pequeñita voz detrás de mí."


translate spanish epilogue_mi_45e11c59:


    "Como si alguien me estuviera llamando."


translate spanish epilogue_mi_20953507:


    "Bajé del porche y di unos cuantos pasos en dirección hacia la voz."


translate spanish epilogue_mi_9f8b3255:


    "Alguien me estaba espiando desde detrás de un árbol."


translate spanish epilogue_mi_a19d3e12:


    "Era Ulyana.{w} No, no era Ulyana, sino alguien que se parecía a ella..."


translate spanish epilogue_mi_a9617350:


    "Me detuve en seco."


translate spanish epilogue_mi_30c877bb:


    "La mitad de su rostro estaba rasgado y colgaba en jirones. Su mandíbula en una sonrisita que temblaba nerviosamente, como si fuera una carcajada. Un ojo inyectado en sangre, medio colgando de su cuenca, repentinamente giró su pupila para mirarme a mí."


translate spanish epilogue_mi_70b8148d:


    "Dejé de respirar.{w} Sentí como si fuera a morir justo ahora debido a esa conmoción."


translate spanish epilogue_mi_e5b6bc80:


    us "Semyon..."


translate spanish epilogue_mi_b5deb76e:


    "La cosa que se parecía a Ulyana me estaba llamando."


translate spanish epilogue_mi_5da5e5ae:


    "Las fuerzas de mis piernas empezaban a abandonarme."


translate spanish epilogue_mi_85dad370:


    "Itenté no desplomarme, traté de gritar, traté de huir..."


translate spanish epilogue_mi_c9e0ed8c:


    "Pero mi consciencia ya me había abandonado el cuerpo que ahora se convirtió en tan sólo un muñeco de trapo, movido por la voluntad de un titiritero invisible."


translate spanish epilogue_mi_118f2951:


    "Un segundo más y..."


translate spanish epilogue_mi_bfc45034:


    ma "¡Ey!"


translate spanish epilogue_mi_dca1260c:


    "Instintivamente me di la vuelta hacia aquella voz, capté la visión de Masha, de pie en el porche, y luego al instante volví a mirar a «Ulyana»."


translate spanish epilogue_mi_5007383a:


    "Pero ya se había ido..."


translate spanish epilogue_mi_d04071e3:


    "Masha se dio cuenta de mi rostro pálido mortecino y saltó corriendo hacia mí en un abrir y cerrar de ojos."


translate spanish epilogue_mi_49a38e33:


    ma "¿Qué? ¿Qué sucedió?"


translate spanish epilogue_mi_1c8f07f7:


    my "Yo... sólo vi a Ulyana... Ahí..."


translate spanish epilogue_mi_02db51d2:


    "Señalé con mi dedo hacia el árbol."


translate spanish epilogue_mi_f7f58c4b:


    "Por supuesto, la visión de Slavya mutilada fue horrible..."


translate spanish epilogue_mi_c633ec22:


    "Pero al menos estaba muerta y no caminando por ahí a plena luz del día con su rostro rasgado a pedazos, y ella por cierto tampoco me llamaba por mi nombre (incluso aunque no fuera mi propio nombre)."


translate spanish epilogue_mi_3c3e173c:


    "La última pizca de mis fuerzas desertaron y me derrumbé sobre mis rodillas."


translate spanish epilogue_mi_d3701ebe:


    ma "¿Qué? ¿Dónde?"


translate spanish epilogue_mi_5217a1fa:


    "Masha me sacudía por los hombros."


translate spanish epilogue_mi_b957be9d:


    my "No era ella... Eso era... No lo sé..."


translate spanish epilogue_mi_9bf74aca:


    ma "¡Contrólate!"


translate spanish epilogue_mi_409bc2a3:


    "Sentí una bofetada suya en la cara y Masha me hizo una mirada inquisitiva."


translate spanish epilogue_mi_32cd69c5:


    "Mi mejilla ardía, pero lentamente comencé a recobrar la compostura."


translate spanish epilogue_mi_e5d90224:


    my "Ulyana estaba allí... con su rostro...{w} Básicamente, era un zombie. Sí, un zombie de Ulyana. ¡Esa es la explicación más razonable que puedo dar!"


translate spanish epilogue_mi_f159f577:


    "Masha me miraba boquiabierta."


translate spanish epilogue_mi_ddba048a:


    ma "Cualquier cosa puede ocurrir aquí..."


translate spanish epilogue_mi_0f8b803f:


    my "Si la hubieras visto por ti misma..."


translate spanish epilogue_mi_cbdc4e80:


    ma "¡Gracias a Dios que no! Vamos venga, ¡te traeré algo de agua!"


translate spanish epilogue_mi_a20cefa7_20:


    "..."


translate spanish epilogue_mi_33498a6c:


    "Describí mi encuentro con aquella criatura con todo lujo de detalle a los demás."


translate spanish epilogue_mi_3eab3cd4:


    ro "Puedo decirte una cosa... ¡la ciencia no puede explicar lo que sea que esté ocurriendo aquí de momento!"


translate spanish epilogue_mi_db0c1724:


    dv "¿La ciencia? ¡Es una furia malvada lo que está sucediendo aquí! ¡La mano de Satanás! Desearía tener una iglesia a mano... para encender una velita..."


translate spanish epilogue_mi_6a561d69:


    "Lena no dijo nada, recurrió a un sollozo silencioso."


translate spanish epilogue_mi_1a7c1a1e:


    "Casi estaba completamente recuperado, ahora sería capaz de pensar con claridad."


translate spanish epilogue_mi_0a4cb391:


    my "¡Ahora está perfectamente claro que tenemos que escapar de aquí!"


translate spanish epilogue_mi_83ad4a9c:


    ro "Bueno, no soy un científico de verdad... Y mucho menos un psíquico de lo paranormal, pero, si recuerdo correctamente, todos estos demonios y espíritus están vinculados con un cierto lugar. Por tanto, si nos alejamos de ese lugar..."


translate spanish epilogue_mi_f6b9796e:


    ma "¡Vaya, esa es una excelente solución!"


translate spanish epilogue_mi_be002e4f:


    "Masha sonrió escépticamente."


translate spanish epilogue_mi_ad288150_1:


    my "Estoy de acuerdo."


translate spanish epilogue_mi_605106a3:


    "Exclamé, ignorándola."


translate spanish epilogue_mi_f8f4d1e6:


    dv "¡Claro!"


translate spanish epilogue_mi_bd8d303c:


    "Lena simplemente hizo un apenas perceptible asentimiento con la cabeza."


translate spanish epilogue_mi_375aadac:


    my "Pues, ¡ya está todo decidido!"


translate spanish epilogue_mi_0505734e:


    "Di un golpe en la mesa con mi puño y me levanté."


translate spanish epilogue_mi_ae1a89b2:


    ma "¡Quieto! No estamos todos de acuerdo con eso..."


translate spanish epilogue_mi_cd0d8c85:


    my "¡¿Por qué iríamos a dudar?! ¡En serio, no quiero un segundo encuentro con aquella cosa!"


translate spanish epilogue_mi_a7480fc6:


    ma "¿Adónde vamos? ¿Te das cuenta de lo lejos que puede estar alcanzar la ciudad a pie?"


translate spanish epilogue_mi_e0790bc4:


    my "Maldita sea, ¿de verdad crees que nadie conduce por la carretera? Un vehículo, un autobús... lo que sea, ¡ya encontraremos algo!"


translate spanish epilogue_mi_26c4382d:


    ro "No estaría tan seguro..."


translate spanish epilogue_mi_b9590f03:


    my "Entonces, ¿crees que es mejor que permanezcamos aquí?"


translate spanish epilogue_mi_ee0e2d60:


    ro "Bueno, no dije eso..."


translate spanish epilogue_mi_59962f16:


    un "Da miedo aquí... Especialmente por la noche..."


translate spanish epilogue_mi_e2ce5d0c:


    "susurró en voz baja Lena."


translate spanish epilogue_mi_8690ec90:


    ma "¡Eso no significa que tengamos que ir a cualquier parte!"


translate spanish epilogue_mi_3cd95137:


    "Dijo Masha a gritos."


translate spanish epilogue_mi_395daf08:


    "Le hice una mirada de cerca.{w} No, algo había cambiado en ella de eso estaba seguro."


translate spanish epilogue_mi_c748f801:


    "Quizá debería tener una amable charla con ella sobre ello."


translate spanish epilogue_mi_c2ecd2a9:


    "¿O tal vez no debería?{w} Quizás ella también..."


translate spanish epilogue_mi_a8506c23:


    my "Vale, ¡hagamos una votación!"


translate spanish epilogue_mi_f2f360c8:


    "Dije en un tono bastante seguro de mí mismo, a pesar de que aun así tenía mis dudas."


translate spanish epilogue_mi_f988541d:


    "¿Y si Masha tenía razón? ¿Quizá no deberíamos estar huyendo sin rumbo por ahí?"


translate spanish epilogue_mi_0c03293b:


    "Por un lado, no tengo ni idea de qué nos espera más allá de las puertas de este campamento.{w} Por otro lado... esto es realmente un infierno quedarse aquí sin más..."


translate spanish epilogue_mi_40ef1a37_2:


    ma "Vale..."


translate spanish epilogue_mi_b53b2a58:


    "Ella se encogió de hombros."


translate spanish epilogue_mi_0a36094e:


    "Y aun así, ¡no me quedaré ni un minuto más aquí!"


translate spanish epilogue_mi_5f20a77f:


    my "¡Irnos!"


translate spanish epilogue_mi_2b34d1a9:


    ma "Quedarnos."


translate spanish epilogue_mi_f4738552:


    dv "Intentémoslo... tal vez, con suerte, conseguimos subirnos a algo."


translate spanish epilogue_mi_5bf88fe1:


    ro "Irnos..."


translate spanish epilogue_mi_a7287f96:


    "Todos fijaron sus miradas en dirección a Lena."


translate spanish epilogue_mi_151b8dfc:


    un "No quiero quedarme aquí..."


translate spanish epilogue_mi_4e16488f:


    "Parecía que mi decisión no iba a afectar el resultado de todas formas."


translate spanish epilogue_mi_1521e234:


    "Así que, ¡nos vamos!"


translate spanish epilogue_mi_e130e273:


    my "¡Muy bien!"


translate spanish epilogue_mi_3059b01f:


    "Me incorporé de golpe en pie. Todos seguían sentados con sus cabezas gachas."


translate spanish epilogue_mi_d04a0580:


    my "En tanto que escogimos..."


translate spanish epilogue_mi_bc561ca0:


    ma "Bueno, ya que escogimos..."


translate spanish epilogue_mi_7b696593:


    "Masha se levantó lentamente y me miró."


translate spanish epilogue_mi_5638fcf6:


    dv "¿Y qué hay de nuestro equipaje?"


translate spanish epilogue_mi_97d81d27:


    "Preguntó Alisa incómodamente."


translate spanish epilogue_mi_445fb36b:


    ro "¿Crees en serio que deberíamos preocuparnos por eso ahora?"


translate spanish epilogue_mi_ea7b081f:


    "Murmuró Router con pesimismo."


translate spanish epilogue_mi_31933f01:


    dv "Bueno, probablemente tengas razón..."


translate spanish epilogue_mi_75139ff0:


    "Bueno, en mi caso, mis cosas ya se desvanecieron en alguna parte..."


translate spanish epilogue_mi_a20cefa7_21:


    "..."


translate spanish epilogue_mi_ede53218:


    "En pocos minutos estábamos en la parada de autobús."


translate spanish epilogue_mi_6049b2ea:


    ma "La ciudad está en esa dirección."


translate spanish epilogue_mi_38c29669:


    "Masha indicó con un gesto hacia el horizonte."


translate spanish epilogue_mi_ec1be65f:


    my "Fue justo anteayer..."


translate spanish epilogue_mi_fbd62a8b_1:


    "Sonreí."


translate spanish epilogue_mi_67d59d3c:


    ro "Claro, por esa dirección."


translate spanish epilogue_mi_4264eb79:


    "Confirmó Router."


translate spanish epilogue_mi_283c52cb:


    "Nos quedamos quietos en silencio por unos instantes, y luego lentamente nos dirigimos hacia nuestra destinación."


translate spanish epilogue_mi_a20cefa7_22:


    "..."


translate spanish epilogue_mi_a20cefa7_23:


    "..."


translate spanish epilogue_mi_97183294:


    "El sol veraniego era insoportablemente abrasador, haciendo que el asfalto se fundiera."


translate spanish epilogue_mi_c0fe229c:


    "Todo a unos pocos cientos de metros más allá... la carretera, los árboles, la hierba, el cielo... se fundía en una brillante calina que se parecía más bien a una neblina ardiente."


translate spanish epilogue_mi_f47addae:


    "El sudor se me caía y empezaba a pensar que iba a tener una insolación."


translate spanish epilogue_mi_770d11e5:


    "Seguramente habría sido una idea genial traer más agua con nosotros, pero antes cuando estábamos partiendo, la única cosa en la cual podíamos pensar era en sacar nuestros traseros tan lejos como fuera posible de aquel maldito campamento."


translate spanish epilogue_mi_4d43cd01:


    "De un momento a otro sentí que estaba a punto de perder la conciencia."


translate spanish epilogue_mi_7aadfff4:


    "Únicamente la miserable vibración de los saltamontes me mantuvo en contacto con la realidad."


translate spanish epilogue_mi_c7b1fe99:


    "¡Por supuesto!{w} Este infernal mediodía era, desde luego, su parte favorita del día."


translate spanish epilogue_mi_3e8862d2:


    "No se podían preocupar del calor abrasador, de la cero humedad o de la falta de ráfagas de aire."


translate spanish epilogue_mi_9a28a9d2:


    "Por el contrario, se sentían estupendos... era el perfecto telón de fondo para su acto final de su última simfonía del apocalipsis..."


translate spanish epilogue_mi_13fa06ef:


    "Probablemente deberíamos habernos quedado en el campamento después de todo."


translate spanish epilogue_mi_abedaf26:


    "O al menos, deberíamos haber empezado esta caminata más tarde, casi al atardecer."


translate spanish epilogue_mi_5a9c692b:


    "Aunque vagar por la carretera desconocida por la noche tampoco era muy buena idea..."


translate spanish epilogue_mi_7ea693c4:


    "El sonido de un llanto reprimido provino detrás de mí."


translate spanish epilogue_mi_3c7e799a:


    "Un centenar de metros más allá, Alisa estaba sosteniendo a duras penas a Lena, aguantándola antes de que se desplomara al suelo."


translate spanish epilogue_mi_aec78f4c:


    "Corrí hacia ellas."


translate spanish epilogue_mi_1d810d31:


    my "¿Qué ocurre?"


translate spanish epilogue_mi_ea512261:


    dv "¡Se ha desmayado!"


translate spanish epilogue_mi_ad5a6d31:


    "Le ayudé a cargar con Lena hasta el arcén. Había al menos una sombra de una pequeña arboleda."


translate spanish epilogue_mi_ac1911e9:


    ma "Y esto concluye con nuestra caminata..."


translate spanish epilogue_mi_6a8b4df5:


    "Dijo Masha con humor pesimista, hundiéndose en la hierba."


translate spanish epilogue_mi_916de10c:


    ma "¡Te avisé!"


translate spanish epilogue_mi_348aff25:


    my "Está bien, pero y eso que cambia ahora..."


translate spanish epilogue_mi_b117497a:


    "Estuvimos viajando por lo menos un par de horas, por lo que pensar en volver al campamento no tenía sentido.{w} Bueno, al menos hasta el atardecer, cuando el calor cese un poco."


translate spanish epilogue_mi_faf254ea:


    ro "Y todavía no hemos visto ni un solo vehículo..."


translate spanish epilogue_mi_864e06fe:


    "Me giré y miré a Router.{w} Se quitó la camisa y comenzó a escurrir el sudor de ésta."


translate spanish epilogue_mi_7ba1bf38:


    "Alisa estaba tratando de reanimar a Lena."


translate spanish epilogue_mi_38622444:


    my "Si esa insolación..."


translate spanish epilogue_mi_f913a1de:


    dv "¡Para!"


translate spanish epilogue_mi_c8f00552:


    "Me gritó."


translate spanish epilogue_mi_25c82915:


    "Si eso era una insolación, entonces estábamos en graves problemas... ella no podría ir a ninguna parte."


translate spanish epilogue_mi_961bbfe5:


    "Caminé por la carretera, protegí mis ojos del sol con la palma de la mano y me fijé en la distancia."


translate spanish epilogue_mi_fdbd24c7:


    "No había nada salvo el mismo camino de asfalto recorriendo hasta más allá del horizonte, sin señal alguna de civilización.{w} O de cualquier signo de presencia humana."


translate spanish epilogue_mi_3444bdc0:


    "Era como si fuéramos las únicas personas que quedaran, no solamente en el campamento, sino en todo el mundo."


translate spanish epilogue_mi_c43dec15:


    "Aunque si lo piensas bien, todo lo que había sucedido antes, no es en verdad una gran sorpresa."


translate spanish epilogue_mi_3915eb70:


    "Volví con los demás y me senté en un tronco."


translate spanish epilogue_mi_46d4b9bb:


    my "Supongo que tenemos que explorar más adelante."


translate spanish epilogue_mi_4c4c5aba:


    "Miré a Router."


translate spanish epilogue_mi_750d0f10:


    my "Para comprobar qué hay más allá... Quizá podríamos encontrar algo... Y de mientras, vosotras permaneced aquí."


translate spanish epilogue_mi_5a93ab85:


    dv "Pero..."


translate spanish epilogue_mi_1ef5403c:


    my "Ella no puede ir a ninguna parte ahora mismo de todas maneras."


translate spanish epilogue_mi_2ff95471:


    "Lena estaba tumbada con sus ojos cerrados y jadeando aire."


translate spanish epilogue_mi_202c17a8:


    ma "Vale, adelante."


translate spanish epilogue_mi_5dd3dee9:


    "Dijo Masha con calma."


translate spanish epilogue_mi_3b32e781:


    "Justo se me ocurrió que tendría que tener una conversa con ella...{w} Pero, definitivamente, no es momento de eso ahora."


translate spanish epilogue_mi_5175cd14:


    my "¿Vamos?"


translate spanish epilogue_mi_250c1da5:


    "Router se encogió de hombros y me siguió."


translate spanish epilogue_mi_a20cefa7_24:


    "..."


translate spanish epilogue_mi_f402d84a:


    "Estuvimos caminando unos quince minutos. Las muchachas estaban bien lejos ya."


translate spanish epilogue_mi_9aa33312:


    ro "¿Qué crees? ¿Alguna posibilidad de encontrarnos con alguien?"


translate spanish epilogue_mi_a3533961:


    "Preguntó en voz baja."


translate spanish epilogue_mi_9ef048f4:


    my "¡No lo sé!"


translate spanish epilogue_mi_987deb91:


    "Contesté con un tono enojado... Realmente quería abstenerme de hablar ahora."


translate spanish epilogue_mi_0a3b55b3:


    "Los campos sin fin se extendían por el horizonte, esparcidos con escasas áreas de árboles y colinas."


translate spanish epilogue_mi_e710db3b:


    "El paisaje se veía tan típico y tranquilo que, de hecho, causaba inquietud."


translate spanish epilogue_mi_68224858:


    "Si nos encontráramos en la llanura del Infierno, por lo menos esperaríamos ser atacados desde cualquier parte, por lo que estaríamos preparados para abrazar lo peor."


translate spanish epilogue_mi_ebc7f617:


    "Ahora nos sentimos simplemente como pequeñas gotitas de pintura en el lienzo de un pintor anónimo, el cual pintó el verano en el centro de Rusia.{w} O donde fuera que estuviéramos ahora..."


translate spanish epilogue_mi_b17071ab:


    "Todo esto hacía que nuestro miedo fuera más fuerte..."


translate spanish epilogue_mi_6ae2ce2f:


    ro "¡Mira!"


translate spanish epilogue_mi_631bd070:


    "Gritó Router."


translate spanish epilogue_mi_5b9ca325:


    "Forcé la vista y vi que la carretera daba una vuelta."


translate spanish epilogue_mi_d049f728:


    my "¿Y qué tiene de especial?"


translate spanish epilogue_mi_0d705a9c:


    ro "¡Tiene que haber alguien por ahí!"


translate spanish epilogue_mi_7e25d624:


    my "¿Qué te hace pensar eso?"


translate spanish epilogue_mi_15aa17c6:


    ro "Bueno, ya que hay una curva... Hemos estado caminando en línea recta todo el tiempo hasta ahora..."


translate spanish epilogue_mi_ae92f472:


    my "Vale, vayamos y echemos un vistazo."


translate spanish epilogue_mi_36b49d85:


    "En pocos minutos llegamos finalmente...{w} a las puertas del campamento de pioneros «Sovyonok»..."


translate spanish epilogue_mi_971b05e7:


    "Me derrumbé en el bordillo y enterré mi cara entre mis manos."


translate spanish epilogue_mi_0fd2ac9d:


    "No sentía miedo. De hecho, no sentía ninguna emoción para nada, tan sólo la fatiga y el vacío en mi interior."


translate spanish epilogue_mi_bb76d279:


    ro "Pero... pero... ¡¿cómo?!"


translate spanish epilogue_mi_9840e224:


    "Murmuró Router."


translate spanish epilogue_mi_429571ae:


    my "Ni idea...{w} A parte, ¿qué más da? No es más extraño que cualquier otra cosa que me haya ocurrido en los últimos días."


translate spanish epilogue_mi_550ecf00:


    "Le miré, sintiéndome exhausto."


translate spanish epilogue_mi_9f5fe769:


    "Los labios de Router estaban temblando, una expresión de horror mortal se dibujaba en su rostro."


translate spanish epilogue_mi_2bf4d850:


    my "Volvamos y recojamos a las muchachas."


translate spanish epilogue_mi_ca7fc9e6:


    "No le esperé y sencillamente volví de vuelta arrastrándome donde las dejamos."


translate spanish epilogue_mi_a20cefa7_25:


    "..."


translate spanish epilogue_mi_9c57ac94:


    ma "¿Encontraste algo?"


translate spanish epilogue_mi_407fb42e:


    "Preguntó Masha, dando un mordisco a unas fresas de origen desconocido."


translate spanish epilogue_mi_e5032065:


    my "Encontramos... un campamento..."


translate spanish epilogue_mi_25d0d643:


    ma "¿Qué campamento?"


translate spanish epilogue_mi_efd76d15_1:


    "Me miró sorprendida."


translate spanish epilogue_mi_11995523:


    my "Nuestro... campamento..."


translate spanish epilogue_mi_20f54ffc_2:


    ma "¿Qué?"


translate spanish epilogue_mi_31b8751f:


    "Masha parecía bastante tonta ahora, por lo que no pude evitar una sonrisa de ironía."


translate spanish epilogue_mi_855bda49:


    ma "Pero íbais hacia delante, no hacia atrás."


translate spanish epilogue_mi_c6243ca7:


    my "Delante... Y Sovyonok está allí."


translate spanish epilogue_mi_7ae81d9e:


    ma "¡Ey, para de bromear!"


translate spanish epilogue_mi_8e302a5d:


    "Exclamó."


translate spanish epilogue_mi_c483e315:


    my "Lo digo en serio..."


translate spanish epilogue_mi_5a93ab85_1:


    dv "Pero..."


translate spanish epilogue_mi_01ffaf07:


    "Dijo Alisa en voz baja."


translate spanish epilogue_mi_ab26c060:


    "Estaba sentada en la hierba acunando la cabeza de Lena, quien todavía dormía."


translate spanish epilogue_mi_8b206af0:


    my "Claro, así sin más..."


translate spanish epilogue_mi_20dcd489:


    "Dije cansado y me dejé caer en el suelo."


translate spanish epilogue_mi_bf48949e:


    ma "Vale, ¡significa eso que nos vamos allí!"


translate spanish epilogue_mi_65d91c76:


    "Miré a Masha intensamente y de repente me di cuenta de que no podía esperar más."


translate spanish epilogue_mi_de289d79:


    my "Ey, ¿qué pasa contigo?"


translate spanish epilogue_mi_9af1ef36:


    ma "¿Qué quieres decir?"


translate spanish epilogue_mi_6e540bea:


    "Su aspecto decía a gritos que no era la hora de una charla despreocupada."


translate spanish epilogue_mi_ebb6fb85:


    my "Ayer estabas muerta de miedo, y hoy en cambio tienes una actitud de «esto me pasa a todas horas»."


translate spanish epilogue_mi_825a0cff:


    ma "Pero, ¿qué sentido tiene entrar en pánico?"


translate spanish epilogue_mi_783cf09d:


    "Ella dibujó una apenas visible sonrisa en su cara."


translate spanish epilogue_mi_f484fdd8:


    my "Si no te conociera..."


translate spanish epilogue_mi_c6b1f443:


    "Me callé, mordí mi labio y me di la vuelta."


translate spanish epilogue_mi_db691b48:


    "Pero de verdad, ¿la conozco bien?{w} Claro que pasamos muchos años juntos, pero..."


translate spanish epilogue_mi_e6e88a9f:


    "Si lo piensas bien, yo siempre pensé más en mí mismo.{w} O mejor dicho, siempre creí que ella estaba completamente satisfecha."


translate spanish epilogue_mi_d1b363f3:


    "Y ella siempre me pareció que era la misma conmigo.{w} La misma como cualquier otra cosa, incluso cuando rompimos. Como si no fuera una persona real, solamente mi artículo de ropa favorita."


translate spanish epilogue_mi_cf143826:


    "Pensando sobre ello, no tengo ni idea de qué siente en su alma. ¡Nunca me preocupé por ello!"


translate spanish epilogue_mi_63ac3909:


    "De este modo, su comportamiento bajo estas circunstancias, no resulta más extravagante que cualquier otra cosa que nos haya ocurrido."


translate spanish epilogue_mi_2ff98e18:


    "Y si piensas sobre la conversación que tuvimos en la playa hace dos noches atrás..."


translate spanish epilogue_mi_2969b51f:


    "Probablemente ella sea más fuerte que yo."


translate spanish epilogue_mi_b28f3085:


    ma "¡Ey! ¡¿Me estás escuchando?!"


translate spanish epilogue_mi_e9afa089:


    my "Oh, lo siento, me distraí. Ha sucedido tanto..."


translate spanish epilogue_mi_bcebe051:


    "Me miró rigurosamente."


translate spanish epilogue_mi_3508c9b1:


    ma "¡Volvamos hacia el campamento! O donde fuera que estuvierais... Si es que es el mismo antiguo campamento..."


translate spanish epilogue_mi_a27cf3ba_1:


    my "Muy bien..."


translate spanish epilogue_mi_6352bf59:


    "De hecho, no es que tuviéramos otra opción... unas cuantas horas más con este calor abrasador sólo produciría más bajas..."


translate spanish epilogue_mi_7241af63:


    ma "Pero tenemos que cargar con ella."


translate spanish epilogue_mi_a516c62b:


    my "Sí... Espera un segundo, déjame que lo solucione..."


translate spanish epilogue_mi_af54c066:


    "Encontré dos palos bastante largos y robustos en la arboleda cercana."


translate spanish epilogue_mi_e3e90986:


    "Al volver, le ordené a Router que se quitara la camisa."


translate spanish epilogue_mi_9efba5a0:


    "Trató de oponerse pero pronto se dio cuenta de que no tenía sentido discutir."


translate spanish epilogue_mi_914261b2:


    "Me quité mi camisa también e hicimos un tipo de camilla atando los tejidos a los palos."


translate spanish epilogue_mi_4a929c62:


    ma "¿Resistirá?"


translate spanish epilogue_mi_0f7b726d:


    "Masha hizo una mirada de duda a nuestra camilla, pero al final le dio un par de apretones fuertes, asegurándose de su resistencia y sonrió."


translate spanish epilogue_mi_044b0bcd:


    my "Resistirá, al menos hasta que lleguemos al campamento..."


translate spanish epilogue_mi_a20cefa7_26:


    "..."


translate spanish epilogue_mi_5aba3686:


    "Era difícil avanzar con este calor, pero era todavía más difícil cargar con la herida... es decir con Lena."


translate spanish epilogue_mi_0c231086:


    "En varias ocasiones casi me desmayo y apenas me recompuse medio caído en el suelo."


translate spanish epilogue_mi_3c08fc74:


    "Router estaba incluso peor... tanto Masha como Alisa se iban cambiando el lugar con él de tanto en cuanto."


translate spanish epilogue_mi_b8ecaed9:


    "Aun así, mi orgullo me impedía pedirles a ellas que cargaran con Lena aunque fuera por un minuto."


translate spanish epilogue_mi_8915be50:


    "Está claro que es estúpido comportarse así bajo estas circunstancias, pero no me podía ayudar a mí mismo."


translate spanish epilogue_mi_539b7522:


    "Éste es otro de mis defectos en mi personalidad, junto a mi frialdad, mi descuido hacia los demás... incluso mis seres queridos... y otros rasgos de baja nobleza."


translate spanish epilogue_mi_f81c0c30:


    "Finalmente, lo hicimos de alguna manera como para llegar hasta las puertas del campamento, y me derrumbé en la hierba en la sombra de una estatua de un desconocido pionero, completamente agotado."


translate spanish epilogue_mi_867157a6:


    ma "En realidad es nuestro campamento..."


translate spanish epilogue_mi_bdc084f3:


    "Masha se entretuvo al pronunciar sus palabras."


translate spanish epilogue_mi_0947ef24:


    ma "Y desde luego, se ve igual."


translate spanish epilogue_mi_8fa854c5:


    dv "Tenemos que llevarla a una cabaña."


translate spanish epilogue_mi_01ffaf07_1:


    "Mencionó Alisa en voz baja."


translate spanish epilogue_mi_e31180f6:


    my "Claro..."


translate spanish epilogue_mi_c4f3ee83:


    "Me levanté con esfuerzo considerable, y me preparé para la última recta."


translate spanish epilogue_mi_a20cefa7_27:


    "..."


translate spanish epilogue_mi_6ba66183:


    "La opción obvia era la cabaña de Olga Dmitrievna."


translate spanish epilogue_mi_15104e27:


    "Más que nada porque pudimos transcurrir la última noche sin problemas graves.{w} Por lo menos aun estamos vivos..."


translate spanish epilogue_mi_59348943:


    "Yo y Router nos tiramos exhaustos encima de una cama, las muchachas estaban cambiando de ropa a Lena en la otra."


translate spanish epilogue_mi_0a658695:


    ma "¡No miréis!"


translate spanish epilogue_mi_72499cac:


    "Dijo Masha severamente."


translate spanish epilogue_mi_6731b709:


    my "No hay nada que mirar que valga la pena..."


translate spanish epilogue_mi_6eb72cdb:


    "Resoplé y me di la vuelta, dando un duro golpe en las costillas a Router, siguiéndome en mi posición."


translate spanish epilogue_mi_e2b706b0:


    ma "Vamos a buscar algo de agua. Y probablemente hurgaremos por la cantina también..."


translate spanish epilogue_mi_db46de88:


    my "Es peligroso ir solas..."


translate spanish epilogue_mi_f76f1de1:


    ma "¿Quieres venirte?"


translate spanish epilogue_mi_fde537c1:


    "Me sonrió."


translate spanish epilogue_mi_833e3637:


    my "No gracias... Por lo menos llevaos esa tubería con vosotras."


translate spanish epilogue_mi_fefef18f_1:


    "Dije indeciso."


translate spanish epilogue_mi_f8f2e420:


    ma "¿Crees de verdad que nos ayudará?"


translate spanish epilogue_mi_6450d358:


    "Suspiré profundamente y cerré los ojos."


translate spanish epilogue_mi_59f84e9e:


    my "Bueno... Pues id con cuidado..."


translate spanish epilogue_mi_cf8ac78f:


    "La puerta se cerró tras un segundo."


translate spanish epilogue_mi_4eaf6646:


    ro "¿Te has dormido?"


translate spanish epilogue_mi_0fe78771:


    "El susurro de Router me alcanzó."


translate spanish epilogue_mi_863570d0:


    my "Como si uno pudiera dormir aquí..."


translate spanish epilogue_mi_a7be59ec:


    "De repente, escuché el gemido de Lena desde la otra cama."


translate spanish epilogue_mi_d9d00dfc:


    "Me alcé sobre mis codos y la miré."


translate spanish epilogue_mi_af832e65:


    "Lena estaba revolviéndose y dando vueltas bajo la manta."


translate spanish epilogue_mi_efc1edac:


    "Repentinamente, ella abrió los ojos y de forma abrupta giró su rostro hacia mí."


translate spanish epilogue_mi_ca6611ca:


    "Una sonrisa diabólica estaba petrificada en su cara.{w} La misma que vi ayer en el atardecer..."


translate spanish epilogue_mi_3eb7004f:


    un "Bastante caliente ya, ¿eh?"


translate spanish epilogue_mi_f1709333:


    "Ella rompió en carcajadas."


translate spanish epilogue_mi_51df1151:


    "Sentí un escalofrío recorrer mi espalda. Mi habilidad para hablar se desvaneció."


translate spanish epilogue_mi_8199e005:


    my "Tú... tú..."


translate spanish epilogue_mi_6c21d46f_1:


    ro "¿Qué?"


translate spanish epilogue_mi_586ee385:


    "Preguntó Router con su sueño."


translate spanish epilogue_mi_f2047f00:


    "Me giré hacia él bruscamente."


translate spanish epilogue_mi_8aab4a10:


    my "¡Mira!"


translate spanish epilogue_mi_fe583ccc:


    "Movía mis manos, señalando la cama de Lena."


translate spanish epilogue_mi_6c21d46f_2:


    ro "¿Qué?"


translate spanish epilogue_mi_58d3724b:


    "Él se levantó y miró hacia donde estaba indicando primero, luego se volvió hacia mí."


translate spanish epilogue_mi_49b544cf:


    "Me giré y vi tan sólo a Lena durmiendo pacíficamente."


translate spanish epilogue_mi_866a81a4:


    my "¿No lo escuchaste?"


translate spanish epilogue_mi_6c21d46f_3:


    ro "¿Qué?"


translate spanish epilogue_mi_9cb45ed8:


    my "Su... Lena estaba despierta... y dijo..."


translate spanish epilogue_mi_0717c0bb:


    ro "Ah, parece que tú también has tenido una insolación..."


translate spanish epilogue_mi_4d9e42bd:


    "Se giró cara a la pared."


translate spanish epilogue_mi_ea5146f0:


    "Pero estaba completamente seguro de que lo escuché y lo vi..."


translate spanish epilogue_mi_0c0d3615:


    "Bueno, podría ser cierto al final que el calor me hubiera afectado..."


translate spanish epilogue_mi_42499ac9:


    "Pero todo era exactamente como aquella vez.{w} ¡Y no era Lena!"


translate spanish epilogue_mi_ef3c872b:


    "¡Por lo menos no era la Lena que conocí! ¡Como si estuviera poseída por el diablo!"


translate spanish epilogue_mi_8209af39:


    "Me tumbé y me eché la sábana por encima de la cabeza."


translate spanish epilogue_mi_dead6866:


    "En verdad quería dormir, pero la posibilidad de que no me despertara nunca más si me dormía ahora, me mantenía despierto e intranquilo."


translate spanish epilogue_mi_a600ff76:


    "No sé cuánto tiempo me pasé tumbado así, temblando por cada ruido sospechoso, pero de un momento a otro la puerta se abrió de golpe y unos pasos resonaron en la habitación."


translate spanish epilogue_mi_3cdfabf7:


    "Apenas observé a través de la sábana, que pude entrever a Alisa y Masha entrando con un cubo de agua y unas bolsas."


translate spanish epilogue_mi_f6d3ba26:


    ma "Veo que te estás poniendo un poco paranoico."


translate spanish epilogue_mi_2e3f558d:


    my "Claro, algo así..."


translate spanish epilogue_mi_24d62045:


    "Respondí con una voz temblorosa, tratando de sacar una sonrisa."


translate spanish epilogue_mi_3657be4d:


    my "¿va todo bien?"


translate spanish epilogue_mi_57a73d0f:


    ma "Eso parece."


translate spanish epilogue_mi_fb21cabf:


    "Eché un vistazo a Lena una vez más, pero estaba todavía durmiendo en silencio."


translate spanish epilogue_mi_bfd0137f:


    "¿Quizá solamente fuera mi imaginación?"


translate spanish epilogue_mi_7f889c60:


    "Seguro que sí, dadas las condiciones de estrés emocional extremo y de fatiga física, podría estar viendo cosas..."


translate spanish epilogue_mi_074f63d2:


    "Me levanté con bastante esfuerzo, agarré un vaso de la mesa y serví algo de agua."


translate spanish epilogue_mi_543f3a10:


    my "Y bien, ¿qué vamos hacer?"


translate spanish epilogue_mi_98b3a8c1:


    ma "Tenemos que quedarnos aquí por ahora."


translate spanish epilogue_mi_2ceeb90b:


    dv "Pero Olga Dmitrievna llamó a la policía..."


translate spanish epilogue_mi_b75cedb9:


    "Murmuró Alisa."


translate spanish epilogue_mi_9eab3900:


    ma "Claro, pero es que todo el mundo desapareció tras eso."


translate spanish epilogue_mi_0b0513f7:


    my "En cualquier caso, hasta que Lena recupere el conocimiento..."


translate spanish epilogue_mi_d162c751:


    "Aunque volviera a verla así de nuevo, sinceramente preferiría mejor que no despertara..."


translate spanish epilogue_mi_5d8b05ff:


    "Alisa se estiró en la cama de Lena, y Masha se sentó a mi lado."


translate spanish epilogue_mi_09de083e:


    ma "Duerme."


translate spanish epilogue_mi_4719c4e5:


    "Ella me acarició suavemente por encima de mi cabeza."


translate spanish epilogue_mi_9c51ff5e:


    my "En serio puedes dormir con todo esto que sucede..."


translate spanish epilogue_mi_239bed8a:


    ma "Haremos guardia. ¡Se te ve muy cansado!"


translate spanish epilogue_mi_3c89501d:


    "Supongo que tenía razón."


translate spanish epilogue_mi_8a2a3476:


    my "Vale, ¡pero sólo una siesta! Despiértame antes del atardecer."


translate spanish epilogue_mi_43c3e662:


    "No dijo ni una palabra, en vez de ello solamente sonrió."


translate spanish epilogue_mi_b70a39b4:


    my "¡Promételo!"


translate spanish epilogue_mi_17742508:


    ma "¡Lo prometo!"


translate spanish epilogue_mi_1ce87451:


    "Contestó Masha en voz baja."


translate spanish epilogue_mi_ba17b8a7:


    "Cerré mis ojos y al instante caí en un sueño."


translate spanish epilogue_mi_a20cefa7_28:


    "..."


translate spanish epilogue_mi_934c20c6:


    "Ahí vuelven otra vez... los saltamontes."


translate spanish epilogue_mi_beaa4cc0:


    "Esta vez es su rey, una enorme monstruosidad de color verde ciénaga con unas patas largas y curvadas, con alas que podrían ser de la envergadura de un avión de pasajeros y unos enormes ojos oscuros como el alquitrán."


translate spanish epilogue_mi_f3c2c459:


    "Se inclinaba sobre mí, sonriendo y farfullando algo realmente ensordecedor."


translate spanish epilogue_mi_507578a0:


    "Todavía no entiendo nada de su lengua, pero, a juzgar por su tono, no era nada agradable que pudieras escuchar en las noticias de la mañana."


translate spanish epilogue_mi_e7fe809f:


    "Quería apartarme, pero me atrapaba estrechándome con sus miserables zarpas."


translate spanish epilogue_mi_4977fdd5:


    "Podía escuchar mis huesos crugir, sintiendo mis costillas romperse, desgarrando mis órganos internos con sus puntiagudas púas."


translate spanish epilogue_mi_7da28214:


    "Sentí como si cientos de martillos me machacaran dentro de mi cráneo, mi visión se desvaneció y al siguiente instante, una fuente de sangre y carne se esparció a borbotones por todo el lugar..."


translate spanish epilogue_mi_a20cefa7_29:


    "..."


translate spanish epilogue_mi_48f17ebc:


    "Salté bañado en sudor frío. Mi propio grito todavía resonaba en la habitación."


translate spanish epilogue_mi_f92e8047:


    "Me llevó varios segundos despejarme de las legañas de mis ojos."


translate spanish epilogue_mi_6b7f9fb7:


    "Miré a mi alrededor frenéticamente y me di cuenta de que ya era tarde, y que estaba completamente oscuro ahí fuera."


translate spanish epilogue_mi_d897367d:


    "¡Qué fastidio! No apagamos las luces ayer..."


translate spanish epilogue_mi_1d5cb079:


    "Masha estaba tumbada a mi lado dormida, mientras que Router estaba acurrucado en el suelo."


translate spanish epilogue_mi_95cddf67:


    "Lena estaba estirada en la otra cama, envuelta estrechamente entre la manta."


translate spanish epilogue_mi_52614955:


    "A pesar de eso, Alisa no estaba en ninguna parte."


translate spanish epilogue_mi_333411d8_2:


    my "¡Ey!"


translate spanish epilogue_mi_f5a3eeec:


    "Desperté a Masha con unos cuantos codazos bruscos."


translate spanish epilogue_mi_20f54ffc_3:


    ma "¿Qué?"


translate spanish epilogue_mi_6f181833:


    "Lentamente volvió en sí."


translate spanish epilogue_mi_66e200e7:


    my "¡No te pregunté!"


translate spanish epilogue_mi_cd89b571:


    ma "Lo siento... Me dormí..."


translate spanish epilogue_mi_c592cc74:


    "Tenía tal expresión de culpabilidad en su rostro que me sentí mal."


translate spanish epilogue_mi_4ab7a146:


    "No debería haber confiado en ella.{w} Por lo menos podría haber puesto un despertador..."


translate spanish epilogue_mi_95dc5bb2:


    "Desperté a Router."


translate spanish epilogue_mi_10190a88:


    my "¿Dónde está Alisa?"


translate spanish epilogue_mi_e77cd9de:


    ma "No lo sé..."


translate spanish epilogue_mi_4b1dca0c:


    "Dijo Masha preocupadamente."


translate spanish epilogue_mi_bb853559:


    my "Bueno, ¿quién lo va a saber? ¡Estábamos todos durmiendo!"


translate spanish epilogue_mi_8b687bf3:


    ma "Bueno, ella estaba durmiendo aquí y luego..."


translate spanish epilogue_mi_e01ba3e1:


    "Su mirada culpable recorrió como un rayo la habitación."


translate spanish epilogue_mi_ed2f886e:


    my "¡Y entonces te dormiste!"


translate spanish epilogue_mi_f5196784:


    "Dejó caer su cabeza y expiró un suspiro profundo."


translate spanish epilogue_mi_20b8375c:


    my "Oh, perfecto, ¡simplemente perfecto!"


translate spanish epilogue_mi_0a0f07b3:


    "Pegué un golpe sobre la mesa con mi puño, haciendo que Masha diera un salto."


translate spanish epilogue_mi_b0069244:


    my "Vale, ¡quédate sentada aquí e iremos a buscarla!"


translate spanish epilogue_mi_020772e0:


    ma "¿Sola...?"


translate spanish epilogue_mi_49838f90:


    "Su rostro de repente dibujó una expresión de miedo."


translate spanish epilogue_mi_550b76e2:


    my "¡Sola! ¡¿No fuiste tú quien me dijo que no vale la pena entrar en pánico?!"


translate spanish epilogue_mi_93f232b2:


    ma "Claro, pero no es..."


translate spanish epilogue_mi_ff3b375f:


    my "¡Oh, muy bien! ¡Iré solo!"


translate spanish epilogue_mi_c8d6b6d8:


    ma "Pero..."


translate spanish epilogue_mi_42faecaa:


    my "¿O quieres en realidad sentarte aquí sola con Lena?"


translate spanish epilogue_mi_96cd1356:


    "Masha se calló."


translate spanish epilogue_mi_68e74430:


    ro "Es peligroso ir solo..."


translate spanish epilogue_mi_2dca72b2:


    "Me percaté de que Router parecía estar ya completamente despierto."


translate spanish epilogue_mi_81495ac1:


    my "¿Y no es peligroso para ella estar aquí sentada sola?"


translate spanish epilogue_mi_8d4fc7d5:


    "Tuve la sensación de que no tenía sentido alguno estar discutiendo, así que me levanté, agarré mi confiable tubería y me dirigí hacia la puerta."


translate spanish epilogue_mi_f498c94c:


    "En cualquier caso, puesto que la gente ha estado desapareciendo en la nada, uno no se podía sentir seguro en ninguna parte o con compañía."


translate spanish epilogue_mi_3bd941f9:


    "Desde luego, también podría ser que Alisa solamente fue a algún lugar.{w} A los aseos, por ejemplo."


translate spanish epilogue_mi_cfe4baf3:


    "Aunque, dado lo asustada que estaba, creo que habría despertado a Masha o sencillamente se habría aguantado hasta el amanecer..."


translate spanish epilogue_mi_9ce03507:


    "Maldita sea, ¡cualquier cosa salvo ir sola por el campamento!"


translate spanish epilogue_mi_979b3743:


    "Reuní fuerzas, tiré de la maneta y me fui fuera."


translate spanish epilogue_mi_e426c290:


    "Barrí con mis ojos todo el paisaje y luego..."


translate spanish epilogue_mi_97b95495:


    "Vi a Alisa...{w} colgando desde arriba del suelo justo en el árbol frente a la cabaña..."


translate spanish epilogue_mi_03acb541:


    "Un dolor agudo me punzó mis sienes y me desplomé sobre el suelo."


translate spanish epilogue_mi_19e8325e:


    "El horror era tan sobrecogedor que, literalmente, sentí que todo mi cuerpo estaba encadenado en acero."


translate spanish epilogue_mi_c71ffa7a:


    "Cegado por el miedo, apenas me las apañé para retornar a la cabaña y cerrar la puerta."


translate spanish epilogue_mi_78bb1682:


    ma "¡¿Qué ocurrió?!"


translate spanish epilogue_mi_3c910ff4:


    "Masha corrió hacia mí."


translate spanish epilogue_mi_3d6cfafe_1:


    my "¡No mires!"


translate spanish epilogue_mi_36ea64e3:


    "Le agarré de su brazo."


translate spanish epilogue_mi_6f6e2c1b:


    my "¡No vayas fuera!"


translate spanish epilogue_mi_39555e25:


    ma "¡¿Qué pasa?!"


translate spanish epilogue_mi_ce032298:


    "Alcanzó el pomo de la puerta. Intenté detenerla, pero estaba tan débil que solamente pude dejar ir una queja de desesperación."


translate spanish epilogue_mi_f0dd9ecf:


    "Al abrir la puerta, Masha chilló y se quedó de piedra."


translate spanish epilogue_mi_0e2c4200:


    "Reuniendo mi última pizca de fuerza, me incorporé y le di una patada a la puerta para cerrarla."


translate spanish epilogue_mi_fd6cf8ba:


    "Al siguiente instante Masha se mareó y cayó en mis brazos."


translate spanish epilogue_mi_9b50f023:


    "La cargué hasta la cama y la dejé al lado del afectado Router."


translate spanish epilogue_mi_8e5b2243:


    "Su rostro me decía, definitivamente, que no estaba interesado en saber qué sucedió ahí."


translate spanish epilogue_mi_c545824e:


    "Todos estos chillidos despertaron a Lena. Se alzó ligeramente en su cama y nos hizo una mirada."


translate spanish epilogue_mi_012448da:


    un "¿Qué ocurre...?"


translate spanish epilogue_mi_6c39322b:


    "Preguntó en un susurro."


translate spanish epilogue_mi_3f27c2c0_1:


    "No sabía cómo debería responderle."


translate spanish epilogue_mi_7a995e66:


    "Sentí surgir un llanto en mi garganta, pero me las apañé para sofocarlo en un suspiro hueco."


translate spanish epilogue_mi_ed8a0e3f:


    "Lena estaba horrorizada, pero seguía sin darse cuenta qué había ocurrido exactamente aquí."


translate spanish epilogue_mi_e35e5625:


    my "No... salgas... fuera..."


translate spanish epilogue_mi_f9065ef9:


    "Gemí."


translate spanish epilogue_mi_657108f7:


    "A diferencia de Masha, ella no se puso a discutir."


translate spanish epilogue_mi_cca21832:


    "Un silencio opresivo siguió."


translate spanish epilogue_mi_761c4b2e:


    "Incluso los saltamontes parecían haberse callado con el fin de dar suspense a la situación."


translate spanish epilogue_mi_1bafb56b:


    "Masha todavía estaba tumbada en mi regazo, Router estaba estremeciéndose en el rincón de la cama y Lena estaba sentada frente a mí con una expresión de tristeza y desesperación en su rostro."


translate spanish epilogue_mi_3874b4a4:


    "Miraba fijamente la puerta sin parpadear, esperando el momento en que se abriría de golpe y..."


translate spanish epilogue_mi_d8767c7a:


    "Mi miedo no se podía manifestar de cualquier forma concreta... un homicida maníaco, un monstruo, un alienígena...{w} Era el terror de lo desconocido, las tinieblas y el vacío."


translate spanish epilogue_mi_ce811c64:


    "No estaba preocupado de que pudiera acabar mutilado a pedazos justo como Slavya, o acabar como un zombie como Ulyana."


translate spanish epilogue_mi_170b2003:


    "Estaba preocupado de desaparecer, desvanecerme sin rastro de este mundo..."


translate spanish epilogue_mi_e374bab2:


    "Masha dejó ir un gemido en voz baja."


translate spanish epilogue_mi_4fce6de4:


    ma "Tuve tal pesadilla..."


translate spanish epilogue_mi_908757cc:


    my "No era un sueño..."


translate spanish epilogue_mi_bb759ffc:


    "Abrió sus ojos como platos y me miró con horror."


translate spanish epilogue_mi_198e3f4c:


    ma "Entonces... ¡¿allí?!"


translate spanish epilogue_mi_3d6cfafe_2:


    my "¡No mires!"


translate spanish epilogue_mi_741958cd:


    "Le grité y la abracé rápidamente."


translate spanish epilogue_mi_b2cd5a0c:


    "Lena comenzó a llorar con intensidad, corrió hacia mí y se apretó junto a mí, temblando completamente."


translate spanish epilogue_mi_bd486098:


    "Largos minutos se arrastraron en un agonizante silencio."


translate spanish epilogue_mi_282e0eee:


    ma "Bueno... ¿qué haremos ahora?"


translate spanish epilogue_mi_10a44f66:


    "Preguntó Masha entre lágrimas."


translate spanish epilogue_mi_b44a635d:


    "No parecía tan segura de sí misma ahora.{w} Aunque todos estábamos en la misma situación..."


translate spanish epilogue_mi_777a7686:


    "El tiempo transcurrió, y aun así nada sucedió."


translate spanish epilogue_mi_af951e50:


    "Teníamos que tomar una decisión."


translate spanish epilogue_mi_104c4b70:


    "No es que pudiera decir que recuperase todo mi conocimiento tras lo que vi, pero sí que sentía mi mente un poco despejada."


translate spanish epilogue_mi_bf92a608:


    "Sabía una cosa ciertamente... ¡no podíamos permanecer aquí!{w} Y no porque no fuera más peligroso que cualquier otro lugar..."


translate spanish epilogue_mi_b1129cb1:


    "Simplemente era porque no podía soportar estar cerca del cadáver de Alisa."


translate spanish epilogue_mi_81f3f410:


    "Apuesto a que es incluso una experiencia más desgarradora para las muchachas..."


translate spanish epilogue_mi_5ec6958d:


    my "Tenemos que irnos..."


translate spanish epilogue_mi_5788b9db:


    "Dije en voz baja."


translate spanish epilogue_mi_2f93c801:


    "Sentí que Masha y Lena me abrazaban estrechándome más."


translate spanish epilogue_mi_7cabd0eb:


    ro "Tenemos que..."


translate spanish epilogue_mi_48701c6a:


    "Router dejó de temblar afectado y abandonó la cama."


translate spanish epilogue_mi_32912129:


    my "¿Cómo te sientes?"


translate spanish epilogue_mi_3329e7d9:


    "Le pregunté a Lena."


translate spanish epilogue_mi_d00d3d0f:


    "Ella no dijo nada."


translate spanish epilogue_mi_95fe422d:


    my "¿Puedes caminar?"


translate spanish epilogue_mi_3c68ea6c:


    un "Probablemente..."


translate spanish epilogue_mi_5d37a806:


    "Suavemente me deshice de sus abrazos y me levanté."


translate spanish epilogue_mi_f90fc978:


    my "¡Tenemos que correr! Por la noche será más fácil irnos."


translate spanish epilogue_mi_d58a29ab:


    ma "Pero nosotros..."


translate spanish epilogue_mi_8f19793e:


    "Masha trató de poner una excusa humildemente."


translate spanish epilogue_mi_73866edf:


    my "Claro, sé que escogimos quedarnos, pero... ¡Ahora tenemos que escapar de aquí!"


translate spanish epilogue_mi_951f9710:


    "Me giré hacia Router."


translate spanish epilogue_mi_ef2e4fda:


    my "Te das cuenta de que... ellas no deberían ver todo eso."


translate spanish epilogue_mi_a91e5c2f:


    "Él asintió con la cabeza de manera apenas visible."


translate spanish epilogue_mi_2aa79f94:


    "Aunque Router no supiera que ocurrió con Alisa, era obvio que su vívida imaginación le habría evocado muchas teorías espantosas durante este rato por lo que, probablemente, él estaría aun más abatido que yo."


translate spanish epilogue_mi_7037899d:


    my "Vale, nos cogeremos de la mano y avanzaremos. Iré primero en vanguardia. Masha la siguiente, luego Lena y después tú."


translate spanish epilogue_mi_6b024c96:


    "Miré a las muchachas."


translate spanish epilogue_mi_dd76c6a4:


    "Estaban sentadas con sus cabezas agachadas, sollozando en silencio."


translate spanish epilogue_mi_549e20f2:


    my "¿Preparados?"


translate spanish epilogue_mi_c5d5810d:


    "Nadie contestó... era obviamente una pregunta inútil... nadie puede estar preparado para semejante situación."


translate spanish epilogue_mi_35423a3b:


    "Cogí la mano de Masha, me di la vuelta y les hice una mirada seria a Lena y Router."


translate spanish epilogue_mi_f91862ca:


    my "Vale, ahora cerrar vuestros ojos y no los abráis sin importar lo que suceda, ¿lo entendéis?"


translate spanish epilogue_mi_44cacce6_2:


    ro "Seh..."


translate spanish epilogue_mi_f7a66aa8:


    "Lena asintió."


translate spanish epilogue_mi_7983c203:


    "Me armé de valor, abrí de golpe la puerta y salí fuera, sosteniendo la mano de Masha."


translate spanish epilogue_mi_eec4cf2b:


    my "¡Vigilad vuestros pasos!"


translate spanish epilogue_mi_f298ccdd:


    "Cuando nos abrimos camino hacia fuera, lancé una última mirada a Alisa."


translate spanish epilogue_mi_bb16154d:


    "Lo que vi ahora no me tiró al suelo, aunque mi corazón latía frenéticamente, me sentía mal y mareado con el estómago revuelto."


translate spanish epilogue_mi_0573dd61:


    "Ella estaba colgada alrededor de unos cinco metros por encima del suelo. Diversos pañuelos estaban entrelazados en una suerte de cuerda que la aferraba prieta por su garganta."


translate spanish epilogue_mi_cdda0258:


    "Los ojos hinchados, la lengua sobresaliendo, la putrefacción avanzada en su cuerpo... era más bien como si no hubiera muerto hace un par de horas, sino por lo menos un par de días."


translate spanish epilogue_mi_c76687ee:


    "Una expresión de agonía y sufrimiento inhumano permanecía congelado en su rostro."


translate spanish epilogue_mi_a33a94a5:


    "Estaba claro que algo así no se lo podía haber hecho ella misma."


translate spanish epilogue_mi_788c07e6:


    "Dios mío, ¿quién está haciendo todo esto? ¿Quizás estamos en el infierno...?"


translate spanish epilogue_mi_aa8da201:


    "Apresuré mis pasos alejándome de aquella escena diabólica, arrastrando a los demás conmigo."


translate spanish epilogue_mi_a20cefa7_30:


    "..."


translate spanish epilogue_mi_912b3321:


    "Les ordené abrir los ojos sólo cuando alcanzamos la plaza."


translate spanish epilogue_mi_d2d78eec:


    ma "Ella está... ¿ella está todavía allí?"


translate spanish epilogue_mi_55b4c0e5:


    "Preguntó Masha, sin soltarme la mano."


translate spanish epilogue_mi_413cf259:


    "Simplemente asentí."


translate spanish epilogue_mi_8c308047:


    un "¡¿Quién?! ¡¿Alisa?!"


translate spanish epilogue_mi_c6b1fe3f:


    "Lena gritó y rompió a llorar."


translate spanish epilogue_mi_6e61e470:


    "Masha la abrazó y trató de consolarla."


translate spanish epilogue_mi_5d58c3b8:


    ro "¿Qué le sucedió a ella?"


translate spanish epilogue_mi_54683d6b:


    "Masculló Router con lengua vacilante."


translate spanish epilogue_mi_50c8a92f:


    my "Estoy seguro de que no te agradaría saberlo."


translate spanish epilogue_mi_290e56ee:


    "Por un rato estuve ahí quieto y mirando la luna."


translate spanish epilogue_mi_ac9a276c:


    "Lena estaba sollozando, Router estaba dando círculos nervioso a nuestro alrededor, y Masha se sentó en el banco y enterró su rostro entre sus manos."


translate spanish epilogue_mi_f077aeea:


    my "Vale, es la hora..."


translate spanish epilogue_mi_54ac6199:


    "Dije en voz baja, aunque nadie se movió."


translate spanish epilogue_mi_727a919f:


    my "¡No podemos quedarnos aquí!"


translate spanish epilogue_mi_38a7da59:


    "Caminé hacia Masha, me incliné sobre ella y le cogí de la mano."


translate spanish epilogue_mi_2178548f:


    "Alzó sus ojos llenos de lágrimas hacia mí y asintió."


translate spanish epilogue_mi_891afcb1:


    "Lentamente nos dirigimos hacia la parada del autobús..."


translate spanish epilogue_mi_a20cefa7_31:


    "..."


translate spanish epilogue_mi_d6e969ac:


    "La noche se ha cernido sobre el campamento."


translate spanish epilogue_mi_2b88b9e3:


    "A lo largo del camino, me di cuenta que los saltamontes estaban casi en silencio."


translate spanish epilogue_mi_4d1b313b:


    "Permaneciendo en silencio por un tiempo, ahora reanudaban su desagradable canción, pero esta vez eran mucho menos ruidosos y, diría, un poquito más respetuosos."


translate spanish epilogue_mi_24889ac6:


    "Su ajetreado día con su continuo ruido, se intercambió por una noche de violín y orquestra."


translate spanish epilogue_mi_af1d202f:


    "¿Hay alguna posibilidad de que sean conscientes sobre lo que está ocurriendo aquí...?"


translate spanish epilogue_mi_5a24d3d5:


    "Por supuesto, si asumimos que en el infierno hay saltamontes también..."


translate spanish epilogue_mi_efbb6c03:


    "Llegamos a la parada de autobús y nos detuvimos, dudando."


translate spanish epilogue_mi_b2bc2a02:


    ma "Y bien, ¿dónde vamos?"


translate spanish epilogue_mi_e3229b58:


    "Preguntó Masha, limpiándose las lágrimas."


translate spanish epilogue_mi_1ca97523:


    my "Vale, ya lo hemos intentado yendo hacia la derecha... Vayamos hacia la izquierda."


translate spanish epilogue_mi_bf0ef4ba:


    ma "Pero no hay ninguna ciudad hacia allí..."


translate spanish epilogue_mi_c9afa297:


    my "Y hace tan sólo un par de días atrás no había ningún campamento hacia la derecha..."


translate spanish epilogue_mi_b4e45382:


    un "Tal vez no deberíamos..."


translate spanish epilogue_mi_49be4260:


    "Lena sollozó callada."


translate spanish epilogue_mi_f2ab6c8a:


    my "No tenemos opción... ¡No quiero quedarme aquí más tiempo!"


translate spanish epilogue_mi_93a07a2a:


    "Estábamos justo a punto de irnos cuando escuchamos ruidos acercándose desde bastante lejos."


translate spanish epilogue_mi_bb6a2944:


    "Forcé mi vista y entreví alguien acercándose por la carretera."


translate spanish epilogue_mi_cbc33115:


    ro "¡Un autobús!"


translate spanish epilogue_mi_2b6b2a9a:


    "Exclamó Router con júbilo en su voz."


translate spanish epilogue_mi_89a22708:


    my "¡Cállate, tonto! ¡Eso no es un autobús! ¡Son personas acercándose!"


translate spanish epilogue_mi_6f0bbf9c:


    "Le siseé."


translate spanish epilogue_mi_aea41fc4:


    ma "Muchas personas..."


translate spanish epilogue_mi_c67e1ffd:


    "Dijo Masha ansiosamente."


translate spanish epilogue_mi_f5f53cd8:


    "Estaba completamente claro que no deberíamos seguir permaneciendo aquí mucho más."


translate spanish epilogue_mi_353f7742:


    "Me di la vuelta y vi alguien acercándose por la carretera desde la otra dirección también.{w} El ruido comenzaba a ser ensordecedor."


translate spanish epilogue_mi_a3e8eadb:


    "La estridulación de los saltamontes... Aunque no una habitual de ellos... sino una mucho más ruidosa, una versión distorsionada, como si hubiera pasado a través de un amplificador de guitarra."


translate spanish epilogue_mi_1f4d46ef:


    my "¡Corred!"


translate spanish epilogue_mi_c34e863e:


    "Grité, pero nadie se movió."


translate spanish epilogue_mi_b6e2867e:


    my "¡Oh, a la mierda!"


translate spanish epilogue_mi_f6db9d2f:


    "Agarré la mano de Masha y la de Lena y salí corriendo como un rayo de vuelta al campamento."


translate spanish epilogue_mi_f19be6a7:


    "Las muchachas avanzaron a trompicones con estupor, por lo que al final, literalmente, las estuve arrastrando conmigo."


translate spanish epilogue_mi_fae82ef7:


    "Habiendo corrido hasta la plaza, me detuve a recuperar algo de aliento y solamente entonces me di cuenta de que Router había sido dejado atrás."


translate spanish epilogue_mi_b1c873bb:


    ma "¡Tenemos que volver atrás a por él!"


translate spanish epilogue_mi_41f008ff:


    "Chilló Masha y se desplazó hacia la parada de autobús."


translate spanish epilogue_mi_a900b142:


    my "¡¿Te volviste loca?!"


translate spanish epilogue_mi_f149054d:


    "Le estiré de la mano y la forcé a detenerse."


translate spanish epilogue_mi_c95121c1:


    my "¡¿Es que quieres que nos aguarde el mismo destino?!"


translate spanish epilogue_mi_3d0e96c9:


    "Lena estaba temblando tan violentamente, que sentí que mi mano derecha estaba agitándose mientras sostenía la suya."


translate spanish epilogue_mi_27fc3c78:


    my "¡Sigue corriendo!"


translate spanish epilogue_mi_3a8faa33:


    ma "¡¿Hacia dónde?!"


translate spanish epilogue_mi_d149a305:


    my "¡No lo sé, hacia los bosques!"


translate spanish epilogue_mi_b84b36c8:


    "Retornar a la cabaña de Olga Dmitrievna no era una opción."


translate spanish epilogue_mi_2e4f2c35:


    "Mi instinto de supervivencia también me sugería que no debíamos permanecer en una zona abierta tampoco."


translate spanish epilogue_mi_4215b313:


    "Corrimos hacia los bosques."


translate spanish epilogue_mi_4716024f:


    "Sostuve estrechamente las manos de las muchachas."


translate spanish epilogue_mi_0e01cbac:


    "Corrían con dificultad, especialmente Lena quien iba oscilando y tropezando. Ella se derrumbó en unas cuantas ocasiones, pero rápidamente la incorporé, animándola con gritos y arrastrándola."


translate spanish epilogue_mi_b9aff25b:


    "Habiendo cruzado los árboles ya hicimos la parte más dura."


translate spanish epilogue_mi_30941c88:


    "Aunque la luna llena estaba brillando en el cielo, en la oscuridad estaba constantemente tropezando con salientes, brancas y cayendo en agujeros."


translate spanish epilogue_mi_90e827b2:


    "Las ortigas estaban cortándome las piernas y las hojas me golpeaban en la cara, dejándome dolorosos rasguños."


translate spanish epilogue_mi_2795f897:


    "Mi corazón latía con fuerza. Cada respiración que daba, inflaba dolorosamente mis pulmones. La sangre latía tan fuertemente en mi cabeza, que parecía que mi cráneo estuviera a punto de explotar en cualquier momento por culpa de la presión."


translate spanish epilogue_mi_5379c1e1:


    "Dejé de sentir mi cuerpo... mis piernas me llevaron por sí mismas."


translate spanish epilogue_mi_1df107da:


    "Al final llegamos a un claro y nos detuvimos."


translate spanish epilogue_mi_2a7f7693:


    "Exhaustos, las muchachas se desplomaron en el suelo, y yo me senté en un árbol caído."


translate spanish epilogue_mi_274d0040:


    "¿Quiénes eran ellos? ¿Personas o demonios? ¿U otra cosa?"


translate spanish epilogue_mi_a42af439:


    "¿Y de dónde provenía ese sonido de estridulación?"


translate spanish epilogue_mi_53300ec8:


    "No podía figurarme quiénes eran con el crepúsculo, pero eso sea probablemente lo mejor."


translate spanish epilogue_mi_b8132fa2:


    "Tras haber recuperado ligeramente mi conocimiento, me acerqué a las muchachas."


translate spanish epilogue_mi_1b7c0a78:


    "Lena estaba tumbada grogui en el suelo. Parecía estar completamente abatida."


translate spanish epilogue_mi_9cc83bd6:


    "Masha estaba sentada, abrazando sus rodillas y meciéndose de lado a lado."


translate spanish epilogue_mi_60b21c95:


    my "Tenemos que hacer algo..."


translate spanish epilogue_mi_38896a66:


    "Dije mientras me sentaba a su lado."


translate spanish epilogue_mi_51e97271:


    ma "Es demasiado tarde... Vamos a morir..."


translate spanish epilogue_mi_5a9cdae9:


    "Susurró con una voz ahogada."


translate spanish epilogue_mi_226dce17:


    my "Todavía estamos vivos, eso significa que aun hay esperanza."


translate spanish epilogue_mi_0a647cc5:


    ma "No, no hay nada que hacer... Todo está perdido..."


translate spanish epilogue_mi_872ed3da:


    "La rodeé con mis brazos y la estreché entre ellos."


translate spanish epilogue_mi_85ff07ec:


    my "No, no lo es..."


translate spanish epilogue_mi_a20cefa7_32:


    "..."


translate spanish epilogue_mi_f54fc1f7:


    "Me senté, mirando fijamente en la oscuridad del bosque."


translate spanish epilogue_mi_cb4ba357:


    "Los saltamontes apenas se les escuchaba ahora."


translate spanish epilogue_mi_dda9e6e8:


    "Si no fuera por la dificultosa respiración de Masha, creería estar muerto."


translate spanish epilogue_mi_661c87b5:


    ma "Discúlpame..."


translate spanish epilogue_mi_c1e898ec:


    "Susurró en voz baja."


translate spanish epilogue_mi_9a8df94b:


    my "¿Por qué?"


translate spanish epilogue_mi_ae39ac0b:


    ma "Por todo... Por ser así... Por todo en lo que fracasamos y no logramos..."


translate spanish epilogue_mi_256eea8e:


    my "No es el mejor momento para eso ahora..."


translate spanish epilogue_mi_0fd16807:


    "Dije suavemente."


translate spanish epilogue_mi_2b77fde2:


    ma "No nos queda mucho tiempo... Así que diría que es el momento adecuado."


translate spanish epilogue_mi_403d5760:


    "Alzó su mirada hacia mí y me sonrió ligeramente."


translate spanish epilogue_mi_4b94df60:


    ma "Perdóname, si sólo no hubiera sido tan egoísta desde un principio..."


translate spanish epilogue_mi_d16187bb:


    "La miré a sus ojos, sus profundamente afligidos ojos, y sentí una lágrima cálida surcando mi mejilla."


translate spanish epilogue_mi_2110e4c0:


    my "No, soy yo quien debería disculparse..."


translate spanish epilogue_mi_a70dec20:


    "Era totalmente ignorante sobre qué decir en tal situación."


translate spanish epilogue_mi_a84db421:


    "Le hice tanto daño a ella, le causé tanto dolor y, ahora, en el último minuto, no pude pensar en otra cosa más que en un vacío «lo siento»."


translate spanish epilogue_mi_852082f5:


    "Estaba abrumado de ira por mi impotencia, por ser incapaz de proteger la única persona que me estimaba."


translate spanish epilogue_mi_bf98df2f:


    my "Sabes, siempre te amé..."


translate spanish epilogue_mi_f538b416:


    ma "Lo sé..."


translate spanish epilogue_mi_a68bf534_1:


    "Dijo ella en voz baja."


translate spanish epilogue_mi_7a73e50c:


    "Masha sonrió de nuevo, aunque su rostro temblaba y las lágrimas recorrían sus mejillas."


translate spanish epilogue_mi_8cbe7e05:


    my "Y todavía te amo..."


translate spanish epilogue_mi_f538b416_1:


    ma "Lo sé..."


translate spanish epilogue_mi_6a903ceb:


    "No podía soportarlo... Quería darme la vuelta, de cerrar los ojos..."


translate spanish epilogue_mi_8e9b5288:


    "¡Pero no puedo permitirme debilidad alguna en estos últimos momentos!"


translate spanish epilogue_mi_d1d01520:


    "Me incliné sobre ella y la besé apasionadamente."


translate spanish epilogue_mi_99de19ed:


    "El tiempo se detuvo para nosotros.{w} ¡Este maldito campamento de pioneros, la oscura noche, todas aquellas criaturas del averno e incluso los violentos saltamontes cesaron de existir!"


translate spanish epilogue_mi_b67167b0:


    "El Universo entero cesó de existir... solamente dos de nosotros quedaban..."


translate spanish epilogue_mi_ce617998_2:


    "..."


translate spanish epilogue_mi_daa91f05:


    "Justo cuando casi quería creer que todo fue tan sólo un mal sueño, los bosques volvieron a la vida con crujidos."


translate spanish epilogue_mi_79516c27:


    "El follaje a lo lejos se estremeció, y una muchedumbre de vagas siluetas emergió de la oscuridad."


translate spanish epilogue_mi_57818ffd:


    "Todavía no podía discernirlas bien, pero estaba seguro de que eran las mismas criaturas que vimos en la parada del autobús."


translate spanish epilogue_mi_aa083e71:


    "Mi cuerpo entero estaba afectado por el desaliento. Estaba preocupado de respirar."


translate spanish epilogue_mi_58343c59:


    ma "Y así... así es como acaba..."


translate spanish epilogue_mi_d8b53ec6:


    "Dijo Masha en voz baja, abrazándome más fuerte."


translate spanish epilogue_mi_6bd68c39:


    "No, ¡este no es el fin!"


translate spanish epilogue_mi_198fcb2e:


    "Repentinamente, me di cuenta de que iba a luchar por ella hasta la última gota de mi sangre."


translate spanish epilogue_mi_1139d9cb:


    "¡Tanto como hubiera una mínima posibilidad de sobrevivir!"


translate spanish epilogue_mi_c7c16c8a:


    "Me puse de pie, tiré de ella para levantarla del suelo y corrí hacia Lena."


translate spanish epilogue_mi_118fa0c9:


    "Ella estaba tumbada, mortalmente pálida, con sus ojos abiertos y no parecía comprender en absoluto qué estaba sucediendo."


translate spanish epilogue_mi_29d61edf:


    "Empecé a sacudirla por los hombros."


translate spanish epilogue_mi_97a2b531:


    my "¡Levántate!"


translate spanish epilogue_mi_7e5612ad:


    "No hubo reacción."


translate spanish epilogue_mi_fefd014c:


    "Le di unas cuantas bofetadas y ella dejó ir un apagado gemido."


translate spanish epilogue_mi_09ccb2a2:


    my "¡Levántate! ¡Ahora!"


translate spanish epilogue_mi_27ba2774:


    "Mientras alzaba a Lena, se me quedó mirando fijamente con una mirada grogui."


translate spanish epilogue_mi_1f4d46ef_1:


    my "¡Corramos!"


translate spanish epilogue_mi_e900bc14:


    "Agarré a las muchachas por sus manos y corrí sin rumbo fijo, cuanto más lejos para «ellas», mejor."


translate spanish epilogue_mi_7a43a1d3:


    "Me caía, me levantaba otra vez, ayudando a Lena y Masha a levantarse también, cortando a través del denso follaje. En ocasiones, el ruido se desvanecía, dejándonos solamente con el silencioso estridular de los saltamontes."


translate spanish epilogue_mi_1464e77a:


    "La luz parpadeó en la distancia."


translate spanish epilogue_mi_a20cefa7_33:


    "..."


translate spanish epilogue_mi_2bf52109:


    "Pronto llegamos a un pequeño claro, completamente resguardado por árboles. Había un edificio que de alguna manera se parecía a un jardín de parvulitos."


translate spanish epilogue_mi_74cb7d4d:


    my "¿Qué es esto?"


translate spanish epilogue_mi_46835ec1:


    "Tartamudeé."


translate spanish epilogue_mi_809c3b70:


    un "El edificio del antiguo campamento..."


translate spanish epilogue_mi_0cb7a1ff:


    "Dijo Lena débilmente."


translate spanish epilogue_mi_bb7631a3:


    "Parecía que lentamente estaba volviendo en sí."


translate spanish epilogue_mi_7c3c1f20:


    "Me detuve, dudando."


translate spanish epilogue_mi_153ace4d:


    "Estaba claro que era bastante ingenuo creer que encontraríamos protección ahí... era obvio que no podíamos estar seguros en ninguna parte con todo lo que estaba sucediendo."


translate spanish epilogue_mi_aa5cdda1:


    "Por otra parte, estábamos al borde de desplomarnos.{w} Especialmente las muchachas."


translate spanish epilogue_mi_4015903e:


    "Hice unos cuantos pasos con dudas hacia el edificio, cuando el ruido de crujidos detrás de mí llegó a mis oídos. Me quedé de piedra, incapaz de darme la vuelta."


translate spanish epilogue_mi_84eb76e0:


    "Los crujidos crecían en intensidad, y de repente escuchamos el estridular diabólico."


translate spanish epilogue_mi_1aaec2c1:


    "Hice un esfuerzo sobrehumano y me di la vuelta."


translate spanish epilogue_mi_e9aee5ea:


    "Una muchedumbre estaba detrás de mí...{w} Una muchedumbre de muchachas pequeñas."


translate spanish epilogue_mi_dc80e866:


    "O mejor dicho, una muchedumbre de Ulyanas..."


translate spanish epilogue_mi_fddbd92f:


    "Todas ellas eran exactamente iguales que la misma Ulyana que vi esta mañana... con el mismo rostro hecho jirones y la horrible sonrisa desencajada."


translate spanish epilogue_mi_9e72eb1c:


    "Y todas ellas hacían el mismo infernal sonido, el grillar de los saltamontes, un ruido diabólico que helaba mi corazón."


translate spanish epilogue_mi_52648267:


    "Nos quedamos de piedra quietos, incapaces de dar siquiera un solo paso atrás."


translate spanish epilogue_mi_b22355a1:


    "Repentinamente, una de las «Ulyanas» vino hacia delante saliendo de la multitud y mostró el brazo de detrás de ella...{w} con la cabeza cercenada de Router."


translate spanish epilogue_mi_f58f26f9:


    "Una mueca de horror estaba congelada permanentemente en su rostro."


translate spanish epilogue_mi_67e4b031:


    "Casi me desmayo cuando empezó a hablar:"


translate spanish epilogue_mi_24eb79a9:


    us "¡Hola, Semyon! ¿Qué tal estás?"


translate spanish epilogue_mi_64c1dacf:


    "Su voz se sentía como una bala perforando justo a través de mi cerebro. Grité y salí huyendo hacia el edificio del antiguo campamento, arrastrando a las muchachas conmigo."


translate spanish epilogue_mi_85b7581c:


    "Debo admitir que hacía un rato que dejé completamente de pensar sobre Masha o Lena."


translate spanish epilogue_mi_6fa07538:


    "Tal vez ellas estuvieran gritando también, quizá fueran presas del terror... No lo sé."


translate spanish epilogue_mi_e3a0e8e4:


    "La cosa más importante ahora, era alejarse tanto como fuera posible de estos monstruos."


translate spanish epilogue_mi_2da23a26:


    "Entramos volando en el edificio, e inmediatamente el suelo desapareció bajo nuestros pies y caí en la oscuridad..."


translate spanish epilogue_mi_a20cefa7_34:


    "..."


translate spanish epilogue_mi_a20cefa7_35:


    "..."


translate spanish epilogue_mi_6c66893a:


    "Me desperté en completa oscuridad."


translate spanish epilogue_mi_a490ddde:


    "Mi cuerpo entero me dolía, especialmente mis piernas, pero de alguna forma me las apañé para ponerme en pie, apoyándome contra la pared."


translate spanish epilogue_mi_20b70770:


    "El sonido de las «Ulyanas» no se escuchaba en ninguna parte, lo cual me calmó un poco."


translate spanish epilogue_mi_7f05575c:


    "Llamé con una voz aguda:"


translate spanish epilogue_mi_02b36d0b:


    my "Masha... Lena..."


translate spanish epilogue_mi_4fc4bd86:


    "Escuché un movimiento próximo y sentí que alguien sujetaba mi pierna."


translate spanish epilogue_mi_23250185:


    "Instintivamente salté hacia atrás, pero a la vez una voz débil provino de debajo:"


translate spanish epilogue_mi_5854ece9:


    ma "¿Estamos vivos todavía...?"


translate spanish epilogue_mi_00cfd6e5:


    my "Eso parece."


translate spanish epilogue_mi_a313a700:


    "Ayudé a Masha a incorporarse."


translate spanish epilogue_mi_c8171111:


    "Repentinamente, una luz brillante prendió un par de metros más allá de mí."


translate spanish epilogue_mi_97874b9d:


    "Me apoyé contra la pared, temblando, pero inmediatamente me percaté de que era Lena, quien resultó encender una linterna. ¿De dónde la sacó?"


translate spanish epilogue_mi_9341d7f5:


    my "¿Dónde la conseguiste...?"


translate spanish epilogue_mi_70effa3b:


    "Lena se acercó, cojeando."


translate spanish epilogue_mi_8f0774a8:


    un "La cogí solo por si acaso... ayer..."


translate spanish epilogue_mi_956b67a0:


    "Agarré la linterna de ella e iluminé los alrededores."


translate spanish epilogue_mi_1fddf6ec:


    "Estábamos en algún tipo de túnel... las paredes estaban cubiertas por cables, bombillas apagadas colgaban en apliques desde el techo."


translate spanish epilogue_mi_64a7a549:


    ma "¿Dónde estamos?"


translate spanish epilogue_mi_365e331e:


    my "No lo sé... Parece algún tipo de refugio antiaéreo..."


translate spanish epilogue_mi_cf6427db:


    "Miré arriba y vi un agujero bastante grande, más allá del cual apenas se podía distinguir el pasillo del edificio del antiguo campamento."


translate spanish epilogue_mi_9b78bde9:


    "El suelo probablemente se había pudrido por el tiempo, y caímos por aquí abajo."


translate spanish epilogue_mi_02fb066a:


    "A primera vista, no había forma de escalar para volver... demasiado alto, y no había nada a lo que agarrarse."


translate spanish epilogue_mi_b5a405a0:


    my "Esto es lo que hay, ¿avanzamos?"


translate spanish epilogue_mi_bbde6bdd:


    "Pregunté, sin esperar realmente una respuesta."


translate spanish epilogue_mi_ec980db8:


    ma "No lo sé... Pero tendríamos que comprobarlo."


translate spanish epilogue_mi_2264a05e:


    "Iluminé con la linterna delante nuestro y di unos cuantos pasos inciertos."


translate spanish epilogue_mi_bfec5f9c:


    "Mi cuerpo entero estaba cubierto de moratones y arañazos, pero aun así podía caminar."


translate spanish epilogue_mi_4ef21e5f:


    un "Ay..."


translate spanish epilogue_mi_bc66543c:


    "Lena chilló detrás de mí."


translate spanish epilogue_mi_1d810d31_1:


    my "¿Qué pasa?"


translate spanish epilogue_mi_4bad9ddd:


    un "Creo que me torcí mi tobillo..."


translate spanish epilogue_mi_d2070307:


    "Sinceramente, estaba esperando a que el siguiente diálogo fuera un cliché algo así como «dejádme atrás, sólo seré una carga», pero obviamente ella nunca diría algo así."


translate spanish epilogue_mi_7f15c298:


    my "Vale, apoyáte en mí."


translate spanish epilogue_mi_b462a513:


    "Fui hasta Lena y le ofrecí mi hombro."


translate spanish epilogue_mi_2d5feb7b:


    "Pronto las paredes de piedra dieron lugar a paredes de madera, y acabamos llegando a un tipo de mina."


translate spanish epilogue_mi_ff0e72ed:


    ma "¡Tomemos un descanso!"


translate spanish epilogue_mi_347e63c2:


    "Pidió Masha."


translate spanish epilogue_mi_fd1bdbfc:


    "Con cuidado bajé a Lena al suelo y me senté cerca."


translate spanish epilogue_mi_b1f368d2:


    my "Bueno, por lo menos esos monstruos no se les ha visto en ninguna parte..."


translate spanish epilogue_mi_710a57f3:


    ma "Pero, ¿por cuánto tiempo más...?"


translate spanish epilogue_mi_6450d358_1:


    "Suspiré profundamente y cerré mis ojos."


translate spanish epilogue_mi_b587d808:


    ma "¿Qué crees que está ocurriendo aquí?"


translate spanish epilogue_mi_49190220:


    "La voz de Masha estaba temblando, pero aun así sonaba más o menos con confianza."


translate spanish epilogue_mi_7bfb3264:


    "Es como si ella estuviera tan exhausta y devastada, que ya no hubiera fuerzas ni para sentir miedo."


translate spanish epilogue_mi_576daca7:


    my "No lo sé... Y sinceramente, ¡no lo quiero saber!"


translate spanish epilogue_mi_18e5c515:


    ma "Pero... ¿escaparemos?"


translate spanish epilogue_mi_6731092c:


    my "¡Definitivamente!"


translate spanish epilogue_mi_0ffb9fba:


    "No podía ver su rostro, pero estaba seguro de que ella sonreía."


translate spanish epilogue_mi_e5153f6b:


    "De repente la linterna parpadeó y se apagó."


translate spanish epilogue_mi_1c74b1ee:


    "La golpeé con la palma de la mano varias veces, las paredes de la mina se iluminaron con una tenue luz una vez más, ¡e inmediatamente me percaté de que algo iba mal!"


translate spanish epilogue_mi_df6b624b:


    "¡Lena no estaba en ninguna parte!"


translate spanish epilogue_mi_6ec38792:


    my "Lena desapareció..."


translate spanish epilogue_mi_9db6b90d:


    "Susurré."


translate spanish epilogue_mi_20f54ffc_4:


    ma "¿Qué?"


translate spanish epilogue_mi_cfb3fc4f:


    "Masha me agarró por la manga y miró fijamente en el punto en que Lena había estado hace un momento atrás."


translate spanish epilogue_mi_a4a54acd:


    ma "¿Cómo? ¿Pero cómo?"


translate spanish epilogue_mi_72683e3e:


    "Comenzó a llorar silenciosamente."


translate spanish epilogue_mi_83e118dc:


    ma "Vamos a morir... ¡Definitivamente vamos a morir!"


translate spanish epilogue_mi_6fca0341:


    "Yo también tenía muchas ganas de romper a llorar."


translate spanish epilogue_mi_0b13bed1:


    "Las tinieblas nos rodearon, encerrándonos."


translate spanish epilogue_mi_784f3191:


    "La linterna se apagaba gradualmente... y con ella, nuestras opciones de sobrevivir."


translate spanish epilogue_mi_57aee98a:


    my "Levántate, es la hora de irse. Las pilas se terminarán pronto."


translate spanish epilogue_mi_ed94e63d:


    "En un par de minutos más tarde llegamos a una bifurcación."


translate spanish epilogue_mi_dd3ac820:


    my "¿Qué demonios...? ¡¿Acaso esto es también un laberinto?!"


translate spanish epilogue_mi_2eb0d079:


    "Siseé en voz baja."


translate spanish epilogue_mi_0238f96f:


    ma "Volvamos atrás, ¿quizás?"


translate spanish epilogue_mi_a595d43b:


    my "No. De ninguna manera."


translate spanish epilogue_mi_a3a11b35:


    "Masha se apretó contra mi aun más."


translate spanish epilogue_mi_4ea6e0a7:


    ma "¿Adónde pues?"


translate spanish epilogue_mi_326ad2f9_2:


    my "No lo sé..."


translate spanish epilogue_mi_f7db3185:


    "Pero teníamos que escoger de todas formas."


translate spanish epilogue_mi_d59a939e:


    "Y decidí ir hacia la derecha."


translate spanish epilogue_mi_ab9caf40:


    "Poco después, llegamos a otra bifurcación."


translate spanish epilogue_mi_5dea2762:


    ma "¿Y ahora dónde?"


translate spanish epilogue_mi_7a1ec301:


    my "Mantengamos la dirección que tomamos antes..."


translate spanish epilogue_mi_3507d86b:


    "Habíamos estado vagando por mucho rato, no nos quedaban ni fuerzas ni voluntad para movernos."


translate spanish epilogue_mi_5afc66a6:


    "Estaba tan terriblemente sediento, que estaba dispuesto a chupar la humedad de las paredes."


translate spanish epilogue_mi_9e241c26:


    ma "Sentémonos... solamente un poco... un poquitín..."


translate spanish epilogue_mi_367b10bc:


    my "¡No podemos! ¡Debemos continuar!"


translate spanish epilogue_mi_493613af:


    ma "Pero solamente un minuto..."


translate spanish epilogue_mi_84b04f60:


    "Masha suplicó angustiada."


translate spanish epilogue_mi_e9e6e7f9_2:


    my "Está bien..."


translate spanish epilogue_mi_54517287:


    "Ella bajó su cabeza encima de mis hombros."


translate spanish epilogue_mi_3c3d66ba:


    "La linterna estaba parpadeando más y más a menudo, así que decidí apagarla por ahora, para ahorrar la energía de las pilas."


translate spanish epilogue_mi_58343c59_1:


    ma "En fin..."


translate spanish epilogue_mi_ce617998_3:


    "..."


translate spanish epilogue_mi_2ffc89fb:


    "No sé cuánto tiempo transcurrió."


translate spanish epilogue_mi_245bdc9c:


    "Desesperadamente quería dormir. Mis ojos se cerraban por sí solos."


translate spanish epilogue_mi_fd42b024:


    "Cómo nos conocimos..."


translate spanish epilogue_mi_29183e7d:


    "De eso hacía ya mucho tiempo..."


translate spanish epilogue_mi_1f88cd59:


    my "¿Te acuerdas...?"


translate spanish epilogue_mi_0be4039d:


    "Pregunté en un susurro."


translate spanish epilogue_mi_b6f57941:


    ma "¿Qué...?"


translate spanish epilogue_mi_059b8596:


    "La voz de Masha estaba temblorosa."


translate spanish epilogue_mi_d00cf9c5:


    my "Cómo nos conocimos..."


translate spanish epilogue_mi_bfd44c3c:


    ma "Sí..."


translate spanish epilogue_mi_4d08847b:


    "Trató de reír, pero inmediatamente comenzó a toser."


translate spanish epilogue_mi_c09c2b0c:


    ma "Voy a dormir un rato, ¿vale?"


translate spanish epilogue_mi_db20ce9f:


    my "Sólo por un rato, o te dormirás un día entero otra vez..."


translate spanish epilogue_mi_8229d523:


    ma "Bueno, ¿pero tú me despertarás...?"


translate spanish epilogue_mi_5e3404c1:


    my "Por supuesto..."


translate spanish epilogue_mi_2f0abbfd:


    "La besé y cerré mis ojos."


translate spanish epilogue_mi_20a50466:


    "El olvido se estaba acercando, pero no me importó... fui transportado atrás en el tiempo, justo en aquel día..."


translate spanish epilogue_mi_a20cefa7_36:


    "..."


translate spanish epilogue_mi_a20cefa7_37:


    "..."


translate spanish epilogue_mi_dc909087:


    "Mi cuerpo entero estaba dolorido..."


translate spanish epilogue_mi_ccf84f7d:


    "Quizá mi cerebro no pudo descansar tranquilamente, debido a que estuvo distraído por miles de señales de mis terminaciones nerviosas."


translate spanish epilogue_mi_ca2bcd31:


    "De alguna forma me alcé y desperté a Masha."


translate spanish epilogue_mi_20c2627d:


    "Después de todo, estábamos juntos para sobrevivir..."


translate spanish epilogue_mi_d737a002:


    "¿Cuánto tiempo he estado en esta mina...?"


translate spanish epilogue_mi_48f216c9:


    "Bifurcación tras bifurcación, túnel tras túnel."


translate spanish epilogue_mi_2c9bce0d:


    "Masha estaba susurrando algo todo el rato."


translate spanish epilogue_mi_c8ad0429:


    "«A la izquierda»... «A la derecha»... «A la izquierda»... «A la derecha»..."


translate spanish epilogue_mi_d9534268:


    "Parecía que ya no era capaz de aguantar hacia otra esquina..."


translate spanish epilogue_mi_b8aa830c:


    "Pero, de repente, la linterna iluminó un pasillo estrecho en la oscuridad, y pronto nos hallamos en una sala abierta."


translate spanish epilogue_mi_853bf27c:


    ma "¡Mira!"


translate spanish epilogue_mi_8060d8b7:


    "Masha estaba indicando algo en la oscuridad."


translate spanish epilogue_mi_c367b132:


    "Apunté con la linterna ahí y vi un pañuelo tirado en el suelo."


translate spanish epilogue_mi_a8f16511:


    ma "Este no es..."


translate spanish epilogue_mi_6d40bd4c:


    my "¡No! ¡Vayámonos!"


translate spanish epilogue_mi_cd9d1345:


    "Al menos nos escapamos del laberinto y nos hallamos en una pequeña habitación."


translate spanish epilogue_mi_6fffd30f:


    "Habían botellas rotas por todo el suelo, tuberías a lo largo de las paredes y al final de éstas unas válvulas oxidadas. Un revoltijo de grafitis estaban garabateados por las paredes."


translate spanish epilogue_mi_c51d3130:


    my "Al menos es un cambio de aires..."


translate spanish epilogue_mi_9da1463d:


    "Suspiré."


translate spanish epilogue_mi_a2d6a840:


    "La linterna estaba casi muerta y tenía que sacudirla cada diez segundos para recuperar ligeramente su energía de las pilas."


translate spanish epilogue_mi_ccc4b1b1:


    "Exhausta, Masha se dejó caer al suelo."


translate spanish epilogue_mi_7ddfed17:


    my "No podemos parar. ¡Estoy seguro que la salida está cerca por aquí!"


translate spanish epilogue_mi_e98886ba:


    ma "¿Cómo lo sabes...?"


translate spanish epilogue_mi_3a8d7079:


    "La miré."


translate spanish epilogue_mi_36c6417c:


    "Obviamente, ya no tenía más fuerzas... Masha estaba luchando contra su propio cansancio y podía perder el conocimiento en cualquier momento."


translate spanish epilogue_mi_b87232f7:


    my "¡Lo sé!"


translate spanish epilogue_mi_046fd47c:


    "Sonreí, tratando de ser alentador."


translate spanish epilogue_mi_fdd1561c:


    ma "Bueno... Si es así..."


translate spanish epilogue_mi_2d188ff6:


    "Ella se inclinó contra la pared y se levantó a duras penas."


translate spanish epilogue_mi_d86bb40d:


    my "Siéntate aquí por ahora, miraré que hay más allá..."


translate spanish epilogue_mi_d2de1f1d:


    ma "Estoy preocupada..."


translate spanish epilogue_mi_811b3088:


    "Masha temblaba."


translate spanish epilogue_mi_376a198b:


    my "¡No me voy lejos!"


translate spanish epilogue_mi_600c1cfd:


    "Alzó sus ojos hacia mí, llenos de esperanza, y asintió casi imperceptiblemente."


translate spanish epilogue_mi_e3edc783:


    "Miré a mi alrededor y me percaté de una pesada puerta de hierro."


translate spanish epilogue_mi_44b1af16:


    "Mis intentos por abrirla no resultaron... estaba cerrada completamente por el óxido."


translate spanish epilogue_mi_c3fa1cc1:


    "Ufff, desearía que aquella tubería estuviera aquí ahora..."


translate spanish epilogue_mi_2f004e19:


    "¡Pero no había otra opción!"


translate spanish epilogue_mi_a331b385:


    "Apoyé uno de mis pies en la pared y lancé todo mi peso contra el pomo.{w} Mis músculos se marcaron, mi frente estaba llena de sudor, mi visión se oscureció..."


translate spanish epilogue_mi_d24acfc5:


    "Pero la puerta no se desplazó, tan solamente hizo un ruido hueco."


translate spanish epilogue_mi_3fd97b24:


    my "Muy bien, espera un segundo..."


translate spanish epilogue_mi_e0d77cc3:


    "Grité hacia Masha, jadeando por falta de aire."


translate spanish epilogue_mi_b203e2e6:


    my "Tengo que encontrar algún tipo de palanca."


translate spanish epilogue_mi_cbec9827:


    "Corrí hacia Masha, me incliné hacia ella y le sonreí."


translate spanish epilogue_mi_f977f9ea:


    my "¡Defitinivamente hay una salida detrás de esa puerta!"


translate spanish epilogue_mi_ad4d86a9:


    ma "Ahh..."


translate spanish epilogue_mi_21d01a89:


    "Me miró con cansancio."


translate spanish epilogue_mi_c0cfd78b:


    "Salí corriendo fuera de la habitación y comencé a explorar la mina en búsqueda de algo que sirviera."


translate spanish epilogue_mi_55ef4138:


    "La linterna estaba a punto de apagarse y, habiendo parpadeado por última vez, finalmente se fue..."


translate spanish epilogue_mi_f94d2e95:


    "Entré en pánico y comencé correr torpemente alrededor, tratando de encontrar el camino de vuelta."


translate spanish epilogue_mi_e071baa4:


    "Sin embargo, seguí corriendo hacia las paredes y no pude recordar muy bien como llegué hasta aquí."


translate spanish epilogue_mi_c391fe01:


    my "¡Masha! ¡Masha!"


translate spanish epilogue_mi_80d8e32e:


    "Chillé, con la esperanza de que me escuchara."


translate spanish epilogue_mi_1b079b4b:


    "De repente un sonido se escuchó detrás de mí, una luz apareció y mi sombra se proyectó sobre el suelo, extendiéndose lejos hacia la oscuridad."


translate spanish epilogue_mi_03cecdb6:


    "Estaba paralizado de miedo."


translate spanish epilogue_mi_67aec98b:


    "Escuché una voz familiar."


translate spanish epilogue_mi_2e95fbdc:


    un "¿Qué pasa Semyon, te has perdido?"


translate spanish epilogue_mi_b5c21e08:


    "Lentamente me di la vuelta y vi a Lena, quien sostenía una antorcha de fuego en su mano."


translate spanish epilogue_mi_19ff5743:


    "Su rostro estaba desfigurado por una carcajada inhumana."


translate spanish epilogue_mi_d7da412d:


    un "¡Vengo a por ti!"


translate spanish epilogue_mi_c256bf58:


    "Esta era la misma Lena que vi en la plaza la última tarde."


translate spanish epilogue_mi_2f54a629:


    "La misma que en la cabaña de Olga Dmitrievna."


translate spanish epilogue_mi_48cecf56:


    "Ésa era Lena y, a la vez, no era Lena..."


translate spanish epilogue_mi_363ad07d:


    "Esta criatura era más bien como aquellas «Ulyanas»..."


translate spanish epilogue_mi_cdd889d8:


    un "¡Vengo a por ti!"


translate spanish epilogue_mi_9148c42c:


    my "Tú... tú... ¿quién eres?"


translate spanish epilogue_mi_28b5ea24:


    "Me apreté arrinconándome."


translate spanish epilogue_mi_50e370da:


    un "¡Soy Lena! ¿No me reconoces?"


translate spanish epilogue_mi_a0df0b59:


    "Rompió en una carcajada endemoniada."


translate spanish epilogue_mi_32ddb218:


    un "¿Me olvidaste completamente mientras te divertías con ellas?"


translate spanish epilogue_mi_84b9a0ea:


    my "Yo...yo..."


translate spanish epilogue_mi_cfc352ff:


    "Las palabras se atascaban en mi garganta."


translate spanish epilogue_mi_3198ebd7:


    un "¡Muy bien! ¡No queda nadie ahora! ¡Solamente nosotros dos!"


translate spanish epilogue_mi_47c53372:


    my "Entonces tú... ¿tú lo hiciste todo...?"


translate spanish epilogue_mi_ef814bbe:


    un "¡Bingo! No todo claro..."


translate spanish epilogue_mi_1346f417:


    "Por un segundo su rostro se volvió culpable, pero luego volvió a sonreír."


translate spanish epilogue_mi_ba906862:


    un "¡Slavya fue la primera! ¡Ella fue siempre tan molesta! ¡Ella era Doña Confiabilidad! ¡Diligente, trabajadora, siempre en todas partes, llevándose bien con todos! ¡Puajj! Muchacho, no sabes cómo chillaba cuando le corté los brazos, ¿eh? ¡Qué lástima que no estabas para verlo todo!"


translate spanish epilogue_mi_c255c3bb:


    "Su boca comenzó a temblar, su saliva goteaba y sus ojos se quedaron en blanco."


translate spanish epilogue_mi_a183bd23:


    un "Y luego Alisa... ¡«Todo el mundo me molesta, no necesito a nadie»! ¡Ahora está colgada tranquilamente sola en un árbol! Sin embargo, no fue fácil subirla hasta allí arriba, ¡pero causó un mayor efecto! Cómo chillabais todos, ¿eh? ¡Como cerdos en el matadero!"


translate spanish epilogue_mi_34cb066a:


    "El rostro de Lena estaba retorcido en una sonrisa de placer enfermo."


translate spanish epilogue_mi_16487ed9:


    un "Muy bien, solamente tengo que ocuparme de esa puta de Miku... ¡y entonces se acabó!"


translate spanish epilogue_mi_4879df12:


    "Se sacó de detrás de ella un cuchillo de carnicero gigante, cubierto con sangre."


translate spanish epilogue_mi_4fd3d6cb:


    un "Bueno, en realidad..."


translate spanish epilogue_mi_604a0a8b:


    "Me pareció que se distrajo con los pensamientos."


translate spanish epilogue_mi_3c2cd797:


    un "No sé qué es esa mierda con la forma de Ulyana... Ella y los demás pioneros no fueron muertos por mí... ¿Pero acaso de verdad importa ahora?"


translate spanish epilogue_mi_9daeafd9:


    "Sacudió la antorcha y la llama se extendió por un segundo."


translate spanish epilogue_mi_7e9959b4:


    un "Espera aquí, ¡seré rápida!"


translate spanish epilogue_mi_006d380a:


    "Su carcajada endemoniada se escuchó detrás de mí."


translate spanish epilogue_mi_eb5af906:


    "¡Eso significa que va detrás de Masha!"


translate spanish epilogue_mi_6bbab6e5:


    "¡Debo hacer algo rápidamente!"


translate spanish epilogue_mi_64fdda0c:


    "Pero no tengo ni la más absoluta idea de hacia dónde correr... la oscuridad me desorientaba completamente."


translate spanish epilogue_mi_b632416a:


    "¡A la mierda!"


translate spanish epilogue_mi_c66c511c:


    "Corrí por el camino por el que presumí que Lena fue."


translate spanish epilogue_mi_0eae989f:


    "Tras sólo un corto trozo de camino, me choqué dolorosamente contra una pared. Me di la vuelta, tratando de moverme lentamente, pero me tropecé con otro obstáculo, me caí y grité dolorido."


translate spanish epilogue_mi_999e1188:


    "Intentando ponerme en pie, me di cuenta de que no podía cargar con mi peso en mi pierna izquierda.{w} Parecía que me la había torcido..."


translate spanish epilogue_mi_4b8b6c31:


    "Traté de gatear pero fue mal... todo el suelo de la mina estaba cubierto con piedras afiladas, vías oxidadas y tablas rotas."


translate spanish epilogue_mi_a3fa3479:


    "Tras solamente un corto camino, mis brazos estaban desgarrados como un pedazo de carne sangrante y mis ojos no se podían abrir debido al polvo y la arena."


translate spanish epilogue_mi_2d13ba42:


    "Busqué a tientas en mis bolsillos y saqué la caja de cerillas."


translate spanish epilogue_mi_47fd5c48:


    "Traté de prender una cerilla con mis temblorosas manos, pero no lograba encenderlas."


translate spanish epilogue_mi_10e3d62a:


    my "Tranquilízate... Tranquilízate..."


translate spanish epilogue_mi_d09690e2:


    "Murmuré."


translate spanish epilogue_mi_0187584b:


    "Al final, tras el quinto intento, la mina se iluminó por una tenue luz."


translate spanish epilogue_mi_58360f3f:


    "Velozmente me orienté y averigüé hacia dónde tenía que correr."


translate spanish epilogue_mi_fc8120bd:


    "Me parecieron pasar solamente unos pocos segundos hasta que encontré la habitación donde dejé a Masha."


translate spanish epilogue_mi_8c229ba9:


    "La última cerilla se agotó, pero tuve suficiente tiempo para ver a Lena, alzando su cuchillo de carnicero por encima de Masha."


translate spanish epilogue_mi_9c2f56ee:


    "Salí corriendo hacia ellas, gritando y me abalancé hacia delante."


translate spanish epilogue_mi_deada465:


    "Mis manos de repente colisionaron con algo. Caí al suelo y empecé a golpear a todo a mi alrededor."


translate spanish epilogue_mi_67c9b16b:


    "La mayoría de mis puñetazos no acertaron en el blanco, pero entonces dieron con algo blando. Escuché un gemido y un silencio sepulcral se hizo."


translate spanish epilogue_mi_3be834f5:


    "Rodé hacia atrás y comencé a respirar profundamente, inhalando el aire seco con voracidad."


translate spanish epilogue_mi_22b173a8:


    my "¿Masha?"


translate spanish epilogue_mi_805bb83c:


    "La llamé."


translate spanish epilogue_mi_a4178908:


    "Escuché sollozos en voz baja."


translate spanish epilogue_mi_0019cb32:


    my "¿Sigues viva?"


translate spanish epilogue_mi_268d1f74:


    ma "Sí..."


translate spanish epilogue_mi_e3a26dcc:


    "Susurró."


translate spanish epilogue_mi_d1ec164c:


    my "¿Te hirió?"


translate spanish epilogue_mi_ab472d5e:


    ma "No..."


translate spanish epilogue_mi_055645cd:


    "Comencé a buscar a tientas en la oscuridad y pronto encontré la antorcha y un encendedor."


translate spanish epilogue_mi_3401954a:


    "Sombras sin forma empezaron a bailar en las paredes, y vi el cuerpo de Lena desparramado en el suelo.{w} Parecía estar viva."


translate spanish epilogue_mi_3aa51417:


    "Iba a acabar con ella."


translate spanish epilogue_mi_e3f3c64d:


    ma "No lo hagas..."


translate spanish epilogue_mi_f72c4a89:


    "Masha gateó justo hasta mí y me agarró de mi pierna."


translate spanish epilogue_mi_be4dbf99:


    my "Pero no podemos simplemente..."


translate spanish epilogue_mi_e3f3c64d_1:


    ma "No necesitas..."


translate spanish epilogue_mi_71ab98c7:


    "Desde luego, Lena no era la culpable de todo..."


translate spanish epilogue_mi_9218c96e:


    "¡Pero era el demonio personificado!"


translate spanish epilogue_mi_e8f60892:


    "No podemos permitir que una humana como ella (si es que podemos todavía llamarla humana) viva."


translate spanish epilogue_mi_2046e79a:


    "Pero cuando miré a Masha, me di cuenta de que no podía... sencillamente no podía..."


translate spanish epilogue_mi_1c94cf88:


    "Cogí el cuchillo de carnicero del suelo, le di un par de vueltas en mis manos, y decidí que podría servirnos como un perfecto pasamanos."


translate spanish epilogue_mi_56ceedde:


    "Yendo hacia la puerta, lo clavé atascado en el pomo giratorio y empujé tan fuertemente como podía."


translate spanish epilogue_mi_6904897c:


    "La puerta chirrió y se abrió. Aire fresco invadió la habitación."


translate spanish epilogue_mi_92b62876:


    my "Estamos salvados..."


translate spanish epilogue_mi_44e2cc8f:


    "Retorné con Masha, la ayudé a incorporarse y abandonamos la habitacón, dejando a Lena tumbada inconsciente."


translate spanish epilogue_mi_800c5a39:


    "Más allá de la puerta, había un corto pasillo con una escalera al final de éste, la cual nos llevaba a una rejilla."


translate spanish epilogue_mi_c6d7deb7:


    "Trepé y la empujé con todas mis fuerzas."


translate spanish epilogue_mi_379678b6:


    "La rejilla cayó con un estruendo, trepamos arriba."


translate spanish epilogue_mi_790139a0:


    "Me estiré en la hierba, exhausto y miré alrededor."


translate spanish epilogue_mi_b402b18c:


    "Otra vez esta plaza, otra vez este campamento..."


translate spanish epilogue_mi_144666f0:


    "La brillante alba roja estaba amaneciendo por el este, quemando las copas de un bosque distante."


translate spanish epilogue_mi_698a2bad:


    "Las últimas estrellas me guiñaron, ignorantes de todo lo que había ocurrido aquí, en este infierno."


translate spanish epilogue_mi_9f24a4f9:


    "Masha descansó su cabeza sobre mi hombro y señaló hacia el cielo con su temblorosa mano."


translate spanish epilogue_mi_58999707:


    ma "Desearía que estuviéramos allí..."


translate spanish epilogue_mi_a7b77934:


    "A pesar de que escapamos de la mazmorra, nada terminó... muchedumbres de Ulyanas zombis están merodeando por el campamento, una Lena loca está abajo y el cielo sabe cuántas cosas demoníacas más nos encontraremos aquí."


translate spanish epilogue_mi_26d8a756:


    ma "Realmente quiero dormir..."


translate spanish epilogue_mi_3b7315ab:


    "No sabía qué responderle."


translate spanish epilogue_mi_85ad9044:


    "Porque no sé adónde correr, no sé adónde podemos estar seguros."


translate spanish epilogue_mi_d036f439:


    "No sé cómo retornar a nuestro mundo..."


translate spanish epilogue_mi_7956f114:


    my "Duerme..."


translate spanish epilogue_mi_bc298dba:


    "Acaricié la cabeza de Masha."


translate spanish epilogue_mi_c4c2efec:


    ma "Escucha, ¡esto sería un guión fantástico para una película!"


translate spanish epilogue_mi_33b4df45:


    "Se rio a carcajadas."


translate spanish epilogue_mi_5f9f4f73:


    "La miré horrorizado."


translate spanish epilogue_mi_887f26c3:


    "Masha estaba sonriendo."


translate spanish epilogue_mi_bc65e8e5:


    "Pero su sonrisa no era diabólica como la otra de Lena. Era pura, honesta, tal vez incluso infantil."


translate spanish epilogue_mi_9de01d70:


    "Todo su cansancio se fue y sus ojos estaban brillando con chispas de alegría."


translate spanish epilogue_mi_7122db66:


    my "¿Qué... te ha ocurrido?"


translate spanish epilogue_mi_70580914:


    "Apenas podía hablar."


translate spanish epilogue_mi_8ec442e1:


    ma "¡Todo va bien!"


translate spanish epilogue_mi_bd43e6c0:


    "Se apoyó de nuevo en mi hombro."


translate spanish epilogue_mi_9d008617:


    ma "¡Ésa era la última toma!"


translate spanish epilogue_mi_35287c9d:


    "Repentinamente, sentí cómo las fuerzas me abandonaban."


translate spanish epilogue_mi_23661344:


    "Mi visión se oscurecía, mi consciencia comenzó a alejarse flotando a alguna parte."


translate spanish epilogue_mi_f491b17d:


    "Estaba tumbado en silencio y sólo el sonido de la respiración de Masha se podía escuchar."


translate spanish epilogue_mi_b2f531df:


    "No la estridulación de los saltamontes... como si todos ellos se hubieran muerto al fin... sino solamente la ininterrumpida respiración de Masha."


translate spanish epilogue_mi_99fe2e9b:


    "Y su sonrisa..."


translate spanish epilogue_mi_a20cefa7_38:


    "..."


translate spanish epilogue_mi_a20cefa7_39:


    "..."


translate spanish epilogue_mi_7ffeaac0:


    "Me debatí para abrir los ojos."


translate spanish epilogue_mi_9af7bec5:


    "Una brillante luz lunar se reflejaba en el agua, alegremente saltando sobre las olas, una liviana brisa nocturna estaba soplando y, en algún lugar lejano, una lechuza ululando."


translate spanish epilogue_mi_ea099931:


    "Miré a Masha, quien dormía pacíficamente al lado, cubierta por mi camisa."


translate spanish epilogue_mi_654b8602:


    "Menudo sueño..."


translate spanish epilogue_mi_38e7da2f:


    "Mi teléfono móvil mostraba a media luz la hora... ¡las tres de la madrugada!"


translate spanish epilogue_mi_846c7ecb:


    "Mejor que nos vayamos a la cama, o mañana Olga Dmitrievna nos hará..."


translate spanish epilogue_mi_d3fe6684:


    "Decidí simplemente despertar a Masha, pero de repente me percaté de un guión abierto con un bolígrafo a su lado."


translate spanish epilogue_mi_6c272b09:


    "Eché un vistazo rápido por diversas páginas y estaba horrorizado... ¡era completamente idéntico a lo que ocurrió en mi sueño!"


translate spanish epilogue_mi_bbc14d5c:


    "Tuve náuseas hasta mi garganta y mis ojos se oscurecieron."


translate spanish epilogue_mi_fb974fc0:


    "Masha se dio la vuelta tumbada, abrió sus ojos y me miró."


translate spanish epilogue_mi_5c118b9c:


    ma "¿Estás despierto?"


translate spanish epilogue_mi_aa1fbd9c:


    my "Tú... ¿escribiste esto?"


translate spanish epilogue_mi_ef225471:


    "Le pregunté espeso."


translate spanish epilogue_mi_41585d2c:


    ma "Sí, ¿qué pasa?"


translate spanish epilogue_mi_9ad49ae0:


    "No sabía cómo contestar.{w} Sencillamente no podía comprender qué estaba ocurriendo."


translate spanish epilogue_mi_efefdbbe:


    ma "Estabas tan lindo durmiendo que no quise despertarte. Y los saltamontes estaban estridulando tan fuerte... Así que yo..."


translate spanish epilogue_mi_d1046a49:


    "Ella sonrió."


translate spanish epilogue_mi_eb681808:


    my "Espera, ¡¿pero estamos aquí ahora?! Quiero decir, no somos pioneros, estamos rodando aquí y todo ese asunto, ¿no?"


translate spanish epilogue_mi_fed3ba60:


    ma "Sí, por supuesto."


translate spanish epilogue_mi_d6404691:


    "Masha me miró desconcertada."


translate spanish epilogue_mi_75cc25f6:


    "Tal vez desperté, leí lo que ella escribió y, luego, ¿me dormí otra vez...?"


translate spanish epilogue_mi_0e0ceee1:


    ma "¡Estás empapado en sudor!"


translate spanish epilogue_mi_4b123fd7:


    "Buscó a tientas en sus bolsillos del uniforme de pionera tirado a su lado y me dio un pañuelo.{w} Un típico pañuelo de mano ruso bordado con coloridas flores."


translate spanish epilogue_mi_a7d1b840:


    "Lo cogí y repentinamente sentí un arrebato de calor. El mismo calor que sientes cuando alguien querido te hace un regalo."


translate spanish epilogue_mi_f60fe5df_1:


    ma "¿Nos vamos?"


translate spanish epilogue_mi_99462d82:


    "Masha comenzó a vestirse."


translate spanish epilogue_mi_665516ab:


    my "Sí... vamos..."


translate spanish epilogue_mi_839949ee:


    "Apenas me puse en pie y fui hacia el río."


translate spanish epilogue_mi_fd5cfac0:


    "La fría agua me reanimó y aquel sueño dejó de parecerme tan real."


translate spanish epilogue_mi_2c25b971:


    "Después de todo, solamente era un sueño, ¿no?"


translate spanish epilogue_mi_9dd5fe3e:


    "En nuestro camino de vuelta, Masha sostenía mi mano y charlaba de todo sin parar."


translate spanish epilogue_mi_b3e9120f:


    "Llegamos a la plaza."


translate spanish epilogue_mi_0ef867d4:


    ma "Espero que esas criaturas me dejen dormir al menos esta noche..."


translate spanish epilogue_mi_e59f817f:


    "Dijo pensativamente."


translate spanish epilogue_mi_15c31469:


    my "¿Quiénes?"


translate spanish epilogue_mi_914e5307:


    ma "¡Los saltamontes!"


translate spanish epilogue_mi_fbeb45b9:


    my "Ah... sí, eso sería genial..."


translate spanish epilogue_mi_8425d0a6:


    ma "Bueno, me voy..."


translate spanish epilogue_mi_2d38f6b9:


    "Me miró intensamente, se dio la vuelta y estaba a punto de irse."


translate spanish epilogue_mi_77ececca:


    my "¡Espera!"


translate spanish epilogue_mi_15f28b4d:


    "Le agarré de la mano, la empujé hacia mí y la besé apasionadamente."


translate spanish epilogue_mi_3d50e8ce:


    "Porque a veces, un sueño solamente es un sueño..."


translate spanish epilogue_mi_d792d7ba:


    "Y la realidad es la realidad..."
# Decompiled by unrpyc: https://github.com/CensoredUsername/unrpyc
