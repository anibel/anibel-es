
translate spanish day5_main1_aa4d323e:


    "Corríamos...{w} Corríamos con todas nuestras últimas fuerzas..."


translate spanish day5_main1_11656716:


    "Como aquellos que corren por sus preciadas vidas."


translate spanish day5_main1_5edff0f6:


    "Como un condenado, sabiendo que no hay esperanza alguna para salvar su vida, luchando aun así contra lo inevitable y su propio destino... "


translate spanish day5_main1_5db27d0a:


    "Apenas pude cerrar la pesada puerta de metal."


translate spanish day5_main1_92ce41cd:


    th "No tengo ni idea de cuán profundo puede ser este refugio antiaéreo o si puede resistir una explosión nuclear, pero no hay otro lugar donde escondernos..."


translate spanish day5_main1_4c9141de:


    "Ella me agarró mi mano estrechándola fuertemente."


translate spanish day5_main1_6f240b82:


    me "No te preocupes..."


translate spanish day5_main1_81e0185f:


    "Pedazos del techo caían y las paredes temblaban."


translate spanish day5_main1_43320cff:


    "Estaba preparado para lo peor."


translate spanish day5_main1_eabbd6cf:


    "Pero la muerte es ese tipo de cosa para la cual no puedes estar preparado..."


translate spanish day5_main1_aceafbaa:


    "Pero de repente, un completo silencio se hizo. Zumbaba incluso más ruidosamente que las explosiones."


translate spanish day5_main1_e7df2349:


    dreamgirl "Tal vez es la hora de decirnos adiós..."


translate spanish day5_main1_1f90d83e:


    "Estaba lloriqueando."


translate spanish day5_main1_8ba5cb54:


    "Quería consolarla de alguna forma, pero me percaté que no había nada que pudiera hacer."


translate spanish day5_main1_9513cd87:


    me "Sí..."


translate spanish day5_main1_b0692838:


    dreamgirl "Sabes, yo..."


translate spanish day5_main1_337804c3:


    "Un horrible estallido casi me rompe los tímpanos."


translate spanish day5_main1_0ac01129:


    "Parece que estoy bajo un pedazo de techo derrumbado, pero no siento ningún dolor."


translate spanish day5_main1_02f7c13f:


    "Todo lo que quiero es no dejarla escapar..."


translate spanish day5_main1_893465aa:


    "Me desperté con un sudor frío, con falta de aire y jadeando."


translate spanish day5_main1_8d045bac:


    "Me llevó un rato recobrar mis sentidos."


translate spanish day5_main1_1d678e2c:


    me "Era un sueño...{w} Solamente era un sueño..."


translate spanish day5_main1_85574277:


    "Mi perpleja mente, sin embargo, rechazaba creer eso."


translate spanish day5_main1_cba74cb0:


    th "¿Pero quién era aquella muchacha que estaba allí conmigo?"


translate spanish day5_main1_92ca5a14:


    "No quería deshacerme de su mano tan desesperadamente..."


translate spanish day5_main1_d2a550ca:


    "Lamentablemente, no podía recordarla en absoluto."


translate spanish day5_main1_cfe2aec8:


    "El reloj mostraba las diez y pocos minutos."


translate spanish day5_main1_e1d34af1:


    "Lentamente recobré mis sentidos en tanto la realidad comenzaba a reclamar su lugar en mi cabeza... y mi estómago rugió vilmente."


translate spanish day5_main1_4e2d25a2:


    me "Está bien... la guerra es la guerra, ¡pero la comida debería ser servida en su debido momento!"


translate spanish day5_main1_f33a92de:


    "Resultó que Olga Dmitrievna no estaba aquí... debe haber decidido no despertarme."


translate spanish day5_main1_c0506319:


    th "Bueno, ¡las gracias van para nuestra líder del campamento por esto!"


translate spanish day5_main1_d29a68d9:


    "Tras las aventuras de ayer tenía que descansar."


translate spanish day5_main1_fc576f62:


    "La pasada noche se mantenía en un borroso recuerdo, el cual realmente no quería ni recordar."


translate spanish day5_main1_358f1320:


    "Es más importante buscar algo de comer ahora y...{w} ¡asearme!"


translate spanish day5_main1_16f93484:


    th "¡Exacto!"


translate spanish day5_main1_c02083da:


    th "Porque un pionero debe siempre estar limpio y pulcro."


translate spanish day5_main1_15333fca:


    "A pesar de que estaría de acuerdo con este principio incluso aunque no fuera un pionero (y, obviamente, no lo soy)."


translate spanish day5_main1_f50cd452:


    "En mi camino hacia los lavabos me encontré con Electronik."


translate spanish day5_main1_7e4abd77:


    "Empezó a saludar con su mano y corrió hacia mí."


translate spanish day5_main1_5237cfbf:


    el "¡Buenas mañanas!{w} ¡Gracias por hallar a Shurik! Sin él yo no..."


translate spanish day5_main1_b97e3fd5:


    me "No es nada..."


translate spanish day5_main1_dab051d6:


    "Estaba un poco incómodo."


translate spanish day5_main1_611aba0e:


    el "¡No, de verdad! No seas tan tímido... ¡la patria debe estar orgullosa de sus héroes!"


translate spanish day5_main1_04210531:


    me "Y qué hay de Shurik... ¿Cómo se encuentra esta mañana?{w} ¿Está bien?"


translate spanish day5_main1_c4c5697f:


    "Tras la locura de ayer, pensé que tal pregunta era completamente válida."


translate spanish day5_main1_805f0715:


    el "¡Sí, por completo!{w} La única cosa es que no puede recordar nada..."


translate spanish day5_main1_859bc359:


    me "¿No puede?"


translate spanish day5_main1_2850cdb5:


    "No estaba muy sorprendido."


translate spanish day5_main1_4819f710:


    el "Dijo que fue al campamento abandondo ayer, y luego... Se levantó en su cama por la mañana.{w} Quiero decir que no recuerda nada entre esos dos acontecimientos."


translate spanish day5_main1_5ec2624e:


    me "Ya veo...{w} Muy bien pues..."


translate spanish day5_main1_caccdbd7:


    el "Te perdiste el desayuno, ¿verdad? ¡Ven a nuestro club! ¡Te daremos de comer! Tengo algo especial."


translate spanish day5_main1_89ac8d3c:


    "Electronik sonrió de una forma conspiratoria."


translate spanish day5_main1_94afa350:


    me "Gracias, iré, probablemente..."


translate spanish day5_main1_7f29f4c2:


    "Tenía que asearme primero de todas maneras."


translate spanish day5_main1_c6f70531:


    el "¡Te estaremos esperando!"


translate spanish day5_main1_641f39b8:


    "Se despidió con la mano y se fue para ocuparse de sus asuntos."


translate spanish day5_main1_c64ccad8:


    "No había nadie cerca de los lavabos."


translate spanish day5_main1_127f580b:


    "Hoy el agua resultó estar sorprendentemente cálida."


translate spanish day5_main1_375200fb:


    th "Supongo que ya está suficientemente caliente..."


translate spanish day5_main1_b4c71998:


    "Habiendo aseado mi cara, me di cuenta de que no sería fácil asearse el resto de mi cuerpo ahí."


translate spanish day5_main1_e7b1f5de:


    th "Tal vez debiera ir a los baños..."


translate spanish day5_main1_540ebe5d:


    th "Pero como no hay nadie por aquí..."


translate spanish day5_main1_242cdd8f:


    "Giré el grifo de forma que el agua cayera de forma paralela al suelo y empecé a quitarme la ropa."


translate spanish day5_main1_8e855925:


    th "¿Y si alguien me ve?"


translate spanish day5_main1_6f67ff49:


    th "Bueno, me enjuagaría y secaría rápidamente, y me pondría la ropa..."


translate spanish day5_main1_315b7c24:


    "El agua, la cual me pareció cálida en mis manos y cara, la sentí en mi cuerpo fría hasta los huesos."


translate spanish day5_main1_a862e7c2:


    "Asearme en todo el rato me llevó no mucho más que diez segundos y empecé a pasarme un trapo rápidamente tras ello."


translate spanish day5_main1_203544b1:


    "Pero aun no había terminado de todas maneras... ¡que escuché voces venir en la dirección del camino!"


translate spanish day5_main1_8c7c1ae8:


    "La única solución que se me ocurrió en medio segundo... agarré mi ropa y salí corriendo hacia los arbustos."


translate spanish day5_main1_374a9333:


    "Un momento después Alisa y Ulyana aparecieron cerca de los lavabos."


translate spanish day5_main1_150b3ccc:


    dv "¡Podías haberlo hecho por ti misma! ¿Para qué me traes aquí?"


translate spanish day5_main1_88bcfdf2:


    us "¿Es demasiado problema para ti?"


translate spanish day5_main1_cc876a45:


    dv "Está bien, déjame..."


translate spanish day5_main1_657eb718:


    "Las miré detenidamente y me di cuenta de que ambas estaban cubiertas de pintura roja."


translate spanish day5_main1_2c5cbf0a:


    th "Qué sorpresa...{w} Me pregunto cómo se lo hicieron..."


translate spanish day5_main1_066f749f:


    "Alisa abrió el grifo y comenzó a frotar la espalda de Ulyana."


translate spanish day5_main1_fe4eae47:


    dv "¡Quítate el sujetador!"


translate spanish day5_main1_99ac44aa:


    us "¿Y si alguien nos ve...?"


translate spanish day5_main1_fc7e06ef:


    dv "Qué... ¿es que hay algo que ver?"


translate spanish day5_main1_473574eb:


    "Sonrió."


translate spanish day5_main1_6f871fea:


    us "Vale... ¡Pero sé rápida!"


translate spanish day5_main1_dd98f289:


    "Era cierto que no había mucho que ver, pero incluso así me quedé mirando fijamente las muchachas."


translate spanish day5_main1_dcd32ae7:


    "Era una pena que estuvieran dándome la espalda."


translate spanish day5_main1_89e896b4:


    "Un minuto más tarde Alisa logró limpiarle toda la pintura."


translate spanish day5_main1_55cf29e5:


    dv "¡Terminé!"


translate spanish day5_main1_38773291:


    us "¡Gracias!"


translate spanish day5_main1_d53c1a0e:


    dv "De nada..."


translate spanish day5_main1_924b8a95:


    "Alisa respondió perezosamente."


translate spanish day5_main1_c473bf91:


    us "Escucha, déjame que intente tu..."


translate spanish day5_main1_68b5468a:


    "Ella señaló al sujetador de Alisa."


translate spanish day5_main1_82a80785:


    dv "A ti eso seguro que no te va..."


translate spanish day5_main1_f348d116:


    us "Bueno, me gustaría probarlo de todas formas..."


translate spanish day5_main1_19b2ba93:


    dv "Pero aquí..."


translate spanish day5_main1_ba15cbf1:


    us "No hay nadie aquí, ¿verdad?"


translate spanish day5_main1_b8352a08:


    "Ulyana miró hacia mi lado y sonrió con astucia."


translate spanish day5_main1_eba695a3:


    "Estaba completamente seguro de que no podía verme aquí en estos arbustos, pero..."


translate spanish day5_main1_fe7431ac:


    dv "Basta ya con estas tonterías..."


translate spanish day5_main1_3bd56665:


    "Ulyana ya no estaba escuchándola más, y en vez de ello agarró el sujetador de Alisa con un diestro movimiento."


translate spanish day5_main1_cc3da604:


    "Ahora, debo confesar, ¡tenía algo que mirar!"


translate spanish day5_main1_7f273933:


    "Observé las dos muchachas persiguiéndose la una detrás de la otra en los lavabos sin soltar aliento."


translate spanish day5_main1_e09372ff:


    "Alisa cubrió sus pechos con sus manos, así que apenas podía ver nada."


translate spanish day5_main1_d0252e13:


    "Me incliné hacia delante y tropecé con una piedra, cayendo fuera de los arbustos..."


translate spanish day5_main1_2341b367:


    "Alisa y Ulyana se quedaron de piedra, mirándome."


translate spanish day5_main1_ef3d3ea6:


    "Traté de cubrir mi desnudo con mi cara de culpable."


translate spanish day5_main1_11a555c2:


    "El cuadro duró por unos segundos, luego Alisa agarró su camisa y de alguna forma se la puso al instante."


translate spanish day5_main1_b7d8bf72:


    dv "¡Tú! ¡Tú!"


translate spanish day5_main1_ec010716:


    "Su rostro se puso de rojo a morado.{w} Parecía como si fuera a estallar en una explosión nuclear en cualquier segundo."


translate spanish day5_main1_4d760247:


    "La única cosa que quería era desintegrarme en átomos e irme tan lejos del epicentro como fuera posible."


translate spanish day5_main1_97e02b14:


    us "¡Estaba sentado ahí todo el tiempo!"


translate spanish day5_main1_470f0c0e:


    th "En fin, ella se percató de mi presencia pues..."


translate spanish day5_main1_b7d8bf72_1:


    dv "¡Tú! ¡Tú!"


translate spanish day5_main1_3a1dcf49:


    me "Y yo...{w} Bueno yo... Accidentalmente...{w} Si sabes lo que quiero decir..."


translate spanish day5_main1_a9f31aea:


    "Alisa salió corriendo a por mi."


translate spanish day5_main1_4ffa3601:


    "Cubriéndome el trasero con una mano y sosteniendo mi ropa con la otra, corrí hacia el bosque."


translate spanish day5_main1_9ba510d2:


    "Me pareció la mejor solución en aquel momento... como si mostrándome desnudo en medio del campamento acompañado por dos muchachas chillándome no fuera una buena idea..."


translate spanish day5_main1_20ff3f6f:


    "Corrí sin mirar atrás."


translate spanish day5_main1_e7dc840b:


    "Y me detuve pocos minutos después para recobrar el aliento."


translate spanish day5_main1_0dab6283:


    "Pareció que se terminó la persecución."


translate spanish day5_main1_544faf65:


    th "¡Así que me las arreglé para salvarme!"


translate spanish day5_main1_49bd14a5:


    "Pero a costa de que mis pies se cortaron, se arañaron y sangraron... ya que no tuve tiempo de ponerme las botas."


translate spanish day5_main1_244a0a02:


    "Me senté en un tocón de madera y suspiré..."


translate spanish day5_main1_ada2f8dd:


    "Poco tiempo después, habiéndome vestido ya, abandoné el bosque."


translate spanish day5_main1_01337998:


    th "¡Tenía que decidir qué hacer a continuación!"


translate spanish day5_main1_0004b659:


    th "Mis pies me hacían daño... ¡por lo que debería ir directo a la enfermería!"


translate spanish day5_main1_21e25d14:


    th "Pero por otra parte, mi estómago tampoco me iba a esperar."


translate spanish day5_main1_bc7d5816:


    th "¿Quizá debería aceptar la invitación de Electronik?"


translate spanish day5_main1_ee8b56d6:


    th "¿O dirigirme a la cantina con la esperanza de hallar algo que sobrara allí...?"


translate spanish day5_dining_hall_c113591d:


    "Siempre procuro tener cuidado con mi salud.{w} Más aun, extremo las precauciones en los momentos en que no podría aguantar más."


translate spanish day5_dining_hall_5b254b74:


    "Y ahora podía caminar, mis pies no iban a dolerme mucho."


translate spanish day5_dining_hall_1068ff00:


    "Así que mis pies se podían curar con el tiempo, mientras que el hambre iba a esperar lo que un caramelo en la puerta de una escuela."


translate spanish day5_dining_hall_29600c1e:


    th "¡Seguramente los pioneros no terminaron con todo!"


translate spanish day5_dining_hall_16cc44b9:


    th "¡Al menos un par de salchichas, huevos o, en el peor de los casos, un par de rebanadas de pan deberían quedar!"


translate spanish day5_dining_hall_daf1231e:


    "La cantina estaba tan desierta y silenciosa por sus alrededores que incluso dudé por un segundo."


translate spanish day5_dining_hall_d065f713:


    "¿Acaso no es aquí donde cada pionero halla su felicidad tres veces al día (y algunos más veces), no es un oasis para este desértico verano caluroso, no es un laboratorio secreto químico para estudiar cómo los tipos de comida desconocida afectan a los cuerpos de los adolescentes no desarrollados?"


translate spanish day5_dining_hall_01ef2d8a:


    "Ahora este edificio se parecía más a una fortaleza abandonada por sus defensores, algo así como La Rochelle abandonada por los Hugonotes."


translate spanish day5_dining_hall_d00d888a:


    "Solamente entra aquí y los fantasmas de los guerreros que aceptaron la muerte heroica te rodearan..."


translate spanish day5_dining_hall_ff4b2359:


    "La cantina se veía igual que siempre aun así."


translate spanish day5_dining_hall_59713482:


    "Simplemente estaba vacía por completo, salvo por...{w} Miku, la cual estaba limpiando una mesa."


translate spanish day5_dining_hall_c404fc22:


    "Viendo aquello me di la vuelta rápidamente y traté de escapar furtivamente, pero no lo logré..."


translate spanish day5_dining_hall_44d5aaf7:


    mi "¡Hola, Semyon! ¿Viniste aquí a comer? Te perdiste el desayuno, ¿verdad? Quiero decir que no te vi... Podrías haber estado aquí pero no te vi. ¡De todas formas es bueno que vinieras!"


translate spanish day5_dining_hall_479c6b10:


    me "Ehmm, hola...{w} Bueno yo... Sí, solamente vine... Me preguntaba si quedaría algo para mi tal vez..."


translate spanish day5_dining_hall_a44fc8b3:


    mi "¡No quedó nada! ¡Tendrás que esperar a la hora de la comida! Por cierto, ¿me ayudarías? Estoy limpiando aquí..."


translate spanish day5_dining_hall_810f39c8:


    me "¿Para qué?"


translate spanish day5_dining_hall_780b4277:


    mi "¿Qué quieres decir?"


translate spanish day5_dining_hall_d946b833:


    "Hizo un mohín con los labios como si se hubiera ofendido."


translate spanish day5_dining_hall_d0d699dd:


    mi "¡Alguien tiene que limpiar! Lo hacemos por turnos. ¡Tendrás tu turno también!"


translate spanish day5_dining_hall_c0778825:


    th "Gracias, pero no..."


translate spanish day5_dining_hall_6933143e:


    me "Vale, lo capto..."


translate spanish day5_dining_hall_9b0aa1ca:


    "Estaba a punto de irme, pero Miku aun no podía parar."


translate spanish day5_dining_hall_c78b2cc2:


    mi "Y bien, ¿me ayudarás?"


translate spanish day5_dining_hall_a4480274:


    "No sé el porqué estuve de acuerdo."


translate spanish day5_dining_hall_79c948da:


    "A veces ocurre que tomas una decisión primero y, luego, te preguntas durante un rato el porqué dijiste lo que dijiste."


translate spanish day5_dining_hall_2f281a85:


    "Piensas una vez y otra...{w} Y todavía sigues sin llegar a comprender por qué diantres hiciste eso."


translate spanish day5_dining_hall_d93feb70:


    "Así es como me sentí limpiando las mesas una detrás de la otra."


translate spanish day5_dining_hall_68f21c4b:


    mi "Sabes, ¡hice una nueva canción! ¿Quieres que te la cante?"


translate spanish day5_dining_hall_209b83b3:


    th "No tenía muchos deseos."


translate spanish day5_dining_hall_0270e5da:


    mi "Mmm, no...."


translate spanish day5_dining_hall_60f3d758:


    "Ella empezó a reflexionar."


translate spanish day5_dining_hall_85b5c92a:


    mi "Podría ser difícil cantar y limpiar al mismo tiempo... ¡La cantaré luego pues!"


translate spanish day5_dining_hall_3aeb2d0f:


    "Miku me hizo una encantadora sonrisa."


translate spanish day5_dining_hall_3b03af3a:


    me "Sí, claro..."


translate spanish day5_dining_hall_6eaf9733:


    mi "¡Fue genial cómo salvaste a Shurik ayer! ¡Todo el campamento ha estado hablando sólo de esto desde bien temprano por la mañana!"


translate spanish day5_dining_hall_992a60d0:


    th "Me sentí justo como un héroe..."


translate spanish day5_dining_hall_668b75c5:


    me "No fue gran cosa la verdad."


translate spanish day5_dining_hall_57cc9875:


    mi "¡No, de verdad, lo digo en serio! Yo no me habría atrevido en adentrarme en los bosques por la noche... en el antiguo campamento... ¿Conoces los rumores sobre éste? Sobre una líder de campamento que se pegó un tiro a sí misma..."


translate spanish day5_dining_hall_3bb17a2c:


    th "Antes de decirlo se detuvo."


translate spanish day5_dining_hall_52e843c6:


    mi "¡Y da mucho miedo todo!"


translate spanish day5_dining_hall_b22bc996:


    me "Sep, probablemente."


translate spanish day5_dining_hall_f15c1e3c:


    "Traté de aislarme a mí mismo de cualquier distracción externa y me concentré en limpiar."


translate spanish day5_dining_hall_235ecfcf:


    "Me ayudó a terminar más pronto de lo esperado."


translate spanish day5_dining_hall_7a81d8e3:


    me "¡Ya está hecho!"


translate spanish day5_dining_hall_07ea95a8:


    mi "¡Gracias!"


translate spanish day5_dining_hall_25fab848:


    "Todavía quedaba algo de tiempo hasta la hora de comer, así que decidí ir a dar un paseo."


translate spanish day5_dining_hall_90a17a70:


    mi "Vale, muy bien..."


translate spanish day5_dining_hall_b2673fad:


    "Es como si hubiera enfadado a Miku."


translate spanish day5_dining_hall_f3c8b2f3:


    th "Pero qué le voy hacer... así es la vida..."


translate spanish day5_dining_hall_9d2e50ce:


    "Abandoné la cantina, me senté en el banco al lado de la puerta y suspiré cansado."


translate spanish day5_dining_hall_7e6cef9d:


    "Mis pies todavía me dolían un poco aunque no tanto como antes, y todavía tenía el estómago vacío..."


translate spanish day5_dining_hall_29ebce26:


    "Todavía quedaba un rato hasta la hora de comer, así que decidí ir a dar un paseo."


translate spanish day5_dining_hall_549330fd:


    "Escogí una dirección aleatoria, la cual bien podría explicarse con la sola palabra de «adelante»."


translate spanish day5_dining_hall_8983ed56:


    "Al final, me hallé en la plaza."


translate spanish day5_dining_hall_283be7ae:


    "Esto no era ninguna sorpresa ya que el monumento de Genda parecía ser el punto de encuentro de este campamento y una especie de kilómetro cero..."


translate spanish day5_dining_hall_356059a5:


    "Me senté en el banco y comencé a pensar."


translate spanish day5_dining_hall_85bc341b:


    th "Cuatro días han pasado y no he conseguido acercarme ni un centímetro en saber cómo llegué aquí."


translate spanish day5_dining_hall_37f3c603:


    th "Es cierto que unas cuantas cosas extrañas han sucedido durante este tiempo, pero casi cada una de ellas puede ser explicada lógicamente tras reflexionar con calma."


translate spanish day5_dining_hall_9cc8af14:


    th "Cada una de ellas podría haber ocurrido en la vida normal..."


translate spanish day5_dining_hall_ee9a7a1a:


    th "La vida {i}normal{/i}.{w} Ese concepto perdió su significado original estando yo aquí."


translate spanish day5_dining_hall_e5526803:


    th "Las reacciones del ambiente, las acciones y palabras de otras personas, o mis propias palabras... ciertamente, ninguna de estas aquí es normal."


translate spanish day5_dining_hall_66dcd718:


    th "En los últimos cuatro días mi visión del mundo ha recibido una serie de puñetazos y ganchos dolorosos en el estómago que me han llevado a estar, si no noqueado, por lo menos seriamente arrollado."


translate spanish day5_dining_hall_7b0e3b31:


    th "A veces no comprendo por qué actúo de una forma u otra o digo algunas cosas..."


translate spanish day5_dining_hall_19f2dbbc:


    th "En realidad, comprendo, pero no de inmediato."


translate spanish day5_dining_hall_ac9be890:


    th "Tales reparos sin embargo, no me ayudan a actuar de forma diferente, más cuerdamente y apropiadamente a la situación."


translate spanish day5_dining_hall_bc9e662f:


    th "Los instantes de certeza que me suceden se vuelven cada vez más y más inusuales."


translate spanish day5_dining_hall_85fdbea0:


    th "Si mi único deseo durante el primer día era escapar de aquí, pues ahora mis principales preocupaciones son encontrar comida, cómo evitar incorporarme a las formaciones por las mañanas y qué decirle a Olga Dmitrievna si Alisa se queja de mi..."


translate spanish day5_dining_hall_f2b21e4a:


    th "¡Esas cosas son verdaderamente importantes para mi!"


translate spanish day5_dining_hall_770314f2:


    th "Y días tras día, las preocupaciones diarias como éstas eclipsan mis pensamientos sobre cómo el mundo a mi alrededor, junto a este campamento y esas muchachas, ¡son en el fondo completamente anormales!"


translate spanish day5_dining_hall_4ba0ec94:


    th "Pero no puedo hacer nada por mí.{w} Porque sencillamente olvidé..."


translate spanish day5_dining_hall_5c46142a:


    th "De la misma manera que respiramos sin pensar en ello, me uno a la vida rutinaria de los lugareños cada día más y más, sin darme cuenta de ello."


translate spanish day5_dining_hall_95d43b05:


    th "Cada vez me convierto más en un pionero promedio..."


translate spanish day5_dining_hall_6614ca63:


    me "¡No!{w} ¡Está mal!"


translate spanish day5_dining_hall_216fd396:


    "Grité fuerte y me abofeteé la cara unas cuantas veces."


translate spanish day5_dining_hall_8d33c464:


    "Inesperadamente la campana sonó, avisando a los pioneros de la hora de comer desde los altavoces."


translate spanish day5_dining_hall_f23afdfa:


    me "¡Al fin!"


translate spanish day5_dining_hall_436b6b08:


    "Me fui corriendo hasta la cantina, dejando solos atrás en la plaza mis inspirados pensamientos, los cuales podrían serles interesantes a Genda, y sólo si estuviera vivo..."


translate spanish day5_aidpost_c1caf0d4:


    "Siempre procuro tener cuidado con mi salud.{w} Más aun, extremo las precauciones en los momentos en que no podría aguantar más."


translate spanish day5_aidpost_b92e3272:


    "Así que sin dudar, me fui hacia la enfermería."


translate spanish day5_aidpost_6e2d6c49:


    "Pero justo frente a la puerta me detuve, como si una misteriosa fuerza me retuviera."


translate spanish day5_aidpost_f49a6790:


    "No tenía ningún deseo de vérmelas con la enfermera del campamento, a menos que fuera una urgencia, pero por otra parte, casi hice una gran hazaña ayer al responsabilizarme de la enfermería."


translate spanish day5_aidpost_9cb3955d:


    "Por lo que en cierta forma, ella estaba en deuda conmigo..."


translate spanish day5_aidpost_41de82b8:


    "Hice tripas corazón y abrí la puerta."


translate spanish day5_aidpost_8e557000:


    cs "¡Hola, pionero!"


translate spanish day5_aidpost_bb4c809d:


    me "Hola..."


translate spanish day5_aidpost_0f095ac2:


    cs "Siento mucho llegar tarde ayer..."


translate spanish day5_aidpost_fa9efa41:


    me "Tampoco fue un problema..."


translate spanish day5_aidpost_e2736162:


    cs "¿Por qué viniste?{w} ¿Estás enfermo?"


translate spanish day5_aidpost_945c59bc:


    "Sonrió astutamente."


translate spanish day5_aidpost_f6d4c30b:


    me "Bueno, yo...{w} Tengo mis pies un poco..."


translate spanish day5_aidpost_a10407c0:


    cs "¿Un poco qué?"


translate spanish day5_aidpost_3f4d5019:


    me "Mis pies."


translate spanish day5_aidpost_be55d6e6:


    "Di una respuesta estúpida."


translate spanish day5_aidpost_ee52ce7f:


    cs "Vale, muéstralos."


translate spanish day5_aidpost_ebe6fcf2:


    "Me senté en el sofá y me quité las botas."


translate spanish day5_aidpost_387cc55b:


    cs "¿Cómo te lo hiciste?"


translate spanish day5_aidpost_97c64dc4:


    "Mejor me callo lo que ocurrió en los lavabos."


translate spanish day5_aidpost_2fcc5991:


    me "Bueno yo..."


translate spanish day5_aidpost_db1556e6:


    cs "Vale, olvídalo.{w} Espera un segundo..."


translate spanish day5_aidpost_37b10e56:


    "Rebuscó entre el cajón y sacó un frasco del cual extrajo una gran pastilla roja con una muesca de cruz grabada."


translate spanish day5_aidpost_7603637e:


    me "¿Y eso para qué es?"


translate spanish day5_aidpost_8f1a1f4c:


    "Estaba trastornado por el tamaño de la pastilla y su extraña forma... se rompen por la mitad normalmente y no a cuartos."


translate spanish day5_aidpost_8040e280:


    cs "¡Esto es para no tener que amputarte tus piernas!"


translate spanish day5_aidpost_3d342d5a:


    me "¡¿Qué?!"


translate spanish day5_aidpost_91d22182:


    cs "Nada. No te preocupes...{w} ¡pionero!{w} Te la tomarás y todo irá bien."


translate spanish day5_aidpost_9fe20b07:


    me "Y esta pastilla...{w} ¿Por qué tiene este aspecto?"


translate spanish day5_aidpost_7e2d8a95:


    cs "Esto es para los pacientes que se niegan a tomar la medicina por vía oral.{w} Se la atornillamos pues con un destornillador."


translate spanish day5_aidpost_7ccd5058:


    "No me atreví a preguntar dónde se les atornillaba para ser exactos."


translate spanish day5_aidpost_c2372b85:


    cs "Y ahora trata de aguantar un poco."


translate spanish day5_aidpost_5c6ac9cb:


    "Cogió un paquete de algodón, rompió un pedazo y le aplicó yodo en él."


translate spanish day5_aidpost_ad181873:


    "Estaba preparado para el sufrimiento del infierno..."


translate spanish day5_aidpost_4b4491c1:


    me "Ayyyyyy..."


translate spanish day5_aidpost_1e0476d1:


    "Gemí en silencio."


translate spanish day5_aidpost_ad1b9cb0:


    "Pero mi fe en este método para desinfectar era mucho mejor que en el de las pastillas raras, así que tenía soportar la quemazón."


translate spanish day5_aidpost_4e9c1b77:


    cs "¡Ya está!"


translate spanish day5_aidpost_4eb5e7c1:


    "Me puse las botas y traté de caminar."


translate spanish day5_aidpost_920fc608:


    "Mis pies todavía me dolían, pero el dolor agudo se fue."


translate spanish day5_aidpost_61477e60:


    me "¡Gracias!"


translate spanish day5_aidpost_f328eb53:


    cs "¡No hay de qué!{w} Déjate caer en cualquier momento...{w} ¡pionero!"


translate spanish day5_aidpost_40d35bf5:


    "Le dije adiós y me fui de la enfermería."


translate spanish day5_aidpost_c4d64e48:


    "¡Mi problema de salud fue resuelto!"


translate spanish day5_aidpost_132ab340:


    th "¡Es justo la hora de comer!"


translate spanish day5_clubs_6f918f97:


    "Estaba deseperadamente hambriento."


translate spanish day5_clubs_c113591d:


    "Siempre procuro tener cuidado con mi salud.{w} Más aun, extremo las precauciones en los momentos en que algo duele mucho."


translate spanish day5_clubs_f98258e8:


    "Y ahora podía caminar y mis pies ya no me dolían tanto."


translate spanish day5_clubs_ff3100c2:


    th "Así que mis pies se podían curar de un momento a otro, mientras que mi hambre no podría esperar..."


translate spanish day5_clubs_52edf477:


    "Escogí tomar ventaja de la invitación de Electronik."


translate spanish day5_clubs_13624c4d:


    "En fin, conociendo a los pioneros locales, era inteligente asumir que no quedaría ni un pedazo rancio de pan en la cantina..."


translate spanish day5_clubs_2035d522:


    th "Y el club de cibernética de hecho estaba en deuda conmigo."


translate spanish day5_clubs_c6f1a614:


    "Con tales pensamientos me aproximé al edificio de clubs."


translate spanish day5_clubs_e8ed57a1:


    "Y escuché chillidos que provenían de su interior."


translate spanish day5_clubs_d8f147f1:


    "Traté de escucharlos de más cerca, pero no me podía imaginar nada."


translate spanish day5_clubs_5b5a4f32:


    sh "¡Devuélvelo!"


translate spanish day5_clubs_6c5410b6:


    us "No, ¡no quiero!"


translate spanish day5_clubs_d25dd8ba:


    "Ulyana estaba correteando por la habitación con una bobina de cable en sus manos, perseguida por Shurik."


translate spanish day5_clubs_12e6be7b:


    me "Muchachos, siento interrumpiros pero, pero..."


translate spanish day5_clubs_5b5a4f32_1:


    sh "¡Devuélvelo!"


translate spanish day5_clubs_1217af9c:


    "No me prestaron ni un ápice de atención."


translate spanish day5_clubs_6c5410b6_1:


    us "¡No quiero!"


translate spanish day5_clubs_87a4498e:


    "Shurik, completamente distraído por la persecución me pasó rozando, y casi me tumba."


translate spanish day5_clubs_95efcfe1:


    me "¡Ey!"


translate spanish day5_clubs_37ace03e:


    "Ulyana estaba correteando en círculos alrededor de la habitación riéndose alegremente."


translate spanish day5_clubs_ec25da0f:


    th "Me pregunto para qué necesitará ese cable..."


translate spanish day5_clubs_81589db2:


    "Mientras tanto el jefe del club de cibernética se le veía bien, incluso tras la locura de ayer.{w} Uno podría incluso decir que se le veía despejado."


translate spanish day5_clubs_2ddd0bed:


    "Pero esto probablemente no le ayudaría en atrapar a Ulyana... ella era más pequeña, más ágil y más rápida al fin y al cabo."


translate spanish day5_clubs_075ee6b7:


    "Podía hacer que él se cansara fácilmente..."


translate spanish day5_clubs_95efcfe1_1:


    me "¡Ey!"


translate spanish day5_clubs_0b5f4462:


    "Ulyana corrió hasta mí y se escondió detrás de mí."


translate spanish day5_clubs_692dfc67:


    "Shurik en su lugar abandonó el club, pegando un portazo al salir."


translate spanish day5_clubs_8c929878:


    th "Parece que estaba enfadado..."


translate spanish day5_clubs_25606905:


    me "¡Ey! ¡¿Qué pasa contigo?!"


translate spanish day5_clubs_b73a1dab:


    us "¡No me atraparás!"


translate spanish day5_clubs_575bed8e:


    "Ella miraba a Electronik, quien estaba observando en silencio toda esta tonta carrera, y le sacó su lengua."


translate spanish day5_clubs_67bf1dec:


    el "¡Semyon, quítale el cable!"


translate spanish day5_clubs_3cfac947:


    me "¿Y en primer lugar para qué lo necesitas? ¿Es que no tienes suficiente cable o algo así?"


translate spanish day5_clubs_65fffe51:


    "Él trató de atrapar a Ulyana, quien estaba escondida detrás de mí."


translate spanish day5_clubs_3a233083:


    "Cuando él se movía a la derecha, ella se movía a la izquierda, luego él se movía a izquierda y ella a la derecha..."


translate spanish day5_clubs_db785e8c:


    "Al final me cansé de todo esto y agarré el cable de Ulyana con un diestro movimiento."


translate spanish day5_clubs_7a0f81d3:


    us "¡Dámelo! ¡Devuélmelo!"


translate spanish day5_clubs_25ebf834:


    "Gritó con resentimiento."


translate spanish day5_clubs_bb517af5:


    me "¡No lo haré! ¡Para ya con este alboroto!"


translate spanish day5_clubs_aac11710:


    "Sostuve el cable por encima de mi cabeza, por lo que Ulyana con su altura no podría alcanzarlo."


translate spanish day5_clubs_d2db29cb:


    el "¡Gracias!"


translate spanish day5_clubs_44f47ba7:


    us "¡Vale, está bien!"


translate spanish day5_clubs_99aa653e:


    "Resopló y se apartó de mí."


translate spanish day5_clubs_66d3549f:


    me "En primer lugar, ¿por qué lo necesitas?"


translate spanish day5_clubs_7d1c8c94:


    us "Eso no es asunto tuyo..."


translate spanish day5_clubs_c68d93e1:


    "Ulyana me miró con astucia."


translate spanish day5_clubs_88702a1e:


    us "Quieres que le diga a todo el mundo que tú..."


translate spanish day5_clubs_960701df:


    "Le tapé la boca con mi mano y me la llevé arrastrándola hacia fuera."


translate spanish day5_clubs_d0245af1:


    me "Vale, tenemos que irnos..."


translate spanish day5_clubs_c000d8e7:


    "Le dije a Electronik mientras soltaba un risita estúpida."


translate spanish day5_clubs_4d17bd21:


    el "¿Y para qué viniste...?"


translate spanish day5_clubs_7525ab2b:


    "Una vez fuera, solté a Ulyana que pataleaba."


translate spanish day5_clubs_1828214b:


    me "Escúchame bien, sabes que aquello fue un accidente...{w} Y aun más, ¡fue causado por tu culpa!"


translate spanish day5_clubs_86dba4fb:


    us "¡Yo no sé nada!{w} Los hechos son hechos: ¡nos estabas espiando!"


translate spanish day5_clubs_d226b66c:


    us "¡Ay, ay, ay!{w} Qué pensaría Olga Dmitrievna..."


translate spanish day5_clubs_df680674:


    "Por una parte, no me quería preocupar ni lo más mínimo por lo que la líder del campamento diría, pero por el otro lado, todo estaba en contra mío y teniendo en cuenta mi posición, sería mejor no ponerme en dicha situación."


translate spanish day5_clubs_97ad7b66:


    me "Bien, tal vez podamos hacer algún trato..."


translate spanish day5_clubs_6532261a:


    us "Mmmmm..."


translate spanish day5_clubs_60f3d758:


    "Empezó a reflexionar."


translate spanish day5_clubs_9c1e8275:


    us "¡Ya sé!"


translate spanish day5_clubs_747f9ddc:


    "Me esperaba lo peor."


translate spanish day5_clubs_e97a33bb:


    us "¡Tú me traes el cable!"


translate spanish day5_clubs_47728b3d:


    me "¿Pero para qué lo necesitas?"


translate spanish day5_clubs_fc2357ec:


    us "¡Lo necesito para mis necesidades!"


translate spanish day5_clubs_2fca2e42:


    me "Vale, asumamos que lo consigo...{w} ¿No se lo dirás a nadie?"


translate spanish day5_clubs_6f158bac:


    us "¡Te doy mi palabra de pionera!"


translate spanish day5_clubs_85b6f436:


    "Difícilmente podría creerla."


translate spanish day5_clubs_88068e4d:


    "Pero por otra parte, tan sólo era una bobina de cable... ¿por qué no debería intentarlo?"


translate spanish day5_clubs_91dd4d67:


    me "Vale, está bien..."


translate spanish day5_clubs_a64a24e7:


    us "¡Trato hecho!"


translate spanish day5_clubs_fee7bf01:


    me "Pero debes saber que..."


translate spanish day5_clubs_f85583fb:


    us "¡Claro, claro, de acuerdo!"


translate spanish day5_clubs_0d425fdb:


    "Me interrumpió."


translate spanish day5_clubs_b2bf8ea2:


    me "Espera aquí."


translate spanish day5_clubs_a4e889e3:


    "Dicho esto, me fui dentro del edificio de clubs."


translate spanish day5_clubs_0af71d51:


    me "No, no hay cable para ti, ¡ya es suficiente!"


translate spanish day5_clubs_1d6fcf81:


    us "¡Pues entonces se lo contaré a todos!"


translate spanish day5_clubs_5da9850d:


    me "¡Lo contarás de todas formas! O Alisa lo hará..."


translate spanish day5_clubs_80eb9adc:


    us "Ahh, ¡piérdete!"


translate spanish day5_clubs_6d0fed3e:


    me "Claro, así es, como si fuera mi culpa..."


translate spanish day5_clubs_a18bb399:


    us "Lo es. Nos estabas espiando."


translate spanish day5_clubs_69a83100:


    me "¡No lo estaba!"


translate spanish day5_clubs_2719b638:


    "Pero da igual cuanto lo disimulara, en realidad lo estaba..."


translate spanish day5_clubs_76442aba:


    us "Alisa no cree lo mismo."


translate spanish day5_clubs_bdc70299:


    me "Ella no se pone de acuerdo conmigo en montones de cosas."


translate spanish day5_clubs_d927617d:


    us "Y Olga Dmitrievna tampoco estará muy contenta."


translate spanish day5_clubs_a36b990a:


    me "¡¿Sabes qué?!"


translate spanish day5_clubs_6ea78a01:


    "Comenzaba a perder mi paciencia."


translate spanish day5_clubs_07f22fdf:


    me "Si quieres contarlo todo... ¡adelante ve ahora! ¡Y no olvides mencionar que el default del gobierno, la depresión mundial, el calentamiento global y que la Plaga del Génesis son todos culpa mía!"


translate spanish day5_clubs_7843e56a:


    us "Oh, venga ya, no hace falta que te pongas así... Solamente bromeaba."


translate spanish day5_clubs_ba72b31c:


    me "¿Bromeabas...?"


translate spanish day5_clubs_578ce4ba:


    "De repente me di cuenta de que realmente estaba demasiado furioso."


translate spanish day5_clubs_07630dae:


    me "¡Tus bromas son estúpidas! Y qué se supone que debo hacer en cada momento... ¿suponer si van en serio o son sólo bromas?"


translate spanish day5_clubs_df6dbf74:


    us "Sep."


translate spanish day5_clubs_fde537c1:


    "Sonrió."


translate spanish day5_clubs_0f1412bc:


    us "Es más divertido así."


translate spanish day5_clubs_eac89d90:


    "Ulyana se dio la vuelta y se fue corriendo a la plaza, despidiéndose con la mano al partir."


translate spanish day5_clubs_beff659f:


    th "Aun así, nunca sabes cómo tratar con ella de una forma cordial..."


translate spanish day5_clubs_01a2b4fe:


    "Suspiré y volví dentro del edificio de clubs."


translate spanish day5_clubs_ee53a93e:


    "Electronik estaba construyendo algo con la atención cautivada."


translate spanish day5_clubs_6768144c:


    me "Ya he terminado con todo...{w} Así que pensé que me podrías dar algo como prometiste..."


translate spanish day5_clubs_72b8e1d5:


    "Mi voz sonó un poquitín aduladora, lo cual fue suficiente para molestarme."


translate spanish day5_clubs_cda5bf38:


    me "Claro que tampoco voy a insistir pero..."


translate spanish day5_clubs_73232f80:


    el "Un minuto."


translate spanish day5_clubs_819d22c9:


    "Se apartó de su trabajo y cogió un par de bollos del cajón, y también un clásico paquete triangular de kéfir."


translate spanish day5_clubs_10c6e757:


    el "Como en tu casa."


translate spanish day5_clubs_73aab3cd:


    me "Gracias..."


translate spanish day5_clubs_c321210e:


    "Mientras estaba entretenido comiendo, Electronik no se apartó de su dispositivo ni por un segundo."


translate spanish day5_clubs_88d66003:


    "Estaba enrollando el cable, el cual le había quitado a Ulyana, en una bobina."


translate spanish day5_clubs_c90aa8d9:


    th "¡Así que era el cable lo que ella quería!"


translate spanish day5_clubs_b12c3bd0:


    me "¿Y eso qué es?"


translate spanish day5_clubs_ce6e33a1:


    el "Un inductor."


translate spanish day5_clubs_01d8c6fc:


    me "¿Un hindú thor?"


translate spanish day5_clubs_a1512ad1:


    el "¡Únete a nuestro club y lo conocerás todo!"


translate spanish day5_clubs_99c684ef:


    "Me miró y sonrió astutamente."


translate spanish day5_clubs_232d02c3:


    me "Pensaré en ello..."


translate spanish day5_clubs_8e38bd64:


    "Por supuesto, no iba a unirme a nada, pero teniendo en cuenta que me dio de comer, tenía que ser amable."


translate spanish day5_clubs_0560d317:


    el "Por cierto, como dije, tengo algo más..."


translate spanish day5_clubs_f39fd8a3:


    me "Bueno, claro..."


translate spanish day5_clubs_73232f80_1:


    el "Espera un segundo."


translate spanish day5_clubs_ebaab1b0:


    "Se fue a la habitación adyacente y volvió tras un minuto con algún tipo de paquete."


translate spanish day5_clubs_1dfcdcd0:


    el "¡Ta-ta-chaaán!"


translate spanish day5_clubs_6ba4e579:


    "Y me lo dio."


translate spanish day5_clubs_4b293ede:


    "Había una gran botella de vodka «Stolichnaya» en su interior."


translate spanish day5_clubs_4e5a25f1:


    me "Ehmm, lo capto, pero aun estamos por la mañana..."


translate spanish day5_clubs_7cc7ef1a:


    th "¿O es que Electronik comparte el dicho de «emborráchate por la mañana... y tómate el día entero libre»?"


translate spanish day5_clubs_9479eb17:


    el "¡¿De qué estás hablando?!{w} ¡No sugiero que nos lo bebamos! ¡Lo tenemos para limpiar las ópticas!"


translate spanish day5_clubs_5e972698:


    th "Limpiar las ópticas, claro seguro...{w} Internamente..."


translate spanish day5_clubs_f2e007f0:


    me "Vale..."


translate spanish day5_clubs_9991da5b:


    "Le devolví el paquete."


translate spanish day5_clubs_a5f87a00:


    me "¡Por cierto! ¿Me dejarías esta bobina?"


translate spanish day5_clubs_ce0d2801:


    el "¿Para qué la necesitas?"


translate spanish day5_clubs_c97fadc0:


    me "Uhmmm…"


translate spanish day5_clubs_d6323f95:


    "Probablemente tuve que pensar un motivo de antemano."


translate spanish day5_clubs_98fe77ae:


    me "Sólo la necesito."


translate spanish day5_clubs_09b8008a:


    el "Pero nosotros también la necesitamos."


translate spanish day5_clubs_68f1c3d2:


    me "¿En serio? Bueno, olvídalo pues..."


translate spanish day5_clubs_ba73ed1f:


    th "No puedo obligarle a que me lo dé, ¿verdad?"


translate spanish day5_clubs_cd9f16d5:


    me "Pues me voy..."


translate spanish day5_clubs_642cd44e:


    el "¡Vuelve cuando quieras!"


translate spanish day5_clubs_bdd3ba78:


    me "Claro..."


translate spanish day5_clubs_c5676147:


    th "Cuando quiera beber..."


translate spanish day5_clubs_212a81b3:


    "Al salir caminando, me pregunté en primer lugar por qué querría él enseñarme el vodka..."


translate spanish day5_clubs_3c6ad981:


    "Ulyana salió corriendo hacia mí al instante."


translate spanish day5_clubs_1fb33640:


    us "¿Y? ¿Qué tal? ¿Lo conseguiste?"


translate spanish day5_clubs_1eaad051:


    me "No..."


translate spanish day5_clubs_07a5780c:


    us "¡Pues es tu culpa entonces!"


translate spanish day5_clubs_03515da3:


    me "¿De verdad que necesitas ese cable...?"


translate spanish day5_clubs_b934057e:


    us "¡Pero al menos tienes mérito por intentarlo!"


translate spanish day5_clubs_ad02f53a:


    me "¿Qué?"


translate spanish day5_clubs_d8362ad8:


    "Estaba sorprendido."


translate spanish day5_clubs_a10e2b46:


    us "¡Alisa lo contará todo de todas formas!"


translate spanish day5_clubs_a4c6b91e:


    "Ulyana se rio."


translate spanish day5_clubs_d1d0d845:


    us "Realmente está muy enfadada contigo."


translate spanish day5_clubs_078810d4:


    me "Apuesto a que sí..."


translate spanish day5_clubs_3e1d421a:


    "Murmuré en voz baja."


translate spanish day5_clubs_fa77eb7d:


    us "Vale, ¡nos vemos!"


translate spanish day5_clubs_3861dc96:


    "Se me despidió con la mano y se fue corriendo."


translate spanish day5_clubs_50e6fe96:


    th "Y aquí estoy, en una estúpida situación una vez más, eso es todo."


translate spanish day5_clubs_5bd8bf7d:


    th "No es la primera vez..."


translate spanish day5_clubs_bedd20be:


    "Repentinamente mi hambre retornó de nuevo..."


translate spanish day5_clubs_9a611cea:


    "Seguramente una comida con unos bollos y un kéfir era estupendo. Pero no era suficiente para llenarme."


translate spanish day5_clubs_8b9210c2:


    "Afortunadamente para mí escuché el sonido de la campana, avisando a los pioneros para comer."


translate spanish day5_main2_c9f98d7e:


    th "El día justo había comenzado y ya me habían sucedido varios percances..."


translate spanish day5_main2_9dff93e1:


    th "¡Pero los salvé y ahora me merecía justificadamente poderme hartar!"


translate spanish day5_main2_b855c58c:


    "Hoy no fui el último en llegar, así que podía escoger una mesa libre."


translate spanish day5_main2_5a8c068e:


    "La comida incluía una sopa de arvejas y puré de patatas con pescado."


translate spanish day5_main2_e5fed9f2:


    "Era una gran decepción para mí, en tanto que no comía pescado de ningún tipo, por tanto iba a tener menos calorías de lo habitual."


translate spanish day5_main2_9a2a6cb4:


    "Pronto Slavya y Lena se acercaron a mi mesa."


translate spanish day5_main2_99c96681:


    sl "¿Podemos?"


translate spanish day5_main2_b7bd4e35:


    "Sonrió amablemente."


translate spanish day5_main2_bcdef80b:


    me "¿Eh?{w} ¡Sí, claro!"


translate spanish day5_main2_566b45f6:


    "Me incorporé y saqué una silla para ella."


translate spanish day5_main2_7b602452:


    me "¡Por favor!"


translate spanish day5_main2_cafd2e9e:


    "Estaba de muy buen humor en aquel momento."


translate spanish day5_main2_538f35f4:


    un "Que aproveche..."


translate spanish day5_main2_62d27eaf:


    "Dicho esto, Lena comenzó a mirarme y continuó mirándome durante un rato pero luego, al darse cuenta de lo rara que se veía, la apartó hacia su plato."


translate spanish day5_main2_3c5ee540:


    me "Tú también."


translate spanish day5_main2_952025ed:


    sl "¿Tienes algún plan para hoy, Semyon?"


translate spanish day5_main2_5a7a7577:


    me "Nop."


translate spanish day5_main2_c0d912bb:


    "Le di una respuesta honesta en tanto que de hecho no tenía planes.{w} Excepto por {i}seguir buscando respuestas{/i}, pero eso era más bien como un objetivo general."


translate spanish day5_main2_8df8d875:


    sl "¿Te apetece ir a dar una vuelta en barca hasta la isla con nosotras?"


translate spanish day5_main2_c16c4c12:


    th "La isla...{w} Bueno, creo que la he visto desde el embarcadero."


translate spanish day5_main2_7332342b:


    me "¿Para qué?"


translate spanish day5_main2_ea29c6fe:


    sl "Olga Dmitrievna nos pidió recoger algunas fresas.{w} ¡Hay muchas fresas allí y son muy deliciosas!"


translate spanish day5_main2_679ff36d:


    "Podía imaginarme el sabor incluso sin probarlas, con tan sólo contemplar el rostro de Slavya."


translate spanish day5_main2_f29bf018:


    me "Fresas...{w} ¿Y para qué son?"


translate spanish day5_main2_2abd657f:


    sl "No lo sé, ¡pero está claro que es una idea genial!"


translate spanish day5_main2_5ccbf2e6:


    th "Bueno, de hecho lo es.{w} Además, todavía no he estado en la isla."


translate spanish day5_main2_1066065f:


    me "Claro, seguro."


translate spanish day5_main2_a20cefa7:


    "..."


translate spanish day5_main2_f2b9563d:


    "En pocos minutos estábamos en el embarcadero."


translate spanish day5_main2_a8b1bc59:


    sl "Bueno, aquí está la barca.{w} Esperad, voy a ir a por los remos ahora."


translate spanish day5_main2_86c42a71:


    "Me quedé solo cara a cara con Lena."


translate spanish day5_main2_1745ad04:


    me "¿Te gustan las fresas?"


translate spanish day5_main2_3898ad76:


    un "Bueno, la verdad es que no...{w} Pero son sabrosas."


translate spanish day5_main2_561828ec:


    "Lena sonrió."


translate spanish day5_main2_f2e007f0:


    me "Ya veo..."


translate spanish day5_main2_3a3284a7:


    "No sabía qué decirle a continuación, cómo seguir con la conversación."


translate spanish day5_main2_858a16c5:


    "Si Slavya no volviera, podríamos probablemente estar sentados aquí hasta el atardecer sin decirnos una palabra."


translate spanish day5_main2_ca656115:


    sl "¡Ahí va!"


translate spanish day5_main2_60c4cc55:


    "Me dio un par de pesados remos."


translate spanish day5_main2_4ab1b169:


    me "Claro... Gracias..."


translate spanish day5_main2_d8f75e4c:


    "Embarcamos en la barca, la desamarré, la empujé fuera de la orilla y traté de empezar a remar."


translate spanish day5_main2_ff778a3e:


    me "¿Y exactamente adónde nos dirigimos?"


translate spanish day5_main2_7c19e280:


    sl "¡Justo ahí!"


translate spanish day5_main2_199f5847:


    "Ella indicó con su dedo hacia la isla."


translate spanish day5_main2_adf7269e:


    sl "Esa isla se llama «La Cercana»."


translate spanish day5_main2_97833beb:


    th "Me pregunto qué capitán le pondría semejante nombre tan original...{w} Bueno, de hecho la isla es próxima a la orilla."


translate spanish day5_main2_0fd8390d:


    me "¡Afirmativo capitán!"


translate spanish day5_main2_c5a9758a:


    th "Si sólo supiera qué me esperaba más adelante..."


translate spanish day5_main2_bb189b0d:


    "No era un remero con experiencia... Había remado una barca tan solamente una o dos veces en toda mi vida."


translate spanish day5_main2_dfd96964:


    "Había menos de medio kilómetro hasta la isla, pero íbamos haciendo el camino en zigzags gracias a mis «habilidades»."


translate spanish day5_main2_c1ed01f4:


    "Aproximadamente a la altura de la mitad del viaje mis brazos me dolían tanto que solté los remos para descansar."


translate spanish day5_main2_cf757ee3:


    me "Bueno...{w} ¿No hay fresas por ahí en cualquier parte?{w} Quiero decir, ¿en lugares más accesibles?"


translate spanish day5_main2_31c67473:


    sl "Pero las más sabrosas son las que crecen ahí."


translate spanish day5_main2_683509ba:


    "Slavya me hizo una mirada desconcertante."


translate spanish day5_main2_6506e675:


    un "¿Es muy duro para ti remar solo?"


translate spanish day5_main2_e1e2ebb7:


    "Lena, a diferencia de Slavya, comprendió todo a la primera."


translate spanish day5_main2_b9dbf1a1:


    me "Oh...{w} No es nada..."


translate spanish day5_main2_7f474b89:


    "De todas formas, no podía dejar que una frágil muchacha me ayudara."


translate spanish day5_main2_255dc29b:


    "El resto del camino me lo pasé concentrado en permanecer ileso mientras llegábamos a la isla."


translate spanish day5_main2_b4c0cce9:


    "Slavya y Lena conversaron sobre algo, pero no estaba escuchando... era demasiado para mí."


translate spanish day5_main2_f0934032:


    "Al fin, llegamos."


translate spanish day5_main2_d6e51afd:


    "Completamente exhausto, llegué a la orilla y miré en dirección al embarcadero."


translate spanish day5_main2_9289c81b:


    "Me pareció estar tan lejos, que me sentí como la primera persona en la luna, observando la Tierra al girar."


translate spanish day5_main2_8f83b289:


    sl "¡Allá vamos!"


translate spanish day5_main2_e7fdbd78:


    "Slavya me dio una cesta."


translate spanish day5_main2_799068d0:


    "Era una isla pequeña... apenas unos cientos de metros de largo."


translate spanish day5_main2_b15dff1c:


    "Y se parecía más a un bosquecillo de abedules, con filas de árboles cubriendo su superficie entera."


translate spanish day5_main2_5f826482:


    "Un mar verde calmado se extendía bajo nuestros pies, con un viento que causaba solitarias oleadas en su superficie de tanto en tanto."


translate spanish day5_main2_2e1cc3e7:


    "Esta isla era como una paraíso perdido."


translate spanish day5_main2_6ec442f5:


    th "No me extraña que las más deliciosas fresas crezcan justo aquí."


translate spanish day5_main2_7568fc9b:


    sl "Tenemos que dividirnos, de esa forma haremos el trabajo más rápido."


translate spanish day5_main2_d76f93fa:


    me "Sí, claro."


translate spanish day5_main2_1f4c91db:


    un "Pero solamente hay dos cestas."


translate spanish day5_main2_f6237fab:


    "Dijo Lena humildemente."


translate spanish day5_main2_e2c244d1:


    sl "Oh, tienes razón, ¡fallo mío!"


translate spanish day5_main2_c1db23a3:


    un "Entonces, ¿cómo nos dividiremos?"


translate spanish day5_main2_94c61009:


    me "Déjame ir contigo."


translate spanish day5_main2_23fc5b72:


    un "Vamos..."


translate spanish day5_main2_7f370439:


    sl "Vale, ¡muy bien!"


translate spanish day5_main2_235efb86:


    "Slavya agarró la segunda cesta y se aventuró hacia el otro lado de la isla."


translate spanish day5_main2_b17829b3:


    me "¿Y bien?"


translate spanish day5_main2_3875ba7b:


    un "¿Bien?"


translate spanish day5_main2_71f4991e:


    me "¿Vamos?"


translate spanish day5_main2_37fef1ca:


    un "Claro."


translate spanish day5_main2_561828ec_1:


    "Lena sonrió."


translate spanish day5_main2_e937d3e9:


    me "¡Solamente presta atención! ¡No dejes ni una fresa atrás!"


translate spanish day5_main2_51487473:


    un "Tú tampoco."


translate spanish day5_main2_a20cefa7_1:


    "..."


translate spanish day5_main2_a704bf84:


    "Así que era la hora de recolectar."


translate spanish day5_main2_afe45067:


    "Ciertamente, las fresas de aquí eran deliciosas."


translate spanish day5_main2_de5b3003:


    "Podría habérmelas comido todas probablemente, si no me hubiese parado a tiempo."


translate spanish day5_main2_f8b9b72c:


    th "A pesar de crecer silvestremente, las fresas eran del mismo tamaño y tenían el mismo rico color rojo como las de cultivo, por lo que estaba claro que nuestra visita aquí no fue en vano."


translate spanish day5_main2_bbcae5a7:


    "Lena me seguía de cerca, ya que solamente teníamos una cesta para ambos."


translate spanish day5_main2_ac634faa:


    "Me sentía como un auténtico cazador de setas, examinando cada matorral y cuidadosamente palpando la hierba."


translate spanish day5_main2_b7921614:


    un "Bueno, eres mejor que yo..."


translate spanish day5_main2_65640e4e:


    me "¿Lo soy?{w} Sinceramente, no las estoy contando."


translate spanish day5_main2_0f83bc7d:


    un "¡Claro que lo eres!"


translate spanish day5_main2_435f0faa:


    "La cesta ya estaba casi llena por la mitad."


translate spanish day5_main2_c88a4757:


    me "Debes disfrutar de la naturaleza, ¿verdad?"


translate spanish day5_main2_f1be5fed:


    un "Sí."


translate spanish day5_main2_5d5a082e:


    "Los brillantes rayos del sol atravesaron las copas de los árboles, cegándome por unos segundos."


translate spanish day5_main2_b6151df9:


    "Me senté en el suelo y me apoyé contra un árbol."


translate spanish day5_main2_7587bc3f:


    me "Aun así, ¡es tan bonito esto!"


translate spanish day5_main2_9b60c62c:


    "Lena se sentó a mi lado.{w} Tan cerca que nuestros codos se tocaban."


translate spanish day5_main2_46b74af0:


    un "¡Claro!"


translate spanish day5_main2_4bfe22ae:


    "Sólo nos sentamos y disfrutamos del momento.{w} Parecía que el tiempo se detuvo."


translate spanish day5_main2_9c76c985:


    "El viento agitaba suavemente las hojas de los árboles, algunos insectos perezosamente saltaban por la hierba y las manchas de luz solar salpicaban jugueteando con la lejana superficie del agua."


translate spanish day5_main2_78fd2536:


    "Lena apoyó su cabeza en mi hombro."


translate spanish day5_main2_d1d0b93e:


    "Estaba sorprendido al principio, pero luego escuché su respiración normal y pensé que sería sólo cosa del tiempo."


translate spanish day5_main2_bbbf378c:


    "Probablemente se sintió soñolienta y quiso hacer una siesta rápida."


translate spanish day5_main2_464ee2c5:


    "Simplemente me quedé sentado ahí y sin pensar en nada durante unos minutos."


translate spanish day5_main2_9dcce076:


    "Pero entonces, comenzaron a surcar por mi mente palabras a la velocidad ultrasónica:"


translate spanish day5_main2_4bf43e20:


    th "Lena.{w} Tan cerca.{w} Durmiendo.{w} Tan cálida.{w} Tan suave.{w} Sentimientos..."


translate spanish day5_main2_3a8d7079:


    "Me quedé observándola."


translate spanish day5_main2_a5887389:


    "Tenía tal aspecto tan sereno y tranquilo en su cara, que parecía que ella no estuviera justo aquí, sino en algún tipo de mundo mucho mejor."


translate spanish day5_main2_f950a1bd:


    "No sé que habría sucedido en el siguiente instante de no haber escuchado la voz de Slavya."


translate spanish day5_main2_2295bc5f:


    sl "¡Semyon! ¡Lena!"


translate spanish day5_main2_fdadda66:


    "Moví la cabeza de lado a lado durante unas cuantas veces para recobrar el sentido.{w} Lena comenzó a despertarse."


translate spanish day5_main2_c0ebc5f2:


    "Abrió sus ojos y me hizo una mirada inexpresiva."


translate spanish day5_main2_badffbcb:


    me "¿Tuviste un buen sueño?"


translate spanish day5_main2_d0ef9648:


    un "¿Eh?"


translate spanish day5_main2_8d04e459:


    "De repente, percatándose de que se adormeció apoyándose en mi hombro, Lena se ruborizó."


translate spanish day5_main2_be58af4f:


    un "Oh... Lo siento..."


translate spanish day5_main2_6d36c28b:


    me "No pasa nada."


translate spanish day5_main2_2eefbc0c:


    "Slavya se acercó a nosotros, así que Lena se levantó rápidamente."


translate spanish day5_main2_fde84cf5:


    sl "Bueno, ¿y cuántas conseguisteis?"


translate spanish day5_main2_da3a9368:


    "Suspiré."


translate spanish day5_main2_633224a4:


    sl "Eso no es mucho..."


translate spanish day5_main2_380af4e6:


    "Su cesta estaba llena de fresas a rebosar."


translate spanish day5_main2_e384ce23:


    sl "Bueno, ¡es suficiente igualmente!{w} ¡Es hora de volver!"


translate spanish day5_main2_7cd2b80a:


    "Agarré la cesta y nos dirigimos hacia la barca."


translate spanish day5_main2_790a16f6:


    "No quería caminar por aquí solo, así que esperaba que Slavya se uniría a mi, ya que no me atrevía a pedírselo."


translate spanish day5_main2_e6680b45:


    me "Bueno, es obvio: una cesta para mí, otra para vosotras."


translate spanish day5_main2_97bf0978:


    sl "¡No, déjame ir contigo!"


translate spanish day5_main2_c17a722d:


    "Slavya sonrió."


translate spanish day5_main2_35ec24e9:


    me "Vale..."


translate spanish day5_main2_f8089075:


    "Estaba un poco sorprendido, pero también estaba contento de que fuera así."


translate spanish day5_main2_46439482:


    "Lena no resultó enfadarse u ofenderse para nada."


translate spanish day5_main2_a20cefa7_2:


    "..."


translate spanish day5_main2_a704bf84_1:


    "La cosecha ha comenzado."


translate spanish day5_main2_afe45067_1:


    "Las fresas eran deliciosas aquí, en verdad."


translate spanish day5_main2_de5b3003_1:


    "Podría llegar a comérmelas todas si no me detuviera a tiempo."


translate spanish day5_main2_f8b9b72c_1:


    th "A pesar de crecer silvestremente, las fresas eran del mismo tamaño y tenían el mismo rico color rojo como las de cultivo, por lo que estaba claro que nuestra visita aquí no fue en vano."


translate spanish day5_main2_11dc0364:


    "Slavya estaba caminando justo a mi lado, ya que teníamos solamente una cesta."


translate spanish day5_main2_ac634faa_1:


    "Me sentía como un auténtico cazador de setas, examinando cada matorral y cuidadosamente palpando la hierba."


translate spanish day5_main2_9f273fda:


    sl "¡Presta atención!"


translate spanish day5_main2_eebba4df:


    "Un completo montón de fresas habían sido dejadas atrás."


translate spanish day5_main2_cd47bc4e:


    me "Ah, claro...{w} Lo siento."


translate spanish day5_main2_705bea28:


    sl "No pasa nada."


translate spanish day5_main2_9db1f947:


    me "Debes estar disfrutando estando aquí, ¿no?{w} Después de todo te gusta la naturaleza."


translate spanish day5_main2_49cc0ae4:


    sl "¡Por supuesto!"


translate spanish day5_main2_c17a722d_1:


    "Slavya sonrió."


translate spanish day5_main2_0854ae45:


    sl "Me recuerda a mi hogar... tenemos abedules similares allí."


translate spanish day5_main2_f32c544b:


    "Ella contemplaba soñadora algún lugar perdido en la distancia."


translate spanish day5_main2_14e38355:


    me "Mira, siempre quise preguntártelo... ¿qué te gusta en general?"


translate spanish day5_main2_ed0e2cff:


    me "Pareces estar ocupada las 24 horas del día, los siete días de la semana, y pareciera que no tienes tiempo para descansar en absoluto."


translate spanish day5_main2_7db509b3:


    sl "Ehmmm…"


translate spanish day5_main2_60f3d758:


    "Comenzó a reflexionar."


translate spanish day5_main2_63626b5d:


    sl "No lo sé realmente.{w} Hacer diversas actividades es algo que disfruto."


translate spanish day5_main2_882dd540:


    me "Bueno, eso es comprensible pero, ¿y aun así?"


translate spanish day5_main2_9698acdd:


    sl "¡Me gusta el punto y la costura!{w} Cosas así..."


translate spanish day5_main2_f22d26a4:


    "Slavya sacó un pañuelo de su bolsillo.{w} Había flores rojas, amarillas y verdes bordadas en él."


translate spanish day5_main2_aef33dd6:


    "Estaban enmarañadas las unas con las otras de una forma complicada, creando sofisticadas formas geométricas."


translate spanish day5_main2_576c9c96:


    "Un pañuelo típico ruso hecho a mano."


translate spanish day5_main2_fa5e2895:


    "Al echarle un vistazo, al instante imaginaba a Slavya vestida con un antiguo vestido de sarafán, sentada en un banco junto a una casa en ruinas con múltiples niños jugando y correteando a su alrededor."


translate spanish day5_main2_9918c29c:


    me "Es bastante precioso."


translate spanish day5_main2_8c1bfef0:


    sl "¡Gracias!{w} ¡Déjame que te lo regale!"


translate spanish day5_main2_95e49024:


    "Tal propuesta me avergonzó."


translate spanish day5_main2_08a40647:


    me "No deberías..."


translate spanish day5_main2_127a57de:


    sl "¡No, cógelo!"


translate spanish day5_main2_72fe7131:


    "Miré el pañuelo una vez más y me lo puse en el bolsillo."


translate spanish day5_main2_73aab3cd:


    me "Gracias..."


translate spanish day5_main2_a20cefa7_3:


    "..."


translate spanish day5_main2_ff2ae452:


    "Habían tantas fresas aquí que, tras media hora, teníamos la cesta llena a rebosar."


translate spanish day5_main2_fb9748eb:


    me "Parece que terminamos..."


translate spanish day5_main2_e498e9bd:


    sl "Sí.{w} Tenemos muchas, así que seguro que deberían ser suficientes."


translate spanish day5_main2_f110cff6:


    "Cuando volvimos a la barca, Lena aun no estaba allí."


translate spanish day5_main2_b852b10d:


    sl "Necesitará más tiempo para llenar la cesta al estar sola."


translate spanish day5_main2_ff886d6d:


    me "Claro, supongo..."


translate spanish day5_main2_94972c05:


    "Miré al río."


translate spanish day5_main2_827d1834:


    "Los centelleos del sol bailaban jovialmente a través de la superficie del agua, donde era lo único que los diferenciaba de un espejo... así es como el río resultaba estar en calma."


translate spanish day5_main2_29dcd13b:


    sl "¿Qué piensas?"


translate spanish day5_main2_51eebe65:


    me "Nada en verdad...{w} ¿Y tú?"


translate spanish day5_main2_7be9fd97:


    sl "¿Yo...?{w} ¿Qué ocurrirá cuando las vacaciones terminen? Tendremos que abandonar este campamento y volver a nuestros hogares..."


translate spanish day5_main2_08224f21:


    sl "¿Me volveré a encontrar otra vez con alguien que haya conocido aquí?{w} ¿Volveré a verte otra vez...?"


translate spanish day5_main2_e932afd0:


    "Me miraba con los ojos tan llenos de melancolía, que no sabía qué decirle."


translate spanish day5_main2_bffe94f9:


    "Lena apareció de la nada, rompiendo el silencio."


translate spanish day5_main2_bfcc6d0c:


    un "Oh, ya estáis...{w} Aquí está."


translate spanish day5_main2_26b9d76b:


    "Nos mostró su cesta llena de fresas."


translate spanish day5_main2_e2c68eb7:


    sl "¡Genial!{w} Ahora podemos volver."


translate spanish day5_main2_3ea9d288:


    "Todavía tenía en mi mente el rostro de Slavya y aquellas palabras."


translate spanish day5_main2_9b1d3958:


    "Tristeza y melancolía no eran los sentimientos típicos de ella."


translate spanish day5_main2_8bcb5251:


    th "¿Podría estar ocultándolos bajo esa máscara de alegría?"


translate spanish day5_main2_54c1d08a:


    "No tenía una respuesta a esta cuestión, y sabía que tampoco podría hallar una igualmente."


translate spanish day5_main2_e3a29d10:


    th "Tal vez más tarde..."


translate spanish day5_main2_4d28e778:


    "El camino de vuelta llevó menos tiempo, en tanto que traté de concentrarme en remar e ignoré todo lo demás."


translate spanish day5_main2_1012dd1b:


    "Mi único deseo era mantenerme en vida, en tanto que el primer viaje no tuvo ningún accidente y ahora mis manos empezaban a estar agotadas de los barridos con los remos."


translate spanish day5_main2_d6437409:


    "Habiendo amarrado la barca, me desplomé en el suelo sin más energía."


translate spanish day5_main2_ba4a18aa:


    "Slavya y Lena se inclinaron hacia mí."


translate spanish day5_main2_379cb129:


    sl "¡Podrías habernos dicho algo si era tan duro para ti!"


translate spanish day5_main2_31b01f67:


    un "Sí..."


translate spanish day5_main2_bb2ef35c:


    me "No importa, está bien...{w} Decansaré un poco aquí y me recupero..."


translate spanish day5_main2_50def11b:


    sl "Vale, pues lleva estas cestas a Olga Dmitrievna por favor, tenemos algo que hacer."


translate spanish day5_main2_85279b68:


    me "Sí, claro."


translate spanish day5_main2_6d720424:


    "Estaba preparado para decir que sí a cualquier cosa en aquel momento, así tan sólo no tendría que levantarme."


translate spanish day5_main2_3bfb15a5:


    "Slavya dejó las cestas llenas de fresas a mi lado y se dirigió hacia la plaza, conversando felizmente con Lena sobre alguna cosa."


translate spanish day5_main2_cb288325:


    me "La parte más difícil ya está hecha..."


translate spanish day5_main2_258341cc:


    "Eso es lo que pensé antes de levantarme y coger las cestas..."


translate spanish day5_main2_f1fdfa2c:


    "Tras remar sentía que fueran como sacos de cemento, incluso cuando sólo pesaran apenas poco más de unos cuantos kilogramos cada una."


translate spanish day5_main2_331ec88f:


    "Así que el viaje hacia la cabaña de la líder me llevó más tiempo de lo habitual... tenía que detenerme cada cincuenta metros para descansar."


translate spanish day5_main2_3f67afab:


    "Una vez llegué, dejé las cestas en el suelo y me senté con dificultad en la tumbona."


translate spanish day5_main2_30582463:


    me "¡Olga Dmitrievna!{w} Olga Dmitrievna, ¡tengo regalos para ti!"


translate spanish day5_main2_dfffcbac:


    "No hubo ninguna respuesta."


translate spanish day5_main2_a11cb528:


    "Apenas pude levantarme y entrar en la cabaña."


translate spanish day5_main2_40d2a09a:


    "No había nadie allí."


translate spanish day5_main2_d189a7a2:


    me "Si no las necesitas... como quieras..."


translate spanish day5_main2_c3f0764a:


    "Me tumbé en la tumbona y me dormí."


translate spanish day5_main2_8bc92ca0:


    "Tuve un sueño raro sobre una carrera con fresas."


translate spanish day5_main2_f12d326b:


    "Estaba remando una barca con mi última pizca de fuerza, tratando de escapar de fresas que me perseguían."


translate spanish day5_main2_b573ff1d:


    "Mis manos me estaban fallando y apenas podía ver algo debido al sudor que cubría mi cara. La sangre bombeaba en mi sien, pero las fresas se me estaban acercando."


translate spanish day5_main2_371f84cd:


    "Me mostraban sus dientes amenazadoramente."


translate spanish day5_main2_57cc90da:


    th "Pero espera...{w} ¡¿Fresas con dientes?!"


translate spanish day5_main2_bed90e86:


    mt "¡...Semyon! ¡Semyon!"


translate spanish day5_main2_4b4e1333:


    "Me desperté."


translate spanish day5_main2_821dbe9f:


    "Olga Dmitrievna estaba de pie a mi lado, sacudiéndome los hombros."


translate spanish day5_main2_4f14ab7e:


    mt "Veo que conseguiste una abundante recolecta, ¿no?"


translate spanish day5_main2_f0de0091:


    mt "Vale. ¡Pero eso no es todo!"


translate spanish day5_main2_b635cee7:


    th "En serio, estaba a punto de tener un descanso buenísimo..."


translate spanish day5_main2_6091ae1b:


    mt "¿Sabes para qué son estas fresas?"


translate spanish day5_main2_230628fb:


    me "Ni idea..."


translate spanish day5_main2_f1e8b4cf:


    "Qué confesión más honesta."


translate spanish day5_main2_035bcc73:


    mt "¡Haremos un pastel con ellas!"


translate spanish day5_main2_f2e007f0_1:


    me "Ya veo..."


translate spanish day5_main2_918f0332:


    th "Bueno, eso tiene sentido."


translate spanish day5_main2_cf6837ea:


    mt "¡En honor del milagroso rescate de Shurik!{w} ¡Y todo gracias a ti!"


translate spanish day5_main2_e8bfedc9:


    "Estaba claro que traer las fresas no era la última cosa que tenía que hacer."


translate spanish day5_main2_b125d822:


    th "Y por qué, por favor explícamelo, si soy tal héroe, ¿por qué tengo que organizar una celebración en mi nombre por mí mismo...?"


translate spanish day5_main2_443723fe:


    me "Bueno, supongo..."


translate spanish day5_main2_cb1beef9:


    mt "En fin...{w} ¡Tengo una importante tarea para ti!"


translate spanish day5_main2_a8dc8e24:


    mt "Nos faltan levadura, harina y azúcar.{w} ¡Y las necesito todas en la cantina antes de la cena!"


translate spanish day5_main2_1cdc0576:


    me "¿Y aquellos que van a hacer el pastel, no pueden acaso ocuparse de esto por su propia mano de alguna forma?"


translate spanish day5_main2_13e8802b:


    "Pregunté lamentándome."


translate spanish day5_main2_4cb23232:


    mt "¡Por supuesto que no pueden!{w} ¡Todos ellos están ocupados!{w} ¡Y tú eres el único en todo el campamento que no hace nada!"


translate spanish day5_main2_c4eaf48d:


    "Aunque sus palabras eran parcialmente ciertas, eso no lo hacía más fácil para mí."


translate spanish day5_main2_aefacb3e:


    "Además, aquellas palabras se sintieron como un balazo en la cabeza."


translate spanish day5_main2_05b86f04:


    mt "Así que, ¡toma nota!{w} Conseguirás la levadura en la enfermería, la harina en la biblioteca y el azúcar en el edificio de clubs."


translate spanish day5_main2_42814c09:


    me "Espera, espera un..."


translate spanish day5_main2_e9da3bbd:


    mt "No tengo tiempo, ¡tengo prisa!{w} ¡Buena suerte!"


translate spanish day5_main2_0457a719:


    "Ella sonrió astutamente y se fue."


translate spanish day5_main2_8824565a:


    th "Está claro que hay un montón de cosas extrañas en este campamento, pero...{w} ¿Levadura en una enfermería? Vale, puedo aceptarlo. Pero...{w} ¿Harina en una biblioteca?"


translate spanish day5_main2_dd117348:


    th "Y azúcar...{w} ¡No, está más allá de mi comprensión!"


translate spanish day5_main2_d71f77ee:


    "Escupí al suelo."


translate spanish day5_main2_6a2ac63d:


    me "¡Ni quiero, ni creeré en esto! Dime, sólo dímelo: ¿me estás tomando el pelo?"


translate spanish day5_main2_2b37191e:


    "No me sorprendería si un montón de trolls gordos y verdosos aparecieran justo aquí a mi lado, cada uno de ellos sintiéndose obligados a reírse de mi."


translate spanish day5_main2_b150a7df:


    th "¿Quizás así se va al infierno el pastel...?"


translate spanish day5_main2_f58ed270:


    "Sopesé mis opciones durante un rato."


translate spanish day5_main2_c815c3c0:


    th "No, si el gran plan de Olga Dmitrievna fracasaba, entonces estaría acabado."


translate spanish day5_main2_7cca4369:


    th "Y complicaría tanto mi vida en el campamento, como mi búsqueda de respuestas, las cuales dejé de buscar hace tiempo."


translate spanish day5_main2_515e2ec4:


    th "Parece que no tengo alternativa..."


translate spanish day5_aidpost_2_71caa85a:


    th "Sentí como si hubiera visitado la enfermería en demasiadas ocasiones recientemente."


translate spanish day5_aidpost_2_7a9eec06:


    th "Pero qué puedo hacer... así es como van las cosas."


translate spanish day5_aidpost_2_af5fbbdb:


    "Suspiré y llamé a la puerta."


translate spanish day5_aidpost_2_e9203c64:


    cs "¡Adelante!"


translate spanish day5_aidpost_2_e6e52c0a:


    "La enfermera lo dijo con cierto acento risueño."


translate spanish day5_aidpost_2_b9a6c45b:


    me "¡Buenas tardes!{w} Olga Dmitrievna me ha enviado para coger un poco de..."


translate spanish day5_aidpost_2_5286c4cf:


    "Dudé un poco."


translate spanish day5_aidpost_2_68451631:


    me "Levadura..."


translate spanish day5_aidpost_2_2d204cc8:


    cs "Ah, claro."


translate spanish day5_aidpost_2_6d77584d:


    "Ella me hizo una amplia sonrisa."


translate spanish day5_aidpost_2_524e70a4:


    cs "Es sólo que no me queda....{w} pionero."


translate spanish day5_aidpost_2_e8bf1827:


    me "¿Cómo es eso?{w} Ella dijo que..."


translate spanish day5_aidpost_2_48f8061d:


    cs "Bueno, tenía algo, pero ahora ya no queda."


translate spanish day5_aidpost_2_62e70926:


    "No me molesté en preguntarle por qué tenían en primer lugar."


translate spanish day5_aidpost_2_55f0b627:


    cs "Bueno, no te preocupes.{w} Puedes coger alguna aspirina, por ejemplo."


translate spanish day5_aidpost_2_624f6a17:


    th "Eso podría serme de utilidad, en serio."


translate spanish day5_aidpost_2_ec7a55c9:


    me "¿Dónde la consigo entonces...?"


translate spanish day5_aidpost_2_ed0914b5:


    "Suspiré."


translate spanish day5_aidpost_2_19a3e4d7:


    cs "¡Ten esto!"


translate spanish day5_aidpost_2_4fb9e776:


    "Abrió el cajón y extrajo un tipo de botella."


translate spanish day5_aidpost_2_3a770cd2:


    "Le eché un vistazo de cerca.{w} Era cerveza «Ostankinskoe»."


translate spanish day5_aidpost_2_51149e68:


    me "..."


translate spanish day5_aidpost_2_94e2a40d:


    cs "¿Qué pasa?{w} La cerveza es un producto fermentado también."


translate spanish day5_aidpost_2_8fb93608:


    "Me contempló profundamente."


translate spanish day5_aidpost_2_97a20ab2:


    cs "¡Nadie se dará cuenta!"


translate spanish day5_aidpost_2_51d589e0:


    "En parte tenía razón, pero todo me pareció tan grotesco que no podía saber qué decir."


translate spanish day5_aidpost_2_d9f00730:


    me "¿Estás... segura?"


translate spanish day5_aidpost_2_a61a2efe:


    cs "¡Absolutamente!"


translate spanish day5_aidpost_2_d76d86ac:


    me "Vale pues..."


translate spanish day5_aidpost_2_fe10b3f1:


    "La botella claramente no entraría en los bolsillos de mis pantalones cortos."


translate spanish day5_aidpost_2_73aab3cd:


    me "Bueno, gracias..."


translate spanish day5_aidpost_2_e7fd1586:


    "Murmuré tímidamente, abandonando la enfermería."


translate spanish day5_aidpost_2_942696fd:


    th "Bueno, la cerveza en cierta manera podía sustituir la levadura."


translate spanish day5_aidpost_2_a2b4dc7e:


    th "Incluso mi limitado conocimiento en química y biología, eran suficientes para aceptarlo.{w} Pero..."


translate spanish day5_aidpost_2_4997ec0f:


    "Por lo general, caminar por ahí con esta botella en mis manos me parecía una idea de tontos, así que decidí llevármela a la cabaña de Olga Dmitrievna y esconderla allí."


translate spanish day5_aidpost_2_aeaa1520:


    "Pero tenía que alcanzarla de alguna forma sin que nadie se diera cuenta de la cerveza."


translate spanish day5_aidpost_2_f988b661:


    "Escondí la botella bajo mi camisa."


translate spanish day5_aidpost_2_b2e65042:


    "Y todo habría ido bien, pero Slavya me llamó al pasar por la plaza."


translate spanish day5_aidpost_2_9b22a2b9:


    "En realidad, me asaltó por detrás tan repentinamente que incluso me dio un susto."


translate spanish day5_aidpost_2_99a001ee:


    sl "¿Cómo va eso?"


translate spanish day5_aidpost_2_ad02f53a:


    me "¿Exactamente el qué?"


translate spanish day5_aidpost_2_28a39c69:


    sl "Tu búsqueda de los ingredientes."


translate spanish day5_aidpost_2_ed34a1cf:


    me "Ah, así que ya lo sabes..."


translate spanish day5_aidpost_2_2bb8d573:


    sl "¡Sí!"


translate spanish day5_aidpost_2_c17a722d:


    "Slavya sonrió."


translate spanish day5_aidpost_2_7de85321:


    me "Va bien..."


translate spanish day5_aidpost_2_f7e1cdf8:


    "Respondí, tratando de no mostrar mi inquietud."


translate spanish day5_aidpost_2_9d5782dc:


    sl "¿Y qué tienes ahí?"


translate spanish day5_aidpost_2_c76c023d:


    "Ella señaló la botella que salía debajo de mi camisa."


translate spanish day5_aidpost_2_5652d5eb:


    me "Ah, esto..."


translate spanish day5_aidpost_2_bda7946b:


    th "¡Me descubrió!"


translate spanish day5_aidpost_2_4c420dcf:


    me "Ah, no es nada..."


translate spanish day5_aidpost_2_478e684d:


    "Me sonrojé con una risita tonta."


translate spanish day5_aidpost_2_d96f5cdf:


    me "Es la hora de irse..."


translate spanish day5_aidpost_2_bc62fb23:


    "Estaba casi corriendo, dejando atrás la plaza con una perpleja Slavya."


translate spanish day5_aidpost_2_74475de8:


    th "Es genial que ella sea ese tipo de personas que no preguntan cosas innecesarias."


translate spanish day5_aidpost_2_bd1cc79d:


    "Pero hay otras personas en este campamento, las cuales no tienen otra cosa que hacer que meter la narices donde no les llaman, en asuntos que no les conciernen."


translate spanish day5_aidpost_2_0616ad18:


    "Cruzadas las cabañas de los pioneros, tropecé con Ulyana."


translate spanish day5_aidpost_2_397ec628:


    us "¿Qué estás ocultando ahí?"


translate spanish day5_aidpost_2_07c1adb6:


    "Me estaba observando con una de esas descaradas miradas suyas."


translate spanish day5_aidpost_2_6ada69b4:


    "Pensé que no tenía sentido negar nada, así que le contesté de manera provocativa:"


translate spanish day5_aidpost_2_38b6a274:


    me "¡No es asunto tuyo! Soy un oficial de cifrados portando un mensaje al cuartel general."


translate spanish day5_aidpost_2_1ebafc67:


    us "Eso es ciertamente un gran...{w} mensaje..."


translate spanish day5_aidpost_2_fd58cad9:


    "Estaba cargando con la botella a la altura de la cintura, así que estuve un poco avergonzado."


translate spanish day5_aidpost_2_d8a23d43:


    us "¿Quieres ayuda?"


translate spanish day5_aidpost_2_5fc5692c:


    me "¡Me las apañaré solo!"


translate spanish day5_aidpost_2_5a4c3675:


    "Pasé caminando por delante de ella con confianza y procedí con mi destino."


translate spanish day5_aidpost_2_fbcbd186:


    "Para mi sorpresa, ella no dijo nada, ni siquiera me siguió."


translate spanish day5_aidpost_2_1266f2bc:


    "No había nadie en la cabaña de Olga Dmitrievna, por lo que me las arreglé con éxito para poner la botella bajo mi cama."


translate spanish day5_aidpost_2_4e945031:


    "Una vez estuve fuera, suspiré con alivio."


translate spanish day5_aidpost_2_74665f46:


    "De verdad, ¡no podía creerme que me preocuparía tanto por una sola botella de cerveza!{w} Como si hubiera retornado al instituto."


translate spanish day5_aidpost_2_af3e0460:


    th "Ahora que ya está a salvo..."


translate spanish day5_aidpost_2_ceb963db:


    th "Incluso si alguien la encuentra, puedo declarar que no es mía... Siempre podría pensar en alguna excusa creíble gracias a mi enorme experiencia."


translate spanish day5_clubs_2_bb74ce24:


    "Siento que he hecho más cosas hoy, que todos los días anteriores juntos."


translate spanish day5_clubs_2_7fddb993:


    "De este modo, aproximándome al edificio de clubes, casi había olvidado pensar en cómo de incómodo debe ser ir a buscar azúcar ahí."


translate spanish day5_clubs_2_ebe5438a:


    "Shurik y Electronik estaban construyendo con mucho entusiasmo alguna cosa."


translate spanish day5_clubs_2_2e2f3c1b:


    "Estaban tan ocupados, que ni se dieron cuenta de mi presencia."


translate spanish day5_clubs_2_2b02462e:


    "Miré de cerca."


translate spanish day5_clubs_2_3af379d1:


    "Era algún tipo de robot...{w} O al menos el cuerpo de alguno."


translate spanish day5_clubs_2_729ff343:


    "Además, este robot era una hembra y tenía orejas de animal."


translate spanish day5_clubs_2_a869bd00:


    "No quise levantar teorías sobre la finalidad o propósito de semejante dispositivo construido por tales celebridades del campo de las cibernéticas."


translate spanish day5_clubs_2_c69b15fb:


    "Incluso aunque el diseño pareciera funcional, tenía mis dudas de que este robot pudiera algún día conquistar la Tierra, o al menos de ser capaz de hacer algo por sí mismo."


translate spanish day5_clubs_2_81faf816:


    "Pero parecían divertirse más en el proceso de contruirlo, que en los propios resultados finales."


translate spanish day5_clubs_2_76cfb844:


    "Y eso era algo que compartía, incluso a pesar de que no lo quisiera admitir."


translate spanish day5_clubs_2_d63ce887:


    "Por otra parte, no estaban preocupados por un posible fracaso, por las críticas o las bromas...{w} Trabajaban para su fin sin prestar atención a los demás, quienes lo tildarían de idealista o incluso de absurdo."


translate spanish day5_clubs_2_ff329017:


    th "Vaya, pareciera que los comparo en realidad con otras celebridades de la ciencia..."


translate spanish day5_clubs_2_b90af56a:


    me "Ey, muchachos."


translate spanish day5_clubs_2_853a9b87:


    "Les saludé inseguro."


translate spanish day5_clubs_2_5c419eaf:


    sh "¡Oh! ¡Semyon! Ven, ¡siempre nos alegra verte por aquí!"


translate spanish day5_clubs_2_d193794e:


    th "En realidad ya estaba dentro..."


translate spanish day5_clubs_2_2bd5ad91:


    sh "Sabes...{w} ¡Perdona por lo que ocurrió ayer!{w} Apenas recuerdo nada, pero...{w} Bueno..."


translate spanish day5_clubs_2_95e7fcc0:


    me "¡Olvídalo! ¡No pasa nada!"


translate spanish day5_clubs_2_f3cf77af:


    el "¿Y qué te trae por nuestra humilde morada?"


translate spanish day5_clubs_2_e7ed1aae:


    "Electronik me miraba tímidamente."


translate spanish day5_clubs_2_f069bcd3:


    "A veces siento que hace esa cara cuando sabe algo acerca de la otra persona, algo que puede usar en el momento idóneo."


translate spanish day5_clubs_2_88b7e8e0:


    me "Azúcar.{w} Necesito azúcar."


translate spanish day5_clubs_2_e4fdc3d2:


    "El recuerdo de una imagen de un antiguo videojuego me vino de repente a la cabeza, donde algún tipo de unidad como un constructor o algo así, gritaba con todos sus cinco píxels de estatura: «¡Oro! ¡Necesitamos más oro!»."


translate spanish day5_clubs_2_9965d584:


    el "Tenemos."


translate spanish day5_clubs_2_9b27ce05:


    "Dijo Electronik tranquilamente."


translate spanish day5_clubs_2_edb7828d:


    sh "¿Para qué lo quieres?"


translate spanish day5_clubs_2_6a1f6f30:


    "Me pareció que no debería explicarle a Shurik que querían cocinar un pastel para él.{w} No debería estropearle la sorpresa."


translate spanish day5_clubs_2_cea669a0:


    me "No lo sé...{w} Olga Dmitrievna me dijo que consiguiera un poco..."


translate spanish day5_clubs_2_f7be46a6:


    el "Vale, espera."


translate spanish day5_clubs_2_c1165691:


    "Electronik desapareció tras la puerta de la habitación de al lado."


translate spanish day5_clubs_2_72678105:


    me "¿Y por qué tenéis azúcar aquí? ¿Por qué no en la cantina?"


translate spanish day5_clubs_2_c5831f8c:


    sh "Cuando el camión de víveres vino la última vez, fue la última cosa a descargar."


translate spanish day5_clubs_2_8afaf429:


    sh "Y puesto que nuestro edificio es el más cercano a la entrada, decidieron dejarlo aquí para ahorrarse esfuerzos..."


translate spanish day5_clubs_2_9b463aae:


    th "Eso es razonable, ¿verdad?"


translate spanish day5_clubs_2_bb104bbc:


    "La puerta se abrió, revelando a Electronik tirando de un enorme saco tras él."


translate spanish day5_clubs_2_d24fcbc5:


    "No sabía realmente cómo de grande iba a ser el pastel, pero aquello era obviamente demasiada azúcar."


translate spanish day5_clubs_2_30543032:


    me "Bueno, gracias, pero no la necesito toda..."


translate spanish day5_clubs_2_776a0c07:


    el "¿Pero dónde la pondremos?"


translate spanish day5_clubs_2_961b180e:


    "Electronik me hizo una mirada de sorpresa."


translate spanish day5_clubs_2_590bb4f8:


    el "No tenemos nada para ponerla.{w} Pediste la azúcar... pues llévatela."


translate spanish day5_clubs_2_ec12a3f5:


    th "Parecía ser que la sonrisa anterior no fue sin motivo."


translate spanish day5_clubs_2_0cff3fa1:


    me "¿Tal vez si me ayudas?{w} No hay que cargarla hasta muy lejos..."


translate spanish day5_clubs_2_40b931b1:


    el "Estamos ocupados."


translate spanish day5_clubs_2_07640196:


    "Indicó con su mano al robot."


translate spanish day5_clubs_2_c06e6e9a:


    "Le hice una mirada a Shurik.{w} Me lo debía, después de todo."


translate spanish day5_clubs_2_d7fa97fa:


    "Dudó y luego apartó la mirada avergonzado."


translate spanish day5_clubs_2_9b5a3058:


    "Suspiré, agarré el saco y me dirigí hacia la puerta."


translate spanish day5_clubs_2_d9acf4d6:


    me "Gracias de todas formas..."


translate spanish day5_clubs_2_d52a2c3a:


    "Dije al partir, afanándome."


translate spanish day5_clubs_2_256429b1:


    "Pero no llegué muy lejos."


translate spanish day5_clubs_2_87117e72:


    "Justo tras poco más de veinte metros, tuve que dejar el saco en el suelo para descansar."


translate spanish day5_clubs_2_0fd93e15:


    "No tenía ni idea cuánto pesaba, pero se notaba como si fueran más de veinte kilos."


translate spanish day5_clubs_2_4099b982:


    "Por una parte, eran tan sólo unos doscientos metros hasta la cantina."


translate spanish day5_clubs_2_e147e5e5:


    "Por otra parte, incluso tal distancia con esta pesada carga en mi hombro (o en su lugar en mis manos, en mis piernas, bajo mi brazo o incluso en mi cabeza) me resultaba imposible de hacer."


translate spanish day5_clubs_2_d88fce53:


    "Y en tanto que me resigné a moverme en pequeñas carreras con prolongadas pausas entre ellas (así podría llegar por la noche al menos), escuché una voz detrás de mí:"


translate spanish day5_clubs_2_73f17541:


    un "Tal vez podría ayudarte..."


translate spanish day5_clubs_2_1cd2d1a1:


    "Vi a Lena delante de mí."


translate spanish day5_clubs_2_5cbd7cac:


    me "No creo que puedas..."


translate spanish day5_clubs_2_2fb0f57d:


    "Era uno de aquellos momentos en que me sentí tan fastidiosamente en tan baja forma..."


translate spanish day5_clubs_2_be079536:


    un "Puedo traer un carrito."


translate spanish day5_clubs_2_dbff5c0c:


    th "Un carrito...{w} ¡¿Por qué no se me ocurrió eso a mi?!"


translate spanish day5_clubs_2_02b2a515:


    me "Sí, ¡eso sería genial!"


translate spanish day5_clubs_2_a8585d26:


    un "Espera aquí, ¡vuelvo en seguida!"


translate spanish day5_clubs_2_47fe246e:


    "Sonrió y se fue corriendo en dirección a la plaza."


translate spanish day5_clubs_2_e8fbb4d7:


    th "¿Qué habría hecho sin ella...?"


translate spanish day5_clubs_2_8efa90a2:


    th "Qué bien que Lena no sea siempre tímida y pueda tomar la iniciativa a veces."


translate spanish day5_clubs_2_89141735:


    "Comencé a pensar."


translate spanish day5_clubs_2_952b5881:


    "Parece bastante atípica ahora.{w} Sin rasgo alguno de timidez en su rostro, en realidad todo lo contrario... sonrisas y confianza."


translate spanish day5_clubs_2_017038ff:


    "El ofrecimiento de ayuda no es que fuera algo extraordinario por sí mismo, pero viniendo de Lena..."


translate spanish day5_clubs_2_a1ed0501:


    "Minutos más tarde, ella volvió con un pequeño carrito."


translate spanish day5_clubs_2_3fed3b65:


    "Puse el saco en él."


translate spanish day5_clubs_2_3066598f:


    me "Gracias."


translate spanish day5_clubs_2_69777dad:


    un "No hay de qué..."


translate spanish day5_clubs_2_42f94d04:


    "Ella se ruborizó y miró al suelo."


translate spanish day5_clubs_2_3ecb53f0:


    th "Vaya, ¡la Lena que todos conocemos ha vuelto!"


translate spanish day5_clubs_2_ab1d28c0:


    un "En fin, me voy pues..."


translate spanish day5_clubs_2_45858c8d:


    me "Sí, ¡nos vemos!{w} ¡Y gracias de nuevo!"


translate spanish day5_clubs_2_3da1183b:


    "Le grité cuando se iba."


translate spanish day5_clubs_2_d45c5c2b:


    "A veces tengo la sensación de que hay dos personas diferentes viviendo dentro de Lena."


translate spanish day5_clubs_2_0b282078:


    "Pero la segunda... confiada, feliz y a veces incluso descarada... solamente aparece cuando me habla a mí."


translate spanish day5_clubs_2_9a4a089a:


    th "¿O me lo estoy imaginando otra vez...?"


translate spanish day5_clubs_2_fce0897d:


    "Pensé que sería mejor si consiguiera todos los ingredientes de una vez, por lo que me dirigí hacia la cabaña de Olga Dmitrievna con el carrito."


translate spanish day5_library_2_d8c69e27:


    "Si cada uno de los lugares de la lista de ingredientes hasta ahora se le podía sacar algo de sentido, la harina de la biblioteca me resultó imposible."


translate spanish day5_library_2_94112c60:


    "Pensé mucho acerca de quién y el porqué la dejaría en la biblioteca, pero no podía hallar una explicación coherente después todo."


translate spanish day5_library_2_328028d5:


    "Puesto lo severa que es Zhenya, mejor que llame a la puerta."


translate spanish day5_library_2_9b83bcec:


    mz "Está abierta."


translate spanish day5_library_2_ccef914a:


    "Zhenya me observó detenidamente de cerca, tras sus gafas."


translate spanish day5_library_2_6705d775:


    mz "¿Qué quieres?"


translate spanish day5_library_2_e39034b2:


    me "Ehmm...{w} No pienses en nada raro, pero necesito..."


translate spanish day5_library_2_511a0256:


    "No quise parecer un idiota, así que preferí explicarle las cosas con detalle."


translate spanish day5_library_2_c2c36c71:


    me "Necesito algo de harina. Olga Dmitrievna dijo que está aquí. Sé que suena raro que se guarde la harina en una biblioteca, pero..."


translate spanish day5_library_2_16f45806:


    me "Me enviaron a ti. Y se necesita para un pastel, para celebrar el rescate de Shurik."


translate spanish day5_library_2_e3838181:


    mz "Sí, tengo la harina, ¿qué tiene de raro?"


translate spanish day5_library_2_80c6622f:


    "Zhenya respondió sorprendida."


translate spanish day5_library_2_f64f4e8b:


    "En ese momento sentí como si me hubieran golpeado en la cabeza con algo pesado y perdiera la habilidad de comprender algo."


translate spanish day5_library_2_079c3fed:


    th "Harina en la biblioteca...{w} Claro, qué tiene de extraño..."


translate spanish day5_library_2_e29b49ed:


    th "Estamos en el País de las Maravillas, soy Alicia, ahora me comeré la seta mágica y volveré a casa..."


translate spanish day5_library_2_fe1f361c:


    mz "¡Ey!"


translate spanish day5_library_2_ca8c1883:


    me "¿Eh? ¡Sí!"


translate spanish day5_library_2_5c270e5d:


    "Estaba soñando despierto."


translate spanish day5_library_2_e683043b:


    mz "Espera aquí, vuelvo enseguida."


translate spanish day5_library_2_660215b2:


    "Desapareció tras las estanterías, mientras tanto me crucé de brazos y empecé a esperar."


translate spanish day5_library_2_c8ef07cc:


    "Un instante después el ruido de una trampilla crujiendo por sus bisagras me llamó la atención."


translate spanish day5_library_2_21111af0:


    me "Ey, ¿necesitas algo de ayuda?"


translate spanish day5_library_2_91d35e95:


    "Pregunté en voz alta."


translate spanish day5_library_2_644e33dc:


    mz "¡Ya puedo sola!"


translate spanish day5_library_2_47919150:


    "Zhenya me ladró."


translate spanish day5_library_2_dcc13160:


    "Parece estar en los sótanos, así que la esperaré un poco."


translate spanish day5_library_2_d08df322:


    me "Vale, vale..."


translate spanish day5_library_2_0a82bf01:


    "Pocos minutos habían transcurrido, pero Zhenya todavía no había vuelto."


translate spanish day5_library_2_83502309:


    "Me empecé a preocupar cuando, de repente, la puerta fue abierta de golpe y Alisa entró en la biblioteca."


translate spanish day5_library_2_c46172f4:


    "Ella también parecía sorprendida al verme aquí."


translate spanish day5_library_2_cdb9fe3e:


    dv "¿Qué estás haciendo {i}tú{/i} aquí?"


translate spanish day5_library_2_3df6714e:


    me "¿No me está permitido estar aquí?"


translate spanish day5_library_2_30a65a09:


    "Le dije de forma brusca."


translate spanish day5_library_2_7ebd37d9:


    "Alisa estaba claramente un poco abrumada."


translate spanish day5_library_2_f1713752:


    dv "Ah, ¿para qué me molesto...?"


translate spanish day5_library_2_866ddbdc:


    "Resopló y se dirigió a la mesa de Zhenya."


translate spanish day5_library_2_de1f890a:


    me "¿Y por qué estás tú aquí pues?"


translate spanish day5_library_2_6184e4df:


    "Alisa me escrutaba con su mirada cautelosamente y casi abre la boca para decir algo, pero entonces parece que cambió de opinión y se dio la vuelta, escondiendo sus manos tras su espalda."


translate spanish day5_library_2_fe41f14c:


    me "¿Devolviendo un libro?"


translate spanish day5_library_2_91b7e642:


    "Solté la primera cosa que se me ocurrió por la cabeza."


translate spanish day5_library_2_2d3c80f4:


    dv "No es asunto tuyo..."


translate spanish day5_library_2_eb262731:


    "Respondió con indicios de duda."


translate spanish day5_library_2_1c67eb95:


    me "¿Qué libro es?"


translate spanish day5_library_2_991d9a14:


    "Alisa se quedó en silencio."


translate spanish day5_library_2_d8028619:


    me "Oh, venga ya, ¡déjamelo ver!{w} Me pregunto que lee la Señorita «¡Peligro-Alto-Voltaje!»..."


translate spanish day5_library_2_2d3c80f4_1:


    dv "Que no es asunto tuyo..."


translate spanish day5_library_2_f7c41790:


    "Su voz parecía tener menos confianza."


translate spanish day5_library_2_42f650e4:


    me "Vale, vale, no insistiré más..."


translate spanish day5_library_2_76998bd7:


    "De hecho, estaba bastante interesado en saber qué leía Alisa."


translate spanish day5_library_2_66eefb60:


    "Además, estaba bastante entretenido en verle un libro en sus manos."


translate spanish day5_library_2_a36340e5:


    "La televisión, películas o una computadora, si alguna de ellas estuviera disponible aquí... todas estas cosas parecían más apropiadas como entretenimiento para una muchacha como ella."


translate spanish day5_library_2_30470e07:


    "Pero en su lugar, tenía un libro..."


translate spanish day5_library_2_a1a458f1:


    "Mi curiosidad me pudo. Me lancé en el momento oportuno, cuando Alisa apartó la vista de mi, y le arrebaté el libro."


translate spanish day5_library_2_97a3e89b:


    dv "¡Arrgh!"


translate spanish day5_library_2_c9e0274f:


    "Chilló."


translate spanish day5_library_2_a1b7c998:


    "Al siguiente instante su rostro adquirió tal expresión, que me hizo cuestionar mi última acción."


translate spanish day5_library_2_38429226:


    th "Si voy a morir, al menos sabré de qué."


translate spanish day5_library_2_a0da6e68:


    "Sostenía entre mis manos una copia de «Lo que el viento se llevó»."


translate spanish day5_library_2_8d03e8c1:


    "Ése era el mismo libro que Lena estaba leyendo aquel atardecer en un banco."


translate spanish day5_library_2_29dc2955:


    "Estaba tan asombrado que olvidé completamente mi inminente muerte."


translate spanish day5_library_2_6958082e:


    me "¿Es interesante?"


translate spanish day5_library_2_3b4b97c0:


    dv "Claro..."


translate spanish day5_library_2_eb7dd1a8:


    "Alisa respondió sin entusiasmo, sonrojándose."


translate spanish day5_library_2_621c7bd3:


    me "Vale pues..."


translate spanish day5_library_2_75b79b0a:


    "Le devolví el libro."


translate spanish day5_library_2_df52def3:


    "Alisa lo tiró encima de la mesa y se fue de la biblioteca rápidamente sin mirarme."


translate spanish day5_library_2_c3a92102:


    th "Así que los asuntos humanos no son algo ajeno para ella.{w} A fin de cuentas, ella es también una muchacha."


translate spanish day5_library_2_fe404d31:


    "Tras revisar rápidamente todo lo que había sucedido justo ahora, llegué a la conclusión de que en realidad no tenía nada de raro."


translate spanish day5_library_2_e499eebb:


    "La curiosidad mató al gato."


translate spanish day5_library_2_0dc343a6:


    th "De todas formas, es Alisa... y eso significa que podría acabar siendo un desastre total, más aun tengo que recoger los ingredientes."


translate spanish day5_library_2_ab719773:


    dv "Bueno, ya vendré más tarde..."


translate spanish day5_library_2_24b51b37:


    "Alisa se fue de la biblioteca sin mirarme siquiera."


translate spanish day5_library_2_89141735:


    "Eso me hizo pensar."


translate spanish day5_library_2_72ba180f:


    th "¿De qué podría sentirse tan avergonzada de este libro?"


translate spanish day5_library_2_4521510a:


    th "Una avergonzada Alisa es algo extraordinario por sí mismo...{w} Pero Alisa avergonzada por un libro..."


translate spanish day5_library_2_10c14b59:


    th "Pero qué sentido tiene especular ahora... no hay manera de saberlo ya."


translate spanish day5_library_2_4160881a:


    "Finalmente, la profunda queja de Zhenya sonó, alcanzando cada uno de los rincones de la biblioteca."


translate spanish day5_library_2_bece771e:


    mz "¡Cógela!"


translate spanish day5_library_2_98368a17:


    "Crucé por las estanterías y contemplé la sudurosa bibliotecaria sentada cerca de la trampilla que conducía a los sótanos con un pequeño saco a su lado."


translate spanish day5_library_2_7959d521:


    th "Bueno, me imagino que deben tener algún tipo de almacén ahí abajo..."


translate spanish day5_library_2_553525de:


    me "¡Gracias!"


translate spanish day5_library_2_821c2037:


    "Agarré el saco y abandoné la biblioteca."


translate spanish day5_library_2_089e3478:


    "Gracias a Dios, no era muy pesada, así que cargué con ella hasta la cabaña de Olga Dmitrievna sin mucho esfuerzo."


translate spanish day5_main3_8645b244:


    "Finalmente, parecía que todo había sido recogido."


translate spanish day5_main3_4f706171:


    "Agarré el carrito con azúcar y lo saqué fuera, puse el saco de harina en él, a continuación las dos cestas de fresas de alguna forma entre ambos sacos."


translate spanish day5_main3_ee5f67e3:


    "Y la cerveza iba escondida bajo mi camisa, sólo por si acaso."


translate spanish day5_main3_f702be22:


    "El dia estaba llegando a su fin, así que tenía que darme prisa, en tanto que el pastel requeriría algo de tiempo en cocinarse."


translate spanish day5_main3_1546fb97:


    "Por supuesto, disfrutaba mucho más tumbándome, cerrando los ojos y haciendo una buena cabezadita, pero sencillamente no podía fallarle a Olga Dmitrievna."


translate spanish day5_main3_050e260b:


    "Ciertamente, después de todos los problemas que he dado, me siento personalmente responsable por el éxito de esta fiesta."


translate spanish day5_main3_bfc08830:


    "Al llegar a la plaza, me detuve por un instante para recuperar mi aliento."


translate spanish day5_main3_dcabd394:


    "No es que el carrito pesara... corría bastante suave sin apenas hacer esfuerzo. Simplemente es que cualquier gran esfuerzo físico me molestaba ahora.{w} Tanto física como mentalmente."


translate spanish day5_main3_fc8d210b:


    "Me senté en el banco y cerré mis ojos por un momento."


translate spanish day5_main3_5dad0356:


    dreamgirl "¿Qué es eso?"


translate spanish day5_main3_4999a965:


    "No me importaba quien fuera realmente... probablemente sólo algún tipo de muchacha pionera que tuvo interés por un compañero desconocido que se afligía."


translate spanish day5_main3_cb23542f:


    me "¿De qué estás hablando?"


translate spanish day5_main3_b037ba1a:


    "Le pregunté agotado."


translate spanish day5_main3_d00d3d0f:


    "Ella no me contestó."


translate spanish day5_main3_b9035f5a:


    me "Esos son los ingredientes para un pastel...{w} ¿Te gustan los pasteles?"


translate spanish day5_main3_3d6bef45:


    dreamgirl "No lo sé..."


translate spanish day5_main3_159dd052:


    me "¿Qué? ¿No has probado nunca un pastel?"


translate spanish day5_main3_3d6bef45_1:


    dreamgirl "No lo sé..."


translate spanish day5_main3_9ee429fd:


    "Obviamente, la muchacha no entendía de qué le estaba hablando, pero no me sorprendió en ese instante."


translate spanish day5_main3_7285209b:


    "No estaba en verdad interesado en la conversación. Estaba tan cansado, que tenía intención cero de clasificar distracciones externas, y de etiquetarlas ya fueran comunes o no."


translate spanish day5_main3_6978c07f:


    me "Ya veo..."


translate spanish day5_main3_0a00a780:


    me "Acércate a la cantina más tarde y probarás un bocado."


translate spanish day5_main3_f1668a0e:


    dreamgirl "¿En serio?"


translate spanish day5_main3_dae36352:


    me "De verdad."


translate spanish day5_main3_5b24c110:


    dreamgirl "¿Y de qué están hechos?"


translate spanish day5_main3_2a985733:


    me "¿Qué?"


translate spanish day5_main3_fad6f3d4:


    "Pregunté indiferentemente."


translate spanish day5_main3_52a831f5:


    dreamgirl "Estos...{w} ¡Pasteles!"


translate spanish day5_main3_46d149a2:


    me "Bueno...{w} Algo de harina, algo de azúcar, diversos rellenos y decoraciones..."


translate spanish day5_main3_61fefa63:


    th "Eso sí era una pregunta extraña, ¿no sabe acaso de qué están hechos los pasteles?"


translate spanish day5_main3_0fa8d81f:


    dreamgirl "¿Y los tienes todos aquí?"


translate spanish day5_main3_c8b42142:


    me "Claro, algunos."


translate spanish day5_main3_749fd519:


    dreamgirl "¿Y azúcar?"


translate spanish day5_main3_e2a3a4fa:


    me "Y azúcar..."


translate spanish day5_main3_b206627b:


    dreamgirl "¿Me darías un poquito?"


translate spanish day5_main3_7332342b:


    me "¿Para qué?"


translate spanish day5_main3_9d2d461d:


    "Pensé que eso ya era demasiado."


translate spanish day5_main3_15d24d3a:


    "Una repentina ráfaga de viento me hizo agarrar instintivamente el carrito y abrí los ojos."


translate spanish day5_main3_0cf7617c:


    "Sin embargo, no había nadie allí."


translate spanish day5_main3_195802d1:


    th "¿Estaba soñando?"


translate spanish day5_main3_0ee6c06e:


    "Me di cuenta, a pesar de todo, de que el saco de azúcar estaba desatado y un pequeño montón se había desbordado."


translate spanish day5_main3_2fb04556:


    th "¿Podría ser que se asustara por el viento y saliera corriendo...?"


translate spanish day5_main3_5e73ac52:


    "Habiendo atado el saco, me levanté del banco y continué con mi reto de llevar por el camino las fresas y demás."


translate spanish day5_main3_d3d42b9c:


    "No había ni una sola persona cerca de la cantina.{w} No me extraña... todavía faltaba una hora para la cena."


translate spanish day5_main3_1b6a6dde:


    "Llevé el carrito hacia la salida de atrás y le di los ingredientes a la cocinera del campamento."


translate spanish day5_main3_17448fb7:


    "Debieron de haberle dicho ya qué hacer con ellos si ya me estaba echando semejante mirada de disgusto.{w} No estoy seguro de cuánto tiempo lleva cocinar un pastel, pero parecía que ella tendría que darse prisa."


translate spanish day5_main3_088c8385:


    "Solamente quería relajarme durante el resto de tiempo que me quedara hasta la cena."


translate spanish day5_main3_294475af:


    "En resumen, estaba tan cansando que me senté en las escaleras y esperé."


translate spanish day5_main3_89ce1a6f:


    "Mis ojos se cerraron solos... Supongo que estaba tan agotado tras todo el día, que no me di cuenta de que alguien se me acercó hasta que me dio una palmadita en el hombro."


translate spanish day5_main3_a0a72914:


    mi "¡Hola!"


translate spanish day5_main3_7629afc5:


    "Miku estaba frente a mi."


translate spanish day5_main3_b5cc7e1c:


    me "Sehh..."


translate spanish day5_main3_6aa1252d:


    "No necesitaba un espejo para imaginarme la expresión de escepticismo y enojo en mi rostro."


translate spanish day5_main3_11ad29bb:


    mi "Oh, discúlpame, debo haberte interrumpido..."


translate spanish day5_main3_df78914f:


    me "No pasa nada, solamente estaba sentado aquí."


translate spanish day5_main3_618d7741:


    mi "Ah, ¡muy bien pues!"


translate spanish day5_main3_1af920ee:


    "Miku me hizo una sonrisa."


translate spanish day5_main3_3f7156f6:


    mi "Solamente llegué para cenar. Pensé que sería la hora ya, luego me pareció que era demasiado pronto, pero decidí comprobarlo por si acaso... tal vez no soy yo la que se equivocó, ¡sino el reloj!"


translate spanish day5_main3_bf5ddeb1:


    mi "Bueno, no el reloj, el reloj no se puede equivocar, es sólo que lo malinterpreté..."


translate spanish day5_main3_3c421d62:


    "Resultaba estar totalmente confusa ahora y se quedó en silencio."


translate spanish day5_main3_7fe5f005:


    me "Todavía falta media hora para cenar."


translate spanish day5_main3_8824776e:


    mi "Oh, eso es genial, pues entonces me sentaré aquí y esperaré contigo, ¿te importa?"


translate spanish day5_main3_203abf9f:


    "A decir verdad, sí me importa."


translate spanish day5_main3_05c9aec0:


    me "Sabes, tengo asuntos de los que ocuparme..."


translate spanish day5_main3_e44244b3:


    "Me levanté rápidamente y me fui sin decirle adiós, ignorando a Miku como siempre había hecho, mientras ella me chillaba detrás de mí alguna cosa."


translate spanish day5_main3_b094f8aa:


    "Un minuto después, llegué a la plaza y me senté en el banco con la firme intención de hallar un lugar tranquilo y silencioso donde poder esperar hasta la cena."


translate spanish day5_main3_da5773ae:


    "Creo que esta es la primera vez en los últimos cuatro días y medio en que me he sentido así."


translate spanish day5_main3_de22be40:


    "No estaba solamente irritado debido a algunos insignificantes detalles sino que, ciertamente, estaba realmente enfadado."


translate spanish day5_main3_8b26ac2a:


    "He dejado completamente de preocuparme sobre dónde estoy y por qué estoy aquí."


translate spanish day5_main3_9e4b97f1:


    "Tampoco me he preocupado sobre cómo salir de aquí."


translate spanish day5_main3_07d4e30c:


    "Lo que me molesta es que siempre tengo que estar responsabilizándome de alguna estúpida tarea ordenada por nuestra líder del campamento, y siempre soy yo quien se encuentra en situaciones estúpidas y a veces incluso acabo pareciendo un payaso."


translate spanish day5_main3_bd134020:


    th "Si todo esto es algún tipo de broma extraterrestre o algún plan maestro de una Mente Universal, ¡mejor que consulten con su psiquiatra!"


translate spanish day5_main3_13370091:


    "Apreté los dientes y cerré mis puños."


translate spanish day5_main3_8ea911ef:


    th "¡Y lo más enojante es que todo lo que sucede parece ocurrir por sí mismo de alguna forma!"


translate spanish day5_main3_e2dfefc7:


    th "Sería más feliz si no tuviera que cargar por ahí con sacos de azúcar que pesan una tonelada, ¡pero es que tampoco tenía elección!"


translate spanish day5_main3_12a8d8d9:


    th "Quiero decir, que cualquiera otra opción llevaría a consecuencias peores que a esfuerzos físicos u orgullo herido..."


translate spanish day5_main3_60e71618:


    us "¿Con quién estás enfadado?"


translate spanish day5_main3_4a8fcfb4:


    "Ulyana estaba de pie frente a mi y me sonreía tímidamente."


translate spanish day5_main3_90b46808:


    me "Nadie en realidad..."


translate spanish day5_main3_12a0290a:


    "Respondí ausente."


translate spanish day5_main3_4758c0c0:


    "Pero mis puños decían lo contrario."


translate spanish day5_main3_be723785:


    me "Así tan fácil..."


translate spanish day5_main3_40bd06ac:


    us "Vale, vale, como quieras...{w} Mejor que me contaras, ¿por qué corrías por ahí por el campamento todo el día con esos sacos?"


translate spanish day5_main3_d8e5e6d0:


    me "Porque tenía que hacerlo."


translate spanish day5_main3_d20f4795:


    "Respondí a regañadientes."


translate spanish day5_main3_a0086905:


    us "Supongo que era comida."


translate spanish day5_main3_726459ba:


    me "Quizás lo fuera..."


translate spanish day5_main3_28b2648c:


    "Ulyana iba a decir algo, pero en aquel momento la campana sonó, llamando a los pioneros a cenar."


translate spanish day5_main3_f1fc33c2:


    "Suspiré de alivio y rápidamente me dirigí hacia la cantina, dejando atrás a Ulyana."


translate spanish day5_main3_1d7b62f5:


    mt "¡Semyon, muchas gracias!"


translate spanish day5_main3_9508d41b:


    me "¿Por qué?"


translate spanish day5_main3_92e433aa:


    "La líder del campamento me hizo una sonrisa amistosa."


translate spanish day5_main3_f601ce01:


    mt "¡Por el pastel por supuesto!"


translate spanish day5_main3_707cf821:


    me "Ah, claro..."


translate spanish day5_main3_e512b012:


    "Fue en aquel mismo momento que comprendí el verdadero significado del dicho «Da las gracias para dar de comer al gato»."


translate spanish day5_main3_374f0622:


    mt "¿No se lo dijiste a nadie? ¡Debe ser una sorpresa!"


translate spanish day5_main3_9513cd87:


    me "Claro..."


translate spanish day5_main3_6a31846d:


    mt "¡Ese es mi muchacho!{w} ¡Y ahora ves a cenar!"


translate spanish day5_main3_937d7bb8:


    "Olga Dmitrievna movió su mano, indicando hacia la cantina."


translate spanish day5_main3_5452915f:


    "Me adentré por la entrada lentamente y empecé a buscar un sitio libre."


translate spanish day5_main3_f6bdde48:


    "Resultó que hoy había muchos asientos vacíos, así que tenía la oportunidad de comer solo."


translate spanish day5_main3_2061d557:


    "Había pescado con puré de patatas para cenar."


translate spanish day5_main3_062530c8:


    th "Qué desafortunado otra vez... Me quedaré con hambre.{w} ¿Y no teníamos a mediodía antes para comer pescado también...?"


translate spanish day5_main3_0e40ce73:


    th "¡¿Es que es el día de comer sólo pescado?!"


translate spanish day5_main3_ce79b819:


    "Habiendo apartado mi plato con el morador del mar frito, apoyé mi cabeza entre mis manos y cerré los ojos."


translate spanish day5_main3_d13c0f93:


    "Pero pronto alguien vino a la mesa."


translate spanish day5_main3_44b645c0:


    sl "Ey, ¿te encuentras bien?"


translate spanish day5_main3_84662d7a:


    me "Estoy bien..."


translate spanish day5_main3_35836741:


    "Contesté sin cambiar de posición."


translate spanish day5_main3_cb89aafa:


    sl "¿Cansado?"


translate spanish day5_main3_bf1c2987:


    me "Claro, un poco..."


translate spanish day5_main3_17c764f2:


    sl "Qué mal."


translate spanish day5_main3_81e2449f:


    "Slavya lo dijo seriosamente."


translate spanish day5_main3_17c6b21c:


    me "Por supuesto..."


translate spanish day5_main3_4fb291b0:


    sl "Te acuerdas de que nos iremos de excursión tras la cena, ¿no? ¿Lo has preparado todo?"


translate spanish day5_main3_25632213:


    me "¿Qué? ¿Dónde?"


translate spanish day5_main3_9830e8f7:


    "Abrí mis ojos y alcé mi cabeza al instante."


translate spanish day5_main3_bf312592:


    "Lena estaba al lado de Slavya."


translate spanish day5_main3_cd82f264:


    sl "La excursión..."


translate spanish day5_main3_a4614188:


    "Estaba sorprendida."


translate spanish day5_main3_1acd2585:


    sl "¿No lo sabías?"


translate spanish day5_main3_5cb822ee:


    me "No..."


translate spanish day5_main3_63edcced:


    "Agaché mi cabeza sobre la mesa y me cubrí con las manos."


translate spanish day5_main3_72338241:


    th "Si sólo me pudiera tragar la tierra justo ahora..."


translate spanish day5_main3_34d8cd0a:


    "Las muchachas permanecían en silencio."


translate spanish day5_main3_cfa6c6ec:


    "Me quedé solo con mis pensamientos por un rato, y eso me estaba bien."


translate spanish day5_main3_cdac6a32:


    "Quizá podría quedarme sentado así hasta el final de la hora de cenar, pero la fuerte voz de Olga Dmitrievna se escuchó desde el otro lado de la cantina."


translate spanish day5_main3_0b4ca722:


    mt "¡Muchachos!{w} Para celebrar el milagroso rescate de nuestro amigo y camarada Shurik, ¡hemos preparado este pastel para todos!"


translate spanish day5_main3_639a506c:


    "Alcé mi cabeza perezosamente y miré en dirección a la líder del campamento, pero no podía ver nada más allá de las espaldas de los pioneros."


translate spanish day5_main3_c6fcef15:


    mt "Un segundo... Sólo un segundo..."


translate spanish day5_main3_a783f6df:


    th "Y nada de mí.{w} Nada sobre mí que rescaté a Shurik o reuniendo los ingredientes para el pastel..."


translate spanish day5_main3_6400b884:


    th "Como si así debiera ser."


translate spanish day5_main3_df46be92:


    th "Bueno, hubiera sido un error esperar algo de nuestra líder del campamento."


translate spanish day5_main3_6ab86cd0:


    sl "¡Vamos!{w} ¡O nos quedaremos sin nuestra parte!"


translate spanish day5_main3_c17a722d:


    "Slavya sonrió."


translate spanish day5_main3_a8b5b91a:


    un "Vamos."


translate spanish day5_main3_c1956a30:


    "Lena estaba de acuerdo."


translate spanish day5_main3_3b03af3a:


    me "Sí, claro..."


translate spanish day5_main3_a2f9d7eb:


    "Me levanté a regañadientes y me uní a las muchachas detrás de ellas."


translate spanish day5_main3_f95eb2e4:


    "En cuanto nos aproximamos a la multitud de pioneros, Olga Dmitrievna simplemente dejó el pastel en medio de la mesa."


translate spanish day5_main3_9e9a688f:


    mt "Y ahora..."


translate spanish day5_main3_902ab03d:


    "La líder del campamento no pudo terminar ya que Ulyana se lanzó rápidamente en picado desde la muchedumbre de pioneros sobre el pastel."


translate spanish day5_main3_44ef2910:


    "Se las apañó para mordisquearlo un par de veces antes de ser apartada."


translate spanish day5_main3_3ec376bc:


    "Estaba pataleando y chillando."


translate spanish day5_main3_72ace9f9:


    "Me quedé mirando perplejamente desde fuera de todo este drama: Alisa sonriendo; Lena cogiendo algo de crema con su dedo; todos los pioneros furiosos alrededor."


translate spanish day5_main3_7f98185e:


    "Me sentí completamente fuera de lugar aquí. Pensé que si cerraba los ojos ahora y los abría otra vez... he llegado, de vuelta a la seguridad de mi piso delante de una computadora."


translate spanish day5_main3_f569f26d:


    "Pestañeé pero nada cambió; sólo el ruido y la confusión se volvieron más agudos."


translate spanish day5_main3_87959490:


    mt "¡Ulyana! ¡Te has excedido!"


translate spanish day5_main3_34125318:


    us "Yo...{w} Yo sólo..."


translate spanish day5_main3_6429cb93:


    "Bueno, de hecho comportarse así es incluso demasiado para ella."


translate spanish day5_main3_d677809e:


    "Shurik irrumpió en la conversación... ¿o era un tribunal de guerra?"


translate spanish day5_main3_58d5ebe0:


    sh "¡Por favor, Olga Dmitrievna! Puesto que el pastel es para celebrar mi retorno, tampoco es tan grave..."


translate spanish day5_main3_a6820f71:


    "Dudó."


translate spanish day5_main3_57d1ba21:


    mt "¡No importa!"


translate spanish day5_main3_aac2b1f6:


    "La líder del campamento se giró hacia Ulyana."


translate spanish day5_main3_33bbaf19:


    mt "Y tú...{w} Hoy te voy a castigar de verdad, ¡así te comportarás la siguiente vez!"


translate spanish day5_main3_935bc568:


    us "¡Bah! ¡Da igual!"


translate spanish day5_main3_99aa653e:


    "Resopló y se dio la vuelta."


translate spanish day5_main3_cd01cddf:


    mt "¡No vendrás en la excursión con nosotros esta noche!"


translate spanish day5_main3_2babf74f:


    us "¡Como si quisiera!"


translate spanish day5_main3_54e5777a:


    "Tenía más que deseos de cambiarme de lugar con Ulyana y así evitar ir a la excursión, pero quién sabe..."


translate spanish day5_main3_038c58cb:


    "Si lo hubiera adivinado de antemano, ¡habría sido el primero en lanzarme como un enloquecido y destrozar el maldito pastel!"


translate spanish day5_main3_a4bcbbe9:


    "Tras un par de minutos de confusión, los pioneros empezaron a dispersarse."


translate spanish day5_main3_c997a5fb:


    mt "¡Tú tienes que prepararte también!{w} Habrá una formación en la plaza en media hora."


translate spanish day5_main3_80aff84c:


    "Miré directamente a los ojos de la líder del campamento, tratando de expresar mi actitud de forma no verbal, pero parece que fracasé."


translate spanish day5_main3_a89a2c76:


    mt "¡No llegues tarde!"


translate spanish day5_main3_fd82295b:


    "Ulyana estaba sentada junto a la mesa cuando me acerqué a ella en mi camino a la salida."


translate spanish day5_main3_94be5d8c:


    me "¿Por qué lo hiciste?"


translate spanish day5_main3_4d3873eb:


    "Se le veía muy enfadada.{w} Pero tenía derecho a estarlo."


translate spanish day5_main3_500104f1:


    us "Quería."


translate spanish day5_main3_e8371c45:


    "Ulyana contestó súbitamente."


translate spanish day5_main3_017bb18f:


    me "Entonces, ¿estás contenta ahora?"


translate spanish day5_main3_2ddb5b95:


    us "¡Claro que lo estoy!{w} Y tú... ¡buena suerte con la excursión!"


translate spanish day5_main3_72d5c9fe:


    "Sonrió maliciosamente, se puso de pie de un salto y salió corriendo de la cantina."


translate spanish day5_main3_b53a9fd1:


    th "Bueno, un poquito de suerte no hará daño..."


translate spanish day5_main3_a9aa1a31:


    "«Quien la sigue, la consigue»... era exactamente este proverbio el que estaba dándome vueltas por mi cabeza durante todo el camino hasta la cabaña de la líder del campamento."


translate spanish day5_main3_831c71df:


    "De alguna forma, no podía arreglármelas para discutir, pretender que estoy enfermo o simplemente saltármela sin motivo aparente."


translate spanish day5_main3_f85ee780:


    "Los hechos de este día me enseñaron obediencia.{w} Aunque a veces ocurriera lo que ocurriera o no tuviera sentido para mi."


translate spanish day5_main3_4da475d5:


    "Mientras caminaba, tuve una idea."


translate spanish day5_main3_96c41623:


    th "Bueno, de hecho, ¿cómo debería estar preparado?"


translate spanish day5_main3_9679533e:


    th "¿Ropas? Solo tengo un abrigo y un par de tejanos.{w} De todas formas, olvidé preguntar si iba a ser una excursión durante la noche o no."


translate spanish day5_main3_fd2a6187:


    "No podía pensar en nada mejor, así que agarré el jersey que tenía conmigo cuando llegué al campamento (por la noche puede que haga frío), y arrastré mis pasos lentamente hasta la plaza."


translate spanish day5_main3_ca17b192:


    "Todo el campamento ya estaba allí, aunque faltaban diez minutos antes de la hora que Olga Dmitrievna había designado."


translate spanish day5_main3_1e295624:


    "Me incorporé casi al borde y esperé pacientemente."


translate spanish day5_main3_fd3eb8d2:


    "La noche está cayendo."


translate spanish day5_main3_d20ed5d2:


    th "Si nos viéramos desde fuera, qué imagen más cómica seríamos: una muchedumbre de pioneros, acostumbrados a formar perfectamente, como si esperaran una orden silenciosa del Genda de bronce."


translate spanish day5_main3_018d282a:


    th "Y todo esto sucediendo en los rayos escarlatas del ocaso."


translate spanish day5_main3_b7cd9dd7:


    "Y ahí está él, moviendo su mano y gritando «atacad», como soldados que rugen con sus pañuelos rojos cargando en la batalla contra algún enemigo imaginario..."


translate spanish day5_main3_18d7dd2e:


    "Pero Olga Dmitrievna se mostró y comenzó a hablar en vez de Genda."


translate spanish day5_main3_553eaa56:


    mt "Parece que todo el mundo está aquí...{w} ¡Genial!"


translate spanish day5_main3_a8c428b5:


    "Estaba tan cansado hoy que no podía ni pensar en algo, por lo que sencillamente acabé por escuchar a la líder del campamento."


translate spanish day5_main3_443f99aa:


    mt "¡Ahora hoy, iremos de excursión!"


translate spanish day5_main3_747aae61:


    mt "¡Es esencial para cada pionero estar preparado para la acción y rescate de sus camaradas, a ofrecer su ayuda en la hora necesitada, salvándolos de una situación desesperada!"


translate spanish day5_main3_fc6094ea:


    mt "¡Tenemos que aprender a hacer todas estas cosas juntos!"


translate spanish day5_main3_fc0d9c17:


    "Un rumor corrió a través de toda la multitud de pioneros sugiriendo que lo más probable es que esta expedición verdaderamente épica, acabaría haciendo tareas de limpieza en el bosque, a varios cientos de pasos más allá de la plaza."


translate spanish day5_main3_038d0ddf:


    "De alguna forma también lo creía yo."


translate spanish day5_main3_81d86d37:


    mt "Caminaremos en parejas.{w} Así que, si no habéis escogido un compañero vosotros mismos aun, ¡ahora es buen momento para hacerlo!"


translate spanish day5_main3_30d15b67:


    "Los pioneros rápidamente se hicieron a la idea y comenzaron a emparejarse."


translate spanish day5_main3_ae8616af:


    "Parecía que era el único que no tenía compañero."


translate spanish day5_main3_a9a03604:


    "Slavya estaba entusiasmada hablando algo con Olga Dmitrievna, Lena estaba con Miku, Electronik estaba, obviamente, con Shurik."


translate spanish day5_main3_aea0ad16:


    th "Bueno, puede que no sea mala idea ir solo después de todo."


translate spanish day5_main3_105fc865:


    mt "¡Semyon!"


translate spanish day5_main3_4e47d15f:


    "La voz de la líder del campamento me sacó de mis pensamientos."


translate spanish day5_main3_92f6d31e:


    "Fui hacia ella a regañadientes."


translate spanish day5_main3_8dfde425:


    mt "Veo que no has encontrado un compañero."


translate spanish day5_main3_d3242212:


    me "Eso parece..."


translate spanish day5_main3_d1a7c5c7:


    mt "Pues irás con Zhenya... ella también está sola."


translate spanish day5_main3_27ad27f5:


    "Fui golpeado por ese tipo especial de desesperación, aquella que solamente experimentan los verdaderos solitarios."


translate spanish day5_main3_b7994624:


    "Entonces, parece que estoy solo con la quisquillosa bibliotecaria, con la que no me arriesgaría ni a pasar un par de horas incluso aunque me pagaran por ello..."


translate spanish day5_main3_b73326a6:


    th "Aunque ahora, ambos parecíamos estar en el mismo barco..."


translate spanish day5_main3_d17a396e:


    "Me aproximé lentamente a Zhenya."


translate spanish day5_main3_6818c128:


    me "Bueno, supongo que iremos juntos..."


translate spanish day5_main3_c25dd9ca:


    "Ella me miró."


translate spanish day5_main3_0af22cb1:


    mz "¡No te creas que estoy contenta!"


translate spanish day5_main3_44ba86f4:


    "Dijo Zhenya seriosamente."


translate spanish day5_main3_5122493d:


    me "¿Por qué diantres te irías a sentir contenta?"


translate spanish day5_main3_d7b5efd3:


    "Pregunté ingenuamente."


translate spanish day5_main3_d0a8dd78:


    mz "¡Olvídalo!{w} Será todo mucho mejor si solamente te callas."


translate spanish day5_main3_f4dcd652:


    th "Eh, no podía ir mejor que esto..."


translate spanish day5_main3_b28b669b:


    "Me dio la espalda y siguió a los otros pioneros."


translate spanish day5_main3_e32f998e:


    "Todavía no vi una razón especial por la que caminar en parejas."


translate spanish day5_main3_34fb045a:


    "De todas maneras, caminábamos por los ya trillados senderos del bosque, así que sería bastante difícil perderse incluso si quisieras hacerlo."


translate spanish day5_main3_d1f6b652:


    "Además, cuando ya habíamos estado de excursión durante media hora, no nos adentrábamos en las profundidas del bosque, tratando de hacerle frente a todos los peligros que pudieran probar nuestra valentía y fortalecer nuestros espíritus de pioneros, sino que en su lugar, sólo caminábamos en círculos."


translate spanish day5_main3_946c8923:


    "Sin embargo, si tenemos en cuenta que Olga Dmitrievna era nuestra jefa, esta excursión podría ser comparada con la marcha del hobbit desde La Comarca hasta Mordor..."


translate spanish day5_main3_97574c1e:


    "Tal como Zhenya insistió, la estaba siguiendo a distancia en silencio."


translate spanish day5_main3_41f63256:


    "A la bibliotecaria le parecía estar perfectamente bien de esta forma."


translate spanish day5_main3_6afab6eb:


    me "Ey, ¿sabes cuándo llegaremos a nuestro destino?"


translate spanish day5_main3_67e23fdb:


    mz "¿Nuestro qué?"


translate spanish day5_main3_88370e84:


    me "¡Ejem! El lugar donde nos asentaremos y montaremos el campamento."


translate spanish day5_main3_059aa439:


    mz "¡El sentido de esta excursión no es instalar un campamento, sino la excursión en sí misma!{w} ¡No lo entendiste!"


translate spanish day5_main3_97b64a58:


    "Claro, parece que no tengo ni idea de lo que son excursiones..."


translate spanish day5_main3_4587e3c3:


    me "Supongo que tienes razón, pero aun así..."


translate spanish day5_main3_bb86bd36:


    mz "¡Que no lo sé!"


translate spanish day5_main3_75f80dfe:


    "Contestó ásperamente y aceleró el paso."


translate spanish day5_main3_133cde0a:


    "La atrapé y le pregunté:"


translate spanish day5_main3_e4b066d7:


    me "Escucha, por qué siempre eres tan..."


translate spanish day5_main3_17313088:


    "Estaba a punto de decir «mala», pero me callé a medias."


translate spanish day5_main3_6687b26f:


    me "No te he hecho nada de malo, ¡y tampoco lo voy a hacer!"


translate spanish day5_main3_efd76d15:


    "Me observó sorprendida."


translate spanish day5_main3_296b44b5:


    mz "Siempre tan... ¿qué?"


translate spanish day5_main3_231d5bf5:


    me "Bueno, insociable... Algo así...{w} ¿O acaso es algo sobre mí?"


translate spanish day5_main3_da119946:


    mz "¡Oh, venga ya, deja ya esa estupidez!"


translate spanish day5_main3_8b13fe9b:


    me "Como desees..."


translate spanish day5_main3_39a4936f:


    "Decidí no iniciar una conversación con ella durante el resto de la excursión."


translate spanish day5_main3_a20cefa7:


    "..."


translate spanish day5_main3_66653ce7:


    "Al menos Olga Dmitrievna decidió terminar con este arduo trabajo propio de Sísifo."


translate spanish day5_main3_f2e5e7d4:


    mt "Es la hora parar."


translate spanish day5_main3_e9d2363e:


    "El lugar escogido resultó ser un claro bastante grande, con unos cuantos árboles en semicírculo haciendo una improvisada glorieta, con los restos de una fogata en el medio."


translate spanish day5_main3_d3f88135:


    "Obviamente tales excursiones son una tradición de esta campamento."


translate spanish day5_main3_e664fb77:


    "Fui enviado a buscar leña junto a los otros muchachos."


translate spanish day5_main3_5463f5b6:


    "No nos llevó mucho tiempo ya que habían muchas brancas y troncos de diverso tamaño por los alrededores."


translate spanish day5_main3_dcb20eaf:


    "COn el tiempo, Olga Dmitrievna encendió fuego usando algún tipo de viejo periódico."


translate spanish day5_main3_df660aed:


    "Estaba ansioso por saber qué había escrito en él, pero no podía discernir nada salvo símbolos soviéticos."


translate spanish day5_main3_44bc05c0:


    "Los pioneros tomaron su lugar en los troncos caídos como bancos y comenzaron a hablar de cosas."


translate spanish day5_main3_0b4fac65:


    "Parece que el objetivo final de esta actividad había sido logrado."


translate spanish day5_main3_c751793a:


    th "La únicas cosas que faltaban eran una olla de sopa de pescado, tazas de aluminio con vodka y una guitarra."


translate spanish day5_main3_a7859a66:


    "Pero no me sorprendería si todas estas cosas aparecieran de alguna forma."


translate spanish day5_main3_29dcd13b:


    sl "¿Qué estás pensando?"


translate spanish day5_main3_a6893681:


    "Slavya se sentó a mi lado."


translate spanish day5_main3_60a79f40:


    me "Oh, nada en especial...{w} Sólo disfrutando de la excursión."


translate spanish day5_main3_27c62366:


    "Respondí sarcásticamente."


translate spanish day5_main3_5e83eb9a:


    sl "No pareces muy contento."


translate spanish day5_main3_f8cc5c73:


    me "Bueno, no voy a saltar de alegría, lo siento."


translate spanish day5_main3_8f1735d3:


    sl "Vale, no te molestaré."


translate spanish day5_main3_70009934:


    "Estuvo sentada conmigo un rato, pero tras darse cuenta que no estaba con humor para conversar, me dejó solo para disfrutar de mi introspección."


translate spanish day5_main3_81fad12b:


    "Y todo lo que deseaba era tumbarme en la cama y caer dormido tan pronto como fuera posible, pero en vez de ello estaba rodeado por humo y cháchara inútil de pioneros a mi alrededor."


translate spanish day5_main3_9aaac94d:


    "Estaban entusiasmados y reían, en general disfrutando de este cálido atardecer de verano."


translate spanish day5_main3_2803f4ab:


    "En un lado alejado del claro, me percaté que Lena estaba discutiendo con Alisa intensamente."


translate spanish day5_main3_d93c3f94:


    "«Intensamente» y Lena, parecían algo completamente contrarios para mi."


translate spanish day5_main3_193e4c57:


    "Slavya se había ido a alguna parte tras nuestra conversación."


translate spanish day5_main3_37441d60:


    "Electronik y Shurik estaban tratando de demostrar frenéticamente algo a Olga Dmitrievna."


translate spanish day5_main3_3257d53a:


    "Parece que soy el único que no pertenece a este lugar."


translate spanish day5_main3_a4f8dc02:


    th "Pero por otra parte, ¿por qué debería preocuparme?"


translate spanish day5_main3_cb94daa6:


    "Todo lo que hacía era simplemente contemplar el fuego."


translate spanish day5_main3_c8d1a036:


    th "Hay un dicho que dice que uno podría estar contemplándolo siempre, así como la corriente del agua."


translate spanish day5_main3_68d47823:


    "Pero también había una tercera cosa en todo esto..."


translate spanish day5_main3_1d9e5974:


    th "¿Qué era eso?"


translate spanish day5_main3_29414842:


    mt "Uno podría contemplar tres cosas siempre: ver arder el fuego, ver la corriente del agua y... ¡cómo son las otras personas!"


translate spanish day5_main3_903e62de:


    "La líder del campamento me sacó de mi ensimismamiento."


translate spanish day5_main3_31151d59:


    mt "Semyon, ¿no crees que es muy pronto para relajarse?"


translate spanish day5_main3_0a50a8a2:


    me "¿Pero qué tendría que hacer si no?"


translate spanish day5_main3_435b54b7:


    "Sinceramente, no podía entender qué era lo que quería Olga Dmitrievna de mi."


translate spanish day5_main3_2c3aefa4:


    mt "No sé..."


translate spanish day5_main3_2ebab91c:


    "Se calló por un momento."


translate spanish day5_main3_3e0f4283:


    mt "Pero si hay algo que debas hacer, pues hazlo sin ningún género de dudas."


translate spanish day5_main3_fa103e1d:


    "Me sonrió ambigüamente y se fue de nuevo al fuego a tirar unas cuantas brancas."


translate spanish day5_main3_8045c9cb:


    "Tras esas palabras, estoy completamente seguro que ella me trata como un esclavo personal."


translate spanish day5_main3_5d460c3e:


    "O al menos como si fuera mano de obra gratuita, la cual es, estrictamente hablando, la misma cosa."


translate spanish day5_main3_5618085a:


    "Suspiré y agaché mi cabeza en mis manos, esperando que mis padecimientos terminarían hoy..."


translate spanish day5_main3_a582c672:


    "Alguien me dio una palmada en el hombro."


translate spanish day5_main3_008bd100:


    "Levanté la mirada y vi a Shurik y a Electronik, quienes estaban sentados a mi lado."


translate spanish day5_main3_132aeab6:


    me "¿Qué queréis?"


translate spanish day5_main3_b037ba1a_1:


    "Pregunté cansado."


translate spanish day5_main3_a3f9f456:


    el "¡No estés triste!"


translate spanish day5_main3_8afd5934:


    me "¿Hay algo mejor que hacer?"


translate spanish day5_main3_a2ade9c9:


    sh "Mira, hemos estado hablando sobre las posibilidades para el avance del club de cibernética con Olga Dmitrievna..."


translate spanish day5_main3_13f2497e:


    sh "Y hay un problema... necesitamos más muchachos.{w} Si pudieras..."


translate spanish day5_main3_a6820f71_1:


    "Él dudó."


translate spanish day5_main3_ac13c40b:


    th "El «avance» y estos muchachos son incompatibles el uno con el otro."


translate spanish day5_main3_b77b4d76:


    "No dije nada y en vez de ello, empecé a mirar a los pioneros de mi alrededor."


translate spanish day5_main3_eb10acd3:


    sh "¿Y bien?"


translate spanish day5_main3_b9a3cb6a:


    me "No tengo tiempo...{w} ¿No podéis ver que siempre estoy ocupado con los recados de la líder del campamento?"


translate spanish day5_main3_74081b6b:


    sh "Claro, supongo que tienes razón...{w} Hoy fue una situación bochornosa como fue todo con Ulyana."


translate spanish day5_main3_a3bf9ccf:


    "Le miré con sorpresa."


translate spanish day5_main3_3e7b5ad7:


    th "Es como si Shurik se culpara a sí mismo por el accidente del pastel."


translate spanish day5_main3_8043b470:


    me "Claro, lo es..."


translate spanish day5_main3_5bb39a9d:


    "Todos los pioneros parecían estar aquí salvo Slavya que no podía localizarla en ninguna parte."


translate spanish day5_main3_35484a41:


    sh "Creo que está enfadada conmigo..."


translate spanish day5_main3_ad02f53a:


    me "¿Quién?"


translate spanish day5_main3_fad6f3d4_1:


    "Pregunté ausente."


translate spanish day5_main3_948b648b:


    sh "Ulyana.{w} ¿Tal vez debería disculparme?"


translate spanish day5_main3_7fe19203:


    me "No, no es tu culpa..."


translate spanish day5_main3_53bddd68:


    "Estuvimos sentados en silencio por un rato, luego me levanté y dije:"


translate spanish day5_main3_37f21c35:


    me "Mis piernas se han entumecido, mejor me doy un paseo."


translate spanish day5_main3_d7826409:


    "No dieron ninguna contestación."


translate spanish day5_main3_d9cf7a94:


    "Di unas cuantas vueltas en círculo por nuestro improvisado campamento, percatándome de la cercana mirada de la líder del campamento observándome."


translate spanish day5_main3_3b6642f3:


    "Era como si Olga Dmitrievna no pudiera esperarse ni un segundo para acercarse y darme una nueva tarea."


translate spanish day5_main3_0724ca3e:


    "No hallé a Slavya en ninguna parte."


translate spanish day5_main3_a3f3fccb:


    th "¿Quizá debería intentar ir a buscarla?"


translate spanish day5_main3_84b23eff:


    "Por otra parte, me sentía mal por Ulyana cada vez que recordaba su rostro enfadado."


translate spanish day5_main3_d76fc57e:


    th "Quizá esta excursión no es la cosa más entretenida, pero estar sentado todo el rato solo no era mejor tampoco."


translate spanish day5_main3_016c7fc0:


    "Pero al mismo tiempo no quería ir a ninguna parte..."


translate spanish day5_main3_3e703e80:


    "Estaba completamente seguro de que Slavya estaría bien, por lo que decidí quedarme."


translate spanish day5_main3_d37bbb9f:


    th "Pero por otra parte, ¡por qué tenía que preocuparme de esa bruja!"


translate spanish day5_main3_f6cc82fd:


    th "Supongo que por hoy ya es suficiente para mí."


translate spanish day5_main3_c58cdcde:


    "Me senté en mi anterior lugar y esperé a que acabara la excursión pacientemente, casi sintiendo físicamente las miradas de la líder del campamento como me apuntaban."


translate spanish day5_main3_5b5fdf66:


    "Al fin se levantó y declaró:"


translate spanish day5_main3_85ade4d1:


    mt "¡Y ahora juguemos a Ciudades!"


translate spanish day5_main3_79d3e6a7:


    "No tenía nada en contra del juego."


translate spanish day5_main3_9432dd18:


    th "Pero era obvio que la excursión se iba a alargar a causa de ello..."


translate spanish day5_main3_6f4a9363:


    "Los pioneros se sentaron alrededor del fuego."


translate spanish day5_main3_1dff3b67:


    "Me fijé en Lena y Alisa, las cuales tomaron asiento en un tronco opuesto a mi."


translate spanish day5_main3_f58cf444:


    th "Parece que todo está bien."


translate spanish day5_main3_7db4f674:


    "Y hace tan sólo unos minutos antes, pensé que era justo todo lo contrario mientras miraba su riña."


translate spanish day5_main3_9f3fc574:


    th "Pero cualquier cosa es posible..."


translate spanish day5_main3_74340d5d:


    "Realmente quería saber sobre qué estaban hablando, pero era imposible ahora, y podía sentir como la fatiga crecía en mi más y más."


translate spanish day5_main3_6615be77:


    "Mi mente estaba completamente en blanco."


translate spanish day5_main3_99ab0d84:


    "Para ser más exactos, mi cabeza estaba tan espesa que no había lugar en ella para desarrollar ideas."


translate spanish day5_main3_e447cec9:


    "Cuando en mis mejores tiempos mi cerebro parecía ser una gran autopista con millones de pensamientos circulando por ahí, persiguiéndose unos a otros y causando accidentes graves, ahora era más bien un sendero perdido en el bosque, el cual es raramente usado y solamente en casos excepcionales."


translate spanish day5_main3_3b240839:


    "Slavya no retornó."


translate spanish day5_main3_b00a6ffe:


    th "¿Quizá tuviera algo que hacer?"


translate spanish day5_main3_5147a728:


    "Pero, una vez más, no hay forma de saberlo ahora."


translate spanish day5_main3_3f2cf4ef:


    mt "Vale, ¡comencemos!{w} ¡Moscú!"


translate spanish day5_main3_dad4cc7b:


    "Los pioneros empezaron a nombrar ciudades."


translate spanish day5_main3_3f3598ba:


    "Finalmente, fue mi turno."


translate spanish day5_main3_58df1a5a:


    "Traté de escuchar atentamente la primera letra de la ciudad que tendría que usar."


translate spanish day5_main3_d1d876fd:


    me "Arcángel."


translate spanish day5_main3_f374397d:


    "Jugamos varias rondas."


translate spanish day5_main3_0759a0f8:


    "Cada nuevo nombre de ciudad, hacía más difícil de recordar todos los nombres que se hubieran mencionado antes."


translate spanish day5_main3_e27c9e7a:


    "Mi atención se fue disipando y al final me acabé perdiendo en todas aquellas capitales, megalópolis, pueblos y asentamientos urbanos."


translate spanish day5_main3_b45788c7:


    mt "¡Semyon! ¡Semyon! ¡Es tu turno!"


translate spanish day5_main3_b4c8d23c:


    "Olga Dmitrievna me trajo de vuelta a la realidad."


translate spanish day5_main3_60202c5c:


    me "Oh, discúlpame... ¿Cuál era el último?"


translate spanish day5_main3_cb702f2c:


    mt "¡Estás soñando despierto otra vez!{w} Era Sevastopol."


translate spanish day5_main3_25856564:


    me "Vale, pues ahora digo Londres..."


translate spanish day5_main3_9affe25e:


    mt "Ya está dicha."


translate spanish day5_main3_bad80ec1:


    me "Bueno, pues..."


translate spanish day5_main3_63447575:


    "Me hizo pensar.{w} Había montones de ciudades en el mundo que comenzaran con «L», pero era complicado recordar siquiera una ahora."


translate spanish day5_main3_b6595b35:


    me "¿Liverpool?"


translate spanish day5_main3_6bbc1d1e:


    mt "¡Ya se dijo!"


translate spanish day5_main3_b9a2a957:


    me "¿Los Ángeles?"


translate spanish day5_main3_4c5275f0:


    mt "¡Ah, al fin!"


translate spanish day5_main3_e7e84af8:


    "Me hizo una mirada desdeñosa pero el juego siguió."


translate spanish day5_main3_fca355af:


    "Difícilmente podría haber pensado otra ciudad que empezase con «L», pero, afortunadamente, era la última ronda."


translate spanish day5_main3_4e97d333:


    mt "Vale, ¡suficiente por hoy! Ya se ha hecho tarde, ¡hora de volver!"


translate spanish day5_main3_99742b57:


    "Suspiré con alivio."


translate spanish day5_main3_5e1a747b:


    "En nuestro camino de vuelta, caminamos como queríamos sin ir en parejas."


translate spanish day5_main3_d3fc10d2:


    "La noche cayó sobre el campamento.{w} Una perfecta noche común y normal."


translate spanish day5_main3_46ada0f2:


    "Era una de esas noches cuando los cielos oscuros, las estrellas e incluso la luna creciente, no producían ningún sentimiento especial y el grillar de los grillos y las canciones de los pájaros nocturnos, eran más bien como una rutina de ruidos de trabajo más que un coro nocturno."


translate spanish day5_main3_a20cefa7_1:


    "..."


translate spanish day5_main3_604b0d7d:


    "En pocos minutos todos los pioneros estaban formados en la plaza."


translate spanish day5_main3_5e7a97d2:


    "Era bastante tarde ya y la fatiga hizo estragos, así que nuestra formación no estaba perfectamente alineada."


translate spanish day5_main3_26bc47b6:


    "Me parecía más una formación de vikingos tras una exitosa batalla, donde los guerreros estaban felices y sonreían, anticipando su regreso con sus familias más que en pensar mantener una correcta formación."


translate spanish day5_main3_86ecb47a:


    "Pero otro podría ver posiblemente una tropa completamente derrotada o un montón de supervivientes que tienen que marchar hacia su patria con sus últimas fuerzas."


translate spanish day5_main3_75fc1be9:


    mt "¡Gracias a todos!{w} Y ahora idos a dormir, ¡ya es muy tarde!"


translate spanish day5_main3_539072ed:


    "Los pioneros se fueron corriendo rápidamente cada uno de ellos en su dirección, y me quedé solo junto a la líder del campamento."


translate spanish day5_main3_ca1d4f17:


    mt "¡Y nosotros deberíamos ir también!"


translate spanish day5_main3_006c39f4:


    "Fuimos en completo silencio hacia la cabaña de Olga Dmitrievna."


translate spanish day5_main3_79ed0be1:


    mt "Muy bien, ¡a dormir!"


translate spanish day5_main3_899c3f81:


    "Dijo, apagando la luz."


translate spanish day5_main3_ae190c31:


    "Me estuve dando vueltas y revolviéndome por bastante rato, recordando todo lo que pasó durante el día."


translate spanish day5_main3_8212c1e8:


    "Por una parte, estaba saturado de cansancio.{w} Por otra parte... no podía dejar de sentir que me olvidaba de algo, que hice algo mal, que dije algo mal..."


translate spanish day5_main3_a6f64aad:


    "Y esta sensación de vacío me estaba atormentando."


translate spanish day5_main3_9cd093b9:


    "Eran las dos de la madrugada aproximadamente."


translate spanish day5_main3_7962d319:


    "Todas las cosas malas terminarían tarde o temprano.{w} O por lo menos me darían un respiro..."


translate spanish day5_main3_d3e44219:


    "Me quedé dormido."


translate spanish day5_dv_b3a56e2b:


    "Sólo Lena y Alisa destacaban entre todo este esplendor."


translate spanish day5_dv_9494f7ea:


    "Bueno, claro que era bastante natural para Alisa estar discutiendo con alguien de esa manera."


translate spanish day5_dv_49a0c8bc:


    "Pero escuchar a Lena hablando con voz alta..."


translate spanish day5_dv_9844ee70:


    "Me acerqué silenciosamente, tratando de comprender qué era lo que ocurría."


translate spanish day5_dv_6ca87438:


    dv "¡No, escúchame a mi!"


translate spanish day5_dv_76e80067:


    un "Di todo lo que quieras, ¡ya lo dije todo!"


translate spanish day5_dv_d1acba6b:


    th "Parece un asunto serio."


translate spanish day5_dv_7d833908:


    "Traté con tanto esfuerzo como pude para no atraer su atención, pareciendo como si estuviera ahí sin ninguna intención de escucharlas."


translate spanish day5_dv_65a0ca80:


    dv "No hay nada que pensar sobre ello... ¡todo está tan claro como el día!"


translate spanish day5_dv_08837eb1:


    dv "No intentes darme lecciones de esa forma, te conozco muy bien."


translate spanish day5_dv_85d1fa2f:


    un "¿Por qué? ¡Claro, por que lo sabes todo! ¿Entonces por qué no se lo cuentas a él tú misma?"


translate spanish day5_dv_cf4dd80f:


    "Lena se estaba enfadando. Eso por sí solo ya era realmente extraño, incluso considerando que no sabía de qué estaban discutiendo."


translate spanish day5_dv_7dfc2afa:


    dv "¡Eso no es asunto tuyo!"


translate spanish day5_dv_12152c43:


    "Alisa resopló, se dio la vuelta y su mirada se encontró con la mía."


translate spanish day5_dv_b91f8b82:


    "Un segundo después, Lena me hizo una mirada también."


translate spanish day5_dv_aeac4053:


    "Las muchachas se quedaron confusas por un rato."


translate spanish day5_dv_a22d002f:


    dv "Tú...{w} ¡¿Escuchaste a escondidas?!"


translate spanish day5_dv_e808d0e5:


    me "¿Yo?{w} Oh, ¡no, no, no! Solamente... estaba... pasando por..."


translate spanish day5_dv_61947ed2:


    "Puse la mejor sonrisa que tenía, pero no pareció ayudar mucho."


translate spanish day5_dv_f829601e:


    dv "Por cierto, ¡hoy me estaba espiando!"


translate spanish day5_dv_31e62164:


    "Alisa me hizo una sonrisa maliciosa."


translate spanish day5_dv_74229088:


    "Lena me miró inquisitivamente."


translate spanish day5_dv_57da622c:


    th "Primero, ¿por qué se iba a creer ella esta historia de esa retorcida zorra?"


translate spanish day5_dv_d122756b:


    me "¡No espiaba!{w} ¡Sabes muy bien que fue un accidente! ¡Ulyana debería ser la única culpable!"


translate spanish day5_dv_f4cf67d8:


    dv "Seguro que lo era... ¡cuéntale eso a los policías!"


translate spanish day5_dv_b0cbdad9:


    th "Al parecer, mi excusa no sirvió."


translate spanish day5_dv_93816b7f:


    dv "¿Te gustaron mis tetas?"


translate spanish day5_dv_169666e8:


    "Un escalofrío recorrió mi espalda."


translate spanish day5_dv_8bfbf78c:


    un "¿Es eso...{w} verdad?"


translate spanish day5_dv_b17dc3c0:


    "Lena me miraba de modo suplicante."


translate spanish day5_dv_78824aab:


    me "Bueno, verás, fue un accidente...{w} No vi nada allí."


translate spanish day5_dv_7394ce16:


    dv "¿No las viste? Bien, ¡te las puedo enseñar otra vez!"


translate spanish day5_dv_a55bd08a:


    "Alisa chilló furiosa."


translate spanish day5_dv_a03136b3:


    "No sabía qué decir."


translate spanish day5_dv_c67e9088:


    dv "¿Lo ves? ¡Te lo dije!"


translate spanish day5_dv_709e3a50:


    un "No... No..."


translate spanish day5_dv_0d581cb1:


    "Lena comenzó a murmurar, un instante después se fue corriendo."


translate spanish day5_dv_87a2f256:


    me "Espera, ¡¿qué sucedió?!"


translate spanish day5_dv_3da1183b:


    "Le grité tras ella."


translate spanish day5_dv_0cd43818:


    dv "Ves lo que hiciste... ¡ahora esa muchacha está molesta!"


translate spanish day5_dv_31e62164_1:


    "Ella me hizo una sonrisa vengativa."


translate spanish day5_dv_fab61109:


    me "Lo primero de todo, ¡no fui yo quien la molestó! Lo segundo, ¡no tengo ni idea de que estabais discutiendo!"


translate spanish day5_dv_2b19dcc7:


    "Se me estaba agotando la paciencia."


translate spanish day5_dv_b578411d:


    dv "¿Por qué debería contártelo?"


translate spanish day5_dv_65ea82d5:


    "Parecía como si ella pensara que nuestra conversación hubiera terminado, y estaba a punto de irse."


translate spanish day5_dv_0c737116:


    "Le agarré de la mano súbitamente."


translate spanish day5_dv_6a189dfc:


    "Alisa me miró con miedo, pero no dijo nada."


translate spanish day5_dv_277c9533:


    "Estaba enfadado en aquel instante. ¡Muy enfadado!"


translate spanish day5_dv_f6618979:


    "Primero, estaba extremadamente enfurecido de que Lena estuviera molesta por mi (incluso a pesar de que en realidad tenía algo de culpa)."


translate spanish day5_dv_7c8a30ee:


    "Segundo, estaba enfurecido por la arrogancia de Alisa."


translate spanish day5_dv_513493ad:


    "Tercero, estaba tan cansado que estaba a punto de perder mi control con cualquier cosa por pequeña que fuera."


translate spanish day5_dv_2bf8f738:


    me "¿Estás feliz ahora?"


translate spanish day5_dv_9c13f0cc:


    "Le siseé, indicando en la dirección de la oscuridad a la cual Lena se fue corriendo."


translate spanish day5_dv_240840c7:


    me "¡Solamente tienes agallas para poner bombas en monumentos y herir a muchachas inocentes!"


translate spanish day5_dv_f71a0fac:


    "Se le veía asustada, triste y confusa al mismo tiempo."


translate spanish day5_dv_050fb154:


    dv "Ella misma...{w} ¡podría herir a cualquiera!{w} ¡No la conoces!"


translate spanish day5_dv_73f6779b:


    me "La única verdad aquí es que no la conozco muy bien. ¡Pero estoy completamente seguro de que ella no haría daño a nadie! ¡Ése es tu trabajo!"


translate spanish day5_dv_03cd505c:


    "Nos quedamos en silencio por un rato."


translate spanish day5_dv_60d9b7af:


    "Sostenía la mano de Alisa y no sabía qué hacer."


translate spanish day5_dv_47ae85aa:


    "La solución me vino por sí misma."


translate spanish day5_dv_370eed20:


    me "¡Tienes que ir y disculparte!"


translate spanish day5_dv_f9f3ed76:


    dv "¿Por qué demonios debería hacerlo?"


translate spanish day5_dv_fd492212:


    "Alisa trató de actuar como una arrogante como siempre, pero no le iba a funcionar en esta ocasión."


translate spanish day5_dv_7b8ee948:


    me "¡Porque lo digo yo!"


translate spanish day5_dv_983d54f9:


    "Me negué a escuchar cualquier excusa y la arrastré al lado de la hoguera."


translate spanish day5_dv_ff2ed8eb:


    me "Olga Dmitrievna, lo siento, tenemos un asunto urgente que tratar, por lo que tendremos que irnos pronto."


translate spanish day5_dv_7d32c910:


    "La líder trató de oponerse, pero no la escuché y me dirigí a por Lena, tirando de Alisa conmigo."


translate spanish day5_dv_a69e912e:


    "Tras un rato, le solté la mano."


translate spanish day5_dv_e687fb2b:


    me "¿Alguna excusa?"


translate spanish day5_dv_552eae04:


    "Todo el orgullo y la arrogancia habían abandonado el rostro de Alisa hace tiempo."


translate spanish day5_dv_5f5a4508:


    dv "Si lo que quieres es que vaya contigo lo haré, pero no tengo nada de qué disculparme... Le dije la verdad."


translate spanish day5_dv_f99126ea:


    me "Pues eso ya lo veremos."


translate spanish day5_dv_a767512c:


    "Le espeté."


translate spanish day5_dv_29f6f60c:


    "Habíamos estado caminando callados hasta que entramos en el campamento."


translate spanish day5_dv_9cee08fa:


    "Llegamos a la plaza y le pregunté a Alisa:"


translate spanish day5_dv_2442aa77:


    me "¿Dónde vamos ahora?"


translate spanish day5_dv_37160fae:


    dv "¿Cómo voy a saberlo?"


translate spanish day5_dv_65c09781:


    me "¿Y quién dijo que conocía muy bien a Lena? ¡¿Acaso fui yo?!"


translate spanish day5_dv_571e60de:


    "Alisa dudó."


translate spanish day5_dv_8a9865ff:


    dv "Bueno, ella puede que esté en la isla...{w} A veces va cuando quiere estar sola."


translate spanish day5_dv_a499094f:


    me "¡Excelente!"


translate spanish day5_dv_c8bd92fc:


    "Nos dirigimos hasta el embarcadero."


translate spanish day5_dv_cffd7334:


    "La noche había caído en el campamento, mientras buscaba los remos y me entretenía con las cuerdas."


translate spanish day5_dv_07f664e2:


    "En realidad faltaba una barca."


translate spanish day5_dv_e631a0c0:


    me "Vale, hoy es tu día de suerte, vas a tener que hacer unos cuantos ejercicios duros para definir tus biceps, triceps y otros músculos de los brazos."


translate spanish day5_dv_27c62366:


    "Dije sarcásticamente."


translate spanish day5_dv_25a535c0:


    dv "¿Lo dices en serio?"


translate spanish day5_dv_883a35cc:


    "Alisa me hizo una mirada temerosa."


translate spanish day5_dv_183b833e:


    th "Bueno, quizás eso fue un poco brusco.{w} Incluso para ella..."


translate spanish day5_dv_5b893c52:


    me "Muy bien, no te haré remar, claro..."


translate spanish day5_dv_1b5cc40c:


    "Eso no sonó con mucha confianza."


translate spanish day5_dv_36f1c607:


    "Ondear los remos aparentaba ser mucho más difícil en esta ocasión."


translate spanish day5_dv_a5ba40b7:


    "Bueno, eso no me sorprende, considerando que hoy debo haber cumplido ya con mi cuota anual de remos."


translate spanish day5_dv_968f6af6:


    "Me detuve en medio del río para recobrar el aliento."


translate spanish day5_dv_ee0bd590:


    "El campamento estaba cubierto por la noche.{w} Una noche trivial y ordinaria."


translate spanish day5_dv_46ada0f2:


    "Era una de esas noches cuando los cielos oscuros, las estrellas e incluso la luna creciente, no producían ningún sentimiento especial y el grillar de los grillos y las canciones de los pájaros nocturnos, eran mas bien como una rutina de ruidos de trabajo más que un coro nocturno."


translate spanish day5_dv_4b0e60fa:


    "Estaba mirando fijamente en la oscuridad, tratando de vislumbrar la sombría isla."


translate spanish day5_dv_b4ddcd02:


    me "¿Cómo la conociste?"


translate spanish day5_dv_3f85db61:


    "Alisa le dio un poco de escalofríos."


translate spanish day5_dv_e3340992:


    dv "Crecimos juntas."


translate spanish day5_dv_0b791288:


    "Respondió lentamente tras algo de duda."


translate spanish day5_dv_feaeb3c3:


    me "Y acabáis en el mismo campamento de pioneros.{w} ¡Qué coincidencia más fabulosa!"


translate spanish day5_dv_826081ef:


    "Todas estas cuestiones, misterios y miedos que habían sido tan bien escondidos, surgieron tan repentinamente en mi cabeza."


translate spanish day5_dv_4cdcce8a:


    th "Maldita sea, podría simplemente tirarla de la barca y ahogarla justo aquí y ahora."


translate spanish day5_dv_8a51e645:


    th "Puedo pedir respuestas."


translate spanish day5_dv_01dabe85:


    th "Aunque, podría ser peligroso a menos que estuviera seguro de lo que hacía."


translate spanish day5_dv_d79d8ce6:


    "Pero aun y así, pensé que esta vez jugaba con las piezas blancas, así que podía hacer el primer movimiento, y no tan sólo bloquear los golpes del oponente."


translate spanish day5_dv_9c1029af:


    me "Y bien, ¿qué tipo de campamento es este?"


translate spanish day5_dv_acae4786:


    "Alisa me miró fijamente perpleja."


translate spanish day5_dv_38f0a76d:


    me "¿Dónde estamos? ¿Por qué estamos aquí?"


translate spanish day5_dv_27b39f33:


    "Se quedó callada."


translate spanish day5_dv_46287afa:


    me "¡Que me contestes!"


translate spanish day5_dv_91d35e95:


    "Le grité."


translate spanish day5_dv_3f5fc648:


    dv "Ey, ¿qué pasa contigo...?{w} Si quieres que me disculpe con Lena, lo haré..."


translate spanish day5_dv_6579c7ab:


    "Ahí es dónde me di cuenta de lo estúpido que me estaba comportando, como poco."


translate spanish day5_dv_7113a55a:


    th "Ella podria no estar implicada sencillamente en nada... de esto."


translate spanish day5_dv_7ebb2614:


    th "Y lo que es más importante...{w} Nadie puede mentir tan convincentemente."


translate spanish day5_dv_481f6bb0:


    "La expresión temerosa de Alisa era, además, otro motivo para creerla."


translate spanish day5_dv_44b2f075:


    me "Lo siento..."


translate spanish day5_dv_613ae970:


    "Eso era todo lo que era capaz de decir en aquel momento."


translate spanish day5_dv_2283e3ea:


    "Y aun quise mantener mi ventaja."


translate spanish day5_dv_8b39987d:


    "Los últimos pocos metros fueron realmente duros."


translate spanish day5_dv_1fa7fb2a:


    "Exhausto, salí fuera de la barca y me derrumbé en el frío suelo."


translate spanish day5_dv_4942088d:


    "Alisa se quedó cerca, mirándome."


translate spanish day5_dv_395406fd:


    "Pensé que iba a provocarme o a pronunciar alguna ocurrencia hiriente, pero en tanto que permaneció en silencio, asumí que todavía estaba trastornada o algo así."


translate spanish day5_dv_7de75851:


    "Incluso a pesar de que se estaba más bien a oscuras aquí, habría sido más fácil esconderse en mi viejo apartamento que en esta isla enana."


translate spanish day5_dv_8f34fcaa:


    "Aun más, Lena debe habernos escuchado ya mientras estábamos cruzando el río, así que por ahora ya era demasiado tarde para preguntarle a Alisa cualquiera otra cuestión, a pesar de que todavía tenía alguna."


translate spanish day5_dv_17717ee8:


    me "Escucha..."


translate spanish day5_dv_3b8d3ec7:


    "La miré pero no vi nada... era una oscura noche y no tenía ojos de gato."


translate spanish day5_dv_0940755d:


    me "Bueno, cuando encontremos a Lena le vas a contar que nunca quisiste herirle sus sentimientos, y que todo lo que sucedió por la mañana no era nada salvo un malentendido, ¿vale?"


translate spanish day5_dv_cb693d1d:


    "Mi voz no sonó con tanta seguridad como lo hizo hace un par de minutos atrás, pero con suficiente confianza."


translate spanish day5_dv_64847fff:


    "Aunque Alisa no dijera nada, estaba claro por el aspecto de su cara que estaba de acuerdo."


translate spanish day5_dv_89f2dd53:


    "Me levanté y fuimos a buscar a Lena."


translate spanish day5_dv_9ca3b435:


    "Su barca estaba atada a un gancho en un costado alejado de la isla."


translate spanish day5_dv_a5b3f875:


    me "Eso es muy inteligente por su parte.{w} Nadie vería su barca de lejos."


translate spanish day5_dv_3cc8517a:


    "Murmuré."


translate spanish day5_dv_afb89a5f:


    me "Bueno, eso significa que ella está en alguna parte entre los árboles, ¡busquémosla!"


translate spanish day5_dv_90bebd35:


    "Me dirigí hacia la arboleda pero Alisa se quedó quieta."


translate spanish day5_dv_889111c0:


    me "¿Qué pasa?{w} Creía que ya habíamos discutido todo..."


translate spanish day5_dv_f2f597bd:


    dv "Err...{w} No es eso...{w} Es solamente que...{w} es oscuro... ahí dentro..."


translate spanish day5_dv_8d507ec8:


    "Tuve que agudizar mi vista para verle la cara."


translate spanish day5_dv_28c1ce27:


    me "La arboleda es más pequeña que el jardín de margaritas de mi abuela. ¿De qué hay que preocuparse?"


translate spanish day5_dv_da6c23b0:


    "Alisa no respondió."


translate spanish day5_dv_f4740084:


    me "Si quieres saber lo que pienso, estaría encantado de dejarte sola aquí. ¡Nunca sé de qué broma eres capaz!"


translate spanish day5_dv_a1bcd7f2:


    dv "Bueno..."


translate spanish day5_dv_6239b879:


    "Se me acercó."


translate spanish day5_dv_0127153c:


    dv "Vayamos así..."


translate spanish day5_dv_9a59280f:


    "Me agarró de la mano."


translate spanish day5_dv_18180057:


    "No me esperaba esa reacción."


translate spanish day5_dv_09c023e9:


    "Mis mejillas se sonrojaron, perdí mi respiración y apenas podía decir algo."


translate spanish day5_dv_729fa06e:


    "Toda la situación en sí no ayudaba en nada a la conversación, lo cual era probablemente lo mejor puesto que dudo que hubiera podido ser capaz de decir algo inteligente."


translate spanish day5_dv_140f9a5e:


    "Lentamente nos dirigimos hacia la arboleda."


translate spanish day5_dv_dbdafe82:


    "Pocos instantes después recuperé algo de control sobre mí mismo."


translate spanish day5_dv_f8e9e2d5:


    me "vamos, no hay realmente nada que temer.{w} Vinimos aquí durante el día, reuniendo unas pocas fresas... no fue problema alguno. Sólo es una arboleda de abedules normal y corriente..."


translate spanish day5_dv_a6dda4a2:


    me "En verdad se está muy bien aquí durante el día."


translate spanish day5_dv_29bf0ac9:


    "Añadí tras una breve pausa."


translate spanish day5_dv_2c4d16f2:


    "Alisa no me miraba, estaba mirando fijamente en alguna parte en la distancia."


translate spanish day5_dv_57ad0457:


    me "Bueno, como quieras..."


translate spanish day5_dv_dbfec409:


    "Al cabo de un rato, ella señaló delante, frente a nosotros."


translate spanish day5_dv_65ced1f6:


    dv "Ahí está."


translate spanish day5_dv_5460ca51:


    "Miré de cerca entre la oscuridad, pero apenas podía ver algo."


translate spanish day5_dv_9e040406:


    "Cuando nos acercamos, vimos a Lena sentada junto a un árbol.{w} Estaba llorando."


translate spanish day5_dv_41ec3a67:


    un "¿Por qué habéis venido hasta aquí?"


translate spanish day5_dv_563f0f0a:


    "Preguntó con lágrimas en los ojos."


translate spanish day5_dv_8fc2a6d9:


    un "Y tú..."


translate spanish day5_dv_0b024121:


    "Miró a Alisa pero no terminó su frase."


translate spanish day5_dv_86522e17:


    "En ese momento Alisa soltó mi mano."


translate spanish day5_dv_498638c2:


    me "Bueno, verás..."


translate spanish day5_dv_9eb817ac:


    dv "Solamente quería disculparme por ser algo severa y grosera..."


translate spanish day5_dv_272bb3ad:


    "Alisa me interrumpió.{w} Su voz había recobrado su acostumbrada arrogancia en ella."


translate spanish day5_dv_7fc953c2:


    "Quizá sea porque no quiera mostrarse débil frente a Lena."


translate spanish day5_dv_0ed5b186:


    un "¡Qué disculpa más perfecta!{w} Donde dije digo, digo Diego, ¡¿verdad?!"


translate spanish day5_dv_9b03effe:


    un "¡Qué aguda eres en insistirme que hacía algo mal, cuando luego tú haces eso mismo!"


translate spanish day5_dv_991e4d49:


    "Lena se puso a gritar descontroladamente."


translate spanish day5_dv_d2b96176:


    dv "Lo has malinterpretado todo..."


translate spanish day5_dv_04d782d6:


    un "¡He entendido lo que he visto!"


translate spanish day5_dv_35da24e2:


    me "Err... Veréis, muchachas...{w} Estáis discutiendo y creo que estoy relacionado de alguna forma en todo ello, ¡pero no comprendo nada!"


translate spanish day5_dv_c501f81c:


    un "Explícaselo a él."


translate spanish day5_dv_6b44c45f:


    "Le dijo Lena a Alisa con una sonrisita."


translate spanish day5_dv_921308b4:


    dv "No hay nada que explicar..."


translate spanish day5_dv_006275ed:


    un "Bueno, ¿entonces me dejarás intentarlo?"


translate spanish day5_dv_319d4fcb:


    dv "¡Lo que sea!"


translate spanish day5_dv_184d37fc:


    "Entrecruzó sus brazos por encima de su pecho y se dio la vuelta."


translate spanish day5_dv_a958d2ed:


    un "Pues bien, Semyon...{w} Ella me reprocha que fuera detrás tuyo."


translate spanish day5_dv_c52b3095:


    "Por un minuto hubo un completo silencio."


translate spanish day5_dv_e8d3e7f3:


    "Lena no me miró como para explicarme algo más, y en cuanto a mí, todavía estaba ahí quieto tratando de ensamblar las desperdigadas piezas de un puzle para completarlo."


translate spanish day5_dv_149b3064:


    me "¿Qué quieres decir?"


translate spanish day5_dv_885d4f38:


    un "Exactamente lo que dije."


translate spanish day5_dv_220fc882:


    dv "Eso no es así, eso no es lo que yo quiero decir."


translate spanish day5_dv_f15e5159:


    "Interrumpió Alisa."


translate spanish day5_dv_bbd6ebea:


    un "Por supuesto...{w} Siempre has sido así..."


translate spanish day5_dv_6771fae9:


    "Lena rompió a llorar otra vez."


translate spanish day5_dv_7a91f6b6:


    me "Espera, ¿cómo...?"


translate spanish day5_dv_99a3bbc7:


    th "Entonces... ¿Le gusto a Lena?"


translate spanish day5_dv_b711211f:


    un "¡Y tú! ¿Tú qué haces? ¡Tratas de convencerme de que todo es un malentendido, mientras le sostienes la mano a ella!"


translate spanish day5_dv_56b5ad1b:


    th "¿Qué? ¡¿Entonces a Alisa le gusto también?!{w} Oh, venga ya, ¿Alisa queriendo a alguien? Eso no puede estar ocurriendo..."


translate spanish day5_dv_428cd21d:


    dv "Es sólo que se está oscuro aquí y..."


translate spanish day5_dv_72b459e3:


    un "Eso es propio de ti.{w} Siempre tan arrogante, siempre tratando de controlar a los demás, pero cuando se trata de ti..."


translate spanish day5_dv_da6c23b0_1:


    "Alisa no contestó."


translate spanish day5_dv_d66e57b9:


    "Realmente sentía que tenía que decir algo con tal de rebajar la tensión de la situación."


translate spanish day5_dv_246e6c69:


    me "Ey, espera...{w} Quizá todos hayamos hecho algo mal...{w} Yo..."


translate spanish day5_dv_443723c9:


    dv "Esto no tiene nada que ver contigo..."


translate spanish day5_dv_b386505f:


    "La voz de Alisa fue peculiarmente distante."


translate spanish day5_dv_013ab4b3:


    me "A juzgar por la conversación, esto tiene mucho que ver conmigo."


translate spanish day5_dv_cb70eba8:


    un "¡Sí, claro que tiene!{w} ¡Pregúntale a ella qué piensa de ti y por qué me dijo que no fuera detrás tuyo!"


translate spanish day5_dv_f7e5e4be:


    th "No creo que Lena fuera en realidad «detrás de mí»."


translate spanish day5_dv_91cb39e7:


    dv "Te lo voy a decir una vez más, yo..."


translate spanish day5_dv_19fe9001:


    un "¡Ya hemos escuchado suficiente tu historia!"


translate spanish day5_dv_eae66d30:


    "Lena se levantó y salió corriendo hacia la oscuridad."


translate spanish day5_dv_d1bcdf75:


    "Me sentía clavado como una estaca en el suelo, sin saber qué hacer."


translate spanish day5_dv_f72d2ca7:


    th "Si seguía a Lena ahora, ¿qué le diría cuando la hallase?"


translate spanish day5_dv_f0495ba4:


    th "Después de todo, no sé tampoco de qué va todo esto."


translate spanish day5_dv_09af90a3:


    th "Mi despiste y mis tontos intentos de consolarla solamente lograrían molestarla más."


translate spanish day5_dv_87ab0698:


    me "Supongo que es mejor dejarla sola por un tiempo..."


translate spanish day5_dv_e5989bb1:


    "Eso no sonó muy seguro."


translate spanish day5_dv_448ae357:


    dv "Me da lo mismo."


translate spanish day5_dv_c19759e7:


    "Sin ninguna otra palabra Alisa se dirigió hacia la barca."


translate spanish day5_dv_a20cefa7:


    "..."


translate spanish day5_dv_9525b0c0:


    "El camino entero para cruzar el río hasta el embarcadero fue un infierno. Tenía que pararme cada diez o veinte metros para recuperar el aliento."


translate spanish day5_dv_7231fc65:


    "Alisa estaba mirando el río y no me prestaba antención."


translate spanish day5_dv_2ace92a8:


    "Bueno, parece que ésta es la primera situación en el campamento que está completamente más allá de mi comprensión."


translate spanish day5_dv_5248806b:


    "Incluso a pesar de que en definitiva, es simplemente trivial, una situación del día a día."


translate spanish day5_dv_c998baac:


    "No estuve tan confundido incluso cuando fui por la noche al antiguo campamento."


translate spanish day5_dv_0fc4d70a:


    th "Después de todo, si le echas un vistazo con más detalle, no hay nada de extraño aquí."


translate spanish day5_dv_74caf240:


    th "Lena es muy tímida y quizás incluso una persona introvertida, y aun así es humana y no puede evitar comportarse como tal."


translate spanish day5_dv_6b02e0fc:


    th "Considerando eso, su reacción se ajusta a dicha situación bastante bien."


translate spanish day5_dv_8096860d:


    "Incluso aunque no entendí el motivo de la disputa, estaba claro como el agua que las muchachas no se habrían comportado de esa forma si no tuvieran una buena razón."


translate spanish day5_dv_e82ce761:


    "Y tal como fueron las cosas, la causa era yo."


translate spanish day5_dv_b467b940:


    "Y ésa es la parte extraña de toda la situación."


translate spanish day5_dv_b1b2da01:


    me "Ey, siento que todo fuera de esta manera.{w} Si solamente no hubiera escuchado vuestra conversación..."


translate spanish day5_dv_6a0b3208:


    "Era como si sintiera una necesidad patológica de justificarme."


translate spanish day5_dv_443f185c:


    dv "No todo es culpa tuya."


translate spanish day5_dv_560d31b2:


    "Dijo con la mente ausente."


translate spanish day5_dv_d59d8325:


    dv "Solamente es que apareciste en el lugar adecuado, en el momento adecuado y reinició un mecanismo que se había detenido hacía mucho tiempo."


translate spanish day5_dv_3aad82ef:


    me "No lo acabo de entender..."


translate spanish day5_dv_e1d83f9f:


    dv "Claro que no.{w} Y en realidad no deberías entenderlo ahora.{w} Lo comprenderás más tarde."


translate spanish day5_dv_1b595140:


    "Me lanzó una mirada intensa."


translate spanish day5_dv_fca3e3d8:


    me "¡Maldita sea!{w} ¡¿Por qué siempre me ocurre a mí?!"


translate spanish day5_dv_635de124:


    "Tanto en el instituto como en la universidad, los problemas parecían escogerme a mí como su objetivo primario."


translate spanish day5_dv_455102c5:


    "¿Que alguien bloqueaba la puerta de clase con una fregona? Pues me llevaba un escrito de reprimenda en mi cuaderno de calificaciones."


translate spanish day5_dv_f91c8482:


    "¿Que había una pelea? Consideradas todas las posibilidades, debía ser yo el que la inició."


translate spanish day5_dv_9e87993c:


    "¿Que suspendía un examen? Bueno, por supuesto que era mi culpa y el profesor que me odiaba no tenía nada que ver en ello."


translate spanish day5_dv_66fe6755:


    "Incluso mis padres siempre preferían reprocharme a mí antes que a unas circunstancias adversas, la intervención de un tercero o la sentencia de la providencia."


translate spanish day5_dv_6130f8de:


    "En cierto momento comencé a creer que de alguna forma atraía los problemas. ¿Recuerdas la Ley de Murphy?{w} En mi caso era como «Si algo malo tiene que ocurrir, entonces me sucederá a mí»."


translate spanish day5_dv_c8a1ebf7:


    "Por ese motivo siempre traté de mantenerme al margen de cualquier problema, donde pudiera acabar siendo el cabeza de turco."


translate spanish day5_dv_ffc93cf9:


    "Bueno, a juzgar por lo de hoy, no estoy en condiciones..."


translate spanish day5_dv_7e2b13fc:


    dv "Es como si hubiera algún tipo de aura a tu alrededor... atraes la atención."


translate spanish day5_dv_194f815a:


    "Sorprendido miré a Alisa.{w} Estaba sonriendo."


translate spanish day5_dv_8a97cc6a:


    th "Es como si leyera mi mente..."


translate spanish day5_dv_df9b37de:


    me "Problemas, mayoritariamente."


translate spanish day5_dv_127fc146:


    dv "Quién sabe... quién sabe..."


translate spanish day5_dv_3fed2b26:


    "Lo dijo soñadoramente y se quedó mirando fijamente el río."


translate spanish day5_dv_4676d969:


    dv "Es una noche maravillosa, ¿verdad?"


translate spanish day5_dv_a4b71fa9:


    me "Justo como cualquiera otra."


translate spanish day5_dv_223e723d:


    "Me sentía consumido."


translate spanish day5_dv_7e23ddcf:


    dv "Eso es exactamente sobre lo que estoy hablando...{w} Simplemente una noche normal y corriente, nada de especial. El mejor momento para hacer feliz de verdad a nuestra admirable líder del campamento."


translate spanish day5_dv_3145d815:


    dv "¡Tiene que conocer que su pionero favorito es un sucio pervertido que disfruta espiando muchachas desnudas!"


translate spanish day5_dv_9821dca6:


    "Algo se quebró en mi interior otra vez..."


translate spanish day5_dv_c606f827:


    me "No irás en serio, ¿verdad?"


translate spanish day5_dv_aa6b4182:


    dv "Por qué no...{w} ¡Claro que voy en serio!"


translate spanish day5_dv_d4711875:


    "No se le veía para nada que estuviera bromeando."


translate spanish day5_dv_6fc6b848:


    me "Bueno, como quieras pero, ¿crees de verdad que es el mejor momento para hacer eso?"


translate spanish day5_dv_6d61f732:


    dv "¿No se supone que deberías remar?"


translate spanish day5_dv_afbb39ba:


    "Me di cuenta de que nuestra barca aun estaba flotando en medio del río."


translate spanish day5_dv_0934364c:


    me "¡Espera!{w} ¡No ignores mi pregunta!"


translate spanish day5_dv_78b06dfc:


    dv "Volvamos al campamento...{w} Me lo pensaré."


translate spanish day5_dv_20705088:


    "Quedarme a pasar una noche entera en una barca no era algo a lo que aspirara, así que me tuve que aplicar a los remos."


translate spanish day5_dv_1b0374d0:


    "A duras penas llegamos al embarcadero."


translate spanish day5_dv_ffb36c55:


    "Estaba exhausto. Tanto por estar remando como por la intención de Alisa de contarle a Olga Dmitrievna el incidente de esta mañana."


translate spanish day5_dv_d45a2e04:


    "Si conozco bien a nuestra líder del campamento, su reacción será bastante predecible."


translate spanish day5_dv_cc9ba692:


    me "En fin, así que realmente vas a ir a visitar a Olga Dmitrievna ahora, ¿no?"


translate spanish day5_dv_f74976c4:


    dv "¿Por qué no?"


translate spanish day5_dv_3aa0974f:


    "Me hizo su más pícara sonrisa."


translate spanish day5_dv_6c9ff79e:


    me "Es de noche después de todo.{w} Debe estar durmiendo ya."


translate spanish day5_dv_34c6ac1d:


    dv "Quién sabe, puede que esté despierta..."


translate spanish day5_dv_e2560be6:


    me "Venga ya, ¡sabes perfectamente que fue una coincidencia! ¡Un simple malentendido provocado por Ulyana!"


translate spanish day5_dv_43cdac3a:


    dv "No, no lo fue..."


translate spanish day5_dv_dec618ce:


    "Se cruzó de brazos y se dirigió hacia el campamento."


translate spanish day5_dv_86a32f18:


    "Reuní todas mis restantes fuerzas, salté y la seguí."


translate spanish day5_dv_44ba1020:


    "Al principio consideré agarrarla por el brazo, pero luego me di cuenta de que pudiera no ser una buena idea tras todo lo que había sucedido hoy."


translate spanish day5_dv_079f62e5:


    me "Espera..."


translate spanish day5_dv_a4b6ed32:


    "Caminé arduamente sin vigor junto a ella."


translate spanish day5_dv_86a0035c:


    me "Hablemos de esto."


translate spanish day5_dv_4f27c86c:


    dv "¿Es que hay algo que discutir?"


translate spanish day5_dv_1eeed204:


    "Caminamos callados el uno junto al otro durante un rato."


translate spanish day5_dv_a7a06c6d:


    "Por lo menos ella caminaba lentamente, así que no tenía problemas en mantener su ritmo y estaba agradecido por ello."


translate spanish day5_dv_5495a454:


    me "Ey, ¿hay algo que pueda hacer para convencerte de que no se lo cuentes a Olga Dmitrievna?"


translate spanish day5_dv_ddf334ae:


    dv "No sé, pero, bueno... hay una cosa..."


translate spanish day5_dv_b3e9120f:


    "Llegamos a la plaza."


translate spanish day5_dv_6378d82b:


    me "Y bien, ¿qué es?"


translate spanish day5_dv_21c6f737:


    dv "Bueno, deja de perseguir a Lena por ejemplo..."


translate spanish day5_dv_0e345e20:


    th "¡Por el amor de Dios! ¿Qué le hace pensar que estoy persiguiendo a Lena?"


translate spanish day5_dv_434f8b3b:


    "Comenzaba a perder mis estribos."


translate spanish day5_dv_496ee797:


    me "¡¿Por qué te inventas tales tonterías?!{w} Tú empezaste esa riña en el bosque, por tu culpa tuvimos que ir hasta aquella isla, ¡y ahora vuelves a lo mismo!{w} ¡Basta ya!"


translate spanish day5_dv_4c611d30:


    me "¡No la estoy persiguiendo!"


translate spanish day5_dv_922b2e90:


    "Alisa estaba quieta, la brillante luz lunar iluminó su rostro, el cual mostraba evidentes señales de resentimiento y disgusto."


translate spanish day5_dv_dbf16f4b:


    dv "¡Como si no te hubiera visto como la miras!"


translate spanish day5_dv_b9cdb4df:


    me "¿Cómo la miro?"


translate spanish day5_dv_0ba7b0db:


    dv "¡De esa forma!"


translate spanish day5_dv_d2b04ecc:


    me "¿De qué forma?"


translate spanish day5_dv_5f616c8b:


    dv "¡Ya lo sabes!"


translate spanish day5_dv_df0640da:


    "Miró hacia otra parte, pero permaneció estando quieta."


translate spanish day5_dv_41e33f1a:


    me "¡Ey, deja ya de ver a los demás a través de tu retorcida imaginación! Si no puedes parar de inventarte historias, ¡pues por lo menos te las guardas para ti!"


translate spanish day5_dv_b8304cd4:


    me "¡No hagas sufrir a los demás por tu culpa y tus historias! No me importa si lo haces conmigo, ¡pero ahora vas e involucras a Lena!"


translate spanish day5_dv_a6dd47ba:


    "Realmente perdí los estribos para bien, pero Alisa no contestó."


translate spanish day5_dv_73cc2124:


    "Nos hundimos en un inesperado silencio que solamente se rompió por los interrumpidos sollozos de Alisa."


translate spanish day5_dv_1564df3a:


    me "¡De mal en peor! Ahora lloras TÚ...{w} ¿Os habéis vuelto locos aquí?"


translate spanish day5_dv_7a6449b3:


    "Junté mis manos en desesperación."


translate spanish day5_dv_1a648809:


    "Podía entender por qué Lena estaba llorando.{w} Pero ver a Alisa llorar era inimaginable a decir cuanto menos..."


translate spanish day5_dv_e14b3828:


    "Tampoco podía entender sus motivos."


translate spanish day5_dv_f6741270:


    "En cualquier otro momento, estaría en definitiva trastornado, pero en este instante estaba demasiado cansado para eso."


translate spanish day5_dv_6615be77:


    "Mi mente estaba completamente en blanco."


translate spanish day5_dv_99ab0d84:


    "Para ser más exactos, mi cabeza estaba tan espesa que no había lugar en ella para desarrollar ideas."


translate spanish day5_dv_e447cec9:


    "Si comparase mi cerebro en sus mejores tiempos con una gran autopista llena de pensamientos veloces circulando por ahí, persiguiéndose unos a otros y causando accidentes graves, ahora era más bien un diminuto sendero en un distante y desolado bosque, el cual es solamente usado en tiempos de necesidad."


translate spanish day5_dv_c1d95e71:


    "Alisa seguía callada. Por lo menos dejó de llorar."


translate spanish day5_dv_3c584237:


    me "Si de verdad crees que es tan serio, ve y cuéntaselo todo a Olga Dmitrievna...{w} Espero que te haga sentir mejor..."


translate spanish day5_dv_bd4f7e30:


    dv "Vale, no lo haré..."


translate spanish day5_dv_8e49058e:


    "Dijo en voz baja y se giró hacia mi."


translate spanish day5_dv_f381d5cd:


    "Sus lágrimas se habían secado, pero su rostro parecía expresar toda la absoluta melancolía del mundo."


translate spanish day5_dv_3e47cc5e:


    dv "Me siento herida..."


translate spanish day5_dv_e6e0ffb2:


    me "¿Qué te hiere?"


translate spanish day5_dv_b037ba1a:


    "Le pregunté con cansancio."


translate spanish day5_dv_8926ae0c:


    dv "Siempre es así.{w} Ella siempre tiene toda la atención y siempre me deja a un lado."


translate spanish day5_dv_5637fae3:


    "Tenía dificultades para comprender de qué estaba hablando, así que decidí seguirle el juego."


translate spanish day5_dv_1f586e25:


    th "Con suerte me dejará al fin solo."


translate spanish day5_dv_c1141a62:


    me "Eso no es cierto.{w} Haces llamar la atención y la gente no te ignora. Yo no lo hago."


translate spanish day5_dv_5ec2967a:


    "Levanté mi mirada y la observé."


translate spanish day5_dv_8d8f127d:


    "Su expresión facial era una mezcla entre el asombro y la expectación."


translate spanish day5_dv_455fafb6:


    me "¿Lo ves?"


translate spanish day5_dv_694b9cc2:


    dv "Pues entonces tú...{w} Podrías..."


translate spanish day5_dv_cee25919:


    "De repente, su voz resultó tan tierna que me encogí atónito."


translate spanish day5_dv_20588e11:


    "El rostro de Alisa se sonrojó y escondió sus ojos."


translate spanish day5_dv_48f34051:


    dv "Es decir...{w} ¿Quieres decir realmente que soy tan buena como ella?"


translate spanish day5_dv_0250a659:


    "Quise decir algo así como «¡Incluso mejor!», pero me abstuve."


translate spanish day5_dv_693741eb:


    me "Sí, eso creo..."


translate spanish day5_dv_dab6d6c3:


    "Parece que no comprendió que lo dije de forma completamente insincera."


translate spanish day5_dv_1d46b2fc:


    dv "Está bien, ¡hora de dormir!"


translate spanish day5_dv_a412e07f:


    "Gritó de repente Alisa alegremente."


translate spanish day5_dv_ff8903e2:


    "Ahora actuaba otra vez más como era ella misma en realidad."


translate spanish day5_dv_b2e53022:


    dv "Nos vemos mañana."


translate spanish day5_dv_c3c87c93:


    "Se despidió con la mano y se fue corriendo."


translate spanish day5_dv_99742b57:


    "Suspiré con alivio."


translate spanish day5_dv_d688f7f1:


    th "Bueno, se acabó por hoy."


translate spanish day5_dv_334bf905:


    "Toda mi restante energía se agotó con la carrera hacia la cabaña de Olga Dmitrievna."


translate spanish day5_dv_d055c80a:


    "No había luz, así que tratando de no despertar a la líder del campamento, me desvestí silenciosamente y me tumbé."


translate spanish day5_dv_b0e44a4b:


    th "Sigue siendo interesante, ¿qué quiso decir Alisa?"


translate spanish day5_dv_c93f9c04:


    th "¿Y Lena...?"


translate spanish day5_dv_2def8e8d:


    "Es completamente incierto que será de ellas y de mí... como si hubiéramos sido tragados por algún tipo de vórtice, el cual comenzó a girar con una furiosa fuerza."


translate spanish day5_dv_9b5148d6:


    th "Pero, ¿qué es lo que va a ocurrir?"


translate spanish day5_dv_801e05ca:


    th "¿Y dónde seré lanzado pues?"


translate spanish day5_dv_a20cefa7_1:


    "..."


translate spanish day5_sl_608d1f0d:


    "Estaba realmente interesado en saber dónde se fue Slavya."


translate spanish day5_sl_3ff2c172:


    "Aunque también quería irme de este lugar tan rápido como fuera posible y, si no pudiera, tumbarme hasta dormirme, como mínimo para pasarme un rato solo, lejos de todos estos pioneros y Olga Dmitrievna, quien no podía dejar de mantenerme ocupado haciendo cosas."


translate spanish day5_sl_2c4efb4d:


    "Escogí un buen momento y me di a la fuga hacia el bosque."


translate spanish day5_sl_d3fc10d2:


    "La noche había caído sobre el campamento.{w} Una noche completamente normal y corriente."


translate spanish day5_sl_46ada0f2:


    "Era una de esas noches cuando los cielos oscuros, las estrellas e incluso la luna creciente, no producían ningún sentimiento especial y el grillar de los grillos y las canciones de los pájaros nocturnos, eran mas bien como una rutina de ruidos de trabajo más que un coro nocturno."


translate spanish day5_sl_42be4cb2:


    "Vagué alrededor del bosque sin propósito alguno, tratando de no alejarme demasiado del campamento."


translate spanish day5_sl_89eb1213:


    "Después de todo, había posibilidades de encontrarme con Ulyana por ahí, y ese desastre natural sería probablemente incluso peor que la líder del campamento."


translate spanish day5_sl_4d903dc8:


    "Me senté en un árbol caído y comencé a reflexionar."


translate spanish day5_sl_905d8f87:


    "¿Por qué me está sucediendo todo esto a mi? ¿Por qué continuo hallándome en situaciones estúpidas, en cada momento y en cualquier parte?"


translate spanish day5_sl_f58b1a89:


    th "Incluso habiéndome materializado repentinamente en un extraño campamento de pioneros en medio de la nada, no tengo porqué convertirme en un sujeto de experimentación, en una víctima de una mente cósmica enferma o en un participante en la guerra intergaláctica del lado de un grupo de pacifistas propensos al suicidio, como el héroe habitual de ciencia ficción."


translate spanish day5_sl_013538e9:


    th "No, me tengo que esconder en un bosque nocturno ocultándome de una delirante líder de campamento y sus pioneros estajanovistas..."


translate spanish day5_sl_99250f31:


    "Las estrellas en el cielo estaban brillando."


translate spanish day5_sl_d9e32f7f:


    th "Quizá no sólo den su luz a este campamento y a mi, sino que también brillan en la ciudad donde nací, y donde mi antiguo hogar está..."


translate spanish day5_sl_1b24971b:


    "Era como si un dolor se hubiera arraigado en mi pecho."


translate spanish day5_sl_3ef36634:


    "Visualicé mi viejo piso claramente, y un detestable ardor empezó a surgir de mi estómago hacia mi garganta."


translate spanish day5_sl_86074a46:


    "No, no era melancolía.{w} Era más bien como una reminescencia triste."


translate spanish day5_sl_30f40a77:


    "Porque a pesar de todo lo ocurrido, me siento más vivo aquí en menos de cinco días, que allí durante los últimos años."


translate spanish day5_sl_d5ded2d1:


    "Ahora no estaba realmente tan seguro de querer volver."


translate spanish day5_sl_87600ecf:


    "Solamente una cuestión me atormentaba todavía... cómo y por qué acabé aquí.{w} Resplandecía en mi mente de nuevo."


translate spanish day5_sl_e3fa1442:


    "Últimamente, no me he pasado mucho tiempo buscando respuestas o incluso solamente pensar en mi actual situación."


translate spanish day5_sl_cba9268c:


    "Mis pensamientos estaban ocupados con el día a día, con la rutina diaria."


translate spanish day5_sl_2e35c234:


    "Y ahora, con el fin de acabar con ello y ser capaz de desear quedarme aquí por gusto, necesito comprender la naturaleza de este lugar."


translate spanish day5_sl_6534fa35:


    "Es solamente que incluso el ruiseñor en una cárcel de oro, tiene derecho a saber cómo y por quiénes está prisionero ahí.{w} Y tras eso, tomar la decisión tanto si se queda como si no..."


translate spanish day5_sl_7e84e611:


    "Probablemente, me habría perdido en mis pensamientos existencialistas durante un largo tiempo, pero escuché voces acercándose... Olga Dmitrievna y los pioneros."


translate spanish day5_sl_a025a4ba:


    th "Parece que están continuando con su excursión..."


translate spanish day5_sl_b7831d31:


    "Me dirigí al campamento con ritmo veloz."


translate spanish day5_sl_20e62a68:


    th "No tenía deseo de volver a la cabaña de la líder del campamento porque seguramente sería regañado, y tan sólo estar esperando por ello es mucho peor."


translate spanish day5_sl_607f0110:


    "Llegué a la playa bastante pronto."


translate spanish day5_sl_5f1f337b:


    "Y no estaba solo aquí... un uniforme de pionero estaba tirado en la arena.{w} Uno de muchacha..."


translate spanish day5_sl_4048e221:


    "Pero nadie parecía estar nadando en el agua."


translate spanish day5_sl_274f5b11:


    "Casi estuve a punto de pensar que era algún tipo de brujería, cuando de repente escuché una voz detrás de mí."


translate spanish day5_sl_6a030984:


    sl "¿Saltándote la excursión?"


translate spanish day5_sl_51149e68:


    me "..."


translate spanish day5_sl_19be3aa3:


    sl "Pensé que estabas todavía en el bosque."


translate spanish day5_sl_74e314f2:


    "Me giré y vi a Slavya en bikini."


translate spanish day5_sl_ec56f2f6:


    me "Lo siento, supongo que te he interrumpido."


translate spanish day5_sl_c3af5591:


    sl "No pasa nada, ya casi estoy."


translate spanish day5_sl_0bcdd9c2:


    me "¿Por qué te fuiste a nadar durante la noche?"


translate spanish day5_sl_bea64ff8:


    sl "Oh bueno, ¿está prohibido?"


translate spanish day5_sl_d1046a49:


    "Sonrió."


translate spanish day5_sl_ff30fec3:


    me "Bueno, no...{w} Es sólo que...{w} ¿No le importará a Olga Dmitrievna que te fueras tan pronto...?"


translate spanish day5_sl_ddc20430:


    sl "¡Pero tú hiciste lo mismo!"


translate spanish day5_sl_72b67782:


    "Slavya me echó una mirada fugaz."


translate spanish day5_sl_fe32f248:


    me "Claro que lo hice..."


translate spanish day5_sl_3be00ada:


    "Me senté en la arena y me quedé mirando fijamente el río."


translate spanish day5_sl_9cd3b7aa:


    me "No te gustó la excursión, ¿verdad?"


translate spanish day5_sl_d22b7e79:


    sl "No, no es eso...{w} Solamente quería estar sola un rato."


translate spanish day5_sl_a21bded2:


    me "Y te he interrumpido."


translate spanish day5_sl_97ab3600:


    sl "No, realmente no."


translate spanish day5_sl_4a2c94f6:


    me "No sueles ser así."


translate spanish day5_sl_b31d38bf:


    sl "¿De qué hablas?"


translate spanish day5_sl_dbe426b0:


    me "Bueno, irte así sin más..."


translate spanish day5_sl_51aa3969:


    sl "Bueno, no soy un robot que solamente pueda actuar de acuerdo a un programa predefinido."


translate spanish day5_sl_ec2c2a7c:


    "Ella se puso a reír."


translate spanish day5_sl_078b05d1:


    me "Claro, eso es cierto..."


translate spanish day5_sl_3ca9afdd:


    "Aun estaba desconcertado, y mi fatiga se volvía más y más fuerte."


translate spanish day5_sl_6615be77:


    "Mi mente estaba completamente en blanco."


translate spanish day5_sl_99ab0d84:


    "Para ser más exactos, mi cabeza estaba tan espesa que no había lugar en ella para desarrollar ideas."


translate spanish day5_sl_e447cec9:


    "Si comparase mi cerebro en sus mejores tiempos con una gran autopista llena de pensamientos veloces circulando por ahí, persiguiéndose unos a otros y causando accidentes graves, ahora era más bien un diminuto sendero en un distante y desolado bosque, el cual es solamente usado en tiempos de necesidad."


translate spanish day5_sl_0458ec4c:


    "Así que dije primero lo que se me pasó por la mente:"


translate spanish day5_sl_00992d89:


    me "¿No crees que todo aquí es muy extraño?"


translate spanish day5_sl_706bd1ea:


    sl "¿Extraño?"


translate spanish day5_sl_15ae2aa5:


    me "Todo lo que sucede aquí.{w} Es el modelo ideal de un campamento de pioneros."


translate spanish day5_sl_51310c74:


    me "Evidentemente, no es que yo supiera mucho sobre ellos, pero es exactamente tal cual lo imaginé."


translate spanish day5_sl_b31d38bf_1:


    sl "¿De qué hablas?"


translate spanish day5_sl_f3118ca2:


    "Slavya me miraba perpleja."


translate spanish day5_sl_143220ed:


    me "¿No has sentido nunca como si no pertenecieras al lugar donde estás?"


translate spanish day5_sl_311a3a2f:


    sl "No sé."


translate spanish day5_sl_f4af8a55:


    me "Para ser más precisos, no al que estás destinado a estar.{w} Sino que es como si estuvieras a miles de kilómetros alejado del hogar o incluso en otra galaxia."


translate spanish day5_sl_fb1ab657:


    sl "No te comprendo..."


translate spanish day5_sl_042de057:


    me "Estamos igual en esto..."


translate spanish day5_sl_5653f3b3:


    "Me tumbé sobre mi espalda y miré a las estrellas."


translate spanish day5_sl_b18f0221:


    me "¿Qué pensarías de mí si te dijera que soy un extranjero del futuro?"


translate spanish day5_sl_33d5edca:


    sl "¿Tú?"


translate spanish day5_sl_a0cddebc:


    "Preguntó Slavya con el semblante muy serio."


translate spanish day5_sl_399fd2dd:


    me "Bueno, asumamos que lo soy.{w} ¿Cómo debería retornar a mi época?"


translate spanish day5_sl_8068cf2d:


    sl "¿Realmente quieres?"


translate spanish day5_sl_5dcae275:


    th "Está claro que todas las conversaciones con ella que tienen que ver con mi situación, incluso la más mínima idea sobre ello, siempre acaban terminando de la misma forma."


translate spanish day5_sl_02fb630c:


    th "Es como si me estuviera ofreciendo una posibilidad de quedarme.{w} Casi como insistiendo en ello."


translate spanish day5_sl_5bb9832a:


    me "Bueno, digamos que no estoy muy seguro.{w} {i}Allí{/i}, como podríamos llamarle, todo es como si fuera mi hogar para mí...{w} O, mejor, más familiar."


translate spanish day5_sl_ba3caafa:


    me "Prácticamente todo me resulta conocido y estoy preparado para cualquier situación."


translate spanish day5_sl_9d70ab7f:


    me "Y aquí es exactamente todo lo contrario, literalmente cada cosa por pequeña que sea es como una sorpresa. Y todo es...{w} diferente."


translate spanish day5_sl_863dcd94:


    sl "¿Y eso es malo?"


translate spanish day5_sl_56eeb493:


    me "No lo llamaría malo...{w} Desconocido. Confuso.{w} A veces puede ser difícil cambiar algo. Especialmente para gente con mi carácter."


translate spanish day5_sl_83493bf7:


    sl "Pero, ¿qué es lo que quieres?"


translate spanish day5_sl_51fffd9d:


    me "Para empezar, no puedo responder a dicha pregunta hasta que no sepa exactamente dónde es «aquí»."


translate spanish day5_sl_aaa90d0e:


    sl "¡Pues ve y búscalo!"


translate spanish day5_sl_3b021425:


    me "Si tan sólo fuera así de fácil..."


translate spanish day5_sl_0b1c1ea9:


    sl "Pero, ¿qué tiene de difícil?"


translate spanish day5_sl_88ed05d2:


    me "¡Todo es difícil!{w} No sé ni por dónde empezar...{w} ¡Y constantemente estoy siendo distraído!"


translate spanish day5_sl_84f92ed7:


    sl "Hablas de ello muy seriamente, ¡es como si fuera todo de verdad!"


translate spanish day5_sl_ec2c2a7c_1:


    "Se rio."


translate spanish day5_sl_183aaa3f:


    me "Quién sabe..."


translate spanish day5_sl_c95262b6:


    "Hubo un largo silencio a continuación.{w} Repentinamente, Slavya estornudó."


translate spanish day5_sl_1beeb1c2:


    me "Salud."


translate spanish day5_sl_309686e8:


    sl "¡Gracias!"


translate spanish day5_sl_d0e6cc2c:


    me "No fue buena idea salir a nadar por la noche. Podrías enfermar. Date prisa y vete a tu cabaña, hace frío aquí fuera."


translate spanish day5_sl_c393724a:


    sl "No es nada, mejor me quedo sentada aquí contigo un poquito más.{w} Ahora me visto."


translate spanish day5_sl_fedfb810:


    "Bueno, estaba ciertamente halagado, pero..."


translate spanish day5_sl_85c017d1:


    me "Vamos, pasearé contigo."


translate spanish day5_sl_d70f58e2:


    "Pero no dimos ni una docena de pasos, que Slavya se aferró a mi brazo."


translate spanish day5_sl_75ba3164:


    me "¿Qué ocurre?"


translate spanish day5_sl_d161ce4e:


    sl "Oh, de repente me siento algo mareada..."


translate spanish day5_sl_473c8882:


    "Toqué su frente.{w} Estaba ardiendo."


translate spanish day5_sl_de3adeeb:


    "Nunca fui capaz de medir la temperatura del cuerpo con el tacto, pero en este caso era muy obvio."


translate spanish day5_sl_8323f78a:


    me "¡Te lo dije!"


translate spanish day5_sl_1ba9f4d6:


    sl "¡Achús!"


translate spanish day5_sl_a15cb848:


    me "¡Démonos prisa!"


translate spanish day5_sl_4d435875:


    sl "No... Podría contagiar a Zhenya...{w} Escucha, mejor que vayamos a la enfermería."


translate spanish day5_sl_44076421:


    me "¿Y qué harás en la enfermería por la noche sola? ¡No tiene sentido!"


translate spanish day5_sl_06c202f0:


    sl "¡No, claro que lo tiene! Si no me quieres ayudar, ¡iré por mí misma!"


translate spanish day5_sl_2cc25ffd:


    "Soltó mi mano, y estaba preparada para irse."


translate spanish day5_sl_a2051ad9:


    me "No te enfades.{w} Ponte esto encima, ¡que hace frío!"


translate spanish day5_sl_b04f271d:


    "Le dejé mi jersey, el cual me había traído para la excursión."


translate spanish day5_sl_16c00715:


    th "Al fin le encontré algo de utilidad..."


translate spanish day5_sl_309686e8_1:


    sl "¡Gracias!"


translate spanish day5_sl_6275b191:


    "Se puso el jersey encima y me hizo una mirada tierna y dulce, que ya no pude discutir más con ella."


translate spanish day5_sl_9fbebf7b:


    me "Vale está bien, a la enfermería pues, ¡tal como dijiste!"


translate spanish day5_sl_02e4ba3b:


    "Cinco minutos después estábamos frente a la puerta de la enfermería, y Slavya estaba cogiendo la llave correcta."


translate spanish day5_sl_a22a6123:


    "Todavía consideraba esta idea bastante tonta.{w} Nadie se muere por un resfriado común."


translate spanish day5_sl_84768b52:


    "Al menos no en el siglo pasado."


translate spanish day5_sl_097ff7d4:


    "No podía hallar una buena razón como para pasar la noche en la enfermería."


translate spanish day5_sl_3a4dbc67:


    "Finalmente Slavya abrió la puerta y se apoyó en mi brazo."


translate spanish day5_sl_36203cca:


    sl "Todavía estoy un poquito mareada."


translate spanish day5_sl_e5045fc4:


    "Dijo culpablemente."


translate spanish day5_sl_4461e513:


    "Slavya se dejó caer en la cama, me senté en la silla de al lado."


translate spanish day5_sl_48f0ab25:


    me "¡Aun así, escucha! Sola durante toda la noche en la enfermería..."


translate spanish day5_sl_7da35453:


    th "A fin de cuentas, al menos Zhenya podría darte un vaso de agua, si necesitaras uno.{w} Ella no se enfermaría... es joven y fuerte."


translate spanish day5_sl_7f80fc24:


    sl "Está bien.{w} No quiero molestar a nadie. Y la enfermera vendrá mañana."


translate spanish day5_sl_3d2e3b21:


    "De repente me imaginé a mí mismo en el lugar de Slavya. Imaginé que tendría que pasarme toda la noche, solo..."


translate spanish day5_sl_460094c3:


    "Y escalofríos recorrieron mi espalda."


translate spanish day5_sl_3deaf8e7:


    me "Escucha, tal vez pueda quedarme contigo durante un rato..."


translate spanish day5_sl_a3c61bd4:


    "Le debía mucho a Slavya.{w} De hecho, tampoco me quería ir."


translate spanish day5_sl_afee0832:


    sl "¿Para qué?{w} Todo estará bien. Gracias por acompañarme hasta aquí. Deberías irte a dormir."


translate spanish day5_sl_aa120605:


    me "Aun así creo que..."


translate spanish day5_sl_e9c90895:


    sl "No pasa nada."


translate spanish day5_sl_353eced4:


    "Por un instante pensé..."


translate spanish day5_sl_82680eaa:


    "Desde luego, nada terrible iba a ocurrirle aquí, pero me sentiría más relajado si me quedara con ella."


translate spanish day5_sl_9fbe4547:


    me "Creo que..."


translate spanish day5_sl_2b08672e:


    sl "Oh, venga ya."


translate spanish day5_sl_0d7f306b:


    "Slavya se quejó, como si se ofendiera."


translate spanish day5_sl_9f3f4e67:


    me "No me echarás fuera, ¿verdad?"


translate spanish day5_sl_2e9b0de6:


    "Le sonreí tímidamente."


translate spanish day5_sl_a8c6f6ab:


    sl "Está bien. Pero si te contagias, ¡será culpa tuya!"


translate spanish day5_sl_7d705a39:


    "Estaba feliz de mi pequeña victoria."


translate spanish day5_sl_96286888:


    sl "Vale pues, ¿qué hacemos?"


translate spanish day5_sl_9a9c2e41:


    "Deberían haber algunas cartas para jugar en algún cajón."


translate spanish day5_sl_14a83ddb:


    me "¿Durak?"


translate spanish day5_sl_bf89e18e:


    "De hecho, no conocía otros juegos salvo este y el póker."


translate spanish day5_sl_b58c757b:


    sl "Vale."


translate spanish day5_sl_403a4e5b:


    "Pasamos mucho tiempo jugando.{w} Perdí completamente la noción del tiempo."


translate spanish day5_sl_5a3cb568:


    "Charlamos y reímos mucho."


translate spanish day5_sl_653470e9:


    "Slavya no parecía estar tan enferma."


translate spanish day5_sl_f248ccf9:


    "Y al fin la medianoche vino..."


translate spanish day5_sl_1bd398ca:


    sl "Hora de dormir."


translate spanish day5_sl_6ded9a67:


    me "Probablemente..."


translate spanish day5_sl_70dd0c01:


    sl "Pero, ¿adónde irás?"


translate spanish day5_sl_8fb93608:


    "Mi miró fijamente."


translate spanish day5_sl_51906baa:


    sl "Venga ya.{w} No deberías quedarte ahí sentado conmigo."


translate spanish day5_sl_b550b4b7:


    me "Puedo hacerlo de todas formas..."


translate spanish day5_sl_617f93da:


    th "En ese momento podía hasta dormirme de pie."


translate spanish day5_sl_848c7c43:


    "Slavya me observó durante un rato, y luego corrió la cortina y dijo:"


translate spanish day5_sl_df84d49a:


    sl "¡Buenas noches!"


translate spanish day5_sl_8de47ec6:


    me "Igualmente."


translate spanish day5_sl_b551d5e5:


    "Bajé mi cabeza apoyándola en mis brazos y me dormí al instante."


translate spanish day5_sl_56adc4c2:


    "A pesar de estar exhausto, dormí inquieto.{w} Era más bien a duermevela."


translate spanish day5_sl_c443185a:


    "Mi conciencia se mantenía yéndose y viniendo una y otra vez."


translate spanish day5_sl_b0ea066f:


    "El mejor estado posible para soñar algo psicodélico..."


translate spanish day5_sl_c118b5ae:


    "La línea de autobús número 410 circulaba atravesando la habitación, estaba nevando desde el techo y el monitor de mi computadora surgió de repente en medio de la mesa."


translate spanish day5_sl_196f6c57:


    "Las paredes medio empapeladas con papel pintado, montones de porquería surgían por el suelo y mi vieja cama se materializó en la esquina."


translate spanish day5_sl_d7272335:


    "Estaba en mi viejo piso otra vez."


translate spanish day5_sl_7fc90acf:


    "El escenario cambió al que era mi hogar en la ciudad."


translate spanish day5_sl_bcd4a1a8:


    "Era como si estuviera corriendo a través de hordas eternas de personas, atascándome constantemente."


translate spanish day5_sl_3624e130:


    "Y entonces las tinieblas se cernieron sobre mí.{w} Las tinieblas primigenias del universo."


translate spanish day5_sl_b5fa9f77:


    "Salté apoderado por el terror.{w} Estaba empapado en sudor frío como si hubiera vertido un río entero."


translate spanish day5_sl_fe12675a:


    "Slavya me estaba mirando preocupada desde la cama."


translate spanish day5_sl_60108fae:


    me "Sólo fue una pesadilla..."


translate spanish day5_sl_15aba1cf:


    "Itenté sonreír."


translate spanish day5_sl_1c6cbe1a:


    sl "Estabas gritando..."


translate spanish day5_sl_6c7c2069:


    me "¿Qué gritaba?"


translate spanish day5_sl_f9a31b17:


    sl "No lo sé.{w} Era incomprensible."


translate spanish day5_sl_8be56802:


    "El reloj sólo marcaba las doce y media."


translate spanish day5_sl_11adbbfc:


    sl "Semyon."


translate spanish day5_sl_b6626098:


    me "¿Ah?"


translate spanish day5_sl_5ef3b66e:


    sl "Tengo frío..."


translate spanish day5_sl_564a8986:


    "Slavya se frotaba con los brazos su cuerpo envuelta en el jersey, temblando."


translate spanish day5_sl_a0d2cda7:


    me "Aguanta, buscaré una sábana."


translate spanish day5_sl_2b495447:


    "Busqué armario tras armario, jurando y perjurando para mí mismo como la maldita enfermería no tenía nada para cubrir a sus pacientes."


translate spanish day5_sl_6ab83d4c:


    me "Lo siento, no hay nada aquí..."


translate spanish day5_sl_640c2a39:


    me "Espera, saldré corriendo a por alguna cosa."


translate spanish day5_sl_97419f60:


    sl "No tienes por qué hacerlo...{w} ¿Te tumbarías justo aquí conmigo? Estaré más calentita."


translate spanish day5_sl_c849a05b:


    "Lo estaba diciendo completamente en serio."


translate spanish day5_sl_68527d19:


    "Su voz sonó tan lastimosa, que por un segundo me dejó sin aliento."


translate spanish day5_sl_22609679:


    me "¿Estás segura de que...{w} eso...{w} es correcto?"


translate spanish day5_sl_ed91b7e2:


    sl "¿Acaso no lo es?"


translate spanish day5_sl_07ed6d6a:


    "Me preguntó, ruborizada."


translate spanish day5_sl_7edd72b2:


    me "No, porque..."


translate spanish day5_sl_d3c57f8c:


    "Lentamente me quité mis zapatos y me tumbé justo en el borde de la cama, tratando de ocupar cuanto menos espacio fuera posible, tratando de encogerme y de detener la respiración."


translate spanish day5_sl_f211b57a:


    "Slavya me abrazó suavemente y dejó descansar su cabeza en mi pecho."


translate spanish day5_sl_c61518ac:


    sl "Sí, mucho mejor."


translate spanish day5_sl_2481134c:


    "Ronroneó."


translate spanish day5_sl_6d3a10e3:


    "Era incapaz de decir algo, así que me quedé ahí tumbado tranquilamente."


translate spanish day5_sl_eb090c68:


    me "Muy bien..."


translate spanish day5_sl_00a08a43:


    sl "Gracias."


translate spanish day5_sl_9508d41b:


    me "¿Por qué?"


translate spanish day5_sl_f0f90483:


    "Slavya mantuvo sus ojos cerrados y parecía preparada para caer dormida en cualquier segundo."


translate spanish day5_sl_bca81bd5:


    sl "Por estar aquí."


translate spanish day5_sl_9a94410c:


    me "No es nada..."


translate spanish day5_sl_d663f86e:


    th "Hace tan solamente dos minutos atrás, ella estaba desesperada tratando de deshacerse de mí."


translate spanish day5_sl_fc7dc843:


    sl "No, de verdad."


translate spanish day5_sl_b320bfee:


    me "Pues nada, no hay de qué."


translate spanish day5_sl_7a6acecb:


    "No dijo nada, pero estaba seguro de que Slavya sonrió."


translate spanish day5_sl_77f7eb26:


    me "¡Póngase en contacto en cualquier momento, como suelen decir!"


translate spanish day5_sl_c5ee54e6:


    sl "Eres muy amable, te preocupas mucho."


translate spanish day5_sl_1a49756a:


    me "Bueno, lo intento."


translate spanish day5_sl_a0e4fdf2:


    sl "Eres un buen amigo."


translate spanish day5_sl_d1052636:


    me "¿Amigo? Bueno, claro, probablemente..."


translate spanish day5_sl_29a4e2ac:


    "De alguna forma esa palabra me golpeó dolorosamente."


translate spanish day5_sl_3b03b28d:


    "Por otra parte, no había motivo para creer que era algo más que simplemente un amigo para Slavya..."


translate spanish day5_sl_a2344f49:


    th "De todas formas, ¿a qué vienen estos pensamientos?"


translate spanish day5_sl_df7e98e4:


    me "Tú también..."


translate spanish day5_sl_954ffd8b:


    "No podía encontrar las palabras idóneas."


translate spanish day5_sl_7411ac41:


    th "¿Qué ha sido eso de «Tú también»?{w} ¿Ha sido un placer tratar contigo, esperamos que en un futuro haya una cooperación de beneficio mutuo?"


translate spanish day5_sl_ada6764b:


    th "O... claro, es divertido ser tu amigo también, ¿vamos a jugar en el arenero?"


translate spanish day5_sl_ecd07382:


    "Esto era el doble de duro de aguantar teniendo a Slavya tumbada justo a mi lado... No puedo correr, no puedo esconderme e incluso aunque no me mire a los ojos, sé que ella me ve a través de ellos."


translate spanish day5_sl_a549e991:


    sl "Estás angustiado por algo, ¿no?"


translate spanish day5_sl_98db6f67:


    me "No, estoy bien."


translate spanish day5_sl_ef63c01d:


    sl "Pero puedo decirte que lo estás."


translate spanish day5_sl_2d91a0c5:


    me "Bueno, tal vez..."


translate spanish day5_sl_5c6c0b54:


    sl "¿Quieres hablar de ello?"


translate spanish day5_sl_99ee26fe:


    me "Realmente, no... De hecho, no hay nada de qué hablar."


translate spanish day5_sl_45f565d7:


    sl "Entonces, ¿realmente no sabes qué te molesta?"


translate spanish day5_sl_dab7f3be:


    me "Claro, será eso probablemente."


translate spanish day5_sl_e47336fd:


    sl "Pues entonces, en primer lugar, ¿por qué estás molesto?"


translate spanish day5_sl_c954d69f:


    "Slavya se rio."


translate spanish day5_sl_3ae34e17:


    me "Supongo que no debería."


translate spanish day5_sl_dbfe4f1e:


    sl "¡Qué bien!"


translate spanish day5_sl_6dd013b2:


    "Ella me abrazó aun más fuertemente."


translate spanish day5_sl_970eac9a:


    me "¿Ya no tienes frío?"


translate spanish day5_sl_ad99cfcc:


    sl "Estoy bien. Solamente dime si no estás a gusto."


translate spanish day5_sl_5c1a0ecc:


    me "No, estoy bien."


translate spanish day5_sl_ed54fb3b:


    sl "¿Como aquella vez? ¿En el barco?"


translate spanish day5_sl_1339e6eb:


    "Mis músculos, como si se acordaran de aquella competición de remos, me dieron recuerdos de aquello con un leve dolor."


translate spanish day5_sl_b21be332:


    me "Es completamente diferente, no es por lo mismo."


translate spanish day5_sl_8c47f676:


    sl "Vale, vale."


translate spanish day5_sl_044e54c2:


    "Dijo tímidamente y alzó su cabeza un poquito."


translate spanish day5_sl_b78463d7:


    sl "Entonces, ¿todo va bien?"


translate spanish day5_sl_f0ccc081:


    me "Claro, perfectamente."


translate spanish day5_sl_719f7d6e:


    "Empecé a preocuparme. Estaba seguro de que Slavya quería algo de mí."


translate spanish day5_sl_0793c467:


    "Quería que hiciera o dijera alguna cosa."


translate spanish day5_sl_bd971270:


    th "Pero, ¿qué...?"


translate spanish day5_sl_24ef8389:


    sl "¿Estás seguro?"


translate spanish day5_sl_626d142f:


    me "Claro...{w} Probablemente."


translate spanish day5_sl_440927ac:


    sl "Pues bien... ¡Que tengas buenas noches!"


translate spanish day5_sl_3c5ee540:


    me "Igualmente."


translate spanish day5_sl_a20cefa7:


    "..."


translate spanish day5_sl_a7b8346c:


    "Tras un rato, se durmió."


translate spanish day5_sl_dbce3303:


    "Comprobé el reloj con el rabillo del ojo.{w} Era solamente la una de la madrugada, eso significa que aun debía quedarme tumbado en esta posición hasta el amanecer."


translate spanish day5_sl_0dd8c20c:


    "No estaba muy preocupado por mi brazo entumeciéndose."


translate spanish day5_sl_dd2a66dd:


    "Solamente, estando tan próximo a Slavya, tan peligrosamente cerca, no podía dormir para nada."


translate spanish day5_sl_39c41ef8:


    "Diversos pensamientos invadían mi cabeza."


translate spanish day5_sl_c93104db:


    th "¿Realmente consideraba toda esta situación como normal...?"


translate spanish day5_sl_a20cefa7_1:


    "..."


translate spanish day5_sl_9719a2d8:


    "Justo cuando estaba pensando lo malo que sería si alguien nos encontrara, la puerta se abrió, la luz se encendió y Olga Dmitrievna entró deprisa."


translate spanish day5_sl_ede80479:


    "Mi corazón dio un vuelco y se detuvo por un instante."


translate spanish day5_sl_96da4eb5:


    mt "Semyon..."


translate spanish day5_sl_9142f8f6:


    "Comenzó la líder del campamento con una voz diabólicamente calmada."


translate spanish day5_sl_b51939f9:


    mt "Te he estado buscando en todas partes, y tú... tú te escapaste del bosque antes de tiempo, no viniste a la cabaña a dormir y, ahora, ¡estás seduciendo a nuestra mejor pionera!"


translate spanish day5_sl_5e2bbb18:


    "Slavya se despertó al instante."


translate spanish day5_sl_3872252b:


    "Ella se quedó mirando fijamente a la líder del campamento con unos ojos soñolientos por unos segundos, y entonces comprendió instantáneamente qué ocurría y saltó de la cama."


translate spanish day5_sl_110c77e8:


    sl "¡Olga Dmitrievna! ¡No es lo que piensas! Sólo estaba enferma y Semyon me acompañó aquí."


translate spanish day5_sl_764cea7c:


    sl "Y luego tuve frío y...{w} Le dije que se fuera a su cabaña."


translate spanish day5_sl_a4985f09:


    mt "Oh, claro, ¡seguro! Así que estás enferma, ¿no?"


translate spanish day5_sl_7b8fbab7:


    sl "Lo estoy..."


translate spanish day5_sl_4aa379e0:


    "Contestó humildemente Slavya."


translate spanish day5_sl_dc4f5852:


    mt "¡Pues entonces tendrás tratamiento!"


translate spanish day5_sl_bcebe051:


    "Me observó amenazadoramente."


translate spanish day5_sl_c2745a0c:


    mt "¡Y tú! ¡Levántate y sígueme!"


translate spanish day5_sl_a7b2aa21:


    "Comprendí que discutir no era una opción, abandoné la enfermería, no sin antes mirar a Slavya."


translate spanish day5_sl_3e2db886:


    "Alcanzamos la plaza sin cruzar palabra alguna."


translate spanish day5_sl_fb0b3f24:


    "Olga Dmitrievna me miró con saña y me dijo:"


translate spanish day5_sl_d702b0ee:


    mt "¡Hora de irte a casa! Necesito tener una conversa seria contigo."


translate spanish day5_sl_f725c780:


    "Durante todo el camino me sentía muy mal."


translate spanish day5_sl_8551a9d4:


    "La líder del campamento me seguía silenciosamente."


translate spanish day5_sl_0a646630:


    "Creo que una vez más, me vi involucrado en una situación estúpida, la cual sería percibida de forma totalmente equivocada por todos."


translate spanish day5_sl_68e9f070:


    th "Ah... no hay nada que discutir..."


translate spanish day5_sl_7dbd4345:


    "¿Y si estuviera en el lugar de Olga Dmitrievna?{w} Dos adolescentes acurrucados en una enfermería vacía... por la noche.{w} Es difícil tener una buena excusa."


translate spanish day5_sl_25df23e4:


    "Lo peor de todo, ¡es que estas cosas siempre me ocurren específicamente a mí!"


translate spanish day5_sl_ce7660c4:


    "Siempre consideré que tengo una mente analítica. Sin embargo, raramente la pongo en la práctica."


translate spanish day5_sl_f40fe17c:


    "Entramos en la cabaña y la líder del campamento continuó con el interrogatorio."


translate spanish day5_sl_8237174c:


    mt "¿Tendrías la amabilidad de explicarte?"


translate spanish day5_sl_49ae3aab:


    "Parecía que se había calmado un poco."


translate spanish day5_sl_8ba11b89:


    "Por lo menos esa parte de su carácter era atractivo. Era, de igual modo, una mujer impulsiva, pero también se le calmaban los ánimos rápidamente."


translate spanish day5_sl_512994b2:


    me "Slavya ya te lo dijo todo..."


translate spanish day5_sl_43521b0d:


    mt "¿Esperas que me crea eso?"


translate spanish day5_sl_49fe0ea8:


    me "No puedo obligarte a que lo creas, pero no voy a inventarme ninguna excusa tampoco, porque es la verdad."


translate spanish day5_sl_5fb08a35:


    "Me miró por un rato, y luego dijo:"


translate spanish day5_sl_5be2bfc3:


    mt "Mañana decidiré qué hago con vosotros dos."


translate spanish day5_sl_9f74c45c:


    "La líder del campamento apagó la luz, me acurruqué en las sábanas, sin siquiera molestarme en quitarme la ropa y me giré de cara a la pared."


translate spanish day5_sl_68e31fbe:


    th "Es interesante, ¿por qué me culpa solamente a mi de lo ocurrido?{w} Después de todo, no estaba solo."


translate spanish day5_sl_5e211841:


    th "Slavya es un ejemplo modélico de pionera, pero..."


translate spanish day5_sl_bfacbe72:


    "No, no intento echarle las culpas a ella. Simplemente me siento demasiado avergonzado por verme en una situación tan tonta una vez más."


translate spanish day5_sl_d8ca9160:


    "Involuntariamente, sin malos pensamientos, sin esperar ninguna trampa..."


translate spanish day5_sl_53374338:


    "Podría haber estado ahí compadeciéndome por un buen rato, pero la fatiga se apoderó de mi, y me desvanecí."


translate spanish day5_us_8f2500e8:


    "Ni me acordé de Ulyana esa tarde."


translate spanish day5_us_4754cc34:


    th "Aquello con el pastel...{w} Como si nunca hubiera ocurrido..."


translate spanish day5_us_d4e9d4ea:


    "A pesar de todo, recordaba su cara resentida y decepcionada."


translate spanish day5_us_760ba5e1:


    "Tal vez en otra ocasión nunca habría escogido acercarme a ella, pero ahora surgió una buena razón para abandonar este lugar y terminar esta estúpida excursión."


translate spanish day5_us_afda6c4c:


    "Recordando lo que Olga Dmitrievna me dijo de estar preparado para nuevas tareas, decidí no pedirle permiso y, tras escoger un momento adecuado, desaparecí en el bosque."


translate spanish day5_us_d6e969ac:


    "La noche cayó sobre el campamento."


translate spanish day5_us_fbf53d12:


    "Mentalmente le agradecí a la líder del campamento por no buscar a fondo, ya que eso significaba que la civilización estaba a tan sólo unos pocos cientos de metros de lejos."


translate spanish day5_us_94c6d083:


    "Otra larga caminata a través del bosque nocturno no estaba en mis planes."


translate spanish day5_us_57b7303c:


    "Pronto llegué a la plaza y dudaba."


translate spanish day5_us_6f52cdaf:


    th "¿No parece estúpido?"


translate spanish day5_us_b54027f4:


    th "¿Qué le diría a Ulyana?{w} Además, en primer lugar, ¿por qué iría a verla?"


translate spanish day5_us_4b80dcb9:


    "Mi cabeza estaba tan espesa que no había lugar en ella para desarrollar ideas."


translate spanish day5_us_e447cec9:


    "Si comparase mi cerebro en sus mejores tiempos con una gran autopista llena de pensamientos veloces circulando por ahí, persiguiéndose unos a otros y causando accidentes graves, ahora era más bien un diminuto sendero en un distante y desolado bosque, el cual es solamente usado en tiempos de necesidad."


translate spanish day5_us_cd65d7a1:


    "Así que es mejor no pensar más en ello y solamente actuar."


translate spanish day5_us_5365a619:


    th "Pero, ¿adónde debería ir?{w} ¿Tal vez a su cabaña?{w} ¿Y si no está allí?"


translate spanish day5_us_cb5fdfc2:


    "De repente, algo me tiró al suelo y caí en el asfalto.{w} Suerte que estiré los brazos a tiempo, de otra manera me habría roto la nariz..."


translate spanish day5_us_759e5578:


    us "¡Te pillé!"


translate spanish day5_us_447bdfb5:


    "Rápidamente me incorporé del suelo y vi a Ulyana frente a mí."


translate spanish day5_us_1b093c3a:


    me "¡¿Qué haces?!"


translate spanish day5_us_88297410:


    "No podía dejar de gritarle, a pesar de mis mejores esfuerzos."


translate spanish day5_us_259f22bf:


    us "¡No deberías distraerte!"


translate spanish day5_us_2f6a59ef:


    "Respondió con saña."


translate spanish day5_us_056f2d76:


    me "¿Y si me hubiera roto la nariz? ¿O me hubiera roto el brazo?"


translate spanish day5_us_78d01ae3:


    "Le dije más calmado."


translate spanish day5_us_7de3b85d:


    us "Bueno, pero nada de eso sucedió."


translate spanish day5_us_e4e8e714:


    me "Mira, ¿por qué siempre te burlas de mí?"


translate spanish day5_us_515f902a:


    "Ya me había arrepentido de mi decisión de ir a hacerle compañía."


translate spanish day5_us_6ee354fc:


    me "¿No fue el castigo de hoy suficiente para ti? ¿Pudiste sacar alguna simple conclusión?"


translate spanish day5_us_91d3b1af:


    us "Que todo fue culpa tuya."


translate spanish day5_us_2c703153:


    "La sonrisa del rostro de Ulyana desapareció instantáneamente."


translate spanish day5_us_9afe61c9:


    me "¿Qué es exactamente mi culpa?"


translate spanish day5_us_4c55e5c0:


    us "¡Pues todo!"


translate spanish day5_us_590ed0a7:


    me "¿Quieres decir que me culpas siempre?"


translate spanish day5_us_551306e9:


    us "¡Sí!"


translate spanish day5_us_184d37fc:


    "Cruzó sus brazos sobre su pecho y se dio la vuelta."


translate spanish day5_us_eb6f6f93:


    me "Genial..."


translate spanish day5_us_ad3d6bce:


    "De repente quise irme bien lejos de este lugar tan rápido como fuera posible."


translate spanish day5_us_4ef07a9a:


    me "Pero fue por ti que vine aquí..."


translate spanish day5_us_b7ef3111:


    us "¿Qué?"


translate spanish day5_us_2940996c:


    me "Supuse que estarías bastante aburrida, puesto que todos están en la excursión y tú estás aquí sola..."


translate spanish day5_us_7625dabe:


    us "¡Me da igual!"


translate spanish day5_us_1272c1de:


    "Chilló alegremente."


translate spanish day5_us_36434b99:


    us "Bueno, si las cosas están así, ¿qué quieres hacer?"


translate spanish day5_us_149b3064:


    me "¿Qué quieres decir?"


translate spanish day5_us_5db74333:


    us "Pero vamos a ver, ¡pero si has venido por mí!"


translate spanish day5_us_5a38be43:


    "Tal pensamiento no se me había ocurrido."


translate spanish day5_us_1206b412:


    th "Aunque realmente no sé por qué vine aquí..."


translate spanish day5_us_b8ee6c9c:


    me "Tras semejante saludo que hiciste, no me apetece nada."


translate spanish day5_us_457fd234:


    us "¡Pues entonces es asunto mío escoger!"


translate spanish day5_us_f8556bbb:


    "Dijo Ulyana jovialmente y comenzó a pensar."


translate spanish day5_us_36baf507:


    "Me la estuve mirando durante un rato, pero entonces ya no me pude aguantar más:"


translate spanish day5_us_83506e3c:


    me "Escucha, si estás planeando hacer algo otra vez..."


translate spanish day5_us_ba2766af:


    us "¡Ya sé!{w} ¡Vamos a asustar a los demás! Por ejemplo, ¡nos vestiremos de fantasmas! ¡Será muy divertido!"


translate spanish day5_us_9c7c48b0:


    th "No lo encontraba divertido de ningún modo."


translate spanish day5_us_6a2846ee:


    me "Basta ya..."


translate spanish day5_us_532041e9:


    "Comencé agotado, pero paré en seco."


translate spanish day5_us_146e3f57:


    th "Tal vez no sea tan mala idea."


translate spanish day5_us_49be75d5:


    "Después de todo, para cuando crea que ya ha acabado de reunir todo lo que necesite, Olga Dmitrievna y los pioneros ya habrán vuelto."


translate spanish day5_us_b8d157c1:


    "Si vine aquí por Ulyana, pues entonces le seguiré la corriente."


translate spanish day5_us_22634694:


    me "Quizás tengas razón."


translate spanish day5_us_b7ef3111_1:


    us "¿Qué?"


translate spanish day5_us_2dbdc7b7:


    "Me estaba mirando con sus ojos abiertos como platos."


translate spanish day5_us_1b7f7977:


    us "¿Vas a estar de acuerdo así sin más?"


translate spanish day5_us_4bd5340f:


    me "Bueno, ¿qué tiene de malo...?{w} Si insistes..."


translate spanish day5_us_29659e99:


    "Me estudió atentamente por unos cuantos segundos y luego recitó de golpe:"


translate spanish day5_us_7601cc90:


    us "¡Excelente!{w} ¡Pues necesitamos algunas sábanas! Queremos vernos como fantasmas de verdad, ¿no?"


translate spanish day5_us_907b6d20:


    me "Probablemente..."


translate spanish day5_us_03325ca7:


    us "¡Ves a buscarlas!"


translate spanish day5_us_3d4a0aa6:


    "Ulyana proclamó imperiosamente."


translate spanish day5_us_310cd775:


    me "¿Dónde crees que voy a ir a buscarlas?"


translate spanish day5_us_a2c60a47:


    us "Cógelas de tu cabaña, obviamente."


translate spanish day5_us_cdd74e25:


    me "No era tan obvio para mi..."


translate spanish day5_us_acbf1354:


    us "Entonces... ¿es que no quieres?"


translate spanish day5_us_044b5473:


    "Ella hizo al instante una cara triste."


translate spanish day5_us_69eeffd7:


    me "¡Vale, vale!"


translate spanish day5_us_48beb79c:


    "Recordé de hecho que habían ropas para las camas de repuesto en el armario de Olga Dmitrievna, así que tal vez no había por qué preocuparse..."


translate spanish day5_us_a4951fb8:


    "Nos aproximamos a la cabaña de la líder del campamento y le dije a Ulyana:"


translate spanish day5_us_153ceca2:


    me "Espérame aquí."


translate spanish day5_us_34c369b7:


    us "¡A sus órdenes!"


translate spanish day5_us_30bcaace:


    "Recitó de golpe y me saludó."


translate spanish day5_us_d9777aab:


    "Parecía que Ulyana estaba completamente inmersa en el juego."


translate spanish day5_us_900dfc81:


    "En breve hallé dos blancas y límpias sábanas."


translate spanish day5_us_03e5f1cc:


    th "Era una vergüenza que fueran a ser manchadas..."


translate spanish day5_us_71a21743:


    me "Aquí están, cógelas."


translate spanish day5_us_5d11f8d4:


    "Le di las sábanas."


translate spanish day5_us_55a5bd1b:


    us "Es demasiado grande para mí."


translate spanish day5_us_2a2dcaaf:


    "Dijo Ulyana tras darle vueltas en sus manos."


translate spanish day5_us_243ba3e8:


    th "No me extraña, considerando su estatura."


translate spanish day5_us_99c46342:


    me "Dámela."


translate spanish day5_us_3fae3b47:


    "Le di la vuelta doblándola por la mitad y se la retorné."


translate spanish day5_us_e2dcfcce:


    us "Mucho mejor ahora.{w} ¡Sígueme!"


translate spanish day5_us_ef43e763:


    "Se puso la ropa blanca encima de su cabeza y salió corriendo hacia el bosque."


translate spanish day5_us_9e8ba2ed:


    me "¡Espera!"


translate spanish day5_us_69531d20:


    "Me lancé tras ella."


translate spanish day5_us_880b3622:


    "En tan sólo unos pocos minutos más tarde, estábamos cerca de la hoguera de los pioneros, escondidos tras los árboles."


translate spanish day5_us_4e62ae4d:


    "Ahora estaba claro que la broma, inofensiva al principio, tuvo un giro inesperado y desagradable."


translate spanish day5_us_4f51f0fe:


    "Por algún motivo estaba seguro desde un principio de que la excursión terminaría antes de que Ulyana hiciera algo, y ahora estábamos a tan solamente diez metros de los pioneros, vestidos con sábanas. No teníamos aspecto aterrador, más bien cómico."


translate spanish day5_us_8f4b438d:


    us "¡Prepárate! ¡A mi orden!"


translate spanish day5_us_451e8da8:


    me "Espera, espera.{w} En realidad estaba bromeando cuando dije estar de acuerdo...{w} ¡Piénsatelo otra vez! ¡No será nada bueno! ¡Te quedarás castigada bajo arresto hasta el final de la jornada!"


translate spanish day5_us_16b3df32:


    me "Y yo además..."


translate spanish day5_us_f9d44a76:


    us "¡Sin retirada, no hay rendición!{w} ¿Estás preparado? ¡Cuento hasta tres!"


translate spanish day5_us_0f768398:


    "Comencé a contemplar todas las posibles escapatorias en mi cabeza febrilmente.{w} No habían muchas de ellas."


translate spanish day5_us_3f1c9f2d:


    "La primera. Yo y Ulyana salimos corriendo de detrás de los árboles y los pioneros comienzan a reírse, metiéndome en problemas y acabando regañado considerablemente por la líder del campamento, e incluso posiblemente peor."


translate spanish day5_us_31114dd5:


    "Y Ulyana es declarada culpable con la medida más severa posible como castigo bajo la ley del campamento de pioneros."


translate spanish day5_us_3b066783:


    "La segunda. Me quedo aquí y observo junto a los otros como Ulyana corre por el claro con su sábana."


translate spanish day5_us_6d29669b:


    "Se le declara culpable con una medida severa, como en la primera opción, y permanezco relativamente a salvo."


translate spanish day5_us_30ea56d4:


    "La tercera. Hago todo lo posible para prevenir que ella cometa este acto de vandalismo moral y nadie sufre, salvo su autoestima."


translate spanish day5_us_52605bb2:


    "Podría haber ido todo muy bien, pero tanto si esos pensamientos me llevaron más de tres segundos como si Ulyana redució la cuenta, no me incorporé junto a ella a tiempo cuando saltó en el claro, gritando inhumanamente."


translate spanish day5_us_d63ede28:


    "Como era de esperar, todos los pioneros estaban riéndose a carcajadas, alguno incluso se cayó del tronco en el que estaba sentado y comenzó a rodar por el suelo de risa."


translate spanish day5_us_733c66fc:


    "Intenté arreglarlo y grité tan fuerte como pude, de forma que Ulyana me escuchara, pero no los otros:"


translate spanish day5_us_f0a46cde:


    me "¡Tonta! ¡Para ya! ¡Ven aquí!"


translate spanish day5_us_37e33704:


    "No sé si fue porque mi persuasión la convenció, o porque Ulyana comprendió que su actuación había fracasado, pero salió corriendo en mi dirección rápidamente y, sin parar, se escondió en el bosque."


translate spanish day5_us_95318591:


    "No dudé en seguirla."


translate spanish day5_us_d75de63e:


    "Con semejante desenlace existía alguna remota posibilidad de que no fuera castigada otra vez."


translate spanish day5_us_a78b707a:


    "Menos mal que no me uní a esa tragicómica actuación."


translate spanish day5_us_97b3825e:


    th "Ahora tenía que encontrar a Ulyana."


translate spanish day5_us_56432bce:


    "No resultó ser muy difícil, en tanto que no se pudo ir muy lejos."


translate spanish day5_us_db6a8922:


    "Ulyana estaba sentada en un tocón de árbol...{w} llorando."


translate spanish day5_us_70edb764:


    "Me quedé quieto, con dudas."


translate spanish day5_us_73c62008:


    "Desde luego, tal desenlace era de esperar, pero ahora no tenía ni remota idea de qué hacer o de cómo consolarla."


translate spanish day5_us_a3275ce4:


    "Y a parte de eso, volví al campamento intencionadamente y estuve de acuerdo en participar en semejante espectáculo..."


translate spanish day5_us_f8f658a7:


    "Pero, sencillamente, fue mal."


translate spanish day5_us_0e2f3120:


    "Tanto como que estaba cansado, terriblemente cansado."


translate spanish day5_us_311d70c7:


    "En aquel momento, deseaba sencillamente dejar de existir.{w} Simplemente quería cerrar mis ojos y aparecer en cualquier otra parte."


translate spanish day5_us_ce999c85:


    "Preferiblemente en un lugar tranquilo y silencioso."


translate spanish day5_us_67119f08:


    "Pero aquellos sollozos de Ulyana me obligaban a tomar parte."


translate spanish day5_us_c414404b:


    "Caminé hasta ella y me senté en el suelo."


translate spanish day5_us_82d52896:


    me "Y bien, ¿qué esperabas igualmente...?"


translate spanish day5_us_262b65eb:


    "Comencé filosóficamente."


translate spanish day5_us_8d394e00:


    me "Estaba claro que iba acabar así."


translate spanish day5_us_1b26a454:


    us "¡Eres tú el culpable de todo! ¡Tú!"


translate spanish day5_us_5f31ce4d:


    "Ulyana gritó entre lágrimas."


translate spanish day5_us_a508b663:


    me "Está bien, ¿qué habría cambiado si hubiera saltado junto a ti siguiéndote?{w} Ambos habríamos sido el hazmerreír, eso es todo."


translate spanish day5_us_177d555a:


    us "¡Siempre actuas así! ¡Siempre!"


translate spanish day5_us_df7cba70:


    "Su sollozo se volvía más y más grave y, entonces, de repente se abalanzó contra mi y comenzó a aporrearme con sus puños en mi pecho.{w} Los golpes no eran fuertes."


translate spanish day5_us_34d398f8:


    "Eran más bien como un intento de desembarazarse la desesperación que atenazaba a Ulyana, más que un deseo real de golpearme."


translate spanish day5_us_d2af7ffb:


    me "¡Cálmate ya!"


translate spanish day5_us_a0e82e49:


    "Dije firmemente."


translate spanish day5_us_5f0944f0:


    "Ella paró de llorar por un segundo y me abrazó."


translate spanish day5_us_a8064fda:


    us "Quizás nada hubiera cambiado, pero yo me habría sentido mucho más a gusto si lo hubiéramos hecho juntos."


translate spanish day5_us_6fd205af:


    "No sabía qué decir, así que le acaricié su cabeza."


translate spanish day5_us_2e4814b2:


    us "¿Puedo quedarme así un poquito más?"


translate spanish day5_us_10409023:


    me "Sí."


translate spanish day5_us_d624d515:


    "En ese momento, no parecía el reactor nuclear a punto de estallar en la forma de una pequeña muchacha, sino más bien como una hermanita mía que hubiera hecho alguna travesura."


translate spanish day5_us_7cb9453a:


    "No estaba enfadado con ella para nada; por el contrario, me pareció que también me estaba preocupando por como había fracasado su comedia de fantasmas."


translate spanish day5_us_42868526:


    me "Está bien, no pasa nada...{w} ¡Les asustaremos apropiadamente la siguiente vez!"


translate spanish day5_us_c463eeea:


    us "Claro..."


translate spanish day5_us_a38f6822:


    "No sé por cuanto tiempo estuvimos sentados en silencio."


translate spanish day5_us_3dcc62a7:


    "Ulyana dejó de llorar y no quería molestarla, en tanto que se había calmado."


translate spanish day5_us_8f418ac5:


    "Pero tampoco tenía deseos de quedarme en el bosque durante toda la noche."


translate spanish day5_us_2bb4e019:


    me "Ey, volvamos al campamento."


translate spanish day5_us_4d1b85a8:


    "Le sacudí suavemente por los hombros, pero no hubo respuesta."


translate spanish day5_us_17489859:


    "Ulyana se había dormido."


translate spanish day5_us_95efcfe1:


    me "¡Ey!"


translate spanish day5_us_17edece2:


    "La sacudí más fuerte.{w} Sin resultado."


translate spanish day5_us_1161aa63:


    "En aquel momento quise llorar."


translate spanish day5_us_c56aa18d:


    th "¿Por qué siempre me ocurren estas cosas a mi? ¿Por qué continuo hallándome en situaciones estúpidas, siempre y en cualquier parte?"


translate spanish day5_us_f58b1a89:


    th "Incluso habiéndome materializado repentinamente en un extraño campamento de pioneros en medio de la nada, no tengo porqué convertirme en un sujeto de experimentación, en una víctima de una mente cósmica enferma o en un participante en la guerra intergaláctica del lado de un grupo de pacifistas propensos al suicidio, como el héroe habitual de ciencia ficción."


translate spanish day5_us_e29a567a:


    th "No, en vez de eso me tenía que pasar la noche en el bosque con una muchacha pequeña en mis brazos, envuelta en una sábana."


translate spanish day5_us_5c588563:


    th "La próxima vez, mejor tener los experimentos monstruosos..."


translate spanish day5_us_4cc5c895:


    "Me puse en pie y cargué con la adormecida Ulyana en mi espalda."


translate spanish day5_us_8e5b9b1b:


    "Tal vez hubiera alguna forma de despertarla, pero primero, no quería y, segundo, un peso más o menos... llegados a este punto ya daba igual."


translate spanish day5_us_3521c810:


    "Qué bien que por lo menos no pesa..."


translate spanish day5_us_c048facc:


    "En la plaza me detuve, puse a Ulyana en un banco y me estiré junto a ella, totalmente exhausto."


translate spanish day5_us_66f6667a:


    "Incluso una pequeña muchacha es pesada de llevar durante tanto rato."


translate spanish day5_us_99250f31:


    "Las estrellas del cielo estaban brillando."


translate spanish day5_us_d9e32f7f:


    th "Quizá no sólo den su luz a este campamento y a mí, sino que también brillan en la ciudad donde nací, y donde mi antiguo hogar está..."


translate spanish day5_us_1b24971b:


    "Era como si un dolor se hubiera arraigado en mi pecho."


translate spanish day5_us_3ef36634:


    "Visualicé mi viejo piso claramente, y un destestable ardor empezó a surgir de mi estómago hacia mi garganta."


translate spanish day5_us_86074a46:


    "No, no era melancolía.{w} Era más bien como una reminescencia triste."


translate spanish day5_us_30f40a77:


    "Porque a pesar de todo lo ocurrido, me siento más vivo aquí en menos de cinco días, que allí durante los últimos años."


translate spanish day5_us_d5ded2d1:


    "Y ahora no estaba realmente tan seguro de querer volver."


translate spanish day5_us_f0e63108:


    "Solamente una cuestión me atormentaba todavía... cómo y por qué acabé aquí.{w} Resplandecía en mi mente de nuevo."


translate spanish day5_us_e3fa1442:


    "Últimamente, no me he pasado mucho tiempo buscando respuestas o incluso solamente pensar en mi actual situación."


translate spanish day5_us_cba9268c:


    "Mis pensamientos estaban ocupados con el día a día, con la rutina diaria."


translate spanish day5_us_2e35c234:


    "Y ahora, con el fin de acabar con ello y ser capaz desear quedarme aquí por gusto, necesito comprender la naturaleza de este lugar."


translate spanish day5_us_6534fa35:


    "Es solamente que incluso el ruiseñor en una cárcel de oro, tiene derecho a saber cómo y por quiénes está prisionero ahí.{w} Y tras eso, tomar la decisión tanto si se queda como si no..."


translate spanish day5_us_849ef2d5:


    "No sé cuánto tiempo estaría dedicando a pensamientos existencialistas, pero el ruidoso ronquido de Ulyana me trajo de vuelta a la realidad."


translate spanish day5_us_d9d3457b:


    me "Tan menuda, pero ronca tan fuerte..."


translate spanish day5_us_c4195e16:


    "Suspiré, cargué con Ulyana en mi espalda otra vez y me dirigí a su cabaña."


translate spanish day5_us_a20cefa7:


    "..."


translate spanish day5_us_12b2a39f:


    "No tenía deseos de darle explicaciones sobre todo a Alisa, así que dejé a la soñolienta Ulyana en el porche, llamé a la puerta y me fui velozmente."


translate spanish day5_us_cf46738c:


    "Me aproximé a la cabaña de Olga Dmitrievna con sentimientos encontrados."


translate spanish day5_us_1531a117:


    "Por una parte, hice lo que tenía previsto hacer... consolé y entretuve a Ulyana."


translate spanish day5_us_6aec4a07:


    "Por otra parte..."


translate spanish day5_us_d05290b8:


    "Las dos sábanas arrugadas en mis manos eran más bien como trapos arrancados de una raída camisa de fuerza."


translate spanish day5_us_bb449b79:


    "Abrí la puerta suavemente y entré."


translate spanish day5_us_105fc865:


    mt "¡Semyon!"


translate spanish day5_us_b3e320ef:


    "Olga Dmitrievna estaba sentada en el escritorio, y parecía que había estado esperándome."


translate spanish day5_us_79427e67:


    mt "¿Me quieres explicar algo?"


translate spanish day5_us_377d0453:


    me "Bueno...{w} No la regañes a ella otra vez...{w} ¡Es mi culpa!"


translate spanish day5_us_47603f81:


    "Y de esa forma me convertí en un héroe, para mi sorpresa."


translate spanish day5_us_3ed3fd1f:


    "Mi lengua actuó antes que mis pensamientos."


translate spanish day5_us_8c77f545:


    "Quizás algunos de mis mejores rasgos, de los cuales incluso no soy consciente, se mostraban por sí mismos."


translate spanish day5_us_16f0739b:


    "Humanidad frente al sentido común."


translate spanish day5_us_4354ab86:


    mt "¿De verdad?"


translate spanish day5_us_cce98559:


    "Habiendo empezado esto, escogí mantenerme en mi decisión."


translate spanish day5_us_0b7ff73e:


    me "Bueno, fui yo quien consiguió las sábanas. Y estaba escondido tras un árbol."


translate spanish day5_us_cc347b04:


    mt "¿Detrás de qué árbol?"


translate spanish day5_us_0193dcd6:


    "La líder del campamento me miraba con asombro."


translate spanish day5_us_ee5de7cc:


    mt "Y bien, ¿para qué necesitabas las sábanas?"


translate spanish day5_us_220e2848:


    "Me di cuenta de mi error."


translate spanish day5_us_bc2d73a5:


    me "Entonces, ¿no estás hablando sobre lo del bosque?"


translate spanish day5_us_5afe237b:


    mt "Semyon, no te entiendo."


translate spanish day5_us_13ed3d89:


    mt "Quería saber dónde desapareciste tan misteriosamente durante la excursión, pero ahora estaría más interesada en escuchar tu historia de las sábanas."


translate spanish day5_us_87f8ffa1:


    th "¡Pero era imposible que ella y todos los demás no hubieran visto el espectáculo de Ulyana!"


translate spanish day5_us_e65fa514:


    th "Los pioneros riéndose a carcajadas no podrían haber sido solo imaginaciones mías."


translate spanish day5_us_f41a46e2:


    me "Olga Dmitrievna, en serio...{w} ¿No viste a nadie con una sábana saltando recientemente?"


translate spanish day5_us_de5231d6:


    mt "Vaya, ¿eras tú?"


translate spanish day5_us_8fb93608:


    "Me echó un vistazo de cerca."


translate spanish day5_us_c20bb56d:


    me "No, no fui yo..."


translate spanish day5_us_85c3d025:


    th "¿No podía darse cuenta solamente con mi altura?"


translate spanish day5_us_32446a1f:


    mt "Pero estás sosteniendo las sábanas..."


translate spanish day5_us_a73acf7d:


    me "Sí..."


translate spanish day5_us_d874bb71:


    "No comprendía si es que estaba tomándome el pelo, o es que realmente ella no sabía nada de que iba esto."


translate spanish day5_us_3a23059a:


    me "Olga Dmitrievna, hagamos como si esta conversación no hubiera tenido lugar.{w} Estoy muy cansado por hoy."


translate spanish day5_us_d034fa83:


    mt "Está bien, vete a la cama."


translate spanish day5_us_24edbca1:


    "Para mi sorpresa, ella estuvo de acuerdo."


translate spanish day5_us_db925e9e:


    "Desde luego, estaba con asombro por semejante reacción, pero decidí aprovecharme del momento. Me envolví con la sábana de la cama y me tumbé cara a la pared."


translate spanish day5_us_4c4a4071:


    "Pero no podía dormirme."


translate spanish day5_us_8d27541d:


    "No había pensamientos, tenía dolor de cabeza, pero aun así no podía dormirme."


translate spanish day5_us_9db6bada:


    "Me di la vuelta hacia el otro lado e imágenes de este día empezaron a destellear frente a mis ojos."


translate spanish day5_us_e7fb5b3f:


    "Apreté con fuerza mis ojos tratando de apartarlas, pero no funcionó."


translate spanish day5_us_e227a4a1:


    "Repentinamente, escuché golpes en la ventana."


translate spanish day5_us_f9ca50cb:


    "Olga Dmitrievna parecía estar dormida."


translate spanish day5_us_ce13ac06:


    "Me vestí y salí fuera."


translate spanish day5_us_a35885f7:


    "Ulyana estaba frente a mi con una traviesa sonrisa."


translate spanish day5_us_92e87dbb:


    us "¿Cómo te lo hiciste para cargarme hasta la cabaña...?{w} ¿No fue difícil?"


translate spanish day5_us_f0de0297:


    me "Realmente no...{w} ¿A qué has venido?"


translate spanish day5_us_047a7a48:


    "No podía dormir, pero en aquel momento la cama parecía ser el único lugar donde pudiera residir sin padecer, por lo cual la repentina visita de Ulyana no me agradaba mucho."


translate spanish day5_us_c198f325:


    us "¿Cómo estaba ella? ¿Te regañó?"


translate spanish day5_us_be6d9222:


    me "No, lo evité de alguna manera..."


translate spanish day5_us_4275b6c0:


    us "¡Pues eso es genial! ¡Siempre tengo suerte!"


translate spanish day5_us_d0119426:


    me "Eso seguro..."


translate spanish day5_us_645ce19d:


    "Mis ojos empezaron a cerrarse contra mi voluntad.{w} Parece que el sueño se apodera de mí finalmente."


translate spanish day5_us_cdcc908e:


    me "Mira, estoy muy cansado..."


translate spanish day5_us_0f78845d:


    us "No nos llevará mucho tiempo.{w} Cierra tus ojos."


translate spanish day5_us_e0f3d01c:


    "Era la cosa más fácil de hacer, y no quería saber tampoco para qué lo necesitaba... tal vez así ella me dejaría solo más pronto."


translate spanish day5_us_5cbb3956:


    "Cerré mis ojos."


translate spanish day5_us_2bd7350e:


    "Y en ese instante sentí un lacónico beso."


translate spanish day5_us_5d1fc15b:


    "Mis ojos se abrieron por sí solos, pero Ulyana ya estaba corriendo lejos, despidiéndose de mí con la mano."


translate spanish day5_us_1d1a05f7:


    "Me quedé quieto, paralizado y no podía ni tan siquiera gritarle algo mientras se iba."


translate spanish day5_us_74510c40:


    "No sé por cuánto tiempo permanecí quieto de esa manera, pero finalmente el frío de la noche aclaró mi estupor."


translate spanish day5_us_78b47b21:


    "Me estremecí y retorné de vuelta a la cama."


translate spanish day5_us_eff30d1d:


    "Esta vez no quería dormir, quise pensar en todo ello, pero mis ojos, los cuales ya empezaron a cerrarse cuando ya estaba frente a Ulyana, parecieron enviar una orden al resto del cuerpo por la cual caí dormido antes de darme cuenta qué sucedió..."


translate spanish day5_un_b3a56e2b:


    "Sólo Lena y Alisa destacaban entre todo este esplendor."


translate spanish day5_un_9494f7ea:


    "Bueno, claro que era bastante natural para Alisa estar discutiendo con alguien de esa manera."


translate spanish day5_un_49a0c8bc:


    "Pero escuchar a Lena hablando con voz alta...."


translate spanish day5_un_9844ee70:


    "Me acerqué silenciosamente, tratando de comprender qué era lo que ocurría."


translate spanish day5_un_6ca87438:


    dv "¡No, escúchame a mi!"


translate spanish day5_un_8c9cf7c5:


    un "¡No voy a escucharte nada!"


translate spanish day5_un_8eaadb05:


    dv "¿De verdad? ¡Pues genial!"


translate spanish day5_un_4e3a9f81:


    "Alisa se dio la vuelta y su mirada se cruzó con la mía."


translate spanish day5_un_0961f894:


    "Al principio estaba confusa sobre lo que sucedía, pero luego..."


translate spanish day5_un_7f5542bc:


    dv "¡Así que tú estabas escuchando a escondidas!"


translate spanish day5_un_12887db1:


    me "¿Qué? ¿Yo? ¡No!"


translate spanish day5_un_712bd43f:


    "Comenzó a avanzar lentamente hacia mi, yo di unos pasos atrás."


translate spanish day5_un_ee80f2e5:


    "Lena, sin embargo, permaneció quieta donde estaba."


translate spanish day5_un_607f19ad:


    dv "Muy bien, vale pues, ¡escucha!"


translate spanish day5_un_c992f6a8:


    "Alisa se detuvo a medio camino y se dio la vuelta hacia Lena."


translate spanish day5_un_6aa66cea:


    dv "Por cierto, ¿sabías que él me estaba espiando a escondidas hoy?"


translate spanish day5_un_3875ba7b:


    un "¿Qué?"


translate spanish day5_un_91748180:


    dv "Espiándome. ¡Y él lo vio {i}todo{/i}!"


translate spanish day5_un_f328f2ed:


    un "Es eso... ¿es eso cierto?"


translate spanish day5_un_52e59e20:


    "Lena retorció sus manos consternada, me hizo una rápida mirada y comenzó a mirar fijamente en el suelo, como si se la hubiera tragado el mundo."


translate spanish day5_un_127f1428:


    me "¡No y no, nada de eso ocurrió!"


translate spanish day5_un_806ed8e6:


    "Habiendo hecho una mirada de enojo a Alisa, me acerqué a Lena."


translate spanish day5_un_32dec18e:


    dv "¿Por qué lo niegas? ¡Tengo incluso un testigo!"


translate spanish day5_un_246d6902:


    me "¡Tu testigo es incluso peor que tú!"


translate spanish day5_un_05eee1a3:


    dv "Entonces, ¿sigues declarando que no sucedió?"


translate spanish day5_un_5bf1c5fc:


    "Me tomé un segundo para pensar."


translate spanish day5_un_756d591a:


    th "Pero de verdad, ¿por qué tenía semejante deseo patológico de querer excusarme frente a Lena?{w} Como si a pesar de todo la necesitara vitalmente, necesitara que su opinión sobre mi permaneciera intacta."


translate spanish day5_un_9cc4691d:


    th "Aun así, ¿cómo sabría lo que ella pensaba?"


translate spanish day5_un_cd4b1ccb:


    me "¡No ocurrió!"


translate spanish day5_un_b7eb3fd9:


    "Lena lanzó una siniestra mirada a Alisa y alzó su mirada justo hacia mi tras eso, una mirada llena de desasosiego y esperanza."


translate spanish day5_un_71c04b4b:


    "De repente se me hizo insoportable sostenerle la mirada a esos ojos."


translate spanish day5_un_0d6ab95e:


    me "No sucedió..."


translate spanish day5_un_cacc8d60:


    "Repetí menos seguro."


translate spanish day5_un_ba1e125e:


    dv "Bueno, da igual."


translate spanish day5_un_e36048c5:


    "Alisa me desdeñó detrás de mí."


translate spanish day5_un_c6c151b9:


    dv "Es asunto tuyo a quién crees."


translate spanish day5_un_97aaae01:


    "Le dijo a Lena displicentemente, luego se giró y se fue hacia la hoguera."


translate spanish day5_un_50793df0:


    un "¿Es verdad?"


translate spanish day5_un_56ad8efd:


    "Me sentí incómodo, como un pez fuera del agua, teniendo la sensación urgente de acabar con esto cuanto más rápido mejor."


translate spanish day5_un_ba176c8b:


    th "¿Y por qué tendría que excusarme con ella?"


translate spanish day5_un_5156bbe7:


    me "Y bien, ¿qué pasa si ocurrió?"


translate spanish day5_un_5dc693b2:


    "Miré a Lena desafiante, pero sólo para percatarme de un brillante rayo de sol del ocaso reflejándose en una lágrima, la cual surcaba la piel de su mejilla."


translate spanish day5_un_e6d47aad:


    me "No, quiero decir..."


translate spanish day5_un_f203e664:


    un "No necesitas mentir."


translate spanish day5_un_0c5b6d1e:


    "Limpió sus lágrimas y trató de sonreír."


translate spanish day5_un_2060fc9e:


    un "A fin de cuentas, no es asunto mío."


translate spanish day5_un_f32b3a36:


    me "No, por qué no..."


translate spanish day5_un_f3c734aa:


    th "¿Pero por qué lo es?"


translate spanish day5_un_35b0d3f6:


    me "Es solamente que yo... Bueno... Fue un accidente, ¿entiendes?{w} Un accidente que fue causado por Ulyana, nada más."


translate spanish day5_un_6e34d368:


    un "Claro, seguro..."


translate spanish day5_un_85bc2437:


    me "¡Es verdad!"


translate spanish day5_un_e71ea824:


    un "Te creo."


translate spanish day5_un_89436b45:


    "Lena decía todo sin pizca de emoción, como si no le importara para nada lo que ocurría."


translate spanish day5_un_76748f71:


    "Estaba a punto de creérmelo, sus lágrimas de hace unos segundos atrás..."


translate spanish day5_un_14dd805c:


    me "Me parece a mí que no."


translate spanish day5_un_5c9d8120:


    un "¿Qué es esto, algún tipo de juego?"


translate spanish day5_un_29ff2cb1:


    "Lena dijo en voz baja, pero había un ápice de ira en su tono."


translate spanish day5_un_0a753ae6:


    un "¿Realmente necesitas convencerme de eso? E incluso si te creí, ¿entonces qué? ¿Acaso se volvería verdad?"


translate spanish day5_un_ce5015a9:


    me "No trato de convencerte de nada, solamente quiero hacerte entender que no soy culpable... y no quiero que..."


translate spanish day5_un_ffacfd31:


    un "¡¿Por qué me debería importar algo?!"


translate spanish day5_un_f78c17ba:


    "Gritó Lena."


translate spanish day5_un_a7f39912:


    "Estaba de pie dándole la espalda a la hoguera, pero estaba seguro de que todos los pioneros se giraron en nuestra dirección."


translate spanish day5_un_fd952043:


    un "Si quieres espiar a alguien... ¡hazlo! ¡Haz lo que tú quieras! Pero, ¿por qué me molestas?"


translate spanish day5_un_f8882e89:


    me "Yo no..."


translate spanish day5_un_427acce6:


    "Pero en verdad, desde cualquier punto de vista, parecía eso... Estaba tratando de excusarme frente a Lena sin éxito.{w} Como un niño culpable, o quizá incluso como un marido que cometía un desliz..."


translate spanish day5_un_b24ff123:


    un "¡Pues así que ya sabes! ¡No me importa!"


translate spanish day5_un_5cd48f53:


    "Lena se giró y se dirigió de vuelta al campamento a paso rápido."


translate spanish day5_un_6f3fa70b:


    "No traté de detenerla... ciertamente, no era ahora la mejor solución."


translate spanish day5_un_5307c03e:


    "Está borde, no le ayudará en nada tratar de convencerla o apelar a su razón."


translate spanish day5_un_c03bbe86:


    th "¿Desde cuando es así ella?{w} Gritando de esa forma, poniéndose loca..."


translate spanish day5_un_37a8623c:


    "Con esos pensamientos en mente, retorné a la hoguera y me senté en un tronco de madera."


translate spanish day5_un_ce617998:


    "..."


translate spanish day5_un_dbeba143:


    dv "¿Y bien?"


translate spanish day5_un_a07446cb:


    "Me preguntó Alisa tras sentarse cerca."


translate spanish day5_un_ad02f53a:


    me "¿Qué?"


translate spanish day5_un_1a47cd1e:


    dv "No has tenido mucho éxito, ¿eh?"


translate spanish day5_un_2abbfc44:


    me "Como puedes ver..."


translate spanish day5_un_3792f79f:


    "Gruñí mientras removía las brasas con un palo."


translate spanish day5_un_e73a96b8:


    "Se hacía completamente oscuro, y el claro se zambullía en la oscuridad."


translate spanish day5_un_87200809:


    "El paisaje que nos rodeaba era una reminescencia de un retrato del libro de un niño acerca de un monstruo del bosque... una criatura desconocida del bosque estaba a punto de saltar de detrás de un árbol, lechuzas depredadoras ululaban amenazadoramente en las brancas de los árboles, incluso los ratones te observaban fijamente con recelo."


translate spanish day5_un_3c849f9b:


    "No estaba preocupado en aquel momento. Todo era diferente respecto al día anterior, cuando estuve en el calabozo del antiguo campamento..."


translate spanish day5_un_d74fcdd1:


    "La luna estaba brillando, prestando atención a nuestra valiente tropa. El mundo estaba durmiendo, esperando al alba, bañándose en la luz lunar."


translate spanish day5_un_e9c4c23a:


    dv "Tal como creía. A pesar de que era de esperar."


translate spanish day5_un_fda262d8:


    me "Pero, ¿por qué...?"


translate spanish day5_un_aef6d994:


    "Al fin me las apañé para coger el tronco más grande que ardía y lo tiré al centro del fuego, causando que las llamas ascendieran casi un metro de alto y que un vórtice de chispas se esparciera en todas las direcciones."


translate spanish day5_un_77bf3519:


    me "¿Se comporta Lena a menudo de esa forma?"


translate spanish day5_un_185b84dd:


    dv "¿Cómo?"


translate spanish day5_un_56fc9a67:


    me "Gritándole a los demás.{w} No puedo creerme que actuara de semejante forma."


translate spanish day5_un_989f2bee:


    dv "Ella es humana, como todos los demás."


translate spanish day5_un_93c166ce:


    "Alisa soltó una risita."


translate spanish day5_un_32fca04b:


    dv "Como si no te hubieras dado cuenta antes."


translate spanish day5_un_0f99a0a2:


    me "¿Que no me había dado cuenta de qué?"


translate spanish day5_un_5e9fa3e4:


    dv "Su."


translate spanish day5_un_e54b39e9:


    me "¿Qué quieres decir?"


translate spanish day5_un_b08ce089:


    dv "Quiero decir que este es su verdadero rostro."


translate spanish day5_un_ec069979:


    me "¿Qué quieres decir con «verdadero rostro»?"


translate spanish day5_un_2e4631df:


    dv "¡Oh, pero que estúpido eres!"


translate spanish day5_un_34dc294a:


    me "Acaba con lo que empezaste."


translate spanish day5_un_44462a0c:


    dv "Ella no es lo que aparenta ser, no tal y como creías antes."


translate spanish day5_un_95364e9c:


    me "Y bien, ¿cómo es ella?"


translate spanish day5_un_6fbef0db:


    dv "Vale, estoy harta de esto..."


translate spanish day5_un_3f845aea:


    "Alisa se levantó, con intención irse."


translate spanish day5_un_bd114f34:


    "No dije nada, solamente me quedé sentado y escuché el silencio del bosque."


translate spanish day5_un_1c95b08d:


    dv "Por cierto, si quieres seguirla, creo que está en la isla."


translate spanish day5_un_278fc1b6:


    me "¿En qué isla?"


translate spanish day5_un_5f02cbe1:


    dv "Aquella en la que estuviste reuniendo fresas hoy."


translate spanish day5_un_b91fd3d7:


    me "¿Y qué está haciendo ella allí?"


translate spanish day5_un_d7920d68:


    dv "¡Arrgh, de verdad que eres un idiota!"


translate spanish day5_un_42e0fed6:


    "Alisa pateó el suelo, rodeó la hoguera y se sentó en el lugar contrario."


translate spanish day5_un_ff34e775:


    "Debo haber perdido de verdad el sentido de las cosas."


translate spanish day5_un_6615be77:


    "Mi mente estaba completamente en blanco."


translate spanish day5_un_ff8730b2:


    "Para ser más exactos, mi cabeza estaba tan espesa que no había lugar en ella para desarrollar ideas."


translate spanish day5_un_e447cec9:


    "Si comparase mi cerebro en sus mejores tiempos con una gran autopista llena de pensamientos veloces circulando por ahí, persiguiéndose unos a otros y causando accidentes graves, ahora era más bien un diminuto sendero en un distante y desolado bosque, el cual es solamente usado en tiempos de necesidad."


translate spanish day5_un_643db9b6:


    "Traté de reflexionar sobre lo que había sucedido, analizarlo de algún modo, pero parecía que me estaba dando cabezazos contra un muro invisible."


translate spanish day5_un_0877776f:


    "Además, me era imposible hallar una sola emoción en mi alma, cualquier respuesta. Como si todo le hubiera ocurrido a otro."


translate spanish day5_un_8d9c0b6a:


    "Fracasé en convencer a Lena de que no era culpable.{w} ¿Debería importarme?"


translate spanish day5_un_dc1bef5c:


    "Lena lloró, gritó y luego se fue.{w} ¿Y qué?"


translate spanish day5_un_0845afe0:


    "No lo siento para nada.{w} ¿Acaso debería?"


translate spanish day5_un_7c882346:


    "Me mordí el labio hasta que empezó a sangrar y me levanté."


translate spanish day5_un_bbae1908:


    "No podía aguantar permanecer aquí entre todos estos pioneros riendo."


translate spanish day5_un_5ba34865:


    th "Debería alejarme corriendo. No importa dónde. Hacia el bosque, hacia las minas e incluso al espacio. No me importa. Siempre y cuando me vaya lejos de aquí."


translate spanish day5_un_fbc3bc79:


    "Aproveché el momento en que la líder del campamento estaba mirando hacia otra dirección y me desvanecí entre las sombras del bosque."


translate spanish day5_un_a20cefa7:


    "..."


translate spanish day5_un_6219e64c:


    "No había llovido desde hacía tiempo."


translate spanish day5_un_c6749284:


    th "Sin embargo, tampoco estaba seguro de si aquí llovía."


translate spanish day5_un_5d4ba34b:


    th "Qué idea más estúpida, pues claro que llueve. Las plantas requieren de humedad."


translate spanish day5_un_cb1b2ded:


    "La noche refrescaba más que por la tarde, pero el aire todavía no había tenido suficiente tiempo para enfríarse. Me sentía algo mareado por la falta de aire de la tarde."


translate spanish day5_un_40724f26:


    "De repente, me di cuenta de que realmente me apetecía nadar."


translate spanish day5_un_489cb8db:


    "Es un deseo bastante normal porque los días eran calurosos y te empapabas de sudor durante el día."


translate spanish day5_un_1056c0d4:


    "Tampoco me di cuenta de que me estaba acercando al embarcadero."


translate spanish day5_un_d177ba32:


    th "¿Por qué no la playa?"


translate spanish day5_un_9bb6a06d:


    "Me acordé de nuevo de Lena. Todo lo que trataba de ignorar acababa retornando de nuevo y me dio que pensar en cuestiones inadecuadas, respuestas inapropiadas y en vagos deseos."


translate spanish day5_un_afb26968:


    "Era obvio que había sido traído al embarcadero por mi subconsciente."


translate spanish day5_un_17e84712:


    th "Pero, ¿qué quiero {i}realmente{/i}?"


translate spanish day5_un_4f586330:


    th "¿Disculparme con ella por algo, convencerla de algo?{w} No, improbablemente."


translate spanish day5_un_eb0d22d4:


    "Sólo quería algún tipo de reacción de ella. Quería que ella dijera «Sí, lo comprendo todo» solamente con una sonrisa."


translate spanish day5_un_304c60e6:


    "Y entonces podría dejar de sentirme de {i}ese modo{/i}..."


translate spanish day5_un_a78ee8f1:


    "Era necesario para mi ser comprendido por alguien, incluso en {i}este{/i} mundo."


translate spanish day5_un_21fa3a03:


    "Desamarré una barca, la empujé hacia el agua, me subí y agarré los remos."


translate spanish day5_un_ce617998_1:


    "..."


translate spanish day5_un_1607c44f:


    "Era fácil remar esta vez... mis manos todavía me dolían, pero me las ingenié para desarrollar algún tipo de técnica que me permitiera navegar bastante recto."


translate spanish day5_un_134ecd94:


    "El río parecía congelado, apartándose bajo la barca como un velo translúcido."


translate spanish day5_un_cdd6fc1f:


    "La luz lunar perforaba con profundidad el agua, por lo cual era casi posible ver su fondo."


translate spanish day5_un_1b592616:


    "Comencé a remar más."


translate spanish day5_un_ce617998_2:


    "..."


translate spanish day5_un_9f7443d1:


    "Sorprendentemente, la isla se parecía ahora al igual que durante el día."


translate spanish day5_un_f47c3547:


    "{i}Aquí{/i} normalmente todo suele ser diferente. Tras el crepúsculo era como otro mundo, un mundo misterioso. Aterrador a veces, pero bello a su manera. Un mundo de sombras y susurros, un mundo nocturno."


translate spanish day5_un_b94320d8:


    "Caminé lentamente por los alrededores de la isla, buscando la barca que usó Lena para llegar hasta aquí."


translate spanish day5_un_35778f50:


    "La hierba crujía suavemente bajo mis pies, de vez en cuando las olas golpeaban la orilla pacíficamente, y rebotaban de nuevo como moscas demasiado agotadas de estar chocando frente a un vidrio."


translate spanish day5_un_575aa0ca:


    "La brisa marina agitaba perezosamente las hojas de los árboles, más por costumbre que por un deseo real de hacer cantar la arboleda."


translate spanish day5_un_0bba232e:


    "Contemplaba dicho maravilloso retrato con tal deleite, que no me di cuenta de una cosa hasta que me tropecé con ella."


translate spanish day5_un_44e820c3:


    "Era una barca."


translate spanish day5_un_0c66a774:


    th "En fin, desde luego... ella no vendría nadando hasta aquí."


translate spanish day5_un_c486b958:


    "Me dirigí hacia el centro de la isla."


translate spanish day5_un_ce617998_3:


    "..."


translate spanish day5_un_b2eec26c:


    "Tras un centenar de metros, escuché un crujido detrás de un árbol."


translate spanish day5_un_4dcf727f:


    un "No te acerques más..."


translate spanish day5_un_5f75988c:


    "Susurró Lena."


translate spanish day5_un_51149e68:


    me "..."


translate spanish day5_un_70edb764:


    "Dudé."


translate spanish day5_un_0ca1cc9c:


    un "No te acerques más."


translate spanish day5_un_78558274:


    "Dijo más fuerte."


translate spanish day5_un_3efb73c2:


    me "¿Cómo supiste que era yo?"


translate spanish day5_un_edfa793d:


    "Aunque ella no había mostrado que lo supiera."


translate spanish day5_un_65bf82fc:


    me "Está bien..."


translate spanish day5_un_f4fd5ba0:


    "Me apoyé contra el árbol, tratando de no ver detrás de él."


translate spanish day5_un_ad697c77:


    me "Alisa me contó que estarías aquí."


translate spanish day5_un_ca19d4ba:


    un "¿Y qué?"


translate spanish day5_un_801934ec:


    "Me era difícil saber qué emociones estaba experimentando Lena."


translate spanish day5_un_d382dd22:


    "Su voz sonaba bastante firme; aunque también podía leer irritación y enojo en ella, no podía comprender por qué estaba enfadada o si seguía estándolo."


translate spanish day5_un_13bf83a1:


    me "Aquello fue incómodo."


translate spanish day5_un_7a2f384f:


    "Estaba haciéndolo lo mejor posible con tal de evitar disculpas y excusas innecesarias, pero no podía hallar otras palabras en su lugar."


translate spanish day5_un_e5bf1202:


    un "¿Ésa era la única razón por la cual viniste?"


translate spanish day5_un_e7f8d1d4:


    me "No... Bueno, no lo sé."


translate spanish day5_un_3c94e6e3:


    un "No lo sabes pero, ¿aun así vienes?"


translate spanish day5_un_fc30fd2b:


    me "Sí."


translate spanish day5_un_334f3921:


    un "No deberías."


translate spanish day5_un_839ef62e:


    me "¿Por qué? Claro que si te estoy molestando..."


translate spanish day5_un_c9d68cca:


    un "¿Por qué me estás siguiendo?"


translate spanish day5_un_48199bb7:


    me "Yo..."


translate spanish day5_un_4a3d8c67:


    "Ella tenía razón... parecía realmente eso. Además, definitivamente me sentía atraído por ella."


translate spanish day5_un_84f2a0fc:


    me "No deberías pensar así."


translate spanish day5_un_1adc8b36:


    un "¿Y qué debería pensar?"


translate spanish day5_un_595d7b8c:


    me "No lo sé."


translate spanish day5_un_8d18dd8b:


    un "Solamente puedo extraer conclusiones basándome en tu comportamiento."


translate spanish day5_un_9044e6ea:


    me "De verdad que no lo sé... Supongo que será mejor que me vaya."


translate spanish day5_un_8477995f:


    "Estaba confundido, y era difícil estar tan cerca de ella."


translate spanish day5_un_984ea120:


    un "¿Por qué? Ya que viniste..."


translate spanish day5_un_44251c8f:


    "Me pareció oír un tono juguetón en su voz."


translate spanish day5_un_01822020:


    me "De acuerdo."


translate spanish day5_un_77825d23:


    un "¿Y?"


translate spanish day5_un_ad02f53a_1:


    me "¿Qué?"


translate spanish day5_un_e9197f7e:


    un "¿Qué te contó Alisa?"


translate spanish day5_un_668b75c5:


    me "Nada en especial."


translate spanish day5_un_8bf5923b:


    un "Ya veo."


translate spanish day5_un_fc30fd2b_1:


    me "Claro."


translate spanish day5_un_4924952d:


    un "Bueno pues."


translate spanish day5_un_751ac511:


    me "Sep..."


translate spanish day5_un_40151fe6:


    "Tan sólo permanecimos en silencio por un rato."


translate spanish day5_un_b8d98c47:


    un "Ya que viniste, cuéntame alguna cosa."


translate spanish day5_un_e3b565dd:


    me "Bueno, no sé..."


translate spanish day5_un_8ee362ac:


    un "Por ejemplo, qué ocurrió esta mañana."


translate spanish day5_un_2ea0a340:


    me "¿Que qué ocurrió esta mañana?"


translate spanish day5_un_5b543368:


    un "Ya sabes."


translate spanish day5_un_142ff09f:


    me "Tú misma no quisiste hablar sobre ello."


translate spanish day5_un_7af3f67c:


    un "No quise, ¡pero ahora quiero!"


translate spanish day5_un_ea56876b:


    "La voz de Lena temblaba."


translate spanish day5_un_2fc43074:


    "Ya no podía comprender para nada qué estaba sucediendo."


translate spanish day5_un_968df1e5:


    th "¿Quizás no es ella quien está detrás del árbol?"


translate spanish day5_un_a8a3ea5d:


    un "¡No mires!"


translate spanish day5_un_7c18f5e8:


    me "Vale, vale... Pero, ¿qué pasa?"


translate spanish day5_un_d893718b:


    un "Nada. Pero no mires."


translate spanish day5_un_67742710:


    me "De acuerdo, como desees."


translate spanish day5_un_c928b72a:


    un "Entonces, ¿no ocurrió nada?"


translate spanish day5_un_a336db31:


    me "Bueno, algo sí... ¡Pero fue todo solamente un tonto accidente!"


translate spanish day5_un_4f1a98c1:


    un "Entonces, ¿por qué estabas tan preocupado sobre lo que yo pudiera pensar?"


translate spanish day5_un_7cd08f32:


    me "¡No estoy preocupado!"


translate spanish day5_un_0594d317:


    me "Y de todas formas, tú, ¡tú misma!"


translate spanish day5_un_505ae84f:


    "Dije con voz fuerte, empezándome a sentir irritado."


translate spanish day5_un_acb49e13:


    un "¿Qué pasa conmigo?"


translate spanish day5_un_f2ac40dd:


    me "¿Por qué estás tan preocupada acerca de esta situación?"


translate spanish day5_un_3537de2e:


    un "¿Quién te dijo que esté preocupada sobre esta situación?"


translate spanish day5_un_2c8023fc:


    me "¿Y entonces qué?"


translate spanish day5_un_62592cb0:


    un "Nada..."


translate spanish day5_un_3d26df77:


    "Dijo en un susurro y se quedó callada."


translate spanish day5_un_e6ab9030:


    "Tal conversación no nos llevaría a ninguna parte."


translate spanish day5_un_1826deee:


    th "Lena no dirá nada y, yo, resulto ser tan estúpido como para no poder adivinarlo, o tan siquiera para estar seguro de mí mismo."


translate spanish day5_un_ae93dffd:


    me "Lo siento, supongo que es culpa mía todo. Simplemente no puedo comprender..."


translate spanish day5_un_962ad609:


    un "Por qué..."


translate spanish day5_un_ad02f53a_2:


    me "¿Qué?"


translate spanish day5_un_68ca672f:


    un "¿Por qué siempre estás disculpándote? Por todo. Por lo que hiciste, por lo que estás haciendo, incluso por lo que no has hecho aun."


translate spanish day5_un_a337cbd5:


    me "Pero..."


translate spanish day5_un_0f57e6b5:


    "Quizá tenga razón, pero no me podía comportar de forma diferente. Sentía la necesidad de disculparme ante ella, con todos."


translate spanish day5_un_336e6bce:


    "Así no pensarían mal de mí, no se burlarían de mí. De esta forma podía dejar al margen lo inexpresado y eliminar los malentendidos."


translate spanish day5_un_7259241d:


    un "De todas formas, ¡eso es asunto tuyo!"


translate spanish day5_un_339b95c7:


    "Lena se enfadó."


translate spanish day5_un_ca9b074e:


    un "¡Disculparse, tener excusas! ¿Por qué debería importarme...?"


translate spanish day5_un_607b607b:


    me "Vale, bueno, ¡no soy culpable! De hecho, no creo ni que pudiera ser culpado por algo, pero entonces, ¿qué pasa? ¿Qué te pasa a ti?"


translate spanish day5_un_1e693cd1:


    un "¿Por qué tendría que pasarme nada a mí?"


translate spanish day5_un_6478a1d7:


    me "No lo sé... Solamente me lo parece."


translate spanish day5_un_6355a504:


    un "¡Oh! ¡Crees que te lo parece!"


translate spanish day5_un_0467a4c4:


    "Ella se rio."


translate spanish day5_un_27b88dd3:


    un "Hablas como si me conocieras."


translate spanish day5_un_51149e68_1:


    me "..."


translate spanish day5_un_cbc4bb84:


    "No contesté."


translate spanish day5_un_af01240b:


    un "Por favor, vete..."


translate spanish day5_un_850cca06:


    "Dijo Lena tras un rato."


translate spanish day5_un_a09db615:


    "No me moví, no podía, no tenía fuerzas para hacerlo."


translate spanish day5_un_d2f6ef68:


    "Llegados a este punto no quería nada... no quería ni excusarme, ni disculparme. No quería ni su comprensión."


translate spanish day5_un_8ef1b6f9:


    "Estaba demasiado cansado de todo."


translate spanish day5_un_5ac38cf1:


    me "¿Por qué?"


translate spanish day5_un_f174e90e:


    un "Sólo vete..."


translate spanish day5_un_e3a26dcc:


    "Susurró."


translate spanish day5_un_1457a622:


    me "No quiero."


translate spanish day5_un_380bd525:


    un "Entonces me iré."


translate spanish day5_un_bf0dc08a:


    me "Volvamos juntos."


translate spanish day5_un_049b021f:


    un "No."


translate spanish day5_un_35d834dd:


    me "¿Cuánto tiempo te vas a pasar ofendida? ¿Por qué? ¿Qué pasa contigo? Todos se comportan aquí con normalidad, excepto tú."


translate spanish day5_un_3fddb229:


    "No me importaba qué significaban mis palabras. Como si las hubiera dicho otra persona y el tema de la conversación no significara nada."


translate spanish day5_un_5dedc88b:


    un "En verdad no comprendiste nada. Alisa tenía razón."


translate spanish day5_un_4bf0d4a0:


    me "¿Sobre qué?"


translate spanish day5_un_20163f63:


    un "Olvídalo..."


translate spanish day5_un_0b35a130:


    "Cerré mis ojos para pensármelo."


translate spanish day5_un_475b9270:


    th "No, no me voy. No lo comprendo, no sé qué pensar, ni qué hacer."


translate spanish day5_un_abacf06a:


    th "¿Y desde cuándo dejé de preocuparme sobre mi situación en este mundo o cómo volver?"


translate spanish day5_un_d8c918ef:


    th "¿Desde cuándo solo puedo pensar en Lena, o cómo los demás me verían, o cómo mis acciones se percibirían por los otros?"


translate spanish day5_un_56543886:


    th "Es sencillamente estúpido y no soy yo; de hecho, es inapropiado en esta situación."


translate spanish day5_un_aed8e1b2:


    me "En resumen, ¡no traté de hacer algo y no tuve intención de justificarme por cosas que no hice!"


translate spanish day5_un_dfffcbac:


    "No hubo respuesta."


translate spanish day5_un_4183115b:


    me "¡Ey! ¿Me escuchas?"


translate spanish day5_un_d7d3178a:


    "Al fin decidí ver quien estaba escondido en realidad tras el árbol, pero no había nadie ahí."


translate spanish day5_un_6807a309:


    th "¡Así que se fue!"


translate spanish day5_un_e9ad8eab:


    "Salí corriendo hacia la barca de Lena, pero ya estaba muy lejos, casi cerca del embarcadero."


translate spanish day5_un_8f338516:


    me "¡Me da igual!"


translate spanish day5_un_8494703d:


    "Grité y caminé por la orilla."


translate spanish day5_un_ce617998_4:


    "..."


translate spanish day5_un_2c35439c:


    "Volví relativamente rápido y sin muchos problemas."


translate spanish day5_un_9d79bcf3:


    "Sin embargo, mis manos me dolían terriblemente, y mis ojos se estaban cerrando por sí mismos."


translate spanish day5_un_dd481fe9:


    "Probablemente tal carga física y, aun más importante, emocional sea demasiado para una persona."


translate spanish day5_un_961bdcdb:


    "Caminé lentamente hacia la cabaña de la líder del campamento, mirando fijamente mis pies y sin pensar en nada."


translate spanish day5_un_793d9878:


    "Alguien me llamó en la plaza."


translate spanish day5_un_93b5fc50:


    dv "¡Ey!"


translate spanish day5_un_53356b4d:


    "Alisa corrió hacia mí."


translate spanish day5_un_2cf26385:


    dv "¿Has estado en la isla?"


translate spanish day5_un_9513cd87:


    me "Sí..."


translate spanish day5_un_d331deef:


    "A decir verdad, no quería hablar con ella sobre nada, pero ya no tenía fuerza para mentirle."


translate spanish day5_un_9c8b084d:


    dv "¿Y qué tal fue?"


translate spanish day5_un_e5f72f43:


    me "No importa... estoy muy cansado y me voy a dormir."


translate spanish day5_un_a7c99a15:


    dv "¡Vamos, cuéntame!"


translate spanish day5_un_48f402f2:


    "Su cara tenía una mueca tan obscena que temblé de ira."


translate spanish day5_un_3f72c7b7:


    me "¡¿Por qué debería importarte?!"


translate spanish day5_un_12b03082:


    dv "Bueno, yo sólo..."


translate spanish day5_un_5449a6fc:


    "Murmuró con desaliento."


translate spanish day5_un_cc04c95f:


    me "¡Ocúpate de tus asuntos!"


translate spanish day5_un_ea222abb:


    "Le espeté, y aceleré mi paso hacia las cabañas."


translate spanish day5_un_206f4c0d:


    "Alisa no intentó detenerme."


translate spanish day5_un_1e4d5480:


    "Fastidiado, Olga Dmitrievna no había regresado aun, y no pude encontrar mi llave."


translate spanish day5_un_325046a9:


    th "¿Y si la perdí?"


translate spanish day5_un_b5d9b711:


    "Lo único que podia hacer era esperar."


translate spanish day5_un_95440e29:


    "Me dejé caer en la tumbona y cerré mis ojos."


translate spanish day5_un_3eda7fc1:


    "Sentía un gran pesar y mi alma estaba siendo destrozada por vagas expectativas."


translate spanish day5_un_71767923:


    "De vez en cuando tenía un sentimiento de que ya estaba muerto (sólo que sin darme cuenta) y que había sido lanzado al infierno."


translate spanish day5_un_ef43a5c3:


    th "Pero en realidad, en lugar de estar intentando salir de aquí, estoy girando en una rotonda diabólica que va más y más rápido. Me estoy involucrando más y más con la vida de este mundo, de este campamento."


translate spanish day5_un_f6d61e07:


    "Como si no tuviera una vida anterior... la real."


translate spanish day5_un_6a8003ef:


    "Como si siempre estuviera interesado en las opiniones de los demás..."


translate spanish day5_un_0730defb:


    th "¡Maldita sea! ¡Nunca me importó!"


translate spanish day5_un_226088e0:


    th "¿Por qué justo aquí? ¿Por qué justo ahora?"


translate spanish day5_un_701c073f:


    "Recordé el rostro de Lena, en lágrimas."


translate spanish day5_un_79fc3a70:


    th "Sí, probablemente no la opinión de otros, pero su opinión era la que me importaba..."


translate spanish day5_un_195bd715:


    "Oí pasos en la lejanía y después de un rato Olga Dmitrievna apareció."


translate spanish day5_un_fb7d3b52:


    "Me miró por unos segundos. Parecía que estaba a punto de decir algo, pero luego sólo suspiró, abrió la puerta con su llave y entró."


translate spanish day5_un_f73e0745:


    "La seguí."


translate spanish day5_un_62928289:


    "Sombras sin forma, recuerdos borrosos, fragmentos de sentimientos y emociones se arremolinaron en mi cabeza por un largo tiempo."


translate spanish day5_un_da141c38:


    "Por tanto tiempo que tras un rato no podía distinguir dónde estaba o qué me estaba pasando."


translate spanish day5_un_621dba00:


    "La única salvación era dormir..."
# Decompiled by unrpyc: https://github.com/CensoredUsername/unrpyc
