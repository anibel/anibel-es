
translate spanish day4_std_morning_81731118:


    "Me desperté con un endiablado zumbido en el interior de mi cabeza."


translate spanish day4_std_morning_163f48a5:


    "El zumbido parecía provenir de mi profunda consciencia."


translate spanish day4_std_morning_dfa1885d:


    "Pero cuando recuperé los sentidos, me di cuenta de que la causa de ese zumbido era la alarma del reloj."


translate spanish day4_std_morning_c8d82a68:


    th "Qué raro. ¿De dónde vino? ¿Por qué está cerca de mi cama?"


translate spanish day4_std_morning_51698244:


    "Eran las siete y media, de acuerdo con el reloj."


translate spanish day4_std_morning_77df9db1:


    "Olga Dmitrievna ya se había ido, así que no había nadie que me obligara a ir a la formación."


translate spanish day4_std_morning_f7391aeb:


    th "Por tanto, puedo dormir un poco más."


translate spanish day4_std_morning_08555663:


    "Cerré mis ojos, pero parecía como que mi consciencia ya tenía suficiente café como para aguantar largo y tendido para un día productivo. Necesitaba levantarme."


translate spanish day4_std_morning_dae26579:


    th "Debería planificar mi día. Después de todo, ¡necesito al menos encontrar alguna cosa hoy!"


translate spanish day4_std_morning_213e2357:


    "Pero aun y así, nada se me ocurrió."


translate spanish day4_std_morning_dbbf160b:


    th "Está bien. Necesito levantarme por completo e ir a asearme."


translate spanish day4_std_morning_0f23f979:


    "En mi camino hacia los lavabos me encontré con Zhenya."


translate spanish day4_std_morning_2b068e19:


    me "¿Por qué te has levantado tan temprano?"


translate spanish day4_std_morning_86a5cb53:


    mz "¿Hay algo de malo en levantarse temprano?"


translate spanish day4_std_morning_bb2e0646:


    "Me miraba como si la hubiera insultado."


translate spanish day4_std_morning_08d4c5a9:


    me "No, nada. Sólo curiosidad."


translate spanish day4_std_morning_44cce5e2:


    mz "¡No es asunto tuyo!"


translate spanish day4_std_morning_b3f5b0be:


    "¡Caramba! Zhenya no es muy simpática, incluso cuando está de buen humor. Pero hoy se le veía furiosa."


translate spanish day4_std_morning_eabc170b:


    "El agua fría me espabiló, la perplejidad en mi mente se disipó y mis pensamientos comenzaron a ponerse en orden."


translate spanish day4_std_morning_ca15c6d0:


    "Curiosamente, empecé a preocuparme más por encontrar un buen lugar en la cantina que en hallar respuestas."


translate spanish day4_std_morning_db0151db:


    "Me cepillé mis dientes y estaba a punto de irme, cuando un ruido me llegó a mis oídos traído por la brisa."


translate spanish day4_std_morning_4766e5e2:


    th "Probablemente es una ardilla u otro tipo de animal."


translate spanish day4_std_morning_3cb11a08:


    "Escuché otro ruido, esta vez un poco más alejado."


translate spanish day4_std_morning_2a6c3c6a:


    "Andé unos diez metros por el camino, buscando la fuente de éste."


translate spanish day4_std_morning_e0c86c0b:


    "Nadie. Tan solamente el bosque matutino."


translate spanish day4_std_morning_82583edb:


    "Retorné a los lavabos y vi a Miku, quien estaba buscando alguna cosa entre la hierba."


translate spanish day4_std_morning_152eeed8:


    "Al darse cuenta de mi presencia, ella sonrió y saltó acercándose a mi."


translate spanish day4_std_morning_a1863396:


    mi "¡Oh, hola! ¡Buenos días! Accidentalmente se me han caído mis polvos para dientes. Sólo trataba de recuperarlos de nuevo."


translate spanish day4_std_morning_3243720e:


    "La hierba cubierta de rocío no parecía ser de mucha ayuda."


translate spanish day4_std_morning_7f1bbccc:


    me "¿Estás segura que es una buena idea?"


translate spanish day4_std_morning_fdcbae6e:


    mi "Bueno, ¿y por qué? ¿Y qué puedo hacer si no? ¡No tengo más! Eran los últimos que me quedaban."


translate spanish day4_std_morning_30820f3b:


    "Ella hizo un puchero, como cuando a un crío se le ha arrebatado su juguete favorito."


translate spanish day4_std_morning_7ff796f1:


    me "Aquí tienes, te daré de los míos..."


translate spanish day4_std_morning_fd9fb11a:


    "Metí la mano en la bolsa, la cual yacía sobre el fregadero."


translate spanish day4_std_morning_1ac55bec:


    "No estaban los polvos para dientes ahí."


translate spanish day4_std_morning_4396dacc:


    th "Mmm... qué raro. ¡No los había puesto ahí!{w} Me alejé hace tan sólo un minuto y ahora ya no estaban..."


translate spanish day4_std_morning_b866f4d7:


    me "Escucha, parece que me los he olvidado..."


translate spanish day4_std_morning_b5e2c150:


    "No quería contarle que justo los había acabado de perder."


translate spanish day4_std_morning_25a1bc7e:


    "Conociendo la sensibilidad de esta muchacha, era seguro asumir que la desaparición de objetos la impresionaría tanto que su cerebro se reiniciaría para evitar sobrecalentarse."


translate spanish day4_std_morning_3f91116b:


    me "Bueno, estaré fuera..."


translate spanish day4_std_morning_165f0267:


    mi "Claro, de acuerdo. Visítanos... Quiero decir, visítame en el club de música, todavía sigo sola ahí. Pero nosotros... quiero decir yo..."


translate spanish day4_std_morning_d82a5e6d:


    "La voz de Miku se fundía en las frescura de la veraniega mañana, lejos detrás de mí."


translate spanish day4_std_morning_0c128c91:


    "A la vuelta en la cabaña de la líder, cogí mi teléfono móvil y miré el indicador de batería."


translate spanish day4_std_morning_2f9d1f0c:


    "Suficiente energía para un día, tal vez incluso menos."


translate spanish day4_std_morning_3d04d563:


    th "Por supuesto, no me sería de gran ayuda aquí, pero aun así..."


translate spanish day4_std_morning_632d44dd:


    "Encontrar un cargador en los ochenta... significaría inventárselo."


translate spanish day4_std_morning_f6cc3af9:


    "Iba a dirigirme para tomar el desayuno, cuando alguien llamó a la puerta."


translate spanish day4_std_morning_482a0501:


    "Era Slavya."


translate spanish day4_std_morning_47583a01:


    sl "¡Buenas mañanas!{w} ¿Has visto a Olga Dmitrievna?"


translate spanish day4_std_morning_9e0939cf:


    "Me quedé mirando fijamente sus pechos. Tan solamente me habían fascinado demasiado el día anterior."


translate spanish day4_std_morning_2645a069:


    sl "¿Semyon?"


translate spanish day4_std_morning_3f835829:


    "Slavya me miraba preocupada, pero aun así no podía arreglármelas para apartar mi mirada de sus pechos."


translate spanish day4_std_morning_a428f2b1:


    sl "¿Te encuentras mal?"


translate spanish day4_std_morning_a21b3e34:


    me "Al contrario, todo está simplemente..."


translate spanish day4_std_morning_65e261bd:


    sl "¿Olga Dmitrievna...?"


translate spanish day4_std_morning_535da2f1:


    me "No lo sé, no estaba aquí cuando me desperté."


translate spanish day4_std_morning_1e0f3ed2:


    "Al final me hice con el control de la situación."


translate spanish day4_std_morning_0d62f1e5:


    sl "Está bien, todavía tengo una cosa que hacer, en fin nos vemos en el desayuno."


translate spanish day4_std_morning_145891de:


    "Slavya sonrió, me saludó con la mano y se alejó corriendo."


translate spanish day4_std_morning_f6cc3af9_1:


    "Iba a dirigirme para tomar el desayuno, cuando alguien llamó a la puerta."


translate spanish day4_std_morning_26171c75:


    "Era Lena."


translate spanish day4_std_morning_5edd55c2:


    un "Ermm... buenos días."


translate spanish day4_std_morning_70958877:


    me "Claro, buenos días..."


translate spanish day4_std_morning_dab051d6:


    "Me cogía un poco por sorpresa."


translate spanish day4_std_morning_6e7a684a:


    "Los sucesos del día anterior todavía estaban frescos en mi memoria, pero realmente no quería hablar de ellos."


translate spanish day4_std_morning_ed2df426:


    me "Estás buscando probablemente a Olga Dmitrievna, ¿no?"


translate spanish day4_std_morning_736f44dd:


    un "Bueno, no...{w} Quiero decir, sí..."


translate spanish day4_std_morning_db521796:


    "Lena estaba como de costumbre... tímida y fácilmente avergonzada."


translate spanish day4_std_morning_c62b7542:


    "Ese baile en el embarcadero... ahora parecía no más que un sueño."


translate spanish day4_std_morning_edd9b25a:


    me "Ella no está aquí, lo siento..."


translate spanish day4_std_morning_98757b47:


    un "Mmm.. vale pues..{w} Me voy..."


translate spanish day4_std_morning_751ac511:


    me "Vale..."


translate spanish day4_std_morning_2dd7a526:


    "Cuando Lena se fue, pensé que había sido demasiado frío con ella, por lo que decidí que la próxima vez que nos viéramos sería más amable."


translate spanish day4_std_morning_07104fa7:


    "Aun así, en cualquier caso, la mañana estaba brillando y era bella."


translate spanish day4_std_morning_5a3770b6:


    "El sol radiante estaba brillando por encima del espacio y el tiempo del campamento «Sovyonok», calentando a sus lugareños y recargándolos con energía para gastarla en un día productivo."


translate spanish day4_std_morning_08b429c2:


    "O, en mi caso, para malgastarla en intentos fútiles de hallar una explicación a todo lo que está sucediendo aquí."


translate spanish day4_std_morning_eaabbc69:


    "Había una gran e inusual multitud de gente cerca de la cantina."


translate spanish day4_std_morning_2260fedb:


    "Por supuesto, no había otro lugar en el campamento que a los pioneros les gustara tanto como la cantina, pero ¿por qué se amontonaban en el porche...?"


translate spanish day4_std_morning_f5aef4ac:


    "Me acerqué para averiguar qué era lo que ocurría."


translate spanish day4_fail_morning_bd4585a5:


    "Un terrible ruido resonaba en mi cabeza y me despertó.{w} Mi cráneo se sentía como si alguien lo estuviera golpeando desde dentro."


translate spanish day4_fail_morning_fce68231:


    "Me froté los ojos, vi la alarma del reloj sonar en la mesita de la cama, la apagué y me quedé dormido otra vez."


translate spanish day4_fail_morning_a20cefa7:


    "..."


translate spanish day4_fail_morning_d941f014:


    "Cuando me desperté, eran ya las nueve menos cuarto."


translate spanish day4_fail_morning_772587d6:


    th "Parece que ya es la hora de levantarse. No puedo perderme mi desayuno."


translate spanish day4_fail_morning_3b1d754c:


    "Me levanté, agobiado, y...{w} ¡Repentinamente recordé la última noche!"


translate spanish day4_fail_morning_5ef37d77:


    "Había un pedazo de papel debajo de la almohada:"


translate spanish day4_fail_morning_470a435f:


    "«Estás aquí por una razón»."


translate spanish day4_fail_morning_2dc1af34:


    th "Todavía no lo entiendo, ¿qué significa...?"


translate spanish day4_fail_morning_50258f02:


    th "¿Cómo lo hice para enviarme un mensaje a mi mismo?"


translate spanish day4_fail_morning_cdc1a9f5:


    th "¿Por qué no recuerdo eso?"


translate spanish day4_fail_morning_f2a17f0f:


    "Más preguntas que respuestas.{w} Más bien no hay respuestas, para ser exactos."


translate spanish day4_fail_morning_3d4304d6:


    "Abandoné la cabaña y miré por el campamento."


translate spanish day4_fail_morning_25fa9b94:


    th "No, no parece una ilusión."


translate spanish day4_fail_morning_b5c3816d:


    "Pero esta nota... hizo que todo fuera mucho más complicado."


translate spanish day4_fail_morning_2b1800da:


    th "¿Por qué debería confiar?"


translate spanish day4_fail_morning_5b2a5873:


    "Mi estómago rugió traicioneramente."


translate spanish day4_fail_morning_4c70df13:


    th "Incluso el Agente Mulder no realizaba investigaciones estando hambriento."


translate spanish day4_fail_morning_eaabbc69:


    "Había una gran e inusual muchedumbre de gente cerca de la cantina."


translate spanish day4_fail_morning_2260fedb:


    "Por supuesto, no había otro lugar en el campamento que a los pioneros les gustara tanto como la cantina, pero ¿por qué se amontonaban en el porche...?"


translate spanish day4_fail_morning_f5aef4ac:


    "Me acerqué para averiguar qué era lo que ocurría."


translate spanish day4_us_morning_0e224319:


    "Sentí calidez en mi sueño."


translate spanish day4_us_morning_62c15031:


    "En ocasiones sucede. Tu cerebro no está despierto del todo, pero sientes aun así el brillar del sol en tus ojos, y cuando al fin despiertas parpadeas durante un rato hasta acostumbrarte a la luz."


translate spanish day4_us_morning_57bf0ff9:


    "Era una maravillosa mañana.{w} Los pájaros estaban cantando, el aire era fresco y aromático, y el mundo estaba bañado de luz matutina."


translate spanish day4_us_morning_51613331:


    "Me quedaría un rato más en la cama, pero... algo no iba bien."


translate spanish day4_us_morning_32a6b4a1:


    "Todos los sucesos de ayer por la noche me pasaron por la mente."


translate spanish day4_us_morning_be5ac746:


    "Historias de miedo y otras no tanto, Ulyana..."


translate spanish day4_us_morning_a581c654:


    "Quien estaba abrazada y acurrucada junto a mi mientras roncaba. No como lo haría una pequeña muchacha."


translate spanish day4_us_morning_a84a89a7:


    "Pero tampoco me preocupa."


translate spanish day4_us_morning_73427713:


    th "Después de todo, aun es temprano. Seguramente no mucho más de las siete o las ocho en punto."


translate spanish day4_us_morning_bb3ada36:


    th "¿Quién querría ir a la biblioteca tan temprano?"


translate spanish day4_us_morning_5c2e2f4e:


    "Me senté apoyando mi espalda y miré a través de la ventana."


translate spanish day4_us_morning_a3c34756:


    th "En un par de minutos el sol justo iluminaría los ojos de Ulyana."


translate spanish day4_us_morning_7e2c6b03:


    me "¡Ése es el momento en que te despertarás!"


translate spanish day4_us_morning_70e45333:


    th "Es interesante, ¿cómo se las arregló para abrazarme tan estrechamente?"


translate spanish day4_us_morning_a9947f93:


    th "Por supuesto, no me podía zafar de ella para nada."


translate spanish day4_us_morning_a26bbdc9:


    "Teóricamente, estaba listo para perder una o dos horas más quedándome así, pero de repente escuché pasos."


translate spanish day4_us_morning_89be002b:


    th "¡Suenan a problemas!"


translate spanish day4_us_morning_05c821d8:


    me "¡Despierta! ¿Me escuchas? ¡Despierta!"


translate spanish day4_us_morning_e09dbdfe:


    "Empecé cuidadosamente, pero insistiendo en mover a Ulyana por sus hombros, tratando de desengancharme de ella, pero todo fue en vano."


translate spanish day4_us_morning_5fe87263:


    "Mientras tanto, los pasos se acercaban más."


translate spanish day4_us_morning_ee87d584:


    th "Tengo que salvarnos a cualquier precio."


translate spanish day4_us_morning_fb5bb4b6:


    "Ponerme en pie parecía ser impracticable... eso es algo que ya aprendí... por lo que decidí gatear."


translate spanish day4_us_morning_0178b537:


    "Mis movimientos se parecían muchísimo a los de un entrenamiento reservista (al cual nunca fui)... un soldado arrastrando su oficial herido bajo fuego de artillería."


translate spanish day4_us_morning_385cbf84:


    "El oficial está inconsciente, el soldado está exhausto, y capas de alambre con púas los rodean."


translate spanish day4_us_morning_29669d4f:


    "Apenas me las arreglé para agacharme tras una estantería y esconder a Ulyana cuando las puertas de la biblioteca se abrieron."


translate spanish day4_us_morning_d2052b63:


    "Zhenya estaba en el umbral de la puerta."


translate spanish day4_us_morning_c24e1c91:


    th "Parecía estar un poco sobrecargada con sus tareas, en tanto que viene nada más comenzar el día con el alba."


translate spanish day4_us_morning_61db85c5:


    el "¡...espera!"


translate spanish day4_us_morning_3417c9cb:


    "Escuché una voz familiar."


translate spanish day4_us_morning_177ac4cc:


    th "¿Eh? ¿Un visitante? Así que, después de todo, tiene un motivo para venir hasta aquí."


translate spanish day4_us_morning_ccf4620f:


    mz "¿Por qué no podrías haber venido ayer? U hoy, pero más tarde..."


translate spanish day4_us_morning_2dfa95a0:


    el "La ciencia no espera a nadie, a pesar de que su madre es la paciencia."


translate spanish day4_us_morning_229b38d1:


    mz "La ciencia..."


translate spanish day4_us_morning_49bd4154:


    "Era Electronik."


translate spanish day4_us_morning_dfa36536:


    mz "Espera un segundo, te lo busco."


translate spanish day4_us_morning_35e81a10:


    "Ella se dirigió hacia nosotros."


translate spanish day4_us_morning_1a39bf90:


    "Apenas pude darle la vuelta a Ulyana a mi alrededor, así ella estaba en mi espalda y podría gatear a cuatro patas hacia la estantería más cercana, donde me desplomé y traté de coger un respiro."


translate spanish day4_us_morning_b01a480f:


    mz "¿De qué era? ¿Matemáticas cibernéticas...?{w} ¿O cibernética matemática...?"


translate spanish day4_us_morning_5e51dcff:


    th "Parece que no se dio cuenta de nosotros."


translate spanish day4_us_morning_266ebd21:


    el "Estaría interesado en algo como el bombardeo electrónico de los fotovoltaicos."


translate spanish day4_us_morning_dc097374:


    mz "¿Estás loco? ¿Por qué íbamos a tener algo militar?"


translate spanish day4_us_morning_6335428f:


    el "Eso no es del todo militar..."


translate spanish day4_us_morning_e0071b38:


    "Por un rato ambos se quedaron en silencio."


translate spanish day4_us_morning_861c8f5c:


    el "Zhenya..."


translate spanish day4_us_morning_6a6e65d4:


    mz "¿Qué?"


translate spanish day4_us_morning_ab9b10c0:


    el "Vamos a pasear por el río esta noche..."


translate spanish day4_us_morning_552a9040:


    mz "¿Para qué?"


translate spanish day4_us_morning_62e815fe:


    el "Bueno...{w} Sólo..."


translate spanish day4_us_morning_7f61d9af:


    mz "Tengo un montón de cosas que hacer...{w} Vamos, vete ya, que tu robot te está esperando."


translate spanish day4_us_morning_3323f7c2:


    el "Vale..."


translate spanish day4_us_morning_1e0523ce:


    "El golpe de la puerta al cerrar era como un epitafio para los males de amor de Electronik."


translate spanish day4_us_morning_67878bf4:


    th "Resulta que no es un robot de sangre fría después de todo."


translate spanish day4_us_morning_7efc1a56:


    "Solté una risita en silencio."


translate spanish day4_us_morning_c59fe92e:


    "De todas formas, reírse era algo un poco inapropiado. Tenemos que escapar de la biblioteca de alguna manera."


translate spanish day4_us_morning_89531da4:


    "Esperar hasta que Zhenya se fuera a desayunar, parecía la solución más fácil en ese momento."


translate spanish day4_us_morning_395caa92:


    "Después de todo, ella es una proletaria del trabajo comunista. No uno de los míticos robots de Electronik. Ella tiene que comer por lo menos en algún momento."


translate spanish day4_us_morning_5672d57d:


    "Mientras tanto, Ulyana no iba a despertarse."


translate spanish day4_us_morning_29b73a64:


    th "Por lo menos ya no está roncando más."


translate spanish day4_us_morning_9a87b38e:


    "La biblioteca estaba silenciosa."


translate spanish day4_us_morning_928c4608:


    "No vi a Zhenya, pero estaba más o menos satisfecha con dicha situación."


translate spanish day4_us_morning_ba7c4e5e:


    "Súbitamente, ruidos incomprensibles provenían de su escritorio: clicks, cracks, y...{w} la música empezó."


translate spanish day4_us_morning_70db9b38:


    "¡¿El himno soviético?!"


translate spanish day4_us_morning_ba1d06f5:


    th "¡Qué grande!"


translate spanish day4_us_morning_c14cb5ff:


    "Hubiera estado bien si no fuera por Zhenya, ¡quien comenzó a cantar a su vez!"


translate spanish day4_us_morning_c94f8616:


    mz "Unidos por siempre en fraternidad y trabajo..."


translate spanish day4_us_morning_75088394:


    "Solamente podía envidiar sus sentimientos patrióticos."


translate spanish day4_us_morning_06f8fd05:


    "Desafortunadamente, desafinaba un poco su voz. La escena pop soviética definitivamente no se perdió ninguna estrella llamada Zhenya."


translate spanish day4_us_morning_9877cbdf:


    "El himno finalizó y una voz desconocida empezó a hablar sobre los grandes logros de un plan quinquenal de cosecha de trigo."


translate spanish day4_us_morning_26511e70:


    "Era una radio, obviamente."


translate spanish day4_us_morning_2d3e165e:


    "Comencé a escuchar con atención. Tal vez dijeran algo de interés."


translate spanish day4_us_morning_526eb40c:


    "Pero tras la lista de logros agriculturales, la voz empezó a desaparecer y se fue en pocos minutos."


translate spanish day4_us_morning_13ff876e:


    th "Alguna distorsión o anomalía en la señal, probablemente."


translate spanish day4_us_morning_a0c04095:


    "Zhenya se puso de pie y se dirigió hacia donde estábamos."


translate spanish day4_us_morning_9e8818da:


    "La situación se nos escapaba de las manos."


translate spanish day4_us_morning_356a32e8:


    "Me las arreglé para soltarme del abrazo de Ulyana con un esfuerzo titánico."


translate spanish day4_us_morning_079daa56:


    "Estaba libre para escapar, pero estaba tan apretado de estar agachado que ya no tenía fuerzas para ponerme en pie."


translate spanish day4_us_morning_86aec307:


    th "Es la hora de prepararse para lo peor y comenzar a pensar en excusas..."


translate spanish day4_us_morning_a755654d:


    "De repente los pasos se detuvieron."


translate spanish day4_us_morning_d2163297:


    "Parecía ser que Zhenya estaba en el otro lado de la estantería."


translate spanish day4_us_morning_f120f183:


    "Escuché el ruido del crujir de libros."


translate spanish day4_us_morning_ce9a6b7b:


    th "Probablemente, estaba buscando alguna cosa."


translate spanish day4_us_morning_9743d34f:


    "Cogió un libro y retornó a su escritorio."


translate spanish day4_us_morning_f06c5e02:


    "La puerta se abrió de golpe."


translate spanish day4_us_morning_3cf16294:


    mt "¡¿Semyon?!{w} ¿Les has visto?"


translate spanish day4_us_morning_5fd5cacf:


    "Olga Dmitrievna estaba definitivamente sin aliento."


translate spanish day4_us_morning_5419bf5d:


    mt "¡¿Ulyana?!"


translate spanish day4_us_morning_ac7585a7:


    mz "No..."


translate spanish day4_us_morning_80c6622f:


    "Zhenya respondió con sorpresa."


translate spanish day4_us_morning_519eac8f:


    "La puerta se cerró de golpe tan ruidosamente como la vez anterior."


translate spanish day4_us_morning_8adc70f4:


    "Pareciera que todo el mundo nos está buscando, y Zhenya comenzará a buscar en los alrededores de la biblioteca..."


translate spanish day4_us_morning_2dc3e10c:


    "Pero afortunadamente, justo en ese momento la campana sonó en la distancia, avisando a los pioneros para el desayuno."


translate spanish day4_us_morning_4d1c12f2:


    "Zhenya, siendo una persona puntual, decidió no perder el tiempo y abandonó la biblioteca en pocos minutos. Ahora sólo estábamos yo y Ulyana en la biblioteca."


translate spanish day4_us_morning_35353818:


    "Así que era la hora de escoger qué hacer con ella."


translate spanish day4_us_morning_195ba3f0:


    "No preocupado más por ser descubierto, me incliné hacia ella y le grité al oído."


translate spanish day4_us_morning_e501c3ad:


    me "¡Lázaro, levántate y anda!"


translate spanish day4_us_morning_e9fe52a8:


    "Al instante se puso de pie y empezó a mirar a su alrededor."


translate spanish day4_us_morning_a94e04aa:


    "Percatándose de mi, los ojos de Ulyana se pusieron como platos."


translate spanish day4_us_morning_ba2cf72c:


    us "¿Qué estás haciendo aquí?"


translate spanish day4_us_morning_571b4ce2:


    me "Jugando a espías."


translate spanish day4_us_morning_ed803309:


    us "¿Eh?"


translate spanish day4_us_morning_e29d2e60:


    me "Olvídalo... ¿Has dormido bien?"


translate spanish day4_us_morning_41c2189c:


    us "Sí..."


translate spanish day4_us_morning_feb4bc5f:


    "Me parece que no está completamente despierta."


translate spanish day4_us_morning_d2228502:


    me "¿Desayunamos?"


translate spanish day4_us_morning_41c2189c_1:


    us "Sí..."


translate spanish day4_us_morning_8bceae14:


    "Dejamos atrás la biblioteca."


translate spanish day4_us_morning_d5d18ead:


    th "¡Al fin!"


translate spanish day4_us_morning_9eb55c9a:


    th "Ahora ya estaba seguro de que no tendría que dar explicaciones a nadie sobre qué, cómo y por qué Ulyana y yo pasamos toda la noche ahí..."


translate spanish day4_us_morning_71de8971:


    us "Perdona por dormirme de esa forma..."


translate spanish day4_us_morning_6d36c28b:


    me "Está bien, no pasa nada."


translate spanish day4_us_morning_9b91052f:


    "Supongo que mis palabras sonaron demasiado insinceras, ya que ella me miró con cara de incredulidad."


translate spanish day4_us_morning_4322ee3d:


    us "Espera un minuto...{w} ¿Qué estabas haciendo allí todo este tiempo?"


translate spanish day4_us_morning_75f4b174:


    me "No me creerías."


translate spanish day4_us_morning_b515de5d:


    us "Espera un momento...{w} Eso significa..."


translate spanish day4_us_morning_4f6d2aa7:


    "Ulyana soltó una risita, dio un paso atrás y me gritó fuertemente:"


translate spanish day4_us_morning_abd7e09d:


    us "¡Soy la primera para el desayuno!"


translate spanish day4_us_morning_74c13f1d:


    me "No era una gran sorpresa tras semejante profundo silencio..."


translate spanish day4_us_morning_72667b52:


    "Dije, pero Ulyana ya no me podía escuchar, estando tan alejada."


translate spanish day4_us_morning_eaabbc69:


    "Había una gran e inusual multitud de gente cerca de la cantina."


translate spanish day4_us_morning_2260fedb:


    "Por supuesto, no había otro lugar en el campamento que a los pioneros les gustara tanto como la cantina, pero ¿por qué se amontonaban en el porche...?"


translate spanish day4_us_morning_f5aef4ac:


    "Me acerqué para averiguar qué era lo que ocurría."


translate spanish day4_main1_3fe8cd89:


    "Parecía que todo el campamento se había reunido en el porche: estaban todas las muchachas conocidas, Olga Dmitrievna y Electronik."


translate spanish day4_main1_0c95f62a:


    "Estaban teniendo una conversación muy animada."


translate spanish day4_main1_6785b764:


    "Me aproximé."


translate spanish day4_main1_c74edda5:


    mt "¡Ah, Semyon!"


translate spanish day4_main1_af775fc1:


    mt "¡¿Dónde demonios has estado?!{w} ¡Te estuve esperando toda la noche y te he estado buscando desde la madrugada!{w} Ulyana me dijo que os fuisteis juntos de la biblioteca ayer."


translate spanish day4_main1_9d5d157b:


    "Miré a Ulyana.{w} Estaba sonriendo acrimoniosamente."


translate spanish day4_main1_51149e68:


    me "..."


translate spanish day4_main1_2f165ff9:


    mt "Bueno, ¡nos ocuparemos de esto más tarde!"


translate spanish day4_main1_4c6e2dcd:


    mt "¿Has visto a Shurik hoy?"


translate spanish day4_main1_3aabe659:


    me "Nop, ¿qué pasa?"


translate spanish day4_main1_28e30b6d:


    mt "¡No le hemos podido encontrar ya desde madrugada!"


translate spanish day4_main1_70b53a62:


    th "Pioneros desapareciendo... bueno, eso es algo nuevo."


translate spanish day4_main1_547a39ce:


    mt "Pero él estaba contigo ayer, ¿no?"


translate spanish day4_main1_1e640e7b:


    "Ella se dirigía a Electronik."


translate spanish day4_main1_8fb4fd1c:


    el "Sí, él estaba..."


translate spanish day4_main1_7c91c87b:


    mt "Entonces, te levantas esta mañana y... ¿ya no estaba en ninguna parte?"


translate spanish day4_main1_62543f01:


    el "No..."


translate spanish day4_main1_30b2a809:


    mt "¿Por qué no viniste a mi inmediatamente?"


translate spanish day4_main1_9f2b5808:


    el "Bueno, creí que se levantó pronto para ir a asearse o algo así..."


translate spanish day4_main1_a5b6d3cb:


    sl "¿Mencionó alguna cosa ayer?"


translate spanish day4_main1_f2c87db5:


    "Preguntó Slavya interponiéndose."


translate spanish day4_main1_c620bee1:


    el "¿Por ejemplo?"


translate spanish day4_main1_01cfc02e:


    sl "Pues por ejemplo, si se iba a alguna parte..."


translate spanish day4_main1_62543f01_1:


    el "Nop..."


translate spanish day4_main1_cd7d9ef9:


    me "¿Qué tiene de malo exactamente todo esto?{w} Solamente son las nueve de la mañana. Quizá decidió darse un paseo."


translate spanish day4_main1_6059e846:


    sl "No conoces a Shurik."


translate spanish day4_main1_453d16cb:


    "Me miró seriosamente."


translate spanish day4_main1_66a4843a:


    me "Bueno no, yo no..."


translate spanish day4_main1_66774de2:


    "Pero no veía nada de sospechoso en esta situación."


translate spanish day4_main1_fac4579f:


    mt "Vale, dejemos de ponernos histéricos. ¡Le buscaremos!"


translate spanish day4_main1_d68ecb78:


    us "¿Cómo podría perderse el desayuno?"


translate spanish day4_main1_56aa99aa:


    "Ulyana sonrió."


translate spanish day4_main1_3fab6262:


    dv "¡Exactamente! Es la hora de comer de todas formas."


translate spanish day4_main1_a24b7420:


    "Los pioneros procedieron a entrar en la cantina."


translate spanish day4_main1_7a51fac4:


    "Pero yo fui detenido por Olga Dmitrievna."


translate spanish day4_main1_29b74cfc:


    mt "Y tú, Semyon, por favor espera aquí."


translate spanish day4_main1_e79d0fd5:


    me "Claro..."


translate spanish day4_main1_d124d112:


    mt "¿Podrías tener el detalle de explicar dónde has estado por la noche?"


translate spanish day4_main1_f883183e:


    me "Bueno..."


translate spanish day4_main1_597e6c44:


    "Este es el tipo de situación en el que no había pensado antes..."


translate spanish day4_main1_7a46a669:


    "La verdad, no podía haber pasado desapercibido. Debería al menos haber pensado en algún tipo de explicación clara."


translate spanish day4_main1_538bd425:


    "Pero no lo hice..."


translate spanish day4_main1_8f6d4306:


    me "Bueno, yo...{w} Yo y Ulyana estábamos colocando los libros en las estanterías de la biblioteca. ¡Pero entonces me encerró ahí y se fue corriendo! ¡Estuve atrapado allí hasta esta mañana!"


translate spanish day4_main1_a1c3fbc7:


    mt "¡Estuve hoy en la biblioteca!{w} Y no te vi allí."


translate spanish day4_main1_3f76b658:


    th "Soy consciente de ello."


translate spanish day4_main1_c0f84c22:


    me "Bueno...{w} Me fui silenciosamente."


translate spanish day4_main1_41bc7385:


    mt "¿Y por qué está Ulyana contándome algo completamente diferente?"


translate spanish day4_main1_2ff7837a:


    th "Mmm, me pregunto qué puede haberle contado..."


translate spanish day4_main1_2f2b2114:


    me "¡Ya sabes cómo son sus historias!"


translate spanish day4_main1_d27d2beb:


    mt "Sí, quizás estás en lo cierto..."


translate spanish day4_main1_d4764288:


    "La líder del campamento hizo una pausa por un instante."


translate spanish day4_main1_fe53b407:


    mt "¡No pienses que eso significa que te vaya a creer a ti!"


translate spanish day4_main1_f9dd954e:


    "No pensé eso..."


translate spanish day4_main1_18c37203:


    mt "Vale, nos ocuparemos de ello más tarde, ¡no lo olvidaré!{w} Es más importante encontrar a Shurik ahora."


translate spanish day4_main1_9513cd87:


    me "Claro..."


translate spanish day4_main1_7d563950:


    mt "Ya te puedes ir..."


translate spanish day4_main1_af8e37ef:


    "Una vez más, no tenía ningún lugar que escoger para sentarme. Los únicos asientos libres estaban en la mesa con Alisa y Ulyana."


translate spanish day4_main1_ed08ccfd:


    us "Coge asiento."


translate spanish day4_main1_ebb45b6a:


    "Señalaba la silla al lado suyo."


translate spanish day4_main1_04d910fd:


    "Me senté."


translate spanish day4_main1_1a2c041d:


    "Alisa estaba como de costumbre. No parecía que fuera a comenzar una pelea conmigo por perderme su actuación que previamente había prometido ir a ver."


translate spanish day4_main1_36580956:


    th "Bueno, si a ella no le gusta hablar sobre ello, entonces... ¿por qué hacerlo yo?"


translate spanish day4_main1_b7584acc:


    us "¿No vas a ir a coger tu comida?"


translate spanish day4_main1_97ba4a43:


    th "¡Buena idea!{w} No había pensado sobre eso... "


translate spanish day4_main1_e2f039bb:


    "El desayuno de hoy no parece muy diferente del de ayer, pero parecía más apetitoso."


translate spanish day4_main1_ca4c8a18:


    "O quizá tan sólo estaba muy hambriento."


translate spanish day4_main1_f0e3f8e3:


    "O tal vez estaba impaciente de terminármelo tan pronto como fuera posible con tal de evitar otra broma de Ulyana."


translate spanish day4_main1_15c24207:


    us "¿Quieres ir hoy a la playa con nosotras?"


translate spanish day4_main1_18dfa619:


    me "¿Cuándo?"


translate spanish day4_main1_516609bc:


    us "Justo después de desayunar."


translate spanish day4_main1_40ae9715:


    "A decir verdad, relajarme no estaba en mis planes de hoy, pero si iba a pasarme el tiempo pensando, ¿por qué no debería hacerlo mientras me bronceo?"


translate spanish day4_main1_c1286eed:


    me "Claro, por qué no..."


translate spanish day4_main1_a64a24e7:


    us "¡Ah, guay!"


translate spanish day4_main1_b7bd4e35:


    "Me sonrió simpáticamente."


translate spanish day4_main1_2cb6f965:


    me "Apuesto a que estás preparando otra broma..."


translate spanish day4_main1_e4b476c4:


    us "¡No, no del todo!"


translate spanish day4_main1_dd0d143d:


    "Ulyana agitó sus manos."


translate spanish day4_main1_6d3417f3:


    dv "¡Sin lugar a dudas!"


translate spanish day4_main1_08dcf315:


    "Alisa hizo una sonrisa."


translate spanish day4_main1_232ce05b:


    dv "¡Ella siempre es así!"


translate spanish day4_main1_27304fd6:


    "Miré con atención a Alisa y de repente me acordé de ayer por la noche."


translate spanish day4_main1_a2cb95d1:


    "Su extraño comportamiento, nuestra discusión."


translate spanish day4_main1_35ddcb27:


    "Pero ahora se le veía como de costumbre."


translate spanish day4_main1_c3ed033d:


    th "¿Quizá no hay necesidad de decirle nada?"


translate spanish day4_main1_c2e8c7a9:


    "A pesar de que no fui."


translate spanish day4_main1_b56982ca:


    "Al final, todo lo que ocurrió esa noche dejó de parecer poco corriente, como si todo fuera tan sólo un sueño."


translate spanish day4_main1_6c304510:


    us "¡De ninguna manera!"


translate spanish day4_main1_1ca23a9b:


    me "Creo que tiene razón."


translate spanish day4_main1_53340723:


    us "¡Nadie me cree!"


translate spanish day4_main1_d1036c99:


    "Ulyana agarró su bandeja de platos sucios y se puso de pie."


translate spanish day4_main1_0a9f12c1:


    us "¡Buen provecho!"


translate spanish day4_main1_39b14315:


    "Lo dijo de una forma que no dejaba lugar a dudas... nada bueno me espera hoy en la playa."


translate spanish day4_main1_f86ec2e1:


    "Yo y Alisa nos quedamos solos."


translate spanish day4_main1_37176700:


    me "Sabes, supongo que no debía ir contigo después de todo."


translate spanish day4_main1_2d5beca1:


    dv "¿Y eso por qué?"


translate spanish day4_main1_8d93196b:


    me "Bueno, tengo algo que hacer..."


translate spanish day4_main1_844c800d:


    dv "¿Y qué es eso exactamente que deberías hacer?"


translate spanish day4_main1_d12f273c:


    "Me miró a los ojos y no podía ni tan siquiera encontrar una excusa apropiada."


translate spanish day4_main1_6517fda2:


    me "Tampoco tengo un bañador."


translate spanish day4_main1_da8226ff:


    dv "Prueba uno mío."


translate spanish day4_main1_e10064b1:


    me "¿Acaso crees que me va?"


translate spanish day4_main1_8d7ab4dd:


    dv "¡Dale una oportunidad!"


translate spanish day4_main1_1f304310:


    me "Creo que no vale la pena intentarlo..."


translate spanish day4_main1_3479d749:


    dv "¡No seas mequetrefe!{w} ¡Te buscaremos algún bañador!"


translate spanish day4_main1_54a8822f:


    "Aquellas palabras me hicieron temer su idea aun más."


translate spanish day4_main1_0547c124:


    dv "Espérame por la cantina, volveré pronto."


translate spanish day4_main1_505d91ae:


    me "Vale..."


translate spanish day4_main1_850a4c6e:


    th "No hay nada de malo en esperarla, ¿verdad?"


translate spanish day4_main1_6488a41b:


    "Al terminar mi desayuno, abandoné el edificio y me senté en las escaleras."


translate spanish day4_main1_52db742c:


    "Pioneros, todos ocupados con sus propios asuntos, con prisas me dejaron atrás, uno por uno."


translate spanish day4_main1_6db745af:


    "Ninguno se detuvo por mi.{w} Tampoco ninguno miró en mi dirección."


translate spanish day4_main1_3594b82e:


    "Parecía que ellos no me consideraban un extraño, sino todo lo contrario... tan sólo otro adolescente de su edad, su compañero, se podría decir."


translate spanish day4_main1_15995c83:


    "Fui consciente de estar pensando que comencé a considerar este campamento y sus lugareños sin tanta cautela como hice el primer día."


translate spanish day4_main1_750c0871:


    "Por supuesto, Alisa volvió en un par de minutos."


translate spanish day4_main1_1d6a18c3:


    dv "¿Preparado?"


translate spanish day4_main1_eaccb1bc:


    me "¿Preparado para qué?"


translate spanish day4_main1_82448915:


    "Me ofreció el bañador...{w} A pesar de que difícilmente se le podría llamar bañador..."


translate spanish day4_main1_d7b10247:


    "El bañador era como unos calzoncillos de color rosa decorados con mariposas y flores.{w} En realidad, {i}eran{/i} unos calzoncillos."


translate spanish day4_main1_956c7f92:


    me "Estoy algo preocupado como para preguntártelo, pero... ¿dónde conseguiste {i}esto{/i}?"


translate spanish day4_main1_6993170f:


    dv "¿Tienes demasiado miedo de ponértelos?"


translate spanish day4_main1_009490c9:


    me "Bueno, ya sabes...{w} No tengo la más mínima intención de ponérmelos."


translate spanish day4_main1_398d5637:


    "Apreciaba su broma, pero no iba hacer de mi el hazmerreír."


translate spanish day4_main1_f3133a95:


    dv "¡Póntelos!"


translate spanish day4_main1_8736d85c:


    "Dijo de forma brusca."


translate spanish day4_main1_57346f05:


    me "¿Por qué no te los pruebas tú en mi lugar?{w} ¡Creo que el color de este bañador realzaría tus ojos muy hermosamente!"


translate spanish day4_main1_37b30d8c:


    dv "¡Hagamos una apuesta!"


translate spanish day4_main1_6a537964:


    "No tenía deseo alguno de aceptar sus apuestas."


translate spanish day4_main1_1498c76e:


    me "No, gracias."


translate spanish day4_main1_b5ac4c3c:


    dv "Muy bien, una de dos o vas a la playa llevándolos puestos, ¡o nadas desnudo!"


translate spanish day4_main1_a6fdaa06:


    th "Sopesando los puntos a favor y en contra, incluso la segunda opción me parece mejor que la primera."


translate spanish day4_main1_1b8d0016:


    me "¡No tengo intenciones de ir a ninguna parte!"


translate spanish day4_main1_b43b3d6a:


    dv "¡Pues entonces le diré a todo el mundo que te dejaste estos calzoncillos en mi cabaña!"


translate spanish day4_main1_66ba2af9:


    me "¿Por qué haría yo eso?"


translate spanish day4_main1_37160fae:


    dv "¿Cómo podría saberlo yo?"


translate spanish day4_main1_42be2b6f:


    "Explotó en carcajadas."


translate spanish day4_main1_9efe078e:


    "No quería discutir con Alisa, por lo que al final decidí ir a la playa."


translate spanish day4_main1_14c26ca5:


    "Pero no con ese extravagante bañador."


translate spanish day4_main1_74372f49:


    me "Vale. Estaré aquí en diez minutos."


translate spanish day4_main1_ea4299c9:


    dv "¡No tardes!"


translate spanish day4_main1_3dfe79f9:


    "Se fue corriendo tras decir eso."


translate spanish day4_main1_77b838f6:


    "Arrastré mis pasos hasta la cabaña de la líder para coger una toalla y con suerte hallar alguna cosa como bañador."


translate spanish day4_main1_cdc7b7a7:


    "Olga Dmitrievna me estaba esperando en la habitación."


translate spanish day4_main1_044559b3:


    mt "Semyon, ¿has escuchado algo acerca Shurik?"


translate spanish day4_main1_d45464b9:


    me "Lo mismo que hace media hora atrás..."


translate spanish day4_main1_edefb213:


    "Caminé hasta mi cama y agarré una toalla."


translate spanish day4_main1_d024b380:


    mt "¿Vas a alguna parte?"


translate spanish day4_main1_cb685aa8:


    me "Sí, a la playa."


translate spanish day4_main1_850934a0:


    mt "Espera. ¿Tienes algún bañador?{w} Viniste aquí sin equipaje creo recordar..."


translate spanish day4_main1_82f7a16c:


    "Qué raro, ese hecho no pareció sorprenderla durante nuestro primer encuentro."


translate spanish day4_main1_5cb822ee:


    me "No..."


translate spanish day4_main1_f01befe8:


    mt "Entonces, ¿qué vestirás?"


translate spanish day4_main1_9da88e5d:


    th "Buena pregunta. ¿Qué?"


translate spanish day4_main1_7479b07e:


    me "No lo sé..."


translate spanish day4_main1_22a42eb2:


    mt "Espera un segundo."


translate spanish day4_main1_81a73d46:


    "Caminó hacia el armario y abrió un cajón."


translate spanish day4_main1_b666fe0c:


    "En un instante tenía un par de bañadores negros normal y corrientes."


translate spanish day4_main1_21d21de6:


    th "¿De dónde los consiguió?{w} Y aun más relevante... ¿por qué?"


translate spanish day4_main1_e922975f:


    "Alguien de la pasada temporada debió olvidarlos..."


translate spanish day4_main1_b2cd5380:


    "Considerando todas las cosas raras en este campamento, no era realmente sorprendente encontrar unos bañadores de hombre en la habitación de Olga Dmitrievna."


translate spanish day4_main1_3066598f:


    me "Gracias."


translate spanish day4_main1_86258ed4:


    "Los bañadores eran justo de mi talla."


translate spanish day4_main1_a20cefa7:


    "..."


translate spanish day4_main1_742830e4:


    "Me cambié en la cabaña y me fui a la playa."


translate spanish day4_main1_cf92b9ce:


    "Un montón de pioneros ya se habían concentrado aquí, pero sólo reconocí a Alisa y Ulyana."


translate spanish day4_main1_170351a5:


    us "¡Acércate!"


translate spanish day4_main1_a0ef15ef:


    "Caminé hacia ellas y me senté en la arena."


translate spanish day4_main1_27f5a44d:


    dv "Veo que encontraste algo mejor..."


translate spanish day4_main1_99336af1:


    "Ella se fijó en mi bañador y sonrió satisfecha."


translate spanish day4_main1_5e648c4f:


    me "Como puedes ver...."


translate spanish day4_main1_0eed83b2:


    us "¡Vamos, nademos!"


translate spanish day4_main1_d1aa6677:


    me "No quiero.{w} Tal vez más tarde..."


translate spanish day4_main1_d3a86de3:


    "No era un gran entusiasta de la natación."


translate spanish day4_main1_ba1e125e:


    dv "Bueno, haz lo que quieras."


translate spanish day4_main1_a6eddd01:


    "Las muchachas salieron corriendo hacia el agua."


translate spanish day4_main1_521b9f2c:


    th "¿Por qué vine aquí?{w} ¿Por qué no estoy por ahí buscando respuestas...?"


translate spanish day4_main1_a50d7f70:


    th "¿Debería preocuparme por ello ahora?"


translate spanish day4_main1_a482e6e3:


    "Sovyonok me parecía normal."


translate spanish day4_main1_b9b33704:


    "En verdad, muchas cosas extrañas me habían sucedido durante estos tres días y medio, pero ninguno de ellos, tomados por separado, no parecían sobrenaturales en absoluto.{w} Especialmente teniendo en cuenta que no me acerqué para nada a algo parecido a una pista."


translate spanish day4_main1_cc830959:


    "Por el contrario, todo lo que ha ocurrido únicamente me ha confundido aun más."


translate spanish day4_main1_4795bad4:


    th "¿Qué otras alternativas tengo?"


translate spanish day4_main1_662e06d0:


    th "No obtendré ninguna verdad de los lugareños.{w} A pesar de que incluso lo sepan..."


translate spanish day4_main1_4ed7892c:


    th "¿Debería intentar escapar de este lugar?"


translate spanish day4_main1_8000702a:


    th "Pero, ¿cuán lejos puedo caminar, considerando que tampoco sé dónde estoy?"


translate spanish day4_main1_e75df2a1:


    th "Parece ser que mi única opción es esperar."


translate spanish day4_main1_4f43f435:


    "Tras un rato las muchachas volvieron.{w} Ulyana tenía alguna cosa en sus manos."


translate spanish day4_main1_30f4be0c:


    us "¡Mira!"


translate spanish day4_main1_06938095:


    "Lo observé y vi un cangrejo.{w} Tan sólo un cangrejo normal."


translate spanish day4_main1_456ae98b:


    "Ulyana se tumbó a mi lado y comenzó a atormentarlo."


translate spanish day4_main1_dd41af79:


    me "¡Deja al pobre animal solo!"


translate spanish day4_main1_3b3d6cb9:


    us "¡¿Por qué?!{w} ¡Sólo es un cangrejo!"


translate spanish day4_main1_c9e60657:


    me "¿Y qué?{w} ¡Tiene derecho a vivir también!"


translate spanish day4_main1_ea38a367:


    us "¡Le arrancaré las pinzas y le pediré al cocinero que me lo haga hervido para la cena!"


translate spanish day4_main1_68e2c448:


    me "Como si no hubiera nada para comer..."


translate spanish day4_main1_4a330faf:


    "Miré a Alisa.{w} Parecía estar totalmente desinteresada en la diversión de Ulyana con la pobre criatura marina."


translate spanish day4_main1_411121e6:


    me "¡Díselo!"


translate spanish day4_main1_86658590:


    dv "¿Qué hay de malo?{w} Es un cangrejo, ¡se lo merece!"


translate spanish day4_main1_6a626905:


    "Parece que las muchachas se perdieron sus lecciones sobre naturaleza en la escuela primaria y carecían de empatía por el medioambiente..."


translate spanish day4_main1_679b46e1:


    me "¡Déjalo!"


translate spanish day4_main1_4ec958c5:


    "Le arrebaté el cangrejo a Ulyana."


translate spanish day4_main1_4949cf90:


    us "Haz lo que quieras..."


translate spanish day4_main1_80d64677:


    "Me sorprendí un poco de que no opusiera resistencia."


translate spanish day4_main1_56d514c5:


    "Miré a los ojos del pobre animal."


translate spanish day4_main1_c21fcd5d:


    "No expresaban nada, pero creía que si pudieran hablar, estaría realmente enfadado, tal vez incluso iría a la Convención de Derechos Humanos de las Naciones Unidas."


translate spanish day4_main1_70c26240:


    "A decir verdad, no estaba muy seguro de que eso ayudase..."


translate spanish day4_main1_4595a707:


    "Dejé libre el cangrejo en el río."


translate spanish day4_main1_53896b88:


    us "No importa, atraparé más. Hay montones de ellos por aquí."


translate spanish day4_main1_45927c46:


    "Dijo Ulyana."


translate spanish day4_main1_972f86d7:


    me "No lo dudo..."


translate spanish day4_main1_ca01a7fa:


    "Murmuré."


translate spanish day4_main1_b18fe1e2:


    "El tiempo pasó y me estaba adormeciendo."


translate spanish day4_main1_357544c9:


    "Me quedé dormido."


translate spanish day4_main1_0c650a7c:


    "No recuerdo qué estaba soñando, si es que estaba soñando, pero me desperté cuando alguien me sacudió por los hombros."


translate spanish day4_main1_3c1fbed3:


    "Olga Dmitrievna permanecía a mi lado."


translate spanish day4_main1_517d89d0:


    me "¿Has venido a nadar tú también?"


translate spanish day4_main1_fd959960:


    "Pregunté, todavía medio dormido."


translate spanish day4_main1_da0111bc:


    mt "No.{w} Es casi la hora de comer y aun no hemos podido encontrar a Shurik."


translate spanish day4_main1_82a4bab8:


    "Dijo la líder, detrás de mí en un traje de baño húmedo."


translate spanish day4_main1_f95c9fc9:


    me "¿Y?"


translate spanish day4_main1_1df88814:


    mt "Quiero que le busques."


translate spanish day4_main1_7cc521ca:


    me "¿Qué pasa, es que soy el único pionero en todo este campamento?"


translate spanish day4_main1_5581e203:


    "Sinceramente, estaba enojado."


translate spanish day4_main1_ccbda48f:


    "Cada vez iba siendo más claro que Olga Dmitrievna me consideraba su muchacho de los recados, para ser usado como un esclavo.{w} O viceversa..."


translate spanish day4_main1_89e79303:


    mt "Si me acerco a ti es porque quiero que tú me ayudes."


translate spanish day4_main1_5aa2346a:


    "¿Por qué exactamente yo?"


translate spanish day4_main1_aad225f0:


    "Sin embargo, tras darle algunas vueltas, decidí estar de acuerdo."


translate spanish day4_main1_2186b207:


    "Dspués de todo, mis hombros y espalda se broncearon mientras estaba durmiendo, y buscar a Shurik me familiarizaría con más lugares inexplorados del campamento."


translate spanish day4_main1_505d91ae_1:


    me "Vale..."


translate spanish day4_main1_b55d2be6:


    "No era precisamente correcto vagar por ahí vistiendo sólo un bañador, por lo que debería primero cambiarme."


translate spanish day4_main1_a20cefa7_1:


    "..."


translate spanish day4_main1_97b8a867:


    "Diez minutos más tarde estaba fuera de la cabaña de Olga Dmitrievna, decidiendo adónde ir."


translate spanish day4_busstop_84a6a4dc:


    "La parada del autobús resultaba ser una buena elección."


translate spanish day4_busstop_310157f5:


    "Un pensamiento extraño me vino a la mente... quizá Shurik está en la misma situación que yo y por ello se escapó del campamento, tal vez con la línea de autobús 410."


translate spanish day4_busstop_ae9e2dee:


    "Podría llegar a ser verdad, si él hubiera llegado aquí {i}por accidente{/i} también."


translate spanish day4_busstop_2832326d:


    "A pesar de todo las posibilidades de que eso sea así son bastante bajas."


translate spanish day4_busstop_6d68af7d:


    th "¡Pero nunca sabes!"


translate spanish day4_busstop_5a9376e0:


    th "Y si el autobús realmente llegase..."


translate spanish day4_busstop_794a9280:


    "Sin embargo, difícilmente podría creer en eso."


translate spanish day4_busstop_d2b67202:


    "Por supuesto, pasé un par de minutos esperando en la parada del autobús, asegurándome de que no había rastro de Shurik u otra persona, y me fui de vuelta al campamento."


translate spanish day4_busstop_3f3e9168:


    "Alguien apareció corriendo por el portal del campamento y se chocó conmigo.{w} El impacto no fue grande, por lo que tan sólo me quedé estupefacto."


translate spanish day4_busstop_548da2a4:


    "Miku estaba delante de mí y frotándose su lastimada frente."


translate spanish day4_busstop_37374d4c:


    me "Oh, perdona..."


translate spanish day4_busstop_7459a96f:


    mi "¡No pasa nada! ¡Es culpa mía! Estaba yendo al club de música, pero me despisté pensando en una nueva canción... Ya sabes, pensando en la letra y la música. No me di cuenta de cómo llegué hasta aquí."


translate spanish day4_busstop_a9298d73:


    mi "¡Así que no tienes por qué disculparte!"


translate spanish day4_busstop_03f805f3:


    "Su cháchara de palabras por minuto obviamente desbordaban mi capacidad para comprender cosas en mi mente."


translate spanish day4_busstop_513cdea5:


    "Hice un intento de retirarme sin demora."


translate spanish day4_busstop_e170ad8d:


    me "Claro, claro... Me tengo que ir... Ya sabes..."


translate spanish day4_busstop_206044ed:


    mi "¡Oh, espera!"


translate spanish day4_busstop_6eecb271:


    "Me quería ir como siempre sin tener que escucharla, pero Miku me cogió de la mano."


translate spanish day4_busstop_71a0a5fc:


    "Su agarrón me dio tal inquietud como que tuve la clara visión de ser ejecutado agonizantemente por otra sesión de rap."


translate spanish day4_busstop_14425216:


    mi "¿Puedes ayudarme un momentín, por favor? ¿Solamente un poquitín?"


translate spanish day4_busstop_80eb41cf:


    "Definitivamente eso no estaba en mi lista de cosas a hacer hoy."


translate spanish day4_busstop_d90e19df:


    me "Bueno, estaría encantado, pero es que..."


translate spanish day4_busstop_ffd83896:


    mi "¡Oh, por favor!"


translate spanish day4_busstop_74c53f6a:


    "Miku me miraba con tales ojos de cachorrito que el corazón se me partía de debilidad."


translate spanish day4_busstop_075057b4:


    "Tampoco me iba soltar la mano de ninguna manera."


translate spanish day4_busstop_d6c51513:


    me "¿Y qué ayuda necesitas exactamente?"


translate spanish day4_busstop_855dcd0b:


    mi "¡Me acompañarás! ¡No me las puedo arreglar para tocar una canción por mi misma! Puedo cantar. O tocar. Pero no puedo hacer ambas a la vez."


translate spanish day4_busstop_4c33c840:


    th "Incluso nuestra diva tiene sus puntos débiles."


translate spanish day4_busstop_4942eb5b:


    me "Bueno, ya sabes, no puedo tocar realmente cualquier instrumento..."


translate spanish day4_busstop_9f6720fa:


    mi "¡Olvídate de eso! ¡Te lo enseñaré todo! ¡Vamos, vayámonos!"


translate spanish day4_busstop_4ccac507:


    th "Por otra parte, tampoco me pierdo nada."


translate spanish day4_busstop_e7714637:


    "Sin embargo..."


translate spanish day4_busstop_e2956e63:


    me "Ya sabes, debería estar buscando a Shurik y todo eso..."


translate spanish day4_busstop_48f85b22:


    "Miku me echó una mirada frustrada."


translate spanish day4_busstop_2554171f:


    mi "¡Sólo por un poquito!"


translate spanish day4_busstop_0d2840a0:


    "No hallaba palabras..."


translate spanish day4_busstop_be6c9d1e:


    "Me arrastró con ella."


translate spanish day4_busstop_59712563:


    th "Probablemente nada bueno salga de esto, pero la única forma de zafarme de ella es apartar mi mano de ella a la fuerza."


translate spanish day4_busstop_0dd57599:


    "Pero eso no sería muy agradable."


translate spanish day4_busstop_161f1f1a:


    th "¡Nada malo puede ocurrir, después de todo!{w} Probablemente..."


translate spanish day4_busstop_82406965:


    "En un minuto ya estábamos frente al club de música."


translate spanish day4_busstop_c75b785f:


    "Miku cogió una guitarra."


translate spanish day4_busstop_d8313998:


    mi "¡Atento, echa un vistazo!"


translate spanish day4_busstop_ddd216e1:


    "Se sentó y comenzó a tocar."


translate spanish day4_busstop_62735169:


    "Traté de seguir sus manos.{w} La melodía parecía bastante sencilla."


translate spanish day4_busstop_074f6095:


    "Parecía bastante fácil de repetir."


translate spanish day4_busstop_d28a7a64:


    mi "¿Comprendiste?"


translate spanish day4_busstop_b4dbe06d:


    me "Más o menos."


translate spanish day4_busstop_39da62e2:


    mi "Inténtalo."


translate spanish day4_busstop_4bfbb8ba:


    "Cogí la guitarra y empecé a tocar."


translate spanish day4_busstop_b59ab969:


    "Realmente no me salió bien..."


translate spanish day4_busstop_3865d2b8:


    mi "Déjame que te enseñe una vez más."


translate spanish day4_busstop_045ce299:


    "Tocaba mucho mejor que yo."


translate spanish day4_busstop_4db3b5a0:


    "Mirando a Miku, me asombré."


translate spanish day4_busstop_ac84947f:


    th "Claro que era una cotorra, era ingenua y torpe...{w} Aun así, ella era extremadamente talentosa en la música."


translate spanish day4_busstop_1e78833e:


    mi "Inténtalo de nuevo."


translate spanish day4_busstop_dfcc8234:


    "La manejé mucho mejor la segunda vez."


translate spanish day4_busstop_763aacd2:


    mi "¡Guau, eso está mucho mejor!"


translate spanish day4_busstop_d1046a49:


    "Ella sonrió."


translate spanish day4_busstop_a0871d04:


    "No era difícil, de hecho... tan sólo repetir las mismas notas una vez y otra."


translate spanish day4_busstop_621a4f25:


    th "¡Simplemente no pierdas el ritmo!"


translate spanish day4_busstop_20b61502:


    mi "¡Comienza a mi señal!"


translate spanish day4_busstop_35ec24e9:


    me "Vale..."


translate spanish day4_busstop_697d9fca:


    mi "¿Preparado? ¡Vamos!"


translate spanish day4_busstop_52370984:


    "Era una canción en japonés."


translate spanish day4_busstop_f1a32904:


    "Para ser sinceros, no comprendí ni una palabra, pero el canto de Miku era bastante bueno.{w} De hecho, ¡era espectacular!"


translate spanish day4_busstop_0f7f3a55:


    "Ponía su corazón en cada nota, en cada palabra."


translate spanish day4_busstop_b6da10b3:


    th "Claro, la música probablemente sea la mejor cosa a la cual ella pueda dedicar su vida a ello."


translate spanish day4_busstop_f7383791:


    "Parecía como si no fuera ella quien hubiera escogido la música, sino que la música fuera quien la hubiera escogido a ella..."


translate spanish day4_busstop_c487d162:


    "La última media hora se mostró una Miku de un matiz completamente diferente."


translate spanish day4_busstop_a20cefa7:


    "..."


translate spanish day4_busstop_128ef1ec:


    mi "¡Oh, gracias! ¿Te gustó? Finalmente lo logré contigo, conmigo sola no sale tan bien. A veces me pierdo o con las palabras o con los acordes. ¡Y contigo fue justo perfecto! Muchas gracias."


translate spanish day4_busstop_327d8e83:


    mi "Realmente tienes talento, ¡ya sabes!{w} Simplemente tocar así de esa forma sin habértelo preparado..."


translate spanish day4_busstop_5dbd8783:


    th "Nop, parece que cambié mi opinión sobre ella demasiado rápidamente."


translate spanish day4_busstop_5b2f2663:


    me "¡Gracias por la canción!{w} Me tengo que ir, ¡nos vemos!"


translate spanish day4_busstop_df33f13a:


    mi "Gracias por..."


translate spanish day4_busstop_f1561010:


    "El resto de sus palabras se quedaron atrás cuando salí por la puerta."


translate spanish day4_busstop_a9ebca9c:


    "Me recliné sobre la pared del edificio del club y suspiré."


translate spanish day4_busstop_ab0c1c58:


    "La canción de Miku todavía estaba sonando en mi cabeza."


translate spanish day4_forest_cfbff117:


    "Era obvio que Olga Dmitrievna y sus pioneros ya habían buscado por cada rincón y lugar del campamento."


translate spanish day4_forest_5cc420a6:


    "Deambularon por todas partes a lo largo y ancho de él..."


translate spanish day4_forest_305ab809:


    "Así que probablemente no valga la pena buscar a Shurik en la cantina o en la playa."


translate spanish day4_forest_bbfd297a:


    "¡O en el club de cibernética! Era su segundo hogar.{w} O, tal vez, incluso el primero."


translate spanish day4_forest_b2a1e220:


    "Por consiguiente podría valer la pena echar un vistazo por los alrededores del bosque."


translate spanish day4_forest_50fb2210:


    "No tengo intenciones de adentrarme muy lejos en el bosque. Si no seré yo quien tendrá que ser buscado mañana."


translate spanish day4_forest_919b405e:


    "Realmente no visité la campiña a menudo."


translate spanish day4_forest_a12c2f29:


    "Solamente me quedaba en una casa de campo cada verano durante mi infancia.{w} Pero esa estaba muy cerca de la ciudad."


translate spanish day4_forest_a605f85e:


    "Pero en este campamento, era posible hallar todo lo que no había visto por largo tiempo: una abundante vegetación, pájaros cantando y un aire fresco."


translate spanish day4_forest_a278fb75:


    "Encontré una pradera y me senté en un tocón."


translate spanish day4_forest_6853b13e:


    th "Qué pacífico es este lugar..."


translate spanish day4_forest_0617ce76:


    th "¿Pero dónde demonios está Shurik?"


translate spanish day4_forest_b4222e21:


    "En realidad, podría haber sido secuestrado en contra de su voluntad."


translate spanish day4_forest_ef624150:


    "Este campamento dista lejos de ser normal, por lo que la desaparición de un pionero debería no ser tan extraño."


translate spanish day4_forest_d487dd90:


    th "Una posibilidad intrigante... ¿Podría ser obra de cualesquiera fuerza la que me envió aquí, o tan sólo lo hizo algún otro ente de aquí?"


translate spanish day4_forest_25b671a8:


    "Ensimismado en mis propios pensamientos, no me percaté de cómo la hierba detrás de mí comenzó a moverse."


translate spanish day4_forest_4e2bb113:


    "Miré de cerca y vi una ardilla."


translate spanish day4_forest_fd1017fc:


    "Se me acercó cuidadosamente y se quedó quieta en mis manos."


translate spanish day4_forest_aa2cbac9:


    "Probablemente esté acostumbrada aquí a que la alimenten."


translate spanish day4_forest_871d8c7f:


    me "Lo siento, amiga, no tengo nada que darte..."


translate spanish day4_forest_94efe0a6:


    "Por supuesto, la ardilla no podía comprenderme y continuó sentada justo ahí, esperando algún premio."


translate spanish day4_forest_5fbbd1e5:


    "Lo sentía por ella porque no tenía ni una miga de pan en mis bolsillos."


translate spanish day4_forest_3b611457:


    "Me di cuenta de que incluso me avergonzaba mirarla a los ojos, así que decidí irme."


translate spanish day4_forest_a20cefa7:


    "..."


translate spanish day4_forest_97cf4b53:


    "Tras vagar un rato más, llegué a los lavabos."


translate spanish day4_forest_8563a8ed:


    th "Aparentemente no se encuentra en el bosque tampoco.{w} Al menos no en la zona de los alrededores."


translate spanish day4_forest_977ea43c:


    "E ir más lejos simplemente da miedo."


translate spanish day4_forest_150dfa24:


    "Caminé hacia el lavabo, me quité la camisa y traté de limpiarme, ya que estaba completamente sudado."


translate spanish day4_forest_cf3e2df3:


    "Sin embargo, no era tan fácil."


translate spanish day4_forest_0fd38ed9:


    th "No podría meterme ni en la jofaina del lavabo y tampoco había manguera o cucharón para cogerla..."


translate spanish day4_forest_9ff095ed:


    "De repente escuché pasos detrás de mí.{w} Me di la vuelta."


translate spanish day4_forest_fd05bbb8:


    "Electronik estaba caminando en mi dirección."


translate spanish day4_forest_051049d1:


    el "¿Buscando a Shurik?"


translate spanish day4_forest_16055c7f:


    me "Sí...{w} ¿Tú también?"


translate spanish day4_forest_d1214932:


    el "Yo igual..."


translate spanish day4_forest_959d6609:


    me "Escucha, tú le conoces mejor, ¿dónde puede haberse ido?"


translate spanish day4_forest_1cbcc91e:


    el "No tengo ni idea."


translate spanish day4_forest_2e34b08a:


    "Respondió tristemente."


translate spanish day4_forest_d074bc24:


    me "Bueno...{w} No entiendo por qué todo el mundo se ha preocupado tanto por todo esto. Por la noche, ¿no estaba contigo en la cabaña? Así que no puede haberse ido muy lejos...{w} ¿Quizá salió a dar un paseo?"


translate spanish day4_forest_5def083f:


    el "¡No conoces a Shurik!"


translate spanish day4_forest_668b3318:


    "Dijo Electronik con mucho ánimo."


translate spanish day4_forest_b25357aa:


    el "¡Él se dedica fanáticamente a su trabajo!{w} Su vida consiste en robótica y cibernética. ¡Gente como él son como una entre un millón! No, ¡en mil millones! ¡Su talento no tiene límites! ¡Le admiro y es un hombre de acero!"


translate spanish day4_forest_503848df:


    el "¡No, sencillamente victorioso!"


translate spanish day4_forest_6b3cf294:


    "En ese instante se parecía a Hitler, haciendo un discurso frente a una masa de miles."


translate spanish day4_forest_fe5e3be9:


    "Incluso sus gestos se igualaban."


translate spanish day4_forest_ccf562b1:


    me "Vale...{w} ¿Entonces qué?"


translate spanish day4_forest_b4f43170:


    el "¿«Entonces qué»? ¿Es que no lo comprendes?"


translate spanish day4_forest_5cb822ee:


    me "No..."


translate spanish day4_forest_37697e5c:


    el "Pasa todo el tiempo allí, ¿entiendes? ¡Todo su tiempo libre en nuestro club! ¡TODO!"


translate spanish day4_forest_2c080d1a:


    me "Así que, desaparecer de esa manera es algo inusual para él..."


translate spanish day4_forest_acffd441:


    el "¡Por supuesto!"


translate spanish day4_forest_2ce86487:


    "Parece que Electronik se tranquilizó."


translate spanish day4_forest_505d91ae:


    me "Vale..."


translate spanish day4_forest_2c5335a6:


    "Me miraba con atención."


translate spanish day4_forest_3aee18b1:


    el "¿Vas a asearte?"


translate spanish day4_forest_b37f1cb4:


    me "En verdad no...{w} Tan sólo refrescarme un poco. Hace calor."


translate spanish day4_forest_4fa820b9:


    el "Yo también."


translate spanish day4_forest_2771ee33:


    "Miró alrededor."


translate spanish day4_forest_459c94ac:


    el "Desearía que hubiera una palangana o una manguera. Algo con lo que agarrar más agua."


translate spanish day4_forest_1403f59c:


    "Claro, ya me di cuenta de ello."


translate spanish day4_forest_01eb5767:


    el "Pues vamos allá..."


translate spanish day4_forest_fe274fdb:


    "Caminó hasta los lavabos y apretó uno de los grifos."


translate spanish day4_forest_6341f283:


    "Para mi sorpresa, el chorro de agua salpicaba más que irse por la jofaina del lavabo, el agua casi salpicaba en un ángulo recto."


translate spanish day4_forest_5dcfd900:


    "De esa forma podías apañártelas para asearte."


translate spanish day4_forest_0d1796ca:


    "Mientras tanto, Electronik se quitó su camisa y se agachó, pareciera como si se hubiese quitado los pantalones cortos."


translate spanish day4_forest_746f00c8:


    "A decir verdad, no podría decirlo ya que el agua le ocultaba de tal forma hasta la cintura."


translate spanish day4_forest_f589c698:


    "Dirigió el chorro de agua hacia él mismo y comenzó a cantar alguna cosa relajadamente."


translate spanish day4_forest_aabf34aa:


    el "Vamos a limpiar el pajarito, dulce pajarito..."


translate spanish day4_forest_76b7903e:


    "Me quedé de piedra, no sabía qué hacer."


translate spanish day4_forest_a7e62300:


    "Parece que se dio cuenta de ello."


translate spanish day4_forest_b0d91aa4:


    el "Voy a asearme y luego te dejo usarlo."


translate spanish day4_forest_01fbaabb:


    "¡Cielos! ¡Tuve un increíble escalofrío!"


translate spanish day4_forest_4c762e8b:


    me "Bueno, sabes...{w} ¡Justo me acabo de acordar de que tengo algo que hacer! ¡Me voy!"


translate spanish day4_forest_c6ce9432:


    "A parte de todas las excentricidades de Electronik, este ha sido el momento más extravagante que he visto."


translate spanish day4_forest_b530b3f7:


    el "¡Ey! ¿Qué pasa contigo?{w} ¡Una ducha fría en un día caluroso como el de hoy es lo mejor!"


translate spanish day4_forest_9308b010:


    me "¡No, no, no!{w} ¡De ninguna manera!{w} Y de todas formas, me tengo que ir."


translate spanish day4_forest_b9ecea82:


    "Me puse la camisa rápidamente y me fui corriendo de vuelta al bosque."


translate spanish day4_forest_eedd04d7:


    th "Me pregunto qué se le pasa por la cabeza..."


translate spanish day4_house_of_mt_ed200de8:


    th "Pero sigue siendo una idea estúpida."


translate spanish day4_house_of_mt_3cdc1097:


    "Si Shurik estaba escondiéndose en cualquier lugar del campamento, ya se le habría encontrado (claro, asumiendo que quisiera ser encontrado)."


translate spanish day4_house_of_mt_90c7afe2:


    th "Por lo que dudo seriamente que pueda ayudarle."


translate spanish day4_house_of_mt_afd17b4a:


    "Y con esa idea en mente entré en la cabaña y me estiré en la cama."


translate spanish day4_house_of_mt_f20ad58c:


    th "Nada bueno puede ocurrir si Olga Dmitrievna me halla aquí."


translate spanish day4_house_of_mt_d712f4f6:


    th "¡Ya basta! ¿Soy una criatura cobarde y temerosa, o soy una con derechos y dignidad?"


translate spanish day4_house_of_mt_befe5d34:


    "Tampoco quería hacer nada de todas maneras."


translate spanish day4_house_of_mt_2300b407:


    "El día era tan caluroso como justo lo fue el día anterior, y la única cosa para hacer era quedarme en la cama y esperar hasta la hora de comer."


translate spanish day4_house_of_mt_43e095ca:


    "Me estaba quedando dormido cuando alguien llamó a la puerta."


translate spanish day4_house_of_mt_48555f41:


    me "Entra."


translate spanish day4_house_of_mt_482a0501:


    "Slavya estaba en la entrada de la puerta."


translate spanish day4_house_of_mt_df14250c:


    sl "¿Olga Dmitrievna no está aquí?"


translate spanish day4_house_of_mt_5a7a7577:


    me "No."


translate spanish day4_house_of_mt_63863ec8:


    sl "¿Y qué estás haciendo aquí?"


translate spanish day4_house_of_mt_1c3f46c0:


    "Preguntó con recelo."


translate spanish day4_house_of_mt_a57d59c9:


    "Me miré a mi mismo, tumbado en la cama y dije:"


translate spanish day4_house_of_mt_15d05c86:


    me "Tumbado..."


translate spanish day4_house_of_mt_f8b93764:


    sl "Eso puedo verlo.{w} Pero oí que Olga Dmitrievna te pidió que buscaras a Shurik."


translate spanish day4_house_of_mt_ff471f9a:


    me "Bueno, sí..."


translate spanish day4_house_of_mt_e26abf96:


    sl "¿Y?"


translate spanish day4_house_of_mt_6715e0fb:


    me "¿Qué sentido tiene?{w} Estoy seguro de que ella ya ha puesto todo el campamento patas arriba."


translate spanish day4_house_of_mt_e06cc00e:


    me "No ha transcurrido mucho tiempo.{w} ¿Por qué deberíamos estar alarmados?"


translate spanish day4_house_of_mt_04fc551f:


    sl "Sabes, a veces podrían ocurrir cosas..."


translate spanish day4_house_of_mt_edee7f76:


    "Slavya me miraba pensativamente."


translate spanish day4_house_of_mt_b68d7b34:


    sl "Levántate."


translate spanish day4_house_of_mt_34c3a4c4:


    me "¿Me tengo que levantar?"


translate spanish day4_house_of_mt_2551395c:


    "Estaba tan fatigado que incluso la simple idea de ir a alguna parte me daba pereza."


translate spanish day4_house_of_mt_2bb8d573:


    sl "¡Sí, lo harás!"


translate spanish day4_house_of_mt_90cc9542:


    "Slavya no es una persona a la cual le pudiera decir que no."


translate spanish day4_house_of_mt_6cdf61dd:


    "Me levanté a regañadientes y salí de la cabaña con ella."


translate spanish day4_house_of_mt_5aaa09bd:


    "Nos quedamos quietos en la puerta un rato, relajándonos con los rayos del veraniego sol.{w} A pesar de que más bien me quedaba frito..."


translate spanish day4_house_of_mt_fca62f46:


    me "¿A dónde vamos a ir?"


translate spanish day4_house_of_mt_276df626:


    sl "Tenemos que buscar en todas partes."


translate spanish day4_house_of_mt_57a26992:


    th "Buena idea... ¡Genial!"


translate spanish day4_house_of_mt_018386dd:


    "Nuestro primer lugar donde buscar en nuestro camino era la biblioteca."


translate spanish day4_house_of_mt_42ea0267:


    "Slavya fue hacia Zhenya que estaba sentada cerca de la mesa y le habló."


translate spanish day4_house_of_mt_188600a4:


    "Yo me quedé justo en la entrada. No quería hablar con esta bibliotecaria más que lo necesario."


translate spanish day4_house_of_mt_eccb8e77:


    "Minutos después Slavya volvió conmigo."


translate spanish day4_house_of_mt_48013d82:


    me "¿Y bien?"


translate spanish day4_house_of_mt_37eba565:


    sl "Nada..."


translate spanish day4_house_of_mt_4c3d69ef:


    "Ella negó con la cabeza."


translate spanish day4_house_of_mt_e93ab54f:


    th "Como era de esperar."


translate spanish day4_house_of_mt_a20cefa7:


    "..."


translate spanish day4_house_of_mt_79e8cb2c:


    "La cantina.{w} Todavía teníamos tiempo antes de que fuera la hora de comer por lo que no había ninguna habitual multitud de pioneros."


translate spanish day4_house_of_mt_e2de9d7b:


    "Estaba vacío por dentro y por fuera del edificio."


translate spanish day4_house_of_mt_84df38db:


    "Mientras Slavya estaba hablando con los cocineros, me senté y jugué con el salero."


translate spanish day4_house_of_mt_ae189afc:


    "Un poco de sal se escapó del recipiente."


translate spanish day4_house_of_mt_f8a8a439:


    "Ciertamente, estaría preocupado si fuera supersticioso."


translate spanish day4_house_of_mt_31901972:


    "Muy extraño, tampoco estaba aquí."


translate spanish day4_house_of_mt_387e3a7a:


    "Hubiera sido inesperado si ella lo hubiese hallado, por ejemplo, en la nevera..."


translate spanish day4_house_of_mt_a20cefa7_1:


    "..."


translate spanish day4_house_of_mt_d5c5547a:


    "El siguiente lugar donde buscar según lo planeado era la enfermería."


translate spanish day4_house_of_mt_60f69217:


    "Decidí no entrar y esperé fuera a Slavya."


translate spanish day4_house_of_mt_6b61c0c5:


    "Sin resultados."


translate spanish day4_house_of_mt_c08b785a:


    "Habían pioneros jugando a fútbol en la pista deportiva."


translate spanish day4_house_of_mt_b4ae2b53:


    "Hubiera sido difícil para Shurik perderse entre ellos."


translate spanish day4_house_of_mt_b2c08c50:


    "Finalmente llegamos al edificio de clubes."


translate spanish day4_house_of_mt_38b1dea9:


    me "¿Crees que podría estar ahí? Pensaba que era el primer lugar al cual la gente habría ido a buscarlo..."


translate spanish day4_house_of_mt_7827a8db:


    sl "Entremos."


translate spanish day4_house_of_mt_40d2a09a:


    "Estaba vacío por dentro."


translate spanish day4_house_of_mt_2836519c:


    "Slavya abrió la puerta y se fue dentro de la siguiente habitación."


translate spanish day4_house_of_mt_f73e0745:


    "La seguí."


translate spanish day4_house_of_mt_0ba05a1b:


    "Todo esto me parecía una tarea estúpida."


translate spanish day4_house_of_mt_a967e5b1:


    "Además era especialmente raro porque era idea de Slavya."


translate spanish day4_house_of_mt_9b88aabc:


    th "Quiero decir, entiendo la responsabilidad y todo eso...{w} pero... ¿es que no era obvio que él no estaba en el campamento?"


translate spanish day4_house_of_mt_13a0360f:


    th "Después de todo, no estaba jugando al econdite con nosotros, ¿verdad?"


translate spanish day4_house_of_mt_c1888233:


    sl "No hay nadie aquí tampoco..."


translate spanish day4_house_of_mt_6e164047:


    me "¿Qué te esperabas?{w} ¿Que estuviera sentado en un armario?"


translate spanish day4_house_of_mt_dfc5030f:


    sl "¡Vale, vale!"


translate spanish day4_house_of_mt_6e0f81b2:


    "Parece que la he ofendido."


translate spanish day4_house_of_mt_bc695e77:


    me "Lo siento, disculpa...{w} ¡Pero es verdad!"


translate spanish day4_house_of_mt_d2e028e4:


    sl "Lo comprendo...{w} Pero tenemos que descartar todas las posibilidades."


translate spanish day4_house_of_mt_1a2d3a52:


    me "Vale, escucha, ¿qué crees que ha pasado de verdad?"


translate spanish day4_house_of_mt_1829f925:


    sl "¿Sobre la desaparición de Shurik?"


translate spanish day4_house_of_mt_fc30fd2b:


    me "Sí."


translate spanish day4_house_of_mt_68504b39:


    sl "Quizá se fue al bosque y ha sido secuestrado por un duende."


translate spanish day4_house_of_mt_ec2c2a7c:


    "Se rio de ello."


translate spanish day4_house_of_mt_579bb939:


    me "Qué cuento más fabuloso..."


translate spanish day4_house_of_mt_adb6e4a6:


    th "Sin embargo, en este campamento nunca sabes."


translate spanish day4_house_of_mt_920eda59:


    sl "Sí, es verdad...{w} ¡Pero ahora no es momento de hacer bromas!"


translate spanish day4_house_of_mt_d7a9b9f5:


    me "¡Ánimo!{w} ¡Le encontraremos!"


translate spanish day4_house_of_mt_4f31da71:


    sl "Eso espero..."


translate spanish day4_house_of_mt_c17a722d:


    "Slavya sonrió."


translate spanish day4_house_of_mt_551b3c25:


    sl "Bueno, aun tengo algunas cosas que hacer."


translate spanish day4_house_of_mt_038a986f:


    me "Nos vemos más tarde."


translate spanish day4_house_of_mt_432f59be:


    "Se fue, pero permanecí por un rato allí mirando fijamente el armario del club de cibernética."


translate spanish day4_house_of_mt_a20cefa7_2:


    "..."


translate spanish day4_house_of_mt_c6af67a5:


    "Aun así estaba tan inspirado por las acciones de Slavya que decidí continuar buscando por algún lugar."


translate spanish day4_boathouse_9797429b:


    th "Tal vez Shurik se fue a coger piedras cerca del agua."


translate spanish day4_boathouse_140891ff:


    th "En el peor de los casos encontraré su cadáver..."


translate spanish day4_boathouse_639b643a:


    th "¡Pero estoy seguro de que no acabó así!"


translate spanish day4_boathouse_ea7057f1:


    "Estaba cruzando la plaza cuando alguien me llamó."


translate spanish day4_boathouse_418a6303:


    dv "¡Espera!"


translate spanish day4_boathouse_3900d745:


    "Alisa se acercó y sonrió.{w} Inmediatamente tuve la sensación de que había alguna trampa."


translate spanish day4_boathouse_2f227a4f:


    dv "¿Dónde estás yendo?"


translate spanish day4_boathouse_d6059f36:


    me "Estoy buscando a Shurik...{w} Olga Dmitrievna me lo pidió."


translate spanish day4_boathouse_ce4ee092:


    dv "¿Y qué tal va? ¿Emocionante?"


translate spanish day4_boathouse_9d114d2d:


    "Me miró atentamente a los ojos y los aparté incómodo."


translate spanish day4_boathouse_cba9cbf0:


    me "Realmente no...{w} Pero ya sabes, un pionero se ha perdido."


translate spanish day4_boathouse_36e1c3b7:


    dv "Claro, no estás muy entusiasmado por una minucia así, ¿verdad?"


translate spanish day4_boathouse_cb23542f:


    me "¿Qué quieres decir?"


translate spanish day4_boathouse_b0848313:


    dv "Sólo transcurrieron unas horas.{w} Tal vez se ha demorado mientras daba un paseo..."


translate spanish day4_boathouse_da12543c:


    "Ciertamente pensaba lo mismo, pero no iba a contarle eso."


translate spanish day4_boathouse_f7f0bc77:


    me "Sí, eso es verdad.{w} Pero quién sabe qué puede haber sucedido..."


translate spanish day4_boathouse_64f4e64e:


    dv "¡Déjame que te ayude con ello!"


translate spanish day4_boathouse_16a32560:


    me "Mmmm... ¿con qué?"


translate spanish day4_boathouse_a3e48491:


    "Sospeché."


translate spanish day4_boathouse_01b2df0c:


    dv "¡Con buscar a Shurik!"


translate spanish day4_boathouse_beae491d:


    me "Oh, pero eso ya lo puedo hacer yo..."


translate spanish day4_boathouse_55688a4e:


    dv "¡Anda ya, vamos!"


translate spanish day4_boathouse_f0fb482a:


    "Me sonrió pero había algo de malicia en su sonrisa."


translate spanish day4_boathouse_cd88bad5:


    "Naturalmente, su sonrisa era bastante bonita, pero me pareció en realidad que había oculto algo más."


translate spanish day4_boathouse_b88b8d82:


    me "Bueno, si insistes..."


translate spanish day4_boathouse_4e8dafb2:


    "Como tampoco podía comprender qué le pasaba por su cabeza, no tenía un motivo real como para rechazar su ayuda."


translate spanish day4_boathouse_b5f16424:


    dv "Pero antes de irnos tengo que coger una cosa de mi cabaña."


translate spanish day4_boathouse_12c4ee4b:


    me "Vale, te esperaré."


translate spanish day4_boathouse_62de1363:


    dv "¿Lo dices en serio eso de quedarte ahí esperando?{w} ¡Vamos!"


translate spanish day4_boathouse_505d91ae:


    me "Vale..."


translate spanish day4_boathouse_a2980b83:


    "Nos acercamos a la cabaña de Alisa."


translate spanish day4_boathouse_7aa63adb:


    "Sería sencillamente como el resto de cabañas, si no fuera por la Jolly Roger de su puerta."


translate spanish day4_boathouse_e4591d9e:


    dv "Por cierto, Ulyana es mi compañera de habitación."


translate spanish day4_boathouse_53a56040:


    me "Vale."


translate spanish day4_boathouse_36881cd4:


    "Entramos en la cabaña."


translate spanish day4_boathouse_983dd462:


    "El caos total de dentro me hizo recordar realmente a mi viejo piso."


translate spanish day4_boathouse_0d66d2a4:


    th "Aunque, por qué pensé en él como «viejo»..."


translate spanish day4_boathouse_225e52c0:


    "Por lo general, me hubiera imaginado la habitación de una muchacha de forma diferente: sábanas blancas como la nieve; paredes, suelo y techo brillantes. Impecables."


translate spanish day4_boathouse_cd30d970:


    th "Pero cuando consideras que dos de los más «ejemplares» pioneros estan viviendo aquí..."


translate spanish day4_boathouse_dab5031c:


    "Permanecimos silenciosamente durante un tiempo."


translate spanish day4_boathouse_f744c43e:


    me "¿Y qué era lo que necesitabas coger?"


translate spanish day4_boathouse_765cf23a:


    dv "¿Eh?"


translate spanish day4_boathouse_fe1ee079:


    "Parece que he interrumpido los pensamientos de Alisa."


translate spanish day4_boathouse_72b1039b:


    dv "Claro...{w} En realidad no está aquí...{w} Espera aquí, vuelvo en un minuto."


translate spanish day4_boathouse_0049a2fb:


    "Sonrió y se fue corriendo."


translate spanish day4_boathouse_ebc67191:


    th "Realmente hoy parece estar algo excéntrica."


translate spanish day4_boathouse_7fb057e3:


    "Comencé a examinar la habitación."


translate spanish day4_boathouse_5dfa3fd6:


    "No diría que hubiera habido una explosión allí, tan sólo era un digno desorden."


translate spanish day4_boathouse_49172fa3:


    th "Bueno, al menos viendo todo este desorden, creado en una única temporada de pioneros, puedo decir con seguridad que no soy la persona más desordenada en el mundo."


translate spanish day4_boathouse_b3f9ad2c:


    "¡La decoración de mi piso llevó años ser completada!"


translate spanish day4_boathouse_1f000f06:


    "No estaba pensando en nada en particular, tan sólo echaba un vistazo a la habitación."


translate spanish day4_boathouse_76afe60b:


    "Posters de artistas soviéticos, algunos libros en las estanterías, varios objetos de casa..."


translate spanish day4_boathouse_76d3f20d:


    "¡Y entonces me di cuenta de que algo iba mal!"


translate spanish day4_boathouse_8a607983:


    th "¡Alguna cosa ocurrirá pronto!{w} ¡Algo malo!"


translate spanish day4_boathouse_c1ef0499:


    th "¿Por qué me dejó Alisa completamente solo aquí?"


translate spanish day4_boathouse_3eb661ac:


    "Había algo que tenía que descubrir."


translate spanish day4_boathouse_9e3a46df:


    "Mis instintos me decían que era peligroso permanecer aquí."


translate spanish day4_boathouse_f00b42b5:


    "Fui a la puerta y traté de abrirla."


translate spanish day4_boathouse_531d7dd3:


    "¡Cerrada!"


translate spanish day4_boathouse_f67a6003:


    th "¡Qué sorpresa!{w} ¿Cómo se las arregló para encerrarme silenciosamente?"


translate spanish day4_boathouse_de1fdea1:


    th "No sé qué le pasó por su cabeza, ¡pero necesito salir de aquí!"


translate spanish day4_boathouse_dee27299:


    "Fui a la ventana, la abrí y trepé por ella."


translate spanish day4_boathouse_0ed51470:


    th "Ahora me puedo ir y ocuparme de mis asuntos o..."


translate spanish day4_boathouse_b69f8c48:


    "Escogí la segunda opción y esperé."


translate spanish day4_boathouse_e924afbe:


    th "¿Cuál es la razón por la cual me ha encerrado?"


translate spanish day4_boathouse_7b372665:


    "Quería saber qué estaba sucediendo, además realmente quería ver la cara de Alisa cuando se encontrara la habitación vacía."


translate spanish day4_boathouse_4c2291c3:


    "Me escondí en los arbustos cercanos a la cabaña y esperé."


translate spanish day4_boathouse_7eae97a5:


    "Tras un rato escuché pasos."


translate spanish day4_boathouse_009f6978:


    "Alisa y Olga Dmitrievna se acercaron a la cabaña."


translate spanish day4_boathouse_f9acd723:


    dv "¡Lo verás todo con tus propios ojos!"


translate spanish day4_boathouse_bc262665:


    "Ella abrió la puerta y entraron..."


translate spanish day4_boathouse_cc2a5873:


    "En pocos segundos después salió corriendo fuera."


translate spanish day4_boathouse_813cd9cc:


    dv "Sabes, yo..."


translate spanish day4_boathouse_8b502027:


    "El rostro de Alisa se veía como si justo hubiera ganado una competición de atrapar al ratón, pero en el último momento hallase que no era un deporte olímpico."


translate spanish day4_boathouse_4195d1ac:


    mt "¿Así que simplemente desapareció?"


translate spanish day4_boathouse_962ac463:


    "Olga Dmitrievna le preguntó escéptica."


translate spanish day4_boathouse_5460bb59:


    th "¡Hice lo correcto al cerrar la ventana detrás de mí al salir!"


translate spanish day4_boathouse_2178d069:


    dv "Oh no...{w} Ya sabes..."


translate spanish day4_boathouse_3f9c9a9a:


    mt "Lo entiendo todo, ¡Dvachevskaya!"


translate spanish day4_boathouse_fdf3425b:


    mt "Tú y tu compañera de habitación... ¡siempre lo mismo! ¡¿Es que no habéis tenido suficiente todavía?!"


translate spanish day4_boathouse_38429b64:


    dv "¡Pero es verdad...!"


translate spanish day4_boathouse_b29a5db5:


    mt "¿En serio? Una historia tras otra. Semyon en tu cabaña, Semyon acosándote. ¿Por qué sigues mintiendo? Eso es horrible."


translate spanish day4_boathouse_a48fe9fa:


    "Alisa parecía estar realmente enfadada y, para mi sorpresa, no trataba de discutir."


translate spanish day4_boathouse_81dede20:


    "Por un lado era divertido verla, pero por el otro... me sentía un poco mal por Dvachevskaya."


translate spanish day4_boathouse_0e804b76:


    "Aunque se lo merece..."


translate spanish day4_boathouse_4d1f6fb4:


    "Nuestra líder terminó de regañarla y se fue."


translate spanish day4_boathouse_2ef1d6a7:


    "Alisa se volvió loca.{w} Apretó los puños y se estremeció, su rostro estaba sonrojado, me pareció que iba a explotar de rabia."


translate spanish day4_boathouse_2eb4d7e1:


    "Me senté en los arbustos y me reí en silencio."


translate spanish day4_boathouse_7a53a4ae:


    "Sin embargo, me preguntaba qué se le pasó por la cabeza así que salí de mi escondite, sin preocuparme por si me atacaría."


translate spanish day4_boathouse_c7546dc1:


    me "Y bien, ¿qué era exactamente lo que querías enseñarle a la líder?{w} ¿A mi?"


translate spanish day4_boathouse_d6196049:


    "Alisa se giró y me miró incrédula."


translate spanish day4_boathouse_60e7b002:


    "Tras un instante, su sorpresa se transformó en rabia."


translate spanish day4_boathouse_b7d8bf72:


    dv "¡Tú! ¡Tú!"


translate spanish day4_boathouse_8f37a6c9:


    me "¿Qué pasa conmigo?"


translate spanish day4_boathouse_f834c3f8:


    "Se tranquilizó un poco."


translate spanish day4_boathouse_12133f6d:


    dv "Quería estar sin deudas contigo."


translate spanish day4_boathouse_f0d6d1c8:


    "Hizo una sonrisa de soberbia."


translate spanish day4_boathouse_21cbf526:


    me "¿Cómo?"


translate spanish day4_boathouse_e8988316:


    dv "Perdiste la apuesta.{w} Así que quería mostrarle a la líder del campamento cómo me estabas acosando."


translate spanish day4_boathouse_0fc6db81:


    th "Me pregunto qué podría encontrar de malo Olga Dmitrievna en esta situación, no se tratará de..."


translate spanish day4_boathouse_0686c1b4:


    dv "¡Quería venganza!"


translate spanish day4_boathouse_16b585f8:


    me "¿Por qué?"


translate spanish day4_boathouse_f3add060:


    th "En serio, ¿qué le hice a esta muchacha para que quisiera vengarse de mi?"


translate spanish day4_boathouse_fa0ca1ce:


    dv "¡Me venciste en las cartas!"


translate spanish day4_boathouse_b0dcc84f:


    th "¡Qué fantástico motivo!{w} No hay duda..."


translate spanish day4_boathouse_f2e007f0:


    me "Ya veo..."


translate spanish day4_boathouse_b6a65aa2:


    dv "¡Porque eres un perdedor!"


translate spanish day4_boathouse_32974cc7:


    me "¿Y eso por qué?"


translate spanish day4_boathouse_15bab5bb:


    dv "¡Debido a que tienes miedo de competir contra mi!"


translate spanish day4_boathouse_3c1b245e:


    me "¿Cuándo?"


translate spanish day4_boathouse_ef0158a3:


    dv "¡Cuando jugamos a cartas!"


translate spanish day4_boathouse_9d2033ff:


    "Que buen motivo..."


translate spanish day4_boathouse_c63ec5a4:


    me "Claro..."


translate spanish day4_boathouse_57d2d760:


    me "¿Quizá debería disculparme contigo?"


translate spanish day4_boathouse_81684e59:


    "Le pregunté sarcásticamente."


translate spanish day4_boathouse_0eebc5d9:


    dv "¡Anda ya!"


translate spanish day4_boathouse_c0d6d356:


    "Se fue dentro de la cabaña, pegando un portazo."


translate spanish day4_boathouse_5e595186:


    "No estaba enfadado con Alisa."


translate spanish day4_boathouse_663b2604:


    "A fin de cuentas, me esperaba de ella que hiciera algo así."


translate spanish day4_boathouse_d12d5f50:


    "De todas formas, no me pasó nada."


translate spanish day4_boathouse_2c46efd4:


    "Era muy afortunado... su confusión y vergüenza me alegraron el día."


translate spanish day4_boathouse_5030ce16:


    "Con una risita, abandoné la cabaña de Alisa, la cual estuvo a punto de convertirse en una trampa mortal."


translate spanish day4_main2_a20cefa7:


    "..."


translate spanish day4_main2_48d0d4e6:


    "Fui a desayunar con sentimientos encontrados... con una sensación de logro y con otra de pérdida de tiempo."


translate spanish day4_main2_4cb70312:


    "La cantina estaba llena de gente."


translate spanish day4_main2_58e949cc:


    "No pude pasar desapercibido... Olga Dmitrievna me llamó."


translate spanish day4_main2_a4d48910:


    mt "Semyon, ven con nosotros."


translate spanish day4_main2_08ad4dba:


    "La líder del campamento, Slavya y Electronik estaban sentados en una mesa para cuatro."


translate spanish day4_main2_2acc25a8:


    "Asentí y fui a coger mi comida."


translate spanish day4_main2_523ebbc0:


    "En esta ocasión tuve que esperar varios minutos en la cola."


translate spanish day4_main2_c6c2e073:


    "El menú de hoy no difería mucho respecto al de otros días.{w} Al menos los platos parecían iguales."


translate spanish day4_main2_05eb9d8b:


    "Cuando me senté en la mesa y desee bon appétit a todos, Olga Dmitrievna dijo:"


translate spanish day4_main2_b397d74c:


    mt "Entonces, ¿qué creéis?"


translate spanish day4_main2_c233b6a2:


    me "¿Sobre qué?"


translate spanish day4_main2_67341092:


    mt "Buscamos a Shurik por todas partes.{w} Es mediodía y no se le halla en ningún lugar."


translate spanish day4_main2_3d1cf378:


    "Iba cogiendo la idea, pero preferí no hacer hincapié en ello."


translate spanish day4_main2_5f3e793a:


    sl "Hemos buscado en todo el campamento."


translate spanish day4_main2_46ae85f2:


    el "Fui por todos los alrededores de los bosques colindantes."


translate spanish day4_main2_cefc6cd9:


    "Olga Dmitrievna me miró."


translate spanish day4_main2_f0777904:


    me "Y yo...{w} ayudé también."


translate spanish day4_main2_ad33fcd7:


    mt "¡Tenemos que llamar a la policía!"


translate spanish day4_main2_cc3b3354:


    me "¿Tal vez deberíamos esperar hasta anochecer?"


translate spanish day4_main2_26be9189:


    "Pregunté perezosamente."


translate spanish day4_main2_d859378c:


    me "Quizá se fue a casa..."


translate spanish day4_main2_29941732:


    mt "¡Eso no puede ser!{w} Shurik vive a miles de kilómetros de aquí."


translate spanish day4_main2_51a52fb5:


    me "¿Por tren?"


translate spanish day4_main2_932e3a79:


    mt "La estación más cercana..."


translate spanish day4_main2_b7d1c016:


    "Ella se calló."


translate spanish day4_main2_61df2540:


    mt "Está lejos..."


translate spanish day4_main2_35d07915:


    "Ahora se estaba volviendo interesante."


translate spanish day4_main2_3c635a17:


    th "Siempre que una conversación alcanza el punto en que se tratan vías por las que podría abandonar Sovyonok, todos los lugareños del campamento empiezan a cambiar de tema."


translate spanish day4_main2_91bfb245:


    me "¿Cómo de lejos es «lejos»?"


translate spanish day4_main2_a63beaef:


    mt "Realmente muy lejos."


translate spanish day4_main2_f3bfc63b:


    "La líder del campamento me miró, indicando con su expresión que hacer más preguntas no era aconsejable."


translate spanish day4_main2_12bcc6fb:


    sl "¡Pues deberíamos ir más adentro del bosque! ¡Quizá se ha perdido!"


translate spanish day4_main2_775f2318:


    el "Shurik siempre lleva una brújula."


translate spanish day4_main2_3c97ead2:


    "Intervino Electronik."


translate spanish day4_main2_df6e5065:


    "Me pregunto qué podría hallarse en su mágico chaleco (asumiendo que tiene uno)."


translate spanish day4_main2_feb702c4:


    "Si me fuera a perder en el bosque, una brújula no sería de mucha utilidad..."


translate spanish day4_main2_0ff502ce:


    mt "¡Policía!{w} ¡Deberíamos llamar a la policía al anochecer! "


translate spanish day4_main2_35192e6e:


    th "Por lo menos ahora mismo no."


translate spanish day4_main2_af1d0e2a:


    me "Vale, lo harás más tarde..."


translate spanish day4_main2_ec9674fa:


    "Se quedaron todos en silencio."


translate spanish day4_main2_a0920c3c:


    me "Deberíamos poder encontrarlo antes de anochecer, todavía tenemos tiempo."


translate spanish day4_main2_97d66c28:


    mt "¡Si en realidad se ha perdido no tenemos tiempo que perder!"


translate spanish day4_main2_24f8e21c:


    sl "No podemos estar seguros."


translate spanish day4_main2_dea475da:


    mt "Entonces, ¿dónde está? ¿Dónde?"


translate spanish day4_main2_515c8c9d:


    "Había cierta verdad en las palabras de la líder del campamento. Esconderse todo el día de esa forma era sospechoso."


translate spanish day4_main2_5f25be5b:


    "¿Por qué haría algo así?{w} Shurik me pareció ser un pionero bastante serio. Ulyana habría sido más apropiada para un comportamiento así."


translate spanish day4_main2_2ddaebf2:


    "Por lo que hay sobrados motivos para creer que él se ha ido."


translate spanish day4_main2_593ca1bb:


    me "Todo lo que se podría hacer ya se ha hecho. Solamente tenemos que esperar."


translate spanish day4_main2_56f14e0a:


    "Slavya, Electronik y Olga Dmitrievna me miraron tristemente pero no dijeron ni una palabra."


translate spanish day4_main2_a20cefa7_1:


    "..."


translate spanish day4_main2_6aa774a1:


    "Tras terminar mi comida, recogí la bandeja de platos sucios y abandoné la cantina."


translate spanish day4_main2_5abbd6f2:


    th "Aun queda todo el día por delante..."


translate spanish day4_main2_11e5ce88:


    th "¿Y ahora qué?"


translate spanish day4_main2_19b6f21b:


    "El campamento se vació a la hora de la siesta."


translate spanish day4_main2_99bbc067:


    "Solamente Genda me miraba fijamente a través de sus gafas."


translate spanish day4_main2_ab4e08c6:


    "Por supuesto, estaba mirando hacia alguna parte, pero tenía la sensación de que constantemente me miraba."


translate spanish day4_main2_5f2f4e3e:


    th "Apuesto a que él sabe dónde se esconde Shurik.{w} Sólo que no puede decir nada..."


translate spanish day4_main2_a3a27538:


    "La desaparición del líder del club de cibernética me hizo pensar."


translate spanish day4_main2_219aa2d9:


    th "¿Tal vez tiene algo que ver con mi caso?"


translate spanish day4_main2_2cf5c012:


    cs "¡Oh, pionero!"


translate spanish day4_main2_08272ac1:


    "La enfermera estaba frente a mi."


translate spanish day4_main2_4a52b166:


    "La miré con curiosidad."


translate spanish day4_main2_d7304160:


    cs "Ve y toma mi lugar en la enfermería.{w} Tengo un asunto urgente. Alguien herido."


translate spanish day4_main2_de5dd677:


    me "¿Yo?"


translate spanish day4_main2_dacf7660:


    cs "¡Sí, tú!{w} ¡Toma las llaves!"


translate spanish day4_main2_f30d0d14:


    "La enfermera me lanzó las llaves y se fue corriendo."


translate spanish day4_main2_469e7359:


    th "¿Por qué yo?{w} ¿Es que no hay nadie más en este campamento? ¿Qué se supone que debería hacer exactamente? ¿Y si ocurre algo?"


translate spanish day4_main2_7d9ad10a:


    th "Arghh, qué hago ahora, perdí mi oportunidad de no aceptar..."


translate spanish day4_main2_ab1de823:


    "Permanecí frente a la puerta con incertidumbre."


translate spanish day4_main2_c422bd2d:


    th "Por una parte no hay nada de qué preocuparse... me pasaré aquí media hora o así, y ella estará de vuelta."


translate spanish day4_main2_6e8191b6:


    th "Pero, ¿y si alguien viene en realidad por ayuda?{w} ¿Con una pierna rota? ¿O con una herida en la cabeza?"


translate spanish day4_main2_78c21f2c:


    "Comencé a preocuparme demasiado."


translate spanish day4_main2_72b4eb4d:


    "Espero que no haya heridas más importantes que moratones y rasguños en este campamento."


translate spanish day4_main2_65e07906:


    "Pero al mismo tiempo no podía quitarme la idea de la cabeza de que en una situación real sería absolutamente inútil."


translate spanish day4_main2_1e5b77ba:


    th "¡No sé ni cómo realizar una reanimación cardiopulmonar!"


translate spanish day4_main2_d02d8b17:


    "Una revista en la mesa de la enfermera me llamó la atención."


translate spanish day4_main2_b5804212:


    th "Una buena manera de relajarse.{w} Supongo..."


translate spanish day4_main2_ec4a19eb:


    "Se titula «Moda soviética»."


translate spanish day4_main2_e9159fbe:


    "Carece de fecha de publicación."


translate spanish day4_main2_726e419d:


    "Sin embargo, no era ninguna sorpresa. Habían muchas más cosas extrañas sucediendo aquí. No sabía mucho sobre revistas soviéticas. Tal vez es que en realidad no tienen fecha de publicación en ellas."


translate spanish day4_main2_21eab677:


    "Modelos vestidas con ropa de moda antigua, estaban frente a mi en aquellas páginas de fotografías."


translate spanish day4_main2_bb76f93b:


    th "Hoy en día nadie vestiría semejante ropa."


translate spanish day4_main2_a026df63:


    "Sonreí burlonamente."


translate spanish day4_main2_f5d07ae7:


    th "Me pregunto si Slavya, por ejemplo, consideraría esto moderno."


translate spanish day4_main2_2453e3b6:


    th "Puedo imaginarme que ocurriría si ella apareciera en mi época vistiendo algo así."


translate spanish day4_main2_864530e1:


    th "Me la imagino caminando agarrados de la mano. Llevo puesto mi abrigo con la capucha y ella viste un espléndido vestido bordado de encaje y cosas así."


translate spanish day4_main2_e7482e77:


    "Me parece que ya me imagino a Slavya en mi mundo...{w} conmigo...{w} y no sólo Slavya."


translate spanish day4_main2_19330451:


    th "Este vestido le sienta mejor a Ulyana, este bonito sarafán se vería mejor en Alisa, esta falda y rebeca se verían lindas en Lena."


translate spanish day4_main2_acf18888:


    "Si tan sólo pudieran ser reales..."


translate spanish day4_main2_45c2a3ea:


    "No, las vi, las escuché, podría incluso tocarlas."


translate spanish day4_main2_b793115e:


    th "Pero aun así ellas están aquí y yo...{w} Yo, sencillamente no pertenezco a este lugar."


translate spanish day4_main2_c7ecb5e1:


    th "Es ajeno a mí.{w} Solamente espero a una oportunidad para escapar de aquí."


translate spanish day4_main2_34ecfe30:


    th "Esperando porque ya nada más depende de mí..."


translate spanish day4_main2_a734d9d3:


    "Suspiré, apoyé mi cabeza en la mesa y me quedé dormido."


translate spanish day4_main2_a20cefa7_2:


    "..."


translate spanish day4_main2_ad69b965:


    "Fui despertado por el ruido de la puerta al abrirse."


translate spanish day4_main2_26171c75:


    "Lena estaba en la puerta."


translate spanish day4_main2_aa82a2c6:


    th "Ayer le prometí ayudarle en la enfermería..."


translate spanish day4_main2_6bb5c0bf:


    "Mis mejillas se volvieron escarlatas brevemente y me giré aparentando estar ocupado."


translate spanish day4_main2_fbb88586:


    un "¿La enfermera no está aquí?{w} Pues ya vendré más tarde..."


translate spanish day4_main2_37395644:


    me "¡La estoy sustituyendo!"


translate spanish day4_main2_d87b9ec9:


    th "Ya que soy responsable de las vidas de los pioneros, debería hacerme cargo de ellos con responsabilidad."


translate spanish day4_main2_7b38c5ca:


    "Aunque, de hecho, tan sólo estaba preocupado de que algo malo ocurriera por mi culpa."


translate spanish day4_main2_06a1509d:


    me "¿Hay alguna incidencia?"


translate spanish day4_main2_4c00f79b:


    "Traté de darle a Lena la sonrisa más profesional que pudiera darle con el fin de no confundirla."


translate spanish day4_main2_69f94ecc:


    un "Nada en especial...{w} Solamente un pequeño dolor de cabeza."


translate spanish day4_main2_2357f708:


    me "¡Ocupémonos de ello!"


translate spanish day4_main2_27d0cf32:


    th "Algún analgésico tal vez..."


translate spanish day4_main2_f9569158:


    "Está claro que no sabía dónde hallar las medicinas requeridas así que me llevó un rato encontrarlas."


translate spanish day4_main2_649ec83c:


    me "¡Finalmente!"


translate spanish day4_main2_64f21126:


    "Le ofrecí una pastilla de metamizol a Lena."


translate spanish day4_main2_082d3571:


    un "Gracias..."


translate spanish day4_main2_d1046a49:


    "Ella sonrió."


translate spanish day4_main2_38a347b1:


    "Eso era completamente inesperado y perdí el sentido de la realidad, fijándome en ella."


translate spanish day4_main2_3875ba7b:


    un "¿Qué?"


translate spanish day4_main2_cb318c1b:


    "Lena al instante se encontró incómoda."


translate spanish day4_main2_bec7dcec:


    me "Escucha, me he estado preguntando... ¿Te gusta esto?"


translate spanish day4_main2_6cbc07c6:


    "No sé qué se me pasó por la cabeza, pero cogí la revista de la mesa y le enseñé la fotografía de la falda y la rebeca, las cuales le sentarían bien a Lena en mi opinión."


translate spanish day4_main2_f9690e49:


    "Quizá me volví loco de atar, pensando en que todas las muchachas estuvieran en mi mundo."


translate spanish day4_main2_50cb5ce9:


    "O quizá quería distraerme en vez de tan sólo esperar a que la enfermera volviera."


translate spanish day4_main2_1523dd90:


    "Lena miró la fotografía."


translate spanish day4_main2_1aa04f80:


    un "Sí, supongo..."


translate spanish day4_main2_bc64bbfd:


    me "¿Están de moda cosas así?"


translate spanish day4_main2_3c68ea6c:


    un "Supongo..."


translate spanish day4_main2_340ec68e:


    "Empezó a estar confusa y se ruborizó."


translate spanish day4_main2_9812399d:


    un "¿Por qué me lo preguntas?"


translate spanish day4_main2_59e7c056:


    th "Realmente, porque..."


translate spanish day4_main2_082d3571_1:


    un "Gracias..."


translate spanish day4_main2_dcd2e3df:


    me "Está bien, ¡solamente quiero ser honesto por completo!"


translate spanish day4_main2_47092547:


    un "..."


translate spanish day4_main2_c4d1a45e:


    "Estuvimos en silencio por un rato."


translate spanish day4_main2_a417690d:


    me "¿Cómo está tu dolor de cabeza?"


translate spanish day4_main2_c1c02f2e:


    un "Mucho mejor, gracias."


translate spanish day4_main2_d1046a49_1:


    "Sonrió."


translate spanish day4_main2_a17b8e3e:


    un "Me voy."


translate spanish day4_main2_3fc88750:


    me "¡Que tengas suerte!"


translate spanish day4_main2_7e83005b:


    "Lena se fue y continué mirando la revista."


translate spanish day4_main2_a20cefa7_3:


    "..."


translate spanish day4_main2_500f0017:


    "Tras un rato alguien llamó a la puerta otra vez."


translate spanish day4_main2_22afe73c:


    th "Parece que la enfermería es el lugar más popular al que ir."


translate spanish day4_main2_b588274b:


    "De repente quise interpretar el papel de enfermero...{w} justo la versión masculina...{w} y dije:"


translate spanish day4_main2_6b33d385:


    me "¡Entre!"


translate spanish day4_main2_573b3dff:


    "La puerta se abrió y Ulyana entró en la habitación."


translate spanish day4_main2_5536cc92:


    me "¡Guau! ¿Me perdí el momento en que quisiste empezar a llamar a las puertas?"


translate spanish day4_main2_9ef7bfe7:


    us "¿Hay algún problema en que llame a la puerta?"


translate spanish day4_main2_d6ea162b:


    "Frunció el ceño."


translate spanish day4_main2_396150e8:


    me "¿Qué te duele?"


translate spanish day4_main2_cd1dfac0:


    us "¿Por qué demonios te lo iba a contar a ti?{w} ¿Dónde está la enfermera?"


translate spanish day4_main2_7611a63b:


    me "Justo delante de ti."


translate spanish day4_main2_d2429dec:


    "Crucé de forma imposible mis piernas y la miré inquisitivamente."


translate spanish day4_main2_9881e8d1:


    us "Mejor me voy.{w} Es mejor estar muerta que ser tratada por ti."


translate spanish day4_main2_7f6b716b:


    "Sonrió con picardía."


translate spanish day4_main2_5ae0e9e5:


    me "¡Ni me dejaste intentarlo!"


translate spanish day4_main2_2ccca5b3:


    "Ulyana se lo pensó por un instante."


translate spanish day4_main2_2348abed:


    us "Aunque me podrías dar algunas pastillas."


translate spanish day4_main2_fdf90b79:


    me "¿Qué te molesta?"


translate spanish day4_main2_aa62d7ce:


    "Le llevó un rato responderme."


translate spanish day4_main2_f32986c4:


    us "Dolor de estómago..."


translate spanish day4_main2_150675b6:


    me "¿Seguro que no será que tienes la cabeza vacía?"


translate spanish day4_main2_3e1d421a:


    "Balbuceé en voz baja."


translate spanish day4_main2_76d30161:


    us "¿Qué dijiste?"


translate spanish day4_main2_f6290a5b:


    me "Nada. Dame un segundo."


translate spanish day4_main2_f0d4eaf6:


    "Encontré las pastillas en el primer cajón que abrí."


translate spanish day4_main2_3ae2d123:


    us "¡Gracias, doctor!"


translate spanish day4_main2_d2b56c39:


    "Sonrió alegremente."


translate spanish day4_main2_45c9bd9f:


    "Viendo a Ulyana no me podía imaginar como semejante cría optimista e inquieta podría tener algún problema de salud."


translate spanish day4_main2_db45ba49:


    "Me hizo una mirada siniestra."


translate spanish day4_main2_f3f3ce8a:


    us "Pues no, ¡ahí te quedas!"


translate spanish day4_main2_4323f27c:


    "Ulyana se fue corriendo de la enfermería y cerró la puerta de un portazo."


translate spanish day4_main2_bd0d0f91:


    "Tal vez no debería haber dicho eso."


translate spanish day4_main2_4196aef5:


    me "Teniendo en cuenta la cocina lugareña, eso no parece tan sorprendente."


translate spanish day4_main2_f2c43065:


    us "Quizá..."


translate spanish day4_main2_f0d6d1c8:


    "Sonrió con arrogancia."


translate spanish day4_main2_5c8b5c7d:


    us "¡Veo que estás aun vivito y coleando!"


translate spanish day4_main2_6e98e9f8:


    me "Bueno, no me quejo..."


translate spanish day4_main2_14f237e5:


    us "¡Adiós!"


translate spanish day4_main2_4323f27c_1:


    "Ulyana se fue corriendo de la enfermería, dando un portazo."


translate spanish day4_main2_a20cefa7_4:


    "..."


translate spanish day4_main2_dbbcc16f:


    "Una vez más volví a estudiar aquellas tendencias de moda soviética."


translate spanish day4_main2_a3a786a7:


    "El tiempo transcurrió y la enfermera aun seguía ausentándose."


translate spanish day4_main2_5db604a8:


    "No estaba buscando a Shurik, tampoco buscando pistas respecto a mi situación...{w} Solamente estaba sentado pasando páginas de una revista."


translate spanish day4_main2_900a2743:


    "Justo por un instante me sentí bastante satisfecho por esta situación..."


translate spanish day4_main2_f82c0a74:


    th "Prácticamente todo lo ocurrido aquí podría ser visto como un simple viaje a un campamento de verano, y si pronto nada cambia, entonces sí que puedo empezar a preocuparme."


translate spanish day4_main2_cbdcd68a:


    "Otra vez alguien llamó a la puerta."


translate spanish day4_main2_7c25eaac:


    th "¿Es que me perdí acaso un brote de una epidemia?"


translate spanish day4_main2_482a0501:


    "Slavya estaba en las escaleras de la puerta."


translate spanish day4_main2_23d59ff0:


    sl "¡Oh, hola! ¿No está aquí la enfermera?"


translate spanish day4_main2_7f88d310:


    me "Hola. No, no está aquí. Hoy me ocupo yo."


translate spanish day4_main2_8803d0ca:


    sl "Genial.{w} Quisiera..."


translate spanish day4_main2_6e6e30b3:


    "Ella dudó."


translate spanish day4_main2_ad02f53a:


    me "¿Qué?"


translate spanish day4_main2_23a777ed:


    sl "Por cierto, Semyon..."


translate spanish day4_main2_626d4643:


    "Dijo Slavya, y me miró con atención."


translate spanish day4_main2_ad02f53a_1:


    me "¿Qué?"


translate spanish day4_main2_809b714a:


    sl "Parece que he perdido mis llaves. ¿Las has visto en alguna parte?"


translate spanish day4_main2_c2f310ec:


    th "¡Claro, las tengo!"


translate spanish day4_main2_fe5cc028:


    me "Sí, aquí están, las encontré cerca de la cantina ayer. Quise devolvértelas pero lo olvidé."


translate spanish day4_main2_7506f80e:


    "Era realmente pésimo mintiendo... mis mejillas enrojecieron, mis ojos miraban a otra parte y mis manos las movía nerviosas."


translate spanish day4_main2_71a21743:


    me "Aquí tienes."


translate spanish day4_main2_01753d9d:


    "Me estaba preparando para alguna excusa apropiada, pero Slavya tan sólo agarró las llaves y dijo:"


translate spanish day4_main2_edc3cf97:


    sl "Gracias."


translate spanish day4_main2_1a6d0231:


    me "Bueno, ¿y a qué has venido?"


translate spanish day4_main2_3e086ec1:


    "Necesitaba realmente cambiar de tema."


translate spanish day4_main2_12a9dd7f:


    me "¿Hay algo que te duela?"


translate spanish day4_main2_87efefbd:


    sl "No, no es nada..."


translate spanish day4_main2_06e847e7:


    th "Qué raro.{w} Nunca hubiera pensado que alguien tan extrovertida como Slavya se callaría algo en silencio."


translate spanish day4_main2_0ca10a35:


    me "Si hay algo que te moleste, ¡cuéntamelo!{w} Por eso estoy aquí, para ocuparme de la gente."


translate spanish day4_main2_ed4f432e:


    "Sonreí ampliamente."


translate spanish day4_main2_537386e4:


    sl "No...{w} Quiero decir sí, pero no."


translate spanish day4_main2_981e60cc:


    "Escuchar eso hacia cualquier cosa posible, incluso dividir entre cero."


translate spanish day4_main2_d633a8ca:


    me "Bueno, ¿cómo puedo ayudarte?"


translate spanish day4_main2_e2229da3:


    sl "¿Tú?{w} Supongo... que no puedes."


translate spanish day4_main2_d6c3c729:


    "Ella sonrió y estaba a punto de irse, pero de repente se paró."


translate spanish day4_main2_0a917d1d:


    sl "Sin embargo...{w} ¿Podrías salir fuera un minuto?"


translate spanish day4_main2_9ae266c2:


    th "¿Por qué no?"


translate spanish day4_main2_65bf82fc:


    me "Vale..."


translate spanish day4_main2_cf1fe9a1:


    "Salí fuera de la enfermería y me apoyé en la pared."


translate spanish day4_main2_47737877:


    th "Me pregunto qué debe estar haciendo, ¿alguna cosa que tampoco puedo ver?"


translate spanish day4_main2_8ee2b602:


    "Después de un minuto la puerta se abrió y Slavya salió."


translate spanish day4_main2_00d76af5:


    "Llevaba un pequeño paquete."


translate spanish day4_main2_fda5f154:


    me "¿Qué es eso?"


translate spanish day4_main2_1e8b4779:


    sl "¡Nada!"


translate spanish day4_main2_ac04f5d1:


    "Se ruborizó."


translate spanish day4_main2_b4db0146:


    sl "¡Gracias!"


translate spanish day4_main2_c46d7c1e:


    me "De nada..."


translate spanish day4_main2_ec475aa3:


    "Slavya se fue corriendo."


translate spanish day4_main2_0f420061:


    th "Pareciera que no debí preguntarle..."


translate spanish day4_main2_3fc88750_1:


    me "¡Que vaya bien!"


translate spanish day4_main2_3da1183b:


    "Le grité cuando se fue."


translate spanish day4_main2_309686e8:


    sl "¡Gracias!"


translate spanish day4_main2_a3a10705:


    "Me dio una de sus dulces sonrisas antes de irse."


translate spanish day4_main2_a20cefa7_5:


    "..."


translate spanish day4_main2_43b742c4:


    "Miré la hora."


translate spanish day4_main2_12b9bc98:


    "Comenzaba a ser tarde, leí la revista por completo desde la portada a la última cubierta, pero la enfermera aun seguía sin estar aquí."


translate spanish day4_main2_e60481fd:


    "De repente la puerta se abrió de golpe y Alisa entró rápidamente dentro."


translate spanish day4_main2_8fb93608:


    "Me miró de forma penetrante."


translate spanish day4_main2_24b939bd:


    dv "¿Qué haces tú aquí?"


translate spanish day4_main2_0085f606:


    me "Sentado..."


translate spanish day4_main2_9fb02d3f:


    "Admití sinceramente."


translate spanish day4_main2_2b2b99a8:


    dv "¡Pues vale!{w} Es mejor incluso sin la enfermera."


translate spanish day4_main2_36cead21:


    "Murmuró."


translate spanish day4_main2_46efe3f1:


    me "¿Estás enferma?"


translate spanish day4_main2_81684e59:


    "Pregunté mordazmente."


translate spanish day4_main2_d1591f00:


    "Alisa no me respondió y se me acercó."


translate spanish day4_main2_7693d10f:


    dv "Apártate."


translate spanish day4_main2_7332342b:


    me "¿Por qué?"


translate spanish day4_main2_b86ba02e:


    dv "Pues para abrir el cajón, ¡¿no es obvio?!"


translate spanish day4_main2_29a836d9:


    me "¿Para qué?"


translate spanish day4_main2_eed3c5aa:


    "Empezó a enfadarse."


translate spanish day4_main2_b2a1824d:


    dv "No es asunto tuyo."


translate spanish day4_main2_d0eeeaf6:


    me "Bueno, estoy a cargo aquí..."


translate spanish day4_main2_60f3d758:


    "Se puso a pensar un instante."


translate spanish day4_main2_f54686ff:


    dv "¡Pues dame algo de carbón activado!"


translate spanish day4_main2_f841182c:


    me "¿Dolor de estómago?"


translate spanish day4_main2_3a950d86:


    dv "Claro."


translate spanish day4_main2_1fcb3138:


    "Sonrió maliciosamente."


translate spanish day4_main2_c5e0f7d5:


    "Tuve la sensación de que alguna cosa iba mal..."


translate spanish day4_main2_1cfd6748:


    th "Pero nunca sabes...{w} ¿Y si realmente tenía dolor?"


translate spanish day4_main2_01822020:


    me "Vale."


translate spanish day4_main2_e386270a:


    "Eché mano en el cajón y saqué un paquete de carbón activado."


translate spanish day4_main2_8526df6e:


    dv "¡Gracias!"


translate spanish day4_main2_b7cee3ff:


    "Me lo arrebató de mis manos y se fue corriendo."


translate spanish day4_main2_952420fb:


    me "En serio, ¿dolor de estómago?"


translate spanish day4_main2_e6d2dba1:


    dv "¡De verdad!"


translate spanish day4_main2_494cb28f:


    me "No lo parece..."


translate spanish day4_main2_30d58c19:


    dv "¿Qué pasa, que tengo que vomitar todo el desayuno por todas partes para que me creas?"


translate spanish day4_main2_5052e4e8:


    me "No sé que cosas terribles se pueden hacer con carbón activado, ¡pero estoy seguro de que pensarás en algo!"


translate spanish day4_main2_6e6e30b3_1:


    "Ella dudó."


translate spanish day4_main2_2e830d48:


    dv "¿Y qué si tienes razón?"


translate spanish day4_main2_ccdea9ff:


    me "¡Ahí va!{w} ¡Lo sabía!{w} ¡Pues entonces seguro que no te voy a dar nada!"


translate spanish day4_main2_422e86f8:


    "Alisa intentó alcanzar los cajones, pero le impedí que alcanzara el pastillero con mi cuerpo como Alexander Matrosov."


translate spanish day4_main2_da695243:


    "A pesar de toda su insolencia seguía siendo sólo una muchacha, y no tuvo oportunidad alguna de apartarme."


translate spanish day4_main2_672c3026:


    dv "¡Da igual! Lo buscaré en otra parte."


translate spanish day4_main2_4a53f5ef:


    "Se dio la vuelta y se dirigió a la salida."


translate spanish day4_main2_579e0280:


    "Al llegar a la puerta Alisa se detuvo y miró atrás."


translate spanish day4_main2_40117237:


    dv "Por cierto, la líder del campamento y la enfermera estan trayendo a un pionero con una pierna rota.{w} No me importa mucho, pero parece que les cuesta."


translate spanish day4_main2_9f4cd3fa:


    me "¡Ah, claro!"


translate spanish day4_main2_9aa2cf7a:


    dv "Ve a mirarlo por ti mismo..."


translate spanish day4_main2_0f163ab9:


    "Alisa parecía estar mintiendo, pero por otra parte, siempre y cuando pueda tenerla a vista..."


translate spanish day4_main2_4ecdd924:


    "Me acerqué a la puerta y miré fuera.{w} No había nadie allí."


translate spanish day4_main2_c0277722:


    me "Ehmm, ¿dónde exactamente?"


translate spanish day4_main2_83ca7233:


    dv "Aun no han llegado hasta aquí pues."


translate spanish day4_main2_4a9b3777:


    "Hizo un gesto sin sentido."


translate spanish day4_main2_df8028e5:


    dv "¡Diviértete!"


translate spanish day4_main2_eeae4ddf:


    "Retorné a la mesa y sólo entonces me di cuenta de que uno de los cajones estaba abierto."


translate spanish day4_main2_d82dccfe:


    th "¡¿Cómo se lo llevó tan rápidamente?!"


translate spanish day4_main2_26b03227:


    th "Me di la vuelta sólo un segundo..."


translate spanish day4_main2_5992178b:


    th "Aun así, ¿qué travesura puede hacerse con carbón activado?"


translate spanish day4_main2_a20cefa7_6:


    "..."


translate spanish day4_main2_22f56cd3:


    "Faltaban 15 minutos para la cena, y la enfermera no había vuelto."


translate spanish day4_main2_26c2ca6d:


    th "Supongo que dejar sin nadie este lugar no es apropiado."


translate spanish day4_main2_5cce2231:


    th "Me pidió que me ocupara después de todo..."


translate spanish day4_main2_259b8764:


    th "Y quién sabe qué pudiera ocurrir si me voy..."


translate spanish day4_main2_96201ae8:


    th "Aun así, ¿quién querría robar algo de aquí?"


translate spanish day4_main2_084da37b:


    th "Salvo Alisa robando carbón activado..."


translate spanish day4_main2_2f835300:


    "Abrí de nuevo la revista y comencé a pasar páginas por enésima vez."


translate spanish day4_main2_c423f302:


    "Las modas de los ochenta ya no eran tan divertidas."


translate spanish day4_main2_9cba9e2a:


    "Bostecé en repetidas ocasiones."


translate spanish day4_main2_b3a0bc3f:


    "Ya eran las seis en punto, por lo que la cena ya ha comenzado."


translate spanish day4_main2_154cf8b8:


    "Por supuesto, tenía que ocuparme hasta el final de esta responsabilidad, pero mi estómago parecía tener su propia opinión al respecto."


translate spanish day4_main2_fa7b39d4:


    "Me levanté e ininterrumpidamente me dirigí a la salida."


translate spanish day4_main2_172df4fa:


    "Un ruido de crujido se escuchó tras la puerta."


translate spanish day4_main2_0360cdef:


    th "¿Podría ser alguien que se haya envenenado, roto un brazo o algo incluso peor?"


translate spanish day4_main2_dfc8fcaa:


    "Suspiré profundamente y giré el pomo de la puerta."


translate spanish day4_main2_4fcedea9:


    "Quienquiera que fuese ya no estaba ahí."


translate spanish day4_main2_c5913e31:


    "Resulta que solamente fue mi imaginación..."


translate spanish day4_main2_63fae2b6:


    "Volví dentro e instantáneamente me percaté de que algo había cambiado en la enfermería."


translate spanish day4_main2_fc8152bc:


    "Para ser más exactos, una manzana había aparecido en la mesa."


translate spanish day4_main2_166e21e4:


    th "¿Pero de dónde?"


translate spanish day4_main2_263f36e7:


    "Las frutas no pueden simplemente materializarse de la nada por sí mismas, ¿acaso pueden?"


translate spanish day4_main2_7f119004:


    "Aunque en este campamento cualquier cosa es posible..."


translate spanish day4_main2_8c3d5b7c:


    "Existe la posibilidad de que alguien sólo la trajera aquí."


translate spanish day4_main2_a3f9bd63:


    "Quizá Lena quiso agradecérmelo."


translate spanish day4_main2_c9e3d9e8:


    th "Teniendo en cuenta que ella es muy tímida y..."


translate spanish day4_main2_60cc7919:


    th "¡Pero espera!{w} ¿Cómo podría haberse deslizado sin cruzarse conmigo?"


translate spanish day4_main2_91c185e1:


    th "¡Lena no treparía por la ventana, a pesar de todo!"


translate spanish day4_main2_598572e0:


    th "Así que debe haber algo más..."


translate spanish day4_main2_a705caba:


    "Repentinamente me sobrecogió el miedo."


translate spanish day4_main2_c2d327f7:


    th "¿Y si la manzana está envenenada?"


translate spanish day4_main2_7cd7e93e:


    "A pesar de eso, ¿por qué alguien iba a complicarse, cuando estoy totalmente a su merced y puedo ser muerto de forma más fácil?"


translate spanish day4_main2_dea7837b:


    th "¡Oh! ¿Podría ser la manzana que Eva mordió en el Jardín del Edén?"


translate spanish day4_main2_74f2512f:


    th "En dicho caso, ¡debería mejor no comérmela!"


translate spanish day4_main2_efaf11ed:


    "A la vez, mi estómago me recordaba que estaba ahí con un rugido traidor."


translate spanish day4_main2_0aeb7b69:


    th "Sin embargo, me llevaría rato cerrar la enfermería, llegar hasta la cantina, conseguir la comida..."


translate spanish day4_main2_1f01a97c:


    "Mi estómago se iba a devorar a sí mismo."


translate spanish day4_main2_7e26087d:


    th "No hay nada de malo, supongo..."


translate spanish day4_main2_49800ad1:


    "Una vez más, traté de recordar si esta manzana ya estaba antes aquí."


translate spanish day4_main2_4d50734a:


    th "A fin de cuentas, ¡debe haber aparecido de alguna parte!"


translate spanish day4_main2_9c6489f9:


    th "¿Quizás llegó a través de la ventana?"


translate spanish day4_main2_20f7d3af:


    "La persiana estaba un poco abierta."


translate spanish day4_main2_74cca6f2:


    th "Entonces, alguien debe haberla alcanzado y debió de ponerla aquí."


translate spanish day4_main2_ec09e77a:


    th "¡Nada importante! Aunque..."


translate spanish day4_main2_02c83bbb:


    "Al fin el hambre prevaleció, y comencé a morder la manzana verde y madura con mucho gusto."


translate spanish day4_main2_cab5f253:


    "Su sabor parecía normal."


translate spanish day4_main2_cd98f167:


    th "Aunque no creo que, incluso el veneno más fuerte, pueda tener sabor."


translate spanish day4_main2_b6572b50:


    th "Pero ya es demasiado tarde para pensar sobre ello..."


translate spanish day4_main2_582156c4:


    th "¡No!{w} Incluso asumiendo que es una manzana normal y corriente, en cualquier caso, ¡no debería comerme fruta sin lavar!"


translate spanish day4_main2_0b227b4a:


    th "Así de esa manera..."


translate spanish day4_main2_f6b3f760:


    "Hallé un motivo para mi miedo y dejé a parte la manzana."


translate spanish day4_main2_38770f6e:


    "Era el momento de ir a cenar."


translate spanish day4_main2_13514a11:


    "Por el camino me encontré con Electronik."


translate spanish day4_main2_9e8d764f:


    me "¿Qué tal va? ¿Has encontrado a Shurik?"


translate spanish day4_main2_c6d96820:


    el "No, todavía no hay señales de él..."


translate spanish day4_main2_0228ef23:


    me "No te preocupes...{w} ¡Le encontraremos!"


translate spanish day4_main2_257dff95:


    "Traté de animarle."


translate spanish day4_main2_d1dd1c27:


    el "¡Ha transcurrido demasiado tiempo!{w} Seguiré buscándole."


translate spanish day4_main2_6ffcae27:


    me "¿Qué hay de la cena?"


translate spanish day4_main2_2b8d0068:


    el "No, encontrar a Shurik es más importante."


translate spanish day4_main2_a1983f0e:


    "Murmuró reflexionando."


translate spanish day4_main2_0470832e:


    "Le dejé solo en la encrucijada y le deseé buena suerte."


translate spanish day4_main2_00f424c0:


    "Los pioneros se amontonaban en la entrada."


translate spanish day4_main2_9219d4ed:


    "Aceleré mi ritmo, intentando no ser el último en la fila, al menos por hoy."


translate spanish day4_main2_29a4258a:


    "Y tuve suerte... en el rincón más alejado había una mesa absolutamente vacía."


translate spanish day4_main2_5d928b01:


    "Cogí mi cena rápidamente y tomé asiento en la mesa."


translate spanish day4_main2_dffdc57f:


    "La cena de hoy consiste en una sopa de fruta y un par de bollos."


translate spanish day4_main2_eac59148:


    "Este menú me sorprendió al principio, pero al saborearlo realmente estaba delicioso."


translate spanish day4_main2_e2cd40bb:


    "Me concentré en comer."


translate spanish day4_main2_c81cbbaa:


    mi "¡Lena, vayamos allá! ¡Mira... tres sillas vacías!"


translate spanish day4_main2_8bd763eb:


    "Lena y Miku se quedaron frente a mi."


translate spanish day4_main2_e140e52b:


    un "¿Están ocupadas?"


translate spanish day4_main2_0a28548a:


    "Preguntó Lena."


translate spanish day4_main2_82cf5563:


    me "No, coge asiento."


translate spanish day4_main2_e8b7b6b4:


    th "Claro que desearía que estuviera sola."


translate spanish day4_main2_082d3571_2:


    un "Gracias..."


translate spanish day4_main2_f1ccf9dd:


    "Tan pronto como Lena dijo eso Zhenya apareció detrás de ella."


translate spanish day4_main2_2ab6d036:


    mz "Me sentaré aquí. Están todas ocupadas."


translate spanish day4_main2_11d6a6d3:


    "Dijo mientras dejaba su bandeja en la mesa y se sentaba, sin ni tan siquiera esperar a mi respuesta."


translate spanish day4_main2_46957efa:


    me "Claro. Haz como si estuvieras en tu casa..."


translate spanish day4_main2_596d1d03:


    "Murmuré tristemente."


translate spanish day4_main2_6fa187e2:


    mz "¡¿Qué?!"


translate spanish day4_main2_cfaa423d:


    me "Nada..."


translate spanish day4_main2_1830a553:


    "A decir verdad, quería reducir toda la compañía a tan solamente Lena, a pesar de que ni Miku ni Zhenya me estuvieran causando problemas."


translate spanish day4_main2_eea06034:


    "Excepto que una era demasiado charlatana y la otra era muy arrogante."


translate spanish day4_main2_1ffd1a45:


    th "Pero aun así eran absolutamente inofensivas, especialmente comparándolas con otras a las que podría nombrar."


translate spanish day4_main2_0a276ba6:


    un "Oh, parece que me olvidé de mi llave..."


translate spanish day4_main2_1236c393:


    mi "¡No te preocupes, coge la mía!"


translate spanish day4_main2_39b90d94:


    "Estaba sorprendido por la escueta respuesta de Miku."


translate spanish day4_main2_b6287b9f:


    me "Vosotras...{w} ¿vivís juntas?"


translate spanish day4_main2_94800851:


    mi "¡Por supuesto! ¿No lo sabías? Juntas. Nuestra cabaña es la última de todas. Quiero decir la más alejada de aquí. Quiero decir la del final."


translate spanish day4_main2_0d57a898:


    "No me habría sorprendido si alguien me hubiera explicado que Lena vivía con Slavya{w} o con Zhenya en el peor de los casos.{w} Incluso con Electronik."


translate spanish day4_main2_c29a6bc8:


    th "Pero la silenciosa y tímida Lena con la excesivamente charlatana de Miku como compañera...{w} ¡Eso era realmente una sorpresa!"


translate spanish day4_main2_670ca875:


    mz "¿Has encontrado a Shurik?"


translate spanish day4_main2_40a04971:


    "Es extraño que Zhenya esté afectada por los problemas de alguien."


translate spanish day4_main2_5a7a7577:


    me "No."


translate spanish day4_main2_f9068a33:


    mz "Seguramente está en el pueblo comprando cigarrillos.{w} O vodka."


translate spanish day4_main2_58ae4528:


    "Se puso a resoplar."


translate spanish day4_main2_e9d77940:


    me "¿Pueblo?"


translate spanish day4_main2_d535d09a:


    "Por momentos se volvía más interesante la conversación."


translate spanish day4_main2_86a5cb53:


    mz "¿Tienes algún problema con los pueblos?"


translate spanish day4_main2_1490cc40:


    "Zhenya se extrañó."


translate spanish day4_main2_fd219846:


    me "¿Quieres decir que hay un pueblo cerca por aquí?"


translate spanish day4_main2_429609fe:


    mz "Eso creo..."


translate spanish day4_main2_858becb4:


    "Respondió con inseguridad."


translate spanish day4_main2_31ee18ee:


    "Miré a Lena y Miku, pero estaban entretenidas con su cena y no prestaron atención a nuestra conversación."


translate spanish day4_main2_9e200e27:


    me "¿Quieres decir que no lo sabes exactamente?"


translate spanish day4_main2_2c01aece:


    mz "¡¿Por qué tendría que saberlo?!"


translate spanish day4_main2_b265fe1c:


    "Zhenya miró fijamente a su plato."


translate spanish day4_main2_3a65b125:


    me "Pero debe haber alguna cosa en las cercanías."


translate spanish day4_main2_e76c967c:


    mz "Escucha..."


translate spanish day4_main2_a2516dfe:


    me "Escucho."


translate spanish day4_main2_391fb882:


    mz "¡No lo sé! ¡¿Me dejarás cenar?!"


translate spanish day4_main2_76b07f1d:


    th "Parece que no obtendré nada de ella."


translate spanish day4_main2_b71b185c:


    "Aunque existe la posibilidad de que ella realmente no sepa nada..."


translate spanish day4_main2_91e6868c:


    "El tiempo restante lo pasamos escuchando a Miku hablar sobre algo sin sentido. Me estaba volviendo loco poco a poco en silencio..."


translate spanish day4_main2_7c2f2de1:


    "Obviamente, la primera cosa que hice tras terminar fue irme para respirar aire fresco."


translate spanish day4_main2_ef1b28a4:


    "El sol se estaba poniendo."


translate spanish day4_main2_153cf25d:


    th "Decidí dar un paseo."


translate spanish day4_main2_2c803352:


    th "Es altamente improbable que halle alguna cosa más interesante para hacer durante el resto del anochecer. Así que, quizá, podría surgir algo bastante inesperado paseando."


translate spanish day4_main2_441bd7cd:


    "Me estaba acercando a la plaza cuando escuché un gran estrépito.{w} Parecía que algo había explotado."


translate spanish day4_main2_2c4a9213:


    "Estaba paralizado."


translate spanish day4_main2_9abc32b1:


    th "Estoy en un ambiente hostil, ¡desconociendo las normas y leyes de este lugar!"


translate spanish day4_main2_e35e500a:


    th "¡Sería mejor que corriera!{w} Pero a la vez tenía curiosidad..."


translate spanish day4_main2_f6711c85:


    "Probablemente me habría quedado simplemente quieto ahí, pero alguien me agarró de la mano."


translate spanish day4_main2_db010348:


    "Era Olga Dmitrievna."


translate spanish day4_main2_7cc1e65d:


    mt "¿Qué haces aquí quieto?{w} ¡Vayamos a ver qué ocurrió!"


translate spanish day4_main2_51893c59:


    me "¿No puedes ir sin mí?"


translate spanish day4_main2_df9d9f60:


    "Le supliqué lastimosamente."


translate spanish day4_main2_92fa383c:


    mt "¡No será nada importante!{w} Espero..."


translate spanish day4_main2_723aed67:


    "Cuando llegamos a la plaza ya había una muchedumbre de pioneros."


translate spanish day4_main2_0b603c9e:


    "Olga Dmitrievna se abrió paso vigorosamente entre la multitud y se acercó a la escena del crimen."


translate spanish day4_main2_c9d63825:


    "Obviamente alguien había intentado derribar a Genda."


translate spanish day4_main2_7a693237:


    "Pero los agresores fallaron... el monumento aun estaba en pie en su lugar."


translate spanish day4_main2_f15dbe9c:


    "Solamente había un tenue rastro de ceniza en el pedestal."


translate spanish day4_main2_466f41f2:


    mt "Y bien, ¿quién hizo esto?"


translate spanish day4_main2_fe6f2470:


    "Miró por encima de la muchedumbre de pioneros."


translate spanish day4_main2_588fe4a7:


    "Seguramente no estaba hecho por una organización terrorista perfectamente organizada."


translate spanish day4_main2_dcd82f25:


    "Estos individuos se acercaron solamente para ver qué sucedía."


translate spanish day4_main2_1da09f52:


    "Me percaté de la presencia de Ulyana y Alisa entre la multitud.{w} Y parece que nuestra líder del campamento se dio cuenta también."


translate spanish day4_main2_fdaff1c2:


    mt "¡Vosotras dos, venid aquí!"


translate spanish day4_main2_c276a464:


    "Se aproximaron a regañadientes."


translate spanish day4_main2_4013aec6:


    us "¿Por qué siempre yo?"


translate spanish day4_main2_ac0e5120:


    dv "Si eso crees..."


translate spanish day4_main2_f8a4a1d3:


    mt "¡Muéstrame tus manos, Dvachevskaya!"


translate spanish day4_main2_0c3815a2:


    dv "¿Qué pasa con ellas?"


translate spanish day4_main2_a967cbc5:


    "Las miré de cerca y vi que estaban manchadas de negro."


translate spanish day4_main2_6914b9a6:


    mt "Ahora está claro...{w} ¡¿Con qué hicistéis una bomba?!"


translate spanish day4_main2_74595ca6:


    "La aprendiz de terrorista parecía dudar si confesar o no, pero entonces soltó orgullosamente:"


translate spanish day4_main2_73fdb6c9:


    dv "¡Carbón activado, nitrato de potasio y azufre!"


translate spanish day4_main2_e3192dfb:


    th "¡Espera un momento!{w} ¡Carbón!{w} ¡Carbón que ella robó del equipo de primeros auxilios!"


translate spanish day4_main2_2d58bb92:


    mt "¿Por qué exactamente el monumento?{w} ¿Qué te hizo este honrado hombre a ti? El luchador por los derechos de..."


translate spanish day4_main2_23e3015c:


    dv "Además, él me dio el carbón activado."


translate spanish day4_main2_276a218b:


    "Me señaló a mi con su dedo. La plaza entera me miraba."


translate spanish day4_main2_034b00d2:


    "Pensamientos sobre la insensatez de este acto debido al pequeño tamaño de la bomba, me abandonaron en aquel instante."


translate spanish day4_main2_a6f5f832:


    me "¡Ella lo robó!{w} ¡No hice nada!"


translate spanish day4_main2_978d7670:


    mt "Incluso aunque él te lo hubiera dado, estoy segura de que Semyon nunca tomaría parte en semejante desagradable acto antisocial!"


translate spanish day4_main2_e7ed10ff:


    me "¡Sí, sí! ¡Exactamente!"


translate spanish day4_main2_d3ec559b:


    "Me puse de acuerdo."


translate spanish day4_main2_a20cefa7_7:


    "..."


translate spanish day4_main2_a20cefa7_8:


    "..."


translate spanish day4_main2_d88f4446:


    "Difícilmente podría imaginarme cuánto podría haber regañado a Alisa si Electronik no hubiera intervenido en aquel momento gritando:"


translate spanish day4_main2_e6b5d4f7:


    el "¡Le encontré! ¡Le encontré!"


translate spanish day4_main2_a09465a1:


    "Todo el mundo se dio la vuelta hacia él."


translate spanish day4_main2_50ceb1d4:


    "Sostenía en sus manos una bota."


translate spanish day4_main2_16b80206:


    el "¡Aquí está!"


translate spanish day4_main2_b9a254c5:


    "Electronik se jactó agitándola por encima de su cabeza."


translate spanish day4_main2_11da0634:


    el "¡Es la bota de Shurik!"


translate spanish day4_main2_c764847f:


    mt "¡Vale, tranquilízate!{w} ¡Cuéntanos en detalle dónde la hallaste!"


translate spanish day4_main2_a425fd4f:


    el "En el bosque.{w} ¡Por el camino hacia el antiguo campamento!"


translate spanish day4_main2_13dffab5:


    "Murmullos surgieron entre los pioneros."


translate spanish day4_main2_8c7b2ad8:


    all "El antiguo campamento... El antiguo campamento..."


translate spanish day4_main2_d3e8a3d1:


    mt "¿Estás seguro?"


translate spanish day4_main2_d0c3eb6a:


    el "¡Completamente!"


translate spanish day4_main2_438450d6:


    me "¿Qué tiene de especial ese antiguo campamento?"


translate spanish day4_main2_37372bdb:


    "Me uní a la conversación."


translate spanish day4_main2_1b61865c:


    mt "No tiene nada de especial, de verdad..."


translate spanish day4_main2_49a81686:


    "Dijo tartamudeando."


translate spanish day4_main2_91c692ef:


    el "Una de las leyendas de Sovyonok cuenta la historia del fantasma de una joven líder de campamento que habita ahí. Se enamoró de un pionero pero fue rechazada por él y entonces ella se suicidó..."


translate spanish day4_main2_417824db:


    us "Se hizo el hara-kiri con un cuchillo de cocina.{w} ¡Al día siguiente el muchacho fue atropellado por un autobús!"


translate spanish day4_main2_98631528:


    "Ulyana huyó corriendo de la multitud."


translate spanish day4_main2_5eb46b8b:


    me "¿Autobús?"


translate spanish day4_main2_94dec8ff:


    "Me abstuve de preguntar por el número de la línea."


translate spanish day4_main2_8e90eaa7:


    el "Pero la ciencia no ha confirmado la existencia de fantasmas. Así que no hay nada de qué preocuparnos."


translate spanish day4_main2_2d8001fb:


    mt "De todas formas, ¡alguien debería ir allí!"


translate spanish day4_main2_4ab0957c:


    "De repente la multitud comenzó a dispersarse."


translate spanish day4_main2_957e36e8:


    sl "Olga Dmitrievna, es casi de noche. ¿Tal vez mañana?"


translate spanish day4_main2_51b9d15d:


    "Me giré a mi alrededor y vi a Slavya y Lena."


translate spanish day4_main2_f8e498d7:


    mt "Y si por la noche...{w} ¿Y si por la noche le sucede algo...?{w} ¡No! ¡Hoy! ¡Ahora mismo!"


translate spanish day4_main2_ddc6851f:


    me "Por cierto, ¿dónde está ese lugar?"


translate spanish day4_main2_747f2d95:


    "Electronik me describió aproximadamente la dirección y me contó la historia del antiguo campamento."


translate spanish day4_main2_cb78152f:


    "La líder del campamento me miró atentamente."


translate spanish day4_main2_3ac0f1ea:


    me "Si crees que yo..."


translate spanish day4_main2_e13e7c4b:


    mt "Eres el único hombre aquí."


translate spanish day4_main2_81abb051:


    "Miré a mi alrededor."


translate spanish day4_main2_db2e005d:


    th "Por supuesto...{w} ¡Electronik fue rápido en escaparse!"


translate spanish day4_main2_0bb42214:


    "Aun así no tengo ganas de caminar solo por el bosque."


translate spanish day4_main2_eba0fa62:


    "Si me lo pides, quisiera..."


translate spanish day4_main2_83894804:


    "¿Qué estoy diciendo?{w} ¿Por qué querría ir ella conmigo...?"


translate spanish day4_main2_a89ae039:


    th "¿Estaría de acuerdo?"


translate spanish day4_main2_69f37ce2:


    "Miré a Slavya."


translate spanish day4_main2_22e7806d:


    "No me prestaba atención."


translate spanish day4_main2_f88a9c6c:


    th "Parece que no es mi día..."


translate spanish day4_main2_3bf87393:


    th "Así que su sed de aventuras tiene sus límites también."


translate spanish day4_main2_3ecb90c0:


    "Es estúpido pensar que Lena no se asustará."


translate spanish day4_fail_867359a2:


    me "¡¿Voy a ir yo solo?!"


translate spanish day4_fail_be1d7816:


    "Olga Dmitrievna parecía pensativa."


translate spanish day4_fail_d7898f7f:


    mt "Quizás tengas razón...{w} Iremos mañana por la mañana todos juntos."


translate spanish day4_fail_c890c351:


    "Nos quedamos en silencio por un rato, luego nos dispersamos por el campamento."


translate spanish day4_fail_6a9a81a0:


    "Todo el mundo pareció haberse olvidado de Alisa y su acto de terrorismo. Quizá por el poco daño causado."


translate spanish day4_fail_727cde7c:


    "Junto a la líder del campamento, nos dirigimos a nuestra cabaña."


translate spanish day4_fail_d95546c4:


    "Traté de pensar en por qué razón Shurik iría al antiguo campamento."


translate spanish day4_fail_6b65c5dc:


    "Está interesado en robots y cibernética, no en historias de miedo y fantasmas."


translate spanish day4_fail_9917267e:


    th "¿Qué tiene que ver la bota con todo ello...?"


translate spanish day4_fail_a35e5ae3:


    "Recordé cómo decidí que esperaría hasta que sucediera algo verdaderamente extraordinario..."


translate spanish day4_fail_ea2f2219:


    "Y entonces me di cuenta que... ¡esto lo era!"


translate spanish day4_fail_76ef374b:


    me "Olga Dmitrievna, creo que me voy a dar un paseo antes de ir a la cama."


translate spanish day4_fail_11c5c177:


    mt "¡Que no sea uno largo!"


translate spanish day4_fail_80e4f228:


    me "¡Vale!"


translate spanish day4_fail_e3c48cd1:


    "Cuando llegué a la plaza comprendí que vagar por los alrededores del bosque o algún otro lugar extraño pudiera no ser tan siniestro..."


translate spanish day4_fail_04005f5b:


    "¡Pero hacerlo por la oscuridad...!"


translate spanish day4_fail_9fa06fb8:


    "No quise volver a la cabaña de Olga Dmitrievna pero tenía que encontrar alguna linterna.{w} Por ejemplo, había una en la enfermería."


translate spanish day4_fail_f0d2d342:


    th "Me alegré de que no me pasara todo el tiempo allí leyendo la revista de moda."


translate spanish day4_fail_a20cefa7:


    "..."


translate spanish day4_fail_f5100b67:


    "La linterna la encontré rápidamente, y en pocos minutos ya estaba otra vez en la plaza, con renovadas fuerzas."


translate spanish day4_fail_48a0470e:


    "Justo tenía que tomar la última decisión antes de adentrarme en el bosque."


translate spanish day4_fail_4590a9c1:


    "Moviéndome a través de las cabañas de pioneros, siguiendo el camino del bosque hacia el campamento abandonado..."


translate spanish day4_fail_dc86bdf9:


    "Me adentré en el camino."


translate spanish day4_fail_7bd87323:


    "Electronik me explicó que el viejo edificio fue construido tras la guerra."


translate spanish day4_fail_fd3c1cdc:


    "Se parecía a un jardín de parvulitos (o como unos barracones, supuse) y claramente podía alojar menos pioneros que el actual campamento."


translate spanish day4_fail_e9bbdb04:


    "Había sido abandonado hacía ya unos veinte años."


translate spanish day4_fail_a20cefa7_1:


    "..."


translate spanish day4_fail_afeb43da:


    "Ahora se estaba totalmente a oscuras..."


translate spanish day4_fail_4b08c931:


    "El bosque se veía completamente distinto por la noche."


translate spanish day4_fail_00c53d58:


    "Sombras misteriosas se ocultaban tras los árboles. Extraños crujidos y graznidos de pájaros nocturnos provenían de cualquier parte, brancas que se quebraban traicioneramente bajo mis pies."


translate spanish day4_fail_7944d14b:


    "La luna llena estaba brillando, así que decidí ahorrar pilas de la linterna... Estoy seguro de que las necesitaría muy pronto."


translate spanish day4_fail_acbdce76:


    "Cuanto más lejos me adentraba, muchos más pensamientos desagradables me venían a la cabeza."


translate spanish day4_fail_e275989e:


    "No estaba preocupado del todo por la oscuridad. No creía ni en fantasmas ni en otras estupideces místicas, pero al mismo tiempo, durante los últimos días me he encontrado con demasiados fenómenos incomprensibles en contra de mis propias convicciones, comencé a leer entre líneas hasta con la cosa más insignificante."


translate spanish day4_fail_f927e7d0:


    "Obviamente me sentía excepcionalmente inquieto en el bosque por la noche."


translate spanish day4_fail_eab673b2:


    "Allá donde miraba, cada sombra, cada árbol o arbusto se semejaba a criaturas sobrecogedoras, que hubieran salido directamente de películas de horror o de las novelas de Stephen King."


translate spanish day4_fail_2ca15dc2:


    "Una branca parecía ser la mano de alguien, ansiosa de agarrarte por el hombro. Un tronco caído... el cadáver de alguien, alguien que fracasó en su intento de huir del bosque. En cada agujero vi un abismo inescrutable."


translate spanish day4_fail_c6ac6be9:


    "Si me hubiera encontrado en este bosque bajo cualquier otra circunstancia, sin todos estos autobuses y campamentos de pioneros, claro que me sentiría aun así incómodo, pero podría entonces explicar todo con lógica y no estaría preocupado de nada, salvo por las lechuzas y los ratones claro."


translate spanish day4_fail_bd7ebd85:


    "Pero en este momento habían razones más que suficientes para tener miedo."


translate spanish day4_fail_851bae5d:


    "A pesar de todo esto, alcancé el antiguo campamento sin ningún percance."


translate spanish day4_fail_a20cefa7_2:


    "..."


translate spanish day4_fail_c9298dc3:


    "En un pequeño claro rodeado por árboles se hallaba una cosa que parecía un jardín de parvulitos."


translate spanish day4_fail_ca186bf0:


    "La pintura se caía a trozos de las paredes. Casi todas las ventanas estaban rotas. Mampostería rota allí y allá, agujeros en el tejado a semejanza de aquellos realizados por un bombardeo."


translate spanish day4_fail_c42f0ebd:


    "Frente al edificio estaba un pequeño jardín, rodeado por una cerca oxidada."


translate spanish day4_fail_cb36b943:


    "Aparentemente, una vez fue un patio de juegos con su tiovivo, su columpio, su tobogán y otras atracciones soviéticas."


translate spanish day4_fail_ba4e4337:


    "Pero el tiempo es cruel. Todas ellas estaban rotas y mutiladas por la corrosión."


translate spanish day4_fail_f3f7ddd1:


    "La hierba estaba tan alta que casi me alcanzaba la cintura."


translate spanish day4_fail_c1bb36b5:


    "La luna surgió de entre las nubes e iluminó el viejo campamento."


translate spanish day4_fail_7a22a02d:


    "Era como si todo volviera a la vida: las ventanas brillaron con nuevos vidrios y las paredes parecían como si hubieran sido pintadas recientemente."


translate spanish day4_fail_96af7e9a:


    "Escuché una risa alegre. Vi unos niños translúcidos corriendo animadamente y jugando por el patio. Un estricto líder del campamento en la entrada, sosteniendo una cesta con manzanas maduras en sus manos. La vieja señora de la limpieza haciendo una siesta en la sombra."


translate spanish day4_fail_30d755a6:


    "Me dio escalofríos por la espalda."


translate spanish day4_fail_389b1cbf:


    "Por supuesto, mi imaginación me estaba jugando malas pasadas, pero el antiguo campamento por sí mismo era muy similar a las fotografías postapocalípticas que había visto, por lo que estas visiones no eran una sorpresa."


translate spanish day4_fail_e652569f:


    "Permanecí indeciso durante un largo rato antes de osar ir más lejos."


translate spanish day4_fail_654087eb:


    th "¡Después de todo, estoy aquí para encontrar a Shurik!"


translate spanish day4_fail_17d0369d:


    th "Aunque aun no consigo entender qué debe de estar haciendo en un lugar como éste...{w} si es que está aquí..."


translate spanish day4_fail_0e534257:


    th "¡Una bota próxima a aquí no significa nada!"


translate spanish day4_fail_e4b26812:


    "Sin embargo, si no fue devorado por los lobos (por alguna razón pareciera que no hay lobos en este bosque), éste debe ser el único lugar donde pudiera estar ocultándose."


translate spanish day4_fail_067a08ab:


    "Cuidadosamente crucé el patio, apartando la hierba alta a mi alrededor y tratando de no tropezarme con alguna escarpada pieza de metal."


translate spanish day4_fail_4c060fb7:


    "Pero me detuve en la escalera de la entrada, dudando."


translate spanish day4_fail_39f1d886:


    "Una impenetrable y pesada oscuridad me observaba devolviéndome la mirada desde dentro."


translate spanish day4_fail_443d22e0:


    "Incluso la brillante luz lunar no podía iluminar ningún objeto, o incluso resaltar una silueta."


translate spanish day4_fail_ef3a7e9d:


    "Mi cabeza estaba repleta de diferentes pensamientos, que a fin de cuentas todos ellos me decían que era mejor no adentrarse más y mejor salir corriendo rápidamente de allí."


translate spanish day4_fail_9aeb4e06:


    "Pero me armé de fuerzas, encendí la luz de la linterna y di un paso en el umbral."


translate spanish day4_fail_bef84d9a:


    "Dentro del edificio era como el típico jardín de parvulitos."


translate spanish day4_fail_11a53aa3:


    "Todavía recuerdo mis días de infancia, así que todo lo de aquí me resultaba muy familiar."


translate spanish day4_fail_b863bbfe:


    "Dormitorios alineados cerca los unos de los otros, una gran habitación para el ocio, lavabos, un comedor y habitaciones de trasteros."


translate spanish day4_fail_10696fa4:


    "Señales de júbilo infantil del pasado se apreciaban en todas partes. De tanto en tanto, la linterna dibujaba mi atención hacia una muñeca en descomposición, o un vehículo de juguete de plástico, o un balón de goma pinchado, o a una hilera rota de soldados de lata."


translate spanish day4_fail_186331b8:


    "Había un libro encima de una mesa, desteñido por la edad."


translate spanish day4_fail_cb2cab2c:


    "Hojeé unas pocas páginas."


translate spanish day4_fail_f381b7fd:


    "Era una historia que había leído hace mucho tiempo atrás, en mi infancia.{w} Algo acerca de la guerra..."


translate spanish day4_fail_b27a5c2e:


    "Los enemigos destruyeron el jardín de parvulitos y todos sus críos se escondieron en el pequeño sótano junto a un puñado de adultos, cuando las bombas explotaban sobre la superficie."


translate spanish day4_fail_25b7d2ba:


    "No recuerdo el final de la historia, pero si mal no recuerdo, nadie moría."


translate spanish day4_fail_81629920:


    "Observando las ilustraciones, todas las emociones surgían: el miedo, la tristeza, la empatía por los pobres niños y la sensación de desesperanza que había experimentado mientras leía aquel libro, volvieron a emanar sobre mí."


translate spanish day4_fail_1c3a4b63:


    "Es extraño que bajo esas mismas circunstancias, justo aquí y ahora, halla encontrado aquel libro en esta realidad."


translate spanish day4_fail_107ba7e9:


    th "Probablemente haya alguien que esté controlando el transcurso de esta historia, quien escribe el guión para este absurdo juego, quien viste a los pioneros lugareños con esa ropa y quien crea lugares como este campamento abandonado."


translate spanish day4_fail_0d6ce2d3:


    th "A decir verdad tiene mucho talento... la dedicación se presta incluso al más mínimo detalle."


translate spanish day4_fail_ffa0fc7b:


    "Aunque este lugar fuera abandonado hace largo tiempo, todo aquí parece haber permanecido igual sin tocarse."


translate spanish day4_fail_e82fe0f4:


    "Por supuesto, el tiempo no ha dejado de transcurrir para este campamento, en muchos años ni ha sido visitado por los lugareños."


translate spanish day4_fail_67a745c4:


    "Un lugar perfecto para la juventud, justo para adolescentes. Podrían beber alcohol, permitirse placeres amorosos o simplemente quemarlo todo por diversión."


translate spanish day4_fail_5cc1d037:


    "Electronik mencionó que este lugar está lleno de leyendas.{w} Leyendas de fantasmas y demonios habitando aquí..."


translate spanish day4_fail_20237b24:


    "Toma como ejemplo el relato sobre la líder del campamento que cometió suicidió."


translate spanish day4_fail_9f4da515:


    "En {i}mi{/i} mundo nadie creería que eso pudiera impedir hacer algo a alguien. Pero en esta realidad cualquier cosa es posible."


translate spanish day4_fail_5ae24796:


    "Ciertamente, el miedo no desapareció, pero decidí que ya que vine aquí, llegaría hasta el fondo de la cuestión."


translate spanish day4_fail_f267b85f:


    "El edificio era pequeño, por lo que no fue difícil buscar completamente por él."


translate spanish day4_fail_230cbcfd:


    "Pero no hallé ni una pista de Shurik."


translate spanish day4_fail_7e30252f:


    th "¿Quizá debería sencillamente llamarle?"


translate spanish day4_fail_14b6dd58:


    th "Pero por otro lado, tal vez no sería tan buena idea."


translate spanish day4_fail_7fff55da:


    "Frustrado, me senté en un banco en la entrada y suspiré."


translate spanish day4_fail_4d3113b1:


    me "Fantamas, ¿dónde estáis?"


translate spanish day4_fail_9d6a7df4:


    "Pregunté al campamento."


translate spanish day4_fail_7539188b:


    "Y de repente me di cuenta de un extraño surco en un rincón."


translate spanish day4_fail_3f4305c5:


    "Una pequeña trampilla... un adulto apenas podría escurrirse por ella."


translate spanish day4_fail_1154aac9:


    "Y me pareció que había sido abierta recientemente ya que el polvo a su alrededor estaba limpio."


translate spanish day4_fail_70f0a211:


    "Y ésta es la parte en la que debería empezar a tener miedo."


translate spanish day4_fail_4b219aad:


    th "Así que Shurik fue a alguna parte..."


translate spanish day4_fail_8e98e395:


    th "¿Qué podría estar acechando debajo de la trampilla?{w} ¿Un sótano?{w} ¿Qué estaría haciendo él ahí?"


translate spanish day4_fail_f1c33359:


    "Abrí la trampilla y apunté con la linterna en la oscuridad."


translate spanish day4_fail_d6d55fdd:


    "Había una escalera que se dirigía hacia abajo un puñado de metros, llegando a un suelo firme de hormigón."


translate spanish day4_fail_f98d9bf1:


    "Había un angosto pasillo que desaparecía en la oscuridad."


translate spanish day4_fail_da411e3b:


    "Dudé durante un buen rato."


translate spanish day4_fail_b42ebb80:


    "Por un lado, no tenía el más mínimo deseo de adentrarme ahí, pero por otro lado, si Shurik estaba realmente ahí..."


translate spanish day4_fail_84882d7a:


    "Maldecí a gritos y comencé a descender."


translate spanish day4_fail_ddec5394:


    "Abajo se estaba tan oscuro que incluso la linterna no era de mucha utilidad."


translate spanish day4_fail_30592890:


    "Un techo bajo, cableado extendiéndose a través de las paredes, con revestimiento de pasamanos de hierro rotos."


translate spanish day4_fail_43e505f2:


    th "Ya he visto todo esto en alguna parte..."


translate spanish day4_fail_1ed31216:


    "Empecé a moverme lentamente hacia la profundidad de la mazmorra."


translate spanish day4_fail_bd147fc2:


    "El camino fue hacia abajo y tras unas pocas docenas de metros se terminó en unas escaleras con escalones altos."


translate spanish day4_fail_af70ec38:


    "Pronto me hallé a mi mismo frente a una gran puerta de metal pesado."


translate spanish day4_fail_2c241583:


    "Había una señal pintada en ella."


translate spanish day4_fail_39f74a0e:


    "Miré de cerca y me di cuenta de que era una señal de peligro biológico."


translate spanish day4_fail_a60db0bf:


    th "No es de extrañar... si el antiguo campamento fue construido tras la guerra y duró hasta los últimos años de la década de los setenta."


translate spanish day4_fail_2bd94a6b:


    th "Así que era un refugio antiaéreo por lo que no había nada que temer..."


translate spanish day4_fail_d224dd30:


    "Aun así, esos pensamientos me asustaron."


translate spanish day4_fail_4b931dd1:


    "Tomé la decisión de abrir la puerta..."


translate spanish day4_fail_d415bd1a:


    "...la cual inmediatamente se cerró con un ruido chirriante tan pronto como atravesé la puerta."


translate spanish day4_fail_240fda6a:


    "Me estremecí fuertemente, mis manos empezaron a temblar y mi visión se volvió borrosa."


translate spanish day4_fail_8247734a:


    "Recuperé la consciencia tras unos segundos y solamente entonces me percaté de que aparentemente la puerta disponía de un fuerte resorte."


translate spanish day4_fail_9e97fbcb:


    "Me hallé en una habitación bastante espaciosa."


translate spanish day4_fail_0ba401c8:


    "Con el foco de luz de la linterna podía ver unas literas, algunos armarios por las paredes y diversos dispositivos de uso desconocido ubicados en el rincón más alejado."


translate spanish day4_fail_a1458d50:


    "Parece que este refugio antiaéreo jamás ha sido usado..."


translate spanish day4_fail_21c3af84:


    "Nadie llegó hasta aquí.{w} Las camas estaban hechas."


translate spanish day4_fail_274647ee:


    "Lo cual es extraño por sí mismo..."


translate spanish day4_fail_fd3face8:


    "Observé cuidadosamente alrededor de la habitación unas cuantas veces."


translate spanish day4_fail_6d5a6727:


    "Los armarios estaban llenos de máscaras de gas, trajes de protección químicos, raciones de conservas, diversos utillajes domésticos."


translate spanish day4_fail_76cae0d1:


    "Los dispositivos a primera vista podría determinar que servían para controlar los niveles de radiación, presión del aire y temperatura."


translate spanish day4_fail_8c887cf9:


    "En resumen, era el típico refugio antiaéreo."


translate spanish day4_fail_6cf7d00d:


    "Nunca había estado en semejante lugar antes, pero era justo como me lo imaginaba."


translate spanish day4_fail_1aa39f51:


    "La única cosa que permanecía inexplorada era la puerta al otro extremo de la habitación."


translate spanish day4_fail_efc3a1da:


    "Giré el pomo de la puerta, pero ésta permaneció cerrada."


translate spanish day4_fail_24bba245:


    th "Si alguien bajó recientemente aquí, y no se le hallara en esta habitación, entonces sería lógico asumir que tiene que estar tras la puerta."


translate spanish day4_fail_b7f23876:


    "Le llamé en voz baja."


translate spanish day4_fail_c088b6e1:


    me "¿Shurik...?"


translate spanish day4_fail_dfffcbac:


    "No hubo respuesta."


translate spanish day4_fail_f8c98a55:


    th "Quizá se encerró...{w} ¿Pero por qué?"


translate spanish day4_fail_234abe88:


    "No estaba claro qué debería hacer."


translate spanish day4_fail_d965f701:


    "Quizá sería más inteligente volver mañana con ayuda, pero por alguna razón tuve la sensación de que detrás de la puerta, hallaría respuestas que no podían esperar a ser encontradas."


translate spanish day4_fail_6f36167b:


    "Crucé caminando la habitación y agarré una palanca del rincón."


translate spanish day4_fail_88aff48d:


    "Puse la palanca en forma de cuña en la parte inferior de la puerta y traté de romper las visagras."


translate spanish day4_fail_548728ed:


    "Bastante extraño, tras un rato lo logré; afortunadamente no era tan grande como la puerta previa."


translate spanish day4_fail_90069d4f:


    "La puerta se salió y cayó golpeando en el suelo."


translate spanish day4_fail_eb8bd4b5:


    "Tomé un respiro profundo y apunté con la linterna por el pasaje."


translate spanish day4_fail_55ed8e88:


    "Un pasillo se desvanecía en la oscuridad, igual que el que me trajo hasta aquí."


translate spanish day4_fail_a20cefa7_3:


    "..."


translate spanish day4_fail_9276a63c:


    "La paredes se iban estrechando, mi cabeza casi tocaba el techo, pero no podía alcanzar el final del pasillo."


translate spanish day4_fail_709d42b2:


    "Caminé cuidadosamente, mirando al suelo. Esa fue la razón por la cual no caí por el agujero..."


translate spanish day4_fail_3903f0e0:


    "El agujero era alrededor de un metro y medio de diámetro, y había tierra abajo en el fondo en vez de hormigón."


translate spanish day4_fail_9b16cd8e:


    "Parece que el agujero fue causado por una explosión."


translate spanish day4_fail_80434573:


    th "No era demasiado profundo... Podría escalarlo fácilmente más tarde."


translate spanish day4_fail_cf5b6c1b:


    "Después de saltar al agujero, me hallé en un lugar que se asemejaba a una antigua mina."


translate spanish day4_fail_1a034f9f:


    "Las paredes y el techo estaban reforzados con vigas de madera."


translate spanish day4_fail_4bec4ed2:


    "El túnel era tan largo que mi linterna era insuficiente..."


translate spanish day4_fail_a20cefa7_4:


    "..."


translate spanish day4_fail_8e2bc52d:


    "Pocos minutos después me hallé en una encrucijada."


translate spanish day4_fail_68ebea2c:


    "Es demasiado peligroso ir más lejos, podría perderme."


translate spanish day4_fail_0ef45387:


    "Decidí que sería una buena idea marcar de alguna forma el lugar desde el cual empezaba mi viaje por el laberinto, así que recogí una piedra e hice una muesca grande en la pared."


translate spanish day4_fail_7dd017ff:


    th "Pues..."


translate spanish mine_crossroad_07a69b74:


    "Parece que hay otra bifurcación en el camino."


translate spanish mine_crossroad_ff6d6d5f:


    "Parece que hay otra bifurcación en el camino."


translate spanish fail_mine_return_to_start_fda95a66:


    "Retorné al lugar desde el cual había comenzado."


translate spanish fail_mine_return_to_start_d452f00a:


    th "Así que seguí el camino incorrecto por ahí."


translate spanish fail_mine_return_to_start_2bce21f4:


    th "Continuémos buscando."


translate spanish girls_mine_return_to_start_f89efc62:


    "Retornamos al lugar desde el cual habíamos comenzado."


translate spanish girls_mine_return_to_start_c806e46d:


    th "Así que seguimos el camino incorrecto por ahí."


translate spanish girls_mine_return_to_start_2bce21f4:


    th "Prosigamos buscando."


translate spanish fail_mine_coalface_61204914:


    "Estuve aquí hace tan sólo pocos minutos antes."


translate spanish fail_mine_coalface_31787369:


    "Finalmente salí caminando del túnel a una sala extensa con un techo elevado."


translate spanish fail_mine_coalface_3ef8c583:


    "Aunque a pesar de todo difícilmente se le podría llamar una sala; debían haber solido extraer alguna cosa aquí.{w} Carbón, probablemente, o tal vez oro."


translate spanish fail_mine_coalface_3c54fe69:


    "Las paredes habían sido talladas con picos o martillos neumáticos."


translate spanish fail_mine_coalface_3e6cec54:


    "Este lugar era negro como el alquitrán, por lo que mi única salvación era la linterna."


translate spanish fail_mine_coalface_ffe6f7a7:


    th "Si se rompiera, es improbable que pudiera escapar de aquí..."


translate spanish fail_mine_coalface_58411121:


    "En un rayo de iluminación localicé repentinamente un pedazo de ropa roja."


translate spanish fail_mine_coalface_98466bb2:


    "¡Un pañuelo de pionero!"


translate spanish fail_mine_coalface_96b3272c:


    "Shurik estaba obviamente aquí en alguna parte."


translate spanish fail_mine_coalface_93ec21f7:


    me "¡Shurik! ¡Shurik!"


translate spanish fail_mine_coalface_f23d45c0:


    "Solamente mi eco me respondió."


translate spanish fail_mine_coalface_cc8806c6:


    th "¿Pero dónde podría haberse ido?"


translate spanish fail_mine_coalface_db4b78eb:


    "No hay ningún lugar a dónde ir desde aquí."


translate spanish fail_mine_coalface_33bf97c7:


    th "Quizá haya otros lugares en estos túneles que no vi."


translate spanish fail_mine_coalface_858b6cbe:


    th "¡Así que debería buscar más!"


translate spanish fail_mine_halt_61204914:


    "Estuve aquí hace tan sólo pocos minutos antes."


translate spanish fail_mine_halt_739fe800:


    "Llegué a algún tipo de mina del campamento."


translate spanish fail_mine_halt_9f9a3875:


    "Picos y cascos estaban por todas partes, un vagón oxidado estaba en un rincón."


translate spanish fail_mine_halt_a2b01ca8:


    "Todos los utensilios era tan viejos... eran de principios del siglo XX más que de mediados."


translate spanish fail_mine_halt_631320fe:


    th "Entonces esto es realmente una mina.{w} Lo cual significa que podría estar vagando por aquí durante mucho tiempo sin hallar forma de salir."


translate spanish fail_mine_halt_1efc66da:


    "Pero tuve la sensación de que la salida estaba cerca en alguna parte y fui adentrándome."


translate spanish girls_mine_halt_e40597aa:


    "Ya habíamos estado aquí."


translate spanish girls_mine_halt_498e0e02:


    "Llegamos a algún tipo de mina del campamento."


translate spanish girls_mine_halt_9f9a3875:


    "Picos y cascos estaban por todas partes, un vagón oxidado estaba en un rincón."


translate spanish girls_mine_halt_a2b01ca8:


    "Todos los utensilios era tan viejos... eran de principios del siglo XX más que de mediados."


translate spanish girls_mine_halt_631320fe:


    th "Entonces esto es realmente una mina.{w} Lo cual significa que podría estar vagando por aquí durante mucho tiempo sin hallar forma de salir."


translate spanish girls_mine_halt_910be1a3:


    "Pero tuve la sensación de que la salida estaba cerca en alguna parte y fuimos adentrándonos."


translate spanish fail_mine_miku_29f8f3fe:


    "Fui más y más lejos..."


translate spanish fail_mine_miku_08cda699:


    "Parecía que no tenían fin estos túneles, como si estuviera atrapado en el Laberinto del Minotauro y la terrible criatura fuera a aparecer pronto."


translate spanish fail_mine_miku_6dda7c89:


    "Estaba exhausto y lentamente me resbalé al suelo, mientras me reclinaba contra la húmeda pared de la mina."


translate spanish fail_mine_miku_98a75609:


    th "¿Dónde demonios está Shurik...?"


translate spanish fail_mine_miku_db9b9ede:


    th "¿Y cuántas horas he estado caminando por aquí?"


translate spanish fail_mine_miku_93a335d8:


    "Mis ojos empezaron a cerrarse."


translate spanish fail_mine_miku_154d0a94:


    th "Sólo por un minuto..."


translate spanish fail_mine_exit_c43624ee:


    "Al girar por la última esquina, una puerta apareció de la oscuridad."


translate spanish fail_mine_exit_2236a915:


    "La abrí y fui adentro, una habitación pequeña."


translate spanish fail_mine_exit_50ed1d0a:


    "Nadie estaba aquí salvo..."


translate spanish fail_mine_exit_6c35e784:


    "...Shurik, ¡que estaba sentado en la esquina!"


translate spanish fail_mine_exit_b01a7f13:


    me "¡Shurik!"


translate spanish fail_mine_exit_bfe85680:


    "Me miró fijamente con los ojos llenos de terror."


translate spanish fail_mine_exit_2cfb869c:


    sh "¡AHH! ¡No te me acerques!"


translate spanish fail_mine_exit_f192c172:


    me "¡Shurik, soy yo! ¡Semyon!"


translate spanish fail_mine_exit_fe05dcbb:


    sh "¡No seré una presa fácil!"


translate spanish fail_mine_exit_962a501b:


    "¡Agarró un pesado pasamanos que tenía detrás y empezó a blandirlo violentamente!"


translate spanish fail_mine_exit_ce533195:


    me "Shurik, ¡¿qué te ocurre?! ¡Tranquilízate!"


translate spanish fail_mine_exit_1a0db18d:


    sh "¡Primero me has traído hasta aquí! ¡Luego me arrastras hasta esta mina! ¡Y ahora pretendes insinuar que eres Semyon!"


translate spanish fail_mine_exit_f7d0be86:


    th "Se debe haber vuelto completamente loco..."


translate spanish fail_mine_exit_95727752:


    me "¡Shurik! ¡Tranquilízate! ¡Nadie pretende suplantarme! ¡Soy yo de verdad!"


translate spanish fail_mine_exit_d566ef9e:


    "Traté de hablar lo más calmado que podía, pero me estaba sintiendo casi más inquieto que él."


translate spanish fail_mine_exit_c8104f36:


    sh "¿Semyon?"


translate spanish fail_mine_exit_c9c76fb6:


    "Echó un vistazo."


translate spanish fail_mine_exit_e5f3e2a0:


    sh "¿Eres de verdad tú?"


translate spanish fail_mine_exit_6034747a:


    me "¡Sí! Y ahora cuéntame qué ocurrió. ¿Cómo llegaste hasta aquí?"


translate spanish fail_mine_exit_d1fc8105:


    "Shurik recobró su aliento y comenzó a hablar:"


translate spanish fail_mine_exit_b0096a58:


    sh "Necesitaba algunas partes para el robot.{w} Y escuché que aquí había un refugio antiaéreo en el antiguo campamento...{w} Y que podrían haber algunas herramientas ahí."


translate spanish fail_mine_exit_400eb8c8:


    sh "Por lo que me levanté por la mañana y fui..."


translate spanish fail_mine_exit_04e9519a:


    "Se detuvo por un minuto, tratando de lidiar con el temblor de sus manos."


translate spanish fail_mine_exit_c71c2b1c:


    sh "Encontré algunas partes útiles pero entonces..."


translate spanish fail_mine_exit_3fd0e1a0:


    "Pude leer miedo instintivo en sus ojos."


translate spanish fail_mine_exit_3bf32b52:


    sh "Las voces... Me decían que fuera hasta aquí... Fui hasta abajo de la mina... Luego me decían que fuera a la derecha, luego a la izquierda, luego a la derecha, luego a la izquierda, luego a la derecha, luego a la izquierda..."


translate spanish fail_mine_exit_0c32fa11:


    "Comenzó a reír histéricamente."


translate spanish fail_mine_exit_3edbb0d2:


    sh "¡Pero ahora no me atraparán! ¡Me quedaré sentado aquí! ¡Ellas no me podrán hallar aquí!"


translate spanish fail_mine_exit_8453558b:


    "Una vez más empezó a agitar el pasamanos en el aire."


translate spanish fail_mine_exit_f1661152:


    me "Shurik... No hay nadie aquí... He desperdiciado mucho tiempo caminando en la mina como tú, pero no escuché ningunas voces."


translate spanish fail_mine_exit_2c5335a6:


    "Fijó su mirada en mí."


translate spanish fail_mine_exit_0495a2f5:


    sh "¡Entonces estás con ellas!"


translate spanish fail_mine_exit_fabedc6a:


    "Shurik se abalanzó rápidamente sobre mí."


translate spanish fail_mine_exit_4dcbc4d2:


    "Apenas podía esquivarle."


translate spanish fail_mine_exit_4ca30280:


    "Pero la linterna no tuvo tanta suerte... y se llevó toda la fuerza del impacto de Shurik."


translate spanish fail_mine_exit_973c06e7:


    "Nos hallamos en completa oscuridad."


translate spanish fail_mine_exit_9978cb5a:


    "Ahora era mi turno para perder la cabeza."


translate spanish fail_mine_exit_6eeebb1a:


    me "¡¿Qué has hecho, idiota?!"


translate spanish fail_mine_exit_da6ba350:


    "Me lancé a golpear con mi mano en la dirección en la que supuse que Shurik estaba, pero no fue útil."


translate spanish fail_mine_exit_68d73b99:


    me "¡Ahora moriremos ambos aquí!"


translate spanish fail_mine_exit_e68105fc:


    "No obtuve nada salvo silencio como respuesta, y luego una risa bastante histérica."


translate spanish fail_mine_exit_222240df:


    sh "Está bien... Todo correcto...{w} ¡Sé dónde está la salida! ¡Sígueme!"


translate spanish fail_mine_exit_1b5cf4c1:


    me "¡¿Seguirte a ti adónde?! ¡No puedo ver ni una maldita cosa!"


translate spanish fail_mine_exit_78e3c7d3:


    sh "Sigue mi voz."


translate spanish fail_mine_exit_05321f51:


    "No tuve otra opción."


translate spanish fail_mine_exit_e7e27795:


    sh "Por aquí..."


translate spanish fail_mine_exit_013b19be:


    "La voz de Shurik se escuchó en alguna parte más adelante."


translate spanish fail_mine_exit_cb4b20ed:


    me "¿Cómo sabes hacia dónde ir?"


translate spanish fail_mine_exit_c98b8e2a:


    sh "Caminé hasta llegar hasta aquí..."


translate spanish fail_mine_exit_0cd4b3c9:


    me "¿Entonces por qué no te fuiste?"


translate spanish fail_mine_exit_88482955:


    sh "Las voces...{w} ¡Silencio! ¡Nos pueden oír!"


translate spanish fail_mine_exit_1e45ae53:


    "En teoría podia creer que existía algún tipo de fuerza sobrenatural aquí."


translate spanish fail_mine_exit_54f45c90:


    "En el campamento Sovyonok solamente sería una ocurrencia más del día."


translate spanish fail_mine_exit_06e526d9:


    "Pero no había razón por la que entrar en pánico aun."


translate spanish fail_mine_exit_e559fc54:


    "A pesar de que mis manos empezaban a temblar sin parar."


translate spanish fail_mine_exit_e7e27795_1:


    sh "Por aquí..."


translate spanish fail_mine_exit_695378fd:


    "Lentamente seguí su voz y al final perdí la noción del tiempo."


translate spanish fail_mine_exit_a5d3cc23:


    "Podríamos haber estado vagabundeando durante unos minutos... o unas horas."


translate spanish fail_mine_exit_3dae981c:


    "Finalmente una tenue luz apareció en la distancia."


translate spanish fail_mine_exit_a370b3b4:


    sh "Ya llegamos..."


translate spanish fail_mine_exit_9a15b181:


    "Alcanzamos el muro.{w} El camino hacia la libertad estaba obstruido por una reja en el techo."


translate spanish fail_mine_exit_0b2dcdb3:


    "Me apreté junto a ella y reconocí la plaza a través de los pedazos del panorama que podía ver."


translate spanish fail_mine_exit_49341337:


    me "¡Álzame!"


translate spanish fail_mine_exit_56091405:


    "Shurik obedientemente aceptó mi petición."


translate spanish fail_mine_exit_f0562a64:


    "Empecé a golpear la reja con mis puños con tal fuerza que en tan sólo con unos golpes, mis brazos quedaron completamente cubiertos de sangre..."


translate spanish fail_mine_exit_c8bf1db3:


    "Pero en respuesta la reja cedió y se abrió al fin."


translate spanish fail_mine_exit_8a62a2ef:


    "Un minuto después estábamos fuera en la superficie detrás del monumento a Genda."


translate spanish fail_mine_exit_aefc775f:


    "Cuando vi esta reja antes, no tenía ni idea de que pudieran haber catacumbas bajo ella..."


translate spanish fail_mine_exit_96505b67:


    sh "¡Vendrán a por ti!"


translate spanish fail_mine_exit_0735175d:


    "Me dejé caer al suelo, exhausto."


translate spanish fail_mine_exit_a444ac12:


    me "¿Quién?"


translate spanish fail_mine_exit_823b8704:


    sh "¡Vendrán!"


translate spanish fail_mine_exit_0f567504:


    "Dijo Shurik otra vez y se fue corriendo..."


translate spanish fail_mine_exit_7a6522f6:


    "Estaba tan exhausto que no podía incluso molestarme en pensar sobre unas voces, o recordar el abandonado campamento, el refugio antiaéreo o la mina."


translate spanish fail_mine_exit_4d8a8dd9:


    "Casi inconscientemente, llegué a la cabaña de la líder del campamento."


translate spanish fail_mine_exit_6363b20e:


    "Pero no podía irme a la cama directamente."


translate spanish fail_mine_exit_f6e23c0c:


    mt "¡Bueno, mira quien está aquí!"


translate spanish fail_mine_exit_f10bec26:


    me "Olga Dmitrievna..."


translate spanish fail_mine_exit_ea404d77:


    "Comencé agotado, intentando recortar las explicaciones al mínimo posible."


translate spanish fail_mine_exit_1e3c4bff:


    mt "Dijiste que te «ibas a dar un paseo», ¡pero en vez de eso...!"


translate spanish fail_mine_exit_ffab041d:


    mt "Y yo... ¡Estaba a punto de irme y llamar a la policía!{w} Y..."


translate spanish fail_mine_exit_4370a259:


    me "Olga Dmitrievna, ¿puedes esperar hasta mañana? Estoy muy, muy cansado de veras."


translate spanish fail_mine_exit_dfbc9991:


    "La líder del campamento me echó un vistazo."


translate spanish fail_mine_exit_7e1d09c7:


    mt "Vale, de acuerdo..."


translate spanish fail_mine_exit_5344f89f:


    "Estaba un poco sorprendido de que estuviera de acuerdo con tanta facilidad, pero prefería no desaprovechar esta oportunidad y, rápidamente, apagué las luces y me estiré bajo las sábanas."


translate spanish fail_mine_exit_74343bec:


    "Y entonces aquella muchacha de la mina apareció justo ante mis ojos."


translate spanish fail_mine_exit_4ae428ab:


    "La vi con tanta claridad como si ella realmente estuviera frente a mi cama."


translate spanish fail_mine_exit_5fc49513:


    th "No, sólo estoy viendo cosas..."


translate spanish fail_mine_exit_16730ca7:


    th "Pero aun así, ¿por qué no me asusté a su vez? Solamente fui y hablé con ella con total normalidad. Así tal cual."


translate spanish fail_mine_exit_ee91ad9c:


    th "Todas las respuestas están aquí. Esa muchacha con orejas de gata sabe algo con certeza."


translate spanish fail_mine_exit_0011fed6:


    "Quizás estaba demasiado exhausto por culpa del antiguo campamento, la mina, y por el laberinto y mis nervios a flor de piel."


translate spanish fail_mine_exit_e12babdb:


    th "Debería haberle preguntado sobre alguna cosa..."


translate spanish fail_mine_exit_4ab8d03c:


    th "Pero todavía tengo tiempo..."


translate spanish day4_uv_c936a5db:


    "A la siguiente esquina el foco de luz de la linterna iluminó una gran sombra."


translate spanish day4_uv_df679a7b:


    "Estaba temblando de miedo y no me podía mover ni un solo paso más."


translate spanish day4_uv_52523f03:


    "Me desperté de mi estupor sólo por la idea de que aquello pudiera ser Shurik."


translate spanish day4_uv_b01a7f13:


    me "¡Shurik!"


translate spanish day4_uv_f8c43a30:


    "No hubo respuesta..."


translate spanish day4_uv_213cd37d:


    "Alrededor del rincón la sombra desapareció, vi una forma humana borrosa..."


translate spanish day4_uv_59b5788c:


    "Alguien estaba sentado sobre una piedra.{w} O tal vez alguna cosa..."


translate spanish day4_uv_480f771d:


    "Me detuve en seco."


translate spanish day4_uv_2821e6eb:


    me "¿Qué estás haciendo aquí?"


translate spanish day4_uv_51e9a976:


    "Escuché una voz dulce:"


translate spanish day4_uv_6add52f7:


    uvp "Atrapando ratones."


translate spanish day4_uv_88e92f6a:


    "Abrí mi boca...{w} pero no podía decir nada."


translate spanish day4_uv_51149e68:


    me "..."


translate spanish day4_uv_c7528539:


    uvp "¿Viste alguno por aquí?"


translate spanish day4_uv_d695b46b:


    me "No, pero estoy seguro de que hay muchos de ellos..."


translate spanish day4_uv_a4cdf691:


    "Alumbré con mi linterna su rostro..."


translate spanish day4_uv_3abd8a8c:


    "O había algo raro en su cabeza o bien en su especie biológica, ya que sus orejas y su cola no se parecían en nada a un humano."


translate spanish day4_uv_157dacf2:


    me "¿Y cuántos has atrapado?"


translate spanish day4_uv_02281cca:


    uvp "Ni uno."


translate spanish day4_uv_3d4f4933:


    "Respondió seriosamente."


translate spanish day4_uv_e2d148b8:


    "No tenía aspecto de ser hotil. Subconscientemente sentí que no debía preocuparme por ella, pero a la vez no tenía ni la más absoluta idea de qué decirle, cómo decírselo, qué es o qué hace."


translate spanish day4_uv_5be41974:


    "Pero por otra parte, normalmente si de repente te encuentras con alguien en una mina desierta tendrías un ataque de corazón del susto."


translate spanish day4_uv_11cfe5f6:


    "¡Y la muchacha frente a mi no era una pionera del campamento definitivamente!"


translate spanish day4_uv_3c5fb85c:


    me "Y tú... ¿Quién eres tú?"


translate spanish day4_uv_2372d07d:


    uvp "Probablemente le estás buscando a {i}él{/i}..."


translate spanish day4_uv_2a985733:


    me "¿A quién?"


translate spanish day4_uv_b9a21822:


    uvp "Ese muchacho..."


translate spanish day4_uv_ea6d6d42:


    me "Bueno...{w} Sí..."


translate spanish day4_uv_8fd15dcf:


    "Respondí tras una pequeña pausa."


translate spanish day4_uv_6bae3f57:


    me "Pero aun así..."


translate spanish day4_uv_e657d961:


    "La muchacha me interrumpió."


translate spanish day4_uv_9d150002:


    uvp "Él ha estado caminando por aquí durante un largo rato.{w} Traté de hablar con él, pero no me escuchó."


translate spanish day4_uv_5c1c58e4:


    me "¡¿Y dónde está él ahora?!"


translate spanish day4_uv_813a06f8:


    uvp "No lo sé...{w} Tal vez todavía esté vagando por alguna parte."


translate spanish day4_uv_e48430c4:


    "No parecía estar muy interesada en esta conversación."


translate spanish day4_uv_75223819:


    me "Y tú...{w} ¿Cómo has llegado...{w} hasta aquí?"


translate spanish day4_uv_b4a3ab00:


    uvp "De la misma forma que lo hiciste tú. Desde la superficie."


translate spanish day4_uv_1654f26c:


    me "¿Sin linterna?"


translate spanish day4_uv_f2374f2b:


    uvp "¿Por qué no?"


translate spanish day4_uv_d222fc60:


    "Me echó una mirada de perplejidad."


translate spanish day4_uv_2250eefe:


    "Perdí completamente toda comprensión."


translate spanish day4_uv_eec7a97d:


    "La linterna parpadeó. Asustado, empecé a golpearla con mi puño."


translate spanish day4_uv_a5894410:


    "Cuando la luz volvió a iluminar, la muchacha ya no estaba allí..."


translate spanish day4_uv_38eb0973:


    th "No sé quién o qué es ella, pero podría serme de ayuda para hallar las respuestas."


translate spanish day4_uv_4173197c:


    "La cosa más extraña es que no estaba asustado de ella. Tan sólo sentí que esta muchacha no me haría ningún daño."


translate spanish day4_uv_9e917b0c:


    "¡Pero ahora debo encontrar a Shurik!"


translate spanish day4_uv_8288c2fa:


    th "Tengo la sensación de que ésta no será la última vez que me encuentre con esta muchacha..."


translate spanish day4_uv_53d1fea3:


    "No sé por qué, pero estoy seguro de ello."


translate spanish day4_sl_45708cef:


    sl "¡Iré con él!"


translate spanish day4_sl_678c7ff3:


    mt "¡Muy bien! Es mejor juntos."


translate spanish day4_sl_238ce8d6:


    me "¿Estás segura?"


translate spanish day4_sl_30273633:


    "Pregunté a Slavya en voz baja."


translate spanish day4_sl_ec7a09a0:


    "Quería estar seguro, pero..."


translate spanish day4_sl_bf4362df:


    "Ella no me respondió, solamente sonrió."


translate spanish day4_sl_3c3e2229:


    "Miré sin decir nada a Slavya por un rato."


translate spanish day4_sl_007619f0:


    th "¿Por qué siempre me está ayudando?"


translate spanish day4_sl_d905d2f2:


    mt "¡Muy bien! Buena suerte a ambos pues."


translate spanish day4_sl_b904fd9e:


    me "La necesitaremos..."


translate spanish day4_sl_ed27161f:


    "La líder del campamento y las otras muchachas se despidieron y se fueron por su cuenta."


translate spanish day4_sl_13a18ea4:


    "Estaba un tanto sorprendido de que trataran como algo normal nuestra caminata nocturna...{w} a través del bosque...{w} hacia el campamento abandonado."


translate spanish day4_sl_0bfe3d69:


    th "Bueno... no se puede hacer nada."


translate spanish day4_sl_2dbd94e3:


    sl "Espera un minuto, traeré una linterna."


translate spanish day4_sl_1279b922:


    th "Una linterna, claro..."


translate spanish day4_sl_078dc3eb:


    "Mientras estaba esperando a Slavya, tuve un extraño pensamiento."


translate spanish day4_sl_9047a687:


    th "¿Por qué ella siempre es tan agradable, simpática y está dispuesta a ayudarme?"


translate spanish day4_sl_78b4cda5:


    th "¿Por qué se ofreció a ir conmigo a Dios sabe qué lugar?"


translate spanish day4_sl_6e630e74:


    th "Siempre acaban siendo los más tranquilos..."


translate spanish day4_sl_07db949a:


    th "¿Debería preocuparme por ella?"


translate spanish day4_sl_0cebc928:


    sl "¡He vuelto!"


translate spanish day4_sl_f1ebc68b:


    "Con una sonrisa Slavya me dio la linterna."


translate spanish day4_sl_ad3d2e0c:


    sl "¿Vamos?"


translate spanish day4_sl_7bd87323:


    "Por lo que Electronik nos explicó, el viejo edificio fue construido justo tras la guerra."


translate spanish day4_sl_fd3c1cdc:


    "Se parecía a un jardín de parvulitos (o como unos barracones, supuse) y claramente podía alojar menos pioneros que el actual campamento."


translate spanish day4_sl_c4f7341a:


    "Había sido abandonado hacía ya unos veinte años..."


translate spanish day4_sl_a52ca566:


    "Ahora se estaba totalmente a oscuras..."


translate spanish day4_sl_3868483b:


    "Parecía como si el bosque estuviera preocupado tanto de nuestra presencia, como nosotros de él.{w} Los árboles se apartaban delicadamente, agrupándose a los lados del camino y pacientemente observándonos."


translate spanish day4_sl_59e3e4f7:


    "Una lechuza ululó en alguna parte lejos, pero más silenciosamente que de costumbre."


translate spanish day4_sl_6dd60970:


    "No estaba preocupado caminando al lado de Slavya."


translate spanish day4_sl_560f6e52:


    "Probablemente en cualquier otra situación, incluso en el mundo real, estaría demasiado atemorizado como para adentrarme en el bosque sin tener un camino apropiado."


translate spanish day4_sl_be70614e:


    "Pero traté de deshacerme de estos pensamientos."


translate spanish day4_sl_e29f96bd:


    "Aunque era extraño, Slavya probablemente confortaba mis instintos de supervivencia y mi sentido de la realidad quedaba afectado."


translate spanish day4_sl_78eb5981:


    "Si continuamos así, acabaré saltando directo encima de alguna bestia con una sonrisa en el rostro..."


translate spanish day4_sl_0b419961:


    me "No estás nada preocupada, ¿verdad?"


translate spanish day4_sl_63f53767:


    sl "¿Qué quieres decir?"


translate spanish day4_sl_ab195626:


    "Slavya se giró y me miró, sonriendo de una forma rara, ya fuera por incomprensión o por una educada pantomima.{w} O tal vez solamente estaba viendo cosas."


translate spanish day4_sl_d91626f2:


    me "Bueno... La noche, el bosque, el antiguo campamento..."


translate spanish day4_sl_1bd8d8f4:


    sl "Sí, claro, algo de escalofríos."


translate spanish day4_sl_cc73ad1f:


    me "¿Sólo un algo?"


translate spanish day4_sl_1b5187ef:


    sl "¿Estás preocupado?"


translate spanish day4_sl_8e0381aa:


    me "No... Bueno... Sí, pero es una reacción humana natural en este tipo de situación."


translate spanish day4_sl_4891112d:


    "No quería mostrar mi miedo, pues no estaba realmente asustado. Tan sólo sentía que sentir miedo, es lo que debería ser en una situación como ésta."


translate spanish day4_sl_9f24fbc1:


    "Es como si mi cuerpo se hubiera detenido en producir anticuerpos para luchar contra una infección.{w} Estaba asustado, no por el bosque nocturno, sino por el hecho de que estaba reaccionando hacia todo lo que sucedía mal."


translate spanish day4_sl_f337bb5a:


    "Por otra parte, tampoco quería hacer el ridículo."


translate spanish day4_sl_01e9adbb:


    sl "Ya veo."


translate spanish day4_sl_4fb83426:


    "Slavya sonrió otra vez y continuó caminando."


translate spanish day4_sl_a20cefa7:


    "..."


translate spanish day4_sl_68cdaa0d:


    "El camino, rodeado por árboles y arbustos, se iba haciendo más estrecho."


translate spanish day4_sl_42ad9c81:


    "Cuanto más profundo nos adentrábamos en el bosque, con menos claridad se podía observar la luna."


translate spanish day4_sl_eb593e12:


    "Por un instante las nubes ocultaron la luna y todo se sumergió casi en la más absoluta negra oscuridad."


translate spanish day4_sl_cbdffea8:


    "Cerré mis ojos estrechando los párpados y cuando los abrí, la luna volvió a recuperar su lugar de nuevo, pintando el mundo con sus colores plateados en su mortaja nocturna."


translate spanish day4_sl_50e0695f:


    th "¡Justo me percaté de cuánto llego a odiar la oscuridad!"


translate spanish day4_sl_368ffb97:


    "Finalmente un espacio despejado apareció delante a lo lejos y, en un minuto más tarde, estábamos al borde de un claro del bosque."


translate spanish day4_sl_92efe0b6:


    "Había un desvencijado edificio del antiguo campamento, erosionado por la lluvia, oxidado y con escarabajos."


translate spanish day4_sl_fb10b433:


    "Nos sonreía con un ventanas rotas como una boca desdentada. Un reproche silencioso de la muerte podía leerse en esa sonrisa."


translate spanish day4_sl_b33715a3:


    "Una niebla sepulcral cubría enteramente el claro de forma que fantasmas, espíritus malignos, zombis, diablos y otros entes malignos parecían amenazar desde la neblina."


translate spanish day4_sl_28fa12ec:


    "Este panorama bastaba para darle un ataque de corazón a una persona común, o al menos un ataque de pánico."


translate spanish day4_sl_f4b1d738:


    "Si Slavya no me agarraba la mano, con certeza que no sería capaz de aguantarme en pie..."


translate spanish day4_sl_0ab7ae30:


    sl "No asustará tanto por aquí."


translate spanish day4_sl_06ab0532:


    "Sonrió."


translate spanish day4_sl_9cfcb779:


    "No podía decirle nada, solamente le cogí de la mano estrechándosela."


translate spanish day4_sl_f65f03d8:


    th "¿Qué estoy haciendo aquí?{w} Buscar a Shurik por la noche en el bosque, ¡¿{i}aquí{/i}?!"


translate spanish day4_sl_5cc597fa:


    th "¿Cómo pude estar de acuerdo?{w} Ninguna persona normal en sus cabales lo haría..."


translate spanish day4_sl_3a771d0d:


    th "Pero el mundo a mi alrededor no puede ser llamado normal precisamente."


translate spanish day4_sl_f86ea402:


    "Traté de persuadirme a mi mismo de que estaba hallando respuestas, y que por tanto no podía ir a peor."


translate spanish day4_sl_0ef9ad1a:


    "Para ser sinceros, nada malo me había ocurrido durante los últimos cuatro días en este campamento. A parte de ello, no había motivo alguno para creer que algo malo iría a suceder."


translate spanish day4_sl_4ddccd16:


    "Tal vez fuera la forma en que el mundo funcionaba... ¿la gente puede acaso vivir sin aventuras?"


translate spanish day4_sl_ad3d2e0c_1:


    sl "¿Vamos?"


translate spanish day4_sl_9776d559:


    me "Ehmm, sí, supongo..."


translate spanish day4_sl_0f0cb0fd:


    "Pero no hicimos ni un paso."


translate spanish day4_sl_05bba4b4:


    "Slavya parecía estar esperándome a dar el primer paso, pero ya tenía suficiente coraje con solamente estar de pie aquí sin salir huyendo."


translate spanish day4_sl_95f1e61d:


    "La luna se mostró desde detrás de las nubes otra vez e iluminó el edificio del antiguo campamento."


translate spanish day4_sl_682e5f18:


    "Ahora se parecía menos a una tumba, pero todas las mortales imaginaciones que imaginé se hicieron más reales. Las visiones de fantasmas se volvieron más tangibles, los lamentos del viento y los crujidos de la hierba se hicieron más claros. Ruidos de origen desconocido podían escucharse a lo lejos."


translate spanish day4_sl_0bcb6a42:


    sl "Resultó que era un gran lugar, en el pasado..."


translate spanish day4_sl_6cfd5fba:


    "Miré cuidadosamente a Slavya."


translate spanish day4_sl_3059e4dc:


    "Quizá ella estuviera asustada, pero trataba de ocultarlo.{w} Sabiendo que me sentía un poco mejor y más tranquilo."


translate spanish day4_sl_b55719b1:


    me "Tal vez. ¿Vamos?"


translate spanish day4_sl_7b8fbab7:


    sl "Claro..."


translate spanish day4_sl_57efbd93:


    "Caminé con cuidado, observando cada paso, tratando de no tocar la vieja cerca, el torcido tiovivo o el oxidado tobogán."


translate spanish day4_sl_031a7b86:


    "En algunas partes la hierba alcanzaba a la cintura, por lo que cualquier paso en falso podría dirigirnos a tener las piernas o las manos rotas en un accidente."


translate spanish day4_sl_b0c6ca01:


    "Al fin llegamos a la entrada y dirigí la linterna hacia el interior."


translate spanish day4_sl_ca84f49d:


    "La oscuridad que tenía delante era mucho más aterradora que la vista de atrás... al menos la luna brillaba fuera."


translate spanish day4_sl_1da1942d:


    "Reuní todo mi coraje y crucé el umbral."


translate spanish day4_sl_2eb693a0:


    "El interior del edificio del antiguo campamento se parecía a un jardín de parvulitos de mi infancia. La disposición del interior, incluso los pudridos muebles eran similares."


translate spanish day4_sl_c63ec5a4:


    me "Bueno..."


translate spanish day4_sl_2b54d2f5:


    "Temblé, identificando con mi linterna más y más recuerdos largamente desaparecidos de la alegría de la infancia."


translate spanish day4_sl_69664b95:


    sl "Espeluznante."


translate spanish day4_sl_5eabb736:


    "Slavya no solamente sostenía mi mano ahora, sino que se aferraba, apretándola firmemente contra sí misma."


translate spanish day4_sl_85558387:


    "Aun así no sentí miedo en su voz."


translate spanish day4_sl_592fe89e:


    me "Me pregunto si fue buena idea venir aquí en medio de la noche..."


translate spanish day4_sl_e330b23b:


    sl "Quizás no."


translate spanish day4_sl_438cf7fc:


    me "¿Deberíamos volver pues?"


translate spanish day4_sl_251c7d47:


    sl "¡No! ¿Qué hay de Shurik?"


translate spanish day4_sl_9a478370:


    "Dijo con un genuino asombro."


translate spanish day4_sl_6d33a4b2:


    me "Bueno, Shurik... ¿Por qué vino en primer lugar aquí? ¿Tal vez tampoco esté aquí?"


translate spanish day4_sl_1a99efc0:


    sl "¿Dónde podría encontrarse?"


translate spanish day4_sl_43f9ae8d:


    me "No lo sé. Devorado por los lobos, quizás."


translate spanish day4_sl_290c429f:


    sl "¡No digas eso!"


translate spanish day4_sl_167a71f8:


    "Slavya frunció el ceño y se alejó de mi."


translate spanish day4_sl_09003bb4:


    "Estaba ya tan acostumbrado a estar con ella que me enfadé un poco por su reproche."


translate spanish day4_sl_ff0e577b:


    me "Bueno, vale, vale... vamos a asumir que no hay lobos."


translate spanish day4_sl_d8ada2af:


    th "A pesar de que nunca puedes estar seguro..."


translate spanish day4_sl_74000c71:


    me "Bueno, ya que estamos aquí, busquémosle."


translate spanish day4_sl_ce617998:


    "..."


translate spanish day4_sl_5b164ba0:


    "Buscamos en todas las habitaciones del antiguo campamento: habitaciones de juegos, dormitorios, la cantina, la cocina y los lavabos. Incluso miré en los armarios, los cuales estaban repletos de cajas de cartón vacías y podridas."


translate spanish day4_sl_693b20be:


    sl "¡Mira!"


translate spanish day4_sl_b212fb3a:


    "Cuando retornamos al lugar donde comenzamos, Slavya cogió alguna cosa del suelo y me la dio."


translate spanish day4_sl_0ca58b5a:


    "Era una vieja muñeca rota, con hilos descosidos, careciendo de una mano y un ojo, desteñida por la humedad."


translate spanish day4_sl_23104825:


    me "¿Y bien?"


translate spanish day4_sl_c22db2eb:


    sl "Qué penita..."


translate spanish day4_sl_c1b9e2f6:


    me "¿Qué? ¿La muñeca?"


translate spanish day4_sl_8b01f8be:


    sl "Sí."


translate spanish day4_sl_d4817050:


    me "¿Por qué te da pena?"


translate spanish day4_sl_95ebdaf5:


    sl "Alguien jugó con ella, y luego simplemente la tiró y se olvidó de ella. Y ha estado aquí sola todos estos años, en la lluvia, en el frío..."


translate spanish day4_sl_37bdb5c9:


    "No sabría decir realmente si Slavya iba en serio o no."


translate spanish day4_sl_c14f498f:


    "No creo que sea el momento de pensar en esas cosas."


translate spanish day4_sl_0d679731:


    "Por otra parte, ¿cuál era el momento apropiado para ello?"


translate spanish day4_sl_3ca74edc:


    "Slavya parecía muy enfadada."


translate spanish day4_sl_d76f93fa:


    me "Sí, tal vez."


translate spanish day4_sl_34900be1:


    "Cogí la muñeca y la examiné."


translate spanish day4_sl_48de1c46:


    "Recuerdo como una vez hallé un león de peluche justo de la misma manera que Slavya encontró esta muñeca. Me lo llevé a casa, lo limpié y fui tan cuidadoso con él como lo era con otros juguetes, incluso a pesar de que tenía muchos años como para retirarse."


translate spanish day4_sl_3ccc4d70:


    me "Deberías dárselo a Ulyana."


translate spanish day4_sl_c954d69f:


    "Slavya se rio."


translate spanish day4_sl_3ae7ec76:


    "De repente el viento ululó por el pasillo, cantando un tipo de melodía fantasmal."


translate spanish day4_sl_0aa5972c:


    "Una luz destelló e instintivamente salté, se me quedó atrapada la pierna en alguna cosa y me caí en el suelo."


translate spanish day4_sl_7c5d32f6:


    sl "¿Estás bien?"


translate spanish day4_sl_28fa8946:


    "Slavya corrió hacia mi y me ayudó a levantarme."


translate spanish day4_sl_e01d393f:


    me "Sí, más o menos..."


translate spanish day4_sl_f92f7071:


    "Me tropecé con una trampilla."


translate spanish day4_sl_e28dff1c:


    "Es extraño que no nos hubiéramos percatado ya de ella... la suciedad alrededor de la puerta de la trampilla estaba limpia. Parecía que fue abierta no hace mucho tiempo."


translate spanish day4_sl_473e195c:


    me "No vamos a bajar por ahí, ¿verdad?"


translate spanish day4_sl_0f373940:


    "Pregunté, tratando de anticiparme a las sugerencias de Slavya."


translate spanish day4_sl_beaacb2a:


    "Ella tan sólo me dirigió una triste mirada."


translate spanish day4_sl_3a450870:


    me "Vamos... ¿estamos jugando a ser topos ahora?"


translate spanish day4_sl_e7af80fb:


    "Me quejé, mientras levantaba la puerta de la trampilla con bastante esfuerzo."


translate spanish day4_sl_637f8cc9:


    "Mi sentido común se lamentaba, diciéndome que no debería hacer esto, pero por el contrario mi razonamiento me sugería que si este mundo me quería muerto, ya habría sido muerto."


translate spanish day4_sl_7e0c9bc6:


    "Pero es solamente a causa de Slavya, ¡de otra forma no bajaría por ahí ni en broma!"


translate spanish day4_sl_a133f4fb:


    "Dos metros de una escalera de metal oxidada llevaban a un suelo firme de hormigón."


translate spanish day4_sl_ceabf4e4:


    "Me incliné por el borde e iluminé un pasillo estrecho, que se perdía en la oscuridad."


translate spanish day4_sl_fb03e6d3:


    me "¿Y qué hay ahí?"


translate spanish day4_sl_030f1acd:


    sl "Creo que escuché algo sobre un refugio antiaéreo bajo el antiguo campamento."


translate spanish day4_sl_28acc263:


    me "¿Un refugio antiaéreo?"


translate spanish day4_sl_d96d518d:


    "Bueno, eso es bastante lógico, considerando en la época en que se construyó."


translate spanish day4_sl_8017924d:


    me "¿Por qué iria a necesitar Shurik ir ahí?"


translate spanish day4_sl_311a3a2f:


    sl "No lo sé."


translate spanish day4_sl_a1d6e601:


    th "Por supuesto, no era necesario que fuera él quien hubiera abierto la trampilla (y es obvio que alguien la había abierto recientemente), pero... ¿quién iba a ser si no un chiflado inventor el que tuviera la idea de bajar ahí abajo?"


translate spanish day4_sl_0eb09c36:


    th "Si rechazamos la involucración de teorías sobrenaturales, no pueden haber otros humanos en este bosque."


translate spanish day4_sl_91db26c0:


    "Al menos eso es lo que pensé."


translate spanish day4_sl_c068a23a:


    me "Está bien, iré primero, si todo está correcto entonces sígueme."


translate spanish day4_sl_85ed45c7:


    sl "¿Qué podría no estar bien?"


translate spanish day4_sl_9cb98215:


    "Slavya me miró asustada, pero parecía preguntarme más que decirlo por cortesía."


translate spanish day4_sl_60d825fa:


    me "Bueno, no lo sé... Ratas, por ejemplo. ¿Te preocupan las ratas?"


translate spanish day4_sl_84e0dd1c:


    th "Supongo que no debería haberle preguntado."


translate spanish day4_sl_73542db8:


    "Suspiré profundamente y comencé a descender."


translate spanish day4_sl_ce617998_1:


    "..."


translate spanish day4_sl_2b476ea8:


    "Avanzamos lentamente, agarrados de las manos."


translate spanish day4_sl_e4392fea:


    "En la oscuridad total, la linterna no era de gran ayuda. Solamente de vez en cuando podía iluminar pedazos de viejos periódicos y trozos de metal aquí y allá."


translate spanish day4_sl_3c66bee2:


    "Repentinamente comencé a sentir como si las paredes y el techo, cubiertos de cables retorcidos sin fin, me estuvieran asfixiando, como si el pasillo se estuviera encogiendo."


translate spanish day4_sl_3ffd1664:


    "Alargué mis brazos, tratando de medir el ancho del pasillo."


translate spanish day4_sl_f987b701:


    sl "¿Estamos aun muy lejos?"


translate spanish day4_sl_cb8392a3:


    "Preguntó Slavya tristemente."


translate spanish day4_sl_8f2edfc1:


    me "¿Lejos respecto a qué? No tengo pista alguna."


translate spanish day4_sl_6f9aa3f1:


    sl "Claro..."


translate spanish day4_sl_af396620:


    "Justo entonces apareció una gran puerta de metal frente a mi como si nada."


translate spanish day4_sl_f6458d5d:


    sl "Oh..."


translate spanish day4_sl_1135b8b4:


    "Chilló Slavya suavemente."


translate spanish day4_sl_6d46c180:


    "La agarré por el pomo, y pareció ceder un poquito."


translate spanish day4_sl_a752ccba:


    me "¿Estás preparada?"


translate spanish day4_sl_8a3c9e52:


    sl "No estoy segura..."


translate spanish day4_sl_49678fc1:


    "Aun así, hablando francamente, ¿qué podría prepararte para semejantes situaciones?"


translate spanish day4_sl_4876f13f:


    "Giré unas cuantas veces el pomo, chirriando horriblemente, entonces finalmente alcanzó su punto límite y se quedó en silencio."


translate spanish day4_sl_a5571da6:


    "Empujé con todas mis fuerzas la puerta y a regañadientes se abrió.{w} Había un tipo de sala detrás de ella..."


translate spanish day4_sl_dfcada65:


    "Que resultó ser el refugio antiaéreo en sí mismo."


translate spanish day4_sl_40173f81:


    "Cuando entramos, las luces de repente se encendieron, haciendo que me encogiera al deslumbrarnos."


translate spanish day4_sl_2a01283b:


    "Aunque pareciera que había aquí algún tipo de fuente de energía independiente: a la vez que habían sido construidas estas catacumbas, la gente habia estado edificando cosas hasta el final."


translate spanish day4_sl_9d89915d:


    "El refugio antiaéreo estaba exactamente igual que como había imaginado. Tenía todo lo que se necesitaba para sustentar la prolongación de existencia humana durante una guerra: los armarios contenían trajes de protección química, máscaras de gas, raciones de conservas y botellas de agua. En la pared más alejada, me percaté de diferentes dispositivos: para la presión del aire, para los niveles de radiación, una radio y otros."


translate spanish day4_sl_867bb71b:


    "Sin embargo, este lugar parecía como si nunca nadie lo hubiera usado (lo cual no era en realidad sorprendente).{w} Más aun, tenía todo el aspecto de que nadie había entrado tras ser construido."


translate spanish day4_sl_0fa153ce:


    "Slavya me hizo una mirada inquisitiva."


translate spanish day4_sl_74d4b2c0:


    me "Bueno, Shurik definitivamente no está aquí."


translate spanish day4_sl_2e01cda6:


    sl "¿Pero estuvo?"


translate spanish day4_sl_ea7c0036:


    me "Ni idea. Pero si él estuvo, sencillamente no se pudo desvanecer."


translate spanish day4_sl_368d74f1:


    "Miré alrededor de la habitación otra vez y me di cuenta de otra puerta en la pared de mi izquierda."


translate spanish day4_sl_156f7cf2:


    me "¿Quizá por ahí?"


translate spanish day4_sl_03dccc13:


    "Sin embargo, la puerta no se abrió."


translate spanish day4_sl_79177436:


    sl "¿Y si la bloqueó desde el otro lado?"


translate spanish day4_sl_869dccdf:


    me "¿Pero por qué?"


translate spanish day4_sl_8a3c9e52_1:


    sl "No lo sé..."


translate spanish day4_sl_1f8bda75:


    "Aun así, era bastante posible, asumiendo que estuviera ahí."


translate spanish day4_sl_820a03b7:


    "Reflexionando sobre todo ello, Shurik estaba escondido, quizás escapando de alguien."


translate spanish day4_sl_dde3d971:


    "Tales pensamientos me inquietaron, y sentí como tenía que llegar tan rápidamente como pudiera al otro extremo de la puerta."


translate spanish day4_sl_5426c6ac:


    "Empujé con fuerza, pero la puerta solamente hizo un débil chirrido."


translate spanish day4_sl_1fecc13d:


    sl "Ten, tal vez esto pudiera funcionar..."


translate spanish day4_sl_c2d7184d:


    "Slavya me entregó un pasamanos."


translate spanish day4_sl_7b68d30d:


    "Hurgué con él, considerando cómo podría ayudarme a hacer palanca en la puerta."


translate spanish day4_sl_33d34823:


    me "Vamos a intentarlo."


translate spanish day4_sl_74880975:


    "Usando toda mi fuerza, me las arreglé para sacar la puerta de sus bisagras y cayó sobre el suelo."


translate spanish day4_sl_05aa00af:


    "Mientras me estaba recuperando de mi esfuerzo, Slavya iluminó con la linterna el nuevo pasillo abierto."


translate spanish day4_sl_00166c91:


    sl "Hay un corredor por aquí, justo igual que el anterior."


translate spanish day4_sl_a2885c69:


    me "Pufff... Éste lugar es un verdadero laberinto."


translate spanish day4_sl_c6391d27:


    sl "¿Vamos?"


translate spanish day4_sl_079f62e5:


    me "Espera..."


translate spanish day4_sl_41c2ba2c:


    "Me senté en una litera, agarré una polvorienta funda de almohada y me limpié el sudor de la frente."


translate spanish day4_sl_13ab9fc8:


    me "Déjame recuperar el aliento."


translate spanish day4_sl_87f6e3eb:


    "Slavya se sentó junto a mi y comenzó a estudiar los detalles de los patrones de las baldosas del suelo."


translate spanish day4_sl_48fd6cec:


    me "Mejor que apaguemos la luz de la linterna por ahora."


translate spanish day4_sl_406417b4:


    sl "Oh, sí..."


translate spanish day4_sl_aba53b88:


    "Se ruborizó ligeramente y apretó el botón de apagar."


translate spanish day4_sl_de24b50e:


    me "Esto es sencillamente siniestro..."


translate spanish day4_sl_12cb2c61:


    "Me di cuenta de que durante la última media hora no estuve tratando estos hechos como reales."


translate spanish day4_sl_87863660:


    "Como si no me estuviera ocurriendo a mi, como si simplemente fueran un sueño."


translate spanish day4_sl_6a0d0b3e:


    "Justo como entonces, hace cuatro días atrás, en los primeros minutos de hallarme aquí."


translate spanish day4_sl_79ab8f59:


    "Sí, al final quizás empiece ha acostumbrarme a este mundo, esta {i}realidad{/i}... ya que se ha convertido en realidad para mi."


translate spanish day4_sl_c2db5cf0:


    "Y esa profunda sensación en mi alma de que tenía que encontrar una salida no era ya tan crucial."


translate spanish day4_sl_a00907f6:


    "Pero una vez más, en un abrir y cerrar de ojos: el bosque, el antiguo campamento, el refugio antiaéreo."


translate spanish day4_sl_3c3e55db:


    "No había nada en absoluto de sobrenatural en cualquiera de ellos... pero tampoco era algo normal, como algo de mi vida anterior."


translate spanish day4_sl_7b8fbab7_1:


    sl "Sí..."


translate spanish day4_sl_a4965e91:


    "Estuvo de acuerdo Slavya."


translate spanish day4_sl_00992d89:


    me "¿No encuentras nada raro en todo esto?"


translate spanish day4_sl_a96c222e:


    sl "¿Exactamente qué?"


translate spanish day4_sl_5aaaf3dd:


    me "Bueno, el hecho de que estemos aquí sentados, solamente tú y yo, en la noche. ¿No sería lo normal en dicha situación esperar hasta la mañana y llamar a la policía en su lugar?"


translate spanish day4_sl_33cb1281:


    sl "Sí, supongo que sí."


translate spanish day4_sl_5e20fc4d:


    "Respondió pensativamente."


translate spanish day4_sl_aca724aa:


    me "Por supuesto, en realidad casi mi ofrecí voluntario. No debería haberlo hecho."


translate spanish day4_sl_e9545c3b:


    "Bueno, «casi» no era correcto, pero era así."


translate spanish day4_sl_0f83eee7:


    sl "Pero tú solamente querías ayudar. Si Shurik está aquí, deberíamos encontrarlo cuanto antes sea posible."


translate spanish day4_sl_6577f1d4:


    me "A este paso seremos nosotros los que tendrán que ser buscados. Ya sabes, se las apañó para perderse aquí."


translate spanish day4_sl_a336bbb9:


    "Slavya parecía frustrada."


translate spanish day4_sl_6e2ae7df:


    me "Esta bién, vale, no quise decir nada parecido. Quizá no esté aquí, tal vez en realidad se volvió al campamento hace rato."


translate spanish day4_sl_c05d1b8c:


    sl "Sí, quizás."


translate spanish day4_sl_7b411858:


    me "Pero deberíamos comprobarlo, ¿correcto?"


translate spanish day4_sl_49cc0ae4:


    sl "¡Por supuesto!"


translate spanish day4_sl_d6679b71:


    "Slavya se levantó y me tendió su mano."


translate spanish day4_sl_50e89832:


    "Parecía no estar tan preocupada como yo."


translate spanish day4_sl_555bb396:


    "Aun así ya estaba cansado de estar asustado, y ahora sólo quería acabar con todo esto cuanto antes fuera posible."


translate spanish day4_sl_ce617998_2:


    "..."


translate spanish day4_sl_8d29725f:


    "Este corredor era idéntico al otro desde el cual llegamos al refugio antiaéreo."


translate spanish day4_sl_c77a261c:


    "Estaba caminando a un ritmo lento, mirando cuidadosamente el suelo y comprobando de tanto en tanto si el pasillo se estrechaba."


translate spanish day4_sl_d14ade49:


    sl "¡Mira!"


translate spanish day4_sl_9f8002ec:


    "Slavya estrechó mi mano aun más y señaló hacia una apertura en medio del túnel."


translate spanish day4_sl_5197c9ee:


    "Era lo bastante ancho como para que un hombre lo escalara (o cayera abajo)."


translate spanish day4_sl_02b0275d:


    sl "Tal vez..."


translate spanish day4_sl_62129d64:


    "Apunté con la luz hacia abajo, pero parecía que no había nada salvo el suelo."


translate spanish day4_sl_642193e3:


    me "Creo que tenemos que comprobar esto."


translate spanish day4_sl_3c17ab25:


    "El agujero era lo bastante amplio como para escalarlo más tarde, por lo que a primera vista no había nada de qué preocuparse."


translate spanish day4_sl_bdf18d28:


    "Descendí con cuidado y ayudé a Slavya."


translate spanish day4_sl_7a9a5a51:


    "Nos encontramos en algún tipo de mina."


translate spanish day4_sl_171403e3:


    "Las paredes y el techo estaban reforzadas con vigas de madera, y unos raíles se estrechaban a lo lejos."


translate spanish day4_sl_3428662f:


    "Aquí y allá la tierra presionaba contra el frágil diseño de tablones carcomidos, los cuales estaban pudridos por el tiempo. Hacían que toda la estructura no inspirara confianza."


translate spanish day4_sl_a5f51cbc:


    sl "¿Dónde crees que estamos?"


translate spanish day4_sl_ee72963d:


    me "No lo sé, algún tipo de mina. ¿Qué podrían haber estado extrayendo aquí?"


translate spanish day4_sl_80b2ad66:


    sl "Carbón, ¿quizá?"


translate spanish day4_sl_0be49839:


    me "Quizás..."


translate spanish day4_sl_c39ad14a:


    "Empezamos a avanzar lentamente."


translate spanish day4_sl_29351dec:


    "Las piedras se desplazaban traicioneramente bajo nuestros pies allí y allá; la luz proveniente de la linterna tembló y parpadeaba de una pared a la otra, a menudo invocando sombras deformes las cuales desaparecían tan pronto como la luz estaba sobre ellas."


translate spanish day4_sl_253c2530:


    sl "Da más miedo aquí que arriba..."


translate spanish day4_sl_1066065f:


    me "Supongo."


translate spanish day4_sl_86ad247f:


    "Cuanto más lejos íbamos, ¡más creía que Shurik no tenía nada que ver con todo este lugar!"


translate spanish day4_sl_a28736d8:


    th "No podía haber sido raptado por un chupacabras, ¿acaso podría ser?{w} Está bien, no podemos descartar por completo esa posibilidad tampoco."


translate spanish day4_sl_2c6a97fe:


    "Al fin llegamos a un desvío en el túnel."


translate spanish day4_sl_11667520:


    sl "¿Hacia dónde vamos ahora?"


translate spanish day4_sl_595d7b8c:


    me "No lo sé."


translate spanish day4_sl_2d6b4b1d:


    "Traté de aparentar al menos algo parecido a calma en mi voz, pero realmente no tenía ni idea.{w} Nada de nada: ni adónde ir, ni dónde estaba Shurik, ¡ni tan siquiera dónde estábamos!"


translate spanish day4_sl_986e3d9c:


    sl "¿Y si nos perdemos...?"


translate spanish day4_sl_afcce3c5:


    me "Claro, cierto, dejemos una marca conforme hemos pasado ya por aquí."


translate spanish day4_sl_97280ee5:


    "Agarré una piedra bastante grande y con sumo cuidado grabé una cruz en una de las vigas que sostenía el techo."


translate spanish day4_sl_05bb4d2a:


    me "Con esta marca al menos sabremos que hemos cruzado ya por este punto."


translate spanish day4_sl_af1d965e:


    sl "Bien."


translate spanish day4_sl_c17a722d:


    "Slavya sonrió."


translate spanish day4_sl_bea7cbc5:


    sl "Entonces, ¿por cuál seguimos?"


translate spanish day4_sl_ce617998_3:


    "..."


translate spanish sl_mine_coalface_e40597aa:


    "Ya habíamos estado aquí."


translate spanish sl_mine_coalface_5b4c3c90:


    "Finalmente atravesamos el túnel y llegamos a una sala extensa con un techo elevado."


translate spanish sl_mine_coalface_3ef8c583:


    "Aunque a pesar de todo difícilmente se le podría llamar una sala; debían haber solido extraer alguna cosa aquí.{w} Carbón, probablemente, o tal vez oro."


translate spanish sl_mine_coalface_4253b858:


    "Las paredes habían sido talladas con picos o martillos neumáticos."


translate spanish sl_mine_coalface_c718c063:


    "Este lugar era negro como el alquitrán, por lo que nuestra única salvación era la linterna."


translate spanish sl_mine_coalface_b97863a3:


    th "Si se rompiera, es improbable que pudiéramos escapar de aquí..."


translate spanish sl_mine_coalface_58411121:


    "En un rayo de iluminación localicé repentinamente un pedazo de ropa roja en una esquina."


translate spanish sl_mine_coalface_ca0a4f90:


    "¡Eso era un pañuelo de pionero!"


translate spanish sl_mine_coalface_96b3272c:


    "Shurik estaba obviamente aquí en alguna parte."


translate spanish sl_mine_coalface_93ec21f7:


    me "¡Shurik! ¡Shurik!"


translate spanish sl_mine_coalface_cc29db24:


    sl "¡Shurik! ¡Shurik!"


translate spanish sl_mine_coalface_b99be3e2:


    "Solamente nuestro eco nos respondió."


translate spanish sl_mine_coalface_e27745b6:


    sl "No te preocupes, ¡estará bien!"


translate spanish sl_mine_coalface_b894ecef:


    "Francamente, en aquel momento estaba mucho más preocupado de mí mismo..."


translate spanish sl_mine_coalface_a411960f:


    th "¿Pero adónde podría haberse ido desde aquí?"


translate spanish sl_mine_coalface_55719814:


    "Esta sala no tenía otra salida..."


translate spanish sl_mine_coalface_2b95387f:


    th "Por supuesto, todavía habían algunos lugares en estos túneles a los cuales no habíamos estado aun."


translate spanish sl_mine_coalface_858b6cbe:


    th "Entonces... ¡tenemos que seguir buscando!"


translate spanish sl_mine_exit_e56eea78:


    "Tras otra esquina, la linterna iluminó una puerta de madera."


translate spanish sl_mine_exit_233631e4:


    me "Bueno, al menos eso es algo."


translate spanish sl_mine_exit_14491239:


    "A decir verdad, estaba tan cansado de estar vagando por esta mina que había dejado de pensar completamente sobre Shurik. Solamente quería hallar una salida tan pronto como fuera posible."


translate spanish sl_mine_exit_920627d5:


    sl "¿Qué hay tras ella?"


translate spanish sl_mine_exit_fac00056:


    me "Justo vamos a comprobarlo."


translate spanish sl_mine_exit_dc9f0445:


    "Tras la puerta había una pequeña sala, la cual se parecía a una habitación de trastero propia de una caldera, o a una de las habitaciones del refugio antiaéreo."


translate spanish sl_mine_exit_d4b4511f:


    "El suelo estaba contaminado de botellas vacías y las paredes estaban pintadas con una mugre de grafitis, lo cual significaba que otra gente había visitado más de una vez este lugar antes que nosotros.{w} Aunque, tal y como estaban las cosas, debían haber estado hace mucho tiempo."


translate spanish sl_mine_exit_b5d787b8:


    "Movía el foco de luz alrededor de la habitación, tratando de comprobar hasta el último rincón, cuando de repente..."


translate spanish sl_mine_exit_7d62949b:


    "Hallé a Shurik acurrucado en un rincón."


translate spanish sl_mine_exit_a7c6dc11:


    sl "¡Shurik!"


translate spanish sl_mine_exit_83931b19:


    "Chilló Slavya."


translate spanish sl_mine_exit_54f665b3:


    "Sin embargo, no parecía que nos estuviera prestando atención."


translate spanish sl_mine_exit_dd1de183:


    me "¡Aquí estás! ¡Hemos estado buscándote toda la noche! ¿Por qué diantres viniste aquí?"


translate spanish sl_mine_exit_959f145c:


    "Shurik me miró con unos ojos borrosos y ciegos."


translate spanish sl_mine_exit_f335a763:


    sh "¿Y quiénes sois vosotros?"


translate spanish sl_mine_exit_7393b21b:


    me "¿Qué quieres decir con «quiénes sois vosotros»? Levántate y vámonos."


translate spanish sl_mine_exit_5b4d50f1:


    sh "No voy a ir a ninguna parte con vosotros."


translate spanish sl_mine_exit_50ed4f02:


    "Shurik se apartó y permaneció en la oscuridad otra vez."


translate spanish sl_mine_exit_27e3ed3d:


    sh "Queréis engañarme otra vez. Me habéis hecho ir en círculos a través de los túneles, ¿no? Sí, ¡lo hicisteis! ¡Os conozco por lo que sois!"


translate spanish sl_mine_exit_284e0011:


    "La voz de Shurik se tornó un susurro diabólico."


translate spanish sl_mine_exit_fe9c97c8:


    me "Corta ya con ese sin sentido."


translate spanish sl_mine_exit_3069c4df:


    "Quería acercarme a él para ponerlo en pie, pero Slavya me detuvo."


translate spanish sl_mine_exit_f9e442d6:


    sl "No está en sus cabales."


translate spanish sl_mine_exit_a13f5a0a:


    me "Bueno, ¡tampoco yo estoy en mis cabales! Has vagado aquí toda la noche... seguro que no estarías muy cuerdo."


translate spanish sl_mine_exit_f98911a4:


    "Ella le sacudió su cabeza reprobándolo y, lentamente, ayudó a recomponerse a Shurik cuidadosamente mientras permanecían en la luz."


translate spanish sl_mine_exit_80764f4c:


    sl "Está bien, soy yo, Slavya."


translate spanish sl_mine_exit_af188dfb:


    sh "¿La de verdad?"


translate spanish sl_mine_exit_be9d5df3:


    "Shurik la miró e inmediatamente empezó a sollozar silenciosamente."


translate spanish sl_mine_exit_5e2b81c3:


    sl "¡Por supuesto! ¿Quién si no podría ser? Y ese es Semyon que está conmigo. Vinimos a buscarte. ¡Todo irá bien ahora!"


translate spanish sl_mine_exit_0be700a9:


    sh "¿De verdad que no sois {i}ellas{/i}?"


translate spanish sl_mine_exit_553577fe:


    sl "¡De verdad que no! Somos nosotros."


translate spanish sl_mine_exit_3002543e:


    "Él trató de levantarse; pero estaba aparentemente desorientado y hubiera caído de no ser por Slavya, quien le agarró por el brazo."


translate spanish sl_mine_exit_dbdfe385:


    sl "¡Allá vamos! ¡Con cuidado!"


translate spanish sl_mine_exit_740d40c9:


    "Cuando llegaron hasta mi, dije:"


translate spanish sl_mine_exit_a47d7467:


    me "Está bien, ¡es la hora de largarnos de aquí!"


translate spanish sl_mine_exit_05d7a2e2:


    sh "No podemos volver hacia atrás..."


translate spanish sl_mine_exit_52f884ab:


    me "¿Volver por dónde? ¿Por la mina?"


translate spanish sl_mine_exit_2ba450b2:


    sh "Sí."


translate spanish sl_mine_exit_24404a66:


    "Shurik estaba hablando tranquilamente, pero a la vez su voz estaba temblando ligeramente."


translate spanish sl_mine_exit_5ac38cf1:


    me "¿Y eso por qué?"


translate spanish sl_mine_exit_798ba618:


    sh "Es donde... ¡{i}ellas{/i} están!"


translate spanish sl_mine_exit_f1b8c285:


    me "¿Quiénes son {i}ellas{/i}?"


translate spanish sl_mine_exit_83aa975e:


    sh "Las voces."


translate spanish sl_mine_exit_777da593:


    me "¿Qué voces?"


translate spanish sl_mine_exit_25a17e79:


    "Comenzaba a perder mi paciencia."


translate spanish sl_mine_exit_5d4ebca9:


    "Este lugar y su situación eran ciertamente normales, pero si entonces había alguna cosa sobrenatural aquí, según las leyes del cine de miedo, hace rato que ya debería haberse mostrado su rostro."


translate spanish sl_mine_exit_15266f71:


    sl "Tranquilízate, Shurik. Tan sólo explícanoslo con todo detalle."


translate spanish sl_mine_exit_aef80036:


    "Respiró profundamente un poco y empezó:"


translate spanish sl_mine_exit_25825ae0:


    sh "Necesitaba algunas partes de recambio para el robot, y escuché que aquí había un refugio antiaéreo en el antiguo campamento. Y normalmente suele haber mucho equipo variado en los refugios antiaéreos. Es viejo, por supuesto, pero vale la pena desmontarlo: siempre puedes encontrar bobinas, resistencias electrónicas..."


translate spanish sl_mine_exit_3b6c99a8:


    sh "Así que fui pronto por la mañana, pensé que podría terminar rápidamente y volver para el desayuno."


translate spanish sl_mine_exit_579e2e33:


    sh "Conseguí todo lo que necesitaba."


translate spanish sl_mine_exit_c03bb0fe:


    "Sacó algunas bombillas y cables de sus bolsillos y nos los mostró."


translate spanish sl_mine_exit_9d133ba2:


    sh "Pero entonces, allí arriba en el refugio antiaéreo, escuché algunos ruidos tras la puerta. Algo como gemidos o lamentaciones. Al principio me asusté, pero entonces pensé que una persona podría estar en peligro. Así que fui allí..."


translate spanish sl_mine_exit_9ec05142:


    sh "Y aquí estaba esta mina, ¡este maldito laberinto! Me perdí completamente. Y las voces, las voces siempre diciéndome alguna cosa, a veces gritando, a veces susurrando, ¡y sin comprender sus palabras!"


translate spanish sl_mine_exit_b022d1a8:


    "Él estaba a punto de romper a llorar, pero Slavya le pasó la palma suavemente por su cabeza, y Shurik continuó:"


translate spanish sl_mine_exit_e48ebc1f:


    sh "De todas maneras, encontré esta habitación, al menos aquí no puedes escucharlas a {i}ellas{/i}..."


translate spanish sl_mine_exit_1eeae244:


    me "Por lo que decidiste sentarte y esperar a que te rescataran, ¿como en una película de serie «B»?"


translate spanish sl_mine_exit_0f9dcec3:


    "Pregunté sarcásticamente."


translate spanish sl_mine_exit_a783d9b3:


    "Slavya me lanzó una mirada de enfado."


translate spanish sl_mine_exit_d5aa4a23:


    me "Bueno, en cualquier caso, ¡es la hora de salir! Si no hay objeciones."


translate spanish sl_mine_exit_64d006fa:


    sh "Venid, ¡conozco un atajo!"


translate spanish sl_mine_exit_1f5418a3:


    me "¿Un atajo?"


translate spanish sl_mine_exit_d3c3faf7:


    sh "Claro, ya sabes, caminé a lo largo y ancho de este lugar."


translate spanish sl_mine_exit_5fabfeff:


    me "Está bien..."


translate spanish sl_mine_exit_a20cefa7:


    "..."


translate spanish sl_mine_exit_31a38a29:


    "Shurik no mintió."


translate spanish sl_mine_exit_8fb267ae:


    "Así fue, tras vagar tan solamente un par de minutos por la mina, nos hallamos bajo una rejilla, por la cual podíamos ver el cielo."


translate spanish sl_mine_exit_401125ca:


    me "¿Entonces por qué no saliste por ti mismo?"


translate spanish sl_mine_exit_9fd952a9:


    sh "Lo intenté, ya verás."


translate spanish sl_mine_exit_34b807b5:


    "Me alcé y empujé contra la rejilla unas cuantas veces."


translate spanish sl_mine_exit_59e04ffd:


    th "Es verdad, no puedes romperlo tal cual.{w} Hay que usar algo pesado..."


translate spanish sl_mine_exit_e1eb6421:


    "Miré hacia abajo a Slavya y Shurik."


translate spanish sl_mine_exit_ea249dfd:


    sl "Inténtalo con la linterna, ¿quizás?"


translate spanish sl_mine_exit_af0c14fb:


    me "La linterna..."


translate spanish sl_mine_exit_b7873ad2:


    "Hurgué con ella: no era una de esas copias baratas chinas de la década de los noventa, de las cuales solía tener muchas en nuestro refugio de campo de verano; no, era una auténtica y resistente linterna de metal soviética.{w} No un martillo, claro, pero si lo intentaba con esfuerzo..."


translate spanish sl_mine_exit_68320b1d:


    "Me incliné y empecé a golpear la rejilla, apuntando a los puntos a los cuales se encontraba apuntalada en el muro."


translate spanish sl_mine_exit_67cc1ddd:


    "Pronto tuve suerte: escuché como se quebraba y algunos tornillos cayeron."


translate spanish sl_mine_exit_ce617998:


    "..."


translate spanish sl_mine_exit_fb1e61f2:


    "Un minuto más tarde, estaba tumbado en la hierba bajo el monumento y respirando el aire fresco de la noche con placer.{w} Aunque tras la mina, cualquier aire sería fresco."


translate spanish sl_mine_exit_b4b29936:


    sh "Pues bueno, me voy, gracias..."


translate spanish sl_mine_exit_3598f2d1:


    "Shurik parecía un poco perdido."


translate spanish sl_mine_exit_52f39431:


    "Le eché un vistazo inquisitivamente durante un rato, pero acabé por no decirle nada y simplemente me despedí con la mano para decirle adiós."


translate spanish sl_mine_exit_da4e05bd:


    "Después de todo, no tenía tampoco la fuerza ni la voluntad para hablar con él."


translate spanish sl_mine_exit_e0453692:


    th "¿Y qué le diría a él?{w} Regañarle ahora era inútil, y lo mismo también si tratara de darle una lección."


translate spanish sl_mine_exit_0df46cc3:


    sl "Vaya noche..."


translate spanish sl_mine_exit_07fd16f1:


    "Slavya estaba sentada junto a mí y miraba soñadora la luna."


translate spanish sl_mine_exit_068c4c7d:


    me "Si... Si me llegan a decir hace una semana atrás que iría a un campamento de pioneros y que incluso tendría que trepar atravesando refugios antiaéreos..."


translate spanish sl_mine_exit_a2b22e6d:


    sl "Pero fue divertido."


translate spanish sl_mine_exit_c15d505f:


    me "De alguna manera."


translate spanish sl_mine_exit_5e8427f4:


    "Había por supuesto algo agradable en todo esto.{w} Pero también había algo extraño: durante las últimas horas, nada en absoluto me podia haber ocurrido, pero al final, nada de nada había sucedido."


translate spanish sl_mine_exit_1ca3789d:


    "En un par de años, probablemente recordaría esto como sencillamente un divertido incidente de la infancia.{w} Si es que llego a vivir más."


translate spanish sl_mine_exit_fa7152d2:


    me "Qué crees tú, ¿realmente escuchó algo allí?"


translate spanish sl_mine_exit_9fee39ea:


    sl "No lo sé... Es posible, claro, pero lo más probable es que fuera tan sólo su imaginación. En situaciones semejantes, cualquiera podría imaginarse cosas."


translate spanish sl_mine_exit_b35d647f:


    "Ésa no era solamente la explicación más razonable, sino que también era la más probable, porque nada me había ocurrido a mí ni a Slavya."


translate spanish sl_mine_exit_defa6d12:


    me "Bueno, hay una cosa que creo y es que debería ir a ver a un psiquiatra."


translate spanish sl_mine_exit_e8c8afba:


    sl "¡Supongo que debería!"


translate spanish sl_mine_exit_c954d69f:


    "Se rio Slavya."


translate spanish sl_mine_exit_e086255d:


    sl "Está bien, hora de ir a la cama."


translate spanish sl_mine_exit_8613242c:


    me "Así es."


translate spanish sl_mine_exit_65810382:


    "Sentí como si necesitara decirle algo a Slavya.{w} No estaba seguro de qué, pero podía sentir la necesidad."


translate spanish sl_mine_exit_8753f434:


    me "¡Gracias!"


translate spanish sl_mine_exit_306a3019:


    sl "¿Por qué?"


translate spanish sl_mine_exit_d222fc60:


    "Me hizo una mirada de sorpresa."


translate spanish sl_mine_exit_115b4f29:


    me "No me las habría arreglado sin ti."


translate spanish sl_mine_exit_22d72bcc:


    sl "No fue nada..."


translate spanish sl_mine_exit_1c771384:


    "Slavya se ruborizó."


translate spanish sl_mine_exit_15a32661:


    th "Estaba en deuda.{w} Otra vez."


translate spanish sl_mine_exit_2a83f273:


    th "Me pregunto cómo podría devolvérselo, al menos parcialmente, por todo lo que ella ha hecho por mí."


translate spanish sl_mine_exit_aa269f37:


    "Miré fijamente el cielo nocturno, lentamente me quedé dormido."


translate spanish sl_mine_exit_d50a711e:


    "Todas las aventuras de hoy me dejaron tan agotado que no tenía más fuerzas."


translate spanish sl_mine_exit_7ecbe987:


    me "Sabes, bajo diferentes circunstancias, seguramente te habría confesado mi amor..."


translate spanish sl_mine_exit_c537a406:


    "Dije, perdiendo el contacto con la realidad."


translate spanish sl_mine_exit_0c6574db:


    "En ocasiones, los pensamientos son más rápidos que el sentido común..."


translate spanish sl_mine_exit_7d9eccb6:


    "Pero Slavya ya no estaba allí."


translate spanish sl_mine_exit_2e4b7881:


    th "Era probablemente lo mejor... al menos ella no me escuchó..."


translate spanish sl_mine_exit_eeffa084:


    th "Aunque era extraño... ¿por qué se fue sin decirme adiós?"


translate spanish sl_mine_exit_52ccd478:


    th "Bueno, da igual, me voy a dormir y ya veré mañana."


translate spanish sl_mine_exit_c4cf6525:


    "Estaba tan cansado que apenas pude llegar a la cabaña de la líder del campamento...{w} ¡Y allí había una luz en la ventana!"


translate spanish sl_mine_exit_b9666e2a:


    th "Eso significa que me está esperando."


translate spanish sl_mine_exit_750b01bf:


    th "Y eso, a su vez, significa que voy a ser sermoneado otra vez."


translate spanish sl_mine_exit_f6601815:


    th "¡Y esta vez será tan absurda como otras veces!"


translate spanish sl_mine_exit_d7d24cf6:


    "No tenía ni el más mínimo deseo de discutir tanto si tenía razón como si no."


translate spanish sl_mine_exit_12c908c6:


    "La tumbona que había cerca me atraía como un amigo, como si me sugiriera que debería dormir en ella, olvidando la mina, a Shurik, la líder del campamento..."


translate spanish sl_mine_exit_19f0e8ea:


    me "Ahí estás..."


translate spanish sl_mine_exit_8a478121:


    "Dije inexpresivo, mirando aun a mi querida tumbona."


translate spanish sl_mine_exit_105fc865:


    mt "¡Semyon!"


translate spanish sl_mine_exit_4b47445a:


    me "Claro, claro...{w} Hallamos a Shurik, Slavya te dará los detalles mañana, y ahora realmente preferiría dormir..."


translate spanish sl_mine_exit_014b5f98:


    "Me miró confundida por un instante y entonces me respondió:"


translate spanish sl_mine_exit_7e1d09c7:


    mt "Oh bueno, perfecto..."


translate spanish sl_mine_exit_2ab3da5a:


    "No sé si el mencionar a Slavya jugó algún papel importante, pero en medio minuto más tarde ya estaba en mi cama."


translate spanish sl_mine_exit_c849f321:


    "Los sucesos del día continuaban estando latentes por breves momentos en mi cabeza, pero pronto empezaron a ser molestos, como si mi consciencia tratara de ponerse a salvo y empezara a reiniciarse."


translate spanish day4_dv_55e8aea3:


    mt "¡Y Dvachevskaya va contigo!"


translate spanish day4_dv_a7bd7fb0:


    "Era como si Olga Dmitrievna me hubiera leído la mente."


translate spanish day4_dv_34113b9d:


    dv "¿Y eso por qué?"


translate spanish day4_dv_9dcfa902:


    mt "Bueno, ¿quién trató de destruir el monumento?{w} Y por si no fuera suficiente, ¡¿quién trató de calumniar a Semyon?!"


translate spanish day4_dv_f1c01595:


    "Lo dijo de tal forma que Alisa no se atrevió a discutir."


translate spanish day4_dv_ed1378b5:


    dv "¿Contento ahora?"


translate spanish day4_dv_8efa4588:


    "Me espetó una vez estuvimos solos."


translate spanish day4_dv_49e69f9d:


    me "¿Qué se supone que debo hacer con esto?{w} ¿Era acaso yo quien trataba de destruir el monumento?"


translate spanish day4_dv_f505265f:


    "Alisa gruñó y se apartó."


translate spanish day4_dv_90b40594:


    dv "¡No voy a ir ninguna parte contigo!"


translate spanish day4_dv_2cbb8397:


    me "¡Oh, por favor no lo hagas!"


translate spanish day4_dv_b5004dfa:


    dv "¡Genial pues!"


translate spanish day4_dv_d1bfd0cf:


    me "¡Excelente!"


translate spanish day4_dv_9001d819:


    "Nos quedamos callados por un rato, sin mirarnos el uno al otro."


translate spanish day4_dv_48e0b151:


    "Me atreví a romper el silencio primero."


translate spanish day4_dv_7b7a2ac8:


    me "¿Eso es todo?"


translate spanish day4_dv_7e503b02:


    dv "¿Todo qué?"


translate spanish day4_dv_43dded4a:


    me "Todo lo que me querías decir."


translate spanish day4_dv_55cf29e5:


    dv "¡Sí, eso es todo!"


translate spanish day4_dv_976566c1:


    me "¿Te importa entonces si te pregunto por qué trataste de destruir el monumento?"


translate spanish day4_dv_288c13a3:


    dv "Estaba aburrida..."


translate spanish day4_dv_f39f8aa5:


    th "Ah, claro, ¡hacerlo estallar por los aires te habría entretenido claramente!"


translate spanish day4_dv_5628d51f:


    "Alisa parecía realmente reacia a ir conmigo."


translate spanish day4_dv_8d7dd406:


    "En cuanto a mi, de todas maneras, he decidido ir a buscar a Shurik después de todo."


translate spanish day4_dv_9a84d633:


    "No era él lo que más me interesara, sino el campamento abandonado."


translate spanish day4_dv_e1bb0ffe:


    "Por alguna razón, tenía una sensación de que podría hallar algo allí."


translate spanish day4_dv_19accf37:


    "Y realmente no me apasionaba la idea de ir solo por la noche..."


translate spanish day4_dv_8d8791e8:


    me "Así que... ¿estás asustada pues?"


translate spanish day4_dv_2ff41508:


    dv "¿Me estás vacilando?"


translate spanish day4_dv_7f6b716b:


    "Ella me hizo una sonrisa sarcástica."


translate spanish day4_dv_f9e95578:


    me "Oh no, yo nunca...{w} ¡Pues buena suerte si te quedas aquí!"


translate spanish day4_dv_2b7ea55e:


    "Me di la vuelta y comencé a caminar en la dirección donde debería estar el antiguo campamento."


translate spanish day4_dv_4ae306eb:


    "Alisa me atrapó al borde del bosque."


translate spanish day4_dv_33be1cc8:


    dv "¡Espérate!{w} ¡Voy a ir contigo!"


translate spanish day4_dv_708a9465:


    me "¿Por qué te rebajas a cambiar de opinión tan de repente?"


translate spanish day4_dv_27c62366:


    "Pregunté irónicamente."


translate spanish day4_dv_aa4bc8f9:


    dv "¡No necesitas saberlo!"


translate spanish day4_dv_eda552ee:


    "Gruñó y me ofreció una linterna."


translate spanish day4_dv_30b5f8c4:


    "Eso era bastante sensato."


translate spanish day4_dv_56fc6798:


    "No sé qué habría hecho allí, solo en la oscuridad, si no fuera por Alisa."


translate spanish day4_dv_7bd87323:


    "Por lo que Electronik nos explicó, el viejo edificio fue construido justo tras la guerra."


translate spanish day4_dv_fd3c1cdc:


    "Se parecía a un jardín de parvulitos (o como unos barracones, supuse) y claramente podía alojar menos pioneros que el actual campamento."


translate spanish day4_dv_c4f7341a:


    "Había sido abandonado hacía ya unos veinte años..."


translate spanish day4_dv_456886a9:


    "El bosque nocturno era completamente distinto durante el día."


translate spanish day4_dv_5d66727e:


    "Hace tan solamente un par de horas atrás era justo como un pequeño bosquecillo donde incluso un ciego no podía perderse."


translate spanish day4_dv_e7ca9c28:


    "Sin embargo, en cuanto la oscuridad se cernía los árboles parecían crecer, los arbustos de repente ocupaban los huecos y, el sendero, tan ancho durante el día, se tornaba en apenas unas visibles olas en el verde oscuro océano boscoso."


translate spanish day4_dv_30605aad:


    "Las últimas pinceladas en este cuadro eran los pájaros, los insectos y las bestias, los cuales daban un concierto, cada uno con su propio instrumento, creando juntos una simfonía nocturna terrorífica y aun así asombrosa."


translate spanish day4_dv_a9effafd:


    "Paseamos lentamente, Alisa un poco más adelante respecto a mi que le seguía sus pasos, tratando de no quedar muy atrás."


translate spanish day4_dv_52d4466a:


    "Actuaba segura de sí misma.{w} Exactamente no como yo... cualquier crujido de los arbustos cercanos o el ulular de una lechuza sobre mi cabeza me hacía encojer y mirar alrededor tímidamente."


translate spanish day4_dv_97608943:


    "Por supuesto, era obvio que no había nada que temer aquí... los bosques locales no eran como para estar pululados de bestias salvajes, y parecía que no había otra gente en este mundo, a parte de aquellos en el campamento."


translate spanish day4_dv_0c72d684:


    "Aun así, el miedo estaba ahí en un nivel instintivo, y traté de caminar con cuidado y estar tan atento como pudiera estar.{w} La discreción es la mejor parte del valor a fin de cuentas."


translate spanish day4_dv_ee008bda:


    "Alisa se detuvo de repente.{w} Tan repentinamente que casi me tropiezo con ella."


translate spanish day4_dv_15f32c97:


    dv "¿Y ahora dónde?"


translate spanish day4_dv_47e7fadb:


    me "¿Qué? Bueno... Recto hacia delante, supongo."


translate spanish day4_dv_7b471372:


    dv "¿Estás seguro?"


translate spanish day4_dv_558379f9:


    "Traté de imaginarme la dirección."


translate spanish day4_dv_1a602959:


    "Parecía que íbamos en la correcta dirección.{w} O al menos en la senda sugerida por Electronik."


translate spanish day4_dv_2a16d041:


    "Sin embargo sin mapa, sin un navegador GPS o un GLONASS, no estaba tampoco seguro de estar incluso en el mismo planeta (o donde fuera)."


translate spanish day4_dv_4dde0bc7:


    me "No hemos tomado ni una esquina, hemos estado caminando recto todo el rato, tal como Electronik dijo..."


translate spanish day4_dv_a44acfbb:


    dv "Entonces, ¿por qué no hemos llegado todavía al antiguo campamento?"


translate spanish day4_dv_cd895649:


    me "¡¿Cómo voy a saberlo?!"


translate spanish day4_dv_492a8d61:


    "La forma grosera de Alisa para hablar me ponía de los nervios."


translate spanish day4_dv_cebebf7d:


    dv "¿Ya nos hemos perdido?"


translate spanish day4_dv_eb6a6442:


    me "¿Por qué dices eso? Apenas estamos a un kilómetro de distancia del campamento."


translate spanish day4_dv_b66acd62:


    dv "¿Pues entonces?"


translate spanish day4_dv_fbb515c6:


    "Se le veía visiblemente más triste."


translate spanish day4_dv_023d75be:


    me "¡Pues nada! ¡Si no te gusta, puedes volver!"


translate spanish day4_dv_aeb753f4:


    "Alisa me miró decepcionada por un momento y luego su rostro cambió bruscamente."


translate spanish day4_dv_4a85e25e:


    dv "¡Pues me voy entonces!"


translate spanish day4_dv_4fdd6288:


    "Se giró de repente y comenzó a irse a paso ligero en dirección al campamento."


translate spanish day4_dv_cacdf009:


    me "¡Que tengas buena suerte!"


translate spanish day4_dv_3613cae1:


    "Le grité mientras se iba."


translate spanish day4_dv_39966305:


    th "Sin embargo, Alisa puede que tenga razón y estuviéramos de hecho perdidos.{w} En cuyo caso no era buena idea permanecer solo aquí..."


translate spanish day4_dv_1f17e8a8:


    "La fría noche soplaba fuerte y temblé."


translate spanish day4_dv_3fe492ee:


    th "Pero ir detrás de ella significaría ser un perdedor; incluso peor, admitir que tenía razón.{w} ¡No, no estoy preparado para aceptar eso!"


translate spanish day4_dv_a158a040:


    "Podría haberme pasado probablemente todo el rato discutiendo conmigo mismo sobre esto, de no ser por el chillido de Alisa."


translate spanish day4_dv_5b0d6420:


    "Mis piernas me llevaron hacia la dirección en la que se fue."


translate spanish day4_dv_4739db18:


    "Casi veinte metros más allá me caí por un agujero, arreglándomelas justo a tiempo para saltar en el último momento."


translate spanish day4_dv_41315982:


    "En cuanto a Alisa, parecía que no tuvo la misma suerte."


translate spanish day4_dv_172dcb0f:


    "Estaba a punto de iluminar la oscuridad con la linterna cuando me di cuenta que ya se la había llevado ella.{w} Bueno, eso era de esperar."


translate spanish day4_dv_c22eb442:


    me "¿Estás bien?"


translate spanish day4_dv_3b4b97c0:


    dv "Claro..."


translate spanish day4_dv_20abb91b:


    "Su voz ahogada se escuchó desde abajo."


translate spanish day4_dv_f3139a0d:


    me "¿Te rompiste algo?"


translate spanish day4_dv_ce4becd7:


    dv "¡Demonios que sé yo! ¡Idiota!"


translate spanish day4_dv_44682451:


    "Una luz parpadeó en el agujero. Me acerqué y me agaché para examinar cómo de profundo era."


translate spanish day4_dv_dd73bb1b:


    "El derruido agujero tenía unos tres metros de profundidad, pero había una oscuridad total abajo."


translate spanish day4_dv_5d83ec49:


    dv "¡Ayúdame a salir!"


translate spanish day4_dv_a342db42:


    "Alisa gritó enfadada."


translate spanish day4_dv_ea49f8ff:


    me "¿Y cómo crees que debería hacer eso?"


translate spanish day4_dv_6be52e6b:


    dv "¡No lo sé! ¡Piensa en algo!"


translate spanish day4_dv_71d84df3:


    me "Bueno, ¿qué ves ahora? Descríbelo."


translate spanish day4_dv_af7113a7:


    dv "Bueno, es algún tipo de túnel y... Qué va, solamente un túnel."


translate spanish day4_dv_b949157e:


    "¿Un túnel? Eso es raro."


translate spanish day4_dv_c9a2c926:


    dv "¡Venga ya, baja aquí!"


translate spanish day4_dv_285fa6b8:


    me "¿Es la mejor idea que tienes? Mejor salgo corriendo al campamento a por una cuerda."


translate spanish day4_dv_7cc30c48:


    dv "¿Me vas a dejar sola aquí?"


translate spanish day4_dv_8f7c4481:


    "La voz de Alisa perdió toda su arrogancia."


translate spanish day4_dv_fab07924:


    me "Pero estás sola ahí abajo de todas formas..."


translate spanish day4_dv_93b5fc50:


    dv "¡Ey!"


translate spanish day4_dv_e2d0858a:


    "Chilló fuertemente y dirigió la linterna justo a mis ojos."


translate spanish day4_dv_0f7a1181:


    me "Bueno, vale no te asustes, los trolls no te van a comer aquí, ¡volveré con una linterna!"


translate spanish day4_dv_66878d88:


    "A Alisa le dio un ataque y empezó a gritar ella sola ahí abajo pero no podía entender sus palabras."


translate spanish day4_dv_7514d7d5:


    "Sin embargo, solamente pude dar unos pasos antes de que el suelo bajo mis pies cediera, y antes de saber que me estaba cayendo abajo."


translate spanish day4_dv_85879201:


    "Agarré una estrecha raíz que de alguna manera me ayudó a descender lentamente hacia abajo..."


translate spanish day4_dv_de9de6d3:


    "...pero se rompió a causa de mi peso y se fue volando hacia abajo conmigo."


translate spanish day4_dv_ce617998:


    "..."


translate spanish day4_dv_006481b5:


    dv "¿Estás bien?"


translate spanish day4_dv_63fdaa9f:


    "Apenas abrí un ojo que ya tenía sobre mi un foco de luz."


translate spanish day4_dv_aababdd9:


    me "Arghh, apártala..."


translate spanish day4_dv_bdc0c338:


    "Alisa estaba en cluquillas a mi lado y mirándome de alguna manera nerviosa."


translate spanish day4_dv_4ddc9f96:


    dv "¿Algún miembro roto?"


translate spanish day4_dv_7479b07e:


    me "No lo sé..."


translate spanish day4_dv_322546b6:


    "Moví mis manos y pies... todo parecía estar bien."


translate spanish day4_dv_217dc2c0:


    "Dada la altura, ¡tenía mucha suerte de caer bien!"


translate spanish day4_dv_f32b91db:


    "Apenas me las arreglé para ponerme en pie y mirar el lugar del accidente."


translate spanish day4_dv_e2ce46b6:


    "Afirmativamente, estábamos en algún tipo de largo pasillo. Había algún tipo de pesado cableado recorriendo las paredes y bombillas revestidas de metal pesado en apliques sobre el techo."


translate spanish day4_dv_88cece4e:


    "En cualquiera otra situación, habría supuesto que aquello era un túnel de servicio de metro, pero era difícil que Sovyonok tuviera semejante nivel de infraestructura..."


translate spanish day4_dv_d359bcda:


    dv "¿Dónde estamos?"


translate spanish day4_dv_c45d86b6:


    me "No lo sé, ¡pero tenemos que salir de aquí!"


translate spanish day4_dv_45db0ef2:


    "Trepar por la pared derruida era una opción bastante difícil. De hecho, me preguntaba cómo habíamos sobrevivido a una caída con semejante altura."


translate spanish day4_dv_638df9b4:


    me "Bueno, parece que tenemos que encontrar una salida en alguna parte."


translate spanish day4_dv_c56fe6de:


    dv "¿Cómo haremos eso?"


translate spanish day4_dv_9d41ebbb:


    "Alisa preguntó, volviéndose pálida lentamente."


translate spanish day4_dv_7507cfac:


    "Ni un solo rastro de su arrogancia habitual quedaba ya en su cara."


translate spanish day4_dv_333f2018:


    me "Bueno, supongo... Tiene que haber alguna salida por aquí, ¿verdad?"


translate spanish day4_dv_91700607:


    dv "Probablemente, ¿pero hacia qué dirección tenemos que ir?"


translate spanish day4_dv_c59d4b63:


    th "Esa era una buena pregunta."


translate spanish day4_dv_7c3c6bab:


    "A primera vista, parecía una buena idea volver hacia el campamento (al menos en la dirección que creo que era), pero no habia visto nada que se pareciera a una salida desde las catacumbas.{w} Y eso significaba que deberíamos intentar mejor la dirección opuesta."


translate spanish day4_dv_8bb4bd4e:


    me "¡Dame eso!"


translate spanish day4_dv_f3a15952:


    "Le quité a Alisa de las manos la linterna e iluminé con su luz en la oscuridad."


translate spanish day4_dv_c31cbf17:


    me "¡Por ahí!"


translate spanish day4_dv_ce617998_1:


    "..."


translate spanish day4_dv_7fb53b2d:


    "Estábamos avanzando lentamente, con Alisa sujetándose a mí firmemente con su mano."


translate spanish day4_dv_bef06bb8:


    "En otras circunstancias podría haber sido sorprendido, avergonzado, o incluso regocijarme (¿cómo podría saberlo?) pero ahora mismo solamente estaba ansioso por salir de aquí cuanto antes."


translate spanish day4_dv_8fa117b1:


    "Mis moratones de la caída me dolían. La sangre bombeaba en mi cabeza como un pesado péndulo. Incluso el parpadeante foco de luz que seguía iluminando continuamente las mismas paredes de cemento, no estaba exactamente aumentando nuestra moral."


translate spanish day4_dv_e7613964:


    me "Maldita sea y maldito Shurik..."


translate spanish day4_dv_e0ebb201:


    dv "¿Qué?"


translate spanish day4_dv_acbe50d2:


    me "¿Por qué fuimos al bosque de noche en primer lugar? ¡Para hallar a Shurik!"


translate spanish day4_dv_f174d829:


    dv "Oh, tan sólo olvídale."


translate spanish day4_dv_622bba83:


    me "Quizás él cayó justo como nosotros."


translate spanish day4_dv_3d5f8ddf:


    "Desde luego, Alisa tenía razón... este no era el mejor momento para comprometerse en una misión de rescate. Si no somos capaces de salir, habrá una misión de rescate para nosotros en su lugar."


translate spanish day4_dv_b6c57128:


    "Pero no podía apartar de mi mente este pensamiento."


translate spanish day4_dv_fb54620a:


    "Era como si Shurik estuviera justo frente a mi, haciéndome una mirada de reproche."


translate spanish day4_dv_b0dd778f:


    dv "¡Mira!"


translate spanish day4_dv_9d770d3f:


    "Alisa chilló."


translate spanish day4_dv_79b85d9a:


    "Casi me golpeo contra una inmensa puerta de metal..."


translate spanish day4_dv_d0842bb5:


    "Una señal de peligro biológico llamó mi atención."


translate spanish day4_dv_a9087c69:


    me "Esto significa que estamos en un refugio antiaéreo..."


translate spanish day4_dv_c11bed91:


    "Murmuré en voz baja."


translate spanish day4_dv_aa5fd937:


    dv "Claro, he escuchado alguna cosa de eso..."


translate spanish day4_dv_017f286b:


    me "¿De verdad? ¿Por qué no lo mencionaste antes?"


translate spanish day4_dv_10384b59:


    dv "¿Cómo iba a saber que sería importante?"


translate spanish day4_dv_7ecde56e:


    me "Supongo que tienes razón... Está bien."


translate spanish day4_dv_7db5329b:


    "Agarré el pomo giratorio y lo empujé con todas mis fuerzas como pude, tratando de girarlo."


translate spanish day4_dv_80cc6fd2:


    "El metal oxidado chirrió pero refusó desplazarse."


translate spanish day4_dv_1f8abc27:


    me "Deja de hacer la vaga, ¡ayúdame!"


translate spanish day4_dv_0398ce56:


    "Alisa dudó un poco pero entonces agarró el pomo giratorio y empezó a empujar."


translate spanish day4_dv_6ec0b8eb:


    "Finalmente logramos girar el pomo y abrimos la puerta."


translate spanish day4_dv_47dd4261:


    "Era difícil confundir erróneamente la habitación a la que entramos por otra cosa. Era en definitiva un refugio antiaéreo."


translate spanish day4_dv_75eef015:


    "Armarios llenos de máscaras de gas y raciones de conservas, muchos dispositivos, literas y sistemas de circulación de aire... había todo lo que alguien necesitaría para sobrevivir a una guerra nuclear.{w} Bueno, al menos para sobrevivir a la primera explosión."


translate spanish day4_dv_c5fd3f99:


    "Alisa se aferró aun más fuerte a mi brazo."


translate spanish day4_dv_ad02f53a:


    me "¿Qué es esto?"


translate spanish day4_dv_35f94c91:


    dv "¿Qué de «qué»...?"


translate spanish day4_dv_04f9ace4:


    "Parloteó en voz baja."


translate spanish day4_dv_a1e11f2c:


    dv "Estoy asustada..."


translate spanish day4_dv_958e50a5:


    me "¿De qué estás asustada?"


translate spanish day4_dv_be3d8921:


    th "Exacto. ¿De qué?"


translate spanish day4_dv_ad914bab:


    th "Hemos ido a parar a un refugio antiaéreo, ahora estamos vagando aquí en la oscuridad, tratando de hallar una salida...{w} Oh, claro, no hay nada de qué preocuparse."


translate spanish day4_dv_1664c72b:


    "Aquellos pensamientos me hicieron estremecerme e instintivamente me aproximé más a Alisa."


translate spanish day4_dv_9f11ce03:


    "Ella no dijo nada, solamente se ruborizó un poco y me dio la espalda."


translate spanish day4_dv_e789012e:


    me "Bueno, en cualquier caso..."


translate spanish day4_dv_e0ebb201_1:


    dv "¿Qué?"


translate spanish day4_dv_c7ebc638:


    me "Tenemos que seguir buscando una salida."


translate spanish day4_dv_3614f1a5:


    dv "Bueno, hay otra puerta."


translate spanish day4_dv_ded6f407:


    "Así es, había otra puerta en la pared más alejada, igual que la otra que habíamos cruzado."


translate spanish day4_dv_1695119f:


    "Sin embargo, no importa con cuanta fuerza traté de abrirla, estaba bloqueda."


translate spanish day4_dv_5961a3eb:


    dv "¿Necesitas ayuda?"


translate spanish day4_dv_96fbd823:


    me "Nop, está completamente atascada..."


translate spanish day4_dv_7b6b83d1:


    "Eché un vistazo alrededor de la habitación, tratando de encontrar alguna cosa que sirviera como una palanca y me percaté de un pasamanos en el lejano rincón."


translate spanish day4_dv_a499094f:


    me "¡Genial!"


translate spanish day4_dv_223ad1b0:


    "Alisa me hizo una mirada fija inexpresiva."


translate spanish day4_dv_70066e56:


    me "¡Cuidado!"


translate spanish day4_dv_b614218a:


    "Sin embargo, aun así no pude mover ni un centímetro el pomo giratorio a pesar de usar el pasamanos."


translate spanish day4_dv_152a52ab:


    "Totalmente exhausto, me desplomé en una litera hecha con cuidado y solté un suspiro."


translate spanish day4_dv_32250ada:


    me "Tenemos que volver atrás."


translate spanish day4_dv_11b295cf:


    dv "¡Pero si nosotros vinimos de allí precisamente!"


translate spanish day4_dv_b3c4b2e1:


    "Alisa parecía recobrar un poco de su autocontrol, así como también su habitual arrogancia."


translate spanish day4_dv_f527e7c1:


    me "Bueno, te puedes quedar aquí si quieres... hay agua, raciones de conservas, probablemente la radio todavía funcione..."


translate spanish day4_dv_f67a7d59:


    "Me miró con despecho pero no me dijo nada."


translate spanish day4_dv_0c0902a5:


    "Todo mi cuerpo me dolía terriblemente."


translate spanish day4_dv_0d661aed:


    "Era ahora cuando me estaba relajando cuando empecé a darme cuenta de cuanto me dolía."


translate spanish day4_dv_0e1194d0:


    "Una herida profunda en mi pierna estaba sangrando abundantemente, mis manos estaban bastante magulladas, la sangre se apelmazaba en mi cabello."


translate spanish day4_dv_d39992ac:


    me "Estás tú... ¿bien?"


translate spanish day4_dv_4b680097:


    "Le pregunté a Alisa, tanto si fue sólo por cortesía o porque realmente estaba preocupado por ella."


translate spanish day4_dv_aa62d7ce:


    "Le llevó algo de tiempo contestarme."


translate spanish day4_dv_413f8d3c:


    dv "¿Cómo podemos estar bien en una situación como esta?"


translate spanish day4_dv_c71c3992:


    me "Bueno, quiero decir..."


translate spanish day4_dv_7c28149f:


    "Señalé la herida de mi pierna."


translate spanish day4_dv_8298da02:


    dv "¡Oh!"


translate spanish day4_dv_16d734ec:


    "Chilló Alisa."


translate spanish day4_dv_3bf9faff:


    dv "¡Tenemos que desinfectarla urgentemente!"


translate spanish day4_dv_bee1cd2f:


    me "No es necesario. Sobreviviré."


translate spanish day4_dv_3044c877:


    dv "¡Oh, para ya! ¡Tenemos que hacerlo o corres el riesgo de que se te infecte y entonces te amputarán la pierna! Mi abuelo perdió su pierna en la guerra de esa manera."


translate spanish day4_dv_83a7a438:


    "Alisa parecía estar verdaderamente preocupada así que opté por no discutir."


translate spanish day4_dv_21b14d25:


    me "Vale, pero hay un buen trozo de aquí a la enfermería."


translate spanish day4_dv_6c7839f5:


    dv "Aguanta."


translate spanish day4_dv_f08e28b7:


    "Sonrió y empezó a rebuscar entre los armarios."


translate spanish day4_dv_d617f7d1:


    "Había una alta probabilidad de que encontrara algún tipo de medicina en el refugio antiaéreo, y bien pronto Alisa sacó un equipo de primeros auxilios con una mirada triunfante."


translate spanish day4_dv_68a3327c:


    dv "Vale, aguanta un poco más."


translate spanish day4_dv_8cc97caa:


    me "Bueno, lo intentaré."


translate spanish day4_dv_6db936c0:


    "Tratar la herida con un pedazo de algodón mojado en yodo se sentía como hierro al rojo vivo. Apreté los dientes fuertemente y siseé de dolor."


translate spanish day4_dv_cc381102:


    dv "Oh, venga ya, no es tan doloroso."


translate spanish day4_dv_73eef208:


    me "¡Lo es! ¿Tal vez quieras probarlo por ti misma?"


translate spanish day4_dv_74a128a2:


    "Fue mucho más fácil con mis otras heridas y pronto me vi como un leopardo... todo cubierto de puntos marrones."


translate spanish day4_dv_73aab3cd:


    me "Gracias..."


translate spanish day4_dv_16cc6217:


    dv "No te creas que fue algo especial, ¡tan sólo teníamos que tratar tus heridas!"


translate spanish day4_dv_f505265f_1:


    "Alisa resopló y me dio la espalda."


translate spanish day4_dv_1c08f469:


    me "Claro, claro..."


translate spanish day4_dv_ec44416e:


    "Me abstuve de discutir y me levanté de la litera."


translate spanish day4_dv_9322dc07:


    "Incluso un pequeño descanso, claramente, ha sido bueno para mi."


translate spanish day4_dv_ca0fc878:


    me "Bueno, ¿nos vamos?"


translate spanish day4_dv_ce617998_2:


    "..."


translate spanish day4_dv_fbd5ef22:


    "Alisa se aferró a mi brazo, caminando al mismo ritmo de mis pasos."


translate spanish day4_dv_5adb4b2c:


    "Retornamos lentamente, con cuidado buscando un agujero en el techo."


translate spanish day4_dv_02fac7b0:


    "El túnel parecía no tener fin. No podíamos encontrar el lugar donde nos caímos, ni tras cinco minutos ni tras diez minutos."


translate spanish day4_dv_9f66e5e7:


    dv "¿Nos perdimos?"


translate spanish day4_dv_bf52bfa2:


    "Alisa preguntó tristemente."


translate spanish day4_dv_3b34fb45:


    me "Puede ser posible perderse en el bosque..."


translate spanish day4_dv_251da636:


    "Justo me acordé de que eso es exactamente lo que nos sucedió hace media hora atrás, por lo que para evitar asustarla más, continué con confianza:"


translate spanish day4_dv_722c0913:


    me "Pero esto es solamente un túnel recto... ¡no te puedes perder aquí!"


translate spanish day4_dv_9b43edaf:


    dv "Entonces, ¿dónde vamos a acabar al final pues?"


translate spanish day4_dv_61395ab8:


    "Mis palabras no parecieron calmar a Alisa lo suficiente..."


translate spanish day4_dv_070a72a2:


    me "Saldremos de aquí, siempre hay una salida."


translate spanish day4_dv_6ed2d8af:


    dv "¿De verdad?"


translate spanish day4_dv_b0ed4000:


    me "¡Claro, así es! Los trabajadores que lo construyeron de alguna forma tenían que subir a la superficie, ¿no crees?"


translate spanish day4_dv_bf62b6f0:


    dv "Claro, supongo."


translate spanish day4_dv_faa0cd99:


    "Ella intentaba aparentar ser fuerte como de costumbre, pero no parecía ir muy bien. En la tenue luz era evidente que Alisa estaba temblando. Una mirada de extremo esfuerzo lo disimulaba en su rostro."


translate spanish day4_dv_31843b52:


    me "Está bien. ¡Sigamos adelante!"


translate spanish day4_dv_ce617998_3:


    "..."


translate spanish day4_dv_36099f9d:


    "En pocos minutos me percaté de un agujero en el suelo.{w} Se parecía a un impacto de granada, lo suficientemente grande para que pudiéramos atravesarlo."


translate spanish day4_dv_4453f8b0:


    dv "¿Qué hay ahí abajo?"


translate spanish day4_dv_7479b07e_1:


    me "Al infierno si lo sé..."


translate spanish day4_dv_606bacdd:


    "Me agaché y dirigí el foco de luz hacia abajo. Suelo húmedo, algún tipo de vías de raíles."


translate spanish day4_dv_d07b5181:


    me "Debe ser una mina."


translate spanish day4_dv_47a46c70:


    "Alisa me miró inquisitivamente."


translate spanish day4_dv_1e95970b:


    "Naturalmente, podíamos deslizarnos por el agujero fácilmente, pero mis instintos insistían en que no encontraríamos tampoco nada bueno si procedíamos túnel abajo... tanto si hubiera un túnel sin salida como si hubiera otra puerta atascada."


translate spanish day4_dv_675becdd:


    "Por otra parte, era ingenuo creer que encontraríamos una salida en las minas."


translate spanish day4_dv_4b228483:


    me "Podríamos intentarlo. En el peor de los casos simplemente retrocedemos... es lo suficientemente bajo como para que lo trepemos posteriormente."


translate spanish day4_dv_a4cb18c4:


    "Su rostro tenía una expresión afectada. Entonces Alisa se apartó de la luz y dijo en voz baja:"


translate spanish day4_dv_632d7403:


    dv "Si lo dices tú..."


translate spanish day4_dv_ce617998_4:


    "..."


translate spanish day4_dv_78bd231b:


    "Como pensé, estábamos en una mina. Kilómetros de raíles oxidados se estrechaban a lo lejos de los pasillos. Podía imaginarme como veía los vagones de las minas sobrecargados chirriando por estas vías hace décadas atrás. Las paredes estaban apuntaladas con vigas pudridas y el agua de vez en cuando goteaba del techo."


translate spanish day4_dv_0ccb51dd:


    "No estaba seguro de si daba más miedo aquí o en las catacumbas del refugio antiaéreo de arriba. Diría que «allá arriba» era como un escenario postapocalíptico y «aquí abajo» reinaba un medievo oculto."


translate spanish day4_dv_89403983:


    "En cualquier caso estaba incluso más ansioso por escapar de aquí, y hacerlo cuanto antes."


translate spanish day4_dv_710c355f:


    "Alisa no pronunció ni una palabra desde que descendimos en la mina. Tan solamente caminaba a mi lado, aferrándose a mi mano tan fuertemente que empecé a preocuparme."


translate spanish day4_dv_580235f4:


    me "¿Estás bien?"


translate spanish day4_dv_e8936f84:


    dv "No."


translate spanish day4_dv_a68bf534:


    "Dijo en voz baja."


translate spanish day4_dv_fc571a13:


    me "Comprendo que puedes no estar «bien», pero ¿qué tal lo llevas?"


translate spanish day4_dv_3a950d86:


    dv "Bien."


translate spanish day4_dv_d15a76eb:


    "Respondió con esa voz indiferente."


translate spanish day4_dv_448d2873:


    me "Eso es bueno, ya que..."


translate spanish day4_dv_9f378f6c:


    "Realmente quise decir «ya que si te vuelves loca, sucederá una masacre»."


translate spanish day4_dv_64368234:


    me "Ya que hallaremos una salida. Sin duda, encontraremos una."


translate spanish day4_dv_887ed8a6:


    dv "Claro..."


translate spanish day4_dv_2528c1d3:


    "Parecía que no aguantaría mucho más."


translate spanish day4_dv_24b0636a:


    th "¡Debíamos ir más rápido!"


translate spanish day4_dv_0bcb931e:


    "Aceleré mis pasos y alcanzamos una bifurcación tras un par de docenas de metros."


translate spanish day4_dv_2ad79933:


    me "Bueno..."


translate spanish day4_dv_36bca938:


    dv "Y ahora, ¿por dónde?"


translate spanish day4_dv_af77dc81:


    "Alisa parecía haber desconectado completamente del mundo exterior y no se preocupaba sobre lo que nos podría suceder, o cómo podríamos abandonar este lugar."


translate spanish day4_dv_411a0051:


    me "Bueno, pensemos..."


translate spanish day4_dv_a6bcb093:


    th "¿Hacia la izquierda o hacia la derecha?{w} ¿Y si nos perdemos...?"


translate spanish day4_dv_4909406f:


    "Opté por dejar una marca en la pared en caso de que nos perdiéramos y retornáramos a este punto otra vez."


translate spanish day4_dv_272c439e:


    "En seguida, una gran cruz grabada con una piedra decoraba una de las vigas."


translate spanish day4_dv_803ee2f2:


    me "Bien, ahora estamos preparados para ir..."


translate spanish dv_mine_coalface_e40597aa:


    "Ya habíamos estado aquí."


translate spanish dv_mine_coalface_5b4c3c90:


    "Finalmente atravesamos el túnel y llegamos a una sala extensa con un techo elevado."


translate spanish dv_mine_coalface_3ef8c583:


    "Aunque a pesar de todo difícilmente se le podría llamar una sala; debían haber solido extraer alguna cosa aquí.{w} Carbón, probablemente, o tal vez oro."


translate spanish dv_mine_coalface_3c54fe69:


    "Las paredes habían sido talladas con picos o martillos neumáticos."


translate spanish dv_mine_coalface_c718c063:


    "Este lugar era negro como el alquitrán, por lo que nuestra única salvación era la linterna."


translate spanish dv_mine_coalface_b97863a3:


    th "Si se rompiera, es improbable que pudiéramos escapar de aquí..."


translate spanish dv_mine_coalface_58411121:


    "Entre la luz localicé repentinamente un pedazo de ropa roja."


translate spanish dv_mine_coalface_ca0a4f90:


    "¡Eso era un pañuelo de pionero!"


translate spanish dv_mine_coalface_96b3272c:


    "Shurik estaba obviamente aquí en alguna parte."


translate spanish dv_mine_coalface_93ec21f7:


    me "¡Shurik! ¡Shurik!"


translate spanish dv_mine_coalface_f23d45c0:


    "Solamente mi eco me respondió."


translate spanish dv_mine_coalface_cb6219ee:


    dv "¿Por qué estás gritando? Como si fuera a escucharte..."


translate spanish dv_mine_coalface_858fffec:


    me "Nunca sabes."


translate spanish dv_mine_coalface_07c129c5:


    "Alisa no me respondió, solamente suspiró."


translate spanish dv_mine_coalface_686bf61c:


    "No había otra salida desde esta sala."


translate spanish dv_mine_coalface_7ea80226:


    th "Obviamente, era bastante posible que hubiera algún escondrijo que no habíamos comprobado aun..."


translate spanish dv_mine_coalface_858b6cbe:


    th "Entonces... ¡tenemos que seguir buscando!"


translate spanish dv_mine_exit_8daf197d:


    "En la siguiente esquina, vi una puerta de madera con la luz de la linterna."


translate spanish dv_mine_exit_599ac8f7:


    me "¡Ahí va, hay alguna cosa!"


translate spanish dv_mine_exit_16377e5b:


    dv "¿Eso crees?"


translate spanish dv_mine_exit_7434af59:


    "Alisa parecía estar un poco animada."


translate spanish dv_mine_exit_771aa502:


    me "¡Claro!"


translate spanish dv_mine_exit_e2d76069:


    "Para ser sinceros, no estaba en verdad muy seguro de que hubiera una salida tras esta puerta."


translate spanish dv_mine_exit_f3b9dae9:


    "Pero tampoco es que tuviéramos otra alternativa."


translate spanish dv_mine_exit_efd4a253:


    "Empujé el mango de la puerta..."


translate spanish dv_mine_exit_c4bfa0fb:


    "Nos hallamos en algún tipo de habitación de trastero propia de una caldera, o como una habitación de utilidad para el refugio antiaéreo."


translate spanish dv_mine_exit_d9981fe3:


    "Botellas vacías, colillas de cigarrillos y otros desechos esparcidos por el suelo."


translate spanish dv_mine_exit_2328f028:


    "¡Eso demuestra que hay una salida de estas cuevas!"


translate spanish dv_mine_exit_34338260:


    "Registré la habitación con la linterna y en el rincón más alejado vi..."


translate spanish dv_mine_exit_d4831ba9:


    "A Shurik, acurrucado y todo él temblando."


translate spanish dv_mine_exit_b01a7f13:


    me "¡Shurik!"


translate spanish dv_mine_exit_58f9ccc4:


    sh "¿Quién... quién es?"


translate spanish dv_mine_exit_377c5a6f:


    "Tartamudeó."


translate spanish dv_mine_exit_4701c46e:


    me "¡Shurik! ¡¿Te hemos estado buscando toda la noche y estabas sentado justo aquí?! ¡Levántate!"


translate spanish dv_mine_exit_3901fe16:


    sh "No voy a ir ninguna parte..."


translate spanish dv_mine_exit_6ebe4e41:


    "Comenzó a hablar en voz baja, pero con cada palabra su voz iba recobrando fuerza hasta que estaba gritando a pleno pulmón."


translate spanish dv_mine_exit_7979942d:


    sh "¡No, no voy a ir ninguna parte con vosotros! ¡No existís! ¡No estáis aquí de todas formas! ¡Es una alucinación! ¡No tiene rigor científico! ¡Claro, no es científico!"


translate spanish dv_mine_exit_5857f574:


    me "¡¿Qué quieres decir conque «no estamos aquí»?! ¡Estamos aquí, vinimos buscándote a ti!"


translate spanish dv_mine_exit_783dbf4d:


    "Alisa había permanecido en silencio hasta ahora, pero entonces dejó ir mi mano y dio unos pasos hacia Shurik."


translate spanish dv_mine_exit_8bdd7529:


    me "¿Qué estás haciendo...?"


translate spanish dv_mine_exit_420d89ea:


    "Pregunté en voz baja."


translate spanish dv_mine_exit_782e55fe:


    "Ella dudó por un instante, pero procedió como si no me hubiera escuchado."


translate spanish dv_mine_exit_bfea42b5:


    "Finalmente, habiéndose acercado al pionero desaparecido, Alisa le dio una cruel bofetada en la cara."


translate spanish dv_mine_exit_3a43352d:


    dv "¡Tú! ¡Casi morimos mientras te buscábamos! ¡Aquel maldito bosque por la noche, aquel refugio antiaéreo, estas extravagantes minas! ¡Soy un montón de moratones gracias a ti! Y tu qué... solamente pasando el tiempo, emborrachándote y viendo arañas rosas, ¡bastardo!"


translate spanish dv_mine_exit_af2a4a69:


    "Shurik la miró fijamente con una mirada perpleja."


translate spanish dv_mine_exit_57632dcb:


    dv "Y ahora, ¡levanta tu culo y muévete!"


translate spanish dv_mine_exit_7e0c8149:


    sh "No..."


translate spanish dv_mine_exit_fc567eb6:


    "Comenzó a susurrar en voz baja."


translate spanish dv_mine_exit_7e0c8149_1:


    sh "No..."


translate spanish dv_mine_exit_8f7b3a64:


    "Algo profundo en mi, casi al borde de mi conciencia, tuvo el presentimiento de que algo malo iba a ocurrir ahora."


translate spanish dv_mine_exit_b5bdda9c:


    "Ocurrió. Una premonición, intuición o tan sólo un análisis de la situación, en la forma en que Shurik estaba sentado o de la expresión en su rostro indicaron sus siguientes acciones."


translate spanish dv_mine_exit_94c2b3ff:


    "Y probablemente, todo ello en su conjunto. Salí corriendo hacia Alisa, la luz saltando furiosamente por toda la habitación e iluminando por un segundo el rostro de Shurik, retorcido de locura."


translate spanish dv_mine_exit_495d2be6:


    "Llevaba un pasamanos de metal y si hubiera sido un instante más tarde, claramente le habría golpeado en la cabeza a Alisa con ello."


translate spanish dv_mine_exit_0e566744:


    "Pero lo hice."


translate spanish dv_mine_exit_e6910a04:


    "Todo fue como un sueño: el grito distante de una muchacha, como un eco sordo y vacío, la linterna apagada y un inquietante silencio, salvo por la profunda respiración de Shurik."


translate spanish dv_mine_exit_4aee785c:


    "Me llevó un rato recuperar los sentidos."


translate spanish dv_mine_exit_de443349:


    me "¡¿Qué estás haciendo, eres idiota?!"


translate spanish dv_mine_exit_faa7fd6f:


    "Comprobé mi brazo y sentí que estaba bien... el golpe alcanzó la linterna.{w} Por lo que ahora estábamos en una completa oscuridad."


translate spanish dv_mine_exit_2b53bb97:


    sh "¡No lo conseguiréis, no me atraparéis!"


translate spanish dv_mine_exit_e324829f:


    "La risa histérica de Shurik se fue desvaneciendo lejos en la distancia."


translate spanish dv_mine_exit_61203408:


    me "¡Ey! ¡¿Dónde estás yendo?! ¡Vuelve aquí ahora!"


translate spanish dv_mine_exit_e37edfba:


    sh "No me atraparéis..."


translate spanish dv_mine_exit_a8eff0da:


    "El chillido se desvaneció en la distancia..."


translate spanish dv_mine_exit_5919c4da:


    me "Será loco..."


translate spanish dv_mine_exit_4fd3278c:


    "Intenté arreglar la linterna en la oscuridad. No hubo forma, Shurik la había roto en pedazos con su pasamanos. Luego de repente pensé en Alisa."


translate spanish dv_mine_exit_a1e11399:


    me "¿Te encuentras bien?"


translate spanish dv_mine_exit_2a2977e4:


    "Escuché un sollozo silencioso y al mismo instante sentí su abrazo, rodeándome estrechamente."


translate spanish dv_mine_exit_0288e8be:


    me "Vale, está bien, ya pasó todo, todo irá bien..."


translate spanish dv_mine_exit_f7c6dd5b:


    th "Bueno, de hecho nada fue bien, pero necesitaba darle un respiro para que se recuperase. De otra forma no tendríamos oportunidad de salir con vida de aquí."


translate spanish dv_mine_exit_e77948eb:


    "Le acaricié tiernamente su cabeza."


translate spanish dv_mine_exit_e7f9499f:


    th "Me pregunto si Alisa llorando... ¿no es algo gracioso?{w} Tal vez en unas circunstancias diferentes..."


translate spanish dv_mine_exit_a1e11f2c:


    dv "Estoy asustada..."


translate spanish dv_mine_exit_cb460d93:


    me "Está bien, yo también estoy asustado..."


translate spanish dv_mine_exit_abca858d:


    "Me percaté de esta fuerte y repentina transformación en Alisa."


translate spanish dv_mine_exit_7e138f85:


    th "Pero que diablos, cualquiera en estas circunstancias se asustaría.{w} Además, ella es una muchacha..."


translate spanish dv_mine_exit_1e55b63f:


    th "¿Pero por qué yo no estoy asustado?{w} No, no estoy asustado... ¿por qué puedo tener aun mi mente despejada y hablar con conocimiento?"


translate spanish dv_mine_exit_4a5234b5:


    "¿Puede que sea porque tengo que preocuparme de alguien más a parte de mi mismo?"


translate spanish dv_mine_exit_8ce5aeb2:


    me "Vale, venga, tenemos que salir de aquí de cualquier forma."


translate spanish dv_mine_exit_5a998b89:


    dv "Espera, sólo un segundo..."


translate spanish dv_mine_exit_db7a783b:


    "Dijo Alisa en voz baja, y la habitación se volvió a iluminar con el tenue parpadeo de la llama de un encendedor."


translate spanish dv_mine_exit_c0d3e816:


    me "¿De dónde ha salido eso?"


translate spanish dv_mine_exit_3d4c4201:


    dv "La estatua..."


translate spanish dv_mine_exit_d01e1437:


    "Forzó una amable sonrisa y me prestó el encendedor."


translate spanish dv_mine_exit_505d91ae:


    me "Muy bien..."


translate spanish dv_mine_exit_863a1c4e:


    "Rápidamente barrí con mi mirada por toda la habitación, buscando algo que pudiera ser usado como una antorcha."


translate spanish dv_mine_exit_d0ca8ec6:


    "Había algunos harapientos trapos y un pasamanos entre la mugre del suelo. Una de las botellas contenía un líquido que olía como alcohol industrial."


translate spanish dv_mine_exit_c238b970:


    "Para bien o para mal, en un minuto estaba sosteniendo algo parecido a una antorcha de fuego con mis manos."


translate spanish dv_mine_exit_93e8f263:


    me "No sé cuánto durará, así que tenemos que movernos rápidamente."


translate spanish dv_mine_exit_bf5b8ada:


    dv "¿Hacia dónde?"


translate spanish dv_mine_exit_92ae42e9:


    "Alisa parecía recobrar su conocimiento."


translate spanish dv_mine_exit_4a1ef41f:


    me "Volvamos atrás. Al menos llegaremos hasta el refugio antiaéreo. Una cosa es segura, hay luz allá."


translate spanish dv_mine_exit_ce1ee8c8:


    "Ella asintió con su cabeza en silencio y se agarró a mi mano."


translate spanish dv_mine_exit_ce617998:


    "..."


translate spanish dv_mine_exit_9e59519b:


    "La antorcha ardía pobremente: tuvimos que prendarla de nuevo cada ciertos minutos. Uno no tenía que ser un Oráculo para predecir la extrema corta vida del trapo que usábamos."


translate spanish dv_mine_exit_4b60d852:


    "Comencé a preocuparme cada vez más y más de que no abandonaríamos la oscuridad de la mina. No podías confiar demasiado solamente en el encendedor."


translate spanish dv_mine_exit_3d40f229:


    "Sin embargo, alcanzamos bastante rápidamente el agujero que nos llevó a las catacumbas... parece ser que memoricé el camino después de todo."


translate spanish dv_mine_exit_6afe7d5f:


    "Lo subimos con dificultad, arrastrándonos sobre las rocas casi a ciegas."


translate spanish dv_mine_exit_15f32c97:


    dv "Vale, ¿y ahora adónde vamos?"


translate spanish dv_mine_exit_f590dde1:


    "Preguntó Alisa, recuperando el aliento."


translate spanish dv_mine_exit_47bf9d1c:


    me "¿Qué quieres decir? Vamos hacia esa habitación con las literas y armarios."


translate spanish dv_mine_exit_ec19d6bf:


    dv "¿Qué camino es ese?"


translate spanish dv_mine_exit_5e081fd0:


    "Abrí mi boca para responder, pero de repente me di cuenta de que no tenia ni la menor idea."


translate spanish dv_mine_exit_61ac798c:


    "El agujero se veía igual desde todos los lados, justo como las paredes, el suelo y el techo. No teníamos ni una sola pista que indicara la dirección correcta."


translate spanish dv_mine_exit_f883183e:


    me "Bueno..."


translate spanish dv_mine_exit_7b1bbd69:


    dv "¿No lo sabes?"


translate spanish dv_mine_exit_87e8b661:


    "Alisa preguntó tristemente y se sentó en el suelo."


translate spanish dv_mine_exit_5a7a7577:


    me "No."


translate spanish dv_mine_exit_09882a0d:


    "Respondí en voz baja, apagué la antorcha para preservarla y me senté a su lado."


translate spanish dv_mine_exit_816feedf:


    dv "¿Vamos a morir aquí?"


translate spanish dv_mine_exit_89d773f5:


    "Ella dejó descansar su cabeza sobre mi hombro."


translate spanish dv_mine_exit_c69e1f3a:


    "La voz de Alisa sonaba desanimada, incluso uno diría que tranquila, pero podía sentir que su cuerpo estaba temblando... fuera por el frío o por el miedo, o por ambos en conjunto."


translate spanish dv_mine_exit_15d317c8:


    me "¡No seas tonta! Claro que las que cosas se han puesto mal, pero están muy lejos todavía de «vamos a morir»... Vendrán a rescatarnos por la mañana. Olga Dmitrievna llamará a la policía. Bueno..."


translate spanish dv_mine_exit_a74b8123:


    th "Me habría gustado creerme a mi mismo."


translate spanish dv_mine_exit_4677b777:


    dv "Ya veo..."


translate spanish dv_mine_exit_751ac511:


    me "Claro..."


translate spanish dv_mine_exit_ce617998_1:


    "..."


translate spanish dv_mine_exit_b162cf42:


    "Perdí la cuenta del tiempo que transcurrió sentado aquí..."


translate spanish dv_mine_exit_d3c437c7:


    "Estaba preocupado de ir hacia delante o hacia atrás, estaba preocupado de tomar una elección, en tanto que nuestra antorcha claramente no duraría en ambas direcciones. Vagar en la oscuridad total sin ninguna fuente de luz era definitivamente peor que esperar aquí a ser rescatados."


translate spanish dv_mine_exit_2607a8c2:


    "Al menos de esa manera tendríamos algo de luz en caso de emergencia..."


translate spanish dv_mine_exit_af386fc2:


    "De repente un estruendo empezó a escucharse como un eco en la distancia."


translate spanish dv_mine_exit_b03fee0e:


    th "Bueno, ¡aquí está esa emergencia!"


translate spanish dv_mine_exit_40ea5406:


    "Era claramente el rechinar de los tornillos de una puerta mezclados en ese estruendo."


translate spanish dv_mine_exit_7b953cc3:


    me "¡Levántate!"


translate spanish dv_mine_exit_a1c23a4f:


    "Con rudeza puse en pie a Alisa y prendí la antorcha con manos nerviosas."


translate spanish dv_mine_exit_d75c13ce:


    dv "¿Qué es?"


translate spanish dv_mine_exit_93c290bf:


    "Preguntó consternada."


translate spanish dv_mine_exit_e79963e5:


    me "No lo sé, pero parece que hay alguien. ¡Tenemos que correr!"


translate spanish dv_mine_exit_ce36a4aa:


    "Para ser sinceros, no estaba seguro de que fuera una gran idea."


translate spanish dv_mine_exit_228b3ef4:


    th "¿Y si el loco de Shurik está ahí?{w} Y, de hecho, ¡ése sigue siendo el menor de los males que puedo imaginar!"


translate spanish dv_mine_exit_43f41aac:


    "Pero no teníamos alternativa."


translate spanish dv_mine_exit_5e04ae36:


    "Las piedras se desperdigaban bajo nuestros pies. Traté deseperadamente de no caerme, sosteniendo la antorcha frente a mí con una mano y con la otra arrastrando a Alisa."


translate spanish dv_mine_exit_19c33fc2:


    "Ella corría en silencio, respirando profundamente."


translate spanish dv_mine_exit_cd113210:


    "Realmente quería mirarla detrás, pero no tenía ni la fuerza ni el tiempo para ello."


translate spanish dv_mine_exit_8f8cca4d:


    "Finalmente el contorno de la puerta abierta apareció frente a nosotros, salté dentro del refugio antiaéreo, preparándome para luchar contra cualquier enemigo que estuviera aquí..."


translate spanish dv_mine_exit_208a6c81:


    "Pero la habitación estaba vacía..."


translate spanish dv_mine_exit_60e62f39:


    "Barrí con mis ojos toda la habitación, intentando con esfuerzo divisar monstruos escondidos, a Shurik o cualquier otra entidad malvada, pero era todo exactamente igual que como la primera vez que estuvimos aquí."


translate spanish dv_mine_exit_cf80a68b:


    "Excepto por la puerta, la segunda puerta...{w} ¡Estaba abierta!"


translate spanish dv_mine_exit_9e0f57f2:


    me "¡Alguien ha pasado por aquí!"


translate spanish dv_mine_exit_1c4ba667:


    "Comenté triunfalmente."


translate spanish dv_mine_exit_e12061da:


    dv "¿Quién?"


translate spanish dv_mine_exit_f17c350e:


    me "No lo sé, tal vez fuera Shurik..."


translate spanish dv_mine_exit_b26a8910:


    dv "¿Pero cómo se las ha arreglado para abrirla?"


translate spanish dv_mine_exit_8a8827fd:


    me "¡Y a quién le importa! ¡Venga!"


translate spanish dv_mine_exit_ce617998_2:


    "..."


translate spanish dv_mine_exit_dbf5f587:


    "Había otro pasillo tras esa puerta, y ascendía lentamente."


translate spanish dv_mine_exit_6e4dbe96:


    th "Por lo que estábamos más cerca de la superficie."


translate spanish dv_mine_exit_36ebc0c6:


    "Esta en lo cierto, tras varios centenares de pasos, tropecé con una escalera que dirigía a una pequeña trampilla en el techo."


translate spanish dv_mine_exit_1c207941:


    "No llevó mucho esfuerzo abrirla."


translate spanish dv_mine_exit_4453f8b0:


    dv "¿Qué hay ahí?"


translate spanish dv_mine_exit_4164ce88:


    me "A quien le importa, ¡es mejor que permanecer aquí abajo de todas maneras!"


translate spanish dv_mine_exit_6f888876:


    "Trepé por las escaleras y me hallé rodeado por la penumbra."


translate spanish dv_mine_exit_6ca992b0:


    "Sin embargo, esta penumbra era definitivamente {i}más clara{/i} que la oscuridad de las catacumbas; cuanto más se acostumbraron mis ojos a ella, los contornos de las paredes, las escaleras y la puerta que llevaba afuera, emergieron en la pálida luz lunar."


translate spanish dv_mine_exit_8aeb5a0d:


    "Ayudé a Alisa a escalar y literalmente surgimos de allí."


translate spanish dv_mine_exit_681894bf:


    "La salida de las catacumbas estaba localizada en algún tipo de edificio en ruinas que se parecía a una guardería o a una escuela de pueblo."


translate spanish dv_mine_exit_24905350:


    dv "Debe ser el antiguo campamento..."


translate spanish dv_mine_exit_630ee13c:


    "Alisa se sentó en uno de los columpios y se quitó el sudor de su frente con el dorso de la mano."


translate spanish dv_mine_exit_0adb5982:


    me "Ya veo... Bueno, al final lo encontramos."


translate spanish dv_mine_exit_ef2764bc:


    "Sonreí involuntariamente."


translate spanish dv_mine_exit_2a6404d7:


    dv "¿De qué te estás riendo?"


translate spanish dv_mine_exit_76027b11:


    "Ella frunció el ceño e hizo una mueca de disgusto.{w} En pocas palabras, volvía a ser la de siempre."


translate spanish dv_mine_exit_fc154605:


    me "Y bien, ¿ya no hace tanto miedo aquí?"


translate spanish dv_mine_exit_39ef9914:


    dv "¡Tampoco tenía miedo allí!"


translate spanish dv_mine_exit_2de4d7dc:


    me "¡Oh, venga ya!"


translate spanish dv_mine_exit_f8f4d1e6:


    dv "Es cierto, ¡no tenía!"


translate spanish dv_mine_exit_66368319:


    me "Ah, da igual..."


translate spanish dv_mine_exit_0812a5df:


    "Estaba enfadado."


translate spanish dv_mine_exit_bb429265:


    th "Me sentía realmente herido por culpa de Alisa. Si no fuera por mí, ella aun seguiría ahí abajo, ¡en esa maldita mina!"


translate spanish dv_mine_exit_25532b53:


    dv "¿Adónde vas?"


translate spanish dv_mine_exit_6773d0b2:


    "Preguntó con una voz menos segura en tanto me di la vuelta y caminé lentamente en dirección hacia el campamento."


translate spanish dv_mine_exit_19a037e5:


    me "Vuelvo al campamento."


translate spanish dv_mine_exit_93b5fc50:


    dv "¡Ey!"


translate spanish dv_mine_exit_95ef7c80:


    "Alisa saltó alejándose y comenzó a caminar a mi lado."


translate spanish dv_mine_exit_fbe21e1f:


    "Por un instante quise decirle alguna cosa despreciable, pero no estaba preparado para comenzar a discutir..."


translate spanish dv_mine_exit_860a390f:


    "Encontramos el sendero que tomamos antes con sorprendente rapidez. Incluso los dos agujeros por los cuales nos caímos."


translate spanish dv_mine_exit_18eb7fb3:


    "Estaba solamente a unos doscientos metros del antiguo campamento."


translate spanish dv_mine_exit_ce617998_3:


    "..."


translate spanish dv_mine_exit_47e214d8:


    "Me detuve en la plaza y me giré hacia Alisa."


translate spanish dv_mine_exit_505d91ae_1:


    me "Bueno..."


translate spanish dv_mine_exit_8bfb630c:


    dv "Bueno..."


translate spanish dv_mine_exit_4d3e55f3:


    "Parecía insegura, incluso confundida."


translate spanish dv_mine_exit_2e84db32:


    me "Entonces..."


translate spanish dv_mine_exit_887ed8a6:


    dv "Claro..."


translate spanish dv_mine_exit_7f338ab0:


    "Por dentro mío quería chillarle, regañarla, incluso insultarla.{w} Por otra parte, quería tener una conversación significativa."


translate spanish dv_mine_exit_a3c6e840:


    "Al final simplemente me mantuve en silencio, me di la vuelta y lentamente me dirigí hacia la cabaña de la líder."


translate spanish dv_mine_exit_5bda040e:


    "Pero alguna cosa me trastornó persistentemente la tranquilidad de la noche.{w} Escuché con atención y me percaté de que algo estaba roncando..."


translate spanish dv_mine_exit_ff984fa0:


    "...era Shurik, ¡quien estaba durmiendo pacíficamente en un banco!"


translate spanish dv_mine_exit_95efcfe1:


    me "¡Ey!"


translate spanish dv_mine_exit_997fab3e:


    "Llamé a Alisa, quien no había caminado muy lejos."


translate spanish dv_mine_exit_79d3bbc8:


    me "¡Tú, levántate!"


translate spanish dv_mine_exit_94f86bab:


    "Shurik lentamente recobró su conocimiento y nos miró fijamente de forma perpleja."


translate spanish dv_mine_exit_a5cfef85:


    sh "Oh, ¿ya es de mañana?"


translate spanish dv_mine_exit_ec14641b:


    me "¿Mañana? Claro, claro..."


translate spanish dv_mine_exit_565e9484:


    "A veces uno actua primero, luego piensa."


translate spanish dv_mine_exit_d3e52cd4:


    "Por supuesto, todo comienza en el cerebro, luego la corriente llega a través de los enlaces neuronales, de este modo se transmite la orden al cuerpo."


translate spanish dv_mine_exit_e6d884ea:


    "Pero a veces el subconsciente funciona simplemente más rápido que la consciencia.{w} Ésa es exactamente la razón por la cual mi puño golpeó en la tripas de Shurik, y fue entonces cuando en realidad me di cuenta de lo que había hecho."


translate spanish dv_mine_exit_92f15912:


    "Tosió, tratando de recuperar el aliento, retorciéndose de dolor en el banco."


translate spanish dv_mine_exit_8f6f0153:


    me "¡¿Qué demonios fue todo aquello?!"


translate spanish dv_mine_exit_c415cb10:


    "Ya me sentía mal por ello... No debería haberlo tratado tan severamente..."


translate spanish dv_mine_exit_43efdcda:


    sh "¿De qué estás... de qué estás hablando?"


translate spanish dv_mine_exit_40c0b040:


    "Shurik me miraba estupefacto."


translate spanish dv_mine_exit_1a18e0c9:


    me "¡En la mina!"


translate spanish dv_mine_exit_a786120b:


    sh "¿Qué mina?"


translate spanish dv_mine_exit_d18f8765:


    "Negaba con la cabeza, completamente confundido."


translate spanish dv_mine_exit_1ccd3e61:


    sh "¿Y qué hago aquí?"


translate spanish dv_mine_exit_a031441e:


    sh "¿Y qué hacéis vosotros aquí?"


translate spanish dv_mine_exit_3f214d58:


    dv "¡Deja de hacerte el tonto!"


translate spanish dv_mine_exit_3d3d749e:


    "Alisa interrumpió la conversación."


translate spanish dv_mine_exit_e44dae44:


    dv "¡Casi me matas allí abajo! ¡¿Cómo pretendes insinuar que nada ocurrió?!"


translate spanish dv_mine_exit_f86b3336:


    sh "¿Qué sucedió?"


translate spanish dv_mine_exit_7a20f42c:


    me "¿De verdad que no te acuerdas?"


translate spanish dv_mine_exit_7e0c8149_2:


    sh "De nada..."


translate spanish dv_mine_exit_1780b682:


    me "Vale, entonces... ¿qué es lo último que recuerdas?"


translate spanish dv_mine_exit_7843e662:


    "Shurik trató de concentrarse."


translate spanish dv_mine_exit_32afcb7b:


    sh "Bueno, me fui por mi cuenta al antiguo campamento por la mañana. Sabía que podría encontrar algo de hardware para el robot ahí, así que..."


translate spanish dv_mine_exit_f95c9fc9:


    me "¿Así qué?"


translate spanish dv_mine_exit_43309355:


    sh "Y eso es todo... No recuerdo nada a partir de ahí. Luego ya me desperté aquí."


translate spanish dv_mine_exit_e94e8b48:


    "Expiré un profundo suspiro en tanto me daba la vuelta."


translate spanish dv_mine_exit_357bf682:


    me "Bueno, adelante, dulces sueños..."


translate spanish dv_mine_exit_32b62074:


    dv "Ey, ¿dónde te vas...?"


translate spanish dv_mine_exit_381b0677:


    "Ignoré a Alisa y me tropecé a distancia con la cabaña de Olga Dmitrievna.{w} Ella se quedó, discutiendo con Shurik."


translate spanish dv_mine_exit_ce617998_4:


    "..."


translate spanish dv_mine_exit_594f36e4:


    "Me senté en la tumbona y mire arriba a las estrellas."


translate spanish dv_mine_exit_6b72237f:


    "Esta noche parecía estar brillando más que de costumbre."


translate spanish dv_mine_exit_4b4166ca:


    "Quizá lo parecían a causa de que no hace mucho rato mi única fuente de luz fue una débil linterna, y luego aquella antorcha."


translate spanish dv_mine_exit_a3c69ac7:


    "Las estrellas son más brillantes que una linterna y obviamente que una antorcha.{w} La mayoría de las estrellas, supongo, son incluso más brillantes que el propio sol, pero están tan alejadas..."


translate spanish dv_mine_exit_d806349a:


    me "Bueno, ¿y por qué has venido?"


translate spanish dv_mine_exit_7fd15fd8:


    "Pregunté sin tan siquiera girar mi cabeza... los pasos de Alisa podían escucharse bien mientras avanzaban en el silencio de la noche."


translate spanish dv_mine_exit_29cec6ff:


    dv "Bueno, yo..."


translate spanish dv_mine_exit_80ecf5fd:


    me "¿Qué te dijo Shurik?"


translate spanish dv_mine_exit_6a9ad2e5:


    dv "Dijo que no se acordaba de nada, que era algo «sin rigor científico» y cosas sin sentido como ésas."


translate spanish dv_mine_exit_946b1e7d:


    me "Creo que realmente no se acuerda a causa del trastorno y el estrés."


translate spanish dv_mine_exit_2a5b356a:


    th "¿Quién soy yo para recriminárselo?{w} Estaba en el mismo barco no hace mucho tiempo."


translate spanish dv_mine_exit_b9277d5e:


    th "Y aun estoy en él a pesar de todo.{w} ¿Tengo acaso amnesia?"


translate spanish dv_mine_exit_9a5edc2d:


    me "Muy bien, ¿por qué viniste?"


translate spanish dv_mine_exit_fa9696b2:


    "Nuevamente, supongo que sabía la respuesta... exactamente por eso no me fui a dormir ya, sino que me quedé esperándola aquí."


translate spanish dv_mine_exit_c9fe61bc:


    dv "Bueno, yo..."


translate spanish dv_mine_exit_4eface95:


    "Alisa se sentó a mi lado."


translate spanish dv_mine_exit_4e4c10a9:


    dv "Quería algo así como... darte las gracias... Después de todo, allí abajo... Tú... Eso, ya sabes..."


translate spanish dv_mine_exit_92279f92:


    me "No hay problema."


translate spanish dv_mine_exit_028267bd:


    "Dije amablemente y me recosté."


translate spanish dv_mine_exit_9763a346:


    dv "Bueno, muy bien pues..."


translate spanish dv_mine_exit_cb0a1b3d:


    "Ella se levantó y se iba a ir."


translate spanish dv_mine_exit_9492db5a:


    me "Si crees que estoy enfadado contigo, no lo estoy. Todo está bien."


translate spanish dv_mine_exit_54ef0861:


    dv "¡En realidad no pensaba eso!"


translate spanish dv_mine_exit_6f52c0b0:


    "Alisa se alteró."


translate spanish dv_mine_exit_712a2ea4:


    me "Está bien pues."


translate spanish dv_mine_exit_8bfb630c_1:


    dv "Vale..."


translate spanish dv_mine_exit_751ac511_1:


    me "Claro..."


translate spanish dv_mine_exit_a1bcd7f2:


    dv "Entonces..."


translate spanish dv_mine_exit_1bd407b9:


    me "¡Ya te puedes ir!"


translate spanish dv_mine_exit_19ed4ed8:


    "Dije amablemente y me despedí con la mano."


translate spanish dv_mine_exit_eb93b5d8:


    dv "¡Me iré cuando quiera!"


translate spanish dv_mine_exit_cbd890f2:


    me "Entonces...¿eso es que no quieres irte?"


translate spanish dv_mine_exit_ebd91224:


    dv "¡Quiero!"


translate spanish dv_mine_exit_2b9ec1df:


    me "¿Y Bien? ¿Hay alguna cosa que te impida irte?"


translate spanish dv_mine_exit_82ddf868:


    dv "¡Idiota!"


translate spanish dv_mine_exit_c46574f3:


    "Alisa pisoteó el suelo y rápidamente se alejó caminando de la cabaña de la líder."


translate spanish dv_mine_exit_b44e66a1:


    "Hice una profunda respiración y me levanté."


translate spanish dv_mine_exit_d5cb476b:


    "Mi cabeza estaba terriblemente mareada por la fatiga."


translate spanish dv_mine_exit_05cac35e:


    th "Bueno, al menos Olga Dmitrievna estará ya durmiendo y no tendré que dar explicaciones de nada..."


translate spanish dv_mine_exit_8e63c1c0:


    "Sin embargo, eso no era tan fácil."


translate spanish dv_mine_exit_69a264ae:


    "La líder del campamento estaba allí en medio de la habitación y se estaba preparando obviamente para una larga conversación."


translate spanish dv_mine_exit_a38129f5:


    "O más bien para interrogar."


translate spanish dv_mine_exit_d8b1d698:


    mt "¿Quisieras explicarte?"


translate spanish dv_mine_exit_132385d6:


    me "¿Qué sucede?{w} De hecho, ¿no te importó cuando fuimos a buscar a Shurik?"


translate spanish dv_mine_exit_28247bed:


    mt "Y que, ¿le habéis encontrado?"


translate spanish dv_mine_exit_ded1e9af:


    "Parecía que ella estuviera más preocupada por mí al llegar tarde que del destino de un pionero perdido."


translate spanish dv_mine_exit_3385696b:


    me "Sí, lo encontramos.{w} Por cierto, ¿qué haces aquí a oscuras?"


translate spanish dv_mine_exit_6334b91e:


    mt "¿Qué?"


translate spanish dv_mine_exit_f72b1120:


    me "Digo que, ¿qué hacías en la oscuridad?"


translate spanish dv_mine_exit_3597bffc:


    mt "Porque es la hora de ir a dormir."


translate spanish dv_mine_exit_14658aae:


    "No podía estar más de acuerdo con ella, aunque estaba ligeramente sorprendido por semejante repentino cambio de humor."


translate spanish dv_mine_exit_a20cefa7:


    "..."


translate spanish dv_mine_exit_a4baed18:


    "Apenas me tumbé en la cama, caí dormido sin desvestirme."


translate spanish dv_mine_exit_b6a45476:


    th "Pero aun así, Alisa...{w} Alisa..."


translate spanish dv_mine_exit_fff7cafa:


    "Tan sólo no sabía que debería pensar de ella."


translate spanish dv_mine_exit_f6f64c2c:


    "No era a causa de que se estuviera comportando de forma extraña recientemente.{w} No, por el contrario, todo su comportamiento era bastante constante y comprensible."


translate spanish dv_mine_exit_b0dedce5:


    "Incluso el volver para darme las gracias."


translate spanish dv_mine_exit_7bad5d82:


    "Pensé en Alisa mucho más que en cualquier otra cosa que hubiera ocurrido esta noche."


translate spanish dv_mine_exit_8dbd75e9:


    "Aunque, si te fijas, no había nada de sobrenatural en ello."


translate spanish dv_mine_exit_a701bda4:


    "Incómodo... claro, aterrador, incluso escalofriante... seguro.{w} Algo relacionado con mi llegada aquí... difícilmente."


translate spanish dv_mine_exit_b249ce58:


    "Estos pensamientos me dejaron durmiendo."


translate spanish day4_us_aa8cb942:


    us "¡Cuenta conmigo!"


translate spanish day4_us_13d9d85c:


    "Observé a Ulyana, asombrado."


translate spanish day4_us_2452df7b:


    "Su sed de aventuras parecía no tener límites."


translate spanish day4_us_60164174:


    us "¡Mira! La noche, fantasmas, un antiguo campamento... ¡es genial!"


translate spanish day4_us_bf708ac9:


    th "Por una parte, semejante compañía siempre conlleva problemas, pero por otra parte, te sientes seguro yendo acompañado..."


translate spanish day4_us_003878d1:


    mt "¡Perfecto!"


translate spanish day4_us_af906072:


    "Habiéndonos despedido de las otras muchachas y Olga Dmitrievna, nos dejaron solos."


translate spanish day4_us_48cc7f3f:


    me "¿Acaso consideras esto solamente como una caminata para entretenerse?"


translate spanish day4_us_e8b3b40c:


    us "Bueno, sí. ¿Qué pasa?"


translate spanish day4_us_94876b55:


    "Ulyana soltó una risita nerviosa."


translate spanish day4_us_b92e40c7:


    me "Olvídalo... En serio..."


translate spanish day4_us_ed0914b5:


    "Suspiré."


translate spanish day4_us_2534cd63:


    us "¡Oh, quieto!{w} ¡Voy a traer una linterna!"


translate spanish day4_us_cd917d11:


    me "Vale."


translate spanish day4_us_ad322fcb:


    "Justo iba a sugerirlo yo también."


translate spanish day4_us_73291f33:


    th "Parecía que no solamente iría a visitar el antiguo campamento por la noche, sino que también vigilaría a una nerviosa cría..."


translate spanish day4_us_3ce9798d:


    "En fin, ésa es Ulyana..."


translate spanish day4_us_4da4d2d1:


    th "Debería ser el doble de prudente."


translate spanish day4_us_7bd87323:


    "Por lo que Electronik nos explicó, el viejo edificio fue construido justo tras la guerra."


translate spanish day4_us_fd3c1cdc:


    "Se parecía a un jardín de parvulitos (o como unos barracones, supuse) y claramente podía alojar menos pioneros que el actual campamento."


translate spanish day4_us_e9bbdb04:


    "Había sido abandonado hacía ya unos veinte años."


translate spanish day4_us_b5d9b351:


    "Ulyana seguía delante saltando, como si todo fuera tan solamente un juego para ella."


translate spanish day4_us_5f48e206:


    "Estaba incómodo."


translate spanish day4_us_71ff7e44:


    "En realidad era algo normal para una persona en mi situación... en el bosque por la noche con terroríficos animales y pájaros impacientes por tirarse encima de ti, la luna llena y todo este otro mundo al que llegué no hace mucho."


translate spanish day4_us_1608aae1:


    "Puede que hubiera sido mejor ir solo, sin necesidad de tener que estar vigilando a Ulyana quien iba corriendo delante de mí."


translate spanish day4_us_04ecc3b7:


    th "¿Cómo es que no se ha tropezado con algo ya?"


translate spanish day4_us_bd8fd797:


    me "Escucha. Ve con cuidado..."


translate spanish day4_us_d4cf3f77:


    us "¿O qué?"


translate spanish day4_us_0382e70f:


    "Se dio la vuelta tan de repente que me dio un sobresalto."


translate spanish day4_us_74e537ca:


    me "Nada. Podrías herirte."


translate spanish day4_us_aae12bf3:


    us "¿Te estás preocupando por mi?"


translate spanish day4_us_dd02f8d8:


    me "Pues claro que lo estoy. Quiero decir, eso es lo normal en semejante situación."


translate spanish day4_us_c0584c4d:


    "Ulyana hizo un puchero."


translate spanish day4_us_17717ee8:


    me "Escucha..."


translate spanish day4_us_6ba67613:


    "Decidí continuar con nuestra conversación... es más relajante y menos alarmante así."


translate spanish day4_us_9d0c5198:


    me "¿Qué pasa con ese antiguo campamento? Dijiste antes allí, en la plaza..."


translate spanish day4_us_87d3ffe3:


    us "Claro, ¡es en verdad un lugar que da miedo! Dicen que todos los pioneros murieron allí y se volvieron fantasmas los cuales lo guardan como último refugio terrenal."


translate spanish day4_us_7db7b174:


    me "¿De qué murieron?"


translate spanish day4_us_3d71a838:


    "Era bastante difícil creerse sus historias de miedo."


translate spanish day4_us_f62f4e9c:


    us "¿Cómo iba a saberlo? No había nacido siquiera entonces."


translate spanish day4_us_622c73dd:


    me "Pero lo dijiste con confianza."


translate spanish day4_us_2585e19d:


    us "¡Obtengo mi información de fuentes muy fiables!"


translate spanish day4_us_578a42ac:


    me "¿Y de dónde son ésas? ¿De Alisa?"


translate spanish day4_us_ccb042d0:


    us "¡No te lo voy a decir!"


translate spanish day4_us_fa7e457b:


    me "Bien, ¿y qué fue lo siguiente? ¿Murieron y se volvieron fantasmas?"


translate spanish day4_us_38197147:


    us "Eso es todo..."


translate spanish day4_us_974a0ad3:


    me "¿Qué quieres decir conque «eso es todo»?"


translate spanish day4_us_2dd06aae:


    us "Y ahora las almas de los pioneros muertos, vagan por el campamento y se llevan a todos los que se atreven a entrar... ¡al mundo de los muertos!"


translate spanish day4_us_7ba98731:


    me "¡Guau! Vale, ¡avancemos!"


translate spanish day4_us_ce617998:


    "..."


translate spanish day4_us_71ed9190:


    "El tiempo pasó. Nos adentramos en las profundidades del bosque donde los árboles lo cercan todo. De repente me di cuenta de que el bosque se había silenciado completamente."


translate spanish day4_us_3e13ee96:


    "Como si los pájaros nocturnos se hubieran escondido esperando a alguna cosa y los insectos hubieran escarvado en el suelo. Incluso el viento se había extinguido."


translate spanish day4_us_25d125dd:


    "Imaginé que los rayos de la luz lunar penetraban entre los abundantes follajes, sonando como cuerdas tensadas."


translate spanish day4_us_171ac87d:


    "Al fin los árboles se apartaron y caminamos por un gran claro..."


translate spanish day4_us_609185f4:


    "En medio de ello había un antiguo edificio similar a un jardín de parvulitos."


translate spanish day4_us_56ad6a30:


    "Estaba amortajado en una pesada neblina. Parecía como si hubiéramos llegado a un cementerio, y el antiguo edificio del campamento en su centro como una cripta."


translate spanish day4_us_883f8d73:


    "De acuerdo con Ulyana, había almas de pioneros vagando por aquí. La verdad es que era como una gran sepultura..."


translate spanish day4_us_b8d90de9:


    "Me dio un escalofrío y aferré con más fuerza la linterna."


translate spanish day4_us_b92c533e:


    me "Un espantoso lugar a decir verdad..."


translate spanish day4_us_4b95ee74:


    us "¡Oh, venga ya!"


translate spanish day4_us_632560ea:


    "Me dio una palmadita en la espalda alegremente, lo cual me asustó más."


translate spanish day4_us_96025918:


    "Iba a avanzar cuando la luna se mostró de entre las nubes, iluminando el claro, el antiguo edificio del campamento y a nosotros."


translate spanish day4_us_d383d5fe:


    "Bajo aquella luz todo parecía no tan antiguo. El derruido trabajo de albañilería, el oxidado tobogán y el tiovivo, los pocos vidrios milagrosamente en pie de algunas ventanas, todos ellos se volvieron más vívidos."


translate spanish day4_us_90df7238:


    "Comencé a imaginarme que desconocidos monstruos surgían hacia el claro desde el bosque, un lugar de eterna oscuridad donde incluso la luna no podía reinar."


translate spanish day4_us_97e4fa69:


    th "Espero que les asuste la luz, como a los vampiros...{w} A pesar de todo, tal vez pudieran convertirse en lobos gigantes con la luna llena, como los hombres lobo..."


translate spanish day4_us_bc306349:


    us "¿Por qué te has detenido?"


translate spanish day4_us_1bb0a12c:


    me "Estoy pensando..."


translate spanish day4_us_6a81728a:


    us "¿Sobre qué?"


translate spanish day4_us_61fe8457:


    me "¿Por qué iría a querer venir Shurik a semejante lugar?"


translate spanish day4_us_5fd5e401:


    us "¿Cómo voy a saberlo? Se lo preguntaremos cuando le encontremos."


translate spanish day4_us_ab70605d:


    me "Sí. Obviamente..."


translate spanish day4_us_60414457:


    "Murmuré y seguí a Ulyana."


translate spanish day4_us_2a3004d5:


    "Ahora ella caminaba con más cuidado, mirando bajo sus pies, deteniéndose de vez en cuando e incluso echando un vistazo detrás a menudo."


translate spanish day4_us_39ff0700:


    "Eso es natural. En algunas partes la hierba le alcanzaba hasta la altura de su pecho y nadie hubiera sabido que habría en el suelo... pedazos de metal, piedras, vidrio hecho añicos..."


translate spanish day4_us_87203d37:


    "Finalmente alcanzamos la puerta. Ulyana se detuvo y dijo:"


translate spanish day4_us_3110f8c7:


    us "¡Bueno! ¡Allá vamos!"


translate spanish day4_us_af4772b5:


    me "Como si hubieras ganado una carrera... Esto no es un juego."


translate spanish day4_us_05780ff1:


    us "Tú eres... eres..."


translate spanish day4_us_4e7dfaf3:


    me "¿Qué?"


translate spanish day4_us_024305f8:


    us "¡Aburrido!"


translate spanish day4_us_f39b4d19:


    "Hice una cara de disgusto y di un paso con confianza en la oscuridad. Era peligroso dejar que fuera Ulyana quien fuera primero, peligroso para ella, peligroso para mi y quizás incluso para toda la humanidad."


translate spanish day4_us_ec2f3403:


    "El interior del antiguo edificio del campamento me dio una mayor impresión deprimente si cabe. Hasta me sentí triste por un instante.{w} O probablemente por la gente que vivió aquí hace tiempo atrás."


translate spanish day4_us_e2350d58:


    "Seguramente había estado lleno de la alegría de los niños en el pasado. Pioneros corriendo por los alrededores, jugando a juegos que olvidé, una líder estricta como Olga Dmitrievna manteniendo el orden. Una temporada terminada, otra nueva comenzaba."


translate spanish day4_us_b4495206:


    "Y ahora estaba solamente ahí, derruyéndose, pudriéndose y quedando en el olvido."


translate spanish day4_us_30f4be0c:


    us "¡Mira!"


translate spanish day4_us_ca4f2dbb:


    "Ulyana me dio una muñeca vieja y rota, cayéndose a pedazos debido a la humedad. Unos pedazos más del pasado."


translate spanish day4_us_9bfb0137:


    me "¿Y qué?"


translate spanish day4_us_3005a190:


    us "Nada..."


translate spanish day4_us_8a70ba7f:


    "Se apartó de la luz, pero me percaté de su pena en su rostro."


translate spanish day4_us_4280984e:


    me "Tan triste como un cementerio."


translate spanish day4_us_614210cb:


    us "¡Espantoso!"


translate spanish day4_us_64a3eba4:


    me "No he visto nada que sea espantoso aun."


translate spanish day4_us_fbc2f69e:


    "Claramente se estaba más relajado aquí dentro que fuera."


translate spanish day4_us_a9de51da:


    "En verdad era como un cementerio, como cuando buscas una sepultura mientras caminas entre inumerables lápidas, sintiendo malestar dentro, pero entonces la hallas y tu alma se tranquiliza.{w} Como si te acostaras cerca de ella..."


translate spanish day4_us_311704a0:


    "Me estremecí y miré con la linterna alrededor de la habitación.{w} No había señal de Shurik."


translate spanish day4_us_d6c1b117:


    "Sin embargo, ¿qué esperaba hallar aquí? ¿Su cadáver?"


translate spanish day4_us_7b1fd8de:


    me "Parece que no está aquí..."


translate spanish day4_us_36335213:


    us "¿Qué tal el segundo piso?"


translate spanish day4_us_b01a7f13:


    me "¡Shurik!"


translate spanish day4_us_bf4be854:


    "Llamé a gritos, pero solamente mi eco me respondió."


translate spanish day4_us_1f4ba85c:


    us "¡Shurik! ¡Sal ya!"


translate spanish day4_us_8a79c954:


    me "Lo ves..."


translate spanish day4_us_883708ad:


    us "Deberíamos comprobarlo igualmente."


translate spanish day4_us_53c9fe63:


    me "Vale, vale..."


translate spanish day4_us_ce617998_1:


    "..."


translate spanish day4_us_784add86:


    "Tampoco había señales de vida en el segundo piso."


translate spanish day4_us_ae6a6307:


    "Me senté en las escaleras y dejé caer mi cabeza, sintiéndome desafortunado."


translate spanish day4_us_080b0b24:


    me "¿Y a dónde ahora? No podemos estar caminando por todo el bosque. Y tengo ya sueño..."


translate spanish day4_us_66088108:


    us "¿Por qué te quejas tanto?"


translate spanish day4_us_8f18aa98:


    "Ulyana estaba irritada."


translate spanish day4_us_8285506c:


    us "Quejas y quejas. Como un crío."


translate spanish day4_us_f75ea3f6:


    me "Oh, ¿acaso estoy equivocado?"


translate spanish day4_us_d92dc560:


    us "Tengas razón o no... ¿qué importa? Si estamos buscando a Shurik, ¡tenemos que hallarlo!"


translate spanish day4_us_04098bfe:


    me "¿Pero cómo?"


translate spanish day4_us_a43ed67b:


    "Supliqué."


translate spanish day4_us_78f6e14d:


    us "¡No lo sé! ¡De alguna forma!"


translate spanish day4_us_add3ace1:


    "Ulyana se parecía a una estricta profesora y yo a un descuidado niño de escuela."


translate spanish day4_us_1a536bbc:


    th "¿Pero no debería ser al revés?"


translate spanish day4_us_30f4be0c_1:


    us "¡Mira!"


translate spanish day4_us_0939ec56:


    "Enfoqué con la luz en la dirección que ella me señaló y me percaté de una trampilla en el rincón, rodeada de porquería.{w} Parecía que había sido abierta no hace mucho tiempo."


translate spanish day4_us_4e111b28:


    us "¡Debe de estar ahí abajo seguramente!"


translate spanish day4_us_ede7df57:


    "Ulyana salió corriendo hacia la trampilla e hizo un gran esfuerzo para abrirla."


translate spanish day4_us_92badaea:


    me "¿Por qué iba a ir ahí abajo? Eso es probablemente... No lo sé. Algunos aldeanos. ¿Hay alguna aldea cerca?"


translate spanish day4_us_930ac3c9:


    us "No lo sé..."


translate spanish day4_us_cb4ca95f:


    "Ulyana respondió, respirando profundamente.{w} No podía arreglárselas para abrir la trampilla."


translate spanish day4_us_89141735:


    "Comencé a pensar."


translate spanish day4_us_2f3a091c:


    "Había seguramente una posibilidad de que Shurik pudiera estar ahí."


translate spanish day4_us_b73efd8f:


    "Desconocía muchas cosas de este mundo después de todo. En realidad, no sabía nada. ¿Por qué iban los lugareños a comportarse de acuerdo a mi razonamiento?{w} ¿Podría ser que intentara escapar por ahí abajo, huyendo de los lobos?"


translate spanish day4_us_8e32b775:


    th "¿Aun así hay lobos? Quizá lechuzas."


translate spanish day4_us_9dc26b9a:


    me "Vale, vayamos a comprobarlo..."


translate spanish day4_us_a11b339c:


    "Tensé mis músculos y me las arreglé para abrir la trampilla."


translate spanish day4_us_93757775:


    "La puerta golpeó ruidosamente el agrietado suelo de madera y, a la vez, Ulyana se inclinó sobre el agujero de la trampilla, iluminando el sótano con la linterna."


translate spanish day4_us_369cfea8:


    us "¡Es algún tipo de túnel!"


translate spanish day4_us_631fa55f:


    me "¿Un túnel?"


translate spanish day4_us_b405ffe1:


    "La eché atrás agarrándola por el pescuezo, con el fin de ver ahí por mi mismo."


translate spanish day4_us_d5d98923:


    us "¡Ey!"


translate spanish day4_us_e4344f7f:


    me "¡La curiosidad mató al gato!"


translate spanish day4_us_8b032119:


    "Había de hecho un largo túnel perdiéndose en la oscuridad. Se parecía a una mazmorra de un videojuego."


translate spanish day4_us_53694ead:


    "No había ningún peligro a primera vista, ni agua que llegase a la cintura, ni ratas, ni zombis..."


translate spanish day4_us_4fbc46ec:


    me "Vale. Descendamos por ahí a ver. ¡Pero con mucho cuidado!"


translate spanish day4_us_c0841288:


    us "¡Roger!"


translate spanish day4_us_b8db1da9:


    "Ulyana está radiante."


translate spanish day4_us_ce617998_2:


    "..."


translate spanish day4_us_c2440a9c:


    "En verdad estaba oscuro ahí abajo. La linterna servía de poca ayuda. Iluminaba tenuemente las paredes de cimientos, bombillas en apliques colgaban por el techo, enmarañadas en cables y abundante porquería cubriendo el suelo."


translate spanish day4_us_34477885:


    "Lentamente avanzamos. Agarré la mano de Ulyana, temiendo que saliera corriendo por delante de mí."


translate spanish day4_us_f835b5ac:


    "Probablemente estaba un poco preocupado por mí mismo también, no queriendo acabar solo en esta espesa oscuridad."


translate spanish day4_us_c6aac5b3:


    me "¿Tal vez deberíamos volver? Shurik no puede haber ido tan lejos..."


translate spanish day4_us_951106fa:


    us "¿Y si él tenía una linterna?"


translate spanish day4_us_8a8c5c02:


    me "Bueno, incluso con una linterna... ¿Qué estaría haciendo él aquí?"


translate spanish day4_us_3e242075:


    us "¡No lo sé! ¿No estás interesado en lo que pueda haber delante?"


translate spanish day4_us_990233c8:


    me "Ni un ápice, estoy más interesado en..."


translate spanish day4_us_54c2445e:


    "No terminé de decir mi enunciado... que una enorme puerta de metal apareció repentinamente ante nosotros como si de la nada."


translate spanish day4_us_87a07145:


    "Al instante me di cuenta de la señal de peligro biológico."


translate spanish day4_us_28acc263:


    me "¿Un refugio antiaéreo?"


translate spanish day4_us_5211f0b6:


    "Averigüé."


translate spanish day4_us_41e59a3d:


    us "Probablemente."


translate spanish day4_us_171958cc:


    me "¿Has escuchado algo sobre ello?"


translate spanish day4_us_28220b96:


    us "No sé. ¿Importa?"


translate spanish day4_us_4f8c4714:


    me "¿Cómo que...? ¿Y si hay radiación?"


translate spanish day4_us_7ef85af6:


    us "¿Qué radiación? ¿De dónde?"


translate spanish day4_us_38d6ffbb:


    me "Sí, tienes razón..."


translate spanish day4_us_9bb98464:


    "Agarré el pomo giratorio y traté de moverlo."


translate spanish day4_us_8cb0acfd:


    "Cedió, para mi sorpresa y gimió como un dinosaurio moribundo."


translate spanish day4_us_8bd067c0:


    "Al fin la puerta se abrió y Ulyana corrió dentro tan pronto como pudo."


translate spanish day4_us_95efcfe1:


    me "¡Ey!"


translate spanish day4_us_396dea3d:


    "Tras la puerta estaba probablemente la principal habitación del refugio."


translate spanish day4_us_deed307a:


    "Habían varias camas cerca de mí, algunos dispositivos cerca de la pared más alejada con taquillas. Las luces fluorescentes en el techo brillaban iluminando la habitación."


translate spanish day4_us_2b06fcd1:


    me "¿De dónde proviene la electricidad? ¿Hay generadores de respaldo funcionando todavía?"


translate spanish day4_us_3cb76044:


    "Apagué la linterna para ahorrar energía."


translate spanish day4_us_464f6f21:


    "Ulyana comenzó a hurgar entre las taquillas y armarios, sacando máscaras de gas, algunos paquetes y diversas herramientas."


translate spanish day4_us_1a51a1af:


    me "¿No tienes nada mejor que hacer?"


translate spanish day4_us_0dcdea24:


    us "¡No!"


translate spanish day4_us_8263b828:


    "Me miró con disgusto."


translate spanish day4_us_efbfc8f1:


    "Me senté en la cama bien hecha y miré a mi alrededor una vez más."


translate spanish day4_us_44a51345:


    "Justo en frente de mi en la habitación había una puerta, una igual que la anterior por la que entramos."


translate spanish day4_us_cac4f276:


    th "¿Podría ser que Shurik se hubiera ido por ahí (si es que estaba aquí)? ¿Y por qué?"


translate spanish day4_us_48e24bcd:


    us "¿Por qué haces esa cara tan larga?"


translate spanish day4_us_b89cbb7e:


    me "Solamente estoy cansado..."


translate spanish day4_us_9fb02d3f:


    "Confesé sinceramente."


translate spanish day4_us_f062a0a0:


    us "¡Entonces descansa por un rato!"


translate spanish day4_us_54ad0367:


    "Ulyana saltó encima mío y me empujó en el pecho."


translate spanish day4_us_b9943d3a:


    "Me tambaleé atrás sorprendido y me golpeé la cabeza contra la pared."


translate spanish day4_us_fc7f611f:


    me "¡Qué estás haciendo!"


translate spanish day4_us_8776d0b9:


    "Le agarré de sus brazos y la estiré hacia mi."


translate spanish day4_us_53d31d5c:


    "Perdió su equilibrio y cayó a mi lado."


translate spanish day4_us_ed5f24ac:


    us "¡Ay!"


translate spanish day4_us_60bb0a79:


    me "¡Tú empezaste esto!"


translate spanish day4_us_4b1f34cb:


    "Ulyana sacó su lengua y se sentó."


translate spanish day4_us_02aa8fe6:


    me "Bueno, ¿y ahora?"


translate spanish day4_us_2fa697bf:


    us "Por ahí, la otra puerta..."


translate spanish day4_us_00a82683:


    me "Oh, claro... Shurik predijo una guerra nuclear y optó por esconderse de antemano, ¿no?"


translate spanish day4_us_c399a64c:


    us "¡Quizás lo hizo!"


translate spanish day4_us_1759bd02:


    me "¿Quizás? Este refugio está demasiado cerca de la superficie. Podría proteger frente a la radiación, pero si la bomba cayera bastante cerca..."


translate spanish day4_us_3503d6d7:


    us "Eres demasiado serio con estos asuntos."


translate spanish day4_us_fde537c1:


    "Sonrió maliciosamente."


translate spanish day4_us_8a58cac1:


    "En verdad no sabía cuándo debía ser serio con ella y cuándo bromear."


translate spanish day4_us_9f4bfb7f:


    "Pareciera que la diferencia de edad entre nosotros realmente tuviera importancia."


translate spanish day4_us_a51509b7:


    th "¿Cuántos años tiene... 10 años, o más?"


translate spanish day4_us_1ab60ac4:


    "Suspiré y me levanté de la cama."


translate spanish day4_us_99949f0f:


    me "Vale. Intentémoslo."


translate spanish day4_us_fad4bb18:


    "Pero esta puerta fue menos dócil que la primera."


translate spanish day4_us_a23c0e66:


    "Los tornillos chirriaban, pero el pomo giratorio no se movió ni un centímetro."


translate spanish day4_us_9da6b067:


    me "Parece atascada..."


translate spanish day4_us_afc3494c:


    us "¡Déjame!"


translate spanish day4_us_76c86e05:


    "Ulyana se lanzó rápidamente a la puerta con un pasamanos que cogió de vete a saber dónde, y lo usó de palanca con todo su mediocre peso."


translate spanish day4_us_d0cada00:


    "Eso nos dio una oportunidad. La ayudé y pronto la puerta cayó sobre el suelo con un golpe."


translate spanish day4_us_50d5d9d2:


    "Las bisagras resultaron estar completamente oxidadas."


translate spanish day4_us_0c596808:


    th "Parece que no todo fue hecho para durar desde {i}entonces{/i}."


translate spanish day4_us_1df933d0:


    "Tras la puerta había un túnel casi como el mismo de antes por el que habíamos venido hasta aquí."


translate spanish day4_us_4bb12c41:


    me "¡No hagas nada estúpido!"


translate spanish day4_us_99685367:


    "Agarré a Ulyana de la mano y di un paso saliendo de la habitación."


translate spanish day4_us_ce617998_3:


    "..."


translate spanish day4_us_8ea41ecb:


    "Era otro túnel sin final."


translate spanish day4_us_83e78ba1:


    "El techo parecía tornarse más bajo, a pesar de que incluso me diera cuenta, lógicamente, de que no cambiara."


translate spanish day4_us_4b91848a:


    "Ulyana no parecía estar molesta para nada, a pesar de todo. Iba canturreando mientras caminaba."


translate spanish day4_us_6ed9af94:


    "Eso me enojaba más y más."


translate spanish day4_us_7a841c5f:


    me "Parece que te lo estás pasando divertido..."


translate spanish day4_us_83eff6be:


    us "¡Pues claro! ¿Tú no?"


translate spanish day4_us_7a50435b:


    me "No. No lo encuentro divertido. ¡Deberíamos hallar a Shurik rápidamente y salir de aquí!"


translate spanish day4_us_d877fb1e:


    us "Puede que no esté aquí después de todo."


translate spanish day4_us_94eb0d42:


    me "¿Entonces por qué demonios...?"


translate spanish day4_us_30f4be0c_2:


    us "¡Mira!"


translate spanish day4_us_4f298886:


    "Ulyana me quitó la linterna de mis manos."


translate spanish day4_us_bb4d07b2:


    "Había a un par de metros frente a nosotros un gran agujero en el suelo."


translate spanish day4_us_3fb17507:


    us "¡Puede que él esté aquí abajo!"


translate spanish day4_us_974d3351:


    "Caminó hasta el borde y se agachó."


translate spanish day4_us_3e125f8d:


    us "Hay raíles..."


translate spanish day4_us_00e5898d:


    "Parecía que había una mina bajo este túnel."


translate spanish day4_us_3dd043c8:


    "La profundidad del agujero era lo suficientemente baja como para permitirnos treparlo, así que ya sabía qué diría Ulyana."


translate spanish day4_us_2e1a85d1:


    us "¡Vamos allá!"


translate spanish day4_us_70ddfdf6:


    "Quería oponerme, pero ella había saltado abajo, abandonándome en la completa oscuridad."


translate spanish day4_us_95efcfe1_1:


    me "¡Ey!"


translate spanish day4_us_e539b0ce:


    "Tenía que seguirla."


translate spanish day4_us_98c927dd:


    "No sé qué excavaban aquí, pero la mina había sido abandonada hacía mucho tiempo. Las vigas se habían humedecido, los raíles se oxidaron, la tierra se esparció a través de las paredes en algunos lugares."


translate spanish day4_us_ac3a62f7:


    "El túnel entero, perdiéndose en lo desconocido, era sospechoso. Parecía como si fuera a derrumbarse y a enterrarnos."


translate spanish day4_us_da8e6eb5:


    us "¡Vamos!"


translate spanish day4_us_e1d9394c:


    "Ulyana tiraba desesperadamente de mi brazo."


translate spanish day4_us_20e92b26:


    me "¿Adónde? ¿Por qué? ¿Qué estaría haciendo Shurik aquí?"


translate spanish day4_us_46a40361:


    us "¡¿Y si...?!"


translate spanish day4_us_cfc2bc6d:


    "Puso una cara muy seria."


translate spanish day4_us_0c35b5c3:


    us "¡¿Y si él está sentado aquí en alguna parte, herido y esperando ayuda, y nosotros simplemente nos vamos, abandonándolo a una muerte segura...?!"


translate spanish day4_us_55b2a7b1:


    "Estimé la altura del techo una vez más y caminé lentamente detrás de la nerviosa Ulyana."


translate spanish day4_us_ce617998_4:


    "..."


translate spanish day4_us_b17c36b3:


    "Pronto alcanzamos una bifurcación."


translate spanish day4_us_830688df:


    us "¡Vayamos hacia la derecha!"


translate spanish day4_us_c6dd4736:


    me "¡Espera!"


translate spanish day4_us_36ea64e3:


    "Le agarré del brazo."


translate spanish day4_us_5da43ef2:


    us "¿Qué pasa?"


translate spanish day4_us_3688cbd4:


    me "¿Y si es un callejón sin salida? O algo peor que un callejón sin salida... ¿un completo laberinto?"


translate spanish day4_us_52f35a85:


    us "Bueno..."


translate spanish day4_us_60f3d758:


    "Se puso a pensar."


translate spanish day4_us_c86b37ad:


    us "¡Pues marquemos el punto inicial!"


translate spanish day4_us_8083c22c:


    "Ulyana cogió una piedra grande del suelo y marcó una cruz en una de las vigas que sostenían el techo."


translate spanish day4_us_f8d84cb9:


    me "¿Crees que eso nos ayudará?"


translate spanish day4_us_3e60cbf2:


    us "¡Lo hará!"


translate spanish day4_us_511d63f8:


    th "Probablemente tendré que escoger hacia dónde ir. No puedo dejárselo simplemente a una pequeña muchacha."


translate spanish us_mine_coalface_e40597aa:


    "Ya habíamos estado aquí."


translate spanish us_mine_coalface_5b4c3c90:


    "Finalmente atravesamos el túnel y llegamos a una sala extensa con un techo elevado."


translate spanish us_mine_coalface_3ef8c583:


    "Aunque a pesar de todo difícilmente se le podría llamar una sala; debían haber solido extraer alguna cosa aquí.{w} Carbón, probablemente, o tal vez oro."


translate spanish us_mine_coalface_3c54fe69:


    "Las paredes habían sido talladas con picos o martillos neumáticos."


translate spanish us_mine_coalface_c718c063:


    "Este lugar era negro como el alquitrán, por lo que nuestra única salvación era la linterna."


translate spanish us_mine_coalface_b97863a3:


    th "Si se rompiera, es improbable que pudiéramos escapar de aquí..."


translate spanish us_mine_coalface_58411121:


    "En un rayo de iluminación, localicé un pedazo de ropa roja en un rincón."


translate spanish us_mine_coalface_ca0a4f90:


    "¡Eso era un pañuelo de pionero!"


translate spanish us_mine_coalface_96b3272c:


    "Shurik estaba obviamente aquí en alguna parte."


translate spanish us_mine_coalface_93ec21f7:


    me "¡Shurik! ¡Shurik!"


translate spanish us_mine_coalface_455d9f62:


    us "¡Shurik!"


translate spanish us_mine_coalface_b99be3e2:


    "Solamente nuestro eco nos respondió."


translate spanish us_mine_coalface_15e63d5a:


    me "Espero que esté bien..."


translate spanish us_mine_coalface_f87c925c:


    us "No te preocupes, ¡lo encontraremos!"


translate spanish us_mine_coalface_a411960f:


    th "¿Pero a dónde podría haber ido?"


translate spanish us_mine_coalface_686bf61c:


    "Esta sala no tenía otra salida..."


translate spanish us_mine_coalface_7ea80226:


    th "Es posible que hayan algunos lugares en estos túneles los cuales no hemos visitado..."


translate spanish us_mine_coalface_858b6cbe:


    th "¡Parece que tendremos que seguir buscándole!"


translate spanish us_mine_exit_26c6312e:


    "Al fin, gracias al foco de luz de la linterna se mostró una vieja puerta de madera en la oscuridad."


translate spanish us_mine_exit_0e7f3bff:


    us "¡Allá vamos!"


translate spanish us_mine_exit_25f7abbc:


    me "¿Adónde?"


translate spanish us_mine_exit_03e083fb:


    us "A alguna parte. No lo sé."


translate spanish us_mine_exit_e2cbe440:


    "Pero estaba en lo cierto en una cosa... al menos saldríamos del laberinto."


translate spanish us_mine_exit_1467f5ac:


    "Tras todas estas esquinas y bifurcaciones, no es tan seguro que pudiéramos volver, por otra parte, ¿por qué no iba a tener esta mina alguna salida?"


translate spanish us_mine_exit_5ac980a3:


    "Ulyana abrió la puerta y miró fijamente en la oscuridad."


translate spanish us_mine_exit_6acdac4b:


    me "Entonces, ¿no irás primera como de costumbre?"


translate spanish us_mine_exit_52f35a85:


    us "Bueno..."


translate spanish us_mine_exit_01822020:


    me "Vale."


translate spanish us_mine_exit_02157de8:


    "Di un paso en el umbral."


translate spanish us_mine_exit_80505c24:


    "Una pequeña habitación se hallaba tras la puerta... quizá una habitación de trastos del refugio antiaéreo."


translate spanish us_mine_exit_c7136184:


    "Había botellas y colillas de cigarrillos, lo cual significa que alguien estuvo aquí antes de nosotros."


translate spanish us_mine_exit_ecb7a6ba:


    "No era un hecho muy alentador por sí mismo, pero ahora estaba seguro de que había otra salida de la mina (ya que {i}ellos{/i} no pudieron venir desde el mismo camino en que lo hicimos nosotros)."


translate spanish us_mine_exit_56d7df1e:


    "Registré con el foco de luz de la linterna la habitación, examinando cualquier rincón. De repente se iluminó una figura humana..."


translate spanish us_mine_exit_1925a7ca:


    "¡Era Shurik, acurrucado contra una de las paredes!"


translate spanish us_mine_exit_72821d25:


    me "¡Ey! ¡Ahí estás! Te hemos estado buscando durante toda la noche y tú..."


translate spanish us_mine_exit_212f5a54:


    "Parecía que no se hubiera dado cuenta de nosotros, tan sólo sentado y murmurando cosas."


translate spanish us_mine_exit_b01a7f13:


    me "¡Shurik!"


translate spanish us_mine_exit_58f9ccc4:


    sh "Quién... ¿Quién es?"


translate spanish us_mine_exit_21a8f802:


    me "¡¿Qué quieres decir con «quién»?! ¡El equipo de rescate! ¡Levántate y vayámonos!"


translate spanish us_mine_exit_42731173:


    sh "¡No iré a ninguna parte con vosotros!"


translate spanish us_mine_exit_7c4bde06:


    "Murmuró."


translate spanish us_mine_exit_71df32d3:


    sh "Me llevaréis por estos túneles otra vez, ¡lo sé! ¡No iré a ninguna parte! ¡Me quedaré aquí mismo, no me atraparéis!"


translate spanish us_mine_exit_1882e4f8:


    me "¡Para ya con ese sin sentido!"


translate spanish us_mine_exit_c5c0b112:


    "Parece que se volvió loco."


translate spanish us_mine_exit_aeb6a5a7:


    sh "¡No, no! No me engañaréis esta vez."


translate spanish us_mine_exit_8b610151:


    me "Que pares ya..."


translate spanish us_mine_exit_9a85ecb6:


    "Di unos pocos pasos hacia Shurik, pero saltó de golpe y blandió un pasamanos de metal."


translate spanish us_mine_exit_1817dc02:


    sh "¡No os acerquéis a mi! ¡Dejadme solo!"


translate spanish us_mine_exit_bf9a997b:


    me "Tranquilízate! Soy yo, ¡Semyon! ¿No me reconoces?"


translate spanish us_mine_exit_9f720364:


    sh "¿Semyon...? ¡No, tú no eres Semyon!"


translate spanish us_mine_exit_48892acd:


    "Me percaté que Ulyana, que estaba a mi lado, había desaparecido en alguna parte."


translate spanish us_mine_exit_dd35a3e1:


    sh "Tú no eres Semyon y voy a..."


translate spanish us_mine_exit_b64a44f7:


    "En la parpadeante luz la mano de Shurik apareció en un instante, blandiendo el pasamanos de metal. Me cubrí la cabeza instintivamente."


translate spanish us_mine_exit_f5b87a6f:


    "Nada."


translate spanish us_mine_exit_2533f390:


    "Cuando abrí mis ojos, él ya había desaparecido. Ulyana permanecía a mi lado con una risita, sosteniendo el pasamanos de metal."


translate spanish us_mine_exit_af36de66:


    us "¡Justo como un centinela!"


translate spanish us_mine_exit_4c03fe75:


    me "Claro, un centinela..."


translate spanish us_mine_exit_19ad538e:


    "Desde alguna parte bien lejos se escuchó la risa endiablada de Shurik."


translate spanish us_mine_exit_92bb7ff9:


    us "Salió corriendo..."


translate spanish us_mine_exit_d0833b8f:


    me "Déjale. ¡No me preocupa si se muere aquí!"


translate spanish us_mine_exit_ff689675:


    "Escupí al suelo y me apoyé en la pared."


translate spanish us_mine_exit_3429bb5b:


    th "Si no hubiera sido por Ulyana..."


translate spanish us_mine_exit_c05d018c:


    "¡No estaba preparado para eso!{w} Shurik puede que no me hubiera matado, ¡pero podría haberme herido gravemente!"


translate spanish us_mine_exit_95c2fa20:


    "Quedarme aquí herido sería lo mismo que morir. Tampoco sabemos cuándo llegaría la ayuda.{w} ¿Y podrían acaso hallarme en este laberinto?"


translate spanish us_mine_exit_9eb821b3:


    "¿Por qué demonios estuve de acuerdo en venir aquí?{w} Muchacha consentida..."


translate spanish us_mine_exit_85b29f3a:


    us "Parece que vayas a matar a alguien."


translate spanish us_mine_exit_058d6daa:


    me "Si algún candidato se presenta voluntario."


translate spanish us_mine_exit_07e9d3d8:


    "Ulyana se estremeció."


translate spanish us_mine_exit_dbe0f3c1:


    me "No, tú no. Puede ser bonito darte un azote, pero no hay motivo para matarte."


translate spanish us_mine_exit_f0d6d1c8:


    "Ella se rio pícara."


translate spanish us_mine_exit_b16368a6:


    me "Aun..."


translate spanish us_mine_exit_50d4d0ba:


    us "¡Oh, venga ya!"


translate spanish us_mine_exit_850926ff:


    me "Vale. ¡Es claramente el momento de irse de aquí! Pueden enviar sus fuerzas especiales, equipos de rescate, cazafantasmas, lo que sea... mañana. Me trae sin cuidado."


translate spanish us_mine_exit_a661ea5a:


    us "¿Volvemos atrás?"


translate spanish us_mine_exit_479f496b:


    "Miré alrededor de la habitación una vez más y me di cuenta de una puerta a mi izquierda."


translate spanish us_mine_exit_c2e764e0:


    me "Guau."


translate spanish us_mine_exit_1a153a40:


    "La puerta era justo como en el refugio antiaéreo... una enorme de metal."


translate spanish us_mine_exit_e592b1be:


    "Empujé varias veces el pomo giratorio, pero solamente chirriaba débilmente."


translate spanish us_mine_exit_3e37cbde:


    th "Si tan sólo tuviera aquel pasamanos..."


translate spanish us_mine_exit_3324aa7a:


    us "¿No funciona?"


translate spanish us_mine_exit_0c87327f:


    "Ulyana preguntó con una voz desanimada."


translate spanish us_mine_exit_5cb822ee:


    me "No..."


translate spanish us_mine_exit_48ef60d6:


    "En realidad ya no me quedaban fuerzas."


translate spanish us_mine_exit_94802177:


    "En otra situación me esforzaría, le pediría ayuda a Ulyana, buscaría algo que usar como palanca, pero ahora tan solamente quería escapar de esta mina."


translate spanish us_mine_exit_85c47979:


    "Quería guardar la esperanza de que había otro camino que escoger que supusiera menos esfuerzo."


translate spanish us_mine_exit_334e8c15:


    "Y eso significaba tener esperanza de acordarme del camino que hicimos por el laberinto."


translate spanish us_mine_exit_3486283d:


    me "Volvamos atrás."


translate spanish us_mine_exit_89d3eb13:


    us "Vale."


translate spanish us_mine_exit_5f443c07:


    "Sonrió y me cogió de la mano."


translate spanish us_mine_exit_ce617998:


    "..."


translate spanish us_mine_exit_e26fdffd:


    "Caminamos incluso más lentos mientras retornábamos de vuelta por el laberinto."


translate spanish us_mine_exit_db3ba18f:


    "Las piedras se dispersaban bajo nuestros pies, el agua goteaba desde el techo sobre nuestras cabezas, sintiéndose como gotas de estaño fundido."


translate spanish us_mine_exit_dc5bc7a5:


    "Ulyana se tranquilizó y me siguió en silencio."


translate spanish us_mine_exit_ea63ce77:


    me "¿Ha ocurrido algo?"


translate spanish us_mine_exit_5f2c0f51:


    us "¿Qué quieres decir?"


translate spanish us_mine_exit_8a64dac8:


    me "Es raro en ti que te quedes callada durante más de un minuto."


translate spanish us_mine_exit_0242fc7b:


    us "No, todo va bien..."


translate spanish us_mine_exit_c417bbd2:


    "Pero algo iba mal, claramente."


translate spanish us_mine_exit_a92297a2:


    "Una bifurcación tras otra."


translate spanish us_mine_exit_04ad16d0:


    "Hace un minuto estaba seguro de que en la siguiente esquina vería la cruz que Ulyana había marcado antes. Estaba equivocado."


translate spanish us_mine_exit_d65de998:


    "Mi fe en mi habilidad para orientarnos se iba desmoronando con cada segundo."


translate spanish us_mine_exit_9e4e4c62:


    me "¡Bueno, da igual!"


translate spanish us_mine_exit_554c40e7:


    "Opté por distraerme con una conversación."


translate spanish us_mine_exit_6d9d0fd7:


    us "¡Está bien! Es solo que..."


translate spanish us_mine_exit_ad02f53a:


    me "¿Qué?"


translate spanish us_mine_exit_c6444665:


    us "Todo fue tan mal. Con Shurik. Y... ahora estamos atrapados aquí."


translate spanish us_mine_exit_a54e284d:


    me "Salvaste mi vida. Deberías estar orgullosa de ti misma."


translate spanish us_mine_exit_ccd2dd89:


    "Traté de animarla, pero Ulyana no pareció entenderlo."


translate spanish us_mine_exit_b1356ab0:


    us "Pero si no le hubiera quitado ese pasamanos de metal, quizá él se habría quedado quieto."


translate spanish us_mine_exit_51f70f47:


    me "Tanto si se hubiera quedado quieto como si no, ¿cuál es la diferencia ahora? Todavía tenemos que salir de aquí, ¿no?"


translate spanish us_mine_exit_c3e3f337:


    us "Sí, pero..."


translate spanish us_mine_exit_af351568:


    me "¡Todo va bien! Ese loco hallará una salida eso seguro."


translate spanish us_mine_exit_aa7fa335:


    "Realmente estaba convencido de ello."


translate spanish us_mine_exit_ce617998_1:


    "..."


translate spanish us_mine_exit_f7cc90bf:


    "Finalmente llegamos a un largo túnel, en la pared del cual estaba la cruz."


translate spanish us_mine_exit_3daa2b3d:


    "Ulyana se animó un poco y casi corrimos el resto del camino hasta la superficie."


translate spanish us_mine_exit_de2c6d73:


    me "¡Lo ves!"


translate spanish us_mine_exit_b88638a5:


    "La luna llena brilla por encima nuestro otra vez, y el edificio del antiguo campamento no parecía tan ominoso como antes.{w} Especialmente en comparación con el refugio antiaéreo y las catacumbas."


translate spanish us_mine_exit_6bffdd99:


    us "Fue divertido, ¿verdad?"


translate spanish us_mine_exit_53c7724e:


    "Parecía como si Ulyana hubiera recuperado su habitual alegría."


translate spanish us_mine_exit_edb2bc43:


    me "No estoy seguro de que sea «divertido», pero me alegro de que escapáramos."


translate spanish us_mine_exit_c6618fc6:


    us "Entonces, ¿debemos ir a buscar a Shurik?"


translate spanish us_mine_exit_67c5f2d1:


    me "¿Qué?"


translate spanish us_mine_exit_e113e81a:


    "Me quedé sin palabras por un instante, incapaz incluso de acabar la frase."


translate spanish us_mine_exit_3d6467aa:


    me "¡¿Estás loca?! ¡Ya le hemos encontrado! Mañana Olga Dmitrievna y la policía pueden ir ahí abajo y atrapar al cavernícola. Para hacerle las pruebas."


translate spanish us_mine_exit_3dc79f49:


    us "¡Bueeeeno!"


translate spanish us_mine_exit_6f3fc9d9:


    me "¡No buenos! ¡Vuelta al campamento! ¡A dormir!"


translate spanish us_mine_exit_07bb3c51:


    "Caminé rápidamente desde aquel lugar espantoso, ignorando la furiosa Ulyana."


translate spanish us_mine_exit_ce617998_2:


    "..."


translate spanish us_mine_exit_680a7e90:


    "En unos diez minutos estábamos de vuelta ya en la plaza."


translate spanish us_mine_exit_09a35d39:


    me "Vale. Eso es todo por hoy. ¡Rompan filas, soldados!"


translate spanish us_mine_exit_d8af262d:


    "Ulyana saludó y estaba a punto de irse, cuando de repente, chilló y comenzó a mover sus manos con entusiasmo."


translate spanish us_mine_exit_700adfc4:


    us "¡Mira! ¡Mira!"


translate spanish us_mine_exit_7047bfac:


    "Me giré hacia los bancos y vi a Shurik tumbado en uno de ellos."


translate spanish us_mine_exit_0f53a61e:


    me "Alucina..."


translate spanish us_mine_exit_11b6866f:


    "Era en verdad difícil de despertarle, como si el «cavernícola» hubiera decidido dormir durante un año por adelantado."


translate spanish us_mine_exit_68fdcd41:


    sh "¿Ah? ¿Qué? ¿Dónde estoy?"


translate spanish us_mine_exit_5d5d525a:


    "Murmuraba soñoliento."


translate spanish us_mine_exit_46b3bd18:


    me "¿Te quieres explicar ahora?"


translate spanish us_mine_exit_613a674f:


    sh "¿Explicar qué?"


translate spanish us_mine_exit_8cd1cf37:


    us "¡Hemos estado buscándote toda la noche! ¡Y saltaste encima de Semyon con un pasamanos de metal! ¡Y luego saliste corriendo!"


translate spanish us_mine_exit_c6875964:


    "Ulyana saltó hacia el banco, preparada para explotar de rabia."


translate spanish us_mine_exit_8e0eb312:


    sh "¿Qué sucedió? ¿Y por qué estoy aquí?"


translate spanish us_mine_exit_6e10baeb:


    "Shurik parecía recobrar sus sentidos."


translate spanish us_mine_exit_6891b2b4:


    me "¡Serías tan amable de explicarnos eso! ¡Cómo escapaste de la mina! En primer lugar, ¿por qué fuiste allí? Todo, ¡punto a punto!"


translate spanish us_mine_exit_517dc919:


    sh "¿Qué mina?"


translate spanish us_mine_exit_2dc7c7d4:


    "Había cierta sinceridad de sorpresa en sus ojos que me empezó hacer dudar.{w} Realmente puede que no recuerde nada."


translate spanish us_mine_exit_70974d60:


    me "¿Dónde has estado en las últimas... doce horas?"


translate spanish us_mine_exit_bbe83e3a:


    sh "No lo sé..."


translate spanish us_mine_exit_1afdf66f:


    "Shurik se sentó y su cara frunció el ceño con el esfuerzo de pensar."


translate spanish us_mine_exit_31d31d38:


    sh "Fui por la mañana al antiguo campamento. La gente dice que allí había algún tipo de equipo viejo. Para partes del robot. Y..."


translate spanish us_mine_exit_5d2d6c1c:


    "Nos miró fijamente confundido."


translate spanish us_mine_exit_f95c9fc9:


    me "¿Y?"


translate spanish us_mine_exit_80c68aa7:


    sh "Y eso es todo..."


translate spanish us_mine_exit_a7156351:


    me "Por lo que... ¿no lo recuerdas?"


translate spanish us_mine_exit_f8cef8d3:


    sh "No lo recuerdo."


translate spanish us_mine_exit_855db9ee:


    me "Vale."


translate spanish us_mine_exit_303044ab:


    "Me senté a su lado y me incliné apoyando la espalda en el banco."


translate spanish us_mine_exit_6472a1a6:


    "Las estrellas brillaban en el cielo."


translate spanish us_mine_exit_fe87209a:


    "Ellas recuerdan todo.{w} Incluso lo que Shurik estaba haciendo en el refugio antiaéreo."


translate spanish us_mine_exit_4ae29059:


    me "Estrés postraumático."


translate spanish us_mine_exit_80f898e3:


    us "Póster... ¿qué?"


translate spanish us_mine_exit_470d1a54:


    sh "Semejantes síntomas son experimentados por las personas tras un estrés intenso. Después de una catástrofe por ejemplo."


translate spanish us_mine_exit_ca0e1330:


    "Shurik remarcó con una mirada inteligente en su rostro."


translate spanish us_mine_exit_9f48d48f:


    me "Deberías dormir ahora."


translate spanish us_mine_exit_035cd0d4:


    sh "Sí, pero..."


translate spanish us_mine_exit_7241624a:


    me "Hablaremos mañana."


translate spanish us_mine_exit_c2ac9dce:


    "Shurik me miró por un rato, pero luego se levantó y lentamente caminando, se fue a su cabaña sin decir una palabra."


translate spanish us_mine_exit_f2233194:


    us "¿Y qué pasa con él?"


translate spanish us_mine_exit_f96a54b5:


    me "Probablemente olvidó todo lo que ocurrió en la mina."


translate spanish us_mine_exit_356011d8:


    us "¡Está mintiendo!"


translate spanish us_mine_exit_ad59f734:


    me "Mira, ¿qué necesidad de mentir tiene él?"


translate spanish us_mine_exit_d58e2a3f:


    us "Porque así no tendría que responder sobre por qué trató de... atacarte... con un pasamanos de metal..."


translate spanish us_mine_exit_858becb4:


    "Dijo insegura."


translate spanish us_mine_exit_d8eaac03:


    me "No lo parece. ¿Importa ahora?"


translate spanish us_mine_exit_c9dba43d:


    us "¡Importa! ¡Debemos hallarle! ¡El criminal debe ser castigado!"


translate spanish us_mine_exit_823f9939:


    me "Si esa regla se te aplicara a ti, ya habrías sido puesta bajo arresto domiciliario hace mucho tiempo. O peor incluso."


translate spanish us_mine_exit_0d7a7ad1:


    us "¿Qué tiene eso que ver con esto? ¡No me abalanzo encima de las personas con un pasamanos!"


translate spanish us_mine_exit_2a2a9dda:


    me "No fue intencional."


translate spanish us_mine_exit_5ee2ab65:


    us "¡Miente!"


translate spanish us_mine_exit_7fd78ed0:


    me "{i}Puede{/i} que mienta."


translate spanish us_mine_exit_3aec35b1:


    "Estaba realmente exhausto tras hoy, especialmente esta noche, y ciertamente me daba igual si Shurik estaba diciendo la verdad o simplemente pretendía decirla."


translate spanish us_mine_exit_87c100aa:


    "Realmente parece que no recuerda nada."


translate spanish us_mine_exit_93078286:


    me "Me voy a dormir."


translate spanish us_mine_exit_be15443d:


    us "Pues..."


translate spanish us_mine_exit_3b19a06b:


    "Ulyana saltó y se puso de puntillas."


translate spanish us_mine_exit_e1a87c65:


    us "¡Buenas noches!"


translate spanish us_mine_exit_b5cc7e1c:


    me "Tú también..."


translate spanish us_mine_exit_3f096259:


    "No lo sé, parecía como si tuviera algo especial en la expresión de su cara en ese momento, no importa."


translate spanish us_mine_exit_0ee0f1a6:


    "La líder del campamento me había estado esperando en «casa»."


translate spanish us_mine_exit_523cb2dc:


    mt "Creía que no volverías."


translate spanish us_mine_exit_04cc2a2a:


    "Me habría esperado cualquier reacción de Olga Dmitrievna, salvo por esa."


translate spanish us_mine_exit_12012f33:


    mt "Desde que te fuiste con Ulyana."


translate spanish us_mine_exit_c4c1debf:


    me "¿No se supone que te preocupas por nosotros?"


translate spanish us_mine_exit_40fec304:


    mt "¿Por qué debería? Estás bien."


translate spanish us_mine_exit_d950ea90:


    me "Oh, pues vale..."


translate spanish us_mine_exit_adda73b6:


    "No tenía ni las fuerzas ni la voluntad de discutir con la líder, o hallar los motivos de su comportamiento."


translate spanish us_mine_exit_bc6a86e2:


    "Me quité la ropa y me arrastré hasta debajo de las sábanas."


translate spanish us_mine_exit_9f489f27:


    th "Es demasiado para una sola persona."


translate spanish us_mine_exit_262beff1:


    "Buscar a Shurik en las mazmorras era un trabajo para socorristas profesionales.{w} Hacerlo con Ulyana... para hombres dementes."


translate spanish us_mine_exit_54005ff3:


    th "Pero fue de alguna manera divertido."


translate spanish us_mine_exit_40413c26:


    "Me dormí con una sonrisa en mi cara..."


translate spanish day4_un_867359a2:


    me "No tengo por qué ir solo, ¿verdad?"


translate spanish day4_un_be1d7816:


    "Olga Dmitrievna reflexionó por un momento."


translate spanish day4_un_db0f7ce7:


    mt "Puede que tengas razón... Iremos juntos mañana."


translate spanish day4_un_c58ce529:


    "Durante los últimos minutos me di cuenta de que Lena tenía una extraña mirada en su rostro.{w} Como si quisiera decir algo, pero no se atreviera a ello."


translate spanish day4_un_08dadf4e:


    "Los pioneros comenzaron a dispersarse, como si se hubieran olvidado de Alisa y de la explosión."


translate spanish day4_un_958ec37b:


    "Incluso nuestra líder parecía tranquila y no reaccionó cuando la aspirante de terrorista abandonó la plaza, escondiéndose detrás de Ulyana."


translate spanish day4_un_63db6e89:


    mt "Deberíamos irnos también."


translate spanish day4_un_9513cd87:


    me "Sí..."


translate spanish day4_un_a5b61259:


    "La noche caía sobre el campamento."


translate spanish day4_un_feec15e3:


    "Solamente había un breve instante entre los primeros rayos del sol, poniéndose hasta quedarse en la completa oscuridad, aquí en el sur, o quizá en este mundo. Nunca se tiene suficiente tiempo para disfrutar de la variedad de colores de un ocaso."


translate spanish day4_un_04b50f79:


    "Era demasiado pronto para irse a la cama, pero la líder se fue caminando hacia la cabaña confiadamente, como si mentalmente me arrastrara con ella."


translate spanish day4_un_90487e63:


    me "Olga Dmitrievna, me daré un pequeño paseo."


translate spanish day4_un_5cb759d8:


    mt "Vale..."


translate spanish day4_un_ed75d184:


    "Me miró con atención pero no halló excusa que oponer, encogió sus hombros y siguió caminando."


translate spanish day4_un_e4483483:


    "Retorné a la plaza."


translate spanish day4_un_0fb3cb29:


    "Realmente no quería mirar de cerca el banal daño hecho a la estatua de Genda. Estaba exactamente en el centro del campamento.{w} Si no sabes adónde ir, deberías empezar por ahí."


translate spanish day4_un_df68a624:


    "Me senté en el banco y miré hacia el oeste. Me preguntaba si la Tierra {i}aquí{/i} giraba sobre su eje tal como debería.{w} O si había en realidad un norte o un sur..."


translate spanish day4_un_fbfbafbb:


    "Es difícil de decir.{w} En estos momentos no tenía ni idea de cómo comprobar las leyes fundamentales de la naturaleza."


translate spanish day4_un_e8479859:


    un "Hola..."


translate spanish day4_un_85361757:


    "Lena apareció a mi lado como si de la nada."


translate spanish day4_un_87696cce:


    me "Hola... ¿No puedes dormir?"


translate spanish day4_un_efd76d15:


    "Me miraba sorprendida."


translate spanish day4_un_28e80bef:


    me "Bueno, sí. Todavía es pronto..."


translate spanish day4_un_c67f7c91:


    un "¿Puedo sentarme?"


translate spanish day4_un_33712bb6:


    me "Claro, por supuesto, siéntate."


translate spanish day4_un_280592ba:


    "Me hice un poco a un lado."


translate spanish day4_un_cb2ccc7b:


    "Decir «un poco» era de hecho un eufemismo, en realidad me moví al final del banco."


translate spanish day4_un_626232a6:


    un "Gracias."


translate spanish day4_un_32c8bf7a:


    "Lena se sentó y miró al cielo como si se hubiera olvidado de mi."


translate spanish day4_un_9c9cf456:


    un "Qué triste..."


translate spanish day4_un_02347a38:


    me "¿Qué es triste?"


translate spanish day4_un_e7203d10:


    un "Que Shurik desapareciera."


translate spanish day4_un_8ad48a87:


    me "Sí, algo malo."


translate spanish day4_un_51dccb02:


    "Estaba tranquila como de costumbre, quedándose en silencio la mayor parte del tiempo; se ruborizaba y se sentía avergonzada sólo cuando tenía que hablar o hacer algo."


translate spanish day4_un_94abc2fa:


    "Ese mismo silencio, que podría ser considerado incómodo para muchas personas (incluyéndome a mi mismo), era bastante natural para ella."


translate spanish day4_un_d45c7222:


    "Difícilmente me podía imaginar a Lena haciendo un esfuerzo para escoger cuidadosamente las palabras correctas para iniciar una conversación o hacer un buen comentario, tratando de no parecer estúpida o, en su lugar, tratando de no parecer grosera como Alisa."


translate spanish day4_un_b00fea13:


    "Simplemente no podía compararla con nadie. Ella estaba sencillamente satisfecha de ser como era ella misma."


translate spanish day4_un_029e699d:


    "¿Significa eso que mis intentos de iniciar una conversación con ella pudieran ser vistos como torpes? ¿Que la expresión «ser amigos» pudiera ser interpretada como una intrusión en su vida privada?"


translate spanish day4_un_5e028b03:


    "Pero algo en esta muchacha me atraía."


translate spanish day4_un_0ec5dc2a:


    "Tal vez era su misterio, y ciertamente ella no carecía de aspecto o encanto femenino. No tenía una respuesta para eso."


translate spanish day4_un_309fa209:


    th "Eso es porque todavía no había sido acusado abiertamente de ser molesto..."


translate spanish day4_un_703b7283:


    me "¡Estoy seguro de que será hallado! ¿Cómo puedes escapar de un submarino?"


translate spanish day4_un_4f2afd96:


    "Lena no pareció entender la broma."


translate spanish day4_un_0b8134cd:


    "Este campamento solamente a mí le debe parecer un gran submarino."


translate spanish day4_un_58027c9c:


    un "Eso espero."


translate spanish day4_un_8cd244fa:


    me "Mañana Olga Dmitrievna llamará a la policía. ¡Seguro que le hallarán!"


translate spanish day4_un_86342ac5:


    un "Y si durante la noche..."


translate spanish day4_un_e57a9cb4:


    "Su expresión se volvió triste."


translate spanish day4_un_74c7f5fa:


    me "¿Y si algo le ocurriera a él?"


translate spanish day4_un_c95128dc:


    th "Solo, por la noche, en el bosque... ¡Cualquier cosa podría sucederle!"


translate spanish day4_un_765a6a1c:


    un "Debe sentirse solo."


translate spanish day4_un_3d219146:


    me "¡Nadie le obligó a ir allí!"


translate spanish day4_un_6a910ab9:


    un "¿Y si simplemente se perdió?"


translate spanish day4_un_2791ad53:


    me "No debería irse caminando al bosque solo."


translate spanish day4_un_63cda4c6:


    un "¡No sientes nada de lástima por él! Shurik puede que esté sentado allí completamente solo..."


translate spanish day4_un_dffcef38:


    me "Claro que siento pena por él..."


translate spanish day4_un_7f15ab29:


    "Me sentí avergonzado."


translate spanish day4_un_eda05f52:


    "En cualquier caso Lena tenía razón. Una persona estaba desaparecida."


translate spanish day4_un_3b718ed7:


    un "Cualquier cosa podría suceder durante la noche..."


translate spanish day4_un_5000641d:


    me "No vamos a ir a buscarle realmente ahora, ¿verdad?"


translate spanish day4_un_fac6df53:


    "Ella no respondió, mirando aun a lo lejos, donde los rayos del sol brillaban por encima de las copas de los viejos árboles, como si tratara de compartir un poco de su calidez con la gente."


translate spanish day4_un_2807b2ef:


    me "¿Crees de verdad que es una buena idea vagar por los alrededores del bosque en la oscuridad?"


translate spanish day4_un_b354da3c:


    un "Probablemente no."


translate spanish day4_un_e49a0145:


    "Por alguna razón estaba seguro de que era exactamente lo que ella pensaba."


translate spanish day4_un_2e282ea3:


    "Recientemente, me pareció empezar a comprender más a menudo a Lena incluso sin palabras.{w} Y ella parecía influir sobre mí psicológicamente, haciéndome estar de acuerdo con ella."


translate spanish day4_un_d9cc43d9:


    "El silencio de Lena era más informativo que cualquier parloteo o intento de persuasión."


translate spanish day4_un_b181f8ef:


    me "Ya le buscaron durante el día."


translate spanish day4_un_2b8cb61b:


    un "¿En todas partes?"


translate spanish day4_un_03f5817b:


    "Dejó de observar el ocaso y me miró a mí."


translate spanish day4_un_53cb4078:


    me "No lo sé. Creo que en todas partes."


translate spanish day4_un_076d7069:


    un "¿Y qué hay del antiguo campamento?"


translate spanish day4_un_3727cf80:


    "Por primera vez sus palabras sonaron con seguridad y no fueron vagas o indiferentes."


translate spanish day4_un_f405c42e:


    me "¿Dónde está? No tengo ni idea."


translate spanish day4_un_7ce714f3:


    un "Nos lo explicó Electronik."


translate spanish day4_un_ce3fa441:


    me "Bueno. Si le crees..."


translate spanish day4_un_55fee8b0:


    "Solté una risita estúpida, pero Lena seguía mirándome seriosamente."


translate spanish day4_un_092d430e:


    me "Claro, si no es demasiado lejos..."


translate spanish day4_un_c90cc27f:


    un "Entonces, ¿quieres ir?"


translate spanish day4_un_7bb63055:


    th "¡Por supuesto que no!"


translate spanish day4_un_6ee034ca:


    me "Podemos. Si es sólo ir y volver, rápidamente..."


translate spanish day4_un_b993fafe:


    un "Vale."


translate spanish day4_un_99baefd5:


    "Lena sonrió y me dio una linterna, que apareció de la nada."


translate spanish day4_un_34b3d127:


    me "Sí. Eso será muy útil..."


translate spanish day4_un_666cb784:


    th "¿Significa eso que ella se preparó de antemano?{w} ¿Y que nada dependía de mi?"


translate spanish day4_un_bdea403d:


    "Suspiré como si fuera desafortunado y me dirigí hacia el bosque con ella."


translate spanish day4_un_ce617998:


    "..."


translate spanish day4_un_d6e969ac:


    "La noche cayó sobre el campamento."


translate spanish day4_un_c621431e:


    "Caminamos lentamente. Lena estaba a mi lado, cerca pero no demasiado."


translate spanish day4_un_e8646f8d:


    "Era extraño, pero no se le veía preocupada por nada."


translate spanish day4_un_6f897bb8:


    th "Además, parecía no estar muy molesta por lo que estábamos haciendo, como si no estuviéramos caminando en el bosque por la noche, sino viendo una película con otras personas haciendo los papeles destacados."


translate spanish day4_un_99f8ee9b:


    "En realidad, Electronik dijo que el antiguo campamento no estaba muy lejos, y que si caminamos en línea recta, entonces difícilmente nos perderíamos."


translate spanish day4_un_f0457e58:


    "Tras unos minutos, estaba completamente inseguro de que camináramos en línea recta, tras unos pocos minutos más comenzó a parecer que sería un milagro para nosotros salir incluso de aquí."


translate spanish day4_un_794417f4:


    "Pero traté de no perder la calma frente a Lena, así que traté de caminar con ánimo."


translate spanish day4_un_67236665:


    "El bosque estaba lleno de silencio, de sombras titilantes y de destelleantes rayos lunares. La hierba susurraba silenciosamente bajo nuestros pies y las brancas crujían en nuestras cabezas."


translate spanish day4_un_cf17831d:


    "Los viejos alcornoques estaban en pie junto a los jóvenes abedules. Grandes setas emergían bajo los segundos, como si se quitaran sus grandes sombreros saludando."


translate spanish day4_un_3805c942:


    "En cualquier otro día, o mejor en cualquier otro momento del día, se habría visto realmente bello."


translate spanish day4_un_3e7291b0:


    "Puede que sea seguro por la noche también, pero no obstante me estremecía con cada ráfaga de aire."


translate spanish day4_un_d5312815:


    un "Mira."


translate spanish day4_un_2b42b39e:


    "Lena indicó más adelante. Me froté los ojos y vi una grieta entre los árboles."


translate spanish day4_un_c1f40a14:


    "En un minuto estábamos en un gran claro. En medio de éste había un edificio, el cual era como una escuela de aldea o un jardín de parvulitos."


translate spanish day4_un_a547b60d:


    "La pintura se caía a pedazos de las paredes. Habían varios agujeros en el tejado, como las secuelas tras un bombardeo. Y las ventanas sin vidrios nos miraban tristes y un poco amenazadoramente.{w} No era una vista muy gratificante."


translate spanish day4_un_fda5c27f:


    "No podía recordar cómo me había imaginado este lugar hace unos instantes atrás. Era como si todas las imágenes se hubieran borrado de mi memoria, reemplazadas por este panorama de cementerio deprimente."


translate spanish day4_un_af5a1200:


    me "Bueno, es espeluznante..."


translate spanish day4_un_a1b1799c:


    "Lena estaba todavía en silencio, pero una expresión natural de temor apareció en su rostro."


translate spanish day4_un_6e55cb5d:


    un "¿Crees que está ahí dentro?"


translate spanish day4_un_7479b07e:


    me "No tengo ni idea..."


translate spanish day4_un_8ae2c27f:


    "Si fuera Shurik, entonces una casa encantada sería el último lugar en el que me escondería."


translate spanish day4_un_35698e3a:


    un "¿Debemos ir?"


translate spanish day4_un_63a2b840:


    "No logré responder... la luna apareció de detrás de las nubes e iluminó el claro con nuevos colores."


translate spanish day4_un_8577fae8:


    "En realidad con un color... el blanco mortecino."


translate spanish day4_un_29a44ad3:


    "Podía ver mucho más claramente los árboles distantes, la niebla amortajándolos. Sentí como la temperatura descendió varios grados, dándome un escalofrío."


translate spanish day4_un_9d47d7cc:


    un "¿Estás preocupado?"


translate spanish day4_un_9191d5fa:


    "Lena preguntó con calma."


translate spanish day4_un_8251b3c4:


    me "¿Sinceramente?"


translate spanish day4_un_245b3c9a:


    "Ella sonrió casi imperceptiblemente y me cogió de la mano."


translate spanish day4_un_9ddc743b:


    "Habría causado una tormenta de emociones en cualquier otra situación, pero en aquel momento lo sentí como una necesidad básica."


translate spanish day4_un_763505e7:


    "Lentamente caminamos hacia el edificio."


translate spanish day4_un_ce617998_1:


    "..."


translate spanish day4_un_679ab4c6:


    "Caminando a través del patio, empujé un tiovivo, haciendo que chirriara desagradablemente mientras hacía medio giro."


translate spanish day4_un_3bade428:


    "Lena tembló y me estrechó la mano con la que me cogía."


translate spanish day4_un_1dd28790:


    me "Lo siento... probablemente solamente recordé mi infancia."


translate spanish day4_un_5a29deb9:


    un "¿Te gustaban los tiovivos?"


translate spanish day4_un_d61e8c95:


    me "Sí... En realidad, no lo sé, no lo recuerdo. Probablemente. A todos los niños les gusta."


translate spanish day4_un_ad4edfc8:


    un "No me gustaban."


translate spanish day4_un_5ac38cf1:


    me "¿Por qué?"


translate spanish day4_un_200a724b:


    un "Me mareo cuando estoy en ellos."


translate spanish day4_un_42576b67:


    me "No me extraña, si giras muy rápidamente."


translate spanish day4_un_1f63bde1:


    un "Me gustaban más los columpios."


translate spanish day4_un_417eff12:


    me "Bueno... ¡pero te puedes marear en un columpio también!"


translate spanish day4_un_961f1961:


    un "¿Por qué?"


translate spanish day4_un_7479b07e_1:


    me "No sé..."


translate spanish day4_un_33548823:


    "Esa conversación me había distraído un poco y dejé de preocuparme sobre todo: acerca de Shurik, sobre nuestro viaje nocturno, sobre Lena..."


translate spanish day4_un_efd8ecf9:


    th "Después de todo, este mundo no es tan ajeno."


translate spanish day4_un_0a11ff96:


    "Finalmente llegamos a las puertas..."


translate spanish day4_un_b6e2f0dc:


    "El interior del antiguo campamento me recordó a un jardín de parvulitos, el mismo al que fui en mi infancia."


translate spanish day4_un_4b790dca:


    "A primera vista incluso la disposición era la misma."


translate spanish day4_un_b01a7f13:


    me "¡Shurik!"


translate spanish day4_un_2554ba11:


    un "¡Shurik!"


translate spanish day4_un_5498d175:


    "Un silencio sepulcral nos replicó.{w} Incluso el viento de fuera se había calmado."


translate spanish day4_un_833aced3:


    me "Parece que no hay nadie aquí."


translate spanish day4_un_6bea7733:


    un "Deberíamos comprobarlo de todas formas."


translate spanish day4_un_0edfb52c:


    "La valentía de Lena no dejaba de sorprenderme."


translate spanish day4_un_a85771c8:


    "O, debería decir, su falta natural de instinto de supervivencia."


translate spanish day4_un_7163af4a:


    "No sé si este comportamiento es extraño, o no, para esta muchacha."


translate spanish day4_un_a70602a7:


    me "Vale, hagámoslo..."


translate spanish day4_un_ce617998_2:


    "..."


translate spanish day4_un_6f0ee1a6:


    "Examinamos cuidadosamente todas las habitaciones del antiguo campamento, incluso inspeccioné el ático."


translate spanish day4_un_587c11d8:


    "Había señales de que la gente había visitado este lugar en todas partes: periódicos, botellas vacías y otra porquería. Pero no había señal de Shurik."


translate spanish day4_un_70fc2e8b:


    "Retornamos a la entrada en la cual habíamos empezado nuestra búsqueda."


translate spanish day4_un_ed5bdfd6:


    me "¿Qué deberíamos hacer?"


translate spanish day4_un_4657d7ce:


    un "No tengo ni idea..."


translate spanish day4_un_c19ecb61:


    "Lena se sentó en los escalones y se quedó mirando fijamente sus pies."


translate spanish day4_un_a8e34a9c:


    me "Creo que deberíamos volver..."


translate spanish day4_un_c0d7168c:


    "Empecé con cuidado."


translate spanish day4_un_4ca3177a:


    me "Es tarde y... ¿acaso podemos realmente nosotros dos solos buscarle por todo el bosque?"


translate spanish day4_un_85add91e:


    un "Puede que tengas razón."


translate spanish day4_un_d3d630bf:


    "Se le veía triste y su expresión me dio a entender que la búsqueda no se había terminado."


translate spanish day4_un_8aa027b2:


    me "¡Bueno, en fin!"


translate spanish day4_un_66287c48:


    "Moví las manos en señal de resignación y me senté a su lado."


translate spanish day4_un_b2005ce0:


    me "Deberíamos pensar en el peor de los casos..."


translate spanish day4_un_98fbbb39:


    un "¿Quieres decir que...?"


translate spanish day4_un_a7f8624d:


    me "No, pero... ¿Hay acaso fauna salvaje por los alrededores?"


translate spanish day4_un_999aca00:


    un "Lo dudo."


translate spanish day4_un_65356482:


    "Lena se calmó al instante."


translate spanish day4_un_e4ef2e97:


    me "¡Puede que esté durmiendo en alguna parte! Se despertará por la mañana y volverá al campamento."


translate spanish day4_un_6e34d368:


    un "Sí, por supuesto..."


translate spanish day4_un_064ad673:


    "Me puse de pie y comencé a caminar en círculos alrededor de la entrada."


translate spanish day4_un_9ae0b3a2:


    th "Realmente quería abandonar este lugar, escapar del bosque, pero era como si el comportamiento de Lena me retuviera aquí."


translate spanish day4_un_06d65acf:


    "Quise tratar de convencerla, pero entonces me percaté de algo en el suelo."


translate spanish day4_un_3fde514e:


    "Era una trampilla. Había un pequeño montón de porquería y polvo a su alrededor."


translate spanish day4_un_00a5fafc:


    th "¡Debe haber sido abierta recientemente!"


translate spanish day4_un_2bed846c:


    me "Mira."


translate spanish day4_un_39f5c0fa:


    un "¿Crees que Shurik está ahí?"


translate spanish day4_un_1bb4e1f7:


    "Lena se agachó en cuclillas y con cuidado tiró del mango de la trampilla."


translate spanish day4_un_cdff3e66:


    me "Puede que no sea Shurik, pero seguro que alguien la usó recientemente."


translate spanish day4_un_cb3016d3:


    "Ya me había arrepentido de haber hallado esa maldita puerta al infierno."


translate spanish day4_un_f533d24e:


    un "¿Lo comprobamos?"


translate spanish day4_un_a2335b11:


    "La trampilla no era muy pesada, así que podías abrirla sin mucho esfuerzo."


translate spanish day4_un_ef14dccc:


    "Dirigí el foco de luz de la linterna en ella y vi una escalera yendo abajo un par de metros."


translate spanish day4_un_c80ee062:


    me "Es como un sótano..."


translate spanish day4_un_a7796b9d:


    un "¿Bajamos?"


translate spanish day4_un_3d1667de:


    "Miré a Lena por un corto instante, tratando de comprender qué pasaba por su mente."


translate spanish day4_un_d42ee41a:


    th "¿Tendría ella ansias de aventuras como Ulyana?{w} ¿Y dónde estaría su joven entusiasmo pues?"


translate spanish day4_un_e799e894:


    th "O tal vez simplemente se le fue un poco la cabeza..."


translate spanish day4_un_637e04ca:


    "Lena no parece una persona loca."


translate spanish day4_un_d58dfe0e:


    th "Pero de todas formas, ¿quién dijo que se le pudiera evaluar con una lógica de comportamiento humano?"


translate spanish day4_un_dfcf67ae:


    "Ese pensamiento debería hacer que me asuste, pero de alguna manera no le presté mucha atención entre el otro millón de pensamientos.{w} Algunos de ellos eran mucho más importantes... por ejemplo, qué cosa podría haber al acecho ahí abajo."


translate spanish day4_un_ce617998_3:


    "..."


translate spanish day4_un_bbdc9393:


    "Descendí hacia abajo y miré alrededor."


translate spanish day4_un_3a575910:


    me "Todo está bien."


translate spanish day4_un_5222c78f:


    "Después de estar seguro de que no había nada por lo que preocuparse, llamé a Lena."


translate spanish day4_un_fe24c71d:


    "Estábamos en el largo pasillo, el cual ciertamente no era un sótano."


translate spanish day4_un_f3fd57d1:


    "Su arquitectura se parecía mucho más a un calabozo del KGB, o un túnel de metro en mantenimiento. No sé cuál sería mejor."


translate spanish day4_un_00ed8ed5:


    "Habían inumerables cables a lo largo de las paredes, amarrados por ganchos de metal cada medio metro. Habían bombillas bajo el techo, cubiertas por apliques oxidados. El hormigón desmoronado crujía bajo nuestros pies desagradablemente."


translate spanish day4_un_35698e3a_1:


    un "¿Debemos ir?"


translate spanish day4_un_c6ff2b5b:


    "Preguntó Lena sin emoción alguna."


translate spanish day4_un_43695a7a:


    me "¿A dónde? ¿Allá?"


translate spanish day4_un_53b04f6d:


    un "Bueno, sí. ¿Y si Shurik está allí?"


translate spanish day4_un_1a763905:


    me "¿Qué estaría haciendo allí?"


translate spanish day4_un_673ea5b0:


    "En cualquier caso hoy realmente no podía rechazarla, así que nos olvidamos de nuestro miedo y nos dirigimos hacia la oscuridad."


translate spanish day4_un_ce617998_4:


    "..."


translate spanish day4_un_0431d71e:


    "Lena caminó a mi lado, agarrando mi mano."


translate spanish day4_un_5e66f0b6:


    "El silencio del calabozo se interrumpió solamente por el sonido de nuestros pasos y el agua goteando del techo."


translate spanish day4_un_cdc7cb75:


    "Avanzamos lentamente, quizá demasiado lentamente. De repente sentí un arrebato de claustrofobia."


translate spanish day4_un_7b3a6c14:


    "Apreté los dientes y aferré con más fuerza la linterna, pero perdí fuerzas al apretar, temiendo que rompiera nuestra única fuente de luz."


translate spanish day4_un_fa5b94fe:


    "Lena se quedó en silencio y su silencio resultaba más ruidoso que sus palabras. Comencé a tener miedo."


translate spanish day4_un_3b137961:


    me "Di algo."


translate spanish day4_un_9faa94d8:


    un "La puerta."


translate spanish day4_un_ad02f53a:


    me "¿Qué?"


translate spanish day4_un_9faa94d8_1:


    un "Que hay una puerta."


translate spanish day4_un_72630fc0:


    "Indicó delante."


translate spanish day4_un_934026f6:


    "Llegamos a una enorme puerta de metal con una señal de peligro biológico."


translate spanish day4_un_eb8f4603:


    me "Parece un refugio antiaéreo..."


translate spanish day4_un_f1f136f3:


    un "Sí. Escuché algo de eso."


translate spanish day4_un_9e352857:


    me "¿Por qué está ahí?"


translate spanish day4_un_316a211f:


    un "No tengo ni idea. Tal vez sea por la Crisis de los Misiles de Cuba."


translate spanish day4_un_9ecf579f:


    me "¿Cuba?"


translate spanish day4_un_41b4cc8d:


    "Estimé una aproximación de la época de la construcción del campamento, y tenía sentido."


translate spanish day4_un_c02a1e81:


    "Aun así, construir un refugio antiaéreo aquí era como construir un aeropuerto en Alarcón de Cuenca en España, o en Altamirano de Brandsen en Argentina.{w} No era suficientemente profundo y estaba demasiado lejos de la civilización."


translate spanish day4_un_225dc90a:


    "El pomo giratorio de la puerta chirrió lentamente. Tuve que empujar con todas mis fuerzas antes de que girase un par de veces."


translate spanish day4_un_4b931dd1:


    "Me hice a la idea de su dificultad y abrí la puerta..."


translate spanish day4_un_856f240c:


    "Entramos en la habitación que parecía ser los «dormitorios» principales."


translate spanish day4_un_dfdbb734:


    "Habían algunas camas, cajas, un tipo de equipo científico. Habían sido preparadas a conciencia para un apocalipsis nuclear."


translate spanish day4_un_a4794e3a:


    "No hallamos ninguna señal de Shurik aquí a pesar de todo."


translate spanish day4_un_d5312815_1:


    un "Mira."


translate spanish day4_un_f0153e82:


    "Lena sostenía una pistola de bengalas y sonreía."


translate spanish day4_un_a1e442ec:


    me "¿Por qué iba a necesitarla?"


translate spanish day4_un_9a220deb:


    un "Para luchar contra los monstruos."


translate spanish day4_un_e23f4f15:


    me "No hay monstruos aquí."


translate spanish day4_un_7c7f5007:


    "Como mínimo quería creer eso."


translate spanish day4_un_1318a9ad:


    un "Si tú lo dices..."


translate spanish day4_un_6d9f91ff:


    me "¡Te lo garantizo!"


translate spanish day4_un_df288870:


    "No quería hacerla enfadar, así que metí la pistola de bengalas en mi cinturón. Nunca se sabe..."


translate spanish day4_un_f457cf0f:


    "Buscamos detenidamente en la habitación una vez más."


translate spanish day4_un_7131cb1c:


    "Habían dos salidas.{w} La primera era la puerta por la cual habíamos entrado, y la otra era una puerta exactamente igual en la pared de la izquierda."


translate spanish day4_un_3dda53ec:


    "Por un instante sentí entusiasmo, las prisas por llegar al final de este laberinto y ver qué premio me esperaba allí."


translate spanish day4_un_ae82ae84:


    "Sin embargo, seguramente no era un videojuego y no había opción de guardar."


translate spanish day4_un_8be5d580:


    un "¿Quizá con esto?"


translate spanish day4_un_634ac583:


    "Lena me ofreció un pasamanos bastante grande."


translate spanish day4_un_ad83807e:


    me "No. Lo intentaré sin él primero."


translate spanish day4_un_c87458f4:


    "Sin embargo, la puerta no se quiso mover... solamente chirrió desagradablemente y el pomo giratorio no se giró ni un milímetro."


translate spanish day4_un_901746cb:


    me "Vale. Dámelo."


translate spanish day4_un_2ece48fa:


    "Fue muy fácil con el pasamanos."


translate spanish day4_un_c8250092:


    "Al final el obstáculo se derrumbó, golpeando el suelo con un estruendo."


translate spanish day4_un_50d5d9d2:


    "Las bisagras estaban completamente oxidadas."


translate spanish day4_un_13d39abc:


    "Dirigí el foco de luz de la linterna al pasadizo. Había un pasillo justo como el que habíamos cruzado hasta aquí."


translate spanish day4_un_35698e3a_2:


    un "¿Vamos?"


translate spanish day4_un_2f236b34:


    "Era como si Lena me estuviera dirigiendo constantemente."


translate spanish day4_un_d3520d70:


    me "¿Dónde vas con tanta prisa?"


translate spanish day4_un_338e0f1a:


    un "¿Yo? Yo no..."


translate spanish day4_un_340ec68e:


    "Se sonrojó confusa."


translate spanish day4_un_e6e2a051:


    th "Otra vez... ¿qué debería hacer con ella?{w} Primero no tiene miedo alguno y luego se queda muda tras hablarle."


translate spanish day4_un_2968b0cc:


    me "No tienes aspecto de tener miedo de algo."


translate spanish day4_un_1c959e2d:


    un "No lo sé. ¿De qué debería tener miedo?"


translate spanish day4_un_aa5f81a3:


    un "Tú me protegerás igualmente..."


translate spanish day4_un_3f6ffa1d:


    "Añadió, apenas de forma audible..."


translate spanish day4_un_a766ab32:


    "Así que Lena cuenta conmigo. Ella cree en mí sin importar qué..."


translate spanish day4_un_4bac4a4b:


    th "Es posible.{w} Estúpido, ingenuo, pero posible."


translate spanish day4_un_cb8a29df:


    "Sabía con claridad que no podía proteger a nadie, ni a mí. Nada depende de mí en este mundo."


translate spanish day4_un_72c7c023:


    "¡Las fuerzas que me han traído aquí podrían hacer cualquier cosa!"


translate spanish day4_un_e908fa11:


    "Eso no significaba exactamente que una muerte inevitable me esperara al final del túnel, podría estar esperándome en cualquier parte de este campamento."


translate spanish day4_un_b749d8c0:


    me "¡Vamos!"


translate spanish day4_un_ce617998_5:


    "..."


translate spanish day4_un_04e4c88e:


    "Traté de caminar más rápidamente, y Lena no pareció estar molesta por eso ya que fácilmente mantuvo el ritmo conmigo."


translate spanish day4_un_6fa018f9:


    "Este pasillo era exactamente igual que el anterior.{w} Hasta el último detalle."


translate spanish day4_un_06e7b9dd:


    "No había nada de raro en ello, pero en algun momento tuve la sensación de que estábamos caminando en círculos."


translate spanish day4_un_b5e87c10:


    "La linterna en mi mano empezaba a temblar visiblemente. El foco de luz iba de un lado a otro de las paredes y el suelo y, repentinamente, iluminó un agujero bastante grande..."


translate spanish day4_un_787a88e3:


    "El agujero no era muy profundo y, abajo, se podían ver raíles."


translate spanish day4_un_d5a761a9:


    un "¿Qué hay ahí abajo?"


translate spanish day4_un_2b1cc4e3:


    me "Es como una mina."


translate spanish day4_un_f533d24e_1:


    un "¿Debemos echar un vistazo?"


translate spanish day4_un_403cf625:


    me "¿Por qué no vamos más adelante siguiendo el pasillo?"


translate spanish day4_un_13849591:


    un "No sé, creo que deberíamos ir por ahí abajo."


translate spanish day4_un_30068944:


    "Calculé la altura... sería posible salir por nuestra cuenta."


translate spanish day4_un_b6d2f56b:


    me "Vale. Comprobémoslo."


translate spanish day4_un_4b5ac164:


    "Salté hacia abajo en el agujero y ayudé a Lena a bajar."


translate spanish day4_un_540f2604:


    "Realmente era una mina."


translate spanish day4_un_b1af84b4:


    th "Me pregunto qué podrían haber estado extrayendo de aquí..."


translate spanish day4_un_37b84aa9:


    me "¿Qué minerales hay en esta zona?"


translate spanish day4_un_0da946d4:


    un "No lo sé."


translate spanish day4_un_7839cf57:


    me "Bueno, claro, que pregunta más estúpida. Parece que ya no tiene nada..."


translate spanish day4_un_b53b2290:


    "Nos dirigimos hacia la oscuridad."


translate spanish day4_un_a7838803:


    "Era difícil caminar, porque no podía saber dónde pisaba: tablas de madera inseguras o suelo desnivelado."


translate spanish day4_un_9cee4921:


    "Tampoco podía estar junto a las paredes... la estrechez del túnel nos obligaba a permanecer entre los raíles y no quería soltar la mano de Lena."


translate spanish day4_un_2c6a97fe:


    "Finalmente alcanzamos una bifurcación."


translate spanish day4_un_f604608d:


    me "Vaya, genial."


translate spanish day4_un_7b74056c:


    un "¿Adónde deberíamos ir?"


translate spanish day4_un_d2b4c033:


    me "¿Dónde? No estoy muy seguro de que podamos salir de aquí, especialmente si vamos a jugar a Pac-Man..."


translate spanish day4_un_9b63fa7b:


    un "¿Jugar a qué?"


translate spanish day4_un_a4513cee:


    me "Olvídalo. Nos perderemos."


translate spanish day4_un_f05312d6:


    un "¿Y si hay una salida también?"


translate spanish day4_un_99f6233a:


    me "Puede que haya una... ¿Y si no la hay?"


translate spanish day4_un_7749b4db:


    un "Pues entonces, ¿deberíamos volver?"


translate spanish day4_un_93f7497e:


    "Me mordí el labio hasta sangrar y grité tan fuerte como podía."


translate spanish day4_un_b01a7f13_1:


    me "¡Shurik!"


translate spanish day4_un_3def6bf8:


    "El ruidoso eco rebotó en cada dirección a la vez. La tierra incluso se caía del techo en algunas partes."


translate spanish day4_un_a6afb8d5:


    me "Lo ves..."


translate spanish day4_un_d25a0ad8:


    un "Pues seguiré sola."


translate spanish day4_un_ad02f53a_1:


    me "¡¿Qué?!"


translate spanish day4_un_ff99c828:


    "Sonreí estúpidamente."


translate spanish day4_un_64e96062:


    me "¿Sola? ¿Dónde?"


translate spanish day4_un_e3e62f26:


    un "Tenemos que hallar a Shurik. Puede estar..."


translate spanish day4_un_55c7ffde:


    "Lena se ruborizó enseguida y miró fijamente el suelo."


translate spanish day4_un_6003d47f:


    me "No, no, no, eso no lo harás. Si vamos, lo hacemos juntos."


translate spanish day4_un_3f6ce067:


    un "Vale. Pues vamos."


translate spanish day4_un_08bcf5c5:


    "Me sonrió y me cogió de la mano."


translate spanish day4_un_f6728d01:


    th "Cómo se lo hace..."


translate spanish day4_un_c9cc1313:


    me "Pero primero deberíamos..."


translate spanish day4_un_d8a01162:


    "Agarré una piedra afilada del suelo y marqué una cruz en una de las vigas, las cuales sostenían las paredes."


translate spanish day4_un_01e8bf07:


    me "Así sabremos dónde empezamos."


translate spanish un_mine_coalface_e40597aa:


    "Ya habíamos estado aquí."


translate spanish un_mine_coalface_5b4c3c90:


    "Finalmente atravesamos el túnel y llegamos a una sala extensa con un techo elevado."


translate spanish un_mine_coalface_3ef8c583:


    "Aunque a pesar de todo difícilmente se le podría llamar una sala. Debían haber solido extraer alguna cosa aquí.{w} Carbón, probablemente, o tal vez oro."


translate spanish un_mine_coalface_3c54fe69:


    "Las paredes habían sido talladas con picos o martillos neumáticos."


translate spanish un_mine_coalface_65a5fdb8:


    "Este lugar era negro como el alquitrán, por lo que nuestra única salvación era la linterna."


translate spanish un_mine_coalface_fadb68bd:


    "Si se rompiera, es improbable que pudiéramos escapar de aquí..."


translate spanish un_mine_coalface_58411121:


    "Me percaté, en un rayo de iluminación, de un pedazo de ropa roja en una esquina."


translate spanish un_mine_coalface_ca0a4f90:


    "¡Eso era un pañuelo de pionero!"


translate spanish un_mine_coalface_96b3272c:


    "Shurik estaba obviamente aquí en alguna parte."


translate spanish un_mine_coalface_93ec21f7:


    me "¡Shurik! ¡Shurik!"


translate spanish un_mine_coalface_ca9e3fa1:


    un "¡Shurik! ¡Shurik!"


translate spanish un_mine_coalface_b99be3e2:


    "Solamente nuestro eco nos respondió."


translate spanish un_mine_coalface_c1938ec1:


    un "No debe estar muy lejos, ya que encontramos su pañuelo aquí."


translate spanish un_mine_coalface_e80273e8:


    "Hablando francamente, me sorprendería mucho saber dónde es «aquí» en realidad..."


translate spanish un_mine_coalface_a411960f:


    th "¿Dónde podría haberse ido desde aquí?"


translate spanish un_mine_coalface_686bf61c:


    "No había ninguna otra salida desde esta sala."


translate spanish un_mine_coalface_7ea80226:


    th "Ciertamente, era posible que hubiera otros lugares en estos túneles que no hayamos visitado aun..."


translate spanish un_mine_coalface_858b6cbe:


    th "¡Eso significa que tenemos que seguir buscándole!"


translate spanish un_mine_exit_0e2f1383:


    "Al girar en la siguiente esquina, una puerta de madera apareció en la luz."


translate spanish un_mine_exit_6585ac5f:


    me "Al menos es algo..."


translate spanish un_mine_exit_3875ba7b:


    un "¿Qué?"


translate spanish un_mine_exit_81846d80:


    me "Al menos no es otra bifurcación."


translate spanish un_mine_exit_97b34350:


    un "¿Qué hay ahí?"


translate spanish un_mine_exit_75b7c7b4:


    me "No tenemos ninguna otra opción que comprobarlo."


translate spanish un_mine_exit_1f752f8a:


    "Giré con fuerza el mango de la puerta y la abrí."


translate spanish un_mine_exit_351ceb93:


    "Había una sala detrás de ella, la cual podría ser una de las salas de trastos del refugio antiaéreo."


translate spanish un_mine_exit_a8afc189:


    "Botellas vacías y colillas de cigarrilo estaban desperdigadas por todas partes. Las paredes estaban todas cubiertas de garabatos."


translate spanish un_mine_exit_9c847221:


    th "¡Entonces eso significa que hay otra salida por aquí!"


translate spanish un_mine_exit_dd71665d:


    "No quería creer que la gente que había dejado todo aquello, hubiera seguido el mismo camino que nosotros hicimos."


translate spanish un_mine_exit_f89bf0e3:


    "Tristemente, Shurik no estaba aquí."


translate spanish un_mine_exit_1cc1d82f:


    me "Oh..."


translate spanish un_mine_exit_614dca6b:


    "Me apoyé en la pared y me dejé caer al suelo."


translate spanish un_mine_exit_f5df0746:


    me "Ya debemos haber estado en todas partes."


translate spanish un_mine_exit_c57bbc05:


    un "No en todas."


translate spanish un_mine_exit_a8a4d8de:


    "Lena indicó la puerta del rincón."


translate spanish un_mine_exit_4b1b3ebe:


    "Se asemejaba a la misma que daba al refugio antiaéreo."


translate spanish un_mine_exit_7208c8c6:


    me "Ahí debería estar probablemente la salida, tal como dijiste."


translate spanish un_mine_exit_35698e3a:


    un "¿Vamos?"


translate spanish un_mine_exit_9f46626d:


    me "Descansemos un instante."


translate spanish un_mine_exit_b993fafe:


    un "Vale."


translate spanish un_mine_exit_fd0aed6a:


    "Lena se sentó a mi lado, muy cerca, y me cogió de la mano."


translate spanish un_mine_exit_6e5e2fd8:


    un "Todo va bien."


translate spanish un_mine_exit_e54b39e9:


    me "¿Qué quieres decir?"


translate spanish un_mine_exit_840b2721:


    un "Que no hemos hallado a Shurik."


translate spanish un_mine_exit_8f9391d8:


    me "Deberíamos pensar en que salgamos nosotros."


translate spanish un_mine_exit_533890b7:


    un "Saldremos."


translate spanish un_mine_exit_f9b93501:


    me "Sí. Probablemente me acuerde del camino."


translate spanish un_mine_exit_6590db25:


    "O, al menos, creía recordarlo."


translate spanish un_mine_exit_e04c5cfb:


    un "No estoy preocupada en absoluto."


translate spanish un_mine_exit_f7f16276:


    "Dijo repentinamente tras una pequeña pausa."


translate spanish un_mine_exit_69d18c31:


    me "Eso es bueno."


translate spanish un_mine_exit_d0909642:


    un "Porque estoy contigo."


translate spanish un_mine_exit_2f2d5a31:


    "De repente, provino un ruido tras la puerta de la mina."


translate spanish un_mine_exit_debe8be2:


    "Me puse de pie de golpe y empecé a buscar algo que pudiera usar como arma."


translate spanish un_mine_exit_266bb45a:


    "El ruido de las pisadas fuertes se acercaba."


translate spanish un_mine_exit_75fc727b:


    "Finalmente, la puerta se abrió y Shurik apareció tras ella."


translate spanish un_mine_exit_c305b061:


    "En el silencio me quedé helado, mirándole fijamente."


translate spanish un_mine_exit_80c1c240:


    sh "¡Ahí estáis! ¿Creíais que os podríais esconder de mi?"


translate spanish un_mine_exit_ad02f53a:


    me "¿Qué?"


translate spanish un_mine_exit_f4b92687:


    sh "¿Creíais que no os hallaría? ¡Pero lo hice!"


translate spanish un_mine_exit_5db31dbf:


    "No estaba cuerdo, eso estaba seguro: su rostro estaba deformado por una mueca de terror, sus ojos brillaban bajo sus gafas. El pionero desaparecido sostenía un pasamanos de metal en sus manos."


translate spanish un_mine_exit_6f664776:


    me "¡¿Te has vuelto loco?! ¡Somos nosotros!"


translate spanish un_mine_exit_2e350af4:


    sh "Claro, ¡puedo ver que sois vosotros!"


translate spanish un_mine_exit_2bec6403:


    "Dio un par de pasos hacia nosotros."


translate spanish un_mine_exit_7bb9ce4b:


    "Instintivamente protegí a Lena."


translate spanish un_mine_exit_6cb76a93:


    sh "¿Creíais que me tomaríais el pelo? ¡¿Conduciéndome aquí y allá?! ¡¿«Hacia la izquierda, hacia la derecha, hacia la izquierda, hacia la derecha»?! Y seguí y seguí..."


translate spanish un_mine_exit_a94436e5:


    "Levantó el pasamanos de metal."


translate spanish un_mine_exit_039ca5ae:


    "Todo lo que sucedió posteriormente fue como a cámara lenta."


translate spanish un_mine_exit_7e0cb307:


    "Shurik, abalanzándose sobre nosotros."


translate spanish un_mine_exit_38d16829:


    "Yo, empujando a Lena fuera de su alcance."


translate spanish un_mine_exit_61257df0:


    "El pasamanos, lentamente dibujando un arco dirigiéndose sobre mi cabeza..."


translate spanish un_mine_exit_7276c335:


    "...mi mano con la linterna, ascendiendo..."


translate spanish un_mine_exit_fb557852:


    "Después de todo ello, una completa oscuridad. Respiración rápida. Sangre latiendo por mis sienes. Silencio... un espantoso y pesado silencio, mezclándose con la oscuridad."


translate spanish un_mine_exit_ee0f0bdf:


    "Moví mi mano, tratando de hallar la pared, cuando sentí el tacto de alguien."


translate spanish un_mine_exit_d7fd3730:


    un "No te preocupes."


translate spanish un_mine_exit_2233ca2f:


    "Escuché una voz familiar cerca."


translate spanish un_mine_exit_79b75990:


    me "¡¿Dónde estás, maldito lunático?!"


translate spanish un_mine_exit_a5efbe41:


    "Grité."


translate spanish un_mine_exit_6a0e7f4b:


    un "Se fue."


translate spanish un_mine_exit_90465eb1:


    me "¿En qué dirección se fue? ¿Adónde?"


translate spanish un_mine_exit_a742d1cc:


    "La voz de Lena no sonaba muy relajada, pero ciertamente tampoco sonaba como debería en semejantes circunstancias."


translate spanish un_mine_exit_7606245e:


    un "Tranquilízate."


translate spanish un_mine_exit_73e0f4a4:


    "Me abrazó tiernamente, rozando su cuerpo contra el mío."


translate spanish un_mine_exit_e2d3e825:


    "Traté de reponer mis sentidos, recuperar mi aliento, adaptarme a la oscuridad."


translate spanish un_mine_exit_8ef5b76e:


    me "¿Qué hacemos ahora?"


translate spanish un_mine_exit_6729a574:


    un "Tienes una pistola."


translate spanish un_mine_exit_34ee4713:


    me "Bien. ¿Y a quién disparo?"


translate spanish un_mine_exit_dae1cf79:


    un "Está cargada con una bengala."


translate spanish un_mine_exit_5b8394c2:


    "Puede que ella esté en lo cierto."


translate spanish un_mine_exit_e10fcac5:


    "Agarré la pistola de mi cintura, apunté a un lado y disparé al azar."


translate spanish un_mine_exit_e38eb2e8:


    "La sala se iluminó con una luz roja brillante."


translate spanish un_mine_exit_c7063a79:


    "La bengala, estando en el rincón, era como fuegos artificiales o un cohete pirotécnico."


translate spanish un_mine_exit_7fae402f:


    un "Vayámonos rápidamente, no arderá por mucho tiempo."


translate spanish un_mine_exit_25f7abbc:


    me "¿Adónde?"


translate spanish un_mine_exit_fdecf719:


    "Lena indicó en silencio al segundo piso."


translate spanish un_mine_exit_6d6ad7e8:


    "No fue difícil abrirla y nos precipitamos en la oscuridad..."


translate spanish un_mine_exit_ce617998:


    "..."


translate spanish un_mine_exit_bf010e6a:


    "La bengala ardía más débilmente con cada segundo que pasaba."


translate spanish un_mine_exit_be25fae0:


    "Tropezaba con cada paso, incluso me caí en un par de ocasiones, pero sin reducir mi ritmo."


translate spanish un_mine_exit_9fb2c558:


    th "Si va fuera..."


translate spanish un_mine_exit_c6df834b:


    "Finalmente, vimos luz frente a nosotros y llegamos a una escalera, conduciendo a una rejilla en el techo. La bengala silvó y se apagó."


translate spanish un_mine_exit_fc8c5efd:


    me "Gracias a Dios..."


translate spanish un_mine_exit_6ebe51f8:


    "Resultó que estábamos justo debajo de la estatua de Genda."


translate spanish un_mine_exit_b28b9392:


    "La rejilla era bastante recia, pero nos las arreglamos para abrirla tras romper los tornillos con la linterna."


translate spanish un_mine_exit_63a68c76:


    "Tras alcanzar la superficie, me derrumbé en la hierba, exhausto."


translate spanish un_mine_exit_729724c7:


    me "Eso fue terrible..."


translate spanish un_mine_exit_772c30bb:


    "Lena se sentó a mi lado..."


translate spanish un_mine_exit_f5eeaa60:


    me "Fue espantoso..."


translate spanish un_mine_exit_7b02c40e:


    "En aquel momento me importaba un comino este mundo, el campamento, la línea 410 de autobús o mi anterior vida."


translate spanish un_mine_exit_0cde231e:


    "La cosa más extraña y desagradable de todo era que nada de todos estos sucesos tenían algo de sobrenatural."


translate spanish un_mine_exit_d36716df:


    "Shurik sencillamente se volvió loco, demente.{w} No tiene nada de raro, me habría vuelto igual en su lugar..."


translate spanish un_mine_exit_b064b37c:


    "Lena acarició mi cabeza tiernamente y sonrió."


translate spanish un_mine_exit_97e317a3:


    un "Ya ha pasado todo."


translate spanish un_mine_exit_b46c791f:


    me "No sé... ¡Hay un pionero loco por los bosques! Podrías incluso decir... ¡un homicida maníaco!"


translate spanish un_mine_exit_dd9e978b:


    un "Creo que se pondrá bien."


translate spanish un_mine_exit_2cdd58d5:


    me "¿Bien? ¡No estoy tan seguro de eso!"


translate spanish un_mine_exit_7fc6d167:


    un "La cosa más importante es que estamos bien."


translate spanish un_mine_exit_89d38d04:


    "Aun sonreía."


translate spanish un_mine_exit_b923095e:


    me "¿Cómo puedes estar tan calmada?"


translate spanish un_mine_exit_79468419:


    un "Ya te dije antes que no tengo nada de qué preocuparme si estoy contigo."


translate spanish un_mine_exit_f739c9b0:


    "Por supuesto. Antes nos habría salvado a Lena y a mi."


translate spanish un_mine_exit_c066a1fa:


    "Pero solamente fue una coincidencia, nada más."


translate spanish un_mine_exit_507b51cd:


    "Si Shurik hubiera sido raudo...{w} O más loco..."


translate spanish un_mine_exit_de6f6646:


    me "Gracias por el cumplido."


translate spanish un_mine_exit_735a4f9a:


    un "De nada."


translate spanish un_mine_exit_1f9cc7f6:


    me "A pesar de todo, deberíamos habernos quedado aquí en primer lugar."


translate spanish un_mine_exit_332127e8:


    un "Probablemente tengas razón."


translate spanish un_mine_exit_4bde9263:


    "Dijo tranquilamente."


translate spanish un_mine_exit_e41e554d:


    "Tengo en verdad sueño.{w} Debido al estrés, al cansancio y a lo tarde que era por la noche..."


translate spanish un_mine_exit_38af4b55:


    "Debíamos ir a dormir."


translate spanish un_mine_exit_db493765:


    th "Pero tenía que ponerme en pie e ir caminando hasta la cabaña de la líder...{w} No estaba preparado para ello."


translate spanish un_mine_exit_1dff0f5a:


    "Mis ojos se cerraron sólo por un instante..."


translate spanish un_mine_exit_ce617998_1:


    "..."


translate spanish un_mine_exit_43cab7ab:


    "Las estrellas me miraban desde arriba en el cielo.{w} Miles de ellas, incluso millones."


translate spanish un_mine_exit_0f63591e:


    "Ahora su luz no parecía ser tan distante y fría; por el contrario, me centelleaban, como si susurraran entre ellas mismas, contándose alegres cuentos de hadas en una impaciente competición."


translate spanish un_mine_exit_46e33d3d:


    "Sobre una galaxía muy, muy lejana, de cerditos de peluche morados, sobre misteriosos cinturones de asteroides donde las naves han desaparecido, sobre un capitan de una nave espacial que no teme a nada y a su brava tripulación, sobre tesoros inauditos y cumbres inalcanzables de un planeta en el borde del universo..."


translate spanish un_mine_exit_318652a2:


    th "Me pregunto si, ¿es eso un sueño...?"


translate spanish un_mine_exit_7c64a5d0:


    "Me incorporé un poco y me di cuenta de que mi cabeza estaba en el regazo de Lena."


translate spanish un_mine_exit_4e391c09:


    me "¿Dormí mucho?"


translate spanish un_mine_exit_11be2cb0:


    "Pregunté confuso, pero no tenía prisa por levantarme."


translate spanish un_mine_exit_60386181:


    un "No sé. No tengo reloj."


translate spanish un_mine_exit_0467a4c4:


    "Ella se rio."


translate spanish un_mine_exit_240fb495:


    me "¿Aproximadamente?"


translate spanish un_mine_exit_71b8ed88:


    un "Bueno. Tal vez unos veinte minutos."


translate spanish un_mine_exit_aba21a0c:


    me "Ah... Pues eso está bien."


translate spanish un_mine_exit_adab8aab:


    "Me estiré otra vez, sintiéndome a gusto y relajado como nunca antes. Los sucesos de la noche se volvían distantes, como si comenzara a olvidarlos, tal como la historia que las estrellas me estaban contando."


translate spanish un_mine_exit_d5ce5430:


    un "Shurik ha vuelto."


translate spanish un_mine_exit_3d342d5a:


    me "¡¿Qué?!"


translate spanish un_mine_exit_abe0afc6:


    "Me puse de pie instantáneamente."


translate spanish un_mine_exit_d4879afa:


    un "Ahí va, durmiendo en el banco."


translate spanish un_mine_exit_44f88c82:


    "Lena señaló a los bancos para mi sorpresa."


translate spanish un_mine_exit_63d48677:


    me "¿Y qué estabas...?"


translate spanish un_mine_exit_fbd96cba:


    un "Estabas durmiendo tan tranquilamente que no quise despertarte."


translate spanish un_mine_exit_502b0fe3:


    "Sentí un escalofrío de miedo, porque tal comportamiento no era simplemente extraño, tampoco era sencillamente inapropiado. Me asustó mucho más que la demencia de Shurik en el calabozo."


translate spanish un_mine_exit_c0778843:


    "Un lunático, que hace tan solamente media hora trató de matarnos, llega, se estira en un banco de al lado y se duerme, ¿y ella simplemente se queda sentada aquí?"


translate spanish un_mine_exit_f1ca775b:


    un "No hay nada de qué preocuparse, parece que está un poco agotado."


translate spanish un_mine_exit_a948a0c5:


    "Lena se confundió y sonrojó."


translate spanish un_mine_exit_e06c41ca:


    un "Caminaba de manera insegura y no nos miró. Y si hubiera hecho ruido..."


translate spanish un_mine_exit_0c721333:


    "Estaba a punto de llorar."


translate spanish un_mine_exit_a8068fc6:


    me "Vale. No te preocupes."


translate spanish un_mine_exit_cfe251db:


    "Había lógica en sus palabras.{w} Probablemente hizo lo correcto."


translate spanish un_mine_exit_ae234167:


    "En cualquier caso, teníamos que interrogar a Shurik a fondo."


translate spanish un_mine_exit_15e2c240:


    "Me puse en pie, caminé rápidamente hacia el banco en que estaba durmiendo, y lo abofeteé."


translate spanish un_mine_exit_82e16e17:


    sh "¡Ay!"


translate spanish un_mine_exit_99fd0181:


    "Se levantó de golpe."


translate spanish un_mine_exit_4372f7aa:


    sh "¡¿Qué demonios?!"


translate spanish un_mine_exit_215fb22b:


    me "¡¿Qué estás haciendo, bastardo?!"


translate spanish un_mine_exit_13919f7d:


    sh "¿Qué?"


translate spanish un_mine_exit_a83772c6:


    "Shurik me miraba fijamente con cara asustada y, además, aspecto de cuerdo."


translate spanish un_mine_exit_d21f05af:


    me "¡¿Qué fue todo eso de ahí abajo?!"


translate spanish un_mine_exit_c53304b3:


    sh "¿Qué, dónde?"


translate spanish un_mine_exit_55aa43d1:


    me "¡En el calabozo, en la mina, en el refugio antiaéreo! ¿Acaso no perdiste completamente los estribos?"


translate spanish un_mine_exit_6ce75e3c:


    sh "No te comprendo..."


translate spanish un_mine_exit_f1ccf3ee:


    "Miró a su alrededor."


translate spanish un_mine_exit_5b89be3d:


    sh "¿Qué hago aquí?"


translate spanish un_mine_exit_2b4eca13:


    me "¡¿Y qué deberías hacer aquí en tu opinión?! Oh, espera, ya lo sé... ¡deberías estar en un psiquiátrico!"


translate spanish un_mine_exit_1cd4852c:


    sh "Fui al antiguo campamento por la mañana a por piezas de repuesto y luego..."


translate spanish un_mine_exit_fc07db1d:


    un "¿No recuerdas nada después de eso?"


translate spanish un_mine_exit_cf2bfb89:


    "Preguntó Lena tras caminar hasta nosotros."


translate spanish un_mine_exit_b74741dc:


    sh "Sí, y yo..."


translate spanish un_mine_exit_82e63599:


    me "No disimules."


translate spanish un_mine_exit_2be01380:


    "Dije tranquilamente y me senté a su lado."


translate spanish un_mine_exit_37d99f1d:


    "Realmente parecía que a pesar de todo, no estaba mintiendo."


translate spanish un_mine_exit_eb7f67e1:


    sh "No comprendo nada... ¡No tiene rigor científico!"


translate spanish un_mine_exit_925090f3:


    me "A quién le importa. No creas que te voy a creer."


translate spanish un_mine_exit_ef21e5d1:


    sh "La pérdida de memoria no puede ocurrir así como así..."


translate spanish un_mine_exit_224d6a7e:


    "Shurik habló para sí mismo, murmuró algo, sin prestarnos ninguna atención."


translate spanish un_mine_exit_a8b5b91a:


    un "Vayámonos."


translate spanish un_mine_exit_3a1bc23d:


    "Dijo Lena en voz baja."


translate spanish un_mine_exit_5b76ac8a:


    me "¿Debemos dejarle simplemente aquí?"


translate spanish un_mine_exit_5ec8c2d4:


    un "No está en sus cabales, tiene que dormir."


translate spanish un_mine_exit_34e11933:


    me "¡Es peligroso dejar a este psicópata solo! ¡Puede que estrangule a Electronik con una bobina de inducción por la noche!"


translate spanish un_mine_exit_e672ac80:


    un "Estará bien. Creéme."


translate spanish un_mine_exit_fd384959:


    "No tenía razón para no creerla."


translate spanish un_mine_exit_5b31be1d:


    "Sin embargo, tampoco no tenía una razón para creerla."


translate spanish un_mine_exit_fbd85565:


    th "Por otra parte, ¿a quién le importa ahora? ¡Quería irme a dormir cuanto antes fuera posible!"


translate spanish un_mine_exit_505d91ae:


    me "Vale..."


translate spanish un_mine_exit_19c4dce2:


    "Dejamos a Shurik, el cual todavía murmuraba algo para sí mismo."


translate spanish un_mine_exit_ce617998_2:


    "..."


translate spanish un_mine_exit_cd10cb1d:


    un "Ya llegamos."


translate spanish un_mine_exit_1d9b8f65:


    me "¿Qué? ¿Dónde?"


translate spanish un_mine_exit_41a8b963:


    un "Yo tengo que ir más lejos."


translate spanish un_mine_exit_561828ec:


    "Lena sonrió."


translate spanish un_mine_exit_ac77630c:


    "No había pensado en nada en todo el camino hasta aquí, sencillamente la seguí, y no me había dado cuenta de que llegamos a la cabaña de Olga Dmitrievna."


translate spanish un_mine_exit_9513cd87:


    me "Sí..."


translate spanish un_mine_exit_8cf9c83d:


    un "Gracias... por hoy."


translate spanish un_mine_exit_6c156bd2:


    me "No hay nada que agradecerme. Ya es bueno que hayamos vuelto ilesos. ¡Veremos qué tiene que decir Shurik mañana!"


translate spanish un_mine_exit_05ed2d0a:


    un "Gracias de todas formas..."


translate spanish un_mine_exit_9e3e0847:


    "Dijo misteriosamente y apartó su mirada."


translate spanish un_mine_exit_e88d2104:


    me "De nada."


translate spanish un_mine_exit_6ecba0e3:


    un "Bueno, ¡es hora de que me vaya!"


translate spanish un_mine_exit_6b46b235:


    "Lena se giro rápidamente y se fue caminando velozmente en dirección a su cabaña."


translate spanish un_mine_exit_692879ab:


    "Inmediatamente me puse a pensar en que algo iba mal, no muy bien que digamos."


translate spanish un_mine_exit_3b0e393f:


    th "Tras todo lo que había ocurrido, decir simplemente «bueno, es hora de que me vaya»?"


translate spanish un_mine_exit_864250dc:


    th "Normalmente funciona de forma diferente en dichas situaciones, ¿no es así?"


translate spanish un_mine_exit_1a76d291:


    "No tengo ni idea de lo que esperaba después de todo..."


translate spanish un_mine_exit_90db22de:


    "El cansancio me golpeaba de nuevo. Caminando con dificultad, entré en la cabaña."


translate spanish un_mine_exit_9dc23163:


    "La líder estaba sentada en mi cama..."


translate spanish un_mine_exit_65d8d90c:


    mt "Semyon..."


translate spanish un_mine_exit_5c8a950f:


    "Empezaba a hablar tristemente."


translate spanish un_mine_exit_3300a99a:


    mt "¿Dónde has estado?"


translate spanish un_mine_exit_4e8fd4d6:


    me "Bueno... yo..."


translate spanish un_mine_exit_18329aed:


    "Preparado para ser regañado, no me esperaba esa reacción."


translate spanish un_mine_exit_8644ddd6:


    me "Fui a buscar a Shurik."


translate spanish un_mine_exit_b166a941:


    mt "¿Solo?"


translate spanish un_mine_exit_57cf671c:


    me "No, con Lena."


translate spanish un_mine_exit_46b3ba5a:


    me "Sí, solo."


translate spanish un_mine_exit_49d63af4:


    mt "Bueno...{w} ¿Y qué hay de Shurik?"


translate spanish un_mine_exit_786bc7ef:


    "Es como si Olga Dmitrievna estuviera realmente preocupada por el devenir del pionero desaparecido."


translate spanish un_mine_exit_c8bc02e9:


    th "¿Qué tiene de extraño eso?"


translate spanish un_mine_exit_b9bcf031:


    th "La líder puede tomarse no seriamente su trabajo, ¡pero eso no significaría que sea una persona sin corazón!"


translate spanish un_mine_exit_111b3046:


    me "Está bien...{w} Tanto como «bien» pudiera estar en semejante situación."


translate spanish un_mine_exit_564980ed:


    mt "Eso es bueno.{w} Ve a dormir ahora."


translate spanish un_mine_exit_47b3e3c3:


    me "Espera, pero..."


translate spanish un_mine_exit_29dbd358:


    "Ella apagó la luz rápidamente, mostrando que la conversación había terminado y que no podía preguntarle nada."


translate spanish un_mine_exit_9a3906dd:


    th "¿Aun así debería?"


translate spanish un_mine_exit_f504d3d8:


    th "¿Preguntarle por qué no me había regañado?"


translate spanish un_mine_exit_a20cefa7:


    "..."


translate spanish un_mine_exit_a129a5fd:


    th "Aun así, Lena..."


translate spanish un_mine_exit_c654370d:


    th "¿Qué ocurrió hoy?"


translate spanish un_mine_exit_c01dd812:


    "Mis pensamientos ralentizaron su ritmo antes de detenerse por completo..."
# Decompiled by unrpyc: https://github.com/CensoredUsername/unrpyc
