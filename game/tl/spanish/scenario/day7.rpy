
translate spanish day7_main_aa6af803:


    "El brillante rayo de luz solar estaba iluminando mis ojos incluso a través de mis párpados."


translate spanish day7_main_5c43311e:


    "Me estiré perezosamente y rápidamente me salí de la cama."


translate spanish day7_main_f254f75a:


    "Eran las dos del mediodía."


translate spanish day7_main_b36d3471:


    th "Parece como si ayer realmente agotara mi cuerpo y requiriese de mucho más tiempo de lo habitual para recuperarme."


translate spanish day7_main_c1bcf764:


    "Me di una vuelta alrededor de la habitación, pensando en qué debería hacer hoy."


translate spanish day7_main_d7fae9fa:


    "Obviamente mi vida en el campamento ya no sería la misma otra vez tras todo lo que escuché sobre ese extraño pionero."


translate spanish day7_main_f80b4755:


    th "Y si todo es como dijo, tengo mucho tiempo por delante."


translate spanish day7_main_6bd2f897:


    "Agarré mi estuche de aseo y me fui afuera."


translate spanish day7_main_e37820fc:


    "Tan solamente había dado un par de pasos cuando alguien corrió hacia mi por detrás."


translate spanish day7_main_fe998b08:


    "Me giré y vi a Alisa."


translate spanish day7_main_b3e42438:


    dv "¡Ey, ve con cuidado!"


translate spanish day7_main_0ffdf7aa:


    "Dijo indiferentemente y continuó corriendo."


translate spanish day7_main_17ea660a:


    "Sostenía algún tipo de bolsa en sus manos..."


translate spanish day7_main_dc306e1e:


    th "Ah, da igual, otro día normal y corriente en este disparatado lugar."


translate spanish day7_main_2e63ec7e:


    "Terminé de cepillarme los dientes, luego me pasé una eternidad limpiándome la cara con el agua helada para despejarme los sentidos y refrescarme la cabeza al menos un poco."


translate spanish day7_main_e3cedd9c:


    "Me sentí un poco mejor tras eso."


translate spanish day7_main_c32db707:


    "De repente, la esperanza me sobrecogió de la nada.{w} No era ese tipo de esperanza en que crees que es probable abandonar este lugar de forma segura..."


translate spanish day7_main_79994a07:


    "Más bien no quería creer que todo lo que me explicó aquel tipo, fuera tan malo como dijo."


translate spanish day7_main_7efc0341:


    pi "Buenos días..."


translate spanish day7_main_215b97bf:


    "Escuché una débil voz proveniente de los bosques."


translate spanish day7_main_091593a0:


    "Alguien permanecía detrás de un árbol."


translate spanish day7_main_70958877:


    me "Buenas..."


translate spanish day7_main_6bedc27f:


    pi "¿Estás preparado?"


translate spanish day7_main_eaccb1bc:


    me "¿Preparado para qué?"


translate spanish day7_main_a93dcec8:


    "Me fijé de nuevo y parecía que era el mismo pionero de la parada de autobús de ayer."


translate spanish day7_main_3d1d50e7:


    "Desde luego, estaba preparado para un día raro, así que no estaba muy sorprendido y empecé esta conversación bastante lúcido."


translate spanish day7_main_c6c61387:


    pi "No le creíste, ¿verdad?"


translate spanish day7_main_cb23542f:


    me "¿De qué estás hablando?"


translate spanish day7_main_74acfe46:


    pi "Todo lo que él dijo..."


translate spanish day7_main_fe4e16b8:


    me "Muy bien, ¿pero cuál es tú opinión sobre lo que está sucediendo?"


translate spanish day7_main_71a0a00c:


    "Ya me había distanciado del mundo exterior y decidí tratar todo lo que me sucedía como si sólo fuera una película de ficción, no como si fuera realidad."


translate spanish day7_main_e427bce7:


    th "Con suerte, me daría más espacio para reflexionar de manera lógica y razonable sobre la situación."


translate spanish day7_main_4ad2a811:


    pi "Hay una salida. ¡Tiene que haberla!"


translate spanish day7_main_ad9f7f99:


    "Dijo emocionado."


translate spanish day7_main_9da02d58:


    me "No lo sé... Pero me gustaría creerlo también."


translate spanish day7_main_6c7b7a89:


    pi "Si solamente tú..."


translate spanish day7_main_2a454c26:


    sl "¡Semyon!"


translate spanish day7_main_47eaf14e:


    "Me di la vuelta."


translate spanish day7_main_5d2bdfb2:


    "Slavya estaba justo a mi lado."


translate spanish day7_main_177f4329:


    sl "¿Con quién estabas hablando?"


translate spanish day7_main_32d6e567:


    me "Ejem... Con nadie...{w} Solamente hablaba conmigo."


translate spanish day7_main_85f46292:


    "Creo que es muy difícil explicarle algo sobre los extraños de los mundos paralelos."


translate spanish day7_main_eb65a9c2:


    th "Probablemente ella no pueda ni verlos de todas formas..."


translate spanish day7_main_f35f8815:


    sl "¿Ya te has preparado?"


translate spanish day7_main_6f63a38c:


    me "¿Preparado? ¿Para otra excursión?"


translate spanish day7_main_f7b781d4:


    sl "¡No!{w} Hoy es el último día de la temporada."


translate spanish day7_main_0657e28f:


    me "¿Qué...?"


translate spanish day7_main_955f98b3:


    "Una estúpida sonrisa apareció en mi cara."


translate spanish day7_main_8ecccc0f:


    sl "Habrá un autobús esta tarde. Partiremos."


translate spanish day7_main_82b7666b:


    me "¡Oh, fantástico!"


translate spanish day7_main_128e12c4:


    th "Estaba preparado para cualquier giro inesperado por increíble que fuera, pero..."


translate spanish day7_main_f4182ba6:


    "¿Puede que fuera esto de lo que el misterioso pionero estaba hablando?"


translate spanish day7_main_2e7eb2bb:


    th "Probablemente lo dijo."


translate spanish day7_main_7bea6fbb:


    th "Bueno, aparentemente tendré que ir en una segunda ocasión y repetir una vez más una semana en este campamento."


translate spanish day7_main_1358eee6:


    th "¡Pero esta vez lo conozco todo!"


translate spanish day7_main_08625009:


    me "Aun no he empacado...{w} Tampoco es que tenga mucho que empacar de todas maneras."


translate spanish day7_main_6f9aa3f1:


    sl "Vale..."


translate spanish day7_main_a04a9677:


    "Slavya cambió la mirada."


translate spanish day7_main_fe79dc7e:


    "Parecía como si quisiera decirme alguna cosa, pero dudó."


translate spanish day7_main_9a860373:


    sl "¡Bueno, pues nos vemos!"


translate spanish day7_main_279eeea1:


    me "Claro..."


translate spanish day7_main_0f8792e5:


    th "Tenía que haber preguntado al menos cuándo partíamos..."


translate spanish day7_main_adbb8b50:


    "Fui a la plaza con la intención de descubrirlo."


translate spanish day7_main_dd4f3123:


    th "¡Tiene que haber alguien que conozca ahí!"


translate spanish day7_main_a261ff3b:


    "Pero lo creas o no, solamente estaba Genda esperándome en la plaza."


translate spanish day7_main_588af67a:


    th "Probablemente todos los pioneros están ocupados empacando."


translate spanish day7_main_6ae3edb7:


    "Tomé asiento en un banco y simplemente me quedé mirando el cielo."


translate spanish day7_main_76f33ef6:


    th "Resulta que hoy tan sólo tengo que esperar a partir, irme en el autobús y despertar otra vez en este campamento, igual que el primer día..."


translate spanish day7_main_2163cc41:


    "Todo estaba ahogado por el silencio."


translate spanish day7_main_a7963ac8:


    "Es exactamente como en ese momento de un día de verano cuando el sol parece detenido en el cielo, los pájaros y los grillos se hubieran ido hacer una siesta tras comer, mientras el viento ahorra sus energías para entregar a la gente su muy esperada y refrescante brisa por la noche."


translate spanish day7_main_7bc45999:


    "De repente se me pasó por la mente que no solamente me había perdido la comida, sino que tampoco tuve un desayuno."


translate spanish day7_main_6274d258:


    "Era ridículo buscar algo en la cantina.{w} Los pioneros seguramente habrían limpiado todo antes de partir."


translate spanish day7_main_fbfba1db:


    "Me rasqué la cabeza y me encaminé hasta la cabaña de Olga Dmitrievna."


translate spanish day7_main_e56fa1ff:


    th "¡Seguro que debería haber algo comestible en el cajón de la mesa!"


translate spanish day7_main_ea19c1fa:


    "Alguien me llamó cuando me aproximaba a la puerta."


translate spanish day7_main_2faacefc:


    "Shurik y Electronik velozmente se aproximaron a la cabaña."


translate spanish day7_main_63a9aae8:


    el "¿Ya has empacado?"


translate spanish day7_main_129cce15:


    me "Ya empaqué."


translate spanish day7_main_cc5ffa9b:


    "Contesté, imitándole."


translate spanish day7_main_18e33553:


    el "No has sido tú mismo estos últimos días..."


translate spanish day7_main_6e4c96dc:


    me "¿Qué te hace decir eso?"


translate spanish day7_main_40962235:


    el "¿Como voy a saberlo? Tú lo sabrás mejor..."


translate spanish day7_main_55493947:


    me "Quiero decir... ¿cómo es que estás tan interesado?"


translate spanish day7_main_481d2b70:


    el "Por nada..."


translate spanish day7_main_10cfbea3:


    sh "¡Un verdadero pionero siempre trata los problemas de sus camaradas como si fueran suyos!"


translate spanish day7_main_ff16defd:


    "Le hice una mirada escéptica."


translate spanish day7_main_13d9478e:


    me "Gracias por vuestro interés. Estoy bien."


translate spanish day7_main_88d04331:


    "En realidad había en el cajón media barra de pan duro y un pedazo de jamón ahumado."


translate spanish day7_main_26352937:


    "Me comí todo con gran disfrute, limpiándolo luego con una agua apestosa que Olga Dmitrievna probablemente solía usar para regar las plantas."


translate spanish day7_main_c67840a3:


    "En cuanto terminé, alguien empezó a llamar a la puerta."


translate spanish day7_main_48555f41:


    me "Entra."


translate spanish day7_main_ed42b147:


    "Ulyana entró deprisa en la habitación."


translate spanish day7_main_bb2205bc:


    us "¡Oh, sólo eres tú!"


translate spanish day7_main_3db311fc:


    "Dijo decepcionada."


translate spanish day7_main_b9fcdeff:


    me "¿Y a quién esperabas ver? ¿Un circo completo con osos?"


translate spanish day7_main_94876b55:


    "Ulyana soltó una risita."


translate spanish day7_main_da46daf5:


    us "¿Dónde está Olga Dmitrievna?"


translate spanish day7_main_595d7b8c:


    me "No lo sé."


translate spanish day7_main_8e0229dd:


    "Me encogí de hombros."


translate spanish day7_main_301d3861:


    us "¿Por qué no?"


translate spanish day7_main_2f877f3b:


    me "No lo sé porque no lo sé...{w} ¿Y qué quieres de ella por cierto?"


translate spanish day7_main_756d3f6a:


    us "Tenía que preguntarle algo antes de partir."


translate spanish day7_main_c1537911:


    me "Vale, le diré que la has estado buscando si la veo."


translate spanish day7_main_95cb8133:


    us "Por cierto, ¿por qué no estás empacando?"


translate spanish day7_main_3178b41b:


    me "Como si tuviera mucho que empacar..."


translate spanish day7_main_fa77eb7d:


    us "¡Bueno, nos vemos!"


translate spanish day7_main_84ce9118:


    "Ella hizo una sonrisa traviesa y se fue corriendo de la cabaña, cerrando de un portazo detrás de ella."


translate spanish day7_main_b0f8ec72:


    "Y aun así, es realmente raro que nadie parece perplejo por la repentina partida."


translate spanish day7_main_80d609d4:


    th "¿Y por qué soy el único que no se la esperaba?"


translate spanish day7_main_e2fab788:


    th "¿Cómo que a todo el mundo parece preocuparle si he empacado todas mis cosas?"


translate spanish day7_main_fa546b6a:


    th "Así mismo, ¿nadie se da cuenta de que ésta será la última vez que nos veremos los unos a los otros...?"


translate spanish day7_main_dcef1d84:


    "De repente me vinieron a la mente la palabras de aquel tipo de ayer en el bosque, sobre todos los lugareños siendo irreales."


translate spanish day7_main_de05ab05:


    th "Bueno, ahora mismo estoy más preparado que nunca para creerle."


translate spanish day7_main_d7317467:


    "El cajón del escritorio donde hallé mi desayuno contenía muchas cosas."


translate spanish day7_main_d43240e6:


    "Cogí un lápiz y un pedazo de papel, los examiné por un rato y luego los introduje dentro de mi bolsillo."


translate spanish day7_main_8dcd7fa6:


    th "¡Por si acaso!"


translate spanish day7_main_39f8f65c:


    "No tenía intenciones de observar a todos los pioneros corriendo por ahí y empacando, así que sencillamente me tumbé en mi cama y no me di ni cuenta de lo aturdido que estaba."


translate spanish day7_main_a20cefa7:


    "..."


translate spanish day7_main_59194f62:


    "Fue la voz de alguien lo que me despertó."


translate spanish day7_main_9e6959c5:


    "Un pionero familiar estaba sentado justo frente a mi, dándome la espalda."


translate spanish day7_main_937c66f2:


    "Comencé a estar un poco acostumbrado a él desde ayer, e incluso me parecía que dejé de temerle."


translate spanish day7_main_d1240714:


    me "Ey, ¿por qué siempre me ocultas tu rostro?"


translate spanish day7_main_cc040a11:


    pi "Porque no deberías verlo."


translate spanish day7_main_0fa7e23d:


    me "Si tú lo dices..."


translate spanish day7_main_9ca20712:


    "No estaba en posición de discutir."


translate spanish day7_main_c1b4c0f1:


    me "Y bien, ¿qué me vas a revelar esta vez?"


translate spanish day7_main_600f0c98:


    pi "Ya sabes que es el último día de la temporada, ¿verdad?"


translate spanish day7_main_fc30fd2b:


    me "Sep."


translate spanish day7_main_6800c2ff:


    pi "¿Y qué, ya has hablado con {i}aquel{/i}?"


translate spanish day7_main_fc30fd2b_1:


    me "Sep."


translate spanish day7_main_cf3c4c1e:


    pi "Y bien, ¿qué te explicó?"


translate spanish day7_main_4a46ef3d:


    me "Nada en especial. Me dijo que hay una forma de escapar de aquí."


translate spanish day7_main_6beb007e:


    "El pionero rompió a reírse a carcajadas."


translate spanish day7_main_caf2ecdd:


    pi "Claro, yo también me creía eso hace mucho tiempo."


translate spanish day7_main_f6352aef:


    me "¿Y ahora?"


translate spanish day7_main_52ab222f:


    pi "¿Y qué {i}es{/i} «ahora»?{w} Tengo mi pasado, la vida de entonces..."


translate spanish day7_main_867d892e:


    "Dejó de hablar durante un rato."


translate spanish day7_main_015d5bda:


    pi "De todos modos, hace ya mucho tiempo de eso, así que en verdad no me acuerdo mucho."


translate spanish day7_main_499952ad:


    pi "El futuro siempre es lo mismo... bucles, bucles y más bucles, repeticiones de la misma historia.{w} ¿Dónde está el {i}ahora{/i}?"


translate spanish day7_main_50a2b7a7:


    me "Bueno, odio decirlo, pero aun no estoy tan perdido en el tiempo como tú."


translate spanish day7_main_09c7c654:


    pi "¡Oh, eso no es nada! ¡Todo eso viene con el tiempo!"


translate spanish day7_main_54556d0f:


    "Soltó una risa diabólica."


translate spanish day7_main_2ede7e21:


    me "Sólo hay una cosa que no entiendo... ¿cuál es la razón por la cual vienes a mi? ¿qué esperas lograr?"


translate spanish day7_main_ed700d2d:


    pi "¿Yo? La verdad es que nada."


translate spanish day7_main_ed4c078d:


    me "¿Pues entonces por qué viniste?"


translate spanish day7_main_239cb75b:


    pi "Es solamente porque tú, él y los otros como nosotros... son las únicas personas reales por aquí."


translate spanish day7_main_cce08df0:


    "Teniendo en cuenta todo lo que ya me había dicho, sencillamente no estaba preparado para creerme que todos los lugareños fueran sólo marionetas en algún tipo de horrible representación teatral."


translate spanish day7_main_4529ad5a:


    me "¿Estás seguro de que tienes razón?"


translate spanish day7_main_6fa19435:


    pi "¿Tener razón sobre qué?"


translate spanish day7_main_311f2261:


    me "Bueno... ¿Es que tienes razón sobre todo?"


translate spanish day7_main_1e5d88ee:


    pi "No puedo tener razón o estar equivocado.{w} No elegí este mundo, no llegué por propia voluntad a él. Solamente estoy aquí. Y tú sólo estás aquí."


translate spanish day7_main_8b97aebf:


    me "Escucha, ya tengo dolor de cabeza de que estés filosofando."


translate spanish day7_main_d4c1382a:


    "Estaba algo desconcertado de cómo estaba tan tranquilo hablando con este misterioso pionero."


translate spanish day7_main_0e575d19:


    th "Bueno, ahí va, justo frente a mí... toda la fantasía y la brujería que está aconteciendo en este campamento."


translate spanish day7_main_265d8152:


    th "Allá vamos... una explicación (por lo menos una parcial) de cómo llegué hasta aquí."


translate spanish day7_main_377ed3b6:


    th "Aquí están... ¡las respuestas que tanto tiempo he estado buscando!"


translate spanish day7_main_0a0561bd:


    "Por otra parte, mi comportamiento era bastante razonable."


translate spanish day7_main_d9e03d7d:


    "Mientras no podía explicar bien qué estaba ocurriendo, este tipo chiflado tan sólo hablaba y hablaba, pero no es que sus palabras hayan cambiado nada."


translate spanish day7_main_8a6c765f:


    "Entonces, ¿qué sentido tiene seguir escuchándolo?"


translate spanish day7_main_6a19df1b:


    pi "¡Oh, no! Pronto comprenderás todo por ti mismo."


translate spanish day7_main_5022bc8a:


    "Alguien estaba llamando a la puerta.{w} Me levanté para abrirla."


translate spanish day7_main_482a0501:


    "Era Slavya en la entrada."


translate spanish day7_main_fad161d6:


    me "¿Viniste a ver a Olga Dmitrievna?"


translate spanish day7_main_37eba565:


    sl "No..."


translate spanish day7_main_7e6970f3:


    me "Pues entra."


translate spanish day7_main_c073a9b1:


    "Estaba a un uno por ciento seguro de que el pionero ya había desaparecido."


translate spanish day7_main_8cf78390:


    "Resultó que estaba en lo cierto."


translate spanish day7_main_6a9ff616:


    "Slavya tomó asiento en la cama, yo me hice un ovillo contra el armario en el otro extremo de la habitación."


translate spanish day7_main_c8621acb:


    "Ella estaba claramente nerviosa."


translate spanish day7_main_ea63ce77:


    me "¿Ha sucedido algo?"


translate spanish day7_main_c18a9682:


    sl "No... Es sólo que hoy es el último día..."


translate spanish day7_main_165306ee:


    me "Bueno, ya soy consciente. Mejor tarde que nunca."


translate spanish day7_main_6a46b661:


    sl "Bueno, por lo que pensé...{w} Quiero decir...{w} Probablemente no nos volvamos a ver nunca más."


translate spanish day7_main_2aa28af8:


    me "Es un mundo pequeño, tal como dicen."


translate spanish day7_main_aaf30b84:


    sl "Pero quizá me podrías dar tú dirección para escribirte."


translate spanish day7_main_f178bf3d:


    th "Se la daría, si al menos yo la supiera."


translate spanish day7_main_9e6bb37c:


    me "Sabes... Hagámoslo de otra forma... dame tu dirección. Te escribiré nada más volver."


translate spanish day7_main_305689ee:


    sl "¿Pero por qué no quieres darme la tuya?"


translate spanish day7_main_a91117ee:


    me "Bueno... Estábamos a punto de mudarnos, así que nunca sabes...{w} Es mejor que yo te escriba."


translate spanish day7_main_f734a826:


    "Intenté poner mi sonrisa más dulce, con tal de hacer mi historia más creíble."


translate spanish day7_main_596c8eed:


    sl "Ah, vale... Está bien pues..."


translate spanish day7_main_a4cfd165:


    "Slavya se levantó y parecía irse."


translate spanish day7_main_98609165:


    me "Ey, espera, ¿qué hay de la dirección?"


translate spanish day7_main_a53cdaac:


    sl "Más tarde."


translate spanish day7_main_318e3c4f:


    "Una expresión mezcla de lamento y decepción cruzó por su rostro."


translate spanish day7_main_1e9c65d7:


    "Cerré la puerta y justo entonces escuché la malvada voz del pionero detrás de mí."


translate spanish day7_main_7fcab065:


    pi "Y bien, ¿feliz ahora? Has herido a una muchacha."


translate spanish day7_main_7700b04d:


    me "¿Con qué se supone que la he herido? ¿Qué debería haberle dicho? Querida mía, ¿escríbele a mi abuela del pueblo?"


translate spanish day7_main_a75b7aef:


    me "¿O debería haberle dado la dirección de un hogar que probablemente no está todavía ni construído?"


translate spanish day7_main_87494573:


    pi "¿Y qué? ¡Yo no soy el guardián de mi hermano!{w} Es tu mundo, no el mío. Me las apaño en el mío."


translate spanish day7_main_febd1fcf:


    "Esa última frase me avergonzó."


translate spanish day7_main_997bc03d:


    me "Sabes qué..."


translate spanish day7_main_5e237961:


    "No logré terminar la frase... alguien estaba llamando a la puerta otra vez."


translate spanish day7_main_6b33d385:


    me "¡Entra!"


translate spanish day7_main_50581468:


    "Ulyana entró rápidamente en la cabaña."


translate spanish day7_main_a406d12c:


    me "¿Por qué tantas prisas, señora?"


translate spanish day7_main_3ea49673:


    "Hice una excesiva reverencia ostentosa."


translate spanish day7_main_4081a71f:


    us "¿Yo? Yo... solamente..."


translate spanish day7_main_440da628:


    "Sus ojos se apartaron rápidamente con cautela y sus mejillas se ruborizaron."


translate spanish day7_main_02437b85:


    us "¡Solamente quería decirte adiós!"


translate spanish day7_main_47d964b9:


    me "Habrá tiempo para eso de todas formas. Después de todo, nos vamos todos en el mismo autobús."


translate spanish day7_main_6a55610a:


    us "Claro, tienes razón, pero es algo vergonzoso delante de todos."


translate spanish day7_main_7420944b:


    me "Oh, ¿así que hay algo de lo que te puedes avergonzar?"


translate spanish day7_main_60bde9d8:


    "Me reí."


translate spanish day7_main_3dc79f49:


    us "¡Ya vale!"


translate spanish day7_main_366345bd:


    "Hizo un puchero."


translate spanish day7_main_a18522d8:


    us "Sólo quería decirte que en realidad no eres un imbécil... De hecho, ¡eres un tipo genial!"


translate spanish day7_main_7dc51ce4:


    "Sus palabras me asombraron."


translate spanish day7_main_f2486061:


    me "Bueno, gracias...{w} También es bueno juntarse contigo."


translate spanish day7_main_4a74cafa:


    us "Bien, ¡y ya está!"


translate spanish day7_main_519137f4:


    "Salió con prisas afuera, dando un portazo fuerte."


translate spanish day7_main_d76f8bb2:


    pi "Ey, ¿no te esperabas eso de ella?"


translate spanish day7_main_b78c6e26:


    me "¿Tú sí?"


translate spanish day7_main_c233607a:


    pi "Yo no tengo nada que ver, ya te lo dije."


translate spanish day7_main_e15ce1bd:


    me "Es como si tú ya supieras qué iba a decir."


translate spanish day7_main_5de5d3d5:


    pi "Tal vez sí... Tal vez no..."


translate spanish day7_main_b6653187:


    me "¿Solamente viniste aquí para burlarte de mí?"


translate spanish day7_main_6ea78a01:


    "Comencé a perder la paciencia."


translate spanish day7_main_e7918564:


    pi "Esa idea también se cruzó por mi mente."


translate spanish day7_main_4d6f2bc2:


    me "¿Pues entonces por qué demonios vienes hasta aquí?"


translate spanish day7_main_54b22d30:


    me "Si necesitara un hombre que estuviera comentando todas mis acciones, ¡contrataría a un psicólogo profesional!"


translate spanish day7_main_3b8fbfa4:


    pi "¿Crees que después de tanto tiempo no estoy tan cualificado como uno de ellos?"


translate spanish day7_main_e499cb70:


    me "Mi estimación es que tras tanto tiempo, te has vuelto loco de remate."


translate spanish day7_main_a9edc4dd:


    pi "Todos los psicólogos son unos chiflados..."


translate spanish day7_main_eb56bdcb:


    me "Sí, pero no todos los chiflados son psicólogos."


translate spanish day7_main_4fdeb53f:


    "Se rio a carcajadas."


translate spanish day7_main_28f25b65:


    pi "¡Sentido del humor, qué alentador! Francamente, tus bromas son bastante sosas.{w} ¿De quién te estás riendo? ¡De ti mismo!"


translate spanish day7_main_7e656166:


    me "Escucha, si no tienes nada que hacer en tu propio mundo, ve y molesta a ese {i}segundo{/i} mundo."


translate spanish day7_main_8cd37642:


    pi "¿Y tú tienes algo que hacer en tu mundo?"


translate spanish day7_main_18cd1dcb:


    "Replicó el pionero sagazmente."


translate spanish day7_main_06f26021:


    me "Sabes, buscaré qué puedo hacer... Empacaré mis cosas, abandonaré este campamento..."


translate spanish day7_main_925fc2f3:


    pi "¿Y luego qué?"


translate spanish day7_main_9791c0b4:


    me "Luego qué...{w} ¡Cómo voy a saberlo!"


translate spanish day7_main_f05adfcc:


    me "Nunca he estado en semejante situación antes, créetelo o no."


translate spanish day7_main_7109a0ab:


    pi "Tan sólo has olvidado que no podrás abandonar el campamento."


translate spanish day7_main_a4f8c07f:


    th "Sí, tenía razón sobre este asunto.{w} Probablemente..."


translate spanish day7_main_c8fda9f8:


    me "Porque hayas fracasado tú en escapar, ¡no significa que yo también vaya a fracasar!"


translate spanish day7_main_f038ee18:


    pi "Tú mandas jefe."


translate spanish day7_main_cc46f69e:


    "Llamaron a la puerta.{w} Tan flojamente que apenas pude escucharlo."


translate spanish day7_main_67e1f584:


    me "Maldita sea, ¡¿quién será ahora?!"


translate spanish day7_main_c78e5705:


    "Siseé en voz baja y grité:"


translate spanish day7_main_6b33d385_1:


    me "¡Entra!"


translate spanish day7_main_abbe1f0a:


    "Pero la puerta no se abrió."


translate spanish day7_main_c5b18002:


    "Así que la abrí yo mismo."


translate spanish day7_main_26171c75:


    "Lena estaba en la entrada."


translate spanish day7_main_40993023:


    "Parecía que la asusté de verdad."


translate spanish day7_main_200803e8:


    me "Oh, lo siento, no quería... ¿Viniste a ver a Olga Dmitrievna?"


translate spanish day7_main_049b021f:


    un "No."


translate spanish day7_main_ed81b2e1:


    "Dijo, mirando al suelo."


translate spanish day7_main_84eb3967:


    me "Entonces, ¿de qué se trata?"


translate spanish day7_main_5f2d59cb:


    th "¿Qué quería Lena de mi?"


translate spanish day7_main_2526c703:


    me "Entra."


translate spanish day7_main_b516a337:


    "Entró y se quedó en medio de la habitación dudando."


translate spanish day7_main_e22a9c2a:


    me "¿Quieres sentarte?"


translate spanish day7_main_d6169916:


    "Señalé una de las camas."


translate spanish day7_main_2d050cb5:


    "Lena dudó un poco más, no obstante al final se sentó."


translate spanish day7_main_ea63ce77_1:


    me "¿Sucedió algo?"


translate spanish day7_main_72b9a973:


    un "No, es sólo que..."


translate spanish day7_main_918776a6:


    "Me hizo una mirada rápida, pero se ruborizó de golpe y apartó la mirada."


translate spanish day7_main_bacbd43d:


    un "¡Ten!"


translate spanish day7_main_989da459:


    "Lena cogió algo de su bolsillo y me lo entregó."


translate spanish day7_main_23db2f78:


    "Estaba estupefacto... era mi teléfono."


translate spanish day7_main_27205bdb:


    me "Pero... ¿De dónde lo sacaste...?"


translate spanish day7_main_8836199f:


    un "Lo encontré en el bosque..."


translate spanish day7_main_e109c728:


    me "Vale, ¿pero por qué crees que es mío?"


translate spanish day7_main_2bc078c7:


    un "Un muchacho me lo dijo..."


translate spanish day7_main_75088da6:


    me "¿Le viste antes?"


translate spanish day7_main_da35aa0b:


    un "No lo sé, no podía verle el rostro, pero iba vestido con el uniforme de pionero."


translate spanish day7_main_c773393c:


    "De golpe, todo estaba claro."


translate spanish day7_main_82b1e370:


    me "¿Y no te preguntaste qué es?"


translate spanish day7_main_30490fdf:


    "Miré a la pantalla... todavía le quedaba algo de batería, por lo tanto el teléfono móvil no debería parecerle solamente una pieza de plástico a Lena."


translate spanish day7_main_4f35044c:


    un "No lo sé, algún tipo de juguete..."


translate spanish day7_main_a3eea1e0:


    me "Sí, tienes razón..."


translate spanish day7_main_577b13f1:


    "Rápidamente abrí la serpiente en el menú y le dejé el teléfono."


translate spanish day7_main_19416c28:


    me "¡Ten, para ti! ¡De recuerdo!"


translate spanish day7_main_afefddf2:


    un "Oh, pero qué haces, no puedo..."


translate spanish day7_main_2ed4ff25:


    "Lena agitaba sus manos como signo para no aceptarlo."


translate spanish day7_main_f4b1f89a:


    me "Cógelo, tengo montones de estos en mi casa."


translate spanish day7_main_cdda1bfc:


    "Se resistió un poco más, pero al final cogió el dispositivo."


translate spanish day7_main_c67a4136:


    un "¿Y qué hago con él?"


translate spanish day7_main_9216d06a:


    me "Aprieta las teclas para moverla a izquierda y derecha, tienes que comerte esas bolitas mientras evitas que toquen tu cola."


translate spanish day7_main_1239b2a3:


    un "¡Guau, es muy interesante!"


translate spanish day7_main_d1046a49:


    "Ella sonrió."


translate spanish day7_main_1aa9d6c3:


    un "¡Gracias! Aun así no tengo nada para ti, qué vergüenza."


translate spanish day7_main_58e414f2:


    me "No necesito nada, gracias."


translate spanish day7_main_d4366f58:


    un "No, ¡eso no está bien!"


translate spanish day7_main_2b2d9484:


    "Dijo con una voz mucho más segura de sí misma que de costumbre."


translate spanish day7_main_39665fa5:


    un "Es el último día después de todo."


translate spanish day7_main_9513cd87:


    me "Sí..."


translate spanish day7_main_f93613f2:


    un "Espero que nos volvamos a encontrar..."


translate spanish day7_main_e16f1ab0:


    me "Creo que sí."


translate spanish day7_main_77f2eecc:


    un "Pues tengo un regalo para ti."


translate spanish day7_main_5109b055:


    me "¿Y qué es?"


translate spanish day7_main_03f55ca9:


    un "Cierra tus ojos."


translate spanish day7_main_7fdc006d:


    "Lo hice."


translate spanish day7_main_deebb2e3:


    un "¡Y promete que no los abrirás hasta que no te lo diga!"


translate spanish day7_main_71d3b8c8:


    me "De acuerdo."


translate spanish day7_main_b472b19d:


    un "No, ¡tienes que prometerlo!"


translate spanish day7_main_1f17061d:


    me "Muy bien, ¡lo prometo!"


translate spanish day7_main_3e527e8e:


    "En un instante sentí un suave beso en mi mejilla."


translate spanish day7_main_7aaa79b3:


    "Estaba ansioso de abrir mis ojos, pero se lo prometí..."


translate spanish day7_main_d8b768ba:


    un "¡Ábrelos!"


translate spanish day7_main_d6d054a1:


    "La habitación estaba vacía."


translate spanish day7_main_188f31b1:


    me "Qué muchacha..."


translate spanish day7_main_026f195c:


    "La única cosa que logré decir."


translate spanish day7_main_9d8b1602:


    pi "Y bien, ¿cómo te sientes, semental?"


translate spanish day7_main_56c91cad:


    "Escuché una risa malvada justo en el lugar en el que Lena había estado sentada."


translate spanish day7_main_0802a5ba:


    me "Así que esa es la nueva forma de tomarme el pelo, ¿eh? ¿Usando a los demás?"


translate spanish day7_main_82d1e669:


    pi "¿Yo? ¿Tomar el pelo? ¡Dios no lo quiera!{w} Ciertamente, ¡conseguiste un beso de una dulce muchacha gracias a mí! Eso sí, en la mejilla..."


translate spanish day7_main_20d94de6:


    "Realmente quería partirle la cara en ese instante, pero no estaba seguro de que fuera un ser corpóreo aquí."


translate spanish day7_main_9183379b:


    me "Deduzco que esta tomadura de pelo no será la última..."


translate spanish day7_main_fcc63fd6:


    "Dije más calmado."


translate spanish day7_main_e02cb0bc:


    pi "Quién sabe, quién sabe...{w} ¿No es divertido?"


translate spanish day7_main_d2749b3c:


    "El pionero se rio a carcajadas. Quizás el Profesor Moriarty se reía así, al anticipar el éxito de su diabólico plan."


translate spanish day7_main_7a033c20:


    me "Tú te lo estás disfrutando, pero yo no."


translate spanish day7_main_33573f9b:


    pi "¡Relájate! Ya no queda casi nada... ¡y luego vendrá tu segunda vez!"


translate spanish day7_main_e2a097db:


    pi "Tras hacer una docena de veces, te merecerás una parada en boxes.{w} Aunque probablemente no me necesitarás para entonces... lo acabarás resolviendo y comprendiendo todo por ti mismo."


translate spanish day7_main_15b2c48a:


    me "Tampoco es que te necesite ahora mismo."


translate spanish day7_main_d7caa3c1:


    pi "Oh, qué ingrato..."


translate spanish day7_main_417a57f0:


    me "¡Basta ya, déjalo!"


translate spanish day7_main_6c7b7a89_1:


    pi "Si sólo..."


translate spanish day7_main_5f318380:


    me "¡Que te calles ya!"


translate spanish day7_main_0d2ff854:


    pi "No lo comprendes..."


translate spanish day7_main_6d00754d:


    me "¡¡¡QUE CIERRES TU MALDITA BOCA!!!"


translate spanish day7_main_2d7f3c73:


    "Grité con tanta fuerza que las paredes temblaron."


translate spanish day7_main_348c1e05:


    "La puerta de entrada se abrió de golpe y entró Alisa."


translate spanish day7_main_69acda8f:


    dv "¿Soy yo, o te has vuelto completamente loco de remate?"


translate spanish day7_main_93c290bf:


    "Me preguntó con temor."


translate spanish day7_main_dd69c3a1:


    me "Bueno, se podría decir que sí."


translate spanish day7_main_a172912c:


    "Contesté furiosamente."


translate spanish day7_main_b7182058:


    dv "¿Por qué estás gritando?"


translate spanish day7_main_5c9c623e:


    me "Porque quiero."


translate spanish day7_main_7efa97c5:


    "Me di cuenta de que la llegada «casual» de Alisa ya había sido planeada por este pionero, y que sería comentada por él de modo que tarde o temprano tendría que taparme los oídos con tal de no escucharle."


translate spanish day7_main_f20d0c38:


    dv "¿Es que te volviste psicótico o algo así?"


translate spanish day7_main_a4579566:


    "Alisa se recostó en una cama de forma despreocupada."


translate spanish day7_main_bec896af:


    me "¿A qué se debe este honor?"


translate spanish day7_main_d28f8e57:


    dv "Tan sólo me dejé caer por aquí... Y tú estás chillando..."


translate spanish day7_main_0a9c213a:


    me "No haces nada viniendo aquí para «sólo dejarte caer por aquí»."


translate spanish day7_main_f58c07db:


    dv "No tengo nada que hacer. He empacado mis cosas. Es un aburrimiento..."


translate spanish day7_main_af374609:


    me "Vale, vale..."


translate spanish day7_main_e9100bc5:


    dv "Si piensas que vine hasta aquí porque..."


translate spanish day7_main_7386d3f8:


    "Me hizo una mirada con rabia y se dio la vuelta."


translate spanish day7_main_2e5dec60:


    dv "¡Realmente no debería haber venido tras eso!"


translate spanish day7_main_8e7c35a6:


    me "¿Tras qué? No dije ni una palabra."


translate spanish day7_main_84a9b240:


    dv "¡Por supuesto! ¡En vez de ello lo pensaste!"


translate spanish day7_main_7179f364:


    me "Oh, vaya, ¿así que ahora lees mentes?"


translate spanish day7_main_9b855aa7:


    dv "No hace falta leer tu mente... lo llevas escrito en tu cara."


translate spanish day7_main_e18b878d:


    "Era difícilmente posible que se viera algo más en mi cara, aparte de la fatiga y la rabia."


translate spanish day7_main_7b79a718:


    me "¿Y qué viste?"


translate spanish day7_main_359525c1:


    dv "¡Eso no es asunto tuyo!"


translate spanish day7_main_4dfbba46:


    me "Bueno, tampoco te estoy reteniendo aquí."


translate spanish day7_main_ee0160d3:


    dv "¡Ni pensarlo! Voy donde quiera. No me digas lo que tengo que hacer."


translate spanish day7_main_7a6ef78d:


    me "Vale, pues quédate aquí, ¡por el amor de Dios!"


translate spanish day7_main_5a25a62a:


    "De todas formas, prefería mucho antes la compañía de Alisa que la del pionero."


translate spanish day7_main_35fda676:


    "Me tumbé y cerré los ojos."


translate spanish day7_main_09b65c63:


    "Le llevó unos cuantos minutos romper el silencio a Alisa."


translate spanish day7_main_35834dca:


    dv "¿De verdad que no quieres explicarme nada?"


translate spanish day7_main_e473fa2f:


    me "¿Por ejemplo?"


translate spanish day7_main_81291356:


    dv "Hoy es el último día a fin de cuentas."


translate spanish day7_main_029a43e7:


    me "Qué bien, ¿estás contenta?"


translate spanish day7_main_87a35bd8:


    dv "Bueno..."


translate spanish day7_main_17f57844:


    "Tartamudeó insegura."


translate spanish day7_main_3930aa38:


    dv "Todo el mundo se va."


translate spanish day7_main_eef28ca7:


    me "Qué alivio."


translate spanish day7_main_6589795d:


    dv "¿Y eso es todo?"


translate spanish day7_main_aa5aa16f:


    me "¿Necesitas algo más?"


translate spanish day7_main_493ea103:


    dv "¿No te gusta estar aquí?"


translate spanish day7_main_d0b8151b:


    "Su voz sonaba diferente que de costumbre."


translate spanish day7_main_26cff5fe:


    me "He visto mejores lugares."


translate spanish day7_main_f853a1e5:


    dv "Desde luego, eres tan estúpido que hablarte es una completa pérdida de tiempo."


translate spanish day7_main_739017d2:


    "Se levantó y se dirigió hacia la salida."


translate spanish day7_main_cfa74516:


    me "Sep, ¡qué tengas suerte!"


translate spanish day7_main_d6ac8c0a:


    "Alisa se giró hacia mí.{w} En su rostro se dibujó una cara de furia."


translate spanish day7_main_546c756b:


    dv "¡¿Podrías haber dicho por lo menos que me echarías de menos?!"


translate spanish day7_main_0b12d1b2:


    me "Claro que sí."


translate spanish day7_main_db268193:


    dv "¡Pelele!"


translate spanish day7_main_06efabf4:


    "Cerró de un portazo fuerte."


translate spanish day7_main_ac6d49df:


    me "¡Estúpida!"


translate spanish day7_main_19e291ef:


    "Por supuesto, Alisa no escuchó mi última línea."


translate spanish day7_main_3f9dd422:


    pi "Qué muñeca más caprichosa, ¿eh?"


translate spanish day7_main_71dbf37a:


    me "Tan buena como tú."


translate spanish day7_main_fe52d02d:


    pi "Y tú, en ese caso."


translate spanish day7_main_dec36634:


    me "Faltaría menos, sigue comparándome contigo."


translate spanish day7_main_cc9559a7:


    "Remarqué con malicia."


translate spanish day7_main_f5e0300a:


    pi "¿Y cuál es la diferencia? Estamos en la misma situación, tan sólo he estado un poco más de tiempo aquí.{w} Bueno, de hecho, mucho más..."


translate spanish day7_main_224b03df:


    me "Y ya te has acabado volviendo completamente loco."


translate spanish day7_main_c299cbd0:


    pi "No es de extrañar."


translate spanish day7_main_6a69d0f4:


    "Su ronca risotada me estaba fastidiando mucho."


translate spanish day7_main_69abf7b9:


    me "Escucha, ¿te he dicho ya que deberías dedicarte a ser actor? ¡Harías un papel brillante de Hannibal Lecter!"


translate spanish day7_main_f93c3816:


    me "Sobre todo porque tú mismo te consideras un psiquiatra."


translate spanish day7_main_5521fd0f:


    pi "Me lo pensaré.{w} Bueno, ¡debo irme ahora! Quizás nos veamos otra vez."


translate spanish day7_main_658edb67:


    me "¡Piérdete!"


translate spanish day7_main_7b4acbfa:


    "Me di la vuelta para verle, pero el pionero ya había desaparecido."


translate spanish day7_main_f23afdfa:


    me "¡Por fin!"


translate spanish day7_main_bc9abf02:


    th "Aun así, ¿por qué vino?"


translate spanish day7_main_0da09332:


    "Puede que sea así que tanto yo, como él y los otros, seamos las únicas personas reales por aquí, pero estaba desesperado por no querer creerlo."


translate spanish day7_main_01d888f8:


    "Todo su discurso era como algún tipo de complejo juego."


translate spanish day7_main_0c605775:


    "Como si tratara de darme alguna pista detrás de otra, tratando de guiarme hacia algo, permitiéndome descubrir alguna trama conspirativa."


translate spanish day7_main_801bab94:


    th "Qué mal que no tenga mucho éxito con eso, ya que estoy sin ideas..."


translate spanish day7_main_a20cefa7_1:


    "..."


translate spanish day7_main_bee56547:


    "El tiempo se arrastraba traicioneramente lento, pero ya son las cinco de la tarde."


translate spanish day7_main_891514f3:


    "Es común durante verano... si uno cuenta los segundos y los minutos parecería incluso que una única hora nunca llegara a su fin, pero si uno piensa en otra cosa entonces el día entero pasa volando rápidamente."


translate spanish day7_main_941b1a51:


    "Decidí empezar a empacar."


translate spanish day7_main_fb69cac0:


    th "Podrían irse sin mí..."


translate spanish day7_main_511be532:


    "Cogí rápidamente todas mis cosas de invierno y las metí dentro de una mochila. Casi estuve a punto de tirarme encima de la cama otra vez, pero la puerta se abrió y entró Olga Dmitrievna."


translate spanish day7_main_d8e5d2bb:


    mt "Oh, veo que ya has empacado.{w} ¡Eso es genial! ¡Vayámonos!"


translate spanish day7_main_ce6ffaad:


    "Me levanté a regañadientes, agarré mi sencillo equipaje y la seguí."


translate spanish day7_main_54c4948c:


    th "Realmente no me preocupa que sucederá a partir de ahora."


translate spanish day7_main_5af78794:


    th "Había sospechado durante mucho tiempo que nada depende de mi en este mundo, y los recientes acontecimientos habían reforzado mi completa confianza sobre esto."


translate spanish day7_main_06390870:


    th "Puede que mañana me despierte en la línea de autobús número 410, o puede que no me despierte en absoluto."


translate spanish day7_main_5826caff:


    th "Y eso es todo compañeros, es así de simple."


translate spanish day7_main_1a9d1f7c:


    th "No tiene sentido quedarse aquí, no hay ningún lugar al que correr."


translate spanish day7_main_8dc6c9ca:


    th "Básicamente, mi única salida es partir en el autobús con todos los demás.{w} Ir hacia lo desconocido."


translate spanish day7_main_e4adaddf:


    th "Eso es todo lo que había estado haciendo durante la última semana... tropezando por un sendero estrecho en una oscuridad negra como el alquitrán, inseguro de saber dónde estaba el comienzo y dónde el final."


translate spanish day7_main_0d0d1634:


    "Estábamos casi cerca de las puertas cuando escuché a alguien, me llamaba por mi nombre."


translate spanish day7_main_592b8e1f:


    bush "Semyon... Semyon..."


translate spanish day7_main_f779d1e3:


    me "Olga Dmitrievna, discúlpame sólo un minuto."


translate spanish day7_main_65032a00:


    mt "Vale, pero ve rápido, ¡o nos iremos sin ti!"


translate spanish day7_main_18687800:


    "Me dirigí hacia los arbustos adonde se escuchó la voz."


translate spanish day7_main_592b8e1f_1:


    bush "Semyon... Semyon..."


translate spanish day7_main_36c554a3:


    "Me abrí paso por el espeso follaje y llegué a un sendero del bosque."


translate spanish day7_main_52117956:


    "La voz parecía estar llamándome de la nada: en un momento se le escuchaba tras un árbol, y otrora parecía estar detrás de mí."


translate spanish day7_main_8d4918eb:


    "Probablemente sea de alguno de los extraños de los mundos paralelos, pero sospecho que éste en concreto no me lo he encontrado todavía antes."


translate spanish day7_main_f24bb417:


    bush "No nos queda mucho tiempo."


translate spanish day7_main_fd341de6:


    me "Te escucho."


translate spanish day7_main_6b2e53ca:


    bush "Sé que ya te has puesto en contacto con {i}él{/i} y con {i}él{/i}."


translate spanish day7_main_fc30fd2b_2:


    me "Sí."


translate spanish day7_main_6c48903a:


    bush "Y sé que te han explicado. No me preguntes cómo lo sé."


translate spanish day7_main_53a56040:


    me "Vale."


translate spanish day7_main_0e7f1fb9:


    bush "Pero debes saber una cosa. La cosa que sé...{w} Hay más de una docena de nosotros aquí. De hecho, más de un centenar..."


translate spanish day7_main_85f19512:


    bush "Pero muchos de ellos se escaparon."


translate spanish day7_main_3cf4cc40:


    "Traté de digerir lo que me dijo y de formular las preguntas adecuadas."


translate spanish day7_main_5c937cbe:


    me "¿Entonces por qué estás aquí?"


translate spanish day7_main_df98d116:


    bush "Permanecí."


translate spanish day7_main_7332342b:


    me "¿Por qué?"


translate spanish day7_main_0f22f523:


    bush "¡Para ayudar a los otros a encontrar la salida!"


translate spanish day7_main_d52f7ebb:


    th "Guau, qué generoso..."


translate spanish day7_main_fc7d44e9:


    me "¿Y por qué debería creerte? Tú ya eres el tercero...{w} Bueno, de hecho, no estoy ni seguro de que no seáis todos solamente alucinaciones."


translate spanish day7_main_6972bbdc:


    bush "No es una cuestión de confianza..."


translate spanish day7_main_ca1e4751:


    me "¿Entonces de qué?"


translate spanish day7_main_be914630:


    bush "¡Es sobre la elección correcta!"


translate spanish day7_main_ac9043c1:


    bush "Considera este campamento como un laberinto gigante... deberías tomar los caminos adecuados para encontrar una salida."


translate spanish day7_main_bc5b6d3b:


    me "Vale, ¿y cómo voy a saber cuáles son los correctos?"


translate spanish day7_main_58019608:


    bush "Lo sabrás... Ven conmigo..."


translate spanish day7_main_1878bc08:


    me "¿Adónde?"


translate spanish day7_main_4a75165b:


    bush "A la segunda vez."


translate spanish day7_main_34fc2c23:


    me "Espera un segundo...{w} Aquel pionero dijo que mi segunda vez comenzaría mañana."


translate spanish day7_main_ac491a56:


    bush "¡Miente!"


translate spanish day7_main_1386fd85:


    "El orador alzó su voz."


translate spanish day7_main_73da8c64:


    bush "¡Se ha vuelto completamente loco y está tratando de destruir a todos los otros!"


translate spanish day7_main_20f48057:


    me "Así que si no voy contigo y parto en el autobús, entonces... seré destruido, ¿cierto?"


translate spanish day7_main_8422eef3:


    bush "No lo sé..."


translate spanish day7_main_c5e0555a:


    me "¿Pues por qué lo dices?"


translate spanish day7_main_f2ac56d7:


    bush "Nadie ha retornado tras una conversa con él."


translate spanish day7_main_50e4f830:


    me "Escucha, ¿por qué tendría que fiarme de ti?"


translate spanish day7_main_aa1b7016:


    "Estaba claramente llegando a mi límite."


translate spanish day7_main_817f4941:


    th "Mientras que aquel pionero solamente me tomaba el pelo y no me parecía un peligro de verdad, este otro en cambio, hablaba con claridad sobre cosas por las que merecía la pena preocuparse."


translate spanish day7_main_9bad067c:


    th "No sabía en cuál de los dos debería confiar."


translate spanish day7_main_e1e025fb:


    th "Este problema es como querer que un ciego nos diga si la luz de la habitación está encendida o apagada."


translate spanish day7_main_355517e6:


    th "Él sólo puede suponerlo.{w} Como yo..."


translate spanish day7_main_068aa355:


    bush "Date prisa, ¡el tiempo se nos agota!"


translate spanish day7_main_aff91d3f:


    me "Ey, espera un segundo..."


translate spanish day7_main_77d3e654:


    bush "Toma una decisión... ¡¿te vienes o no?!"


translate spanish day7_main_f6c00c2d:


    me "Vale, vayámonos."


translate spanish day7_main_3976ac4c:


    th "De todas formas, él resultaba mucho más creíble que el pionero loco."


translate spanish day7_main_f59377cf:


    th "Por supuesto, esta solución podría resultar ser fatal, pero la alternativa no era mejor."


translate spanish day7_main_220c95fc:


    "Iba a dar un palo de ciego..."


translate spanish day7_main_2715a6e1:


    me "De acuerdo, bien ¿adónde vamos?"


translate spanish day7_main_b3a4212e:


    bush "Ya casi estamos ahí..."


translate spanish day7_main_9c4ed245:


    "Mi visión se empezó a nublar y sentí como la consciencia me abandonaba..."


translate spanish day7_main_300388db:


    me "No, sabes qué, no puedo creerte únicamente por un par de minutos."


translate spanish day7_main_92441a59:


    th "Por lo menos conozco al otro tipo un poco más, y sus argumentos me parecen de mayor peso."


translate spanish day7_main_026e81de:


    bush "Lo lamentarás..."


translate spanish day7_main_c9f8c4d3:


    "Esas palabras sonaron como si surgieran de otro mundo."


translate spanish day7_main_c80400a7:


    "La voz parecía desaparecer."


translate spanish day7_main_99e05e2b:


    "Desde luego, no podía estar seguro de si mi elección era la correcta o no."


translate spanish day7_main_f36decb5:


    "Supongo que era una a ciegas obligado por la falta de tiempo."


translate spanish day7_main_f9e8cad7:


    th "No obstante, tanto si estoy en lo cierto como si no... pronto lo descubriremos."


translate spanish day7_main_2c7b78d5:


    "En un par de minutos estaba en la parada de autobús junto a todos los otros pioneros."


translate spanish day7_main_709d3e7d:


    mt "¿Todo el mundo está aquí?"


translate spanish day7_main_14167bf5:


    "Comenzó a decir Olga Dmitrievna."


translate spanish day7_main_044ab0d8:


    mt "Hoy os vais de nuestro campamento, y me gustaría deciros algo antes de partir."


translate spanish day7_main_00123e37:


    "Estaba visiblemente nerviosa y, desesperadamente, se quedó sin palabras."


translate spanish day7_main_019ed77a:


    mt "Espero que recordéis el tiempo que transcurristeis aquí durante vuestra vida, y que retengáis solamente recuerdos agradables sobre Sovyonok."


translate spanish day7_main_b48fe53b:


    mt "También espero que os hayáis convertido un poquito en mejores personas, que hayáis logrado aprender algo y conocido nuevos amigos...{w} Tan solamente... volved el próximo año."


translate spanish day7_main_42256d78:


    "La líder del campamento apartó su mirada.{w} Era como si estuviera tratando de aguantarse las lágrimas para sí misma."


translate spanish day7_main_80682609:


    "No me esperaba que ella se pusiera tan sentimental."


translate spanish day7_main_f4683d53:


    "Aunque su discurso me resultó un completo sin sentido. Como de costumbre."


translate spanish day7_main_09939b29:


    "Los pioneros empezaron a amontonarse dentro del autobús, charlando alegremente."


translate spanish day7_main_4bd26034:


    "Decidí esperar un poco y despedirme apropiadamente de este campamento."


translate spanish day7_main_38f849e4:


    "De repente, sentí una profunda necesidad de lanzar una moneda."


translate spanish day7_main_b4d64ac3:


    "Por supuesto, no hay ninguna fuente por aquí, y sinceramente tampoco sentía que fuera a retornar, pero sigue siendo una superstición y no creo en ellas.{w} Por lo menos nunca creí en ellas antes."


translate spanish day7_main_f881a31d:


    "Al rebuscar entre mis bolsillos, solamente encontré un par de envoltorios de caramelos, un lápiz y un pedazo de papel."


translate spanish day7_main_68eaeb3b:


    "Los sostuve por un instante, luego me puse en cuclillas, puse el papel en el suelo y garabateé unas palabras."


translate spanish day7_main_470a435f:


    "«Estás aquí por una razón»."


translate spanish day7_main_4bd38202:


    "Con una sonrisita por mi estupidez, tiré el pedazo de papel bajo las ruedas del autobús y entré dentro."


translate spanish day7_main_d20ef26b:


    "Encontré un asiento en el medio."


translate spanish day7_main_92e96785:


    "Pero la cosa más extraña era que todo el mundo estaba sentado en parejas, y yo era el único que estaba solo."


translate spanish day7_main_c59d06a0:


    th "Sin embargo, ¿qué más da ahora?"


translate spanish day7_main_a0b475c4:


    th "En un par de horas desapareceré o volveré a empezar de nuevo todo."


translate spanish day7_main_a20cefa7_2:


    "..."


translate spanish day7_main_b9800de2:


    "El autobús se desplazaba lentamente hacia el distrito central, de vez en cuando rebotaba con los baches de la carretera."


translate spanish day7_main_60b0a3f1:


    "Era imposible ver nada más allá con las viejas ventanillas en la oscuridad total de la noche."


translate spanish day7_main_6cd4a42b:


    "De todos modos, no me podía importar gran cosa el paisaje de los alrededores... Simplemente me senté y esperé a lo inevitable."


translate spanish day7_main_637f8d4a:


    "Por primera vez en mucho tiempo, mi cabeza estaba completamente vacía."


translate spanish day7_main_1b370f01:


    "Los pioneros a mi alrededor disfrutaban del viaje."


translate spanish day7_main_eac2b791:


    "Ulyana y Alisa estaban jugando a las cartas."


translate spanish day7_main_18b1408e:


    "Lena estaba leyendo un libro, y Slavya estaba durmiendo."


translate spanish day7_main_8be4c497:


    "Miku estaba tratando en vano de iniciar una conversa con Zhenya, y Zhenya estaba tratando de contenerse con esfuerzo para no empezar a matar gente."


translate spanish day7_main_5400ac8b:


    "Electronik y Shurik estaban construyendo algo, como siempre."


translate spanish day7_main_5ae9bf47:


    "Era el único ignorado completamente por todos."


translate spanish day7_main_6035b571:


    th "Puede que fuera una percepción ligeramente inverosímil... me acostumbré a ser el centro del universo en este campamento, pensé que todo giraba a mi alrededor."


translate spanish day7_main_5ed13ddb:


    th "Bueno, puede que eso fuera así hasta cierto punto, pero aquí y ahora soy sólo un extraño objeto, una molécula fuera de lugar en un patrón armónico de una red de cristal."


translate spanish day7_main_a20cefa7_3:


    "..."


translate spanish day7_main_d602b53d:


    "No estoy seguro de cuánto tiempo pasamos en la carretera, pero el sueño empezó a apoderarse de mí."


translate spanish day7_main_7f8e5aa9:


    "Estaba desesperado luchando contra Morfeo, tratando de mantenerme despierto cuanto tiempo pudiera."


translate spanish day7_main_8f87d7c9:


    th "A fin de cuentas, es bastante posible que hoy fuera el último día de mi vida."


translate spanish day7_main_2fc046f6:


    th "Por lo que es bastante razonable aferrarse tanto como pudiera a estas pocas horas de sin sentido, de existencia inútil..."


translate spanish day7_main_a20cefa7_4:


    "..."


translate spanish day7_main_f6b7f9ad:


    "Repentinamente me acordé del pedazo de papel y sus escasas palabras."


translate spanish day7_main_30775a01:


    th "¡Maldita sea, las he visto antes!"


translate spanish day7_main_e730bb3c:


    th "Cómo podía ser tan ignorante..."


translate spanish day7_main_7a69a252:


    th "Quizá todos estos pioneros, todo este galimatías nubló mi conciencia demasiado..."


translate spanish day7_main_cba5d1e3:


    "Mi cabeza estaba literalmente estallando en preguntas."


translate spanish day7_main_ef6d6baa:


    "Sin embargo, la fatiga física y, aun más importante, la emocional hicieron estragos, así que me dormí..."


translate spanish day7_un_28b2bda0:


    "De algún modo me desperté en mi piso."


translate spanish day7_un_a32e02ac:


    "No me evocó ningún sentimiento... tan sólo otro día más de mi vida, eso es todo."


translate spanish day7_un_7d869d50:


    "Salí de la cama y me fui a la computadora."


translate spanish day7_un_ec63c684:


    "Un mensaje parpadeaba en la barra de tareas."


translate spanish day7_un_4e652ca0:


    "Me senté y me quedé mirando la pantalla."


translate spanish day7_un_e275ca5a:


    th "Alguien me escribió... y eso significa que alguien me necesita todavía."


translate spanish day7_un_3b3f4f87:


    "El mensaje solamente contenía una palabra... «Despierta»."


translate spanish day7_un_002780e6:


    th "¿Qué querrán de mi?{w} Una ID desconocida... ¿será eso spam?{w} ¿Y qué significa... «despierta»...?"


translate spanish day7_un_315e078b:


    th "Si ya no estoy durmiendo."


translate spanish day7_un_bd7d69e4:


    "La ventana del mensaje seguía parpadeando."


translate spanish day7_un_9ba4d0e0:


    "Intenté cerrarla, pero fallé. En vez de ello, otra más apareció.{w} Y una y otra más... Y todas ellas contenían el mismo «despierta»."


translate spanish day7_un_c5553e17:


    me "¡¿Qué demonios queréis de mi?!"


translate spanish day7_un_a5efbe41:


    "Chillé."


translate spanish day7_un_39ba5ebd:


    "Pronto toda la pantalla estaba centelleando con idénticos mensajes."


translate spanish day7_un_395338c2:


    "No podía soportarlo más, lo agarré y lo lancé contra la pared con todas mis fuerzas."


translate spanish day7_un_61465614:


    "Un teléfono sonó."


translate spanish day7_un_7411cb8f:


    th "Qué raro, ¿quién podría ser?"


translate spanish day7_un_191851cc:


    "Descolgué el teléfono y escuche simplemente un «despierta»."


translate spanish day7_un_4f1bf480:


    "En el otro extremo, miles de voces... de hombres, de mujeres, de niños... estaban chillando..."


translate spanish day7_un_92dce532:


    dreamgirl "¡Despierta!"


translate spanish day7_un_e6cd0617:


    "Lancé el teléfono al suelo pero no se rompió y siguió sonando."


translate spanish day7_un_b6784bd0:


    "En un instante, toda la habitación estaba llena de extraños."


translate spanish day7_un_1fe77d6f:


    "Me agarraban de mis manos, me miraba de cerca a mis ojos y me gritaban y gritaban..."


translate spanish day7_un_92dce532_1:


    dreamgirl "¡Despierta!"


translate spanish day7_un_9a3170d8:


    "Me sobresalté en mi cama empapado en sudor frío."


translate spanish day7_un_658777a7:


    th "Ufff... Vaya sueño..."


translate spanish day7_un_f61bd817:


    "Recuperé mis sentidos en un par de minutos y eché un vistazo al reloj."


translate spanish day7_un_0718c34d:


    th "Son las cinco de la tarde.{w} ¡¿Cuánto he dormido?!"


translate spanish day7_un_cd800d7f:


    "Todos los acontecimientos de ayer me pasaron por mi cabeza.{w} Buscando a Lena, la conversación desoladora, Alisa derrotada..."


translate spanish day7_un_1a354f35:


    th "Realmente tenía que hablar con ella, tal vez se habría sosegado durante la noche."


translate spanish day7_un_3f9b887f:


    "Me vestí, salí fuera de la cabaña, dudé un poco mientras reflexionaba en mis pensamientos, luego me dirigí hacia la cabaña de Lena."


translate spanish day7_un_8e4dce15:


    th "¿Qué debo decirle, cómo inicio la conversación?{w} No puedo ir sólo con un «Hola, ¿qué tal? Solamente pasaba por aquí»..."


translate spanish day7_un_061fbae3:


    th "No hay manera en que pudiera darle una lección de forma inmediata."


translate spanish day7_un_7e701d1f:


    "Sin haber resuelto esto, llamé a la puerta.{w} Nadie me contestó."


translate spanish day7_un_6506b798:


    "Llamé a la puerta otra vez y luego la abrí por el pomo."


translate spanish day7_un_89013315:


    th "No está cerrada, qué raro."


translate spanish day7_un_1ff94a41:


    "Sin embargo no había nadie dentro."


translate spanish day7_un_fc00e421:


    th "Bueno, eso significa que Lena no está aquí."


translate spanish day7_un_f9d3aec1:


    th "Cogeré algo de comida, puede que la encuentre en la cantina."


translate spanish day7_un_ba6405b9:


    "Yendo por las hileras de cabañas, no encontré ni un solo pionero."


translate spanish day7_un_ee95ab11:


    th "Eso también es raro, siempre está llena por aquí a esta hora del día."


translate spanish day7_un_1ffe59a2:


    "Tampoco había nadie en la plaza."


translate spanish day7_un_1f9572c5:


    "Al llegar a la cantina, empecé a preocuparme seriamente... es la hora de cenar, pero no hay señal alguna de una muchedumbre de pioneros hambrienta."


translate spanish day7_un_b2fcee60:


    "Si sólo fuera eso...{w} ¡La cantina estaba cerrada!"


translate spanish day7_un_71d41a98:


    "Es como si algo hubiera sucedido durante el día."


translate spanish day7_un_17d7007b:


    "Me senté en el porche y comencé a valorar la situación."


translate spanish day7_un_df3b23b2:


    "Todos los pioneros se habían ido.{w} Sin avisar."


translate spanish day7_un_51b3e31f:


    th "Bueno, desde luego, es difícil de decir que este campamento o la forma en que llegué aquí sea normal, pero aun no me he encontrado ninguna cosa completamente inexplicable durante esta semana."


translate spanish day7_un_2ea6120c:


    th "Habían muchas cosas extrañas, pero..."


translate spanish day7_un_3825b75c:


    me "Quizá se están ocultando en aquel búnker..."


translate spanish day7_un_c25b3dcc:


    "Hice un risita ruidosa."


translate spanish day7_un_8bc30be9:


    "Aunque todo esto es muy misterioso, por decir un eufemismo, ni por asomo estaba cerca de experimentar un ataque de pánico."


translate spanish day7_un_484a3495:


    un "No, es sólo que ellos han partido."


translate spanish day7_un_2df970b0:


    "Casi me da un ataque de corazón del susto.{w} Lena estaba a mi lado."


translate spanish day7_un_6e7d7566:


    me "Tú... Tú...{w} ¡No puedes acercarte sigilosamente a la gente de esa forma!{w} ¡Casi me das un paro cardíaco!"


translate spanish day7_un_be58af4f:


    un "Lo siento..."


translate spanish day7_un_1ee77ef5:


    "Dijo tranquilamente."


translate spanish day7_un_43dff079:


    "Ahora me percaté en sus palabras.{w} «Han partido»..."


translate spanish day7_un_bfcc9a41:


    me "¡Espera, espera un momento! ¡¿Qué quieres decir con que «han partido»?!"


translate spanish day7_un_0f9ef342:


    un "La temporada terminó."


translate spanish day7_un_9c158afb:


    me "¿Y la líder del campamento?"


translate spanish day7_un_e24368b3:


    un "Tenía un asunto del que ocuparse en la ciudad, así que ella se fue con ellos."


translate spanish day7_un_a04aa514:


    me "Vale, ¿y tú?"


translate spanish day7_un_5e51765d:


    un "Me quedé."


translate spanish day7_un_9895a7c7:


    "Lena estaba hablando con una completa calma, como si todo lo que ha sucedido fuera completamente normal y rutinario."


translate spanish day7_un_ae14ffa6:


    me "¿Cómo? ¿Y eso por qué?"


translate spanish day7_un_0128f27c:


    un "¿Hay algún problema?{w} Cuando la temporada se termina, los niños parten para sus hogares, ¿acaso no?"


translate spanish day7_un_630033b9:


    me "¿Y cuándo se decidió que hoy sería el último día de la temporada?"


translate spanish day7_un_35db2fba:


    un "No te lo creerás, pero se decidió desde el primer día.{w} Además, Olga Dmitrievna avisó de eso en la formación."


translate spanish day7_un_06a6d311:


    "Claro, incluso en aquellas formaciones a las que asistí, la última cosa que se me pasó por la mente fue escuchar los avisos de la líder del campamento."


translate spanish day7_un_a3b16791:


    me "¿Entonces por qué nadie me dijo nada?"


translate spanish day7_un_fd89f04c:


    "Estuve esperando una oportunidad para escaparme de este maldito campamento durante tanto tiempo que, cuando finalmente aparece, me quedé dormido cuando surgió."


translate spanish day7_un_325ed1b8:


    th "Bueno, ése es exactamente mi estilo."


translate spanish day7_un_49d22b71:


    un "Les pedí que no lo hicieran."


translate spanish day7_un_b7baf6a3:


    me "Incluso aunque tu les pidieras que no lo hicieran, Olga...{w} ¡Espera! ¡¿Qué?!"


translate spanish day7_un_49d22b71_1:


    un "Lo pedí."


translate spanish day7_un_0dc9f9e8:


    "Dijo Lena con la misma confianza."


translate spanish day7_un_ee2b3274:


    me "¿Y qué, les pediste que me dejaran atrás también?"


translate spanish day7_un_f1be5fed:


    un "Sí."


translate spanish day7_un_65cf65b0:


    me "Y no crees que todo esto es un pequeño..."


translate spanish day7_un_38a6d40e:


    me "¡¿GRAVE ERROR?!"


translate spanish day7_un_8807f63e:


    "Empecé a gritar."


translate spanish day7_un_1a28e6a0:


    un "Nop, es perfectamente normal."


translate spanish day7_un_55f389f6:


    "Le eché un largo vistazo a Lena."


translate spanish day7_un_13e71061:


    "Ni una singular emoción se ha cruzado por el rostro de ella desde que empezamos nuestra conversa."


translate spanish day7_un_5a1b2d40:


    th "Es como si la muchacha que está justo frente a mi ahora mismo, no sea el mismo tipo de muchacha que estaba asustada por los grillos, se ruborizara en cada ocasión y leyera novelas románticas."


translate spanish day7_un_a981adad:


    me "Vale, lo entiendo."


translate spanish day7_un_14f2eabe:


    th "Parece que esta situación está directamente conectada con mi misteriosa llegada a este campamento."


translate spanish day7_un_0b34c373:


    me "¿Quién eres tú?"


translate spanish day7_un_0dd1a163:


    un "¿Yo? Esta mañana era Lena."


translate spanish day7_un_0586ca54:


    "Respondió pacientemente."


translate spanish day7_un_40c0bfd0:


    me "Oh, claro, y yo soy Optimus Prime, nacido hace nueve millones de años atrás en las Tierras Altas de Escocia."


translate spanish day7_un_ebd88aea:


    "No estaba preocupado por ella ni por algo de mi alrededor en ese momento."


translate spanish day7_un_ddce0400:


    "Solamente me estaba enojando con rabia por dentro mío."


translate spanish day7_un_5d53b103:


    un "Encantada de conocerte, soy Lena."


translate spanish day7_un_6b20d365:


    me "Vale, ahora pongámonos serios.{w} Pienso que lo que me estás diciendo es francamente imposible."


translate spanish day7_un_17a451e0:


    me "Hoy, tal como resulta ser, es el último día. De algún modo no sabía nada, y la líder del campamento parte con todos, dejándome atrás simplemente porque tú se lo pediste."


translate spanish day7_un_367d9ad6:


    me "¿No lo encuentras un tanto anormal?"


translate spanish day7_un_4d7961b1:


    un "Quizás así sea..."


translate spanish day7_un_e3b83b37:


    me "¿Me estás ocultando algo?"


translate spanish day7_un_515af32c:


    un "Quizá lo hago..."


translate spanish day7_un_cdd77ebf:


    me "¡Pues escupe todo lo que sepas!"


translate spanish day7_un_7540469a:


    un "Ya te lo dije."


translate spanish day7_un_cc572823:


    "Me tapé la cara con mis manos e hice una respiración profunda."


translate spanish day7_un_db58107e:


    me "Vale, ¿y qué se supone que voy hacer en esta situación?"


translate spanish day7_un_0da946d4:


    un "No lo sé."


translate spanish day7_un_fd0ec1e9:


    me "¿Y qué vas hacer tú?"


translate spanish day7_un_fe61a603:


    un "No lo sé. Me dejaré arrastrar por la corriente."


translate spanish day7_un_aefb602a:


    me "Bien ahí va la corriente."


translate spanish day7_un_185c618b:


    "Siseé, extendiendo bruscamente mis manos."


translate spanish day7_un_b723e2c5:


    un "¿Y por qué te estás preocupando tanto?"


translate spanish day7_un_8f9ecda9:


    "Por primera vez, un ligero interés ha aparecido en su rostro."


translate spanish day7_un_e7810ecb:


    me "¿Y cómo esperas que me sienta? ¿Alegre?"


translate spanish day7_un_0005cbde:


    un "Bueno, no mucho la verdad, pero tú lo has querido."


translate spanish day7_un_c9cfb0f4:


    me "¿Yo? ¡Estás loca!"


translate spanish day7_un_30630964:


    un "¿Y quién dijo ayer que le importaba, que quería estar conmigo?"


translate spanish day7_un_5cc1cbcf:


    me "¿Qué tiene eso que ver con esto?"


translate spanish day7_un_741edec3:


    "Fingí no comprenderlo, pero al fin empecé a caer en la idea de qué iba todo esto."


translate spanish day7_un_15749306:


    un "¡Tu deseo fue concedido!"


translate spanish day7_un_3b03af3a:


    me "Claro, seguro..."


translate spanish day7_un_c11bed91:


    "Murmuré en voz baja."


translate spanish day7_un_5a52a4ba:


    me "Bueno, pues aquí estoy contigo.{w} Lo cual no explica en absoluto qué ocurrió."


translate spanish day7_un_4d28d31b:


    un "Siempre hay un par de trucos que podrían ayudarte a conseguirlo."


translate spanish day7_un_3eac24c4:


    me "Y tú...{w} Así es como..."


translate spanish day7_un_f1be5fed_1:


    un "Sí."


translate spanish day7_un_e7732512:


    "Una sonrisa se dibujó en su rostro."


translate spanish day7_un_ac9ef530:


    me "Ejem, si llego a saber que eres tan insidiosa desde buen principio..."


translate spanish day7_un_4be26182:


    "A decir verdad, de alguna manera me gustaba toda esta situación.{w} Bueno, no me gustaba mucho... pero sentía curiosidad."


translate spanish day7_un_9f8b7dc3:


    th "Por supuesto, lamentaba mucho no haber podido escapar con todos los otros.{w} Sin embargo, ahora tenía un tête-à-tête con Lena."


translate spanish day7_un_a2a35aab:


    th "Eso puede significar que tendré una oportunidad de ver su verdadero yo."


translate spanish day7_un_9b37382f:


    "Y todas las respuestas que estaba anhelando, podrían estar justo aquí y no en alguna misteriosa ciudad a la cual todos los demás se habían dirigido."


translate spanish day7_un_6f985085:


    me "Vale, ¿y bien qué vamos a hacer ahora?"


translate spanish day7_un_493d793b:


    un "¿Nosotros?"


translate spanish day7_un_d1046a49:


    "Ella sonrió."


translate spanish day7_un_2a23e310:


    me "Claro, nosotros.{w} Somos los únicos que quedamos aquí.{w} La cantina está cerrada y dudo mucho que haya quedado algo de comida allí de todos modos."


translate spanish day7_un_46a920a9:


    me "Por supuesto, Olga Dmitrievna debería retornar en algún futuro inmediato, pero..."


translate spanish day7_un_0efd4712:


    un "¿Acaso importa?"


translate spanish day7_un_3d127dd4:


    me "Vale, no importa.{w} ¿Alguna sugerencia?"


translate spanish day7_un_bdb81d42:


    un "Mmmm, ¿qué quieres?"


translate spanish day7_un_cd14a0d6:


    "Preguntó con una pícara sonrisa."


translate spanish day7_un_291eb303:


    me "Habría cogido algo para comer...{w} Y luego no sé."


translate spanish day7_un_4af71b46:


    un "Bueno, vayamos a por algo."


translate spanish day7_un_25f7abbc:


    me "¿Adónde?"


translate spanish day7_un_98dfebb8:


    un "Debería haber algo de comida en mi cabaña."


translate spanish day7_un_8d0f1bba:


    me "Vale, trato hecho."


translate spanish day7_un_58427f30:


    "Entramos y Lena hurgó por el cajón del escritorio."


translate spanish day7_un_2fe68f0d:


    un "Bueno, hay galletas, ¿quieres unas pocas?"


translate spanish day7_un_3e7a1ceb:


    "Me dio un paquete medio abierto de «galletas María»."


translate spanish day7_un_d37f7637:


    me "Anda, qué lindas..."


translate spanish day7_un_27c62366:


    "Me burlé."


translate spanish day7_un_008527ca:


    un "¿Por qué?"


translate spanish day7_un_5cbbe2b2:


    me "Galletas María...{w} Como en mi infancia..."


translate spanish day7_un_34ea9cb7:


    un "Supongo."


translate spanish day7_un_0ac292f9:


    "Sonrió y se sentó a mi lado."


translate spanish day7_un_f71c90f1:


    me "¿Y bien?"


translate spanish day7_un_3cc85031:


    "Pregunté con mi boca llena."


translate spanish day7_un_3875ba7b:


    un "¿Qué?"


translate spanish day7_un_afcb638f:


    me "Pues que qué vamos a hacer..."


translate spanish day7_un_4657d7ce:


    un "No lo sé..."


translate spanish day7_un_8063e0c4:


    "Contempló melancólicamente a través de la ventana."


translate spanish day7_un_c52a621b:


    un "¿Pero qué es lo que quieres?"


translate spanish day7_un_57ffc5a8:


    me "Yo..."


translate spanish day7_un_bce388b7:


    th "Pero en serio, ¿qué es lo que quiero?"


translate spanish day7_un_f7a69265:


    "Hace meramente media hora antes, mi único pensamiento era escapar de este maldito campamento."


translate spanish day7_un_b783c75e:


    "Pero ahora, justo aquí, a su lado, algo parecía cambiar."


translate spanish day7_un_7f75f03b:


    me "Bueno... Tampoco lo sé..."


translate spanish day7_un_f67c85bb:


    un "Piensa más."


translate spanish day7_un_6d6679e5:


    "Se aproximó más cerca y me contempló directamente a mis ojos."


translate spanish day7_un_b97ac4d9:


    me "Bueno, yo... Ya sabes..."


translate spanish day7_un_5036ab46:


    "Mi cara se ruborizó y mi mente se revolucionó."


translate spanish day7_un_619d5ebf:


    "No solamente porque estaba tan próximo a una muchacha que claramente quería algo de mí, sino porque principalmente esa muchacha era Lena."


translate spanish day7_un_28a06992:


    un "¿Y bien?"


translate spanish day7_un_1b595140:


    "Me observó de cerca."


translate spanish day7_un_51149e68:


    me "..."


translate spanish day7_un_9e509f53:


    me "Lena, yo..."


translate spanish day7_un_3875ba7b_1:


    un "¿Qué?"


translate spanish day7_un_4800ea90:


    "Decirle que ella no se veía como ella misma no era nada."


translate spanish day7_un_aa2b3fa8:


    "No estaba preocupado para nada por esta metamorfosis. Más bien estaba preocupado por mí mismo... lo que podría hacer en tal situación."


translate spanish day7_un_48199bb7:


    me "Yo..."


translate spanish day7_un_5bf65010:


    un "¿No querías estar conmigo...?"


translate spanish day7_un_609ebc3b:


    "Me susurró a mi oído, seductoramente."


translate spanish day7_un_2dc0d4db:


    me "Quería...{w} Quiero decir... ¡Quiero!"


translate spanish day7_un_99cb3823:


    un "¿Entonces cuál es el problema?"


translate spanish day7_un_5e5a445c:


    me "Sabes, es todo tan...{w} Además, tengo otras circunstancias..."


translate spanish day7_un_9307c403:


    un "¿Qué circunstancias?"


translate spanish day7_un_182b9bc9:


    "Se apartó de mi."


translate spanish day7_un_f0e8eea2:


    me "Bueno...{w} varias... circunstancias..."


translate spanish day7_un_df31a10a:


    un "No será que prefieres..."


translate spanish day7_un_d49ccc82:


    "Lena se rio."


translate spanish day7_un_5c51ac23:


    me "¡No-no, por qué creíste eso!"


translate spanish day7_un_eca1df05:


    "Parecía que me ruboricé aun más."


translate spanish day7_un_99cb3823_1:


    un "¿Entonces cuál es el problema?"


translate spanish day7_un_2f2888eb:


    me "¿Estás segura de que quieres eso?"


translate spanish day7_un_feb3b3d8:


    "Traté de cambiar de tema inmediatamente."


translate spanish day7_un_ba874842:


    "Después de todo, hace tan sólo una semana atrás tenía mi propia vida normal. Una vida donde todo estaba estrictamente atado y bien organizado, donde no había lugar para las muchachas."


translate spanish day7_un_f3bf26c2:


    "Y ahora estoy en algún tipo de misterioso campamento, sentado junto a Lena y con indicios de haber algo entre nosotros."


translate spanish day7_un_398b3c5d:


    th "¿Cómo debería comportarme en dicha situación?"


translate spanish day7_un_31b01f67:


    un "Sí..."


translate spanish day7_un_a1cfdd9e:


    "Ella frunció el ceño."


translate spanish day7_un_57db0c16:


    un "Por supuesto que quiero, ¿hay algún problema?"


translate spanish day7_un_ae17a426:


    me "Bueno, es mi..."


translate spanish day7_un_3f78e394:


    "Me estaba comportando como si no tuviera siquiera diecisiete años (que era como ahora me veía), sino como si fuera aun más joven."


translate spanish day7_un_d314a10c:


    un "También la mía..."


translate spanish day7_un_d1046a49_1:


    "Sonrió."


translate spanish day7_un_3179bb68:


    "Tenía que escoger qué era lo siguiente que haría... ahora, tras acabarse la temporada y que todos los pioneros se fueran."


translate spanish day7_un_43e408be:


    "Tenía que hallar las respuestas."


translate spanish day7_un_21a442ca:


    "Por encima de todo, solamente tenía que tener cuidado con esta muchacha que podía cambiar su comportamiento e incluso su personalidad, a placer suyo tan fácilmente..."


translate spanish day7_un_1d5c58e7:


    "Pero exactamente en aquel momento estaba poseído por un único pensamiento... «¡¿pero qué diablos?!»"


translate spanish day7_un_a755f898:


    me "Bueno, si no te importa..."


translate spanish day7_un_5cbb3956:


    "Cerré mis ojos."


translate spanish day7_un_ca7b7ca9:


    "Ella no dijo nada, sólo sonrió y se acercó aun más."


translate spanish day7_un_0684f7af:


    "Nuestros labios se fundieron en un largo beso."


translate spanish day7_un_5233788b:


    "Me olvidé de todo en ese instante: me olvidé de este extraño campamento, me olvidé de los pioneros locales que de hecho se fueron, me olvidé de nuestra absurda líder de campamento..."


translate spanish day7_un_4c273632:


    "Me olvidé de mi anterior vida y de mi futuro, si es que alguna vez la recuperaba."


translate spanish day7_un_bddb8f96:


    "Ahora mismo la única que me importaba era Lena, sus suaves labios, su calor que se derramaba profundamente en mi alma, haciéndome arder en mi interior."


translate spanish day7_un_d35a905d:


    "La aparté suavemente."


translate spanish day7_un_3c8c14d4:


    me "Espera...{w} ¿No crees que de esta forma es demasiado...{w} rápido?"


translate spanish day7_un_b672514d:


    un "¿Por qué?"


translate spanish day7_un_d1317e4f:


    "Me sonrió y me miró de tal forma, que inmediatamente sentí que estaba preparado para sumergirme en sus ojos."


translate spanish day7_un_ef40802e:


    un "¿No estás seguro?"


translate spanish day7_un_9513cd87:


    me "Sí..."


translate spanish day7_un_3cb55656:


    "Susurré en voz baja."


translate spanish day7_un_a20cefa7:


    "..."


translate spanish day7_un_67d22f65:


    "En aquel preciso momento amaba verdaderamente a Lena, quería estrecharla aun más fuerte y no dejarla nunca."


translate spanish day7_un_b59d5f4a:


    "Y ella sentía lo mismo, por supuesto."


translate spanish day7_un_94abf778:


    "El tiempo transcurrió muy rápidamente, y nosotros nos fundimos en uno."


translate spanish day7_un_a20cefa7_1:


    "..."


translate spanish day7_un_739a1875:


    "Cuando me desperté, ya era oscuro afuera."


translate spanish day7_un_23b365c0:


    "Salí de la cama, me puse los pantalones y di un paseo alrededor de la habitación."


translate spanish day7_un_f75f09af:


    th "¿Qué fue esto?{w} ¿Fue solamente un instinto animal o tal vez fue algo más?"


translate spanish day7_un_601aa36f:


    th "No, está mal, ¡fue un completo error!"


translate spanish day7_un_98050cd0:


    th "Estoy atrapado en este campamento, he perdido mi oportunidad de escaparme de aquí y, por encima de todo, estoy con una muchacha la cual probablemente tiene doble personalidad y trastorno maníaco depresivo."


translate spanish day7_un_8f52dae4:


    "Miré a la dormida Lena."


translate spanish day7_un_728cd1d4:


    th "Sea lo que sea, ella es maravillosa."


translate spanish day7_un_eebc563a:


    "Todo lo que sucedió entre nosotros, literalmente hace sólo un par de horas, me vino a la mente y sentí escalofríos recorrer mi espalda."


translate spanish day7_un_7858e570:


    th "Qué va, no sé si es correcto o no, pero si pudiera retroceder en el tiempo, lo habría hecho exactamente igual."


translate spanish day7_un_ec183f94:


    "Sonreí ampliamente y me senté cerca de ella en la cama."


translate spanish day7_un_bad_022c8614:


    "Eran casi las diez de la noche."


translate spanish day7_un_bad_1b57fb49:


    th "Bueno, probablemente sea la hora de levantarse."


translate spanish day7_un_bad_bfb63a52:


    "Le di un ligero empujón a Lena en el hombro."


translate spanish day7_un_bad_c09b685c:


    "Ella abrió sus ojos."


translate spanish day7_un_bad_e7c35764:


    me "Buenos días. Bueno, de hecho, es de noche."


translate spanish day7_un_bad_e0a6620a:


    un "Hola."


translate spanish day7_un_bad_c861a148:


    "Sonrió tiernamente."


translate spanish day7_un_bad_5c767700:


    me "Hora de levantarse, dormilona."


translate spanish day7_un_bad_ab39f43d:


    un "¿Es que tienes prisa?"


translate spanish day7_un_bad_33e2c702:


    me "Bueno, no...{w} Pero estamos completamente solos juntos en este campamento..."


translate spanish day7_un_bad_ca19d4ba:


    un "¿Y qué?"


translate spanish day7_un_bad_1b595140:


    "Me echó un buen vistazo."


translate spanish day7_un_bad_5e428d20:


    me "Nada, simplemente...{w} ¿Cuándo volverá Olga Dmitrievna?"


translate spanish day7_un_bad_7144803b:


    un "¿Acaso es eso importante para ti?"


translate spanish day7_un_bad_cd66338b:


    "Puso una cara con una expresión seria en su rostro."


translate spanish day7_un_bad_9e8dd32e:


    me "Bueno, sin alimentos nos vamos a morir aquí."


translate spanish day7_un_bad_e0269484:


    "Me reí."


translate spanish day7_un_bad_2a02ff5d:


    un "Pues eres libre de irte."


translate spanish day7_un_bad_6556b37c:


    "Apartó la mirada hacia la pared."


translate spanish day7_un_bad_b48b240c:


    me "¿Pero cómo puedo irme?"


translate spanish day7_un_bad_4b07d0b6:


    un "En el autobús, por supuesto."


translate spanish day7_un_bad_20781912:


    me "No hay autobuses por aquí."


translate spanish day7_un_bad_ebc83514:


    un "¿Entonces por qué crees que hay aquí una parada de autobús, una para la línea 410?"


translate spanish day7_un_bad_6821f908:


    me "Francamente, no lo sé."


translate spanish day7_un_bad_45c95a7d:


    un "Le dije a Olga Dmitrievna que tenía algunos asuntos urgentes pendientes de resolver contigo aquí, que luego iríamos más tarde."


translate spanish day7_un_bad_ad02f53a:


    me "¿Qué?"


translate spanish day7_un_bad_1ffdefb7:


    "Sentí como si fuera fulminado por un relámpago."


translate spanish day7_un_bad_cca86205:


    "No sabía de qué debería sorprenderme más... el hecho de que haya autobuses circulando por aquí, o por el hecho de que la líder del campamento estuvo de acuerdo en dejar atrás a dos pioneros en un campamento vacío de esta manera."


translate spanish day7_un_bad_b97307f7:


    un "Lo que dije."


translate spanish day7_un_bad_53a970d1:


    me "¿Quieres decir que podemos irnos?"


translate spanish day7_un_bad_d6ccd123:


    un "Adelante, nadie te retiene."


translate spanish day7_un_bad_89256079:


    "Lo dijo todo mientras estaba sentada completamente inmóvil... de hecho, estaba tan quieta que sus palabras me resultaron tan frías como el mármol que daban escalofríos."


translate spanish day7_un_bad_526a1798:


    me "Vale, siento haber reaccionado así... Es sólo que... Es que todo lo que sucedió hoy ha sido una sorpresa total para mí."


translate spanish day7_un_bad_654ececc:


    un "No parecías estar tan sorprendido hace pocas horas atrás."


translate spanish day7_un_bad_039f555e:


    th "Probablemente dije algo mal...{w} Completamente mal."


translate spanish day7_un_bad_b47ccf80:


    me "Bueno, no te ofendas...{w} No nos vamos a quedar aquí hasta el final de los tiempos, ¿verdad?{w} Si hay una forma de irse..."


translate spanish day7_un_bad_cf8bd28b:


    "Lena no dijo nada."


translate spanish day7_un_bad_4cd94bec:


    "Le miraba a la espalda tratando de figurarme y comprender qué pensaba."


translate spanish day7_un_bad_1385de95:


    un "¡Está bien!"


translate spanish day7_un_bad_3fec7cdf:


    "Exclamó alegremente, tras una pausa, luego saltó de la cama y empezó a vestirse rápidamente."


translate spanish day7_un_bad_d9dd4daf:


    un "Venga, empaca tus cosas, ¡me encontrarás en la plaza en diez minutos!"


translate spanish day7_un_bad_a2242d63:


    "Lena se inclinó sobre mí y me dio un apasionado beso."


translate spanish day7_un_bad_01822020:


    me "Muy bien."


translate spanish day7_un_bad_86defb2c:


    "Salí fuera de su cabaña y corrí hasta la cabaña de la líder del campamento."


translate spanish day7_un_bad_6ba4f013:


    "Sinceramente, no tenía casi nada que empacar."


translate spanish day7_un_bad_d7b1f94e:


    "Metí mi ropa de invierno en una mochila, guardé mi teléfono en mi bolsillo y me dirigí hacia la plaza."


translate spanish day7_un_bad_a20cefa7:


    "..."


translate spanish day7_un_bad_623bcbe0:


    "Ya han pasado quince minutos, pero Lena seguía sin estar aquí."


translate spanish day7_un_bad_b4641094:


    "Lo justifiqué con el hecho de que ella tiene muchas cosas que empacar y, de acuerdo a ello, necesita más tiempo para estar preparada."


translate spanish day7_un_bad_a20cefa7_1:


    "..."


translate spanish day7_un_bad_66dd03fe:


    "Sin embargo, tampoco vino en media hora y empecé a sospechar alguna cosa."


translate spanish day7_un_bad_46e4b02b:


    "Mis piernas me llevaron hasta su cabaña antes de que me diera cuenta."


translate spanish day7_un_bad_e0851da8:


    "Me arrojé abriendo la puerta y vi a Lena tumbada en la cama."


translate spanish day7_un_bad_1d0de26a:


    "Todo a su alrededor estaba empapado en sangre... las sábanas, la manta. El suelo estaba manchado por la sangre... y pude ver un enorme corte en el antebrazo de Lena."


translate spanish day7_un_bad_e25e3383:


    "Corrí hasta ella y empecé a sacudirla por los hombros."


translate spanish day7_un_bad_815e4974:


    me "¡Lena! ¡Lena! ¡¿Por qué?!"


translate spanish day7_un_bad_630ef49b:


    "Ella estaba todavía consciente."


translate spanish day7_un_bad_e89187e1:


    un "Hola Semyon."


translate spanish day7_un_bad_777137e4:


    "Una débil sonrisa en sus labios se congeló."


translate spanish day7_un_bad_9a95d0f0:


    me "¡Aguanta! ¡Ey, no te rindas! ¡Pensaré en alguna cosa ahora mismo! ¡Escucha, todo saldrá bien! ¡No te vas a morir!"


translate spanish day7_un_bad_200625b6:


    "Obviamente, no me lo creía ni yo mismo... Lena se había cortado las venas a través de su codo hasta su muñeca."


translate spanish day7_un_bad_e56517e3:


    "Era un corte profundo, y dado todo el tiempo que pasé esperándola en la plaza, ella se había desangrado demasiado."


translate spanish day7_un_bad_df119e7d:


    "Probablemente incluso una ambulancia no podría hacer nada aquí y ahora en este vacío campamento, lejos del mundo, Lena tenía cero posibilidades de sobrevivir..."


translate spanish day7_un_bad_ac6d49df:


    me "¡¿Cómo puedes ser tan tonta?!"


translate spanish day7_un_bad_57856a8b:


    "La abracé y la sostuve estrechándola contra mí."


translate spanish day7_un_bad_6d0fc2fd:


    "Unas lágrimas resbalaban por mis mejillas, desapareciendo en su cabello."


translate spanish day7_un_bad_1b1fd4fe:


    "Nunca en toda mi vida he llorado tan consternadamente."


translate spanish day7_un_bad_ce1f1f67:


    me "¡Tonta! ¿Por qué tuviste que cortarte a lo largo? ¡Todos los demás lo hacen cruzado, y tú vas y lo haces a lo largo!"


translate spanish day7_un_bad_0ab6f7e7:


    un "Lo siento...{w} Ocurrió así..."


translate spanish day7_un_bad_5820dcba:


    "Murmuró débilmente."


translate spanish day7_un_bad_3492de92:


    me "¡¿Pero por qué?! ¡¿Por qué?!"


translate spanish day7_un_bad_3a8a8420:


    un "Estoy cansada... muy cansada..."


translate spanish day7_un_bad_c2ddb952:


    "Lena se quedó callada."


translate spanish day7_un_bad_abd71de2:


    "La miré directamente a sus ojos... todavía estaba consciente, pero el último latido de vida en ella se le estaba muriendo rápidamente."


translate spanish day7_un_bad_19198e2a:


    un "Estoy muy cansada de todo...{w} De llevar una máscara...{w} De sufrir...{w} Solamente quería estar contigo...{w} Pero también te fuiste..."


translate spanish day7_un_bad_b1c8ef56:


    me "¡No me fui a ninguna parte! ¡Estoy aquí! ¡¿Por qué?! ¡¿Por qué lo has hecho?!"


translate spanish day7_un_bad_0b0df940:


    un "Lo siento..."


translate spanish day7_un_bad_68679f3d:


    "Tenía un nudo en la garganta por las lágrimas, incapaz de decir nada."


translate spanish day7_un_bad_d2f8b2b7:


    un "Lo siento...{w} Te veré...{w} Más tarde..."


translate spanish day7_un_bad_359cbafc:


    "La abracé estrechándola aun más fuerte."


translate spanish day7_un_bad_3824bfbf:


    "La respiración de Lena se volvía cada vez más débil y muy pronto se detuvo para siempre."


translate spanish day7_un_bad_4664516e:


    "Horrorizado, salté de la cama."


translate spanish day7_un_bad_912ad41d:


    "Mis ojos se oscurecieron, mi corazón latía salvajamente y localicé el cuchillo, manchado de sangre tirado en el suelo."


translate spanish day7_un_bad_d7fd24dc:


    "Un instante después, lo sostenía en mis manos."


translate spanish day7_un_bad_8f9c66f6:


    "La hoja se detuvo a sólo un milímetro de mi muñeca..."


translate spanish day7_un_bad_8a75b802:


    th "¿Pero para qué? ¿Cómo podría ser de ayuda eso?"


translate spanish day7_un_bad_9c8a7ab2:


    "Me senté ahí completamente alterado y solamente me quedé mirando a Lena."


translate spanish day7_un_bad_ff9eb876:


    me "No, no estás muerta."


translate spanish day7_un_bad_47037f84:


    "Exploté en carcajadas histéricas."


translate spanish day7_un_bad_675c0b56:


    me "Venga vamos, dormilona, ¡es la hora de despertarse!"


translate spanish day7_un_bad_31c290d0:


    "Dije suavemente y la sacudí por sus hombros."


translate spanish day7_un_bad_123bb3db:


    "Pero Lena no despertó."


translate spanish day7_un_bad_c1836b70:


    me "Qué hice... yo...{w} ¡¿Qué he hecho?!"


translate spanish day7_un_bad_7f565988:


    "Salí fuera de la cabaña horrorizado y corrí como un loco."


translate spanish day7_un_bad_90732d43:


    "No sé cuánto tiempo transcurrió, pero al fin me agoté y me derrumbé en el suelo."


translate spanish day7_un_bad_8c7dae16:


    "Un silencio hostil había a mi alrededor, y únicamente las estrellas me miraban desde lo alto en un silencioso reproche."


translate spanish day7_un_bad_9e1de710:


    "Éstas eran las mismas estrellas que Lena admiraba ayer..."


translate spanish day7_un_bad_a5b7f2b5:


    "Otra vez un llanto me desgarró haciéndome llorar."


translate spanish day7_un_bad_c2891e48:


    th "¡¿Por qué, por qué lo hiciste?! ¡¿Por qué me aparté de ella?! ¡¿Adónde me fui?! ¡Nunca la dejé y nunca lo iba hacer!"


translate spanish day7_un_bad_871cc013:


    "Sólo en aquel momento me di cuenta de lo verdaderamente importante que era ella para mí."


translate spanish day7_un_bad_08f145b6:


    "Me di cuenta que a pesar de todas sus peculiaridades, todo lo que sucedió hoy, todo lo que aconteció durante nuestro breve período de tiempo para conocernos, ella se convirtió repentinamente en lo más preciado de mi vida."


translate spanish day7_un_bad_daac228f:


    "Y yo... Me olvidé instantáneamente de ella, de sus sentimientos, tan pronto como escuché algo sobre el maldito autobús."


translate spanish day7_un_bad_824a58dd:


    "Ciertamente, no justifica su acto pero cómo pude haber dejado de pensar en ella..."


translate spanish day7_un_bad_ee990b86:


    "Me quedé ahí por un rato más, observando las estrellas."


translate spanish day7_un_bad_a20cefa7_2:


    "..."


translate spanish day7_un_bad_51f4d2a1:


    "Los árboles se mecían tranquilamente con la suave brisa nocturna por encima de mi cabeza. A los árboles no les importaba un comino lo que me ocurría."


translate spanish day7_un_bad_a33a26f0:


    "El paisaje resultaba familiar."


translate spanish day7_un_bad_3256dae3:


    "Conteniendo mis lágrimas, me dirigí de vuelta hacia el campamento."


translate spanish day7_un_bad_e2267550:


    "Todo aquí parecía estar igual que ayer, como hace pocos días atrás...{w} La plaza, el monumento de Genda... las cabañas de los pioneros... la cabaña de Lena..."


translate spanish day7_un_bad_5dd67afb:


    "Estaba hecho pedazos por dentro mío."


translate spanish day7_un_bad_d4c3458a:


    "Sentía que el dolor iría a romper mi cuerpo en millones de pequeños pedazos en cualquier momento."


translate spanish day7_un_bad_56824cd3:


    "Me caí de rodillas y empecé a pegar puñetazos contra el suelo, hasta que los puños estaban completamente manchados de sangre."


translate spanish day7_un_bad_54b67798:


    th "Si solamente me hubiera percatado un poco antes...{w} Sólo un instante antes, no estoy pidiendo más..."


translate spanish day7_un_bad_3a000f1c:


    th "Ella era tan... tan...{w} Incluso el más mínimo indicio hubiera sido suficiente para ella."


translate spanish day7_un_bad_425b26e9:


    "Únicamente en este momento me di cuenta que Lena había muerto.{w} Y que una parte de mí, había muerto con ella."


translate spanish day7_un_bad_dd13b05c:


    "Probablemente la parte de mí que diría que es la mejor..."


translate spanish day7_un_bad_4212818d:


    "Recuperé mis sentidos tras un rato, de pie en su cabaña."


translate spanish day7_un_bad_a42b463d:


    "La sangre se había secado ya, la luz lunar no se reflejaba en ella."


translate spanish day7_un_bad_51c253d9:


    "Fui a su cama y me senté junto al cuerpo de Lena."


translate spanish day7_un_bad_3eddd6fc:


    "Estaba terriblemente afectado de estar aquí, pero sentía que tenía que decirle algo."


translate spanish day7_un_bad_07109f1d:


    me "Lo siento."


translate spanish day7_un_bad_7cd29b00:


    "Comencé."


translate spanish day7_un_bad_eefec37d:


    me "Es demasiado tarde, desde luego, pero si me puedes escuchar en alguna parte más allá, sólo recuerda, por favor, que te amaré por siempre, ¡para el resto de mi vida!"


translate spanish day7_un_bad_73cd14af:


    "Y esa era la verdad descarnada."


translate spanish day7_un_bad_ab86e84f:


    me "Siento haber ignorado tus sentimientos. Siento que siempre haya pensado sólo en mí mismo. Lo siento todo...{w} Tenía que haber sido yo quien debería haber muerto, no tú."


translate spanish day7_un_bad_41e2281b:


    "Cubrí su cuerpo con una manta y lentamente abandoné la cabaña."


translate spanish day7_un_bad_e845b484:


    "Recuperé la consciencia en la parada del autobús."


translate spanish day7_un_bad_38f3ab16:


    me "Y bien, ¿huyendo, escoria?"


translate spanish day7_un_bad_02a0d7a5:


    "Murmuré ensombrecido para mí mismo."


translate spanish day7_un_bad_5684a1f5:


    th "No podía estar permaneciendo ni un solo minuto más en este campamento."


translate spanish day7_un_bad_cfc84616:


    th "Lena nunca volverá, no tengo excusa para lo que he hecho.{w} Tan sólo esperaré el autobús que me llevará lejos de aquí."


translate spanish day7_un_bad_fb75b41c:


    "Me importaba un comino lo que fuera a sucederme mañana o dentro de una hora."


translate spanish day7_un_bad_8a6bbc18:


    "No me importaban las respuestas, me daba igual cómo llegué hasta aquí..."


translate spanish day7_un_bad_1167379e:


    "Muy pronto vi un destello de una luz tenue a lo lejos.{w} Tampoco estaba sorprendido."


translate spanish day7_un_bad_b2ca87a4:


    "En un minuto, estaba sentado en el vacío autobús número 410 y estaba mirando a través de una deteriorada ventanilla en la oscuridad de la noche."


translate spanish day7_un_bad_238ed2ad:


    "Mi mente estaba en blanco."


translate spanish day7_un_bad_ee87ed80:


    "Todo lo que nos hace humanos... los sentimientos, las emociones, las ambiciones, el sufrimiento... Las sentí todas allí en aquel campamento de pioneros."


translate spanish day7_un_bad_27cdd9e3:


    "Ahora todo lo que tengo es esta noche y el vacío autobús."


translate spanish day7_un_bad_26c4841e:


    "No hay más futuro, no hay más presente."


translate spanish day7_un_bad_eb03b956:


    "Si muero mañana, eso solamente significaría que otro cuerpo humano más ha cesado de existir... mi verdadero yo murió {i}allí{/i} hace pocas horas antes."


translate spanish day7_un_bad_a20cefa7_3:


    "..."


translate spanish day7_un_bad_6bb89a09:


    "No sé cuánto tiempo pasó, pero la fatiga se apoderó de mí."


translate spanish day7_un_bad_75b8b39e:


    "No iba a luchar contra ella, ya que daba igual tanto si me quedaba durmiendo como si me quedaba despierto."


translate spanish day7_un_bad_61f61677:


    "Apenas podía mantener mis ojos abiertos, y muy pronto me rendí..."


translate spanish day7_un_good_1e14927c:


    "No tenía ningún deseo de despertar a Lena."


translate spanish day7_un_good_0f009ba2:


    "Era un placer inexplicable... tan sólo contemplarla cómo dormía."


translate spanish day7_un_good_cb08a804:


    "La gente dice que el sueño revela el verdadero rostro de una persona."


translate spanish day7_un_good_655fad3e:


    "Escuché una leyenda sobre una antigua civilización que tenía una costumbre: antes del matrimonio, la mujer contempla a su prometido durmiendo durante tres noches."


translate spanish day7_un_good_d9e8de3e:


    "Y entonces ella decidía si quería casarse con él o no."


translate spanish day7_un_good_0d6d6fe6:


    "Teniendo eso en mente, podía decir con seguridad que el rostro de Lena no mostraba sino bondad, amabilidad y una inocencia propia de una niña."


translate spanish day7_un_good_d1a6079c:


    th "¿De verdad que puede ser esta la muchacha que noqueó ayer a Alisa?"


translate spanish day7_un_good_416ed528:


    "Bueno, sea lo que fuere, ya pasó todo."


translate spanish day7_un_good_82dadd76:


    "Y nunca he visto a Lena así...{w} La humildad de los primeros días, la histeria cuando estábamos en la isla, la rabia de ayer en la plaza, su pasión de hoy..."


translate spanish day7_un_good_aa392382:


    th "¿Cómo una sola persona puede sobrellevar tantos rasgos de personalidad tan opuestos?"


translate spanish day7_un_good_1a2112d1:


    th "¿Puede que en su caso ella tenga en verdad doble, triple, e incluso cuadrúple trastorno de personalidad?"


translate spanish day7_un_good_a20cefa7:


    "..."


translate spanish day7_un_good_b36c6fa6:


    "No sé cuánto tiempo pasé solamente contemplando a Lena mientras dormía.{w} Puede que hayan sido un par de minutos, o puede que hayan sido un par de horas."


translate spanish day7_un_good_d8d3fddd:


    "Finalmente, ella se despertó."


translate spanish day7_un_good_c02c0710:


    un "Buenos días."


translate spanish day7_un_good_42545412:


    "Lena sonrió tiernamente."


translate spanish day7_un_good_74f1716a:


    un "Ven aquí."


translate spanish day7_un_good_794fe748:


    "Se alzó y me abrazó."


translate spanish day7_un_good_39d29f37:


    "No me resistí y me dejé caer despreocupadamente encima de la cama."


translate spanish day7_un_good_5b22e8ef:


    un "Eres tan maravilloso..."


translate spanish day7_un_good_d8e711d5:


    "Me susurró al oído, besándome en el cuello."


translate spanish day7_un_good_a6b25fe7:


    me "Tú también..."


translate spanish day7_un_good_124fb462:


    "Pero mi voz dudó."


translate spanish day7_un_good_a2aed382:


    un "¿Quieres hacerlo otra vez?"


translate spanish day7_un_good_c57f6bf1:


    "Me preguntó juguetona."


translate spanish day7_un_good_03e5b22a:


    me "Espera."


translate spanish day7_un_good_d35a905d:


    "La aparté con delicadeza."


translate spanish day7_un_good_05b8f5af:


    me "Sabes, todo esto es de todo, menos sencillo.{w} Cuando nos conocimos, parecías ser una persona, luego parecías ser otra, y hoy..."


translate spanish day7_un_good_4e07d0a8:


    me "¡Ahora no puedo comprender para nada quién eres en verdad!"


translate spanish day7_un_good_916ed086:


    me "Y por otro lado, lo que le hiciste a Alisa..."


translate spanish day7_un_good_16158fef:


    un "Otra vez ella..."


translate spanish day7_un_good_32fed232:


    "Lena se cubrió con una manta y se apartó de mi."


translate spanish day7_un_good_9a7e66a7:


    un "¿Acaso no te gusta lo que hicimos? ¿Es que quieres hacerlo también con ella? ¡Ahora no te detendré! Venga, vamos, ¡compáranos! Te daré su dirección. ¡Me podrás decir quién es mejor! O podríamos tener un trío..."


translate spanish day7_un_good_9c1b1178:


    "No sonaba como si estuviera hablando sobre mí o sobre ella."


translate spanish day7_un_good_fa7c02f2:


    "Como si estuviera hablando de completos extraños."


translate spanish day7_un_good_6d308520:


    me "Oh, otra vez así."


translate spanish day7_un_good_9da1463d:


    "Suspiré."


translate spanish day7_un_good_7ba67c69:


    me "Durante hoy, ya deberías haberte dado cuenta de que la última persona en la que pensaría ahora, resultaría ser Alisa."


translate spanish day7_un_good_302fa0d4:


    un "Vale, tienes razón."


translate spanish day7_un_good_a1a1f8ca:


    "Se dio la vuelta hacia mí, me sonrió y me abrazó estrechándome."


translate spanish day7_un_good_9aed7767:


    me "Espera...{w} Todavía tengo que comprender..."


translate spanish day7_un_good_41cfffd5:


    un "¿Por qué?"


translate spanish day7_un_good_485071ca:


    me "No puedo sencillamente...{w} ¡Y sí, me importa! Por ese motivo necesito saber quién eres de verdad."


translate spanish day7_un_good_91c10784:


    un "Yo soy solamente yo."


translate spanish day7_un_good_24e34fe5:


    "Contestó con la misma lánguida voz."


translate spanish day7_un_good_b91c9203:


    "Empecé a perder los estribos de todas estas emociones."


translate spanish day7_un_good_bf337784:


    th "Tengo que calmarme de algún modo."


translate spanish day7_un_good_1c3e144c:


    "Me puse en pie y me moví para sentarme en la cama opuesta."


translate spanish day7_un_good_aed0636a:


    un "Está bien..."


translate spanish day7_un_good_0dadd0ec:


    "Dijo Lena decepcionada y empezó a vestirse."


translate spanish day7_un_good_52eb706a:


    me "No estoy pidiendo mucho.{w} Solamente unas pocas respuestas a unas pocas preguntas."


translate spanish day7_un_good_6e7d937c:


    un "Más tarde. Es la hora de partir."


translate spanish day7_un_good_8dd5c477:


    "Me interrumpió impasiblemente."


translate spanish day7_un_good_1d9b8f65:


    me "¿Qué? ¿Adónde vamos?"


translate spanish day7_un_good_3149b337:


    un "¿Qué quieres decir con adónde? ¡Pues a la ciudad, claro!"


translate spanish day7_un_good_55d5b035:


    me "Pero me dijiste..."


translate spanish day7_un_good_36460bd7:


    un "Bueno, puede que me callara alguna cosa.{w} De hecho, persuadí a Olga Dmitrievna de que todavía teníamos asuntos algo extremadamente urgentes pendientes aquí, que ya iríamos más tarde."


translate spanish day7_un_good_8a5bbf40:


    me "¿Cómo iríamos a la ciudad entonces?"


translate spanish day7_un_good_d8473fed:


    un "Con autobús, por supuesto."


translate spanish day7_un_good_2966b125:


    me "No pasan autobuses por aquí."


translate spanish day7_un_good_fbd62a8b:


    "Hice una sonrisita."


translate spanish day7_un_good_c78f7540:


    un "¿Qué quieres decir que no pasan autobuses? ¡Claro que pasan! La línea 410."


translate spanish day7_un_good_9491336f:


    "No sabía si debería creerlo o no... la situación parecía irse fuera de control... pero no quise discutir."


translate spanish day7_un_good_65bf82fc:


    me "Va-vale..."


translate spanish day7_un_good_0897efd9:


    "Resolví cautelosamente."


translate spanish day7_un_good_a45e6739:


    un "Venga va, ve a empacar, nos encontraremos en la plaza en diez minutos."


translate spanish day7_un_good_505d91ae:


    me "De acuerdo..."


translate spanish day7_un_good_345e129a:


    "No discutí y salí de la cabaña."


translate spanish day7_un_good_63363cb0:


    "A decir verdad, no tenía casi nada que empacar."


translate spanish day7_un_good_a692e759:


    "Sólo metí mi ropa de invierno en una mochila y me dirigí a la plaza."


translate spanish day7_un_good_9c54c577:


    "Lena ya estaba allí esperándome con una mochila de gimnasio en su hombro."


translate spanish day7_un_good_8aac883d:


    me "No parece que cargues con mucho equipaje."


translate spanish day7_un_good_0562d03f:


    un "El suficiente."


translate spanish day7_un_good_06ab0532:


    "Me sonrió."


translate spanish day7_un_good_7373b9f3:


    me "Déjame que lo lleve."


translate spanish day7_un_good_735a4f9a:


    un "Muchas gracias."


translate spanish day7_un_good_f78985a9:


    "Aunque su mochila pareciera casi liviana, un poco de caballerosidad nunca hace daño a nadie."


translate spanish day7_un_good_e265a3a1:


    "Estábamos caminando, Lena estaba constantemente contando chistes, anécdotas, historias divertidas y nunca paraba de reír."


translate spanish day7_un_good_0dc27b80:


    th "Ahora estaba verdaderamente desconcertado de quien tenía a mi lado.{w} Es aquella misma que conocí hace una semana, o es la que vi por primera vez esta mañana."


translate spanish day7_un_good_4cd8fa22:


    "Cuando alcanzamos la parada de autobús, saqué mi teléfono móvil y comprobé qué hora era.{w} Sorprendentemente, todavía quedaba algo de batería."


translate spanish day7_un_good_e43133b4:


    "Eran casi las once de la noche."


translate spanish day7_un_good_8207f0bf:


    th "¿No es un poco tarde para los autobuses?"


translate spanish day7_un_good_928da381:


    un "Oh, ¿qué es esto?"


translate spanish day7_un_good_d83a6ae8:


    "Preguntó Lena con curiosidad."


translate spanish day7_un_good_1ebb2d15:


    me "Oh, sólo un juguete...{w} Ten, es un regalo."


translate spanish day7_un_good_f12d0ee9:


    un "¡Gracias!"


translate spanish day7_un_good_b6aed7ee:


    "Ella sonrió y cogió el teléfono."


translate spanish day7_un_good_a217d0cd:


    un "¿A qué se puede jugar con él?"


translate spanish day7_un_good_48e8a886:


    me "Ya lo verás tú misma más tarde. No es tan difícil."


translate spanish day7_un_good_bb460699:


    "De todas formas, un teléfono móvil es totalmente inútil aquí."


translate spanish day7_un_good_a20cefa7_1:


    "..."


translate spanish day7_un_good_43fb154a:


    "Esperamos por una media hora, Lena siguió hablando y hablando."


translate spanish day7_un_good_21403291:


    "Bueno, debo admitir que encontré sus historias amenas y me sentía cómodo con ella, ¿pero y el autobús?"


translate spanish day7_un_good_71f2297f:


    me "Y qué hay de..."


translate spanish day7_un_good_7b804235:


    "No terminé la oración que vi un destello de faros de luz a lo lejos."


translate spanish day7_un_good_083d605d:


    un "¡Ah, ahí está!"


translate spanish day7_un_good_7f656032:


    "Exclamó Lena entusiasmada."


translate spanish day7_un_good_429932d4:


    "El autobús nos cargó alejándonos lentamente del campamento pionero Sovyonok. Solamente quería creer que nunca jamás volveríamos."


translate spanish day7_un_good_c996f2ed:


    "La oscuridad más allá de las ventanillas nos impidió ver la carretera, los bosques o los campos... en realidad, puede que sea el caso en que hayan desaparecido y que estemos volando a través del vacío hacia lo desconocido."


translate spanish day7_un_good_c2afe0a0:


    "De todas formas, no me podía dejar de preocupar por lo que me rodeaba... Continué escuchando a Lena."


translate spanish day7_un_good_357a77a9:


    "Quizás hoy ella había dicho más palabras que en toda su vida entera."


translate spanish day7_un_good_9bbf9c21:


    me "Sabes..."


translate spanish day7_un_good_b756babd:


    "Le interrumpí al fin."


translate spanish day7_un_good_726d51ff:


    me "Todavía sigo sin entender..."


translate spanish day7_un_good_a604848c:


    un "¿Qué exactamente?"


translate spanish day7_un_good_06ab0532_1:


    "Ella sonrió."


translate spanish day7_un_good_cbb2f3bd:


    me "Cómo podías ser tan insegura, tan humilde al principio, incapaz de decir dos palabras seguidas... Y luego...{w} Así..."


translate spanish day7_un_good_fcc06fdd:


    un "¿Es importante eso?"


translate spanish day7_un_good_f8a3ddee:


    me "Sí, ¡lo es para mi!"


translate spanish day7_un_good_88bb1318:


    un "Bueno..."


translate spanish day7_un_good_d92703f2:


    "Respiró profundamente antes de responder."


translate spanish day7_un_good_440c732d:


    un "Verás, siempre he sido en la forma en que me viste por primera vez cuando nos conocimos...{w} Era así en público."


translate spanish day7_un_good_736c9df5:


    un "No pude vivir como quise desde la infancia. Así que... me puse una máscara..."


translate spanish day7_un_good_b7d1c016:


    "Ella se calló."


translate spanish day7_un_good_0ff7e242:


    un "Aunque... ¡No entremos en detalles sobre eso!"


translate spanish day7_un_good_61aec53e:


    "Lena se rio y se acurrucó agarrando mi brazo, apretando su cuerpo contra el mío estrechamente."


translate spanish day7_un_good_def14ee4:


    me "Vale, lo entiendo..."


translate spanish day7_un_good_8e15585f:


    "Probablemente fuera muy duro para ella hablar de ello ciertamente."


translate spanish day7_un_good_c1c67ccd:


    "De todas maneras, entendí mucho a pesar de tan breve explicación."


translate spanish day7_un_good_6d3e0546:


    me "Pero aun así...{w} ¿Puedo estar seguro de que no volverás a convertirte en esa otra vez...?{w} ¿Que no te esconderás bajo tu caparazón, ni te rendirás a tu enojo o rabia?"


translate spanish day7_un_good_daaa0105:


    un "Eso depende de ti."


translate spanish day7_un_good_bf744da8:


    "Lena sonrió astutamente."


translate spanish day7_un_good_1a37d158:


    "Era ahora cuando me percaté de que estaba haciendo una apuesta muy arriesgada."


translate spanish day7_un_good_62d42734:


    th "Por una parte, me he pasado una semana en este oscuro mundo solamente tratando de hallar algunas respuestas, no tengo nada a mi nombre, y ninguna parte dónde ir, ¡maldita sea!"


translate spanish day7_un_good_8ab802ed:


    th "Por otra parte... hay esta muchacha que obviamente me importa muchísimo..."


translate spanish day7_un_good_5c4556e4:


    "Casi no necesitaba pensar si tenía algún sentimiento por Lena."


translate spanish day7_un_good_0baeb136:


    "Solamente quería estar junto a su lado, mirarla y escuchar su voz.{w} La quería de la forma en que ella era."


translate spanish day7_un_good_fa770e07:


    "Me aseguré a mí mismo de que ésta que está frente a mi es... la verdadera Lena."


translate spanish day7_un_good_10603e2c:


    "Y ahora, cuando estos pensamientos me vienen a la mente... sobre mi anterior vida, mi misteriosa aparición en un campamento de pioneros de la década de los ochenta..."


translate spanish day7_un_good_fc2a9652:


    th "¡No es el momento para tales pensamientos!"


translate spanish day7_un_good_8b4d1244:


    th "Me siento bien al estar cerca de ella, no quiero retornar a ninguna parte, no quiero ninguna respuesta, ¡no quiero saber qué es lo que va a suceder mañana!"


translate spanish day7_un_good_2b03d3cf:


    me "Si fuera por mí... pues solamente quiero creer que tú siempre serás como eres ahora. ¡Porque ésta es la forma en que te amo!"


translate spanish day7_un_good_f3f46bb0:


    un "¡Pues seguiré siendo así, tanto tiempo como permanezcamos juntos!"


translate spanish day7_un_good_6dd013b2:


    "Me abrazó estrechándome más fuerte."


translate spanish day7_un_good_a20cefa7_2:


    "..."


translate spanish day7_un_good_46979785:


    "No sé cuánto tiempo estuvimos en la carretera, pero la conversa de Lena gradualmente comenzó a decrecer."


translate spanish day7_un_good_9a1a8533:


    "Ella apoyó su cabeza en mi hombro, pero seguía contándome una historia sobre su gato tomándose una medicina somnífera, mareándose y sembrando el caos."


translate spanish day7_un_good_a7bfc79d:


    "Lentamente comencé a cabecear de sueño."


translate spanish day7_un_good_ad535286:


    me "Sin embargo, ¿por qué golpeaste a Alisa?"


translate spanish day7_un_good_20b153f9:


    "Me miró de cerca con una expresión seria en su rostro."


translate spanish day7_un_good_bb6fada3:


    un "¡Siempre odié a esa mala puta!"


translate spanish day7_un_good_bab1178b:


    "Lena se rio a carcajadas y me acarició con su morro en el hombro."


translate spanish day7_un_good_a20cefa7_3:


    "..."


translate spanish day7_un_good_a3e8e773:


    "Traté desesperadamente de no caer dormido. Es desconocido lo que nos aguarda allí... tras la siguiente esquina del autobús.{w} Puede que sea una nueva vida la que me espera, o puede que sea el fin de este cuento de hadas... y seré enterrado en el féretro de las paredes y el techo despellejados de mi viejo piso."


translate spanish day7_un_good_115465f3:


    "Pero era una batalla perdida... Morfeo ha invocado legiones de monstruos bajo las órdenes de Fatiga, Cansancio, Desolación e Incertidumbre."


translate spanish day7_un_good_349e1321:


    "No tenía ninguna oportunidad de vencer la batalla frente a estos Cuatro Jinetes del Apocalipsis... y me rendí ante el sueño."


translate spanish day7_us_a1b824b0:


    "Probablemente haya peores cosas en la vida que dormirse mientras estás acurrucado con una pequeña muchacha."


translate spanish day7_us_85a98e77:


    "Y ahora la puerta se abre y un hombre armado se abalanza hacia nosotros."


translate spanish day7_us_b1bd846f:


    "No puedo discernir qué está chillando, pero sus intenciones claramente no son buenas."


translate spanish day7_us_a52663fb:


    "Qué va, no estoy asustado para nada.{w} Más bien incómodo, o incluso avergonzado."


translate spanish day7_us_dea9e5b0:


    "Cuando abrí mis ojos, me llevó algo de tiempo darme cuenta de adónde estaba exactamente."


translate spanish day7_us_aea7d683:


    "Se estaba a oscuras aquí. Solamente una tenue luz brillaba bajo la puerta."


translate spanish day7_us_7be5da68:


    th "¿Por qué hacen habitaciones de trastero sin ventanas?"


translate spanish day7_us_22623e57:


    th "Aunque obviamente es así como debería ser... Pero... ¿Pues por qué diantres alguien se lleva la televisión y el reproductor de vídeo aquí?"


translate spanish day7_us_b5ec365a:


    "Maldecí para mis adentros a los arquitectos de estos edificios, luego sacudí a Ulyana por sus hombros."


translate spanish day7_us_8cb5ffe6:


    me "¡Venga vamos, lévantate ya!"


translate spanish day7_us_80c5bc91:


    "Ella se estiró, y como estábamos tumbados juntos, podía ver sus soñolientos ojos incluso en la oscuridad."


translate spanish day7_us_6d998213:


    us "¿Qué...? ¡Déjame dormir!"


translate spanish day7_us_04545e87:


    "Ulyana trató de apartarse, pero la agarré firmemente por su hombro con mi mano."


translate spanish day7_us_77910b1b:


    me "No sé qué hora es, pero en cualquier caso sería inteligente por nuestra parte salir de aquí."


translate spanish day7_us_bc4db99c:


    us "Ahora no. Más tarde."


translate spanish day7_us_eaa8c8b3:


    "Susurró, medio dormida."


translate spanish day7_us_6089f3a9:


    me "¡Te dije que te despertaras, venga ya!"


translate spanish day7_us_56ce9d85:


    "Me puse en pie de un salto y, fácilmente, la levanté a ella hasta ponerla en pie."


translate spanish day7_us_47c27e5d:


    us "Oouuuaaaah..."


translate spanish day7_us_c0371831:


    "Gimió Ulyana con frustración en su voz."


translate spanish day7_us_748c57a1:


    "Empecé a buscar el interruptor de luz, pero entonces repentinamente escuché pasos afuera de la puerta."


translate spanish day7_us_027c68b0:


    "Mi corazón se hundió."


translate spanish day7_us_f9b338df:


    el "¿Por qué tan temprano?"


translate spanish day7_us_8023c69c:


    sh "Tenemos un montón de trabajo por hacer, ¡no hay tiempo que perder! Sabes que tenemos que hacerlo todo antes de que partamos."


translate spanish day7_us_324f3179:


    el "Muy bien, vale..."


translate spanish day7_us_918870bf:


    "Parece ser que nuestros dos animados cibernéticos están haciendo una visita al edificio de clubs antes de que amanezca hoy."


translate spanish day7_us_555a7855:


    me "¡Shhh!"


translate spanish day7_us_413ae5b6:


    "Le susurré a Ulyana."


translate spanish day7_us_7d2c0a44:


    us "Y qué pasa..."


translate spanish day7_us_a7fcd40d:


    "No logró acabar su oración ya que la silencié con mi mano."


translate spanish day7_us_5f863e08:


    el "Bueno, entiendo todo eso, ¡pero pudimos haber esperado al menos hasta después del desayuno!"


translate spanish day7_us_ac12d769:


    sh "Oh, ¿acaso tienes otra cosa que hacer?"


translate spanish day7_us_62543f01:


    el "No..."


translate spanish day7_us_6fe0e4ad:


    "Escuché algo de duda en la voz de Electronik."


translate spanish day7_us_94943518:


    sh "¿Seguro que no quieres irte hacia la biblioteca alegre y tempranamente otra vez?"


translate spanish day7_us_f9592fdf:


    el "No tuve ni la más mínima intención..."


translate spanish day7_us_bc927332:


    "Respondió fastidiosamente."


translate spanish day7_us_357f75f0:


    sh "Oh, sí, claro, si tú lo dices."


translate spanish day7_us_327920bd:


    "Pronto se les escuchó trabajar tras la puerta... Podía escuchar los martillazos del martillo, el traqueteo de las máquinas y el zumbido de la electrónica."


translate spanish day7_us_2ca031ea:


    "Electronik y Shurik estaban discutiendo sus propios asuntos, así que no presté mucha atención."


translate spanish day7_us_81c60560:


    "Estaba más interesado en saber cuándo se marcharían finalmente del edificio."


translate spanish day7_us_28532eec:


    th "Bastante pronto, la hora del desayuno estaba llegando.{w} Pero si tenemos en cuenta la pasión de Shurik..."


translate spanish day7_us_e1c95826:


    us "¡Suéltame!"


translate spanish day7_us_417eb8f9:


    "Ulyana finalmente se deshizo de mí, pero no alzó su voz después de todo."


translate spanish day7_us_00c87776:


    us "¿Por qué no podemos irnos simplemente?"


translate spanish day7_us_6c39322b:


    "Preguntó en voz baja."


translate spanish day7_us_60ae17bd:


    me "Y qué te crees... ¿qué todo está correcto?"


translate spanish day7_us_38311340:


    us "Bueno, ¿y qué problema hay?"


translate spanish day7_us_9ca91be8:


    me "Hombre, pues que nos hemos pasado toda la noche juntos aquí..."


translate spanish day7_us_2bcd45b7:


    th "Y es obvio lo que ellos asumirían, considerando que tuve la precaución de cerrar la puerta."


translate spanish day7_us_cbf4636b:


    us "¿Y?"


translate spanish day7_us_ba0e85e5:


    me "¿Qué quieres decir con «y»?"


translate spanish day7_us_1fd5a2a5:


    "Suspiré con cansancio."


translate spanish day7_us_2c1c12f9:


    me "¡Confía en mi! Sé que no te importa a ti, ¡pero a mi sí!"


translate spanish day7_us_8b8ddb79:


    us "Vale, está bien, nos esconderemos aquí."


translate spanish day7_us_dcea2b7f:


    "Aceptó Ulyana a regañadientes."


translate spanish day7_us_c441e2a5:


    "Estaba preparado para esperar por un largo rato si fuera necesario, pero apenas pasaron un par de minutos más tarde, que la puerta se abrió y alguien entró."


translate spanish day7_us_ea040539:


    sl "¡Buenas mañanas, muchachos!"


translate spanish day7_us_073bef57:


    el "¡Buenos días!"


translate spanish day7_us_c6c5df39:


    sh "¡Buenas!"


translate spanish day7_us_c50dbd79:


    "Era Slavya."


translate spanish day7_us_36054e3a:


    sl "Quería pediros si tenéis por casualidad cinta adhesiva."


translate spanish day7_us_a716e0cd:


    sh "Tenemos... en alguna parte..."


translate spanish day7_us_1758adf3:


    "Contestó Shurik pensativamente."


translate spanish day7_us_837638ee:


    el "¡Ah, echa un vistazo en la habitación de atrás!"


translate spanish day7_us_d9880b2f:


    "Estas palabras me pusieron la piel de gallina, agarré la maneta de la puerta y la aguanté dominada."


translate spanish day7_us_a3e6a343:


    "Slavya se aproximó a la puerta desde el otro lado e intentó abrirla, pero yo estaba bloqueando la puerta con todas mis fuerzas."


translate spanish day7_us_9daed956:


    sl "Está cerrada."


translate spanish day7_us_338f90c7:


    el "No puede ser... ¡nunca cerramos esa puerta!"


translate spanish day7_us_96e3c2da:


    sh "Dejadme intentarlo."


translate spanish day7_us_9a13cece:


    "Shurik dio un estirón a la maneta de la puerta pero no sucedió nada, aunque me llevó hacer mucha más fuerza para mantenerla bloqueada."


translate spanish day7_us_28d6326b:


    sh "Ahhmm, está atrancada. ¡Echadme una mano!"


translate spanish day7_us_8e62bdbd:


    "En unos instantes, estaban tratando de abrir la puerta ambos junto a Electronik."


translate spanish day7_us_3c57cbd3:


    "Sujeté la maneta como si toda mi vida dependiera de ello, pero de todas maneras fue una breve batalla... mis manos cedieron rápidamente y la solté."


translate spanish day7_us_4a18af52:


    "La puerta se abrió de golpe, el brillo de la luz del sol me cegó, así que no podía ver las sorprendidas caras de Shurik, Electronik y Slavya durante los primeros segundos."


translate spanish day7_us_0cb3a629:


    el "Ejem... ¡Buenos días!"


translate spanish day7_us_9b07033b:


    us "Buenas..."


translate spanish day7_us_db8c0cef:


    "Ulyana estaba detrás de mí, así que no podía verla pero podía sentir su vergüenza en su voz."


translate spanish day7_us_f3289a98:


    sh "¿Y qué estabais haciendo aquí?"


translate spanish day7_us_e4baa3e6:


    "Preguntó Shurik casi como si no estuviera sorprendido."


translate spanish day7_us_b0e31fb0:


    me "Bueno...{w} A decir verdad, estábamos viendo una película. Ulyana trajo la cinta, y tenéis un reproductor de vídeo aquí..."


translate spanish day7_us_f20523f5:


    "Shurik suspicaz se quedó mirando alrededor de la habitación."


translate spanish day7_us_2affcd83:


    "Le di un codazo al costado de Ulyana hasta que captó el mensaje y enseñó la cinta."


translate spanish day7_us_a303a91b:


    el "¿Y qué película es?"


translate spanish day7_us_05d05fbf:


    "Una apenas visible sonrisa se dibujó en el rostro de Electronik."


translate spanish day7_us_8f6c89a0:


    me "Una película normal y corriente... ¡Una de acción! ¡Una nueva!"


translate spanish day7_us_816f2706:


    "Entonces me imaginé lo que él y los demás estaban pensando justo ahora, y me sobrecogió la rabia."


translate spanish day7_us_8ccefc46:


    me "Si os creéis que... nosotros... ¡Nada de ESO!"


translate spanish day7_us_48e6007e:


    "Señalé con mi dedo a Ulyana."


translate spanish day7_us_d2a8f0f0:


    "Ella me miró directamente a mí a su vez."


translate spanish day7_us_82fab5b1:


    sl "Nadie te está acusando de nada a ti..."


translate spanish day7_us_f3e5d2d3:


    "Dijo Slavya sin mirarme..."


translate spanish day7_us_3129c89c:


    me "¡Por lo menos hay una persona razonable entre todos los presentes!"


translate spanish day7_us_cc54c296:


    sl "Aun así..."


translate spanish day7_us_7dd3d65d:


    "Añadió en voz baja."


translate spanish day7_us_ad02f53a:


    me "¿Qué?"


translate spanish day7_us_47020024:


    us "Todo sucedió exactamente como te ha contado él."


translate spanish day7_us_29d44d62:


    "Ulyana se unió a la conversación."


translate spanish day7_us_0313b033:


    us "Estábamos simplemente viendo una película y luego nos dormimos... Era muy tarde..."


translate spanish day7_us_daef1297:


    el "No pensamos en nada...{w} Solamente es una situación estúpida, venga vamos, ya es suficiente..."


translate spanish day7_us_e3a1d5a1:


    "Electronik trató de quitar brasa al fuego bromeando."


translate spanish day7_us_a17efb6c:


    sl "Creo que será Olga Dmitrievna quien lo juzgará."


translate spanish day7_us_2ff596a2:


    "Dijo Slavya con una voz fría."


translate spanish day7_us_04e9a59a:


    me "¡Ey, espera! ¡¿Por qué entrometer a la líder del campamento en esto?!"


translate spanish day7_us_9f99e4ab:


    sl "¿Quién si no?"


translate spanish day7_us_2b3752c2:


    me "Pero que no lo ves, ¡te estamos diciendo la verdad!"


translate spanish day7_us_180a5d04:


    sl "No soy quien para juzgar..."


translate spanish day7_us_2d71c574:


    me "¡Maldita sea! ¡¿Pues quién?! ¡Tú lo has visto todo con tus propios ojos!"


translate spanish day7_us_97f0d6ac:


    sl "Ese asunto le corresponde a la líder del campamento decidirlo."


translate spanish day7_us_11681176:


    "Dijo Slavya en voz baja y se dio la vuelta para irse."


translate spanish day7_us_b93ba56b:


    me "¡Pero espera!"


translate spanish day7_us_279248eb:


    "Aparecí frente a la puerta de un solo salto y le impedí el camino."


translate spanish day7_us_04377996:


    me "¡Escucha!"


translate spanish day7_us_4c8b51f3:


    sl "Esto no es asunto mío..."


translate spanish day7_us_a74cf5b3:


    "Slavya estaba tratando de evitar mi mirada."


translate spanish day7_us_a9ab19d6:


    th "Pareciera que está incómoda también con esta situación."


translate spanish day7_us_5b85d78d:


    sl "Solamente me veo obligada a..."


translate spanish day7_us_c7f85ff8:


    me "¿Quién te obliga a ti? ¿Para qué necesitas hacer todo esto?"


translate spanish day7_us_a6fdad47:


    sl "Porque..."


translate spanish day7_us_60e65045:


    "Ella era incapaz de hallar las palabras para terminar."


translate spanish day7_us_aca23380:


    me "¡Así que no tienes que ir a ninguna parte... no necesitas contarle nada a nadie."


translate spanish day7_us_37eba565:


    sl "No..."


translate spanish day7_us_c72cecb4:


    "Dijo nerviosamente, luego alzó su cabeza y me hizo una mirada intensa."


translate spanish day7_us_19d8e410:


    sl "Lo siento, Semyon."


translate spanish day7_us_fcafb3be:


    us "Sólo deja que se vaya, déjala."


translate spanish day7_us_9a93eedf:


    "Giré mi cabeza hacia Ulyana. Esta fracción de segundo fue suficiente para que Slavya se colara fuera del edificio de clubs."


translate spanish day7_us_b93ba56b_1:


    me "¡Espera! ¡Solamente espera!"


translate spanish day7_us_c3edfb53:


    "Le grité mientras ella corría, pero no tuvo ningún efecto."


translate spanish day7_us_10f920b2:


    th "Perseguir a Slavya, en un fútil intento para impedir que ella fuera a la líder del campamento, no tenía ningún sentido. Si ella tiene la intención de contárselo, yo no puedo impedírselo."


translate spanish day7_us_f94a034f:


    us "De todas formas, ella no puede demostrar nada."


translate spanish day7_us_9367d115:


    "Ulyana hizo una risita."


translate spanish day7_us_271eed32:


    me "¿Qué más da si puede demostrar o no algo?{w} ¿No comprendes tu posición en todo esto?"


translate spanish day7_us_de872d99:


    me "En cualquier situación en la que te ves implicada eres culpable inevitablemente de antemano.{w} Además, en un caso como éste..."


translate spanish day7_us_4becc777:


    us "Bueno, pues somos responsables los dos."


translate spanish day7_us_5345aad1:


    "Sonrió con soberbia astuta."


translate spanish day7_us_0c53b718:


    me "Exacto..."


translate spanish day7_us_767711c4:


    "Salí fuera del edificio y me senté en las escaleras."


translate spanish day7_us_ca5d2b01:


    el "Bueno... Estaremos de tu parte, por si de caso. ¿Verdad, Shurik?"


translate spanish day7_us_f7843a33:


    sh "Eso creo... No comprendo muy claramente que sucedió aquí, pero presumiblemente nada de qué preocuparse."


translate spanish day7_us_06498067:


    el "Vale, ¡nos vamos a desayunar!"


translate spanish day7_us_4c751a5e:


    "Pronto los perdimos de vista."


translate spanish day7_us_b451f12c:


    us "¡Vayamos a comer también!"


translate spanish day7_us_2dcff881:


    "Dijo Ulyana alegremente."


translate spanish day7_us_1cbe0f5f:


    me "¡Sólo piensas en la comida!"


translate spanish day7_us_439a68cf:


    us "¿Para qué te molestas en quejarte?{w} ¿Acaso cambiará algo?"


translate spanish day7_us_42da4d87:


    "Ella tenía razón, al menos sobre eso."


translate spanish day7_us_87f2529e:


    th "Solamente nos quedaba esperar la decisión de la líder del campamento.{w} O que Slavya cambiara de opinión por el camino."


translate spanish day7_us_52e891ef:


    me "Vayámonos..."


translate spanish day7_us_e5f23980:


    "Nos dirigimos hacia la cantina."


translate spanish day7_us_e64b885e:


    "Todo el campamento se había reunido para el desayuno, salvo por Olga Dmitrievna y Slavya."


translate spanish day7_us_29b1cc7a:


    th "Puede que sea lo mejor, supongo."


translate spanish day7_us_eabb99b9:


    us "¿En qué piensas?"


translate spanish day7_us_30a8c2b2:


    "Me preguntó Ulyana alegremente, mientras cogíamos la comida y nos sentábamos."


translate spanish day7_us_df42402e:


    me "Lo mismo."


translate spanish day7_us_682ca4a2:


    us "¡Venga ya, deja de preocuparte de cosas sin importancia!"


translate spanish day7_us_25e396e3:


    me "Tal vez no tenga importancia para ti..."


translate spanish day7_us_7ac5a652:


    th "Bueno, en verdad, ¿qué tiene de particular que sea tan malo?"


translate spanish day7_us_dd7a2cb9:


    th "Toda situación puede ser interpretada desde diferentes ángulos.{w} Y especialmente si tienes una razón..."


translate spanish day7_us_126beeed:


    us "Anda ya, ¿qué es lo peor que puede ocurrir?"


translate spanish day7_us_44eab9f2:


    me "Sabes, nuestra líder de campamento... es un poquito... excéntrica."


translate spanish day7_us_1a257db4:


    us "Excen... Bueno, tal vez sea así, pero no hicimos nada de eso. ¡Nada de nada!"


translate spanish day7_us_6ad7fada:


    me "Espero que ella se crea eso también..."


translate spanish day7_us_edf1846c:


    "Mi relación con Ulyana había mejorado bastante últimamente."


translate spanish day7_us_88166c1e:


    "Al principio, sólo la vi como una cría traviesa y malcriada, pero ahora empezaba a ver el lado bueno de su carácter."


translate spanish day7_us_f3d379ac:


    "Aunque no hubiera mucho..."


translate spanish day7_us_f6c5d224:


    "Y ahora, justo cuando todo comenzaba a ir bien, una dura conversa con la líder del campamento se avecinaba."


translate spanish day7_us_da1b7a68:


    th "Quizá me soñé todo lo que sucedió, y la reacción de Slavya de esta mañana fue causada por la sorpresa..."


translate spanish day7_us_a1bda0c8:


    mt "¿Disfrutando tu desayuno?"


translate spanish day7_us_2515daf1:


    "Olga Dmitrievna estaba por encima mío.{w} Me miraba amenazadoramente."


translate spanish day7_us_df6dbf74:


    us "Sí."


translate spanish day7_us_ea23ce21:


    mt "¿Te importaría explicarme tu comportamiento?"


translate spanish day7_us_00c2b734:


    me "¿Importarme explicar el qué?"


translate spanish day7_us_1a0200f5:


    mt "Bueno, por ejemplo, ¿cómo es que te encontrabas en la habitación de trastero del club de cibernética? ¿Dónde conseguiste la llave? ¿E incluso qué estabas haciendo ahí?"


translate spanish day7_us_40981815:


    "Recordé las circunstancias en que obtuve esta maldita llave."


translate spanish day7_us_03126c7a:


    th "Debería hablar con Electronik sobre ello."


translate spanish day7_us_75eb26f7:


    me "Si Slavya te explicó nuestra versión..."


translate spanish day7_us_581b4f29:


    us "¡Estuvimos viendo una película!"


translate spanish day7_us_483db5e3:


    "Declaró Ulyana con seriedad."


translate spanish day7_us_7996582c:


    mt "¿Toda la noche?"


translate spanish day7_us_17187bef:


    "Preguntó sarcásticamente la líder del campamento."


translate spanish day7_us_8882dbb1:


    us "Luego solamente me quedé dormida, y Semyon se quedó conmigo."


translate spanish day7_us_e8218d47:


    mt "Y pues, ¿no hubo ni una sola oportunidad de que os fuerais a vuestras cabañas?"


translate spanish day7_us_d3ad4e8d:


    us "¡Ni la más mínima!"


translate spanish day7_us_5f234a66:


    mt "¿Y tú qué vas a decir?"


translate spanish day7_us_db73f201:


    me "Bueno, suena bastante estúpido, pero ella tiene razón."


translate spanish day7_us_73f28212:


    mt "¿Esperas que me crea eso?"


translate spanish day7_us_5232e44c:


    me "Bueno, es la verdad..."


translate spanish day7_us_28860bdc:


    mt "No puedo acusaros realmente de nada..."


translate spanish day7_us_f9fe8dfb:


    "Comenzó a decir lentamente la líder del campamento."


translate spanish day7_us_78c9d534:


    mt "Pero por otra parte, toda esta situación está más allá de lo normal. ¡Los pioneros no deben comportarse de esta forma!{w} Y hay demasiadas incoherencias en vuestra historia."


translate spanish day7_us_c7395410:


    me "Lo entendemos..."


translate spanish day7_us_64ed00c4:


    "Lo acepté fatalísticamente."


translate spanish day7_us_ed3d54bd:


    mt "Haré lo siguiente: Ulyana, estás bajo arresto, serás confinada en tu cabaña, ya decidiré qué hacer contigo más tarde."


translate spanish day7_us_2b2111cc:


    "Miré a Ulyana atentamente.{w} Al contrario de lo que me esperaba, no se le veía enfadada."


translate spanish day7_us_b346132a:


    mt "Claramente, no creo que esto sea tu culpa, pero para ser justo..."


translate spanish day7_us_9ac1f10f:


    us "¡Genial! ¡Ya sabes dónde encontrarme en caso de que me necesites!"


translate spanish day7_us_21586975:


    "Ella se puso en pie instantáneamente y se dirigió hacia la salida."


translate spanish day7_us_601dcc29:


    "Olga Dmitrievna no trató de detenerla."


translate spanish day7_us_f86f5ead:


    mt "¡Siempre hay montones de problemas con ella! Y te ha metido en todo esto..."


translate spanish day7_us_463bcf35:


    me "No entiendo realmente en qué me ha metido.{w} Eso sí, pareces ser demasiado severa con ella."


translate spanish day7_us_86c24609:


    mt "¡Muéstrame otro pionero que sea como ella!"


translate spanish day7_us_9b510cae:


    "La líder del campamento se rio."


translate spanish day7_us_d8a59e1a:


    me "Cuando se le castiga bajo arresto por comportarse mal esa es otra historia, pero en este caso..."


translate spanish day7_us_b0a68be7:


    mt "En realidad es que no sé realmente qué estaba sucediendo en verdad, ¡pero mi deber es vigilar por vuestro comportamiento moral! Y esta situación es sospechosa. Muy sospechosa."


translate spanish day7_us_e6d5b5bb:


    me "¿Y cuánto tiempo la vas a mantener encerrada?"


translate spanish day7_us_2c3aefa4:


    mt "No lo sé..."


translate spanish day7_us_c8efaa8d:


    "Olga Dmitrievna valoró por un rato."


translate spanish day7_us_aa5a8210:


    mt "Hoy es la partida.{w} Pero bajo estas circunstancias..."


translate spanish day7_us_015aa4b8:


    me "¿Qué? ¿Qué partida?"


translate spanish day7_us_e5e97d5d:


    "Me sobresalté con dicha palabra."


translate spanish day7_us_2a286c8b:


    mt "El plazo de la temporada termina hoy. Hoy es el último día."


translate spanish day7_us_d3383672:


    me "Mmmm, ¿qué...?"


translate spanish day7_us_ca88a665:


    "Era la única respuesta que podía pronunciar."


translate spanish day7_us_95d81bde:


    th "El último día, significa...{w} ¡Que al fin seré capaz de abandonar este maldito campamento!"


translate spanish day7_us_148a37fc:


    th "¿Quizá es la hora de que mi sufrimiento llegue a su fin, y de que retorne a mi realidad ordinaria?"


translate spanish day7_us_d94530d9:


    me "¿Pero por qué tan de repente?"


translate spanish day7_us_484d29a2:


    mt "¿De repente para quién? ¡Hablé de ello en la formación!"


translate spanish day7_us_b7dab458:


    "Ella estaba en lo cierto. En las formaciones normalmente estaba durmiendo o mirando alrededor, sin escuchar los anuncios."


translate spanish day7_us_230b0b30:


    me "¿Y a qué hora?"


translate spanish day7_us_f2dd2ae4:


    mt "Alrededor de las cinco en punto. ¡No te olvides de estar preparado!"


translate spanish day7_us_b8beaef4:


    th "Apenas tenía nada que empacar."


translate spanish day7_us_d6c3fd46:


    "Se levantó, cogió una bandeja y se iba a ir."


translate spanish day7_us_10a9cce9:


    me "¿Entonces qué le ocurrirá a Ulyana?"


translate spanish day7_us_24e7f6f2:


    mt "No lo sé aun, ya te lo dije.{w} Ella partirá más tarde probablemente."


translate spanish day7_us_a1e9eded:


    me "¿Y eso cómo? ¿Sin los demás?"


translate spanish day7_us_4859697b:


    mt "Bueno, sí."


translate spanish day7_us_9e33a049:


    me "¿Acaso eso es normal?"


translate spanish day7_us_cb6d083f:


    "Estaba verdaderamente sorprendido."


translate spanish day7_us_9ee92636:


    mt "¿Qué tiene de malo?"


translate spanish day7_us_e037db1b:


    th "No sé si «malo», pero es bastante extraño eso seguro."


translate spanish day7_us_4cedb800:


    mt "Bueno, todavía tengo trabajo que hacer."


translate spanish day7_us_a20cefa7:


    "..."


translate spanish day7_us_a075466c:


    "Removí sin sentido mis gachas, las cuales se habían enfríado hacía bastante rato."


translate spanish day7_us_6d8e9a1c:


    th "La partida, ¡la posibilidad de escapar de aquí!"


translate spanish day7_us_d58d3bbd:


    th "Pero por otra parte, Ulyana..."


translate spanish day7_us_ddc689f2:


    "Me sentía culpable frente a ella."


translate spanish day7_us_0b1acb99:


    th "A fin de cuentas, ella estaba castigada mientras que yo, por lo mismo, no lo estaba.{w} Eso no es justo."


translate spanish day7_us_2b277b74:


    "No, no es que quiera compartir su desgracia, pero no creo que sea justo que ella se quede allí sentada encerrada."


translate spanish day7_us_4de1adb3:


    th "Bueno, todavía queda algo de tiempo antes de irnos. Debería ser suficiente para aclarar la situación."


translate spanish day7_us_743c2089:


    "Primero, decidí hablar con Slavya."


translate spanish day7_us_90ef6742:


    th "Espero que se haya calmado."


translate spanish day7_us_246c591f:


    "Normalmente solía encontrarme con Slavya en la plaza, así que fui allí sin dudar."


translate spanish day7_us_21e83890:


    th "¿Por qué exactamente ahí?{w} Porque en este campamento predominantemente me la encontraba ahí."


translate spanish day7_us_b960cd1c:


    "Pero no había ni una sola alma cerca del refugio de Genda."


translate spanish day7_us_bab2fcfe:


    "Me quedé por un rato ahí, mirando el monumento, luego me dirigí hacia la biblioteca."


translate spanish day7_us_645d78b5:


    "Tiene sentido pensar que Zhenya pudiera saber dónde se hallaba su compañera ahora mismo."


translate spanish day7_us_d20a5a40:


    "Tras llamar a la puerta (al recordar mi previa experiencia sabía que ésta no era una costumbre inútil), entré."


translate spanish day7_us_a715c94f:


    "Zhenya dejó de prestar atención al libro y me miró detenidamente."


translate spanish day7_us_6705d775:


    mz "¿Qué quieres?"


translate spanish day7_us_b3e89296:


    me "¿Qué pasa? ¿Por qué reaccionas así? ¿Es que no puedo entrar?"


translate spanish day7_us_3f35d5c7:


    mz "Y qué... ¿viniste aquí sin motivo alguno?{w} Dudo mucho que quieras leer algo."


translate spanish day7_us_ff471f9a:


    me "Bueno, no..."


translate spanish day7_us_dd5de5c4:


    "Los clásicos del marxismo-leninismo no eran mi literatura favorita."


translate spanish day7_us_e0d0e8a3:


    me "Quería saber dónde está Slavya."


translate spanish day7_us_41ebc836:


    mz "¿Para qué quieres saberlo?"


translate spanish day7_us_166c7947:


    "Lo dijo con la certeza de que la conversación se hubiera terminado, de este modo prosiguió con su lectura."


translate spanish day7_us_9c2b514a:


    me "Bueno, puesto que te estoy preguntando, está claro que necesito saberlo."


translate spanish day7_us_4101fa65:


    mz "Debería estar en el embarcadero."


translate spanish day7_us_6dc67cf3:


    "Respondió Zhenya indiferentemente."


translate spanish day7_us_73aab3cd:


    me "Gracias..."


translate spanish day7_us_0faa275e:


    "Conociendo lo que quería saber, me di prisa para salir de este baluarte de mezquindad."


translate spanish day7_us_da95a21f:


    th "Sí, Zhenya tiene un duro espíritu.{w} Por lo menos tengo dificultades para comprenderla."


translate spanish day7_us_ba64d8f0:


    "En el embarcadero algunos pioneros estaban empujando las barcas dentro de las dársenas, mientras otros corrían por aquí y por allá con los remos y las cuerdas."


translate spanish day7_us_e721198a:


    "Tras mirar detenidamente, localicé a Slavya, quien estaba sentada bastante lejos, cerca del agua."


translate spanish day7_us_f191de9e:


    me "¿Aireándote?"


translate spanish day7_us_757f4d47:


    "Pregunté de la forma más neutral que me surgió."


translate spanish day7_us_8b01f8be:


    sl "Sep."


translate spanish day7_us_4e965a2e:


    "Respondió sin darse la vuelta."


translate spanish day7_us_b3a03b6f:


    me "Escucha, yo..."


translate spanish day7_us_89748db4:


    sl "¿...quieres hablar sobre Ulyana?"


translate spanish day7_us_ff471f9a_1:


    me "Bueno, claro..."


translate spanish day7_us_5b802725:


    "Hablando sinceramente, no tenía ni idea de qué hablar.{w} Slavya nos encontró en la habitación de trastero, lo malinterpretó y le contó todo a la líder del campamento."


translate spanish day7_us_70278229:


    "Ahora ya no es asunto suyo, si lo piensas bien."


translate spanish day7_us_752bcc5c:


    th "Por otra parte, sería una completa pérdida de tiempo hablar ahora con Olga Dmitrievna."


translate spanish day7_us_64efd5bd:


    "Quizá sólo sea un deseo subconsciente de querer comprender el razonamiento de Slavya, de ser capaz de exonerarla de alguna forma..."


translate spanish day7_us_0483babd:


    sl "¿Y qué es lo que quieres decirme?"


translate spanish day7_us_eb47c7e6:


    me "Bueno, Ulyana fue castigada.{w} Tal vez incluso no pueda partir con nosotros."


translate spanish day7_us_ecd25c83:


    sl "No me extraña."


translate spanish day7_us_b4a66582:


    me "Sólo quería decirte que nada especial sucedió allí."


translate spanish day7_us_ff2ee0f7:


    sl "Para ser sincera, no lo sé. Solamente tenía que informar de todo."


translate spanish day7_us_c1375773:


    me "Y así lo hiciste. ¿Acaso hizo algo bueno para alguien?"


translate spanish day7_us_3cc8517a:


    "Murmuré para mí mismo."


translate spanish day7_us_dd320952:


    sl "Por supuesto, no estoy segura de que fuera lo correcto..."


translate spanish day7_us_6175a110:


    "Dijo confundida."


translate spanish day7_us_b41d6a32:


    me "Bueno, lo que está hecho, hecho está...{w} ¿Crees que es posible que dejen libre a Ulyana del arresto domiciliario?"


translate spanish day7_us_4d657d7b:


    sl "Te estás preocupando demasiado por ella."


translate spanish day7_us_3568a2ad:


    "Slavya me miró al fin y sonrió."


translate spanish day7_us_c5c058f9:


    me "No es sobre ella... ¡Es una cuestión de justicia!"


translate spanish day7_us_b89f54ec:


    "Aun estando poco confundido, hallé la respuesta adecuada."


translate spanish day7_us_822ca264:


    sl "Bueno, ya conoces a nuestra líder de campamento."


translate spanish day7_us_df7535dd:


    me "La conozco, eso tenlo por seguro."


translate spanish day7_us_ce7c946a:


    sl "Pues sólo espera. En cualquier momento se calmará."


translate spanish day7_us_44dec0d8:


    "Sí, supongo que esa era la mejor opción."


translate spanish day7_us_c8c804ac:


    me "Sí, tienes razón."


translate spanish day7_us_26dcea4e:


    "Me quedé junto a ella callado por un tiempo."


translate spanish day7_us_f37824e2:


    "Slavya no parecía tener muchos deseos de seguir hablando."


translate spanish day7_us_e2cc6a60:


    "Aun así, todavía había una sensación de vacío, pero afrontar la idea de semejante situación incómoda que pudiera alargarse por varias horas, me hizo optar por no agobiarme."


translate spanish day7_us_2e597f6a:


    me "Vale, pues me voy."


translate spanish day7_us_5f596122:


    sl "Nos vemos."


translate spanish day7_us_d1046a49:


    "Ella sonrió."


translate spanish day7_us_7177cc70:


    "En medio de la plaza, me detuve a pensar."


translate spanish day7_us_733af708:


    th "Todavía hay tiempo de sobras hasta la hora de la partida, y no tengo nada que hacer."


translate spanish day7_us_0477db27:


    th "Justo ayer, cuando parecía que me quedaría atrapado aquí para siempre y tendría un montón tiempo... aunque en realidad quedaba muy poco... sentía la necesidad de pensar y actuar rápidamente."


translate spanish day7_us_f5c36e19:


    th "Pero ahora que solamente me quedaban cinco horas antes de abandonar este campamento para siempre, no tenía ni la más remota idea de en qué gastar el tiempo."


translate spanish day7_us_60d55935:


    "Decidí visitar a Ulyana."


translate spanish day7_us_cfa9bf4e:


    th "Después de todo, incluso aunque no le esté permitido salir de la cabaña, eso no significa que no pueda hacerle una visita."


translate spanish day7_us_63ca30ce:


    "Llamé a la puerta suavemente."


translate spanish day7_us_37c07e8b:


    us "¡No eres bienvenido aquí!"


translate spanish day7_us_61815909:


    "Una voz enojada se escuchó detrás de la puerta."


translate spanish day7_us_4c56125d:


    "Abrí la puerta y entré."


translate spanish day7_us_824d3de5:


    me "¡Salve, prisionera!"


translate spanish day7_us_70b26771:


    us "Oh, eres tú..."


translate spanish day7_us_743d2def:


    "Dijo Ulyana decepcionada."


translate spanish day7_us_96cb823a:


    me "Y qué, ¿soy acaso el único que no es bienvenido aquí?"


translate spanish day7_us_15aba1cf:


    "Traté de sonreír."


translate spanish day7_us_467b6ff9:


    us "¿Para qué viniste?"


translate spanish day7_us_43e44e64:


    me "Bueno, pensé que estarías aburriéndote aquí sola."


translate spanish day7_us_e7312d00:


    us "¡Estoy bien!"


translate spanish day7_us_c33a6926:


    me "¿Dónde está Alisa?"


translate spanish day7_us_67ce2945:


    us "Como puedes ver, no está aquí."


translate spanish day7_us_8a6c21b7:


    me "Anda ya, ¿por qué estás tan enojada? Por la mañana estabas de muy buen humor."


translate spanish day7_us_76aad34e:


    us "¿Enojada? ¿Yo?"


translate spanish day7_us_d54c78b5:


    me "Yo no, eso seguro..."


translate spanish day7_us_621db685:


    us "No tienes ninguna otra cosa que hacer y por eso has venido hasta aquí, ¿cierto?"


translate spanish day7_us_0d9da0d6:


    me "Claro, es culpa mía..."


translate spanish day7_us_18e7bfa2:


    "Suspiré teatreramente y bajé la cabeza."


translate spanish day7_us_2259a5d2:


    us "Bueno, pues siéntate."


translate spanish day7_us_83c16d37:


    "Me senté en la cama de enfrente."


translate spanish day7_us_f9c0308a:


    us "Anda, cuéntame algo."


translate spanish day7_us_36e3343b:


    me "¡Intentemos demostrar de alguna manera a Olga Dmitrievna que no hicimos nada malo!"


translate spanish day7_us_26f39e54:


    us "Yo no hice nada malo."


translate spanish day7_us_e9d754a4:


    "Me corrigió Ulyana."


translate spanish day7_us_b1a66e30:


    us "No parece que tengas nada que ver al respecto."


translate spanish day7_us_d9150933:


    me "Vale, que así sea."


translate spanish day7_us_03f66e24:


    us "Pero, ¿por qué?"


translate spanish day7_us_86d84474:


    th "Una pregunta con trampa."


translate spanish day7_us_ecae36f6:


    "Es como si hubiéramos intercambiado nuestros roles: yo estaba sugiriendo un estúpido plan, mientras ella era la voz de la cordura."


translate spanish day7_us_873c6ad1:


    me "Bueno, ¡pues porque no hicimos nada de malo!"


translate spanish day7_us_56f9ef5d:


    us "¿Qué pasa contigo de todas maneras? Me quedaré aquí castigada por un par de horas y ya está. Además, pronto tenemos que partir de todos modos."


translate spanish day7_us_442bf024:


    "Se dejó caer encima de la cama y se quedó mirando el techo."


translate spanish day7_us_3838f56d:


    me "Bueno, claro, pero..."


translate spanish day7_us_5093b270:


    "Me esforcé por intentar animarla, pero parece que no coseché mucho éxito."


translate spanish day7_us_14e075c5:


    me "¿Quizá puedo hacer alguna cosa?"


translate spanish day7_us_6494556f:


    us "Ya es la hora de comer."


translate spanish day7_us_43b742c4:


    "Eché un vistazo a mi reloj."


translate spanish day7_us_ddedc7ac:


    me "Sí, correcto."


translate spanish day7_us_07a52ab7:


    us "Ahí va un trabajo para ti. Puesto que no puedo salir fuera, ve y traéme algo para comer."


translate spanish day7_us_6ae432cd:


    me "¡A sus órdenes!"


translate spanish day7_us_a0ee099d:


    "La saludé y salí deprisa afuera."


translate spanish day7_us_7b5685fb:


    "Recientemente comencé a pensar que, al fin, algo había cambiado en la mente de Ulyana."


translate spanish day7_us_56f958e0:


    "Tal vez el castigo había tenido efecto en ella, o posiblemente fuera otra cosa."


translate spanish day7_us_c059b51b:


    "Y mi actitud con respecto a ella ha cambiado."


translate spanish day7_us_2a9a781a:


    "Sus malos actos normalmente no me hacían sentir otra cosa que irritación, pero ahora había también comprensión y algún tipo de afinidad."


translate spanish day7_us_c2b62820:


    "Después de todo, también fui un niño."


translate spanish day7_us_2326b742:


    th "Tal vez si uno le explicara el porqué algo está bien o mal, puede que fuera capaz de evitar muchos errores."


translate spanish day7_us_e9f2b2e6:


    "En la cantina tuve una discusión con un cocinero que se negaba a darme una doble ración."


translate spanish day7_us_749be311:


    "Sin embargo, todo el mundo en el campamento ya sabían que Ulyana estaba encerrada, así que en cualquier momento mi poder de persuasión se impondría frente a la norma de la dieta."


translate spanish day7_us_9c84b285:


    "Bien pronto estaba sentado en la cabaña de Ulyana, engulliendo albóndigas con patatas fritas."


translate spanish day7_us_19b4f87b:


    me "Como si fuera la última comida."


translate spanish day7_us_e718f922:


    us "¿De qué hablas?"


translate spanish day7_us_076ef871:


    me "Bueno, todos los prisioneros condenados a la pena de muerte tienen derecho a un último deseo.{w} Por consiguiente, mi último deseo es una comida como esta."


translate spanish day7_us_6532261a:


    us "Mmmm..."


translate spanish day7_us_1d2850e0:


    "Para mi sorpresa la comida estaba realmente deliciosa."


translate spanish day7_us_1357b107:


    me "¿Cuál sería tu deseo?"


translate spanish day7_us_8a3e0f25:


    us "Bueno... ¡No ser ejecutada, por supuesto!"


translate spanish day7_us_ec2c2a7c:


    "Ella se rio."


translate spanish day7_us_4e8385e3:


    me "No puedes."


translate spanish day7_us_d8734f33:


    us "¿Por qué? Si puedes desear cualquier cosa."


translate spanish day7_us_d1a58061:


    me "Bueno, puedes, pero hasta ciertos límites."


translate spanish day7_us_1b7bd7e3:


    us "Pues entonces eso significa que no vale todo."


translate spanish day7_us_ba513381:


    me "Bueno, vale, no puede ser cualquier cosa..."


translate spanish day7_us_2bca74af:


    us "Entonces eso no tiene nada de interesante."


translate spanish day7_us_a220139e:


    me "Bueno, en primer lugar, creo que ser un condenado a muerte difícilmente puede ser algo interesante."


translate spanish day7_us_e98bfbde:


    "Me reí entre dientes."


translate spanish day7_us_389622ca:


    us "No sabría decir, nunca tuve dicha experiencia."


translate spanish day7_us_0cf85698:


    "Pero si lo piensas, es casi mi situación."


translate spanish day7_us_8c75e136:


    th "Este campamento es mi cárcel.{w} Por muchas más horas permaneceré bajo arresto, y luego afrontaré la incerteza, como lo haría frente a la muerte."


translate spanish day7_us_72658116:


    th "La única diferencia es que tenía más opciones. Si quisiera atender a la formación pues lo haría, y si no quería pues no lo hacía."


translate spanish day7_us_9e77a60a:


    me "¿Qué es lo que vas a hacer ahora?"


translate spanish day7_us_5f2c0f51:


    us "¿Qué quieres decir?"


translate spanish day7_us_c9d0b0d9:


    me "Bueno, tras el campamento."


translate spanish day7_us_efd76d15:


    "Me miraba con sorpresa."


translate spanish day7_us_85f9af1a:


    us "De vuelta a la escuela, por supuesto."


translate spanish day7_us_133710bb:


    th "Sí, solamente para mí el abandonar este lugar es algo así como cruzar un tipo de barrera, una frontera, el fin de alguna cosa y el principio de otra."


translate spanish day7_us_cbeb66da:


    "Hace una semana atrás era terriblemente difícil para mi el darme cuenta de que había sido abducido de mi mundo habitual, y llevado a Dios sabe dónde.{w} Pero luego me acostumbré."


translate spanish day7_us_40e7e33b:


    "Y aquí estamos de nuevo..."


translate spanish day7_us_74755571:


    th "Básicamente, la única diferencia es que ahora no afronto una sensación de miedo u horror, sino un sentimiento de desafilada y quebrada incertidumbre."


translate spanish day7_us_cdd166bb:


    us "¿Y tú?"


translate spanish day7_us_6aabcfa6:


    me "Yo... Bueno, hallaré algo que hacer."


translate spanish day7_us_94d9cae2:


    us "¿Algo?"


translate spanish day7_us_42be2b6f:


    "Rompió a reír a risotadas."


translate spanish day7_us_90305e74:


    me "Claro, ¿qué pasa?"


translate spanish day7_us_a5f6da24:


    us "¡Deberías haber ido a una escuela para ser un payaso!"


translate spanish day7_us_5ac38cf1:


    me "¿Por qué?"


translate spanish day7_us_79d4d869:


    us "Una no puede evitar reírse con tan sólo verte."


translate spanish day7_us_ed762271:


    me "Pero, ¿por qué?"


translate spanish day7_us_1c0cb259:


    us "Siempre actuas como si fueras algún tipo de mártir, como un nuevo mesías para todos los rusos."


translate spanish day7_us_67f0f7a7:


    "Bueno, había un poquito bastante de verdad en sus palabras."


translate spanish day7_us_bb751cb7:


    me "Tengo mis motivos."


translate spanish day7_us_e0608260:


    "Murmuré y me giré para mirar por la ventana."


translate spanish day7_us_96bedb40:


    us "¿Qué motivos?"


translate spanish day7_us_e9cde24f:


    me "Varios...{w} ¿Por qué sientes tanta curiosidad?"


translate spanish day7_us_b72e8852:


    us "¿Has olvidado que soy una niña?"


translate spanish day7_us_3aa0974f:


    "Sonrió socarronamente."


translate spanish day7_us_31faa27f:


    me "Bueno, media hora contigo es como una terrible tortura para mí."


translate spanish day7_us_7c214dde:


    us "¡Mira quién habla! ¡Tú tan sólo eres una persona con un carácter desagradable!"


translate spanish day7_us_1aff923f:


    me "¿Qué te hace pensar eso?"


translate spanish day7_us_ebf04f0a:


    us "Siempre estás reflexionando profundamente, tratando de hallar alguna cosa, analizando a los demás a tu alrededor."


translate spanish day7_us_1a13d024:


    "Le eché una mirada asombrada a Ulyana."


translate spanish day7_us_3637341f:


    "Nunca habría esperado que semejante pequeña muchacha fuera capaz de tal racionamiento tan maduro."


translate spanish day7_us_5730768c:


    me "Bueno... ¿y?"


translate spanish day7_us_38197147:


    us "Bueno, ya está..."


translate spanish day7_us_0d76fe54:


    me "Por lo menos sé cómo comportarme... y no acabo encerrado."


translate spanish day7_us_b9e39a7d:


    us "Es una cuestión de oportunidad."


translate spanish day7_us_7f6b716b:


    "Ella sonrió astutamente."


translate spanish day7_us_a7b22250:


    me "¡Sí, claro!"


translate spanish day7_us_26a5c7be:


    us "Si la noche anterior..."


translate spanish day7_us_5e054826:


    "Se detuvo de golpe."


translate spanish day7_us_fc2d031c:


    me "¿«La noche anterior» qué?"


translate spanish day7_us_0cdc580c:


    us "Nada..."


translate spanish day7_us_7a4616c7:


    me "No, ¡termina lo que empezaste!"


translate spanish day7_us_b58aec3f:


    "Justo abrió la boca cuando se escucharon pasos detrás de la puerta y, un segundo más tarde, Olga Dmitrievna entró en la habitación."


translate spanish day7_us_aef6989a:


    mt "Ah, estáis aquí. Así es mejor."


translate spanish day7_us_6553b668:


    "Parecía confusa y se quedó sin palabras."


translate spanish day7_us_cb8b6ba5:


    mt "Bueno, reconsideré el incidente de esta mañana...{w} No es que se haya aclarado mucho más, pero no parece ser algo grave.{w} Así que, Ulyana, oficialmente ya no estás más tiempo encerrada."


translate spanish day7_us_0cd3e52f:


    me "Ojalá hubieras hecho eso desde un principio."


translate spanish day7_us_901a0e98:


    "Murmuré."


translate spanish day7_us_574528c5:


    mt "¿Dijiste algo?"


translate spanish day7_us_3c5e2cfc:


    me "No, nada."


translate spanish day7_us_73c573c4:


    mt "Se acerca la hora de partir, recoged vuestras cosas."


translate spanish day7_us_8af1d414:


    "Dicho eso, abandonó la cabaña."


translate spanish day7_us_4456980d:


    us "¡Ahora ya ves como acabó resultando todo!"


translate spanish day7_us_c63ec5a4:


    me "Claro..."


translate spanish day7_us_9da1463d:


    "Suspiré."


translate spanish day7_us_2d5f3035:


    me "¿Vas a estar empacando?"


translate spanish day7_us_f2cfbfb7:


    us "Claro, supongo...{w} ¿Y tú?"


translate spanish day7_us_bad_7d11a631:


    me "Sí, también me voy a empacar mis cosas."


translate spanish day7_us_bad_cf3fffbb:


    "Ella no contestó, así que me fui de la cabaña."


translate spanish day7_us_bad_953ac80a:


    th "Bueno, parece que mi última misión en este campamento está completada... el castigo de Ulyana se anuló después de todo."


translate spanish day7_us_bad_044c011d:


    th "Lo único que me queda es abandonar este lugar y prepararme para algo nuevo."


translate spanish day7_us_bad_782e9fd9:


    "Barrí con la vista la habitación cuando entré en la cabaña de la líder del campamento."


translate spanish day7_us_bad_9bc29860:


    th "¿Acaso tengo algo que empacar?"


translate spanish day7_us_bad_cae3ae11:


    "Metí mis ropas de invierno en una mochila y tomé asiento en la cama."


translate spanish day7_us_bad_b02e82a9:


    "Fui poseído por una agonizante sensación de vacío... sea déjà vu o sea un sentimiento de que olvidé algo, pero aun así no pude recordar qué era exactamente."


translate spanish day7_us_bad_6288575d:


    th "Cuando todo está dicho y hecho, no he logrado hallar ninguna respuesta aquí y, ahora, tendré que continuar mi búsqueda en otra parte."


translate spanish day7_us_bad_1aa8b92f:


    th "¿Importa realmente el cómo y el por qué llegué hasta aquí, si no es posible siquiera escapar?{w} Y parece que nada depende de mí de todos modos."


translate spanish day7_us_bad_41fa4bd9:


    "Todo resultaba más simple antes."


translate spanish day7_us_bad_5a9fed74:


    "Claro que no tenía grandes expectativas, mis perspectivas difícilmente eran impresionantes, pero al menos todo estaba mucho más claro."


translate spanish day7_us_bad_3a418180:


    "Incluso esta semana que pasé aquí, hizo surgir más problemas que toda mi vida anterior."


translate spanish day7_us_bad_a20cefa7:


    "..."


translate spanish day7_us_bad_f98266b4:


    "El reloj marcaba las cinco en punto."


translate spanish day7_us_bad_d924adad:


    "Cogí la mochila y caminé rápidamente hacia la parada de autobús, ¡preocupado de que pudieran irse sin mí!"


translate spanish day7_us_bad_abfd8483:


    "Resultó que el autobús estaba esperando ya ahí, así como todos los pioneros."


translate spanish day7_us_bad_090f0c1d:


    mt "¿Todo el mundo está aquí?"


translate spanish day7_us_bad_14167bf5:


    "Comenzó a decir Olga Dmitrievna."


translate spanish day7_us_bad_044ab0d8:


    mt "Hoy os vais de nuestro campamento, y me gustaría deciros algo antes de partir."


translate spanish day7_us_bad_00123e37:


    "Estaba visiblemente nerviosa y, desesperadamente, se quedó sin palabras."


translate spanish day7_us_bad_019ed77a:


    mt "Espero que recordéis el tiempo que transcurristeis aquí durante vuestra vida, y que retengáis solamente recuerdos agradables sobre Sovyonok."


translate spanish day7_us_bad_b48fe53b:


    mt "También espero que os hayáis convertido un poquito en mejores personas, que hayáis logrado aprender algo y conocido nuevos amigos...{w} Tan solamente... volved el próximo año."


translate spanish day7_us_bad_42256d78:


    "La líder del campamento apartó su mirada.{w} Era como si estuviera tratando de aguantarse las lágrimas para sí misma."


translate spanish day7_us_bad_80682609:


    "No me esperaba que ella se pusiera tan sentimental."


translate spanish day7_us_bad_f4683d53:


    "Aunque su discurso me resultó un completo sin sentido. Como de costumbre."


translate spanish day7_us_bad_592d05f0:


    "Los pioneros empezaron a entrar lentamente en el autobús."


translate spanish day7_us_bad_5176ce5d:


    "Eché un vistazo rápido entre la multitud, buscando a Ulyana."


translate spanish day7_us_bad_422a1788:


    me "¿Dónde está Ulyana?"


translate spanish day7_us_bad_2dd2f5e0:


    mt "Ella no vendrá."


translate spanish day7_us_bad_0012312c:


    "Respondió la líder brevemente."


translate spanish day7_us_bad_ae14ffa6:


    me "¿Cómo? ¿Por qué?"


translate spanish day7_us_bad_945c2cb1:


    mt "Está encerrada."


translate spanish day7_us_bad_55986632:


    me "Pero dijiste..."


translate spanish day7_us_bad_f037a948:


    mt "Lo está de nuevo."


translate spanish day7_us_bad_149b3064:


    me "¿Qué quieres decir?"


translate spanish day7_us_bad_76b8fe45:


    mt "¡Ey, métete dentro o nos iremos sin ti!"


translate spanish day7_us_bad_a8a52ea3:


    th "Ciertamente, era el último hombre que quedaba fuera."


translate spanish day7_us_bad_4b08df28:


    me "Espera un segundo..."


translate spanish day7_us_bad_8d6e2c28:


    mt "¡Que te muevas!"


translate spanish day7_us_bad_ef663ab0:


    "Ella me dio un empujón hacia dentro del autobús.{w} Decidí no discutir."


translate spanish day7_us_bad_4c34caf5:


    th "Al final, ¡no puedo seguir siendo siempre responsable de ella! ¡Ahí la tienes otra vez! Si ella va a ser tan cabezona, ¡pues que afronte las consecuencias!"


translate spanish day7_us_bad_92535a77:


    "Tomé el último asiento."


translate spanish day7_us_bad_d1b5df44:


    "No había ningún compañero para mí, lo cual me parecía estupendo en ese momento."


translate spanish day7_us_bad_06550b87:


    "Sin embargo, muy pronto Alisa se sentó junto a mí."


translate spanish day7_us_bad_fddf23ae:


    dv "¿Dónde está Ulyana?"


translate spanish day7_us_bad_bd9299cf:


    me "Se quedó..."


translate spanish day7_us_bad_69139e02:


    dv "¿Por qué?"


translate spanish day7_us_bad_ba766f00:


    me "Encerrada..."


translate spanish day7_us_bad_ec5a83b6:


    "Daba respuestas con tan sólo una palabra, mostrando mi falta de interés en la conversación."


translate spanish day7_us_bad_81056a1f:


    dv "¿Para qué?"


translate spanish day7_us_bad_7479b07e:


    me "No lo sé..."


translate spanish day7_us_bad_86e1bfff:


    dv "No me digas... ¿Ni tan siquiera trataste de averiguarlo?"


translate spanish day7_us_bad_d762c781:


    me "¿Vale la pena quedarse atrás por eso?"


translate spanish day7_us_bad_72474ea4:


    dv "Deberías."


translate spanish day7_us_bad_b0739623:


    me "¿Para qué? Si quieres puedes quedarte. No nos hemos ido muy lejos."


translate spanish day7_us_bad_aa5269ba:


    dv "Vale, vale..."


translate spanish day7_us_bad_ee6019f2:


    "Dijo tranquilamente y se fue a su lugar."


translate spanish day7_us_bad_1ff20cee:


    "Slavya y Zhenya estaban sentadas en los asientos de delante y hablaban con entusiasmo sobre alguna cosa. Lena, Miku, Shurik y Electronik estaban sentados cerca de mí jugando a cartas. Alisa y otra muchacha estaban mirando una revista."


translate spanish day7_us_bad_e72a9bb8:


    "Probablemente, era el único que no tenía gran cosa que hacer."


translate spanish day7_us_bad_f2666b16:


    "Me acabé dando cuenta de que todavía me sentía un poco culpable por lo que le ocurrió a Ulyana."


translate spanish day7_us_bad_0e4c5769:


    th "¿Pero qué podía hacer?{w} Si me hubiera quedado con ella, habría perdido probablemente mi única posibilidad de irme..."


translate spanish day7_us_bad_05830ef4:


    th "Y en mi posición, ¡jugar a dichos juegos podría resultar fatal!"


translate spanish day7_us_bad_13511f52:


    th "Y además, ¡ya basta!"


translate spanish day7_us_bad_f86e1ee0:


    "Aun así seguía sin poder dejar de pensar en ella.{w} De que me equivocaba, de que actué cobardemente, de que hice una cosa mala.{w} De que cualquiera en mi lugar habría..."


translate spanish day7_us_bad_654261be:


    th "Pero, ¿por qué?{w} De todas formas, ¿qué significa ella para mí, como para que yo tenga que tomar riesgos en su nombre?"


translate spanish day7_us_bad_8ce59d82:


    "Era aun más difícil responder a esta pregunta."


translate spanish day7_us_bad_7415ba73:


    "Durante la semana que transcurrí aquí, pude conocerla a ella y a los demás bastante bien."


translate spanish day7_us_bad_a7064953:


    th "Al final, ¡me sentía responsable de Ulyana de alguna manera!"


translate spanish day7_us_bad_abd762d2:


    th "¿Pero qué sentido tiene pensar en esto ahora?"


translate spanish day7_us_bad_69057b86:


    th "Estoy abandonando este campamento de pioneros.{w} Me dirijo hacia lo desconocido."


translate spanish day7_us_bad_79fbcf58:


    th "Desde este instante, tengo que tomar todas mis decisiones basándome en la suposición de que este mundo es ajeno a mí y, muy probablemente, hostil."


translate spanish day7_us_bad_dd2f1e94:


    th "Y no tengo a nadie en quien confiar."


translate spanish day7_us_bad_d80fce90:


    "Aunque, si lo piensas, semejante estado de cosas no es tan nuevo para mí... siempre estuve solo en el pasado..."


translate spanish day7_us_bad_a20cefa7_1:


    "..."


translate spanish day7_us_bad_e1a1a085:


    "Sovyonok ha sido dejado bien lejos atrás, la noche ha caído en este extraño mundo. Era tan oscuro que parecía que el autobús estaba flotando a través de un océano de negro azulado, y que solamente a veces surgían sombríos bosques y campos alzándose por encima del horizonte, haciendo olas por su superficie."


translate spanish day7_us_bad_9c63431a:


    "De todas maneras, los alrededores eran la última de mis preocupaciones ahora... Estaba profundamente ensimismado en mis reflexiones."


translate spanish day7_us_bad_ef6dcef8:


    "Me sentía como si hubiera dejado atrás un asunto sin finalizar en el campamento."


translate spanish day7_us_bad_bdc41394:


    "Aunque a pesar de todo, no puedo volver allí ahora."


translate spanish day7_us_bad_3dfd000d:


    th "Las cosas malas, las cosas buenas... pronto caerán en el olvido, dejando únicamente el hecho de mi aparición aquí, el fin de mi anterior vida y el comienzo de una nueva."


translate spanish day7_us_bad_0bf16c1e:


    th "Siendo prácticos, no es nada sofisticado... solamente un campamento de pioneros de la década de los ochenta, al cual llegué desde mi propia época..."


translate spanish day7_us_bad_cd6651e8:


    th "En cualquier caso, mejor que me preocupe sobre lo que me espera en aquella ciudad a la que nos dirigimos."


translate spanish day7_us_bad_6e3dcb07:


    th "Después de todo, no tengo ningún lugar al que retornar.{w} No tengo hogar, ni dinero, ni amigos y tampoco parientes."


translate spanish day7_us_bad_2560e7b0:


    th "¿Y todos estos pioneros de los que me despediré en pocas horas?{w} Nunca más volveré a verlos otra vez.{w} ¿Y ellos? No creo que se acuerden de mí en un par de años."


translate spanish day7_us_bad_34c047f7:


    th "No es algo importante para ellos, no soy el viajero del tiempo sino un muchacho común, su compañero..."


translate spanish day7_us_bad_a20cefa7_2:


    "..."


translate spanish day7_us_bad_790bebae:


    "La carretera parecía no tener fin.{w} Muchos de los pioneros ya hacía rato que estaban durmiendo, aun así yo todavía combatía la necesidad de hacer una cabezada."


translate spanish day7_us_bad_55c5b97a:


    "Siempre es mejor entrar en lo desconocido permaneciendo despierto, aunque lo desconocido siempre tiene una ventaja en esta batalla... el devenir del tiempo que es capaz de controlar a su conveniencia."


translate spanish day7_us_bad_59d8bd33:


    "Esperas un minuto, una hora, pero no sucede nada. Tus nervios, forzados ya hasta sus límites, finalmente se rinden al estrés y simplemente caes dormido..."


translate spanish day7_us_good_0f8e9855:


    me "No parece que tenga mucho que empacar."


translate spanish day7_us_good_fa7387e7:


    "Ciertamente, era la verdad."


translate spanish day7_us_good_07b57da2:


    us "¿Te importa si me ayudas pues?"


translate spanish day7_us_good_011dac7d:


    me "Vale, de acuerdo, ¿por qué no?"


translate spanish day7_us_good_9e6ddcf0:


    "Empezó a sacar sus ropas de los armarios, amontonándola sobre su cama."


translate spanish day7_us_good_16c1c561:


    me "Vigila, la estropearás toda."


translate spanish day7_us_good_a5b3f692:


    us "¡No pasa nada! ¡La lavaré en casa!"


translate spanish day7_us_good_8821f1ec:


    "Camisetas, faldas, pantalones cortos, vestidos, zapatos, zapatillas deportivas, ropa interior... El montón de ropa crecía y crecía."


translate spanish day7_us_good_1b010f5f:


    me "¿De verdad traíste todas estas cosas aquí tú misma?"


translate spanish day7_us_good_2312e712:


    "Teniendo en cuenta la constitución de Ulyana, era difícil de creer."


translate spanish day7_us_good_fc656e44:


    us "¡Sí, por supuesto!"


translate spanish day7_us_good_ce3666e5:


    "Se rio."


translate spanish day7_us_good_968191a9:


    us "Vamos, échame una mano."


translate spanish day7_us_good_907c8b48:


    "Empezamos a empacar todas las ropas dentro de una gran maleta."


translate spanish day7_us_good_7128760a:


    "Traté de empacar las cosas con cuidado al principio, pero tras percatarme de que no tenía sentido, recurrí sencillamente a meterlo todo dentro con tal de que cupiera."


translate spanish day7_us_good_e48f1d3a:


    "Por fin, no había más ropas en la cama, e incluso nos las apañamos para cerrar la cremallera de la maleta."


translate spanish day7_us_good_848e1ff0:


    us "Bien, ya está."


translate spanish day7_us_good_9513cd87:


    me "Sí..."


translate spanish day7_us_good_153e49e8:


    "Miré el reloj.{w} Faltaban unos cuarenta minutos hasta la hora de partir."


translate spanish day7_us_good_05577344:


    us "Sabes, fue divertido."


translate spanish day7_us_good_cb23542f:


    me "¿Qué quieres decir?"


translate spanish day7_us_good_150ba4fa:


    us "Bueno, que la última semana fue divertida."


translate spanish day7_us_good_75e0e5f1:


    me "Ah, claro..."


translate spanish day7_us_good_f2170060:


    "Dije con la mente ausente."


translate spanish day7_us_good_734b6aae:


    us "¿No te lo parece?"


translate spanish day7_us_good_665c7c6a:


    me "¿Por qué lo preguntas...?"


translate spanish day7_us_good_68413a8d:


    us "Tu respuesta me pareció algo insincera."


translate spanish day7_us_good_b2352a58:


    me "Bueno, debo admitir que no estoy exactamente para tirar cohetes, claro."


translate spanish day7_us_good_14cb44fc:


    us "Pero, ¿qué es lo que no te ha gustado?"


translate spanish day7_us_good_5839d87f:


    th "No es que pueda contarlo todo ahora de golpe..."


translate spanish day7_us_good_d2eaf470:


    me "Verás... Algunos momentos fueron, por decirlo de alguna manera, un poquito inesperados."


translate spanish day7_us_good_2ee7e228:


    us "Soso, eres muy aburrido..."


translate spanish day7_us_good_0c55cf94:


    "Dijo Ulyana y se apartó de mí."


translate spanish day7_us_good_34c3e0de:


    me "Bueno, ¿y qué te esperabas de mí?"


translate spanish day7_us_good_cac3332a:


    us "Es como si no tuvieras nada en absoluto que recordar más tarde."


translate spanish day7_us_good_b654b924:


    me "Claro, seguro que hay mucho que recordar."


translate spanish day7_us_good_5154e89b:


    "Le sonreí resplandecientemente."


translate spanish day7_us_good_ac91a0ae:


    us "Sep, ¡eso es lo que estoy diciendo!"


translate spanish day7_us_good_e24147c1:


    "Ella me miró detenidamente.{w} Eso me hizo estar un poco incómodo."


translate spanish day7_us_good_f3274282:


    me "¿Y ahora qué?"


translate spanish day7_us_good_f68e9feb:


    us "Y yo, ¿te acordarás de mí?"


translate spanish day7_us_good_39fd95ae:


    me "Claro que lo haré. De todos..."


translate spanish day7_us_good_7b24e7b5:


    us "De todos..."


translate spanish day7_us_good_d445de56:


    me "Y tú... ¡tú más que todos!"


translate spanish day7_us_good_55ab6c4e:


    "Saludé, poniéndome firme llevándome la mano a mi corazón."


translate spanish day7_us_good_7827470a:


    us "¡Eso está mejor!"


translate spanish day7_us_good_f3bc15be:


    th "Sin embargo, ¿de verdad tengo algo que recordar sobre ella?"


translate spanish day7_us_good_37b15a62:


    th "Claro que habíamos tenido montones de aventuras, ¿pero qué significa Ulyana para mí?{w} Sólo una entrometida cría de otra realidad..."


translate spanish day7_us_good_b532bb7b:


    th "Pensándolo bien, ¿de verdad me importa ahora tanto mi situación?{w} Desde buen principio que aparecí aquí, todo cambió tremendamente, tanto que ahora mismo sentía curiosidad por lo que fuera a suceder."


translate spanish day7_us_good_2104e6d9:


    th "Es obvio que lo desconocido no es que sea una perspectiva agradable, pero debo admitir que es imponente."


translate spanish day7_us_good_78852785:


    th "Tanto si retorno a mi mundo como si no, ya no depende más de mí."


translate spanish day7_us_good_119d1deb:


    th "Quiero decir que en realidad no tengo ninguna opción... Me tengo que adaptar a las condiciones de vida locales."


translate spanish day7_us_good_3c4827d3:


    "Y una de estas condiciones estaba justo frente a mí, sonriéndome ampliamente."


translate spanish day7_us_good_77a7cf13:


    me "Sabes, no eres tan tonta como creí al principio."


translate spanish day7_us_good_0f029e42:


    us "En primer lugar, ¿por qué crees que soy tonta?"


translate spanish day7_us_good_3db311fc:


    "Dijo con un tono de ofendida."


translate spanish day7_us_good_2824e5e8:


    me "Sólo bromeaba."


translate spanish day7_us_good_27972fbd:


    us "Tú y tus bromas..."


translate spanish day7_us_good_96ba4ae7:


    me "Muy bien, es la hora."


translate spanish day7_us_good_89d3eb13:


    us "Vayámonos."


translate spanish day7_us_good_5b0783c9:


    "Dijo Ulyana con júbilo y señaló la maleta."


translate spanish day7_us_good_f2e007f0:


    me "Claro..."


translate spanish day7_us_good_379e3d71:


    "Cargué con sus pertenencias.{w} El peso casi me hizo partir en dos."


translate spanish day7_us_good_fd618572:


    "Estaba agradecido al menos por el hecho de que la parada de autobús estaba alejada a tan sólo unos pocos cientos de metros."


translate spanish day7_us_good_6ef81a93:


    "Metí sus cosas dentro del autobús, y luego me di prisa para mi humilde equipaje."


translate spanish day7_us_good_fee09454:


    "Tras un rato, todos los pioneros estaban aquí."


translate spanish day7_us_good_090f0c1d:


    mt "¿Todo el mundo está aquí?"


translate spanish day7_us_good_14167bf5:


    "Comenzó a decir Olga Dmitrievna."


translate spanish day7_us_good_044ab0d8:


    mt "Hoy os vais de nuestro campamento, y me gustaría deciros algo antes de partir."


translate spanish day7_us_good_00123e37:


    "Estaba visiblemente nerviosa y, desesperadamente, se quedó sin palabras."


translate spanish day7_us_good_019ed77a:


    mt "Espero que recordéis el tiempo que transcurristeis aquí durante vuestra vida, y que retengáis solamente recuerdos agradables sobre Sovyonok."


translate spanish day7_us_good_b48fe53b:


    mt "También espero que os hayáis convertido un poquito en mejores personas, que hayáis logrado aprender algo y conocido nuevos amigos...{w} Tan solamente... volved el próximo año."


translate spanish day7_us_good_42256d78:


    "La líder del campamento apartó su mirada.{w} Era como si estuviera tratando de aguantarse las lágrimas para sí misma."


translate spanish day7_us_good_6a707c88:


    "No me esperaba que ella se pusiera tan sentimental, pero estaba completamente de acuerdo con todo lo que dijo."


translate spanish day7_us_good_6888cb6b:


    "Quizás fue la primera vez que sus palabras no me entraban por un oído y me salían por el otro."


translate spanish day7_us_good_7400c0ae:


    "Bien pronto todo el mundo subió al autobús.{w} Me senté en el último asiento, al lado de Ulyana."


translate spanish day7_us_good_e3f34bf2:


    "En la primera fila estaban Slavya y Zhenya, luego, un poco más cerca de nosotros, Lena, Miku, Electronik y Shurik que estaban jugando a cartas.{w} Alisa estaba sola despatarrada en su asiento a dos filas de nosotros... no tenía ningún compañero."


translate spanish day7_us_good_69fc4c0b:


    me "Resulta un poco incómodo para tu compañera de habitación..."


translate spanish day7_us_good_7c056817:


    us "¡No pasa nada! Me has ayudado a prepararme y me trajiste las cosas aquí."


translate spanish day7_us_good_8f838576:


    "La miré detenidamente."


translate spanish day7_us_good_fb2f522b:


    th "Parecía que este pequeño demonio había cambiado su actitud respecto a mí de una forma radical."


translate spanish day7_us_good_607b9495:


    th "¿Podría ser que fuera... un amigo para ella?{w} Una extraña palabra que parecía haber perdido su significado para mí hacía mucho tiempo."


translate spanish day7_us_good_ebd06a5c:


    th "No recuerdo a ninguno de mis conocidos, cuando todavía los tenía, a quienes les pudiera llamar «amigos».{w} Tal vez incluso desde cuando iba a la escuela..."


translate spanish day7_us_good_ef14ed90:


    th "Y ahora alguien me consideraba un amigo..."


translate spanish day7_us_good_d54d3387:


    th "De todas maneras, ¿qué significa todo esto para mí?"


translate spanish day7_us_good_d742ed4f:


    "Siempre me he sentido más a gusto pensando en asuntos abstractos, perspectivas distantes y asuntos globales, que los problemas simples y cotidianos."


translate spanish day7_us_good_70b35443:


    "Y ciertamente, durante todo el tiempo que me he pasado en este campamento, he logrado que nos volviéramos buenos amigos yo y Ulyana."


translate spanish day7_us_good_dadbd740:


    "Ella despertó estos largamente olvidados sentimientos en mí."


translate spanish day7_us_good_5cbb42fa:


    "Porque esto es lo que significa ser compañeros o amigos."


translate spanish day7_us_good_d53e2161:


    "Sonreí y le despeiné su cabello con suavidad."


translate spanish day7_us_good_d8ad57a4:


    us "¿Y eso a qué viene ahora?"


translate spanish day7_us_good_d946b833:


    "Ella hizo un mohín."


translate spanish day7_us_good_616b5736:


    me "¡Porque sí!"


translate spanish day7_us_good_36244ee4:


    us "¡Pervertido!"


translate spanish day7_us_good_9ddb8918:


    us "¿Te casarás conmigo cuando crezca?"


translate spanish day7_us_good_8288e06d:


    me "¡Claro!"


translate spanish day7_us_good_969c2b47:


    us "¡Haré que lo cumplas!"


translate spanish day7_us_good_3362bb2d:


    me "¡O sea sí!"


translate spanish day7_us_good_6fc26df0:


    us "¿Vamos a unirnos para jugar a cartas con ellos?"


translate spanish day7_us_good_3fbaf00b:


    me "¡Por qué no!"


translate spanish day7_us_good_60a78af5:


    "Nos reunimos alrededor de una maleta que estaba siendo usada de mesa improvisada."


translate spanish day7_us_good_72f8664f:


    "Pronto Alisa se nos unió."


translate spanish day7_us_good_f9ee83c7:


    "Me estaba riendo mucho, bromas estupendas y simplemente disfrutando de estar en un estado que normalmente uno le diría de «felicidad».{w} Una sencilla felicidad, aquí y ahora."


translate spanish day7_us_good_3105c441:


    "Ahora mismo, este montón de pioneros con los que he logrado hacer amistad en esta breve semana, eran un millón de veces más importantes para mí que buscar cómo llegué a este mundo, y cómo salir de aquí."


translate spanish day7_us_good_32e7a990:


    th "Al final, ¿debería incluso molestarme en tratar de volver?"


translate spanish day7_us_good_a20cefa7:


    "..."


translate spanish day7_us_good_07039b05:


    "Se estaba haciendo oscuro.{w} El juego se terminó hacía ya rato y los pioneros habían retornado a sus asientos."


translate spanish day7_us_good_b2a3af4b:


    "No tenía ni idea de cuánto tiempo llevaría llegar hasta el distrito central, pero parecía una eternidad."


translate spanish day7_us_good_5fd5c146:


    "Más allá de las ventanillas del autobús solamente la oscuridad total se alcanzaba a ver, casi como consumiendo todo el mundo, rodeándolo todo salvo hasta la carrocería del Icarus."


translate spanish day7_us_good_8d851fd8:


    "De todas maneras, los alrededores eran lo último que me preocupaba ahora... Estaba disfrutando el momento."


translate spanish day7_us_good_4ba60f47:


    th "Parece que esta realidad es completamente normal."


translate spanish day7_us_good_7e661857:


    th "¿Acaso importa de verdad cómo llegué hasta aquí, si las cosas resultan ser geniales? Me convertí en otra persona, y conocí nuevos amigos."


translate spanish day7_us_good_eabb99b9:


    us "¿En qué piensas?"


translate spanish day7_us_good_94622b60:


    me "En la vida."


translate spanish day7_us_good_c8e68f82:


    us "¿Y qué tal?"


translate spanish day7_us_good_d1bfd0cf:


    me "¡Estupenda!"


translate spanish day7_us_good_fae4f64a:


    "Ulyana se rio discretamente."


translate spanish day7_us_good_f4357af5:


    th "No tengo un lugar al que retornar, así que puedo elegir cualquier camino en la vida que quiera."


translate spanish day7_us_good_9e5ffe85:


    th "Sí, pronto partiremos en caminos distintos y, probablemente, nunca más veré a muchos de ellos otra vez, ¡pero seguiremos siendo amigos para siempre!"


translate spanish day7_us_good_d097bca7:


    "Una cálida y agradable sensación recorrió mí cuerpo."


translate spanish day7_us_good_0a73f977:


    "Me sentí como un niño otra vez."


translate spanish day7_us_good_76539999:


    "Ulyana apoyó su cabeza en mi hombro y rápidamente se durmió."


translate spanish day7_us_good_a20cefa7_1:


    "..."


translate spanish day7_us_good_481a603e:


    "A veces puedes sentirte agotado no únicamente a causa de un trabajo duro o de sentimientos tristes, sino también a causa de la diversión, el placer y la felicidad."


translate spanish day7_us_good_8cffdf27:


    "Probablemente puedas querer incluso continuar, pero no tengas energía para ello, con tu alma requiriendo tiempo para descansar y tu cuerpo pidiéndote tranquilidad."


translate spanish day7_us_good_59cfc807:


    "Me dormí con una sonrisa en mi cara."


translate spanish day7_dv_34b34bb6:


    "Unas personas me estaban persiguiendo..."


translate spanish day7_dv_2fb6d076:


    "O tal vez no fueran personas sino manchas negras borrosas con un endemoniado panorama de fondo."


translate spanish day7_dv_56a7165e:


    "Corrí... corrí, tropezándome con algo, sin aire que respirar."


translate spanish day7_dv_c534a505:


    "Mi entera naturaleza estaba sobrecogida por miedo y terror instintivo."


translate spanish day7_dv_f6c12f28:


    "Y luego perdí la memoria..."


translate spanish day7_dv_a20cefa7:


    "..."


translate spanish day7_dv_81b5abc9:


    "Abrí mis hinchados párpados sólo para ser iluminado por la brillante luz del día."


translate spanish day7_dv_66a25612:


    "Un sabor terrible en mi boca, dolor por todo el cuerpo y un severo dolor de cabeza... parecía que alguien estuvo bebiendo mucho ayer..."


translate spanish day7_dv_130627c2:


    "Tras recuperar mis sentidos, empecé a recordar los acontecimientos de la última noche."


translate spanish day7_dv_961cb5de:


    "Creo que había una botella de vodka, la cual vacié felizmente junto a Alisa..."


translate spanish day7_dv_955e4b23:


    "Traté de levantarme, pero algo me sujetaba mi brazo izquierdo."


translate spanish day7_dv_169141ea:


    "Era Alisa roncando tranquilamente."


translate spanish day7_dv_fed4fffc:


    "Desnuda..."


translate spanish day7_dv_53e612f9:


    "Todo lo que ocurrió se refrescó en mi mente."


translate spanish day7_dv_2d377264:


    "El miedo fue rápidamente reemplazado por un sentimiento de euforia."


translate spanish day7_dv_d8bc19f3:


    "Me relajé perezosamente en la cama, disfrutando una mañana resacosa."


translate spanish day7_dv_94b6a4c7:


    "Un poco más tarde Alisa se despertó."


translate spanish day7_dv_75b94b19:


    "La besé suavemente y le dije:"


translate spanish day7_dv_0a6d11b5:


    me "¡Buenos días!"


translate spanish day7_dv_117eed62:


    "Ella me hizo una mirada perpleja por un par de segundos, y luego se sobresaltó gritando:"


translate spanish day7_dv_b7d8bf72:


    dv "¡Tú! ¡Tú!"


translate spanish day7_dv_08572abe:


    me "¿Yo qué?"


translate spanish day7_dv_4f57be91:


    "Percatándose de que ella estaba desnuda, Alisa me arrebató la manta y se envolvió en ella."


translate spanish day7_dv_0c9248c1:


    dv "Ayer tú..."


translate spanish day7_dv_8a5850eb:


    "Siseó."


translate spanish day7_dv_0e8d2401:


    me "Pero fue tu idea..."


translate spanish day7_dv_8b988724:


    dv "¡Estaba borracha!"


translate spanish day7_dv_bb4b7abd:


    me "Pues como yo, qué le vas hacer..."


translate spanish day7_dv_725e3ccf:


    "Alisa estaba mirándome encolerizada, pero luego se calmó y se sentó a mi lado."


translate spanish day7_dv_40473235:


    dv "Muy bien, lo que sea que haya ocurrido, ocurrió..."


translate spanish day7_dv_238ce8d6:


    me "¿Segura?"


translate spanish day7_dv_0c27c20a:


    "Me levanté y traté de abrazarla."


translate spanish day7_dv_48a6403e:


    dv "No lo hagas..."


translate spanish day7_dv_bb97ed04:


    "Susurró, ruborizada."


translate spanish day7_dv_5ac38cf1:


    me "¿Por qué?"


translate spanish day7_dv_9e177d54:


    dv "Porque..."


translate spanish day7_dv_f374d34a:


    "Alisa negó frenéticamente con su cabeza."


translate spanish day7_dv_339d121b:


    dv "¡Porque ya son las cuatro en punto!"


translate spanish day7_dv_a6f35be5:


    me "¿Y qué? ¿No me digas que llegas tarde a alguna parte?"


translate spanish day7_dv_3daf3890:


    dv "¡Todos se irán sin nosotros!"


translate spanish day7_dv_25f7abbc:


    me "¿Irse adónde?"


translate spanish day7_dv_350637f6:


    dv "¡Hoy es el último día!"


translate spanish day7_dv_09586d84:


    me "¿El último día de qué?"


translate spanish day7_dv_141c5aec:


    "Pregunté, confundido completamente."


translate spanish day7_dv_9d1223cd:


    dv "¡El último día del plazo de esta temporada!"


translate spanish day7_dv_3d342d5a:


    me "¡¿Qué?!"


translate spanish day7_dv_4b58184b:


    "La resaca que parecía haberse ido hacía ya rato, hizo su agudo retorno."


translate spanish day7_dv_24c7fd3d:


    dv "¿No me digas que no lo sabías?"


translate spanish day7_dv_975a5f59:


    "Preguntó Alisa con sorpresa."


translate spanish day7_dv_1b078565:


    me "Entonces, ¿tú lo sabías?"


translate spanish day7_dv_0f45ad2e:


    dv "Bueno, sí...{w} Pero de alguna forma me había olvidado."


translate spanish day7_dv_e1f38c76:


    me "¿Y ahora qué deberíamos hacer?"


translate spanish day7_dv_3490638a:


    "En realidad, ésa no era la cosa más importante de la que me estuviera preocupando."


translate spanish day7_dv_e3a67172:


    th "Si hoy es el último día, entonces todos se irán a alguna parte.{w} ¡Así que puedo escapar de este campamento de alguna manera!"


translate spanish day7_dv_3b3335bc:


    dv "Bueno, no tengo ni idea."


translate spanish day7_dv_516b334e:


    me "¡Quizás aun estemos a tiempo!"


translate spanish day7_dv_713e5769:


    "Salté de la cama y empecé a vestirme rápidamente."


translate spanish day7_dv_1252f291:


    "Fue difícil, en tanto mi coordinación estaba afectada gravemente por la abundante indulgencia de ayer."


translate spanish day7_dv_9acc6e59:


    me "¿A qué estás esperando?"


translate spanish day7_dv_b1376477:


    "Alisa no contestó pero empezó a vestirse también."


translate spanish day7_dv_d7fddf78:


    "En un par de minutos más tarde estábamos en la desierta plaza."


translate spanish day7_dv_4c7430c4:


    me "¡Vayamos a la cabaña de la líder del campamento!"


translate spanish day7_dv_3a7fd3f3:


    "Tras abrir la cabaña de Olga Dmitrievna con mi llave, no hallé a nadie en su interior."


translate spanish day7_dv_a20cefa7_1:


    "..."


translate spanish day7_dv_fc48590b:


    "Comprobamos todo el campamento y descubrimos que estábamos solos aquí."


translate spanish day7_dv_a20cefa7_2:


    "..."


translate spanish day7_dv_732ccffa:


    "Tras retornar a la plaza, me senté y me cubrí la cara con mis manos como si fuera un muerto."


translate spanish day7_dv_16a845b1:


    "Todavía me sentía mal, pero ahora resultaba que tenía que resolver un serio problema."


translate spanish day7_dv_4f9d4e08:


    me "¿Dónde podrían haberse ido?"


translate spanish day7_dv_6a9c9a91:


    dv "Bueno, hacia el distrito central, supongo."


translate spanish day7_dv_e462bbf9:


    me "¿Sabes dónde es?"


translate spanish day7_dv_eb9b0712:


    dv "Aproximadamente."


translate spanish day7_dv_b5ff223e:


    me "¿Es posible alcanzarlo a pie?"


translate spanish day7_dv_3e69c35f:


    dv "No lo sé."


translate spanish day7_dv_f85f3acb:


    me "¿Y cuánto tiempo lleva en autobús?"


translate spanish day7_dv_6ac4fbb2:


    dv "Un par de horas."


translate spanish day7_dv_6373dd16:


    "Rápidamente calculé cuánto es eso en kilómetros."


translate spanish day7_dv_5039789f:


    "Teniendo en cuenta la calidad de la industria de motores de vehículos soviéticos y las carreteras soviéticas, uno podía presumir que es bastante posible alcanzarlo a pie."


translate spanish day7_dv_78543246:


    "Siempre y cuando Alisa supiera la exacta dirección."


translate spanish day7_dv_98084336:


    "Además, en mi actual situación no es fácil determinar tales pasos."


translate spanish day7_dv_cfd1657b:


    "Pero tampoco podemos quedarnos aquí.{w} Solos, sin comida en un campamento de pioneros desierto..."


translate spanish day7_dv_ce8c5196:


    "En la vida real sería definitivamente una mala idea, y no tenía otra opción salvo considerar todo como realidad."


translate spanish day7_dv_304c9144:


    me "Vayámonos."


translate spanish day7_dv_621eb081:


    dv "¿Adónde?"


translate spanish day7_dv_17368c07:


    me "¡Al distrito central!"


translate spanish day7_dv_0ff80729:


    dv "¡¿Te has vuelto loco?!"


translate spanish day7_dv_948f6b23:


    me "¿Preferirías quedarte aquí?"


translate spanish day7_dv_a094d465:


    dv "¡Claro! Se darán cuenta de que faltamos y retornarán."


translate spanish day7_dv_296ed7ce:


    me "¿No crees que es raro que no se percataran durante la partida?"


translate spanish day7_dv_d8bfa1da:


    dv "Bueno, sí, pero..."


translate spanish day7_dv_60f3d758:


    "Ella empezó a reflexionar."


translate spanish day7_dv_efc9fc55:


    dv "Pero ir a alguna parte por nuestro propio pie..."


translate spanish day7_dv_e708673b:


    me "¡Pero dijiste que sabías la dirección!"


translate spanish day7_dv_0db84079:


    dv "La sé..."


translate spanish day7_dv_33c04461:


    "Aceptó sin mucha confianza."


translate spanish day7_dv_d232a207:


    me "¡¿Entonces acaso hace falta pensar más?!{w} Tal vez encontremos algún autobús local por el camino."


translate spanish day7_dv_8f93dbc3:


    th "La verdad es que no me lo creía del todo, pero si hay alguna salida de este campamento, esa idea parecía ser bastante buena."


translate spanish day7_dv_69c97e52:


    dv "No lo sé... Como dijiste..."


translate spanish day7_dv_1488dd24:


    "Parloteó confusa."


translate spanish day7_dv_79601b2c:


    me "Pues empieza a empacar tus cosas. Nos vemos aquí en media hora."


translate spanish day7_dv_3a7b0668:


    "Corrí hacia la cabaña de Olga Dmitrievna."


translate spanish day7_dv_47b0c421:


    "Así es, no gran cosa que empacar."


translate spanish day7_dv_9e214029:


    "Metí mis ropas de invierno en una mochila y estaba a punto de irme, cuando una interesante idea se me ocurrió."


translate spanish day7_dv_1d1dfe83:


    th "¿Para qué las necesitaría en realidad?"


translate spanish day7_dv_7f3b0a96:


    th "No parece ser ni finales de verano así que, ¿para qué cargar con peso innecesario?{w} Especialmente cuando no sé hasta dónde tendré que caminar..."


translate spanish day7_dv_294dbace:


    "Tras razonar que uno podría sobrevivir a semejante tiempo sin un abrigo ni calurosas botas, guardé mi teléfono móvil con su moribunda batería en mi bolsillo y me fui a la plaza."


translate spanish day7_dv_68674ae5:


    "Tuve que esperar por lo menos unos veinte minutos a Alisa."


translate spanish day7_dv_d5afc1dc:


    "Mi resaca parecía aliviarse un poco, así que podría concentrarme mejor en la noche anterior."


translate spanish day7_dv_ee82b163:


    th "¿Qué significa realmente ella para mi?"


translate spanish day7_dv_391b61de:


    th "¿Fue solamente sexo estando ebrios o algo más?"


translate spanish day7_dv_dac5d5ff:


    th "Naturalmente no puedo hablar por Alisa.{w} Resulta incluso difícil para mí."


translate spanish day7_dv_515b15e3:


    "Empecé a recordar el curso de mi relación con ella, cómo se burló y me tomó el pelo al principio, su arrogancia y su autoconfianza..."


translate spanish day7_dv_4ec52090:


    "Fue más que compensada por la dulzura de la última noche."


translate spanish day7_dv_93eb0c2d:


    th "Estaba claro que ella ya no es la persona que quiere parecer.{w} Pero, ¿qué tipo de persona es la verdadera Alisa?"


translate spanish day7_dv_7d2a5519:


    th "¿Y siento algún sentimiento por ella?{w} Siento simpatía, algo así como afecto, pero... ¿hay algo más?"


translate spanish day7_dv_b0325c5c:


    "Mis reflexiones fueron interrumpidas por Alisa."


translate spanish day7_dv_a303f6cc:


    dv "Ten, cógela."


translate spanish day7_dv_77743949:


    "Ella me soltó una gran mochila."


translate spanish day7_dv_4907682a:


    me "¿Qué es esto?"


translate spanish day7_dv_ff782f2a:


    "Pregunté escépticamente."


translate spanish day7_dv_e780e2ad:


    dv "Mis cosas."


translate spanish day7_dv_aae0b9eb:


    me "¿Por qué tan pocas?"


translate spanish day7_dv_4eaa9948:


    dv "Me traje tanto como podía caber."


translate spanish day7_dv_5491b40f:


    me "Mira...{w} No sabemos cuán lejos tendremos que ir.{w} ¿Qué tal si coges solamente las cosas más necesarias?"


translate spanish day7_dv_44f79413:


    dv "Entonces qué, ¿sencillamente las tiro fuera? ¿Las dejo aquí?"


translate spanish day7_dv_15d90504:


    "Alisa resopló."


translate spanish day7_dv_206f97a8:


    "Primeramente, no podía comprender cómo es que tenía tantas cosas y, segundo, ¿qué hay ahí que tenga tanto valor?"


translate spanish day7_dv_8d0625c6:


    me "Pero el caballeroso deber de cargar con ello es mío, ¿cierto?"


translate spanish day7_dv_bdbe816e:


    dv "¡Por supuesto!"


translate spanish day7_dv_3aa0974f:


    "Ella sonrió socarronamente."


translate spanish day7_dv_8d91232a:


    "Valoré cuánto pesaría."


translate spanish day7_dv_278bae91:


    th "Unos diez kilogramos, supongo.{w} Puedo cargar con eso unos pocos kilómetros, pero no mucho más..."


translate spanish day7_dv_9887bbde:


    me "Comprendes que no llegaremos muy lejos con eso."


translate spanish day7_dv_d3b04189:


    dv "Bueno, lo intentaremos..."


translate spanish day7_dv_a7bd2a57:


    me "¿Quieres decir que puedo tirar algo más tarde?"


translate spanish day7_dv_7ec4ae09:


    dv "Ya veremos."


translate spanish day7_dv_521b5dfc:


    "Respondió con los ojos brillando arteramente."


translate spanish day7_dv_9886068f:


    me "Vale, te tomo la palabra."


translate spanish day7_dv_91ff7f5a:


    th "Después de todo, como tengo que cargarlo, tengo el derecho de tirarle las cosas que haya dentro."


translate spanish day7_dv_a07a03c0:


    "Y así nuestro viaje comienza."


translate spanish day7_dv_92f1963f:


    "Me llevó al menos una hora para empezar a cansarme."


translate spanish day7_dv_31827d05:


    "Nunca hubiera dicho que poseía semejante resistencia."


translate spanish day7_dv_948d6304:


    "Ya fuera porque la mochila era fácil de cargar o bien por los ejercicios de la última noche que habían entrenado mis músculos."


translate spanish day7_dv_3e917655:


    "Me detuve en la cuneta y tiré las pertenencias de Alisa al suelo."


translate spanish day7_dv_e4dfaea7:


    dv "¿Qué haces?"


translate spanish day7_dv_6932f262:


    "Claramente estaba resentida."


translate spanish day7_dv_6557adca:


    me "¿Quieres cargarlas tú misma?"


translate spanish day7_dv_b2bffcd8:


    dv "¡Por supuesto que no!"


translate spanish day7_dv_ddbf8003:


    me "En dicho caso, necesito descansar."


translate spanish day7_dv_5ced9ae4:


    "Naturalmente, la decisión racional era deshacerse de la mayoría de cosas, reservando solamente las cosas más necesarias, pero no podía dejarme llevar por semejantes medidas drásticas."


translate spanish day7_dv_a87b4b28:


    me "Has estado callada todo el tiempo..."


translate spanish day7_dv_47e8d127:


    "Alisa se quedó con la mirada perdida en la carretera que se desvanecía en la distancia."


translate spanish day7_dv_2895df22:


    dv "¿Y de qué hay que hablar?"


translate spanish day7_dv_e807ab10:


    me "No lo sé...{w} Pero te comportas como si no hubiera ocurrido nada."


translate spanish day7_dv_b9934743:


    dv "¿Acaso sucedió algo?"


translate spanish day7_dv_46ec1ab9:


    "Preguntó ausente."


translate spanish day7_dv_256912af:


    me "Bueno... Ya sabes..."


translate spanish day7_dv_6330e388:


    dv "¿Tuviste suficiente descanso? ¡Pues vámonos!"


translate spanish day7_dv_45b6c18e:


    "Alisa comenzó a caminar ágilmente alejándose de mí."


translate spanish day7_dv_1d8b65b9:


    me "¡Ey, espera un minuto!"


translate spanish day7_dv_02244c64:


    "Cargué al hombro con la mochila y corrí tras ella."


translate spanish day7_dv_a20cefa7_3:


    "..."


translate spanish day7_dv_8af27249:


    "Hemos estado deambulando en silencio por un rato, con Alisa ligeramente adelantada y yo detrás."


translate spanish day7_dv_d9f48ac6:


    "No tenía ni idea de cómo iniciar una conversación."


translate spanish day7_dv_f9d9d427:


    "Parecía estar dando muestras obvias de que los acontecimientos de anoche fueron un gran error."


translate spanish day7_dv_c81f3a7e:


    "Por lo menos eso me pareció a mí..."


translate spanish day7_dv_e4773f56:


    "Aunque bajo mi punto de vista, lo que sucedió tenía que ocurrir."


translate spanish day7_dv_bd664ace:


    "Durante los últimos días en el campamento, claramente me había sentido atraído por Alisa.{w} Y creí que ella también se sentía atraída por mi."


translate spanish day7_dv_9f1719bd:


    th "¿En ese caso cuál es el problema?"


translate spanish day7_dv_5f62b20e:


    th "¿Quizá ella no quiere que sea sólo cosa de una noche?"


translate spanish day7_dv_9bbf9c21:


    me "Sabes..."


translate spanish day7_dv_00dae805:


    "Ella se dio la vuelta y me miró."


translate spanish day7_dv_6afc1bb3:


    me "Si hice alguna cosa mal..."


translate spanish day7_dv_6812f447:


    dv "No hiciste nada mal."


translate spanish day7_dv_a31a4d9f:


    me "Pero..."


translate spanish day7_dv_904f584f:


    dv "Simplemente resultó salir así."


translate spanish day7_dv_953edcaa:


    me "¿Entonces solamente fue un accidente?"


translate spanish day7_dv_3ae4e7bf:


    dv "No dije eso."


translate spanish day7_dv_b233eb86:


    me "¿Entonces qué dijiste?"


translate spanish day7_dv_20a5d791:


    "Mi incapacidad para obtener una respuesta directa me hizo perder la paciencia, y alcé mi voz."


translate spanish day7_dv_60992bdb:


    me "¡Sólo me das indirectas ambiguas! ¿Qué hay de la franqueza y la sinceridad? ¡Tú no eras así antes!"


translate spanish day7_dv_86f37595:


    dv "La gente tiende a cambiar..."


translate spanish day7_dv_47359365:


    "Se adelantó con ritmo lento."


translate spanish day7_dv_ad786d68:


    me "¿Cambiar? ¿Llamas a esto «cambiar»?{w} ¡Eres una persona completamente diferente ahora! Al igual que Lena, sinceramente."


translate spanish day7_dv_1647718e:


    "Alisa se detuvo, pero no se giró."


translate spanish day7_dv_e9b62978:


    dv "No me compares...{w} ¡¿Entiendes?!{w} ¡No me compares con ella!"


translate spanish day7_dv_03ee962e:


    "Ella habló tranquilamente, pero había rabia en su voz."


translate spanish day7_dv_d3383672:


    me "¿Qué...?"


translate spanish day7_dv_b6ed7be6:


    "Me dejó a cuadros sorprendido."


translate spanish day7_dv_98eb301b:


    dv "Me echas la culpa por todo pero, ¿y tú qué? ¡Constantemente la estás mirando a ella! Incluso ahora al verme sigues pensando en ella..."


translate spanish day7_dv_3837a7d9:


    me "Espera, qué pasa..."


translate spanish day7_dv_c011ba0d:


    dv "¿Por qué viniste entonces ayer? ¡Podrías haberte ido con ella! Ya lo sabías..."


translate spanish day7_dv_3b51cf39:


    me "¿Saber qué?"


translate spanish day7_dv_ffc2234f:


    dv "¡Que sabías que te estaba esperando a ti!"


translate spanish day7_dv_cdd661a7:


    "Parecía ser que hacer esta confesión fue algo difícil para Alisa, pero al principio le di poca importancia a sus palabras."


translate spanish day7_dv_760c9675:


    me "Bueno, tú me dijiste que fuera ayer, así que yo..."


translate spanish day7_dv_0433bfa6:


    dv "¡No se trata de esto!"


translate spanish day7_dv_d1db8c32:


    "Alisa rompió en sollozos y se cubrió su rostro con las manos."


translate spanish day7_dv_613a279f:


    "Estaba completamente sorprendido."


translate spanish day7_dv_5bf38ff0:


    "Todavía no entendía cuál era su indirecta al respecto, por lo que era imposible hablar con normalidad."


translate spanish day7_dv_aa4f4033:


    me "Lo siento...{w} Perdóname..."


translate spanish day7_dv_a9505b4f:


    dv "¡Deja de disculparte!"


translate spanish day7_dv_e506eadf:


    me "Pero si dije o hice algo malo..."


translate spanish day7_dv_1a4b9468:


    dv "¿Y qué sentido tiene disculparse? Si ya lo hiciste...{w} Mejor haberte comportado bien desde el principio."


translate spanish day7_dv_732b1419:


    me "¡Pero no sé cómo!"


translate spanish day7_dv_bdcc6431:


    "Alisa me miró."


translate spanish day7_dv_b4bb6756:


    "Había tanta pena en su mirada, que no pude soportarla y me di la vuelta."


translate spanish day7_dv_8a74fc28:


    "Nos quedamos quietos así por un rato..."


translate spanish day7_dv_5fcaf917:


    "Pronto se recuperó y se incorporó de nuevo."


translate spanish day7_dv_1b7149e8:


    dv "Vale, olvídalo. Continuemos."


translate spanish day7_dv_7f90898b:


    "Naturalmente, no podía olvidarlo aunque quisiera, pero no tenía nada que decir de todas maneras, así que la seguí en silencio."


translate spanish day7_dv_a20cefa7_4:


    "..."


translate spanish day7_dv_bf9809fa:


    "El sol se ocultaba lentamente tras el horizonte."


translate spanish day7_dv_546ddc40:


    "Estaba inmerso en mis pensamientos e incluso me olvidé de esa pesada mochila en mi espalda."


translate spanish day7_dv_7d33e08e:


    "Tenía que decir algo, hacer alguna cosa."


translate spanish day7_dv_29a605f9:


    th "Alisa me espera.{w} Por lo menos algunas palabras..."


translate spanish day7_dv_ee5b037a:


    th "Pero, ¿por dónde debería empezar?"


translate spanish day7_dv_3522dad7:


    "Me había acostumbrado a que todo en mi vida fuera bastante simple."


translate spanish day7_dv_26ce155f:


    "Cualquier tarea parecía estar ya terminada tras sólo unos pocos pasos."


translate spanish day7_dv_bb97fe3b:


    "En vez de considerar el largo camino para mi objetivo, inmediatamente empecé a pensar acerca de los resultados, disfrutando de una victoria aun no conseguida."


translate spanish day7_dv_99fc5064:


    "Además, dudo mucho que hubiera tenido que afrontar una situación similar como esta en el mundo real."


translate spanish day7_dv_6fb63bc0:


    "Obviamente, durante mucho tiempo había soñado conque una persona se pudiera preocupar por mí."


translate spanish day7_dv_ff2cf0a2:


    "Pero ahora mismo dicha persona está frente a mí, y aun así no siento nada."


translate spanish day7_dv_79e93d21:


    "Y no puedo decirle a Alisa ni un «sí», ni un «no», tan sólo porque no sé que sucederá después."


translate spanish day7_dv_cb230162:


    "Mientras tanto se oscureció."


translate spanish day7_dv_31cc1e11:


    me "Supongo que deberíamos hacer una acampada para la noche."


translate spanish day7_dv_683a8ca7:


    dv "No nos queda mucho para llegar."


translate spanish day7_dv_3e4e35f3:


    me "¿Estás segura?{w} Y aunque fuera así, es mejor viajar durante el día."


translate spanish day7_dv_19dee4e1:


    dv "Tú sabrás..."


translate spanish day7_dv_8cef92aa:


    "Respondió fríamente."


translate spanish day7_dv_2da2cbd9:


    "Nos detuvimos al borde de un bosque."


translate spanish day7_dv_e627ef17:


    me "¿Tienes algo útil ahí dentro?"


translate spanish day7_dv_2e4b2df9:


    "Señalé a la mochila."


translate spanish day7_dv_c665ba01:


    dv "Echa un vistazo."


translate spanish day7_dv_62531094:


    "Principalmente habían calzados y ropas, pero en el fondo hallé una revista y una caja de cerillas. Suficientes como para prender un fuego."


translate spanish day7_dv_55d331e0:


    "Pronto estuvimos sentados en un árbol caído y calentando nuestras manos por el fuego."


translate spanish day7_dv_64dedd42:


    "Solamente ahora, me di cuenta al fin de que me había escapado de aquel maldito campamento."


translate spanish day7_dv_cef2d4ff:


    th "No hallé ninguna respuesta; por el contrario, todo se había vuelto aun más enmarañado mientras, supongo, me pasaba el resto de mi vida en esta realidad."


translate spanish day7_dv_584626de:


    th "Si no tomo en consideración todos los aspectos de ciencia ficción, está claro que estoy en alguna parte del sur al final de la década de los ochenta."


translate spanish day7_dv_c66b43bf:


    th "Cómo y, aun más relevante, por qué fui traído aquí no es tan importante ahora, en tanto me tengo que adaptar de alguna forma."


translate spanish day7_dv_0cc0cf06:


    th "Y debería empezar de nuevo con la muchacha sentada a mi lado."


translate spanish day7_dv_48a428eb:


    me "Y bien, ¿qué haremos cuando alcancemos el distrito central?"


translate spanish day7_dv_9eca505b:


    dv "¡Llamar a nuestros padres, qué si no!"


translate spanish day7_dv_94184036:


    "Alisa sonrió astutamente.{w} Ella tenía alguien a quien llamar."


translate spanish day7_dv_f4eb646d:


    me "¿Y luego?"


translate spanish day7_dv_e639a729:


    dv "Y entonces todo se acabó."


translate spanish day7_dv_e54b39e9:


    me "¿Cómo?"


translate spanish day7_dv_35919e4b:


    dv "Bueno, nos iremos a nuestras casas."


translate spanish day7_dv_22dddcd1:


    "Realmente no estaba sorprendido por esta respuesta, sencillamente no había pensado acerca de qué sucedería tras el fin del plazo de la temporada."


translate spanish day7_dv_5c1ed51e:


    me "¿Y eso es todo?"


translate spanish day7_dv_964c391d:


    dv "¿Qué si no?"


translate spanish day7_dv_ee102f21:


    me "No lo sé...{w} Tras todo lo que ocurrió..."


translate spanish day7_dv_be621763:


    dv "¡Pero es que no ocurrió nada!"


translate spanish day7_dv_bdfc5195:


    "Me dijo alegremente."


translate spanish day7_dv_7a16e55f:


    me "¿Qué quieres decir conque «no ocurrió nada»?"


translate spanish day7_dv_5788094d:


    dv "Tendrás una oportunidad para encontrar a Lena."


translate spanish day7_dv_7540e341:


    "Estaba sorprendido, Alisa lo dijo con absoluta amabilidad."


translate spanish day7_dv_1deed426:


    me "¡Como si necesitara a Lena!"


translate spanish day7_dv_bad_3bbe4c98:


    dv "¿A no?"


translate spanish day7_dv_bad_af26aec3:


    "Para ser honestos, Lena era la última persona en la que estaba pensando ahora."


translate spanish day7_dv_bad_3d1847ad:


    me "No."


translate spanish day7_dv_bad_7f24994f:


    dv "Entonces, ¿tal vez soy yo?"


translate spanish day7_dv_bad_eb6f47e6:


    "Miré a Alisa con atención.{w} Ella sonrió por despecho."


translate spanish day7_dv_bad_3674b5bc:


    th "Y hace nada estaba llorando..."


translate spanish day7_dv_bad_86e80d63:


    me "¿Qué pasa contigo?"


translate spanish day7_dv_bad_ff946cd9:


    "Evité la pregunta."


translate spanish day7_dv_bad_50f50099:


    dv "¿Qué opinas de mi?"


translate spanish day7_dv_bad_ae44c7f6:


    me "No lo sé..."


translate spanish day7_dv_bad_0c350a0a:


    dv "¿Qué tal si lo dices sencillamente sin rodeos?"


translate spanish day7_dv_bad_90c7a728:


    "Un largo silencio se hizo."


translate spanish day7_dv_bad_ec6e249c:


    th "Alisa está esperando una respuesta directa, pero no puedo ni saberla yo mismo."


translate spanish day7_dv_bad_785620ea:


    th "Este campamento de pioneros... y ahora esta muchacha, que quiere escuchar algo que no puedo decir tan fácilmente."


translate spanish day7_dv_bad_5dc514e5:


    me "Verás..."


translate spanish day7_dv_bad_a3d0602e:


    "Y entonces lo comprendí de verdad."


translate spanish day7_dv_bad_cff69fc4:


    "Comprendí que ahora no hay que ser ingenioso o aguantarse las cosas para uno mismo."


translate spanish day7_dv_bad_881eda46:


    "Casi logré romper la jaula.{w} ¡Lo peor se acabó!"


translate spanish day7_dv_bad_ef1f22d6:


    "O, por el contrario, lo peor está por venir pero es la hora de hablar claramente."


translate spanish day7_dv_bad_fd117b88:


    me "Verás...{w} Lo que estás esperando de mí es casi imposible."


translate spanish day7_dv_bad_a80e7dec:


    dv "Es como si todo fuera imposible para ti."


translate spanish day7_dv_bad_393056bc:


    "Ella resopló."


translate spanish day7_dv_bad_bc3ffa54:


    me "Es raro decirlo, pero es la verdad.{w} No sé lo que me sucederá... no necesariamente mañana, ¡sino en cualquier instante!"


translate spanish day7_dv_bad_1183f209:


    dv "¿Y qué puede ser? ¿Acaso te desvanecerás en la nada?"


translate spanish day7_dv_bad_b9c3fa63:


    me "Muy posiblemente."


translate spanish day7_dv_bad_d5f6c4f2:


    "Ella rompió a carcajadas."


translate spanish day7_dv_bad_ac122a30:


    dv "Sabes, estaba preparada para cualquier excusa, pero no para una como esa."


translate spanish day7_dv_bad_0fdb45a6:


    me "No son excusas, sólo escúchame..."


translate spanish day7_dv_bad_1bc70a2f:


    "Alisa me hizo una mirada atenta y, al menos parecía, que no tendría intención de interrumpirme."


translate spanish day7_dv_bad_8de689be:


    me "Verás, no pertenezco a este lugar.{w} No estoy hablando del campamento, sino de todo el mundo.{w} Todos los de aquí sois personas completamente extrañas e incomprensibles para mí."


translate spanish day7_dv_bad_2ee7cffc:


    dv "Como lo eres para nosotros, en realidad."


translate spanish day7_dv_bad_2a05cd1b:


    me "Incluso no estoy seguro de si eres de mi planeta o de mi realidad."


translate spanish day7_dv_bad_cb2bf9d2:


    dv "Acaba ya..."


translate spanish day7_dv_bad_117896c8:


    "Dijo Alisa cansada."


translate spanish day7_dv_bad_340880cb:


    me "Imagínate que una vez te vas a dormir y despiertas en un lugar completamente distinto.{w} Y no solamente un lugar, sino también en un tiempo diferente."


translate spanish day7_dv_bad_c0004c2a:


    me "¡Eso es lo que me ha ocurrido!{w} Solía vivir a principios del siglo XXI en una ciudad lejos de aquí."


translate spanish day7_dv_bad_79a7b35b:


    me "Era de algún modo más mayor que como veo ahora.{w} Tenía mi propia vida, al margen de si era buena o mala, era mía."


translate spanish day7_dv_bad_e281434b:


    me "Y entonces una noche cogí un autobús, me dormí y me desperté aquí."


translate spanish day7_dv_bad_0743d894:


    me "Como un pionero con diecisiete años."


translate spanish day7_dv_bad_560a2626:


    "Alisa me miraba sorprendida."


translate spanish day7_dv_bad_3439946a:


    dv "¿Y esperas que te crea?"


translate spanish day7_dv_bad_be346c4b:


    me "No lo sé, depende de ti...{w} La verdad es que no fue fácil para mi contarte eso."


translate spanish day7_dv_bad_82b2a360:


    dv "Eres un maestro cuentacuentos, en serio."


translate spanish day7_dv_bad_41aa7bd4:


    me "Espera un segundo..."


translate spanish day7_dv_bad_de80ec27:


    "Hurgué entre mis bolsillos y le mostré mi teléfono."


translate spanish day7_dv_bad_f584d6ef:


    "Para mi gran sorpresa, todavía quedaba algo de batería y la pantalla brillaba encendida."


translate spanish day7_dv_bad_15c145d7:


    me "¿Has visto en alguna ocasión algo como esto? ¿Tienes algo así?"


translate spanish day7_dv_bad_45f81bbc:


    "Se lo dejé a Alisa."


translate spanish day7_dv_bad_56878f3a:


    "Ella no paraba de toquetearlo, apretando los botones."


translate spanish day7_dv_bad_aa4e170d:


    dv "Bueno... Algún tipo de juguete importado de fuera. ¿Y qué?"


translate spanish day7_dv_bad_135e53c8:


    me "No es un juguete, es un teléfono."


translate spanish day7_dv_bad_99ad611b:


    dv "¿Y dónde están los cables, y los auriculares?"


translate spanish day7_dv_bad_0467a4c4:


    "Se rio."


translate spanish day7_dv_bad_2f7003bc:


    me "No hay necesidad de ellos.{w} Desafortunadamente, no puedo demostrarte cómo funcionan porque no han sido inventados en tu época."


translate spanish day7_dv_bad_1e3907df:


    dv "Claro, seguro..."


translate spanish day7_dv_bad_d45cda8e:


    "Dijo escépticamente y me devolvió el teléfono de vuelta."


translate spanish day7_dv_bad_1b521e0d:


    me "De todas formas, es decisión tuya si quieres creerme o no.{w} Te cuento todo esto sólo para que sepas en qué situación me encuentro ahora."


translate spanish day7_dv_bad_48d8149b:


    me "Todavía no sé qué me ocurrirá en una hora, así que tales decisiones..."


translate spanish day7_dv_bad_4741fcda:


    dv "Está bien, no tienes porqué decir más.{w} Y no tienes que tomar ninguna decisión."


translate spanish day7_dv_bad_3a7fe15b:


    "Se levantó y tiró algo más de leña al fuego."


translate spanish day7_dv_bad_b6877455:


    me "Si te he ofendido..."


translate spanish day7_dv_bad_b4919381:


    dv "Qué va, todo está bien."


translate spanish day7_dv_bad_3e1254a0:


    "Dijo fríamente."


translate spanish day7_dv_bad_39cad26b:


    dv "Lo entiendo.{w} Eres un visitante del futuro, por lo que muchachas normales como yo no te interesan."


translate spanish day7_dv_bad_71b71623:


    me "Me has malinterpretado..."


translate spanish day7_dv_bad_cac50a1b:


    dv "No, no lo hice.{w} Es una historia muy creíble, lógica y, por encima de todo, verídica."


translate spanish day7_dv_bad_d0bafdfc:


    dv "Cualquier muchacha picaría."


translate spanish day7_dv_bad_6facbf02:


    me "Entonces, ¿todavía piensas que estoy inventándome excusas?"


translate spanish day7_dv_bad_9f0b84db:


    "Alisa permaneció callada."


translate spanish day7_dv_bad_2284906f:


    me "¿O que me volví completamente loco?"


translate spanish day7_dv_bad_d558b838:


    dv "Una podría esperarse cualquier cosa de ti."


translate spanish day7_dv_bad_9514ab02:


    me "¿Por qué tienes que ser así?"


translate spanish day7_dv_bad_258b2342:


    "Dije con tristeza."


translate spanish day7_dv_bad_30e02149:


    dv "¿Y yo qué? No hice nada malo."


translate spanish day7_dv_bad_5345aad1:


    "Se rio a despecho."


translate spanish day7_dv_bad_0039dd59:


    me "Sólo imagina que todo es cierto, y trata de comprender por qué me comporto así..."


translate spanish day7_dv_bad_a51804eb:


    dv "Ya me lo imaginé, ya comprendí..."


translate spanish day7_dv_bad_2534d25c:


    "Iba a decir otra cosa, pero se escuchó una bocina por la carretera."


translate spanish day7_dv_bad_99a62d84:


    "Había un autobús en la cuneta.{w} Un LiAZ de una línea normal y corriente."


translate spanish day7_dv_bad_a3ea7899:


    "Es raro que hayamos notado su aproximación."


translate spanish day7_dv_bad_0725f998:


    th "¡Por fin! ¡La salvación!"


translate spanish day7_dv_bad_d5a384e6:


    dv "¿Nos vamos?"


translate spanish day7_dv_bad_53da10dd:


    "Preguntó Alisa con una voz completamente insulsa y sin emoción."


translate spanish day7_dv_bad_4bfb0b4b:


    me "¡Sí, claro!"


translate spanish day7_dv_bad_963da71b:


    "Agarré la mochila."


translate spanish day7_dv_bad_eb09504e:


    dv "Puedo arreglármelas por mí misma."


translate spanish day7_dv_bad_376173e1:


    "Ella me cogió la mochila."


translate spanish day7_dv_bad_3fd88211:


    "Era obvio que era muy pesada para ella, pero no me negué."


translate spanish day7_dv_bad_b5b9d022:


    "Bien pronto, nos sentamos tranquilamente en los asientos del final del autobús."


translate spanish day7_dv_bad_994c7297:


    "Tenía varios sentimientos encontrados en mi interior. Por un lado, me alegré inconmensurablemente de volver al mundo civilizado; por otra parte, me sentía culpable por la conversa a medias con Alisa."


translate spanish day7_dv_bad_26ebea5b:


    me "Lo siento, de que haya actuado así...{w} Esperabas algo diferente..."


translate spanish day7_dv_bad_af909b39:


    dv "No esperaba nada de ti."


translate spanish day7_dv_bad_8cef92aa:


    "Respondió fríamente."


translate spanish day7_dv_bad_89272480:


    me "Sólo quería que comprendieras...{w} Y, en cuanto a nuestra relación, ¡me gustas! ¡De verdad!"


translate spanish day7_dv_bad_64464ef0:


    dv "Me alegra escuchar eso..."


translate spanish day7_dv_bad_bb653366:


    "Pero su voz no sonaba tan alegre."


translate spanish day7_dv_bad_65ecf8a3:


    me "Lo dije sinceramente.{w} Verás, no sé lo que ocurrirá, pero aquí y ahora..."


translate spanish day7_dv_bad_0507e954:


    dv "Supongo que ayer por la noche me querías incluso más."


translate spanish day7_dv_bad_3b5928b5:


    me "No, me estás malinterpretando..."


translate spanish day7_dv_bad_68ac3caa:


    dv "¿Deberíamos hacerlo otra vez mientras el conductor no nos puede ver?"


translate spanish day7_dv_bad_cddb4809:


    me "Cálmate por un instante, ¿vale? ¡Ni tan siquiera lo pensé!"


translate spanish day7_dv_bad_4e1d6150:


    dv "¿Qué pasa?{w} Lo quieres y no me importa, es la única cosa para la que soy buena..."


translate spanish day7_dv_bad_30b2cc2a:


    me "Alisa, espera..."


translate spanish day7_dv_bad_e4d66988:


    dv "Vale, quiero dormir."


translate spanish day7_dv_bad_fe96cf03:


    "Se recostó en su asiento y cerró sus ojos."


translate spanish day7_dv_bad_079f62e5:


    me "Espera un segundo..."


translate spanish day7_dv_bad_b5173cb6:


    dv "¡Estoy durmiendo! ¡Hablémos más tarde!"


translate spanish day7_dv_bad_af5a03bc:


    "No me negué."


translate spanish day7_dv_bad_699d1594:


    "Después de todo, es bueno consultar con tu almohada. Además, Alisa necesitaba algo de tiempo para calmarse."


translate spanish day7_dv_bad_6304447d:


    "Aunque desde luego, estaba desesperado por explicárselo todo a ella de forma que lo comprendiera."


translate spanish day7_dv_bad_71df3cde:


    "Pero me contuve."


translate spanish day7_dv_bad_1a5a841d:


    "Aun así, yo estaba enfermo de corazón."


translate spanish day7_dv_bad_a20cefa7:


    "..."


translate spanish day7_dv_bad_4a65de63:


    "Estábamos solos en el autobús."


translate spanish day7_dv_bad_7d4a72bc:


    "Afuera se veían las sombras oscuras que atravesábamos. A veces era posible discernir un árbol, un río o una torre de alta tensión de la red eléctrica."


translate spanish day7_dv_bad_0041c3e7:


    "Pero el paisaje no parecía diferir de aquel del campamento. ¿Quizás el mundo sea un círculo y estamos moviéndonos a través de él?"


translate spanish day7_dv_bad_401b2186:


    "De todas maneras, los alrededores eran la última de mis preocupaciones ahora... Me quedé mirando sólo al suelo y pensé sobre mañana."


translate spanish day7_dv_bad_594edbed:


    th "¿Qué ocurrirá?{w} Esta ciudad, este planeta, este tiempo... todos son extraños para mí..."


translate spanish day7_dv_bad_30bdeb77:


    th "Y continuar conversando con Alisa, quien estaba totalmente decepcionada conmigo..."


translate spanish day7_dv_bad_a20cefa7_1:


    "..."


translate spanish day7_dv_bad_4eeeabc5:


    "El proceso de esperar suele ser más tedioso que su resultado."


translate spanish day7_dv_bad_85334293:


    "Y al fin, las cosas que tienen que suceder no parecen tan importantes o temibles. Todas tus fuerzas están agotadas. Solamente hay cansancio y vacío en tu alma."


translate spanish day7_dv_bad_ab33fd37:


    "El autobús estaba dirigiéndose hacia lo desconocido. No podía resistirme y caí dormido."


translate spanish day7_dv_good_b6233cde:


    dv "Yo no estaría tan segura."


translate spanish day7_dv_good_7a0d2a29:


    me "Y yo te digo que estoy seguro. ¡Déjame que sea yo quien decida!"


translate spanish day7_dv_good_cf61573a:


    dv "¿A sí?{w} ¿Cuándo empezaste a hacer tales tonterías?"


translate spanish day7_dv_good_d0b801cc:


    me "Hace..."


translate spanish day7_dv_good_a586fdb0:


    "De repente me acordé de mi actual edad."


translate spanish day7_dv_good_7e088224:


    me "¡Diecisiete años atrás!"


translate spanish day7_dv_good_301b1799:


    dv "Bueno, bueno, admirable...{w} Pero para ser honesta, no me di cuenta de nada de eso."


translate spanish day7_dv_good_80e71ea5:


    me "¿Exactamente qué deberías haber notado?"


translate spanish day7_dv_good_0ed90974:


    dv "¡Que eres capaz de decirme algo sin ningún género de dudas!"


translate spanish day7_dv_good_012bdaba:


    "Ella tenía toda la razón sobre este aspecto."


translate spanish day7_dv_good_570d60e3:


    me "¿Y qué es exactamente lo que quieres escuchar?"


translate spanish day7_dv_good_fa78d9d9:


    dv "¡Ya lo sabes!"


translate spanish day7_dv_good_24d8e0d2:


    "Este juego de indirectas podía durar por siempre."


translate spanish day7_dv_good_7dc7d107:


    me "Si lo que quieres saber es qué siento respecto a ti, entonces te hablaré con honestidad... No lo sé ni yo mismo."


translate spanish day7_dv_good_1abe160f:


    dv "¡De eso es exactamente de lo que te estoy hablando!"


translate spanish day7_dv_good_e51d106b:


    "Alisa se dio la vuelta."


translate spanish day7_dv_good_667cf5e8:


    me "Pero qué puedo hacer, ¡si es la verdad!"


translate spanish day7_dv_good_421779c8:


    dv "¿Y qué puedo hacer yo?"


translate spanish day7_dv_good_30e22da3:


    me "¡No te pido nada!"


translate spanish day7_dv_good_4c06e27c:


    dv "¿Sí? ¿Estás seguro?"


translate spanish day7_dv_good_eee643db:


    me "Claro..."


translate spanish day7_dv_good_218d5a60:


    "Contesté tras un rato."


translate spanish day7_dv_good_a89de161:


    dv "¿Y qué estuviste haciendo durante todos los siete días en el campamento?"


translate spanish day7_dv_good_cbe79a70:


    me "¿Qué estuve haciendo?"


translate spanish day7_dv_good_6010fd21:


    dv "Lo estuviste pidiendo, ¿no?"


translate spanish day7_dv_good_014b09c2:


    me "¿Qué estuve pidiendo? ¿Por qué razón? ¿Puedes hablar de alguna manera en que la gente pueda entenderte? ¡Los que leen la mente están de vacaciones!"


translate spanish day7_dv_good_8a8032f7:


    dv "Si quisieras, lo entenderías..."


translate spanish day7_dv_good_206c4e41:


    me "¡No puedo entenderlo sin una explicación!"


translate spanish day7_dv_good_0ec98f8c:


    dv "Está bien, esto es una conversa improductiva..."


translate spanish day7_dv_good_42c9c2a4:


    "Se puso en pie y se dirigió hacia la carretera."


translate spanish day7_dv_good_b9225b9a:


    me "¡De ninguna manera!"


translate spanish day7_dv_good_b0133881:


    "Perdí mi paciencia completamente, así que me puse en pie de un salto, corrí tras ella en un par de zancadas, le agarré de la mano y estiré de ella hacia mí."


translate spanish day7_dv_good_05c5777e:


    "Pero había calculado mal la cantidad de fuerza que debía usar, y nos caímos al suelo con Alisa tumbada debajo de mí."


translate spanish day7_dv_good_01ae0d80:


    dv "¿Qué, lo de ayer no fue suficiente? ¿Quieres más?"


translate spanish day7_dv_good_18438f67:


    "Su rostro estaba enrojecido y sus ojos estaban brillando."


translate spanish day7_dv_good_444788e7:


    "Me sentí confundido de veras. Solté la mano de la muchacha, pero no se levantó de golpe."


translate spanish day7_dv_good_db56aa2f:


    me "Solamente quiero comprenderlo, ¿en qué me equivoqué?"


translate spanish day7_dv_good_dd60046c:


    dv "¿Por qué siempre piensas que hiciste algo mal?"


translate spanish day7_dv_good_7de960a6:


    me "¡Porque también piensas así!"


translate spanish day7_dv_good_dcad2da3:


    dv "¿Quién te dijo eso? ¡No lo hice!"


translate spanish day7_dv_good_981eb02b:


    me "¡Pero puedo verlo!"


translate spanish day7_dv_good_e3bdc4b7:


    dv "Lo ves todo, ¿no?{w} Ves y extraes conclusiones de que no deberías interferir, de que deberías quedarte al margen, no decir nada..."


translate spanish day7_dv_good_7351a8db:


    me "¡Saco conclusiones basándome en la lógica!"


translate spanish day7_dv_good_77d7b000:


    dv "¿Estás seguro de que tú lógica es correcta?"


translate spanish day7_dv_good_5c588803:


    "No sabía qué contestar a eso."


translate spanish day7_dv_good_40534c30:


    "Por un rato estuvimos tumbados ahí."


translate spanish day7_dv_good_b315c215:


    "Estaba completamente confundido, y no parecía que Alisa quisiera continuar nuestra conversación."


translate spanish day7_dv_good_121b1f59:


    "Finalmente, ella me apartó y se puso en pie."


translate spanish day7_dv_good_c09a48f6:


    dv "¿Por qué estás aquí ahora... conmigo?"


translate spanish day7_dv_good_359b6740:


    me "Porque todo el mundo se fue del campamento y nosotros nos quedamos..."


translate spanish day7_dv_good_826048a2:


    dv "No estoy hablando de eso..."


translate spanish day7_dv_good_4f930f50:


    me "Bueno, porque me gustas..."


translate spanish day7_dv_good_58e259d9:


    "Finalmente me atreví a decírselo directamente."


translate spanish day7_dv_good_3d42189c:


    dv "¿Estás seguro?{w} ¿Y qué hay de Lena?{w} Ella siempre estaba a tu lado..."


translate spanish day7_dv_good_f4da743f:


    dv "Quizás es que ella supone demasiado trabajo.{w} ¿Y yo? Soy muy fácil... ¡solamente chascas tus dedos y estoy en tu cama!"


translate spanish day7_dv_good_176694f4:


    th "No diría que eso fuera tan fácil con ella..."


translate spanish day7_dv_good_f37462ea:


    me "¡No me atribuyas palabras que no dije!"


translate spanish day7_dv_good_2a75e18b:


    dv "Si no dices nada, ¡así que no hay mucho que decir!{w} ¡Pero piensas por una docena de personas!"


translate spanish day7_dv_good_2563ff87:


    "Bueno, ella tenía toda la razón en ese extremo."


translate spanish day7_dv_good_d5c105bf:


    me "¡No pensé en absoluto en algo así!"


translate spanish day7_dv_good_2e22f90c:


    dv "Pues entonces, ¿me tengo que creer simplemente todo esto?"


translate spanish day7_dv_good_ccfdaf24:


    me "No tienes por qué creerlo. ¡No te obligo!{w} Pero es la verdad, ¡así que no me inventé nada!"


translate spanish day7_dv_good_fc9f6ff5:


    dv "¡Las palabras tienen que ser demostradas con hechos!"


translate spanish day7_dv_good_30792c5e:


    th "¿Y qué es lo que espera de mí?"


translate spanish day7_dv_good_37530dee:


    me "Bueno, podríamos hacerlo otra vez..."


translate spanish day7_dv_good_dae3bc9a:


    "Dije con una sonrisa estúpida."


translate spanish day7_dv_good_d835fa5b:


    "No tengo ni idea de cómo me atreví a bromear en dicha situación. Especialmente tratándose de una broma como {i}ésa{/i}."


translate spanish day7_dv_good_d43bb4d6:


    dv "¡Claro! ¡Venga vamos!"


translate spanish day7_dv_good_7a01cb1d:


    "Dijo Alisa con una voz tranquila, caminó hasta el fuego y empezó a desnudarse."


translate spanish day7_dv_good_af45c70b:


    "Me puse en pie de un salto, corrí tras ella y le agarré de su brazo."


translate spanish day7_dv_good_0d1b5397:


    me "¡¿Qué haces?! ¡Espera! ¡No quise decir eso!"


translate spanish day7_dv_good_ed6b05b6:


    dv "Pero bueno, ¿cuál es el problema?{w} ¿No me necesitas sólo para eso?"


translate spanish day7_dv_good_53efb905:


    me "Por supuesto que no. ¡Me has entendido mal!"


translate spanish day7_dv_good_d81dcc94:


    dv "¡¿Pues entonces cómo debería entenderte?!"


translate spanish day7_dv_good_633e9a1b:


    "Liberó de golpe su brazo y empezó a llorar."


translate spanish day7_dv_good_a184da49:


    "Me tumbé junto a ella, la abracé y comencé a acariciar su cabeza."


translate spanish day7_dv_good_7c4d73aa:


    me "Todo va bien. Cálmate..."


translate spanish day7_dv_good_28b62007:


    dv "¡Déjame sola!"


translate spanish day7_dv_good_be68715b:


    "Dijo entre lágrimas, pero no intentó liberarse."


translate spanish day7_dv_good_8971dc3e:


    me "Verás, tuve una vida complicada antes de este campamento. No solía estar acostumbrado a esto... No sé cómo actuar frente a situaciones como ésta."


translate spanish day7_dv_good_da2cd44a:


    me "He estado solo durante mucho tiempo, sin amigos, sin seres queridos."


translate spanish day7_dv_good_ee9729c2:


    me "Simplemente he olvidado cómo sentir y cómo amar.{w} Me resulta difícil."


translate spanish day7_dv_good_70bdd2cd:


    me "Es mucho mejor no involucrarse en esos asuntos. Al final, se volvió lo normal no hacerlo.{w} Y aquí estamos... en algo que nunca podría haberme imaginado."


translate spanish day7_dv_good_f83fac68:


    me "No significa que sea un bastardo egoísta...{w} Aunque supongo que tal vez lo sea..."


translate spanish day7_dv_good_18509af9:


    me "No puedo hacerlo así, de inmediato..."


translate spanish day7_dv_good_cc722f9f:


    "Alisa no respondió y continuó llorando."


translate spanish day7_dv_good_49a9ce42:


    me "¡Pero sé una cosa ciertamente! No hay nada entre mí y Lena.{w} Ella es una amiga, nada más. Nunca la vi de otra manera."


translate spanish day7_dv_good_a5aef048:


    me "¡Y te necesito! No solamente para lo que tú crees que quiero."


translate spanish day7_dv_good_9e669319:


    me "Simplemente es que no sé cómo decirte las cosas que quieres escucharme decir... Comportan obligaciones...{w} Verás, ahora estoy en una situación donde no sé ni tan siquiera qué es lo que me sucederá, y aquí..."


translate spanish day7_dv_good_48c0bf6b:


    "Alisa no parecía estar escuchándome en absoluto, o sencillamente no se creía mis excusas."


translate spanish day7_dv_good_cd717fa3:


    th "Bueno, debería estar orgulloso de mí mismo... Hice llorar a una muchacha. ¿Y qué muchacha? Alisa, ¡quien a primera vista parecía ser completamente ajena a cualquier sentimiento romántico!"


translate spanish day7_dv_good_06e93473:


    me "Sabes, no puedo tomar esta decisión por ambos.{w} Únicamente puedo decir por mí mismo que estoy preparado para estar contigo, de apoyarte, ¡de protegerte!"


translate spanish day7_dv_good_6ef95196:


    me "Pero tú...{w} ¿Para qué me ibas a necesitar?"


translate spanish day7_dv_good_4a9891cd:


    me "No tengo nada en mi alma, no sé qué es lo que quiero de esta vida.{w} Además, estoy aquí ahora, pero mañana puede que me haya ido..."


translate spanish day7_dv_good_6196bca7:


    "Alisa dejó de llorar por un instante."


translate spanish day7_dv_good_64da3e76:


    dv "¿No entendiste que yo ya tomé esa decisión hace ya mucho tiempo?"


translate spanish day7_dv_good_2e84db32:


    me "Entonces..."


translate spanish day7_dv_good_5b7bc846:


    "Alzó su mirada hacia mi con los ojos llorosos."


translate spanish day7_dv_good_1ca244f1:


    "Pude ver felicidad en ellos por un instante."


translate spanish day7_dv_good_7419452b:


    "Alisa se lanzó a mis brazos y me besó dulcemente."


translate spanish day7_dv_good_0f399005:


    "Fue el mejor beso de toda mi vida."


translate spanish day7_dv_good_794a0ba9:


    "No, no era tan pasional como el de ayer por la noche, pero había tanta ternura, confianza y amor que, sencillamente, me derretía en ella, en sus labios, en sus brazos."


translate spanish day7_dv_good_fe659328:


    "Me olvidé de todo, sobre el campamento, sobre mi pasado y las dudas que me atormentaban justo hace un momento antes."


translate spanish day7_dv_good_e3bf8cd7:


    "Ella era todo lo que yo necesitaba.{w} Ahora.{w} ¡Y para siempre!"


translate spanish day7_dv_good_63829ee5:


    "Este beso podría haber durado para siempre, pero de repente una bocina se escuchó cerca, como si estuviera dentro de mi cabeza."


translate spanish day7_dv_good_48538083:


    "Me giré hacia la carretera y vi un autobús.{w} Un antiguo LiAZ, era como uno normal y corriente."


translate spanish day7_dv_good_377572e1:


    "Por un rato nos quedamos abrazados y lo miramos."


translate spanish day7_dv_good_1a224f5a:


    "Pronto, el conductor salió fuera y nos gritó."


translate spanish day7_dv_good_f2e98a50:


    FIXME_voice "¡Ey, palomitas! ¡Éste es el último autobús hacia la ciudad! ¿Os venís?"


translate spanish day7_dv_good_a640ab70:


    dv "¿Nos vamos?"


translate spanish day7_dv_good_60c6ccbe:


    "Dijo Alisa alegremente."


translate spanish day7_dv_good_4b4b5248:


    "Nos pusimos de pie de golpe. Agarré la mochila, cogí a Alisa por la mano y corrimos hacia el autobús."


translate spanish day7_dv_good_74424fe8:


    "En un par de segundos nos acomodamos en los asientos de atrás."


translate spanish day7_dv_good_b0a142e5:


    "El conductor nos gritaba alguna cosa desde su lugar, pero no le escuché."


translate spanish day7_dv_good_7e7c36fa:


    th "¡Ahí va, una embarcación de rescate, dirigiéndose hacia una nueva vida!"


translate spanish day7_dv_good_37a9c52a:


    "Miré a Alisa. Estaba ansioso de decirle alguna cosa, de continuar la conversación de antes junto al fuego. Ella estaba durmiendo tranquilamente, descansando su cabeza en mi hombro."


translate spanish day7_dv_good_099b09a8:


    "No la desperté. No había necesidad. Teníamos mucho tiempo por delante nuestro."


translate spanish day7_dv_good_4a65de63:


    "No había nadie en el autobús salvo nosotros."


translate spanish day7_dv_good_22a0d0c3:


    "El autobús iba rápido, dejando atrás no únicamente mi {i}anterior{/i} vida, sino también la semana que había transcurrido en el campamento de pioneros llamado Sovyonok."


translate spanish day7_dv_good_0a03607e:


    "Tuve más sucesos a lo largo de estos siete días, que durante todos los años de mi vida {i}real{/i}.{w} ¿Y qué más me estaba esperando más allá en el futuro?"


translate spanish day7_dv_good_200afadf:


    "Los paisajes comunes de los alrededores que se veían a través de la ventana no me interesaban mucho. Tan sólo observaba dormir tranquilamente a Alisa."


translate spanish day7_dv_good_83fb48fe:


    "Mi corazón estaba lleno de calidez y calma, y mi cabeza al fin detuvo la zozobra de pensamientos."


translate spanish day7_dv_good_a20cefa7:


    "..."


translate spanish day7_dv_good_0ec09f2e:


    "Pero la felicidad es un estado frágil. Muchas cosas la pueden destruir. El tiempo, por ejemplo."


translate spanish day7_dv_good_6b42fada:


    "Cuanto más tiempo dura tu felicidad, más común se vuelve. Las emociones se atenuan. Nuevos problemas y preocupaciones surgen."


translate spanish day7_dv_good_7ba8dcd0:


    "Me sentía bien, pero el cansancio acumulado no me permitió disfrutar el momento. Me dormí..."


translate spanish day7_sl_d19d9343:


    "Me desperté con los primeros rayos del sol a causa del frío."


translate spanish day7_sl_05d73391:


    "Obviamente, el saco de dormir estaba caliente y no tuvo suficiente tiempo como para ser cubierto por la escarcha, pero aun así fue una novedad pasarme la noche en un bosque."


translate spanish day7_sl_8df959d7:


    "Cuidadosamente, con el fin de evitar despertar a Slavya, salí fuera del saco."


translate spanish day7_sl_01b6883d:


    "El fresco aire matutino finalmente disipó mi somnolencia, y la serenidad de la última noche me retornó."


translate spanish day7_sl_fa705823:


    "Supongo que fue el momento más feliz de mi vida."


translate spanish day7_sl_78603168:


    "Intimidad, ternura, amor, pasión... todas esas emociones interconectadas conmigo durante esas breves horas."


translate spanish day7_sl_6b037955:


    "Miré a Slavya... era bella.{w} Su rostro estaba en una expresión que carecía de emociones, con absoluta tranquilidad y paz."


translate spanish day7_sl_2b82da62:


    th "Supongo que así es como los ángeles duermen..."


translate spanish day7_sl_f50d77d4:


    "Como no tenía ningún deseo de despertarla, simplemente me senté a su lado y observé cómo la mañana comenzaba."


translate spanish day7_sl_3e13649b:


    "La naturaleza...{w} El fresco rocío, el liviano viento, las hojas trémulas de los árboles y las manchas de la luz solar en el agua, ayer solían ser ajenas para mí."


translate spanish day7_sl_470e2440:


    "La jungla de cristal de una gran ciudad era el único bosque que conocía antes."


translate spanish day7_sl_485f4671:


    "Y si alguien me hubiera dicho que simplemente me podría sentar y disfrutar de cosas así, a pesar de la fría noche y los molestos mosquitos, sin una computadora o internet, claramente no le habría creído."


translate spanish day7_sl_e1ffc0eb:


    sl "¡Buenos días!"


translate spanish day7_sl_69f37ce2:


    "Miré a Slavya."


translate spanish day7_sl_987561b6:


    me "¡Buenas! ¿Dormiste bien?"


translate spanish day7_sl_3b4389a0:


    sl "Mejor que nunca."


translate spanish day7_sl_46eff4e1:


    me "Yo también."


translate spanish day7_sl_4932445c:


    "Me alcanzó y me besó."


translate spanish day7_sl_b7a6a5b1:


    "El beso me pareció sempiterno y sencillamente me deshice en él."


translate spanish day7_sl_4925608a:


    sl "Tomar el desayuno sería genial."


translate spanish day7_sl_fab71e09:


    "Finalmente Slavya habló, dejándome ir."


translate spanish day7_sl_1066065f:


    me "Claro, supongo."


translate spanish day7_sl_78042d2e:


    sl "¿Sabes qué hora es?"


translate spanish day7_sl_1e9c08b9:


    me "No, pero creo que todavía es temprano."


translate spanish day7_sl_1f18c165:


    sl "Espero que la cantina esté abierta aun. Estoy taaaaan hambrienta."


translate spanish day7_sl_54aee577:


    me "Yo también."


translate spanish day7_sl_e0880e8b:


    "Recogimos nuestras cosas y caminamos hacia el campamento, agarrándonos de las manos."


translate spanish day7_sl_b9e72e01:


    "La cantina efectivamente estaba abierta y, por raro que parezca, éramos sus primeros visitantes."


translate spanish day7_sl_79df6cca:


    "No era una sorpresa que el desayuno no fuera nada lujoso: unas gachas, dos huevos, pan seco y té aguado."


translate spanish day7_sl_b27dc208:


    me "Sabes, nunca me gustó la comida local."


translate spanish day7_sl_5e13855b:


    sl "A mí tampoco, a decir verdad."


translate spanish day7_sl_5e6d7806:


    "Slavya se rio."


translate spanish day7_sl_e83d08e1:


    me "Pero no hay otra opción...{w} Y es bastante bueno para un estómago vacío."


translate spanish day7_sl_7b2eabb4:


    sl "¡Sí, eso es cierto!"


translate spanish day7_sl_f9be2c84:


    "A diferencia de mí, ella comía con cuidado."


translate spanish day7_sl_37662204:


    "Me gotearon las gachas en mi camisa y se derramó el té por encima mío como de costumbre."


translate spanish day7_sl_bfeafc61:


    sl "¿Siempre comes así?"


translate spanish day7_sl_72cb68c0:


    me "¿Algún problema con eso?"


translate spanish day7_sl_3cc85031:


    "Pregunté con la boca llena."


translate spanish day7_sl_e79d08b4:


    "Ella simplemente se rio."


translate spanish day7_sl_df14ec0d:


    me "Oh... No, no siempre. Pero ocurre cuando estoy hambriento de veras.{w} Desde luego, puedo ser más civilizado."


translate spanish day7_sl_50f431fa:


    th "Bueno, puedo intentarlo por lo menos."


translate spanish day7_sl_1108cd46:


    sl "No te preocupes. No es nada."


translate spanish day7_sl_fc08829f:


    "Bien pronto terminamos de desayunar, y nos fuimos hacia la salida."


translate spanish day7_sl_68728339:


    "Apenas me había dado cuenta todavía de que éramos las únicas personas en la cantina, hasta que Olga Dmitrievna apareció en el umbral de la entrada."


translate spanish day7_sl_33a6f7b8:


    mt "Oh, sois vosotros...{w} Bueno, no voy a preguntaros dónde pasasteis la noche...{w} Así que es eso..."


translate spanish day7_sl_08429d75:


    "La líder del campamento estaba obviamente incómoda."


translate spanish day7_sl_e173004d:


    sl "¡Buenos días a ti también, Olga Dmitrievna!"


translate spanish day7_sl_e1614169:


    "Dijo Slavya con una radiante sonrisa."


translate spanish day7_sl_96bc1657:


    mt "Sí, buenas... buenas...{w} Vale, simplemente no olvidéis empacar vuestras cosas."


translate spanish day7_sl_16698d4c:


    sl "¡No lo haremos!"


translate spanish day7_sl_996ab8e2:


    me "¿A qué se refiere ella? ¿Otra excursión otra vez?"


translate spanish day7_sl_02f1b760:


    "Le pregunté a Slavya cuando salimos fuera de la cantina."


translate spanish day7_sl_e5708664:


    sl "¡No!{w} Hoy es el último día. ¿Lo has olvidado?"


translate spanish day7_sl_09586d84:


    me "¿El último día de qué?"


translate spanish day7_sl_86874628:


    sl "¡El último día de la temporada!"


translate spanish day7_sl_5a084819:


    "Decir que sus palabras me dejaron mudo era un eufemismo."


translate spanish day7_sl_8f86373c:


    me "¿Cómo? ¿Por qué?"


translate spanish day7_sl_88194eef:


    sl "¿Por qué estás tan sorprendido? ¿No lo sabías? Fue anunciado durante la formación."


translate spanish day7_sl_2d769d89:


    "Pero incluso en esas formaciones a las cuales atendí, simplemente me quedaba durmiendo de pie."


translate spanish day7_sl_5cb822ee:


    me "No..."


translate spanish day7_sl_54f37a06:


    sl "¡Ahora ya lo sabes!"


translate spanish day7_sl_cfb1c9d5:


    "Slavya sonrió con naturalidad."


translate spanish day7_sl_536cdcc1:


    me "¿Y qué debería hacer...?"


translate spanish day7_sl_70f1dc1a:


    "Sus palabras me dieron un vuelco en el corazón."


translate spanish day7_sl_22659260:


    th "Por un lado, por fin me escaparía de este maldito campamento. Puede que obtenga algunas respuestas."


translate spanish day7_sl_a8307773:


    th "Por otra parte... Acabo de encontrar algo verdaderamente importante para mí. ¿Y ahora tengo que dejarlo escapar?"


translate spanish day7_sl_63f53767:


    sl "¿Qué quieres decir?"


translate spanish day7_sl_869602ed:


    me "Oh, nada...{w} ¿Y cuándo es la partida?"


translate spanish day7_sl_4e45ffe9:


    sl "Por la tarde, a las cinco o las seis en punto."


translate spanish day7_sl_431ec0f7:


    me "Bueno, ¡todavía queda mucho tiempo!"


translate spanish day7_sl_818ed3e4:


    sl "Sí, pero tengo algunas cosas que hacer.{w} Y debería encontrar a Zhenya, puede que se esté preocupando, ya que estuve ausente toda la noche."


translate spanish day7_sl_c62f6955:


    th "Oh sí, vaya que sí, seguro que se preocupa..."


translate spanish day7_sl_142536ec:


    sl "¡Nos vemos para comer!"


translate spanish day7_sl_412b31af:


    "Slavya me dio un beso en una mejilla y se fue corriendo."


translate spanish day7_sl_9f3c503e:


    "Me quedé permaneciendo en la entrada de la cantina."


translate spanish day7_sl_cdfd20d4:


    "No tenía absolutamente nada que hacer, no hay nada que empacar, así que la única cosa que hacer era vagar por los alrededores del campamento hasta la tarde y pensar, pensar y pensar..."


translate spanish day7_sl_23106771:


    th "Daría cualquier cosa por ser capaz de vaciar mi cabeza durante estas diez horas."


translate spanish day7_sl_630dd74a:


    "Una voz me sacó de mi ensimismamiento."


translate spanish day7_sl_1acf0704:


    el "¡Hola!"


translate spanish day7_sl_e7fb612f:


    "Elektronik estaba frente a mí, tan feliz como de costumbre."


translate spanish day7_sl_b5cc7e1c:


    me "Tú también..."


translate spanish day7_sl_d60a1514:


    "Contesté ausente."


translate spanish day7_sl_206f44a2:


    el "Sabes, ¡todo el campamento está hablando acerca de ti!"


translate spanish day7_sl_d57bdfe4:


    me "Supongo que sé por qué..."


translate spanish day7_sl_1c122ee3:


    el "Entonces tú... ¿lo hiciste ya?"


translate spanish day7_sl_c8bd0473:


    "Preguntó con una risita."


translate spanish day7_sl_a19056f4:


    me "¿Crees que te concierne?"


translate spanish day7_sl_e84597be:


    el "Bueno, no. Yo sólo..."


translate spanish day7_sl_ad94eafa:


    me "¡Entonces cállate!"


translate spanish day7_sl_5c46a06d:


    "Dejándolo solo con sus especulaciones, caminé hasta la plaza."


translate spanish day7_sl_dc17db89:


    th "¿Por qué se comporta de forma que hace volverse loco a todos?{w} Aunque, probablemente sea sólo yo quien se vuelve loco..."


translate spanish day7_sl_7036dfea:


    "Estaba completamente inmerso en mis pensamientos, sin mirar alrededor, y no me percaté de Miku hasta que chocó contra mí."


translate spanish day7_sl_d8625dc6:


    mi "¡Oh, perdona! ¡Debería ser más cuidadosa! Estaba ensimismada y no me di cuenta de ti. Ya sabes cómo sucede... ¡Oh, y saludos!"


translate spanish day7_sl_38926eb8:


    "Miré por encima de su hombro y proseguí caminando."


translate spanish day7_sl_425d7917:


    mi "Oh, Semyon, ¡espera un segundo! ¿Qué pasa con Slavya? ¡Cuéntame, es muy interesante! Parece que todo el mundo en el campamento lo sabe excepto yo. No, no es que sea curiosa, pero ya que todo es tan serio..."


translate spanish day7_sl_933d1b5a:


    "Que diga que no es curiosa es como negar una verdad universal."


translate spanish day7_sl_f6740215:


    me "Nada en especial."


translate spanish day7_sl_1e997695:


    "Contesté sin darme la vuelta."


translate spanish day7_sl_dc2db981:


    mi "Bueno, si no quieres decirlo..."


translate spanish day7_sl_52f2af91:


    "Traté de aislarme a mí mismo de todas las cosas irritantes externas, así que ignoré las otras palabras de Miku."


translate spanish day7_sl_2d7ae10b:


    "La zona de la plaza fue reemplazada bien pronto por la playa."


translate spanish day7_sl_5e0ecadd:


    "Afortunadamente, la juvenil gema de la escena pop soviética no andaba cerca."


translate spanish day7_sl_0f673e81:


    "Tomé lugar en la arena y comencé a mirar el río."


translate spanish day7_sl_98d846ec:


    th "¿Por qué diantres todo el campamento está tan interesado en mi relación con Slavya?"


translate spanish day7_sl_bb84fa35:


    th "En mi mundo, nadie habría dicho una sola palabra.{w} Por lo menos no tan alto."


translate spanish day7_sl_c310401c:


    th "Al menos no escuchas las cosas que dicen detrás de tu espalda."


translate spanish day7_sl_0d2a8813:


    "Por ahora estaba preparado para soportar los rumores, mientras no tuviera que estar informando a todos sobre mi vida privada."


translate spanish day7_sl_5db83ec4:


    "El sol se alzaba cada vez más y más alto, y me estaba entrando sueño."


translate spanish day7_sl_948fae16:


    "Mis ojos estaban ya cerrados cuando, de repente, escuché a alguien sentarse a mi lado."


translate spanish day7_sl_08478924:


    "Era Alisa."


translate spanish day7_sl_6a87787c:


    dv "¡Felicidades!"


translate spanish day7_sl_75ba0772:


    me "¿Cómo?"


translate spanish day7_sl_caa43468:


    dv "¿No lo sabes?"


translate spanish day7_sl_7a835092:


    th "Claro, no hace falta ser Einstein para deducirlo..."


translate spanish day7_sl_321eb6b8:


    me "Para ser sinceros, estoy harto y cansado de esto...{w} Todo el mundo tiene que estar mencionándolo.{w} ¡Basta ya!"


translate spanish day7_sl_e5a5925c:


    dv "¿Y qué te esperabas?{w} Está fuera de lo normal para el campamento. ¡Incluso diría que es excepcional!"


translate spanish day7_sl_ec2c2a7c:


    "Ella se rio."


translate spanish day7_sl_513d88a1:


    dv "¡Y mostrarse ante la líder del campamento de esa forma! Incluso Ulyana no podría lograrlo.{w} Yo tampoco, probablemente..."


translate spanish day7_sl_687ba52e:


    "No sabía cómo responder ante eso... tanto si era un cumplido como si era una burla."


translate spanish day7_sl_9eb3d677:


    me "Sabes, estás exagerando las cosas más de la cuenta.{w} Para mí no había nada de extraordinario."


translate spanish day7_sl_b241d169:


    me "Además, no comprendo el porqué tienes que meter tus narices dónde no te llaman en mi vida.{w} ¿No pensaste que solamente es asunto mío y de nadie más?"


translate spanish day7_sl_e9cbda60:


    dv "Quizá sea así... Tal vez sea así..."


translate spanish day7_sl_576ab57f:


    "Dijo Alisa misteriosamente."


translate spanish day7_sl_fc97acab:


    "Nos quedamos sentados en silencio por un rato."


translate spanish day7_sl_749ec097:


    "Quería que se fuera cuanto antes, pero Alisa estaba observando los pioneros en la playa, y no parecía tener intenciones de irse a ninguna parte."


translate spanish day7_sl_0ee3b46c:


    dv "Y bien, ¿qué vas hacer ahora?"


translate spanish day7_sl_cb23542f:


    me "¿De qué hablas?"


translate spanish day7_sl_518e3571:


    dv "Hoy es el último día."


translate spanish day7_sl_7321b637:


    me "Claro, ¿y qué?"


translate spanish day7_sl_9772f413:


    dv "Todo el mundo se irá."


translate spanish day7_sl_23104825:


    me "¿Y?"


translate spanish day7_sl_8503bc50:


    dv "Y Slavya también se irá."


translate spanish day7_sl_38f8adbc:


    me "Y tú también te irás. ¿Y?"


translate spanish day7_sl_26679ce6:


    dv "Os separareis."


translate spanish day7_sl_6474973e:


    th "Eso ya lo sé. Gracias."


translate spanish day7_sl_467d3cb1:


    me "Obviamente."


translate spanish day7_sl_3030a38d:


    dv "¿Y qué piensas al respecto?"


translate spanish day7_sl_46d67e60:


    me "¿Qué debería pensar? No puedo permanecer aquí para siempre."


translate spanish day7_sl_cbf330c2:


    dv "Por supuesto que no.{w} Pero qué, ¿la vas a dejar que se vaya así sin más?"


translate spanish day7_sl_1ecab4b2:


    me "No...{w} Quiero decir, ¿qué puedo hacer?"


translate spanish day7_sl_fd11f974:


    "Para ser francos, realmente no tenía ni idea de qué hacer.{w} Mi cabeza estaba hecha un completo lío."


translate spanish day7_sl_2aefe5ec:


    th "Seré capaz de poderme ir de aquí. Ésa era la cosa más importante para mí durante la última semana. Y ahora tengo que responder a una cuestión mucho más complicada."


translate spanish day7_sl_4892f4fc:


    th "¿Cómo terminaban «los amores de verano» para los adolescentes soviéticos normales (y mi amor era claramente uno «de verano»)?{w} Simplemente se iban a sus hogares y ya está..."


translate spanish day7_sl_78491662:


    th "Podían vivir a miles de kilómetros de distancia los unos de los otros.{w} En mi caso, en diferentes mundos."


translate spanish day7_sl_a5e6cfbd:


    th "No parece que uno pueda inventarse una solución decente para dicho problema en un par de horas."


translate spanish day7_sl_31914dbe:


    dv "¿Cómo voy a saberlo? ¡Es cosa tuya decidirlo!"


translate spanish day7_sl_2da8076e:


    "Alisa hizo un sonrisita, se puso en pie y corrió hasta el río."


translate spanish day7_sl_2b4a56da:


    "Faltaba sólo media hora para la hora de comer, así que lentamente caminé hasta la cantina."


translate spanish day7_sl_c60c50bf:


    "Los pioneros no habían logrado ocupar todas las mesas libres, así que pude escoger un lugar bastante bueno pero, a mi pesar, no mi favorito en el rincón."


translate spanish day7_sl_03bda289:


    "Estaba a punto de empezar a comer cuando alguien, bruscamente, movió una silla de la mesa cerca de mí y tomó asiento."


translate spanish day7_sl_d7e5ecca:


    us "¡Hola que tal! ¿Cómo estás?"


translate spanish day7_sl_556f4ec1:


    "Era Ulyana.{w} Una extraordinaria versión educada de ella."


translate spanish day7_sl_892a8819:


    me "Estaba bien hasta ahora."


translate spanish day7_sl_27c62366:


    "Le hice una mueca desdeñándola."


translate spanish day7_sl_11569ff2:


    us "¡Oh, venga ya! Mejor que me lo cuentes, ¿qué vas hacer?"


translate spanish day7_sl_59c7266e:


    me "Comer."


translate spanish day7_sl_9da1463d:


    "Suspiré."


translate spanish day7_sl_10e5fe18:


    us "No estoy hablando de eso.{w} Al final de la temporada, tú y Slavya partiréis, ¿qué harás?"


translate spanish day7_sl_5c9ad26b:


    th "Es como si mi futuro fuera un problema prioritario para todos los lugareños."


translate spanish day7_sl_af94a60d:


    me "Ey, ¿en serio qué pasa?"


translate spanish day7_sl_f13441a0:


    us "¡Sólo curiosidad!"


translate spanish day7_sl_15ec1554:


    me "¿Curiosidad acerca de qué?{w} ¡Yo no te pregunto qué vas hacer mañana, mañana pasado o en un mes! ¿Por qué te tienes que entrometer en la vida de otro?"


translate spanish day7_sl_5b024fb2:


    us "No me estoy entrometiendo en nada."


translate spanish day7_sl_c6e7d0a8:


    "Dijo Ulyana, molesta."


translate spanish day7_sl_85f844bf:


    us "No soy nada buena para todas esas cosas..."


translate spanish day7_sl_7b60b058:


    me "Bastante cierto, todavía eres una cría."


translate spanish day7_sl_81615f37:


    us "¡Pues entonces tienes que explicarte, puesto que tú eres el adulto aquí!"


translate spanish day7_sl_7c3e569d:


    me "¿Qué debería explicar?"


translate spanish day7_sl_71f29a50:


    us "Pues estaréis en contacto y os escribiréis por cartas, ¿cierto? ¿Y luego volveréis a encontraros en este campamento para el siguiente verano?"


translate spanish day7_sl_e2283e70:


    th "Comunicarse...{w} con cartas..."


translate spanish day7_sl_33774013:


    "Me resulta tan exagerado."


translate spanish day7_sl_7e748919:


    "Y no lo digo por la idea de las hojas y los sobres de papel. Me refiero a la posibilidad de no ver a Slavya durante un año, o probablemente incluso más.{w} O para siempre..."


translate spanish day7_sl_946147fa:


    us "¿Entonces qué?"


translate spanish day7_sl_e67f8463:


    sl "¿Puedo unirme a vosotros?"


translate spanish day7_sl_cf0d6604:


    "Alcé mi vista y vi a Slavya de pie cerca de mi con una bandeja de comida."


translate spanish day7_sl_5f86c688:


    us "Vale, es hora de irse."


translate spanish day7_sl_b80fdba5:


    "Ulyana saltó fuera y corrió hasta el otro extremo de la cantina."


translate spanish day7_sl_4d6b2adc:


    sl "¿De qué hablabais?"


translate spanish day7_sl_1ec6c417:


    me "Ya la conoces. ¿De qué podríamos estar hablando si no?"


translate spanish day7_sl_c17a722d:


    "Slavya sonrió."


translate spanish day7_sl_750701bd:


    me "¿Terminaste con tus asuntos?"


translate spanish day7_sl_0dad5262:


    sl "¡Sí! Ahora estoy completamente libre. Puedo ayudarte a empacar tus cosas."


translate spanish day7_sl_7c158bc0:


    me "No es que tenga mucho que empacar..."


translate spanish day7_sl_4bb84a95:


    sl "Bueno, pues entonces me puedes ayudar con las mías."


translate spanish day7_sl_71d3b8c8:


    me "Claro."


translate spanish day7_sl_852ac74c:


    "Esa era la oportunidad perfecta para una relajada conversa con ella.{w} Aunque no tenía ni la más mínima idea de qué debería decirle."


translate spanish day7_sl_0494524b:


    "Bien pronto acabamos con nuestra comida y nos dirigimos a la cabaña de Slavya."


translate spanish day7_sl_33094fdb:


    "Todo en ella estaba limpio como una patena, no como la cabaña de Olga Dmitrievna."


translate spanish day7_sl_31d68c71:


    sl "Oh, no sé por dónde debería empezar..."


translate spanish day7_sl_917a3612:


    "Sacó su mochila de viaje y empezó a rebuscar por un armario."


translate spanish day7_sl_1b0f9bba:


    "Mientras tanto, me senté en la cama y empecé a reordenar mis ideas."


translate spanish day7_sl_1cdd2e37:


    me "¿Tienes planes para después del campamento? ¿Qué harás?"


translate spanish day7_sl_af11ccdd:


    sl "Bueno, sabes..."


translate spanish day7_sl_943eaf3a:


    "Alzó su mirada hacia mí y me hizo una sonrisa."


translate spanish day7_sl_01aec217:


    sl "Volveré al instituto."


translate spanish day7_sl_2c057d1c:


    me "Ah... Sí..."


translate spanish day7_sl_5c7b7c48:


    sl "¿Y tú?"


translate spanish day7_sl_79f50df9:


    th "¿Y yo qué?{w} No voy a considerar en serio el ir otra vez de nuevo al instituto por segunda vez..."


translate spanish day7_sl_2260325d:


    me "¿Yo? Bueno, yo también, supongo."


translate spanish day7_sl_31433173:


    me "Aunque... No creo realmente que vuelva a mi ciudad."


translate spanish day7_sl_52dd0423:


    th "Hablando en serio, incluso si creyera que volviera, no tenía a ninguna parte adonde ir."


translate spanish day7_sl_70f37a53:


    sl "¿Por qué?"


translate spanish day7_sl_04bffd62:


    me "Simplemente porque... Porque no hay nada que hacer allí. Nada en absoluto. Y no hay nadie esperándome allí."


translate spanish day7_sl_a6afd81b:


    sl "Pero, ¿y tus padres?"


translate spanish day7_sl_dd0b7f61:


    me "Mis padres...{w} Bueno, verás... no están allí ahora."


translate spanish day7_sl_7a41296e:


    sl "¿Pero entonces dónde están?"


translate spanish day7_sl_848f65f9:


    me "Ellos están... trabajando en el extranjero por decirlo así."


translate spanish day7_sl_0857dec3:


    sl "¡Ah, así que eres un tipo afortunado!"


translate spanish day7_sl_d2b56c39:


    "Sonrió alegremente."


translate spanish day7_sl_5ac38cf1:


    me "¿Por qué?"


translate spanish day7_sl_347ac558:


    sl "Te enviarán cosas del extranjero y tal."


translate spanish day7_sl_36322c82:


    th "No diría que mi situación fuera afortunada."


translate spanish day7_sl_ce4797d9:


    me "Bueno, de todas formas..."


translate spanish day7_sl_6a5f7148:


    sl "¡No te molestes!"


translate spanish day7_sl_aa9df60c:


    th "Realmente parece no comprender lo que digo."


translate spanish day7_sl_496c0518:


    me "No me he molestado."


translate spanish day7_sl_61615b2e:


    "Y era verdad."


translate spanish day7_sl_926dd5d8:


    sl "¿Pues qué pasa?"


translate spanish day7_sl_56cbe60a:


    me "Muchas cosas..."


translate spanish day7_sl_b8748488:


    sl "¿Por ejemplo?"


translate spanish day7_sl_eecf1d19:


    me "Bueno, por ejemplo..."


translate spanish day7_sl_95862924:


    "Estaba a punto de estallar dando explicaciones sobre mi situación y mi deseo de no tener que separarme de Slavya, cuando la puerta se abrió de golpe con un portazo y Zhenya entró."


translate spanish day7_sl_d59f2277:


    mz "Ah, estáis aquí."


translate spanish day7_sl_c58c9392:


    sl "Sí, estamos empacando el equipaje."


translate spanish day7_sl_dcbd9d98:


    mz "Ya veo... Fue buena idea no llegar diez minutos tarde u os habría interrumpido en medio de vuestro... «equipaje»."


translate spanish day7_sl_afb6e684:


    "Slavya se ruborizó y siguió guardando las cosas dentro de la mochila nerviosamente."


translate spanish day7_sl_67bf96a7:


    mz "¿Qué tal, amante heroico?"


translate spanish day7_sl_146cea46:


    "Esa manera de dirigirse a mí me dio asco."


translate spanish day7_sl_ad02f53a:


    me "¿Qué?"


translate spanish day7_sl_95bc0f12:


    mz "Nada..."


translate spanish day7_sl_c7bb77e9:


    "Se me quedó mirando atentamente por un rato."


translate spanish day7_sl_f9da3a8f:


    mz "¡Vigila! Ahora alguien más que tú depende de tus elecciones."


translate spanish day7_sl_60a03249:


    "No respondí, con esas palabras Zhenya se marchó de la cabaña."


translate spanish day7_sl_404c3ae1:


    sl "¿Qué quiso decir?"


translate spanish day7_sl_9ec962ea:


    me "Creo que no lo sabrás nunca con seguridad."


translate spanish day7_sl_f277b278:


    sl "Aun así..."


translate spanish day7_sl_2922f709:


    me "Bueno... Quería hablarte sobre eso yo mismo."


translate spanish day7_sl_b7c51fee:


    th "Estaba a punto de empezar, pero entonces..."


translate spanish day7_sl_d00ac2ba:


    sl "¿Sobre qué?"


translate spanish day7_sl_b40211ef:


    "Slavya me miró detenidamente."


translate spanish day7_sl_4ee92ca9:


    me "Es sólo que... hoy partimos, ¿cierto?"


translate spanish day7_sl_5ede84c0:


    sl "Correcto."


translate spanish day7_sl_cb5149bf:


    me "Y volveremos a nuestras ciudades."


translate spanish day7_sl_be32f309:


    sl "Bueno, claro."


translate spanish day7_sl_774c66dd:


    me "¿Y cuándo volveré a verte?"


translate spanish day7_sl_bbc1f2e4:


    "Slavya se quedó pensativa."


translate spanish day7_sl_8a3c9e52:


    sl "No lo sé..."


translate spanish day7_sl_05cb701f:


    me "Y yo tampoco lo sé."


translate spanish day7_sl_30ac20c5:


    sl "¡Pero podrías escribirme cartas!"


translate spanish day7_sl_6f3d442d:


    "Ella sonrió, pero su sonrisa no me tranquilizó."


translate spanish day7_sl_f8d768e0:


    me "Sí, desde luego, pero sabes, eso no es lo mismo."


translate spanish day7_sl_926dd5d8_1:


    sl "¿Entonces qué es?"


translate spanish day7_sl_d9c6a876:


    me "¿Puedo ir contigo?"


translate spanish day7_sl_a2800813:


    "Decidí arriesgarme y le hablé directamente."


translate spanish day7_sl_aa131597:


    sl "¿Y qué harás allí?"


translate spanish day7_sl_f4e53387:


    "Slavya no parecía estar sorprendida, sino un poco irritada."


translate spanish day7_sl_2f7570dd:


    sl "Los estudios, mis padres...{w} ¿Y qué dirá la gente?"


translate spanish day7_sl_7768528c:


    "No estaba pensando en las opiniones de las otras personas.{w} Y para ser sinceros, no me preocupaban mucho."


translate spanish day7_sl_6e0d3350:


    me "Bueno, ¡siempre podemos hallar una manera!"


translate spanish day7_sl_c7e0fd92:


    sl "¿Qué manera?"


translate spanish day7_sl_baa95abd:


    me "¡Pensaremos en algo!"


translate spanish day7_sl_5d12fc48:


    sl "Semyon, somos demasiado jóvenes para esto..."


translate spanish day7_sl_0acece57:


    th "De hecho, no lo soy."


translate spanish day7_sl_2325fa76:


    me "¿Y qué?"


translate spanish day7_sl_655480b8:


    sl "Tal vez tras un tiempo..."


translate spanish day7_sl_870d8d45:


    me "¿Cuánto tiempo necesitas? ¿Un año? ¿Dos años? ¿Cinco?"


translate spanish day7_sl_742d5557:


    sl "Lo dices como si yo lo...{w} No lo sé..."


translate spanish day7_sl_dafbd76d:


    "Parecía confundida."


translate spanish day7_sl_c39dd65b:


    me "¿Así que todo lo que sucedió aquí fue solamente un amor de verano?"


translate spanish day7_sl_262d7cae:


    "Empezaba a enojarme."


translate spanish day7_sl_2732ce31:


    sl "¡No! ¡Por supuesto que no! Sólo..."


translate spanish day7_sl_ad02f53a_1:


    me "¿Qué?"


translate spanish day7_sl_d9d64c2f:


    sl "No estoy preparada para discutirlo ahora mismo. Hablémoslo más tarde."


translate spanish day7_sl_ffe0dbb0:


    me "¿Cuánto más tarde? Únicamente nos quedan un par de horas hasta la partida."


translate spanish day7_sl_86719bd3:


    "Slavya no dijo nada y continuó empacando sus cosas."


translate spanish day7_sl_e6f46fe1:


    "Me habría esperado casi cualquier tipo de reacción de ella, salvo ésta."


translate spanish day7_sl_31110d51:


    "De hecho, ella sencillamente me apartó."


translate spanish day7_sl_c4036085:


    th "Resulta que ella no necesita continuar nuestra relación.{w} Por lo que no soy tan importante para ella."


translate spanish day7_sl_bad_adf2ccc7:


    me "Mira, todavía no lo comprendo."


translate spanish day7_sl_bad_90097fb1:


    sl "¿El qué?"


translate spanish day7_sl_bad_2034de62:


    "Preguntó Slavya sin darse la vuelta."


translate spanish day7_sl_bad_dbb5b4ae:


    me "El porqué no quieres..."


translate spanish day7_sl_bad_3c8f9ea1:


    sl "¿Y cómo te creías que iba a ser?"


translate spanish day7_sl_bad_2cddef76:


    me "Bueno, puedo encontrar un trabajo, vivir en un piso alquilado."


translate spanish day7_sl_bad_e867c7b6:


    sl "¿Y qué puedes hacer? ¿Tendrás por lo menos un certificado de estudios finalizados?"


translate spanish day7_sl_bad_c6d4d90d:


    th "De hecho sí. Está por alguna parte del armario..."


translate spanish day7_sl_bad_ae7c378c:


    me "¡Eso no es lo que cuenta aquí!"


translate spanish day7_sl_bad_af407039:


    sl "¿Por qué no?"


translate spanish day7_sl_bad_74f68a72:


    me "¡Si tienes un objetivo puedes superar cualquier obstáculo!{w} Además, no es tan grave."


translate spanish day7_sl_bad_a9c0b671:


    sl "Eso es lo que tú te crees."


translate spanish day7_sl_bad_54740717:


    me "Quizá tengas razón. Pero de todas formas, ¡no puedo entender tu reacción!{w} A fin de cuentas, si sólo nos vamos, eso es todo, ése es el fin..."


translate spanish day7_sl_bad_0f3b42d7:


    sl "No dije eso."


translate spanish day7_sl_bad_590152a9:


    me "Pero es obvio."


translate spanish day7_sl_bad_fb896de0:


    sl "¡¿De dónde sacaste esa idea?!"


translate spanish day7_sl_bad_544fc2bc:


    "Gritó."


translate spanish day7_sl_bad_ca84e284:


    "Tal vez era la primera vez que había visto a Slavya así."


translate spanish day7_sl_bad_0d2c9366:


    me "Bueno, déjame decirlo de forma diferente. Sencillamente no quiero volver al lugar donde vivo...{w} y tampoco comunicarme contigo sólo con cartas o teléfono, deseando un encuentro."


translate spanish day7_sl_bad_dc38e5e1:


    sl "También será difícil para mí."


translate spanish day7_sl_bad_7512585c:


    me "Bueno, ¿pues cuál es el problema?"


translate spanish day7_sl_bad_380f8d60:


    sl "Sabes, no todo depende de nosotros..."


translate spanish day7_sl_bad_cfeb918e:


    me "¡¿Pues de quién depende si no?!"


translate spanish day7_sl_bad_30abaa79:


    th "Aunque nadie sabría mejor que yo cuánta razón tenía ella. Si todo dependiera de mí, nunca me habría encontrado aquí."


translate spanish day7_sl_bad_87a8fd14:


    sl "Después de todo, hay normas sociales, leyes, moral y ética."


translate spanish day7_sl_bad_d80f315d:


    me "¿Y qué tiene que ver todo eso? ¡¿Acaso importan?!"


translate spanish day7_sl_bad_cdfc45ff:


    sl "Si no son importantes para ti, lo son para mí."


translate spanish day7_sl_bad_2a579a0f:


    me "Vale.{w} Estoy dispuesto incluso a comportarme de acuerdo a tus estándares, pero hay algo aun más importante."


translate spanish day7_sl_bad_2952e1d9:


    sl "Quizá... Pero no puedo tan repentinamente..."


translate spanish day7_sl_bad_3d0a9e16:


    me "No tienes mucho tiempo más para pensarlo."


translate spanish day7_sl_bad_e18fa2db:


    "Me paré por un instante, para reordenar mis ideas."


translate spanish day7_sl_bad_a86972c4:


    me "Sólo puedo ofrecerme a mí mismo...{w} Ya que es la única cosa que tengo."


translate spanish day7_sl_bad_fbcc5341:


    me "Además, no sé qué puede ocurrirme dentro de una hora, pero estoy seguro de que, pase lo que pase, si tengo que empezar de cero lo quiero hacer contigo."


translate spanish day7_sl_bad_7c100b18:


    sl "Hablas de manera preciosa, ¡pero imagínate cómo me siento!{w} Para ti todo es muy simple, pero tengo mi propia vida a la que estoy acostumbrada, y cambiarlo todo así sin más..."


translate spanish day7_sl_bad_34cae222:


    me "Yo tenía una antes también."


translate spanish day7_sl_bad_adf03656:


    "Dije en voz baja."


translate spanish day7_sl_bad_e07c37b3:


    sl "¿Qué?"


translate spanish day7_sl_bad_3a389e87:


    me "Digo que es inevitable.{w} Los cambios son parte de la vida, no puedes vivir sin afrontarlos."


translate spanish day7_sl_bad_235fca6e:


    "Slavya me miró pensativamente, se quedó quieta por un rato sosteniendo su mochila en sus manos, luego se acercó hacia mí, se sentó al lado y apoyó su cabeza en mi hombro."


translate spanish day7_sl_bad_91e26f2b:


    sl "Vale, te creo. ¡Si lo dices tú, pues que sea así!"


translate spanish day7_sl_bad_ae141d61:


    "En aquel momento, sentí una felicidad perfecta."


translate spanish day7_sl_bad_e2ff5a09:


    th "No importa que hace una semana atrás estuviera en un mundo completamente diferente, viviendo otra vida.{w} Todo aquello se fue y ahora me parecía como algo ajeno y de locos."


translate spanish day7_sl_bad_f903c358:


    "Aun no sé cómo podría considerar eso como normal."


translate spanish day7_sl_bad_eb2a107a:


    th "Ahora estoy menos preocupado por conocer el cómo y el porqué ocurrió todo."


translate spanish day7_sl_bad_3548661d:


    th "Ahora no busco respuestas.{w} ¿Para qué lo haría, cuando están todas justo delante de mí?"


translate spanish day7_sl_bad_5a380cf2:


    th "Ahora tengo un largo camino por delante, mano a mano con la persona que amo.{w} Y mirar atrás no tiene sentido."


translate spanish day7_sl_bad_5734f7c1:


    "Podría quedarme sentado por siempre, sin darme cuenta del tiempo, pero Slavya al fin miró el reloj y con delicadeza me susurró a mi oído:"


translate spanish day7_sl_bad_b0527f28:


    sl "Es la hora."


translate spanish day7_sl_bad_2df15734:


    me "Sí, exacto.{w} Ve hacia la parada de autobús, mientras yo me daré prisa hasta la cabaña de la líder del campamento para coger mis cosas."


translate spanish day7_sl_bad_e0c8585e:


    sl "¡Eres muy desorganizado! Deberías haberlas recogido por adelantado."


translate spanish day7_sl_bad_5e6d7806:


    "Se rio Slavya."


translate spanish day7_sl_bad_34d1b774:


    me "Tampoco tengo mucho que empacar de todas maneras..."


translate spanish day7_sl_bad_a4b3d627:


    "Salí de la cabaña y corrí en dirección hacia la cabaña de Olga Dmitrievna."


translate spanish day7_sl_bad_2be7619b:


    "La líder del campamento se quedó quieta en la entrada de la puerta, aparentemente esperándome."


translate spanish day7_sl_bad_6cda25dd:


    mt "Pensé que no vendrías..."


translate spanish day7_sl_bad_1a154f74:


    "No respondí, me metí dentro de un brinco, empaqué mi ropa de invierno en una mochila y salí fuera."


translate spanish day7_sl_bad_d5902b51:


    mt "Venga vamos, que ya es la hora."


translate spanish day7_sl_bad_db5dcd8e:


    "En un minuto estábamos en la parada del autobús, junto al resto de pioneros."


translate spanish day7_sl_bad_709d3e7d:


    mt "¿Todo el mundo está aquí?"


translate spanish day7_sl_bad_14167bf5:


    "Comenzó a decir Olga Dmitrievna."


translate spanish day7_sl_bad_044ab0d8:


    mt "Hoy os vais de nuestro campamento, y me gustaría deciros algo antes de partir."


translate spanish day7_sl_bad_00123e37:


    "Estaba visiblemente nerviosa y, desesperadamente, se quedó sin palabras."


translate spanish day7_sl_bad_019ed77a:


    mt "Espero que recordéis el tiempo que transcurristeis aquí durante vuestra vida, y que retengáis solamente recuerdos agradables sobre Sovyonok."


translate spanish day7_sl_bad_b48fe53b:


    mt "También espero que os hayáis convertido un poquito en mejores personas, que hayáis logrado aprender algo y conocido nuevos amigos...{w} Tan solamente... volved el próximo año."


translate spanish day7_sl_bad_42256d78:


    "La líder del campamento apartó su mirada.{w} Era como si estuviera tratando de aguantarse las lágrimas para sí misma."


translate spanish day7_sl_bad_6a707c88:


    "No me esperaba que ella se pusiera tan sentimental, pero estaba completamente de acuerdo con todo lo que dijo."


translate spanish day7_sl_bad_45cf8a7c:


    "Quizás fue la primera vez que sus palabras no me entraban por un oído y me salían por el otro."


translate spanish day7_sl_bad_e64058b6:


    "Creo que echaré de menos este campamento.{w} E incluso a Olga Dmitrievna."


translate spanish day7_sl_bad_53f18694:


    th "Aunque, si este es el comienzo de una nueva vida, ¿entonces por qué no volver aquí una vez más?"


translate spanish day7_sl_bad_6eb2ae8d:


    "Habíamos dejado bien lejos atrás Sovyonok.{w} La semana había transcurrido como en un instante, y llegó a un final, pero eso era el principio de algo nuevo. Mi nueva vida en un nuevo mundo.{w} Junto a Slavya."


translate spanish day7_sl_bad_d27ec580:


    "La luna estaba brillando, haciendo que todo estuviera iluminado como durante el día. Más allá de las ventanillas del autobús se veían atravesar los campos sin fin, en ocasiones cubiertos de gris y no con ropajes menos alegres, decorados en la distancia por el verde oscuro de los espléndidos bosques."


translate spanish day7_sl_bad_8d851fd8:


    "De todas maneras, los alrededores eran la última de mis preocupaciones ahora... estaba disfrutando el momento."


translate spanish day7_sl_bad_5fe75290:


    "Yo y Slavya estábamos sentados en la última fila, observando a los demás pioneros."


translate spanish day7_sl_bad_9a99d836:


    "Más adelante, Ulyana estaba alborotada. Corría por la cabina del autobús y gritaba.{w} Lena estaba leyendo un libro y Alisa estaba durmiendo."


translate spanish day7_sl_bad_86689381:


    th "Pareciera que este mundo es absolutamente normal."


translate spanish day7_sl_bad_f27eb605:


    th "Y no importa realmente cómo llegué aquí, si todo resulta ser tan maravilloso. Me convertí en una persona diferente, conocí a Slavya."


translate spanish day7_sl_bad_29dcd13b:


    sl "¿En qué piensas?"


translate spanish day7_sl_bad_94622b60:


    me "En la vida."


translate spanish day7_sl_bad_0bb42e8e:


    sl "¿Y qué tal va?"


translate spanish day7_sl_bad_1495be07:


    me "¡Genial!"


translate spanish day7_sl_bad_2fd33210:


    "Se rio suavemente."


translate spanish day7_sl_bad_9bbf9c21:


    me "Sabes..."


translate spanish day7_sl_bad_8b01c678:


    sl "Esperemos un poquito. El autobús me está haciendo dormir. Haré una cabezadita si no te importa."


translate spanish day7_sl_bad_4bfb0b4b:


    me "¡Claro, adelante!"


translate spanish day7_sl_bad_909286be:


    "Ella descansó su cabeza en mi hombro y de pronto se durmió."


translate spanish day7_sl_bad_a20cefa7:


    "..."


translate spanish day7_sl_bad_fec04684:


    "La inminente llegada al distrito central... y un nuevo capítulo en mi vida... no me preocupó para nada."


translate spanish day7_sl_bad_e23b4758:


    "CUando estás tan en lo alto, las pequeñas cosas de abajo no te molestan."


translate spanish day7_sl_bad_d8cf6937:


    "Por lo tanto, caí dormido tan pronto como me sentí cansado, sin miedo de nada."


translate spanish day7_sl_good_3fe69130:


    me "¡Solamente te crees que es complicado para ti!"


translate spanish day7_sl_good_c966fd52:


    sl "Me parece a mí que también es difícil para ti. Pero por algún motivo tú no lo crees así."


translate spanish day7_sl_good_8f6ce43c:


    me "¿Y si te digo que no tengo un lugar al que volver, que desde el pasado lunes no hay otra vida más para mí y que tengo que comenzar de cero?"


translate spanish day7_sl_good_e1b3f8e0:


    sl "No lo sé...{w} No me resulta muy razonable."


translate spanish day7_sl_good_bc56b5b9:


    me "¡Y no debería! Pero es la verdad."


translate spanish day7_sl_good_217f925e:


    sl "Y entonces qué...{w} Por favor, explícate."


translate spanish day7_sl_good_53896a36:


    me "¡Llegué a este campamento por accidente!{w} En realidad, soy de otro tiempo, quizá de otro mundo."


translate spanish day7_sl_good_f88606a6:


    me "De principios del siglo XXI."


translate spanish day7_sl_good_41a98fea:


    me "Y soy mayor de lo que aparento."


translate spanish day7_sl_good_4a22576a:


    me "No sé realmente ni cómo llegué aquí."


translate spanish day7_sl_good_08ff2d6a:


    "Slavya me miró fijamente, pero no dijo nada."


translate spanish day7_sl_good_b62b586c:


    "Los instantes se hicieron eternos."


translate spanish day7_sl_good_05affea5:


    "Esperé que se riera de mí, que me llamara loco o alguna cosa incluso peor."


translate spanish day7_sl_good_f40ae6c7:


    sl "¿Y por qué me cuentas todo esto a mí?"


translate spanish day7_sl_good_31da2fb6:


    "Al final preguntó con voz baja."


translate spanish day7_sl_good_db105195:


    me "Quiero que comprendas mi situación."


translate spanish day7_sl_good_3bb29744:


    sl "Pero eso...{w} Simplemente suena estúpido."


translate spanish day7_sl_good_4c64048c:


    me "¡Por supuesto! Si fuera tú, reaccionaría de la misma manera."


translate spanish day7_sl_good_48a19f73:


    sl "¿Y qué te esperas de mí?"


translate spanish day7_sl_good_53d3fd1d:


    me "No lo sé...{w} Sencillamente sentí que tenía que confesártelo."


translate spanish day7_sl_good_837efc0e:


    sl "No estoy diciendo que me estés mintiendo, y tampoco pienso que estés loco, pero tienes que entender que simplemente no puedo aceptarlo."


translate spanish day7_sl_good_2e320730:


    me "Sí, lo entiendo...{w} Simplementé pensé que te ayudaría a comprender mi situación y mis motivos para mis acciones."


translate spanish day7_sl_good_5673e9a6:


    sl "No lo hizo, para serte honesta."


translate spanish day7_sl_good_0257709a:


    "Suspiré frustrado."


translate spanish day7_sl_good_96e94f68:


    sl "Y mi opinión no ha cambiado."


translate spanish day7_sl_good_2ed72018:


    me "Pero..."


translate spanish day7_sl_good_0848ad63:


    sl "Mejor que te vayas, tienes que empacar tus cosas o llegarás tarde."


translate spanish day7_sl_good_c744b4ed:


    me "Hay..."


translate spanish day7_sl_good_07c08f1b:


    sl "Hablemos más tarde."


translate spanish day7_sl_good_d9f15fcb:


    "No iba a discutir."


translate spanish day7_sl_good_a1fa17bf:


    th "Si Slavya no ha cambiado de opinión tras mi confesión, entonces no tiene sentido seguir insistiendo ahora mismo."


translate spanish day7_sl_good_00aaa21e:


    "Consternado, me arrastré hasta la cabaña de Olga Dmitrievna."


translate spanish day7_sl_good_f08c18e7:


    "La líder del campamento estaba quieta en las escaleras de la entrada, parecía que estaba esperándome."


translate spanish day7_sl_good_959e190e:


    mt "¿Por qué estás tan apesadumbrado? ¿Sucedió algo?"


translate spanish day7_sl_good_2ba695f3:


    me "No, nada. Solamente vine para empacar mis cosas."


translate spanish day7_sl_good_3e5d4e16:


    mt "Oh, venga vamos, suéltalo. Puede que te sientas mejor."


translate spanish day7_sl_good_44d67563:


    me "Bueno...{w} ¿Como crees que puedo demostrar que estoy diciendo la verdad a alguien que no me cree?"


translate spanish day7_sl_good_1bcba649:


    mt "¿Estás completamente seguro de que le estás contando la verdad?"


translate spanish day7_sl_good_7468f5f7:


    me "¡Absolutamente!"


translate spanish day7_sl_good_b06f64e7:


    mt "Pues quizá puede que necesiten tiempo para repensarlo."


translate spanish day7_sl_good_d796d61f:


    me "Salvo que ya no queda tiempo."


translate spanish day7_sl_good_c9524a2a:


    mt "Pues no lo sé."


translate spanish day7_sl_good_4a9b3777:


    "Ella se encogió de hombros con desaliento."


translate spanish day7_sl_good_22cd2b19:


    me "Bueno, por lo menos es algo."


translate spanish day7_sl_good_312eb4e2:


    "Introduje mis ropas de invierno en una mochila y me dirigí hacia la parada de autobús."


translate spanish day7_sl_good_7e15aae1:


    "Si hace un par de días atrás hubiera visto el Icarus en las puertas del campamento, esperando a los pioneros, habría sido la persona más feliz de esta realidad."


translate spanish day7_sl_good_9eebbd94:


    "Pero ahora mismo no me sentía con buen humor para regocijarme."


translate spanish day7_sl_good_5b7603c0:


    "Todavía quedaba mucho tiempo hasta la hora de la partida del autobús."


translate spanish day7_sl_good_8ef889cc:


    "Me senté en el bordillo y agaché mi cabeza entre mis manos."


translate spanish day7_sl_good_d37b9e02:


    un "¿Por qué estás tan triste?"


translate spanish day7_sl_good_c32db60d:


    "Una voz familiar me sacó de mi ensimismamiento."


translate spanish day7_sl_good_9553ce6a:


    me "Solamente..."


translate spanish day7_sl_good_c1368442:


    un "¿Tuviste una riña con Slavya?"


translate spanish day7_sl_good_dd69c3a1:


    me "Podría decirse."


translate spanish day7_sl_good_88021e16:


    un "¿Y cuál fue la razón?"


translate spanish day7_sl_good_1b550b25:


    me "Ella no cree que sea un extraño del futuro."


translate spanish day7_sl_good_abcb2c8d:


    "Sonreí irónicamente."


translate spanish day7_sl_good_312181fb:


    un "Tampoco lo habría creído yo."


translate spanish day7_sl_good_5ffaa56d:


    "Dijo Lena con seriedad."


translate spanish day7_sl_good_82ba88b1:


    me "Si estuviera en tu lugar, yo tampoco lo haría."


translate spanish day7_sl_good_f8d26a13:


    un "No es tan grave, ¡haréis las paces!"


translate spanish day7_sl_good_3b40e91b:


    me "Claro, pero no queda mucho tiempo más..."


translate spanish day7_sl_good_1e436020:


    un "¿Tiempo para qué?"


translate spanish day7_sl_good_3782f67a:


    me "Para tomar la decisión correcta..."


translate spanish day7_sl_good_a20cefa7:


    "..."


translate spanish day7_sl_good_a48849f7:


    "Tras un rato, todo el campamento se congregó en la parada del autobús."


translate spanish day7_sl_good_a2beb057:


    mt "¿Todo el mundo está aquí?"


translate spanish day7_sl_good_14167bf5:


    "Comenzó a decir Olga Dmitrievna."


translate spanish day7_sl_good_044ab0d8:


    mt "Hoy os vais de nuestro campamento, y me gustaría deciros algo antes de partir."


translate spanish day7_sl_good_00123e37:


    "Estaba visiblemente nerviosa y, desesperadamente, se quedó sin palabras."


translate spanish day7_sl_good_019ed77a:


    mt "Espero que recordéis el tiempo que transcurristeis aquí durante vuestra vida, y que retengáis solamente recuerdos agradables sobre Sovyonok."


translate spanish day7_sl_good_b48fe53b:


    mt "También espero que os hayáis convertido un poquito en mejores personas, que hayáis logrado aprender algo y conocido nuevos amigos...{w} Tan solamente... volved el próximo año."


translate spanish day7_sl_good_42256d78:


    "La líder del campamento apartó su mirada.{w} Era como si estuviera tratando de aguantarse las lágrimas para sí misma."


translate spanish day7_sl_good_80682609:


    "No me esperaba que ella se pusiera tan sentimental."


translate spanish day7_sl_good_27a36bac:


    "Aunque su discurso me resultó un completo sin sentido. Como de costumbre."


translate spanish day7_sl_good_e115ea83:


    th "Desde luego, aquí es donde conocí a Slavya. Pero ahora venían tiempos duros. Y lo que sucedería ahora no estaba nada claro."


translate spanish day7_sl_good_e477e76c:


    "Así que no me preocupaba si echaría de menos este lugar y a Olga Dmitrievna o no.{w} Improbablemente..."


translate spanish day7_sl_good_388908da:


    "CUando los pioneros comenzaron a entrar en el autobús, me abrí paso entre la muchedumbre hacia Slavya."


translate spanish day7_sl_good_2be46bfb:


    me "Sentémonos juntos."


translate spanish day7_sl_good_b58c757b:


    sl "Vale."


translate spanish day7_sl_good_9087bb2a:


    "Dijo en voz baja."


translate spanish day7_sl_good_6fa79838:


    "Incluso creí ver una sombra de sonrisa en su rostro."


translate spanish day7_sl_good_d2835e2a:


    "El autobús circuló a través de la amplitud de mi nuevo mundo. Afuera, a los campos les seguían los bosques, y luego las tranquilos ríos y las zonas montañosas."


translate spanish day7_sl_good_341bc523:


    "Me sentía tan familiarizado con todo esto... como si fuera mi propia patria."


translate spanish day7_sl_good_c8980b8f:


    "De todas maneras, los alrededores eran la última de mis preocupaciones ahora... me esforzaba por tratar de encontrar las palabras idóneas para iniciar una conversación."


translate spanish day7_sl_good_6c7e0e00:


    "Estuvimos sentados ahí por un rato, probablemente durante un par de horas. Pero todos los intentos por entablar una conversación habían acabado con respuestas breves de una palabra."


translate spanish day7_sl_good_f10d9132:


    me "¡Olvídate de lo que dije antes! No quiero inventarme excusas o decirte que no es verdad.{w} Es sólo que ahora... la cosa más importante para mí eres tú."


translate spanish day7_sl_good_572579b8:


    sl "Comprendo."


translate spanish day7_sl_good_1b595140:


    "Me miró detenidamente."


translate spanish day7_sl_good_8e0333a3:


    me "A pesar de que a veces actúo mal y de que digo tonterías...{w} Al margen de ello, ¡lo más importante para mí ahora es estar contigo! Y siempre lo será."


translate spanish day7_sl_good_552f93e2:


    me "Entiendo que actué demasiado precipitadamente, planeando aventuras estúpidas. Pero podemos intentar que todo vaya bien. ¡Juntos! ¡Tú y yo!"


translate spanish day7_sl_good_0fb1f792:


    "Slavya todavía permanecía mirándome."


translate spanish day7_sl_good_0907c839:


    sl "¿Te das cuenta de que será difícil? ¿De que no puedes correr ciegamente hacia la oscuridad? Porque sino entonces tendremos muchos problemas, los cuales no seremos capaces de resolver."


translate spanish day7_sl_good_da9a63e8:


    me "¡Sí, ahora lo entiendo!{w} Pero por ti, estoy preparado para cualquier cosa."


translate spanish day7_sl_good_b8586c1f:


    "Ella no me dijo ni una palabra, salvo besarme dulcemente y apoyar su cabeza en mi hombro."


translate spanish day7_sl_good_62038221:


    "¡Eso significa definitivamente un «sí»!"


translate spanish day7_sl_good_44708f32:


    "No quise molestarla más... la dejé dormir."


translate spanish day7_sl_good_e8f795a9:


    "La cuestión es que Slavya finalmente me entendió, comprendió lo que significa ella para mí.{w} Se percató de que haría cualquier cosa por su bien."


translate spanish day7_sl_good_24228837:


    "E incluso si estoy reaccionando de forma desmesurada o decidiendo demasiado pronto, ella está preparada para rectificarme en el camino correcto.{w} ¡El sendero que caminaremos juntos!"


translate spanish day7_sl_good_a20cefa7_1:


    "..."


translate spanish day7_sl_good_7aa62860:


    "La noche empezó a reclamar sus derechos y el autobús entero se rindió al sueño."


translate spanish day7_sl_good_5d1c29d3:


    "Al principio traté de combatirlo, ¿pero tiene utilidad insistir y malgastar mis esfuerzos cuando el futuro se había vuelto al fin tan claro como el agua para mí?"


translate spanish day7_sl_good_c01a1705:


    "Mis ojos se cerraron sólo por un instante..."
# Decompiled by unrpyc: https://github.com/CensoredUsername/unrpyc
