
translate spanish day6_main_8ae28bb6:


    "No sé qué hora era cuando Olga Dmitrievna me despertó, pero aun antes de abrir mis ojos sentí la inminencia de la muerte."


translate spanish day6_main_8d76b64a:


    "Todo mi cuerpo me dolía, me sentía mareado, mi mente estaba nublada por la confusión."


translate spanish day6_main_d8a5165b:


    mt "¡Semyon! Despiértate inmediatamente o te perderás el desayuno y lo más importante, ¡la formación!"


translate spanish day6_main_1950340c:


    "Parecía que la líder del campamento tenía la intención de explotarme al máximo hoy, igual que cualquier otro día."


translate spanish day6_main_10ee39fe:


    "Murmuré algo, cubrí mi cabeza con la manta y me di la vuelta hacia la pared."


translate spanish day6_main_fd3267f5:


    th "Después de todo, la ira de Olga Dmitrievna no pasará de las palabras."


translate spanish day6_main_511dc216:


    "Por lo menos eso quería creer en aquel momento."


translate spanish day6_main_a5a51a86:


    mt "Levántate ahora mismo o..."


translate spanish day6_main_7898d95e:


    "«¿O qué?»... Estaba a punto de decir triunfalmente, pero me quedé callado."


translate spanish day6_main_181f6035:


    "No a causa del miedo, sino porque estaba demasiado perezoso como para abrir la boca."


translate spanish day6_main_45358519:


    th "En serio, ¿qué podría hacerme?"


translate spanish day6_main_53306e73:


    th "¿Sermonearme en la formación? ¿Colgar mi foto en el muro de la vergüenza?"


translate spanish day6_main_2308a0d4:


    th "¿O usarme para experimentos alienígenas inhumanos?"


translate spanish day6_main_ffaac6cf:


    th "Bueno, estoy preparado para eso también, sólo déjame dormir un par de horas."


translate spanish day6_main_4547c60b:


    mt "Vale... ¡Pero si te pierdes la formación...!"


translate spanish day6_main_68fee20c:


    "La puerta se cerró de un portazo tras la líder del campamento, y el sueño que habia estado a punto de desaparecer reclamó mi mente otra vez."


translate spanish day6_main_a20cefa7:


    "..."


translate spanish day6_main_4dec4f4d:


    "Desperté deslumbrado por la luz del sol."


translate spanish day6_main_8f17d018:


    "Era la una de la madrugada según mi teléfono, el cual estaba exprimiendo las últimas gotas de energía de su batería."


translate spanish day6_main_9409b38e:


    "Extrañamente, mi cuerpo no me dolía, mi cabeza estaba despejada, y, en definitiva, era un buen comienzo para el día."


translate spanish day6_main_6142f969:


    "Tras estirar mis brazos, pretendiendo hacer ejercicios de primera hora, salí de la cabaña y me dirigí hacia los lavabos."


translate spanish day6_main_5b04907d:


    th "Sí, me dormí durante el desayuno, pero la comida será servida pronto, así que no debería preocuparme de quedarme hambriento como ayer."


translate spanish day6_main_7052ffcc:


    "Por el camino, me crucé con un pionero cuyo rostro me parecía extrañamente familiar."


translate spanish day6_main_1593a66d:


    "Pero estaba yendo tan rápido que no pude verlo realmente. Y cuando me di la vuelta, ya se había ido por la esquina."


translate spanish day6_main_609cc4c0:


    "No había nadie cerca de los lavabos."


translate spanish day6_main_8bf50f95:


    "Probablemente todos estén ocupados con las tareas de Olga Dmitrievna y Dios sabe qué más."


translate spanish day6_main_81c75e91:


    "Cepillé mis dientes rápidamente, lavé mi cara y ya estaba a punto de irme, cuando de repente escuché el sonido del agua fluyendo por el grifo opuesto."


translate spanish day6_main_ca1b7d89:


    "Había un pionero apoyado contra una jofaina."


translate spanish day6_main_8ce7bba1:


    "No pude ver su rostro, pero a juzgar por su figura se parecía al que había visto hace un par de minutos."


translate spanish day6_main_003b19dd:


    pi "¿Cómo te va? ¡El sol está tan brillante hoy!"


translate spanish day6_main_75daf906:


    "Miré hacia el cielo, protegiendo mis ojos con mi palma."


translate spanish day6_main_725738da:


    me "Sí, nada en especial..."


translate spanish day6_main_6ef01be6:


    "No fue fácil conseguir una mejor vista del pionero... debido a la forma en la que permanecía, la radiante luz del sol que se reflejaba en el agua y la superficie brillante de metal de la jofaina que le oscurecía profundamente su rostro y no pude distinguir ninguna característica."


translate spanish day6_main_cdb07297:


    pi "La líder del campamento hoy está furiosa... ¡muy furiosa!"


translate spanish day6_main_cb4b580d:


    "Su voz también sonaba fastidiosamente familiar."


translate spanish day6_main_2fa452e8:


    me "Bueno, ella siempre es así..."


translate spanish day6_main_c0d7b8c0:


    pi "Bueno, claro la conoces mejor..."


translate spanish day6_main_319ce924:


    "Traté de recordar si antes había escuchado esta voz o había visto este pionero."


translate spanish day6_main_efa43c61:


    pi "Bueno, nos vemos..."


translate spanish day6_main_7c10e79b:


    "Cerró el agua y rápidamente marchó hacia el camino del bosque."


translate spanish day6_main_fe539829:


    "Por un segundo pensé en seguirlo, detenerlo, pero descarté la idea rápidamente ya que decidí que no valía la pena arruinar tal excelente mañana con sospechas y especulaciones molestas."


translate spanish day6_main_de1f880f:


    "La naturaleza resplandecía con la brillante luz de la vida: las abundantes copas de los árboles se mecían rítmicamente en el viento, susurrando una con la otra; la brisa acariciaba gentilmente el alto pasto esmeralda; las aves arrullaban en la sombra, escapando al calor del mediodía; los árboles y los campos, extendiéndose detrás del horizonte, se disolvían en la cálida luz del sol."


translate spanish day6_main_010548de:


    th "No sé qué mes es ahora pero parece ser a mitad del verano."


translate spanish day6_main_fc0175bf:


    "Recordé claramente las vacaciones de verano de mi niñez y mi juventud... tiempos de ocio y alegría despreocupada."


translate spanish day6_main_c43f265a:


    th "Juegos de la infancia, cuántos había..."


translate spanish day6_main_a11227aa:


    th "No le diría que no a jugar a juegos de guerra o al escondite ahora.{w} O balancearme en un columpio."


translate spanish day6_main_bb2be5d8:


    th "O tal vez construir un castillo de arena y habitarlo con soldados de juguete, listos para defender a su amo hasta la última gota de su sangre de plástico."


translate spanish day6_main_a20cefa7_1:


    "..."


translate spanish day6_main_d2032a24:


    "Fui a la plaza y me senté en un banco, esperando a la hora de comer.{w} Parecía que no faltaba mucho tiempo."


translate spanish day6_main_aac9dcb2:


    "De tanto en tanto, los pioneros pasaban por delante de mí, a veces solos, a veces en parejas o en grupos de tres."


translate spanish day6_main_2be25cd9:


    "Pero siempre alguien que no conocía... Alisa, Ulyana, Lena o Slavya, no se las veía por ningún lado."


translate spanish day6_main_e960bd40:


    "Pensamientos sobre el despropósito de la existencia circulaban por mi cerebro, pero esto no me preocupaba en un día tan hermoso."


translate spanish day6_main_fa556551:


    th "Sólo piensa sobre ello, ¿quién podría pensar en lamentarse por su vida vivida en vano o desaprovechada desde un principio mientras se baña en los rayos de un sol tan apetecible?"


translate spanish day6_main_fe4d7ca8:


    th "¡Ciertamente yo no!"


translate spanish day6_main_86f706e5:


    "Contemplé a Genda."


translate spanish day6_main_ad19e3a6:


    "Estaba meditando, como siempre."


translate spanish day6_main_32b69a4b:


    th "Seguramente él nunca tiene dudas innecesarias."


translate spanish day6_main_4eb2c759:


    "Recordé mis primeras horas en este campamento y el día anterior, la angustia, la ansiedad y el miedo."


translate spanish day6_main_e55e0216:


    th "Todo eso parece estar tan lejos ahora, aunque tan poco tiempo haya pasado."


translate spanish day6_main_d7baf9b3:


    th "¿Saldré de aquí o no...?"


translate spanish day6_main_0330e53e:


    "Ya no me preocupaba tanto como antes."


translate spanish day6_main_83657a58:


    th "Tal vez ya estoy muerto.{w} Entonces ésta es la última parada, por favor bájese del tren..."


translate spanish day6_main_ecb0d206:


    pi "¿En qué estás pensando?"


translate spanish day6_main_a9c9e2a5:


    "Miré hacia arriba y vi a aquel pionero que había visto antes."


translate spanish day6_main_323f65fe:


    "Otra vez no pude ver su cara, ya que el sol brillaba en mis ojos."


translate spanish day6_main_91a6c9e2:


    me "Ya sabes, la vida..."


translate spanish day6_main_6af5686f:


    "Parecía que se sentaría a mi lado, pero el pionero se quedó en el mismo lugar, sólo que medio girado, lo cual impidió por completo cualquier esperanza de ver su rostro."


translate spanish day6_main_a0e278fd:


    me "Escucha, ¿nos hemos visto antes? No creo recordarte..."


translate spanish day6_main_80d5b78e:


    pi "Bueno, digamos que sabes quién soy."


translate spanish day6_main_fb74ce55:


    me "Pero no lo sé."


translate spanish day6_main_b5b3bfa4:


    "Me reí sinceramente."


translate spanish day6_main_e0782e42:


    pi "Aquí... no lo sabes."


translate spanish day6_main_b8948d3e:


    "Respondió brevemente."


translate spanish day6_main_f2e007f0:


    me "Ya veo..."


translate spanish day6_main_79123fc6:


    "Honestamente, no es que no quisiera hablar, sólo que no sabía sobre qué hablar."


translate spanish day6_main_50b86b79:


    "Y mi alma estaba tan tranquila que esto no me molestó."


translate spanish day6_main_f6e6d257:


    pi "¿Es tu primera vez aquí?"


translate spanish day6_main_232e0bb1:


    "Hizo una pregunta pero su tono insinuaba que sólo esperaba una confirmación."


translate spanish day6_main_1703d110:


    me "Sí... ¿y tú?"


translate spanish day6_main_43d309aa:


    pi "¿Yo?"


translate spanish day6_main_00b5ddd1:


    "Se detuvo por unos segundos."


translate spanish day6_main_8d5ee990:


    pi "Qué va, no es mi primera vez aquí...{w} Uno podría decir que he visitado este campamento desde mi tierna infancia."


translate spanish day6_main_df6d3916:


    "Tal respuesta me interesó."


translate spanish day6_main_e7458b35:


    me "Bueno...{w} ¿Y cómo era... antes...?"


translate spanish day6_main_d64a6b28:


    pi "Siempre es igual...{w} Olga Dmitrievna siendo la líder del campamento, todos los mismos pioneros alrededor, todas las mismas formaciones por la mañana y todos estos accidentes retorcidos."


translate spanish day6_main_bd473b02:


    "Por un momento pensé que era yo el que hablaba, no él."


translate spanish day6_main_fe8d0158:


    me "Interesante..."


translate spanish day6_main_4f1daffa:


    pi "Es solo que con cada nuevo..."


translate spanish day6_main_a6820f71:


    "Titubeó."


translate spanish day6_main_1bda58b4:


    pi "Año...{w} Más y más cosas interesantes pasan y uno llega a entender mejor lo que está ocurriendo."


translate spanish day6_main_cb23542f:


    me "¿De qué estás hablando?"


translate spanish day6_main_225bea57:


    "Esta conversación había disparado mi curiosidad positivamente."


translate spanish day6_main_b46780c4:


    th "Es una lástima que no pueda distinguir el rostro de este pionero en absoluto."


translate spanish day6_main_aa6afd6a:


    pi "Bueno, cada temporada en el campamento de pioneros me recuerda a la anterior."


translate spanish day6_main_831f663f:


    "Dijo con calma."


translate spanish day6_main_5f789b50:


    me "Probablemente...{w} Ésta es mi primera."


translate spanish day6_main_2015f3db:


    pi "Se nota."


translate spanish day6_main_531b4fd4:


    "El pionero sonrió."


translate spanish day6_main_cdab879e:


    pi "Pero parece que no será la última."


translate spanish day6_main_93624028:


    me "Bueno, aquí es divertido y todo, pero...{w} Ya sabes lo que dicen, no hay lugar como el..."


translate spanish day6_main_aefb5c47:


    pi "¡Hogar, pero aun tienes que volver allí!"


translate spanish day6_main_d12dd680:


    "Ahora estaba completamente seguro de que este tipo me estaba escondiendo algo."


translate spanish day6_main_3a65de66:


    "Para ser exactos, destacaba demasiado entre la normalidad del campamento y era demasiado diferente de los habitantes locales."


translate spanish day6_main_b22130bc:


    me "¿Qué quieres decir?{w} ¿Piensas que estoy atascado aquí para siempre o algo así?"


translate spanish day6_main_8a6dbb06:


    "Dije, pronunciando cada palabra."


translate spanish day6_main_70a4fcf8:


    "El pionero no tuvo tiempo de responder ya que la campana para comer sonó."


translate spanish day6_main_65eca27b:


    "Giré mi cabeza hacia los altavoces, y para el momento en que miré atrás, el tipo ya se había ido."


translate spanish day6_main_7136b652:


    "Miles de teorías y especulaciones aparecieron instantáneamente en mi mente, pero me detuve, recordando toda la normalidad aparente de este campamento."


translate spanish day6_main_d654b2a7:


    "Después de todo, nada sobrenatural ha ocurrido en estos cinco días."


translate spanish day6_main_fbd5c1ad:


    "Además, todo aquí parece demasiado natural, a veces hasta aburrido."


translate spanish day6_main_d6045179:


    th "¿Tal vez este pionero no quiso decir nada con eso y solamente lo malentendí?"


translate spanish day6_main_070cce50:


    "Pensando en eso, fui a la cantina, con la intención de darme un banquete."


translate spanish day6_main_e0e16d60:


    "A veces me parecía que la comida aquí era semejante a las muchedumbres amontonadas alrededor de los comedores sociales durante una hambruna."


translate spanish day6_main_74f00562:


    "Los pioneros corrían por los alrededores, empujándose unos a otros, intentando arrastrarse hasta la primera comida y tomar la mesa más cómoda."


translate spanish day6_main_d065bf1d:


    "Estaba tranquilo, de pie y pacientemente esperando a que el cocinero me diera mis raciones de comida asignadas."


translate spanish day6_main_10ad7f24:


    "Para la comida de hoy teníamos Okroshka (la cual realmente no me gustaba) y chuletas con patatas."


translate spanish day6_main_57891974:


    "Me senté en una esquina y me regocijé mentalmente de que fuera capaz de comer en paz."


translate spanish day6_main_74749cbe:


    "Mi mesa era la más alejada de la cocina. Podía esperar razonablemente que los pioneros que estaban buscando un lugar libre no la alcanzarían."


translate spanish day6_main_c3b3dadd:


    "O que llegarían los últimos."


translate spanish day6_main_92a1900b:


    "Sin embargo, cuando empecé el primer plato, Slavya, Shurik y Electronik aparecieron desde la muchedumbre."


translate spanish day6_main_252cd706:


    sl "¿Podemos?"


translate spanish day6_main_f0cecb70:


    "No tenía nada en contra de su compañía."


translate spanish day6_main_8f9071f9:


    me "Por supuesto."


translate spanish day6_main_2b21bf5c:


    "La comida estaba yendo sorprendemente con calma, ni siquiera Electronik estaba parloteando como usualmente hace."


translate spanish day6_main_3763e444:


    "Finalmente terminé la comida, me estiré en mi silla y, satisfecho, chasqueé mi lengua."


translate spanish day6_main_8ef508e6:


    me "Escuchad, conocéis ese pionero...{w} Lo vi hoy...{w} Ya sabéis, él es muy..."


translate spanish day6_main_59b52b32:


    "De repente me di cuenta de que realmente no sabía como describirlo."


translate spanish day6_main_01a7d78d:


    me "Bueno, más o menos mi altura, misma constitución..."


translate spanish day6_main_789d0b9f:


    sl "Es difícil de saber con tal descripción."


translate spanish day6_main_45a469ba:


    "Slavya sonrió."


translate spanish day6_main_b0d50ab6:


    el "Bueno, la mitad del campamento está compuesta de tipos como ése, si de eso se trata."


translate spanish day6_main_06cde8a8:


    "En definitiva, estaban en lo correcto."


translate spanish day6_main_eaba70e6:


    sh "¿Por qué preguntas?"


translate spanish day6_main_30d66e9b:


    me "Es sólo que lo conocí hoy.{w} Y me pareció como si no lo hubiera visto aquí antes."


translate spanish day6_main_58193b9c:


    el "¡Mira en la cantina!{w} ¡No creo que se pierda la comida!"


translate spanish day6_main_11b8b706:


    th "¡¿Por qué no pensé en eso?!"


translate spanish day6_main_44cf71eb:


    me "¡Eso es!{w} ¡De acuerdo muchachos, disfruten su comida!"


translate spanish day6_main_f11b8b6b:


    "Me levanté y empecé a caminar lentamente a lo largo de las filas de mesas."


translate spanish day6_main_be7f7c50:


    th "Lena y Zhenya están sentadas por allí. Les hice una sonrisa amistosa."


translate spanish day6_main_0a699e1d:


    th "Alisa y Ulyana están riendo y discutiendo algo."


translate spanish day6_main_58fae8be:


    th "Olga Dmitrievna, rodeada de pioneros...{w} Qué bueno que no me vio."


translate spanish day6_main_73c2f32d:


    th "Casi no hay asientos libres, pero el tipo de esta mañana no se le encuentra en ningún lugar."


translate spanish day6_main_bf019437:


    th "La situación se está poniendo más y más interesante..."


translate spanish day6_main_2814bfb0:


    th "Parece que no lo encontraré aquí."


translate spanish day6_main_da518989:


    th "¿Tal vez ya había comido?"


translate spanish day6_main_75e20332:


    "Me dirigí hacia la salida."


translate spanish day6_main_c68ce9bc:


    "Estaba tan caluroso afuera que parecía que te derretirías instantáneamente tras salir de la sombra."


translate spanish day6_main_9f9676f7:


    "De repente sentí ganas de dormir."


translate spanish day6_main_be0bca85:


    "Bostecé y me dirigí a la cabaña de la líder del campamento para seguir la interpretación doméstica del principio de Arquímides."


translate spanish day6_main_c150c0d1:


    "Después de todo, es improbable que Olga Dmitrievna regrese pronto (al menos eso pienso), así que claramente tendré un par de horas para descansar."


translate spanish day6_main_207c30bb:


    th "De todas formas, realmente no espero que otras opciones surjan.{w} Especialmente con este calor."


translate spanish day6_main_ee0c51ab:


    "La frescura celestial en la cabaña se convirtió en mi salvación."


translate spanish day6_main_95912f70:


    "Me quité la ropa y me tiré a la cama."


translate spanish day6_main_3da3d910:


    "Los grillos estaban grillando perezosamente fuera, el viento estaba sacudiendo las cortinas de la ventana, y me hundí en un sueño..."


translate spanish day6_main_a20cefa7_2:


    "..."


translate spanish day6_main_cd340537:


    "Golpes en la puerta me despertaron.{w} Los golpes eran suaves pero insistentes."


translate spanish day6_main_cc72393f:


    "Me levanté de mala gana y fui a abrirla."


translate spanish day6_main_ae0d3213:


    "Extrañamente, no había nadie en las escaleras de la puerta."


translate spanish day6_main_2a6b67de:


    "Me froté los ojos y salí para mirar alrededor.{w} Aun así, no había nadie por los aledaños."


translate spanish day6_main_c567b577:


    th "Un sueño, ¿tal vez?"


translate spanish day6_main_ca2d8082:


    "Ya eran las cuatro en punto."


translate spanish day6_main_ce2e12fc:


    "Me sentía cansado."


translate spanish day6_main_b2353364:


    th "Probablemente no debería haber dormido por la tarde.{w} ¡Después de todo, ya sabía lo que iba a pasar!"


translate spanish day6_main_111fd3b2:


    "Me puse mis botas, salí de la cabaña y cerré la puerta."


translate spanish day6_main_45ecefad:


    th "¿Pero adónde debería ir? Después de todo no queda mucho tiempo hasta la cena."


translate spanish day6_main_ff6f5b1a:


    "Un pensamiento repentino se me ocurrió en que sería estupendo refrescarme, así que fui hacia la playa."


translate spanish day6_main_9f56ddfa:


    "El sol pasó la franja del mediodía hace tiempo.{w} Aquí se vuelve oscuro temprano, en el sur."


translate spanish day6_main_100dfcab:


    "Cerré los ojos y alcé mi mirada hacia el disco ardiente."


translate spanish day6_main_dc571f95:


    pi "¿Quién era ése justo ahora?"


translate spanish day6_main_c4ddbe2a:


    "Me encogí de hombros."


translate spanish day6_main_1a9fc9dc:


    "El mismo tipo misterioso estaba parado frente a mí.{w} Y una vez más su rostro era dificil de ver debido a la luz brillante."


translate spanish day6_main_9330aefd:


    "Por un momento el sol me cegó, así que no pude ver nada más allá del borroso contorno de los objetos."


translate spanish day6_main_f4ab4a03:


    me "¿Me estás acosando?"


translate spanish day6_main_ba4fcc35:


    pi "Nop, sólo pasaba por aquí."


translate spanish day6_main_99107c75:


    "Respondió con calma."


translate spanish day6_main_7cf46969:


    me "¡Entonces no deberías hacer eso!"


translate spanish day6_main_b73b893a:


    pi "¿Así que no has visto a nadie?"


translate spanish day6_main_b4c5d120:


    me "Correcto."


translate spanish day6_main_c9d6ddfd:


    "Froté mis ojos pero no ayudó mucho."


translate spanish day6_main_efd14637:


    me "¿Por qué no te vi durante la comida?"


translate spanish day6_main_9c68f611:


    "Pregunté abiertamente."


translate spanish day6_main_7b536ae4:


    pi "No tenía hambre."


translate spanish day6_main_b703fc38:


    "Él rio."


translate spanish day6_main_61bd5e66:


    "Parpadeé un par de veces y mi visión volvió. Pero el pionero había milagrosamente desaparecido una vez más."


translate spanish day6_main_eec03d52:


    me "¡¿Qué diablos está pasando aquí?!"


translate spanish day6_main_9b11e809:


    "Ahora estaba absolutamente seguro de que había algo extraño sobre ese tipo."


translate spanish day6_main_76509461:


    th "¡Debe estar directamente relacionado con todo lo que está pasando aquí!"


translate spanish day6_main_47c82893:


    th "Tendré que averiguarlo."


translate spanish day6_main_96aea495:


    th "Pero para empezar es bueno confirmar que no estoy equivocado, y que él no es sólo un pionero local."


translate spanish day6_main_c9a31bdf:


    "Estaba bastante abarrotado en la playa.{w} Parecía que todos los del campamento estaban aquí."


translate spanish day6_main_a3ae7c9c:


    th "Bueno, con razón, hace tanto calor afuera."


translate spanish day6_main_d272a3e6:


    "Olga Dmitrievna estaba parada sólo un poco más lejos de los demás, vigilando a su ejército de pioneros."


translate spanish day6_main_7806ecc0:


    "Justo cuando pensé que haber venido aquí podría no haber sido la mejor idea, ella me vio."


translate spanish day6_main_105fc865:


    mt "¡Ey, Semyon!"


translate spanish day6_main_245e3bea:


    mt "¡No estabas en la formación! ¡Y por lo general solamente estás holgazaneando todo el día!"


translate spanish day6_main_3f27c2c0:


    "No supe cómo contestar."


translate spanish day6_main_75ee262f:


    mt "¿Y cómo esperas convertirte en un pionero ejemplar...?"


translate spanish day6_main_28cd78c0:


    "Continuó más suavemente."


translate spanish day6_main_b900c6f1:


    me "Olga Dmitrievna, en el campamento...{w} ¿Hay un pionero que... se parezca mucho a mí?"


translate spanish day6_main_5faf7021:


    "Me miró sorprendida."


translate spanish day6_main_ee235fec:


    mt "Tal vez, no sé...{w} ¿Por qué preguntas?"


translate spanish day6_main_92ba045e:


    me "Oh, sólo es curiosidad..."


translate spanish day6_main_db5e08e9:


    mt "Bueno tenemos muchos muchachos aquí."


translate spanish day6_main_7227b86f:


    me "Está bien, no importa..."


translate spanish day6_main_2bbb0cb6:


    "El grito de alguien vino desde el río y Olga Dmitrievna se apresuró en esa dirección."


translate spanish day6_main_a1ecea1f:


    th "Bueno, parece que me salvé de un sermón."


translate spanish day6_main_89b6f9be:


    "Me senté en la arena y miré fijamente a los pioneros jugando en el agua."


translate spanish day6_main_88733a8a:


    "Tras unos minutos de sentarme aquí, Ulyana se me acercó."


translate spanish day6_main_f62bc905:


    us "¿Por qué tan triste?"


translate spanish day6_main_600e482e:


    me "No estoy triste."


translate spanish day6_main_371d353a:


    us "Bueno, ¿por qué estás sentado aquí completamente solo?"


translate spanish day6_main_1bb0a12c:


    me "Sólo estoy pensando..."


translate spanish day6_main_6a81728a:


    us "¿En qué?"


translate spanish day6_main_df1dac3f:


    "Decidí ahorrarle mi historia sobre el misterioso pionero."


translate spanish day6_main_6da88382:


    me "Bueno imagina que estás charlando con una persona extraña...{w} Y tienes que averiguar exactamente qué es tan extraño en ella."


translate spanish day6_main_95250360:


    us "¡Fácil! ¡Simplemente le preguntaría!"


translate spanish day6_main_ce3666e5:


    "Ella rio."


translate spanish day6_main_f9b97b16:


    me "Demasiado simple...{w} Pero él no respondería."


translate spanish day6_main_01b8a656:


    us "¿Cómo lo sabes? ¿Has intentado preguntarle?"


translate spanish day6_main_4e9618f8:


    me "No, pero es obvio..."


translate spanish day6_main_03ccbb4d:


    "Ulyana no respondió, sólo se sentó a mi lado y dejó escapar un cansado suspiro."


translate spanish day6_main_bb0c418e:


    us "Estoy cansada de nadar."


translate spanish day6_main_98a645b3:


    me "¿Y si sabe algo, pero no te lo dijera?"


translate spanish day6_main_09228ed5:


    us "¡Fuérzalo a que lo haga!"


translate spanish day6_main_65e449ac:


    th "¿Pero cómo...?"


translate spanish day6_main_9fa67ce4:


    "Por un rato más nos quedamos sentados hablando sobre varios temas y luego me levanté, me despedí y me fui."


translate spanish day6_main_1da41780:


    th "Es hora de encontrar al extraño misterioso y conseguir todas las respuestas."


translate spanish day6_main_b30b8dd4:


    "El primer lugar a visitar era la parada de autobús."


translate spanish day6_main_560be1b3:


    th "¡Después de todo, aquí es donde todo comenzó!"


translate spanish day6_main_38c39f41:


    "La carretera estaba vacía, como siempre.{w} Solo pequeños tornados de polvo eran ocasionalmente visibles en ella."


translate spanish day6_main_4a8cd2f2:


    "Estaba listo para darme la vuelta y volver al campamento cuando escuché a alguien susurrar."


translate spanish day6_main_aa3f5830:


    pi "¡No confíes en él!"


translate spanish day6_main_c3c01734:


    "Alguien se estaba escondiendo detrás del monumento."


translate spanish day6_main_2475793e:


    "Un pionero estaba sentado allí dándome la espalda."


translate spanish day6_main_c32fda68:


    "Y una vez más, su voz parecía extrañamente familiar."


translate spanish day6_main_0a2309cb:


    me "¡¿Quién eres?!"


translate spanish day6_main_8c7ee3c8:


    "Me acerqué unos pasos hacia él."


translate spanish day6_main_d6271c95:


    pi "¡Detente! ¡No te acerques más!"


translate spanish day6_main_f0d11be8:


    "Por alguna razón, me detuve."


translate spanish day6_main_68a3cebc:


    "Desde algún lugar profundo dentro de mí, tuve confianza y entendí que no debería discutir con él."


translate spanish day6_main_c41b6a4c:


    me "De acuerdo, me quedaré aquí..."


translate spanish day6_main_9f354a24:


    pi "¿Lo has visto? ¿Has hablado con él?"


translate spanish day6_main_ae9a3d04:


    "Preguntó nervioso."


translate spanish day6_main_3edfb9a5:


    me "¿De quién siquiera estás hablando?"


translate spanish day6_main_f3effee3:


    "Estaba vestido en un uniforme apropiado de pionero."


translate spanish day6_main_65856116:


    pi "Ya sabes quien..."


translate spanish day6_main_08a2904f:


    "¡Y definitivamente lo sabía!{w} Estaba hablando sobre el extraño pionero que había visto antes."


translate spanish day6_main_9513cd87:


    me "Sí..."


translate spanish day6_main_424c8856:


    "Respondí tras un momento de silencio."


translate spanish day6_main_e3fcbbd3:


    pi "¿Qué te dijo?"


translate spanish day6_main_137679fe:


    "El pionero preguntó en un tono suplicante."


translate spanish day6_main_e88413fc:


    me "En realidad, nada..."


translate spanish day6_main_5d3e0c2a:


    pi "¿Te dio un consejo? ¿Te dijo qué hacer? ¿Te amenazó?"


translate spanish day6_main_3a4fb558:


    me "No, nada de eso...{w} Seguro, me pareció raro, pero nada más..."


translate spanish day6_main_7896c94c:


    pi "¡Recuerda que puede no estar solo! O aun más probable, está solo pero puede que te encuentres más pioneros que se parezcan a él."


translate spanish day6_main_0effa2c7:


    me "¿Y qué hay de ti? ¿Quién eres? ¿De quién te escondes?"


translate spanish day6_main_18d9b086:


    pi "Lo entenderás...{w} En su debido momento...{w} Sólo recuerda, ¡lo más importante es encontrar la salida!"


translate spanish day6_main_ea099937:


    "De repente pasó una fuerte ráfaga de viento, arrancando las hojas de los árboles y arrojando una vieja bolsa de papel en mi cara. Cubriéndome, aparté los ojos."


translate spanish day6_main_3b1e051a:


    "Cuando miré de vuelta al monumento, el pionero ya se había ido."


translate spanish day6_main_cb002221:


    "En ese momento estaba agobiado por el miedo.{w} Miedo real, casi tangible."


translate spanish day6_main_5eb7b77f:


    "Recuerdo haber estado asustado durante mis primeras horas en el campamento."


translate spanish day6_main_2ad95628:


    "Pero en ese entonces todo a mi alrededor parecía amable y amistoso. Ahora Sovyonok había revelado sus colmillos, preparándose para comerme."


translate spanish day6_main_a7216041:


    "Y delante...lo desconocido..."


translate spanish day6_main_6c246aae:


    "Se me había puesto por todo mi cuerpo la piel de gallina, mi garganta estaba seca y mis manos estaban temblando. Intenté ignorarlo todo y volver al campamento."


translate spanish day6_main_8983704d:


    "Ya era la hora de cenar, pero no tenía deseo de ir a la cantina."


translate spanish day6_main_c10e6db8:


    "Después de encontrarme al segundo pionero misterioso, solo pensar en los lugareños, si no me asustaba, al menos me intimidaba."


translate spanish day6_main_3f3d68fb:


    th "Ni siquiera se quién, o qué, son todos ellos."


translate spanish day6_main_d902b9fb:


    th "Y a pesar de que todavía no había pasado nada, ¡no significaba que pudiera confiar en ellos!"


translate spanish day6_main_94d63846:


    "Me detuve frente al edificio de clubs, pero luego me di cuenta de que ése no era el mejor lugar para estar... me podría encontrar con alguien, lo cual no quería hacer en ese momento."


translate spanish day6_main_fdbf6225:


    "Casi corriendo, me apresuré hacia el bosque y caminé unos pocos metros antes de detenerme finalmente."


translate spanish day6_main_35b23e96:


    th "Por un lado... el pionero misterioso...{w} Bueno, dos para ser exactos."


translate spanish day6_main_d208b9b7:


    th "Por otro lado... todos los habitantes de este campamento, quienes parecían absolutamente normales."


translate spanish day6_main_8ae0dc11:


    "De cualquier forma, mi decisión de alejarme y dejar que todo se resuelva por su cuenta estaba bajo seria presión."


translate spanish day6_main_0b638a1b:


    "Llegados este punto lo tenía claro... Tenía que hacer algo."


translate spanish day6_main_5fd8c6ed:


    th "¿Pero qué exactamente...?"


translate spanish day6_main_5225748d:


    "Todos están cenando probablemente, así que tengo una buena posibilidad de llegar hasta el lugar de Olga Dmitrievna sin ser visto."


translate spanish day6_main_f9f20d48:


    "Decidí primero coger la ropa que vestía cuando llegué aquí, junto con mi teléfono móvil."


translate spanish day6_main_346b48a6:


    th "No podré formar un plan al instante, así que por ahora podría esconderme en el bosque."


translate spanish day6_main_1840851c:


    "Subí los escalones y entré en el edificio."


translate spanish day6_main_e7710e4b:


    "Mi ropa estaba donde la había dejado y mi teléfono donde debía estar... debajo de mi almohada."


translate spanish day6_main_b5791e64:


    "Lo tomé rapidamente y estaba listo para meterlo en mi bolsillo cuando noté algo en la pantalla."


translate spanish day6_main_4eda2e3c:


    "La ventana de mensajes estaba abierta."


translate spanish day6_main_3d971385:


    "El mensaje decía:"


translate spanish day6_main_a645ed9e:


    "«¡Estás equivocado, Semyon, estás muy equivocado!»"


translate spanish day6_main_d3429bef:


    "Sentí como mi alma dejaba mi cuerpo."


translate spanish day6_main_a9617350:


    "Me quedé de piedra."


translate spanish day6_main_8962b7ae:


    "Estaba temblando y mi sangre estaba bombeando tan rápido, que sentí como si mi cráneo fuera a explotar de la presión."


translate spanish day6_main_4e8980f6:


    "Me llevó al menos un minuto entero para recuperar el conocimiento."


translate spanish day6_main_b2d0ecc1:


    th "Olga Dmitrievna pudo haber escrito ese mensaje...{w} O uno de los pioneros."


translate spanish day6_main_5b297fa5:


    th "Es fácil comprender cómo hacerlo, incluso si no están familiarizados con esta tecnología."


translate spanish day6_main_98fe4021:


    th "¡Pero nadie más que yo debería saber dónde guardo mi teléfono!"


translate spanish day6_main_2d7a0195:


    "Este acontecimiento fue la gota que colmó el vaso."


translate spanish day6_main_d3412ceb:


    "Salí de la casa corriendo, decidido a no volver allí nunca más."


translate spanish day6_main_a20cefa7_3:


    "..."


translate spanish day6_main_d6e969ac:


    "La noche cayó sobre el terreno del campamento."


translate spanish day6_main_8e9948b3:


    "Había estado sentado en el bosque por unas horas, estremeciéndome por cualquier ruido."


translate spanish day6_main_a921011f:


    "El lugar que elegí estaba lejos del camino, así que sería difícil encontrarme."


translate spanish day6_main_b808e2cc:


    "Por el momento tenía un par de planes en mi cabeza.{w} Intentar huir, matar a cada uno de ellos en el campamento..."


translate spanish day6_main_ecefb337:


    "Por otro lado sin embargo, un pensamiento me perseguía y me di cuenta de que no debía comportarme así por un pionero misterioso. Ni por ese mensaje extraño en el teléfono..."


translate spanish day6_main_f4c17cd7:


    th "Todo esto reforzaba la idea de que todo lo que sucedía aquí estaba muy lejos de ser normal, sin embargo, no había pruebas de que nadie en el campamento tuviera algo que ver con ello."


translate spanish day6_main_09d97100:


    "Podría haberme quedado aquí hasta el amanecer, perdido en mis pensamientos, pero oí pasos viniendo desde algún lugar cercano..."


translate spanish day6_main_34d72ebd:


    pi "¿Así que decidiste escapar después de todo?"


translate spanish day6_main_c9a36b10:


    "Me di la vuelta pero no pude ver el rostro del pionero en la oscuridad."


translate spanish day6_main_06562ffd:


    "De todas formas, estaba bastante seguro de que era {i}él{/i}."


translate spanish day6_main_cb29da41:


    "Pues no tenía miedo."


translate spanish day6_main_c09ac6c3:


    "Más exactamente, estaba tan exhausto físicamente que estaba preparado para cualquier tipo de acontecimientos y de alguna forma logré razonar apropiadamente y mantuve una conversación."


translate spanish day6_main_0a666ca8:


    me "No llamaría a esto una escapada."


translate spanish day6_main_6f0a1453:


    "Respondí lentamente, alargando cada palabra."


translate spanish day6_main_2f8ebf8c:


    pi "¿Eh? ¿Entonces qué es?"


translate spanish day6_main_4ca5f33e:


    me "Una retirada táctica..."


translate spanish day6_main_871ad7d7:


    pi "¡Brillante!"


translate spanish day6_main_1141c237:


    "Estalló en carcajadas."


translate spanish day6_main_32251891:


    me "Escucha ¿por qué no me dices qué está pasando, quién eres y qué quieres de mí?"


translate spanish day6_main_2f1ec6d2:


    pi "¿Qué te díjo {i}aquel tipo{/i} en la entrada?"


translate spanish day6_main_794b660f:


    "Parecía que ni siquiera había oído mi pregunta."


translate spanish day6_main_07ceaf40:


    me "Me dijo que no confiara en ti."


translate spanish day6_main_bc2b8f99:


    "Mentí."


translate spanish day6_main_8f076af1:


    "Pero por otro lado, pensé que eso quiso decir."


translate spanish day6_main_b22a7523:


    pi "Bueno, él siempre es así... Huyendo, escondiéndose..."


translate spanish day6_main_85e1a38c:


    "Podía oír la irritación en su voz."


translate spanish day6_main_49ff021e:


    me "¡No sé qué tipo de circo estás manejando aquí, pero no voy a ser parte de él!"


translate spanish day6_main_f1508df8:


    pi "Oh, por qué no...{w} ¡Después de todo, tú eres la estrella!"


translate spanish day6_main_565563b0:


    "No podía ver su cara, pero estaba seguro de que sonreía."


translate spanish day6_main_26e04ee0:


    me "¡Explícame mi papel entonces!"


translate spanish day6_main_e4cb38e3:


    pi "Sabes, yo era como tú al principio...{w} La primera vez todo fue tranquilamente."


translate spanish day6_main_6ca36315:


    pi "Luego escapé, traté de entender qué estaba pasando, me enojé e incluso...{w} ¡los torturé para que me dijeran la verdad!"


translate spanish day6_main_a667d35b:


    "Empezó a reír como un loco."


translate spanish day6_main_5eb260fc:


    "Temblé."


translate spanish day6_main_8b9a92c3:


    pi "¡Pero fue en vano!"


translate spanish day6_main_6b9446af:


    "Continuó el pionero luego de calmarse un poco."


translate spanish day6_main_2a471045:


    pi "En vano...{w} Y luego empecé a notar los lapsos..."


translate spanish day6_main_a7d4acbc:


    pi "Al principio solo escuchaba sus voces... desde lejos y a veces en mi cabeza."


translate spanish day6_main_3dc38f3b:


    pi "Luego, siluetas difusas aparecieron.{w} Entonces lentamente tomaron forma física."


translate spanish day6_main_c695ff8a:


    pi "¡Y finalmente, entraron en mi mundo! Podía tocarlos, presentárselos a otros pioneros..."


translate spanish day6_main_81e2b75f:


    pi "¡Y todos eran diferentes! ¡Diferentes! ¿Entiendes? ¡Diferentes!"


translate spanish day6_main_54c1072c:


    "Empezó a gritar."


translate spanish day6_main_634696b6:


    pi "Y volvió a ocurrir una y otra vez. Te puedes acostumbrar a los ciclos, pero..."


translate spanish day6_main_b4bb03eb:


    pi "Luego aprendí cómo entrar a sus mundos por mí mismo, interactuar con los otros. Pero resulta que no estaba solo. ¡Hay muchos de nosotros!"


translate spanish day6_main_62a7ca22:


    pi "Hoy viste al menos uno más."


translate spanish day6_main_7e8f7bb7:


    "El pionero se sumió en silencio."


translate spanish day6_main_f4b1a463:


    "No sabía que preguntar y no quería interrumpir su historia, así que simplemente esperé."


translate spanish day6_main_0c7f5431:


    "Tras un minuto continuó."


translate spanish day6_main_bb9de06b:


    pi "No es tan simple, por supuesto...{w} Y no siempre es posible."


translate spanish day6_main_7fd20c4d:


    pi "Solo bajo ciertas circunstancias...{w} Cuando sientes emociones fuertes, por ejemplo."


translate spanish day6_main_fb2863a3:


    "Instantáneamente recordé como lo vi por primera vez, en el momento en que no sentí ninguna emoción para nada."


translate spanish day6_main_f7c370d3:


    pi "Incluyendo los momentos en que te sientes feliz..."


translate spanish day6_main_75932a4e:


    "Era como si estuviera leyendo mis pensamientos."


translate spanish day6_main_505d91ae:


    me "Ya veo..."


translate spanish day6_main_88477eb9:


    me "Así que, ¿estás intentando decir que hay varios mundos paralelos con el mismo campamento y sus habitantes, en los cuales yo soy reemplazado por ti o el tipo que me crucé en la parada de autobús...?"


translate spanish day6_main_ee412b1d:


    pi "Sí, algo por el estilo..."


translate spanish day6_main_7c3f9b3e:


    "Su respuesta no me sorprendió en absoluto."


translate spanish day6_main_04ff5586:


    "Al final, está claro que lo que sea que esté pasando aquí, está más allá de los límites del entendimiento humano, y esta teoría no parecía inusual en lo más mínimo."


translate spanish day6_main_b263087c:


    me "Pero dijiste que todo se repite..."


translate spanish day6_main_c5256338:


    pi "Sí, todo se repite."


translate spanish day6_main_90478ae9:


    me "¿Y qué pasa...{w} luego?"


translate spanish day6_main_af16c541:


    pi "Empiezas justo por el principio... despiertas en el autobús, vienes al campamento, conoces a Olga Dmitrievna, las muchachas, Electronik..."


translate spanish day6_main_eea0fa70:


    me "Pero si te sucedió a ti, ¿por qué debería pasarme a mí también?"


translate spanish day6_main_cae5da7b:


    pi "¡Le pasa a todos!"


translate spanish day6_main_25c5c857:


    "Una vez más, rio histéricamente."


translate spanish day6_main_571d1544:


    me "¿Y cuántos «ciclos» has experimentado hasta ahora?"


translate spanish day6_main_7768eb2b:


    pi "Ya dejé de contarlos...{w} Aunque al principio intenté recordarlos.{w} Tal vez unos pocos cientos..."


translate spanish day6_main_7927bc7d:


    "Con razón, me estaba muy claro que este tipo estaba sufriendo de una disfunción mental progresiva."


translate spanish day6_main_72d6794a:


    me "¿Pero buscaste una salida, no?"


translate spanish day6_main_ab51e1d2:


    "No contestó."


translate spanish day6_main_dfec187b:


    me "Eso es de lo que aquel tipo en la parada de autobús estaba hablando."


translate spanish day6_main_e710f157:


    pi "¡Porque él no entiende nada, maldición!"


translate spanish day6_main_332b374a:


    "Gritó."


translate spanish day6_main_31847263:


    pi "¡Porque él es como tú! ¡Corriendo constantemente, escondiéndose!"


translate spanish day6_main_42cd9689:


    me "¿Qué sugieres entonces?"


translate spanish day6_main_d5f8131d:


    pi "Si tuviera alguna sugerencia, no estaría aquí hablando contigo."


translate spanish day6_main_3a64a45c:


    me "Bueno, pero lo intentaste, ¿o no?"


translate spanish day6_main_5887898e:


    pi "¿Estás tratando de entender lo que sé, puesto que he estado aquí por un largo tiempo?"


translate spanish day6_main_c276f7a2:


    "Preguntó alegremente."


translate spanish day6_main_c38656e1:


    me "Bueno, eso es evidente..."


translate spanish day6_main_94935db0:


    pi "Sí, intenté escapar de aquí."


translate spanish day6_main_6bc52dbe:


    pi "Allí..."


translate spanish day6_main_ee7633ba:


    "Apuntó hacia el bosque."


translate spanish day6_main_57f82592:


    pi "...no hay nada.{w} Nada más que arboles.{w} Cuando había caminado por varios días y finalmente me desmayé, estaba en el autobús de nuevo."


translate spanish day6_main_00d4b823:


    pi "Caminar por la carretera es lo mismo... es infinita."


translate spanish day6_main_24fd33fb:


    pi "Hablar con alguien sobre ello es inútil. Probablemente ya lo aprendiste por ti mismo."


translate spanish day6_main_fb6303a8:


    me "Sí."


translate spanish day6_main_677a1d30:


    "Interrumpí."


translate spanish day6_main_ea337e24:


    pi "Inclusive si intentas explicar tu situación directamente, a lo mejor te consideran un idiota."


translate spanish day6_main_a3fc2851:


    me "¿Y si intentas no dejar el autobús?"


translate spanish day6_main_75d11f10:


    pi "Es inútil.{w} Tarde o temprano, te encuentran."


translate spanish day6_main_3ba30819:


    pi "Inclusive intenté pasar todo el tiempo allí.{w} Al final me dormí, y todo comenzó por el principio."


translate spanish day6_main_472d9432:


    me "¿Y si lo enciendes tú mismo y te vas?"


translate spanish day6_main_5782e23f:


    pi "No hay llaves.{w} Y no tengo habilidades para robar vehículos."


translate spanish day6_main_9e6c1787:


    me "Un ciclo maldito..."


translate spanish day6_main_8712b475:


    pi "¡Exacto!"


translate spanish day6_main_d39e8b29:


    "Un largo silencio se hizo"


translate spanish day6_main_1d014408:


    "Finalmente, pregunté:"


translate spanish day6_main_99ff1407:


    me "¿Y qué hay de la gente de por aquí? ¡¿No tienen sospechas razonables en absoluto?!"


translate spanish day6_main_92239f1a:


    pi "¿Sospechas?"


translate spanish day6_main_bbf472a0:


    "Una risa de nuevo."


translate spanish day6_main_b33ce14d:


    pi "¡Lo único sospechoso sobre ellos es el hecho de que no importa cuánto les digas, te miran fijamente con sus ojos completamente abiertos!"


translate spanish day6_main_24929dab:


    pi "La gente...{w} Les temía al principio, también."


translate spanish day6_main_a250d056:


    pi "Luego los usé para diferentes experimentos...{w} Y ahora no puedo considerarlos humanos ¡Son todos muñecos, marionetas!"


translate spanish day6_main_74deb4fc:


    pi "¡Puedes predecir cualquier reacción fácilmente, cualquier palabra o acción!"


translate spanish day6_main_de26d074:


    me "¿Entonces no vale la pena temerles, o sí?"


translate spanish day6_main_e38b3c64:


    pi "Al que vale la pena temer es a ti mismo..."


translate spanish day6_main_47ff30a4:


    "Dijo suavemente."


translate spanish day6_main_9180ae03:


    pi "¿Sabes lo gracioso que es cuando los huesos humanos se quiebran lentamente por la distensión...?"


translate spanish day6_main_642dbde1:


    th "Parece que finalmente perdió la cabeza."


translate spanish day6_main_d97fd181:


    me "Escucha, entiendo todo, pero..."


translate spanish day6_main_c5e21a03:


    pi "El otro también piensa que es demasiado..."


translate spanish day6_main_88c33a5e:


    pi "Pero no importa. ¡Nadie aquí es real salvo tú!"


translate spanish day6_main_90713cb4:


    "El pionero se calló de repente."


translate spanish day6_main_8520460f:


    me "Bueno, no, no estoy de acuerdo contigo."


translate spanish day6_main_e8e26d07:


    pi "Probablemente estés pensando «¿por qué no me aseguré de eso yo mismo?», ¿o no? «¿Por qué no intenté llegar a la aldea o pueblo más cercano?»."


translate spanish day6_main_7c6f498a:


    me "Sí, tuve tales pensamientos."


translate spanish day6_main_691f28f6:


    pi "«¿Por qué no sospeché? ¿Por qué estaba haciendo otras cosas en lugar de buscar respuestas?»."


translate spanish day6_main_2c057d1c:


    me "Bueno... Sí..."


translate spanish day6_main_fa8bd2c9:


    pi "¡Olvídalo!{w} Eso es normal.{w} Todos nos comportamos así la primera vez aquí."


translate spanish day6_main_a5bbe860:


    me "¿Pero, cuántos otros has visto hasta ahora?"


translate spanish day6_main_aa579a81:


    pi "No muchos."


translate spanish day6_main_447e6d41:


    "El pionero se quedó ensimismado."


translate spanish day6_main_dcc4b12f:


    pi "Alrededor de diez, tal vez...{w} ¡Pero estoy seguro de que hay muchos más!"


translate spanish day6_main_011e2d8a:


    me "Y...{w} son todos...{w} ¿así?"


translate spanish day6_main_65c1d01f:


    pi "De hecho, sí.{w} Sólo los detalles son diferentes, pero lo principal es invariable... ¡No hay salida!"


translate spanish day6_main_01815b85:


    pi "Y, por cierto, ¡tú eres como la mayoría de ellos!"


translate spanish day6_main_509c847e:


    pi "La única diferencia es que tú has oído todo de mí en lugar de experimentarlo por ti mismo."


translate spanish day6_main_e03c0d9a:


    "No sé si debería agradecerle por eso..."


translate spanish day6_main_4b5045fd:


    me "Bueno, ¿y ahora qué?"


translate spanish day6_main_f3d36d37:


    pi "Nada."


translate spanish day6_main_d9259528:


    "Dijo brevemente."


translate spanish day6_main_36cf3956:


    "Cerré mis ojos y me sumí en mis pensamientos.{w} Ése fue un error crucial."


translate spanish day6_main_0925d989:


    "Cuando abrí mis ojos, no vi a nadie, tal como las anteriores veces."


translate spanish day6_main_6d094282:


    th "Así que está claro lo que está pasando...{w} Pero, al final, ¿cómo podría todo lo que dijo guiarme a una respuesta?"


translate spanish day6_main_387743f2:


    th "Sí, no estoy solo. Sí, todo se repite.{w} ¿Pero cuál es la razón de todo esto?"


translate spanish day6_main_ad7a645c:


    th "Y, lo que es aun más importante... ¿Dónde está la salida...?"


translate spanish day6_main_f75ae07b:


    "Lo único útil que extraje de la conversación es que los lugareños no valen la pena ser temidos."


translate spanish day6_main_a7aff17c:


    "Y eso era bastante significativo para mí, ya que era mucho mejor dormir en una cabaña cálida en lugar del bosque."


translate spanish day6_main_2c3edc12:


    "Suspiré, tomé mis cosas y me dirigí al campamento."


translate spanish day6_main_89090f90:


    "Mientras caminaba, surgía ocasionalmente la conversación con mi compañero de fatigas."


translate spanish day6_main_b3935654:


    th "¿Por qué no le pregunté sobre el mensaje en mi teléfono? Y otras cosas...{w} Había pistas, después de todo..."


translate spanish day6_main_222e2152:


    th "Tal vez si hubiera encontrado algunos detalles, podría haber llegado a alguna conclusión."


translate spanish day6_main_bd2b9931:


    "Sin embargo, no parecía que nuestro encuentro fuera a ser el último."


translate spanish day6_main_168f0e75:


    "Las luces en la cabaña de Olga Dmitrievna estaban encendidas."


translate spanish day6_main_535a1938:


    "Abrí la puerta cuidadosamente y entré."


translate spanish day6_main_f6e23c0c:


    mt "¡Hablando del rey de Roma, aquí estás!"


translate spanish day6_main_30eab556:


    "Dijo enojada la líder del campamento."


translate spanish day6_main_5ad08176:


    me "Olga Dmitrievna, estoy muy cansado ahora mismo, así que pospongamos el sermón."


translate spanish day6_main_54224d71:


    mt "Me pregunto de qué estás Cansado..."


translate spanish day6_main_2e9daa0c:


    me "¡Por todo!"


translate spanish day6_main_df4d1541:


    "Le espeté groseramente."


translate spanish day6_main_0193dcd6:


    "La líder del campamento me miró con sorpresa."


translate spanish day6_main_33eccc5f:


    "Me tiré en la cama sin desvestirme."


translate spanish day6_main_307422bd:


    mt "Semyon, ¡un pionero ejemplar no debería comportarse así!"


translate spanish day6_main_b8a5fff1:


    me "¿Y cómo debería comportarme entonces?"


translate spanish day6_main_dccb57d2:


    mt "Bueno, no así..."


translate spanish day6_main_9a8666dc:


    "La líder de campamento tartamudeó por un momento, como si se esforzara para encontrar las palabras correctas."


translate spanish day6_main_fd3b5cff:


    mt "¡Debería respetar a sus mayores!"


translate spanish day6_main_56af3994:


    me "La respeto inmensamente, seguro..."


translate spanish day6_main_105fc865_1:


    mt "¡Semyon!"


translate spanish day6_main_8b538c95:


    "El sarcasmo en mi voz no pasó desapercibido."


translate spanish day6_main_87be519f:


    me "Y ahora me gustaría dormir."


translate spanish day6_main_f10bfcfd:


    mt "Espera, yo..."


translate spanish day6_main_b616628c:


    me "Por cierto, ¿qué tan lejos está el pueblo más cercano? ¿Cuándo llega el autobús? ¿Cómo puedo siquiera salir de aquí?"


translate spanish day6_main_a2cc86f7:


    "Pregunté en un tono sorprendentemente calmado."


translate spanish day6_main_68f9d7af:


    "No hubo respuesta, y no debería haber ninguna."


translate spanish day6_main_d16f1af1:


    "Dije todo eso sólo para deshacerme de ella."


translate spanish day6_main_6cba906b:


    me "¿Por qué estás callada?"


translate spanish day6_main_3dfe0def:


    mt "Estoy cansada, hablemos de esto mañana."


translate spanish day6_main_03e11cff:


    "La líder del campamento se levantó y apagó la luz."


translate spanish day6_main_e49c22a1:


    th "Sí, es tal y como dijo ese tipo."


translate spanish day6_main_0d4a118f:


    th "Me pregunto por qué le hablé tan calmado y correctamente..."


translate spanish day6_main_c64fd468:


    th "Debería haber estado temblando de horror..."


translate spanish day6_main_c516c7b0:


    "Aquel pionero no tenía mucha credibilidad, pero tenía una sensación de que es incapaz de lastimarme."


translate spanish day6_main_7af9b647:


    "Tan pronto como pensé que sería una buena idea preguntar algo más, la fatiga se apoderó de mí y me dormí."


translate spanish day6_un_f1700078:


    "Me desperté porque alguien sacudía mis hombros."


translate spanish day6_un_1a840eb3:


    "Era demasiado difícil abrir mis ojos, así que solo gimoteé pateticamente."


translate spanish day6_un_6b65ae8f:


    mt "¡Levántate ya! ¡Te vas a perder la formación!"


translate spanish day6_un_df156736:


    "Una vez que me di cuenta de qué era lo que Olga Dmitrievna quería de mí y qué hora era, me giré cara a la pared."


translate spanish day6_un_cb9038f3:


    "Estaba tan cansado que quería estrangular a la líder del campamento sólo para que dejara de perturbar mi recuperación del ayer infernal."


translate spanish day6_un_05acfb3e:


    mt "¡Semyon! ¡Levántate inmediatamente!"


translate spanish day6_un_ceeafad7:


    "Reuní mi fuerza, abrí mis ojos y me senté."


translate spanish day6_un_1e98f221:


    me "Olga Dmitrievna...{w} Entiendo, pero tuve un día difícil ayer..."


translate spanish day6_un_e57c744c:


    me "¿Puedo dormir hasta tarde al menos por hoy?"


translate spanish day6_un_10659a77:


    "Comencé a suplicar."


translate spanish day6_un_215606b1:


    mt "¡Está fuera de discusión! ¡La formación es obligatoria para todos los pioneros!{w} Y ya te la has saltado un par de veces."


translate spanish day6_un_703bafc8:


    "Mi cabeza estaba completamente dormida así que no podía encontrar argumentos para contrarrestar eso."


translate spanish day6_un_f9754f3e:


    "En un par de minutos ya estábamos parados en la plaza."


translate spanish day6_un_22b326f3:


    "Cabeceé, sufriendo para no dormirme de pie, así que me perdí todo lo que Olga Dmitrievna anunció."


translate spanish day6_un_df687591:


    "La mayoría de los pioneros parecían sentir lo mismo."


translate spanish day6_un_58d2f9bd:


    "Elektronik bostezaba constantemente, Alisa tenía ojeras gigantes, sólo Ulyana parecía estar llena de salud y energía, como siempre."


translate spanish day6_un_316f08e7:


    "Barrí la formación con mis ojos una vez más pero no pude encontrar a Lena."


translate spanish day6_un_ecc81dcb:


    th "Eso es raro.{w} Ella, por lo general, es una muchacha diligente y comprometida, así que es impropio de ella perderse tales eventos."


translate spanish day6_un_3d4c4b35:


    th "Por otro lado, eso de ayer fue demasiado para ella. Tal estrés.{w} Probablemente esté deprimida..."


translate spanish day6_un_f49ecdba:


    "Aunque ese tipo de comportamiento fue inesperado viniendo de ella."


translate spanish day6_un_9512b0f6:


    "Quiero decir, sospechaba que ella no era realmente el tipo de persona que quería aparentar ser en los ojos de los demás, pero nunca me hubiera esperado un cambio tan drástico."


translate spanish day6_un_f2c60111:


    "Lena... ¡Increíble!... De alguna forma me recordaba a Alisa, aún más severa y brutal a veces."


translate spanish day6_un_e4d65836:


    "Y ahora no estaba seguro de cómo comportarme cerca de ella... Simplemente le temía."


translate spanish day6_un_ff2fa3df:


    "Finalmente, la formación terminó y los pioneros se arrastraron hasta el desayuno."


translate spanish day6_un_20de4020:


    "Miku me alcanzó cerca de la cantina."


translate spanish day6_un_b0f1c022:


    mi "¡Semyon, buen día! ¿Cómo dormiste? ¿Tuviste algún sueño? ¿Cómo estás? ¿Listo para el desayuno?"


translate spanish day6_un_8d857931:


    "Como siempre, parloteaba sin pausa, añadiendo una linda sonrisa al mismo tiempo."


translate spanish day6_un_e7ce88cd:


    me "Estoy bien."


translate spanish day6_un_56b00510:


    "Respondí perezosamente."


translate spanish day6_un_ec604917:


    mi "Hoy está bastante gris... Tal vez sea por el clima; está bastante oscuro, incluso puede que llueva... ¡Así que sí! ¡Todos están tristes y lúgubres! Pensé que tal vez algo había pasado pero nadie me dijo qué."


translate spanish day6_un_aafa05de:


    mi "¡¿Puedes imaginártelo!? Algo ocurrió y todos saben acerca de ello excepto yo. Estaba triste al respecto, pero entonces..."


translate spanish day6_un_bb18abf7:


    me "No te preocupes, no te perderás el fin del mundo."


translate spanish day6_un_27c62366:


    "Emití un comentario sarcástico."


translate spanish day6_un_57d2c9aa:


    mi "¿Eh?"


translate spanish day6_un_94555fb6:


    "Parecía que estaba tan inmersa en su monólogo que no se daba cuenta de lo que sucedía a su alrededor."


translate spanish day6_un_a7320a03:


    me "Y si de alguna forma te lo vas a perder... yo me aseguraré de hacértelo saber..."


translate spanish day6_un_c63fd243:


    me "Nada. Disfruta de tu comida."


translate spanish day6_un_63855039:


    "Marché hacia la cantina con paso firme para recibir mi porción diaria de grasa, proteínas y carbohidratos."


translate spanish day6_un_88b41ad3:


    "Para mi sorpresa, un asiento en el rincón mas alejado estaba libre."


translate spanish day6_un_c21d2e19:


    "Me senté e intenté con mucho esfuerzo poner una cara que le haría saber a los demás que no había necesidad de acercárseme, a menos que hubiera una emergencia."


translate spanish day6_un_b5a8b072:


    "Simplemente quería sentarme solo y pensar.{w} Además, el mantener mi mente alejada de otras cosas me estaba quitando el sueño."


translate spanish day6_un_c0c4e707:


    sl "Hola, ¿te molesta si te acompaño?"


translate spanish day6_un_3501408a:


    "No vi a Slavya parada allí."


translate spanish day6_un_d80e4062:


    th "Extraño..."


translate spanish day6_un_9513cd87:


    me "Para nada..."


translate spanish day6_un_d20f4795:


    "Respondí luego de dudar un segundo."


translate spanish day6_un_59a6b536:


    sl "¿Estás de mal humor?"


translate spanish day6_un_997c151d:


    me "Un poco."


translate spanish day6_un_e4d522d4:


    sl "¿Pasó algo malo?"


translate spanish day6_un_b97e3fd5:


    me "No realmente..."


translate spanish day6_un_87297b64:


    sl "No tienes porqué contarme si no quieres."


translate spanish day6_un_cbce3f49:


    me "No hay nada que contarte."


translate spanish day6_un_6488c1ac:


    "Continuamos comiendo en silencio hasta que pregunté:"


translate spanish day6_un_79ee911e:


    me "¿Adónde fuiste anoche?"


translate spanish day6_un_e1994127:


    sl "¿Yo? Oh solo quería estar sin compañía por un rato."


translate spanish day6_un_4a2c94f6:


    me "Eso no es propio de ti."


translate spanish day6_un_f59ecd73:


    "Me sentí un poco mas animado."


translate spanish day6_un_c6212685:


    sl "¿En serio? Bueno, tal vez...{w} Tampoco es común verte tan melancólico."


translate spanish day6_un_0139b861:


    th "Puede que tenga razón."


translate spanish day6_un_d560542b:


    "Inclusive en las situaciones más desfavorables siempre he tratado de tener una actitud positiva."


translate spanish day6_un_e4c10516:


    "No es que fuera un optimista, solo intentaba no sentirme muy deprimido."


translate spanish day6_un_24507d3d:


    "Recordando mi antigua vida, simplemente es natural... ni bien dejas que la depresión te tome, la horca empieza a parecer muy atractiva."


translate spanish day6_un_6ab05865:


    me "Quizás."


translate spanish day6_un_0565c647:


    "Para cuando Slavya terminó de comer yo aun seguía comiendo las gachas con la cuchara."


translate spanish day6_un_d23277e3:


    sl "Me voy."


translate spanish day6_un_e4eadbc7:


    me "Por cierto, ¿has visto a Lena?"


translate spanish day6_un_e00071aa:


    sl "Nop, ¿por qué preguntas?"


translate spanish day6_un_2283b18c:


    me "No la vi esta mañana en la formación.{w} Eso es raro... No es algo que ella haría."


translate spanish day6_un_5d832dbe:


    sl "Tal vez tengas razón.{w} No creo que sea un asunto muy importante para ser honesta."


translate spanish day6_un_be2f234f:


    me "Sí, por supuesto, solo me preguntaba..."


translate spanish day6_un_2da87938:


    "Me senté en mi lugar por unos minutos más y luego me dirigí afuera sin terminar mi desayuno."


translate spanish day6_un_fdfb7569:


    th "Hoy está bastante gris.{w} El primer día nublado desde que llegué aquí.{w} Ya me había acostumbrado un poco al sol ardiente y brillante."


translate spanish day6_un_f69f6bef:


    "El calor que solo disminuiría al anochecer parecía ser un componente irremplazable de este lugar."


translate spanish day6_un_d29d26f8:


    "Parece que el tiempo cambia hasta en este lugar después de todo."


translate spanish day6_un_d31c084d:


    th "Casi como si estuviera coincidiendo con mi humor..."


translate spanish day6_un_df21ce17:


    th "Es como si fuera tan sólo más evidencia de que este lugar o sus creadores son conscientes y tienen grandes habilidades para contar historias."


translate spanish day6_un_b83c2795:


    "A veces solo quería concentrarme en una teoría; concentrarme en ella y olvidar todas las demás."


translate spanish day6_un_6752541e:


    "Decidir que todo es obra de alienígenas o que estoy en un universo paralelo.{w} O hechicería o experimentos militares..."


translate spanish day6_un_f82edbaf:


    "¡Solo elegir una y acabar con esto!"


translate spanish day6_un_89aa71e4:


    "Parar de pensar en todas las posibles explicaciones de esta situación, constantemente pasando de una a la otra.{w} Sólo concentrarme en una."


translate spanish day6_un_78520b75:


    th "Pero es imposible."


translate spanish day6_un_4b4dd244:


    th "(Casi) no tengo hechos sobre nada.{w} Nada extraordinario me ha ocurrido aquí todavía."


translate spanish day6_un_a0795afe:


    "Sí, algunas cosas extrañas pasaron. Pero pueden pasar y pasan en el mundo real también."


translate spanish day6_un_35362687:


    "Luego de otro ciclo de pensamientos similares me encontré en la parada del autobús de la línea 410 y las puertas del campamento Sovyonok."


translate spanish day6_un_4aa5ce10:


    th "Sin respuestas, sin indicios, sin pistas..."


translate spanish day6_un_4fa5905f:


    "Caminaba adonde fuera que mis piernas me llevaran."


translate spanish day6_un_f9c294f6:


    "Había silencio en el area residencial.{w} Para ser precisos, no había ni una persona allí."


translate spanish day6_un_6a121a11:


    "Mi asombro creció aun más cuando Electronik apareció a la vuelta de la esquina."


translate spanish day6_un_a3828cd7:


    "Quería llamarlo pero me detuve, porque estaba yendo un poco demasiado seguro en una dirección desconocida."


translate spanish day6_un_49160043:


    th "Es extraño, eso va en contra de su naturaleza."


translate spanish day6_un_4fa01f29:


    th "De todas formas, ¿de qué puedo hablar con Electronik?{w} Y sí, tendría que iniciar una conversación primero..."


translate spanish day6_un_7e840313:


    th "Parecía que estaba completamente desesperanzado."


translate spanish day6_un_42f4c858:


    "Sin embargo, sería interesante seguirlo y averiguar dónde está yendo tan apurado."


translate spanish day6_un_de5de174:


    "Cuando era niño me gustaban los juegos de espías, ¡y aquí está mi oportunidad de ponerme en los pies de un espía real!"


translate spanish day6_un_5a5a09fe:


    "Decidí no escabullirme ni esconderme de manera especial y sólo lo seguí silenciosamente."


translate spanish day6_un_6b9662bf:


    "Pronto llegamos a la biblioteca.{w} Electronik llamó y entró."


translate spanish day6_un_ad78315b:


    "Me paré detrás de un árbol grande para que nadie pudiera verme y empecé a esperar."


translate spanish day6_un_a20cefa7:


    "..."


translate spanish day6_un_c1a2fbe8:


    "Estuvo ausente por un largo tiempo."


translate spanish day6_un_2cee12ba:


    th "Quizás ésta fue una idea estúpida, porque de hecho, ¿qué importaba si había ido a la biblioteca?{w} ¿Tal vez sólo fue a conseguir algo para leer?"


translate spanish day6_un_41a8466c:


    th "¿Caminaba rápido? ¿Y qué?"


translate spanish day6_un_539d4165:


    th "Tal vez tenga algún asunto después..."


translate spanish day6_un_5b0195d4:


    "El golpe fuerte de una puerta interrumpió mis pensamientos."


translate spanish day6_un_66094c34:


    "Miré hacia la biblioteca y vi a Electronik, escapando de allí, y a Zhenya, quien corría tras él, gritando:"


translate spanish day6_un_07d3d267:


    mz "¡Ya no quiero oírlo! ¡Ni tampoco verte a ti!"


translate spanish day6_un_d825c319:


    "Me pasaron corriendo. Pero estaban tan concentrados en la persecución que, naturalmente, no me vieron."


translate spanish day6_un_17db73bb:


    "Toda la situación parecía muy graciosa, y decidí que tenía que averiguar de qué se trataba."


translate spanish day6_un_8bbec564:


    th "Me pregunto, ¿hacia dónde podría Electronik estar corriendo de esa manera?"


translate spanish day6_aidpost_5f9ed739:


    "¿Tal vez corría hacia la enfermería?"


translate spanish day6_aidpost_ceb48884:


    th "No, esa es obviamente una idea tonta..."


translate spanish day6_dinner_e22dfa0f:


    "Esconderse en la cantina no sería una mala decisión."


translate spanish day6_dinner_78f58283:


    "Sin embargo, tampoco estaba aquí."


translate spanish day6_after_map_e40ce72e:


    th "Sí, es bastante obvio que corrió directamente hacia su nativo club de cibernética."


translate spanish day6_after_map_fcae6aa1:


    "Entré sin llamar pero no encontré a nadie adentro."


translate spanish day6_after_map_249528f8:


    me "Electronik, ¡Soy yo!"


translate spanish day6_after_map_71f484c6:


    "El sonido de pisadas provino de la habitación contigua y pronto el mismo Electronik apareció."


translate spanish day6_after_map_13e9141f:


    el "¡Hola Semyon! Sólo estoy..."


translate spanish day6_after_map_930b325a:


    "Sus ojos corrían culpablemente y su camisa tenía signos visibles de sudor."


translate spanish day6_after_map_1e590c6b:


    me "Veo que estás practicando deportes.{w} Carreras."


translate spanish day6_after_map_3573e50d:


    el "Yo...{w} ¿Viste eso?"


translate spanish day6_after_map_594616b0:


    "Preguntó nefastamente."


translate spanish day6_after_map_ccf2f1f7:


    me "Sí, puramente por coincidencia. Así que decidí pasar para preguntar cómo estabas y qué estaba pasando..."


translate spanish day6_after_map_603882a4:


    el "Nada en especial, de verdad..."


translate spanish day6_after_map_61b29886:


    th "Ciertamente, escapar de una bibliotecaria enojada... no es nada..."


translate spanish day6_after_map_0a1f52d0:


    me "Puedes contarme."


translate spanish day6_after_map_260966c7:


    "Sonreí encantadoramente."


translate spanish day6_after_map_e53cf926:


    el "¿De verdad? Y no le dirás..."


translate spanish day6_after_map_88f39b4b:


    me "¡Por supuesto! ¡Tan callado como los muertos! ¡Lo juro!"


translate spanish day6_after_map_8636977a:


    "Pensé que había exagerado un poco ahí, pero parece que lo había convencido."


translate spanish day6_after_map_ab8f7115:


    el "Está bien."


translate spanish day6_after_map_a4e10992:


    "Aspiró profundamente, reuniendo fuerzas."


translate spanish day6_after_map_4b01cbdb:


    el "Sabes, me ha gustado Zhenya desde el primer día..."


translate spanish day6_after_map_3763685c:


    "Al escuchar esas palabras quería tirarme al suelo y rodar, siendo sacudido por violentos ataques de risa, pero me contuve por respeto a él. Aun así, sus siguientes palabras no entraron por mis oídos."


translate spanish day6_after_map_7d9d9a52:


    el "...así que eso decidí. Y bueno, ya viste que pasó..."


translate spanish day6_after_map_58f9de3f:


    th "Un sujeto valiente, ciertamente."


translate spanish day6_after_map_b8456c80:


    me "Bueno, tú...{w} ¡Inténtalo con mas empeño y supongo que la próxima vez tendrás éxito!"


translate spanish day6_after_map_1a0d5f6e:


    "Le di una palmada en el hombro, intentando seriamente no reír."


translate spanish day6_after_map_3ebcd212:


    el "Gracias por apoyarme."


translate spanish day6_after_map_d5ffee81:


    "Sonrió tristemente."


translate spanish day6_after_map_3b5c973e:


    me "De acuerdo, tengo que irme. Tengo trabajo que hacer."


translate spanish day6_after_map_6e47a0d1:


    "Salí disparado del edificio de clubs y finalmente me reí a carcajadas."


translate spanish day6_after_map_a3fe05d4:


    th "Sin embargo, si lo piensas bien, Electronik y Zhenya harían una pareja maravillosa."


translate spanish day6_after_map_5ea74e09:


    th "De hecho son una pareja perfecta.{w} Es extraño que Zhenya lo haya rechazado..."


translate spanish day6_after_map_9c84def6:


    "Iba hacia la plaza pensando en este episodio."


translate spanish day6_after_map_4c7202cc:


    th "Después de todo, Electronik no es tan simplón..."


translate spanish day6_after_map_27837c94:


    th "Así nomás, declaró su amor, inclusive dándose cuenta que el rechazo era inevitable.{w} O por el contrario, ¿será porque es tan simple como una tarta de manzana?"


translate spanish day6_after_map_b874d159:


    th "De cualquier forma, puedes sentir la honestidad y la sinceridad en su comportamiento."


translate spanish day6_after_map_d0e49694:


    th "Gente más complicada pasaría horas, días, meses, años pensando cómo presentar esto de una forma mejor, qué consecuencias tendría y si deberías siquiera molestarte.{w} Yo haría eso."


translate spanish day6_after_map_16546c05:


    th "No, más bien, he hecho eso."


translate spanish day6_after_map_604e72aa:


    th "Pero él simplemente lo dijo.{w} Sin éxito por supuesto, pero podría haber terminado diferente..."


translate spanish day6_after_map_69934023:


    "Estos pensamientos me pusieron completamente melancólico, tan así que la señal para la comida, usualmente tan anticipada, no despertó ninguna emoción en mí."


translate spanish day6_after_map_db39b8db:


    "La cantina estaba que reventaba.{w} Parece que el campamento estaba emergiendo de la depresión matutina lentamente."


translate spanish day6_after_map_583f6bbf:


    "Tal vez el sol que había salido de entre las nubes contribuyó a eso, o tal vez algo más que me había perdido cuando corría tras Electronik."


translate spanish day6_after_map_252e9ca8:


    "Resultó que sólo los lugares al lado de Ulyana y Alisa estaban libres."


translate spanish day6_after_map_09b90b56:


    "Me preparé y empecé a caminar hacia ellas; comer todavía era una necesidad."


translate spanish day6_after_map_ec04d37b:


    me "¿Puedo sentarme con vosotras?"


translate spanish day6_after_map_b4f1aecf:


    dv "Oh solo siéntate."


translate spanish day6_after_map_57fa2035:


    us "¿Por qué estás tan triste?"


translate spanish day6_after_map_35ce6c29:


    me "Es sólo que tú eres demasiado alegre.{w} Estoy intentando mantener el balance de la energía en el universo."


translate spanish day6_after_map_a064eabe:


    "Ulyana hizo una risita."


translate spanish day6_after_map_8c8bd74b:


    "Extrañamente, las muchachas no me prestaron atención y hablaron sobre sus propios temas."


translate spanish day6_after_map_cc02e7a3:


    "Al principio pensé que era algo bueno, pero luego empecé a pensar que no se estaban fijando en mí."


translate spanish day6_after_map_2b3338c6:


    me "Por cierto, ¿dónde está Lena?"


translate spanish day6_after_map_11255df4:


    "A causa de mi reflexión me había olvidado de ella por completo."


translate spanish day6_after_map_3e69c35f:


    dv "No lo sé."


translate spanish day6_after_map_d4d17245:


    "Alisa contestó distraída."


translate spanish day6_after_map_84eb34c3:


    me "¿Aun no ha aparecido?"


translate spanish day6_after_map_f355226b:


    dv "Como puedes ver..."


translate spanish day6_after_map_3fd95c7c:


    "Miré alrededor de la cantina, pero no pude verla por ningún lado."


translate spanish day6_after_map_9fcb4846:


    me "¿Y nadie ha oído nada sobre ella?"


translate spanish day6_after_map_e8936f84:


    dv "No."


translate spanish day6_after_map_ae30c1d7:


    me "¿No te parece extraño?"


translate spanish day6_after_map_c1788a1d:


    us "¿Qué es tan extraño?{w} Tal vez esté leyendo, quizás durmiendo o algo."


translate spanish day6_after_map_01b5873a:


    me "Parece más como si estuvieras hablando sobre ti misma."


translate spanish day6_after_map_f3820ec1:


    dv "No tiene nada que ver contigo, ¿o si?"


translate spanish day6_after_map_c501a590:


    "Alisa interrumpió enojada."


translate spanish day6_after_map_61d482ed:


    me "Bueno, si una persona desapareció..."


translate spanish day6_after_map_37715109:


    dv "¡No estabas tan preocupado cuando Shurik desapareció!"


translate spanish day6_after_map_74053528:


    me "Eso es diferente..."


translate spanish day6_after_map_0e2114f3:


    dv "¿Sí? ¿Y por qué, me pregunto?"


translate spanish day6_after_map_1b8b414c:


    "No tenía una respuesta y simplemente miré perplejo a Alisa antes de volver a mi comida."


translate spanish day6_after_map_517d0fc9:


    "Ella no insistió en seguir con la conversación."


translate spanish day6_after_map_a20cefa7:


    "..."


translate spanish day6_after_map_b686c259:


    "El almuerzo había terminado. Me puse de pie silenciosamente y salí de la cantina sin despedirme."


translate spanish day6_after_map_9d0e81e1:


    th "Así que, Lena desapareció...{w} ¿Qué debería hacer ahora?"


translate spanish day6_after_map_be3ae4b1:


    th "Por otro lado, ¿por qué debería hacer algo?{w} ¿Por qué yo?{w} ¿Dónde estoy y por qué estoy aquí? ¿Quiénes son todas estas personas?"


translate spanish day6_after_map_25de3652:


    th "Uno no puede estar absolutamente seguro de que Lena sea lo que parece ser."


translate spanish day6_after_map_eeb3cd87:


    th "Tal vez todo esto ni siquiera existe, ¿así que por qué debería preocuparme...?"


translate spanish day6_after_map_fd2a5fac:


    th "Sin embargo, para mí, Lena todavía es Lena.{w} La muchacha modesta y callada que conocí el primer día."


translate spanish day6_after_map_7edba5d0:


    th "E incluso su extraño comportamiento no podría cambiar mi actitud hacia ella."


translate spanish day6_after_map_22235f9d:


    th "Al final, no es seguro que yo exista.{w} Así que aunque este mundo sea lógico (al menos hasta cierto punto), ¡tengo que aprovechar sus reglas!"


translate spanish day6_after_map_fd5f28b2:


    "Rápidamente fui a la cabaña de la líder del campamento."


translate spanish day6_after_map_c028139c:


    "Una vez dentro, vi a Olga Dmitrievna recostada en la cama y leyendo un libro."


translate spanish day6_after_map_1246e12e:


    me "¿Sabes dónde está Lena?"


translate spanish day6_after_map_017acf74:


    mt "No, ¿por qué preguntas?"


translate spanish day6_after_map_07ba9e65:


    me "No la encuentro en ningún lugar. ¡Faltó tanto al desayuno como a la comida!"


translate spanish day6_after_map_9dd49ab7:


    mt "¿Y qué?"


translate spanish day6_after_map_4de002b5:


    "Me miró perpleja."


translate spanish day6_after_map_1c44d4fc:


    me "¿Qué quieres decir con «y qué»?{w} Cuando Shurik desapareció todo el campamento estaba buscándolo desde la mañana."


translate spanish day6_after_map_fe72fec3:


    mt "No te entiendo."


translate spanish day6_after_map_6fa16493:


    "Algo extraño le sucede a la líder del campamento otra vez.{w} Se comportaba de forma absolutamente incomprensible, ilógica."


translate spanish day6_after_map_ad45a807:


    me "¿Piensas que es normal? ¿Y dónde está ahora?"


translate spanish day6_after_map_9c19b5ad:


    mt "No lo sé."


translate spanish day6_after_map_53405852:


    "Olga Dmitrievna respondió con calma."


translate spanish day6_after_map_b8029b05:


    me "¡Esto es demasiado!"


translate spanish day6_after_map_6ea78a01:


    "Empecé a perder la calma."


translate spanish day6_after_map_8009c859:


    mt "Pregúntale a Miku, ella es su compañera de habitación después de todo."


translate spanish day6_after_map_c6b76c70:


    th "Esa era una buena idea, porque obviamente no conseguiría más respuestas aquí."


translate spanish day6_after_map_b424b5bf:


    "Salí, cerré la puerta de un golpe y fui a buscar la cabaña de Miku y Lena."


translate spanish day6_after_map_b83fcbd4:


    "Fue algo bueno que la muchacha orquestra me haya dicho dónde vive, ya que gracias a ello estaba frente a la puerta de su cabaña un minuto después."


translate spanish day6_after_map_df84c43e:


    "Debería haber llamado, pero por alguna razón no pude."


translate spanish day6_after_map_3bda9055:


    "Luego de respirar profundamente un par de veces, llamé a la puerta varias veces."


translate spanish day6_after_map_c7ed3d71:


    mi "¡Entra!"


translate spanish day6_after_map_3417c9cb:


    "Oí una voz familiar."


translate spanish day6_after_map_7299645f:


    me "Hola... ¿Sabes dónde está Lena?"


translate spanish day6_after_map_02794ad5:


    mi "No... No la he visto hoy. La estás buscando, ¿no?"


translate spanish day6_after_map_374cfeae:


    me "¿No crees qué es extraño?"


translate spanish day6_after_map_9d504cc9:


    "Para ese momento había comenzado a sospechar de que todos escondían información sobre el paradero de Lena, de una conspiración, de estar involucrados en mi estancia aquí, del asesinato de Kennedy y de cientos de otras cosas terribles."


translate spanish day6_after_map_2f93b124:


    mi "Bueno... Ya sabes, tal vez... Pensé que había ido a algún lugar y entonces me perdí haciendo cosas; el desayuno, el club de música, ayudar a limpiar, y luego el almuerzo, y luego, y luego..."


translate spanish day6_after_map_f7d66ec2:


    me "Está bien, ya veo... ¿Y qué hay de ayer? ¿Todo estaba normal?"


translate spanish day6_after_map_e9cf8521:


    mi "Bueno... Llegó tarde e inmediatamente se fue a la cama, no noté nada mal."


translate spanish day6_after_map_86efd052:


    "No hay posibilidad de averiguar algo aquí tampoco."


translate spanish day6_after_map_3066598f:


    me "Gracias."


translate spanish day6_after_map_d9b28d3a:


    "Dije abruptamente y me fui."


translate spanish day6_after_map_6130e681:


    "En aquel momento me pareció que la perdida Lena era la única persona viva en este paquete de muñecos parlantes, ¡y tenía que encontrarla!"


translate spanish day6_after_map_e739b113:


    "Sin embargo, parecía imposible hacer eso por mi cuenta, así que fui a conseguir ayuda."


translate spanish day6_after_map_a35b54c4:


    th "¿Quién estaría mas dispuesto a ayudarme?{w} ¡Por supuesto, Slavya!"


translate spanish day6_after_map_a328c184:


    "Supuse que estaría dedicándose a limpiar algo.{w} Por ejemplo, la plaza."


translate spanish day6_after_map_b89225c8:


    "Así que fui allí."


translate spanish day6_after_map_15d046bb:


    "Mi sexto sentido no me decepcionó."


translate spanish day6_after_map_1a8f3935:


    me "¡Hola!"


translate spanish day6_after_map_9104ac12:


    sl "¡Hola!"


translate spanish day6_after_map_2c385c8f:


    me "¿Has visto a Lena?"


translate spanish day6_after_map_e00071aa:


    sl "No. ¿Por qué lo preguntas?"


translate spanish day6_after_map_c72e7da6:


    me "Nadie la ha visto desde esta mañana. Estuvo ausente durante el desayuno y también durante la comida."


translate spanish day6_after_map_cffc6886:


    sl "Qué extraño."


translate spanish day6_after_map_ec0bd519:


    me "También pienso que todo esto es, cuanto menos, extraño.{w} ¿Puedes ayudarme a encontrarla?"


translate spanish day6_after_map_ce25ce10:


    sl "Oh, lo siento.{w} ¿Quizás luego? Tengo que terminar de limpiar aquí..."


translate spanish day6_after_map_19e9e137:


    "Fue como el golpe de un rayo."


translate spanish day6_after_map_96afba80:


    "Di un par de pasos torpes hacia atrás y salí corriendo de allí."


translate spanish day6_after_map_235cdb53:


    th "¡No, esa no era ella! ¡Es como si alguien la hubiera reemplazado!{w} No sólo a ella sino a todos los habitantes de este campamento."


translate spanish day6_after_map_7012273e:


    th "¿Qué está sucediendo?{w} Lo más extraño es que no tiene nada que ver conmigo, ¡sino con Lena!"


translate spanish day6_after_map_e760a29d:


    th "¿Tal vez llegó aquí de la misma manera que yo?"


translate spanish day6_after_map_83f4ce09:


    th "¡Exacto!{w} Ésa podría ser la razón por la cual está callada la mayor parte del tiempo."


translate spanish day6_after_map_105dafff:


    th "No, espera...{w} ¿Qué hay del hecho de que ella conoce a Alisa...?"


translate spanish day6_after_map_bf8e6d5c:


    th "No, algo no encaja."


translate spanish day6_after_map_cb18b97c:


    "Mi cabeza iba a explotar, y empecé a sofocarme."


translate spanish day6_after_map_915d90ea:


    "Después de recuperar el aliento miré a mi alrededor y me encontré en la parada del autobús. Me senté en el bordillo y me cubrí la cara con las manos."


translate spanish day6_after_map_cbafac0b:


    th "Si antes nada dependía de mí, pero iba relativamente fluído, entonces ahora estaba, como siempre, indefenso, pero la situación era completamente diferente."


translate spanish day6_after_map_a980985a:


    th "Mirar la batalla siendo combatida desde un costado sin ninguna amenaza a tu vida y estar en el área de conflicto siendo incapaz de ayudar a aquellos que te importan son dos cosas diferentes."


translate spanish day6_after_map_a20cefa7_1:


    "..."


translate spanish day6_after_map_fff50899:


    "Tan sólo estaba sentado."


translate spanish day6_after_map_f3fefe18:


    "El tiempo transcurría y el sol comenzó a ponerse."


translate spanish day6_after_map_62739121:


    th "Probablemente la cena ya había comenzado."


translate spanish day6_after_map_3edce0f9:


    th "Aunque qué más da, todavía no quiero comer."


translate spanish day6_after_map_41cc0c56:


    "Me puse en pie y caminé arduamente de vuelta al campamento con las piernas flojas."


translate spanish day6_after_map_7b58f3a3:


    th "Como siempre, la único que queda es esperar."


translate spanish day6_after_map_a4552563:


    "Decidí ir a la playa."


translate spanish day6_after_map_a2ad895b:


    th "Cuando todo el mundo estaba cenando, me podría sentar ahí tranquilamente y ponerme a reflexionar."


translate spanish day6_after_map_4bbcd690:


    th "Aunque, ¿sobre qué podría pensar? ¿Y cuánto?"


translate spanish day6_after_map_6de0d6ca:


    "Sin embargo, mi momento de tranquilidad fue estropeado."


translate spanish day6_after_map_9fc97989:


    "En la playa me encontré con Zhenya, la cual me sorprendió mucho."


translate spanish day6_after_map_eb39fdd5:


    me "¿Tú también vienes aquí?"


translate spanish day6_after_map_47641ee9:


    "Ella me observó a través de sus gafas."


translate spanish day6_after_map_43937691:


    mz "¿Acaso te piensas que no soy un ser humano?"


translate spanish day6_after_map_d9f75732:


    me "No, no quise decir eso."


translate spanish day6_after_map_ed1dc0ad:


    mz "¿Pues entonces qué quieres decir?"


translate spanish day6_after_map_562a22e9:


    me "Nada...{w} Por cierto, ¿has visto a Lena?"


translate spanish day6_after_map_c48a46a8:


    mz "No."


translate spanish day6_after_map_f2e007f0:


    me "Ya veo..."


translate spanish day6_after_map_4e3a8dcb:


    mz "¿Qué es lo que quieres de ella?"


translate spanish day6_after_map_2dfb23fa:


    me "Bueno... Nadie le ha visto desde ayer por la noche."


translate spanish day6_after_map_e8b5756a:


    mz "¿Crees que alguien se puede perder en este campamento?"


translate spanish day6_after_map_56d4e39c:


    "Se rio a carcajadas."


translate spanish day6_after_map_cf3d96a9:


    me "Shurik lo consiguió..."


translate spanish day6_after_map_a4dfd52d:


    mz "Ese fue un caso especial...{w} Esos tipos son como dos gotas de agua, nunca sabes qué te puedes esperar de ellos."


translate spanish day6_after_map_6ebc2860:


    "Inmediatamente me acordé del incidente matutino."


translate spanish day6_after_map_340d0570:


    me "Dime...{w} ¿Por qué estabas persiguiendo a Electronik esta mañana?"


translate spanish day6_after_map_9889b720:


    "Zhenya se puso incómoda."


translate spanish day6_after_map_63dcdb44:


    mz "No es asunto tuyo."


translate spanish day6_after_map_efa19ecb:


    me "Solamente preguntaba."


translate spanish day6_after_map_54df90eb:


    mz "¡Porque es un idiota!"


translate spanish day6_after_map_36d3e865:


    "Se dio la vuelta tras estas palabras."


translate spanish day6_after_map_33d14fd3:


    me "Quizá no deberías ser tan estricta con él."


translate spanish day6_after_map_82514e53:


    "Yo tampoco tenía muy claro si es que estaba defendiendo a Electronik, o que simplemente quería continuar con la conversación."


translate spanish day6_after_map_16cd0044:


    mz "Y bien, ¿cómo debería tratarle?"


translate spanish day6_after_map_5f7ad39e:


    me "Bueno, dale una oportunidad...{w} Ya sabes, semejante acto requiere mucho coraje."


translate spanish day6_after_map_b2adba60:


    mz "Lo dices... como si fuera algo especial, algún tipo de logro.{w} ¿Serías capaz de hacerlo tú mismo?"


translate spanish day6_after_map_846fcae4:


    "Me lo pensé y contesté tras un rato."


translate spanish day6_after_map_e05aaca3:


    me "No lo sé...{w} No tuve todavía el momento idóneo."


translate spanish day6_after_map_a9c3105e:


    mz "Espero que lo tengas pronto."


translate spanish day6_after_map_a7ce299f:


    "Dijo Zhenya bruscamente y se fue caminando en dirección a la cantina."


translate spanish day6_after_map_a6b3e5a5:


    "Me senté en la arena y me puse a pensar."


translate spanish day6_after_map_e55707cf:


    th "Eso es cierto, Electronik era capaz de decirlo, ¿pero acaso podía hacer yo lo mismo?{w} Ésa es la cuestión, una gran cuestión."


translate spanish day6_after_map_182cea07:


    th "Si tuviera el momento adecuado...{w} ¿Pero cuál y cuándo?"


translate spanish day6_after_map_df7d6c66:


    th "Siempre es más fácil pensar en algo efímero, estar preparado para docenas de posibles situaciones, de predecir los siguientes acontecimientos varios pasos por delante."


translate spanish day6_after_map_20124702:


    th "Pero la mayor parte de veces, todo acaba yendo de forma distinta."


translate spanish day6_after_map_7fad78b3:


    th "Incluso el más mínimo hecho puede ser suficiente para dar al traste con tus planes."


translate spanish day6_after_map_04ab0520:


    th "Y si no estás preparado para hacer {i}eso{/i} en cualquier instante, cuando sea, bajo cualquier circunstancia, si sólo estás preparado para cuando todo está exactamente como habías esperado, es improbable que logres algo que valga la pena."


translate spanish day6_after_map_ab503b46:


    th "Por tanto, la única respuesta correcta a la pregunta de Zhenya era un «no»."


translate spanish day6_after_map_60b33880:


    th "No uno ambiguo, ni inseguro, ni poco entusiasta o una respuesta forzada.{w} Sencillamente un «no»."


translate spanish day6_after_map_7889693d:


    th "Sólo había un simple «sí» y un simple «no»."


translate spanish day6_after_map_f4bb7bbe:


    "Siempre es difícil para mi entender eso."


translate spanish day6_after_map_90e26c1c:


    "Entre ambos extremos siempre he ido con un gran número de diferentes respuestas como «tal vez», «quizás», «posiblemente si» y «no estoy seguro pero lo intentaré»..."


translate spanish day6_after_map_a20cefa7_2:


    "..."


translate spanish day6_after_map_e51168a0:


    "Estaba tan absorbido por mis propios pensamientos que no me di cuenta de cómo la oscuridad se apoderó del campamento."


translate spanish day6_after_map_3aa3c67c:


    "Bajo otras circunstancias ya me habría ido a dormir...{w} Pero buscar a Lena por la noche no fue una buena idea."


translate spanish day6_after_map_d8482e89:


    "Me levanté y lentamente vagué sin rumbo."


translate spanish day6_after_map_c84711ef:


    "Pronto, el camino me llevó hasta la pista de deportes."


translate spanish day6_after_map_97ec2962:


    "Me quedé quieto ahí por un rato, y estuve a punto de irme cuando escuché ruidos."


translate spanish day6_after_map_cfc15ee6:


    th "Un ruido seco, otro ruido seco...{w} ¡Algo extremadamente familiar!"


translate spanish day6_after_map_32a97e95:


    "Salí corriendo hacia la cancha de voleibol y vi a Lena, quien estaba tratando de golpear el volante de bádminton con su raqueta sin éxito."


translate spanish day6_after_map_59811faa:


    "Me quedé trastornado por un largo rato."


translate spanish day6_after_map_550358a1:


    "Mi cabeza estaba completamente vacía, tan sólo la miraba y sentía una emoción de alegría.{w} Júbilo por encontrarla, alegría por verla de nuevo."


translate spanish day6_after_map_f86b9a25:


    "Finalmente recuperé mis sentidos y decidí aproximarme..."


translate spanish day6_after_map_d092d462:


    "Pero tras un par de pasos, me detuve."


translate spanish day6_after_map_cf3bc831:


    th "¿Y qué le digo ahora?{w} «¿Qué alegría que te encontré?»{w} «¿Dónde has estado, estaba preocupado?»"


translate spanish day6_after_map_de715a93:


    th "Tras la conversación de ayer es muy improbable que quiera verme."


translate spanish day6_after_map_e0f727b2:


    th "¿Y si Lena me pregunta por qué la estaba buscando?{w} ¿Por qué estaba preocupado?"


translate spanish day6_after_map_4baf4e5a:


    th "Ni tan siquiera lo sé yo...{w} Probablemente sea porque ella estuvo ausente demasiado rato."


translate spanish day6_after_map_db5327a9:


    th "Tal vez si no hubiera sido ella, sino otra persona que conociera, habría estado preocupado de la misma forma."


translate spanish day6_after_map_9e166d2c:


    th "Quizá si ayer me hubiera comportado de forma distinta, ella no habría desaparecido hoy."


translate spanish day6_after_map_b1d2df25:


    th "No estoy seguro, pero trataré de pensar en algo más adecuado..."


translate spanish day6_after_map_5c5017b8:


    "Di otro paso y me volví a detener."


translate spanish day6_after_map_36e92b6c:


    th "Probablemente, tal vez, quizás, no estoy seguro..."


translate spanish day6_after_map_fac2cdca:


    th "Una y otra vez, surgían estas palabras en mi mente, en mi vida..."


translate spanish day6_after_map_ee8a9b71:


    th "¡Inconscientemente, sin mi voluntad!"


translate spanish day6_after_map_10d6f972:


    th "¿Pero por qué? ¿Para qué propósito?"


translate spanish day6_after_map_6179f9a3:


    th "¡Tengo que tomar una decisión! ¡De una vez por todas!"


translate spanish day6_after_map_beab36a3:


    th "Aunque...{w} Hay dos sencillas palabras... «sí» y «no»."


translate spanish day6_after_map_bc795975:


    "¡Al fin todo está claro!"


translate spanish day6_after_map_40942432:


    "Fui a la cancha, me aproximé a Lena, le sonreí y le dije:"


translate spanish day6_after_map_1a8f3935_1:


    me "¡Ey!"


translate spanish day6_after_map_f0d43f46:


    "Ella se giró y me miró."


translate spanish day6_after_map_e8479859:


    un "Hola..."


translate spanish day6_after_map_0974d8de:


    me "Estuviste desaparecida todo el día."


translate spanish day6_after_map_abad00d7:


    un "Sí... Estuve paseando por ahí."


translate spanish day6_after_map_158d2b03:


    "Lo decía todo con tranquilidad y calma sin ápice de vergüenza o timidez.{w} De hecho, sin emoción alguna."


translate spanish day6_after_map_9cac764a:


    me "Estábamos preocupados por ti."


translate spanish day6_after_map_a9e86fe4:


    "A pesar de que el único que se preocupó fui yo."


translate spanish day6_after_map_334f3921:


    un "No deberías."


translate spanish day6_after_map_696f7e51:


    me "Pero no puedes....{w} ¡No puedes simplemente desaparecer de esta forma!"


translate spanish day6_after_map_f1268435:


    "Traté de sonreír haciendo que mis palabras no sonaran como un reproche."


translate spanish day6_after_map_608904a9:


    un "No creo que nadie se preocupe por eso."


translate spanish day6_after_map_d1150751:


    me "Yo sí me preocupo."


translate spanish day6_after_map_b672514d:


    un "¿Por qué?"


translate spanish day6_after_map_24fb8611:


    "Pude entrever sorpresa en sus ojos."


translate spanish day6_after_map_e8345567:


    me "Porque... ¡Porque no está bien!"


translate spanish day6_after_map_d982cf8e:


    "No importa cuánto tratara de ser lo más honesto posible, acabaría saliendo mal."


translate spanish day6_after_map_4738c0dc:


    un "¡Ah! Ya veo...{w} Vale, no lo haré otra vez."


translate spanish day6_after_map_0f363f3f:


    "Parecía que esta conversación no le interesara a ella."


translate spanish day6_after_map_ec43a852:


    "El silencio se hizo."


translate spanish day6_after_map_615bc5a9:


    "Solamente no sabía qué otra cosa decirle, además Lena parecía bastante conforme con el silencio."


translate spanish day6_after_map_6215010e:


    me "¿Eres buena?"


translate spanish day6_after_map_d0fd1810:


    "Dije al fin, señalando la raqueta."


translate spanish day6_after_map_15f9eed8:


    un "No mucho..."


translate spanish day6_after_map_6bc45b3c:


    me "Si quieres yo..."


translate spanish day6_after_map_8fa9fd07:


    un "No, no quiero."


translate spanish day6_after_map_afff794c:


    "Ella caminó hasta el banco y dejó la raqueta y el volante de bádminton."


translate spanish day6_after_map_505d91ae:


    me "Vale..."


translate spanish day6_after_map_7e92ea41:


    "A decir verdad, no me esperaba esa respuesta."


translate spanish day6_after_map_3cebee2a:


    un "¿Quieres que observemos las estrellas juntos?"


translate spanish day6_after_map_3b03af3a:


    me "Sí, por supuesto..."


translate spanish day6_after_map_76efef38:


    "Me senté a su lado."


translate spanish day6_after_map_a39c7cb2:


    "Lena contemplaba atentamente el cielo."


translate spanish day6_after_map_67f740f6:


    "Montones de pequeñas luces brillaban por encima mío. Algunas eran más brillantes, otras eran apenas visibles."


translate spanish day6_after_map_e0e85a4c:


    "Nunca comprendí realmente qué le ve la gente en sentarse y mirar las estrellas."


translate spanish day6_after_map_f99d691b:


    "Después de todo, aquí en la Tierra solamente son unos pequeños puntos de luz y es improbable que la gente sepa qué tipo de cuerpos celestes son, así como su tamaño y lo lejos que se encuentran respecto de nosotros."


translate spanish day6_after_map_543454aa:


    "Desde luego, es muy romántico simplemente disfrutar de la luz de las lejanas estrellas, pero para mí era como estar mirando una pared de tochos. También tiene sus huecos, sus irregularidades en el trabajo de albañilería y su adorno en la piedra."


translate spanish day6_after_map_e767efc0:


    "Un cielo estrellado en miniatura."


translate spanish day6_after_map_6ff78afb:


    me "¿Y qué ves por ahí?"


translate spanish day6_after_map_1663efd6:


    un "Estrellas..."


translate spanish day6_after_map_60e76402:


    "Dijo ella misteriosamente con su cabeza erguida."


translate spanish day6_after_map_cdb6a705:


    me "Sí, lo sé, las veo también.{w} ¿Pero qué tienen de especial?"


translate spanish day6_after_map_47fad4d7:


    un "No lo sé...{w} Es como si me hablaran. Hay otras personas allá. Tienen su propia vida, probablemente mejor que la nuestra, y ellos también miran al cielo y ven la Tierra, a mí y a ti."


translate spanish day6_after_map_b51220d8:


    me "Voces de estrellas lejanas..."


translate spanish day6_after_map_73da0b9c:


    th "Ya había escuchado eso en otra parte."


translate spanish day6_after_map_548f7ac2:


    un "Se podría decir así."


translate spanish day6_after_map_092f8254:


    me "Interesante teoría."


translate spanish day6_after_map_1b701aca:


    un "No, no, la teoría es bastante común."


translate spanish day6_after_map_8603b4ab:


    "Lena me miró."


translate spanish day6_after_map_33f7f134:


    "En la tenue luz lunar, me percaté de una pequeña lágrima surcando hacia abajo su mejilla.{w} O al menos eso imaginé..."


translate spanish day6_after_map_92d22845:


    me "Supongo que soy más... digamos que una persona práctica."


translate spanish day6_after_map_15c1d9fb:


    "Ella no dijo nada, tan sólo se quedó mirando fijamente otra vez el cielo."


translate spanish day6_after_map_a6f3957e:


    me "Pero si lo piensas, por supuesto, hay un espacio interestelar infinito, millones de planetas, cientos de galaxias...{w} ¡Es fascinante!"


translate spanish day6_after_map_94f87d77:


    "Mi voz se escuchaba excitada de manera poco natural."


translate spanish day6_after_map_51cb2968:


    un "No necesitas hablar de todas estas cosas."


translate spanish day6_after_map_cb23542f:


    me "¿Sobre qué?"


translate spanish day6_after_map_0ed52f26:


    un "Sobre esto...{w} Sobre todo..."


translate spanish day6_after_map_23571ed6:


    me "No, las estrellas son...{w} son realmente muy bonitas..."


translate spanish day6_after_map_61cd4196:


    un "¿Por qué viniste?"


translate spanish day6_after_map_1ed10390:


    "Ahora seguro que estaba llorando."


translate spanish day6_after_map_4cd41dd4:


    me "Yo...yo...{w} te estaba buscando."


translate spanish day6_after_map_41cfffd5:


    un "¿Por qué?"


translate spanish day6_after_map_9432886b:


    me "¡No sé por qué! ¡Tan sólo te buscaba!"


translate spanish day6_after_map_4cd400a2:


    un "Me encontraste, ¿estás feliz ahora?"


translate spanish day6_after_map_f883183e:


    me "Bueno..."


translate spanish day6_after_map_5eca3ce7:


    "Murmuré."


translate spanish day6_after_map_90b68121:


    un "¿Por qué no te fuiste con Alisa?"


translate spanish day6_after_map_9f078a0b:


    me "¿Qué tiene que ver ella con esto?"


translate spanish day6_after_map_53406a5d:


    un "¿Acaso intentas decirme que ella no tiene nada que ver?"


translate spanish day6_after_map_214e0fd5:


    me "¡Sí, eso es exactamente lo que digo! ¿Por qué sacaste otra vez este tema? Pensé que lo habíamos aclarado todo."


translate spanish day6_after_map_d2905358:


    "Desde luego, no habíamos aclarado nada.{w} Para ser más precisos, ni tan siquiera empezamos."


translate spanish day6_after_map_3fd16509:


    "Sin embargo, no entiendo por qué todo esto es necesario."


translate spanish day6_after_map_a5ef42ee:


    un "¿De verdad crees que quiero hablar sobre ello?"


translate spanish day6_after_map_3ae4ee1d:


    me "Si no quieres, entonces... ¿por qué?"


translate spanish day6_after_map_f20d2d44:


    un "Porqué...{w} Porqué...{w} ¡Porque es sobre ti!"


translate spanish day6_after_map_fd549485:


    me "¿Porque es sobre mí el qué?"


translate spanish day6_after_map_e358f2de:


    un "Sobre ti y Alisa..."


translate spanish day6_after_map_4fa9efef:


    me "Yo y Alisa ¿qué?{w} No tenemos una relación... ¡no tenemos nada en absoluto!"


translate spanish day6_after_map_bdc08ff0:


    un "No me mientas..."


translate spanish day6_after_map_50c351a6:


    "Lena se dio la vuelta sollozando callada."


translate spanish day6_after_map_73766a33:


    th "¿Por qué esto es tan importante para ella?"


translate spanish day6_after_map_dcecdc26:


    me "¿Qué otra cosa puedo decir si no me crees?"


translate spanish day6_after_map_6706f668:


    un "¡Cuéntame la verdad!"


translate spanish day6_after_map_61fa1f64:


    me "¡Ya te la he dicho!"


translate spanish day6_after_map_aa3b5bb0:


    un "¡Pues entonces vete con ella!"


translate spanish day6_after_map_0aa84a7b:


    me "¿Por qué debería irme con ella? ¡No me quiero ir a ninguna parte!"


translate spanish day6_after_map_5041432a:


    un "¿Por qué te quedas sentado aquí torturándome?"


translate spanish day6_after_map_80e9ae79:


    me "¡Oh, Señor mío! ¿Cómo voy a estar torturándote?"


translate spanish day6_after_map_cf8bd28b:


    "Lena no respondió."


translate spanish day6_after_map_f59f13df:


    me "Te he estado buscando todo el día, ¡porque estaba preocupado! ¡Vine aquí porque tú estás aquí! ¡Estoy sentado aquí porque quiero estar aquí! ¿Qué no comprendes?"


translate spanish day6_after_map_70497417:


    "Ella dejó de llorar por un instante."


translate spanish day6_after_map_dd41c6da:


    un "¿Es eso cierto?"


translate spanish day6_after_map_771aa502:


    me "¡Por supuesto!"


translate spanish day6_after_map_2630532a:


    un "¿Significo algo para ti?"


translate spanish day6_after_map_97962bdb:


    "Incluso teniendo en cuenta todas las cosas que justo había dicho, esta cuestión me asaltó inesperadamente."


translate spanish day6_after_map_ea6d6d42:


    me "Bueno...{w} Sí..."


translate spanish day6_after_map_dba41e75:


    un "¡Si sólo pudieras escucharte a ti mismo ahora!"


translate spanish day6_after_map_2844b0b9:


    "Ella lloró, se puso en pie y caminó rauda hacia la salida de la pista de deportes."


translate spanish day6_after_map_9e8ba2ed:


    me "¡ESpera!"


translate spanish day6_after_map_ea828f9c:


    "Le atrapé y le agarré su brazo, pero ella se desembarazó de mi mano."


translate spanish day6_after_map_6afc93f8:


    un "¡No me toques!"


translate spanish day6_after_map_5ad79041:


    "Ahora Lena era una persona totalmente diferente."


translate spanish day6_after_map_c1e1344b:


    "Y no podía decir que fuera beligerante, autoritaria o empecinada.{w} Sencillamente ella no albergaba dudas de lo que estaba haciendo."


translate spanish day6_after_map_42af3d5b:


    me "¡Espera!{w} ¿Qué dije? ¡Todo eso es cierto!"


translate spanish day6_after_map_96759709:


    un "¡Ahórrate todo este absurdo por ella!"


translate spanish day6_after_map_78f10d19:


    "Traté de detener a Lena una vez más, pero ella me hizo una mirada tan fuerte que no me atreví a discutir."


translate spanish day6_after_map_26abaae4:


    me "¡Solamente imagínate cómo me siento!{w} ¡Haces un puñado de cosas estúpidas, y ahora crees que soy culpable de todos los pecados mortales!"


translate spanish day6_after_map_3abf163e:


    un "¿Por qué deberías preocuparte?"


translate spanish day6_after_map_18650fcd:


    me "¡Debería!"


translate spanish day6_after_map_c2fbebf3:


    "Se detuvo por un instante y me miró."


translate spanish day6_after_map_3b50af60:


    un "¡Son todo mentiras!"


translate spanish day6_after_map_861f4d27:


    "Con ira golpeé mi frente con un poste de hierro."


translate spanish day6_after_map_999750cd:


    un "¡Puedes quedarte golpeándote a ti mismo ahí!"


translate spanish day6_after_map_8da3ba3c:


    "Había visto ser a Lena de muchas cosas, pero no despiadada e indiferente."


translate spanish day6_after_map_1db6eb05:


    me "¡Espera, hablemos con calma!"


translate spanish day6_after_map_019acfa8:


    un "¡No hay nada de qué hablar!"


translate spanish day6_after_map_b44c2c2d:


    "Ella atravesó la pista de fútbol y la seguí detrás, tratando de persuadirla en vano de que me escuchara."


translate spanish day6_after_map_e6426ab5:


    th "No sé por qué estoy haciendo todo esto. ¿Para demostrar que tengo razón?"


translate spanish day6_after_map_db9e6092:


    th "¿Con tal de no ser malinterpretado?{w} ¿Así seré mejor visto por ella?"


translate spanish day6_after_map_666d032c:


    th "¿O hay otra razón?"


translate spanish day6_after_map_2fd24a5d:


    "En cualquier caso, en este momento sentí que era necesario."


translate spanish day6_after_map_b3e9120f:


    "Llegamos a la plaza."


translate spanish day6_after_map_197a0325:


    "Lena era bastante rápida, apenas podía seguirle el ritmo."


translate spanish day6_after_map_3cdd393e:


    "Tenía que decir algo. Mi cabeza estaba llena de opciones encontradas, pero nada adecuadas."


translate spanish day6_after_map_9e8ba2ed_1:


    me "¡Espera!"


translate spanish day6_after_map_ba080751:


    "No hubo respuesta."


translate spanish day6_after_map_70cd7a9e:


    me "Escucha..."


translate spanish day6_after_map_ba080751_1:


    "No hubo respuesta."


translate spanish day6_after_map_d175e20e:


    me "¡¿Vas a parar?!"


translate spanish day6_after_map_ba080751_2:


    "No hubo respuesta."


translate spanish day6_after_map_a3922398:


    me "¡Vamos a despertar a todo el campamento!"


translate spanish day6_after_map_b23d87cc:


    un "¿Y qué? ¿Qué más da?"


translate spanish day6_after_map_e688e51e:


    "Qué extraño, pero en aquel momento estaba más preocupado por nuestro comportamiento que por Lena."


translate spanish day6_after_map_54f55e48:


    "Ella se detuvo."


translate spanish day6_after_map_673b1993:


    un "Gracias por acompañarme, ahora me iré sola."


translate spanish day6_after_map_53d3d956:


    "Dijo Lena mordazmente."


translate spanish day6_after_map_9a44e183:


    me "¡Aun no me has escuchado!"


translate spanish day6_after_map_3a8fc4c1:


    un "Para mí, es como si ya te hubiera escuchado, ¡incluso varias veces!"


translate spanish day6_after_map_14f0e6dc:


    me "¿Pues entonces qué es lo que quieres de mí?"


translate spanish day6_after_map_fd132569:


    un "¿Yo? ¿De ti? ¡Nada en absoluto!"


translate spanish day6_after_map_a1a64448:


    "Ella sonaba mucho más segura que yo, no había ni un ápice de duda en su mirada."


translate spanish day6_after_map_2f879a85:


    me "¿Qué puedo decir...? ¿Qué puedo hacer...?{w} ¡Maldita sea!"


translate spanish day6_after_map_ed3bf88e:


    "Estaba a punto de romper a llorar."


translate spanish day6_after_map_4b3132f4:


    un "No te pongas tan sentimental. No ocurrió nada."


translate spanish day6_after_map_f6b5a1cc:


    me "¡¿Que nada ocurrió?! ¡Definitivamente algo sucedió!"


translate spanish day6_after_map_488ea261:


    un "No te perderás nada si no me prestas atención."


translate spanish day6_after_map_6750969b:


    me "¡Deja que eso lo decida yo!"


translate spanish day6_after_map_de950caa:


    un "No, en serio, ¿por qué?{w} Hay muchas actividades agradables las cuales podrías hacer.{w} Yo, francamente, tampoco lo necesito."


translate spanish day6_after_map_9f0f0f1e:


    me "Si no lo necesitas, ¡¿por qué siempre mencionas constantemente a Alisa?!"


translate spanish day6_after_map_d67764cb:


    "El rostro de Lena cambió."


translate spanish day6_after_map_11f8377a:


    un "Eso no es asunto tuyo, ¡¿lo entiendes?!"


translate spanish day6_after_map_5fab1698:


    me "No, ¡ahora es asunto mío!"


translate spanish day6_after_map_52a66644:


    "Ella me miraba como si estuviera preparada para matarme."


translate spanish day6_after_map_6a9541dc:


    un "¿Por qué me estás molestando? ¿No la tienes a ella? ¡Pues vete con ella! Ella no te rechazará, te lo aseguro."


translate spanish day6_after_map_17a396ba:


    "Lena comenzó a chillar, su cabello se despeinó, su rostro se sonrojó y sus ojos estaban inyectados en sangre."


translate spanish day6_after_map_d2b2d0c2:


    me "¡Cálmate! No iré con ella. ¿Por qué piensas eso? Tan sólo mira... Ahora estoy aquí y no me voy a ir a ninguna parte."


translate spanish day6_after_map_87b3deb0:


    "Parecía calmarse un poquito."


translate spanish day6_after_map_3a8b8317:


    dv "¿Qué está pasando aquí?"


translate spanish day6_after_map_166adabe:


    "Me di la vuelta y vi a Alisa, mordisqueando perezosamente un bollo."


translate spanish day6_after_map_9bba946f:


    th "En mal lugar y en el peor momento. Esta situación es un perfecto ejemplo."


translate spanish day6_after_map_10e07777:


    "Me quedé de piedra sorprendido, sin saber qué decir."


translate spanish day6_after_map_41dceae9:


    "Pero Lena fue más inteligente."


translate spanish day6_after_map_f335a36d:


    un "Es la hora de pasarlo. ¡Agárralo!"


translate spanish day6_after_map_e2e97404:


    "Alisa miró sorprendida.{w} No me extraña, ella no había escuchado toda la conversación."


translate spanish day6_after_map_7cac8762:


    dv "¿Qué?"


translate spanish day6_after_map_cfaec870:


    un "Dije que lo cojas."


translate spanish day6_after_map_32fc49d5:


    "En un instante Lena se volvió tal como ella era antes... tranquila y serena."


translate spanish day6_after_map_20cf849d:


    dv "¿Qué debería coger?"


translate spanish day6_after_map_0216fe85:


    un "¡A él!"


translate spanish day6_after_map_3f535c7c:


    "Ella me señaló con su dedo con aversión."


translate spanish day6_after_map_21610fc5:


    un "Se queja de cuánto te quiere, de cómo no puede vivir sin ti y de cosas así."


translate spanish day6_after_map_e0ebb201:


    dv "¿Qué?"


translate spanish day6_after_map_61fafefa:


    "Alisa abrió los ojos como platos."


translate spanish day6_after_map_5135656c:


    me "No, estás completamente equivocada...{w} Lena sólo está bromeando."


translate spanish day6_after_map_d9758ca6:


    "Hice una risita nerviosa."


translate spanish day6_after_map_9849559e:


    un "¿Por qué? Eso es exactamente lo que está ocurriendo."


translate spanish day6_after_map_6a9072d6:


    me "Escucha, todo tiene su límite."


translate spanish day6_after_map_f25c750f:


    "Le dije con calma."


translate spanish day6_after_map_bd03c02c:


    me "¿Es que quieres que haya víctimas?"


translate spanish day6_after_map_a94aa304:


    un "¿Por qué? Ya te sugerí que deberías irte con ella."


translate spanish day6_after_map_0ece655b:


    dv "No sé de qué estáis hablando aquí, ¡pero no me involucréis!"


translate spanish day6_after_map_c981c313:


    un "¿No lo sabes...?"


translate spanish day6_after_map_51cf0a90:


    "Dijo Lena suavemente, aunque su voz tenía un rastro de rabia en ella."


translate spanish day6_after_map_73a43983:


    un "¡¿Otra vez lo mismo?!"


translate spanish day6_after_map_16082370:


    "Alisa la miró asustada."


translate spanish day6_after_map_e75b572c:


    dv "Escucha, comprendo que tú, bueno, mmm...{w} Pero yo no lo sé... De verdad... ni una sola palabra, ¡ni tan siquiera una sola idea de él! ¡Lo juro!"


translate spanish day6_after_map_efca6f44:


    un "¡¿Qué no lo sabes?!"


translate spanish day6_after_map_75b8e738:


    "Lena se abalanzó sobre ella, y antes de que me diera cuenta, golpeó Alisa con un fuerte gancho de derecha."


translate spanish day6_after_map_c097944c:


    "Bueno, una sencilla bofetada en efecto, ¡pero semejante golpe podía romperle la mandíbula a alguien!"


translate spanish day6_after_map_1bd51a91:


    "Alisa se derrumbó y pareció perder el conocimiento."


translate spanish day6_after_map_91c4ce70:


    th "¿Qué debería hacer?"


translate spanish day6_after_map_660ef966:


    "Elegí que sin importar lo que sucediera, no me afectaba."


translate spanish day6_after_map_7596bb80:


    th "Pero, por otra parte, ¿cómo podía permanecer indiferente?"


translate spanish day6_after_map_41d6bd2d:


    th "¡¿Soy un hombre de verdad?!"


translate spanish day6_after_map_1b093c3a:


    me "¡¿Qué estás haciendo?!"


translate spanish day6_after_map_dc606069:


    "Salí corriendo hacia la muchacha desmayada y traté de comprobar si estaba bien."


translate spanish day6_after_map_5e987c49:


    me "¿Es que te has vuelto completamente loca? Si me quieres abandonar, hacerme callar u otra cosa... ¡de acuerdo! ¡Pero esto, esto es cosa de locos! ¡¿Tal vez necesites una camisa de fuerza para que así nadie acabe herido?!"


translate spanish day6_after_map_0710111a:


    "Lena se quedó quieta, apretando su puño."


translate spanish day6_after_map_246bc5b9:


    "Podrías haberte roto tu mano con semejante golpe."


translate spanish day6_after_map_8bf70748:


    un "Tú... Tú...{w} ¡No entiendes nada!"


translate spanish day6_after_map_8373e0fe:


    "Las lágrimas le brotaron, surcando su rostro abajo y se fue de la plaza."


translate spanish day6_after_map_3ccb1990:


    "Me pareció que aun podía escuchar el llanto de Lena mientras intentaba reanimar a Alisa."


translate spanish day6_after_map_b0111b39:


    "Al fin, pudo volver a recuperar el conocimiento."


translate spanish day6_after_map_605ea41f:


    me "¿Estás bien? ¿Cómo te encuentras?"


translate spanish day6_after_map_f537afe9:


    "Alisa movió su mandíbula a izquierda y derecha."


translate spanish day6_after_map_898a6e8c:


    dv "Viviré..."


translate spanish day6_after_map_d84b48c5:


    "La ayudé a incorporarse."


translate spanish day6_after_map_64e51ae5:


    dv "Ya te lo dije..."


translate spanish day6_after_map_6146f956:


    me "Bueno, ¡eso no importa ahora! ¡Tienes que ir hasta la enfermería!"


translate spanish day6_after_map_93df0dc6:


    dv "Es demasiado tarde para la enfermería...{w} Me iré a dormir y descansaré bien por la noche y, por la mañana, ya iré allí."


translate spanish day6_after_map_3e4756aa:


    me "Vale, pues te acompaño."


translate spanish day6_after_map_2807c5a8:


    dv "No puedo rechazarlo."


translate spanish day6_after_map_853a9bed:


    "Dijo Alisa, tratando de hacerme una sonrisa."


translate spanish day6_after_map_e82487ee:


    "Durante todo el camino estuvimos en silencio."


translate spanish day6_after_map_705d46ac:


    "Probablemente no estaba en condiciones para hablar, y no pude hallar las palabras adecudas que decir. Todo esto fue una verdadera conmoción para mi."


translate spanish day6_after_map_08c54854:


    "Me encontré como mero espectador de un drama en el cual justo acababa de ser protagonista principal..."


translate spanish day6_after_map_f6503a89:


    "Después de que la puerta se cerrara tras Alisa, me quedé quieto por un largo rato y observé su cabaña."


translate spanish day6_after_map_7775e100:


    th "¿Qué debería hacer tras todo esto?"


translate spanish day6_after_map_d541a4e0:


    "La fatiga se apoderaba de mí. Tenía que dormir."


translate spanish day6_after_map_f4e9f6bc:


    th "Creo que Lena está en un estado ahora en el cual le es improbable afrontar la realidad adecuadamente."


translate spanish day6_after_map_0b692d89:


    "Completamente destrozado, me tumbé en la cama y me arrastró el sueño."


translate spanish day6_us_932d85c4:


    "No puedo decir que mi mañana fuera buena.{w} En vez de ello, me desperté destrozado por completo."


translate spanish day6_us_44869d66:


    "La habitación estaba invadida por una brillante luz solar. Fuera por la ventana, los pájaros estaban piando desenfadadamente."


translate spanish day6_us_3d1fd928:


    "A regañadientes me arrastré hasta ponerme sentado y me estiré."


translate spanish day6_us_26166937:


    "Lentamente comencé a recordar los acontecimientos del día anterior."


translate spanish day6_us_ba99f4d8:


    "Un viaje, un juego tonto de fantasmas, Ulyana quedándose dormida y..."


translate spanish day6_us_8eb6fcb7:


    "¡...un beso!"


translate spanish day6_us_9ab1bc4f:


    "Me ruboricé de inmediato.{w} Fue tan sorprendente que perdí el sentido de la realidad por un rato."


translate spanish day6_us_89d1c6c1:


    th "La cuestión principal es, ¿cómo debería tratarla ahora?{w} A primera vista nada en especial había ocurrido, pero..."


translate spanish day6_us_b9f2d616:


    th "Además, ella es la última muchacha de la cual uno esperaría tal acción, en todo el campamento."


translate spanish day6_us_7b429d44:


    "Ella era como mucho como una pequeña hermanita, nada más."


translate spanish day6_us_d11fbcdb:


    "Pero para ella, aparentemente, era más que un gran hermano..."


translate spanish day6_us_b3cdc0ae:


    "A pesar de todo, quizás tan sólo me imaginé eso y no había ningún significado más, ninguna profunda razón o alusiones a conclusiones precipitadas en este beso.{w} ¿Sólo una expresión de gratitud?"


translate spanish day6_us_22c8c3a5:


    th "Está claro que Ulyana es capaz de extraordinarias hazañas.{w} Y puede convertir fácilmente cosas sencillas en otras complicadas."


translate spanish day6_us_bd31a746:


    "Finalmente, decidí dejar de lado el asunto hasta que pudiera encontrarme cara a cara con ella."


translate spanish day6_us_b202d75c:


    "El reloj indicaba las once.{w} Lo cual significaba que Olga Dmitrievna no me despertó para el desayuno."


translate spanish day6_us_4dc8c2d9:


    "Sin embargo, no debería preocuparme. No quedaba mucho tiempo hasta la hora de comer."


translate spanish day6_us_aca3d21b:


    "Cogí mi estuche con los accesorios de aseo y me fui a asearme."


translate spanish day6_us_3701bba9:


    "No me encontré con nadie de camino."


translate spanish day6_us_8ddbdf53:


    th "Probablemente, todos los pioneros estuvieran ocupados con sus cosas."


translate spanish day6_us_a5a2f280:


    "Cerca del lugar de las jofainas también se estaba desierto."


translate spanish day6_us_9c48b593:


    "Tan sólo cinco minutos más tarde, estuve de nuevo frente a la cabaña de la líder del campamento, pensando en qué hacer."


translate spanish day6_us_a5889f74:


    "Nada que valiera la pena se me ocurrió, me senté en la tumbona y posiblemente podría haberme quedado allí hasta la hora de comer, pero me sacó de mi ensimismamiento una voz familiar."


translate spanish day6_us_9104ac12:


    sl "¡Hola!"


translate spanish day6_us_d058f15f:


    "Alcé la mirada y vi a Slavya quien me sonreía dulcemente."


translate spanish day6_us_a87c71d7:


    me "Hola."


translate spanish day6_us_452193c9:


    sl "¿Te has dormido otra vez?"


translate spanish day6_us_6581dd6b:


    me "Eso parece..."


translate spanish day6_us_269ed985:


    sl "Eso no es bueno..."


translate spanish day6_us_472ae0f8:


    me "Quizás... ¿Y adónde vas?"


translate spanish day6_us_dd46fbd4:


    "En realidad, no estaba interesado realmente en los planes de Slavya para hoy, pero quería mantener la conversación."


translate spanish day6_us_619b2083:


    sl "Bueno, tengo algunas cosas que hacer."


translate spanish day6_us_f2e007f0:


    me "Ya veo..."


translate spanish day6_us_54d766e4:


    "Se hizo un silencio.{w} Ella estaba a punto de irse cuando, de repente, dije:"


translate spanish day6_us_bdfd5ce1:


    me "¿Te importa si te sientas conmigo por un rato?"


translate spanish day6_us_ba1218a0:


    sl "Claro, ¿por qué no?"


translate spanish day6_us_97a08ca0:


    "Slavya se sentó cerca."


translate spanish day6_us_d1c22b75:


    sl "Alguna cosa te preocupa, ¿verdad?"


translate spanish day6_us_78b966f1:


    me "No, ¿por qué crees eso?"


translate spanish day6_us_9ac0f8e4:


    sl "Puedo leerlo en tu rostro."


translate spanish day6_us_ca162b6d:


    "Se rio dulcemente."


translate spanish day6_us_a6ff12a6:


    me "De verdad que no..."


translate spanish day6_us_f22c4266:


    "Aunque probablemente Slavya tenía razón... algo {i}estaba{/i} realmente preocupándome.{w} Obviamente, me preocupaba por los acontecimientos de la última noche."


translate spanish day6_us_60656899:


    me "Escucha, ¿tienes algún hermano o hermana pequeña?"


translate spanish day6_us_bb4d3f88:


    sl "Así es."


translate spanish day6_us_b8863b83:


    me "¿Y cómo os lleváis?"


translate spanish day6_us_85a61fa8:


    sl "Bastante bien."


translate spanish day6_us_6d77584d:


    "Ella sonrió ampliamente."


translate spanish day6_us_9aed200b:


    sl "¿Y tú?"


translate spanish day6_us_5cb822ee:


    me "No, yo no..."


translate spanish day6_us_96096e29:


    "Dije tras una breve pausa."


translate spanish day6_us_cfdfe577:


    sl "¿Entonces por qué me preguntas?"


translate spanish day6_us_0f0d016f:


    me "No lo sé, solamente..."


translate spanish day6_us_f36a24a2:


    me "¿Siempre los comprendes?"


translate spanish day6_us_e7a9578f:


    sl "Lo intento."


translate spanish day6_us_0f15cfb0:


    me "A veces no puedo figurarme qué quieren los críos..."


translate spanish day6_us_7519a16d:


    "Repentinamente me detuve por un instante."


translate spanish day6_us_b771fb63:


    th "Slavya no sabe en realidad que soy mucho más mayor de lo que aparento.{w} Para ella, yo mismo soy todavía un jovencito."


translate spanish day6_us_5baee7ee:


    me "Aquellos que son más jóvenes..."


translate spanish day6_us_70f37a53:


    sl "¿Por qué?"


translate spanish day6_us_f4a5584e:


    me "Bueno, no siempre puedo comprender las razones de sus actos. A menudo se comportan de forma irracional."


translate spanish day6_us_257da516:


    sl "¿Acaso tú no eras igual de mayor que ellos tiempo atrás?"


translate spanish day6_us_efd76d15:


    "Ella me miraba sorprendida."


translate spanish day6_us_80ee985d:


    me "Desde luego que lo fui..."


translate spanish day6_us_de2e65e6:


    "En aquel instante, me pareció que había pasado ya mucho tiempo.{w} Y cuando miré a Ulyana en aquella ocasión, fue como un abismo de años."


translate spanish day6_us_b77a46d6:


    sl "¡Bueno! Piensa en ti mismo en aquella época."


translate spanish day6_us_774a19b3:


    me "Fue diferente..."


translate spanish day6_us_3e1005b1:


    "Al menos eso me lo parecía a mí."


translate spanish day6_us_457d02d4:


    sl "Si es así, ¡bien por ti!"


translate spanish day6_us_c63c3cdc:


    "Slavya se rio otra vez."


translate spanish day6_us_71b0154f:


    me "¿Y tú?{w} ¿Hiciste cosas estúpidas también?"


translate spanish day6_us_d85b7a18:


    sl "Depende de la edad de la que hablemos."


translate spanish day6_us_79e03e6b:


    me "Bueno, digamos...{w} A los catorce años por ejemplo..."


translate spanish day6_us_a25fc2aa:


    sl "Bueno, distintas cosas me ocurrieron..."


translate spanish day6_us_5e20fc4d:


    "Dijo pensativamente."


translate spanish day6_us_0536f6a9:


    me "Vale, eso es bonito, pero sigue sin dejar las cosas claras..."


translate spanish day6_us_d97a7610:


    sl "¿Tal vez es la forma en la que debería ser?"


translate spanish day6_us_963937db:


    me "Quizá..."


translate spanish day6_us_f0a4acf4:


    "Slavya se levantó, se despidió con un adiós y se fue. Cerré mis ojos y me dormí."


translate spanish day6_us_a20cefa7:


    "..."


translate spanish day6_us_c8a36022:


    "El sonido de la campana avisando a los pioneros para comer me despertó."


translate spanish day6_us_33663426:


    "Aunque el sonido era flojo y provenía de muy lejos, mi cuerpo reaccionó como un autómata de reloj y, en el momento debido, ya estaba cerca de la puerta de la cantina."


translate spanish day6_us_c789b795:


    "Y así fue cómo me encontré con Ulyana."


translate spanish day6_us_da3a622a:


    us "¡Hola, que tal!"


translate spanish day6_us_d2510e4c:


    me "Ah... Ey..."


translate spanish day6_us_886b57c4:


    us "¡Ya comí!"


translate spanish day6_us_9bc7d8f8:


    "Se rio alegremente y se fue corriendo."


translate spanish day6_us_33ab27ac:


    "No la detuve.{w} A fin de cuentas, no tenía nada que decirle a ella."


translate spanish day6_us_ec659793:


    "Cogí una comida y ya me estaba dirigiendo hacia mi rincón favorito, cuando repentinamente alguien me agarró por mi mano."


translate spanish day6_us_04f58a0e:


    dv "¡Siéntate aquí!"


translate spanish day6_us_f294c7bb:


    "La voz de Alisa sonaba amenazadora, pero a la vez de alguna forma parecía una súplica."


translate spanish day6_us_29f71b5c:


    me "¿Qué sucedió?"


translate spanish day6_us_73ab0e4f:


    dv "Tengo algo que discutir contigo."


translate spanish day6_us_cb7dcf26:


    "Dijo con un tono ligeramente más suave."


translate spanish day6_us_505d91ae:


    me "Vale..."


translate spanish day6_us_12af78a1:


    "Me senté a su lado."


translate spanish day6_us_c233b6a2:


    me "Y bien, ¿de qué se trata?"


translate spanish day6_us_c1a63ca7:


    dv "¿Tú estabas ayer allí...{w} cuando ella hizo esa tonta broma con la sábana?"


translate spanish day6_us_ae83cf7a:


    th "No tenía ningún sentido mentirle a Alisa."


translate spanish day6_us_627457bb:


    me "Claro, estuve ahí..."


translate spanish day6_us_3cf2f629:


    dv "Eso es... Eso es..."


translate spanish day6_us_56d4e39c:


    "Se rio a carcajadas."


translate spanish day6_us_bf0fda5f:


    dv "¿Por qué diantres hiciste eso?"


translate spanish day6_us_d35441c0:


    "Alisa al instante se volvió seria y me miró fijamente."


translate spanish day6_us_4954bdac:


    me "¿Por qué no se lo preguntas a ella?"


translate spanish day6_us_6153932c:


    dv "No, te lo pregunto a ti ahora, ¡así que tienes que contestar!"


translate spanish day6_us_fccc1215:


    me "¡No lo sé!"


translate spanish day6_us_ccbb1994:


    "Carraspeé."


translate spanish day6_us_bd86e9cc:


    me "¿O mejor me cuentas por qué la líder del campamento no reaccionó?"


translate spanish day6_us_c46902a7:


    dv "¿Cómo debería haber reaccionado?"


translate spanish day6_us_2552c9ac:


    me "Creo que ya lo sabes..."


translate spanish day6_us_84ea8c71:


    "La miré con escepticismo."


translate spanish day6_us_a306c7bc:


    dv "Bueno, ella sencillamente no se dio cuenta quién era."


translate spanish day6_us_65256f6f:


    "Bueno, debo admitir que las habilidades deductivas de Olga Dmitrievna dejan mucho que desear."


translate spanish day6_us_251f6e0f:


    dv "Y luego...{w} ¿Fuiste tú quién se la llevó por la noche?"


translate spanish day6_us_311e429f:


    me "Nop, fueron los elfos de Shakespear."


translate spanish day6_us_12b4f273:


    "Dije socarronamente."


translate spanish day6_us_1e5bc39c:


    me "¿Qué problema hay?"


translate spanish day6_us_b3b67d03:


    dv "Ninguno..."


translate spanish day6_us_2e06d203:


    "Alisa miró fijamente su plato y comenzó a comer con concentración."


translate spanish day6_us_6cc1bbe6:


    dv "También...{w} Salió corriendo a alguna parte por la noche y volvió con muy buen humor..."


translate spanish day6_us_f95c9fc9:


    me "¿Y?"


translate spanish day6_us_79b4bb58:


    dv "¿Sabes algo acerca de eso?"


translate spanish day6_us_37ad7929:


    me "Y aunque sepa algo, ¿por qué te importa?"


translate spanish day6_us_8fb93608:


    "Ella me hizo una mirada intensa."


translate spanish day6_us_304d61e3:


    dv "Bueno, me preocupo."


translate spanish day6_us_3856e844:


    th "Nunca habría pensado que ella pudiera estar preocupada por alguien a parte de sí misma."


translate spanish day6_us_df1ea57f:


    me "Nada de eso ocurrió si es lo que quieres saber..."


translate spanish day6_us_744c373a:


    dv "Qué va, no es acerca de eso..."


translate spanish day6_us_6d91ce66:


    "Alisa respondió brevemente, sin mirarme."


translate spanish day6_us_9e028cbf:


    "Probablemente fue una mala idea sacar este tema."


translate spanish day6_us_4db446fd:


    dv "Por cierto, ¿qué es eso de «nada de eso»?"


translate spanish day6_us_3ffb4733:


    me "¿Qué quieres decir?"


translate spanish day6_us_ae504ccb:


    dv "¡Lo que acabaste de decir!"


translate spanish day6_us_357ddbfd:


    me "¿Qué es eso que acabé de decir?"


translate spanish day6_us_bb9ef712:


    "Traté de bromear para confundirla."


translate spanish day6_us_93884669:


    dv "Ey... ¡escúchame! Te estoy vigilando."


translate spanish day6_us_465a1be8:


    me "¡Oh, estoy tan y tan asustado!"


translate spanish day6_us_46433dd4:


    "Dije groseramente, agarré mi bandeja y abandoné la mesa."


translate spanish day6_us_868ec380:


    "La verdad es que no estaba preocupado por las palabras de Alisa, incluso a pesar de que sonaran como una amenaza."


translate spanish day6_us_f2a61bc9:


    "De hecho, estaba más interesado en su atención por Ulyana..."


translate spanish day6_us_3a2303e3:


    "De todos los lugareños, seguramente ella era la última de la que esperarse que se preocupara por otra persona."


translate spanish day6_us_523908a6:


    "Caminé para salir de la cantina y entrar en un verdadero horno.{w} El sol ardía tan despiadadamente que nadie podría estar bajo los abrasadores rayos."


translate spanish day6_us_5a8af8ba:


    "Era la hora de buscar algún lugar fresco con sombra."


translate spanish day6_us_8816790a:


    "Y la playa parecía el mejor lugar para esto."


translate spanish day6_us_7ffbd155:


    "Muy extrañamente, la playa no estaba masificada."


translate spanish day6_us_d78798fb:


    "Quizá muchos de los pioneros estaban asustados por el calor abrasador, o tal vez decidieron hacer una siesta después de comer."


translate spanish day6_us_539fca33:


    "No me traje mi bañador, por lo que me senté bajo una sombrilla y comencé a observar a los nadadores."


translate spanish day6_us_aee79133:


    "No había nadie que conociera, así era mejor en tanto que tuve la oportunidad de sentarme tranquilamente a pensar."


translate spanish day6_us_6339194e:


    th "Cinco días y medio ya habían transcurrido desde mi llegada, ¿y qué había cambiado?"


translate spanish day6_us_e1327ac0:


    th "Habían un par de acontecimientos inexplicables pero completamente inofensivos, pero de todas formas, era simplemente un campamento de pioneros normal y corriente."


translate spanish day6_us_becbdeab:


    "Y esto era lo que me inquietaba más aun."


translate spanish day6_us_1d93d559:


    th "¿Qué iba a suceder?"


translate spanish day6_us_e2f93f20:


    th "Otro día, otra semana, ¿y qué ocurrirá cuando termine la temporada?{w} No sé a dónde retornaré."


translate spanish day6_us_efe9c612:


    th "Quizá en esta época ni había nacido aun.{w} O incluso peor..."


translate spanish day6_us_07aa477a:


    "Tras estar pensando todo esto durante unos cuantos minutos, me fui de paseo adonde ayer por la tarde."


translate spanish day6_us_b188d2fb:


    "Qué raro, me preocupaba mucho más ahora que antes."


translate spanish day6_us_5818c1ec:


    "Por un lado, quería decir algo para aclarar la situación, pero por otra parte, evité cualquier encuentro con Ulyana por todos los medios, preocupado por hacer el tonto con una palabra o gesto erróneo."


translate spanish day6_us_d6d9a6e5:


    "Cerré mis ojos y me quedé dormido por un rato."


translate spanish day6_us_a20cefa7_1:


    "..."


translate spanish day6_us_3615e0f8:


    "Cuando me desperté, vi a Lena a mi lado."


translate spanish day6_us_4be9f1c5:


    me "¿Cuánto rato has estado sentada aquí?"


translate spanish day6_us_84bd5d09:


    "Pregunté sorprendido."


translate spanish day6_us_267018a5:


    un "No mucho."


translate spanish day6_us_74e632e7:


    "Qué raro que no me despertara."


translate spanish day6_us_ea63ce77:


    me "¿Ocurrió algo?"


translate spanish day6_us_7d85bdfc:


    "No sé por qué, pero mi sexto sentido me decía que ella estaba ansiosa de contarme algo pero que no se atrevía."


translate spanish day6_us_50dd705e:


    un "No. ¿Y a ti?"


translate spanish day6_us_8cd2864d:


    me "Tampoco."


translate spanish day6_us_cc4a1ab6:


    un "¿Seguro?"


translate spanish day6_us_771aa502:


    me "¡Por supuesto!"


translate spanish day6_us_2f04a7f9:


    "Sonreí."


translate spanish day6_us_d9b000a0:


    un "¿Pues de qué quieres hablar?"


translate spanish day6_us_0f1517b5:


    me "¿Por qué crees que quiero hablar?"


translate spanish day6_us_cdbc06cf:


    un "Me lo parece."


translate spanish day6_us_dd0d8a90:


    me "Qué va, para nada."


translate spanish day6_us_a5f21b45:


    "Un largo silencio le siguió."


translate spanish day6_us_1458865a:


    me "¿Tienes hermanas o hermanos pequeños?"


translate spanish day6_us_50dd705e_1:


    un "No. ¿Y tú?"


translate spanish day6_us_8cd2864d_1:


    me "No."


translate spanish day6_us_9e52f44a:


    un "Entonces, ¿por qué preguntas eso?"


translate spanish day6_us_d659d744:


    me "A veces pienso que no comprendo en absoluto a los críos."


translate spanish day6_us_959d479a:


    un "Yo también."


translate spanish day6_us_f41e692e:


    "El rostro de Lena no expresaba nada."


translate spanish day6_us_898b12b0:


    un "Pero probablemente tenga que ser así."


translate spanish day6_us_2eb02c21:


    me "¿Qué quieres decir?{w} ¿Cómo debería tratarlos pues?"


translate spanish day6_us_c80e177b:


    un "Trátalos de la manera en que creas que es correcto."


translate spanish day6_us_096e3a82:


    me "Bueno, esa es la gran cuestión de fondo... no sé lo que es correcto y lo que es incorrecto."


translate spanish day6_us_0263b300:


    un "Cuanto más lo pienses, más lamentarás los errores."


translate spanish day6_us_c256f910:


    me "Bueno, eso es cierto pero no ayuda..."


translate spanish day6_us_f82ea479:


    "Estaba sorprendido de alguna forma por semejantes profundas palabras de Lena."


translate spanish day6_us_9437b7c4:


    "Desde luego, ella lo dijo todo con un ápice de timidez o modestia y sin una apropiada sinceridad, pero aun así..."


translate spanish day6_us_5e0eb09d:


    me "¿Quieres decir que debería seguir la primera idea que me venga a la mente?"


translate spanish day6_us_60bde9d8:


    "Me reí."


translate spanish day6_us_61e40971:


    un "Como dice el dicho, lo primero que pienses es lo correcto, ya que cuando menos se piensa, salta la liebre."


translate spanish day6_us_8500866b:


    me "¿Crees eso?"


translate spanish day6_us_cb890890:


    un "No sé..."


translate spanish day6_us_e6ff543f:


    "Estuvimos sentados ahí durante un rato, luego ella se puso en pie."


translate spanish day6_us_ccdf7460:


    un "Bueno, me tengo que ir."


translate spanish day6_us_ce5c1ffe:


    me "¡Gracias por la conversa!"


translate spanish day6_us_584c5532:


    un "¡No hay de qué!"


translate spanish day6_us_d7a43efd:


    "Lena ligeramente sonrió."


translate spanish day6_us_7c4da760:


    "Me tumbé y me quedé mirando el ardiente sol."


translate spanish day6_us_d79d6192:


    th "Así que lo primero que se piensa es lo correcto, ¿eh?{w} ¿Y si mi primera idea fuera devolverle el beso?"


translate spanish day6_us_9d9f68a5:


    th "Por cierto, ¿cuál {i}fue{/i} el primer pensamiento que me vino a la mente en aquel momento?"


translate spanish day6_us_e288f243:


    "Traté con esfuerzo de recordar, pero no funcionó.{w} Ni tan siquiera un retazo de pensamiento me surgió de la mente."


translate spanish day6_us_7e684fb8:


    "Sin embargo, en aquella situación sólo podía seguir el consejo de Lena, puesto que todo sucedió tan repentinamente."


translate spanish day6_us_a20cefa7_2:


    "..."


translate spanish day6_us_28dd5f2a:


    "El sol se estaba poniendo lentamente, por lo que me alcé y me fui hacia la cantina, en tanto que mi estómago me recordaba que incluso los protagonistas de semejante fantástica historia tenían que comer a menudo."


translate spanish day6_us_793d9878:


    "Alguien me llamó por la plaza."


translate spanish day6_us_b4ae4166:


    "Era Electronik.{w} Corrió hasta mí, recuperó su aliento y dijo:"


translate spanish day6_us_45391550:


    el "Semyon..."


translate spanish day6_us_9aa9ea53:


    me "Sí, ese soy yo."


translate spanish day6_us_caf0b8c7:


    el "Coge esto."


translate spanish day6_us_5e22b544:


    "Me dio un tipo de llave."


translate spanish day6_us_51c2c17c:


    me "¿Qué es esto?"


translate spanish day6_us_24914d26:


    el "Es la llave para nuestro edificio de clubs."


translate spanish day6_us_9a940e9d:


    me "¿Y para qué la iba a querer?"


translate spanish day6_us_9fd6666c:


    el "Olga Dmitrievna me lo pidió...{w} Considerando que no volveré por hoy."


translate spanish day6_us_64143d09:


    me "Pues dásela a Shurik."


translate spanish day6_us_23cd6835:


    el "Él tiene sus propios asuntos."


translate spanish day6_us_483eac77:


    me "Pues llévatela tú mismo."


translate spanish day6_us_74e85ae4:


    el "No puedo."


translate spanish day6_us_5ac38cf1:


    me "¿Por qué?"


translate spanish day6_us_7a4a266c:


    el "Escúchame, sólo cógela."


translate spanish day6_us_c8e2439f:


    me "¿Por qué iba a necesitarla? ¿Qué voy a hacer con ella?"


translate spanish day6_us_83054bac:


    el "Me la devolverás más tarde."


translate spanish day6_us_17717ee8:


    me "Escucha..."


translate spanish day6_us_34709e3e:


    el "Bueno, ¡me tengo que ir!"


translate spanish day6_us_a5cf9eb3:


    "Me metió la llave en mi mano y salió corriendo."


translate spanish day6_us_378693dd:


    th "Da igual cómo te lo mires, él es realmente muy raro..."


translate spanish day6_us_ca9eac02:


    "Sin embargo, no le presté suficiente atención a este hecho y simplemente puse la llave en mi bolsillo."


translate spanish day6_us_3f520ea8:


    "Llegué a la cantina incluso antes de la cena."


translate spanish day6_us_b5e4650e:


    "Afortunadamente ya estaba abierta, pero ninguna comida se estaba sirviendo."


translate spanish day6_us_cc81d257:


    "Tomé mi asiento favorito en el más alejado rincón y me concentré jugando con un mondadientes."


translate spanish day6_us_2a72bf60:


    "Pronto tuve la impresión de que alguien estaba junto a mí."


translate spanish day6_us_94e4a1c1:


    "Alcé mi mirada y divisé a Olga Dmitrievna."


translate spanish day6_us_e4be0ed5:


    "Nos estuvimos mirando fijamente el uno al otro durante un rato."


translate spanish day6_us_fdb677a4:


    me "¿Necesitas algo?"


translate spanish day6_us_4c09a32f:


    "Rompí el silencio al fin."


translate spanish day6_us_acc07115:


    mt "Sabes, Semyon...{w} Claro que lo entiendo todo, pero..."


translate spanish day6_us_ad02f53a:


    me "¿Qué?"


translate spanish day6_us_7c87b14a:


    "Le pregunté sorprendido."


translate spanish day6_us_70193a9b:


    mt "¿Crees que no me di cuenta de tu desaparición ayer por la noche?"


translate spanish day6_us_a96d8927:


    th "«Desaparición» es bastante exagerado...{w} Más bien diría que sólo me fui por un rato."


translate spanish day6_us_e7288eb6:


    mt "Bueno, ¿y bien?"


translate spanish day6_us_1e5bc39c_1:


    me "¿Qué pasa?"


translate spanish day6_us_421df7f4:


    mt "Pensé que eso no sería nada, pero estaba dispuesta a comprobarlo."


translate spanish day6_us_8782263a:


    "La expresión de la cara de la líder del campamento se estaba convirtiendo poco a poco en un ceño fruncido."


translate spanish day6_us_6100ad4b:


    me "Pues bien, ¿lo has comprobado?"


translate spanish day6_us_27f521dd:


    "Todavía no podía entender de qué se trataba."


translate spanish day6_us_6b3e0c5d:


    mt "¡Claro que lo comprobé!{w} ¡Y tú lo sabes!{w} ¡Para un pionero de verdad eso es... eso es...! ¡Es inaceptable! ¡Es vergonzoso! ¡Es deshonroso! ¡Especialmente con ella!"


translate spanish day6_us_7e6d6b00:


    "Ella se sonrojó del exceso de esfuerzo."


translate spanish day6_us_443c0493:


    mt "¡No quiero ni escuchar tus patéticas excusas!"


translate spanish day6_us_fd11f26f:


    "Una idea vaga de lo que ella estaba hablando se me iluminó."


translate spanish day6_us_185a8790:


    me "Entonces, ¿qué ocurrió, serías tan amable de explicarlo?"


translate spanish day6_us_6ca85047:


    mt "¡Y ahora tienes la desfachatez de jugar a ser inocente! ¡Tú! ¡Con Ulyana!"


translate spanish day6_us_cae6c168:


    "Ahora todo estaba tan claro como el agua."


translate spanish day6_us_07246779:


    me "Te tengo que contar dos cosas. La primera..."


translate spanish day6_us_262d7cae:


    "Comencé a enfadarme."


translate spanish day6_us_b3aa00a8:


    me "Nada de eso sucedió allí. Sería el último en tener cualquier tipo de relación con dicha situación."


translate spanish day6_us_ca25a37e:


    mt "Y tú te crees eso..."


translate spanish day6_us_4ff3e72d:


    me "Segundo..."


translate spanish day6_us_a3058a6d:


    "La interrumpí groseramente."


translate spanish day6_us_7de2af8d:


    me "¿Quién te explicó eso?"


translate spanish day6_us_da2aca8e:


    "De hecho, la respuesta era obvia."


translate spanish day6_us_231ed06e:


    "Ulyana no haría eso. Y si ella había elegido una vez más ponerme una trampa, lo habría hecho mucho antes. Del tiempo que he transcurrido aquí, comencé a conocerla bien."


translate spanish day6_us_83723d62:


    th "Y eso solamente deja una posible candidata..."


translate spanish day6_us_d1eaca67:


    mt "¿Y qué importa?"


translate spanish day6_us_4d01563d:


    me "¡No, no! ¡Claro que importa!"


translate spanish day6_us_e5fc00e7:


    mt "¿Serías tan amable de explicar tu comportamiento pues?"


translate spanish day6_us_703eeb00:


    "Parecía que dejé a Olga Dmitrievna un poco desprevenida con mi vehemencia."


translate spanish day6_us_3d718732:


    me "¡No hay nada que explicar!"


translate spanish day6_us_9829aaef:


    "Rápidamente me levanté y me dirigí hacia la salida de la cantina."


translate spanish day6_us_19709348:


    mt "Semyon, espera..."


translate spanish day6_us_653b8455:


    "Escuché el parloteo de la líder del campamento detrás de mí."


translate spanish day6_us_2080cf3d:


    "Si actuas con decisión con ella, pues no resulta tan segura de sí misma."


translate spanish day6_us_4d5ffb3d:


    "Bien, ahora tengo que dejar las cosas claras con Dvachevskaya."


translate spanish day6_us_50e7f679:


    "Mientras que sus bromas no me afectaban, o solamente me afectaban indirectamente, podía sobrellevarlo."


translate spanish day6_us_abec8829:


    "De hecho si lo pienso bien, incluso comencé a considerarla no tan mala persona."


translate spanish day6_us_412c8322:


    "Me encontré con Slavya en la plaza."


translate spanish day6_us_c8aa9582:


    me "¿Sabes dónde está Alisa?"


translate spanish day6_us_b3c273bf:


    sl "Sí, ¿qué pasa?"


translate spanish day6_us_18c5db17:


    me "¡Pues dímelo!"


translate spanish day6_us_5ac2d11c:


    "Mis palabran sonaron obviamente bruscas, pero ella no pareció darle mucha importancia."


translate spanish day6_us_26d880cd:


    sl "Ella está en el club de música."


translate spanish day6_us_95bfa391:


    "Slavya estaba en lo cierto, me encontré con Alisa cerca del club de música.{w} Sin embargo, estaba acompañada de Miku."


translate spanish day6_us_8a7949da:


    me "¿Te importaría explicarte?"


translate spanish day6_us_ba464770:


    "No perdí el tiempo en comentarios amables."


translate spanish day6_us_a36d4b17:


    dv "No. ¿Qué te hace pensar eso?"


translate spanish day6_us_01ffd8fc:


    "Alisa me hizo una bonita sonrisa."


translate spanish day6_us_c01cc6ab:


    mi "¡Oh, Semyon, hola! ¡Qué bien que estés por aquí también! ¿Quizás nosotros tres podríamos tocar alguna cosa? Tengo una nueva canción, sabes, ¡es muy divertida! ¡Estoy segura de que te gustará, te lo prometo!"


translate spanish day6_us_90c9fd46:


    mi "O podemos escoger algo antiguo..."


translate spanish day6_us_6dae2579:


    "Interrumpí la conversación de Miku."


translate spanish day6_us_0259c598:


    me "Podrías..."


translate spanish day6_us_57d2c9aa:


    mi "¿Qué?"


translate spanish day6_us_90f2e3fb:


    "Ella me miró con la sonrisa de una niña que no comprende nada. Desde aquel momento entendí exactamente qué se le pasaba por la cabeza a ella... pequeños conejitos, ositos de peluche, gatitos... pero ni rastro de inteligencia."


translate spanish day6_us_b05f8c8c:


    me "Necesito hablar con Alisa."


translate spanish day6_us_7cbb1d69:


    "Dije en un tono que impedía discusión alguna."


translate spanish day6_us_94b93a6a:


    mi "Vale, tal vez más tarde..."


translate spanish day6_us_980963c1:


    "Parecía que se enfadó..."


translate spanish day6_us_6cdf2111:


    th "¡Aunque me trae sin cuidado!"


translate spanish day6_us_1cb52a30:


    me "Bien..."


translate spanish day6_us_0b01579b:


    dv "No entiendo qué quieres decir."


translate spanish day6_us_c11d0305:


    "Dijo Alisa, ofendida."


translate spanish day6_us_f16ef830:


    dv "Y de todos modos, ¡me tengo que ir!"


translate spanish day6_us_17bb7bcb:


    "Se dio la vuelta y estaba a punto de irse."


translate spanish day6_us_75807571:


    "Bruscamente, agarré de la muñeca a Alisa."


translate spanish day6_us_ba86d4d5:


    dv "¡¿Qué estás haciendo?!"


translate spanish day6_us_e5fbd716:


    "La muchacha estaba asustada de verdad."


translate spanish day6_us_27478259:


    me "¡Ey, no tan rápida!"


translate spanish day6_us_97c863d3:


    "Ella se detuvo y se giró hacia mí."


translate spanish day6_us_c644793a:


    me "No estoy aquí para matarte..."


translate spanish day6_us_30c1f806:


    "Mis palabras sonaron a broma pero Alisa se encogió."


translate spanish day6_us_ec416668:


    me "¿Quién le contó a la líder del campamento que Ulyana me vino por la noche?"


translate spanish day6_us_3e69c35f:


    dv "No lo sé."


translate spanish day6_us_e657f196:


    "Alegó lastimosamente."


translate spanish day6_us_c3effd86:


    me "Ya lo hablamos esta mañana, ¿no?"


translate spanish day6_us_0d3c2df7:


    "Ella no dijo nada, pero continuó mirándome con miedo en sus ojos."


translate spanish day6_us_45e343a5:


    me "Y lo más importante... ¿cuál es el propósito de contar miles de mentiras?"


translate spanish day6_us_ad8d4b66:


    dv "¡De verdad que no sé de qué me estás hablando!"


translate spanish day6_us_5dbc6805:


    "Sus ojos estaban llenos de lágrimas."


translate spanish day6_us_7e881d63:


    "Repentinamente, me sentí como si me hubiera fulminado un relámpago."


translate spanish day6_us_97da91c1:


    th "¿Y si ella no es culpable?{w} ¿Y por qué creí inmediatamente que fue ella?{w} Cualquiera podría habernos visto fácilmente ayer por la noche."


translate spanish day6_us_8cce8ea8:


    th "Nunca sabes. Incluso Ulyana podría habérselo contado a cualquiera."


translate spanish day6_us_b77a7f8b:


    me "¿Estás segura?"


translate spanish day6_us_09073ec6:


    "Le pregunté con severidad."


translate spanish day6_us_3a950d86:


    dv "Sí."


translate spanish day6_us_cea34798:


    "Pensé durante un segundo, lo cual fue suficiente para que Alisa se escapara."


translate spanish day6_us_b2d3e6d4:


    th "Ahora la única verdadera solución es volver a hablar con Olga Dmitrievna."


translate spanish day6_us_7149be6f:


    "La hallé en el porche de la cantina."


translate spanish day6_us_37e4ac0e:


    "En aquel momento, el sol ya había casi desaparecido en el horizonte."


translate spanish day6_us_636e78e7:


    mt "Viniste después de todo..."


translate spanish day6_us_8c1c36bb:


    "Comenzó ella."


translate spanish day6_us_1a590f52:


    me "Olga Dmitrievna, ¿podrías decirme francamente, quién te lo dijo?"


translate spanish day6_us_61a9bb1f:


    me "Acabo de hablar con Dvachevskaya... no es ella. Ulyana no lo haría por sí misma, así que... ¿quién es?"


translate spanish day6_us_373aadf9:


    "De hecho, ni yo mismo me percaté de por qué necesitaba saberlo."


translate spanish day6_us_7e4fe70a:


    th "Al principio estaba tremendamente enfadado con Alisa, pero ahora..."


translate spanish day6_us_d1318244:


    mt "Bueno..."


translate spanish day6_us_6e6e30b3:


    "Dudó."


translate spanish day6_us_eeb01990:


    mt "Una extraña muchacha...{w} Pero aun así eso no..."


translate spanish day6_us_09fba842:


    me "¿Una extraña muchacha?"


translate spanish day6_us_7d0924ac:


    mt "Bueno, sí... No parecía de nuestro campamento."


translate spanish day6_us_80770549:


    me "¿Por qué te explicó eso?"


translate spanish day6_us_336d97d4:


    mt "¡¿Cómo voy a saberlo?!"


translate spanish day6_us_e6e7decd:


    me "Olga Dmitrievna, pero esto es..."


translate spanish day6_us_5c589d2a:


    "Respiré profundamente y me di la vuelta."


translate spanish day6_us_ed919e5f:


    th "Sencillamente, ¿por qué tendría que mentir ella tan obviamente?"


translate spanish day6_us_483b4ef7:


    th "Aunque, ¿quizás está tratando de proteger a alguien?{w} Digamos...{w} Maldita sea, no puedo ni imaginármelo."


translate spanish day6_us_4366e696:


    me "Muy bien..."


translate spanish day6_us_76102a17:


    "Dije en voz baja y caminé raudo alejándome de la cantina."


translate spanish day6_us_25f025cf:


    "No tenía ningún deseo de continuar escuchando las estúpidas mentiras de Olga Dmitrievna."


translate spanish day6_us_e2e7f765:


    th "Nuestra bien nombrada «líder de campamento»..."


translate spanish day6_us_b46ab80b:


    "Caminé sin rumbo, divagando completamente ensimismado."


translate spanish day6_us_390dbdba:


    us "¿Qué estás de guardia?"


translate spanish day6_us_16e9259d:


    "Escuché una voz alegre detrás de mí."


translate spanish day6_us_fb91294d:


    "Era Ulyana."


translate spanish day6_us_31ff7fd5:


    me "Nada de especial, sólo..."


translate spanish day6_us_c6821b44:


    "Parecía que no había posibilidades de descubrir quién era el informante de Olga Dmitrievna, y que el incidente de ayer fue silenciado de alguna forma, así que decidí no pensar en ello."


translate spanish day6_us_09d23ad2:


    us "¿Qué tienes pensado para esta noche?"


translate spanish day6_us_5203c9d1:


    me "Supongo que nada..."


translate spanish day6_us_29fcfde5:


    "Tras todos mis intentos frenéticos por ocultar la verdad, sentí una inexplicable culpa frente Alisa y Ulyana."


translate spanish day6_us_8eec6359:


    "A pesar de que no estaba claro de qué clase de culpabilidad se trataba frente a Ulyana."


translate spanish day6_us_876ae211:


    us "Pues tengo una sugerencia."


translate spanish day6_us_094d6b91:


    me "¿Cuál es?"


translate spanish day6_us_288bb33e:


    us "Me traje una cinta de vídeo de casa..."


translate spanish day6_us_6d3b56e5:


    "Sus ojos brillaron conspiratoriamente."


translate spanish day6_us_f95c9fc9_1:


    me "¿Y?"


translate spanish day6_us_2b9ac9b1:


    th "No me podía acordar ya de cuándo fue la última vez que vi o escuché alguna cosa grabada en una cinta de vídeo."


translate spanish day6_us_b54f9cfb:


    us "¡Qué tiene una película genial!"


translate spanish day6_us_33f86965:


    me "¿Dónde vamos a conseguir un reproductor de cintas de vídeo aquí?"


translate spanish day6_us_9b9a6c28:


    "Esas palabras me surgieron de mi pasado y decidí poner a prueba cuán ciertas eran mis suposiciones sobre en qué época había ido a parar."


translate spanish day6_us_635b72e6:


    us "¡En el club de cibernética por supuesto!"


translate spanish day6_us_4ce76f25:


    me "No hay nada de eso ahí."


translate spanish day6_us_449236f5:


    "Dije con seguridad."


translate spanish day6_us_6fdda8ad:


    us "¿Y qué hay de la habitación trasera?"


translate spanish day6_us_fc831b50:


    "Estaba en lo cierto.{w} Podría haber allí."


translate spanish day6_us_2d3bdcfb:


    me "Bueno tal vez...{w} Pero de todos modos, es demasiado tarde y todo está cerrado."


translate spanish day6_us_ba96048e:


    "Ese extraño sentimiento de culpabilidad que sentía frente a Ulyana no me daba oportunidad de rechazarla inmediatamente."


translate spanish day6_us_d82bc37c:


    us "¡Podemos entrar por allí a través de la ventana!"


translate spanish day6_us_17cfa934:


    "Se rio con malicia."


translate spanish day6_us_75cef782:


    me "Bueno, sabes..."


translate spanish day6_us_6533f6ed:


    us "Quizá tengas razón. Ojalá pudiéramos tener una llave..."


translate spanish day6_us_2b00c58d:


    "Caviló Ulyana."


translate spanish day6_us_71a7833d:


    th "¡Pero yo tenía una!{w} En mi bolsillo."


translate spanish day6_us_433d2c22:


    "Y justo me acordé ella."


translate spanish day6_us_4e580acc:


    me "Ahora que lo dices, ¡pues sí! ¡Tengo una llave!"


translate spanish day6_us_001cfbf1:


    "La próxima vez, mejor me lo pienso antes de hablar."


translate spanish day6_us_24a62d16:


    us "¡Eso es bueno! Pues iré corriendo rápidamente para coger la cinta, espera aquí que ahora mismo vuelvo."


translate spanish day6_us_8b7b1283:


    "Ella desapareció antes incluso de que pudiera abrir mi boca."


translate spanish day6_us_983ee57d:


    "Es extraño que ella no se sorprendiera de que la llave para la puerta del club de cibernética resultara estar, misteriosamente, en mi bolsillo en el lugar y momento adecuados."


translate spanish day6_us_b974419e:


    "Pero en cualquier caso, tenía que escoger qué hacer."


translate spanish day6_us_d21cc0d2:


    "La cosa más sensata y razonable era no ir a ninguna parte con ella.{w} Pero me sentía culpable..."


translate spanish day6_us_dddd87f0:


    th "A pesar de que me parecía que no había nada de criminal en ver una película junto a ella."


translate spanish day6_us_1b5d4998:


    th "Pero por otra parte, con Ulyana incluso las cosas más inofensivas se pueden volver en un pandemónium.{w} Como mínimo."


translate spanish day6_us_f97a40c8:


    "Ya empezaba a pensar que olvidaría todo lo que había ocurrido esta tarde, sobre mis intentos frenéticos de descubrir la verdad sobre Alisa y Olga Dmitrievna..."


translate spanish day6_us_a20cefa7_3:


    "..."


translate spanish day6_us_1dc2a2a7:


    "En sólo un par de minutos ella retornó."


translate spanish day6_us_bcf9c5aa:


    us "¿Más pensamientos?"


translate spanish day6_us_df7954e6:


    "Miré a Ulyana y me sentí un poco incómodo."


translate spanish day6_us_1c3f5164:


    us "¿Vamos?"


translate spanish day6_us_a4c1c729:


    me "Escucha, sabes, tus ideas son...{w} digamos...{w} no muy acertadas."


translate spanish day6_us_e1f1e8c6:


    me "Y al final todo acabará mal otra vez."


translate spanish day6_us_5d3c20b3:


    us "¿Estás asustado?"


translate spanish day6_us_bcadcefa:


    me "No lo estoy."


translate spanish day6_us_7714aa0d:


    me "Solamente soy un hombre maduro, y no estoy interesado en tales juegos."


translate spanish day6_us_35cd0fa9:


    us "¿Hombre maduro?"


translate spanish day6_us_ec2c2a7c:


    "Ella se rio."


translate spanish day6_us_359cc1c1:


    th "Desde luego, me olvidé que tenía aspecto de diecisiete como mucho."


translate spanish day6_us_9a385a27:


    us "¿Qué te hace pensar eso?"


translate spanish day6_us_6f30efaf:


    "Era difícil de verdad de aceptar, pero no tenía nada que decir al respecto."


translate spanish day6_us_0c18ac79:


    "Ahora que lo pienso... todo el tiempo que me he pasado aquí y no he tenido ni un sola reflexión en particular, ni como experiencia vital ni pensando en frío, que sirva para examinar y ponderar la situación."


translate spanish day6_us_98eb7069:


    "Aunque la otra cuestión es... ¿acaso hice gala de eso en {i}aquella{/i} vida?"


translate spanish day6_us_242a5814:


    th "Y de hecho, incluso si lo pienso...{w} Podría recordar algunos ejemplos por supuesto, pero..."


translate spanish day6_us_7a9e2886:


    me "Vale, vayamos."


translate spanish day6_us_c52254db:


    "Estaba tan ofendido por las palabras de Ulyana que le pregunté:"


translate spanish day6_us_537e718c:


    me "¿Y qué es un hombre maduro en tu opinión?"


translate spanish day6_us_1998fdba:


    us "Uno que es responsable por sus acciones. Uno que no hace algo si primero no ha pensado en las consecuencias. Una persona que es capaz de cuidarse no solamente a sí mismo, sino también a los demás."


translate spanish day6_us_27bca7e1:


    me "Y todo eso no te describe a ti, eso seguro."


translate spanish day6_us_bb9ef712_1:


    "Traté de reírme a carcajadas."


translate spanish day6_us_c23eb093:


    us "Tampoco pretendo ser de ese modo."


translate spanish day6_us_ddb5934e:


    "Sonrió ampliamente Ulyana."


translate spanish day6_us_7efaef44:


    "Sí, estaba completamente en lo cierto.{w} No poseo ninguna de esas cualidades."


translate spanish day6_us_31f38e05:


    "Pero siempre pensé que no ser como un adolescente, es todo lo que se requiere."


translate spanish day6_us_35c738f1:


    "Parece que, en general, no hay diferencia entre yo y la otra gente, y si lo deseara podría ser exactamente como son ellos."


translate spanish day6_us_bd922584:


    th "¿Acaso es incorrecto?"


translate spanish day6_us_0a98cbd7:


    "De hecho... no, porque en este campamento traté de comportarme de la forma más lógica, racional, prudente y adecuada...{w} ¡como un adulto!"


translate spanish day6_us_53d1df42:


    th "Pero si incluso Ulyana dice lo contrario..."


translate spanish day6_us_bc130c9d:


    th "¡¿Pues entonces cómo demonios puedo escapar de aquí?!"


translate spanish day6_us_cc350778:


    "Entramos. Le llevó algo de tiempo a Ulyana encontrar el interruptor de luz."


translate spanish day6_us_ded0fd48:


    us "¡Presto!"


translate spanish day6_us_5e0cd80f:


    me "¡Muy bien, bravo!"


translate spanish day6_us_77e18087:


    "Aplaudí exageradamente con mis manos."


translate spanish day6_us_ad0cb0ae:


    us "¡Ey! ¡Deja de burlarte de mí!"


translate spanish day6_us_e16a3b7b:


    "Ella hizo un puchero."


translate spanish day6_us_80d646aa:


    me "Vale, ¿y ahora qué?"


translate spanish day6_us_3671b8c0:


    us "¡Por aquí!"


translate spanish day6_us_d42abb00:


    "Ulyana fue a la puerta de la segunda habitación, dudó por un instante y empujó para abrir."


translate spanish day6_us_724ee48f:


    "No había mucho espacio en su interior. Parecía más bien que esta habitación se usara para almacenar."


translate spanish day6_us_7c05bc4a:


    "Las cajas amontonadas las unas con las otras habían formado un tipo de montaña como los Alpes en miniatura; varios dispositivos desperdigados aquí y allá, me recordaban a una furiosa y caótica lluvia de ideas en la mente de un científico; las estanterías en la pared del fondo dejaban claro que no era un simple almacén para trastos sin usar."


translate spanish day6_us_d39afe89:


    "A mi derecha había una televisión junto a un reproductor de vídeo abajo."


translate spanish day6_us_a3c51985:


    "Interesante, no eran soviéticos... Diría que eran japoneses.{w} Al menos por su aspecto."


translate spanish day6_us_62c4c780:


    "No muy sorprendente, en aquellos días (o para ser más exactos... en estos días) las importaciones ya existían."


translate spanish day6_us_8bf0de68:


    us "¡Qué te dije!"


translate spanish day6_us_709cc631:


    "Ulyana sonreía triunfantemente."


translate spanish day6_us_ff471f9a:


    me "Claro, claro..."


translate spanish day6_us_6c9f5d6f:


    us "¿Has visto algo como esto antes?"


translate spanish day6_us_756a29e3:


    me "Yo..."


translate spanish day6_us_46586735:


    "Quizá tengo un reproductor de cintas de vídeo también. Probablemente esté tirado por alguna parte en la guardilla de mi hogar."


translate spanish day6_us_cd67e98a:


    us "Parece que no estás muy sorprendido."


translate spanish day6_us_95322256:


    me "¿Por qué iba a estar sorprendido?"


translate spanish day6_us_2c209cc5:


    us "¡Una televisión japonesa con un reproductor de vídeo! ¡Lo dices como si vieras a menudo cosas así!"


translate spanish day6_us_69f8f8a0:


    me "Mmm... no muy a menudo últimamente."


translate spanish day6_us_78674bdc:


    "Me puse de acuerdo perezosamente."


translate spanish day6_us_7505e340:


    us "Qué pesimista eres..."


translate spanish day6_us_469db7d9:


    us "¡No obstante, vale! ¡Coge!"


translate spanish day6_us_4e5aa691:


    "Ella me dio una cinta.{w} Le di unas vueltas en mis manos, pero no encontré ninguna pegatina o etiqueta."


translate spanish day6_us_fb03e6d3:


    me "¿Y de qué es?"


translate spanish day6_us_d23e2c34:


    us "No lo sé exactamente."


translate spanish day6_us_9137ce1a:


    "Reflexionó Ulyana."


translate spanish day6_us_3bb74eea:


    us "¡Pero estoy segura de que es algo muy interesante!"


translate spanish day6_us_4121ba2c:


    "Pronto el logotipo de una muy conocida compañía americana de películas apareció en la pantalla."


translate spanish day6_us_7331bf96:


    us "¡Mira, mira, ahí va!"


translate spanish day6_us_4653fbcc:


    "Ulyana comenzó incluso a impacientarse nerviosa."


translate spanish day6_us_1b32e37e:


    me "Lo veo..."


translate spanish day6_us_f7aa1f59:


    "Unos minutos más tarde, me percaté de que la cinta tenía una de las películas más famosas de los ochentas. Era sobre un cyborg del futuro, enviado para matar a alguien del pasado, de esa manera no nacería el héroe durante esos años... "


translate spanish day6_us_f027422b:


    "Interesante coincidencia.{w} Se podría decir que yo también fui enviado del futuro."


translate spanish day6_us_5a80f756:


    th "Pues entonces... ¿Mi misión es matar alguien de entre los lugareños?"


translate spanish day6_us_aa60fb72:


    "No pude evitar reírme."


translate spanish day6_us_686edcc3:


    us "¿Qué te hace tanta gracia?"


translate spanish day6_us_1a66e4d6:


    "Ulyana me miraba con reproche."


translate spanish day6_us_3c5e2cfc:


    me "Oh, no, nada."


translate spanish day6_us_b1a006e1:


    us "¡Pareciera que no te gusta la película!"


translate spanish day6_us_731f7b7d:


    me "La película está bien.{w} La he visto diversas veces."


translate spanish day6_us_433e89df:


    us "¿Cómo puede ser eso?"


translate spanish day6_us_41c9ba10:


    "Sus ojos se abrieron como platos sorprendida."


translate spanish day6_us_78ab486a:


    me "Bueno...{w} Mi amigo me prestó una cinta."


translate spanish day6_us_6532261a:


    us "Mmm..."


translate spanish day6_us_e2092d77:


    "Ulyana me miró con atención."


translate spanish day6_us_a9137dac:


    me "De todas formas, continuemos viéndola."


translate spanish day6_us_e868da76:


    "Señalé a la pantalla."


translate spanish day6_us_993a1e1e:


    "Cuando la película estaba más o menos por la mitad, escuché a alguien tratando de abrir la puerta de fuera."


translate spanish day6_us_a2f7bc76:


    "Se dice que cuando una persona tiene uno de sus instintos más flojos, los otros son mucho mejores.{w} Estaba bastante a ciegas, pero podía escuchar perfectamente."


translate spanish day6_us_0eebb0ce:


    me "¡Apaga las luces!"


translate spanish day6_us_9db6b90d:


    "Susurré."


translate spanish day6_us_b7ef3111:


    us "¿Qué?"


translate spanish day6_us_c18f0f1a:


    me "¡Que apagues las luces!"


translate spanish day6_us_39642f61:


    "Ulyana se dio cuenta de que éste no era el momento para discutir conmigo y corrió hasta el interruptor.{w} Mientras tanto, apreté el botón de pausa."


translate spanish day6_us_955fefc7:


    "Escuché pasos en la otra habitación y vi parpadear la luz de una linterna bajo la puerta."


translate spanish day6_us_bf71caa2:


    th "Me pregunto quién decidió escabullirse en el club de cibernética por la noche..."


translate spanish day6_us_de372b78:


    mt "No están aquí."


translate spanish day6_us_d099a808:


    "Era la voz de Olga Dmitrievna."


translate spanish day6_us_9b04210e:


    "Pronto la puerta de fuera se cerró de golpe, y respiré con alivio."


translate spanish day6_us_505772f4:


    me "¿Lo ves? ¡Otra vez por tu culpa!"


translate spanish day6_us_b7ef3111_1:


    us "¿Qué?"


translate spanish day6_us_50398fc3:


    "Ulyana me miraba fijamente sorprendida."


translate spanish day6_us_bd77af7a:


    me "¿Por qué iba venir ella aquí por la noche?{w} ¡Está claro que es porque nos busca!"


translate spanish day6_us_2405d879:


    me "A Alisa poco le importa tu ausencia, ¡pero yo figuro asignado en la cabaña de la líder del campamento!"


translate spanish day6_us_60f3d758:


    "Ella pensó por un rato."


translate spanish day6_us_2072b7ef:


    us "Bueno, ¿y qué?"


translate spanish day6_us_e4a7abab:


    me "¿Y qué...? ¿Por qué constantemente estás buscando problemas? Es casi como si te gustara."


translate spanish day6_us_10573fa6:


    us "¡Es más divertido así!"


translate spanish day6_us_08fab8ce:


    me "¿Es divertido cuando te regañan y castigan constantemente?"


translate spanish day6_us_0e694a7f:


    us "¡Quien no arriesga, no gana!"


translate spanish day6_us_7a6bb0f1:


    "Ella se rio alegremente."


translate spanish day6_us_ff350d77:


    me "En ocasiones, también hay reglas.{w} Bueno, hasta ahora has evitado consecuencias graves, ¡pero quién sabe lo que podría ocurrirte la próxima vez! ¡Podría acabar muy mal!"


translate spanish day6_us_43d44c0e:


    us "¡Te quejas como una abuela!"


translate spanish day6_us_3db311fc:


    "Dijo ofendida."


translate spanish day6_us_23e36b36:


    me "¿Y quién dijo hace poco que no podía ser considerado un adulto?"


translate spanish day6_us_cff7648a:


    us "Olvídalo, acabemos de ver la película."


translate spanish day6_us_d07a4ff2:


    "Decidí no discutir con ella."


translate spanish day6_us_e7c03f19:


    th "A fin de cuentas, ya estamos en la lista de Olga Dmitrievna, y una hora antes o después... ¿realmente importa?"


translate spanish day6_us_e487a8f7:


    "En cada escena de miedo Ulyana daba un respingo, chillaba o me agarraba de la mano."


translate spanish day6_us_0958d1fb:


    "No estaba muy entusiasmado con la película, pero aun así simulé estar también interesado."


translate spanish day6_us_12d2bbe0:


    "Además del cyborg malvado, había un hombre bueno enviado desde el futuro para evitar sus malvados planes."


translate spanish day6_us_9a8a93f1:


    th "Quizá no estoy aquí con el fin de convertirme en una brutal máquina de matar, sino por el contrario, para cambiar o prevenir algo..."


translate spanish day6_us_a140a31c:


    th "Para evitar algún acontecimiento, por ejemplo, o para detener a alguien que haga algún delito..."


translate spanish day6_us_2d270f7c:


    th "La analogía era bastante interesante, pero no se basaba en ningún hecho real."


translate spanish day6_us_dd48914d:


    th "No tengo ninguna escopeta en mis manos, no estoy portando ninguna chaqueta de cuero y no tengo una motocicleta que conducir..."


translate spanish day6_us_30190641:


    th "¡Ni tan siquiera tengo gafas de sol!"


translate spanish day6_us_8d2a1158:


    us "¡Mira, mira!"


translate spanish day6_us_e99c1fdf:


    "La siguiente persecución se desplegaba en la pantalla con disparos y destrucción de varios tipos de vehículos a ruedas y con hélices."


translate spanish day6_us_7d3685bb:


    me "¿Crees que el tipo bueno ganará?"


translate spanish day6_us_d6b61a8f:


    "Por supuesto, sabía la respuesta."


translate spanish day6_us_6c856ecd:


    us "¡Obviamente!{w} ¡El bien siempre triunfa!"


translate spanish day6_us_15e55f1b:


    "Dijo con seriedad."


translate spanish day6_us_90e45f3b:


    "Si consideramos que Ulyana a menudo se sale con éxito en sus absurdas aventuras, el significado de la palabra «bien» tiene uno retorcido."


translate spanish day6_us_3bc8c3ba:


    us "¡Espera! Pero si tú ya la has visto."


translate spanish day6_us_0886d374:


    me "Claro, solamente quería saber tu opinión..."


translate spanish day6_us_1a8f9a59:


    "Pronto la película llegó a su fin."


translate spanish day6_us_8b49353c:


    "Durante las escenas finales Ulyana corrió alrededor de la habitación, tratando de conseguir el mejor punto de vista de todo lo que sucedía en la pantalla sin perderse nada."


translate spanish day6_us_1c0e27c2:


    us "¡Uff!"


translate spanish day6_us_80984763:


    "Dejó ir con alivio cuando los créditos finales empezaron."


translate spanish day6_us_c874b643:


    me "¿Te gustó?"


translate spanish day6_us_ae076645:


    us "¡¿Acaso lo preguntas?! ¡Pues claro!"


translate spanish day6_us_f2e007f0_1:


    me "Eso es bueno..."


translate spanish day6_us_8b59d260:


    us "Pues no lo parece, de verdad."


translate spanish day6_us_c4bf4ad4:


    "Me hizo una miradita de reojo."


translate spanish day6_us_09b227d4:


    me "Bueno, es una película para críos después de todo."


translate spanish day6_us_f868e368:


    us "Ah... ¿De verdad?"


translate spanish day6_us_ed8eb672:


    me "Bueno...{w} Claro, ¿y qué?"


translate spanish day6_us_565373fe:


    us "Nada."


translate spanish day6_us_9827d8ef:


    "Bostezó y se sentó en el suelo, apoyando su espalda contra el montón de cajas."


translate spanish day6_us_46f6e5ee:


    me "Probablemente es la hora de irse a dormir."


translate spanish day6_us_b1586995:


    us "Estoy cansada y no me iré a ninguna parte."


translate spanish day6_us_b8e7bf20:


    me "Como desees.{w} Mejor me iré pues."


translate spanish day6_us_ba7d98ab:


    us "¡Ey, espera!"


translate spanish day6_us_80abfaf2:


    "Ulyana inmediatamente se puso en pie y me agarró de la mano."


translate spanish day6_us_38a6668d:


    us "¿Me vas a dejar sola aquí?"


translate spanish day6_us_4cad9cd9:


    me "Bueno, si quieres, ¿debería llevarte a cuestas hasta tu cabaña?"


translate spanish day6_us_fefef18f:


    "Dije inseguro."


translate spanish day6_us_3f9a7fc5:


    "A fin de cuentas, ya había experimentado eso."


translate spanish day6_us_b1823081:


    us "¡No! ¡Quiero dormirme aquí!"


translate spanish day6_us_94133a62:


    "Hurgó en la caja a su lado y sacó algún tipo de sábanas y cojines."


translate spanish day6_us_4bff6f94:


    me "De nuevo la misma historia..."


translate spanish day6_us_25f24811:


    "Suspiré."


translate spanish day6_us_10c6982a:


    me "Solamente piensa. Olga Dmitrievna ya nos está buscando. Y si nos pasamos toda la noche aquí..."


translate spanish day6_us_443d35ef:


    us "Bueno, primeramente, ella te está buscando a ti. Eso no tiene nada que ver conmigo."


translate spanish day6_us_be3315b6:


    "Se rio Ulyana."


translate spanish day6_us_fc9e5e8f:


    me "Aunque fuera así, ¡entonces tú también serás descubierta!"


translate spanish day6_us_ff3c9f13:


    us "¿Qué te hace pensar eso?{w} Y si me descubren, pues está bien, ¡estoy acostumbrada a ello!"


translate spanish day6_us_7eb76897:


    me "¿Por qué eres tan cabezona? Solamente hay un par de cientos de metros hasta tu cabaña."


translate spanish day6_us_737dc545:


    "Así como que tampoco podrías decir que Ulyana estuviera completamente exhausta."


translate spanish day6_us_4c76cf23:


    us "¡Estoy muy cansada!"


translate spanish day6_us_3a4d70a0:


    "Ella se envolvió entre una sábana, me dio la espalda y comenzó diligentemente a hacer falsos ronquidos."


translate spanish day6_us_6cacb19d:


    me "Vale pues, nos vemos mañana."


translate spanish day6_us_8f11bfa0:


    "Traté de irme, pero Ulyana me volvió a agarrar de la mano."


translate spanish day6_us_69f4e5e6:


    me "Bueno, ¿y ahora qué?"


translate spanish day6_us_8627abec:


    "No dijo nada. Parecía que la hubiera asustado."


translate spanish day6_us_feabf980:


    "Muy bien pues, lo que debería hacer estaba completamente poco claro."


translate spanish day6_us_873663f3:


    th "Nuestra pequeña aventura con la cinta fue una idiotez desde un principio, pero ahora ya estaba fuera de mi comprensión."


translate spanish day6_us_2ba6c73f:


    "No tenía ni idea de qué lógica había tras sus acciones. Qué intenciones y motivaciones eran."


translate spanish day6_us_0ffe8ded:


    "Sin embargo, a pesar de todo esto, no podía decir que «no» firmemente."


translate spanish day6_us_08b7211c:


    "Algo me detuvo. Quizá un sentido del deber, o de lástima, o de la paciencia que se debe tener para hacerse entender con los niños."


translate spanish day6_us_849cfae0:


    me "Vale, ¿qué es lo que quieres de mí?"


translate spanish day6_us_f1881724:


    us "¡Ven a dormir conmigo!"


translate spanish day6_us_9a9afc69:


    "Me parece que eso ya lo he visto en otra parte."


translate spanish day6_us_b01cd732:


    me "Bueno, aceptémoslo..."


translate spanish day6_us_4645bec9:


    "Apagué las luces y me acurruqué a su lado."


translate spanish day6_us_ca6c74b8:


    "Por lo menos era época de verano, de otra modo dormir en el suelo de madera no sería muy buena idea."


translate spanish day6_us_2ed1402b:


    th "La falta de un colchón no hace falta ni mencionarla."


translate spanish day6_us_376cd472:


    us "¿Por qué has aceptado tan fácilmente?"


translate spanish day6_us_12bc6723:


    "Podía notar resentimiento en su voz."


translate spanish day6_us_87a707b8:


    me "Lo pediste... y así lo hice."


translate spanish day6_us_e8799476:


    "De hecho, estaba completamente convencido de que Ulyana caería dormida rápidamente, como siempre, y que la llevaría a cuestas hasta su cabaña."


translate spanish day6_us_7e1e0002:


    th "Básicamente, es un buen plan a pesar del hecho de que nuevamente servía como burro de carga."


translate spanish day6_us_08c4f608:


    us "¡Ey, háblame!"


translate spanish day6_us_b1158062:


    me "Tan sólo duerme. Eres tú la que quería."


translate spanish day6_us_0bf1db7d:


    us "¡Solamente habla!"


translate spanish day6_us_c233b6a2_1:


    me "¿Sobre qué?"


translate spanish day6_us_c034a3dd:


    us "Cuéntame algo interesante sobre ti, anécdotas graciosas de tu vida."


translate spanish day6_us_0668a617:


    me "Mi vida no tiene nada de particularmente interesante."


translate spanish day6_us_2776dad8:


    us "¿No recuerdas nada?"


translate spanish day6_us_97091cb6:


    th "Incluso aunque tuviera algo, contárselo a ella no era buena idea."


translate spanish day6_us_67eea6e1:


    me "Nada."


translate spanish day6_us_bb57e7a2:


    us "¡No puede ser tan malo!"


translate spanish day6_us_9779d985:


    "Me giré hacia Ulyana."


translate spanish day6_us_691925cb:


    "Ella estaba estirada con sus ojos abiertos como platos, mirando fijamente el techo."


translate spanish day6_us_e0f7ef7d:


    me "Puede ser, créeme."


translate spanish day6_us_22602e79:


    us "¡Pero eso es muy aburrido!"


translate spanish day6_us_ba42ab02:


    me "De tanto en tanto..."


translate spanish day6_us_0e03ae69:


    "Ella se quedó callada por un rato, luego rio y dijo:"


translate spanish day6_us_77b3d75b:


    us "¡Pero aquí te divertiste!"


translate spanish day6_us_c10011a5:


    me "¡Oh, eso es verdad!"


translate spanish day6_us_89bea129:


    "Me reí."


translate spanish day6_us_563002a7:


    us "¿Te acordarás de este campamento?"


translate spanish day6_us_601cfdfa:


    th "Primero tengo que escapar de aquí."


translate spanish day6_us_d06923c2:


    me "Por supuesto, lo haré."


translate spanish day6_us_21d8894d:


    us "¿Y de los otros?"


translate spanish day6_us_50b56044:


    me "Lo haré..."


translate spanish day6_us_f4bcf7be:


    us "¿Y de mí?"


translate spanish day6_us_aa854ae9:


    me "Y de ti..."


translate spanish day6_us_93a0d0b4:


    "Lo dije todo con un tono absolutamente uniforme y, para ser sinceros, sin pensar apenas el significado de sus preguntas."


translate spanish day6_us_e73afb35:


    us "Y yo lo haré..."


translate spanish day6_us_a60d2254:


    "Susurró Ulyana."


translate spanish day6_us_3c6c3d52:


    "Inmediatamente sentí caer su cabeza sobre mi pecho y su brazo rodearme el cuello."


translate spanish day6_us_9f8d7b41:


    me "¿Cómoda?"


translate spanish day6_us_0162dcba:


    us "Claro."


translate spanish day6_us_acaae73d:


    "Murmuró."


translate spanish day6_us_2f598c90:


    me "¿No encontraste mejor lugar?"


translate spanish day6_us_f1d4b5e2:


    "Negó con la cabeza ligeramente."


translate spanish day6_us_1943e342:


    th "Bueno, dejemos que se quede así."


translate spanish day6_us_47e54bbf:


    th "Al final, quizá caerá rendida de sueño más rápidamente."


translate spanish day6_us_a20cefa7_4:


    "..."


translate spanish day6_us_94caab83:


    "Probablemente no pasaron más de cinco minutos antes de que decidiera comprobar si se durmió, así que ligeramente la sacudí por los hombros."


translate spanish day6_us_b7ef3111_2:


    us "¿Qué?"


translate spanish day6_us_322c760e:


    "Me preguntó sin alzar la mirada."


translate spanish day6_us_16a426b8:


    "Me percaté de que su voz era de un tono inusual."


translate spanish day6_us_f075d0eb:


    me "¿Por qué no estás durmiendo?"


translate spanish day6_us_a63d8c58:


    us "¡Mira quien habla!"


translate spanish day6_us_a20cefa7_5:


    "..."


translate spanish day6_us_28e2eb35:


    "El tiempo transcurrió traicioneramente lento."


translate spanish day6_us_d499dbc6:


    "Decidí esperar por lo menos otros diez minutos, antes de comprobar si se durmió al fin."


translate spanish day6_us_8efc776c:


    "Pero los segundos parecían ser horas..."


translate spanish day6_us_7d75fee5:


    "Mis ojos comenzaron a cerrarse lentamente.{w} Traté de vencer el sueño para estar despierto."


translate spanish day6_us_d580dc80:


    "Parpadeé por un instante y me desperté, únicamente para darme cuenta de que en mi estado estuve cerca de perder el conocimiento."


translate spanish day6_us_f9df56b2:


    th "¿Tal vez sólo cerré los ojos por un minuto?{w} ¿Qué hay de terrible en ello?"


translate spanish day6_us_6abdb446:


    "Se me pasó esa idea por la cabeza."


translate spanish day6_us_b8b42840:


    "E inmediatamente divagué..."


translate spanish day6_sl_662a5ccc:


    "No dormí bien."


translate spanish day6_sl_84b3863a:


    "Me seguí despertando a cada minuto o dos, y luego me volví a quedar dormido."


translate spanish day6_sl_8076c467:


    "No soñé mucho. Tan sólo algunas imágenes grotescas, que olvidé por la mañana."


translate spanish day6_sl_b893b471:


    "Olga Dmitrievna me despertó."


translate spanish day6_sl_93a88bb1:


    mt "¡Despiértate ya!"


translate spanish day6_sl_c96aaf8e:


    "Me froté mis ojos y la miré inquisitivamente."


translate spanish day6_sl_f6a4fbf2:


    me "¿Qué hora es?"


translate spanish day6_sl_42495f59:


    mt "¡Bastante tarde!"


translate spanish day6_sl_ea0df862:


    "Mi cuerpo entero me dolía, la cabeza me pesaba como una pelota de bolos."


translate spanish day6_sl_e419b053:


    mt "Ponte en pie y vete a asearte, ¡ahora mismo!"


translate spanish day6_sl_e9440aad:


    "Me levanté obedientemente, agarré mi estuche de aseo y me fui fuera."


translate spanish day6_sl_4a70bb48:


    "No quería hablar con la líder del campamento en este momento."


translate spanish day6_sl_f672dc60:


    th "Iba a darme un sermón claramente.{w} De los duros."


translate spanish day6_sl_1c2ef58a:


    th "Pero lo dejo para cuanto más tarde, mejor."


translate spanish day6_sl_06732be2:


    "Después de todo, siempre hice las cosas de este modo."


translate spanish day6_sl_36a0fcdc:


    "Tratando de no caer dormido mientras caminaba, de alguna manera llegué a los lavabos.{w} El agua fría me ayudó a recuperar los sentidos."


translate spanish day6_sl_ae8a5bf9:


    th "¿Quizá debería hacer una visita a Slavya? Todavía tengo tiempo."


translate spanish day6_sl_a714f4e1:


    th "Sólo para disculparme con ella..."


translate spanish day6_sl_7daba806:


    th "Por otra parte, ¿por qué me tendría que disculpar?{w} Fue su idea."


translate spanish day6_sl_1398f30a:


    th "¿Y qué? ¿Qué hice mal?"


translate spanish day6_sl_8e04721c:


    th "Una vez más me encontraba en situaciones estúpidas, donde todo estaba en mi contra..."


translate spanish day6_sl_3d8cc6b9:


    "Repiré profundamente y fui de vuelta a la cabaña de Olga Dmitrievna, con la firme intención de demostrar mi inocencia, o al menos demostrar que me mantendré en mis trece."


translate spanish day6_sl_cb36cd72:


    "Pero la habitación estaba vacía."


translate spanish day6_sl_efc1096d:


    "Me senté en la cama y me tapé la cara con mis manos."


translate spanish day6_sl_6a13efce:


    th "Necesito inventarme una excusa. Ahora. Una bonita y creíble excusa."


translate spanish day6_sl_114a09ac:


    th "Por si ella no se cree la verdad.{w} Bueno, eso no es sorprendente... si yo fuera ella, tampoco me lo creería..."


translate spanish day6_sl_e967e0d0:


    "Nada que valiera la pena me venía a la mente."


translate spanish day6_sl_ae02f020:


    "Solamente estaba sentado ahí, contemplando el amanecer."


translate spanish day6_sl_f25133e7:


    "La puerta se abrió silenciosamente y la líder del campamento entró en la habitación."


translate spanish day6_sl_3a4485f1:


    me "Olga Dmitrievna, todavía me gustaría explicarte aquello..."


translate spanish day6_sl_cf0c0005:


    mt "No hace falta..."


translate spanish day6_sl_c5e1ce92:


    "La miré, sorprendido."


translate spanish day6_sl_79c7318e:


    mt "¡Lo comprendo todo!"


translate spanish day6_sl_91b3c16d:


    me "¿Entonces comprendes que no era lo que parecía?"


translate spanish day6_sl_6889d390:


    mt "Por qué... ¡era exactamente lo que parecía!{w} No quiero culparos ni a ti ni a Slavya. Lo entiendo todo, la pubertad, las hormonas, todas esas cosas..."


translate spanish day6_sl_47b5cf5f:


    "Suspiré cansadamente y agaché mi cabeza entre mis manos."


translate spanish day6_sl_93a58ce6:


    me "¿Qué es lo que tengo que decirte para que me creas?"


translate spanish day6_sl_c8c68b8a:


    mt "Nada.{w} Ya te lo dije... lo comprendo todo."


translate spanish day6_sl_8ba64c1f:


    me "Tu no entiendes nada..."


translate spanish day6_sl_b459e062:


    "Murmuré en voz baja."


translate spanish day6_sl_f9270db9:


    mt "Vale, ¡dirígite a la formación!"


translate spanish day6_sl_106dc472:


    "Me levanté y me arrastré perezosamente junto a ella."


translate spanish day6_sl_dd234e0f:


    me "Pero, ¿podría al menos hacerle una visita?"


translate spanish day6_sl_b71e2412:


    mt "Por supuesto que no."


translate spanish day6_sl_7318b2da:


    "La líder del campamento me miró con tal sorpresa, como si mi pregunta fuera algo completamente inapropiado."


translate spanish day6_sl_c3fad59f:


    mt "Ya he avisado a la enfermera para que no te deje acceder."


translate spanish day6_sl_c2e10946:


    me "Bueno, ¿me está permitido al menos saber cómo se encuentra?"


translate spanish day6_sl_a809ce74:


    mt "Bueno, en realidad parece que Slavya está enferma."


translate spanish day6_sl_66a3dff1:


    "No logré preguntarle cómo de grave era, en tanto que ya llegamos a la plaza."


translate spanish day6_sl_90404e3b:


    mt "Ve y toma tu posición."


translate spanish day6_sl_2dca417a:


    "Tomé lugar al lado de Electronik."


translate spanish day6_sl_4f3f853d:


    el "Y bien, ¿qué sucedió ayer?"


translate spanish day6_sl_ae5c7f77:


    "Me susurró."


translate spanish day6_sl_cb23542f:


    me "¿De qué estás hablando?"


translate spanish day6_sl_e1649ec8:


    el "Bueno, de repente desapareciste."


translate spanish day6_sl_9553ce6a:


    me "Bueno, nada en especial..."


translate spanish day6_sl_ca0c06f1:


    th "Contarle todo a todo el campamento era la última cosa que quería hacer."


translate spanish day6_sl_3490597b:


    th "Después de todo, si la propia líder del campamento nos malinterpretó, qué pensarían los demás..."


translate spanish day6_sl_b37413a9:


    "A pesar de que ya comencé a creer que no era tan {i}malo{i}."


translate spanish day6_sl_e6dd2114:


    "Toda la formación consistió en pasar lista y anunciar que los baños estaban cerrados hoy para ser reparados."


translate spanish day6_sl_244fe891:


    "Finalmente, los pioneros se fueron corriendo para desayunar, parloteando alegremente.{w} Los seguí miserablemente."


translate spanish day6_sl_ad482dd4:


    "Hablando francamente, no estaba de humor para comer."


translate spanish day6_sl_09b5d477:


    "Tras estar esperando cerca de las puertas de la cantina por un rato, me di la vuelta y me alejé."


translate spanish day6_sl_39e8f495:


    "No sé cómo acabé yendo sobre mis pasos, pero casualmente acabé apareciendo de la nada frente a la enfermería."


translate spanish day6_sl_1a778ee7:


    "Parece que mi piloto automático me condujo."


translate spanish day6_sl_770ba296:


    "Me quedé quieto en la puerta, completamente perdido."


translate spanish day6_sl_703f77b3:


    th "No me dejarán de todos modos...{w} ¿Qué puedo hacer ahora?"


translate spanish day6_sl_bf506fe5:


    th "¿Debería esperar una declaración de Olga Dmitrievna, si es que la hace?{w} ¿Debería esperar hasta que todo el mundo en el campamento lo sepa?"


translate spanish day6_sl_917651b4:


    th "¡Eso es incluso peor!{w} Las malvadas miradas por todas partes, la alienación..."


translate spanish day6_sl_45615f79:


    "Eso es lo que ocurrirá seguramente."


translate spanish day6_sl_6a4721bc:


    th "No, no hice nada malo, pero en el círculo vicioso de esta sociedad tal comportamiento sería tratado, si no como un pecado, claramente como una grave desviación."


translate spanish day6_sl_2f9f3e25:


    th "¡Y eso significa que ya no tengo nada que perder!"


translate spanish day6_sl_991e1a72:


    "Llamé a la puerta sin dudar."


translate spanish day6_sl_89ac2598:


    cs "¡Sólo un momento!"


translate spanish day6_sl_01d2073c:


    "Empujé el pomo de la puerta, pero ésta no se abría."


translate spanish day6_sl_13207c20:


    th "Mmm, ¿está cerrada?"


translate spanish day6_sl_e26ebd50:


    "En unos instantes escuché cómo la llave se deslizaba por la cerradura y apareció la enfermera."


translate spanish day6_sl_a5eeb60e:


    cs "Buenos días, pionero..."


translate spanish day6_sl_32f6d96b:


    me "¡Hola!"


translate spanish day6_sl_df3861de:


    "Respondí con confianza."


translate spanish day6_sl_0e5cbc98:


    cs "Bueno, ¿sabes que no te está permitido estar aquí?"


translate spanish day6_sl_1654d9af:


    me "Esto... lo sé...{w} Pero si tú..."


translate spanish day6_sl_e5ee009f:


    cs "Qué va... ¡Así no es como funcionan las cosas!"


translate spanish day6_sl_7665ceec:


    "Dijo alegremente."


translate spanish day6_sl_a786531b:


    cs "Una orden de la líder del campamento es una orden después de todo.{w} Si me preguntas a mi, personalmente, no encuentro nada de inmoral en tu comportamiento, pero Olga Dmitrievna..."


translate spanish day6_sl_8b820e8f:


    me "¿Quizá pueda hacer alguna cosa...?"


translate spanish day6_sl_5d8ff9ca:


    cs "No."


translate spanish day6_sl_cb795a40:


    "La miré con rabia, me di la vuelta y me alejé caminando con lentitud teatrera."


translate spanish day6_sl_34d50336:


    "No sabía qué hacer..."


translate spanish day6_sl_2e89c23b:


    "Tan sólo me apresuré hacia delante, dando un puñetazo a los árboles que me encontrara por el camino."


translate spanish day6_sl_a20cefa7:


    "..."


translate spanish day6_sl_a2e15f1e:


    "Me recuperé de esta explosión de rabia solamente cuando mis manos me dolieron gravemente, y mis labios estuvieron mordidos hasta sangrar."


translate spanish day6_sl_1947d5ea:


    "Y la cosa más interesante es que todo mi increíble despertar en un campamento de pioneros desconocido se ha vuelto en el más trivial drama de la vida.{w} Un drama tan estúpido como mi entera existencia."


translate spanish day6_sl_82da43b7:


    "Y una vez más hice algo mal, pensé alguna cosa mala y ellos lo malinterpretaron..."


translate spanish day6_sl_a51631b5:


    th "Está bien pues, dejemos que todo sea..."


translate spanish day6_sl_c0292f4c:


    "No había nadie cerca de la cantina."


translate spanish day6_sl_c23a417d:


    "Suspiré de alivio y tomé lugar en el alejado rincón, esperando que nadie me vería."


translate spanish day6_sl_ac6511c6:


    "Rellenando mi bandeja con algo de desayuno, estaba a punto de irme ya cuando Miku salió corriendo hasta mi mesa."


translate spanish day6_sl_337999bd:


    mi "¡Oh, buenos días, Semyon! Hoy me dormí más o menos... ¿Puedo tomar lugar junto a ti?"


translate spanish day6_sl_705add58:


    "Ella se sentó directamente."


translate spanish day6_sl_4b92e09f:


    me "Bueno, ya me estaba yendo..."


translate spanish day6_sl_579406ce:


    mi "¿Es verdad que tú y Slavya estáis... así? Bueno, ya sabes que quiero decir."


translate spanish day6_sl_5b30cd59:


    "Ella se me quedó mirando fijamente de cerca, parpadeando sus ojos."


translate spanish day6_sl_b9e0f0b2:


    me "¿Quién te dijo eso?"


translate spanish day6_sl_e3364d6a:


    "Bueno, uno podía esperar que los rumores circularan rápido.{w} ¡Pero no tan rápido!"


translate spanish day6_sl_331ec19b:


    "Comenzaba a encolerizar lentamente."


translate spanish day6_sl_f4fd2d5d:


    mi "Lo escuché de Zhenya, ella probablemente lo escuchó de Cheesekov. Él lo escuchó de Olga Dmitrievna. No quiero que me malinterpretes... no es asunto mío pero... ¡Si no quieres, pues no contestes! Pero si no es un secreto..."


translate spanish day6_sl_a3ccfa9b:


    me "Tienes razón. No quiero responder."


translate spanish day6_sl_1203e79e:


    "Me levanté y, rápidamente, me dirigí hacia la salida."


translate spanish day6_sl_23fd5e58:


    "Era un día caluroso.{w} Esto aumentó mi rabia, en tanto que no me gusta la calor."


translate spanish day6_sl_57084d4c:


    "A pesar de que ahora mismo, cualquier cosa podría sacarme de mis casillas.{w} Pero ahora tenía que hacerme a la idea para recuperar las riendas de mí mismo."


translate spanish day6_sl_50ccb861:


    "No quería salir corriendo tras nadie, así que me adentré en el bosque donde podría pensar con calma."


translate spanish day6_sl_1d5b76c1:


    "Habiendo parado en un claro, me senté en un tocón, cogí un palo y empecé a escribir en el suelo con él."


translate spanish day6_sl_4104b753:


    th "Si no hay nada que depende de mí, todo lo que tengo que hacer es sencillamente esperar."


translate spanish day6_sl_8763d3f6:


    th "Pero...{w} De hecho, ¿qué puedo hacer?"


translate spanish day6_sl_6c70ad8a:


    th "¿Asaltar la enfermería?{w} Incluso aunque tuviera éxito, ¿qué haría luego?"


translate spanish day6_sl_630e01c1:


    th "¿Intentar hacer recapacitar a Olga Dmitrievna?{w} Difícilmente posible..."


translate spanish day6_sl_3b57bdd9:


    "Qué gracia, todo este completo misterio se convirtió en una simple situación del día a día.{w} Si estuviera ocurriendo en mi mundo, no habría problemas en absoluto."


translate spanish day6_sl_115eb200:


    "Venga va, de verdad, ¿qué problema hay... incluso si dormí con la muchacha, a quién le importa?"


translate spanish day6_sl_964f46c9:


    "Incluso aunque en realidad tuviera diecisiete, tampoco habría ocurrido nada terrible de ese modo... Habría sido algo ligeramente más incómodo, pero aun así tolerable..."


translate spanish day6_sl_f32a5b64:


    th "¿Y ahora qué?{w} No soy el amo de mi destino..."


translate spanish day6_sl_46c5eccc:


    "Aunque, ¿lo fui acaso?{w} ¿Acaso hay algo que dependiera totalmente de mí en toda mi vida?"


translate spanish day6_sl_a9996173:


    "¿He alcanzado siquiera alguna cima que estuviera tratando de ver?"


translate spanish day6_sl_b7175618:


    th "Y ahora, cuando he alcanzado una, no puedo permanecer en ella..."


translate spanish day6_sl_19ac515c:


    th "¿Tal vez solamente fui mero espectador desde muy al principio?{w} Mi aparición mística en este campamento, todos los lugareños, todos los acontecimientos que me sucedieron..."


translate spanish day6_sl_ee7dab86:


    th "¿Todo esto es sólo un extraño juego en el cual únicamente soy un peón?"


translate spanish day6_sl_4f0726c7:


    th "Bueno, si eso es así, ¡entonces jugaré mi papel lo mejor posible!"


translate spanish day6_sl_8af8d5f1:


    "Me levanté y me dirigí hacia el campamento, preparado para no hacer prisioneros."


translate spanish day6_sl_e773fa2d:


    "El plan no estaba completamente preparado en mi cabeza, ¡pero ya me hice la idea!"


translate spanish day6_sl_06b4910d:


    "No había nadie en la cabaña de la líder del campamento."


translate spanish day6_sl_be72f150:


    "Hurgué entre los armarios y empaqué un saco de dormir, mis ropas de invierno, una linterna y algunas otras prendas que hallé."


translate spanish day6_sl_e5172865:


    "Salí fuera e inmediatamente al correr di con Alisa."


translate spanish day6_sl_5959997f:


    dv "¿Te vas de excursión?"


translate spanish day6_sl_a90c5245:


    me "Claro, de excursión."


translate spanish day6_sl_df3861de_1:


    "La tranquilicé."


translate spanish day6_sl_07a7866b:


    dv "¡Bien, ves con cuidado!"


translate spanish day6_sl_79ac8364:


    "Me hizo una pícara sonrisa y siguió su camino."


translate spanish day6_sl_885cf3e5:


    "Apresuré mis pasos hasta la enfermería."


translate spanish day6_sl_8c0459e9:


    th "La enfermera es una humana a fin de cuentas, así que de un momento a otro tendrá que ir a comer."


translate spanish day6_sl_c3cfebc5:


    "Me escondí entre los arbustos, esperando."


translate spanish day6_sl_a20cefa7_1:


    "..."


translate spanish day6_sl_32d9f799:


    "El tiempo se fue volando.{w} Mis pensamientos se asentaron y tenía una firme convicción."


translate spanish day6_sl_aa94e82e:


    "No he pensado mi plan más allá de los primeros pasos... Sencillamente no me preocupa."


translate spanish day6_sl_5236e874:


    th "La cosa más importante es que actúe de la forma en que creo que es apropiado, y que lo que tenga que ocurrir... suceda."


translate spanish day6_sl_15377a38:


    "Al fin, la enfermera salió fuera de la enfermería."


translate spanish day6_sl_0c4f3316:


    "Miró por alrededor y, sin pecartarse de peligro alguno, cerró la enfermería con la llave y rápidamente se fue hacia la cantina."


translate spanish day6_sl_808818b1:


    "Cuando ella se fue por la esquina, me fui hasta la ventana en pocas zancadas y comencé a golpearla febrilmente."


translate spanish day6_sl_7d8aa336:


    "Slavya se mostró pronto."


translate spanish day6_sl_0e4abe5d:


    sl "Semyon, ¿qué estás haciendo aquí?{w} No te está permitido..."


translate spanish day6_sl_3ce9b29e:


    me "¿Cómo te encuentras?"


translate spanish day6_sl_6d6acda2:


    sl "Estoy bien...{w} Pero Olga Dmitrievna me dijo que me quedara aquí otro día..."


translate spanish day6_sl_d1eb8d48:


    th "¡Ella me dijo algo completamente distinto!"


translate spanish day6_sl_e00f006d:


    me "Vale. Sal fuera."


translate spanish day6_sl_8df31520:


    "Dije con confianza."


translate spanish day6_sl_ba36c8c0:


    sl "¿Qué dices? ¿Para qué?"


translate spanish day6_sl_fb77ae46:


    "Preguntó con miedo."


translate spanish day6_sl_7a4ad964:


    me "¡Sal fuera ahora! ¡Te lo explicaré todo después!"


translate spanish day6_sl_44e59b45:


    "Me miró por un rato, pero luego abrió la ventana sin hacer más preguntas."


translate spanish day6_sl_c0792059:


    "Le ayudé a saltar hasta el suelo."


translate spanish day6_sl_05a7a65b:


    sl "Vale, ¿y ahora qué?"


translate spanish day6_sl_b749d8c0:


    me "¡Vayámonos!"


translate spanish day6_sl_4a86fb6e:


    "Le agarré de la mano y me abrí camino hasta el bosque."


translate spanish day6_sl_2754ebb3:


    "Slavya se detuvo pocos minutos después."


translate spanish day6_sl_aa07a769:


    sl "Semyon, ¿qué estás haciendo?"


translate spanish day6_sl_70ae8bb8:


    me "¿Qué pasa? ¿Crees que todo estaba yendo bien así?"


translate spanish day6_sl_149bd1f8:


    sl "No, pero será peor de esta forma."


translate spanish day6_sl_8c2bd012:


    me "¿Por qué? ¡Tenemos derecho a actuar de la forma que creemos que es correcta!"


translate spanish day6_sl_9ee0cfcb:


    sl "Vale, ¿pero qué sucederá luego?"


translate spanish day6_sl_936b630d:


    "Aun no he pensado en ello."


translate spanish day6_sl_40898a23:


    "Cada paso en mi plan había ido bastante bien hasta ahora, pero no me había aplicado en pensar más allá de eso."


translate spanish day6_sl_6bf1e9fc:


    me "¡Echa un vistazo!"


translate spanish day6_sl_b46c28f2:


    "Abrí la mochila y le mostré las cosas que cogí de la cabaña de la líder del campamento."


translate spanish day6_sl_e26abf96:


    sl "¿Y?"


translate spanish day6_sl_0fa153ce:


    "Slavya me miró inquisitivamente."


translate spanish day6_sl_ccf6bd0d:


    me "No lo sé...{w} Permaneceremos en el bosque por un rato...{w} ¡Les mostraremos que nuestra opinión cuenta!"


translate spanish day6_sl_c23e3fba:


    sl "¡Pero eso es estúpido y una criaturada!"


translate spanish day6_sl_59c4f9d9:


    me "Bueno, si no quieres..."


translate spanish day6_sl_d4d98b56:


    "Ella me hizo una mirada de lástima."


translate spanish day6_sl_eb5caabf:


    sl "Como tú digas..."


translate spanish day6_sl_ecece0fc:


    "Esas palabras me dieron escalofríos."


translate spanish day6_sl_0834429f:


    th "«Como tú digas»..."


translate spanish day6_sl_6428f6b8:


    "Nadie me dijo algo así antes."


translate spanish day6_sl_c747ecee:


    me "En cualquier caso...{w} ¡Creo que ésta es la mejor decisión por ahora!"


translate spanish day6_sl_6f9aa3f1:


    sl "Muy bien..."


translate spanish day6_sl_ecfe56f5:


    "Dejé el saco de dormir en el suelo y nos sentamos en él."


translate spanish day6_sl_032a8535:


    sl "Y bien, ¿qué vamos a hacer?"


translate spanish day6_sl_7479b07e:


    me "No lo sé..."


translate spanish day6_sl_83657023:


    "Nunca he estado tan cerca de un rotundo fracaso."


translate spanish day6_sl_6f0e498c:


    "No, no estaba cuestionando mi elección, pero desde luego... no tenía ni idea de qué hacer."


translate spanish day6_sl_9d1d5c5c:


    me "¿Qué sugerirías?"


translate spanish day6_sl_8cefafd9:


    sl "Sería genial tener algo que comer...{w} ¿Traíste algo contigo?"


translate spanish day6_sl_a5ff15c9:


    th "Y eso era exactamente lo que olvidé..."


translate spanish day6_sl_5cb822ee:


    me "No..."


translate spanish day6_sl_0080cf26:


    "Contesté lentamente."


translate spanish day6_sl_6f9aa3f1_1:


    sl "Está bien..."


translate spanish day6_sl_0b938548:


    "Estuvimos sentados en silencio."


translate spanish day6_sl_e893c99b:


    th "Me pregunto si nos buscarán.{w} Aunque si lo hicieran, era improbable que consiguieran hallarnos... el bosque es bastante grande."


translate spanish day6_sl_e870b5fc:


    th "Pero la comida...{w} Hacia la tarde estaré hambriento también..."


translate spanish day6_sl_55e17cab:


    me "Vale, quédate aquí, volveré enseguida."


translate spanish day6_sl_29684dd8:


    sl "¿Adónde vas?"


translate spanish day6_sl_b8a8c899:


    me "¡A por algo de comida!"


translate spanish day6_sl_a81ddd10:


    sl "¿Quizá deberíamos ir juntos?"


translate spanish day6_sl_a428b9a5:


    me "No... ¡Vuelvo enseguida!"


translate spanish day6_sl_775467e0:


    "Me dirigí con un ritmo rápido hasta el campamento, dejando a Slavya sola en el claro."


translate spanish day6_sl_bbc0779a:


    "Estoy completamente seguro de que ella no se irá a ninguna parte, me esperará.{w} Espero estar tan seguro como para luego recordar el camino de vuelta también..."


translate spanish day6_sl_d8d6f4b9:


    th "Incluso aunque no hayan empezado a buscarnos a ambos, pues seguramente habrán descubierto que Slavya está desaparecida, así que me tengo que mover con cuidado e intentar no ser descubierto."


translate spanish day6_sl_60719f55:


    th "En general, cualquier pionero era una amenaza ahora. No solamente los que me conocían... tal secuestro tan obvio seguramente sería conocido por todos en poco tiempo."


translate spanish day6_sl_90bcafae:


    "Y mientras que llegar a la cantina en unas cuantas carreras no era difícil del todo, no tenía ni idea de qué hacer luego."


translate spanish day6_sl_566963d3:


    th "No puedo caminar simplemente por ahí como si nada hubiera ocurrido..."


translate spanish day6_sl_4a1751e3:


    us "¿Te estás escondiendo ahí?"


translate spanish day6_sl_e9efaf89:


    "Casi me da un ataque de corazón del susto."


translate spanish day6_sl_6377c405:


    "Ulyana estaba detrás de mí."


translate spanish day6_sl_a1efee9e:


    us "¿Estás hambriento?"


translate spanish day6_sl_c4b83f7b:


    "Me miró fijamente."


translate spanish day6_sl_9ba98794:


    us "Vamos, ¿por qué estás tan callado?"


translate spanish day6_sl_d6658e34:


    "Me quedé mudo por un momento, no podía pensar en una respuesta."


translate spanish day6_sl_961f4581:


    us "Pues, ¿para qué viniste?"


translate spanish day6_sl_1a3f56c5:


    us "Oh, ¿en serio...?"


translate spanish day6_sl_e7ec8520:


    me "Claro, de verdad..."


translate spanish day6_sl_b7e4de4c:


    us "Vale, ¡enseguida vuelvo!"


translate spanish day6_sl_30a664f4:


    "Para mi sorpresa, Ulyana no me hizo preguntas incómodas."


translate spanish day6_sl_e2f777eb:


    "En un abrir y cerrar de ojos, la perdí de vista al deslizarse por las puertas de la cantina."


translate spanish day6_sl_a977e8fd:


    "Estaba divagando si debería confiar en ella y esperar, o si debería largarme antes de que volviera con refuerzos..."


translate spanish day6_sl_ab25b248:


    "Sin embargo, Ulyana interrumpió mis dudas rápidamente cuando reapareció en un par de minutos con una bolsa de plástico en sus manos."


translate spanish day6_sl_9a7ad094:


    us "¡Ahí tienes!"


translate spanish day6_sl_b493cbf1:


    "La bolsa estaba llena de bollos y tenía unos cuantos paquetes de kéfir en ella."


translate spanish day6_sl_65a156a9:


    me "¿Pero por qué?"


translate spanish day6_sl_336a06df:


    us "Bueno... porque es valiente... y... ¡guay!"


translate spanish day6_sl_945c59bc:


    "Ella sonrió astutamente."


translate spanish day6_sl_83d1e252:


    us "¡Dale recuerdos a Slavya!"


translate spanish day6_sl_233fc538:


    "Ulyana se despidió con la mano y salió corriendo."


translate spanish day6_sl_f6f60dcb:


    "Me quedé ahí de pie por un rato confundido, pero luego retorné de vuelta al bosque."


translate spanish day6_sl_2ae59a66:


    "Era algo poco común por parte Ulyana pero, siendo sinceros, le estaba agradecido."


translate spanish day6_sl_a3e91759:


    "A fin de cuentas, no podía conseguir la comida por mí mismo durante el día sin meterme en algún problema..."


translate spanish day6_sl_f703ec01:


    "Al retornar de vuelta al claro, me encontré a Slavya sentada en el saco de dormir exactamente en la misma posición."


translate spanish day6_sl_482db2cf:


    me "Ahí va..."


translate spanish day6_sl_8dadd6b7:


    "Le di la bolsa."


translate spanish day6_sl_e7830fb4:


    sl "¡Lo conseguiste muy rápido!"


translate spanish day6_sl_0b70dfeb:


    "Dijo emocionada."


translate spanish day6_sl_254d4d53:


    me "Bueno, encontré ayuda..."


translate spanish day6_sl_af5c4880:


    "Slavya me hizo una mirada inquisitiva, pero no entré en detalles."


translate spanish day6_sl_1ec45799:


    "Comimos afanándonos sin intercambiar palabra."


translate spanish day6_sl_563de47a:


    "Suspiré con satisfacción al final."


translate spanish day6_sl_af6fb71f:


    me "Qué bien, realmente muy bien."


translate spanish day6_sl_8b01f8be:


    sl "Sep."


translate spanish day6_sl_a4965e91:


    "Asintió Slavya."


translate spanish day6_sl_eacac508:


    sl "¿Qué vamos hacer ahora?"


translate spanish day6_sl_d60537c4:


    me "¿Quizá tengas alguna sugerencia?"


translate spanish day6_sl_1210502c:


    sl "¿Sobre qué?"


translate spanish day6_sl_6a46b67c:


    me "Bueno, al menos sobre cómo lidiar con la líder del campamento."


translate spanish day6_sl_29040378:


    sl "No sé...{w} Y estás seguro que..."


translate spanish day6_sl_748c8dfc:


    me "¡Sí, lo estoy!"


translate spanish day6_sl_e5e97d5d:


    "Le interrumpí."


translate spanish day6_sl_ccf4cd89:


    me "¡Esto es un acto de protesta al fin y al cabo!{w} Ella debe ser consciente de que no puede darme órdenes..."


translate spanish day6_sl_2a7a8a3b:


    "Tartamudeé."


translate spanish day6_sl_704441dd:


    me "¡...darnos órdenes!"


translate spanish day6_sl_4d7af59e:


    "Me sentí como si hubiera dicho una cosa estúpida."


translate spanish day6_sl_97d14a00:


    "Incluso si era el muchacho de los recados de Olga Dmitrievna, Slavya era..."


translate spanish day6_sl_f9d7b013:


    sl "¡Eres muy gracioso!"


translate spanish day6_sl_b7bd4e35:


    "Ella me hizo una sonrisa bonita."


translate spanish day6_sl_ee9341f5:


    sl "Podríamos simplemente ir a hablar con ella.{w} Pero si prefieres quedarte sentado aquí y esperar, me parece bien."


translate spanish day6_sl_6c4c506e:


    me "Traté de hablar con ella..."


translate spanish day6_sl_c77d0992:


    sl "¿Estás seguro de que lo hiciste de la manera correcta?"


translate spanish day6_sl_32522388:


    th "¿Cuál es la manera correcta?"


translate spanish day6_sl_002325fc:


    "Para ser honestos, no sabía la respuesta."


translate spanish day6_sl_33279267:


    "Tales conversaciones incómodas eran siempre para mi algo así como una tarea insuperable."


translate spanish day6_sl_28d5f3e2:


    "Estoy seguro de que si supiera cómo lidiar con tales cosas, todo iría de una manera distinta."


translate spanish day6_sl_6650b9b5:


    "Desde luego, en tanto estuviera preocupado y me culpara, no tendría oportunidad de tener más confianza sobre mí mismo..."


translate spanish day6_sl_7479b07e_1:


    me "No lo sé..."


translate spanish day6_sl_c954d69f:


    "Slavya rompió a carcajadas."


translate spanish day6_sl_3d342d5a:


    me "¡¿Qué?!"


translate spanish day6_sl_31892cbe:


    sl "Nada...{w} Sólo que me siento como una princesa, la cual fue rescatada del castillo de la malvada bruja por un Príncipe Azul."


translate spanish day6_sl_8f838576:


    "La miré con detenimiento."


translate spanish day6_sl_e89f228b:


    "Si bien todo esto sonó bastante romántico, pensé que lo que Slavya había dicho era un completo sin sentido."


translate spanish day6_sl_97cde972:


    th "Pero claro, si Olga Dmitrievna fuera la bruja malvada...{w} ¡Entonces sí tendría sentido!"


translate spanish day6_sl_88e6b2de:


    me "Sí... La similitud claramente está ahí."


translate spanish day6_sl_f51cd4bc:


    "Lo dije pronunciando cada una de las palabras."


translate spanish day6_sl_3915d229:


    sl "Y a las princesas no se las da la oportunidad de elegir en dichos casos normalmente."


translate spanish day6_sl_aabed66d:


    "Slavya continuó con un tono críptico."


translate spanish day6_sl_1cabe26b:


    th "¿Que no se les da la oportunidad para elegir?{w} ¡Eso es imposible!"


translate spanish day6_sl_e89a598d:


    th "Soy yo quien no tuvo elección aquí... tan sólo sentarme y esperar hasta que me vaya de vuelta a mi realidad por medio de alguna oportunidad milagrosa."


translate spanish day6_sl_63017633:


    th "¿Tiene algo en común Slavya conmigo?{w} Ella es una pionera ejemplar, trabajadora y bella."


translate spanish day6_sl_c7da1f43:


    "¡Ella tiene cientos, si no miles, de oportunidades!"


translate spanish day6_sl_db88a5ef:


    th "Probablemente éste sea el momento que soñé toda mi vida... tal inaccesible muchacha como Slavya no sólo sentada a mi lado, hablándome y riendo, ¡sino además completamente dependiendo de mí!"


translate spanish day6_sl_a49c6922:


    th "¿Pero es algo bueno o malo?{w} ¿Y qué debería hacer?"


translate spanish day6_sl_18e9a332:


    "Habían más preguntas que respuestas."


translate spanish day6_sl_9a4e7e0b:


    "Cuando es suficientemente difícil tomar una decisión sobre mi propia vida y asumir la responsabilidad de mis propios actos, ¿cómo podría hacer eso también por nosotros...?"


translate spanish day6_sl_afb4e267:


    "Hasta ahora me mantuve al margen de tales problemas con la esperanza de que se resolvieran solos, sin mi intervención."


translate spanish day6_sl_3057669a:


    "Pero no podía actuar de ese modo ahora."


translate spanish day6_sl_d1c57701:


    "Y no sólo porque, de hecho, únicamente el cielo sabía dónde y en qué tiempo me hallaba."


translate spanish day6_sl_2b586af9:


    me "Sabes, podría estar equivocado."


translate spanish day6_sl_0fa153ce_1:


    "Slavya me miró inquisitivamente."


translate spanish day6_sl_3b122786:


    me "No lograremos nada quedándonos tan solamente sentados aquí.{w} ¡Y no sólo eso! Es estúpido y una criaturada... tenías razón."


translate spanish day6_sl_82f6ffb1:


    sl "¿Y qué deberíamos hacer pues?"


translate spanish day6_sl_b749d8c0_1:


    me "¡Irnos!"


translate spanish day6_sl_91946be4:


    "Nos levantamos. Cogí el saco de dormir y me dirigí hacia el campamento, agarrando a Slavya por la mano."


translate spanish day6_sl_98346aa3:


    "Llegamos pronto a la plaza, donde Olga Dmitrievna estaba allí como si nos esperase."


translate spanish day6_sl_57e9c38f:


    mt "¡Cuánto tiempo sin veros!"


translate spanish day6_sl_7b0b0261:


    "Dijo enojada."


translate spanish day6_sl_bee8d492:


    mt "¿Tenéis algo que decir al respecto para excusaros?"


translate spanish day6_sl_a0bfe3f6:


    "Contesté tranquilamente."


translate spanish day6_sl_ccda0fcf:


    "Contesté repulsivamente."


translate spanish day6_sl_7f368bf8:


    mt "Oh, ¿de verdad?{w} Pues vale. ¿Así que crees que todo esto es normal? Bien..."


translate spanish day6_sl_7ab0fbc2:


    "Se pausó por unos momentos y se encaró a Slavya."


translate spanish day6_sl_b7ba9ce3:


    mt "Vete a tu cabaña.{w} Sé que tú no eres culpable."


translate spanish day6_sl_e290e93e:


    "Slavya no se movió ni un centímetro. Ella solamente bajó su mirada y sostuvo mi mano estrechándola más fuerte."


translate spanish day6_sl_1ed1a34c:


    mt "¡Slavya!"


translate spanish day6_sl_da106b03:


    me "Como puedes ver, ella tampoco quiere."


translate spanish day6_sl_c53ecc7f:


    mt "¿Qué le hiciste para seducirla?"


translate spanish day6_sl_e5c8bdf9:


    me "Dios mío, ¡no dije nada!"


translate spanish day6_sl_262d7cae:


    "Empezó a agotárseme la paciencia."


translate spanish day6_sl_57062f56:


    me "¿Por qué piensas que hemos hecho algo malo?{w} ¡No hicimos nada! ¡Nada en absoluto!"


translate spanish day6_sl_9b7d56c6:


    mt "¡Como si no lo hubiera visto!"


translate spanish day6_sl_f0d6d1c8:


    "Ella sonrió."


translate spanish day6_sl_dcebf1f8:


    me "¿Y qué es lo que viste?"


translate spanish day6_sl_a5f17bb1:


    "Ella se detuvo, pero continuó apenas un segundo después con el mismo tono:"


translate spanish day6_sl_17e9592b:


    mt "Suficiente."


translate spanish day6_sl_d3772757:


    me "¡Guau, eres muy buena sacando conclusiones! ¿Qué tal un empleo en el NKVD?"


translate spanish day6_sl_4e67b316:


    mt "¡Basta ya con tu grosería!"


translate spanish day6_sl_5f44af84:


    me "Ni tan siquiera comencé."


translate spanish day6_sl_d808c8a3:


    "Dije sarcásticamente."


translate spanish day6_sl_3e824ff1:


    mt "Vale, se te acabó el tiempo no voy a continuar con esta discusión sin sentido. Estás castigado hasta el final de la temporada."


translate spanish day6_sl_34787a27:


    me "Oh, ¿en serio? ¿Y cómo lo vas hacer?"


translate spanish day6_sl_e8cb9456:


    mt "¡Te voy a encerrar en la cabaña!"


translate spanish day6_sl_9f712cc6:


    me "¿Y si me opongo?"


translate spanish day6_sl_af66aeeb:


    "Olga Dmitrievna me hizo una mirada sorprendida."


translate spanish day6_sl_aafc257a:


    "Fue justo ahora cuando me di cuenta que una multitud se encontraba reunida a nuestro alrededor."


translate spanish day6_sl_a198bf9e:


    "Podía localizar a Lena, Alisa y Ulyana entre la multitud."


translate spanish day6_sl_46cac45a:


    mt "¿Qué quieres decir con «oponerte»?"


translate spanish day6_sl_e7f3c77f:


    "Dijo lentamente la líder del campamento."


translate spanish day6_sl_563e55f9:


    me "Quiero decir exactamente lo que dije."


translate spanish day6_sl_3a35e669:


    mt "¡Pero no puedes! Un verdadero pionero..."


translate spanish day6_sl_2f21154b:


    me "Bueno, ¡eso significa que soy uno falso!"


translate spanish day6_sl_680ddca2:


    "Olga Dmitrievna se detuvo, como si tratara de reunir fuerzas."


translate spanish day6_sl_08cd4a35:


    mt "¿Crees que no puedo obligarte?"


translate spanish day6_sl_832f577a:


    me "¿Y cómo harías eso?"


translate spanish day6_sl_30d5768e:


    "Me reí enérgicamente."


translate spanish day6_sl_b2ab1d1c:


    th "Ignorando las diversas fuerzas místicas de las que al menos era consciente ahora mismo, la líder del campamento estaba claro que no tenía ni la más minima idea de cómo obligar a nada."


translate spanish day6_sl_25d8873c:


    mt "¡¿Sabes qué?!"


translate spanish day6_sl_ad02f53a:


    me "¿Qué?"


translate spanish day6_sl_c051b56f:


    "Era como si Olga Dmitrievna ya hubiera comprendido que ella había perdido, pero aun así no quisiera admitir su derrota a causa de su orgullo y su posición como la mayor y más sabia de todo el campamento."


translate spanish day6_sl_91d81026:


    mt "Yo...{w} yo...{w} ¡Enviaré una queja a tu escuela! ¡No serás aceptado en el Komsomol! ¡No podrás unirte al Partido!"


translate spanish day6_sl_4a0430d0:


    "No tenía manera de contestar contra semejantes amenazas tan graves."


translate spanish day6_sl_32847c55:


    mt "¡Acéptalo, no tienes elección!"


translate spanish day6_sl_4f1d9777:


    "Slavya, quien se mantuvo callada hasta ahora, intervino en el asunto:"


translate spanish day6_sl_0fbed59a:


    sl "Olga Dmitrievna, ¿por qué asumes que tienes razón desde un principio? ¿Por qué siempre culpas a Semyon de todo? ¿Qué tiene tan de especial lo que ha sucedido? ¿Qué ha hecho él... qué hemos hecho mal? ¿Estás completamente segura de lo que nos acusas?"


translate spanish day6_sl_63d41120:


    sl "Y si hubiera sido otra persona en mi lugar, ¿habrías actuado de la misma manera?"


translate spanish day6_sl_d6544d57:


    mt "Slavya, deberías saber que..."


translate spanish day6_sl_133f5bcb:


    sl "Esa es la cuestión, ¡lo sé todo! Te conozco a ti, y sé que Semyon no hizo nada malo. Sé que tú tienes prejuicios contra él. Por cierto, sin una razón clara. ¡Incluso a pesar de que intenta hacer lo mejor posible!"


translate spanish day6_sl_e25671be:


    sl "Sí, resulta incómodo a veces, pero sé que pone todos sus esfuerzos y su alma en ello. ¿Y tú se lo reprochas tan sólo porque él es de la manera que es?"


translate spanish day6_sl_9a7a49a4:


    "Estaba encantado por el monólogo de Slavya, al escucharlo sin entenderlo bien del todo, tanto si me estaba elogiando, afirmando hechos o expresando su opinión (la cual probablemente sean prejuicios)."


translate spanish day6_sl_adaf79ec:


    "Supongo que sus palabras exactas me entraron por una oreja y salieron por la otra."


translate spanish day6_sl_c51c54a3:


    "Solamente la melodía de su voz era suficiente para mí... me hipnotizaba y me portaba lejos de esta enferma mujer y este estúpido campamento de pioneros, mientras calmaba y curaba las heridas de mi alma."


translate spanish day6_sl_90afe97b:


    mt "Vale, ya no sé qué más hacer con vosotros..."


translate spanish day6_sl_a9bda318:


    "Olga Dmitrievna se tranquilizó un poquito."


translate spanish day6_sl_8d10714b:


    mt "¡Pero no os penséis que apruebo vuestro comportamiento!"


translate spanish day6_sl_60c09bca:


    "Se dio la vuelta, se deslizó a través de la multitud y se dirigió a su cabaña."


translate spanish day6_sl_64e8ffbe:


    "Me esperaba una tormenta de aplausos como en las películas americanas, pero la multitud comenzó a disolverse rápidamente en tanto que el espectáculo llegó a su fin."


translate spanish day6_sl_b164096b:


    "Y Ulyana simplemente me guiñaba el ojo astutamente."


translate spanish day6_sl_3e600926:


    sl "Bueno, sabes, hemos capeado el temporal."


translate spanish day6_sl_2ffca3fd:


    "Miré a Slavya, estaba sonriendo."


translate spanish day6_sl_431c06db:


    me "¡No, no del todo!{w} Habría estado discutiendo con ella hasta muy tarde por la noche, y tú la sorteaste con facilidad y directamente...{w} ¡No lo habría dicho con mejores palabras ni aunque hubiese tenido una semana entera para pensar!"


translate spanish day6_sl_a441fb1c:


    sl "Oh, venga ya..."


translate spanish day6_sl_d3368ac8:


    me "¿Qué? ¡Esa la sencilla verdad, sin ápice de mentiras o halagos!"


translate spanish day6_sl_092ec533:


    "Ella me escudriñó con su mirada en mis ojos y rompió a carcajadas."


translate spanish day6_sl_28b09c2f:


    sl "Sólo dije la primera cosa que se me pasó por la cabeza, y así es como resultó ser."


translate spanish day6_sl_2266dea3:


    me "Lo primero que pienses es lo correcto, ya que cuando menos se piensa, salta la liebre... así reza el dicho."


translate spanish day6_sl_e330b23b:


    sl "Supongo que sí."


translate spanish day6_sl_c13a9458:


    "Justo me di cuenta ahora de que todavía estábamos agarrados de la mano el uno al otro."


translate spanish day6_sl_cc04a8ea:


    "Tras sentirme un poco incómodo, intenté soltar mi mano pero Slavya me lo impidió.{w} Esto me dio escalofríos por un segundo."


translate spanish day6_sl_0b0dc7e3:


    sl "¿Adónde vamos?"


translate spanish day6_sl_6ba416ef:


    "No tenía deseos de volver a mi cabaña.{w} Hablando estrictamente, no quería permanecer en el campamento para nada."


translate spanish day6_sl_e59ccd54:


    me "¿Qué dirías si sugiero que retornemos al bosque?"


translate spanish day6_sl_21f85f08:


    "Slavya me miró sorprendida."


translate spanish day6_sl_54f96f7f:


    me "Bueno, ya sabes, solamente a veces..."


translate spanish day6_sl_05b93dff:


    sl "¡Vale, vayamos!"


translate spanish day6_sl_1d885fd1:


    "No me esperaba semejante rápida respuesta, no tenía ni idea de cómo acabar mi oración."


translate spanish day6_sl_d574deca:


    "Estaba a punto de explicarle acerca de una sensación que siento a veces... cuando quieres ir a algún lugar más allá de donde está tú espacio seguro.{w} Por lo menos durante un rato, al menos un par de horas."


translate spanish day6_sl_59c2b914:


    "Solía hacer esto antes bastante a menudo... visitar distritos de fuera o incluso ciudades vecinas."


translate spanish day6_sl_abd50e2b:


    "Nuevos lugares y sitios que calman mi corazón y me ayudan a darme cuenta de que el mundo no está limitado por los muros de mi piso, o por la ruta diaria hacia la universidad o el puesto de trabajo."


translate spanish day6_sl_e1e86b71:


    "Y por consiguiente, hay gente que lo hace mejor o peor que yo."


translate spanish day6_sl_0d93e329:


    "Y así mis típicos problemas no son tan malos como parecen ser."


translate spanish day6_sl_b18f3f5e:


    "De repente sonreí."


translate spanish day6_sl_e07c37b3:


    sl "¿Qué?"


translate spanish day6_sl_41274eff:


    me "Nada, sólo recordé...{w} ¡No importa! ¡Vayámonos!"


translate spanish day6_sl_3529c624:


    "Nos dirigimos hacia el bosque, todavía agarrados de las manos."


translate spanish day6_sl_359460da:


    "De un momento a otro, llegamos al claro, probablemente el mismo en el que nos sentamos hace una hora atrás. O podría ser uno diferente, en tanto que no podría distinguir uno de otro."


translate spanish day6_sl_817547ad:


    sl "¿Aquí?"


translate spanish day6_sl_0fa153ce_2:


    "Slavya me hizo una mirada inquisitiva."


translate spanish day6_sl_e4bedbb9:


    me "No, vayamos más lejos."


translate spanish day6_sl_af1d965e:


    sl "Vale."


translate spanish day6_sl_5261c24b:


    "Tras otra media hora vagando a través del bosque, llegamos a un pequeño lago."


translate spanish day6_sl_66a1624c:


    me "¡Qué lugar tan hermoso!"


translate spanish day6_sl_9bb4b00c:


    sl "Tienes razón..."


translate spanish day6_sl_3605a2b3:


    "Slavya sonrió enigmáticamente."


translate spanish day6_sl_b45d6866:


    me "¡Pues sugiero que nos detengamos aquí!"


translate spanish day6_sl_e54e1256:


    sl "Bonito lugar."


translate spanish day6_sl_4f7b9290:


    "Puse el saco de dormir en el suelo y me senté en él."


translate spanish day6_sl_ddd76d2d:


    "Slavya hizo lo mismo."


translate spanish day6_sl_b7cba703:


    me "¿Y qué vamos a hacer?"


translate spanish day6_sl_48ee8384:


    sl "No lo sé... Fue tu idea adentrarnos en el bosque..."


translate spanish day6_sl_21a837aa:


    me "Sí, mía..."


translate spanish day6_sl_2e80f38f:


    th "Tenía que tomar una decisión una vez más.{w} Cuando no tenía ni la más mínima idea de qué podríamos hacer."


translate spanish day6_sl_d3c3f841:


    th "Podriamos hablar, por ejemplo.{w} ¿Pero sobre qué...?"


translate spanish day6_sl_428107ab:


    sl "¡Oh, vamos, no te pongas nervioso!"


translate spanish day6_sl_c954d69f_1:


    "Slavya se rio."


translate spanish day6_sl_66f97779:


    me "No estoy..."


translate spanish day6_sl_033b4661:


    sl "¡Puedo verlo!"


translate spanish day6_sl_a63fa39d:


    me "Bueno, es sólo porque..."


translate spanish day6_sl_7bf46dbb:


    sl "No pasa nada."


translate spanish day6_sl_0fa7e23d:


    me "Como digas."


translate spanish day6_sl_b9b338a4:


    "Suspiré y me quedé mirando fijamente el suelo."


translate spanish day6_sl_9269f3f7:


    sl "Tal vez podrías hablarme sobre ti."


translate spanish day6_sl_59929532:


    me "Bueno, no hay realmente gran cosa que contar..."


translate spanish day6_sl_ac0560c7:


    sl "¡Es imposible que no pueda haber nada de interesante en la vida de una persona!"


translate spanish day6_sl_9eeceb0e:


    me "¿Cómo que no?{w} Soy un ejemplo de eso..."


translate spanish day6_sl_dec3abfe:


    sl "Tú sólo..."


translate spanish day6_sl_60f3d758:


    "Ella dudó."


translate spanish day6_sl_8e71c1af:


    sl "¿Podría ser que no me estuvieras contando algo?"


translate spanish day6_sl_8b677452:


    me "¿Como qué?"


translate spanish day6_sl_762cefe1:


    sl "No lo sé...{w} ¿Como si me ocultaras algo?"


translate spanish day6_sl_869dccdf:


    me "¿Pero por qué lo haría?"


translate spanish day6_sl_446d678d:


    sl "¿Cómo lo voy a saber? Tú lo sabrás mejor."


translate spanish day6_sl_7dff98c4:


    me "Bueno...{w} No..."


translate spanish day6_sl_3c9fa231:


    sl "Ibas a decir algo..."


translate spanish day6_sl_d2b04ecc:


    me "¿Cómo?"


translate spanish day6_sl_94474f8e:


    sl "¡Incorrecto!"


translate spanish day6_sl_f7fcc72d:


    "Slavya se rio otra vez."


translate spanish day6_sl_ee707346:


    "Estoy seguro de que otra persona en su lugar, no se habría comportado con tanta calma."


translate spanish day6_sl_2d42f5c8:


    me "Vale, ¿y bien, de qué quieres que hable?"


translate spanish day6_sl_50dc5f53:


    sl "Bueno, cuéntamente algo sobre ti. ¡Algo interesante que te haya sucedido en la vida!"


translate spanish day6_sl_e9233663:


    "Ahora mismo estaba completamente seguro de que no había nada de interesante en mi vida."


translate spanish day6_sl_dfb2a4e4:


    me "Vale, déjame pensar..."


translate spanish day6_sl_8a84040d:


    "Puse cara de inteligente."


translate spanish day6_sl_40e09782:


    me "Cuando era un niño... más o menos con cinco o seis años, creo... había un pequeño refugio cerca de la casa del pueblo de mi familia."


translate spanish day6_sl_6642f6b7:


    me "Una bien construída, hecha de tablones de madera, con tejado de tela asfáltica, por lo que podía ser usado como un refugio antiaéreo en caso de guerra."


translate spanish day6_sl_23e95695:


    me "Así que un día estaba sentado arriba con alguien y...{w} repentinamente me caí abajo."


translate spanish day6_sl_4ea8163e:


    me "Y habían detrás unas camas mullidas formadas por los matorrales de fresas."


translate spanish day6_sl_78f3b806:


    me "Y durante aquellos precisos segundos mientras caía pensé... exacto, la Parca había venido a por mí, vi circular toda mi vida frente a mis ojos. Una vida de tan sólo seis años, pero una vida al fin y al cabo..."


translate spanish day6_sl_a269facd:


    me "Así que caí en aquella cama mullida de matorrales y, obviamente, no sentí nada ya que allí había un suelo muy blandito."


translate spanish day6_sl_b0563737:


    me "Y ésa es la historia..."


translate spanish day6_sl_c954d69f_2:


    "Slavya se rio."


translate spanish day6_sl_0ea44879:


    sl "¡Pero eso es una tontería!"


translate spanish day6_sl_1c6992b3:


    me "No pensé eso en aquel momento."


translate spanish day6_sl_320e2edd:


    sl "¿Y ahora qué?"


translate spanish day6_sl_851543b3:


    me "No sé...{w} Supongo que..."


translate spanish day6_sl_3aa2017b:


    sl "Bien.{w} ¡Cuéntame otra cosa!"


translate spanish day6_sl_26ec147e:


    me "También dirás que es una tontería..."


translate spanish day6_sl_e1a615ef:


    sl "¡No, qué va!"


translate spanish day6_sl_1cc1d82f:


    me "Eh..."


translate spanish day6_sl_384065ad:


    "Comencé a pensar de nuevo."


translate spanish day6_sl_d9e5ad4e:


    me "También hubo una vez que caí en un estanque desde un puente y casi me ahogo."


translate spanish day6_sl_745d222d:


    sl "Tus historias son tan..."


translate spanish day6_sl_75767d26:


    "Dijo Slavya en un tono enojado."


translate spanish day6_sl_c0ec9074:


    me "Te lo dije... no hay mucho que contar, así que te expliqué la primera historia que se me ocurrió."


translate spanish day6_sl_3eb56ac0:


    sl "¡Ah, hablando de esto, el lago!"


translate spanish day6_sl_b0c344be:


    "Slavya miró hacia el agua."


translate spanish day6_sl_3049af19:


    sl "Vayamos a nadar, ¿te parece?"


translate spanish day6_sl_fcde635e:


    me "No me gusta mucho eso.{w} No tengo tampoco ningún bañador."


translate spanish day6_sl_81cd01eb:


    sl "¡Oh, venga ya!"


translate spanish day6_sl_6db70b0c:


    "Ella me hizo una sonrisa cautivadora."


translate spanish day6_sl_e8fc1211:


    me "Bueno, no sé de verdad si..."


translate spanish day6_sl_b7400b08:


    "Desde luego, estaba ansioso por aceptar, pero aun así dudaba."


translate spanish day6_sl_72436a13:


    sl "¡Oh, venga vamos!"


translate spanish day6_sl_41c34d45:


    "Slavya se rio y fue corriendo hasta el lago, desabrochándose su camisa por el camino."


translate spanish day6_sl_a62eafb3:


    me "Bueno... Mmm..."


translate spanish day6_sl_5b6dae07:


    "Me levanté del saco de dormir y lentamente me dirigí hasta ella."


translate spanish day6_sl_e09166ac:


    "Cuando estuve cerca, Slavya ya estaba sumergiéndose en el lago, con toda su ropa tirada por el suelo."


translate spanish day6_sl_87701f3c:


    sl "¡Ven aquí!"


translate spanish day6_sl_7e260440:


    me "Bueno yo...{w} No estoy seguro de si quiero..."


translate spanish day6_sl_5eca3ce7:


    "Murmuré lentamente."


translate spanish day6_sl_12241ecd:


    sl "¡Vamos! ¡Que es divertido!"


translate spanish day6_sl_6217020f:


    "Ella no parecía sentirse para nada incómoda por mi presencia."


translate spanish day6_sl_d8852a28:


    "Cuando desde un principio quise darme la vuelta."


translate spanish day6_sl_6bf6e07b:


    th "Y ahí va esta cuestión... ¿qué es lo más apropiado?{w} Si Slavya cree que todo está bien y me comporto torpe e incómodamente... ¿acaso no le molestará?"


translate spanish day6_sl_a7e3e304:


    th "¡Pero ahora eso no es la cosa más importante!"


translate spanish day6_sl_4fe83cce:


    me "No me traje mi bañador..."


translate spanish day6_sl_96942526:


    "Supongo que no era la mejor excusa en este instante."


translate spanish day6_sl_cbfa9c42:


    sl "¿Y? Yo tampoco..."


translate spanish day6_sl_475bb393:


    "Slavya lo dijo con la voz un poco baja."


translate spanish day6_sl_8160a6c3:


    sl "No te preocupes."


translate spanish day6_sl_8cac9455:


    me "¡No lo estoy! No hay nada de qué preocuparse..."


translate spanish day6_sl_6dfc8e2e:


    sl "Pues entra."


translate spanish day6_sl_3f27c2c0:


    "No sabía qué decir."


translate spanish day6_sl_1a6a0ccb:


    "Probablemente lo acataría... pero con todo ocurriendo de un modo distinto.{w} No de una manera tan espontánea y casual."


translate spanish day6_sl_3ef72e17:


    th "Y en serio... ¿por qué Slavya es tan {i}simpática{/i} conmigo?{w} ¿O es que hay algo más que amistad?{w} ¡De verdad que no lo entiendo!"


translate spanish day6_sl_d88264e4:


    sl "¡Como quieras!"


translate spanish day6_sl_1e8379d1:


    "Dijo decepcionada."


translate spanish day6_sl_7cd8cd17:


    sl "Pues prende fuego."


translate spanish day6_sl_7332342b:


    me "¿Para qué?"


translate spanish day6_sl_936f6483:


    sl "¡Porqué!{w} Cuando salga... ¡necesito secarme!"


translate spanish day6_sl_6829db90:


    me "Oh, sí... bueno..."


translate spanish day6_sl_d608686c:


    "Rápidamente me fui al saco de dormir, agarrando brancas por el camino."


translate spanish day6_sl_fb820884:


    "Recoger leña no era ningún problema, ¿pero cómo prendería el fuego...?"


translate spanish day6_sl_c0609f2f:


    "No tenía ni una cerilla, ni un encendedor."


translate spanish day6_sl_eee42951:


    "Como cualquier persona civilizada, sé que es posible hacerlo con la fricción de un palo con otro, pero no lo he hecho nunca antes.{w} Bueno, no tenía ninguna otra idea..."


translate spanish day6_sl_e5ff5614:


    "Empecé a perforar lentamente un palo con otro."


translate spanish day6_sl_2723a1cb:


    "Tenía suerte de que los palos estuvieran suficientemente secos, ya que de otra forma no tendría posibilidad alguna."


translate spanish day6_sl_27361633:


    "En realidad, era bastante escéptico sobre mis posibilidades, pero tras sólo un minuto un poco de humo apareció y una llama de fuego apenas visible prendió, muriendo un momento después."


translate spanish day6_sl_f5d403da:


    "Por lo menos estaba claro que podría arreglármelas para prender fuego de este modo."


translate spanish day6_sl_75c51157:


    "Me llevó otros veinte minutos de pura tortura, pero al final conseguí una pequeño fuego prendiendo cerca del saco de dormir."


translate spanish day6_sl_b893bafe:


    "Estaba lanzando perezosamente más palos en él."


translate spanish day6_sl_b50189b8:


    "Así es como son las cosas... un hombre puede adaptarse a cualquier cosa, así que podría incluso sobrevivir en un bosque nevado."


translate spanish day6_sl_2545e174:


    "Mis pensamientos de supervivencia fueron interrumpidos por Slavya, quien apareció cerniéndose sobre mí."


translate spanish day6_sl_14c08f52:


    sl "¡Guau! Eres bueno."


translate spanish day6_sl_ad95378a:


    "Alcé mi mirada y rápidamente la aparté."


translate spanish day6_sl_190c6df5:


    "Ella vestía una camisa, pero gracias a su humedad no estaba ocultando mucho."


translate spanish day6_sl_79ca2420:


    "No era un problema sumergida en el lago, pero es bastante diferente cuando ella está tan cerca."


translate spanish day6_sl_9fe63324:


    sl "Una no se perdería contigo."


translate spanish day6_sl_e7783d47:


    "Slavya se sentó apoyando su espalda contra la mía."


translate spanish day6_sl_12a92a61:


    sl "Ojalá me seque cuanto antes."


translate spanish day6_sl_4baac495:


    "De algún modo, yo también lo deseaba."


translate spanish day6_sl_4166759d:


    sl "Y bien, ¿qué?"


translate spanish day6_sl_ad02f53a_1:


    me "¿Qué?"


translate spanish day6_sl_ce38d8f8:


    "Pregunté con voz desconcertada."


translate spanish day6_sl_d43342bf:


    sl "¡Cuéntame otra cosa!"


translate spanish day6_sl_77005469:


    me "Realmente no sé..."


translate spanish day6_sl_f574621f:


    "Sentí como si algo se hubiera atascado en mi garganta... definitivamente, narrar estaba fuera de duda."


translate spanish day6_sl_a1ced71a:


    sl "Te has quedado de piedra de repente."


translate spanish day6_sl_d82bbbfe:


    th "En serio, ¿por qué me quedaría de piedra?"


translate spanish day6_sl_09834bc7:


    me "No es nada, solamente..."


translate spanish day6_sl_64fb8da4:


    "Tuve la sensación de que no sería capaz de quedarme sentado así por mucho tiempo más."


translate spanish day6_sl_10e70ee5:


    "Los instintos animales estaban nublando mi mente."


translate spanish day6_sl_e909c247:


    me "¿Crees... que esto es normal?"


translate spanish day6_sl_e07c37b3_1:


    sl "¿El qué?"


translate spanish day6_sl_877d42ed:


    me "Estar sentados así."


translate spanish day6_sl_0da077c9:


    sl "¿Qué tiene de malo?"


translate spanish day6_sl_b060c097:


    "Parece que Slavya no lo comprendió de verdad..."


translate spanish day6_sl_7d63ae9b:


    me "Bueno, eres...{w} ¿Realmente no lo entiendes...?"


translate spanish day6_sl_1fd5a2a5:


    "Suspiré impotente."


translate spanish day6_sl_86c2396f:


    sl "Bueno... La verdad es que no lo tengo claro..."


translate spanish day6_sl_2295ea06:


    "Su voz sonó pícara."


translate spanish day6_sl_ba65d0ea:


    sl "¿Tú qué crees?"


translate spanish day6_sl_a15c0346:


    me "Bueno, yo...{w} Yo diría..."


translate spanish day6_sl_75404f9d:


    sl "¿Quizá no deberíamos contenernos?"


translate spanish day6_sl_b31684a0:


    "Sus palabras relampaguearon en mi cabeza."


translate spanish day6_sl_69908825:


    th "¡Ella me está dando su consentimiento!"


translate spanish day6_sl_618577b1:


    "Algo maravilloso sucedió entre nosotros, algo que no podría haber soñado jamás hace seis días atrás."


translate spanish day6_sl_f6734dd5:


    "¿Tal vez sea esta... la razón por la cual estoy en este campamento?"


translate spanish day6_sl_64e5abef:


    sl "¡Fue increíble!"


translate spanish day6_sl_9513cd87:


    me "Sep..."


translate spanish day6_sl_a2d3eaed:


    "Por un rato estuvimos tan sólo tumbados el uno junto al otro."


translate spanish day6_sl_add6d7aa:


    sl "Comienza a refrescar."


translate spanish day6_sl_62823e20:


    me "Espera."


translate spanish day6_sl_3a7548c7:


    "Abrí el saco de dormir y nos metimos dentro juntos."


translate spanish day6_sl_d5fe4f59:


    "Slavya descansó su cabeza sobre mi hombro."


translate spanish day6_sl_0cc68dfe:


    sl "¿Te importa si duermo un poquito? Me encuentro muy agotada..."


translate spanish day6_sl_3b03af3a:


    me "Claro, adelante..."


translate spanish day6_sl_755c0af5:


    "Poco después su respiración se volvió profunda y firme."


translate spanish day6_sl_3634f3f3:


    "El viento hizo susurrar suavemente las hojas trémulas de los árboles de nuestro alrededor, la hierba se mecía a nuestro lado, y diminutos insectos zumbaban fastidiosamente por encima nuestro."


translate spanish day6_sl_0ece9306:


    "Repentinamente me percaté de que éste era el mejor lugar que jamás había visto en mi vida."


translate spanish day6_sl_b249ce58:


    "Y pensando en eso, me dormí."


translate spanish day6_sl_ce617998:


    "..."


translate spanish day6_sl_a20cefa7_2:


    "..."


translate spanish day6_sl_dcbe279a:


    "Abrí mis ojos y vi la copa de un roble agitándose por encima."


translate spanish day6_sl_4ec20237:


    "Un fuerte viento se alzó, dándome escalofríos hasta los huesos."


translate spanish day6_sl_35f2530a:


    "La luna llena está frente a mí... la noche ha caído."


translate spanish day6_sl_b7d4386e:


    "No sabría decir realmente qué hora era, pero no importaba demasiado."


translate spanish day6_sl_41242fed:


    "Slavya todavía estaba durmiendo."


translate spanish day6_sl_3df19969:


    "Doblé el saco de dormir para acurrucarla, me vestí y me dirigí hacia el lago."


translate spanish day6_sl_5666a2a3:


    "Estaba realmente sediento y no había otras opciones disponibles."


translate spanish day6_sl_7a95c9e6:


    th "Además, no creo que el agua de aquí esté envenenada."


translate spanish day6_sl_1829982a:


    th "No estamos por Chernobyl o algo así."


translate spanish day6_sl_fab822c4:


    th "Por lo menos eso quería creer."


translate spanish day6_sl_45db8adc:


    "Recogí algo de agua y con avidez mojé mi cara entre mis manos."


translate spanish day6_sl_c60407cd:


    "El agua tenía muy buen sabor, parecía que viniera de un manantial, no de un lago."


translate spanish day6_sl_1f3eb0be:


    "Mi reflejo se me quedó mirando, borroso por el agua y bañado por la luz lunar."


translate spanish day6_sl_023f70e4:


    "Parecía estar sonriendo."


translate spanish day6_sl_342dc487:


    th "Qué raro, ni me di cuenta..."


translate spanish day6_sl_0a38142d:


    "Contento, me dirigí de vuelta con todas las intenciones de dormir hasta que amaneciera..."


translate spanish day6_sl_855eb22e:


    "...y una ramita chascó cerca."


translate spanish day6_sl_f17137d4:


    "Me giré para ver qué hizo aquel ruido, y vi a alguien detrás de un árbol."


translate spanish day6_sl_1d2d3e1b:


    me "¿Quién hay ahí?"


translate spanish day6_sl_323cb2fa:


    "Grité, preocupado."


translate spanish day6_sl_3a866645:


    FIXME_voice "No hay necesidad de chillar así...."


translate spanish day6_sl_c555bb93:


    "La voz me resultaba familiar."


translate spanish day6_sl_7ccaacb2:


    "Me acerqué y vi a Lena."


translate spanish day6_sl_2821e6eb:


    me "¿Qué estás haciendo aquí?"


translate spanish day6_sl_8543722f:


    un "Dando un paseo."


translate spanish day6_sl_2864c058:


    me "¡¿A estas horas?!"


translate spanish day6_sl_920912b6:


    un "¿Qué, no puedo?"


translate spanish day6_sl_b9c06d63:


    me "No, es sólo raro."


translate spanish day6_sl_28dbd9ca:


    "Me apoyé contra un árbol y reflexioné."


translate spanish day6_sl_2d954428:


    th "Está claro que ella está aquí por algún motivo."


translate spanish day6_sl_a5f26668:


    th "Además, nos costó llegar hasta aquí. ¡No puedes llegar hasta este lago con un «paseo»!"


translate spanish day6_sl_9c114242:


    th "E incluso por la noche, se podía ver claramente a Slavya durmiendo lejos a una docena de pasos..."


translate spanish day6_sl_f95c9fc9:


    me "¿Y?"


translate spanish day6_sl_3875ba7b:


    un "¿Qué?"


translate spanish day6_sl_35f65f56:


    me "¿Ibas a decir algo?"


translate spanish day6_sl_38f1c7fb:


    un "¿Qué se supone que iba a decir?"


translate spanish day6_sl_87e50db3:


    me "Eso lo sabes tú."


translate spanish day6_sl_4b0db65e:


    un "No entiendo qué estás diciendo."


translate spanish day6_sl_ffb6e8f5:


    me "Vale...{w} ¿Llevas mucho rato aquí?"


translate spanish day6_sl_4ae58fde:


    un "Bueno, no mucho..."


translate spanish day6_sl_8b16db24:


    me "¿Cuánto es mucho?"


translate spanish day6_sl_dc72cd9c:


    un "¡No mucho!"


translate spanish day6_sl_2406c221:


    "Espetó Lena."


translate spanish day6_sl_93ecd6c4:


    me "Muy bien...{w} ¿Eso es todo?"


translate spanish day6_sl_0da946d4:


    un "No lo sé."


translate spanish day6_sl_e7e5f09c:


    "Permanecimos callados durante un rato."


translate spanish day6_sl_1350bc39:


    un "Probablemente deberíais volver. Slavya se resfriará."


translate spanish day6_sl_65796b62:


    me "¿Entonces lo viste?"


translate spanish day6_sl_2fe9320f:


    un "Era difícil no verlo."


translate spanish day6_sl_bd831b71:


    me "Si eso crees..."


translate spanish day6_sl_17b466c5:


    un "Bueno, y qué se supone que debo pensar, ¿eh?{w} Está claro como el agua."


translate spanish day6_sl_8c370f90:


    me "Sí, supongo...{w} ¿Entonces no vas a decir nada?"


translate spanish day6_sl_1ab636d8:


    un "¿Qué hay que decir?{w} Tras tu espectáculo de hoy en la plaza, las palabras no parecen ser necesarias."


translate spanish day6_sl_d7ec763f:


    me "No hubo ningún espectáculo...{w} Nosotros sólo..."


translate spanish day6_sl_e767eb14:


    th "Es como si me estuviera excusando frente a ella."


translate spanish day6_sl_5e5d8d97:


    me "¡Eso no es asunto tuyo de todos modos!"


translate spanish day6_sl_80519381:


    un "Exacto.{w} Es asunto tuyo.{w} Solo ve con cuidado. Vigila."


translate spanish day6_sl_1084810b:


    me "¿De qué estás hablando?"


translate spanish day6_sl_f1725204:


    un "Si todo lo obtienes con facilidad, corres el riesgo de no saberlo valorar.{w} Aunque deberías."


translate spanish day6_sl_787e2d21:


    me "No lo entiendo."


translate spanish day6_sl_30f3651c:


    un "Bueno, pues como Slavya. ¡Bingo, esta vez es mágico!{w} Y luego lo haces una segunda y una tercera vez, y luego te cansas de ello.{w} Quizás."


translate spanish day6_sl_f14d3856:


    "Lena se detuvo."


translate spanish day6_sl_9e6298f4:


    un "Eso es lo que trato de decir... tienes que amar lo que tienes, incluso aunque no te hayas esforzado mucho en conseguirlo."


translate spanish day6_sl_6d7c4722:


    me "Si crees que me tomo esto como un juego, entonces estás completamente equivocada."


translate spanish day6_sl_44fccd69:


    un "No creo eso.{w} Pero es posible."


translate spanish day6_sl_2b60d758:


    me "¡Además!{w} ¿Qué te parece si te mantienes al margen de lo que no es asunto tuyo?"


translate spanish day6_sl_356d8152:


    un "Solamente te estoy aconsejando."


translate spanish day6_sl_94c6774e:


    me "No recuerdo haberlo pedido."


translate spanish day6_sl_7967d505:


    un "Creo que deberías prestar más atención a lo que haces o planeas hacer, así no tendrás que torturarte por los remordimientos más adelante."


translate spanish day6_sl_54eac6ad:


    me "Mira, ¡esto comienza a parecer una amenaza!"


translate spanish day6_sl_12de8fbf:


    th "Ehmm, realmente lo es..."


translate spanish day6_sl_5361e5b6:


    un "Bueno, pues quizá lo sea..."


translate spanish day6_sl_71411d8c:


    "Dijo Lena misteriosamente."


translate spanish day6_sl_766131be:


    me "Entonces tú me..."


translate spanish day6_sl_cad298e3:


    un "No, no del todo..."


translate spanish day6_sl_d8b4d5a6:


    un "Ahora, para ser exactos."


translate spanish day6_sl_7dd3d65d:


    "Añadió ella en un susurro."


translate spanish day6_sl_51cf710a:


    "Lena se dio la vuelta y se adentró en las profundidades del bosque."


translate spanish day6_sl_5f2d5d23:


    "No la detuve."


translate spanish day6_sl_a4493a46:


    "Cuando ya casi le había perdido de vista, me percaté de algo en su mano brillando con la luz de la luna."


translate spanish day6_sl_77992443:


    th "Qué muchacha más rara..."


translate spanish day6_sl_4850ebe2:


    "Suspiré y retorné con Slavya."


translate spanish day6_sl_bd5a6473:


    "El calor de su cuerpo me resguardó del frío de la noche."


translate spanish day6_sl_8fbd4ba7:


    "Así mismo, ojalá mantenga en calor a Slavya..."


translate spanish day6_dv_c15cbee2:


    "Me desperté temprano, cuando Olga Dmitrievna estaba todavía dormida. Me estiré y me senté en la cama."


translate spanish day6_dv_4f2d9d90:


    "Me sentía sorprendentemente bien, aunque debería haber dormido todo el día tras lo que ocurrió la noche anterior."


translate spanish day6_dv_2702e092:


    "En silencio, con tal de no despertar a la líder del campamento, cogí el estuche de aseo y me salí afuera."


translate spanish day6_dv_0cbfa63c:


    "Me olvidé de mirar el reloj, pero parecían las seis o las siete de la mañana."


translate spanish day6_dv_1e1204ff:


    "El campamento estaba dormido."


translate spanish day6_dv_1857932a:


    "La hierba se sacudía el rocío de la noche, una leve niebla flotaba por encima del suelo, y los primeros rayos de sol matutinos surgían por entre las copas de los árboles, no muy cálidos aun, como si fuera la bombilla del fluorescente de la casa de enfrente."


translate spanish day6_dv_62f242c5:


    "Inspiré una bocanada profunda del gustoso aire fresco y pensé que, incluso una lechuza nocturna como yo, puede disfrutar despertarse temprano a veces."


translate spanish day6_dv_1fc263e9:


    "Las «calles» del campamento estaban vacías por supuesto."


translate spanish day6_dv_6b08ba9d:


    "El agua de aquí siempre ha sido fría, pero hoy parece estar sacada de las profundidades de la Antártida.{w} Incluso mis manos se entumecieron..."


translate spanish day6_dv_07d87721:


    "Habiendo terminado con mi aseo, decidí dar un pequeño paseo por el campamento."


translate spanish day6_dv_c8815fd4:


    th "Olga Dmitrievna está dormida, aun no servirán el desayuno, así que no hay nada que hacer..."


translate spanish day6_dv_e44d834c:


    "Llegué a la plaza y miré a Genda."


translate spanish day6_dv_37cdd1ec:


    th "A ese tipo no le importa si es de día o de noche. ¡Siempre está alerta!"


translate spanish day6_dv_4b83221f:


    "Sonreí de mi ocurrente broma y me dirigí hacia el embarcadero."


translate spanish day6_dv_714f4f97:


    "Mientras caminaba tarareaba una canción, completamente seguro de que era el único despierto por aquí."


translate spanish day6_dv_24dad558:


    "Para gran sorpresa mía, vi a alguien sentado en el embarcadero, dejando sus pies colgando por encima del agua."


translate spanish day6_dv_b284c8b7:


    "Alisa."


translate spanish day6_dv_b3358970:


    "Me acerqué y me detuve, sin saber muy bien qué decir."


translate spanish day6_dv_31a54a49:


    "Entonces ella se giró de repente y se me quedó mirando fijamente."


translate spanish day6_dv_39426f9a:


    dv "¿Llevas mucho rato aquí?"


translate spanish day6_dv_5cb822ee:


    me "No..."


translate spanish day6_dv_6373b237:


    dv "¿Por qué no estás durmiendo?"


translate spanish day6_dv_b78c6e26:


    me "¿Y tú?"


translate spanish day6_dv_bc3de2ca:


    "Se dio la vuelta y continuó mirando el río de nuevo."


translate spanish day6_dv_48bb9e3b:


    th "Parece que no soy el único que está de buen humor hoy."


translate spanish day6_dv_ed9a68b7:


    "Por lo menos ella no estaba siendo insolente otra vez."


translate spanish day6_dv_186c0eae:


    dv "No puedo dormir."


translate spanish day6_dv_f2e007f0:


    me "Ya veo..."


translate spanish day6_dv_c203a783:


    dv "Sobre ayer...{w} Lo entendiste todo, ¿verdad?"


translate spanish day6_dv_ae56873f:


    me "¿Qué exactamente?"


translate spanish day6_dv_5a2cc2c6:


    dv "¡Oh, por el amor de Dios!"


translate spanish day6_dv_0239f53c:


    th "Mi respuesta no pareció ir muy bien."


translate spanish day6_dv_f6787280:


    me "Qué va, lo entendí..."


translate spanish day6_dv_bc2b8f99:


    "Le mentí."


translate spanish day6_dv_db8773f6:


    dv "Bien... bien..."


translate spanish day6_dv_c8722ed0:


    "De repente, ella me miró directamente a los ojos y sonrió."


translate spanish day6_dv_114bfae2:


    dv "¡En dicho caso, tengo planes para ti esta noche!"


translate spanish day6_dv_0b273a12:


    me "¿Eh?{w} ¿Puedo preguntarte cuáles son?"


translate spanish day6_dv_d0e6c06e:


    dv "¡No necesitas saberlos!"


translate spanish day6_dv_0ba7958b:


    me "Bueno, como voy a ser parte de ellos..."


translate spanish day6_dv_0ac4aace:


    dv "Lo sabrás cuando sea el momento."


translate spanish day6_dv_67da3356:


    me "¿Puedes darme una pista al menos?"


translate spanish day6_dv_9f6452c6:


    dv "¡Nop!"


translate spanish day6_dv_58acf86d:


    "Se puso en pie y caminó hacia mí."


translate spanish day6_dv_7c3046d7:


    "Un poco asustado, me hice a un lado."


translate spanish day6_dv_225e8e60:


    dv "¡No hagas nada estúpido!"


translate spanish day6_dv_3af4c20c:


    "Dijo Alisa, yéndose del embarcadero."


translate spanish day6_dv_f1425e57:


    th "Me pregunto qué querrá decir.{w} ¿Y cuáles son esos planes...?"


translate spanish day6_dv_544d6ac4:


    "No tenía ningún deseo de formar parte de algún acto terrorista contra un monumento u otra cosa..."


translate spanish day6_dv_9ede4c1b:


    "Además, todavía estaba hechizado por la conversa de ayer con Lena en la isla."


translate spanish day6_dv_8241f1ad:


    th "Probablemente ella esté todavía enfadada...{w} Y no logré explicarle todo."


translate spanish day6_dv_4184783d:


    th "Aunque tampoco sé qué habría dicho si hubiera tenido la oportunidad."


translate spanish day6_dv_0566e7d0:


    "Me sentía culpable de todo lo que había sucedido, pero no podía descubrir exactamente de qué era culpable."


translate spanish day6_dv_7ea39484:


    "El campamento comenzaba a despertarse..."


translate spanish day6_dv_9af1e38e:


    "Todavía faltaba una hora para el desayuno, pero no tenía ningún deseo de volver a la cabaña de la líder del campamento, así que opté por esperar en la cantina."


translate spanish day6_dv_c58fe4ea:


    th "¡Por lo menos sería el primero por primera vez!"


translate spanish day6_dv_25d2dc92:


    "Me senté en los escalones y observé el amanecer."


translate spanish day6_dv_75d19de5:


    "En ocasiones es genial tan sólo contemplar la llegada de un nuevo día."


translate spanish day6_dv_cd46fe4c:


    "Estaba abrumado por una completa paz y tranquilidad, no había ideas que me estuvieran dando dolores de cabeza como de costumbre."


translate spanish day6_dv_743da966:


    "Sencillamente me bañé en los cálidos rayos de la estrella celeste."


translate spanish day6_dv_cfe6a6d0:


    "En cualquier otro lugar y circunstancias, podría decir que incluso era feliz."


translate spanish day6_dv_5492e5b8:


    un "¡Buenos días! Qué madrugador hoy..."


translate spanish day6_dv_3edc6eb9:


    "Alcé mi vista y vi a Lena de pie frente a mí.{w} Ella estaba sonriendo."


translate spanish day6_dv_0c9902bf:


    me "Tú también..."


translate spanish day6_dv_f6ff66c7:


    "Todos los acontecimientos de la última noche vinieron a mi memoria."


translate spanish day6_dv_17717ee8:


    me "Escucha..."


translate spanish day6_dv_5aa4cd1c:


    un "Si es sobre lo de ayer, ¡abstente por favor!"


translate spanish day6_dv_cd5b1943:


    "Me interrumpió bruscamente."


translate spanish day6_dv_54f0d7cf:


    un "No sé qué es lo que me pasó...{w} Fue todo...{w} ¡Tan sólo olvídalo!"


translate spanish day6_dv_8fb93608:


    "Se me quedó mirando fijamente."


translate spanish day6_dv_b9aa504e:


    "Lena parecía bastante más segura de sí misma que de costumbre."


translate spanish day6_dv_0b610250:


    me "Si sólo fuera todo más sencillo..."


translate spanish day6_dv_96b90470:


    un "¡No hay nada de complicado en ello!"


translate spanish day6_dv_1a20f786:


    "Ella se sentó a mi lado."


translate spanish day6_dv_6fd2c686:


    "Tan cerca que me sentí un poco incómodo, pero no hice el intento de apartarme de ella."


translate spanish day6_dv_b6d571c6:


    un "En ocasiones tengo riñas con Alisa."


translate spanish day6_dv_77ff46df:


    th "Claro, ya me di cuenta."


translate spanish day6_dv_025de025:


    me "Ya veo...{w} Pero en esta ocasión, sucedió a causa de mi."


translate spanish day6_dv_a9702184:


    un "Quién sabe..."


translate spanish day6_dv_069a2ec8:


    "Contestó, ensimismada."


translate spanish day6_dv_a4255212:


    me "Así que es porque...{w} Sabes...{w} Estoy abochornado."


translate spanish day6_dv_99d1db6f:


    me "Quizás en realidad me culpo...{w} O mejor dicho, no trato de decir que no tenga la culpa, pero..."


translate spanish day6_dv_5d2a7a44:


    "Estaba tratando de hacerme explicar como lo haría un estudiante de último grado escolar de primaria respecto a un profesor."


translate spanish day6_dv_b0c06763:


    un "¡Vale, no lo hagas!{w} Como dije, ¡nada grave sucedió!"


translate spanish day6_dv_bd04f9b5:


    me "Si eso crees..."


translate spanish day6_dv_bd742d8d:


    un "Solamente deberías pensar en cómo actuar a partir de ahora..."


translate spanish day6_dv_dc477843:


    me "¡Otra vez no!"


translate spanish day6_dv_9b902801:


    "Me cubrí el rostro con mis manos, mostrando claramente que no tenía ni la más mínima idea de qué iba a pasar aquí y que todo, por decirlo suavemente, me resultaba muy desagradable."


translate spanish day6_dv_bc39bcd1:


    me "¿Podrías expresarte con más claridad?"


translate spanish day6_dv_41cfffd5:


    un "¿Para qué?"


translate spanish day6_dv_67a6d56e:


    "Dijo evasivamente."


translate spanish day6_dv_ebf71d2d:


    me "Mira, ¡si no sé qué hice mal, no podré hacerlo mejor!{w} No puedo elegir cómo «actuar», tal como dijiste."


translate spanish day6_dv_f2f9031f:


    un "Creo que lo comprenderás..."


translate spanish day6_dv_86da72f4:


    me "Me gustaría creer eso también, pero..."


translate spanish day6_dv_27475010:


    "Repentinamente, la puerta de la cantina se abrió, y Olga Dmitrievna salió afuera."


translate spanish day6_dv_03906774:


    mt "¿Esperando sentados tan pronto para comer?"


translate spanish day6_dv_759d5254:


    "Dijo en broma."


translate spanish day6_dv_5567bace:


    un "¡Buenos días!"


translate spanish day6_dv_4fb197ff:


    "Dijo Lena alegremente. Se puso en pie y se dirigió hacia dentro."


translate spanish day6_dv_35698e3a:


    un "¿Vamos?"


translate spanish day6_dv_1c08f469:


    me "Claro, claro..."


translate spanish day6_dv_d2b198a7:


    "No habíamos terminado de hablar, pero los pioneros aparecieron de la nada e inundaron el lugar. Mientras cogía mi desayuno, Lena ya estaba sentada, rodeada por Miku, Zhenya y Slavya. Lamentablemente, no habían más sillas libres en su mesa."


translate spanish day6_dv_b59dc367:


    "Sintiéndome desanimado e intencionadamente sin saludar a nadie, me dirigí hacia mi lugar favorito en el lejano rincón de la cantina."


translate spanish day6_dv_64b85b7a:


    "Me comí las gachas avena (realmente hoy estaban duras) y las hice bajar con un poco de té frío. Luego comencé a observar a los demás."


translate spanish day6_dv_9224c445:


    "Todos los pioneros parecían estar aquí."


translate spanish day6_dv_4b50e0bf:


    "Algunos estaba charlando, otros, como Electronik y Shurik, estaban inmersos comiendo, y otros estaban embobados por ahí, como yo."


translate spanish day6_dv_3ddb5562:


    "Estaba a punto de irme cuando Ulyana se abalanzó a mi mesa, con su bandeja colmada de comida."


translate spanish day6_dv_0a9f12c1:


    us "¡Bon appétit!"


translate spanish day6_dv_eb56d1ba:


    "Dijo sin pensar."


translate spanish day6_dv_ceffc1ce:


    me "¿Robando otra vez?"


translate spanish day6_dv_25072178:


    "Pregunté, mirando fijamente con los ojos abiertos su bandeja."


translate spanish day6_dv_93b62c6f:


    us "¡Un cuerpo que crece necesita muchas calorías!"


translate spanish day6_dv_3c9d580c:


    me "Claro, seguro que deberías crecer un poco..."


translate spanish day6_dv_9db6b90d:


    "Susurré."


translate spanish day6_dv_b7ef3111:


    us "¿Qué?"


translate spanish day6_dv_19ce3442:


    "No creo que me escuchase."


translate spanish day6_dv_84662d7a:


    me "Nada..."


translate spanish day6_dv_75e20332:


    "Me dirigí hacia la salida."


translate spanish day6_dv_a2b6719e:


    us "¿Te vas tan pronto?"


translate spanish day6_dv_afe56f0b:


    "Me chilló por detrás, desalentada.{w} No le contesté."


translate spanish day6_dv_2f41a894:


    "Debería devolver el estuche de aseo. No quiero cargar con él por ahí todo el día."


translate spanish day6_dv_8dc289f1:


    th "Además, ¿hoy qué voy hacer?"


translate spanish day6_dv_ae9fb3ad:


    "Nada me vino a la mente."


translate spanish day6_dv_409f5623:


    "Estaba de tal humor, que parecía que alguien iba a aparecer e invitarme a un viaje."


translate spanish day6_dv_f321b160:


    "Abandoné la cabaña de la líder del campamento y me detuve por un momento, entonces escuché una voz que me llamaba."


translate spanish day6_dv_2a454c26:


    sl "¡Semyon!"


translate spanish day6_dv_c50dbd79:


    "Era Slavya."


translate spanish day6_dv_8543a313:


    sl "¿Ocupado?"


translate spanish day6_dv_5cb822ee_1:


    me "No..."


translate spanish day6_dv_0b8f24fb:


    sl "¿Puedes ayudarme?"


translate spanish day6_dv_3df40f08:


    th "Por supuesto..."


translate spanish day6_dv_3e0102a8:


    "No me apetecía buscar respuestas en un día tan maravilloso como éste."


translate spanish day6_dv_32de2538:


    th "Un par de docenas de horas no iría a cambiar mi situación a fin de cuentas."


translate spanish day6_dv_3eeb5def:


    me "¿De qué se trata?"


translate spanish day6_dv_ac32d9f9:


    sl "Deberías ir al club de cibernética...{w} No sé para qué exactamente, ellos te lo explicarán todo."


translate spanish day6_dv_6954719a:


    th "Los desafortunados inventores de nuevo..."


translate spanish day6_dv_65bf82fc:


    me "Muy bien..."


translate spanish day6_dv_309686e8:


    sl "¡Gracias!"


translate spanish day6_dv_0167daaa:


    "Ella sonrió dulcemente y se fue corriendo."


translate spanish day6_dv_dd9995b4:


    "Pues bien, un viaje hacia la guarida del enemigo me aguarda delante... a la guarida del malvado Doctor Electronik y el diabólico Profesor Shurik.{w} Doctor Jekyll y Míster Hyde..."


translate spanish day6_dv_1ad8199f:


    "Abrí con firmeza la puerta y entré."


translate spanish day6_dv_45391550:


    el "Semyon..."


translate spanish day6_dv_1d3a15b7:


    "Dijo Electronik un poco ominosamente, mirando de refilón un tipo de aparato que él había estado ajustando con un destornillador."


translate spanish day6_dv_81505d19:


    me "Slavya me envió."


translate spanish day6_dv_e75769fe:


    el "Lo sé."


translate spanish day6_dv_f95c9fc9:


    me "¿Y?"


translate spanish day6_dv_772dd018:


    el "¿Qué?"


translate spanish day6_dv_7c46f700:


    "La segunda puerta se abrió, vino Shurik."


translate spanish day6_dv_839ded9e:


    sh "Semyon."


translate spanish day6_dv_cf64a48c:


    "Me saludó con el mismo tono que Electronik tuvo."


translate spanish day6_dv_1e1be213:


    me "Bueno, ya me saludaron..."


translate spanish day6_dv_27c62366:


    "Bromeé."


translate spanish day6_dv_ccd5a1e0:


    me "Y bien, ¿qué necesitáis de mí?"


translate spanish day6_dv_782ffade:


    el "¡Tenemos una tarea importante para ti!"


translate spanish day6_dv_a2516dfe:


    me "Te escucho."


translate spanish day6_dv_74f9f6cb:


    sh "La enfermera vino ayer."


translate spanish day6_dv_c8c1467c:


    "Se detuvo, como si no quisiera continuar."


translate spanish day6_dv_f95c9fc9_1:


    me "¿Y?"


translate spanish day6_dv_58c13b19:


    "Pregunté con cuidado."


translate spanish day6_dv_cce022c6:


    el "Por lo que tenemos una importante misión para ti."


translate spanish day6_dv_63eb63d4:


    me "Eso ya lo entendí..."


translate spanish day6_dv_c8416a10:


    sh "Vas a entregarle un paquete."


translate spanish day6_dv_0997405a:


    me "¿No podéis hacer eso vosotros mismos? La enfermería está justo ahí mismo."


translate spanish day6_dv_c40a9bb9:


    "Dije escépticamente."


translate spanish day6_dv_b6cbf345:


    el "No podemos."


translate spanish day6_dv_47d017ef:


    "Dijo Electronik sorprendido."


translate spanish day6_dv_379f1dd8:


    th "Desde luego, qué pareja más extraña."


translate spanish day6_dv_11a96bf4:


    me "Ya veo...{w} Espero que no sea otro pesado saco..."


translate spanish day6_dv_f1caa021:


    el "No, no, ¡nada de eso!"


translate spanish day6_dv_f4166d68:


    "Sonrió."


translate spanish day6_dv_0031a3ab:


    me "De acuerdo, muy bien..."


translate spanish day6_dv_1888335c:


    "Acepté prudentemente."


translate spanish day6_dv_3a378445:


    sh "Espera."


translate spanish day6_dv_4ef4398f:


    "Shurik desapareció por la puerta y pronto retornó sosteniendo algo envuelto en papel."


translate spanish day6_dv_b9fafe93:


    sh "Solamente ve con cuidado."


translate spanish day6_dv_d9c0add8:


    me "Vale, vale."


translate spanish day6_dv_abb01373:


    "Cogí el paquete. Aunque los pioneros no me estuvieran dando la llave de las Puertas del Cielo, aun así era como si al menos me estuvieran dando la Antorcha Olímpica."


translate spanish day6_dv_c3714a4b:


    me "¿Puedo irme?"


translate spanish day6_dv_89559a2a:


    el "Por supuesto."


translate spanish day6_dv_a193f014:


    "Dijo Electronik en tono amenazador."


translate spanish day6_dv_98287c05:


    sh "¡Solamente ve con cuidado!"


translate spanish day6_dv_d7cdf7b2:


    "Pidió Shurik."


translate spanish day6_dv_9dd454a1:


    "Afuera, le eché un buen vistazo al paquete."


translate spanish day6_dv_e18730b6:


    "No me dijeron que no lo abriera."


translate spanish day6_dv_2c50bd85:


    th "Aunque sería sensato alejarse un poco."


translate spanish day6_dv_42b9bac1:


    "Rápidamente me fui caminando hasta la plaza y, asegurándome que nadie me estuviera siguiendo, me deslicé dentro del bosque."


translate spanish day6_dv_32712073:


    "Me quedé quieto por un rato, indeciso, mirando el paquete."


translate spanish day6_dv_76fdd246:


    th "Qué interesante.{w} ¿Qué hay en él?{w} ¿Por qué es tan importante para ellos?{w} ¿Por qué no podían llevárselo ellos mismos?"


translate spanish day6_dv_b468a82a:


    "Tenía muchas preguntas."


translate spanish day6_dv_fb32ffc9:


    "Abrí el paquete y vi una botella de vodka «Stolichnaya»..."


translate spanish day6_dv_420166b4:


    th "Bueno, ¿y qué?{w} Pioneros llevando una botella de vodka a la enfermera.{w} O mejor dicho, lo hago yo por ellos.{w} Ocurre todos los días."


translate spanish day6_dv_87a11b40:


    "Envolví de nuevo la botella y me rasqué la cabeza."


translate spanish day6_dv_d78595da:


    th "Esto no es que sea raro, es que es estúpido."


translate spanish day6_dv_72e21fc3:


    th "Tan estúpido que no podía pensar ni una teoría razonable."


translate spanish day6_dv_93a5c183:


    "Me quedé quieto por un rato más, pensé que no debería dejar escapar tan maravillosa oportunidad. Rápidamente caminé hasta la cabaña de la líder del campamento."


translate spanish day6_dv_521a35e9:


    "Gracias a Dios, Olga Dmitrievna no estaba ahí. Entré a hurtadillas y empecé rebuscar entre el contenido del cajón de la mesa."


translate spanish day6_dv_e0aa3f01:


    "Bien pronto, vertí el vodka en una botella de soda, mientras que la botella de vodka la rellené de agua rancia para las plantas."


translate spanish day6_dv_089fc8e9:


    "Contento con mi astuto plan, salí fuera y me di prisa por llegar a la enfermería."


translate spanish day6_dv_1e5e0588:


    th "En serio, ¿qué hay de malo?{w} ¡Ellos casi me tienden una trampa!{w} Si la líder del campamento me hubiera visto..."


translate spanish day6_dv_ef0f0a3b:


    th "¿O si me hubiera visto otra persona?"


translate spanish day6_dv_2fc32561:


    th "Podría habérmelas apañado..."


translate spanish day6_dv_cc444139:


    th "¡Así que no fue para tanto!"


translate spanish day6_dv_c8139598:


    "Reuní todas mis fuerzas y llamé a la puerta de la enfermería."


translate spanish day6_dv_5d7721f0:


    cs "Hola...{w} ¡pionero!"


translate spanish day6_dv_99266ac9:


    me "¡Hola! Tengo un paquete para ti... una botella para que empines el codo... quiero decir, un paquete para tu botiquín."


translate spanish day6_dv_a2debb84:


    "Estaba tan eufórico que empecé a decir tonterías."


translate spanish day6_dv_a9d81cd2:


    cs "Ahh..."


translate spanish day6_dv_c2732a68:


    "Dijo ella."


translate spanish day6_dv_7d07968d:


    cs "Dámelo."


translate spanish day6_dv_aecf76f3:


    "Le di el paquete."


translate spanish day6_dv_b68868ff:


    "La enfermera lo metió en el cajón del escritorio sin mirarlo."


translate spanish day6_dv_bda69ee6:


    cs "No lo abriste, ¿verdad?"


translate spanish day6_dv_f837dd45:


    me "¡Por supuesto que no!"


translate spanish day6_dv_3d78efca:


    "Espeté."


translate spanish day6_dv_aedf1019:


    cs "Buen muchacho."


translate spanish day6_dv_76aba977:


    me "Muy bien, pues me iré..."


translate spanish day6_dv_40e2f3e6:


    cs "Ve, ve... pionero."


translate spanish day6_dv_4aaea0be:


    "Cerré la puerta tras de mí y solté un suspiro."


translate spanish day6_dv_23623d22:


    th "Siempre podría echarle la culpa a los inventores locos y, en mi situación, ¡podría incluso usar mi más básica técnica para aliviar el estrés!"


translate spanish day6_dv_0cd6a776:


    "Llegué a la plaza.{w} El reloj mostraba la hora de mediodía. Eso significaba que la hora de comer se acercaba."


translate spanish day6_dv_e4fa05b8:


    "Me senté en el banco con la clara intención de no moverme de aquí hasta que la campana de aviso sonara."


translate spanish day6_dv_c6cc3d58:


    "Los pioneros estaban corriendo por ahí.{w} Estaban sonriendo y gritando alegremente."


translate spanish day6_dv_94906696:


    th "Sí, definitivamente era un día maravilloso."


translate spanish day6_dv_cdd7919b:


    "Eché un vistazo por la plaza y vi a Lena aproximándose."


translate spanish day6_dv_f3d744ae:


    un "¿Relajándote?"


translate spanish day6_dv_6b635703:


    me "Algo así..."


translate spanish day6_dv_b8aa5db1:


    "Se sentó a mi lado y miró al cielo."


translate spanish day6_dv_2c5438ac:


    un "Bonito día, ¿eh?"


translate spanish day6_dv_31663314:


    me "Lo es..."


translate spanish day6_dv_fde4d309:


    "Estaba un poco incómodo, Lena parecía distinta, no sentía vergüenza, su rostro no estaba sonrojado y estaba segura de sí misma."


translate spanish day6_dv_c20baf86:


    me "Sobre aquella conversación..."


translate spanish day6_dv_8891daf5:


    un "Ya te lo he explicado todo."


translate spanish day6_dv_8cd8b52e:


    me "Claro, pero..."


translate spanish day6_dv_3cd3c424:


    un "Tan sólo ovídalo."


translate spanish day6_dv_48f1666a:


    me "Si tú lo dices..."


translate spanish day6_dv_a4f042ce:


    "Estuvimos sentados por un corto periodo callados."


translate spanish day6_dv_2c070d08:


    me "No puedo, no así..."


translate spanish day6_dv_dd328974:


    un "Y bien, ¿qué piensas sobre Alisa?"


translate spanish day6_dv_d22f0a40:


    "Dijo, ignorando mi pregunta."


translate spanish day6_dv_591ad32f:


    me "¿Que qué pienso sobre Alisa?"


translate spanish day6_dv_86e16e26:


    "Repetí, ensimismado."


translate spanish day6_dv_943e93f2:


    me "La verdad es que no lo sé...{w} ¿Por qué?"


translate spanish day6_dv_caa0bbb4:


    un "Parecías estar muy preocupado ayer."


translate spanish day6_dv_633b4e6d:


    me "Bueno pues claro... ¿Pero qué tiene eso que ver?"


translate spanish day6_dv_a7c0899d:


    un "¡Hay una conexión con ello!"


translate spanish day6_dv_ce3666e5:


    "Se rio."


translate spanish day6_dv_9b6ef66d:


    me "Asumámoslo..."


translate spanish day6_dv_235d1b4e:


    un "¿Y bien?"


translate spanish day6_dv_e7450e08:


    me "Bueno ella..."


translate spanish day6_dv_2350d104:


    "Y otra vez comencé a pensar."


translate spanish day6_dv_111d6d4f:


    th "En serio, ¿qué opinas de ella?"


translate spanish day6_dv_d2293c40:


    me "Bueno ella...{w} En ocasiones ella llega a ser arrogante."


translate spanish day6_dv_ab97ad5d:


    un "Eso es cierto."


translate spanish day6_dv_a0062ec1:


    th "¿Qué otra cosa puedo decir?{w} Ahora que lo pienso, no conozco apenas a Alisa.{w} El modo en que se comporta me hace enfadar a veces."


translate spanish day6_dv_c86c4e80:


    me "Si te estás preguntando si tengo alguna opinión negativa sobre ella, pues no."


translate spanish day6_dv_631a37e5:


    me "Quiero decir que ella se deja llevar por sus arrebatos, pero todo el mundo tiene sus particularidades..."


translate spanish day6_dv_4c144815:


    "Lena me miró fijamente."


translate spanish day6_dv_93207c79:


    un "¿En serio?"


translate spanish day6_dv_97bb8cf6:


    me "¿Dije algo malo?"


translate spanish day6_dv_7bc1d385:


    un "No, en realidad no..."


translate spanish day6_dv_544a3a75:


    "Se rio a carcajadas una vez más, justo antes de que la campana para ir a comer sonara."


translate spanish day6_dv_5aac5414:


    un "¡Muy bien, nos vemos!"


translate spanish day6_dv_7c13b2c0:


    "No la detuve, aunque tenía muchísimas preguntas sin resolver."


translate spanish day6_dv_42970b85:


    th "¿Pero cómo se las hago?{w} ¿Debería acaso?"


translate spanish day6_dv_3869395e:


    "Me levanté y lentamente me arrastré hasta la cantina."


translate spanish day6_dv_14e6034a:


    "Únicamente quedaban unos pocos asientos libres, así que tenía que sentarme con Electronik y Shurik."


translate spanish day6_dv_497bae1a:


    el "¿Lo entregaste?"


translate spanish day6_dv_9513cd87:


    me "Sí..."


translate spanish day6_dv_40231c6f:


    "Respondí perezosamente, hundiendo mi tenedor en el alforfón de kasha."


translate spanish day6_dv_807920a2:


    el "¿Ningún incidente?"


translate spanish day6_dv_a0c4afad:


    me "Ninguno...{w} ¿Qué había por cierto?"


translate spanish day6_dv_01cfac1d:


    el "¿No lo abriste?"


translate spanish day6_dv_2c5335a6:


    "Se me quedó mirando fijamente."


translate spanish day6_dv_5cb822ee_2:


    me "No..."


translate spanish day6_dv_fa45ce2d:


    el "¡Genial!"


translate spanish day6_dv_385405d4:


    "Habiendo terminado mi comida, me fui."


translate spanish day6_dv_bebd5afd:


    "Me sobrevino un repentino bostezo."


translate spanish day6_dv_2984a468:


    th "Hacer una siesta no sería mala idea."


translate spanish day6_dv_f4822807:


    "Me dirigí hacia la cabaña de la líder del campamento."


translate spanish day6_dv_b85dfb0f:


    "Se sentía como si el sol de verano fuera a fundir todo el planeta, junto a todos sus lugareños."


translate spanish day6_dv_4e7b8856:


    "No había lugar donde esconderse... la luz solar parecía iluminar hasta el último rincón. Al alzar la vista te arriesgabas a quedarte ciego."


translate spanish day6_dv_4d2a4c1c:


    "Consumiendo mis pocas últimas fuerzas, giré el pomo de la puerta, entré en la habitación y me derrumbé en la cama."


translate spanish day6_dv_aefac231:


    "Desde luego, no me esperaría que dentro de la cabaña se estuviera muy fresco, pero era mucho mejor que estar afuera."


translate spanish day6_dv_802ba619:


    "Dichosamente, me estiré en la cama y cerré los ojos."


translate spanish day6_dv_799e8d13:


    "Extrañamente, el sueño me abandonó. Estaba agobiado por pensamientos que pululaban por mi cabeza."


translate spanish day6_dv_601d58f7:


    "Ahí va otra teoría, una respuesta sobre la cuestión de cómo llegué hasta aquí.{w} Ahí va una explicación de qué está ocurriendo aquí.{w} Y ahí va la verdadera forma de todos aquellos que habitan este lugar."


translate spanish day6_dv_8152340b:


    "Recuerdo a Lena diciendo: «¿Qué piensas sobre Alisa?»"


translate spanish day6_dv_baeb4f56:


    th "En serio, ¿qué pienso de ella?{w} ¿Qué puedo pensar sobre una persona que solamente he conocido por unos pocos días?{w} ¿Puedo sacar conclusiones basándome en un par de palabras y actos?"


translate spanish day6_dv_4881f3b0:


    th "Aunque sus actos..."


translate spanish day6_dv_a026df63:


    "Me reí un poco."


translate spanish day6_dv_5492f5b1:


    "Cualquier organización relevante habría sacado justo la misma conclusión en base a la explosión del monumento."


translate spanish day6_dv_27abcde7:


    th "Aunque a pesar de todo, yo...{w} ¿no me importa?"


translate spanish day6_dv_e0e83cfe:


    th "¿Tal vez en realidad no me importa en absoluto ella?"


translate spanish day6_dv_79c3d71e:


    th "¿Puede alguien estar interesado en una persona en la que no estás interesado?"


translate spanish day6_dv_32ee260b:


    th "No, eso es algo absurdo..."


translate spanish day6_dv_7e8c00bf:


    "Llamaron en la puerta."


translate spanish day6_dv_6b33d385:


    me "¡Entra!"


translate spanish day6_dv_d96562ca:


    "Estaba demasiado holgazán como para levantarme, así que solamente me recosté un poquito."


translate spanish day6_dv_cc6d016b:


    "La puerta se abrió y vi a Alisa.{w} Vaya, qué coincidencia..."


translate spanish day6_dv_e8401a0f:


    "Se detuvo, insegura, y se me quedó mirando por un par de segundos."


translate spanish day6_dv_344347eb:


    me "Supongo que estás buscando a la líder del campamento..."


translate spanish day6_dv_6a8dcc12:


    th "¿Aunque por qué lo haría?"


translate spanish day6_dv_e8936f84:


    dv "No."


translate spanish day6_dv_e2960fb5:


    me "¿Qué pasa pues?"


translate spanish day6_dv_a09125aa:


    dv "Te buscaba a ti."


translate spanish day6_dv_5e777c99:


    "Parecía estar calmada y para nada hostil."


translate spanish day6_dv_15c7a20d:


    "Me di cuenta al pensar, de que no estaba preocupado por ella y que no tenía problema alguno cuando ella se comportaba así... con normalidad."


translate spanish day6_dv_7332342b:


    me "¿Para qué?"


translate spanish day6_dv_7d4b9078:


    dv "Quiero dejar las cosas claras."


translate spanish day6_dv_0ed62df2:


    "Se sentó en la cama de enfrente."


translate spanish day6_dv_ae56873f_1:


    me "¿Exactamente qué?"


translate spanish day6_dv_4cf6f6d7:


    dv "Quiero aclarar cualquier malentendido o falta de comprensión."


translate spanish day6_dv_32c1cb65:


    me "Muy bien, ¡estoy dispuesto a ello!"


translate spanish day6_dv_d6f5f041:


    dv "¿Has hablado hoy con Lena?"


translate spanish day6_dv_fc30fd2b:


    me "Sí."


translate spanish day6_dv_21a68fd8:


    dv "¿Sobre qué?"


translate spanish day6_dv_96467507:


    me "Nada importante..."


translate spanish day6_dv_3e9127d1:


    dv "Mientes."


translate spanish day6_dv_e9a6a178:


    "Alisa parecía tensa y preocupada.{w} Cruzó sus piernas y se mordió el labio."


translate spanish day6_dv_6300bc11:


    dv "Entiendes que yo...{w} ¿Que yo siempre soy considerada responsable por cualquier cosa que suceda?"


translate spanish day6_dv_0288549d:


    me "Bueno, siempres tienes la culpa."


translate spanish day6_dv_c41aa4a5:


    "Dije desconsideradamente."


translate spanish day6_dv_69364372:


    "Un destello brilló en sus ojos, pero se mantuvo hablando con calma:"


translate spanish day6_dv_f0c03665:


    dv "Quizás...{w} Pero en este caso...{w} Yo y Lena, siempre somos así... incluso si ella empieza algo, ¡siempre es la inocente mientras que yo asumo toda la culpa!"


translate spanish day6_dv_63fdbbbc:


    me "Entonces estás diciendo que Lena...{w} La verdad, no entiendo qué dices."


translate spanish day6_dv_60dc8a1d:


    dv "¡Tú nunca entiendes nada! ¡¿Verdad?!"


translate spanish day6_dv_d1fbe814:


    "Echó chispas."


translate spanish day6_dv_c727773e:


    th "Esto es raro, nunca diría que soy lento para captar las cosas."


translate spanish day6_dv_3ce5fd8b:


    me "Explícate."


translate spanish day6_dv_25156efc:


    dv "¿Sabes que le gustas a Lena?"


translate spanish day6_dv_1aa433e1:


    "Ella se sonrojó tras decir esas palabras."


translate spanish day6_dv_5cb822ee_3:


    me "No..."


translate spanish day6_dv_e0299b09:


    "Respondí honradamente."


translate spanish day6_dv_37a6038e:


    dv "Bueno, pues ahora ya lo sabes..."


translate spanish day6_dv_9ecfc70c:


    me "Ahora sí..."


translate spanish day6_dv_ae829ad1:


    "Un incómodo silencio se apoderó de la habitación."


translate spanish day6_dv_62543cf8:


    "Estaba asimilando lo que ella me dijo, y Alisa parecía insegura de cómo continuar la conversación."


translate spanish day6_dv_768d0b51:


    th "Le gusto a Lena...{w} ¿Podría haberlo supuesto?{w} Improbablemente."


translate spanish day6_dv_49ebd22b:


    "Mi intuición suele ser buena, pero cuando se trata de mí y de mis sentimientos, no funciona bien en absoluto."


translate spanish day6_dv_23bbf0e9:


    "La cosa más rara era que encajé la noticia sin apenas problemas."


translate spanish day6_dv_23eb34e0:


    "Justo en el momento, estaba escuchando a Alisa, solamente estaba concentrado en ella. Ya podría pensar en todo lo demás más tarde."


translate spanish day6_dv_4b721f97:


    dv "¡Y se cree que te voy a separar de ella!{w} Ella se piensa constantemente eso, ¿entiendes? ¡Incluso a pesar de que siempre sea al revés! Todo el mundo la mira porque es como si fuera un ángel, ¡y nadie me presta atención a mí!"


translate spanish day6_dv_61d7ee92:


    dv "Para los demás no soy más que un hombro sobre el que llorar..."


translate spanish day6_dv_3bc3b19b:


    "Alisa comenzó a chillar."


translate spanish day6_dv_095c18d7:


    me "Mira, yo...{w} ¡Por favor no te enfades tanto! ¡No pienso así de ti!"


translate spanish day6_dv_ecfa445c:


    dv "Claro..."


translate spanish day6_dv_c2c94b69:


    "Todavía no tenía ni idea de qué la obligó a ser tan sincera.{w} Y no sabía cómo reaccionar.{w} Además, no estaba para nada preocupado."


translate spanish day6_dv_c62c09a6:


    "Era como si estuviera viendo otro episodio de alguna serie de televisión y, que lo que estaba ocurriendo en la habitación, formaba parte del otro lado de la pantalla."


translate spanish day6_dv_ef389ffb:


    me "Comprendo, fui la causa de todo ello...{w} Pero hoy Lena me dijo que todo está en orden."


translate spanish day6_dv_5eb2b915:


    dv "¡Eso es propio de ella! ¡Probablemente te habrás percatado de que ella no es tan tranquilita como quiere aparentar!"


translate spanish day6_dv_5b3e26db:


    th "Comencé a suponerlo."


translate spanish day6_dv_650270d4:


    me "Si puedo hacer cualquier cosa..."


translate spanish day6_dv_323fb304:


    dv "Dime..."


translate spanish day6_dv_092b3b93:


    "Alisa alzó su mirada.{w} Sólo entonces vi cómo había estado llorando."


translate spanish day6_dv_8c4f1a22:


    "Algún tipo de malestar se revolvió por dentro mío.{w} No podía continuar tratando esta conversación como si fuera ajena a mí por más tiempo."


translate spanish day6_dv_c7e864ed:


    dv "¿Te gusta Lena? ¿O te gusto yo?"


translate spanish day6_dv_da3f2db4:


    "Supongo que debería haber esperado una pregunta como ésa, pero simplemente no estaba preparado."


translate spanish day6_dv_84e48168:


    me "Así sin más..."


translate spanish day6_dv_a0eda2ae:


    th "Lena es dulce por supuesto...{w} Pero Alisa tiene sus puntos fuertes también."


translate spanish day6_dv_a769edd0:


    "Además, la forma en que estaba hace un momento, podría estar a la altura de Lena."


translate spanish day6_dv_a4fad65f:


    me "No os conozco a las dos mucho...{w} Una pregunta como ésa...{w} Es difícil de responder directamente."


translate spanish day6_dv_9a3e6f7f:


    "Si pudiera decir que «no», lo habría hecho, en aquel preciso instante."


translate spanish day6_dv_73ec7242:


    "Pero únicamente tendría también una oportunidad para la otra respuesta."


translate spanish day6_dv_56680e86:


    "Y no sabía qué elegir."


translate spanish day6_dv_f673f8e3:


    dv "Tú quisiste que esta situación se resolviera."


translate spanish day6_dv_80322d80:


    me "Por supuesto, pero..."


translate spanish day6_dv_32cbd9c4:


    dv "¿Por qué «pero» otra vez?"


translate spanish day6_dv_3ea35b71:


    me "¿Por qué «otra vez»?"


translate spanish day6_dv_4c7ded3a:


    dv "¿Por qué el «porqué»?"


translate spanish day6_dv_f21d1a90:


    me "Mira, no vamos a ninguna parte con esto."


translate spanish day6_dv_3baacd9d:


    dv "Bien, ¡pues responde!"


translate spanish day6_dv_db0ec79f:


    "Su rostro recayó en su acostumbrada atrevida arrogancia."


translate spanish day6_dv_d1744488:


    me "Necesito tiempo para pensar..."


translate spanish day6_dv_f097abfb:


    dv "¡Pues adelante, piensa!"


translate spanish day6_dv_b750f4ae:


    "Alisa se levantó bruscamente y se dirigió fuera."


translate spanish day6_dv_5783bb1b:


    dv "¡No te olvides de que tenemos planes para esta noche!"


translate spanish day6_dv_70813d40:


    "Dijo en la entrada de la puerta, sonriendo."


translate spanish day6_dv_d7d9b64d:


    "Cerró la puerta al irse antes de que pudiera decir cualquier cosa."


translate spanish day6_dv_94e61f1e:


    th "¿Planes? ¿Qué tipo de planes...?"


translate spanish day6_dv_76c05f84:


    "La conversa con Alisa me dejó una fuerte impresión."


translate spanish day6_dv_37b565c4:


    th "Así que le gusto a Lena..."


translate spanish day6_dv_0e2e48a7:


    th "Por sí mismas, esas novedades podrían trastornar y sobresaltar. Y yo, por encima de eso, estaba obligado a tomar una decisión. Una difícil, por decir un eufemismo. Y cualquier elección que tomase sería un error para la otra."


translate spanish day6_dv_7e235707:


    th "Si tan sólo pudiera rechazar a ambas..."


translate spanish day6_dv_e7446732:


    "Aunque no me agradaba la propia palabra «rechazar»."


translate spanish day6_dv_92bca9c0:


    th "Si pudiera contestar su cuestión con un «no», sería mucho más fácil."


translate spanish day6_dv_4cf63875:


    "Pero sabía que no funcionaban las cosas de esa manera."


translate spanish day6_dv_1aaf9536:


    th "Después de hablar con Alisa, me di cuenta de que ya nada iba a ser igual que antes."


translate spanish day6_dv_0b14c784:


    "No puede serlo, da igual lo que suceda.{w} Y tendré que tomar una decisión..."


translate spanish day6_dv_0fa075d4:


    th "Puesto que rechazo el supuesto «no», tenía que escoger una solución positiva.{w} Si, desde luego, pudiera aceptarla."


translate spanish day6_dv_66117c1c:


    "Pero estoy seguro de que al margen de todos los posibles resultados, no hay ninguna que pueda satisfacer a todos, pero también sería alguna por la que quisiera luchar."


translate spanish day6_dv_0086716d:


    "Y la elección parecía obvia: Lena.{w} Ella es preciosa, dulce y amable."


translate spanish day6_dv_6cd9e693:


    "Pero por otra parte, había algo en Alisa que me atraía."


translate spanish day6_dv_be0923e0:


    "Repentinamente la puerta se abrió de golpe y Olga Dmitrievna entró."


translate spanish day6_dv_0bae5ad4:


    mt "Holgazaneando otra vez, ¿eh?"


translate spanish day6_dv_fe458b8c:


    me "Solamente descansaba...{w} Hace mucha calor afuera..."


translate spanish day6_dv_2ebe60a9:


    mt "Ve y ayuda a Slavya, que estaba pidiendo ayuda."


translate spanish day6_dv_25f7abbc:


    me "¿Dónde?"


translate spanish day6_dv_7a91aa07:


    "Pregunté perezosamente."


translate spanish day6_dv_a18b435d:


    mt "En la pista de deportes."


translate spanish day6_dv_5c421508:


    "A regañadientes, me levanté y me dirigí fuera."


translate spanish day6_dv_ee607f36:


    "Debería realmente ir a tomar el viento de todos modos."


translate spanish day6_dv_a7fe7928:


    "Había mucha gente en la pista de deportes.{w} Algunos estaban jugando a fútbol, otros jugaban a bádminton.{w} Los críos más jóvenes se perseguían los unos a los otros."


translate spanish day6_dv_2a454c26_1:


    sl "¡Semyon!"


translate spanish day6_dv_876e414e:


    "Slavya caminó hacia mí."


translate spanish day6_dv_bf8808dd:


    me "¿Requieres de mi asistencia?"


translate spanish day6_dv_ecb3efc7:


    "Le pregunté con un tono solemne."


translate spanish day6_dv_b793031f:


    sl "Sí...{w} Escucha, ¿cogiste la llave de la enfermería?"


translate spanish day6_dv_5a7a7577:


    me "No."


translate spanish day6_dv_9babb1c2:


    th "¡¿Por qué yo otra vez?!"


translate spanish day6_dv_93bd5577:


    sl "Bueno, has estado por ahí mucho recientemente."


translate spanish day6_dv_ac1c31df:


    th "Supongo que tiene razón..."


translate spanish day6_dv_5cb822ee_4:


    me "No..."


translate spanish day6_dv_234b5492:


    sl "¿Quizá sepas quién fue?"


translate spanish day6_dv_3119e7b5:


    me "No. ¿Por qué?"


translate spanish day6_dv_da46fa34:


    sl "La enfermera me vino pidiendo mi llave... ya sabes que tengo una llave de cada edificio del campamento.{w} Me dijo que la suya se perdió."


translate spanish day6_dv_4070ac03:


    me "Podría ser que la perdiera..."


translate spanish day6_dv_0fe6b15a:


    "Me encogí de hombros."


translate spanish day6_dv_1197e9fc:


    sl "No, me dijo que no pudo ser."


translate spanish day6_dv_85cd4b82:


    me "Pues yo no lo sé."


translate spanish day6_dv_35b300f9:


    sl "Vale."


translate spanish day6_dv_0049a2fb:


    "Sonrió y se fue corriendo."


translate spanish day6_dv_52ec3723:


    "Me dirigí hacia la cabaña de la líder del campamento, con la determinación de dormir después de todo."


translate spanish day6_dv_02ad66d4:


    "Olga Dmitrievna no estaba ahí, gracias a Dios, así que me tumbé en la cama y perdí el conocimiento."


translate spanish day6_dv_a20cefa7:


    "..."


translate spanish day6_dv_4df22cd5:


    "No recuerdo exactamente qué soñé, pero me dejó una fuerte impresión, como lo hacen las siestas tras el mediodía."


translate spanish day6_dv_ea3f5806:


    "Me esforcé por levantarme de la cama y miré el reloj.{w} Eran justo pasadas las siete."


translate spanish day6_dv_53b55b1b:


    th "Nada de sorprendente, si lo piensas... debí haberme esperado que me dormiría durante todo el día."


translate spanish day6_dv_23d4a9c7:


    th "Aunque, tal como fue, me perdí así mismo la cena..."


translate spanish day6_dv_31e3afb8:


    "Dicho pensamiento me molestó, así que me dirigí hacia la salida."


translate spanish day6_dv_ee3bcd9a:


    "Me detuve en la entrada de la puerta y pensé en qué hacer ahora."


translate spanish day6_dv_df471347:


    "No sé cuánto tiempo estuve quieto ahí... mi cabeza todavía estaba espesa tras dormir... si alguien no me hubiera llamado."


translate spanish day6_dv_280c3ef3:


    "Slavya estaba caminando rápidamente hacia la cabaña."


translate spanish day6_dv_1e187d3c:


    sl "Semyon..."


translate spanish day6_dv_eef5cfb9:


    "Empezó a decir mientras recuperaba el aliento."


translate spanish day6_dv_fce6b834:


    sl "¿Estás seguro de que no tienes la llave de la enfermería?"


translate spanish day6_dv_cafb72c1:


    me "Por supuesto que no, ¿para qué la iba a necesitar?"


translate spanish day6_dv_af3f3208:


    sl "Bueno, ¿sabes quién podría haberla cogido?"


translate spanish day6_dv_cddb9533:


    me "No. ¿Por qué?"


translate spanish day6_dv_a19bb019:


    sl "Bueno... Algo se perdió."


translate spanish day6_dv_8b677452:


    me "¿Qué?"


translate spanish day6_dv_b9cf32d1:


    sl "Algo importante..."


translate spanish day6_dv_eba68a85:


    "Slavya dudó."


translate spanish day6_dv_28ce78b4:


    sl "Un medicamento."


translate spanish day6_dv_eb5ac138:


    th "Entonces, ¿todo este revuelo se empezó por algún medicina?{w} Aunque..."


translate spanish day6_dv_89141735:


    "Reflexioné."


translate spanish day6_dv_19d82568:


    th "Quiero decir, tengo la botella en mi cabaña...{w} ¿Se refiere a eso?"


translate spanish day6_dv_bbccfea0:


    "Traté de poner cara de póquer, sin hacer nada que pudiera delatarme."


translate spanish day6_dv_065dee42:


    me "Vale.{w} Pues no lo sé, lo siento..."


translate spanish day6_dv_6f9aa3f1:


    sl "De acuerdo..."


translate spanish day6_dv_280be8fe:


    "Dijo Slavya frustrada."


translate spanish day6_dv_e22d0105:


    "La miré y traté de imaginarme cómo de mala era la situación para mí, o si me hallaba en problemas."


translate spanish day6_dv_dffe083c:


    th "Por un lado, el vodka... Yo...{w} lo hurté."


translate spanish day6_dv_935f06ca:


    th "Y por el otro, ella dice que «se perdió», como si desapareciera por sí mismo, sin influencia externa."


translate spanish day6_dv_383cfe31:


    th "Aunque es bastante improbable que ella dijera que una botella de vodka fuera robada de la enfermería."


translate spanish day6_dv_910fce4e:


    th "Así que la forma en que lo dijo es la correcta. El problema era distinto."


translate spanish day6_dv_1072ee7f:


    "De repente me asusté de verdad."


translate spanish day6_dv_73f94476:


    th "¿Y si descubren que fui yo?"


translate spanish day6_dv_65b4be16:


    th "Incluso aunque no fuera la botella lo que buscan, la acabarían encontrando mientras buscasen lo que se perdió."


translate spanish day6_dv_dea23e04:


    "Básicamente, ¡tenía que tomar partido cuanto antes!{w} ¡Y lo primero que había que hacer ahora era deshacerme de toda evidencia!"


translate spanish day6_dv_bea31ad1:


    "Entré de golpe en la cabaña, puse la botella bajo mi cinturón y ropa, y salí corriendo hacia el bosque.{w} Enterrarla en alguna parte era la mejor opción por ahora."


translate spanish day6_dv_27c88b77:


    "Me tomé mi tiempo en localizar un buen lugar, sin notar que la noche cayó sobre el campamento."


translate spanish day6_dv_768e9627:


    "Una voz familiar me sorprendió."


translate spanish day6_dv_93b5fc50:


    dv "¡Ey!"


translate spanish day6_dv_66bf0ead:


    "Apenas pude darme la vuelta, puse la cara más normal posible."


translate spanish day6_dv_dbd3bc52:


    dv "Y bien. ¿Qué haces por aquí?"


translate spanish day6_dv_385b3fe9:


    me "Oh, sólo daba un paseo..."


translate spanish day6_dv_405fbe4e:


    "Mi voz sonaba tranquila, pero lo comprendí... no podía engañar a Alisa."


translate spanish day6_dv_4529043a:


    dv "¿Solo? ¿Por la noche? ¿En el bosque?"


translate spanish day6_dv_4e1558a6:


    me "Bueno sí... ¿qué pasa?"


translate spanish day6_dv_fcd85f45:


    dv "Nada. Nada en absoluto."


translate spanish day6_dv_1b595140:


    "Se me quedó mirando fijamente."


translate spanish day6_dv_76aba977_1:


    me "Vale, pues yo..."


translate spanish day6_dv_c250ee11:


    dv "¡Quieto!{w} ¿No te estás olvidando de algo?"


translate spanish day6_dv_01f23cac:


    me "No creo."


translate spanish day6_dv_4daa4260:


    dv "¡Teníamos un acuerdo!"


translate spanish day6_dv_c233b6a2:


    me "¿Sobre qué?"


translate spanish day6_dv_59e88892:


    dv "¡Sobre esta noche!"


translate spanish day6_dv_1a41c767:


    "Creo que tiene razón. Había algo así."


translate spanish day6_dv_d72e579c:


    me "Sí, pero sabes..."


translate spanish day6_dv_afd1c304:


    dv "¡No sé nada! ¡Vayámonos!"


translate spanish day6_dv_d3dbfb33:


    "Se dio la vuelta y me hizo un gesto para que la siguiera."


translate spanish day6_dv_2a3aebf5:


    "Pensé que no tenía ningún sentido discutir... ninguna palabra o acción lo podría impedir."


translate spanish day6_dv_fe0a8a5c:


    th "Y entonces..."


translate spanish day6_dv_1faec534:


    "Suspiré y la seguí."


translate spanish day6_dv_b5027457:


    "Habían pasado un par de minutos cuando llegamos a su cabaña."


translate spanish day6_dv_4d608c0d:


    "Mientras estuvimos caminando, estuve ocupado tratando de encontrar el mejor momento y lugar para deshacerme del vodka, pero Alisa estuvo tan cerca de mí que no pude lograr nada."


translate spanish day6_dv_dbeba143:


    dv "¿Y bien?"


translate spanish day6_dv_1b595140_1:


    "Se me quedó mirando."


translate spanish day6_dv_d6f22291:


    me "¿Bien qué?"


translate spanish day6_dv_e3e21bbe:


    "Estaba tan nervioso que mi cabello estaba húmedo por la sudor."


translate spanish day6_dv_2ffa378a:


    "La única cosa importante en mi mente en aquel momento era cómo ocultar la botella para que no la notara."


translate spanish day6_dv_988e7f2d:


    dv "¿Qué dices?"


translate spanish day6_dv_a4388070:


    me "¿Sobre qué?"


translate spanish day6_dv_3614794f:


    "Alisa reflexionó."


translate spanish day6_dv_8c1b9d53:


    dv "Vale, supongo que tienes razón... te he traído aquí conmigo con tal de ser yo quien decida."


translate spanish day6_dv_505d91ae:


    me "Vale..."


translate spanish day6_dv_dea40394:


    "Acepté, inseguro."


translate spanish day6_dv_92e1ccd1:


    dv "Espera un minuto..."


translate spanish day6_dv_cb6c3ebb:


    "Buscó bajo la cama y en pocos segundos...{w} ¡sacó una botella de vodka «Stolichnaya»!"


translate spanish day6_dv_0ab734f4:


    "Estaba impresionado."


translate spanish day6_dv_3ff96290:


    dv "Sorprendido, ¿eh?"


translate spanish day6_dv_36d2fce0:


    th "¡Desde luego!{w} Aunque no por el propio vodka, sino por el hecho de que era exactamente la misma botella que llevé a la enfermería por la mañana."


translate spanish day6_dv_1d557f44:


    "El turbio contenido lo denotaba."


translate spanish day6_dv_4d1c9363:


    dv "¿Y bien?"


translate spanish day6_dv_13181c03:


    "Alisa me hizo una mirada cómplice."


translate spanish day6_dv_ad02f53a:


    me "¿Qué?"


translate spanish day6_dv_9a78a547:


    dv "¿Quieres echar un trago?"


translate spanish day6_dv_235f9753:


    "No sabía qué decir a eso."


translate spanish day6_dv_d54f3e81:


    th "Lo único que ella tenía entre manos era solamente agua asquerosa. El verdadero vodka estaba en la botella bajo mi cinturón y ropa."


translate spanish day6_dv_8d2027c8:


    me "Bueno, yo no soy de eso..."


translate spanish day6_dv_77dfc065:


    dv "¡No me digas que no bebes!"


translate spanish day6_dv_9859878b:


    "Alisa hizo un puchero."


translate spanish day6_dv_d2ea3d33:


    me "No lo sé... No, yo sólo..."


translate spanish day6_dv_c9e23ee7:


    dv "¡Muy bien pues! ¡Sólo un segundo!"


translate spanish day6_dv_f816894f:


    "En tan solamente un instante, Alisa sacó dos vasos y los llenó hasta arriba."


translate spanish day6_dv_3a7f0ff5:


    dv "¡Hagámoslo!"


translate spanish day6_dv_bdc908c8:


    "Dijo ella dándome uno de los vasos y, sin que pudiera contestarle, bebió del suyo."


translate spanish day6_dv_31c05c43:


    dv "¡Puajjj!"


translate spanish day6_dv_8ad16087:


    "Se atragantó."


translate spanish day6_dv_17c04f77:


    "En un instante se dio cuenta..."


translate spanish day6_dv_1df814f8:


    dv "¡¿Pero qué demonios es esto?!"


translate spanish day6_dv_e54a6b95:


    "Alisa comenzó a escupir."


translate spanish day6_dv_c73f3fec:


    dv "¡¿A esto lo llaman vodka?!"


translate spanish day6_dv_420183fc:


    "A pesar de mis circunstancias, no pude evitar reírme."


translate spanish day6_dv_b2fde6e2:


    dv "¿De qué te estás riendo?"


translate spanish day6_dv_6858883c:


    "Gritó Alisa, roja como un tomate."


translate spanish day6_dv_9ed6914d:


    dv "Te estoy preguntando, ¡¿qué te hace tanta gracia?!"


translate spanish day6_dv_6c56b254:


    me "La verdad es que es gracioso."


translate spanish day6_dv_0c423888:


    dv "¡Ah! ¿Así que te estás divirtiendo? ¡Bébete el tuyo, verás que risas luego!"


translate spanish day6_dv_2488dfae:


    "No tenía ningún deseo de probar el agua rancia, pero solamente para evitar que sospechase, le pregunté:"


translate spanish day6_dv_7150554e:


    me "Y bien, ¿cómo sabe?"


translate spanish day6_dv_a0c02db8:


    dv "Pues como agua pudrida."


translate spanish day6_dv_e0350d8e:


    "Dijo Alisa enfadada."


translate spanish day6_dv_8d978d04:


    dv "Sólo iba a relajarme un poco, tener algo de descanso culto, y luego esto..."


translate spanish day6_dv_84e0b485:


    me "Descanso culto..."


translate spanish day6_dv_4243032b:


    "Repetí, en tanto un nuevo ataque de risa hizo que me partiera por la mitad."


translate spanish day6_dv_43e1408d:


    dv "¡Ey, para ya!"


translate spanish day6_dv_df3fece3:


    "Me gritó Alisa y se me abalanzó encima."


translate spanish day6_dv_5fb59874:


    "Ella intentaba golpearme, o morderme u otra cosa. Quizá su plan era aplastarme hasta morir por su autoridad. Simplemente le agarré de sus manos y, todo lo que pudo hacer ella, era soplarme iracunda con impotencia."


translate spanish day6_dv_88ce415c:


    "Todo iba bien hasta que las piernas de Alisa se resbalaron y comenzó a caer."


translate spanish day6_dv_550fc70f:


    "La solté de las manos, así que ella se cayó justo sobre mí, golpeándome con un cabezazo doloroso en la nariz."


translate spanish day6_dv_3d629c43:


    "Gemí de dolor, la aparté de un empujón y retornó a su lado."


translate spanish day6_dv_bc81e122:


    dv "¿Qué tenemos aquí?"


translate spanish day6_dv_6d5303d4:


    "Cuando me percaté que ella se apoderó de la botella, todo el dolor desapareció."


translate spanish day6_dv_679b46e1:


    me "¡Devuélvemela!"


translate spanish day6_dv_7be7066b:


    "Intenté recuperar la botella, pero Alisa hábilmente saltó de la cama y corrió alejándose hasta el extremo de la habitación."


translate spanish day6_dv_8e960ce3:


    dv "¡Veamos qué hay!"


translate spanish day6_dv_f8e2a6f3:


    "Inmediatamente retiró el tapón y le dio un trago."


translate spanish day6_dv_652453a6:


    "En un instante, su rostro se puso colorado como un tomate y sus ojos parecían que se le iban a salir de sus órbitas."


translate spanish day6_dv_bc888301:


    dv "¡¿Qué... qué es esto?!"


translate spanish day6_dv_17c09957:


    "Carraspeó mientras recuperaba sus sentidos."


translate spanish day6_dv_a4a2a1fa:


    me "Eso es en realidad vodka..."


translate spanish day6_dv_2f3f0691:


    "Parece que al final acepté la derrota."


translate spanish day6_dv_38cbafd3:


    dv "¿Pero dónde lo conseguiste...?"


translate spanish day6_dv_c5adf9aa:


    me "¿De verdad te creíste que robaste el de verdad en la enfermería?"


translate spanish day6_dv_cac1539b:


    "Ella asintió."


translate spanish day6_dv_009a5837:


    me "Bueno, era la botella correcta, está claro, pero su contenido..."


translate spanish day6_dv_a99785e2:


    dv "¡Vaya, qué ingenioso!"


translate spanish day6_dv_eadf401a:


    "Ella ya estaba comenzando a mascullar las palabras."


translate spanish day6_dv_ec4582f5:


    "Alisa se acercó a la ventana, derramó el agua de mi vaso y lo reemplazó con vodka."


translate spanish day6_dv_3a7f0ff5_1:


    dv "¡Venga va!"


translate spanish day6_dv_413213a1:


    me "¡Que no lo quiero!"


translate spanish day6_dv_09bcc5f4:


    th "En serio, este no es ni el momento ni el lugar idóneos."


translate spanish day6_dv_d7cfce4d:


    dv "¡Bébetelo! ¡O sino le diré a todos que tú robaste la botella!"


translate spanish day6_dv_348ce313:


    me "¡Pero fuiste tú!"


translate spanish day6_dv_b144a585:


    dv "Bueno, ¡pero tú tienes el vodka!"


translate spanish day6_dv_20df12ec:


    me "¡No puedes demostrarlo!"


translate spanish day6_dv_8e3540a1:


    dv "¿Te crees que tengo que hacerlo?"


translate spanish day6_dv_da707ec9:


    "Dijo Alisa maliciosamente."


translate spanish day6_dv_edfca763:


    "Inseguro de qué hacer, me quedé mirando fijamente el vaso."


translate spanish day6_dv_8e14b6e7:


    th "Lo que importa es deshacerse del vodka, y esta es una de las maneras de hacerlo..."


translate spanish day6_dv_a1cabb40:


    "Expiré bruscamente y me tragué el contenido del vaso de un solo trago."


translate spanish day6_dv_a5f5245d:


    "Un breve momento después, me arrepentí de ello."


translate spanish day6_dv_257940d2:


    me "¿Tienes algo para... digerir esto?"


translate spanish day6_dv_4e45e644:


    dv "¡Espera un segundo!"


translate spanish day6_dv_0e2d45d5:


    "Dijo Alisa emocionada, y sacó un gran salchichón de su escritorio."


translate spanish day6_dv_da1a8792:


    "Se lo arrebaté de sus manos y le di un gran mordisco."


translate spanish day6_dv_acf11e47:


    me "Buff..."


translate spanish day6_dv_9c8b084d:


    dv "¿Y qué tal era?"


translate spanish day6_dv_bdd0a304:


    me "¿Y a mi me lo preguntas...?{w} ¡No es que me ponga a empinar el codo todos los días sabes!"


translate spanish day6_dv_2919a3a5:


    "No entendía realmente cómo había aceptado esto tan fácilmente."


translate spanish day6_dv_9c8b084d_1:


    dv "Bueno, ¿qué tal era?"


translate spanish day6_dv_183e652e:


    me "Como...{w} ¡Como sabor a chicle de vodka!"


translate spanish day6_dv_e5ec287e:


    dv "Muy bien, ¡pues es la hora de otra ronda!"


translate spanish day6_dv_ece6c281:


    "Abrió la botella otra vez y llenó hasta la mitad ambos vasos."


translate spanish day6_dv_947b745d:


    me "¿No te parece mucho?"


translate spanish day6_dv_9e0a34e8:


    dv "¡No hay vuelta atrás!"


translate spanish day6_dv_dc7ac58a:


    me "Bueno, puesto que ya empezamos..."


translate spanish day6_dv_8778e051:


    "El alcohol empezó hacer efecto. Sin pensármelo mucho, me bebí de un trago el vodka."


translate spanish day6_dv_28cad17a:


    "Alisa era claramente menos experimentada... estaba teniendo problemas con su segundo vaso."


translate spanish day6_dv_84399dec:


    dv "Uff...{w} Cómo hacerlo..."


translate spanish day6_dv_5f08e9f7:


    "Ya parecía estar realmente muy borracha."


translate spanish day6_dv_17750322:


    me "«¡Si ya saben cómo me pongo, para qué me invitan!»"


translate spanish day6_dv_3f99c8fe:


    "Dije contento, echándome un poco más."


translate spanish day6_dv_02260da3:


    "Algo profundo dentro de mí me decía que era demasiado, pero la escopeta ya estaba cargada y preparada."


translate spanish day6_dv_4ff95f61:


    dv "¿Y qué hay de mi?"


translate spanish day6_dv_0cb8369f:


    "Alisa estaba encolerizada."


translate spanish day6_dv_5097c292:


    me "¿Es que no has tenido suficiente?"


translate spanish day6_dv_a4506eb9:


    dv "¡De ninguna manera!"


translate spanish day6_dv_d009fa63:


    "Ella me arrebató la botella de mi mano y comenzó a beber directamente."


translate spanish day6_dv_ea0035fe:


    me "¡Ey!{w} ¡Tenemos vasos ya sabes!"


translate spanish day6_dv_99af8ac3:


    dv "¡Y un cuerno!"


translate spanish day6_dv_6dee2c25:


    "Gritó y vociferó entre carcajadas de risas."


translate spanish day6_dv_b13a1ec7:


    dv "¡Esto es genial!"


translate spanish day6_dv_f6dadb64:


    "Dijo, recuperando el aliento."


translate spanish day6_dv_bddbfd2a:


    me "Pues claro, no está mal."


translate spanish day6_dv_49f40434:


    "Miré la botella... nos bebimos dos tercios."


translate spanish day6_dv_4fc941de:


    "Estaba definitivamente borracho llegados a este punto, y Alisa parecía estar incluso peor."


translate spanish day6_dv_c365b5cf:


    th "Por otro lado, ¡apenas quedaba nada!"


translate spanish day6_dv_f1f22231:


    "Suspiré y llené un vaso hasta arriba para mi y otro por la mitad para Alisa."


translate spanish day6_dv_1ddb42b6:


    dv "¡Eso es todo!"


translate spanish day6_dv_ddc629f8:


    "Dichas estas palabras, arrojó la botella por la ventana."


translate spanish day6_dv_4037402e:


    me "¿Y si la encuentran?"


translate spanish day6_dv_31a41270:


    dv "¡Oh, qué más da!"


translate spanish day6_dv_b4d6e8a8:


    "Ambos nos reímos."


translate spanish day6_dv_1e6410a5:


    me "Pues vale. ¡Por todas las damas presentes!"


translate spanish day6_dv_f9f12001:


    "Refunfuñé y me lo bebí."


translate spanish day6_dv_1db3dad4:


    dv "¡Y por todos los caballeros!"


translate spanish day6_dv_4a63f2cd:


    "Alisa Siguió mi ejemplo."


translate spanish day6_dv_04ff19b4:


    dv "Me estoy mareando..."


translate spanish day6_dv_96ca161a:


    "Se desabrochó unos cuantos botones de su camisa."


translate spanish day6_dv_8f838576:


    "La miré prudentemente."


translate spanish day6_dv_4fce3e9d:


    th "¿Siempre había sido así de bella?{w} ¿O acaso todo el mundo me parece bello llegados a este punto...?"


translate spanish day6_dv_49ff7c76:


    "Fuera lo que fuese, no podía ser capaz de apartar la mirada."


translate spanish day6_dv_11513fad:


    dv "¡Te vas a caer por un precipicio si sigues mirándome así!"


translate spanish day6_dv_93c166ce:


    "Se rio Alisa."


translate spanish day6_dv_44b2f075:


    me "Lo siento..."


translate spanish day6_dv_e18f175c:


    "Avergonzado, aparté mi mirada a mis pies."


translate spanish day6_dv_59610270:


    dv "Puedes mirar si estás tan interesado."


translate spanish day6_dv_06bac448:


    "Con algo de esfuerzo, abrí un ojo y la vi cómo continuaba desabrochándose los botones de su camisa."


translate spanish day6_dv_f506e4ea:


    "Me quedé sin palabras en la boca.{w} Además, ¿quién necesitaría palabras en un momento como ése?"


translate spanish day6_dv_a20cefa7_1:


    "..."


translate spanish day6_dv_1f2532e5:


    "Lo que sucedió después, fue lo que se supone que tiene que ocurrir."


translate spanish day6_dv_ec3e3dbf:


    "En ese instante era incapaz de pensar qué era lo correcto."


translate spanish day6_dv_3c562aeb:


    "Y tampoco quería... simplemente me sentía bien."


translate spanish day6_dv_a20cefa7_2:


    "..."


translate spanish day6_dv_4d03f762:


    "La bombilla se fundió, o uno de los dos la apagó, independientemente de eso la habitación se quedó a oscuras."


translate spanish day6_dv_0a606dd1:


    "Miré alrededor de su cabaña y pensé en una cosa."


translate spanish day6_dv_422a1788:


    me "¿Dónde está Ulyana?"


translate spanish day6_dv_126253a8:


    dv "¿Qué? ¿Estás preocupado porque alguien entre y nos vea?"


translate spanish day6_dv_2dc438e7:


    "Alisa se rio amablemente."


translate spanish day6_dv_2d3ba9c4:


    me "Podría ocurrir..."


translate spanish day6_dv_ae2894c9:


    dv "Le pedí que se pasara la noche en la cabaña de otra muchacha."


translate spanish day6_dv_4ff54a12:


    me "Espera, entonces tú...{w} ¿planeaste todo esto?"


translate spanish day6_dv_8c03dbe0:


    dv "Bueno, algo como esto no, desde luego..."


translate spanish day6_dv_938d5502:


    "Me miraba atentamente, sonrió y cerró sus ojos."


translate spanish day6_dv_c63ec5a4:


    me "Claro..."


translate spanish day6_dv_6c190789:


    "Miré fijamente el techo durante un buen rato y pronto escuché una respiración profunda en voz baja... Alisa estaba dormida."


translate spanish day6_dv_1899453f:


    "Repentinamente tenía dolor de cabeza... el alcohol parecía hacer estragos."


translate spanish day6_dv_8fbfda03:


    "Lo mejor para estos casos es seguir el ejemplo y dormir también.{w} Antes de acabar de pensar en ello, ya me estaba durmiendo rápidamente."
# Decompiled by unrpyc: https://github.com/CensoredUsername/unrpyc
