
translate spanish day1_e9644438:


    "Un radiante rayo de sol cegó mis ojos."


translate spanish day1_8e82fc0a:


    "Al principio no le di importancia, puesto que aun no estaba despierto del todo."


translate spanish day1_44c308e2:


    "Mis piernas me llevaron hacia la puerta por inercia."


translate spanish day1_ccc28a84:


    me "¡Maldita sea, parece que me dormí y perdí mi parada!"


translate spanish day1_aee379b4:


    "Pero no había puertas..."


translate spanish day1_d42615e7:


    "He mirado alrededor del autobús y me he dado cuenta de que no era un buen y viejo Marcopolo desgastado, en su lugar el autobús era un modelo Icarus, ¡y nuevo!"


translate spanish day1_03cecdb6:


    "Me quedé de piedra, conmocionado."


translate spanish day1_3665cd10:


    th "¿Cómo...?{w} ¿Por qué...?{w} ¿Estoy muerto...?"


translate spanish day1_2df08a3e:


    th "¿He sido secuestrado?"


translate spanish day1_a23b7265:


    th "No, debo de estar muerto..."


translate spanish day1_a2eb760d:


    "Toqué mi cuerpo descontroladamente, me abofeteé en la cara unas veces, golpeé el respaldo de la silla de delante con mi frente..."


translate spanish day1_892f12f3:


    th "Está claro: tanto si todavía estoy vivo como si estoy muerto, puedo seguir sintiendo el dolor."


translate spanish day1_b55f45ec:


    th "¡¿Pero cómo podría ocurrir todo esto?!"


translate spanish day1_8b1a1045:


    th "Tal vez estuve dormido demasiado tiempo y al final acabé en la estación de autobuses."


translate spanish day1_fa8f47fe:


    th "Y entonces qué, ¿me pusieron dentro de otro autobús...?"


translate spanish day1_05f4c741:


    "Salí corriendo a echar un vistazo alrededor."


translate spanish day1_7fd7201a:


    "Todo era campo verde allá donde miraba: hierba espesa en los bordes de la carretera, árboles, flores..."


translate spanish day1_0c35f127:


    th "¡Verano!{w} Pero... ¡¿Cómo?!{w} Era invierno hace sólo un momento..."


translate spanish day1_ae3344e7:


    "Tenía un terrible dolor de cabeza.{w} Como si mi cabeza fuera a explotar."


translate spanish day1_2208f558:


    "Poco a poco, empecé a recordar."


translate spanish day1_8d858ed9:


    "Una gran carretera que se extiende hacia el horizonte; bosques, llanuras, campos, lagos y nuevamente bosques."


translate spanish day1_786f868f:


    th "Creo que estaba durmiendo; pero entonces, ¿cómo puedo recordar todo esto?"


translate spanish day1_2f605107:


    "Y luego...{w} Una laguna..."


translate spanish day1_5fabbbed:


    "Una muchacha se inclina hacia mi."


translate spanish day1_73437152:


    "Ella susurra suavemente alguna cosa en mi oído."


translate spanish day1_c2d5fb61:


    "Y luego otra vez una laguna..."


translate spanish day1_2e96e275:


    "Y entonces me desperté aquí."


translate spanish day1_c8d2bde3:


    th "¿Quién es esa extraña muchacha?"


translate spanish day1_a1a6b8d6:


    th "¿O ella sólo fue un sueño?"


translate spanish day1_ebfa0491:


    "Por alguna razón pensar en ella me hizo sentir mejor y me tranquilizó un poco. Sentí aflorar calidez desde mi interior."


translate spanish day1_fde77cc5:


    th "¿Podría ser ella quién me trajo aquí?"


translate spanish day1_9bacf66f:


    th "¡Entonces necesito encontrarla!"


translate spanish day1_a35e9118:


    th "Y la mejor manera de buscarla es salir de aquí."


translate spanish day1_378336bd:


    "Corrí hacia la izquierda, después hacia la derecha, luego me detuve dudando hacia dónde ir y, finalmente, corrí en la dirección de donde el autobús probablemente vino."


translate spanish day1_a20cefa7:


    "..."


translate spanish day1_a20cefa7_1:


    "..."


translate spanish day1_723ee7f4:


    "El ejercicio físico hace resfrescar la mente de uno. Los pensamientos se vuelven claros y se vuelve un poco más fácil evaluar la realidad que te rodea."


translate spanish day1_cb806649:


    "No es mi caso, aun así... Estaba sentado en el borde de la carretera, jadeando e intentando recuperar mi aliento respirando el aire caliente."


translate spanish day1_6d244e52:


    "En cualquier caso, la carrera hizo su trabajo: el miedo desapareció por un momento."


translate spanish day1_adb77bc9:


    th "¿Tal vez realmente sólo estoy soñando...?"


translate spanish day1_93fa4347:


    "Sin embargo después de recordar mi dolor, inmediatamente rechacé la idea."


translate spanish day1_fae72ec6:


    me "No estoy ni soñando ni muerto..."


translate spanish day1_36f36703:


    "Una estrecha carretera recorría el campo lejos en la distancia.{w} Esa exacta carretera de mi sueño."


translate spanish day1_21bd3c65:


    th "Debo estar muy lejos de mi hogar."


translate spanish day1_41a25460:


    "Y no es que fuera invierno ayer y hoy sea completamente verano."


translate spanish day1_fe462b7c:


    "Tiene que ver con el lugar."


translate spanish day1_610fd3ae:


    th "Por supuesto, el verano es normalmente así: verde y caluroso; pero todo lo de aquí no es como en la vida real."


translate spanish day1_b66d48e9:


    "Todo parece que haya sido tomado de los panoramas pintados por los autores rusos del siglo XIX."


translate spanish day1_d7a6969b:


    "La hierba es demasiado abundante; los arbustos no son como deberían ser, son tan espesos que no puedes ver nada a través de ellos, para ser honestos como las copas de los árboles..."


translate spanish day1_6482df8a:


    "Y los propios árboles...{w} El bosque estaba bastante alejado, pero los árboles parecían como si incluso hubieran cerrado sus filas esperando una orden para avanzar por los campos y las llanuras."


translate spanish day1_46d39e23:


    "Me quedé sin aliento y miré el autobús, el cual ahora apenas era visible."


translate spanish day1_d0f34e2d:


    th "Esa fue una buena carrera..."


translate spanish day1_5a5230cb:


    "Una vez más el miedo se apoderó de mi."


translate spanish day1_52a8e81e:


    th "Y aquellos tendidos eléctricos...{w} ¡Tiene que haber gente aquí!"


translate spanish day1_a53379f8:


    "¿Pero qué {i}significa{/i}?"


translate spanish day1_8d395e64:


    "De hecho, ¡a fin de cuentas tampoco significa nada!{w} ¿Incluso el infierno puede tener tendidos eléctricos?"


translate spanish day1_fbe4386c:


    "Prender y abrasar los pecadores es demasiado anticuado..."


translate spanish day1_c88e5ea3:


    "Debo haber alcanzado el punto de no retorno, después del cual da igual si pierdes completamente la cabeza como si finalmente tratas de entender qué está ocurriendo."


translate spanish day1_3cbef58c:


    th "Y mientras aun tengo la opción de escoger, ¡mejor debería escoger la segunda opción!"


translate spanish day1_28b9b20f:


    "Me dirigí lentamente de vuelta al autobús."


translate spanish day1_9aeca35d:


    th "Por supuesto, era aterrador."


translate spanish day1_d5a8e5e1:


    th "Pero es poco probable que encuentre una respuesta en los campos o en los bosques, y este miserable montón de tornillos es el único tipo de vínculo con el mundo real."


translate spanish day1_a20cefa7_2:


    "..."


translate spanish day1_001631c6:


    th "Debería explorar la zona cuidadosamente."


translate spanish day1_3e09185c:


    "Un muro de ladrillos y sus puertas coronadas con el letrero «Sovyonok»; estatuas de pioneros a sus lados; y cerca una señal de carretera indicando el número de la ruta del autobús: «410»."


translate spanish day1_59b146e5:


    me "Hoy el viaje está tardando un buen rato."


translate spanish day1_e98bfbde:


    "Hice una mueca de sonrisa."


translate spanish day1_0fa2263b:


    "Una persona puede comportarse inadecuadamente en situaciones extremas."


translate spanish day1_db78176c:


    th "Algo así me está ocurriendo a mi ahora."


translate spanish day1_95a9a169:


    "Este lugar no parecía abandonado del todo... las puertas no tenían óxido, ni los muros daño aparente."


translate spanish day1_3ba35159:


    me "«Sovyonok»..."


translate spanish day1_33b4e80e:


    th "¿Qué clase de nombre es ese?"


translate spanish day1_94f91cd7:


    th "A juzgar por las estatuas de los pioneros, podría ser un campamento de colonias para niños.{w} ¡Y este campamento parece estar en activo!"


translate spanish day1_deb4632a:


    th "Por supuesto, la más simple explicación, lógicamente, no explica nada en absoluto."


translate spanish day1_b987229b:


    th "La extraña muchacha, el autobús alterado, el verano, el campamento de pioneros..."


translate spanish day1_33930872:


    "Cientos de teorías recorrieron my mente instantáneamente: desde una abducción alienígena a un sueño de letargo, desde una alucinación a un viaje en el tiempo y el espacio..."


translate spanish day1_d28308d8:


    "Ninguna era peor que cualquier otra, pero realmente no había forma de escoger una sola."


translate spanish day1_c83682b4:


    "Entonces me acordé: ¡Puedo intentar hacer una llamada telefónica!"


translate spanish day1_30421d7d:


    "Cogí mi teléfono móvil y marqué el primer número de mi lista de contactos."


translate spanish day1_a5e58fe8:


    "Pero en vez de visualizarse la señal de cobertura, la pantalla estaba mostrando una gran cruz."


translate spanish day1_19146449:


    th "Claro, puede no haber señal en un lugar como éste tan alejado."


translate spanish day1_0d3882c7:


    th "¡Aun así no puedo ser el único que llegó aquí!"


translate spanish day1_c52f433f:


    th "¡Los autobuses no circulan solos!"


translate spanish day1_9191dff2:


    "Comprobé el autobús desde todas partes para asegurarme que no era una alucinación."


translate spanish day1_841adb1f:


    "Trozos de barro en la parte inferior, algo de óxido aquí y allá, pintura desvanecida y neumáticos desgastados: no, definitivamente este es un «Icarus» muy normal."


translate spanish day1_6109f212:


    th "Vale, exactamente el tipo de autobús que te lleva a lugares más allá de tu comprensión, si te duermes descuidadamente."


translate spanish day1_0343ddfb:


    "Hice una risita nerviosa."


translate spanish day1_d4028078:


    "Apareció por sí misma, esporádicamente.{w} Porque no era el lugar o momento correcto para reír..."


translate spanish day1_17403436:


    th "Pero, ¿dónde demonios está el conductor?"


translate spanish day1_7530ded5:


    "Cautelosamente me senté en el bordillo al lado del autobús y empecé a esperar."


translate spanish day1_a20cefa7_3:


    "..."


translate spanish day1_4bc2caf0:


    "Mi paciencia no duró mucho."


translate spanish day1_ede35d71:


    "Mi ansiedad parecía haber alcanzado el máximo, y empecé a volverme un poco histérico."


translate spanish day1_c8bc0b5a:


    "En este tipo de situación, probablemente cualquiera habría sentido algo similar."


translate spanish day1_0b02c10b:


    "Alienígenas y mundos paralelos se fueron de mi imaginación, dejando solamente vacío y tinieblas."


translate spanish day1_33b8346c:


    th "¿Es así como acabará todo? ¿Así terminará {i}mi vida{/i}?"


translate spanish day1_0818ff10:


    th "Pero quería hacer muchas más cosas, tantas para las cuales no tenía tiempo todavía..."


translate spanish day1_405f9fbf:


    "Se apoderó de mi una idea: esto era definitivamente el fin."


translate spanish day1_97d1733b:


    th "¡¿Pero por qué?!"


translate spanish day1_6e7e69ad:


    th "¡No es justo! ¡Seguramente no soy peor que otros!"


translate spanish day1_a72fb0ee:


    th "Dios, ¿por qué...?"


translate spanish day1_69065e88:


    "Lágrimas brotaban de mis ojos sin poder contenerlas, me hice un ovillo y empecé a rodar por la hierba."


translate spanish day1_4629954b:


    me "¡¿POR QUÉ?! ¡¿QUE HE HECHO?! ¡¿POR QUÉ YO?!"


translate spanish day1_6f37277c:


    "Pero mis clamores sólo fueron escuchados por las estatuas mudas de los pioneros y por un pájaro en un árbol, el cual inmediatamente alzó el vuelo con sus alas y se fue, graznando en su propia lengua, como si estuviera riéndose del idiota que osó interrumpirle la siesta después de comer."


translate spanish day1_893c0214:


    "Al final me quedé sin aliento debido a mi llanto, y permanecí quieto tranquilamente, sollozando de vez en cuando."


translate spanish day1_a20cefa7_4:


    "..."


translate spanish day1_1b527b73:


    "Después de un rato, me las arreglé para reponerme de mi estado."


translate spanish day1_659aa794:


    "Con la mente un poco despejada, como si el terror y el miedo a la muerte me hubieran dado un pequeño respiro."


translate spanish day1_dba992b6:


    th "Después de todo, si alguien quisiera matarme, ¿qué sentido tiene todo esto?"


translate spanish day1_1a34ee7c:


    th "No parece un experimento tampoco."


translate spanish day1_56b2c610:


    th "Si esto es alguna sorprendente coincidencia entonces probablemente no comporta amenaza."


translate spanish day1_cc407c1e:


    th "De todas formas, por ahora, parece no haber peligro."


translate spanish day1_35abcf62:


    "El pánico se fue pronto."


translate spanish day1_bbe3feaf:


    "Por supuesto, la sangre todavía martilleaba en mi cabeza y mis manos estaban temblando aun, pero al menos ahora podía pensar con claridad."


translate spanish day1_007b1ddb:


    th "Ahora mismo, ¡no hay nada que realmente pueda cambiar de todas maneras!{w} Por lo tanto, no importa cuanto piense o me enfade, sólo hará que las cosas empeoren."


translate spanish day1_607957e4:


    th "No tiene sentido hacer suposiciones hasta que no tenga algunos hechos veraces."


translate spanish day1_cea13f7e:


    th "En cualquier caso, no aprenderé nada descansando aquí."


translate spanish day1_f68fbb50:


    "Este campamento (si es que realmente es un campamento) parece el único lugar en el que la gente podría estar, por lo que decidí caminar hacia allí y apenas alcancé las puertas, cuando..."


translate spanish day1_cb43b355:


    "Una muchacha apareció por detrás de ellas..."


translate spanish day1_c9771ed6:


    "...vistiendo un uniforme de pionera."


translate spanish day1_76dc3c1c:


    th "Mi razonamiento no me abandonaba esta vez."


translate spanish day1_c454f340:


    th "Por lo que nuevamente, un uniforme de pionero en pleno siglo XXI..."


translate spanish day1_55a53f71:


    th "Y además, muchachas {i}aquí{/i}..."


translate spanish day1_a0dd748b:


    "Estaba de piedra, incapaz de dar un paso."


translate spanish day1_888fb514:


    "Sentí una enorme necesidad de salir corriendo.{w} Corriendo tan lejos como pudiera de este lugar, lejos de ese autobús, las puertas, las estatuas y lejos de ese maldito pájaro y su siesta."


translate spanish day1_1a518774:


    "Tan sólo correr, libre como el viento, más y más rápido, saludando a los planetas con los que me cruzara y guiñando el ojo a las galaxias."


translate spanish day1_8abc0b99:


    "Corriendo, para convertirme en un rayo púlsar y volverme en un vestigio de radiación, corriendo hacia lo desconocido."


translate spanish day1_b53ca9d4:


    "Corriendo sin importar donde, ¡en tanto que esté {b}lejos{/b} de este lugar!"


translate spanish day1_d158f7e2:


    "Mientras tanto, la muchacha se acercó y sonrió."


translate spanish day1_3fbcaafd:


    "No pude evitar darme cuenta de su belleza, incluso aunque estuviera tembloroso de miedo."


translate spanish day1_01e37be4:


    "Los instintos humanos funcionan a pesar de la falta de consciencia, y aunque sólo el 5%% del cerebro es responsable de los procesos cognitivos, el 95%% restante están siempre ocupados manteniendo {i}la vida{/i}, y en particular, garantizando el funcionamiento estable del sistema hormonal."


translate spanish day1_b619432f:


    "Desesperadamente quise ser menos complicado y parar de pensar en las citas formales de la Enciclopedia, aunque mis pensamientos surgieran de uno en uno, resultando ser estúpido, fuera de lugar, como si fuera tomado de un monólogo interno de un héroe de alguna novela negra de tapa blanda para aficionados."


translate spanish day1_159ede02:


    "Un precioso rostro eslavo, unas largas trenzas como si fueran dos brazos brillantes de heno y unos ojos azules en los cuales te puedes sumergir."


translate spanish day1_ebdc09cc:


    slp "Hola, ¿acabas de llegar ahora?"


translate spanish day1_a009dbf2:


    "Me quedé de piedra del asombro."


translate spanish day1_b5aec161:


    "La muchacha parecía perfectamente normal, incluso tenía apariencia humana, pero todavía no podía pronunciar una sola palabra."


translate spanish day1_fff6f9eb:


    th "Es demasiado tarde para correr..."


translate spanish day1_880e4e10:


    slp "¿Es que me he equivocado?"


translate spanish day1_06176d1f:


    "Me costó mucho esfuerzo dar una respuesta:"


translate spanish day1_13317d7b:


    me "Ehm... claro..."


translate spanish day1_c44cb8e0:


    slp "¿Qué?"


translate spanish day1_d51bc1bf:


    me "Oh, nada, quiero decir {i}sólo acabo de llegar{/i}."


translate spanish day1_c0a65fae:


    "Respondí rápidamente, temiendo que fuera yo el que hubiese dicho algo de más que fuera erróneo."


translate spanish day1_13317d7b_1:


    me "Ehm... claro..."


translate spanish day1_3c13f21e:


    slp "Muy bien pues, ¡bienvenido!"


translate spanish day1_6d77584d:


    "Ella hizo una amplia sonrisa."


translate spanish day1_82d0a78d:


    "Extraño, parecía como si tuviera frente a mi sólo a una muchacha normal."


translate spanish day1_31e75c3d:


    th "¡Bah! No debería haber vuelto aquí, los bosques y los campos parecían mejores..."


translate spanish day1_c2c61eee:


    th "Pero qué voy hacer a continuación... intentar hablar con ella como si fuera humana o alejarme corriendo, ¿o da igual?"


translate spanish day1_85e4f294:


    "La sangre bombeaba de manera incontrolable en mi cabeza, destrozándola desde el interior; un poquito más, y la pobre muchacha pionera se salpicaría del espantoso contenido de mi cráneo..."


translate spanish day1_26d0580a:


    slp "¿Qué te hace tanta gracia?"


translate spanish day1_5b1e985b:


    "La muchacha me miraba fijamente."


translate spanish day1_632c64ff:


    "Me hizo sentir escalofríos que recorrían mi espalda, y mis rodillas empezaron a temblar."


translate spanish day1_fa942807:


    me "N... nada..."


translate spanish day1_81fa1f0f:


    slp "Pues fantástico."


translate spanish day1_35ff2f5f:


    th "¿Fantástico? ¿Qué hay de fantástico en esto?"


translate spanish day1_9d000438:


    "De repente me sentí como si dejara todo atrás: el autobús detrás de mí, el invierno de ayer y el verano de hoy, quitarme el picante jersey y tan sólo creer que todo esto era real, que todo estaba destinado a ser y que, en definitiva, era para mejor..."


translate spanish day1_1ab1c37a:


    me "Ya deberías saberlo probablemente..."


translate spanish day1_d0ebceb7:


    slp "Deberías ir a ver a nuestra líder de campamento, ¡ella te lo explicará todo!"


translate spanish day1_3399b38f:


    slp "Mira.{w} Ve recto hasta la plaza, después gira a la izquierda. Verás pequeñas cabañas."


translate spanish day1_f40c35be:


    "Ella me señaló en dirección a las puertas, como si supiera qué había detrás de ellas."


translate spanish day1_54f0a60f:


    slp "Bueno, puedes preguntarle a cualquiera dónde está la cabaña de Olga Dmitrievna."


translate spanish day1_17993887:


    me "Yo... ehmm..."


translate spanish day1_81c28b1c:


    slp "¿Lo cogiste?"


translate spanish day1_a1e6972e:


    "Por supuesto, no lo hice."


translate spanish day1_2f0e0a31:


    slp "Me tengo que ir."


translate spanish day1_77cc4ba5:


    "La muchacha me saludó con su mano y desapareció detrás de las puertas."


translate spanish day1_bc447943:


    "Pareció como si para ella yo fuera...{w} ordinario."


translate spanish day1_07ef85cb:


    "Y todo este espectáculo con el autobús y los viajes tras despertar o cuando dormía, solo me afectaba a mi, mientras aquí todo estaba justo como se supone que es."


translate spanish day1_2d3f952e:


    th "Líder del campamento, uniformes de pionera..."


translate spanish day1_cf1c4a81:


    th "¿Están haciendo una reconstrucción histórica aquí?"


translate spanish day1_bce4a681:


    th "Espero no encontrarme a Lenin encima de un vehículo blindado en esta plaza."


translate spanish day1_1b36af68:


    th "Pero incluso eso no me sorprendería ahora mismo..."


translate spanish day1_39014c31:


    "Después de permanecer solo un rato, me dirigí dentro del campamento."


translate spanish day1_6f92edab:


    "A poco más de 50 metros delante de mí, apareció a la izquierda un pequeño edificio de una planta.{w} La cartelera cerca de la puerta decía «Clubs»."


translate spanish day1_b20957b2:


    "Iba a acercarme..."


translate spanish day1_93faf713:


    "Cuando la puerta se abrió repentinamente y una muchacha bajita, vistiendo uniforme de pionera, apareció."


translate spanish day1_bc2ffd42:


    "Su hermosa cara me dejó la impresión de estar portando una aflicción por el destino de toda la humanidad con una verdadera melancolía universal."


translate spanish day1_5b8bfb48:


    "Tan pronto como ella me vio, la muchacha se quedó de piedra, como si se asustara."


translate spanish day1_82b60add:


    "Yo también me quedé de piedra, considerando qué era lo mejor a hacer... acercarme primero o esperar hasta que ella mostrara algo de iniciativa.{w} O quizá salir corriendo a fin de cuentas..."


translate spanish day1_d6de2681:


    "Aunque ésta última opción era sugerida constantemente sólo por mi instinto de supervivencia (al menos eso es lo que quiero creer)."


translate spanish day1_2cebe5d7:


    "No el peor instinto humano, sino uno más bien lejos de ser el más racional."


translate spanish day1_63473f65:


    "Si este instinto jugara al póker contra habilidades deductivas, el resultado estaría predeterminado."


translate spanish day1_a971bc7a:


    "Y aquellas habilidades deductivas (o al menos similares) me estuvieran indicando que no había necesidad alguna de estar preocupado por esa muchacha."


translate spanish day1_9b858553:


    "De repente alguien saltó desde los arbustos cercanos."


translate spanish day1_b978b533:


    "Una muchacha vistiendo una brillante camiseta roja con las letras «URSS» escritas en ella."


translate spanish day1_7ebe0cfb:


    th "Una reproducción exacta de su época."


translate spanish day1_42c9a02c:


    "Ella parecía bastante bajita desde la distancia y probablemente era más joven que ambas muchachas pioneras... la de las puertas de la entrada y esta otra muchacha frente a la puerta de los «Clubs»."


translate spanish day1_c52afe92:


    "Al fin decidí acercarme, pero la «URSS» (como la llamé en mi cabeza) saltó hacia la primera muchacha y empezó a explicar alguna cosa mientras atravesó el aire."


translate spanish day1_b63ce3ba:


    "La otra muchacha, a su vez, parecía confusa, miró hacia abajo y no hizo ninguna respuesta."


translate spanish day1_d44e3c53:


    "Probablemente hubiera continuado observando su divertida {i}conversa{/i}, pero repentinamente la «URSS» sacó algo de su bolsillo y empezó a moverlo frente a la cara de la primera muchacha."


translate spanish day1_258e8119:


    "Esa {i}cosa{/i} resultó ser un saltamontes."


translate spanish day1_ed6083c2:


    unp "Aaaaahhhhhhhhhhhhhhh!"


translate spanish day1_87779d4a:


    "La primera muchacha chilló."


translate spanish day1_f91880c6:


    "No debe ser muy entusiasta de los insectos, ya que salió corriendo instantáneamente hacia donde presumiblemente Lenin hizo su discurso sobre la revolución consumada de los proletarios y campesinos."


translate spanish day1_03448d77:


    "Es decir... hacia la plaza..."


translate spanish day1_ebc4605c:


    "La «URSS» me observó, sonrió juguetona y salió pitando detrás de la otra."


translate spanish day1_38e92acb:


    th "No empieza mal el día."


translate spanish day1_1c924fd9:


    th "No tengo ni una pista de dónde estoy, a parte de esto, hay algunos críos por aquí jugando a ser pioneros."


translate spanish day1_3f084620:


    th "Y por lo que puedo decir, este lugar está situado a miles de kilómetros de mi hogar.{w} Incluso podría ser una realidad distinta."


translate spanish day1_b8844662:


    "Y ésta era ciertamente una {i}realidad{/i}..."


translate spanish day1_bd1b7173:


    "Quiero decir, todo lo que me rodeaba parecía tan real (quizás un tanto adornado) que empecé a considerar de que, de hecho, mi vida previa podría haber sido solamente un sueño."


translate spanish day1_947b07b9:


    th "¿Y qué debería hacer ahora?"


translate spanish day1_a20cefa7_5:


    "..."


translate spanish day1_a48ce857:


    "Estaba resiguiendo las grietas de las baldosas, las cuales pavimentaban el camino, de manera que me quedé sin rumbo en el edificio de los «Clubs»."


translate spanish day1_52f502cd:


    th "Tan solamente unos pocos segundos antes de que hubiese decidido tomar una decisión."


translate spanish day1_6f416834:


    "Fue cuando me acordé de mi mismo rodando por la hierba, sollozando..."


translate spanish day1_487a7790:


    "Me encogí de disgusto."


translate spanish day1_34312b93:


    "Quizás es otro instinto, cuando toda la energía se usa para lloriquear y autocompadecerse, una de dos: o bien el cuerpo se queda en hibernación, o mobiliza sus reservas."


translate spanish day1_8ff0ae14:


    "El mío pareció haber escogido la segunda opción, porque sin más, tuve esta determinación para figurarme qué era lo que estaba pasando."


translate spanish day1_6aea12ff:


    "Y con el fin de hacer esto, tuve que actuar como un hombre... como humano: tratar de mantener la dignidad como un representante de {i}mi propio mundo{/i}."


translate spanish day1_267d5adc:


    "Seguí el camino por el que se alzaban pequeñas cabañas hacia la izquierda y hacia la derecha, aparentemente casas de los pioneros de aquí."


translate spanish day1_772090f6:


    "En realidad, se veían bastante acogedoras desde fuera."


translate spanish day1_5ff36d96:


    "Incluso aunque nací en la Unión Soviética, nunca he sido parte de sus organizaciones infantiles, ni de los Pioneros ni tampoco de la juvenil Pequeños de Octubre."


translate spanish day1_d869f630:


    "Me imaginé la vida diaria de un típico campamento de pioneros un poco diferente: grandes barracones con largas hileras de literas de metal, toques de llamada a las seis en punto reproducidas por una sirena, un minuto para hacer tu cama y entonces unirte a la formación en la respectiva plaza..."


translate spanish day1_3a1335b5:


    th "¡Espera!{w} ¿Podría ser que lo esté confundiendo con otra cosa...?"


translate spanish day1_9eee971e:


    "¡De repente fui golpeado por la espalda!"


translate spanish day1_bb359f8d:


    "Me tambaleé pero permanecí en mi lugar, me di la vuelta y me preparé para ser un héroe mientras luchaba por mi vida..."


translate spanish day1_57666fa8:


    "Pero todo lo que encontré fue otra muchacha de pie frente a mi."


translate spanish day1_ee51804d:


    "Me quedé boquiabierto de sorpresa."


translate spanish day1_68b22888:


    dvp "Anda recoge tu boca del suelo."


translate spanish day1_143f62c0:


    "Cerré mi boca."


translate spanish day1_c1b4c8b4:


    "El mismo uniforme de pionera pero parecía, digámoslo así, provocativo en la forma en que ella lo vestía."


translate spanish day1_f689de87:


    "Como todas las muchachas que antes me he encontrado aquí, ésta era bastante guapa, pero su actitud excesivamente arrogante mataba cualquier deseo de conocerla mejor."


translate spanish day1_5ecab483:


    dvp "Así que... ¿Eres nuevo por aquí?"


translate spanish day1_51149e68:


    me "..."


translate spanish day1_dc8deb21:


    dvp "Bueno, ¡ya nos veremos!"


translate spanish day1_df07b66c:


    "Me lanzó una mirada amenazadora y me pasó de largo."


translate spanish day1_be998354:


    "Esperé que la muchacha pionera se volviera en la esquina...{w} ¡Quién sabe qué podría haber llegado hacer!"


translate spanish day1_310f2718:


    "La cosa más interesante era que incluso esta muchacha hostil me pareció completamente normal, no me dio la impresión de ser alguna amenaza de muerte."


translate spanish day1_dbdad37c:


    "Salvo quizás el peligro de recibir algún puñetazo en la nariz."


translate spanish day1_78546616:


    "Al fin, me las arreglé para llegar a la plaza."


translate spanish day1_79587a2a:


    "No había ningún Lenin en un vehículo blindado, sin embargo uno realmente podría esperar alguna cosa parecida después de todo lo que había sucedido."


translate spanish day1_b624fcdd:


    "En su lugar, a pesar de ello, un monumento dedicado a cierto camarada se alzaba en medio de la plaza. Las letras del pedestal se leían como «GENDA»."


translate spanish day1_4f2ef086:


    th "Debe ser alguna importante figura del Partido."


translate spanish day1_2f3a0b1e:


    "Habían algunos pequeños bancos a los lados."


translate spanish day1_f324eeda:


    th "Aquí se está bastante cómodo."


translate spanish day1_9d30be31:


    th "¿Dónde me dijo la muchacha que me dirigiera?{w} ¿A la izquierda o la derecha?"


translate spanish day1_c6de1218:


    th "A la izquierda, a la derecha, a la izquierda, a la derecha..."


translate spanish day1_58ada7be:


    th "¿Y para qué voy a ir allí de todas formas...?"


translate spanish day1_bd699a39:


    th "Ah, claro, decidí que pretendería ser normal..."


translate spanish day1_833eb5f3:


    th "Pues, ¡a la derecha!"


translate spanish day1_4870b408:


    "A través de una pequeña arboleda..."


translate spanish day1_be1bac02:


    "Caminé hasta un embarcadero."


translate spanish day1_66caa1a6:


    th "Debo haber tomado la dirección incorrecta."


translate spanish day1_a02d94f5:


    slp "¡Oh, camino incorrecto!"


translate spanish day1_0a8fd9cf:


    "Me giré hacia la voz."


translate spanish day1_ea065f04:


    "Esta primera muchacha estaba delante de mí."


translate spanish day1_4725a971:


    slp "Te dije que giraras hacia la izquierda desde la plaza y... ¿Adónde has terminado?"


translate spanish day1_59abc400:


    "Ella se había cambiado el uniforme de pionera por un bikini."


translate spanish day1_e85f6b79:


    slp "¡Oh, disculpa que no me haya presentado!{w} ¡Mi nombre es Slavya!"


translate spanish day1_2fa68520:


    slp "En realidad, mi nombre completo es Slavyana, pero todo el mundo me llama Slavya.{w} ¡También puedes hacerlo tú!"


translate spanish day1_6e37a835:


    me "Ehm... sep..."


translate spanish day1_0cc735df:


    "Todavía me siento un poco confuso, por lo que puede que no tenga más respuestas significativas."


translate spanish day1_4c5321be:


    sl "¿Y cuál es tu nombre?"


translate spanish day1_c5787aa3:


    "Me sentí como si ella pudiera ver a través de mi."


translate spanish day1_5cf26729:


    me "Ehm... Yo... sep... Semyon..."


translate spanish day1_e2552123:


    sl "Encantada de conocerte, Semyon."


translate spanish day1_a89521fc:


    sl "Bueno, ya he acabado aquí.{w} ¿Podrías esperar aquí un minuto? Me voy a cambiar y vamos a ver a Olga Dmitrievna juntos, ¿de acuerdo?"


translate spanish day1_65bf82fc:


    me "De acuerdo..."


translate spanish day1_43560fad:


    "Tras estas palabras ella salió corriendo y yo me senté en el embarcadero, dejando que mi pies colgaran en el agua."


translate spanish day1_90942777:


    "Vestía unas botas de invierno, pero en semejante clima, no había nada de malo en dejar que mis pies se humedecieran."


translate spanish day1_5c4ff44b:


    "Es más, me los refrescaba un poco."


translate spanish day1_ea94e5d1:


    "Contemplando el río, comencé a reflexionar pensando todo lo que había ocurrido."


translate spanish day1_24d93bb8:


    th "Si esto es algún tipo de conspiración, es una de las extrañas, incluso demasiado amistosa."


translate spanish day1_5edac208:


    th "No, más bien parece un suceso aleatorio."


translate spanish day1_044eac6b:


    th "Algún suceso aleatorio enteramente incomprensible."


translate spanish day1_a733d584:


    sl "¿Vamos?"


translate spanish day1_3d8e6458:


    "Slavya estaba junto a mi lado, vestida con el uniforme de pionera otra vez."


translate spanish day1_85ccdc6d:


    me "Vayámonos..."


translate spanish day1_1ea0d875:


    th "He estado aquí durante muy poco tiempo, pero de toda la gente que me he encontrado, ella resulta ser la menos sospechosa."


translate spanish day1_efe66139:


    th "Sin embargo, ¡este hecho es por sí mismo sospechoso!"


translate spanish day1_b3e9120f:


    "Caminamos hacia la plaza."


translate spanish day1_73d6913a:


    "La muchacha «URSS» y la muchacha que me golpeó en la espalda cuando estaba allí, se perseguían la una a la otra."


translate spanish day1_805a8066:


    th "¿Es algún tipo de juego al que están jugando?"


translate spanish day1_6c5b8d8e:


    sl "Ulyana, ¡basta de correr! ¡Se lo voy a contar todo a Olga Dmitrievna!"


translate spanish day1_e2f0dae9:


    us "¡Sip, capitán!"


translate spanish day1_0bfd5bdf:


    "Decidí no preguntar a Slavya durante un rato sobre todo lo que estaba ocurriendo y acerca de los lugareños."


translate spanish day1_5280f6b1:


    th "Lo mejor es que primero encuentre esa misteriosa Olga Dmtrievna."


translate spanish day1_1cd246d8:


    th "Parece que ella es la jefa aquí."


translate spanish day1_3337c244:


    "Estábamos caminando atravesando hileras de cabañas casi idénticas, algunas de las cuales parecían como grandes barriles de cerveza, mientras que otras parecían más bien como casetas familiares."


translate spanish day1_598416c3:


    "Finalmente, Slavya se paró frente a una pequeña cabaña de un piso."


translate spanish day1_4ce282c2:


    "Se parecía a un cuadro de un artista: la pintura desgastada, con esquirlas del paso del tiempo allí y allá, centelleaba en el sol; las persianas de la ventana, ligeramente entreabiertas, se balanceaban por el viento casi imperceptiblemente; y grandes arbustos de lilas crecían a sus costados."


translate spanish day1_9ba44673:


    "Parecía como si esta destartalada choza estuviera ahogándose en la tormenta del terciopelo púrpura, y las lilas, como una fuerza elemental, estuvieran inexorablemente destruyendo la casa de la líder de las tropas."


translate spanish day1_3dc6042e:


    sl "¿A qué estas esperando? ¡Vamos!"


translate spanish day1_cc0015de:


    "Slavya me sacó de mis ensueños."


translate spanish day1_25a8e409:


    mt_voice "...y deja de burlarte de Lena ya..."


translate spanish day1_23aa893b:


    "¡¿Rena?!"


translate spanish day1_368e2a32:


    th "Parece que hay alguien ahí dentro."


translate spanish day1_42c5f640:


    "Ciertamente, un momento después la puerta se abrió, y Ulyana salió corriendo atravesándola rápidamente con la misma maliciosa sonrisa."


translate spanish day1_9b6e58bf:


    "La muchacha con coletas fue la siguiente en salir."


translate spanish day1_01fca3c6:


    sl "¡Lena, no te molestes con ella!"


translate spanish day1_0d446d18:


    th "Así que su nombre es Lena...{w} Tengo que agradecer que no sea Rena, ¡por lo menos!"


translate spanish day1_92afc216:


    un "No lo haré..."


translate spanish day1_a3f0580d:


    "No terminó su frase, ruborizada se dirigió rápidamente hacia la plaza."


translate spanish day1_a653101d:


    "Por alguna razón sentí querer girarme y seguirla con mis ojos, pero Slavya dijo:"


translate spanish day1_84aaac50:


    sl "Ven."


translate spanish day1_9e8e7cd3:


    "Entramos en la cabaña."


translate spanish day1_4a8e191a:


    "Dentro de ella vi que era similar a lo que me estaba imaginando: dos camas, una mesa, un par de sillas, una alfombra sencilla en el suelo y un armario."


translate spanish day1_c87f01c5:


    "Nada en especial, pero al mismo tiempo se sentía hogareña y acogedora, aunque esta habitación estaba casi en el mismo estado de desorden que mi propio piso."


translate spanish day1_f9471bdb:


    "Una muchacha que aparentaba alrededor de los veinticinco años estaba de pie cerca de la ventana."


translate spanish day1_1529b84e:


    th "La naturaleza la dotó con un hermoso rostro y un buen cuerpo..."


translate spanish day1_0aa01509:


    th "Al menos hay una cosa que puede mantenerte feliz en este pandemonio: las lugareñas son bellas."


translate spanish day1_80b7fab7:


    mtp "¡Finalmente has llegado aquí!{w} ¡Excelente!{w} Mi nombre es Olga Dmitrievna, soy la líder del campamento."


translate spanish day1_81f67e23:


    me "¡Encantado de conocerte, soy Semyon."


translate spanish day1_2a6869f0:


    "Decidí hablar como si no estuviera sorprendido por todo lo que estaba ocurriendo."


translate spanish day1_b93477a9:


    "Ella se me acercó."


translate spanish day1_9a812b52:


    mt "Te hemos estado esperando desde buena mañana."


translate spanish day1_b7364cea:


    me "¿Me habéis estado esperando? ¿A mi...?"


translate spanish day1_ca77693f:


    mt "¡Claro, por supuesto!"


translate spanish day1_dda106bc:


    me "Y cuándo vendrá el próximo autobús, porque yo..."


translate spanish day1_fec151d0:


    mt "¿Para qué lo necesitas?"


translate spanish day1_8f1ac2ed:


    th "Vale, claro, por qué iba a necesitarlo..."


translate spanish day1_5a1280bd:


    th "Supongo que debería no hacer preguntas directas: la gente de aquí puede reaccionar a ellas bastante mal respecto a lo que desearía."


translate spanish day1_79c3f7b2:


    th "Y dudo que obtenga cualquier respuesta."


translate spanish day1_8d50c9cf:


    me "Por nada, sólo curiosidad..."


translate spanish day1_1725efc3:


    me "Por otra parte, ¿dónde estamos exactamente?{w} Quiero decir... ¿Cuál es nuestra dirección de correo?"


translate spanish day1_21adf436:


    me "Quisiera enviar una carta a mis padres para explicarles que estoy bien aquí."


translate spanish day1_efb897d9:


    "Por alguna razón, tuve la desesperada idea de que si me las arreglaba solo, encontraría algo ahí fuera."


translate spanish day1_5a58e925:


    mt "¡Oh, pero si tus padres hicieron una llamada hace tan sólo media hora! Te dieron recuerdos."


translate spanish day1_41180a2e:


    th "Esto sí que es una sorpresa..."


translate spanish day1_350d74d5:


    me "Pues, ¿puedo llamarles? Porque me olvidé de contarles algo antes de partir."


translate spanish day1_44f6bf31:


    mt "No."


translate spanish day1_9d48f5b9:


    "Me dirigió una dulce y natural sonrisa."


translate spanish day1_9bddf507:


    me "¿Por qué no?"


translate spanish day1_c40fd752:


    mt "No tenemos un teléfono aquí."


translate spanish day1_51149e68_1:


    me "..."


translate spanish day1_729f518a:


    me "Entonces, ¿cómo hicieron mis padres una llamada aquí?"


translate spanish day1_120ecdbc:


    mt "Acabo de venir del distrito central de la ciudad, es allí donde hablé con ellos."


translate spanish day1_434cc4a4:


    th "Ah, ¿es así como fue?"


translate spanish day1_68be55cd:


    me "¿Y puedo ir de alguna manera a la ciudad?"


translate spanish day1_44f6bf31_1:


    mt "No."


translate spanish day1_89d38d04:


    "Ella seguía manteniendo la misma sonrisa."


translate spanish day1_5ac38cf1:


    me "¿Por qué?"


translate spanish day1_06cd3c2d:


    mt "Porque el siguiente autobús sólo viene dentro de una semana."


translate spanish day1_a3268632:


    "Decidí no interrogarla sobre cómo se lo hizo para ir y volver: no obtendría respuesta de todas formas."


translate spanish day1_bd84df53:


    "En todo este tiempo, Slavya estaba junto a mi lado y parecía no encontrar nada raro en nuestra conversación."


translate spanish day1_869d68e4:


    mt "¡Oh, tenemos que buscar un uniforme para ti!"


translate spanish day1_75c6d686:


    th "¡No tengo absolutamente ningún deseo de ponerme los pantalones cortos de pionero o de vestir el ridículo pañuelo rojo!"


translate spanish day1_94a1d364:


    th "Sin embargo, vestir ropa de invierno en verano tampoco es que sea una buena idea."


translate spanish day1_f4df3367:


    me "Está bien, gracias..."


translate spanish day1_00ec5ab7:


    th "Me pregunto si soy el único aquí que encuentra extraño que alguien vista chaqueta y botas de invierno con este calor."


translate spanish day1_13e0bfb3:


    mt "Perfecto, estaré fuera pues, ¡además puedes ir familiarizándote con el campamento!{w} ¡No olvides venir a cenar por la noche!"


translate spanish day1_8af1d414:


    "Habiendo dicho esto, salió fuera de la cabaña caminando."


translate spanish day1_3be70f85:


    "No, «caminando» no es la palabra correcta: ella salió corriendo."


translate spanish day1_733c1098:


    "Estaba ahora solo con Slavya."


translate spanish day1_4c0d1dce:


    sl "Debo irme también, tengo cosas que hacer."


translate spanish day1_0a470c4f:


    sl "Date un paseo, echa un vistazo por el campamento.{w} Nos vemos por la noche."


translate spanish day1_d47d9534:


    th "Si ahí no había trampa o amenaza, entonces esta realidad, en la persona de Slavya, se volvía cada vez más algo de lo que sentir cariño."


translate spanish day1_a20cefa7_6:


    "..."


translate spanish day1_a28a58fe:


    "Por primera vez hoy me di cuenta de que aquí se estaba excesivamente caliente."


translate spanish day1_3aa2905a:


    "Aunque, obviamente, mi ropa era la responsable."


translate spanish day1_6224b6ad:


    "Me quité mi abrigo y lo solté encima de la cama, mi jersey le siguió después: ahora solamente vestía la camisa."


translate spanish day1_1302b288:


    th "¡Así se está mucho mejor!"


translate spanish day1_0650308f:


    "Todo lo que podía hacer ahora era seguir el consejo y echar un vistazo por el campamento."


translate spanish day1_faf01b13:


    th "Intentaré hallar alguna respuesta mientras tanto."


translate spanish day1_abe359aa:


    "Cruzando por la «zona residencial» local, vi un individuo pionero viniendo hacia mi."


translate spanish day1_19166f7e:


    "Y realmente era un individuo pionero, no una muchacha pionera. Aparentemente aquí había hombres incluso en este reino de Amazonas."


translate spanish day1_6e392819:


    elp "Hola, eres nuevo aquí, debes ser Semyon, ¿verdad?"


translate spanish day1_f4234049:


    me "Y cómo sabes tú..."


translate spanish day1_eb3ac837:


    elp "¡Todo el mundo ya lo sabe! Por otra parte, soy Electronik. El auténtico. Puedes llamarme así."


translate spanish day1_1ab8b0e8:


    th "Electronik. El auténtico. Las cosas estaban yendo de castaño claro a castaño oscuro."


translate spanish day1_f2e007f0:


    me "Muy bien..."


translate spanish day1_13475f83:


    el "Ulyana también me llama Cheese."


translate spanish day1_f005cc88:


    me "¿Cheese? ¿Queso?"


translate spanish day1_dd47efcd:


    th "¿Por qué no coliflor?"


translate spanish day1_8966f24f:


    el "Porque mi apellido es Cheesekov."


translate spanish day1_f2e007f0_1:


    me "Ya veo..."


translate spanish day1_ab73c55e:


    el "¡Deja que te enseñe todo!"


translate spanish day1_c45870d5:


    "Acepté su oferta, ya que habría sido más difícil conocer este lugar por mi cuenta."


translate spanish day1_901746cb:


    me "Vale, vamos."


translate spanish day1_e555f27e:


    "Volvimos a la plaza una vez más."


translate spanish day1_6bd8e03f:


    th "Como si no hubieran más lugares a los que ir en este campamento..."


translate spanish day1_ac68c5e6:


    "Lena estaba sentada en uno de los bancos, leyendo algún libro.{w} Electronik con confianza se acercó a ella."


translate spanish day1_03a7db73:


    el "¡Hola, Lena! Conocí al tipo nuevo, Semyon."


translate spanish day1_ebd7111d:


    "Comenzó enérgicamente."


translate spanish day1_f37aca17:


    me "Hola.{w} Bueno, se podría decir que ya nos conocimos."


translate spanish day1_31b01f67:


    un "Sí..."


translate spanish day1_51f9c6e8:


    "Ella apartó la mirada del libro por un instante, se fijó en mi, se sonrojó y volvió a la lectura, como si no notara nuestra presencia aquí."


translate spanish day1_0e6973ff:


    el "Muy bien, vamos a seguir."


translate spanish day1_e71595cb:


    "Al principio estaba sorprendido de que para «conocer» a esta muchacha todo se redujera a un puñado de palabras, pero entonces pensé que era mejor así.{w} La actitud vigorosa de Electronik no encajaba bien con la timidez de Lena."


translate spanish day1_304c9144:


    me "Vamos."


translate spanish day1_d2c497e2:


    "Luego llegamos a un edificio, el cual instantáneamente identifiqué como una cantina."


translate spanish day1_ac073a3b:


    el "Y aquí..."


translate spanish day1_e84fdadf:


    me "¡Lo sé! ¡Aquí es donde consumes alimentos orgánicos!"


translate spanish day1_06114188:


    el "Sep, algo así..."


translate spanish day1_4eab803f:


    "Allí, en el porche de la cantina, se encontraba la muchacha antipática que me golpeó en la espalda hace poco."


translate spanish day1_2cd75b17:


    "Al verla, mi buen humor se desvaneció en un parpadeo."


translate spanish day1_2eef73b2:


    th "De verdad, ahora no es el mejor momento de tomarle el pelo, incluso aunque sea bastante hilarante."


translate spanish day1_88b653a6:


    th "Primero necesito averiguar qué es lo que ocurre aquí, ¡o al menos dónde estoy!"


translate spanish day1_84bfeb9f:


    el "Ésa, la de ahí, es Alisa Dvachevskaya. Sé cauteloso con ella."


translate spanish day1_36dc9fcc:


    "Estaba hablando en susurros."


translate spanish day1_c065e234:


    el "No te arriesgues nunca a llamarla DvaCheh, ¡a ella no le gusta eso!"


translate spanish day1_242c3a55:


    dv "¿Qué dijiste? ¿Cómo me llamaste?"


translate spanish day1_c941236c:


    "Ella debió escucharle."


translate spanish day1_7cbd6d0d:


    "En un parpadeo, Alisa saltó desde el porche y salió corriendo hacia nosotros."


translate spanish day1_7810c210:


    el "Muy bien, a partir de aquí te las arreglas solo..."


translate spanish day1_8a6dfaab:


    "Electronik salió pitando."


translate spanish day1_68ae01db:


    "Decidí que no tenía deseo de cruzarme otra vez con esta muchacha agresiva, Alisa, y corrí a toda velocidad detrás de Electronik."


translate spanish day1_87908dda:


    "Corriendo hacia la plaza, lo perdí de vista."


translate spanish day1_f5ecd77a:


    "Pero Dvachevskaya tampoco me estaba persiguiendo."


translate spanish day1_efa7a730:


    th "Aun así, realmente no debería arriesgarme a llamarla de esa manera.{w} Incluso en mi cabeza..."


translate spanish day1_d436642f:


    "Después de recuperar mi aliento, me pregunté por qué había reaccionado así."


translate spanish day1_50af9809:


    th "Está bien, ella es una muchacha... una de las agresivas...{w} ¿Pero por qué correr?"


translate spanish day1_cd458724:


    "No hallando una buena respuesta, me senté en un banco y permanecí donde estaba el monumento a Genda."


translate spanish day1_2d7b339e:


    "Alisa, que me pasó corriendo, se paró por un momento y gruñió:"


translate spanish day1_1823fa4f:


    dv "¡Me ocuparé de ti más tarde!"


translate spanish day1_35feebce:


    me "¿Ocuparte de mi? ¿Qué hice mal?"


translate spanish day1_7eff5e99:


    "Añadí una forzada sonrisa de culpabilidad a mis palabras."


translate spanish day1_8fa3ff1e:


    th "Pero, ¿y de qué soy culpable...?"


translate spanish day1_9dbfd6f2:


    "Ella no me contestó y continuó persiguiendo a Electronik."


translate spanish day1_a20cefa7_7:


    "..."


translate spanish day1_eb85f5b2:


    th "Parece que tendré que matar el tiempo solo, esperando para la cena."


translate spanish day1_c1af6492:


    "Decidí ir hacia el este.{w} Por lo menos en la dirección en que el este debería haber estado en mi mundo."


translate spanish day1_990e43f3:


    "Poco después, me encontré cerca de un campo de fútbol."


translate spanish day1_55a5886e:


    "Aquí había un partido en todo su apogeo."


translate spanish day1_6e3f39b7:


    th "Supongo que verlo un rato no haría ningún daño."


translate spanish day1_80a62586:


    "En mi infancia y en la adolescencia no era un mal jugador e incluso pensé en ser profesional, pero unas cuantas lesiones seguidas acabaron con mi deseo por no querer arriesgar mi salud por el bien de una incierta posibilidad en un partido."


translate spanish day1_ac5ae99c:


    "Críos de diferentes edades estaban corriendo en el campo: pude ver a un muchacho de unos diez años y una muchacha de unos catorce años..."


translate spanish day1_f462954c:


    th "Una muchacha...{w} ¡Ey, esa es Ulyana!"


translate spanish day1_5e68b29c:


    th "Claro, está jugando a fútbol... ¿qué hay de sorprendente?"


translate spanish day1_1ae034aa:


    th "Después de todo, ella parece ser de las que son inquietas."


translate spanish day1_0286167f:


    "Me encontraba de pie bastante alejado del campo, aun así ella se percató de mi."


translate spanish day1_f85a8374:


    us "¡Ey, tú!"


translate spanish day1_77bd833d:


    "Me gritó Ulyana."


translate spanish day1_afc02b59:


    us "¿Quieres jugar?"


translate spanish day1_3f27c2c0:


    "No sabía qué responder."


translate spanish day1_7fb785ba:


    th "Por una parte, no es una gran idea estar corriendo durante diez minutos."


translate spanish day1_b2114442:


    th "Pero por la otra, cualquier paso en falso en mi situación podría ser el último."


translate spanish day1_be98ba5d:


    "Pero en cualquier caso, mi vestimenta no era la óptima para este tiempo."


translate spanish day1_0b1fdc5d:


    "Si jugara con botas de invierno y con los tejanos calurosos, sudaría como un cerdo."


translate spanish day1_16639bbb:


    th "Y jugar descalzo y sin tejanos sería simplemente poco ético."


translate spanish day1_91f30c65:


    me "Quizás en otro momento, ¿vale?"


translate spanish day1_1cdbc40f:


    "Grité en respuesta, me volví y me fui caminando."


translate spanish day1_317df2d8:


    "Detrás de mí Ulyana me gritó algo sobre mis pantalones, o sobre mi por ser un perdedor, o alguna cosa así..."


translate spanish day1_a20cefa7_8:


    "..."


translate spanish day1_4b9550bc:


    "La noche estaba cayendo, haciéndome sentir cansado y vacío después de un día perdido sin objetivo alguno."


translate spanish day1_a20cefa7_9:


    "..."


translate spanish day1_2d76a94d:


    "Volví a la plaza, me senté en un banco y solté un suspiro de cansancio."


translate spanish day1_c7f14958:


    th "Mejor me siento aquí y espero a la cena.{w} Después de todo, es fácil encontrar respuestas cuando no estás hambriento."


translate spanish day1_6face009:


    th "Darán comida a la gente de aquí... ¿verdad?"


translate spanish day1_6efc8181:


    th "Sabes, es curioso como la más simple necesidad humana puede doblegar la voluntad de reflexionar sobre las cosas, de luchar por algo."


translate spanish day1_8098381c:


    th "Por ejemplo, me siento hambriento ahora y me preocupa mucho menos dónde estoy o qué es lo que me está ocurriendo."


translate spanish day1_8e0062ff:


    th "¿Puede que a las personas más importantes también les afectara esto?"


translate spanish day1_11ca5f98:


    th "Y en ese caso, ¿cómo inició Espartaco el levantamiento de esclavos tiempo atrás...?"


translate spanish day1_6d0083af:


    th "Sólo puedo concluir que no soy una persona importante, y que realmente no importa en qué mecanismo sirvo como pieza: la sociedad, mátrix o un campamento de pioneros extraño."


translate spanish day1_31836b32:


    "Mis pensamientos fueron interrumpidos por el repicar de la campana de un altavoz en un poste de luz."


translate spanish day1_bbaa6f9b:


    th "¡Debe ser la hora de la cena!"


translate spanish day1_d1afa489:


    "Me dirigí hacia la cantina; era bueno saber dónde me encontraba."


translate spanish day1_60f3a365:


    "Olga Dmitrievna estaba allí, permaneciendo de pie en el porche."


translate spanish day1_3181ce0b:


    "Me paré y la miré de cerca, como si estuviera esperando algo."


translate spanish day1_620995d5:


    "Me miró por un momento, pero al final se me acercó."


translate spanish day1_e4e46775:


    mt "Semyon, ¿a qué estás esperando? ¡Entra ya!"


translate spanish day1_3db38509:


    th "Supongo que nada malo puede ocurrir si voy con ella."


translate spanish day1_8646eca1:


    "Mi estómago me animó a seguirla."


translate spanish day1_20d3b7df:


    "Ambos entramos dentro."


translate spanish day1_d5cccf64:


    "La cantina se parecía a...{w} a una cantina."


translate spanish day1_5a204f67:


    "Tuve una oportunidad de visitar una cantina de una fábrica en algún punto de mi vida... {w} Ésta era exactamente igual, sólo que un poco más limpia y más moderna."


translate spanish day1_7f269d4e:


    "Sillas y mesas de metal, azulejos esmaltados en las paredes y en el suelo, una vajilla poco sofisticada ocasionalmente con alguna grieta."


translate spanish day1_2b4a4a89:


    th "Supongo que así es como es una cantina en un campamento de pioneros."


translate spanish day1_865ed000:


    mt "Semyon, espera un momento, te encontraremos un lugar donde puedas sentarte..."


translate spanish day1_fed0ccad:


    "Ella miró alrededor del lugar."


translate spanish day1_cbc89c54:


    mt "Dvachevskaya, ¡quédate aquí!"


translate spanish day1_bccd2e3b:


    "Olga Dmitrievna gritó a Alisa quien pasaba por ahí."


translate spanish day1_e0ebb201:


    dv "¿Qué?"


translate spanish day1_686395a7:


    mt "¿Qué pasa con tu ropa?"


translate spanish day1_f893781b:


    dv "¿Es que hay algo de malo en ella?"


translate spanish day1_6c59a28a:


    "Claro, su vestimenta parecía algo provocativa..."


translate spanish day1_7f4c6c16:


    mt "¡Pon tu uniforme correctamente ahora mismo!"


translate spanish day1_aa5269ba:


    dv "Está bien, vale..."


translate spanish day1_02da0b4a:


    "Alisa puso su camisa correctamente y se fue caminando, lanzándome una mirada desagradable."


translate spanish day1_a1e6af5d:


    mt "Bueno, ¿dónde podemos encontrar un lugar donde puedas sentarte?"


translate spanish day1_30ea3e74:


    "No había muchas sillas libres."


translate spanish day1_986412cf:


    mt "Ve hacia allí, ¡con Ulyana!"


translate spanish day1_2cfe56a5:


    me "Ehm... Quizás yo..."


translate spanish day1_385a7396:


    mt "Que sí, venga, ¡la comida ya está en la mesa también!"


translate spanish day1_07918b6c:


    "No tenía otra opción que aceptar."


translate spanish day1_ff014804:


    "De acuerdo, existía la posibilidad de que las chuletas estuvieran envenenadas con curare, el puré de patatas sazonado generosamente con arsénico y el vaso lleno de excelente anticongelante en lugar de compota..."


translate spanish day1_1dcade22:


    "¡Pero todo parecía tan apetecible que no tuve opción de resistirme!"


translate spanish day1_d5d98923:


    us "¡Ey!"


translate spanish day1_087d8e36:


    me "¿Qué quieres?"


translate spanish day1_4b969ae9:


    "Respondí bastante groseramente a Ulyana, quien estaba sentada a mi lado."


translate spanish day1_bcac0437:


    us "¿Por qué no jugaste a fútbol con nosotros?"


translate spanish day1_ee0ece72:


    me "Por mis ropas."


translate spanish day1_ac6bee46:


    "Le dije, indicando el origen del problema."


translate spanish day1_762f9dc6:


    us "Oh, está bien pues, come."


translate spanish day1_71c21715:


    "Sin embargo, no había mucho más que comer... ¡¡¡La chuleta desapareció de mi plato!!!"


translate spanish day1_2623f571:


    th "Solamente ella podría haberlo hecho."


translate spanish day1_f9c55ec5:


    th "No, para ser más precisos, ¡nadie salvo Ulyana podría hacerlo!"


translate spanish day1_58e7978c:


    me "¡Devuélveme mi chuleta!"


translate spanish day1_0f31d85b:


    us "En una gran familia, si te duermes, ¡la pierdes!{w} ¡Te puede costar una chuleta si eres descuidado!"


translate spanish day1_ea7db8e5:


    me "¡Te digo que me la devuelvas!"


translate spanish day1_3e16ffc9:


    "La miré amenazadoramente y estaba a punto de llegar a las manos..."


translate spanish day1_39e0f78f:


    us "¡Lo ves, no la tengo!"


translate spanish day1_8fb93759:


    "Y ciertamente, el plato de Ulyana estaba vacío... parecía que esta pequeña muchacha comía tan rápido como le robaba las chuletas a alguien."


translate spanish day1_bf4d7f64:


    us "Tranquilo, ¡ahora vamos a hacer ejercicio fuera!"


translate spanish day1_c2895463:


    "Cogió mi plato y salió corriendo."


translate spanish day1_4c2a1209:


    "No tenía sentido seguirla: si quisieran evenenarme aquí, podrían haberlo hecho de una forma más fácil."


translate spanish day1_4bb06742:


    "Alrededor de un minuto más tarde, Ulyana volvió y me devolvió mi plato con una humeante y caliente chuleta en él."


translate spanish day1_39cd2cfa:


    us "¡Otra para el hambriento!"


translate spanish day1_73aab3cd:


    me "Gracias..."


translate spanish day1_dfe4386e:


    "Era todo lo que podía decir."


translate spanish day1_e425e9ee:


    "Estaba tan hambriento que mis sospechas se disiparon inmediatamente.{w} Pinché con mi tenedor la chuleta y..."


translate spanish day1_2158fc2f:


    th "¡¿Pero qué?!{w} ¡Una cucaracha!{w} No... ¡No es una cucaracha!{w} ¡Un insecto!{w} ¡Tiene patas y se está meneando!"


translate spanish day1_27e62654:


    "El plato cayó en el suelo y se rompió en pedazos; la silla me golpeó fuertemente en mi pierna mientras caía."


translate spanish day1_c1ea30fa:


    "Los insectos me desagradan desde que era un niño, pero cuando estos bichos horribles aparecen en mi plato, ¡eso ya es excesivo!"


translate spanish day1_670b85c3:


    me "Tú, pequeña..."


translate spanish day1_cdba60b2:


    "Ulyana parecía preparada para semejante cambio y ya estaba en la puerta, riéndose como si ella sólo hubiera escuchado una broma de una ocurrente comedia."


translate spanish day1_a0076c62:


    "Salí rápido detrás de ella."


translate spanish day1_4a7f4c33:


    "Salimos ambos corriendo afuera de la cantina."


translate spanish day1_51f77987:


    "Solamente estábamos a una docena de metros aparte, y sentí que podía cazar a esta pequeña muchacha fácilmente."


translate spanish day1_c0e0bceb:


    "Corrimos atravesando la plaza..."


translate spanish day1_8d3b0cc2:


    "Cruzando los edificios de los clubs..."


translate spanish day1_f7567198:


    "Y corrimos hasta adentrarnos en el camino del bosque."


translate spanish day1_d50859c6:


    "Comencé a jadear por mi aliento."


translate spanish day1_80f73680:


    th "Debería haber dejado de fumar supongo..."


translate spanish day1_1178684b:


    "Perdí de vista a Ulyana en la siguiente esquina."


translate spanish day1_b24a1f41:


    th "¡No puede ser cierto que se las haya ingeniado para escaparse de mi!"


translate spanish day1_c255b412:


    th "¡Simplemente no puede ser!"


translate spanish day1_7a8d7835:


    "Estaba de pie tratando de recuperar mi aliento otra vez."


translate spanish day1_a20cefa7_10:


    "..."


translate spanish day1_fd3eb8d2:


    "La noche estaba cayendo."


translate spanish day1_b570941e:


    th "Parece que estoy perdido..."


translate spanish day1_bce9bc24:


    th "Es una mala idea quedarme en los bosques por la noche; mejor volver al campamento."


translate spanish day1_bbe1aefc:


    "Aun así, no tenía ni idea de hacia dónde ir."


translate spanish day1_985a9376:


    th "Bueno, escogeré de forma aleatoria."


translate spanish day1_a20cefa7_11:


    "..."


translate spanish day1_a20cefa7_12:


    "..."


translate spanish day1_f8583ab1:


    "Me perdí por un rato en el bosque e incluso pensé en gritar por auxilio, pero finalmente vi la verja del campamento detrás de los árboles."


translate spanish day1_7ad8ae02:


    th "Todo estaba en su lugar."


translate spanish day1_7f4df294:


    me "El autobús se fue..."


translate spanish day1_6cfccad8:


    "Mascullé tranquilamente."


translate spanish day1_dbfa4d22:


    "Por un lado, no había nada de extraño en ello: el autobús no podía estar ahí solo por siempre."


translate spanish day1_42ed0653:


    "Y por otra parte, significa que alguien lo estaba conduciendo, ¡porque los autobuses no se conducen solos!"


translate spanish day1_dcc5269b:


    th "¿O lo hacen?"


translate spanish day1_ba1dfa38:


    "Este mundo parecía demasiado normal, pero cada suceso aquí tenía al menos dos explicaciones para ello: una ordinaria... real, la explicación de cada día... y otra surreal."


translate spanish day1_b678b61b:


    "Ciertamente, el conductor podría haber estado fuera por un descanso y me fui demasiado pronto, y por eso..."


translate spanish day1_ec1d1437:


    th "En cualquier caso, ¡éste no es lugar para mi!"


translate spanish day1_9aa1fa90:


    "Tanto si ese autobús se conducía por sí mismo como si no, era probablemente una cuestión importante, pero era mucho más importante cómo había llegado aquí de todos modos."


translate spanish day1_2b9ab305:


    "Y qué lugar era {i}éste{/i}..."


translate spanish day1_5aa31516:


    "Los campos y los bosques, extendiéndose hacia el horizonte, no tenían respuestas; no había nada de familiar en ellos."


translate spanish day1_a20cefa7_13:


    "..."


translate spanish day1_13255b43:


    "Un mundo extraño, raro y ajeno.{w} Y aun así, no había razón para estar aterrado a su vez."


translate spanish day1_c83e85c5:


    "Una de dos, o bien mi instinto de supervivencia decidió resignarse de su labor, o bien toda esta carrera por el campamento y con sus pioneros lugareños me han sosegado demasiado con su despreocupada normalidad, que simplemente me olvidé a su vez de lo que me había sucedido hace tan sólo un par de horas atrás."


translate spanish day1_e9c8c9dc:


    "Aunque probablemente ya no tenía fuerzas para preocuparme."


translate spanish day1_ab64ceb2:


    "Todo lo que quería era un poco de paz, de calma; sólo quería tener un respiro de todo ello, y sólo tras eso continuaría buscando respuestas, preparado y con energías."


translate spanish day1_c260705c:


    "Sin embargo, eso sería algo más tarde..."


translate spanish day1_5cfa0252:


    th "¿Y ahora qué? ¿Me puedo permitir relajarme?"


translate spanish day1_a20cefa7_14:


    "..."


translate spanish day1_b2e9fb9d:


    "Se había hecho completamente oscuro y en cualquier caso era mejor pasar la noche en el campamento."


translate spanish day1_46aa5cbc:


    "Estaba a punto de volver cuando alguien se me acercó silenciosamente desde atrás."


translate spanish day1_ac5eb104:


    sl "Hola, ¿qué estás haciendo aquí tan tarde?"


translate spanish day1_51149e68_2:


    me "..."


translate spanish day1_245b70c4:


    "Era Slavya que estaba detrás de mí.{w} Estaba tan sorprendido que me dio un susto."


translate spanish day1_ec2df0cc:


    sl "Así que no has cogido a Ulyana, ¿verdad?"


translate spanish day1_d1046a49:


    "Ella sonrió."


translate spanish day1_5e1fc6b8:


    "Asentí decepcionado y suspiré."


translate spanish day1_dd08f845:


    sl "No me extraña.{w} Nadie puede hacerlo."


translate spanish day1_476d94f1:


    th "Claro, ella es rápida como un cohete.{w} Podría haber encontrado mejor uso a su energía para buscarse aventuras..."


translate spanish day1_d1722b3c:


    sl "Debes estar hambriento, después de todo no has cenado..."


translate spanish day1_d0dae46d:


    "Por supuesto, había olvidado el hambre, pero tras estas palabras suyas, mis tripas sonaron traidoramente recordándome que les prestara atención."


translate spanish day1_c17a722d:


    "Slavya sonrió."


translate spanish day1_4cbb01a1:


    sl "Vamos pues."


translate spanish day1_e01dee92:


    me "¿Cómo, la cantina está aun abierta?"


translate spanish day1_40c17de2:


    sl "No pasa nada, tengo las llaves."


translate spanish day1_36955eb7:


    me "¿Las llaves?"


translate spanish day1_ab83b32e:


    sl "Sí, yo tengo las llaves de todas las instalaciones de este campamento."


translate spanish day1_9080c90e:


    me "¿Cómo es eso?"


translate spanish day1_ca97714d:


    sl "Bueno, soy algo así como la asistente de la líder del campamento de aquí."


translate spanish day1_7e4b1b8c:


    me "Ya veo.{w} Bueno, vamos."


translate spanish day1_63a3eb4c:


    "Era una de esas ofertas que no puedes rechazar."


translate spanish day1_00f9254f:


    "Cuando alcanzamos la plaza, Slavya se detuvo en seco."


translate spanish day1_7f5d338c:


    sl "Discúlpame, debería avisar a mi compañera de habitación que llegaré tarde... ella es tan puntual que si vuelvo tarde se preocupará."


translate spanish day1_518df0ed:


    sl "Ve hacia la cantina que yo iré en un minuto, ¿te parece?"


translate spanish day1_505d91ae:


    me "De acuerdo..."


translate spanish day1_32ac72cc:


    "Realmente no me esperaba que alguien estuviera allí a estas horas, aparte de mi."


translate spanish day1_b6cef34a:


    "Y ese alguien estaba al parecer tratando de abrir la puerta desesperadamente."


translate spanish day1_18f36ecf:


    "Sin ningún tipo de escrúpulo personal, caminé hacia el porche."


translate spanish day1_d706aa5c:


    "Quien movia el pomo de la puerta resultó ser Alisa."


translate spanish day1_e81739e5:


    th "Probablemente debería haberme mantenido alejado y esperar... "


translate spanish day1_bbe1ef0f:


    "Ella me miró atentamente durante un rato, luego dijo:"


translate spanish day1_a872d369:


    dv "No te quedes ahí parado, ¡échame una mano o algo!"


translate spanish day1_e54b39e9:


    me "¿Qué quieres decir?"


translate spanish day1_9736714c:


    dv "¡Qué me ayudes a abrir la puerta!"


translate spanish day1_7332342b:


    me "¿Por qué?"


translate spanish day1_c67591a7:


    dv "¡Porque quiero algunos bollos con kéfir! ¡La cena no fue suficiente!"


translate spanish day1_8d3f7a19:


    me "Ehmmm...{w} ¿Realmente es una buena idea?"


translate spanish day1_1381f7ca:


    dv "¿No estás hambriento tú, eh?{w} Ulyana no te dejó cenar tranquilamente, ¿verdad?"


translate spanish day1_f396e777:


    "Ella sonrió sarcásticamente."


translate spanish day1_ad73a68c:


    th "Es cierto, no lo hizo..."


translate spanish day1_c43ed392:


    me "Está bien, Slavya vendrá ahora y..."


translate spanish day1_c8b0074b:


    dv "¡¿QUÉ?!"


translate spanish day1_ca8046cd:


    th "Creo que no debía haberle dicho eso."


translate spanish day1_e7b745f9:


    dv "¡Pues me largo!"


translate spanish day1_5f47ed77:


    dv "¡Y pagarás por esto!{w} ¡Ya me debes dos!"


translate spanish day1_a180b25b:


    "Habiendo dicho esto, Alisa desapareció en la noche."


translate spanish day1_efb5548d:


    th "¿Y a qué se debe la primera?"


translate spanish day1_a20cefa7_15:


    "..."


translate spanish day1_696c481c:


    "Slavya no me hizo esperar demasiado."


translate spanish day1_8c608cb1:


    sl "¿Va todo bien?"


translate spanish day1_d5cf1edd:


    me "Claro, ¿por qué lo preguntas?"


translate spanish day1_18938c76:


    sl "Por nada, es igual."


translate spanish day1_dc129c74:


    "Era mejor que no le explicara nada sobre Alisa."


translate spanish day1_b5544ae3:


    me "Todo va bien."


translate spanish day1_944a6c8e:


    "Dije eso e instantáneamente escuché un rasgo de deshonestidad en mi voz."


translate spanish day1_6d7067b9:


    sl "Bueno, ¿vamos ya?"


translate spanish day1_3792d81a:


    "Parece que Slavya no se dio cuenta de nada.{w} O al menos ella lo estaba disimulando muy bien."


translate spanish day1_44f2c4ea:


    "Entramos en la cantina."


translate spanish day1_c8f556b6:


    sl "Espera un poco, voy a coger algo."


translate spanish day1_d09b3412:


    "Me senté en una silla y obedientemente esperé a mi salvadora."


translate spanish day1_8775f75b:


    "Mi cena era sencilla: unos pocos bollos y un vaso de kéfir."


translate spanish day1_c6c983c8:


    th "No me extraña... apuesto a que los hambrientos pioneros terminaron con todo."


translate spanish day1_37e90611:


    "Sin embargo, incluso esto era mucho mejor que mi dieta habitual."


translate spanish day1_d5866868:


    "Slavya se sentó frente a la mesa y me miró mientras estaba comiendo."


translate spanish day1_fbb73cfa:


    me "¿Acaso tengo algo en la cara?"


translate spanish day1_178aa33f:


    sl "No, sólo que..."


translate spanish day1_d1046a49_1:


    "Ella sonrió."


translate spanish day1_dc9c1b81:


    sl "Qué... ¿cómo te lo has pasado hoy en tu primer día en el capamento?"


translate spanish day1_6a05d51d:


    me "Bueno, no lo sé realmente..."


translate spanish day1_b12cf159:


    "Es absurdo preguntarle a alguien que repentinamente se encontró a sí mismo en una realidad diferente, tanto si le gustaba la comida de la cantina, la líder del campamento o la cabaña que le tocaba."


translate spanish day1_46db5ea6:


    sl "Ya verás, ¡pronto te acostumbrarás!"


translate spanish day1_68e00554:


    "Slavya permaneció soñadora mirando en la ventana."


translate spanish day1_b05e0e4e:


    th "Francamente, no tengo ningún deseo de acostumbrarme a todo esto; pero ella en realidad no lo sabe..."


translate spanish day1_b9ffb640:


    th "O al menos eso es lo que ella quiere hacerme creer."


translate spanish day1_2c50ec15:


    me "Bueno, de todos modos, es agradable estar aquí."


translate spanish day1_443c7288:


    "Tenía que romper el incómodo silencio de alguna forma."


translate spanish day1_17d13602:


    sl "¿En qué piensas?"


translate spanish day1_a271f10e:


    "Me preguntó sin interés."


translate spanish day1_1b986acc:


    me "Claro. Este lugar es tan..."


translate spanish day1_d4bc890b:


    "Quise decir «retro», pero me las arreglé para no decirlo."


translate spanish day1_edbbb7b2:


    "Después de todo, era retro para mi, ¿pero para ellos?{w} Pudiera ser que fuera el único tipo de vida que conocieran."


translate spanish day1_78a8ca85:


    "Si el término {i}vida{/i} era aplicable aquí después de todo..."


translate spanish day1_789035fa:


    sl "¿Entonces qué?"


translate spanish day1_00577787:


    "Me miró fijamente.{w} Como si algo importante dependiera de mi respuesta."


translate spanish day1_7c2d154d:


    me "Bueno, no lo sé... precioso. ¡Sí! Es precioso estar aquí."


translate spanish day1_c1307b0b:


    sl "Supongo que tienes razón."


translate spanish day1_78a2fe3f:


    "Ella volvió a sonreír."


translate spanish day1_e349590f:


    sl "Está muy bien que lo pienses."


translate spanish day1_5ac38cf1_1:


    me "¿Por qué?"


translate spanish day1_17e99afb:


    sl "Bueno, no todo el mundo le gusta estar aquí..."


translate spanish day1_1a377d44:


    me "¿Y tú?"


translate spanish day1_1995267f:


    sl "¿Yo?"


translate spanish day1_fc30fd2b:


    me "Sí."


translate spanish day1_031c751c:


    sl "Me encanta estar aquí, es genial."


translate spanish day1_5c30029f:


    me "Entonces no tienes por qué preocuparte de lo que los demás piensen."


translate spanish day1_879638a9:


    sl "Bueno, en verdad tampoco me preocupo."


translate spanish day1_c954d69f:


    "Slavya se rio."


translate spanish day1_72c6e2b1:


    "Esta conversación parecía estar dirigiéndome por mal camino hacia donde quería llegar."


translate spanish day1_1a712a21:


    sl "Y tú estás preocupado de ti mismo..."


translate spanish day1_1d09f697:


    me "¿En serio? ¿Por qué?"


translate spanish day1_8b1f703e:


    sl "Bueno, cuando alguien está masticando con tanta intensidad..."


translate spanish day1_07129b8a:


    me "Lo siento."


translate spanish day1_705bea28:


    sl "No pasa nada."


translate spanish day1_02cdc4c3:


    "No podía esforzarme por ser más cuidadoso con esta muchacha."


translate spanish day1_ded79e77:


    "¿Pero por qué exactamente ella? Con cualquier lugareña podría serlo."


translate spanish day1_e53b428c:


    "Cualquiera de ellos parecía completamente normal para mi.{w} Precisamente {i}normal{/i}, ¡tan normal que da escalofríos recorriendo toda la espalda!"


translate spanish day1_51e2809e:


    "Normal, no como un vecino humano que hace ruido con un taladro en una mano y un subwoofer en la otra."


translate spanish day1_ab3da39e:


    "No como un pasajero humano de los que puedes encontrar a menudo en el metro o en el transporte público."


translate spanish day1_15d26ee0:


    "No como un colega de trabajo humano al lado de tu escritorio en la oficina."


translate spanish day1_29ee93d2:


    "Y tampoco como un amigo humano que sólo se diferencia de otros por su constante insistencia."


translate spanish day1_82012fff:


    "Todos ellos parecen {b}normales{/b}... como se esperaría de ellos... con sus propios inconvenientes pero sin ninguna clase de superpoderes."


translate spanish day1_c821ace7:


    "Y Slavya también era...{w} ¿preciosa?"


translate spanish day1_7f68049d:


    "La miré fijamente a hurtadillas sin saber qué decir."


translate spanish day1_1ec8deab:


    sl "Lo siento, quise enseñarte el campamento pero me tuve que ir."


translate spanish day1_567b2330:


    me "Yo... creo no me perdí nada importante mientras estuve por mi cuenta."


translate spanish day1_2e601a49:


    sl "¿Estás seguro de que no te has perdido nada?"


translate spanish day1_efd47648:


    "Ella sonrió tanto que tuve que cerrar mis ojos de vergüenza, confundido."


translate spanish day1_cdbd4a1c:


    me "Bueno, cómo podria saberlo... es mi primer día que estoy aquí."


translate spanish day1_122de170:


    sl "Vale, ¿y qué viste hasta ahora?"


translate spanish day1_875830b6:


    me "He visto la plaza, la cantina, el campo de fútbol..."


translate spanish day1_76985cc0:


    sl "¿Y la playa?"


translate spanish day1_b11fae54:


    me "Solamente desde lejos."


translate spanish day1_f5ac888c:


    sl "¡De verdad que deberías ir allí! ¡O vamos juntos!"


translate spanish day1_d5688542:


    me "Claro, vale... Iremos..."


translate spanish day1_446ce20c:


    "Su naturalidad empezó a asustarme, pero entonces pensé... ¿Qué pasa si todo lo que ocurre aquí es como se supone que debe ser y este mundo sólo me parece extraño a mi pero para ellos es...{w} el auténtico?"


translate spanish day1_6578880f:


    "¿Tal vez he sido enviado al pasado...?"


translate spanish day1_5410f15c:


    "Sí, eso explicaría muchas cosas."


translate spanish day1_aad73e6b:


    me "¿Puedo hacerte una pregunta tonta?"


translate spanish day1_fadcaef0:


    sl "No."


translate spanish day1_7b5c12bd:


    "Slavya sonrió y se levantó de la mesa."


translate spanish day1_c5c451d5:


    sl "Es tarde... ¿Encontrarás tu camino hacia Olga Dmitrievna por ti mismo?"


translate spanish day1_9fef43a7:


    me "Por supuesto que lo haré pero... ¿por qué debería ir allí?"


translate spanish day1_821bd937:


    sl "Ella te dará cobijo en algún lugar."


translate spanish day1_7332342b_1:


    me "¿Para qué?"


translate spanish day1_bfddcddb:


    "Probablemente esta pregunta pareció estúpida porque Slavya echó una gran carcajada."


translate spanish day1_c070ca19:


    sl "Necesitas dormir en algún lugar, ¿cierto?"


translate spanish day1_4405a6cf:


    me "Eso tiene sentido..."


translate spanish day1_2455141e:


    sl "Está bien, me voy pues.{w} ¡Buenas noches!"


translate spanish day1_0a0db17f:


    me "Igualmente..."


translate spanish day1_ca407fc5:


    th "Es extraño que se fuera con tanta prisa..."


translate spanish day1_c6b742a3:


    "Una llave que se habia dejado encajada en el pomo llamó mi atención."


translate spanish day1_08ccb5b7:


    "Iba a alcanzar a Slavya, ¿pero dónde vive?"


translate spanish day1_3aa2ad8d:


    "E ir llamando a todas las puertas durante media noche no parecía una brillante idea."


translate spanish day1_9356c1f1:


    th "Mejor la cogeré... mañana se la devolveré porque quién sabe qué es lo que ocurre aquí por la noche."


translate spanish day1_56811613:


    "Dichos pensamientos me dieron un respiro... soy yo quien en primer lugar necesita ir con mucho cuidado."


translate spanish day1_5802aa2b:


    th "Por otra parte para qué la necesito..."


translate spanish day1_0eb5a1f8:


    "La noche, a pesar de ser oscura no estaba silenciosa del todo... uno podía escuchar grillar a los grillos, las canciones de los pájaros nocturnos y el crujir de los árboles en todas partes."


translate spanish day1_ee63d9fd:


    "Me vino el repentino deseo de seguir el consejo de Slavya e ir hacia la cabaña de la líder del campamento."


translate spanish day1_b358634e:


    "No sé por qué, pero la mirada de la construcción de bronce desconocida del comunismo me puso de buen humor."


translate spanish day1_e66f685d:


    "Me senté en el banco y empecé a repasar todo lo que me había ocurrido hoy.{w} Era lo mejor que podía ofrecerme mi buen humor."


translate spanish day1_2719b81b:


    "Aquí se estaba más animado que cerca de la cantina, unos pioneros que se demoraban estaban corriendo, por lo que éste lugar no parecía aterrador para nada."


translate spanish day1_2dc0eb74:


    th "El autobús, el capamento de verano, las muchachas..."


translate spanish day1_3aff8100:


    "Estaba cansado de todo lo nuevo y extraño que podía no tener una sola explicación de lo que estaba sucediendo."


translate spanish day1_4caea1ce:


    "Escuché vagamente un crujido cercano."


translate spanish day1_f2d26be1:


    "Me estremecí y miré en esa dirección."


translate spanish day1_6993b49f:


    th "Una muchacha.{w} Leyendo un libro."


translate spanish day1_2807e1fc:


    th "Lena."


translate spanish day1_1bdc01a9:


    "Decidí acercarme a ella y hablarle."


translate spanish day1_7ccf917f:


    "Ella era la única persona nueva que conocí aquí con la que no tenía apenas palabras para conversar."


translate spanish day1_7b241855:


    me "Hola, ¿qué estás leyendo?"


translate spanish day1_ac772bf2:


    "Lena estaba sorprendida, tanto que incluso dio un brinco."


translate spanish day1_ea263cee:


    me "Perdona, ¡no quería asustarte!"


translate spanish day1_62592cb0:


    un "No importa..."


translate spanish day1_ad42b83a:


    "Se ruborizó y miró fijamente su libro otra vez."


translate spanish day1_c43d3dfb:


    me "¿Y qué estás leyendo?"


translate spanish day1_f3297c42:


    "En la portada ponía: «Lo que el viento se llevó»."


translate spanish day1_53009119:


    me "Buen libro..."


translate spanish day1_626232a6:


    un "Gracias."


translate spanish day1_87f26837:


    th "Hablando sinceramente, no lo he leído pero creo que dicha literatura encaja muy bien con ella."


translate spanish day1_21b3e47f:


    "Lena parecía no estar interesada en continuar con la conversación."


translate spanish day1_f85b2b40:


    me "Bueno, si te estoy molestando..."


translate spanish day1_049b021f:


    un "No."


translate spanish day1_62091b7c:


    "Me respondió mientras todavía miraba el libro."


translate spanish day1_13ee5d36:


    me "¿Puedo sentarme a tu lado por un rato?"


translate spanish day1_41cfffd5:


    un "¿Por qué?"


translate spanish day1_e92b6e6e:


    th "¿De verdad... por qué...?"


translate spanish day1_a0c74c5a:


    "Tal vez porque tan solamente estaba muy cansado y estar en compañía es mejor que estar solo."


translate spanish day1_bf12dc37:


    "Y quizá quería averiguar algo de ella."


translate spanish day1_0864712b:


    "Con mucho cuidado observé a Lena."


translate spanish day1_07aeaaac:


    th "Pero no, dudo que..."


translate spanish day1_3102ef10:


    me "Bueno, no sé... ¿No me está permitido?"


translate spanish day1_3b683375:


    un "Siéntete libre..."


translate spanish day1_d783747c:


    me "Pero si te estoy molestando..."


translate spanish day1_ab09f879:


    un "No, no lo haces."


translate spanish day1_858edfda:


    me "Puedo irme, sólo dímelo..."


translate spanish day1_0d42a600:


    un "No pasa nada, está bien."


translate spanish day1_f517812c:


    me "Vale, pues..."


translate spanish day1_98f71c0d:


    "Me senté al borde del banco cuidadosamente."


translate spanish day1_0fcded02:


    "Después de tan intensa conversación estar aquí era la última cosa que quería, pero no sería amable por mi parte sólo levantarme e irme."


translate spanish day1_61ec364d:


    me "No fue muy bien, ¿eh?"


translate spanish day1_cf8bd28b:


    "Lena no respondió nada."


translate spanish day1_8caec5df:


    "Parece que hice el idiota."


translate spanish day1_60dd740c:


    "Apuesto que si estuviera Ulyana aquí se habría reído bastante de mi."


translate spanish day1_e1927099:


    me "¿Disfrutas estando aquí?"


translate spanish day1_610b2212:


    "Recordé la pregunta de Slavya y pensé que sería un buen comienzo para una conversación."


translate spanish day1_f1be5fed:


    un "Sí."


translate spanish day1_783cf09d:


    "Ella sonrió suavemente."


translate spanish day1_23729c98:


    me "Supongo que también me gusta..."


translate spanish day1_b5a17bee:


    "Lena definitivamente no es muy sociable y probablemente no puede soportar una conversación sin sentido tan fácilmente como Slavya."


translate spanish day1_141ec821:


    "Pero había algo en ella que me atraía la atención."


translate spanish day1_f8e0c3e7:


    "Como una visión o reflejo momentáneos en tus gafas en una noche de otoño lluvioso que te hace dar la vuelta y permanecer en la oscuridad buscando alguna cosa, algo que viste por el rabillo de tu ojo."


translate spanish day1_04705c56:


    "Por supuesto, no conseguías distinguir o comprender lo qué era, pero se sentía tan tentador..."


translate spanish day1_9ee89722:


    "Lena todavía seguía leyendo el libro sin prestar atención a mi presencia."


translate spanish day1_62582949:


    "Y yo no tenía ninguna intención del todo de hacerle alguna pregunta sobre este campamento o sobre el mundo en general."


translate spanish day1_a1e73db2:


    me "Bonita noche..."


translate spanish day1_5ef3d7d4:


    un "Sí..."


translate spanish day1_76d86f28:


    th "¿Cómo demonios comenzaría una conversación con ella?"


translate spanish day1_69ef7bda:


    un "Es tarde, me tengo que ir..."


translate spanish day1_45544479:


    me "Sí, es bastante tarde..."


translate spanish day1_2629037f:


    un "Buenas noches."


translate spanish day1_fab13f1e:


    me "Igualmente..."


translate spanish day1_e5d71cd0:


    "Había algo extraño en esta muchacha."


translate spanish day1_482011b5:


    th "A primera vista ella era la típica muchacha pionera tímida y modesta, pero..."


translate spanish day1_efa2bb54:


    "El misterio de Lena tiene lugar entre una lista enorme de misterios de este campamento, los cuales comienzo a juntarlos ordenadamente en mi cabeza."


translate spanish day1_a20cefa7_16:


    "..."


translate spanish day1_047c6176:


    th "Noche relajada, no hay nada como un rato sin nada que hacer..."


translate spanish day1_8b243a91:


    "Me dirigí hacia la cabaña de Olga Dmitrievna."


translate spanish day1_76db5439:


    "La luz estaba encendida."


translate spanish day1_01c690bf:


    mt "¡Hola, Semyon!{w} ¡Llegas bastante tarde!"


translate spanish day1_ccd4c421:


    me "Sep...{w} Fui a dar una vuelta por el campamento."


translate spanish day1_6e6b09c8:


    mt "Bueno.{w} Dormirás aquí."


translate spanish day1_d0ba333c:


    "Ella señaló con su dedo a una de las camas."


translate spanish day1_54ddf89e:


    me "¿Aquí mismo...?"


translate spanish day1_07acf34e:


    "Estaba un poco sorprendido."


translate spanish day1_0cc846e0:


    mt "Claro, ¿qué ocurre algo?{w} Estamos sin cabañas libres de todas maneras."


translate spanish day1_e5c2c901:


    "La líder del campamento sonrió, pero creo que era más bien una sonrisa de cortesía."


translate spanish day1_e6f03e6e:


    mt "¿Es que no quieres ser un pionero decente?"


translate spanish day1_6d7c696f:


    "Había un claro énfasis en la palabra «decente»."


translate spanish day1_fa9f2c55:


    me "Sep... Por supuesto..."


translate spanish day1_c2a35634:


    "Me distraí en pensamientos por un momento."


translate spanish day1_8edc3e00:


    me "¿No le importa señorita?"


translate spanish day1_d642ee60:


    "Ella me miró extrañada.{w} Con sorpresa y algo ofendida en sus ojos."


translate spanish day1_a63ad949:


    mt "¡Los pioneros deberían respetar a los mayores!"


translate spanish day1_8492f5e5:


    "Dijo Olga Dmitrievna de forma estricta."


translate spanish day1_4adb9df1:


    me "Por supuesto deberían, nadie ha cuestionado tal cosa..."


translate spanish day1_343b3a4f:


    "Parloteé sin darme cuenta qué era lo que estaba mal."


translate spanish day1_1c5c9771:


    mt "¿No deberías tú también...?"


translate spanish day1_8fb93608:


    "Ella se me quedó mirando fijamente."


translate spanish day1_4b041b53:


    "Bajo esa mirada incluso el mejor mithril forjado por un maestro enano de las mazmorras más profundas se habría desecho."


translate spanish day1_8e8da635:


    me "¿Que debería qué?{w} ¿Vaya, qué pasa?"


translate spanish day1_50297304:


    "El fastidio y la falta de comprensión me hicieron hablar un poco fuerte."


translate spanish day1_20f9f9be:


    mt "¡Tienes que dirigirte a los adultos apropiadamente!"


translate spanish day1_0bdad8e7:


    th "Sí, por supuesto aquí hay muchas cosas raras...{w} Pero esta muchacha es tan solamente un par de años más mayor que yo..."


translate spanish day1_d2336ee2:


    th "O quizás incluso más joven..."


translate spanish day1_c447246d:


    "Pero decidí no discutir... mientras que sólo unos pocos minutos antes nunca le habría llamado adulta, tengo que admitir que también tiene un carácter bastante fuerte."


translate spanish day1_6a31f527:


    "Y en cualquier caso no estaba en posición de discutir."


translate spanish day1_28205106:


    me "Como diga... señora."


translate spanish day1_9fde8ff9:


    "Murmuré."


translate spanish day1_c0fad5bd:


    mt "¡Eso está mejor!{w} ¡Así es como un pionero decente debería comportarse!"


translate spanish day1_fc0afa91:


    mt "¡Y ahora es la hora de irse a dormir!"


translate spanish day1_cee3bfd9:


    "Hablando sinceramente no iba a convertirme en un pionero decente o en uno indecente."


translate spanish day1_06cf444d:


    "Ayer mismo no iba a convertirme en un pionero de todos modos..."


translate spanish day1_e62f6767:


    th "¿Pero tengo opción ahora?"


translate spanish day1_a3857c58:


    "«Sin deseo... te pondremos bajo el fuego»... este es el lema que Olga Dmitrievna probablemente iba a usar."


translate spanish day1_fe66fa24:


    "Fui hacia la cama y cerré mis ojos sólo para darme cuenta de lo cansado que estaba después de hoy."


translate spanish day1_80860151:


    "Algo martilleó terriblemente en mi cabeza como si mi cerebro empezara un turno de noche."


translate spanish day1_af68a167:


    "Y parecía que estaban más acostumbrados a trabajar con acero mecanizado que con piezas electrónicas."


translate spanish day1_95ae7d8f:


    "El autobús voló por mi cabeza..."


translate spanish day1_de155e12:


    "Y la plaza con el monumento..."


translate spanish day1_4d51c966:


    "La cantina, llena de pioneros...{w} Y una malvada cara de Ulyana."


translate spanish day1_169054b7:


    "Slavya..."


translate spanish day1_06f01a7c:


    "Lena..."


translate spanish day1_753d87f9:


    "E incluso recordar Alisa tampoco me dio mucha sensación negativa."


translate spanish day1_1de79ea9:


    th "¿Qué pasaría si estoy aquí por algo bueno...?"
# Decompiled by unrpyc: https://github.com/CensoredUsername/unrpyc
