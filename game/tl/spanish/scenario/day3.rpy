
translate spanish day3_main1_c980a67d:


    "La noche dejó tras de sí una sensación de ansiedad. Me desperté estando exhausto tanto física como mentalmente."


translate spanish day3_main1_d58e9a94:


    "Me ocurre siempre: ves imágenes increiblemente lúcidas en tu sueño, como si estuviera en una gran producción de cine Hollywoodiense con un guión lleno de giros, actores cotizados y efectos visuales multimillonarios. Y entonces despiertas por la mañana sin recordar absolutamente nada."


translate spanish day3_main1_5cd53aa6:


    "Eso es lo que trataba de hacer... recuperar las imágenes mostradas por mi relajada mente. Pero esa parte de mi memoria estaba vacía... una de dos o estaba archivada, o estaba formateada del todo."


translate spanish day3_main1_cc6f7f6e:


    "Eran las siete de la mañana, según el reloj."


translate spanish day3_main1_4b2cb0f9:


    th "Hoy parecía ser un pájaro madrugador."


translate spanish day3_main1_a9a851c3:


    "Olga Dmitrievna todavía estaba durmiendo, abrigándose estrechamente en su manta."


translate spanish day3_main1_2ee0953d:


    th "¿Abrigándose tan estrechamente...? ¿En un verano caluroso...? Mmmm..."


translate spanish day3_main1_de284251:


    "Me levanté, estirándome, y me acerqué al espejo para echarme un vistazo."


translate spanish day3_main1_eb6077b0:


    th "Mejor me afeitaba."


translate spanish day3_main1_832ce63c:


    "Qué idea más rara. Antes dos veces al mes era suficiente, pero ahora no era necesario en la forma en que me veía."


translate spanish day3_main1_4e0332f2:


    "Tampoco encontré una cuchilla de todas maneras."


translate spanish day3_main1_cbc809e5:


    th "Incluso, para qué la necesito... si tengo el aspecto de un adolescente de diecisiete años como mucho."


translate spanish day3_main1_eddcb947:


    th "Así...{w} El desayuno no puede empezar antes de las nueve de la mañana, y eso significa que tengo mucho tiempo."


translate spanish day3_main1_26c1194a:


    "Que puedo emplear en pasear y asearme, por ejemplo."


translate spanish day3_main1_4144add1:


    "El agua ya no parecía tan helada como el día anterior."


translate spanish day3_main1_289c3fa7:


    "Al contrario... su agradable frío me ayudó a animarme y a volver a la realidad (tanto como se le pudiera decir real)."


translate spanish day3_main1_399d144c:


    "Los polvos para dientes no parecían estar pasados de moda. Un rápido pensamiento me sobrecogió ya que no eran tan diferentes respecto a la pasta de dientes."


translate spanish day3_main1_300a064a:


    "Cuando retorné a la cabaña, Olga Dmitrievna ya estaba levantada."


translate spanish day3_main1_d6087460:


    "Ella estaba de pie frente al espejo peinándose el cabello."


translate spanish day3_main1_b8d13a2a:


    mt "¡Buenas mañanas, Semyon!"


translate spanish day3_main1_3773033f:


    me "Buenas."


translate spanish day3_main1_c0392c73:


    "Debido a la falta de sueño, mi cabeza estaba todavía canturreando, y los pensamientos estaban juntos todos revueltos. Sin embargo, no me arrepentí de levantarme en vez de dormir como habitualmente dos o tres horas más."


translate spanish day3_main1_52b8b2dd:


    mt "¿Cómo es que te levantaste tan pronto?"


translate spanish day3_main1_a3d19ed0:


    me "Pues sólo ocurrió..."


translate spanish day3_main1_3e828ab9:


    mt "Y sin el pañuelo una vez más. Permíteme..."


translate spanish day3_main1_e3b2ca3e:


    me "Lo haré yo mismo, no es difícil... de camino a la cantina."


translate spanish day3_main1_74f457d8:


    mt "No lo creo, ¡hazlo ahora!{w} ¿Te olvidaste de que hay que formar antes de desayunar?"


translate spanish day3_main1_3ac43883:


    th "¿Cómo podría olvidarlo?"


translate spanish day3_main1_a9616761:


    me "¿Y de qué va la formación hoy?"


translate spanish day3_main1_ac9273a5:


    mt "Discutiremos el orden del día."


translate spanish day3_main1_a3da2eed:


    me "¿Y cuál es el orden?"


translate spanish day3_main1_689ac639:


    "Hablando francamente, me trae sin cuidado {i}qué{/i} pudiera haber en el orden del día para nuestra valiente tropa de pioneros. Estaba mayormente interesado en {i}dónde{/i} estaría nuestra brava líder o nuestra pequeña armada de pioneros durante las siguientes 12-15 horas... quizá me las arreglaría para alejarme.{w}"


translate spanish day3_main1_045f8001:


    mt "¡Averigüemos eso en la formación!"


translate spanish day3_main1_945c59bc:


    "Ella me hizo una astuta sonrisa."


translate spanish day3_main1_fb73d398:


    me "Olga Dmitrievna, sabes, mi estómago se enojaría... mejor que..."


translate spanish day3_main1_6e70eda4:


    me "Bueno, ya sabes..."


translate spanish day3_main1_c0565634:


    me "Creo que está bien perderse la formación una vez. Todos cogerían asco si no me pudiera contener allí..."


translate spanish day3_main1_923b9fdb:


    th "¿De dónde salía todo ese coraje...?"


translate spanish day3_main1_063c7cf2:


    mt "Tienes cinco minutos más."


translate spanish day3_main1_95a2da76:


    me "Me preocupa si necesitaré más tiempo.{w} Podrías explicármelo ahora mismo, por favor."


translate spanish day3_main1_7ffb918a:


    me "Claro que intentaré que no sea así, pero sólo por si acaso me lo pierdo..."


translate spanish day3_main1_bdf2ab42:


    mt "¡Oh, está bien!{w} En nuestra lista para hoy está limpiar en ciertos lugares del campamento, organizar los libros de la biblioteca, y otras tareas menores. Y, por supuesto, tenemos una fiesta de baile por la noche."


translate spanish day3_main1_fa2ec241:


    mt "¡Ya lo verás tú mismo!"


translate spanish day3_main1_dc6aa38c:


    mt "¡Ahora debes tomar parte en las actividades del campamento!"


translate spanish day3_main1_585915f9:


    th "¡Apuesto que sí!"


translate spanish day3_main1_c2355535:


    me "¡Por supuesto! Y ahora mejor que yo..."


translate spanish day3_main1_41c48b54:


    mt "¡Vamos, empieza a irte!"


translate spanish day3_main1_2f2e2be3:


    "Salí fuera de la cabaña y me escondí detrás de los arbustos esperando que Olga Dmitrievna se fuera."


translate spanish day3_main1_8eee44c3:


    "Normalmente, no tenía una razón peculiar para comportarme de esta forma."


translate spanish day3_main1_740a8290:


    "Pero mi voz interior me estaba diciendo que no tenía nada que hacer en la formación."


translate spanish day3_main1_a20cefa7:


    "..."


translate spanish day3_main1_1466986b:


    "Después de que la líder del campamento se fuera, volví a la cabaña y decidí esperar para el desayuno ahí."


translate spanish day3_main1_24e4a223:


    th "Bueno, no tengo intención de tomar parte en la vida de la comunidad hoy."


translate spanish day3_main1_dfefa0e2:


    th "Limpiar y organizar... eso no era lo que alguien en mi situación debería estar haciendo."


translate spanish day3_main1_0ed74327:


    "Dos días han transcurrido, y aun estoy en el mismo lugar en el que comencé."


translate spanish day3_main1_a8e4db15:


    "Todos los inquilinos parecían ser perfectamente gente común. No estaban involucrados en ninguna conspiración, no se comunicaban con aliens y no tenían ni idea sobre viajes espaciales."


translate spanish day3_main1_e9ea2720:


    "Claro que cada uno de ellos era un poco particular. Pero estas peculiaridades no se salían tampoco de lo normal."


translate spanish day3_main1_e9429608:


    "Aun más, habiendo conocido cualquiera de estos pioneros en mi mundo habitual, no sospecharia nada raro."


translate spanish day3_main1_469fa83c:


    "Parecían ser incluso más normales y naturales que, digámoslo así, mi propio yo."


translate spanish day3_main1_32fd1d9f:


    "Esta forma de razonar era bastante lógica, la parte ilógica era la ausencia de respuestas."


translate spanish day3_main1_097dcaf5:


    th "Gente ordinaria en un lugar no ordinario."


translate spanish day3_main1_2f89955c:


    th "Eso ya lo he escuchado y visto, en alguna parte..."


translate spanish day3_main1_23beae7b:


    th "Docenas de pioneros (o incluso más) viven sus vidas habituales y hacen sus cosas del día a día. Ellos no entienden que el mundo que les rodea no es lo que parece ser."


translate spanish day3_main1_c2aba579:


    th "Vale.{w} Pero este es su punto de vista.{w} En cuanto a mi, no estoy familiarizado tanto con el mundo del campamento «Sovyonok» como con sus inquilinos. Incluso si parecieran comportarse como personas normales bajo las mismas circunstancias."


translate spanish day3_main1_83ea97da:


    th "Tengo que escoger aquí y ahora... ¿Está esa gente involucrada en lo que me está ocurriendo?"


translate spanish day3_main1_4427373a:


    th "¿Pueden darme alguna respuesta?"


translate spanish day3_main1_6d2752d6:


    "O tengo que buscar la verdad ahí fuera."


translate spanish day3_main1_a20cefa7_1:


    "..."


translate spanish day3_main1_ffb0123f:


    "Me parece que estoy yendo demasiado lejos con mis ideas... no hay una multitud de pioneros hambrientos habitualmente cerca de la cantina."


translate spanish day3_main1_2fba02c8:


    "O incluso dentro de la cantina."


translate spanish day3_main1_fe31d110:


    "Probablemente, muchos de ellos hayan tenido su desayuno antes de las nueve de la mañana."


translate spanish day3_main1_378c20a9:


    th "Eso es mejor aun. Dos es compañía; tres es multitud."


translate spanish day3_main1_51afaa36:


    "Lena estaba sentada en el rincón más alejado de la cantina, perezosamente pinchando con su tenedor una masa amorfa que se parecía vagamente a gachas de avena."


translate spanish day3_main1_1fcd8de0:


    "Desayunar con ella parecía una buena idea. Podemos tener una conversa tranquila."


translate spanish day3_main1_33a88c19:


    th "O al menos intentar tener una."


translate spanish day3_main1_e70262c5:


    "Estaba de camino hacia su mesa cuando de repente alguien me agarró de la manga."


translate spanish day3_main1_d960d8bf:


    "Zhenya, la bibliotecaria."


translate spanish day3_main1_c2aade8d:


    mz "Coge tu comida y ven. Tenemos que hablar."


translate spanish day3_main1_51149e68:


    me "..."


translate spanish day3_main1_be6dc4ed:


    mz "¿A qué estás esperando?"


translate spanish day3_main1_0b5fe8db:


    "Estaba un poco confundido."


translate spanish day3_main1_1d988865:


    me "Perdona, no es un poco demasiado... ¿repentino?"


translate spanish day3_main1_7ff5889f:


    mz "¿Qué ocurre? Coge tu comida y siéntate."


translate spanish day3_main1_ab08d3e6:


    "Supongo que ella juzga su comportamiento como algo completamente normal."


translate spanish day3_breakfast_un_c5c4ca0e:


    "Estaba seguro de que a Lena no le importaría si me sentaba a su lado."


translate spanish day3_breakfast_un_20c17916:


    mz "¡Quieto! ¡Espera un segundo!"


translate spanish day3_breakfast_un_9a143b5c:


    me "Lo siento. Quizás, más tarde."


translate spanish day3_breakfast_un_1643039e:


    "Aparté educadamente mi manga de sus manos y me dirigí hacia el rincón más alejado de la cantina."


translate spanish day3_breakfast_un_a9a20700:


    "Zhenya estaba gritando algo en mi dirección, pero traté de ignorarla."


translate spanish day3_breakfast_un_b5bcbe39:


    me "¡Hola, buenos días!"


translate spanish day3_breakfast_un_5cbc2f90:


    "Habiendo escuchado a la bibliotecaria gritar, Lena estaba echándome miradas furtivas por un rato."


translate spanish day3_breakfast_un_480fc425:


    un "Buenas..."


translate spanish day3_breakfast_un_49f1d3e1:


    "Raramente por extraño que fuera, ella no se ruborizó, sino que en vez de ello sonrió."


translate spanish day3_breakfast_un_80a13296:


    me "¿Te importaría si me uno a ti? Me tomaré la comida y me iré en poco tiempo."


translate spanish day3_breakfast_un_3d4036d2:


    un "Sí, claro."


translate spanish day3_breakfast_un_a20cefa7:


    "..."


translate spanish day3_breakfast_un_ea39cb42:


    "En un minuto estaba ya sentado frente a ella con una selección sencilla de platos en la bandeja: gachas de avena, dos huevos escalfados, cuatro piezas de pan de trigo, una salchicha hervida, un vaso de compota, hecha de frutas y bayas desconocidas."


translate spanish day3_breakfast_un_9aeb930e:


    me "¡Buen provecho!"


translate spanish day3_breakfast_un_626232a6:


    un "Gracias."


translate spanish day3_breakfast_un_5a8d98a1:


    "Estaba tratando de comer tan pulcramente como pudiera... nada de mascar ruidosamente, ensuciarme de comida o tirarme la compota encima."


translate spanish day3_breakfast_un_7bb18871:


    th "Parecía que comer como un humano y no como un cerdo no era tan difícil después de todo."


translate spanish day3_breakfast_un_997be624:


    "Como de costumbre, Lena permanecía silenciosa, eso significaba que debería empezar una conversa."


translate spanish day3_breakfast_un_c7fca932:


    me "¿Irás al baile esta noche?"


translate spanish day3_breakfast_un_1aa04f80:


    un "Probablemente..."


translate spanish day3_breakfast_un_34d1d8f6:


    "Ella permanecía silenciosa otro rato."


translate spanish day3_breakfast_un_44be8128:


    un "¿Y tú?"


translate spanish day3_breakfast_un_799c0105:


    me "No lo sé aun..."


translate spanish day3_breakfast_un_911940f8:


    "Pensé que no quería ir allí de todas formas."


translate spanish day3_breakfast_un_cc5d1c46:


    "No me gustaban las fiestas de baile desde los tiempos en que iba a la escuela..."


translate spanish day3_breakfast_un_49e77139:


    un "¿Por qué?"


translate spanish day3_breakfast_un_3956a59c:


    me "¿Qué quieres decir?"


translate spanish day3_breakfast_un_121175cf:


    "Mi mente aun estaba muy lejos, en los tiempos en que iba a la escuela."


translate spanish day3_breakfast_un_d3f48c81:


    un "¿Por qué no quieres ir?"


translate spanish day3_breakfast_un_949e4fbd:


    me "No dije que no quiera ir..."


translate spanish day3_breakfast_un_da258ccf:


    un "Vale pues."


translate spanish day3_breakfast_un_19437dd5:


    me "Pero si me estás invitando..."


translate spanish day3_breakfast_un_153a3cb2:


    "Lena se enrojeció y apartó la mirada."


translate spanish day3_breakfast_un_d6e377c8:


    un "Ehmm, realmente yo no..."


translate spanish day3_breakfast_un_f61b93da:


    me "Disculpa, sólo bromeaba."


translate spanish day3_breakfast_un_aa3ce0f5:


    "Me sentía confundido. Era mejor no {i}bromear{/i} de esa forma."


translate spanish day3_breakfast_un_b993fafe:


    un "Está bien."


translate spanish day3_breakfast_un_3198fcbf:


    "Dijo indiferentemente Lena y se concentró en su comida, mientras su rostro se enrojeció todavía más."


translate spanish day3_breakfast_un_32938f90:


    th "¡Para qué continuar hablando sobre este baile como si no hubiera otra cosa de la que poder hablar!{w} ¡Espera! Hay algo que en realidad..."


translate spanish day3_breakfast_un_998716c3:


    me "Por cierto, ¿tal vez tú sepas qué quería preguntarme Zhenya?"


translate spanish day3_breakfast_un_d0ef9648:


    un "¿Ein?"


translate spanish day3_breakfast_un_d9512f33:


    me "Quiero decir, cuando justo entré... ella quería alguna cosa de mi."


translate spanish day3_breakfast_un_66762623:


    un "No lo sé..."


translate spanish day3_breakfast_un_855db9ee:


    me "Ya veo."


translate spanish day3_breakfast_un_67900051:


    "La conversación alcanzó un punto muerto así que traté de aparentar estar concentrado con mi desayuno."


translate spanish day3_breakfast_un_359fdf19:


    "Lena había terminado primera su desayuno."


translate spanish day3_breakfast_un_82988e32:


    un "Debería irme. ¡Nos vemos más tarde!"


translate spanish day3_breakfast_un_6384aca8:


    me "¡Hasta la vista!"


translate spanish day3_breakfast_mz_e36ce35f:


    "No estoy seguro de por qué, pero acepté la invitación."


translate spanish day3_breakfast_mz_25dd7e46:


    th "Y si puedo aprender algo nuevo...{w} Total, ella es una rata de biblioteca."


translate spanish day3_breakfast_mz_3159bf22:


    "Un minuto más tarde estaba sentado frente a Zhenya, con la bandeja y un desayuno sencillo en ella: gachas de avena, dos huevos escalfados, cuatro piezas de pan de trigo, una salchicha hervida y una vaso de compota."


translate spanish day3_breakfast_mz_9aeb930e:


    me "¡Bon appétit!"


translate spanish day3_breakfast_mz_ca4481dc:


    mz "Gracias."


translate spanish day3_breakfast_mz_8df9aaff:


    "Decidí actuar con educación tanto como pude, y también comer pulcramente... sin mascar ruidosamente y sin derramar pedazos de comida o vertir la compota."


translate spanish day3_breakfast_mz_ebae22b0:


    me "Bueno, ¿y de qué quieres hablar?"


translate spanish day3_breakfast_mz_f4ef416b:


    mz "Hoy en la biblioteca..."


translate spanish day3_breakfast_mz_f6feb39d:


    "No estaba escuchando a partir de este punto, inmediatamente recordé las palabras de Olga Dmitrievna sobre las actividades previstas para hoy."


translate spanish day3_breakfast_mz_63e91f2f:


    mz "¡...no lo olvides!"


translate spanish day3_breakfast_mz_0e394660:


    me "¿Oh? ¿Qué?"


translate spanish day3_breakfast_mz_8af6f3a4:


    mz "Dije que no te olvides de visitar la biblioteca después de comer."


translate spanish day3_breakfast_mz_7332342b:


    me "¿Por qué?"


translate spanish day3_breakfast_mz_486c6399:


    mz "¿Estabas escuchando?"


translate spanish day3_breakfast_mz_5a7a7577:


    me "Nop."


translate spanish day3_breakfast_mz_9fb02d3f:


    "Admití honestamente."


translate spanish day3_breakfast_mz_b4215fb3:


    "El rostro de Zhenya se enrojeció, luego se volvió verde, y finalmente adquirió un suave color morado."


translate spanish day3_breakfast_mz_198f044e:


    mz "¡Hoy! ¡Después de comer! ¡Tú! ¡En la biblioteca! ¿Lo pillas, zoquete?"


translate spanish day3_breakfast_mz_a758b252:


    th "¿Zoquete?{w} ¿Qué clase de insulto es ese?{w} A pesar de que «un pionero nunca debía usar una palabrota», ¿cómo podía olvidar esta norma?"


translate spanish day3_breakfast_mz_0b0c4f9e:


    me "¡Oh, sí, claro! ¡Discúlpame! ¡Por supuesto, estaré ahí!"


translate spanish day3_breakfast_mz_63573b0e:


    "Normalmente, realmente no estaba planeando ir a ninguna parte, pero si quieres que alguien hable, primero necesitas sacrificarte un poco."


translate spanish day3_breakfast_mz_55998771:


    me "Por cierto, ¿qué año es? Me he olvidado totalmente de cúal es."


translate spanish day3_breakfast_mz_42d07202:


    "Decidí jugar limpio."


translate spanish day3_breakfast_mz_f7f9fcdc:


    "Me miró perplejamente y respondió:"


translate spanish day3_breakfast_mz_8ff4b6fa:


    mz "¿Te has vuelto completamente loco?"


translate spanish day3_breakfast_mz_6303a3f4:


    me "Sinceramente, así es cómo me siento estos últimos días."


translate spanish day3_breakfast_mz_133cbf39:


    "Zhenya no respondió."


translate spanish day3_breakfast_mz_b1214d8e:


    me "Bueno, ¿y en qué año estamos?"


translate spanish day3_breakfast_mz_085d124a:


    "Sonreí amablemente."


translate spanish day3_breakfast_mz_4899008d:


    mz "Escucha, ¿por qué no visitas la enfermería?"


translate spanish day3_breakfast_mz_fddf5e90:


    me "¿Me dirán en qué año estamos allí?"


translate spanish day3_breakfast_mz_4686520d:


    mz "¡Oh por supuesto! ¡Y mucho más!"


translate spanish day3_breakfast_mz_63613035:


    "Zhenya se levantó y se dirigió hacia la salida."


translate spanish day3_breakfast_mz_6999c456:


    me "Espera, no terminaste tu desayuno..."


translate spanish day3_breakfast_mz_e9ec16dc:


    "Pero parecía como si ella ya no fuera a volver."


translate spanish day3_main2_0c42e260:


    "Estaba terminando de masticar la salchicha, sabía a un cable de par trenzado."


translate spanish day3_main2_d8c4da3f:


    me "Me pregunto si tendrán un acceso a internet aquí..."


translate spanish day3_main2_c0d3e816:


    me "Supongo que no."


translate spanish day3_main2_7ce38afc:


    "Un pionero sentado en la otra mesa se dio cuenta de que estaba hablándome a mi mismo y me observó. Le sonreí y le saludé en respuesta."


translate spanish day3_main2_cec89b26:


    "Tras semejante saludo amistoso, él, por alguna razón, abandonó la cantina rápidamente sin terminar su desayuno."


translate spanish day3_main2_b91a15cc:


    th "Habiendo acabado con el desayuno... era el momento de buscar algunas pistas."


translate spanish day3_main2_db7938e1:


    "Un rato después finalmente llegué a la enfermería."


translate spanish day3_main2_80770b79:


    th "No tengo nada que hacer en dicho lugar."


translate spanish day3_main2_495d074a:


    "A pesar de que la enfermera parecía preocuparse bastante, era mejor mantener las distancias con ella."


translate spanish day3_main2_0c5af512:


    "¡Semejante benevolencia era en realidad espantosa!"


translate spanish day3_main2_4357f4cf:


    "Ella quizá debía saber algo, pero simplemente yo no tenía coraje para entrar en la enfermería."


translate spanish day3_main2_7ce2c0d2:


    th "Aun más, no tenía motivo alguno.{w} Ni uno solo."


translate spanish day3_main2_f48ed40c:


    "Inmerso en mis propios pensamientos, no me di cuenta de que la enfermera estaba en el porche."


translate spanish day3_main2_e091fcfe:


    cs "¡Hola por ahí... pionero!"


translate spanish day3_main2_b93477a9:


    "Ella se acercó."


translate spanish day3_main2_b306dd8f:


    cs "¿Debes encontrarte mal, has venido a por una medicina?"


translate spanish day3_main2_23823de6:


    me "No, no... Yo... estaba sólo dando vueltas..."


translate spanish day3_main2_10ca59a3:


    cs "Oh, haciendo novillos ya veo."


translate spanish day3_main2_d264f0d9:


    cs "Pero ya que estás aquí, tengo una tarea importante para ti."


translate spanish day3_main2_4a52b166:


    "Le hice una mirada inquisitiva."


translate spanish day3_main2_54a8b79a:


    th "La palabra «tarea» me hizo sentir inquieto, sonaba como algo inevitable."


translate spanish day3_main2_82b55e26:


    "La enfermera sonrió con malicia."


translate spanish day3_main2_972a2399:


    cs "Ambos me ayudaréis a hacer el inventario del suministro de medicinas, las que han llegado hoy."


translate spanish day3_main2_35ca7b8b:


    th "¿Ambos?{w} ¿Quiénes somos?{w} Tal vez lo oí mal."


translate spanish day3_main2_51c41ef6:


    th "¿Pero con qué palabra se puede confundir «ambos»?"


translate spanish day3_main2_8e5e0396:


    un "Hola de nuevo..."


translate spanish day3_main2_e8479859:


    un "Hola..."


translate spanish day3_main2_3b501dc9:


    "Lena apareció por detrás de la enfermera."


translate spanish day3_main2_f4ec6c0a:


    "Resulta raro que no me haya dado cuenta de ella hasta ahora."


translate spanish day3_main2_32f43013:


    cs "Ven aquí después de cenar. Ya le he explicado todo a Lena, ella te dirá qué tienes que hacer."


translate spanish day3_main2_1b0bb99e:


    th "Medicinas, ¿eh? ¿Suministradas hoy?"


translate spanish day3_main2_edbbd70f:


    th "Pero Olga Dmitrievna me dijo que ningún autobús llegaría hasta un par de días."


translate spanish day3_main2_513f14a5:


    me "Así que hoy hubo visitantes en nuestro campamento..."


translate spanish day3_main2_d589ffac:


    cs "Sí, llegaron de la ciudad esta mañana. ¿Por qué lo preguntas?"


translate spanish day3_main2_0baf4d95:


    me "Solo curiosidad..."


translate spanish day3_main2_29261639:


    cs "¡Muy bien pues... pionero!{w} Entonces esta noche, después de cenar, ¡estarás aquí!"


translate spanish day3_main2_918e5db5:


    un "Quizá yo..."


translate spanish day3_main2_f1184b36:


    "Apenas se hacia notar Lena en todo el rato."


translate spanish day3_main2_5a680cf2:


    "Claro, tiene la capacidad de permanecer invisible como un habilidoso ninja."


translate spanish day3_main2_0dac54fc:


    un "Podría hacerlo yo sola."


translate spanish day3_main2_c4e56f03:


    "Qué raro, parecía más extrovertida durante el desayuno."


translate spanish day3_main2_07ecb7bb:


    cs "¡De ninguna manera! Hay demasiadas cajas."


translate spanish day3_main2_189f0241:


    cs "¡Y un pionero siempre está preparado!"


translate spanish day3_main2_92699031:


    cs "¿No es así... pionero?"


translate spanish day3_main2_96567aee:


    "Me hizo una mirada y una sonrisa intimidante."


translate spanish day3_main2_fb4b4310:


    "No quiero pasarme todas las noches haciendo algo como esto, tengo también otras cosas más importantes que hacer."


translate spanish day3_main2_b7a89246:


    "Pero rechazar esta petición sería en verdad inapropiado."


translate spanish day3_main2_e73ac8a5:


    "Si no fuera Lena quien estuviera aquí, ni me lo pensaría en largarme."


translate spanish day3_helpaccept_752f2744:


    cs "¡Fantástico!{w} ¡Así es como un verdadero pionero debería responder!"


translate spanish day3_helpaccept_512b33c1:


    "Incluso tras estas palabras me resultaba tedioso seguir el lema «¡Preparado! ¡Siempre preparado!»."


translate spanish day3_helpaccept_eb225364:


    cs "Pues ya puedes irte."


translate spanish day3_helpaccept_85a9dc87:


    "Miré a Lena, que estaba todavía quieta permaneciendo con su mirada en el suelo."


translate spanish day3_helpaccept_83ee98b6:


    "Supongo que todo el tiempo que ella estuvo quieta ahí, fue suficiente para conocer todo sobre la vida y los hábitos de los diferentes insectos."


translate spanish day3_helpaccept_80cb7fbb:


    "Además, a Lena le gusta leer, por lo que probablemente ella suele leer muchos libros de biología, botánica y de todo eso."


translate spanish day3_helpaccept_84a80626:


    th "Tal vez, ¿quiere convertirse en una entomóloga?"


translate spanish day3_helpaccept_a20cefa7:


    "..."


translate spanish day3_helpaccept_9a9ab591:


    un "¿Hacia dónde vas a ir ahora?"


translate spanish day3_helpaccept_e6c70fa7:


    "Las palabras de Lena me sacaron de mis ofuscados pensamientos."


translate spanish day3_helpaccept_928a1e98:


    "Estuvimos permaneciendo en la plaza por un rato, y mis especulaciones sobre la vida de las cucarachas solamente eran un intento de escapar de esta incómoda situación."


translate spanish day3_helpaccept_553ade04:


    th "A pesar de todo, ¿por qué la veía incómoda?"


translate spanish day3_helpaccept_4f92bcbe:


    un "Simplemente no te olvides, ¿de acuerdo?"


translate spanish day3_helpaccept_56e472ea:


    me "¿Que no me olvide qué?"


translate spanish day3_helpaccept_7ae819c0:


    un "Bueno... esta noche, después de cenar... en la enfermería..."


translate spanish day3_helpaccept_0734476b:


    "Su rostro tenía un ligero enojo en él."


translate spanish day3_helpaccept_d7ab165c:


    th "De ninguna manera, ¡eso es imposible!"


translate spanish day3_helpaccept_41dde6d3:


    me "¡De acuerdo, vale! Justo después de cenar estaré a tu servicio."


translate spanish day3_helpaccept_31f1800b:


    "Lo consideré demasiado tarde... tras mis palabras Lena se ruborizó muchísimo."


translate spanish day3_helpaccept_3d2683cd:


    "Necesito encontrar la forma de quitarle importancia a mis palabras."


translate spanish day3_helpaccept_e57a438b:


    me "¿Por qué te dijo de hacerlo la enfermera?"


translate spanish day3_helpaccept_dc5220db:


    un "No lo sé.{w} Sólo estaba sentada en el banco leyendo un libro, cuando ella se acercó y..."


translate spanish day3_helpaccept_3d5bff62:


    th "Es inteligente pedírselo a una persona que sea fácil de convencer, no te dirá que no."


translate spanish day3_helpaccept_5364658a:


    me "Vale.{w} Bueno pues, después de cenar..."


translate spanish day3_helpaccept_37b80610:


    un "Vale.{w} Iré luego."


translate spanish day3_helpaccept_4bfb0b4b:


    me "¡Sí, claro!"


translate spanish day3_helpaccept_92254f5c:


    "Se fue caminando hacia algún lugar próximo a los clubs, y yo permanecí en la plaza por un rato."


translate spanish day3_helpreject_28e3d09b:


    th "Quizá, no debería haberle mentido."


translate spanish day3_helpreject_78a68a86:


    "Debería pensar antes de decir algo."


translate spanish day3_helpreject_64ea43f2:


    cs "¿Oh de verdad? ¿Y en qué se supone que le ayudarás exactamente?"


translate spanish day3_helpreject_90328db4:


    "Esto iba a peor, pero decidí permanecer en mis trece."


translate spanish day3_helpreject_20aa6bf2:


    me "Su cabaña...{w} Ella me pidió que la limpiara, ordenara las cosas y todo eso."


translate spanish day3_helpreject_eb1f3b24:


    me "¡Apuesto a que ya sabes el caos que tiene esa cabaña!"


translate spanish day3_helpreject_71a1959c:


    cs "Claro... ¿ella te pidió que le ordenaras su armario también?"


translate spanish day3_helpreject_bd7955c4:


    "La enfermera me miró intensamente, tanto que el fiasco de Napoleón en la batalla de Waterloo parecía ahora una pequeña molestia comparado con mi actual fracaso."


translate spanish day3_helpreject_4d02b8d1:


    "Iba a decir algo, pero ella continuó hablando."


translate spanish day3_helpreject_bd364f86:


    cs "Vale pues... pionero.{w} Nos las arreglaremos sin ti."


translate spanish day3_helpreject_48cb1418:


    me "Discúlpame. Pero, ya ves..."


translate spanish day3_helpreject_a1643280:


    "Dije y miré a Lena."


translate spanish day3_helpreject_bd547c7b:


    "Ella seguía mirando el suelo como antes, mientras se ruborizaba más y más."


translate spanish day3_helpreject_28645b37:


    un "No pasa nada... Me las arreglaré yo sola..."


translate spanish day3_helpreject_084c94a3:


    me "Vale pues."


translate spanish day3_helpreject_ffe91023:


    "Un silencio incómodo procedió."


translate spanish day3_helpreject_6c088e5b:


    "Quería que la tierra me tragara por mentir tan mal que probablemente ofendí a Lena."


translate spanish day3_helpreject_1c49e0e4:


    un "Bueno, ¿adiós pues?"


translate spanish day3_helpreject_670845c1:


    me "¡Adiós!"


translate spanish day3_helpreject_5ae42b21:


    "La enfermera estaba aun sonriendo con malicia."


translate spanish day3_helpreject_d6986490:


    "Me di la vuelta y caminé alejándome casi corriendo."


translate spanish day3_helpreject_5c0a5a05:


    "Aunque de hecho, realmente quería salir corriendo, el sentido común me aconsejaba no hacerlo."


translate spanish day3_helpreject_53287076:


    "Mis pasos me llevaron de vuelta a la plaza."


translate spanish day3_main3_16660e40:


    th "Y ahora, ¿qué?"


translate spanish day3_main3_212cd6fc:


    "Decidí visitar mi cabaña, con el fin de recoger mi teléfono."


translate spanish day3_main3_a2b246f4:


    th "No sé dónde me hallaré hoy, ¡pero tener un reloj siempre es útil!"


translate spanish day3_main3_a387e44b:


    th "A pesar de que tener un reloj de pulsera es más fácil."


translate spanish day3_main3_2aff1bdf:


    "Repentinamente escuché una melodía distinta, la cual surgía a través del ruido habitual de este campamento."


translate spanish day3_main3_f9785632:


    "La escuché atentamente.{w} Suena como una guitarra eléctrica."


translate spanish day3_main3_adb705e3:


    "Tres acordes repetitivos, no mucho más."


translate spanish day3_main3_f1b3a7a5:


    "Pero esta melodía todavía era algo más «cálida», como si se usara un tubo de vacío para amplificar el audio."


translate spanish day3_main3_93557c84:


    th "¿Podría haber semejante lujo aquí?"


translate spanish day3_main3_a012ce31:


    "Definitivamente, el sonido venía de algún lugar de la zona del escenario musical."


translate spanish day3_main3_da2b4460:


    th "Me pregunto, ¿quién podría ser?"


translate spanish day3_main3_32f4a81d:


    th "Quienquiera que fuese, ¿qué más da? Tengo mejores cosas que hacer."


translate spanish day3_house_of_mt_c93a9c6d:


    "Volví a la cabaña de la líder."


translate spanish day3_house_of_mt_81f57dce:


    "Olga Dmitrievna estaba tumbada perezosamente en su cama y leyendo un libro."


translate spanish day3_house_of_mt_ca064f09:


    mt "¡Semyon! ¿Qué estás haciendo aquí?"


translate spanish day3_house_of_mt_ff169076:


    th "Eso es lo que yo también podría decir de ti."


translate spanish day3_house_of_mt_ba187c2f:


    me "Me he olvidado una cosa...{w} Por lo que he vuelto."


translate spanish day3_house_of_mt_63ff0148:


    mt "¡Estabas ausente en la formación!"


translate spanish day3_house_of_mt_12ae07e8:


    me "Oh, claro. Discúlpame..."


translate spanish day3_house_of_mt_c5cabca5:


    mt "Está bien.{w} De todos modos, deberías hacer algo útil hoy."


translate spanish day3_house_of_mt_cea8428c:


    "Justo como pensé.{w} Pero tengo mi propia visión de lo que significa «útil»."


translate spanish day3_house_of_mt_e473fa2f:


    me "¿Como por ejemplo?"


translate spanish day3_house_of_mt_2410adae:


    mt "Deberías ir a la plaza, para ayudar en la limpieza. Slavya está ocupándose de ello."


translate spanish day3_house_of_mt_4bc77a00:


    th "Qué raro, justo estuve allí y no vi a nadie."


translate spanish day3_house_of_mt_bce5b940:


    mt "O podrías visitar a los muchachos del club de electrónica... me pidieron que alguien les echara una mano."


translate spanish day3_house_of_mt_280f15cf:


    th "¡Esos «Padres de la invención» otra vez!"


translate spanish day3_house_of_mt_ad452bb7:


    mt "Finalmente, podrías ayudar en el club de deporte."


translate spanish day3_house_of_mt_e2d85953:


    me "¿Qué están haciendo?"


translate spanish day3_house_of_mt_1ab1a7d4:


    mt "Estan renovando la pista: arreglando los bancos, cambiando las redes de las porterías y todo eso."


translate spanish day3_house_of_mt_00d98ab0:


    "Sinceramente, ninguna de esas opciones me entusiasmaban."


translate spanish day3_house_of_mt_3f6ade2d:


    th "¿Por qué siempre ella trata de cargarme con algo de trabajo?"


translate spanish day3_house_of_mt_14075888:


    th "De verdad, salvo Slavya, nadie trabaja aquí, mientras que otras personas particularmente están disfrutando su tiempo libre con espíritu joven."


translate spanish day3_house_of_mt_88a88f60:


    "Cada intento de la líder parece que sea con el único propósito de entretenerme... evitar que busque dónde me encuentro ahora."


translate spanish day3_house_of_mt_c75c7820:


    "En serio, todas las preguntas profundas y vitales concernientes a mi existencia aquí son barridas por una escoba, junto a la basura y los escombros."


translate spanish day3_house_of_mt_11998600:


    "En mi caso pasar algo de tiempo trabajando podría dar resultado, pero al igual que con un paciente que padezca gangrena y rechace una amputación... Permaneceré en mis convicciones."


translate spanish day3_house_of_mt_c8e476b7:


    me "Ya sabes, todavía tengo mis propios asuntos."


translate spanish day3_house_of_mt_f11d83c9:


    mt "Oh, ¿de verdad? ¿Qué tipo de asuntos?"


translate spanish day3_house_of_mt_8efbf7e2:


    "Ella se animó."


translate spanish day3_house_of_mt_f883183e:


    me "Bueno..."


translate spanish day3_house_of_mt_eec772da:


    th "¡No puedo contarle simplemente lo que realmente pienso!"


translate spanish day3_house_of_mt_b155d7d8:


    th "Por otro lado, ¿por qué no?"


translate spanish day3_house_of_mt_e7621f5a:


    th "¡Pero es muy peligroso!"


translate spanish day3_house_of_mt_d4332ab2:


    th "Al menos, por ahora mantengo un frágil equilibrio y nada me está amenazando.{w} Es mejor que todo siga así."


translate spanish day3_house_of_mt_4c7fb1b8:


    mt "¡Justo como pensé!"


translate spanish day3_house_of_mt_508a7e16:


    "Qué {i}pensó{/i} exactamente ella será un secreto."


translate spanish day3_house_of_mt_3a32c862:


    mt "Probablemente no te has dado cuenta aun, lo importante que es participar y tener vida social.{w} Ésa es la única forma de convertirse en un pionero ejemplar."


translate spanish day3_house_of_mt_34d07d39:


    "No quería continuar prestando atención a otro pesado discurso, por lo que decidí aceptar uno de los ofrecimientos."


translate spanish day3_house_of_mt_3a33dc3e:


    th "A veces las respuestas en este mundo se hallan en los lugares más insospechados.{w} O al menos eso quería creer..."


translate spanish day3_house_of_mt_0137337f:


    mt "¿Así, debería escoger por ti?"


translate spanish day3_house_of_mt_d6a8a466:


    "La voz de la líder se tornó autoritaria."


translate spanish day3_house_of_mt_a0d17775:


    me "Puedo hacerlo yo mismo, gracias..."


translate spanish day3_house_of_mt_1553fbf7:


    "Mascullé entre dientes para después salir de la cabaña."


translate spanish day3_square_b65de720:


    "Entre dos males... bueno, entre tres en este caso... escoge el menor, dicen."


translate spanish day3_square_0685b6b9:


    "Slavya no era mala para nada... así que mi elección era obvia."


translate spanish day3_square_c3fd147b:


    "Cualquier criatura racional procura que su casa esté limpia, pero normalmente percibo la limpieza del hogar como algo similar al hacer deporte... claro, es una cosa buena, pero no es lo mío."


translate spanish day3_square_bd3a88ba:


    "De todas maneras, me gustaba la idea de pintar los bancos o la de ayudar a dos futuros genios de la ciencia rusa (¿o soviética?) aun así."


translate spanish day3_square_52919e7f:


    "Había una docena de pioneros en la plaza."


translate spanish day3_square_b54a584e:


    th "De dónde salieron todos ellos, me preguntaba."


translate spanish day3_square_70ff76da:


    "Me acerqué a Slavya."


translate spanish day3_square_1a8f3935:


    me "¡Ey, qué hay!"


translate spanish day3_square_c2326fc9:


    sl "¡Oh, ey! ¿Viniste a ayudar?"


translate spanish day3_square_eac909f4:


    me "Bueno, no salió de mi propia voluntad de hecho..."


translate spanish day3_square_f0f5042d:


    "Lo volví reflexionar."


translate spanish day3_square_01e9adbb:


    sl "Ya veo."


translate spanish day3_square_85d5b601:


    sl "Coge la escoba, tu zona está justo al lado del monumento."


translate spanish day3_square_59f34355:


    "Hablando francamente, no había nada que barrer ahí. Me pareció bastante limpio."


translate spanish day3_square_8c05f3f7:


    "Aunque algo de suciedad había aun por el suelo."


translate spanish day3_square_a20cefa7:


    "..."


translate spanish day3_square_4bf5b11e:


    "Tras barrer durante un rato volví con Slavya que estaba sentada en un banco tomando un descanso."


translate spanish day3_square_3d0d567b:


    me "Qué día más precioso, eh..."


translate spanish day3_square_93a88d17:


    sl "Pues sí, aunque sea un tanto caluroso."


translate spanish day3_square_2a6651bc:


    "Se cubrió los ojos con su mano y miró hacia el cielo."


translate spanish day3_square_d1bfa7c3:


    me "¡Eres como una obrera infatigable del trabajo comunista!"


translate spanish day3_square_b61ed746:


    sl "¡Anda ya! Solamente me gusta ayudar a otros."


translate spanish day3_square_69d18c31:


    me "Eso es bueno."


translate spanish day3_square_5c7b7c48:


    sl "¿Y qué hay de ti?"


translate spanish day3_square_08572abe:


    me "¿Sobre mi qué?"


translate spanish day3_square_7b636361:


    sl "Bueno, me parece que estás un poco ajetreado con el servicio a la comunidad..."


translate spanish day3_square_d76f93fa:


    me "Bueno, probablemente."


translate spanish day3_square_70f37a53:


    sl "¿Cómo es eso?"


translate spanish day3_square_7479b07e:


    me "En realidad no lo sé..."


translate spanish day3_square_03e6aa1b:


    th "En serio... ¡no puede esperar que yo simplemente le suelte a ella todas mis ideas acerca del asunto!"


translate spanish day3_square_5c2a445c:


    th "Se podría ahogar en ellos, Dios no lo quiera!"


translate spanish day3_square_b87ff550:


    sl "¿Tal vez sólo no te gusta la compañía?"


translate spanish day3_square_b9c3fa63:


    me "En realidad es bastante posible."


translate spanish day3_square_f5936064:


    "Había algo de verdad en las palabras de Slavya.{w} Supongo que ella era buena leyendo la mente de los demás."


translate spanish day3_square_553e30c8:


    "Como mínimo... leyendo la mía."


translate spanish day3_square_cff06c81:


    th "A pesar de todo, ¿cómo puede ser posible si a veces encuentro difícil entenderme a mi mismo?"


translate spanish day3_square_6466d398:


    sl "¿Qué vas a hacer cuando la temporada termine?"


translate spanish day3_square_e54b39e9:


    me "¿Qué quieres decir?"


translate spanish day3_square_15860af3:


    sl "Bueno, ¿vas a entrar en la universidad? ¿O quieres aprender un oficio?"


translate spanish day3_square_08e5bdfd:


    "Ya había logrado aprender todo sobre mis oficios... ver anime y navegar por internet."


translate spanish day3_square_862a00b6:


    me "No lo sé todavía... ¿y tú qué?"


translate spanish day3_square_65f5243e:


    sl "Tenemos una pequeña granja en nuestro hogar, por lo que quiero ayudar a mis padres ahí."


translate spanish day3_square_42782762:


    "Sonaba bastante raro... ¿hay granjas privadas en la URSS, donde todas las granjas han de ser colectivas?"


translate spanish day3_square_a5764d60:


    "Pero decidí no entrar en detalles."


translate spanish day3_square_c14f0a08:


    sl "¿Y qué hacen tus padres?"


translate spanish day3_square_f74fad8a:


    "Mis padres no eran un buen tema de conversa."


translate spanish day3_square_2975c9e6:


    "No es que mis padres no sean buenos. Más bien lo contrario, son gente de buen corazón."


translate spanish day3_square_0a57324e:


    "Pero no era el momento idóneo para semejante conversación."


translate spanish day3_square_884ea735:


    me "Mi padre trabaja en el consejo local, y mi madre es profesora."


translate spanish day3_square_0130c38d:


    "Eso en realidad no era verdad.{w} Más bien una media verdad."


translate spanish day3_square_a9edc837:


    sl "Guay."


translate spanish day3_square_5451282f:


    "Ella parecía considerarlo realmente algo guay."


translate spanish day3_square_907b6d20:


    me "Supongo que sí..."


translate spanish day3_square_14c9829e:


    "La conversación llegó a un punto muerto, así que estaba intentando apartar mi mirada de Slavya... para fijarla en mis pies, arriba en el cielo o sólo mirar a mi alrededor."


translate spanish day3_square_d0928e48:


    sl "¡Sabes, estoy segura de que te encontrarás bien igualmente!"


translate spanish day3_square_edee7f76:


    "Slavya me miró profundamente."


translate spanish day3_square_e4df3286:


    th "¿Y ahora a qué viene eso?"


translate spanish day3_square_4752f785:


    th "¿Y de qué iba este «igualmente»?"


translate spanish day3_square_1e5c4921:


    me "G-gracias..."


translate spanish day3_square_87af141e:


    "Murmuré."


translate spanish day3_square_e18b6de3:


    me "¿Pero qué quieres decir?"


translate spanish day3_square_2f5cbfd7:


    sl "Supuse que tiendes a ser demasiado pesimista a veces."


translate spanish day3_square_be54772f:


    th "¿No era esa una manera demasiado, ehmm, directa?"


translate spanish day3_square_2edc6114:


    me "Quizás."


translate spanish day3_square_a6743adf:


    sl "¡Pero ya verás que todo irá bien!"


translate spanish day3_square_2edc6114_1:


    me "Tal vez."


translate spanish day3_square_723945e3:


    "Tras esta conversación me sentía incómodo."


translate spanish day3_square_80cd1196:


    "Por un lado, no había nada de especial en sus palabras."


translate spanish day3_square_fbf07be0:


    "En general, casi cualquier persona en el lugar de Slavya podría llegar a las mismas conclusiones."


translate spanish day3_square_e331d636:


    "No obstante, sentí (y no por primera vez) como si ella pudiera ver a través de mi."


translate spanish day3_square_a3f06af0:


    me "Bueno, vamos a seguir limpiando."


translate spanish day3_square_f021de41:


    sl "Ajá."


translate spanish day3_square_31b44837:


    "Ella sonrió y cogió la escoba."


translate spanish day3_square_5475d679:


    "Nunca pensé que limpiar pudiera ofrecer tan profundo disfrute."


translate spanish day3_square_68594816:


    "No, no estaba molesto a causa de esta conversa. Por el contrario, me alegraba oír eso de ella."


translate spanish day3_square_a5d8ba12:


    "Por primera vez alguien me estaba hablando sobre mi vida sin criticarme, sin darme ningún estúpido consejo basado en su experiencia personal, sin intenciones de convencerme y sin tener prejuicios."


translate spanish day3_square_e172c3fb:


    "Me alegraba de que alguien me apoyara."


translate spanish day3_square_c7b3226b:


    th "¡No, eso no es del todo correcto!"


translate spanish day3_square_01f6d7a7:


    "Me alegraba de que fuera Slavya quien me apoyara."


translate spanish day3_square_a20cefa7_1:


    "..."


translate spanish day3_square_5daea6b4:


    "Al poco rato, la música estaba sonando por el campamento, avisando del momento del almuerzo. Dejé la escoba con un suspiro de alivio y con la sensación de cumplimiento del deber, y me fui a la cantina."


translate spanish day3_clubs_7484a246:


    "Bueno, no deben-ser-tan-grandes después de todo."


translate spanish day3_clubs_f63f81c0:


    "Esa fue seguramente una decisión difícil de tomar."


translate spanish day3_clubs_2a9a2232:


    th "En efecto, ¿hay alguna razón para engancharse y apreciar un tiempo de más con tipos raros sacados de principos de la ciencia ficción del siglo XX?"


translate spanish day3_clubs_2344f7dc:


    th "¿Pero y si me las arreglo para encontrar algo útil con ellos?"


translate spanish day3_clubs_f8ea823f:


    th "Al menos puedo intentarlo."


translate spanish day3_clubs_f58fc437:


    "Estando de pie en el umbral de los edificios de clubes, dudé por un momento. Finalmente, todas las dudas se disiparon. Abrí la puerta y entré."


translate spanish day3_clubs_a57a7173:


    "Electronik y Shurik estaban viendo algo de cerca mientras se inclinaban encima de la mesa."


translate spanish day3_clubs_247d8ad8:


    me "¡Qué tal, cibernéticos! ¿Cómo va?"


translate spanish day3_clubs_0a24ed23:


    "Trataba de comportarme lo más simpáticamente como pude."


translate spanish day3_clubs_994b5e60:


    el "¡Hola! ¡Te estábamos esperando!"


translate spanish day3_clubs_2b8da8a8:


    me "Sin duda."


translate spanish day3_clubs_ed2501a3:


    "Electronik obviamente pretendía ser llamado «el Oráculo»."


translate spanish day3_clubs_736892e2:


    me "Así que, ¿qué necesitas?"


translate spanish day3_clubs_b2eaaab0:


    sh "Nos ayudarás a completar el robot."


translate spanish day3_clubs_8b36978a:


    me "De ninguna manera.{w} Eso está fuera de mi alcance."


translate spanish day3_clubs_df6d570c:


    sh "¡No te preocupes! Te enseñaremos.{w} Cuanto más me odies, más aprenderás. Soy muy exigente..."


translate spanish day3_clubs_0e64dff5:


    el "¡...pero yo soy justo!"


translate spanish day3_clubs_14702694:


    "Electronik se unió jovialmente."


translate spanish day3_clubs_034978a3:


    me "Muchachos, la vida puede ser más que diversión y juegos. Hay tanto de qué hablar...{w} Pero tengo una pregunta y vosotros parece que tenéis una respuesta."


translate spanish day3_clubs_51e049ed:


    sh "¿Cuál es la pregunta?"


translate spanish day3_clubs_db02021d:


    "Preguntó escéptico."


translate spanish day3_clubs_3dcaa86c:


    me "¿Crees que es posible viajar en el tiempo?"


translate spanish day3_clubs_d54b6c5c:


    el "¿Por qué irias a pensar en semejantes cosas?"


translate spanish day3_clubs_c7fd889f:


    "Electronik de repente se puso serio."


translate spanish day3_clubs_09834bc7:


    me "Solamente es una idea..."


translate spanish day3_clubs_1b1a3349:


    me "Ayer cogí un libro de la biblioteca... «La Máquina del Tiempo» de H.G. Wells. Debes haberlo leído. Así que, bueno, estoy pensando en ello."


translate spanish day3_clubs_2db74799:


    sh "A-ah..."


translate spanish day3_clubs_a1121260:


    el "¿Quieres viajar al futuro, ver cómo es la vida allí?"


translate spanish day3_clubs_9cd1fcea:


    me "Qué va, realmente no.{w} Más bien me interesa viajar al pasado."


translate spanish day3_clubs_684146a1:


    el "¿Y eso?"


translate spanish day3_clubs_01e55b39:


    me "No lo sé.{w} Por qué no..."


translate spanish day3_clubs_e63df87d:


    me "Bueno, ¿y qué te parece?"


translate spanish day3_clubs_7094c1fe:


    sh "La Teoría General de la Relatividad postula la existencia de agujeros de gusano, también conocidos como túneles o puentes en el espacio.{w} Pero no lo entenderías de todas formas."


translate spanish day3_clubs_4d434542:


    th "Apuesto a que no."


translate spanish day3_clubs_fa38dcf0:


    sh "De todas formas, en cualquier suposición encontraríamos una gran cantidad de paradojas."


translate spanish day3_clubs_d7d34f50:


    sh "Por ejemplo, si viajaras al pasado y te mataras a ti mismo entonces eso significaría que tu {i}yo{/i} del presente jamás pudo existir."


translate spanish day3_clubs_14901a5e:


    me "Bueno... Parece que..."


translate spanish day3_clubs_c54945d0:


    el "En pocas palabras, es demasiado poco científico."


translate spanish day3_clubs_99f562b4:


    me "Ya veo, ¿pero y si fuera posible...?"


translate spanish day3_clubs_999acc60:


    me "Quiero decir, ¿necesitaría algún tipo de máquina, un instrumento o dispositivo para ir al pasado?"


translate spanish day3_clubs_fc3a598d:


    me "¿O es suficiente solo con dormirse en el lugar equivocado y despertarse en otro tiempo y realidad?"


translate spanish day3_clubs_722901ae:


    sh "Estoy seguro de que la ciencia moderna no tiene respuesta para tu pregunta aun."


translate spanish day3_clubs_14c45b6b:


    th "¡Oh, sep! Suena como de la revista «Mecánicas Populares».{w} He visto un par de números por ahí en mis estanterías también."


translate spanish day3_clubs_855db9ee:


    me "Ya veo."


translate spanish day3_clubs_4ca8b273:


    th "Resulta que venir aquí fue una mala idea..."


translate spanish day3_clubs_84c5e8c5:


    sh "De todos modos si asumimos que las leyes de la física conocidas son incorrectas...{w} O al contrario... las propias leyes son correctas pero no lo sabemos todo sobre ellas..."


translate spanish day3_clubs_a81295d2:


    sh "Entonces puede ser muy posible."


translate spanish day3_clubs_2ad79933:


    me "Vale..."


translate spanish day3_clubs_d061ee8b:


    el "O podria ser otra persona, por ejemplo, una especie más avanzada que la humana que posea conocimientos superiores de la naturaleza."


translate spanish day3_clubs_2ad79933_1:


    me "Vale..."


translate spanish day3_clubs_d6791c6b:


    sh "Podrían construir una máquina del tiempo o viajar a través del tiempo con otros medios."


translate spanish day3_clubs_1fe7cd27:


    me "Vale..."


translate spanish day3_clubs_fde5f662:


    th "Entonces, ¿significa eso que fui enviado al pasado por el Gran Hermano de la humanidad?"


translate spanish day3_clubs_738b4f20:


    th "Una idea interesante.{w} Y parece que es razonable, si lo piensas."


translate spanish day3_clubs_ec5fec4e:


    me "¿Y cómo podría uno encontrar estos... esos de los que has estado hablando... si estuviera ya en el pasado?"


translate spanish day3_clubs_3d1c4a13:


    el "¡¿Cómo demonios lo vamos a saber?!"


translate spanish day3_clubs_e6875f9c:


    "Ambos se rieron a carcajadas."


translate spanish day3_clubs_d66197e0:


    me "¡Bueno, gracias, muchachos!"


translate spanish day3_clubs_385627a7:


    "Estaba a punto de irme."


translate spanish day3_clubs_4469b6cb:


    el "¡Ey, espera! ¿Y qué hay de nuestro robot...?"


translate spanish day3_clubs_a54d7085:


    me "¡Estoy seguro de que todo irá bien con él!"


translate spanish day3_clubs_ebde4c68:


    "Los argumentos de los principiantes de cibernéticas eran bastante lógicos pero, ay, no estaba ni a un centímetro de estar cerca de la solución al misterio de mi llegada a este mundo."


translate spanish day3_clubs_aaeb79f3:


    "Me fui a dar un paseo por el campamento durante un rato y luego me dirigí a la cantina."


translate spanish day3_playground_us_8f643492:


    "Esperaba poder evitar el trabajo obligatorio si hacía ver que ayudaba en el club de deportes."


translate spanish day3_playground_us_0eacc803:


    th "Obviamente ellos tienen muchísimos miembros y probablemente de todas formas se las apañarían sin un par de manos más."


translate spanish day3_playground_us_cf36ec08:


    "Sin embargo, era mejor visitar la pista de deportes... al menos para comprobarlo."


translate spanish day3_playground_us_1ea33f5c:


    th "¡Sólo por si acaso Olga Dmitrievna halla a su pionero preferido haciendo el holgazán ignorando el servicio a la comunidad!"


translate spanish day3_playground_us_994a3de0:


    th "Nunca sabes. Todavía podría imaginarme peores castigos que un reproche en público de una reunión del partido."


translate spanish day3_playground_us_19538e61:


    "En verdad tampoco contemplaba que eso sucediera."


translate spanish day3_playground_us_d1f23116:


    "Súbitamente, no vi nada para limpiar, pintar o reparar salvo un partido de fútbol."


translate spanish day3_playground_us_5c2f927e:


    "Estaban jugando cinco contra seis."


translate spanish day3_playground_us_6a412919:


    "Eché un vistazo entre los jugadores y reconocí a Ulyana."


translate spanish day3_playground_us_bb01375d:


    th "No me entrometería, solamente lo vería."


translate spanish day3_playground_us_4d2c583d:


    "Los equipo estaban claramente mal equilibrados."


translate spanish day3_playground_us_3156dd24:


    "Un equipo consistía enteramente de muchachos jóvenes que aparentaban tener unos doce años."


translate spanish day3_playground_us_826e23c2:


    "En el otro equipo todos eran adolescentes mayores."


translate spanish day3_playground_us_2cec2c18:


    "Y eso a pesar de que tenían un jugador de más... Ulyana."


translate spanish day3_playground_us_9cd2564c:


    "Por lo que... ¿Qué puede hacer una sola persona, aun más, una muchacha?"


translate spanish day3_playground_us_e419209b:


    "Bien pronto, me di cuenta de que estaba equivocado."


translate spanish day3_playground_us_8eabf611:


    "Ulyana tenía una buena técnica de regate, atravesando las defensas una a una."


translate spanish day3_playground_us_ec740a50:


    "Obviamente carecía de habilidades de juego en equipo pero tampoco eran necesarias con ese nivel... ella marcaba goles con envidiable facilidad."


translate spanish day3_playground_us_4ef98874:


    "Me acerqué a la pista."


translate spanish day3_playground_us_ad1edcfd:


    us "¡Ey! ¡Ven aquí con nosotros! A ellos les falta un jugador en su equipo."


translate spanish day3_playground_us_c1521ece:


    me "¡Supongo que es así como se hace servicio a la comunidad aquí!"


translate spanish day3_playground_us_c8c094f8:


    us "¡Hemos terminado ya todo!"


translate spanish day3_playground_us_59c94eb5:


    "Dijo Ulyana, sintiéndose ofendida."


translate spanish day3_playground_us_c5377880:


    "Miré alrededor por la pista de deportes otra vez... los bancos estaban recientemente pintados y habían redes nuevas puestas detrás de las porterías."


translate spanish day3_playground_us_f94073b8:


    th "¿Cómo se las han arreglado para ocuparse de todo esto?"


translate spanish day3_playground_us_c9ad2b53:


    us "¡Vamos únete a su equipo!"


translate spanish day3_playground_us_c387cb1e:


    th "Bueno, dado que la última vez me escapé a tiempo, debería claramente jugar ahora."


translate spanish day3_playground_us_fa4c2e93:



    nvl clear
    "El balón al centro. Comenzamos."


translate spanish day3_playground_us_231641a0:


    "Con el primerísimo contacto ya me hice con el balón chutándolo por la escuadra con facilidad."


translate spanish day3_playground_us_4e23a665:


    "En realidad no era nada difícil... la pista era como mucho de cincuenta metros de largo y el guardameta del equipo contrario no podía alcanzar siquiera el larguero."


translate spanish day3_playground_us_6f9f7610:


    "Un poco más tarde, estábamos empatados, a pesar de que nuestro equipo estaba con siete u ocho goles en contra desde el principio."


translate spanish day3_playground_us_f50e5ca2:


    "Hablando objetivamente, uno se daría cuenta también de que el equipo de Ulyana marcó un puñado de goles durante mi rato aquí."


translate spanish day3_playground_us_676b6f90:


    "Más exactamente, ella los marcó."


translate spanish day3_playground_us_4c72f6da:


    "No se podría decir que tuviera esos trucos a lo Ronaldinho o tiros a lo Beckham sacados de la manga... ella sólo chutaba el balón en campo abierto y se acercaba a la portería chutando con la «puntera»."


translate spanish day3_playground_us_0a5728b2:



    nvl clear
    "A pesar de ello, semejante torpe estrategia les dio un resultado positivo. No podía estar pendiente de cada uno de los oponentes y mis compañeros de equipo no eran de mucha utilidad."


translate spanish day3_playground_us_d2f46bc1:


    "Ciertamente, jugaba mucho mejor que todos ellos y en teoría podía marcar tantas veces como quisiera pero a mi me parecía antideportivo.{w} Ganar no lo es todo, es participar lo que cuenta, ¿no es así?"


translate spanish day3_playground_us_6cc66158:


    "Hasta que Ulyana dijo:"


translate spanish day3_playground_us_0de2793e:


    us "Tiro de penalti.{w} El que gane, gana el partido."


translate spanish day3_playground_us_36f04bc6:


    "Me hice a la idea de que no quería perder."


translate spanish day3_playground_us_002ee4a4:


    "En ese momento estábamos 11:9 a nuestro favor, por lo que tampoco era motivo para negarse."


translate spanish day3_playground_us_1c08886f:


    us "Estás en la portería, voy a chutar. Luego nos cambiamos."


translate spanish day3_playground_us_bcaae21b:



    nvl clear
    me "¿Un tiro cada uno?"


translate spanish day3_playground_us_df6dbf74:


    us "Sí."


translate spanish day3_playground_us_04bd3ea8:


    "Permanecí en la línea y me preparé."


translate spanish day3_playground_us_db151b99:


    "Ella cogió carrerilla y...{w} la agarré directamente en mis manos."


translate spanish day3_playground_us_57a622de:


    us "¡Eso no es justo!"


translate spanish day3_playground_us_16bdda4a:


    me "¿Qué no es justo?"


translate spanish day3_playground_us_098f4867:


    us "Voy a chutar de nuevo."


translate spanish day3_playground_us_3065208e:


    me "Vale, hazlo."


translate spanish day3_playground_us_d602bb2d:


    "Dije sonriendo."


translate spanish day3_playground_us_a6e809e4:


    "El segundo chute hubiera sido mejor en rugby."


translate spanish day3_playground_us_57923b1f:


    me "¿Una vez más?"


translate spanish day3_playground_us_76b2fe88:


    nvl clear


translate spanish day3_playground_us_5ebb10b9:


    us "¡Pues no!"


translate spanish day3_playground_us_393056bc:


    "Ella gimoteó."


translate spanish day3_playground_us_f88907e2:


    us "¡Pero no conseguirás marcarme un gol a mi!"


translate spanish day3_playground_us_f4d2ad6b:


    me "Vamos a ver..."


translate spanish day3_playground_us_05c10dae:


    "Chuté con precisión colocando el balón por la escuadra derecha."


translate spanish day3_playground_us_9f646b2e:


    "Ulyana ni siquiera se movió."


translate spanish day3_playground_us_94dc722a:


    "Sin embargo, no había motivo ya que... tiros de penalti como éstos eran imparables."


translate spanish day3_playground_us_34ad2df9:


    me "Parece que he vencido."


translate spanish day3_playground_us_a01972f6:


    "Ella no me respondió nada y simplemente me miró resentidísima."


translate spanish day3_playground_us_24a977a7:


    "Tenía que quitarle importancia a la situación."


translate spanish day3_playground_us_f5bed6f9:


    me "¡No te enfades!{w} Yo sólo..."


translate spanish day3_playground_us_e3b0eb4b:


    "Y ahí es cuando la música volvió a tocar, llamando a los pioneros para almorzar."


translate spanish day3_playground_us_f6bba3b9:


    us "Muy bien, ¡ya me ocuparé de ti en otro momento!"


translate spanish day3_playground_us_a66acadc:


    "Ella sonrió, saludó con su mano y se fue corriendo en dirección a la cantina."


translate spanish day3_playground_us_19a2ba8b:


    "Hice unos pocos toques con el balón y la seguí."


translate spanish day3_stage_dv_57fadfbd:


    "Al llegar al escenario me percaté de una mirada de Alisa en la grada."


translate spanish day3_stage_dv_14e3da9e:


    "La muchacha lo estaba dando todo... con sus ojos cerrados, tenía una de sus piernas encima de uno de los altavoces y estaba balanceando su cuerpo al ritmo de la música."


translate spanish day3_stage_dv_ebaf2447:


    "No podía reconocer la melodía pero estaba muy seguro de que la había escuchado antes."


translate spanish day3_stage_dv_bbd1630b:


    th "Probablemente algún tipo de rock soviético."


translate spanish day3_stage_dv_b91c885c:


    "Podría nombrar una docena de bandas de éste género inmediatamente, pero no era un gran experto."


translate spanish day3_stage_dv_abb4c6d0:


    "La canción parecía bastante sencilla, literalmente una progresión de tres acordes."


translate spanish day3_stage_dv_61c07cfb:


    "Probablemente, incluso aunque hubiera podido tocarla, me hubiese tomado tiempo practicarla."


translate spanish day3_stage_dv_3ae0fcac:


    th "Me pregunto dónde aprendió Alisa a tocar así..."


translate spanish day3_stage_dv_3e58d153:


    "En la URSS este tipo de música nunca fue popular nacionalmente."


translate spanish day3_stage_dv_923112f9:


    "La futura estrella del rock no se dio cuenta de mi presencia."


translate spanish day3_stage_dv_3aea360d:


    "Parecía que trataba de convertirse en una con la música, resonar en cada una de las notas, en cada barra, en cada triplete."


translate spanish day3_stage_dv_d8f81d0f:


    "Los héroes del rock de los ochenta me vinieron al instante a la cabeza... muchos de ellos estaban dedicados a la música de una forma similar."


translate spanish day3_stage_dv_a08c8857:


    th "Si ella estuviera vestida en algo más acorde para la moda de aquellos tiempos en vez del uniforme de pionera, nadie notaría la diferencia."


translate spanish day3_stage_dv_8ec8402b:


    "Su {i}actuación{/i} estaba llegando al final."


translate spanish day3_stage_dv_2a480da6:


    "Tocando un par de los últimos acordes, Alisa se dio cuenta de mi por fin."


translate spanish day3_stage_dv_13716aa5:


    dv "¿Te ha gustado?"


translate spanish day3_stage_dv_4f67d38e:


    "No parecía estar tan sorprendida como hubiera esperado."


translate spanish day3_stage_dv_4041176b:


    me "Claro, estuvo guay."


translate spanish day3_stage_dv_944e8574:


    dv "¡Como si pudieras hacerlo mejor!"


translate spanish day3_stage_dv_495c9ac7:


    "Sonrió Alisa astutamente."


translate spanish day3_stage_dv_149c1b07:


    me "No estoy diciendo que lo pueda hacer mejor.{w} No soy un crack con la música."


translate spanish day3_stage_dv_65c958c7:


    dv "No lo dudo."


translate spanish day3_stage_dv_bd025218:


    me "Sep."


translate spanish day3_stage_dv_d16a7088:


    "No sabía qué decirle así que me iba ir."


translate spanish day3_stage_dv_f72c72fe:


    dv "Espera un minuto."


translate spanish day3_stage_dv_ad02f53a:


    me "¿Qué?"


translate spanish day3_stage_dv_4657233d:


    dv "¿Te enteraste de la fiesta de disco de esta noche?"


translate spanish day3_stage_dv_f14dc8aa:


    me "Pensé que sólo era para bailar..."


translate spanish day3_stage_dv_f48c6409:


    dv "Discoteca o bailar... ¿qué diferencia hay?"


translate spanish day3_stage_dv_823e3ce8:


    "Ella frunció el ceño."


translate spanish day3_stage_dv_c6a323d5:


    me "Sí, me enteré, ¿y qué?"


translate spanish day3_stage_dv_a67a2d53:


    dv "¿Vas a ir?"


translate spanish day3_stage_dv_2625e9c4:


    "Habiendo dicho estas palabras, se dio la vuelta."


translate spanish day3_stage_dv_0e943fd7:


    me "No estoy seguro... ¿Y tú?"


translate spanish day3_stage_dv_2e685a48:


    dv "¿Qué iba hacer allí? ¿Ver un rebaño de idiotas?"


translate spanish day3_stage_dv_30498730:


    "Había cierta verdad en las palabras de Alisa, pero esta pintoresca falta de respeto me sorprendió."


translate spanish day3_stage_dv_a94386e7:


    "Está claro que no es la idea de fiestas de baile lo que ella odia."


translate spanish day3_stage_dv_977aad9c:


    me "¿Pero por qué?"


translate spanish day3_stage_dv_a35f4984:


    dv "¿Qué quieres decir con «por qué»?"


translate spanish day3_stage_dv_99ad6855:


    me "Bueno, ¿por qué no quieres venir?{w} Las fiesta de baile suelen ser divertidas..."


translate spanish day3_stage_dv_c217ed8b:


    dv "¿Realmente crees eso?"


translate spanish day3_stage_dv_868730ed:


    "Un cierto rasgo de enojo se notaba en su voz."


translate spanish day3_stage_dv_9d80b9cf:


    me "No lo sé.{w} Pero creía que a ti te..."


translate spanish day3_stage_dv_86c3ec28:


    dv "Nunca me han gustado."


translate spanish day3_stage_dv_0d425fdb:


    "Me interrumpió."


translate spanish day3_stage_dv_c0fab6ae:


    dv "¡No tengo nada que hacer en tales fiestas!"


translate spanish day3_stage_dv_97523377:


    me "Ya veo...{w} Bueno, ¿y cuál es tu plan pues?"


translate spanish day3_stage_dv_a2c6cc1c:


    "Le pregunté solamente para mantener la conversa."


translate spanish day3_stage_dv_81a1f427:


    dv "Simplemente me iré a practicar."


translate spanish day3_stage_dv_2c8d8df5:


    me "¿Qué irás a practicar?"


translate spanish day3_stage_dv_096cd639:


    dv "¡La canción, gilipollas! ¡La has estado escuchando!"


translate spanish day3_stage_dv_d4729704:


    "Alisa se volvió completamente loca. Unas cuantas preguntas incorrectas... y tendré que correr para salvar el pellejo."


translate spanish day3_stage_dv_5f54b3cd:


    me "¿Quién escribió esta canción?"


translate spanish day3_stage_dv_b4427906:


    dv "¡Yo!"


translate spanish day3_stage_dv_2d4780ec:


    me "Entonces, ¿la escribiste tú misma?"


translate spanish day3_stage_dv_5977fdff:


    dv "¡Yo mi misma!"


translate spanish day3_stage_dv_67492ba0:


    me "Guay..."


translate spanish day3_stage_dv_4933fe62:


    "Un silencio incómodo se respiraba en el aire."


translate spanish day3_stage_dv_621c7bd3:


    me "Vale pues..."


translate spanish day3_stage_dv_caeeba52:


    dv "¿No quieres escucharla?"


translate spanish day3_stage_dv_98aef877:


    me "¿No la acabo de escuchar?"


translate spanish day3_stage_dv_0433bfa6:


    dv "¡Eso no es lo que quiero decir!"


translate spanish day3_stage_dv_e16a3b7b:


    "Ella hizo una mueca."


translate spanish day3_stage_dv_83ff5116:


    dv "¡La canción entera! Ahora mismo sólo es una improvisación."


translate spanish day3_stage_dv_0d499a44:


    me "Ah... Vamos, hazlo pues..."


translate spanish day3_stage_dv_2af74aa2:


    dv "Ahora mismo no."


translate spanish day3_stage_dv_9e500e36:


    me "Pues no la toques..."


translate spanish day3_stage_dv_46a31379:


    "Me sentía desconcertado sobre lo que ella quería de mi."


translate spanish day3_stage_dv_3564e101:


    dv "¿Quieres decir que no me quieres escuchar tocar?"


translate spanish day3_stage_dv_1594019e:


    th "Parece que la he frustrado."


translate spanish day3_stage_dv_9a66cc72:


    "Estaba preparado para que me machacara la guitarra en la frente o algo peor."


translate spanish day3_stage_dv_d7b3342b:


    "Pero solamente había una expresión de enfado en el rostro de Alisa."


translate spanish day3_stage_dv_02a64e1d:


    me "Quiero... Te lo dije."


translate spanish day3_stage_dv_57a1e5c0:


    dv "Pues ven aquí esta noche... La tocaré para ti."


translate spanish day3_stage_dv_421da051:


    th "¿«Para mi»?{w} Qué precioso."


translate spanish day3_stage_dv_964231c7:


    me "Esta noche es una fiesta de baile."


translate spanish day3_stage_dv_c9fb6705:


    dv "¡Dijiste que no irías allí!"


translate spanish day3_stage_dv_42ff6b7c:


    th "Yo no dije eso..."


translate spanish day3_stage_dv_2c3eb69e:


    "Hablando francamente, no quería ir, pero tener un lío con DvaCheh tampoco era bueno."


translate spanish day3_stage_dv_117c8fe1:


    me "Olga Dmitrievna no lo entendería..."


translate spanish day3_stage_dv_73cb60c5:


    dv "¡Engáñala!"


translate spanish day3_stage_dv_c8090db3:


    "Dijo Alisa, con más carácter."


translate spanish day3_stage_dv_7f329f3b:


    "Entre ser una vergüenza en la fiesta de baile y la descabellada habilidad con la guitarra de Dvachevskaya prefiero escoger lo segundo."


translate spanish day3_stage_dv_0d0c3fa8:


    dv "Buen muchacho."


translate spanish day3_stage_dv_1f246e2c:


    "Sonrió de forma extraña."


translate spanish day3_stage_dv_19538e61:


    "Todo esto no era lo que en verdad había planeado."


translate spanish day3_stage_dv_2567c047:


    dv "¡Oh vamos déjalo, tampoco es que estuviera realmente interesada!"


translate spanish day3_stage_dv_412caa87:


    "Ella resopló con desaprobación y se alejó."


translate spanish day3_stage_dv_a97da226:


    "Estaba pensando si la decisión que tomé era una correcta cuando, repentinamente, fui interrumpido por la sirena, avisando a los pioneros para el almuerzo."


translate spanish day3_stage_dv_9256eb27:


    "Miré en dirección a la cantina."


translate spanish day3_stage_dv_20b3b612:


    th "Supongo que es el momento de ir... el hambre es una espina muy afilada."


translate spanish day3_stage_dv_402e53da:


    "Me di la vuelta con la intención de llamar a Alisa para ir juntos."


translate spanish day3_stage_dv_c0edcd63:


    "Pero se le veía muy desanimada y deprimida."


translate spanish day3_stage_dv_be5bcccb:


    me "Cada compositor debería saber cuándo es el momento de dejarlo."


translate spanish day3_stage_dv_3e1d421a:


    "Gruñí con mi suspiro."


translate spanish day3_main4_a20cefa7:


    "..."


translate spanish day3_main4_e3a9b50b:


    "«¿Quién marcha en masa sobre su paso? ¡Es nuestra escuadra de pioneros!»"


translate spanish day3_main4_9086ffe8:


    "Los pioneros {i}en masa{/i} marchan habitualmente hacia la cantina en el campamento «Sovyonok»."


translate spanish day3_main4_ffd49d36:


    "Estaba buscando algún lugar en el que nadie me molestara, justo como ocurrió antes."


translate spanish day3_main4_45805593:


    th "¡Sólo para poder comer en paz!"


translate spanish day3_main4_5b7d20b3:


    "Y para eso no debo ser por lo menos el último en llegar."


translate spanish day3_main4_3d31ce74:


    th "Pero estaba bastante seguro: fuera lo que fuese que ocurre aquí, sucede tanto si quiero como si no."


translate spanish day3_main4_4cb70312:


    "La cantina estaba tan llena que no cabía una aguja."


translate spanish day3_main4_7168ef23:


    "Olga Dmitrievna estaba frente a la entrada, como un halcón haciendo guardia."


translate spanish day3_main4_764d9df7:


    mt "Bueno, Semyon, ¿has estado trabajando duramente hoy?"


translate spanish day3_main4_aac8720f:


    me "Bastante."


translate spanish day3_main4_4e3031c8:


    mt "Bien hecho, ¡de verdad qué bien! ¡Y lo más duro está por llegar todavía!"


translate spanish day3_main4_f1d5cd91:


    th "Apuesto que sí..."


translate spanish day3_main4_63b5e296:


    mt "Bien, toma asiento junto a las muchachas."


translate spanish day3_main4_4cd2fcde:


    "Ella me indicó la mesa que estaba al lado de una columna.{w} Slavya, Ulyana y Lena ya estaban sentadas allí."


translate spanish day3_main4_65e51c2d:


    th "No era mala compañía.{w} Al menos, no la peor..."


translate spanish day3_main4_74860e53:


    "Cogí la comida y me acerqué a ellas."


translate spanish day3_main4_4bf10d5d:


    me "¿No os importa si me siento aquí?"


translate spanish day3_main4_a4520622:


    "Me dio la sensación de parecer algo pintoresco. No había otro lugar con asientos libres de todas formas."


translate spanish day3_main4_ae35202b:


    sl "¡Sí, claro que puedes!"


translate spanish day3_main4_ae6727cd:


    us "Adelante."


translate spanish day3_main4_2622f621:


    "Lena permaneció en silencio."


translate spanish day3_main4_fe3ea323:


    "Hoy la comida consistía en un plato de borscht (sospeché que algo de carne estaba oculto en él, pero no tenía evidencia de ello), algo de ave de corral (aparentemente de pollo doméstico) con patatas fritas y una tradicional copa de compota."


translate spanish day3_main4_6e7c603f:


    "Me daba cuenta que cada vez me gustaba más y más la comida lugareña."


translate spanish day3_main4_b6d69ff6:


    "Supongo que llegué a la conclusión de que no tenía otra opción. Con lo cual no tenía sentido quejarme de ello."


translate spanish day3_main4_123f8387:


    th "Gracias a Dios que hay {i}alguna cosa{/i} para comer."


translate spanish day3_main4_b15bd6e7:


    sl "¿Vendrás a la fiesta de baile esta noche?"


translate spanish day3_main4_595d7b8c:


    me "No lo sé."


translate spanish day3_main4_6e25c9b3:


    th "A pesar de que ya tenía mis planes con Alisa..."


translate spanish day3_main4_4c1600f4:


    us "¡Vendrá! ¡No tiene adonde ir!"


translate spanish day3_main4_99ce3e5f:


    "Dijo Ulyana felizmente."


translate spanish day3_main4_56e1d2cc:


    me "Tú irás seguro..."


translate spanish day3_main4_aa1aa469:


    us "¡Por supuesto! No puedo perderme la oportunidad de verte hacer el ridículo."


translate spanish day3_main4_15dfe923:


    "Estaba en lo cierto así que decidí no responderle."


translate spanish day3_main4_b78c6e26:


    me "¿Y qué hay de ti?"


translate spanish day3_main4_3329e7d9:


    "Le pregunté a Lena."


translate spanish day3_main4_31b01f67:


    un "Sí..."


translate spanish day3_main4_521b6831:


    "Respondió brevemente."


translate spanish day3_main4_56d548d4:


    sl "¿Lo ves? Deberías ir tú también pues."


translate spanish day3_main4_f6a30a37:


    "Dijo Slavya como si me dejara sin opción."


translate spanish day3_main4_e75e0db2:


    us "No te olvides de ponerte tu traje de frac."


translate spanish day3_main4_0033c883:


    "Aparentemente Ulyana estaba tan satisfecha con su broma que se rió a carcajadas."


translate spanish day3_main4_bb5ceb0f:


    "Pero en realidad no tenía nada para vestir."


translate spanish day3_main4_006252b0:


    "En mi armario todo lo que había era tan sólo un uniforme de pionero y las ropas de invierno, las cuales eran inapropiadas incluso por la noche."


translate spanish day3_main4_99eeab2f:


    me "¿Y de qué irás vestida, artista?"


translate spanish day3_main4_0063fafa:


    us "¡Se-cre-to!"


translate spanish day3_main4_85dd239b:


    me "¿Será un vestidito como en una sesión por la tarde de un jardín de parvulitos?"


translate spanish day3_main4_8d42266c:


    "Ulyana se sonrojó... parece que me las arreglé para ofenderla."


translate spanish day3_main4_90fa481a:


    us "¡No, vestiré un traje de protección biológico! ¡Así no me infectaré de ti!"


translate spanish day3_main4_405ea5ee:


    me "Me pregunto qué tipo de enfermedad te esperas coger de mi..."


translate spanish day3_main4_a840c516:


    sl "¡Basta ya, muchachos! ¡No riñáis!"


translate spanish day3_main4_1c4b960b:


    us "¡La estupidez, por supuesto!"


translate spanish day3_main4_59986520:


    "Parecía que Ulyana estaba muy satisfecha de sí misma, ya que se le ocurrió una brillante respuesta otra vez."


translate spanish day3_main4_7312ace7:


    me "Ya sabes, si ya tienes una gripe no puedes coger un resfriado."


translate spanish day3_main4_dbe6fc9f:


    "Es un juego de dos."


translate spanish day3_main4_4d08faa9:


    us "¿Qué estás insinuando?"


translate spanish day3_main4_8d6a94af:


    me "Oh nada, nada en absoluto..."


translate spanish day3_main4_2ed6c63f:


    "Aparté la mirada astutamente."


translate spanish day3_main4_b155806b:


    us "¿Quieres decir que...?"


translate spanish day3_main4_a852076f:


    "Ella se volvió a sonrojar otra vez."


translate spanish day3_main4_36af1581:


    me "No quiero decir nada."


translate spanish day3_main4_e7059468:


    un "Muchachos..."


translate spanish day3_main4_94c9b804:


    "Dado que fue Lena quien intervino, probablemente era el momento idóneo para parar."


translate spanish day3_main4_e4c12ca4:


    us "¡Ya verás, recibirás de tu propia medicina!"


translate spanish day3_main4_b9d24980:


    me "¿Qué? ¿Finalmente te hiciste sabia?"


translate spanish day3_main4_5fea1e70:


    "En vez de responder, Ulyana agarró su plato lleno de borscht y me lo tiró encima de mi cabeza."


translate spanish day3_main4_9f238d85:


    "Este juego alcanzó un inesperado final..."


translate spanish day3_main4_670b85c3:


    me "Oh, enana..."


translate spanish day3_main4_874c2400:


    "Saltó alejándose e intentó salir corriendo."


translate spanish day3_main4_3d220a8d:


    "Pero esta vez no se escaparía... Le agarré de la mano."


translate spanish day3_main4_e4c890fc:


    th "Ehm, ¿y ahora qué? ¡No podía golpear su cabeza contra la mesa!"


translate spanish day3_main4_8f1d5041:


    "Esta situación se alargó unos pocos segundos."


translate spanish day3_main4_8811fef6:


    "Repentinamente, Ulyana hábilmente cogió una copa de compota y me la lanzó en mi cara."


translate spanish day3_main4_3958434a:


    "De esa manera se las arregló para que le soltara la mano."


translate spanish day3_main4_7e7c5066:


    "Salió corriendo hacia el mostrador del buffet, y yo salí detrás de ella."


translate spanish day3_main4_7321633c:


    "Esto conllevó que varias mesas se cayeran, un montón de vajillas rotas, cinco pioneros lisiados con varias heridas y con ambas partes completamente exhaustas."


translate spanish day3_main4_b1e8f257:


    th "Un empate, o algo así."


translate spanish day3_main4_17b22f81:


    th "Un empate beligerante.{w} Con demasiada beligerancia."


translate spanish day3_main4_e6d45acc:


    "Estábamos el uno frente al otro y respirando profundamente."


translate spanish day3_main4_0224f2b4:


    me "¡Dime que no te comportarás así otra vez!"


translate spanish day3_main4_54810cc5:


    us "¡¿Y tú?!"


translate spanish day3_main4_23a87547:


    "Olga Dmitrievna se interpuso entre nosotros cuando apareció por detrás."


translate spanish day3_main4_2948bc41:


    "Aunque no le prestases atención, semejante desorden no pasa desapercibido."


translate spanish day3_main4_501e435e:


    mt "Bueno, ¿están satisfechos consigo mismos ahora?"


translate spanish day3_main4_a3227502:


    "Su voz aparentaba estar en calma, pero estaba seguro de que estaba a punto de explotar."


translate spanish day3_main4_9799cd00:


    mt "¡¿Y quién va a limpiar todo este desastre?!"


translate spanish day3_main4_643530f6:


    "Qué sucedió exactamente."


translate spanish day3_main4_a71a3e52:


    mt "¡¿Quién, os estoy preguntando, quién?!"


translate spanish day3_main4_cc05f4a5:


    us "¡Él!"


translate spanish day3_main4_6e0e9ab6:


    "Respondió Ulyana con plena confianza."


translate spanish day3_main4_8cc8ea72:


    me "¡Ella!"


translate spanish day3_main4_078c536a:


    "Me opuse con menos confianza."


translate spanish day3_main4_221ac838:


    mt "¡Ambos!"


translate spanish day3_main4_cd7733bf:


    "La líder del campamento dio fin a este argumento recursivo."


translate spanish day3_main4_abca6dac:


    "Generalmente, no estaba seguro de que mi parte de culpa fuera más que la suya."


translate spanish day3_main4_d4b025ce:


    "Aunque Ulyana no tenía ni una pizca de culpabilidad en su cara."


translate spanish day3_main4_3297e1dc:


    us "¡Lárgate!{w} ¡No voy a limpiar! ¡Nadie tiene tiempo para eso!"


translate spanish day3_main4_79a78d45:


    us "¡Esto es culpa suya! ¡Él empezó primero!"


translate spanish day3_main4_fa057752:


    me "¡No, no lo es!"


translate spanish day3_main4_0f7ff14d:


    us "¡Oh sí, sí lo es!"


translate spanish day3_main4_179e42ee:


    mt "¡No te voy a seguir la corriente con ése sin sentido!"


translate spanish day3_main4_97932981:


    mt "Semyon, coge una fregona, un cubo, algún trapo, ya sabes, ese tipo de cosas que hay en la despensa de limpieza, ¡¡¡y tú!!!"


translate spanish day3_main4_ccad0a84:


    "Ella miró a Ulyana con tal ardiente mirada, que me sentí un poco mal por la muchacha."


translate spanish day3_main4_f438658b:


    mt "¡¡¡Tú!!!{w} ¡Comienza a recoger la vajilla rota inmediatamente!"


translate spanish day3_main4_097f4a0a:


    "Olga Dmitrievna tomó un gran respiro y prosiguió:"


translate spanish day3_main4_12c1e19a:


    mt "¡No eres más que un estorbo!{w} Cuántas veces te habré dicho que..."


translate spanish day3_main4_051185fa:


    "Decidí saltarme el sermón e ir a la despensa de limpieza que resultó estar al lado de la salida."


translate spanish day3_main4_2ffc3634:


    "De repente una idea perfecta se me pasó por mi cabeza... ¿por qué simplemente no me voy corriendo?"


translate spanish day3_main4_27e1f3fe:


    "Claro, Olga Dmitrievna ya nos ha pillado, pero no soy el único a quien echarle la culpa."


translate spanish day3_main4_9c178a5f:


    "Por supuesto, no soy ese {i}pionero auténtico{/i} del que siempre está hablando, pero en comparación con Ulyana..."


translate spanish day3_main4_4ec22d66:


    "De todas formas, ella es la principal culpable."


translate spanish day3_main4_87ca6467:


    th "No es que no tuviera nada mejor que hacer que limpiar el desastre de aquí.{w} ¡Es que tengo que hallar las respuestas!"


translate spanish day3_main4_bfb5e7fb:


    "«La verdad está ahí fuera» algo así se me pasó por la cabeza."


translate spanish day3_library_sl_ccebe3fc:


    "La solución vino de la nada."


translate spanish day3_library_sl_9cad1570:


    "Mientras Olga Dmitrievna estaba regañando a Ulyana, me escabullí por la salida, gracias a Dios que estaba cerca."


translate spanish day3_library_sl_2a352a63:


    "Estando fuera, dudé por unos momentos y salí corriendo hacia Electronik."


translate spanish day3_library_sl_f9674fb7:


    "Parecía que estaba trabajando hasta tarde en el club."


translate spanish day3_library_sl_023f2a34:


    el "¿Por qué vas con prisas?"


translate spanish day3_library_sl_6e69e20d:


    me "¿Prisas...?{w} Se me escapa el tren en diez minutos."


translate spanish day3_library_sl_e64f933d:


    el "¿Qué? {w} ¡Ey, espera!"


translate spanish day3_library_sl_d7d54b7f:


    "Las palabras de Electronik se quedaron detrás de mí flotando en el aire."


translate spanish day3_library_sl_1afa7cdc:


    "Salí corriendo hacia la plaza."


translate spanish day3_library_sl_f44b49d9:


    me "No era en definitiva el mejor lugar para esconderse."


translate spanish day3_library_sl_863b1e12:


    "Así es, en un estado de estrés o fatiga, frecuentemente empiezo a hablar conmigo mismo."


translate spanish day3_library_sl_f5dc1ea7:


    "Bueno, en realidad no es hablar sino más bien murmurar bajo mis suspiros."


translate spanish day3_library_sl_6c76fea4:


    "Me ayuda a concetrarme y a encontrar la correcta solución rápidamente."


translate spanish day3_library_sl_2e3c65cd:


    me "Vale, ¿y ahora adónde?{w} ¿Dónde debería ir...?"


translate spanish day3_library_sl_ea2b71fd:


    "El mejor lugar sería aquel en el que haya el menor número de gente."


translate spanish day3_library_sl_bfa9bfb0:


    th "En consecuencia, mi camino estaba en los bosques."


translate spanish day3_library_sl_a088dacb:


    "Medio minuto después, ya estaba relajado, sentado en un tocón de un árbol cerca del bosque."


translate spanish day3_library_sl_caca443f:


    th "¡Eso fue una huida exitosa!"


translate spanish day3_library_sl_15e7cffc:


    th "¡Espero que le enseñen a Ulyana a comportarse! Por supuesto, ¡ya no es una niña!"


translate spanish day3_library_sl_cbda97c3:


    th "Bueno, triste pero cierto...{w} Ya no es una niña..."


translate spanish day3_library_sl_be69861b:


    "Ahora, un pensamiento pasó por mi mente: Podría haber resuelto bastante mal toda la situación."


translate spanish day3_library_sl_fb96d515:


    th "Para ser justos, una parte de la vajilla rota lo estaba por mi culpa."


translate spanish day3_library_sl_e732f41b:


    th "Aun más, uno podría decir que no debí haberme líado en esa pelea desde el principio."


translate spanish day3_library_sl_89141735:


    "Eso me tuvo reflexionando."


translate spanish day3_library_sl_72cbabba:


    sl "¿Cómo está el tiempo?"


translate spanish day3_library_sl_5d2bdfb2:


    "Slavya estaba justo detrás de mí."


translate spanish day3_library_sl_a99618e8:


    th "¿Cómo se las arregló para escabullirse sin que me diera cuenta?{w} Gracias a Dios, no es Olga Dmitrievna."


translate spanish day3_library_sl_84662d7a:


    me "Bien..."


translate spanish day3_library_sl_78884236:


    "Ésa fue la única palabra que supe hallar."


translate spanish day3_library_sl_c9b49156:


    sl "Justo pensé que te encontraría aquí."


translate spanish day3_library_sl_5ac38cf1:


    me "¿Por qué?"


translate spanish day3_library_sl_f1262d21:


    sl "No estoy segura, es sólo..."


translate spanish day3_library_sl_72426581:


    "Ella levantó la mirada hacia el cielo."


translate spanish day3_library_sl_42471a85:


    sl "Hay poca gente por aquí."


translate spanish day3_library_sl_becb13d6:


    th "¡¿Piensa realmente Slavya que tengo algún tipo de fobia social?!{w} Pensándolo bien, en realidad tiene razones para creerlo."


translate spanish day3_library_sl_bf693f95:


    me "¿Y qué estás... haciendo aquí?"


translate spanish day3_library_sl_94fc3843:


    sl "Buscándote."


translate spanish day3_library_sl_9fa90903:


    "Ella volvió a sonreír, pero esta vez su sonrisa parecía un poco distinta. Un poco amistosa, diría."


translate spanish day3_library_sl_8052945a:


    me "¿Yo? ¿Por qué?"


translate spanish day3_library_sl_b57bc8ed:


    sl "¿No lo puedes imaginar?"


translate spanish day3_library_sl_a5d87f4f:


    "Me preparé para soportar un severo sermón."


translate spanish day3_library_sl_e2e2018f:


    sl "Comprendo que te sientas menos culpable que Ulyana."


translate spanish day3_library_sl_66b23847:


    th "¡Me está defendiendo la ayudante de la líder del campamento!{w} Me sentí todavía más patético."


translate spanish day3_library_sl_4fa4d55e:


    th "Maldita sea, compórtate en serio, ¡no estamos jugando aquí al {i}poli malo y al poli bueno{i}!"


translate spanish day3_library_sl_50c80811:


    sl "Pero asumiendo que no vas a ayudarla, me ayudarás a mi."


translate spanish day3_library_sl_d322f1a0:


    "Dicho eso, Slavya miró de nuevo el cielo otra vez."


translate spanish day3_library_sl_7b2c24d7:


    th "Me pregunto qué trata de hallar ahí..."


translate spanish day3_library_sl_a9c6d0dd:


    me "¿Y qué es lo que necesito hacer yo?"


translate spanish day3_library_sl_b90384cf:


    sl "Tenemos que reordenar los libros de la biblioteca."


translate spanish day3_library_sl_28c44327:


    "Eso era mejor que una esclavitud disciplinaria junto a Ulyana.{w} Y de todas formas, era una oferta que no podía rechazar."


translate spanish day3_library_sl_c7aaed10:


    me "Estoy completamente a su servicio, mademoiselle. ¿O madam?"


translate spanish day3_library_sl_ec2c2a7c:


    "Ella se rió."


translate spanish day3_library_sl_e6b3e7b4:


    sl "¡Pues sígueme, monsieur!"


translate spanish day3_library_sl_f4346447:


    "En un par de minutos estábamos en la biblioteca."


translate spanish day3_library_sl_4faed1c0:


    "En todo el camino mantuve la vista alrededor por temor a encontrarme con Olga Dmitrievna."


translate spanish day3_library_sl_e82faf41:


    "Parece que Slavya no se percató de mi preocupación."


translate spanish day3_library_sl_9a6e5488:


    th "Y aun más, es bastante raro que ella no me haya regañado."


translate spanish day3_library_sl_1c2eed77:


    "Bueno, si prosiguiéramos nuestra analogía, Slavya es definitivamente una buen policía."


translate spanish day3_library_sl_f58d39b1:


    "Cuando estuvimos dentro, un reluciente rayo del sol brillaba através de la ventana de la biblioteca, con una miríada de partículas de polvo bailando en los haces de luz."


translate spanish day3_library_sl_35c68aa3:


    "Éste es el polvo de una biblioteca especial con su distintivo olor que nunca puedes confundir con otra cosa."


translate spanish day3_library_sl_1d93103b:


    "Estas piezas microscópicas de colecciones de obras moribundas de los clásicos del Marxismo-Leninismo."


translate spanish day3_library_sl_325f1bcd:


    th "Es interesante, ¿huele el polvo de la estantería de libros de ficción como el polvo de los libros de física o química?"


translate spanish day3_library_sl_826e40a7:


    sl "Comienza por esos estantes."


translate spanish day3_library_sl_17c95df4:


    "Slavya señaló la estantería de libros cerca de la escultura del busto de Lenin."


translate spanish day3_library_sl_46f0983a:


    me "Vale, ¿pero qué debo hacer exactamente?"


translate spanish day3_library_sl_968cfe00:


    sl "Sólo quitar los libros de sus estantes y ponerlos encima del suelo.{w} Les quitaremos el polvo y luego los volveremos a poner."


translate spanish day3_library_sl_6febb73f:


    "Viendo todo este polvo, leer apenas resulta la actividad favorita de los pioneros."


translate spanish day3_library_sl_3a98efa2:


    me "¿Alguna idea de dónde puede estar Zhenya?"


translate spanish day3_library_sl_a1e6dd7a:


    sl "No lo sé, probablemente se fue a algún lado."


translate spanish day3_library_sl_ea719463:


    th "Qué raro."


translate spanish day3_library_sl_a2356fab:


    "Parece que sólo hay dos lugares en el mundo en el que puedes encontrarte a Zhenya... la biblioteca y la cantina."


translate spanish day3_library_sl_ac2cba61:


    "Me puse a la tarea."


translate spanish day3_library_sl_0eb6b971:


    "Creo que nunca he tenido tantos libros a la vez en mis manos en toda mi vida."


translate spanish day3_library_sl_ce83d765:


    "De todas maneras, estoy más acostumbrado a leer de una pantalla de ordenador o de una tablet."


translate spanish day3_library_sl_de4998dd:


    "Primero, porque no hay necesidad de pagar por libros electrónicos, y segundo, no puedes hallar todo en formato impreso."


translate spanish day3_library_sl_77254c81:


    sl "¿Qué tal va?"


translate spanish day3_library_sl_515b792e:


    "Preguntó Slavya, permaneciendo detrás de las estanterías."


translate spanish day3_library_sl_31dbcb74:


    me "Va bien, poco a poco."


translate spanish day3_library_sl_3799372a:


    me "¿Has leído alguno de esos?"


translate spanish day3_library_sl_8f3fa4b1:


    sl "¿Cuáles?"


translate spanish day3_library_sl_5625a91d:


    me "Bueno, sobre el comunismo..."


translate spanish day3_library_sl_d51d54a0:


    sl "Qué va, soy más de libros sobre historia.{w} Y aventuras."


translate spanish day3_library_sl_7f0819cb:


    me "Yo también..."


translate spanish day3_library_sl_a3f8af02:


    "Bueno, la literatura histórica no era exactamente mi favorita, ¡pero en comparación con la lucha de clases del socialismo, eso sí que no era precisamente lo mío!"


translate spanish day3_library_sl_eae58056:


    sl "Ey, no le guardes rencor a Ulyana."


translate spanish day3_library_sl_ec1593ec:


    me "No lo hago..."


translate spanish day3_library_sl_e1c77be8:


    "Así es, sentí que mi ego se recuperó casi del todo tras el incidente en la cantina.{w} Además, me las arreglé para limpiarme de los restos de comida en un lago del bosque."


translate spanish day3_library_sl_ca83718e:


    sl "No lo hace todo por despecho."


translate spanish day3_library_sl_04a51e77:


    me "Por despecho o no, ¡pero debería reflexionar sobre sus propias acciones!{w} Ah, da igual..."


translate spanish day3_library_sl_0f8bf2e5:


    "Seguí caminando entre docenas de libros escritos por gente totalmente desconocida por mi."


translate spanish day3_library_sl_92df4c4a:


    th "Voy a olvidar quiénes son en un minuto, y nunca sabré sobre qué escribieron."


translate spanish day3_library_sl_4012f849:


    th "Ésa es la muerte de muchos escritores... convertirse en un montón de letras abandonadas en una tapa dura en una estantería de una biblioteca en una realidad de un campamento de pioneros que no existe."


translate spanish day3_library_sl_73956111:


    sl "¿Has terminado?"


translate spanish day3_library_sl_f8226aa9:


    me "Claro, eso creo."


translate spanish day3_library_sl_7bfe1a7a:


    sl "Yo también.{w} Vale, hagámoslo de esta manera, les pasaré un trapo y tu te encargas de colocarlos de nuevo en las estanterías."


translate spanish day3_library_sl_f38a2469:


    me "Pero no me acuerdo en qué orden se hallaban antes."


translate spanish day3_library_sl_57b94aaa:


    sl "Da igual. Nadie los lee de todas formas."


translate spanish day3_library_sl_68a9ffa6:


    th "Parece que ella tiene una sonrisa para cada ocasión."


translate spanish day3_library_sl_19451d64:


    "Solamente un par de ocasiones me he dado cuenta de ligeras diferencias, matices de emociones en esa sonrisa... alegría, felicidad, tristeza..."


translate spanish day3_library_sl_853f82b2:


    "Pero estos matices fueron muy evanescentes, no estaba completamente seguro de que no me imaginara cosas."


translate spanish day3_library_sl_5ff6b0ce:


    "Esta vez su sonrisa parecía un poco coqueta."


translate spanish day3_library_sl_1e475060:


    me "Claro, probablemente.{w} Al menos, yo no leería todas estas cosas."


translate spanish day3_library_sl_08cfb744:


    "Slavya estaba quitando el polvo con un paño húmedo, y yo iba poniendo los libros de nuevo en las estanterías."


translate spanish day3_library_sl_3a2c6311:


    "Éramos un buen equipo... nuestra tarea ya estaba casi lista."


translate spanish day3_library_sl_9fdf4842:


    sl "¿Por qué no cogiste nada de los estantes de arriba?"


translate spanish day3_library_sl_4c1d6d10:


    me "¿Los estantes de arriba?"


translate spanish day3_library_sl_f8c41e21:


    "Eché un vistazo."


translate spanish day3_library_sl_cbcf5cb7:


    me "Lo siento, no me he dado cuenta."


translate spanish day3_library_sl_705bea28:


    sl "No pasa nada."


translate spanish day3_library_sl_f9e5b237:


    me "¡Espera un momento!{w} Traeré la silla."


translate spanish day3_library_sl_164801d0:


    "Incluso con mi estatura considerable, no los podía alcanzar."


translate spanish day3_library_sl_5c3aa9a0:


    "Me puse de pie en la silla y procedí en retirar los libros del estante y dárselos a Slavya."


translate spanish day3_library_sl_6f01b19b:


    "Después de un rato, solamente quedaban una docena de libros en la parte más alejada del estante."


translate spanish day3_library_sl_a29261f6:


    "Me pareció que no sería difícil alcanzarlos."


translate spanish day3_library_sl_45a87cde:


    "Siempre me ocurre que prefiero dar un mordisco grande, a dar dos mordiscos.{w} Y hacer dicho mordisco sin mover la silla debajo de mis pies..."


translate spanish day3_library_sl_57e655b2:


    "Resultó ser un error fatal..."


translate spanish day3_library_sl_3b6da452:


    "Abrí mis ojos, me encontraba encima de Slavya."


translate spanish day3_library_sl_64eccf44:


    me "¡¿Estás...?! ¡¿Estoy...?! ¡¿Estás bien?!"


translate spanish day3_library_sl_df45d6bc:


    "Tuve un poco de miedo."


translate spanish day3_library_sl_9f4025f7:


    th "A pesar de que sólo tengo diecisiete años ahora, ¡caerse de una silla encima de una muchacha ya es otra historia!"


translate spanish day3_library_sl_0451d1e4:


    sl "Estoy bien."


translate spanish day3_library_sl_55c4c14d:


    "Su rostro sólo estaba a unos centímetros del mío."


translate spanish day3_library_sl_f3139a0d:


    me "¿Te has roto algo?"


translate spanish day3_library_sl_3f137cac:


    sl "No creo."


translate spanish day3_library_sl_673a4231:


    sl "Te has vuelto un doble de especialista de riesgos bastante pésimo."


translate spanish day3_library_sl_ce3666e5:


    "Ella sonrió."


translate spanish day3_library_sl_e2d2475f:


    me "Apuesto a que sí."


translate spanish day3_library_sl_e10fbc50:


    "La observé en sus ojos.{w} Tan sólo la miré."


translate spanish day3_library_sl_382be24f:


    "No estaba seguro de qué debería decirle...{w} Y Slavya se quedó dudando o bien me dejó la iniciativa."


translate spanish day3_library_sl_f0551a4a:


    th "A este paso algo podría llegar a suceder..."


translate spanish day3_library_sl_5ff7b840:


    th "¡Sus labios estaban tan cerca de los míos!"


translate spanish day3_library_sl_44711d46:


    "El natural deseo comenzó a despertar en mi cuerpo, ¿pero era lo {i}correcto{/i}?"


translate spanish day3_library_sl_f481ef93:


    "Tenía el deseo de continuar, pero me veía incapaz de hacerlo."


translate spanish day3_library_sl_ef3ea7a1:


    "Slavya sólo estaba permaneciendo en silencio y mirándome fijamente a los ojos."


translate spanish day3_library_sl_5ab0d3a1:


    "De repente, escuché la campana sonando vagamente."


translate spanish day3_library_sl_8e8e0763:


    sl "Es la hora de cenar."


translate spanish day3_library_sl_751ac511:


    me "Claro..."


translate spanish day3_library_sl_d8eca9e9:


    sl "¿Deberíamos ir?{w} ¿O deberíamos estar así un rato más?"


translate spanish day3_library_sl_e4a6b1bd:


    "Ella sonrió otra vez y esta vez su sonrisa me pareció atractiva, como si ella quisiera darme a entender algo."


translate spanish day3_library_sl_050169d2:


    me "Vayámonos..."


translate spanish day3_library_sl_19989263:


    "No me moví ni un centímetro."


translate spanish day3_library_sl_0f8c2844:


    sl "Bueno, para irnos, primero tendrás que levantarte."


translate spanish day3_library_sl_85a7ebd8:


    me "Obviamente..."


translate spanish day3_library_sl_e6991c34:


    "Sentí como si una fuerza invisible me estuviera reteniendo en mi lugar."


translate spanish day3_library_sl_8f6ef867:


    "Slavya parece que reconoció eso y, con mucho cuidado, se deslizó debajo de mi."


translate spanish day3_library_sl_c0e00208:


    sl "¿Vas a seguir permaneciendo ahí?"


translate spanish day3_library_sl_ec2c2a7c_1:


    "Sonrió."


translate spanish day3_library_sl_491711dd:


    "Llegado a este punto finalmente recuperé mis sentidos y me puse de pie."


translate spanish day3_library_sl_8a8c94c1:


    me "Lo siento..."


translate spanish day3_library_sl_a707867b:


    sl "No pasa nada."


translate spanish day3_library_sl_cd4fe72c:


    sl "Pero no hemos terminado con todo.{w} Tendremos que limpiar lo demás más tarde."


translate spanish day3_library_sl_85279b68:


    me "Claro, por supuesto."


translate spanish day3_library_sl_92690f30:


    "Estuve en silencio todo el camino de vuelta a la cantina."


translate spanish day3_library_sl_f1edeb24:


    "Slavya divagó sobre su vida de cada día en el campamento, pero puesto que era claramente un monólogo, no le presté mucha atención."


translate spanish day3_library_sl_d67f2f47:


    "Al llegar a la cantina, vi a Olga Dmitrievna en el porche."


translate spanish day3_library_sl_2c96d9df:


    mt "¿Me quieres dar alguna explicación, jovencito?"


translate spanish day3_library_sl_c3fe9d67:


    "Ahora recordé que he olvidado alguna cosa..."


translate spanish day3_library_sl_5cb822ee:


    me "Nop..."


translate spanish day3_library_sl_18279173:


    mt "¿Qué tal fue tu deber con la limpieza?"


translate spanish day3_library_sl_65bf82fc:


    me "Ehm, bastante bien..."


translate spanish day3_library_sl_09a04ec4:


    mt "A pesar de eso Ulyana me dijo que no fue así."


translate spanish day3_library_sl_3074c0ad:


    me "¡Es todo culpa suya!"


translate spanish day3_library_sl_6fc0750c:


    mt "¿Y qué te dije yo a ti?"


translate spanish day3_library_sl_6ced5f4c:


    mt "¡Os ordené que limpiarais juntos!{w} ¡¿Y qué estabas haciendo?!"


translate spanish day3_library_sl_d2aa1636:


    "No sabía qué decir, a pesar de todo ella tenía razón."


translate spanish day3_library_sl_418c13dd:


    sl "¡No le regañes!{w} Semyon estaba ayudándome en la biblioteca."


translate spanish day3_library_sl_38689622:


    mt "Oh, ¿en serio?"


translate spanish day3_library_sl_7314bee2:


    sl "¡Claro!"


translate spanish day3_library_sl_bc20eb0b:


    mt "Bueno, todo eso está muy bien y tal pero...{w} Pero resumiendo, ¡esta noche no vas a cenar!"


translate spanish day3_library_sl_a2911372:


    me "¡¿Cómo es eso?! ¡¿Por qué?!"


translate spanish day3_library_sl_902a9a2f:


    "Protesté."


translate spanish day3_library_sl_5ec7a533:


    "Sin embargo, mi queja se parecía más a una petición para poder comer en una prisión... atrevida y temeraria, pero aun así estúpida y contraproducente."


translate spanish day3_library_sl_b77f1870:


    mt "¡Espero que así aprendas una lección para hacerme caso!"


translate spanish day3_library_sl_fcc1c309:


    "En ese instante, Olga Dmitrievna se parecía más a un antiguo comandante de la legión romana que a una pequeña líder de campamento."


translate spanish day3_library_sl_5bdcf334:


    "No tenía ni idea de cómo rebatirla."


translate spanish day3_library_sl_ae8c4442:


    sl "¡Perdónale!{w} ¡Y hazme a mi como responsable!"


translate spanish day3_library_sl_d4764288:


    "La líder del campamento le llevó un tiempo reflexionar sobre ello."


translate spanish day3_library_sl_777093a7:


    mt "¡Está bien, adelante!"


translate spanish day3_library_sl_309686e8:


    sl "¡Gracias!"


translate spanish day3_library_sl_73aab3cd:


    me "Gracias..."


translate spanish day3_library_sl_34c73a83:


    "Nos dimos prisa en entrar."


translate spanish day3_library_sl_4df43400:


    sl "Bueno, voy a ir con las muchachas, ya habíamos quedado antes.{w} ¡Cuídate!"


translate spanish day3_library_sl_3ec70e0f:


    "Ella sonrió y se despidió con la mano."


translate spanish day3_library_sl_99d3acba:


    "En serio, ¡¿qué haría sin ella!?"


translate spanish day3_library_sl_0eba76f0:


    "No había ningún asiento libre cerca de Slavya y, además, no conocía a sus amigas."


translate spanish day3_cleaning_us_99ef151c:


    "Qué va, salir corriendo no es ni de buen trozo una buena idea."


translate spanish day3_cleaning_us_93c60cc4:


    "Ante todo, Olga Dmitrievna ya me ha pillado, y salir corriendo solamente agravaría mi castigo."


translate spanish day3_cleaning_us_394c9d9e:


    "Así como que, después de todo, en parte era culpable..."


translate spanish day3_cleaning_us_2f94b8f1:


    th "¡Claro que todo era culpa suya!"


translate spanish day3_cleaning_us_25f82315:


    "Pero si no hubiera reaccionado de esa forma, probablemente podríamos haber evitado esa pelea."


translate spanish day3_cleaning_us_996e7fec:


    th "Probablemente..."


translate spanish day3_cleaning_us_3e5b8732:


    "He abierto el armario de la despensa de la limpieza para coger una escoba, una fregona y un paño de limpieza."


translate spanish day3_cleaning_us_f469532f:


    "Olga Dmitrievna ya no estaba cerca de Ulyana cuando llegué."


translate spanish day3_cleaning_us_24ab10f2:


    me "¿Se ha ido?"


translate spanish day3_cleaning_us_7e694dd0:


    us "¿No lo puedes ver por ti mismo?"


translate spanish day3_cleaning_us_afe5cb8e:


    "Ulyana se veía enfadada, con todo su espíritu de juventud desvanecido sin rastro alguno."


translate spanish day3_cleaning_us_3b4a6242:


    me "Vale, espera un segundo, voy a ir primero a limpiarme un poco."


translate spanish day3_cleaning_us_a464805b:


    "Le eché una mirada enojado y me dirigí hacia la salida."


translate spanish day3_cleaning_us_80aa159b:


    "Me lavé los restos de comida y retorné a la cantina."


translate spanish day3_cleaning_us_8bf25c46:


    me "Bueno, no hay salida... tenemos que limpiarlo."


translate spanish day3_cleaning_us_0713a7b2:


    us "¡Todo por tu culpa!"


translate spanish day3_cleaning_us_97c9d07e:


    "Solamente me bastaba una mirada suya para darme escalofríos."


translate spanish day3_cleaning_us_771aa502:


    me "¡Por supuesto!"


translate spanish day3_cleaning_us_12685598:


    me "¡Soy yo quien tiene la culpa de todo!{w} Soy yo el único que supone un desastre natural aquí."


translate spanish day3_cleaning_us_005b5a5d:


    us "Oh, cállate..."


translate spanish day3_cleaning_us_abd7f53b:


    "Aun así, es muy extraño que ella no esté tratando de evitar este deber de limpieza."


translate spanish day3_cleaning_us_87e97c80:


    "Ulyana tuvo una oportunidad muy buena para escaparse corriendo y dejarme solo, pero por alguna razón hizo exactamente lo contrario, recogiendo diligentemente los pedazos de la vajilla rota, fregando el suelo y recogiendo las sillas y mesas."


translate spanish day3_cleaning_us_ad056558:


    "De hecho, iba tan rápida que me resultaba difícil seguirle el ritmo."


translate spanish day3_cleaning_us_a3e4be71:


    me "Ahora no te comportes de repente como una santa."


translate spanish day3_cleaning_us_a9babbd5:


    us "Es que no quisiera pasarme todo el día aquí, ¡idiota!"


translate spanish day3_cleaning_us_9fc103c6:


    "Todavía seguía pareciendo algo irritada."


translate spanish day3_cleaning_us_19fa1fbf:


    me "Escucha, tienes que entender que uno no puede comportarse de esa forma...{w} Al menos hasta cierto punto..."


translate spanish day3_cleaning_us_fbfed032:


    "De alguna manera pensé en tener algún tipo de conversa moralista con ella."


translate spanish day3_cleaning_us_e142a9de:


    us "¡No hice nada!{w} Eras tú quien me estaba tratando como una cría."


translate spanish day3_cleaning_us_4f354c21:


    "Ulyana agarró un cubo con una fregona y se alejó hacia un rincón de la cantina."


translate spanish day3_cleaning_us_d71063b0:


    th "Parece que aun sigue enfadada."


translate spanish day3_cleaning_us_b32f5157:


    "Viendo la montaña de vajilla rota detrás de mí, finalmente comprendí la magnitud de la catástrofe."


translate spanish day3_cleaning_us_7300ad57:


    th "Tenemos suerte de que al menos los tenedores y las cucharas son de metal... al menos tenemos algo con lo que comer.{w} Pero apenas quedan ya platos..."


translate spanish day3_cleaning_us_e66e1571:


    us "Ey..."


translate spanish day3_cleaning_us_81282f69:


    "Ulyana me llamó con un grito."


translate spanish day3_cleaning_us_cbdad383:


    "Me acerqué a ella."


translate spanish day3_cleaning_us_557a9104:


    us "No lo entiendo, ¿por qué me odias tanto?"


translate spanish day3_cleaning_us_8e1a6421:


    "Su rostro se volvió tan serio que estaba preparado para creerme que no era otra de sus trampas."


translate spanish day3_cleaning_us_dffd29c3:


    me "¿Qué te hace pensar eso?"


translate spanish day3_cleaning_us_f0847682:


    us "No tengo ni idea, por eso te lo pregunto a ti."


translate spanish day3_cleaning_us_e5f99c8b:


    me "No te odio.{w} Es sólo que a veces te comportas como...{w} Bueno, ya sabes."


translate spanish day3_cleaning_us_8c306ba5:


    "Tan sencillo como eso, y aun así una verdad."


translate spanish day3_cleaning_us_a00845ac:


    us "¿Como qué?{w} No tengo ni idea."


translate spanish day3_cleaning_us_cc439301:


    "Alzó su mirada, atravesándome con su curiosidad."


translate spanish day3_cleaning_us_4470295b:


    me "Bueno, para empezar, ¿por qué tuviste la genial idea de tirarme encima la compota?"


translate spanish day3_cleaning_us_c656e178:


    us "Lo estabas pidiendo a gritos."


translate spanish day3_cleaning_us_65b7d3f3:


    "Ella sonrió por primera vez desde que comenzamos la limpieza."


translate spanish day3_cleaning_us_2bb47ab4:


    me "Claro, exactamente..."


translate spanish day3_cleaning_us_47d09bdb:


    "Expiré un profundo suspiro."


translate spanish day3_cleaning_us_0621ba40:


    me "Entonces, ¿qué es lo que te esperas que hagan los demás?"


translate spanish day3_cleaning_us_ff47fe7e:


    us "Nada."


translate spanish day3_cleaning_us_2f6a59ef:


    "Respondió ácidamente."


translate spanish day3_cleaning_us_8e2b133b:


    "Esta conversación ya estaba acabada, por lo que proseguí limpiando en silencio."


translate spanish day3_cleaning_us_a20cefa7:


    "..."


translate spanish day3_cleaning_us_86688e12:


    "Nos llevó unas pocas horas arreglar la cantina."


translate spanish day3_cleaning_us_0a8d709c:


    "Al final, toda la vajilla rota se recogió. Las sillas y las mesas estaban en su lugar correspondiente, el suelo se veía limpio."


translate spanish day3_cleaning_us_d6447ba7:


    "Estaba sentado junto a Ulyana cerca del mostrador de comida, respirando profundamente y merecidamente descansando."


translate spanish day3_cleaning_us_a4eb05ec:


    me "Ahora ya ves cuánto esfuerzo hay que poner debido a una broma pesada."


translate spanish day3_cleaning_us_d9f878c6:


    us "¡Pero no estoy cansada del todo!"


translate spanish day3_cleaning_us_fc386561:


    "A pesar de que el sudor que le chorreaba por su cara decía lo contrario."


translate spanish day3_cleaning_us_8e0db1a9:


    me "Oh bueno, está bien para ti..."


translate spanish day3_cleaning_us_926c6d27:


    us "Y ahora, ¿qué es lo siguiente que vamos a hacer?"


translate spanish day3_cleaning_us_2a831701:


    me "No tengo ni idea de ti, pero yo mejor que me vaya..."


translate spanish day3_cleaning_us_2fe78b26:


    us "¡Nop! ¡Eso no es así!"


translate spanish day3_cleaning_us_86e9c04d:


    us "Tienes que..."


translate spanish day3_cleaning_us_6e6e30b3:


    "Ella dudó."


translate spanish day3_cleaning_us_cbab93c7:


    us "¡Me tienes que ayudar con otra pequeña cosa!"


translate spanish day3_cleaning_us_341c6bf4:


    me "¿Pensando en otra estúpida broma?"


translate spanish day3_cleaning_us_7862b445:


    us "¡Apuesta a que sí!"


translate spanish day3_cleaning_us_6d77584d:


    "Estaba sonriendo ampliamente."


translate spanish day3_cleaning_us_b72aa87d:


    me "No soy tu hombre.{w} Hoy ya he tenido suficiente con un solo castigo."


translate spanish day3_cleaning_us_005a932e:


    us "¡Vale, ahí va el trato!{w} Si me ayudas ahora, ¡no habrá ninguna broma más!"


translate spanish day3_cleaning_us_d5e744c0:


    "Está claro que era una carta de invitación, pero de alguna manera no podía acabar de creerme a Ulyana ni por un momento."


translate spanish day3_cleaning_us_24184228:


    th "Bueno, no hace daño preguntar."


translate spanish day3_cleaning_us_72d07964:


    me "¿Y cuál es tu ingenioso plan?"


translate spanish day3_cleaning_us_8e695095:


    us "¡Vamos a robar los caramelos!"


translate spanish day3_cleaning_us_3d342d5a:


    me "¡¿Qué?!"


translate spanish day3_cleaning_us_cce1c4dc:


    "Debía haberme esperado algo como eso."


translate spanish day3_cleaning_us_82811001:


    th "Los caramelos son para los críos..."


translate spanish day3_cleaning_us_f544383f:


    us "Pronto el cocinero saldrá a tirar la basura, ¡nadie nos verá!"


translate spanish day3_cleaning_us_df96cac2:


    me "¡No cuentes conmigo!"


translate spanish day3_cleaning_us_44f47ba7:


    us "¡Ah, da igual!"


translate spanish day3_cleaning_us_99aa653e:


    "Gruñó y se dio la vuelta."


translate spanish day3_cleaning_us_1838e844:


    us "¡Pues lo haré por mi misma!"


translate spanish day3_cleaning_us_821e8719:


    me "No te dejaré..."


translate spanish day3_cleaning_us_e1f72ec9:


    "No pude terminar de decir mis palabras que Ulyana ya había saltado encima del mostrador, salió corriendo a la despensa, abrió la puerta y empezó a hurgar en ella."


translate spanish day3_cleaning_us_e182607e:


    me "Ey, ¡para ya!{w} ¡Parece que no hayas tenido suficientes problemas ya con Olga Dmitrievna!"


translate spanish day3_cleaning_us_6586cba7:


    "No me contestó."


translate spanish day3_cleaning_us_fdb724d1:


    me "No te librarás de algo así simplemente con la obligación de limpiar."


translate spanish day3_cleaning_us_2992696a:


    "Ulyana cerró la despensa.{w} Sostenía una bolsa de caramelos en sus manos."


translate spanish day3_cleaning_us_244f004d:


    me "¡Eh, enana, déjalos en su lugar!"


translate spanish day3_cleaning_us_5cc17f12:


    "Me enseñó su lengua y salió corriendo por la puerta de atrás."


translate spanish day3_cleaning_us_8d50913c:


    "¡No podía dejar que se escapara sin más!{w} Salí corriendo como un rayo persiguiéndola."


translate spanish day3_cleaning_us_499e4041:


    "Está claro, la muchacha tenía gran ventaja, pero aun así saqué todas mis fuerzas en perseguirla."


translate spanish day3_cleaning_us_0ab77e34:


    th "¡No voy a perderla otra vez!"


translate spanish day3_cleaning_us_a0344103:


    "Corrimos por toda la plaza..."


translate spanish day3_cleaning_us_971a61ea:


    "Dirigimos nuestros pasos al edificio del club de música..."


translate spanish day3_cleaning_us_c8e8d113:


    "Y llegamos al sendero del bosque."


translate spanish day3_cleaning_us_596c2686:


    "Ya casi tenía alcanzada a Ulyana, cuando se paró abrúptamente..."


translate spanish day3_cleaning_us_13e17fb4:


    "No podía detenerme de golpe, por lo que me choqué con ella, cayendo ambos al suelo."


translate spanish day3_cleaning_us_31ba2a96:


    "Rodamos por la hierba..."


translate spanish day3_cleaning_us_9cd22020:


    me "¡Te atrapé!"


translate spanish day3_cleaning_us_7aeb2243:


    "Di un grito de victoria."


translate spanish day3_cleaning_us_c67ac3de:


    us "No lo hiciste..."


translate spanish day3_cleaning_us_ed702531:


    "Respondió con un tono de vergüenza."


translate spanish day3_cleaning_us_0ec6d3ef:


    "Ulyana estaba debajo de mi."


translate spanish day3_cleaning_us_92dbd985:


    "Su cara estaba justo al lado de la mía."


translate spanish day3_cleaning_us_cb6f36c0:


    "Sentía su aliento y el calor de su cuerpo."


translate spanish day3_cleaning_us_57515ee8:


    th "Claro que ahora tan solo es una niña, pero pronto se volverá una mujer."


translate spanish day3_cleaning_us_b08ceff8:


    "Era una situación bastante incómoda."


translate spanish day3_cleaning_us_c19988a4:


    us "¿Vas a violarme?"


translate spanish day3_cleaning_us_436a485d:


    "Dijo volviendo a su juicio."


translate spanish day3_cleaning_us_0c857a98:


    me "¿Es lo que quieres?"


translate spanish day3_cleaning_us_b4df4200:


    "En cualquier caso, era más bien un juego para ella."


translate spanish day3_cleaning_us_0fca62ac:


    us "¡Claro que sí!"


translate spanish day3_cleaning_us_0f58246c:


    "Ella hizo una sonrisa maliciosa y resopló de risa tranquilamente.{w} O fue tan solamente mi imaginación."


translate spanish day3_cleaning_us_568b5895:


    me "No estoy realmente de humor..."


translate spanish day3_cleaning_us_44f47ba7_1:


    us "¡Ah, da igual!"


translate spanish day3_cleaning_us_bd670e0e:


    "Ulyana se acercó más y me mordió en la nariz."


translate spanish day3_cleaning_us_b47815ea:


    "No me esperaba eso e incluso me aparté un poco."


translate spanish day3_cleaning_us_4e8535ee:


    "Solamente unos momentos de duda fueron suficientes para que ella arrancara a correr alejándose unas docenas de pasos."


translate spanish day3_cleaning_us_28d12718:


    us "¡Mira!{w} ¡Lo lamentarás luego!"


translate spanish day3_cleaning_us_5cabe167:


    "Ella se rio a carcajadas y desapareció entre el bosque."


translate spanish day3_cleaning_us_780778e4:


    "La bolsa de caramelos la dejó en el suelo cerca de mí."


translate spanish day3_cleaning_us_6e19bf3f:


    th "Me pregunto si la soltó a propósito..."


translate spanish day3_cleaning_us_11007c59:


    "Ya debe ser casi la hora de cenar, por lo que tengo que darme prisa en devolver los caramelos."


translate spanish day3_cleaning_us_ff6853b7:


    "Y preferiblemente (muy preferiblemente) estar muy alerta."


translate spanish day3_cleaning_us_f7b4ae2b:


    "Por supuesto, podría explicar la situación... que era Ulyana quien los había robado..."


translate spanish day3_cleaning_us_e4f68848:


    th "¿Pero quién iba a creerme?"


translate spanish day3_cleaning_us_492cba74:


    "Olga Dmitrievna ya estaba esperándome en la entrada de la cantina..."


translate spanish day3_cleaning_us_2299f5be:


    mt "¡Buen trabajo, Semyon!"


translate spanish day3_cleaning_us_fb544be4:


    me "¿En qué?"


translate spanish day3_cleaning_us_1ed8ab85:


    "Escondí la bolsa de dulces detrás de mí."


translate spanish day3_cleaning_us_9d2438b5:


    "La bolsa era transparente, y demasiado grande para guardarla en los bolsillos."


translate spanish day3_cleaning_us_7746de39:


    mt "Me refería a la limpieza. Todo está limpio y ordenado."


translate spanish day3_cleaning_us_59d93851:


    me "Ah, sí..."


translate spanish day3_cleaning_us_a8ce4ea0:


    mt "¿Dónde está Ulyana?"


translate spanish day3_cleaning_us_f7c4eac6:


    th "Ojalá lo supiera."


translate spanish day3_cleaning_us_c7337a6d:


    me "Ella... vendrá pronto..."


translate spanish day3_cleaning_us_881d262a:


    mt "Vale pues. Ve y cena."


translate spanish day3_cleaning_us_ad1ee043:


    "Entré en la cantina."


translate spanish day3_cleaning_us_5a56cd46:


    "No era una gran sorpresa encontrármela llena."


translate spanish day3_cleaning_us_0a6a1ce5:


    "Ahora en verdad no sabía cómo devolver la bolsa sin ser descubierto."


translate spanish day3_cleaning_us_4bcefbd3:


    th "Ciertamente, puedo hacerlo por la noche, ¡¿pero qué hago ahora con ella?!"


translate spanish day3_cleaning_us_2a454c26:


    sl "¡Semyon!"


translate spanish day3_cleaning_us_71e7861c:


    "Me giré. Slavya estaba frente a mi."


translate spanish day3_cleaning_us_da29ffa7:


    sl "Guau, ¿qué es esto?"


translate spanish day3_cleaning_us_6651226b:


    "Ella se fijó en la bolsa que no supe esconder a tiempo."


translate spanish day3_cleaning_us_5a9cf61b:


    th "¡Atrapado in fraganti, o más bien atrapado con los caramelos! Mejor estar preparados para ser regañados."


translate spanish day3_cleaning_us_1a98bf30:


    me "Estos son... {w} dulces..."


translate spanish day3_cleaning_us_81dfe51a:


    sl "¿De dónde?"


translate spanish day3_cleaning_us_d9ec0a9f:


    th "Los he hurtado, maldita sea..."


translate spanish day3_cleaning_us_934cbef8:


    me "Los conseguí de Ulyana."


translate spanish day3_cleaning_us_320366d1:


    sl "Oh ya veo. El mismo perro viejo..."


translate spanish day3_cleaning_us_5d6eebdb:


    me "¿Qué quieres decir?"


translate spanish day3_cleaning_us_63f30cbb:


    sl "No es la primera vez que ella hurta los dulces."


translate spanish day3_cleaning_us_11ef631b:


    th "¿Y por qué no me sorprende?"


translate spanish day3_cleaning_us_ee1d96ff:


    sl "Deja que me ocupe de esto."


translate spanish day3_cleaning_us_73aab3cd:


    me "Gracias..."


translate spanish day3_cleaning_us_03e8e491:


    "Slavya me salvó el pellejo una vez más."


translate spanish day3_cleaning_us_1751857b:


    "Cogió la bolsa y se dirigió al bufé."


translate spanish day3_cleaning_us_574fb3bf:


    "No tenía ni un ápice de ganas de escuchar lo que ella me iba a decir después de retornar la bolsa de caramelos. Así que empecé a mirar alrededor de la cantina para buscar un lugar en el que sentarme."


translate spanish day3_main5_144331d6:


    "Parecía que iba a compartir mesa para la cena con Electronik y Shurik."


translate spanish day3_main5_967e239c:


    "No había muchos más lugares donde sentarse."


translate spanish day3_main5_67af4832:


    me "¡¿Qué ocurre caballeros?!"


translate spanish day3_main5_cc50a49a:


    "Cada vez que tenía que tratar con ellos, tenía unas enormes ganas de burlarme de ellos o al menos soltarles algo impertinente o arrogante."


translate spanish day3_main5_887a8a04:


    "Me estaría arriesgando demasiado al comportarme de esa manera, pero los {i}hermanos electrónicos{/i} eran mi principal fuente de emociones positivas."


translate spanish day3_main5_1d386f5f:


    me "¿Qué tal os va?"


translate spanish day3_main5_489d4411:


    el "Bien. ¿Y tú?"


translate spanish day3_main5_7cf58fb3:


    me "Ni bien ni mal."


translate spanish day3_main5_2e8cf482:


    sh "¿Ha sucedido algo?"


translate spanish day3_main5_ad9962cd:


    me "Muchas cosas."


translate spanish day3_main5_a4a08fa3:


    el "¿Lo compartes?"


translate spanish day3_main5_b216e161:


    me "Qué va, en otro momento tal vez."


translate spanish day3_main5_44ead865:


    sh "Como desees."


translate spanish day3_main5_b491ceac:


    "Hizo un gesto de indiferencia."


translate spanish day3_main5_3ca98c02:


    el "¡Vamos a ir al baile después de cenar!"


translate spanish day3_main5_b528275f:


    "Electronik soltó una risita."


translate spanish day3_main5_e1a72042:


    me "Lo sé."


translate spanish day3_main5_d1f8823e:


    el "¿A quién invitarás?"


translate spanish day3_main5_7b2a4bde:


    me "Aun no he pensado en ello.{w} ¿Y tú?"


translate spanish day3_main5_7d7e9876:


    el "Yo... Bueno, yo..."


translate spanish day3_main5_94a58e2f:


    "Pareció que dicha pregunta le cogió con la guardia baja."


translate spanish day3_main5_7607ce16:


    me "Pídeselo a Ulyana. Eso la haría feliz."


translate spanish day3_main5_8da8904a:


    el "¡No, gracias!"


translate spanish day3_main5_5753b37f:


    "Electronik agitó sus manos furiosamente."


translate spanish day3_main5_5fe023fb:


    me "Y tú, Shurik, le pedirás a Alisa salir contigo."


translate spanish day3_main5_e778b8e0:


    sh "Gracias, mejor me abstengo."


translate spanish day3_main5_1d41d511:


    "Parecía más tranquilo que su compañero."


translate spanish day3_main5_246c6e50:


    me "¡Oh, vamos, muchachos, sería divertido!"


translate spanish day3_main5_29c157fd:


    el "En cualquier caso...{w} ¡Tenemos cosas que hacer!{w} ¡Todavía tenemos que terminar el robot!"


translate spanish day3_main5_41e8d12f:


    me "¡Oh, esa es una maravillosa idea!{w} ¡Pídeselo a tu robot! ¿Puede bailar?"


translate spanish day3_main5_9eb621e9:


    sh "Aun no puede caminar."


translate spanish day3_main5_067ae991:


    "Shurik probablemente no lo captó."


translate spanish day3_main5_04d8b901:


    el "¡Por qué no, sería una gran demostración de nuestros logros frente al campamento!"


translate spanish day3_main5_345dabaf:


    sh "¿Y qué les enseñaríamos?"


translate spanish day3_main5_69ce28cd:


    el "Ah, claro, tienes razón..."


translate spanish day3_main5_b54d08ef:


    "Ambos se quedaron quietos mirando sus platos decepcionadamente."


translate spanish day3_main5_a20cefa7:


    "..."


translate spanish day3_main5_a20cefa7_1:


    "..."


translate spanish day3_main5_a20cefa7_2:


    "..."


translate spanish day3_main5_3f89d2ed:


    "La cena se terminó y los pioneros comenzaron a dispersarse."


translate spanish day3_main5_ad0e1b9c:


    me "¿Qué vestiréis para el baile?"


translate spanish day3_main5_83b23459:


    "Les pregunté a Shurik y a Electronik."


translate spanish day3_main5_3b3ff830:


    el "No tenemos nada especial que vestir. Iremos tal cual estamos."


translate spanish day3_main5_4bca7d01:


    "Señaló a su uniforme de pionero."


translate spanish day3_main5_51acbdbc:


    th "No parecían preocuparse mucho por su aspecto."


translate spanish day3_main5_46388e2b:


    th "¡Por qué debería preocuparme pues!"


translate spanish day3_main5_a2a4c2bc:


    th "Vestir ropa de invierno no era una opción, por lo que iré tal cual estoy ahora."


translate spanish day3_main5_0058648c:


    me "¿Cuándo empieza?"


translate spanish day3_main5_129ca123:


    el "A partir de las nueve en punto."


translate spanish day3_main5_855db9ee:


    me "Vale."


translate spanish day3_main5_a8b4968b:


    "Abandoné la cantina y respiré profundamente un aire fresco nocturno."


translate spanish day3_main5_52c76225:


    "Recordé aquellos discos raros que normalmente solía escuchar al ir a la escuela."


translate spanish day3_main5_b1cb0198:


    "Dudas, inseguridad e incluso miedo..."


translate spanish day3_main5_68f4a1be:


    "No podía bailar, no sabía cómo reaccionar si alguien me invitara, tampoco me molestaría en invitar a alguien."


translate spanish day3_main5_c972d742:


    "Después de todo, me sentía bastante incómodo con ello."


translate spanish day3_main5_742d0473:


    "Y era aun más molesto ver a otros cómo se divertían."


translate spanish day3_main5_fa8fda07:


    "No, no les envidiaba.{w} Era más bien algo como un sentimiento brusco que la gente pudiera disfrutar de algo que me era tan extraño."


translate spanish day3_main5_1ecbd57e:


    "Pensé que todavía faltaba mucho tiempo hasta que llegara la hora de la fiesta de baile, por lo que podría dormir un poco para estar más descansado por la noche."


translate spanish day3_main5_629eed15:


    "Al llegar a la cabaña de la líder del campamento, me dejé caer en la cama inmediatamente y cerré mis ojos."


translate spanish day3_main5_a20cefa7_3:


    "..."


translate spanish day3_main5_f7195c18:


    "Qué extraño, me he despertado justo a tiempo incluso sin el despertador."


translate spanish day3_main5_da045b54:


    "El reloj indicaba las nueve."


translate spanish day3_main5_2da36184:


    "Mira que es una cosa rara que ocurra eso."


translate spanish day3_main5_97bec1c5:


    "Aunque me sentía adormecido."


translate spanish day3_main5_dab6de06:


    th "Probablemente no era la mejor idea dormir durante el día después de todo."


translate spanish day3_main5_48355910:


    "Voy a ir a ver a Alisa en vez de ir a la fiesta de baile."


translate spanish day3_main5_d91422b8:


    "Así que es la hora pues."


translate spanish day3_main5_abd3bafa:


    "Pero también quedé de acuerdo con Lena para ayudarla..."


translate spanish day3_main5_f2246ca0:


    th "¿Qué debería hacer ahora?"


translate spanish day3_main5_5dd208f8:


    "Bueno, tengo que irme."


translate spanish day3_main5_a74ee93a:


    "En un par de minutos estaba en la plaza."


translate spanish day3_main5_c5292350:


    "Los altavoces y algún tipo de equipo de DJ estaban instalados cerca del monumento y los árboles estaban decorados con guirnaldas de luces."


translate spanish day3_main5_bfae52bf:


    th "Ah, una fiesta de baile popular típica."


translate spanish day3_main5_b71fb061:


    "Había muchos pioneros alrededor, pero ni una cara familiar, así que me senté en el banco y esperé."


translate spanish day3_main5_bf88823d:


    th "Después de todo, no tengo porqué bailar."


translate spanish day3_main5_ba5eaafc:


    "Quizá pueda simplemente sentarme y hablar con alguien."


translate spanish day3_main5_f62bc905:


    us "Ey, ¿y esa cara triste?"


translate spanish day3_main5_c9bf617b:


    "Ulyana."


translate spanish day3_main5_b9767350:


    me "¿Estás aquí para sugerirme algo?"


translate spanish day3_main5_ab2952f4:


    us "¡Venga ya, vamos a bailar!"


translate spanish day3_main5_9528c3cd:


    me "Es demasiado pronto...{w} Si no hay ni música todavía."


translate spanish day3_main5_0225548f:


    us "Bla bla bla, ¡eres muy aburrido!"


translate spanish day3_main5_386b2a74:


    th "Claro, no soy la persona más alegre para semejante evento."


translate spanish day3_main5_7ad70676:


    "Ella se fue corriendo."


translate spanish day3_main5_a11f2300:


    th "Así que Ulyana vestía algo parecido a un vestido de noche.{w} Qué divertido."


translate spanish day3_main5_9104ac12:


    sl "¡Hola!"


translate spanish day3_main5_3c7474d6:


    "Slavya."


translate spanish day3_main5_a87c71d7:


    me "Ey."


translate spanish day3_main5_81c4dbcb:


    "Se sentó a mi lado."


translate spanish day3_main5_c2bffcd7:


    sl "¿Qué tal va la noche?"


translate spanish day3_main5_e7ce88cd:


    me "Bien."


translate spanish day3_main5_0d67c838:


    sl "¿Por qué estás tan triste?"


translate spanish day3_main5_b97e3fd5:


    me "No lo estoy..."


translate spanish day3_main5_795edb64:


    sl "Vale. ¡Bailar seguro que te animará!"


translate spanish day3_main5_907b6d20:


    me "Quizás..."


translate spanish day3_main5_0981716b:


    sl "¡No te olvides de tener un baile conmigo!"


translate spanish day3_main5_5fc53cc4:


    "Ella se rio y se fue con prisas hacia el equipo musical."


translate spanish day3_main5_aedce5d2:


    "La situación se estaba volviendo grave. No podía quedarme sentado durante toda la fiesta de baile."


translate spanish day3_main5_bd946546:


    un "Hola."


translate spanish day3_main5_d106dd84:


    "Lena se acercó."


translate spanish day3_main5_8bd25464:


    me "Ah, hola, también estás aquí..."


translate spanish day3_main5_6837bff6:


    th "¿Acaso es asombroso?"


translate spanish day3_main5_f1be5fed:


    un "Sí."


translate spanish day3_main5_4457b2a1:


    me "Vale...{w} ¿Vas a iluminar la fiesta?"


translate spanish day3_main5_a4468653:


    "La broma resultó ser amarga."


translate spanish day3_main5_47092547:


    un "..."


translate spanish day3_main5_c5ac7756:


    "Lena se ruborizó y miró al suelo."


translate spanish day3_main5_8c3325f1:


    me "Bueno, tal vez no sea la mejor idea... podrías prender fuego a todo..."


translate spanish day3_main5_80702140:


    un "Vale, entonces debo..."


translate spanish day3_main5_279eeea1:


    me "Claro..."


translate spanish day3_main5_bc5b46e9:


    "Se fue."


translate spanish day3_main5_a20cefa7_4:


    "..."


translate spanish day3_main5_94d94b39:


    "Parecía que estuviera todo el campamento en la plaza."


translate spanish day3_main5_3d7269a4:


    "Los pioneros formaban grandes grupos, hablaban, bromeaban por todas partes y reían."


translate spanish day3_main5_e3412cd3:


    "En el panel de DJ Ulyana discutía a voces con Olga Dmitrievna sobre la lista de música a escucharse para esta noche."


translate spanish day3_main5_461da4d2:


    "Ahí va.{w} La música empezó a sonar."


translate spanish day3_main5_e9f89819:


    "No conocía la banda o la música, pero realmente sentí que podría etiquetarla como «clásicas populares soviéticas» si me preguntaras por ellas."


translate spanish day3_main5_337cd741:


    "Los pioneros estaban quietos justo ahí durante un rato como si no escucharan la música."


translate spanish day3_main5_5f0a6bf8:


    "Siempre es difícil hacer un primer paso.{w} Especialmente si estás muy seguro de que vas a ser el único que podría hacerlo..."


translate spanish day3_main5_f14850ff:


    "Ulyana parecía no conocer esa sencilla verdad."


translate spanish day3_main5_1b76cd8a:


    "Alcanzó el centro de la plaza con unos pocos saltos y empezó a gritar:"


translate spanish day3_main5_617994a2:


    us "¡¿A qué estáis esperando?!"


translate spanish day3_main5_62d41fb8:


    "Y empezó a menearse ridículamente."


translate spanish day3_main5_b59f99e6:


    "«Menearse» es la palabra exacta.{w} No podías encontrar una mejor."


translate spanish day3_main5_530ffc25:


    "Se veía tan tonta y graciosa, que no me pude resistir a reírme."


translate spanish day3_main5_da8b7f2f:


    "Ella se percató."


translate spanish day3_main5_015e0ef0:


    us "¡Ey! ¡Semyon!"


translate spanish day3_main5_11709dee:


    "Hice ver que no la había escuchado."


translate spanish day3_main5_81e15cab:


    us "¡Para ya de disimular! Ven aquí."


translate spanish day3_main5_0acd34c9:


    "Seguí ignorándola."


translate spanish day3_main5_57b6835b:


    "Los pioneros se dieron cuenta poco a poco de que la fiesta les empezaba a atraer y comenzaron a bailar."


translate spanish day3_main5_0cfd660b:


    "Me pareció realmente estúpido desde mi punto de vista."


translate spanish day3_main5_6ca388ff:


    th "¿Agitar los brazos y las piernas de uno mismo al compás de unos éxitos musicales largamente olvidados?{w} ¡Venga ya, sé realista!"


translate spanish day3_main5_785dd32e:


    "Por supuesto, no podía bailar cualquier cosa pero ésta en concreto no se le podía llamar baile tampoco."


translate spanish day3_main5_7023662f:


    sl "Ey, Semyon, ¿qué haces sentado ahí?{w} ¿No quieres bailar?"


translate spanish day3_main5_19346aa1:


    "Estaba tan ensimismado en mis propios pensamientos que incluso no me di cuenta de Slavya."


translate spanish day3_main5_6a4b94d5:


    me "De verdad que no..."


translate spanish day3_main5_d7a9e07a:


    sl "¿Estás seguro?"


translate spanish day3_main5_d1046a49:


    "Ella sonrió."


translate spanish day3_main5_65003d93:


    me "Tal vez más tarde."


translate spanish day3_main5_7ccf3096:


    th "¡Maldita sea! ¡¿Qué se supone que hago aquí?!"


translate spanish day3_evening_sl_906aa1af:


    th "¿Por qué irías a una fiesta de baile si prefieres estar quieto en un rincón?"


translate spanish day3_evening_sl_007e73c0:


    me "Vale, pero solamente un poquito..."


translate spanish day3_evening_sl_d0c11d8b:


    sl "¡Ése es el espíritu!"


translate spanish day3_evening_sl_55ab51a9:


    "Ella me ofreció su mano."


translate spanish day3_evening_sl_e6ed4325:


    "Estábamos quietos en medio de la multitud de gente bailando."


translate spanish day3_evening_sl_0dd1c4e2:


    "Bueno, de hecho, era yo quien estaba permaneciendo quieto, Slavya estaba animada, balanceándose al ritmo de la música."


translate spanish day3_evening_sl_588cc129:


    "Ya me estaba arrepintiendo y maldiciéndome por aceptar esta propuesta tan incómoda."


translate spanish day3_evening_sl_8bd9ab89:


    th "Vale, aquí estoy yo, en la encrucijada otra vez, ¿y ahora qué?"


translate spanish day3_evening_sl_3987eb50:


    "Eché un vistazo a los pioneros que bailaban a mi alrededor."


translate spanish day3_evening_sl_7cf46727:


    th "¡Nop! No estoy preparado para esto."


translate spanish day3_evening_sl_6a467bdf:


    "La música se paró de forma repentina."


translate spanish day3_evening_sl_dd530cb7:


    "Algunas personas todavía estaban bailando por inercia, pero la mayoría paró."


translate spanish day3_evening_sl_a231e522:


    mt "¡No entres en pánico!"


translate spanish day3_evening_sl_cdf4ba52:


    "La líder fue al panel del DJ y ajustó algunas cosas."


translate spanish day3_evening_sl_80ab8af2:


    mt "La siguiente canción. Las damas invitan a los caballeros."


translate spanish day3_evening_sl_69f37ce2:


    "Miré a Slavya."


translate spanish day3_evening_sl_e0a7c20b:


    "Ella estrechó sus manos junto a las mías con una sonrisa."


translate spanish day3_evening_sl_660ca959:


    "No deberías rechazar a las muchachas."


translate spanish day3_evening_sl_480cbce8:


    "Bailamos durante unos pocos minutos."


translate spanish day3_evening_sl_3ff4b37b:


    "Su pecho latía acelerándose y su rostro se ruborizó más y más."


translate spanish day3_evening_sl_98bcf9db:


    "Slavya miraba fijamente en mis ojos."


translate spanish day3_evening_sl_77871c15:


    "Traté de apartar la mirada hacia los pioneros que bailaban o hacia mis pies o hacia algo."


translate spanish day3_evening_sl_a4bce591:


    "Era un sentimiento extraño: mi cuerpo entero se estremecía de tanto en tanto, pero era un estremecimiento agradable, sin ningún atisbo de incomodidad o de vergüenza."


translate spanish day3_evening_sl_d4eb018d:


    "Mi mente se volvió milagrosamente tranquila."


translate spanish day3_evening_sl_f5c6ebae:


    "¡Me di cuenta que no quería dejar escapar esta muchacha y que estaba preparado para bailar con ella para siempre!"


translate spanish day3_evening_sl_5cf561ad:


    "Saltando cerca de nosotros, Ulyana paró por un momento y sonrió con picardía."


translate spanish day3_evening_sl_67912668:


    "Quizá estuviera viendo cosas, pero no podía quitarme de mis pensamientos que aquella muchacha insolente estaba tratando de lanzar una indirecta de algo."


translate spanish day3_evening_sl_1eafcf89:


    "Puse cara para asustarla, pero ella ya se había ido."


translate spanish day3_evening_sl_a428f2b1:


    sl "¿Hay algo que va mal?"


translate spanish day3_evening_sl_e6509443:


    "Slavya preguntó tranquilamente."


translate spanish day3_evening_sl_fb700e8f:


    me "Todo está bien...{w} Quiero decir que todo va genial."


translate spanish day3_evening_sl_46fbad31:


    "Aun no estaba tartamudeando, pero estaba a punto de hacerlo."


translate spanish day3_evening_sl_df85f677:


    sl "Estás muy tenso."


translate spanish day3_evening_sl_89875d8e:


    me "Un poco."


translate spanish day3_evening_sl_9fb02d3f:


    "Respondí sinceramente."


translate spanish day3_evening_sl_478c945d:


    sl "A los muchachos no os gusta bailar."


translate spanish day3_evening_sl_48176bfb:


    me "Supongo..."


translate spanish day3_evening_sl_43c3e662:


    "No dijo nada, simplemente sonrió."


translate spanish day3_evening_sl_a20cefa7:


    "..."


translate spanish day3_evening_sl_b2671e23:


    "La canción se terminó al fin."


translate spanish day3_evening_sl_97f9a3fb:


    "Todavía estaba junto Slavya, pero ella se apartó fácilmente de mi alcance."


translate spanish day3_evening_sl_42e06c7b:


    sl "¡Gracias por el baile!"


translate spanish day3_evening_sl_39a579dc:


    me "Debería agradecértelo yo."


translate spanish day3_evening_sl_ff3d4c64:


    "Estuve simplemente permaneciendo de pie mirándola fijamente durante un tiempo."


translate spanish day3_evening_sl_ed185ba1:


    "La siguiente pista de música empezó a sonar."


translate spanish day3_evening_sl_69421a79:


    sl "¿Una más?"


translate spanish day3_evening_sl_0a5e88b4:


    me "No, creo que descansaré un rato."


translate spanish day3_evening_sl_144a26aa:


    "Traté de irme de la plaza tan pronto como fuera posible."


translate spanish day3_evening_sl_46643faf:


    "No recuerdo cómo acabé en la playa."


translate spanish day3_evening_sl_b8ec268f:


    th "¿Por qué me fui corriendo?"


translate spanish day3_evening_sl_52f987f3:


    "Parecía exactamente eso, que salí corriendo."


translate spanish day3_evening_sl_a727c48b:


    th "El baile fue bonito y Slavya estaba complacida."


translate spanish day3_evening_sl_c5789e6f:


    "Pero había algo que no estaba correcto, me sentía inquieto con tan sólo pensar en la fiesta de baile."


translate spanish day3_evening_sl_915c2c39:


    "¿Quizá simplemente perdí el control de la situación y me puse sentimental?"


translate spanish day3_evening_sl_073908ff:


    "Eso sucede raramente."


translate spanish day3_evening_sl_e79ce22b:


    "Siempre trato de comportarme y de evaluar con calma la situación.{w} ¿Pero logré hacerlo en esta ocasión?"


translate spanish day3_evening_sl_cd66cbff:


    "Me senté en la arena y miré el río."


translate spanish day3_evening_sl_8a61b36d:


    "La luna llena estaba por encima del horizonte de agua."


translate spanish day3_evening_sl_71e227a7:


    "Los recuerdos me abrumaron."


translate spanish day3_evening_sl_98e1accf:



    nvl clear
    "Tengo seis años. Yo y mi padre estamos pescando. Lanzo mi anzuelo torpemente y espero. Un minuto, dos, diez. No hay pez que muerda el anzuelo. Mientras tanto, mi padre consigue uno detrás de otro."


translate spanish day3_evening_sl_01fe6588:


    me "¿Cómo lo haces?"


translate spanish day3_evening_sl_454a26a0:


    "Se explica. Pero ahora no me acuerdo de todos los detalles."


translate spanish day3_evening_sl_cd97be40:


    "Tengo nueve años. Oscuridad, un edificio deshabitado cerca de nuestro hogar lleno de fantasmas, vampiros, o solamente sintechos. Una escalera de madera inestable hacia el segundo piso, colgando sobre el abismo... en el agujero hacia el sótano."


translate spanish day3_evening_sl_0a0165e9:


    "Estoy asustado, pero aun así subo y bajo esta escalera cada día. Un ladrillo cae en mi pie y pierdo una uña del pie."


translate spanish day3_evening_sl_742d2cdc:


    "Tengo doce años. Mi primera victoria en un torneo de competición de videojuegos. Jugamos a uno de lucha en videoconsolas. Una audiencia imaginaria aplaude mi asombrosa victoria."


translate spanish day3_evening_sl_6fffdf4d:



    nvl clear
    "Tengo quince años. Varios maravillosos goles con mis pies. Y un chute maravilloso desde 30 metros de lejos justo por la escuadra..."


translate spanish day3_evening_sl_20e21081:


    "Tengo diecisiete años. Mi primer amor. Una imagen fugaz, una sombra que se aleja y se desvanece en la neblina de la ciudad. No puedo atraparla entre los edificios y los rincones."


translate spanish day3_evening_sl_b039b984:


    "Los siguientos recuerdos se vuelven más claros, pero de lejos son menos brillantes."


translate spanish day3_evening_sl_294cee60:


    "Las imágenes de mi infancia y juventud, los que vi, eran confusos. Carecían de detalles y no podía ver bien los rostros de las personas. A veces simplemente se veían más bien como un lienzo, cubierto con pintura derramada."


translate spanish day3_evening_sl_f7b289c8:


    "Pero el sentimiento de aquellas imágenes neblinosas era mucho más cálido que aquel otro de los recuerdos más recientes."


translate spanish day3_evening_sl_76b2fe88:


    nvl clear


translate spanish day3_evening_sl_a20cefa7_1:


    "..."


translate spanish day3_evening_sl_6792bf1f:


    sl "¡Aquí estás!"


translate spanish day3_evening_sl_f493af42:


    "Slavya se inclinó hacia mi.{w} No tenía miedo."


translate spanish day3_evening_sl_93544105:


    sl "¿POr qué te fuiste tan pronto?"


translate spanish day3_evening_sl_3b07d9cd:


    me "Solamente sentí que quería estar solo un rato."


translate spanish day3_evening_sl_e79530ba:


    "Parecía más bien que quería esconderme en un caparazón, que no había mundo externo para mi, y que mis dobles se ocuparan de todas las respuestas de ahí fuera.{w} Siempre había tres de ellos ahí.{w} El sordo, el mudo y el ciego."


translate spanish day3_evening_sl_3a8dd74d:


    sl "¿No es bonito estar aquí?"


translate spanish day3_evening_sl_21eebc87:


    "Slavya miraba al río."


translate spanish day3_evening_sl_63d245c0:


    me "Claro, lo es..."


translate spanish day3_evening_sl_d3e0ec8b:


    sl "¿Qué te parece un baile más?"


translate spanish day3_evening_sl_70849ba4:


    me "No, gracias. Estoy bien."


translate spanish day3_evening_sl_c8cd5039:


    "En el fondo de mi sabía que no debería ser tan rudo con Slavya, pero era como una marioneta en la cuerda floja, forzado a decir cosas que nunca diría bajo otras circunstancias."


translate spanish day3_evening_sl_29dcd13b:


    sl "¿Qué estás pensando?"


translate spanish day3_evening_sl_f0e61106:


    me "Nada..."


translate spanish day3_evening_sl_3da81d34:


    sl "¡No puedes pensar sobre nada!"


translate spanish day3_evening_sl_b29aedbf:


    "Hizo un puchero con sus labios, pero sonrió tras ello, dejándome saber que en realidad no estaba enfadada."


translate spanish day3_evening_sl_4ab2d20d:


    sl "Siempre estás pensando sobre alguna cosa, incluso aunque no te des cuenta."


translate spanish day3_evening_sl_6ab05865:


    me "Quizás."


translate spanish day3_evening_sl_fbb41494:


    sl "Entonces... ¿Qué estás pensando ahora mismo?"


translate spanish day3_evening_sl_a3db8fcb:


    me "Sobre las lechuzas."


translate spanish day3_evening_sl_870d8d2c:


    "Dije la primera cosa que se me pasó por la cabeza."


translate spanish day3_evening_sl_37437f1e:


    sl "¿Por qué lechuzas?"


translate spanish day3_evening_sl_ce3666e5:


    "Ella sonrió."


translate spanish day3_evening_sl_595d7b8c:


    me "No lo sé."


translate spanish day3_evening_sl_cd57019d:


    sl "¿Has visto alguna vez alguna lechuza?"


translate spanish day3_evening_sl_d490fd5c:


    me "Por supuesto que sí."


translate spanish day3_evening_sl_05e130d4:


    th "Qué pregunta más tonta..."


translate spanish day3_evening_sl_1537169b:


    sl "¿Te gustan?"


translate spanish day3_evening_sl_66bebe85:


    me "Como cualquier otro pájaro."


translate spanish day3_evening_sl_1dcb4b49:


    sl "Un pájaro nocturno."


translate spanish day3_evening_sl_26b4f833:


    "Especificó Slavya."


translate spanish day3_evening_sl_c6d4e117:


    me "Sí, un pájaro nocturno."


translate spanish day3_evening_sl_67f5dc8a:


    th "Un pájaro nocturno."


translate spanish day3_evening_sl_f08d71cf:


    "Imaginé una lechuza... una cajita con plumas con unos ojos ciegos enormes."


translate spanish day3_evening_sl_35f51a31:


    sl "¿Eres una lechuza?"


translate spanish day3_evening_sl_e54b39e9:


    me "¿Qué?"


translate spanish day3_evening_sl_0db8f540:


    sl "Ya sabes, hay alondras y lechuzas. Algunos prefieren madrugar pronto y otros prefieren dormir."


translate spanish day3_evening_sl_e90153b6:


    "Sus palabras me hicieron volver a la realidad."


translate spanish day3_evening_sl_a6f160d4:


    "Imaginé mi habitación polvorienta, llena de suciedad, una montaña de platos sin fregar en la mesa, una guitarra pudriéndose en la esquina, una corbata colgando de una lámpara y, como cumbre de todo esto... un montón de calcetines sucios bajo la cama."


translate spanish day3_evening_sl_4ac82707:


    "Era por supuesto una lechuza.{w} La noche era mi lugar."


translate spanish day3_evening_sl_2e21ef54:


    "Pero de alguna forma me las estaba arreglando para despertarme temprano en este campamento."


translate spanish day3_evening_sl_dc4d60ee:


    me "Lo sé."


translate spanish day3_evening_sl_b712550e:


    sl "¿Entonces qué eres?"


translate spanish day3_evening_sl_1d219eb9:


    me "Una lechuza, supongo...{w} Me gusta dormir."


translate spanish day3_evening_sl_6b0f0e89:


    sl "Y yo soy una alondra.{w} Cuanto antes te despiertas, más cosas puedes hacer durante el día."


translate spanish day3_evening_sl_8392621f:


    "Aquí no hay nada realmente urgente por mi parte para hacer, así que no veo ninguna diferencia entre el día y la noche.{w} Simplemente se está más tranquilo y relajado durante la noche."


translate spanish day3_evening_sl_09bf6d3d:


    me "Slavya, ¿no tienes ningún problema en tu vida?"


translate spanish day3_evening_sl_5240e44d:


    sl "¿Qué quieres decir?"


translate spanish day3_evening_sl_a4614188:


    "Se preguntó ella."


translate spanish day3_evening_sl_77f4a782:


    me "Siempre estás tan alegre, tan dispuesta a ayudar, con tanta facilidad para empezar alguna cosa."


translate spanish day3_evening_sl_7f5e995c:


    me "Es como si no hubiera nada que pudiera detenerte..."


translate spanish day3_evening_sl_af407039:


    sl "¡¿Por qué?!"


translate spanish day3_evening_sl_ec2c2a7c:


    "Ella sonrió."


translate spanish day3_evening_sl_cbff0809:


    sl "Soy tan sólo una persona normal."


translate spanish day3_evening_sl_7c783ece:


    th "Eso es correcto.{w} Tan sólo una persona normal en un lugar normal."


translate spanish day3_evening_sl_cce49fa3:


    me "A veces pienso que soy el único extraño aquí."


translate spanish day3_evening_sl_52f705e3:


    sl "¿No te gusta el campamento?"


translate spanish day3_evening_sl_8f568239:


    me "No quiero decir solamente el campamento.{w} En la anterior..."


translate spanish day3_evening_sl_2a7a8a3b:


    "Metí la pata."


translate spanish day3_evening_sl_4b44803d:


    th "No debería hablar con tanta franqueza aunque sea ella."


translate spanish day3_evening_sl_5d943aa7:


    me "En casa también.{w} Creo que no soy como los otros. No es el lugar en el que debo estar..."


translate spanish day3_evening_sl_4b441221:


    sl "¡Oh para ya! Es una tontería pensar así."


translate spanish day3_evening_sl_81571d87:


    th "Me estaba encerrando en mi mismo alejándome del resto del mundo hace tan sólo un rato y ahora abro mi corazón a esta muchacha."


translate spanish day3_evening_sl_15fa3132:


    th "¿Qué me está ocurriendo?"


translate spanish day3_evening_sl_97fb9be9:


    me "No, en serio. No pondrías tu atención en mi bajo otras circunstancias.{w} Soy muy distinto de ti. Soy vago, insociable y carezco de talentos destacables."


translate spanish day3_evening_sl_45a7a568:


    me "Sería la última persona de la que te darías cuenta en una muchedumbre de una gran ciudad.{w} Aunque incluso salir fuera es raro para mi."


translate spanish day3_evening_sl_618ab2ac:


    sl "Semyon, me estás asustando."


translate spanish day3_evening_sl_453d16cb:


    "Me miró muy seria."


translate spanish day3_evening_sl_d4064844:


    "Estaba avergonzado, pero no aparté mi mirada."


translate spanish day3_evening_sl_aad6bab5:


    me "¿Me equivoco?"


translate spanish day3_evening_sl_738f16d6:


    sl "¡Por supuesto!{w} Tú eres tú. No hay ninguna otra persona como tú. Deberías simplemente tener algo de autoconfianza y paciencia, ¡y lo conseguirás todo!{w} No lo dudes."


translate spanish day3_evening_sl_c4f4bcb6:


    "Miré al cielo."


translate spanish day3_evening_sl_3b021425:


    me "Si todo fuera tan sencillo como dices..."


translate spanish day3_evening_sl_b574b5d1:


    sl "¡No es para nada difícil! ¡Vamos a empezar ahora mismo!"


translate spanish day3_evening_sl_1ad35916:


    me "¿Empezar qué?"


translate spanish day3_evening_sl_7109d569:


    sl "¡El cambio!"


translate spanish day3_evening_sl_ee1a91b3:


    th "¿Así sin más?"


translate spanish day3_evening_sl_ea49f8ff:


    me "¿Y cómo piensas hacerlo?"


translate spanish day3_evening_sl_541b7562:


    sl "¡Deberíamos hacer algo útil!"


translate spanish day3_evening_sl_e473fa2f:


    me "¿Por ejemplo?"


translate spanish day3_evening_sl_7db509b3:


    sl "Bueno..."


translate spanish day3_evening_sl_60f3d758:


    "Ella pensó durante un rato."


translate spanish day3_evening_sl_44f18559:


    sl "Vamos a limpiar la plaza.{w} ¡La fiesta ya ha terminado!"


translate spanish day3_evening_sl_6e3178d0:


    me "¿Solamente nosotros dos?"


translate spanish day3_evening_sl_bd88c88c:


    th "¿Seríamos capaces de hacer todo el trabajo?"


translate spanish day3_evening_sl_be7f9c31:


    sl "No hay tanto que hacer.{w} ¡El primer paso es el más importante de todos!"


translate spanish day3_evening_sl_c3348a75:


    th "No puedo discutirle en ese aspecto porque tiene razón, pero quitar las luces de los árboles en la oscuridad, mover el equipo de audio tan pesado y grande..."


translate spanish day3_evening_sl_03eb9a2e:


    "Todo eso no estaba en mi lista de cosas a hacer."


translate spanish day3_evening_sl_0e6e2d6e:


    "Pero no sabía cómo rechazarla educadamente."


translate spanish day3_evening_sl_615361ab:


    me "¿Tal vez lo hacemos mañana?{w} ¿Con todos?"


translate spanish day3_evening_sl_c0da7d07:


    sl "¿Por qué esperar hasta mañana?{w} ¿No sería maravilloso estar mañana en la plaza limpia durante la formación?"


translate spanish day3_evening_sl_8cede868:


    th "Lo primero de todo, tampoco me gustan en general las formaciones."


translate spanish day3_evening_sl_5b4109bc:


    me "Ey, te comprendo...{w} ¡Pero esto es demasiado!"


translate spanish day3_evening_sl_bbc1f2e4:


    "Slavya estaba ensimismada en sus pensamientos."


translate spanish day3_evening_sl_245f16e1:


    sl "Sí, parece que tienes razón."


translate spanish day3_evening_sl_3e8d56f1:


    th "Incluso a veces puedo estar en lo cierto.{w} Y ella puede estar equivocada."


translate spanish day3_evening_sl_d746a7a3:


    sl "Pues..."


translate spanish day3_evening_sl_83de587e:


    "Ella se encogió y bostezó."


translate spanish day3_evening_sl_3b5d4e01:


    sl "¡Pues vayámonos tan sólo a dormir!"


translate spanish day3_evening_sl_3cd13b17:


    "Buena idea, pero quería conversar más."


translate spanish day3_evening_sl_186a1c59:


    me "No es muy pronto..."


translate spanish day3_evening_sl_bf68bee5:


    sl "¡No!"


translate spanish day3_evening_sl_8b29d788:


    "Slavya reprochó directamente."


translate spanish day3_evening_sl_440467e0:


    sl "Luego te dormirás por completo mañana."


translate spanish day3_evening_sl_3dd3f806:


    me "¡Cuéntame un cuento para dormir entonces!"


translate spanish day3_evening_sl_75c35567:


    "Espeté la primera cosa que se me pasó por la mente."


translate spanish day3_evening_sl_e8eb4335:


    sl "Solamente sé aquellas cosas que leo en los libros...{w} Debes conocerlas también."


translate spanish day3_evening_sl_807b7383:


    th "Quizás..."


translate spanish day3_evening_sl_744ed324:


    "Qué fallo."


translate spanish day3_evening_sl_ad3d2e0c:


    sl "¿Nos vamos?"


translate spanish day3_evening_sl_55ab51a9_1:


    "Ella me dio la mano."


translate spanish day3_evening_sl_6199f44f:


    me "Me quedaré un rato más aquí, necesito tomar un poco el aire fresco."


translate spanish day3_evening_sl_76d0ac27:


    me "¡Te puedes ir! ¡Buenas noches!"


translate spanish day3_evening_sl_5f3273c1:


    sl "¡Buenas noches!"


translate spanish day3_evening_sl_ec475aa3:


    "Slavya se fue con prisas."


translate spanish day3_evening_sl_22e44be2:


    "Quería disfrutar mi derrota total primero."


translate spanish day3_evening_sl_4ff0543c:


    "No me quedaría ni un minuto más con ella tampoco después de esa brillante idea. ¿Cuentos para dormir? ¿Pero qué diantres?"


translate spanish day3_evening_sl_335c00f5:


    th "Así es cómo son las cosas. Tratas de tener una conversa casual con una muchacha y se vuelve en un completo despropósito."


translate spanish day3_evening_sl_368a09ed:


    "Observé el cielo nocturno."


translate spanish day3_evening_sl_7de6936f:


    "Las estrellas, aquellas pequeñas luces de soles distantes, parpadeaban alegremente como si se rieran de mi."


translate spanish day3_evening_sl_b61a3a4c:


    "Me di la vuelta y enterré mi rostro en la arena."


translate spanish day3_evening_sl_10854c7a:


    "Después de un rato me di cuenta que aquello era otra de mis brillantes ideas, justo al mismo nivel que los cuentos para dormir."


translate spanish day3_evening_sl_a8978170:


    "Me levanté, escupí, y me fui hacia la cabaña de la líder."


translate spanish day3_evening_sl_a20cefa7_2:


    "..."


translate spanish day3_evening_sl_5195ec0b:


    "Durante todo el camino sentí un picor en mi espalda."


translate spanish day3_evening_sl_81f35693:


    "No solamente mi espalda, todo mi cuerpo."


translate spanish day3_evening_sl_533207f2:


    "Me pregunto por qué será... puesto que no me he dado una ducha en varios días cuando aquí hace tanto calor durante el día, por lo que sudas como un cerdo."


translate spanish day3_evening_sl_17474fa5:


    th "Debería buscar algo de jabón y una toalla."


translate spanish day3_evening_sl_aadfea7e:


    "Puedo encontrar de todo en la cabaña de Olga Dmitrievna."


translate spanish day3_evening_sl_f8c52b26:


    "Tras un par de minutos estaba de nuevo en la plaza con una bolsa de utensilios para el aseo."


translate spanish day3_evening_sl_b0cbc573:


    th "Tengo que encontrar unos baños ahora."


translate spanish day3_evening_sl_3b2b772c:


    "Era fácil. El edificio estaba justo en el borde del bosque."


translate spanish day3_evening_sl_662916c1:


    th "Es realmente el mejor lugar para unos baños."


translate spanish day3_evening_sl_d2fefd9e:


    "Creo que no hay ningún hombre del saco ahí dentro, pero las luces están encendidas."


translate spanish day3_evening_sl_2ee9293b:


    me "¿Y quién tendría la brillante idea de tener una ducha a media noche?"


translate spanish day3_evening_sl_2eb0d079:


    "Murmuré para mis adentros."


translate spanish day3_evening_sl_68584892:


    "Solamente hay un problema... es un muchacho o una muchacha."


translate spanish day3_evening_sl_c7224776:


    "En el primer supuesto podríamos tan sólo tener una ducha juntos sin ningún tipo de problema."


translate spanish day3_evening_sl_289b8414:


    th "No estoy acostumbrado a los baños públicos, pero puedo sobrellevarlo por esta vez."


translate spanish day3_evening_sl_0c723b2c:


    "En el segundo supuesto tendría que esperar o aguantarme con los picores durante toda la noche."


translate spanish day3_evening_sl_99683490:


    th "Quién sabe cuánto tiempo tendré que esperar..."


translate spanish day3_evening_sl_9c652616:


    "Hice una miradita por la ventana.{w} Pero no podía ver nada a causa del vapor."


translate spanish day3_evening_sl_de4b06b3:


    "Repentinamente, Slavya apareció justo delante de mí como si surgiera a través de la niebla."


translate spanish day3_evening_sl_3300e119:


    "Naturalmente, ella estaba desnuda, como cualquiera lo estaría en un baño."


translate spanish day3_evening_sl_f8f1d6e9:


    "Boquiabierto, me quedé contemplándola."


translate spanish day3_evening_sl_45d87efb:


    "Nunca había visto a una muchacha desnuda tan de cerca, incluso aunque fuera detrás de un vidrio."


translate spanish day3_evening_sl_4029c0f3:


    "Mi cuerpo reaccionó como es lógico."


translate spanish day3_evening_sl_0ad798c3:


    "Los impulsos nerviosos viajaron desde mis ojos hacia la parte inferior de mi cuerpo."


translate spanish day3_evening_sl_c0894e89:


    "Parecía que no podía abandonar mi observación sin importar lo que sucediera, incluso aunque estallara una guerra."


translate spanish day3_evening_sl_addbff34:


    "Pero Slavya no me percató."


translate spanish day3_evening_sl_defd30a4:


    "Se estaba duchando sin prisa. Enjuagando su cabello, frotando su cuerpo con una esponja y dejando caer toda el agua de un cubo sobre sí misma."


translate spanish day3_evening_sl_4cdeecd0:


    "Entonces comenzó a limpiarse el cabello."


translate spanish day3_evening_sl_f24eb2b8:


    "No me puedo imaginar cuánto tiempo necesita para limpiarlo completamente, pero estos minutos se fueron volando como segundos de tan inmerso que me hallaba en mi contemplación."


translate spanish day3_evening_sl_a20cefa7_3:


    "..."


translate spanish day3_evening_sl_9d93e651:


    "Finalmente terminó de ducharse, suspiró con satisfacción y caminó hacia la puerta."


translate spanish day3_evening_sl_a20cefa7_4:


    "..."


translate spanish day3_evening_sl_4ad1145a:


    "Me las arreglé para recuperar mis sentidos justo en el momento en que ella fue a cambiar de habitación, y me escondí en los arbustos más cercanos."


translate spanish day3_evening_sl_9d9ca7f3:


    "Lo mejor que podía hacer era no tentar el destino y abandonar aquel lugar, pero llegué demasiado tarde."


translate spanish day3_evening_sl_bf131a4a:


    "Slavya apareció en el porche justo un momento después, se quedó quieta ahí por unos instantes, disfrutando la brisa de la noche y caminó en la dirección de mi escondite."


translate spanish day3_evening_sl_c31ff7a6:


    "Tenía una docena de excusas pasando por mi mente, pero ella pasó de largo incluso mirando los arbustos donde estaba escondido."


translate spanish day3_evening_sl_efff4fc1:


    th "Parece que tengo suerte..."


translate spanish day3_evening_sl_62c6e320:


    "Salí cautelosamente y me fui hacia la cabaña de Olga Dmitrievna, mientras olvidé completamente mis intenciones de tomar una ducha."


translate spanish day3_evening_sl_a085be61:


    "Estaba totalmente exhausto y quise tan sólo echarme en la cama y quedarme dormido."


translate spanish day3_evening_sl_d8f66d7a:


    "Mis ojos no podían esperar más y se fueron cerrando poco a poco."


translate spanish day3_evening_sl_a936b78e:


    "Era peligroso incluso pestañear."


translate spanish day3_evening_sl_3d48081a:


    "Aun no conozco el campamento lo suficiente como para caminar por él con mis ojos casi cerrados."


translate spanish day3_evening_sl_a20cefa7_5:


    "..."


translate spanish day3_evening_sl_44fae48f:


    "Repentinamente Slavya apareció detrás de mí."


translate spanish day3_evening_sl_3820771a:


    me "..."


translate spanish day3_evening_sl_6de32e31:


    sl "¿Te vas a ir a dormir?"


translate spanish day3_evening_sl_da49764c:


    me "Bueno... sí..."


translate spanish day3_evening_sl_97fc1ed6:


    "Justo en este momento me acordé de la vista en los baños y aparté mi mirada tímidamente."


translate spanish day3_evening_sl_b78c6e26:


    me "¿Y tú?"


translate spanish day3_evening_sl_7405f4b0:


    sl "¡Estoy limpiando!"


translate spanish day3_evening_sl_2214025f:


    "Sacó una escoba que tenía detrás de ella."


translate spanish day3_evening_sl_dfc6b36d:


    "Slavya con una escoba en la vacía plaza por la noche se parecía a una bruja sacada de un cuento de hadas para niños."


translate spanish day3_evening_sl_e07c37b3:


    sl "¿Qué?"


translate spanish day3_evening_sl_62fd010a:


    me "Nada.{w} ¿Es realmente una buena idea limpiar la plaza tras un baño?"


translate spanish day3_evening_sl_f52a7f9e:


    sl "¿Y cómo sabes tú que me he dado un baño?"


translate spanish day3_evening_sl_28bebef0:


    "Sentí físicamente como el miedo se apoderaba de mi cuerpo, un frío sudor empezó a recorrer mi espalda, mis pensamientos se congelaron por lo que no podía pensar en ninguna excusa."


translate spanish day3_evening_sl_3820771a_1:


    me "..."


translate spanish day3_evening_sl_a684124e:


    "Slavya aun permanecía mirándome fijamente sorprendida."


translate spanish day3_evening_sl_888019d6:


    "Parecía que mi vida dependiera de esta respuesta."


translate spanish day3_evening_sl_12308c9c:


    me "Bueno. Tu cabello está húmedo..."


translate spanish day3_evening_sl_1f26ca7d:


    sl "Oh, tienes razón."


translate spanish day3_evening_sl_5d0b4e88:


    "Deseaba que la tierra me tragara, así me desvanecería de este mundo sólo como si fuera repentina e irreversiblemente como me desvanecí del {i}mío{/i}."


translate spanish day3_evening_sl_c493719d:


    sl "¿Vas a darte un baño también?"


translate spanish day3_evening_sl_2e67d98e:


    me "Yo... Bueno..."


translate spanish day3_evening_sl_3a1790bb:


    "Slavya miró la bolsa en mis manos."


translate spanish day3_evening_sl_92d5dd33:


    me "¡Sí, sí! ¡Así que buenas noches!"


translate spanish day3_evening_sl_6b587b4c:


    "Me giré y corrí a toda velocidad de vuelta a los baños."


translate spanish day3_evening_sl_63c688cb:


    th "Qué desastre, qué vergonzoso..."


translate spanish day3_evening_sl_09404bd9:


    "Caminé lentamente, maldiciéndome a mi mismo por todo lo sucedido."


translate spanish day3_evening_sl_ee7118ff:


    th "No debería haber espiado en primer lugar."


translate spanish day3_evening_sl_e5405609:


    th "Pero si miro a hurtadillas debo prestar más atención y pensar en una excusa de antemano."


translate spanish day3_evening_sl_45531a98:


    th "Oh..."


translate spanish day3_evening_sl_a20cefa7_6:


    "..."


translate spanish day3_evening_sl_0cfbc599:


    "No me llevó mucho tiempo ducharme."


translate spanish day3_evening_sl_9dc5cc01:


    "Parecía que la noche hubiera hecho su propio baile o, posiblemente, un concierto.{w} Las estrellas eran las luces. Los pájaros e insectos eran una orquestra, una lechuza, ululando en algún lugar, era su director. El ruido del viento en los árboles era el aplauso de la audiencia."


translate spanish day3_evening_sl_36d666ac:


    "La noche resultaba ser mucho más bonita cuando permanecías en el bosque limpio y fresco."


translate spanish day3_evening_sl_501fc9a9:


    "De repente escuché un ruido en los arbustos más cercanos."


translate spanish day3_evening_sl_7492209c:


    "Temblé, pero no me quedé muy asustado."


translate spanish day3_evening_sl_c160bc4e:


    th "Podría ser una ardilla u otro animal."


translate spanish day3_evening_sl_a710287b:


    th "No obstante debería comprobarlo."


translate spanish day3_evening_sl_add052f9:


    "Fui a los arbustos."


translate spanish day3_evening_sl_f110d6af:


    "Pero no encontré nada ni a nadie sospechoso."


translate spanish day3_evening_sl_869f707c:


    th "Parecía simplemente..."


translate spanish day3_evening_sl_bd3b5e0d:


    "Con el cuerpo limpio y el espíritu relajado fui a la cabaña de la líder."


translate spanish day3_evening_sl_1424964a:


    "Olga Dmitrievna estaba ya dormida."


translate spanish day3_evening_sl_01ffcfe1:


    "Me fui a la cama sin quitarme la ropa y me cubrí con una sábana."


translate spanish day3_evening_sl_b675339f:


    "No era capaz de dormirme durante un largo rato, recordando a la desnuda Slavya, su propia imagen no podía eclipsar mis pensamientos para encontrar respuestas."


translate spanish day3_evening_un_dee15e4b:


    "Una invitación más para bailar podría haber arruinado mi orgullo por completo."


translate spanish day3_evening_un_f61e6089:


    "Empecé a pensar sobre una apropiada excusa para irme y estaba tratando de hallar el momento idóneo para hacerlo, pero entonces vi a Lena.{w} Se estaba dirigiendo lentamente hacia mi dirección."


translate spanish day3_evening_un_b9dd006a:


    un "¿Tal vez deberíamos ir?"


translate spanish day3_evening_un_25f7abbc:


    me "¿Dónde?"


translate spanish day3_evening_un_f12b72b3:


    "Estaba tan inmerso en mis pensamientos que realmente no entendí qué quería decir."


translate spanish day3_evening_un_bf618d8b:


    un "A la enfermería.{w} Pero, si quieres quedarte aquí y bailar..."


translate spanish day3_evening_un_a35ed640:


    "Dudo mucho que ella fuera a bailar. Lena se había quedado quieta toda la noche."


translate spanish day3_evening_un_5b6584a0:


    me "¡No, paso!{w} Vamos."


translate spanish day3_evening_un_ea985af0:


    th "¡Al menos no tendría que quedarme quieto aquí como un tímido ratón de biblioteca!"


translate spanish day3_evening_un_329854dd:


    "En serio, tratar de ocultarme a mi mismo en un rincón no era muy gratificante."


translate spanish day3_evening_un_c5a5dd23:


    th "Incluso un toro en una tienda china sería mucho más ágil que yo bailando en una pista de baile."


translate spanish day3_evening_un_3db6c6b7:


    "En primer lugar, no tenía ni la más mínima intención de bailar."


translate spanish day3_evening_un_68b672da:


    un "Bueno. ¿Vamos a ir a la enfermería?"


translate spanish day3_evening_un_ef4b7608:


    "Lena me hizo retornar a la realidad sacándome de mis pensamientos."


translate spanish day3_evening_un_3b8ef189:


    "Solamente estuvimos quietos cerca de la cantina por un rato."


translate spanish day3_evening_un_95bffd3a:


    me "Sí, claro... ¡Gracias!"


translate spanish day3_evening_un_2e3d9fb1:


    un "¿Por qué?"


translate spanish day3_evening_un_f8a6296f:


    "Me miró sorprendida."


translate spanish day3_evening_un_177d6e89:


    me "Bueno... Por sacarme de ahí..."


translate spanish day3_evening_un_cea5943f:


    "No debería haberle explicado que eso de bailar no es lo mío."


translate spanish day3_evening_un_966aaa9d:


    me "Es...{w} ya sabes... ¡tan aburrido estar ahí!"


translate spanish day3_evening_un_fb773468:


    un "Me parece a mi que a ti no te gusta bailar."


translate spanish day3_evening_un_d153fd8d:


    "No había rastro de sarcasmo en su afirmación, a pesar de su cara añiñada."


translate spanish day3_evening_un_5e7d152e:


    "Parece que ella realmente no lo entendió."


translate spanish day3_evening_un_bae3aca3:


    me "Sí. No me gusta del todo. No es lo mío."


translate spanish day3_evening_un_5b72d7bd:


    un "Yo tampoco.{w} Nadie se molesta en invitarme a bailar."


translate spanish day3_evening_un_2f7bb5d6:


    "Lena se ruborizó y se quedó mirando el suelo como acostumbraba."


translate spanish day3_evening_un_4cb51946:


    me "Qué extraño."


translate spanish day3_evening_un_940302b3:


    un "¿Qué es extraño?"


translate spanish day3_evening_un_f9720baf:


    me "Bueno... Que nadie te invite."


translate spanish day3_evening_un_4429143b:


    un "¿Eso crees?"


translate spanish day3_evening_un_ca11e47a:


    "Nuevamente, ella me miró con una expresión de sorpresa y con falta de comprensión."


translate spanish day3_evening_un_92393659:


    "Estaba confundido y no podía hallar palabras para responderle de manera apropiada."


translate spanish day3_evening_un_34c195e3:


    me "¡Sí! ¡Ciertamente!{w} Si me gustara bailar, claro que te habría invitado."


translate spanish day3_evening_un_082d3571:


    un "Gracias..."


translate spanish day3_evening_un_e99c1593:


    "No nos dijimos ni una sola palabra durante el resto del camino."


translate spanish day3_evening_un_542f1cb7:


    "Lena obviamente se sentía completamente confundida por mi torpe cumplido y no sabía sobré qué tema hablarle para darle conversación."


translate spanish day3_evening_un_c0de7e94:


    "Estaba completamente oscuro a esa hora y el melancólico edificio de la enfermería, cubierto por la niebla de la noche, se parecía mucho a una casa embrujada."


translate spanish day3_evening_un_2079dd47:


    "Sentí un fuerte deseo de darme la vuelta y caminar en la dirección opuesta sin hacer ruido."


translate spanish day3_evening_un_13fbafd7:


    "Le eché un rápido vistazo a Lena y me di cuenta que ella se veía como de costumbre... tímida, modesta, insegura, pero de ninguna manera asustada."


translate spanish day3_evening_un_6cdc223c:


    "Eso me hacía estar incluso aun más incómodo."


translate spanish day3_evening_un_095403d0:


    th "No podía ser que ella no estuviera asustada, mientras que yo..."


translate spanish day3_evening_un_cc26ac1a:


    "De repente una lechuza ululó cerca y yo me estremecí."


translate spanish day3_evening_un_cc73b68c:


    "Lena parecía que o no la escuchó, o bien no le prestó atención, o simplemente apenas se asustó."


translate spanish day3_evening_un_9b14946c:


    "Me resultaba difícil de creer que pudiera ser la tercera opción, pero no quise preguntarle sobre ello, para no mostrar mi miedo."


translate spanish day3_evening_un_24cab478:


    "Entré en la enfermería y encontré un interruptor en la oscuridad."


translate spanish day3_evening_un_49249630:


    me "¿Vendrá la enfermera más tarde?"


translate spanish day3_evening_un_404b569f:


    un "No vendrá..."


translate spanish day3_evening_un_e0ce2859:


    th "Vale, ella no vendrá, entiendo...{w} Espera un segundo...{w} ¿Qué quieres decir conque ella no vendrá?"


translate spanish day3_evening_un_e1a4709a:


    me "Ah, ya veo..."


translate spanish day3_evening_un_5fa11b3d:


    "No es que me preocupe estar a solas con Lena.{w} Dentro.{w} Por la noche.{w} Sin nadie cerca..."


translate spanish day3_evening_un_36602009:


    th "Solamente en las películas suele ocurrir algo en semejantes circunstancias."


translate spanish day3_evening_un_0216a72c:


    "Es sólo que estoy con Lena y no con Ulyana o Slavya."


translate spanish day3_evening_un_83f4508f:


    "Y en verdad ha cambiado mi actitud ante las cosas que están sucediendo."


translate spanish day3_evening_un_9e2d7d99:


    un "Ahí están las cajas."


translate spanish day3_evening_un_923e6e0a:


    "Señaló al problemático montón de cajas."


translate spanish day3_evening_un_9066f7f1:


    "Habían como una docena de ellas."


translate spanish day3_evening_un_75fde160:


    "Llevaría bastante más que diez minutos de trabajo."


translate spanish day3_evening_un_3a066bb2:


    "Puse una de las cajas encima de la mesa delante de mí y comencé a sacar su contenido."


translate spanish day3_evening_un_30cdf24e:


    "Habían vendas. Muchísimos paquetes pequeños de vendas."


translate spanish day3_evening_un_b4c9e1a9:


    un "Ten, coge esto."


translate spanish day3_evening_un_ec9bb664:


    "Lena me dio un pedazo de papel."


translate spanish day3_evening_un_b5daf913:


    "Había un formulario con casillas a rellenar y rápidamente me di cuenta que debía poner el nombre a la izquierda, la descripción en el medio, si hay una, y la cantidad en las casillas del formulario de la derecha."


translate spanish day3_evening_un_50cb3d48:


    th "No es una base de datos, pero cumple con el propósito."


translate spanish day3_evening_un_48b8c403:


    "Comenzamos a trabajar a pleno rendimiento."


translate spanish day3_evening_un_2b543552:


    un "Semyon..."


translate spanish day3_evening_un_ad02f53a:


    me "¿Qué?"


translate spanish day3_evening_un_0c8d272a:


    "Miré a Lena."


translate spanish day3_evening_un_3500c639:


    "Ella me miró fijamente por un par de segundos, aparentemente reflexionando sobre qué hacer o decir algo, pero entonces volvió a bajar su mirada otra vez."


translate spanish day3_evening_un_8da69387:


    un "No, olvídalo."


translate spanish day3_evening_un_9f980148:


    "Era físicamente difícil para mi simplemente sentarme y no decir ni una palabra.{w} Sin embargo, no me atrevo a hablar primero, no solamente porque no se me ocurría ningún tema de conversación, sino que simplemente me sentía cohibido. A esta muchacha se le podía incomodar fácilmente por cualquier cosa."


translate spanish day3_evening_un_2b543552_1:


    un "Semyon..."


translate spanish day3_evening_un_e79d0fd5:


    me "¿Sí?"


translate spanish day3_evening_un_dbc3003f:


    un "Estás contándolas otra vez de nuevo."


translate spanish day3_evening_un_e1d4a194:


    "Por supuesto había empezado a sacar y poner las vendas de la «base de datos» que ya había contado."


translate spanish day3_evening_un_87731a54:


    me "Oh, lo siento."


translate spanish day3_evening_un_d00d3d0f:


    "Ella no respondió."


translate spanish day3_evening_un_6b863b6b:


    me "Ey. ¿De dónde eres? Quiero decir, ¿de dónde viniste? Es decir, ¿dónde naciste? Esto... ¿dónde vives?"


translate spanish day3_evening_un_4dbf341d:


    un "Bueno, yo...{w} Hay una ciudad no muy lejos de aquí..."


translate spanish day3_evening_un_652ee7d0:


    me "No muy lejos de aquí. ¿Dónde es eso?"


translate spanish day3_evening_un_e807e738:


    un "En alguna parte..."


translate spanish day3_evening_un_adc0b8f3:


    "Parece que no quiere hablar sobre ello."


translate spanish day3_evening_un_270ee4bf:


    th "¿Está ocultando Lena algo también?"


translate spanish day3_evening_un_1663f57c:


    "Tiene una explicación en el caso de Olga Dmitrievna, pero en el caso de Lena..."


translate spanish day3_evening_un_7edc04e0:


    th "Está claro que no es su estilo."


translate spanish day3_evening_un_4b79f78a:


    me "¿Es un secreto?"


translate spanish day3_evening_un_f4abdb32:


    un "No...{w} Solamente..."


translate spanish day3_evening_un_8a3717f6:


    me "Entonces... ¿Es en alguna parte del sur también?"


translate spanish day3_evening_un_9857e8f9:


    "Debería haber pensado algo más original."


translate spanish day3_evening_un_6791c4d1:


    "Cada una de mis singulares y repentinas ideas acaban yendo mal, y este intento de jugar al espía resultó ser incluso peor."


translate spanish day3_evening_un_fa0dd942:


    "Y pensé en el sur ya que el único campamento de pioneros de la Unión Soviética que recordaba era «Artek»."


translate spanish day3_evening_un_c81cda2f:


    un "... Sí..."


translate spanish day3_evening_un_e39caf93:


    "Lena dudó al responder."


translate spanish day3_evening_un_45925f2f:


    "No sabría decir si ella estaba mintiendo o no."


translate spanish day3_evening_un_3dfaf878:


    un "¿No te gusta estar aquí?"


translate spanish day3_evening_un_38d9fd3d:


    th "Me pregunto qué parte de lo que dije le hizo pensar en eso..."


translate spanish day3_evening_un_8610e699:


    me "¡Pues claro que no! ¡Me gusta!"


translate spanish day3_evening_un_f13d454c:


    "Mi falsa amabilidad sonaba muy insincera, áspera para los oídos."


translate spanish day3_evening_un_1a377d44:


    me "¿Y qué hay de ti?"


translate spanish day3_evening_un_cc213e9d:


    un "Me gusta.{w} Se está muy tranquilo aquí, hay muchos libros en la biblioteca.{w} Y la gente es simpática..."


translate spanish day3_evening_un_a3126422:


    me "Simpática... pero no toda..."


translate spanish day3_evening_un_b672514d:


    un "¿Por qué?"


translate spanish day3_evening_un_6cde6e35:


    "¿Dije esa última parte en voz alta?"


translate spanish day3_evening_un_4c03a0c5:


    "Resulta que lo hice."


translate spanish day3_evening_un_339a1158:


    me "Bueno, ya sabes...{w} Ulyana, por ejemplo. Ella es como una batería Energizer con un completo desequilibrio de tanta energía que rebosa."


translate spanish day3_evening_un_d39ab564:


    un "Batería...{w} ¿Qué?"


translate spanish day3_evening_un_4d8feba6:


    "Tal vez fui enviado atrás en el pasado realmente."


translate spanish day3_evening_un_6421514b:


    me "Olvídalo."


translate spanish day3_evening_un_a656cdf8:


    me "¡O Alisa! Decir que «¡el pionero es el ejemplo modélico para los niños!», ¡ciertamente no la describiría a ella! Si alguien comenzara a seguir su ejemplo, sería un completo desastre para el país en veinte años o así..."


translate spanish day3_evening_un_61114f6a:


    th "Sin embargo, pensando sobre ello ahora, podrías extraer la conclusión de que todo el mundo seguía el ejemplo de Alisa Dvachevskaya en los 80's, donde probablemente me hallaba ahora mismo."


translate spanish day3_evening_un_f187db47:


    un "Ella no es asi, normalmente."


translate spanish day3_evening_un_76bd4f62:


    me "«No es así»... ¿no es cómo qué?"


translate spanish day3_evening_un_0a526940:


    un "No es como tú has dicho que es ella."


translate spanish day3_evening_un_61c3831d:


    me "Para empezar, no había dicho nada.{w} Solamente he afirmado el hecho de que ella no es el mejor ejemplo a seguir."


translate spanish day3_evening_un_21b0641e:


    un "Bueno... Quizás."


translate spanish day3_evening_un_25b8be5c:


    me "Parece que tú la conoces bastante bien..."


translate spanish day3_evening_un_cd1ed0e4:


    un "Probablemente."


translate spanish day3_evening_un_2bf6a5e9:


    "Dije solamente eso para mantener la conversación, no me esperaba una contestación como esa."


translate spanish day3_evening_un_b3051fc6:


    "Alisa y Lena eran muy diferentes. La idea de que ambas pudieran ser parecidas me era increíble."


translate spanish day3_evening_un_68730dd3:


    un "Venimos de la misma ciudad."


translate spanish day3_evening_un_508a08eb:


    "Como si hubiera adivinado mi pregunta."


translate spanish day3_evening_un_2a8c5047:


    un "Tenemos amigos en común, a pesar de que Alisa es un año mayor."


translate spanish day3_evening_un_eaa5b39b:


    me "Vale. Es un poco extraño.{w} Oh, no de esa manera. Sólo estoy sorprendido."


translate spanish day3_evening_un_25ed7170:


    un "Todo el mundo se sorprende."


translate spanish day3_evening_un_d7a43efd:


    "Lena sonrió un poco."


translate spanish day3_evening_un_0bf46b97:


    "Cogí la segunda caja."


translate spanish day3_evening_un_f520e822:


    "Analgésico, carbón activado, analgésico, carbón activado, soluciones salinas, permanganato de potasio, furacilin, analgésico..."


translate spanish day3_evening_un_88d5fb70:


    "Lena siempre hablaba en oraciones sencillas."


translate spanish day3_evening_un_2594eb2e:


    "¿Cómo puedo comunicarme con ella si cada conversación se vuelve un monólogo o simplemente un incómodo silencio?"


translate spanish day3_evening_un_d0a225ef:


    "No estaba muy satisfecho con semejante estado de cosas."


translate spanish day3_evening_un_69f23bef:


    "A veces pareciera que ella oculta alguna cosa bajo su máscara de timidez.{w} ¿Pero por qué?"


translate spanish day3_evening_un_52bebeb3:


    me "Sabes, leí un libro no hace mucho tiempo...{w} ¿Te gusta la ciencia ficción?"


translate spanish day3_evening_un_267018a5:


    un "No mucho."


translate spanish day3_evening_un_eda10b2f:


    th "Maldita sea. Otro fallo."


translate spanish day3_evening_un_41202424:


    me "Bueno, si no te gusta...{w} ¿Entonces qué libros te gustan?"


translate spanish day3_evening_un_25828f24:


    un "De diferentes tipos..."


translate spanish day3_evening_un_c28a338c:


    "La conversación no estaba yendo fluidamente así que lo mejor era dejarlo estar."


translate spanish day3_evening_un_c7228702:


    "Quién sabe por qué, pero pensé otra vez en el baile."


translate spanish day3_evening_un_7e05f767:


    "Sentimientos de desasosiego, malestar e incluso de vergüenza me abrumaron una vez más."


translate spanish day3_evening_un_9a396b7b:


    "Parece que no soy tan distinto de Lena cuando se trata de cosas así. Soy tímido y preocupado para las cosas que no puedo comprender o hacer."


translate spanish day3_evening_un_54d0f688:


    th "Probablemente, debería derrotar mis propios miedos antes de nada."


translate spanish day3_evening_un_cd4e8ca1:


    th "Y esto me ayudará a comprenderla mejor."


translate spanish day3_evening_un_7c4840c7:


    "Acepté dicha idea."


translate spanish day3_evening_un_f8fb534c:


    me "Solamente quedan un par de cajas..."


translate spanish day3_evening_un_f1be5fed:


    un "Sí."


translate spanish day3_evening_un_e1fb3662:


    me "Ey. Tengo una idea.{w} ¿Por qué no vamos a la cantina tras esto?{w} Creo que la cinta de música se tiene que devolver allí."


translate spanish day3_evening_un_9a25521c:


    th "¿Qué hace en la cantina?"


translate spanish day3_evening_un_3db7cd22:


    "Dudé."


translate spanish day3_evening_un_41cfffd5:


    un "¿Para qué?"


translate spanish day3_evening_un_4125d583:


    "Su honesta mirada dejaba muy claro que ella no tenía ni la más remota idea de lo que iba a proponerle."


translate spanish day3_evening_un_1279bc8e:


    me "Bueno... Hablando francamente, es que no sé cómo bailar, ése es el motivo por el cual no me agrada.{w} Esa es la razón por la que estaba incómodo allí."


translate spanish day3_evening_un_054e8b5f:


    me "Quizá podría agradecértelo bailando contigo."


translate spanish day3_evening_un_8ebabae6:


    un "Pero yo..."


translate spanish day3_evening_un_7c35a05f:


    "Ella paró de ordenar las medicinas por un momento, se ruborizó y me miró fijamente a mis ojos."


translate spanish day3_evening_un_7f15ab29:


    "Soy un poco terco."


translate spanish day3_evening_un_72e48efd:


    th "Realmente era una idea estúpida."


translate spanish day3_evening_un_99ad9909:


    me "No pasa nada, si no quieres...{w} ¡No insistiré!"


translate spanish day3_evening_un_d8e58db9:


    un "¿Qué pasa si alguien nos ve?"


translate spanish day3_evening_un_5226ed1b:


    "Ciertamente ni se me ocurrió algo así."


translate spanish day3_evening_un_9cb1a370:


    "No es un gran problema.{w} Pero..."


translate spanish day3_evening_un_606af4d5:


    me "Todo el mundo está ya dormido.{w} ¡Y nadie irá a la cantina por la noche!"


translate spanish day3_evening_un_3855e2ee:


    un "¿Cómo lo haremos para entrar?"


translate spanish day3_evening_un_6e8ce990:


    "Debería haber pensado sobre ello de antemano."


translate spanish day3_evening_un_962242d8:


    me "Bueno..."


translate spanish day3_evening_un_e9f68551:


    "Tenía las llaves de Slavya, pero no quería mencionarlo. Alguien podría llegar a tener una idea equivocada sobre mi, como por ejemplo haberlas robado."


translate spanish day3_evening_un_964defe1:


    "En ese momento lamenté no haber cogido ya mi teléfono móvil en la cabaña de la líder."


translate spanish day3_evening_un_4ee977cd:


    "Olga Dmitrievna me habló a grito pelado y entonces lo olvidé."


translate spanish day3_evening_un_6f016ce7:


    "De todas maneras, tenía que decir algo para salir al paso de este inoportuno tema."


translate spanish day3_evening_un_7e3db1d5:


    me "Ehmm, ¿qué tipo de música te gusta?"


translate spanish day3_evening_un_9c66e26a:


    un "Diferentes tipos...{w} Ninguna en particular."


translate spanish day3_evening_un_f8be0238:


    me "Pues imaginemos que está sonando ahora...{w} Quiero decir que tú... que la escuchamos sonando."


translate spanish day3_evening_un_52f34a61:


    un "¿Cómo se hace?"


translate spanish day3_evening_un_0c4b535c:


    me "¡Como si sonara en tu cabeza!"


translate spanish day3_evening_un_43e2549c:


    "Todavía recordaba la melodía que los pioneros bailaban.{w} La música y las palabras estaban realmente muy claras en mi memoria."


translate spanish day3_evening_un_db88fd32:


    un "No estoy segura de que pueda hacerlo."


translate spanish day3_evening_un_7b5327be:


    me "Puedes tan sólo intentarlo."


translate spanish day3_evening_un_3c68ea6c:


    un "Probablemente..."


translate spanish day3_evening_un_04cfc77c:


    "¡Conseguí que accediera!"


translate spanish day3_evening_un_7ddfa955:


    "En el caso de Lena, incluso ese «probablemente» puede ser considerado un «sí»."


translate spanish day3_evening_un_a20cefa7:


    "..."


translate spanish day3_evening_un_067ecc79:


    "Permanecimos en silencio mientras ordenábamos los suministros médicos durante todo el tiempo que restó, anotando sus nombres y cantidades."


translate spanish day3_evening_un_f7242642:


    "Debería prestar más atención a cada palabra que digo con la suerte que tengo."


translate spanish day3_evening_un_3c9b2687:


    "Aunque permaneciera en silencio la mayor parte del tiempo."


translate spanish day3_evening_un_72af65bb:


    "Al fin terminamos con la última caja."


translate spanish day3_evening_un_5b5ee5ef:


    "Le di a Lena el formulario rellenado y empecé a mirarla, como si hubiera visto un Pies Grandes montando un monociclo mientras hacía malabares con cerditos.{w} Era asombroso, espantoso y, por encima de todo, fascinante."


translate spanish day3_evening_un_2342bdd6:


    "Repentinamente ella rompió en carcajadas."


translate spanish day3_evening_un_3d342d5a:


    me "¡¿Qué?!"


translate spanish day3_evening_un_6f2b6d18:


    un "Vaya cara..."


translate spanish day3_evening_un_094d6b91:


    me "¿Qué pasa con ella?"


translate spanish day3_evening_un_d460b3a8:


    un "Nada, sólo que es graciosa."


translate spanish day3_evening_un_f90f07e5:


    me "¿En serio...?"


translate spanish day3_evening_un_791682db:


    un "Sí. Entonces, ¿dónde vamos a ir?"


translate spanish day3_evening_un_25f7abbc_1:


    me "¿Dónde?"


translate spanish day3_evening_un_530d77f3:


    "Sus últimas palabras me dejaron descolocado, olvidé completamente de qué estábamos hablando."


translate spanish day3_evening_un_9ac6ce51:


    "Hablando francamente, había abandonado este mundo por completo por un instante."


translate spanish day3_evening_un_3d8fbc00:


    un "Bueno, ya sabes...{w} Para bailar."


translate spanish day3_evening_un_48e08966:


    "Lena se sonrojó inmediatamente y su rostro adquirió una expresión extraña de timidez, ansiedad y miedo."


translate spanish day3_evening_un_558e6a65:


    me "¡Ah, sí! Lo siento, me había ensimismado en mis pensamientos.{w} Vamos al embarcadero."


translate spanish day3_evening_un_38967a03:


    "No sé por qué escogí ese lugar."


translate spanish day3_evening_un_c980d140:


    "Tal vez porque podrías dar con un pionero en la plaza, en la zona residencial o cerca de la cantina, pero no en el embarcadero.{w} Al menos eso es lo que creía."


translate spanish day3_evening_un_cd9d64c3:


    "O quizá por la gran y brillante luna que se reflejaba en el agua por la noche."


translate spanish day3_evening_un_5626210f:


    "Y es luna llena hoy."


translate spanish day3_evening_un_7a658aae:


    "No sé cómo, pero esta opción apareció por sí misma."


translate spanish day3_evening_un_c8879bbd:


    me "Si no te apetece, entonces podemos..."


translate spanish day3_evening_un_ac6b93bc:


    un "No. Es realmente un lugar bonito."


translate spanish day3_evening_un_0899349d:


    "Lena cerró la puerta y nos dirigimos hacia el embarcadero."


translate spanish day3_evening_un_2baf966e:


    "La noche caía sobre el campamento durmiente."


translate spanish day3_evening_un_9380078a:


    un "¿Deberíamos seguir la carretera?"


translate spanish day3_evening_un_22b1f5c5:


    me "¿Por qué? Será más rápido a través del bosque. Hay un buen camino por ahí."


translate spanish day3_evening_un_2745ec45:


    un "Bueno. Podría haber ahí..."


translate spanish day3_evening_un_c8d94649:


    me "¿Quién? ¿Una lechuza?"


translate spanish day3_evening_un_60bde9d8:


    "Me reí."


translate spanish day3_evening_un_7b76c769:


    un "Oh, tú..."


translate spanish day3_evening_un_fa1386f8:


    "Una sombra de disgusto se asomó por su cara."


translate spanish day3_evening_un_b2a637c4:


    me "Olvídalo. ¡Lo siento!"


translate spanish day3_evening_un_68893216:


    un "Está oscuro ahí."


translate spanish day3_evening_un_413580ee:


    me "¿Estás preocupada?"


translate spanish day3_evening_un_9c33c3aa:


    un "No, realmente no..."


translate spanish day3_evening_un_732bddc6:


    me "Si quieres..."


translate spanish day3_evening_un_a798f094:


    un "Vale...{w} Pero puedo..."


translate spanish day3_evening_un_44ced226:


    "Sin terminar lo que decía, se agarró a mi brazo."


translate spanish day3_evening_un_17ef55d6:


    un "Te... ¿Te molesta?"


translate spanish day3_evening_un_006ab8be:


    "Ahora era mi ocasión para ruborizarme."


translate spanish day3_evening_un_771aa502:


    me "¡Por supuesto que no!"


translate spanish day3_evening_un_c41c7779:


    "Caminamos a través del bosque."


translate spanish day3_evening_un_90e23667:


    "Aunque difícilmente era un bosque.{w} Más bien una pequeña arboleda entre el campamento y el embarcadero."


translate spanish day3_evening_un_8ba91e28:


    "Unos cientos de metros de largo."


translate spanish day3_evening_un_e7d25333:


    "Nunca creería que pudiera haber algo de qué preocuparse, incluso para mi mismo."


translate spanish day3_evening_un_b12f537c:


    "Pero justo parecía que el miedo de Lena era contagioso."


translate spanish day3_evening_un_cd8be3f9:


    "Las brancas de los árboles se balancearon sobre nosotros.{w} Sentí un escalofrío y Lena se aferró a mi brazo aun más fuertemente."


translate spanish day3_evening_un_4a492d8c:


    me "No te preocupes. Debe ser una ardilla."


translate spanish day3_evening_un_0905212f:


    un "Claro..."


translate spanish day3_evening_un_51f323e1:


    "Alcanzamos el embarcadero.{w} La noche era realmente bella."


translate spanish day3_evening_un_52acfaa6:


    "Caminé acercándome al río y llamé a Lena."


translate spanish day3_evening_un_6bf1e9fc:


    me "¡Mira!"


translate spanish day3_evening_un_5732579f:


    "El embarcadero, el cobertizo y la luna se estaban reflejando en el agua."


translate spanish day3_evening_un_4de4a131:


    "Parecía como otro mundo con la superfície del agua como puerta al país de las maravillas.{w} Podrías tan solamente saltar y encontrarte en el otro extremo."


translate spanish day3_evening_un_983cba26:


    me "¿Puedo tener este baile?"


translate spanish day3_evening_un_cc87c2d0:


    "Extendí mi mano hacia ella y me incliné torpemente."


translate spanish day3_evening_un_36d02627:


    "Lena dudó.{w} Debo haber exagerado con mis gestos."


translate spanish day3_evening_un_d77a4476:


    me "No hay nada que temer, no soy bueno tampoco."


translate spanish day3_evening_un_981f5474:


    th "¿Por qué «tampoco»?{w} Ella no dijo que no supiera bailar, aunque fuera algo obvio."


translate spanish day3_evening_un_b993fafe:


    un "Vale."


translate spanish day3_evening_un_d32a46e3:


    "Lena me dio su mano, la llevé un poco hacia a mi lado y suavemente puse mis brazos rodeando su cintura."


translate spanish day3_evening_un_2c69e0e2:


    "Estuvimos solamente quietos así por unos breves instantes."


translate spanish day3_evening_un_6cb44a45:


    un "¿Y qué es lo siguiente?"


translate spanish day3_evening_un_bdc4a7dc:


    me "Bueno. No lo sé...{w} ¿Te acuerdas de aquella canción?"


translate spanish day3_evening_un_38866932:


    un "Apenas...{w} Pero la recuerdo."


translate spanish day3_evening_un_cdeaa5d1:


    me "¡Genial! Hagamos un vals como en las películas."


translate spanish day3_evening_un_e0124613:


    un "¿Cómo es eso?"


translate spanish day3_evening_un_dd7ae836:


    "En vez de responderle comencé cuidadosamente a moverme en círculos con Lena."


translate spanish day3_evening_un_2d6366e8:


    me "Ves. ¡No es tan difícil después de todo!"


translate spanish day3_evening_un_a35af87c:


    un "Claro..."


translate spanish day3_evening_un_480cbce8:


    "Estuvimos bailando al compás durante varios minutos."


translate spanish day3_evening_un_0e0749ce:


    "O como quieras llamarle a ello."


translate spanish day3_evening_un_16ce780a:


    "Sentí su calor, a pesar de que no estábamos cerca el uno del otro."


translate spanish day3_evening_un_3ff4b37b:


    "Su pecho respiraba profundamente y su rostro comenzó a ruborizarse más y más."


translate spanish day3_evening_un_2c5d4171:


    "Lena no me miró, evitando su mirada de un lugar a otro."


translate spanish day3_evening_un_fdcc8c70:


    "Repentinamente me di cuenta de que nunca antes había sentido algo así."


translate spanish day3_evening_un_70d5f90d:


    "Era la ternura que se estaba apoderando de la realidad, como si me hubiera hallado a mi mismo en otra, en un mundo mejor."


translate spanish day3_evening_un_2d5d1563:


    "Me percaté de que no quería dejar escapar a esta muchacha, y que daría cualquier cosa por estar tan solamente bailando este vals con ella por siempre."


translate spanish day3_evening_un_b3998185:


    "Abracé a Lena más estrechamente y, solamente entonces, fue cuando ella me miró directamente."


translate spanish day3_evening_un_ef0ec8a2:


    "Había sorpresa y confusión en su mirada, pero sin temor alguno."


translate spanish day3_evening_un_5c5e383c:


    "Ella no estaba preocupada por mi y tampoco se separó de mi."


translate spanish day3_evening_un_cc8b4535:


    un "Y dijiste que no sabías bailar."


translate spanish day3_evening_un_1ec032b9:


    me "En verdad no sé..."


translate spanish day3_evening_un_615f491b:


    "Estaba confuso."


translate spanish day3_evening_un_20470c50:


    "No esperaba esa reacción de ella."


translate spanish day3_evening_un_9dc68533:


    "¿Dónde habían desaparecido su confusión, timidez y miedo?"


translate spanish day3_evening_un_48c7652f:


    me "Tu también... Bailas bastante bien..."


translate spanish day3_evening_un_466707e6:


    un "¡Ya lo sé!"


translate spanish day3_evening_un_f8ce941d:


    "Una sonrisa juguetona se esbozaba en su rostro."


translate spanish day3_evening_un_ea63114f:


    "¿O tal vez solamente me lo parecío a mi?"


translate spanish day3_evening_un_3e1fa0d9:


    "No, juraría que la vi."


translate spanish day3_evening_un_160309c6:


    th "¿Cómo era posible?"


translate spanish day3_evening_un_d8f01710:


    "La imagen de la tímida y modesta Lena no encajaba para nada con este momento."


translate spanish day3_evening_un_d941a3d5:


    th "¿Qué debería decir?{w} ¿Qué es lo que debería hacer?"


translate spanish day3_evening_un_93db6486:


    "Podría sólo permanecer con ella en este vals bailando, lo cual era cada vez más extraño con cada minuto que pasaba."


translate spanish day3_evening_un_c81d9d23:


    mt "¡Semyon! Semyon, ¿dónde estás?"


translate spanish day3_evening_un_438b28d5:


    "La voz provenía de los alrededores del bosque."


translate spanish day3_evening_un_e5902723:


    th "¡Maldita sea, en que mal momento!"


translate spanish day3_evening_un_105fc865:


    mt "¡Semyon!"


translate spanish day3_evening_un_980eb22f:


    "Era Olga Dmitrievna.{w} Debe haber estado preocupada porque debería haber vuelto hace ya rato."


translate spanish day3_evening_un_22027067:


    "¡Debería haber supuesto que la incansable líder del campamento empezaría a buscar al {i}perdido{/i} pionero!"


translate spanish day3_evening_un_d26f19fa:


    th "Debería haberla avisado de antemano."


translate spanish day3_evening_un_f5e453bf:


    th "¡Oh! No tiene ningún sentido pensar sobre ello ahora..."


translate spanish day3_evening_un_74229088:


    "Lena me miraba de forma inquisitiva."


translate spanish day3_evening_un_42de22f4:


    un "Ser vistos juntos no sería una buena idea."


translate spanish day3_evening_un_f14f0728:


    me "¿Por qué?{w} Vamos a explicarle que todo está bien."


translate spanish day3_evening_un_211c6e09:


    un "No. Vamos a escondernos y volveremos al campamento más tarde."


translate spanish day3_evening_un_93c1d6e9:


    th "Su idea era extraña.{w} Todo estaba yendo tan bien..."


translate spanish day3_evening_un_5e1c6dc6:


    th "Al fin algo empezaba a funcionar.{w} O lo parecía..."


translate spanish day3_evening_un_04f3f218:


    th "Pero, ¿qué era ese {i}algo{/i}?"


translate spanish day3_evening_un_c5b4a9e1:


    me "Pues vale."


translate spanish day3_evening_un_83831235:


    "Decidí no discutir, no era apropiado en dicha situación."


translate spanish day3_evening_un_e073d1d4:


    "Olga Dmitrievna gritó por un rato y luego se fue."


translate spanish day3_evening_un_2308a96f:


    un "Vayámonos."


translate spanish day3_evening_un_9513cd87:


    me "Sí..."


translate spanish day3_evening_un_63803f24:


    "Lena no se agarró a mi brazo en el camino de vuelta."


translate spanish day3_evening_un_a604adb5:


    "Estaba un poquito frustrado, pero no estaba lo bastante seguro de dar el primer paso."


translate spanish day3_evening_un_31cc1eca:


    "Permanecimos en silencio otra vez. No era típico de Lena comenzar una conversación y simplemente no sabía qué decir tras todo lo ocurrido en el embarcadero."


translate spanish day3_evening_un_89f67cb6:


    "Ella se quedó todo el rato mirando al suelo en el camino de vuelta, manteniendo su «habitual» expresión."


translate spanish day3_evening_un_985381f5:


    "¡Qué cambio más raro!"


translate spanish day3_evening_un_46255706:


    "O para ser más precisos, lo que era raro era aquella sonrisa en el embarcadero y las palabras de Lena."


translate spanish day3_evening_un_a20cefa7_1:


    "..."


translate spanish day3_evening_un_4f71cd7e:


    "Ella se paró en la plaza."


translate spanish day3_evening_un_ccdf7460:


    un "Bueno. Tengo que irme."


translate spanish day3_evening_un_9513cd87_1:


    me "Vale..."


translate spanish day3_evening_un_375d8a14:


    un "Gracias... por lo de hoy..."


translate spanish day3_evening_un_9513cd87_2:


    me "Sí..."


translate spanish day3_evening_un_26a8bbab:


    "Lena se dio la vuelta y se dirigió a su cabaña, y yo permanecí quieto donde estaba."


translate spanish day3_evening_un_59d28a2d:


    th "¿Qué demonios fue eso?"


translate spanish day3_evening_un_8e35b95f:


    th "Ese baile, ese cambio repentino en su carácter y luego todo volvió a la normalidad."


translate spanish day3_evening_un_a2d69018:


    "Era como si estuviera abrazando otra Lena en ese corto momento."


translate spanish day3_evening_un_a5692cd4:


    "¡Correcto! No era ella misma. Ella parecía una persona completamente distinta."


translate spanish day3_evening_un_c579109c:


    th "¿Quizá no la conozco lo suficientemente bien?"


translate spanish day3_evening_un_92dafc98:


    "Antes tuve la sensación de que Lena escondía algo bajo esa máscara de timidez."


translate spanish day3_evening_un_79930909:


    "Lleva más de dos días poder comprender a alguien."


translate spanish day3_evening_un_c154d46b:


    th "¡Maldita sea! ¡¿Qué debería hacer?!{w} ¡Quizá sólo estoy imaginando cosas!"


translate spanish day3_evening_un_a20cefa7_2:


    "..."


translate spanish day3_evening_un_f4822807:


    "Caminé hacia la cabaña de la líder."


translate spanish day3_evening_un_cec9d8ef:


    "El campamento entero estaba durmiendo por lo que no había nadie que me viera."


translate spanish day3_evening_un_dd369d3a:


    th "¿Y qué?{w} ¡¿Qué tiene de malo que un pionero se dé un paseo por la noche!?"


translate spanish day3_evening_un_35589041:


    "Cuando ya estaba casi cerca de la puerta, escuché un ruido detrás de mí."


translate spanish day3_evening_un_c6f6cc81:


    "Las brancas del arbusto al lado de los árboles crujieron como si alguien se hubiera movido ahí."


translate spanish day3_evening_un_3bc47b84:


    th "No debería haber ningún animal salvaje por aquí cerca..."


translate spanish day3_evening_un_2bdac6ec:


    th "¡Alguien debe haber estado observándome!"


translate spanish day3_evening_un_8591c155:


    "Justo en ese momento salí corriendo en dirección hacia al ruido, me adentré por la fuerza en los arbustos y busqué por los alrededores."


translate spanish day3_evening_un_258661a5:


    "Estaba muy oscuro así que no vi nada o a nadie."


translate spanish day3_evening_un_d49de170:


    "No tenía sentido continuar. Incluso si alguien me estaba observando, debe haberse ido ya."


translate spanish day3_evening_un_f54ea053:


    "Me fui de vuelta a la cabaña."


translate spanish day3_evening_un_76db5439:


    "La luz de dentro estaba encendida."


translate spanish day3_evening_un_d336a541:


    th "Parece que todavía está despierta."


translate spanish day3_evening_un_37715fc9:


    mt "¡Semyon! ¡¿Dónde has estado?!"


translate spanish day3_evening_un_72311896:


    me "¿Qué quieres decir con «dónde»?{w} Yo y Lena hemos estado ordenando los suministros médicos en la enfermería. Ya te lo dije antes que haría eso por la noche."


translate spanish day3_evening_un_d858aa01:


    mt "¿De verdad? ¿Cuándo fue eso? ¡Os llevó mucho tiempo! Estuve en la enfermería hace una media hora antes, ¡estaba cerrada y las luces apagadas!"


translate spanish day3_evening_un_8125eef3:


    me "Bueno... Ejem... Decidimos dar una pequeño paseo."


translate spanish day3_evening_un_a91b818e:


    mt "¡Yo preocupándome por vosotros, mientras vosotros estabais dando un paseo!"


translate spanish day3_evening_un_a81f88a1:


    me "Bueno, lo siento...{w} Te lo haré saber la próxima vez."


translate spanish day3_evening_un_140addaa:


    mt "¡Por favor hazlo!{w} Ve a la cama, ¡es tarde!"


translate spanish day3_evening_un_254cc408:


    "No puedo estar más de acuerdo con ella."


translate spanish day3_evening_un_f2607f9a:


    th "Había sido un día difícil en muchos sentidos."


translate spanish day3_evening_un_89fdb957:


    "Estaba demasiado cansado para reflexionar sobre lo ocurrido por la noche y dudo que tuviera cualquier pensamiento decente al respecto..."


translate spanish day3_evening_un_95c84c3a:


    th "La cosa más importante es que no tengo ni una idea de cómo reaccionar frente a todo esto."


translate spanish day3_evening_un_d0119574:


    th "Con Lena, con el campamento, con todo este mundo."


translate spanish day3_evening_un_d12034ba:


    "Prácticamente no invertí ni un minuto en buscar respuestas.{w} Más aun he estado evitándolas como si la posibilidad de descubrirlas me asustara."


translate spanish day3_evening_un_5db7cdc5:


    "Y Lena...{w} El tiempo que he pasado con ella era mucho más importante que todos mis intentos por retornar al mundo {i}real{/i}."


translate spanish day3_evening_un_9e50ec2f:


    "Podría preocuparme durante un buen rato, intentando comprender qué hice mal y qué no hice, pero el cansancio se apoderó de mi..."


translate spanish day3_evening_us_8e722b79:


    "Slavya se fue."


translate spanish day3_evening_us_1a407541:


    "Probablemente no debería rechazarla después de todo lo que hizo por mi, pero no estaba hecho para actividades como ésta."


translate spanish day3_evening_us_324a06ef:


    "Los pioneros siguieron bailando."


translate spanish day3_evening_us_d21cca78:


    th "Al menos les gustaba el baile."


translate spanish day3_evening_us_ea24578a:


    "Olga Dmitrievna también bailaba."


translate spanish day3_evening_us_ea82e2f0:


    "En mi opinión eso no es muy apropiado.{w} La líder tiene que mantener el orden."


translate spanish day3_evening_us_78fb937a:


    th "Especialmente puesto que ella no tiene 17 años."


translate spanish day3_evening_us_f2be2b10:


    "Olga Dmitrievna se me acercó como si sintiera que alguien dudara de su profesionalidad."


translate spanish day3_evening_us_0f6bc7b5:


    mt "¿Por qué no estás bailando?"


translate spanish day3_evening_us_8717f26e:


    me "En verdad no quiero..."


translate spanish day3_evening_us_b0e280b5:


    mt "Bueno, es tu opción."


translate spanish day3_evening_us_945c59bc:


    "Sonrió pícara."


translate spanish day3_evening_us_31a7a515:


    mt "¡Pues tengo una tarea perfecta para ti!"


translate spanish day3_evening_us_e473fa2f:


    me "Una tarea, ¿cuál es...?"


translate spanish day3_evening_us_a1f730b2:


    "Cualquier otra actividad me parecía mejor que bailar."


translate spanish day3_evening_us_bc92d844:


    mt "Hoy limpiaste la cantina muy bien, pero...{w} Pero creo que aun no has reparado tu error completamente."


translate spanish day3_evening_us_8941bd9d:


    me "¿Ehhh?"


translate spanish day3_evening_us_8403b986:


    mt "¡Ulyana!"


translate spanish day3_evening_us_123f5418:


    us "¡¿Qué?!"


translate spanish day3_evening_us_7f1e0dc0:


    mt "Acércate."


translate spanish day3_evening_us_86cbd5c6:


    "Ulyana se nos acercó renuentemente."


translate spanish day3_evening_us_1e9714fe:


    mt "Veo que ya has tenido suficiente baile por hoy como para venir aquí."


translate spanish day3_evening_us_6c304510:


    us "No, ¡no es suficiente!"


translate spanish day3_evening_us_eb9a46a9:


    "Estaba sudando como una cerda, por lo que parecía que la líder tenía razón."


translate spanish day3_evening_us_9a131f0d:


    mt "Tengo una tarea para ti y Semyon."


translate spanish day3_evening_us_4b8e25e2:


    us "¡Pero Olga Dmitrievna...!"


translate spanish day3_evening_us_4aca71e6:


    "Ulyana suplicó."


translate spanish day3_evening_us_c588b18c:


    us "¡Ahora mismo es la hora de bailar! ¡Y es muy tarde!"


translate spanish day3_evening_us_63ea0fa5:


    mt "No llevará mucho rato.{w} Slavya estuvo ordenando los libros en la biblioteca, pero no ha terminado..."


translate spanish day3_evening_us_c4a407cc:


    mt "Solamente tenéis un par de estanterías para hacer."


translate spanish day3_evening_us_e566dcaf:


    us "Pero..."


translate spanish day3_evening_us_c2f28fc6:


    mt "¡No hay peros!"


translate spanish day3_evening_us_888f7373:


    th "Por el amor de Dios."


translate spanish day3_evening_us_8b0f570d:


    "No me gusta el trabajo social, pero bailar..."


translate spanish day3_evening_us_7f594afe:


    me "¡Estoy preparado!"


translate spanish day3_evening_us_d0e475bd:


    mt "¡Buen trabajo, Semyon! ¡Ése es mi muchacho! ¡Un auténtico pionero! ¡Deberías seguir su ejemplo Ulyana!"


translate spanish day3_evening_us_8378599b:


    "Ulyana no apreciaba semejante ejemplo a pesar de todo."


translate spanish day3_evening_us_88adb859:


    mt "¡Así que hazlo!{w} ¡Todo el campamento confía en ti!"


translate spanish day3_evening_us_d172a747:


    us "Me acordaré de esto."


translate spanish day3_evening_us_86a7768d:


    "Siseó."


translate spanish day3_evening_us_7cf397bf:


    "El campamento por la noche era bello.{w} El silencio y la paz me calmaba."


translate spanish day3_evening_us_b69e6c0f:


    "Solamente la música distante de la plaza me recordaba que no estaba solo aquí."


translate spanish day3_evening_us_38d5d35e:


    "Y Ulyana quien volvió recientemente."


translate spanish day3_evening_us_79dbaa1d:


    me "Supongo que podrás limpiar con tu vestido."


translate spanish day3_evening_us_a026df63:


    "Sonreí con una mueca."


translate spanish day3_evening_us_fb48ad0c:


    us "¡Esto es todo culpa tuya!"


translate spanish day3_evening_us_74af0838:


    "Respiró profundamente y su rostro estaba tan rojo como un tomate, por lo que no estaría sorprendido si empezara a salirle humo de sus orejas."


translate spanish day3_evening_us_0ab904a2:


    us "¡Tú otra vez!{w} ¡Todo es culpa tuya!"


translate spanish day3_evening_us_6a9fa49b:


    me "¿Por qué yo otra vez?"


translate spanish day3_evening_us_0f170283:


    us "¡Si solamente te hubieses estado quieto!"


translate spanish day3_evening_us_21221d76:


    me "¿Qué hubiera cambiado?{w} Ella nos habría mandado hacerlo de todas formas."


translate spanish day3_evening_us_5a606f08:


    us "Tú..."


translate spanish day3_evening_us_a2d9c3e3:


    "No se veía capaz de hablar con normalidad y solamente siseaba."


translate spanish day3_evening_us_5bbc2e7c:


    me "¿Y qué si hubiera estado en silencio?{w} ¿Crees que te habría dejado estar divirtiéndote?"


translate spanish day3_evening_us_c1625886:


    "Ulyana me miró."


translate spanish day3_evening_us_49ae3aab:


    "Parecía que se tranquilizaba un poco."


translate spanish day3_evening_us_8fa6a151:


    us "¡En serio! ¡Tampoco te interesa!{w} ¡No sabes ni bailar!"


translate spanish day3_evening_us_9574dd57:


    me "¿Y qué si no sé?"


translate spanish day3_evening_us_05b51e5d:


    us "Si hubieras bailado conmigo...{w} ¡Me habría divertido!"


translate spanish day3_evening_us_f51acc4b:


    "Volvió a su habitual carácter «infantil»."


translate spanish day3_evening_us_b3acf935:


    me "Podría haberlo hecho...{w} Pero... ya ves como se volvieron las cosas..."


translate spanish day3_evening_us_1d9ef291:


    "No tendremos otra oportunidad para ver quién es mejor bailando."


translate spanish day3_evening_us_90a2bbc6:


    "Nos acercamos a la biblioteca. Estaba ya muy oscura por fuera."


translate spanish day3_evening_us_f49d96cb:


    "Apenas estaba sorprendido por la petición de olga Dmitrievna...{w} o más bien la orden...{w} de ordenar los libros por la noche."


translate spanish day3_evening_us_bc244cfb:


    th "Esto es muy extraño."


translate spanish day3_evening_us_a7e2aa20:


    "No había ni una luz por dentro y el interruptor no funcionaba, con disgusto lo apretaba para escuchar sólo un «click»."


translate spanish day3_evening_us_d3f5e62a:


    "Quizá funcionaba correctamente, pero las luces no se encendían."


translate spanish day3_evening_us_f09b37ef:


    us "Espera, voy a por las velas."


translate spanish day3_evening_us_681ab9c1:


    "Estaba a punto de preguntarle cómo sabía ella dónde se hallaban las velas y las cerillas, cuando dos pequeñas luces aparecieron en una mesa a mi lado."


translate spanish day3_evening_us_7827470a:


    us "¡Así está mejor!"


translate spanish day3_evening_us_574bbbd5:


    "Dijo Ulyana, satisfecha consigo misma."


translate spanish day3_evening_us_3f38f9a0:


    me "¿Y ahora qué?"


translate spanish day3_evening_us_fbf7b967:


    us "¿Cómo que ahora qué?"


translate spanish day3_evening_us_c349ed5f:


    "Ella me miraba inquisitivamente."


translate spanish day3_evening_us_050fe076:


    me "¿Qué haremos ahora?"


translate spanish day3_evening_us_8232daec:


    us "Puff, ¿cómo quieres que lo sepa?"


translate spanish day3_evening_us_393056bc:


    "Soltó una risita."


translate spanish day3_evening_us_b57abe01:


    me "Genial."


translate spanish day3_evening_us_6046a48f:


    "Caminé alrededor de las estanterías de la biblioteca."


translate spanish day3_evening_us_a5101d38:


    th "Olga Dmitrievna dijo alguna cosa como «un par de estanterías»..."


translate spanish day3_evening_us_0e2a5ebd:


    "Toqué los libros para asegurarme de que no tuvieran polvo en ellos."


translate spanish day3_evening_us_f637ef9a:


    "Pareciera que alguien hizo un gran trabajo antes que nosotros."


translate spanish day3_evening_us_d054667b:


    "Habiendo examinado toda la biblioteca no encontré nada que limpiar.{w} La líder debe haberse equivocado."


translate spanish day3_evening_us_eb36cc9f:


    "Repentinamente escuché pisadas detrás de mí."


translate spanish day3_evening_us_cca19138:


    "El viejo y agrietado suelo chirriaba así que Ulyana no pasó inadvertida para cogerme desprevenido."


translate spanish day3_evening_us_08ef6f67:


    us "¡Buu!"


translate spanish day3_evening_us_468f92d5:


    "Me di la vuelta."


translate spanish day3_evening_us_9e09f5fa:


    me "Oh Dios mío. Estoy tan asustado, por favor..."


translate spanish day3_evening_us_80eb9adc:


    us "¡Oh, da igual!"


translate spanish day3_evening_us_e2993c31:


    "Se alejó resentida."


translate spanish day3_evening_us_4ab03e66:


    me "Parece bastante limpio aquí, así que..."


translate spanish day3_evening_us_c1661c00:


    "Los lomos de los libros vagamente iluminados me miraban reprochándomelo."


translate spanish day3_evening_us_4c1d899a:


    "Autores desconocidos de hace montones de años olvidados."


translate spanish day3_evening_us_51f94d8f:


    th "Me pregunto si todavía alguien se acuerda de ellos...{w} Zhenya debe acordarse."


translate spanish day3_evening_us_b78d5ef3:


    "Estoy seguro de que ella recuerda todo."


translate spanish day3_evening_us_fce45449:


    us "Vale. Sentémonos."


translate spanish day3_evening_us_dc9d3ef6:


    "Ulyana me acercó una silla.{w} Me senté en ella."


translate spanish day3_evening_us_fecfa4ed:


    me "Bueno, ¿y ahora qué?"


translate spanish day3_evening_us_5d9c94b9:


    us "Vamos a contarnos historias de miedo.{w} Tenemos dos sillas y dos velas, por lo que debería ser dos historias, una mía y otra tuya."


translate spanish day3_evening_us_cd917d11:


    me "Vale."


translate spanish day3_evening_us_0e57cf1c:


    "Estuve de acuerdo sin pensarlo."


translate spanish day3_evening_us_c76d5efb:


    "No tenía ni la más remota intención de volver al baile y por mi experiencia sé que no había nada que hacer en el campamento por la noche."


translate spanish day3_evening_us_1da6e1e4:


    "Especialmente sabiendo un par de buenas historias con las que podría asustar realmente a Ulyana."


translate spanish day3_evening_us_a38cf680:


    "Y eso era estupendo."


translate spanish day3_evening_us_959cb1f4:


    me "Tú primero."


translate spanish day3_evening_us_1549669b:


    us "Vale."


translate spanish day3_evening_us_906544fe:


    "Se puso cómoda al revés, rodeando con sus brazos la parte trasera de la silla y aproximó la vela a su rostro lo más cerca posible."


translate spanish day3_evening_us_517ac6ae:



    nvl clear
    "{i}Érase una vez, en un pueblo lejano vivía un muchacho. Tan sólo un muchacho normal. Iba a la escuela, jugaba con otros niños. No había nada de especial en él.{/i}"


translate spanish day3_evening_us_e153dd08:


    "{i}Un día hizo una apuesta con su amigo conforme no tenía miedo de ir a la casa abandonada...{/i}"


translate spanish day3_evening_us_82b72ef3:


    "Ulyana hizo una pausa larga."


translate spanish day3_evening_us_4d3eecb3:


    me "¿Y qué había en esa casa?"


translate spanish day3_evening_us_cd35d04d:


    us "¡No me interrumpas!"


translate spanish day3_evening_us_4d2c229a:


    me "¿Te lo estás inventando mientras lo explicas?"


translate spanish day3_evening_us_39cf5c52:


    "Hizo un puchero con sus labíos y continuó."


translate spanish day3_evening_us_f5578357:


    "{i}Decían que una bruja había vivido en aquella casa y la gente todavía veía su fantasma allí por la noche. Nadie lo sabía con seguridad, pero todo el mundo estaba asustado.{/i}"


translate spanish day3_evening_us_f3da10f9:


    "{i}El muchacho había dicho que todo eso no tenía sentido y que él estaba preparado para pasarse toda la noche allí. Así que lo hizo. Pero no retornó por la mañana.{/i}"


translate spanish day3_evening_us_31aee4ac:


    "{i}Se le encontró ahorcado.{/i}"


translate spanish day3_evening_us_eb137a30:



    nvl clear
    "Ulyana volvió a parar en su explicación otra vez."


translate spanish day3_evening_us_fd837679:


    me "Emocionante...{w} ¿Se ahorcó él mismo con sus cordones de las zapatillas?"


translate spanish day3_evening_us_abfdf469:


    "Dije escépticamente."


translate spanish day3_evening_us_306e9012:


    "Ella frunció el ceño nuevamente."


translate spanish day3_evening_us_6a86fe31:


    me "¿Es ése el final?"


translate spanish day3_evening_us_c25bebac:


    us "¡Claro que no!"


translate spanish day3_evening_us_81736160:


    "{i}Enterraron el cuerpo del muchacho. De la forma en que se supone que se debe hacer. En un ataúd y esas cosas.{/i}"


translate spanish day3_evening_us_387cb49c:


    "{i}Sus parientes y amigos lo lloraron lamentándose, pero no podían traérlo a la vida de nuevo.{/i}"


translate spanish day3_evening_us_4498a264:


    "{i}Tras unos días, la gente comenzó a desaparecer en el pueblo. Nadie sabía cómo y por qué. Simplemente se desvanecieron en la nada.{/i}"


translate spanish day3_evening_us_2b80bb65:


    "{i}Los habitantes del pueblo querían llamar a la policía, pero un amigo de aquel muchacho que se ahorcó a sí mismo en la casa de la bruja, les explicó que había visto al muchacho. Nadie le creyó al principio, pero la gente continuó desapareciendo.{/i}"


translate spanish day3_evening_us_0b0a71f5:



    nvl clear
    "{i}Entonces la gente del pueblo decidió cavar en su tumba. Habían grandes arañazos de las uñas en su interior. Pero no había ningún cuerpo.{/i}"


translate spanish day3_evening_us_c6ee723c:


    "{i}¿Dónde se había ido el cadáver?{/i}"


translate spanish day3_evening_us_afd82e5f:


    "Ulyana se alejó la vela e hizo una cara de miedo (o al menos ella creía que daba miedo)."


translate spanish day3_evening_us_725f29b0:


    "{i}Al final todos los habitantes del pueblo desaparecieron, y los transeúentes ocasionales que pasaban cerca de la casa de la bruja, explicaban que habían visto dos fantasmas.{/i}"


translate spanish day3_evening_us_76b2fe88:


    nvl clear


translate spanish day3_evening_us_c65a5a14:


    "Ulyana sopló sobre su vela y terminó de hablar."


translate spanish day3_evening_us_56417cdb:


    "Parecía que la historia había acabado."


translate spanish day3_evening_us_d1bfd0cf:


    me "¡Maravillosa!"


translate spanish day3_evening_us_bfb0f205:


    "Aplaudí."


translate spanish day3_evening_us_5bdb65dc:


    me "No vale como para un Premio Pulitzer, pero..."


translate spanish day3_evening_us_d6c3ca28:


    us "¿Estás asustado?{w} ¿Tienes miedo?"


translate spanish day3_evening_us_6c2b1b7e:


    me "Oh, sólo estoy temblando..."


translate spanish day3_evening_us_f4067405:


    us "¡Bah! Da igual...{w} Cuenta tu historia pues.{w} ¡Estoy segura de que ni me estremeceré!"


translate spanish day3_evening_us_59133844:



    nvl clear
    "Recogí varias ideas y escogí explicar una historia que había leído en el blog de un conocido mío hace pocos meses atrás."


translate spanish day3_evening_us_0864a00e:


    "Él era bastante bueno contando historias. Al menos me gustaba su estilo. Así que el éxito estaba garantizado."


translate spanish day3_evening_us_cf924028:


    "Ciertamente, no me acordaba de la historia exactamente palabra a palabra, así que la expliqué con mis propias palabras."


translate spanish day3_evening_us_56bad404:


    "{i}Hay una lejana estación espacial, en la última frontera de la humanidad, situada en un extremo junto a una civilización hostil. Al tercer mes de un alto al fuego.{/i}"


translate spanish day3_evening_us_0fe7b509:


    "{i}Realmente no se le puede llamar un alto al fuego, más bien una «no guerra». Un pequeño grupo de supervivientes está exhausto tras un largo asedio.{/i}"


translate spanish day3_evening_us_6dc81529:


    "{i}Por una parte no pueden retirarse, abandonando el puesto fronterizo. Por otra parte, comprenden que no resistirán ni un minuto más si el enemigo ataca.{/i}"


translate spanish day3_evening_us_04d2b110:



    nvl clear
    "{i}Desesperación es la mejor palabra para describir su situación.{/i}"


translate spanish day3_evening_us_68e6fa98:


    "{i}La comida se está agotando, solamente hay munición para unos pocos disparos los cuales más bien parecen como fuegos artificiales para la bienvenida.{/i}"


translate spanish day3_evening_us_0f0fc437:


    "{i}Sólo hay un puñado de aire y agua, debido a los sistemas de soporte vital.{/i}"


translate spanish day3_evening_us_420be6e0:


    "{i}La gente no se habla entre sí desde hace semanas. Han llegado al punto en el cual no ven ningún sentido en malgastar su tiempo en comunicarse cuando la muerte les acecha tras la puerta.{/i}"


translate spanish day3_evening_us_b4d08157:


    "{i}O detrás del grosor del fuselaje de la estación para ser más precisos.{/i}"


translate spanish day3_evening_us_f910168c:


    "{i}En verdad es mejor simplemente esperar a ser rescatados o bien al ataque del enemigo. Ambos resultados conllevarán el fin de esta tortura.{/i}"


translate spanish day3_evening_us_9e98cab6:



    nvl clear
    "{i}Esta situación es como una partida de ajedrez contra un oponente invisible. Es la parte más difícil... el final del juego. Cualquier paso en falso conllevará la derrota. Eres tú o tu enemigo.{/i}"


translate spanish day3_evening_us_6880febd:


    "{i}¿Pero acaso lo sabe el enemigo? Pueden estar preocupados de hacer un paso en falso también. Mientras tanto los humanos podrían mover su rey. Pero tan solamente una casilla adelante y luego otra casilla atrás otra vez.{/i}"


translate spanish day3_evening_us_38c79fcf:


    "{i}Cualquier otro movimiento conllevaría la derrota automáticamente. Un paso adelante implica una amenaza de ataque. Y un paso atrás es una deliberada necesidad de defensa. El juego sería mucho más fácil si el oponente estuviera sentado frente a ti. Incluso un jugador de ajedrez experimentado no puede ocultar los movimientos oculares por los nervios, una esporádica gota de sudor o un temblor de mano.{/i}"


translate spanish day3_evening_us_22f0a385:


    "{i}Por un lado todo es fútil, todo el mundo es consciente de que la situación es desesperada, ya que nadie escogería hacer un movimiento para escapar de la derrota inminente.{/i}"


translate spanish day3_evening_us_35856953:



    nvl clear
    "{i}Por otro lado, cuando ves a tu oponente frente a ti, comprendiendo que es totalmente como tú y que puede cometer un error, puedes apostarlo todo más fácilmente. El oponente puede sentir las mismas dificultades.{/i}"


translate spanish day3_evening_us_4973842c:


    "{i}Éstas son las circunstancias de los pocos hombres y mujeres en el borde limítrofe del universo. No había órdenes del cuartel general desde hacía varios días.{/i}"


translate spanish day3_evening_us_7c9be39f:


    "{i}A pesar de que las últimas órdenes no tenían ninguna utilidad, solamente la habitual petición de mantener la defensa.{/i}"


translate spanish day3_evening_us_f87c860d:


    "{i}Una casilla adelante, una casilla atrás. La oscilación de un péndulo eterno.{/i}"


translate spanish day3_evening_us_f34c1a35:


    "{i}Al inicio del cuarto mes del alto el fuego, el comandante decide retirarse.{/i}"


translate spanish day3_evening_us_f2ccbac0:


    "{i}Sus motivos son que las vidas de sus soldados son más importantes que los ideales míticos de la humanidad. Nadie le juzgará por esto.{/i}"


translate spanish day3_evening_us_f5762547:



    nvl clear
    "{i}Más aun, ellos no pueden cambiar nada.{/i}"


translate spanish day3_evening_us_0117105e:


    "{i}Los preparativos no llevan mucho tiempo. El bien más importante que se puede salvar del hundimiento de un barco es tu propia vida.{/i}"


translate spanish day3_evening_us_ce3c3dd4:


    "{i}Toda la tripulación de la estación abordan la embarcación de salvavidas. La secuencia de lanzamiento empieza. Tres, dos, uno... y nada. Las compuertas no se abren.{/i}"


translate spanish day3_evening_us_ec3c9d93:


    "{i}Envían mecánicos para inspeccionar el fallo mecánico. Pero no hallan nada. Las compuertas deberían abrirse. Pero no se abren en el segundo intento. O en el tercero...{/i}"


translate spanish day3_evening_us_e838b9cb:


    "{i}El comandante ordena a su tripulación abandonar la estación con los botes salvavidas. Pero tampoco los pueden lanzar. Dicen que es un fallo técnico. La tripulación trata de ponerse en contacto con el cuartel general en vano.{/i}"


translate spanish day3_evening_us_a22d8215:



    nvl clear
    "{i}Tras unos pocos días el oficial al mando se percata de que no hay rastro en los radares. No hay nada... ni planetas, ni asteroides, ni naves enemigas. Solamente una oscuridad sin fin.{/i}"


translate spanish day3_evening_us_44deaf56:


    "{i}Se les acabó la comida en un mes. Todo el mundo cree que éste es el final. Pero nadie muere, no tras un día, una semana e incluso después de un mes.{/i}"


translate spanish day3_evening_us_6d909058:


    "{i}Como si no hubiera necesidad de alimentarse para sustentar la vida humana.{/i}"


translate spanish day3_evening_us_fe89102b:


    "{i}En estos momentos la mayoría de la tripulación se vuelve loca. Algunos de ellos sólo se quedan en sus habitaciones y rezan, algunos vagan alrededor de la estación, otros tratan de suicidarse.{/i}"


translate spanish day3_evening_us_7ed3bc1a:


    "{i}Pero ni los disparos a quemarropa con una pistola de plasma, ni sumergiéndose en un hydrotanque, ni tan siquiera cortándose las venas da ningún resultado.{/i}"


translate spanish day3_evening_us_405821aa:



    nvl clear
    "{i}Los días se suceden, meses o tal vez incluso años... nadie guarda noción alguna del paso del tiempo. La gente loca recupera sus cabales nuevamente y la gente normal se vuelve loca. Se repite múltiples veces.{/i}"


translate spanish day3_evening_us_a8f61a7d:


    "{i}Al final, la tripulación acepta su condición de ser.{/i}"


translate spanish day3_evening_us_f0789661:


    "{i}Empiezan a asumir ocupaciones ociosas: representaciones teatrales, torneos deportivos y lecturas de sus propios libros escritos.{/i}"


translate spanish day3_evening_us_520553c5:


    "{i}Un gran número de parejas de famílias surgen y muchas otras rompen.{/i}"


translate spanish day3_evening_us_df10e82a:


    "{i}Es un flujo de tiempo infinito, un ciclo cerrado de la vida humana. Y tan solamente la oscuridad del radar les recuerda el vacío que hay fuera. Pero también hay un vacío en el interior de la estación.{/i}"


translate spanish day3_evening_us_dd21be31:


    "{i}Al final la gente dejó de vivir. Simplemente permanecieron en sus habitaciones y durmieron. Al principio no las 24 horas de la semana, pero a medida que el tiempo transcurrió aprendieron a caer en estado de letargo.{/i}"


translate spanish day3_evening_us_134f2ab0:



    nvl clear
    "{i}Cada persona tenía sus propios sueños... una retornaba a su infancia, otra conseguía volver a estar con su persona más querida, otra defendía los ideales de la Humanidad con su arma en las manos. Algunas otras sólo vagan en el espacio...{/i}"


translate spanish day3_evening_us_d9aa3d87:


    "{i}En un lamentable estado tras las escaramuzas con su enemigo, una embarcación de rescate atraca en la estación. El escuadrón de abordaje se adentra.{/i}"


translate spanish day3_evening_us_0bf36ead:


    "{i}La estación entera es como un pedazo de chatarra de metal la cual ha estado flotando en el espacio durante miles de años. El núcleo del reactor se apagó hace ya mucho tiempo. Habían marcas de los disparos de lásers en las paredes. El equipo había sido aplastado por completo.{/i}"


translate spanish day3_evening_us_6b174e6c:


    "{i}Hallaron esqueletos deteriorados de la gente que murió horriblemente defendiendo la frontera de la humanidad en casi todas las habitaciones.{/i}"


translate spanish day3_evening_us_ab489ab3:


    "{i}Y tan solamente un mensaje críptico enviado al cuartel general que implicaba la sospecha de que algo iba mal.{/i}"


translate spanish day3_evening_us_43b9f5d0:



    nvl clear
    "{i}«¡Salvadnos!\n{/i}"


translate spanish day3_evening_us_93ed5913:


    "{i}Sólo queremos morir...»{/i}"


translate spanish day3_evening_us_6b713cd4:


    "Finalicé la historia y miré a Ulyana."


translate spanish day3_evening_us_2c2388cc:


    "Su cara estaba oculta por la oscuridad."


translate spanish day3_evening_us_768354f5:


    me "Así que esa es la historia..."


translate spanish day3_evening_us_e7066533:


    "Apagué soplando la luz de la vela."


translate spanish day3_evening_us_3d0f0f54:


    "Ulyana chilló, se levantó y se lanzó sobre mi."


translate spanish day3_evening_us_574fb6aa:


    "Caímos sobre el suelo."


translate spanish day3_evening_us_a4d2b1c8:


    me "¿Qué pasa contigo?"


translate spanish day3_evening_us_3005a190:


    us "Nada..."


translate spanish day3_evening_us_3cff4607:


    "Su voz sonaba algo asustada."


translate spanish day3_evening_us_871c17c7:


    "Parece que mi historia hizo efecto."


translate spanish day3_evening_us_c163059f:


    "Mentalmente me felicité."


translate spanish day3_evening_us_4c250e85:


    "A pesar de que Ulyana no se hubiera divertido."


translate spanish day3_evening_us_355d45cd:


    "Ella me abrazó, temblaba y sollozaba."


translate spanish day3_evening_us_638d6acb:


    me "¿Qué pasa contigo, tontilla?"


translate spanish day3_evening_us_0192e2f2:


    "Di una palmadita en su cabeza."


translate spanish day3_evening_us_6dc624b2:


    me "Sólo es una historia.{w} No es real."


translate spanish day3_evening_us_24578402:


    us "¡Tú y tus estúpidas historias!"


translate spanish day3_evening_us_00a07cdc:


    "Ulyana me abrazó aun más."


translate spanish day3_evening_us_14314e51:


    me "¿Te has asustado?"


translate spanish day3_evening_us_c463eeea:


    us "Claro..."


translate spanish day3_evening_us_d47079d5:


    "Francamente, no me esperaba tal sincera respuesta por su parte."


translate spanish day3_evening_us_0268fff6:


    me "Bueno, no pasa nada."


translate spanish day3_evening_us_f776f380:


    th "El tiempo cura todas las heridas. Se calmará."


translate spanish day3_evening_us_3e0531a1:


    us "Escucha, Semyon..."


translate spanish day3_evening_us_67c5f2d1:


    me "¿Qué?"


translate spanish day3_evening_us_62db7763:


    us "No importa, no es nada."


translate spanish day3_evening_us_dc1b0573:


    "Enterró su rostro en mi pecho."


translate spanish day3_evening_us_f8253995:


    "Los minutos pasaron."


translate spanish day3_evening_us_a92c5503:


    me "Vale, entiendo... ¿Tal vez deberíamos irnos?"


translate spanish day3_evening_us_0cd20c05:


    "Escuché.{w} Ulyana roncaba silenciosamente."


translate spanish day3_evening_us_61d4bcc5:


    me "¡Ey! ¡Despierta!"


translate spanish day3_evening_us_094ae4ba:


    th "¿Se puede incluso caer rendido y dormirse de miedo?"


translate spanish day3_evening_us_ba0cb0ae:


    me "¡Te digo que despiertes!"


translate spanish day3_evening_us_71063c36:


    "No reacciona."


translate spanish day3_evening_us_c589ce7a:


    "Traté de levantarme."


translate spanish day3_evening_us_20833c6d:


    "Ciertamente Ulyana no pesará mucho más de 40 kilogramos, pero imagínate a ti mismo estando bajo semejante peso."


translate spanish day3_evening_us_81eadbc5:


    "¡Realmente no es tan fácil levantarse!"


translate spanish day3_evening_us_2f516faf:


    "Pensarías que Ulyana estaba muerta si no fuera por su respiración."


translate spanish day3_evening_us_19979d09:


    "Definitivamente podría hacer un mayor esfuerzo..."


translate spanish day3_evening_us_ceb2ee9d:


    th "Pero entonces la despertaría y entonces... ¡sería vuelta a empezar!"


translate spanish day3_evening_us_c52a9778:


    th "Una situación poco envidiable."


translate spanish day3_evening_us_feb9b84c:


    th "Aunque hay la opción de esperar hasta que ella se despierte por sí misma."


translate spanish day3_evening_us_96e8f382:


    th "Desde luego, no se dormirá completamente hasta por la mañana con semejante historia."


translate spanish day3_evening_us_dd9bfa6a:


    "Miré por la ventana el cielo estrellado."


translate spanish day3_evening_us_44c11d50:


    th "Me pregunto si de verdad hay un puesto fronterizo a lo lejos con una tripulación fantasma..."


translate spanish day3_evening_us_8ac1f25d:


    "Mis ojos lentamente se cerraban y en un instante caí dormido..."


translate spanish day3_evening_dv_701cf26a:


    "Unos pocos minutos más tarde me acerqué al escenario."


translate spanish day3_evening_dv_50cb2f7e:


    th "¿Quizás fue una mala idea aceptar su oferta?"


translate spanish day3_evening_dv_13334dce:


    th "Tan solamente hablar con Dvachevskaya ya es peligro..."


translate spanish day3_evening_dv_bdfb9d61:


    th "Por otra parte tampoco tenía deseos de ir a bailar."


translate spanish day3_evening_dv_b864db36:


    th "¿Qué puedo hacer allí?{w} Sólo haría el ridículo..."


translate spanish day3_evening_dv_5815ef70:


    th "No, estoy mejor escuchando a Alisa tocar la guitarra."


translate spanish day3_evening_dv_1340350c:


    "Al acercarme, vi una persona sentada en el borde del escenario con sus piernas colgando."


translate spanish day3_evening_dv_5ee37cbd:


    dv "¡Bueno, viniste después de todo!"


translate spanish day3_evening_dv_171149e1:


    "Alisa apartó la guitarra y saltó al suelo."


translate spanish day3_evening_dv_7c8a24e7:


    "No había ni rastro de alegría en su rostro por ahora.{w} Tocar para alguien seguro que es más interesante que estar solo."


translate spanish day3_evening_dv_2abbfc44:


    me "Como puedes ver..."


translate spanish day3_evening_dv_64374587:


    dv "¿Qué vamos a hacer?"


translate spanish day3_evening_dv_6c4f856c:


    me "¿Qué quieres decir con «qué»?"


translate spanish day3_evening_dv_554549ca:


    dv "Te estoy preguntando."


translate spanish day3_evening_dv_c222ae63:


    me "Bueno... tú querías..."


translate spanish day3_evening_dv_e1d0c3f8:


    "De repente tuve una idea de que este tipo de situaciones suelen ser bromas habituales de ella y que nunca debería haber esperado escucharla tocar canciones con la guitarra."


translate spanish day3_evening_dv_c87e1fd0:


    me "Me voy a la cama. ¡Adiós!"


translate spanish day3_evening_dv_4f545b83:


    "Me di la vuelta y me despedí a propósito lentamente con la mano."


translate spanish day3_evening_dv_93b5fc50:


    dv "¡Ey!"


translate spanish day3_evening_dv_744f6c22:


    "Alisa me agarró por mi manga."


translate spanish day3_evening_dv_ad02f53a:


    me "¿Qué?"


translate spanish day3_evening_dv_18f4b1ad:


    dv "¿No quieres escucharme?"


translate spanish day3_evening_dv_ba6356e6:


    me "¿Escuchar qué? Si tú misma no sabes ni lo que vas a hacer."


translate spanish day3_evening_dv_3ce16a8b:


    dv "¡Claro que lo sé!"


translate spanish day3_evening_dv_55df167e:


    "Ella cogió la guitarra y tocó las cuerdas con un plectro un par de veces."


translate spanish day3_evening_dv_a2494e1f:


    dv "Siéntate a mi lado."


translate spanish day3_evening_dv_eaadd141:


    "No me opuse y en un instante estuve sentado a su lado."


translate spanish day3_evening_dv_55b9978e:


    th "Es mejor así."


translate spanish day3_evening_dv_69e988e0:


    th "Me perdí el baile, me iba ahora, y entonces... ¿Qué hubiera hecho?"


translate spanish day3_evening_dv_994a7e72:


    "El comportamiento de un extranjero era más acorde con el de un observador pasivo que con el de un ansioso de buscar respuestas, el cual había querido ser con muy mal resultado."


translate spanish day3_evening_dv_6a5d85fb:


    dv "Bueno, te mostraré como tocar esta canción ahora."


translate spanish day3_evening_dv_9fb4d40d:


    me "¿Simplemente así? ¿Tal cual?"


translate spanish day3_evening_dv_7f046cee:


    dv "¿Qué quieres decir con «tal cual»?"


translate spanish day3_evening_dv_3eb0fb19:


    me "Hace mucho tiempo que no he cogido una guitarra..."


translate spanish day3_evening_dv_22bda204:


    th "Probablemente ella podría suponer cuánto tiempo habia pasado desde la última vez. Mis habilidades eran suficientemente malas como para hacer reír a un gato."


translate spanish day3_evening_dv_87662605:


    dv "¡No es difícil!"


translate spanish day3_evening_dv_db47df41:


    "Le hice una mirada intensa a Alisa."


translate spanish day3_evening_dv_2f5dd571:


    "Ahora me parece claramente que ella sólo se ve incapaz de expresar sus sentimientos."


translate spanish day3_evening_dv_f464d10c:


    "Si pudiera olvidar su extraño autocontrol por un instante... vería una muchacha común e incluso divertida y simpática."


translate spanish day3_evening_dv_05afbea0:


    "Y cuando su autoconfianza y su insolencia retornan, volvía a estar de mala leche como de costumbre."


translate spanish day3_evening_dv_0d77d9e0:


    th "¿Y cuál es la auténtica?"


translate spanish day3_evening_dv_93b5fc50_1:


    dv "¡Ey!"


translate spanish day3_evening_dv_ad02f53a_1:


    me "¿Qué?"


translate spanish day3_evening_dv_265b67ba:


    dv "¡Para de soñar despierto!"


translate spanish day3_evening_dv_94598a92:


    me "Oh, claro, perdona..."


translate spanish day3_evening_dv_a38c32a6:


    dv "Entonces, ¿me estás viendo?"


translate spanish day3_evening_dv_dc57eba2:


    me "Lo hago. ¿Qué canción es esta?"


translate spanish day3_evening_dv_4d022e04:


    dv "¡La misma que toqué esta mañana, por supuesto!"


translate spanish day3_evening_dv_46559df1:


    me "Oh... Vale. Te escucho."


translate spanish day3_evening_dv_13b39fa1:


    "Alisa se preparó y comenzó a tocar."


translate spanish day3_evening_dv_ce617998:


    "..."


translate spanish day3_evening_dv_56072db3:


    dv "¡Eso es!.{w} ¿Lo cogiste todo?"


translate spanish day3_evening_dv_2a7d102c:


    th "Puede que lo haya entendido, pero probablemente no seré capaz de imitarlo."


translate spanish day3_evening_dv_51584317:


    me "Cómo puedo..."


translate spanish day3_evening_dv_52e5d545:


    dv "Está bien. ¡La práctica hace la perfección!"


translate spanish day3_evening_dv_8f9b4924:


    "Indudablemente ella tiene razón al respecto, pero he tenido problemas con la práctica a lo largo de mi vida."


translate spanish day3_evening_dv_91dd4d67:


    me "Bueno..."


translate spanish day3_evening_dv_77bdacb3:


    "Cogí la guitarra de Alisa e intenté tocar el primer acorde."


translate spanish day3_evening_dv_59bff6c6:


    "Era difícil, como si mis dedos estuvieran atados por un nudo y no quisieran estar en el lugar correcto."


translate spanish day3_evening_dv_008caeac:


    "Solía ser mejor antes, pero ahora me sería imposible incluso tocar la «Campanita del lugar»."


translate spanish day3_evening_dv_dbeba143:


    dv "¿Y bien?"


translate spanish day3_evening_dv_2074dc26:


    me "Espera un momento..."


translate spanish day3_evening_dv_4145963f:


    dv "¡Parece que no serás una estrella del rock!"


translate spanish day3_evening_dv_91623ab5:


    "Ella se puso a reír simpáticamente."


translate spanish day3_evening_dv_4608b651:


    me "Puede que sea mejor así..."


translate spanish day3_evening_dv_f6a527e5:


    "Silvé, intentando vanamente recordar el orden de las cuerdas a tocar."


translate spanish day3_evening_dv_4366e696:


    me "Bueno..."


translate spanish day3_evening_dv_54c3617f:


    "La guitarrá empezó a hacer ruidos endemoniados que se semejaban a quejidos de un dinosaurio herido."


translate spanish day3_evening_dv_a4415adc:


    "Los dedos de mi siniestra constantemente se equivocaban de trastes y mi diestra no tocaba con ritmo."


translate spanish day3_evening_dv_7c983f00:


    me "En fin, llamémoslo rock progresivo."


translate spanish day3_evening_dv_334edc91:


    dv "¿Progresivo? ¡Bueno, bueno! ¡Dámela!"


translate spanish day3_evening_dv_13705a4a:


    "Me quitó la guitarra y sonó completamente diferente en sus manos."


translate spanish day3_evening_dv_11e95cca:


    "Me sentí avergonzado por mi torpeza por un instante."


translate spanish day3_evening_dv_b5823702:


    "La habilidad de Alisa puede que no fuera del nivel de un guitarrista profesional, no obstante tocaba cosas sencillas como esta con confianza."


translate spanish day3_evening_dv_195bfa48:


    "Debía de haber practicado muchísimo."


translate spanish day3_evening_dv_561d4770:


    dv "¿La has cogido esta vez?"


translate spanish day3_evening_dv_d9ffec9a:


    "Terminó y me ofreció la guitarra."


translate spanish day3_evening_dv_c97fadc0:


    me "Bueno..."


translate spanish day3_evening_dv_e9ad5daa:


    "Lo hice mejor la segunda vez, pero todavía seguía estando muy lejos del nivel de Alisa."


translate spanish day3_evening_dv_9422d1b3:


    "O de cualquier nivel suficientemente decente."


translate spanish day3_evening_dv_7c284714:


    "Era consciente de cuán pésimo era comparado con ella."


translate spanish day3_evening_dv_c3b32ce7:


    th "Parecía sencillo... ¡sólo tocaba una canción con tres acordes!"


translate spanish day3_evening_dv_34a8a8e1:


    dv "La melodía era sencilla, a pesar de todo."


translate spanish day3_evening_dv_ee31e548:


    "Dejé la guitarra a un lado."


translate spanish day3_evening_dv_ed1bcf7d:


    "Alisa se ensimismó en sus pensamientos por un instante."


translate spanish day3_evening_dv_f28fc90f:


    dv "¡Hasta un parvulario podría tocar mejor!"


translate spanish day3_evening_dv_e7c47ec2:


    me "Gracias,{w} ¡lo intenté!"


translate spanish day3_evening_dv_e131c7ad:


    dv "¡Ya me di cuenta!"


translate spanish day3_evening_dv_bd64fe14:


    "Quizás trataba de demostrarme su evidente superioridad tocando la guitarra."


translate spanish day3_evening_dv_a9370cae:


    me "Parece que no todo el mundo puede ser un músico."


translate spanish day3_evening_dv_26e899f1:


    dv "Realmente no..."


translate spanish day3_evening_dv_343fa444:


    "Alisa alzó su mirada al cielo."


translate spanish day3_evening_dv_799ffa98:


    dv "Aunque tú no puedes ser uno, ¡eso seguro!"


translate spanish day3_evening_dv_38d5b464:


    me "¿Y eso por qué?"


translate spanish day3_evening_dv_c3abde57:


    dv "¡Deberías haberte escuchado!"


translate spanish day3_evening_dv_43afff04:


    "Empecé a lamentar haber venido aquí."


translate spanish day3_evening_dv_67787d9f:


    th "Bueno, tampoco me importaría. ¿Por qué necesita Alisa hacer esto?{w} Es obvio que soy un guitarrista horrible."


translate spanish day3_evening_dv_8c75e8f3:


    me "Obviamente.{w} Pero si practicara más..."


translate spanish day3_evening_dv_26bc6b0e:


    dv "Bueno, ¿pero practicarás?"


translate spanish day3_evening_dv_968fea51:


    me "No lo sé... Tal vez lo haga."


translate spanish day3_evening_dv_bca2071d:


    "Aunque la guitarra era el último de mis problemas. Estoy atrapado en un mundo extraño del cual no puedo escapar...{w} Eso es más importante que una estúpida discusión con Alisa."


translate spanish day3_evening_dv_67458183:


    "De repente sentí una molesta autocompasión, apatía y una necesidad de abandonar este lugar, de olvidar esta noche."


translate spanish day3_evening_dv_ca179b6a:


    "Aquellos sentimientos dejaban una fea impresión y un deseo ardiente de hacer (o decir) algo estúpido."


translate spanish day3_evening_dv_3fcbf50b:


    me "¡Como si pudiera cambiar algo!"


translate spanish day3_evening_dv_1145d96e:


    dv "¿Qué?"


translate spanish day3_evening_dv_c372f019:


    me "¡Pues yo aprendiendo a tocar la guitarra!"


translate spanish day3_evening_dv_9d34d000:


    dv "¡Seguramente no podrías! Apenas rozarías la guitarra por un puñado de días y luego la dejarías."


translate spanish day3_evening_dv_e7eaa70e:


    "Las palabras de Alisa hirieron mi corazón."


translate spanish day3_evening_dv_5187f093:


    me "¡¿Sabes qué?!"


translate spanish day3_evening_dv_35264e84:


    "Oh bueno, ¿debería {i}saberlo{/i} ella? ¿Acaso es asunto suyo?"


translate spanish day3_evening_dv_05bd68b6:


    "¿Reacciono de esta forma justo porque es la verdad... y porque es Alisa quien dijo esa verdad?"


translate spanish day3_evening_dv_64d153d7:


    me "¡No puedes saber cómo hacerlo todo!"


translate spanish day3_evening_dv_b4c62b8a:


    dv "Probablemente no...{w} Pero deberías poder hacer al menos alguna cosa."


translate spanish day3_evening_dv_6528a29b:


    me "Entonces, ¿me estás diciendo que no puedo hacer nada?"


translate spanish day3_evening_dv_a44772be:


    dv "¿Cómo podría saberlo?"


translate spanish day3_evening_dv_92d27981:


    me "Ya sabes, yo..."


translate spanish day3_evening_dv_21c57864:


    th "¿Yo qué?"


translate spanish day3_evening_dv_380c1152:


    th "¿Debería explicarle mis conocimientos en informática?{w} ¿O sobre los libros que he leído y las películas que he visto?"


translate spanish day3_evening_dv_6660ca80:


    me "Ah bueno, esta es una conversación sin sentido."


translate spanish day3_evening_dv_4a20a990:


    dv "Huelga decirlo."


translate spanish day3_evening_dv_d48bc21c:


    me "¡Solamente te interesa divertirte!"


translate spanish day3_evening_dv_cb92aa34:


    dv "¡Y tú hacer nada salvo quejarte!"


translate spanish day3_evening_dv_297beca3:


    me "¡Olvídalo!"


translate spanish day3_evening_dv_a08e0ef8:


    "Salté abajo del escenario y me fui caminando de la zona del concierto."


translate spanish day3_evening_dv_8bd551d4:


    dv "¡Vete pues!"


translate spanish day3_evening_dv_11b833b9:


    "Escuché como Alisa me gritaba."


translate spanish day3_evening_dv_9d84e4e8:


    me "¡Ya me estoy yendo!"


translate spanish day3_evening_dv_2eb0d079:


    "Susurré."


translate spanish day3_evening_dv_35bec68e:


    "El camino me llevó de vuelta hacia la plaza."


translate spanish day3_evening_dv_51f9be11:


    "Estaba Slavya en el centro de ésta, barriendo la zona."


translate spanish day3_evening_dv_d2abd3ca:


    sl "Oh, Semyon, ¡hola! Creía que todos estaban ya durmiendo."


translate spanish day3_evening_dv_4477db54:


    me "No... ¿Qué estás haciendo?"


translate spanish day3_evening_dv_753b1df5:


    sl "Limpiando tras el baile. ¿Por qué no viniste?"


translate spanish day3_evening_dv_79a8a3cb:


    me "Solamente..."


translate spanish day3_evening_dv_4bd32d93:


    sl "¿Y dónde has estado?"


translate spanish day3_evening_dv_bbc9f7af:


    me "Yo..."


translate spanish day3_evening_dv_66b39d2d:


    "No tenía ni la más mínima intención de contarle a Slavya cómo me sentí avergonzado en el escenario con la guitarra frente a Alisa, mientras todo el campamento se divertía en la plaza."


translate spanish day3_evening_dv_b498ffb4:


    me "Me di un paseo."


translate spanish day3_evening_dv_527c6bb3:


    sl "Vale... Pero deberías haber venido. Fue divertido."


translate spanish day3_evening_dv_77b6031e:


    me "Me alegra que lo disfrutaras."


translate spanish day3_evening_dv_a7c92b66:


    "Hoy he estado sudando como un cerdo por preocupaciones o tan sólo por la calurosa noche."


translate spanish day3_evening_dv_c79602cb:


    me "Ey, ¿sabes dónde puedo ir a tomar un baño?"


translate spanish day3_evening_dv_46023748:


    sl "Claro. Ve recto, luego gira un poco hacia la izquierda del camino, posteriormente hacia la derecha y allí encontrarás unos baños."


translate spanish day3_evening_dv_3820771a:


    me "..."


translate spanish day3_evening_dv_56fc315a:


    sl "¿Debería acompañarte?"


translate spanish day3_evening_dv_90d3bf06:


    me "No, no. ¡Lo encontraré por mi mismo!"


translate spanish day3_evening_dv_8a838751:


    "No quería de ninguna forma atosigarla más."


translate spanish day3_evening_dv_844a11d0:


    "Caminé por el camino del bosque en la dirección en que Slavya me enseñó."


translate spanish day3_evening_dv_7fd553fe:


    "Mi cuerpo entero me picaba."


translate spanish day3_evening_dv_1ac85161:


    th "¿Cómo no me di cuenta de ello antes?"


translate spanish day3_evening_dv_b2840d09:


    "Y aun más importante, ¿y qué si no me doy cuenta?"


translate spanish day3_evening_dv_19111076:


    th "Si las cosas siguen yendo así, entonces tendré que permanecer en este mundo para siempre."


translate spanish day3_evening_dv_cd1088c8:


    "Temblé sólo de pensarlo."


translate spanish day3_evening_dv_ea4df3bd:


    "Probablemente más por la idea de que no sentiera ningún temor o incluso tan solamente miedo."


translate spanish day3_evening_dv_d95e35f6:


    "Justo como si fuera el fluir natural de las cosas."


translate spanish day3_evening_dv_0ba45212:


    "Podía ver unos baños detrás de los árboles."


translate spanish day3_evening_dv_4c8601db:


    th "¿Y por qué los pioneros tienen los baños aquí?"


translate spanish day3_evening_dv_c7b24c86:


    th "Olga Dmitrievna me parece que dijo algo sobre unas reparaciones de las duchas."


translate spanish day3_evening_dv_7cf75d85:


    me "No hay razón para preocuparse ahora..."


translate spanish day3_evening_dv_c5ff1076:


    "Habían jabón y toallas limpias por lo que me duché rápidamente, salí afuera y respiré una gran bocanada de aire fresco nocturno."


translate spanish day3_evening_dv_9f7fc9a5:


    th "Ah, ¡es tan maravilloso cuando la higiene física conlleva limpieza espiritual!"


translate spanish day3_evening_dv_00142a80:


    "Todos mis problemas, si no se habían ido, al menos se volvieron menos importantes. Es como si el agua no solamente hubiera aseado mi sucio cuerpo, sino también las preocupaciones de mi mente."


translate spanish day3_evening_dv_9b68d7c1:


    "Repentinamente empecé a bostezar, así que me dirigí hacia la cabaña de la líder, frontándome los ojos."


translate spanish day3_evening_dv_8c18ef81:


    "No habia dado apenas unos pasos, cuando Alisa saltó de los arbustos."


translate spanish day3_evening_dv_e58bfb0c:


    dv "Tú... ¿Qué haces aquí?"


translate spanish day3_evening_dv_aa2954ff:


    me "Sólo me estaba duchando."


translate spanish day3_evening_dv_e2ccd354:


    "Volvió en sí y me habló de nuevo."


translate spanish day3_evening_dv_d87c29ac:


    dv "¿Y?"


translate spanish day3_evening_dv_ad02f53a_2:


    me "¿Y qué?"


translate spanish day3_evening_dv_410dff3b:


    dv "¡¿Cómo debería saberlo?!"


translate spanish day3_evening_dv_b7d92302:


    me "Pues entonces, ¿por qué me hablas si no tienes ni idea?"


translate spanish day3_evening_dv_0adca3fb:


    "Toda la paz interior se desvaneció en un instante, sólo la irritación y la fatiga permanecían."


translate spanish day3_evening_dv_e44bfa1d:


    me "Vale, bueno si no te importa..."


translate spanish day3_evening_dv_a5e7a3c6:


    dv "Espera..."


translate spanish day3_evening_dv_f3274282:


    me "¿Ahora qué?"


translate spanish day3_evening_dv_761956ea:


    dv "Si antes allí en el escenario pensaste que... bueno... No quise ofenderte ni nada parecido..."


translate spanish day3_evening_dv_1f988280:


    me "Está bien, no estoy ofendido. Simplemente no sé tocar la guitarra. Nada importante, mucha gente no sabe."


translate spanish day3_evening_dv_403f582b:


    "Sentía que había signos inequívocos de irritación en mi voz, pero no podía hacer nada con ello. Quería decírselo todo a la cara, justo ahora mismo."


translate spanish day3_evening_dv_5b7b1ded:


    dv "Exacto, ¡no puedes!"


translate spanish day3_evening_dv_aee5d595:


    "Alisa, obviamente, se estaba conteniendo la risa."


translate spanish day3_evening_dv_d5503c43:


    me "Oh, ¡basta ya! ¡Ya tuve suficiente de ti!"


translate spanish day3_evening_dv_a3a48b46:


    "Me sacó de mis casillas totalmente en ese instante."


translate spanish day3_evening_dv_423dbcda:


    "Otro segundo más y hubiera empezado a gritarle...{w} O peor."


translate spanish day3_evening_dv_f72c72fe:


    dv "Espera."


translate spanish day3_evening_dv_d177607c:


    me "¡Que me dejes tranquilo!"


translate spanish day3_evening_dv_cc1a995c:


    "Alisa me siguió pero no intentó detenerme."


translate spanish day3_evening_dv_0cc8c845:


    dv "De verdad que no quería ofenderte..."


translate spanish day3_evening_dv_2cebe654:


    "Algún poder invisible me hizo darme la vuelta."


translate spanish day3_evening_dv_d09cc761:


    me "Vale. ¿Puedo irme ahora?"


translate spanish day3_evening_dv_4907e592:


    dv "Sí... Por supuesto..."


translate spanish day3_evening_dv_664f2019:


    "A la luz lunar Alisa resultaba muy bella con una leve expresión de culpabilidad en su rostro."


translate spanish day3_evening_dv_aa3cefba:


    th "Si sólo mantuviera su boca cerrada..."


translate spanish day3_evening_dv_74c9cbb9:


    dv "¡¿Por qué estás sonriendo tan gratamente?!"


translate spanish day3_evening_dv_d857ef67:


    th "Aunque..."


translate spanish day3_evening_dv_356af705:


    me "Por nada. Sólo pensaba que si tan solamente fueras un poquito más educada..."


translate spanish day3_evening_dv_0fd482e1:


    dv "¿Entonces qué?"


translate spanish day3_evening_dv_389e70d3:


    "No debería decir estas cosas, la gente podría tener ideas equivocadas, dependiendo de su capacidad intelectual."


translate spanish day3_evening_dv_a0eb57ae:


    me "Pues nada."


translate spanish day3_evening_dv_6f263bd2:


    dv "¡Deberías acabar lo que empezaste!"


translate spanish day3_evening_dv_c27026d4:


    me "No hay nada que terminar. Estoy muy cansado y me voy a ir dormir."


translate spanish day3_evening_dv_d69aa454:


    dv "¡Detente ahí mismo!"


translate spanish day3_evening_dv_301d2f4b:


    me "¡No me hagas salir corriendo de ti!"


translate spanish day3_evening_dv_5e3d8340:


    dv "¡Como si pudieras!"


translate spanish day3_evening_dv_60769081:


    "Puede que tenga razón. Estoy exhausto ya de este día. Y, naturalmente, no tenía intención de salir corriendo tras un baño."


translate spanish day3_evening_dv_ba176b89:


    dv "¿Entonces qué?"


translate spanish day3_evening_dv_ad02f53a_3:


    me "¿Qué?"


translate spanish day3_evening_dv_a8a0c403:


    dv "Si solamente fuera más educada, ¿entonces qué?"


translate spanish day3_evening_dv_ef8f1d34:


    me "Inténtalo y lo verás."


translate spanish day3_evening_dv_279c0b72:


    dv "¿Por qué debería hacer algo por un idiota como tú...?"


translate spanish day3_evening_dv_fb217e62:


    me "Pues no lo intentes. ¿Qué tiene que ver eso conmigo?"


translate spanish day3_evening_dv_772db0f1:


    "Suspiré y me arrastré lentamente hasta la plaza."


translate spanish day3_evening_dv_227a4155:


    dv "¿Y qué hay de ti pues?"


translate spanish day3_evening_dv_ad02f53a_4:


    me "¿Qué?"


translate spanish day3_evening_dv_dd2be400:


    "Le contesté sin girarme siquiera hacia ella."


translate spanish day3_evening_dv_7bc2ed19:


    dv "Bueno, si lo fuera, ¿entonces tú...?"


translate spanish day3_evening_dv_a70104fb:


    me "No sé de qué estás hablando, pero imaginemos que «lo haría» si así es más fácil para ti."


translate spanish day3_evening_dv_9a1f6cf1:


    dv "¡Imbécil!"


translate spanish day3_evening_dv_779b2571:


    "Alisa me gritó y se dirigió hacia los baños."


translate spanish day3_evening_dv_76cd70be:


    "La plaza brillaba limpia tanto como una patena o como una carrtera alemana, las cuales eran limpiadas con champú, o eso decía la gente."


translate spanish day3_evening_dv_ac1742ca:


    th "¡Ésa es Slavya!"


translate spanish day3_evening_dv_881163f0:


    th "Y justo discutí con Alisa y nada más..."


translate spanish day3_evening_dv_2a8b99c4:


    th "¿Y para qué?"


translate spanish day3_evening_dv_7fd16daf:


    th "¡Como si fuera realmente necesario!"


translate spanish day3_evening_dv_ea92f05b:


    th "Y su comportamiento..."


translate spanish day3_evening_dv_6c061942:


    "En cualquier otra situación seguramente habría dicho que le gusto a Alisa y que tan solamente lo oculta tras su rudeza."


translate spanish day3_evening_dv_01f29ad6:


    th "¡Aunque ahora mismo eso no es importante!"


translate spanish day3_evening_dv_fa76e542:


    "Las relaciones, el amor, cualquier cosa..."


translate spanish day3_evening_dv_12677de3:


    th "¡Quizás ella no es ni humana!"


translate spanish day3_evening_dv_e9aaa3dd:


    th "¿Y qué si ella fuera una humana y todo esto estuviera ocurriendo en el mundo {i}real{/i}...?"


translate spanish day3_evening_dv_92236afc:


    "Alejé esos pensamientos de mi y me dirigí hacia la cabaña de la líder. El cansancio se apoderó de mi."


translate spanish day3_evening_dv_2579577e:


    "Era como un obstáculo invisible lo que me detuvo frente a la puerta."


translate spanish day3_evening_dv_71e467af:


    th "Olga Dmitrievna ciertamente querría saber dónde estuve y por qué me perdí la fiesta de baile."


translate spanish day3_evening_dv_aabe8f1c:


    th "Tendría un interrogatorio completo, seguramente."


translate spanish day3_evening_dv_bcc9746d:


    "Contando conque Alisa también se perdió el baile, no sería difícil suponer que nos la saltamos ambos juntos."


translate spanish day3_evening_dv_5df6ba98:


    th "Por otra parte, debería incluso contarle a ella que en vez de ir al baile, Alisa estuvo practicando con la guitarra y que... yo tan sólo me dejé llevar por la corriente..."


translate spanish day3_evening_dv_4739fcde:


    th "En realidad no es nada especial..."


translate spanish day3_evening_dv_8a5e9765:


    "En verdad no importa por qué me fui."


translate spanish day3_evening_dv_0b609861:


    th "Así que..."


translate spanish day3_evening_dv_abdca19a:


    "A la vez, creía que podría ser mejor quedarse callado."


translate spanish day3_evening_dv_bdf7e286:


    th "Bueno, decidiré en el momento."


translate spanish day3_evening_dv_ed6ae9d5:


    "Suspiré y giré el pomo de la puerta."


translate spanish day3_evening_dv_e96fefb3:


    "Olga Dmitrievna todavía estaba esperándome, llevando puesto un vestido."


translate spanish day3_evening_dv_47f7a7d5:


    mt "Bueno Semyon, ¿dónde has estado?"


translate spanish day3_evening_dv_48199bb7:


    me "Yo..."


translate spanish day3_evening_dv_ae28ee74:


    me "A decir verdad, estuve en el escenario con Alisa."


translate spanish day3_evening_dv_baaff23f:


    mt "¿Y qué hiciste allá?"


translate spanish day3_evening_dv_0c8efc3f:


    me "Tocar la guitarra..."


translate spanish day3_evening_dv_30a21108:


    me "Yo...{w} Sólo estaba caminando por los alrededores."


translate spanish day3_evening_dv_b166a941:


    mt "¿Solo?"


translate spanish day3_evening_dv_9513cd87:


    me "Sí..."


translate spanish day3_evening_dv_db050801:


    mt "Vale, eso no es importante. ¿Por qué no viniste al baile?"


translate spanish day3_evening_dv_52fe9abf:


    me "Bueno, para ser sinceros, no me gusta bailar..."


translate spanish day3_evening_dv_ff3e3b04:


    mt "¿Y qué?{w} ¡Eso era un evento público del campamento al que cualquier pionero debe ir!"


translate spanish day3_evening_dv_1b595140:


    "Me miraba con atención."


translate spanish day3_evening_dv_3607aec3:


    mt "Podrías simplemente sentarte en alguna parte tranquilamente."


translate spanish day3_evening_dv_2993aaa0:


    me "Lo siento..."


translate spanish day3_evening_dv_ed0914b5:


    "Suspiré."


translate spanish day3_evening_dv_dbc34155:


    mt "Vale."


translate spanish day3_evening_dv_d14fc77f:


    "La líder se relajó."


translate spanish day3_evening_dv_41704f8b:


    mt "¡Vete a la cama rápidamente!"


translate spanish day3_evening_dv_9c792872:


    th "La tormenta ya pasó."


translate spanish day3_evening_dv_ce617998_1:


    "..."


translate spanish day3_evening_dv_5649b7bb:


    "Me di vueltas una y otra vez, pero no podía dormirme."


translate spanish day3_evening_dv_7bb07423:


    "Mi mente estaba cargada de pensamientos sobre la música, las guitarras y Alisa..."


translate spanish day3_evening_dv_1a6a274a:


    th "Me preguntaba si podría llegar a ser un músico profesional..."


translate spanish day3_evening_dv_c5ba79b7:


    th "¿Y quería ser uno de ellos?"


translate spanish day3_evening_dv_24a063fd:


    th "¿Por qué debería yo {i}quererlo{/i}...? Ciertamente sería algo muy divertido, pero poco más."


translate spanish day3_evening_dv_bc447272:


    th "¿O es Alisa el motivo?"


translate spanish day3_evening_dv_66b20a8a:


    th "En verdad ella es como un quiróptero, ¡succionando toda tu energía!"


translate spanish day3_evening_dv_ce617998_2:


    "..."


translate spanish day3_evening_dv_b5247e67:


    "Mis ojos empezaron a cerrarse y caí dormido un instante después."


translate spanish day3_fail_6f4c676f:


    "Slavya fue de vuelta a la pista de baile."


translate spanish day3_fail_5b8f0841:


    "Me senté un rato más y luego, cautelosamente, me alejé de allí tras asegurarme que nadie se percataba de mi."


translate spanish day3_fail_a20cefa7:


    "..."


translate spanish day3_fail_ab41d603:


    "No quería ver a nadie más tras semejante éxito de fiesta de baile."


translate spanish day3_fail_57cdff00:


    "El lugar más tranquilo de por aquí es la estación de autobuses, la cual quizá nunca sería visitada por la línea 410 jamás."


translate spanish day3_fail_5c5d8e3f:


    "¡Pero!{w} Se me escapó un grito."


translate spanish day3_fail_32f73f0c:


    "Había un autobús justo frente a mi.{w} Uno como el del primer día."


translate spanish day3_fail_193dd370:


    "Estaba petrificado."


translate spanish day3_fail_6770889d:


    me "¿Cómo, cuándo, cómo, por qué...?"


translate spanish day3_fail_83e56da8:


    "En seguida recordé todas mis teorías sobre cómo llegué a este campamento."


translate spanish day3_fail_8d9231e3:


    "Fui golpeado por la realidad: durante estos pocos días ya me había acostumbrado tanto a la vida lugareña, que ya había comenzado a olvidar que lo que ocurre aquí no era en verdad normal y corriente."


translate spanish day3_fail_dada5103:


    "Sólo permanecí quieto ahí y observé el maldito Icarus. Luego me di una bofetada con mis manos en mis mejillas, para asegurarme de que no era una ilusión. El autobús aun estaba allí."


translate spanish day3_fail_28a25175:


    th "Si está aquí, ¡entonces significa que es la hora de volver a casa!"


translate spanish day3_fail_b7190439:


    me "¡Sayonara, pioneros!"


translate spanish day3_fail_8ff4f863:


    "Salí corriendo hacia la puerta..."


translate spanish day3_fail_a0123424:


    "Volví a recuperar mis sentidos en el suelo.{w} Mi nariz me dolía intensamente."


translate spanish day3_fail_9b2c898b:


    "Me puse de pie y traté de comprender qué sucedía."


translate spanish day3_fail_929396d1:


    th "Parece como si me hubiera chocado contra algo..."


translate spanish day3_fail_8d0f3707:


    "El autobús era más que real al tacto."


translate spanish day3_fail_b50ff2e2:


    "Traté de alcanzar con mi mano la puerta...{w} Pero había un tipo de muro invisible."


translate spanish day3_fail_ca7b4ca3:


    "Estaba abrumado por un cuasi instinto animal de miedo."


translate spanish day3_fail_2dda9f46:


    "Miedo a todo... al campamento, a sus inquilinos y a este autobús."


translate spanish day3_fail_6d8da21b:


    th "¿Cómo demonios he llegado hasta aquí?"


translate spanish day3_fail_835b1bec:


    th "¿Qué es este maldito Icarus al cual no puedo acceder?{w} ¡¿Por qué me está sucediendo esto a mi?!"


translate spanish day3_fail_908aa7d1:


    "Repentinamente un fuerte viento sopló haciéndome tambalear."


translate spanish day3_fail_dea6ec52:


    "Me giré a mi alrededor y vi un pequeño pedazo de papel bajo la rueda del autobús."


translate spanish day3_fail_2ef2aa5c:


    "Alguna cosa estaba escrita en él."


translate spanish day3_fail_3b835d43:


    "{i}Estás aquí por una razón.{/i}"


translate spanish day3_fail_b1b730db:


    "La mala escritura me resultó familiar."


translate spanish day3_fail_a9d82379:


    th "De verdad que la he visto en alguna otra parte..."


translate spanish day3_fail_28815dfd:


    "¡Amanecía sobre mi!{w} Agarré un pequeño pedazo de carbón vegetal del suelo y garabateé el mismo enunciado en la parte de atrás."


translate spanish day3_fail_aa5a386f:


    "¡La escritura era idéntica!"


translate spanish day3_fail_078ccf4d:


    "Mi mente se aclaró."


translate spanish day3_fail_e5eddf45:


    th "¡Me he enviado un mensaje desde el futuro! ¡Eso es!"


translate spanish day3_fail_b57a15b1:


    th "O no.{w} Del pasado..."


translate spanish day3_fail_38859e59:


    th "¡Maldita sea! ¡No entiendo nada!"


translate spanish day3_fail_f958927a:


    "En cualquier caso, ésa era mi escritura."


translate spanish day3_fail_6844d216:


    "Ciertamente no era difícil de falsificarla, pero estaba seguro de que había escrito aquel mensaje yo mismo."


translate spanish day3_fail_04f28a5d:


    "Tras doblar aquel pedazo de papel entre mis manos decidí intentar acceder al autobús una vez más."


translate spanish day3_fail_e5a11986:


    "El muro invisible aun estaba ahí."


translate spanish day3_fail_54febcf3:


    "Di un rodeo alrededor del Icarus, di unas puntadas en las ruedas, eché un vistazo por dentro a través de las ventanas."


translate spanish day3_fail_8425e955:


    "Todo parecía ser absolutamente normal.{w} Pero en realidad no lo era."


translate spanish day3_fail_5386dfcb:


    "Lancé unas piedras grandes a los vidrios pero simplemente rebotaron sin hacer aparente daño."


translate spanish day3_fail_53e2e58a:


    "No tuvo efecto. ¡Ni tan siquiera un rasguño!"


translate spanish day3_fail_78354201:


    "Me senté en la acera y suspiré con impotencia."


translate spanish day3_fail_35da3684:


    "Si lo piensas, ese pedazo de papel es una pista de algo..."


translate spanish day3_fail_751d8d30:


    "Y parece que en esta situación no se me puede herir."


translate spanish day3_fail_105fc865:


    mt "¡Semyon!"


translate spanish day3_fail_07e68b3f:


    th "Parece que la líder me está buscando."


translate spanish day3_fail_06c92db3:


    th "Interesante, ¿qué dirá respecto al autobús ahora?{w} ¿Continuará insistiendo en que no llegará durante los próximos días?"


translate spanish day3_fail_c3a1e6ea:


    "Me puse en pie y salí corriendo en dirección a la voz de Olga Dmitrievna."


translate spanish day3_fail_d4b7729b:


    me "¡¿Qué me dices de eso?!"


translate spanish day3_fail_dc7df731:


    "Le espeté triunfantemente mientras agitaba mi mano indicando hacia la carretera."


translate spanish day3_fail_9de9961c:


    mt "¿Decir sobre qué?"


translate spanish day3_fail_afc5a641:


    "Respondió sorprendida."


translate spanish day3_fail_47eaf14e:


    "Me giré."


translate spanish day3_fail_26562526:


    "El autobús había desaparecido...{w} tan repentinamente como había aparecido."


translate spanish day3_fail_f0ef1916:


    "Mi grito de triunfo se ahogó en mi garganta."


translate spanish day3_fail_0c4809e3:


    mt "Ya es la hora de irse a dormir. ¡Vayámonos!"


translate spanish day3_fail_639b476b:


    me "Pero... pero..."


translate spanish day3_fail_6334b91e:


    mt "¿Qué?"


translate spanish day3_fail_be80cde7:


    me "¡El autobús! ¡Había un autobús! ¡Hace un momento atrás!"


translate spanish day3_fail_ef1ece89:


    mt "El autobús no podría estar aquí."


translate spanish day3_fail_1ee77ef5:


    "Dijo tranquilamente."


translate spanish day3_fail_ed2597b4:


    "Me quedé mirando fijamente la cara de Olga Dmitrievna. O ella sabía mentir muy bien o realmente no vio nada."


translate spanish day3_fail_65b9f63e:


    "Entonces, ¿soy yo acaso quien puede estar viendo cosas?"


translate spanish day3_fail_41e67f91:


    th "No puede ser verdad. ¡Vi realmente ese maldito 410!"


translate spanish day3_fail_09631ec6:


    me "Deja de mentir."


translate spanish day3_fail_5788b9db:


    "Dije tranquilamente."


translate spanish day3_fail_5afe237b:


    mt "Semyon, no te entiendo."


translate spanish day3_fail_c27faba3:


    me "¡Que me dejes de mentir! ¡Había un autobús! ¡Eres tú, eres tú quien me retiene aquí! ¡¿Por qué?!"


translate spanish day3_fail_9e2a7291:


    "Apreté mis dientes, pero traté de hablar con calmada voz."


translate spanish day3_fail_a34667cc:


    mt "Me estás asustando... ¡Es la hora de ir a la cama!"


translate spanish day3_fail_f4cdc187:


    th "No conseguiré nada de ella, como de costumbre."


translate spanish day3_fail_1665b82c:


    "Aunque en verdad, realmente quería ir a dormir."


translate spanish day3_fail_c0511a05:


    "Caminé ligeramente sobrepasando a la líder, sin prestarle atención deliberadamente."


translate spanish day3_fail_01dfa1f8:


    "No me podía dormir durante un buen rato, y sólo el arrugado pedazo de papel con las palabras «Estás aquí por una razón» me recordaron que los hechos de estos tres últimos días eran reales."
# Decompiled by unrpyc: https://github.com/CensoredUsername/unrpyc
