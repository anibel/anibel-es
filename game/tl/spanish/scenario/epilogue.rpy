
translate spanish epilogue_main_7a98964b:


    "Un dolor agudo latía atravesando todo mi cuerpo."


translate spanish epilogue_main_7c0ecf1c:


    "Lo sentía en especial más pronunciadamente en mis sienes."


translate spanish epilogue_main_28766e67:


    "Se sentía como si me fuera a destrozar en miles de pedazos pequeños en cualquier momento, dándole libertad al viento para que accediera a mi vacía cabeza."


translate spanish epilogue_main_1f5640c9:


    "Probablemente no habría sido capaz de soportar ni cinco minutos de dicha tortura si no hubiera abierto mis ojos a tiempo."


translate spanish epilogue_main_4d647778:


    "Estaba en {i}alguna parte{/i}, era imposible de decir con exactitud dónde."


translate spanish epilogue_main_81be611c:


    "Mi mente estaba nublada en nieblas, mis pensamientos estaban confundidos."


translate spanish epilogue_main_0c4d877e:


    "El instante entre los estados de inconsciencia y consciencia, cuando recuerdas vívidamente tus sueños."


translate spanish epilogue_main_e050dc5d:


    "Estaba yendo a alguna parte en autobús y me estaba a punto de dormir, cuando repentinamente una muchacha se me acercó y empezó a hablar rápidamente."


translate spanish epilogue_main_5d58d05f:


    "No podía entender ni una palabra, pero la muchacha me miraba muy enfadada."


translate spanish epilogue_main_6bdcd82e:


    "Fracasé en comprender qué necesitaba de mí."


translate spanish epilogue_main_21564d96:


    "El tiempo transcurrió, y ella permaneció hablando y hablando."


translate spanish epilogue_main_bb180213:


    "Me estaba empezando de verdad a enojar."


translate spanish epilogue_main_23d9757b:


    "Quería decirle que parara o por lo menos que se calmara un poquito, pero fracasé... ya fuera porque no logré decirle nada o porque mis palabras no le llegaron."


translate spanish epilogue_main_14eb51b4:


    "No podía ver tampoco su rostro. Simplemente estaba escuchando y mirando fijamente, mirando y escuchando..."


translate spanish epilogue_main_685c283d:


    "Quizá no fuera importante para mi en ese momento."


translate spanish epilogue_main_f176077d:


    "Estoy seguro de que no te acordarías de cómo se veía el mosquito que te inturrumpió el sueño."


translate spanish epilogue_main_79c81bc5:


    "Estoy seguro de que no lograrías acordarte de la frecuencia del aleteo de sus alas o de la inclinación de su probóscide."


translate spanish epilogue_main_065ff797:


    "Y esta muchacha es solamente una entre un millón de voces que te perturban para concentrarte, para pensar, para dormirte..."


translate spanish epilogue_main_66488078:


    "Cuanto más elevaba su voz, más difícil me resultaba captar sus palabras."


translate spanish epilogue_main_829ffeb0:


    "La cabina del autobús, los asientos desgastados por el tiempo, el irregular suelo, el techo oxidado, el vidrio resquebrajado del parabrisas... todo se iba a la deriva.{w} Junto con ella."


translate spanish epilogue_main_1281b344:


    "Y luego sentí una sensación única de alivio."


translate spanish epilogue_main_de7746b8:


    "No importaba cuán real habían sido el autobús y la muchacha... no eran nada más que un estorbo para mí."


translate spanish epilogue_main_6f529678:


    "Y aquí estoy, atrapado en un vacío total y precipitándome al interior de un sueño..."


translate spanish epilogue_main_ed7f45f7:


    "Guau, los sueños pueden ser realmente algo...{w} No como una pesadilla, pero claramente tampoco algo agradable."


translate spanish epilogue_main_4ad85600:


    "Me incorporé, me pellizqué la cara para volver a la realidad, hice un bostezo ruidoso y me preparé para asearme."


translate spanish epilogue_main_2ebebacc:


    "Era una mañana bastante gris."


translate spanish epilogue_main_6695bb75:


    "Incapaz de encontrar mi estuche de aseo en la mesita de noche, llegué a la conclusión de que podría apañármelas sin él."


translate spanish epilogue_main_66d354ed:


    "Me fue difícil vestirme... mis manos tamblaban demasiado."


translate spanish epilogue_main_571f5d50:


    "Miré mi reflejo en el espejo y comprobé mi barba incipiente de dos semanas."


translate spanish epilogue_main_c4b53f19:


    "Bueno, tal vez Olga Dmitrievna tenga una cuchilla..."


translate spanish epilogue_main_602cb68c:


    "Fue solamente entonces, cuando salí afuera que, repentinamente, ¡comprendí que no estaba en un campamento de pioneros!"


translate spanish epilogue_main_f8f2d3b4:


    "¡He vuelto a mi piso!"


translate spanish epilogue_main_7132ec6d:


    "Y he salido de mi habitación al pasillo, ¡no afuera de una cabaña!"


translate spanish epilogue_main_d7d976c7:


    "Estaba abrumado por la sorpresa, por la constenarción, por el miedo, incluso por el terror."


translate spanish epilogue_main_f8f1fe90:


    "¡¿Pero cómo?!"


translate spanish epilogue_main_4b63500a:


    "Me senté en la cama y me tapé la cara con mis manos, tratando de recordar el hilo de acontecimientos de ayer."


translate spanish epilogue_main_c0692396:


    "Sí, el autobús, el última día de la temporada."


translate spanish epilogue_main_28a24bd1:


    "Claro, me dormí...{w} y me desperté de vuelta en «casa»."


translate spanish epilogue_main_e6a0051c:


    "Bueno, hasta cierto punto, puede incluso parecer bastante lógico."


translate spanish epilogue_main_9b3b626a:


    "Parecía que el asombro inicial se diluía durante los primeros segundos."


translate spanish epilogue_main_2b7cb0bd:


    "Después de todo, para empezar, el hecho de que vuelva así tras una semana ausente, no es más extraño que mi súbita aparición en algún campamento de pioneros de la década de los ochenta."


translate spanish epilogue_main_94b8b09a:


    "Los sucesos de los últimos dos días brillaron ante mí en un instante."


translate spanish epilogue_main_4cdec556:


    "Aquel misterioso pionero, sus palabras..."


translate spanish epilogue_main_16f67736:


    "¿Pero cómo me las he apañado para volver a la realidad entonces?"


translate spanish epilogue_main_302cfd4b:


    "De acuerdo con él, iba a estar atrapado {i}allí{/i} para siempre, ¡en tanto no había salida!"


translate spanish epilogue_main_da19ba98:


    "Por otra parte, no estaba solo."


translate spanish epilogue_main_c8c85310:


    "Aquel pionero en la parada de autobús y la misteriosa voz enfatizaron lo contrario... que la salida existía."


translate spanish epilogue_main_d51d1680:


    "¿Significa eso que la hallé, que logré escapar del bucle infinito?{w} ¿Pero cómo?"


translate spanish epilogue_main_2d5435f1:


    "Sin embargo, no estaba seguro de si debería regocijarme o afligirme... durante toda la última semana me había acostumbrado en cierta forma a los monólogos interiores sempiternos, la búsqueda de respuestas y el análisis exhaustivo de todo, así que sencillamente no podía aceptar este hecho tal como era, sin descubrir qué me había ocurrido exactamente."


translate spanish epilogue_main_797ee454:


    "Claro que él me dijo que era el único que había observado la existencia de los otros tipos como yo justo durante su primer bucle, ¿pero significa eso alguna cosa?"


translate spanish epilogue_main_6d0aec47:


    "De todas formas, ¡todas sus teorías e ideas se derrumbaron al instante como un castillo de naipes!"


translate spanish epilogue_main_117f74f8:


    "Ahora tenía que escoger cómo debería responder."


translate spanish epilogue_main_f5b5600e:


    "Desde luego, ¡debería regocijarme!{w} A fin de cuentas, volví al mundo de verdad."


translate spanish epilogue_main_9d077188:


    "Tal vez los últimos siete días fueron solamente un sueño."


translate spanish epilogue_main_ef3f3b75:


    "Ciertamente, no había ninguna prueba de que realmente estuviera {i}allí{/i}."


translate spanish epilogue_main_6bf2c0b3:


    "No veo por ninguna parte mi uniforme de pionero, mi edad es justo como era antes.{w} Mi teléfono está en la mesa, completamente recargado."


translate spanish epilogue_main_9854060f:


    "Pero uno puede engañar a su propia memoria.{w} Uno puede vivir semejantes experiencias tremendamente reales en un sueño."


translate spanish epilogue_main_e2ba07bf:


    "Todavía recuerdo los acontecimientos de toda la semana con gran lujo de detalles."


translate spanish epilogue_main_b42bf578:


    "¿Quizás estuve en coma todo este tiempo?"


translate spanish epilogue_main_7db54ad1:


    "Una risa irónica se me escapó por la boca."


translate spanish epilogue_main_37cb3d33:


    "Qué va, eso tampoco es una posibilidad."


translate spanish epilogue_main_59c846f5:


    "En fin... ¿bien está lo que bien acaba?"


translate spanish epilogue_main_fdf1c653:


    "Las últimas horas en el campamento me vinieron a la cabeza."


translate spanish epilogue_main_376c1a1c:


    "Ciertamente, no me esperaba escapar... fuera de aquel campamento o de aquella realidad... y estaba más que preparado para otra semana.{w} Y otra y otra más..."


translate spanish epilogue_main_bff86508:


    "Había aceptado mi papel, me había reconciliado con mi destino."


translate spanish epilogue_main_4a86a5af:


    "¿Y qué se supone que iba hacer después de todo lo que ocurrió?"


translate spanish epilogue_main_c6b1ea80:


    "Expiré un suspiro de resignación, me levanté de mi cama con esfuerzo considerable y me fui hacia la computadora."


translate spanish epilogue_main_0dba5f6f:


    "Es raro, pero de acuerdo con ella, únicamente habían trasncurrido 14 horas desde mi desaparición de este mundo, no una semana entera."


translate spanish epilogue_main_295da44b:



    nvl clear
    "Un nuevo mensaje en el programa de mensajería instantánea..."


translate spanish epilogue_main_8ffdf5b6:


    "{i}¡Hola Semyon! Ayer... ¡Fue legendario! ¡Nos vemos! :))){/i}"


translate spanish epilogue_main_da755414:


    "Mi amigo de la universidad. Fue él quien me invitó a aquella fiesta con el resto de sus amigos universitarios a la que iba a ir."


translate spanish epilogue_main_6c9bac4f:


    "Repentinamente, recuperé todos los sentidos: el dolor de cabeza, el mareo, todos los síntomas de una resaca."


translate spanish epilogue_main_393d03d9:


    "Es como si efectivamente fuera a la fiesta de ayer. Y estuve con mucha jarana... así que, ¿no había ninún campamento de pioneros? Pero entonces, ¿de dónde surgieron todos estos recuerdos, emociones y sentimientos?"


translate spanish epilogue_main_dc6e0b16:



    nvl clear
    "Todo esto era tan incomprensible que me enfurecí. Empecé a maldecir vilmente, tratando de rasgarme el cabello y golpeando el teclado con mis puños. No retorné a la normalidad hasta que no quedaron teclas en él."


translate spanish epilogue_main_f36bf9ba:


    "¿Por qué me debería preocupar tanto? Nadie se preocupa cuando un sueño o una alucinación se acaba. Además, la gente habitualmente se alegra de que ocurra. ¿Estaba tan preparado para permanecer allí que mi retorno a la realidad se volvió para mi tan indeseable?"


translate spanish epilogue_main_5f5b219a:



    nvl clear
    "Y luego una idea sencilla y obvia pasó por mi mente. Quizás me esté volviendo demente..."


translate spanish epilogue_main_2500664b:


    "Ciertamente, los enfermos mentales frecuentemente tienen visiones las cuales perciben como realidad. Además, tenía todos los síntomas de la demencia: nunca abandoné los cuatro muros de mi prisión, nunca socializo, tengo múltiples problemas psicológicos. Bueno, ¡eso explica muchas cosas!"


translate spanish epilogue_main_18e12dde:



    nvl clear
    "Mi oscura y nerviosa carcajada hizo eco en la habitación."


translate spanish epilogue_main_92e8ada7:


    "Supongo que es la hora adecuada para que visite a un terapeuta. O mejor aun un psiquiatra. ¿O debería sólo encerrarme en un psiquiátrico?"


translate spanish epilogue_main_636725da:


    me "¡Un caballo! ¡Un caballo! ¡Mi reino por un caballo!"


translate spanish epilogue_main_5f0e3c02:


    "Vociferé a pleno pulmón entre mis accesos de carcajada de maníaco."


translate spanish epilogue_main_ecb4a2ab:


    me "¡No te discutas con ella! ¡Ella es una lunática!"


translate spanish epilogue_main_56059efa:


    "El ruído que mis vecinos hicieron en la pared me calmó de alguna manera."


translate spanish epilogue_main_5eb2202b:



    nvl clear
    "Estoy tan hecho trizas, que lo único que podría ayudarme es una lobotomía."


translate spanish epilogue_main_ce617998:


    "..."


translate spanish epilogue_main_258159ac:


    "Me senté tras perder los papeles completamente durante varias ahoras. Sin hacer nada, sin pensar en nada, solamente mirar fijamente la pantalla, la cual se había puesto en modo de espera hacía rato. Finalmente moví el ratón y abrí varias pestañas en el navegador. Y bien, ¿qué hay de nuevo? ¡F5, F5!"


translate spanish epilogue_main_81eac3b9:


    "Como si los acontecimientos de la semana pasada no hubieran ocurrido nunca. Venga ya, ¿acaso tienes que afligirte por una alucinación? ¿Fue real? Claro. ¿Muchos recuerdos? Claro. ¿Parecen muy verdaderos? ¡Pues claro que sí! ¿Alguna prueba? Nop."


translate spanish epilogue_main_6e5d6669:



    nvl clear
    "El veredicto está claro, así como mi futuro rumbo. He sospechado durante largo tiempo que mi actual modo de vida no me estaba haciendo ningún bien. No estaba preparado para semejante extraordinario acontecimiento, ¿pero y qué? Tendré que estar más preparado para mi próximo viaje."


translate spanish epilogue_main_ce617998_1:


    "..."


translate spanish epilogue_main_76b2fe88:


    nvl clear


translate spanish epilogue_main_8e39de8c:


    "La noche. Un arroyo sin fin de ideas e imágenes se apoderaron de mi mente distrayéndome de mis lamentaciones y reflexiones. Un mensaje instantáneo apareció muy inesperadamente."


translate spanish epilogue_main_b336ac6e:


    message "Hola, ¿cómo estás?"


translate spanish epilogue_main_e3463832:


    "Claro, una forma muy original de empezar una conversación."


translate spanish epilogue_main_e7ce88cd:


    me "Como siempre."


translate spanish epilogue_main_931ab60c:


    "La lista de perfiles de contactos estaba completamente vacía. No había ni un apodo... solamente una cifra de nueve dígitos. No puedes imaginarte qué tipo de persona hay en el otro extremo con tan solamente mirar un número, ¿no? Está claro que algún día será totalmente diferente. Identificadores digitales unificados en vez de nombres, un par de símbolos como biografía, diversos bytes de personalidad y tres y medio bits para los sentimientos..."


translate spanish epilogue_main_dd99d86f:



    nvl clear
    message "¿Volviste sano y salvo a casa?"


translate spanish epilogue_main_387fcb02:


    "Probablemente alguien de la fiesta de ayer."


translate spanish epilogue_main_830dea84:


    me "Eso parece."


translate spanish epilogue_main_707b84c6:


    message "Recuerdas todo lo que te conté ayer, ¿verdad?"


translate spanish epilogue_main_f5ebe454:


    me "Bueno... Creo que no."


translate spanish epilogue_main_62a3a2c6:


    "Aun hubo otro mensaje tras una larga pausa."


translate spanish epilogue_main_ca8f6404:



    nvl clear
    message "Ya veo."


translate spanish epilogue_main_998b1726:


    "Sinceramente, no tengo ni remota idea de quién es, ni de por qué me escribe. Mientras este individuo sin identificar no me comenzara a acosar, podría tolerar fácilmente su presencia en mi lista de contactos."


translate spanish epilogue_main_33de6e67:


    message "Bueno, no pasa nada. Nos encontraremos de nuevo. Seguro que sí."


translate spanish epilogue_main_f5239078:


    "Claro, lo esperaré con impaciencia, sep..."


translate spanish epilogue_main_c92f92b9:



    nvl clear
    me "Muy bien."


translate spanish epilogue_main_e24c9d81:


    message "¡Adiós!"


translate spanish epilogue_main_0e18a85a:


    "No contesté."


translate spanish epilogue_main_f8d54840:


    "Este raro y fastidioso día por fin estaba llegando lentamente a su final. Al irme a la cama, volví otra vez a recordar en mi cabeza todos los hechos que parecieron ser tan reales para mí. ¿Dónde me voy a levantar mañana? ¿Retornarían esas alucinaciones? Qué va, lo más probable es que sencillamente lo olvide todo."


translate spanish epilogue_main_95611f9f:



    nvl clear
    "Qué raro, pero ahora realmente siento que todo está sucediendo de verdad. {i}Allí{/i} todo era sólo ficción, un juego de una imaginación deforme, o algo verdaderamente fantástico, y {i}aquí{/i} es realidad. A veces inadecuada, otras veces se desarrolla de forma diferente a como deseabas, sorpresas como ésta, ¡pero aun así es la realidad!"


translate spanish epilogue_main_fb80d3eb:


    "Me dormí con estos pensamientos."


translate spanish epilogue_main_ce617998_2:


    "..."


translate spanish epilogue_main_76b2fe88_1:


    nvl clear


translate spanish epilogue_main_c07a412b:


    "Poco a poco, mi vida vuelve a su curso natural, y comienzo a olvidar los hechos de aquella semana. Después de todo, nada malo sucedió. Incluso si fuera claramente real, ¿a quién le importa? Aquello fue solamente siete dias en un campamento de pioneros. No me mataron, nadie me secuestró para practicarme experimentos, nadie me lavó el cerebro así que, ¿por qué me tengo que preocupar ahora por la causa de aquello?"


translate spanish epilogue_main_d7bc5196:



    nvl clear
    "Y si ciertamente fue una alucinación, solamente puedo esperar a que no se vuelva a repetir."


translate spanish epilogue_main_76b2fe88_2:


    nvl clear


translate spanish epilogue_main_1c95a7cd:


    "Aun seguía encerrado. Mi computadora e internet eran mis únicos amigos, y el teclado era mi único canal para comunicarme con el mundo exterior. La tecla F5 se volvió de nuevo la única cosa importante en mi vida. Pensándolo bien, no era tampoco tan malo."


translate spanish epilogue_main_76b2fe88_3:


    nvl clear


translate spanish epilogue_main_88474bfc:


    "Pero un día una combinación de dígitos familiar apareció en la pantalla... aquel contacto extraño mío había vuelto a estar en línea una vez más."


translate spanish epilogue_main_68c6e51f:


    message "¡Ey!"


translate spanish epilogue_main_a87c71d7:


    me "Ey."


translate spanish epilogue_main_15fd667b:


    message "¿Qué tal estás?"


translate spanish epilogue_main_e7ce88cd_1:


    me "Bien."


translate spanish epilogue_main_c56a1499:


    "Probablemente le conociera de algún lado. Pero a quién le importa, de todas formas..."


translate spanish epilogue_main_fcdab43c:



    nvl clear
    message "¿Alguna novedad?"


translate spanish epilogue_main_dd5093d8:


    me "Pues no."


translate spanish epilogue_main_d166e979:


    message "¿Es la estabilidad una auténtica señal de clase?"


translate spanish epilogue_main_dd69c3a1:


    me "Puede decirse que sí."


translate spanish epilogue_main_e43109d5:


    message "Oh, ya veo."


translate spanish epilogue_main_e99ffbdc:


    "Para mí esta conversación había terminado, pero él no se sentía igual."


translate spanish epilogue_main_efe11ea9:



    nvl clear
    message "Entonces, ¿nada ha cambiado?"


translate spanish epilogue_main_8dd0767d:


    me "Nop. ¿Debería?"


translate spanish epilogue_main_6fa5b1d2:


    message "Bueno, ¡no es que vayas a experimentar cosas así en el día a día! No todo el mundo lo experimenta..."


translate spanish epilogue_main_1084810b:


    me "¿De qué estás hablando?"


translate spanish epilogue_main_3c014c6c:


    message "¿No lo entendiste?"


translate spanish epilogue_main_67b3ef9f:


    "Empezaba a entenderlo."


translate spanish epilogue_main_a160c0c8:



    nvl clear
    me "Preferiría que tú lo explicaras."


translate spanish epilogue_main_ba495f57:


    message "¿No recuerdas el campamento?"


translate spanish epilogue_main_907853ad:


    "¡Sentí como una descarga eléctrica! Todos los sucesos de aquella semana brotaron en mi mente. Mis manos comenzaron a temblar, un frío sudor ardía en mi frente. Me di la vuelta horrorizado, imaginándome ya que veía alienígenas, fantasmas o la Parca. Pero no había nadie en la habitación salvo yo mismo, como siempre. Corrí hacia el pasillo, comprobé la cocina y los baños... seguía sin haber nadie. Al final, retorné a la computadora. La misma ventana de mensajes todavía parpadeaba, como si fuera perfectamente un mensaje normal y corriente."


translate spanish epilogue_main_56c768f9:



    nvl clear
    me "¿Quién eres?"


translate spanish epilogue_main_b69f1bc8:


    "Mis manos temblaban tanto que apenas podía apretar las teclas correctas."


translate spanish epilogue_main_56d58b67:


    message "Oh, ¿entonces lo recuerdas?"


translate spanish epilogue_main_30227704:


    "¿Mis alucinaciones seguían continuando? ¿O en realidad nunca abandoné el campamento? ¿O después de todo solamente es un sueño? O..."


translate spanish epilogue_main_54a73f72:


    me "No sabía qué decir."


translate spanish epilogue_main_3a4edaaa:


    "Confesé sinceramente. De cualquier manera, sentí que esta entidad (la única definición que se me ocurría) no necesitaba ningún tipo de conexión a internet para comunicarse conmigo."


translate spanish epilogue_main_bbccea89:



    nvl clear
    message "Bueno, puedes estar simplemente callado, todo está perfectamente claro xD"


translate spanish epilogue_main_a073eb1b:


    "Esa sonrisa al final de la línea se parecía al mismísimo diablo, abriendo sus deformes mandíbulas para devorarme."


translate spanish epilogue_main_e1d6b6aa:


    me "¿Qué me está ocurriendo?"


translate spanish epilogue_main_d850b239:


    "Finalmente me decidí a hacerle esa pregunta."


translate spanish epilogue_main_5e3cf096:


    message "Nada. Nada en absoluto. Solamente estás viviendo tu vida habitual."


translate spanish epilogue_main_39faba7a:


    me "Pero el campamento... ¿Fue real?"


translate spanish epilogue_main_93656c09:


    message "¿Tú qué crees?"


translate spanish epilogue_main_4515fb38:


    me "No estoy seguro."


translate spanish epilogue_main_83608308:


    message "Si a ti te pareció real, entonces sí, fue real. Si no, entonces no lo fue. Es así de simple."


translate spanish epilogue_main_d1986394:



    nvl clear
    "¡No es tan sencillo!"


translate spanish epilogue_main_a87d627d:


    me "¿Qué me ocurrirá?"


translate spanish epilogue_main_9db6b90d:


    "Susurré."


translate spanish epilogue_main_ab2d839e:


    message "Nada de lo que tengas que preocuparte."


translate spanish epilogue_main_7b4418d5:


    "Parecía que realmente pudiera escucharme sin que usara internet. El terror se apoderó de mi cuerpo, cerré mis ojos y me acurruqué haciendo un ovillo."


translate spanish epilogue_main_dfa8be82:


    me "¿Por qué? ¿Por qué me estás haciendo esto a mí?"


translate spanish epilogue_main_4d3bcab6:


    "Comencé a delirar. Las palabras surgían por sí mismas de mi boca. Montones de palabras irrazonables y sin sentido."


translate spanish epilogue_main_9ee3c724:



    nvl clear
    me "¿Por qué? ¿Por qué yo? ¿Para qué? ¿Para qué? ¡Déjame solo! ¡No hice nada malo!"


translate spanish epilogue_main_ae870164:


    "Tras diversos minutos, hallé el coraje para abrir un ojo y echar un vistazo en la pantalla."


translate spanish epilogue_main_148f12c7:


    message "Tú eres la causa de todo."


translate spanish epilogue_main_3f769b22:


    "La ventana del mensaje estaba parpadeando fríamente."


translate spanish epilogue_main_5f3254a2:


    me "¿Y ahora qué?"


translate spanish epilogue_main_a1f21fcf:


    message "Oh, ¡eso lo verás muy pronto! :)"


translate spanish epilogue_main_76b2fe88_4:


    nvl clear


translate spanish epilogue_main_74902b45:


    "Todo comenzó a tornarse negro en mis ojos, el ruido ensordeció mis oídos y me empecé a sentir mareado. Sentí como mi propia alma se escapaba de su prisión física. Los sucesos de los últimos días se volvieron borrosos, como si le hubieran ocurrido a otra persona en vez de a mí. Luego las memorias del campamento empezaron a desvanecerse, como si alguien usara una goma de borrar para efectivamente borrar las palabras escritas con un lápiz. Un instante después, no sentí nada, como si estuviera sumido en un estado cósmico de dicha."


translate spanish epilogue_main_f7590cd3:


    "Si alguien me hubiera visto en ese momento, se habría dado cuenta de que estaba sonriendo..."


translate spanish epilogue_main_a20cefa7:


    "..."


translate spanish epilogue_main_529298f6:


    me "Ehmm... Estoy algo atascado aquí."


translate spanish epilogue_main_6f3ba371:


    "Apreté el botón de reportar spam, cerré la ventana, minimicé el navegador y eché una mirada al reloj."


translate spanish epilogue_main_612c456e:


    "Hora de irse o voy a llegar tarde..."


translate spanish epilogue_main_a20cefa7_1:


    "..."


translate spanish epilogue_un_bad_0da63856:


    "Hay momentos en que la realidad se vuelve poco importante, insignificante, indigna de atención."


translate spanish epilogue_un_bad_6693cd84:


    "Hay momentos en que tu tormento espiritual ensombrece cualquier otra cosa."


translate spanish epilogue_un_bad_092e6505:


    "E incluso si el mundo se terminara... tú ni lo notarías.{w} Si un cuchillo fuera a apuñalarte... tú no lo notarías.{w} Incluso si hirvieras eternamente en las calderas del infierno, parecería un simple inconveniente menor."


translate spanish epilogue_un_bad_92427898:


    "Después de todo, hay problemas más importantes que esos..."


translate spanish epilogue_un_bad_ab4e3f76:


    "Cuando abrí mis ojos, me di cuenta de que algo iba mal."


translate spanish epilogue_un_bad_089c801c:


    "Me llevó un rato despejar mis pensamientos."


translate spanish epilogue_un_bad_2638c66a:


    "Finalmente me percaté de que no estaba en un autobús, un autobús...{w} en mi viejo piso."


translate spanish epilogue_un_bad_7c7bf187:


    "Bueno, eso era de esperar."


translate spanish epilogue_un_bad_724c6d60:


    "Como si me hubiera pasado una semana entera preparándome para un examen y, en el último minuto, hubiera fracasado estrepitosamente."


translate spanish epilogue_un_bad_c9575f91:


    "Y el resultado de este fracaso era mi vuelta al mundo real..."


translate spanish epilogue_un_bad_95da6ae5:


    "Sin embargo, ahora no me parecía mucho más real que el campamento de pioneros Sovyonok."


translate spanish epilogue_un_bad_f7d76256:


    "No es de extrañar. La realidad es lo que puedes escuchar, sentir, tocar y saborear.{w} Y todo eso estaba realmente allí."


translate spanish epilogue_un_bad_85abf9cd:


    "Aquel mundo era real hasta el más mínimo detalle. A veces era como si fuera mi propia vida anterior la que fuera una ficción."


translate spanish epilogue_un_bad_f527260f:


    "Y ahora, tengo que recordar cómo existir aquí..."


translate spanish epilogue_un_bad_459b2ee7:


    "¿Aunque por qué?{w} Me siento como un hombre que haya sido tirado fuera de un coche a toda velocidad, sin darse cuenta siquiera."


translate spanish epilogue_un_bad_fd341608:


    "Y fue abandonado, tirado en la carretera con los brazos y las piernas rotas mientras el vehículo desaparecía en la noche, llevándose consigo los últimos rastros de esperanza."


translate spanish epilogue_un_bad_b99b2263:


    "Lena..."


translate spanish epilogue_un_bad_6f0311f7:


    "Su imagen afloró en mi febril cerebro con tanta claridad, que quise llorar sin soportarlo."


translate spanish epilogue_un_bad_ae8f3acd:


    "No... quise gritar, arrancarme el cabello, golpear con mis puños en la pared mientras hiciera gritos inhumanos."


translate spanish epilogue_un_bad_f7554966:


    "Sin embargo, mi alma estaba vacía."


translate spanish epilogue_un_bad_c0d3b99f:


    "Traté en vano de hallar al menos los ecos del dolor, de la culpabilidad, o de la lástima por ella, pero nada hubo."


translate spanish epilogue_un_bad_18791a54:


    "Estaba tan sólo tumbado aquí, mirando fijamente el techo..."


translate spanish epilogue_un_bad_53f73d1e:


    "No estaba interesado para nada en cómo y por qué volví."


translate spanish epilogue_un_bad_5e02e068:


    "Después de todo, ¿a quién le importa el proceso en que vendes tu alma al diablo, todas las formalidades legales del contrato, las firmas, los sellos y timbres?"


translate spanish epilogue_un_bad_db394f22:


    "Lo que es importante es el resultado.{w} Y éste es el resultado que he obtenido."


translate spanish epilogue_un_bad_5e75b8df:


    "¡No, no es que fuera traído de vuelta a la realidad!{w} Si me hubiera subido hacia esa destinación en el autobús, difícilmente podría haber cambiado algo para mí."


translate spanish epilogue_un_bad_53be3572:


    "En este caso, el resultado es lo que le sucedió a Lena.{w} Y la causa son mis acciones.{w} Estaba completamente seguro de ello."


translate spanish epilogue_un_bad_a3b352cb:


    "Después de todo, ella no habría hecho eso sin causa alguna...{w} ¡No, Lena no es así!{w} ¡Así que todo es culpa mía!"


translate spanish epilogue_un_bad_7f92c544:


    "Se hace duro vivir, sabiendo que eres el motivo por el cual otra persona murió."


translate spanish epilogue_un_bad_1b453803:


    "Como si yo personalmente hubiera sostenido el cuchillo, con calma y cuidado le cortara las muñecas y la contemplase morir.{w} Y luego hui..."


translate spanish epilogue_un_bad_bd552dae:


    "Por supuesto, no podía hacer nada en dicha situación, pero aun así sentí que me comporté como un cobarde."


translate spanish epilogue_un_bad_1f993547:


    "No, peor aun..."


translate spanish epilogue_un_bad_0c858154:


    "Aunque, ¿realmente importa cuál es la mejor definición de mis acciones?"


translate spanish epilogue_un_bad_c763d1a6:


    "Me maldecí a mí mismo por permanecer tan tranquilo mientras pensaba sobre esta situación."


translate spanish epilogue_un_bad_d14867bb:


    "Después de todo, debería estar de luto por Lena y culparme a mí mismo...{w} Pero ahora ya nada depende de mí.{w} Si ya no podía entonces... "


translate spanish epilogue_un_bad_f7620350:


    "Comencé a tener escalofríos, mi cuerpo estaba temblando y resultaba difícil respirar."


translate spanish epilogue_un_bad_7a23b79f:


    "Los instintos de supervivencia se sobrepusieron a la culpabilidad por un rato, y me tambaleé hacia la cocina donde bebí un sedante."


translate spanish epilogue_un_bad_8325eee3:


    "Se pueden encontrar siempre en la despensa de cualquier persona antisocial como yo."


translate spanish epilogue_un_bad_06a8cbb9:


    "Una vez me tomé la mitad de las pastillas que habían en el paquete, retorné hacia la habitación y me puse en la computadora.{w} Estar en silencio era insoportable."


translate spanish epilogue_un_bad_561abd10:


    "Reproducí la primera canción aleatoria que apareció y, pronto, me di cuenta de que era probablemente la más deprimente composición musical que tenía en mi disco duro."


translate spanish epilogue_un_bad_2233adb0:


    "A pesar de ello, no quería pararla... el ruido de fondo me ayudaba a enfríar mis pensamientos."


translate spanish epilogue_un_bad_5fa296f2:


    "Tenía que escoger qué hacer ahora con mi vida."


translate spanish epilogue_un_bad_d453809f:


    "Estaba seguro de una cosa... todo lo que había ocurrido en el campamento, mi aparición en él, mi retorno inesperado, no me importaba nada de ello."


translate spanish epilogue_un_bad_a36af85e:


    "Y no solamente eso, tampoco me preocupaba ni el contexto ni los motivos para todos esos hechos."


translate spanish epilogue_un_bad_6fb09a75:


    "La única cosa que me importaba era Lena."


translate spanish epilogue_un_bad_081d7480:


    "Tropecé horriblemente con la palabra «era».{w} Claramente, ella se fue."


translate spanish epilogue_un_bad_cc0ad615:


    "Desde luego, es posible que sólo fuera un sueño y que ella nunca hubiera existido para empezar."


translate spanish epilogue_un_bad_810ea4a2:


    "Pero entonces, también nuestro mundo real podría ser una alucinación engañosa así mismo."


translate spanish epilogue_un_bad_0403a983:


    "¿Por qué no?{w} Si la gente sufre por la pérdida de sus seres queridos {i}aquí{/i}, ¡¿por qué debería pensar en la muerte de Lena {i}allí{/i} como si tan sólo fuera resultado de mi enferma imaginación?!"


translate spanish epilogue_un_bad_ff643764:


    "La vi con mis propios ojos. Sentí la conmoción, el miedo y el temor..."


translate spanish epilogue_un_bad_88638ccf:


    "¡Maldita sea!{w} Para mí no es que aquello {i}fuera{/i} la realidad, es que sigue {i}siendo{/i} la realidad."


translate spanish epilogue_un_bad_9dedbadc:


    "¡Lo es y seguirá siéndolo!"


translate spanish epilogue_un_bad_e2eb86dd:


    "¡Y estoy seguro de que no estoy soñando!"


translate spanish epilogue_un_bad_e27cd146:


    "Aunque sería mejor que soñara aquello..."


translate spanish epilogue_un_bad_59ef3021:


    "El espantoso rugido de las guitarras resonaban desde los altavoces.{w} Sonaban como un réquiem.{w} Un réquiem para mí..."


translate spanish epilogue_un_bad_cb6f7baf:


    "¿Y ahora debo vivir con este sentimiento de culpabilidad?{w} ¡No, no lo aceptaré!"


translate spanish epilogue_un_bad_034f1df0:


    "Mi mente no era exactamente un modelo de estabilidad emocional, pero...{w} Ni tan siquiera un hombre con una mente estable podría encajar estas conmociones sin más."


translate spanish epilogue_un_bad_167e249d:


    "¡Y ya sentía que me estaba volviendo loco!"


translate spanish epilogue_un_bad_14cf330d:


    "Traté de suprimir todos estos pensamientos.{w} No, no para olvidar, solamente para darme un respiro."


translate spanish epilogue_un_bad_6ff084a7:


    "Sólo por un minuto..."


translate spanish epilogue_un_bad_41d75612:


    "Pero no funcionó... el dolor destruía mi cuerpo una vez y otra.{w} Ya comenzaba a sentirlo físicamente.{w} Aun así, el dolor físico es siempre más débil que el dolor mental..."


translate spanish epilogue_un_bad_04d0ffcc:


    "Me caí al suelo, estreché mis rodillas y empecé a mecerme en un vaivén adelante y atrás en posición fetal."


translate spanish epilogue_un_bad_d629c94e:


    "La sangre palpitaba en mi cabeza tan fuertemente, que sentí como mi cráneo se hacía añicos de un momento a otro."


translate spanish epilogue_un_bad_ddf2c20b:


    "Golpeé una pata de la mesa y una pequeña vela rodó por el suelo hasta un montón de papeles."


translate spanish epilogue_un_bad_a52bd6cd:


    "Una pequeña, de unos veinte centímetros de largo, estaba torcida casi en un ángulo recto, pero todavía retenía su forma. El pábilo sobresalía de un extremo."


translate spanish epilogue_un_bad_210e40a9:


    "Busqué un encendedor y prendí la vela."


translate spanish epilogue_un_bad_510f67d0:


    "Que sea en memoria de Lena..."


translate spanish epilogue_un_bad_55f5fa34:


    "Tal vez en otro mundo se encuentre mejor que..."


translate spanish epilogue_un_bad_ce617998:


    "..."


translate spanish epilogue_un_bad_be1b0bed:


    "Me senté en el suelo y observé mientras la cera goteaba en mis dedos."


translate spanish epilogue_un_bad_a54003ea:


    "No sentí ningún dolor... probablemente mi sistema nervioso estaba tan exhausto, que era incapaz de transmitir los impulsos de dolor a mi cerebro."


translate spanish epilogue_un_bad_ef0a06ae:


    "La llama me tranquilizó un poco... simplemente contemplé el fuego y no pensé en nada."


translate spanish epilogue_un_bad_160075f5:


    "Por fin, algo de paz en mi mente..."


translate spanish epilogue_un_bad_ce617998_1:


    "..."


translate spanish epilogue_un_bad_f07d7c82:


    "Se había consumido la mitad de la vela."


translate spanish epilogue_un_bad_bf7d5546:


    "Súbitamente, me imaginé que mi vida era justo esa vela."


translate spanish epilogue_un_bad_f97deac3:


    "No solamente la mía... la vida de cualquier persona."


translate spanish epilogue_un_bad_7d4f29f6:


    "Todo lo que se nos ha dado desde arriba es su larga duración."


translate spanish epilogue_un_bad_a017ae8e:


    "Pero cualquier cosa puede suceder... el viento puede soplar, la mano que la sostiene puede temblar o el pábilo quemarse entero...{w} Y una vida terminará antes de lo que debiera."


translate spanish epilogue_un_bad_2ebac29f:


    "Pero después de todo, ¡cada vela puede ser diferente!"


translate spanish epilogue_un_bad_550851ec:


    "Como por ejemplo ésta... de 3 rublos; una el doble de grande... 7; una de las grandes, gruesa como el mango de una pala... 70."


translate spanish epilogue_un_bad_986feabd:


    "Me pregunto si la vela de Lena se quemó antes de tiempo o fue simplemente más pequeña que las demás..."


translate spanish epilogue_un_bad_d3c73c95:


    "Aunque dudo mucho que haya alguna más pequeña que la mía..."


translate spanish epilogue_un_bad_0a488632:


    "Giré la vela en mis manos."


translate spanish epilogue_un_bad_06a3d48f:


    "Qué interesante es eso... puedo soplar la llama de la vela en cualquier instante, y eso es todo..."


translate spanish epilogue_un_bad_1c8c0f19:


    "Pero mientras tanto, la vida no es cera, no puedes combinar dos pequeñas velas para hacer una de tamaño mediano."


translate spanish epilogue_un_bad_fe0373f7:


    "Me habría encantado ofrecerle los restos que me quedasen a Lena."


translate spanish epilogue_un_bad_038be31f:


    "¡A cualquiera que los necesite más que yo!"


translate spanish epilogue_un_bad_317fb567:


    "¿Por qué los necesitaría? No siento dolor de la cera caliente, su llama no me da ningún calor, a duras penas ilumina la habitación."


translate spanish epilogue_un_bad_d887153f:


    "A decir sin rodeos, un desperdicio de pábilo, cera y oxígeno..."


translate spanish epilogue_un_bad_7bf48038:


    "Soplé la vela."


translate spanish epilogue_un_bad_87239164:


    "Esta acción no evocó ni una sola emoción en mí."


translate spanish epilogue_un_bad_aed890bf:


    "Lentamente me puse en pie y me dirigí hacia el baño."


translate spanish epilogue_un_bad_ce617998_2:


    "..."


translate spanish epilogue_un_bad_a61ee024:


    me "A lo largo, no cruzado...{w} Todo el mundo se corta de forma cruzada, pero tú lo harás justo a lo largo..."


translate spanish epilogue_un_bad_50614f77:


    "La imagen de Lena apareció frente a mis ojos."


translate spanish epilogue_un_bad_efd90378:


    "Estaba sonriendo."


translate spanish epilogue_un_bad_8c835c15:


    me "Nos encontraremos al fin de nuevo...{w} Lo siento mucho..."


translate spanish epilogue_un_bad_9ee997b0:


    "El agua cálida, las sombras y sueños agonizantes me trajeron al fin algo de paz."


translate spanish epilogue_un_bad_16861ec1:


    "Podría ser que, realmente, nunca fui en verdad a aquel campamento."


translate spanish epilogue_un_bad_487e661d:


    "O que nunca volví en realidad."


translate spanish epilogue_un_bad_1c0fe513:


    "Pero acaso importa ahora..."


translate spanish epilogue_un_bad_cb89588c:


    "Casi sentí físicamente el abrazo de Lena."


translate spanish epilogue_un_bad_c4d43861:


    me "Todo irá bien...{w} Cogeremos el autobús juntos y nos iremos hasta un lugar...{w} Adonde nadie nos encontrará...{w} Donde seremos felices juntos..."


translate spanish epilogue_un_bad_7e153b32:


    "Mis fuerzas me abandonaban y empecé a hundirme en el agua, la cual se derramaba por encima del borde de la bañera."


translate spanish epilogue_un_bad_8c835c15_1:


    me "Seguro que nos encontraremos...{w} Perdóname... "


translate spanish epilogue_un_bad_ce617998_3:


    "..."


translate spanish epilogue_un_good_ced219ed:


    un "¡Despiértate!"


translate spanish epilogue_un_good_2e7081b6:


    "Lentamente recobré el conocimiento."


translate spanish epilogue_un_good_451af48e:


    "Es como si me cayera de un acantilado tan alto, que las propias nubes se acumulan a su alrededor."


translate spanish epilogue_un_good_6188d36b:


    "El suelo se está acercando, pero no temo que vaya a morir. Mi caída se ralentiza y mi cuerpo, suavemente, se adentra en el cálido éter..."


translate spanish epilogue_un_good_456fff17:


    "Abrí los ojos."


translate spanish epilogue_un_good_9e2c1e5b:


    un "¡Vas a dormirte toda tu vida como sigas así!"


translate spanish epilogue_un_good_ed8bfd35:


    "Lena me miraba con aspecto serio."


translate spanish epilogue_un_good_0bd39427:


    me "¿Ah? ¿Qué? ¿Ya estamos allí...?"


translate spanish epilogue_un_good_79937daa:


    un "¿Dónde es «allí»?"


translate spanish epilogue_un_good_211eccb4:


    "Miré a mi alrededor."


translate spanish epilogue_un_good_5efc1f6b:


    "Un banco, la plaza, Genda..."


translate spanish epilogue_un_good_aedb5b04:


    "¿Pero cómo?{w} Estaba en el autobús, para irme al distrito central."


translate spanish epilogue_un_good_576a105f:


    un "¿Por qué me estás mirando como si hubieras visto un fantasma?"


translate spanish epilogue_un_good_8bb49e0e:


    me "Pero nosotros...{w} ¡¿Por qué sigo estando aquí?!"


translate spanish epilogue_un_good_a5d4a4e2:


    "Por un segundo sentí un terrible miedo."


translate spanish epilogue_un_good_0113bcd7:


    "¿Es que nunca escaparé de éste lugar?"


translate spanish epilogue_un_good_973d03cf:


    me "Estábamos...{w} En el autobús... Juntos..."


translate spanish epilogue_un_good_5f6db029:


    un "¿En el autobús?"


translate spanish epilogue_un_good_ef4c9ffe:


    me "Ayer..."


translate spanish epilogue_un_good_77a0ecbc:


    un "¿Ayer?"


translate spanish epilogue_un_good_18b26cc6:


    me "¡Sí, ayer! ¿No lo recuerdas?"


translate spanish epilogue_un_good_d6dd39b3:


    "Lena comenzó a pensar."


translate spanish epilogue_un_good_5bd0cf6d:


    un "Ayer...{w} No recuerdo nada de eso.{w} Quizá solamente tuviste un mal sueño."


translate spanish epilogue_un_good_7bef23f0:


    "Considerando que todo lo que ya ha sucedido parece como una alucinación, pues esto debe ser un sueño dentro de un sueño."


translate spanish epilogue_un_good_5f1d7a74:


    me "Bueno, nos lo tomaremos con calma..."


translate spanish epilogue_un_good_54beb87a:


    "Traté de pensar lógicamente."


translate spanish epilogue_un_good_b23cd5f9:


    me "La temporada terminó ayer, todos los pioneros se fueron, pero nosotros nos quedamos...{w} Y entonces cogimos un autobús y nos dirigimos hacia el distrito central."


translate spanish epilogue_un_good_a3e6cada:


    "Lena claramente estaba perpleja."


translate spanish epilogue_un_good_5a05f88f:


    un "¿Me estás tomando el pelo?"


translate spanish epilogue_un_good_5d17104b:


    me "¡No le estoy tomando el pelo a nadie!"


translate spanish epilogue_un_good_be6c71e6:


    un "Hoy es el último día."


translate spanish epilogue_un_good_56a8dc53:


    me "Espera...{w} Entonces, ¿cuánto tiempo me he pasado aquí?"


translate spanish epilogue_un_good_71c0ad20:


    "Lena comenzó a contar con sus dedos."


translate spanish epilogue_un_good_45719f1b:


    un "Parece que... unos siete días."


translate spanish epilogue_un_good_1163b74b:


    me "¿Ocho no?"


translate spanish epilogue_un_good_0262d06c:


    un "Son siete, ¡definitivamente siete!"


translate spanish epilogue_un_good_7b2a2cf9:


    "Solté un suspiro de cansancio y me cubrí mi cara con mis manos."


translate spanish epilogue_un_good_90cfa1fe:


    "Bueno, ése es otro enigma en la larga lista de misterios de este campamento."


translate spanish epilogue_un_good_85181956:


    me "En fin, vale...{w} Aunque tengo la sensación de que no podremos irnos hoy todavía."


translate spanish epilogue_un_good_b672514d:


    un "¿Por qué?"


translate spanish epilogue_un_good_4d730f88:


    me "Sexto sentido."


translate spanish epilogue_un_good_8d11582d:


    un "Te comportas hoy de forma un tanto extraña."


translate spanish epilogue_un_good_5b132458:


    me "Lo supongo, tengo mis motivos..."


translate spanish epilogue_un_good_9d577beb:


    un "¿Me los quieres explicar?"


translate spanish epilogue_un_good_8f838576:


    "La contemplé con atención."


translate spanish epilogue_un_good_e86e77ad:


    me "Lo haré."


translate spanish epilogue_un_good_5ce3e082:


    "Claro, ¿por qué no?"


translate spanish epilogue_un_good_d2bb6bf4:


    "Cuando tuve una oportunidad real de irme de este campamento, todo fracasó..."


translate spanish epilogue_un_good_87127cb1:


    "Así que, ¿qué tengo que perder ahora?"


translate spanish epilogue_un_good_0ee8c6fb:


    me "¿Y ahora por dónde empiezo...?"


translate spanish epilogue_un_good_9e69a9ba:


    un "Comienza por el principio."


translate spanish epilogue_un_good_a3854fdc:


    me "Sí, por supuesto...{w} Bueno, nací, estudié, me casé...{w} Mmmm... en fin, vale, todavía no me he casado..."


translate spanish epilogue_un_good_a20e9f2e:


    un "Aun estás a tiempo."


translate spanish epilogue_un_good_dce47469:


    me "Sí, pero...{w} Bueno... En pocas palabras, es como si no fuera de este mundo."


translate spanish epilogue_un_good_97c2c800:


    "Con la mirada que me hizo, estaba claro que ella no comprendía nada."


translate spanish epilogue_un_good_1072a05d:


    me "Llegué aquí por accidente. Incluso no sé cómo."


translate spanish epilogue_un_good_7887dd77:


    un "¿Y cómo es tu mundo?"


translate spanish epilogue_un_good_c2b423ab:


    "Lena preguntó con aspecto serio."


translate spanish epilogue_un_good_d1d836af:


    me "Es... Bueno...{w} Es virtualmente el mismo, pero veinte años en el futuro... de tu mundo."


translate spanish epilogue_un_good_fb5752ef:


    un "¿Tenéis ya naves espaciales?"


translate spanish epilogue_un_good_f2fa462b:


    me "Bueno, si es por eso, tú también las tienes.{w} De todas maneras, no hemos hecho grandes logros en ese campo todavía."


translate spanish epilogue_un_good_6b84b606:


    un "¡Suena interesante!"


translate spanish epilogue_un_good_bac480a2:


    "No sabría decir si Lena se lo tomaba en serio, o simplemente lo hacía ver."


translate spanish epilogue_un_good_632121f6:


    me "Solía vivir en un lugar completamente diferente también. Lejos hacia el norte, a juzgar por el paisaje. En una gran ciudad."


translate spanish epilogue_un_good_94abe78c:


    un "Nunca he estado en las grandes ciudades."


translate spanish epilogue_un_good_e8e0a840:


    me "Quizá eso sea mejor."


translate spanish epilogue_un_good_9790301a:


    un "¿Me enseñarás tu casa?"


translate spanish epilogue_un_good_96768537:


    "Parecía estar tomándose en serio esta conversación, tanto como yo."


translate spanish epilogue_un_good_7e5baac7:


    me "Bueno, no estoy muy seguro de que exista en este mundo..."


translate spanish epilogue_un_good_8d8e7fcf:


    un "¡Oh, venga ya!"


translate spanish epilogue_un_good_e49a9950:


    "Lena hizo un puchero."


translate spanish epilogue_un_good_481c2e78:


    me "Está bien, claro, lo haré..."


translate spanish epilogue_un_good_df776ecb:


    un "¿Y cómo llegaste hasta aquí?"


translate spanish epilogue_un_good_cf4b3576:


    me "Una tarde subí al autobús, la línea 410 y... me desperté aquí.{w} No sé nada más a parte de eso."


translate spanish epilogue_un_good_39b2719c:


    un "¡La línea 410! ¡Ésa es justo la que hace parada fuera del campamento!"


translate spanish epilogue_un_good_57ebfa1e:


    me "Sí, pero mi 410 viaja en otro lugar... y tiempo."


translate spanish epilogue_un_good_1c2133a9:


    un "¿Prevés volver?"


translate spanish epilogue_un_good_78a5a750:


    "Dijo Lena, y me percaté de un tono de tristeza en su voz."


translate spanish epilogue_un_good_ad816568:


    me "No tengo ni idea de cómo..."


translate spanish epilogue_un_good_cf0505aa:


    un "¿Y si lo supieras?"


translate spanish epilogue_un_good_f078db34:


    me "Haces preguntas difíciles."


translate spanish epilogue_un_good_be29202a:


    un "¿Qué pasa?"


translate spanish epilogue_un_good_72d6612e:


    me "Nada, pero...{w} Verás, justo me acabo de acostumbrar recientemente a estar aquí.{w} Las primeras horas en este campamento fueron un trastorno para mí tal que... No lo sé..."


translate spanish epilogue_un_good_5f555b1e:


    un "Está bien, tienes tiempo para pensártelo."


translate spanish epilogue_un_good_b1d14b10:


    "¿Tiempo para pensármelo?"


translate spanish epilogue_un_good_5540c8a2:


    "Parece que incluso si Lena me cree, ella considera toda esta situación como si tan sólo fuera un típico inconveniente y poco más."


translate spanish epilogue_un_good_59c33efd:


    me "De todas formas, no depende de mí.{w} Posiblemente incluso si quisiera quedarme...{w} Verás, el que me trajo aquí también puede devolverme igualmente."


translate spanish epilogue_un_good_c17e95b3:


    un "Creo que cualquier suceso en la vida de una persona tiene una razón de ser.{w} Por tanto, tú estás aquí por algún motivo."


translate spanish epilogue_un_good_1b2ffe6d:


    me "Tal vez..."


translate spanish epilogue_un_good_5db9f7be:


    un "Y si no has retornado...{w} Es porque se te necesita aquí."


translate spanish epilogue_un_good_da6af767:


    me "En fin, no puedo descartar esa interpretación."


translate spanish epilogue_un_good_9a80266d:


    mt "¡Ey! ¡Hora de hacer las maletas!"


translate spanish epilogue_un_good_9719a190:


    "La voz de la líder del campamento se escuchó de lejos."


translate spanish epilogue_un_good_7ef75530:


    un "¿Escuchaste eso?"


translate spanish epilogue_un_good_2bf6ead1:


    me "No tengo mucho que hacer..."


translate spanish epilogue_un_good_c09b5abb:


    un "¡Pero yo sí!{w} Así que... ¡nos vemos cerca del autobús!"


translate spanish epilogue_un_good_86be10c0:


    me "¿Quieres que te ayude?"


translate spanish epilogue_un_good_8fa9fd07:


    un "Gracias, pero no hace falta."


translate spanish epilogue_un_good_22f7b476:


    "Lena sonrió y corrió en dirección a su cabaña."


translate spanish epilogue_un_good_6c54000c:


    "Continué sentado en el banco, tratando de superar el trastorno."


translate spanish epilogue_un_good_c391d6da:


    "Mis pensamientos seguían siendo un caos, una teoría detrás de otra."


translate spanish epilogue_un_good_094c98fd:


    "Por un lado, siendo yo incapaz de escapar de este campamento, no más extraño que hallarme a mí mismo aquí otra vez, pero..."


translate spanish epilogue_un_good_a33f4f66:


    "¿Y ahora qué? ¿Qué se supone que voy hacer?{w} ¿Hay alguna posibilidad de escapar del campamento si lo vuelvo a intentar?"


translate spanish epilogue_un_good_675b9b96:


    "De todas formas, ¿qué sentido tiene estar de los nervios?{w} Por ahora ya sé perfectamente que nada depende de mí en absoluto."


translate spanish epilogue_un_good_70c222fc:


    "¿Quizá simplemente tendría que sentarme detrás y disfrutar del viaje?"


translate spanish epilogue_un_good_ef2764bc:


    "No puedo hacer otra cosa que sonreír."


translate spanish epilogue_un_good_b2d4b2d6:


    "Especialmente ahora, cuando tengo un motivo de más por el que no tengo prisa para escaparme.{w} Y ese motivo es Lena."


translate spanish epilogue_un_good_3ed77c37:


    "Tal vez ella esté en lo cierto, y estoy aquí por alguna razón."


translate spanish epilogue_un_good_4a181a23:


    "Al final, incluso si reduciera las explicaciones posibles para mi situación a un mínimo razonable, todavía habría miles."


translate spanish epilogue_un_good_ce005bce:


    "Y si persistiera agotándome en analizarlas, sencillamente me volvería loco."


translate spanish epilogue_un_good_251ac201:


    "¿Quizás es la hora de escoger una opción?"


translate spanish epilogue_un_good_72f1add5:


    "Tal vez este mundo no es tan malo después de todo.{w} Especialmente desde que por primera vez en mucho tiempo, tengo algo por lo que vivir."


translate spanish epilogue_un_good_994914a4:


    "Todavía es algo difuso, pero ya tengo algo, ¡existe!"


translate spanish epilogue_un_good_8ae8f091:


    "Y ahora tengo el poder no simplemente de retenerlo, ¡sino de desarrollarlo en algo mucho más grande!"


translate spanish epilogue_un_good_88e5892d:


    "Con tales pensamientos, me puse en pie y comencé a caminar hacia la cabaña de Olga Dmitrievna con el fin de empacar mis humildes pertenencias y así abandonar el campamento de pioneros Sovyonok para siempre."


translate spanish epilogue_un_good_ce617998:


    "..."


translate spanish epilogue_un_good_752fc388:



    nvl clear
    "..."


translate spanish epilogue_un_good_2000e780:


    "Uno pudiera pensar que es extraño (y así es como exactamente me lo parecía a mí), pero finalmente alcanzamos el distrito central. Con el tiempo me cansé en el autobús y me dormí. Cuando me desperté corrí para bajar por el pasillo del autobús, jadeando por aire, ¡pero pronto me di cuenta de que esta larga semana se acabó!"


translate spanish epilogue_un_good_a8aed4e9:


    "Al fin me las arreglé para huir del campamento. Todo el tiempo que transcurrí allí me pareció mucho más largo que sólo siete días... Y ahora todo se acabó."


translate spanish epilogue_un_good_33d7cf72:


    "Pronto tras eso, los acontecimientos se sucedieron a un ritmo veloz."


translate spanish epilogue_un_good_b7912630:


    "No hubo tiempo para reflexionar o para «hallar respuestas». Además, de vez en cuando me olvidaba completamente del campamento."


translate spanish epilogue_un_good_6383c486:


    "Como cualquiera que se encontrara a sí mismo en un entorno completamente extraño, estaba totalmente perdido. No tenía documentación, ni tan siquiera un simple certificado de nacimiento y ninguna habilidad para ganarme la vida... profesiones tales como especialista en informática o teleoperador no estaban demandadas."


translate spanish epilogue_un_good_b54c638a:



    nvl clear
    "Lena retornó a su vida normal. Ella tenía que graduarse en su instituto y luego prepararse para acceder a la universidad. El campamento de pioneros estaba en el sur de la URSS, tal como presumí. Lena vivía en una ciudad con una población alrededor de cien mil habitantes. Era fácil llegar hasta allí con el autobús desde el distrito central."


translate spanish epilogue_un_good_c7376b62:


    "No había nada de especial en la ciudad. Una gran fábrica, hileras de viviendas de cinco pisos, cabañas de madera en la periferia, tiendas de víveres que cerraban a las siete de la tarde y un supermercado, un verdadero Edén para los adictos a las compras, con opciones que incluían zapatos de goma para invierno, abrigos de piel de borrego para mujeres y sombreros de ratón almizclero para hombres."


translate spanish epilogue_un_good_02306dd9:


    "Antes ya habría tratado escapar de un lugar como éste, pero ahora era mi hogar. Los casos de hambruna eran raros en el siglo XX en Europa. Me las apañé para encontrar un trabajo. Me empleé como asistente de tornero en una fábrica y también me dieron una habitación en una pensión."


translate spanish epilogue_un_good_e8a877a1:



    nvl clear
    "Con el tiempo, este trabajo inicialmente difícil y extraño comenzó a darme cierta satisfacción. Parcialmente, por supuesto, ya que me aseguraba la subsistencia para comprar alimentos. El tiempo pasó y escalé toda una carrera, conviertiéndome en jefe de todo un turno de trabajo."


translate spanish epilogue_un_good_6bd80c6f:


    "Mis colegas estaban tan asombrados de mi talento y mi perseverancia como yo lo estaba. Lena se graduó del instituto y yo me pasé varias noches con una calculadora, diseñando diagramas de gastos e ingresos y, al fin, me preparé para proponerle matrimonio a Lena."


translate spanish epilogue_un_good_8de1f0df:


    "No recuerdo ahora si le llevó bastante tiempo pensárselo, si sugirió que firmáramos un acuerdo prenupcial o si protestó por la falta de una dote, pero en poco tiempo ambos nos mudamos a una habitación de piso comunitario cerca de la fábrica que me costó dos tercios de mi salario."


translate spanish epilogue_un_good_ce390941:



    nvl clear
    "Gracias a la educación gratuita soviética para todos o tal vez gracias a la perseverancia de mi esposa, al final me hice estudiante a tiempo parcial de la universidad politécnica de la ciudad."


translate spanish epilogue_un_good_467cd7ed:


    "Al principio protesté enérgicamente, diciendo que un proletario honesto no necesitaba actuar como los repugnantes intelectuales, pero después llegué a la conclusión de que una titulación haría mi vida más llevadera."


translate spanish epilogue_un_good_d5213709:


    "No fue difícil estudiar gracias a mis conocimientos de mi último intento al hacer estudios superiores. El tiempo transcurrió y el país comenzó a cambiar. Desde luego que ya sabía qué ocurriría, pero aun así los cambios vinieron por sorpresa."


translate spanish epilogue_un_good_5f973fa2:



    nvl clear
    "Podrías compararlo con ser atrapado por una avalancha en plena montaña. Te puedes preparar todo lo que quieras, que de todas maneras te enterrará. La fábrica fue privatizada y entonces cerró."


translate spanish epilogue_un_good_7676e0eb:


    "Pluriemplearme como conductor particular de un «Kopeyka» destartalado que heredé de mi suegro, no era realmente un negocio rentable. Estuve incluso preparado para inscribirme en un puesto de líder de campamento en Sovyonok, cuando el último pariente de Lena falleció y nos dejó una habitación de un piso en alguna parte de la Rusia central."


translate spanish epilogue_un_good_c490afc8:


    "Tras un consejo familiar decidimos mudarnos. Dimos la bienvenida al inicio de la década de los noventa en una estrecha cocina de una khrushchovka, viendo «El Lago de los Cisnes». Gracias a trabajos esporádicos extras ahorré algo de dinero y decidimos mudarnos a una gran ciudad. Y por supuesto, escogí mi lugar de nacimiento..."


translate spanish epilogue_un_good_b16966f7:



    nvl clear
    "Decidimos vender el piso y utilizar el dinero para invertir en negocios. Por algún tiempo vivimos entre el lujo, comprando abrigos de piel de visón y vehículos extranjeros caros, comiendo en restaurantes y haciendo viajes a otros países. Como muchos otros durante aquella época, tuve la suerte de crecer, empezando desde abajo."


translate spanish epilogue_un_good_082d8313:


    "Mi negocio era venta al por menor de material de construcción. Era un mercado rentable en aquel momento ya que la gente conseguía mucho dinero y querían invertirlo en viviendas lujosas."


translate spanish epilogue_un_good_200309d6:


    "Quizá tal estilo de vida podría haber funcionado, pero a los noventa se les llamó «libertinos» por alguna razón. Tras dar con los fraudes y la corrupción, me quedé con un frigorífico vacío, una cartera vacía y con un amargo sabor de sangre en mi boca."


translate spanish epilogue_un_good_f5d9692f:



    nvl clear
    "Ésa fue la época en que traté de evocar mi condición de «hombre del futuro». Casas de apuestas que eran inexistentes en la era soviética aparecieron por todas partes y corrí a apostar por todo lo que sabía. Sin embargo, encajé nuevas decepciones, en tanto que los partidos eran ganados por equipos completamente diferentes. Incluso el campeón de la Copa Mundial de Fútbol de 1994 no fue Brasil."


translate spanish epilogue_un_good_353ba4d9:


    "Cuando simplemente veía el fútbol en la televisión, todo iba como debería, pero en cuanto me gastaba hasta la última de mis monedas apostando, incluso hasta los perdedores acababan venciendo fenomenalmente a los favoritos."


translate spanish epilogue_un_good_ea090535:


    "Además de eso, traté de continuar con una carrera de consejero político. Sin embargo, no te aceptan en semejantes posiciones tan altas sin tener contactos especiales o alguna cosa que hiciera a la gente interesarse. No sé cuánto tiempo habría seguido a través de ese calvario si no fuera por un suceso que lo cambió todo... Lena y yo tuvimos un niño."


translate spanish epilogue_un_good_c402b437:



    nvl clear
    "Apurado, comencé a buscar cualquier posible vía para ganar suficiente como para mantener mi familia. Recordé algunos de los viejos amigos de mi padre y a través de ellos, simulando ser un pariente lejano de mí mismo, conseguí el puesto de analista de datos de principiante en un banco. Al mismo tiempo hice un nuevo intento para obtener mi grado (o para ser más precisos para {i}terminarlo{/i})."


translate spanish epilogue_un_good_0c3a6b1b:


    "El tiempo pasó lentamente. El dinero era el justo para llenar nuestros estómagos. Casi vivía en el trabajo durante días, tratando de encontrar tiempo para mis exámenes. Lena se quedó en casa con el bebé. Eso fue cuando comencé a escribir... Lo recuerdo como si fuera ayer. Fue justo tras medianoche y estaba sentado en la cocina frente a mi antiguo y desgastado i386, matando violentamente monstruos en «DOOM II»."


translate spanish epilogue_un_good_fa74f193:


    "Quería dormir a rabiar, pero los videojuegos de computadora por lo menos me distraían ligeramente del infinito aburrimiento de los días. Cuando completaba otro nivel, estaba súbitamente inmerso en pensamientos sobre todo lo que había pasado durante los últimos años. Era todo demasiado para una persona. Y así fue cuando decidí escribir todo lo que me había ocurrido."


translate spanish epilogue_un_good_65ff89ca:



    nvl clear
    "Abrí el procesador de textos y escribí alrededor de doscientas palabras, con la clara intención de continuarlo mañana. Sin embargo no lo recordé al día siguiente, ni en una semana después."


translate spanish epilogue_un_good_a55760f5:


    "Aproximadamente un mes después Lena, quien estaba tecleando algo en la computadora, me recordó mi libro inacabado. Lo releí y lo eliminé, aterrorizado. En aquel momento empecé a dudar de que pudiera siquiera hacerlo. Después de todo, era una completa y épica novela, y no era capaz de escribir más de dos enunciados juntos."


translate spanish epilogue_un_good_a0fb86f8:


    "Para añadir que no es nada fácil escribir sobre ti mismo... Por lo que el trabajo creativo lo puse en segundo plano por ahora. Celebré la llegada del año 2000 como un especialista acreditado con responsabilidades de jefe de departamento. Mientras me mudé a otro piso, hallé a mi viejo i386 entre la basura que iba a tirar y recordé mis intenciones de escribir un libro sobre mi vida."


translate spanish epilogue_un_good_b64fc8f9:



    nvl clear
    "Me reí. Qué estúpido. Aun así, tras algún tiempo abrí mi portátil e hice una suerte de borrador de una corta historia acerca de un joven hombre sin una definitiva ocupación ni aspiraciones, el cual se halla en un planeta diferente. El estilo, la ortografía y la puntuación, así como la propia idea, se ajustaban muy bien a dicho absurdo guión."


translate spanish epilogue_un_good_06d5c1a8:


    "Le mostré la historia a mi esposa. Ella se rio pero dijo que estaba bien escrita y me aconsejó continuar."


translate spanish epilogue_un_good_62204972:


    "Alrededor de un año después, mi cajón designado para guardar mi obras literarias estaba lleno. Lo encontré bastante raro porque me pasaba días enteros en el trabajo, a veces incluso durante vacaciones, y trataba de pasar la mayor parte de mi tiempo libre con mi familia. Aun así, eso es lo que ocurrió."


translate spanish epilogue_un_good_16103041:



    nvl clear
    "Por aquella época decidí publicar mis cortas historias. Como con cualquier trabajo de principiante, la mayoría del mío fue rechazado, pero un par de ellos fueron aceptados (incluyendo mi primera obra). Estaba tocando el cielo con los dedos. Por supuesto, no me pagaron nada, ¡pero mi obra se podría leer! Lo cual significa que alguien la encontraría interesante."


translate spanish epilogue_un_good_d17d0576:


    "El tiempo pasó. Tuvimos un segundo bebé. Durante mi tiempo libre escribí lentamente una novela. No, no acerca de mi vida o sobre el campamento de pioneros. Decidí no volver a tocar esos temas nunca más. Todo lo que ocurrió años atrás pareció ser la voluntad de Dios, misericordia que no debería ser tentada."


translate spanish epilogue_un_good_bc5da8f3:


    "¿Quién puede decir qué habría sucedido si no me hubiera despertado en un autobús a las puertas del campamento Sovyonok?"


translate spanish epilogue_un_good_d7be6fd0:


    "Un año después la novela fue terminada. Tras hacer enfadar a casi todos los editores, me las apañé para alcanzar un acuerdo con una pequeña compañía. El editor, un hombre anciano que vestía una androjosa chaqueta de tweed y unas gruesas gafas las cuales casi rozaban su magnífico bigote gris, me dijo:"


translate spanish epilogue_un_good_d813e73f:



    nvl clear
    "«Tu habilidad es corriente, joven... Corriente, sí. Pero la idea por sí misma es interesante... interesante, sí. Bueno, necesitas más práctica. Y, mmm... Más lectura... Sí.»"


translate spanish epilogue_un_good_d13fddf7:


    "Me acordé de estas palabras para el resto de mi vida. Repartí las copias que me enviaron como parte del acuerdo de licencia entre mis amigos y colegas. Lena, quien leyó solamente mi novela tras ser publicada, me contó que le gustó mucho. Básicamente como un deseo de no hacerme enfadar, supongo."


translate spanish epilogue_un_good_78ec671a:


    "El tiempo transcurrió... Los días del calendario eran imparables cuando se aproximaban a aquel día en que fui arrebatado de mi mundo habitual y puesto en esta vida, comenzando por el misterioso campamento de pioneros y ahora acabando aquí. Ahora soy un escritor de éxito (al menos de acuerdo con el número de copias de libros que he vendido), tengo una esposa maravillosa y dos niños. Mi vida ha dado un giro de 180 grados."


translate spanish epilogue_un_good_e0db8497:



    nvl clear
    "Hoy a medianoche es el comienzo de algo nuevo. {i}Mañana{/i} es el día en que estuve esperando durante tantos años."


translate spanish epilogue_un_good_2cd0045b:


    un "¿En qué estás pensando?"


translate spanish epilogue_un_good_83704a72:


    "Abrí mis ojos y vi a Lena."


translate spanish epilogue_un_good_8e609a05:


    "Ella puso una taza de té en la mesa, cerca del sillón."


translate spanish epilogue_un_good_72199a1c:


    "La leña estaba crepitando suavemente en el fuego de la chimenea, y una tormenta de nieve estaba azotando fuera."


translate spanish epilogue_un_good_932e1068:


    "Me envolví en una sábana de cuadros y miré a Lena."


translate spanish epilogue_un_good_f07f7371:


    me "¿Sabes qué día es hoy?"


translate spanish epilogue_un_good_6b22e1e1:


    un "No, ¿cuál?"


translate spanish epilogue_un_good_e152372b:


    me "¿Te acuerdas de lo que te conté tiempo atrás en el campamento?"


translate spanish epilogue_un_good_049b021f:


    un "No."


translate spanish epilogue_un_good_13821bda:


    "No me extraña. Durante todos estos años nunca volví a sacar el tema de mi misteriosa aparición en este mundo."


translate spanish epilogue_un_good_1814dbd5:


    me "¿Te acuerdas cuando te conté que no era de este mundo?"


translate spanish epilogue_un_good_d6dd39b3_1:


    "Lena se detuvo."


translate spanish epilogue_un_good_9ebd484f:


    un "Bueno, algo así... sí..."


translate spanish epilogue_un_good_3ed0a2cc:


    me "Pues hoy es el día en que, hace muchos años atrás, me subí en el autobús 410 y me desperté en el campamento de pioneros."


translate spanish epilogue_un_good_ffd733f0:


    un "Entonces, ¿es como un aniversario?"


translate spanish epilogue_un_good_561828ec:


    "Lena sonrió."


translate spanish epilogue_un_good_ca6a2d01:


    me "Sí, algo así..."


translate spanish epilogue_un_good_58a9940e:


    un "¿Es una celebración triste para ti?"


translate spanish epilogue_un_good_0d7f2c30:


    me "No, ¡para nada!"


translate spanish epilogue_un_good_4013090d:


    un "Yo ya no recuerdo mucha cosa de aquella época..."


translate spanish epilogue_un_good_e68e962b:


    me "Quizá sea lo mejor..."


translate spanish epilogue_un_good_dd47136a:


    "Añadí algo de leña al fuego y agarré la taza de la mesa."


translate spanish epilogue_un_good_429c57e7:


    me "Tal vez escribiré una novela.{w} La leerás y te acordarás."


translate spanish epilogue_un_good_b63153d6:


    un "¿Crees que es una buena idea?"


translate spanish epilogue_un_good_aed0e1bf:


    "Lena se puso seria."


translate spanish epilogue_un_good_1b89c45a:


    "Desde luego, una vez me prometí a mí mismo que no escribiría nunca sobre ello."


translate spanish epilogue_un_good_2141272e:


    me "Quizá no.{w} Después de todo, todo lo que nos ocurrió es sólo para nosotros dos."


translate spanish epilogue_un_good_e5f761b0:


    un "Eso es exactamente lo que creo."


translate spanish epilogue_un_good_82c3c74a:


    "Suspiré y miré al fuego."


translate spanish epilogue_un_good_b5944be5:


    me "Sabes, la vida es como una vela. Algunos tienen una corta, otros una larga. Y puede terminarse de quemar en cualquier momento."


translate spanish epilogue_un_good_76bc0c24:


    un "Entonces nuestra vida es como este fuego de chimenea."


translate spanish epilogue_un_good_d76f93fa:


    me "Sí, ojalá."


translate spanish epilogue_un_good_5c7f770a:


    "La miré y sonreí."


translate spanish epilogue_un_good_1064aa29:


    me "Dormiré un rato más."


translate spanish epilogue_un_good_9271b549:


    "Lena me besó y fijó su mirada en el fuego de una manera soñadora."


translate spanish epilogue_un_good_18bde327:


    "Mis ojos se cerraron y mi entera realidad se redució toda al crepitar de la leña."


translate spanish epilogue_un_good_b130e182:


    "Comencé a caer lentamente, en alguna parte lejos."


translate spanish epilogue_un_good_da5a6c7c:


    "No, no fue un sueño. Más bien fue como un éter cálido, rodeando mi entera existencia suavemente."


translate spanish epilogue_un_good_ce617998_1:


    "..."


translate spanish epilogue_us_f08a51bb:


    "Sentí como si no hubiera dormido a duras penas."


translate spanish epilogue_us_b10d729b:


    "Sucede muchas veces. Sólo cierras tus ojos y caes dormido, la manecillas de las horas hacen varias vueltas, la mañana se aproxima y tú despiertas, pero se te pegan las sábanas."


translate spanish epilogue_us_f8bebe71:


    "Bostecé tanto que casi me rompo la mandíbula y me puse en pie a causa del dolor."


translate spanish epilogue_us_ab939c0c:


    "Alguna cosa iba mal..."


translate spanish epilogue_us_2a6b0fba:


    "A pesar de «alguna cosa»... ¡todo iba mal!"


translate spanish epilogue_us_7c5d345f:


    "Había vuelto a mi piso."


translate spanish epilogue_us_d3ab1684:


    "Pero...{w} ¿Pero cómo es eso posible?"


translate spanish epilogue_us_b97b6ee6:


    "Comencé a entrar en pánico y a moverme alrededor de mi habitación con la esperanza de calmarme."


translate spanish epilogue_us_32fd8672:


    "El cansancio físico puede superar a menudo el emocional."


translate spanish epilogue_us_2c5d0412:


    "Mi cabeza estaba vacía, el miedo y el terror se apoderaron completamente de mi ser, y algún tipo de canción flotaba en mi mente... quizás una oración o tal vez simplemente un embrollo incoherente de pensamientos destinados, si no para tranquilizarme, pues al menos para distraerme del pánico."


translate spanish epilogue_us_1645c72d:


    "Había pasado media hora cuando me desplomé en el suelo, exhausto, y fijé mis ojos en el techo."


translate spanish epilogue_us_80b0da80:


    "Pareciera que no estuve en ninguna parte. La antigua lámpara de candelabro me observaba cruelmente con sus polvorientos brazos, las grietas en el yeso de las paredes estaban en su mismo lugar, y el empapelado despegado de las paredes no se había movido ni un poco."


translate spanish epilogue_us_8d04f2aa:


    "¿Fue realmente un sueño...?"


translate spanish epilogue_us_69c49704:


    "¡Pero no puede serlo!{w} ¡Sencillamente no puede serlo!"


translate spanish epilogue_us_afbfd5dd:


    "Transcurrí una semana entera en aquel campamento de pioneros."


translate spanish epilogue_us_164fbde5:


    "Claramente estuve allí. Recuerdo todo perfectamente, desde mi despertar a bordo del autobús hasta justo mi partida...{w} Ni los sueños ni las alucionaciones pueden ser tan reales."


translate spanish epilogue_us_ca2a0b33:


    "De alguna forma me levanté, fui a la cocina, llené un vaso de agua y retorné a la habitación."


translate spanish epilogue_us_d0a7feb4:


    "La sangre palpitaba todavía en mis sienes, pero por lo menos el pánico de los primeros minutos se fue.{w} O sólo se dio un respiro."


translate spanish epilogue_us_c3ffb486:


    "Me concentré en lo último que recordaba. Era la partida del campamento."


translate spanish epilogue_us_7136c5b9:


    "La noche, el autobús rebotando con las sacudidas, la suciedad del vidrio empañado, más allá del cual nada era visible y los pioneros..."


translate spanish epilogue_us_2ce6d3ca:


    "Estaba completamente seguro de que nunca retornaría.{w} O sencillamente es que no pensé en ello."


translate spanish epilogue_us_8dcc331d:


    "De todas formas, estaba preparado para nuestra llegada al distrito central en pocas horas y estaba también considerando posibles opciones de cara a un futuro."


translate spanish epilogue_us_d8532661:


    "¿O no?"


translate spanish epilogue_us_0babffab:


    me "¡Maldita sea!"


translate spanish epilogue_us_0e363452:


    "Vociferé y estiré del cabello con fuerza."


translate spanish epilogue_us_5058105d:


    me "¡No puedo acordarme!"


translate spanish epilogue_us_60547951:


    "Las últimas horas en aquel mundo se mezclaron en un monótono espejismo, como si un borracho Renoir estuviera acabando su pintura con un rodillo en vez de con un pincel."


translate spanish epilogue_us_3302407b:


    me "Pero no es tan malo.{w} Espera un minuto, ¿por qué tendría que ser malo? Por el contrario, ¡todo está correcto! ¡Incluso diría que perfecto!"


translate spanish epilogue_us_d0f0488f:


    me "Me he liberado de las cadenas de aquel maldito mundo y he vuelto a casa. Lo más importante ahora es no volver otra vez allí.{w} Puede ocurrir, ¡seguro que sí!"


translate spanish epilogue_us_ce617998:


    "..."


translate spanish epilogue_us_f896d7c0:


    me "¡Pero también puede que no ocurra!{w} Desde luego, no tengo nada de qué preocuparme ahora, ¡todo irá bien! Obviamente sólo estuve imaginándome cosas. Exacto, ¡me lo imaginaba!"


translate spanish epilogue_us_4d149f2f:


    me "No importa que todo lo sintiera como si fuera real, algo así no puede ocurrir. ¡No puede! Exclamé con toda seguridad que algo así no puede suceder. La ciencia moderna afirma que es imposible. ¡Que no está permitido!"


translate spanish epilogue_us_f4546a9f:


    "Me reí a carcajadas."


translate spanish epilogue_us_33c6265f:


    "Una voz interior trató de detener la diarrea verbal que escupía por mi boca, pero no tuvo éxito."


translate spanish epilogue_us_27fabe99:


    "Es como si mis palabras y mis pensamientos se separasen para poder existir de forma independiente entre ellos."


translate spanish epilogue_us_4a73860c:


    "Mi cerebro raudo me tranquilizó e intenté analizar la situación, mientras sencillamente mi lengua trató de aliviar el estrés mediante pronunciar más y más palabras nuevas sin sentido alguno."


translate spanish epilogue_us_94206733:


    "Finalmente, de alguna manera fui capaz de recuperar la compostura. Aparté las cortinas y miré a través de las ventanas."


translate spanish epilogue_us_a5c5546b:


    "La ciudad por la noche estaba exactamente igual que hace una semana atrás."


translate spanish epilogue_us_450bbc84:


    "El panorama me trajo de vuelta a la realidad, por lo menos en gran parte."


translate spanish epilogue_us_533137c4:


    "Después de todo, si todo lo sucedido es ahora normal y nada es sospechoso o sobrenatural, ¿acaso no fue entonces un sueño?"


translate spanish epilogue_us_94950be4:


    "Esencialmente, ahora sólo hay dos opciones."


translate spanish epilogue_us_06d20129:


    "Puedo estar de acuerdo en que fue solamente un sueño y tranquilizarme.{w} O puedo creer mis propios sentimientos y aceptar que el campamento, el autobús y los pioneros fueron reales."


translate spanish epilogue_us_193a19e2:


    "Da igual cuál escoja, no voy a obtener ninguna respuesta."


translate spanish epilogue_us_adf47948:


    "Es gracioso, estuve buscando dichas respuestas durante toda una semana (o por lo menos intentándolo) y no hallé ninguna, pero me liberé de aquel extraño mundo de todas formas. ¿Y ahora qué?"


translate spanish epilogue_us_62a5f438:


    "El enigma permanece y más cuestiones se han planteado."


translate spanish epilogue_us_2b19ebd4:


    "Al final, estaba cansado y me dejé caer en la cama. En pocos segundos estaba roncando felizmente..."


translate spanish epilogue_us_ce617998_1:


    "..."


translate spanish epilogue_us_54d920bf:



    nvl clear
    "Mucho tiempo ha pasado desde el momento en que volví de Sovyonok. Y muchas cosas han cambiado en mi vida."


translate spanish epilogue_us_bd3a1548:


    "Durante la primera semana estuve estresándome la cabeza a base de recapitular los detalles de todo lo que había ocurrido, dibujando gráficos y diagramas, escribiendo en foros para hablar sobre fenómenos paranormales. Incluso tuve en mente visitar un psíquico."


translate spanish epilogue_us_9462ce27:


    "Al final quedó en nada, justo como esperaba. No es de extrañar. Tan sólo era una criatura humana, y estos hechos eran obviamente un asunto de un ente supremo."


translate spanish epilogue_us_6101364c:


    "Si un cavernícola se hallara a sí mismo a principios del siglo XXI, hasta él comprendería más las cosas que yo."


translate spanish epilogue_us_7ec6a1be:



    nvl clear
    "Bueno, al principio pensaría que un teléfono móvil que transmite la voz de alguien a docenas de kilómetros de distancia, sería una voluntad divina milagrosa."


translate spanish epilogue_us_29d72188:


    "Pero uno se puede acostumbrar a ello."


translate spanish epilogue_us_e0d7c1dc:


    "Quizá si completara unos estudios secundarios y una educación superior técnica, sería incluso capaz de comprender cómo funciona la comunicación móvil."


translate spanish epilogue_us_82144eab:


    "Es un ejemplo básico exagerado. Tan irreal como todo lo que experimenté."


translate spanish epilogue_us_65950373:


    "Es improbable que llegue a comprender cómo sucedió y quién estaba detrás de ello. Y dudo que pudiera acostumbrarme a ello, si me volviera a ocurrir otra vez."


translate spanish epilogue_us_9bdf285f:



    nvl clear
    "Pero una cuestión me perturbaba desde entonces... ¿Por qué, por qué yo?"


translate spanish epilogue_us_7b47c1ee:


    "¿Qué hice yo (o no hice) para merecer esta mala (o buena) suerte?"


translate spanish epilogue_us_5535f094:


    "Por supuesto, en las novelas de ciencia ficción a menudo ocurren maravillas a personas bastante ordinarias, pero eso solamente fue pura casualidad."


translate spanish epilogue_us_716452f4:


    "Pero estaba completamente seguro de que estos hechos ocurrieron por alguna razón. Justo como un genérico héroe de ciencia ficción que viajó a través de miles de años en el futuro, perdiéndose en el lugar y tiempo equivocados."


translate spanish epilogue_us_6a21d9ba:



    nvl clear
    "Pero en el futuro nadie le conoció. Todos creían que era tan sólo un loco."


translate spanish epilogue_us_9087af0c:


    "¿Pero y en mi situación? Me estaban esperando en el campamento (al menos la líder del campamento lo estaba). ¿Qué respuesta tendría frente a esto?"


translate spanish epilogue_us_7d414027:


    "¿Quizá fui escogido de forma aleatoria para ser estudiado posteriormente? Incluso así, la pregunta de «por qué» no solo sigue en pie, sino que además se vuelve incluso más importante."


translate spanish epilogue_us_ce617998_2:


    "..."


translate spanish epilogue_us_4284aa16:



    nvl clear
    "Tras no encontrar ninguna explicación razonable, retorné a mi vida habitual."


translate spanish epilogue_us_28ca60d9:


    "Pero ahora ya no estaba sentado frente a mi computadora las veinticuatro horas de la semana, limpiando la tecla F5 del polvo... tuve nuevos intereses en mi vida (pero no sabría decir a ciencia cierta cuáles), y repentinamente quise hacer estudios superiores."


translate spanish epilogue_us_e0d94309:


    "No para encontrar un trabajo y tampoco porque «sea necesario», simplemente porque me acordé de lo divertido que sería durante el primer año de los estudios."


translate spanish epilogue_us_ddf05860:


    "Charlar con compañeros de curso, los días de diversión juvenil desenfadada, repletos de la energía que tanto eché de menos durante los últimos años."


translate spanish epilogue_us_9b73f6c1:


    "Un gran hombre dijo una vez que «Un hombre que tanto lee y usa tan poco su cerebro, recae en hábitos de pereza mental». Resulta difícil discutir dicho razonamiento."


translate spanish epilogue_us_6a62e6c6:



    nvl clear
    "El hombre normal y corriente no se cruza de brazos porque sea perezoso, sino sencillamente porque el asunto en cuestión no es de su interés."


translate spanish epilogue_us_b4927a3f:


    "Por ejemplo, no estoy interesado en coser. ¿Significa eso que soy un haragán si no hago un par de jerseys de lana al mes?"


translate spanish epilogue_us_67291156:


    "Pero sentirse demasiado perezoso como para abrir un libro... eso es algo que puedo comprender. Aunque me guste leer, cuando me pongo a pensar que tengo que leer una voluminosa novela, página por página..."


translate spanish epilogue_us_68cd3f08:


    "E incluso si es interesante, incluso si me absorbe completamente hasta el punto de que no puedo dejarlo, esa parte sólo llega al final. Ahora mismo todo lo que tengo que hacer es coger un libro de la estantería y abrirlo. Pero siento tanta pereza..."


translate spanish epilogue_us_83bbcd46:


    "No sé exactamente qué me influenció, pero casi la mitad de mi tiempo (en vez de entre el cinco y diez por ciento de antes) me entretenía con algo útil... leí, escribí, estudié algo nuevo y me comprometí para hacer deporte (por lo menos, hacía ejercicio por las mañanas)."


translate spanish epilogue_us_01e63747:



    nvl clear
    "A veces pensé que era la influencia del campamento."


translate spanish epilogue_us_6b3df02c:


    "Uno no puede descartar dicha posibilidad... durante todos los siete días de la semana estaba participando en actividades sociales, la comunicación verbal en buena medida con los demás, me habría causado antes pavor y una severa introversión."


translate spanish epilogue_us_c9700033:


    "Por otra parte, no es fácil cambiar la personalidad de uno en solamente una semana."


translate spanish epilogue_us_9deeccfb:


    "Especialmente para alguien tan terco como yo."


translate spanish epilogue_us_ae21b9f8:



    nvl clear
    "Pero es fácil mostrarles una nueva dirección, mostrarles la línea que los guíe."


translate spanish epilogue_us_cfcb2a60:


    "Aun así, nunca creería que sería posible con alguien como yo."


translate spanish epilogue_us_7d323ccc:


    "De todas formas, disfruté tanto estos cambios que traté de no pensar sobre sus causas."


translate spanish epilogue_us_ae7018a0:


    "Qué demonios... nadie se preocuparía por el «porqué» si les pudiera tocar el gordo apostando sus últimos pantalones."


translate spanish epilogue_us_ce617998_3:


    "..."


translate spanish epilogue_us_b60d6130:



    nvl clear
    "En verano me matriculé de nuevo en la universidad y los estudios empezaron en otoño."


translate spanish epilogue_us_f8eaaeec:


    "El tiempo pasó. Asistí con voracidad a las clases y seminarios, y estudié para los test y exámenes con tal entusiasmo que jamás hubiera esperado de mí mismo."


translate spanish epilogue_us_792db99f:


    "Me las arreglé para formar parte del grupo con sorprendente facilidad. Aunque fuera más mayor que otros estudiantes, eso no supuso problema alguno."


translate spanish epilogue_us_cae314c1:


    "Tal vez debido a mi naturaleza inmadura o a los cambios de mi personalidad, ciertamente no lo sé. Muy probablemente la respuesta se encuentra en un término medio."


translate spanish epilogue_us_6135d2ed:



    nvl clear
    "El placer de comunicarme con otras personas, perdido hacía muchos años atrás, retornó."


translate spanish epilogue_us_aafbc9f6:


    "Fue fácil juntarme con los demás, sus problemas no me resultaban tan distantes y triviales. La vida normal, la cual antes había considerado simplemente gris y aburrida, empezó a brillar con nuevos colores."


translate spanish epilogue_us_71461a28:


    "A veces me pareció que me había vuelto un muñeco de trapo, que me convertí en uno de los millones de idénticos soldaditos de plomo que estaban en pie alineados en filas en una estantería de juguetes."


translate spanish epilogue_us_e2123de9:


    "Pero en esta tienda, aparte de la brillante vitrina que llamaba la atención con sus adornadas letras describiendo nuevas ofertas de temporada y descuentos de navidad, era un almacén donde todos los productos defectuosos eran tirados: un osito de peluche sin una pata, un camión de bomberos que requería de un mecánico, un transformer que se parecía más a un microondas que a un poderoso robot, rompecabezas que al unirse hacían pinturas post-modernistas flipantes..."


translate spanish epilogue_us_bfbc90e1:



    nvl clear
    "Mi lugar se hallaba entre estos juguetes rotos."


translate spanish epilogue_us_b863937a:


    "Y mientras algunas personas podrían refugiarse entre los fondos de caridad y los orfanatos, mi único destino era el vertedero y el reciclaje."


translate spanish epilogue_us_1be16bf7:


    "¡Así que no pude evitar disfrutar tales cambios en mi vida!"


translate spanish epilogue_us_ce617998_4:


    "..."


translate spanish epilogue_us_be1991b7:


    "Era la última clase de la asignatura más difícil del cuatrimestre."


translate spanish epilogue_us_ead34c7c:


    "A muchos estudiantes no les gustaba."


translate spanish epilogue_us_18d7148a:


    "Debido a su dificultad y a su profesor complejo de entender, supongo."


translate spanish epilogue_us_31f9553d:


    "Pero encontré cierto placer en lidiar con tablas, gráficos y diagramas."


translate spanish epilogue_us_65f71ceb:


    "Al contar las figuras por piezas, las reordenaba en columnas y líneas en el correcto orden, sumando, sustrayendo, dividiendo y multiplicando, y empleando éstas para obtener una imagen precisa de cualquier hecho."


translate spanish epilogue_us_9f752038:


    "No había número que se resistiera a mi atenta mirada. Todos ellos serían capturados, calculados, analizados. Cada uno de ellos tendría un número de índice y un lugar en su apropiada celda."


translate spanish epilogue_us_f15dbc2c:


    "Tras su llegada, cada dígito sería asignado a una litera, un uniforme de trabajo y vestimentas de descanso, y sería enviado a realizar su tarea asignada."


translate spanish epilogue_us_620a5afb:


    "Algunos cavarían una zanja de nueve a cinco, otros marcharían para enfrentarse contra las tendencias, y otros tratarían de combatir la línea de regresión en el rango de tiro."


translate spanish epilogue_us_bb1b122e:


    odn "¿En qué estás pensando?"


translate spanish epilogue_us_ec822f80:


    "A regañadientes, dejé de escribir y miré a mi compañero."


translate spanish epilogue_us_51f07f25:


    me "Tomando apuntes de la clase, como puedes ver."


translate spanish epilogue_us_6f6e3c30:


    odn "¡Anda ya, déjalo!{w} ¡Leéte sólo el manual de ejercicios más tarde!"


translate spanish epilogue_us_a2c7fb06:


    me "También me leeré el manual de ejercicios."


translate spanish epilogue_us_d93f9cb9:


    odn "Siempre estoy asombrado contigo."


translate spanish epilogue_us_baaee44b:


    me "¿Qué te parece tan asombroso?"


translate spanish epilogue_us_41e93016:


    odn "Tienes intenciones de conseguir matrícula de honor, ¿no?"


translate spanish epilogue_us_21b742e8:


    me "Nunca pensé en ello..."


translate spanish epilogue_us_096e8101:


    odn "¡No me digas que realmente lo encuentras interesante!"


translate spanish epilogue_us_ef73bda8:


    "Recordé que hace tan sólo un momento antes me imaginaba a mí mismo engalonado y cargando con un estandarte que ondeaba junto a un regimiento de dígitos, así que no pude evitar sonreír."


translate spanish epilogue_us_56f957c4:


    odn "¡Tampoco necesitas todas estas cosas!"


translate spanish epilogue_us_93d43576:


    me "Todo será necesario.{w} Por lo menos para un desarrollo general."


translate spanish epilogue_us_a5b0bb2a:


    "El compañero de clase sonrió sarcásticamente."


translate spanish epilogue_us_38f2115a:


    me "Apuesto a que ni te has leído un solo libro en toda tu vida."


translate spanish epilogue_us_d6011e93:


    odn "¿Y qué?"


translate spanish epilogue_us_f13d64cb:


    "Me inquirió en tono desafiante."


translate spanish epilogue_us_fcfd9bb3:


    me "Simplemente afirmo un hecho."


translate spanish epilogue_us_c7273c00:


    odn "Y tú siempres estás en las nubes."


translate spanish epilogue_us_69ed5808:


    "Estaba en lo cierto con eso. Aunque recuperé parte de mis habilidades sociales, a menudo pierdo el contacto con la realidad, pensando en las musarañas."


translate spanish epilogue_us_33265fad:


    me "¡Lo dices como si fuera una cosa mala!"


translate spanish epilogue_us_39ec8d69:


    odn "¡Y tampoco hay nada de malo en no leer libros!"


translate spanish epilogue_us_6ba8578f:


    me "Los seres humanos no pueden tener solamente necesidades materiales."


translate spanish epilogue_us_ef353f7b:


    "Dije filosóficamente, en tono socarrón."


translate spanish epilogue_us_da783828:


    odn "¡Anda ya, eres tan cansino como un bostezo!"


translate spanish epilogue_us_ce617998_5:


    "..."


translate spanish epilogue_us_5c98c047:


    "La clase estaba llegando a su fin, y empecé a pensar en qué hacer el resto del día."


translate spanish epilogue_us_26180779:


    "Necesitaba ir a comprar algo de comida, después finalizar un proyecto y enviárselo al cliente. Y luego escribiría unas notas..."


translate spanish epilogue_us_44324923:


    "Y luego por la tarde puede que simplemente lea o vea algo."


translate spanish epilogue_us_75613726:


    "A menos que alguien me llamara o me escribiera. No hay ningún asunto urgente pendiente, así que puedo pasar algo de tiempo libre con los compañeros."


translate spanish epilogue_us_21b43907:


    odn "Oh, cinco minutos enteros para irnos..."


translate spanish epilogue_us_5620292c:


    "Miré mi teléfono móvil y me di cuenta de que hacía exactamente un año tras mi vuelta del campamento de pioneros Sovyonok."


translate spanish epilogue_us_c2222e39:


    "Mi alma se sentía cálida de tales pensamientos, y sonreí dichosamente."


translate spanish epilogue_us_f6b9bef5:


    "Ya no recuerdo tan a menudo aquellos hechos."


translate spanish epilogue_us_4510141f:


    "Obviamente uno no puede olvidar algo así, aquellos momentos tan extraños y extraordinarios que se grabaron en mi memoria para siempre."


translate spanish epilogue_us_da6f1a84:


    "Normalmente incluso los momentos más felices se desvanecen y sólo permanecen reminiscencias de ellos, nada más. Pero la semana que pasé en Sovyonok fue diferente."


translate spanish epilogue_us_c7712fd2:


    "Recordé todo con perfecto detalle: el terror de los primeros minutos tras despertarme en el autobús. El primer día... duro y lleno de sorpresas y de asombrosos conocidos. Alegres y despreocupadas bromas con Ulyana, por ejemplo aquella travesura de los fantasmas."


translate spanish epilogue_us_3b043f92:


    "¡Los mejores comediantes soviéticos la envidiarían!"


translate spanish epilogue_us_1133bb6a:


    "¡Y los pioneros pusieron unas caras!{w} ¡Se morían de risa!"


translate spanish epilogue_us_88f6794d:


    "Seguro que sería bonito encontrarse con Ulyana en la vida real."


translate spanish epilogue_us_5fdf8a75:


    "Bueno, ella no siempre es perfecta, es hiperactiva y no tiene ni idea de comportarse con buenas maneras, pero..."


translate spanish epilogue_us_ad5fa5b0:


    "Uno no se encuentra a menudo con una persona tan sincera, alegremente despreocupada y con tantas energías."


translate spanish epilogue_us_957cd999:


    "Tal vez ella me prestó parte de esa energía a mí..."


translate spanish epilogue_us_732f5a0a:


    "El hecho de que tuviera que quedarse atrás, lo hace incluso más triste."


translate spanish epilogue_us_66623e13:


    "Por supuesto que solo es un sueño, un producto de mi imaginación."


translate spanish epilogue_us_59330073:


    "Aunque aun así te puedes preocupar por un personaje de libro o de cine.{w} Uno se ríe y llora con ellos, y siente también su dolor."


translate spanish epilogue_us_41ee4507:


    "La misma cosa para mí... pareciera que si hubiera actuado de manera distinta, esta histora habría tenido incluso un alegre final feliz."


translate spanish epilogue_us_e8ee1985:


    "Sin embargo, estoy seguro de que Ulyana está feliz en ese mundo."


translate spanish epilogue_us_10bf53a2:


    "Sencillamente no puedo imaginármela enojada por más de cinco minutos. Su optimismo le ayudará a encontrar el camino, incluso en las más complicadas situaciones."


translate spanish epilogue_us_5432fef5:


    "Me pregunto como se verá cuando crezca..."


translate spanish epilogue_us_ce617998_6:


    "..."


translate spanish epilogue_us_78903e24:


    "La campana sonó. Mi compañero se puso en pie y me miró intensamente."


translate spanish epilogue_us_320bd80d:


    odn "Vale, ya nos veremos... ¿Me puedes pasar tus apuntes más tarde?"


translate spanish epilogue_us_22448110:


    me "Si hubieras estado tomando notas tu mismo durante la clase, no necesitarías ninguno."


translate spanish epilogue_us_0c8f943d:


    odn "¿Entonces puedo?"


translate spanish epilogue_us_9f5895ba:


    me "Vale, está bien."


translate spanish epilogue_us_31844f25:


    odn "¡Gracias!"


translate spanish epilogue_us_3e06fe65:


    "Sonrió y se fue por la entrada del aula."


translate spanish epilogue_us_0912f764:


    "Lentamente empaqué todos los manuales de ejercicios y libros de texto dentro de la mochila y miré hacia la entrada del aula."


translate spanish epilogue_us_c11e20a5:


    "Por un instante pude oler el conocimiento en el aire, como la sal del mar en el aire en la cubierta de un barco."


translate spanish epilogue_us_d5ee5f01:


    "Miles y miles de estudiantes vienen aquí a estudiar, para aprender algo nuevo o para olvidar algo que solían saber."


translate spanish epilogue_us_4f2c8f9f:


    "Algunos se duermen durante las clases, otros toman apuntes atentamente, como yo, pero nadie era completamente indiferente."


translate spanish epilogue_us_36afd483:


    "Los que no se preocupaban se quedaban en casa, corrían las cortinas de su ventana y fijaban su mirada en una pantalla."


translate spanish epilogue_us_a3f5dd2b:


    "Suspiré a regañadientes y me fui por la puerta del aula con la firme intención de no malgastar el día."


translate spanish epilogue_us_303f415b:


    "La campana sonó. Mi compañero se puso en pie, se despidió y se fue."


translate spanish epilogue_us_7de17a9f:


    "Empecé a empaquetar mis manuales de ejercicios y libros de texto dentro de la mochila, cuando repentinamente escuché la voz de alguien frente a mí."


translate spanish epilogue_us_ec6b06a5:


    usg "Discúlpame, ¿es esta el aula número 34 de clases?"


translate spanish epilogue_us_446ad0da:


    me "Hay una señal en la puerta."


translate spanish epilogue_us_e87f24be:


    usg "¡No la hay!"


translate spanish epilogue_us_a7b9d37d:


    "La voz de la muchacha sonó ofendida."


translate spanish epilogue_us_974f3678:


    me "Alguien debió de quitarla."


translate spanish epilogue_us_faab6aa2:


    usg "Entonces, ¿es la número 34?"


translate spanish epilogue_us_fc30fd2b:


    me "Claro."


translate spanish epilogue_us_18a6d4fc:


    "Al fin cerré la mochila y alcé mi cabeza, tratando de ponerme en pie."


translate spanish epilogue_us_053ad9b8:


    "La muchacha resultaba terriblemente familiar."


translate spanish epilogue_us_04dca07f:


    me "¿Nos conocemos...?"


translate spanish epilogue_us_41fdb5dd:


    usg "Sí, pensé lo mismo."


translate spanish epilogue_us_f2f12dc1:


    "Me miraba bastante sorprendida."


translate spanish epilogue_us_94093acc:


    me "Pero no recuerdo dónde puedo haberte conocido."


translate spanish epilogue_us_6c0a1771:


    usg "Yo tampoco..."


translate spanish epilogue_us_8cc9c861:


    me "¿De qué año eres?"


translate spanish epilogue_us_7aad28e6:


    usg "De primero."


translate spanish epilogue_us_b8aea2a0:


    me "Oh, estás muy verde pues..."


translate spanish epilogue_us_fb117d69:


    "La muchacha sonrió."


translate spanish epilogue_us_080a02d2:


    usg "¡Pero aun tengo mucho por delante!"


translate spanish epilogue_us_c49f13e1:


    me "Como si ya lo hubiera acabado todo..."


translate spanish epilogue_us_48cc1cb9:


    usg "¿Y qué hay de ti?"


translate spanish epilogue_us_dd578b4d:


    me "De cuarto."


translate spanish epilogue_us_cfefc74e:


    usg "Y bien, ¿es difícil de estudiar?"


translate spanish epilogue_us_8f48e6be:


    me "Durante el primer año lo es."


translate spanish epilogue_us_05e57566:


    usg "¡Lo sabía!"


translate spanish epilogue_us_1e8379d1:


    "Dijo en un modo molesta."


translate spanish epilogue_us_04f0c0cd:


    me "Pero se vuelve fácil con el tiempo...{w} Es siempre más fácil en cuanto te acostumbras."


translate spanish epilogue_us_638e6427:


    usg "En ocasiones soy muy perezosa para hacer las cosas..."


translate spanish epilogue_us_3685e98e:


    me "Claro, eso a veces pasa."


translate spanish epilogue_us_da4ba352:


    usg "¿Aun tienes algo del primer año?{w} Si me lo dejas, me será mucho más fácil para prepararme los exámenes."


translate spanish epilogue_us_123037fe:


    "Obviamente, no tenía nada, mi primer año lo terminé hace mucho tiempo."


translate spanish epilogue_us_934f55fc:


    "Pero de repente sentí un fuerte deseo de pedirle su número de teléfono móvil, así que mentí."


translate spanish epilogue_us_14b5f47a:


    me "Sí, quizás...{w} Pero tendría que comprobarlo."


translate spanish epilogue_us_463c9a51:


    usg "¡Genial!"


translate spanish epilogue_us_5274a2e2:


    "Nos intercambiamos nuestros números."


translate spanish epilogue_us_96f1754d:


    me "Mi nombre es Semyon.{w} ¿Cómo debería llamarte?"


translate spanish epilogue_us_f46d0d3c:


    usg "Ulyana."


translate spanish epilogue_us_fcd06166:


    "¡Y entonces me percaté a quién me recordaba esta muchacha!"


translate spanish epilogue_us_8a86e6b2:


    "Exacto, ella es Ulyana, ¡sólo que cinco años más mayor!"


translate spanish epilogue_us_919cb1ad:


    "Por un instante me quedé sin palabras."


translate spanish epilogue_us_9f328817:


    us "¿Qué? ¡Es un nombre normal y corriente!{w} ¡Era el nombre de Lenin!"


translate spanish epilogue_us_89c17322:


    "Dijo haciendo un puchero."


translate spanish epilogue_us_f5b985ac:


    me "No... Sólo...{w} ¿Has estado alguna vez en un campamento de pioneros?"


translate spanish epilogue_us_4af695ee:


    us "Cuando era una cría...{w} ¿Por qué me lo preguntas?"


translate spanish epilogue_us_cf8568cf:


    "En un abrir y cerrar de ojos, todos los sucesos que ocurrieron en Sovyonok destellearon frente a mis ojos."


translate spanish epilogue_us_63854d93:


    "Entonces, ¡no fue un sueño!"


translate spanish epilogue_us_cba25f57:


    us "Aunque era solamente un campamento de verano, no de «pioneros»..."


translate spanish epilogue_us_903a86f3:


    "Añadió Ulyana."


translate spanish epilogue_us_9445daad:


    me "¿Entonces no vestías un uniforme?"


translate spanish epilogue_us_c3110504:


    us "¿Aquel con el pañuelo rojo en el cuello?{w} ¡Por supuesto que no! ¡Qué cantoso!"


translate spanish epilogue_us_56d4e39c:


    "Ella se rio a carcajadas."


translate spanish epilogue_us_3649081a:


    us "Pero, ¿por qué lo preguntas?"


translate spanish epilogue_us_8f44567c:


    "Estaba a punto de explicarle todo, pero entonces me di cuenta de que ella sólo me consideraría un psicópata."


translate spanish epilogue_us_6b2b31a6:


    "Después de todo, esta muchacha era mucho más mayor que la Ulyana que recordaba."


translate spanish epilogue_us_ae985c6d:


    "Tal vez sólo se le parece..."


translate spanish epilogue_us_3cd4f58f:


    me "Tuve un sueño...{w} Y creo que te vi a ti.{w} Una especie de déjà vu."


translate spanish epilogue_us_d1a9fc56:


    us "Deja... ¿qué?"


translate spanish epilogue_us_ec20dd52:


    me "Es cuando uno ve algo por primera vez, pero cree que lo ha visto antes."


translate spanish epilogue_us_abc2ab7a:


    us "Bueno, ahora que lo mencionas..."


translate spanish epilogue_us_ce43a5b5:


    "Su rostro se volvió serio."


translate spanish epilogue_us_78382c80:


    us "Creo que te he visto en alguna parte también.{w} ¡Definitivamente!{w} Quizás sólo sea eso que dijiste..."


translate spanish epilogue_us_e8c05531:


    me "Una interesante coincidencia, ¿no te parece?"


translate spanish epilogue_us_2170a31a:


    us "No lo sé...{w} ¡Es igual! ¡Te llamaré! ¡Hasta la vista!"


translate spanish epilogue_us_5de584b6:


    "Se rio y se fue corriendo de la sala."


translate spanish epilogue_us_0dcbddab:


    "La apariencia, el comportamiento, la forma de hablar... ¡Está claro que es exactamente Ulyana!"


translate spanish epilogue_us_894a40f8:


    "Entonces, ¿significa eso que el sueño se vuelve realidad?"


translate spanish epilogue_us_23b134c2:


    "¡No puedo sino disfrutarlo!"


translate spanish epilogue_us_9e9cd5d5:


    "Y, por encima de todo, ¡veré seguramente en otra ocasión a esta siempre alegre y jovial muchacha!"


translate spanish epilogue_dv_9bc20c27:


    "Parece que me estoy volviendo viejo..."


translate spanish epilogue_dv_2be57040:


    "El día se terminó y el agotamiento debería haberse ido hace rato ya, pero todavía me sentía mal y mareado."


translate spanish epilogue_dv_6be0ab43:


    "A pesar de cuán duro lo intenté, era incapaz de abrir mis ojos. Me di la vuelta."


translate spanish epilogue_dv_99485000:


    "Me pregunto que me espera en el distrito central...{w} ¿Cómo voy a vivir?{w} Quizás no sea tan malo..."


translate spanish epilogue_dv_df8947d0:


    "Una cosa segura es, y es que no sé por dónde empezar, pero por otra parte este mundo no parece hostil. Sé muchas cosas de aquí, todo el mundo habla ruso..."


translate spanish epilogue_dv_cdc9d191:


    "Pues entonces, ¿tal vez no va a ocurrir nada malo...?"


translate spanish epilogue_dv_bf9b8fdd:


    "Repentinamente me sentí muy enfermo."


translate spanish epilogue_dv_910fe671:


    "¡Maldita sea, creí que ya lo había echado todo ayer por la tarde!"


translate spanish epilogue_dv_098084b9:


    "Me las apañé para llegar hasta el aseo, moviéndome instintivamente."


translate spanish epilogue_dv_80b1d831:


    "Tras un rato, cuando mi sucio negocio se acabó, retorné a mi habitación."


translate spanish epilogue_dv_7d131181:


    "Y entonces me di cuenta..."


translate spanish epilogue_dv_7b407504:


    "¡He vuelto a mi viejo piso!"


translate spanish epilogue_dv_04cada73:


    "Sin embargo, no es tan viejo..."


translate spanish epilogue_dv_bdfdf404:


    "Parece que nada cambió durante la semana de mi ausencia."


translate spanish epilogue_dv_e99eda04:


    "Las capas de polvo no crecieron más."


translate spanish epilogue_dv_49074c47:


    "La posición del teclado, la inclinación de la pantalla y la mancha de una taza de té en la mesa sorprendió a mis ojos."


translate spanish epilogue_dv_47eb912e:


    "Teóricamente, ¡un hombre en mis condiciones no debería percatarse de semejantes detalles!"


translate spanish epilogue_dv_55584c51:


    "Pero toda la habitación atrajo mi atención hasta el más mínimo detalle, como si parpadearan con diferentes colores."


translate spanish epilogue_dv_46f98924:


    "Los calcetines estaban tirados exactamente de la misma manera en que habían estado antes. La manta estaba puesta de manera arrugada en la cama sin hacer tal como estaba antes, la sucia ventana mostraba el mismo panorama de la calle como un cinescopio estropeado."


translate spanish epilogue_dv_76d36664:


    "Itenté en vano estar aterrorizado o por lo menos asustado."


translate spanish epilogue_dv_185107f9:


    "Quizá sea el efecto de un trastorno, trastorno de estrés postraumático o algo así..."


translate spanish epilogue_dv_dc9713bf:


    "Después de todo, hace varias horas antes tampoco creía que retornaría al mundo real."


translate spanish epilogue_dv_ce3c1db8:


    "El campamento de pioneros, las muchachas, Alisa, el mítico distrito central al que habría llegado de no ser porque me había despertado en mi piso. Todo aquello me empezó a parecer real."


translate spanish epilogue_dv_3b4fb89e:


    "¿Quizá mis fantásticas vacaciones a Sovyonok fueron solamente un sueño?"


translate spanish epilogue_dv_bb1b5b1b:


    "Mis pies comenzaron a acumular círculos alrededor de la habitación.{w} Sostuve mis manos detrás de mí y empecé a pensar sobre ello."


translate spanish epilogue_dv_05941cbe:


    "Solamente una cosa está clara, he retornado tan repentinamente como llegué {i}allí{/i}."


translate spanish epilogue_dv_b0e545e2:


    "Sin aparente motivo o razón."


translate spanish epilogue_dv_c2f030da:


    "Tal vez haya algún tipo de explicación, pero está más allá de mí (o de humana) comprensión."


translate spanish epilogue_dv_54e489ca:


    "Tanto si fue un engaño por fuerzas interestelares, o un ente supremo, como experimentos del gobierno (creí menos probable la última), no dejaron prueba alguna."


translate spanish epilogue_dv_94c0ae0c:


    "Si era incapaz de hallar respuestas en el campamento, sería una estupidez esperar que las cosas se aclararan {i}aquí{/i}."


translate spanish epilogue_dv_f55be16f:


    "Es como buscar evidencias o pruebas en una casa que ha sido quemada hasta las cenizas."


translate spanish epilogue_dv_077e08fa:


    "Incluso si un crimen fue cometido, todas las pruebas fueron destruidas por el fuego."


translate spanish epilogue_dv_be44e243:


    "Tal vez si tuviera las habilidades y el equipo de un criminólogo, podría hallar algo, pero..."


translate spanish epilogue_dv_dea456b1:


    "Las memorias de la semana que pasé en el campamento son todo lo que tengo."


translate spanish epilogue_dv_9362d886:


    "Pero no fue un sueño. ¡Lo sé con seguridad!"


translate spanish epilogue_dv_5bfae656:


    "Mi agotamiento era completamente real. Mis sentimientos y emociones fueron genuinas, no solamente producto de un sueño."


translate spanish epilogue_dv_a78cb617:


    "Por lo tanto, estuve allí, me pasé una semana en el campamento de pioneros Sovyonok con la líder del campamento y los pioneros...{w} con Alisa."


translate spanish epilogue_dv_a2226b0b:


    "Sus recuerdos me hicieron suspirar."


translate spanish epilogue_dv_42975286:


    "Habría sido genial irse con ella."


translate spanish epilogue_dv_78c33026:


    "¡Espera un minuto!{w} ¡¿Qué pasa conmigo?!{w} Debería estar alegre de retornar a casa, ¡al mundo real!"


translate spanish epilogue_dv_db34fc11:


    "¿O no debería...?"


translate spanish epilogue_dv_0b94b031:


    "Por ahora simplemente no podía evaluar la situación de manera razonable."


translate spanish epilogue_dv_2f3f1dc7:


    "No sabía si quería quedarme allí o si quería disfrutar de mi retorno largamente esperado."


translate spanish epilogue_dv_d091ab38:


    "Me percaté de una moneda de cinco rublos en la mesa. La cogí y empecé a lanzarla sin propósito alguno, contando el número de caras y cruces."


translate spanish epilogue_dv_ba6c1813:


    "Hace mucho tiempo, quizá en la época del instituto, escuché que en un infinito número de lanzamientos las probabilidades no son del 50 por ciento para caras y cruces."


translate spanish epilogue_dv_b79ab5f5:


    "Tal vez porque una moneda no tiene una forma y peso perfectos o por otra razón."


translate spanish epilogue_dv_0906061c:


    "Sin embargo, tras un rato conté con 70 caras y 71 cruces."


translate spanish epilogue_dv_a3037b78:


    "Así que la teoría de la relativadad se da...{w} Pero no soy quien para decirlo, no sé mucho sobre ello."


translate spanish epilogue_dv_6766bb1d:


    "Recordé mi infancia, cuando había escuchado la palabra «cruces» por primera vez."


translate spanish epilogue_dv_b5103a20:


    "Por alguna razón, siempre me hacía pensar en el «aparejo» de un barco."


translate spanish epilogue_dv_0dc3131c:


    "Tal vez haya algo de lógico en esto... un aparejo de barco se constituye de mástiles y vergas en perpendicular, haciendo cruces."


translate spanish epilogue_dv_31eac3c5:


    "Ahora esta teoría me resultaba extraña, pero a la vez tampoco podía encontrar cualquier conclusión evidente para rebatirla."


translate spanish epilogue_dv_ba47df6c:


    "Pues bien, ¿significa eso que los aparejos salen más frecuentemente que las caras?"


translate spanish epilogue_dv_736afd2f:


    "Pero uno debe considerar así mismo que un aparejo también tiene caras donde le sopla el viento, así que..."


translate spanish epilogue_dv_80525988:


    "Me estiré de mi cabello y suspiré cansado."


translate spanish epilogue_dv_73fcc8eb:


    "¡Ahora no es el momento de pensar en tales tonterías!"


translate spanish epilogue_dv_51221043:


    "Por otra parte, ¿cuál es el momento idóneo?{w} ¿Qué puedo hacer? ¿Qué está bajo mi control?"


translate spanish epilogue_dv_4c4bbb47:


    "Si no me molesté en tratar de hallar respuestas durante una semana allí, ¿qué sentido tendría pensarlo ahora?"


translate spanish epilogue_dv_bc27a2ca:


    "Quizá sólo debería intentar volver a mi vida normal y corriente..."


translate spanish epilogue_dv_6ce763b8:


    "Las palabras «normal» y «corriente» eran difíciles para mí."


translate spanish epilogue_dv_3ceed523:


    "¿Por qué escogí tales específicas palabras?{w} ¡Podría haber dicho «pasada», «anterior» u otra cosa!"


translate spanish epilogue_dv_4e6dae94:


    "Supongo que porque era a lo que estaba acostumbrado... ésa es la más obvia respuesta."


translate spanish epilogue_dv_1e1bd7d8:


    "Como suelen decir, cuando te acostumbras a algo se vuelve una reacción instintiva."


translate spanish epilogue_dv_87ae0d50:


    "Al instante, todo los sucesos de la última semana flotaron frente a mi ojos."


translate spanish epilogue_dv_1614d0af:


    "¿En serio que no me quería ir de allí?"


translate spanish epilogue_dv_b3b1bf62:


    "¿Fue sólo una semana suficiente para acostumbrarme a los recados de Olga Dmitrievna todo el rato, las bromas y burlas de Ulyana por ahí, o el descaro y las tomaduras de pelo de Alisa?"


translate spanish epilogue_dv_612f35d1:


    "No tenía respuesta para esta cuestión, pero es obvio que ya las echaba en falta."


translate spanish epilogue_dv_07307489:


    "Aunque estoy seguro de que ya nunca retornaré allí."


translate spanish epilogue_dv_da4fdd9d:


    "Obviamente, no sabía si la fuerza que me llevó allí querría divertirse otra vez conmigo.{w} Y una y otra vez..."


translate spanish epilogue_dv_06277978:


    "Es posible."


translate spanish epilogue_dv_eccd0d95:


    "Pero por ahora tengo lo que tengo... una aventura que recordaré para el resto de mi vida."


translate spanish epilogue_dv_b8699e2f:


    "Maldita sea, ¿cuánta gente consigue experimentar algo como eso?{w} ¡Incluso libros o sueños sobre tales cosas son inimaginables!"


translate spanish epilogue_dv_e7a9c5b6:


    "La cosa más importante es que no era la malvada isla del Doctor Moreau, sino un campamento de pioneros soviético con pioneros normales."


translate spanish epilogue_dv_41392bfa:


    "Y su ordinariedad fue sobrepasada por la asombrosa materialización de mi llegada allí."


translate spanish epilogue_dv_4f464055:


    "De todas formas, no fue la peor experiencia de mi vida.{w} ¡Quizás fue una de las mejores!"


translate spanish epilogue_dv_1a6feebe:


    "Obviamente, es triste que todo haya terminado a medias, pero toda historia tiene un final."


translate spanish epilogue_dv_aa9a36c1:


    "Está bien que haya terminado de esta manera.{w} Quién sabe qué podría haber ocurrido..."


translate spanish epilogue_dv_ce617998:


    "..."


translate spanish epilogue_dv_4bbd270b:



    nvl clear
    "El tiempo pasó. Los hechos que tuvieron lugar en el campamento, ahora parecían más bien una novela fascinante que una vez leí."


translate spanish epilogue_dv_5faded9b:


    "No, no los he olvidado del todo. Podría recordarlos hasta el más mínimo detalle."


translate spanish epilogue_dv_2a6f7dd3:


    "Recuerdo como de niño solía estar bajo una manta con una linterna, leyendo un fascinante libro sobre naves espaciales hasta que amaneciera."


translate spanish epilogue_dv_ba6313e4:


    "A veces, era incapaz de levantarme para ir al instituto. Mis padres siempre solían regañarme por eso, pero con cada noche que se acercaba, cuidadosamente pasaba cada página tratando de absorber cada pequeño detalle de la historia."


translate spanish epilogue_dv_b9465d76:



    nvl clear
    "Tuve una sensación similar con el campamento de pioneros. La realidad (si pudiera considerarla como tal) me parecía ahora más como un libro."


translate spanish epilogue_dv_84be21a5:


    "No creo que fuera malo en absoluto. Al final, lo importante es recordar, y no es relevante tanto si esas memorias son recuerdos de una presentación de diapositivas, como si son de fotografías, de vídeo o audio."


translate spanish epilogue_dv_a20cefa7:


    "..."


translate spanish epilogue_dv_0775f137:



    nvl clear
    "Ahora cada autobús me hace sentir tanto miedo como esperanza."


translate spanish epilogue_dv_41f919cf:


    "Al principio di alguna vuelta sin propósito alguno por la línea 410. En diversas ocasiones, me rendí dormido en el mismo asiento."


translate spanish epilogue_dv_c066fcc3:


    "Pero no sucedió nada. Ni un ápice de algo sobrenatural."


translate spanish epilogue_dv_a3314542:


    "A medida que el tiempo transcurría, dejé de tomar esos inútiles viajes."


translate spanish epilogue_dv_ce617998_1:


    "..."


translate spanish epilogue_dv_32ab7558:



    nvl clear
    "Alguna cosa cambió en mi vida, ¡no podía ser de otra manera!"


translate spanish epilogue_dv_d3186f16:


    "Un par de semanas después de mi retorno del campamento, agarré mi guitarra la cual estaba en un rincón cogiendo polvo en la soledad."


translate spanish epilogue_dv_77eccfd0:


    "Al tocar un par de acordes, me acordé de Alisa... para mí su imagen era inseparable de este instrumento."


translate spanish epilogue_dv_56c6309a:


    "Al principio no se me daba bien... tal vez simplemente no tuviera talento para la música. Comencé a leer de forma autodidacta libros y a ver vídeos de lecciones."


translate spanish epilogue_dv_f6f2a4ed:


    "Por otra parte, todo el esfuerzo ya no suponía una carga para mí. Sencillamente tocaba los acordes y pulía mis habilidades."


translate spanish epilogue_dv_30a27c7e:


    "Y como con cualquier otro tipo de actividad, incluyendo la música, la práctica hace la perfección."


translate spanish epilogue_dv_56ead4c3:


    "Aproximadamente medio año después, me di cuenta de que logré algunos resultados. Por lo menos ahora podía tocar con soltura partes de mis canciones favoritas."


translate spanish epilogue_dv_07a11c4d:



    nvl clear
    "Satisfecho con mis progresos, puse un par de anuncios en foros de música, tratando de formar mi propia banda."


translate spanish epilogue_dv_47a62878:


    "Todavía no tenía ni idea de qué tipo de música quería tocar; mis gustos variaban de algo muy heavy a bandas sonoras de anime."


translate spanish epilogue_dv_ede9ef2c:


    "Al principio me pareció imposible."


translate spanish epilogue_dv_c41adf0a:


    "¡Tenía que comunicarme con extraños! ¿Y si somos muy diferentes? Y de todas fromas, ¿cómo le explico a los demás qué tipo de música quiero hacer?"


translate spanish epilogue_dv_2510a2ef:


    "Pero todo fue muy fácil."


translate spanish epilogue_dv_41b1dc0a:



    nvl clear
    "Los potenciales candidatos resultaron no ser aterradores, y nuestros gustos eran casi los mismos."


translate spanish epilogue_dv_367522a6:


    "Después de todo, compartíamos los mismos objetivos, así que los problemas que surgían a raíz de la falta de comprensión podían ser resueltos."


translate spanish epilogue_dv_03fe9757:


    "Y pronto tuvimos una banda."


translate spanish epilogue_dv_7c51a82e:


    "De repente descubrí mi talento como compositor."


translate spanish epilogue_dv_6e2dd77c:


    "Habiéndome pasado días y noches enteras con el editor de notas y los secuenciadores, empecé a componer cosas aceptables. Al menos en opinión del resto de miembros de la banda y de los compañeros a los que les mostré mis canciones."


translate spanish epilogue_dv_2e540bca:



    nvl clear
    "Muy pronto hicimos nuestro primer concierto."


translate spanish epilogue_dv_1ab31e80:


    "Un club de poca importancia con poco más espacio que mi dormitorio, tres asistentes y medio más que no estaba exactamente sobrio, estaba atrozmente ansioso y el sonido del equipo estaba constantemente funcionando mal. Pero todo eso no me molestó porque estaba feliz."


translate spanish epilogue_dv_dc28a178:


    "¡Puesto que éste fue sólo el primer paso para el éxito!"


translate spanish epilogue_dv_b6db561e:


    "Sin embargo, pasó mucho tiempo antes de que fuéramos invitados de nuevo."


translate spanish epilogue_dv_ac45d183:


    "Con gran dificultad logramos grabar tres pistas y publicarlas en internet. Las críticas estaban lejos de ser eufóricas, pero no me molestaba mucho. Era solamente el comienzo."


translate spanish epilogue_dv_b178ebc9:



    nvl clear
    "Tal vez algunos consideraban nuestra música simplemente una actividad de ocio o algo pasajero, pero yo me lo tomaba en serio."


translate spanish epilogue_dv_74103c40:


    "Después de todo, era yo quien fundó la banda y también toda las composiciones de música eran mías."


translate spanish epilogue_dv_78c27567:


    "Sentí una responsabilidad personal por el destino de mi obra. Cada momento libre que tenía, lo invertí en mejorar mi técnica para tocar y, tras un año, ya podía decir con confianza que no era el peor guitarrista del mundo."


translate spanish epilogue_dv_1a44fbb5:



    nvl clear
    "Y entonces recibimos algo de popularidad."


translate spanish epilogue_dv_425eb152:


    "Dábamos conciertos casi cada semana y finalmente fuimos retribuidos. No era importante que solamente fueran cinco rublos por persona, el hecho de que nuestro trabajo estuviera en demanda y alguien estuviera dispuesto a pagarnos por él (incluso a pesar de que normalmente éramos el calentamiento de otras bandas), era lo que en verdad importaba."


translate spanish epilogue_dv_ce617998_2:


    "..."


translate spanish epilogue_dv_6e06d90f:


    "Estaba sentado frente a la pantalla, torturando una guitarra."


translate spanish epilogue_dv_5d68b7b8:


    "La fecha del próximo concierto estaba enmarcada con un círculo en el calendario del escritorio de la computadora."


translate spanish epilogue_dv_b8653b2e:


    "Solo pensarlo se me ponía la piel de gallina."


translate spanish epilogue_dv_42802d30:


    "Era la primera vez que íbamos a ser las estrellas principales.{w} Y así mismo teníamos una presentación de un álbum."


translate spanish epilogue_dv_89bb111d:


    "Me preguntaba cuántos discos venderíamos...{w} Desde luego, la era del disco compacto ya se terminó, pero todavía hay quienes lo aprecian."


translate spanish epilogue_dv_78b39126:


    "Y quizás algunos lo comprarían solamente para mostrar su apoyo a nuestra banda."


translate spanish epilogue_dv_67aef320:


    "Miré la fecha una vez más.{w} Sí, hace exactamente un año que retorné de Sovyonok."


translate spanish epilogue_dv_299e685a:


    "Mi sonrisa se reflejaba en la oscura pantalla."


translate spanish epilogue_dv_ed82539c:


    "Me pregunto si a Alisa le habría agradado mi música de existir en este mundo.{w} ¡No lo creo!"


translate spanish epilogue_dv_1c7acd8a:


    "Ella prefería clásicos del rock soviético.{w} Pero quién sabe..."


translate spanish epilogue_dv_1e250ea6:


    "Toqué un par de complicadas partes con la guitarra y concluí, con un sentimiento de satisfacción, que el sonido de mi guitarra superaba de largo el de las guitarras «urálicas» soviéticas, y que mi habilidad era superior al de los guitarristas de mi misma época."


translate spanish epilogue_dv_663b0360:


    "Me pregunto cómo habría logrado todo esto sin aquellos fantásticos sucesos...{w} Improbablemente..."


translate spanish epilogue_dv_4e2974c6:


    "No tiene nada de raro. Mucha gente cambia su vida tras estar al borde del límite."


translate spanish epilogue_dv_82a1b7c9:


    "Evidentemente, nada peligroso sucedió entonces, ya que una conmoción psicológica es una buena sustituta para un grave peligro."


translate spanish epilogue_dv_3c4da4a0:


    "Ensayé una vez más mi parte, puse la guitarra en el maletín, empaqué todas mis cosas y me dirigí hacia la salida."


translate spanish epilogue_dv_199c3750:


    "Hacía frío fuera, justo como {i}aquel{/i} día..."


translate spanish epilogue_dv_e801f24b:


    "Lentamente me aproximé a la parada del autobús de la línea 410."


translate spanish epilogue_dv_55024429:


    "Qué coincidencia, estoy yendo al club justo con este mismo autobús."


translate spanish epilogue_dv_11dda01e:


    "Con el corazón encogido, conté los segundos."


translate spanish epilogue_dv_a0a306c4:


    "Pronto el autobús llegó. Descuidadamente abrió sus puertas y se tragó unos cuantos pasajeros."


translate spanish epilogue_dv_fbe14004:


    "Era una noche de invierno justo como {i}entonces{/i}.{w} Los coches iluminaban el exterior con sus faros, prendiendo la ciudad mientras se preparaba para dormir."


translate spanish epilogue_dv_688e4dad:


    "Cuanto más cerca estábamos del centro de la ciudad, mucha más gente se encontraba en las calles."


translate spanish epilogue_dv_ca09470b:


    "Algunos de ellos charlaban alegremente, otros estaban detenidos frente a magníficos mostradores de tiendas de ropa y otros tantos se daban prisa hacia alguna parte, sin prestar atención a nada."


translate spanish epilogue_dv_25cb0d1d:


    "Este día no tenía deseo alguno de retornar a aquel campamento.{w} En cualquier otro momento tal vez, ¡pero hoy no!"


translate spanish epilogue_dv_535c0d02:


    "Debo tocar en el concierto, y todo lo demás se puede ir al infierno."


translate spanish epilogue_dv_9b124b7d:


    "¡Tenía un objetivo claro y nada más importaba!"


translate spanish epilogue_dv_ce617998_3:


    "..."


translate spanish epilogue_dv_3ddfa4cf:


    "El club era considerablemente grande comparado con el primero en el que tocamos nuestro primer concierto, pero por ahora estaba vacío.{w} Empezamos la comprobación de sonido."


translate spanish epilogue_dv_633c3b22:


    "No podía escuchar la batería a través del monitor de altavoces, con lo cual traté desesperadamente de explicárselo al técnico de sonido."


translate spanish epilogue_dv_d28a96c6:


    "Sonido afinado, dispositivos instalados, lista de canciones preparada. ¿Preparados para rockanrolear?"


translate spanish epilogue_dv_ab38f79d:


    "Pero es un día especial. No hacemos el calentamiento. Otros tipos se ocupan del calentamiento antes de nuestro concierto.{w} De este modo, entramos al final."


translate spanish epilogue_dv_2dc1b4a6:


    "Mientras escuchaba la primera banda, me percaté de que tenían mucho que mejorar."


translate spanish epilogue_dv_e867ef2d:


    "Por otra parte, no éramos mucho mejores no hace mucho tiempo."


translate spanish epilogue_dv_3561335d:


    "A medida que el tiempo se acercaba a nuestra actuación, mi tensión crecía enormemente."


translate spanish epilogue_dv_b07e6f10:


    "¡Vamos, que estaba literalmente temblando de ansiedad!"


translate spanish epilogue_dv_4daf3bc3:


    "Decidí no escuchar a la otra banda y esperé tranquilamente en los vestidores."


translate spanish epilogue_dv_d6ae003b:


    "A juzgar por sus expresiones, mis compañeros estaban del mismo humor."


translate spanish epilogue_dv_00e13f2c:


    "Estuvimos en silencio.{w} En realidad, no teníamos nada que contarnos."


translate spanish epilogue_dv_8986e3df:


    "Nuestras partes habían sido aprendidas y perfeccionadas largo tiempo atrás."


translate spanish epilogue_dv_a7cff533:


    "Sólo los nervios nos podían poner en aprietos, pero ya teníamos algo de experiencia así que todo iría bien.{w} Por lo menos, me consolaba a mí mismo con esta idea."


translate spanish epilogue_dv_ce617998_4:


    "..."


translate spanish epilogue_dv_da0729f4:


    "¡Y ahora, al fin, la hora llegó!"


translate spanish epilogue_dv_40b909bb:


    "No recuerdo cómo entramos en el escenario y saludamos a la audiencia."


translate spanish epilogue_dv_a65dc811:


    "Mi percepción se concentró con el primer acorde de la primera canción que se escuchó por los altavoces."


translate spanish epilogue_dv_58bfcc73:


    "Mientras estaba sentado en los vestidores, traté desesperadamente de hacer entrar en calor mis helados dedos, pero ahora recorrían las cuerdas sin el más mínimo error, como si estuvieran encantados."


translate spanish epilogue_dv_15afa28b:


    "Una idea me vino a la mente... ¿por qué aun así no cometo ni un solo error?"


translate spanish epilogue_dv_76c871e1:


    "Cuando había tocado todas estas partes en casa o durante ensayos, nunca lo había pensado, lo había dado por sentado."


translate spanish epilogue_dv_b60a105d:


    "Pero ahora, cuando estás frente a una audiencia se siente diferente."


translate spanish epilogue_dv_a8ba8d2a:


    "En alguna profunda parte de mi mente estaba la idea de que en cualquier momento, pronto, mi dedo se resbalaría y...{w} ¡Gracias a Dios mis pensamientos no interferían mientras tocaba!"


translate spanish epilogue_dv_9819f44a:


    "Nuestra actuación estaba llegando a su fin, y ahora era el momento del difícil solo que estuve aprendiendo por tanto tiempo."


translate spanish epilogue_dv_e621a17f:


    "Llegados a ese punto, mis nervios se desvanecieron y lo toqué perfectamente, incluso mejor que durante los ensayos."


translate spanish epilogue_dv_ed12c375:


    "Finalmente nuestra actuación se terminó y el concierto rompió en un gran aplauso."


translate spanish epilogue_dv_552e80e1:


    "Sonreí incómodamente (al menos eso pensé) y me incliné en diversas ocasiones con el resto de la banda."


translate spanish epilogue_dv_4ff24831:


    "Nos gritaron «¡Otra!». Incluso sin que ellos gritaran estaría encantado de tocar otra, pero nos quedamos sin más canciones en tanto que sólo éramos una recién formada banda."


translate spanish epilogue_dv_ce617998_5:


    "..."


translate spanish epilogue_dv_485788f0:


    "La euforia reinó en los vestidores mientras nos felicitábamos y abrazábamos unos a otros. El sonido de las botellas de champán al abrirlas resonaban en las paredes."


translate spanish epilogue_dv_6ab4e9e3:


    "Todos nuestros discos habían sido vendidos, lo cual me hizo incluso más feliz."


translate spanish epilogue_dv_1e7ce85a:


    "Finalmente, ¡también lo conseguí!"


translate spanish epilogue_dv_a744e489:


    "Es imposible predecir el futuro de nuestra banda o de nuestra aventura con la música."


translate spanish epilogue_dv_7c912a9c:


    "Pero una cosa estaba clara... Al fin encontré algo que realmente me interesaba y a la que puedo dedicar mi vida entera."


translate spanish epilogue_dv_701ac863:


    "Así mismo, puedo hacer que otras personas distintas de mí sean felices también."


translate spanish epilogue_dv_32dffc6c:


    "¡Ahora era probablemente el hombre más feliz sobre la Tierra!{w} Al menos seguro que era el mejor momento de mi vida."


translate spanish epilogue_dv_ce617998_6:


    "..."


translate spanish epilogue_dv_abdb027b:


    "El club está vacío, los instrumentos son guardados y estoy esperando a los demás para irnos a casa."


translate spanish epilogue_dv_290d0099:


    "Echo un vistazo al escenario donde estuve hace justo una hora atrás."


translate spanish epilogue_dv_46ed0349:


    "Una vez más, júbilo, regocijo y emoción me inundaban."


translate spanish epilogue_dv_b9afffc3:


    "Esta no será la última vez que aparezco en ese escenario, eso seguro."


translate spanish epilogue_dv_d3215206:


    "Y quizás incluso escenarios más grandes como salas de conciertos y estadios olímpicos estén en mi futuro. ¿Quién sabe?{w} ¡Ahora todo es posible!"


translate spanish epilogue_dv_1e17703e:


    "Tras despedirme de mis compañeros, corrí hasta la parada del autobús y, afortunadamente, alcancé el autobús de la línea 410."


translate spanish epilogue_dv_748039c5:


    "No había nadie dentro. Tomé asiento detrás del conductor, justo detrás de él. Una melodía familiar se podía escuchar desde la radio."


translate spanish epilogue_dv_5ef8eb1d:


    "Tal vez algún día mis canciones también se escucharán en la radio."


translate spanish epilogue_dv_6570bb77:


    "En cierta manera, este viaje era similar a los hechos que tomaron lugar hace un año atrás."


translate spanish epilogue_dv_76a32943:


    "Quiero decir que en este momento todo fue como esperaba, lo logré gracias a un trabajo duro y el objetivo deseado se alcanzó."


translate spanish epilogue_dv_9a35ddf5:


    "Pero retorno como otro hombre.{w} Un hombre que ha cambiado en cierta forma."


translate spanish epilogue_dv_674125ad:


    dvg "¡Ey!"


translate spanish epilogue_dv_1b10feb5:


    "Me di la vuelta y vi una muchacha que estaba a mi lado."


translate spanish epilogue_dv_cf74f4a4:


    "Su tono de voz no era muy cordial, pero tampoco era hostil."


translate spanish epilogue_dv_c2b1de8c:


    dvg "¡Me gustó tu actuación!"


translate spanish epilogue_dv_73aab3cd:


    me "Gracias..."


translate spanish epilogue_dv_6b60c623:


    "La oscuridad del club me impedía ver con claridad su cara, pero estaba seguro de que me resultaba extremadamente familiar."


translate spanish epilogue_dv_bd0cee6c:


    me "Y tú..."


translate spanish epilogue_dv_1d56b639:


    "Di un paso adelante."


translate spanish epilogue_dv_531bb64d:


    "¡Exacto!{w} ¡Era Alisa!"


translate spanish epilogue_dv_e3137c54:


    "Me quedé de piedra, incapaz de hablar."


translate spanish epilogue_dv_6a3ab760:


    dvg "¿Qué pasa conmigo?"


translate spanish epilogue_dv_5b8f2192:


    me "Nada...{w} Es sólo que creo haberte visto en alguna parte."


translate spanish epilogue_dv_ba143499:


    "Decidí no abordarla con preguntas sobre el campamento de pioneros."


translate spanish epilogue_dv_6fa8d283:


    "Después de todo, es posible que esta muchacha meramente se parezca a ella.{w} O algo así..."


translate spanish epilogue_dv_6827cc68:


    "De todas formas, era obvio que ella me veía por primera vez.{w} O al menos lo aparentaba..."


translate spanish epilogue_dv_0f19819d:


    dvg "Bueno, puede ser verdad."


translate spanish epilogue_dv_ae3e36fe:


    "Ella me miró detenidamente."


translate spanish epilogue_dv_5a477656:


    me "Mi nombre es Semyon."


translate spanish epilogue_dv_0c70048e:


    "Forzando una sonrisa artificial, le ofrecí mi mano."


translate spanish epilogue_dv_9cd81f75:


    dvg "En realidad lo sé."


translate spanish epilogue_dv_c0d3e816:


    me "¿Cómo?"


translate spanish epilogue_dv_5c2b6b63:


    dvg "Lo leí en internet."


translate spanish epilogue_dv_021e0faf:


    "Sí, claro."


translate spanish epilogue_dv_339fce09:


    me "¿Y tú eres...?"


translate spanish epilogue_dv_be5fc071:


    dvg "Alisa..."


translate spanish epilogue_dv_1ebb64bd:


    "Respondió un tanto confusa."


translate spanish epilogue_dv_04cf27b7:


    "¡Entonces tenía razón!{w} Pero, ¿hasta qué punto estaba en lo cierto...?"


translate spanish epilogue_dv_2ec84ac1:


    me "Escucha, ¿fuiste a un campamento de pioneros de niña?"


translate spanish epilogue_dv_ed23c5e1:


    dv "N... no...{w} ¿Por qué lo preguntas?"


translate spanish epilogue_dv_e1ab806a:


    me "Solamente...{w} Sólo me pareció haberte conocido allí."


translate spanish epilogue_dv_98abf1e8:


    dv "¿Cómo podrías conocerme allí si no he estado nunca?"


translate spanish epilogue_dv_ec2c2a7c:


    "Sonrió."


translate spanish epilogue_dv_85d7f47f:


    dv "Definitivamente, ¡todos los músicos sois algo raros!"


translate spanish epilogue_dv_7bd57e71:


    me "No lo dudes."


translate spanish epilogue_dv_98885eac:


    dv "Bueno... Solamente quería decirte que tu música no está mal."


translate spanish epilogue_dv_0c7cecbc:


    "Alisa se dio la vuelta para irse."


translate spanish epilogue_dv_dadc6a74:


    "Sentí que tenía que hacer o decir algo."


translate spanish epilogue_dv_48603599:


    me "¿Vendrás a nuestro siguiente concierto?"


translate spanish epilogue_dv_8e1cb694:


    dv "¿Cuándo es?"


translate spanish epilogue_dv_37503a95:


    "Para ser sinceros no tenía ni idea."


translate spanish epilogue_dv_e56b5b6f:


    me "Pronto..."


translate spanish epilogue_dv_26dbb3bd:


    dv "Sí, quizás...{w} ¡Envíame un mensaje!"


translate spanish epilogue_dv_3b03af3a:


    me "Claro, por supuesto..."


translate spanish epilogue_dv_1cf7fbad:


    "Nos intercambiamos los detalles para contactar entre nosotros."


translate spanish epilogue_dv_b256ad93:


    dv "Pues ya nos veremos en otra ocasión, ¡probablemente!"


translate spanish epilogue_dv_f57169e1:


    "Se despidió con la mano y se fue."


translate spanish epilogue_dv_1e45c266:


    "Obviamente, no tenía ni idea de si era exactamente la misma Alisa que conocí en el campamento."


translate spanish epilogue_dv_9835dc5e:


    "Cualquier teoría era inválida por la absoluta ausencia de pruebas."


translate spanish epilogue_dv_c0c56290:


    "Pero en aquel momento era la última cosa que me preocupaba, en tanto que estaba lleno de emociones, tanto por el concierto en sí como por el encuentro con Alisa."


translate spanish epilogue_dv_b44b240b:


    me "Después de todo, nada me impide llamarla y luego ya veremos..."


translate spanish epilogue_dv_7bdf75da:


    "Sonreí, cargué en el hombro con mi guitarra y empecé a caminar hasta la parada de autobús de la línea 410 con mis compañeros."


translate spanish epilogue_sl_d4b0f999:


    "Hay sueños de los que no querrías despertarte nunca."


translate spanish epilogue_sl_63d02893:


    "Es como si uno fuera arrastrado por una corriente cálida hacia un lugar más allá del horizonte, contemplando dichosamente el banal mundo, oculto tras las nubes."


translate spanish epilogue_sl_f9b4fc62:


    "El pasado es dejado atrás, sus ecos no rasgan tu alma.{w} Y el futuro está justo aquí mismo, solamente tienes que alcanzarlo."


translate spanish epilogue_sl_410a9fd5:


    "No importa lo que te esté esperando ahí, el simple proceso de sumergirte a ti mismo en este mundo de hadas de serenidad y felicidad, es por sí mismo mucho más importante."


translate spanish epilogue_sl_aa0f75b6:


    "Siempre he creído que el universo estuvo en este estado y que un día retornará de nuevo a él."


translate spanish epilogue_sl_38f385ca:


    "Nuestra vida es sólo un abrir y cerrar de ojos, sólo un segundo en comparación con el universo, el nacimiento y la muerte de las estrellas es un minuto y el de las galaxias son horas."


translate spanish epilogue_sl_9c9a7284:


    "Pero incluso todo eso puesto junto, no sería ni un día."


translate spanish epilogue_sl_bd631aa1:


    "Nadie piensa en cómo vivir cada segundo específico así que, ¿acaso importa realmente lo que hace cada gota de agua en un caudaloso río, corriendo más allá del horizonte de la existencia?"


translate spanish epilogue_sl_1f69fba6:


    "Abrí ampliamente mis ojos con dicha."


translate spanish epilogue_sl_1210b0d8:


    "No tenía deseo alguno de levantarme mientras sintiera esa perfecta dicha, envuelto en una manta."


translate spanish epilogue_sl_590f6c27:


    "Después de todo, no tengo adónde ir.{w} En realidad, como de costumbre..."


translate spanish epilogue_sl_be7b0f0b:


    "Y no tengo planes."


translate spanish epilogue_sl_003b132f:


    "Entonces sencillamente, ¿por qué no debería dormir?"


translate spanish epilogue_sl_bd93e9e9:


    "Me giré y me quedé frente al viejo y desgastado papel que cubría la curvada pared."


translate spanish epilogue_sl_0a1ee678:


    "Me pregunto cuánto tiempo hace que renovaron por última vez esta habitación...{w} ¿Y de todas maneras por qué aquí hay papel de pared en vez de madera?"


translate spanish epilogue_sl_433cc26c:


    "Unas vagas y cálidas campanas comenzaron a resonar en alguna profunda parte de mi consciencia."


translate spanish epilogue_sl_771e1b84:


    "Aparté la manta, me puse en pie y furiosamente empecé a mirar a mi alrededor."


translate spanish epilogue_sl_9b6e0d96:


    "Sí, me encontraba en mi piso otra vez."


translate spanish epilogue_sl_2f2ad9f9:


    "Al principio estaba estupefacto."


translate spanish epilogue_sl_18a8a5cb:


    "La conmoción era tan grande que mi cerebro era incapaz de asimilar lo que sucedía."


translate spanish epilogue_sl_ea18891a:


    "Estaba simplemente de pie y miraba frente a mí la pantalla que estaba en la mesa."


translate spanish epilogue_sl_047fe1ee:


    "No podía pensar. Incluso olvidé respirar."


translate spanish epilogue_sl_3c6ecad1:


    "Finalmente mi mente comenzó a despejarse."


translate spanish epilogue_sl_fb89f263:


    "¿En cualquier caso qué ocurrió?{w} Me quedé dormido en el autobús, ¿yendo al distrito central?{w} Sí, algo así..."


translate spanish epilogue_sl_aa71a533:


    "Y Slavya estaba sentada a mi lado.{w} Y ahora he vuelto aquí..."


translate spanish epilogue_sl_9762c4e5:


    "Parece que durante la semana que me pasé allí en el campamento, me había acostumbrado tanto al hecho de que ya no volvería nunca a mi anterior vida, que ahora sencillamente no tenía ni idea de cómo reaccionar frente a todo esto."


translate spanish epilogue_sl_a44e9759:


    "Después de todo, en un principio mi deseo más importante era volver a casa..."


translate spanish epilogue_sl_b4b3948b:


    "Pero al final, todo fue como en una mala película. Al menos, había vuelto, pero todavía seguía afectado."


translate spanish epilogue_sl_af8879d8:


    "No, no sentía miedo.{w} Más bien estaba interesado... y molesto."


translate spanish epilogue_sl_29f47261:


    "Tras todo, ya me había hecho a la idea de empezar una vida nueva con Slavya, dejando todos mis problemas atrás... los insultos, los sufrimientos, la reflexión, asuntos a medias y planes de futuro."


translate spanish epilogue_sl_57794fd0:


    "Obviamente, esa vida no podía ser peor que ésta por lo menos."


translate spanish epilogue_sl_5c585492:


    "Pero no hay forma de retornar a ella ahora..."


translate spanish epilogue_sl_e0b5fc71:


    "POr otra parte, si alguien me hubiera dicho hace una semana atrás que habría sido enviado a un mundo distinto de aquella manera, en un abrir y cerrar de ojos, no le habría creído."


translate spanish epilogue_sl_21b9702d:


    "Y bien, ¿qué hace que semejante suceso fantástico sea imposible de ocurrir de nuevo?"


translate spanish epilogue_sl_d390fb44:


    "Moviendo mis piernas con dificultad, fui a la cocina, tomé un vaso de agua y retorné a la habitación."


translate spanish epilogue_sl_b6e3af01:


    "El líquido helado me dejó con un mal sabor de gusto a cloro que me despejó hasta cierto punto."


translate spanish epilogue_sl_e5e5f203:


    "Ahora tenía que decidir qué hacer."


translate spanish epilogue_sl_41ef88b1:


    "De repente, me di cuenta que no podía soportar el silencio."


translate spanish epilogue_sl_80b426bb:


    "Tras encender la computadora, seleccioné aleatoriamente una canción en el reproductor y logré calmarme de alguna manera."


translate spanish epilogue_sl_a0bf71cd:


    "Y hablando seriamente, ¿qué puedo hacer?{w} Hace poco fui consciente de que absolutamente nada depende de mí."


translate spanish epilogue_sl_a90c22ac:


    "Por voluntad de alguien fui arrebatado de mi mundo habitual y luego devuelto."


translate spanish epilogue_sl_35575ac5:


    "Ni una respuesta hallé durante la semana en el campamento."


translate spanish epilogue_sl_c9372110:


    "¿Qué sentido tiene especular ahora...?"


translate spanish epilogue_sl_d4e2d25c:


    "Debo olvidar todo lo que ocurrió, ¡como en un mal sueño!"


translate spanish epilogue_sl_32fbcaae:


    "Quizás aquello solo fueron alucinaciones, me trae sin cuidado ahora."


translate spanish epilogue_sl_9e130725:


    "La única cosa que me retenía de olvidar completamente ese corto período de mi vida, era Slavya."


translate spanish epilogue_sl_6d445c91:


    "Recordé su sonrisa y todo el tiempo que pasábamos juntos."


translate spanish epilogue_sl_4ef0184b:


    "Desde justo el encuentro del primer día, hasta la noche en el bosque y nuestra partida del campamento."


translate spanish epilogue_sl_51bc49e7:


    "Mi corazón se agitaba furiosamente dentro de mí pecho, esparciendo un dolor opresivo a través de mi cuerpo."


translate spanish epilogue_sl_36b7144d:


    "Me gustó desde la primera vez que la vi.{w} Tan dulce, tan atenta, tan comprensiva..."


translate spanish epilogue_sl_98add8a2:


    "Como esa heroína de anime... ¿cuál era su nombre? Aquella que ayudaba a un tipo antisocial como yo a superar una depresión."


translate spanish epilogue_sl_264c0834:


    "Y si lo piensas, ¡no existe gente así en la vida real!"


translate spanish epilogue_sl_5aa379da:


    "Slavya no pedía nada a cambio, no necesitaba ánimos, nunca esperó ser comprendida o elogiada por su trabajo."


translate spanish epilogue_sl_0a40af6e:


    "Ella era simplemente ella misma... una muchacha que no puede existir en la vida real."


translate spanish epilogue_sl_c52a2b6d:


    "Si lo piensas claramente, así es como era realmente."


translate spanish epilogue_sl_bcc4565b:


    "Es como si hubiera visto un único y largo sueño durante una semana."


translate spanish epilogue_sl_057f727b:


    "Con el campamento de pioneros, los adolescentes soviéticos y su líder de campamento, con las calurosas noches de verano y los encuentros junto a un fuego, con los desenfadados juegos de niños y la simple diversión humana, y con un instante que duró una semana y el sempiterno verano."


translate spanish epilogue_sl_dccb98b2:


    "Pero no solamente lo «vi».{w} Estuve allí, ¡yo formaba parte de todo!"


translate spanish epilogue_sl_d77755f6:


    "Mis ojos de mala gana se llenaron de lágrimas.{w} No lágrimas de dolor o desesperación... lágrimas de tristeza y una radiante melancolía."


translate spanish epilogue_sl_d1d01df5:


    "Incluso si todo se acabó, experimenté algo que mucha gente no podría ver ni en sueños."


translate spanish epilogue_sl_dc042d8c:


    "La imagen de Slavya destelleaba en mi cabeza, más y más brillante."


translate spanish epilogue_sl_75314796:


    "Habría querido muchísimo que ella volviera de aquel campamento junto a mí..."


translate spanish epilogue_sl_b649e1a2:


    "Tal vez no tengo nada que ofrecerle ahora, pero toda mi entera vida está por delante."


translate spanish epilogue_sl_4d7ca51b:


    "Supongo que aprendí muchas lecciones útiles de aquellos hechos."


translate spanish epilogue_sl_0b2cd74c:


    "No podía evitar pensar que todo parecía demasiado perfecto, como si estuviera escrito de acuerdo a algún simple plan."


translate spanish epilogue_sl_ad88d4c0:


    "Un héroe fracasado, un incidente fantástico y una maravillosa transformación..."


translate spanish epilogue_sl_eab34488:


    "Supongo que no es posible.{w} No en esta vida..."


translate spanish epilogue_sl_a1f94223:


    "¿Me convertiré en la persona que fui antes, sólo en un día o una semana después?"


translate spanish epilogue_sl_e3775951:


    "Bueno, es el devenir más realista que puede ocurrir."


translate spanish epilogue_sl_84316324:


    "Una cosa es segura, nunca más podré volver atrás..."


translate spanish epilogue_sl_ce617998:


    "..."


translate spanish epilogue_sl_dfcf3261:



    nvl clear
    "Un mes ha transcurrido desde que retorné de Sovyonok. O, a decir más correctamente, desde que desperté."


translate spanish epilogue_sl_65020c5a:


    "Retorné a mi vida habitual como un ermitaño, navegando por internet durante días, saliendo fuera sólo para comprar. Como esperaba, empezaba a adaptarme otra vez."


translate spanish epilogue_sl_5b8761f3:


    "Bueno, obviamente, ocurrían momentos de melancolía y depresión, una persona como yo no puede vivir sin ellos, pero no duraban tanto como de costumbre. Excepto aquellas veces que, mientras dormía, retornaba a aquel verano, aquel misterioso campamento..."


translate spanish epilogue_sl_c49c4047:


    "Pero tras despertarme, traté de disipar estos pensamientos tan pronto como fuera posible. A fin de cuentas, ¿qué sentido tiene soñar en convertirse en un hechicero?"


translate spanish epilogue_sl_7bbf60a6:



    nvl clear
    "Incluso si los milagros ocurren (¡y yo era la prueba viviente!), suceden de manera independiente por completo. La palabra «dulce» no te da un sabor dulce, a pesar de que la pronuncies a menudo."


translate spanish epilogue_sl_9a22d402:


    "No obstante, algo ha cambiado en mi existencia. Anteriormente no pensaba mucho más allá, no me preocupaba por cuánto viviría, tanto si fuera una semana como si fueran 40 años, pero ahora miraba más adelante con optimismo."


translate spanish epilogue_sl_5c734fc5:


    "No como si tratara de cambiar alguna cosa o convertirme en una persona diferente, sino que el mundo ahora me parecía mucho más comprensible. No era sencillo, tampoco amistoso, no, para nada amistoso, pero simplemente comprensible."


translate spanish epilogue_sl_d59449dc:


    "Antes no podía enfrentarme con algunos sucesos o hechos, a pesar de que de alguna manera, me daba cuenta de que estaban ahí por alguna razón. Ahora empezaba a ver la vida como algo simple... lo que ocurre, ¡ocurre para bien!"


translate spanish epilogue_sl_e1feb5f7:



    nvl clear
    "O por lo menos sucede y puedo vivir con ello. Y si no puedo, estoy desamparado en ese caso. Supongo que empecé a sonreír en más ocasiones, o al menos ya no cargaba con aquella mirada de profunda frustración universal las veinticuatro horas de la semana."


translate spanish epilogue_sl_a49c2c8d:


    "Incluso mis conocidos (en internet, obviamente) se dieron cuenta de un cambio en mí. Y sus opiniones estaban de acuerdo en que estos cambios eran buenos."


translate spanish epilogue_sl_cffd8fe2:


    "Ahora creo que estoy preparado para vivir. Gracias a Sovyonok y sus lugareños.{w} Sin ellos no habría sido capaz..."


translate spanish epilogue_sl_ce617998_1:


    "..."


translate spanish epilogue_sl_76b2fe88:


    nvl clear


translate spanish epilogue_sl_61db16fc:


    "Fue a principios de enero."


translate spanish epilogue_sl_0d80cbdc:


    "La ciudad apenas se había recuperado de la resaca de Nochevieja, las calles estaban casi vacías durante el día. Por no mencionar las noches, cuando el único extraño caminando a través de la calle, parecía extremedamente ocupado con algún asunto, corriendo hacia alguna parte por alguna razón por la que no podía esperar."


translate spanish epilogue_sl_f357f57f:


    "No tenía motivo alguno, simplemente quise dar un paseo... es necesario tomar el aire fresco de vez en cuando."


translate spanish epilogue_sl_6c6bdb78:


    "Una noche de invierno es el mejor momento para estar sólo contigo mismo."


translate spanish epilogue_sl_8cf56a1b:



    nvl clear
    "Siempre creí que la verdadera solitud se puede sentir en una multitud mejor que en un caluroso desierto, llano y sin límites, o en la cumbre de una montaña cubierta de nieve."


translate spanish epilogue_sl_761bb15b:


    "En el devenir de las personas, las palabras, pensamientos y aspiraciones de todas ellas tienen su meta y dirección."


translate spanish epilogue_sl_2ffddc18:


    "Y era el único que se paseaba entre ellos, sin merecer atención siquiera de los demás."


translate spanish epilogue_sl_c5c605e3:


    "¿Quizás ellos son vectores moviéndose en diferentes direcciones? Nunca se cruzan sus coordenadas cartesianas."


translate spanish epilogue_sl_e35f54cf:



    nvl clear
    "Lo mismo pasa aquí... esta gente está caminando, corriendo en una dirección y yo me dejo arrastrar hacia otra."


translate spanish epilogue_sl_6ec15f5e:


    "¿Pero acaso me siento sólo?{w} Antes... sí. Pero ahora... muy improbablemente."


translate spanish epilogue_sl_ad5015f4:


    "En las brillantes y parpadeantes luces de la ciudad, el ruido de los vehículos y la ebullición de las muchedumbres... ¡Disfrutaba de una celestial simfonía de silencio!"


translate spanish epilogue_sl_c26cab4f:


    "Pero ahora la situación es ligeramente distinta, solamente estoy yo en la calle, por lo que estoy solo no solamente conmigo mismo, sino con todo el mundo."


translate spanish epilogue_sl_804dc10a:



    nvl clear
    "¡Ser un grano de arena en el desierto no es lo mismo que ser una gota de agua en el océano!"


translate spanish epilogue_sl_cc6ceeaf:


    "Estando entre la gente nadie se percata de mí, nadie me presta atención, pero ahora todos los que echen un vistazo por la ventana o me vean mientras conducen pensarán: «¿Por qué no se queda en casa?», «¿Quizás tenga un asunto urgente pendiente?», «¿Se ha vuelto loco?», «¿O estará borracho?»..."


translate spanish epilogue_sl_87c3c17d:


    "Nunca me gustó estar fuera de lugar entre las muchedumbres cuando es apropiado, así que no era una opción para mí el llevar la contraria a la sociedad."


translate spanish epilogue_sl_1e8b6cba:


    "Pero ahora era diferente."


translate spanish epilogue_sl_c230e647:



    nvl clear
    "Como si tratara de decirle a todos: «¡Mirad! ¡Puedo ser feliz también! ¡Vosotros necesitáis vuestra televisión y vuestro chocolate caliente, pero yo sólo necesito la nieve, la noche, una habitación oscura y una pantalla! ¡Mirad! ¡Ya no soy peor que vosotros!»."


translate spanish epilogue_sl_53d3760b:


    "Me sentí triste en lo profundo de mi corazón, pero era una tristeza serena."


translate spanish epilogue_sl_bb50df60:


    "¿Acaso necesito de verdad las cosas que ellos tienen y yo no?"


translate spanish epilogue_sl_7d88b51c:


    "¿Quizás tengo algo de mucho más valor?"


translate spanish epilogue_sl_ce617998_2:


    "..."


translate spanish epilogue_sl_950f1be7:


    "No me di cuenta de que llegué a la parada del autobús de la línea 410."


translate spanish epilogue_sl_7535addf:


    me "Qué recuerdos..."


translate spanish epilogue_sl_8da3fa57:


    "Me senté y hurgué en mis bolsillos, tratando de buscar los cigarrillos.{w} Sin embargo el paquete estaba vacío."


translate spanish epilogue_sl_a54cf057:


    me "Vaya..."


translate spanish epilogue_sl_fd5a0f69:


    "Lo doblé arrugándolo y lo tiré a un contenedor de basura."


translate spanish epilogue_sl_a81c77bf:


    me "¡Tal vez me vuelva más sano!"


translate spanish epilogue_sl_a97808f5:


    "Una estrella solitaria estaba brillando en el cielo."


translate spanish epilogue_sl_264e6e2b:


    "Hoy leí en internet que, de acuerdo a los cálculos de los científicos, uno de los más bellos acontecimientos astronómicos ya ha cesado de existir y sólo volveremos a verlo décadas después."


translate spanish epilogue_sl_e61de771:


    "¿Quizás esta estrella ya ha explotado también y ahora solamente es una bonita imagen en el cielo nocturno? En algún planeta perdido en los extremos de la galaxia."


translate spanish epilogue_sl_7ad4fe45:


    "Sin embargo, ¿por qué una u otra cosa existe?"


translate spanish epilogue_sl_28b32374:


    "¿Es simplemente porque justo está ahí, tiene forma y puede ser tocado?"


translate spanish epilogue_sl_c62c06d1:


    "¿O es porque creemos que existe?"


translate spanish epilogue_sl_604959ba:


    "A primera vista la respuesta es sencilla.{w} Pero por otra parte..."


translate spanish epilogue_sl_13a6a7c7:


    "A pesar de que esta estrella muriera, todavía podemos apreciar su fría luz."


translate spanish epilogue_sl_21b7ddca:


    "Tal vez ayudó a alguien a salir de un bosque nevado en pleno invierno, o le dio a otro esperanza o un poco de calidez a otro."


translate spanish epilogue_sl_19248a5c:


    "¿Puede un simple objeto astronómico, que explotó Dios sabe cuando, hacer todo esto?{w} ¿Pueden miles de millones de personas creer en algo que realmente no existe?"


translate spanish epilogue_sl_311843a1:


    "Pero la creencia por sí misma nunca materializó algo real.{w} Al menos ninguno que yo haya escuchado."


translate spanish epilogue_sl_ce617998_3:


    "..."


translate spanish epilogue_sl_8844dc3d:


    "Nevó más fuertemente, ahora era una verdadera tormenta de nieve.{w} A medianoche."


translate spanish epilogue_sl_d1e598ce:


    "¿Se volverá mi carro en una calabaza en cualquier instante?"


translate spanish epilogue_sl_f2165086:


    "¿Quizá mi retorno de Sovyonok es el final de mi propia versión del cuento de «Cenicienta»?"


translate spanish epilogue_sl_ced18126:


    "Y aun así, ¿es realmente un final malo?"


translate spanish epilogue_sl_9f4f4f69:


    "Porque en cierto modo, cualquier cuento contiene al menos una pequeña parte de verdad."


translate spanish epilogue_sl_f7a557f1:


    "Recordé a Slavya, me puse en pie, me sacudí la nieve y me adentré lentamente en la noche."


translate spanish epilogue_sl_1eaf8011:


    "No sentía pena, no tenía remordimientos."


translate spanish epilogue_sl_aabe0924:


    "Tal vez esto es tan sólo otro episodio de ese cuento, ese cálido sueño, que tanto quería continuar."


translate spanish epilogue_sl_498f7c4e:


    "¿Por qué no puede repetirse, si ya ha ocurrido una vez?"


translate spanish epilogue_sl_8ebdfc5e:


    "¿Y si me encuentro con Slavya en la realidad...?"


translate spanish epilogue_sl_a20cefa7:


    "..."


translate spanish epilogue_sl_8074d8af:


    "Estaba a punto de irme, pero de repente alguien apareció tras el sólido velo blanco y se resguardó corriendo bajo el techo de la parada de autobús."


translate spanish epilogue_sl_96af921b:


    "Era claramente una muchacha puesto que tenía una larga coleta colgando fuera de su capucha."


translate spanish epilogue_sl_81772974:


    "Ella estaba dándome la espalda, así que no podía figurarme qué rostro tenía."


translate spanish epilogue_sl_501a8b3a:


    slg "Discúlpeme, ¿ya salió el último autobús?"


translate spanish epilogue_sl_454f3f2b:


    "Me preguntó sin darse la vuelta."


translate spanish epilogue_sl_14528e2c:


    "¿Adónde se dirigirá tan tarde?"


translate spanish epilogue_sl_0a4debed:


    "La línea 410 debe ser la línea más maldita de esta ciudad."


translate spanish epilogue_sl_6e4ac050:


    "No sólo porque a veces secuestra a la gente hacia otra realidad, sino porque encima no puede venir cuando quieres."


translate spanish epilogue_sl_451ccc51:


    me "Hay otro autobús que llegará, justo tras la medianoche, creo."


translate spanish epilogue_sl_d722e8b7:


    slg "Gracias."


translate spanish epilogue_sl_839f9131:


    "La sonrisa se podía apreciar en su voz."


translate spanish epilogue_sl_58d296bc:


    "Durante un rato tan sólo estuve sentado en silencio, observando a la muchacha."


translate spanish epilogue_sl_09bca910:


    "Finalmente ella habló:"


translate spanish epilogue_sl_ffb89fb4:


    slg "¿Dónde vas tan tarde?"


translate spanish epilogue_sl_a945e3a8:


    "¿De verdad que eso le incumbe?"


translate spanish epilogue_sl_68e9d064:


    me "Solamente doy un paseo..."


translate spanish epilogue_sl_f2f10b4d:


    "Respondí pensativamente."


translate spanish epilogue_sl_9eb3ac16:


    me "Y tú... ¿estás aquí por algo importante?"


translate spanish epilogue_sl_a8743c64:


    slg "Bueno, realmente no...{w} Sólo vuelvo a casa."


translate spanish epilogue_sl_591089ca:


    me "¿Dónde vives?"


translate spanish epilogue_sl_9a483b3c:


    "No pude evitar preguntarle."


translate spanish epilogue_sl_f9bd2f63:


    slg "Muy lejos..."


translate spanish epilogue_sl_99a29ab7:


    "Dijo vagamente."


translate spanish epilogue_sl_64f7f202:


    me "Pues yo vivo cerca."


translate spanish epilogue_sl_a7590312:


    slg "Es una buena zona."


translate spanish epilogue_sl_c75aa891:


    "Yo diría que no podría haber otra peor."


translate spanish epilogue_sl_263611b4:


    me "Tampoco tuve muchas opciones..."


translate spanish epilogue_sl_13f66e78:


    slg "¿No te gusta?"


translate spanish epilogue_sl_24cdca72:


    me "No sé, me acostumbré por ahora."


translate spanish epilogue_sl_837a1356:


    slg "¡Creo que la gente puede ser feliz allá donde quieran!"


translate spanish epilogue_sl_907b6d20:


    me "Quizás..."


translate spanish epilogue_sl_2612f632:


    slg "Bueno, ¡piénsalo!{w} Las personas viven incluso en el Polo Norte... en estaciones quiero decir.{w} ¡Y también en el desierto del Sáhara! Y en muchas otras partes. ¡Son las personas por sí mismas lo que son más importante!"


translate spanish epilogue_sl_a82636de:


    me "Es difícil estar en desacuerdo contigo."


translate spanish epilogue_sl_76f34f02:


    "Mi comentario sonó tan filosófico que inconscientemente sonreí."


translate spanish epilogue_sl_f55284f9:


    slg "No me pareces muy sincero conmigo."


translate spanish epilogue_sl_724d66a5:


    me "Y eso, ¿por qué lo dice, señora?"


translate spanish epilogue_sl_8baa587f:


    "Ella se rio en voz baja."


translate spanish epilogue_sl_de9d3a74:


    slg "¡Creo que eres una buena persona!"


translate spanish epilogue_sl_b4d548da:


    "Ahora se dio la vuelta y..."


translate spanish epilogue_sl_7f9ddde9:


    "Me di cuenta de por qué su voz, su estatura y todo lo demás me resultaba tan familiar."


translate spanish epilogue_sl_5d2bdfb2:


    "Slavya estaba frente a mí."


translate spanish epilogue_sl_d7bb50f0:


    "Por un instante perdí la capacidad de hablar, pero solamente por un momento."


translate spanish epilogue_sl_30204e6b:


    "Por supuesto, podría ser tan sólo una muchacha que se pareciera a la Slavya de mi sueño.{w} O estoy experimentando el mayor déjà vu..."


translate spanish epilogue_sl_7c085c9f:


    "Esta ciudad no es un campamento de pioneros, no estoy vistiendo un uniforme y ahora no es verano."


translate spanish epilogue_sl_a32736d4:


    "Tratando de convencerme de que solamente era una ilusión, elegí hacerle una pregunta."


translate spanish epilogue_sl_82ad3926:


    me "¿Nos hemos conocido antes?"


translate spanish epilogue_sl_6ac4a2e6:


    "Me examinó detenidamente y me sonrió."


translate spanish epilogue_sl_38e86f4c:


    slg "No lo creo...{w} pero tu cara me resulta familiar."


translate spanish epilogue_sl_40a6ca02:


    me "Tú... ¿habías estado alguna vez en un campamento de pioneros de niña?"


translate spanish epilogue_sl_73927ac0:


    slg "¡Pues no!"


translate spanish epilogue_sl_ce3666e5:


    "Ella se rio."


translate spanish epilogue_sl_c7fd5288:


    slg "Cuando nací ya no había. "


translate spanish epilogue_sl_0d9f82ca:


    "Oh, cierto..."


translate spanish epilogue_sl_2d02b5b6:


    slg "Aunque...{w} Tuve un sueño recientemente..."


translate spanish epilogue_sl_60f3d758:


    "Pensó por un segundo."


translate spanish epilogue_sl_83d4403a:


    me "Yo también.{w} Quiero decir sobre un campamento de pioneros."


translate spanish epilogue_sl_fe5d4dde:


    slg "Tal vez sea ahí dónde nos conocimos."


translate spanish epilogue_sl_e26e8716:


    "La muchacha lo dijo en serio."


translate spanish epilogue_sl_fd914013:


    me "Posiblemente."


translate spanish epilogue_sl_4c079f2d:


    slg "Por cierto, mi nombre es Slavya. En realidad, mi nombre completo es Slavyana pero todos me llaman Slavya. ¡También puedes llamarme así!"


translate spanish epilogue_sl_b4002489:


    "Iba a contarle que eso lo sabía, pero me abstuve."


translate spanish epilogue_sl_b54dd65c:


    "Después de todo, ¿acaso importa saber por qué la Slavya de mi sueño está justo aquí y ahora frente a mí en esta congelada parada de autobús de la encantadora línea número 410?"


translate spanish epilogue_sl_a4217a2f:


    "A fin de cuentas, ¡hay sueños de los que no querrías despertarte nunca!"


translate spanish epilogue_sl_94f8232c:


    me "El mío es Semyon..."


translate spanish epilogue_sl_e62d08e6:


    sl "Qué nombre más bonito."


translate spanish epilogue_sl_6170b881:


    me "Comienza con una «S» también."


translate spanish epilogue_sl_8b01f8be:


    sl "Claro."


translate spanish epilogue_sl_8b7f6f51:


    me "Bueno, así que vives lejos, ¿no?"


translate spanish epilogue_sl_af4e649f:


    sl "Bueno, no mucho..."


translate spanish epilogue_sl_7cfc34fb:


    "Ella nombró el distrito."


translate spanish epilogue_sl_f14fd67f:


    me "Pensé que sería en alguna parte del sur."


translate spanish epilogue_sl_70f37a53:


    sl "¿Por qué?"


translate spanish epilogue_sl_e902ce8b:


    me "No sé...{w} Tal vez porque tu aspecto es como el de una pionera."


translate spanish epilogue_sl_90481d5f:


    sl "¡Anda ya, tú y tus pioneros!"


translate spanish epilogue_sl_c954d69f:


    "Slavya se rio."


translate spanish epilogue_sl_ee42356b:


    me "Quizás no nos hubiéramos conocido de no ser por ellos."


translate spanish epilogue_sl_70f37a53_1:


    sl "¿Por qué?"


translate spanish epilogue_sl_cfc2bc6d:


    "Su expresión se volvió seria."


translate spanish epilogue_sl_e8b9f9a3:


    me "Sólo era... una broma...{w} Escucha, ¿estás de acuerdo en que el mundo existe porque creemos en él?"


translate spanish epilogue_sl_17172b5c:


    sl "Eso es algún tipo de idealismo subjetivo..."


translate spanish epilogue_sl_bbc1f2e4:


    "Slavya lo pensó un poco."


translate spanish epilogue_sl_eb6cea32:


    me "Recientemente esta escuela de filosofía se volvió próxima a mis ideas."


translate spanish epilogue_sl_5095dd33:


    sl "Bueno, no lo sé...{w} No soy muy buena con estas materias."


translate spanish epilogue_sl_e15fe545:


    me "¿Cuáles son tus materias pues?"


translate spanish epilogue_sl_d1046a49:


    "Sonrió."


translate spanish epilogue_sl_719e7c45:


    sl "Unas diferentes."


translate spanish epilogue_sl_7f708c46:


    me "¡No lo dudo!"


translate spanish epilogue_sl_d1aa5e5c:


    sl "¡Es que hace tan solo unos minutos que nos hemos conocido!"


translate spanish epilogue_sl_79ac6bd7:


    me "Pero parece que haya sido mucho más tiempo."


translate spanish epilogue_sl_1cc42161:


    sl "Por alguna razón tengo la misma sensación..."


translate spanish epilogue_sl_cc1284f3:


    "Una pausa incómoda le siguió."


translate spanish epilogue_sl_644f4e1f:


    "La nevada se redució pero aun así no había venido el autobús."


translate spanish epilogue_sl_d86d06b8:


    "Miré mi reloj y supuse que ya no llegaría."


translate spanish epilogue_sl_6a9e8821:


    "Slavya parecía estar pasando frío ahora, en tanto sus mejillas se ponían más y más coloradas y sus manos se movían frenéticamente, tratando de hacer entrar en calor su cuerpo."


translate spanish epilogue_sl_161b1e49:


    me "Parece que el autobús de la línea 410 ya no circulará hoy."


translate spanish epilogue_sl_1555b86b:


    sl "Supongo..."


translate spanish epilogue_sl_1e8379d1:


    "Dijo desafortunada."


translate spanish epilogue_sl_732bddc6:


    me "Si quieres..."


translate spanish epilogue_sl_8fe9063e:


    "Comencé a decir mientras reunía fuerzas."


translate spanish epilogue_sl_e07c37b3:


    sl "¿Qué?"


translate spanish epilogue_sl_beb50f2a:


    me "Puedes quedarte conmigo..."


translate spanish epilogue_sl_4f36139c:


    sl "Pero si nos acabamos de conocer."


translate spanish epilogue_sl_0467a4c4:


    "Se rio."


translate spanish epilogue_sl_bd3e21d9:


    "No parecía estar asustada ni un ápice."


translate spanish epilogue_sl_6b12c510:


    me "Sí, lo entiendo, pero se pasa mucho frío fuera..."


translate spanish epilogue_sl_d03f0acf:


    sl "¿Estás preocupado?"


translate spanish epilogue_sl_c93fe8e6:


    "Slavya me miró alegremente."


translate spanish epilogue_sl_10f87319:


    me "¿Yo? ¿Debería estarlo?"


translate spanish epilogue_sl_b71c6525:


    sl "Quién sabe... quién sabe..."


translate spanish epilogue_sl_58f5f795:


    me "Solamente lo sugerí, no te pienses nada raro..."


translate spanish epilogue_sl_c99f65be:


    "Empecé a poner excusas."


translate spanish epilogue_sl_1ee2e52a:


    sl "¡Comprendo!"


translate spanish epilogue_sl_ec2c2a7c:


    "Ella se volvió a reír."


translate spanish epilogue_sl_0cc7ab77:


    sl "Si no te importa, claro..."


translate spanish epilogue_sl_7ec51fd8:


    "Incluso a pesar de parecer imposible, ella se sonrojó aun más."


translate spanish epilogue_sl_71f4991e:


    me "¿Vamos?"


translate spanish epilogue_sl_c1235c80:


    sl "Esperemos un poco más, tal vez llegue."


translate spanish epilogue_sl_71d3b8c8:


    me "Vale."


translate spanish epilogue_sl_ce617998_4:


    "..."


translate spanish epilogue_sl_42acf572:


    sl "¿Y qué haces en tu tiempo libre?"


translate spanish epilogue_sl_ac2505d0:


    "Tras un rato Slavya me preguntó."


translate spanish epilogue_sl_cebb61ed:


    me "Yo... bueno...{w} Trabajo en casa, algo así."


translate spanish epilogue_sl_03bc366f:


    "No podía decirle a ella que sencillamente no hacía «nada»."


translate spanish epilogue_sl_36ffb439:


    sl "¡Suena bien!"


translate spanish epilogue_sl_75f4e79a:


    me "¿Por qué?"


translate spanish epilogue_sl_d74e7a4a:


    sl "No sé...{w} Tienes mucho tiempo libre, ¡no tienes que ir a ninguna parte! Y todo eso..."


translate spanish epilogue_sl_b78c6e26:


    me "¿Y tú?"


translate spanish epilogue_sl_b620325b:


    "Pregunté, tratando de cambiar de asunto de algún modo."


translate spanish epilogue_sl_c42d7da0:


    sl "Estoy estudiando."


translate spanish epilogue_sl_606db6d9:


    me "¿Sacándote un grado en etnografía?"


translate spanish epilogue_sl_01f26b1d:


    sl "¿Qué es eso?"


translate spanish epilogue_sl_38a71a20:


    "Preguntó Slavya con cara seria."


translate spanish epilogue_sl_a52d1d58:


    me "Olvídalo..."


translate spanish epilogue_sl_5bca72ca:


    sl "Seré una ecologista."


translate spanish epilogue_sl_f19cb2e9:


    me "Bueno, una profesión de la que estar orgullosa..."


translate spanish epilogue_sl_ac8018d5:


    "Charlamos sobre variadas cosas y el autobús todavía estaba sin aparecer."


translate spanish epilogue_sl_8231d531:


    "Ya era más de la una.{w} El tiempo transcurría demasiado rápido."


translate spanish epilogue_sl_e3127a21:


    "Supongo que Slavya comprendía mis pensamientos."


translate spanish epilogue_sl_ad3d2e0c:


    sl "¿Nos vamos?"


translate spanish epilogue_sl_06ab0532:


    "Ella sonrió."


translate spanish epilogue_sl_a6a0a962:


    "Me puse en pie y me dirigí lentamente hacia mi piso."


translate spanish epilogue_sl_312acdec:


    "Ella se aferró con cuidado a mi brazo y me miró a los ojos."


translate spanish epilogue_sl_ae697d22:


    sl "Sabes, ¡estoy segura de que este año será mejor que el anterior!"


translate spanish epilogue_sl_a20cefa7_1:


    "..."
# Decompiled by unrpyc: https://github.com/CensoredUsername/unrpyc
