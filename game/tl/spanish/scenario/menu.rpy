translate spanish strings:


    old "Да, я пойду с тобой"
    new "Sí, iré contigo"


    old "Нет, я останусь здесь"
    new "No, me quedaré aquí"


    old "Не отвечать ей"
    new "No responder"


    old "Ответить"
    new "Responder"


    old "Побежать за ним"
    new "Correr tras él"


    old "Ничего не делать"
    new "No hacer nada"


    old "Попытаться отнять котлету"
    new "Intentar agarrar la chuleta"


    old "Не пытаться отнять котлету"
    new "No intentar agarrar la chuleta"


    old "Взять ключи"
    new "Coger las llaves"


    old "Не трогать"
    new "Dejar las llaves"


    old "Похвалить книгу"
    new "Elogiar el libro"


    old "Ничего не говорить"
    new "Mantenerse callado"


    old "Пойти за картами со Славей"
    new "Ir a coger las cartas con Slavya"


    old "Пойти одному"
    new "Ir solo"


    old "Поспорить с Алисой"
    new "Apostar con Alisa"


    old "Не спорить с Алисой"
    new "No apostar con Alisa"


    old "Пройти обучение"
    new "Jugar el tutorial"


    old "Пропустить обучение"
    new "Saltarse el tutorial"


    old "Играть турнир"
    new "Jugar el torneo"


    old "Пропустить турнир (выиграть у Алисы)"
    new "Saltarse el torneo (Gano contra Alisa)"


    old "Пропустить турнир (проиграть Алисе)"
    new "Saltarse el torneo (Pierdo contra Alisa)"


    old "Пропустить турнир (проиграть Ульяне)"
    new "Saltarse el torneo (Pierdo contra Ulyana)"


    old "Пропустить турнир (проиграть Лене)"
    new "Saltarse el torneo (Pierdo contra Lena)"


    old "Извини, но я уже с Леной договорился"
    new "Lo siento, pero se lo prometí a Lena"


    old "Ладно, подожди минутку"
    new "Está bien, espera un minuto"


    old "Хорошо, я приду..."
    new "Vale, iré..."


    old "Знаете, меня Ольга Дмитриевна попросила вечером ей помочь..."
    new "Sabes, Olga Dmitrievna me pidió ayudarla esta noche..."


    old "Стоит пойти посмотреть"
    new "Debería ir a echar un vistazo"


    old "Какая разница? Надо продолжить искать ответы"
    new "¿A quién le importa? Mejor sigo buscando respuestas"


    old "Пожалуй, я помогу Славе"
    new "Mejor ayudo a Slavya"


    old "Думаю, помогу ребятам в постройке гигантских роботов"
    new "Creo que mejor les ayudo a los tipos aquellos con su ingenio de robot gigante"


    old "Ладно, я помогу спортивному клубу"
    new "Está bien, ayudaré al club de deporte"


    old "Ладно, я приду"
    new "Vale, iré"


    old "Нет, извини"
    new "No, lo siento"


    old "Сбежать"
    new "Huir"


    old "Остаться и помочь Ульяне убраться"
    new "Quedarse y ayudar a Ulyana con la limpieza"


    old "Пойти к Алисе"
    new "Ir con Alisa"


    old "Пойти на дискотеку"
    new "Ir a la fiesta de baile"


    old "Рассказать, что был с Алисой"
    new "Explicarle que he estado con Alisa"


    old "Придумать другое оправдание"
    new "Inventarse otra excusa"


    old "Пойти с Мику"
    new "Ir con Miku"


    old "Не ходить с Мику"
    new "No ir con Miku"


    old "Мне кажется, на тебе это бы смотрелось прекрасно"
    new "Creo que estás maravillosa con eso"


    old "Да просто так"
    new "Sólo preguntaba"


    old "Небось ворованных конфет объелась?"
    new "Sobredosis de caramelos robados, ¿cierto?"


    old "В столовой отравилась?"
    new "¿Tomaste comida en mal estado en la cantina?"


    old "Спросить, что это"
    new "Preguntarle sobre el paquete"


    old "Не спрашивать"
    new "No preguntarle sobre el paquete"


    old "Дать Алисе уголь"
    new "Darle a Alisa el carbón activado"


    old "Не давать ей уголь"
    new "No darle a Alisa el carbón activado"


    old "Съесть яблоко"
    new "Comer la manzana"


    old "Не есть"
    new "No comer la manzana"


    old "Пошёл с Алисой"
    new "...ir con Alisa"


    old "Пошёл со Славей"
    new "...ir con Slavya"


    old "Пошёл с Ульяной"
    new "...ir con Ulyana"


    old "Пошёл с Леной"
    new "...ir con Lena"


    old "Пошёл один"
    new "...ir solo"


    old "Налево"
    new "Ir a la izquierda"


    old "Направо"
    new "Ir a la derecha"


    old "Сказать, что ходил с Леной"
    new "Explicarle que fuiste con Lena"


    old "Сказать, что ходил один"
    new "Explicarle que fuiste solo"


    old "Ладно…"
    new "Vale..."


    old "Нет, знаешь, у меня дела ещё…"
    new "No, sabes, tengo algunas cosas que hacer..."


    old "Согласиться"
    new "Aceptar"


    old "Отказаться"
    new "Rechazar"


    old "Пойти с Леной"
    new "Ir con Lena"


    old "Пойти со Славей"
    new "Ir con Slavya"


    old "Да, я старался!"
    new "¡Sep, lo hice lo mejor que pude!"


    old "Это всё благодаря помощи девочек!"
    new "¡Gracias a la ayuda de las muchachas!"


    old "Попытаться выхватить книгу у Алисы"
    new "Intentar arrebatar el libro de las manos de Alisa"


    old "Проявить осторожность"
    new "Estar alerta"


    old "Попытаться узнать, о чём спорят Лена и Алиса"
    new "Intentar descubrir de qué están discutiendo Alisa y Lena"


    old "Не делать ничего, просто сидеть"
    new "No hacer nada, solamente quedarse sentado"


    old "Попытаться найти Славю"
    new "Intentar encontrar a Slavya"


    old "Пойти к Ульяне"
    new "Ir con Ulyana"


    old "Просто сидеть"
    new "Permanecer sentado"


    old "Не вмешиваться"
    new "No interferir"


    old "Помочь Алисе"
    new "Ayudar a Alisa"


    old "Грубо схватить её за руку"
    new "Agarrar su brazo bruscamente"


    old "Попытаться остановить словами"
    new "Intentar detenerla verbalmente"


    old "Да просто есть захотелось"
    new "Solamente estaba hambriento"


    old "Хочу взять еду для Слави"
    new "Quiero conseguir algo para Slavya"


    old "Мне не в чем оправдываться"
    new "¡No me tengo que justificar para nada!"


    old "Мы ничего такого не делали, чтобы перед вами оправдываться!"
    new "¡No hicimos nada para tener que justificarnos!"


    old "Пойти за голосом"
    new "Seguir la voz"


    old "Не ходить за голосом"
    new "No seguir la voz"


    old "Это всё она!"
    new "¡Todo eso es culpa suya!"


    old "Всё это моя вина..."
    new "Todo eso es culpa mía..."


    old "Идти вперёд"
    new "Avanzar"


    old "Повернуть назад"
    new "Volver"


    old "Выбрать Юлю"
    new "Elegir a Yulya"


    old "Выбрать Славю"
    new "Elegir a Slavya"


    old "Выбрать Алису"
    new "Elegir a Alisa"


    old "Выбрать Лену"
    new "Elegir a Lena"


    old "Выбрать Ульяну"
    new "Elegir a Ulyana"


    old "Выбрать Машу"
    new "Elegir a Masha"
# Decompiled by unrpyc: https://github.com/CensoredUsername/unrpyc
