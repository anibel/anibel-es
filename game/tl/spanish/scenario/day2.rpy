
translate spanish day2_main1_5a2ae8b2:


    "Estaba teniendo un sueño..."


translate spanish day2_main1_fc07b71b:


    "Parecía que estaba en algún tipo de vacío, con nada más que nada a mi alrededor."


translate spanish day2_main1_895753c3:


    "Pero no solamente {i}alrededor{/i}: Era la única criatura en el Universo."


translate spanish day2_main1_0ceef837:


    "Como si el Universo hubiera retornado a algún estado singular justo antes del Big Bang."


translate spanish day2_main1_e5affe53:


    "Y algo estaba a punto de suceder."


translate spanish day2_main1_1409c490:


    "Repentinamente escuché una voz."


translate spanish day2_main1_7451916c:


    "No podía comprender las palabras, pero resultaron familiares."


translate spanish day2_main1_30fbd9d9:


    "La voz estaba susurrando algo gentilmente, como si arrullara."


translate spanish day2_main1_72c99b77:


    "Y entonces me di cuenta...{w} Era la voz de aquella extraña muchacha del autobús.{w} La muchacha del sueño."


translate spanish day2_main1_08be4530:


    th "¿Pero qué está tratando de contarme? ¿Quién es ella...?"


translate spanish day2_main1_4b4e1333:


    "Desperté."


translate spanish day2_main1_43e6696c:


    "Un rayo solar que brilló me golpeaba en mis ojos."


translate spanish day2_main1_ddaaef10:


    "Era casi el mediodía."


translate spanish day2_main1_88d071f4:


    "Después de estirarme perezosamente en la cama y bostezar, empecé a recordar el día anterior."


translate spanish day2_main1_062f89ad:


    "En pocos segundos, todos los sucesos pasaron frente a mi: el autobús, el campamento y los lugareños."


translate spanish day2_main1_32bb3fe1:


    th "¡No, tiene que estar mal!"


translate spanish day2_main1_efb38133:


    "No toda esta situación, no estando yo aquí... estaba mal por defecto... mi actitud frente a lo que estaba pasando era incorrecta."


translate spanish day2_main1_7ac9eb97:


    th "Porque ayer bien que me caí dormido aquí, y antes de ello, estuve hablando a gusto con las pioneras de aquí, ¡incluso me las apañé para bromear!"


translate spanish day2_main1_31cacf90:


    th "¡¿Cómo podria llegar a estar en semejante situación?!"


translate spanish day2_main1_9f97589a:


    th "Debería estar asustado, sobresaltado por cada pequeño crujido, debería evitar todo contacto de criaturas potencialmente hostiles."


translate spanish day2_main1_321177f9:


    "Los sucesos del último día se vuelven borrosos, como si tuviera una resaca."


translate spanish day2_main1_0b66f5c8:


    th "Realmente se siente como una mañana después de una fiesta bastante larga: la conducta natural del día anterior, perfecta y absolutamente normal se convierte en una pesadilla por la mañana, una grotesca ilustración de la «Divina Comedia»."


translate spanish day2_main1_2c6f5607:


    th "Sí, es justo como eso, y no puedo cambiar el pasado ahora."


translate spanish day2_main1_7756bdbc:


    "Entonces, una vez más, probablemente juzgaría mi situación y actuaría de acuerdo a ella."


translate spanish day2_main1_b93eac53:


    "Observé a mi alrededor, tratando de averiguar si es que no habría sido tirado en algun lugar, pero la cabaña de Olga Dmitrievna parecía la misma de ayer."


translate spanish day2_main1_63937111:


    "Todo parecía estar en su lugar, excepto por un conjunto de uniforme de pionero, el cual estaba colgando de la cabecera de la cama."


translate spanish day2_main1_3b552dd7:


    "Al final, lo busqué a tientas e intenté ponérmelo desconfiadamente."


translate spanish day2_main1_d906fb96:


    th "De todos modos, esto es mejor que ir caminando por ahí con ropa de invierno."


translate spanish day2_main1_207c3b3d:


    th "Desearía poderme ver a mi mismo... ¡Apuesto a que me veo como un payaso!"


translate spanish day2_main1_01922da1:


    "Y para eso, necesito un espejo.{w} Al menos uno pequeño."


translate spanish day2_main1_c8ca898d:


    "Y finalmente hallé uno en la puerta del armario."


translate spanish day2_main1_9fe62ef5:


    me "¡La virgen...!"


translate spanish day2_main1_3e51bea7:


    "¡Estaba mirando al recién descubierto pionero y salté alejándome sorprendido!"


translate spanish day2_main1_a7207d27:


    "¡Había allí algún adolescente en el otro lado del espejo!"


translate spanish day2_main1_a5cf122d:


    "Él se parecía a mi, ¡pero no era yo!"


translate spanish day2_main1_50576b33:


    "¡¿Dónde estaba la barba de varias semanas, dónde estaban las bolsas de mis ojos, mi espalda encorvada, esa mortecina fatiga en mi cara?!"


translate spanish day2_main1_b7edc738:


    "Parecía que no había sido enviado al pasado o a una realidad paralela, sino que en su lugar simplemente cambié mi cuerpo con el de otro."


translate spanish day2_main1_977f3766:


    th "¡Correcto, si eso es realmente fácil!{w} ¡Esas cosas ocurren cada día!"


translate spanish day2_main1_7c803d3e:


    "Eché un vistazo más de cerca a ese extraño y sólo entonces... ¡me di cuenta de que en realidad era yo!"


translate spanish day2_main1_473252f1:


    "Solamente que no era mi yo de hoy, tal vez el yo entre los años del instituto y la universidad."


translate spanish day2_main1_bc978da0:


    th "Bueno, al menos eso es algo."


translate spanish day2_main1_1e928d66:


    th "Ahí va, la {i}persona en apuros{/i} no se dio cuenta de que el elefante estaba en la habitación."


translate spanish day2_main1_78884d1e:


    th "Pero la líder del campamento se dio cuenta, y la noche pasada me dijo que dejara de dirigirme a ella sin el debido respeto..."


translate spanish day2_main1_e3987898:


    th "¡Ah, déjalo!"


translate spanish day2_main1_cbe4d96c:


    th "Dudo que mi apariencia afecte en algo."


translate spanish day2_main1_c20955fe:


    "Si el reloj no estaba mintiendo, el desayuno ya estaba perdido."


translate spanish day2_main1_07623b09:


    th "Oh bueno, trataré de encontrar algo en la cantina."


translate spanish day2_main1_b1e4da72:


    th "Funcionó bastante bien ayer con Slavya, ¿no?"


translate spanish day2_main1_0a24a340:


    "Estos recuerdos me hicieron sonreír involuntariamente."


translate spanish day2_main1_86af3570:


    "El sol estaba brillando fuera, una brisa suave estaba soplando."


translate spanish day2_main1_a7b6773f:


    th "Un hermoso día de verano."


translate spanish day2_main1_d9a6ffae:


    "No me sentía tan bien por la mañana desde hacía años."


translate spanish day2_main1_6597a8e7:


    "Todos los problemas se fueron, desvanecidos en las nubes que eran blancas como la nieve."


translate spanish day2_main1_be95a8d4:


    "Olga Dmitrievna salió de la nada."


translate spanish day2_main1_b8d13a2a:


    mt "¡Buenos días, Semyon!"


translate spanish day2_main1_32f6d96b:


    me "¡Buenas!"


translate spanish day2_main1_eda54ed8:


    "Sonreí, haciéndolo lo mejor que pude, no importaba cómo, mi mañana tenía que ser ciertamente buena."


translate spanish day2_main1_53e06c95:


    mt "Llegaste ayer, por lo que decidí no despertarte, pero el desayuno..."


translate spanish day2_main1_d1895f30:


    mt "¡Olvídalo! ¡Ten, coge esto!"


translate spanish day2_main1_71894711:


    "Me dio algo envuelto en papel."


translate spanish day2_main1_db4f4b37:


    "A juzgar por las manchas de aceite, tenían que haber sándwiches dentro."


translate spanish day2_main1_8aa9a83a:


    me "¡Ah, gracias!"


translate spanish day2_main1_641d577e:


    mt "¡Ahora ve a ducharte!"


translate spanish day2_main1_385627a7:


    "Iba a salir."


translate spanish day2_main1_22a42eb2:


    mt "Espera un segundo."


translate spanish day2_main1_66afc59e:


    "Olga Dmitrievna volvió rápidamente dentro de la casa y volvió para dejarme en mis manos una pequeña bolsa."


translate spanish day2_main1_9c319d40:


    "Dentro de ella encontré un cepillo de dientes, jabón, una pequeña toalla y otra cosa... No lo miré bien."


translate spanish day2_main1_29289d61:


    mt "¡Un pionero debería estar siempre limpio y pulcro!"


translate spanish day2_main1_1d1b2ba8:


    mt "Por ser la primera vez déjame ponerte correctamente el pañuelo, el tuyo está colgando deshecho.{w} Deberías hacértelo tú solo una vez hayas aprendido cómo."


translate spanish day2_main1_66e9a1e6:


    me "¿Tenemos que hacerlo? Ahora me voy a asear."


translate spanish day2_main1_4a476f09:


    th "Claro, está bien, podría engancharse en el grifo y estrangularme..."


translate spanish day2_main1_e511e399:


    mt "Bueno, luego pues.{w} Y no te olvides de ir a formar."


translate spanish day2_main1_8eba6c7d:


    th "Lápices, papeles, formarse estudiando...{w} ¡No olvides esas cosas!"


translate spanish day2_main1_a25f95a7:


    me "¿Qué formación?"


translate spanish day2_main1_41981848:


    mt "¡¿Qué quieres decir con «qué formación»?!"


translate spanish day2_main1_823e3ce8:


    "Ella frunció el ceño."


translate spanish day2_main1_ebb2ca84:


    mt "¡Es lunes hoy!"


translate spanish day2_main1_5afc6ea3:


    th "Qué raro, mis aproximaciones me decían que era domingo."


translate spanish day2_main1_769920ed:


    th "Por otra parte, un cambio en el día de la semana no era la peor cosa."


translate spanish day2_main1_a24cdacb:


    mt "Normalmente tenemos que formar bien pronto por la mañana, antes de desayunar; pero es lunes hoy, por lo que hoy la tenemos a las 12 en punto."


translate spanish day2_main1_a89a2c76:


    mt "¡No llegues tarde!"


translate spanish day2_main1_0de6c041:


    me "De acuerdo. ¿Pero dónde?"


translate spanish day2_main1_a56c3145:


    mt "En la plaza, ¡¿dónde si no?!"


translate spanish day2_main1_49afcaa3:


    "No había motivos para discutir."


translate spanish day2_main1_7ca9038d:


    "Me dirigí hacia el «lugar de aseo»."


translate spanish day2_main1_909fa19e:


    "Ya sabía que podía olvidarme de duchas y lavabos separados, pero al ver ésta parafernalia del socialismo decadente... una divertida tortuga con un caparazón de hojalata, montones de patas de grifos y un vientre de cerámica... me puse malo."


translate spanish day2_main1_fc6f68e6:


    "No era una persona delicada, aun así, estando ahí, me di cuenta de que había un mínimo de nivel de comodidad normal, que encuentro molesto prescindir de ella."


translate spanish day2_main1_201b58e0:


    "Es frecuente que... cuando pierdes algo que piensas que era ordinario y común, de repente comprendes cómo de esencial era."


translate spanish day2_main1_fc32f6b3:


    th "¡Ah, déjalo! Como si tuviera opción."


translate spanish day2_main1_e5cc3b97:


    "El agua estaba congelada."


translate spanish day2_main1_7a2b0aea:


    "Mientras me limpiaba las manos no era un problema, al limpiar la cara y mi boca se volvió un mayor problema."


translate spanish day2_main1_9d2484de:


    "No había pasta de dientes en la bolsa, la que me dio Olga Dmitrievna."


translate spanish day2_main1_13ab8033:


    "Podía frotar mis dientes sin ella, pero había una pequeña caja circular envuelta con la toalla."


translate spanish day2_main1_331eedf5:


    "«Polvo para dientes»."


translate spanish day2_main1_60d90a2e:


    th "¡Genial! Un punto para mi por estar en algún lugar del pasado."


translate spanish day2_main1_43dc525a:


    "Me limpié bastante rápidamente, también porque el agua estaba congelada."


translate spanish day2_main1_86c7f6e3:


    "Alguien se estaba acercando rápidamente, o más bien corriendo hacia mi."


translate spanish day2_main1_47eaf14e:


    "Me giré."


translate spanish day2_main1_644b2c05:


    "Era Slavya vestida con ropa de deporte."


translate spanish day2_main1_2984b340:


    "Esta muchacha se veía probablemente bien en cualquier cosa: el uniforme de pionera, en bikini, y, seguramente, incluso en un traje espacial."


translate spanish day2_main1_396d52a7:


    sl "¡Ey!"


translate spanish day2_main1_65d811ca:


    me "Ohayo... Quiero decir, wazzu... ¡Buenos días! Sep..."


translate spanish day2_main1_12f788c2:


    "Oh vaya, qué tacto."


translate spanish day2_main1_0ec1deba:


    sl "¿Cómo es que no veniste a desayunar?"


translate spanish day2_main1_9ac34b39:


    me "Me dormí."


translate spanish day2_main1_a512e5ef:


    "Lo dije como si estuviera orgulloso de ello."


translate spanish day2_main1_531482a7:


    me "Pero Olga Dmitrievna me dejó algunos sándwiches."


translate spanish day2_main1_e3b70370:


    sl "¡Oh, qué bien pues! ¡No te olvides de ir a formar!"


translate spanish day2_main1_85279b68:


    me "Sí, claro."


translate spanish day2_main1_ea0e94f5:


    th "Como si pudiera olvidarme."


translate spanish day2_main1_09688a75:


    sl "Bien, ¡debo correr! ¡Pásatelo bien!"


translate spanish day2_main1_06aca64d:


    "Ella se despidió con su mano y desapareció más allá de la esquina del camino."


translate spanish day2_main1_4ef9a3ba:


    th "Parece que faltan bastantes minutos hasta la formación."


translate spanish day2_main1_a1bcef4b:


    "Debería pasarme rápidamente por mi «hogar» para dejar mi bolsa de aseo y comerme los sándwiches, y luego dirigirme a la plaza."


translate spanish day2_main1_8fc4fbf5:


    "Me abalancé sobre la puerta de la cabaña de la líder del campamento para abrirla y meterme con prisa dentro, como si saltara dentro del último vagón de un tren partiendo."


translate spanish day2_main1_8f99475f:


    "No resultó ser la mejor de las ideas, porque dentro hallé a Olga Dmitrievna..."


translate spanish day2_main1_361b5648:


    "¡Quién se estaba cambiando!"


translate spanish day2_main1_989f5b30:


    "Me quedé de piedra en el lugar, tratando de no respirar."


translate spanish day2_main1_6f55abad:


    "Finalmente la líder del campamento se percató de mi."


translate spanish day2_main1_105fc865:


    mt "¡Semyon!"


translate spanish day2_main1_4d49e3f9:


    "Aparté mi mirada inmediatamente."


translate spanish day2_main1_c2b62a94:


    mt "¡¿Has oído hablar de avisar en la puerta?! ¡Ahora sal fuera!"


translate spanish day2_main1_118d6aad:


    th "Claro, eso fue realmente torpe."


translate spanish day2_main1_fa866f87:


    "Aunque me gustó la vista."


translate spanish day2_main1_ee01ae30:


    "Olga Dmitrievna me siguió fuera en un minuto."


translate spanish day2_main1_f47b36ab:


    mt "Ten, coge esto.{w} Ahora es tu hogar también."


translate spanish day2_main1_c3639b5f:


    "Ella mi dio una llave.{w} La puse en mi bolsillo."


translate spanish day2_main1_fcf7f167:


    th "Hogar..."


translate spanish day2_main1_c9d7a8ab:


    th "Por supuesto, si ignoras cómo de fantasmagóricos son los sucesos actuales aquí, este campamento distaba lejos de ser el peor lugar de la Tierra, pero llamarlo hogar..."


translate spanish day2_main1_8bb6d04f:


    th "¡Sólo he pasado un día aquí!"


translate spanish day2_main1_7ab9cde3:


    th "Dudo mucho que me acostumbre."


translate spanish day2_main1_cd306d7c:


    mt "Muy bien, vamos, llegamos tarde."


translate spanish day2_main1_dc3be649:


    me "¿Pero y qué hay de los sándwiches...?"


translate spanish day2_main1_a350f4cb:


    mt "¡Cómetelos por el camino!"


translate spanish day2_main1_9898e071:


    "Estábamos caminando a través de las alineadas cabañas de los pioneros mientras guardaba los sándwiches de jamón, Olga Dmitrievna estaba todo el rato hablando y hablando.{w} ¡No cerraba el pico hablando como una cotorra!"


translate spanish day2_main1_90484daa:


    "Pero no me preocupé de nada más que la comida."


translate spanish day2_main1_38e0aff0:


    mt "¿Comprendido?"


translate spanish day2_main1_b6626098:


    me "¿Ein?"


translate spanish day2_main1_3c5ea71b:


    mt "¡No me estabas escuchando!"


translate spanish day2_main1_559ab9c6:


    me "Lo siento..."


translate spanish day2_main1_ff1825ac:


    mt "¡Hoy es tu primer día en tu nueva vida como pionero!"


translate spanish day2_main1_b2cb81b3:


    mt "¡Y deberías hacerlo lo mejor posible para tener una vida feliz!"


translate spanish day2_main1_88b4ce94:


    me "Ah, ya, claramente..."


translate spanish day2_main1_76079f83:


    mt "¡Lo digo en serio! Un pionero tiene muchos deberes, se le otorgan grandes responsabilidades: ¡participar en el trabajo social, ayudar a los jóvenes y a estudiar, a estudiar y estudiar más!"


translate spanish day2_main1_7f4c0bdf:


    mt "Estamos aquí como una gran familia.{w} Y formarás parte de ella."


translate spanish day2_main1_3f18fd02:


    th "Sep, parte...{w} Incluso firmaria una tarjeta de miembro del Partido, si así me pudiera salvar de escucharle esas tonterías."


translate spanish day2_main1_126fe430:


    mt "Espero que una vez terminado tu plazo aquí, guardes los más satisfactorios recuerdos de nuestro campamento."


translate spanish day2_main1_c17a76a5:


    mt "¡Recuerdos que perdurarán para toda la vida!"


translate spanish day2_main1_9b154f85:


    me "¿Y cuándo terminará ese plazo?"


translate spanish day2_main1_3b76cadb:


    mt "¿Por qué me sigues haciendo preguntas absurdas?"


translate spanish day2_main1_adbfd80d:


    th "Parecía que no iba a conseguir ninguna información de ella."


translate spanish day2_main1_c6e6c275:


    "Una pena.{w} Este mundo resulta tan simpático, pero nunca se molestó en presentarse {i}a sí mismo{/i}."


translate spanish day2_main1_edbe22f5:


    th "Tal vez, ¿ahora me tomo las cosas más a la ligera que ayer?"


translate spanish day2_main1_cc8bfc43:


    "Parece que tenemos algún tipo de silenciosa tregua sobre {i}ello{/i}... ya que {i}ello{/i} no trata de herirme... pero me está prohibido hacer preguntas."


translate spanish day2_main1_b692bf4b:


    "Por supuesto, esta situación no es cómoda pero, ¿qué puedo hacer con ella?{w} Una mala paz es mejor que una buena disputa."


translate spanish day2_main1_bfb454f3:


    mt "La cosa más importante para ti ahora... es hacer lo mejor posible durante el tiempo que pases aquí."


translate spanish day2_main1_8e4b3ee3:


    me "Lo haré lo mejor que pueda."


translate spanish day2_main1_760779dc:


    "Sinceramente, estaba bastante cansado de esta conversación."


translate spanish day2_main1_cdb44710:


    "¡Sería bueno saber dónde está ese «aquí»!{w} Pero..."


translate spanish day2_main1_5fdbda0d:


    "Llegamos a la plaza."


translate spanish day2_main1_bf8d48ab:


    "Los pioneros ya estaban formando."


translate spanish day2_main1_69eaae74:


    me "¿Qué, aun hay alguien que no esté aquí?"


translate spanish day2_main1_d5cf567b:


    mt "Ehmm, no. Ya están todos."


translate spanish day2_main1_f65725c8:


    "Miró a sus valientes tropas pioneras."


translate spanish day2_main1_e4178ce3:


    mt "Muy bien, formad."


translate spanish day2_main1_2240ebeb:


    th "Raro.{w} ¿Por qué me dijo que no hay más lugares para dormir?"


translate spanish day2_main1_73025a75:


    "Mientras nuestra jefa iba diciendo sus planes para esta semana, me fijé en la gente."


translate spanish day2_main1_6af3ef8e:


    "Unas cabezas más allá de mi estaba Electronik, un poco más allá... Lena y Slavya, al final de la formación... Ulyana y Alisa."


translate spanish day2_main1_80439bd7:


    th "Todos los que conocí estaban aquí."


translate spanish day2_main1_d2e34ec7:


    "Olga Dmitrievna habló sobre alguna competición, y presté mi atención hacia el monumento."


translate spanish day2_main1_0c15405c:


    th "«Genda»..."


translate spanish day2_main1_89f74fc0:


    "No podía recordar ningún revolucionario con semejante nombre."


translate spanish day2_main1_49c36f8b:


    "Tenía también una postura extraña... como si mirara alrededor con recelo, tal vez con desprecio o incluso con desdén."


translate spanish day2_main1_ae1cb199:


    th "Probablemente algún líder local..."


translate spanish day2_main1_b6ab0cdc:


    sl "¿Soñando despierto otra vez?"


translate spanish day2_main1_cc0015de:


    "Slavya me retornó a la realidad."


translate spanish day2_main1_0ceaa470:


    "Olga Dmitrievna permanecía cerca."


translate spanish day2_main1_31af9f80:


    mt "¿Todavía recordáis el plan para esta semana?"


translate spanish day2_main1_b54906d2:


    me "¿El plan?{w} ¡El plan que nunca voy a olvidar!"


translate spanish day2_main1_003878d1:


    mt "¡Perfecto!"


translate spanish day2_main1_a7925aca:


    "Ella miró a Slavya."


translate spanish day2_main1_0e306996:


    mt "¿Lo habéis entendido?"


translate spanish day2_main1_8b01f8be:


    sl "Sí."


translate spanish day2_main1_b8640c11:


    "Slavya me dejó un pedazo de papel."


translate spanish day2_main1_5c374a56:


    mt "Es una lista. Hay cuatro lugares a marcar con visitas. Házlas todas hoy."


translate spanish day2_main1_ffebe5e8:


    mt "Antes de que comiences, inscríbete en un club. Hay unos cuantos clubs en el edificio de clubs y un club de música en un edificio aparte."


translate spanish day2_main1_76437f4d:


    mt "Luego visita la enfermería."


translate spanish day2_main1_1d88b658:


    mt "Y finalmente, visita la biblioteca."


translate spanish day2_main1_f2150eaa:


    mt "¿Lo entendiste?"


translate spanish day2_main1_fc30fd2b:


    me "Sí."


translate spanish day2_main1_a43db633:


    "La lista parecía una buena oportunidad para buscar algo por ahí fuera, ya que tendría que ir a lugares en los que no he estado antes."


translate spanish day2_main1_d0722a98:


    mt "Pues vamos venga, empecemos ahora mismo."


translate spanish day2_main1_abacb833:


    me "¿Pero cuándo y cómo?"


translate spanish day2_main1_794c528c:


    mt "¡No te preocupes! Te traeré más sándwiches. ¡La lista es más importante!"


translate spanish day2_main1_1ef3d9f1:


    sl "Buena suerte."


translate spanish day2_main1_167e9f8c:


    "Se alejaron de mi rápidamente, lejos con tal de no preguntarles nada más."


translate spanish day2_main1_6866c9c8:


    th "Me perdí el desayuno, ahora me perderé la comida también."


translate spanish day2_main1_ce0b356d:


    th "Esto no puede ser bueno."


translate spanish day2_main1_d2a48ffa:


    th "¿Quizá me las apaño a tiempo?"


translate spanish day2_main1_bb3dc1b6:


    th "La comida empieza a la una del mediodía.{w} Por lo que si voy allí podría llegar a perderme algún lugar de la lista."


translate spanish day2_main1_7a51eb9a:


    th "Vale, ¡es demasiado pronto para ir a la cantina de todas maneras!"


translate spanish day2_musclub_5ea37963:


    "El club de música, un edificio de una planta, estaba ubicado a cierta distancia de los otros edificios del campamento."


translate spanish day2_musclub_69443bca:


    "Abrí la puerta y entré sin dudarlo."


translate spanish day2_musclub_8cb5bcfe:


    "Aquí habían suficientes instrumentos para una orquestra entera: las baterías, las guitarras e incluso un piano."


translate spanish day2_musclub_aa3ba673:


    "Estuve mirando con detalle durante un rato cada instrumento, tratando de suponer de qué época eran, pero repentinamente escuché un ruido de algo arrastrándose debajo del piano."


translate spanish day2_musclub_babe6137:


    th "Una muchacha. Parecía que estaba buscando alguna cosa."


translate spanish day2_musclub_baf9ce11:


    "Ella permanecía con sus cuatro extremidades en una sugerente posición, al principio dudé en hablarle."


translate spanish day2_musclub_559ab9c6:


    me "Discúlpame..."


translate spanish day2_musclub_0fe0c850:


    mip "¡Aaaah! ¿Quién está ahí?"


translate spanish day2_musclub_4391282c:


    "Trató de ponerse en pie pero la parte inferior del piano se lo impidió."


translate spanish day2_musclub_250ae924:


    mip "¡Aauuh!"


translate spanish day2_musclub_645f668d:


    "Se esforzó por salir."


translate spanish day2_musclub_cc2dd0eb:


    me "Lo siento, te he asustado..."


translate spanish day2_musclub_021d4634:


    mip "¡No es nada! Oh, tienes una lista, ¿eres nuevo por aquí?"


translate spanish day2_musclub_ec29382e:


    me "¿Ah? Sí."


translate spanish day2_musclub_46ce9eb5:


    mip "Mi nombre es Miku."


translate spanish day2_musclub_2f3d5d1f:


    mi "No, de verdad, ¡lo digo en serio! Nadie me cree, es mi nombre de verdad. Mi madre es japonesa. Mi padre la conoció cuando estaba construyendo... Bueno no era él quien estaba construyendo, él es un ingeniero..."


translate spanish day2_musclub_9fae511d:


    mi "¡Y él estaba trabajando en una planta energética nuclear de allí! O una presa... ¿O un puente...? ¡Lo que sea!"


translate spanish day2_musclub_222c3455:


    "Hablaba tan rápido que se comía la mitad de las palabras que trataba de pronunciar."


translate spanish day2_musclub_ec7942bc:


    me "Soy Semyon."


translate spanish day2_musclub_d2c6efe6:


    mi "¡Genial! ¿Quieres unirte a nuestro club? Ahora solamente estoy yo aquí, ¡pero contigo seremos dos entonces! ¿Sabes tocar algo?"


translate spanish day2_musclub_00ccccca:


    "Fue cuando me volví antisocial que compré una guitarra y aprendí algunos acordes, pero luego lo olvidé... como todo lo que dejo por requerir más de un par de horas para aprenderlo."


translate spanish day2_musclub_dd80a4a7:


    me "Ya sabes, realmente no tenía planeado hacer nada de eso..."


translate spanish day2_musclub_946e96fa:


    mi "¡Oh vale, te enseñaré! Tal vez una trompeta, ¿por ejemplo? ¿Qué tal un violín? ¡Los conozco todos, sinceramente!"


translate spanish day2_musclub_b2395a19:


    "No tenía ningún sentido discutir con una muchacha de orquestra, ya que me esperaba otro torrente de palabras para acribillarme."


translate spanish day2_musclub_3783ff78:


    me "Ey, me lo pensaré, ¿puedes firmarme por ahora?"


translate spanish day2_musclub_b5f95324:


    mi "Sí sí, claro, ¡dámela! ¡Acuérdate de venir! ¡También canto! Te cantaré alguna canción folclórica japonesa. O tal vez si no te va mucho, ¿algo más contemporáneo?"


translate spanish day2_musclub_1c12d36f:


    me "Claaaaaro... gracias, ¡ahora me tengo que ir!"


translate spanish day2_musclub_045fe4ca:


    mi "De acuerdo, ven en cualquier momento..."


translate spanish day2_musclub_f2b7b54b:


    "El final de su enunciado se quedó dentro."


translate spanish day2_musclub_2f7d7510:


    "Puede que sea agradable dejarse caer con la guitarra por la noche, pero en tal compañía..."


translate spanish day2_musclub_2c9e7ab2:


    "Me giré para irme y me encontré cara a cara con Alisa."


translate spanish day2_musclub_db45ba49:


    "Ella me miró sospechosamente."


translate spanish day2_musclub_5c36e901:


    dv "¿Por qué viniste?"


translate spanish day2_musclub_77321a84:


    me "La lista..."


translate spanish day2_musclub_f2265658:


    dv "¿Firmada?"


translate spanish day2_musclub_9513cd87:


    me "Sí..."


translate spanish day2_musclub_6eb94c13:


    dv "¡Entonces muévete!"


translate spanish day2_musclub_d8a05e0a:


    "Alisa se fue dentro, y me di prisa por irme del lugar."


translate spanish day2_clubs_fe574fd8:


    "Me fui al edificio de clubs."


translate spanish day2_clubs_a3b49ee7:


    "A decir verdad, nunca me gustaron las actividades extracurriculares."


translate spanish day2_clubs_352ba995:


    "En la escuela habitualmente encontraba excusas para saltarme las clases extra. En la universidad no tuve interés en participar en el consejo escolar."


translate spanish day2_clubs_2acddae0:


    "No estaba interesado en el boxeo, en aeromodelismo o en costura."


translate spanish day2_clubs_edf42e23:


    "Por lo que vine aquí tan solamente para marcar la casilla de la lista."


translate spanish day2_clubs_7631c2b8:


    "No había nadie."


translate spanish day2_clubs_68cc9679:


    "Me encontré a mi mismo en una especie de refugio de entusiastas en robótica juvenil: había cables y circuitos impresos dispersos por todas partes, chips, y encima de la mesa orgullosamente erguido un oscilógrafo."


translate spanish day2_clubs_58c977db:


    "Escuché voces en otra habitación y entonces dos pioneros aparecieron."


translate spanish day2_clubs_40997bda:


    "Uno era Electronik, el otro no lo conocía."


translate spanish day2_clubs_42afd4b2:


    el "¡Hola, Semyon! Te estábamos esperando."


translate spanish day2_clubs_4d5cf736:


    th "Parece que lo sepa todo sobre todos..."


translate spanish day2_clubs_3dbd6055:


    me "¿Cómo es que me estábais esperando?"


translate spanish day2_clubs_9069fff4:


    el "Bueno porque has venido para inscribirte en nuestro club de cibernética, ¿no es así?"


translate spanish day2_clubs_1a229aa6:


    "No me dejó responderle."


translate spanish day2_clubs_3db9f783:


    el "Y este es Shurik, ¡él es el responsable de aquí!"


translate spanish day2_clubs_b5112857:


    me "¿Asumo que sólo sois vosotros dos en el club?"


translate spanish day2_clubs_f6f084ef:


    el "Bueno, se puede decir que ahora somos tres."


translate spanish day2_clubs_c4b2274f:


    "Shurik se me acercó y me ofreció la mano con confianza."


translate spanish day2_clubs_75eec175:


    "Su rostro era un tanto familiar."


translate spanish day2_clubs_93f251d0:


    sh "¡Bienvenido al Club!"


translate spanish day2_clubs_751ac511:


    me "Sep..."


translate spanish day2_clubs_b9d16cf3:


    el "Ahora, ¡te mostraré todo!{w} Haz como si estuvieras en tu casa."


translate spanish day2_clubs_4b6d8fbc:


    me "Ehmm, muchachos, sólo quería decir que..."


translate spanish day2_clubs_e600b6dc:


    sh "Siempre damos la bienvenida a nuevos miembros."


translate spanish day2_clubs_ac94a513:


    "Lo dijo de una forma que de repente me vino el himno de la Unión Soviética sonando en mi cabeza."


translate spanish day2_clubs_8aaf9ff7:


    "Es asombroso, incluso recordé las palabras... en el primer grado tenía un cuaderno con el texto en la parte posterior."


translate spanish day2_clubs_c82f982c:


    me "Uhmm, no, solamente quiero dejar firmada mi lista."


translate spanish day2_clubs_4a94d273:


    el "Sep, primero te inscribes al club y luego te firmamos la lista."


translate spanish day2_clubs_1438f764:


    "Él sonrió."


translate spanish day2_clubs_01f5a268:


    "Estaba a punto de empezar un largo y aburrido argumento, pero entonces escuché a alguien entrar."


translate spanish day2_clubs_63509643:


    "Miré atrás y vi a Slavya."


translate spanish day2_clubs_131a6aad:


    sl "¡Ah, Semyon! Espero que no te estén dando ningún problema..."


translate spanish day2_clubs_cf9000b4:


    "Estrechó sus ojos, mirando al futuro de la industria robótica de la madre patria."


translate spanish day2_clubs_f47d1028:


    sl "Conozco a ésos dos... ¡pueden darte algunos problemas!"


translate spanish day2_clubs_705bab2b:


    me "Bueno, ya sabes, en realidad sólo necesito tener firmada la lista..."


translate spanish day2_clubs_f0bcf20a:


    "Decidí tomar ventaja de esta situación."


translate spanish day2_clubs_cdedcb52:


    sl "No hay problema, dámela."


translate spanish day2_clubs_094e2e65:


    "Slavya cogió el papel y se aproximó a Shurik."


translate spanish day2_clubs_ec58ff09:


    sl "¡Fírmalo!"


translate spanish day2_clubs_204589a0:


    sh "Espera, aun no hemos hecho..."


translate spanish day2_clubs_0e869d11:


    sl "¡Házlo! ¡Fírmalo ahora!"


translate spanish day2_clubs_8ceef228:


    "Ella le hizo tal mirada intimidatoria a Shurik que le quitó cualquier posible excusa."


translate spanish day2_clubs_845789bc:


    "Escribió algunos garabatos en la lista, y se lo agradecí a Slavya, luego me fuí con muy buen humor."


translate spanish day2_library_89cf3d9c:


    "En realidad, me encanta leer... pero pasar mis días en una biblioteca bajo las actuales circunstancias estaba bien lejos de mi alcance."


translate spanish day2_library_651b3815:


    "Mejor que me dé prisa con esta «lista»."


translate spanish day2_library_3113f296:


    "Cuando entré dentro, un recuerdo de mi infancia emergió en mi cabeza."


translate spanish day2_library_dc667c83:


    "Era muy vivo."


translate spanish day2_library_c1f61108:


    "Tengo unos siete u ocho años, estamos en la biblioteca con mi madre."


translate spanish day2_library_c0052ab2:


    "Mientras ella está mirando entre los libros que pudiera necesitar para mis estudios, yo estoy sentado en la esquina mirando entre la colección de cómics."


translate spanish day2_library_740b504e:


    "Entonces no sabía por qué tenían tantos y por qué yo no podía llevarme algunos conmigo."


translate spanish day2_library_10b93d8b:


    "El concepto de propiedad colectiva era algo que mi mente no podía captar aun en aquella edad."


translate spanish day2_library_71daf897:


    "Aun así, ya entonces todo el concepto de propiedad era confuso para mi."


translate spanish day2_library_26efdcce:


    "Ese recuerdo pareció incluso más extraño ahora, mientras permanecía en este campamento peculiar, donde podrían habérselas apañado para hacer el comunismo «en tres años»."


translate spanish day2_library_faaf4baa:


    "La simbología soviética estaba por todas partes, las estanterías estaban llenas de literatura relacionada."


translate spanish day2_library_dd1940c8:


    "Por supuesto no planeaba leerme ninguno de esos. Llegar a conocer toda la colección de libros de Marx era la última cosa que pensaría en la Tierra."


translate spanish day2_library_86d9517e:


    th "¿Dónde está la bibliotecaria?"


translate spanish day2_library_a5dd78db:


    "No invertí mucho tiempo buscándola."


translate spanish day2_library_075aa233:


    "Miré fijamente.{w} Cabello corto, unas gafas de montura gruesa, un rostro bastante bonito."


translate spanish day2_library_74556d4c:


    "Estaba roncando tan tranquilamente, que no me atreví a despertarla."


translate spanish day2_library_4112cbd4:


    th "Puedo esperar. Si ella no se despierta en media hora, entonces quizá..."


translate spanish day2_library_cdaec1c2:


    "No podia simplemente sentarme ahí, por lo que cogí un libro de manera aleatoria de la estantería más cercana."


translate spanish day2_library_48a28d8c:


    "Arthur Schopenhauer, «El Mundo como Voluntad y Representación»."


translate spanish day2_library_78cf63d0:


    "Lo abrí brúscamente a la mitad y empecé a leer:"


translate spanish day2_library_d7ca3dbf:



    nvl clear
    "La vida de un hombre, con su eterno cuidado, quiere y sufre, se debe observar como la explicación y la paráfrasis del acto de procreación, por ejemplo, la absoluta reivindicación de la voluntad de vivir; y es más, es también la razón natural por la que el hombre debe morir y piensa con ansiedad sobre este deber."


translate spanish day2_library_cc4bf7fc:


    "¿No es ésta la evidencia de que nuestra existencia implica culpabilidad?"


translate spanish day2_library_26562ac7:


    "En cualquier medida, siempre existimos, de época en época pagando con mortalidad nuestros nacimientos, nosotros siempre existimos y alternativamente portamos todo nuestro júbilo y melancolía de la vida, ya que ninguno de los dos puede sucedernos sin efecto alguno... ése es el resultado de nuestra voluntad para vivir."


translate spanish day2_library_4cfe9a45:



    nvl clear
    "Así el miedo a la muerte, la cual a pesar de todas las miserias de la vida nos mantiene firmemente a ella, es realmente ilusorio; pero tan ilusorio como lo es el impulso que nos ha atraído a ello."


translate spanish day2_library_0af732d3:


    "Dicha atracción por sí misma puede verse objetivamente como las mutuas largas miradas de dos amantes; son la expresión más pura de la voluntad de vivir en su afirmación. ¡Qué dulce y tierno es!"


translate spanish day2_library_a5a1d951:


    "Quieren felicidad y mucho placer, y tiernas alegrías para sí mismos, para otros, para todos."


translate spanish day2_library_7e8c00bf:


    "Alguien llamó a la puerta."


translate spanish day2_library_919288d5:


    "Cerré el libro rápidamente y lo puse en su lugar."


translate spanish day2_library_a7a56164:


    th "Que hábito más agradable... llamar a la puerta.{w} Debería acostumbrarme a eso."


translate spanish day2_library_4432512e:


    "Era Lena."


translate spanish day2_library_adda1c0b:


    un "Oh..."


translate spanish day2_library_1a8f3935:


    me "¡Hola!"


translate spanish day2_library_2f04a7f9:


    "Sonreí."


translate spanish day2_library_ca64d35c:


    un "Hola, yo, ehm, solamente quería retornar un libro..."


translate spanish day2_library_04118915:


    "Ella tenía «Lo que el viento se llevó», el que vi ayer."


translate spanish day2_library_b946894f:


    un "Oh, Zhenya está durmiendo, volveré más tarde..."


translate spanish day2_library_6ec572fc:


    mz "Estoy despierta."


translate spanish day2_library_50100392:


    "Me giré sorprendido hacia ella."


translate spanish day2_library_3e33ac87:


    "Ella me observó fijamente detrás de su mesa."


translate spanish day2_library_4b484fa7:


    mz "¿Qué es lo que querías?"


translate spanish day2_library_51266558:


    me "Necesito que me firmes aquí..."


translate spanish day2_library_c7f48bc6:


    mz "Déjalo aquí."


translate spanish day2_library_486518bc:


    "Rápidamente firmó el papel y me lo devolvió."


translate spanish day2_library_eb72bb13:


    "Tenía esa apariencia en su rostro que me hacía querer quedarme tranquilo."


translate spanish day2_library_9db07b02:


    "Lena se acercó a ella para retornar el libro, le di las gracias a Zhenya y me fui."


translate spanish day2_aidpost_30420f41:


    th "¿Qué sentido tiene visitar la enfermería?"


translate spanish day2_aidpost_37fed0c1:


    "Mi salud estaba bien, el aire fresco de aquí claramente me hizo algo bueno... Me sentía mejor de lo habitual."


translate spanish day2_aidpost_513b3994:


    th "Pero tenía que visitarla."


translate spanish day2_aidpost_92c7a992:


    "Entré."


translate spanish day2_aidpost_b0f75288:


    th "Una enfermera común, como mi doctor de cabecera."


translate spanish day2_aidpost_cbdb3f5c:


    "Una mujer de edad media estaba sentada en la mesa.{w} Obviamente, ella era una enfermera."


translate spanish day2_aidpost_7b8718b9:


    "Me miró, atenta y calculadoramente, mientras continuaba escribiendo alguna cosa."


translate spanish day2_aidpost_7cce673f:


    csp "Bueno, hola... pionero."


translate spanish day2_aidpost_f14de383:


    "Dijo la enfermera sin ser distraída de su trabajo."


translate spanish day2_aidpost_b5fe60d8:


    me "Buenas tardes, señora... Tengo algo..."


translate spanish day2_aidpost_a3450ccf:


    csp "Siéntate, por favor."


translate spanish day2_aidpost_9fa1bef7:


    "Miré alrededor de la habitación."


translate spanish day2_aidpost_40098e3a:


    csp "En la camilla."


translate spanish day2_aidpost_04d910fd:


    "Me senté."


translate spanish day2_aidpost_89b67c83:


    csp "Desnúdate."


translate spanish day2_aidpost_a9281d57:


    "Ella decía todo esto con una voz normal y corriente."


translate spanish day2_aidpost_f43fc4fb:


    me "¿Para qué...?"


translate spanish day2_aidpost_9e007bed:


    csp "Para inspeccionar, para auscultarte y otras comprobaciones varias ya sabes."


translate spanish day2_aidpost_fa5c927d:


    csp "Por cierto, mi nombre es Violetta, pero puedes llamarme Viola."


translate spanish day2_aidpost_548e0299:


    "Se giró hacia mi."


translate spanish day2_aidpost_d4984f4c:


    cs "¿A qué estás esperando? Desnúdate."


translate spanish day2_aidpost_f4cd982b:


    me "Pero si no tengo problemas de salud. Tengo esto..."


translate spanish day2_aidpost_c2002e32:


    "Con cuidado le di el papel."


translate spanish day2_aidpost_c5b50119:


    cs "Más tarde."


translate spanish day2_aidpost_116c6a89:


    "Cogió el estetoscopio de su cuello y parecía que pretendiese diseccionarme con él."


translate spanish day2_aidpost_89132ea0:


    "Pero entonces alguien llamó a la puerta."


translate spanish day2_aidpost_1128b7a1:


    "La enfermera respondió a regañadientes:"


translate spanish day2_aidpost_2e8aea9e:


    cs "¡Entra!"


translate spanish day2_aidpost_75d12839:


    "En un instante la puerta se abrió ampliamente y Elektronik entró con prisa."


translate spanish day2_aidpost_7220ee64:


    el "¡Hola! Yo... me he caído durante el partido de fútbol. Es una tontería, por supuesto, me encuentro bien, pero Olga Dmitrievna..."


translate spanish day2_aidpost_5741b364:


    "Había un moratón bien grande en el ojo de Elektronik."


translate spanish day2_aidpost_efefe224:


    th "Dudo mucho que eso pudiera ser una herida de fútbol."


translate spanish day2_aidpost_13a0ecdb:


    cs "Siéntate, le echaré un vistazo."


translate spanish day2_aidpost_07c0e46a:


    "Le dijo a él."


translate spanish day2_aidpost_8855685b:


    cs "Y tú, pásame tu lista."


translate spanish day2_aidpost_d2d6a28b:


    "La enfermera la firmó rápidamente y continuó:"


translate spanish day2_aidpost_42b88054:


    cs "Si algo te duele... ven inmediatamente a mi... pionero."


translate spanish day2_aidpost_159b2e86:


    "Decidí no responder y salí, cerrando la puerta detrás de mí."


translate spanish day2_aidpost_9ff8721b:


    th "La enfermera es claramente algo..."


translate spanish day2_dinner_c6f8dc13:


    "No obstante, decidí ir a comer."


translate spanish day2_dinner_c8dab275:


    th "La lista no se va a ir (la puedo llevar a firmar más tarde), mientras que mis tripas claramente no podrán esperar hasta la cena."


translate spanish day2_dinner_92787784:


    "Con esos pensamientos entré en la cantina."


translate spanish day2_dinner_5f1442eb:


    "No había casi nadie dentro... aparentemente, muchos pioneros ya habían comido."


translate spanish day2_dinner_79c74b45:


    "Una camarera de tamaño impactante me proporcionó una comida gloriosa de tres platos: una sopa «Ypres broth», estofado húngaro de «Lavrentiy Pavlovich» con guarnición de patatas cocidas de acuerdo a la moda más loca del siglo XV y una compota de «Tabla Periódica»."


translate spanish day2_dinner_3f63379f:


    "Un restaurante de cinco estrellas envidiaría tal menú, pero estaba demasiado hambriento para preocuparme."


translate spanish day2_dinner_0327cad9:


    "Después de todo, comparado con mi comida habitual de ramen o de mac&quesos, esto no era por supuesto tan malo."


translate spanish day2_dinner_a677d27c:


    "Me senté detrás de la mesa más cercana y me concentré en masticar."


translate spanish day2_dinner_a5341529:


    "Mi concentración pronto fue distraída por una repentina fuerte palmada en mi espalda. Casi me ahogué."


translate spanish day2_dinner_0d703f5f:


    "Ulyana estaba delante de mí, con aspecto de victoria dibujada en su cara."


translate spanish day2_dinner_8de67373:


    me "¡Un día te voy a ahorcar!"


translate spanish day2_dinner_d8dece45:


    us "¡Cógeme si puedes!"


translate spanish day2_dinner_b4cfaed8:


    "¡Ella sacó su lengua fuera!"


translate spanish day2_dinner_0a675d44:


    us "Lo intentaste una vez y no pudiste."


translate spanish day2_dinner_abd93e7a:


    me "Muy bien, ¡pues te haré una emboscada por ahí!"


translate spanish day2_dinner_57a622de:


    us "¡Eso no es justo!"


translate spanish day2_dinner_fccbe21c:


    me "Mira quién fue a hablar, ¡la justa!"


translate spanish day2_dinner_71fc19fd:


    "Sonreí."


translate spanish day2_dinner_e00339d0:


    us "Vale, espera, voy a coger algo de comida y vuelvo, comeremos juntos."


translate spanish day2_dinner_822ea040:


    "Podría hacerlo sin semejante compañía. Mejor me doy prisa con mi comida."


translate spanish day2_dinner_2d5c9aae:


    "Sin embargo Ulyana volvió sólo medio minuto después."


translate spanish day2_dinner_d8e82882:


    "Llevaba una gran carne asada en su plato y unas pocas patatas hervidas."


translate spanish day2_dinner_b90cc3bd:


    "Comparado con mi «manjar»..."


translate spanish day2_dinner_bff8415e:


    me "¿Cómo lo has... dónde lo has conseguido?"


translate spanish day2_dinner_2da2c81a:


    us "¡Hay que saber lugares!"


translate spanish day2_dinner_1d43f87e:


    "Ella me miró y me hizo una sonrisa con sus 32... o cuantos tenga... de dientes."


translate spanish day2_dinner_d05806b0:


    th "¡No me lo creo!"


translate spanish day2_dinner_47797bcf:


    "Nunca fui un gran bromista, y en la escuela normalmente solía encontrarme del lado del que las padecía."


translate spanish day2_dinner_e27a02d2:


    "Pero me tengo que volver contra ella de alguna forma."


translate spanish day2_dinner_5dafbcae:


    me "¿Y si Olga Dmitrievna sabe que robas comida?"


translate spanish day2_dinner_d7d48f29:


    us "¡No la estoy robando!"


translate spanish day2_dinner_1678d751:


    "Se encendió."


translate spanish day2_dinner_fc822a9d:


    me "Eso será lo que le vas a contar a ella. Me pregunto si te creerá..."


translate spanish day2_dinner_88a1ff3e:


    us "¡¿Y cómo lo sabría ella?!"


translate spanish day2_dinner_911579da:


    me "Bueno... eso depende... de muchas cosas."


translate spanish day2_dinner_ac96beb1:


    us "¿Como qué?"


translate spanish day2_dinner_1b595140:


    "Ulyana me miró a los ojos."


translate spanish day2_dinner_25334771:


    me "Tráeme un bollo. ¡Uno dulce!"


translate spanish day2_dinner_47228a37:


    us "¿Dónde se supone que lo voy a conseguir?"


translate spanish day2_dinner_430fbe26:


    me "Bueno, ¿el mismo lugar dónde conseguiste todo eso?"


translate spanish day2_dinner_930035ce:


    "Señalé su plato."


translate spanish day2_dinner_35913bdd:


    "Ella dudó."


translate spanish day2_dinner_b9e1270b:


    us "Está bien. ¡Un bollo!"


translate spanish day2_dinner_0cd3249d:


    us "¡Y prométeme que no se lo contarás a Olga Dmitrievna!"


translate spanish day2_dinner_23e8f891:


    me "¡Tienes la palabra de un pionero!"


translate spanish day2_dinner_0b374dce:


    "Salió corriendo en dirección a la cocina. Sin dudar abrí el pimentero y lo vacié en su bebida de compota."


translate spanish day2_dinner_99986110:


    "Tan pronto como terminé, la inquieta muchacha volvió."


translate spanish day2_dinner_5c02fe6a:


    us "¡Aquí tienes, extorsionador!"


translate spanish day2_dinner_d22005bb:


    th "Parece que no se dio cuenta."


translate spanish day2_dinner_c0146da9:


    me "Vale. Ahora, el último en beberse su compota tendrá que llevarse las bandejas."


translate spanish day2_dinner_07366c5a:


    us "¡No seas tonto!"


translate spanish day2_dinner_7f82c019:


    me "¿Tonto? Mírame."


translate spanish day2_dinner_78abf728:


    us "No voy a jugar a esas criaturadas."


translate spanish day2_dinner_d4d00518:


    me "¡Mira quién fue a hablar!"


translate spanish day2_dinner_6437d74d:


    "Sonreí maliciosamente."


translate spanish day2_dinner_8f15074d:


    us "¡Eh, {i}tú{/i}! ¡Mírame pues! ¡Uno, dos y tres!"


translate spanish day2_dinner_d16b7597:


    "Y así ella no me dio tiempo a coger mi vaso, que en un solo movimiento se la bebió de golpe."


translate spanish day2_dinner_dc6679ad:


    "En tan sólo un segundo su expresión cambió a una de auténtico terror, las mejillas se le pusieron rojas y sus ojos estaban a punto de explotar."


translate spanish day2_dinner_c1908ed9:


    "Saltó de la mesa y se fue corriendo a la zona de bebidas a por agua, hecha un retrato y gritando mientras se iba."


translate spanish day2_dinner_053a22e5:


    us "¡Tú! ¡Tú! Tú..."


translate spanish day2_dinner_b225e29e:


    "Decidí no quedarme a esperarla y me fui de allí con mi risita mientras me terminaba mi bollo."


translate spanish day2_main2_a20cefa7:


    "..."


translate spanish day2_main2_dfe2adc7:


    "Finalmente, tengo todas las firmas. Ahora tenía que volver con Olga Dmitrievna y dárselas."


translate spanish day2_main2_98a12405:


    "Ella estaba sentada delante de su cabaña, leyendo un libro."


translate spanish day2_main2_bb12859e:


    "No parecía ser un buen comportamiento modélico para el pionero perfecto en el que ella pretendía convertirme."


translate spanish day2_main2_281d80a7:


    "Me pregunto si sus responsabilidades se extendían más allá de hacer discursos enardecidos a una formación, regañar a Ulyana e implicarse con mi crecimiento moral, físico e ideológico."


translate spanish day2_main2_482db2cf:


    me "Aquí tienes..."


translate spanish day2_main2_7cce4a44:


    "Le ofrecí mi lista."


translate spanish day2_main2_f913543d:


    "La puso en su bolsillo sin mirar siquiera las firmas."


translate spanish day2_main2_e0ce9392:


    th "¡Genial! Podría haberla firmado sin ir a ninguna parte."


translate spanish day2_main2_7b44c8b1:


    mt "¡Perfecto! Así que... ¿conociste a nuestra enfermera?"


translate spanish day2_main2_9513cd87:


    me "Sí..."


translate spanish day2_main2_ee81ac0c:


    "Por alguna razón, su pregunta me dio escalofríos que recorrieron mi espalda."


translate spanish day2_main2_342a300f:


    mt "¿En qué club te has inscrito?"


translate spanish day2_main2_8ba55fc5:


    me "No lo hice... Me lo tengo que pensar primero."


translate spanish day2_main2_af5748f9:


    mt "Qué pena. ¡Es vital para ti inscribirte en alguno mañana!"


translate spanish day2_main2_02398cce:


    th "¡Por supuesto, claro!"


translate spanish day2_main2_5d6ded1e:


    mt "Bien. Es hora de ir a cenar."


translate spanish day2_main2_b59eddaa:


    th "¡Ya era hora! Empezaba a estar hambriento."


translate spanish day2_main2_4ff192ad:


    "Nos dirigimos hacia la cantina juntos, con Olga Dmitrievna."


translate spanish day2_main2_de8fec0b:


    "Miré hacia el cielo y me percaté de que el sol ya se estaba poniendo."


translate spanish day2_main2_962703d4:


    "En el porche estaban: Alisa, Electronik..."


translate spanish day2_main2_cb3d2e1e:


    "Ulyana y Slavya."


translate spanish day2_main2_30f5e6dc:


    "CUando llegamos, escuché de qué estaban hablando."


translate spanish day2_main2_f04cac09:


    dv "Y nunca me llames Dva-Che jamás, ¡o recibirás otra más!"


translate spanish day2_main2_a8e77ffd:


    el "¡Yo no te llamé así! ¡Estás escuchando cosas que no son!"


translate spanish day2_main2_689622eb:


    us "¡Lo hizo, lo hizo! ¡Lo escuché todo!"


translate spanish day2_main2_4c435994:


    el "¡Si tú no estabas ahí!"


translate spanish day2_main2_ab85c187:


    us "¡Estaba, estaba! ¡En los arbustos!"


translate spanish day2_main2_992ca12a:


    sl "¡Vamos muchachos! ¡Parad!"


translate spanish day2_main2_75a4c8b7:


    th "Así que no era una lesión de fútbol lo que padeció Electronik hoy de madrugada."


translate spanish day2_main2_cdbfd744:


    th "La enfermera hizo un gran trabajo... ¡No podía ver siquiera su ojo morado!"


translate spanish day2_main2_03ada258:


    "Olga Dmitrievna se acercó a ellos y preguntó por el alboroto:"


translate spanish day2_main2_4bd88eb6:


    mt "¿Qué ocurrió?"


translate spanish day2_main2_b07cfe09:


    sl "Alisa y Cheesekov..."


translate spanish day2_main2_ecff4d02:


    dv "¡No hice nada!"


translate spanish day2_main2_6d1704fc:


    "Ella encogió los hombros con antipatía y se fue dentro."


translate spanish day2_main2_f38aed46:


    mt "Está bien. Es la hora de cenar."


translate spanish day2_main2_6c8ce019:


    "Entré el último."


translate spanish day2_main2_84208ced:


    "No había muchas sillas vacías."


translate spanish day2_main2_26bbcb23:


    "Al cruzar la cantina se observaban sillas vacías al lado de Alisa, pero prefiero pasar hambre durante una semana o dos que arriesgar mi cuello estando cerca de ella."


translate spanish day2_main2_c136a510:


    "También había un asiento vacío al lado de Ulyana, pero no soy aficionado a la cocina tradicional china de comer «todo lo que se arrastre»."


translate spanish day2_main2_7c6eeade:


    "Finalmente, una silla vacía al lado de Miku."


translate spanish day2_main2_1a652dc4:


    "Parece que tendré que escoger mi veneno..."


translate spanish day2_main2_8b9ac1bf:


    me "¿Te importa si me siento aquí?"


translate spanish day2_main2_a9efde1c:


    mi "¡Oh sí, claro! ¡Quiero decir no, no me importa! Digo sí, ¡puedes sentarte aquí!"


translate spanish day2_main2_04d910fd:


    "Me senté."


translate spanish day2_main2_6ec2d5c6:


    mi "Mira, hoy toca alforfón. ¿Te gusta? ¡Y pollo! No me gusta el pollo. Bueno, tampoco es que no me guste..."


translate spanish day2_main2_a2e3290c:


    mi "Pero si me preguntas que preferiría, por decir algo, carne de res stroganoff o ragú... No, ¡quizá mejor solo una hamburguesa! ¡O filete de cuarto trasero! ¿Te gusta?"


translate spanish day2_main2_f14d5492:


    me "No soy escrupuloso con mi comida."


translate spanish day2_main2_ea0a7a01:


    "Y esa es la pura verdad."


translate spanish day2_main2_af2e7a5f:


    mi "Oh, entiendo. Pero los postres, ya sabes, no son muy buenos aquí. ¡Me gustan los helados! ¿Te gustan? ¡Me encantan el «48 kopecks» y el «Leningradskoe»! ¡Oh, perdona, siempre estoy hablando de mi misma!"


translate spanish day2_main2_9d46cf08:


    mi "¿Tal vez eres más de frío?"


translate spanish day2_main2_14b91769:


    "La cena empezaba a ponerme de los nervios, gracias a semejante compañía."


translate spanish day2_main2_28655f21:


    "Y no soy ese tipo de persona que simplemente puede ignorar a alguien que te está hablando.{w} Incluso aunque sea ella."


translate spanish day2_main2_c7c2dae1:


    th "Estamos en la misma mesa después de todo."


translate spanish day2_main2_f0878075:


    mi "Ya sabes, una vez compré un cucurucho de gofre y empecé a comérmelo, ¿y sabes qué luego? ¡Me encontré un tornillo en él! Un tornillo de verdad, ¿te lo puedes imaginar? ¿O era una tuerca? No lo sé en realidad."


translate spanish day2_main2_791bb7e0:


    mi "Los tornillos son aquellos que aprietas con un destornillador, y las tuercas son aquellas que las manejas con una llave, ¿verdad?"


translate spanish day2_main2_24441c77:


    th "Si hubiera una competición de comer rápidamente en estos momentos, probablemente estaría entre los tres primeros ahora mismo."


translate spanish day2_main2_6cbc801e:


    me "Bien, me voy a ir, ¡bon appétit!"


translate spanish day2_main2_91330d37:


    "Me levanté y me dirigí fuera."


translate spanish day2_main2_a6b59989:


    "Miku estaba diciendo algo, pero sus palabras se ahogaron entre la multitud de los ruidosos pioneros que cenaban."


translate spanish day2_main2_757dd6ab:


    "Me fui afuera y me senté en las escaleras, esperando que la cena se asentara un poco."


translate spanish day2_main2_bbb07ad8:


    "Simplemente me senté ahí y observé cómo se hizo de noche."


translate spanish day2_main2_a85b2ea6:


    "Aquí todas las cosas estan tan llenas de vida durante el día: críos riendo y gritando de felicidad, bromeando y corriendo por ahí, charlando constantemente, jugando y también nadando en la playa."


translate spanish day2_main2_bf72d174:


    "Pero después con la oscuridad el campamento cambia excepcionalmente."


translate spanish day2_main2_1732123e:


    "Los sonidos del día se transforman con el silencio, solamente ahora con el canto del grillo o por el de un pájaro nocturno se rompe."


translate spanish day2_main2_0a2a1026:


    "El campamento se iba a dormir."


translate spanish day2_main2_c328b2c5:


    "En cada sombra podías ver cosas... ¿Tal vez un fantasma? ¿O un espíritu del bosque? ¿O un animal salvaje? Un ser humano sería la última cosa en aparecer."


translate spanish day2_main2_c347461a:


    "Así es como se veía la pasada noche. Y ahora."


translate spanish day2_main2_2ac17ce1:


    "Los lugareños seguían su rutina muy estrictamente."


translate spanish day2_main2_3a690eee:


    "Durante el día, el campamento era suyo. Por la noche, pertenecía más a las fuerzas de la naturaleza que a los humanos."


translate spanish day2_main2_0b5df81a:


    "Alguien tocó mi hombro."


translate spanish day2_main2_47eaf14e:


    "Miré atrás."


translate spanish day2_main2_49bd4154:


    "Era Electronik."


translate spanish day2_main2_6385785b:


    el "Vamos a jugar a cartas."


translate spanish day2_main2_f8ca8757:


    me "¿Cartas?"


translate spanish day2_main2_6d492a61:


    el "¡Sep! Inventé un juego nuevo. ¡Uno muy bueno!"


translate spanish day2_main2_bfe132d8:


    me "¿Bueno? ¿Como cuál?"


translate spanish day2_main2_ad0877c3:


    el "Bueno, primero tenemos que encontrar las cartas, después ya te lo contaré."


translate spanish day2_main2_3c6dcd82:


    me "Pues ves a buscarlas, ¿cuál es el problema?"


translate spanish day2_main2_2adc32e8:


    el "Solamente Olga Dmitrievna las tiene, y ella no me las dará..."


translate spanish day2_main2_5ac38cf1:


    me "¿Por qué?"


translate spanish day2_main2_6c259ad3:


    el "Bueno, la última vez que nosotros..."


translate spanish day2_main2_0f60fa5f:


    "Olga Dmitrievna y Slavya salieron del porche."


translate spanish day2_main2_8831b761:


    el "¡Olga Dmitrievna! ¡Semyon precisamente quería preguntarte si hay alguna posibilidad de coger el juego de cartas!"


translate spanish day2_main2_09a322d4:


    me "En realidad..."


translate spanish day2_main2_458888bd:


    mt "¿Para qué propósito?"


translate spanish day2_main2_de85b885:


    el "¡Hemos inventado un juego nuevo!"


translate spanish day2_main2_aa6ae640:


    th "Nosotros no, tú."


translate spanish day2_main2_c7f522d1:


    mt "¿Qué juego?"


translate spanish day2_main2_3d77030f:


    el "Necesito las cartas para enseñarlo."


translate spanish day2_main2_ee0157c7:


    mt "Mmm. No me gusta esto...{w} Bueno si Semyon está contigo entonces, quizá, esté bien..."


translate spanish day2_main2_0d6093c8:


    me "Para ser sinceros realmente..."


translate spanish day2_main2_93e80e0a:


    sl "¡Nosotros iremos a buscarlas juntos, Olga Dmitrievna!"


translate spanish day2_cards_with_sl_a755f898:


    me "Si no te importa..."


translate spanish day2_cards_with_sl_547903c7:


    sl "¡Claro! Vamos."


translate spanish day2_cards_with_sl_956c974d:


    "Nos dirigimos hacia mi cabaña."


translate spanish day2_cards_with_sl_2d658ff8:


    "Aproximadamente a medio camino Slavya paró bruscamente."


translate spanish day2_cards_with_sl_ca62c00d:


    sl "¡Ey, me acabo de acordar! Las cartas están en mi cabaña."


translate spanish day2_cards_with_sl_7c2541af:


    th "En buen momento."


translate spanish day2_cards_with_sl_1b37703b:


    me "¿Y dónde está?"


translate spanish day2_cards_with_sl_2015af9e:


    sl "Está justo por ahí abajo por el camino, ¡vamos!"


translate spanish day2_cards_with_sl_a20cefa7:


    "..."


translate spanish day2_cards_with_sl_c8a2cf84:


    "Llegamos a la cabaña que, de hecho, parecía más bien una caravana."


translate spanish day2_cards_with_sl_113ad468:


    sl "Sólo espera un minuto aquí, ¡ahora vuelvo!"


translate spanish day2_cards_with_sl_5fa71576:


    "Tan solamente le llevó unos segundos volver."


translate spanish day2_cards_with_sl_ca656115:


    sl "¡Aquí están!"


translate spanish day2_cards_with_sl_21c26363:


    "Ella me enseñó una baraja de cartas bastante desgastadas."


translate spanish day2_cards_with_sl_24f3bb91:


    me "Deben de estar marcadas aparentemente."


translate spanish day2_cards_with_sl_ccc884da:


    sl "Eso es antideportivo. ¡¿Qué se hizo del juego justo?!"


translate spanish day2_cards_with_sl_74535a0c:


    "Dímelo a mi. Es difícil hacer trampas cuando no sabes las reglas..."


translate spanish day2_cards_with_sl_ad3d2e0c:


    sl "¿Vamos?"


translate spanish day2_cards_with_sl_304c9144:


    me "Vayámonos."


translate spanish day2_cards_with_sl_a20cefa7_1:


    "..."


translate spanish day2_cards_with_sl_5316a3b2:


    "Mientras volvíamos decidí intentar algo para hallar alguna respuesta."


translate spanish day2_cards_with_sl_6f6dbf74:


    me "¿Cuánto tiempo has estado aquí?"


translate spanish day2_cards_with_sl_4a08d89f:


    sl "En este campamento... alrededor de una semana."


translate spanish day2_cards_with_sl_237356a1:


    me "Ya veo... ¿Y de dónde veniste?"


translate spanish day2_cards_with_sl_9cbcee67:


    sl "Soy del norte."


translate spanish day2_cards_with_sl_5fd101b5:


    me "¿Y eso es...?"


translate spanish day2_cards_with_sl_3e5e8c80:


    sl "Un frío norte."


translate spanish day2_cards_with_sl_943eaf3a:


    "Me miró y sonrió."


translate spanish day2_cards_with_sl_a94c6d7f:


    th "Pareciera que nadie en este campamento es propenso a responderme incluso a las más inocentes de mis preguntas."


translate spanish day2_cards_with_sl_9256c52d:


    "Intenté aproximarme desde otro ángulo."


translate spanish day2_cards_with_sl_f2292cea:


    me "¿Y qué es lo que te gusta?"


translate spanish day2_cards_with_sl_63f53767:


    sl "¿Qué quieres decir?"


translate spanish day2_cards_with_sl_1b25eb7c:


    me "Bueno, ¿tus aficiones?"


translate spanish day2_cards_with_sl_710d465d:


    sl "Oh... Me gusta la naturaleza."


translate spanish day2_cards_with_sl_fe5ee6e6:


    "Qué extraño. Hoy no está muy habladora por alguna razón."


translate spanish day2_cards_with_sl_219d4457:


    me "¿Naturaleza? Ya veo.{w} ¿Quieres ser una historiadora de la naturaleza?"


translate spanish day2_cards_with_sl_92f9746d:


    sl "Más bien una historiadora cómun. Siempre estuve interesada en nuestra historia como nación."


translate spanish day2_cards_with_sl_e357f0bd:


    "Eso se ajustaría mucho a ella por supuesto."


translate spanish day2_cards_with_sl_2efeef27:


    "Pareciera que entre todos los lugareños ella fuera la única que no tuviera nada que esconder."


translate spanish day2_cards_with_sl_0a42064b:


    th "¿Y si ella estuviera atrapada aquí como yo y simplemente no pudiera creer en nadie lo suficiente como para explicarlo?"


translate spanish day2_cards_with_sl_300c021f:


    "Traté de explorar esa posibilidad."


translate spanish day2_cards_with_sl_6c6041ec:


    me "¿Y por qué razón escogiste este campamento?"


translate spanish day2_cards_with_sl_a89e29ad:


    sl "No lo hice. Mis padres consiguieron un cupón de su trabajo para mi."


translate spanish day2_cards_with_sl_8105ae57:


    th "Otro error..."


translate spanish day2_cards_with_sl_49976930:


    me "Bueno, ¿y si pudieras elegir?"


translate spanish day2_cards_with_sl_baad4a6d:


    sl "¡Es agradable estar aquí!{w} No creo que escogiera otro lugar si pudiera... ¡Estar aquí es como si fueras otra persona!"


translate spanish day2_cards_with_sl_e9ca641d:


    "No es como yo lo veía."


translate spanish day2_cards_with_sl_d84daee3:


    me "¿Qué quieres decir con ser «otra persona»?"


translate spanish day2_cards_with_sl_b1f7a78f:


    sl "Pues que solamente es como si hubiera muchas posibilidades. ¡Puedes aprender mucho, conocer tanta gente nueva, hay gente tan interesante!"


translate spanish day2_cards_with_sl_fcc6a9dc:


    "Ahora ella comenzaba a parecerse a nuestra jefa, lo que me alarmó bastante."


translate spanish day2_cards_with_sl_30054c86:


    "Decidí dejar de hacer preguntas por ahora."


translate spanish day2_cards_with_sl_58bb1834:


    "Cuando volvimos, Olga Dmitrievna le dijo a Slavya:"


translate spanish day2_cards_with_sl_fc9fa381:


    mt "¡Me acordé de que las cartas estaban en la tuya!"


translate spanish day2_cards_with_sl_9446991e:


    sl "No pasa nada, las tenemos."


translate spanish day2_cards_with_sl_74619cda:


    mt "Bien, bien."


translate spanish day2_cards_without_sl_587eb799:


    me "Voy a buscarlas."


translate spanish day2_cards_without_sl_888bedb3:


    mt "Está bien. Están en mi cabaña, en el cajón del escritorio."


translate spanish day2_cards_without_sl_8b243a91:


    "Me dirigí hacia la cabaña de Olga Dmitrievna."


translate spanish day2_cards_without_sl_9ff4064f:


    th "Aun así, ¿por qué estoy conforme?"


translate spanish day2_cards_without_sl_ef9c0eef:


    th "Y nuevamente, ¿qué opciones tenía?"


translate spanish day2_cards_without_sl_c56c19ea:


    th "No hay nada que hacer aquí por la noche, al menos tendré algo de diversión."


translate spanish day2_cards_without_sl_a5b9da23:


    "Pero mi mente seguía persistiendo en que todo esto no estaba siendo un buen momento para divertirse."


translate spanish day2_cards_without_sl_4158e5dc:


    th "Por otra parte, si {i}aquí{/i} existe un campamento con pioneros... entonces tiene que haber alguien haciéndolo."


translate spanish day2_cards_without_sl_c1286124:


    th "Y aunque no fuera verdad, entonces la cosa más lógica es buscar respuestas aquí dentro, no en los bosques o en los campos."


translate spanish day2_cards_without_sl_23bd994f:


    "Pero ahora mismo en este momento, {i}las respuestas{/i} pueden encontrarse en las cartas."


translate spanish day2_cards_without_sl_cf613096:


    "Abrí la puerta con mi llave y entré."


translate spanish day2_cards_without_sl_09b88332:


    "Para mi sorpresa, no habían cartas dentro."


translate spanish day2_cards_without_sl_956232aa:


    "En su lugar estaba lleno de tazas, tenedores y cuchillos, platos, cinta adhesiva, tijeras, guantes de goma, un trapo, unas pocas bolsas de plástico, lapiceros y unos pocos bolígrafos rotos."


translate spanish day2_cards_without_sl_d96cc0f8:


    "De todo menos las cartas de juego."


translate spanish day2_cards_without_sl_c0fa483e:


    th "¿Quizá debería mirar en el armario?"


translate spanish day2_cards_without_sl_c357b6ff:


    "Estaba repleto de ropa de Olga Dmitrievna, pero el pequeño cajón con una cerradura de arriba me llamó la atención."


translate spanish day2_cards_without_sl_d7abae60:


    "Lo traté de abrir, pero no se abrió."


translate spanish day2_cards_without_sl_394c4aa1:


    th "Me pregunto si ella estará escondiendo algo..."


translate spanish day2_cards_without_sl_a9798457:


    th "Forzarlo para abrirlo no era buena idea, incluso si las cartas estaban allí."


translate spanish day2_cards_without_sl_da208a8e:


    "Me estaba dando la vuelta para irme ya que recordé algo."


translate spanish day2_cards_without_sl_69cc04fb:


    "Slavya olvidó sus llaves ayer. Están aquí."


translate spanish day2_cards_without_sl_505d2b4b:


    th "Y si..."


translate spanish day2_cards_without_sl_7774a647:


    "Por un momento dudé, pero entonces me acerqué al armario y empecé a poner la llave en la cerradura."


translate spanish day2_cards_without_sl_5b37b93a:


    "Por supuesto sería extraño esperar un resultado... positivo, ¿por qué iba Slavya a llevar una llave de los efectos personales de Olga Dmitrievna?"


translate spanish day2_cards_without_sl_87702aa9:


    "Pero para mi sorpresa, otra llave se estaba deslizando en la cerradura y giraba varias veces sin problema."


translate spanish day2_cards_without_sl_bd5226c8:


    "De repente la puerta crujió detrás de mí."


translate spanish day2_cards_without_sl_82b93712:


    "Salté, girándome... pero no había nadie en la cabaña."


translate spanish day2_cards_without_sl_5ffdfa53:


    th "¿Fue el viento...?"


translate spanish day2_cards_without_sl_6e22d9c9:


    "Miré con precaución fuera. No había ni una sola persona a la vista."


translate spanish day2_cards_without_sl_82fc9b6c:


    "Tal vez no fue nada, pero aun así me sentí inquieto."


translate spanish day2_cards_without_sl_a20cefa7:


    "..."


translate spanish day2_cards_without_sl_badefa0c:


    "Incluso miré en los arbustos alrededor de la cabaña, pero luego decidí volver y buscar entre todos los oscuros secretos de nuestra jefa."


translate spanish day2_cards_without_sl_3d3069e0:


    sl "Semyon, ¿qué te hace tardar tanto?"


translate spanish day2_cards_without_sl_f9a84268:


    me "Ehm... Yo... si..."


translate spanish day2_cards_without_sl_28bdcb47:


    "Con manos temblorosas giré la llave y la quité de la cerradura con una sacudida."


translate spanish day2_cards_without_sl_abc9e8c7:


    "Slavya se acercó."


translate spanish day2_cards_without_sl_45ebf54a:


    sl "¡Ah! ¡Mis llaves! ¡Pensé que las había perdido! ¿Dónde las hallastes?"


translate spanish day2_cards_without_sl_52fd9c46:


    me "Estaban en los arbustos... fuera. Las encontré justo ahora..."


translate spanish day2_cards_without_sl_ab249332:


    "Gracias a Dios que me las apañé para cerrarlo antes de que ella lo viera."


translate spanish day2_cards_without_sl_71f4991e:


    me "¿Vamos?"


translate spanish day2_cards_without_sl_9882a00c:


    "Quería irme afuera tan rápido como fuera posible, y preferiblemente olvidar mi intento de violar la privacidad de alguien."


translate spanish day2_cards_without_sl_9703c95b:


    "Cuando volvimos Olga Dmitrievna me informó:"


translate spanish day2_cards_without_sl_656cc591:


    mt "Perdona, me acordé que las cartas estaban en la cabaña de Slavya.{w} Fue a buscarlas mientras las estabas buscando."


translate spanish day2_cards_without_sl_899192e2:


    "Miré a Slavya, ella sonrió tímidamente."


translate spanish day2_cards_without_sl_79ea7a49:


    th "Vamos muchachos, no os preocupéis por mi, estaré bien..."


translate spanish day2_pre_cards_9f1a8f06:


    "Slavya y Olga Dmitrievna se fueron dentro."


translate spanish day2_pre_cards_09995e69:


    "Iba a seguirlas pero alguien me cogió de la mano."


translate spanish day2_pre_cards_08478924:


    "Alisa."


translate spanish day2_pre_cards_d0c76f00:


    "Su mirada fija me dio escalofríos por mi espalda. Era una poco agradable."


translate spanish day2_pre_cards_fe95bf48:


    me "¿Quieres algo?"


translate spanish day2_pre_cards_fcaa413f:


    "Pregunté cuidadosamente."


translate spanish day2_pre_cards_29ab9a8f:


    dv "¿Vas a jugar a ese estúpido juego?"


translate spanish day2_pre_cards_d14b9495:


    me "Uhmm... ¿sí? ¿Hay algo malo en ello?"


translate spanish day2_pre_cards_a364a5cb:


    dv "Qué va, nada."


translate spanish day2_pre_cards_bce20dc9:


    "Ella comenzó a irse pero entonces se volvió lentamente para mirar atrás y sonrió."


translate spanish day2_pre_cards_9900fc17:


    dv "¿Entonces juegas a cartas?"


translate spanish day2_pre_cards_11e21afc:


    me "Un poco."


translate spanish day2_pre_cards_f288a1e1:


    "No me podía imaginar qué era lo que quería."


translate spanish day2_pre_cards_3b50c11d:


    dv "¿Sólo al «durak» y ya está?"


translate spanish day2_pre_cards_11670603:


    th "Como si fueras un as del póker..."


translate spanish day2_pre_cards_0624cee5:


    me "Bueno, sí. Técnicamente."


translate spanish day2_pre_cards_7c7e60b4:


    dv "Pues entonces no tienes ni una opotunidad."


translate spanish day2_pre_cards_5ac38cf1:


    me "¿Por qué?"


translate spanish day2_pre_cards_de97c6a8:


    dv "¡Porqué!"


translate spanish day2_pre_cards_72a60644:


    me "¿Entonces sabes las reglas?"


translate spanish day2_pre_cards_bdbe816e:


    dv "¡Pues claro!"


translate spanish day2_pre_cards_3ad45daf:


    me "Bueno, entonces tendrás ventaja."


translate spanish day2_pre_cards_75b64a69:


    "No tenía sentido seguir con la conversación así que di unos pasos hacia la puerta."


translate spanish day2_pre_cards_cbef602e:


    dv "¡¿Por qué estás intentando todo el rato esquivarme?!"


translate spanish day2_pre_cards_d3fbefa3:


    th "¿Es que hay algo de lo que quieras hablar conmigo?"


translate spanish day2_pre_cards_38f554a8:


    dv "Apostemos."


translate spanish day2_pre_cards_1084810b:


    me "¿Qué quieres decir?"


translate spanish day2_pre_cards_abd41233:


    dv "¡Eres tonto! Las cartas, ¡qué va ser si no!"


translate spanish day2_pre_cards_2020ce91:


    me "¿Y sobre qué quieres apostar?"


translate spanish day2_pre_cards_8d15d8fc:


    dv "¡Que ganaré!"


translate spanish day2_pre_cards_14abf4d1:


    me "Ése es el resultado más probable."


translate spanish day2_pre_cards_47f0669e:


    "Consentí tranquilamente."


translate spanish day2_pre_cards_e82a5cb1:


    dv "¿Así que estás preocupado?"


translate spanish day2_pre_cards_7af7e68f:


    me "No, no lo estoy...{w} No soy de apostar cuando no estoy seguro de mis posibilidades."


translate spanish day2_pre_cards_f43da706:


    dv "Y tampoco eres de los que arriesgan."


translate spanish day2_pre_cards_7c484d79:


    th "Qué observadora. Estoy impresionado."


translate spanish day2_pre_cards_9fba406d:


    me "Correcto, así que yo..."


translate spanish day2_pre_cards_0d611ce5:


    dv "¡No lo eres!"


translate spanish day2_pre_cards_624ed6e0:


    me "¿Y ahora qué?"


translate spanish day2_pre_cards_8eadd935:


    "Suspiré agotado."


translate spanish day2_pre_cards_e071c864:


    "Empezaba a molestarme con sus conversas tontas sobre una apuesta sin sentido."


translate spanish day2_pre_cards_3f46f6f4:


    dv "¡Si no apuestas conmigo, le contaré a todos que intentaste seducirme!"


translate spanish day2_pre_cards_3d342d5a:


    me "¡¿Qué?!"


translate spanish day2_pre_cards_89e2898c:


    dv "¡Lo que escuchaste!"


translate spanish day2_pre_cards_75246721:


    th "Me la imagino haciendo eso..."


translate spanish day2_pre_cards_96533413:


    me "No seas estúpida.{w} ¿Quién te va a creer? He estado menos de dos días aquí, por lo que..."


translate spanish day2_pre_cards_9ffc882d:


    dv "¿Quieres probar tu suerte?"


translate spanish day2_pre_cards_1f560713:


    me "Así es...{w} ¿Y qué sucederá si gano?"


translate spanish day2_pre_cards_8a92943c:


    dv "No le diré nada a nadie."


translate spanish day2_pre_cards_a21dfdaa:


    me "¿Y si pierdo?"


translate spanish day2_pre_cards_09931ae3:


    dv "¿Otra vez te pones tonto?{w} Le contaré a todo el mundo que trataste de seducirme, ya te lo he dicho."


translate spanish day2_pre_cards_d4012ccc:


    me "Por lo tanto, ahora me estás diciendo que tengo que probar algo que no hice, ¿cuando en realidad jamás lo hice?"


translate spanish day2_pre_cards_37b7659b:


    dv "Si es así como quieres verlo."


translate spanish day2_pre_cards_17cc9d34:


    "No es una decisión sencilla."


translate spanish day2_pre_cards_742c0e25:


    "Por un lado... era estúpido estar de acuerdo... no conocía las reglas del juego y los juegos no eran lo mío."


translate spanish day2_pre_cards_a57f27fe:


    "Por otra parte... ella podía hacer de mi vida un infierno."


translate spanish day2_pre_cards_286e380f:


    th "Y una vez más, ¿puedo confiar en ella?"


translate spanish day2_pre_cards_3f4ee582:


    th "Ella podría hacerlo incluso si gano."


translate spanish day2_pre_cards_7cc3db09:


    dv "¿Tomaste ya una decisión?"


translate spanish day2_pre_cards_25b3a2ba:


    "Estaba a punto de responder, pero repentinamente Lena apareció detrás de mí."


translate spanish day2_pre_cards_a07b4105:


    dv "¿Qué?"


translate spanish day2_pre_cards_c93b52ae:


    un "Nada..."


translate spanish day2_pre_cards_3653acd5:


    "Lena se dio prisa por entrar dentro."


translate spanish day2_pre_cards_dbeba143:


    dv "¿Y bien?"


translate spanish day2_pre_cards_369c0b87:


    th "Me puedo arrepentir cientos de veces..."


translate spanish day2_pre_cards_788e115a:


    me "¡Está bien, lo haré!"


translate spanish day2_pre_cards_d1046a49:


    "Ella sonrió."


translate spanish day2_pre_cards_e541d96d:


    me "Pero si gano..."


translate spanish day2_pre_cards_33997702:


    dv "Claro, claro. Sin trampas."


translate spanish day2_pre_cards_8fa3ceb0:


    "Alisa se giró, se fue por las escaleras y entró en la cantina."


translate spanish day2_pre_cards_e69bd7ba:


    th "¿Por qué estoy haciendo esto?"


translate spanish day2_pre_cards_201eb6bf:


    th "¿Por qué ella puede conmigo, a pesar de todo, si lo desea?"


translate spanish day2_pre_cards_5354793c:


    th "Ella lo decidió de todas maneras."


translate spanish day2_pre_cards_e331dbe9:


    th "No. ¡No voy a formar parte de tus malas intenciones!"


translate spanish day2_pre_cards_a4a97e2b:


    me "Lo siento."


translate spanish day2_pre_cards_bf45c8f3:


    dv "¡Eres un debilucho!"


translate spanish day2_pre_cards_3b0ca4e8:


    "Ella se encogió de hombros y se fue por las escaleras. Antes de entrar dentro me advirtió:"


translate spanish day2_pre_cards_9ccfc1da:


    dv "¡Prepárate para las consecuencias!"


translate spanish day2_pre_cards_46588f42:


    th "¿Consecuencias...?"


translate spanish day2_pre_cards_a83588c3:


    th "¿Qué pasa si tomé la decisión incorrecta?"


translate spanish day2_pre_cards_c92ad65b:


    "Después de todo, ella puede realmente complicarme las cosas aquí."


translate spanish day2_pre_cards_752fe6dc:


    "Por otra parte, no podía simplemente dejarme llevar en algo tan temerario."


translate spanish day2_pre_cards_808392c5:


    "Dejé ir un largo suspiro y la seguí a través de la puerta."


translate spanish day2_cards_6c6e7a1b:


    "Dentro todo estaba preparado."


translate spanish day2_cards_18d88118:


    "Unos pocos pioneros estaban de pie aquí y allá hablando."


translate spanish day2_cards_28235b58:


    "Las mesas se movieron de su lugar para preparar la partida para jugadores y espectadores."


translate spanish day2_cards_bd07ee1b:


    "Miré a mi alrededor."


translate spanish day2_cards_1fda7f4b:


    "Algo estaba sucediendo en un distante rincón."


translate spanish day2_cards_3f899848:


    "Cuando estuve más cerca vi una hoja de papel grande con un diagrama dibujado en ella."


translate spanish day2_cards_5f123cf2:


    "Mi nombre estaba entre los jugadores."


translate spanish day2_cards_a9b3e057:


    me "¿Y a quién se le ocurrió esto?"


translate spanish day2_cards_8b5a8e17:


    "Le di una palmada a Electronik que estaba al lado."


translate spanish day2_cards_7d795258:


    el "¡Bueno, por supuesto, fue un humilde servidor!"


translate spanish day2_cards_d353e361:


    "Se inclinó hacia mi bromeando. Eso me hizo estar incómodo hasta el punto de estremecerme."


translate spanish day2_cards_234f4ba7:


    me "¿Y por qué demonios estoy entre los jugadores entonces?"


translate spanish day2_cards_24671a0f:


    "Estaba decepcionado."


translate spanish day2_cards_5c08eef1:


    "Hace unos segundos pensé que aun tenía una remota posibilidad de evitar este torneo... entonces no tendría que temer por la venganza de Alisa por perder la apuesta con ella."


translate spanish day2_cards_1c56c3d2:


    "Hace unos segundos pensé que aun tenía una remota posibilidad de evitar este torneo... entonces no tendría que temer por la venganza de Alisa por no apostar con ella."


translate spanish day2_cards_9ca648ce:


    th "Pero ahora esa esperanza se fue."


translate spanish day2_cards_8191ef08:


    el "Era pura suerte."


translate spanish day2_cards_f0331a22:


    th "Claro, suerte... salvo que ya estaba familiarizado con cada uno de los participantes."


translate spanish day2_cards_dc663958:


    th "¡Cuando ya había unas docenas de otros pioneros en la sala!"


translate spanish day2_cards_565fd677:


    "La ansiedad se estaba apoderando de mi."


translate spanish day2_cards_287334ff:


    "Era una sensación de estar siendo observado, mientras permanecías en una habitación vacía sin ventanas ni puertas."


translate spanish day2_cards_d9135825:


    me "¿Habrá algún premio?"


translate spanish day2_cards_e0592077:


    "Le pregunté perezosamente."


translate spanish day2_cards_b19ed28a:


    "Me quería distraer con una conversa insulsa."


translate spanish day2_cards_98766828:


    "Electronik estaba a punto de responder, cuando Ulyana apareció de la nada y comenzó a saltar alrededor suyo."


translate spanish day2_cards_fbd0629e:


    us "¡Premios, premios!"


translate spanish day2_cards_aa52231b:


    us "¡Escuché algo sobre premios!"


translate spanish day2_cards_2a82cf49:


    me "¿Sabes cuál es el principal lema de los Juegos Olímpicos?"


translate spanish day2_cards_c7b7a3fa:


    us "¿Qué? No."


translate spanish day2_cards_41d394ae:


    me "¡Lo entenderás cuando crezcas!"


translate spanish day2_cards_afdea261:


    "Hizo una cara irónica y le dio un codazo en las costillas a Electronik."


translate spanish day2_cards_06a30073:


    us "¿Qué hay en cuanto al premio?"


translate spanish day2_cards_bdf4c52f:


    el "Bueno... No lo sé.{w} No depende de mi."


translate spanish day2_cards_87bdcd1f:


    "Hizo un gesto de impotencia."


translate spanish day2_cards_7da0931b:


    th "De verdad, si tanto queriáis montar este estúpido juego, al menos podriáis dar al ganador una medalla de chocolate o alguna cosa."


translate spanish day2_cards_b35ebcaf:


    "Ulyana de repente saltó y se largó corriendo hacia un lugar."


translate spanish day2_cards_95df2d6c:


    th "Desearía ser así de optimista..."


translate spanish day2_cards_806b5dfa:


    me "Entonces, ¿cuáles son las reglas?"


translate spanish day2_cards_c4fe8ebc:


    el "¡Espera un segundo!{w} No todos están aquí aun."


translate spanish day2_cards_b58585ce:


    "Miré alrededor de la cantina... Alisa, Slavya, Lena, Miku y Shurik estaban aquí."


translate spanish day2_cards_b6cde061:


    me "Parecía que todos estaban aquí..."


translate spanish day2_cards_7b898ccd:


    el "¡No todos! ¡Zhenya no está aquí!"


translate spanish day2_cards_0d6ad6c9:


    "¿Se siente inquieto? ¿O sólo me lo parece a mi?"


translate spanish day2_cards_51851d31:


    me "Ella no está aquí, ¿y qué?"


translate spanish day2_cards_3a674479:


    me "Escoge a otra persona en su lugar."


translate spanish day2_cards_d17d3a9a:


    el "No, no puedo hacer eso..."


translate spanish day2_cards_8b484ca2:


    "Respondió lentamente."


translate spanish day2_cards_1522ed9b:


    "Decidí no preguntarle exactamente por qué no podía hacer eso."


translate spanish day2_cards_ed082483:


    me "Bueno, ve a por ella o haz algo, no sé."


translate spanish day2_cards_30b1301c:


    mt "Él no puede irse, es quien organiza el evento."


translate spanish day2_cards_06d5bcd8:


    "La líder del campamento apareció de la nada."


translate spanish day2_cards_8153d6f9:


    el "¡Pero Olga Dmitrievna!"


translate spanish day2_cards_1d92e913:


    "Electronik se quejó."


translate spanish day2_cards_4e9a3025:


    mt "Semyon irá.{w} ¿Verdad, Semyon?"


translate spanish day2_cards_943eaf3a:


    "Ella me miró y sonrió."


translate spanish day2_cards_6bdedf0e:


    th "Por supuesto, ¿quién si no...?"


translate spanish day2_cards_8cc053bc:


    me "¿Ella dónde está?"


translate spanish day2_cards_3030d7cf:


    mt "En la biblioteca, supongo."


translate spanish day2_cards_4366e696:


    me "Vale..."


translate spanish day2_cards_f0078f92:


    "Arrastré mis pies hacia la puerta."


translate spanish day2_cards_063c8560:


    el "¡Por favor date prisa!"


translate spanish day2_cards_e98e1cff:


    "¿Cuál es el problema de Electronik de todas formas?"


translate spanish day2_cards_75952aae:


    th "La noche se acerca pronto."


translate spanish day2_cards_ec3b332d:


    "Me iba a tomar mi tiempo, por lo que lentamente me dirigí hacia la biblioteca."


translate spanish day2_cards_7692b3b8:


    "Pero encontré a Zhenya antes de lo esperado... ella estaba sentada en un banco de la plaza, frente a Genda, quien permanecía en silencio como siempre."


translate spanish day2_cards_eca593b5:


    me "¿Qué estás haciendo aquí?{w} ¡Todo el mundo te está buscando!"


translate spanish day2_cards_17c805c8:


    mz "Sentada aquí, como puedes ver."


translate spanish day2_cards_823e3ce8:


    "Frunció el ceño."


translate spanish day2_cards_a15cb848:


    me "¡Pues vamos!"


translate spanish day2_cards_085c1aa0:


    mz "No quiero."


translate spanish day2_cards_a595fb55:


    "Miró a otra parte."


translate spanish day2_cards_5ac38cf1:


    me "¿Por qué?"


translate spanish day2_cards_0a62b9a3:


    mz "¡Porque no quiero!"


translate spanish day2_cards_12af78a1:


    "Me senté a su lado."


translate spanish day2_cards_9539c19b:


    me "Escucha, a mi tampoco me gusta la idea de esta competición, pero tampoco podemos dejarlos tirados a los demás."


translate spanish day2_cards_7ae8c79a:


    "Seguro que eso no sonaba como algo propio de mi."


translate spanish day2_cards_f8b4058b:


    "Hace un par de días atrás no habría incluso ni pensado en decir algo parecido a eso."


translate spanish day2_cards_377eefc7:


    "Zhenya me miró con sorpresa en su rostro."


translate spanish day2_cards_59271a84:


    mz "Entonces, ¿todos me están esperando?"


translate spanish day2_cards_6d6c0c61:


    th "Exactamente, ¿sobre qué estaba hablando?"


translate spanish day2_cards_fc30fd2b:


    me "Sí."


translate spanish day2_cards_40d696ab:


    mz "No iré de todas formas."


translate spanish day2_cards_a5db4948:


    "Ella frunció el ceño y amagó su rostro."


translate spanish day2_cards_65a156a9:


    me "¿Pero por qué?"


translate spanish day2_cards_6cd0f3dd:


    "Agité mis brazos, inquiriendo."


translate spanish day2_cards_7311aff6:


    mz "No sé cómo jugar a cartas..."


translate spanish day2_cards_4473e360:


    me "¿Y qué?{w} Yo tengo el mismo problema."


translate spanish day2_cards_687e7e02:


    mz "¿Entonces cómo vas a jugar?"


translate spanish day2_cards_a7327cc4:


    me "¿Qué? ¿Solamente puedes hacer cosas acerca de las cuales lees en libros o algo así?"


translate spanish day2_cards_976a6d64:


    mz "Claro."


translate spanish day2_cards_a4614188:


    "Estaba sorprendida."


translate spanish day2_cards_32c9a33a:


    me "¿Y qué ocurre si acabas en la Antártida y tienes que cazar osos polares para sobrevivir?"


translate spanish day2_cards_0fa90515:


    mz "Los osos polares no viven en la Antártida."


translate spanish day2_cards_beecbb90:


    "Zhenya sonrió."


translate spanish day2_cards_c2ae7af0:


    me "No importa. ¡Es sólo un ejemplo!"


translate spanish day2_cards_8d36304d:


    me "Vamos, no es como si dependiera la vida de alguien en dicho resultado."


translate spanish day2_cards_60f3d758:


    "Le llevó un tiempo pensar."


translate spanish day2_cards_fc98202b:


    mz "Solamente no quiero dejar a los demás tirados."


translate spanish day2_cards_3a30e832:


    me "Correcto."


translate spanish day2_cards_edd5c8c1:


    "Afirmé sarcásticamente."


translate spanish day2_cards_dbd5c99b:


    mz "¡Y no pienses en nada!"


translate spanish day2_cards_5b122da1:


    "No cogí lo que pretendía decirme pero da igual."


translate spanish day2_cards_78e0f168:


    "Obviamente, todo el mundo tiene sus puntos débiles."


translate spanish day2_cards_be072bc7:


    "En un minuto ambos estábamos de vuelta a la cantina."


translate spanish day2_cards_56cd1a43:


    "Todos miraron a Electronik."


translate spanish day2_cards_ff4ddd7c:


    el "Pues..."


translate spanish day2_cards_f8f1459d:


    "Se aclaró la garganta."


translate spanish day2_cards_7ddd8bdc:


    el "Cada ronda consiste en una partida."


translate spanish day2_cards_0de601df:


    el "En caso de empate, se repite la partida."


translate spanish day2_cards_a323cae4:


    el "Después de esto, el vencido pierde y empieza la siguiente ronda."


translate spanish day2_cards_69f4f055:


    el "Puesto que el número de voluntarios..."


translate spanish day2_cards_f311836a:


    "Él me miró."


translate spanish day2_cards_e7dd93af:


    el "Puesto que el número de jugadores solamente es de ocho, sólo tendremos tres rondas."


translate spanish day2_cards_e2f721ce:


    el "¿Está todo claro?"


translate spanish day2_cards_5e17549c:


    "La multitud aclamó afirmativamente."


translate spanish day2_cards_e97014bc:


    us "¿Y cuáles son los premios? Los premios, ¿cuáles son?"


translate spanish day2_cards_1fcf7c30:


    sl "¡Ulyana, déjalo ya!"


translate spanish day2_cards_3a71b956:


    "Slavya se avanzó y trató de coger a Ulyana."


translate spanish day2_cards_2b37c513:


    us "¡No descansaré hasta que el premio sea mío!"


translate spanish day2_cards_038f6c96:


    "Esta muchacha parecía tener ella sola suficiente energía como para teletransportarse a Alpha Centauri."


translate spanish day2_cards_fbd0629e_1:


    us "¡Premios, premios!"


translate spanish day2_cards_fdbf4fb7:


    "Repetía una y otra vez."


translate spanish day2_cards_7d36e3c2:


    sl "Para ya."


translate spanish day2_cards_09337077:


    "Slavya intentó razonar con ella."


translate spanish day2_cards_313ef528:


    "Electronik parecía estar desarrollando síntomas de vértigo con todo ese ajetreo a su alrededor."


translate spanish day2_cards_6c19d524:


    me "Vamos a empezar ya."


translate spanish day2_cards_fc1804fc:


    "Dije tranquilamente, y añadí a Ulyana:"


translate spanish day2_cards_f0d862c6:


    me "O no conseguirás ningún premio."


translate spanish day2_cards_937ae63e:


    "Parece ser que mi argumento le bastó, por lo que se fue a su lugar."


translate spanish day2_cards_739a28dd:


    "Slavya la siguió, dándome las gracias con una sonrisa cuando pasó cerca de mi."


translate spanish day2_cards_ea7ae555:


    "Los pioneros finalmente se colocaron."


translate spanish day2_cards_34834de5:


    "Me acerqué a la mesa en la que Lena se sentó."


translate spanish day2_cards_f1ce5d14:


    me "¿Te importa?"


translate spanish day2_cards_78e5e8c4:


    "Levantó su mirada y se ruborizó."


translate spanish day2_cards_f9967eb5:


    me "No te preocupes, tampoco sé las reglas."


translate spanish day2_cards_4752f785:


    th "¿Y cómo puedo estar tan seguro de que no solamente soy «yo»?"


translate spanish day2_cards_04d910fd:


    "Me senté."


translate spanish day2_cards_569cc0a5:


    me "Al parecer tendremos que jugar la primera ronda juntos."


translate spanish day2_cards_f1be5fed:


    un "Sí."


translate spanish day2_cardgame_ea01d133:


    "Finalmente Electronik comenzó a explicar las reglas."


translate spanish demo_play_intro_accc2d8a:


    el "Mirad las cartas atentamente."


translate spanish demo_play_intro_5fa54f4c:


    el "¡Hay exactamente seis de ellas frente a vosotros!"


translate spanish demo_play_intro_ccdb8a86:


    th "Espero que todos los presentes sepan cómo contar."


translate spanish demo_play_intro_283d409f:


    el "Ahora podéis mirarlas."


translate spanish demo_play_intro_5bf90d15:


    "Después de que todo el mundo haya visto sus cartas, Electronik las barajó."


translate spanish demo_play_intro_fb1ad002:


    el "Las reglas son las mismas que las del póker."


translate spanish demo_play_intro_c3658729:


    el "Espero que todos sepan cómo jugar..."


translate spanish demo_play_intro_b18e73e6:


    "Conozco las reglas, pero no estaba tan seguro de los demás."


translate spanish demo_play_intro_5d5160d3:


    el "Primero de todo está la carta más alta, luego la pareja, la doble pareja, el trío... {w} Y así."


translate spanish demo_play_intro_66ab73d4:


    el "En la primera ronda eliges la carta que quieras coger de tu oponente."


translate spanish demo_play_intro_dbcd0300:


    el "A su vez, el oponente puede elegir barajar sus cartas dos veces."


translate spanish demo_play_intro_d3a6fb9a:


    el "O puede escoger no hacerlo, si no necesita la carta que tú quieres coger."


translate spanish demo_play_intro_faea44e8:


    el "Toma nota de que aquí tu oponente puede ver qué cartas se intercambian de lugar."


translate spanish demo_play_intro_d5e89419:


    el "En el siguiente paso, tu oponente escoge una carta."


translate spanish demo_play_intro_ee59bb67:


    el "Y así todo el rato... Creo que está muy claro."


translate spanish demo_play_intro_3c934234:


    "No estaba tan claro para mi."


translate spanish demo_play_intro_135cb9f4:


    us "¡Ey tú, Einstein!"


translate spanish demo_play_intro_5b29bd77:


    "Ulyana le gritó desde su mesa."


translate spanish demo_play_intro_6cf11f92:


    us "¡No pillé nada!"


translate spanish demo_play_intro_3affc0c2:


    el "Lo harás cuando comencemos."


translate spanish demo_play_intro_8dd13e36:


    "Electronik se fue a una mesa con un diagrama, dejando a Ulyana en soledad con la espera."


translate spanish demo_play_intro_ce1458ea:


    me "Vas primero."


translate spanish demo_play_intro_881ff0c4:


    "Tenía la esperanza de poder concentrarme en el juego cuanto antes mejor."


translate spanish demo_play_intro_3dc8ac41:


    "Lena, perpleja más de lo habitual, me tendió las cartas."


translate spanish demo_play_me_defend_1_5bbc93a2:


    "En medio de la mesa sus manos se pararon."


translate spanish demo_play_me_defend_1_c4c9ab6d:


    un "Podrías..."


translate spanish demo_play_me_defend_1_0f88ba77:


    th "¡Oh claro! ¡Debería estar escondiendo mis cartas!"


translate spanish demo_play_me_defend_1_9592b4e5:


    th "Lo que estaba diciendo Electronik..."


translate spanish demo_play_me_defend_1_7f329a7a:


    "Para tratar de confundir al oponente puedo intercambiar dos de mis cartas.{w} Dos veces."


translate spanish demo_play_me_defend_1_30d81d36:


    "O puedo no hacerlo."


translate spanish demo_play_me_defend_1_a9691ffc:


    "¿Debería esconderlas o no?"


translate spanish demo_play_me_defend_1_12313e8c:


    "De todas maneras, puedo estar de acuerdo y darle la carta que ella escoja directamente."


translate spanish demo_play_me_defend_1_3f7d9aa4:


    "De otra manera Lena puede cambiar de idea y coger otra carta si quiere.{w} O tal vez no."


translate spanish demo_play_me_select_1_2c550258:


    "¡Las cosas empezaban a estar más claras!{w} O, al menos, comprensibles..."


translate spanish demo_play_me_select_1_ab26be30:


    me "Ahora era mi turno."


translate spanish demo_play_me_select_1_6a346d80:


    "Puedo recuperar mi carta de ella, o puedo escoger otra distinta."


translate spanish demo_play_rival_defend_10293eac:


    "Lena puede intentar proteger su carta."


translate spanish demo_play_rival_defend_092d114b:


    "Pero si la observo atentamente, puedo coger la carta que escogí a pesar del intercambio hecho."


translate spanish demo_play_after_loop_a4fc9855:


    "¡Lo hice!"


translate spanish day_2_cards_continue_b8733e71:


    "Electronik que estaba silencioso observando nuestra partida, asintió con la cabeza."


translate spanish day_2_cards_continue_f37b7ba7:


    "Parece que ya lo vamos entendiendo."


translate spanish day_2_cards_continue_a335157b:


    el "Ahora, durante el turno del oponente se intercambian cartas tres veces, presta atención en cada una... penetra a tu oponente con tu mirada."


translate spanish day_2_cards_continue_b27ea81d:


    "Solté una risita. «Penetrar»."


translate spanish day_2_cards_continue_ae91c5b2:


    el "¿Qué es tan divertido?"


translate spanish day_2_cards_continue_055701c5:


    me "Oh, olvídalo."


translate spanish day_2_cards_continue_394b559a:


    "Traté de poner una cara seria."


translate spanish day_2_cards_continue_aa08701b:


    "Él me miró fijamente por un momento y entonces siguió."


translate spanish day_2_cards_continue_3e03a63e:


    el "Y luego las mostramos para ver qué cartas son las mejores."


translate spanish day_2_cards_continue_f0cced6b:


    "Electronik volvió a su diagrama."


translate spanish un_play_draw_2c705b13:


    el "¡Empate! ¡Deberías jugar una vez más!"


translate spanish un_play_win_5df79197:


    "¡Gané!"


translate spanish un_play_win_90577728:


    "De verdad... es difícil jugar a un juego por primera vez que se ha inventado justo en el momento... y no por uno mismo."


translate spanish un_play_win_4dee3b34:


    "¡Pero gané!"


translate spanish un_play_win_0b9b056b:


    "Aunque, el momento de gloria fue amargo por el hecho de que era Lena quien era el perdedor."


translate spanish un_play_win_8d4faa34:


    "En general ella no estaba muy segura de sí misma conmigo, pero ahora que perdió..."


translate spanish un_play_win_d7bb4c00:


    th "Me da vergüenza incluso mirarla."


translate spanish un_play_win_3f0a42a8:


    "Probablemente debería haber perdido para aumentar su autoestima."


translate spanish un_play_win_08d9c222:


    th "Pero tengo una apuesta con Alisa..."


translate spanish un_play_win_442b0ff1:


    "Mientras tanto, Electronik anunció orgullosamente que la primera ronda habia terminado."


translate spanish un_play_win_4c17c888:


    "Una lista de aquellos que consiguieron llegar a la segunda ronda apareció en el esquema después de un pequeño rato."


translate spanish un_play_win_6ece79aa:


    "Las parejas de semifinales consistían en Alisa contra Zhenya y Ulyanya contra..."


translate spanish un_play_win_aac891f4:


    "Mi."


translate spanish un_play_win_482610ae:


    "Un suspiro de resignación se me escapó."


translate spanish un_play_win_3e9272c6:


    "¡Ulyana inmediatamente tomó asiento delante de mí!"


translate spanish un_play_win_9d897d0d:


    us "¡Ja!"


translate spanish un_play_win_2ed6127e:


    "Me miró con una sonrisa."


translate spanish un_play_win_9e5e59e8:


    us "¿Cómo te las arreglaste para vencer a Lena?"


translate spanish un_play_win_69120a2e:


    us "¿Trampas, tal vez?"


translate spanish un_play_win_140619d9:


    me "No soy como tú."


translate spanish un_play_win_4f2b481d:


    me "Solamente sé cómo jugar a cartas."


translate spanish un_play_win_14e9ade2:


    th "Al menos eso es lo que quiero que ella crea."


translate spanish un_play_win_dd90f754:


    me "¿Y cómo pudiste vencer a Shurik?"


translate spanish un_play_win_4dc5e73c:


    us "Ah..."


translate spanish un_play_win_b7c182de:


    "Ulyana movió su mano, haciendo un gesto de lo fácil que era."


translate spanish un_play_win_e22a379f:


    us "Le amenacé que me uniría a su club."


translate spanish un_play_win_dc47b4bb:


    "Ella volvió a sonreír nuevamente."


translate spanish un_play_win_da4c608f:


    us "¿Lo soportarías tú?"


translate spanish un_play_win_9d811be6:


    me "¡Qué va!"


translate spanish un_play_win_35ba093a:


    us "Ugh..."


translate spanish un_play_win_502323d6:


    us "¡Pues elegiré la carta que te daré yo misma!"


translate spanish un_play_win_4998c00d:


    me "¿Has escuchado las reglas?"


translate spanish un_play_win_7625dabe:


    us "¡Ah, olvídalas!"


translate spanish un_play_win_ff6428f7:


    "Parecía que no le importaba realmente mucho."


translate spanish un_play_win_7a4f4560:


    me "Vale, pero elegiré las cartas que te daré a ti también."


translate spanish un_play_win_0a1ee1fc:


    us "¡Trato hecho!"


translate spanish un_play_win_7ddb7c7e:


    "Inspirado por una victoria en la primera ronda, me aventuré a hacer ese movimiento peligroso."


translate spanish un_play_win_fa27d833:


    "Podría haberlo discutido, reclamar a Electronik y finalmente hacerlo a mi manera, pero por alguna razón me sentí verdaderamente confiado en el resultado de esta ronda."


translate spanish un_play_win_09a416bb:


    "Sí, estábamos rompiendo las reglas, pero estaba en el mismo barco que Ulyana."


translate spanish un_play_win_05c369b7:


    "Miré hacia a Electronik."


translate spanish un_play_win_d0aaf07c:


    el "¡Es el momento de empezar las semifinales!"


translate spanish un_play_win_8ce591ef:


    "Ordenó."


translate spanish un_play_win_352aee2b:


    "He comprobado cuidadosamente mis cartas, asegurándome de que Ulyana no pudiera verlas."


translate spanish us_play_me_defend_2_7232aa89:


    us "¡Ey tú! ¡No cambies las cartas, que me confundes!"


translate spanish us_play_me_defend_2_25579ef8:


    "Mmm..."


translate spanish us_play_draw_2c705b13:


    el "¡Empate! ¡Jugad una vez más!"


translate spanish us_play_win_4c353fb8:


    us "¡Ey! ¡Esto no es justo!"


translate spanish us_play_win_49d03e3a:


    us "¡Tenías que haberlas controlado y perder!"


translate spanish us_play_win_fb0127bc:


    "Se le pusieron rojas las mejillas del resentimiento de tal forma que se parecía a David el Gnomo."


translate spanish us_play_win_6905d85b:


    us "Repitamos la partida, pero esta vez tienes que controlarlo, ¡¿lo captas?!"


translate spanish us_play_win_4782a874:


    "No era sólo yo quien le había escuchado, sino toda la sala."


translate spanish us_play_win_2286cffa:


    "Incluso Electronik."


translate spanish us_play_win_0e9c7f85:


    el "¡No están permitidas repetir las partidas!"


translate spanish us_play_win_2f715ea7:


    "Ulyana lo ignoró completamente."


translate spanish us_play_win_615b52f1:


    us "¡Debes perder!"


translate spanish us_play_win_f6ec2b26:


    me "No tengo ni la más mínima intención de jugar por segunda vez contigo."


translate spanish us_play_win_498ef814:


    "Dije tranquilamente."


translate spanish us_play_win_a7075cc6:


    us "Está bien, ¿lo quieres así?"


translate spanish us_play_win_7c6b0a82:


    me "Sí, así. "


translate spanish us_play_win_f60b0b79:


    us "Pues entonces esparciré rumores de que tú acosaste sexualmente a Alisa."


translate spanish us_play_win_e4705df4:


    "Me susurró."


translate spanish us_play_win_32b78b25:


    me "¡¿Qué?!"


translate spanish us_play_win_50dfdc11:


    "Me recliné sobre la mesa y le hice una mirada amenazante."


translate spanish us_play_win_e2a63898:


    me "¿Así que me estuviste escuchando?"


translate spanish us_play_win_56390f46:


    us "Lo escuché mientras pasaba por ahí."


translate spanish us_play_win_6d4b54e2:


    "De todas formas, es mucho mejor jugar una ronda extra que..."


translate spanish us_play_win_7189cb82:


    th "¡Ciertamente lo puede hacer!"


translate spanish us_play_win_0e589c83:


    "Suspiré y supliqué a Electronik."


translate spanish us_play_win_e5983d15:


    me "No hay problema, haremos otra vez una partida rápida."


translate spanish us_play_win_0c8d9dc7:


    el "Ah, lo que sea..."


translate spanish us_play_win_d656c5e0:


    "Se encogió de hombros."


translate spanish us_play_win_a5e9e043:


    "Por lo que la revancha empezó."


translate spanish us2_play_draw_2c705b13:


    el "¡Un empate! Tenéis que volver a jugar."


translate spanish us2_play_win_31467604:


    me "Super fácil."


translate spanish us2_play_win_0f831176:


    "Dije y me recliné en mi asiento."


translate spanish us2_play_win_57a622de:


    us "¡Esto no es justo!"


translate spanish us2_play_win_8f3148e1:


    th "¡Espero que no vuelva a pedir otra revancha!"


translate spanish us2_play_win_dd53341d:


    me "¿Por qué?"


translate spanish us2_play_win_e98bfbde:


    "Solté una risita."


translate spanish us2_play_win_08916ca6:


    us "Vale..."


translate spanish us2_play_win_68edce41:


    "Dijo Ulyana ofendida y se levantó de la mesa."


translate spanish us2_play_win_aec6e03a:


    "Examiné el esquema del torneo de más cerca, intentando comprender quién iba a ser mi rival final."


translate spanish us2_play_win_1afc5d06:


    "En el mismo momento Alisa tomó lugar en mi mesa."


translate spanish us2_play_win_6286b0f8:


    "Puse una fingida y tonta sonrisa."


translate spanish us2_play_win_ad1e84f8:


    th "¡Como si estuviera preocupado por ella!"


translate spanish us2_play_win_d29e64cc:


    me "Enhorabuena."


translate spanish us2_play_win_bb61fa08:


    dv "Te arrepentirás de no haber sido más flojo."


translate spanish us2_play_win_b7a1e7d5:


    th "Ya estoy arrepintiéndome..."


translate spanish us2_play_win_a18f9993:


    "Pero no el que no haya sido «más flojo»... Estoy lamentando que he formado parte de éste estúpido juego en primer lugar."


translate spanish us2_play_win_c3ff6c62:


    dv "¿Esperas ganar?"


translate spanish us2_play_win_58f07f35:


    me "Espero que cumplas con tu promesa."


translate spanish us2_play_win_7807add1:


    dv "Vale, ¡a jugar!"


translate spanish dv_play_draw_2c705b13:


    el "¡Empate! Tendréis que jugar una vez más."


translate spanish day2_main3_84842450:


    "De todas formas aun así es frustrante perder..."


translate spanish day2_main3_c9e7d2f7:


    "Al menos he ganado una ronda."


translate spanish day2_main3_1f69a49c:


    "He perdido con Dvachevskaya."


translate spanish day2_main3_e25a1002:


    th "Y es la que menos debía perder."


translate spanish day2_main3_183277f1:


    th "Me imagino qué hará mañana."


translate spanish day2_main3_00105f86:


    th "Ella podría avergonzarme en la formación (si no me duermo, claro), podría quejarse a Olga Dmitrievna."


translate spanish day2_main3_d73b14bb:


    th "Podría tan sólo esparciar rumores por todo el campamento."


translate spanish day2_main3_d7329552:


    th "Lo peor de todo es que la creerían a ella, no a mi."


translate spanish day2_main3_cc371952:


    "No estaba seguro de cómo, pero estaba al 100%% seguro de ello."


translate spanish day2_main3_4cda6c6c:


    th "Y no importa que no hiciera ninguna apuesta con Alisa, todo lo que importa es que gané."


translate spanish day2_main3_bac15b3e:


    th "¡Si es Alisa puedo imaginarme que hará mañana!"


translate spanish day2_main3_d20a7c98:


    th "Haría de mi el hazmerreír mañana durante la formación contándole todo a Olga Dmitrievna."


translate spanish day2_main3_3e52af3c:


    th "O difundiría habladurías por el campamento."


translate spanish day2_main3_e1a0fa0c:


    th "Lo peor de todo es que todos le creerían a ella, no a mi.{w} No estaba seguro de cómo, pero estaba al 100%% seguro de ello."


translate spanish day2_main3_a8cecdb4:


    "Abandoné la cantina."


translate spanish day2_main3_f9f946ef:


    "Era todavía pronto para ir a dormir y una corta caminata parecía una buena idea."


translate spanish day2_main3_b3b9bfb2:


    th "¿Hacia dónde debería ir?"


translate spanish day2_aidpost_eve_dad1f28b:


    "Continué hacia delante, deambulando alrededor, y llegué a la enfermería."


translate spanish day2_aidpost_eve_1b18dca2:


    "Si necesitara un auxilio sería de un psiquiatra, e ir a ver a nuestra enfermera no se me pasó por la cabeza ni por asomo."


translate spanish day2_aidpost_eve_a20cefa7:


    "..."


translate spanish day2_square_eve_8c430c9f:


    "Llegué a la plaza y tomé asiento en un banco."


translate spanish day2_square_eve_dd9e540e:


    "Simplemente permanecer frente a la estatua de Genda resultaba ser una buena idea."


translate spanish day2_square_eve_a20cefa7:


    "..."


translate spanish day2_beach_eve_4ae280fe:


    "Llegué a la playa."


translate spanish day2_beach_eve_7700f3ad:


    "No estaba de buen humor y en verdad no tenía ningún deseo de nadar, pero me acerqué a la orilla e intenté tocar el agua con mi mano."


translate spanish day2_beach_eve_691e5417:


    "El agua estaba cálida."


translate spanish day2_beach_eve_5dc0ba05:


    "Parecía que todavia se enfríaba después de semejante caluroso día."


translate spanish day2_beach_eve_06f63b4a:


    th "Bueno, quizá más tarde volveré para nadar."


translate spanish day2_beach_eve_a20cefa7:


    "..."


translate spanish day2_dock_eve_ee198286:


    "Decidí ir al embarcadero."


translate spanish day2_dock_eve_c4425934:


    "El sol estaba aun reluciendo levemente por encima del horizonte y el río se jactaba con todas sus diferentes tonalidades de rojo, amarillo y naranja."


translate spanish day2_dock_eve_8572e4d2:


    "El agua parecía estar ardiendo en llamas, permanecí quieto en tanto se iba haciendo débil, hasta que al fin se ocultó completamente."


translate spanish day2_dock_eve_a20cefa7:


    "..."


translate spanish day2_busstop_eve_62cce665:


    "Los sucesos del día anterior seguían brillando como imágenes en mi cabeza: aquella maldita e inútil lista, ese estúpido torneo..."


translate spanish day2_busstop_eve_73a11ef4:


    "Ahora mismo no quiero hacer nada, ni hablar con nadie. Incluso no quiero ni empezar a pensar sobre mi actual situación."


translate spanish day2_busstop_eve_6f392ba3:


    "Volví a la plaza y tomé asiento en un banco, mientras permanecía frente a la estatua de Genda."


translate spanish day2_busstop_eve_a20cefa7:


    "..."


translate spanish day2_busstop_eve_58e66c22:


    "No sé cuánto tiempo he estado sentado así, pero finalmente los grillos que cantaban me devolvieron a la realidad."


translate spanish day2_busstop_eve_c4f332c1:


    "Me levanté y ciegamente caminé en una dirección cualquiera."


translate spanish day2_busstop_eve_f17e0aeb:


    th "La parada del autobús..."


translate spanish day2_busstop_eve_301f4821:


    "De alguna manera, era el segundo día seguido que me encontraba en este lugar."


translate spanish day2_busstop_eve_31ee41ff:


    th "¿Tal vez, subconscientemente, siento que el autobús estará esperándome aquí, para devolverme al mundo real?"


translate spanish day2_busstop_eve_b99b8886:


    th "Muy improbablemente."


translate spanish day2_busstop_eve_74d34bfa:


    th "Por otra parte por qué no..."


translate spanish day2_busstop_eve_39c07ffa:


    "Ya se había hecho muy oscuro."


translate spanish day2_busstop_eve_5daf4589:


    "Sólo estaba permaneciendo aquí y contemplando el cielo nocturno."


translate spanish day2_busstop_eve_4bb46255:


    "La astronomía nunca me ha llamado mucho la atención como la astronáutica."


translate spanish day2_busstop_eve_b9df3f0f:


    "Siempre ha sido más interesante mirar en las fotografías de las lejanas constelaciones, nebulosas y galaxias hechas por artistas que tratar de conseguir medir las velocidades angulares y las masas de las estrellas."


translate spanish day2_busstop_eve_f721c30a:


    "Pero, ciertamente podría encontrar la constelación de la Gran Osa."


translate spanish day2_busstop_eve_011b7220:


    "Si me perdiera en bosques nevados la única manera de encontrar la ruta para guiarme sería ese pequeño conocimiento sobre musgos creciendo en la cara norte de un árbol."


translate spanish day2_busstop_eve_252aa5da:


    "No obstante, no creo que me ayudara a encontrar la ruta para guiarme..."


translate spanish day2_busstop_eve_ffa2f154:


    "Después de un par de minutos me dirigí hacia el campamento."


translate spanish day2_busstop_eve_a20cefa7_1:


    "..."


translate spanish day2_stage_eve_2c57c848:


    "Me decidí a caminar hacia el norte (o al menos hacia la dirección que creía que era el norte)."


translate spanish day2_stage_eve_ac512323:


    "Tras unos minutos más tarde vi un escenario de concierto, unas cuantas filas de bancos de madera y al parecer una grada de madera."


translate spanish day2_stage_eve_0bd0ce88:


    "Fui a la grada."


translate spanish day2_stage_eve_ff3859bd:


    "Muchos y variados equipos de música... altavoces, un micrófono de pie e incluso un piano."


translate spanish day2_stage_eve_85b7add9:


    "Me imaginé que estaba frente a una gran audiencia, gritando y coreando mi nombre, mis ojos cegados por las luces del proyector."


translate spanish day2_stage_eve_aa3b726d:


    "Imaginando una guitarra en mis manos, intenté realizar un largo e impactante solo."


translate spanish day2_stage_eve_7fe7938c:


    "Supuse que se vería muy gracioso desde el punto de vista de un extraño... un tipo raro agitando sus brazos en la grada, haciendo el simio y poniendo caras."


translate spanish day2_stage_eve_dc4028dc:


    th "¡Espero que nadie me viera aquí!"


translate spanish day2_stage_eve_a44729f3:


    "Teniendo eso en mente salté de la grada y me di prisa por irme."


translate spanish day2_stage_eve_a20cefa7:


    "..."


translate spanish day2_football_eve_bd44db9a:


    "Era ciertamente bastante tarde para el fútbol, por lo que el campo estaba vacío."


translate spanish day2_football_eve_2b9a9e9a:


    "Pasé ahí un par de minutos, escuchando el silencio sin más y entonces me fui."


translate spanish day2_football_eve_a20cefa7:


    "..."


translate spanish day2_dv_a4dc0a8d:


    "¡Hurra! ¡Gané! ¡Le hice saber a Alisa quién manda aquí!"


translate spanish day2_dv_e284eef4:


    th "¡Eso significa que apostar con ella no fue una pérdida de tiempo!"


translate spanish day2_dv_6d9b3054:


    "Ahora lo único pendiente es esperar a que ella no difunda falsos rumores por el hecho de haber perdido."


translate spanish day2_dv_c21f93b9:


    "De repente, tuve el deseo de celebrar mi propia victoria y tener un zambullido en la playa."


translate spanish day2_dv_32525770:


    "A decir verdad, no sé realmente nadar pero la posibilidad de zambullirme en la fría agua bajo la luz lunar me pareció atractiva."


translate spanish day2_dv_e75dca33:


    "Después de todo, me pasé todo el día de ayer con mi ropa de invierno y seguramente se le podría exprimir abundantemente todo lo que sudé."


translate spanish day2_dv_1c3ad5c8:


    "Por la noche nadie debería estar aquí, por lo que me quité toda mi ropa salvo mis calzoncillos y me sumergí en el agua."


translate spanish day2_dv_90e7fd64:


    th "Si lo hubiera sabido habría cogido mi bañador."


translate spanish day2_dv_affe5cb3:


    "Normalmente era suficiente para mi nadar unos 15-20 metros, pero esta vez la euforia de mi victoria me estimuló a batir ese récord."


translate spanish day2_dv_df18f64c:


    "Nadé lentamente y con tacto, vigilando cualquier movimiento con mis brazos y piernas, manteniendo mi aliento preparado."


translate spanish day2_dv_620aa5dd:


    "Repentinamente, ¡PUM!"


translate spanish day2_dv_de055afa:


    "Un golpe en mi espalda me envió bajo el agua."


translate spanish day2_dv_33564eb7:


    "Comencé a ahogarme, pero aun así me esforcé por salir fuera y agarrarme a la boya más cercana."


translate spanish day2_dv_8fc9d4bf:


    "Me giré y vi a Alisa nadando tras de mi."


translate spanish day2_dv_33864d31:


    me "¡¿Qué demonios estás haciendo?!"


translate spanish day2_dv_c3a057d6:


    dv "¿Que qué estoy haciendo? Aclamar al ganador."


translate spanish day2_dv_d6c4be35:


    me "¡¿Y si me empiezo a ahogar qué?!"


translate spanish day2_dv_f20a4a5d:


    dv "Bueno, te rescataré."


translate spanish day2_dv_8386bb65:


    me "Puff, está bien..."


translate spanish day2_dv_6409b342:


    "Era sencillamente peligroso permanecer aquí, ella me podría volver a ahogar fácilmente. Reuní todas mis fuerzas y nadé hacia la orilla."


translate spanish day2_dv_556fac3c:


    "La arena de la playa me cubría el cuerpo entero y lentamente empecé a recobrar mi aliento."


translate spanish day2_dv_f577f3f7:


    "Después de un rato Alisa salió del agua."


translate spanish day2_dv_d82da8ae:


    dv "¡Sabes, nadas bien!"


translate spanish day2_dv_869a64b8:


    th "Yo no lo diría."


translate spanish day2_dv_4a35be24:


    me "Claro, tú también."


translate spanish day2_dv_64dd4f95:


    dv "Bueno, ¡¿no era eso obvio?!"


translate spanish day2_dv_cbc4bb84:


    "No dije nada."


translate spanish day2_dv_9a5b6429:


    dv "Me has ganado dos veces hoy.{w} Eso significa que te perdonaré dos de tus deudas."


translate spanish day2_dv_ff2a842c:


    th "¿De qué clase de deudas está hablando ella?"


translate spanish day2_dv_7d05db3b:


    "Parecía que Alisa tenía un problema examinando la realidad."


translate spanish day2_dv_493885e5:


    me "Muchas gracias..."


translate spanish day2_dv_27c62366:


    "Dije sarcásticamente."


translate spanish day2_dv_01820ffe:


    dv "Ya sabes, no eres el perdedor que pensé..."


translate spanish day2_dv_5b1b7a36:


    "Alisa llevaba un traje de bañador que realzaba perfectamente la belleza de su figura."


translate spanish day2_dv_84e41e3b:


    "Bueno, dejando de lado todos sus inconvenientes de su carácter esta ventaja de ella no podría dejarse de lado."


translate spanish day2_dv_c6ee82bf:


    me "¿Desde cuándo me convertí en un perdedor?"


translate spanish day2_dv_945c59bc:


    "Ella sonrió astutamente."


translate spanish day2_dv_3bbe4c98:


    dv "¿Qué hay de malo? ¿Es que eres uno de ellos?"


translate spanish day2_dv_f837dd45:


    me "¡Por supuesto que no!"


translate spanish day2_dv_072e019b:


    dv "¿Y cómo lo demostrarás?"


translate spanish day2_dv_fdf9f2c2:


    me "¡No voy a demostrar nada!"


translate spanish day2_dv_a3b5accf:


    dv "¡Oh! ¿Así que eso es todo?"


translate spanish day2_dv_516018de:


    "Dijo sin enfado alguno."


translate spanish day2_dv_e7ec8520:


    me "Sí, eso es..."


translate spanish day2_dv_4a405aad:


    "El silencio era de esperar."


translate spanish day2_dv_1c5b30ab:


    "Una brisa de viento jugaba suavemente con las olas, enviándolas a la orilla y recogiéndolas otra vez."


translate spanish day2_dv_00eafdba:


    "Alisa todavía me miraba fijamente, al parecer, como si ella se hubiera olvidado completamente de mi presencia."


translate spanish day2_dv_c4f3bd04:


    me "¡Ey, aquí la Tierra a Alisa!"


translate spanish day2_dv_2d89497e:


    "Su mirada perdida inmediatamente cambió a una normal."


translate spanish day2_dv_b63997f5:


    dv "En fin, adiós."


translate spanish day2_dv_293bd21f:


    "Cogió sus ropas que estaban cerca y se alejó."


translate spanish day2_dv_a20cefa7:


    "..."


translate spanish day2_dv_837271a8:


    "Era tarde, pero decidí pasar un poco más de tiempo descansando tumbado sobre mi espalda y observando las estrellas."


translate spanish day2_dv_95ca3d6a:


    "Después de todo, antes no tenía tantas oportunidades para ello."


translate spanish day2_dv_8b8426ec:


    "O simplemente traté de evitar cada oportunidad."


translate spanish day2_dv_b48dade4:


    th "Bueno, si pensamos acerca de ello, la luz de las estrellas lejanas nos alcanza solamente tras millones de años de recorrido..."


translate spanish day2_dv_5739d22f:


    th "Ahora mismo puedo ver la estrella porque brilló mucho antes, en un pasado lejano."


translate spanish day2_dv_799ecc91:


    th "Y ahora esta estrella se fue, ya explotó..."


translate spanish day2_dv_1c004364:


    th "¡Espera un minuto!{w} ¡Se llevó mis ropas también!"


translate spanish day2_dv_bb8c1b2a:


    "Me levanté y miré alrededor de la playa."


translate spanish day2_dv_ec32c246:


    "Sin duda, Alisa cogió mi uniforme de pionero."


translate spanish day2_dv_6ccc3e04:


    th "¡Maldita sea!"


translate spanish day2_dv_37751890:


    "Empecé a pensar que ella no era tan mala... "


translate spanish day2_dv_801f233a:


    th "Necesito una buena idea rápidamente."


translate spanish day2_dv_1c7817b8:


    th "Ciertamente podría ir a Olga Dmitrievna y quejarme pero no en mis calzoncillos húmedos..."


translate spanish day2_dv_a11b7a5a:


    "Tampoco sabía en que cabaña vivía Alisa."


translate spanish day2_dv_9aedec39:


    th "Y llamar en cada una de las puertas no era en realidad una buena idea."


translate spanish day2_dv_faa17736:


    th "¿Tal vez debería acudir a Slavya?"


translate spanish day2_dv_691cf254:


    th "Sí, por supuesto... por la noche vistiendo sólo calzoncillos...{w} ¡Lo que me faltaba, eramos pocos y parió la abuela!"


translate spanish day2_dv_1388e5eb:


    "¡Aparentemente, parece que estoy jodido!"


translate spanish day2_dv_a6749c0b:


    "En cualquier caso tengo que hacer algo."


translate spanish day2_dv_6c982559:


    "Tenía suerte de que no mucho tiempo había pasado desde que ella se fue, por lo que podría aun atrapar a Alisa..."


translate spanish day2_dv_9744eb3a:


    "¡...date prisa!"


translate spanish day2_dv_1e0acdbc:


    "En un abrir y cerrar de ojos me hallé en la plaza."


translate spanish day2_dv_20bc07ef:


    "Para mi gran sorpresa, Alisa estaba sentada en un banco aparentemente aburrida."


translate spanish day2_dv_0706b8a9:


    "Ya había cambiado."


translate spanish day2_dv_679b46e1:


    me "¡Devuélvemelo!"


translate spanish day2_dv_e5a0d224:


    dv "Claro que sí, ten..."


translate spanish day2_dv_1063c21e:


    "Con un tono de culpabilidad me retornó mi uniforme."


translate spanish day2_dv_51149e68:


    me "..."


translate spanish day2_dv_6cf682fe:


    dv "No pienses que te estaba esperando intencionadamente o algo así..."


translate spanish day2_dv_a71ccb66:


    "Alisa se giró y se fue hacia las cabañas con un paso relajado."


translate spanish day2_dv_24bb116e:


    "Me quedé de piedra en el suelo."


translate spanish day2_dv_a26ba0eb:


    "No me esperaba dicho cambio."


translate spanish day2_dv_b327337f:


    th "Quizá los remordimientos le pudieron..."


translate spanish day2_dv_4e97c063:


    th "Un giro inesperado..."


translate spanish day2_dv_afa37a67:


    th "De todas formas, es mejor estar lejos de ella. Y los recientes sucesos no cambian nada."


translate spanish day2_dv_8b243a91:


    "Me dirigí hacia la cabaña de Olga Dmitrievna."


translate spanish day2_sl_51d58656:


    "Estaba volviendo hacia el campamento, cuando de repente oí un ruido detrás de las puertas."


translate spanish day2_sl_5c128ad7:


    th "¿Quién es ahora...?"


translate spanish day2_sl_01f96367:


    "Cerca del edificio de clubs parecía que alguien estaba caminando por el camino hacia el bosque."


translate spanish day2_sl_d5b66cdc:


    "Era tan oscuro que no podía ver nada salvo una sombra borrosa."


translate spanish day2_sl_6368700c:


    "Me preocupaba quién podría estar por ahí a estas horas de la noche."


translate spanish day2_sl_5f2c4088:


    th "¿Un pionero rompiendo la disciplina? ¡Ay, ay, ay!"


translate spanish day2_sl_bb1c83e5:


    "Perseguí una misteriosa sombra rápidamente pero con mucho cuidado."


translate spanish day2_sl_707dc7b1:


    "Me movía de camino en camino, y tras un rato me encontré en las profundidades del bosque perdiendo al fin la pista del extraño."


translate spanish day2_sl_7c11ada4:


    th "¿Quizá debería volver?"


translate spanish day2_sl_da7dcced:


    "Los árboles se abrieron tras de mi, y un magnífico panorama del pequeño lago del bosque sorprendió mi vista."


translate spanish day2_sl_9ae5e62c:


    "Y entonces me percaté de Slavya...{w} Estaba dando brincos agitadamente, desatándose el pañuelo y la camisa de pionera dejándolas flotar en el aire."


translate spanish day2_sl_e1672e9e:


    "Toda esa escena parecía incluso más fantástica que mi presencia en este campamento."


translate spanish day2_sl_4b243d51:


    "Slavya resultaba ser un espíritu del bosque o, tal vez, una ninfa."


translate spanish day2_sl_76f15cf3:


    "Ella parecía tan natural, más como una antigua diosa que una criatura humana."


translate spanish day2_sl_0ad8db2d:


    "Recordé todas las teorías teológicas, las que leí hace ya un tiempo."


translate spanish day2_sl_efc6aadb:


    "Me hizo recordar a la del panteísmo... la idea de que Dios está en toda la naturaleza, en cualquier cosa que vemos a nuestro alrededor."


translate spanish day2_sl_df1b0b99:


    th "¿Y si no hubieran sido los aliens o los viajes en el tiempo sino la Divina Providencia la que me trajo aquí?"


translate spanish day2_sl_9a178047:


    th "En efecto, Slavya mencionó que ella amaba la naturaleza."


translate spanish day2_sl_4850f462:


    th "Por lo que, incluso ella resulta ser un enigma, ¿acaso no lo es?"


translate spanish day2_sl_3565ced7:


    "Pero entonces todas las restantes ropas se le deslizaron cayendo al suelo y..."


translate spanish day2_sl_2dc5f1ec:


    "Slavya se fue dentro del agua.{w} Desnuda..."


translate spanish day2_sl_62969ec1:


    "Slavya se fue dentro del agua."


translate spanish day2_sl_3f9652c6:


    "Estaba avergonzado, pero no podía apartar mi vista de ella."


translate spanish day2_sl_62af0d4c:


    "La luz lunar plateada se reflejaba en su húmeda piel, haciendo que Slavya se pareciera a una antigua estatua griega.{w} Venus de Milo, ¿tal vez?"


translate spanish day2_sl_9a56444a:


    "Esta escena era tan fabulosa, que no había lugar para lujuria carnal... sólo sublime admiración de la verdadera belleza."


translate spanish day2_sl_88bcf51a:


    "Estaba simplemente disfrutando de Slavya con tierna admiración, olvidando cualquier cosa incluso la existencia."


translate spanish day2_sl_1275a4ec:


    "Tal vez, ¿esto no era el infierno sino el cielo?"


translate spanish day2_sl_fed951af:


    "Una branca de árbol crugió vilmente bajo mi pie. Slavya se giró, pero no había manera de que me reconociera en la oscuridad de la noche.{w} O al menos eso parecía."


translate spanish day2_sl_4f55ff6e:


    "Rápidamente salió fuera del lago, se puso sus ropas de manera rápida y la perdí de vista en el bosque."


translate spanish day2_sl_4c18ac16:


    "Tranquilamente fui tras ella."


translate spanish day2_sl_389d811b:


    "Slavya se adentró tranquilamente entre los árboles, escogiendo los caminos más fáciles y evitando graciosamente los troncos caídos, agujeros y obstáculos."


translate spanish day2_sl_1f1f282b:


    "Era una ardua tarea mantener el ritmo de ella. Aun más, definitivamente no quería ser descubierto. Primeramente, mirar a hurtadillas no era bueno. Y segundo, de hecho estaba por ver qué estaba haciendo ella aquí."


translate spanish day2_sl_10a06639:


    "Aunque de alguna forma parecía ser bastante inocente... al parecer no tenía nada que ver con mi presencia en este mundo."


translate spanish day2_sl_8edf9ccd:


    "Sólo era {i}inocente{/i}. No valia la pena mirar a hurtadillas."


translate spanish day2_sl_c2d72193:


    "Al final salimos a la plaza."


translate spanish day2_sl_bfc4d100:


    "Slavya se paró y se giró hacia mi."


translate spanish day2_sl_8f535089:


    sl "¿Crees que no me he dado cuenta de ti?"


translate spanish day2_sl_996aa1aa:


    "Estaba un poco confundido, pero traté de adoptar una aparente tranquilidad."


translate spanish day2_sl_a59596f3:


    me "¿Y desde cuándo?"


translate spanish day2_sl_ee6a8067:


    sl "No estoy segura..."


translate spanish day2_sl_abc9e8c7:


    "Slavya se acercó."


translate spanish day2_sl_7a2e9449:


    sl "Cinco minutos antes, quizá."


translate spanish day2_sl_c9c9cf9a:


    me "Por lo que, ¿también en el lago...?"


translate spanish day2_sl_44b1529f:


    sl "¿De qué lago estás hablando?"


translate spanish day2_sl_c97fadc0:


    me "Bueno..."


translate spanish day2_sl_f0f0ce17:


    "Slavya parecía estar sinceramente sorprendida, por lo que simplemente no podía imaginarme cómo estaba pretendiendo que nada había ocurrido o..."


translate spanish day2_sl_67207812:


    me "Bueno, olvídalo."


translate spanish day2_sl_8d771c48:


    "Me hice a la idea de comportarme como un caballero (tan educadamente como fuera posible en estas circunstancias) y no dije nada."


translate spanish day2_sl_af1d965e:


    sl "Muy bien."


translate spanish day2_sl_b1dd558c:


    "Inesperadamente fácil, estuvo de acuerdo."


translate spanish day2_sl_29a99ab6:


    sl "¡Qué noche más bonita!"


translate spanish day2_sl_532e90d7:


    "Slavya tomó asiento en un banco y miró hacia el cielo."


translate spanish day2_sl_71dfbb36:


    me "Supongo que dichas noches son frecuentes aquí."


translate spanish day2_sl_19f9cb53:


    sl "Bueno, probablemente..."


translate spanish day2_sl_437776df:


    me "¿Por qué lo dudas?"


translate spanish day2_sl_e064e531:


    sl "No lo sé, sólo estoy pensando."


translate spanish day2_sl_c233b6a2:


    me "¿Pensando sobre qué?"


translate spanish day2_sl_7ef301f9:


    "Ella se quedó mirándome fijamente como si estuviera buscando algo en mi rostro, pero luego volvió a contemplar las estrellas."


translate spanish day2_sl_c55641df:


    sl "De vez en cuando estoy de ese humor por la noche...{w} Durante el día estoy ajetreada en el trabajo sin apenas tiempo para relajarme, mientras que por la noche se está tan tranquilo aquí."


translate spanish day2_sl_da5951c8:


    sl "Si no hubiera sido por los grillos y las aves nocturnas, una se sentiría como si estuviese cara a cara con el Universo."


translate spanish day2_sl_6c369edd:


    "Por alguna razón no consideré a Slavya como alguien que hablara sobre semejantes asuntos."


translate spanish day2_sl_3229d777:


    me "Por lo que a mi respecta, se está incluso demasiado tranquilo aquí."


translate spanish day2_sl_5606d7d1:


    sl "¿Lo es?"


translate spanish day2_sl_0c5e8544:


    me "Sí, de verdad. ¿Tiene algo de malo?"


translate spanish day2_sl_0f20c625:


    sl "Bueno..."


translate spanish day2_sl_fcc29073:


    sl "¡Muy bien!"


translate spanish day2_sl_91f795db:


    "Ella se levantó brúscamente y se alisó la falda."


translate spanish day2_sl_411ed0f9:


    sl "Es la hora de irse a la cama."


translate spanish day2_sl_c3abce40:


    me "¡Buenas noches!"


translate spanish day2_sl_ee02ee2d:


    "La miré como se iba caminando."


translate spanish day2_sl_91aa9e4f:


    "Nuestra conversación podría haber sido sobre cualquier otra cosa sin importancia, pero me pareció que tenía un profundo y secreto significado que sólo podía aparecer {i}aquí{/i}, solamente junto a Slavya."


translate spanish day2_sl_e881ac0f:


    "Resultaba que aun en mi situación, este tipo de momentos de paz y serenidad eran absolutamente necesarios. Me hacían sentir unido al Universo."


translate spanish day2_sl_fe349bbe:


    "Diría incluso que vitales... ¡especialmente ahora!"


translate spanish day2_sl_a20cefa7:


    "..."


translate spanish day2_sl_a20cefa7_1:


    "..."


translate spanish day2_sl_41f711f9:


    "No estoy seguro de cuánto tiempo transcurrí sentado aquí, pero después de un rato comencé a sentirme soñoliento."


translate spanish day2_un_197a0aaf:


    "Me quería ir."


translate spanish day2_un_146e98d1:


    th "¡Cómo pude perder en la primera ronda...!"


translate spanish day2_un_a3a64f91:


    "No tenía excusa."


translate spanish day2_un_5e63525e:


    "La pista de deportes parecía ser el mejor lugar para mi soledad por ahora."


translate spanish day2_un_df7b3cbc:


    th "¿A quién se le ocurriría ir a jugar a fútbol por la noche?"


translate spanish day2_un_579f8206:


    "Me senté en un banco cerca de la pista y empecé a pensar sobre lo que había sucedido."


translate spanish day2_un_404f90fe:


    "Repentinamente desde el campo de voleibol comencé a escuchar ruidos que me parecieron como sonidos estridentes o silbidos."


translate spanish day2_un_a9cd9809:


    "Girándome vi a alguien que desesperadamente movía su mano de manera jovial."


translate spanish day2_un_8464c860:


    th "¿A quién le estará haciendo ese gesto?"


translate spanish day2_un_1e068335:


    "Para mi asombro era Lena."


translate spanish day2_un_fec5d55a:


    "Estaba lazando el volante de bádminton en el aire e intentaba golpearlo con la raqueta."


translate spanish day2_un_aa5dee50:


    "Sin embargo, para ser muy, muy sinceros, ella era pésima en bádminton."


translate spanish day2_un_3355bd20:


    "Sólo estuve mirando un rato, pero entonces decidí acercarme a ella."


translate spanish day2_un_24cfa746:


    "Fui alrededor del campo de la cancha de voleibol y me metí dentro de forma que ella pudiera verme."


translate spanish day2_un_8c20dbc2:


    "Lena era tímida como un ciervo, por lo que era mejor no repetir los errores pasados."


translate spanish day2_un_1a8f3935:


    me "¡Hola!"


translate spanish day2_un_5a070b64:


    "Ella me contempló e inmediatamente escondió su raqueta y el volante de bádminton detrás de ella."


translate spanish day2_un_9938563a:


    me "¿Te gusta el bádminton?"


translate spanish day2_un_9c33c3aa:


    un "En verdad no..."


translate spanish day2_un_dd278e82:


    me "Veo que no te va muy bien.{w} ¿Necesitas algo de ayuda?"


translate spanish day2_un_319450df:


    "A decir verdad, no era muy bueno en bádminton pero como cualquier otro niño solía jugar de tanto en tanto."


translate spanish day2_un_627e964d:


    me "Déjame que te enseñe."


translate spanish day2_un_626232a6:


    un "Gracias."


translate spanish day2_un_ac04f5d1:


    "Se ruborizó."


translate spanish day2_un_3883f3ff:


    un "Quería unirme al equipo de bádminton, pero realmente al final no lo contemplé."


translate spanish day2_un_aa50848d:


    un "Incluso a día de hoy tampoco lo intentaria, pero..."


translate spanish day2_un_418d4bde:


    "Ella me miraba."


translate spanish day2_un_90c16780:


    un "Nunca tenía suerte con las cartas, pero parecía que hoy estaba en racha. Así que pensé que me podría funcionar de la misma forma que con el bádminton..."


translate spanish day2_un_7d6095b6:


    "Tras estas palabras me di cuenta que perder con Lena era doblemente vergonzoso."


translate spanish day2_un_2a1db9ac:


    me "Nunca hubiera pensado que serías tan diestra con los deportes."


translate spanish day2_un_a852076f:


    "Se volvió a ruborizar."


translate spanish day2_un_0f6c5c86:


    me "Oh, discúlpame...{w} ¡Vamos venga, déjame enseñarte!"


translate spanish day2_un_3d8ebca6:


    "Cogí la raqueta, lancé el volante de bádminton hacia el aire y..."


translate spanish day2_un_b356ae1f:


    "Lo golpeé con tal fuerza que salió disparado volando por encima de la valla y se perdió entre los árboles."


translate spanish day2_un_9b63169a:


    me "¡Oh, lo siento!"


translate spanish day2_un_f1725684:


    "Guau, no hubiera esperado eso de mi."


translate spanish day2_un_5d87ca29:


    un "Olvídalo...{w} Aunque, era el último..."


translate spanish day2_un_c4393a9f:


    me "¿El último? ¡Vayamos a por él!"


translate spanish day2_un_9d020e39:


    un "Mejor que no...{w} Ahí... en el bosque..."


translate spanish day2_un_d5d9bec0:


    me "¿Quién hay allí? ¿Un duende?"


translate spanish day2_un_7c8e7385:


    "Solté una carcajada."


translate spanish day2_un_8a2da24a:


    un "Quizá..."


translate spanish day2_un_8def79fc:


    "Parecía ser el único que estaba bromeando."


translate spanish day2_un_b96a2bcb:


    me "No hay nadie de quién preocuparse, ¡venga vamos!"


translate spanish day2_un_5806cffb:


    un "Bueno, si estás conmigo..."


translate spanish day2_un_d8c460b7:


    "Nos alejamos de la pista de deportes, y comenzamos a inspeccionar los árboles."


translate spanish day2_un_965c33d2:


    "De repente, el graznido de una lechuza cortó la noche."


translate spanish day2_un_e9ba05f1:


    "Parecía que Lena estaba tan asustada que me agarró con ambas manos, dándome algo así como un abrazo."


translate spanish day2_un_8bfe647b:


    "Ese apretón fuerte era un poco incómodo."


translate spanish day2_un_827560fe:


    "¡Sentir el cuerpo de una muchacha y su calor tan cerca de mi!"


translate spanish day2_un_12cdb438:


    "Estaba colmado de ternura."


translate spanish day2_un_2b91a3d1:


    "Tenía un deseo profundo de protegerla y salvarla de cualquiera incluso si sólo era una lechuza u otro pájaro nocturno."


translate spanish day2_un_5e49a6dc:


    "Mi único deseo era preservar ese abrazo eternamente."


translate spanish day2_un_a89add5c:


    "Sin embargo, todas las cosas buenas llegan a su final tarde o temprano."


translate spanish day2_un_6c0ae31e:


    "Después de un rato me di cuenta que era un mochuelo el que graznaba en una branca próxima a nosotros."


translate spanish day2_un_12d3ce3e:


    "Estaba sujetando estrechamente nuestro volante de bádminton."


translate spanish day2_un_f751a0a0:


    me "¿Es de él de quien estabas tan asustada?"


translate spanish day2_un_0905212f:


    un "Ajá..."


translate spanish day2_un_69e96144:


    me "¡Echa un vistazo! No es tan asustadizo."


translate spanish day2_un_f200dfd5:


    "Lena lo miró detrás de mí."


translate spanish day2_un_6543e2da:


    un "Tienes razón, no asusta tanto..."


translate spanish day2_un_efd93d7c:


    me "Ey, espera un segundo."


translate spanish day2_un_ae039cd0:


    "Educadamente me aparté de su abrazo y me acerqué al mochuelo."


translate spanish day2_un_2aa9b5ff:


    "Al principio parecía que tenía algo de miedo y se apartó volando, dejando caer el volante de bádminton."


translate spanish day2_un_d348cee1:


    "Pero el mochuelo permanecía posado."


translate spanish day2_un_1ba95990:


    "Me las arreglé para hacerme con el volante de bádminton cogiéndolo y apartándolo del pájaro con cuidado."


translate spanish day2_un_56edd61c:


    me "¡Ey, mira! ¡Pero si es casi doméstico!{w} ¿Te gustaría tenerlo de mascota?"


translate spanish day2_un_db78f5e4:


    un "Tal vez, ¿en otro momento...?"


translate spanish day2_un_d01f4370:


    "Le devolví el volante a Lena."


translate spanish day2_un_3cd7a073:


    un "Gracias."


translate spanish day2_un_783cf09d:


    "Ella sonrió amablemente."


translate spanish day2_un_4d93b5df:


    un "Me tengo que ir."


translate spanish day2_un_0f243eb9:


    me "Buena suerte con el bádminton."


translate spanish day2_un_f5e7f5a8:


    "Lena sonrió otra vez, y se fue con prisa hacia el campamento."


translate spanish day2_un_4bf89e3c:


    th "¡Qué linda puede ser una muchacha!"


translate spanish day2_un_a20cefa7:


    "..."


translate spanish day2_us_ce80d80d:


    "Los sucesos del día anterior seguían brillando como imágenes en mi cabeza: aquella maldita e inútil lista, ese estúpido torneo..."


translate spanish day2_us_5cef682c:


    "Esta noche no tenía intención de hacer nada o de hablar con nadie.{w} Investigar mi complicada situación era lo último que haría en la Tierra en esta noche."


translate spanish day2_us_d9b0b0f8:


    "Me fui hacia el norte.{w} O al menos donde creía que estaba."


translate spanish day2_us_d66455f1:


    "Era mi costumbre desde joven... ir hacia el norte."


translate spanish day2_us_a488b614:


    "Me gustaba esa parte de mi ciudad natal más que los distritos del sur."


translate spanish day2_us_1759ce8e:


    "Viajar a los lugares de veraneo del Mar Negro tampoco eran lo mío... bosques y campos sin límites eran lo más adecuado para mi en lugar de las playas y los barjanes."


translate spanish day2_us_ac512323:


    "Unos minutos más tarde, un escenario al aire libre formado por varias filas de bancos de madera y una grada, apareció frente a mi vista."


translate spanish day2_us_0bd0ce88:


    "Subí a la grada."


translate spanish day2_us_ff3859bd:


    "Muchos y variados equipos de música... altavoces, un micrófono de pie e incluso un piano."


translate spanish day2_us_85b7add9:


    "Me imaginé que estaba frente a una gran audiencia, gritando y coreando mi nombre, mis ojos cegados por las luces del proyector."


translate spanish day2_us_aa3b726d:


    "Imaginando una guitarra en mis manos, intenté realizar un largo e impactante solo."


translate spanish day2_us_7fe7938c:


    "Supuse que se vería muy gracioso desde el punto de vista de un extraño... un tipo raro agitando sus brazos en la grada, haciendo el simio y poniendo caras."


translate spanish day2_us_dc4028dc:


    th "¡Espero que nadie me viera aquí!"


translate spanish day2_us_fa3c839e:


    us "¡Ey!"


translate spanish day2_us_b65b6fe7:


    "Se elevó una voz de algún lugar."


translate spanish day2_us_997dd5cf:


    "Miré arriba y vi a Ulyana colgada de la viga del techo de la grada."


translate spanish day2_us_89f814b5:


    us "¿Qué andas haciendo aquí?"


translate spanish day2_us_2ec2a349:


    me "Yo sólo..."


translate spanish day2_us_dab45ac8:


    "Negarlo era obviamente estéril."


translate spanish day2_us_cfae0a9c:


    me "Lo viste por ti misma, ¿no?"


translate spanish day2_us_7217c832:


    "Dije frustradamente y me giré."


translate spanish day2_us_a7445561:


    us "Oh, veo que tienes talento con la guitarra."


translate spanish day2_us_8f216cfd:


    "No dije nada."


translate spanish day2_us_57071005:


    us "Ey, vamos, ¡no me pongas esa cara! ¡Era bastante divertido!"


translate spanish day2_us_20f32dda:


    "Ella hizo una risita."


translate spanish day2_us_d0b077c3:


    me "Bastante divertido, ¿eh?"


translate spanish day2_us_2ff94fc7:


    "Resoplé."


translate spanish day2_us_df6dbf74:


    us "Sep."


translate spanish day2_us_69b6d036:


    "Respondió tranquilamente Ulyana."


translate spanish day2_us_400c9ff0:


    us "Ven aquí."


translate spanish day2_us_25f7abbc:


    me "¿Dónde?"


translate spanish day2_us_8b888dc6:


    us "¡Conmigo!"


translate spanish day2_us_ae5dfee6:


    me "No voy a subirme ahí, ¡no trates de convencerme!"


translate spanish day2_us_39336f89:


    "No es que tenga vértigo. Subir ahí simplemente no tiene sentido, ¿o acaso lo tiene?"


translate spanish day2_us_053e61c9:


    us "¡No! Pues quédate ahí abajo."


translate spanish day2_us_45d1c061:


    "Sentí en mis huesos que algo iba mal, aun así me dirigí hacia ella."


translate spanish day2_us_d926232c:


    "Cuando me hallé debajo de Ulyana, ella gritó:"


translate spanish day2_us_5736d39d:


    us "¡Al agua pato!"


translate spanish day2_us_b2c5ecfe:


    "Y saltó..."


translate spanish day2_us_b9b5fa03:


    "Cientos de pensamientos aparecieron como destellos frente a mi en un instante."


translate spanish day2_us_20495ccc:


    th "¿Cómo iba a cogerla? ¿Valía la pena intentarlo? ¿Y si se moría? ¿Y si ella me rompe algo? ¡¿Por qué demonios me tiene que ocurrir a mi?!"


translate spanish day2_us_614bbca7:


    th "Es culpa suya... ¡No perdamos más el tiempo!"


translate spanish day2_us_7c464e77:


    "Guau, cuántos pensamientos cruzan por la mente en un abrir y cerrar de ojos."


translate spanish day2_us_54bb57e2:


    "Mientras a veces, muchos años no son suficientes para lograr hacerse una idea."


translate spanish day2_us_39a5f3d1:


    "Al final la lógica y el instinto de supervivencia ganaron la batalla, así que di un paso atrás."


translate spanish day2_us_818e5d64:


    "Ulyana aterrizó suavemente, cayendo, y al instante se puso de pie y me miró ofendida."


translate spanish day2_us_9a98847e:


    us "Santo cielo, ¿por qué no me cogiste?"


translate spanish day2_us_5181be41:


    me "No te has herido..."


translate spanish day2_us_37dbf984:


    "Respondí, cambiando la mirada."


translate spanish day2_us_a7d45458:


    us "¿Y si me hubiera hecho daño?"


translate spanish day2_us_c1b7ead1:


    me "¡Pero no ha sido así!{w} ¿Qué pasa contigo? ¿Has visto demasiadas películas de serie B?"


translate spanish day2_us_d3a6c509:


    us "Así que, ¿no te preocupas por mi?"


translate spanish day2_us_f396e777:


    "Ella sonrió."


translate spanish day2_us_89c187b4:


    me "Bueno, en esta situación... Ciertamente, me preocupo."


translate spanish day2_us_c906b0f6:


    us "Me siento halagada."


translate spanish day2_us_8de3d1a0:


    me "Ey, quítate de la cabeza esa idea..."


translate spanish day2_us_630ea867:


    us "Vale, vale. Estás perdonado por lo de las cartas."


translate spanish day2_us_0a94a0ea:


    me "Y tú no lo estás..."


translate spanish day2_us_39d27213:


    "No tuve tiempo de acabar mi frase... Ulyana saltó de la grada y se desvaneció en la oscuridad de la noche."


translate spanish day2_us_bb143e3a:


    th "Oh, sep. Otra criaturada más de esa tonta muchacha."


translate spanish day2_us_b40ec65c:


    th "Claro que estaba preocupado por ella en ese momento."


translate spanish day2_us_2b9842d5:


    th "Como cualquiera lo estaría en su lugar..."


translate spanish day2_us_78ddfb9b:


    "Después de maldecir una vez más a Ulyana en mi cabeza, me fui hacia mi cabaña."


translate spanish day2_main4_6f1dc765:


    "Por primera vez hoy, finalmente sentí cómo de cansado estaba."


translate spanish day2_main4_3fd94a8a:


    "No había ninguna luz en la ventana, por lo que Olga Dmitrievna debía estar durmiendo ya."


translate spanish day2_main4_9390dd47:


    th "Qué raro, ayer ella me esperó."


translate spanish day2_main4_aceec196:


    "Entré en la cabaña, tranquilamente me cambié y me estiré en la cama."


translate spanish day2_main4_2b052db0:


    th "Cuando lo piensas, mi situación no se ha esclarecido nada hoy."


translate spanish day2_main4_0abec8be:


    th "De hecho, me he pasado todo el día haciendo cosas inútiles; incluso nunca hubiera pensado en hacer algo como esto en el mundo real."


translate spanish day2_main4_4b59bf55:


    th "Aunque tenía mucho tiempo."


translate spanish day2_main4_0a500eea:


    th "Cuánto tiempo tendría para estar aquí en total... era todavía un misterio."


translate spanish day2_main4_381a6c2e:


    th "Tal vez una eternidad, o quizá solamente quedaban unos pocos minutos."


translate spanish day2_main4_c21f9a60:


    "Simplemente no quería pensar sobre el pasado, sobre cómo llegué a este campamento."


translate spanish day2_main4_3d823552:


    "Por primera vez en mucho tiempo me sentía realmente muy cansado... no solamente emocionalmente cansado, sino también físicamente, psicológicamente y Dios sabe qué más..."


translate spanish day2_main4_5016abac:


    "Solo quería que todos y todo se largaran... empezando por mis propios pensamientos.{w} Quería que alguien resolviera este lío por sí mismo."


translate spanish day2_main4_ceda5e44:


    "O al menos sin mi necesaria ayuda."


translate spanish day2_main4_dabe5250:


    th "¿Y qué ocurre si me quedo atrapado aquí para siempre?"


translate spanish day2_main4_92283096:


    th "Entonces tendré que acostumbrarme a ello..."


translate spanish day2_main4_410fa16d:


    th "¿Y así sin más...? Yo... No estoy preparado...{w} Ejem..."


translate spanish day2_main4_dcadb353:


    "Mi conciencia se me escapó volando lentamente y se volvió progresivamente más difícil concentrarse en alguna cosa distinta."


translate spanish day2_main4_72f8d69d:


    th "Quizá sea mejor esperar hasta mañana..."


translate spanish day2_main4_ba8204f7:


    "Me di la vuelta y caí dormido."
# Decompiled by unrpyc: https://github.com/CensoredUsername/unrpyc
