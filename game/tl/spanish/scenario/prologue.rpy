
translate spanish prologue_24958abb:


    "Estaba teniendo ese sueño una vez más."


translate spanish prologue_07dac942:


    "{i}Ese{/i} sueño..."


translate spanish prologue_55dfd177:


    "...el mismo de cada noche."


translate spanish prologue_9ce34629:


    "Pero se olvida todo a la mañana siguiente, como de costumbre."


translate spanish prologue_0c90c0c8:


    "Quizás es lo mejor..."


translate spanish prologue_8df02469:


    "Sólo un recuerdo de la memoria permanecerá, de puertas medio abiertas como si me invitaran a algún lugar, con dos pioneros de fría piedra cerca."


translate spanish prologue_deca17df:


    "Y también aquella extraña muchacha...{w} Que sigue preguntándome:"


translate spanish prologue_814eff1e:


    dreamgirl "¿Quieres venir conmigo?"


translate spanish prologue_e7b0f130:


    "¿Venir...?"


translate spanish prologue_a2e02439:


    "Pero adónde..."


translate spanish prologue_fe88af01:


    "y... ¿por qué?"


translate spanish prologue_4d0aa097:


    "¿Y dónde estoy, de todos modos?"


translate spanish prologue_0683e19b:


    "Claro, si todo hubiera sucedido en la vida real, entonces estaría ciertamente asustado."


translate spanish prologue_233a6b9d:


    "¡Eso es lo más probable!"


translate spanish prologue_cd46d7f7:


    "Pero esto es solamente un sueño.{w} El mismo sueño de cada noche."


translate spanish prologue_af4b467b:


    "¡Tiene que haber un motivo!"


translate spanish prologue_4cd30aa0:


    "No tienes porqué saber {i}dónde{/i} o {i}por qué{/i} para darte cuenta: realmente algo está ocurriendo."


translate spanish prologue_2e4e4eec:


    "Algo, apremiante para mi atención."


translate spanish prologue_7bd50019:


    "¡Ya que todo lo que me rodea aquí es real!"


translate spanish prologue_354cb9c5:


    "Tan reales como las cosas de mi propio piso: podría abrir las puertas, escuchar como crujen las bisagras, rascar la herrumbre desmigajada con mis manos, inhalar el frío aire y tiritar de frío."


translate spanish prologue_5fb243fe:


    "Podría; pero para hacer eso, necesitaría levantarme, dar un paso, acercar mi mano..."


translate spanish prologue_a6f914fa:


    "Pero esto es un sueño. Lo entiendo, pero ¿y qué? ¿Acaso cambiaría mi {i}comprensión{/i}?"


translate spanish prologue_edb5109a:


    "Porque estar aquí es como estar en el otro lado de una pantalla estropeada de una vieja televisión, la cual lucha contra el ruido estático y se esfuerza por mostrar todo a su audiencia sin perderse un ápice de detalle."


translate spanish prologue_28f18900:


    "Pero la imagen se vuelve borrosa...{w} Debo despertar pronto."


translate spanish prologue_a20cefa7:


    "..."


translate spanish prologue_14a2c666:


    "¿Quizás debería preguntarle algo?{w} A la muchacha."


translate spanish prologue_5da0f47a:


    "Cuál es su nombre..."


translate spanish prologue_d7b38ee8:


    "Acerca de las estrellas, por ejemplo..."


translate spanish prologue_82d77cb1:


    "Aunque, ¿por qué las estrellas?"


translate spanish prologue_51851d8b:


    "¡Mejor pregunto sobre las puertas!{w} ¡Sí, las puertas!"


translate spanish prologue_e5f4ee19:


    "Ella se sorprendería mucho."


translate spanish prologue_1edb57c8:


    "o mejor, ¿por qué escribimos la {i}h{/i} al inicio de algunas palabras si no la pronunciamos aspirada?"


translate spanish prologue_3234148c:


    "Bonita letra..."


translate spanish prologue_2367bcf1:


    "¡Como si ya no existiera!"


translate spanish prologue_d92e974d:


    "Aun así, ¿qué tienen que ver con este lugar las letras, las puertas y las estrellas?"


translate spanish prologue_e2138459:


    "Porque si cada noche tengo {i}este{/i} sueño, el cual se olvidará pronto de todos modos, ¡tengo que buscar respuestas aquí y ahora!"


translate spanish prologue_d5050891:


    "Y allí, si te fijas bien, puedes observar las Nubes de Magallanes..."


translate spanish prologue_e2b591e8:


    "¡Como si hubiera acabado en el hemisferio sur!"


translate spanish prologue_a20cefa7_1:


    "..."


translate spanish prologue_22d0eaf7:


    "En un sueño, en el que hay pequeñas cosas que atrapan la mayor parte de tu atención: un color de hierba no natural, curvas imposibles en líneas rectas o tu propio reflejo distorsionado... mientras el verdadero peligro, que podría poner un final aquí y ahora a todo, pareciera una nimiedad."


translate spanish prologue_26999fe4:


    "Es natural, ya que {i}aquí{/i} no puedes morir."


translate spanish prologue_a6da1123:


    "Lo sé con seguridad... Lo he hecho cientos de veces."


translate spanish prologue_e85b8394:


    "Pero si no pudieras morir, ¿hay alguna razón para vivir?"


translate spanish prologue_a0462877:


    "Podría preguntarle a la muchacha. Ella es una lugareña, podría saberlo."


translate spanish prologue_ee712a8b:


    "¡Sí, exactamente!{w} Podría preguntarle sobre el búho, por ejemplo."


translate spanish prologue_b0865ff2:


    "Es un pájaro extraño..."


translate spanish prologue_2be63cfe:


    "Aunque, no importa..."


translate spanish prologue_a20cefa7_2:


    "..."


translate spanish prologue_814eff1e_1:


    dreamgirl "¿Quieres venir conmigo?"


translate spanish prologue_db203a62:


    "Y cada vez tengo que contestarle."


translate spanish prologue_7ad37ff1:


    "Es la única forma, de lo contrario el sueño no terminará, y... nunca me despertaré."


translate spanish prologue_027137ed:


    "Cada vez es más difícil decidir la respuesta."


translate spanish prologue_34fac5fe:


    "¿Dónde estoy? ¿Qué estoy haciendo aquí? ¿Quién es ella?"


translate spanish prologue_071572e1:


    "¿Y por qué mi vida depende tanto de esta respuesta?"


translate spanish prologue_ec721a0b:


    "¿O quizá no?"


translate spanish prologue_51d80ee4:


    "Después de todo solamente es un sueño."


translate spanish prologue_ca1b37f9:


    "Sólo un sueño..."


translate spanish prologue_41cdd4ce:


    "La pantalla de la computadora se quedó mirándome fijamente como si estuviera viva."


translate spanish prologue_86cfd3d1:


    "A veces realmente me pareció que era consciente de sí misma, que tenía sus pensamientos, deseos y ambiciones; que tenía sentimientos, pudiera amar y sufrir."


translate spanish prologue_b3960be5:


    "Como si en nuestra relación, la pantalla no fuera un objeto... sino que fuera yo el pedazo de plástico y textolita."


translate spanish prologue_67859f20:


    "Hay cierta verdad en ello, probablemente porque la computadora me proporciona el 90%% de mi comunicación con el mundo exterior."


translate spanish prologue_e051da8b:


    "Tablones de imágenes anónimas, algunos chats de tanto en tanto, raramente ICQ o Jabber, y foros aun más raramente."


translate spanish prologue_3f938594:


    "¡La gente al otro extremo de internet simplemente no existe!"


translate spanish prologue_5a1a52c8:


    "Todos ellos son simplemente creaciones de su enferma imaginación, un error en su código fuente o un bug del kernel, el cual empezó a vivir una vida propia."


translate spanish prologue_2728421d:


    "Si uno mirase mi existencia aparte, tales pensamientos no parecerían tan disparatados, y un psicólogo seguramente me daría un puñado de sofisticados diagnósticos y tal vez me escribiría un prospecto médico para enviarme al psiquiátrico."


translate spanish prologue_9d607629:


    "Un pequeño piso sin rastro de reparación o parecido de orden en él, y siempre desde la ventana la misma vista de la gris y agitada megalópolis de día y noche en algún lugar... tales son las condiciones de mi vida."


translate spanish prologue_5823e040:


    "Bueno, de acuerdo, no comenzó todo de esta manera..."


translate spanish prologue_7380c69b:


    "Nací, fui a la escuela y la finalicé... como todos los demás."


translate spanish prologue_e5f87d9f:


    "Fui aceptado en la universidad, donde transcurrí un año y medio yendo mal y pasando apuros."


translate spanish prologue_c895b6f5:


    "Estuve vagando en diversos empleos.{w} A veces funcionaba bastante bien, en otras ocasiones incluso cobraba decentemente por ello."


translate spanish prologue_26348b28:


    "Pero sentí que todo ello no fuera mío, como si fuera en realidad parte de la biografía de otro hombre."


translate spanish prologue_111ab6cc:


    "No estaba viviendo una vida en su plenitud... estaba en un bucle de círculos monótonos.{w} Como en la película «El Día de la marmota»."


translate spanish prologue_bab8b972:


    "Sólo que no tenía opción de elegir en cómo transcurrir mi día, y cada día se repitiera con la misma espiral viciosa.{w} La espiral del vacío, la miseria y la desesperación."


translate spanish prologue_76b2fe88:


    nvl clear


translate spanish prologue_b41ec809:


    "Durante los últimos pocos años sólo me senté delante de la pantalla todo el día."


translate spanish prologue_c39afb80:


    "A veces había trabajos no cualificados, en otras ocasiones mis padres me ayudaron."


translate spanish prologue_dfb24146:


    "A fin de cuentas, era capaz de sustentarme por mi mismo."


translate spanish prologue_ba1a156e:


    "Realmente no es de extrañar, ya que mis necesidades eran más bien pocas."


translate spanish prologue_9bc39b5c:


    "Casi no salgo de casa, y mi comunicación con el resto de la gente prácticamente consiste en correspondencia online con {i}los anónimos{/i}, quienes no tienen un nombre real, ni sexo ni edad."


translate spanish prologue_a3a04a99:


    "Así que, resumiendo, una vida bastante típica de una típica persona antisocial de su época.{w} Un tipo de Donnie Darko en pequeña escala, sin visiones relacionadas con el fin del mundo."


translate spanish prologue_ac5e97c3:


    "Quizás algún importante y respetable director hará una película sobre mi, y se convertirá en un clásico del cine moderno dramático.{w} O tal vez haré una yo mismo..."


translate spanish prologue_eea3f384:


    "Sin embargo, qué sentido tiene engañarme a mi mismo... Lo he intentado muchas veces, pero aun así no podría siquiera lograr un simple guión."


translate spanish prologue_6d041340:



    nvl clear
    "También he intentado aprender muchas otras cosas."


translate spanish prologue_64793d14:


    "No tengo talento para dibujar.{w} Programar... me aburrí.{w} Idiomas extranjeros... requiere mucho tiempo..."


translate spanish prologue_b41193f7:


    "La única cosa que me gustaba hacer era leer, pero aun así no me considero a mi mismo como un erudito."


translate spanish prologue_d0321015:


    "Probablemente era un as en ver anime y el rey de las bromas sosas de internet."


translate spanish prologue_acb955ab:


    "Si tuviera que cobrar por ello, probablemente sería una persona feliz (y una persona rica también), pero dudo que llenara el vacío de mi interior."


translate spanish prologue_76b2fe88_1:


    nvl clear


translate spanish prologue_8d743298:


    "Hoy fue otro típico día de un típico perdedor en su típica vida."


translate spanish prologue_4d249ee8:


    "Y hoy es el día en el que tengo que ir a una reunión de mi universidad."


translate spanish prologue_3d7e70ae:


    "Hablando francamente, realmente no quiero ir."


translate spanish prologue_556a5316:


    "¿Y qué sentido tiene? Si a fin de cuentas fue poco el tiempo que pasé con ellos."


translate spanish prologue_b84fba8f:


    "Sin embargo, fui persuadido por un amigo: mi actual colega de universidad y uno de los pocos con quien tengo contacto no solamente por Internet."


translate spanish prologue_a2136874:


    "Una helada noche.{w} La parada de autobús. Esperando."


translate spanish prologue_55a843c8:


    "Nunca me gustó el invierno.{w} A pesar de que el verano tampoco es mi estación."


translate spanish prologue_6c43f774:


    "Es sólo que no veo motivo alguno para preferir cualquier época del año, no importa mucho qué tiempo hace ahí fuera cuando pasas toda la semana en casa."


translate spanish prologue_a43f6da8:


    "Hoy el autobús tardaba tanto en venir, que estaba a punto de maldecirlo todo y gastar los últimos cientos de rublos en un taxi (la idea de volver a casa no cruzó por mi mente por alguna razón)."


translate spanish prologue_42c6ca3e:


    "Como de costumbre, millones de pensamientos pasaron por mi mente, pero no había ni uno solo que fuera útil como para ser aprovechado."


translate spanish prologue_ebf71c4c:


    "Ese tipo de pensamiento que podrías hacerlo realidad, darle forma, convertirlo en una idea y ponerlo en práctica."


translate spanish prologue_6b447492:


    "¿Quizá podría empezar por mi propio negocio?{w} ¿Pero de dónde sacaría el dinero?"


translate spanish prologue_1263d7e9:


    "¿O tal vez debería volver a trabajar en una oficina?{w} ¡No, de ninguna manera!"


translate spanish prologue_ef2e5c25:


    "¿A lo mejor debería intentar ser autónomo?{w} ¿Pero qué habilidades tengo? Quién me necesita después de todo..."


translate spanish prologue_42e509b4:


    "De repente recordé mi infancia...{w} O mejor, mis años de adolescencia, la época en que tenía 15-17 años."


translate spanish prologue_153757bd:


    "¿Por qué exactamente aquellos años?{w} Ni idea."


translate spanish prologue_b3ca007f:


    "Supongo que porque entonces, todo era mucho más simple."


translate spanish prologue_74b11ddc:


    "Era más fácil tomar decisiones, tan complicadas ahora y tan sencillas entonces."


translate spanish prologue_a0da84d1:


    "Despertándome por la mañana, sabía exactamente cómo iba a transcurrir mi día, y siempre estaba esperando con impaciencia que llegara el fin de semana... entonces podría descansar algo y tener tiempo para las cosas que me gustaban: ordenadores, fútbol y salir con mis amigos."


translate spanish prologue_7160de27:


    "Y entonces, al comienzo de la siguiente semana, retomaba mis estudios de nuevo."


translate spanish prologue_334f2acb:


    "Tiempo atrás, no había lugar a cuestiones como «por qué», «quién lo necesita», «qué cambiará si lo hago» o «qué no cambiará»."


translate spanish prologue_272df669:


    "Una vida corriente, tan casual para cualquier persona normal y tan extraño para mi hoy día."


translate spanish prologue_8963d5d3:



    nvl clear
    "Descuidada infancia...{w} Fue también entonces cuando encontré mi primer amor."


translate spanish prologue_4a6ac0a3:


    "Su apariencia y carácter se han desvanecido de mi memoria."


translate spanish prologue_7493b623:


    "Solamente su nombre permanece, como una breve línea de un perfil de red social, junto con los sentimientos, que me abrumaban cuando estaba con ella.{w} El afecto, la ternura y el deseo de cuidarla y protegerla."


translate spanish prologue_064fa38f:


    "Tristemente, no duró mucho."


translate spanish prologue_c7008ed9:


    "Hoy día difícilmente puedo imaginar que ocurra algo así."


translate spanish prologue_f716a97b:


    "Probablemente me gustaría conocer a una muchacha, pero no sé cómo empezar una conversación, qué demonios discutir y cómo atraerla."


translate spanish prologue_d631396b:


    "Bueno, no he conocido una muchacha idónea en mucho tiempo.{w} Pero dónde podria conocer una..."


translate spanish prologue_76b2fe88_2:


    nvl clear


translate spanish prologue_52032d61:


    "El ruido de un motor de vehículo me trajo a la realidad."


translate spanish prologue_5d936a59:


    "Un autobús se detuvo."


translate spanish prologue_68c6dc22:


    "Hay algo anormal en él... pensé."


translate spanish prologue_f260960e:


    "Por otro lado, no importa: solamente la línea «410» circula por esta ruta."


translate spanish prologue_3cf36c8d:


    "Las luces de la calle me alumbran. Es como si su fría luz chispeara dentro de mi, tratando de iluminar mis sentimientos largo tiempo muertos."


translate spanish prologue_ac957580:


    "O tal vez no chispean, sólo los despiertan..."


translate spanish prologue_b9473edc:


    "Porque aquellos sentimientos, han estado viviendo apaciguados en mi por mucho tiempo, despertándose de nuevo."


translate spanish prologue_59544860:


    "La radio del conductor estaba emitiendo alguna melodía familiar.{w} Pero no la estaba escuchando."


translate spanish prologue_53102b62:


    "Estaba viendo circular los coches a través de la empañada ventana."


translate spanish prologue_0931b53b:


    "Ya que la gente está siempre ajetreada hacia algún lugar, persiguiendo algo que necesitan, recluidos en sus propios pequeños mundos, ¿por qué iban a preocuparse por el mío?"


translate spanish prologue_5ccdd86f:


    "Probablemente ellos tengan sus propios complicados problemas; o, quizá, tengan vidas más fáciles."


translate spanish prologue_813e6d62:


    "No lo puedes saber con seguridad, ya que todas las personas son distintas.{w} ¿O no lo son?"


translate spanish prologue_e0c41f97:


    "A veces, las acciones de alguien pueden ser fácilmente predecibles, pero si tratas de ver en lo profundo de su alma, sólo hallarás una oscuridad impenetrable."


translate spanish prologue_a20cefa7_3:


    "..."


translate spanish prologue_9f51c5b0:


    "El autobús se acercaba al centro de la ciudad, y mis pensamientos fueron interrumpidos por las brillantes luces de ésta última."


translate spanish prologue_1029ada9:


    "Cientos de carteles de anuncios, miles de coches, millones de personas."


translate spanish prologue_d443aa58:


    "Estaba viendo este espectáculo de luces, y de alguna manera me dio un terrible sueño."


translate spanish prologue_d56c688b:


    "Mis ojos se cerraron por un momento y entonces..."
# Decompiled by unrpyc: https://github.com/CensoredUsername/unrpyc
