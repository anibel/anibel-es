
translate english day5_main1_aa4d323e:


    "We ran...{w} Ran with the last of our strength..."


translate english day5_main1_11656716:


    "Like those who run for their precious lives."


translate english day5_main1_5edff0f6:


    "Like a doomed person, knowing that there is no hope to save his life, will still fight the inevitable and his own fate... "


translate english day5_main1_5db27d0a:


    "I barely managed to close the heavy metal door behind me."


translate english day5_main1_92ce41cd:


    th "I have no idea how deep this bomb shelter is or if it's able to withstand a nuclear blast, but we have no other place to hide..."


translate english day5_main1_4c9141de:


    "She gripped my hand tight."


translate english day5_main1_6f240b82:


    me "Don't be afraid..."


translate english day5_main1_81e0185f:


    "Bits of the ceiling were falling and the walls were shaking."


translate english day5_main1_43320cff:


    "I was prepared for the worst."


translate english day5_main1_eabbd6cf:


    "But death is the kind of thing that you can't ever be prepared for..."


translate english day5_main1_aceafbaa:


    "But suddenly, complete silence fell. It rang even louder than the explosions."


translate english day5_main1_e7df2349:


    dreamgirl "Maybe it's time to say goodbye..."


translate english day5_main1_1f90d83e:


    "She was whimpering."


translate english day5_main1_8ba5cb54:


    "I wanted to comfort her somehow, but realised that there is nothing I could do."


translate english day5_main1_9513cd87:


    me "Yes..."


translate english day5_main1_b0692838:


    dreamgirl "You know, I..."


translate english day5_main1_337804c3:


    "A horrible bang almost split my ear-drums."


translate english day5_main1_0ac01129:


    "It seems that I'm under a piece of collapsed ceiling but I don't feel any pain."


translate english day5_main1_02f7c13f:


    "All I want is to not let her go..."


translate english day5_main1_893465aa:


    "I woke up in a cold sweat, short of breath and gasping for air."


translate english day5_main1_8d045bac:


    "It took me some time to come to my senses."


translate english day5_main1_1d678e2c:


    me "It was a dream…{w} It was just a dream…"


translate english day5_main1_85574277:


    "My questioning mind, however, refused to believe it."


translate english day5_main1_cba74cb0:


    th "But who was that girl with me there?"


translate english day5_main1_92ca5a14:


    "I didn't want to let go of her hand so desperately..."


translate english day5_main1_d2a550ca:


    "Sadly I couldn't recall her at all."


translate english day5_main1_cfe2aec8:


    "The clocks was showing few minutes past ten."


translate english day5_main1_e1d34af1:


    "I slowly came to my senses as reality started to stake out its claim on my mind – and my stomach foully growled."


translate english day5_main1_4e2d25a2:


    me "Alright – war is war, but lunch should be served in due time!"


translate english day5_main1_f33a92de:


    "It turned out that Olga Dmitrievna wasn't here – she must have decided not to wake me up."


translate english day5_main1_c0506319:


    th "Well, thanks go out to our camp leader for this!"


translate english day5_main1_d29a68d9:


    "After yesterday's adventures I had to have a rest."


translate english day5_main1_fc576f62:


    "Last night was remained a blurry memory which I really didn't want to think about."


translate english day5_main1_358f1320:


    "It's now more important to find something to eat and...{w} to wash myself!"


translate english day5_main1_16f93484:


    th "Exactly!"


translate english day5_main1_c02083da:


    th "Because a pioneer must always be clean and tidy."


translate english day5_main1_15333fca:


    "Though I would agree with this principle even if I wasn't a pioneer (and as a matter of fact, I'm not)."


translate english day5_main1_f50cd452:


    "On my way to the washstands I met Electronik."


translate english day5_main1_7e4abd77:


    "He started to wave his hands and ran up to me."


translate english day5_main1_5237cfbf:


    el "Good morning!{w} Thank you for finding Shurik! Without him I don't even..."


translate english day5_main1_b97e3fd5:


    me "It’s nothing..."


translate english day5_main1_dab051d6:


    "I was a bit embarrassed."


translate english day5_main1_611aba0e:


    el "No, really! Don't be so shy – the country must be proud of its heroes!"


translate english day5_main1_04210531:


    me "And what about Shurik... How did he look this morning?{w} Is he alright?"


translate english day5_main1_c4c5697f:


    "After yesterday's craziness, I thought such a question was completely valid."


translate english day5_main1_805f0715:


    el "Yes, absolutely!{w} The only thing is that he can't remember anything..."


translate english day5_main1_859bc359:


    me "Can’t he?"


translate english day5_main1_2850cdb5:


    "I wasn't surprised at all."


translate english day5_main1_4819f710:


    el "He says that he went to the abandoned camp yesterday, and then... Woke up in his bed in the morning.{w} I mean he remembers nothing between those two events."


translate english day5_main1_5ec2624e:


    me "I see...{w} Alright then…"


translate english day5_main1_caccdbd7:


    el "You missed breakfast, right? Come to our club! We'll feed you! I have something special."


translate english day5_main1_89ac8d3c:


    "Electronik smiled in a conspiratorial way."


translate english day5_main1_94afa350:


    me "Thanks, I'll come, probably..."


translate english day5_main1_7f29f4c2:


    "I had to wash myself first anyway."


translate english day5_main1_c6f70531:


    el "We'll be waiting!"


translate english day5_main1_641f39b8:


    "He waved at me and left to carry out his own business."


translate english day5_main1_c64ccad8:


    "There was nobody near the washstands."


translate english day5_main1_127f580b:


    "The water turned out surprisingly warm today."


translate english day5_main1_375200fb:


    th "It's been warmed up already I guess..."


translate english day5_main1_b4c71998:


    "Having my face washed I realised that it wouldn't be easy to wash the rest of my body here."


translate english day5_main1_e7b1f5de:


    th "Maybe I should go to the showers..."


translate english day5_main1_540ebe5d:


    th "But since there is nobody here..."


translate english day5_main1_242cdd8f:


    "I turned the tap in such a way so water streamed parallel to the ground and started to take off my clothes."


translate english day5_main1_8e855925:


    th "And what if somebody sees me?"


translate english day5_main1_6f67ff49:


    th "Well, I'll rinse and dry myself quickly, and put on my clothes..."


translate english day5_main1_315b7c24:


    "The water, which seemed warm on my hands and face, felt bone-chillingly cold on my body."


translate english day5_main1_a862e7c2:


    "The whole washing process took no longer than ten seconds and I started to wipe myself quickly afterwards."


translate english day5_main1_203544b1:


    "But I didn't managed to finish anyway – there were voices coming in from the direction of the footpath!"


translate english day5_main1_8c7c1ae8:


    "The only solution came to me in a split second – I grabbed my clothes and dashed into the bushes."


translate english day5_main1_374a9333:


    "A moment later Alisa and Ulyana appeared near the washstands."


translate english day5_main1_150b3ccc:


    dv "You could have done it by yourself! Why did you bring me here?"


translate english day5_main1_88bcfdf2:


    us "Is it a big deal for you?"


translate english day5_main1_cc876a45:


    dv "Fine, let me…"


translate english day5_main1_657eb718:


    "I peered at them and noticed that they were both covered in red paint."


translate english day5_main1_2c5cbf0a:


    th "What a surprise…{w} I wonder how they managed that?"


translate english day5_main1_066f749f:


    "Alisa opened the valve and started to rub Ulyana's back."


translate english day5_main1_fe4eae47:


    dv "Take off your bra!"


translate english day5_main1_99ac44aa:


    us "What if somebody sees us...?"


translate english day5_main1_fc7e06ef:


    dv "What – is there anything to see?"


translate english day5_main1_473574eb:


    "She grinned."


translate english day5_main1_6f871fea:


    us "Ok… Just be quick!"


translate english day5_main1_dd98f289:


    "It was true that there wasn't much to look at, but even so I stared at the girls narrowly."


translate english day5_main1_dcd32ae7:


    "It was a pity that they were standing with their backs to me."


translate english day5_main1_89e896b4:


    "A minute later Alisa managed to wash off all the paint."


translate english day5_main1_55cf29e5:


    dv "I'm done!"


translate english day5_main1_38773291:


    us "Thanks!"


translate english day5_main1_d53c1a0e:


    dv "You're welcome…"


translate english day5_main1_924b8a95:


    "Alisa replied lazily."


translate english day5_main1_c473bf91:


    us "Listen, let me try on yours…"


translate english day5_main1_68b5468a:


    "She pointed at Alisa's bra."


translate english day5_main1_82a80785:


    dv "It won't fit you for sure…"


translate english day5_main1_f348d116:


    us "Well, I'd like to try anyway…"


translate english day5_main1_19b2ba93:


    dv "But out here…"


translate english day5_main1_ba15cbf1:


    us "There's nobody here, right?"


translate english day5_main1_b8352a08:


    "Ulyana looked my way and smiled archly."


translate english day5_main1_eba695a3:


    "I was absolutely sure she couldn't see me in these bushes, but…"


translate english day5_main1_fe7431ac:


    dv "Enough with this nonsense…"


translate english day5_main1_3bd56665:


    "Ulyana wasn't listening to her anymore and grabbed Alisa's bra with a dexterous move instead."


translate english day5_main1_cc3da604:


    "Now, I have to confess, I had something to look at!"


translate english day5_main1_7f273933:


    "I watched the two girls chase one another around the washstands with bated breath."


translate english day5_main1_e09372ff:


    "Alisa covered her breasts with her hands so I could barely see anything at all."


translate english day5_main1_d0252e13:


    "I leaned forward and stumbled over a stone, falling out of the bushes…"


translate english day5_main1_2341b367:


    "Alisa and Ulyana stood frozen, staring at me."


translate english day5_main1_ef3d3ea6:


    "I tried to cover my nudity with my guilty face."


translate english day5_main1_11a555c2:


    "The tableau lasted for a few seconds, then Alisa took her shirt and somehow put it on in an instant."


translate english day5_main1_b7d8bf72:


    dv "You! You!"


translate english day5_main1_ec010716:


    "Her face gone from red to purple.{w} It looked like she would explode in a nuclear blast any second."


translate english day5_main1_4d760247:


    "The only thing I wanted was to disintegrate into atoms and to get as far from the epicentre as possible."


translate english day5_main1_97e02b14:


    us "He was sitting there the whole time!"


translate english day5_main1_470f0c0e:


    th "So, she noticed me then…"


translate english day5_main1_b7d8bf72_1:


    dv "You! You!"


translate english day5_main1_3a1dcf49:


    me "And I…{w} Well I… Accidentally…{w} If you know what I mean…"


translate english day5_main1_a9f31aea:


    "Alisa rushed at me."


translate english day5_main1_4ffa3601:


    "Covering my butt with one hand and holding my clothes with the other, I ran in the woods."


translate english day5_main1_9ba510d2:


    "It seemed the best solution to me at that time – as showing up naked in the middle of the camp accompanied by two screaming girls wasn't a good idea…"


translate english day5_main1_20ff3f6f:


    "I ran without looking back."


translate english day5_main1_e7dc840b:


    "And stopped few minutes later to catch my breath."


translate english day5_main1_0dab6283:


    "It seemed that there was no pursuit."


translate english day5_main1_544faf65:


    th "So, I managed to save myself!"


translate english day5_main1_49bd14a5:


    "But at the cost of lacerated, scratched and bleeding feet – as I had no time to put on my boots."


translate english day5_main1_244a0a02:


    "I sat on a tree stump and sighed…"


translate english day5_main1_ada2f8dd:


    "Some time later, having dressed already, I left the forest."


translate english day5_main1_01337998:


    th "I need to decide what to do next!"


translate english day5_main1_0004b659:


    th "My feet are hurting – so I should go straight to infirmary!"


translate english day5_main1_21e25d14:


    th "But on the other hand, my stomach isn't going to wait either."


translate english day5_main1_bc7d5816:


    th "Maybe I should accept Electronik's invitation?"


translate english day5_main1_ee8b56d6:


    th "Or head to the canteen in the hope of finding something left to eat there...?"


translate english day5_dining_hall_c113591d:


    "I always took good care of my health.{w} And even better care at the moments when I couldn't bear it anymore."


translate english day5_dining_hall_5b254b74:


    "And now I was able to walk and my feet weren't even hurting much."


translate english day5_dining_hall_1068ff00:


    "So my feet will heal up eventually, while hunger drives the wolf from the wood."


translate english day5_dining_hall_29600c1e:


    th "Surely the pioneers didn’t finish up everything!"


translate english day5_dining_hall_16cc44b9:


    th "At least a couple of sausages, eggs, or in the worst case a few pieces of bread should be left!"


translate english day5_dining_hall_daf1231e:


    "It was so deserted and quiet around the canteen that I even hesitated for a second."


translate english day5_dining_hall_d065f713:


    "Isn't it here where every pioneer seeks his happiness three times a day (and some even more often), isn't it an oasis of this heated summer desert, isn't it a secret chemical lab studying how types of meals unknown to science affect immature teenage bodies?"


translate english day5_dining_hall_01ef2d8a:


    "Now this building looked more like a bastion abandoned by its defenders, a kind of La Rochelle left by the Huguenots."


translate english day5_dining_hall_d00d888a:


    "Just get in and the ghosts of warriors who accepted a heroic death here will surround you…"


translate english day5_dining_hall_ff4b2359:


    "The c anteen looked the way it always did though."


translate english day5_dining_hall_59713482:


    "It was just completely empty, except for…{w} Miku, who was cleaning a table."


translate english day5_dining_hall_c404fc22:


    "Seeing that I quickly turned around and tried to sneakily escape, but didn't manage to make it…"


translate english day5_dining_hall_44d5aaf7:


    mi "Hi, Semyon! Did you come here to eat? You missed breakfast, didn't you? I mean I didn't see you… You could have been there but I didn't see you. It's good that you came anyway!"


translate english day5_dining_hall_479c6b10:


    me "Umm, hi…{w} Well I… Yes, I just came… Wondering if there is anything left maybe…"


translate english day5_dining_hall_a44fc8b3:


    mi "There's nothing left! You need to wait for lunch! You won't help, me by the way? I'm cleaning up here…"


translate english day5_dining_hall_810f39c8:


    me "What for?"


translate english day5_dining_hall_780b4277:


    mi "What do you mean?"


translate english day5_dining_hall_d946b833:


    "She puffed up her lips and seemed offended."


translate english day5_dining_hall_d0d699dd:


    mi "Somebody has to clean up! We do it in turns. You will have your turn as well!"


translate english day5_dining_hall_c0778825:


    th "Thanks, but no…"


translate english day5_dining_hall_6933143e:


    me "Ok, got it…"


translate english day5_dining_hall_9b0aa1ca:


    "I was going to leave, but Miku still couldn't stop."


translate english day5_dining_hall_c78b2cc2:


    mi "So will you help me?"


translate english day5_dining_hall_a4480274:


    "I don't know why I agreed."


translate english day5_dining_hall_79c948da:


    "It happens often that you make a decision first and then wonder for quite a while why did you say what you said."


translate english day5_dining_hall_2f281a85:


    "You think again and again…{w} And still can't figure out why on Earth you did that."


translate english day5_dining_hall_d93feb70:


    "That's how I felt wiping the tables one after another."


translate english day5_dining_hall_68f21c4b:


    mi "You know, I came up with a new song! Want me to sing it?"


translate english day5_dining_hall_209b83b3:


    th "Not eager at all."


translate english day5_dining_hall_0270e5da:


    mi "Hmm, no…"


translate english day5_dining_hall_60f3d758:


    "She started to think."


translate english day5_dining_hall_85b5c92a:


    mi "It would be hard to sing and clean up at the same time… I'll sing it later then!"


translate english day5_dining_hall_3aeb2d0f:


    "Miku gave me a disarming smile."


translate english day5_dining_hall_3b03af3a:


    me "Yes, of course..."


translate english day5_dining_hall_6eaf9733:


    mi "It's so cool how you saved Shurik yesterday! The entire camp has been talking only about it this since early this morning!"


translate english day5_dining_hall_992a60d0:


    th "I feel just like a hero…"


translate english day5_dining_hall_668b75c5:


    me "It was nothing much really."


translate english day5_dining_hall_57cc9875:


    mi "No, really, I'm serious! I would never dare to go into woods at night… into the the old camp… You know the rumours about it? About a camp leader who shot herself…"


translate english day5_dining_hall_3bb17a2c:


    th "Before they said she hanged herself."


translate english day5_dining_hall_52e843c6:


    mi "And it's so scary in general!"


translate english day5_dining_hall_b22bc996:


    me "Yep, it probably is."


translate english day5_dining_hall_f15c1e3c:


    "I tried to isolate myself from external stimuli and concentrate on the cleaning."


translate english day5_dining_hall_235ecfcf:


    "It helped me to finish sooner than I expected."


translate english day5_dining_hall_7a81d8e3:


    me "And now it's done!"


translate english day5_dining_hall_07ea95a8:


    mi "Thanks!"


translate english day5_dining_hall_25fab848:


    "There was quite a bit of time until lunch so I decided to go for a walk."


translate english day5_dining_hall_90a17a70:


    mi "Ok, fine…"


translate english day5_dining_hall_b2673fad:


    "It looks like I've upset Miku."


translate english day5_dining_hall_f3c8b2f3:


    th "That's the way the cookie crumbles…"


translate english day5_dining_hall_9d2e50ce:


    "Leaving the canteen, I sat on the bench standing by the door and sighed tiredly."


translate english day5_dining_hall_7e6cef9d:


    "My feet were still hurting a bit although not as bad as before, and there was still nothing in my stomach…"


translate english day5_dining_hall_29ebce26:


    "There was quite a bit of time until lunch so I decided to go for a walk."


translate english day5_dining_hall_549330fd:


    "I picked a random direction which could be explained by the single word 'forward'."


translate english day5_dining_hall_8983ed56:


    "In the end, I found myself at the square."


translate english day5_dining_hall_283be7ae:


    "This wasn't a surprise as the monument of Genda appeared to be the central hub of this camp and a kind of Kilometre Zero…"


translate english day5_dining_hall_356059a5:


    "I sat on the bench and started to think."


translate english day5_dining_hall_85bc341b:


    th "Four days have passed and I haven't gotten even an inch closer to working out how I got here."


translate english day5_dining_hall_37f3c603:


    th "It's true that quite a few strange things happened during this time, but almost every one of them can be explained logically after careful thinking."


translate english day5_dining_hall_9cc8af14:


    th "Every single one of them could have happened in normal life…"


translate english day5_dining_hall_ee9a7a1a:


    th "{i}Normal{/i} life.{w} This term lost its original meaning to me here."


translate english day5_dining_hall_e5526803:


    th "Reactions to the environment, the actions and words of other people, or my own words – indeed, none of this here is normal."


translate english day5_dining_hall_66dcd718:


    th "In the past four days my world view had taken a series of painful punches to the stomach and uppercuts which led to it being, if not knocked-out then seriously knocked-down."


translate english day5_dining_hall_7b0e3b31:


    th "Sometimes I don't understand why I act one way or the other or say some things…"


translate english day5_dining_hall_19f2dbbc:


    th "Actually, I do understand, but not straight away."


translate english day5_dining_hall_ac9be890:


    th "Such afterthoughts however, don't help me to act differently, more sanely, and appropriate to the situation at all."


translate english day5_dining_hall_bc9e662f:


    th "Moments of truth happening to me are becoming more and more rare."


translate english day5_dining_hall_85fdbea0:


    th "If my only wish during first day was getting out of here, then now my main concerns are where to find food, how to avoid lineup in the morning and what to say to Olga Dmitrievna if Alisa complains about me…"


translate english day5_dining_hall_f2b21e4a:


    th "And those things are truly important to me!"


translate english day5_dining_hall_770314f2:


    th "And day after day, daily fuss like this overshadows the thoughts in my head about how world around me, together with this camp and these girls, are completely abnormal!"


translate english day5_dining_hall_4ba0ec94:


    th "But I can't do anything about myself.{w} Because I just forget…"


translate english day5_dining_hall_5c46142a:


    th "In the same way we breathe without thinking about it, I am joining in the everyday life of the local inhabitants more and more without realising it."


translate english day5_dining_hall_95d43b05:


    th "I'm steadily becoming an average pioneer…"


translate english day5_dining_hall_6614ca63:


    me "No!{w} This is wrong!"


translate english day5_dining_hall_216fd396:


    "I shouted loud and slapped my face few times."


translate english day5_dining_hall_8d33c464:


    "All of a sudden the bell sound, calling the pioneers for the lunch, came from the loudspeakers."


translate english day5_dining_hall_f23afdfa:


    me "Finally!"


translate english day5_dining_hall_436b6b08:


    "I ran skipping along to the canteen, leaving my inspiring thoughts back at the square where they could sound interesting to Genda alone, and only if he was alive…"


translate english day5_aidpost_c1caf0d4:


    "I was always took good care of my health.{w} And even better care at the times when something starts to hurt badly."


translate english day5_aidpost_b92e3272:


    "So without hesitation I made my way to the infirmary."


translate english day5_aidpost_6e2d6c49:


    "But right in front of door I stopped as if some mysterious force was holding me."


translate english day5_aidpost_f49a6790:


    "I had absolutely no desire to face the camp nurse without any urgent need, but on the other hand, I almost did a great deed yesterday defending the infirmary."


translate english day5_aidpost_9cb3955d:


    "So in some sense she owed me …"


translate english day5_aidpost_41de82b8:


    "I took heart and opened the door."


translate english day5_aidpost_8e557000:


    cs "Hi, pioneer!"


translate english day5_aidpost_bb4c809d:


    me "Hello..."


translate english day5_aidpost_0f095ac2:


    cs "I'm sorry that I was so late yesterday…"


translate english day5_aidpost_fa9efa41:


    me "It wasn't a problem at all…"


translate english day5_aidpost_e2736162:


    cs "Why have you come?{w} Are you sick?"


translate english day5_aidpost_945c59bc:


    "She smiled slyly."


translate english day5_aidpost_f6d4c30b:


    me "Well, I'm…{w} I got my feet a bit…"


translate english day5_aidpost_a10407c0:


    cs "You got a bit what?"


translate english day5_aidpost_3f4d5019:


    me "My feet."


translate english day5_aidpost_be55d6e6:


    "I gave a stupid answer."


translate english day5_aidpost_ee52ce7f:


    cs "Ok, show me."


translate english day5_aidpost_ebe6fcf2:


    "I sat on the couch and took off my boots."


translate english day5_aidpost_387cc55b:


    cs "How did you manage that?"


translate english day5_aidpost_97c64dc4:


    "I'd better keep silent about what happened near the washstands."


translate english day5_aidpost_2fcc5991:


    me "Well I…"


translate english day5_aidpost_db1556e6:


    cs "Ok, never mind.{w} Wait a second…"


translate english day5_aidpost_37b10e56:


    "She rummaged in the drawer and took out a big red pill with a cross-shaped notch from a vial."


translate english day5_aidpost_7603637e:


    me "And what is that for?"


translate english day5_aidpost_8f1a1f4c:


    "I was disturbed by the size of the pill and its strange shape – they are broken in halves usually and not in quarters."


translate english day5_aidpost_8040e280:


    cs "This is to make the amputation of your legs unnecessary!"


translate english day5_aidpost_3d342d5a:


    me "What?!"


translate english day5_aidpost_91d22182:


    cs "Nothing. Don't be afraid…{w} pioneer!{w} You'll take it and it will all be alright."


translate english day5_aidpost_9fe20b07:


    me "And this pill…{w} why does it look like that?"


translate english day5_aidpost_7e2d8a95:


    cs "That's for patients who refuse to take medicines perorally.{w} We screw it into them with a screwdriver then."


translate english day5_aidpost_7ccd5058:


    "I didn't dare to ask where they were being screwed in actually."


translate english day5_aidpost_c2372b85:


    cs "And now try to bear it a bit."


translate english day5_aidpost_5c6ac9cb:


    "She took a big pack of cotton wool, wrapped a bud with it and applied plenty of iodine to it."


translate english day5_aidpost_ad181873:


    "I braced myself for the torments of hell…"


translate english day5_aidpost_4b4491c1:


    me "IIiiii…"


translate english day5_aidpost_1e0476d1:


    "I moaned quietly."


translate english day5_aidpost_ad1b9cb0:


    "But my trust in this method of disinfection was much higher than in the odd pill so I had to withstand the burning."


translate english day5_aidpost_4e9c1b77:


    cs "Now it's done!"


translate english day5_aidpost_4eb5e7c1:


    "I put on my boots and tried to walk."


translate english day5_aidpost_920fc608:


    "My feet were still hurting, but the sharp pain was gone."


translate english day5_aidpost_61477e60:


    me "Thank you!"


translate english day5_aidpost_f328eb53:


    cs "Don't mention it!{w} Drop in anytime…{w} pioneer!"


translate english day5_aidpost_40d35bf5:


    "I said goodbye and left the infirmary."


translate english day5_aidpost_c4d64e48:


    "My health problem was solved!"


translate english day5_aidpost_132ab340:


    th "It's the right time to eat now!"


translate english day5_clubs_6f918f97:


    "I was desperately hungry."


translate english day5_clubs_c113591d:


    "I always took good care of my health.{w} And even better care at the times when something start to hurt badly."


translate english day5_clubs_f98258e8:


    "And now I was able to walk and my feet weren't even hurting much."


translate english day5_clubs_ff3100c2:


    th "So my feet will heal up eventually while my hunger can’t wait…"


translate english day5_clubs_52edf477:


    "I decided to take advantage of Electronik's invitation."


translate english day5_clubs_13624c4d:


    "In the end, knowing the local pioneers, it was smart to assume that there wasn't even a piece of stale bread left at the canteen…"


translate english day5_clubs_2035d522:


    th "And the cybernetics club actually somewhat owe me."


translate english day5_clubs_c6f1a614:


    "With such thoughts I approached the clubs building."


translate english day5_clubs_e8ed57a1:


    "And heard screams coming from it."


translate english day5_clubs_d8f147f1:


    "I tried to listen closely but couldn't figure out anything."


translate english day5_clubs_5b5a4f32:


    sh "Give it back!"


translate english day5_clubs_6c5410b6:


    us "No, I won't!"


translate english day5_clubs_d25dd8ba:


    "Ulyana was running around the room with a coil of wire in her hands, chased by Shurik."


translate english day5_clubs_12e6be7b:


    me "Guys, I'm sorry to interrupt you, but…"


translate english day5_clubs_5b5a4f32_1:


    sh "Give it back!"


translate english day5_clubs_1217af9c:


    "They didn't even give me a bit of attention."


translate english day5_clubs_6c5410b6_1:


    us "No, I won't!"


translate english day5_clubs_87a4498e:


    "Shurik, completely caught up in the chase, slipped past me, almost knocking me down."


translate english day5_clubs_95efcfe1:


    me "Hey!"


translate english day5_clubs_37ace03e:


    "Ulyana was running in circles around the room laughing cheerfully."


translate english day5_clubs_ec25da0f:


    th "I wonder what she needs this wire for?"


translate english day5_clubs_81589db2:


    "Meanwhile the head of the cybernetics club was looking fine even after yesterday's madness.{w} One could even say he was looking fresh."


translate english day5_clubs_2ddd0bed:


    "But this probably wouldn't help him to catch Ulyana – she was smaller, more agile and faster after all."


translate english day5_clubs_075ee6b7:


    "She could easily drive him to exhaustion…"


translate english day5_clubs_95efcfe1_1:


    me "Hey!"


translate english day5_clubs_0b5f4462:


    "Ulyana ran up to me and hid behind my back."


translate english day5_clubs_692dfc67:


    "Shurik in his turn left the club, slamming the door on his way out."


translate english day5_clubs_8c929878:


    th "He was offended it seems…"


translate english day5_clubs_25606905:


    me "Hey, what's wrong with you?!"


translate english day5_clubs_b73a1dab:


    us "You won't catch me!"


translate english day5_clubs_575bed8e:


    "She looked at Electronik, who was silently observing all this silly running, and stuck out her tongue at him."


translate english day5_clubs_67bf1dec:


    el "Semyon, take the wire away from her!"


translate english day5_clubs_3cfac947:


    me "And why do you need it in the first place? You don't have enough wire or something?"


translate english day5_clubs_65fffe51:


    "He was trying to catch Ulyana, who was hiding behind me."


translate english day5_clubs_3a233083:


    "He moved to the right while she moved to the left, he moved to the left and she moved to the right…"


translate english day5_clubs_db785e8c:


    "In the end I got tired of this and grabbed the wire from Ulyana with one dexterous move."


translate english day5_clubs_7a0f81d3:


    us "Give it to me! Give it back!"


translate english day5_clubs_25ebf834:


    "She shouted resentfully."


translate english day5_clubs_bb517af5:


    me "No I won't! Stop horsing around!"


translate english day5_clubs_aac11710:


    "I held the wire above my head so Ulyana with her height couldn't possibly get it."


translate english day5_clubs_d2db29cb:


    el "Thank you!"


translate english day5_clubs_44f47ba7:


    us "Ok, fine!"


translate english day5_clubs_99aa653e:


    "She snorted and turned away from me."


translate english day5_clubs_66d3549f:


    me "Why do you need it in the first place?"


translate english day5_clubs_7d1c8c94:


    us "That's none of your business…"


translate english day5_clubs_c68d93e1:


    "Ulyana looked at me slyly."


translate english day5_clubs_88702a1e:


    us "Do you want me to tell everybody that you…"


translate english day5_clubs_960701df:


    "I shut her mouth with my hand and dragged her along to the exit."


translate english day5_clubs_d0245af1:


    me "Ok, we need to go…"


translate english day5_clubs_c000d8e7:


    "I told Electronik while giggling stupidly."


translate english day5_clubs_4d17bd21:


    el "And what did you come for...?"


translate english day5_clubs_7525ab2b:


    "Once outside, I let the kicking Ulyana free."


translate english day5_clubs_1828214b:


    me "Hear me out, you realise that it was an accident…{w} Even more so, it was caused by you!"


translate english day5_clubs_86dba4fb:


    us "I know nothing!{w} Facts are facts: you were watching us!"


translate english day5_clubs_d226b66c:


    us "Awww!{w} What would Olga Dmitrievna think…"


translate english day5_clubs_df680674:


    "On the one hand, I did not want to care in the slighest what the camp leader would say, but on the other, everything was against me and in my position it would be better to not get myself into such a situation."


translate english day5_clubs_97ad7b66:


    me "Fine, maybe we could make a deal somehow…"


translate english day5_clubs_6532261a:


    us "Hmmm…"


translate english day5_clubs_60f3d758:


    "She started to think."


translate english day5_clubs_9c1e8275:


    us "I know!"


translate english day5_clubs_747f9ddc:


    "I anticipated the worst."


translate english day5_clubs_e97a33bb:


    us "You bring me that wire!"


translate english day5_clubs_47728b3d:


    me "But what do you need it for?"


translate english day5_clubs_fc2357ec:


    us "I need it for my needs!"


translate english day5_clubs_2fca2e42:


    me "Ok, let's assume I get it…{w} You won't tell anybody?"


translate english day5_clubs_6f158bac:


    us "I give you the word of a pioneer!"


translate english day5_clubs_85b6f436:


    "I could hardly believe her."


translate english day5_clubs_88068e4d:


    "But on the other hand, it was just a coil of wire – so why shouldn't I try?"


translate english day5_clubs_91dd4d67:


    me "Ok, fine…"


translate english day5_clubs_a64a24e7:


    us "Deal!"


translate english day5_clubs_fee7bf01:


    me "But you must know that…"


translate english day5_clubs_f85583fb:


    us "Yeah, yeah, of course!"


translate english day5_clubs_0d425fdb:


    "She interrupted me."


translate english day5_clubs_b2bf8ea2:


    me "Wait here."


translate english day5_clubs_a4e889e3:


    "Saying that, I went back into the club's quarters."


translate english day5_clubs_0af71d51:


    me "No, no wire for you, enough already!"


translate english day5_clubs_1d6fcf81:


    us "Then I'll tell everything!"


translate english day5_clubs_5da9850d:


    me "You'll tell everything anyway! Or Alisa will…"


translate english day5_clubs_80eb9adc:


    us "Ahh, get lost!"


translate english day5_clubs_6d0fed3e:


    me "Yeah, sure, as if it was my fault…"


translate english day5_clubs_a18bb399:


    us "It is. You were watching us."


translate english day5_clubs_69a83100:


    me "No I wasn't!"


translate english day5_clubs_2719b638:


    "But, no matter how you slice it, I actually was…"


translate english day5_clubs_76442aba:


    us "Alisa doesn’t think so."


translate english day5_clubs_bdc70299:


    me "She doesn't agree with me about a lot of things."


translate english day5_clubs_d927617d:


    us "And Olga Dmitrievna won't be happy either."


translate english day5_clubs_a36b990a:


    me "You know what?!"


translate english day5_clubs_6ea78a01:


    "I started to lose my temper."


translate english day5_clubs_07f22fdf:


    me "If you want to tell everything – go ahead right now! And don't forget to mention that the government’s default, world depression, global warming and the Genesis Flood are all my fault too!"


translate english day5_clubs_7843e56a:


    us "Oh, c'mon, stop overreacting like that… I was joking."


translate english day5_clubs_ba72b31c:


    me "Joking...?"


translate english day5_clubs_578ce4ba:


    "I suddenly realised that I really was wound up too much."


translate english day5_clubs_07630dae:


    me "Your jokes are stupid! And what am I supposed to do every time – guess if it is for real or a joke?"


translate english day5_clubs_df6dbf74:


    us "Yep."


translate english day5_clubs_fde537c1:


    "She grinned."


translate english day5_clubs_0f1412bc:


    us "It's more funny that way."


translate english day5_clubs_eac89d90:


    "Ulyana turned around and ran towards the square, waving her hand in parting."


translate english day5_clubs_beff659f:


    th "Still, you can't deal with her in a harmonious way…"


translate english day5_clubs_01a2b4fe:


    "I sighed and went back into the club's quarters."


translate english day5_clubs_ee53a93e:


    "Electronik was building something with rapt attention."


translate english day5_clubs_6768144c:


    me "Now I'm done with everything…{w} So I thought you could get me something as you promised…"


translate english day5_clubs_72b8e1d5:


    "My voice sounded bit sycophantic, which was enough to drive me mad."


translate english day5_clubs_cda5bf38:


    me "Of course I'm not insisting but…"


translate english day5_clubs_73232f80:


    el "One minute."


translate english day5_clubs_819d22c9:


    "He tore himself away from his work and got a pair of buns from the drawer and a classic triangular pack of kefir."


translate english day5_clubs_10c6e757:


    el "Be my guest."


translate english day5_clubs_73aab3cd:


    me "Thank you…"


translate english day5_clubs_c321210e:


    "While I was busy eating, Electronik didn't turn away from his device for a second."


translate english day5_clubs_88d66003:


    "He was rolling up the wire, which I had taken away from Ulyana, onto the coil."


translate english day5_clubs_c90aa8d9:


    th "And it was wire she wanted!"


translate english day5_clubs_b12c3bd0:


    me "So what's this?"


translate english day5_clubs_ce6e33a1:


    el "An inductor."


translate english day5_clubs_01d8c6fc:


    me "An duck tor?"


translate english day5_clubs_a1512ad1:


    el "Join our club and you'll know everything!"


translate english day5_clubs_99c684ef:


    "He looked at me and smiled slyly."


translate english day5_clubs_232d02c3:


    me "I'll think about it…"


translate english day5_clubs_8e38bd64:


    "Of course, I wasn't going to join anything but taking into account that he had fed me I had to be polite."


translate english day5_clubs_0560d317:


    el "By the way, as I said, I have something else…"


translate english day5_clubs_f39fd8a3:


    me "Well, yeah…"


translate english day5_clubs_73232f80_1:


    el "Wait a second."


translate english day5_clubs_ebaab1b0:


    "He went into the adjacent room and came back after a minute with some kind of package."


translate english day5_clubs_1dfcdcd0:


    el "Tan-ta-daaa!"


translate english day5_clubs_6ba4e579:


    "And gave it to me."


translate english day5_clubs_4b293ede:


    "There was a big bottle of 'Stolichnaya' vodka inside."


translate english day5_clubs_4e5a25f1:


    me "Huh, I get it, but it's still morning…"


translate english day5_clubs_7cc7ef1a:


    th "Or does Electronik share the motto of 'get drunk in the morning – take the entire day off'?"


translate english day5_clubs_9479eb17:


    el "What are you talking about?!{w} I'm not suggesting we drink it! We have it to clean the optics!"


translate english day5_clubs_5e972698:


    th "Cleaning the optics, yeah right…{w} Internally…"


translate english day5_clubs_f2e007f0:


    me "Ok…"


translate english day5_clubs_9991da5b:


    "I gave him the package back."


translate english day5_clubs_a5f87a00:


    me "By the way! Can you spare me this coil?"


translate english day5_clubs_ce0d2801:


    el "What do you need it for?"


translate english day5_clubs_c97fadc0:


    me "Uhh…"


translate english day5_clubs_d6323f95:


    "Probably I had to think about reason beforehand."


translate english day5_clubs_98fe77ae:


    me "I just need it."


translate english day5_clubs_09b8008a:


    el "But we can't do without it as well."


translate english day5_clubs_68f1c3d2:


    me "Do you? Well, never mind then..."


translate english day5_clubs_ba73ed1f:


    th "I can't just force him to give it to me, right?"


translate english day5_clubs_cd9f16d5:


    me "I'll go then…"


translate english day5_clubs_642cd44e:


    el "Come back anytime!"


translate english day5_clubs_bdd3ba78:


    me "Of course…"


translate english day5_clubs_c5676147:


    th "The moment I want to have a drink…"


translate english day5_clubs_212a81b3:


    "Walking out, I questioned myself why he wanted to show me the vodka in the first place…"


translate english day5_clubs_3c6ad981:


    "Ulyana ran up to me instantly."


translate english day5_clubs_1fb33640:


    us "And? So? Have you got it?"


translate english day5_clubs_1eaad051:


    me "No..."


translate english day5_clubs_07a5780c:


    us "That's your own fault then!"


translate english day5_clubs_03515da3:


    me "Do you need this wire at all...?"


translate english day5_clubs_b934057e:


    us "But you get credit for trying!"


translate english day5_clubs_ad02f53a:


    me "What?"


translate english day5_clubs_d8362ad8:


    "I was surprised."


translate english day5_clubs_a10e2b46:


    us "Alisa would tell everything anyway!"


translate english day5_clubs_a4c6b91e:


    "Ulyana laughed."


translate english day5_clubs_d1d0d845:


    us "She is really angry with you."


translate english day5_clubs_078810d4:


    me "I bet…"


translate english day5_clubs_3e1d421a:


    "I muttered quietly."


translate english day5_clubs_fa77eb7d:


    us "Ok, see ya!"


translate english day5_clubs_3861dc96:


    "She waved at me and ran away."


translate english day5_clubs_50e6fe96:


    th "Here I am, in a stupid situation once again, no big deal."


translate english day5_clubs_5bd8bf7d:


    th "It's not the first time…"


translate english day5_clubs_bedd20be:


    "Suddenly my hunger returned again…"


translate english day5_clubs_9a611cea:


    "For sure, a meal of some buns and kefir was awesome. But it's not enough to fill me up."


translate english day5_clubs_8b9210c2:


    "Luckily for me I heard the bell sound, calling pioneers for lunch."


translate english day5_main2_c9f98d7e:


    th "The day has just started and I've gone through so many things already…"


translate english day5_main2_9dff93e1:


    th "But I did it and now have legitimate grounds to fill myself up!"


translate english day5_main2_b855c58c:


    "Today I wasn't the last one so I could choose a free table."


translate english day5_main2_5a8c068e:


    "Lunch included pea soup and mashed potatoes with fish."


translate english day5_main2_e5fed9f2:


    "It was a major disappointment to me as I don't eat fish in any form and hence will get fewer calories than usually."


translate english day5_main2_9a2a6cb4:


    "Soon Slavya and Lena came to my table."


translate english day5_main2_99c96681:


    sl "Can we?"


translate english day5_main2_b7bd4e35:


    "She smiled nicely."


translate english day5_main2_bcdef80b:


    me "Eh?{w} Yes, sure!"


translate english day5_main2_566b45f6:


    "I stood up and pulled out a chair for her."


translate english day5_main2_7b602452:


    me "Please!"


translate english day5_main2_cafd2e9e:


    "I was in an excellent mood at that moment."


translate english day5_main2_538f35f4:


    un "Enjoy your meal…"


translate english day5_main2_62d27eaf:


    "Saying that, Lena began staring at me and continued for some time but then, after realising how odd she looked, switched to her plate."


translate english day5_main2_3c5ee540:


    me "You too."


translate english day5_main2_952025ed:


    sl "Do you have any plans for today, Semyon?"


translate english day5_main2_5a7a7577:


    me "Nope."


translate english day5_main2_c0d912bb:


    "I gave her an honest answer as indeed I had no plans.{w} Except for {i}searching for answers{/i}, but that was more like a global goal."


translate english day5_main2_8df8d875:


    sl "Do you want to take a boat ride to the island with us?"


translate english day5_main2_c16c4c12:


    th "The island...{w} Well, I think I've seen it from the pier."


translate english day5_main2_7332342b:


    me "For what?"


translate english day5_main2_ea29c6fe:


    sl "Olga Dmitrievna asked us to gather some strawberries.{w} There are a lot of strawberries there and they're so delicious!"


translate english day5_main2_679ff36d:


    "I could imagine the taste without even eating it, just by looking at Slavya's face."


translate english day5_main2_f29bf018:


    me "Strawberries...{w} And what are those for?"


translate english day5_main2_2abd657f:


    sl "I don't know, but it's indeed a great idea!"


translate english day5_main2_5ccbf2e6:


    th "Well, indeed it is.{w} Moreover, I haven't been to the island yet."


translate english day5_main2_1066065f:


    me "Yeah, sure."


translate english day5_main2_a20cefa7:


    "..."


translate english day5_main2_f2b9563d:


    "Within minutes we were already standing at the pier."


translate english day5_main2_a8b1bc59:


    sl "Well, here is the boat.{w} Hang on, I'll go and fetch the paddles now."


translate english day5_main2_86c42a71:


    "I was left face-to-face with Lena."


translate english day5_main2_1745ad04:


    me "Do you like strawberries?"


translate english day5_main2_3898ad76:


    un "Well, not really...{w} But they're tasty."


translate english day5_main2_561828ec:


    "Lena smiled."


translate english day5_main2_f2e007f0:


    me "I see..."


translate english day5_main2_3a3284a7:


    "I didn't know what to say next, how to continue the conversation."


translate english day5_main2_858a16c5:


    "If Slavya didn't come back we could probably sit here till the evening without saying a word."


translate english day5_main2_ca656115:


    sl "Here you go!"


translate english day5_main2_60c4cc55:


    "She handed me a pair of hefty paddles."


translate english day5_main2_4ab1b169:


    me "Yeah… Thanks…"


translate english day5_main2_d8f75e4c:


    "We got into the boat, I untied it, pushed off the shore and tried to start paddling."


translate english day5_main2_ff778a3e:


    me "And where exactly are we heading to?"


translate english day5_main2_7c19e280:


    sl "Right there!"


translate english day5_main2_199f5847:


    "She pointed her finger at the island."


translate english day5_main2_adf7269e:


    sl "That island is named 'the Closest One'."


translate english day5_main2_97833beb:


    th "I wonder what captain gave it such an original name?{w} Well, the island is indeed close to the shore."


translate english day5_main2_0fd8390d:


    me "Aye-aye captain!"


translate english day5_main2_c5a9758a:


    th "If only I'd known what was waiting for me ahead…"


translate english day5_main2_bb189b0d:


    "I wasn't an experienced oarsman – I'd rowed a boat just once or twice in my entire life."


translate english day5_main2_dfd96964:


    "It was less than half a mile to the island, but we were making our way in zigzags thanks to my ‘skills’."


translate english day5_main2_c1ed01f4:


    "By approximately the middle of the trip my arms hurt so badly that I dropped the paddles to get some rest."


translate english day5_main2_cf757ee3:


    me "Well...{w}Aren't there any strawberries anywhere else?{w} I mean, in more accessible places?"


translate english day5_main2_31c67473:


    sl "But the tastiest ones grow there."


translate english day5_main2_683509ba:


    "Slavya gave me a puzzled look."


translate english day5_main2_6506e675:


    un "Is it hard for you to row alone?"


translate english day5_main2_e1e2ebb7:


    "Lena, unlike Slavya, understood everything straight away."


translate english day5_main2_b9dbf1a1:


    me "Oh...{w} It's nothing..."


translate english day5_main2_7f474b89:


    "Anyway, I couldn't let a fragile girl help me."


translate english day5_main2_255dc29b:


    "The rest of the way I spent concentrating on staying alive while getting to the island."


translate english day5_main2_b4c0cce9:


    "Slavya and Lenya discussed something but I wasn't listening – that was too much for me."


translate english day5_main2_f0934032:


    "At last, we arrived."


translate english day5_main2_d6e51afd:


    "Completely exhausted I got out on the shore and looked at the boathouse."


translate english day5_main2_9289c81b:


    "It seemed so far away that I felt like a first person on the moon watching the Earth rise."


translate english day5_main2_8f83b289:


    sl "Here you go!"


translate english day5_main2_e7fdbd78:


    "Slavya handed me a basket."


translate english day5_main2_799068d0:


    "It was a small island – barely a hundred metres long."


translate english day5_main2_b15dff1c:


    "And it looked more like a birch grove with even rows of trees covering its entire surface."


translate english day5_main2_5f826482:


    "A calm green sea spread beneath our feet with wind causing lonely waves on its surface from time to time."


translate english day5_main2_2e1cc3e7:


    "This island looked like a lost paradise."


translate english day5_main2_6ec442f5:


    th "It's no wonder that the most delicious strawberries grow just here."


translate english day5_main2_7568fc9b:


    sl "We've got to split up, that way we'll do the job faster."


translate english day5_main2_d76f93fa:


    me "Yeah, sure."


translate english day5_main2_1f4c91db:


    un "But there are only two baskets."


translate english day5_main2_f6237fab:


    "Said Lena humbly."


translate english day5_main2_e2c244d1:


    sl "Oh, you're right, my bad!"


translate english day5_main2_c1db23a3:


    un "So how are we going to split up then?"


translate english day5_main2_94c61009:


    me "Let me go with you."


translate english day5_main2_23fc5b72:


    un "Let's go…"


translate english day5_main2_7f370439:


    sl "Ok, that's fine!"


translate english day5_main2_235efb86:


    "Slavya grabbed the second basket and ventured to the opposite side of the island."


translate english day5_main2_b17829b3:


    me "Well?"


translate english day5_main2_3875ba7b:


    un "Well?"


translate english day5_main2_71f4991e:


    me "Let's go?"


translate english day5_main2_37fef1ca:


    un "Yeah."


translate english day5_main2_561828ec_1:


    "Lena smiled."


translate english day5_main2_e937d3e9:


    me "Just pay attention! Don't leave a single berry behind!"


translate english day5_main2_51487473:


    un "You too."


translate english day5_main2_a20cefa7_1:


    "..."


translate english day5_main2_a704bf84:


    "So, it was harvest time."


translate english day5_main2_afe45067:


    "Indeed, the strawberries here were delicious."


translate english day5_main2_de5b3003:


    "I could probably eat them all if I didn't stop myself in time."


translate english day5_main2_f8b9b72c:


    th "Despite being wild grown, the berries were close to garden ones in size and had a rich red color, so it was clear that our visit here wasn't in vain."


translate english day5_main2_bbcae5a7:


    "Lena followed me closely as we had only one basket for the both of us."


translate english day5_main2_ac634faa:


    "I felt like a real mushroom picker, examining each shrub and carefully pawing the grass."


translate english day5_main2_b7921614:


    un "Well, you're much better than me..."


translate english day5_main2_65640e4e:


    me "Am I?{w} Frankly, I’m not even counting them."


translate english day5_main2_0f83bc7d:


    un "Yeah, right you are!"


translate english day5_main2_435f0faa:


    "The basket was already half-full."


translate english day5_main2_c88a4757:


    me "You must enjoy nature, right?"


translate english day5_main2_f1be5fed:


    un "I do."


translate english day5_main2_5d5a082e:


    "The bright sun rays pierced the tree tops and blinded me for a second."


translate english day5_main2_b6151df9:


    "I sat down on the ground and leaned against a tree."


translate english day5_main2_7587bc3f:


    me "Still, it's so beautiful here!"


translate english day5_main2_9b60c62c:


    "Lena sat down next to me.{w} So close that our elbows touched."


translate english day5_main2_46b74af0:


    un "Yeah!"


translate english day5_main2_4bfe22ae:


    "We just sat and enjoyed the moment.{w} It seemed like time stood still."


translate english day5_main2_9c76c985:


    "The wind gently shook the tree leaves, some bugs lazily hopped around the grass and splashes of sunlight played on the faraway water surface."


translate english day5_main2_78fd2536:


    "Lena put her head on my shoulder."


translate english day5_main2_d1d0b93e:


    "I was surprised at first, but then I heard her regular breathing and thought that it's just a matter of course."


translate english day5_main2_bbbf378c:


    "Probably she felt drowsy and wanted to take a quick nap."


translate english day5_main2_464ee2c5:


    "I just sat there and didn't think of anything for a few minutes."


translate english day5_main2_9dcce076:


    "But then words started crossing my mind with ultrasonic speed:"


translate english day5_main2_4bf43e20:


    th "Lena.{w} So close.{w} Sleeping.{w} So warm.{w} So gentle.{w} Feelings..."


translate english day5_main2_3a8d7079:


    "I gazed at her."


translate english day5_main2_a5887389:


    "She had such a serene, such a tranquil look on her face that it seemed that right now she's not here, but in some kind of a better world."


translate english day5_main2_f950a1bd:


    "I don’t know what would have happened the next moment if I didn't hear the voice of Slavya."


translate english day5_main2_2295bc5f:


    sl "Semyon! Lena!"


translate english day5_main2_fdadda66:


    "I shook my head from side-to-side a few times to come to my senses.{w} Lena started to wake up."


translate english day5_main2_c0ebc5f2:


    "She opened her eyes and gave me an empty look."


translate english day5_main2_badffbcb:


    me "Have a nice dream?"


translate english day5_main2_d0ef9648:


    un "Huh?"


translate english day5_main2_8d04e459:


    "Suddenly realising that she had dozed off leaning on my shoulder, Lena blushed."


translate english day5_main2_be58af4f:


    un "Oh... I'm sorry..."


translate english day5_main2_6d36c28b:


    me "It's fine."


translate english day5_main2_2eefbc0c:


    "Slavya came over to us, so Lena rushed to get up."


translate english day5_main2_fde84cf5:


    sl "So how much have you got?"


translate english day5_main2_da3a9368:


    "I sighed."


translate english day5_main2_633224a4:


    sl "That's not a lot…"


translate english day5_main2_380af4e6:


    "Her basket was filled with strawberries to the brim."


translate english day5_main2_e384ce23:


    sl "Well, it's enough anyway!{w} It's time to get back!"


translate english day5_main2_7cd2b80a:


    "I grabbed the basket and we headed back to the boat."


translate english day5_main2_790a16f6:


    "I didn't want to walk here alone and hoped that Slavya would join me, but I couldn't bring myself to ask."


translate english day5_main2_e6680b45:


    me "Well, it's obvious: one basket for me, one for you two."


translate english day5_main2_97bf0978:


    sl "No, let me go with you!"


translate english day5_main2_c17a722d:


    "Slavya smiled."


translate english day5_main2_35ec24e9:


    me "Okay..."


translate english day5_main2_f8089075:


    "I was a bit surprised, but I was also glad that it turned out like this."


translate english day5_main2_46439482:


    "Lena seemed to take no offence at all."


translate english day5_main2_a20cefa7_2:


    "..."


translate english day5_main2_a704bf84_1:


    "The reaping has commenced."


translate english day5_main2_afe45067_1:


    "The strawberries were delicious here, indeed."


translate english day5_main2_de5b3003_1:


    "I could probably eat them all if I didn't stop myself in time."


translate english day5_main2_f8b9b72c_1:


    th "Despite being wild grown, the berries were close to the garden ones in size and had a rich red color, so it was clear that our visit here wasn't in vain."


translate english day5_main2_11dc0364:


    "Slavya was walking right beside me as we had only one basket."


translate english day5_main2_ac634faa_1:


    "I felt like a mushroom picker, looking under every shrub and searching through the grass carefully."


translate english day5_main2_9f273fda:


    sl "Pay attention!"


translate english day5_main2_eebba4df:


    "An entire bunch of strawberries had been left behind."


translate english day5_main2_cd47bc4e:


    me "Ah, yeah…{w} I'm sorry."


translate english day5_main2_705bea28:


    sl "It's fine."


translate english day5_main2_9db1f947:


    me "You must enjoy being here, don't you?{w} You like nature after all."


translate english day5_main2_49cc0ae4:


    sl "Of course I do!"


translate english day5_main2_c17a722d_1:


    "Slavya smiled."


translate english day5_main2_0854ae45:


    sl "It reminds me of my home – we have similar beautiful birches there."


translate english day5_main2_f32c544b:


    "She gazed dreamily somewhere into the distance."


translate english day5_main2_14e38355:


    me "Look, I've always wanted to ask – what do you like in general?"


translate english day5_main2_ed0e2cff:


    me "You look busy 24/7 and it seems like you have no time to rest at all."


translate english day5_main2_7db509b3:


    sl "Uhh…"


translate english day5_main2_60f3d758:


    "She started to think."


translate english day5_main2_63626b5d:


    sl "I don't know really.{w} Doing a variety of activities is enjoyable for me."


translate english day5_main2_882dd540:


    me "Well, that's understandable, but still?"


translate english day5_main2_9698acdd:


    sl "I like knitting and sewing!{w} Things like that…"


translate english day5_main2_f22d26a4:


    "Slavya took a handkerchief out of her pocket.{w} There were red, yellow and green flowers embroidered on it."


translate english day5_main2_aef33dd6:


    "They were entangled with each other in a complicated way, creating sophisticated geometric forms."


translate english day5_main2_576c9c96:


    "Such a typical Russian hand-made handkerchief."


translate english day5_main2_fa5e2895:


    "Glimpsing it, I instantly imagined Slavya dressed in an ancient sarafan, sitting on a bench beside a ramshackle house with a crowd of playing children running around."


translate english day5_main2_9918c29c:


    me "It's quite cute."


translate english day5_main2_8c1bfef0:


    sl "Thanks!{w} Let me give it to you as a gift!"


translate english day5_main2_95e49024:


    "Such a proposal embarrassed me."


translate english day5_main2_08a40647:


    me "You shouldn't…"


translate english day5_main2_127a57de:


    sl "No, take it!"


translate english day5_main2_72fe7131:


    "I looked at the handkerchief once again and put it into my pocket."


translate english day5_main2_73aab3cd:


    me "Thank you…"


translate english day5_main2_a20cefa7_3:


    "…"


translate english day5_main2_ff2ae452:


    "There were so many strawberries here that after a mere half an hour we had the basket filled up to the brim."


translate english day5_main2_fb9748eb:


    me "It seems we're done…"


translate english day5_main2_e498e9bd:


    sl "Yes.{w} We've got a lot so it should surely be enough."


translate english day5_main2_f110cff6:


    "When we got back to the boat Lena wasn't there yet."


translate english day5_main2_b852b10d:


    sl "She'd need more time to fill the basket by herself."


translate english day5_main2_ff886d6d:


    me "Yeah, I guess so…"


translate english day5_main2_94972c05:


    "I looked at the river."


translate english day5_main2_827d1834:


    "Sun sparkles happily dancing across the water surface were the only thing that distinguished it from a mirror – that's how calm the river seemed."


translate english day5_main2_29dcd13b:


    sl "What are you thinking about?"


translate english day5_main2_51eebe65:


    me "Nothing really…{w} And you?"


translate english day5_main2_7be9fd97:


    sl "Me...?{w} What will happen once vacation is over? We'll have to leave this camp and go back to our homes…"


translate english day5_main2_08224f21:


    sl "Will I ever see anyone I met here again?{w} Will I ever see you again...?"


translate english day5_main2_e932afd0:


    "She looked at me with her eyes so full of sorrow that I couldn't think of what to say."


translate english day5_main2_bffe94f9:


    "Lena came out of nowhere, breaking the silence."


translate english day5_main2_bfcc6d0c:


    un "Oh, you're done already…{w} Here."


translate english day5_main2_26b9d76b:


    "She showed us a basket full of strawberries."


translate english day5_main2_e2c68eb7:


    sl "Great!{w} Now we can go back."


translate english day5_main2_3ea9d288:


    "And I still had Slavya's face and those words of hers on my mind."


translate english day5_main2_9b1d3958:


    "Sadness and sorrow weren't the kinds of emotions typical of her."


translate english day5_main2_8bcb5251:


    th "Could she be hiding them all the time under a mask of cheerfulness?"


translate english day5_main2_54c1d08a:


    "I had no answer to this question and I knew I couldn’t find one either way."


translate english day5_main2_e3a29d10:


    th "Maybe later…"


translate english day5_main2_4d28e778:


    "The way back took less time as I tried to concentrate on rowing and ignoring everything else."


translate english day5_main2_1012dd1b:


    "My only wish was to get back alive, as the first trip hadn’t gone without consequences and now my hands started to hurt after only a few sweeps of the oars."


translate english day5_main2_d6437409:


    "Having tied up the boat, I fell to the ground with no energy left."


translate english day5_main2_ba4a18aa:


    "Slavya and Lena leaned over me."


translate english day5_main2_379cb129:


    sl "You could have said something if it was so hard for you!"


translate english day5_main2_31b01f67:


    un "Yes…"


translate english day5_main2_bb2ef35c:


    me "Never mind, it's fine…{w} I'll just lie here for a bit and everything will be alright…"


translate english day5_main2_50def11b:


    sl "Ok, then get those baskets to Olga Dmitrievna please, we have something else to do."


translate english day5_main2_85279b68:


    me "Yeah, sure."


translate english day5_main2_6d720424:


    "I was ready to agree with anything at that moment just so I wouldn’t have to get up."


translate english day5_main2_3bfb15a5:


    "Slavya put the baskets full of strawberries next to me and headed to the square, happily chatting with Lena about something."


translate english day5_main2_cb288325:


    me "The hardest part is done anyway…"


translate english day5_main2_258341cc:


    "That’s what I thought before I got up and took the baskets…"


translate english day5_main2_f1fdfa2c:


    "After the rowing they felt like cement bags, even while weighing barely more than a few kilograms each."


translate english day5_main2_331ec88f:


    "So the trip to the camp leader's cabin took much longer than usual – I had to stop every fifty metres to have a rest."


translate english day5_main2_3f67afab:


    "Once I finally made it, I put the baskets on the ground and sat on the deckchair with difficulty."


translate english day5_main2_30582463:


    me "Olga Dmitrievna!{w} Olga Dmitrievna, I've got presents for you!"


translate english day5_main2_dfffcbac:


    "There was no answer."


translate english day5_main2_a11cb528:


    "I barely managed to get up and enter the cabin."


translate english day5_main2_40d2a09a:


    "There was nobody there."


translate english day5_main2_d189a7a2:


    me "If you don't need them – it's up to you…"


translate english day5_main2_c3f0764a:


    "I lay down on the deckchair and fell asleep."


translate english day5_main2_8bc92ca0:


    "I had a weird dream about a strawberry race."


translate english day5_main2_f12d326b:


    "I was rowing a boat with my last ounces of strength, trying to escape huge berries that were chasing me."


translate english day5_main2_b573ff1d:


    "My hands were failing me and I could barely see anything because of the sweat covering my face. Blood was hammering in my temples but the strawberries were getting closer."


translate english day5_main2_371f84cd:


    "They were baring their teeth at me."


translate english day5_main2_57cc90da:


    th "But wait…{w} Strawberries with teeth?!"


translate english day5_main2_bed90e86:


    mt "…Semyon! Semyon!"


translate english day5_main2_4b4e1333:


    "I woke up."


translate english day5_main2_821dbe9f:


    "Olga Dmitrievna was standing beside me, shaking my shoulder."


translate english day5_main2_4f14ab7e:


    mt "I see you got a rich harvest, didn't you?"


translate english day5_main2_f0de0091:


    mt "Ok. But that's not all!"


translate english day5_main2_b635cee7:


    th "Seriously, I was just anticipating the lovely rest I was about to have…"


translate english day5_main2_6091ae1b:


    mt "Do you even know what these strawberries are for?"


translate english day5_main2_230628fb:


    me "Not a clue…"


translate english day5_main2_f1e8b4cf:


    "What an honest confession."


translate english day5_main2_035bcc73:


    mt "We'll make a cake out of them!"


translate english day5_main2_f2e007f0_1:


    me "I see…"


translate english day5_main2_918f0332:


    th "Well, that makes sense."


translate english day5_main2_cf6837ea:


    mt "To honor the miraculous rescue of Shurik!{w} And it's all thanks to you!"


translate english day5_main2_e8bfedc9:


    "It was clear that getting the strawberries wasn't the last thing left to do."


translate english day5_main2_b125d822:


    th "And why, please tell me, if I am such a hero, why do I have to organise a celebration in my name all by myself?.."


translate english day5_main2_443723fe:


    me "Well, I guess…"


translate english day5_main2_cb1beef9:


    mt "So…{w} I have an important task for you!"


translate english day5_main2_a8dc8e24:


    mt "We are missing yeast, flour and sugar.{w} And need it all in the canteen before dinner!"


translate english day5_main2_1cdc0576:


    me "And those who will make the cake can't deal with it on their own somehow?"


translate english day5_main2_13e8802b:


    "I asked pitifully."


translate english day5_main2_4cb23232:


    mt "Of course they can't!{w} All of them are busy!{w} And you are the only one in the whole camp who does nothing!"


translate english day5_main2_c4eaf48d:


    "While her words were partly true, that doesn't make it any easier for me now."


translate english day5_main2_aefacb3e:


    "Moreover, those words felt like a bullet to my head."


translate english day5_main2_05b86f04:


    mt "So, write it down!{w} You'll get yeast in the infirmary, flour in the library and sugar in the clubhouse."


translate english day5_main2_42814c09:


    me "Wait, wait a…"


translate english day5_main2_e9da3bbd:


    mt "I have no time, I'm in a hurry!{w} Good luck!"


translate english day5_main2_0457a719:


    "She smiled slyly and left."


translate english day5_main2_8824565a:


    th "Of course there are a lot of strange things in this camp, but…{w} Yeast in the infirmary? Ok, I can deal with that. But...{w} Flour in the library?"


translate english day5_main2_dd117348:


    th "And sugar…{w} No, it's way beyond my comprehension!"


translate english day5_main2_d71f77ee:


    "I spat on the floor."


translate english day5_main2_6a2ac63d:


    me "I don't want to and I will not believe this! Tell me, just tell me you're pulling my leg."


translate english day5_main2_2b37191e:


    "I would not be surprised if a crowd of fat green trolls would appear here right now beside me, with every one of them feeling obligated to laugh at me."


translate english day5_main2_b150a7df:


    th "So maybe to hell with this cake...?"


translate english day5_main2_f58ed270:


    "I weighed my options for some time."


translate english day5_main2_c815c3c0:


    th "No, if such a major plan of Olga Dmitrievna's fell through, I'd be in for a world of hurt."


translate english day5_main2_7cca4369:


    th "And it would complicate both my life in the camp and my search for answers, which I've stopped for quite a while."


translate english day5_main2_515e2ec4:


    th "It seems I have no choice…"


translate english day5_aidpost_2_71caa85a:


    th "I feel like I've visited the infirmary too often recently."


translate english day5_aidpost_2_7a9eec06:


    th "But what can I do – that's how things pan out."


translate english day5_aidpost_2_af5fbbdb:


    "I sighed and knocked on the door."


translate english day5_aidpost_2_e9203c64:


    cs "Come in!"


translate english day5_aidpost_2_e6e52c0a:


    "The nurse said with a trace of a singsong accent."


translate english day5_aidpost_2_b9a6c45b:


    me "Good afternoon!{w} Olga Dmitrievna has sent me to get some..."


translate english day5_aidpost_2_5286c4cf:


    "I hesitated slightly."


translate english day5_aidpost_2_68451631:


    me "Yeast..."


translate english day5_aidpost_2_2d204cc8:


    cs "Ah, sure."


translate english day5_aidpost_2_6d77584d:


    "She gave me a broad smile."


translate english day5_aidpost_2_524e70a4:


    cs "It’s just that I don't have any…{w} pioneer."


translate english day5_aidpost_2_e8bf1827:


    me "How so?{w} She said that…"


translate english day5_aidpost_2_48f8061d:


    cs "Well, I had some, but now there's none left."


translate english day5_aidpost_2_62e70926:


    "I didn't even bother asking why she had it in the first place."


translate english day5_aidpost_2_55f0b627:


    cs "Well, don't you worry.{w} You can have some aspirin, for example."


translate english day5_aidpost_2_624f6a17:


    th "That could be of some use to me, actually."


translate english day5_aidpost_2_ec7a55c9:


    me "Where do I get it then...?"


translate english day5_aidpost_2_ed0914b5:


    "I sighed."


translate english day5_aidpost_2_19a3e4d7:


    cs "Take this!"


translate english day5_aidpost_2_4fb9e776:


    "She opened the drawer and pulled out some kind of bottle."


translate english day5_aidpost_2_3a770cd2:


    "I took a closer look.{w} It was 'Ostankinskoe' beer."


translate english day5_aidpost_2_51149e68:


    me "..."


translate english day5_aidpost_2_94e2a40d:


    cs "What's the matter?{w} Beer is also a fermented product."


translate english day5_aidpost_2_8fb93608:


    "She gave me a deep gaze."


translate english day5_aidpost_2_97a20ab2:


    cs "Nobody will even notice!"


translate english day5_aidpost_2_51d589e0:


    "She had a point, but everything just looked so grotesque to me that I couldn't find anything to say."


translate english day5_aidpost_2_d9f00730:


    me "Are you... sure?"


translate english day5_aidpost_2_a61a2efe:


    cs "Absolutely!"


translate english day5_aidpost_2_d76d86ac:


    me "Ok then…"


translate english day5_aidpost_2_fe10b3f1:


    "The bottle clearly wouldn't fit into the pocket of my shorts."


translate english day5_aidpost_2_73aab3cd:


    me "Well, thanks..."


translate english day5_aidpost_2_e7fd1586:


    "I mumbled shyly, leaving the infirmary."


translate english day5_aidpost_2_942696fd:


    th "Well, beer certainly could replace yeast."


translate english day5_aidpost_2_a2b4dc7e:


    th "Even my limited knowledge of chemistry and biology was enough to accept this.{w} But…"


translate english day5_aidpost_2_4997ec0f:


    "Generally, walking around with this bottle in my hands looked like a silly idea to me, so I decided to bring it to Olga Dmitrievna's cabin and hide it there."


translate english day5_aidpost_2_aeaa1520:


    "But I had to reach it somehow without anybody noticing the beer."


translate english day5_aidpost_2_f988b661:


    "I hid the bottle under my shirt."


translate english day5_aidpost_2_b2e65042:


    "And everything would have been fine, but Slavya called out to me at the square."


translate english day5_aidpost_2_9b22a2b9:


    "Actually, she sprang out from behind me so suddenly that I even gave a start."


translate english day5_aidpost_2_99a001ee:


    sl "How's it going?"


translate english day5_aidpost_2_ad02f53a:


    me "What exactly?"


translate english day5_aidpost_2_28a39c69:


    sl "Your search for ingredients."


translate english day5_aidpost_2_ed34a1cf:


    me "Ah, so you know already…"


translate english day5_aidpost_2_2bb8d573:


    sl "Yes I do!"


translate english day5_aidpost_2_c17a722d:


    "Slavya smiled."


translate english day5_aidpost_2_7de85321:


    me "It's going alright…"


translate english day5_aidpost_2_f7e1cdf8:


    "I answered, trying not to give away my unrest."


translate english day5_aidpost_2_9d5782dc:


    sl "And what do you have there?"


translate english day5_aidpost_2_c76c023d:


    "She pointed at the bottle sticking out from under my shirt."


translate english day5_aidpost_2_5652d5eb:


    me "Ah, this…"


translate english day5_aidpost_2_bda7946b:


    th "She got me!"


translate english day5_aidpost_2_4c420dcf:


    me "Ah, it's nothing..."


translate english day5_aidpost_2_478e684d:


    "I blushed with a silly giggle."


translate english day5_aidpost_2_d96f5cdf:


    me "It's time to go..."


translate english day5_aidpost_2_bc62fb23:


    "I was almost running, leaving the square with the puzzled Slavya behind."


translate english day5_aidpost_2_74475de8:


    th "It's great that she is one of those people who don't ask unnecessary questions."


translate english day5_aidpost_2_bd1cc79d:


    "But there are people in this camp who like nothing better than to poke their noses into other people's business."


translate english day5_aidpost_2_0616ad18:


    "Passing the pioneers' cabins, I stumbled upon Ulyana."


translate english day5_aidpost_2_397ec628:


    us "What are you hiding there?"


translate english day5_aidpost_2_07c1adb6:


    "She gave me one of her cheeky looks."


translate english day5_aidpost_2_6ada69b4:


    "I thought there was no point in denying anything so I replied in a provocative manner:"


translate english day5_aidpost_2_38b6a274:


    me "It's none of your business! I'm a cipher officer bearing a message to headquarters."


translate english day5_aidpost_2_1ebafc67:


    us "That's certainly a big...{w} message..."


translate english day5_aidpost_2_fd58cad9:


    "I was carrying the bottle at waist height so I was slightly embarrassed."


translate english day5_aidpost_2_d8a23d43:


    us "You want some help?"


translate english day5_aidpost_2_5fc5692c:


    me "I'll deal with it on my own!"


translate english day5_aidpost_2_5a4c3675:


    "I walked past her confidently and proceeded on my way."


translate english day5_aidpost_2_fbcbd186:


    "To my surprise she didn't say anything, nor try to pursue me."


translate english day5_aidpost_2_1266f2bc:


    "There was nobody at Olga Dmitrievna's cabin so I successfully managed to stuff the bottle under my bed."


translate english day5_aidpost_2_4e945031:


    "Once I got outside I sighed with relief."


translate english day5_aidpost_2_74665f46:


    "Really, I couldn't believe that I would ever worry that much about a single bottle of beer!{w} Like I was back in high school."


translate english day5_aidpost_2_af3e0460:


    th "It's a good thing it's safe now..."


translate english day5_aidpost_2_ceb963db:


    th "Even if somebody finds it, I'll claim that it's not mine – I could always think up of a suitable excuse from my enormous experience."


translate english day5_clubs_2_bb74ce24:


    "It feels like I've gone through more things today than all the previous days combined."


translate english day5_clubs_2_7fddb993:


    "Thus, approaching the clubhouse I'd even forgotten to think about how awkward it must be to look for sugar there."


translate english day5_clubs_2_ebe5438a:


    "Shurik and Electronik were enthusiastically building something."


translate english day5_clubs_2_2e2f3c1b:


    "They were so busy that they didn't even notice me."


translate english day5_clubs_2_2b02462e:


    "I looked closely."


translate english day5_clubs_2_3af379d1:


    "It was some kind of a robot…{w} Or at least the body of one."


translate english day5_clubs_2_729ff343:


    "Moreover, this robot was female, and had animal ears."


translate english day5_clubs_2_a869bd00:


    "I didn't want to make up theories about purpose of such a device for luminaries of camp cybernetics."


translate english day5_clubs_2_c69b15fb:


    "Even though the design looked practically workable, I had my doubts about this robot ever being able to conquer Earth, or at least being able to do anything on its own."


translate english day5_clubs_2_81faf816:


    "But they seem to be enjoying the process more that the end result itself."


translate english day5_clubs_2_76cfb844:


    "And that was something we shared, even though I didn't want to admit it."


translate english day5_clubs_2_d63ce887:


    "On the other hand they weren't afraid of possible failure, criticism, or jokes…{w} They were working towards their goal without paying attention to others, who would call it unrealistic or even absurd."


translate english day5_clubs_2_ff329017:


    th "Oh, it looks like I'm truly comparing then to luminaries of the sciences…"


translate english day5_clubs_2_b90af56a:


    me "Hey guys."


translate english day5_clubs_2_853a9b87:


    "I greeted them uncertainly."


translate english day5_clubs_2_5c419eaf:


    sh "Oh! Semyon! Come in, we’re always glad to see you!"


translate english day5_clubs_2_d193794e:


    th "I was actually already inside…"


translate english day5_clubs_2_2bd5ad91:


    sh "You know…{w} Sorry for what happened yesterday!{w} I barely remember anything, but…{w} Well…"


translate english day5_clubs_2_95e7fcc0:


    me "Never mind! It's ok!"


translate english day5_clubs_2_f3cf77af:


    el "And what brings you to our humble abode?"


translate english day5_clubs_2_e7ed1aae:


    "Electronik looked at me slyly."


translate english day5_clubs_2_f069bcd3:


    "Sometimes I feel that he makes such a face when he knows something about the other person, something he can use in a right moment."


translate english day5_clubs_2_88b7e8e0:


    me "Sugar.{w} I need sugar."


translate english day5_clubs_2_e4fdc3d2:


    "An image from an ancient video game suddenly came into my mind, where some kind of unit like a builder or something cried out with all of its five-pixel stature: “Gold! We need more gold!”"


translate english day5_clubs_2_9965d584:


    el "We got it."


translate english day5_clubs_2_9b27ce05:


    "Said Electronik calmly."


translate english day5_clubs_2_edb7828d:


    sh "Why would you want it?"


translate english day5_clubs_2_6a1f6f30:


    "I felt that I shouldn't explain to Shurik that they want to bake a cake for him.{w} I shouldn't spoil the surprise."


translate english day5_clubs_2_cea669a0:


    me "I don’t know...{w} Olga Dmitrievna told me to get some..."


translate english day5_clubs_2_f7be46a6:


    el "Ok, hang on."


translate english day5_clubs_2_c1165691:


    "Electronik disappeared behind the door into the next room."


translate english day5_clubs_2_72678105:


    me "And why do you have sugar here? Why not in the canteen?"


translate english day5_clubs_2_c5831f8c:


    sh "When the food truck came last time, it was the last thing to unload."


translate english day5_clubs_2_8afaf429:


    sh "And given that our building is the closest one to the entrance, they decided to leave it here to save some effort..."


translate english day5_clubs_2_9b463aae:


    th "That's reasonable, isn't it?"


translate english day5_clubs_2_bb104bbc:


    "The door opened, revealing Electronik hauling a huge bag behind him."


translate english day5_clubs_2_d24fcbc5:


    "I really don’t know what the size of the cake will be, but that was obviously too much sugar."


translate english day5_clubs_2_30543032:


    me "Well, thanks, but I don't need it all..."


translate english day5_clubs_2_776a0c07:


    el "But where would we put it?"


translate english day5_clubs_2_961b180e:


    "Electronik gave me a surprised look."


translate english day5_clubs_2_590bb4f8:


    el "We don't have a place for it.{w} You asked for sugar – so take it."


translate english day5_clubs_2_ec12a3f5:


    th "It seems that that previous smile of his wasn't there without reason."


translate english day5_clubs_2_0cff3fa1:


    me "Maybe you'll help me then?{w} It's not that far to carry…"


translate english day5_clubs_2_40b931b1:


    el "We're busy."


translate english day5_clubs_2_07640196:


    "He pointed his hand at the robot."


translate english day5_clubs_2_c06e6e9a:


    "I gazed at Shurik.{w} He owed me, after all."


translate english day5_clubs_2_d7fa97fa:


    "He hesitated and then looked away in shame."


translate english day5_clubs_2_9b5a3058:


    "I sighed, took the bag and headed to the door."


translate english day5_clubs_2_d9acf4d6:


    me "Thanks anyway…"


translate english day5_clubs_2_d52a2c3a:


    "I said at parting, exerting myself."


translate english day5_clubs_2_256429b1:


    "But I didn’t make it too far."


translate english day5_clubs_2_87117e72:


    "Just after a mere twenty metres I had to put the bag down to have a rest."


translate english day5_clubs_2_0fd93e15:


    "I had no idea how much it weighed, but it felt like more than twenty kilos."


translate english day5_clubs_2_4099b982:


    "On the one hand, it was just two hundred metres to the canteen."


translate english day5_clubs_2_e147e5e5:


    "On the other hand even such a distance with this payload on my shoulder (or alternately in my hands, or on my legs, or under my arm, or even on my head) looked impossible for me to cover."


translate english day5_clubs_2_d88fce53:


    "And as I resigned myself to move in minor sprints with prolonged pauses between them (so I could get there by night at least), I heard a voice behind me:"


translate english day5_clubs_2_73f17541:


    un "Maybe I could help you?"


translate english day5_clubs_2_1cd2d1a1:


    "I saw Lena in front of me."


translate english day5_clubs_2_5cbd7cac:


    me "I don't think you can…"


translate english day5_clubs_2_2fb0f57d:


    "It was one of those moments when I felt painfully how dramatically I was out of shape…"


translate english day5_clubs_2_be079536:


    un "I can bring a handcart."


translate english day5_clubs_2_dbff5c0c:


    th "A handcart…{w} Why didn't I think of that myself?!"


translate english day5_clubs_2_02b2a515:


    me "Yes, that would be great!"


translate english day5_clubs_2_a8585d26:


    un "Wait here, I'll be right back!"


translate english day5_clubs_2_47fe246e:


    "She smiled and ran in the direction of the square."


translate english day5_clubs_2_e8fbb4d7:


    th "What would I do without her...?"


translate english day5_clubs_2_8efa90a2:


    th "It's good that Lena isn't always that shy and can take the initiative sometimes."


translate english day5_clubs_2_89141735:


    "I started to think."


translate english day5_clubs_2_952b5881:


    "She seemed quite unusual now.{w} With no trace of shyness on her face and actually the complete opposite – smiles and confidence."


translate english day5_clubs_2_017038ff:


    "The offer of help wasn't something extraordinary by itself, but getting it from Lena…"


translate english day5_clubs_2_a1ed0501:


    "A few minutes later she came back with a smallish handcart."


translate english day5_clubs_2_3fed3b65:


    "I put the bag down on it."


translate english day5_clubs_2_3066598f:


    me "Thanks."


translate english day5_clubs_2_69777dad:


    un "Don't mention it…"


translate english day5_clubs_2_42f94d04:


    "She blushed and looked down."


translate english day5_clubs_2_3ecb53f0:


    th "Oh, the Lena we all know is back!"


translate english day5_clubs_2_ab1d28c0:


    un "So I'll go then…"


translate english day5_clubs_2_45858c8d:


    me "Yes, see you!{w} And thank you again!"


translate english day5_clubs_2_3da1183b:


    "I shouted after her."


translate english day5_clubs_2_d45c5c2b:


    "Sometimes I felt like there are two different people living inside of Lena."


translate english day5_clubs_2_0b282078:


    "But the second one – confident, happy and sometimes even bold – only appears when she talks to me."


translate english day5_clubs_2_9a4a089a:


    th "Or am I making things up again...?"


translate english day5_clubs_2_fce0897d:


    "I thought that it would be better if I get all the ingredients at once, so I headed to Olga's Dmitrievna cabin with the handcart."


translate english day5_library_2_d8c69e27:


    "If every other place on the cake ingredient list made at least some sense to me, then flour from the library made none."


translate english day5_library_2_94112c60:


    "I thought hard about who would put it in the library and why, but couldn't find any sane explanation after all."


translate english day5_library_2_328028d5:


    "Given Zhenya's harsh nature, I'd better knock first."


translate english day5_library_2_9b83bcec:


    mz "Open."


translate english day5_library_2_ccef914a:


    "Zhenya peered at me closely from behind her glasses."


translate english day5_library_2_6705d775:


    mz "What do you want?"


translate english day5_library_2_e39034b2:


    me "Umm…{w} Don't think anything weird, but I need…"


translate english day5_library_2_511a0256:


    "I didn't want to look like an idiot and decided to explain things carefully."


translate english day5_library_2_c2c36c71:


    me "I need some flour. Olga Dmitrievna said that it's here. I understand that it sounds strange to keep the flour in the library but..."


translate english day5_library_2_16f45806:


    me "I was sent to you. And it's needed for a cake, to celebrate Shurik's rescue."


translate english day5_library_2_e3838181:


    mz "Yes, I have the flour, what's so strange about it?"


translate english day5_library_2_80c6622f:


    "Zhenya replied with surprise."


translate english day5_library_2_f64f4e8b:


    "At that second I felt like I'd been hit on the head with a heavy weight and lost the ability to understand anything at all."


translate english day5_library_2_079c3fed:


    th "Flour in the library…{w} Sure, what's so strange about it…"


translate english day5_library_2_e29b49ed:


    th "We're in Wonderland, I'm Alice, now I'm gonna eat that magic mushroom and I'll be back home..."


translate english day5_library_2_fe1f361c:


    mz "Hey!"


translate english day5_library_2_ca8c1883:


    me "Ah? Yeah!"


translate english day5_library_2_5c270e5d:


    "I was daydreaming."


translate english day5_library_2_e683043b:


    mz "Wait here, I'll be right back."


translate english day5_library_2_660215b2:


    "She disappeared behind the bookshelves while I folded my hands and started waiting."


translate english day5_library_2_c8ef07cc:


    "A moment later the sound of a trapdoor groaning on its hinges reached me."


translate english day5_library_2_21111af0:


    me "Hey, you need some help?"


translate english day5_library_2_91d35e95:


    "I inquired loudly."


translate english day5_library_2_644e33dc:


    mz "I'll deal with it!"


translate english day5_library_2_47919150:


    "Zhenya barked out to me."


translate english day5_library_2_dcc13160:


    mz "She seems to be in the basement so I'll have to wait a little."


translate english day5_library_2_d08df322:


    me "Okey-dokey..."


translate english day5_library_2_0a82bf01:


    "A few minutes passed but Zhenya still hadn't returned."


translate english day5_library_2_83502309:


    "I was starting to get worried when the door was suddenly flung open and Alisa came into the library."


translate english day5_library_2_c46172f4:


    "She looked suprised too, seeing me here."


translate english day5_library_2_cdb9fe3e:


    dv "What are {i}you{/i} doing here?"


translate english day5_library_2_3df6714e:


    me "Am I not allowed to be here?"


translate english day5_library_2_30a65a09:


    "I said rudely to her."


translate english day5_library_2_7ebd37d9:


    "Alisa was clearly a bit overwhelmed."


translate english day5_library_2_f1713752:


    dv "Ah, what do I care...?"


translate english day5_library_2_866ddbdc:


    "She snorted and headed to Zhenya's table."


translate english day5_library_2_de1f890a:


    me "And why are you here then?"


translate english day5_library_2_6184e4df:


    "Alisa measured me with her eyes carefully and almost opened the mouth to say something, but then seemed to change her mind and turned away, hiding her hands behind her back."


translate english day5_library_2_fe41f14c:


    me "Returning a book?"


translate english day5_library_2_91b7e642:


    "I blurted the first thing off the top of my head."


translate english day5_library_2_2d3c80f4:


    dv "It's none of your business..."


translate english day5_library_2_eb262731:


    "She replied with a hint of hesitation."


translate english day5_library_2_1c67eb95:


    me "What book is it?"


translate english day5_library_2_991d9a14:


    "Alisa was silent."


translate english day5_library_2_d8028619:


    me "Oh, come on, let me see it!{w} I wonder what Miss 'High-voltage-keep-away!' reads?"


translate english day5_library_2_2d3c80f4_1:


    dv "It's none of your business…"


translate english day5_library_2_f7c41790:


    "Her voice was even less confident."


translate english day5_library_2_42f650e4:


    me "Ok, ok, I don't insist or anything…"


translate english day5_library_2_76998bd7:


    "In fact, I was quite interested to find out what Alisa was reading."


translate english day5_library_2_66eefb60:


    "Moreover, I was quite amused to see a book in her hands."


translate english day5_library_2_a36340e5:


    "TV, movies, or a computer, if one were available here – all these things seemed to be much more appropriate entertainment for a girl like her."


translate english day5_library_2_30470e07:


    "But she had a book instead…"


translate english day5_library_2_a1a458f1:


    "My curiousity won. I struck at the right moment, when Alisa was looking away from me, and snatched the book."


translate english day5_library_2_97a3e89b:


    dv "Ouch!"


translate english day5_library_2_c9e0274f:


    "She screamed."


translate english day5_library_2_a1b7c998:


    "In the following second her face took on such an expression that it made me question my decision."


translate english day5_library_2_38429226:


    th "If I'm about to die at least I will know what for."


translate english day5_library_2_a0da6e68:


    "I held a copy of 'Gone with the Wind' in my hands."


translate english day5_library_2_8d03e8c1:


    "That was the same book that Lena was reading that evening on a bench."


translate english day5_library_2_29dc2955:


    "I was so astonished that I completely forgot about my imminent death."


translate english day5_library_2_6958082e:


    me "Is it interesting?"


translate english day5_library_2_3b4b97c0:


    dv "Yeah…"


translate english day5_library_2_eb7dd1a8:


    "Alisa answered without any enthusiasm, blushing."


translate english day5_library_2_621c7bd3:


    me "Ok then…"


translate english day5_library_2_75b79b0a:


    "I handed the book back to her."


translate english day5_library_2_df52def3:


    "Alisa threw it on the table and left the library quickly without looking at me."


translate english day5_library_2_c3a92102:


    th "So human things aren't alien to her.{w} In the end, she too is a girl."


translate english day5_library_2_fe404d31:


    "After a quick review of everything that just happened, I concluded that there was actually nothing that strange."


translate english day5_library_2_e499eebb:


    "Curiosity killed the cat."


translate english day5_library_2_0dc343a6:


    th "Anyway, it's Alisa – and that means it could quickly turn into a total mess, plus I still have ingredients to collect."


translate english day5_library_2_ab719773:


    dv "Well, I'll come later..."


translate english day5_library_2_24b51b37:


    "Alisa left the library quickly without looking at me."


translate english day5_library_2_89141735:


    "It got me thinking."


translate english day5_library_2_72ba180f:


    th "What could this book could be about if she was so ashamed of it?"


translate english day5_library_2_4521510a:


    th "An ashamed Alisa is something extraordinary by itself…{w} But Alisa, ashamed about a book…"


translate english day5_library_2_10c14b59:


    th "But what's the point in guessing now – there's no way to find anything out."


translate english day5_library_2_4160881a:


    "Finally, Zhenya's deep groan rang out, reaching each and every corner of the library."


translate english day5_library_2_bece771e:


    mz "Grab it!"


translate english day5_library_2_98368a17:


    "I passed by the book shelves and beheld the perspiring librarian sitting near the trapdoor leading into the basement with a small sack next to her."


translate english day5_library_2_7959d521:


    th "Well, they might have some sort of a storehouse down there…"


translate english day5_library_2_553525de:


    me "Thanks!"


translate english day5_library_2_821c2037:


    "I took the sack and left the library."


translate english day5_library_2_089e3478:


    "Thank goodness it wasn't too heavy, so I carried it down to Olga Dmitrievna's cabin without too much effort."


translate english day5_main3_8645b244:


    "Finally, it seems that everything had been collected."


translate english day5_main3_4f706171:


    "I took the handcart with sugar outside and put the sack with the flour in it, followed by the two baskets of strawberries that somehow fitted in between."


translate english day5_main3_ee5f67e3:


    "And the beer was hidden under my shirt, just in case."


translate english day5_main3_f702be22:


    "The day was coming to its end so I had to hurry, as cake itself will need some time to bake."


translate english day5_main3_1546fb97:


    "Of course, I'd rather enjoy lying down, closing my eyes and getting a decent sleep, but I just couldn't let Olga Dmitrievna down."


translate english day5_main3_050e260b:


    "Indeed, after all the trouble I'd gone to, I even felt personally responsible for the success of this event."


translate english day5_main3_bfc08830:


    "Coming to the square, I stopped for a moment to catch my breath."


translate english day5_main3_dcabd394:


    "It wasn't that the cart that was heavy – it ran smoothly without any noticeable effort required. It was just that any physical exertion caused pain to me now.{w} Both a physical and mental one."


translate english day5_main3_fc8d210b:


    "I sat down on the bench and closed my eyes for a moment."


translate english day5_main3_5dad0356:


    dreamgirl "What's that?"


translate english day5_main3_4999a965:


    "I didn't really give a damn who it was – probably just a fellow pioneer girl taking an interest in an unfamiliar companion in distress."


translate english day5_main3_cb23542f:


    me "What are you talking about?"


translate english day5_main3_b037ba1a:


    "I asked her tiredly."


translate english day5_main3_d00d3d0f:


    "She didn't reply."


translate english day5_main3_b9035f5a:


    me "Those're the ingredients for a cake...{w} Do you like cakes?"


translate english day5_main3_3d6bef45:


    dreamgirl "I don’t know..."


translate english day5_main3_159dd052:


    me "What, you've never tried cake?"


translate english day5_main3_3d6bef45_1:


    dreamgirl "I don’t know..."


translate english day5_main3_9ee429fd:


    "Obviously the girl didn't get what I was talking about, but it didn't surprise me at that moment."


translate english day5_main3_7285209b:


    "I really wasn't interested in the conversation. I was so tired that I had zero intention of classifying external distractions and tagging them as either common or uncommon."


translate english day5_main3_6978c07f:


    me "I see..."


translate english day5_main3_0a00a780:


    me "Come down the canteen later and have a bite."


translate english day5_main3_f1668a0e:


    dreamgirl "Really?"


translate english day5_main3_dae36352:


    me "Really."


translate english day5_main3_5b24c110:


    dreamgirl "And what are they made of?"


translate english day5_main3_2a985733:


    me "What?"


translate english day5_main3_fad6f3d4:


    "I asked indifferently."


translate english day5_main3_52a831f5:


    dreamgirl "These...{w} Cakes!"


translate english day5_main3_46d149a2:


    me "Well...{w} Some flour, some sugar, various fillings..."


translate english day5_main3_61fefa63:


    th "Now that's a strange question, doesn't she know what cakes are made of?"


translate english day5_main3_0fa8d81f:


    dreamgirl "And you have it all here?"


translate english day5_main3_c8b42142:


    me "Yeah, sort of."


translate english day5_main3_749fd519:


    dreamgirl "And sugar?"


translate english day5_main3_e2a3a4fa:


    me "And sugar..."


translate english day5_main3_b206627b:


    dreamgirl "Could you lend me a little?"


translate english day5_main3_7332342b:


    me "What for?"


translate english day5_main3_9d2d461d:


    "I thought it was over the top."


translate english day5_main3_15d24d3a:


    "A sudden gust of wind made me grab the cart instinctively and open my eyes."


translate english day5_main3_0cf7617c:


    "However, nobody was there."


translate english day5_main3_195802d1:


    th "Am I daydreaming?"


translate english day5_main3_0ee6c06e:


    "I noticed, though, that the sugar sack was untied and a small heap of it had poured out."


translate english day5_main3_2fb04556:


    th "Could it be that she was scared by the wind and ran away...?"


translate english day5_main3_5e73ac52:


    "Having fixed the sack, I got up from the bench and continued on my challenging strawberry way."


translate english day5_main3_d3d42b9c:


    "There wasn't a single person near the canteen.{w} No wonder – dinner was still an hour away."


translate english day5_main3_1b6a6dde:


    "I brought the handcart to the rear exit and handed the foodstuffs to the camp cook."


translate english day5_main3_17448fb7:


    "She must have already been told what to do with them as she gave me such an unpleasant look.{w} I'm not sure how long it takes to bake a cake, but it seems she had to hurry."


translate english day5_main3_088c8385:


    "I just wanted to relax for the remaining time up until dinner."


translate english day5_main3_294475af:


    "In short, I was so tired that I just sat on the steps and waited."


translate english day5_main3_89ce1a6f:


    "My eyes closed themselves – I guess I got so tired throughout the day that I didn't even notice how someone came up to me until they patted me on the shoulder."


translate english day5_main3_a0a72914:


    mi "Hi!"


translate english day5_main3_7629afc5:


    "Miku was standing before me."


translate english day5_main3_b5cc7e1c:


    me "Yeah..."


translate english day5_main3_6aa1252d:


    "I didn't need a mirror to imagine the expression of scepticism and annoyance on my face."


translate english day5_main3_11ad29bb:


    mi "Oh, excuse me, I must have interrupted you..."


translate english day5_main3_df78914f:


    me "No problem, I was just sitting here."


translate english day5_main3_618d7741:


    mi "Ah, alright then!"


translate english day5_main3_1af920ee:


    "Miku beamed with a smile."


translate english day5_main3_3f7156f6:


    mi "I was just coming to dinner. I thought that it's time already and then it appeared to me that it's too early, but I decided to check just in case – maybe it's not me who's mistaken, but the clock is!"


translate english day5_main3_bf5ddeb1:


    mi "Well, not the clock, clocks can't be mistaken, it's just that I misread it..."


translate english day5_main3_3c421d62:


    "She seemed to be ultimately confused now and fell silent."


translate english day5_main3_7fe5f005:


    me "It's still about half an hour before dinner."


translate english day5_main3_8824776e:


    mi "Oh, that's great, then I'll sit here and wait with you if you don't mind?"


translate english day5_main3_203abf9f:


    "Frankly speaking, I do mind."


translate english day5_main3_05c9aec0:


    me "You know, I have some matters to attend to..."


translate english day5_main3_e44244b3:


    "I stood up quickly and left without saying goodbye, ignoring Miku as I always did, while she screamed something after me."


translate english day5_main3_b094f8aa:


    "A minute later I got to the square and sat on the bench with the firm intention of finding a quiet and safe place here to wait for dinner."


translate english day5_main3_da5773ae:


    "I think this is the first time in the last four-and-a-half days when I've felt like this."


translate english day5_main3_de22be40:


    "I wasn't just irritated because of some insignificant details but, indeed, I was really angry."


translate english day5_main3_8b26ac2a:


    "I've completely stopped caring about where I am and why am I here."


translate english day5_main3_9e4b97f1:


    "I don't care about how to get out either."


translate english day5_main3_07d4e30c:


    "What's driving me mad is that I always have to carry out some stupid task given by our camp leader and it's always me who gets into stupid situations and sometimes even ends up looking like a clown."


translate english day5_main3_bd134020:


    th "If all of this is some alien trick or a plot of the Universal Mind they better consult with their psychiatrist!"


translate english day5_main3_13370091:


    "I gritted my teeth and clenched my fists."


translate english day5_main3_8ea911ef:


    th "And most annoying thing is that everything that happens seems to happen by itself somehow!"


translate english day5_main3_e2dfefc7:


    th "I'd be happier not having to carry around bags of sugar that weigh a ton, but I had no choice at all!"


translate english day5_main3_12a8d8d9:


    th "I mean, any other option would lead to much worse consequences than a muscle strain or hurt pride…"


translate english day5_main3_60e71618:


    us "Who are you angry with?"


translate english day5_main3_4a8fcfb4:


    "Ulyana was standing in front of me and smiled slyly."


translate english day5_main3_90b46808:


    me "Nobody really…"


translate english day5_main3_12a0290a:


    "I answered absently."


translate english day5_main3_4758c0c0:


    "But my fists gave me away."


translate english day5_main3_be723785:


    me "Just like that…"


translate english day5_main3_40bd06ac:


    us "Ok, ok, It's up to you…{w} You better tell me, why did you run around the camp the whole day with some kind of bags?"


translate english day5_main3_d8e5e6d0:


    me "I had to."


translate english day5_main3_d20f4795:


    "I've replied reluctantly."


translate english day5_main3_a0086905:


    us "I guess it was food."


translate english day5_main3_726459ba:


    me "Maybe it was…"


translate english day5_main3_28b2648c:


    "Ulyana was about to say something, but at that moment the bell rang, calling the pioneers for dinner."


translate english day5_main3_f1fc33c2:


    "I sighed in relief and quickly headed to the canteen, leaving Ulyana behind."


translate english day5_main3_1d7b62f5:


    mt "Semyon, thank you very much!"


translate english day5_main3_9508d41b:


    me "For what?"


translate english day5_main3_92e433aa:


    "The camp leader gave me a friendly smile."


translate english day5_main3_f601ce01:


    mt "For the cake of course!"


translate english day5_main3_707cf821:


    me "Ah, sure..."


translate english day5_main3_e512b012:


    "It was at that exact moment that I understood the true meaning of the saying 'Keep your thanks to feed your cat'."


translate english day5_main3_374f0622:


    mt "You didn't tell anybody? This ought to be a surprise!"


translate english day5_main3_9513cd87:


    me "Yeah..."


translate english day5_main3_6a31846d:


    mt "That's my boy!{w} And now off you go to dinner!"


translate english day5_main3_937d7bb8:


    "Olga Dmitrievna waved her hand, pointing at the canteen."


translate english day5_main3_5452915f:


    "I stepped through the doorway slowly and started to look for a free place."


translate english day5_main3_f6bdde48:


    "It turned out there were plenty today so I got the chance to eat all alone."


translate english day5_main3_2061d557:


    "There was fish with mashed potatoes for dinner."


translate english day5_main3_062530c8:


    th "What misfortune once again – I'll be left half hungry.{w} And didn't we have fish for lunch...?"


translate english day5_main3_0e40ce73:


    th "Is it a fish-only day today?!"


translate english day5_main3_ce79b819:


    "Having pushed my plate with the fried sea dweller away, I laid my head on my hands and closed my eyes."


translate english day5_main3_d13c0f93:


    "But soon somebody came up to the table."


translate english day5_main3_44b645c0:


    sl "Hey, are you alright?"


translate english day5_main3_84662d7a:


    me "I'm fine..."


translate english day5_main3_35836741:


    "I replied without changing my position."


translate english day5_main3_cb89aafa:


    sl "Just tired?"


translate english day5_main3_bf1c2987:


    me "Yeah, a little..."


translate english day5_main3_17c764f2:


    sl "That's bad."


translate english day5_main3_81e2449f:


    "Slavya said it seriously."


translate english day5_main3_17c6b21c:


    me "Of course…"


translate english day5_main3_4fb291b0:


    sl "You remember that we are going for the hike after the dinner, don't you? Have you prepared everything?"


translate english day5_main3_25632213:


    me "What? Where?"


translate english day5_main3_9830e8f7:


    "I opened my eyes and lifted my head up instantly."


translate english day5_main3_bf312592:


    "Lena was standing by Slavya."


translate english day5_main3_cd82f264:


    sl "The hike…"


translate english day5_main3_a4614188:


    "She was surprised."


translate english day5_main3_1acd2585:


    sl "Didn't you know?"


translate english day5_main3_5cb822ee:


    me "No..."


translate english day5_main3_63edcced:


    "I put my head down on the table and covered it with my hands."


translate english day5_main3_72338241:


    th "If only I could sink into the ground right away…"


translate english day5_main3_34d8cd0a:


    "The girls remained silent."


translate english day5_main3_cfa6c6ec:


    "I was left alone with my thoughts for some time and that was fine by me."


translate english day5_main3_cdac6a32:


    "Maybe I could have sat that way until the end of dinner time, but the strong voice of Olga Dmitrievna was heard from the opposite end of canteen."


translate english day5_main3_0b4ca722:


    mt "Guys!{w} To celebrate the miraculous rescue of our friend and comrade Shurik, we baked this cake for you all!"


translate english day5_main3_639a506c:


    "I lifted my head idly and looked towards the camp leader, but couldn't see anything beyond the pioneers' backs."


translate english day5_main3_c6fcef15:


    mt "A second… Just a second…"


translate english day5_main3_a783f6df:


    th "And nothing about me.{w} Nothing about me rescuing Shurik or gathering the ingredients for the cake…"


translate english day5_main3_6400b884:


    th "As if that's how it ought to be."


translate english day5_main3_df46be92:


    th "Well, it would be wrong to expect anything else from our camp leader."


translate english day5_main3_6ab86cd0:


    sl "Let's go!{w} Or we won't get our share!"


translate english day5_main3_c17a722d:


    "Slavya smiled."


translate english day5_main3_a8b5b91a:


    un "Let's go."


translate english day5_main3_c1956a30:


    "Lena agreed."


translate english day5_main3_3b03af3a:


    me "Yeah, sure..."


translate english day5_main3_a2f9d7eb:


    "I got up reluctantly and tagged along behind the girls."


translate english day5_main3_f95eb2e4:


    "As we approached the crowd of pioneers, Olga Dmitrievna was just putting the cake in the centre of the table."


translate english day5_main3_9e9a688f:


    mt "And now..."


translate english day5_main3_902ab03d:


    "The camp leader wasn't able to finish as Ulyana rushed out from the pioneer crowd and dived into the cake."


translate english day5_main3_44ef2910:


    "She managed to nibble it a few times before she was pulled away."


translate english day5_main3_3ec376bc:


    "She was kicking and screaming."


translate english day5_main3_72ace9f9:


    "I stared blankly from the outside at all this drama: Alisa smiling; Lena picking up some cream with her finger; all the furious pioneers around."


translate english day5_main3_7f98185e:


    "I felt completely out of place here. I thought that if I closed my eyes now and opened them again – here I am, back to the safety of my apartment in front of a computer."


translate english day5_main3_f569f26d:


    "I blinked but nothing changed; only the noise and the confusion became sharper."


translate english day5_main3_87959490:


    mt "Ulyana! That's the limit!"


translate english day5_main3_34125318:


    us "I...{w} I just..."


translate english day5_main3_6429cb93:


    "Well, in fact behaving like this is a bit over the top even for her."


translate english day5_main3_d677809e:


    "Shurik broke into the conversation – or is it a court-martial?"


translate english day5_main3_58d5ebe0:


    sh "Please, Olga Dmitrievna! Since the cake is celebrating my return, it's no big deal..."


translate english day5_main3_a6820f71:


    "He hesitated."


translate english day5_main3_57d1ba21:


    mt "It doesn't matter!"


translate english day5_main3_aac2b1f6:


    "The camp leader turned to Ulyana."


translate english day5_main3_33bbaf19:


    mt "And you...{w} Today I'm going to punish you to the fullest extent, so you'll behave next time!"


translate english day5_main3_935bc568:


    us "Ah, whatever!"


translate english day5_main3_99aa653e:


    "She snorted and turned back."


translate english day5_main3_cd01cddf:


    mt "You won't be going on the hike with us tonight!"


translate english day5_main3_2babf74f:


    us "As if I wanted to!"


translate english day5_main3_54e5777a:


    "I was more than willing to switch places with Ulyana and skip the hike instead of her, but who knew..."


translate english day5_main3_038c58cb:


    "If I had have guessed beforehand, I'd have been the first one to go berserk and smash the damn cake!"


translate english day5_main3_a4bcbbe9:


    "After a couple of minutes of confusion, the pioneers started to disperse."


translate english day5_main3_c997a5fb:


    mt "You have to get ready too!{w} There'll be a lineup at the square in half an hour."


translate english day5_main3_80aff84c:


    "I looked straight into the eyes of the camp leader, trying to express my attitude non-verbally, but it seems that I failed."


translate english day5_main3_a89a2c76:


    mt "Don't be late!"


translate english day5_main3_fd82295b:


    "Ulyana was sitting at the table when I approached her on my way to the exit."


translate english day5_main3_94be5d8c:


    me "So why did you do it?"


translate english day5_main3_4d3873eb:


    "She looked very upset.{w} But she had a right to be so."


translate english day5_main3_500104f1:


    us "I wanted to."


translate english day5_main3_e8371c45:


    "Ulyana replied abruptly."


translate english day5_main3_017bb18f:


    me "So, you're happy now?"


translate english day5_main3_2ddb5b95:


    us "Of course I am!{w} And you – good luck with the hiking!"


translate english day5_main3_72d5c9fe:


    "She smiled mischievously, sprang up and rushed out of the canteen."


translate english day5_main3_b53a9fd1:


    th "Well, a little bit of luck wouldn't hurt..."


translate english day5_main3_a9aa1a31:


    "'Walk and you shall reach' – is was exactly this proverb that kept whirling in my head all the way down to the camp leader's cabin."


translate english day5_main3_831c71df:


    "Somehow I just couldn't manage to argue, to pretend that I'm sick, or to just skip it without a reason."


translate english day5_main3_f85ee780:


    "The events of this day taught me submissiveness.{w} Although sometimes whatever was happening made no sense to me."


translate english day5_main3_4da475d5:


    "As I walked in, I had a thought."


translate english day5_main3_96c41623:


    th "Well, in fact, how should I get ready?"


translate english day5_main3_9679533e:


    th "Clothes? I only have an overcoat and a pair of jeans.{w} Anyway, I forgot to ask whether it's going to be an overnight hike or not."


translate english day5_main3_fd2a6187:


    "I couldn't think of anything better, so I grabbed the sweater that I had on me when I've arrived in the camp (the night might be fairly chilly) and shuffled slowly off to the square."


translate english day5_main3_ca17b192:


    "All the camp was already there, although it was about ten minutes before the time Olga Dmitrievna had designated."


translate english day5_main3_1e295624:


    "I settled down near the edge and waited patiently."


translate english day5_main3_fd3eb8d2:


    "Night was falling."


translate english day5_main3_d20ed5d2:


    th "If viewed from the outside, what a fairly comical picture we would make: a crowd of pioneers, lined up out of a sheer habit, like they were awaiting a silent command from the bronze Genda."


translate english day5_main3_018d282a:


    th "And all this happening in the scarlet rays of the sunset."


translate english day5_main3_b7cd9dd7:


    "And there he is, waving his hand and screaming 'attack' as roaring soldiers with red neckerchiefs charge into the battle against some ghostly enemy…"


translate english day5_main3_18d7dd2e:


    "But Olga Dmitrievna showed up and started to talk instead of Genda."


translate english day5_main3_553eaa56:


    mt "It seems everybody is here…{w} Great!"


translate english day5_main3_a8c428b5:


    "I was so tired today that I couldn't even think about anything, so I ended up just listening to the camp leader."


translate english day5_main3_443f99aa:


    mt "Now, today we'll go hiking!"


translate english day5_main3_747aae61:


    mt "It is essential for every pioneer to be able to come and rescue his comrade, to offer a helping hand in the hour of need, saving them from a hopeless situation!"


translate english day5_main3_fc6094ea:


    mt "We have to learn to do all these things together!"


translate english day5_main3_fc0d9c17:


    "A whisper that ran through the pioneer crowd suggested that most probably this truly epic expedition would end in a clearing in the forest, several hundred feet away from the square."


translate english day5_main3_038d0ddf:


    "Somehow I thought so too."


translate english day5_main3_81d86d37:


    mt "We'll walk in pairs.{w} So, if you haven't chosen a partner for yourself yet, now's the right time do so!"


translate english day5_main3_30d15b67:


    "Pioneers quickly caught on to the idea and started to match in pairs."


translate english day5_main3_ae8616af:


    "It looked like I was the only one without a partner."


translate english day5_main3_a9a03604:


    "Slavya was enthusiastically discussing something with Olga Dmitrievna, Lena was with Miku, Electronik was, of course, with Shurik."


translate english day5_main3_aea0ad16:


    th "Well, it might be not a bad idea to go alone after all."


translate english day5_main3_105fc865:


    mt "Semyon!"


translate english day5_main3_4e47d15f:


    "The voice of the camp leader pulled me out of my thoughts."


translate english day5_main3_92f6d31e:


    "I went up to her reluctantly."


translate english day5_main3_8dfde425:


    mt "I see that you haven't found a partner."


translate english day5_main3_d3242212:


    me "Seems like it..."


translate english day5_main3_d1a7c5c7:


    mt "Then you'll join Zhenya – she is alone too."


translate english day5_main3_27ad27f5:


    "I was struck with that special kind of despair that only a true loner can experience."


translate english day5_main3_b7994624:


    "So, it appears that I'm left with the prickly librarian that I wouldn't risk spending a couple of hours with even if I was paid for it?"


translate english day5_main3_b73326a6:


    th "Although, we both seem to be in the same boat now..."


translate english day5_main3_d17a396e:


    "I slowly approached Zhenya."


translate english day5_main3_6818c128:


    me "Well, I guess we'll be going together..."


translate english day5_main3_c25dd9ca:


    "She looked up at me."


translate english day5_main3_0af22cb1:


    mz "Don't you even think that I'm glad!"


translate english day5_main3_44ba86f4:


    "Said Zhenya seriously."


translate english day5_main3_5122493d:


    me "Why on earth you should feel glad?"


translate english day5_main3_d7b5efd3:


    "I asked naïvely."


translate english day5_main3_d0a8dd78:


    mz "Never mind!{w} It would be much better if you'd just shut up."


translate english day5_main3_f4dcd652:


    th "Eh, it couldn't get any better than this..."


translate english day5_main3_b28b669b:


    "She turned her back on me and followed the other pioneers."


translate english day5_main3_e32f998e:


    "I still haven't seen any special reason to walk in pairs."


translate english day5_main3_34fb045a:


    "Anyway, we were walking on the well-trodden forest trails and it would be quite difficult to get lost here even if I wanted to."


translate english day5_main3_d1f6b652:


    "Moreover, while we'd already been hiking for half an hour, we weren't rushing into the depths of the forest, trying to face all the dangers that could test our courage and harden our pioneer spirits, but instead were just walking around in circles."


translate english day5_main3_946c8923:


    "However, if we take into account that Olga Dmitrievna was our chief, this hike could be compared to a hobbit's march from the Shire to Mordor..."


translate english day5_main3_97574c1e:


    "Just as Zhenya insisted, I was following her at a distance in silence."


translate english day5_main3_41f63256:


    "The librarian seemed to be perfectly okay with it."


translate english day5_main3_6afab6eb:


    me "Hey, don't you know when we'll reach our destination?"


translate english day5_main3_67e23fdb:


    mz "Our what?"


translate english day5_main3_88370e84:


    me "Ahem, the place where we'll settle down and set up camp."


translate english day5_main3_059aa439:


    mz "The whole point of this hike isn't to set up a camp but the hiking itself!{w} You don't get it!"


translate english day5_main3_97b64a58:


    "Yeah, it seemed that I didn't understand a thing about hiking..."


translate english day5_main3_4587e3c3:


    me "I guess you're right, but still…"


translate english day5_main3_bb86bd36:


    mz "I don’t know!"


translate english day5_main3_75f80dfe:


    "She replied sharply and quickened her pace."


translate english day5_main3_133cde0a:


    "I caught up with her and asked:"


translate english day5_main3_e4b066d7:


    me "Listen, why are you always so..."


translate english day5_main3_17313088:


    "I was about to say 'mean', but stopped short."


translate english day5_main3_6687b26f:


    me "I haven't done anything bad to you and I'm not going to!"


translate english day5_main3_efd76d15:


    "She glared at me in surprise."


translate english day5_main3_296b44b5:


    mz "Always so... What?"


translate english day5_main3_231d5bf5:


    me "Well, unsociable... Kind of...{w} Or is it something about me?"


translate english day5_main3_da119946:


    mz "Oh cut that stupidity out already!"


translate english day5_main3_8b13fe9b:


    me "As you wish..."


translate english day5_main3_39a4936f:


    "I decided not to start a conversation with her for the rest of the hike."


translate english day5_main3_a20cefa7:


    "..."


translate english day5_main3_66653ce7:


    "At last Olga Dmitrievna decided to end this Sisyphean toil."


translate english day5_main3_f2e5e7d4:


    mt "It's time to halt."


translate english day5_main3_e9d2363e:


    "The place chosen turned out to be a quite large glade with a few tree trunks laid in half circle to make an improvised arbour, with the remains of a campfire in the middle."


translate english day5_main3_d3f88135:


    "Obviously such hikes are a tradition of this camp."


translate english day5_main3_e664fb77:


    "I was sent to gather firewood together with the other boys."


translate english day5_main3_5463f5b6:


    "It didn't take long because there were a lot of branches and logs of various sizes lying around."


translate english day5_main3_dcb20eaf:


    "Eventually Olga Dmitrievna lit the fire using some old newspapers."


translate english day5_main3_df660aed:


    "I was eager to know what was written there but couldn't discern anything other than Soviet symbols."


translate english day5_main3_44bc05c0:


    "Pioneers took their places on the fallen log benches and started to talk about things."


translate english day5_main3_0b4fac65:


    "It seems that the final goal of this event had been achieved."


translate english day5_main3_c751793a:


    th "The only things missing were a pot of fish soup, aluminium cups of vodka, and a guitar."


translate english day5_main3_a7859a66:


    "But I wouldn't be surprised if all if these would appear somehow."


translate english day5_main3_29dcd13b:


    sl "What are you thinking about?"


translate english day5_main3_a6893681:


    "Slavya sat next to me."


translate english day5_main3_60a79f40:


    me "Oh, nothing special...{w} Just enjoying the hike."


translate english day5_main3_27c62366:


    "I answered sarcastically."


translate english day5_main3_5e83eb9a:


    sl "You don't look too happy."


translate english day5_main3_f8cc5c73:


    me "Well, I'm not about to jump for joy, sorry about that."


translate english day5_main3_8f1735d3:


    sl "Okay, I won't disturb you. "


translate english day5_main3_70009934:


    "She sat with me for a while, but after realising that I wasn't in the mood of talking she left me to enjoy my introspection all alone."


translate english day5_main3_81fad12b:


    "And all I wished for was to lie down in bed and fall asleep as soon as possible, but I was being surrounded by smoke and the useless chatter of pioneers around me instead."


translate english day5_main3_9aaac94d:


    "They were cheering and laughing and in general enjoying this warm summer evening."


translate english day5_main3_2803f4ab:


    "In the far side of the glade I noticed that Lena was arguing with Alisa intensely."


translate english day5_main3_d93c3f94:


    "'Intensely' and Lena seemed like complete opposites to me."


translate english day5_main3_193e4c57:


    "Slavya had left to go somewhere after our conversation it seems."


translate english day5_main3_37441d60:


    "Electronik and Shurik were trying furiously to prove something to Olga Dmitrievna."


translate english day5_main3_3257d53a:


    "Looks like I'm the only one who doesn’t belong here."


translate english day5_main3_a4f8dc02:


    th "But on the other hand why should I care?"


translate english day5_main3_cb94daa6:


    "All I was doing was just watching the fire."


translate english day5_main3_c8d1a036:


    th "There is a saying that claims that one could watch it forever, as well as running water."


translate english day5_main3_68d47823:


    "But there was also some third thing there…"


translate english day5_main3_1d9e5974:


    th "What was that?"


translate english day5_main3_29414842:


    mt "One could watch three things forever: burning fire, running water and how other people work!"


translate english day5_main3_903e62de:


    "The camp leader pulled me out of my daydreaming."


translate english day5_main3_31151d59:


    mt "Semyon, don't you think that it's too early to relax?"


translate english day5_main3_0a50a8a2:


    me "But what else would I need to do?"


translate english day5_main3_435b54b7:


    "I honestly couldn't get what Olga Dmitrievna wanted from me."


translate english day5_main3_2c3aefa4:


    mt "I don't know…"


translate english day5_main3_2ebab91c:


    "She stopped for a moment."


translate english day5_main3_3e0f4283:


    mt "But if there is something to be done, then do it without hesitation."


translate english day5_main3_fa103e1d:


    "She smiled ambiguously and went back to the fire to throw a few branches on."


translate english day5_main3_8045c9cb:


    "After those words I am completely sure that she treats me like a personal slave."


translate english day5_main3_5d460c3e:


    "Or at least like I'm a free labour force, which is, strictly speaking, the same thing."


translate english day5_main3_5618085a:


    "I sighed and put my head down on my hands, hoping my torments would be over for today…"


translate english day5_main3_a582c672:


    "Someone patted me on the shoulder."


translate english day5_main3_008bd100:


    "I looked up and saw Shurik and Electronik, who sat next to me."


translate english day5_main3_132aeab6:


    me "What do you want?"


translate english day5_main3_b037ba1a_1:


    "I asked tiredly."


translate english day5_main3_a3f9f456:


    el "Don't be sad!"


translate english day5_main3_8afd5934:


    me "Is there anything better to do?"


translate english day5_main3_a2ade9c9:


    sh "Look, we've been discussing the possibilities for the advancement of the cybernetics club with Olga Dmitrievna…"


translate english day5_main3_13f2497e:


    sh "And there is a problem – we need more guys.{w} If you could…"


translate english day5_main3_a6820f71_1:


    "He hesitated."


translate english day5_main3_ac13c40b:


    th "'Advancement' and those guys are incompatible with each other."


translate english day5_main3_b77b4d76:


    "I said nothing and started to look over the pioneers around me instead."


translate english day5_main3_eb10acd3:


    sh "Well?"


translate english day5_main3_b9a3cb6a:


    me "I don't have time....{w} Can't you see that I'm always busy with the camp leader's errands?"


translate english day5_main3_74081b6b:


    sh "Yeah, I guess you're right...{w} It's kind of embarrassing, how it all went today with Ulyana."


translate english day5_main3_a3bf9ccf:


    "I looked at him with surprise."


translate english day5_main3_3e7b5ad7:


    th "It seems that Shurik blames himself for that cake accident."


translate english day5_main3_8043b470:


    me "Yeah, it is…"


translate english day5_main3_5bb39a9d:


    "All the pioneers seemed to be here but I couldn't spot Slavya anywhere."


translate english day5_main3_35484a41:


    sh "I think she is angry with me…"


translate english day5_main3_ad02f53a:


    me "Who?"


translate english day5_main3_fad6f3d4_1:


    "I asked absently."


translate english day5_main3_948b648b:


    sh "Ulyana.{w} Maybe I should apologise?"


translate english day5_main3_7fe19203:


    me "No, it's not your fault…"


translate english day5_main3_53bddd68:


    "We sat silently for a while and then I stood up and said:"


translate english day5_main3_37f21c35:


    me "My legs are numb, I better take a walk."


translate english day5_main3_d7826409:


    "They made no reply."


translate english day5_main3_d9cf7a94:


    "I made a few circles around our improvised camp, noticing the close looks of the camp leader following me."


translate english day5_main3_3b6642f3:


    "Looks like Olga Dmitrievna couldn’t wait to come up with some kind of new task for me."


translate english day5_main3_0724ca3e:


    "I haven't found Slavya anywhere."


translate english day5_main3_a3f3fccb:


    th "Maybe I should go and try to find her?"


translate english day5_main3_84b23eff:


    "On the other hand I felt sorry for Ulyana every time I recalled her upset face."


translate english day5_main3_d76fc57e:


    th "Maybe this hike isn't the most entertaining thing ever but sitting there all alone isn't any better either."


translate english day5_main3_016c7fc0:


    "But at the same time I didn't want to go anywhere…"


translate english day5_main3_3e703e80:


    "I was absolutely sure that Slavya was alright, so I decided to stay."


translate english day5_main3_d37bbb9f:


    th "But on the other hand why should I care about that witch!"


translate english day5_main3_f6cc82fd:


    th "I guess that's enough for me for today."


translate english day5_main3_c58cdcde:


    "I sat in my previous place and waited for the end of hike patiently, almost physically able to feel the looks the camp leader aimed at me."


translate english day5_main3_5b5fdf66:


    "At last she stood up and declared:"


translate english day5_main3_85ade4d1:


    mt "And now let's play Cities!"


translate english day5_main3_79d3e6a7:


    "I had nothing against the game itself."


translate english day5_main3_9432dd18:


    th "But it was obvious that the hike will take longer because of it…"


translate english day5_main3_6f4a9363:


    "Pioneers sat around the fire."


translate english day5_main3_1dff3b67:


    "I noticed Lena and Alisa, who took their places on a trunk opposite me."


translate english day5_main3_f58cf444:


    th "It seems that everything is alright."


translate english day5_main3_7db4f674:


    "And just a few minutes ago I'd thought the opposite while looking at their quarrel."


translate english day5_main3_9f3fc574:


    th "But anything is possible…"


translate english day5_main3_74340d5d:


    "I'd really like to know what they were talking about but it's impossible now and I could feel tiredness growing in me more and more."


translate english day5_main3_6615be77:


    "My mind was completely blank."


translate english day5_main3_99ab0d84:


    "To be precise, my head was so heavy that there was no place in it for ideas to unfold."


translate english day5_main3_e447cec9:


    "While in my better times my brain appeared to be a wide highway with millions of thoughts running by, chasing one another and causing major crashes, now it was more like a footpath lost in the woods which is used rarely and only in exceptional cases."


translate english day5_main3_3b240839:


    "Slavya didn't come back."


translate english day5_main3_b00a6ffe:


    th "Maybe she had something to do?"


translate english day5_main3_5147a728:


    "But, once again, there's no way to find out now."


translate english day5_main3_3f2cf4ef:


    mt "Ok, let's start!{w} Moscow!"


translate english day5_main3_dad4cc7b:


    "Pioneers started to name cities."


translate english day5_main3_3f3598ba:


    "Finally, it was my turn."


translate english day5_main3_58df1a5a:


    "I tried to listen closely to catch the first letter of the city I'll have to use."


translate english day5_main3_d1d876fd:


    me "Arkhangelsk."


translate english day5_main3_f374397d:


    "We played several rounds."


translate english day5_main3_0759a0f8:


    "Each new city name made it harder to remember everything that was mentioned before."


translate english day5_main3_e27c9e7a:


    "My attention was dissipating and I was already lost in all these capitals, megalopolises, villages and urban settlements."


translate english day5_main3_b45788c7:


    mt "Semyon! Semyon! It's your turn!"


translate english day5_main3_b4c8d23c:


    "Olga Dmitrievna brought me back to reality."


translate english day5_main3_60202c5c:


    me "Oh, excuse me... And what was the last one?"


translate english day5_main3_cb702f2c:


    mt "You're daydreaming again!{w} It was Sevastopol."


translate english day5_main3_25856564:


    me "Ok, then I'll say London..."


translate english day5_main3_9affe25e:


    mt "Already used."


translate english day5_main3_bad80ec1:


    me "Well, then..."


translate english day5_main3_63447575:


    "It got me thinking.{w} There were tons of cities in the world, starting with 'L', but it was hard to remember even one of them now."


translate english day5_main3_b6595b35:


    me "Liverpool?"


translate english day5_main3_6bbc1d1e:


    mt "Already there!"


translate english day5_main3_b9a2a957:


    me "Los Angeles?"


translate english day5_main3_4c5275f0:


    mt "Ah, finally!"


translate english day5_main3_e7e84af8:


    "She gave me a scornful look but the game went on."


translate english day5_main3_fca355af:


    "I could hardly bear to think of another city starting with 'L' but, fortunately, it was the last round."


translate english day5_main3_4e97d333:


    mt "Ok, that's enough for today! It's already late, time to go back!"


translate english day5_main3_99742b57:


    "I sighed with relief."


translate english day5_main3_5e1a747b:


    "On our way back we walked as we liked without joining in pairs."


translate english day5_main3_d3fc10d2:


    "Night descended upon a camp.{w} A perfectly regular and normal night."


translate english day5_main3_46ada0f2:


    "It was one of those nights when dark skies, stars and even a crescent moon don't cause any special feelings and the chirping of crickets and the songs of the night birds seem more like routine work noises than a nocturnal chorus."


translate english day5_main3_a20cefa7_1:


    "..."


translate english day5_main3_604b0d7d:


    "In a few minutes all the pioneers were lining up in the square."


translate english day5_main3_5e7a97d2:


    "It was quite late already and fatigue took it's toll so our lineup wasn't perfectly aligned."


translate english day5_main3_26bc47b6:


    "It looked to me more like a line of vikings after a successful battle where the warriors are happy and smiling, anticipating their return to their families rather than thinking about maintaining correct formation."


translate english day5_main3_86ecb47a:


    "But someone else could possibly see a completely defeated troop or a bunch of survivors who have to march to their homeland with the last of their strength."


translate english day5_main3_75fc1be9:


    mt "Thanks everyone!{w} And now go to sleep, it's late already!"


translate english day5_main3_539072ed:


    "Pioneers quickly each ran their own ways and I was left together with the camp leader."


translate english day5_main3_ca1d4f17:


    mt "And we should go too!"


translate english day5_main3_006c39f4:


    "We went in complete silence to Olga Dmitrievna's cabin."


translate english day5_main3_79ed0be1:


    mt "Ok, let's sleep!"


translate english day5_main3_899c3f81:


    "She said, turning off the light."


translate english day5_main3_ae190c31:


    "I was tossing and turning for quite a while, recalling all the events of the day."


translate english day5_main3_8212c1e8:


    "On the one hand, I was overcome with fatigue.{w} On the other hand – I couldn't shake the feeling that I'd forgotten something, done something wrong, said something wrong..."


translate english day5_main3_a6f64aad:


    "And this feeling of incompleteness was tormenting me."


translate english day5_main3_9cd093b9:


    "It was about 2 A.M."


translate english day5_main3_7962d319:


    "All bad things would end sooner or later.{w} Or at least would take a break..."


translate english day5_main3_d3e44219:


    "I fell asleep."


translate english day5_dv_b3a56e2b:


    "Only Lena and Alisa stood out among all this splendor."


translate english day5_dv_9494f7ea:


    "Well, of course it was quite natural for Alisa to be arguing with someone like that."


translate english day5_dv_49a0c8bc:


    "But hearing Lena talk in a raised voice..."


translate english day5_dv_9844ee70:


    "I came closer sliently, trying to understand what was going on."


translate english day5_dv_6ca87438:


    dv "No, you listen to me!"


translate english day5_dv_76e80067:


    un "Think whatever you want, I've said everything!"


translate english day5_dv_d1acba6b:


    th "It looks serious."


translate english day5_dv_7d833908:


    "I tried as hard as I could to not attract any attention and look like I'm just standing there with no intention of listening in on them."


translate english day5_dv_65a0ca80:


    dv "There's nothing to think about – everything is as clear as day!"


translate english day5_dv_08837eb1:


    dv "Don't try to wind me around your little finger like that, I know you too well."


translate english day5_dv_85d1fa2f:


    un "Why, of course you know everything! Then why won't you tell him yourself?"


translate english day5_dv_cf4dd80f:


    "Lena was getting angry. That alone was really weird, even considering that I didn't know know what they were arguing about."


translate english day5_dv_7dfc2afa:


    dv "That's none of your business!"


translate english day5_dv_12152c43:


    "Alisa snorted, turned around, and her eyes met mine."


translate english day5_dv_b91f8b82:


    "Second later, Lena gave me a look as well."


translate english day5_dv_aeac4053:


    "The girls stood there in confusion for some time."


translate english day5_dv_a22d002f:


    dv "You're...{w} Eavedropping?!"


translate english day5_dv_e808d0e5:


    me "Me?{w} Oh, no-no-no! I was just... like... passing by..."


translate english day5_dv_61947ed2:


    "I put on the best smile I had, but it didn't seem to help much."


translate english day5_dv_f829601e:


    dv "By the way, today he was peeping at me!"


translate english day5_dv_31e62164:


    "Alisa gave a malicious smile."


translate english day5_dv_74229088:


    "Lena looked at me questioningly."


translate english day5_dv_57da622c:


    th "First of all, why would she believe this devious fox's stories?"


translate english day5_dv_d122756b:


    me "I didn't peep!{w} You know quite well that it was an accident! Ulyana should be the one to blame!"


translate english day5_dv_f4cf67d8:


    dv "Sure it was – tell that to the cops!"


translate english day5_dv_b0cbdad9:


    th "It seems like my excuse was not working."


translate english day5_dv_93816b7f:


    dv "Did you like my boobs?"


translate english day5_dv_169666e8:


    "A shiver ran up my spine."


translate english day5_dv_8bfbf78c:


    un "Is that...{w} true?"


translate english day5_dv_b17dc3c0:


    "Lena looked at me imploringly."


translate english day5_dv_78824aab:


    me "Well, you see, it was an accident...{w} I didn't see anything there."


translate english day5_dv_7394ce16:


    dv "You didn't? Well, I can show you again!"


translate english day5_dv_a55bd08a:


    "Alisa cried in anger."


translate english day5_dv_a03136b3:


    "I didn't know what to say."


translate english day5_dv_c67e9088:


    dv "You see, I told you!"


translate english day5_dv_709e3a50:


    un "No... No..."


translate english day5_dv_0d581cb1:


    "Lena started to mumble, and rushed away a moment later."


translate english day5_dv_87a2f256:


    me "Wait, what happened!?"


translate english day5_dv_3da1183b:


    "I shouted after her."


translate english day5_dv_0cd43818:


    dv "You see what you did – that girl is upset now!"


translate english day5_dv_31e62164_1:


    "She gave a spiteful smile."


translate english day5_dv_fab61109:


    me "First of all – it wasn't me who upset her! Second – I don't have any idea what you two were arguing about!"


translate english day5_dv_2b19dcc7:


    "I was running out of patience."


translate english day5_dv_b578411d:


    dv "Why should I tell you?"


translate english day5_dv_65ea82d5:


    "It seems like she thought that our talk was over and was about to leave."


translate english day5_dv_0c737116:


    "I grabbed her hand abruptly."


translate english day5_dv_6a189dfc:


    "Alisa glanced at me in fear, but didn't say anything."


translate english day5_dv_277c9533:


    "I was angry at that moment. Really angry!"


translate english day5_dv_f6618979:


    "First, I was wildly enraged that Lena was upset because of me (even though I was actually kind of guilty)."


translate english day5_dv_7c8a30ee:


    "Second, I was enraged by Alisa's insolence."


translate english day5_dv_513493ad:


    "Third, I was so tired that I was about to lose my temper at the smallest thing."


translate english day5_dv_2bf8f738:


    me "Happy now?"


translate english day5_dv_9c13f0cc:


    "I hissed pointing into the darkness where Lena had run off."


translate english day5_dv_240840c7:


    me "You only seem to have the guts to bomb memorials and hurt innocent girls!"


translate english day5_dv_f71a0fac:


    "She looked frightened, saddened and confused at the same time."


translate english day5_dv_050fb154:


    dv "She herself...{w} could hurt anyone!{w} You just don't know her!"


translate english day5_dv_73f6779b:


    me "The only truth here is that I don't know her too well. But I'm absolutely positive that she wouldn't hurt anyone! That's your job!"


translate english day5_dv_03cd505c:


    "We stood in silence for some time."


translate english day5_dv_60d9b7af:


    "I held Alisa's hand and didn't know what to do next."


translate english day5_dv_47ae85aa:


    "The solution dawned upon me all by itself."


translate english day5_dv_370eed20:


    me "You have to go and apologise!"


translate english day5_dv_f9f3ed76:


    dv "Why the hell should I?"


translate english day5_dv_fd492212:


    "Alisa tried to act as arrogant as always, but it wasn't going to work this time."


translate english day5_dv_7b8ee948:


    me "'Cause I said so!"


translate english day5_dv_983d54f9:


    "I refused to hear any objections and dragged her to the side of the bonfire."


translate english day5_dv_ff2ed8eb:


    me "Olga Dmitrievna, I'm sorry, we have some urgent business to deal with, so we have to leave early."


translate english day5_dv_7d32c910:


    "The guidance officer tried to object, but I didn't listen and headed after Lena, pulling Alisa along with me."


translate english day5_dv_a69e912e:


    "After some time, I released her hand."


translate english day5_dv_e687fb2b:


    me "Any objections?"


translate english day5_dv_552eae04:


    "All the pride and arrogance had left Alisa's face long ago."


translate english day5_dv_5f5a4508:


    dv "If you want me to come with you I will, but I have nothing to apologise for – I told her the truth."


translate english day5_dv_f99126ea:


    me "We'll see about that then."


translate english day5_dv_a767512c:


    "I cut her short."


translate english day5_dv_29f6f60c:


    "We had been walking in silence until we entered the camp."


translate english day5_dv_9cee08fa:


    "We came to the square and I asked Alisa:"


translate english day5_dv_2442aa77:


    me "Where do we go next?"


translate english day5_dv_37160fae:


    dv "How should I know?"


translate english day5_dv_65c09781:


    me "And who said that they know Lena well? Was it me?!"


translate english day5_dv_571e60de:


    "Alisa hesitated."


translate english day5_dv_8a9865ff:


    dv "Well, she could be at the island...{w} She often goes there when she wants to be alone."


translate english day5_dv_a499094f:


    me "Excellent!"


translate english day5_dv_c8bd92fc:


    "We headed to the pier."


translate english day5_dv_cffd7334:


    "Night came down on the camp while I searched for paddles and pottered around with the ropes."


translate english day5_dv_07f664e2:


    "One boat was actually missing."


translate english day5_dv_e631a0c0:


    me "Okay, today is your lucky day, you get to do several killer exercises that will build up your biceps, triceps and other arm muscles."


translate english day5_dv_27c62366:


    "I said sarcastically."


translate english day5_dv_25a535c0:


    dv "Are you serious?"


translate english day5_dv_883a35cc:


    "Alisa cast a fearful glance at me."


translate english day5_dv_183b833e:


    th "Well, perhaps that was a little too rough.{w} Even for her..."


translate english day5_dv_5b893c52:


    me "Alright, I won't make you row, of course..."


translate english day5_dv_1b5cc40c:


    "That hardly sounded confident."


translate english day5_dv_36f1c607:


    "Swinging the paddles appeared to be much harder this time."


translate english day5_dv_a5ba40b7:


    "Well, that's not surprising, considering that today I must've already filled my annual rowing quota."


translate english day5_dv_968f6af6:


    "I stopped in the middle of the river to catch my breath."


translate english day5_dv_ee0bd590:


    "The camp was covered by night.{w} A trivial and ordinary night."


translate english day5_dv_46ada0f2:


    "It was one of those nights, when the dark sky, stars and crescent moon don't make you feel anything special, and chirping crickets and twittering birds sound like they are doing a dull routine job, rather than performing an otherwise exciting nocturne."


translate english day5_dv_4b0e60fa:


    "I was staring into the darkness, trying to make out the shadowy island."


translate english day5_dv_b4ddcd02:


    me "How did you get to know her?"


translate english day5_dv_3f85db61:


    "Alisa shivered a little."


translate english day5_dv_e3340992:


    dv "We grew up together."


translate english day5_dv_0b791288:


    "She answered slowly after some hesitation."


translate english day5_dv_feaeb3c3:


    me "And ended up at the same pioneer camp.{w} What a fabulous coincidence!"


translate english day5_dv_826081ef:


    "All these questions, mysteries and fears that had been so well-hidden so far suddenly spawned in my head."


translate english day5_dv_4cdcce8a:


    th "Damn, I could just throw her out of the boat and drown her right here and now."


translate english day5_dv_8a51e645:


    th "I can demand answers."


translate english day5_dv_01dabe85:


    th "Although, it could be dangerous unless I'm sure that I know what I'm doing."


translate english day5_dv_d79d8ce6:


    "But still I thought that this time I was playing with the white pieces so I could make the first move, and not just parry the blows of the opponent."


translate english day5_dv_9c1029af:


    me "So, what kind of camp is this?"


translate english day5_dv_acae4786:


    "Alisa stared at me blankly."


translate english day5_dv_38f0a76d:


    me "Where are we? Why am I here?"


translate english day5_dv_27b39f33:


    "She kept silent."


translate english day5_dv_46287afa:


    me "Answer me!"


translate english day5_dv_91d35e95:


    "I shouted."


translate english day5_dv_3f5fc648:


    dv "Hey, what's wrong with you...?{w} If you want me to apologise to Lena, I will..."


translate english day5_dv_6579c7ab:


    "That's when I realised how stupid I was acting, to say the least."


translate english day5_dv_7113a55a:


    th "She could simply be uninvolved in all... that stuff."


translate english day5_dv_7ebb2614:


    th "And what's more...{w} One can't lie that convincingly."


translate english day5_dv_481f6bb0:


    "Alisa's fearful expression was yet another reason to belive her."


translate english day5_dv_44b2f075:


    me "Sorry..."


translate english day5_dv_613ae970:


    "That was all I was able to say at the moment."


translate english day5_dv_2283e3ea:


    "Yet I wanted to keep my advantage."


translate english day5_dv_8b39987d:


    "The last few metres were really tough."


translate english day5_dv_1fa7fb2a:


    "Exhausted, I got out of the boat and lay on the cold ground."


translate english day5_dv_4942088d:


    "Alisa stood nearby, staring at me."


translate english day5_dv_395406fd:


    "I thought she was about to goad me or utter some barbed witticism, but since she remained silent I assumed she was still shocked or something."


translate english day5_dv_7de75851:


    "Even though it was rather dark here, it would be easier to hide in my old flat than on this tiny island."


translate english day5_dv_8f34fcaa:


    "Furthermore, Lena must have already heard us as we were crossing the river, so by now it's already too late to ask Alisa any more questions, even though I still had some."


translate english day5_dv_17717ee8:


    me "Listen..."


translate english day5_dv_3b8d3ec7:


    "I looked at her but saw nothing – it was a dark night and I didn't have cat's eyes."


translate english day5_dv_0940755d:


    me "Well, when we find Lena you're gonna tell her you never wanted to hurt her feelings and everything that happend in the morning was nothing but a misunderstanding, ok?"


translate english day5_dv_cb693d1d:


    "My voice didn't sounded as confident as it did a couple of minutes ago, but confident enough."


translate english day5_dv_64847fff:


    "Although Alisa said nothing it was clear by the look on her face that she agreed."


translate english day5_dv_89f2dd53:


    "I stood up and we went off to find Lena."


translate english day5_dv_9ca3b435:


    "Her boat was tied to a snag on the far side of the island."


translate english day5_dv_a5b3f875:


    me "That's very clever of her.{w} One wouldn't see the boat from afar."


translate english day5_dv_3cc8517a:


    "I mumbled."


translate english day5_dv_afb89a5f:


    me "Well, that means she's somewhere in the trees, let’s find her!"


translate english day5_dv_90bebd35:


    "I headed into the grove but Alisa stood still."


translate english day5_dv_889111c0:


    me "What's up?{w} I thought we'd discussed everything already..."


translate english day5_dv_f2f597bd:


    dv "Er...{w} It's not that...{w} It's just that...{w} it's dark... in there..."


translate english day5_dv_8d507ec8:


    "I had to strain my eyes to make out her face."


translate english day5_dv_28c1ce27:


    me "The grove is smaller than my grandmother's daisy garden. What is there to be afraid of?"


translate english day5_dv_da6c23b0:


    "Alisa didn't answer."


translate english day5_dv_f4740084:


    me "If you want to know what I'm thinking – I would gladly just leave you here. I never know what trick you're up to!"


translate english day5_dv_a1bcd7f2:


    dv "Well..."


translate english day5_dv_6239b879:


    "She came very close to me."


translate english day5_dv_0127153c:


    dv "Let's go like this..."


translate english day5_dv_9a59280f:


    "She took my hand."


translate english day5_dv_18180057:


    "I hadn't expected this outcome."


translate english day5_dv_09c023e9:


    "My cheeks turned red and hot, my breath was taken away and I could barely say anything."


translate english day5_dv_729fa06e:


    "The whole situation didn't favour conversation which was probably for the best, since I doubt I would be able to say anything smart."


translate english day5_dv_140f9a5e:


    "Slowly we headed into the grove."


translate english day5_dv_dbdafe82:


    "In a couple of moments I regained some control of myself."


translate english day5_dv_f8e9e2d5:


    me "Come on, there is really nothing to be afraid of.{w} We came here during the day, gathering some strawberries – no big deal. Just a normal birch grove..."


translate english day5_dv_a6dda4a2:


    me "It's really nice here during the day."


translate english day5_dv_29bf0ac9:


    "I added after a short pause."


translate english day5_dv_2c4d16f2:


    "Alisa didn't look at me, she was staring somewhere into the distance."


translate english day5_dv_57ad0457:


    me "Well, it's up to you after all..."


translate english day5_dv_dbfec409:


    "A few moments later she pointed somewhere in front of us."


translate english day5_dv_65ced1f6:


    dv "There she is."


translate english day5_dv_5460ca51:


    "I peered into darkness but could barely see anything."


translate english day5_dv_9e040406:


    "When we came closer we saw Lena sitting beside a tree.{w} She was crying."


translate english day5_dv_41ec3a67:


    un "Why have you come here?"


translate english day5_dv_563f0f0a:


    "She asked tearfully."


translate english day5_dv_8fc2a6d9:


    un "And you..."


translate english day5_dv_0b024121:


    "She looked at Alisa but didn't finish the sentence."


translate english day5_dv_86522e17:


    "That moment Alisa released my hand."


translate english day5_dv_498638c2:


    me "Well, you see..."


translate english day5_dv_9eb817ac:


    dv "I just wanted to apologise for being somewhat harsh and rude..."


translate english day5_dv_272bb3ad:


    "Alisa interrupted me.{w} Her voice had regained that usual arrogance of hers."


translate english day5_dv_7fc953c2:


    "Maybe it's because she didn't want to look weak in front of Lena."


translate english day5_dv_0ed5b186:


    un "What a perfect apology!{w} Not walking the walk, are you?!"


translate english day5_dv_9b03effe:


    un "How clever of you to insist that I'm doing a bad thing and then do that same thing yourself!"


translate english day5_dv_991e4d49:


    "Lena switched to shouting uncontrollably."


translate english day5_dv_d2b96176:


    dv "You got it all wrong..."


translate english day5_dv_04d782d6:


    un "I get what I've seen!"


translate english day5_dv_35da24e2:


    me "Er... You see, girls...{w} You're arguing again and I seem to be somehow involved in all that, but I don't understand a thing!"


translate english day5_dv_c501f81c:


    un "Explain it to him."


translate english day5_dv_6b44c45f:


    "Lena said to Alisa with a grin."


translate english day5_dv_921308b4:


    dv "There's nothing to explain..."


translate english day5_dv_006275ed:


    un "Well, maybe you will let me have a try then?"


translate english day5_dv_319d4fcb:


    dv "Whatever!"


translate english day5_dv_184d37fc:


    "She crossed her arms over her chest and turned away."


translate english day5_dv_a958d2ed:


    un "Well then, Semyon...{w} She blamed me for pursuing you."


translate english day5_dv_c52b3095:


    "For a minute there was complete silence."


translate english day5_dv_e8d3e7f3:


    "Lena didn't look like explaining any further, and as for me, I stood still trying to assemble the scattered pieces of the puzzle into a whole."


translate english day5_dv_149b3064:


    me "What do you mean?"


translate english day5_dv_885d4f38:


    un "Exactly what I said."


translate english day5_dv_220fc882:


    dv "It's not like that, that's not what I meant."


translate english day5_dv_f15e5159:


    "Interrupted Alisa."


translate english day5_dv_bbd6ebea:


    un "Of course...{w} You've always been like this..."


translate english day5_dv_6771fae9:


    "Lena burst into tears again."


translate english day5_dv_7a91f6b6:


    me "Wait, how come...?"


translate english day5_dv_99a3bbc7:


    th "So… Lena likes me?"


translate english day5_dv_b711211f:


    un "And you! What about you? You're trying to convince me that it's all wrong while you're holding his hand!"


translate english day5_dv_56b5ad1b:


    th "What? So Alisa likes me too?!{w} Oh, come on, Alisa actually liking someone? This can’t be happening..."


translate english day5_dv_428cd21d:


    dv "It's just it was dark in here and..."


translate english day5_dv_72b459e3:


    un "That's so much like you.{w} Always so arrogant, always trying to control others, but when it comes to yourself..."


translate english day5_dv_da6c23b0_1:


    "Alisa didn't answer."


translate english day5_dv_d66e57b9:


    "I really felt like I had to say something to defuse the situation."


translate english day5_dv_246e6c69:


    me "Hey, wait...{w} Perhaps we just got it all wrong...{w} I..."


translate english day5_dv_443723c9:


    dv "This has nothing to do with you..."


translate english day5_dv_b386505f:


    "Alisa's voice was oddly distant."


translate english day5_dv_013ab4b3:


    me "Judging by the conversation, this has everything to do with me."


translate english day5_dv_cb70eba8:


    un "Yes, it does!{w} Ask her what she thinks of you and why she told me not to go after you!"


translate english day5_dv_f7e5e4be:


    th "I don't think Lena was actually 'going after me'."


translate english day5_dv_91cb39e7:


    dv "I'm telling you once again, I..."


translate english day5_dv_19fe9001:


    un "We heard enough of your story already!"


translate english day5_dv_eae66d30:


    "Lena shot up and ran away into the darkness."


translate english day5_dv_d1bcdf75:


    "I stood nailed to the spot, not sure what to do next."


translate english day5_dv_f72d2ca7:


    th "If I followed Lena now, what would I say when I found her?"


translate english day5_dv_f0495ba4:


    th "After all, I don't even understand what this is all about."


translate english day5_dv_09af90a3:


    th "My cluelessness and silly attempts to console her would only upset her more."


translate english day5_dv_87ab0698:


    me "Guess we'd better let her be alone for some time..."


translate english day5_dv_e5989bb1:


    "That didn't sound very confident."


translate english day5_dv_448ae357:


    dv "Whatever."


translate english day5_dv_c19759e7:


    "Without another word Alisa headed back to the boat."


translate english day5_dv_a20cefa7:


    "..."


translate english day5_dv_9525b0c0:


    "The whole way across the river to the pier was hell of a trip. I had to stop every ten or twenty metres to take a break."


translate english day5_dv_7231fc65:


    "Alisa was staring at the river and didn't pay any attention to me."


translate english day5_dv_2ace92a8:


    "Well, this seems to be the first situation here in the camp that is completely beyond my comprehension."


translate english day5_dv_5248806b:


    "Even though ultimately it is just a trivial, everyday situation."


translate english day5_dv_c998baac:


    "I wasn't this confused even during the all-night expedition to the old camp."


translate english day5_dv_0fc4d70a:


    th "After all, if you have a closer look, there isn't anything strange here at all."


translate english day5_dv_74caf240:


    th "Lena is a very shy and perhaps even introverted person, yet she's human and can't completely avoid normal human developments."


translate english day5_dv_6b02e0fc:


    th "Considering that, her reaction fits the situation quite well."


translate english day5_dv_8096860d:


    "Even if I don't get the source of the quarrel, it's clear as day that the girls would not behave like that if they didn't have a good reason."


translate english day5_dv_e82ce761:


    "And as things were standing – I was that reason."


translate english day5_dv_b467b940:


    "And that's the strangest part of the whole situation."


translate english day5_dv_b1b2da01:


    me "Hey, I'm sorry it went that way.{w} If only I hadn't overheard your conversation..."


translate english day5_dv_6a0b3208:


    "Seems like I was feeling a pathological need to excuse myself."


translate english day5_dv_443f185c:


    dv "It's not your fault at all."


translate english day5_dv_560d31b2:


    "She said absent-mindedly."


translate english day5_dv_d59d8325:


    dv "It's just that you apeared in the right place at the right time and restarted a mechanism that had stood still for a long time."


translate english day5_dv_3aad82ef:


    me "I don’t quite get it..."


translate english day5_dv_e1d83f9f:


    dv "Of course you don't.{w} And actually you shouldn't right now.{w} You'll get it later."


translate english day5_dv_1b595140:


    "She cast an intense glance at me."


translate english day5_dv_fca3e3d8:


    me "Damn it!{w} Why does this always happen to me?!"


translate english day5_dv_635de124:


    "Both at school and colledge, trouble seems to have chosen me as its primary target."


translate english day5_dv_455102c5:


    "Someone blocked the classroom's door with a mop? I got a written reprimand in my mark book."


translate english day5_dv_f91c8482:


    "There was a fight? All things considered, I must’ve started it."


translate english day5_dv_9e87993c:


    "Failed an exam? Well, of course that's my fault and the teacher who hates me has nothing to do with it."


translate english day5_dv_66fe6755:


    "Even my parents always seemed to prefer blaming me rather than unfavorable circumstances, a third party's involvment, or the decrees of providence."


translate english day5_dv_6130f8de:


    "At some point I even started to believe that I somehow do attract trouble. Remember Murphy's law?{w} In my case it was like 'If something nasty is gonna happen, it's gonna happen to me'."


translate english day5_dv_c8a1ebf7:


    "For that reason I always tried to stay out of any trouble where I could become the whipping boy."


translate english day5_dv_ffc93cf9:


    "Well, judging by today, I'm not really good at it..."


translate english day5_dv_7e2b13fc:


    dv "It seems like you've got an aura of some sort – you do attract attention."


translate english day5_dv_194f815a:


    "Surprised I looked at Alisa.{w} She was smiling."


translate english day5_dv_8a97cc6a:


    th "Looks like she can read my mind..."


translate english day5_dv_df9b37de:


    me "Trouble, mostly."


translate english day5_dv_127fc146:


    dv "Who knows... Who knows..."


translate english day5_dv_3fed2b26:


    "She said dreamily and stared at the river."


translate english day5_dv_4676d969:


    dv "It's a wonderful night, isn't it?"


translate english day5_dv_a4b71fa9:


    me "Just like any other one if you ask me."


translate english day5_dv_223e723d:


    "I felt used up."


translate english day5_dv_7e23ddcf:


    dv "That's exactly what I'm talking about...{w} Just an ordinary night, nothing special. The best moment to make our most admirable camp leader really happy."


translate english day5_dv_3145d815:


    dv "She has to know that her favourite pioneer is a nasty pervert who enjoys peeking at naked girls!"


translate english day5_dv_9821dca6:


    "Something snapped inside of me again..."


translate english day5_dv_c606f827:


    me "You aren't serious, are you?"


translate english day5_dv_aa6b4182:


    dv "Why...{w} Of course I'm serious!"


translate english day5_dv_d4711875:


    "She didn't look like she was joking at all."


translate english day5_dv_6fc6b848:


    me "Well, suit yourself, but do you really think this is the best time to do that?"


translate english day5_dv_6d61f732:


    dv "Aren't you supposed to row?"


translate english day5_dv_afbb39ba:


    "I realised that our boat was still drifting in the river."


translate english day5_dv_0934364c:


    me "Wait!{w} Don't dodge my question!"


translate english day5_dv_78b06dfc:


    dv "Let's get back to the camp...{w} I'll have a moment to think it over."


translate english day5_dv_20705088:


    "Getting to spend an entire night in a boat wasn't something I would aspire to, so I had to apply myself to the paddles."


translate english day5_dv_1b0374d0:


    "We barely made it to the pier."


translate english day5_dv_ffb36c55:


    "I was exhausted. Both because of the rowing and because of Alisa's intent to tell Olga Dmitrievna about this morning's incident."


translate english day5_dv_d45a2e04:


    "If I know our camp leader, her reaction will be quite predictable."


translate english day5_dv_cc9ba692:


    me "So, you are actually going to visit Olga Dmitrieva now, are you?"


translate english day5_dv_f74976c4:


    dv "Why not?"


translate english day5_dv_3aa0974f:


    "She gave me her most roguish smile."


translate english day5_dv_6c9ff79e:


    me "It's night after all.{w} She must be sleeping already."


translate english day5_dv_34c6ac1d:


    dv "Who knows, she might well be awake..."


translate english day5_dv_e2560be6:


    me "Come on, you know full well it was a coincidence! A simple misunderstanding triggered by Ulyana!"


translate english day5_dv_43cdac3a:


    dv "No, I don't..."


translate english day5_dv_dec618ce:


    "She flung her arms up and headed to the camp."


translate english day5_dv_86a32f18:


    "I gathered all my remaining strength, jumped up and followed her."


translate english day5_dv_44ba1020:


    "At first I considered grabbing her arm but then realised it might not be such a good idea after everything that had happened today."


translate english day5_dv_079f62e5:


    me "Hold on..."


translate english day5_dv_a4b6ed32:


    "I trudged unenergetically along next to her."


translate english day5_dv_86a0035c:


    me "Let's talk this over."


translate english day5_dv_4f27c86c:


    dv "Is there anything to discuss?"


translate english day5_dv_1eeed204:


    "We silently walked alongside each other for some time."


translate english day5_dv_a7a06c6d:


    "At least she walked slowly so that I had no trouble keeping pace with her and I was thankful for it."


translate english day5_dv_5495a454:


    me "Hey, is there anything I can do to convince you to not tell Olga Dmitrievna?"


translate english day5_dv_ddf334ae:


    dv "Not sure, but, well... there is one thing..."


translate english day5_dv_b3e9120f:


    "We came to the square."


translate english day5_dv_6378d82b:


    me "So what is it?"


translate english day5_dv_21c6f737:


    dv "Well, stop chasing Lena for instance..."


translate english day5_dv_0e345e20:


    th "For God’s sake! What makes her think I'm chasing Lena?"


translate english day5_dv_434f8b3b:


    "I started to lose my temper."


translate english day5_dv_496ee797:


    me "Why are you making up such nonsense?!{w} You started that quarrel in the forest, because of you we had to go to that island and here we go again!{w} Enough is enough!"


translate english day5_dv_4c611d30:


    me "I am not chasing her!"


translate english day5_dv_922b2e90:


    "Alisa stood still, bright moonshine lighting up her face, which showed evident signs of resentment and displeasure."


translate english day5_dv_dbf16f4b:


    dv "Like I don't see how you look at her!"


translate english day5_dv_b9cdb4df:


    me "How do I look at her?"


translate english day5_dv_0ba7b0db:


    dv "Like that!"


translate english day5_dv_d2b04ecc:


    me "Like what?"


translate english day5_dv_5f616c8b:


    dv "You know!"


translate english day5_dv_df0640da:


    "She shifted her gaze but remained standing still."


translate english day5_dv_41e33f1a:


    me "Hey, stop seeing everyone through your twisted imagination! If you can't stop making up nonsense stories then at least keep them to yourself!"


translate english day5_dv_b8304cd4:


    me "Don't make others suffer because of you and your stories! I don't care if you pick on me, but now you got Lena involved!"


translate english day5_dv_a6dd47ba:


    "I really lost my temper for good, but Alisa didn't answer."


translate english day5_dv_73cc2124:


    "We sank into an unexpected silence that was only broken by Alisa's sparse sobs."


translate english day5_dv_1564df3a:


    me "From bad to worse! Now YOU are crying...{w} Have you all gone mad here?"


translate english day5_dv_7a6449b3:


    "I clasped my hands in despair."


translate english day5_dv_1a648809:


    "I could understand why Lena was crying.{w} But seeing Alisa crying is unthinkable to say the least..."


translate english day5_dv_e14b3828:


    "I can't even see her reasoning."


translate english day5_dv_f6741270:


    "At any other time I would definitely be shocked, but at this moment I'm just too tired for that."


translate english day5_dv_6615be77:


    "My mind was absolutely blank."


translate english day5_dv_99ab0d84:


    "To be precise, it was so heavy and full so that not even a single idea would have the chance to dwell there."


translate english day5_dv_e447cec9:


    "If I would compare my brain at its prime to a broad highway full of speeding thoughts overtaking each other and causing giant chaotic crashes, then now it's nothing but a forgotten tiny path in a distant desolate forest, which is only used in times of absolute necessity."


translate english day5_dv_c1d95e71:


    "Alisa kept silent. At least she stopped crying."


translate english day5_dv_3c584237:


    me "If you really think it's so serious, go ahead and tell everything to Olga Dmitrievna...{w} I hope it makes you feel better..."


translate english day5_dv_bd4f7e30:


    dv "Ok, I won't do it..."


translate english day5_dv_8e49058e:


    "She said quietly and turned to me."


translate english day5_dv_f381d5cd:


    "The tears were all gone, but her face seemed to express absolute sorrow at the world."


translate english day5_dv_3e47cc5e:


    dv "I'm just feeling hurt..."


translate english day5_dv_e6e0ffb2:


    me "What hurt you?"


translate english day5_dv_b037ba1a:


    "I asked her tiredly."


translate english day5_dv_8926ae0c:


    dv "It's always like that.{w} She gets all the attention and always pushes me aside."


translate english day5_dv_5637fae3:


    "I had difficulty understanding what she was talking about and decided to play along."


translate english day5_dv_1f586e25:


    th "Hopefully she'd finally leave me alone."


translate english day5_dv_c1141a62:


    me "That's not true.{w} You do draw attention and people don't overlook you. I don't."


translate english day5_dv_5ec2967a:


    "I raised my eyes and looked at her."


translate english day5_dv_8d8f127d:


    "Her facial expression was a mixture of amazement and expectation."


translate english day5_dv_455fafb6:


    me "See?"


translate english day5_dv_694b9cc2:


    dv "So you...{w} Could you..."


translate english day5_dv_cee25919:


    "All of a sudden, her voice became so tender that I flinched in astonishment."


translate english day5_dv_20588e11:


    "Alisa's face turned red and she hid her eyes."


translate english day5_dv_48f34051:


    dv "So...{w} You really think that I'm as good as her?"


translate english day5_dv_0250a659:


    "I wanted to say something like 'Even better!', but stopped myself."


translate english day5_dv_693741eb:


    me "Yes, I do..."


translate english day5_dv_dab6d6c3:


    "It seems she didn't understand that I said it all absolutely insincerely."


translate english day5_dv_1d46b2fc:


    dv "Alright, time to sleep!"


translate english day5_dv_a412e07f:


    "Alisa suddenly yelled cheerfully."


translate english day5_dv_ff8903e2:


    "Now she was acting more like herself again."


translate english day5_dv_b2e53022:


    dv "See you tomorrow."


translate english day5_dv_c3c87c93:


    "She waved her hand and ran away."


translate english day5_dv_99742b57:


    "I sighed in relief."


translate english day5_dv_d688f7f1:


    th "Well, it was over for today."


translate english day5_dv_334bf905:


    "All my remaining energy was wasted on a sprint to Olga Dmitrievna's cabin."


translate english day5_dv_d055c80a:


    "There was no light, so trying not to awake the camp leader I undressed quietly and lay down."


translate english day5_dv_b0e44a4b:


    th "It's still interesting, what did Alisa mean?"


translate english day5_dv_c93f9c04:


    th "And Lena...?"


translate english day5_dv_2def8e8d:


    "It's become totally unclear what is going on with them and with me – as if we got sucked into some sort of vortex, which started to swirl with furious power."


translate english day5_dv_9b5148d6:


    th "But what's going to happen next?"


translate english day5_dv_801e05ca:


    th "And where will I be thrown then?"


translate english day5_dv_a20cefa7_1:


    "..."


translate english day5_sl_608d1f0d:


    "I'm really interested in where Slavya has gone."


translate english day5_sl_3ff2c172:


    "Though I also wanted to leave this place as fast as possible and, if not to finally lie down to sleep, then at the very least to spend some time alone, away from all these pioneers and Olga Dmitrievna, who just couldn't wait to occupy me with something again."


translate english day5_sl_2c4efb4d:


    "I picked a good moment and fled into the forest."


translate english day5_sl_d3fc10d2:


    "Night had fallen onto the camp.{w} An absolutely normal and unremarkable night."


translate english day5_sl_46ada0f2:


    "One of those nights when the dark sky, stars or crescent moon don’t trigger any special emotions, when the crickets' chirping and the singing of night birds seemed more like an everyday routine than a nocturnal chorus."


translate english day5_sl_42be4cb2:


    "I wandered around the forest with no special purpose, trying not to get too far from the camp."


translate english day5_sl_89eb1213:


    "After all, there was a chance to meet Ulyana out there, and that natural disaster would probably be even worse than the camp leader."


translate english day5_sl_4d903dc8:


    "I sat down on a fallen tree and started thinking."


translate english day5_sl_905d8f87:


    "Why is this all happening to me? Why do I keep getting myself into foolish situations, every time and everywhere?"


translate english day5_sl_f58b1a89:


    th "Even having suddenly appeared in a weird pioneer camp in the middle of nowhere, I don't get to become a subject in an experiment, a victim of a sick cosmic mind, or a participant in an intergalactic war on the side of a group of suicide-prone pacifists, like a regular hero of science fiction."


translate english day5_sl_013538e9:


    th "No, I get to hide in a night forest from a raving camp leader and her workhorse pioneers..."


translate english day5_sl_99250f31:


    "The stars in the sky were shining brightly."


translate english day5_sl_d9e32f7f:


    th "Perhaps they give their light not only to me and this camp, but also shine on the city where I was born, and where my old home is..."


translate english day5_sl_1b24971b:


    "It was as if a pain settled in my chest."


translate english day5_sl_3ef36634:


    "I envisioned my old flat clearly, and a detestable burning started making its way from my stomach to my throat."


translate english day5_sl_86074a46:


    "No, it was not wistfullness.{w} More like a sad reminiscence."


translate english day5_sl_30f40a77:


    "Because despite all that's happened, I've felt more alive in less than five days here than I had for the last several years there."


translate english day5_sl_d5ded2d1:


    "Now I really wasn't sure if I wanted to get back."


translate english day5_sl_87600ecf:


    "Only one question still ate at me – how and why I ended up here.{w} It flared up yet again in my mind."


translate english day5_sl_e3fa1442:


    "I haven't spent much time seeking answers or even just thinking about my situation lately."


translate english day5_sl_cba9268c:


    "My thoughts were occupied with everyday, routine affairs."


translate english day5_sl_2e35c234:


    "And now, in order to break away and be able to wish to stay here for good, I need to understand the nature of this place."


translate english day5_sl_6534fa35:


    "It's just that even a nightingale in a golden cage has a right to know how and by whose will he got in there.{w} And after that, to make the choice whether to stay or not..."


translate english day5_sl_7e84e611:


    "Probably I would have been lost in my existential thoughts for a long time, but I heard voices approaching – Olga Dmitrievna and the pioneers."


translate english day5_sl_a025a4ba:


    th "Looks like they're proceeding with their hike..."


translate english day5_sl_b7831d31:


    "I headed to the camp at a fast pace."


translate english day5_sl_20e62a68:


    th "I have no desire to go back to the camp leader's cabin because I would surely be scolded, and just waiting for it is much worse."


translate english day5_sl_607f0110:


    "Soon enough I arrived at the beach."


translate english day5_sl_5f1f337b:


    "And I wasn't alone here – a pioneer uniform was lying on the sand.{w} A girl's one..."


translate english day5_sl_4048e221:


    "But no one seemed to be swimming in the water."


translate english day5_sl_274f5b11:


    "I almost thought it was some new devilry when suddenly a voice came from behind."


translate english day5_sl_6a030984:


    sl "Skipping out on the hike?"


translate english day5_sl_51149e68:


    me "..."


translate english day5_sl_19be3aa3:


    sl "I thought you were still in the woods."


translate english day5_sl_74e314f2:


    "I turned around and saw Slavya in a bikini."


translate english day5_sl_ec56f2f6:


    me "Sorry, I guess I interrupted you."


translate english day5_sl_c3af5591:


    sl "That's okay, I'm almost done."


translate english day5_sl_0bcdd9c2:


    me "Why did you decide to have a swim during the night?"


translate english day5_sl_bea64ff8:


    sl "Oh well, is it prohibited?"


translate english day5_sl_d1046a49:


    "She smiled."


translate english day5_sl_ff30fec3:


    me "Well, no...{w} It's just...{w} Won't Olga Dmitrievna mind you leaving early...?"


translate english day5_sl_ddc20430:


    sl "But you did the same!"


translate english day5_sl_72b67782:


    "Slavya gave me a quick glance."


translate english day5_sl_fe32f248:


    me "Yeah, I did..."


translate english day5_sl_3be00ada:


    "I sat down on the sand and stared at the river."


translate english day5_sl_9cd3b7aa:


    me "You didn't like the hike, did you?"


translate english day5_sl_d22b7e79:


    sl "No, it's not that...{w} I just wanted to be alone for some time."


translate english day5_sl_a21bded2:


    me "And I've interrupted you."


translate english day5_sl_97ab3600:


    sl "No, not really."


translate english day5_sl_4a2c94f6:


    me "That's not like you at all."


translate english day5_sl_b31d38bf:


    sl "What are you talking about?"


translate english day5_sl_dbe426b0:


    me "Well, to leave like that..."


translate english day5_sl_51aa3969:


    sl "Well, I'm not a robot that can only act according to a predefined program."


translate english day5_sl_ec2c2a7c:


    "She laughed."


translate english day5_sl_078b05d1:


    me "Yeah, that's right..."


translate english day5_sl_3ca9afdd:


    "I was still puzzled, and my fatigue was also becoming stronger and stronger."


translate english day5_sl_6615be77:


    "My mind was absolutely blank."


translate english day5_sl_99ab0d84:


    "To be precise, it was so heavy and full so that not even a single idea would have the chance to dwell there."


translate english day5_sl_e447cec9:


    "If I would compare my brain at its prime to a broad highway full of speeding thoughts overtaking each other and causing giant chaotic crashes, then now it's nothing but a forgotten tiny path in a distant desolate forest, which is only used in times of absolute necessity."


translate english day5_sl_0458ec4c:


    "So I said the first thing that crossed my mind:"


translate english day5_sl_00992d89:


    me "Don't you think it's all pretty strange here?"


translate english day5_sl_706bd1ea:


    sl "Strange?"


translate english day5_sl_15ae2aa5:


    me "Everything that happens here.{w} It's the ideal model of a pioneer camp."


translate english day5_sl_51310c74:


    me "Of course, it's not like I know a lot about them, but it looks exactly like I imagined it."


translate english day5_sl_b31d38bf_1:


    sl "What are you talking about?"


translate english day5_sl_f3118ca2:


    "Slavya glanced at me perplexedly."


translate english day5_sl_143220ed:


    me "Did you ever feel that you're not where you belong?"


translate english day5_sl_311a3a2f:


    sl "I don't know."


translate english day5_sl_f4af8a55:


    me "More precisely, not where you are meant to be at all.{w} As if thousands of kilometres away from home, or even in another galaxy."


translate english day5_sl_fb1ab657:


    sl "I don't understand you..."


translate english day5_sl_042de057:


    me "We're similar in this..."


translate english day5_sl_5653f3b3:


    "I lay down on my back and looked at the stars."


translate english day5_sl_b18f0221:


    me "And what would you think if I told you that I'm an alien from the future?"


translate english day5_sl_33d5edca:


    sl "Are you?"


translate english day5_sl_a0cddebc:


    "Asked Slavya in dead earnest."


translate english day5_sl_399fd2dd:


    me "Well, let's assume I am.{w} How should I return to my time?"


translate english day5_sl_8068cf2d:


    sl "Do you really want to?"


translate english day5_sl_5dcae275:


    th "Yeah, all the conversations with her that have to do with my situation, even the slightest hints of it, always end up at the same place."


translate english day5_sl_02fb630c:


    th "It's like she's offering me a chance to stay.{w} Almost insisting on it."


translate english day5_sl_5bb9832a:


    me "Well, let's say that I'm not sure.{w} {i}There{/i}, as we could call it, everything is like home to me...{w} Or rather more familiar."


translate english day5_sl_ba3caafa:


    me "Practically everything is known to me and I'm prepared for any situation."


translate english day5_sl_9d70ab7f:


    me "And here it’s the other way around, literally every little thing comes as a surprise. And everything's...{w} different."


translate english day5_sl_863dcd94:


    sl "Is it really that bad?"


translate english day5_sl_56eeb493:


    me "I wouldn't call it bad...{w} Unfamiliar. Unclear.{w} Sometimes it can be hard to change something. Especially for people with my personality."


translate english day5_sl_83493bf7:


    sl "But what do you really want?"


translate english day5_sl_51fffd9d:


    me "To start with, I can't answer that question until I find out exactly where 'here' is."


translate english day5_sl_aaa90d0e:


    sl "Then go and find out!"


translate english day5_sl_3b021425:


    me "If only it was that easy..."


translate english day5_sl_0b1c1ea9:


    sl "But what's so difficult about it?"


translate english day5_sl_88ed05d2:


    me "Everything's difficult!{w} I don't even know where to start...{w} And I'm constantly being distracted!"


translate english day5_sl_84f92ed7:


    sl "You are talking about it so seriously, it's almost like it's all real!"


translate english day5_sl_ec2c2a7c_1:


    "She laughed."


translate english day5_sl_183aaa3f:


    me "Who knows..."


translate english day5_sl_c95262b6:


    "A pretty long silence followed.{w} Suddenly, Slavya sneezed."


translate english day5_sl_1beeb1c2:


    me "Bless you."


translate english day5_sl_309686e8:


    sl "Thanks!"


translate english day5_sl_d0e6cc2c:


    me "It wasn't a good idea to swim at night. You could get ill. Hurry to your cabin, it's cold out here."


translate english day5_sl_c393724a:


    sl "It's nothing, I'd rather sit here with you a bit more.{w} Now, just let me dress."


translate english day5_sl_fedfb810:


    "Well, I was certainly flattered, but..."


translate english day5_sl_85c017d1:


    me "Let's go, I'll walk you."


translate english day5_sl_d70f58e2:


    "But we didn’t even walk a dozen steps before Slavya grasped my arm."


translate english day5_sl_75ba3164:


    me "What's the matter?"


translate english day5_sl_d161ce4e:


    sl "Oh, I suddenly felt dizzy..."


translate english day5_sl_473c8882:


    "I touched her forehead.{w} It was burning."


translate english day5_sl_de3adeeb:


    "I was never able to measure body temperature by touch, but in this case it was obvious."


translate english day5_sl_8323f78a:


    me "I told you!"


translate english day5_sl_1ba9f4d6:


    sl "Achoo!"


translate english day5_sl_a15cb848:


    me "Come on, come on!"


translate english day5_sl_4d435875:


    sl "No... I could infect Zhenya...{w} Listen, we'd better go to the infirmary."


translate english day5_sl_44076421:


    me "And what would you do in the infirmary at night alone? That's nonsense!"


translate english day5_sl_06c202f0:


    sl "No, it's not nonsense! If you don't want to help, I'll go by myself!"


translate english day5_sl_2cc25ffd:


    "She let go of my hand, and was ready to leave."


translate english day5_sl_a2051ad9:


    me "Don’t get upset.{w} Put this on, it's cold!"


translate english day5_sl_b04f271d:


    "I handed her my sweater, which I'd brought with me on the hike."


translate english day5_sl_16c00715:


    th "Finally found some use for it, good boy."


translate english day5_sl_309686e8_1:


    sl "Thanks!"


translate english day5_sl_6275b191:


    "She pulled the sweater on and gave me such a tender and caring look, that I just couldn't argue with her any more."


translate english day5_sl_9fbebf7b:


    me "Okay fine, the infirmary it is, as you say!"


translate english day5_sl_02e4ba3b:


    "Five minutes later we were standing by the infirmary door and Slavya was picking out the right key."


translate english day5_sl_a22a6123:


    "I still considered this idea quite foolish.{w} Nobody ever died from the common cold."


translate english day5_sl_84768b52:


    "At least not in the past century."


translate english day5_sl_097ff7d4:


    "I couldn't see any good reason to spend the night in the infirmary."


translate english day5_sl_3a4dbc67:


    "Finally Slavya opened the door and leaned on my arm."


translate english day5_sl_36203cca:


    sl "I'm still a bit dizzy."


translate english day5_sl_e5045fc4:


    "She said guiltily."


translate english day5_sl_4461e513:


    "Slavya landed on the bed, I sat on the chair nearby."


translate english day5_sl_48f0ab25:


    me "Still, listen! Alone for the whole night in the infirmary..."


translate english day5_sl_7da35453:


    th "After all, at least Zhenya could get you a glass of water, if you needed one.{w} She wouldn't catch the illness – she's young and strong."


translate english day5_sl_7f80fc24:


    sl "It's alright.{w} I don't want to bother anyone. And the nurse will come tomorrow."


translate english day5_sl_3d2e3b21:


    "I suddenly imagined myself in Slavya's place. Imagined that I had to spend the whole night here, alone..."


translate english day5_sl_460094c3:


    "And shivers ran down my spine."


translate english day5_sl_3deaf8e7:


    me "Listen, maybe I can stay with you for a while..."


translate english day5_sl_a3c61bd4:


    "I owed a lot to Slavya.{w} And in fact, I didn't even want to leave."


translate english day5_sl_afee0832:


    sl "What for?{w} Everything will be fine. Thanks for walking me over. You should go and get some sleep."


translate english day5_sl_aa120605:


    me "I still think..."


translate english day5_sl_e9c90895:


    sl "It's fine."


translate english day5_sl_353eced4:


    "For a moment I thought..."


translate english day5_sl_82680eaa:


    "Of course, nothing terrible will happen to her here, but I myself would feel more relaxed if I stayed with her."


translate english day5_sl_9fbe4547:


    me "I think..."


translate english day5_sl_2b08672e:


    sl "Oh, come on."


translate english day5_sl_0d7f306b:


    "Slavya cried out, as if offended."


translate english day5_sl_9f3f4e67:


    me "You won't throw me out, will you?"


translate english day5_sl_2e9b0de6:


    "I smiled slyly."


translate english day5_sl_a8c6f6ab:


    sl "Alright. But if you get infected, you only have yourself to blame!"


translate english day5_sl_7d705a39:


    "I was happy with my little victory."


translate english day5_sl_96286888:


    sl "Ok, then what shall we do?"


translate english day5_sl_9a9c2e41:


    "There should be some playing cards in the chest."


translate english day5_sl_14a83ddb:


    me "Durak?"


translate english day5_sl_bf89e18e:


    "In fact, I didn't know any other games except that one and poker."


translate english day5_sl_b58c757b:


    sl "Okay."


translate english day5_sl_403a4e5b:


    "We spent a lot of time playing.{w} I completely lost the track of time."


translate english day5_sl_5a3cb568:


    "We chatted, laughed a lot."


translate english day5_sl_653470e9:


    "Slavya didn't seem ill at all."


translate english day5_sl_f248ccf9:


    "Then midnight came..."


translate english day5_sl_1bd398ca:


    sl "Time to sleep."


translate english day5_sl_6ded9a67:


    me "Probably..."


translate english day5_sl_70dd0c01:


    sl "But where are you going to go?"


translate english day5_sl_8fb93608:


    "She gazed at me."


translate english day5_sl_51906baa:


    sl "Come on.{w} You shouldn't be sitting in here with me."


translate english day5_sl_b550b4b7:


    me "I can do it anyway..."


translate english day5_sl_617f93da:


    th "At the moment I could even fall asleep standing up."


translate english day5_sl_848c7c43:


    "Slavya watched me for some time, and then drew the curtain and said."


translate english day5_sl_df84d49a:


    sl "Good night!"


translate english day5_sl_8de47ec6:


    me "Night."


translate english day5_sl_b551d5e5:


    "I lowered my head on my arms and fell asleep in an instant."


translate english day5_sl_56adc4c2:


    "Despite being exhausted, I slept uneasily.{w} It was more of a slumber."


translate english day5_sl_c443185a:


    "My awareness kept leaving me and returning again."


translate english day5_sl_b0ea066f:


    "The best possible state for dreaming something psychedelic..."


translate english day5_sl_c118b5ae:


    "Bus number 410 drove past the room, it was snowing from the ceiling, and my computer's monitor suddenly rose from the middle of the table."


translate english day5_sl_196f6c57:


    "Wallpaper blended through the walls, rubbish piles rose through the floor, and my old bed materialized in the corner."


translate english day5_sl_d7272335:


    "I was in my old flat again."


translate english day5_sl_7fc90acf:


    "The scenery changed to that of my home town."


translate english day5_sl_bcd4a1a8:


    "It was like I was running through endless hordes of people, bustling in and out."


translate english day5_sl_3624e130:


    "And then darkness fell on me.{w} Primal, universal darkness."


translate english day5_sl_b5fa9f77:


    "I jumped up in terror.{w} I was sweating like a river."


translate english day5_sl_fe12675a:


    "Slavya was looking at me worriedly from the bed."


translate english day5_sl_60108fae:


    me "Just a bad dream..."


translate english day5_sl_15aba1cf:


    "I attempted to smile."


translate english day5_sl_1c6cbe1a:


    sl "You were screaming..."


translate english day5_sl_6c7c2069:


    me "Screaming what?"


translate english day5_sl_f9a31b17:


    sl "I don't know.{w} It was incomprehensible."


translate english day5_sl_8be56802:


    "The clock only displayed 0:30."


translate english day5_sl_11adbbfc:


    sl "Semyon."


translate english day5_sl_b6626098:


    me "Huh?"


translate english day5_sl_5ef3b66e:


    sl "I'm freezing..."


translate english day5_sl_564a8986:


    "Slavya was rubbing the arms of her sweater, shivering."


translate english day5_sl_a0d2cda7:


    me "Hang on, I'll find a blanket."


translate english day5_sl_2b495447:


    "I searched through one cabinet after another, swearing to myself about how the damn infirmary had nothing to cover up the patients."


translate english day5_sl_6ab83d4c:


    me "I'm sorry, there's nothing here..."


translate english day5_sl_640c2a39:


    me "Wait, I'll run and fetch something."


translate english day5_sl_97419f60:


    sl "You don't have to.{w} Would you just lie here with me? It'll be much warmer."


translate english day5_sl_c849a05b:


    "She was absolutely serious."


translate english day5_sl_68527d19:


    "Her voice sounded so piteous that for a second I was left breathless."


translate english day5_sl_22609679:


    me "Are you sure that...{w} it...{w} is okay?"


translate english day5_sl_ed91b7e2:


    sl "So it’s not?"


translate english day5_sl_07ed6d6a:


    "She asked, blushing."


translate english day5_sl_7edd72b2:


    me "No, why..."


translate english day5_sl_d3c57f8c:


    "I slowly took off my shoes and lay on the very edge of the couch, trying to occupy as little space as possible, trying to shrink and stop breathing."


translate english day5_sl_f211b57a:


    "Slavya gently embraced me and rested her head on my chest."


translate english day5_sl_c61518ac:


    sl "Yeah, that's better."


translate english day5_sl_2481134c:


    "She purred."


translate english day5_sl_6d3a10e3:


    "I was unable to say anything and just lay there calmly."


translate english day5_sl_eb090c68:


    me "Very good..."


translate english day5_sl_00a08a43:


    sl "Thank you."


translate english day5_sl_9508d41b:


    me "For what?"


translate english day5_sl_f0f90483:


    "Slavya kept her eyes closed and seemed ready to fall asleep at any second."


translate english day5_sl_bca81bd5:


    sl "For being here."


translate english day5_sl_9a94410c:


    me "It's fine..."


translate english day5_sl_d663f86e:


    th "Two minutes ago she was desperately trying to get rid of me."


translate english day5_sl_fc7dc843:


    sl "No, really."


translate english day5_sl_b320bfee:


    me "Then, you're welcome."


translate english day5_sl_7a6acecb:


    "She said nothing but I was sure that Slavya smiled."


translate english day5_sl_77f7eb26:


    me "Contact us any time, as they say!"


translate english day5_sl_c5ee54e6:


    sl "You're so kind, you care so much."


translate english day5_sl_1a49756a:


    me "Well, I try."


translate english day5_sl_a0e4fdf2:


    sl "You're a good friend."


translate english day5_sl_d1052636:


    me "Friend? Well, yeah, probably..."


translate english day5_sl_29a4e2ac:


    "Somehow this word hit me painfully."


translate english day5_sl_3b03b28d:


    "On the other hand, there was no reason to believe that I was something more to Slavya than just a friend..."


translate english day5_sl_a2344f49:


    th "What are these thoughts about anyway?"


translate english day5_sl_df7e98e4:


    me "You too..."


translate english day5_sl_954ffd8b:


    "I just couldn't bring myself to find the right words."


translate english day5_sl_7411ac41:


    th "What's up with the 'You too'?{w} It's been a pleasure dealing with you, we hope for further mutually beneficial cooperation?"


translate english day5_sl_ada6764b:


    th "Or – yeah, it’s cool being friends with you too, let's go and play in the sandbox?"


translate english day5_sl_ecd07382:


    "This was twice as hard to endure with Slavya lying right next to me – I can't run, I can't hide, and even without her looking me in the eyes, I know that she sees right through me."


translate english day5_sl_a549e991:


    sl "You're distressed by something, aren't you?"


translate english day5_sl_98db6f67:


    me "No, I’m fine."


translate english day5_sl_ef63c01d:


    sl "But I can tell that you are."


translate english day5_sl_2d91a0c5:


    me "Well, maybe..."


translate english day5_sl_5c6c0b54:


    sl "Want to talk about it?"


translate english day5_sl_99ee26fe:


    me "Well, not really... In fact, there’s nothing really to talk about."


translate english day5_sl_45f565d7:


    sl "Then you don't really know why you’re upset?"


translate english day5_sl_dab7f3be:


    me "Yeah, probably."


translate english day5_sl_e47336fd:


    sl "Then why get upset in the first place?"


translate english day5_sl_c954d69f:


    "Slavya laughed."


translate english day5_sl_3ae34e17:


    me "I guess I shouldn't."


translate english day5_sl_dbfe4f1e:


    sl "That's great!"


translate english day5_sl_6dd013b2:


    "She hugged me even harder."


translate english day5_sl_970eac9a:


    me "Not cold now?"


translate english day5_sl_ad99cfcc:


    sl "I'm fine. Just tell me if you're not comfortable."


translate english day5_sl_5c1a0ecc:


    me "No, it's okay."


translate english day5_sl_ed54fb3b:


    sl "Like that time? On the boat?"


translate english day5_sl_1339e6eb:


    "My muscles, as if remembering that rowing competition, reminded me of it with a dull ache."


translate english day5_sl_b21be332:


    me "It's totally different."


translate english day5_sl_8c47f676:


    sl "Okay, okay."


translate english day5_sl_044e54c2:


    "She said slyly and raised her head a bit."


translate english day5_sl_b78463d7:


    sl "So, everything is fine?"


translate english day5_sl_f0ccc081:


    me "Yeah, perfect."


translate english day5_sl_719f7d6e:


    "I started to worry. Slavya surely wanted something from me."


translate english day5_sl_0793c467:


    "Wanted me to do or say something."


translate english day5_sl_bd971270:


    th "But what...?"


translate english day5_sl_24ef8389:


    sl "Are you sure?"


translate english day5_sl_626d142f:


    me "Yeah...{w} Probably."


translate english day5_sl_440927ac:


    sl "Well then. Have a good night!"


translate english day5_sl_3c5ee540:


    me "You too."


translate english day5_sl_a20cefa7:


    "..."


translate english day5_sl_a7b8346c:


    "After a while, she fell asleep."


translate english day5_sl_dbce3303:


    "I checked the clock out of the corner of my eye.{w} It was only 1 A.M. and that meant that I still had to lie in this position until morning."


translate english day5_sl_0dd8c20c:


    "I wasn't really concerned about my arm going numb."


translate english day5_sl_dd2a66dd:


    "Just, being so close to Slavya, in such a dangerous proximity, I couldn't sleep at all."


translate english day5_sl_39c41ef8:


    "Various thoughts were invading my head."


translate english day5_sl_c93104db:


    th "Did she really consider this whole situtation normal?"


translate english day5_sl_a20cefa7_1:


    "..."


translate english day5_sl_9719a2d8:


    "Just when I was thinking how bad it would be if anybody found us, the door opened, the light switched on and Olga Dmitrievna rushed in."


translate english day5_sl_ede80479:


    "My heart jumped and stopped for a moment."


translate english day5_sl_96da4eb5:


    mt "Semyon..."


translate english day5_sl_9142f8f6:


    "Started the camp leader in a diabolically calm voice."


translate english day5_sl_b51939f9:


    mt "I've been searching for you everywhere, and you – you escaped from the forest ahead of time, didn't come to the cabin to sleep, and now you're seducing our best pioneer!"


translate english day5_sl_5e2bbb18:


    "Slavya instantly woke up."


translate english day5_sl_3872252b:


    "She was staring at the camp leader with a sleepy eyes for a few seconds, and then instantly understood what's going on and jumped from the bed."


translate english day5_sl_110c77e8:


    sl "Olga Dmitrievna! It's not what you think! I just fell ill and Semyon walked me here."


translate english day5_sl_764cea7c:


    sl "And then I got cold and...{w} I told him to go to to his cabin."


translate english day5_sl_a4985f09:


    mt "Oh, yeah, sure! So you're sick, are you?"


translate english day5_sl_7b8fbab7:


    sl "I am..."


translate english day5_sl_4aa379e0:


    "Answered Slavya humbly."


translate english day5_sl_dc4f5852:


    mt "Then you have to get treatment!"


translate english day5_sl_bcebe051:


    "She watched me menacingly."


translate english day5_sl_c2745a0c:


    mt "And you! Get up and follow me!"


translate english day5_sl_a7b2aa21:


    "I understood that arguing was not an option and left the infirmary, not even looking at Slavya."


translate english day5_sl_3e2db886:


    "We reached the square without saying a word."


translate english day5_sl_fb0b3f24:


    "Olga Dmitrievna viciously looked over at me and said:"


translate english day5_sl_d702b0ee:


    mt "Time to go home! I need to have a serious talk with you."


translate english day5_sl_f725c780:


    "I felt awful the whole way."


translate english day5_sl_8551a9d4:


    "The camp leader silently followed me."


translate english day5_sl_0a646630:


    "I think that once again I've got into a stupid situation which will be perceived totally wrong by everybody."


translate english day5_sl_68e9f070:


    th "Ah... nothing to discuss..."


translate english day5_sl_7dbd4345:


    "What if I were in Olga Dmitrievna's place?{w} Two teenagers cuddling in the empty infirmary... at night.{w} It's hard to come up with any apologies."


translate english day5_sl_25df23e4:


    "The worst thing is that all these things always happen specifically to me!"


translate english day5_sl_ce7660c4:


    "I always considered myself to have an analytical mind. However, I rarely got to put it into practice."


translate english day5_sl_f40fe17c:


    "We entered the cabin and the camp leader continued the interrogation."


translate english day5_sl_8237174c:


    mt "Would you care to explain yourself?"


translate english day5_sl_49ae3aab:


    "She seemed to have calmed down a bit."


translate english day5_sl_8ba11b89:


    "At least this side of her character was appealing. She was, of course, an impulsive woman, but she also cooled down quickly."


translate english day5_sl_512994b2:


    me "Slavya already told you everything..."


translate english day5_sl_43521b0d:


    mt "You expect me to believe that?"


translate english day5_sl_49fe0ea8:


    me "I can't force you to believe it, but I am not going to invent any excuses either, because it's the truth."


translate english day5_sl_5fb08a35:


    "She looked at me for some time, and then said:"


translate english day5_sl_5be2bfc3:


    mt "Tomorrow I will decide what to do with you."


translate english day5_sl_9f74c45c:


    "The camp leader turned off the light, I rolled myself in the blanket, not even bothering to undress, and turned to face the wall."


translate english day5_sl_68e31fbe:


    th "Interesting, why does she only blame me for what happened?{w} I wasn't there alone, after all."


translate english day5_sl_5e211841:


    th "Slavya is a role model pioneer of course, but..."


translate english day5_sl_bfacbe72:


    "No, I didn't intend to shift the blame to her. I just felt too ashamed for getting into an idiotic situation once again."


translate english day5_sl_d8ca9160:


    "Involuntarily, with no malicious thoughts, not expecting any trap..."


translate english day5_sl_53374338:


    "I could have lay there pitying myself for a long time, but fatigue overtook me, and I blanked out."


translate english day5_us_8f2500e8:


    "I didn't think about Ulyana that evening at all."


translate english day5_us_4754cc34:


    th "That thing with the cake...{w} Like it didn't even happen..."


translate english day5_us_d4e9d4ea:


    "Though I remembered well her resentful, disappointed face."


translate english day5_us_760ba5e1:


    "Maybe in some other situation I would never have decided to go to her, but now a fine reason to leave this place and end this stupid hike has appeared."


translate english day5_us_afda6c4c:


    "Recalling that Olga Dmitrievna told me to be prepared for new tasks, I decided not to ask for her permission, and after choosing a proper moment I disappeared in the woods."


translate english day5_us_d6e969ac:


    "Night fell on the camp."


translate english day5_us_fbf53d12:


    "I mentally thanked the camp leader for not researching the land too well, since this meant civilisation was only a few hundred metres away."


translate english day5_us_94c6d083:


    "Another long walk through the night forest was not in my plans."


translate english day5_us_57b7303c:


    "Soon I came to the square and hesitated."


translate english day5_us_6f52cdaf:


    th "Won't it look silly?"


translate english day5_us_b54027f4:


    th "What would I tell Ulyana?{w} Moreover, why would I go to see her in the first place?"


translate english day5_us_4b80dcb9:


    "My head was so heavy and full that there was no room in it for the development of any ideas."


translate english day5_us_e447cec9:


    "If I would compare my brain at its prime to a broad highway full of speeding thoughts overtaking each other and causing giant chaotic crashes, then now it's nothing but a forgotten tiny path in a distant desolate forest, which is only used in times of absolute necessity."


translate english day5_us_cd65d7a1:


    "So it's better not to think about anything and just act."


translate english day5_us_5365a619:


    th "But where should I go?{w} Maybe to her cabin?{w} And what if she's not there?"


translate english day5_us_cb5fdfc2:


    "All of a sudden, something knocked me down and I fell to the asphalt.{w} It's a good thing I had time to stretch out my arms, otherwise I would have broken my nose..."


translate english day5_us_759e5578:


    us "Gotcha!"


translate english day5_us_447bdfb5:


    "I quickly got up from the ground and saw Ulyana standing before me."


translate english day5_us_1b093c3a:


    me "What are you doing?!"


translate english day5_us_88297410:


    "I couldn't stop myself from shouting, despite my best efforts."


translate english day5_us_259f22bf:


    us "You shouldn't be stargazing!"


translate english day5_us_2f6a59ef:


    "She answered viciously."


translate english day5_us_056f2d76:


    me "What if I had smashed my nose? Or had broken my arm?"


translate english day5_us_78d01ae3:


    "I said more calmly."


translate english day5_us_7de3b85d:


    us "Well, neither of those happened."


translate english day5_us_e4e8e714:


    me "Look, why do you always mock me?"


translate english day5_us_515f902a:


    "I already regret my decision to try and ease her loneliness."


translate english day5_us_6ee354fc:


    me "Was today's punishment not enough for you? Can you even draw simple conclusions?"


translate english day5_us_91d3b1af:


    us "It's entirely your fault."


translate english day5_us_2c703153:


    "The smile instantly disappeared from Ulyana's face."


translate english day5_us_9afe61c9:


    me "What, exactly, is my fault?"


translate english day5_us_4c55e5c0:


    us "Everything!"


translate english day5_us_590ed0a7:


    me "You mean I am to blame all the time?"


translate english day5_us_551306e9:


    us "Yes!"


translate english day5_us_184d37fc:


    "She crossed her hands in front of her chest and turned away."


translate english day5_us_eb6f6f93:


    me "Great..."


translate english day5_us_ad3d6bce:


    "I suddenly wanted to get away from this place as fast as possible."


translate english day5_us_4ef07a9a:


    me "But it was because of you I came here..."


translate english day5_us_b7ef3111:


    us "What?"


translate english day5_us_2940996c:


    me "I guess you are quite bored, given that everybody is hiking, and you're alone here..."


translate english day5_us_7625dabe:


    us "I don't care!"


translate english day5_us_1272c1de:


    "She yelled cheerfully."


translate english day5_us_36434b99:


    us "Well, if things are that way, what do you want to do?"


translate english day5_us_149b3064:


    me "What do you mean?"


translate english day5_us_5db74333:


    us "Well, if you came here because of me!"


translate english day5_us_5a38be43:


    "Such a thought hadn't occured to me."


translate english day5_us_1206b412:


    th "Although, I don't really know what I came here for..."


translate english day5_us_b8ee6c9c:


    me "After such a greeting I no longer want to do anything."


translate english day5_us_457fd234:


    us "Then it's up to me to choose!"


translate english day5_us_f8556bbb:


    "Said Ulyana cheerfully and started to think."


translate english day5_us_36baf507:


    "I stared at her for some time, but then couldn't stand it:"


translate english day5_us_83506e3c:


    me "Listen, if you are again planning to..."


translate english day5_us_ba2766af:


    us "I know!{w} Let's scare the others! We'll dress up as ghosts, for example! It's going to be so fun!"


translate english day5_us_9c7c48b0:


    th "I didn't find this funny in any way."


translate english day5_us_6a2846ee:


    me "Enough of that already..."


translate english day5_us_532041e9:


    "I began tiredly, but broke off."


translate english day5_us_146e3f57:


    th "Maybe that's not such a bad idea."


translate english day5_us_49be75d5:


    "After all, by the time she thinks it over and gathers all she needs, Olga Dmitrievna and the pioneers will already be back."


translate english day5_us_b8d157c1:


    "If I came here because of Ulyana then I need to play along."


translate english day5_us_22634694:


    me "Perhaps you're right."


translate english day5_us_b7ef3111_1:


    us "What?"


translate english day5_us_2dbdc7b7:


    "She was looking at me with her eyes wide open."


translate english day5_us_1b7f7977:


    us "Are you going to agree just like that?"


translate english day5_us_4bd5340f:


    me "Well, what's so wrong about that...?{w} And furthermore, if you insist..."


translate english day5_us_29659e99:


    "She studied me attentively for several seconds and then rattled off:"


translate english day5_us_7601cc90:


    us "Excellent!{w} Then we need some bedsheets! We want to look like real ghosts, don't we?"


translate english day5_us_907b6d20:


    me "Probably..."


translate english day5_us_03325ca7:


    us "You go get them!"


translate english day5_us_3d4a0aa6:


    "Ulyana announced imperiously."


translate english day5_us_310cd775:


    me "Where do you think am I going to get them?"


translate english day5_us_a2c60a47:


    us "Take them from your cabin, obviously."


translate english day5_us_cdd74e25:


    me "It wasn’t that obvious to me..."


translate english day5_us_acbf1354:


    us "So, you don't want to?"


translate english day5_us_044b5473:


    "She instantly made a gloomy face."


translate english day5_us_69eeffd7:


    me "Okay, okay!"


translate english day5_us_48beb79c:


    "I recalled the fact that there were spare sets of bed linen in Olga Dmitrievna's wardrobe, so maybe there was nothing to worry about..."


translate english day5_us_a4951fb8:


    "We approached the camp leader's cabin and I said to Ulyana:"


translate english day5_us_153ceca2:


    me "Wait for me here."


translate english day5_us_34c369b7:


    us "Sir, yes, sir!"


translate english day5_us_30bcaace:


    "She rattled off and saluted me."


translate english day5_us_d9777aab:


    "It seemed Ulyana was entirely immersed in the game."


translate english day5_us_900dfc81:


    "I quickly found two clean white bedsheets."


translate english day5_us_03e5f1cc:


    th "It's a shame they are going to get smudged..."


translate english day5_us_71a21743:


    me "Here, take it."


translate english day5_us_5d11f8d4:


    "I handed one of the bedsheets to her."


translate english day5_us_55a5bd1b:


    us "It's too large for me."


translate english day5_us_2a2dcaaf:


    "Ulyana said after twirling it in her hands."


translate english day5_us_243ba3e8:


    th "No wonder, considering her height."


translate english day5_us_99c46342:


    me "Give it to me."


translate english day5_us_3fae3b47:


    "I folded the bedsheet in half and gave it back to her."


translate english day5_us_e2dcfcce:


    us "It's much better now.{w} Follow me!"


translate english day5_us_ef43e763:


    "She put the white cloth over her head and ran into the forest."


translate english day5_us_9e8ba2ed:


    me "Wait!"


translate english day5_us_69531d20:


    "I threw myself after her."


translate english day5_us_880b3622:


    "Just a few minutes later we were near the pioneers' campfire, hiding behind the trees."


translate english day5_us_4e62ae4d:


    "Now it was clear that the trick, harmless at first, was taking an unpleasant twist."


translate english day5_us_4f51f0fe:


    "For some reason I was sure from the start that the hike would end before Ulyana took any actions, and now we were standing ten metres away from the pioneers, dressed in bedsheets. We didn't look frightening at all, rather we looked comical."


translate english day5_us_8f4b438d:


    us "Get ready! On my command!"


translate english day5_us_451e8da8:


    me "Wait, wait.{w} Actually, I was joking when agreed to all this...{w} Think again! It won't do any good! You will get yourself put under house arrest until the end of the shift!"


translate english day5_us_16b3df32:


    me "And so will I..."


translate english day5_us_f9d44a76:


    us "No retreat, no surrender!{w} Are you ready? On the count of three!"


translate english day5_us_0f768398:


    "I started scrolling through all the possible outcomes in my head feverishly.{w} There weren't many of them."


translate english day5_us_3f1c9f2d:


    "First one. Me and Ulyana run out from behind the trees and start the pioneers laughing, resulting in me getting a considerable scolding from the camp leader, and possibly something even worse."


translate english day5_us_31114dd5:


    "And Ulyana is sentenced to the highest measure of punishment feasible under the laws of the pioneer camp."


translate english day5_us_3b066783:


    "Second one. I stay here and observe with the others how Ulyana runs around the glade in a bedsheet."


translate english day5_us_6d29669b:


    "She is sentenced to the highest measure, like in the first option, and I stay in relative safety."


translate english day5_us_30ea56d4:


    "Third one. I do every thing possible to prevent her from committing this act of moral vandalism, and no-one suffers but her self-esteem."


translate english day5_us_52605bb2:


    "It all could have gone so well, but either those thoughts took more than three seconds or Ulyana shortened the count, so I hadn't managed to pull myself together in time when she sprang out into the glade, screaming inhumanly."


translate english day5_us_d63ede28:


    "As expected, all the pioneers laughed loudly, someone even fell from the log he was sitting on and started rolling on the ground."


translate english day5_us_733c66fc:


    "I tried to save the day and yelled as loud as possible, so Ulyana would hear me, but not the others:"


translate english day5_us_f0a46cde:


    me "Fool! Stop it! Come here!"


translate english day5_us_37e33704:


    "I don't know whether it was my persuasion that worked, or Ulyana understood that her performance had failed, but she ran in my direction quickly and, without pausing, hid herself in the forest."


translate english day5_us_95318591:


    "I didn't hesitate to follow her."


translate english day5_us_d75de63e:


    "Such a finale left a tiny chance that she won’t be punished again."


translate english day5_us_a78b707a:


    "It's good that I didn't join in on that tragicomical act."


translate english day5_us_97b3825e:


    th "Now I need to find Ulyana."


translate english day5_us_56432bce:


    "It turned out to be not that hard, as she hadn't managed to get far."


translate english day5_us_db6a8922:


    "Ulyana was sitting on a tree stump...{w} crying."


translate english day5_us_70edb764:


    "I stood still, hesitating."


translate english day5_us_73c62008:


    "Of course, such an outcome was to be expected, but now I had absolutely no idea of what to do or how to comfort her."


translate english day5_us_a3275ce4:


    "And besides, I'd gone back to the camp intentionally, and I had agreed to participate in that show..."


translate english day5_us_f8f658a7:


    "But it just got worse."


translate english day5_us_0e2f3120:


    "As well as that, I was tired, tired unto death."


translate english day5_us_311d70c7:


    "At that moment I wished it would all just cease to exist.{w} I just wanted to close my eyes and appear elsewhere."


translate english day5_us_ce999c85:


    "Preferably in a quiet and peaceful place."


translate english day5_us_67119f08:


    "But the sight of the sobbing Ulyana obliged me to take action."


translate english day5_us_c414404b:


    "I walked up to her and sat down on the ground."


translate english day5_us_82d52896:


    me "Well, what were you expecting anyway...?"


translate english day5_us_262b65eb:


    "I began philosophically."


translate english day5_us_8d394e00:


    me "It was sure to end up this way."


translate english day5_us_1b26a454:


    us "You're to blame for everything! You!"


translate english day5_us_5f31ce4d:


    "Ulyana shouted in tears."


translate english day5_us_a508b663:


    me "So, what would have changed if I had sprung out with you back there?{w} We both would had been laughed at, that's all."


translate english day5_us_177d555a:


    us "You always act this way! Always!"


translate english day5_us_df7cba70:


    "Her sobbing was becoming louder and louder, and then she suddenly rushed at me and started pounding on my chest with her fists.{w} The hits were not hard."


translate english day5_us_34d398f8:


    "It was more likely an attempt to take out the despair that had gripped Ulyana than a real wish to beat me up."


translate english day5_us_d2af7ffb:


    me "Calm down already!"


translate english day5_us_a0e82e49:


    "I said firmly."


translate english day5_us_5f0944f0:


    "She stopped crying for a second and hugged me."


translate english day5_us_a8064fda:


    us "Maybe nothing would have changed, but it would have been more comfortable for me if we went together."


translate english day5_us_6fd205af:


    "I didn't know what to say, so I just patted her on the head."


translate english day5_us_2e4814b2:


    us "Can I stay like this for a bit longer?"


translate english day5_us_10409023:


    me "Yes."


translate english day5_us_d624d515:


    "At that time she didn’t seem like a dangerously explosive nuclear reactor in the form of a little girl, but just like a little sister of mine who'd messed things up."


translate english day5_us_7cb9453a:


    "I wasn't mad at her at all; on the contrary, it seemed I too was beginning to care about the failure of the ghost play-acting."


translate english day5_us_42868526:


    me "It's alright, it's alright...{w} We'll scare them properly next time!"


translate english day5_us_c463eeea:


    us "Yeah..."


translate english day5_us_a38f6822:


    "I don't know for how much longer we sat in silence."


translate english day5_us_3dcc62a7:


    "Ulyana stopped crying, and I didn't want to disturb her, as she had just calmed down."


translate english day5_us_8f418ac5:


    "But I also had absolutely no desire to stay in the forest overnight."


translate english day5_us_2bb4e019:


    me "Hey, let's get back to the camp."


translate english day5_us_4d1b85a8:


    "I shook her shoulder gently, but there was no answer."


translate english day5_us_17489859:


    "Ulyana had fallen asleep."


translate english day5_us_95efcfe1:


    me "Hey!"


translate english day5_us_17edece2:


    "I shook her harder.{w} No effect."


translate english day5_us_1161aa63:


    "At that moment I wanted to cry."


translate english day5_us_c56aa18d:


    th "Why does this always happen to me? Why do I get caught up in these foolish situations, always and everywhere?"


translate english day5_us_f58b1a89:


    th "Even having suddenly appeared in a weird pioneer camp in the middle of nowhere, I don't get to become a subject in an experiment, a victim of a sick cosmic mind, or a participant in an intergalactic war on the side of a group of suicide-prone pacifists, like a regular hero of science fiction."


translate english day5_us_e29a567a:


    th "No, instead I have to spend the night in a forest with a little girl in my arms, who is muffled in a bedsheet."


translate english day5_us_5c588563:


    th "Next time, I'd rather have the monstrous experiments..."


translate english day5_us_4cc5c895:


    "I stood up and put the sleeping Ulyana on my back."


translate english day5_us_8e5b9b1b:


    "Maybe there was a way to wake her up, but firstly, I didn't want to, and secondly, one more burden or one less – at that point it made no difference."


translate english day5_us_3521c810:


    "It's good that she's not heavy..."


translate english day5_us_c048facc:


    "At the square I stopped, put Ulyana on a bench and threw myself down beside her, totally exhausted."


translate english day5_us_66f6667a:


    "Even a little girl is hard to carry for too long."


translate english day5_us_99250f31:


    "The stars in the sky were shining brightly."


translate english day5_us_d9e32f7f:


    th "Perhaps they give their light not only to me and this camp, but also shine on the city where I was born, and where my old home is..."


translate english day5_us_1b24971b:


    "It was as if a pain settled in my chest."


translate english day5_us_3ef36634:


    "I envisioned my old flat clearly, and a detestable burning started to make its way from my stomach to my throat."


translate english day5_us_86074a46:


    "No, it was not wistfullness.{w} More like a sad reminiscence."


translate english day5_us_30f40a77:


    "Because despite all that's happened, I've felt more alive in less than five days here than I had for the last several years there."


translate english day5_us_d5ded2d1:


    "And now I really wasn't sure if I wanted to get back."


translate english day5_us_f0e63108:


    "Only one question still ate at me – how and why I ended up here.{w} It flared up yet again in my mind."


translate english day5_us_e3fa1442:


    "I haven't spent much time seeking answers or even just thinking about my situation lately."


translate english day5_us_cba9268c:


    "My thoughts were occupied with everyday, routine affairs."


translate english day5_us_2e35c234:


    "And now, in order to break away and be able to wish to stay here for good, I need to understand the nature of this place."


translate english day5_us_6534fa35:


    "It's just that even a nightingale in a golden cage has a right to know how and by whose will he got in there.{w} And after that, to make the choice whether to stay or not..."


translate english day5_us_849ef2d5:


    "I don't know how much longer I would have been devoted to existential thoughts, but Ulyana's loud snoring brought me back to reality."


translate english day5_us_d9d3457b:


    me "So small, but she snores so loudly..."


translate english day5_us_c4195e16:


    "I sighed, put Ulyana on my back again and headed to her cabin."


translate english day5_us_a20cefa7:


    "..."


translate english day5_us_12b2a39f:


    "I had no desire to explain everything to Alisa, so I just put the sleeping Ulyana on the porch, knocked at the door and left quickly."


translate english day5_us_cf46738c:


    "I approached Olga Dmitrievna's cabin with mixed feelings."


translate english day5_us_1531a117:


    "On the one hand, I had done what I intended to do – I had comforted and entertained Ulyana."


translate english day5_us_6aec4a07:


    "On the other hand..."


translate english day5_us_d05290b8:


    "The two crumpled bedsheets in my hands looked more like rags left over from some worn-out straitjacket."


translate english day5_us_bb449b79:


    "I opened the door softly and went in."


translate english day5_us_105fc865:


    mt "Semyon!"


translate english day5_us_b3e320ef:


    "Olga Dmitrievna was sitting at the desk, and it seemed she had been waiting for me."


translate english day5_us_79427e67:


    mt "Do you want to explain something to me?"


translate english day5_us_377d0453:


    me "Well...{w} Just don't scold her again...{w} It's my fault!"


translate english day5_us_47603f81:


    "Just like that I became a hero, to my own surprise."


translate english day5_us_3ed3fd1f:


    "My tongue acted faster than my thoughts."


translate english day5_us_8c77f545:


    "Perhaps some of my best traits, which even I wasn’t aware of, have shown themselves."


translate english day5_us_16f0739b:


    "Humanity as opposed to common sense."


translate english day5_us_4354ab86:


    mt "Really?"


translate english day5_us_cce98559:


    "Having started this, I chose to stick to my decision."


translate english day5_us_0b7ff73e:


    me "Well, it was me who got the bedsheets. And I was standing behind a tree back then."


translate english day5_us_cc347b04:


    mt "Behind what tree?"


translate english day5_us_0193dcd6:


    "The camp leader looked at me in astonishment."


translate english day5_us_ee5de7cc:


    mt "So, what did you need bedsheets for?"


translate english day5_us_220e2848:


    "I realised my mistake."


translate english day5_us_bc2d73a5:


    me "So, you are not talking about the forest thing?"


translate english day5_us_5afe237b:


    mt "Semyon, I don't understand you."


translate english day5_us_13ed3d89:


    mt "I wanted to know where you disappeared to so mysteriously during the hike, but now I would be interested in listening to your bedsheet story."


translate english day5_us_87f8ffa1:


    th "But it was impossible that she and all the others hadn't seen Ulyana's performance!"


translate english day5_us_e65fa514:


    th "The pioneers laughing out loud couldn't have been only my imagination."


translate english day5_us_f41a46e2:


    me "Olga Dmitrievna, I'm serious...{w} Didn't you see someone in a bedsheet spring out at you recently?"


translate english day5_us_de5231d6:


    mt "So, it was you?"


translate english day5_us_8fb93608:


    "She gave me a close look."


translate english day5_us_c20bb56d:


    me "No, it wasn't me..."


translate english day5_us_85c3d025:


    th "Couldn't she tell just from my height?"


translate english day5_us_32446a1f:


    mt "But you're holding the bedsheets..."


translate english day5_us_a73acf7d:


    me "Yes..."


translate english day5_us_d874bb71:


    "I couldn't understand if she was playing me for a fool, or she really didn't know what this was all about."


translate english day5_us_3a23059a:


    me "Olga Dmitrievna, let's act like this conversation never happened.{w} I'm too tired for today."


translate english day5_us_d034fa83:


    mt "Alright, go to bed."


translate english day5_us_24edbca1:


    "To my surprise, she readily agreed."


translate english day5_us_db925e9e:


    "Of course, I was astonished by such a reaction, but I decided to use the moment. I wrapped the blanket around myself and turned to face the wall."


translate english day5_us_4c4a4071:


    "But I couldn't sleep."


translate english day5_us_8d27541d:


    "There were no thoughts, my head was aching, but I still couldn’t manage to fall asleep."


translate english day5_us_9db6bada:


    "I rolled to the other side, and images of the day started to flash before my eyes."


translate english day5_us_e7fb5b3f:


    "I shut my eyes tight to drive them away, but it didn’t work."


translate english day5_us_e227a4a1:


    "Suddenly, I heard a knocking at the window."


translate english day5_us_f9ca50cb:


    "Olga Dmitrievna seemed to be asleep."


translate english day5_us_ce13ac06:


    "I dressed myself and walked outside."


translate english day5_us_a35885f7:


    "Ulyana was standing before me with a tricky smile."


translate english day5_us_92e87dbb:


    us "How did you manage to carry me to the cabin...?{w} Wasn't it hard?"


translate english day5_us_f0de0297:


    me "Not really...{w} What have you come for?"


translate english day5_us_047a7a48:


    "I couldn't sleep, but at that moment bed seemed to be the only place where I could reside with no suffering, which is why Ulyana's sudden visit didn't please me at all."


translate english day5_us_c198f325:


    us "How was she? Did you get scolded?"


translate english day5_us_be6d9222:


    me "No, I avoided it somehow..."


translate english day5_us_4275b6c0:


    us "That's great then! I always get lucky!"


translate english day5_us_d0119426:


    me "That's for sure..."


translate english day5_us_645ce19d:


    "My eyes began closing despite my will.{w} It seems that slumber has finally come upon me."


translate english day5_us_cdcc908e:


    me "Look, I'm very tired..."


translate english day5_us_0f78845d:


    us "This won't take long.{w} Close your eyes."


translate english day5_us_e0f3d01c:


    "It was the easiest thing to do, and I didn't even want to know what she needed it for – maybe that way she would leave me alone sooner."


translate english day5_us_5cbb3956:


    "I closed my eyes."


translate english day5_us_2bd7350e:


    "And in that moment I felt a brief kiss."


translate english day5_us_5d1fc15b:


    "My eyes opened by themselves, but Ulyana was already running away, waving her hand to me."


translate english day5_us_1d1a05f7:


    "I stood still, benumbed, and I couldn't even manage to shout anything after her."


translate english day5_us_74510c40:


    "I don't know for how long I stood like that, but eventually the night chill cleared my stupor."


translate english day5_us_78b47b21:


    "I shivered and went back to bed."


translate english day5_us_eff30d1d:


    "This time I didn't want to sleep, I wanted to think it all through, but my eyes, which had begun to close as I was standing before Ulyana, seemed to issue a command to the rest of the body and I fell asleep before I realised what happened..."


translate english day5_un_b3a56e2b:


    "Only Lena and Alisa stood out in all this splendour."


translate english day5_un_9494f7ea:


    "Well, of course it was quite natural for Alisa to be arguing with someone like that."


translate english day5_un_49a0c8bc:


    "But hearing Lena talk in a raised voice..."


translate english day5_un_9844ee70:


    "I came closer silently, trying to find out what was going on."


translate english day5_un_6ca87438:


    dv "No, you listen to me!"


translate english day5_un_8c9cf7c5:


    un "I'm not going to listen to anything!"


translate english day5_un_8eaadb05:


    dv "Really? Great then!"


translate english day5_un_4e3a9f81:


    "Alisa turned away and her eyes met mine."


translate english day5_un_0961f894:


    "At first she obviously was confused about what was going on, but then..."


translate english day5_un_7f5542bc:


    dv "So, you're eavesdropping!"


translate english day5_un_12887db1:


    me "What? Me? No!"


translate english day5_un_712bd43f:


    "She began slowly advancing on me, and I took a few steps back."


translate english day5_un_ee80f2e5:


    "Lena, however, remained standing where she was."


translate english day5_un_607f19ad:


    dv "Well, okay then, listen!"


translate english day5_un_c992f6a8:


    "Alisa stopped halfway and turned back to Lena."


translate english day5_un_6aa66cea:


    dv "By the way, did you know he was peeping on me today?"


translate english day5_un_3875ba7b:


    un "What?"


translate english day5_un_91748180:


    dv "Peeping. And he saw {i}everything{/i}!"


translate english day5_un_f328f2ed:


    un "Is that... is that true?"


translate english day5_un_52e59e20:


    "Lena wrung her hands distressedly, gave me a quick glance and started staring at the ground, as if she was cut off from the world."


translate english day5_un_127f1428:


    me "No, no, nothing like that happened!"


translate english day5_un_806ed8e6:


    "Having thrown an angry glance at Alisa, I came up to Lena."


translate english day5_un_32dec18e:


    dv "Why are you denying it? I even have a witness!"


translate english day5_un_246d6902:


    me "Your witness is even worse than you!"


translate english day5_un_05eee1a3:


    dv "So, you'll stick to your claim that it didn't happen?"


translate english day5_un_5bf1c5fc:


    "I took a second to think."


translate english day5_un_756d591a:


    th "But really, why do I have such a pathological desire to exonerate myself before Lena?{w} As though I vitally need it, need her opinion of me to remain unchanged."


translate english day5_un_9cc4691d:


    th "Though, how would I know what she's thinking?"


translate english day5_un_cd4b1ccb:


    me "It didn't happen!"


translate english day5_un_b7eb3fd9:


    "Lena threw an baleful look at Alisa and raised her eyes to me right after that, eyes full of discomfort and hope."


translate english day5_un_71c04b4b:


    "It suddenly became unbearable to stare into these eyes."


translate english day5_un_0d6ab95e:


    me "It didn't happen..."


translate english day5_un_cacc8d60:


    "I repeated less confidently."


translate english day5_un_ba1e125e:


    dv "Well, whatever."


translate english day5_un_e36048c5:


    "Alisa sneered from behind me."


translate english day5_un_c6c151b9:


    dv "It's up you to decide who to trust."


translate english day5_un_97aaae01:


    "She said to Lena offhandedly, then turned away and headed to the bonfire."


translate english day5_un_50793df0:


    un "Is that true?"


translate english day5_un_56ad8efd:


    "I felt uncomfortable, like a fish out of water, feeling the urge to end this as fast as possible."


translate english day5_un_ba176c8b:


    th "And why should I have to excuse myself to her?"


translate english day5_un_5156bbe7:


    me "Well, so what if it did happen?"


translate english day5_un_5dc693b2:


    "I looked at Lena boldly, but noticed only a bright sunset gleam reflected in a tear which was going down her cheek."


translate english day5_un_e6d47aad:


    me "No, I meant..."


translate english day5_un_f203e664:


    un "You don't need to lie."


translate english day5_un_0c5b6d1e:


    "She wiped off her tears and tried to smile."


translate english day5_un_2060fc9e:


    un "After all, it's none of my business."


translate english day5_un_f32b3a36:


    me "No, why isn't it..."


translate english day5_un_f3c734aa:


    th "But why is it?"


translate english day5_un_35b0d3f6:


    me "It's just that I... Well... It was an accident, get it?{w} An accident which was caused by Ulyana, nothing more."


translate english day5_un_6e34d368:


    un "Yeah, sure..."


translate english day5_un_85bc2437:


    me "It's true!"


translate english day5_un_e71ea824:


    un "I believe you."


translate english day5_un_89436b45:


    "Lena was saying everything with absolutely no emotion, like she didn't care at all about what was going on."


translate english day5_un_76748f71:


    "I was almost able to believe it, but her tears a few seconds ago..."


translate english day5_un_14dd805c:


    me "It seems to me that you don't."


translate english day5_un_5c9d8120:


    un "What is this, some kind of a game?"


translate english day5_un_29ff2cb1:


    "Lena said quietly, but there was a hint of anger in her tone."


translate english day5_un_0a753ae6:


    un "You really need to convince me of that? And even if I believed you, what then? Would it all become true?"


translate english day5_un_ce5015a9:


    me "I'm not trying to convince you of anything, I just want you to understand that I'm not guilty... and didn't want to..."


translate english day5_un_ffacfd31:


    un "Why should I care at all?!"


translate english day5_un_f78c17ba:


    "Lena shouted."


translate english day5_un_a7f39912:


    "I was standing with my back to the bonfire, but I was sure that all the pioneers had turned in our direction."


translate english day5_un_fd952043:


    un "If you want to peep at someone – do it! Do whatever you want! But why do you bother me?"


translate english day5_un_f8882e89:


    me "I'm not..."


translate english day5_un_427acce6:


    "But really, from any point of view, it looked that way – I'm trying to excuse myself to Lena with no success.{w} Like a guilty child, or maybe even like a husband who'd slipped up..."


translate english day5_un_b24ff123:


    un "Just so you know! I don't care!"


translate english day5_un_5cd48f53:


    "Lena turned away and headed back to the camp at a fast walk."


translate english day5_un_6f3fa70b:


    "I didn't try to stop her – now it certainly wasn't the best solution."


translate english day5_un_5307c03e:


    "She's on the edge, no attempts to convince her or appeal to reason would help."


translate english day5_un_c03bbe86:


    th "Since when is she like that?{w} Screaming that way, getting mad...?"


translate english day5_un_37a8623c:


    "With these thoughts in mind, I returned to the bonfire and sat down on a wooden log."


translate english day5_un_ce617998:


    "..."


translate english day5_un_dbeba143:


    dv "So?"


translate english day5_un_a07446cb:


    "Alisa asked after seating herself nearby."


translate english day5_un_ad02f53a:


    me "What?"


translate english day5_un_1a47cd1e:


    dv "Not a great success, huh?"


translate english day5_un_2abbfc44:


    me "As you see..."


translate english day5_un_3792f79f:


    "I growled out while picking at the coals with a stick."


translate english day5_un_e73a96b8:


    "It grew completely dark and the glade was plunged in to darkness."


translate english day5_un_87200809:


    "The surrounding landscape was reminiscent of a picture from a child's book about a wood monster – an unknown forest creature is just about to spring out from behind the tree, predatory owls hoot menacingly in tree branches, even mice looking out from snags stare at you with distrust."


translate english day5_un_3c849f9b:


    "I was not afraid at that moment. Everything was different from the previous day when I was in the dungeon at the old camp..."


translate english day5_un_d74fcdd1:


    "The moon was shining brightly, paying close attention to our brave troop. The world was sleeping, waiting for the morning, bathing in the moonlight."


translate english day5_un_e9c4c23a:


    dv "As I thought. Though it was expected."


translate english day5_un_fda262d8:


    me "But why...?"


translate english day5_un_aef6d994:


    "I finally managed to pick up the largest burning log and throw it into the centre of the fire causing the flames to rise about a metre high and a vortex of sparks to spread out in all directions."


translate english day5_un_77bf3519:


    me "Does Lena often behave like that?"


translate english day5_un_185b84dd:


    dv "How?"


translate english day5_un_56fc9a67:


    me "Screaming at others.{w} I couldn't believe she would act in such a way."


translate english day5_un_989f2bee:


    dv "She is human, like anyone else."


translate english day5_un_93c166ce:


    "Alisa giggled."


translate english day5_un_32fca04b:


    dv "As if you hadn't noticed this before."


translate english day5_un_0f99a0a2:


    me "What hadn't I noticed?"


translate english day5_un_5e9fa3e4:


    dv "Her."


translate english day5_un_e54b39e9:


    me "What do you mean?"


translate english day5_un_b08ce089:


    dv "I mean this is her true face."


translate english day5_un_ec069979:


    me "What do you mean ‘true face’."


translate english day5_un_2e4631df:


    dv "Oh, you’re so stupid! "


translate english day5_un_34dc294a:


    me "Finish what you started."


translate english day5_un_44462a0c:


    dv "She's not what she seems like, not like how you thought of her before."


translate english day5_un_95364e9c:


    me "So how is she?"


translate english day5_un_6fbef0db:


    dv "Okay, I'm sick of this..."


translate english day5_un_3f845aea:


    "Alisa stood up, intending to leave."


translate english day5_un_bd114f34:


    "I didn't say anything, just sat and listened to the silence of the forest."


translate english day5_un_1c95b08d:


    dv "By the way, if you want to follow her, I think she's on the island."


translate english day5_un_278fc1b6:


    me "On what island?"


translate english day5_un_5f02cbe1:


    dv "The one where you were gathering strawberries today."


translate english day5_un_b91fd3d7:


    me "And what is she doing there?"


translate english day5_un_d7920d68:


    dv "Ugh, you really are an idiot!"


translate english day5_un_42e0fed6:


    "Alisa stomped the ground, circled the campfire and sat opposite."


translate english day5_un_ff34e775:


    "I must have really lost track of things."


translate english day5_un_6615be77:


    "My head was absolutely blank."


translate english day5_un_ff8730b2:


    "To be precise, it was so heavy and full so that not even a single idea would have the chance to dwell there."


translate english day5_un_e447cec9:


    "If I would compare my brain at its prime to a broad highway full of speeding thoughts overtaking each other and causing giant chaotic crashes, then now it's nothing but a forgotten tiny path in a distant desolate forest, which is only used in times of absolute necessity."


translate english day5_un_643db9b6:


    "I tried to think over what had happened, analyze it somehow, but it seemed I was bashing against an invisible wall."


translate english day5_un_0877776f:


    "Moreover, it was impossible for me to find a single emotion in my soul, any response. As if it had all happened to someone else."


translate english day5_un_8d9c0b6a:


    "I failed to convince Lena that I'm not guilty.{w} Should I care?"


translate english day5_un_dc1bef5c:


    "Lena cried, screamed, and then left.{w}So what?"


translate english day5_un_0845afe0:


    "I don't feel anything about it.{w} Should I?"


translate english day5_un_7c882346:


    "I bit my lip until it bled and stood up."


translate english day5_un_bbae1908:


    "I couldn't bear to stay here among these laughing pioneers."


translate english day5_un_5ba34865:


    th "I should run away. It doesn't matter where to. To the forest, to the mine, even to space. I don't care. Just so long as I can get far away from here."


translate english day5_un_fbc3bc79:


    "I seized a moment when the camp leader was looking in the other direction and vanished into the forest shadows."


translate english day5_un_a20cefa7:


    "..."


translate english day5_un_6219e64c:


    "There had been no rain for a long time."


translate english day5_un_c6749284:


    th "However, I'm not sure whether it rains here at all."


translate english day5_un_5d4ba34b:


    th "What a stupid idea, of course it does. The plants need some moisture."


translate english day5_un_cb1b2ded:


    "The night was fresher than the afternoon, but the air still hadn't had enough time to cool. I felt a little dizzy from the evening stuffiness."


translate english day5_un_40724f26:


    "Suddenly, I realised that I really wanted to swim."


translate english day5_un_489cb8db:


    "It's quite a normal desire because the days are hot and you constantly feel soaked with sweat during the daytime."


translate english day5_un_1056c0d4:


    "I didn't even notice that I'd been approaching the pier."


translate english day5_un_d177ba32:


    th "Why not the beach?"


translate english day5_un_9bb6a06d:


    "Thoughts of Lena came over me again. Everything that I tried to ignore came out and brought lots of inconvenient questions, inappropriate answers, and vague desires."


translate english day5_un_afb26968:


    "It was obvious that I had been led to the pier by my subconscious."


translate english day5_un_17e84712:


    th "But what do I {i}really{/i} want?"


translate english day5_un_4f586330:


    th "To apologise to her for something, to convince her of something?{w} No, unlikely."


translate english day5_un_eb0d22d4:


    "I just wanted to get some sort of reaction from her. Wanted her to say 'Yes, I understand everything' and just smile."


translate english day5_un_304c60e6:


    "And then I could stop myself from feeling like {i}that{/i}…"


translate english day5_un_a78ee8f1:


    "It was necessary for me to be understood by someone, even in {i}this{/i} world."


translate english day5_un_21fa3a03:


    "I untied a boat, pushed it into water, got in and took up the paddles."


translate english day5_un_ce617998_1:


    "..."


translate english day5_un_1607c44f:


    "It was easier to row this time – my hands were still hurting, but I managed to develop some sort of technique that allowed me to travel quite directly."


translate english day5_un_134ecd94:


    "The river seemed frozen, spreading under the boat like a translucent veil."


translate english day5_un_cdd6fc1f:


    "The moonlight was piercing deep into the water, so it was almost possible to see the bottom."


translate english day5_un_1b592616:


    "I started rowing."


translate english day5_un_ce617998_2:


    "..."


translate english day5_un_9f7443d1:


    "Surprisingly, the island now seemed the same as it did in the daytime."


translate english day5_un_f47c3547:


    "{i}Here{/i} usually everything is different. After dusk it felt like another world, a mystery world. Scary at times, but beautiful in its own way. A world of shadows and whispers, a nocturnal world."


translate english day5_un_b94320d8:


    "I slowly walked around the island, looking for the boat which Lena took to come here"


translate english day5_un_35778f50:


    "The grass softly rustled under my feet, the occasional waves peacefully struck the shore and bounced back like flies too tired from beating on the glass."


translate english day5_un_575aa0ca:


    "The breeze from the water lazily stirred the leaves of the trees, out of habit rather than a real desire to make the night grove sing."


translate english day5_un_0bba232e:


    "I looked at this wonderful picture with such delight that I didn't even notice something until I stumbled into it."


translate english day5_un_44e820c3:


    "It was a boat."


translate english day5_un_0c66a774:


    th "Well of course – she wouldn’t get here by swimming."


translate english day5_un_c486b958:


    "I headed towards the centre of the island."


translate english day5_un_ce617998_3:


    "..."


translate english day5_un_b2eec26c:


    "After a hundred metres I heard a rustling from behind a tree."


translate english day5_un_4dcf727f:


    un "Don't come any closer..."


translate english day5_un_5f75988c:


    "Lena whispered."


translate english day5_un_51149e68:


    me "..."


translate english day5_un_70edb764:


    "I hesitated."


translate english day5_un_0ca1cc9c:


    un "Don't come any closer."


translate english day5_un_78558274:


    "She said louder."


translate english day5_un_3efb73c2:


    me "How did you know it's me?"


translate english day5_un_edfa793d:


    "Though she hadn’t indicated that she'd known that."


translate english day5_un_65bf82fc:


    me "Alright..."


translate english day5_un_f4fd5ba0:


    "I leaned against the tree, trying not to look behind it."


translate english day5_un_ad697c77:


    me "Alisa told me that you would be here."


translate english day5_un_ca19d4ba:


    un "So what?"


translate english day5_un_801934ec:


    "It was difficult for me to know what emotions Lena was experiencing."


translate english day5_un_d382dd22:


    "Her voice was steady enough; even though I could read irritation and annoyance in it, I could not understand whether she was angry, or whether it was all the same to her."


translate english day5_un_13bf83a1:


    me "That came out awkward."


translate english day5_un_7a2f384f:


    "I was doing my best to avoid unnecessary apologies and excuses, but couldn't find other words to use instead."


translate english day5_un_e5bf1202:


    un "Was that the only reason you came?"


translate english day5_un_e7f8d1d4:


    me "No... Well, I don't know."


translate english day5_un_3c94e6e3:


    un "You don't know, but you still came?"


translate english day5_un_fc30fd2b:


    me "Yes."


translate english day5_un_334f3921:


    un "You shouldn't have."


translate english day5_un_839ef62e:


    me "Why? Of course, if I'm bothering you..."


translate english day5_un_c9d68cca:


    un "Why are you following me?"


translate english day5_un_48199bb7:


    me "I..."


translate english day5_un_4a3d8c67:


    "She was right – it certainly looked that way. Moreover, I definitely felt attracted to her."


translate english day5_un_84f2a0fc:


    me "You shouldn't think like this."


translate english day5_un_1adc8b36:


    un "How should I think?"


translate english day5_un_595d7b8c:


    me "I don't know."


translate english day5_un_8d18dd8b:


    un "I can only draw conclusions based on your behavior."


translate english day5_un_9044e6ea:


    me "I really don't know... I guess I'd better go."


translate english day5_un_8477995f:


    "I was confused, and it was hard just to be near her."


translate english day5_un_984ea120:


    un "Why? Since you came..."


translate english day5_un_44251c8f:


    "I seemed to hear a playful tone in her voice."


translate english day5_un_01822020:


    me "Alright."


translate english day5_un_77825d23:


    un "And?"


translate english day5_un_ad02f53a_1:


    me "What?"


translate english day5_un_e9197f7e:


    un "What did Alisa tell you?"


translate english day5_un_668b75c5:


    me "Nothing special."


translate english day5_un_8bf5923b:


    un "I see."


translate english day5_un_fc30fd2b_1:


    me "Yeah."


translate english day5_un_4924952d:


    un "Well then."


translate english day5_un_751ac511:


    me "Yep…"


translate english day5_un_40151fe6:


    "We just stayed silent for a while."


translate english day5_un_b8d98c47:


    un "Since you came, tell me about something."


translate english day5_un_e3b565dd:


    me "Well, I don't know..."


translate english day5_un_8ee362ac:


    un "For example, about what happened this morning."


translate english day5_un_2ea0a340:


    me "What about what happened this morning?"


translate english day5_un_5b543368:


    un "You know."


translate english day5_un_142ff09f:


    me "You yourself didn't want to talk about it."


translate english day5_un_7af3f67c:


    un "I didn't want to, but now I want to!"


translate english day5_un_ea56876b:


    "Lena's voice trembled."


translate english day5_un_2fc43074:


    "I couldn't understanding what was happening at all any more."


translate english day5_un_968df1e5:


    th "Maybe it's not her behind the tree?"


translate english day5_un_a8a3ea5d:


    un "Don't look!"


translate english day5_un_7c18f5e8:


    me "Okay, okay... But what's wrong?"


translate english day5_un_d893718b:


    un "Nothing. Just don't look."


translate english day5_un_67742710:


    me "Alright, as you wish."


translate english day5_un_c928b72a:


    un "So, nothing happened?"


translate english day5_un_a336db31:


    me "Well, something did... But that was all just a silly accident!"


translate english day5_un_4f1a98c1:


    un "Then why are you so worried about what I think about it?"


translate english day5_un_7cd08f32:


    me "I'm not worried!"


translate english day5_un_0594d317:


    me "And anyway, you yourself!"


translate english day5_un_505ae84f:


    "I said louder, beginning to feel irritated."


translate english day5_un_acb49e13:


    un "What about me?"


translate english day5_un_f2ac40dd:


    me "Why are you so concerned about this situation?"


translate english day5_un_3537de2e:


    un "Who said that I'm concerned about this situation?"


translate english day5_un_2c8023fc:


    me "Then what?"


translate english day5_un_62592cb0:


    un "Nothing..."


translate english day5_un_3d26df77:


    "She said in a whisper and fell silent."


translate english day5_un_e6ab9030:


    "Such talk would lead us to nowhere."


translate english day5_un_1826deee:


    th "Lena won't say anything, and I seem to be too stupid to guess, or even just to be sure about myself."


translate english day5_un_ae93dffd:


    me "Sorry, I guess it’s all my fault. I just cannot understand..."


translate english day5_un_962ad609:


    un "Why..."


translate english day5_un_ad02f53a_2:


    me "What?"


translate english day5_un_68ca672f:


    un "Why do you keep apologising? For everything. For what you did, for what you are doing, even for what you haven't done yet."


translate english day5_un_a337cbd5:


    me "But..."


translate english day5_un_0f57e6b5:


    "Maybe she was right, but I couldn't behave differently. I felt the need to apologise to her, to everyone."


translate english day5_un_336e6bce:


    "So they would not think badly of me, wouldn’t laugh at me. So I can put away something unsaid and eliminate misunderstandings."


translate english day5_un_7259241d:


    un "Anyway, that's your business!"


translate english day5_un_339b95c7:


    "Lena got angry."


translate english day5_un_ca9b074e:


    un "Apologise, make excuses! Why should I care...?"


translate english day5_un_607b607b:


    me "Okay, well, I'm not guilty! In fact, I don't think I could be blamed for anything, but then what's wrong? What is wrong with you?"


translate english day5_un_1e693cd1:


    un "Why would something be wrong with me?"


translate english day5_un_6478a1d7:


    me "I don’t know... I just think so."


translate english day5_un_6355a504:


    un "Oh! You just think so!"


translate english day5_un_0467a4c4:


    "She laughed."


translate english day5_un_27b88dd3:


    un "You speak as if you know me."


translate english day5_un_51149e68_1:


    me "..."


translate english day5_un_cbc4bb84:


    "I did not answer."


translate english day5_un_af01240b:


    un "Please leave..."


translate english day5_un_850cca06:


    "Lena said after a while."


translate english day5_un_a09db615:


    "I didn’t move, just could not, had no strength to do so."


translate english day5_un_d2f6ef68:


    "I wanted nothing at that point – didn’t want to excuse myself, to apologise. I didn't even want her understanding."


translate english day5_un_8ef1b6f9:


    "I was too tired of everything."


translate english day5_un_5ac38cf1:


    me "Why?"


translate english day5_un_f174e90e:


    un "Just leave..."


translate english day5_un_e3a26dcc:


    "She whispered."


translate english day5_un_1457a622:


    me "I don't want to."


translate english day5_un_380bd525:


    un "Then I'll leave."


translate english day5_un_bf0dc08a:


    me "Let's just go together."


translate english day5_un_049b021f:


    un "No."


translate english day5_un_35d834dd:


    me "How long are you going to be offended? By what? What’s wrong with you? Everyone here behaves normally, except you."


translate english day5_un_3fddb229:


    "I did not care what my words meant. As if they had been said by someone else and the topic of conversation meant nothing."


translate english day5_un_5dedc88b:


    un "You really don’t understand anything. Alisa was right."


translate english day5_un_4bf0d4a0:


    me "About what?"


translate english day5_un_20163f63:


    un "Nevermind..."


translate english day5_un_0b35a130:


    "I closed my eyes to think it over."


translate english day5_un_475b9270:


    th "No, I can't go. I don't understand, don't know what to think, what to do."


translate english day5_un_abacf06a:


    th "And since when did I stop worrying about my situation in this world, or how to get back?"


translate english day5_un_d8c918ef:


    th "Since when was the only thing I could think about Lena, or how others would look at me, or how my actions looked from the other side?"


translate english day5_un_56543886:


    th "It's just stupid and it's not like me; in fact, it's inappropriate in this situation."


translate english day5_un_aed8e1b2:


    me "In short, I didn't do anything and don't intend to justify things which I didn't do!"


translate english day5_un_dfffcbac:


    "There was no reply."


translate english day5_un_4183115b:


    me "Hey! Do you hear me?"


translate english day5_un_d7d3178a:


    "I finally decided to see who was actually hiding behind the tree, but no one was there."


translate english day5_un_6807a309:


    th "So she left!"


translate english day5_un_e9ad8eab:


    "I rushed over to Lena's boat, but she was already far away, almost near the pier."


translate english day5_un_8f338516:


    me "Whatever!"


translate english day5_un_8494703d:


    "I shouted and walked along the shore."


translate english day5_un_ce617998_4:


    "..."


translate english day5_un_2c35439c:


    "I got back relatively quickly and without much trouble."


translate english day5_un_9d79bcf3:


    "However, my hands still ached terribly, and my eyes were closing themselves."


translate english day5_un_dd481fe9:


    "Probably such a physical and, more importantly, emotional load is too much for one person."


translate english day5_un_961bdcdb:


    "I walked slowly toward the camp leader's cabin, staring down at my feet and thinking about nothing."


translate english day5_un_793d9878:


    "Someone called to me at the square."


translate english day5_un_93b5fc50:


    dv "Hey!"


translate english day5_un_53356b4d:


    "Alisa ran up to me."


translate english day5_un_2cf26385:


    dv "Have you been to the island."


translate english day5_un_9513cd87:


    me "Yes..."


translate english day5_un_d331deef:


    "To tell the truth, I didn't want to talk to her about anything, but I already didn't have the strength to lie to her."


translate english day5_un_9c8b084d:


    dv "And how did it go?"


translate english day5_un_e5f72f43:


    me "Doesn't matter... I'm very tired and I'm going to sleep."


translate english day5_un_a7c99a15:


    dv "Come on, tell me!"


translate english day5_un_48f402f2:


    "Her face had such a nasty grimace that I shuddered with rage."


translate english day5_un_3f72c7b7:


    me "Why should you care?!"


translate english day5_un_12b03082:


    dv "Well, I just..."


translate english day5_un_5449a6fc:


    "She murmured in dismay."


translate english day5_un_cc04c95f:


    me "Mind your own business!"


translate english day5_un_ea222abb:


    "I snapped, and quickened my pace toward the cabins."


translate english day5_un_206f4c0d:


    "Alisa did not try to stop me."


translate english day5_un_1e4d5480:


    "Annoyingly, Olga Dmitrievna had not returned yet, and I could not find my key."


translate english day5_un_325046a9:


    th "What if I lost it?"


translate english day5_un_b5d9b711:


    "The only thing I could do was wait."


translate english day5_un_95440e29:


    "I plopped down into the deck chair and closed my eyes."


translate english day5_un_3eda7fc1:


    "My heart was heavy and my soul was being torn apart by vague expectations."


translate english day5_un_71767923:


    "From time to time I had a feeling that I was already dead (just without realizing it) and had been thrown into hell."


translate english day5_un_ef43a5c3:


    th "But really, instead of trying to get out of here, I'm spinning on a diabolic roundabout and it goes faster and faster. I'm becoming more and more involved in the life of this world, of this camp."


translate english day5_un_f6d61e07:


    "As if I had no past life – the real one"


translate english day5_un_6a8003ef:


    "As if I was always interested in the opinions of others..."


translate english day5_un_0730defb:


    th "Damn it! I never cared about it!"


translate english day5_un_226088e0:


    th "Why right here? Why right now?"


translate english day5_un_701c073f:


    "I recalled the face of Lena, in tears."


translate english day5_un_79fc3a70:


    th "Yeah, probably not the opinions of others, but her opinion was the one I cared about... "


translate english day5_un_195bd715:


    "I heard far footsteps and after a while Olga Dmitrievna appeared."


translate english day5_un_fb7d3b52:


    "She looked at me for a few seconds. Seemed like she was about to say something, but then just sighed, opened the door with her key and went in."


translate english day5_un_f73e0745:


    "I followed her."


translate english day5_un_62928289:


    "Shapeless shadows, blurry memories, fragments of feelings and emotions swirled in my head for a long time."


translate english day5_un_da141c38:


    "For so long that after a while I couldn't tell where I was or what's happening to me."


translate english day5_un_621dba00:


    "The only salvation was sleep..."
# Decompiled by unrpyc: https://github.com/CensoredUsername/unrpyc
