
translate belarusian prologue_24958abb:


    "I was having that dream once again."


translate belarusian prologue_07dac942:


    "{i}That{/i} dream..."


translate belarusian prologue_55dfd177:


    "...same thing every night."


translate belarusian prologue_9ce34629:


    "But it's all forgotten in the morning, as usual."


translate belarusian prologue_0c90c0c8:


    "Maybe it's for the best..."


translate belarusian prologue_8df02469:


    "Only a glimpse of memory will remain, of gates – half-opened, as if inviting me somewhere – with two frozen stone pioneers standing close by."


translate belarusian prologue_deca17df:


    "And also that strange girl...{w} who keeps asking me:"


translate belarusian prologue_814eff1e:


    dreamgirl "Will you come with me?"


translate belarusian prologue_e7b0f130:


    "Come...?"


translate belarusian prologue_a2e02439:


    "But where?"


translate belarusian prologue_fe88af01:


    "...and why?"


translate belarusian prologue_4d0aa097:


    "And where am I, anyway?"


translate belarusian prologue_0683e19b:


    "Of course, if it all happened in real life, I would certainly have been scared."


translate belarusian prologue_233a6b9d:


    "Well what else would one expect to feel?!"


translate belarusian prologue_cd46d7f7:


    "But this is just a dream.{w} The same one I see every night."


translate belarusian prologue_af4b467b:


    "There must be a reason!"


translate belarusian prologue_4cd30aa0:


    "You don't have to know {i}where{/i} or {i}why{/i} to realise: something is really happening."


translate belarusian prologue_2e4e4eec:


    "Something, desperately seeking my attention."


translate belarusian prologue_7bd50019:


    "Since everything that surrounds me here is real!"


translate belarusian prologue_354cb9c5:


    "As real as things in my own flat: I could open the gates, hear the hinges creak, brush the crumbling rust away with my hand, inhale the fresh cool air and shiver from the cold."


translate belarusian prologue_5fb243fe:


    "I could; but to do that, I would need to pick myself up, take a step, move my hand..."


translate belarusian prologue_a6f914fa:


    "But this is a dream. I understand that, but what of it? What does my {i}understanding{/i} change?"


translate belarusian prologue_edb5109a:


    "Because here it's just like on the other side of the cracked screen of an old TV, which struggles to fight against static noise and strives to show its audience everything without missing a single detail."


translate belarusian prologue_28f18900:


    "But the picture is getting blurry...{w} I must be waking up soon."


translate belarusian prologue_a20cefa7:


    "..."


translate belarusian prologue_14a2c666:


    "Maybe I should ask her something?{w} The girl."


translate belarusian prologue_5da0f47a:


    "What's her name..."


translate belarusian prologue_d7b38ee8:


    "About the stars, for instance..."


translate belarusian prologue_82d77cb1:


    "Why the stars, though?"


translate belarusian prologue_51851d8b:


    "I'd rather ask about the gates!{w} Yes, the gates!"


translate belarusian prologue_e5f4ee19:


    "She would be so surprised."


translate belarusian prologue_1edb57c8:


    "Or better, why the dot over an {i}i{/i} was called a tittle, but the dot over a {i}j{/i} was called a superscript dot?"


translate belarusian prologue_3234148c:


    "Nice letters..."


translate belarusian prologue_2367bcf1:


    "As if they don’t exist anymore!"


translate belarusian prologue_d92e974d:


    "Still, what do letters, gates and stars have to do with this place?"


translate belarusian prologue_e2138459:


    "Because even if I'm having {i}this{/i} dream every night, which will be forgotten soon anyway, I've got to look for answers here and now! "


translate belarusian prologue_d5050891:


    "And there, if you look carefully, you can see the Magellanic Clouds..."


translate belarusian prologue_e2b591e8:


    "As if I'd ended up in the Southern Hemisphere!"


translate belarusian prologue_a20cefa7_1:


    "..."


translate belarusian prologue_22d0eaf7:


    "In a dream, there are the small things that catch your attention: an unnatural colour of grass, impossible curves of straight lines, or your own distorted reflection – while the real danger, which could put an end to everything right here and now, seems trivial."


translate belarusian prologue_26999fe4:


    "It's natural, since {i}here{/i} you cannot die."


translate belarusian prologue_a6da1123:


    "I know it for sure – I've done it hundreds of times."


translate belarusian prologue_e85b8394:


    "But if you cannot die, is there a point in living?"


translate belarusian prologue_a0462877:


    "I should ask the girl. She's a local, she should know."


translate belarusian prologue_ee712a8b:


    "Yes, exactly!{w} I should ask her about the owl, for example."


translate belarusian prologue_b0865ff2:


    "One strange bird it is..."


translate belarusian prologue_2be63cfe:


    "Though, it doesn't matter..."


translate belarusian prologue_a20cefa7_2:


    "..."


translate belarusian prologue_814eff1e_1:


    dreamgirl "Will you come with me?"


translate belarusian prologue_db203a62:


    "And every time I have to answer."


translate belarusian prologue_7ad37ff1:


    "It's the only way, otherwise the dream will never end, and I will never wake up."


translate belarusian prologue_027137ed:


    "Every time it's so hard to decide on the answer."


translate belarusian prologue_34fac5fe:


    "Where am I, what am I doing here, who is she?"


translate belarusian prologue_071572e1:


    "And why does so much in my life depend on this answer?"


translate belarusian prologue_ec721a0b:


    "Or maybe it doesn’t...?"


translate belarusian prologue_51d80ee4:


    "It's just a dream after all."


translate belarusian prologue_ca1b37f9:


    "Just a dream..."


translate belarusian prologue_41cdd4ce:


    "The computer screen stared at me as if it was alive."


translate belarusian prologue_86cfd3d1:


    "Sometimes it really did seem to me that it was conscious of itself, had its own thoughts and wishes, ambitions; that it had feelings, could love and suffer."


translate belarusian prologue_b3960be5:


    "As if in our relationship, the screen wasn’t an instrument – it was me who was a lifeless piece of plastic and textolite."


translate belarusian prologue_67859f20:


    "There is some truth in that, probably because the computer provides 90%% of my communication with the outside world."


translate belarusian prologue_e051da8b:


    "Anonymous imageboards, some chats from time to time, rarely ICQ or Jabber, and forums even more rarely."


translate belarusian prologue_3f938594:


    "People on the other end of the internet cable simply do not exist!"


translate belarusian prologue_5a1a52c8:


    "All of them are simply creations of its sick imagination, an error in the source code or a kernel bug, which started living a life of its own."


translate belarusian prologue_2728421d:


    "If one looked at my existence from the outside, such thoughts would seem crazy, and a psychologist would surely give me a bunch of sophisticated diagnoses and maybe write me a doctor's referral to the looney bin."


translate belarusian prologue_9d607629:


    "A small apartment with no signs of repair or any semblance of order in it, and always the same view out the window on the gray megalopolis running somewhere day and night – such are the conditions of my life."


translate belarusian prologue_5823e040:


    "Well, of course, it didn’t all start like this..."


translate belarusian prologue_7380c69b:


    "I was born, went to school and finished it – like all the others."


translate belarusian prologue_e5f87d9f:


    "I was accepted at a university, where I spent a year and a half trailing behind and struggling."


translate belarusian prologue_c895b6f5:


    "I drifted through several jobs.{w} Sometimes it was working out quite well, sometimes I was even getting decent money for it."


translate belarusian prologue_26348b28:


    "But it all felt like it was not mine, as if taken from another man's biography."


translate belarusian prologue_111ab6cc:


    "I wasn't living life to its fullest – it was looping over and over in monotonous circles.{w} Like in the movie 'Groundhog Day'."


translate belarusian prologue_bab8b972:


    "It's just that I had no choice in how to spend my day, and every day repeated itself, the same vicious spiral.{w} A spiral of emptiness, misery and despair."


translate belarusian prologue_76b2fe88:


    nvl clear


translate belarusian prologue_b41ec809:


    "For the last few years I just sat in front of the screen all day."


translate belarusian prologue_c39afb80:


    "Sometimes there were menial jobs, sometimes my parents helped me."


translate belarusian prologue_dfb24146:


    "All in all, I was able to provide for myself."


translate belarusian prologue_ba1a156e:


    "No wonder really, since my needs are quite minor."


translate belarusian prologue_9bc39b5c:


    "I hardly ever leave my home, and my communication with other people almost exclusively consists of online correspondence with {i}the anonymous{/i}, who have no real name, no gender, no age."


translate belarusian prologue_a3a04a99:


    "So, in brief, a quite typical life of a quite typical antisocial person of his time.{w} Kind of Donnie Darko on a minor scale, without Doomsday-related visions."


translate belarusian prologue_ac5e97c3:


    "Maybe some highly respected auther will write a novel about me, and it will become a contemporary classic of modern literature.{w} Or I will write one myself..."


translate belarusian prologue_eea3f384:


    "However, what's the point of fooling myself – I tried many times, but couldn't even come up with a simple short story."


translate belarusian prologue_6d041340:



    nvl clear
    "I tried to learn many other things as well."


translate belarusian prologue_64793d14:


    "Not gifted enough to draw.{w} Programming – got bored.{w} Foreign languages - takes too much time..."


translate belarusian prologue_b41193f7:


    "The only thing I loved doing was reading, but still I never would have called myself a scholar."


translate belarusian prologue_d0321015:


    "Perhaps I was an ace at watching anime and a grandmaster of lame internet jokes."


translate belarusian prologue_acb955ab:


    "If I were to get paid for it, I would probably be a happier person (and a richer person too), but I doubt it would fill the hole inside me."


translate belarusian prologue_76b2fe88_1:


    nvl clear


translate belarusian prologue_8d743298:


    "Today was another typical day of a typical failure's typical life."


translate belarusian prologue_4d249ee8:


    "And today is the day when I have to go to my university reunion."


translate belarusian prologue_3d7e70ae:


    "Frankly speaking, I really didn't want to."


translate belarusian prologue_556a5316:


    "What is the point? The time I spent with them was so short."


translate belarusian prologue_b84fba8f:


    "However, I was persuaded by a friend: my former university mate and one of the few with whom I kept in touch other than through the Internet."


translate belarusian prologue_a2136874:


    "A frosty evening.{w} Bus stop. Waiting."


translate belarusian prologue_55a843c8:


    "I never liked winter.{w} Though hot summer is not my season either."


translate belarusian prologue_6c43f774:


    "It’s just that I see no reason to point out any particular time of the year, it doesn't matter much what the weather is outside when you stay at home 24/7."


translate belarusian prologue_a43f6da8:


    "The bus today was running so late that I was about to curse it all and spend my last few hundreds rubles for a taxi (the idea of just returning home didn't cross my mind for some reason)."


translate belarusian prologue_42c6ca3e:


    "As usual, millions of thoughts flew through my mind, but there was not a single useful one to seize on."


translate belarusian prologue_ebf71c4c:


    "Such a thought that you could bring into existence, give a shape, turn into an idea and put into practice."


translate belarusian prologue_6b447492:


    "Maybe I could start my own business?{w} But where would I get the money from?"


translate belarusian prologue_1263d7e9:


    "Or maybe I could go back to working in an office?{w} No, no way!"


translate belarusian prologue_ef2e5c25:


    "Maybe I should try freelancing?{w} But what skills do I have and who would want me after all...?"


translate belarusian prologue_42e509b4:


    "I suddenly remembered my childhood...{w} Or rather, my teen years, the time when I was 15-17 years old."


translate belarusian prologue_153757bd:


    "Why exactly those years?{w} No idea."


translate belarusian prologue_b3ca007f:


    "I guess it's because back then, everything was much more simple."


translate belarusian prologue_74b11ddc:


    "It was easier to make decisions, so complicated now and so simple then."


translate belarusian prologue_a0da84d1:


    "Waking up in the morning, I knew exactly how my day was going to pass, and I always eagerly looked forward to the weekend. Then I could get some rest and have time for the things I liked: computer, football, going out with friends."


translate belarusian prologue_7160de27:


    "And then, at the beginning of next week, I'd take up my studies again."


translate belarusian prologue_334f2acb:


    "Back then, there were no such worrying questions like 'why', 'who needs it', 'what will change if I do it' or 'what will not change'."


translate belarusian prologue_272df669:


    "A simple lifestyle, so casual for any normal person and so odd to myself today."


translate belarusian prologue_8963d5d3:



    nvl clear
    "That careless childhood age...{w} It was also then that I met my first love."


translate belarusian prologue_4a6ac0a3:


    "Her appearance and personality have vanished from my memory."


translate belarusian prologue_7493b623:


    "Only her name remains, like a brief line from a social network profile, along with the feelings which overwhelmed me when I was with her.{w} Affection, tenderness, the desire to care for her and to protect her..."


translate belarusian prologue_064fa38f:


    "Sadly, it didn’t last long."


translate belarusian prologue_c7008ed9:


    "Today I can hardly imagine something like that happening."


translate belarusian prologue_f716a97b:


    "I would probably like to meet a girl, but I don't know how to start a conversation, what on earth to discuss and how to attract her."


translate belarusian prologue_d631396b:


    "Well, I haven't met any suitable girls for a long time.{w} But where could I meet one anyway...?"


translate belarusian prologue_76b2fe88_2:


    nvl clear


translate belarusian prologue_52032d61:


    "The sound of an engine brought me back to reality."


translate belarusian prologue_5d936a59:


    "A bus pulled over."


translate belarusian prologue_68c6dc22:


    "There was something abnormal about it, I thought."


translate belarusian prologue_f260960e:


    "Then again, it doesn’t matter: only the '410' runs this route."


translate belarusian prologue_3cf36c8d:


    "Street lights pass me by. It's as if their cold light sparks inside of me, trying to ignite feelings long dead."


translate belarusian prologue_ac957580:


    "Or maybe not ignite, just awaken them..."


translate belarusian prologue_b9473edc:


    "Because those feelings, they have been living in me for a long time, slumbering and waking up again."


translate belarusian prologue_59544860:


    "The driver's radio was playing some very familiar tune.{w} But I wasn't listening to it."


translate belarusian prologue_53102b62:


    "I was watching the cars passing by through the fogged-up window."


translate belarusian prologue_0931b53b:


    "Because people are always rushing somewhere, chasing something they need, stuck in their own little worlds, why would they care about mine?"


translate belarusian prologue_5ccdd86f:


    "They probably have their own serious problems, or maybe they have much easier lives."


translate belarusian prologue_813e6d62:


    "You can't know for sure, since all people are different.{w} Or are they?"


translate belarusian prologue_e0c41f97:


    "Sometimes someone's actions can easily be predicted, but if you try to look inside his soul you will only see impenetrable darkness."


translate belarusian prologue_a20cefa7_3:


    "..."


translate belarusian prologue_9f51c5b0:


    "The bus was approaching downtown, and my thoughts were interrupted by the bright city lights."


translate belarusian prologue_1029ada9:


    "Hundreds of billboards, thousands of cars, millions of people."


translate belarusian prologue_d443aa58:


    "I watched this light show, and somehow I got terribly sleepy."


translate belarusian prologue_d56c688b:


    "My eyes closed just for a moment, and then..."
# Decompiled by unrpyc: https://github.com/CensoredUsername/unrpyc
