translate belarusian strings:


    old "Да, я пойду с тобой"
    new "Yes, I'll come with you"


    old "Нет, я останусь здесь"
    new "No, I'll stay here"


    old "Не отвечать ей"
    new "Do not reply"


    old "Ответить"
    new "Reply"


    old "Побежать за ним"
    new "Run after him"


    old "Ничего не делать"
    new "Do nothing"


    old "Попытаться отнять котлету"
    new "Attempt to take the cutlet"


    old "Не пытаться отнять котлету"
    new "Do not attempt to take the cutlet"


    old "Взять ключи"
    new "Take the keys"


    old "Не трогать"
    new "Leave the keys"


    old "Похвалить книгу"
    new "Praise the book"


    old "Ничего не говорить"
    new "Keep silent"


    old "Пойти за картами со Славей"
    new "Go get the cards with Slavya"


    old "Пойти одному"
    new "Go alone"


    old "Поспорить с Алисой"
    new "Bet with Alisa"


    old "Не спорить с Алисой"
    new "Do not bet with Alisa"


    old "Пройти обучение"
    new "Play the tutorial"


    old "Пропустить обучение"
    new "Skip the tutorial"


    old "Играть турнир"
    new "Play the tournament"


    old "Пропустить турнир (выиграть у Алисы)"
    new "Skip the tournament (Win against Alisa)"


    old "Пропустить турнир (проиграть Алисе)"
    new "Skip the tournament (Lose against Alisa)"


    old "Пропустить турнир (проиграть Ульяне)"
    new "Skip the tournament (Lose against Ulyana)"


    old "Пропустить турнир (проиграть Лене)"
    new "Skip the tournament (Lose against Lena)"


    old "Извини, но я уже с Леной договорился"
    new "Sorry, but I promised Lena"


    old "Ладно, подожди минутку"
    new "Fine, wait a minute"


    old "Хорошо, я приду..."
    new "Okay, I'll come..."


    old "Знаете, меня Ольга Дмитриевна попросила вечером ей помочь..."
    new "You know, Olga Dmitrievna asked me to help her tonight..."


    old "Стоит пойти посмотреть"
    new "I should go take a look"


    old "Какая разница? Надо продолжить искать ответы"
    new "Who cares? I'd better keep on looking for answers"


    old "Пожалуй, я помогу Славе"
    new "I'd rather help Slavya"


    old "Думаю, помогу ребятам в постройке гигантских роботов"
    new "I think I'd rather assist the guys with their giant robot engineering"


    old "Ладно, я помогу спортивному клубу"
    new "Fine, I'll help the sports club"


    old "Ладно, я приду"
    new "Okay, I'll come"


    old "Нет, извини"
    new "No, I won't. Sorry"


    old "Сбежать"
    new "Run away"


    old "Остаться и помочь Ульяне убраться"
    new "Stay and help Ulyana with cleaning up"


    old "Пойти к Алисе"
    new "Go to Alisa"


    old "Пойти на дискотеку"
    new "Go to the dance party"


    old "Рассказать, что был с Алисой"
    new "Tell her that you’ve been with Alisa"


    old "Придумать другое оправдание"
    new "Make up another excuse"


    old "Пойти с Мику"
    new "Go with Miku"


    old "Не ходить с Мику"
    new "Don't go with Miku"


    old "Мне кажется, на тебе это бы смотрелось прекрасно"
    new "I think you’d look gorgeous in it"


    old "Да просто так"
    new "Just asking…"


    old "Небось ворованных конфет объелась?"
    new "Overdose of stolen candies, right?"


    old "В столовой отравилась?"
    new "Got food poisoning from the canteen?"


    old "Спросить, что это"
    new "Ask about the bundle"


    old "Не спрашивать"
    new "Do not ask about the bundle"


    old "Дать Алисе уголь"
    new "Give Alisa the activated carbon"


    old "Не давать ей уголь"
    new "Do not give Alisa the activated carbon"


    old "Съесть яблоко"
    new "Eat the apple"


    old "Не есть"
    new "Do not eat the apple"


    old "Пошёл с Алисой"
    new "... go with Alisa"


    old "Пошёл со Славей"
    new "... go with Slavya"


    old "Пошёл с Ульяной"
    new "... go with Ulyana"


    old "Пошёл с Леной"
    new "... go with Lena"


    old "Пошёл один"
    new "... go alone"


    old "Налево"
    new "Go left"


    old "Направо"
    new "Go right"


    old "Сказать, что ходил с Леной"
    new "Tell her you went with Lena"


    old "Сказать, что ходил один"
    new "Tell her you went alone"


    old "Ладно…"
    new "Okay..."


    old "Нет, знаешь, у меня дела ещё…"
    new "No, I won't. You know, I have some things to do..."


    old "Согласиться"
    new "Accept"


    old "Отказаться"
    new "Refuse"


    old "Пойти с Леной"
    new "Go with Lena"


    old "Пойти со Славей"
    new "Go with Slavya"


    old "Да, я старался!"
    new "Yep, I tried my best!"


    old "Это всё благодаря помощи девочек!"
    new "Thanks to the girls' help!"


    old "Попытаться выхватить книгу у Алисы"
    new "Try to snatch the book out of Alisa's hand"


    old "Проявить осторожность"
    new "Stay on guard"


    old "Попытаться узнать, о чём спорят Лена и Алиса"
    new "Try to find out what Alisa and Lena are arguing about"


    old "Не делать ничего, просто сидеть"
    new "Do nothing, just stay seated"


    old "Попытаться найти Славю"
    new "Try to find Slavya"


    old "Пойти к Ульяне"
    new "Go to Ulyana"


    old "Просто сидеть"
    new "Remain seated"


    old "Не вмешиваться"
    new "Do not interfere"


    old "Помочь Алисе"
    new "Help Alisa"


    old "Грубо схватить её за руку"
    new "Grab her arm roughly"


    old "Попытаться остановить словами"
    new "Try to stop her verbally"


    old "Да просто есть захотелось"
    new "I was just hungry"


    old "Хочу взять еду для Слави"
    new "I want to get something for Slavya"


    old "Мне не в чем оправдываться"
    new "I have nothing to justify myself for!"


    old "Мы ничего такого не делали, чтобы перед вами оправдываться!"
    new "We have no reason to justify ourselves!"


    old "Пойти за голосом"
    new "Follow the voice"


    old "Не ходить за голосом"
    new "Do not follow the voice"


    old "Это всё она!"
    new "That's all her fault!"


    old "Всё это моя вина..."
    new "That's all my fault..."


    old "Идти вперёд"
    new "Keep going"


    old "Повернуть назад"
    new "Go back"


    old "Выбрать Юлю"
    new "Choose Yulya"


    old "Выбрать Славю"
    new "Choose Slavya"


    old "Выбрать Алису"
    new "Choose Alisa"


    old "Выбрать Лену"
    new "Choose Lena"


    old "Выбрать Ульяну"
    new "Choose Ulyana"


    old "Выбрать Машу"
    new "Choose Masha"
# Decompiled by unrpyc: https://github.com/CensoredUsername/unrpyc
