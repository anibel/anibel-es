
translate belarusian day1_e9644438:


    "Bright daylight struck my eyes."


translate belarusian day1_8e82fc0a:


    "At first I didn't pay attention, as I wasn't fully awake yet."


translate belarusian day1_44c308e2:


    "On their own, my legs carried me towards the door."


translate belarusian day1_ccc28a84:


    me "Damn, looks like I fell asleep and missed my stop...!"


translate belarusian day1_aee379b4:


    "But there was no door…"


translate belarusian day1_d42615e7:


    "I looked around the bus and realised that it wasn't a good old worn-out LiAZ, instead the bus was an Icarus model, a new one!"


translate belarusian day1_03cecdb6:


    "I froze in shock."


translate belarusian day1_3665cd10:


    th "How...?{w} What...?{w} Am I dead...?"


translate belarusian day1_2df08a3e:


    th "Have I been kidnapped?"


translate belarusian day1_a23b7265:


    th "No, I must be dead..."


translate belarusian day1_a2eb760d:


    "I patted myself down feverishly, slapped myself painfully in the face a few times, banged my forehead on the back of one of the bus seats..."


translate belarusian day1_892f12f3:


    th "It's clear: either I'm still alive, or you can still feel pain when you're dead."


translate belarusian day1_b55f45ec:


    th "But how could this happen?!"


translate belarusian day1_8b1a1045:


    th "Maybe I slept for too long and ended up at the bus depot."


translate belarusian day1_fa8f47fe:


    th "And then what, did they put me onto another bus...?"


translate belarusian day1_05f4c741:


    "I rushed out and took a look around."


translate belarusian day1_7fd7201a:


    "Greenery wherever I looked: tall grass on the roadside, trees, flowers..."


translate belarusian day1_0c35f127:


    th "Summer!{w} But how?!{w} It was winter just a moment ago..."


translate belarusian day1_ae3344e7:


    "My head was aching unbearably.{w} As if it was going to explode."


translate belarusian day1_2208f558:


    "Slowly, I began to remember."


translate belarusian day1_8d858ed9:


    "A long road running off into the distance; forests, plains, fields, lakes, and forests again."


translate belarusian day1_786f868f:


    th "I think I was sleeping; but then how can I remember all of it?"


translate belarusian day1_2f605107:


    "And then…{w} A gap..."


translate belarusian day1_5fabbbed:


    "Some girl leaning over me."


translate belarusian day1_73437152:


    "She softly whispered something into my ear."


translate belarusian day1_c2d5fb61:


    "Then a gap again..."


translate belarusian day1_2e96e275:


    "And then I woke up here."


translate belarusian day1_c8d2bde3:


    th "Who was that strange girl?"


translate belarusian day1_a1a6b8d6:


    th "Or was she just a dream?"


translate belarusian day1_ebfa0491:


    "For some reason thinking about her made me feel better and calmed me down a little. I felt warmth all over, coming from the inside."


translate belarusian day1_fde77cc5:


    th "Could it be her who brought me here?"


translate belarusian day1_9bacf66f:


    th "Then I need to find her!"


translate belarusian day1_a35e9118:


    th " And the best place to look for her is away from here."


translate belarusian day1_378336bd:


    " I rushed to the left, then to the right, then stopped, hesitating over where to go. Finally, I ran in the direction from which the bus had probably came."


translate belarusian day1_a20cefa7:


    "..."


translate belarusian day1_a20cefa7_1:


    "..."


translate belarusian day1_723ee7f4:


    "Physical exercise does refresh one's mind. Thoughts become clearer, and it gets a little easier to evaluate the surrounding reality."


translate belarusian day1_cb806649:


    "Not in my case however – I was sitting on the roadside, wheezing and trying to ease my sore throat by gulping breaths of hot air."


translate belarusian day1_6d244e52:


    "In any case, the run did its job: the fear withdrew for a while."


translate belarusian day1_adb77bc9:


    th "Maybe I really am just dreaming...?"


translate belarusian day1_93fa4347:


    "Though recalling my self-harm on the bus, I immediately rejected the idea."


translate belarusian day1_fae72ec6:


    me "I am neither dreaming nor dead..."


translate belarusian day1_36f36703:


    "A narrow road ran through the field and far into the distance.{w} That exact same road from my dream."


translate belarusian day1_21bd3c65:


    th "I must be very far away from home."


translate belarusian day1_41a25460:


    "And it's not just that it was winter yesterday and it's summer now."


translate belarusian day1_fe462b7c:


    "It's the whole environment."


translate belarusian day1_610fd3ae:


    th "Of course, summer is usually like this, green and hot- but here everything is not entirely lifelike."


translate belarusian day1_b66d48e9:


    "Everything looks like it was taken from the paintings of Russian landscape artists of the nineteenth century."


translate belarusian day1_d7a6969b:


    "The grass is just too lush; the bushes are not like what bushes should be, they are so thick that you can't see anything through them, like treetops, honestly..."


translate belarusian day1_6482df8a:


    "And the trees themselves...{w} The forest was quite far away, but the trees looked as if they had closed their ranks and were now just waiting for the order to advance onto the fields and plains."


translate belarusian day1_46d39e23:


    "I caught my breath and looked at the bus, which was now barely visible."


translate belarusian day1_d0f34e2d:


    th "That was a good run..."


translate belarusian day1_5a5230cb:


    "Fear overtook me once again."


translate belarusian day1_52a8e81e:


    th "Those power lines...{w} There must be people here!"


translate belarusian day1_a53379f8:


    "But what does it {i}mean{/i}?"


translate belarusian day1_8d395e64:


    "In fact, that means nothing at all!{w} Couldn't they have power lines even in hell?"


translate belarusian day1_fbe4386c:


    "Roasting sinners over hot coals? That's so last-century..."


translate belarusian day1_c88e5ea3:


    "I must have reached the point of no return, after which you should either lose your mind completely or finally try to understand what is going on."


translate belarusian day1_3cbef58c:


    th "And while I still have a choice, I should pick the second option!"


translate belarusian day1_28b9b20f:


    "I slowly headed back to the bus."


translate belarusian day1_9aeca35d:


    th "Of course it was scary."


translate belarusian day1_d5a8e5e1:


    th "But I'm not likely to find an answer in the fields or the woods, and this wretched bucket of bolts is the only kind of link that I have with the real world."


translate belarusian day1_a20cefa7_2:


    "..."


translate belarusian day1_001631c6:


    th "I should carefully scout the area."


translate belarusian day1_3e09185c:


    "A brick wall and its gates crowned with a 'Sovyonok' sign, statues of pioneers standing on either side, and a road sign nearby showing the bus route number: '410'."


translate belarusian day1_59b146e5:


    me "The trip's taking a bit too long today."


translate belarusian day1_e98bfbde:


    "I smirked."


translate belarusian day1_0fa2263b:


    "A person may start acting inappropriately in extreme situations."


translate belarusian day1_db78176c:


    th "Something like that is probably happening to me now."


translate belarusian day1_95a9a169:


    "This place didn't look abandoned at all – no rust on the gates, no damage to the walls."


translate belarusian day1_3ba35159:


    me "'Sovyonok'…"


translate belarusian day1_33b4e80e:


    th "What could have a name like that?"


translate belarusian day1_94f91cd7:


    th "Judging by the pioneer statues, it could be a kids summer camp.{w} Moreover, it appears to be open!"


translate belarusian day1_deb4632a:


    th "Of course, the simplest explanation, logically speaking, explains nothing at all."


translate belarusian day1_b987229b:


    th "The strange girl, the altered bus, summer, the pioneer camp..."


translate belarusian day1_33930872:


    "Thousands of theories went through my mind instantly: from alien abduction to lethargic sleep, from a hallucination to a time and space shift..."


translate belarusian day1_d28308d8:


    "None was worse than any other, but there was really no way to pick a single one."


translate belarusian day1_c83682b4:


    "Then it occurred to me - I can try to make a phone call!"


translate belarusian day1_30421d7d:


    "I took out my cellphone and dialed the first number from my contact list."


translate belarusian day1_a5e58fe8:


    "But instead of signal strength bars, the screen was showing a thick cross."


translate belarusian day1_19146449:


    th "Alright, there may be no signal in such a remote place."


translate belarusian day1_0d3882c7:


    th "Though I cannot be the only one who came here!"


translate belarusian day1_c52f433f:


    th "Buses don't drive themselves!"


translate belarusian day1_9191dff2:


    "I examined the bus from all sides to make sure that it wasn't a hallucination."


translate belarusian day1_841adb1f:


    "Bits of dirt on the bottom, some rust here and there, faded paint and worn out tires- no, this is definitely a very ordinary Icarus."


translate belarusian day1_6109f212:


    th "Yeah, exactly the kind of bus which takes you to places beyond your understanding if you carelessly fall asleep."


translate belarusian day1_0343ddfb:


    "I gave a nervous chuckle."


translate belarusian day1_d4028078:


    "It came out by itself, sporadically.{w} Because this wasn't the right place or time to laugh..."


translate belarusian day1_17403436:


    th "But where on earth is the driver?"


translate belarusian day1_7530ded5:


    "I cautiously sat down on the kerb beside the bus and started to wait."


translate belarusian day1_a20cefa7_3:


    "..."


translate belarusian day1_4bc2caf0:


    "My patience didn't last long."


translate belarusian day1_ede35d71:


    "My anxiety seemed to have reached its peak, and I started to go slightly mad."


translate belarusian day1_c8bc0b5a:


    "In such a situation, anyone would have probably felt something similar."


translate belarusian day1_0b02c10b:


    "Aliens and parallel universes were gone from my imagination, leaving only void and darkness."


translate belarusian day1_33b8346c:


    th "Is this how everything will end? How {i}my life{/i} will end?"


translate belarusian day1_0818ff10:


    th "But I wanted to do so much, there were so many things that I had no time for yet..."


translate belarusian day1_405f9fbf:


    "I was overwhelmed by the idea, this was definitely the end."


translate belarusian day1_97d1733b:


    th "But why?!"


translate belarusian day1_6e7e69ad:


    th "It's not fair! Surely I'm no worse than anyone else!"


translate belarusian day1_a72fb0ee:


    th "God, why...?"


translate belarusian day1_69065e88:


    "Tears were burning my eyes unbearably, I curled up and started rolling in the grass."


translate belarusian day1_4629954b:


    me "WHY?! WHAT DID I DO?! WHY ME?!"


translate belarusian day1_6f37277c:


    "But my cries were only heard by the speechless statues of the pioneers and a bird on a tree, which immediately flapped its wings and took off, crying out something in its own bird language, as if laughing at the idiot who dared to interrupt its after-dinner nap."


translate belarusian day1_893c0214:


    "I was left breathless from weeping and just lay quietly, sobbing occasionally"


translate belarusian day1_a20cefa7_4:


    "..."


translate belarusian day1_1b527b73:


    "After a while I managed to pull myself together."


translate belarusian day1_659aa794:


    "My mind cleared up a bit, as if terror and the fear of death gave me a little break."


translate belarusian day1_dba992b6:


    th "All in all, if someone wanted to kill me, what is all this for?"


translate belarusian day1_1a34ee7c:


    th "It doesn't look like an experiment either."


translate belarusian day1_56b2c610:


    th "If this is just some crazy coincidence then there's probably no threat."


translate belarusian day1_cc407c1e:


    th "Anyway, for now it seems there's no danger."


translate belarusian day1_35abcf62:


    "The panic was soon gone."


translate belarusian day1_bbe3feaf:


    "Of course, the blood was still pounding in my temples, and my hands were still shaking, but at least I could think clearly now."


translate belarusian day1_007b1ddb:


    th "Right now, there is nothing I can really change anyway!{w} So no matter how much I think or how mad I get, it would only make things worse."


translate belarusian day1_607957e4:


    th "Until some actual facts appear, there is really no point in making guesses."


translate belarusian day1_cea13f7e:


    th "In any case, I won't learn anything by lounging about here."


translate belarusian day1_f68fbb50:


    "This camp (if, of course, it is really a camp) looked like the only place where people could be, so I decided to go there, and hardly had I reached the gates when..."


translate belarusian day1_cb43b355:


    "A girl came out from behind them..."


translate belarusian day1_c9771ed6:


    "... wearing a pioneer uniform."


translate belarusian day1_76dc3c1c:


    th "My logic didn’t let me down this time."


translate belarusian day1_c454f340:


    th "Then again, a pioneer uniform in the 21st century..."


translate belarusian day1_55a53f71:


    th "And then again, a girl {i}here{/i}..."


translate belarusian day1_a0dd748b:


    "I froze, unable to take a step."


translate belarusian day1_888fb514:


    "I felt very much like running away.{w} Running as far away as I could from this place, far from this bus, gates, statues and far from this bloody bird with its siesta."


translate belarusian day1_1a518774:


    "Just running, free like the wind, faster and faster, waving to the planets passing by, winking at the galaxies."


translate belarusian day1_8abc0b99:


    "Running, becoming a pulsar ray, and turning into vestigial radiation, running to face the unknown."


translate belarusian day1_b53ca9d4:


    "Run no matter where, as long as it is {b}far away{/b} from this place!"


translate belarusian day1_d158f7e2:


    "Meanwhile, the girl came closer and smiled."


translate belarusian day1_3fbcaafd:


    "I could not help but notice her beauty, even though I was trembling with fear."


translate belarusian day1_01e37be4:


    "Human instincts work independant of consciousness, and while only 5%% of the brain is responsible for cognitive processes, the remaining 95%% are always busy sustaining {i}life{/i}, and in particular, ensuring stable functioning of the hormonal system."


translate belarusian day1_b619432f:


    "I desperately wanted to get less complicated, and stop thinking in formal quotes from an encyclopedia, though my thoughts appeared one by one, being stupid, out of place, as if taken from an internal monologue of the hero of some junky softcover crime fiction book."


translate belarusian day1_159ede02:


    "A pretty slavic face, long braids that looked like two armfuls of fresh hay, and blue eyes you could drown in."


translate belarusian day1_ebdc09cc:


    slp "Hi, you must have just arrived?"


translate belarusian day1_a009dbf2:


    "I was frozen in astonishment."


translate belarusian day1_b5aec161:


    "The girl looked perfectly normal, just like an ordinary human, but I still could not utter a single word."


translate belarusian day1_fff6f9eb:


    th "It's too late to run..."


translate belarusian day1_880e4e10:


    slp "Did I say something wrong?"


translate belarusian day1_06176d1f:


    "It took a monumental effort to answer:"


translate belarusian day1_13317d7b:


    me "Uhm... yeah..."


translate belarusian day1_c44cb8e0:


    slp "What?"


translate belarusian day1_d51bc1bf:


    me "Oh, nothing, I mean I've {i}just arrived{/i}."


translate belarusian day1_c0a65fae:


    "I replied quickly, beginning to worry that I might have blurted out something wrong."


translate belarusian day1_13317d7b_1:


    me "Uhm... yeah..."


translate belarusian day1_3c13f21e:


    slp "All right then, welcome!"


translate belarusian day1_6d77584d:


    "She smiled broadly."


translate belarusian day1_82d0a78d:


    "Strange, it looked as if I had just a normal girl in front of me."


translate belarusian day1_31e75c3d:


    th "Bah, I shouldn't have returned here, the woods and fields seemed better..."


translate belarusian day1_c2c61eee:


    th "But what shall I do next – try to speak with her as if she was a human, or run away, or what?"


translate belarusian day1_85e4f294:


    "The blood was pumping unbearably in my head, tearing it apart from the inside; a little bit more and the poor pioneer girl would be splattered with the gruesome contents of my skull..."


translate belarusian day1_26d0580a:


    slp "What's so funny about that?"


translate belarusian day1_5b1e985b:


    "The girl looked me over."


translate belarusian day1_632c64ff:


    "It sent shivers down my spine, and my knees started to tremble."


translate belarusian day1_fa942807:


    me "N... nothing..."


translate belarusian day1_81fa1f0f:


    slp "Great then."


translate belarusian day1_35ff2f5f:


    th "Great? What's so great about that?"


translate belarusian day1_9d000438:


    "Suddenly a thought crossed my mind: to hell with it all. Forget about the bus behind me, the fact that it was winter yesterday and summer today. I wanted to rip off my itchy sweater and just accept that all this is actually happening. Everything is as it should be, all this is for the best..."


translate belarusian day1_1ab1c37a:


    me "Would you happen to know...?"


translate belarusian day1_d0ebceb7:


    slp "You should go to our camp leader, she'll tell you everything!"


translate belarusian day1_3399b38f:


    slp "Look.{w} You go straight ahead to the square, then turn left. You'll see several small cabins."


translate belarusian day1_f40c35be:


    "She pointed at the gates, as if I knew what was behind them."


translate belarusian day1_54f0a60f:


    slp "Well, you can ask someone where Olga Dmitrievna's cabin is."


translate belarusian day1_17993887:


    me "I… erm…"


translate belarusian day1_81c28b1c:


    slp "Got it?"


translate belarusian day1_a1e6972e:


    "Of course I didn't."


translate belarusian day1_2f0e0a31:


    slp "Well, I've got to go now."


translate belarusian day1_77cc4ba5:


    "The girl waved her hand at me and disappeared through the gates."


translate belarusian day1_bc447943:


    "It seemed as if to her, I was something...{w} ordinary."


translate belarusian day1_07ef85cb:


    "And all this show with the bus and the travels while awake or asleep were troubling only to me, while everything here is just the way it is supposed to be."


translate belarusian day1_2d3f952e:


    th "Camp leader, pioneer uniform…"


translate belarusian day1_cf1c4a81:


    th "What, are they doing a historical reenactment here?"


translate belarusian day1_bce4a681:


    th "I hope I won't find Lenin standing atop an armored car in this square."


translate belarusian day1_1b36af68:


    th "But even that would not surprise me right now..."


translate belarusian day1_39014c31:


    "After standing alone for a while, I headed into the camp."


translate belarusian day1_6f92edab:


    "A mere 50 metres ahead a small one-storey house popped up on the left side.{w} The sign near the door said 'Clubs'."


translate belarusian day1_b20957b2:


    "I was about to come closer..."


translate belarusian day1_93faf713:


    "When the door suddenly opened, and a short girl wearing a pioneer uniform came out."


translate belarusian day1_bc2ffd42:


    "Her pretty face gave me the impression of one suffering for the fate of the whole of mankind with a truly universal sorrow."


translate belarusian day1_5b8bfb48:


    "As soon as she saw me the girl froze, as if frightened."


translate belarusian day1_82b60add:


    "I froze too, considering what was the best to do – to approach first or wait until she showed some initiative.{w} Or maybe run away after all..."


translate belarusian day1_d6de2681:


    "Although this last option was constantly being suggested only by my self-preservation instinct (at least that's what I'd like to believe)."


translate belarusian day1_2cebe5d7:


    "Not the worst human instinct, but far from the most logical."


translate belarusian day1_63473f65:


    "If this instinct played poker against deductive abilities, the outcome would be predetermined."


translate belarusian day1_a971bc7a:


    "And those deductive abilities (or at least their semblance) were hinting to me that there was no need to be afraid of this girl."


translate belarusian day1_9b858553:


    "Suddenly somebody jumped out of nearby bushes."


translate belarusian day1_b978b533:


    "A girl wearing a bright red T-Shirt with 'USSR' written on it."


translate belarusian day1_7ebe0cfb:


    th "Such a perfect reproduction of the age."


translate belarusian day1_42c9a02c:


    "She looked quite short from a distance and was probably younger than both pioneer girls – the one at the gates and this girl at the door of the 'Clubs'."


translate belarusian day1_c52afe92:


    "At last I decided to come closer, but the 'USSR' (as I called her in my mind) jumped towards the first girl and started telling her something while wildly waving her arms."


translate belarusian day1_b63ce3ba:


    "The other girl in turn seemed confused and lowered her gaze, remaining silent."


translate belarusian day1_d44e3c53:


    "I would have probably continued to observe their amusing dialogue, but the 'USSR' suddenly pulled something out of her pocket and started waving it in front of the first girl's face."


translate belarusian day1_258e8119:


    "This {i}something{/i} turned out to be a grasshopper."


translate belarusian day1_ed6083c2:


    unp "Eeeeee-eee-eeeeee!"


translate belarusian day1_87779d4a:


    "The first girl squealed."


translate belarusian day1_f91880c6:


    "She must not be too keen on insects, as she instantly rushed off towards the place where Lenin presumably made his speech about the workers' and peasants' revolution."


translate belarusian day1_03448d77:


    "That is to say, towards the square..."


translate belarusian day1_ebc4605c:


    "The 'USSR' glanced at me, grinned playfully, and dashed after her."


translate belarusian day1_38e92acb:


    th "Not a bad start to the day."


translate belarusian day1_1c924fd9:


    th "I have absolutely no clue where I am, besides that, there are some kids here roleplaying as pioneers."


translate belarusian day1_3f084620:


    th "And, as far as I can tell, this place is located thousands of kilometres away from my home.{w} It might even be a different reality."


translate belarusian day1_b8844662:


    "And this was indeed a {i}reality{/i}..."


translate belarusian day1_bd1b7173:


    "I mean, everything around me seemed so real (if a little embellished) that I was starting to think that in fact my previous life could have been just a dream."


translate belarusian day1_947b07b9:


    th "And what am I supposed to do now?"


translate belarusian day1_a20cefa7_5:


    "..."


translate belarusian day1_a48ce857:


    "I was picking at the cracked paving stones with my shoe and staring aimlessly at the 'Clubs' building."


translate belarusian day1_52f502cd:


    th "Just few more seconds before I have to come up with some decision."


translate belarusian day1_6f416834:


    "That’s when I recalled myself rolling on the grass, weeping..."


translate belarusian day1_487a7790:


    "I cringed in disgust."


translate belarusian day1_34312b93:


    "Perhaps it's another instinct: when all energy for whimpering and self-pitying is used up, the body either goes into hibernation or mobilises its reserves."


translate belarusian day1_8ff0ae14:


    "Mine seemed to have chosen the second option, because out of the blue, I found the determination to figure out what was going on."


translate belarusian day1_6aea12ff:


    "And in order to do that, I had to act like a man – like a human: to maintain the dignity of a representative of {i}my own world{/i}."


translate belarusian day1_267d5adc:


    "I followed the path to the left, on the right side of which stood small cabins, apparently the homes of the local pioneers."


translate belarusian day1_772090f6:


    "Actually, they looked quite cozy from the outside."


translate belarusian day1_5ff36d96:


    "Even though I was born in the Soviet Union, I had never been part of its childrens organisations, neither the Pioneers nor even the younger October Children."


translate belarusian day1_d869f630:


    "I imagined the daily life of a typical pioneer camp a bit differently: huge barracks with long rows of metal bunks, wake-up call at six o'clock played by a siren, one minute to make your bed, then joining the formation at the drill square..."


translate belarusian day1_3a1335b5:


    th "Or wait!{w} Could I be confusing it with something else...?"


translate belarusian day1_9eee971e:


    "Suddenly something struck me on the back!"


translate belarusian day1_bb359f8d:


    "I staggered but remained on my feet, turned around and prepared to fight heroically for my life..."


translate belarusian day1_57666fa8:


    "But all I found was another girl standing before me."


translate belarusian day1_ee51804d:


    "My mouth hung open in surprise."


translate belarusian day1_68b22888:


    dvp "Pick your jaw up off the ground."


translate belarusian day1_143f62c0:


    "I closed my mouth."


translate belarusian day1_c1b4c8b4:


    "The same pioneer uniform, but the way she was wearing it looked, let’s say, provocative."


translate belarusian day1_f689de87:


    "Like all the girls I had met here before, this one was rather cute, but her overly arrogant expression killed any desire to get to know her better."


translate belarusian day1_5ecab483:


    dvp "New here, are you?"


translate belarusian day1_51149e68:


    me "…"


translate belarusian day1_dc8deb21:


    dvp "Fine, see ya!"


translate belarusian day1_df07b66c:


    "She threw a threatening glance at me and walked past."


translate belarusian day1_be998354:


    "I waited for the pioneer girl to turn at the corner...{w} Who knows what else she might have been up to!"


translate belarusian day1_310f2718:


    "The most interesting thing was that even this hostile girl seemed completely normal to me, she did not give off the feeling of some deadly danger."


translate belarusian day1_dbdad37c:


    "Except maybe the danger of getting punched in the nose."


translate belarusian day1_78546616:


    "At last, I managed to make it to the square."


translate belarusian day1_79587a2a:


    "There was no Lenin on an armoured car, although one could easily expect something like that after all this had happened."


translate belarusian day1_b624fcdd:


    "Instead, however, a monument to a certain comrade towered in the middle of the square. The letters on the pedestal read 'GENDA'."


translate belarusian day1_4f2ef086:


    th "Must be some big figure in the Party."


translate belarusian day1_2f3a0b1e:


    "There were some small benches at the sides."


translate belarusian day1_f324eeda:


    th "It's quite pleasant here."


translate belarusian day1_9d30be31:


    th "Where did that girl tell me to go?{w} To the left or to the right?"


translate belarusian day1_c6de1218:


    th "To the left, to the right, to the left, to the right…"


translate belarusian day1_58ada7be:


    th "And why am I going there anyway...?"


translate belarusian day1_bd699a39:


    th "Ah, right, I've decided to pretend to be normal..."


translate belarusian day1_833eb5f3:


    th "So, to the right!"


translate belarusian day1_4870b408:


    "Through a small grove..."


translate belarusian day1_be1bac02:


    "I came out at a pier."


translate belarusian day1_66caa1a6:


    th "I must have taken a wrong turn."


translate belarusian day1_a02d94f5:


    slp "Hey, wrong way!"


translate belarusian day1_0a8fd9cf:


    "I turned towards the voice."


translate belarusian day1_ea065f04:


    "That first girl stood before me."


translate belarusian day1_4725a971:


    slp "Now what did I tell you? Take a left at the square, wasn't it?"


translate belarusian day1_59abc400:


    "She had changed from her pioneer uniform into a bikini."


translate belarusian day1_e85f6b79:


    slp "Oh, I still haven't introduced myself!{w} My name is Slavya!"


translate belarusian day1_2fa68520:


    slp "Actually, my full name's Slavyana, but everyone calls me Slavya.{w} So you can too!"


translate belarusian day1_6e37a835:


    me "Uhm... yeah..."


translate belarusian day1_0cc735df:


    "I still felt a bit confused, so I could not come up with more meaningful answers."


translate belarusian day1_4c5321be:


    sl "And what's your name?"


translate belarusian day1_c5787aa3:


    "It felt like she could see right through me."


translate belarusian day1_5cf26729:


    me "Uhm... I... yeah... Semyon..."


translate belarusian day1_e2552123:


    sl "Nice to meet you, Semyon."


translate belarusian day1_a89521fc:


    sl "All right, I'm almost done here.{w} Could you wait here a minute, I'm going to change and we'll go to Olga Dmitrievna together, agreed?"


translate belarusian day1_65bf82fc:


    me "Agreed..."


translate belarusian day1_43560fad:


    "After this exchange she ran off. I sat on the pier and let my feet hang into the water."


translate belarusian day1_90942777:


    "I was wearing heavy winter boots, but in such weather there was nothing wrong in getting my feet wet."


translate belarusian day1_5c4ff44b:


    "Furthermore, it let me cool myself a little."


translate belarusian day1_ea94e5d1:


    "Looking at the river, I was brainstorming and processing everything that has happened."


translate belarusian day1_24d93bb8:


    th "If this is some kind of conspiracy, it's a weird one, even too friendly at times."


translate belarusian day1_5edac208:


    th "No, it really looks more like a random event."


translate belarusian day1_044eac6b:


    th "Some entirely incomprehensible random event."


translate belarusian day1_a733d584:


    sl "Shall we go?"


translate belarusian day1_3d8e6458:


    "Slavya was standing beside me, dressed in the pioneer uniform again."


translate belarusian day1_85ccdc6d:


    me "Let's go…"


translate belarusian day1_1ea0d875:


    th "I've been here for a very short time, but of all the people I've met, she looks the least suspicious."


translate belarusian day1_efe66139:


    th "However, this fact is already suspicious itself!"


translate belarusian day1_b3e9120f:


    "We walked to the square."


translate belarusian day1_73d6913a:


    "The 'USSR' girl and the girl who hit me on the back were there, chasing each other."


translate belarusian day1_805a8066:


    th "Is that some kind of game they’re playing?"


translate belarusian day1_6c5b8d8e:


    sl "Ulyana, enough running! I'll tell everything to Olga Dmitrievna!"


translate belarusian day1_e2f0dae9:


    us "Aye-aye, captain!"


translate belarusian day1_0bfd5bdf:


    "I decided not to question Slavya for a while about what was going on or the local residents."


translate belarusian day1_5280f6b1:


    th "Better meet with this mysterious Olga Dmtrievna first."


translate belarusian day1_1cd246d8:


    th "Sounds like she's the boss here."


translate belarusian day1_3337c244:


    "We walked past the rows of almost identical cabins, some of which looked like fat beer barrels, while others looked more like household sheds."


translate belarusian day1_598416c3:


    "Finally, Slavya stopped in front of a smallish one-storey cabin."


translate belarusian day1_4ce282c2:


    "It looked like an artist's painting: the faded paint, chipping here and there with age, was sparkling in the sun; the window shutters, slightly ajar, were swaying almost unnoticeably in the wind; and huge lilac bushes were growing at the sides."


translate belarusian day1_9ba44673:


    "It seemed as if this ramshackle hut was drowning in a storm of purple silk, and the lilacs, like some elemental force, were inexorably destroying the troop leader's house."


translate belarusian day1_3dc6042e:


    sl "What are you standing around for? Let's go!"


translate belarusian day1_cc0015de:


    "Slavya snapped me out of my daydreaming."


translate belarusian day1_25a8e409:


    mt_voice "... and stop teasing Lena already..."


translate belarusian day1_23aa893b:


    "Rena?!"


translate belarusian day1_368e2a32:


    th "Sounds like there's someone inside."


translate belarusian day1_42c5f640:


    "Indeed, a moment later the door swung open, and Ulyana ran out and dashed past with the same mischievous grin."


translate belarusian day1_9b6e58bf:


    "The pigtailed girl came out next."


translate belarusian day1_01fca3c6:


    sl "Don't let it bother you, Lena!"


translate belarusian day1_0d446d18:


    th "So her name is Lena...{w} Gotta be thankful it's not Rena, at least!"


translate belarusian day1_92afc216:


    un "But I don't..."


translate belarusian day1_a3f0580d:


    "Instead of finishing her sentence, she blushed and quickly headed towards the square."


translate belarusian day1_a653101d:


    "For some reason I felt like turning and following her with my eyes, but Slavya said:"


translate belarusian day1_84aaac50:


    sl "Come."


translate belarusian day1_9e8e7cd3:


    "We entered the cabin."


translate belarusian day1_4a8e191a:


    "Inside it looked something similar to what I'd imagined: two beds, a table, a couple of chairs, a simple carpet on the floor, a wardrobe."


translate belarusian day1_c87f01c5:


    "Nothing special, but at the same time it felt homelike and cosy, although this room was almost in the same state of disorder as my own flat."


translate belarusian day1_f9471bdb:


    "The girl standing near the window appeared to be about twenty five years old."


translate belarusian day1_1529b84e:


    th "Nature had obviously gifted her with a pretty face and a good body..."


translate belarusian day1_0aa01509:


    th "At least one thing can keep you happy in this pandemonium: the locals are beautiful."


translate belarusian day1_80b7fab7:


    mtp "You're finally here!{w} Excellent!{w} My name is Olga Dmitrievna, I'm the camp leader."


translate belarusian day1_81f67e23:


    me "Nice to meet you, I'm Semyon."


translate belarusian day1_2a6869f0:


    "I decided to talk as if I wasn't surprised by anything that was going on."


translate belarusian day1_b93477a9:


    "She came closer."


translate belarusian day1_9a812b52:


    mt "We've been expecting you since early morning."


translate belarusian day1_b7364cea:


    me "You've been expecting me...?"


translate belarusian day1_ca77693f:


    mt "Yes, of course!"


translate belarusian day1_dda106bc:


    me "And when does the next bus come, 'cause I... "


translate belarusian day1_fec151d0:


    mt "And why do you need it?"


translate belarusian day1_8f1ac2ed:


    th "Yeah, right, why would I need it...?"


translate belarusian day1_5a1280bd:


    th "Guess I shouldn't ask direct questions: the people here may react to them quite unlike how I'd prefer."


translate belarusian day1_79c3f7b2:


    th "And I doubt I'd get any answers."


translate belarusian day1_8d50c9cf:


    me "No reason, just curious..."


translate belarusian day1_1725efc3:


    me "By the way, where are we exactly?{w} Our mailing address, I mean?"


translate belarusian day1_21adf436:


    me "I wanted to send a letter to my parents to tell them I got here fine."


translate belarusian day1_efb897d9:


    "For some reason, I had the desperate idea that if I played along, I would find something out."


translate belarusian day1_5a58e925:


    mt "Oh, but your parents called just half an hour ago! Sent their regards to you."


translate belarusian day1_41180a2e:


    th "Now that's a surprise..."


translate belarusian day1_350d74d5:


    me "So, can I call them? 'Cause I forgot to tell them something before I left."


translate belarusian day1_44f6bf31:


    mt "No."


translate belarusian day1_9d48f5b9:


    "She gave me a sweet, spontaneous smile."


translate belarusian day1_9bddf507:


    me "Why not?"


translate belarusian day1_c40fd752:


    mt "We don't have a phone here."


translate belarusian day1_51149e68_1:


    me "…"


translate belarusian day1_729f518a:


    me "Then how could my parents make a call to here?"


translate belarusian day1_120ecdbc:


    mt "I've just come back from the district's central town, I talked with them there."


translate belarusian day1_434cc4a4:


    th "Ah, so that's how it is?"


translate belarusian day1_68be55cd:


    me "And can I somehow get to the town?"


translate belarusian day1_44f6bf31_1:


    mt "No."


translate belarusian day1_89d38d04:


    "She kept the same smile."


translate belarusian day1_5ac38cf1:


    me "Why not?"


translate belarusian day1_06cd3c2d:


    mt "Because the next bus only comes in a week."


translate belarusian day1_a3268632:


    "I decided not to inquire how the troop leader managed to get there and back; I would get no answer anyway."


translate belarusian day1_bd84df53:


    "All this time, Slavya was standing next to me and seemed to find nothing odd in our conversation."


translate belarusian day1_869d68e4:


    mt "Oh, we need to find a uniform for you!"


translate belarusian day1_75c6d686:


    th "I've got absolutely no desire to put on pioneer shorts or to wear the ridiculous red neckerchief!"


translate belarusian day1_94a1d364:


    th "However, wearing winter clothes in summer isn't a great idea either."


translate belarusian day1_f4df3367:


    me "Right, thank you…"


translate belarusian day1_00ec5ab7:


    th "I wonder if I'm the only one here who finds it strange that someone's wearing a coat and winter boots in such heat?"


translate belarusian day1_13e0bfb3:


    mt "Righty-o, I'll be off then, and you can get yourself acquainted with the camp!{w} Don't forget to come to dinner in the evening!"


translate belarusian day1_8af1d414:


    "Having said that, she walked out of the cabin."


translate belarusian day1_3be70f85:


    "No, 'walked' is not the right word- she rushed out."


translate belarusian day1_733c1098:


    "I ended up alone with Slavya."


translate belarusian day1_4c0d1dce:


    sl "I must go too, got things to do."


translate belarusian day1_0a470c4f:


    sl "Take a walk, look around the camp a bit.{w} See you in the evening."


translate belarusian day1_d47d9534:


    th "If there is no threat or catch to this then this reality, as embodied by Slavya, becomes more and more appealing."


translate belarusian day1_a20cefa7_6:


    "..."


translate belarusian day1_a28a58fe:


    "For the first time today I realised that it was awfully hot here."


translate belarusian day1_3aa2905a:


    "Although, obviously, my winter clothes were to blame for that."


translate belarusian day1_6224b6ad:


    "I took off my coat and dropped it onto the bed, my pullover followed it; I was now wearing only the shirt."


translate belarusian day1_1302b288:


    th "That's much better!"


translate belarusian day1_0650308f:


    "All I could do now was follow their advice and go look around the camp."


translate belarusian day1_faf01b13:


    th "I'll try to find something out in the meantime."


translate belarusian day1_abe359aa:


    "Passing the local 'residential district', I saw a pioneer guy coming my way."


translate belarusian day1_19166f7e:


    "And it was really a pioneer guy, not a pioneer girl. Apparently there were men even in this kingdom of Amazons."


translate belarusian day1_6e392819:


    elp "Hello, you're new here, you must be Semyon, right?"


translate belarusian day1_f4234049:


    me "And how do you...?"


translate belarusian day1_eb3ac837:


    elp "Everyone knows already! I'm Electronik, by the way. The real one. You can call me that."


translate belarusian day1_1ab8b0e8:


    th "Electronik. The real one. Things were going from 'crazy' to 'completely insane'."


translate belarusian day1_f2e007f0:


    me "All right..."


translate belarusian day1_13475f83:


    el "Ulyana also calls me Cheesie."


translate belarusian day1_f005cc88:


    me "Cheesie?"


translate belarusian day1_dd47efcd:


    th "On toast, with mushrooms?"


translate belarusian day1_8966f24f:


    el "Because my last name is Cheesekov."


translate belarusian day1_f2e007f0_1:


    me "I see..."


translate belarusian day1_ab73c55e:


    el "Let me show you around!"


translate belarusian day1_c45870d5:


    "I accepted his offer, as it would have taken much longer to get to know this place alone."


translate belarusian day1_901746cb:


    me "Fine, let's go."


translate belarusian day1_e555f27e:


    "We ended up at the square again."


translate belarusian day1_6bd8e03f:


    th "As if this place was all there is to this camp..."


translate belarusian day1_ac68c5e6:


    "Lena was sitting on one of the benches, reading some book.{w} Electronik confidently approached her."


translate belarusian day1_03a7db73:


    el "Hello, Lena! Meet the new guy, Semyon."


translate belarusian day1_ebd7111d:


    "He started briskly."


translate belarusian day1_f37aca17:


    me "Hello.{w} Well, you could say we've already met, in a way."


translate belarusian day1_31b01f67:


    un "Yes…"


translate belarusian day1_51f9c6e8:


    "She looked away from the book for a moment, glanced at me, blushed and went back to reading, as if not noticing that we were still here."


translate belarusian day1_0e6973ff:


    el "All right, let's go on."


translate belarusian day1_e71595cb:


    "I was at first surprised that 'meeting' this girl was reduced to a couple of words, but then I thought that it was better that way.{w} Electronik's vigorous activity did not fit well with Lena's shyness."


translate belarusian day1_304c9144:


    me "Let's go."


translate belarusian day1_d2c497e2:


    "Next we came to a building, which I instantly identified as a canteen."


translate belarusian day1_ac073a3b:


    el "And this here..."


translate belarusian day1_e84fdadf:


    me "I know! This is where you consume organic food!"


translate belarusian day1_06114188:


    el "Yeah, something like that…"


translate belarusian day1_4eab803f:


    "On the canteen's porch stood the unfriendly girl who hit me on the back earlier."


translate belarusian day1_2cd75b17:


    "At the sight of her, my joking mood vanished in a blink of an eye."


translate belarusian day1_2eef73b2:


    th "Really, now is not the best time to be pulling this guy's leg, even though he is quite hilarious."


translate belarusian day1_88b653a6:


    th "First I need to figure out what's what here, or at least where I am!"


translate belarusian day1_84bfeb9f:


    el "Her, over there, that's Alisa Dvachevskaya. Be careful around her."


translate belarusian day1_36dc9fcc:


    "He spoke in a whisper."


translate belarusian day1_c065e234:


    el "Don't ever risk calling her DvaCheh, she doesn't like that!"


translate belarusian day1_242c3a55:


    dv "What did you say? What did you call me?"


translate belarusian day1_c941236c:


    "She must have heard him."


translate belarusian day1_7cbd6d0d:


    "In the blink of an eye, Alisa jumped down from the porch and dashed towards us."


translate belarusian day1_7810c210:


    el "All right, you'll manage from here onwards..."


translate belarusian day1_8a6dfaab:


    "Electronik took to his heels."


translate belarusian day1_68ae01db:


    "I decided that I had no wish to come across this aggressive Alisa girl again and darted after Electronik."


translate belarusian day1_87908dda:


    "Running to the square, I lost sight of him."


translate belarusian day1_f5ecd77a:


    "But Dvachevskaya wasn't chasing me either."


translate belarusian day1_efa7a730:


    th "However, I really should not risk calling her that.{w} Even in my mind..."


translate belarusian day1_d436642f:


    "After catching my breath, I wondered why I had reacted like that."


translate belarusian day1_50af9809:


    th "All right, she is a girl... an aggressive one...{w} But why run away?"


translate belarusian day1_cd458724:


    "Failing to find a good answer, I sat down on a bench and stared at the monument to Genda."


translate belarusian day1_2d7b339e:


    "Alisa, running past, stopped for a moment and growled:"


translate belarusian day1_1823fa4f:


    dv "I'll deal with you later!"


translate belarusian day1_35feebce:


    me "Deal with me? What did I do wrong!?"


translate belarusian day1_7eff5e99:


    "I added a forced guilty smile to my words."


translate belarusian day1_8fa3ff1e:


    th "But what am I guilty of...?"


translate belarusian day1_9dbfd6f2:


    "She made no reply and carried on chasing Electronik."


translate belarusian day1_a20cefa7_7:


    "..."


translate belarusian day1_eb85f5b2:


    th "Looks like I'll have to kill time alone, waiting for dinner."


translate belarusian day1_c1af6492:


    "I decided to go east.{w} At least in the direction where east would be in my world."


translate belarusian day1_990e43f3:


    "Soon after, I found myself near a football pitch."


translate belarusian day1_55a5886e:


    "A game was in full swing there."


translate belarusian day1_6e3f39b7:


    th "I guess watching it for a bit wouldn’t do any harm."


translate belarusian day1_80a62586:


    "In my childhood and teen years I was not a bad player myself and even thought of going professional, but a few injuries in a row killed my desire to risk my health for the sake of uncertain chance in the game."


translate belarusian day1_ac5ae99c:


    "Kids of different ages were running around the pitch: I could see a boy of about ten and a girl of about fourteen years old..."


translate belarusian day1_f462954c:


    th "A girl...{w} Hey, that's Ulyana!"


translate belarusian day1_5e68b29c:


    th "All right, so she plays football - what's so surprising?"


translate belarusian day1_1ae034aa:


    th "She seems a restless one, after all."


translate belarusian day1_0286167f:


    "I was standing quite far from the pitch, but she still noticed me."


translate belarusian day1_f85a8374:


    us "Hey, you!"


translate belarusian day1_77bd833d:


    "Ulyana shouted."


translate belarusian day1_afc02b59:


    us "Wanna play?"


translate belarusian day1_3f27c2c0:


    "I didn't know how to answer."


translate belarusian day1_7fb785ba:


    th "On the one hand, running around for ten minutes is no big deal."


translate belarusian day1_b2114442:


    th "On the other hand, any wrong move in my situation could be my final one."


translate belarusian day1_be98ba5d:


    "But in any case, my attire wasn't suitable for this weather."


translate belarusian day1_0b1fdc5d:


    "If I played in winter boots and warm jeans, I would sweat like a pig."


translate belarusian day1_16639bbb:


    th "And playing barefoot and without jeans would be simply unethical."


translate belarusian day1_91f30c65:


    me "Maybe another time, all right?"


translate belarusian day1_1cdbc40f:


    "I shouted in response, turned around and walked back."


translate belarusian day1_317df2d8:


    "I was followed by Ulyana's screams, about my pants, or about me being a pansy, or something like that..."


translate belarusian day1_a20cefa7_8:


    "..."


translate belarusian day1_4b9550bc:


    "Evening was falling, making me feel tired and empty after a day wasted with no real purpose."


translate belarusian day1_a20cefa7_9:


    "..."


translate belarusian day1_2d76a94d:


    "I came back to the square, sat down on a bench and gave an exhausted sigh."


translate belarusian day1_c7f14958:


    th "I'd better sit here and wait for dinner.{w} After all, it's easier to search for answers when you're not hungry."


translate belarusian day1_6face009:


    th "They do give food to the people here, right...?"


translate belarusian day1_6efc8181:


    th "You know, it's curious how the simplest human needs can break the will to ponder on things, to strive for something."


translate belarusian day1_8098381c:


    th "For example, I feel hungry now, so I care much less about where I am or what is happening to me."


translate belarusian day1_8e0062ff:


    th "Could great people also be affected by this?"


translate belarusian day1_11ca5f98:


    th "And in that case, how did Spartacus start the slave uprising in ancient times...?"


translate belarusian day1_6d0083af:


    th "I can only conclude that I am not a great person, and it does not really matter which mechanism I serve as a gear in: society, the Matrix, or a weird pioneer camp."


translate belarusian day1_31836b32:


    "My thoughts were interrupted by the sound of bells chiming from a loudspeaker on a light pole."


translate belarusian day1_bbaa6f9b:


    th "It must be the dinner call!"


translate belarusian day1_d1afa489:


    "I headed towards the canteen; it was a good thing that now I knew where it was."


translate belarusian day1_60f3a365:


    "Olga Dmitrievna was there, standing on the porch."


translate belarusian day1_3181ce0b:


    "I stopped and looked closely at her, as if I were expecting something."


translate belarusian day1_620995d5:


    "She looked back at me for a while, but at last came closer."


translate belarusian day1_e4e46775:


    mt "Semyon, what are you waiting for? Come in already!"


translate belarusian day1_3db38509:


    th "Guess nothing bad can happen if I go with her."


translate belarusian day1_8646eca1:


    "My stomach backed me up here."


translate belarusian day1_20d3b7df:


    "The two of us went inside."


translate belarusian day1_d5cccf64:


    "The canteen looked like...{w} a canteen."


translate belarusian day1_5a204f67:


    "I'd had a chance to visit a factory canteen at some point in my life...{w} This one was exactly the same, just maybe a bit cleaner and more modern."


translate belarusian day1_7f269d4e:


    "Metal chairs and tables, glazed tiles on the walls and on the floor, unsophisticated tableware with the occasional crack."


translate belarusian day1_2b4a4a89:


    th "Guess that's what a canteen in a pioneer camp supposed to look like."


translate belarusian day1_865ed000:


    mt "Semyon, wait a moment, we'll find you a place to sit…"


translate belarusian day1_fed0ccad:


    "She looked around the place."


translate belarusian day1_cbc89c54:


    mt "Dvachevskaya, hold it right there!"


translate belarusian day1_bccd2e3b:


    "Olga Dmitrievna shouted at Alisa who was passing by."


translate belarusian day1_e0ebb201:


    dv "What?"


translate belarusian day1_686395a7:


    mt "What's up with your clothes?"


translate belarusian day1_f893781b:


    dv "Anything wrong with them?"


translate belarusian day1_6c59a28a:


    "Indeed, her attire looked somewhat provocative..."


translate belarusian day1_7f4c6c16:


    mt "Get your uniform nice and neat right now!"


translate belarusian day1_aa5269ba:


    dv "All right, all right..."


translate belarusian day1_02da0b4a:


    "Alisa got her shirt right and walked past, shooting an unpleasant glare at me."


translate belarusian day1_a1e6af5d:


    mt "So, where can we find you a place to sit?"


translate belarusian day1_30ea3e74:


    "There weren't a lot of free seats."


translate belarusian day1_986412cf:


    mt "Go over there, next to Ulyana!"


translate belarusian day1_2cfe56a5:


    me "Uhm... Maybe I..."


translate belarusian day1_385a7396:


    mt "Yeah, it's fine, the food's already on the table, too!"


translate belarusian day1_07918b6c:


    "I had no other choice but to accept."


translate belarusian day1_ff014804:


    "Of course, there was the probability that the cutlets were poisoned with curare, the mashed potatoes generously seasoned with arsenic, and the glass filled with excellent antifreeze instead of kompot..."


translate belarusian day1_1dcade22:


    "But it all looked so tasty that I had no chance to resist!"


translate belarusian day1_d5d98923:


    us "Hey!"


translate belarusian day1_087d8e36:


    me "Whaddaya want?"


translate belarusian day1_4b969ae9:


    "I replied rather rudely to Ulyana, who was sitting next to me."


translate belarusian day1_bcac0437:


    us "Why didn't you play football with us?"


translate belarusian day1_ee0ece72:


    me "Because of my clothes."


translate belarusian day1_ac6bee46:


    "Said I, pointing at the source of the problem."


translate belarusian day1_762f9dc6:


    us "Oh, all right then, eat."


translate belarusian day1_71c21715:


    "However, there wasn't much left to eat: my cutlet was missing from the plate!"


translate belarusian day1_2623f571:


    th "Only she could have done it."


translate belarusian day1_f9c55ec5:


    th "No, more precisely, none but Ulyana could have done it!"


translate belarusian day1_58e7978c:


    me "Give me back my cutlet!"


translate belarusian day1_0f31d85b:


    us "In a big family, you snooze, you lose!{w} It can cost you a cutlet if you are careless!"


translate belarusian day1_ea7db8e5:


    me "Give it back, I'm telling you!"


translate belarusian day1_3e16ffc9:


    "I looked at her menacingly and was about to reach out my hand..."


translate belarusian day1_39e0f78f:


    us "See, I don't have it!"


translate belarusian day1_8fb93759:


    "And indeed, Ulyana's plate was empty; it seemed that this little girl eats as fast as she steals someone’s cutlets."


translate belarusian day1_bf4d7f64:


    us "Take it easy, we'll work something out now!"


translate belarusian day1_c2895463:


    "She grabbed my plate and ran off."


translate belarusian day1_4c2a1209:


    "There was no point in following her: if they wanted to poison me here, they could have done it in a much easier way."


translate belarusian day1_4bb06742:


    "About a minute later, Ulyana returned and handed me the plate with a steaming hot cutlet on it."


translate belarusian day1_39cd2cfa:


    us "Here's one for the starving!"


translate belarusian day1_73aab3cd:


    me "Thanks…"


translate belarusian day1_dfe4386e:


    "It was all I could say."


translate belarusian day1_e425e9ee:


    "I was so hungry that my suspicions were gone in a flash.{w} I picked up the cutlet with my fork and..."


translate belarusian day1_2158fc2f:


    th "What the!?{w} Some bug!{w} No, not a bug!{w} An insect!{w} It got legs and it's wiggling!"


translate belarusian day1_27e62654:


    "The plate fell to the floor and broke into pieces; the chair hit me hard on my leg while falling."


translate belarusian day1_c1ea30fa:


    "I've disliked insects since I was a child, but when these creepy-crawlies appear in my plate, that's just way too much!"


translate belarusian day1_670b85c3:


    me "You little..."


translate belarusian day1_cdba60b2:


    "Ulyana seemed ready for such a twist and was already at the door, laughing as if she had just heard a fresh stand-up comedy joke."


translate belarusian day1_a0076c62:


    "I dashed after her."


translate belarusian day1_4a7f4c33:


    "We ran out of the canteen."


translate belarusian day1_51f77987:


    "We were just a few dozen metres apart, and I felt I would catch this little girl easily."


translate belarusian day1_c0e0bceb:


    "We ran through the square..."


translate belarusian day1_8d3b0cc2:


    "Past the clubs' house..."


translate belarusian day1_f7567198:


    "And ran onto the forest path."


translate belarusian day1_d50859c6:


    "I started to gasp for breath."


translate belarusian day1_80f73680:


    th "I should have quit smoking I guess…"


translate belarusian day1_1178684b:


    "Ulyana passed out of sight on the next turn."


translate belarusian day1_b24a1f41:


    th "It can't be true that she managed to get away from me!"


translate belarusian day1_c255b412:


    th "It simply can't!"


translate belarusian day1_7a8d7835:


    "I stopped and tried to catch my breath again."


translate belarusian day1_a20cefa7_10:


    "..."


translate belarusian day1_fd3eb8d2:


    "Night was falling."


translate belarusian day1_b570941e:


    th "Looks like I'm lost…"


translate belarusian day1_bce9bc24:


    th "It's a bad idea to stay in the woods at night; better get back to the camp."


translate belarusian day1_bbe1aefc:


    "However, I had absolutely no clue which way to go."


translate belarusian day1_985a9376:


    th "Well, gotta choose at random."


translate belarusian day1_a20cefa7_11:


    "..."


translate belarusian day1_a20cefa7_12:


    "..."


translate belarusian day1_f8583ab1:


    "I wandered for quite some time in the forest and even thought of crying for help, but finally I saw the camp's fence beyond the trees."


translate belarusian day1_7ad8ae02:


    th "Everything fell back into place."


translate belarusian day1_7f4df294:


    me "The bus is gone..."


translate belarusian day1_6cfccad8:


    "I mumbled quietly."


translate belarusian day1_dbfa4d22:


    "On the one hand, there was nothing strange about that: the bus couldn't just stay there forever."


translate belarusian day1_42ed0653:


    "On the other hand, it meant there was someone driving, because buses do not drive themselves!"


translate belarusian day1_dcc5269b:


    th "Or do they?"


translate belarusian day1_ba1dfa38:


    "This world seemed too normal, but every event here had at least two explanations for it: an ordinary, real, everyday explanation, and a surreal one."


translate belarusian day1_b678b61b:


    "Certainly, the driver could have just been off for a snack, and I left too soon, and that's why..."


translate belarusian day1_ec1d1437:


    th "In any case, this is not the place for me!"


translate belarusian day1_9aa1fa90:


    "Whether that bus drives itself or not was probably an important question, but it was much more important to know how I had got here at all."


translate belarusian day1_2b9ab305:


    "And where this {i}here{/i} was..."


translate belarusian day1_5aa31516:


    "The fields and the woods, stretching towards the horizon, had no answers; there was nothing familiar about them."


translate belarusian day1_a20cefa7_13:


    "..."


translate belarusian day1_13255b43:


    "A strange, odd, and alien world.{w} However, at the same time it was absolutely not frightening."


translate belarusian day1_c83e85c5:


    "Either my self-preservation instinct decided to resign from its job, or all this running around the camp and the local pioneers had lulled me so much with their carefree normality that I was simply forgetting what had happened to me just a couple of hours ago."


translate belarusian day1_e9c8c9dc:


    "Although I probably just had no strength left to worry."


translate belarusian day1_ab64ceb2:


    "All I wanted was some peace, calmness; I wanted to just have a break from it all, and only after that would I continue looking for answers, ready and reloaded."


translate belarusian day1_c260705c:


    "However, that would be some time later..."


translate belarusian day1_5cfa0252:


    th "And what about now? Can I allow myself to relax?"


translate belarusian day1_a20cefa7_14:


    "..."


translate belarusian day1_b2e9fb9d:


    "It got completely dark, and in any case it was better to spend the night in the camp."


translate belarusian day1_46aa5cbc:


    "I was about to head back when someone came up silently from behind."


translate belarusian day1_ac5eb104:


    sl "Hello, what are you doing here so late?"


translate belarusian day1_51149e68_2:


    me "..."


translate belarusian day1_245b70c4:


    "It was Slavya standing before me.{w} I was so surprised that I jumped."


translate belarusian day1_ec2df0cc:


    sl "So, you didn't catch Ulyana, did you?"


translate belarusian day1_d1046a49:


    "She smiled."


translate belarusian day1_5e1fc6b8:


    "I nodded disappointedly and sighed."


translate belarusian day1_dd08f845:


    sl "No wonder.{w} No one ever has."


translate belarusian day1_476d94f1:


    th "Yeah, she's a real rocket girl.{w} She could have found a better use for her energy than looking for adventures..."


translate belarusian day1_d1722b3c:


    sl "You must be hungry, you didn’t manage to have dinner after all..."


translate belarusian day1_d0dae46d:


    "Indeed, I had completely forgot about my hunger, but after these words of hers my stomach drew attention to itself by giving a traitorous rumble."


translate belarusian day1_c17a722d:


    "Slavya smiled."


translate belarusian day1_4cbb01a1:


    sl "Let's go then."


translate belarusian day1_e01dee92:


    me "What, is the canteen still open?"


translate belarusian day1_40c17de2:


    sl "It's all right, I have the keys."


translate belarusian day1_36955eb7:


    me "The keys?"


translate belarusian day1_ab83b32e:


    sl "Yes, I have keys to all the facilities in this camp."


translate belarusian day1_9080c90e:


    me "How come?"


translate belarusian day1_ca97714d:


    sl "Well, I'm something like the camp leader's aide here."


translate belarusian day1_7e4b1b8c:


    me "I see.{w} Well, let's go."


translate belarusian day1_63a3eb4c:


    "It was an offer you can’t refuse."


translate belarusian day1_00f9254f:


    "When we reached the square, Slavya stopped in her tracks."


translate belarusian day1_7f5d338c:


    sl "Excuse me, I should warn my roommate that I'll be late; she's so punctual herself that she'll be worried otherwise."


translate belarusian day1_518df0ed:


    sl "You go on to the canteen, and I'll come in a minute, alright?"


translate belarusian day1_505d91ae:


    me "Alright..."


translate belarusian day1_32ac72cc:


    "I really did not expect to find somebody aside from myself there at such a late hour."


translate belarusian day1_b6cef34a:


    "And that somebody was apparently trying hopelessly to open the door."


translate belarusian day1_18f36ecf:


    "Without any secret thoughts, I walked up onto the porch."


translate belarusian day1_d706aa5c:


    "The lock picker turned out to be Alisa."


translate belarusian day1_e81739e5:


    th "I should have probably kept off and waited... "


translate belarusian day1_bbe1ef0f:


    "She looked at me intently for a while, then said:"


translate belarusian day1_a872d369:


    dv "Don't just stand there, give me a hand or something!"


translate belarusian day1_e54b39e9:


    me "Meaning?"


translate belarusian day1_9736714c:


    dv "Help me open the door!"


translate belarusian day1_7332342b:


    me "Why?"


translate belarusian day1_c67591a7:


    dv "'Cause I want some buns and kefir! Dinner wasn't big enough!"


translate belarusian day1_8d3f7a19:


    me "Uhmmm...{w} Is that really a good idea?"


translate belarusian day1_1381f7ca:


    dv "Aren't you hungry yourself, huh?{w} Ulyana didn't let you have a normal dinner, did she?"


translate belarusian day1_f396e777:


    "She smiled sarcastically."


translate belarusian day1_ad73a68c:


    th "It's true, she didn't..."


translate belarusian day1_c43ed392:


    me "It's fine, Slavya will come now and..."


translate belarusian day1_c8b0074b:


    dv "WHAT?!"


translate belarusian day1_ca8046cd:


    th "Guess I shouldn't have said that."


translate belarusian day1_e7b745f9:


    dv "I'm off then!"


translate belarusian day1_5f47ed77:


    dv "And you'll pay for this!{w} You owe me two already!"


translate belarusian day1_a180b25b:


    "Having said that, Alisa disappeared into the night."


translate belarusian day1_efb5548d:


    th "And what was the first one?"


translate belarusian day1_a20cefa7_15:


    "..."


translate belarusian day1_696c481c:


    "Slavya didn't keep me waiting for too long."


translate belarusian day1_8c608cb1:


    sl "Is everything alright?"


translate belarusian day1_d5cf1edd:


    me "Yeah, why are you asking?"


translate belarusian day1_18938c76:


    sl "No reason, it's nothing."


translate belarusian day1_dc129c74:


    "It would be better if I didn't tell her about Alisa."


translate belarusian day1_b5544ae3:


    me "Everything's fine."


translate belarusian day1_944a6c8e:


    "I said that and immediately heard a note of dishonesty in my voice."


translate belarusian day1_6d7067b9:


    sl "Well, shall we go?"


translate belarusian day1_3792d81a:


    "As for Slavya, she seemed not to have noticed anything.{w} Or at least she was pretending she hadn't."


translate belarusian day1_44f2c4ea:


    "We entered the canteen."


translate belarusian day1_c8f556b6:


    sl "Wait a bit, I'll go get something."


translate belarusian day1_d09b3412:


    "I sat down on a chair and obediently waited for my saviour."


translate belarusian day1_8775f75b:


    "My dinner was simple: a few buns and a glass of kefir."


translate belarusian day1_c6c983c8:


    th "No wonder: I bet hungry pioneers finished everything off."


translate belarusian day1_37e90611:


    "However, even that was far better than most of my usual diet."


translate belarusian day1_d5866868:


    "Slavya sat across the table and looked at me while I was eating."


translate belarusian day1_fbb73cfa:


    me "Is there something on my face?"


translate belarusian day1_178aa33f:


    sl "No, just…"


translate belarusian day1_d1046a49_1:


    "She smiled."


translate belarusian day1_dc9c1b81:


    sl "So, how did you like your first day in the camp?"


translate belarusian day1_6a05d51d:


    me "Well, I don't really know..."


translate belarusian day1_b12cf159:


    "It's silly to ask someone who suddenly found himself in a different reality whether he liked the food in the canteen, the camp leader, or his assigned hut."


translate belarusian day1_46db5ea6:


    sl "It's alright, you'll soon get used to it!"


translate belarusian day1_68e00554:


    "Slavya stared out the window dreamily."


translate belarusian day1_b05e0e4e:


    th "Frankly speaking, I have no desire to get used to such things; but as for her, she doesn't know..."


translate belarusian day1_b9ffb640:


    th "Or at least she wants me to think that she doesn't."


translate belarusian day1_2c50ec15:


    me "Well, all in all, it's nice here."


translate belarusian day1_443c7288:


    "I had to somehow break the awkward silence."


translate belarusian day1_17d13602:


    sl "Do you think so?"


translate belarusian day1_a271f10e:


    "She asked without any interest."


translate belarusian day1_1b986acc:


    me "Yeah. This place is so..."


translate belarusian day1_d4bc890b:


    "I wanted to say 'retro', but I managed to hold that back."


translate belarusian day1_edbbb7b2:


    "After all, it was retro for me, but what about them?{w} It might be the only kind of life they knew."


translate belarusian day1_78a8ca85:


    "If the term {i}life{/i} was applicable here at all..."


translate belarusian day1_789035fa:


    sl "So how?"


translate belarusian day1_00577787:


    "She watched me closely.{w} As if something important depended on my answer."


translate belarusian day1_7c2d154d:


    me "Well, I don't know... lovely. Yeah! It's lovely here."


translate belarusian day1_c1307b0b:


    sl "I guess you're right."


translate belarusian day1_78a2fe3f:


    "She smiled again."


translate belarusian day1_e349590f:


    sl "It's very good that you think so."


translate belarusian day1_5ac38cf1_1:


    me "Why?"


translate belarusian day1_17e99afb:


    sl "Well, not everybody likes it here..."


translate belarusian day1_1a377d44:


    me "And what about you?"


translate belarusian day1_1995267f:


    sl "Me?"


translate belarusian day1_fc30fd2b:


    me "Yes."


translate belarusian day1_031c751c:


    sl "I love it here, it's great."


translate belarusian day1_5c30029f:


    me "Then you don't need to worry about what other people think."


translate belarusian day1_879638a9:


    sl "Well, I don't really worry."


translate belarusian day1_c954d69f:


    "Slavya laughed."


translate belarusian day1_72c6e2b1:


    "This conversation seemed to be leading me far astray from where I wanted to get to."


translate belarusian day1_1a712a21:


    sl "And you're worried yourself..."


translate belarusian day1_1d09f697:


    me "Really? Why do you say so?"


translate belarusian day1_8b1f703e:


    sl "Well, when someone is chewing so intensely..."


translate belarusian day1_07129b8a:


    me "I'm sorry."


translate belarusian day1_705bea28:


    sl "It's okay."


translate belarusian day1_02cdc4c3:


    "I couldn't bring myself to be more cautious around this girl."


translate belarusian day1_ded79e77:


    "But why her in particular? Why not any other local inhabitant?"


translate belarusian day1_e53b428c:


    "Every one of them looked completely normal to me.{w} Precisely {i}normal{/i}, so normal it sends chills down my spine and into my marrow!"


translate belarusian day1_51e2809e:


    "Normal, not like a neighbor with a power drill in one hand and a subwoofer in the other."


translate belarusian day1_ab3da39e:


    "Not like a passenger you can often meet in a subway or on public transport."


translate belarusian day1_15d26ee0:


    "Not like a coworker at the next table in an open plan office."


translate belarusian day1_29ee93d2:


    "And not even like a friend who only differs from other humans in his constant insistence."


translate belarusian day1_82012fff:


    "All of them looked {b}normal{/b} – as I would expect them to be – with their own downsides but without any superpowers."


translate belarusian day1_c821ace7:


    "And Slavya was also...{w} cute?"


translate belarusian day1_7f68049d:


    "I glanced at her stealthily, not knowing what to say."


translate belarusian day1_1ec8deab:


    sl "I'm sorry, I wanted to show you the camp but was ran off my feet."


translate belarusian day1_567b2330:


    me "I... didn't miss anything while on my own, I guess."


translate belarusian day1_2e601a49:


    sl "Are you sure you haven't missed anything at all?"


translate belarusian day1_efd47648:


    "She smiled so brightly that I had to drop my eyes in confusion."


translate belarusian day1_cdbd4a1c:


    me "Well, how would I know – it's my first day here."


translate belarusian day1_122de170:


    sl "Okay, and what have you seen so far?"


translate belarusian day1_875830b6:


    me "I've seen the square, this canteen, the football field..."


translate belarusian day1_76985cc0:


    sl "And what about the beach?"


translate belarusian day1_b11fae54:


    me "Just from afar."


translate belarusian day1_f5ac888c:


    sl "You really should go there! Or let's do it together!"


translate belarusian day1_d5688542:


    me "Yeah, ok... We will..."


translate belarusian day1_446ce20c:


    "Her naturalness started to scare me, but then I thought – what if everything that happens here is how it supposed to be and this world looks strange only to me, while for them it is...{w} normal?"


translate belarusian day1_6578880f:


    "Maybe I was thrown into the past...?"


translate belarusian day1_5410f15c:


    "Yes, that would explain a lot."


translate belarusian day1_aad73e6b:


    me "Can I ask a stupid question?"


translate belarusian day1_fadcaef0:


    sl "No."


translate belarusian day1_7b5c12bd:


    "Slavya smiled and stood up from the table."


translate belarusian day1_c5c451d5:


    sl "It's late... Can you find the way to Olga Dmitrievna's by yourself?"


translate belarusian day1_9fef43a7:


    me "Of course I can, but why should I go there?"


translate belarusian day1_821bd937:


    sl "She'll settle you with someone."


translate belarusian day1_7332342b_1:


    me "What for?"


translate belarusian day1_bfddcddb:


    "Probably this question seemed stupid, because Slavya bursted into good-natured laughter."


translate belarusian day1_c070ca19:


    sl "You need to sleep somewhere, right?"


translate belarusian day1_4405a6cf:


    me "That makes sense..."


translate belarusian day1_2455141e:


    sl "Fine, I'll be off then.{w} Good night!"


translate belarusian day1_0a0db17f:


    me "Night..."


translate belarusian day1_ca407fc5:


    th "It's strange that she left in such a hurry..."


translate belarusian day1_c6b742a3:


    "A bundle of keys left in the door lock caught my attention."


translate belarusian day1_08ccb5b7:


    "I was going to catch up to Slavya, but where does she live?"


translate belarusian day1_3aa2ad8d:


    "And knocking on every door during the middle of the night didn't sound like a bright idea."


translate belarusian day1_9356c1f1:


    th "I'd better take them – I'll give them back tomorrow because who knows what happens here at night."


translate belarusian day1_56811613:


    "Such thoughts gave me chills – it's me who needs to be careful here in the first place."


translate belarusian day1_5802aa2b:


    th "On the other hand why would I need them..."


translate belarusian day1_0eb5a1f8:


    "The night, though dark, wasn't silent at all – one could hear chirping crickets, the songs of the night birds and rustling trees from everywhere."


translate belarusian day1_ee63d9fd:


    "A sudden desire to follow Slavya's advice and go to the camp leader’s cabin appeared."


translate belarusian day1_b358634e:


    "I don't know why but the sight of the unknown bronze builder of communism put me in a constructive mood."


translate belarusian day1_e66f685d:


    "I sat on the bench and started to recall everything that happened today.{w} That was all my constructive mood could offer."


translate belarusian day1_2719b81b:


    "Here was much brighter than near the canteen, and tardy pioneers were running by, so this place didn't seem scary at all."


translate belarusian day1_2dc0eb74:


    th "Bus, summer camp, girls…"


translate belarusian day1_3aff8100:


    "I was so tired from everything new and strange that I could not come up with a single explanation for what was going on."


translate belarusian day1_4caea1ce:


    "I heard a barely noticeable rustle nearby."


translate belarusian day1_f2d26be1:


    "I shivered and looked in that direction."


translate belarusian day1_6993b49f:


    th "A girl.{w} Reading a book."


translate belarusian day1_2807e1fc:


    th "Lena."


translate belarusian day1_1bdc01a9:


    "I decided to move closer and talk."


translate belarusian day1_7ccf917f:


    "She was the only new person I had met here without exchanging even a few words."


translate belarusian day1_7b241855:


    me "Hi, what are you reading?"


translate belarusian day1_ac772bf2:


    "Lena was so surprised that she even jumped."


translate belarusian day1_ea263cee:


    me "Sorry, didn't mean to scare you!"


translate belarusian day1_62592cb0:


    un "Never mind..."


translate belarusian day1_ad42b83a:


    "She blushed and stared at the book again."


translate belarusian day1_c43d3dfb:


    me "So what are you reading?"


translate belarusian day1_f3297c42:


    "On the cover was written 'Gone with the Wind'."


translate belarusian day1_53009119:


    me "A good book…"


translate belarusian day1_626232a6:


    un "Thanks."


translate belarusian day1_87f26837:


    th "Honestly speaking, I haven't read it but I think that such literature suits her very well."


translate belarusian day1_21b3e47f:


    "Lena didn't seem to be interested in continuing the conversation."


translate belarusian day1_f85b2b40:


    me "Well, if I'm bothering you..."


translate belarusian day1_049b021f:


    un "No."


translate belarusian day1_62091b7c:


    "She answered while still looking at the book."


translate belarusian day1_13ee5d36:


    me "Can I sit beside you for a while?"


translate belarusian day1_41cfffd5:


    un "Why?"


translate belarusian day1_e92b6e6e:


    th "And really, why...?"


translate belarusian day1_a0c74c5a:


    "Maybe just because I was very tired and having company is better than being alone."


translate belarusian day1_bf12dc37:


    "And maybe I wanted try to find something out from her."


translate belarusian day1_0864712b:


    "I carefully examined Lena."


translate belarusian day1_07aeaaac:


    th "But no, I doubt that..."


translate belarusian day1_3102ef10:


    me "Well, I don't know... I'm not allowed to?"


translate belarusian day1_3b683375:


    un "Feel free..."


translate belarusian day1_d783747c:


    me "But if I'm bothering you..."


translate belarusian day1_ab09f879:


    un "No, you're not."


translate belarusian day1_858edfda:


    me "I can leave, just say..."


translate belarusian day1_0d42a600:


    un "Everything’s alright."


translate belarusian day1_f517812c:


    me "Ok then..."


translate belarusian day1_98f71c0d:


    "I sat on the end of the bench carefully."


translate belarusian day1_0fcded02:


    "After such an intense talk, staying here was the last thing I wanted, but it wouldn't be nice to just stand up and leave."


translate belarusian day1_61ec364d:


    me "That didn’t really go well, huh?"


translate belarusian day1_cf8bd28b:


    "Lena hasn't answered anything."


translate belarusian day1_8caec5df:


    "It seems I made a fool out of myself."


translate belarusian day1_60dd740c:


    "I bet if Ulyana was here she'd have a good laugh at me."


translate belarusian day1_e1927099:


    me "Do you enjoy being here?"


translate belarusian day1_610b2212:


    "I recalled the Slavya's question and thought it would be a good start for a conversation."


translate belarusian day1_f1be5fed:


    un "Yes."


translate belarusian day1_783cf09d:


    "She smiled slightly."


translate belarusian day1_23729c98:


    me "I guess I like it too..."


translate belarusian day1_b5a17bee:


    "Lena definitely isn't very sociable and probably can't carry on a meaningless conversation as easily as Slavya."


translate belarusian day1_141ec821:


    "But there was something about her that attracted attention."


translate belarusian day1_f8e0c3e7:


    "Like a momentary glimpse of a reflection in the glass on a rainy autumn evening, which makes you turn around and stare into darkness searching for something that you saw out of the corner of your eye."


translate belarusian day1_04705c56:


    "Of course you weren’t able to distinguish or understand what it was but it still felt so tempting..."


translate belarusian day1_9ee89722:


    "Lena was still reading the book without paying any attention to my presence."


translate belarusian day1_62582949:


    "And I had no intention of asking her anything about this camp or this world in general."


translate belarusian day1_a1e73db2:


    me "Beautiful night..."


translate belarusian day1_5ef3d7d4:


    un "Yes..."


translate belarusian day1_76d86f28:


    th "How in a world would you start a conversation with her?"


translate belarusian day1_69ef7bda:


    un "It's late, I have to go…"


translate belarusian day1_45544479:


    me "Yes, it's quite late…"


translate belarusian day1_2629037f:


    un "Good night."


translate belarusian day1_fab13f1e:


    me "Night…"


translate belarusian day1_e5d71cd0:


    "There was something strange about this girl."


translate belarusian day1_482011b5:


    th "At first glance she is a typically shy and modest pioneer girl, but…"


translate belarusian day1_efa2bb54:


    "The mystery of Lena took its own place in the massive list of mysteries about this camp which I had started to put together in my head."


translate belarusian day1_a20cefa7_16:


    "..."


translate belarusian day1_047c6176:


    th "A lazy evening, there’s nothing like a good time with nothing to do…"


translate belarusian day1_8b243a91:


    "I headed towards Olga Dmitrievna’s cabin."


translate belarusian day1_76db5439:


    "The light in the house was on."


translate belarusian day1_01c690bf:


    mt "Hello, Semyon!{w} You're quite late!"


translate belarusian day1_ccd4c421:


    me "Yeah…{w} I went for a walk to look around the camp."


translate belarusian day1_6e6b09c8:


    mt "Alright.{w} You will be sleeping here."


translate belarusian day1_d0ba333c:


    "She pointed her finger at one of the beds."


translate belarusian day1_54ddf89e:


    me "Right here...?"


translate belarusian day1_07acf34e:


    "I was a bit surprised."


translate belarusian day1_0cc846e0:


    mt "Yeah, is something wrong?{w} We are out of free cabins anyway."


translate belarusian day1_e5c2c901:


    "The camp leader smiled, but I rather think it was just out of politeness."


translate belarusian day1_e6f03e6e:


    mt "You do want to be a decent pioneer, don't you?"


translate belarusian day1_6d7c696f:


    "There was a clear emphasis on the word 'decent'."


translate belarusian day1_fa9f2c55:


    me "Yep... Sure..."


translate belarusian day1_c2a35634:


    "I was lost in thought for a moment."


translate belarusian day1_8edc3e00:


    me "Don’t ya mind it mademoiselle?"


translate belarusian day1_d642ee60:


    "She looked at me oddly.{w} With surprise and some offense in her eyes."


translate belarusian day1_a63ad949:


    mt "A pioneer should respect their elders!"


translate belarusian day1_8492f5e5:


    " Olga Dmitrievna said strictly."


translate belarusian day1_4adb9df1:


    me "Of course he should, no one argues with that…"


translate belarusian day1_343b3a4f:


    "I blathered, not realising what was wrong."


translate belarusian day1_1c5c9771:


    mt "Shouldn't you also...?"


translate belarusian day1_8fb93608:


    "She stared at me."


translate belarusian day1_4b041b53:


    "Under such a gaze even mithril forged by the best dwarf masters from the deepest dungeons would melt."


translate belarusian day1_8e8da635:


    me "Should I what?{w} What's up, toots?"


translate belarusian day1_50297304:


    "Annoyance and a lack of understanding made me raise my voice."


translate belarusian day1_20f9f9be:


    mt "You must address adults appropriately!"


translate belarusian day1_0bdad8e7:


    th "Yes, of course there are a lot of strange things here…{w} But this girl is just a couple years older than me..."


translate belarusian day1_d2336ee2:


    th "Or maybe even younger..."


translate belarusian day1_c447246d:


    "But I decided not to argue – while just a few minutes ago I would never have called her an adult, I have to admit that she was also given a strong character."


translate belarusian day1_6a31f527:


    "And in any case I wasn't in a position to argue."


translate belarusian day1_28205106:


    me "As you say… ma'am."


translate belarusian day1_9fde8ff9:


    "I mumbled."


translate belarusian day1_c0fad5bd:


    mt "That's much better!{w} That is how a decent pioneer should conduct himself!"


translate belarusian day1_fc0afa91:


    mt "And now it's time to sleep!"


translate belarusian day1_cee3bfd9:


    "Honestly speaking, I was going to become neither a decent nor an indecent pioneer."


translate belarusian day1_06cf444d:


    "Just yesterday I wasn't going to become a pioneer at all..."


translate belarusian day1_e62f6767:


    th "But do I have choice now?"


translate belarusian day1_a3857c58:


    "'If you don't want to, we'll have to make you' – this is the motto Olga Dmitrievna was probably going to use."


translate belarusian day1_fe66fa24:


    "I climbed onto the bed and closed my eyes only to realise how tired I was after today."


translate belarusian day1_80860151:


    "Something hammered in my head awfully, as if my brains had started a night shift."


translate belarusian day1_af68a167:


    "And they seemed to be aimed more at rolling steel than working on something more sensitive."


translate belarusian day1_95ae7d8f:


    "The bus flew through my mind..."


translate belarusian day1_de155e12:


    "And the square with the monument..."


translate belarusian day1_4d51c966:


    "The canteen, full of pioneers...{w} And the malicious face of Ulyana."


translate belarusian day1_169054b7:


    "Slavya..."


translate belarusian day1_06f01a7c:


    "Lena..."


translate belarusian day1_753d87f9:


    "And even recalling Alisa didn’t give me too much of a negative feeling."


translate belarusian day1_1de79ea9:


    th "What if I'm here for good...?"
# Decompiled by unrpyc: https://github.com/CensoredUsername/unrpyc
