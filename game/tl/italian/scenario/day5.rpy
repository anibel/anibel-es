
translate italian day5_main1_aa4d323e:


    "Stavamo correndo…{w} Correndo con tutte le nostre forze…"


translate italian day5_main1_11656716:


    "Come coloro che corrono per salvare la propria vita."


translate italian day5_main1_5edff0f6:


    "Come una persona condannata, che combatte contro il suo destino, alle prese con l'inevitabile, pur sapendo di non avere più alcuna speranza di salvarsi..."


translate italian day5_main1_5db27d0a:


    "Sono riuscito a malapena a richiudere la pesante porta metallica dietro di me."


translate italian day5_main1_92ce41cd:


    th "Non ho idea di quanto sia profondo questo rifugio antiaereo, o se sia in grado di resistere a un'esplosione nucleare, ma non abbiamo un altro posto dove nasconderci…"


translate italian day5_main1_4c9141de:


    "Lei mi ha stretto la mano."


translate italian day5_main1_6f240b82:


    me "Non aver paura…"


translate italian day5_main1_81e0185f:


    "Pezzi di intonaco cadevano dal soffitto, e le pareti tremavano."


translate italian day5_main1_43320cff:


    "Ero pronto al peggio."


translate italian day5_main1_eabbd6cf:


    "Ma la morte è il genere di cosa per la quale non si è mai pronti…"


translate italian day5_main1_aceafbaa:


    "Improvvisamente è piombato un silenzio assoluto. Un silenzio più forte persino delle esplosioni."


translate italian day5_main1_e7df2349:


    dreamgirl "Forse è giunto il momento di dirsi addio…"


translate italian day5_main1_1f90d83e:


    "Lei stava piangendo."


translate italian day5_main1_8ba5cb54:


    "Volevo confortarla in qualche modo, ma mi sono reso conto che non c'era niente che potessi fare."


translate italian day5_main1_9513cd87:


    me "Sì…"


translate italian day5_main1_b0692838:


    dreamgirl "Sai, io…"


translate italian day5_main1_337804c3:


    "Un botto terribile mi ha quasi frantumato i timpani."


translate italian day5_main1_0ac01129:


    "Sembrava che fossi incastrato sotto ad un pezzo di soffitto crollato, ma non sentivo alcun dolore."


translate italian day5_main1_02f7c13f:


    "L'unica cosa che volevo era non lasciarla andare…"


translate italian day5_main1_893465aa:


    "Mi sono svegliato, stavo sudando freddo, ero a corto di fiato, respiravo affannosamente."


translate italian day5_main1_8d045bac:


    "Mi ci è voluto un po' di tempo per riprendere i sensi."


translate italian day5_main1_1d678e2c:


    me "Era un sogno…{w} Era solo un sogno…"


translate italian day5_main1_85574277:


    "La mia mente confusa, tuttavia, rifiutava di crederci."


translate italian day5_main1_cba74cb0:


    th "Ma chi era quella ragazza che stava con me?"


translate italian day5_main1_92ca5a14:


    "Non volevo lasciare la sua mano, così disperatamente..."


translate italian day5_main1_d2a550ca:


    "Purtroppo non ricordavo proprio chi fosse."


translate italian day5_main1_cfe2aec8:


    "Gli orologi mostravano le dieci passate."


translate italian day5_main1_e1d34af1:


    "Lentamente mi sono ripreso, e la realtà ha cominciato a conquistare la mia mente – il mio stomaco ha brontolato vergognosamente."


translate italian day5_main1_4e2d25a2:


    me "D'accordo – la guerra è la guerra, ma il pranzo va servito a tempo debito!"


translate italian day5_main1_f33a92de:


    "Olga Dmitrievna non era nella stanza – deve aver deciso di non svegliarmi."


translate italian day5_main1_c0506319:


    th "Beh, un grazie alla nostra cara leader!"


translate italian day5_main1_d29a68d9:


    "Dopo le disavventure della notte precedente avevo proprio bisogno di una bella dormita."


translate italian day5_main1_fc576f62:


    "La notte scorsa era solo un ricordo confuso, a cui non mi andava proprio di pensare."


translate italian day5_main1_358f1320:


    "Adesso era più importante trovare qualcosa da mangiare e…{w} lavarsi!"


translate italian day5_main1_16f93484:


    th "Esattamente!"


translate italian day5_main1_c02083da:


    th "Perché un pioniere dev'essere sempre pulito e in ordine."


translate italian day5_main1_15333fca:


    "Sarei stato d'accordo con questo principio anche se non fossi un pioniere (e a dire il vero, non lo sono)."


translate italian day5_main1_f50cd452:


    "Durante il tragitto verso i lavabi ho incontrato Electronik."


translate italian day5_main1_7e4abd77:


    "Ha iniziato ad agitare le braccia ed è corso verso di me."


translate italian day5_main1_5237cfbf:


    el "Buongiorno!{w} Grazie per aver ritrovato Shurik! Senza di lui non saprei nemmeno…"


translate italian day5_main1_b97e3fd5:


    me "Di niente…"


translate italian day5_main1_dab051d6:


    "Ero un po' imbarazzato."


translate italian day5_main1_611aba0e:


    el "No davvero! Non fare il modesto – la nazione dev'essere fiera dei propri eroi!"


translate italian day5_main1_04210531:


    me "E riguardo a Shurik… Come si sentiva stamane?{w} È tutto a posto?"


translate italian day5_main1_c4c5697f:


    "Dopo la follia di ieri, ho pensato che una domanda del genere fosse dovuta."


translate italian day5_main1_805f0715:


    el "Sì, decisamente!{w} L'unica cosa è che non riesce a ricordare nulla…"


translate italian day5_main1_859bc359:


    me "Ma davvero?"


translate italian day5_main1_2850cdb5:


    "La cosa non mi ha sorpreso affatto."


translate italian day5_main1_4819f710:


    el "Dice di essere andato al campo abbandonato ieri, e poi… di essersi svegliato nel suo letto la mattina seguente.{w} Voglio dire, non ricorda nulla tra quei due eventi."


translate italian day5_main1_5ec2624e:


    me "Capisco…{w} Va bene allora…"


translate italian day5_main1_caccdbd7:


    el "Ti sei perso la colazione, non è così? Vieni al nostro circolo! Ti daremo da mangiare! Ho qualcosa di speciale."


translate italian day5_main1_89ac8d3c:


    "Electronik ha sorriso in modo cospiratorio."


translate italian day5_main1_94afa350:


    me "Grazie, verrò, probabilmente…"


translate italian day5_main1_7f29f4c2:


    "Prima però dovevo lavarmi comunque."


translate italian day5_main1_c6f70531:


    el "Ti aspettiamo!"


translate italian day5_main1_641f39b8:


    "Mi ha fatto un gesto di saluto e se n'è andato per i fatti suoi."


translate italian day5_main1_c64ccad8:


    "Non c'era nessuno vicino ai lavabi."


translate italian day5_main1_127f580b:


    "L'acqua era sorprendentemente calda oggi."


translate italian day5_main1_375200fb:


    th "Devono averla già riscaldata, immagino…"


translate italian day5_main1_b4c71998:


    "Dopo essermi lavato la faccia mi sono reso conto che non sarebbe stato così facile lavare il resto."


translate italian day5_main1_e7b1f5de:


    th "Forse dovrei andare alle docce…"


translate italian day5_main1_540ebe5d:


    th "Ma dal momento che non sembra esserci nessuno qui…"


translate italian day5_main1_242cdd8f:


    "Ho girato il rubinetto in modo tale da far scorrere l'acqua in orizzontale, e ho cominciato a spogliarmi."


translate italian day5_main1_8e855925:


    th "E se qualcuno mi vedesse?"


translate italian day5_main1_6f67ff49:


    th "Beh, mi risciacquerò e mi asciugherò rapidamente, rivestendomi il più velocemente possibile…"


translate italian day5_main1_315b7c24:


    "L'acqua, che mi era sembrata calda al contatto con le mani, mi è apparsa invece gelata sul resto del corpo."


translate italian day5_main1_a862e7c2:


    "L'intero processo di lavaggio è durato non più di dieci secondi, e ho cominciato ad asciugarmi rapidamente."


translate italian day5_main1_203544b1:


    "Ma non sono riuscito a finire comunque – delle voci si stavano avvicinando dal sentiero!"


translate italian day5_main1_8c7c1ae8:


    "L'unica soluzione mi è balzata nella mente in una frazione di secondo – ho afferrato i miei vestiti e mi sono gettato tra i cespugli."


translate italian day5_main1_374a9333:


    "Un attimo dopo Alisa e Ulyana sono apparse in prossimità dei lavabi."


translate italian day5_main1_150b3ccc:


    dv "Avresti potuto farlo da sola! Perché mi hai portata qui?"


translate italian day5_main1_88bcfdf2:


    us "È un grosso problema per te?"


translate italian day5_main1_cc876a45:


    dv "D'accordo, fammi vedere…"


translate italian day5_main1_657eb718:


    "Ho sbirciato verso di loro e ho notato che erano entrambe ricoperte di vernice rossa."


translate italian day5_main1_2c5cbf0a:


    th "Che sorpresa…{w} Mi chiedo come abbiano fatto?"


translate italian day5_main1_066f749f:


    "Alisa ha aperto il rubinetto e ha iniziato a strofinare la schiena di Ulyana."


translate italian day5_main1_fe4eae47:


    dv "Togliti il reggiseno!"


translate italian day5_main1_99ac44aa:


    us "E se qualcuno ci vedesse...?"


translate italian day5_main1_fc7e06ef:


    dv "Perché – c'è forse qualcosa da vedere lì?"


translate italian day5_main1_473574eb:


    "Ha ridacchiato."


translate italian day5_main1_6f871fea:


    us "Ok… Ma sbrigati!"


translate italian day5_main1_dd98f289:


    "Era vero che non c'era molto da vedere, ma nonostante ciò fissavo le ragazze attentamente."


translate italian day5_main1_dcd32ae7:


    "È un peccato che mi stessero dando le spalle."


translate italian day5_main1_89e896b4:


    "Un minuto più tardi Alisa è riuscita a lavare via tutta la vernice."


translate italian day5_main1_55cf29e5:


    dv "Fatto!"


translate italian day5_main1_38773291:


    us "Grazie!"


translate italian day5_main1_d53c1a0e:


    dv "Prego…"


translate italian day5_main1_924b8a95:


    "Le ha risposto Alisa, pigramente."


translate italian day5_main1_c473bf91:


    us "Senti, mi fai provare il tuo…"


translate italian day5_main1_68b5468a:


    "Ha indicato il reggiseno di Alisa."


translate italian day5_main1_82a80785:


    dv "Non ti andrà bene di sicuro…"


translate italian day5_main1_f348d116:


    us "Beh, mi piacerebbe provarlo comunque…"


translate italian day5_main1_19b2ba93:


    dv "Ma qui fuori…"


translate italian day5_main1_ba15cbf1:


    us "Non c'è nessuno qui, giusto?"


translate italian day5_main1_b8352a08:


    "Ulyana ha guardato nella mia direzione e ha sorriso maliziosamente."


translate italian day5_main1_eba695a3:


    "Ero assolutamente certo che non mi potesse vedere in quei cespugli, ma…"


translate italian day5_main1_fe7431ac:


    dv "Basta con queste sciocchezze…"


translate italian day5_main1_3bd56665:


    "Ma invece di ascoltarla, Ulyana ha afferrato il suo reggiseno con abile mossa."


translate italian day5_main1_cc3da604:


    "Adesso sì che avevo qualcosa da guardare!"


translate italian day5_main1_7f273933:


    "Ho osservato le due ragazze rincorrersi senza fiato attorno ai lavabi."


translate italian day5_main1_e09372ff:


    "Alisa si stava coprendo il seno con le mani e così non sono riuscito a vedere un granché."


translate italian day5_main1_d0252e13:


    "Mi sono chinato in avanti e ho colpito involontariamente una pietra, che è rotolata fuori dai cespugli…"


translate italian day5_main1_2341b367:


    "Alisa e Ulyana sono rimaste immobili, fissandomi."


translate italian day5_main1_ef3d3ea6:


    "Ho cercato di coprire la mia nudità, con espressione colpevole."


translate italian day5_main1_11a555c2:


    "Quella scena muta è durata per alcuni secondi, poi Alisa ha afferrato la sua camicia e in qualche modo se l'è messa addosso in un baleno."


translate italian day5_main1_b7d8bf72:


    dv "Tu! Tu...!"


translate italian day5_main1_ec010716:


    "Il suo viso è passato dal rosso al viola.{w} Sembrava che stesse per esplodere da un momento all'altro."


translate italian day5_main1_4d760247:


    "L'unica cosa che desideravo era disintegrarmi in singoli atomi e sparire il più lontano possibile dall'epicentro di quel disastro."


translate italian day5_main1_97e02b14:


    us "Se ne stava nascosto lì per tutto il tempo!"


translate italian day5_main1_470f0c0e:


    th "Dunque mi aveva notato…"


translate italian day5_main1_b7d8bf72_1:


    dv "Tu! Tuu!!!"


translate italian day5_main1_3a1dcf49:


    me "E io…{w} Beh, io… Per puro caso…{w} Se sai cosa intendo…"


translate italian day5_main1_a9f31aea:


    "Alisa si è precipitata verso di me."


translate italian day5_main1_4ffa3601:


    "Coprendo il mio sedere con una mano e tenendo i vestiti con l'altra, ho iniziato a correre verso la foresta."


translate italian day5_main1_9ba510d2:


    "Sembrava la soluzione migliore per me in quel momento – dato che il mostrarsi nudo in mezzo al Campo rincorso da due ragazze urlanti non sarebbe stata proprio una grande idea…"


translate italian day5_main1_20ff3f6f:


    "Correvo senza guardarmi indietro."


translate italian day5_main1_e7dc840b:


    "Mi sono fermato dopo alcuni minuti, per riprendere fiato."


translate italian day5_main1_0dab6283:


    "Sembrava che le avessi seminate."


translate italian day5_main1_544faf65:


    th "E così sono riuscito a salvarmi!"


translate italian day5_main1_49bd14a5:


    "Ma al costo di essermi lacerato, graffiato e insanguinato i piedi – dato che non ho avuto tempo per mettermi gli stivali."


translate italian day5_main1_244a0a02:


    "Mi sono seduto su un ceppo, sospirando…"


translate italian day5_main1_ada2f8dd:


    "Dopo un po' mi sono rivestito e ho abbandonato la foresta."


translate italian day5_main1_01337998:


    th "Devo decidere quale sarà la mia prossima mossa!"


translate italian day5_main1_0004b659:


    th "I piedi mi fanno proprio male – quindi sarebbe meglio andare dritto all'infermeria!"


translate italian day5_main1_21e25d14:


    th "D'altra parte però, il mio stomaco non può attendere."


translate italian day5_main1_bc7d5816:


    th "Dovrei quindi accettare l'invito di Electronik?"


translate italian day5_main1_ee8b56d6:


    th "O andare alla mensa, nella speranza di trovare qualcosa da mangiare lì...?"


translate italian day5_dining_hall_c113591d:


    "Mi sono sempre preso cura della mia salute.{w} In particolare in quei momenti in cui non ce la facevo più."


translate italian day5_dining_hall_5b254b74:


    "Adesso riuscivo a camminare, i miei piedi non mi facevano più così male."


translate italian day5_dining_hall_1068ff00:


    "I piedi possono guarire da soli, mentre invece la fame spinge il lupo fuori dal bosco."


translate italian day5_dining_hall_29600c1e:


    th "Sono sicuro che i pionieri non hanno già divorato tutto!"


translate italian day5_dining_hall_16cc44b9:


    th "Almeno un paio di salsicce, delle uova, o nel peggiore dei casi qualche fetta di pane, devono essere rimaste per forza!"


translate italian day5_dining_hall_daf1231e:


    "La mensa era così deserta e silenziosa che ho esitato per un secondo."


translate italian day5_dining_hall_d065f713:


    "Non è forse questo il luogo dove ogni pioniere cerca la sua felicità tre volte al giorno (e alcuni anche più spesso), non è forse un'oasi in questo arido deserto estivo, non è forse un laboratorio chimico segreto dove si studia come i tipi di pasti sconosciuti alla scienza possano influenzare gli organismi immaturi degli adolescenti?"


translate italian day5_dining_hall_01ef2d8a:


    "Ora questo edificio sembrava più un bastione abbandonato dai suoi difensori, una sorta di La Rochelle lasciata dagli Ugonotti."


translate italian day5_dining_hall_d00d888a:


    "Sarebbe bastato entrare per essere circondati dai fantasmi dei guerrieri che accettarono una morte eroica…"


translate italian day5_dining_hall_ff4b2359:


    "La mensa appariva sempre la stessa, tuttavia."


translate italian day5_dining_hall_59713482:


    "Solo che era completamente deserta, eccetto per…{w} Miku, che stava pulendo un tavolo."


translate italian day5_dining_hall_c404fc22:


    "Vedendola ho subito tentato di fuggire di nascosto, ma non ce l'ho fatta…"


translate italian day5_dining_hall_44d5aaf7:


    mi "Ciao, Semyon! Sei venuto qui per mangiare? Ti sei perso la colazione, non è vero? Voglio dire, non ti ho visto questa mattina… Forse c'eri, ma io non ti ho visto. Comunque hai fatto bene a venire!"


translate italian day5_dining_hall_479c6b10:


    me "Umm, ciao…{w} Beh, io… Sì, sono venuto… Mi chiedevo se fosse rimasto qualcosa da mangiare…"


translate italian day5_dining_hall_a44fc8b3:


    mi "Non c'è più niente! Devi aspettare il pranzo! A proposito, non è che vorresti darmi una mano? Sto pulendo qui…"


translate italian day5_dining_hall_810f39c8:


    me "E perché?"


translate italian day5_dining_hall_780b4277:


    mi "Cosa intendi?"


translate italian day5_dining_hall_d946b833:


    "Ha gonfiato le labbra, sembrava offesa."


translate italian day5_dining_hall_d0d699dd:


    mi "Qualcuno deve pur pulire! Lo faremo a turno. Avrai anche tu il tuo turno!"


translate italian day5_dining_hall_c0778825:


    th "No, grazie…"


translate italian day5_dining_hall_6933143e:


    me "Capisco…"


translate italian day5_dining_hall_9b0aa1ca:


    "Stavo per andarmene, ma Miku non riusciva proprio a smetterla."


translate italian day5_dining_hall_c78b2cc2:


    mi "Quindi mi vuoi aiutare?"


translate italian day5_dining_hall_a4480274:


    "Non so perché io abbia accettato."


translate italian day5_dining_hall_79c948da:


    "Capita spesso di prendere una decisione, e poi continuare a chiedersi il perché di tale azione."


translate italian day5_dining_hall_2f281a85:


    "Pensandoci e ripensandoci…{w} Senza riuscire a trovare una risposta logica."


translate italian day5_dining_hall_d93feb70:


    "Ecco come mi sentivo mentre pulivo i tavoli uno dopo l'altro."


translate italian day5_dining_hall_68f21c4b:


    mi "Sai, ho scritto una nuova canzone! Vuoi che te la canti?"


translate italian day5_dining_hall_209b83b3:


    th "Decisamente no."


translate italian day5_dining_hall_0270e5da:


    mi "Hmm, però…"


translate italian day5_dining_hall_60f3d758:


    "Ha pensato un attimo."


translate italian day5_dining_hall_85b5c92a:


    mi "Sarebbe difficile cantare e pulire contemporaneamente… Te la canterò più tardi allora!"


translate italian day5_dining_hall_3aeb2d0f:


    "Miku mi ha fatto un sorriso disarmante."


translate italian day5_dining_hall_3b03af3a:


    me "Sì, naturalmente…"


translate italian day5_dining_hall_6eaf9733:


    mi "Sei stato davvero grande ieri a salvare Shurik! In tutto il Campo oggi non si è parlato d'altro!"


translate italian day5_dining_hall_992a60d0:


    th "Mi sento proprio come un eroe…"


translate italian day5_dining_hall_668b75c5:


    me "Anche se non è stato niente di che."


translate italian day5_dining_hall_57cc9875:


    mi "No, davvero, dico sul serio! Non avrei mai il coraggio di andare nel bosco di notte… Nel vecchio campo… Sai le voci che girano? A proposito di una leader del campo che si è sparata…"


translate italian day5_dining_hall_3bb17a2c:


    th "Prima avevano detto che si era impiccata."


translate italian day5_dining_hall_52e843c6:


    mi "In ogni caso, è così spaventoso!"


translate italian day5_dining_hall_b22bc996:


    me "Sì, probabilmente."


translate italian day5_dining_hall_f15c1e3c:


    "Ho cercato di isolarmi dagli stimoli esterni e di concentrarmi sulla pulizia."


translate italian day5_dining_hall_235ecfcf:


    "È ciò mi ha facilitato a terminare prima del previsto."


translate italian day5_dining_hall_7a81d8e3:


    me "Ecco fatto!"


translate italian day5_dining_hall_07ea95a8:


    mi "Grazie!"


translate italian day5_dining_hall_25fab848:


    "Avevo ancora un po' di tempo fino a pranzo, così ho deciso di andare a fare una passeggiata."


translate italian day5_dining_hall_90a17a70:


    mi "Ok, va bene…"


translate italian day5_dining_hall_b2673fad:


    "Miku sembrava frustrata."


translate italian day5_dining_hall_f3c8b2f3:


    th "Ma che ci posso fare – è la vita…"


translate italian day5_dining_hall_9d2e50ce:


    "Dopo aver lasciato la mensa, mi sono seduto sulla panchina vicino alla porta, sospirando stancamente."


translate italian day5_dining_hall_7e6cef9d:


    "I miei piedi erano ancora un po' doloranti, anche se non così tanto come prima, e il mio stomaco era ancora vuoto…"


translate italian day5_dining_hall_29ebce26:


    "C'era ancora un po' di tempo prima di pranzo, così ho deciso di andare a fare una passeggiata."


translate italian day5_dining_hall_549330fd:


    "Sono andato in una direzione casuale che potrebbe essere definita con la semplice parola «avanti»."


translate italian day5_dining_hall_8983ed56:


    "Alla fine mi sono trovato alla piazza."


translate italian day5_dining_hall_283be7ae:


    "Nulla di sorprendente, dato che il monumento di Genda sembrava essere il fulcro centrale di questo Campo, una sorta di punto zero…"


translate italian day5_dining_hall_356059a5:


    "Mi sono seduto su una panchina e ho iniziato a pensare."


translate italian day5_dining_hall_85bc341b:


    th "Sono già passati quattro giorni, e non sono ancora riuscito a capire minimamente come io possa essere finito qui."


translate italian day5_dining_hall_37f3c603:


    th "È vero che alcune cose strane sono accadute durante questo periodo, ma quasi ognuna di esse può essere spiegata con la logica dopo un'attenta analisi."


translate italian day5_dining_hall_9cc8af14:


    th "Ognuno di quegli eventi potrebbe tranquillamente accadere nella vita normale…"


translate italian day5_dining_hall_ee9a7a1a:


    th "La vita {i}normale{/i}.{w} Quel termine sembra aver perso il suo significato originario, da quando sono qui."


translate italian day5_dining_hall_e5526803:


    th "Le reazioni all'ambiente circostante, le azioni e le parole delle persone, le mie stesse parole – non hanno proprio niente di normale."


translate italian day5_dining_hall_66dcd718:


    th "Negli ultimi quattro giorni la mia visione del mondo ha incassato una serie di dolorosi pugni allo stomaco e di montanti, che mi hanno mandato al tappeto, abbattendomi nel profondo dell'animo."


translate italian day5_dining_hall_7b0e3b31:


    th "A volte non capisco perché io mi comporti in un certo modo o perché io dica certe cose…"


translate italian day5_dining_hall_19f2dbbc:


    th "In realtà lo capisco, ma non immediatamente."


translate italian day5_dining_hall_ac9be890:


    th "Certi ripensamenti tuttavia non mi aiutano ad agire diversamente, in modo più sano e adeguato alla situazione."


translate italian day5_dining_hall_bc9e662f:


    th "Le spiegazioni concrete mi si presentano sempre più raramente."


translate italian day5_dining_hall_85fdbea0:


    th "Se il mio unico desiderio durante il primo giorno era quello di andarmene da qui, adesso le mie maggiori preoccupazioni sono il dove poter trovare il cibo, come evitare l'allineamento al mattino, e quale spiegazione dare a Olga Dmitrievna nel caso in cui Alisa si lamentasse di me…"


translate italian day5_dining_hall_f2b21e4a:


    th "E queste cose sono veramente importanti per me!"


translate italian day5_dining_hall_770314f2:


    th "E giorno dopo giorno, la mia mente è offuscata in continuazione da piccoli dettagli quotidiani come quelli, che mi tolgono dalla testa l'idea che questo mondo, questo Campo, queste ragazze, siano in realtà del tutto anormali!"


translate italian day5_dining_hall_4ba0ec94:


    th "Ma non posso farci niente.{w} Perché continuo a dimenticare…"


translate italian day5_dining_hall_5c46142a:


    th "Allo stesso modo in cui si continua a respirare senza accorgersene, io mi immergo sempre più nella vita quotidiana di questo mondo senza rendermene conto."


translate italian day5_dining_hall_95d43b05:


    th "Diventando sempre più un pioniere medio…"


translate italian day5_dining_hall_6614ca63:


    me "No!{w} È tutto sbagliato!"


translate italian day5_dining_hall_216fd396:


    "Ho gridato forte, schiaffeggiandomi ripetutamente."


translate italian day5_dining_hall_8d33c464:


    "Tutto ad un tratto la sirena, che chiamava i pionieri per il pranzo, è risuonata dagli altoparlanti."


translate italian day5_dining_hall_f23afdfa:


    me "Finalmente!"


translate italian day5_dining_hall_436b6b08:


    "Mi sono fatto strada verso la mensa, abbandonando nella piazza tutti i miei pensieri ispiratori, dove sarebbero potuti risultare interessanti solo a Genda, se solo fosse vivo…"


translate italian day5_aidpost_c1caf0d4:


    "Mi sono sempre preso cura della mia salute.{w} In particolare quando qualcosa mi faceva male davvero."


translate italian day5_aidpost_b92e3272:


    "Quindi, senza esitare, sono andato verso l'infermeria."


translate italian day5_aidpost_6e2d6c49:


    "Ma proprio di fronte alla porta mi sono fermato di colpo, come se una forza misteriosa mi stesse trattenendo."


translate italian day5_aidpost_f49a6790:


    "Non avevo nessuna voglia di affrontare l'infermiera del Campo senza alcuna necessità urgente, ma d'altra parte, ieri le avevo fatto un grande favore custodendo l'infermeria."


translate italian day5_aidpost_9cb3955d:


    "Quindi in un certo senso me lo doveva…"


translate italian day5_aidpost_41de82b8:


    "Ho preso coraggio e ho aperto la porta."


translate italian day5_aidpost_8e557000:


    cs "Ciao, pioniere!"


translate italian day5_aidpost_bb4c809d:


    me "Salve…"


translate italian day5_aidpost_0f095ac2:


    cs "Scusa se sono tornata così tardi ieri sera…"


translate italian day5_aidpost_fa9efa41:


    me "Nessun problema…"


translate italian day5_aidpost_e2736162:


    cs "Perché sei venuto?{w} Sei malato?"


translate italian day5_aidpost_945c59bc:


    "Ha sorriso sornione."


translate italian day5_aidpost_f6d4c30b:


    me "Ecco, io…{w} Me li sono un po'…"


translate italian day5_aidpost_a10407c0:


    cs "Te li sei un po' cosa?"


translate italian day5_aidpost_3f4d5019:


    me "I miei piedi."


translate italian day5_aidpost_be55d6e6:


    "Ho risposto stupidamente."


translate italian day5_aidpost_ee52ce7f:


    cs "Ok, fammi vedere."


translate italian day5_aidpost_ebe6fcf2:


    "Mi sono seduto sul lettino e mi sono tolto gli stivali."


translate italian day5_aidpost_387cc55b:


    cs "Come te lo sei fatto?"


translate italian day5_aidpost_97c64dc4:


    "Era meglio tacere riguardo a ciò che era accaduto nei pressi dei lavabi."


translate italian day5_aidpost_2fcc5991:


    me "Beh, io…"


translate italian day5_aidpost_db1556e6:


    cs "Ok, non importa.{w} Aspetta un secondo…"


translate italian day5_aidpost_37b10e56:


    "Ha frugato nel cassetto e ha tirato fuori da una fiala una grossa pastiglia rossa con un segno a forma di croce."


translate italian day5_aidpost_7603637e:


    me "E quella a cosa serve?"


translate italian day5_aidpost_8f1a1f4c:


    "Mi sono preoccupato delle dimensioni di quella pastiglia e di quella sua strana forma – di solito si rompono a metà e non in quattro parti."


translate italian day5_aidpost_8040e280:


    cs "Questa è per evitare l'amputazione delle tue gambe!"


translate italian day5_aidpost_3d342d5a:


    me "Che cosa?!"


translate italian day5_aidpost_91d22182:


    cs "Niente. Non aver paura…{w} pioniere!{w} Prendila e sarà tutto a posto."


translate italian day5_aidpost_9fe20b07:


    me "E questa pastiglia…{w} perché ha quella forma?"


translate italian day5_aidpost_7e2d8a95:


    cs "È per i pazienti che si rifiutano di prendere i farmaci per via orale.{w} In quel caso gliela avvitiamo dentro con un cacciavite."


translate italian day5_aidpost_7ccd5058:


    "Non ho osato chiederle in quale punto venisse avvitata."


translate italian day5_aidpost_c2372b85:


    cs "Brucerà un po'."


translate italian day5_aidpost_5c6ac9cb:


    "Ha preso un grosso pacco di cotone idrofilo, ne ha staccato un pezzo e l'ha imbevuto abbondantemente di iodio."


translate italian day5_aidpost_ad181873:


    "Mi sono preparato a soffrire le pene dell'inferno…"


translate italian day5_aidpost_4b4491c1:


    me "iiiiiii…"


translate italian day5_aidpost_1e0476d1:


    "Ho gemuto in silenzio."


translate italian day5_aidpost_ad1b9cb0:


    "Ma la mia fiducia in quel metodo di disinfezione era molto più grande di quella che avevo nei confronti della strana pillola, quindi ho dovuto sopportare il bruciore."


translate italian day5_aidpost_4e9c1b77:


    cs "Ecco fatto!"


translate italian day5_aidpost_4eb5e7c1:


    "Ho rimesso gli stivali e ho provato a camminare."


translate italian day5_aidpost_920fc608:


    "I piedi mi facevano ancora male, ma il dolore acuto se n'era andato."


translate italian day5_aidpost_61477e60:


    me "Grazie!"


translate italian day5_aidpost_f328eb53:


    cs "Non c'è di che!{w} Torna quando vuoi…{w} pioniere!"


translate italian day5_aidpost_40d35bf5:


    "L'ho salutata e ho lasciato l'infermeria."


translate italian day5_aidpost_c4d64e48:


    "Il mio problema di salute è stato risolto!"


translate italian day5_aidpost_132ab340:


    th "Ora è tempo di mangiare!"


translate italian day5_clubs_6f918f97:


    "Avevo proprio tanta fame."


translate italian day5_clubs_c113591d:


    "Mi sono sempre preso cura della mia salute.{w} In particolare quando qualcosa mi faceva proprio male."


translate italian day5_clubs_f98258e8:


    "E adesso ero in grado di camminare, i piedi non mi facevano più così male."


translate italian day5_clubs_ff3100c2:


    th "In qualche modo guariranno, mentre invece la fame non può aspettare…"


translate italian day5_clubs_52edf477:


    "Ho deciso di approfittare dell'invito di Electronik."


translate italian day5_clubs_13624c4d:


    "Dopotutto, conoscendo i pionieri locali, era logico pensare che non avessero lasciato nemmeno una briciola alla mensa…"


translate italian day5_clubs_2035d522:


    th "E il circolo di cibernetica in qualche modo mi deve un favore."


translate italian day5_clubs_c6f1a614:


    "Con tali pensieri ho raggiunto l'edificio del circolo."


translate italian day5_clubs_e8ed57a1:


    "E ho sentito delle urla provenire dall'interno."


translate italian day5_clubs_d8f147f1:


    "Ho cercato di ascoltare attentamente, ma non riuscivo a capire niente."


translate italian day5_clubs_5b5a4f32:


    sh "Ridammelo!"


translate italian day5_clubs_6c5410b6:


    us "No, non te lo do!"


translate italian day5_clubs_d25dd8ba:


    "Ulyana correva per la stanza tenendo in mano una bobina, mentre Shurik la inseguiva."


translate italian day5_clubs_12e6be7b:


    me "Ragazzi, mi spiace interrompervi, ma…"


translate italian day5_clubs_5b5a4f32_1:


    sh "Ridammelo!"


translate italian day5_clubs_1217af9c:


    "Non mi hanno nemmeno degnato di uno sguardo."


translate italian day5_clubs_6c5410b6_1:


    us "No, non lo farò!"


translate italian day5_clubs_87a4498e:


    "Shurik, totalmente concentrato sull'inseguimento, è scivolato davanti a me, quasi abbattendomi."


translate italian day5_clubs_95efcfe1:


    me "Ehi!"


translate italian day5_clubs_37ace03e:


    "Ulyana correva in cerchio attorno alla stanza ridendo allegramente."


translate italian day5_clubs_ec25da0f:


    th "Mi chiedo a cosa le serva quel filo?"


translate italian day5_clubs_81589db2:


    "Nel frattempo il capo del circolo di cibernetica sembrava essersi ripreso dalla follia di ieri.{w} Si potrebbe anche dire che fosse – rinvigorito."


translate italian day5_clubs_2ddd0bed:


    "Ma neppure questo probabilmente lo avrebbe aiutato a catturare Ulyana – dopotutto lei era più piccola, più agile e più veloce di lui."


translate italian day5_clubs_075ee6b7:


    "Avrebbe potuto portarlo facilmente allo sfinimento..."


translate italian day5_clubs_95efcfe1_1:


    me "Ehi!"


translate italian day5_clubs_0b5f4462:


    "Ulyana è corsa verso di me e si è nascosta alle mie spalle."


translate italian day5_clubs_692dfc67:


    "E Shurik a sua volta ha abbandonato il circolo, sbattendo la porta."


translate italian day5_clubs_8c929878:


    th "Sembra essersi offeso..."


translate italian day5_clubs_25606905:


    me "Ehi, che ti succede?!"


translate italian day5_clubs_b73a1dab:


    us "Non mi prenderete!"


translate italian day5_clubs_575bed8e:


    "Ha guardato Electronik, che stava osservando in silenzio tutto quello stupido teatrino, e gli ha fatto la linguaccia."


translate italian day5_clubs_67bf1dec:


    el "Semyon, fatti ridare il filo!"


translate italian day5_clubs_3cfac947:


    me "E perché vi dovrebbe servire? Non avete altro filo qui, o qualcosa del genere?"


translate italian day5_clubs_65fffe51:


    "Stava cercando di acchiappare Ulyana, che si nascondeva dietro di me."


translate italian day5_clubs_3a233083:


    "Si è mosso a destra, mentre lei si è spostata a sinistra, si è spostato a sinistra, mentre lei si è mossa a destra…"


translate italian day5_clubs_db785e8c:


    "Alla fine mi sono stancato di tutto quel parapiglia, e con un'abile mossa ho strappato il filo dalle mani di Ulyana."


translate italian day5_clubs_7a0f81d3:


    us "Ehi, dammelo! Ridammelo!"


translate italian day5_clubs_25ebf834:


    "Ha gridato con risentimento."


translate italian day5_clubs_bb517af5:


    me "No, non lo farò! Smettila di correre per la stanza!"


translate italian day5_clubs_aac11710:


    "Tenevo il filo sopra la mia testa in modo che Ulyana non potesse raggiungerlo, data la sua statura."


translate italian day5_clubs_d2db29cb:


    el "Grazie!"


translate italian day5_clubs_44f47ba7:


    us "Ok, va bene allora!"


translate italian day5_clubs_99aa653e:


    "Ha sbuffato e si è allontanata da me."


translate italian day5_clubs_66d3549f:


    me "Perché ti serve, in primo luogo?"


translate italian day5_clubs_7d1c8c94:


    us "Non sono affari tuoi…"


translate italian day5_clubs_c68d93e1:


    "Mi ha guardato con un sorrisetto sornione."


translate italian day5_clubs_88702a1e:


    us "Vuoi che dica a tutti che questa mattina stavi…"


translate italian day5_clubs_960701df:


    "Le ho serrato la bocca con la mano, trascinandola verso l'uscita."


translate italian day5_clubs_d0245af1:


    me "Ok, noi dobbiamo andare…"


translate italian day5_clubs_c000d8e7:


    "Ho detto a Electronik ridacchiando stupidamente."


translate italian day5_clubs_4d17bd21:


    el "E allora per cosa eri venuto...?"


translate italian day5_clubs_7525ab2b:


    "Una volta fuori, ho liberato la scalciante Ulyana."


translate italian day5_clubs_1828214b:


    me "Ascoltami bene, ti rendi conto che è stato un malinteso…{w} E sai bene che è stato causato da te!"


translate italian day5_clubs_86dba4fb:


    us "Io non so nulla!{w} I fatti sono fatti: ci stavi spiando!"


translate italian day5_clubs_d226b66c:


    us "Ahi ahi ahi!{w} Chissà cosa penserebbe Olga Dmitrievna se lo venisse a sapere…"


translate italian day5_clubs_df680674:


    "Da un lato, non me ne importava proprio niente di cosa avrebbe pensato la leader del Campo, ma dall'altro, tutte le prove erano contro di me, e avrei fatto meglio a non immischiarmi in una situazione del genere."


translate italian day5_clubs_97ad7b66:


    me "Va bene, forse potremmo fare un patto…"


translate italian day5_clubs_6532261a:


    us "Hmmm…"


translate italian day5_clubs_60f3d758:


    "Ha pensato un po'."


translate italian day5_clubs_9c1e8275:


    us "Ci sono!"


translate italian day5_clubs_747f9ddc:


    "Mi sono preparato al peggio."


translate italian day5_clubs_e97a33bb:


    us "Mi riporterai quel filo!"


translate italian day5_clubs_47728b3d:


    me "Ma a cosa ti serve?"


translate italian day5_clubs_fc2357ec:


    us "Mi serve perché mi serve!"


translate italian day5_clubs_2fca2e42:


    me "Ok, supponiamo che io vada a prenderlo...{w} Allora tu non dirai niente a nessuno?"


translate italian day5_clubs_6f158bac:


    us "Ti do la mia parola di pioniere!"


translate italian day5_clubs_85b6f436:


    "Era difficile riuscire a crederle."


translate italian day5_clubs_88068e4d:


    "Ma d'altra parte, era solo un po' di filo – quindi perché non tentare?"


translate italian day5_clubs_91dd4d67:


    me "Ok, affare fatto..."


translate italian day5_clubs_a64a24e7:


    us "Affare fatto!"


translate italian day5_clubs_fee7bf01:


    me "Ma devi sapere che..."


translate italian day5_clubs_f85583fb:


    us "Si, si, certo!"


translate italian day5_clubs_0d425fdb:


    "Mi ha interrotto."


translate italian day5_clubs_b2bf8ea2:


    me "Aspettami qui."


translate italian day5_clubs_a4e889e3:


    "Detto ciò, sono tornato dentro alla sede dei circoli."


translate italian day5_clubs_0af71d51:


    me "No, niente filo per te, basta!"


translate italian day5_clubs_1d6fcf81:


    us "Allora dirò tutto!"


translate italian day5_clubs_5da9850d:


    me "Lo faresti comunque! O lo farebbe Alisa..."


translate italian day5_clubs_80eb9adc:


    us "Ahh, piantala!"


translate italian day5_clubs_6d0fed3e:


    me "Sì, certo, come se fosse colpa mia..."


translate italian day5_clubs_a18bb399:


    us "Lo è. Ci stavi spiando."


translate italian day5_clubs_69a83100:


    me "Non è vero!"


translate italian day5_clubs_2719b638:


    "Ma, indipendentemente da tutto, i fatti sono fatti..."


translate italian day5_clubs_76442aba:


    us "Alisa non è d'accordo."


translate italian day5_clubs_bdc70299:


    me "Lei non è d'accordo con me su un sacco di cose."


translate italian day5_clubs_d927617d:


    us "E nemmeno a Olga Dmitrievna piacerà la cosa."


translate italian day5_clubs_a36b990a:


    me "Sai che ti dico?!"


translate italian day5_clubs_6ea78a01:


    "Stavo perdendo la pazienza."


translate italian day5_clubs_07f22fdf:


    me "Se proprio vuoi dirlo in giro – allora forza, fallo subito! E non dimenticarti di dire anche che la crisi di governo, la povertà nel mondo, il riscaldamento globale e il Diluvio universale sono tutti colpa mia!"


translate italian day5_clubs_7843e56a:


    us "E dai, smettila di scaldarti... Stavo solo scherzando."


translate italian day5_clubs_ba72b31c:


    me "Scherzando...?"


translate italian day5_clubs_578ce4ba:


    "Improvvisamente mi sono reso conto di aver reagito un po' troppo male."


translate italian day5_clubs_07630dae:


    me "I tuoi scherzi sono proprio stupidi! E cosa dovrei fare tutte le volte – cercare di indovinare se stai scherzando o no?"


translate italian day5_clubs_df6dbf74:


    us "Sì."


translate italian day5_clubs_fde537c1:


    "Ha sogghignato."


translate italian day5_clubs_0f1412bc:


    us "Così è più divertente."


translate italian day5_clubs_eac89d90:


    "Ulyana si è voltata ed è corsa verso la piazza, agitando la mano in segno di addio."


translate italian day5_clubs_beff659f:


    th "Non è proprio possibile trattare con lei in modo armonioso..."


translate italian day5_clubs_01a2b4fe:


    "Ho sospirato e sono tornato al circolo."


translate italian day5_clubs_ee53a93e:


    "Electronik stava costruendo qualcosa con grande attenzione."


translate italian day5_clubs_6768144c:


    me "Adesso sono libero…{w} Così ho pensato che mi potessi dare qualcosa da mangiare, come mi avevi promesso…"


translate italian day5_clubs_72b8e1d5:


    "Il mio tono è suonato un po' servile, e la cosa mi ha dato abbastanza fastidio."


translate italian day5_clubs_cda5bf38:


    me "Naturalmente non insisto, ma…"


translate italian day5_clubs_73232f80:


    el "Solo un minuto."


translate italian day5_clubs_819d22c9:


    "Ha abbandonato il suo lavoro per un istante e ha preso un paio di panini dal cassetto, e una classica confezione triangolare di kefir."


translate italian day5_clubs_10c6e757:


    el "Fa' come se fossi a casa tua."


translate italian day5_clubs_73aab3cd:


    me "Grazie…"


translate italian day5_clubs_c321210e:


    "Mentre ero occupato a mangiare, Electronik non si è staccato nemmeno un secondo dai suoi marchingegni."


translate italian day5_clubs_88d66003:


    "Stava avvolgendo il filo attorno alla bobina, quella che avevo tolto a Ulyana."


translate italian day5_clubs_c90aa8d9:


    th "Ed è proprio quel filo che lei mi ha chiesto di prendere!"


translate italian day5_clubs_b12c3bd0:


    me "E quindi cos'è quello?"


translate italian day5_clubs_ce6e33a1:


    el "Un induttore."


translate italian day5_clubs_01d8c6fc:


    me "Un indi-attore?"


translate italian day5_clubs_a1512ad1:


    el "Unisciti al nostro circolo e saprai tutto!"


translate italian day5_clubs_99c684ef:


    "Mi ha guardato con sorriso sornione."


translate italian day5_clubs_232d02c3:


    me "Ci penserò…"


translate italian day5_clubs_8e38bd64:


    "Naturalmente non avevo alcuna intenzione di aderire a nulla, ma considerando che oggi mi aveva dato da mangiare, dovevo quantomeno essere gentile."


translate italian day5_clubs_0560d317:


    el "Tra l'altro, come ti avevo detto, avrei qualcos'altro…"


translate italian day5_clubs_f39fd8a3:


    me "Bene, sì…"


translate italian day5_clubs_73232f80_1:


    el "Aspetta un secondo."


translate italian day5_clubs_ebaab1b0:


    "È andato nella stanza adiacente ed è tornato dopo un minuto con una specie di pacchetto."


translate italian day5_clubs_1dfcdcd0:


    el "Tan-ta-daaa!"


translate italian day5_clubs_6ba4e579:


    "E me l'ha consegnato."


translate italian day5_clubs_4b293ede:


    "Dentro c'era una grossa bottiglia di vodka «Stolichnaya»."


translate italian day5_clubs_4e5a25f1:


    me "Eh, capisco, ma è ancora mattina…"


translate italian day5_clubs_7cc7ef1a:


    th "O forse Electronik condivide il motto «ubriaco di prima mattina – libero tutto il giorno»?"


translate italian day5_clubs_9479eb17:


    el "Ma cosa hai capito?!{w} Non ti sto proponendo di berla! Ci serviva per pulire le parti interne!"


translate italian day5_clubs_5e972698:


    th "Pulire le parti interne, già…{w} Chissà quali…"


translate italian day5_clubs_f2e007f0:


    me "Ok…"


translate italian day5_clubs_9991da5b:


    "Gli ho ridato il pacchetto."


translate italian day5_clubs_a5f87a00:


    me "Comunque! Mi potresti prestare quel filo?"


translate italian day5_clubs_ce0d2801:


    el "E a cosa ti serve?"


translate italian day5_clubs_c97fadc0:


    me "Eeh..."


translate italian day5_clubs_d6323f95:


    "Forse avrei dovuto pensarci prima."


translate italian day5_clubs_98fe77ae:


    me "Mi serve."


translate italian day5_clubs_09b8008a:


    el "Serve anche a noi, non possiamo farne a meno."


translate italian day5_clubs_68f1c3d2:


    me "Vi serve? Beh, allora non importa..."


translate italian day5_clubs_ba73ed1f:


    th "Non posso di certo costringerlo, giusto?"


translate italian day5_clubs_cd9f16d5:


    me "Allora vado…"


translate italian day5_clubs_642cd44e:


    el "Torna quando vuoi!"


translate italian day5_clubs_bdd3ba78:


    me "Ma certo…"


translate italian day5_clubs_c5676147:


    th "Quando avrò voglia di farmi un drink…"


translate italian day5_clubs_212a81b3:


    "Uscendo, mi sono chiesto perché mi avesse voluto mostrare la vodka, in primo luogo…"


translate italian day5_clubs_3c6ad981:


    "Ulyana è corsa immediatamente verso di me."


translate italian day5_clubs_1fb33640:


    us "Allora? Ce l'hai?"


translate italian day5_clubs_1eaad051:


    me "No..."


translate italian day5_clubs_07a5780c:


    us "Allora è colpa tua!"


translate italian day5_clubs_03515da3:


    me "Hai proprio bisogno di quel filo...?"


translate italian day5_clubs_b934057e:


    us "Ma apprezzo il fatto che tu ci abbia provato!"


translate italian day5_clubs_ad02f53a:


    me "Che cosa?"


translate italian day5_clubs_d8362ad8:


    "Ero sorpreso."


translate italian day5_clubs_a10e2b46:


    us "Alisa dirà tutto comunque!"


translate italian day5_clubs_a4c6b91e:


    "Ulyana ha riso."


translate italian day5_clubs_d1d0d845:


    us "Lei è davvero arrabbiata con te."


translate italian day5_clubs_078810d4:


    me "Ci posso scommettere…"


translate italian day5_clubs_3e1d421a:


    "Ho mormorato a bassa voce."


translate italian day5_clubs_fa77eb7d:


    us "Ok, ci vediamo!"


translate italian day5_clubs_3861dc96:


    "Ha agitato la mano ed è corsa via."


translate italian day5_clubs_50e6fe96:


    th "Eccomi qui, in un'altra situazione stupida, niente di strano."


translate italian day5_clubs_5bd8bf7d:


    th "Non è la prima volta..."


translate italian day5_clubs_bedd20be:


    "Improvvisamente la mia fame si è risvegliata…"


translate italian day5_clubs_9a611cea:


    "Certamente, qualche panino e del kefir erano ottimi. Ma non erano sufficienti a riempirmi del tutto."


translate italian day5_clubs_8b9210c2:


    "Per fortuna la sirena della mensa ha suonato, chiamando i pionieri per il pranzo."


translate italian day5_main2_c9f98d7e:


    th "La giornata è appena iniziata e sono già successe così tante cose…"


translate italian day5_main2_9dff93e1:


    th "Ma ho perseverato, e adesso ho tutto il diritto di mangiare in abbondanza!"


translate italian day5_main2_b855c58c:


    "Oggi non ero l'ultimo a entrare, e quindi ho potuto scegliere un tavolo libero."


translate italian day5_main2_5a8c068e:


    "Il pranzo consisteva in zuppa di piselli e purè di patate con pesce."


translate italian day5_main2_e5fed9f2:


    "Era una grande delusione per me, dato che non mi piace alcun tipo di pesce, e ciò significava che il mio organismo avrebbe ricevuto meno calorie del solito."


translate italian day5_main2_9a2a6cb4:


    "Dopo pochi minuti Slavya e Lena si sono sedute al mio tavolo."


translate italian day5_main2_99c96681:


    sl "Possiamo?"


translate italian day5_main2_b7bd4e35:


    "Ha sorriso dolcemente."


translate italian day5_main2_bcdef80b:


    me "Eh?{w} Sì, certo!"


translate italian day5_main2_566b45f6:


    "Mi sono alzato e ho tirato fuori una sedia per lei."


translate italian day5_main2_7b602452:


    me "Prego!"


translate italian day5_main2_cafd2e9e:


    "Ero di ottimo umore in quel momento."


translate italian day5_main2_538f35f4:


    un "Buon appetito…"


translate italian day5_main2_62d27eaf:


    "Lena ha continuato a fissarmi per un po' di tempo, ma poi, dopo essersi accorta di quanto quel suo sguardo fosse strano, è passata al suo piatto."


translate italian day5_main2_3c5ee540:


    me "Anche a voi."


translate italian day5_main2_952025ed:


    sl "Hai dei piani per oggi, Semyon?"


translate italian day5_main2_5a7a7577:


    me "No."


translate italian day5_main2_c0d912bb:


    "Le ho dato una risposta sincera, dato che non avevo pianificato niente.{w} Fatta eccezione per il {i}cercare risposte{/i}, ma quello era più un piano generale."


translate italian day5_main2_8df8d875:


    sl "Allora vuoi venire con noi a fare un giro in barca verso l'isola?"


translate italian day5_main2_c16c4c12:


    th "L'isola…{w} Beh, credo di averla vista dal molo."


translate italian day5_main2_7332342b:


    me "Per cosa?"


translate italian day5_main2_ea29c6fe:


    sl "Olga Dmitrievna ci ha chiesto di raccogliere le fragole.{w} Ci sono un sacco di fragole là e sono così deliziose!"


translate italian day5_main2_679ff36d:


    "Potevo immaginare il loro gusto senza nemmeno assaggiarle, bastava guardare l'espressione di Slavya."


translate italian day5_main2_f29bf018:


    me "Fragole…{w} E a cosa vi servono?"


translate italian day5_main2_2abd657f:


    sl "Non lo so, ma penso che sia davvero una grande idea!"


translate italian day5_main2_5ccbf2e6:


    th "Beh, in effetti lo è.{w} Soprattutto perché non ho ancora visitato quell'isola."


translate italian day5_main2_1066065f:


    me "Va bene, verrò anch'io."


translate italian day5_main2_a20cefa7:


    "..."


translate italian day5_main2_f2b9563d:


    "In pochi minuti eravamo già pronti al molo."


translate italian day5_main2_a8b1bc59:


    sl "Bene, ecco la barca.{w} Aspettate, vado a prendere i remi."


translate italian day5_main2_86c42a71:


    "Sono rimasto da solo con Lena."


translate italian day5_main2_1745ad04:


    me "Ti piacciono le fragole?"


translate italian day5_main2_3898ad76:


    un "Beh, non proprio…{w} Ma sono gustose."


translate italian day5_main2_561828ec:


    "Lena ha sorriso."


translate italian day5_main2_f2e007f0:


    me "Capisco…"


translate italian day5_main2_3a3284a7:


    "Non sapevo cos'altro dire, come continuare la conversazione."


translate italian day5_main2_858a16c5:


    "Se Slavya non fosse tornata probabilmente saremmo rimasti in silenzio fino a sera."


translate italian day5_main2_ca656115:


    sl "Ecco qua!"


translate italian day5_main2_60c4cc55:


    "Mi ha consegnato un paio di remi pesanti."


translate italian day5_main2_4ab1b169:


    me "Già… Grazie…"


translate italian day5_main2_d8f75e4c:


    "Siamo saliti sulla barca, ho sciolto il nodo, l'ho spinta dal molo e ho iniziato a remare."


translate italian day5_main2_ff778a3e:


    me "E dove stiamo andando esattamente?"


translate italian day5_main2_7c19e280:


    sl "Proprio là!"


translate italian day5_main2_199f5847:


    "Ha puntato il dito verso l'isola."


translate italian day5_main2_adf7269e:


    sl "Quell'isola si chiama «La più vicina»."


translate italian day5_main2_97833beb:


    th "Mi chiedo quale capitano abbia dato un nome del genere a un'isola?{w} Anche se in effetti, quell'isola è proprio la più vicina."


translate italian day5_main2_0fd8390d:


    me "Agli ordini, capitano!"


translate italian day5_main2_c5a9758a:


    th "Ho il vago sospetto che sarà un'impresa difficile…"


translate italian day5_main2_bb189b0d:


    "Non ero un vogatore esperto – avevo remato appena una o due volte in tutta la mia vita."


translate italian day5_main2_dfd96964:


    "L'isola era distante circa mezzo chilometro, ma stavamo procedendo a zig-zag, grazie alle mie «eccelse abilità»."


translate italian day5_main2_c1ed01f4:


    "Ma a circa metà strada le braccia hanno iniziato a farmi talmente male che ho dovuto abbandonare i remi per riposare un po'."


translate italian day5_main2_cf757ee3:


    me "Beh…{w} Dobbiamo proprio andare su quell'isola a raccogliere le fragole?{w} Voglio dire, non ci sono luoghi più accessibili?"


translate italian day5_main2_31c67473:


    sl "Ma quelle più gustose crescono lì."


translate italian day5_main2_683509ba:


    "Slavya mi ha guardato in modo perplesso."


translate italian day5_main2_6506e675:


    un "Ce la fai a remare da solo?"


translate italian day5_main2_e1e2ebb7:


    "Lena, a differenza di Slavya, aveva capito tutto."


translate italian day5_main2_b9dbf1a1:


    me "Oh…{w} Non è niente…"


translate italian day5_main2_7f474b89:


    "In ogni caso, non potevo lasciare che un'esile ragazza mi aiutasse."


translate italian day5_main2_255dc29b:


    "Il mio obbiettivo per il resto del tragitto era quello di cercare di restare in vita."


translate italian day5_main2_b4c0cce9:


    "Slavya e Lena discutevano di qualcosa, ma io non le stavo ascoltando – semplicemente non ne avevo la forza."


translate italian day5_main2_f0934032:


    "Finalmente siamo giunti a destinazione."


translate italian day5_main2_d6e51afd:


    "Sono uscito sulla riva, totalmente esausto, e ho osservato la rimessa per le barche in lontananza."


translate italian day5_main2_9289c81b:


    "Sembrava così lontana che mi sentivo come la prima persona sbarcata sulla Luna, che osservava l'alba della Terra."


translate italian day5_main2_8f83b289:


    sl "Ecco qui!"


translate italian day5_main2_e7fdbd78:


    "Slavya mi ha consegnato un cesto."


translate italian day5_main2_799068d0:


    "Era un'isola piuttosto piccola – larga appena un centinaio di metri."


translate italian day5_main2_b15dff1c:


    "E sembrava più un boschetto di betulle, con file di alberi che ricoprivano tutta la sua superficie."


translate italian day5_main2_5f826482:


    "Un calmo mare d'erba si estendeva sotto ai nostri piedi, con il soffice vento che di tanto in tanto generava onde solitarie."


translate italian day5_main2_2e1cc3e7:


    "L'isola sembrava un paradiso perduto."


translate italian day5_main2_6ec442f5:


    th "Non c'è da meravigliarsi se le fragole più deliziose crescono proprio qui."


translate italian day5_main2_7568fc9b:


    sl "Dobbiamo dividerci, così faremo prima."


translate italian day5_main2_d76f93fa:


    me "Sì, certo."


translate italian day5_main2_1f4c91db:


    un "Ma ci sono solo due cesti."


translate italian day5_main2_f6237fab:


    "Ha detto timidamente Lena."


translate italian day5_main2_e2c244d1:


    sl "Oh, hai ragione, colpa mia!"


translate italian day5_main2_c1db23a3:


    un "E quindi come ci dividiamo?"


translate italian day5_main2_94c61009:


    me "Lascia che venga con te."


translate italian day5_main2_23fc5b72:


    un "Andiamo…"


translate italian day5_main2_7f370439:


    sl "Ok, va bene!"


translate italian day5_main2_235efb86:


    "Slavya ha afferrato il secondo cesto e si è spinta fino all'altra estremità dell'isola."


translate italian day5_main2_b17829b3:


    me "Allora?"


translate italian day5_main2_3875ba7b:


    un "Cosa?"


translate italian day5_main2_71f4991e:


    me "Andiamo?"


translate italian day5_main2_37fef1ca:


    un "Sì."


translate italian day5_main2_561828ec_1:


    "Lena ha sorriso."


translate italian day5_main2_e937d3e9:


    me "Cerca con attenzione! Non dobbiamo tralasciare una singola fragola!"


translate italian day5_main2_51487473:


    un "Anche tu."


translate italian day5_main2_a20cefa7_1:


    "..."


translate italian day5_main2_a704bf84:


    "E così, la raccolta è iniziata."


translate italian day5_main2_afe45067:


    "Quelle fragole erano davvero deliziose."


translate italian day5_main2_de5b3003:


    "Probabilmente le avrei mangiate tutte se non mi fossi fermato in tempo."


translate italian day5_main2_f8b9b72c:


    th "Pur essendo delle fragole selvatiche, queste sono grandi come quelle da giardino, e sono di un colore rosso acceso, e quindi è chiaro che non siamo venuti qui invano."


translate italian day5_main2_bbcae5a7:


    "Io e Lena abbiamo camminato molto vicini, dato che il cesto era uno solo."


translate italian day5_main2_ac634faa:


    "Mi sentivo come un vero raccoglitore di funghi, esaminando ogni arbusto e frugando nell'erba con attenzione."


translate italian day5_main2_b7921614:


    un "Beh, sei molto più bravo di me…"


translate italian day5_main2_65640e4e:


    me "Davvero?{w} A dire il vero non le sto nemmeno contando."


translate italian day5_main2_0f83bc7d:


    un "Sì, lo sei davvero!"


translate italian day5_main2_435f0faa:


    "Il cesto era già mezzo pieno."


translate italian day5_main2_c88a4757:


    me "Devi proprio amare la natura, eh?"


translate italian day5_main2_f1be5fed:


    un "Sì."


translate italian day5_main2_5d5a082e:


    "I raggi luminosi del sole hanno trafitto le cime degli alberi, accecandomi per un secondo."


translate italian day5_main2_b6151df9:


    "Mi sono seduto per terra, appoggiandomi ad un albero."


translate italian day5_main2_7587bc3f:


    me "È proprio bello qui!"


translate italian day5_main2_9b60c62c:


    "Lena si è seduta accanto a me.{w} Così vicino che i nostri gomiti si toccavano."


translate italian day5_main2_46b74af0:


    un "Già!"


translate italian day5_main2_4bfe22ae:


    "Ci siamo seduti e ci siamo goduti il momento.{w} Era come se il tempo si fosse fermato."


translate italian day5_main2_9c76c985:


    "Il vento scuoteva delicatamente le foglie degli alberi, alcuni insetti saltellavano pigramente intorno al prato, e macchie brillanti di luce solare ondeggiavano in lontananza sulla superficie dell'acqua."


translate italian day5_main2_78fd2536:


    "Lena ha appoggiato la testa sulla mia spalla."


translate italian day5_main2_d1d0b93e:


    "All'inizio ne sono rimasto sorpreso, ma poi ho sentito il suo respiro regolare, e ho pensato che si trattasse semplicemente di un gesto spontaneo."


translate italian day5_main2_bbbf378c:


    "Probabilmente aveva sonno e ha deciso di fare un pisolino."


translate italian day5_main2_464ee2c5:


    "Sono stato lì così per qualche minuto, senza pensare a niente."


translate italian day5_main2_9dcce076:


    "Ma poi mille pensieri hanno iniziato ad attraversare la mia mente con velocità supersonica:"


translate italian day5_main2_4bf43e20:


    th "Lena.{w} Così vicina.{w} Sta dormendo.{w} È così calda.{w} Così tenera.{w} Che emozione…"


translate italian day5_main2_3a8d7079:


    "L'ho guardata."


translate italian day5_main2_a5887389:


    "Aveva un'espressione talmente serena e tranquilla che sembrava che in quel momento non fosse lì, ma in una sorta di mondo migliore."


translate italian day5_main2_f950a1bd:


    "Non so cos'altro sarebbe successo se non avessi sentito la voce di Slavya."


translate italian day5_main2_2295bc5f:


    sl "Semyon! Lena!"


translate italian day5_main2_fdadda66:


    "Ho scosso la testa un paio di volte per riprendermi.{w} Lena si è svegliata."


translate italian day5_main2_c0ebc5f2:


    "Ha aperto gli occhi e mi ha fissato con sguardo perso."


translate italian day5_main2_badffbcb:


    me "Dormito bene?"


translate italian day5_main2_d0ef9648:


    un "Eh?"


translate italian day5_main2_8d04e459:


    "Rendendosi conto di essersi appisolata sulla mia spalla, Lena è arrossita."


translate italian day5_main2_be58af4f:


    un "Oh… Scusa…"


translate italian day5_main2_6d36c28b:


    me "Non fa niente."


translate italian day5_main2_2eefbc0c:


    "Slavya è venuta da noi, e così Lena è balzata in piedi."


translate italian day5_main2_fde84cf5:


    sl "Quante ne avete raccolte?"


translate italian day5_main2_da3a9368:


    "Ho sospirato."


translate italian day5_main2_633224a4:


    sl "Non sono molte…"


translate italian day5_main2_380af4e6:


    "Il suo cesto era pieno fino all'orlo."


translate italian day5_main2_e384ce23:


    sl "Beh, sono comunque abbastanza!{w} È ora di tornare!"


translate italian day5_main2_7cd2b80a:


    "Ho afferrato il cesto e siamo tornati alla barca."


translate italian day5_main2_790a16f6:


    "Non avevo voglia di camminare lì da solo, e speravo che Slavya si unisse a me, ma non avevo il coraggio di chiederglielo."


translate italian day5_main2_e6680b45:


    me "Beh, è ovvio: un cesto per me, e uno per voi due."


translate italian day5_main2_97bf0978:


    sl "No, lascia che venga con te!"


translate italian day5_main2_c17a722d:


    "Slavya ha sorriso."


translate italian day5_main2_35ec24e9:


    me "Va bene…"


translate italian day5_main2_f8089075:


    "Sono rimasto un po' sorpreso, ma ero anche contento che le cose si fossero messe in quel modo."


translate italian day5_main2_46439482:


    "Lena sembrava non essere offesa dalla cosa."


translate italian day5_main2_a20cefa7_2:


    "..."


translate italian day5_main2_a704bf84_1:


    "La raccolta è iniziata."


translate italian day5_main2_afe45067_1:


    "E quelle fragole erano davvero deliziose."


translate italian day5_main2_de5b3003_1:


    "Probabilmente le avrei mangiate tutte se non mi fossi trattenuto."


translate italian day5_main2_f8b9b72c_1:


    th "Pur essendo delle fragole selvatiche, queste sono grandi come quelle da giardino, e sono di un colore rosso acceso, e quindi è chiaro che non siamo venuti qui invano."


translate italian day5_main2_11dc0364:


    "Slavya stava camminando accanto a me, dato che avevamo un solo cesto."


translate italian day5_main2_ac634faa_1:


    "Mi sentivo come un vero raccoglitore di funghi, esaminando ogni arbusto e frugando nell'erba con attenzione."


translate italian day5_main2_9f273fda:


    sl "Fai attenzione!"


translate italian day5_main2_eebba4df:


    "Avevo trascurato un intero gruppo di fragole."


translate italian day5_main2_cd47bc4e:


    me "Ah, già…{w} Scusa."


translate italian day5_main2_705bea28:


    sl "Fa niente."


translate italian day5_main2_9db1f947:


    me "Deve proprio piacerti stare qui, vero?{w} Dopotutto ami la natura."


translate italian day5_main2_49cc0ae4:


    sl "Certo che sì!"


translate italian day5_main2_c17a722d_1:


    "Slavya ha sorriso."


translate italian day5_main2_0854ae45:


    sl "Mi ricorda la mia casa – anche noi abbiamo delle betulle simili a queste."


translate italian day5_main2_f32c544b:


    "Ha guardato da qualche parte in lontananza, con aria sognante."


translate italian day5_main2_14e38355:


    me "Senti, ho sempre voluto chiedertelo – cosa ti piace in generale?"


translate italian day5_main2_ed0e2cff:


    me "Sembri essere occupata ventiquattr'ore al giorno, senza avere un attimo di riposo."


translate italian day5_main2_7db509b3:


    sl "Ohh…"


translate italian day5_main2_60f3d758:


    "Ha pensato un po'."


translate italian day5_main2_63626b5d:


    sl "Davvero non saprei.{w} Mi piace fare svariate attività."


translate italian day5_main2_882dd540:


    me "Beh, capisco, ma una cosa in particolare?"


translate italian day5_main2_9698acdd:


    sl "Mi piace lavorare a maglia, e il cucito!{w} Cose del genere…"


translate italian day5_main2_f22d26a4:


    "Slavya ha tolto un fazzoletto dalla sua tasca.{w} C'erano dei fiori rossi, gialli e verdi ricamati su di esso."


translate italian day5_main2_aef33dd6:


    "Erano intrecciati tra loro in modo complesso, creando sofisticate forme geometriche."


translate italian day5_main2_576c9c96:


    "Un tipico fazzoletto russo fatto a mano."


translate italian day5_main2_fa5e2895:


    "Mi sono subito immaginato Slavya vestita di un antico sarafan, seduta su una panchina accanto a una casa fatiscente, con un gruppo di bambini che correvano tutt'attorno."


translate italian day5_main2_9918c29c:


    me "È piuttosto carino."


translate italian day5_main2_8c1bfef0:


    sl "Grazie!{w} Prendilo, te lo regalo!"


translate italian day5_main2_95e49024:


    "Tale proposta mi ha messo in imbarazzo."


translate italian day5_main2_08a40647:


    me "Non dovresti…"


translate italian day5_main2_127a57de:


    sl "No, prendilo!"


translate italian day5_main2_72fe7131:


    "Ho guardato il fazzoletto ancora una volta e poi me lo sono messo in tasca."


translate italian day5_main2_73aab3cd:


    me "Ti ringrazio…"


translate italian day5_main2_a20cefa7_3:


    "..."


translate italian day5_main2_ff2ae452:


    "C'erano talmente tante fragole qui che dopo solo mezz'ora avevamo già il cesto pieno fino all'orlo."


translate italian day5_main2_fb9748eb:


    me "Pare che abbiamo finito…"


translate italian day5_main2_e498e9bd:


    sl "Sì.{w} Ne abbiamo raccolte un sacco, saranno sicuramente abbastanza."


translate italian day5_main2_f110cff6:


    "Quando siamo arrivati alla barca, Lena non era ancora tornata."


translate italian day5_main2_b852b10d:


    sl "Ha bisogno di più tempo per riempire il cesto da sola."


translate italian day5_main2_ff886d6d:


    me "Già, suppongo…"


translate italian day5_main2_94972c05:


    "Ho guardato il fiume."


translate italian day5_main2_827d1834:


    "Il bagliore del sole danzante sulla superficie dell'acqua era l'unica cosa che la distingueva da uno specchio – ecco quanto era calmo quel fiume."


translate italian day5_main2_29dcd13b:


    sl "A cosa stai pensando?"


translate italian day5_main2_51eebe65:


    me "Niente in particolare…{w} E tu?"


translate italian day5_main2_7be9fd97:


    sl "Io…?{w} Che cosa accadrà dopo che la vacanza sarà finita? Dovremo lasciare questo Campo e tornare alle nostre case…"


translate italian day5_main2_08224f21:


    sl "Riuscirò mai a rivedere qualcuno che ho conosciuto qui?{w} Riuscirò mai a rivedere te...?"


translate italian day5_main2_e932afd0:


    "Mi ha guardato con occhi talmente tristi che non sapevo proprio cosa dire."


translate italian day5_main2_bffe94f9:


    "Lena è sbucata fuori dal nulla, rompendo il silenzio."


translate italian day5_main2_bfcc6d0c:


    un "Oh, avete già finito…{w} Ecco qua."


translate italian day5_main2_26b9d76b:


    "Ci ha mostrato il suo cesto pieno di fragole."


translate italian day5_main2_e2c68eb7:


    sl "Grande!{w} Ora possiamo tornare indietro."


translate italian day5_main2_3ea9d288:


    "Nel frattempo le parole e l'espressione di Slavya mi giravano ancora nella testa."


translate italian day5_main2_9b1d3958:


    "Tristezza e dolore non erano da lei."


translate italian day5_main2_8bcb5251:


    th "Potrebbe forse nasconderle dietro a una maschera di allegria?"


translate italian day5_main2_54c1d08a:


    "Sapevo di non avere una risposta a una tale domanda, e che non ne avrei trovata alcuna."


translate italian day5_main2_e3a29d10:


    th "Magari più tardi…"


translate italian day5_main2_4d28e778:


    "Il viaggio di ritorno è durato meno del previsto, dato che ero totalmente concentrato sul remare, ignorando tutto il resto."


translate italian day5_main2_1012dd1b:


    "Il mio unico pensiero era tornare indietro vivo, dato che il viaggio di andata non era stato senza conseguenze, e a quel punto le mie braccia hanno iniziato a far male dopo appena poche remate."


translate italian day5_main2_d6437409:


    "Dopo aver legato la barca al molo, mi sono lasciato cadere a terra, sfinito."


translate italian day5_main2_ba4a18aa:


    "Slavya e Lena si sono chinate sopra di me."


translate italian day5_main2_379cb129:


    sl "Avresti potuto dirci che era così faticoso per te!"


translate italian day5_main2_31b01f67:


    un "Sì…"


translate italian day5_main2_bb2ef35c:


    me "Non importa, sto bene…{w} Resterò qui ancora un po' e mi riprenderò, non preoccupatevi…"


translate italian day5_main2_50def11b:


    sl "Ok, allora dopo riporta i cesti a Olga Dmitrievna per favore, noi abbiamo un'altra cosa da fare."


translate italian day5_main2_85279b68:


    me "Nessun problema."


translate italian day5_main2_6d720424:


    "Sarei stato d'accordo con qualsiasi cosa in quel momento, solo per evitare di dovermi alzare."


translate italian day5_main2_3bfb15a5:


    "Slavya ha appoggiato i due cesti pieni di fragole accanto a me e si è diretta verso la piazza, chiacchierando allegramente con Lena."


translate italian day5_main2_cb288325:


    me "La parte più difficile è terminata…"


translate italian day5_main2_258341cc:


    "O almeno era quello che credevo prima di aver sollevato i due cesti…"


translate italian day5_main2_f1fdfa2c:


    "Dopo quello sforzo in barca sembravano due sacchi di cemento, anche se pesavano poco più di un paio di chili ciascuno."


translate italian day5_main2_331ec88f:


    "E così il viaggio verso la casetta della leader del Campo è durato molto più del previsto – continuavo a fermarmi ogni cinquanta metri per riprendere fiato."


translate italian day5_main2_3f67afab:


    "Quando finalmente sono arrivato a destinazione, ho appoggiato i cesti a terra e mi sono seduto sulla sdraio con difficoltà."


translate italian day5_main2_30582463:


    me "Olga Dmitrievna!{w} Olga Dmitrievna, ho un regalo per lei!"


translate italian day5_main2_dfffcbac:


    "Nessuno ha risposto."


translate italian day5_main2_a11cb528:


    "Sono riuscito a malapena ad alzarmi e ad entrare nella casetta."


translate italian day5_main2_40d2a09a:


    "Non c'era nessuno."


translate italian day5_main2_d189a7a2:


    me "Se non ne hai bisogno – sono fatti tuoi…"


translate italian day5_main2_c3f0764a:


    "Mi sono disteso sulla sdraio e mi sono addormentato."


translate italian day5_main2_8bc92ca0:


    "Ho fatto uno strano sogno riguardante un inseguimento di fragole."


translate italian day5_main2_f12d326b:


    "Ero su una barca e stavo remando con le mie ultime energie, cercando di sfuggire a delle enormi fragole che mi stavano inseguendo."


translate italian day5_main2_b573ff1d:


    "Le mie braccia stavano cedendo, e riuscivo a malapena a vedere qualcosa a causa del sudore che mi scorreva lungo la fronte. Il sangue mi martellava nelle tempie, e le fragole si stavano avvicinando sempre più."


translate italian day5_main2_371f84cd:


    "I loro denti aguzzi mi avevano quasi raggiunto."


translate italian day5_main2_57cc90da:


    th "Aspetta un attimo…{w} Delle fragole con i denti?!"


translate italian day5_main2_bed90e86:


    mt "…Semyon! Semyon!"


translate italian day5_main2_4b4e1333:


    "Mi sono svegliato."


translate italian day5_main2_821dbe9f:


    "Olga Dmitrievna stava in piedi accanto a me, scuotendomi la spalla."


translate italian day5_main2_4f14ab7e:


    mt "Vedo che la raccolta è stata abbondante, non è così?"


translate italian day5_main2_f0de0091:


    mt "Bene, ma non è ancora tutto!"


translate italian day5_main2_b635cee7:


    th "Seriamente, mi stavo già pregustando il mio meritato riposo che sarebbe arrivato di lì a poco…"


translate italian day5_main2_6091ae1b:


    mt "Sai almeno a cosa servono queste fragole?"


translate italian day5_main2_230628fb:


    me "Non ne ho idea…"


translate italian day5_main2_f1e8b4cf:


    "Che sincerità."


translate italian day5_main2_035bcc73:


    mt "Le useremo per fare una torta!"


translate italian day5_main2_f2e007f0_1:


    me "Capisco…"


translate italian day5_main2_918f0332:


    th "Beh, ha un certo senso."


translate italian day5_main2_cf6837ea:


    mt "Per festeggiare il miracoloso salvataggio di Shurik!{w} Ed è stato tutto merito tuo!"


translate italian day5_main2_e8bfedc9:


    "Era chiaro che ottenere le fragole non fosse l'ultima cosa rimasta da fare."


translate italian day5_main2_b125d822:


    th "E mi potresti spiegare perché dovrei organizzare la festa da solo, essendo io l'eroe della situazione?"


translate italian day5_main2_443723fe:


    me "Beh, suppongo…"


translate italian day5_main2_cb1beef9:


    mt "Allora…{w} Avrei un compito importante per te!"


translate italian day5_main2_a8dc8e24:


    mt "Ci manca il lievito, la farina e lo zucchero.{w} E abbiamo bisogno di tutto prima di cena!"


translate italian day5_main2_1cdc0576:


    me "E coloro che devono preparare la torta non possono arrangiarsi in qualche modo a procurarsi gli ingredienti?"


translate italian day5_main2_13e8802b:


    "Ho supplicato."


translate italian day5_main2_4cb23232:


    mt "Ma certo che non possono!{w} Sono tutti molto impegnati!{w} E tu sei l'unico nell'intero Campo che non sta facendo niente!"


translate italian day5_main2_c4eaf48d:


    "Seppur le sue parole erano in parte vere, non mi stava rendendo le cose più facili."


translate italian day5_main2_aefacb3e:


    "Inoltre, quelle parole mi sono arrivate come un proiettile nella testa."


translate italian day5_main2_05b86f04:


    mt "Allora, prendi nota!{w} Il lievito è all'infermeria, la farina nella biblioteca, e lo zucchero nella sede dei circoli."


translate italian day5_main2_42814c09:


    me "Aspetti, aspetti un..."


translate italian day5_main2_e9da3bbd:


    mt "Non ho tempo, vado proprio di fretta!{w} Buona fortuna!"


translate italian day5_main2_0457a719:


    "Mi ha detto con sguardo sornione, e se n'è andata."


translate italian day5_main2_8824565a:


    th "Certo che ci sono un sacco di cose strane in questo Campo, ma…{w} Il lievito all'infermeria? Ok, posso capirlo. Ma!{w} La farina nella biblioteca?"


translate italian day5_main2_dd117348:


    th "E lo zucchero…{w} No, è ben oltre la mia comprensione!"


translate italian day5_main2_d71f77ee:


    "Ho sputato per terra."


translate italian day5_main2_6a2ac63d:


    me "Non voglio credere a queste cose, e non ci crederò! Allora dimmi, dimmelo che mi stai prendendo in giro."


translate italian day5_main2_2b37191e:


    "Non mi sarei sorpreso di vedere una folla di grassi e verdi troll apparire proprio qui accanto a me in quel preciso istante, cominciando a schernirmi come se si sentissero in qualche modo obbligati."


translate italian day5_main2_b150a7df:


    th "Quindi, forse posso mandare al diavolo questa torta...?"


translate italian day5_main2_f58ed270:


    "Ho dato il giusto peso alle mie osservazioni per qualche minuto."


translate italian day5_main2_c815c3c0:


    th "No, se un piano talmente ambizioso di Olga Dmitrievna dovesse andare a rotoli, mi ritroverei in un mare di guai."


translate italian day5_main2_7cca4369:


    th "E mi complicherei solo la vita qui nel Campo, per non parlare del mio obbiettivo principale di trovare risposte, che negli ultimi giorni ho un po' trascurato."


translate italian day5_main2_515e2ec4:


    th "Pare che io non abbia altra scelta…"


translate italian day5_aidpost_2_71caa85a:


    th "Mi sembra che io stia visitando l'infermeria troppo di frequente, ultimamente."


translate italian day5_aidpost_2_7a9eec06:


    th "Ma che ci posso fare – è così che vanno le cose."


translate italian day5_aidpost_2_af5fbbdb:


    "Ho sospirato e ho bussato alla porta."


translate italian day5_aidpost_2_e9203c64:


    cs "Avanti!"


translate italian day5_aidpost_2_e6e52c0a:


    "Ha detto l'infermiera, in una sorta di tono cantilenante."


translate italian day5_aidpost_2_b9a6c45b:


    me "Buon pomeriggio!{w} Olga Dmitrievna mi ha mandato a prendere un po' di…"


translate italian day5_aidpost_2_5286c4cf:


    "Ho esitato un po'."


translate italian day5_aidpost_2_68451631:


    me "Lievito…"


translate italian day5_aidpost_2_2d204cc8:


    cs "Ah, certo."


translate italian day5_aidpost_2_6d77584d:


    "Mi ha fatto un ampio sorriso."


translate italian day5_aidpost_2_524e70a4:


    cs "È solo che non ce l'ho…{w} pioniere."


translate italian day5_aidpost_2_e8bf1827:


    me "E come mai?{w} Mi aveva detto che…"


translate italian day5_aidpost_2_48f8061d:


    cs "Beh, ne avevo un po', ma adesso non ce n'è più."


translate italian day5_aidpost_2_62e70926:


    "Non mi sono nemmeno scomodato a chiederle perché l'avesse tenuto lì, in primo luogo."


translate italian day5_aidpost_2_55f0b627:


    cs "Beh, non ti preoccupare.{w} Ti posso offrire un'aspirina, ad esempio."


translate italian day5_aidpost_2_624f6a17:


    th "Potrebbe tornarmi utile, in effetti."


translate italian day5_aidpost_2_ec7a55c9:


    me "E allora dove lo trovo…?"


translate italian day5_aidpost_2_ed0914b5:


    "Ho sospirato."


translate italian day5_aidpost_2_19a3e4d7:


    cs "Prendi questa!"


translate italian day5_aidpost_2_4fb9e776:


    "Ha aperto il cassetto e ha tirato fuori una bottiglia."


translate italian day5_aidpost_2_3a770cd2:


    "L'ho guardata meglio.{w} Era una birra «Ostankinskoe»."


translate italian day5_aidpost_2_51149e68:


    me "…"


translate italian day5_aidpost_2_94e2a40d:


    cs "Qual è il problema?{w} Anche la birra è un prodotto fermentato."


translate italian day5_aidpost_2_8fb93608:


    "Mi ha lanciato uno sguardo profondo."


translate italian day5_aidpost_2_97a20ab2:


    cs "Non se ne accorgerà nessuno!"


translate italian day5_aidpost_2_51d589e0:


    "C'era della logica nelle sue parole, ma tutto questo mi è sembrato talmente grottesco che non riuscivo a trovare le parole giuste per dire qualcosa a riguardo."


translate italian day5_aidpost_2_d9f00730:


    me "Ma... è sicura?"


translate italian day5_aidpost_2_a61a2efe:


    cs "Assolutamente!"


translate italian day5_aidpost_2_d76d86ac:


    me "Va bene allora…"


translate italian day5_aidpost_2_fe10b3f1:


    "Chiaramente non potevo tenere quella bottiglia nella tasca dei miei pantaloncini."


translate italian day5_aidpost_2_73aab3cd:


    me "Bene, grazie…"


translate italian day5_aidpost_2_e7fd1586:


    "Ho mormorato timidamente, lasciando l'infermeria."


translate italian day5_aidpost_2_942696fd:


    th "Beh, certamente la birra potrebbe sostituire il lievito."


translate italian day5_aidpost_2_a2b4dc7e:


    th "Persino la mia limitata conoscenza della chimica e della biologia è sufficiente ad accettare questa cosa.{w} Ma…"


translate italian day5_aidpost_2_4997ec0f:


    "In generale, andare in giro con quella bottiglia in mano mi è sembrata un'idea stupida, così ho deciso di portarla nella stanza di Olga Dmitrievna e di nasconderla lì."


translate italian day5_aidpost_2_aeaa1520:


    "Ma avrei dovuto raggiungerla in qualche modo, senza farmi vedere da nessuno."


translate italian day5_aidpost_2_f988b661:


    "Ho nascosto la bottiglia sotto la camicia."


translate italian day5_aidpost_2_b2e65042:


    "E tutto sarebbe andato liscio, se non fosse stato per Slavya che mi ha chiamato dalla piazza."


translate italian day5_aidpost_2_9b22a2b9:


    "A dire il vero, mi ha talmente colto di sorpresa che ho sobbalzato."


translate italian day5_aidpost_2_99a001ee:


    sl "Come va?"


translate italian day5_aidpost_2_ad02f53a:


    me "Cosa esattamente?"


translate italian day5_aidpost_2_28a39c69:


    sl "La tua ricerca degli ingredienti."


translate italian day5_aidpost_2_ed34a1cf:


    me "Ah, dunque sai già tutto…"


translate italian day5_aidpost_2_2bb8d573:


    sl "Sì!"


translate italian day5_aidpost_2_c17a722d:


    "Slavya ha sorriso."


translate italian day5_aidpost_2_7de85321:


    me "Sta andando bene…"


translate italian day5_aidpost_2_f7e1cdf8:


    "Ho risposto, cercando di non mostrare traccia della mia inquietudine."


translate italian day5_aidpost_2_9d5782dc:


    sl "E che cos'hai lì?"


translate italian day5_aidpost_2_c76c023d:


    "Ha indicato la bottiglia che spuntava da sotto la mia camicia."


translate italian day5_aidpost_2_5652d5eb:


    me "Ah, questa…"


translate italian day5_aidpost_2_bda7946b:


    th "Mi ha beccato!"


translate italian day5_aidpost_2_4c420dcf:


    me "Ah, non è niente…"


translate italian day5_aidpost_2_478e684d:


    "Sono arrossito, facendo una sciocca risatina."


translate italian day5_aidpost_2_d96f5cdf:


    me "Adesso devo andare…"


translate italian day5_aidpost_2_bc62fb23:


    "Stavo quasi correndo, allontanandomi dalla piazza e dalla perplessa Slavya."


translate italian day5_aidpost_2_74475de8:


    th "La cosa positiva è che lei è una di quelle persone che non fanno domande inutili."


translate italian day5_aidpost_2_bd1cc79d:


    "Mentre invece c'erano persone in quel Campo che non avevano niente di meglio da fare che ficcare il naso nelle faccende altrui…"


translate italian day5_aidpost_2_0616ad18:


    "Passando accanto alle casette dei pionieri, sono incappato in Ulyana."


translate italian day5_aidpost_2_397ec628:


    us "Cosa stai nascondendo lì?"


translate italian day5_aidpost_2_07c1adb6:


    "Mi ha lanciato uno dei suoi sguardi impertinenti."


translate italian day5_aidpost_2_6ada69b4:


    "Non aveva senso negare l'evidenza, così le ho risposto in modo provocatorio:"


translate italian day5_aidpost_2_38b6a274:


    me "Non sono affari tuoi! Sono un ufficiale che porta un messaggio segreto al quartier generale."


translate italian day5_aidpost_2_1ebafc67:


    us "Dev'essere proprio un grosso…{w} messaggio…"


translate italian day5_aidpost_2_fd58cad9:


    "Stavo tenendo la bottiglia proprio in quel punto davanti a me, e quindi è stato piuttosto imbarazzante."


translate italian day5_aidpost_2_d8a23d43:


    us "Ti serve una mano?"


translate italian day5_aidpost_2_5fc5692c:


    me "Me ne occupo io!"


translate italian day5_aidpost_2_5a4c3675:


    "Sono passato davanti a lei con decisione e ho proseguito per la mia strada."


translate italian day5_aidpost_2_fbcbd186:


    "Con mia grande sorpresa non mi ha detto nulla, né ha cercato di perseguirmi."


translate italian day5_aidpost_2_1266f2bc:


    "Non c'era nessuno nella stanza di Olga Dmitrievna, così sono riuscito a nascondere con successo la bottiglia sotto al mio letto."


translate italian day5_aidpost_2_4e945031:


    "Una volta portata a termine la missione ho tirato un sospiro di sollievo."


translate italian day5_aidpost_2_74665f46:


    "Davvero, non credevo che mi sarei mai preoccupato tanto per una semplice bottiglia di birra!{w} Come se fossi ancora al liceo."


translate italian day5_aidpost_2_af3e0460:


    th "È bene che sia nascosta al sicuro…"


translate italian day5_aidpost_2_ceb963db:


    th "Anche se qualcuno la trovasse, dirò che non è mia – data la mia grande esperienza in questo campo, riesco sempre a inventarmi una giustificazione adeguata."


translate italian day5_clubs_2_bb74ce24:


    "Mi è sembrato di aver vissuto più avventure oggi che in tutti i giorni precedenti messi assieme."


translate italian day5_clubs_2_7fddb993:


    "E così, mentre mi stavo avvicinando alla sede dei circoli, mi sono dimenticato di come sarebbe stato goffo chiedere dello zucchero in quel luogo."


translate italian day5_clubs_2_ebe5438a:


    "Shurik ed Electronik stavano costruendo qualcosa con entusiasmo."


translate italian day5_clubs_2_2e2f3c1b:


    "Erano così concentrati che non si sono nemmeno accorti della mia presenza."


translate italian day5_clubs_2_2b02462e:


    "Ho guardato più da vicino."


translate italian day5_clubs_2_3af379d1:


    "Era una specie di robot…{w} O almeno il corpo di un robot."


translate italian day5_clubs_2_729ff343:


    "Inoltre, quel robot era femminile, e aveva delle orecchie da animale."


translate italian day5_clubs_2_a869bd00:


    "Non volevo pensare ad alcuna teoria riguardo allo scopo di un tale dispositivo, creato dai luminari della cibernetica campestre."


translate italian day5_clubs_2_c69b15fb:


    "Anche se il design non era nulla di impossibile, avevo i miei dubbi sul fatto che quel robot potesse essere in grado di conquistare la Terra, o almeno non ci sarebbe riuscito da solo."


translate italian day5_clubs_2_81faf816:


    "Ma loro sembravano godersi il processo più del risultato finale."


translate italian day5_clubs_2_76cfb844:


    "E questo era un atteggiamento che avevamo in comune, anche se non volevo ammetterlo."


translate italian day5_clubs_2_d63ce887:


    "D'altra parte non avevano timore di un possibile fallimento, di ricevere critiche o di essere presi in giro...{w} Stavano collaborando per raggiungere il loro obiettivo senza interessarsi di cosa ne pensassero gli altri, che lo avrebbero definito surreale o persino assurdo."


translate italian day5_clubs_2_ff329017:


    th "Oh, sembra davvero che io li stia paragonando ai luminari della scienza..."


translate italian day5_clubs_2_b90af56a:


    me "Ehi ragazzi."


translate italian day5_clubs_2_853a9b87:


    "Li ho salutati in modo incerto."


translate italian day5_clubs_2_5c419eaf:


    sh "Oh! Semyon! Entra pure, siamo sempre contenti di vederti!"


translate italian day5_clubs_2_d193794e:


    th "In realtà sono già entrato…"


translate italian day5_clubs_2_2bd5ad91:


    sh "Senti…{w} Mi dispiace per quello che è successo ieri!{w} Non ricordo quasi niente, ma…{w} Comunque…"


translate italian day5_clubs_2_95e7fcc0:


    me "Non preoccuparti! È tutto a posto."


translate italian day5_clubs_2_f3cf77af:


    el "E cosa ti porta alla nostra umile dimora?"


translate italian day5_clubs_2_e7ed1aae:


    "Electronik mi ha guardato sornione."


translate italian day5_clubs_2_f069bcd3:


    "Certe volte avevo come la sensazione che dietro a quelle sue facce lui volesse far intendere di sapere un qualcosa di scomodo sul suo interlocutore, un qualcosa da usare come asso nella manica."


translate italian day5_clubs_2_88b7e8e0:


    me "Zucchero.{w} Ho bisogno di zucchero."


translate italian day5_clubs_2_e4fdc3d2:


    "Mi è improvvisamente venuta in mente la scena di un vecchio videogioco, dove una sorta di unità costruttrice o qualcosa del genere, gridava forte dall'elevata statura dei suoi cinque pixel: «Oro! Abbiamo bisogno di più oro!»"


translate italian day5_clubs_2_9965d584:


    el "Ce l'abbiamo."


translate italian day5_clubs_2_9b27ce05:


    "Ha detto Electronik con calma."


translate italian day5_clubs_2_edb7828d:


    sh "A cosa ti serve?"


translate italian day5_clubs_2_6a1f6f30:


    "Non volevo dire a Shurik che stavano preparando una torta per lui.{w} Gli avrei rovinato la sorpresa."


translate italian day5_clubs_2_cea669a0:


    me "Non lo so…{w} Olga Dmitrievna mi ha chiesto di portarglielo…"


translate italian day5_clubs_2_f7be46a6:


    el "Ok, aspetta."


translate italian day5_clubs_2_c1165691:


    "Electronik è scomparso dietro alla porta nella stanza accanto."


translate italian day5_clubs_2_72678105:


    me "E perché avete lo zucchero qui? Perché non in mensa?"


translate italian day5_clubs_2_c5831f8c:


    sh "Quando il camion del cibo è arrivato l'ultima volta, era l'ultima cosa rimasta da scaricare."


translate italian day5_clubs_2_8afaf429:


    sh "E dato che il nostro edificio è il più vicino all'entrata, hanno deciso di lasciarlo qui per evitare sforzi…"


translate italian day5_clubs_2_9b463aae:


    th "Una spiegazione ragionevole, non è vero?"


translate italian day5_clubs_2_bb104bbc:


    "La porta si è aperta, rivelando Electronik che trascinava un sacco enorme dietro di sé."


translate italian day5_clubs_2_d24fcbc5:


    "Non avevo idea delle dimensioni della torta che volevano preparare, ma quello era decisamente {i}troppo{/i} zucchero."


translate italian day5_clubs_2_30543032:


    me "Bene, grazie, ma non mi serve tutto…"


translate italian day5_clubs_2_776a0c07:


    el "Ma dove potremmo metterlo?"


translate italian day5_clubs_2_961b180e:


    "Electronik mi ha guardato sorpreso."


translate italian day5_clubs_2_590bb4f8:


    el "Qui non c'è più posto.{w} Ci hai chiesto dello zucchero – quindi adesso prendilo."


translate italian day5_clubs_2_ec12a3f5:


    th "Sembra che quel suo precedente sorriso non sia stato senza uno scopo."


translate italian day5_clubs_2_0cff3fa1:


    me "Forse mi potreste aiutare?{w} Non è molto lontano da trasportare…"


translate italian day5_clubs_2_40b931b1:


    el "Siamo impegnati."


translate italian day5_clubs_2_07640196:


    "Ha indicato il robot."


translate italian day5_clubs_2_c06e6e9a:


    "Ho guardato Shurik.{w} Dopotutto, mi doveva un favore."


translate italian day5_clubs_2_d7fa97fa:


    "Ha esitato e poi ha distolto lo sguardo, vergognandosi."


translate italian day5_clubs_2_9b5a3058:


    "Ho sospirato, ho afferrato il sacco e l'ho trascinato verso la porta."


translate italian day5_clubs_2_d9acf4d6:


    me "Grazie comunque…"


translate italian day5_clubs_2_d52a2c3a:


    "Ho detto, prima di avviarmi nel mio viaggio tedioso."


translate italian day5_clubs_2_256429b1:


    "Ma non sono riuscito ad arrivare molto lontano."


translate italian day5_clubs_2_87117e72:


    "Dopo solo una ventina di metri ho dovuto appoggiare a terra il sacco, per fare una pausa."


translate italian day5_clubs_2_0fd93e15:


    "Non avevo idea di quanto pesasse, ma a occhio sembravano più di venti chili."


translate italian day5_clubs_2_4099b982:


    "Da un lato erano solo duecento metri fino alla mensa."


translate italian day5_clubs_2_e147e5e5:


    "Ma dall'altro, percorrere quel breve tratto con quel sacco sulle mie spalle (o alternativamente tra le mie mani, o sulle mie gambe, o sotto al mio braccio, o persino sulla mia testa) sembrava un'impresa impossibile."


translate italian day5_clubs_2_d88fce53:


    "E dopo essermi rassegnato a dover procedere a tratti, con pause prolungate nel mezzo (almeno così sarei potuto arrivare entro la notte), ho sentito una voce dietro di me:"


translate italian day5_clubs_2_73f17541:


    un "Forse posso aiutarti?"


translate italian day5_clubs_2_1cd2d1a1:


    "Lena stava di fronte a me."


translate italian day5_clubs_2_5cbd7cac:


    me "Non credo proprio…"


translate italian day5_clubs_2_2fb0f57d:


    "Era uno di quei momenti in cui mi sono drammaticamente reso conto di quanto fossi fuori forma…"


translate italian day5_clubs_2_be079536:


    un "Posso portarti un carretto."


translate italian day5_clubs_2_dbff5c0c:


    th "Un carretto…{w} Perché non ci ho pensato io?!"


translate italian day5_clubs_2_02b2a515:


    me "Sì, sarebbe fantastico!"


translate italian day5_clubs_2_a8585d26:


    un "Aspetta qui, torno subito!"


translate italian day5_clubs_2_47fe246e:


    "Mi ha sorriso ed è corsa verso la piazza."


translate italian day5_clubs_2_e8fbb4d7:


    th "Cosa farei senza di lei…?"


translate italian day5_clubs_2_8efa90a2:


    th "È un bene che Lena non sia sempre così timida, e che a volte prenda l'iniziativa."


translate italian day5_clubs_2_89141735:


    "Ho iniziato a pensare."


translate italian day5_clubs_2_952b5881:


    "In quel momento non mi è sembrata sé stessa.{w} Senza alcuna traccia di timidezza sul volto, anzi l'esatto opposto – sorridente e sicura di sé…"


translate italian day5_clubs_2_017038ff:


    "Un'offerta di aiuto non era niente di speciale, ma dato che era da parte di Lena…"


translate italian day5_clubs_2_a1ed0501:


    "Pochi minuti più tardi è tornata con un carretto piuttosto piccolo."


translate italian day5_clubs_2_3fed3b65:


    "Ho caricato il sacco su di esso."


translate italian day5_clubs_2_3066598f:


    me "Grazie."


translate italian day5_clubs_2_69777dad:


    un "Non c'è di che…"


translate italian day5_clubs_2_42f94d04:


    "È arrossita, abbassando lo sguardo."


translate italian day5_clubs_2_3ecb53f0:


    th "Oh, la Lena che tutti conosciamo è tornata!"


translate italian day5_clubs_2_ab1d28c0:


    un "Bene, allora io vado…"


translate italian day5_clubs_2_45858c8d:


    me "Sì, ci vediamo!{w} E grazie ancora!"


translate italian day5_clubs_2_3da1183b:


    "Le ho gridato alle spalle."


translate italian day5_clubs_2_d45c5c2b:


    "Certe volte mi sembrava di vedere due persone diverse che vivevano all'interno di Lena."


translate italian day5_clubs_2_0b282078:


    "Ma la seconda – fiduciosa, allegra e talvolta persino audace – appariva solo quando parlava con me."


translate italian day5_clubs_2_9a4a089a:


    th "O mi sto immaginando di nuovo le cose...?"


translate italian day5_clubs_2_fce0897d:


    "Ho pensato che sarebbe stato meglio portare alla mensa tutti gli ingredienti in una volta sola, così mi sono diretto verso la casetta di Olga Dmitrievna con il carretto."


translate italian day5_library_2_d8c69e27:


    "Se tutti gli altri luoghi dov'erano gli ingredienti della mia lista mi sembravano avere un certo senso, la farina nella biblioteca non lo aveva affatto."


translate italian day5_library_2_94112c60:


    "Ho pensato a lungo a chi potesse averla messa nella biblioteca, ma non ho trovato alcuna spiegazione logica."


translate italian day5_library_2_328028d5:


    "Dato il caratterino di Zhenya, era meglio bussare alla porta."


translate italian day5_library_2_9b83bcec:


    mz "Aperto!"


translate italian day5_library_2_ccef914a:


    "Zhenya mi ha scrutato attentamente da dietro gli occhiali."


translate italian day5_library_2_6705d775:


    mz "Cosa vuoi?"


translate italian day5_library_2_e39034b2:


    me "Ehm…{w} Non pensare a nulla di strano, ma ho bisogno di…"


translate italian day5_library_2_511a0256:


    "Non volevo fare la figura dell'idiota, così ho deciso di spiegarle le cose in dettaglio."


translate italian day5_library_2_c2c36c71:


    me "Mi serve della farina. Olga Dmitrievna ha detto che è qui. Capisco che può sembrare strano tenere la farina nella biblioteca, ma…"


translate italian day5_library_2_16f45806:


    me "Mi ha mandato da te. E ci serve per fare una torta, per celebrare il salvataggio di Shurik."


translate italian day5_library_2_e3838181:


    mz "Sì, ho la farina, che c'è di così strano?"


translate italian day5_library_2_80c6622f:


    "Mi ha risposto Zhenya, sorpresa."


translate italian day5_library_2_f64f4e8b:


    "In quel momento mi è sembrato di essere stato colpito alla testa da un oggetto pesante, e non riuscivo a capirci più nulla."


translate italian day5_library_2_079c3fed:


    th "Farina nella biblioteca…{w} Certo, cosa può esserci di strano…"


translate italian day5_library_2_e29b49ed:


    th "Dopotutto siamo nel Paese delle Meraviglie, io sono Alice, ora sto andando a mangiare quel fungo magico e tra poco tornerò a casa…"


translate italian day5_library_2_fe1f361c:


    mz "Ehi!"


translate italian day5_library_2_ca8c1883:


    me "Eh? Sì!"


translate italian day5_library_2_5c270e5d:


    "Stavo sognando ad occhi aperti."


translate italian day5_library_2_e683043b:


    mz "Aspetta qui, torno subito."


translate italian day5_library_2_660215b2:


    "È scomparsa dietro agli scaffali, mentre io ho incrociato le braccia nell'attesa."


translate italian day5_library_2_c8ef07cc:


    "Un attimo più tardi sono stato raggiunto dal suono di una botola che cigolava sui suoi cardini."


translate italian day5_library_2_21111af0:


    me "Ehi, hai bisogno di aiuto?"


translate italian day5_library_2_91d35e95:


    "Ho chiesto ad alta voce."


translate italian day5_library_2_644e33dc:


    mz "Ce la faccio da sola!"


translate italian day5_library_2_47919150:


    "Ha abbaiato Zhenya."


translate italian day5_library_2_dcc13160:


    mz "Se non erro è nel seminterrato, quindi dovrai aspettare un po'."


translate italian day5_library_2_d08df322:


    me "Oki-doki…"


translate italian day5_library_2_0a82bf01:


    "I minuti passavano ma Zhenya non era ancora tornata."


translate italian day5_library_2_83502309:


    "Stavo cominciando a preoccuparmi, quando la porta si è spalancata all'improvviso, ed è entrata Alisa."


translate italian day5_library_2_c46172f4:


    "Anche lei sembrava sorpresa di vedermi qui."


translate italian day5_library_2_cdb9fe3e:


    dv "E {i}tu{/i} che ci fai qui?"


translate italian day5_library_2_3df6714e:


    me "Perché, non mi è permesso stare qui?"


translate italian day5_library_2_30a65a09:


    "Le ho risposto bruscamente."


translate italian day5_library_2_7ebd37d9:


    "Alisa era visibilmente confusa."


translate italian day5_library_2_f1713752:


    dv "Ah, chi se ne frega…"


translate italian day5_library_2_866ddbdc:


    "Ha sbuffato e si è diretta al tavolo di Zhenya."


translate italian day5_library_2_de1f890a:


    me "E tu perché sei qui?"


translate italian day5_library_2_6184e4df:


    "Alisa mi ha squadrato attentamente con i suoi occhi, ha aperto la bocca per dire qualcosa, ma poi ha cambiato idea e si è voltata, nascondendo le proprie mani dietro alla schiena."


translate italian day5_library_2_fe41f14c:


    me "Stai restituendo un libro?"


translate italian day5_library_2_91b7e642:


    "Ho detto la prima cosa che mi è passata per la testa."


translate italian day5_library_2_2d3c80f4:


    dv "Non sono affari tuoi…"


translate italian day5_library_2_eb262731:


    "Mi ha risposto, con un pizzico di esitazione."


translate italian day5_library_2_1c67eb95:


    me "Che libro è?"


translate italian day5_library_2_991d9a14:


    "Alisa è rimasta in silenzio."


translate italian day5_library_2_d8028619:


    me "Oh, andiamo, fammi vedere!{w} Mi chiedo cosa legga la signorina «Pericolo-alta-tensione»?"


translate italian day5_library_2_2d3c80f4_1:


    dv "Non sono affari tuoi…"


translate italian day5_library_2_f7c41790:


    "Ha ripetuto, in tono ancora meno sicuro."


translate italian day5_library_2_42f650e4:


    me "Ok, ok, non insisto o altro…"


translate italian day5_library_2_76998bd7:


    "A dire il vero però, ero molto interessato a scoprire cosa leggesse Alisa."


translate italian day5_library_2_66eefb60:


    "Inoltre, ero molto sorpreso di vedere un libro tra le sue mani."


translate italian day5_library_2_a36340e5:


    "La TV, un film, il computer, se fossero disponibili qui – tutte queste cose sembravano essere molto più appropriate per intrattenere una ragazza come lei."


translate italian day5_library_2_30470e07:


    "Ma invece lei aveva un libro…"


translate italian day5_library_2_a1a458f1:


    "La curiosità ha avuto il sopravvento. Ho colpito al momento giusto, quando Alisa stava guardando da un'altra parte, e le ho strappato il libro dalle mani."


translate italian day5_library_2_97a3e89b:


    dv "Ahi!"


translate italian day5_library_2_c9e0274f:


    "Ha urlato."


translate italian day5_library_2_a1b7c998:


    "Un instante più tardi la sua espressione si è tramutata in un modo tale che mi ha fatto molto dubitare della mia decisione."


translate italian day5_library_2_38429226:


    th "Se proprio devo morire, almeno so il perché."


translate italian day5_library_2_a0da6e68:


    "Tenevo tra le mani una copia di «Via col vento»."


translate italian day5_library_2_8d03e8c1:


    "Era lo stesso libro che leggeva Lena l'altra sera su una panchina."


translate italian day5_library_2_29dc2955:


    "Ero talmente stupito che mi sono temporaneamente dimenticato della mia morte imminente."


translate italian day5_library_2_6958082e:


    me "È interessante?"


translate italian day5_library_2_3b4b97c0:


    dv "Sì…"


translate italian day5_library_2_eb7dd1a8:


    "Ha risposto senza entusiasmo, arrossendo."


translate italian day5_library_2_621c7bd3:


    me "Va bene allora…"


translate italian day5_library_2_75b79b0a:


    "Le ho restituito il libro."


translate italian day5_library_2_df52def3:


    "Alisa l'ha gettato sul tavolo e ha lasciato rapidamente la biblioteca, senza guardarmi."


translate italian day5_library_2_c3a92102:


    th "Quindi le cose umane non le sono estranee.{w} Dopotutto, anche lei è una ragazza."


translate italian day5_library_2_fe404d31:


    "Dopo aver pensato a tutto quello che era appena accaduto, sono giunto alla conclusione che non c'era niente di strano."


translate italian day5_library_2_e499eebb:


    "La curiosità uccise il gatto."


translate italian day5_library_2_0dc343a6:


    th "In ogni caso, si tratta di Alisa – e questo significa che tutto potrebbe rapidamente trasformarsi in un disastro totale, e in più non ho ancora finito di cercare gli ingredienti."


translate italian day5_library_2_ab719773:


    dv "Beh, allora torno più tardi…"


translate italian day5_library_2_24b51b37:


    "Ha lasciato rapidamente la biblioteca senza guardarmi."


translate italian day5_library_2_89141735:


    "La cosa mi ha fatto pensare."


translate italian day5_library_2_72ba180f:


    th "Che tipo di libro è quello, se si è vergognata in tal modo?"


translate italian day5_library_2_4521510a:


    th "Alisa che prova vergogna è una cosa straordinaria di per sé…{w} Ma Alisa che si vergogna di un libro…"


translate italian day5_library_2_10c14b59:


    th "Ma che senso ha domandarselo adesso – ormai non potrò mai scoprirlo."


translate italian day5_library_2_4160881a:


    "Finalmente il profondo gemito di Zhenya è risuonato nella stanza, raggiungendo ogni angolo della biblioteca."


translate italian day5_library_2_bece771e:


    mz "Prendi!"


translate italian day5_library_2_98368a17:


    "Sono andato oltre gli scaffali e ho visto la bibliotecaria seduta accanto alla botola, tutta sudata, con accanto un piccolo sacchetto."


translate italian day5_library_2_7959d521:


    th "Beh, potrebbero avere una sorta di magazzino laggiù..."


translate italian day5_library_2_553525de:


    me "Grazie!"


translate italian day5_library_2_821c2037:


    "Ho preso il sacchetto e ho abbandonato la biblioteca."


translate italian day5_library_2_089e3478:


    "Grazie al cielo non era troppo pesante, così l'ho portato giù alla casetta di Olga Dmitrievna senza troppa fatica."


translate italian day5_main3_8645b244:


    "Finalmente avevo tutti gli ingredienti."


translate italian day5_main3_4f706171:


    "Ho preso il carretto con lo zucchero e ho caricato su di esso anche il sacchetto di farina, seguito dai due cesti di fragole, appoggiati al centro in qualche modo."


translate italian day5_main3_ee5f67e3:


    "E la birra l'ho tenuta nascosta sotto alla camicia, per sicurezza."


translate italian day5_main3_f702be22:


    "Si stava facendo sera, così ho dovuto fare in fretta, dato che la preparazione della torta avrebbe richiesto un po' di tempo."


translate italian day5_main3_1546fb97:


    "Certamente avrei preferito starmene sdraiato, chiudendo gli occhi e facendomi una bella dormita, ma non potevo deludere Olga Dmitrievna."


translate italian day5_main3_050e260b:


    "In effetti, dopo tutto lo sforzo che avevo fatto per ottenere quegli ingredienti, mi sentivo responsabile del successo di quell'impresa."


translate italian day5_main3_bfc08830:


    "Andando verso la piazza, mi sono fermato un attimo per riprendere fiato."


translate italian day5_main3_dcabd394:


    "Non è che il carretto fosse pesante – riuscivo a muovermi velocemente. Era solo che ogni singolo sforzo fisico mi provocava dolore.{w} Sia fisico che mentale."


translate italian day5_main3_fc8d210b:


    "Mi sono seduto su una panchina e ho chiuso gli occhi per un instante."


translate italian day5_main3_5dad0356:


    dreamgirl "E quello cos'è?"


translate italian day5_main3_4999a965:


    "Non me ne fregava niente di chi mi stesse parlando – probabilmente era solo una ragazza pioniere che si interessava di un compagno in difficoltà."


translate italian day5_main3_cb23542f:


    me "Di cosa stai parlando?"


translate italian day5_main3_b037ba1a:


    "Le ho chiesto stancamente."


translate italian day5_main3_d00d3d0f:


    "Non mi ha risposto."


translate italian day5_main3_b9035f5a:


    me "Quelli sono gli ingredienti per una torta...{w} Ti piacciono le torte?"


translate italian day5_main3_3d6bef45:


    dreamgirl "Non lo so…"


translate italian day5_main3_159dd052:


    me "Come, non hai mai assaggiato una torta?"


translate italian day5_main3_3d6bef45_1:


    dreamgirl "Non lo so…"


translate italian day5_main3_9ee429fd:


    "Ovviamente la ragazza sembrava non capire quello che le stavo dicendo, ma la cosa non mi ha sorpreso in quel momento."


translate italian day5_main3_7285209b:


    "Non ero proprio interessato alla conversazione. Ero così stanco che non avevo alcun interesse nel classificare le distrazioni esterne etichettandole come solite o insolite."


translate italian day5_main3_6978c07f:


    me "Capisco…"


translate italian day5_main3_0a00a780:


    me "Passa alla mensa più tardi e potrai assaggiarla."


translate italian day5_main3_f1668a0e:


    dreamgirl "Davvero?"


translate italian day5_main3_dae36352:


    me "Davvero."


translate italian day5_main3_5b24c110:


    dreamgirl "E di cosa sono fatte?"


translate italian day5_main3_2a985733:


    me "Che cosa?"


translate italian day5_main3_fad6f3d4:


    "Ho chiesto con indifferenza."


translate italian day5_main3_52a831f5:


    dreamgirl "Queste…{w} torte!"


translate italian day5_main3_46d149a2:


    me "Beh…{w} Un po' di farina, un po' di zucchero, varie creme…"


translate italian day5_main3_61fefa63:


    th "Che domanda strana, davvero non sa di cosa sono fatte le torte?"


translate italian day5_main3_0fa8d81f:


    dreamgirl "E ce li hai tutti qui?"


translate italian day5_main3_c8b42142:


    me "Sì, più o meno."


translate italian day5_main3_749fd519:


    dreamgirl "E lo zucchero?"


translate italian day5_main3_e2a3a4fa:


    me "Anche quello…"


translate italian day5_main3_b206627b:


    dreamgirl "Me ne potresti prestare un po'?"


translate italian day5_main3_7332342b:


    me "E per cosa?"


translate italian day5_main3_9d2d461d:


    "Ho pensato che fosse un po' sopra le righe."


translate italian day5_main3_15d24d3a:


    "Un'improvvisa folata di vento mi ha fatto afferrare istintivamente il carretto, e ho aperto gli occhi."


translate italian day5_main3_0cf7617c:


    "Tuttavia, non c'era nessuno."


translate italian day5_main3_195802d1:


    th "Sto sognando ad occhi aperti?"


translate italian day5_main3_0ee6c06e:


    "Però ho notato che il sacco era slegato, e un po' di zucchero era stato versato fuori."


translate italian day5_main3_2fb04556:


    th "Forse il vento l'ha spaventata ed è corsa via...?"


translate italian day5_main3_5e73ac52:


    "Dopo aver richiuso il sacco, mi sono alzato in piedi e ho proseguito per la mia impegnativa strada fragolosa."


translate italian day5_main3_d3d42b9c:


    "Non c'era anima viva nei pressi della mensa.{w} Non c'è da stupirsi – mancava ancora un'ora alla cena."


translate italian day5_main3_1b6a6dde:


    "Ho portato il carretto verso l'uscita secondaria, e ho consegnato gli ingredienti alla cuoca del Campo."


translate italian day5_main3_17448fb7:


    "Probabilmente sapeva già cosa farne, in quanto mi ha dato uno sguardo piuttosto sgarbato.{w} Non sapevo esattamente quanto tempo servisse per preparare una torta, ma di sicuro si sarebbe dovuta sbrigare."


translate italian day5_main3_088c8385:


    "Volevo solo rilassarmi per il resto del mio tempo prima di cena."


translate italian day5_main3_294475af:


    "Insomma, ero talmente esausto che mi sono limitato a sedermi sui gradini e ad aspettare lì."


translate italian day5_main3_89ce1a6f:


    "I miei occhi si chiudevano da soli – ero così stanco dopo quella giornata che non mi sono nemmeno accorto che qualcuno era venuto da me, finché non mi ha dato una pacca sulla spalla."


translate italian day5_main3_a0a72914:


    mi "Ciao!"


translate italian day5_main3_7629afc5:


    "Era Miku, in piedi davanti a me."


translate italian day5_main3_b5cc7e1c:


    me "Sì…"


translate italian day5_main3_6aa1252d:


    "Non mi serviva uno specchio per immaginare l'espressione di scetticismo e di irritazione sul mio volto."


translate italian day5_main3_11ad29bb:


    mi "Oh scusa, devo averti interrotto…"


translate italian day5_main3_df78914f:


    me "Nessun problema, mi sono appena seduto."


translate italian day5_main3_618d7741:


    mi "Ah, va bene allora!"


translate italian day5_main3_1af920ee:


    "Mi ha fatto un sorriso."


translate italian day5_main3_3f7156f6:


    mi "Stavo giusto venendo a cena. Ho pensato che fosse già ora, ma poi mi è sembrato che fosse troppo presto, ma ho deciso di controllare comunque, per sicurezza – forse non mi ero sbagliata io, ma l'orologio!"


translate italian day5_main3_bf5ddeb1:


    mi "Beh, non l'orologio, gli orologi non possono sbagliarsi, intendo dire, forse l'ho letto male io…"


translate italian day5_main3_3c421d62:


    "Era talmente confusa che ha smesso di parlare."


translate italian day5_main3_7fe5f005:


    me "Manca ancora circa mezz'ora alla cena."


translate italian day5_main3_8824776e:


    mi "Oh, che bello, allora mi siedo qui e aspetto con te, se non ti dispiace?"


translate italian day5_main3_203abf9f:


    "Francamente sì, mi dispiaceva."


translate italian day5_main3_05c9aec0:


    me "Sai, ho alcune cose da sbrigare…"


translate italian day5_main3_e44244b3:


    "Mi sono alzato rapidamente e mi sono allontanato da Miku senza salutarla, come ho sempre fatto, mentre lei urlava qualcosa alle mie spalle."


translate italian day5_main3_b094f8aa:


    "Un minuto più tardi sono arrivato alla piazza e mi sono seduto su una panchina, con la ferma intenzione di trovare un posto tranquillo e sicuro dove poter aspettare la cena."


translate italian day5_main3_da5773ae:


    "Credo che fosse la prima volta negli ultimi quattro giorni e mezzo che mi sentivo così."


translate italian day5_main3_de22be40:


    "Non ero solo irritato a causa di alcuni dettagli insignificanti, ma anzi, ero veramente arrabbiato."


translate italian day5_main3_8b26ac2a:


    "Perché avevo totalmente smesso di preoccuparmi di dove mi trovassi e perché fossi ancora qui."


translate italian day5_main3_9e4b97f1:


    "Non mi importava più nemmeno di cercare di andarmene."


translate italian day5_main3_07d4e30c:


    "La cosa che mi faceva impazzire è che ero sempre costretto a portare a termine un qualche stupido compito assegnatomi dalla nostra leader del Campo, ed ero sempre io a ficcarmi in situazioni stupide, certe volte facendo anche la figura del pagliaccio."


translate italian day5_main3_bd134020:


    th "Se tutto questo è uno scherzo alieno o l'idea di una Mente Universale, allora è meglio che prendano appuntamento con il loro psichiatra!"


translate italian day5_main3_13370091:


    "Ho stretto i denti e anche i pugni."


translate italian day5_main3_8ea911ef:


    th "E la cosa più fastidiosa è che tutto ciò che accade sembra accadere da sé in qualche modo!"


translate italian day5_main3_e2dfefc7:


    th "Sarei felice di non dover portare in giro sacchi di zucchero che pesano una tonnellata, ma non ho altra scelta!"


translate italian day5_main3_12a8d8d9:


    th "Voglio dire, qualsiasi altra opzione potrebbe portare a conseguenze molto peggiori di un affaticamento muscolare o dell'orgoglio ferito…"


translate italian day5_main3_60e71618:


    us "Con chi sei arrabbiato?"


translate italian day5_main3_4a8fcfb4:


    "Ulyana stava davanti a me, sorridendo maliziosamente."


translate italian day5_main3_90b46808:


    me "Nessuno, davvero…"


translate italian day5_main3_12a0290a:


    "Ho risposto distrattamente."


translate italian day5_main3_4758c0c0:


    "Ma i miei pugni mi hanno tradito."


translate italian day5_main3_be723785:


    me "Proprio così…"


translate italian day5_main3_40bd06ac:


    us "Ok, ok, sono fatti tuoi…{w} Ma faresti meglio a dirmi perché andavi in giro per il Campo con tutti quei sacchi?"


translate italian day5_main3_d8e5e6d0:


    me "Perché ho dovuto…"


translate italian day5_main3_d20f4795:


    "Ho risposto con riluttanza."


translate italian day5_main3_a0086905:


    us "Suppongo fosse del cibo."


translate italian day5_main3_726459ba:


    me "Forse lo era…"


translate italian day5_main3_28b2648c:


    "Ulyana stava per dire qualcosa, ma in quel momento ha suonato la sirena, chiamando i pionieri per la cena."


translate italian day5_main3_f1fc33c2:


    "Ho fatto un sospiro di sollievo e mi sono diretto rapidamente alla mensa, lasciando indietro Ulyana."


translate italian day5_main3_1d7b62f5:


    mt "Semyon, ti ringrazio davvero!"


translate italian day5_main3_9508d41b:


    me "Per cosa?"


translate italian day5_main3_92e433aa:


    "La leader del Campo mi ha fatto un sorriso amichevole."


translate italian day5_main3_f601ce01:


    mt "Per la torta, naturalmente!"


translate italian day5_main3_707cf821:


    me "Ah, certo…"


translate italian day5_main3_e512b012:


    "Era in quell'esatto momento che ho capito il significato del detto «Meglio un aiuto che cinquanta consigli»."


translate italian day5_main3_374f0622:


    mt "Non l'hai detto a nessuno, vero? Questa dev'essere una sorpresa!"


translate italian day5_main3_9513cd87:


    me "A nessuno…"


translate italian day5_main3_6a31846d:


    mt "E bravo il mio ragazzo!{w} E adesso, a cena!"


translate italian day5_main3_937d7bb8:


    "Olga Dmitrievna ha agitato la mano, indicando la mensa."


translate italian day5_main3_5452915f:


    "Ho attraversato la soglia e ho iniziato a cercare un posto libero."


translate italian day5_main3_f6bdde48:


    "Oggi ce n'erano parecchi, così ho avuto la possibilità di mangiare da solo."


translate italian day5_main3_2061d557:


    "Per cena c'era del pesce con purè di patate."


translate italian day5_main3_062530c8:


    th "Che sfortuna – resterò di nuovo mezzo affamato.{w} Ma non c'era il pesce pure a pranzo…?"


translate italian day5_main3_0e40ce73:


    th "Questo è forse il giorno dedicato al pesce?!"


translate italian day5_main3_ce79b819:


    "Dopo aver allontanato il mio piatto di abitanti del mare, ho abbassato la testa fra le mani e ho chiuso gli occhi."


translate italian day5_main3_d13c0f93:


    "Ma poco dopo qualcuno si è avvicinato al tavolo."


translate italian day5_main3_44b645c0:


    sl "Ehi, tutto a posto?"


translate italian day5_main3_84662d7a:


    me "Sto bene…"


translate italian day5_main3_35836741:


    "Ho risposto a Slavya, senza cambiare la mia posizione."


translate italian day5_main3_cb89aafa:


    sl "Sei stanco?"


translate italian day5_main3_bf1c2987:


    me "Sì, un po'…"


translate italian day5_main3_17c764f2:


    sl "Questo non va bene."


translate italian day5_main3_81e2449f:


    "Ha detto con serietà."


translate italian day5_main3_17c6b21c:


    me "Naturalmente…"


translate italian day5_main3_4fb291b0:


    sl "Ricordi che dopo cena andremo all'escursione, vero? Hai preparato tutto?"


translate italian day5_main3_25632213:


    me "Che cosa? Dove?"


translate italian day5_main3_9830e8f7:


    "Ho aperto gli occhi sollevando la testa all'istante."


translate italian day5_main3_bf312592:


    "Lena stava in piedi accanto a Slavya."


translate italian day5_main3_cd82f264:


    sl "L'escursione…"


translate italian day5_main3_a4614188:


    "Era sorpresa."


translate italian day5_main3_1acd2585:


    sl "Non lo sapevi?"


translate italian day5_main3_5cb822ee:


    me "No…"


translate italian day5_main3_63edcced:


    "Ho appoggiato la testa sul tavolo e l'ho coperta con le mani."


translate italian day5_main3_72338241:


    th "Se solo potessi sprofondare nel terreno all'istante..."


translate italian day5_main3_34d8cd0a:


    "Le ragazze sono rimaste in silenzio."


translate italian day5_main3_cfa6c6ec:


    "Mi hanno lasciato da solo con i miei pensieri per un po', e la cosa mi andava bene."


translate italian day5_main3_cdac6a32:


    "Forse sarei potuto stare seduto in quel modo fino alla fine della cena, ma la forte voce di Olga Dmitrievna è risuonata dalla parte opposta della mensa."


translate italian day5_main3_0b4ca722:


    mt "Ragazzi!{w} Per celebrare il miracoloso salvataggio del nostro compagno e amico Shurik, abbiamo preparato questa torta per tutti voi!"


translate italian day5_main3_639a506c:


    "Ho alzato pigramente la testa e ho guardato verso la leader del Campo, ma non riuscivo a vedere nulla al di là delle schiene dei pionieri."


translate italian day5_main3_c6fcef15:


    mt "Un secondo… Solo un secondo…"


translate italian day5_main3_a783f6df:


    th "Non mi hanno nemmeno menzionato.{w} Nessuna parola riguardo a me che salvo Shurik, o che raccolgo gli ingredienti per la torta…"


translate italian day5_main3_6400b884:


    th "Come se dovesse essere così."


translate italian day5_main3_df46be92:


    th "Beh, non mi sarei potuto aspettare altro dalla nostra leader del Campo."


translate italian day5_main3_6ab86cd0:


    sl "Andiamo!{w} O non avremo la nostra parte!"


translate italian day5_main3_c17a722d:


    "Slavya ha sorriso."


translate italian day5_main3_a8b5b91a:


    un "Andiamo."


translate italian day5_main3_c1956a30:


    "Ha concordato Lena."


translate italian day5_main3_3b03af3a:


    me "Sì, certo…"


translate italian day5_main3_a2f9d7eb:


    "Mi sono alzato a malincuore e mi sono trascinato dietro alle ragazze."


translate italian day5_main3_f95eb2e4:


    "Mentre ci avvicinavamo alla folla di pionieri, Olga Dmitrievna stava mettendo la torta al centro del tavolo."


translate italian day5_main3_9e9a688f:


    mt "E adesso…"


translate italian day5_main3_902ab03d:


    "La leader del Campo non è neppure riuscita a finire la frase, che Ulyana si è gettata dalla folla di pionieri, tuffandosi verso la torta."


translate italian day5_main3_44ef2910:


    "È riuscita a morderla un paio di volte prima che fosse trascinata via."


translate italian day5_main3_3ec376bc:


    "Stava urlando e scalciando."


translate italian day5_main3_72ace9f9:


    "Ho osservato con sguardo assente lo svolgersi di tutto quel dramma: Alisa sorrideva, Lena stava raccogliendo un po' di crema con il dito, tutti i pionieri furiosi stavano attorno alla torta."


translate italian day5_main3_7f98185e:


    "Mi sentivo completamente fuori posto qui. Ho pensato che se avessi chiuso gli occhi e poi li avessi riaperti – eccomi qua, di nuovo al sicuro nel mio appartamento, davanti al computer…"


translate italian day5_main3_f569f26d:


    "Ho sbattuto le palpebre, ma tutto è rimasto lo stesso. L'unica cosa che è cambiata è che il rumore, la confusione e le grida sono diventate più nitide."


translate italian day5_main3_87959490:


    mt "Ulyana! Questo è troppo!"


translate italian day5_main3_34125318:


    us "Ma io…{w} Ho solo…"


translate italian day5_main3_6429cb93:


    "Beh, in effetti comportarsi in quel modo era un po' sopra le righe persino per lei."


translate italian day5_main3_d677809e:


    "Shurik ha fatto irruzione nella conversazione – o si trattava più di una corte marziale?"


translate italian day5_main3_58d5ebe0:


    sh "Per favore, Olga Dmitrievna! Dato che la torta è per celebrare il mio ritorno, non è un grosso problema…"


translate italian day5_main3_a6820f71:


    "Ha detto, esitando."


translate italian day5_main3_57d1ba21:


    mt "Non importa!"


translate italian day5_main3_aac2b1f6:


    "La leader del Campo si è rivolta nuovamente ad Ulyana."


translate italian day5_main3_33bbaf19:


    mt "E tu…{w} Oggi ho intenzione di punirti come mai prima d'ora, così la prossima volta imparerai a comportarti come si deve!"


translate italian day5_main3_935bc568:


    us "Ah, per l'amor di Dio!"


translate italian day5_main3_99aa653e:


    "Ha sbuffato e si è voltata."


translate italian day5_main3_cd01cddf:


    mt "Oggi non verrai con noi all'escursione!"


translate italian day5_main3_2babf74f:


    us "Come se ci volessi andare!"


translate italian day5_main3_54e5777a:


    "Io ero più che disposto a scambiarmi con Ulyana, saltando l'escursione al posto suo, ma chi l'avrebbe immaginato…"


translate italian day5_main3_038c58cb:


    "Se l'avessi saputo in anticipo, sarei stato io a dare di matto distruggendo quella maledetta torta!"


translate italian day5_main3_a4bcbbe9:


    "Dopo un paio di minuti di confusione, i pionieri hanno iniziato a disperdersi."


translate italian day5_main3_c997a5fb:


    mt "Devi prepararti anche tu!{w} Ci sarà l'allineamento tra mezz'ora, alla piazza."


translate italian day5_main3_80aff84c:


    "Ho guardato la leader del Campo dritto negli occhi, cercando di esprimere la mia opinione in modo non verbale, ma sembrava che io avessi fallito."


translate italian day5_main3_a89a2c76:


    mt "Non tardare!"


translate italian day5_main3_fd82295b:


    "Ulyana stava seduta sul tavolo, quando mi sono avvicinato a lei mentre mi dirigevo verso l'uscita."


translate italian day5_main3_94be5d8c:


    me "Allora, perché l'hai fatto?"


translate italian day5_main3_4d3873eb:


    "Sembrava piuttosto offesa.{w} Ma aveva tutto il diritto di sentirsi così."


translate italian day5_main3_500104f1:


    us "Perché mi andava."


translate italian day5_main3_e8371c45:


    "Mi ha risposto bruscamente."


translate italian day5_main3_017bb18f:


    me "E quindi, sei contenta adesso?"


translate italian day5_main3_2ddb5b95:


    us "Ma certo che lo sono!{w} E tu – buona fortuna per l'escursione!"


translate italian day5_main3_72d5c9fe:


    "Ha sorriso maliziosamente, è balzata in piedi e si è precipitata fuori dalla mensa."


translate italian day5_main3_b53a9fd1:


    th "Beh, un po' di fortuna non mi guasterebbe…"


translate italian day5_main3_a9aa1a31:


    "«Cammina e arriverai a destinazione» – era la frase perfetta che girava vorticosamente nella mia testa per tutto il tragitto verso la casetta della leader del Campo."


translate italian day5_main3_831c71df:


    "Per qualche ragione non avevo voglia di discutere a riguardo, o di fingere di essere malato, o semplicemente di evitare quell'escursione senza motivo."


translate italian day5_main3_f85ee780:


    "Gli eventi di questa giornata mi hanno insegnato il significato della sottomissione.{w} Anche se a volte quello che accadeva non aveva alcun senso per me."


translate italian day5_main3_4da475d5:


    "Appena entrato nella stanza, ho pensato a una cosa."


translate italian day5_main3_96c41623:


    th "Beh, in effetti, come dovrei prepararmi?"


translate italian day5_main3_9679533e:


    th "Quali vestiti mettere? Ho solo un cappotto e un paio di jeans.{w} E comunque, ho dimenticato di chiedere se si tratta di un'escursione notturna oppure no."


translate italian day5_main3_fd2a6187:


    "Non riuscivo a pensare a niente di meglio, così ho messo addosso il maglione con il quale ero arrivato in questo Campo il primo giorno (la notte avrebbe potuto essere abbastanza fredda), e mi sono trascinato lentamente verso la piazza."


translate italian day5_main3_ca17b192:


    "L'intero Campo era già raggruppato lì, anche se mancava ancora una decina di minuti all'orario stabilito da Olga Dmitrievna."


translate italian day5_main3_1e295624:


    "Mi sono sistemato in disparte e ho aspettato pazientemente."


translate italian day5_main3_fd3eb8d2:


    "Stava calando la notte."


translate italian day5_main3_d20ed5d2:


    th "Visti da fuori, chissà che immagine comica saremmo: una folla di pionieri, tutti in fila come d'abitudine, in attesa di ordini da parte del taciturno e bronzeo Genda."


translate italian day5_main3_018d282a:


    th "E tutto questo siparietto è decorato con i raggi scarlatti del tramonto."


translate italian day5_main3_b7cd9dd7:


    "Ed eccolo lì, ad agitare la mano urlando «attaccate», mentre ruggenti soldati con rossi foulard si mettevano in marcia nella battaglia contro un nemico spettrale…"


translate italian day5_main3_18d7dd2e:


    "Ma Olga Dmitrievna è apparsa e ha iniziato a parlare al posto di Genda."


translate italian day5_main3_553eaa56:


    mt "Pare che ci siano tutti…{w} Eccellente!"


translate italian day5_main3_a8c428b5:


    "Ero così stanco oggi che non riuscivo proprio a pensare a nulla, così mi sono limitato ad ascoltare passivamente i suoni emessi dalla leader del Campo."


translate italian day5_main3_443f99aa:


    mt "Allora, oggi andremo a fare un'escursione!"


translate italian day5_main3_747aae61:


    mt "È essenziale che ogni pioniere sia in grado di soccorrere un suo compagno, per dargli una mano nel momento del bisogno, salvandolo da una situazione difficile!"


translate italian day5_main3_fc6094ea:


    mt "E noi dobbiamo imparare a fare tutte queste cose insieme!"


translate italian day5_main3_fc0d9c17:


    "Un sussurro che si è sparso tra la folla di pionieri ha suggerito che molto probabilmente questa cosiddetta «epica escursione» avrebbe avuto come destinazione una semplice radura nella foresta, a qualche centinaio di metri dalla piazza."


translate italian day5_main3_038d0ddf:


    "Era quello che pensavo anch'io."


translate italian day5_main3_81d86d37:


    mt "Cammineremo a coppie.{w} Quindi, se non avete ancora scelto un compagno, fatelo adesso!"


translate italian day5_main3_30d15b67:


    "I pionieri hanno afferrato rapidamente il concetto e hanno iniziato a disporsi a coppie."


translate italian day5_main3_ae8616af:


    "Sembrava che io fossi l'unico senza un compagno."


translate italian day5_main3_a9a03604:


    "Slavya stava discutendo con entusiasmo con Olga Dmitrievna, Lena stava con Miku, Electronik era, ovviamente, con Shurik."


translate italian day5_main3_aea0ad16:


    th "Beh, dopotutto non sarebbe una cattiva idea andarci da solo."


translate italian day5_main3_105fc865:


    mt "Semyon!"


translate italian day5_main3_4e47d15f:


    "La voce della leader del Campo mi ha risvegliato dai miei pensieri."


translate italian day5_main3_92f6d31e:


    "L'ho raggiunta, riluttante."


translate italian day5_main3_8dfde425:


    mt "Vedo che non hai trovato un compagno."


translate italian day5_main3_d3242212:


    me "Sembra di sì…"


translate italian day5_main3_d1a7c5c7:


    mt "Allora starai con Zhenya – anche lei è da sola."


translate italian day5_main3_27ad27f5:


    "Sono stato afflitto da quel genere di disperazione che solo un vero solitario può sperimentare."


translate italian day5_main3_b7994624:


    "E così sembrava che io dovessi stare con la scorbutica bibliotecaria, con la quale non avrei trascorso un paio d'ore nemmeno se mi avessero pagato."


translate italian day5_main3_b73326a6:


    th "Anche se entrambi sembriamo essere nella stessa barca…"


translate italian day5_main3_d17a396e:


    "Mi sono avvicinato lentamente a Zhenya."


translate italian day5_main3_6818c128:


    me "Beh, suppongo che dovremo andare insieme…"


translate italian day5_main3_c25dd9ca:


    "Ha alzato lo sguardo su di me."


translate italian day5_main3_0af22cb1:


    mz "Non pensare che ne sia contenta!"


translate italian day5_main3_44ba86f4:


    "Mi ha detto con serietà."


translate italian day5_main3_5122493d:


    me "E perché diavolo dovresti essere contenta?"


translate italian day5_main3_d7b5efd3:


    "Ho chiesto ingenuamente."


translate italian day5_main3_d0a8dd78:


    mz "Non importa!{w} Sarebbe molto meglio se stessi zitto."


translate italian day5_main3_f4dcd652:


    th "Eh, non potrebbe andare meglio di così…"


translate italian day5_main3_b28b669b:


    "Mi ha dato le spalle e ha iniziato a camminare dietro agli altri pionieri."


translate italian day5_main3_e32f998e:


    "Non ho ancora trovato alcun valido motivo per dover camminare in coppie."


translate italian day5_main3_34fb045a:


    "E comunque, stavamo camminando sul sentiero principale della foresta, e sarebbe stato piuttosto difficile perdersi qui, anche se lo si volesse."


translate italian day5_main3_d1f6b652:


    "Inoltre, dopo aver camminato per oltre mezz'ora, non eravamo ancora arrivati nelle profondità della foresta, dove avremmo potuto affrontare tutti i pericoli che potevano testare il nostro coraggio e indurire il nostro spirito di pioniere, ma stavamo semplicemente girando in tondo."


translate italian day5_main3_946c8923:


    "Tuttavia, se si considera che Olga Dmitrievna era la nostra leader, questa escursione potrebbe essere paragonata alla marcia degli hobbit dalla Contea fino a Mordor…"


translate italian day5_main3_97574c1e:


    "Proprio come voleva Zhenya, io la stavo seguendo mantenendo le distanze, in silenzio."


translate italian day5_main3_41f63256:


    "La bibliotecaria sembrava essere perfettamente a suo agio in quella situazione."


translate italian day5_main3_6afab6eb:


    me "Ehi, non sai per caso quando arriveremo a destinazione?"


translate italian day5_main3_67e23fdb:


    mz "Arriveremo dove?"


translate italian day5_main3_88370e84:


    me "Ehm, il luogo dove ci stabiliremo per il campeggio."


translate italian day5_main3_059aa439:


    mz "Lo scopo di questa escursione non è quello di andare in campeggio, ma l'escursione in sé!{w} Proprio non capisci!"


translate italian day5_main3_97b64a58:


    "Già, probabilmente non ho capito niente delle escursioni…"


translate italian day5_main3_4587e3c3:


    me "Credo che tu abbia ragione, ma comunque…"


translate italian day5_main3_bb86bd36:


    mz "Non lo so!"


translate italian day5_main3_75f80dfe:


    "Ha risposto bruscamente, affrettando il passo."


translate italian day5_main3_133cde0a:


    "L'ho raggiunta e le ho chiesto:"


translate italian day5_main3_e4b066d7:


    me "Senti, perché devi essere sempre così…"


translate italian day5_main3_17313088:


    "Stavo per dire «stronza», ma mi sono trattenuto."


translate italian day5_main3_6687b26f:


    me "Non ti ho fatto niente di male, e non ho intenzione di farlo!"


translate italian day5_main3_efd76d15:


    "Mi ha guardato piuttosto sorpresa."


translate italian day5_main3_296b44b5:


    mz "Sempre così... Cosa?"


translate italian day5_main3_231d5bf5:


    me "Beh, poco socievole… O qualcosa del genere…{w} O forse lo fai solo con me?"


translate italian day5_main3_da119946:


    mz "Oh, piantala con queste stupidate!"


translate italian day5_main3_8b13fe9b:


    me "Come vuoi…"


translate italian day5_main3_39a4936f:


    "Ho deciso di non cercare di iniziare una conversazione con lei per il resto dell'escursione."


translate italian day5_main3_a20cefa7:


    "..."


translate italian day5_main3_66653ce7:


    "Finalmente Olga Dmitrievna ha deciso di porre fine a quel calvario."


translate italian day5_main3_f2e5e7d4:


    mt "È ora di fermarsi."


translate italian day5_main3_e9d2363e:


    "Il luogo scelto si è rivelato essere una radura abbastanza grande, con alcuni tronchi d'albero disposti a semicerchio per formare un pergolato improvvisato, con al centro i resti di un falò."


translate italian day5_main3_d3f88135:


    "Doveva essere una tradizione di questo Campo."


translate italian day5_main3_e664fb77:


    "Sono stato mandato a raccogliere la legna con gli altri ragazzi."


translate italian day5_main3_5463f5b6:


    "Non ci abbiamo messo molto, dato che c'erano un sacco di rami e tronchi di varie dimensioni sparsi tutt'attorno."


translate italian day5_main3_dcb20eaf:


    "Alla fine, con l'aiuto di alcuni vecchi fogli di giornale, Olga Dmitrievna è riuscita ad accendere il fuoco."


translate italian day5_main3_df660aed:


    "Ero curioso di sapere cosa ci fosse scritto su quei giornali, ma non riuscivo a scorgere altro che simboli sovietici."


translate italian day5_main3_44bc05c0:


    "I pionieri hanno preso posto sui tronchi, e hanno iniziato a parlare delle loro cose."


translate italian day5_main3_0b4fac65:


    "Sembrava che l'obiettivo finale di quell'evento fosse stato raggiunto."


translate italian day5_main3_c751793a:


    th "Le uniche cose che mancano sono un piatto di zuppa di pesce, tazze in alluminio per la vodka e una chitarra."


translate italian day5_main3_a7859a66:


    "Ma non mi sarei affatto sorpreso se tali cose fossero apparse tutto d'un tratto."


translate italian day5_main3_29dcd13b:


    sl "A cosa stai pensando?"


translate italian day5_main3_a6893681:


    "Slavya si è seduta accanto a me."


translate italian day5_main3_60a79f40:


    me "Oh, niente di speciale…{w} Mi sto semplicemente godendo l'escursione."


translate italian day5_main3_27c62366:


    "Ho risposto con sarcasmo."


translate italian day5_main3_5e83eb9a:


    sl "Non sembri troppo felice."


translate italian day5_main3_f8cc5c73:


    me "Beh, non ho intenzione di saltare di gioia, scusami tanto."


translate italian day5_main3_8f1735d3:


    sl "Va bene, non voglio disturbarti."


translate italian day5_main3_70009934:


    "È rimasta seduta con me per un po', ma dopo aver capito che non ero in vena di parlare mi ha lasciato da solo con la mia introspezione."


translate italian day5_main3_81fad12b:


    "E tutto quello che desideravo era sdraiarmi sul letto e addormentarmi il prima possibile, ma invece venivo circondato dal fumo e dalle chiacchiere inutili dei pionieri intorno a me."


translate italian day5_main3_9aaac94d:


    "Stavano ridendo e si stavano divertendo in questa calda serata estiva."


translate italian day5_main3_2803f4ab:


    "Nella parte opposta della radura ho notato che Lena e Alisa stavano discutendo intensamente."


translate italian day5_main3_d93c3f94:


    "«Intensamente» e «Lena» mi sembravano due cose totalmente opposte."


translate italian day5_main3_193e4c57:


    "Slavya sembrava essere sparita da qualche parte, dopo la nostra conversazione."


translate italian day5_main3_37441d60:


    "Electronik e Shurik discutevano animatamente di qualcosa con Olga Dmitrievna."


translate italian day5_main3_3257d53a:


    "Probabilmente io ero l'unico intruso tra loro."


translate italian day5_main3_a4f8dc02:


    th "Ma d'altra parte, perché dovrebbe importarmene?"


translate italian day5_main3_cb94daa6:


    "Mi stavo semplicemente limitando a fissare il fuoco."


translate italian day5_main3_c8d1a036:


    th "C'è un detto che afferma che lo si potrebbe guardare ardere per sempre, così come l'acqua che scorre."


translate italian day5_main3_68d47823:


    "Ma c'era anche una terza cosa…"


translate italian day5_main3_1d9e5974:


    th "Che cos'era?"


translate italian day5_main3_29414842:


    mt "Tre cose si possono guardare per sempre: il fuoco che brucia, l'acqua che scorre, e le altre persone che lavorano!"


translate italian day5_main3_903e62de:


    "La leader del Campo mi ha estratto dal mio sognare ad occhi aperti."


translate italian day5_main3_31151d59:


    mt "Semyon, non credi che sia un po' troppo presto per rilassarsi?"


translate italian day5_main3_0a50a8a2:


    me "E che altro dovrei fare?"


translate italian day5_main3_435b54b7:


    "Non riuscivo davvero a capire cosa volesse da me Olga Dmitrievna."


translate italian day5_main3_2c3aefa4:


    mt "Non saprei…"


translate italian day5_main3_2ebab91c:


    "Ci ha pensato un attimo."


translate italian day5_main3_3e0f4283:


    mt "Ma se ci sarà qualcosa da fare, falla senza esitazione."


translate italian day5_main3_fa103e1d:


    "Ha sorriso in modo ambiguo, ed è tornata al fuoco per alimentarlo con altri ramoscelli."


translate italian day5_main3_8045c9cb:


    "Dopo quelle sue parole potevo dire con certezza che lei mi considerasse il suo schiavo personale."


translate italian day5_main3_5d460c3e:


    "O almeno come una forza lavoro non retribuita, che è, a rigor di termini, la stessa cosa."


translate italian day5_main3_5618085a:


    "Ho sospirato, appoggiando la testa sulle mani, sperando che i miei tormenti sarebbero finiti per oggi..."


translate italian day5_main3_a582c672:


    "Qualcuno mi ha dato una pacca sulla spalla."


translate italian day5_main3_008bd100:


    "Ho alzato lo sguardo e ho visto Shurik ed Electronik, che si sono seduti accanto a me."


translate italian day5_main3_132aeab6:


    me "Che volete?"


translate italian day5_main3_b037ba1a_1:


    "Ho chiesto stancamente."


translate italian day5_main3_a3f9f456:


    el "Non essere triste!"


translate italian day5_main3_8afd5934:


    me "C'è forse qualcosa di meglio da fare?"


translate italian day5_main3_a2ade9c9:


    sh "Senti, abbiamo discusso con Olga Dmitrievna riguardo alle prospettive di sviluppo del circolo di cibernetica…"


translate italian day5_main3_13f2497e:


    sh "Ma c'è un problema – non abbiamo abbastanza membri.{w} Forse se tu potessi…"


translate italian day5_main3_a6820f71_1:


    "Ha esitato."


translate italian day5_main3_ac13c40b:


    th "«Sviluppo» e quei due sembrano essere delle entità piuttosto incompatibili tra loro."


translate italian day5_main3_b77b4d76:


    "Non ho risposto, e invece ho iniziato a guardare gli altri pionieri attorno a me."


translate italian day5_main3_eb10acd3:


    sh "Ebbene?"


translate italian day5_main3_b9a3cb6a:


    me "Non ho tempo…{w} Non vedete che sono sempre occupato con le commissioni della leader del Campo?"


translate italian day5_main3_74081b6b:


    sh "Sì, forse hai ragione…{w} È un po' imbarazzante, quello che è accaduto oggi con Ulyana."


translate italian day5_main3_a3bf9ccf:


    "L'ho guardato, sorpreso."


translate italian day5_main3_3e7b5ad7:


    th "Sembra che Shurik si senta in colpa per quell'incidente con la torta."


translate italian day5_main3_8043b470:


    me "Si, lo è…"


translate italian day5_main3_5bb39a9d:


    "Tutti i pionieri sembravano essere qui, ma non riuscivo a vedere Slavya."


translate italian day5_main3_35484a41:


    sh "Credo che lei sia arrabbiata con me…"


translate italian day5_main3_ad02f53a:


    me "Chi?"


translate italian day5_main3_fad6f3d4_1:


    "Ho chiesto distrattamente."


translate italian day5_main3_948b648b:


    sh "Ulyana.{w} Forse dovrei scusarmi con lei?"


translate italian day5_main3_7fe19203:


    me "No, non è stata colpa tua…"


translate italian day5_main3_53bddd68:


    "Siamo rimasti seduti in silenzio per un po', poi mi sono alzato e ho detto:"


translate italian day5_main3_37f21c35:


    me "Ho le gambe intorpidite, è meglio se vado a fare una passeggiata."


translate italian day5_main3_d7826409:


    "Sono rimasti in silenzio."


translate italian day5_main3_d9cf7a94:


    "Ho fatto un paio di giri intorno al nostro accampamento improvvisato, notando le occhiate vigili che mi lanciava la nostra leader."


translate italian day5_main3_3b6642f3:


    "Era come se Olga Dmitrievna non vedesse l'ora di trovare qualche nuovo compito da assegnarmi."


translate italian day5_main3_0724ca3e:


    "Non ho trovato Slavya da nessuna parte."


translate italian day5_main3_a3f3fccb:


    th "Forse dovrei andare a cercarla?"


translate italian day5_main3_84b23eff:


    "D'altra parte, mi dispiaceva per Ulyana ogni volta che pensavo a quella sua espressione sconvolta."


translate italian day5_main3_d76fc57e:


    th "Forse questa escursione non è la cosa più divertente da fare qui, ma stare seduta lì tutta sola lo è ancora meno."


translate italian day5_main3_016c7fc0:


    "Ma allo stesso tempo non avevo voglia di andare da nessuna parte…"


translate italian day5_main3_3e703e80:


    "Ero assolutamente certo che Slavya stesse bene, così ho deciso di rimanere."


translate italian day5_main3_d37bbb9f:


    th "Ma d'altra parte perché dovrei preoccuparmi di quella strega!"


translate italian day5_main3_f6cc82fd:


    th "Direi che per oggi ne ho avute abbastanza."


translate italian day5_main3_c58cdcde:


    "Mi sono seduto al mio posto precedente e ho atteso con pazienza la fine dell'escursione, riuscendo a percepire quasi fisicamente gli sguardi che mi lanciava la leader del Campo."


translate italian day5_main3_5b5fdf66:


    "Alla fine si è alzata e ha dichiarato:"


translate italian day5_main3_85ade4d1:


    mt "E ora giochiamo alle Città!"


translate italian day5_main3_79d3e6a7:


    "Non avevo nulla contro il gioco in sé."


translate italian day5_main3_9432dd18:


    th "Ma è ovvio che l'escursione durerà più a lungo a causa di esso…"


translate italian day5_main3_6f4a9363:


    "I pionieri si sono seduti attorno al fuoco."


translate italian day5_main3_1dff3b67:


    "Ho notato Lena e Alisa, che hanno preso posto su un tronco di fronte a me."


translate italian day5_main3_f58cf444:


    th "Sembra che si siano calmate."


translate italian day5_main3_7db4f674:


    "Fino a pochi minuti fa avrei pensato l'opposto, mentre stavo osservando il loro litigio."


translate italian day5_main3_9f3fc574:


    th "Ma tutto è possibile…"


translate italian day5_main3_74340d5d:


    "Mi sarebbe piaciuto davvero sapere di cosa stessero parlando, ma ormai era troppo tardi per farlo, e riuscivo a sentire la stanchezza crescere dentro di me sempre più."


translate italian day5_main3_6615be77:


    "La mia testa era completamente vuota."


translate italian day5_main3_99ab0d84:


    "Per la precisione, la mia testa era così pesante che non c'era posto in essa per alcun tipo di idea."


translate italian day5_main3_e447cec9:


    "Mentre nei momenti migliori il mio cervello sembrava essere un'enorme autostrada con milioni di pensieri che scorrevano velocemente, inseguendosi a vicenda e causando grossi schianti, ora era più come un sentiero sperduto nel bosco, che veniva percorso raramente e solo in casi eccezionali."


translate italian day5_main3_3b240839:


    "Slavya non è tornata."


translate italian day5_main3_b00a6ffe:


    th "Forse aveva qualcosa da fare?"


translate italian day5_main3_5147a728:


    "Ma, come già detto, non c'era più modo di scoprirlo."


translate italian day5_main3_3f2cf4ef:


    mt "Va bene, cominciamo!{w} Mosca!"


translate italian day5_main3_dad4cc7b:


    "I pionieri hanno iniziato a nominare le città."


translate italian day5_main3_3f3598ba:


    "Alla fine è arrivato il mio turno."


translate italian day5_main3_58df1a5a:


    "Ho cercato di ascoltare attentamente per capire la prima lettera che dovevo utilizzare per dire un nome di città."


translate italian day5_main3_d1d876fd:


    me "Arkhangelsk."


translate italian day5_main3_f374397d:


    "Abbiamo giocato diversi giri."


translate italian day5_main3_0759a0f8:


    "Ogni nuovo nome di città rendeva via via più difficile il ricordarsi tutte quelle che erano già state nominate in precedenza."


translate italian day5_main3_e27c9e7a:


    "La mia attenzione si stava affievolendo, e mi stavo perdendo in tutte quelle capitali, megalopoli, villaggi e insediamenti urbani."


translate italian day5_main3_b45788c7:


    mt "Semyon! Semyon! Tocca a te!"


translate italian day5_main3_b4c8d23c:


    "Olga Dmitrievna mi ha riportato alla realtà."


translate italian day5_main3_60202c5c:


    me "Ah, scusi... Qual era l'ultima?"


translate italian day5_main3_cb702f2c:


    mt "Hai di nuovo la testa tra le nuvole!{w} Era Kabul."


translate italian day5_main3_25856564:


    me "Ok, allora dico Londra…"


translate italian day5_main3_9affe25e:


    mt "Già detta."


translate italian day5_main3_bad80ec1:


    me "Beh, allora…"


translate italian day5_main3_63447575:


    "Ho pensato un po'.{w} C'è una marea di città nel mondo che iniziano per «L», ma non riuscivo a ricordarmene nemmeno una."


translate italian day5_main3_b6595b35:


    me "Liverpool?"


translate italian day5_main3_6bbc1d1e:


    mt "Già detta!"


translate italian day5_main3_b9a2a957:


    me "Los Angeles?"


translate italian day5_main3_4c5275f0:


    mt "Oh, finalmente!"


translate italian day5_main3_e7e84af8:


    "Mi ha dato un'occhiata sprezzante, ma il gioco è proseguito."


translate italian day5_main3_fca355af:


    "Non sopportavo proprio di dover pensare a un'altra città che iniziasse con «L», ma per fortuna era l'ultimo giro."


translate italian day5_main3_4e97d333:


    mt "Va bene, per oggi basta così! È già tardi, è ora di tornare indietro!"


translate italian day5_main3_99742b57:


    "Ho fatto un sospiro di sollievo."


translate italian day5_main3_5e1a747b:


    "Sulla via del ritorno abbiamo camminato come volevamo, senza dover stare in coppia."


translate italian day5_main3_d3fc10d2:


    "La notte è calata sul Campo.{w} Una notte perfettamente normale e regolare."


translate italian day5_main3_46ada0f2:


    "Era una di quelle notti in cui il cielo oscuro, le stelle e una luna crescente non causavano alcun sentimento in particolare, e il frinire dei grilli e il canto degli uccelli notturni sembravano più dei rumori di lavoro di routine, invece che un coro notturno."


translate italian day5_main3_a20cefa7_1:


    "..."


translate italian day5_main3_604b0d7d:


    "Dopo pochi minuti tutti i pionieri erano già in fila alla piazza."


translate italian day5_main3_5e7a97d2:


    "Era piuttosto tardi e la stanchezza ha preso il sopravvento, così il nostro allineamento non era disposto perfettamente."


translate italian day5_main3_26bc47b6:


    "Sembrava più una fila di vichinghi radunati dopo una battaglia di successo, con i guerrieri felici e sorridenti che anticipavano il ritorno dalle loro famiglie, piuttosto che pensare a mantenere la formazione corretta."


translate italian day5_main3_86ecb47a:


    "Ma li si sarebbe potuti anche considerare come una truppa totalmente sconfitta, o un gruppo di sopravvissuti che dovevano marciare verso la loro patria con le loro ultime forze."


translate italian day5_main3_75fc1be9:


    mt "Grazie a tutti!{w} E ora andare a dormire, è già tardi!"


translate italian day5_main3_539072ed:


    "I pionieri di sono dispersi rapidamente, lasciandomi da solo con la leader del Campo."


translate italian day5_main3_ca1d4f17:


    mt "Dovremmo andare anche noi!"


translate italian day5_main3_006c39f4:


    "Siamo andati alla casetta di Olga Dmitrievna in totale silenzio."


translate italian day5_main3_79ed0be1:


    mt "Ok, dormiamo!"


translate italian day5_main3_899c3f81:


    "Ha detto, spegnendo la luce."


translate italian day5_main3_ae190c31:


    "Mi giravo e rigiravo a lungo nel letto, ricordando tutti gli eventi della giornata."


translate italian day5_main3_8212c1e8:


    "Da un lato ero sopraffatto dalla fatica.{w} Dall'altro – avevo la continua sensazione di aver dimenticato qualcosa, di aver fatto qualcosa di sbagliato, di aver detto qualcosa di sbagliato…"


translate italian day5_main3_a6f64aad:


    "E tale sensazione di incompletezza mi stava tormentando."


translate italian day5_main3_9cd093b9:


    "Erano circa le due di notte."


translate italian day5_main3_7962d319:


    "Tutte le cose brutte sarebbero finite prima o poi.{w} O almeno si sarebbero prese una pausa…"


translate italian day5_main3_d3e44219:


    "Mi sono addormentato."


translate italian day5_dv_b3a56e2b:


    "Solo Lena e Alisa spiccavano tra tutto quello splendore."


translate italian day5_dv_9494f7ea:


    "Beh, certamente era piuttosto normale per Alisa litigare con qualcuno in quel modo."


translate italian day5_dv_49a0c8bc:


    "Ma sentire Lena che alzava la voce…"


translate italian day5_dv_9844ee70:


    "Mi sono avvicinato silenziosamente, cercando di capire cosa stesse accadendo."


translate italian day5_dv_6ca87438:


    dv "No, adesso ascoltami bene!"


translate italian day5_dv_76e80067:


    un "Pensa quello che vuoi, io ho già detto tutto!"


translate italian day5_dv_d1acba6b:


    th "Sembra una cosa seria."


translate italian day5_dv_7d833908:


    "Ho cercato in tutti i modi di non attirare l'attenzione su di me e fingere di essere lì per caso, senza mostrare alcun interesse nei confronti della loro conversazione."


translate italian day5_dv_65a0ca80:


    dv "Non c'è niente da pensare – è tutto chiaro come il sole!"


translate italian day5_dv_08837eb1:


    dv "Non agitare quel tuo ditino davanti a me, ti conosco troppo bene ormai."


translate italian day5_dv_85d1fa2f:


    un "Ma certo, ovviamente tu sai sempre tutto! Allora perché non vai là e glielo dici in faccia?"


translate italian day5_dv_cf4dd80f:


    "Lena si stava arrabbiando. Il che era davvero strano, pur considerando il fatto che non sapevo di cosa stessero discutendo."


translate italian day5_dv_7dfc2afa:


    dv "Non sono affari tuoi!"


translate italian day5_dv_12152c43:


    "Alisa ha sbuffato, si è voltata e i suoi occhi hanno incontrato i miei."


translate italian day5_dv_b91f8b82:


    "Un secondo più tardi anche quelli di Lena mi hanno raggiunto."


translate italian day5_dv_aeac4053:


    "Le ragazze sono rimaste lì confuse per un po'."


translate italian day5_dv_a22d002f:


    dv "Ma…{w} Stavi origliando?!"


translate italian day5_dv_e808d0e5:


    me "Io?{w} Oh, no-no! Stavo solo… cioè… passando di qua…"


translate italian day5_dv_61947ed2:


    "Ho tentato di fare il miglior sorriso possibile, ma è servito a poco."


translate italian day5_dv_f829601e:


    dv "A proposito, oggi mi stava spiando di nascosto!"


translate italian day5_dv_31e62164:


    "Alisa ha fatto un sorriso maligno."


translate italian day5_dv_74229088:


    "Lena mi ha guardato con aria interrogativa."


translate italian day5_dv_57da622c:


    th "Anzitutto, perché dovrebbe credere alle storie di quella subdola volpe?"


translate italian day5_dv_d122756b:


    me "Non ti stavo spiando!{w} Sai bene che è stato un malinteso! È stata tutta colpa di Ulyana!"


translate italian day5_dv_f4cf67d8:


    dv "Come no – vallo a dire alla polizia!"


translate italian day5_dv_b0cbdad9:


    th "Sembra che la mia scusa non abbia funzionato."


translate italian day5_dv_93816b7f:


    dv "Ti sono piaciute le mie tette?"


translate italian day5_dv_169666e8:


    "Un brivido mi è sceso lungo la schiena."


translate italian day5_dv_8bfbf78c:


    un "È…{w} vero?"


translate italian day5_dv_b17dc3c0:


    "Mi ha chiesto Lena, implorante."


translate italian day5_dv_78824aab:


    me "Beh, vedi, è successo per caso…{w} E comunque non ho proprio visto niente."


translate italian day5_dv_7394ce16:


    dv "Non hai visto niente? Beh, allora posso mostrartele di nuovo!"


translate italian day5_dv_a55bd08a:


    "Ha esclamato Alisa con rabbia."


translate italian day5_dv_a03136b3:


    "Non sapevo cosa dire."


translate italian day5_dv_c67e9088:


    dv "Visto, te l'avevo detto!"


translate italian day5_dv_709e3a50:


    un "No… No…"


translate italian day5_dv_0d581cb1:


    "Lena ha iniziato a borbottare, ed è corsa via un attimo dopo."


translate italian day5_dv_87a2f256:


    me "Aspetta, cos'è successo?!"


translate italian day5_dv_3da1183b:


    "Le ho gridato mentre si allontanava."


translate italian day5_dv_0cd43818:


    dv "Visto cos'hai fatto – hai sconvolto quella povera ragazza!"


translate italian day5_dv_31e62164_1:


    "Mi ha fatto un sorriso malevolo."


translate italian day5_dv_fab61109:


    me "Prima cosa – non sono stato io a sconvolgerla! Seconda cosa – non ho la più pallida idea di cosa stavate discutendo!"


translate italian day5_dv_2b19dcc7:


    "La mia pazienza aveva un limite."


translate italian day5_dv_b578411d:


    dv "E perché dovrei dirtelo?"


translate italian day5_dv_65ea82d5:


    "Sembrava che Alisa considerasse concluso il nostro discorso, ed era in procinto di andarsene."


translate italian day5_dv_0c737116:


    "L'ho afferrata bruscamente per il braccio."


translate italian day5_dv_6a189dfc:


    "Alisa mi ha guardato spaventata, ma non ha detto nulla."


translate italian day5_dv_277c9533:


    "Ero arrabbiato in quel momento.{w} Molto arrabbiato!"


translate italian day5_dv_f6618979:


    "Per prima cosa, ero estremamente arrabbiato per il fatto che Lena fosse sconvolta per causa mia (anche se effettivamente ero in qualche modo colpevole)."


translate italian day5_dv_7c8a30ee:


    "Seconda cosa, ero furioso per l'insolenza di Alisa."


translate italian day5_dv_513493ad:


    "E terza cosa, ero così stanco che avrei potuto perdere le staffe per la minima sciocchezza."


translate italian day5_dv_2bf8f738:


    me "Sei contenta adesso?"


translate italian day5_dv_9c13f0cc:


    "Ho sibilato, indicando nel buio la direzione in cui era fuggita Lena."


translate italian day5_dv_240840c7:


    me "L'unica cosa che sai fare è far saltare in aria i monumenti e ferire le ragazze innocenti!"


translate italian day5_dv_f71a0fac:


    "Sembrava spaventata, addolorata e confusa allo stesso tempo."


translate italian day5_dv_050fb154:


    dv "Quella lì…{w} potrebbe far del male a chiunque!{w} È solo che non la conosci!"


translate italian day5_dv_73f6779b:


    me "Forse hai ragione che non la conosco molto bene. Ma sono assolutamente certo che lei non farebbe mai del male a nessuno! Quello è un lavoro per te!"


translate italian day5_dv_03cd505c:


    "Siamo rimasti in silenzio per un po'."


translate italian day5_dv_60d9b7af:


    "Tenevo Alisa per il braccio, non sapendo cos'altro fare."


translate italian day5_dv_47ae85aa:


    "La soluzione è arrivata da sé."


translate italian day5_dv_370eed20:


    me "Devi andare da lei e scusarti!"


translate italian day5_dv_f9f3ed76:


    dv "Perché diavolo dovrei?"


translate italian day5_dv_fd492212:


    "Alisa ha tentato di apparire arrogante come sempre, ma questa volta non avrebbe funzionato."


translate italian day5_dv_7b8ee948:


    me "Perché lo dico io!"


translate italian day5_dv_983d54f9:


    "Mi sono rifiutato di ascoltare eventuali obiezioni, e l'ho trascinata verso il falò."


translate italian day5_dv_ff2ed8eb:


    me "Olga Dmitrievna, mi dispiace, ma abbiamo delle questioni urgenti da sbrigare, quindi noi due ce ne andiamo adesso."


translate italian day5_dv_7d32c910:


    "La guida ufficiale ha cercato di opporsi, ma io non l'ho ascoltata, e mi sono precipitato verso la direzione dove se n'era andata Lena, trascinando Alisa dietro di me."


translate italian day5_dv_a69e912e:


    "Dopo un po' di tempo l'ho lasciata andare."


translate italian day5_dv_e687fb2b:


    me "Qualche obiezione?"


translate italian day5_dv_552eae04:


    "Tutto l'orgoglio e l'arroganza di Alisa l'avevano abbandonata da tempo."


translate italian day5_dv_5f5a4508:


    dv "Se proprio vuoi che io venga con te lo farò, ma non ho niente di cui scusarmi – le ho semplicemente detto la verità."


translate italian day5_dv_f99126ea:


    me "Lo vedremo."


translate italian day5_dv_a767512c:


    "Ho tagliato corto."


translate italian day5_dv_29f6f60c:


    "Abbiamo camminato in silenzio, finché abbiamo raggiunto il Campo."


translate italian day5_dv_9cee08fa:


    "Siamo giunti alla piazza e ho chiesto ad Alisa:"


translate italian day5_dv_2442aa77:


    me "E adesso dove?"


translate italian day5_dv_37160fae:


    dv "Che ne so?"


translate italian day5_dv_65c09781:


    me "Chi ha detto di conoscere bene Lena? Sono stato forse io?!"


translate italian day5_dv_571e60de:


    "Alisa ha esitato."


translate italian day5_dv_8a9865ff:


    dv "Beh, potrebbe essere andata all'isola…{w} Ci va spesso quando vuole stare un po' da sola."


translate italian day5_dv_a499094f:


    me "Eccellente!"


translate italian day5_dv_c8bd92fc:


    "Ci siamo avviati verso il molo."


translate italian day5_dv_cffd7334:


    "È calata la notte sul Campo mentre io cercavo i remi, e ho iniziato a trafugare con le corde."


translate italian day5_dv_07f664e2:


    "In effetti c'era una barca in meno."


translate italian day5_dv_e631a0c0:


    me "Bene, oggi è il tuo giorno fortunato, farai esercizio fisico per rafforzare i bicipiti, i tricipiti, e gli altri muscoli delle braccia."


translate italian day5_dv_27c62366:


    "Ho detto con sarcasmo."


translate italian day5_dv_25a535c0:


    dv "Dici sul serio?"


translate italian day5_dv_883a35cc:


    "Alisa mi ha guardato preoccupata."


translate italian day5_dv_183b833e:


    th "Beh, forse sarebbe un po' scabro.{w} Persino nei suoi confronti…"


translate italian day5_dv_5b893c52:


    me "Va bene, non ti farò remare, ovviamente…"


translate italian day5_dv_1b5cc40c:


    "Ho detto, in tono poco sicuro."


translate italian day5_dv_36f1c607:


    "Questa volta roteare i remi si è rivelato molto più difficile del previsto."


translate italian day5_dv_a5ba40b7:


    "Il che non mi ha sorpreso, considerando che oggi dovevo aver già oltrepassato il mio tasso annuale di canottaggio."


translate italian day5_dv_968f6af6:


    "Mi sono fermato nel mezzo del fiume per riprendere fiato."


translate italian day5_dv_ee0bd590:


    "Il Campo era ormai immerso nella notte.{w} Una notte normale e insignificante."


translate italian day5_dv_46ada0f2:


    "Era una di quelle notti dove il cielo oscuro, le stelle e la luna crescente non apparivano come un qualcosa di speciale, e il frinire dei grilli e il cinguettio degli uccelli sembravano una routine noiosa, piuttosto che una sinfonia notturna."


translate italian day5_dv_4b0e60fa:


    "Stavo fissando attraverso l'oscurità, cercando di distinguere in lontananza i contorni dell'isola."


translate italian day5_dv_b4ddcd02:


    me "Come fai a conoscerla così bene?"


translate italian day5_dv_3f85db61:


    "Alisa ha rabbrividito un po'."


translate italian day5_dv_e3340992:


    dv "Siamo cresciute insieme."


translate italian day5_dv_0b791288:


    "Ha detto, dopo un istante di esitazione."


translate italian day5_dv_feaeb3c3:


    me "E siete finite nello stesso campo di pionieri.{w} Che favolosa coincidenza!"


translate italian day5_dv_826081ef:


    "Tutte quelle domande, misteri e paure che avevo nascosto così bene finora, stavano venendo a galla nella mia mente."


translate italian day5_dv_4cdcce8a:


    th "Accidenti, potrei semplicemente buttarla giù dalla barca e affogarla all'istante."


translate italian day5_dv_8a51e645:


    th "Posso pretendere delle risposte."


translate italian day5_dv_01dabe85:


    th "Ma sarebbe un po' rischioso, a meno che io sappia quello che sto facendo."


translate italian day5_dv_d79d8ce6:


    "Ma questa volta ero sicuro di avere i pezzi bianchi sulla scacchiera, avendo la possibilità di fare la prima mossa, e non semplicemente parare i colpi dell'avversario."


translate italian day5_dv_9c1029af:


    me "Allora, che tipo di campo è questo?"


translate italian day5_dv_acae4786:


    "Alisa mi ha fissato con sguardo assente."


translate italian day5_dv_38f0a76d:


    me "Dove siamo? Perché sono qui?"


translate italian day5_dv_27b39f33:


    "È rimasta in silenzio."


translate italian day5_dv_46287afa:


    me "Rispondimi!"


translate italian day5_dv_91d35e95:


    "Ho urlato."


translate italian day5_dv_3f5fc648:


    dv "Ehi, cos'hai che non va…?{w} Se vuoi che io chieda scusa a Lena va bene…"


translate italian day5_dv_6579c7ab:


    "Ed ecco il momento in cui mi sono reso conto di quanto stupidamente mi stavo comportando, per non dire altro."


translate italian day5_dv_7113a55a:


    th "Lei potrebbe semplicemente non sapere niente di... quelle cose."


translate italian day5_dv_7ebb2614:


    th "E per di più…{w} Non sarebbe in grado di mentire in modo così convincente."


translate italian day5_dv_481f6bb0:


    "La sua espressione timorosa era un ulteriore motivo per crederle."


translate italian day5_dv_44b2f075:


    me "Scusa…"


translate italian day5_dv_613ae970:


    "Era tutto quello che sono riuscito a dire in quel momento."


translate italian day5_dv_2283e3ea:


    "Ma volevo mantenere il mio vantaggio."


translate italian day5_dv_8b39987d:


    "Gli ultimi metri sono stati davvero duri."


translate italian day5_dv_1fa7fb2a:


    "Sono sceso dalla barca e mi sono sdraiato per terra, esausto."


translate italian day5_dv_4942088d:


    "Alisa stava lì vicino, fissandomi."


translate italian day5_dv_395406fd:


    "Avevo l'impressione che stesse per fare una qualche battuta pungente, ma dato che continuava a rimanere in silenzio, ho pensato che fosse ancora scioccata o qualcosa del genere."


translate italian day5_dv_7de75851:


    "Anche se era piuttosto buio qui, sarebbe stato più facile nascondersi nel mio vecchio appartamento piuttosto che su quell'isoletta."


translate italian day5_dv_8f34fcaa:


    "Inoltre, Lena probabilmente si era già accorta della nostra presenza mentre stavamo attraversando il fiume, e quindi era troppo tardi per fare altre domande ad Alisa, anche se ne avevo ancora un po'."


translate italian day5_dv_17717ee8:


    me "Senti…"


translate italian day5_dv_3b8d3ec7:


    "L'ho guardata ma non vedevo nulla – era una notte molto buia e non avevo gli occhi di gatto."


translate italian day5_dv_0940755d:


    me "Allora, quando troveremo Lena le dirai che non volevi ferire i suoi sentimenti, e che ciò che è accaduto stamattina è stato tutto un malinteso, ok?"


translate italian day5_dv_cb693d1d:


    "Il mio tono non sembrava così sicuro come lo è stato qualche minuto prima, ma lo era abbastanza."


translate italian day5_dv_64847fff:


    "Anche se Alisa non mi ha risposto, era chiaro che avesse acconsentito."


translate italian day5_dv_89f2dd53:


    "Mi sono alzato in piedi e siamo andati a cercare Lena."


translate italian day5_dv_9ca3b435:


    "La sua barca è stata legata ad uno scoglio sul lato opposto dell'isola."


translate italian day5_dv_a5b3f875:


    me "È molto astuto da parte sua.{w} Nessuno potrebbe notare la sua barca da lontano."


translate italian day5_dv_3cc8517a:


    "Ho mormorato."


translate italian day5_dv_afb89a5f:


    me "Bene, questo significa che è da qualche parte qui tra gli alberi, cerchiamola!"


translate italian day5_dv_90bebd35:


    "Mi sono avviato verso il boschetto, ma Alisa è rimasta dov'era."


translate italian day5_dv_889111c0:


    me "Che c'è?{w} Credevo avessimo discusso già tutto…"


translate italian day5_dv_f2f597bd:


    dv "Eh…{w} Non è per quello…{w} È solo che…{w} è buio… laggiù…"


translate italian day5_dv_8d507ec8:


    "Ho dovuto sforzare gli occhi per poter distinguere il suo viso."


translate italian day5_dv_28c1ce27:


    me "Questo boschetto è più piccolo del giardino di margherite di mia nonna. Cosa c'è da temere?"


translate italian day5_dv_da6c23b0:


    "Non ha risposto."


translate italian day5_dv_f4740084:


    me "Se proprio vuoi sapere come la penso – preferirei lasciarti qui. Chissà quali sorprese mi potresti riservare!"


translate italian day5_dv_a1bcd7f2:


    dv "Bene…"


translate italian day5_dv_6239b879:


    "Si è avvicinata a me."


translate italian day5_dv_0127153c:


    dv "Andiamo così allora…"


translate italian day5_dv_9a59280f:


    "Alisa mi ha tenuto per il braccio."


translate italian day5_dv_18180057:


    "Non me lo sarei aspettato."


translate italian day5_dv_09c023e9:


    "Le mie guance sono diventate rosse e calde, mi mancava il respiro e non riuscivo a dire quasi nulla."


translate italian day5_dv_729fa06e:


    "Questa situazione non favoriva la nostra conversazione, e forse era meglio così, dato che non sarei stato comunque in grado di dire nulla di intelligente."


translate italian day5_dv_140f9a5e:


    "Ci siamo addentrati lentamente nel boschetto."


translate italian day5_dv_dbdafe82:


    "Dopo un attimo ho ripreso un po' il controllo di me stesso."


translate italian day5_dv_f8e9e2d5:


    me "Andiamo, non c'è niente di cui aver paura.{w} Siamo venuti qui durante il giorno, a raccogliere le fragole – Non ci sono stati problemi. È un normale boschetto di betulle…"


translate italian day5_dv_a6dda4a2:


    me "È davvero bello qui durante il giorno."


translate italian day5_dv_29bf0ac9:


    "Ho aggiunto, dopo una breve pausa."


translate italian day5_dv_2c4d16f2:


    "Alisa non mi ha guardato, stava fissando qualcosa in lontananza."


translate italian day5_dv_57ad0457:


    me "Beh, è una cosa soggettiva, dopotutto…"


translate italian day5_dv_dbfec409:


    "Pochi istanti più tardi ha indicato qualcosa davanti a noi."


translate italian day5_dv_65ced1f6:


    dv "Eccola."


translate italian day5_dv_5460ca51:


    "Ho sbirciato nel buio, ma non sono riuscito a vedere quasi nulla."


translate italian day5_dv_9e040406:


    "Quando ci siamo avvicinati, abbiamo visto Lena seduta accanto ad un albero.{w} Stava piangendo."


translate italian day5_dv_41ec3a67:


    un "Perché sei venuto qui?"


translate italian day5_dv_563f0f0a:


    "Ha chiesto in lacrime."


translate italian day5_dv_8fc2a6d9:


    un "E tu…"


translate italian day5_dv_0b024121:


    "Ha guardato Alisa, ma non è riuscita a terminare la frase."


translate italian day5_dv_86522e17:


    "In quel momento Alisa ha lasciato il mio braccio."


translate italian day5_dv_498638c2:


    me "Ecco, vedi…"


translate italian day5_dv_9eb817ac:


    dv "Volevo solo scusarmi con te per essere stata un po' dura e maleducata…"


translate italian day5_dv_272bb3ad:


    "Alisa mi ha interrotto.{w} La sua voce aveva riacquistato quella sua solita arroganza."


translate italian day5_dv_7fc953c2:


    "Forse perché non voleva apparire debole di fronte a Lena."


translate italian day5_dv_0ed5b186:


    un "Che scusa perfetta!{w} I fatti contano più delle parole!"


translate italian day5_dv_9b03effe:


    un "Che cosa intelligente, insistere sul fatto che io abbia fatto qualcosa di sbagliato, e poi fare la stessa cosa!"


translate italian day5_dv_991e4d49:


    "Lena ha cominciato a gridare in modo incontrollabile."


translate italian day5_dv_d2b96176:


    dv "Non hai capito niente…"


translate italian day5_dv_04d782d6:


    un "Capisco esattamente quello che ho visto!"


translate italian day5_dv_35da24e2:


    me "Ehm… Vedete, ragazze…{w} State litigando di nuovo e sembra che io sia in qualche modo coinvolto, ma non riesco a capirci niente!"


translate italian day5_dv_c501f81c:


    un "Spiegaglielo tu."


translate italian day5_dv_6b44c45f:


    "Ha detto Lena ad Alisa, con un ghigno."


translate italian day5_dv_921308b4:


    dv "Non c'è niente da spiegare…"


translate italian day5_dv_006275ed:


    un "Beh, allora forse posso provarci io?"


translate italian day5_dv_319d4fcb:


    dv "Fa' come ti pare!"


translate italian day5_dv_184d37fc:


    "Ha incrociato le braccia e si è girata."


translate italian day5_dv_a958d2ed:


    un "Bene allora, Semyon…{w} Lei mi ha rimproverata per averci provato con te."


translate italian day5_dv_c52b3095:


    "Per un minuto c'è stato un assoluto silenzio."


translate italian day5_dv_e8d3e7f3:


    "Lena sembrava non voler aggiungere altro, e in quanto a me, stavo ancora cercando di mettere assieme tutti i pezzi del puzzle."


translate italian day5_dv_149b3064:


    me "Cosa intendi?"


translate italian day5_dv_885d4f38:


    un "Esattamente quello che ho detto."


translate italian day5_dv_220fc882:


    dv "Non è vero, non è quello che intendevo dire."


translate italian day5_dv_f15e5159:


    "L'ha interrotta Alisa."


translate italian day5_dv_bbd6ebea:


    un "Ma certo…{w} Con te è sempre così…"


translate italian day5_dv_6771fae9:


    "Lena è scoppiata di nuovo in lacrime."


translate italian day5_dv_7a91f6b6:


    me "Aspetta, come…?"


translate italian day5_dv_99a3bbc7:


    th "E così... io piaccio a Lena?"


translate italian day5_dv_b711211f:


    un "E anche tu! Cerchi di convincermi che non è vero mentre invece vai a braccetto con lui!"


translate italian day5_dv_56b5ad1b:


    th "Che cosa? Allora piaccio anche ad Alisa?!{w} Oh, per favore, Alisa che ama qualcuno? Non può essere..."


translate italian day5_dv_428cd21d:


    dv "È solo che è buio qui e…"


translate italian day5_dv_72b459e3:


    un "Lo vedi come sei.{w} Sempre così arrogante, cerchi di manipolare gli altri, ma quando si tratta di te stessa invece..."


translate italian day5_dv_da6c23b0_1:


    "Alisa non ha risposto."


translate italian day5_dv_d66e57b9:


    "Mi sentivo in dovere di dire qualcosa per calmare le acque."


translate italian day5_dv_246e6c69:


    me "Ehi, aspettate…{w} Forse è tutto un malinteso…{w} Io…"


translate italian day5_dv_443723c9:


    dv "Questo non ha nulla a che fare con te…"


translate italian day5_dv_b386505f:


    "La voce di Alisa era stranamente distante."


translate italian day5_dv_013ab4b3:


    me "A giudicare dalla conversazione, tutto ha a che fare con me."


translate italian day5_dv_cb70eba8:


    un "Sì, è vero!{w} Chiedile cosa pensa di te e perché mi ha detto di non provarci con te!"


translate italian day5_dv_f7e5e4be:


    th "Non direi proprio che Lena «ci abbia provato» con me."


translate italian day5_dv_91cb39e7:


    dv "Ti ripeto, io…"


translate italian day5_dv_19fe9001:


    un "Ne ho abbastanza delle tue storie!"


translate italian day5_dv_eae66d30:


    "Lena è balzata in piedi ed è corsa via nel buio."


translate italian day5_dv_d1bcdf75:


    "Sono rimasto inchiodato al suolo, non sapendo cos'altro fare."


translate italian day5_dv_f72d2ca7:


    th "Se ora seguissi Lena, cosa le potrei dire quando l'avrò raggiunta?"


translate italian day5_dv_f0495ba4:


    th "Dopotutto, non ho ancora capito il motivo di tutto quel litigio."


translate italian day5_dv_09af90a3:


    th "I miei stupidi e ridicoli tentativi di consolarla la sconvolgerebbero ancora di più."


translate italian day5_dv_87ab0698:


    me "Immagino che sia meglio lasciarla da sola per un po'…"


translate italian day5_dv_e5989bb1:


    "Non sembravo molto sicuro di me."


translate italian day5_dv_448ae357:


    dv "Fa' come vuoi."


translate italian day5_dv_c19759e7:


    "Senza aggiungere altro Alisa si è diretta verso la barca."


translate italian day5_dv_a20cefa7:


    "..."


translate italian day5_dv_9525b0c0:


    "Il ritorno in barca è stato un inferno. Ogni decina o ventina di metri dovevo fermarmi e riposare."


translate italian day5_dv_7231fc65:


    "Alisa stava fissando il fiume, ignorandomi del tutto."


translate italian day5_dv_2ace92a8:


    "Bene, quella sembrava essere la prima situazione nel Campo che aveva oltrepassato notevolmente i limiti della mia comprensione."


translate italian day5_dv_5248806b:


    "Anche se a pensarci bene, si trattava di una banale situazione quotidiana."


translate italian day5_dv_c998baac:


    "Non mi sono sentito così confuso nemmeno durante la spedizione notturna al vecchio campo."


translate italian day5_dv_0fc4d70a:


    th "Dopotutto, pensandoci bene, non c'è alcuna cosa veramente strana riguardo a tutto ciò."


translate italian day5_dv_74caf240:


    th "Lena è una persona molto timida e forse anche introversa, ma dopotutto è umana e non può evitare del tutto le normali dinamiche umane."


translate italian day5_dv_6b02e0fc:


    th "Considerato ciò, la sua reazione è del tutto comprensibile in questa situazione."


translate italian day5_dv_8096860d:


    "Anche se non avevo capito il motivo della loro lite, è chiaro come il sole che non si sarebbero comportate così se non avessero avuto una buona ragione per farlo."


translate italian day5_dv_e82ce761:


    "E a quanto pare – la ragione ero io."


translate italian day5_dv_b467b940:


    "Ed è la cosa più strana di tutta questa storia."


translate italian day5_dv_b1b2da01:


    me "Ehi, mi dispiace che sia andata in quel modo.{w} Se solo non avessi ascoltato la vostra conversazione…"


translate italian day5_dv_6a0b3208:


    "Pare che io avessi un bisogno patologico di scusarmi."


translate italian day5_dv_443f185c:


    dv "Non è per niente colpa tua."


translate italian day5_dv_560d31b2:


    "Ha detto distrattamente."


translate italian day5_dv_d59d8325:


    dv "Ti sei trovato nel posto sbagliato al momento sbagliato, e hai fatto ripartire un meccanismo che era rimasto fermo da molto tempo."


translate italian day5_dv_3aad82ef:


    me "Non credo di capire…"


translate italian day5_dv_e1d83f9f:


    dv "È naturale che tu non capisca.{w} E non dovresti proprio adesso.{w} Lo capirai più avanti."


translate italian day5_dv_1b595140:


    "Mi ha lanciato un'occhiata intensa."


translate italian day5_dv_fca3e3d8:


    me "Dannazione!{w} Perché queste cose devono capitare sempre a me?!"


translate italian day5_dv_635de124:


    "Sia a scuola che al college, sembrava che i guai mi avessero preso di mira come loro obiettivo primario."


translate italian day5_dv_455102c5:


    "Qualcuno aveva bloccato la porta della classe con una scopa? Mi hanno scritto la nota sul diario."


translate italian day5_dv_f91c8482:


    "C'è stata una rissa? Tutto sommato, dovevo averla iniziata io."


translate italian day5_dv_9e87993c:


    "Sono stato bocciato a un esame? Beh, era ovvio che fosse colpa mia, e che l'insegnante che mi odiava non c'entrasse proprio."


translate italian day5_dv_66fe6755:


    "Anche i miei genitori sembravano preferire incolpare sempre me, piuttosto che considerare i fatti come delle circostanze sfavorevoli, o un coinvolgimento di terzi, o la volontà della Provvidenza."


translate italian day5_dv_6130f8de:


    "Ad un certo punto ho persino iniziato a credere che avessi una predisposizione naturale verso i guai. Avete presente la legge di Murphy?{w} Nel mio caso era «Se qualcosa di brutto deve accadere, allora accadrà proprio a me»."


translate italian day5_dv_c8a1ebf7:


    "Per questo motivo ho sempre cercato di tenermi lontano dai guai, dato che altrimenti sarei diventato il capro espiatorio."


translate italian day5_dv_ffc93cf9:


    "Beh, a giudicare da quest'oggi, non mi riusciva molto bene…"


translate italian day5_dv_7e2b13fc:


    dv "Pare che tu abbia un qualche tipo di aura – attiri l'attenzione su di te."


translate italian day5_dv_194f815a:


    "Sorpreso, ho guardato Alisa.{w} Stava sorridendo."


translate italian day5_dv_8a97cc6a:


    th "Sembra che lei riesca a leggermi nel pensiero…"


translate italian day5_dv_df9b37de:


    me "Per lo più problemi."


translate italian day5_dv_127fc146:


    dv "Chissà… Chissà…"


translate italian day5_dv_3fed2b26:


    "Ha detto con aria sognante, fissando il fiume."


translate italian day5_dv_4676d969:


    dv "È una notte stupenda, non trovi?"


translate italian day5_dv_a4b71fa9:


    me "Come ogni altra, a dire il vero."


translate italian day5_dv_223e723d:


    "Ho detto stancamente."


translate italian day5_dv_7e23ddcf:


    dv "È proprio quello che intendo…{w} Una notte come tante altre, niente di speciale. Il momento migliore per rendere davvero felice la nostra ammirevole leader del Campo."


translate italian day5_dv_3145d815:


    dv "Deve sapere che il suo pioniere preferito è in realtà un brutto pervertito che si diverte a spiare le ragazze nude!"


translate italian day5_dv_9821dca6:


    "Qualcosa è scattato di nuovo dentro di me..."


translate italian day5_dv_c606f827:


    me "Non starai dicendo sul serio, vero?"


translate italian day5_dv_aa6b4182:


    dv "E perché…{w} Certo che sono seria!"


translate italian day5_dv_d4711875:


    "Non sembrava affatto che stesse scherzando."


translate italian day5_dv_6fc6b848:


    me "Beh, fallo pure se proprio ti da soddisfazione, ma non credi che sia già un po' troppo tardi oggi?"


translate italian day5_dv_6d61f732:


    dv "Non dovresti remare?"


translate italian day5_dv_afbb39ba:


    "Mi sono accorto che la nostra barca era ancora alla deriva in mezzo al fiume."


translate italian day5_dv_0934364c:


    me "Aspetta!{w} Non cambiare argomento!"


translate italian day5_dv_78b06dfc:


    dv "Torniamo al Campo…{w} Ci penserò su un attimo."


translate italian day5_dv_20705088:


    "Non avevo intenzione di trascorrere l'intera notte su una barca, così ho dovuto continuare a remare."


translate italian day5_dv_1b0374d0:


    "Abbiamo raggiunto il molo con difficoltà."


translate italian day5_dv_ffb36c55:


    "Ero esausto. Sia per lo sforzo che per la minaccia di Alisa di raccontare a Olga Dmitrievna quello che era successo stamattina."


translate italian day5_dv_d45a2e04:


    "Conoscendo la nostra leader del Campo, la sua reazione era prevedibile."


translate italian day5_dv_cc9ba692:


    me "Allora adesso hai intenzione di andare subito da Olga Dmitrievna?"


translate italian day5_dv_f74976c4:


    dv "Perché no?"


translate italian day5_dv_3aa0974f:


    "Mi ha fatto il sorriso più malizioso di sempre."


translate italian day5_dv_6c9ff79e:


    me "Dopotutto è già tardi.{w} Starà sicuramente dormendo."


translate italian day5_dv_34c6ac1d:


    dv "Chi lo sa, potrebbe anche essere sveglia…"


translate italian day5_dv_e2560be6:


    me "E dai, sai bene che è stato un incidente! Un semplice equivoco innescato da Ulyana!"


translate italian day5_dv_43cdac3a:


    dv "No, non lo so…"


translate italian day5_dv_dec618ce:


    "Ha alzato le spalle e si è diretta verso il Campo."


translate italian day5_dv_86a32f18:


    "Ho raccolto le mie ultime forze e sono balzato in piedi, seguendola."


translate italian day5_dv_44ba1020:


    "In un primo momento ho pensato di afferrarla per il braccio, ma poi ho capito che non sarebbe stata una buona idea, dopo tutto quello che era accaduto oggi."


translate italian day5_dv_079f62e5:


    me "Aspetta…"


translate italian day5_dv_a4b6ed32:


    "Mi sono trascinato faticosamente accanto a lei."


translate italian day5_dv_86a0035c:


    me "Parliamone."


translate italian day5_dv_4f27c86c:


    dv "Perché, c'è qualcosa di cui parlare?"


translate italian day5_dv_1eeed204:


    "Abbiamo camminato in silenzio fianco a fianco per un po'."


translate italian day5_dv_a7a06c6d:


    "Almeno le ero grato per il fatto che camminasse lentamente, così potevo mantenere il passo."


translate italian day5_dv_5495a454:


    me "Ehi, c'è qualcosa che potrei fare per convincerti a non dire niente a Olga Dmitrievna?"


translate italian day5_dv_ddf334ae:


    dv "Non so, beh, forse… ci sarebbe una cosa…"


translate italian day5_dv_b3e9120f:


    "Siamo giunti alla piazza."


translate italian day5_dv_6378d82b:


    me "Allora, cosa?"


translate italian day5_dv_21c6f737:


    dv "Beh, smettila di provarci con Lena, ad esempio…"


translate italian day5_dv_0e345e20:


    th "Per l'amor di Dio! Cosa le fa pensare che io ci stia provando con Lena?!"


translate italian day5_dv_434f8b3b:


    "Ho iniziato a perdere la pazienza."


translate italian day5_dv_496ee797:


    me "Perché continui ancora a dire certe idiozie?!{w} Hai iniziato tu a litigare all'escursione, per colpa tua siamo dovuti andare all'isola, e adesso ricominci di nuovo!{w} Quando è troppo è troppo!"


translate italian day5_dv_4c611d30:


    me "Non ci sto provando con lei!"


translate italian day5_dv_922b2e90:


    "Alisa è rimasta immobile, mentre il lucente chiaro di luna illuminava il suo viso, mostrando su di esso evidenti segni di risentimento e dispiacere."


translate italian day5_dv_dbf16f4b:


    dv "Come se io non mi fossi accorta di come la guardi!"


translate italian day5_dv_b9cdb4df:


    me "E come la guardo?"


translate italian day5_dv_0ba7b0db:


    dv "In quel modo!"


translate italian day5_dv_d2b04ecc:


    me "Che modo?"


translate italian day5_dv_5f616c8b:


    dv "Lo sai bene!"


translate italian day5_dv_df0640da:


    "Ha distolto lo sguardo, ma è rimasta dov'era."


translate italian day5_dv_41e33f1a:


    me "Ehi, finiscila di vedere tutti attraverso la tua immaginazione contorta! Se proprio non riesci a smetterla di inventarti cose senza senso, almeno tienile per te!"


translate italian day5_dv_b8304cd4:


    me "Non far soffrire gli altri a causa delle tue storie! Non m'importa se te la prendi con me, ma non devi coinvolgere Lena!"


translate italian day5_dv_a6dd47ba:


    "Ho davvero perso le staffe alla grande, ma Alisa non ha risposto."


translate italian day5_dv_73cc2124:


    "Siamo sprofondati in un silenzio inaspettato, interrotto occasionalmente dai singhiozzi di Alisa."


translate italian day5_dv_1564df3a:


    me "Di male in peggio! Adesso piangi anche tu…{w} Siete forse impazziti tutti?"


translate italian day5_dv_7a6449b3:


    "Ho giunto le mani per la disperazione."


translate italian day5_dv_1a648809:


    "Potevo capire il motivo per cui Lena avesse pianto.{w} Ma veder piangere Alisa era a dir poco impensabile…"


translate italian day5_dv_e14b3828:


    "Non riuscivo proprio a capirne il motivo."


translate italian day5_dv_f6741270:


    "In qualsiasi altro momento ne sarei rimasto sicuramente scioccato, ma in quell'istante ero semplicemente troppo stanco, di tutto."


translate italian day5_dv_6615be77:


    "La mia testa era completamente vuota."


translate italian day5_dv_99ab0d84:


    "Anzi, era così piena e pesante che non c'era più spazio nemmeno per un singolo pensiero."


translate italian day5_dv_e447cec9:


    "Volendo fare un paragone, se generalmente il mio cervello può essere immaginato come una vasta autostrada percorsa da migliaia di pensieri a tutta velocità, che si scontrano in continuazione generando incidenti caotici, adesso altro non era che una piccola stradina sperduta in una desolata foresta remota, percorsa solo in casi di assoluta necessità."


translate italian day5_dv_c1d95e71:


    "Alisa è rimasta in silenzio. Ma almeno ha smesso di piangere."


translate italian day5_dv_3c584237:


    me "Se la cosa ti sembra davvero così grave, allora va' pure da Olga Dmitrievna…{w} Spero ti faccia sentire meglio…"


translate italian day5_dv_bd4f7e30:


    dv "D'accordo, non lo farò…"


translate italian day5_dv_8e49058e:


    "Ha detto timidamente, girandosi verso di me."


translate italian day5_dv_f381d5cd:


    "Le lacrime se n'erano andate, ma il suo viso sembrava ancora esprimere un dolore assoluto nei confronti del mondo."


translate italian day5_dv_3e47cc5e:


    dv "È solo che mi sento ferita…"


translate italian day5_dv_e6e0ffb2:


    me "Ferita da cosa?"


translate italian day5_dv_b037ba1a:


    "Le ho chiesto stancamente."


translate italian day5_dv_8926ae0c:


    dv "È sempre così.{w} Lei riceve tutte le attenzioni, e io vengo messa in disparte."


translate italian day5_dv_5637fae3:


    "Non riuscivo a capire di cosa stesse parlando, così ho deciso di assecondarla."


translate italian day5_dv_1f586e25:


    th "Almeno così mi lascerà in pace finalmente."


translate italian day5_dv_c1141a62:


    me "Non è vero.{w} Tu riesci ad attirare l'attenzione, le persone non ti ignorano. O almeno, io non ti ignoro."


translate italian day5_dv_5ec2967a:


    "Ho alzato gli occhi e l'ho guardata."


translate italian day5_dv_8d8f127d:


    "La sua espressione era un misto di stupore e speranza."


translate italian day5_dv_455fafb6:


    me "Capisci?"


translate italian day5_dv_694b9cc2:


    dv "Quindi tu…{w} Potresti…"


translate italian day5_dv_cee25919:


    "Tutto ad un tratto, la sua voce è diventata così tenera che ho indietreggiato per lo stupore."


translate italian day5_dv_20588e11:


    "Il viso di Alisa è diventato rosso, ed ha abbassato lo sguardo."


translate italian day5_dv_48f34051:


    dv "Allora…{w} Pensi davvero che io non sia peggiore di lei?"


translate italian day5_dv_0250a659:


    "Volevo dire qualcosa come «sei ancora meglio!», ma mi sono trattenuto."


translate italian day5_dv_693741eb:


    me "Sì, lo penso…"


translate italian day5_dv_dab6d6c3:


    "Sembrava che lei non avesse capito che l'ho detto in modo del tutto insincero."


translate italian day5_dv_1d46b2fc:


    dv "Va bene, è ora di dormire!"


translate italian day5_dv_a412e07f:


    "Ha gridato allegramente."


translate italian day5_dv_ff8903e2:


    "Ora sembrava essere tornata sé stessa."


translate italian day5_dv_b2e53022:


    dv "Ci vediamo domani."


translate italian day5_dv_c3c87c93:


    "Ha agitato la mano e se n'è andata."


translate italian day5_dv_99742b57:


    "Ho tirato un sospiro di sollievo."


translate italian day5_dv_d688f7f1:


    th "Beh, per oggi è finita."


translate italian day5_dv_334bf905:


    "Ho investito le mie ultime energie in uno scatto verso la casetta di Olga Dmitrievna."


translate italian day5_dv_d055c80a:


    "La luce era spenta, e quindi mi sono spogliato silenziosamente e mi sono messo a letto, cercando di non svegliare la leader del Campo."


translate italian day5_dv_b0e44a4b:


    th "Interessante, che cosa intendeva dire Alisa?"


translate italian day5_dv_c93f9c04:


    th "E Lena...?"


translate italian day5_dv_2def8e8d:


    "Che cosa stava succedendo a quelle due ragazze, e a me – era come se fossimo stati risucchiati in una specie di vortice, che ha iniziato a roteare furiosamente con impeto."


translate italian day5_dv_9b5148d6:


    th "Ma come andrà avanti questa storia?"


translate italian day5_dv_801e05ca:


    th "E che fine farò io in tutto questo?"


translate italian day5_dv_a20cefa7_1:


    "..."


translate italian day5_sl_608d1f0d:


    "Ero proprio curioso di sapere dove fosse finita Slavya."


translate italian day5_sl_3ff2c172:


    "Tuttavia, volevo anche lasciare questo posto il più presto possibile e, se proprio non fossi riuscito a sdraiarmi e dormire, mi sarebbe bastato rimanere un po' da solo, lontano da tutti quei pionieri e da Olga Dmitrievna, che non vedeva l'ora di assegnarmi qualcosa di nuovo da fare."


translate italian day5_sl_2c4efb4d:


    "Ho aspettato il momento giusto e sono fuggito nella foresta."


translate italian day5_sl_d3fc10d2:


    "La notte è calata sul Campo.{w} Una notte del tutto normale e insignificante."


translate italian day5_sl_46ada0f2:


    "Una di quelle notti in cui il cielo scuro, le stelle o la luna crescente non scatenavano alcuna emozione in particolare, e il frinire dei grilli e il canto degli uccelli notturni sembravano più una routine quotidiana anziché un coro notturno."


translate italian day5_sl_42be4cb2:


    "Ho vagato per la foresta senza meta, cercando di non allontanarmi troppo dal Campo."


translate italian day5_sl_89eb1213:


    "Dopotutto, in questi boschi potevo rischiare di imbattermi in qualche pericolo, come ad esempio quel disastro naturale di Ulyana, che probabilmente sarebbe stato ancora peggio della leader del Campo."


translate italian day5_sl_4d903dc8:


    "Mi sono seduto su un albero caduto e ho iniziato a pensare."


translate italian day5_sl_905d8f87:


    "Perché doveva accadere tutto a me? Perché dovevo finire sempre in situazioni stupide, tutte le volte, ovunque?"


translate italian day5_sl_f58b1a89:


    th "Anche se sono apparso all'improvviso in uno strano campo di pionieri in mezzo al nulla, non sembro essere la cavia di un esperimento o la vittima di una cosmica mente malata, né sto partecipando ad una guerra intergalattica al fianco di un gruppo di pacifisti con tendenze suicide, come il tipico eroe delle storie di fantascienza."


translate italian day5_sl_013538e9:


    th "No, invece devo nascondermi in una foresta di notte, lontano da una delirante leader del Campo e dai suoi grandi lavoratori pionieristici…"


translate italian day5_sl_99250f31:


    "Le stelle brillavano nel cielo."


translate italian day5_sl_d9e32f7f:


    th "Forse in questo momento la loro luce non sta illuminando solo me e questo Campo, ma anche la città dove sono nato, dove si trova la mia vecchia casa…"


translate italian day5_sl_1b24971b:


    "Era come se avessi un dolore costante al petto."


translate italian day5_sl_3ef36634:


    "Ho immaginato il mio vecchio appartamento in tutti i suoi dettagli, e un detestabile bruciore ha cominciato a farsi strada tra il mio stomaco e la mia gola."


translate italian day5_sl_86074a46:


    "No, non si trattava di malinconia.{w} Era più come un triste ricordo."


translate italian day5_sl_30f40a77:


    "Perché nonostante tutto quello che è successo, mi sono sentito più vivo qui in questi cinque giorni che là negli ultimi anni."


translate italian day5_sl_d5ded2d1:


    "E non ero più sicuro di voler tornare indietro."


translate italian day5_sl_87600ecf:


    "Restava solo una domanda senza risposta – come e perché io fossi finito qui.{w} Quella domanda è divampata ancora una volta nella mia mente."


translate italian day5_sl_e3fa1442:


    "Ultimamente non ho dedicato molto tempo a cercare risposte, o anche solo a pensare alla mia situazione."


translate italian day5_sl_cba9268c:


    "I miei pensieri erano occupati con gli affari quotidiani di routine."


translate italian day5_sl_2e35c234:


    "E ora, per riuscire a distaccarmi ed essere in grado di rimanere lì per sempre, avrei dovuto capire la natura di quel luogo."


translate italian day5_sl_6534fa35:


    "È solo che persino un usignolo in una gabbia d'oro ha il diritto di sapere perché e per mano di chi esso sia finito lì dentro.{w} E solo dopo decidere se restare o meno…"


translate italian day5_sl_7e84e611:


    "Probabilmente mi sarei perso nelle mie domande esistenziali ancora a lungo, se non fosse stato per delle voci che si stavano avvicinando – erano Olga Dmitrievna e i suoi pionieri."


translate italian day5_sl_a025a4ba:


    th "Sembra che stiano continuando la loro escursione…"


translate italian day5_sl_b7831d31:


    "Mi sono incamminato in direzione del Campo."


translate italian day5_sl_20e62a68:


    th "Non ho alcun desiderio di tornare alla stanza della leader, perché verrò sicuramente rimproverato, e starmene seduto lì ad aspettare quel momento sarebbe ancora peggio."


translate italian day5_sl_607f0110:


    "Ben presto sono arrivato alla spiaggia."


translate italian day5_sl_5f1f337b:


    "E pare io non fossi l'unico qui – un'uniforme da pioniere giaceva sulla sabbia.{w} L'uniforme di una ragazza..."


translate italian day5_sl_4048e221:


    "Ma non vedevo nessuno nuotare nell'acqua."


translate italian day5_sl_274f5b11:


    "Stavo quasi pensando che si trattasse di qualche altra diavoleria, quando ho sentito una voce provenire da dietro."


translate italian day5_sl_6a030984:


    sl "Stai marinando l'escursione?"


translate italian day5_sl_51149e68:


    me "…"


translate italian day5_sl_19be3aa3:


    sl "Credevo che fossi ancora nel bosco."


translate italian day5_sl_74e314f2:


    "Mi sono voltato e ho visto Slavya in bikini."


translate italian day5_sl_ec56f2f6:


    me "Scusa, devo averti interrotto."


translate italian day5_sl_c3af5591:


    sl "Fa niente, ho quasi finito."


translate italian day5_sl_0bcdd9c2:


    me "Perché hai deciso di fare una nuotata durante la notte?"


translate italian day5_sl_bea64ff8:


    sl "Oh beh, è forse vietato?"


translate italian day5_sl_d1046a49:


    "Ha sorriso."


translate italian day5_sl_ff30fec3:


    me "Beh, no…{w} È solo che…{w} Non avrai problemi con Olga Dmitrievna per essertene andata prima…?"


translate italian day5_sl_ddc20430:


    sl "Ma anche tu hai fatto lo stesso!"


translate italian day5_sl_72b67782:


    "Mi ha lanciato un'occhiata rapida."


translate italian day5_sl_fe32f248:


    me "Già, l'ho fatto…"


translate italian day5_sl_3be00ada:


    "Mi sono seduto sulla sabbia, fissando il fiume."


translate italian day5_sl_9cd3b7aa:


    me "Non ti è piaciuta l'escursione, vero?"


translate italian day5_sl_d22b7e79:


    sl "No, non è per quello…{w} È solo che volevo stare da sola per un po'."


translate italian day5_sl_a21bded2:


    me "E io ti ho disturbata."


translate italian day5_sl_97ab3600:


    sl "No, non proprio."


translate italian day5_sl_4a2c94f6:


    me "Questo non è da te, per niente."


translate italian day5_sl_b31d38bf:


    sl "Di cosa stai parlando?"


translate italian day5_sl_dbe426b0:


    me "Beh, andartene in quel modo…"


translate italian day5_sl_51aa3969:


    sl "Guarda che non sono un robot che agisce solo secondo un programma prestabilito."


translate italian day5_sl_ec2c2a7c:


    "Ha riso."


translate italian day5_sl_078b05d1:


    me "Sì, hai ragione…"


translate italian day5_sl_3ca9afdd:


    "Ero ancora perplesso, e la mia fatica stava diventando sempre più intensa."


translate italian day5_sl_6615be77:


    "La mia testa era completamente vuota."


translate italian day5_sl_99ab0d84:


    "A dire il vero, era così pesante e in sovraccarico che non sarei riuscito a infilarci nemmeno un singolo pensiero in più."


translate italian day5_sl_e447cec9:


    "Volendo fare un paragone, se generalmente il mio cervello può essere immaginato come una vasta autostrada percorsa da migliaia di pensieri a tutta velocità, che si scontrano ininterrottamente generando incidenti caotici, adesso altro non era che una piccola stradina sperduta in una desolata foresta remota, percorsa solo in casi di assoluta necessità."


translate italian day5_sl_0458ec4c:


    "Così ho detto la prima cosa che mi è passata per la testa:"


translate italian day5_sl_00992d89:


    me "Non pensi che sia tutto abbastanza strano qui?"


translate italian day5_sl_706bd1ea:


    sl "Strano?"


translate italian day5_sl_15ae2aa5:


    me "Tutto quello che succede qui.{w} È il modello ideale di un campo di pionieri."


translate italian day5_sl_51310c74:


    me "Certo, non è che io ne sappia molto a riguardo, ma è esattamente come l'avevo immaginato."


translate italian day5_sl_b31d38bf_1:


    sl "Ma di cosa stai parlando?"


translate italian day5_sl_f3118ca2:


    "Slavya mi ha guardato in modo perplesso."


translate italian day5_sl_143220ed:


    me "Ti è mai capitato di sentirti come un estraneo?"


translate italian day5_sl_311a3a2f:


    sl "Non saprei."


translate italian day5_sl_f4af8a55:


    me "Voglio dire, di trovarti in un luogo dove non dovresti proprio essere.{w} Come se fossi a migliaia di chilometri da casa, o persino in un'altra galassia."


translate italian day5_sl_fb1ab657:


    sl "Non ti capisco…"


translate italian day5_sl_042de057:


    me "In questo siamo simili..."


translate italian day5_sl_5653f3b3:


    "Mi sono disteso supino e ho guardato le stelle."


translate italian day5_sl_b18f0221:


    me "E cosa penseresti se ti dicessi che io sono un alieno venuto dal futuro?"


translate italian day5_sl_33d5edca:


    sl "Lo sei?"


translate italian day5_sl_a0cddebc:


    "Mi ha chiesto molto seriamente."


translate italian day5_sl_399fd2dd:


    me "Beh, supponiamo che io lo sia.{w} Come potrei tornare al mio tempo?"


translate italian day5_sl_8068cf2d:


    sl "Lo vorresti davvero?"


translate italian day5_sl_5dcae275:


    th "Già, tutte le conversazioni con lei che riguardano la mia situazione, persino il minimo accenno, finiscono sempre allo stesso modo."


translate italian day5_sl_02fb630c:


    th "È come se tutte le volte mi stesse offrendo la possibilità di rimanere.{w} Quasi con insistenza."


translate italian day5_sl_5bb9832a:


    me "Beh, diciamo che non ne sono sicuro.{w} Nel {i}mio mondo{/i}, chiamiamolo così, mi sento a casa…{w} O più precisamente, mi è più familiare."


translate italian day5_sl_ba3caafa:


    me "Praticamente tutto mi è noto e sono preparato per qualsiasi situazione."


translate italian day5_sl_9d70ab7f:


    me "Mentre invece qui è il contrario, letteralmente ogni piccola cosa mi appare come inaspettata. Ed è tutto…{w} diverso."


translate italian day5_sl_863dcd94:


    sl "È davvero così brutto?"


translate italian day5_sl_56eeb493:


    me "Non direi brutto, piuttosto…{w} sconosciuto. Poco chiaro.{w} A volte può essere difficile riuscire a cambiare le cose. Soprattutto per uno come me."


translate italian day5_sl_83493bf7:


    sl "Ma cosa vuoi veramente?"


translate italian day5_sl_51fffd9d:


    me "Per cominciare, non posso rispondere a questa domanda finché non scopro dove si trova esattamente questo «qui»."


translate italian day5_sl_aaa90d0e:


    sl "Allora vai a scoprirlo!"


translate italian day5_sl_3b021425:


    me "Se solo fosse così facile…"


translate italian day5_sl_0b1c1ea9:


    sl "Ma cosa c'è di difficile?"


translate italian day5_sl_88ed05d2:


    me "Tutto è difficile!{w} Non saprei nemmeno da dove cominciare…{w} E vengo distratto in continuazione!"


translate italian day5_sl_84f92ed7:


    sl "Ne stai parlando in modo così serio, come se fosse davvero reale!"


translate italian day5_sl_ec2c2a7c_1:


    "Ha fatto una risata."


translate italian day5_sl_183aaa3f:


    me "Chi lo sa…"


translate italian day5_sl_c95262b6:


    "È seguito un lungo silenzio.{w} Improvvisamente, Slavya ha starnutito."


translate italian day5_sl_1beeb1c2:


    me "Salute."


translate italian day5_sl_309686e8:


    sl "Grazie!"


translate italian day5_sl_d0e6cc2c:


    me "Forse fare il bagno di notte non è stata una buona idea. Potresti ammalarti. Torna subito alla tua stanza, fa freddo qui fuori."


translate italian day5_sl_c393724a:


    sl "Non è niente, preferisco stare seduta qui con te ancora un po'.{w} Aspetta, mi rivesto."


translate italian day5_sl_fedfb810:


    "Beh, ne ero certamente lusingato, ma…"


translate italian day5_sl_85c017d1:


    me "Andiamo, ti accompagno."


translate italian day5_sl_d70f58e2:


    "Ma non siamo nemmeno riusciti a fare una decina di passi, che Slavya si è aggrappata al mio braccio."


translate italian day5_sl_75ba3164:


    me "Qual è il problema?"


translate italian day5_sl_d161ce4e:


    sl "Oh, mi gira un po' la testa…"


translate italian day5_sl_473c8882:


    "Le ho toccato la fronte.{w} Scottava."


translate italian day5_sl_de3adeeb:


    "Non sono mai stato in grado di misurare la temperatura corporea al tatto, ma in questo caso mi pareva ovvio."


translate italian day5_sl_8323f78a:


    me "Te l'avevo detto!"


translate italian day5_sl_1ba9f4d6:


    sl "Etciù!"


translate italian day5_sl_a15cb848:


    me "Dai, sbrighiamoci!"


translate italian day5_sl_4d435875:


    sl "No… Potrei contagiare Zhenya…{w} Senti, è meglio se andiamo all'infermeria."


translate italian day5_sl_44076421:


    me "E cosa vorresti fare in infermeria da sola di notte? Che sciocchezza!"


translate italian day5_sl_06c202f0:


    sl "No, non è una sciocchezza! Se non vuoi aiutarmi, allora ci andrò da sola!"


translate italian day5_sl_2cc25ffd:


    "Ha lasciato il mio braccio, e stava per andarsene."


translate italian day5_sl_a2051ad9:


    me "Non arrabbiarti.{w} Mettiti questo, fa freddo!"


translate italian day5_sl_b04f271d:


    "Le ho dato il mio maglione, quello che avevo portato con me durante l'escursione."


translate italian day5_sl_16c00715:


    th "Finalmente è servito a qualcosa di utile, bravo ragazzo…"


translate italian day5_sl_309686e8_1:


    sl "Grazie!"


translate italian day5_sl_6275b191:


    "Si è messa addosso il maglione e mi ha guardato in modo così tenero e riconoscente, che non sono più riuscito a contrariarla."


translate italian day5_sl_9fbebf7b:


    me "Va bene, come vuoi, andiamo all'infermeria allora!"


translate italian day5_sl_02e4ba3b:


    "Cinque minuti più tardi eravamo in piedi davanti alla porta dell'infermeria, mentre Slavya stava cercando la chiave giusta."


translate italian day5_sl_a22a6123:


    "L'idea mi sembrava comunque del tutto insensata.{w} Nessuno è mai morto per un comune raffreddore…"


translate italian day5_sl_84768b52:


    "O almeno non nel secolo scorso."


translate italian day5_sl_097ff7d4:


    "Non vedevo alcuna ragione particolare per cui trascorrere la notte in infermeria."


translate italian day5_sl_3a4dbc67:


    "Alla fine Slavya ha aperto la porta e si è appoggiata al mio braccio."


translate italian day5_sl_36203cca:


    sl "Sono ancora un po' stordita."


translate italian day5_sl_e5045fc4:


    "Ha detto in tono colpevole."


translate italian day5_sl_4461e513:


    "Si è sdraiata sul lettino, e io mi sono seduto sulla sedia lì accanto."


translate italian day5_sl_48f0ab25:


    me "Comunque, ascolta! Stare da sola in infermeria per tutta la notte…"


translate italian day5_sl_7da35453:


    th "Se andasse nella sua stanza, almeno Zhenya potrebbe portarle un bicchiere d'acqua, se le servisse.{w} Non si ammalerebbe – lei è giovane e forte."


translate italian day5_sl_7f80fc24:


    sl "Va tutto bene.{w} Non voglio disturbare nessuno. E l'infermiera arriverà domani."


translate italian day5_sl_3d2e3b21:


    "Mi sono immaginato nei panni di Slavya. Ho immaginato come avrei dovuto trascorrere l'intera notte qui dentro, in solitudine..."


translate italian day5_sl_460094c3:


    "E un brivido mi è sceso lungo la schiena."


translate italian day5_sl_3deaf8e7:


    me "Senti, forse potrei stare qui con te per un po'…"


translate italian day5_sl_a3c61bd4:


    "Dovevo molto a Slavya.{w} E comunque, non volevo proprio andarmene."


translate italian day5_sl_afee0832:


    sl "A quale scopo?{w} Andrà tutto bene. Grazie per avermi accompagnata. Adesso dovresti andare a dormire un po'."


translate italian day5_sl_aa120605:


    me "Credo comunque che…"


translate italian day5_sl_e9c90895:


    sl "Va tutto bene."


translate italian day5_sl_353eced4:


    "Per un attimo ho pensato..."


translate italian day5_sl_82680eaa:


    "Naturalmente non le sarebbe accaduto niente di terribile, ma mi sarei sentito più tranquillo se fossi rimasto qui con lei."


translate italian day5_sl_9fbe4547:


    me "Credo che..."


translate italian day5_sl_2b08672e:


    sl "Oh, suvvia!"


translate italian day5_sl_0d7f306b:


    "Ha gridato, come se si fosse offesa."


translate italian day5_sl_9f3f4e67:


    me "Non vorrai mica buttarmi fuori, vero?"


translate italian day5_sl_2e9b0de6:


    "Ho sorriso sornione."


translate italian day5_sl_a8c6f6ab:


    sl "D'accordo. Ma se ti ammalerai anche tu, peggio per te!"


translate italian day5_sl_7d705a39:


    "Ero compiaciuto del mio piccolo trionfo."


translate italian day5_sl_96286888:


    sl "Ok, allora che cosa facciamo?"


translate italian day5_sl_9a9c2e41:


    "C'erano delle carte da gioco nel cassetto."


translate italian day5_sl_14a83ddb:


    me "Durak?"


translate italian day5_sl_bf89e18e:


    "In realtà, non conoscevo altri giochi oltre a quello e il poker."


translate italian day5_sl_b58c757b:


    sl "Va bene."


translate italian day5_sl_403a4e5b:


    "Abbiamo giocato a lungo.{w} Ho completamente perso la cognizione del tempo."


translate italian day5_sl_5a3cb568:


    "Abbiamo chiacchierato e riso molto."


translate italian day5_sl_653470e9:


    "Slavya non sembrava per niente ammalata."


translate italian day5_sl_f248ccf9:


    "E poi è arrivata la mezzanotte…"


translate italian day5_sl_1bd398ca:


    sl "È ora di dormire."


translate italian day5_sl_6ded9a67:


    me "Forse…"


translate italian day5_sl_70dd0c01:


    sl "E tu dove andrai?"


translate italian day5_sl_8fb93608:


    "Mi ha guardato."


translate italian day5_sl_51906baa:


    sl "Dai.{w} Non dovresti stare seduto qui con me."


translate italian day5_sl_b550b4b7:


    me "Posso farlo comunque…"


translate italian day5_sl_617f93da:


    th "Al momento potrei persino addormentarmi in piedi..."


translate italian day5_sl_848c7c43:


    "Slavya mi ha guardato per un po', ha tirato la tenda e ha detto:"


translate italian day5_sl_df84d49a:


    sl "Buona notte!"


translate italian day5_sl_8de47ec6:


    me "Notte."


translate italian day5_sl_b551d5e5:


    "Ho abbassato la testa tra le mie braccia e mi sono addormentato in un istante."


translate italian day5_sl_56adc4c2:


    "Nonostante fossi esausto, non riuscivo a dormire bene in quel modo.{w} Era più un sonnecchiare."


translate italian day5_sl_c443185a:


    "La mia consapevolezza continuava ad abbandonarmi e ritornare."


translate italian day5_sl_b0ea066f:


    "Lo stato perfetto per sognare qualcosa di psichedelico…"


translate italian day5_sl_c118b5ae:


    "Il bus 410 ha attraversato la stanza, stava nevicando dal soffitto, e tutto d'un tratto il monitor del mio PC è apparso al centro del tavolo."


translate italian day5_sl_196f6c57:


    "Carte da parati si mischiavano sciogliendosi sulle pareti, mucchi d'immondizia sorgevano dal pavimento, e il mio vecchio letto si è materializzato in un angolo."


translate italian day5_sl_d7272335:


    "Ero di nuovo nel mio vecchio appartamento."


translate italian day5_sl_7fc90acf:


    "Poi il paesaggio è passato a quello della mia città natale."


translate italian day5_sl_bcd4a1a8:


    "Era come se stessi correndo tra una folla sconfinata di persone, avanti e indietro."


translate italian day5_sl_3624e130:


    "E poi l'oscurità.{w} Una primitiva e universale oscurità."


translate italian day5_sl_b5fa9f77:


    "Mi sono alzato di scatto in preda al terrore.{w} Stavo sudando come una fontana."


translate italian day5_sl_fe12675a:


    "Slavya mi stava guardando dal letto, preoccupata."


translate italian day5_sl_60108fae:


    me "Era solo un brutto sogno…"


translate italian day5_sl_15aba1cf:


    "Ho cercato di sorridere."


translate italian day5_sl_1c6cbe1a:


    sl "Stavi urlando…"


translate italian day5_sl_6c7c2069:


    me "Urlando cosa?"


translate italian day5_sl_f9a31b17:


    sl "Non lo so…{w} Era incomprensibile."


translate italian day5_sl_8be56802:


    "L'orologio mostrava solo le 0:30."


translate italian day5_sl_11adbbfc:


    sl "Semyon."


translate italian day5_sl_b6626098:


    me "Eh?"


translate italian day5_sl_5ef3b66e:


    sl "Sto congelando…"


translate italian day5_sl_564a8986:


    "Slavya stava sfregando le sue mani sul maglione, tremante."


translate italian day5_sl_a0d2cda7:


    me "Aspetta, cerco una coperta."


translate italian day5_sl_2b495447:


    "Ho cercato da un armadio all'altro, imprecando mentalmente per il fatto che la dannata infermeria non avesse niente con cui coprire i propri pazienti."


translate italian day5_sl_6ab83d4c:


    me "Mi dispiace, qui non c'è niente…"


translate italian day5_sl_640c2a39:


    me "Aspetta, corro a prendere qualcosa."


translate italian day5_sl_97419f60:


    sl "Non è necessario…{w} Non potresti stare sdraiato un po' qui con me? Così avrò più caldo."


translate italian day5_sl_c849a05b:


    "L'ha detto molto seriamente."


translate italian day5_sl_68527d19:


    "La sua voce era così compassionevole che mi ha lasciato senza fiato."


translate italian day5_sl_22609679:


    me "Sei sicura che…{w} io…{w} Non è un problema?"


translate italian day5_sl_ed91b7e2:


    sl "Lo è?"


translate italian day5_sl_07ed6d6a:


    "Ha chiesto, arrossendo."


translate italian day5_sl_7edd72b2:


    me "No, perché…"


translate italian day5_sl_d3c57f8c:


    "Mi sono tolto le scarpe lentamente e mi sono sdraiato sul bordo del letto, cercando di occupare il minor spazio possibile, provando a stringermi, trattenendo il fiato."


translate italian day5_sl_f211b57a:


    "Slavya mi ha abbracciato dolcemente, appoggiando la sua testa sul mio petto."


translate italian day5_sl_c61518ac:


    sl "Sì, così va meglio."


translate italian day5_sl_2481134c:


    "Ha fatto le fusa."


translate italian day5_sl_6d3a10e3:


    "Non sapevo cosa dire, e così sono rimasto lì tranquillo."


translate italian day5_sl_eb090c68:


    me "Bene..."


translate italian day5_sl_00a08a43:


    sl "Grazie."


translate italian day5_sl_9508d41b:


    me "Di cosa?"


translate italian day5_sl_f0f90483:


    "Slavya aveva gli occhi chiusi, e sembrava stesse per addormentarsi da un momento all'altro."


translate italian day5_sl_bca81bd5:


    sl "Per essere qui."


translate italian day5_sl_9a94410c:


    me "Non devi ringraziarmi..."


translate italian day5_sl_d663f86e:


    th "Solo pochi minuti fa ha cercato in tutti i modi di liberarsi di me."


translate italian day5_sl_fc7dc843:


    sl "No, davvero."


translate italian day5_sl_b320bfee:


    me "Di niente."


translate italian day5_sl_7a6acecb:


    "Non ha aggiunto altro, ma ero sicuro che stesse sorridendo."


translate italian day5_sl_77f7eb26:


    me "Sempre disponibile, come si suol dire!"


translate italian day5_sl_c5ee54e6:


    sl "Sei così gentile, così premuroso."


translate italian day5_sl_1a49756a:


    me "Beh, ci provo."


translate italian day5_sl_a0e4fdf2:


    sl "Sei un vero amico."


translate italian day5_sl_d1052636:


    me "Amico? Beh, sì, probabilmente..."


translate italian day5_sl_29a4e2ac:


    "Per qualche ragione quella parola mi ha ferito."


translate italian day5_sl_3b03b28d:


    "D'altra parte, non c'era motivo di credere che io fossi per Slavya qualcosa di più di un semplice amico..."


translate italian day5_sl_a2344f49:


    th "Ma perché sto pensando a queste cose?"


translate italian day5_sl_df7e98e4:


    me "Anche tu..."


translate italian day5_sl_954ffd8b:


    "Non riuscivo proprio a trovare le parole giuste."


translate italian day5_sl_7411ac41:


    th "Cos'è quel «anche tu»?{w} «È stato un piacere fare affari con te, speriamo in una duratura collaborazione reciprocamente vantaggiosa»?"


translate italian day5_sl_ada6764b:


    th "Oppure – «Già, è bello essere amici, andiamo a giocare nella sabbiera?»"


translate italian day5_sl_ecd07382:


    "Tutto questo è stato doppiamente difficile da sopportare, avendo Slavya sdraiata proprio accanto a me – non potevo correre, non potevo nascondermi, e anche se non mi stava guardando negli occhi, sapevo che riusciva comunque a vedere dentro di me."


translate italian day5_sl_a549e991:


    sl "C'è qualcosa che ti turba, non è così?"


translate italian day5_sl_98db6f67:


    me "No, sto bene."


translate italian day5_sl_ef63c01d:


    sl "Ma sono sicura che c'è qualcosa."


translate italian day5_sl_2d91a0c5:


    me "Beh, forse..."


translate italian day5_sl_5c6c0b54:


    sl "Ne vuoi parlare?"


translate italian day5_sl_99ee26fe:


    me "Beh, non proprio... In realtà, non c'è niente di cui parlare."


translate italian day5_sl_45f565d7:


    sl "Allora non sai esattamente cosa di turba?"


translate italian day5_sl_dab7f3be:


    me "Sì, forse."


translate italian day5_sl_e47336fd:


    sl "Allora perché ti senti così?"


translate italian day5_sl_c954d69f:


    "Ha riso."


translate italian day5_sl_3ae34e17:


    me "Credo che non dovrei."


translate italian day5_sl_dbfe4f1e:


    sl "Esatto!"


translate italian day5_sl_6dd013b2:


    "Mi ha abbracciato ancora più forte."


translate italian day5_sl_970eac9a:


    me "Adesso non hai più freddo?"


translate italian day5_sl_ad99cfcc:


    sl "Sto bene. Dimmelo se è scomodo per te."


translate italian day5_sl_5c1a0ecc:


    me "No, va bene."


translate italian day5_sl_ed54fb3b:


    sl "Come quella volta? Sulla barca?"


translate italian day5_sl_1339e6eb:


    "Nel sentire le sue parole ho percepito un dolore ai muscoli, come se si fossero ricordati di quella faticosa impresa."


translate italian day5_sl_b21be332:


    me "È completamente diverso."


translate italian day5_sl_8c47f676:


    sl "Bene, bene."


translate italian day5_sl_044e54c2:


    "Ha detto sornione, sollevando un po' la testa."


translate italian day5_sl_b78463d7:


    sl "Allora, è tutto a posto?"


translate italian day5_sl_f0ccc081:


    me "Sì, è tutto perfetto."


translate italian day5_sl_719f7d6e:


    "Mi stavo preoccupando. Era chiaro che Slavya si aspettasse qualcosa da me."


translate italian day5_sl_0793c467:


    "Voleva che io facessi o dicessi qualcosa."


translate italian day5_sl_bd971270:


    th "Ma cosa...?"


translate italian day5_sl_24ef8389:


    sl "Sei sicuro?"


translate italian day5_sl_626d142f:


    me "Sì...{w} Probabilmente."


translate italian day5_sl_440927ac:


    sl "Bene allora. Sogni d'oro!"


translate italian day5_sl_3c5ee540:


    me "Anche a te."


translate italian day5_sl_a20cefa7:


    "..."


translate italian day5_sl_a7b8346c:


    "Dopo un po' si è addormentata."


translate italian day5_sl_dbce3303:


    "Ho controllato l'orologio con la coda dell'occhio.{w} Era solo l'una di notte, e ciò significava che avrei dovuto restare in quella posizione fino al mattino."


translate italian day5_sl_0dd8c20c:


    "Non mi interessava del mio braccio che si stava intorpidendo."


translate italian day5_sl_dd2a66dd:


    "Essere così vicino a Slavya, così pericolosamente a contatto con lei. Non riuscivo proprio a dormire."


translate italian day5_sl_39c41ef8:


    "Vari pensieri stavano invadendo la mia mente..."


translate italian day5_sl_c93104db:


    th "Davvero lei considera normale questa situazione?"


translate italian day5_sl_a20cefa7_1:


    "..."


translate italian day5_sl_9719a2d8:


    "Proprio mentre stavo pensando a quali brutte cose sarebbero successe se qualcuno ci avesse visto insieme, la porta si è spalancata, e Olga Dmitrievna si è precipitata all'interno della stanza."


translate italian day5_sl_ede80479:


    "Ho avuto un tuffo al cuore."


translate italian day5_sl_96da4eb5:


    mt "Semyon…"


translate italian day5_sl_9142f8f6:


    "Ha esordito la leader del Campo, in un tono diabolicamente troppo calmo."


translate italian day5_sl_b51939f9:


    mt "Ti ho cercato dappertutto, e tu invece – prima fuggi dall'escursione anzitempo, poi non ti vedo tornare alla casetta a dormire, e adesso stai seducendo la nostra pioniera migliore!"


translate italian day5_sl_5e2bbb18:


    "Slavya si è svegliata di colpo."


translate italian day5_sl_3872252b:


    "Ha fissato la leader del Campo per alcuni secondi con occhi assonnati, poi si è resa conto di cosa stesse accadendo, ed è balzata giù dal letto."


translate italian day5_sl_110c77e8:


    sl "Olga Dmitrievna! Non è come sembra! Mi sono ammalata e Semyon mi ha accompagnata qui."


translate italian day5_sl_764cea7c:


    sl "E poi ho avuto freddo, e…{w} Gli ho detto di tornare alla sua stanza."


translate italian day5_sl_a4985f09:


    mt "Oh ma certo, come no! E così sei malata, vero?"


translate italian day5_sl_7b8fbab7:


    sl "Lo sono…"


translate italian day5_sl_4aa379e0:


    "Ha risposto Slavya, con umiltà."


translate italian day5_sl_dc4f5852:


    mt "Allora devi farti curare!"


translate italian day5_sl_bcebe051:


    "Mi ha guardato con aria minacciosa."


translate italian day5_sl_c2745a0c:


    mt "E tu! Alzati e seguimi!"


translate italian day5_sl_a7b2aa21:


    "Ho capito che era meglio non discutere con lei, così me ne sono andato dall'infermeria, senza nemmeno guardare Slavya."


translate italian day5_sl_3e2db886:


    "Abbiamo raggiunto la piazza senza dire una parola."


translate italian day5_sl_fb0b3f24:


    "Olga Dmitrievna mi ha guardato minacciosamente e ha detto:"


translate italian day5_sl_d702b0ee:


    mt "È ora di tornare a casa! Devo discutere con te di alcune cose."


translate italian day5_sl_f725c780:


    "Mi sono sentito male per tutto il tragitto."


translate italian day5_sl_8551a9d4:


    "La leader del Campo mi seguiva in silenzio."


translate italian day5_sl_0a646630:


    "Ecco un'altra stupida situazione nella quale sono caduto, e che sarebbe stata vista da tutti in modo totalmente sbagliato."


translate italian day5_sl_68e9f070:


    th "Ah… Nulla di cui discutere…"


translate italian day5_sl_7dbd4345:


    "E se mi fossi trovato nei panni di Olga Dmitrievna, cosa avrei pensato?{w} Due adolescenti che si coccolano in un'infermeria vuota… a notte fonda.{w} È piuttosto difficile inventarsi una scusa a riguardo."


translate italian day5_sl_25df23e4:


    "La cosa peggiore è che tutte queste cose accadevano sempre e solo a me!"


translate italian day5_sl_ce7660c4:


    "Mi sono sempre considerato come qualcuno dalla mente analitica. Anche se raramente ho avuto modo di usarla…"


translate italian day5_sl_f40fe17c:


    "Siamo entrati nella stanza, e la leader del Campo ha continuato il suo interrogatorio."


translate italian day5_sl_8237174c:


    mt "Vorresti spiegarmi cosa è successo?"


translate italian day5_sl_49ae3aab:


    "Sembrava essersi calmata un po'."


translate italian day5_sl_8ba11b89:


    "Almeno questo lato del suo carattere era una cosa positiva. Certamente, era una donna impulsiva, ma riusciva anche a calmarsi piuttosto rapidamente."


translate italian day5_sl_512994b2:


    me "Slavya le ha già detto tutto…"


translate italian day5_sl_43521b0d:


    mt "E ti aspetti che io le creda?"


translate italian day5_sl_49fe0ea8:


    me "Non posso costringerla a crederci, ma non ho nemmeno intenzione di inventarmi delle scuse, perché è la pura verità."


translate italian day5_sl_5fb08a35:


    "Mi ha guardato per un po', e poi ha detto:"


translate italian day5_sl_5be2bfc3:


    mt "Domani deciderò cosa fare di te."


translate italian day5_sl_9f74c45c:


    "La leader del Campo ha spento la luce, mi sono avvolto nella coperta, senza nemmeno preoccuparmi di spogliarmi, e mi sono girato verso la parete."


translate italian day5_sl_68e31fbe:


    th "Interessante, perché ha rimproverato solo me per quello che è successo?{w} Dopotutto non ero lì da solo."


translate italian day5_sl_5e211841:


    th "Certamente Slavya è il pioniere modello qui, ma…"


translate italian day5_sl_bfacbe72:


    "No, non intendevo addossare la colpa a lei. Provavo già abbastanza vergogna per quello che era successo, non c'era ragione di tuffarmi nuovamente in un'altra stupida situazione."


translate italian day5_sl_d8ca9160:


    "Involontariamente, senza pensieri maligni, senza aspettarsi alcuna trappola…"


translate italian day5_sl_53374338:


    "Avrei potuto stare lì a biasimarmi ancora a lungo, ma la stanchezza ha avuto il sopravvento, e alla fine la mia mente si è spenta."


translate italian day5_us_8f2500e8:


    "Quella sera non avevo mai pensato a Ulyana."


translate italian day5_us_4754cc34:


    th "Quell'incidente con la torta…{w} È come se non fosse mai accaduto…"


translate italian day5_us_d4e9d4ea:


    "Ma ricordavo bene quella sua espressione di risentimento e delusione."


translate italian day5_us_760ba5e1:


    "Forse in qualche altra circostanza non avrei mai pensato di andare da lei, ma adesso era il momento più adatto, il momento giusto per porre fine a questa stupida escursione."


translate italian day5_us_afda6c4c:


    "Considerando che Olga Dmitrievna mi aveva detto di tenermi pronto per qualche nuovo compito, ho deciso di non chiederle il permesso, e dopo aver aspettato il momento giusto, sono scappato nella foresta."


translate italian day5_us_d6e969ac:


    "La notte è calata sul Campo."


translate italian day5_us_fbf53d12:


    "Ho ringraziato mentalmente la leader del Campo per non essersi spinta troppo lontano nella foresta, dato che ciò significava che la civiltà era a poche centinaia di metri di distanza."


translate italian day5_us_94c6d083:


    "Un'altra lunga passeggiata nella foresta notturna non era nei miei piani."


translate italian day5_us_57b7303c:


    "Ben presto sono arrivato alla piazza e mi sono fermato, indeciso."


translate italian day5_us_6f52cdaf:


    th "Non sembrerà un po' stupido?"


translate italian day5_us_b54027f4:


    th "Che cosa dirò a Ulyana?{w} E in primo luogo, perché dovrei andare a trovarla?"


translate italian day5_us_4b80dcb9:


    "La mia testa era così piena e pesante che non c'era più spazio per altri pensieri."


translate italian day5_us_e447cec9:


    "Volendo fare un paragone, se generalmente il mio cervello può essere immaginato come una vasta autostrada percorsa da migliaia di pensieri a tutta velocità, che si scontrano incessantemente generando incidenti caotici, adesso altro non era che una piccola stradina sperduta in una desolata foresta remota, percorsa solo in casi di emergenza."


translate italian day5_us_cd65d7a1:


    "Quindi è meglio agire e non pensare a nulla."


translate italian day5_us_5365a619:


    th "Ma dove potrebbe essere?{w} Forse nella sua casetta?{w} E se non fosse lì?"


translate italian day5_us_cb5fdfc2:


    "All'improvviso qualcosa mi ha travolto e sono caduto per terra.{w} Per fortuna sono riuscito a mettere le mani davanti, altrimenti mi sarei rotto il naso…"


translate italian day5_us_759e5578:


    us "Preso!"


translate italian day5_us_447bdfb5:


    "Mi sono rialzato in fretta e ho visto Ulyana in piedi di fronte a me."


translate italian day5_us_1b093c3a:


    me "Ma che fai?!"


translate italian day5_us_88297410:


    "Nonostante la mia buona volontà, non sono proprio riuscito a trattenermi dal gridare."


translate italian day5_us_259f22bf:


    us "Non dovresti sognare ad occhi aperti!"


translate italian day5_us_2f6a59ef:


    "Ha detto con sarcasmo."


translate italian day5_us_056f2d76:


    me "E se mi fossi rotto il naso? O magari un braccio?"


translate italian day5_us_78d01ae3:


    "Ho detto in tono più calmo."


translate italian day5_us_7de3b85d:


    us "Beh, nessuna delle due cose è successa."


translate italian day5_us_e4e8e714:


    me "Senti, perché devi sempre farmi scherzi?"


translate italian day5_us_515f902a:


    "Mi stavo già pentendo della mia decisione di voler alleviare la sua solitudine."


translate italian day5_us_6ee354fc:


    me "Non ti è bastata la punizione di oggi? Riesci almeno a pensare a delle semplici conseguenze?"


translate italian day5_us_91d3b1af:


    us "È tutta colpa tua."


translate italian day5_us_2c703153:


    "Il sorriso è scomparso immediatamente dal viso di Ulyana."


translate italian day5_us_9afe61c9:


    me "Cosa esattamente sarebbe colpa mia?"


translate italian day5_us_4c55e5c0:


    us "Ogni cosa!"


translate italian day5_us_590ed0a7:


    me "Vuoi dire che è sempre colpa mia, di tutto?"


translate italian day5_us_551306e9:


    us "Sì!"


translate italian day5_us_184d37fc:


    "Ha incrociato le braccia e si è girata."


translate italian day5_us_eb6f6f93:


    me "Grandioso…"


translate italian day5_us_ad3d6bce:


    "Improvvisamente ho avuto il desiderio di allontanarmi da quel luogo il più rapidamente possibile."


translate italian day5_us_4ef07a9a:


    me "Ma sono venuto qui per te…"


translate italian day5_us_b7ef3111:


    us "Che cosa?"


translate italian day5_us_2940996c:


    me "Credevo che ti stessi annoiando, dato che tutti sono andati all'escursione e tu invece sei rimasta qui da sola…"


translate italian day5_us_7625dabe:


    us "Non m'importa!"


translate italian day5_us_1272c1de:


    "Ha gridato allegramente."


translate italian day5_us_36434b99:


    us "Beh, se le cose stanno così, cosa vuoi fare?"


translate italian day5_us_149b3064:


    me "Che intendi?"


translate italian day5_us_5db74333:


    us "Beh, dato che sei venuto qui per me!"


translate italian day5_us_5a38be43:


    "Non ci avevo proprio pensato."


translate italian day5_us_1206b412:


    th "Anche se non so davvero perché io sia tornato qui..."


translate italian day5_us_b8ee6c9c:


    me "Dopo un tale benvenuto non ho più voglia di fare niente."


translate italian day5_us_457fd234:


    us "Allora decido io!"


translate italian day5_us_f8556bbb:


    "Ha detto allegramente, e ha cominciato a pensare."


translate italian day5_us_36baf507:


    "L'ho fissata per un po', ma poi mi sono spazientito:"


translate italian day5_us_83506e3c:


    me "Senti, se stai ancora pensando di…"


translate italian day5_us_ba2766af:


    us "Ci sono!{w} Spaventiamo gli altri! Ci vestiremo da fantasmi, per esempio! Sarà davvero divertente!"


translate italian day5_us_9c7c48b0:


    th "Non ci trovo nulla di divertente."


translate italian day5_us_6a2846ee:


    me "Basta con queste cose…"


translate italian day5_us_532041e9:


    "Ho cominciato a dirle stancamente, ma mi sono fermato."


translate italian day5_us_146e3f57:


    th "Forse non è proprio una cattiva idea."


translate italian day5_us_49be75d5:


    "Tutto sommato, Ulyana non avrebbe fatto in tempo ad organizzare tutto che Olga Dmitrievna e i pionieri sarebbero già tornati."


translate italian day5_us_b8d157c1:


    "Se ero venuto qui per Ulyana, allora dovevo stare al suo gioco."


translate italian day5_us_22634694:


    me "Forse hai ragione…"


translate italian day5_us_b7ef3111_1:


    us "Come hai detto?"


translate italian day5_us_2dbdc7b7:


    "Mi ha guardato ad occhi spalancati."


translate italian day5_us_1b7f7977:


    us "Hai intenzione di cedere così facilmente?"


translate italian day5_us_4bd5340f:


    me "Beh, che c'è di male…?{w} E inoltre, dato che insisti…"


translate italian day5_us_29659e99:


    "Mi ha squadrato con attenzione per alcuni secondi, e poi ha sbottato:"


translate italian day5_us_7601cc90:


    us "Eccellente!{w} Allora ci servono delle lenzuola! Dobbiamo sembrare dei veri fantasmi, giusto?"


translate italian day5_us_907b6d20:


    me "Probabilmente…"


translate italian day5_us_03325ca7:


    us "Allora vai a prenderle!"


translate italian day5_us_3d4a0aa6:


    "Ha annunciato imperiosamente."


translate italian day5_us_310cd775:


    me "E dove credi che le possa trovare?"


translate italian day5_us_a2c60a47:


    us "Nella tua stanza, è ovvio!"


translate italian day5_us_cdd74e25:


    me "Non è così ovvio per me…"


translate italian day5_us_acbf1354:


    us "Che c'è, ci hai ripensato?"


translate italian day5_us_044b5473:


    "La sua espressione si è incupita all'istante."


translate italian day5_us_69eeffd7:


    me "Va bene, va bene!"


translate italian day5_us_48beb79c:


    "Mi sono ricordato che c'erano delle lenzuola di ricambio nell'armadio di Olga Dmitrievna, quindi forse non c'era niente di cui preoccuparsi…"


translate italian day5_us_a4951fb8:


    "Quando siamo arrivati alla casetta della leader del Campo, ho detto a Ulyana:"


translate italian day5_us_153ceca2:


    me "Aspetta qui."


translate italian day5_us_34c369b7:


    us "Signor sì, signore!"


translate italian day5_us_30bcaace:


    "Ha sbottato, facendomi il saluto militare."


translate italian day5_us_d9777aab:


    "Sembrava che Ulyana si fosse immersa totalmente nel gioco."


translate italian day5_us_900dfc81:


    "Ho subito trovato un paio di lenzuola bianche pulite."


translate italian day5_us_03e5f1cc:


    th "È un peccato che dovranno sporcarsi…"


translate italian day5_us_71a21743:


    me "Ecco qui, prendi."


translate italian day5_us_5d11f8d4:


    "Le ho consegnato un lenzuolo."


translate italian day5_us_55a5bd1b:


    us "È troppo grande per me."


translate italian day5_us_2a2dcaaf:


    "Mi ha detto, dopo averlo fatto roteare tra le mani."


translate italian day5_us_243ba3e8:


    th "Non mi stupisce, data la sua statura."


translate italian day5_us_99c46342:


    me "Da' qua."


translate italian day5_us_3fae3b47:


    "Ho piegato il lenzuolo a metà e gliel'ho ridato."


translate italian day5_us_e2dcfcce:


    us "Adesso è molto meglio.{w} Seguimi!"


translate italian day5_us_ef43e763:


    "Si è messa il panno sopra la testa ed è corsa nella foresta."


translate italian day5_us_9e8ba2ed:


    me "Aspetta!"


translate italian day5_us_69531d20:


    "Mi sono gettato dietro a lei."


translate italian day5_us_880b3622:


    "Dopo pochi minuti eravamo già vicino al campeggio dei pionieri, nascosti dietro agli alberi."


translate italian day5_us_4e62ae4d:


    "Ora era chiaro che lo scherzo, che sembrava così innocuo in un primo momento, stava prendendo una brutta piega."


translate italian day5_us_4f51f0fe:


    "Per qualche motivo ero sicuro fin dall'inizio che l'escursione sarebbe finita prima che Ulyana potesse fare qualsiasi cosa, e ora invece ci trovavamo a una decina di metri di distanza dai pionieri, vestiti da fantasmi. Non sembravamo affatto spaventosi, anzi, eravamo proprio ridicoli."


translate italian day5_us_8f4b438d:


    us "Preparati! Al mio comando!"


translate italian day5_us_451e8da8:


    me "Aspetta, aspetta.{w} In realtà, stavo scherzando quando ho accettato di fare tutto questo…{w} Pensaci bene! Non ne verrà fuori niente di buono! Dovrai stare in punizione per il resto della sessione!"


translate italian day5_us_16b3df32:


    me "E lo sarò anch'io…"


translate italian day5_us_f9d44a76:


    us "Niente ritirata, non ci arrendiamo!{w} Sei pronto? Conto fino a tre!"


translate italian day5_us_0f768398:


    "Tutti i possibili esiti di quella situazione hanno iniziato a scorrere nella mia testa febbrilmente.{w} E non ce n'erano molti."


translate italian day5_us_3f1c9f2d:


    "Prima possibilità. Io e Ulyana saltiamo fuori da dietro gli alberi, tra le risate generali dei pionieri, e la cosa si conclude con una bella lavata di capo da parte della leader del Campo, o forse anche peggio."


translate italian day5_us_31114dd5:


    "E Ulyana verrà condannata con la massima pena possibile, ai sensi della legge del Campo di pionieri."


translate italian day5_us_3b066783:


    "Secondo caso. Me ne resto qui nascosto, mentre Ulyana corre intorno alla radura coperta dal lenzuolo."


translate italian day5_us_6d29669b:


    "Lei verrà condannata in maniera esemplare, come nella prima opzione, mentre io rimarrò in relativa sicurezza."


translate italian day5_us_30ea56d4:


    "Terza opzione. Farò ogni cosa possibile e immaginabile per impedirle di commettere questo atto di vandalismo morale, e nessuno ne subirà le conseguenze, se non la sua autostima."


translate italian day5_us_52605bb2:


    "Tutta la mia analisi stava andando così bene, ma sia che quei pensieri fossero durati più di tre secondi, sia che Ulyana non avesse contato fino a tre, alla fine non sono riuscito a salvare la situazione, e l'ho vista balzare nella radura, emettendo urla disumane."


translate italian day5_us_d63ede28:


    "Come previsto, tutti i pionieri hanno cominciato a ridere a crepapelle, qualcuno è persino caduto dal ceppo sul quale stava seduto, rotolandosi a terra dalle risate."


translate italian day5_us_733c66fc:


    "Ho cercato di salvare il salvabile, gridando a Ulyana il più forte possibile, ma comunque in modo che i pionieri non mi sentissero:"


translate italian day5_us_f0a46cde:


    me "Pazza! Smettila! Torna qui!"


translate italian day5_us_37e33704:


    "Non so se fossi riuscito a persuaderla, o se si fosse accorta da sola che la sua performance era fallita miseramente, ma l'ho vista correre verso di me in tutta fretta e, senza fermarsi, si è nascosta nella foresta."


translate italian day5_us_95318591:


    "Non ho esitato a seguirla."


translate italian day5_us_d75de63e:


    "Dopo un finale del genere, c'era una scarsa probabilità che lei non sarebbe stata punita nuovamente."


translate italian day5_us_a78b707a:


    "Ho fatto bene a non partecipare a un tale atto tragicomico."


translate italian day5_us_97b3825e:


    th "Adesso devo trovare Ulyana."


translate italian day5_us_56432bce:


    "Non è stato difficile, dato che non era riuscita ad andare molto lontano."


translate italian day5_us_db6a8922:


    "Ulyana stava seduta su un tronco d'albero...{w} piangendo."


translate italian day5_us_70edb764:


    "Sono rimasto in piedi, titubante."


translate italian day5_us_73c62008:


    "Ovviamente un tale esito era prevedibile, ma non avevo la minima idea di cosa fare o di come potessi consolarla."


translate italian day5_us_a3275ce4:


    "In più, ero tornato al Campo intenzionalmente, e avevo accettato di partecipare a quel teatrino…"


translate italian day5_us_f8f658a7:


    "Ma è finita proprio male."


translate italian day5_us_0e2f3120:


    "E inoltre ero stanco, stanco morto."


translate italian day5_us_311d70c7:


    "In quel momento avrei voluto che tutto cessasse di esistere.{w} Volevo solo chiudere gli occhi e ritrovarmi altrove."


translate italian day5_us_ce999c85:


    "Preferibilmente in un luogo tranquillo e silenzioso."


translate italian day5_us_67119f08:


    "Ma la vista della singhiozzante Ulyana mi ha costretto ad agire."


translate italian day5_us_c414404b:


    "Mi sono avvicinato a lei e mi sono seduto per terra."


translate italian day5_us_82d52896:


    me "Beh, che ti aspettavi comunque...?"


translate italian day5_us_262b65eb:


    "Ho esordito filosoficamente."


translate italian day5_us_8d394e00:


    me "Era sicuro che sarebbe finita in questo modo."


translate italian day5_us_1b26a454:


    us "Sei tu il colpevole di tutto! Tu!"


translate italian day5_us_5f31ce4d:


    "Ha gridato, in lacrime."


translate italian day5_us_a508b663:


    me "Cosa sarebbe cambiato se mi fossi buttato con te laggiù?{w} Avrebbero riso di entrambi, nient'altro."


translate italian day5_us_177d555a:


    us "Ti comporti sempre così! Sempre!"


translate italian day5_us_df7cba70:


    "Il suo pianto stava diventando sempre più forte, ed improvvisamente si è precipitata verso di me, iniziando a battere i pugni sul mio petto.{w} I colpi non erano forti."


translate italian day5_us_34d398f8:


    "Sembrava più un tentativo di liberarsi dalla disperazione che la stava affliggendo, piuttosto che un desiderio di picchiarmi."


translate italian day5_us_d2af7ffb:


    me "Calmati!"


translate italian day5_us_a0e82e49:


    "Ho detto con fermezza."


translate italian day5_us_5f0944f0:


    "Ha smesso di piangere per un secondo, e poi mi ha abbracciato."


translate italian day5_us_a8064fda:


    us "Forse non sarebbe cambiato niente, ma mi sarei sentita meglio se fossimo andati insieme."


translate italian day5_us_6fd205af:


    "Non sapevo cosa dirle, e quindi mi sono limitato ad accarezzarle la testa."


translate italian day5_us_2e4814b2:


    us "Possiamo restare così ancora per un po'?"


translate italian day5_us_10409023:


    me "Sì."


translate italian day5_us_d624d515:


    "In quel momento lei non sembrava più un pericoloso reattore nucleare esplosivo sotto forma di ragazzina, ma piuttosto una sorellina che aveva incasinato le cose."


translate italian day5_us_7cb9453a:


    "Non ero per niente arrabbiato con lei, anzi, sembrava che stessi iniziando a provare dispiacere anch'io per il fallimento di quell'impresa."


translate italian day5_us_42868526:


    me "Va tutto bene, va tutto bene…{w} La prossima volta li spaventeremo come si deve!"


translate italian day5_us_c463eeea:


    us "Già…"


translate italian day5_us_a38f6822:


    "Non so dire per quanto tempo ancora fossimo rimasti seduti in silenzio."


translate italian day5_us_3dcc62a7:


    "Ulyana aveva smesso di piangere, e io non volevo disturbarla, dato che si era appena calmata."


translate italian day5_us_8f418ac5:


    "Ma non avevo nemmeno intenzione di rimanere nella foresta durante la notte."


translate italian day5_us_2bb4e019:


    me "Ehi, torniamo al Campo."


translate italian day5_us_4d1b85a8:


    "Le ho scosso la spalla con delicatezza, ma non ho ottenuto alcuna risposta."


translate italian day5_us_17489859:


    "Ulyana si era addormentata."


translate italian day5_us_95efcfe1:


    me "Ehi!"


translate italian day5_us_17edece2:


    "L'ho scossa più forte.{w} Nessuna reazione."


translate italian day5_us_1161aa63:


    "In quel momento avevo voglia di piangere."


translate italian day5_us_c56aa18d:


    th "Perché deve succedere sempre a me? Perché devo immischiarmi in queste situazioni paradossali, sempre e ovunque?"


translate italian day5_us_f58b1a89:


    th "Anche se sono apparso all'improvviso in uno strano campo di pionieri in mezzo al nulla, non sembro essere la cavia di un esperimento o la vittima di una cosmica mente malata, né sto partecipando ad una guerra intergalattica al fianco di un gruppo di pacifisti con tendenze suicide, come il tipico eroe delle storie di fantascienza."


translate italian day5_us_e29a567a:


    th "No, invece sono costretto a passare la notte in una foresta, con una ragazzina tra le mie braccia avvolta in un lenzuolo."


translate italian day5_us_5c588563:


    th "La prossima volta opterò per gli esperimenti mostruosi…"


translate italian day5_us_4cc5c895:


    "Mi sono alzato in piedi e ho caricato la dormiente Ulyana sulle mie spalle."


translate italian day5_us_8e5b9b1b:


    "Forse c'era un modo per svegliarla, ma per prima cosa non volevo proprio, e seconda cosa, un fardello in più o in meno – a quel punto non avrebbe fatto alcuna differenza."


translate italian day5_us_3521c810:


    "Per fortuna che non era molto pesante…"


translate italian day5_us_c048facc:


    "Arrivati alla piazza mi sono fermato, ho appoggiato Ulyana sopra a una panchina e sono crollato accanto a lei, completamente esausto."


translate italian day5_us_66f6667a:


    "Persino una piccola ragazzina diventa difficile da trasportare, dopo un po' di tempo."


translate italian day5_us_99250f31:


    "Le stelle splendevano luminose nel cielo."


translate italian day5_us_d9e32f7f:


    th "Forse in questo momento la loro luce non illumina solo me e questo Campo, ma anche la città dove sono nato, dove si trova la mia vecchia casa…"


translate italian day5_us_1b24971b:


    "Era come se avessi un dolore costante al petto."


translate italian day5_us_3ef36634:


    "Ho immaginato il mio vecchio appartamento in tutti i suoi dettagli, e un detestabile bruciore ha cominciato a farsi strada tra il mio stomaco e la mia gola."


translate italian day5_us_86074a46:


    "No, non si trattava di malinconia.{w} Era più come un triste ricordo."


translate italian day5_us_30f40a77:


    "Perché nonostante tutto quello che è successo, mi sono sentito più vivo qui in questi cinque giorni che là negli ultimi anni."


translate italian day5_us_d5ded2d1:


    "E non ero più sicuro di voler tornare indietro."


translate italian day5_us_f0e63108:


    "Solo una domanda continuava ancora a tormentarmi – come e perché io fossi finito qui.{w} È divampata nuovamente nel mio cervello."


translate italian day5_us_e3fa1442:


    "Ultimamente non ho dedicato molto tempo a cercare risposte, o anche solo a pensare alla mia situazione."


translate italian day5_us_cba9268c:


    "I miei pensieri erano occupati con le banali faccende di tutti i giorni."


translate italian day5_us_2e35c234:


    "E ora, al fine di riuscire a distaccarmi ed essere in grado di rimanere qui per sempre, dovevo riuscire a capire la natura di questo luogo."


translate italian day5_us_6534fa35:


    "È solo che persino un usignolo in una gabbia d'oro ha il diritto di sapere perché e per mano di chi esso sia finito lì dentro.{w} E solo dopo decidere se restare o meno…"


translate italian day5_us_849ef2d5:


    "Non so per quanto tempo ancora sarei rimasto immerso nelle mie domande esistenziali, ma il forte russare di Ulyana mi ha riportato alla realtà."


translate italian day5_us_d9d3457b:


    me "Così piccola, eppure russa così forte…"


translate italian day5_us_c4195e16:


    "Ho sospirato, l'ho caricata nuovamente sulle spalle e mi sono diretto verso la sua casetta."


translate italian day5_us_a20cefa7:


    "..."


translate italian day5_us_12b2a39f:


    "Non avevo voglia di spiegare tutto ad Alisa, così mi sono limitato ad appoggiare sulla veranda la dormiente Ulyana, ho bussato alla porta e me ne sono andato rapidamente."


translate italian day5_us_cf46738c:


    "Sono tornato alla casetta di Olga Dmitrievna con sentimenti contrastanti."


translate italian day5_us_1531a117:


    "Da un lato, avevo fatto quello che volevo fare – ero riuscito a confortare e divertire Ulyana."


translate italian day5_us_6aec4a07:


    "Ma dall'altro…"


translate italian day5_us_d05290b8:


    "Le due lenzuola sgualcite che tenevo tra le mani sembravano più i resti di una qualche logora camicia di forza."


translate italian day5_us_bb449b79:


    "Ho aperto lievemente la porta e sono entrato."


translate italian day5_us_105fc865:


    mt "Semyon!"


translate italian day5_us_b3e320ef:


    "Olga Dmitrievna era seduta alla scrivania, e sembrava che mi stesse aspettando."


translate italian day5_us_79427e67:


    mt "Ti spiace spiegarmi cosa è successo?"


translate italian day5_us_377d0453:


    me "Ecco…{w} Non la sgridi di nuovo…{w} È stata colpa mia!"


translate italian day5_us_47603f81:


    "A quanto pare ero diventato un eroe, con mia grande sorpresa."


translate italian day5_us_3ed3fd1f:


    "La bocca ha agito prima dei miei pensieri."


translate italian day5_us_8c77f545:


    "Forse ho tirato fuori alcuni dei miei pregi migliori, di cui non ero nemmeno a conoscenza."


translate italian day5_us_16f0739b:


    "L'umanità in contrasto con il buonsenso."


translate italian day5_us_4354ab86:


    mt "Davvero?"


translate italian day5_us_cce98559:


    "Avendo ormai scelto quella via, ho deciso di andare fino in fondo."


translate italian day5_us_0b7ff73e:


    me "Beh, ho preso io le lenzuola. E stavo nascosto dietro a un albero quando è successo."


translate italian day5_us_cc347b04:


    mt "Di che albero stai parlando?"


translate italian day5_us_0193dcd6:


    "La leader del Campo mi ha guardato con meraviglia."


translate italian day5_us_ee5de7cc:


    mt "A cosa ti servivano le lenzuola?"


translate italian day5_us_220e2848:


    "Mi sono reso conto del mio errore."


translate italian day5_us_bc2d73a5:


    me "Quindi, non stava parlando della cosa nella foresta?"


translate italian day5_us_5afe237b:


    mt "Semyon, non ti capisco."


translate italian day5_us_13ed3d89:


    mt "Volevo sapere perché eri scomparso così misteriosamente durante l'escursione, ma adesso sono piuttosto interessata ad ascoltare anche questa tua storia delle lenzuola."


translate italian day5_us_87f8ffa1:


    th "Non è possibile che lei e tutti gli altri non abbiano visto lo spettacolino di Ulyana!"


translate italian day5_us_e65fa514:


    th "Non posso essermi immaginato tutti quei pionieri che ridevano."


translate italian day5_us_f41a46e2:


    me "Olga Dmitrievna, sono serio…{w} Non ha proprio visto nessuno coperto da un lenzuolo balzare davanti a lei?"


translate italian day5_us_de5231d6:


    mt "E così, sei stato tu?"


translate italian day5_us_8fb93608:


    "Mi ha guardato da vicino."


translate italian day5_us_c20bb56d:


    me "No, non sono stato io…"


translate italian day5_us_85c3d025:


    th "Non può arrivarci da sola, valutando la mia statura?"


translate italian day5_us_32446a1f:


    mt "Ma hai in mano le lenzuola…"


translate italian day5_us_a73acf7d:


    me "Sì…"


translate italian day5_us_d874bb71:


    "Non riuscivo a capire se mi stesse prendendo in giro, o se davvero non ne sapesse niente."


translate italian day5_us_3a23059a:


    me "Olga Dmitrievna, facciamo finta che io non abbia detto niente.{w} Sono troppo stanco adesso."


translate italian day5_us_d034fa83:


    mt "D'accordo, vai a dormire."


translate italian day5_us_24edbca1:


    "Con mia grande sorpresa non ha voluto sapere altro."


translate italian day5_us_db925e9e:


    "Certamente ero stupito da una tale reazione, ma ho deciso di approfittarne. Mi sono avvolto tra le coperte e mi sono girato verso la parete."


translate italian day5_us_4c4a4071:


    "Ma non riuscivo a dormire."


translate italian day5_us_8d27541d:


    "Non stavo pensando a niente in particolare, avevo mal di testa, ma non riuscivo ad addormentarmi."


translate italian day5_us_9db6bada:


    "Mi sono girato dall'altra parte, e i ricordi di questa giornata hanno iniziato a scorrere davanti ai miei occhi."


translate italian day5_us_e7fb5b3f:


    "Ho provato a chiudere gli occhi per scacciarli, ma non ha funzionato."


translate italian day5_us_e227a4a1:


    "All'improvviso ho sentito qualcuno bussare alla finestra."


translate italian day5_us_f9ca50cb:


    "Olga Dmitrievna sembrava stesse dormendo."


translate italian day5_us_ce13ac06:


    "Mi sono vestito e sono uscito."


translate italian day5_us_a35885f7:


    "Ulyana era davanti a me, con un sorriso astuto."


translate italian day5_us_92e87dbb:


    us "Come hai fatto a portarmi fino alla casetta…?{w} Non è stato difficile?"


translate italian day5_us_f0de0297:


    me "Non proprio…{w} Perché sei venuta?"


translate italian day5_us_047a7a48:


    "Non riuscivo a dormire, ma il letto sembrava essere comunque l'unico posto dove avrei potuto risiedere senza soffrire, ecco perché l'inaspettata visita di Ulyana non mi è piaciuta affatto."


translate italian day5_us_c198f325:


    us "Com'è andata con lei? Ti ha sgridato?"


translate italian day5_us_be6d9222:


    me "No, sono riuscito ad evitarlo in qualche modo…"


translate italian day5_us_4275b6c0:


    us "Bene allora! Io sono sempre fortunata!"


translate italian day5_us_d0119426:


    me "Ma certo..."


translate italian day5_us_645ce19d:


    "I miei occhi hanno cominciato a chiudersi da soli.{w} Sembrava che il sonno stesse prendendo finalmente il sopravvento su di me."


translate italian day5_us_cdcc908e:


    me "Senti, sono molto stanco…"


translate italian day5_us_0f78845d:


    us "Non ci vorrà molto.{w} Chiudi gli occhi."


translate italian day5_us_e0f3d01c:


    "Era la cosa più facile che mi si potesse chiedere, e non mi interessava nemmeno sapere a cosa le sarebbe servito – forse così mi avrebbe lasciato in pace prima."


translate italian day5_us_5cbb3956:


    "Ho chiuso gli occhi."


translate italian day5_us_2bd7350e:


    "E in quell'istante ho sentito un lieve bacio."


translate italian day5_us_5d1fc15b:


    "I miei occhi si sono aperti di loro spontanea volontà, ma Ulyana stava già correndo via, salutandomi con la mano."


translate italian day5_us_1d1a05f7:


    "Sono rimasto immobile, intorpidito, senza riuscite a gridarle niente, mentre si allontanava."


translate italian day5_us_74510c40:


    "Non saprei dire per quanto tempo io fossi rimasto così, ma alla fine il freddo notturno ha cancellato il mio torpore."


translate italian day5_us_78b47b21:


    "Ho rabbrividito e sono tornato al mio letto."


translate italian day5_us_eff30d1d:


    "Adesso non volevo più addormentarmi, volevo invece pensare a quello che era appena accaduto, ma i miei occhi, che hanno iniziato a chiudersi da soli già quando ero davanti a Ulyana, sembravano impartire un comando al resto del corpo, e mi sono addormentato ancora prima di accorgermene…"


translate italian day5_un_b3a56e2b:


    "Solo Lena e Alisa spiccavano tra tutto quello splendore."


translate italian day5_un_9494f7ea:


    "Beh, ovviamente era piuttosto normale per Alisa litigare in quel modo con qualcuno."


translate italian day5_un_49a0c8bc:


    "Ma sentire Lena che alzava la voce…"


translate italian day5_un_9844ee70:


    "Mi sono avvicinato silenziosamente, per cercare di scoprire cosa stesse accadendo."


translate italian day5_un_6ca87438:


    dv "No, adesso ascoltami bene!"


translate italian day5_un_8c9cf7c5:


    un "Non ho intenzione di ascoltare un bel niente!"


translate italian day5_un_8eaadb05:


    dv "Ah davvero? Bene allora!"


translate italian day5_un_4e3a9f81:


    "Alisa si è voltata e i nostri sguardi si sono incrociati."


translate italian day5_un_0961f894:


    "All'inizio era ovviamente confusa riguardo a ciò che stava accadendo, ma poi…"


translate italian day5_un_7f5542bc:


    dv "E così, stavi origliando!"


translate italian day5_un_12887db1:


    me "Cosa? Io? No!"


translate italian day5_un_712bd43f:


    "È avanzata lentamente verso di me, mentre io ho indietreggiato."


translate italian day5_un_ee80f2e5:


    "Lena invece è rimasta dov'era."


translate italian day5_un_607f19ad:


    dv "Va bene, ascolta pure!"


translate italian day5_un_c992f6a8:


    "Alisa si è fermata a metà strada e si è girata verso Lena."


translate italian day5_un_6aa66cea:


    dv "A proposito, lo sapevi che oggi mi stava spiando di nascosto?"


translate italian day5_un_3875ba7b:


    un "Che cosa?"


translate italian day5_un_91748180:


    dv "Stava sbirciando. E ha visto {i}proprio tutto{/i}!"


translate italian day5_un_f328f2ed:


    un "Ma è… è vero?"


translate italian day5_un_52e59e20:


    "Lena si torceva le mani ansiosamente, mi ha lanciato un'occhiata rapida e ha iniziato a fissare il terreno, come se fosse stata tagliata fuori dal mondo."


translate italian day5_un_127f1428:


    me "No, no, non è successo niente del genere!"


translate italian day5_un_806ed8e6:


    "Dopo aver lanciato uno sguardo di rimprovero verso Alisa, mi sono avvicinato a Lena."


translate italian day5_un_32dec18e:


    dv "Perché lo stai negando? Ho anche un testimone!"


translate italian day5_un_246d6902:


    me "La tua testimone è ancora peggio di te!"


translate italian day5_un_05eee1a3:


    dv "Quindi, sei proprio convinto che non sia successo niente?"


translate italian day5_un_5bf1c5fc:


    "Ho pensato per un momento."


translate italian day5_un_756d591a:


    th "Perché ho la patologica necessità di giustificarmi dinanzi a Lena?{w} Come se fosse una cosa di vitale importanza, come se fosse necessario che la sua opinione nei miei confronti resti invariata."


translate italian day5_un_9cc4691d:


    th "E comunque, come faccio a sapere cosa sta pensando?"


translate italian day5_un_cd4b1ccb:


    me "No! Non è successo!"


translate italian day5_un_b7eb3fd9:


    "Lena ha lanciato uno sguardo minaccioso verso Alisa e subito dopo mi ha guardato, con gli occhi che esprimevano disagio e speranza."


translate italian day5_un_71c04b4b:


    "Quel suo sguardo era per me insopportabile."


translate italian day5_un_0d6ab95e:


    me "Non è successo…"


translate italian day5_un_cacc8d60:


    "Ho ripetuto, in tono meno sicuro."


translate italian day5_un_ba1e125e:


    dv "Sì, come no."


translate italian day5_un_e36048c5:


    "Ha sogghignato Alisa dietro di me."


translate italian day5_un_c6c151b9:


    dv "Sta a te decidere a chi credere."


translate italian day5_un_97aaae01:


    "Ha detto a Lena con disinvoltura, poi si è voltata e si è diretta verso il falò."


translate italian day5_un_50793df0:


    un "È vero quello che ha detto?"


translate italian day5_un_56ad8efd:


    "Mi sono sentito a disagio, come un pesce fuor d'acqua, sentivo il bisogno di dover porre fine a quella faccenda il prima possibile."


translate italian day5_un_ba176c8b:


    th "Perché mai dovrei giustificarmi dinanzi a lei?"


translate italian day5_un_5156bbe7:


    me "Beh, e se invece fosse accaduto davvero?"


translate italian day5_un_5dc693b2:


    "Ho guardato Lena con fermezza, ma l'unica cosa che ho notato è stato un bagliore luminoso del tramonto, riflesso in una lacrima che le stava scendendo lungo la guancia."


translate italian day5_un_e6d47aad:


    me "No, intendevo dire…"


translate italian day5_un_f203e664:


    un "Non c'è bisogno di mentire."


translate italian day5_un_0c5b6d1e:


    "Si è asciugata le lacrime e ha cercato di sorridere."


translate italian day5_un_2060fc9e:


    un "Dopotutto, non sono affari miei."


translate italian day5_un_f32b3a36:


    me "No, perché dici così…"


translate italian day5_un_f3c734aa:


    th "Perché non dovrebbe dirlo?"


translate italian day5_un_35b0d3f6:


    me "È solo che… Ecco… È stato un incidente, capisci?{w} Uno stupido malinteso causato da Ulyana, niente più."


translate italian day5_un_6e34d368:


    un "Sì, certo…"


translate italian day5_un_85bc2437:


    me "È la verità!"


translate italian day5_un_e71ea824:


    un "Ti credo."


translate italian day5_un_89436b45:


    "Lena stava dicendo tutto senza alcuna emozione, come se non le importasse affatto di quello che stava succedendo."


translate italian day5_un_76748f71:


    "Ero quasi in grado di crederle, ma quelle sue lacrime pochi secondi fa…"


translate italian day5_un_14dd805c:


    me "A me sembra di no."


translate italian day5_un_5c9d8120:


    un "Cos'è questo, una specie di gioco?"


translate italian day5_un_29ff2cb1:


    "Ha chiesto con calma, ma ho percepito una nota di rabbia nella sua voce."


translate italian day5_un_0a753ae6:


    un "Hai davvero bisogno di convincermi di questo? E anche se ti credessi, allora cosa? Diventerebbe tutto vero?"


translate italian day5_un_ce5015a9:


    me "Non sto cercando di convincerti, voglio solo farti capire che non è stata colpa mia… e che non volevo…"


translate italian day5_un_ffacfd31:


    un "E perché mai dovrebbe importarmene?!"


translate italian day5_un_f78c17ba:


    "Ha gridato Lena."


translate italian day5_un_a7f39912:


    "Stavo dando le spalle al falò, ma ero sicuro che tutti i pionieri si fossero voltati verso di noi."


translate italian day5_un_fd952043:


    un "Se proprio vuoi spiare qualcuno – fallo e basta! Fai quello che vuoi! Perché devi importunarmi?"


translate italian day5_un_f8882e89:


    me "Io non…"


translate italian day5_un_427acce6:


    "Ma in effetti, sotto tutti i punti di vista, sembrava proprio così – stavo cercando di scusarmi con Lena senza successo.{w} Come un bambino colpevole, o forse anche come un marito infedele…"


translate italian day5_un_b24ff123:


    un "Giusto per fartelo capire! Non m'importa!"


translate italian day5_un_5cd48f53:


    "Lena si è voltata e si è diretta rapidamente verso il Campo."


translate italian day5_un_6f3fa70b:


    "Non ho provato a fermarla – in quel momento non sarebbe stata di certo la cosa migliore da fare."


translate italian day5_un_5307c03e:


    "Era fuori di sé, e nessun tentativo di convincerla o di farla ragionare avrebbe funzionato."


translate italian day5_un_c03bbe86:


    th "Da quando si comporta così?{w} Gridando in quel modo, perdendo le staffe...?"


translate italian day5_un_37a8623c:


    "Con quei pensieri in testa, sono tornato al falò e mi sono seduto su un ceppo."


translate italian day5_un_ce617998:


    "…"


translate italian day5_un_dbeba143:


    dv "Allora?"


translate italian day5_un_a07446cb:


    "Ha chiesto Alisa, dopo essersi seduta accanto a me."


translate italian day5_un_ad02f53a:


    me "Allora cosa?"


translate italian day5_un_1a47cd1e:


    dv "Non è andata molto bene, eh?"


translate italian day5_un_2abbfc44:


    me "Come puoi vedere…"


translate italian day5_un_3792f79f:


    "Ho ringhiato, mentre frugavo tra le braci con un bastone."


translate italian day5_un_e73a96b8:


    "Si è fatto completamente buio, e la radura si è immersa nell'oscurità."


translate italian day5_un_87200809:


    "Il paesaggio circostante sembrava l'illustrazione di un libro per bambini, riguardante un mostro di legno – una sconosciuta creatura della foresta stava per apparire da dietro un albero, i gufi predatori urlavano minacciosamente fra i rami, e persino i topi che si affacciavano dalle loro tane ti guardavano con diffidenza."


translate italian day5_un_3c849f9b:


    "Ma non avevo alcuna paura in quel momento. Tutto era diverso dalla notte precedente, quando ero imprigionato nel vecchio campo…"


translate italian day5_un_d74fcdd1:


    "La luna splendeva, illuminando con cura la nostra truppa coraggiosa. Il mondo stava dormendo immerso nel chiaro di luna, in attesa del mattino."


translate italian day5_un_e9c4c23a:


    dv "Come pensavo. Anche se era prevedibile."


translate italian day5_un_fda262d8:


    me "Ma perché…?"


translate italian day5_un_aef6d994:


    "Finalmente sono riuscito a raccogliere il pezzo di legno più grande e l'ho gettato sopra al fuoco, facendo salire le fiamme a circa un metro di altezza, con un vortice di scintille che si sono disperse in ogni direzione."


translate italian day5_un_77bf3519:


    me "Lena si comporta spesso così?"


translate italian day5_un_185b84dd:


    dv "Così come?"


translate italian day5_un_56fc9a67:


    me "Gridandoti in faccia.{w} Non immaginavo potesse comportarsi in quel modo."


translate italian day5_un_989f2bee:


    dv "Lei è umana, come chiunque altro."


translate italian day5_un_93c166ce:


    "Ha ridacchiato Alisa."


translate italian day5_un_32fca04b:


    dv "Come se tu non l'avessi notato già prima."


translate italian day5_un_0f99a0a2:


    me "Notato cosa?"


translate italian day5_un_5e9fa3e4:


    dv "Lei."


translate italian day5_un_e54b39e9:


    me "In che senso?"


translate italian day5_un_b08ce089:


    dv "Voglio dire, quello è il suo vero volto."


translate italian day5_un_ec069979:


    me "Cosa intendi dire con «il suo vero volto»?"


translate italian day5_un_2e4631df:


    dv "Oh, sei proprio stupido!"


translate italian day5_un_34dc294a:


    me "Finisci ciò che hai iniziato."


translate italian day5_un_44462a0c:


    dv "Lei non è quello che sembra, non è come credevi che fosse."


translate italian day5_un_95364e9c:


    me "E allora com'è?"


translate italian day5_un_6fbef0db:


    dv "Ok, sono stufa di queste cose…"


translate italian day5_un_3f845aea:


    "Alisa si è alzata in piedi, con l'intenzione di andarsene."


translate italian day5_un_bd114f34:


    "Non ho detto niente, mi sono limitato a restare seduto e ad ascoltare il silenzio della foresta."


translate italian day5_un_1c95b08d:


    dv "A proposito, se vuoi raggiungerla, penso che sia andata all'isola."


translate italian day5_un_278fc1b6:


    me "Quale isola?"


translate italian day5_un_5f02cbe1:


    dv "Quella dove siete andati oggi a raccogliere le fragole."


translate italian day5_un_b91fd3d7:


    me "E cosa sta facendo lì?"


translate italian day5_un_d7920d68:


    dv "Ah, ma sei proprio un idiota!"


translate italian day5_un_42e0fed6:


    "Alisa ha battuto il piede sul terreno, ha fatto il giro attorno al falò e si è seduta dalla parte opposta."


translate italian day5_un_ff34e775:


    "Sembrava proprio che io non avessi capito molto di tutto questo."


translate italian day5_un_6615be77:


    "La mia testa era completamente vuota."


translate italian day5_un_ff8730b2:


    "A dire il vero, era così piena e pesante che non c'era più spazio per ulteriori pensieri."


translate italian day5_un_e447cec9:


    "Volendo fare un paragone, se generalmente il mio cervello può essere immaginato come una vasta autostrada percorsa da migliaia di pensieri a tutta velocità, che si scontrano in continuazione generando incidenti caotici, adesso altro non era che una piccola stradina sperduta in una desolata foresta remota, percorsa solo in casi di emergenza."


translate italian day5_un_643db9b6:


    "Cercavo di riflettere su quello che era successo, analizzandolo in qualche modo, ma continuavo a sbattere contro un muro invisibile."


translate italian day5_un_0877776f:


    "Inoltre, non riuscivo a trovare una singola emozione nella mia anima, alcuna reazione. Come se tutto fosse accaduto a qualcun altro."


translate italian day5_un_8d9c0b6a:


    "Non sono riuscito a convincere Lena della mia innocenza.{w} Doveva forse importarmene?"


translate italian day5_un_dc1bef5c:


    "Lena ha pianto, ha urlato, e poi è corsa via.{w} E allora?"


translate italian day5_un_0845afe0:


    "Non provavo nulla a riguardo.{w} Forse avrei dovuto?"


translate italian day5_un_7c882346:


    "Mi sono morso il labbro fino a farlo sanguinare, e poi mi sono alzato."


translate italian day5_un_bbae1908:


    "Non potevo sopportare di stare qui, tra le risate di quei pionieri."


translate italian day5_un_5ba34865:


    th "Dovrei scappare – non importa dove. Nella foresta, nella miniera abbandonata, o addirittura nello spazio. Basta che sia lontano da qui."


translate italian day5_un_fbc3bc79:


    "Ho approfittato di un momento in cui la leader del Campo stava guardando altrove, e sono scomparso tra le ombre della foresta."


translate italian day5_un_a20cefa7:


    "..."


translate italian day5_un_6219e64c:


    "Era da tanto che non pioveva…"


translate italian day5_un_c6749284:


    th "Tuttavia, non sono nemmeno sicuro che la pioggia esista in questo mondo."


translate italian day5_un_5d4ba34b:


    th "Che idea stupida, ma certo che esiste. Le piante necessitano di una certa umidità."


translate italian day5_un_cb1b2ded:


    "La notte era più fresca del pomeriggio, ma l'aria non si era ancora raffreddata abbastanza. Mi sentivo un po' stordito a causa dell'afa serale."


translate italian day5_un_40724f26:


    "Improvvisamente mi è venuta voglia di farmi una nuotata."


translate italian day5_un_489cb8db:


    "Era un desiderio piuttosto normale, dato che le giornate erano calde, e ci si sentiva costantemente imbevuti di sudore durante il giorno."


translate italian day5_un_1056c0d4:


    "Non mi sono nemmeno accorto che mi stavo avvicinando al molo."


translate italian day5_un_d177ba32:


    th "Perché non la spiaggia?"


translate italian day5_un_9bb6a06d:


    "Ho pensato di nuovo a Lena. Tutto quello che ho cercato di dimenticare è tornato a galla, trascinando con sé un sacco di domande scomode, di risposte inappropriate, di strani sentimenti, e di vaghi desideri."


translate italian day5_un_afb26968:


    "Era chiaro – il mio subconscio mi aveva portato qui."


translate italian day5_un_17e84712:


    th "Ma cos'è che voglio {i}veramente{/i}?"


translate italian day5_un_4f586330:


    th "Voglio scusarmi con lei per qualcosa, convincerla di qualcosa?{w} No, è improbabile."


translate italian day5_un_eb0d22d4:


    "Volevo solo avere una qualche reazione da parte sua. Volevo solo sentirmi dire «Sì, ho capito tutto», accompagnato da un sorriso."


translate italian day5_un_304c60e6:


    "Così avrei potuto smetterla di sentirmi {i}in quel modo{/i}…"


translate italian day5_un_a78ee8f1:


    "Avevo il bisogno di essere capito da qualcuno, persino in {i}questo{/i} mondo."


translate italian day5_un_21fa3a03:


    "Ho slegato una barca, l'ho spinta in acqua e ho afferrato i remi."


translate italian day5_un_ce617998_1:


    "…"


translate italian day5_un_1607c44f:


    "Questa volta è stato più facile remare – le mie braccia mi facevano male comunque, ma sono riuscito a inventare una sorta di tecnica che mi ha permesso di viaggiare abbastanza dritto."


translate italian day5_un_134ecd94:


    "Il fiume sembrava congelato, espandendosi sotto la barca come un velo traslucido."


translate italian day5_un_cdd6fc1f:


    "Il chiaro di luna penetrava in profondità nell'acqua, si riusciva quasi a vedere il fondo."


translate italian day5_un_1b592616:


    "Ho continuato a remare."


translate italian day5_un_ce617998_2:


    "…"


translate italian day5_un_9f7443d1:


    "Sorprendentemente, l'isola adesso sembrava uguale a come l'avevo vista di giorno."


translate italian day5_un_f47c3547:


    "Solitamente {i}in questo luogo{/i} era tutto diverso. Dopo il tramonto sembrava un altro mondo, un mondo misterioso. A volte pauroso, ma comunque meraviglioso, a modo suo. Un mondo di ombre e di sussurri, un mondo notturno."


translate italian day5_un_b94320d8:


    "Ho camminato lentamente intorno all'isola, cercando la barca di Lena."


translate italian day5_un_35778f50:


    "L'erba frusciava dolcemente sotto i miei piedi, ogni tanto la riva veniva accarezzata dalle onde, che poi ritornavano indietro come delle mosche che hanno sbattuto contro un vetro."


translate italian day5_un_575aa0ca:


    "La brezza dall'acqua agitava pigramente le foglie degli alberi, forse più per abitudine che per il desiderio di dar voce al boschetto notturno."


translate italian day5_un_0bba232e:


    "Stavo ammirando tutto quel meraviglioso dipinto con così tanta attenzione, che non mi sono accorto di essermi imbattuto in qualcosa."


translate italian day5_un_44e820c3:


    "Era una barca."


translate italian day5_un_0c66a774:


    th "Beh, non c'è da stupirsi – di sicuro non è venuta qui a nuoto."


translate italian day5_un_c486b958:


    "Mi sono diretto verso il centro dell'isola."


translate italian day5_un_ce617998_3:


    "…"


translate italian day5_un_b2eec26c:


    "Dopo un centinaio di metri ho sentito un fruscio provenire da dietro un albero."


translate italian day5_un_4dcf727f:


    un "Non ti avvicinare…"


translate italian day5_un_5f75988c:


    "Ha sussurrato Lena."


translate italian day5_un_51149e68:


    me "…"


translate italian day5_un_70edb764:


    "Ho esitato."


translate italian day5_un_0ca1cc9c:


    un "Non ti avvicinare."


translate italian day5_un_78558274:


    "Ha ripetuto più forte."


translate italian day5_un_3efb73c2:


    me "Come fai a sapere che sono io?"


translate italian day5_un_edfa793d:


    "Anche se in realtà nulla suggeriva che l'avesse pensato."


translate italian day5_un_65bf82fc:


    me "D'accordo…"


translate italian day5_un_f4fd5ba0:


    "Mi sono appoggiato contro l'albero, cercando di non guardare dietro ad esso."


translate italian day5_un_ad697c77:


    me "Alisa mi ha detto che saresti venuta qui."


translate italian day5_un_ca19d4ba:


    un "E allora?"


translate italian day5_un_801934ec:


    "Era davvero difficile sapere quali emozioni la stavano dominando."


translate italian day5_un_d382dd22:


    "La sua voce era abbastanza ferma, anche se mi è sembrato di sentire una nota di irritazione e di fastidio. Non riuscivo a capire se fosse arrabbiata, o se fosse indifferente."


translate italian day5_un_13bf83a1:


    me "È stato proprio imbarazzante."


translate italian day5_un_7a2f384f:


    "Stavo facendo del mio meglio per evitare scuse e giustificazioni inutili, ma non riuscivo proprio a trovare altre parole."


translate italian day5_un_e5bf1202:


    un "Sei venuto qui solo per dirmi questo?"


translate italian day5_un_e7f8d1d4:


    me "No… Beh, non lo so."


translate italian day5_un_3c94e6e3:


    un "Non lo sai, ma sei venuto comunque?"


translate italian day5_un_fc30fd2b:


    me "Sì."


translate italian day5_un_334f3921:


    un "Non avresti dovuto."


translate italian day5_un_839ef62e:


    me "Perché? Naturalmente, se ti sto dando fastidio…"


translate italian day5_un_c9d68cca:


    un "Perché mi stai seguendo?"


translate italian day5_un_48199bb7:


    me "Io…"


translate italian day5_un_4a3d8c67:


    "Aveva ragione – certamente doveva sembrare così. Inoltre, mi sentivo decisamente attratto da lei."


translate italian day5_un_84f2a0fc:


    me "Non dovresti pensare così."


translate italian day5_un_1adc8b36:


    un "E come dovrei pensare?"


translate italian day5_un_595d7b8c:


    me "Non lo so."


translate italian day5_un_8d18dd8b:


    un "Posso trarre conclusioni solo in base al tuo comportamento."


translate italian day5_un_9044e6ea:


    me "Davvero non lo so… Credo sia meglio che io vada."


translate italian day5_un_8477995f:


    "Ero confuso, ed era difficile anche solo stare vicino a lei."


translate italian day5_un_984ea120:


    un "Perché? Dato che sei venuto…"


translate italian day5_un_44251c8f:


    "Mi è sembrato di sentire un tono giocoso nella sua voce."


translate italian day5_un_01822020:


    me "Va bene."


translate italian day5_un_77825d23:


    un "E?"


translate italian day5_un_ad02f53a_1:


    me "Cosa?"


translate italian day5_un_e9197f7e:


    un "Che ti ha detto Alisa?"


translate italian day5_un_668b75c5:


    me "Niente di speciale."


translate italian day5_un_8bf5923b:


    un "Capisco."


translate italian day5_un_fc30fd2b_1:


    me "Già."


translate italian day5_un_4924952d:


    un "Bene allora."


translate italian day5_un_751ac511:


    me "Sì…"


translate italian day5_un_40151fe6:


    "Siamo rimasti in silenzio per un po'."


translate italian day5_un_b8d98c47:


    un "Dal momento che sei venuto, dimmi qualcosa."


translate italian day5_un_e3b565dd:


    me "Beh, non saprei…"


translate italian day5_un_8ee362ac:


    un "Ad esempio, quello che è successo stamattina."


translate italian day5_un_2ea0a340:


    me "Cosa dovrei dirti di quello che è successo stamattina?"


translate italian day5_un_5b543368:


    un "Lo sai."


translate italian day5_un_142ff09f:


    me "Tu stessa non ne volevi parlare."


translate italian day5_un_7af3f67c:


    un "Non volevo, ma ora lo voglio!"


translate italian day5_un_ea56876b:


    "La voce di Lena tremava."


translate italian day5_un_2fc43074:


    "Ormai non riuscivo più a capire cosa stesse accadendo."


translate italian day5_un_968df1e5:


    th "Forse non è lei quella dietro l'albero?"


translate italian day5_un_a8a3ea5d:


    un "Non guardare!"


translate italian day5_un_7c18f5e8:


    me "Ok, ok… Ma cosa c'è che non va?"


translate italian day5_un_d893718b:


    un "Niente. Ma non guardare."


translate italian day5_un_67742710:


    me "Va bene, come vuoi."


translate italian day5_un_c928b72a:


    un "Allora, non è successo niente?"


translate italian day5_un_a336db31:


    me "Beh, in realtà qualcosa sì… Ma è stato tutto uno stupido incidente!"


translate italian day5_un_4f1a98c1:


    un "E allora perché sei così preoccupato di cosa ne penso?"


translate italian day5_un_7cd08f32:


    me "Non sono preoccupato!"


translate italian day5_un_0594d317:


    me "E comunque, anche tu!"


translate italian day5_un_505ae84f:


    "Ho detto a voce più alta, cominciando ad irritarmi."


translate italian day5_un_acb49e13:


    un "Io cosa?"


translate italian day5_un_f2ac40dd:


    me "Perché ti importa così tanto di questa situazione?"


translate italian day5_un_3537de2e:


    un "Chi ha detto che m'importa di questa situazione?"


translate italian day5_un_2c8023fc:


    me "Allora cosa c'è?"


translate italian day5_un_62592cb0:


    un "Niente…"


translate italian day5_un_3d26df77:


    "Ha detto in un sussurro, ed è rimasta in silenzio."


translate italian day5_un_e6ab9030:


    "Quella conversazione non ci stava portando da nessuna parte."


translate italian day5_un_1826deee:


    th "Lena non dirà nulla, e io sembro essere troppo stupido per indovinare, o anche solo per sentirmi sicuro di me stesso."


translate italian day5_un_ae93dffd:


    me "Scusa, probabilmente è tutta colpa mia. Ma non riesco proprio a capire…"


translate italian day5_un_962ad609:


    un "Perché…"


translate italian day5_un_ad02f53a_2:


    me "Cosa?"


translate italian day5_un_68ca672f:


    un "Perché devi sempre scusarti? Per ogni singola cosa. Per quello che hai fatto, per quello che stai facendo, e anche per quello che non hai ancora fatto."


translate italian day5_un_a337cbd5:


    me "Ma…"


translate italian day5_un_0f57e6b5:


    "Forse aveva ragione, ma non riuscivo a comportarmi diversamente. Mi sentivo in dovere di scusarmi, con lei, con tutti."


translate italian day5_un_336e6bce:


    "Così non avrebbero pensato male di me, non avrebbero riso di me. Così avrei potuto mettere in disparte qualcosa di non detto, eliminando le incomprensioni."


translate italian day5_un_7259241d:


    un "Comunque, sono fatti tuoi!"


translate italian day5_un_339b95c7:


    "Lena si è arrabbiata."


translate italian day5_un_ca9b074e:


    un "Scuse, giustificazioni! Perché dovrebbe importarmi…?"


translate italian day5_un_607b607b:


    me "D'accordo, va bene, allora non sono colpevole! Infatti, non credo di poter essere accusato di nulla, ma allora cosa c'è che non va? Cosa c'è di sbagliato in te?"


translate italian day5_un_1e693cd1:


    un "Perché, cosa dovrebbe esserci di sbagliato in me?"


translate italian day5_un_6478a1d7:


    me "Non lo so… Io la penso così."


translate italian day5_un_6355a504:


    un "Oh! Tu la pensi così!"


translate italian day5_un_0467a4c4:


    "Si è messa a ridere."


translate italian day5_un_27b88dd3:


    un "Parli come se mi conoscessi."


translate italian day5_un_51149e68_1:


    me "…"


translate italian day5_un_cbc4bb84:


    "Non le ho risposto."


translate italian day5_un_af01240b:


    un "Per favore, vattene…"


translate italian day5_un_850cca06:


    "Mi ha detto dopo un attimo."


translate italian day5_un_a09db615:


    "Non mi sono mosso, non riuscivo, non ne avevo la forza."


translate italian day5_un_d2f6ef68:


    "A quel punto niente mi interessava più – non volevo scusarmi, giustificarmi. Non volevo nemmeno essere capito da lei."


translate italian day5_un_8ef1b6f9:


    "Ero semplicemente troppo stanco, di tutto."


translate italian day5_un_5ac38cf1:


    me "Perché?"


translate italian day5_un_f174e90e:


    un "Vattene…"


translate italian day5_un_e3a26dcc:


    "Ha sussurrato."


translate italian day5_un_1457a622:


    me "Non voglio."


translate italian day5_un_380bd525:


    un "Allora me ne andrò io."


translate italian day5_un_bf0dc08a:


    me "Andiamo insieme."


translate italian day5_un_049b021f:


    un "No."


translate italian day5_un_35d834dd:


    me "Per quanto ancora hai intenzione di fare l'offesa? Di che cosa? Cos'hai che non va? Tutti qui si comportano normalmente, tranne te."


translate italian day5_un_3fddb229:


    "Non m'importava del significato delle mie parole – era come se qualcun altro le avesse pronunciate al mio posto, e l'argomento di quella conversazione non avesse alcun significato."


translate italian day5_un_5dedc88b:


    un "Davvero non capisci proprio niente. Alisa aveva ragione."


translate italian day5_un_4bf0d4a0:


    me "Riguardo a cosa?"


translate italian day5_un_20163f63:


    un "Non importa…"


translate italian day5_un_0b35a130:


    "Ho chiuso gli occhi e ho riflettuto."


translate italian day5_un_475b9270:


    th "No, non posso andarmene. Non capisco, non so cosa pensare, non so cosa fare."


translate italian day5_un_abacf06a:


    th "E da quando ho smesso di preoccuparmi della mia situazione in questo mondo, o di come poter tornare indietro?"


translate italian day5_un_d8c918ef:


    th "Da quando l'unica cosa a cui penso è Lena, o di come gli altri mi vedono, o di come le mie azioni appaiono esteriormente?"


translate italian day5_un_56543886:


    th "È semplicemente stupido e non è da me, infatti – è del tutto inappropriato in questa situazione!"


translate italian day5_un_aed8e1b2:


    me "In poche parole, non ho fatto niente e non intendo giustificarmi per cose che non ho fatto!"


translate italian day5_un_dfffcbac:


    "Non c'è stata alcuna risposta."


translate italian day5_un_4183115b:


    me "Ehi! Mi hai sentito?"


translate italian day5_un_d7d3178a:


    "Finalmente mi sono deciso a vedere chi si stava nascondendo dietro a quell'albero, ma non c'era nessuno."


translate italian day5_un_6807a309:


    th "Se n'è andata!"


translate italian day5_un_e9ad8eab:


    "Mi sono precipitato verso la barca di Lena, ma ormai era lontana, quasi vicina al molo."


translate italian day5_un_8f338516:


    me "Ah, chi se ne frega!"


translate italian day5_un_8494703d:


    "Ho gridato, camminando lungo la riva."


translate italian day5_un_ce617998_4:


    "…"


translate italian day5_un_2c35439c:


    "Sono tornato in tempi relativamente brevi e senza troppi problemi."


translate italian day5_un_9d79bcf3:


    "Tuttavia, le mie braccia erano comunque doloranti, e i miei occhi si chiudevano da soli."


translate italian day5_un_dd481fe9:


    "Probabilmente un tale sforzo fisico e, cosa più importante, un tale carico emotivo, erano decisamente troppo per una singola persona."


translate italian day5_un_961bdcdb:


    "Ho camminato lentamente verso la casetta della leader del Campo, fissando il terreno senza pensare a niente…"


translate italian day5_un_793d9878:


    "Qualcuno mi ha chiamato dalla piazza."


translate italian day5_un_93b5fc50:


    dv "Ehi!"


translate italian day5_un_53356b4d:


    "Alisa è corsa verso di me."


translate italian day5_un_2cf26385:


    dv "Sei stato all'isola?"


translate italian day5_un_9513cd87:


    me "Sì…"


translate italian day5_un_d331deef:


    "A dire il vero, non avevo proprio voglia di parlare con lei, ma non avevo neppure la forza di mentirle."


translate italian day5_un_9c8b084d:


    dv "E com'è andata?"


translate italian day5_un_e5f72f43:


    me "Non importa… Sono molto stanco e voglio andare a dormire."


translate italian day5_un_a7c99a15:


    dv "Dai, dimmelo!"


translate italian day5_un_48f402f2:


    "Il suo viso aveva una tale smorfia antipatica che ho fremuto dalla rabbia."


translate italian day5_un_3f72c7b7:


    me "Perché dovrebbe fregartene?!"


translate italian day5_un_12b03082:


    dv "Beh, volevo solo…"


translate italian day5_un_5449a6fc:


    "Ha mormorato, con sgomento."


translate italian day5_un_cc04c95f:


    me "Fatti gli affari tuoi!"


translate italian day5_un_ea222abb:


    "Le ho gridato, accelerando il passo verso le casette."


translate italian day5_un_206f4c0d:


    "Alisa non ha provato a fermarmi."


translate italian day5_un_1e4d5480:


    "E come se non bastasse, Olga Dmitrievna non era ancora tornata, e non riuscivo a trovare la mia chiave."


translate italian day5_un_325046a9:


    th "E se l'avessi persa?"


translate italian day5_un_b5d9b711:


    "L'unica cosa da fare era aspettare…"


translate italian day5_un_95440e29:


    "Mi sono lasciato cadere sulla sedia a sdraio e ho chiuso gli occhi."


translate italian day5_un_3eda7fc1:


    "Il mio cuore era pesante, e la mia anima lacerata da aspettative incerte."


translate italian day5_un_71767923:


    "Di tanto in tanto mi sembrava che fossi morto (senza però esserne consapevole), e che fossi stato gettato all'inferno."


translate italian day5_un_ef43a5c3:


    th "Davvero, invece di cercare di andarmene da questo posto, sto girando sempre più velocemente su una rotonda diabolica. Sto diventando sempre più coinvolto nella vita quotidiana di questo Campo."


translate italian day5_un_f6d61e07:


    "Come se non avessi avuto più una vita passata – quella reale."


translate italian day5_un_6a8003ef:


    "Come se mi interessassero sempre le opinioni altrui…"


translate italian day5_un_0730defb:


    th "Accidenti! Non mi è mai importato di esse!"


translate italian day5_un_226088e0:


    th "Perché proprio qui? Perché proprio ora?"


translate italian day5_un_701c073f:


    "Ho ricordato il viso di Lena, in lacrime."


translate italian day5_un_79fc3a70:


    th "Già, forse non sono le opinioni altrui, ma la sua opinione, quella che conta…"


translate italian day5_un_195bd715:


    "Ho sentito dei passi, e poco dopo è apparsa Olga Dmitrievna."


translate italian day5_un_fb7d3b52:


    "Mi ha guardato per alcuni secondi. Sembrava che stesse per dire qualcosa, ma poi ha sospirato, ha aperto la porta con la sua chiave ed è entrata."


translate italian day5_un_f73e0745:


    "L'ho seguita."


translate italian day5_un_62928289:


    "Ombre confuse, ricordi sfocati, frammenti di sensazioni ed emozioni hanno vorticato a lungo nella mia testa."


translate italian day5_un_da141c38:


    "Così a lungo che non riuscivo più a capire dove mi trovassi, o cosa stesse accadendo attorno a me."


translate italian day5_un_621dba00:


    "L'unica mia salvezza era il sonno…"
# Decompiled by unrpyc: https://github.com/CensoredUsername/unrpyc
