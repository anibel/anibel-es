
translate italian day1_e9644438:


    "La luce accecante del giorno ha colpito i miei occhi."


translate italian day1_8e82fc0a:


    "All'inizio non me ne sono accorto, non essendomi ancora svegliato del tutto."


translate italian day1_44c308e2:


    "Le mie gambe mi hanno guidato da sole verso la porta."


translate italian day1_ccc28a84:


    me "Dannazione, devo essermi addormentato e ho perso la mia fermata!"


translate italian day1_aee379b4:


    "Ma non c'era alcuna porta…"


translate italian day1_d42615e7:


    "Mi sono guardato attorno e ho scoperto che non era il vecchio usurato LiAZ, ma un Ikarus, nuovo di zecca!"


translate italian day1_03cecdb6:


    "Sono rimasto pietrificato dallo shock."


translate italian day1_3665cd10:


    th "Ma come...?{w} Cosa...?{w} Sono morto...?"


translate italian day1_2df08a3e:


    th "Sono stato rapito?"


translate italian day1_a23b7265:


    th "No, devo essere proprio morto..."


translate italian day1_a2eb760d:


    "Mi sono tastato freneticamente, mi sono dato qualche forte e doloroso schiaffo, ho picchiato la fronte sul retro di uno dei sedili..."


translate italian day1_892f12f3:


    th "Ora è chiaro: o sono ancora vivo, oppure si può ancora provare dolore dopo la morte."


translate italian day1_b55f45ec:


    th "Ma com'è potuto succedere?!"


translate italian day1_8b1a1045:


    th "Forse mi sono addormentato troppo a lungo e sono finito al deposito."


translate italian day1_fa8f47fe:


    th "E poi cosa, mi hanno messo su un altro bus...?"


translate italian day1_05f4c741:


    "Mi sono precipitato fuori e ho dato un'occhiata in giro."


translate italian day1_7fd7201a:


    "C'era verde ovunque: una folta erba sul ciglio della strada, alberi, fiori…"


translate italian day1_0c35f127:


    th "Estate!{w} Ma come?!{w} Era inverno appena un momento fa…"


translate italian day1_ae3344e7:


    "La testa mi doleva in modo insopportabile.{w} Come se stesse per esplodere."


translate italian day1_2208f558:


    "Pian piano, ho iniziato a ricordare."


translate italian day1_8d858ed9:


    "Ricordavo una lunga strada che si estendeva verso orizzonti lontani; foreste, pianure, campi, laghi, e poi ancora foreste."


translate italian day1_786f868f:


    th "Credo che stessi dormendo, ma allora come riesco a ricordare tutto ciò?"


translate italian day1_2f605107:


    "E poi…{w} un vuoto…"


translate italian day1_5fabbbed:


    "Una ragazza china su di me."


translate italian day1_73437152:


    "Mi ha sussurrato delicatamente qualcosa all'orecchio."


translate italian day1_c2d5fb61:


    "Poi un altro vuoto..."


translate italian day1_2e96e275:


    "E poi mi sono risvegliato qui."


translate italian day1_c8d2bde3:


    th "Chi era quella strana ragazza?"


translate italian day1_a1a6b8d6:


    th "O me la sono sognata?"


translate italian day1_ebfa0491:


    "Per qualche ragione il pensare a lei mi ha fatto sentire meglio e mi ha confortato un po'. Mi sono sentito avvolto da un piacevole calore, proveniente dall'interno."


translate italian day1_fde77cc5:


    th "Forse è lei che mi ha portato qui?"


translate italian day1_9bacf66f:


    th "Allora devo trovarla!"


translate italian day1_a35e9118:


    th "E il posto migliore per cercarla è certamente lontano da qui."


translate italian day1_378336bd:


    "Mi sono precipitato a sinistra, poi a destra, poi mi sono fermato, non sapendo dove andare. Alla fine, sono corso verso la direzione dalla quale presumibilmente era arrivato l'autobus."


translate italian day1_a20cefa7:


    "..."


translate italian day1_a20cefa7_1:


    "..."


translate italian day1_723ee7f4:


    "L'esercizio fisico ti rinfresca la mente. I pensieri diventano più chiari, e risulta più facile valutare la realtà circostante."


translate italian day1_cb806649:


    "Non nel mio caso tuttavia – stavo seduto sul ciglio della strada, ansimando e cercando di alleviare la mia gola dolorante, inspirando boccate di aria calda."


translate italian day1_6d244e52:


    "Ad ogni modo, la corsa ha sortito i suoi effetti: la paura si è ritirata per un istante."


translate italian day1_adb77bc9:


    th "Forse sto davvero sognando...?"


translate italian day1_93fa4347:


    "Anche se ripensando ai miei atti di autolesionismo sull'autobus, ho immediatamente rigettato l'idea."


translate italian day1_fae72ec6:


    me "Non sto né sognando né sono morto..."


translate italian day1_36f36703:


    "Una strada stretta attraversava i campi e finiva in lontananza.{w} La stessa strada del mio sogno."


translate italian day1_21bd3c65:


    th "Devo essere molto lontano da casa."


translate italian day1_41a25460:


    "E non solo perché ieri era inverno e oggi è estate."


translate italian day1_fe462b7c:


    "È l'intero ambiente."


translate italian day1_610fd3ae:


    th "Ovviamente, l'estate è proprio così, verde e calda – ma qui tutto sembra finto."


translate italian day1_b66d48e9:


    "Ogni cosa sembrava essere stata presa dai dipinti degli artisti russi del diciannovesimo secolo."


translate italian day1_d7a6969b:


    "L'erba è troppo rigogliosa; i cespugli non sono come dovrebbero essere, sono così fitti che non si riesce a vedere nulla attraverso di essi, come le cime degli alberi, a dire il vero..."


translate italian day1_6482df8a:


    "E gli alberi stessi...{w} La foresta era abbastanza lontana, ma gli alberi pare avessero serrato i ranghi e stessero solo aspettando il comando di avanzare sui campi e sulle pianure."


translate italian day1_46d39e23:


    "Ho ripreso fiato e ho guardato l'autobus, che adesso era appena visibile."


translate italian day1_d0f34e2d:


    th "È stata una bella corsa..."


translate italian day1_5a5230cb:


    "Sono stato sopraffatto nuovamente dalla paura."


translate italian day1_52a8e81e:


    th "Quelle linee elettriche...{w} Devono esserci delle persone nelle vicinanze!"


translate italian day1_a53379f8:


    "Ma questo cosa {i}significa{/i}?"


translate italian day1_8d395e64:


    "In realtà, non significa proprio niente!{w} Potrebbero avere delle linee elettriche persino all'inferno!"


translate italian day1_fbe4386c:


    "Friggere i peccatori sui carboni ardenti? È così medievale..."


translate italian day1_c88e5ea3:


    "Devo aver raggiunto il punto di non ritorno, oltre il quale si hanno due sole possibilità: perdere completamente la testa, oppure riuscire finalmente a capire cosa sta accadendo."


translate italian day1_3cbef58c:


    th "E finché ne ho la possibilità, dovrei scegliere la seconda opzione!"


translate italian day1_28b9b20f:


    "Sono ritornato lentamente verso l'autobus."


translate italian day1_9aeca35d:


    th "Certamente è stato spaventoso."


translate italian day1_d5a8e5e1:


    th "Ma non credo di riuscire a trovare delle risposte nei campi o nei boschi, e quell'odioso catorcio è l'unico collegamento che ho con il mondo reale."


translate italian day1_a20cefa7_2:


    "..."


translate italian day1_001631c6:


    th "Dovrei esplorare l'area attentamente."


translate italian day1_3e09185c:


    "Un muro di mattoni e un cancello sopra il quale stava la scritta «Sovyonok», statue di pionieri su entrambi i lati, e un cartello stradale mostrante il numero della tratta: «410»."


translate italian day1_59b146e5:


    me "Questo viaggio sta durando un po' troppo."


translate italian day1_e98bfbde:


    "Ho fatto un sorrisetto."


translate italian day1_0fa2263b:


    "Una persona potrebbe iniziare a comportarsi in modo strano in situazioni estreme."


translate italian day1_db78176c:


    th "Probabilmente la stessa cosa sta accadendo a me in questo momento."


translate italian day1_95a9a169:


    "Questo luogo non sembra affatto abbandonato – nessuna traccia di ruggine sul cancello, nessun danno alle mura."


translate italian day1_3ba35159:


    me "«Sovyonok»…"


translate italian day1_33b4e80e:


    th "Cosa potrebbe avere un nome simile?"


translate italian day1_94f91cd7:


    th "A giudicare dalle statue di pionieri, potrebbe trattarsi di un campo estivo per bambini.{w} E inoltre, sembra essere aperto!"


translate italian day1_deb4632a:


    th "Ovviamente la spiegazione più semplice, quella più logica, non spiega assolutamente nulla."


translate italian day1_b987229b:


    th "La strana ragazza, l'autobus rimpiazzato, l'estate, il campo..."


translate italian day1_33930872:


    "Immediatamente migliaia di teorie sono affiorate nella mia mente: dal rapimento alieno al letargo, dalle allucinazioni ai viaggi nel tempo…"


translate italian day1_d28308d8:


    "Nessuna di queste era migliore delle altre, ma non potevo di certo sceglierne una."


translate italian day1_c83682b4:


    "Poi ho avuto un'idea – posso provare a fare una chiamata!"


translate italian day1_30421d7d:


    "Ho tirato fuori il mio telefono e ho selezionato il primo numero dalla mia lista contatti."


translate italian day1_a5e58fe8:


    "Ma invece delle tacche del segnale, lo schermo mostrava una croce spessa."


translate italian day1_19146449:


    th "Va bene, potrebbe non esserci segnale in un luogo così remoto."


translate italian day1_0d3882c7:


    th "Anche se non posso essere l'unico che è arrivato qui!"


translate italian day1_c52f433f:


    th "Gli autobus non si guidano da soli!"


translate italian day1_9191dff2:


    "Ho esaminato il bus da ogni angolo per essere certo che non si trattasse di un'allucinazione."


translate italian day1_841adb1f:


    "Pezzi di fango nella parte inferiore, un po' di ruggine qua e là, vernice sbiadita e gomme usurate – no, questo è a tutti gli effetti un normale Ikarus."


translate italian day1_6109f212:


    th "Già, esattamente il tipo di bus che ti porta in luoghi remoti se ti addormenti con noncuranza."


translate italian day1_0343ddfb:


    "Ho fatto una risatina nervosa."


translate italian day1_d4028078:


    "Mi è venuto spontaneo.{w} Perché questo non era né il tempo né il luogo adatto per ridere..."


translate italian day1_17403436:


    th "Ma dove diavolo è il conducente?"


translate italian day1_7530ded5:


    "Mi sono seduto cautamente sul marciapiede accanto all'autobus e ho iniziato ad aspettare."


translate italian day1_a20cefa7_3:


    "..."


translate italian day1_4bc2caf0:


    "La mia pazienza non è durata a lungo."


translate italian day1_ede35d71:


    "L'ansia sembrava aver raggiunto il picco massimo, e stavo perdendo la ragione."


translate italian day1_c8bc0b5a:


    "In una situazione del genere, chiunque si sarebbe sentito allo stesso modo."


translate italian day1_0b02c10b:


    "Gli alieni e gli universi paralleli erano ormai svaniti dalla mia mente, lasciando spazio solo al vuoto e all'oscurità."


translate italian day1_33b8346c:


    th "Dunque è così che finirà? Che {i}la mia vita{/i} finirà?"


translate italian day1_0818ff10:


    th "Ma devo ancora fare molto, ho ancora così tante cose da fare per le quali non ho avuto tempo..."


translate italian day1_405f9fbf:


    "Sono stato sopraffatto dall'idea: questa era davvero la fine."


translate italian day1_97d1733b:


    th "Ma perché?!"


translate italian day1_6e7e69ad:


    th "Non è giusto! Non sono peggiore di chiunque altro!"


translate italian day1_a72fb0ee:


    th "Dio, perché...?"


translate italian day1_69065e88:


    "Gli occhi pieni di lacrime mi stavano bruciando in modo insopportabile, mi sono rannicchiato e ho cominciato a rotolare sull'erba."


translate italian day1_4629954b:


    me "PERCHÉ?! COSA HO FATTO DI MALE?! PERCHÉ IO?!"


translate italian day1_6f37277c:


    "Ma gli unici a sentire le mie grida erano le due mute statue di pionieri e un uccellino su un albero, che immediatamente ha sbattuto le ali ed è volato via, urlando qualcosa nel suo linguaggio di volatile, come se stesse deridendo l'idiota che ha osato interrompere il suo pisolino post-pranzo."


translate italian day1_893c0214:


    "Mi mancava il fiato a causa del pianto, e mi sono limitato a rimanere sdraiato in silenzio, piagnucolando di tanto in tanto."


translate italian day1_a20cefa7_4:


    "..."


translate italian day1_1b527b73:


    "Dopo un po' mi sono ripreso."


translate italian day1_659aa794:


    "La mia mente si è rimessa a posto, come se il terrore e la paura della morte mi avessero dato un momento di tregua."


translate italian day1_dba992b6:


    th "Tutto sommato, se qualcuno volesse uccidermi, perché tutto questo?"


translate italian day1_1a34ee7c:


    th "Non sembra nemmeno essere un esperimento."


translate italian day1_56b2c610:


    th "Se questa è semplicemente una folle coincidenza allora probabilmente non c'è nulla di cui preoccuparsi."


translate italian day1_cc407c1e:


    th "Comunque, finora non sembra esserci alcun pericolo."


translate italian day1_35abcf62:


    "Il panico mi ha abbandonato."


translate italian day1_bbe3feaf:


    "Ovviamente il sangue mi stava ancora martellando nelle vene, e le mie mani stavano ancora tremando, ma almeno adesso riuscivo a pensare in modo chiaro."


translate italian day1_007b1ddb:


    th "Comunque, non c'è nulla che io possa cambiare adesso!{w} Quindi non importa quanto io cerchi di pensare o quanto arrabbiato io possa essere, potrei solo peggiorare le cose."


translate italian day1_607957e4:


    th "Finché qualche avvenimento concreto non accadrà, non ha alcun senso tirare a indovinare."


translate italian day1_cea13f7e:


    th "Ad ogni modo, non scoprirò nulla restando qui a poltrire."


translate italian day1_f68fbb50:


    "Questo campo (ovviamente, se si tratta davvero di un campo) sembrava essere l'unico luogo dove poter trovare qualcuno, così ho deciso di andarci, e stavo per raggiungere l'ingresso quando..."


translate italian day1_cb43b355:


    "Una ragazza è apparsa da dietro il cancello..."


translate italian day1_c9771ed6:


    "... indossando un'uniforme da pioniere."


translate italian day1_76dc3c1c:


    th "La mia logica non mi ha deluso questa volta."


translate italian day1_c454f340:


    th "Ma poi, un'uniforme da pioniere nel ventunesimo secolo…"


translate italian day1_55a53f71:


    th "D'altronde, una ragazza {i}qui{/i}..."


translate italian day1_a0dd748b:


    "Sono rimasto pietrificato, incapace di fare un singolo passo."


translate italian day1_888fb514:


    "Avevo proprio voglia di fuggire.{w} Fuggire il più lontano possibile da questo luogo, lontano dall'autobus, dal cancello, dalle statue e lontano da questo dannato uccellino con la sua siesta."


translate italian day1_1a518774:


    "Semplicemente correre via, libero come il vento, sempre più veloce, oscillando tra i pianeti, ammiccando alle galassie."


translate italian day1_8abc0b99:


    "Correre, diventare un raggio spaziale, una radiazione cosmica di fondo, correndo verso l'ignoto."


translate italian day1_b53ca9d4:


    "Correre non importa dove, basta che sia {b}molto lontano{/b} da questo luogo!"


translate italian day1_d158f7e2:


    "Nel frattempo la ragazza si è avvicinata, sorridendo."


translate italian day1_3fbcaafd:


    "Non ho potuto ignorare la sua bellezza, anche se stavo tremando dalla paura."


translate italian day1_01e37be4:


    "L'istinto umano funziona indipendentemente dalla consapevolezza, e laddove solo il 5%% del nostro cervello è responsabile dei processi cognitivi, il rimanente 95%% è costantemente occupato a sostenere la {i}vita{/i}, e in particolare, garantire un funzionamento stabile del sistema ormonale."


translate italian day1_b619432f:


    "Ho disperatamente voluto che la cosa diventasse meno complicata, smettendo di pensare a frasi pescate da un'enciclopedia, anche se i miei pensieri comparivano uno dopo l'altro, così stupidi, fuori luogo, come se fossero stati presi dal monologo interiore del protagonista di un qualche romanzo poliziesco da due soldi."


translate italian day1_159ede02:


    "Un bel viso slavo, delle lunghe trecce che sembravano due bracciate di fieno fresco, e occhi talmente blu che sembrava si potesse annegare dentro essi."


translate italian day1_ebdc09cc:


    slp "Ciao, devi essere appena arrivato?"


translate italian day1_a009dbf2:


    "Ero impietrito dallo stupore."


translate italian day1_b5aec161:


    "La ragazza sembrava perfettamente normale, proprio come un essere umano ordinario, ma non riuscivo ancora a pronunciare una singola parola."


translate italian day1_fff6f9eb:


    th "Ormai è troppo tardi per fuggire..."


translate italian day1_880e4e10:


    slp "Ho detto qualcosa di sbagliato?"


translate italian day1_06176d1f:


    "Mi è costato uno sforzo monumentale risponderle:"


translate italian day1_13317d7b:


    me "Ehm…sì…"


translate italian day1_c44cb8e0:


    slp "Che cosa?"


translate italian day1_d51bc1bf:


    me "Oh, niente, intendevo dire, sono {i}appena arrivato{/i}."


translate italian day1_c0a65fae:


    "Ho risposto rapidamente, iniziando a preoccuparmi del fatto di aver detto qualcosa di sbagliato."


translate italian day1_13317d7b_1:


    me "Ehm… sì…"


translate italian day1_3c13f21e:


    slp "Bene allora, benvenuto!"


translate italian day1_6d77584d:


    "Mi ha fatto un ampio sorriso."


translate italian day1_82d0a78d:


    "Strano, sembra che io abbia di fronte una ragazza normale."


translate italian day1_31e75c3d:


    th "Mah, non sarei dovuto tornare qui, i boschi e i campi sembravano essere la scelta migliore..."


translate italian day1_c2c61eee:


    th "Ma adesso cosa faccio – cerco di parlare con lei come se fosse umana, o dovrei fuggire, o cosa?"


translate italian day1_85e4f294:


    "Il sangue mi pulsava violentemente nella testa, distruggendola dall'interno; ancora un po' e la povera ragazza pioniere sarebbe stata ricoperta dal macabro contenuto del mio cranio..."


translate italian day1_26d0580a:


    slp "Che c'è di divertente?"


translate italian day1_5b1e985b:


    "La ragazza mi ha esaminato."


translate italian day1_632c64ff:


    "Ho sentito un brivido scorrermi lungo la schiena, e le mie gambe hanno iniziato a tremare."


translate italian day1_fa942807:


    me "N... niente..."


translate italian day1_81fa1f0f:


    slp "Bene allora."


translate italian day1_35ff2f5f:


    th "Bene? Cosa c'è di {i}bene{/i} in tutto ciò?"


translate italian day1_9d000438:


    "Di colpo un pensiero mi ha attraversato la mente: al diavolo tutto questo. Devo dimenticarmi dell'autobus dietro di me, del fatto che ieri era inverno e che oggi invece è estate. Volevo solo strapparmi il pruriginoso maglione che avevo addosso e accettare tutto quello che mi stava accadendo. Tutto è come dovrebbe essere, tutto sta andando per il meglio..."


translate italian day1_1ab1c37a:


    me "Per caso mi sapresti dire…?"


translate italian day1_d0ebceb7:


    slp "Dovresti andare dalla leader del Campo, lei ti dirà tutto!"


translate italian day1_3399b38f:


    slp "Guarda.{w} Vai sempre dritto verso la piazza, poi gira a sinistra. Vedrai molte piccole casette."


translate italian day1_f40c35be:


    "Mi ha indicato il cancello, come se io sapessi cosa ci fosse dietro."


translate italian day1_54f0a60f:


    slp "Beh, potresti chiedere a qualcuno dove si trova la casetta di Olga Dmitrievna."


translate italian day1_17993887:


    me "Io… ehm…"


translate italian day1_81c28b1c:


    slp "Capito?"


translate italian day1_a1e6972e:


    "Certo che no."


translate italian day1_2f0e0a31:


    slp "Bene, adesso devo andare."


translate italian day1_77cc4ba5:


    "La ragazza è sparita dietro al cancello, salutandomi con la mano."


translate italian day1_bc447943:


    "Sembrava che per lei io fossi qualcosa di...{w} normale."


translate italian day1_07ef85cb:


    "E tutto questo teatrino con l'autobus e il viaggio da sveglio o addormentato stavano turbando solo me, mentre tutto qui sembra essere come dovrebbe."


translate italian day1_2d3f952e:


    th "La leader del Campo, l'uniforme da pioniere…"


translate italian day1_cf1c4a81:


    th "Cos'è, stanno facendo una ricostruzione storica qui?"


translate italian day1_bce4a681:


    th "Spero di non imbattermi in Lenin che sta sopra a un'automobile blindata, in quella piazza."


translate italian day1_1b36af68:


    th "Ma neppure una cosa del genere mi sorprenderebbe in questo momento…"


translate italian day1_39014c31:


    "Dopo essere rimasto fermo per un po', mi sono diretto verso il Campo."


translate italian day1_6f92edab:


    "A soli cinquanta metri più avanti è spuntato sulla sinistra un piccolo edificio monopiano.{w} Il cartello accanto alla porta riportava la scritta «Circolo»."


translate italian day1_b20957b2:


    "Stavo per avvicinarmi..."


translate italian day1_93faf713:


    "Quando all'improvviso la porta si è aperta ed è uscita una bassa ragazza con indosso un'uniforme da pioniere."


translate italian day1_bc2ffd42:


    "Il suo bel viso mi ha dato l'impressione di qualcuno che stava soffrendo per il destino dell'intera umanità, con un autentico dolore universale."


translate italian day1_5b8bfb48:


    "Non appena mi ha visto, la ragazza è rimasta immobile, come terrorizzata."


translate italian day1_82b60add:


    "Anch'io sono rimasto immobile, cercando di capire quale fosse la cosa migliore da fare – se dire qualcosa o se aspettare che fosse lei a mostrare una qualche iniziativa.{w} O semplicemente fuggire..."


translate italian day1_d6de2681:


    "Anche se quest'ultima opzione era costantemente suggerita dal mio istinto di conservazione (o almeno volevo credere che fosse così)."


translate italian day1_2cebe5d7:


    "Non il peggiore degli istinti umani, ma molto distante da quello più logico."


translate italian day1_63473f65:


    "Se questo istinto giocasse a poker contro le abilità deduttive, il risultato sarebbe predeterminato."


translate italian day1_a971bc7a:


    "E tali abilità deduttive (o almeno la loro apparenza) mi stavano suggerendo che non c'era motivo di essere impaurito da quella ragazza."


translate italian day1_9b858553:


    "Ad un tratto qualcuno è sbucato fuori dai cespugli."


translate italian day1_b978b533:


    "Una ragazzina con indosso una maglietta di un rosso acceso, con la scritta «URSS»."


translate italian day1_7ebe0cfb:


    th "Perfettamente in linea con l'epoca."


translate italian day1_42c9a02c:


    "Da lontano mi è sembrata piuttosto bassa, e probabilmente era più giovane delle altre due ragazze – quella che ho incontrato al cancello e quella davanti al «Circolo»."


translate italian day1_c52afe92:


    "Alla fine ho deciso di avvicinarmi, ma la «URSS» (come la chiamavo nella mia testa) è balzata verso la prima ragazza e ha iniziato a dirle qualcosa, agitando selvaggiamente le braccia."


translate italian day1_b63ce3ba:


    "L'altra ragazza di conseguenza è sembrata confusa, e ha abbassato lo sguardo rimanendo in silenzio."


translate italian day1_d44e3c53:


    "Probabilmente sarei rimasto lì a osservare quel loro spassoso discorso, ma la «URSS» ad un certo punto ha tirato fuori qualcosa dalla sua tasca e ha iniziato a ondeggiarla davanti al viso dell'altra ragazza."


translate italian day1_258e8119:


    "Quel {i}qualcosa{/i} si è rivelata essere una cavalletta."


translate italian day1_ed6083c2:


    unp "Aaaaaa-aaa-aaaaaa!"


translate italian day1_87779d4a:


    "Ha strillato la prima ragazza."


translate italian day1_f91880c6:


    "Non dev'essere molto amante degli insetti, dato che si è precipitata istantaneamente verso il luogo dove Lenin presumibilmente aveva tenuto il suo discorso sulla rivoluzione dei lavoratori e dei contadini."


translate italian day1_03448d77:


    "Vale a dire, verso la piazza…"


translate italian day1_ebc4605c:


    "La «URSS» mi ha lanciato un'occhiata, ha sogghignato giocosamente, e si è precipitata dietro a lei."


translate italian day1_38e92acb:


    th "Non male come inizio della giornata."


translate italian day1_1c924fd9:


    th "Non ho la più pallida idea di dove io mi trovi, e inoltre ci sono dei ragazzini che giocano a fare i pionieri."


translate italian day1_3f084620:


    th "E a quanto pare, questo luogo è situato a migliaia di chilometri da casa mia.{w} Potrebbe addirittura trovarsi in un'altra realtà…"


translate italian day1_b8844662:


    "E questa è a tutti gli effetti una {i}realtà{/i}..."


translate italian day1_bd1b7173:


    "Insomma, tutto intorno a me sembrava così reale (anche se un po' abbellito) che stavo iniziando a pensare che in realtà la mia vita precedente fosse stata solo un sogno..."


translate italian day1_947b07b9:


    th "E adesso cosa dovrei fare?"


translate italian day1_a20cefa7_5:


    "..."


translate italian day1_a48ce857:


    "Stavo spostando dei pezzi di selciato rotto con la scarpa, fissando l'edificio del «Circolo» con lo sguardo perso nel vuoto."


translate italian day1_52f502cd:


    th "Ancora qualche secondo, prima di dover prendere una decisione."


translate italian day1_6f416834:


    "È stato allora che mi sono ricordato di come prima mi rotolavo sull'erba, piangendo..."


translate italian day1_487a7790:


    "Ho rabbrividito disgustato."


translate italian day1_34312b93:


    "Forse si tratta di un altro istinto: dopo che tutte le energie vengono consumate per piangere e autocommiserarsi, il corpo va in letargo, o mobilita le sue riserve."


translate italian day1_8ff0ae14:


    "Il mio sembrava aver scelto la seconda opzione, perché di punto in bianco ho trovato la determinazione per capire cosa stesse accadendo."


translate italian day1_6aea12ff:


    "E per riuscirci avrei dovuto agire come un uomo – come un essere umano: per mantenere la dignità di un rappresentante del {i}mio mondo{/i}."


translate italian day1_267d5adc:


    "Ho seguito il sentiero alla mia sinistra, sul lato destro del quale si trovavano delle piccole casette, probabilmente le abitazioni dei pionieri."


translate italian day1_772090f6:


    "A dire il vero sembravano molto accoglienti viste da fuori."


translate italian day1_5ff36d96:


    "Anche se sono nato nell'Unione Sovietica, non ho mai partecipato a queste organizzazioni giovanili di pionieri, e nemmeno ai Piccoli Ottobrini."


translate italian day1_d869f630:


    "Mi immaginavo la tipica vita da pioniere in modo diverso: enormi caserme con lunghe file di cuccette di metallo, il risveglio segnalato da una sirena alle sei del mattino, un minuto per rifare il letto, e poi tutti in formazione alla piazza…"


translate italian day1_3a1335b5:


    th "Ma aspetta!{w} Forse mi sto confondendo con qualcos'altro…?"


translate italian day1_9eee971e:


    "Improvvisamente qualcosa mi ha colpito alle spalle!"


translate italian day1_bb359f8d:


    "Ho barcollato riuscendo però a rimanere in piedi, mi sono voltato preparandomi a combattere eroicamente per la mia vita..."


translate italian day1_57666fa8:


    "Ma quello che mi sono trovato di fronte era solo un'altra ragazza."


translate italian day1_ee51804d:


    "Ho spalancato la bocca dallo stupore."


translate italian day1_68b22888:


    dvp "Tira su quella mascella da terra!"


translate italian day1_143f62c0:


    "Ho chiuso la bocca."


translate italian day1_c1b4c8b4:


    "Aveva la solita uniforme da pioniere, ma il modo in cui la indossava era, diciamo così, un po' provocante."


translate italian day1_f689de87:


    "Come tutte le ragazze che ho incontrato qui, anche questa era piuttosto carina, ma la sua espressione arrogante mi ha fatto subito passare la voglia di conoscerla meglio."


translate italian day1_5ecab483:


    dvp "Sei nuovo qui, vero?"


translate italian day1_51149e68:


    me "…"


translate italian day1_dc8deb21:


    dvp "Bene, ci vediamo!"


translate italian day1_df07b66c:


    "Mi ha lanciato un'occhiata minacciosa e se n'è andata."


translate italian day1_be998354:


    "Ho atteso che sparisse dietro l'angolo...{w} Chissà cos'altro avrebbe potuto avere in mente!"


translate italian day1_310f2718:


    "La cosa più interessante è che anche quella ostile ragazza mi è sembrata del tutto normale, non mi ha dato l'impressione di essere un pericolo mortale."


translate italian day1_dbdad37c:


    "A parte forse il pericolo di prendersi un pugno sul naso..."


translate italian day1_78546616:


    "Finalmente sono riuscito a raggiungere la piazza."


translate italian day1_79587a2a:


    "Non c'era Lenin su un'auto blindata, benché ci si potesse aspettare una cosa simile dopo tutto quello che era successo."


translate italian day1_b624fcdd:


    "C'era invece la statua di un qualche personaggio importante, che si innalzava al centro della piazza. Il monumento riportava la scritta «GENDA»."


translate italian day1_4f2ef086:


    th "Dev'essere qualcuno di importante qui."


translate italian day1_2f3a0b1e:


    "C'erano delle piccole panchine ai lati della piazza."


translate italian day1_f324eeda:


    th "È abbastanza piacevole qui."


translate italian day1_9d30be31:


    th "Dove mi ha detto di andare quella ragazza?{w} A destra o a sinistra?"


translate italian day1_c6de1218:


    th "A destra, a sinistra, a destra, a sinistra…"


translate italian day1_58ada7be:


    th "E poi perché ci sto andando...?"


translate italian day1_bd699a39:


    th "Ah, giusto, perché ho deciso di fingere di essere normale..."


translate italian day1_833eb5f3:


    th "Quindi, a destra!"


translate italian day1_4870b408:


    "Attraverso un piccolo boschetto..."


translate italian day1_be1bac02:


    "Sono finito davanti a un molo."


translate italian day1_66caa1a6:


    th "Devo aver preso la strada sbagliata."


translate italian day1_a02d94f5:


    slp "Ehi, direzione sbagliata!"


translate italian day1_0a8fd9cf:


    "Mi sono voltato verso la voce."


translate italian day1_ea065f04:


    "Quella prima ragazza stava di fronte a me."


translate italian day1_4725a971:


    slp "Non ricordi cosa ti ho detto? Di svoltare a sinistra alla piazza, giusto?"


translate italian day1_59abc400:


    "Invece dell'uniforme da pioniere ora indossava un bikini."


translate italian day1_e85f6b79:


    slp "Oh, scusa, non mi sono ancora presentata!{w} Il mio nome è Slavya!"


translate italian day1_2fa68520:


    slp "In realtà il mio vero nome è Slavyana, ma tutti mi chiamano Slavya.{w} Quindi puoi farlo anche tu!"


translate italian day1_6e37a835:


    me "Ehm… sì…"


translate italian day1_0cc735df:


    "Ero ancora un po' confuso, e quindi non sono riuscito a pensare a una risposta più sensata."


translate italian day1_4c5321be:


    sl "E tu come ti chiami?"


translate italian day1_c5787aa3:


    "Era come se riuscisse a guardarmi attraverso."


translate italian day1_5cf26729:


    me "Ehm… io… sì… Semyon…"


translate italian day1_e2552123:


    sl "Lieta di incontrarti, Semyon."


translate italian day1_a89521fc:


    sl "Bene, io ho quasi finito qui.{w} Potresti aspettare solo un minuto, mi cambio e poi andiamo insieme da Olga Dmitrievna, d'accordo?"


translate italian day1_65bf82fc:


    me "D'accordo…"


translate italian day1_43560fad:


    "Dopo questo scambio di battute è corsa via. Mi sono seduto sul molo e ho immerso i piedi nell'acqua."


translate italian day1_90942777:


    "Stavo indossando dei pesanti scarponi invernali, ma con questo clima non c'era niente di male nel bagnarsi un po' i piedi."


translate italian day1_5c4ff44b:


    "Inoltre, la cosa mi ha rinfrescato un po'."


translate italian day1_ea94e5d1:


    "Guardando il fiume, stavo pensando e elaborando tutto quello che era accaduto."


translate italian day1_24d93bb8:


    th "Se questo è una specie di complotto, allora è molto strano, a volte persino troppo amichevole."


translate italian day1_5edac208:


    th "No, sembra più una coincidenza."


translate italian day1_044eac6b:


    th "Una qualche coincidenza del tutto incomprensibile."


translate italian day1_a733d584:


    sl "Andiamo?"


translate italian day1_3d8e6458:


    "Slavya stava in piedi accanto a me, indossando la solita uniforme da pioniere."


translate italian day1_85ccdc6d:


    me "Andiamo…"


translate italian day1_1ea0d875:


    th "È da poco che sono qui, ma di tutte le persone che ho incontrato, lei sembra essere la meno sospetta."


translate italian day1_efe66139:


    th "Tuttavia, questo fatto è già sospetto di per sé!"


translate italian day1_b3e9120f:


    "Siamo giunti alla piazza."


translate italian day1_73d6913a:


    "La ragazzina «URSS» e la ragazza che mi aveva colpito sulla schiena erano lì, e si stavano rincorrendo."


translate italian day1_805a8066:


    th "È una sorta di gioco quello a cui stanno giocando?"


translate italian day1_6c5b8d8e:


    sl "Ulyana, basta correre! Dirò tutto a Olga Dmitrievna!"


translate italian day1_e2f0dae9:


    us "Agli ordini, capitano!"


translate italian day1_0bfd5bdf:


    "Ho deciso di aspettare un po' prima di chiedere a Slavya di questo luogo o dei suoi abitanti."


translate italian day1_5280f6b1:


    th "Prima è meglio incontrare quella misteriosa Olga Dmitrievna."


translate italian day1_1cd246d8:


    th "Sembra che lei sia il capo qui."


translate italian day1_3337c244:


    "Siamo passati davanti alle file di casette quasi identiche, alcune delle quali sembravano delle grosse botti di birra, mentre altre sembravano più dei capanni."


translate italian day1_598416c3:


    "Finalmente Slavya si è fermata di fronte a una piccola casetta monopiano."


translate italian day1_4ce282c2:


    "Sembrava un quadro pittoresco: la vernice sbiadita, le crepe qua e là mostravano i segni del tempo, riflettendo i raggi del sole; le persiane delle finestre leggermente socchiuse ondeggiavano in modo quasi impercettibile, scosse dal vento, ed enormi cespugli di lillà crescevano ai lati."


translate italian day1_9ba44673:


    "Era come se quella decrepita capanna stesse affondando in un mare di seta viola, con i fiori di lillà che stavano inesorabilmente distruggendo la dimora del capo delle truppe."


translate italian day1_3dc6042e:


    sl "Che stai aspettando? Andiamo!"


translate italian day1_cc0015de:


    "Slavya mi ha fatto riemergere dal mio sognare ad occhi aperti."


translate italian day1_25a8e409:


    mt_voice "… e smettila di fare i dispetti a Lena…"


translate italian day1_23aa893b:


    "Rena?!"


translate italian day1_368e2a32:


    th "Sembra esserci qualcuno dentro."


translate italian day1_42c5f640:


    "Infatti dopo un instante la porta si è spalancata, e Ulyana si è slanciata fuori con il suo solito sogghigno malizioso."


translate italian day1_9b6e58bf:


    "Dopo di lei è uscita la ragazza dai capelli viola."


translate italian day1_01fca3c6:


    sl "Non farti infastidire da lei, Lena!"


translate italian day1_0d446d18:


    th "E così si chiama Lena…{w} Per fortuna che non è Rena!"


translate italian day1_92afc216:


    un "Ma io non…"


translate italian day1_a3f0580d:


    "Invece di finire la frase, è arrossita e si è diretta rapidamente verso la piazza."


translate italian day1_a653101d:


    "Per qualche motivo ho sentito il bisogno di voltarmi e di seguirla con lo sguardo, ma Slavya mi ha detto:"


translate italian day1_84aaac50:


    sl "Vieni."


translate italian day1_9e8e7cd3:


    "Siamo entrati nella casetta."


translate italian day1_4a8e191a:


    "Al suo interno era come immaginavo: due letti, un tavolo, un paio di sedie, un semplice tappeto sul pavimento, un armadietto."


translate italian day1_c87f01c5:


    "Niente di speciale, ma allo stesso tempo appariva familiare e accogliente, anche se il disordine regnava come nel mio appartamento."


translate italian day1_f9471bdb:


    "La ragazza alla finestra sembrava avere attorno ai venticinque anni."


translate italian day1_1529b84e:


    th "La natura l'ha certamente dotata di un bel viso e di belle forme..."


translate italian day1_0aa01509:


    th "Almeno una cosa può rallegrarmi in questo pandemonio: le abitanti sono bellissime."


translate italian day1_80b7fab7:


    mtp "Eccoti finalmente!{w} Ottimo!{w} Il mio nome è Olga Dmitrievna, sono la leader del Campo."


translate italian day1_81f67e23:


    me "Felice di incontrarti, io sono Semyon."


translate italian day1_2a6869f0:


    "Ho deciso di risponderle facendo finta di non essere sorpreso di tutto quello che stava accadendo."


translate italian day1_b93477a9:


    "Si è avvicinata."


translate italian day1_9a812b52:


    mt "Ti stavamo aspettando fin dal primo mattino."


translate italian day1_b7364cea:


    me "Mi stavate aspettando...?"


translate italian day1_ca77693f:


    mt "Ma certo!"


translate italian day1_dda106bc:


    me "E quando passa il prossimo bus, perché io…"


translate italian day1_fec151d0:


    mt "E a cosa ti serve?"


translate italian day1_8f1ac2ed:


    th "Sì, giusto, a cosa mi serve…?"


translate italian day1_5a1280bd:


    th "Forse è meglio non fare domande dirette: le persone qui potrebbero reagire diversamente da come vorrei."


translate italian day1_79c3f7b2:


    th "E dubito che io possa ottenere delle risposte."


translate italian day1_8d50c9cf:


    me "No, niente, ero solo curioso…"


translate italian day1_1725efc3:


    me "Comunque, dove ci troviamo esattamente?{w} L'indirizzo, intendo."


translate italian day1_21adf436:


    me "Vorrei scrivere ai miei genitori per dire loro che sono arrivato tutto intero."


translate italian day1_efb897d9:


    "Per qualche motivo speravo disperatamente che se avessi giocato la mia parte, avrei ottenuto delle risposte."


translate italian day1_5a58e925:


    mt "Oh, ma i tuoi genitori hanno telefonato appena mezz'ora fa! Mi hanno detto di salutarti."


translate italian day1_41180a2e:


    th "Questa sì che è una sorpresa…"


translate italian day1_350d74d5:


    me "Allora, posso chiamarli? Perché ho dimenticato di dire loro qualcosa prima del viaggio."


translate italian day1_44f6bf31:


    mt "No."


translate italian day1_9d48f5b9:


    "Mi ha fatto un dolce e spontaneo sorriso."


translate italian day1_9bddf507:


    me "Perché no?"


translate italian day1_c40fd752:


    mt "Non abbiamo un telefono qui."


translate italian day1_51149e68_1:


    me "…"


translate italian day1_729f518a:


    me "E allora i miei genitori come hanno telefonato?"


translate italian day1_120ecdbc:


    mt "Sono appena tornata dal centro del distretto, è lì che ho parlato con loro."


translate italian day1_434cc4a4:


    th "Ah, è così che stanno le cose?"


translate italian day1_68be55cd:


    me "E posso in qualche modo raggiungere la città?"


translate italian day1_44f6bf31_1:


    mt "No."


translate italian day1_89d38d04:


    "Ha mantenuto lo stesso sorriso."


translate italian day1_5ac38cf1:


    me "Perché no?"


translate italian day1_06cd3c2d:


    mt "Perché il prossimo bus passa tra una settimana."


translate italian day1_a3268632:


    "Ho deciso di non indagare su come il capo delle truppe fosse riuscita ad arrivare là e a tornare indietro: non avrei ottenuto alcuna risposta in ogni caso."


translate italian day1_bd84df53:


    "Per tutto questo tempo Slavya era rimasta accanto a me, e sembrava non trovare nulla di strano nella nostra conversazione."


translate italian day1_869d68e4:


    mt "Oh, dobbiamo trovare un'uniforme per te!"


translate italian day1_75c6d686:


    th "Non ho alcuna intenzione di mettermi addosso i pantaloncini da pioniere, o quel ridicolo foulard rosso!"


translate italian day1_94a1d364:


    th "Anche se andare in giro con un abbigliamento invernale d'estate non è una grande idea."


translate italian day1_f4df3367:


    me "Giusto, grazie…"


translate italian day1_00ec5ab7:


    th "Mi chiedo se io sia l'unico qui che considera strano l'indossare il cappotto e gli scarponi invernali con questo caldo."


translate italian day1_13e0bfb3:


    mt "Bene ragazzi, io me ne vado, e tu puoi iniziare a fare conoscenza con questo Campo!{w} Non dimenticarti di venire a cena stasera!"


translate italian day1_8af1d414:


    "Dopo aver detto ciò, si è incamminata fuori dalla stanza."


translate italian day1_3be70f85:


    "No, «incamminata» non è la parola giusta – direi piuttosto che si è precipitata fuori."


translate italian day1_733c1098:


    "Sono rimasto da solo con Slavya."


translate italian day1_4c0d1dce:


    sl "Devo andare anch'io, ho delle cose da fare."


translate italian day1_0a470c4f:


    sl "Fatti un giro attorno al Campo, guardati attorno.{w} Ci vediamo stasera."


translate italian day1_d47d9534:


    th "Se davvero non c'è trucco né inganno in questa realtà, come rappresentato da Slavya, allora tutto sta iniziando a piacermi sempre più."


translate italian day1_a20cefa7_6:


    "..."


translate italian day1_a28a58fe:


    "Per la prima volta oggi mi sono accorto che faceva un caldo bestiale."


translate italian day1_3aa2905a:


    "Anche se, ovviamente, era colpa del mio abbigliamento invernale."


translate italian day1_6224b6ad:


    "Mi sono tolto il cappotto e l'ho appoggiato sul letto, poi anche il maglione; adesso indossavo solo la maglietta."


translate italian day1_1302b288:


    th "Così va molto meglio!"


translate italian day1_0650308f:


    "L'unica cosa da fare era seguire il loro consiglio e dare un'occhiata attorno al Campo."


translate italian day1_faf01b13:


    th "Nel frattempo cercherò di scoprire qualcosa."


translate italian day1_abe359aa:


    "Passando per il «quartiere residenziale», ho notato un ragazzo pioniere avvicinarsi nella mia direzione."


translate italian day1_19166f7e:


    "Ed era davvero un ragazzo pioniere, non una ragazza. A quanto pare ci sono ragazzi anche in questo regno di Amazzoni."


translate italian day1_6e392819:


    elp "Ciao, sei nuovo qui, tu devi essere Semyon, giusto?"


translate italian day1_f4234049:


    me "E come fai a…?"


translate italian day1_eb3ac837:


    elp "Lo sanno tutti ormai! Io sono Electronik, comunque. Quello vero. Puoi chiamarmi così."


translate italian day1_1ab8b0e8:


    th "Electronik. Quello vero. Le cose sono passate da «folli» a «totalmente fuori di testa»…"


translate italian day1_f2e007f0:


    me "Ok…"


translate italian day1_13475f83:


    el "Ulyana mi chiama anche Cheesie."


translate italian day1_f005cc88:


    me "Cheesie?"


translate italian day1_dd47efcd:


    th "Come il formaggio, sul toast coi funghi?"


translate italian day1_8966f24f:


    el "Perché il mio cognome è Cheesekov."


translate italian day1_f2e007f0_1:


    me "Aah…"


translate italian day1_ab73c55e:


    el "Vieni, ti mostro il Campo!"


translate italian day1_c45870d5:


    "Ho accettato la sua offerta, dato che avrei impiegato molto più tempo a conoscere questo luogo da solo."


translate italian day1_901746cb:


    me "Va bene, andiamo."


translate italian day1_e555f27e:


    "Siamo arrivati di nuovo alla piazza."


translate italian day1_6bd8e03f:


    th "Come se questa fosse l'unica attrazione in questo Campo..."


translate italian day1_ac68c5e6:


    "Lena stava seduta su una delle panchine, leggendo un libro.{w} Electronik le si è avvicinato fiducioso."


translate italian day1_03a7db73:


    el "Ciao, Lena! Ecco il nuovo arrivato, Semyon."


translate italian day1_ebd7111d:


    "Ha esordito vivacemente."


translate italian day1_f37aca17:


    me "Ciao.{w} Beh, si può dire che ci siamo già incontrati, in qualche modo."


translate italian day1_31b01f67:


    un "Sì…"


translate italian day1_51f9c6e8:


    "Ha distolto lo sguardo dal libro per un istante, mi ha guardato, è arrossita ed è tornata a leggere, come se ce ne fossimo già andati."


translate italian day1_0e6973ff:


    el "Va bene, proseguiamo."


translate italian day1_e71595cb:


    "All'inizio ero sorpreso del fatto che il mio «incontro» con quella ragazza fosse consistito solo in un paio di parole, ma forse era meglio così.{w} Il fare vivace di Electronik non era molto adatto a una ragazza timida come Lena."


translate italian day1_304c9144:


    me "Andiamo."


translate italian day1_d2c497e2:


    "Poco dopo siamo arrivati davanti a un edificio, che ho subito riconosciuto essere la mensa."


translate italian day1_ac073a3b:


    el "E quella…"


translate italian day1_e84fdadf:


    me "Lo so! Lì è dove si consuma il cibo organico!"


translate italian day1_06114188:


    el "Sì, qualcosa del genere…"


translate italian day1_4eab803f:


    "Sulla veranda c'era la ragazza poco amichevole che mi aveva colpito alla schiena prima."


translate italian day1_2cd75b17:


    "Dopo averla notata, la mia voglia di scherzare è sparita all'istante."


translate italian day1_2eef73b2:


    th "Davvero, non è il momento migliore per prendere in giro questo tizio, anche se lui è piuttosto divertente."


translate italian day1_88b653a6:


    th "Prima devo scoprire cosa sta accadendo qui, o almeno dove mi trovo!"


translate italian day1_84bfeb9f:


    el "Quella in fondo – è Alisa Dvachevskaya. Fa' attenzione quando sei vicino a lei."


translate italian day1_36dc9fcc:


    "Mi ha sussurrato."


translate italian day1_c065e234:


    el "Non osare chiamarla DvaChe, altrimenti son dolori!"


translate italian day1_242c3a55:


    dv "Che cosa hai detto? Come mi hai chiamata?"


translate italian day1_c941236c:


    "Deve averlo sentito."


translate italian day1_7cbd6d0d:


    "In un batter d'occhio Alisa è balzata giù dalla veranda e si è slanciata verso di noi."


translate italian day1_7810c210:


    el "Ok, adesso forse è meglio se continui da solo…"


translate italian day1_8a6dfaab:


    "Electronik se l'è data a gambe."


translate italian day1_68ae01db:


    "Non avevo intenzione di essere nuovamente vittima di quell'aggressiva Alisa e sono guizzato verso Electronik."


translate italian day1_87908dda:


    "Correndo verso la piazza l'ho perso di vista."


translate italian day1_f5ecd77a:


    "Ma nemmeno Dvachevskaya mi stava più inseguendo."


translate italian day1_efa7a730:


    th "Comunque non dovrei rischiare di chiamarla in quel modo.{w} Forse non dovrei nemmeno pensarlo..."


translate italian day1_d436642f:


    "Dopo aver ripreso fiato, mi sono chiesto perché io avessi reagito così."


translate italian day1_50af9809:


    th "D'accordo, è una ragazza... piuttosto aggressiva...{w} Ma perché sono scappato?"


translate italian day1_cd458724:


    "Non riuscendo a trovare una risposta, mi sono seduto su una panchina e ho iniziato a fissare il monumento di Genda."


translate italian day1_2d7b339e:


    "Alisa, che mi stava passando davanti, si è fermata per un istante e mi ha ringhiato:"


translate italian day1_1823fa4f:


    dv "Con te farò i conti più tardi!"


translate italian day1_35feebce:


    me "Fare i conti? Ma cosa ho fatto di male?!"


translate italian day1_7eff5e99:


    "Ho accompagnato le mie parole con un sorriso forzatamente colpevole."


translate italian day1_8fa3ff1e:


    th "Ma cosa ho fatto di male...?"


translate italian day1_9dbfd6f2:


    "Non mi ha risposto e ha continuato a inseguire Electronik."


translate italian day1_a20cefa7_7:


    "..."


translate italian day1_eb85f5b2:


    th "Sembra che dovrò ammazzare il tempo da solo, aspettando la cena."


translate italian day1_c1af6492:


    "Ho deciso di andare verso est.{w} O almeno, verso l'est del mio mondo."


translate italian day1_990e43f3:


    "Poco dopo, mi sono trovato davanti a un campo da calcio."


translate italian day1_55a5886e:


    "Una partita era in pieno svolgimento."


translate italian day1_6e3f39b7:


    th "Credo che non ci sia niente di male se guardo per un po'."


translate italian day1_80a62586:


    "Da bambino e da ragazzo ero abbastanza bravo con il pallone, avevo anche pensato di diventare un professionista, ma una serie di infortuni mi hanno fatto capire che non valeva la pena rischiare la salute per l'incertezza di un successo."


translate italian day1_ac5ae99c:


    "Ragazzi di varie età stavano correndo sul campo: ho visto un bambino di circa dieci anni e una ragazzina di circa quattordici anni…"


translate italian day1_f462954c:


    th "Una ragazzina…{w} Ehi, ma è Ulyana!"


translate italian day1_5e68b29c:


    th "Ok, quindi lei gioca a calcio – che c'è di strano?"


translate italian day1_1ae034aa:


    th "Sembra una persona irrequieta, dopotutto."


translate italian day1_0286167f:


    "Ero un po' lontano dal campo, ma lei mi ha notato lo stesso."


translate italian day1_f85a8374:


    us "Ehi, tu!"


translate italian day1_77bd833d:


    "Ha gridato Ulyana."


translate italian day1_afc02b59:


    us "Vuoi giocare?"


translate italian day1_3f27c2c0:


    "Non sapevo cosa risponderle."


translate italian day1_7fb785ba:


    th "Da un lato, correre per una decina di minuti non mi farebbe male."


translate italian day1_b2114442:


    th "Ma dall'altro, una mossa sbagliata potrebbe rivelarsi fatale nella mia situazione."


translate italian day1_be98ba5d:


    "In ogni caso i miei abiti non erano adatti a questo clima."


translate italian day1_0b1fdc5d:


    "Se avessi iniziato a giocare con questi scarponi e questi pantaloni, avrei sudato come un maiale."


translate italian day1_16639bbb:


    th "E giocare a piedi nudi senza pantaloni sarebbe semplicemente immorale."


translate italian day1_91f30c65:


    me "Magari un'altra volta, ok?"


translate italian day1_1cdbc40f:


    "Le ho gridato, voltandomi e tornando indietro."


translate italian day1_317df2d8:


    "Ulyana ha gridato qualcosa alle mie spalle, riguardo ai miei pantaloni, o riguardo al fatto che io fossi una checca, o qualcosa del genere..."


translate italian day1_a20cefa7_8:


    "..."


translate italian day1_4b9550bc:


    "Si stava facendo sera, e mi sentivo vuoto e stanco dopo una giornata trascorsa senza fare nulla di utile."


translate italian day1_a20cefa7_9:


    "..."


translate italian day1_2d76a94d:


    "Sono tornato alla piazza, mi sono seduto su una panchina, e ho sospirato, esausto."


translate italian day1_c7f14958:


    th "È meglio se aspetto qui per la cena.{w} Dopotutto, è più facile cercare risposte quando non si ha fame."


translate italian day1_6face009:


    th "Danno da mangiare qui, giusto...?"


translate italian day1_6efc8181:


    th "È curioso come i più semplici bisogni umani possano annullare la volontà di riflettere sulle cose, di lottare per qualcosa."


translate italian day1_8098381c:


    th "Per esempio, adesso ho fame, e quindi non m'importa molto di dove io mi trovi o di cosa mi stia accadendo."


translate italian day1_8e0062ff:


    th "Mi chiedo se la stessa cosa valga anche per le grandi persone..."


translate italian day1_11ca5f98:


    th "E in tal caso, Spartaco come avrebbe potuto iniziare la rivolta degli schiavi nei tempi antichi...?"


translate italian day1_6d0083af:


    th "Quindi posso solo concludere che io non sono una grande persona, e non importa di quale meccanismo io faccia parte: la società, Matrix, o uno strano campo di pionieri."


translate italian day1_31836b32:


    "I miei pensieri sono stati interrotti dal suono proveniente dall'altoparlante situato su un palo della luce."


translate italian day1_bbaa6f9b:


    th "Dev'essere il segnale che indica la cena!"


translate italian day1_d1afa489:


    "Mi sono incamminato verso la mensa – per fortuna sapevo già dov'era."


translate italian day1_60f3a365:


    "Olga Dmitrievna stava lì, in piedi sulla veranda."


translate italian day1_3181ce0b:


    "Mi sono fermato e l'ho guardata attentamente, come se mi aspettassi qualcosa da lei."


translate italian day1_620995d5:


    "Si è voltata verso di me per un istante, ma alla fine si è avvicinata."


translate italian day1_e4e46775:


    mt "Semyon, che cosa stai aspettando? Entriamo!"


translate italian day1_3db38509:


    th "Non dovrebbe succedere nulla di brutto se vado con lei."


translate italian day1_8646eca1:


    "Il mio stomaco ha approvato."


translate italian day1_20d3b7df:


    "Siamo entrati."


translate italian day1_d5cccf64:


    "La mensa sembrava proprio…{w} una mensa."


translate italian day1_5a204f67:


    "In passato ho avuto occasione di frequentare una mensa di fabbrica…{w} Questa era esattamente uguale, tranne forse un po' più moderna e pulita."


translate italian day1_7f269d4e:


    "Sedie e tavoli in metallo, piastrelle smaltate sulle pareti e sul pavimento, piatti e posate poco sofisticati che si rompono occasionalmente."


translate italian day1_2b4a4a89:


    th "Suppongo che sia così che dev'essere una mensa in un campo di pionieri."


translate italian day1_865ed000:


    mt "Semyon, aspetta, ti troviamo un posto dove sederti…"


translate italian day1_fed0ccad:


    "Si è guardata attorno."


translate italian day1_cbc89c54:


    mt "Dvachevskaya, ferma dove sei!"


translate italian day1_bccd2e3b:


    "Ha gridato Olga Dmitrievna ad Alisa che stava passando lì vicino."


translate italian day1_e0ebb201:


    dv "Che c'è?"


translate italian day1_686395a7:


    mt "Cos'hai fatto alla tua divisa?"


translate italian day1_f893781b:


    dv "Perché, c'è qualcosa che non va?"


translate italian day1_6c59a28a:


    "In effetti, il modo in cui la indossava era piuttosto provocante…"


translate italian day1_7f4c6c16:


    mt "Metti a posto quella camicia, subito!"


translate italian day1_aa5269ba:


    dv "Va bene, va bene…"


translate italian day1_02da0b4a:


    "Alisa si è rimessa a posto la camicia e se n'è andata, lanciandomi uno sguardo spiacevole."


translate italian day1_a1e6af5d:


    mt "Vediamo, dove potresti sederti?"


translate italian day1_30ea3e74:


    "Non c'erano molti posti liberi."


translate italian day1_986412cf:


    mt "Siediti là, vicino a Ulyana!"


translate italian day1_2cfe56a5:


    me "Ehm… Forse io…"


translate italian day1_385a7396:


    mt "Sì, è perfetto, c'è già anche il piatto pronto!"


translate italian day1_07918b6c:


    "Ho dovuto accettare controvoglia."


translate italian day1_ff014804:


    "Ovviamente, avrebbero potuto avvelenare le cotolette con il curaro, il purè di patate avrebbe potuto essere generosamente condito con l'arsenico, e il bicchiere riempito con un antigelo di ottima qualità invece del kompot…"


translate italian day1_1dcade22:


    "Ma era tutto così invitante che non ho resistito!"


translate italian day1_d5d98923:


    us "Ehi!"


translate italian day1_087d8e36:


    me "Che vuoi?"


translate italian day1_4b969ae9:


    "Ho risposto piuttosto sgarbatamente a Ulyana, che stava seduta accanto a me."


translate italian day1_bcac0437:


    us "Perché non volevi giocare a calcio con noi?"


translate italian day1_ee0ece72:


    me "A causa dei miei vestiti."


translate italian day1_ac6bee46:


    "Le ho risposto, indicando la fonte del problema."


translate italian day1_762f9dc6:


    us "Oh, va bene allora, mangia."


translate italian day1_71c21715:


    "Tuttavia, non era rimasto molto da mangiare: la mia cotoletta era sparita dal piatto!"


translate italian day1_2623f571:


    th "Sicuramente è stata lei."


translate italian day1_f9c55ec5:


    th "No, più precisamente, nessuno a parte Ulyana avrebbe potuto farlo!"


translate italian day1_58e7978c:


    me "Ridammi la cotoletta!"


translate italian day1_0f31d85b:


    us "In una grande famiglia, chi dorme non piglia pesci!{w} Può costarti una cotoletta se non fai attenzione!"


translate italian day1_ea7db8e5:


    me "Ridammela, ti ho detto!"


translate italian day1_3e16ffc9:


    "L'ho guardata minacciosamente e stavo per allungare la mano..."


translate italian day1_39e0f78f:


    us "Visto, non ce l'ho!"


translate italian day1_8fb93759:


    "Ed effettivamente il piatto di Ulyana era vuoto; sembrava che quella ragazzina mangiasse più in fretta di quanto riuscisse a rubare le cotolette degli altri."


translate italian day1_bf4d7f64:


    us "Non preoccuparti, ci penso io!"


translate italian day1_c2895463:


    "Ha afferrato il mio piatto ed è sparita."


translate italian day1_4c2a1209:


    "Non c'era motivo di seguirla: se mi avessero voluto avvelenare l'avrebbero fatto in modo molto più semplice."


translate italian day1_4bb06742:


    "Dopo circa un minuto, Ulyana è tornata e mi ha ridato il piatto, con sopra una bella cotoletta fumante."


translate italian day1_39cd2cfa:


    us "Ecco qua, per l'affamato!"


translate italian day1_73aab3cd:


    me "Grazie…"


translate italian day1_dfe4386e:


    "Era tutto quello che potevo dire."


translate italian day1_e425e9ee:


    "Ero così affamato che i miei sospetti sono svaniti in un istante.{w} Ho sollevato la cotoletta con la mia forchetta e…"


translate italian day1_2158fc2f:


    th "Ma che?!{w} Un verme!{w} Anzi, non un verme!{w} Un millepiedi!{w} Con tutte quelle zampette che si muovono!"


translate italian day1_27e62654:


    "Il piatto è volato per terra frantumandosi in mille pezzi; la sedia è caduta colpendomi forte sulla gamba."


translate italian day1_c1ea30fa:


    "Gli insetti non mi sono mai piaciuti sin da bambino, ma vedere quelle viscide zampette muoversi nel mio piatto – è decisamente troppo!"


translate italian day1_670b85c3:


    me "Brutta piccola…"


translate italian day1_cdba60b2:


    "Ulyana sembrava aver già previsto tutto e stava già davanti alla porta, ridendo come se le avessero appena raccontato una fresca barzelletta molto divertente."


translate italian day1_a0076c62:


    "Mi sono piombato verso di lei."


translate italian day1_4a7f4c33:


    "L'ho rincorsa fuori dalla mensa."


translate italian day1_51f77987:


    "Solo qualche decina di metri ci separava, ed ero sicuro che l'avrei raggiunta facilmente."


translate italian day1_c0e0bceb:


    "Abbiamo attraversato la piazza…"


translate italian day1_8d3b0cc2:


    "Poi siamo passati davanti al circolo…"


translate italian day1_f7567198:


    "E abbiamo imboccato il sentiero della foresta."


translate italian day1_d50859c6:


    "Ho cominciato a boccheggiare per riprendere fiato."


translate italian day1_80f73680:


    th "Forse dovrei smettere di fumare…"


translate italian day1_1178684b:


    "Al prossimo angolo l'ho persa di vista."


translate italian day1_b24a1f41:


    th "Non è possibile che sia riuscita a sfuggirmi!"


translate italian day1_c255b412:


    th "Non ci credo!"


translate italian day1_7a8d7835:


    "Mi sono fermato, cercando di riprendere fiato nuovamente."


translate italian day1_a20cefa7_10:


    "..."


translate italian day1_fd3eb8d2:


    "Stava calando la notte."


translate italian day1_b570941e:


    th "Devo essermi perso…"


translate italian day1_bce9bc24:


    th "Non è una buona idea restare nel bosco di notte – è meglio se torno al Campo."


translate italian day1_bbe1aefc:


    "Ma non avevo idea di come arrivarci."


translate italian day1_985a9376:


    th "Beh, dovrò andare a caso."


translate italian day1_a20cefa7_11:


    "..."


translate italian day1_a20cefa7_12:


    "..."


translate italian day1_f8583ab1:


    "Ho vagato per un bel po' di tempo nella foresta, ho anche pensato di gridare aiuto, ma alla fine ho intravisto la recinzione del Campo oltre gli alberi."


translate italian day1_7ad8ae02:


    th "Tutto si è rimesso a posto."


translate italian day1_7f4df294:


    me "L'autobus è sparito..."


translate italian day1_6cfccad8:


    "Ho bisbigliato in silenzio."


translate italian day1_dbfa4d22:


    "Dopotutto, non c'era nulla di strano in ciò: quell'autobus non poteva rimanere lì per sempre."


translate italian day1_42ed0653:


    "Ma pensandoci bene, questo significava che ci doveva essere qualcuno al volante, perché i bus non si guidano da soli!"


translate italian day1_dcc5269b:


    th "O forse sì?"


translate italian day1_ba1dfa38:


    "Questo mondo sembrava troppo normale, ma ogni evento qui poteva avere almeno due spiegazioni: una ordinaria, reale, logica, e l'altra surreale."


translate italian day1_b678b61b:


    "Certo, potrebbe anche essere che il conducente fosse andato a fare uno spuntino, e che io fossi sceso troppo presto, ecco spiegato tutto..."


translate italian day1_ec1d1437:


    th "Ma in ogni caso, non appartengo a questo luogo!"


translate italian day1_9aa1fa90:


    "Scoprire se quel bus si potesse guidare da solo era importante, ma lo era molto di più il sapere perché io fossi finito qui."


translate italian day1_2b9ab305:


    "Ma dov'era questo {i}qui{/i}..."


translate italian day1_5aa31516:


    "I campi e i boschi, che si estendevano fino all'orizzonte, non mi davano alcuna risposta; in essi non vedevo nulla di familiare."


translate italian day1_a20cefa7_13:


    "..."


translate italian day1_13255b43:


    "Uno strano ed incomprensibile mondo alieno.{w} Ma allo stesso tempo non era assolutamente spaventoso."


translate italian day1_c83e85c5:


    "O il mio istinto di autoconservazione aveva deciso di abbandonarmi, o tutto quel correre per il Campo e i suoi abitanti mi avevano cullato talmente tanto con la loro spensierata normalità, che mi stavo dimenticando di cosa mi era accaduto solo un paio di ore fa."


translate italian day1_e9c8c9dc:


    "Anche se forse non mi erano rimaste più energie per preoccuparmi."


translate italian day1_ab64ceb2:


    "Tutto quello che desideravo era un po' di calma e tranquillità; volevo solo avere un momento di pausa da tutto questo, e solo in seguito continuare la mia ricerca di risposte, pronto e carico."


translate italian day1_c260705c:


    "Tuttavia, dovevo rimandare quella cosa..."


translate italian day1_5cfa0252:


    th "E adesso? Posso permettermi di rilassarmi un po'?"


translate italian day1_a20cefa7_14:


    "..."


translate italian day1_b2e9fb9d:


    "Si era fatto davvero buio, e in ogni caso sarebbe stato meglio trascorrere la notte nel Campo."


translate italian day1_46aa5cbc:


    "Stavo per tornare indietro quando qualcuno si è avvicinato silenziosamente alle mie spalle."


translate italian day1_ac5eb104:


    sl "Ciao, che ci fai qui a quest'ora?"


translate italian day1_51149e68_2:


    me "…"


translate italian day1_245b70c4:


    "Era Slavya.{w} Mi ha talmente colto di sorpresa che ho trasalito."


translate italian day1_ec2df0cc:


    sl "E così non sei riuscito ad acchiappare Ulyana, vero?"


translate italian day1_d1046a49:


    "Ha sorriso."


translate italian day1_5e1fc6b8:


    "Ho annuito con disappunto, sospirando."


translate italian day1_dd08f845:


    sl "Non mi stupisce.{w} Nessuno ci è mai riuscito."


translate italian day1_476d94f1:


    th "Già, è proprio una scheggia.{w} Potrebbe impiegare le sue energie per qualcosa di meglio che cercare avventure…"


translate italian day1_d1722b3c:


    sl "Devi avere fame, dopotutto non sei riuscito a mangiare oggi…"


translate italian day1_d0dae46d:


    "In effetti mi ero totalmente scordato di quanto fossi affamato, ma dopo le sue parole il mio stomaco ha attirato l'attenzione su di sé brontolando a tradimento."


translate italian day1_c17a722d:


    "Slavya ha sorriso."


translate italian day1_4cbb01a1:


    sl "Dai, andiamo."


translate italian day1_e01dee92:


    me "Ma come, la mensa è ancora aperta a quest'ora?"


translate italian day1_40c17de2:


    sl "Non preoccuparti, ho le chiavi."


translate italian day1_36955eb7:


    me "Le chiavi?"


translate italian day1_ab83b32e:


    sl "Sì, ho le chiavi di tutti gli edifici di questo Campo."


translate italian day1_9080c90e:


    me "E perché?"


translate italian day1_ca97714d:


    sl "Beh, sono una specie di aiutante della leader del Campo qui."


translate italian day1_7e4b1b8c:


    me "Capisco.{w} Va bene, andiamo."


translate italian day1_63a3eb4c:


    "Era un'offerta che non potevo rifiutare."


translate italian day1_00f9254f:


    "Appena arrivati alla piazza, Slavya si è fermata."


translate italian day1_7f5d338c:


    sl "Scusa un attimo, è meglio se avviso la mia compagna di stanza che farò tardi – lei è così puntuale che inizierebbe a preoccuparsi se non mi vedesse tornare."


translate italian day1_518df0ed:


    sl "Tu intanto va' alla mensa, ti raggiungo subito, ok?"


translate italian day1_505d91ae:


    me "Ok…"


translate italian day1_32ac72cc:


    "Non mi sarei mai aspettato di trovare qualcuno qui a quest'ora."


translate italian day1_b6cef34a:


    "E quel qualcuno sembrava stesse cercando disperatamente di aprire la porta."


translate italian day1_18f36ecf:


    "Senza pensieri segreti, mi sono avvicinato alla veranda."


translate italian day1_d706aa5c:


    "Lo scassinatore si è rivelato essere Alisa."


translate italian day1_e81739e5:


    th "Probabilmente avrei fatto meglio a starmene alla larga..."


translate italian day1_bbe1ef0f:


    "Mi ha guardato intensamente per un po', e poi mi ha detto:"


translate italian day1_a872d369:


    dv "Non stare lì impalato, dammi una mano!"


translate italian day1_e54b39e9:


    me "Cioè?"


translate italian day1_9736714c:


    dv "Aiutami ad aprire la porta!"


translate italian day1_7332342b:


    me "E perché?"


translate italian day1_c67591a7:


    dv "Perché voglio dei panini… e del kefir! La cena non era abbastanza!"


translate italian day1_8d3f7a19:


    me "Hmmm…{w} Credi davvero che sia una buona idea?"


translate italian day1_1381f7ca:


    dv "Perché, tu non hai fame, eh?{w} Ulyana ti ha rovinato la cena, non è così?"


translate italian day1_f396e777:


    "Ha sorriso con sarcasmo."


translate italian day1_ad73a68c:


    th "È vero, me l'ha rovinata..."


translate italian day1_c43ed392:


    me "Non c'è problema, fra poco arriva Slavya e…"


translate italian day1_c8b0074b:


    dv "COSA?!"


translate italian day1_ca8046cd:


    th "Forse avrei fatto meglio a tenere la bocca chiusa."


translate italian day1_e7b745f9:


    dv "Io me ne vado!"


translate italian day1_5f47ed77:


    dv "E tu me la pagherai per questo!{w} È già la seconda che mi devi!"


translate italian day1_a180b25b:


    "Dopo aver detto ciò, Alisa è scomparsa nella notte."


translate italian day1_efb5548d:


    th "E quale sarebbe la prima?"


translate italian day1_a20cefa7_15:


    "..."


translate italian day1_696c481c:


    "Slavya non si è fatta attendere troppo a lungo."


translate italian day1_8c608cb1:


    sl "Tutto a posto?"


translate italian day1_d5cf1edd:


    me "Sì, perché me lo chiedi?"


translate italian day1_18938c76:


    sl "No, niente, così."


translate italian day1_dc129c74:


    "Forse è meglio non dirle di Alisa."


translate italian day1_b5544ae3:


    me "È tutto a posto."


translate italian day1_944a6c8e:


    "Il mio tono era poco sincero."


translate italian day1_6d7067b9:


    sl "Bene, andiamo?"


translate italian day1_3792d81a:


    "Ma Slavya sembrava non aver notato nulla di strano.{w} O almeno faceva finta di non averlo notato."


translate italian day1_44f2c4ea:


    "Siamo entrati nella mensa."


translate italian day1_c8f556b6:


    sl "Aspetta, vado a prendere qualcosa da mangiare."


translate italian day1_d09b3412:


    "Mi sono seduto, aspettando con obbedienza il mio salvatore."


translate italian day1_8775f75b:


    "La mia cena era semplice: un paio di panini e un bicchiere di kefir."


translate italian day1_c6c983c8:


    th "Non mi stupisce: i pionieri affamati devono aver lasciato solo le briciole."


translate italian day1_37e90611:


    "Era comunque molto meglio di quello che mangio solitamente."


translate italian day1_d5866868:


    "Slavya si è seduta dall'altra parte del tavolo, guardandomi mangiare."


translate italian day1_fbb73cfa:


    me "Ho qualcosa sul viso?"


translate italian day1_178aa33f:


    sl "No, è solo che…"


translate italian day1_d1046a49_1:


    "Mi ha sorriso."


translate italian day1_dc9c1b81:


    sl "Allora, com'è stato il tuo primo giorno nel Campo?"


translate italian day1_6a05d51d:


    me "Beh, non saprei..."


translate italian day1_b12cf159:


    "È un po' sciocco chiedere a qualcuno che all'improvviso si è ritrovato in un'altra realtà se gli è piaciuto il cibo della mensa, la leader del Campo, o la camera che gli è stata assegnata."


translate italian day1_46db5ea6:


    sl "Non preoccuparti, tra poco ti abituerai!"


translate italian day1_68e00554:


    "Slavya ha guardato fuori dalla finestra con aria sognante."


translate italian day1_b05e0e4e:


    th "Ad essere sincero, non ho alcuna intenzione di abituarmi a tutto questo, ma lei non lo sa..."


translate italian day1_b9ffb640:


    th "O almeno vuole farmi credere che lei non lo sappia."


translate italian day1_2c50ec15:


    me "Beh, tutto sommato, non è poi così male qui."


translate italian day1_443c7288:


    "Ho dovuto rompere in qualche modo quel silenzio imbarazzante."


translate italian day1_17d13602:


    sl "Lo credi davvero?"


translate italian day1_a271f10e:


    "Mi ha domandato, disinteressata."


translate italian day1_1b986acc:


    me "Sì. Questo posto è così..."


translate italian day1_d4bc890b:


    "Stavo per dire «rétro», ma mi sono fermato."


translate italian day1_edbbb7b2:


    "Dopotutto, era rétro per me, ma per loro invece?{w} Forse era l'unico stile di vita che conoscevano."


translate italian day1_78a8ca85:


    "Sempre che il termine {i}vita{/i} avesse un senso qui..."


translate italian day1_789035fa:


    sl "Così come?"


translate italian day1_00577787:


    "Mi ha guardato da vicino.{w} Come se qualcosa di importante dipendesse dalla mia risposta."


translate italian day1_7c2d154d:


    me "Beh, non saprei... carino. Già! È carino qui."


translate italian day1_c1307b0b:


    sl "Suppongo di sì."


translate italian day1_78a2fe3f:


    "Ha sorriso di nuovo."


translate italian day1_e349590f:


    sl "Sono contenta che la pensi così."


translate italian day1_5ac38cf1_1:


    me "Perché?"


translate italian day1_17e99afb:


    sl "Beh, non a tutti piace stare qui..."


translate italian day1_1a377d44:


    me "E a te invece?"


translate italian day1_1995267f:


    sl "A me?"


translate italian day1_fc30fd2b:


    me "Sì."


translate italian day1_031c751c:


    sl "Adoro stare qui, è stupendo."


translate italian day1_5c30029f:


    me "Allora non devi preoccuparti di cosa pensano gli altri."


translate italian day1_879638a9:


    sl "Beh, a dire il vero non mi preoccupo."


translate italian day1_c954d69f:


    "Ha detto ridendo."


translate italian day1_72c6e2b1:


    "Questa conversazione stava prendendo una svolta inaspettata."


translate italian day1_1a712a21:


    sl "E tu invece sembri preoccupato..."


translate italian day1_1d09f697:


    me "Davvero? Perché lo pensi?"


translate italian day1_8b1f703e:


    sl "Beh, quando qualcuno mastica così intensamente..."


translate italian day1_07129b8a:


    me "Oh, scusa."


translate italian day1_705bea28:


    sl "Fa niente."


translate italian day1_02cdc4c3:


    "Non riuscivo a essere più prudente con questa ragazza."


translate italian day1_ded79e77:


    "Ma perché proprio con lei? Perché non con un qualsiasi altro abitante di questo luogo?"


translate italian day1_e53b428c:


    "Tutti loro mi sembravano totalmente normali.{w} Davvero {i}normali{/i}, talmente normali da farmi venire i brividi lungo la schiena e fino al midollo!"


translate italian day1_51e2809e:


    "Intendo normali, non come un vicino di casa con un trapano in una mano e un subwoofer nell'altra."


translate italian day1_ab3da39e:


    "Non come gli sconosciuti che spesso si incontrano nelle metropolitane o sui mezzi pubblici."


translate italian day1_15d26ee0:


    "Non come il collega di un ufficio open-space."


translate italian day1_29ee93d2:


    "E nemmeno come un amico che differisce da chiunque altro solo per la sua costante insistenza."


translate italian day1_82012fff:


    "Tutti loro mi sembravano {b}normali{/b} – come mi aspettavo che fossero – con i loro difetti ma senza alcun superpotere."


translate italian day1_c821ace7:


    "E Slavya era anche...{w} carina?"


translate italian day1_7f68049d:


    "L'ho guardata furtivamente, non sapendo cosa dire."


translate italian day1_1ec8deab:


    sl "Perdonami, avrei voluto mostrarti il Campo ma ero molto impegnata oggi."


translate italian day1_567b2330:


    me "Io... credo di aver già visto tutto, suppongo."


translate italian day1_2e601a49:


    sl "Sei sicuro di aver visto proprio tutto?"


translate italian day1_efd47648:


    "Mi ha sorriso così intensamente che ho dovuto distogliere lo sguardo per l'imbarazzo."


translate italian day1_cdbd4a1c:


    me "Beh, non saprei – è il mio primo giorno qui."


translate italian day1_122de170:


    sl "Ok, allora cos'hai visto finora?"


translate italian day1_875830b6:


    me "Ho visto la piazza, la mensa, il campo da calcio..."


translate italian day1_76985cc0:


    sl "E la spiaggia?"


translate italian day1_b11fae54:


    me "Solo da lontano."


translate italian day1_f5ac888c:


    sl "Dovresti proprio andarci! Anzi, dovremmo andarci insieme!"


translate italian day1_d5688542:


    me "Va bene, ok... Possiamo andarci..."


translate italian day1_446ce20c:


    "La sua naturalezza mi stava facendo preoccupare, ma poi ho pensato – e se tutto quello che stava accadendo fosse come dovrebbe essere, e se questo mondo apparisse strano soltanto ai miei occhi, mentre per loro fosse...{w} normale?"


translate italian day1_6578880f:


    "Forse sono stato catapultato nel passato...?"


translate italian day1_5410f15c:


    "Sì, ciò spiegherebbe molte cose."


translate italian day1_aad73e6b:


    me "Posso farti una domanda?"


translate italian day1_fadcaef0:


    sl "No."


translate italian day1_7b5c12bd:


    "Slavya ha sorriso e si è alzata dal tavolo."


translate italian day1_c5c451d5:


    sl "È tardi... Ce la fai a tornare da Olga Dmitrievna da solo?"


translate italian day1_9fef43a7:


    me "Credo di sì, ma perché dovrei andarci?"


translate italian day1_821bd937:


    sl "Ti troverà una sistemazione."


translate italian day1_7332342b_1:


    me "Perché?"


translate italian day1_bfddcddb:


    "Probabilmente la mia domanda era stupida, perché Slavya è scoppiata in una risata bonaria."


translate italian day1_c070ca19:


    sl "Dovrai pur dormire da qualche parte, no?"


translate italian day1_4405a6cf:


    me "In effetti..."


translate italian day1_2455141e:


    sl "Bene allora, io vado.{w} Buona notte!"


translate italian day1_0a0db17f:


    me "Notte..."


translate italian day1_ca407fc5:


    th "È strano che se ne sia andata così di fretta..."


translate italian day1_c6b742a3:


    "Un mazzo di chiavi lasciato nella porta ha catturato la mia attenzione."


translate italian day1_08ccb5b7:


    "Avrei voluto riportarle a Slavya, ma dove abitava?"


translate italian day1_3aa2ad8d:


    "Bussare a ogni porta nel cuore della notte non era una grande idea."


translate italian day1_9356c1f1:


    th "È meglio se le prendo – gliele darò domani, perché chissà cosa può succedere qui di notte."


translate italian day1_56811613:


    "Questi pensieri mi hanno dato i brividi – ero io quello che doveva fare attenzione qui."


translate italian day1_5802aa2b:


    th "Ma d'altra parte, a cosa mi servono..."


translate italian day1_0eb5a1f8:


    "La notte era buia ma non silenziosa – si poteva sentire il concerto dei grilli, le melodie degli uccelli notturni e il fruscio degli alberi ovunque."


translate italian day1_ee63d9fd:


    "Ho avuto un improvviso desiderio di seguire il consiglio di Slavya e di andare alla casetta della leader del Campo."


translate italian day1_b358634e:


    "Non saprei, ma la vista dello sconosciuto portavoce del comunismo mi ha messo di umore costruttivo."


translate italian day1_e66f685d:


    "Mi sono seduto sulla panchina e ho iniziato a pensare a tutto quello che mi era accaduto oggi.{w} Era tutto ciò che il mio umore costruttivo aveva da offrirmi."


translate italian day1_2719b81b:


    "Qui era molto più luminoso rispetto alla mensa, e alcuni pionieri ritardatari stavano correndo qua e là, quindi questo luogo non era per nulla spaventoso."


translate italian day1_2dc0eb74:


    th "L'autobus, il Campo, le ragazze…"


translate italian day1_3aff8100:


    "Ero talmente stanco di tutte queste cose nuove e strane che non sono riuscito a trovare alcuna spiegazione per tutto ciò."


translate italian day1_4caea1ce:


    "Poi ho sentito un fruscio appena percettibile nelle vicinanze."


translate italian day1_f2d26be1:


    "Ho rabbrividito e ho guardato in quella direzione."


translate italian day1_6993b49f:


    th "Una ragazza.{w} Che sta leggendo un libro."


translate italian day1_2807e1fc:


    th "Lena."


translate italian day1_1bdc01a9:


    "Ho deciso di avvicinarmi per parlare con lei."


translate italian day1_7ccf917f:


    "Di tutte quelle che avevo incontrato, lei è stata l'unica con la quale non ero riuscito a parlare decentemente."


translate italian day1_7b241855:


    me "Ciao, cosa stai leggendo?"


translate italian day1_ac772bf2:


    "Lena era così sorpresa che ha sobbalzato."


translate italian day1_ea263cee:


    me "Scusa, non volevo spaventarti!"


translate italian day1_62592cb0:


    un "Fa niente…"


translate italian day1_ad42b83a:


    "È arrossita ed è tornata a leggere."


translate italian day1_c43d3dfb:


    me "E quindi, cosa stai leggendo?"


translate italian day1_f3297c42:


    "Sulla copertina c'era scritto «Via col vento»."


translate italian day1_53009119:


    me "È un bel libro…"


translate italian day1_626232a6:


    un "Grazie."


translate italian day1_87f26837:


    th "Ad essere sincero non l'ho letto, ma credo che quel tipo di letteratura le si addica molto."


translate italian day1_21b3e47f:


    "Lena non sembrava essere interessata a continuare la nostra conversazione."


translate italian day1_f85b2b40:


    me "Beh, se ti sto disturbando..."


translate italian day1_049b021f:


    un "No."


translate italian day1_62091b7c:


    "Mi ha risposto, continuando a fissare il suo libro."


translate italian day1_13ee5d36:


    me "Ti spiace se mi siedo accanto a te per un po'?"


translate italian day1_41cfffd5:


    un "Perché?"


translate italian day1_e92b6e6e:


    th "Davvero, perché...?"


translate italian day1_a0c74c5a:


    "Forse perché ero molto stanco e lo stare in compagnia era meglio che stare da solo."


translate italian day1_bf12dc37:


    "E forse perché volevo scoprire qualcosa da lei."


translate italian day1_0864712b:


    "Ho esaminato Lena con attenzione."


translate italian day1_07aeaaac:


    th "Ma non credo che otterrò delle risposte..."


translate italian day1_3102ef10:


    me "Beh, non saprei... Non posso?"


translate italian day1_3b683375:


    un "Se vuoi..."


translate italian day1_d783747c:


    me "Ma se ti sto disturbando..."


translate italian day1_ab09f879:


    un "No, non mi stai disturbando."


translate italian day1_858edfda:


    me "Posso andarmene, basta dirmelo..."


translate italian day1_0d42a600:


    un "È tutto a posto."


translate italian day1_f517812c:


    me "Va bene allora..."


translate italian day1_98f71c0d:


    "Mi sono seduto cautamente all'estremità della panchina."


translate italian day1_0fcded02:


    "Dopo una chiacchierata così intensa, stare qui era l'ultima cosa che volevo, ma sarebbe stato scortese andarsene proprio adesso."


translate italian day1_61ec364d:


    me "Non è iniziata molto bene, eh?"


translate italian day1_cf8bd28b:


    "Lena non ha risposto."


translate italian day1_8caec5df:


    "Devo aver fatto la figura dell'idiota."


translate italian day1_60dd740c:


    "Scommetto che Ulyana si sarebbe fatta una bella risata se mi avesse visto adesso."


translate italian day1_e1927099:


    me "Ti piace stare qui?"


translate italian day1_610b2212:


    "Mi sono ricordato della domanda di Slavya e ho pensato che fosse un buon argomento per iniziare una conversazione."


translate italian day1_f1be5fed:


    un "Sì."


translate italian day1_783cf09d:


    "Mi ha risposto, sorridendo lievemente."


translate italian day1_23729c98:


    me "Anche a me, credo..."


translate italian day1_b5a17bee:


    "Lena era decisamente poco socievole e probabilmente non era in grado di portare avanti una conversazione insignificante come sapeva fare Slavya."


translate italian day1_141ec821:


    "Ma c'era qualcosa in lei che mi attraeva."


translate italian day1_f8e0c3e7:


    "Come un istantaneo riflesso di luce in una piovosa serata autunnale, che ti fa voltare e fissare l'oscurità in cerca di quel qualcosa che hai visto con la coda dell'occhio."


translate italian day1_04705c56:


    "Ovviamente non puoi distinguerlo o capire cosa sia, ma è comunque così allettante..."


translate italian day1_9ee89722:


    "Lena intanto continuava a leggere il suo libro, senza preoccuparsi della mia presenza."


translate italian day1_62582949:


    "E io non avevo intenzione di chiederle di questo Campo o di questo mondo in generale."


translate italian day1_a1e73db2:


    me "Una notte bellissima..."


translate italian day1_5ef3d7d4:


    un "Sì..."


translate italian day1_76d86f28:


    th "Ma come diavolo si potrebbe iniziare una conversazione con lei?"


translate italian day1_69ef7bda:


    un "Si è fatto tardi, devo andare…"


translate italian day1_45544479:


    me "Sì, è piuttosto tardi…"


translate italian day1_2629037f:


    un "Buona notte."


translate italian day1_fab13f1e:


    me "Notte…"


translate italian day1_e5d71cd0:


    "C'era qualcosa di strano in quella ragazza."


translate italian day1_482011b5:


    th "A prima vista è una tipica ragazza timida e riservata, ma…"


translate italian day1_efa2bb54:


    "Il mistero di Lena si è aggiunto all'infinita lista di stranezze riguardanti questo Campo, che si stavano accumulando nella mia testa."


translate italian day1_a20cefa7_16:


    "..."


translate italian day1_047c6176:


    th "È tardi ormai, e non ho niente da fare…"


translate italian day1_8b243a91:


    "Mi sono incamminato verso la casetta di Olga Dmitrievna."


translate italian day1_76db5439:


    "La luce all'interno era ancora accesa."


translate italian day1_01c690bf:


    mt "Ciao, Semyon!{w} Sei piuttosto in ritardo!"


translate italian day1_ccd4c421:


    me "Già…{w} Sono andato a fare un giro intorno al Campo."


translate italian day1_6e6b09c8:


    mt "Va bene.{w} Dormirai qui."


translate italian day1_d0ba333c:


    "Ha puntato il dito verso uno dei letti."


translate italian day1_54ddf89e:


    me "Proprio qui...?"


translate italian day1_07acf34e:


    "Sono rimasto un po' sorpreso."


translate italian day1_0cc846e0:


    mt "Sì, c'è qualcosa che non va?{w} Non abbiamo comunque altre camere libere."


translate italian day1_e5c2c901:


    "La leader del Campo mi ha fatto un sorriso, ma credo che l'avesse fatto solo per gentilezza."


translate italian day1_e6f03e6e:


    mt "Vuoi essere un pioniere decente, non è così?"


translate italian day1_6d7c696f:


    "Mi ha chiesto, sottolineando la parola «decente»."


translate italian day1_fa9f2c55:


    me "Sì… Certo…"


translate italian day1_c2a35634:


    "Mi sono perso nei miei pensieri per un attimo."


translate italian day1_8edc3e00:


    me "Le spiace, mademoiselle?"


translate italian day1_d642ee60:


    "Mi ha guardato in modo strano.{w} Un po' sorpresa e un po' offesa."


translate italian day1_a63ad949:


    mt "Un pioniere dovrebbe rispettare i suoi superiori!"


translate italian day1_8492f5e5:


    "Mi ha detto severamente."


translate italian day1_4adb9df1:


    me "Ma certo, nessuno ha detto il contrario…"


translate italian day1_343b3a4f:


    "Ho blaterato, non riuscendo a capire cosa avessi detto di male."


translate italian day1_1c5c9771:


    mt "Non dovresti anche...?"


translate italian day1_8fb93608:


    "Mi ha fissato negli occhi."


translate italian day1_4b041b53:


    "Quel suo sguardo avrebbe potuto sciogliere persino la più solida delle lame forgiate dal miglior maestro fabbro nanico nelle profondità sotterranee."


translate italian day1_8e8da635:


    me "Dovrei cosa?{w} Che c'è, tesoro?"


translate italian day1_50297304:


    "L'irritazione e l'incomprensione mi hanno fatto alzare la voce."


translate italian day1_20f9f9be:


    mt "Devi mostrare rispetto nei confronti degli adulti!"


translate italian day1_0bdad8e7:


    th "Sì, ci sono decisamente un sacco di cose strane qui…{w} Ma questa ragazza ha solo qualche anno più di me…"


translate italian day1_d2336ee2:


    th "O forse è anche più giovane..."


translate italian day1_c447246d:


    "Ma ho deciso di non discutere con lei – anche se un momento fa non l'avrei mai considerata un'adulta, devo ammettere che aveva un bel caratterino."


translate italian day1_6a31f527:


    "E in ogni caso non mi trovavo nella posizione giusta per obiettare."


translate italian day1_28205106:


    me "Come desidera… signora."


translate italian day1_9fde8ff9:


    "Ho borbottato."


translate italian day1_c0fad5bd:


    mt "Così va molto meglio!{w} È così che un pioniere decente dovrebbe comportarsi!"


translate italian day1_fc0afa91:


    mt "E adesso è ora di dormire!"


translate italian day1_cee3bfd9:


    "Sinceramente, non avevo intenzione né di diventare un pioniere decente, né uno indecente."


translate italian day1_06cf444d:


    "Anzi, fino a ieri non avevo alcuna intenzione di diventare un pioniere..."


translate italian day1_e62f6767:


    th "Ma ho una qualche possibilità di scelta adesso?"


translate italian day1_a3857c58:


    "«Se non vuoi farlo, ti costringeremo a farlo» – probabilmente era questo il motto di Olga Dmitrievna."


translate italian day1_fe66fa24:


    "Sono salito sul letto e ho chiuso gli occhi, rendendomi conto di quanto fossi stanco dopo questa giornata."


translate italian day1_80860151:


    "Qualcosa stava martellando tremendamente nella mia testa, come se dentro di essa fosse appena iniziato il turno di notte di una fabbrica."


translate italian day1_af68a167:


    "E sembrava essere la lavorazione di acciaio laminato, piuttosto che qualcosa di delicato."


translate italian day1_95ae7d8f:


    "L'autobus ha attraversato la mia mente..."


translate italian day1_de155e12:


    "E la piazza col monumento..."


translate italian day1_4d51c966:


    "La mensa, affollata di pionieri...{w} E l'espressione maligna di Ulyana."


translate italian day1_169054b7:


    "Slavya..."


translate italian day1_06f01a7c:


    "Lena..."


translate italian day1_753d87f9:


    "E persino il ricordo di Alisa non mi dispiaceva."


translate italian day1_1de79ea9:


    th "E se dovessi restare qui per sempre...?"
# Decompiled by unrpyc: https://github.com/CensoredUsername/unrpyc
