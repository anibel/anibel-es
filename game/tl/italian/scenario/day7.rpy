
translate italian day7_main_aa6af803:


    "La luce del sole era talmente forte che riusciva a folgorare i miei occhi attraverso le mie palpebre."


translate italian day7_main_5c43311e:


    "Mi sono stirato pigramente e sono sceso dal letto con un balzo."


translate italian day7_main_f254f75a:


    "Erano le due di pomeriggio."


translate italian day7_main_b36d3471:


    th "Sembra proprio che la giornata di ieri mi abbia davvero sfinito, e che il mio organismo richieda più tempo del solito per ristabilirsi."


translate italian day7_main_c1bcf764:


    "Ho passeggiato intorno alla stanza, pensando a cosa avrei dovuto fare oggi."


translate italian day7_main_d7fae9fa:


    "Ovviamente la mia vita nel Campo non sarebbe stata più la stessa dopo tutto quello che avevo sentito dire da quello strano pioniere."


translate italian day7_main_f80b4755:


    th "E se tutto è veramente come mi ha detto, allora ho ancora un sacco di tempo a venire."


translate italian day7_main_6bd2f897:


    "Ho raccolto il mio kit per l'igiene e sono andato fuori."


translate italian day7_main_e37820fc:


    "Sono riuscito a fare solo un paio di passi, quando qualcuno mi ha urtato sulla schiena."


translate italian day7_main_fe998b08:


    "Mi sono voltato e ho visto Alisa."


translate italian day7_main_b3e42438:


    dv "Ehi, fa' attenzione!"


translate italian day7_main_0ffdf7aa:


    "Ha detto con indifferenza, ed è corsa per la sua strada."


translate italian day7_main_17ea660a:


    "Teneva tra le mani una specie di sacchetto..."


translate italian day7_main_dc306e1e:


    th "Ah, che importa, ecco un altro tipico giorno in questo luogo pazzesco."


translate italian day7_main_2e63ec7e:


    "Mi sono lavato i denti e poi ho impiegato un'eternità a lavarmi il viso con l'acqua gelata, per svegliarmi come si deve e rinfrescare la mia mente, almeno un poco."


translate italian day7_main_e3cedd9c:


    "Dopo aver fatto ciò mi sentivo già meglio."


translate italian day7_main_c32db707:


    "Improvvisamente una certa speranza è apparsa dal nulla.{w} Certamente non si trattava della speranza di riuscire ad andarmene da questo luogo in modo sicuro…"


translate italian day7_main_79994a07:


    "Piuttosto, non volevo credere che fosse tutto così brutto come mi aveva detto quel ragazzo."


translate italian day7_main_7efc0341:


    pi "Buongiorno…"


translate italian day7_main_215b97bf:


    "Ho sentito un filo di voce provenire dalla foresta."


translate italian day7_main_091593a0:


    "Qualcuno stava dietro a un albero."


translate italian day7_main_70958877:


    me "Giorno…"


translate italian day7_main_6bedc27f:


    pi "Sei pronto?"


translate italian day7_main_eaccb1bc:


    me "Pronto per cosa?"


translate italian day7_main_a93dcec8:


    "Ho guardato meglio, e sembrava che fosse il pioniere che avevo incontrato ieri alla fermata dell'autobus."


translate italian day7_main_3d1d50e7:


    "Anzi, mi ero preparato per una tale stranezza oggi, quindi non mi sono sorpreso più di tanto, e ho iniziato la conversazione a mente piuttosto lucida."


translate italian day7_main_c6c61387:


    pi "Non gli hai creduto, vero?"


translate italian day7_main_cb23542f:


    me "Di cosa stai parlando?"


translate italian day7_main_74acfe46:


    pi "Tutto quello che ti ha detto…"


translate italian day7_main_fe4e16b8:


    me "D'accordo, ma tu invece cosa ne pensi di tutto questo?"


translate italian day7_main_71a0a00c:


    "Avevo già preso le distanze dal mondo esterno, e avevo deciso di trattare ogni cosa come se fosse un film, e non la realtà."


translate italian day7_main_e427bce7:


    th "Sperando che ciò mi possa concedere maggiore spazio per analizzare la situazione in maniera logica."


translate italian day7_main_4ad2a811:


    pi "Un'uscita esiste. Dev'esserci per forza!"


translate italian day7_main_ad9f7f99:


    "Ha detto con entusiasmo."


translate italian day7_main_9da02d58:


    me "Non lo so… Ma anche a me piacerebbe crederci."


translate italian day7_main_6c7b7a89:


    pi "Se solo tu…"


translate italian day7_main_2a454c26:


    sl "Semyon!"


translate italian day7_main_47eaf14e:


    "Mi sono girato."


translate italian day7_main_5d2bdfb2:


    "Slavya stava in piedi accanto a me."


translate italian day7_main_177f4329:


    sl "Con chi stavi parlando?"


translate italian day7_main_32d6e567:


    me "Ahem… Con nessuno…{w} Stavo solo parlando con me stesso."


translate italian day7_main_85f46292:


    "Non credo che valesse la pena raccontarle di alieni provenienti da mondi paralleli."


translate italian day7_main_eb65a9c2:


    th "Probabilmente lei non riesce comunque a vederli…"


translate italian day7_main_f35f8815:


    sl "Hai preparato tutto?"


translate italian day7_main_6f63a38c:


    me "Preparato? Per un'altra escursione?"


translate italian day7_main_f7b781d4:


    sl "Ma no!{w} Oggi è l'ultimo giorno della sessione."


translate italian day7_main_0657e28f:


    me "Che cosa...?"


translate italian day7_main_955f98b3:


    "Un sorriso stupido si è stanziato sul mio volto."


translate italian day7_main_8ecccc0f:


    sl "L'autobus passerà questa sera. Ce ne andiamo."


translate italian day7_main_82b7666b:


    me "Porca vacca…"


translate italian day7_main_128e12c4:


    th "Ero pronto per il più incredibile dei colpi di scena, ma questo…"


translate italian day7_main_f4182ba6:


    "Forse quel misterioso pioniere si riferiva a questo?"


translate italian day7_main_2e7eb2bb:


    th "Probabilmente sì."


translate italian day7_main_7bea6fbb:


    th "Beh, a quanto pare dovrò fare un secondo giro e vivere ancora un'altra settimana in questo Campo."


translate italian day7_main_1358eee6:


    th "Ma questa volta so già tutto!"


translate italian day7_main_08625009:


    me "Non mi sono ancora preparato…{w} Non è che io abbia molto da preparare in ogni caso."


translate italian day7_main_6f9aa3f1:


    sl "Ok…"


translate italian day7_main_a04a9677:


    "Slavya ha portato lo sguardo altrove."


translate italian day7_main_fe79dc7e:


    "Sembrava volesse dirmi qualcosa, ma ha esitato."


translate italian day7_main_9a860373:


    sl "Bene, allora ci vediamo!"


translate italian day7_main_279eeea1:


    me "Sì…"


translate italian day7_main_0f8792e5:


    th "Almeno avrei dovuto chiederle a che ora è fissata la partenza..."


translate italian day7_main_adbb8b50:


    "Sono andato alla piazza con l'intenzione di scoprirlo."


translate italian day7_main_dd4f3123:


    th "Dev'esserci qualcuno che conosco qui!"


translate italian day7_main_a261ff3b:


    "Ma, che ci crediate o no, c'era solo Genda ad aspettarmi nella piazza."


translate italian day7_main_588af67a:


    th "Probabilmente tutti i pionieri sono occupati a fare i bagagli."


translate italian day7_main_6ae3edb7:


    "Mi sono seduto su una panchina e ho fissato il cielo."


translate italian day7_main_76f33ef6:


    th "Pare che oggi l'unica cosa da fare sia aspettare l'autobus, addormentarmi su di esso e risvegliarmi nuovamente in questo Campo, proprio come il primo giorno…"


translate italian day7_main_2163cc41:


    "Tutto stava annegando nel silenzio."


translate italian day7_main_a7963ac8:


    "Quello era esattamente il momento di una giornata estiva in cui il sole sembra essere bloccato nel cielo, gli uccellini e i grilli sono andati a fare un pisolino dopo il pranzo, e il vento sta risparmiando le energie per poter garantire la freschezza serale tanto agognata dalla gente del posto."


translate italian day7_main_7bc45999:


    "Improvvisamente mi sono accorto che non solo mi ero appena perso il pranzo, ma avevo pure saltato la colazione."


translate italian day7_main_6274d258:


    "Sarebbe stato ridicolo cercare qualcosa nella mensa.{w} I pionieri avevano sicuramente già ripulito tutto prima della partenza."


translate italian day7_main_fbfba1db:


    "Mi sono grattato la testa e mi sono diretto verso la casetta di Olga Dmitrievna."


translate italian day7_main_e56fa1ff:


    th "Sicuramente dev'esserci qualcosa di commestibile nel cassetto del tavolo!"


translate italian day7_main_ea19c1fa:


    "Qualcuno mi ha chiamato proprio mentre mi stavo avvicinando alla porta."


translate italian day7_main_2faacefc:


    "Shurik ed Electronik si sono avvicinati rapidamente alla casetta."


translate italian day7_main_63a9aae8:


    el "Hai già fatto i bagagli?"


translate italian day7_main_129cce15:


    me "Ho già fatto i bagagli."


translate italian day7_main_cc5ffa9b:


    "Ho risposto, imitandolo."


translate italian day7_main_18e33553:


    el "È da un po' di giorni che non sembri te stesso…"


translate italian day7_main_6e4c96dc:


    me "Cosa te lo fa pensare?"


translate italian day7_main_40962235:


    el "E che ne so? Dovresti saperlo tu…"


translate italian day7_main_55493947:


    me "Voglio dire – perché ti preoccupi tanto?"


translate italian day7_main_481d2b70:


    el "Così…"


translate italian day7_main_10cfbea3:


    sh "Un vero pioniere considera sempre i problemi di un compagno come i propri!"


translate italian day7_main_ff16defd:


    "Ho lanciato uno sguardo scettico verso di lui."


translate italian day7_main_13d9478e:


    me "Grazie per l'interesse. Sto bene."


translate italian day7_main_88d04331:


    "Alla fine ho trovato una mezza pagnotta di pane raffermo e un pezzo di salsiccia affumicata nel cassetto."


translate italian day7_main_26352937:


    "Li ho mangiati con deliberata soddisfazione, sorseggiando il tutto con l'acqua puzzolente che Olga Dmitrievna probabilmente usava per innaffiare le piante."


translate italian day7_main_c67840a3:


    "Appena dopo aver finito, qualcuno ha bussato alla porta."


translate italian day7_main_48555f41:


    me "Avanti."


translate italian day7_main_ed42b147:


    "Ulyana si è precipitata nella stanza."


translate italian day7_main_bb2205bc:


    us "Oh, sei solo tu!"


translate italian day7_main_3db311fc:


    "Ha detto delusa."


translate italian day7_main_b9fcdeff:


    me "E chi ti aspettavi di vedere? Un circo con tanto di orsi?"


translate italian day7_main_94876b55:


    "Ulyana ha ridacchiato."


translate italian day7_main_da46daf5:


    us "Dov'è Olga Dmitrievna?"


translate italian day7_main_595d7b8c:


    me "Non lo so."


translate italian day7_main_8e0229dd:


    "Ho alzato le spalle."


translate italian day7_main_301d3861:


    us "Perché no?"


translate italian day7_main_2f877f3b:


    me "Non lo so perché non lo so…{w} Cosa vuoi da lei, comunque?"


translate italian day7_main_756d3f6a:


    us "Devo chiederle qualcosa prima della partenza."


translate italian day7_main_c1537911:


    me "Ok, le dirò che la stai cercando, se la vedo."


translate italian day7_main_95cb8133:


    us "A proposito, perché non ti stai preparando?"


translate italian day7_main_3178b41b:


    me "Come se avessi molto da preparare…"


translate italian day7_main_fa77eb7d:


    us "Beh, ci vediamo!"


translate italian day7_main_84ce9118:


    "Ha fatto un sorriso sornione e si è slanciata fuori dalla stanza, sbattendo la porta dietro di sé."


translate italian day7_main_b0f8ec72:


    "Comunque, è davvero strano che nessuno sembri essere perplesso da questa improvvisa partenza."


translate italian day7_main_80d609d4:


    th "E perché io sono l'unico che non se l'aspettava?"


translate italian day7_main_e2fab788:


    th "Come se a loro importasse del fatto che io abbia fatto i bagagli o meno."


translate italian day7_main_fa546b6a:


    th "Inoltre, perché nessuno sembra essere triste per il fatto che oggi sia l'ultimo giorno che possiamo vederci...?"


translate italian day7_main_dcef1d84:


    "Improvvisamente le parole di quel ragazzo ieri nella foresta, riguardo al fatto che tutte le persone qui non fossero reali, sono apparse nella mia mente."


translate italian day7_main_de05ab05:


    th "Beh, ora sono pronto a crederci più che mai."


translate italian day7_main_d7317467:


    "Il cassetto della scrivania dove avevo trovato la mia colazione conteneva un sacco di cianfrusaglie."


translate italian day7_main_d43240e6:


    "Ho preso una matita e un foglio di carta, li ho esaminati per un po', e poi me li sono infilati in tasca."


translate italian day7_main_8dcd7fa6:


    th "Per sicurezza!"


translate italian day7_main_39f8f65c:


    "Non avevo alcuna intenzione di osservare tutti i pionieri andare in giro e prepararsi per la partenza, quindi mi sono limitato a stare sdraiato sul letto, senza nemmeno accorgermi di essermi assopito."


translate italian day7_main_a20cefa7:


    "..."


translate italian day7_main_59194f62:


    "La voce di qualcuno mi ha svegliato."


translate italian day7_main_9e6959c5:


    "Un pioniere familiare era seduto proprio di fronte a me, dandomi le spalle."


translate italian day7_main_937c66f2:


    "Mi ero abituato a lui già da ieri, e sembrava anche che ormai non lo temessi più."


translate italian day7_main_d1240714:


    me "Ehi, perché ti nascondi sempre il volto?"


translate italian day7_main_cc040a11:


    pi "Perché non dovresti vederlo."


translate italian day7_main_0fa7e23d:


    me "Se lo dici tu..."


translate italian day7_main_9ca20712:


    "Non mi trovavo nella posizione giusta per poter discutere."


translate italian day7_main_c1b4c0f1:


    me "Allora, cos'hai intenzione di rivelarmi questa volta?"


translate italian day7_main_600f0c98:


    pi "Sai già che oggi è l'ultimo giorno della sessione, vero?"


translate italian day7_main_fc30fd2b:


    me "Sì."


translate italian day7_main_6800c2ff:


    pi "E hai già parlato con {i}quell'altro{/i}?"


translate italian day7_main_fc30fd2b_1:


    me "Sì."


translate italian day7_main_cf3c4c1e:


    pi "Allora, cosa ti ha detto?"


translate italian day7_main_4a46ef3d:


    me "Niente di speciale. Ha detto che esiste un'uscita."


translate italian day7_main_6beb007e:


    "Il pioniere è scoppiato a ridere."


translate italian day7_main_caf2ecdd:


    pi "Già, lo credevo anch'io, secoli fa."


translate italian day7_main_f6352aef:


    me "E ora?"


translate italian day7_main_52ab222f:


    pi "E che cos'{i}è{/i} questo «ora»?{w} Per me è il passato, la mia vita di allora…"


translate italian day7_main_867d892e:


    "Ha smesso di parlare per un po' di tempo."


translate italian day7_main_015d5bda:


    pi "In ogni caso, è stato molto tempo fa, quindi non ricordo esattamente."


translate italian day7_main_499952ad:


    pi "Il futuro è la stessa cosa – cicli, cicli, cicli, ripetizioni della stessa storia.{w} Dove si trova quel tuo {i}«ora»{/i}?"


translate italian day7_main_50a2b7a7:


    me "Beh, mi spiace dirtelo, ma io non sono ancora così perso nel tempo come lo sei tu."


translate italian day7_main_09c7c654:


    pi "Oh, non fa niente! Ci arriverai col tempo!"


translate italian day7_main_54556d0f:


    "Ha fatto una risata diabolica."


translate italian day7_main_2ede7e21:


    me "C'è solo una cosa che non capisco – perché continui a farmi visita? Che cosa ti aspetti di ottenere?"


translate italian day7_main_ed700d2d:


    pi "Io? Proprio niente."


translate italian day7_main_ed4c078d:


    me "Allora perché sei venuto?"


translate italian day7_main_239cb75b:


    pi "È solo perché tu, quell'altro, e altri come noi sono le uniche persone reali in questo luogo."


translate italian day7_main_cce08df0:


    "Anche dopo tutto quello che mi aveva raccontato, non ero del tutto pronto a credere che tutti i residenti locali fossero solo dei burattini in una sorta di teatrino infernale."


translate italian day7_main_4529ad5a:


    me "Sei sicuro di avere ragione?"


translate italian day7_main_6fa19435:


    pi "Ragione su cosa?"


translate italian day7_main_311f2261:


    me "Beh… Di avere ragione su tutto?"


translate italian day7_main_1e5d88ee:


    pi "Non posso aver ragione o torto.{w} Non ho scelto io questo mondo, non mi ci sono buttato dentro. Sono semplicemente qui. E tu sei semplicemente qui."


translate italian day7_main_8b97aebf:


    me "Senti, mi è già venuto il mal di testa a causa del tuo filosofeggiare."


translate italian day7_main_d4c1382a:


    "Ero un po' perplesso sul perché continuassi a parlare in modo così disinvolto con quel misterioso pioniere."


translate italian day7_main_0e575d19:


    th "Beh, alla fine ecco qua, proprio davanti ai miei occhi – tutte le fantasie e le diavolerie che sono accadute in questo Campo."


translate italian day7_main_265d8152:


    th "Eccola qua – una spiegazione (seppur parziale) di come io sia arrivato qui."


translate italian day7_main_377ed3b6:


    th "Eccole qui – le tanto agognate risposte che cercavo da tempo!"


translate italian day7_main_0a0561bd:


    "D'altra parte, il mio comportamento era abbastanza logico."


translate italian day7_main_d9e03d7d:


    "Mentre non riuscivo a spiegarmi che cosa stesse accadendo, quel ragazzo cuculo continuava a parlare e parlare, ma non è che le sue parole avessero cambiato alcunché."


translate italian day7_main_8a6c765f:


    "Allora che senso aveva continuare ad ascoltarlo?"


translate italian day7_main_6a19df1b:


    pi "Oh, no! Presto capirai tutto da solo."


translate italian day7_main_5022bc8a:


    "Qualcuno stava bussando alla porta.{w} Mi sono alzato per aprirla."


translate italian day7_main_482a0501:


    "C'era Slavya sulla soglia."


translate italian day7_main_fad161d6:


    me "Stai cercando Olga Dmitrievna?"


translate italian day7_main_37eba565:


    sl "No…"


translate italian day7_main_7e6970f3:


    me "Allora entra."


translate italian day7_main_c073a9b1:


    "Ero sicuro al cento percento che il pioniere era già scomparso."


translate italian day7_main_8cf78390:


    "E avevo ragione."


translate italian day7_main_6a9ff616:


    "Slavya si è seduta sul letto, mentre io mi sono rannicchiato contro l'armadio in un angolo della stanza."


translate italian day7_main_c8621acb:


    "Slavya era visibilmente nervosa."


translate italian day7_main_ea63ce77:


    me "È successo qualcosa?"


translate italian day7_main_c18a9682:


    sl "Non proprio… È solo che oggi è l'ultimo giorno…"


translate italian day7_main_165306ee:


    me "Beh, ci sono arrivato anch'io. Meglio tardi che mai."


translate italian day7_main_6a46b661:


    sl "Ecco, così ho pensato…{w} Voglio dire…{w} Probabilmente non ci rivedremo mai più."


translate italian day7_main_2aa28af8:


    me "Il mondo è piccolo, come si suol dire."


translate italian day7_main_aaf30b84:


    sl "Ma forse potresti darmi il tuo indirizzo così ci potremo scrivere le lettere?"


translate italian day7_main_f178bf3d:


    th "Glielo darei, se solo lo sapessi."


translate italian day7_main_9e6bb37c:


    me "Senti… Facciamo il contrario – dammi il tuo indirizzo. Ti scriverò sicuramente non appena sarò arrivato a casa."


translate italian day7_main_305689ee:


    sl "Ma perché non vuoi darmi il tuo?"


translate italian day7_main_a91117ee:


    me "Beh… Stavamo per traslocare, e quindi non si sa mai…{w} È meglio se ti scrivo io."


translate italian day7_main_f734a826:


    "Ho cercato di fare un sorriso quanto più simpatico possibile, per far sembrare la mia storia più credibile."


translate italian day7_main_596c8eed:


    sl "Ah, va bene… D'accordo allora…"


translate italian day7_main_a4cfd165:


    "Slavya si è alzata e ha fatto per andarsene."


translate italian day7_main_98609165:


    me "Ehi, aspetta, e l'indirizzo?"


translate italian day7_main_a53cdaac:


    sl "Te lo dico dopo."


translate italian day7_main_318e3c4f:


    "Un'espressione di tristezza e delusione ha attraversato il suo viso."


translate italian day7_main_1e9c65d7:


    "Dopo che la porta si è richiusa, ho subito udito alle mie spalle la voce maligna del pioniere."


translate italian day7_main_7fcab065:


    pi "Beh, sei contento adesso? Hai ferito una ragazza."


translate italian day7_main_7700b04d:


    me "E come l'avrei ferita? Cosa avrei dovuto dirle? Mia cara, scrivi alla mia amata nonnina in campagna?"


translate italian day7_main_a75b7aef:


    me "O forse avrei dovuto lasciarle l'indirizzo di una casa che probabilmente non è ancora stata costruita?"


translate italian day7_main_87494573:


    pi "E allora? Io non sono il tuo custode!{w} È il tuo mondo, non il mio. Io me la cavo in qualche modo, nel mio."


translate italian day7_main_febd1fcf:


    "Quella sua ultima frase mi ha dato i brividi."


translate italian day7_main_997bc03d:


    me "Sai che ti dico…"


translate italian day7_main_5e237961:


    "Non sono riuscito a finire la frase – qualcuno ha bussato nuovamente alla porta."


translate italian day7_main_6b33d385:


    me "Avanti!"


translate italian day7_main_50581468:


    "Ulyana è volata all'interno della stanza."


translate italian day7_main_a406d12c:


    me "Perché così di corsa, mia signora?"


translate italian day7_main_3ea49673:


    "Ho fatto un inchino eccessivamente appariscente."


translate italian day7_main_4081a71f:


    us "Io? Volevo… solo…"


translate italian day7_main_440da628:


    "I suoi occhi guizzavano con cautela e le sue guance sono diventate rosse."


translate italian day7_main_02437b85:


    us "Volevo solo dirti addio!"


translate italian day7_main_47d964b9:


    me "Ci sarà ancora tempo per questo. Dopotutto, saliremo tutti sullo stesso bus."


translate italian day7_main_6a55610a:


    us "Sì, hai ragione, ma è un po' imbarazzante farlo davanti a tutti."


translate italian day7_main_7420944b:


    me "Oh, e così c'è qualcosa che riesce a metterti in imbarazzo?"


translate italian day7_main_60bde9d8:


    "Ho riso."


translate italian day7_main_3dc79f49:


    us "Err!"


translate italian day7_main_366345bd:


    "Ha messo il broncio."


translate italian day7_main_a18522d8:


    us "Volevo solo dirti che non ti considero un idiota, davvero… In realtà, sei quasi un tipo a posto!"


translate italian day7_main_7dc51ce4:


    "Le sue parole mi hanno stupito."


translate italian day7_main_f2486061:


    me "Beh, ti ringrazio…{w} Anche tu non sei male come compagnia."


translate italian day7_main_4a74cafa:


    us "Ecco, questo è tutto!"


translate italian day7_main_519137f4:


    "Si è precipitata fuori, sbattendo la porta rumorosamente."


translate italian day7_main_d76f8bb2:


    pi "Ehi, non te lo saresti aspettato da parte sua?"


translate italian day7_main_b78c6e26:


    me "E tu?"


translate italian day7_main_c233607a:


    pi "Io non ho niente a che fare con questo, te l'ho già detto."


translate italian day7_main_e15ce1bd:


    me "Sembra che tu sapessi già cosa avrebbe detto."


translate italian day7_main_5de5d3d5:


    pi "Forse sì… Forse no…"


translate italian day7_main_b6653187:


    me "Sei venuto qui solo per prendermi in giro?"


translate italian day7_main_6ea78a01:


    "Stavo cominciando a perdere la pazienza."


translate italian day7_main_e7918564:


    pi "Anche quell'idea mi è passata per la testa."


translate italian day7_main_4d6f2bc2:


    me "Allora perché diavolo continui a stare qui?"


translate italian day7_main_54b22d30:


    me "Se avessi mai bisogno di qualcuno che commenti tutte le mie azioni, mi rivolgerò ad uno psicologo professionista!"


translate italian day7_main_3b8fbfa4:


    pi "Credi che dopo tutto questo tempo io non possa esserlo?"


translate italian day7_main_e499cb70:


    me "A mio parere, dopo tutto questo tempo direi che sei proprio andato fuori di testa."


translate italian day7_main_a9edc4dd:


    pi "Tutti gli psicologi sono folli…"


translate italian day7_main_eb56bdcb:


    me "È vero, ma non tutti i folli sono psicologi."


translate italian day7_main_4fdeb53f:


    "È scoppiato a ridere."


translate italian day7_main_28f25b65:


    pi "Che senso dell'umorismo, davvero incoraggiante! Francamente, le tue battute sono piuttosto scarse.{w} Di chi stai ridendo in realtà? Di te stesso!"


translate italian day7_main_7e656166:


    me "Senti, se non hai niente di meglio da fare nel tuo mondo, allora va' a rompere le scatole a {i}quell'altro{/i}."


translate italian day7_main_8cd37642:


    pi "E tu invece hai qualcosa da fare nel tuo mondo?"


translate italian day7_main_18cd1dcb:


    "Ha ribattuto con veemenza."


translate italian day7_main_06f26021:


    me "Sai, io penserò a cosa fare… Farò i bagagli, lascerò questo Campo…"


translate italian day7_main_925fc2f3:


    pi "E poi cosa?"


translate italian day7_main_9791c0b4:


    me "Poi cosa…{w} Come se lo sapessi!"


translate italian day7_main_f05adfcc:


    me "Non mi sono mai trovato in una situazione del genere, che tu ci creda oppure no."


translate italian day7_main_7109a0ab:


    pi "Forse hai dimenticato un piccolo dettaglio: non sarai in grado di lasciare il Campo."


translate italian day7_main_a4f8c07f:


    th "Sì, ha ragione su questo punto.{w} Probabilmente…"


translate italian day7_main_c8fda9f8:


    me "Solo perché tu non ci sei riuscito, non significa che non possa riuscirci pure io!"


translate italian day7_main_f038ee18:


    pi "Sei tu il capo."


translate italian day7_main_cc46f69e:


    "Qualcuno ha bussato alla porta.{w} Così debolmente che l'ho sentito a malapena."


translate italian day7_main_67e1f584:


    me "Accidenti, chi è di nuovo?!"


translate italian day7_main_c78e5705:


    "Ho sibilato sottovoce, e ho gridato:"


translate italian day7_main_6b33d385_1:


    me "Avanti!"


translate italian day7_main_abbe1f0a:


    "Ma la porta non si è aperta."


translate italian day7_main_c5b18002:


    "Allora l'ho aperta io."


translate italian day7_main_26171c75:


    "Lena stava in piedi sulla soglia."


translate italian day7_main_40993023:


    "Pare che l'avessi davvero spaventata."


translate italian day7_main_200803e8:


    me "Oh, scusa, non volevo… Stai cercando Olga Dmitrievna?"


translate italian day7_main_049b021f:


    un "No."


translate italian day7_main_ed81b2e1:


    "Ha risposto, fissando il terreno."


translate italian day7_main_84eb3967:


    me "Allora, cosa c'è?"


translate italian day7_main_5f2d59cb:


    th "Cosa vuole da me Lena?"


translate italian day7_main_2526c703:


    me "Entra."


translate italian day7_main_b516a337:


    "È entrata e ha esitato al centro della stanza."


translate italian day7_main_e22a9c2a:


    me "Vuoi sederti?"


translate italian day7_main_d6169916:


    "Ho indicato uno dei letti."


translate italian day7_main_2d050cb5:


    "Lena ha esitato ancora un po', ma alla fine si è seduta."


translate italian day7_main_ea63ce77_1:


    me "È successo qualcosa?"


translate italian day7_main_72b9a973:


    un "Niente affatto, è solo che…"


translate italian day7_main_918776a6:


    "Mi ha lanciato un'occhiata rapida, ma al tempo stesso è arrossita e ha distolto lo sguardo."


translate italian day7_main_bacbd43d:


    un "Ecco!"


translate italian day7_main_989da459:


    "Lena ha tolto qualcosa dalla tasca e me l'ha consegnato."


translate italian day7_main_23db2f78:


    "Ero sbalordito – era il mio telefono."


translate italian day7_main_27205bdb:


    me "Ma... Dove l'hai preso...?"


translate italian day7_main_8836199f:


    un "L'ho trovato nella foresta…"


translate italian day7_main_e109c728:


    me "D'accordo, ma perché pensi che sia mio?"


translate italian day7_main_2bc078c7:


    un "Me l'ha detto un ragazzo…"


translate italian day7_main_75088da6:


    me "L'hai mai visto prima?"


translate italian day7_main_da35aa0b:


    un "Non lo so, non sono riuscita a vederlo in faccia, ma era vestito in uniforme da pioniere."


translate italian day7_main_c773393c:


    "Tutto è diventato chiaro come il sole."


translate italian day7_main_82b1e370:


    me "E non ti sei chiesta cos'è questo?"


translate italian day7_main_30490fdf:


    "Ho guardato lo schermo – era rimasta ancora un po' di batteria, quindi Lena non avrebbe potuto considerare quel telefono come un semplice pezzo di plastica."


translate italian day7_main_4f35044c:


    un "Non saprei, una specie di gioco…"


translate italian day7_main_a3eea1e0:


    me "Sì, hai ragione…"


translate italian day7_main_577b13f1:


    "Ho subito aperto Snake dal menu e le ho consegnato il telefono."


translate italian day7_main_19416c28:


    me "Tienilo! Come ricordo!"


translate italian day7_main_afefddf2:


    un "Oh, ma cosa dici, non posso…"


translate italian day7_main_2ed4ff25:


    "Lena ha agitato le mani davanti a me."


translate italian day7_main_f4b1f89a:


    me "Tienilo pure, ce ne ho un sacco a casa."


translate italian day7_main_cdda1bfc:


    "Ha opposto resistenza ancora per un po', ma alla fine ha ceduto."


translate italian day7_main_c67a4136:


    un "E come si usa?"


translate italian day7_main_9216d06a:


    me "Premi i tasti per spostarti a destra e a sinistra, devi cercare di mangiare le palline ed evitare di toccarti la coda."


translate italian day7_main_1239b2a3:


    un "Wow, è così interessante!"


translate italian day7_main_d1046a49:


    "Ha sorriso."


translate italian day7_main_1aa9d6c3:


    un "Grazie! Eppure io non ho niente per te, è così imbarazzante."


translate italian day7_main_58e414f2:


    me "Non ce n'è bisogno, grazie."


translate italian day7_main_d4366f58:


    un "No, così non va bene!"


translate italian day7_main_2b2d9484:


    "Ha detto in un tono che sembrava più sicuro del solito."


translate italian day7_main_39665fa5:


    un "Dopotutto oggi è l'ultimo giorno."


translate italian day7_main_9513cd87:


    me "Già…"


translate italian day7_main_f93613f2:


    un "Spero che ci rivedremo…"


translate italian day7_main_e16f1ab0:


    me "Penso di sì."


translate italian day7_main_77f2eecc:


    un "Allora ho un regalo per te."


translate italian day7_main_5109b055:


    me "E che cos'è?"


translate italian day7_main_03f55ca9:


    un "Chiudi gli occhi."


translate italian day7_main_7fdc006d:


    "Ho fatto come mi ha chiesto."


translate italian day7_main_deebb2e3:


    un "E promettimi che non li aprirai finché non te lo dirò io!"


translate italian day7_main_71d3b8c8:


    me "Va bene."


translate italian day7_main_b472b19d:


    un "No, devi prometterlo!"


translate italian day7_main_1f17061d:


    me "D'accordo, te lo prometto!"


translate italian day7_main_3e527e8e:


    "Dopo un attimo ho sentito un lieve bacio sulla mia guancia."


translate italian day7_main_7aaa79b3:


    "Ero davvero desideroso di aprire gli occhi, ma le avevo promesso di non farlo…"


translate italian day7_main_d8b768ba:


    un "Aprili!"


translate italian day7_main_d6d054a1:


    "La stanza era vuota."


translate italian day7_main_188f31b1:


    me "Che ragazza…"


translate italian day7_main_026f195c:


    "L'unica cosa che sono riuscito a dire."


translate italian day7_main_9d8b1602:


    pi "Allora, come ci si sente, stallone?"


translate italian day7_main_56c91cad:


    "Una risata maligna si è levata dal punto in cui prima era seduta Lena."


translate italian day7_main_0802a5ba:


    me "Allora è questo il tuo nuovo modo di prenderti gioco di me, eh? Manipolando gli altri?"


translate italian day7_main_82d1e669:


    pi "Io? Manipolare? Perdio!{w} Anche se a dire il vero, hai ottenuto un bacio da una dolce ragazza grazie a me! Sulla guancia, ma comunque…"


translate italian day7_main_20d94de6:


    "Avrei voluto davvero picchiarlo in quel momento, ma non ero nemmeno sicuro che fosse fisicamente lì."


translate italian day7_main_9183379b:


    me "Presumo che questo tuo scherzo non fosse l'ultimo?"


translate italian day7_main_fcc63fd6:


    "Ho chiesto, in tono più calmo."


translate italian day7_main_e02cb0bc:


    pi "Chissà, chissà…{w} Non è forse divertente?"


translate italian day7_main_d2749b3c:


    "Ha fatto una grande risata. Forse il professor Moriarty rideva in quel modo, pregustando il successo del suo piano diabolico."


translate italian day7_main_7a033c20:


    me "Tu ti stai certamente divertendo, ma io no."


translate italian day7_main_33573f9b:


    pi "Rilassati, amico! Non manca molto – e poi, ecco il tuo secondo giro!"


translate italian day7_main_e2a097db:


    pi "Dopo averne fatti una dozzina, allora ti sarai guadagnato un pit-stop.{w} Anche se probabilmente non avrai più bisogno di me a quel punto – avrai già capito tutto da solo."


translate italian day7_main_15b2c48a:


    me "Non è che io abbia bisogno di te neppure in questo momento."


translate italian day7_main_d7caa3c1:


    pi "Oh, che ingratitudine…"


translate italian day7_main_417a57f0:


    me "Piantala!"


translate italian day7_main_6c7b7a89_1:


    pi "Se mai tu dovessi…"


translate italian day7_main_5f318380:


    me "Basta, chiudi il becco!"


translate italian day7_main_0d2ff854:


    pi "…non capire…"


translate italian day7_main_6d00754d:


    me "CHIUDI QUELLA DANNATA BOCCA!!!"


translate italian day7_main_2d7f3c73:


    "Ho gridato cosi forte da far tremare le pareti."


translate italian day7_main_348c1e05:


    "La porta d'ingresso si è spalancata improvvisamente ed è entrata Alisa."


translate italian day7_main_69acda8f:


    dv "Sbaglio o qualcuno qui è andato completamente fuori di testa?"


translate italian day7_main_93c290bf:


    "Ha chiesto impaurita."


translate italian day7_main_dd69c3a1:


    me "Beh, può darsi."


translate italian day7_main_a172912c:


    "Ho risposto con rabbia."


translate italian day7_main_b7182058:


    dv "Perché stai urlando?"


translate italian day7_main_5c9c623e:


    me "Perché ne ho voglia."


translate italian day7_main_7efa97c5:


    "Avevo già capito che l'arrivo «accidentale» di Alisa era stato pianificato da quel pioniere, o comunque sarei stato costretto a sentire un suo commento a riguardo, e piuttosto avrei preferito infilarmi due chiodi nelle orecchie."


translate italian day7_main_f20d0c38:


    dv "Sei diventato psicopatico o qualcosa del genere?"


translate italian day7_main_a4579566:


    "Alisa si è lasciata cadere su uno dei letti, rilassandosi."


translate italian day7_main_bec896af:


    me "A cosa devo l'onore?"


translate italian day7_main_d28f8e57:


    dv "Sono venuta solo per il gusto di farlo… E ti trovo qui che urli come un pazzo…"


translate italian day7_main_0a9c213a:


    me "Tu non fai mai niente «solo per il gusto di farlo»."


translate italian day7_main_f58c07db:


    dv "Non ho nient'altro da fare. Ho già preparato la mia roba. Mi sto annoiando…"


translate italian day7_main_af374609:


    me "Bene, bene…"


translate italian day7_main_e9100bc5:


    dv "Se pensi che io sia venuta qui perché…"


translate italian day7_main_7386d3f8:


    "Mi ha lanciato uno sguardo arrabbiato e si è voltata."


translate italian day7_main_2e5dec60:


    dv "Non sarei dovuta venire qui, dopo ciò!"


translate italian day7_main_8e7c35a6:


    me "Dopo cosa? Io non ho detto una parola."


translate italian day7_main_84a9b240:


    dv "Certamente! Ma l'hai pensato!"


translate italian day7_main_7179f364:


    me "Oh, e così adesso riesci a leggere nel pensiero?"


translate italian day7_main_9b855aa7:


    dv "Non c'è bisogno di leggere nei tuoi pensieri – ce l'hai scritto in faccia."


translate italian day7_main_e18b878d:


    "Era impossibile riuscire a leggere qualsiasi cosa sulla mia faccia, a parte la stanchezza e la rabbia."


translate italian day7_main_7b79a718:


    me "E allora cosa leggi sulla mia faccia?"


translate italian day7_main_359525c1:


    dv "Non sono affari tuoi!"


translate italian day7_main_4dfbba46:


    me "Beh, non è che io ti stia costringendo a stare qui."


translate italian day7_main_ee0160d3:


    dv "Non importa! Andrò via quando mi pare! Non spadroneggiarmi."


translate italian day7_main_7a6ef78d:


    me "Ok, rimani qui allora, per l'amor di Dio!"


translate italian day7_main_5a25a62a:


    "In ogni caso, preferivo molto più la compagnia di Alisa che quella del pioniere."


translate italian day7_main_35fda676:


    "Mi sono sdraiato sul letto e ho chiuso gli occhi."


translate italian day7_main_09b65c63:


    "Pochi minuti più tardi Alisa ha rotto il silenzio."


translate italian day7_main_35834dca:


    dv "Davvero non vuoi dirmi niente?"


translate italian day7_main_e473fa2f:


    me "Per esempio?"


translate italian day7_main_81291356:


    dv "Oggi è l'ultimo giorno, dopotutto."


translate italian day7_main_029a43e7:


    me "E sei contenta?"


translate italian day7_main_87a35bd8:


    dv "Beh…"


translate italian day7_main_17f57844:


    "Ha balbettato incerta."


translate italian day7_main_3930aa38:


    dv "Tutti se ne stanno andando."


translate italian day7_main_eef28ca7:


    me "Che liberazione."


translate italian day7_main_6589795d:


    dv "Tutto qui?"


translate italian day7_main_aa5aa16f:


    me "Ti serve altro?"


translate italian day7_main_493ea103:


    dv "Non ti piace qui?"


translate italian day7_main_d0b8151b:


    "La sua voce era insolita."


translate italian day7_main_26cff5fe:


    me "Ho visto posti migliori."


translate italian day7_main_f853a1e5:


    dv "Accidenti, sei così scemo che parlare con te è una totale perdita di tempo."


translate italian day7_main_739017d2:


    "Si è alzata e si è diretta verso l'uscita."


translate italian day7_main_cfa74516:


    me "Sì, buona fortuna anche a te!"


translate italian day7_main_d6ac8c0a:


    "Alisa si è voltata verso di me.{w} Ribolliva dalla rabbia."


translate italian day7_main_546c756b:


    dv "Non potresti almeno dire che ti mancherò?!"


translate italian day7_main_0b12d1b2:


    me "Certamente."


translate italian day7_main_db268193:


    dv "Sei uno stronzo!"


translate italian day7_main_06efabf4:


    "Ha sbattuto forte la porta."


translate italian day7_main_ac6d49df:


    me "Stupida!"


translate italian day7_main_19e291ef:


    "Alisa non ha sentito la mia ultima parola, ovviamente."


translate italian day7_main_3f9dd422:


    pi "Che pulcino ribelle, eh?"


translate italian day7_main_71dbf37a:


    me "Proprio come te."


translate italian day7_main_fe52d02d:


    pi "E anche te, in tal caso."


translate italian day7_main_dec36634:


    me "Certo, continua pure a paragonarmi a te."


translate italian day7_main_cc9559a7:


    "Ho detto con sarcasmo."


translate italian day7_main_f5e0300a:


    pi "E quale sarebbe la differenza? Siamo nella stessa situazione, solo che io sono stato qui più a lungo di te.{w} Beh, in effetti, molto più a lungo di te…"


translate italian day7_main_224b03df:


    me "E sei andato completamente fuori di testa."


translate italian day7_main_c299cbd0:


    pi "Non c'è da stupirsi."


translate italian day7_main_6a69d0f4:


    "La sua risata roca mi stava facendo davvero impazzire."


translate italian day7_main_69abf7b9:


    me "Senti, ti ho già detto che dovresti fare l'attore? Saresti proprio un brillante Hannibal Lecter!"


translate italian day7_main_f93c3816:


    me "Soprattutto perché ti consideri uno psicologo."


translate italian day7_main_5521fd0f:


    pi "Ci penserò su.{w} Bene, ora devo andare! Forse ci vedremo di nuovo."


translate italian day7_main_658edb67:


    me "Sparisci!"


translate italian day7_main_7b4acbfa:


    "Mi sono voltato verso di lui, ma era già scomparso."


translate italian day7_main_f23afdfa:


    me "Oh, finalmente!"


translate italian day7_main_bc9abf02:


    th "Ma comunque, perché è venuto?"


translate italian day7_main_0da09332:


    "Poteva essere vero che io, lui e altri come noi fossimo le uniche persone reali da queste parti, ma ho tentato disperatamente di credere che non fosse così."


translate italian day7_main_01d888f8:


    "Tutti i suoi discorsi sembravano essere una sorta di gioco complesso."


translate italian day7_main_0c605775:


    "Come se mi stesse dando un indizio dopo l'altro, cercando di portarmi verso qualcosa, permettendomi di scoprire una sorta di trama subdola."


translate italian day7_main_801bab94:


    th "Peccato che lui non abbia molto successo in ciò, dato che io sono a corto di idee…"


translate italian day7_main_a20cefa7_1:


    "..."


translate italian day7_main_bee56547:


    "Il tempo scorreva lentamente e con inganno, ma erano già le cinque di pomeriggio."


translate italian day7_main_891514f3:


    "È normale durante l'estate – se si contano i secondi e i minuti sembra che anche una singola ora non finisca mai, ma se la mente è occupata con altri pensieri, allora l'intera giornata vola via in fretta."


translate italian day7_main_941b1a51:


    "Ho deciso di iniziare a fare i bagagli."


translate italian day7_main_fb69cac0:


    th "Potrebbero andarsene senza di me…"


translate italian day7_main_511be532:


    "Ho subito preso tutta la mia roba invernale e l'ho spinta dentro a una borsa. Stavo per sdraiarmi nuovamente sul letto, ma la porta si è aperta ed è entrata Olga Dmitrievna."


translate italian day7_main_d8e5d2bb:


    mt "Ah, vedo che hai già preparato tutto.{w} Ottimo! Andiamo!"


translate italian day7_main_ce6ffaad:


    "Mi sono alzato a malincuore, ho afferrato il mio umile bagaglio e l'ho seguita."


translate italian day7_main_54c4948c:


    th "Proprio non mi importa di cosa accadrà in seguito."


translate italian day7_main_5af78794:


    th "Ho sospettato a lungo che nulla dipendesse da me in questo mondo, e gli eventi recenti hanno rinforzato la mia convinzione."


translate italian day7_main_06390870:


    th "Potrei svegliarmi domani sull'autobus numero 410 o potrei non svegliarmi affatto."


translate italian day7_main_5826caff:


    th "E questo è tutto, gente. È tutto così semplice."


translate italian day7_main_1a9d1f7c:


    th "Non c'è motivo di restare qui, non c'è nessun luogo dove poter scappare."


translate italian day7_main_8dc6c9ca:


    th "In sostanza, la mia unica via d'uscita è prendere il bus con tutti gli altri.{w} Andando verso l'ignoto."


translate italian day7_main_e4adaddf:


    th "È l'unica cosa che ho fatto durante questa settimana – inciampare lungo uno stretto sentiero immerso nel buio nero come la pece, senza avere la certezza di dove fosse il punto di inizio e quello di arrivo."


translate italian day7_main_0d0d1634:


    "Eravamo quasi arrivati al cancello quando ho sentito qualcuno chiamare il mio nome."


translate italian day7_main_592b8e1f:


    bush "Semyon… Semyon…"


translate italian day7_main_f779d1e3:


    me "Olga Dmitrievna, scusi solo un minuto."


translate italian day7_main_65032a00:


    mt "Va bene, ma fa' in fretta o ce ne andremo senza di te!"


translate italian day7_main_18687800:


    "Mi sono diretto verso i cespugli da dove proveniva quella voce."


translate italian day7_main_592b8e1f_1:


    bush "Semyon… Semyon…"


translate italian day7_main_36c554a3:


    "Mi sono fatto strada tra il verde fogliame e sono arrivato a un sentiero nella foresta."


translate italian day7_main_52117956:


    "Quella voce sembrava provenire dal nulla: un momento la sentivo dietro ad un albero, e poi la sentivo dietro alle mie spalle."


translate italian day7_main_8d4918eb:


    "Probabilmente si trattava di uno di quegli alieni provenienti dai mondi paralleli, ma non credevo di averlo incontrato prima d'ora."


translate italian day7_main_f24bb417:


    bush "Non abbiamo molto tempo."


translate italian day7_main_fd341de6:


    me "Ti ascolto."


translate italian day7_main_6b2e53ca:


    bush "So già che sei stato contattato da {i}lui{/i} e da {i}lui{/i}."


translate italian day7_main_fc30fd2b_2:


    me "Già."


translate italian day7_main_6c48903a:


    bush "E so quello che ti hanno detto. Non chiedermi perché."


translate italian day7_main_53a56040:


    me "Va bene."


translate italian day7_main_0e7f1fb9:


    bush "Ma devi sapere una cosa. La cosa che so…{w} Siamo più di una dozzina qui. A dire il vero, siamo più di un migliaio…"


translate italian day7_main_85f19512:


    bush "Ma la maggioranza è riuscita ad andarsene."


translate italian day7_main_3cf4cc40:


    "Ho provato a digerire quello che mi stava dicendo, cercando di fare le domande giuste."


translate italian day7_main_5c937cbe:


    me "Allora perché tu sei qui?"


translate italian day7_main_df98d116:


    bush "Io sono rimasto."


translate italian day7_main_7332342b:


    me "Perché?"


translate italian day7_main_0f22f523:


    bush "Per aiutare gli altri a trovare una via d'uscita!"


translate italian day7_main_d52f7ebb:


    th "Wow, com'è generoso…"


translate italian day7_main_fc7d44e9:


    me "E perché dovrei fidarmi di te? Sei già il terzo…{w} Beh, in realtà non sono nemmeno sicuro che tutti voi non siate solo frutto della mia immaginazione."


translate italian day7_main_6972bbdc:


    bush "Non si tratta di fiducia…"


translate italian day7_main_ca1e4751:


    me "Allora di cosa si tratta?"


translate italian day7_main_be914630:


    bush "Si tratta di fare la scelta giusta!"


translate italian day7_main_ac9043c1:


    bush "Considera questo Campo come un labirinto gigante – e tu devi prendere una serie di direzioni corrette per poter trovare l'uscita."


translate italian day7_main_bc5b6d3b:


    me "Va bene, e come faccio a sapere quali sono quelle giuste?"


translate italian day7_main_58019608:


    bush "Lo saprai… Vieni con me…"


translate italian day7_main_1878bc08:


    me "Dove?"


translate italian day7_main_4a75165b:


    bush "Al secondo giro."


translate italian day7_main_34fc2c23:


    me "Aspetta un attimo…{w} Quel pioniere ha detto che il mio secondo giro inizierà domani."


translate italian day7_main_ac491a56:


    bush "Ha mentito!"


translate italian day7_main_1386fd85:


    "La voce dell'interlocutore si è alzata."


translate italian day7_main_73da8c64:


    bush "Lui è diventato completamente pazzo, e sta cercando di distruggere tutti gli altri!"


translate italian day7_main_20f48057:


    me "Quindi, se non andrò con te e partirò sul bus allora… verrò distrutto, giusto?"


translate italian day7_main_8422eef3:


    bush "Non lo so…"


translate italian day7_main_c5e0555a:


    me "Allora perché dici così?"


translate italian day7_main_f2ac56d7:


    bush "Nessuno è mai tornato dopo aver parlato con lui."


translate italian day7_main_50e4f830:


    me "Senti, perché dovrei fidarmi di te?"


translate italian day7_main_aa1b7016:


    "Ormai ero al limite."


translate italian day7_main_817f4941:


    th "Mentre quel pioniere mi ha solo preso in giro e non sembrava essere un vero pericolo, questo chiaramente sta parlando di cose per cui vale la pena essere in ansia."


translate italian day7_main_9bad067c:


    th "Non so davvero di quale dei due dovrei fidarmi."


translate italian day7_main_e1e025fb:


    th "È come se si chiedesse a una persona cieca di dire se la luce nella stanza è accesa oppure no."


translate italian day7_main_355517e6:


    th "Può solo tirare a indovinare.{w} Proprio come me…"


translate italian day7_main_068aa355:


    bush "Sbrigati, il tempo stringe!"


translate italian day7_main_aff91d3f:


    me "Ehi, aspetta un secondo…"


translate italian day7_main_77d3e654:


    bush "Fa' una scelta – vieni oppure no?!"


translate italian day7_main_f6c00c2d:


    me "D'accordo, andiamo."


translate italian day7_main_3976ac4c:


    th "In ogni caso, costui mi sembra più credibile del folle pioniere."


translate italian day7_main_f59377cf:


    th "Certamente questa scelta potrebbe rivelarsi fatale, ma l'alternativa non è di certo migliore."


translate italian day7_main_220c95fc:


    "Ho semplicemente sparato alla cieca…"


translate italian day7_main_2715a6e1:


    me "Ok, allora dove andiamo?"


translate italian day7_main_b3a4212e:


    bush "Siamo già arrivati…"


translate italian day7_main_9c4ed245:


    "La mia vista ha iniziato ad offuscarsi e i miei sensi hanno cominciato ad abbandonarmi del tutto…"


translate italian day7_main_300388db:


    me "No, sai che ti dico, non posso crederti dopo appena due minuti."


translate italian day7_main_92441a59:


    th "Almeno ho conosciuto l'altro tizio un po' più a lungo, e le sue argomentazioni sembrano più convincenti."


translate italian day7_main_026e81de:


    bush "Te ne pentirai…"


translate italian day7_main_c9f8c4d3:


    "Era come se quelle parole fossero arrivate da un altro mondo."


translate italian day7_main_c80400a7:


    "La voce è scomparsa."


translate italian day7_main_99e05e2b:


    "Certamente, non potevo sapere se la mia scelta fosse stata quella giusta."


translate italian day7_main_f36decb5:


    "È stata una scelta fatta alla cieca, imposta da una limitazione temporale."


translate italian day7_main_f9e8cad7:


    th "Tuttavia, se io abbia fatto la scelta giusta oppure no – lo scoprirò presto."


translate italian day7_main_2c7b78d5:


    "Dopo un paio di minuti ero già alla fermata dell'autobus, assieme a tutti gli altri pionieri."


translate italian day7_main_709d3e7d:


    mt "Ci sono tutti?"


translate italian day7_main_14167bf5:


    "Ha esordito Olga Dmitrievna."


translate italian day7_main_044ab0d8:


    mt "È arrivato per voi il momento di lasciare questo Campo, e vorrei dirvi qualche parola."


translate italian day7_main_00123e37:


    "Era visibilmente nervosa, alla disperata ricerca delle parole giuste."


translate italian day7_main_019ed77a:


    mt "Spero che questa esperienza rimarrà nei vostri cuori per tutta la vita, e che conserverete solo ricordi piacevoli di «Sovyonok»."


translate italian day7_main_b48fe53b:


    mt "Spero anche che questa esperienza vi abbia reso delle persone migliori, che abbiate imparato qualcosa di nuovo, e che vi siate fatti dei nuovi amici qui…{w} Spero… di rivedervi tutti l'anno prossimo."


translate italian day7_main_42256d78:


    "La leader del Campo ha distolto lo sguardo.{w} Sembrava che stesse cercando di trattenere le lacrime."


translate italian day7_main_80682609:


    "Non mi sarei aspettato che potesse essere una persona così emotiva."


translate italian day7_main_f4683d53:


    "Anche se il suo discorso sembrava assolutamente privo di senso per me. Come sempre."


translate italian day7_main_09939b29:


    "I pionieri hanno iniziato a salire sul bus, chiacchierando allegramente."


translate italian day7_main_4bd26034:


    "Ho deciso di fermarmi un attimo e di dare un addio adeguato a questo Campo."


translate italian day7_main_38f849e4:


    "All'improvviso ho sentito il bisogno di lanciare una moneta."


translate italian day7_main_b4d64ac3:


    "Ovviamente non c'era una fontana qui, e sinceramente non avevo voglia di ritornarci, ma è comunque una superstizione, e io non credo alle superstizioni.{w} O almeno non ci avevo mai creduto prima."


translate italian day7_main_f881a31d:


    "Frugando nelle mie tasche, ho trovato solo un paio di carte di caramelle, una matita e un pezzo di carta."


translate italian day7_main_68eaeb3b:


    "Li ho tenuti in mano per un istante, poi mi sono accovacciato, ho appoggiato il foglio per terra e ho scarabocchiato alcune parole."


translate italian day7_main_470a435f:


    "«Sei qui per un motivo»."


translate italian day7_main_4bd38202:


    "Sorridendo alla mia stupidità, ho gettato il foglietto sotto alla ruota del bus e sono salito dentro."


translate italian day7_main_d20ef26b:


    "Ho trovato un posto al centro."


translate italian day7_main_92e96785:


    "La cosa più strana era che tutti si sono seduti a coppie, mentre io ero l'unico da solo."


translate italian day7_main_c59d06a0:


    th "Ma che importa a questo punto?"


translate italian day7_main_a0b475c4:


    th "O scomparirò, o ricomincerà tutto da capo entro un paio d'ore."


translate italian day7_main_a20cefa7_2:


    "..."


translate italian day7_main_b9800de2:


    "Il bus si stava lentamente dirigendo verso il centro del distretto, sobbalzando di tanto in tanto sulle asperità della strada."


translate italian day7_main_60b0a3f1:


    "Era impossibile vedere qualsiasi cosa al di là delle vecchie finestre nel buio pesto della notte."


translate italian day7_main_6cd4a42b:


    "In ogni caso, non ero interessato al paesaggio circostante – mi sono limitato a rimanere seduto e ad attendere l'inevitabile."


translate italian day7_main_637f8d4a:


    "Per la prima volta dopo tanto tempo la mia testa era completamente vuota."


translate italian day7_main_1b370f01:


    "I pionieri intorno a me sembravano apprezzare il viaggio."


translate italian day7_main_eac2b791:


    "Ulyana e Alisa stavano giocando a carte."


translate italian day7_main_18b1408e:


    "Lena stava leggendo un libro, e Slavya stava dormendo."


translate italian day7_main_8be4c497:


    "Miku cercava invano di iniziare una conversazione con Zhenya, e Zhenya stava faticando parecchio nel trattenersi dall'uccidere la gente."


translate italian day7_main_5400ac8b:


    "Electronik e Shurik stavano combinando qualcosa, come sempre."


translate italian day7_main_5ae9bf47:


    "L'unico completamente ignorato da tutti ero io."


translate italian day7_main_6035b571:


    th "Potrebbe essere una percezione un po' inverosimile – mi sono abituato ad essere sempre al centro dell'universo nel Campo, pensando che tutto ruotasse intorno a me."


translate italian day7_main_5ed13ddb:


    th "Ebbene, poteva essere così fino ad un certo punto, ma qui adesso sono solo un oggetto estraneo, una molecola fuori posto nel modello armonico di una struttura cristallina."


translate italian day7_main_a20cefa7_3:


    "..."


translate italian day7_main_d602b53d:


    "Non sono sicuro di quanto tempo abbiamo trascorso viaggiando, ma il sonno aveva già iniziato a farsi sentire."


translate italian day7_main_7f8e5aa9:


    "Stavo combattendo disperatamente contro Morfeo, cercando di rimanere sveglio quanto più a lungo possibile."


translate italian day7_main_8f87d7c9:


    th "Dopo tutto, è molto probabile che oggi sia l'ultimo giorno della mia vita."


translate italian day7_main_2fc046f6:


    th "E quindi probabilmente sarebbe ragionevole per me aggrapparmi a queste ultime ore di questa mia insignificante e inutile esistenza…"


translate italian day7_main_a20cefa7_4:


    "..."


translate italian day7_main_f6b7f9ad:


    "Improvvisamente mi sono ricordato del pezzo di carta e delle poche parole che avevo scritto su di esso."


translate italian day7_main_30775a01:


    th "Dannazione, le ho già viste prima!"


translate italian day7_main_e730bb3c:


    th "Come ho potuto non accorgermene..."


translate italian day7_main_7a69a252:


    th "Forse tutti questi pionieri, tutto quel linguaggio pomposo, stavano offuscando la mia coscienza talmente tanto..."


translate italian day7_main_cba5d1e3:


    "La mia testa è stata letteralmente invasa da uno sciame di domande."


translate italian day7_main_ef6d6baa:


    "Tuttavia, la stanchezza fisica e, cosa ancora più importante, la stanchezza emotiva hanno preso il sopravvento, e mi sono addormentato…"


translate italian day7_un_28b2bda0:


    "In qualche modo mi sono svegliato nel mio appartamento."


translate italian day7_un_a32e02ac:


    "La cosa non ha innescato alcuna emozione in me – un altro semplice giorno della mia esistenza, tutto qui."


translate italian day7_un_7d869d50:


    "Sono sceso dal letto e ho raggiunto il computer."


translate italian day7_un_ec63c684:


    "Una finestra di messaggio stava lampeggiando sulla barra delle applicazioni."


translate italian day7_un_4e652ca0:


    "Mi sono seduto e ho fissato lo schermo."


translate italian day7_un_e275ca5a:


    th "Qualcuno mi ha scritto – e questo significa che qualcuno ha ancora bisogno di me."


translate italian day7_un_3b3f4f87:


    "Il messaggio conteneva una singola parola – «Svegliati»."


translate italian day7_un_002780e6:


    th "Che cosa vogliono da me?{w} Un ID sconosciuto – è forse un messaggio di spam?{w} E cosa significa? – «Svegliati»...?"


translate italian day7_un_315e078b:


    th "Sono già sveglio."


translate italian day7_un_bd7d69e4:


    "La finestra continuava a lampeggiare."


translate italian day7_un_9ba4d0e0:


    "Ho provato a chiuderla, ma senza successo. Invece, è spuntata un'altra finestra.{w} E poi un'altra, e un'altra ancora… E tutte contenevano lo stesso messaggio: «Svegliati»."


translate italian day7_un_c5553e17:


    me "Cosa diavolo volete da me?!"


translate italian day7_un_a5efbe41:


    "Ho urlato."


translate italian day7_un_39ba5ebd:


    "Ben presto l'intero schermo era ricoperto di messaggi identici."


translate italian day7_un_395338c2:


    "Non riuscivo più a sopportarlo, così l'ho afferrato e l'ho fracassato contro il muro, con tutte le mie forze."


translate italian day7_un_61465614:


    "Un telefono ha squillato."


translate italian day7_un_7411cb8f:


    th "Strano, chi potrebbe essere?"


translate italian day7_un_191851cc:


    "Ho alzato la cornetta e ho sentito solo un «Svegliati»."


translate italian day7_un_4f1bf480:


    "All'altra estremità migliaia di voci – uomini, donne, bambini – stavano gridando…"


translate italian day7_un_92dce532:


    dreamgirl "Svegliati!"


translate italian day7_un_e6cd0617:


    "Ho scaraventato il telefono sul pavimento, ma non si è rotto e ha continuato a squillare."


translate italian day7_un_b6784bd0:


    "In quell'istante, l'intera stanza si è riempita di sconosciuti."


translate italian day7_un_1fe77d6f:


    "Afferravano le mie braccia, mi scrutavano negli occhi e urlavano, urlavano…"


translate italian day7_un_92dce532_1:


    dreamgirl "Svegliati!"


translate italian day7_un_9a3170d8:


    "Sono saltato fuori dal mio letto sudando freddo."


translate italian day7_un_658777a7:


    th "Eh… Che sogno…"


translate italian day7_un_f61bd817:


    "Ho ripreso i sensi dopo un paio di minuti e ho dato un'occhiata all'orologio."


translate italian day7_un_0718c34d:


    th "Sono le cinque di sera.{w} Quanto a lungo ho dormito?!"


translate italian day7_un_cd800d7f:


    "Tutti gli eventi di ieri sera sono apparsi nella mia mente.{w} La ricerca di Lena, quella devastante conversazione, Alisa messa KO…"


translate italian day7_un_1a354f35:


    th "Devo assolutamente parlare con lei, forse si è calmata un po' durante la notte."


translate italian day7_un_3f9b887f:


    "Mi sono vestito, sono uscito dalla stanza, ho esitato un po' cercando di raccogliere i miei pensieri, e poi mi sono diretto verso la casetta di Lena."


translate italian day7_un_8e4dce15:


    th "Cosa le dirò, come posso iniziare la conversazione?{w} Non posso semplicemente andare lì e dirle «Ciao, come stai? Sono solo di passaggio»..."


translate italian day7_un_061fbae3:


    th "Non posso farle una ramanzina così su due piedi."


translate italian day7_un_7e701d1f:


    "Senza riuscire a pensare a nulla di utile, ho bussato.{w} Nessuno ha risposto."


translate italian day7_un_6506b798:


    "Ho bussato di nuovo e poi ho tirato la maniglia della porta."


translate italian day7_un_89013315:


    th "Non è chiusa a chiave, che strano."


translate italian day7_un_1ff94a41:


    "Tuttavia non c'era nessuno dentro."


translate italian day7_un_fc00e421:


    th "Beh, ciò significa che Lena non è qui."


translate italian day7_un_f9d3aec1:


    th "Vado a mangiare qualcosa, forse la troverò alla mensa."


translate italian day7_un_ba6405b9:


    "Proseguendo lungo le file di casette, non ho incontrato un singolo pioniere."


translate italian day7_un_ee95ab11:


    th "Anche questo è strano, solitamente è sempre affollato qui in questo momento della giornata."


translate italian day7_un_1ffe59a2:


    "Non c'era nessuno nemmeno alla piazza."


translate italian day7_un_1f9572c5:


    "Arrivato alla mensa, ho iniziato a preoccuparmi sul serio – era l'ora di cena, ma non c'era traccia della solita folla di pionieri affamati."


translate italian day7_un_b2fcee60:


    "Magari fosse solo quello…{w} La mensa era chiusa!"


translate italian day7_un_71d41a98:


    "Sembrava che qualcosa fosse successo durante la giornata."


translate italian day7_un_17d7007b:


    "Mi sono seduto sulla veranda e ho iniziato a pensare alla situazione."


translate italian day7_un_df3b23b2:


    "Tutti i pionieri sono spariti.{w} Senza alcun preavviso."


translate italian day7_un_51b3e31f:


    th "Beh, naturalmente, è difficile dire che questo Campo o il mio modo di arrivare qui siano normali, ma non ho ancora visto nulla di totalmente inspiegabile in tutta questa settimana."


translate italian day7_un_2ea6120c:


    th "C'erano un sacco di cose strane, ma…"


translate italian day7_un_3825b75c:


    me "Forse si stanno nascondendo in quel bunker…"


translate italian day7_un_c25b3dcc:


    "Ho ridacchiato ad alta voce."


translate italian day7_un_8bc30be9:


    "Mentre tutto ciò era molto, per usare un eufemismo, misterioso, ero ancora lontano dall'avere un attacco di panico."


translate italian day7_un_484a3495:


    un "No, se ne sono semplicemente andati."


translate italian day7_un_2df970b0:


    "Mi sono spaventato a morte.{w} Lena stava in piedi accanto a me."


translate italian day7_un_6e7d7566:


    me "Tu… Tu…{w} Non puoi avvicinarti alle persone in questo modo!{w} Mi hai quasi fatto venire un infarto!"


translate italian day7_un_be58af4f:


    un "Scusa…"


translate italian day7_un_1ee77ef5:


    "Ha detto pacatamente."


translate italian day7_un_43dff079:


    "E poi mi sono reso conto di quello che mi aveva appena detto.{w} «Andati»…"


translate italian day7_un_bfcc9a41:


    me "Aspetta, aspetta un secondo! Come sarebbe a dire «se ne sono andati»?!"


translate italian day7_un_0f9ef342:


    un "La sessione è conclusa."


translate italian day7_un_9c158afb:


    me "E la leader del Campo?"


translate italian day7_un_e24368b3:


    un "Aveva alcune faccende da sbrigare in città, così è andata con loro."


translate italian day7_un_a04aa514:


    me "Ok, e tu invece?"


translate italian day7_un_5e51765d:


    un "Io sono rimasta."


translate italian day7_un_9895a7c7:


    "Lena stava parlando con assoluta tranquillità, come se tutto quello che era successo fosse del tutto normale e di routine."


translate italian day7_un_ae14ffa6:


    me "Ma come? Perché?"


translate italian day7_un_0128f27c:


    un "Qual è il problema?{w} Quando la sessione è finita, i ragazzi tornano a casa, non è così?"


translate italian day7_un_630033b9:


    me "E quando è stato deciso che oggi è l'ultimo giorno della sessione?"


translate italian day7_un_35db2fba:


    un "Non ci crederai, ma è stato al primo giorno.{w} Inoltre, Olga Dmitrievna l'ha annunciato all'allineamento."


translate italian day7_un_06a6d311:


    "Già, anche a quegli allineamenti a cui avevo preso parte, l'ultima cosa che mi passava per la testa era ascoltare gli annunci della leader del Campo."


translate italian day7_un_a3b16791:


    me "Allora perché nessuno mi ha detto niente?"


translate italian day7_un_fd89f04c:


    "Avevo atteso talmente a lungo un'occasione per riuscire ad andarmene da questo dannato Campo, e quando finalmente questa si è presentata, ovviamente ci ho dormito sopra..."


translate italian day7_un_325ed1b8:


    th "Beh, questo è esattamente il mio stile."


translate italian day7_un_49d22b71:


    un "Ho chiesto loro di non farlo."


translate italian day7_un_b7baf6a3:


    me "Anche se gliel'avessi chiesto, Olga…{w} Aspetta un secondo! Che cos'hai fatto?!"


translate italian day7_un_49d22b71_1:


    un "Gliel'ho chiesto io."


translate italian day7_un_0dc9f9e8:


    "Ha detto Lena con la stessa fiducia."


translate italian day7_un_ee2b3274:


    me "E poi cosa, hai anche chiesto loro di lasciarmi indietro?"


translate italian day7_un_f1be5fed:


    un "Sì."


translate italian day7_un_65cf65b0:


    me "E non pensi che sia tutto un po'…"


translate italian day7_un_38a6d40e:


    me "TOTALMENTE SBAGLIATO?!"


translate italian day7_un_8807f63e:


    "Ho iniziato a gridare."


translate italian day7_un_1a28e6a0:


    un "No, è perfettamente normale."


translate italian day7_un_55f389f6:


    "Ho squadrato Lena attentamente."


translate italian day7_un_13e71061:


    "Non una singola emozione era apparsa sul suo viso fin dall'inizio della nostra conversazione."


translate italian day7_un_5a1b2d40:


    th "Sembra che la ragazza che sta in piedi proprio davanti a me adesso non sia il tipo di ragazza che ha paura dei grilli, che arrossisce in ogni occasione e che legge romanzi rosa."


translate italian day7_un_a981adad:


    me "Ok, ho capito."


translate italian day7_un_14f2eabe:


    th "Sembra che questa situazione sia direttamente collegata al mio misterioso arrivo in questo Campo."


translate italian day7_un_0b34c373:


    me "Ma tu chi sei?"


translate italian day7_un_0dd1a163:


    un "Io? Questa mattina ero Lena."


translate italian day7_un_0586ca54:


    "Ha risposto pazientemente."


translate italian day7_un_40c0bfd0:


    me "Oh, certo, e io sono Optimus Prime, nato nove milioni di anni fa nelle Highlands scozzesi."


translate italian day7_un_ebd88aea:


    "In quel momento non avevo paura né di lei, né di ogni altra cosa attorno a me."


translate italian day7_un_ddce0400:


    "Stavo solo ribollendo di rabbia."


translate italian day7_un_5d53b103:


    un "Piacere di conoscerti, io sono Lena."


translate italian day7_un_6b20d365:


    me "Ok, ora cerchiamo di essere seri.{w} Penso che quello che stai dicendo sia francamente impossibile."


translate italian day7_un_17a451e0:


    me "Oggi, a quanto pare, è l'ultimo giorno. In qualche modo io non ne sapevo nulla, e la leader del Campo parte assieme a tutti gli altri, lasciandomi qui da solo perché glielo hai chiesto tu."


translate italian day7_un_367d9ad6:


    me "Non trovi che tutto questo sia un po' anormale?"


translate italian day7_un_4d7961b1:


    un "Forse sì…"


translate italian day7_un_e3b83b37:


    me "Mi stai nascondendo qualcosa?"


translate italian day7_un_515af32c:


    un "Forse sì…"


translate italian day7_un_cdd77ebf:


    me "Allora sputa il rospo!"


translate italian day7_un_7540469a:


    un "Ti ho già detto tutto."


translate italian day7_un_cc572823:


    "Ho affondato il viso tra le mie mani, facendo un profondo respiro."


translate italian day7_un_db58107e:


    me "Ok, e cosa dovrei fare io in questa situazione?"


translate italian day7_un_0da946d4:


    un "Non lo so."


translate italian day7_un_fd0ec1e9:


    me "E tu cosa hai intenzione di fare?"


translate italian day7_un_fe61a603:


    un "Non lo so. Mi lascerò trasportare dalle circostanze."


translate italian day7_un_aefb602a:


    me "Beh, ecco le circostanze."


translate italian day7_un_185c618b:


    "Ho sibilato, allargando bruscamente le braccia."


translate italian day7_un_b723e2c5:


    un "Perché ti preoccupi così tanto?"


translate italian day7_un_8f9ecda9:


    "Per la prima volta, un guizzo di interesse è apparso sul suo viso."


translate italian day7_un_e7810ecb:


    me "E come ti aspetti che mi senta? Felice?"


translate italian day7_un_0005cbde:


    un "Beh, non proprio, ma l'hai voluto tu stesso."


translate italian day7_un_c9cfb0f4:


    me "Io? Stai delirando!"


translate italian day7_un_30630964:


    un "E chi ha detto ieri che gli importava di me e che voleva stare con me?"


translate italian day7_un_5cc1cbcf:


    me "E quello che c'entra?"


translate italian day7_un_741edec3:


    "Ho fatto finta di non capire, ma in realtà stavo iniziando a comprendere quali fossero le sue intenzioni."


translate italian day7_un_15749306:


    un "Il tuo desiderio è stato esaudito!"


translate italian day7_un_3b03af3a:


    me "Sì, certo…"


translate italian day7_un_c11bed91:


    "Ho mormorato sottovoce."


translate italian day7_un_5a52a4ba:


    me "Beh, eccomi qui con te.{w} Il che non spiega in alcun modo quello che è successo."


translate italian day7_un_4d28d31b:


    un "Ci sono sempre un paio di trucchi che aiutano nell'intento."


translate italian day7_un_3eac24c4:


    me "Quindi tu…{w} È così che…"


translate italian day7_un_f1be5fed_1:


    un "Sì."


translate italian day7_un_e7732512:


    "Un sorriso è apparso sulle sue labbra."


translate italian day7_un_ac9ef530:


    me "Ahem, se avessi saputo fin dall'inizio che eri così scaltra…"


translate italian day7_un_4be26182:


    "A dire il vero, mi piaceva tutta quella situazione.{w} Beh, non è che mi piacesse – ma ero curioso."


translate italian day7_un_9f8b7dc3:


    th "Naturalmente mi dispiaceva molto di non essere riuscito a fuggire assieme a tutti gli altri.{w} Ma ora siamo solo io e Lena."


translate italian day7_un_a2a35aab:


    th "Questo potrebbe significare che avrò la possibilità di vedere il suo vero volto."


translate italian day7_un_9b37382f:


    "E tutte le risposte che stavo cercando da tanto tempo forse erano proprio lì, e non in qualche misteriosa città dove tutti gli altri erano andati."


translate italian day7_un_6f985085:


    me "Ok, allora che facciamo adesso?"


translate italian day7_un_493d793b:


    un "Noi?"


translate italian day7_un_d1046a49:


    "Ha sorriso."


translate italian day7_un_2a23e310:


    me "Sì, noi.{w} Siamo gli unici rimasti qui.{w} La mensa è ormai chiusa, e ho seri dubbi che sia rimasto del cibo."


translate italian day7_un_46a920a9:


    me "Naturalmente, Olga Dmitrievna potrebbe tornare in un prossimo futuro, ma…"


translate italian day7_un_0efd4712:


    un "Chi se ne frega?"


translate italian day7_un_3d127dd4:


    me "Va bene, non importa.{w} Qualche suggerimento?"


translate italian day7_un_bdb81d42:


    un "Hmm, che cosa vuoi?"


translate italian day7_un_cd14a0d6:


    "Mi ha chiesto con un sorriso sornione."


translate italian day7_un_291eb303:


    me "Vorrei mangiare qualcosa…{w} E poi non saprei."


translate italian day7_un_4af71b46:


    un "Bene, allora andiamo a prendere qualcosa."


translate italian day7_un_25f7abbc:


    me "Dove?"


translate italian day7_un_98dfebb8:


    un "Dovrebbe essere rimasto un po' di cibo nella mia stanza."


translate italian day7_un_8d0f1bba:


    me "Va bene, ci sto."


translate italian day7_un_58427f30:


    "Siamo entrati e Lena ha frugato nel cassetto della scrivania."


translate italian day7_un_2fe68f0d:


    un "Beh, ci sono dei biscotti, ne vuoi un po'?"


translate italian day7_un_3e7a1ceb:


    "Mi ha porto una confezione di biscotti aperta."


translate italian day7_un_d37f7637:


    me "Ah, che carino…"


translate italian day7_un_27c62366:


    "Ho sogghignato."


translate italian day7_un_008527ca:


    un "Perché?"


translate italian day7_un_5cbbe2b2:


    me "Biscotti…{w} Come nella mia infanzia…"


translate italian day7_un_34ea9cb7:


    un "Suppongo."


translate italian day7_un_0ac292f9:


    "Ha sorriso e si è seduta accanto a me."


translate italian day7_un_f71c90f1:


    me "Ebbene?"


translate italian day7_un_3cc85031:


    "Le ho chiesto, con la bocca piena."


translate italian day7_un_3875ba7b:


    un "Che cosa?"


translate italian day7_un_afcb638f:


    me "Quindi cosa facciamo?"


translate italian day7_un_4657d7ce:


    un "Non lo so…"


translate italian day7_un_8063e0c4:


    "Guardava malinconicamente fuori dalla finestra."


translate italian day7_un_c52a621b:


    un "E tu che cosa desideri?"


translate italian day7_un_57ffc5a8:


    me "Io…"


translate italian day7_un_bce388b7:


    th "Ma davvero, cosa desidero?"


translate italian day7_un_f7a69265:


    "Solo mezz'ora fa il mio unico desiderio era quello di allontanarmi da questo maledetto Campo."


translate italian day7_un_b783c75e:


    "Ma ora, proprio qui accanto a lei, qualcosa è cambiato."


translate italian day7_un_7f75f03b:


    me "Beh… Non lo so neppure io…"


translate italian day7_un_f67c85bb:


    un "Pensaci bene."


translate italian day7_un_6d6679e5:


    "Si è avvicinata e mi ha fissato negli occhi."


translate italian day7_un_b97ac4d9:


    me "Beh, io… Sai…"


translate italian day7_un_5036ab46:


    "Il mio viso è diventato rosso, e i miei pensieri erano confusi."


translate italian day7_un_619d5ebf:


    "Non solo perché ero così vicino a una ragazza che voleva chiaramente qualcosa da me, ma soprattutto perché quella ragazza era Lena."


translate italian day7_un_28a06992:


    un "Ebbene?"


translate italian day7_un_1b595140:


    "Mi ha esaminato da vicino."


translate italian day7_un_51149e68:


    me "…"


translate italian day7_un_9e509f53:


    me "Lena, io…"


translate italian day7_un_3875ba7b_1:


    un "Cosa?"


translate italian day7_un_4800ea90:


    "Dire che non sembrava sé stessa è un eufemismo."


translate italian day7_un_aa2b3fa8:


    "Non avevo alcun timore di quella sua metamorfosi. Anzi, avevo più paura di me stesso – di cosa avrei potuto fare in una situazione del genere."


translate italian day7_un_48199bb7:


    me "Io…"


translate italian day7_un_5bf65010:


    un "Non volevi stare con me…?"


translate italian day7_un_609ebc3b:


    "Mi ha sussurrato in un orecchio, in modo seducente."


translate italian day7_un_2dc0d4db:


    me "Sì, lo volevo…{w} Cioè… Lo voglio!"


translate italian day7_un_99cb3823:


    un "Allora qual è il problema?"


translate italian day7_un_5e5a445c:


    me "Sai, è tutto così…{w} Inoltre, ci sono certe questioni…"


translate italian day7_un_9307c403:


    un "Quali questioni?"


translate italian day7_un_182b9bc9:


    "Si è allontanata da me."


translate italian day7_un_f0e8eea2:


    me "Beh…{w} varie… questioni…"


translate italian day7_un_df31a10a:


    un "Non è che forse ti piacciono i… Vero?"


translate italian day7_un_d49ccc82:


    "Lena ha riso."


translate italian day7_un_5c51ac23:


    me "Certo che no, come potresti pensare a una cosa simile!"


translate italian day7_un_eca1df05:


    "Sono arrossito ancora di più."


translate italian day7_un_99cb3823_1:


    un "Allora qual è il problema?"


translate italian day7_un_2f2888eb:


    me "Sei sicura di volerlo?"


translate italian day7_un_feb3b3d8:


    "Ho cercato di cambiare argomento."


translate italian day7_un_ba874842:


    "Dopotutto, appena una settimana fa avevo la mia vita normale. Una vita in cui tutto era ben definito e ben organizzato, e dove non c'era spazio per le ragazze."


translate italian day7_un_f3bf26c2:


    "E ora mi trovavo in una specie di campo misterioso, con Lena seduta accanto a me che stava alludendo a qualcosa."


translate italian day7_un_398b3c5d:


    th "Come dovrei comportarmi in una situazione del genere?"


translate italian day7_un_31b01f67:


    un "Sì…"


translate italian day7_un_a1cfdd9e:


    "Ha aggrottato la fronte."


translate italian day7_un_57db0c16:


    un "Certo che lo voglio, qual è il problema?"


translate italian day7_un_ae17a426:


    me "Beh, è il mio…"


translate italian day7_un_3f78e394:


    "Mi stavo comportando come se non fossi nemmeno diciassettenne (che era come apparivo adesso), come se fossi molto più giovane."


translate italian day7_un_d314a10c:


    un "Anche il mio…"


translate italian day7_un_d1046a49_1:


    "Ha sorriso."


translate italian day7_un_3179bb68:


    "Dovevo decidere cosa fare – ora, dopo che la sessione era terminata e tutti i pionieri erano spariti."


translate italian day7_un_43e408be:


    "Dovevo cercare le risposte."


translate italian day7_un_21a442ca:


    "Soprattutto, dovevo fare attenzione a questa ragazza, che era in grado di trasformare il suo comportamento e anche la sua personalità così facilmente…"


translate italian day7_un_1d5c58e7:


    "Ma in quel preciso istante avevo un solo pensiero nella testa – «Oh, e che diavolo!»"


translate italian day7_un_a755f898:


    me "Beh, se non ti dispiace…"


translate italian day7_un_5cbb3956:


    "Ho chiuso gli occhi."


translate italian day7_un_ca7b7ca9:


    "Non ha detto niente, si è limitata a sorridere e si avvicinata ancora di più."


translate italian day7_un_0684f7af:


    "Le nostre labbra si sono unite in un lungo bacio."


translate italian day7_un_5233788b:


    "In quel momento mi sono scordato di ogni cosa: di questo strano Campo, dei pionieri che se n'erano andati, della nostra stravagante leader…"


translate italian day7_un_4c273632:


    "Mi sono scordato della mia vita passata e pure del mio futuro, se mai sarebbe arrivato."


translate italian day7_un_bddb8f96:


    "In quel momento l'unica cosa che contava era Lena, le sue labbra morbide, il suo calore che si riversava nelle profondità della mia anima, che mi bruciava dentro."


translate italian day7_un_d35a905d:


    "L'ho scostata gentilmente."


translate italian day7_un_3c8c14d4:


    me "Aspetta…{w} Non pensi che sia un po' troppo…{w} affrettato?"


translate italian day7_un_b672514d:


    un "Perché?"


translate italian day7_un_d1317e4f:


    "Ha sorriso e mi ha guardato in un modo tale che mi ha fatto venir voglia di affogare nei suoi occhi."


translate italian day7_un_ef40802e:


    un "Sei sicuro, non è vero?"


translate italian day7_un_9513cd87:


    me "Lo sono…"


translate italian day7_un_3cb55656:


    "Ho sussurrato a bassa voce."


translate italian day7_un_a20cefa7:


    "..."


translate italian day7_un_67d22f65:


    "In quell'esatto momento stavo veramente amando Lena, volevo tenerla stretta e non lasciarla andare mai più."


translate italian day7_un_b59d5f4a:


    "E lei provava lo stesso per me, naturalmente."


translate italian day7_un_94abf778:


    "Il tempo scorreva troppo velocemente, e siamo diventati una cosa sola."


translate italian day7_un_a20cefa7_1:


    "..."


translate italian day7_un_739a1875:


    "Quando mi sono svegliato era già buio."


translate italian day7_un_23b365c0:


    "Sono sceso dal letto, mi sono messo i pantaloni e ho passeggiato attorno alla stanza."


translate italian day7_un_f75f09af:


    th "Cos'è successo?{w} Si è trattato di un semplice istinto animale, o forse qualcosa di più profondo?"


translate italian day7_un_601aa36f:


    th "No, è tutto sbagliato, totalmente sbagliato!"


translate italian day7_un_98050cd0:


    th "Sono bloccato in questo Campo, ho perso la mia occasione per uscire da qui e, cosa ancora più grave, sto con una ragazza che probabilmente ha una doppia personalità e un disturbo maniaco-depressivo."


translate italian day7_un_8f52dae4:


    "Ho guardato la dormiente Lena."


translate italian day7_un_728cd1d4:


    th "In ogni caso, lei è meravigliosa."


translate italian day7_un_eebc563a:


    "Tutto quello che era successo tra di noi solo un paio di ore fa mi è letteralmente balenato davanti agli occhi, e un brivido mi è sceso lungo la schiena."


translate italian day7_un_7858e570:


    th "No, non saprei dire se sia giusto o sbagliato, ma se potessi tornare indietro nel tempo, avrei fatto esattamente la stessa cosa."


translate italian day7_un_ec183f94:


    "Ho fatto un ampio sorriso e mi sono seduto accanto a lei sul letto."


translate italian day7_un_bad_022c8614:


    "Erano quasi le dieci di sera."


translate italian day7_un_bad_1b57fb49:


    th "Beh, probabilmente è il momento di alzarsi."


translate italian day7_un_bad_bfb63a52:


    "Ho scosso delicatamente Lena sulla spalla."


translate italian day7_un_bad_c09b685c:


    "Ha aperto gli occhi."


translate italian day7_un_bad_e7c35764:


    me "Buongiorno. O meglio, buona sera."


translate italian day7_un_bad_e0a6620a:


    un "Ciao."


translate italian day7_un_bad_c861a148:


    "Ha sorriso con tenerezza."


translate italian day7_un_bad_5c767700:


    me "È ora di alzarsi, dormigliona."


translate italian day7_un_bad_ab39f43d:


    un "Sei di fretta?"


translate italian day7_un_bad_33e2c702:


    me "Beh, no…{w} Ma siamo rimasti completamente soli in questo Campo…"


translate italian day7_un_bad_ca19d4ba:


    un "E allora?"


translate italian day7_un_bad_1b595140:


    "Mi ha guardato attentamente."


translate italian day7_un_bad_5e428d20:


    me "Allora, niente…{w} Quando torna Olga Dmitrievna?"


translate italian day7_un_bad_7144803b:


    un "È così importante per te?"


translate italian day7_un_bad_cd66338b:


    "Il suo viso ha assunto un'espressione seria."


translate italian day7_un_bad_9e8dd32e:


    me "Beh, senza cibo moriremo qui."


translate italian day7_un_bad_e0269484:


    "Ho riso."


translate italian day7_un_bad_2a02ff5d:


    un "Sei libero di andare allora."


translate italian day7_un_bad_6556b37c:


    "Ha distolto lo sguardo, girandosi verso la parete."


translate italian day7_un_bad_b48b240c:


    me "Ma come posso andarmene?"


translate italian day7_un_bad_4b07d0b6:


    un "Con l'autobus, ovviamente."


translate italian day7_un_bad_20781912:


    me "Non ci sono autobus qui."


translate italian day7_un_bad_ebc83514:


    un "Allora perché credi che qui ci sia una fermata dell'autobus per la linea 410?"


translate italian day7_un_bad_6821f908:


    me "Francamente, non saprei."


translate italian day7_un_bad_45c95a7d:


    un "Ho detto a Olga Dmitrievna che avevo delle questioni importanti da risolvere qui con te, e che ce ne saremmo andati più tardi."


translate italian day7_un_bad_ad02f53a:


    me "Che cosa?"


translate italian day7_un_bad_1ffdefb7:


    "Era come se un fulmine mi avesse colpito."


translate italian day7_un_bad_cca86205:


    "Non sapevo di cosa essere più sorpreso – del fatto che ci fossero autobus che passavano di qui, o dal fatto che la leader del Campo avesse accettato di abbandonare due pionieri da soli in un campo vuoto così su due piedi."


translate italian day7_un_bad_b97307f7:


    un "Hai sentito bene."


translate italian day7_un_bad_53a970d1:


    me "Vuoi dire che ce ne possiamo andare?"


translate italian day7_un_bad_d6ccd123:


    un "Puoi andartene, nessuno ti costringe a restare."


translate italian day7_un_bad_89256079:


    "Ha detto tutto stando ferma immobile – in realtà, in un modo tale che le sue parole mi hanno dato dei brividi sepolcrali."


translate italian day7_un_bad_526a1798:


    me "Ok, mi dispiace di aver reagito in quel modo... È solo che... Tutto quello che è successo oggi è stata una grossa sorpresa per me."


translate italian day7_un_bad_654ececc:


    un "Non sembravi troppo sorpreso qualche ora fa."


translate italian day7_un_bad_039f555e:


    th "Probabilmente ho detto qualcosa di sbagliato…{w} Qualcosa di totalmente sbagliato."


translate italian day7_un_bad_b47ccf80:


    me "Beh, non offenderti…{w} Non resteremo qui fino alla fine dei tempi, giusto?{w} Se c'è un modo per andarsene…"


translate italian day7_un_bad_cf8bd28b:


    "Lena non ha detto nulla."


translate italian day7_un_bad_4cd94bec:


    "Ho guardato le sue spalle, cercando di capire cosa le passasse per la testa."


translate italian day7_un_bad_1385de95:


    un "Bene!"


translate italian day7_un_bad_3fec7cdf:


    "Ha esclamato allegramente dopo una pausa, e poi è saltata giù dal letto e ha cominciato a vestirsi in fretta."


translate italian day7_un_bad_d9dd4daf:


    un "Dai, prepara le tue cose, ci vediamo alla piazza tra dieci minuti!"


translate italian day7_un_bad_a2242d63:


    "Lena si è chinata e mi ha dato un bacio passionale."


translate italian day7_un_bad_01822020:


    me "D'accordo."


translate italian day7_un_bad_86defb2c:


    "Sono uscito dalla sua stanza e sono corso verso la casetta della leader del Campo."


translate italian day7_un_bad_6ba4f013:


    "Francamente, non avevo quasi nulla da preparare."


translate italian day7_un_bad_d7b1f94e:


    "Ho gettato i miei vestiti invernali in una borsa, mi sono messo il telefono in tasca e mi sono diretto verso la piazza."


translate italian day7_un_bad_a20cefa7:


    "..."


translate italian day7_un_bad_623bcbe0:


    "Quindici minuti erano già trascorsi, ma di Lena ancora nessuna traccia."


translate italian day7_un_bad_b4641094:


    "Probabilmente aveva molte cose da mettere in valigia e, di conseguenza, aveva bisogno di molto più tempo per prepararsi."


translate italian day7_un_bad_a20cefa7_1:


    "..."


translate italian day7_un_bad_66dd03fe:


    "Tuttavia, non si è fatta vedere nemmeno dopo mezz'ora, e ho iniziato a preoccuparmi."


translate italian day7_un_bad_46e4b02b:


    "Le mie gambe mi hanno portato alla sua stanza, ancor prima che me ne fossi reso conto."


translate italian day7_un_bad_e0851da8:


    "Ho spalancato la porta e ho visto Lena sdraiata sul letto."


translate italian day7_un_bad_1d0de26a:


    "Ogni cosa intorno a lei era intrisa di sangue – le lenzuola, la coperta. Il pavimento era bagnato di sangue, e potevo vedere un'enorme lacerazione sul suo avambraccio."


translate italian day7_un_bad_e25e3383:


    "Sono immediatamente corso da lei e ho iniziato a scuoterla per le spalle."


translate italian day7_un_bad_815e4974:


    me "Lena! Lena! Perché?!"


translate italian day7_un_bad_630ef49b:


    "Era ancora cosciente."


translate italian day7_un_bad_e89187e1:


    un "Ciao Semyon."


translate italian day7_un_bad_777137e4:


    "Un debole sorriso si è congelato sulle sue labbra."


translate italian day7_un_bad_9a95d0f0:


    me "Resisti! Ehi, non svenire! Adesso ci penso io! Ascolta, tutto andrà bene! Non morirai!"


translate italian day7_un_bad_200625b6:


    "Ovviamente non potevo sperare in ciò – Lena si era tagliata le vene dal polso fino al gomito."


translate italian day7_un_bad_e56517e3:


    "Era un taglio molto profondo, e dato tutto il tempo che avevo trascorso attendendola alla piazza, aveva sanguinato parecchio."


translate italian day7_un_bad_df119e7d:


    "Probabilmente nemmeno un'ambulanza avrebbe potuto fare molto per salvarla, e ora che il Campo era deserto, lontano dal mondo, Lena non aveva alcuna possibilità di sopravvivere…"


translate italian day7_un_bad_ac6d49df:


    me "Ma quanto puoi essere stupida?!"


translate italian day7_un_bad_57856a8b:


    "L'ho abbracciata forte."


translate italian day7_un_bad_6d0fc2fd:


    "Le lacrime mi scorrevano lungo le guance, scomparendo tra i suoi capelli."


translate italian day7_un_bad_1b1fd4fe:


    "Non ho mai pianto così tanto in tutta la mia vita."


translate italian day7_un_bad_ce1f1f67:


    me "Stolta! Perché dovevi tagliarti lungo il braccio? Tutti lo fanno attraverso il polso – ma tu l'hai fatto lungo il braccio!"


translate italian day7_un_bad_0ab6f7e7:


    un "Perdonami…{w} È successo quello che è successo…"


translate italian day7_un_bad_5820dcba:


    "Ha mormorato debolmente."


translate italian day7_un_bad_3492de92:


    me "Ma perché?! Perché?!"


translate italian day7_un_bad_3a8a8420:


    un "Sono stanca… Sono così stanca…"


translate italian day7_un_bad_c2ddb952:


    "Si è fermata."


translate italian day7_un_bad_abd71de2:


    "L'ho guardata dritto negli occhi – era ancora cosciente, ma l'ultimo guizzo di vita stava rapidamente morendo in lei."


translate italian day7_un_bad_19198e2a:


    un "Sono così stanca di tutto…{w} Stanca di indossare una maschera…{w} Stanca di soffrire…{w} Volevo solo stare con te…{w} Ma mi hai lasciata anche tu…"


translate italian day7_un_bad_b1c8ef56:


    me "Io non ti ho mai lasciata! Guarda, sono proprio qui! Perché?! Cos'hai fatto?!"


translate italian day7_un_bad_0b0df940:


    un "Mi dispiace…"


translate italian day7_un_bad_68679f3d:


    "Le lacrime mi stavano soffocando, non riuscivo a dire nulla."


translate italian day7_un_bad_d2f8b2b7:


    un "Mi dispiace…{w} Ci vedremo…{w} Più tardi…"


translate italian day7_un_bad_359cbafc:


    "L'ho stretta ancora più forte."


translate italian day7_un_bad_3824bfbf:


    "Il respiro di Lena stava diventando sempre più debole, e poco dopo si è fermato del tutto."


translate italian day7_un_bad_4664516e:


    "Inorridito, sono balzato via dal letto."


translate italian day7_un_bad_912ad41d:


    "I miei occhi si sono oscurati, il mio cuore batteva all'impazzata, e ho notato un coltello insanguinato che giaceva sul pavimento."


translate italian day7_un_bad_d7fd24dc:


    "Un attimo dopo, lo tenevo tra le mie mani."


translate italian day7_un_bad_8f9c66f6:


    "La lama si è fermata a un millimetro dal mio polso…"


translate italian day7_un_bad_8a75b802:


    th "Che senso ha? Che cosa risolverebbe?"


translate italian day7_un_bad_9c8a7ab2:


    "Mi sono seduto lì, in completa prostrazione, e mi sono limitato a fissare Lena."


translate italian day7_un_bad_ff9eb876:


    me "No, tu non sei morta."


translate italian day7_un_bad_47037f84:


    "Ho fatto una risata isterica."


translate italian day7_un_bad_675c0b56:


    me "Avanti, dormigliona, è ora di svegliarsi!"


translate italian day7_un_bad_31c290d0:


    "Ho detto dolcemente, scuotendola per le spalle."


translate italian day7_un_bad_123bb3db:


    "Ma Lena non si è svegliata."


translate italian day7_un_bad_c1836b70:


    me "Che cosa… Che…{w} Che cosa ho fatto?!"


translate italian day7_un_bad_7f565988:


    "Mi sono precipitato fuori dalla stanza inorridito, correndo come un matto."


translate italian day7_un_bad_90732d43:


    "Non so quanto tempo fosse passato, ma alla fine ero esausto e sono crollato a terra."


translate italian day7_un_bad_8c7dae16:


    "Un ostile silenzio si era posato tutt'attorno, e solo le stelle mi fissavano con muto rimprovero dall'alto del cielo."


translate italian day7_un_bad_9e1de710:


    "Le stesse stelle che Lena stava ammirando ieri sera…"


translate italian day7_un_bad_a5b7f2b5:


    "E un altro irrefrenabile pianto si è impossessato di me, lacerandomi dall'interno."


translate italian day7_un_bad_c2891e48:


    th "Perché, perché l'ha fatto?! Come l'avrei lasciata?! Dove sono andato?! Io non l'ho mai lasciata, e non avevo intenzione di farlo!"


translate italian day7_un_bad_871cc013:


    "Solo in quel momento mi sono accorto di quanto lei fosse importante per me."


translate italian day7_un_bad_08f145b6:


    "Mi sono reso conto che, nonostante tutti i suoi capricci, nonostante tutto quello che era successo oggi, tutto quello che era accaduto durante il breve periodo in cui ci siamo conosciuti, lei era improvvisamente diventata la cosa più preziosa della mia vita."


translate italian day7_un_bad_daac228f:


    "E io… Ho subito iniziato a ignorarla, a ignorare i suoi sentimenti, non appena ho sentito parlare di quel maledetto bus."


translate italian day7_un_bad_824a58dd:


    "In effetti, non riesco a giustificare il suo atto, ma come ho potuto smettere di pensare a lei…"


translate italian day7_un_bad_ee990b86:


    "Sono rimasto sdraiato lì a lungo, fissando le stelle."


translate italian day7_un_bad_a20cefa7_2:


    "..."


translate italian day7_un_bad_51f4d2a1:


    "Gli alberi stavano ondeggiando pacificamente nella brezza notturna, sopra la mia testa. Agli alberi non fregava proprio niente di quello che mi stava succedendo."


translate italian day7_un_bad_a33a26f0:


    "Il paesaggio sembrava familiare."


translate italian day7_un_bad_3256dae3:


    "Asciugandomi le lacrime, sono tornato verso il Campo."


translate italian day7_un_bad_e2267550:


    "Qui tutto sembrava essere come ieri, come pochi giorni fa…{w} La piazza, il memoriale di Genda… le casette dei pionieri… la casetta di Lena…"


translate italian day7_un_bad_5dd67afb:


    "Ero lacerato dentro."


translate italian day7_un_bad_d4c3458a:


    "Mi sentivo come se il dolore potesse disintegrare il mio corpo in milioni di piccoli pezzi da un momento all'altro."


translate italian day7_un_bad_56824cd3:


    "Sono crollato in ginocchio e ho cominciato a colpire il terreno con i pugni, finché le mie mani non si sono completamente ricoperte di sangue."


translate italian day7_un_bad_54b67798:


    th "Se solo l'avessi capito un po' prima…{w} Solo un istante prima, non chiedo altro…"


translate italian day7_un_bad_3a000f1c:


    th "Lei era così… così…{w} Persino il minimo accenno era sufficiente per lei."


translate italian day7_un_bad_425b26e9:


    "Solo in questo momento mi sono reso conto che Lena era morta.{w} E una parte di me era morta con lei."


translate italian day7_un_bad_dd13b05c:


    "Probabilmente la parte di me che chiamerei la migliore…"


translate italian day7_un_bad_4212818d:


    "Dopo un po' ho ripreso i sensi, ritrovandomi in piedi nella sua stanza."


translate italian day7_un_bad_a42b463d:


    "Le macchie di sangue si erano già seccate, la luce della luna non si rifletteva più in esse."


translate italian day7_un_bad_51c253d9:


    "Sono andato verso il letto e mi sono seduto accanto al corpo di Lena."


translate italian day7_un_bad_3eddd6fc:


    "Avevo il terrore di rimanere lì, ma sentivo il bisogno di dirle qualcosa."


translate italian day7_un_bad_07109f1d:


    me "Mi dispiace."


translate italian day7_un_bad_7cd29b00:


    "Ho iniziato."


translate italian day7_un_bad_eefec37d:


    me "Ormai è troppo tardi, naturalmente, ma se puoi sentirmi lassù da qualche parte, ricorda, per favore, che ti amerò per sempre, per il resto dei miei giorni!"


translate italian day7_un_bad_73cd14af:


    "E quella era la pura verità."


translate italian day7_un_bad_ab86e84f:


    me "Mi dispiace di aver ignorato i tuoi sentimenti. Mi dispiace di aver sempre e solo pensato a me stesso. Mi dispiace per tutto…{w} Ero io colui che sarebbe dovuto morire, non tu."


translate italian day7_un_bad_41e2281b:


    "Ho coperto il suo corpo con un lenzuolo e ho abbandonato lentamente la stanza."


translate italian day7_un_bad_e845b484:


    "Ho ripreso conoscenza alla fermata del bus."


translate italian day7_un_bad_38f3ab16:


    me "E così stai scappando, pezzo di merda?"


translate italian day7_un_bad_02a0d7a5:


    "Ho mormorato cupamente a me stesso."


translate italian day7_un_bad_5684a1f5:


    th "Non potevo sopportare di stare un solo minuto di più in questo Campo."


translate italian day7_un_bad_cfc84616:


    th "Lena non tornerà mai più, non posso giustificarmi per quello che ho fatto.{w} Mi limiterò ad aspettare l'autobus che mi porterà lontano da qui."


translate italian day7_un_bad_fb75b41c:


    "Non m'importava un accidente di quello che sarebbe successo domani, o tra un'ora."


translate italian day7_un_bad_8a6bbc18:


    "Non m'importava delle risposte, non m'importava di come io fossi arrivato qui…"


translate italian day7_un_bad_1167379e:


    "Ben presto ho scorto uno spiraglio di luce fioca in lontananza.{w} Per qualche ragione non ne ero affatto sorpreso."


translate italian day7_un_bad_b2ca87a4:


    "Dopo un minuto stavo seduto sul bus 410 completamente deserto, fissando il buio della notte attraverso un finestrino annebbiato."


translate italian day7_un_bad_238ed2ad:


    "La mia mente era vuota."


translate italian day7_un_bad_ee87ed80:


    "Tutto ciò che ci rende umani – i sentimenti, le emozioni, le aspirazioni, la sofferenza – avevo abbandonato tutto in quel Campo."


translate italian day7_un_bad_27cdd9e3:


    "Tutto ciò che rimaneva era quella notte e l'autobus vuoto."


translate italian day7_un_bad_26c4841e:


    "Non c'era più futuro, non c'era più presente."


translate italian day7_un_bad_eb03b956:


    "Se fossi morto l'indomani, significherebbe solo che un altro corpo umano avrebbe cessato di esistere – il vero «me» era morto {i}laggiù{/i} poche ore fa."


translate italian day7_un_bad_a20cefa7_3:


    "..."


translate italian day7_un_bad_6bb89a09:


    "Non saprei dire quanto tempo fosse trascorso, ma la stanchezza ha preso il sopravvento."


translate italian day7_un_bad_75b8b39e:


    "Non avevo intenzione di combattere contro di essa, dato che dormire o restare svegli non faceva alcuna differenza."


translate italian day7_un_bad_61f61677:


    "Riuscivo a malapena a tenere gli occhi aperti, e ben presto ho perso i sensi…"


translate italian day7_un_good_1e14927c:


    "Non avevo alcun desiderio di svegliare Lena."


translate italian day7_un_good_0f009ba2:


    "Era un piacere indescrivibile – guardarla dormire."


translate italian day7_un_good_cb08a804:


    "Si dice che il sonno riveli il vero volto di una persona."


translate italian day7_un_good_655fad3e:


    "Ho sentito di una leggenda che dice che una qualche civiltà antica aveva una consuetudine: prima di sposarsi, una donna guardava il suo fidanzato dormire, per tre notti."


translate italian day7_un_good_d9e8de3e:


    "E solo dopo decideva se sposarlo oppure no."


translate italian day7_un_good_0d6d6fe6:


    "Considerando ciò, potrei dire con certezza che il viso di Lena non mostrava altro che bontà, gentilezza e un'infantile ingenuità."


translate italian day7_un_good_d1a6079c:


    th "Com'è possibile che sia la stessa ragazza che ha messo KO Alisa ieri sera?"


translate italian day7_un_good_416ed528:


    "Beh, qualunque cosa fosse, ormai era successo."


translate italian day7_un_good_82dadd76:


    "E ho già avuto modo di vedere Lena così…{w} La timidezza nei primi giorni, la crisi isterica quella volta sull'isola, la rabbia in piazza ieri, e oggi la sua passione…"


translate italian day7_un_good_aa392382:


    th "Come può un singolo essere umano gestire così tante personalità contrastanti?"


translate italian day7_un_good_1a2112d1:


    th "Potrebbe forse essere che lei abbia davvero una doppia, tripla, o addirittura quadrupla personalità?"


translate italian day7_un_good_a20cefa7:


    "..."


translate italian day7_un_good_b36c6fa6:


    "Non so quanto tempo io avessi trascorso a guardare Lena dormire.{w} Forse solo un paio di minuti, o forse un paio d'ore."


translate italian day7_un_good_d8d3fddd:


    "Finalmente si è svegliata."


translate italian day7_un_good_c02c0710:


    un "Buongiorno."


translate italian day7_un_good_42545412:


    "Ha sorriso teneramente."


translate italian day7_un_good_74f1716a:


    un "Vieni qui."


translate italian day7_un_good_794fe748:


    "Si è alzata e mi ha abbracciato."


translate italian day7_un_good_39d29f37:


    "Non ho resistito e mi sono lasciato cadere sul letto."


translate italian day7_un_good_5b22e8ef:


    un "Sei stupendo..."


translate italian day7_un_good_d8e711d5:


    "Mi ha sussurrato all'orecchio, baciandomi il collo."


translate italian day7_un_good_a6b25fe7:


    me "Anche tu..."


translate italian day7_un_good_124fb462:


    "Ma la mia voce ha esitato."


translate italian day7_un_good_a2aed382:


    un "Vuoi farlo di nuovo?"


translate italian day7_un_good_c57f6bf1:


    "Ha chiesto scherzosamente."


translate italian day7_un_good_03e5b22a:


    me "Aspetta."


translate italian day7_un_good_d35a905d:


    "L'ho scostata delicatamente."


translate italian day7_un_good_05b8f5af:


    me "Sai, tutto questo è tutt'altro che semplice.{w} Quando ci eravamo appena incontrati, sembravi una certa persona, poi sei sembrata un'altra ancora, e oggi…"


translate italian day7_un_good_4e07d0a8:


    me "Adesso non riesco davvero a capire chi tu sia veramente!"


translate italian day7_un_good_916ed086:


    me "E poi, quello che hai fatto ad Alisa…"


translate italian day7_un_good_16158fef:


    un "Di nuovo lei…"


translate italian day7_un_good_32fed232:


    "Lena si è coperta con il lenzuolo e mi ha dato le spalle."


translate italian day7_un_good_9a7e66a7:


    un "Non ti è piaciuto quello che abbiamo fatto? Vorresti farlo anche con lei? Avanti, io non mi opporrò! Dai, confrontami con lei! Ti lascio il suo indirizzo. Così mi potrai dire chi delle due è la migliore! Oppure potremmo avere un rapporto a tre…"


translate italian day7_un_good_9c1b1178:


    "Era come se non stesse parlando di me o di sé."


translate italian day7_un_good_fa7c02f2:


    "Come se stesse parlando di perfetti sconosciuti."


translate italian day7_un_good_6d308520:


    me "Oh, ci risiamo."


translate italian day7_un_good_9da1463d:


    "Ho sospirato."


translate italian day7_un_good_7ba67c69:


    me "A questo punto dovresti aver già capito che l'ultima persona al mondo a cui vorrei pensare in questo momento – è Alisa."


translate italian day7_un_good_302fa0d4:


    un "Va bene, hai ragione."


translate italian day7_un_good_a1a1f8ca:


    "Si è girata verso di me, ha sorriso e mi ha abbracciato forte."


translate italian day7_un_good_9aed7767:


    me "Aspetta…{w} Devo ancora capire…"


translate italian day7_un_good_41cfffd5:


    un "Perché?"


translate italian day7_un_good_485071ca:


    me "Non posso semplicemente…{w} E sì, per me è importante! Ecco perché ho davvero bisogno di sapere chi sei veramente."


translate italian day7_un_good_91c10784:


    un "Io – sono io."


translate italian day7_un_good_24e34fe5:


    "Ha risposto con la stessa voce languida."


translate italian day7_un_good_b91c9203:


    "I miei freni hanno iniziato a rompersi a causa di tutte quelle emozioni."


translate italian day7_un_good_bf337784:


    th "Devo calmarmi in qualche modo."


translate italian day7_un_good_1c3e144c:


    "Mi sono alzato di scatto, dirigendomi verso il letto di fronte."


translate italian day7_un_good_aed0636a:


    un "Bene…"


translate italian day7_un_good_0dadd0ec:


    "Ha detto in tono deluso, e ha iniziato a vestirsi."


translate italian day7_un_good_52eb706a:


    me "Non ti sto chiedendo molto.{w} Solo poche risposte a poche domande."


translate italian day7_un_good_6e7d937c:


    un "Più tardi. È ora di partire."


translate italian day7_un_good_8dd5c477:


    "Mi ha interrotto, impassibile."


translate italian day7_un_good_1d9b8f65:


    me "Che cosa? Dove stiamo andando?"


translate italian day7_un_good_3149b337:


    un "Che significa, dove? Torniamo in città, ovviamente!"


translate italian day7_un_good_55d5b035:


    me "Ma mi hai detto…"


translate italian day7_un_good_36460bd7:


    un "Beh, forse ti ho tenuto nascosto qualcosa.{w} In realtà, ho convinto Olga Dmitrievna del fatto che avessimo ancora alcune questioni estremamente urgenti qui, e che ce ne saremmo andati più tardi."


translate italian day7_un_good_8a5bbf40:


    me "Come possiamo arrivare in città, allora?"


translate italian day7_un_good_d8473fed:


    un "In autobus, naturalmente."


translate italian day7_un_good_2966b125:


    me "Non ci sono autobus qui."


translate italian day7_un_good_fbd62a8b:


    "Ho sorriso."


translate italian day7_un_good_c78f7540:


    un "Cosa vuoi dire, non ci sono autobus? Certo che ci sono! La linea 410."


translate italian day7_un_good_9491336f:


    "Non sapevo se crederci o no – la situazione sembrava stesse andando fuori controllo – ma non volevo discutere."


translate italian day7_un_good_65bf82fc:


    me "Ah, be-bene…"


translate italian day7_un_good_0897efd9:


    "Ho detto con cautela."


translate italian day7_un_good_a45e6739:


    un "Dai, prepara i bagagli, incontriamoci alla piazza tra dieci minuti."


translate italian day7_un_good_505d91ae:


    me "D'accordo…"


translate italian day7_un_good_345e129a:


    "Ho rinunciato a fare ulteriori domande e sono uscito dalla stanza."


translate italian day7_un_good_63363cb0:


    "Francamente, non avevo quasi nulla da preparare."


translate italian day7_un_good_a692e759:


    "Ho semplicemente gettato il mio abbigliamento invernale in una borsa e mi sono diretto verso la piazza."


translate italian day7_un_good_9c54c577:


    "Lena mi stava già aspettando lì, con una borsa da ginnastica sulla spalla."


translate italian day7_un_good_8aac883d:


    me "Non sembri avere molti bagagli."


translate italian day7_un_good_0562d03f:


    un "Quanto basta."


translate italian day7_un_good_06ab0532:


    "Ha sorriso."


translate italian day7_un_good_7373b9f3:


    me "Lascia che la porti io."


translate italian day7_un_good_735a4f9a:


    un "Sì, grazie."


translate italian day7_un_good_f78985a9:


    "Anche se la sua borsa era leggerissima, un po' di cavalleria non guastava."


translate italian day7_un_good_e265a3a1:


    "Mentre stavamo camminando, Lena raccontava costantemente barzellette, aneddoti, storie divertenti, senza mai smettere di ridere."


translate italian day7_un_good_0dc27b80:


    th "Adesso non so più che tipo di persona io abbia di fronte a me.{w} Lei è quella che ho incontrato una settimana fa, oppure quella con cui mi sono svegliato stamattina?"


translate italian day7_un_good_4cd8fa22:


    "Quando abbiamo raggiunto la fermata del bus, ho tirato fuori il mio telefono cellulare e ho controllato l'ora.{w} Sorprendentemente, c'era ancora un po' di batteria."


translate italian day7_un_good_e43133b4:


    "Erano quasi le undici di sera."


translate italian day7_un_good_8207f0bf:


    th "Non è un po' tardi per gli autobus?"


translate italian day7_un_good_928da381:


    un "Oh, quello cos'è?"


translate italian day7_un_good_d83a6ae8:


    "Ha chiesto con curiosità."


translate italian day7_un_good_1ebb2d15:


    me "Ah, è solo un giocattolo…{w} Prendilo, te lo regalo."


translate italian day7_un_good_f12d0ee9:


    un "Grazie!"


translate italian day7_un_good_b6aed7ee:


    "Ha sorriso e ha preso il telefono."


translate italian day7_un_good_a217d0cd:


    un "Come si gioca?"


translate italian day7_un_good_48e8a886:


    me "Lo capirai da sola. Non è così difficile."


translate italian day7_un_good_bb460699:


    "In ogni caso, un telefono cellulare era del tutto inutile qui."


translate italian day7_un_good_a20cefa7_1:


    "..."


translate italian day7_un_good_43fb154a:


    "Abbiamo aspettato circa mezz'ora e Lena continuava a parlare e parlare."


translate italian day7_un_good_21403291:


    "Beh, devo ammettere che ho trovato le sue storie divertenti e mi sentivo a mio agio con lei, ma dov'era il bus?"


translate italian day7_un_good_71f2297f:


    me "E per quanto riguarda..."


translate italian day7_un_good_7b804235:


    "Non sono riuscito a finire la frase che ho visto il bagliore dei fari in lontananza."


translate italian day7_un_good_083d605d:


    un "Ah, eccolo!"


translate italian day7_un_good_7f656032:


    "Ha esclamato Lena con entusiasmo."


translate italian day7_un_good_429932d4:


    "Il bus ci stava portando lentamente lontano dal campo «Sovyonok». Volevo solo sperare che non ci saremmo tornati mai più."


translate italian day7_un_good_c996f2ed:


    "Il buio oltre la finestra ci ha impedito di vedere la strada, i boschi o i campi – in realtà, poteva anche essere che fossero ormai tutti spariti, e che stessimo volando nel vuoto, verso l'ignoto."


translate italian day7_un_good_c2afe0a0:


    "Comunque non m'importava dell'ambiente circostante – mi sono limitato ad ascoltare Lena."


translate italian day7_un_good_357a77a9:


    "Probabilmente ha detto più parole oggi che in tutta la sua vita passata."


translate italian day7_un_good_9bbf9c21:


    me "Sai…"


translate italian day7_un_good_b756babd:


    "L'ho interrotta alla fine."


translate italian day7_un_good_726d51ff:


    me "Io ancora non capisco…"


translate italian day7_un_good_a604848c:


    un "Che cosa esattamente?"


translate italian day7_un_good_06ab0532_1:


    "Ha sorriso."


translate italian day7_un_good_cbb2f3bd:


    me "Come potevi essere così titubante, così timida in un primo momento, incapace di mettere assieme un paio di parole… E poi invece…{w} Così…"


translate italian day7_un_good_fcc06fdd:


    un "È così importante?"


translate italian day7_un_good_f8a3ddee:


    me "Sì, per me lo è!"


translate italian day7_un_good_88bb1318:


    un "Beh…"


translate italian day7_un_good_d92703f2:


    "Ha fatto un profondo respiro prima di rispondere."


translate italian day7_un_good_440c732d:


    un "Vedi, io sono sempre stata come mi hai visto la prima volta che ci siamo incontrati…{w} Ero così in pubblico."


translate italian day7_un_good_736c9df5:


    un "Non sono mai stata in grado di vivere come avrei voluto, fin da bambina. Quindi… Indossavo una maschera…"


translate italian day7_un_good_b7d1c016:


    "Ha fatto una pausa."


translate italian day7_un_good_0ff7e242:


    un "Comunque... Non scendiamo troppo nei dettagli!"


translate italian day7_un_good_61aec53e:


    "Ha fatto una risata, coccolando il mio braccio, premendosi stretta contro di me."


translate italian day7_un_good_def14ee4:


    me "Ok, capisco…"


translate italian day7_un_good_8e15585f:


    "Probabilmente era molto difficile per lei parlare di questo argomento."


translate italian day7_un_good_c1c67ccd:


    "In ogni caso, avevo capito molto anche da quella sua breve spiegazione."


translate italian day7_un_good_6d3e0546:


    me "Ma comunque…{w} Posso essere sicuro che non ti trasformerai di nuovo in lei…?{w} Che non ti chiuderai nel tuo guscio, che non cederai alla rabbia e alla collera?"


translate italian day7_un_good_daaa0105:


    un "Questo dipende da te."


translate italian day7_un_good_bf744da8:


    "Ha sorriso sornione."


translate italian day7_un_good_1a37d158:


    "E solo allora mi sono reso conto che la posta in gioco era molto alta."


translate italian day7_un_good_62d42734:


    th "Da un lato, ho trascorso una settimana in questo mondo oscuro, limitandomi a cercare delle risposte, non ho niente a mio nome, e non ho alcun posto dove tornare, dannazione!"


translate italian day7_un_good_8ab802ed:


    th "D'altra parte – c'è questa ragazza che ovviamente conta molto per me…"


translate italian day7_un_good_5c4556e4:


    "Non avevo quasi bisogno di chiedermi se provassi qualsiasi cosa nei confronti di Lena."


translate italian day7_un_good_0baeb136:


    "Desideravo solo stare al suo fianco, guardarla, ascoltare la sua voce.{w} Mi piaceva così com'era."


translate italian day7_un_good_fa770e07:


    "Mi sono convinto del fatto che lei fosse proprio così – davanti a me c'era la vera Lena."


translate italian day7_un_good_10603e2c:


    "E ora, quando certi pensieri mi tornavano in mente – la mia vita passata, la mia misteriosa apparizione in un campo di pionieri degli anni ottanta…"


translate italian day7_un_good_fc2a9652:


    th "Non è il momento per tali pensieri!"


translate italian day7_un_good_8b4d1244:


    th "In questo momento sto bene accanto a lei, non voglio tornare da nessuna parte, non voglio alcuna risposta, non voglio pensare a quello che accadrà domani!"


translate italian day7_un_good_2b03d3cf:


    me "Se dipende da me… allora voglio solo credere che sarai sempre come sei adesso. Perché io ti amo così!"


translate italian day7_un_good_f3f46bb0:


    un "Allora rimarrò così finché staremo insieme!"


translate italian day7_un_good_6dd013b2:


    "Mi ha abbracciato ancora più forte."


translate italian day7_un_good_a20cefa7_2:


    "..."


translate italian day7_un_good_46979785:


    "Non so per quanto tempo avessimo viaggiato, ma le chiacchiere di Lena hanno iniziato ad affievolirsi gradualmente."


translate italian day7_un_good_9a1a8533:


    "Ha appoggiato la sua testa sulla mia spalla, ma mi stava ancora raccontando la storia del suo gatto che era riuscito a prendere il sonnifero, che ha avuto le vertigini, scatenando il caos."


translate italian day7_un_good_a7bfc79d:


    "Lentamente ho cominciato ad assopirmi."


translate italian day7_un_good_ad535286:


    me "Ma dimmi una cosa, perché hai colpito Alisa?"


translate italian day7_un_good_20b153f9:


    "Mi ha guardato da vicino con espressione seria."


translate italian day7_un_good_bb6fada3:


    un "Ho sempre detestato quella cagna!"


translate italian day7_un_good_bab1178b:


    "Ha riso forte e si è accoccolata sulla mia spalla."


translate italian day7_un_good_a20cefa7_3:


    "..."


translate italian day7_un_good_a3e8e773:


    "Ho cercato disperatamente di non addormentarmi. Chissà cosa ci aspettava – dopo la prossima curva.{w} Forse una nuova vita, o forse la fine di questa favola – e sarei stato sepolto nuovamente nella bara dei muri scrostati e del soffitto del mio vecchio appartamento."


translate italian day7_un_good_115465f3:


    "Ma era una battaglia persa – Morfeo aveva già convocato legioni di bizzarri mostri sotto il comando di Fatica, Spossatezza, Desolazione e Incertezza."


translate italian day7_un_good_349e1321:


    "Non avevo alcuna possibilità di vincere la battaglia contro questi quattro cavalieri dell'Apocalisse – e sono piombato nel sonno più profondo."


translate italian day7_us_a1b824b0:


    "Probabilmente nella vita ci sono cose peggiori che addormentarsi coccolati da una ragazzina."


translate italian day7_us_85a98e77:


    "La porta si è aperta e uomini armati ci hanno assaltati."


translate italian day7_us_b1bd846f:


    "Non riuscivo a capire quello che stavano urlando, ma non sembravano per nulla amichevoli."


translate italian day7_us_a52663fb:


    "Nah, non avevo paura, per niente.{w} Piuttosto ero imbarazzato, o addirittura provavo vergogna."


translate italian day7_us_dea9e5b0:


    "Quando ho aperto gli occhi mi ci è voluto un po' per capire esattamente dove mi trovassi."


translate italian day7_us_aea7d683:


    "Era buio qui. Solo una luce fioca brillava da sotto la porta."


translate italian day7_us_7be5da68:


    th "Perché costruiscono i magazzini senza le finestre?"


translate italian day7_us_22623e57:


    th "Anche se, ovviamente, è così che devono essere... Ma allora... Perché mai qualcuno dovrebbe tenere un televisore o un videoregistratore qui?"


translate italian day7_us_b5ec365a:


    "Ho maledetto mentalmente gli architetti di questo edificio, e ho scosso Ulyana per le spalle."


translate italian day7_us_8cb5ffe6:


    me "Avanti, alzati subito!"


translate italian day7_us_80c5bc91:


    "Si è stirata, e mentre eravamo sdraiati insieme, sono riuscito a vedere i suoi occhi assonnati anche se era buio."


translate italian day7_us_6d998213:


    us "Co...? Lasciami dormire!"


translate italian day7_us_04545e87:


    "Ulyana ha cercato di scostarsi, ma l'ho afferrata fermamente per la spalla."


translate italian day7_us_77910b1b:


    me "Non so che ore siano, ma dovremmo comunque uscire da qui."


translate italian day7_us_bc4db99c:


    us "Non adesso. Più tardi."


translate italian day7_us_eaa8c8b3:


    "Ha sussurrato, mezza addormentata."


translate italian day7_us_6089f3a9:


    me "Ho detto alzati, andiamo!"


translate italian day7_us_56ce9d85:


    "Mi sono alzato di scatto e l'ho sollevata in piedi con facilità."


translate italian day7_us_47c27e5d:


    us "Uguuu…"


translate italian day7_us_c0371831:


    "Ha gemuto Ulyana, con una nota di frustrazione."


translate italian day7_us_748c57a1:


    "Ho iniziato a cercare l'interruttore della luce, ma improvvisamente ho sentito dei passi oltre la porta."


translate italian day7_us_027c68b0:


    "Ho avuto un tuffo al cuore."


translate italian day7_us_f9b338df:


    el "Perché così presto?"


translate italian day7_us_8023c69c:


    sh "Abbiamo una marea di lavoro da fare, non c'è tempo da perdere! Sai che dobbiamo finire tutto prima della partenza."


translate italian day7_us_324f3179:


    el "D'accordo, va bene…"


translate italian day7_us_918870bf:


    "Sembra che i nostri due elettrizzati cibernetici stessero facendo visita alla sede dei circoli ancora prima dell'alba."


translate italian day7_us_555a7855:


    me "Fa' silenzio!"


translate italian day7_us_413ae5b6:


    "Ho sussurrato a Ulyana."


translate italian day7_us_7d2c0a44:


    us "E qual è il…"


translate italian day7_us_a7fcd40d:


    "Non è riuscita a finire la frase, dato che le ho serrato la bocca con la mano."


translate italian day7_us_5f863e08:


    el "Beh, capisco tutto, ma potremmo almeno aspettare la colazione!"


translate italian day7_us_ac12d769:


    sh "Oh, hai qualcos'altro da fare?"


translate italian day7_us_62543f01:


    el "Non proprio…"


translate italian day7_us_6fe0e4ad:


    "Ho sentito una nota di esitazione nella voce di Electronik."


translate italian day7_us_94943518:


    sh "Sicuro che non vuoi fare di nuovo visita alla biblioteca di prima mattina?"


translate italian day7_us_f9592fdf:


    el "Non ne avevo la minima intenzione…"


translate italian day7_us_bc927332:


    "Gli ha risposto nervosamente."


translate italian day7_us_357f75f0:


    sh "Oh, sì, certo, se lo dici tu."


translate italian day7_us_327920bd:


    "Ben presto hanno dato il via ai lavori dietro la porta – riuscivo a sentire colpi di martello, il crepitio dei macchinari e un ronzio elettronico."


translate italian day7_us_2ca031ea:


    "Electronik e Shurik stavano discutendo delle loro cose, e così non ho prestato molta attenzione a quello che si dicevano."


translate italian day7_us_81c60560:


    "Ero più interessato a sapere quando avrebbero finalmente abbandonato l'edificio."


translate italian day7_us_28532eec:


    th "Tra non molto, dato che l'ora di colazione è imminente.{w} Ma considerando la dedizione di Shurik…"


translate italian day7_us_e1c95826:


    us "Lasciami!"


translate italian day7_us_417eb8f9:


    "Ulyana finalmente si è liberata dalla mia presa, ma comunque senza alzare la voce."


translate italian day7_us_00c87776:


    us "Perché non ce ne possiamo semplicemente andare?"


translate italian day7_us_6c39322b:


    "Ha chiesto sottovoce."


translate italian day7_us_60ae17bd:


    me "E cosa credi – che sia normale?"


translate italian day7_us_38311340:


    us "Qual è il problema?"


translate italian day7_us_9ca91be8:


    me "Beh, abbiamo passato tutta la notte qui insieme…"


translate italian day7_us_2bcd45b7:


    th "Ed è ovvio ciò che penserebbero, considerato che ho preso la precauzione di chiudere la porta."


translate italian day7_us_cbf4636b:


    us "E allora?"


translate italian day7_us_ba0e85e5:


    me "Cosa vuoi dire con «allora»?"


translate italian day7_us_1fd5a2a5:


    "Ho sospirato stancamente."


translate italian day7_us_2c1c12f9:


    me "Fidati di me! So che a te non importa, ma a me sì!"


translate italian day7_us_8b8ddb79:


    us "Ok, va bene, resteremo nascosti qui."


translate italian day7_us_dcea2b7f:


    "Ha concordato con risentimento."


translate italian day7_us_c441e2a5:


    "Ero disposto ad aspettare anche a lungo, se necessario, ma appena un paio di minuti più tardi la porta di ingresso si è aperta e qualcuno è entrato."


translate italian day7_us_ea040539:


    sl "Buongiorno ragazzi!"


translate italian day7_us_073bef57:


    el "Buongiorno!"


translate italian day7_us_c6c5df39:


    sh "Giorno!"


translate italian day7_us_c50dbd79:


    "Era Slavya."


translate italian day7_us_36054e3a:


    sl "Volevo solo chiedervi, per caso avete del nastro adesivo?"


translate italian day7_us_a716e0cd:


    sh "Ce l'abbiamo… Da qualche parte…"


translate italian day7_us_1758adf3:


    "Ha risposto Shurik, pensieroso."


translate italian day7_us_837638ee:


    el "Ah, da' un'occhiata nella stanza sul retro!"


translate italian day7_us_d9880b2f:


    "Quelle parole mi hanno fatto venire la pelle d'oca, e ho afferrato la maniglia della porta in una stretta mortale."


translate italian day7_us_a3e6a343:


    "Slavya si è avvicinata alla porta dal lato opposto, cercando di aprirla, ma io la stavo tenendo bloccata con tutte le mie forze."


translate italian day7_us_9daed956:


    sl "È chiusa."


translate italian day7_us_338f90c7:


    el "Impossibile – non chiudiamo mai a chiave quella stanza!"


translate italian day7_us_96e3c2da:


    sh "Fammi provare."


translate italian day7_us_9a13cece:


    "Shurik ha tirato la maniglia della porta, ma senza successo – anche se ho fatto un grande sforzo per riuscire a tenerla bloccata."


translate italian day7_us_28d6326b:


    sh "Aw, sembra bloccata. Dammi una mano!"


translate italian day7_us_8e62bdbd:


    "Dopo pochi istanti stavano cercando di aprire la porta con Electronik."


translate italian day7_us_3c57cbd3:


    "Ho afferrato la maniglia come se la mia vita intera dipendesse da essa, ma nonostante ciò non sono riuscito a resistere a lungo – le mie braccia hanno ceduto rapidamente, e alla fine ho mollato la presa."


translate italian day7_us_4a18af52:


    "La porta si è spalancata e la forte luce del sole mi ha accecato, non consentendomi di vedere le espressioni sconcertate di Shurik, Electronik e Slavya durante i primi istanti."


translate italian day7_us_0cb3a629:


    el "Ahem… Buongiorno!"


translate italian day7_us_9b07033b:


    us "Giorno…"


translate italian day7_us_db8c0cef:


    "Ulyana stava in piedi dietro a me e quindi non riuscivo a vederla, ma ho potuto sentire l'imbarazzo nella sua voce."


translate italian day7_us_f3289a98:


    sh "E voi che ci fate qui?"


translate italian day7_us_e4baa3e6:


    "Ha chiesto Shurik, quasi come se non fosse sorpreso."


translate italian day7_us_b0e31fb0:


    me "Beh…{w} A dire il vero, stavamo guardando un film. Ulyana ha portato una videocassetta, e dato che c'è un videoregistratore qui…"


translate italian day7_us_f20523f5:


    "Shurik ha fissato con diffidenza le profondità della stanza."


translate italian day7_us_2affcd83:


    "Ho dato a Ulyana una gomitata nel fianco affinché capisse il messaggio, e ha mostrato la videocassetta."


translate italian day7_us_a303a91b:


    el "Che film è?"


translate italian day7_us_05d05fbf:


    "Un ghigno appena visibile è balenato sul volto di Electronik."


translate italian day7_us_8f6c89a0:


    me "Solo un normale film... Un thriller! Uno dei più recenti!"


translate italian day7_us_816f2706:


    "Poi mi sono immaginato quello che lui e gli altri stessero pensando in quel momento, e ho perso le staffe."


translate italian day7_us_8ccefc46:


    me "Se pensate che… noi… Niente del genere!"


translate italian day7_us_48e6007e:


    "Ho puntato il dito contro Ulyana."


translate italian day7_us_d2a8f0f0:


    "Lei a sua volta mi ha guardato con severità."


translate italian day7_us_82fab5b1:


    sl "Nessuno vi sta accusando di niente…"


translate italian day7_us_f3e5d2d3:


    "Ha detto Slavya senza guardarmi."


translate italian day7_us_3129c89c:


    me "Per fortuna che c'è almeno una persona di buon senso tra tutti i presenti!"


translate italian day7_us_cc54c296:


    sl "Eppure…"


translate italian day7_us_7dd3d65d:


    "Ha aggiunto in un sussurro."


translate italian day7_us_ad02f53a:


    me "Che cosa?"


translate italian day7_us_47020024:


    us "È tutto come vi ha detto lui."


translate italian day7_us_29d44d62:


    "Ulyana si è unita alla conversazione."


translate italian day7_us_0313b033:


    us "Stavamo solo guardando un film, e poi ci siamo addormentati… Era così tardi…"


translate italian day7_us_daef1297:


    el "Non abbiamo pensato a niente…{w} È soltanto una stupida situazione, avanti, smettiamola…"


translate italian day7_us_e3a1d5a1:


    "Electronik ha provato a riderci su."


translate italian day7_us_a17efb6c:


    sl "Credo che spetti a Olga Dmitrievna giudicare."


translate italian day7_us_2ff596a2:


    "Ha detto Slavya in tono freddo."


translate italian day7_us_04e9a59a:


    me "Ehi, aspetta! Perché coinvolgere la leader del Campo?"


translate italian day7_us_9f99e4ab:


    sl "E chi altro?"


translate italian day7_us_2b3752c2:


    me "Ma vedi, vi abbiamo detto la verità!"


translate italian day7_us_180a5d04:


    sl "Non sono io a dovervi giudicare…"


translate italian day7_us_2d71c574:


    me "Dannazione! E allora chi?! Hai visto tutto con i tuoi occhi!"


translate italian day7_us_97f0d6ac:


    sl "Sta alla leader del Campo decidere."


translate italian day7_us_11681176:


    "Ha detto a bassa voce, e ha fatto per andarsene."


translate italian day7_us_b93ba56b:


    me "No, aspetta!"


translate italian day7_us_279248eb:


    "Con un balzo mi sono intromesso tra lei e la porta, bloccandole la strada."


translate italian day7_us_04377996:


    me "Ascolta!"


translate italian day7_us_4c8b51f3:


    sl "Questi non sono affari miei…"


translate italian day7_us_a74cf5b3:


    "Slavya stava cercando di evitare il mio sguardo."


translate italian day7_us_a9ab19d6:


    th "Sembra che anche lei sia a disagio per questa situazione."


translate italian day7_us_5b85d78d:


    sl "Mi sento solo in dovere di…"


translate italian day7_us_c7f85ff8:


    me "In dovere verso chi? Perché avresti bisogno di farlo?"


translate italian day7_us_a6fdad47:


    sl "Perché…"


translate italian day7_us_60e65045:


    "Non riusciva a trovare le parole per finire la frase."


translate italian day7_us_aca23380:


    me "Visto? Quindi non c'è bisogno di andare da nessuna parte... Non c'è bisogno di dire niente a nessuno!"


translate italian day7_us_37eba565:


    sl "No…"


translate italian day7_us_c72cecb4:


    "Ha detto nervosamente, ma poi ha alzato la testa e mi ha guardato intensamente."


translate italian day7_us_19d8e410:


    sl "Mi dispiace, Semyon."


translate italian day7_us_fcafb3be:


    us "Lasciala, lascia che vada."


translate italian day7_us_9a93eedf:


    "Mi sono girato verso Ulyana. E quella frazione di secondo è bastata a Slavya per sgattaiolare fuori dalla sede dei circoli."


translate italian day7_us_b93ba56b_1:


    me "Aspetta! Aspetta un secondo!"


translate italian day7_us_c3edfb53:


    "Le ho urlato mentre correva via, ma senza alcun risultato."


translate italian day7_us_10f920b2:


    th "Inseguire Slavya in un futile tentativo di impedirle di andare dalla leader del Campo non ha senso. Se si è decisa a farlo, non posso di certo legarla con la corda…"


translate italian day7_us_f94a034f:


    us "Comunque non ha nessuna prova contro di noi!"


translate italian day7_us_9367d115:


    "Ha ridacchiato Ulyana."


translate italian day7_us_271eed32:


    me "Chi se ne frega se ha le prove o no?{w} Non capisci in che guaio ti sei cacciata?"


translate italian day7_us_de872d99:


    me "In qualsiasi situazione tu possa cacciarti, la tua colpevolezza è sempre scontata.{w} E poi, in una situazione del genere…"


translate italian day7_us_4becc777:


    us "Beh, allora siamo entrambi colpevoli."


translate italian day7_us_5345aad1:


    "Ha sorriso astutamente."


translate italian day7_us_0c53b718:


    me "Esatto…"


translate italian day7_us_767711c4:


    "Sono uscito dall'edificio e mi sono seduto sui gradini."


translate italian day7_us_ca5d2b01:


    el "Beh… Noi saremo dalla tua parte, se proprio dovesse servire. Vero, Shurik?"


translate italian day7_us_f7843a33:


    sh "Direi di sì… Non capisco esattamente cosa sia successo qui, ma probabilmente non è nulla di grave."


translate italian day7_us_06498067:


    el "Va bene, noi andiamo a fare colazione!"


translate italian day7_us_4c751a5e:


    "Poco dopo li abbiamo persi di vista."


translate italian day7_us_b451f12c:


    us "Andiamo a mangiare anche noi!"


translate italian day7_us_2dcff881:


    "Ha detto allegramente Ulyana."


translate italian day7_us_1cbe0f5f:


    me "Pensi solo a mangiare!"


translate italian day7_us_439a68cf:


    us "Perché stare qui a piangersi addosso?{w} Cosa potrebbe cambiare?"


translate italian day7_us_42da4d87:


    "Non posso negare che lei avesse ragione su quel punto."


translate italian day7_us_87f2529e:


    th "Non ci resta che attendere la decisione della leader del Campo."


translate italian day7_us_52e891ef:


    me "Allora andiamo…"


translate italian day7_us_e5f23980:


    "Ci siamo diretti verso la mensa."


translate italian day7_us_e64b885e:


    "L'intero Campo si era riunito per la colazione, ad eccezione di Olga Dmitrievna e di Slavya."


translate italian day7_us_29b1cc7a:


    th "Suppongo sia meglio così."


translate italian day7_us_eabb99b9:


    us "A cosa stai pensando?"


translate italian day7_us_30a8c2b2:


    "Mi ha chiesto allegramente Ulyana, dopo che abbiamo preso i nostri vassoi e ci siamo seduti a tavola."


translate italian day7_us_df42402e:


    me "Alla stessa cosa di prima."


translate italian day7_us_682ca4a2:


    us "E dai, smettila di preoccuparti per niente!"


translate italian day7_us_25e396e3:


    me "Forse per te è niente…"


translate italian day7_us_7ac5a652:


    th "Beh, in realtà, cosa c'è di così brutto in tutto questo?"


translate italian day7_us_dd7a2cb9:


    th "Ogni situazione può essere interpretata sotto diversi punti di vista.{w} Soprattutto se si ha un valido motivo…"


translate italian day7_us_126beeed:


    us "E dai, qual è la cosa peggiore che ci potrebbe capitare?"


translate italian day7_us_44eab9f2:


    me "Sai, la nostra leader del Campo – è un po'… eccentrica."


translate italian day7_us_1a257db4:


    us "Eccen… Beh, forse è vero, ma non abbiamo fatto nulla del genere. Proprio niente!"


translate italian day7_us_6ad7fada:


    me "Spero che ci crederà anche lei…"


translate italian day7_us_edf1846c:


    "Il mio rapporto con Ulyana era migliorato in modo sensazionale ultimamente."


translate italian day7_us_88166c1e:


    "In un primo momento la vedevo solo come una sgradevole e maleducata ragazzina, ma ora stavo cominciando a vedere i lati buoni del suo carattere."


translate italian day7_us_f3d379ac:


    "Anche se non ce n'erano troppi..."


translate italian day7_us_f6c5d224:


    "E ora, proprio quando tutto stava iniziando a funzionare, avrei dovuto affrontare una difficile discussione con la leader del Campo."


translate italian day7_us_da1b7a68:


    th "Forse mi sono semplicemente sognato tutto, e la reazione di Slavya questa mattina è stata solo causata dalla sorpresa…"


translate italian day7_us_a1bda0c8:


    mt "Vi state godendo il vostro pasto?"


translate italian day7_us_2515daf1:


    "Olga Dmitrievna stava incombendo su di me.{w} Mi ha guardato con aria minacciosa."


translate italian day7_us_df6dbf74:


    us "Sì."


translate italian day7_us_ea23ce21:


    mt "Vi spiacerebbe spiegarmi cosa è successo?"


translate italian day7_us_00c2b734:


    me "Spiegarle cosa?"


translate italian day7_us_1a0200f5:


    mt "Beh, per esempio, come siete finiti nel ripostiglio del circolo di cibernetica? Dove avete preso la chiave? Cosa stavate facendo lì dentro?"


translate italian day7_us_40981815:


    "Mi sono ricordato di come avevo ottenuto quella chiave maledetta."


translate italian day7_us_03126c7a:


    th "Dovrei fare quattro chiacchiere con Electronik a riguardo."


translate italian day7_us_75eb26f7:


    me "Se Slavya le ha detto la nostra versione…"


translate italian day7_us_581b4f29:


    us "Stavamo guardando un film!"


translate italian day7_us_483db5e3:


    "Ha esclamato Ulyana con serietà."


translate italian day7_us_7996582c:


    mt "Per l'intera notte?"


translate italian day7_us_17187bef:


    "Ha chiesto con sarcasmo la leader del Campo."


translate italian day7_us_8882dbb1:


    us "E poi mi sono addormentata, e Semyon è rimasto con me."


translate italian day7_us_e8218d47:


    mt "E così, non avevate proprio modo di tornare alle vostre camere?"


translate italian day7_us_d3ad4e8d:


    us "Nemmeno un po'!"


translate italian day7_us_5f234a66:


    mt "E tu che mi dici?"


translate italian day7_us_db73f201:


    me "Beh, suona davvero stupido, ma Ulyana ha ragione."


translate italian day7_us_73f28212:


    mt "E vi aspettate che io vi creda?"


translate italian day7_us_5232e44c:


    me "Beh, è la verità…"


translate italian day7_us_28860bdc:


    mt "Ovviamente non ho le prove per accusarvi di nulla…"


translate italian day7_us_f9fe8dfb:


    "Ha iniziato lentamente la leader del Campo."


translate italian day7_us_78c9d534:


    mt "Ma d'altra parte, tutta questa situazione non è affatto normale. I pionieri non devono comportarsi in questo modo!{w} E ci sono troppe incongruenze nella vostra storia."


translate italian day7_us_c7395410:


    me "Lo comprendiamo…"


translate italian day7_us_64ed00c4:


    "Ho concordato, fatalisticamente."


translate italian day7_us_ed3d54bd:


    mt "Ho deciso quanto segue: Ulyana, sei in punizione, sei confinata nella tua stanza, e deciderò cosa fare di te più tardi."


translate italian day7_us_2b2111cc:


    "Ho guardato Ulyana attentamente.{w} Contrariamente alle mie aspettative, non sembrava turbata."


translate italian day7_us_b346132a:


    mt "Ovviamente non credo che la colpa di tutto questo sia tua, ma ad essere onesti…"


translate italian day7_us_9ac1f10f:


    us "Bene! Sapete dove trovarmi se avrete bisogno di me!"


translate italian day7_us_21586975:


    "Si è alzata immediatamente e si è diretta verso l'uscita."


translate italian day7_us_601dcc29:


    "Olga Dmitrievna non ha cercato di fermarla."


translate italian day7_us_f86f5ead:


    mt "Ci sono sempre così tanti problemi con lei! E ti ha coinvolto in tutto questo…"


translate italian day7_us_463bcf35:


    me "Non riesco proprio a capire in cosa possa avermi coinvolto.{w} E lei sembra essere troppo dura con Ulyana."


translate italian day7_us_86c24609:


    mt "Fammi un esempio di un altro pioniere come lei!"


translate italian day7_us_9b510cae:


    "La leader del Campo si è messa a ridere."


translate italian day7_us_d8a59e1a:


    me "Quando è in punizione per essersi comportata male è un discorso, ma in questo caso…"


translate italian day7_us_b0a68be7:


    mt "In realtà non ho capito bene cosa sia successo, ma il mio dovere è quello di prendermi cura della vostra crescita morale! E questa situazione è piuttosto sospetta. Decisamente sospetta."


translate italian day7_us_e6d5b5bb:


    me "E per quanto tempo ha intenzione di tenerla imprigionata?"


translate italian day7_us_2c3aefa4:


    mt "Non lo so…"


translate italian day7_us_c8efaa8d:


    "Olga Dmitrievna ha riflettuto per un po'."


translate italian day7_us_aa5a8210:


    mt "Oggi è il giorno della partenza.{w} Tuttavia, date le circostanze…"


translate italian day7_us_015aa4b8:


    me "Che cosa? Quale partenza?"


translate italian day7_us_e5e97d5d:


    "L'ho interrotta bruscamente."


translate italian day7_us_2a286c8b:


    mt "La sessione finisce oggi. Questo è l'ultimo giorno."


translate italian day7_us_d3383672:


    me "Err, che cosa...?"


translate italian day7_us_ca88a665:


    "Era l'unica frase che sono riuscito a spremere."


translate italian day7_us_95d81bde:


    th "L'ultimo giorno, il che significa…{w} che sarò finalmente in grado di lasciare questo maledetto Campo!"


translate italian day7_us_148a37fc:


    th "Forse è giunto il momento di porre fine alla mia sofferenza, e di tornare alla mia normale realtà?"


translate italian day7_us_d94530d9:


    me "Ma perché così all'improvviso?"


translate italian day7_us_484d29a2:


    mt "All'improvviso cosa? Ne ho parlato all'allineamento!"


translate italian day7_us_b7dab458:


    "Aveva ragione. Agli allineamenti ero solito dormire o guardarmi intorno, e in realtà non ascoltavo per niente i suoi annunci."


translate italian day7_us_230b0b30:


    me "A che ora?"


translate italian day7_us_f2dd2ae4:


    mt "Verso le cinque di pomeriggio. Non dimenticarti di preparare tutto!"


translate italian day7_us_b8beaef4:


    th "Non ho quasi niente da preparare."


translate italian day7_us_d6c3fd46:


    "Si è alzata, ha preso un vassoio e ha fatto per andarsene."


translate italian day7_us_10a9cce9:


    me "E cosa succederà a Ulyana?"


translate italian day7_us_24e7f6f2:


    mt "Non lo so ancora, te l'ho detto.{w} Lei probabilmente andrà via più tardi."


translate italian day7_us_a1e9eded:


    me "Ma come? Non andrà con tutti gli altri?"


translate italian day7_us_4859697b:


    mt "Ebbene, sì."


translate italian day7_us_9e33a049:


    me "È una cosa normale?"


translate italian day7_us_cb6d083f:


    "Ero davvero sorpreso."


translate italian day7_us_9ee92636:


    mt "Che c'è di sbagliato?"


translate italian day7_us_e037db1b:


    th "Non saprei dire se fosse «sbagliato», ma certamente era piuttosto strano."


translate italian day7_us_4cedb800:


    mt "Beh, io ho ancora del lavoro da fare."


translate italian day7_us_a20cefa7:


    "..."


translate italian day7_us_a075466c:


    "Ho scavato alla cieca nel mio porridge, che si era raffreddato molto tempo fa."


translate italian day7_us_6d8e9a1c:


    th "La partenza, la possibilità di uscire da qui!"


translate italian day7_us_d58d3bbd:


    th "Ma d'altra parte, Ulyana…"


translate italian day7_us_ddc689f2:


    "Mi sentivo in colpa nei suoi confronti."


translate italian day7_us_0b1acb99:


    th "Alla fine, lei è in punizione, mentre io, allo stesso tempo – non lo sono.{w} Questo non è giusto."


translate italian day7_us_2b277b74:


    "No, non è che io volessi condividere la sua miseria, ma non credevo che fosse giusto tenerla chiusa nella sua stanza."


translate italian day7_us_4de1adb3:


    th "Beh, c'è ancora un po' di tempo prima della partenza. Dovrebbe essere sufficiente per riuscire a chiarire la situazione."


translate italian day7_us_743c2089:


    "Per prima cosa, ho deciso di parlare con Slavya."


translate italian day7_us_90ef6742:


    th "Spero che si sia già calmata."


translate italian day7_us_246c591f:


    "Ero abituato a trovare Slavya alla piazza, così sono andato lì senza alcun dubbio."


translate italian day7_us_21e83890:


    th "Perché proprio lì?{w} Perché in questo Campo quello è il luogo in cui la incontro più frequentemente."


translate italian day7_us_b960cd1c:


    "Ma non c'era anima viva nei pressi del rifugio di Genda."


translate italian day7_us_bab2fcfe:


    "Sono rimasto lì per un po', osservando il monumento, poi mi sono diretto verso la biblioteca."


translate italian day7_us_645d78b5:


    "Aveva un certo senso chiedere a Zhenya se sapeva dove fosse la sua compagna di stanza."


translate italian day7_us_d20a5a40:


    "Dopo aver bussato alla porta (ricordando la mia scorsa esperienza, questo non era affatto un vezzo inutile), sono entrato."


translate italian day7_us_a715c94f:


    "Zhenya ha distolto lo sguardo dal suo libro e mi ha squadrato."


translate italian day7_us_6705d775:


    mz "Che vuoi?"


translate italian day7_us_b3e89296:


    me "Cosa c'è che non va? Perché reagisci in quel modo? Non posso nemmeno entrare?"


translate italian day7_us_3f35d5c7:


    mz "Cos'è, sei venuto qui senza motivo?{w} Dubito che tu voglia leggere qualcosa."


translate italian day7_us_ff471f9a:


    me "Beh, no…"


translate italian day7_us_dd5de5c4:


    "I classici del marxismo-leninismo non erano il mio tipo di letteratura preferito."


translate italian day7_us_e0d0e8a3:


    me "Volevo chiederti se sai dov'è Slavya."


translate italian day7_us_41ebc836:


    mz "E perché vorresti saperlo?"


translate italian day7_us_166c7947:


    "L'ha detto come se fosse certa che la conversazione fosse giunta al termine, e quindi ha ripreso a leggere."


translate italian day7_us_9c2b514a:


    me "Beh, dal momento che te lo sto chiedendo, avrei chiaramente bisogno di saperlo."


translate italian day7_us_4101fa65:


    mz "Dovrebbe essere al molo."


translate italian day7_us_6dc67cf3:


    "Mi ha risposto con indifferenza."


translate italian day7_us_73aab3cd:


    me "Grazie…"


translate italian day7_us_0faa275e:


    "Avendo ottenuto quello che volevo, mi sono affrettato a lasciare quella roccaforte del male."


translate italian day7_us_da95a21f:


    th "Già, Zhenya è proprio una ragazza difficile.{w} O almeno, non riesco proprio a capirla."


translate italian day7_us_ba64d8f0:


    "Al molo alcuni pionieri stavano spingendo le barche, mentre altri correvano con in mano remi e corde."


translate italian day7_us_e721198a:


    "Dopo aver guardato più da vicino, ho notato Slavya che stava seduta abbastanza lontano, accanto all'acqua."


translate italian day7_us_f191de9e:


    me "Stai facendo le pulizie?"


translate italian day7_us_757f4d47:


    "Ho fatto la domanda più neutra che mi è venuta in mente."


translate italian day7_us_8b01f8be:


    sl "Sì."


translate italian day7_us_4e965a2e:


    "Mi ha risposto senza voltarsi."


translate italian day7_us_b3a03b6f:


    me "Senti, io…"


translate italian day7_us_89748db4:


    sl "Vorresti parlarmi di Ulyana?"


translate italian day7_us_ff471f9a_1:


    me "Beh, sì…"


translate italian day7_us_5b802725:


    "Francamente parlando, non avevo idea di cosa dirle.{w} Slavya ci aveva scoperto nel ripostiglio, c'è stato un malinteso, e ha riferito tutto alla leader del Campo."


translate italian day7_us_70278229:


    "Adesso la cosa non la riguardava più, a pensarci bene."


translate italian day7_us_752bcc5c:


    th "D'altra parte, sarebbe solo una perdita di tempo parlare con Olga Dmitrievna al momento."


translate italian day7_us_64efd5bd:


    "Forse inconsciamente desideravo capire il ragionamento di Slavya, per essere in grado di assolverla in qualche modo…"


translate italian day7_us_0483babd:


    sl "Allora, cosa volevi dirmi?"


translate italian day7_us_eb47c7e6:


    me "Beh, Ulyana è in punizione.{w} E forse non partirà nemmeno con noi."


translate italian day7_us_ecd25c83:


    sl "Non mi sorprende."


translate italian day7_us_b4a66582:


    me "Voglio solo che tu sappia che non è successo niente di speciale lì dentro."


translate italian day7_us_ff2ee0f7:


    sl "A dire il vero, non saprei. Ho semplicemente dovuto riferire tutto."


translate italian day7_us_c1375773:


    me "E così hai fatto. Ma ha forse migliorato le cose?"


translate italian day7_us_3cc8517a:


    "Ho mormorato a me stesso."


translate italian day7_us_dd320952:


    sl "Certo, non sono sicura che fosse giusto…"


translate italian day7_us_6175a110:


    "Ha detto confusa."


translate italian day7_us_b41d6a32:


    me "Beh, quello che è fatto è fatto…{w} Pensi che sia possibile liberare Ulyana dal confinamento?"


translate italian day7_us_4d657d7b:


    sl "Ti preoccupi troppo per lei."


translate italian day7_us_3568a2ad:


    "Finalmente Slavya mi ha guardato, sorridendo."


translate italian day7_us_c5c058f9:


    me "Non per lei… Mi preoccupo della giustizia!"


translate italian day7_us_b89f54ec:


    "Pur essendo un po' confuso, sono riuscito a trovare le parole giuste."


translate italian day7_us_822ca264:


    sl "Beh, conosci la nostra leader del Campo."


translate italian day7_us_df7535dd:


    me "Eccome, questo è sicuro."


translate italian day7_us_ce7c946a:


    sl "Bisogna solo aspettare. Alla fine forse le passerà."


translate italian day7_us_44dec0d8:


    "Sì, credo che fosse la decisione migliore."


translate italian day7_us_c8c804ac:


    me "Sì, hai ragione."


translate italian day7_us_26dcea4e:


    "Sono rimasto in silenzio accanto a lei per un po'."


translate italian day7_us_f37824e2:


    "Slavya non sembrava desiderosa di continuare la nostra conversazione."


translate italian day7_us_e2cc6a60:


    "Eppure c'era un senso di incompletezza, ma di fronte all'idea che questa situazione di disagio sarebbe durata diverse ore, ho deciso di non intromettermi ulteriormente."


translate italian day7_us_2e597f6a:


    me "Ok, allora io vado."


translate italian day7_us_5f596122:


    sl "Ci vediamo."


translate italian day7_us_d1046a49:


    "Ha sorriso."


translate italian day7_us_7177cc70:


    "Giunto al centro della piazza, ho iniziato a pensare."


translate italian day7_us_733af708:


    th "C'è ancora un sacco di tempo fino alla partenza, e non ho nulla da fare."


translate italian day7_us_0477db27:


    th "Solo ieri, quando mi sembrava che fossi bloccato qui per sempre e che quindi avrei avuto ancora un sacco di tempo libero – anche se in realtà ce n'era veramente poco – sentivo il bisogno di pensare e di agire più in fretta."


translate italian day7_us_f5c36e19:


    th "Ma ora che sono rimaste solo cinque ore dall'abbandonare questo Campo per sempre, non ho la minima idea di come trascorrerle."


translate italian day7_us_60d55935:


    "Ho deciso di fare visita a Ulyana."


translate italian day7_us_cfa9bf4e:


    th "Dopotutto, anche se non le è permesso lasciare la sua stanza, questo non significa che io non possa farle visita."


translate italian day7_us_63ca30ce:


    "Ho bussato delicatamente."


translate italian day7_us_37c07e8b:


    us "Non sei il benvenuto qui!"


translate italian day7_us_61815909:


    "Una voce arrabbiata è risuonata oltre la porta."


translate italian day7_us_4c56125d:


    "Ho tirato la maniglia e sono entrato."


translate italian day7_us_824d3de5:


    me "Salve, prigioniera!"


translate italian day7_us_70b26771:


    us "Oh, sei tu…"


translate italian day7_us_743d2def:


    "Ha detto Ulyana, delusa."


translate italian day7_us_96cb823a:


    me "Cosa c'è, sono l'unico che non è il benvenuto qui?"


translate italian day7_us_15aba1cf:


    "Ho cercato di sorridere."


translate italian day7_us_467b6ff9:


    us "Perché sei venuto?"


translate italian day7_us_43e44e64:


    me "Beh, ho pensato che ti saresti annoiata qui tutta sola."


translate italian day7_us_e7312d00:


    us "Sto bene!"


translate italian day7_us_c33a6926:


    me "Dov'è Alisa?"


translate italian day7_us_67ce2945:


    us "Come puoi vedere, non è qui."


translate italian day7_us_8a6c21b7:


    me "E dai, perché sei così arrabbiata? Stamattina eri molto più allegra."


translate italian day7_us_76aad34e:


    us "Arrabbiata? Io?"


translate italian day7_us_d54c78b5:


    me "Di certo non io, questo è sicuro…"


translate italian day7_us_621db685:


    us "Non avevi nient'altro da fare e così sei venuto qui, vero?"


translate italian day7_us_0d9da0d6:


    me "Sì, colpa mia…"


translate italian day7_us_18e7bfa2:


    "Ho sospirato teatralmente, abbassando la testa."


translate italian day7_us_2259a5d2:


    us "Beh, siediti allora."


translate italian day7_us_83c16d37:


    "Mi sono seduto sul letto di fronte."


translate italian day7_us_f9c0308a:


    us "Allora, dimmi qualcosa."


translate italian day7_us_36e3343b:


    me "Pensiamo a un modo per dimostrare a Olga Dmitrievna che non abbiamo fatto niente di sbagliato!"


translate italian day7_us_26f39e54:


    us "Che «io» non ho fatto niente di sbagliato."


translate italian day7_us_e9d754a4:


    "Mi ha corretto Ulyana."


translate italian day7_us_b1a66e30:


    us "Sembra che tu non abbia niente a che fare con tutto questo."


translate italian day7_us_d9150933:


    me "Va bene, che sia così."


translate italian day7_us_03f66e24:


    us "Ma perché?"


translate italian day7_us_86d84474:


    th "Una domanda difficile."


translate italian day7_us_ecae36f6:


    "Era come se ci fossimo scambiati i ruoli: io le stavo suggerendo un piano stupido, mentre lei era la voce della ragione."


translate italian day7_us_873c6ad1:


    me "Beh, perché non abbiamo fatto niente di male!"


translate italian day7_us_56f9ef5d:


    us "E quale sarebbe il problema? Me ne starò qui per un paio d'ore ed il gioco è fatto. Partiremo a breve comunque."


translate italian day7_us_442bf024:


    "Si è lasciata cadere sul letto e ha iniziato a fissare il soffitto."


translate italian day7_us_3838f56d:


    me "Beh, certo, ma…"


translate italian day7_us_5093b270:


    "Ho provato in tutti i modi a tirarle su il morale, ma sembrava che non ci fossi ancora riuscito."


translate italian day7_us_14e075c5:


    me "Vuoi fare qualcosa, magari?"


translate italian day7_us_6494556f:


    us "È già ora di pranzo."


translate italian day7_us_43b742c4:


    "Ho dato uno sguardo al mio orologio."


translate italian day7_us_ddedc7ac:


    me "Sì, vero."


translate italian day7_us_07a52ab7:


    us "Ecco un lavoretto per te. Dal momento che non posso uscire, vai là e portami qualcosa da mangiare."


translate italian day7_us_6ae432cd:


    me "Signor sì, signore!"


translate italian day7_us_a0ee099d:


    "L'ho salutata e sono corso fuori dalla stanza."


translate italian day7_us_7b5685fb:


    "Ultimamente ho iniziato a pensare che finalmente qualcosa era scattato nella mente di Ulyana."


translate italian day7_us_56f958e0:


    "Forse la punizione ha avuto effetto su di lei, o forse era qualcos'altro."


translate italian day7_us_c059b51b:


    "E anche il mio atteggiamento nei suoi confronti era cambiato."


translate italian day7_us_2a9a781a:


    "Prima le sue malefatte non facevano altro che irritarmi, ma ora c'era anche un po' di comprensione, e una sorta di simpatia."


translate italian day7_us_c2b62820:


    "Dopo tutto, anch'io sono stato ragazzino, in passato."


translate italian day7_us_2326b742:


    th "Forse se qualcuno le spiegasse cosa è giusto e cosa non lo è, probabilmente riuscirebbe ad evitare di commettere molti sbagli."


translate italian day7_us_e9f2b2e6:


    "Alla mensa ho avuto una discussione con la cuoca, che ha rifiutato di darmi una doppia porzione."


translate italian day7_us_749be311:


    "Ad ogni modo, tutti nel Campo sapevano già che Ulyana era confinata nella sua stanza, così alla fine i miei poteri di persuasione hanno vinto contro i regolamenti dietetici."


translate italian day7_us_9c84b285:


    "Ben presto ero seduto nella stanza di Ulyana, mangiando polpette con patate."


translate italian day7_us_19b4f87b:


    me "Proprio come l'ultimo pasto."


translate italian day7_us_e718f922:


    us "Di cosa stai parlando?"


translate italian day7_us_076ef871:


    me "Beh, ogni detenuto nel braccio della morte ha il diritto di esprimere un ultimo desiderio.{w} Così, il mio ultimo desiderio è un pranzo come questo."


translate italian day7_us_6532261a:


    us "Hmm…"


translate italian day7_us_1d2850e0:


    "Con mia grande sorpresa il cibo era davvero delizioso."


translate italian day7_us_1357b107:


    me "E invece quale sarebbe il tuo ultimo desiderio?"


translate italian day7_us_8a3e0f25:


    us "Beh… Di non essere giustiziata, ovviamente!"


translate italian day7_us_ec2c2a7c:


    "Ha riso."


translate italian day7_us_4e8385e3:


    me "Non puoi."


translate italian day7_us_d8734f33:


    us "Perché? Se puoi chiedere qualsiasi cosa…"


translate italian day7_us_d1a58061:


    me "Beh, puoi, ma entro certi limiti."


translate italian day7_us_1b7bd7e3:


    us "Allora non ogni cosa."


translate italian day7_us_ba513381:


    me "Ok, va bene, non ogni cosa…"


translate italian day7_us_2bca74af:


    us "Allora non è così interessante."


translate italian day7_us_a220139e:


    me "Beh, credo che essere prigioniero nel braccio della morte sia già una cosa poco interessante, in primo luogo."


translate italian day7_us_e98bfbde:


    "Ho ghignato."


translate italian day7_us_389622ca:


    us "Non saprei, non ho mai fatto una tale esperienza."


translate italian day7_us_0cf85698:


    "Ma pensandoci, era proprio quella la mia situazione."


translate italian day7_us_8c75e136:


    th "Questo Campo è la mia cella.{w} Ancora per diverse ore sarò in arresto e poi mi attende l'ignoto, proprio come dopo la morte."


translate italian day7_us_72658116:


    th "L'unica differenza è che ho potuto avere una maggiore libertà di scelta. Se volevo partecipare all'allineamento allora lo facevo, altrimenti no…"


translate italian day7_us_9e77a60a:


    me "Che cos'hai intenzione di fare in seguito?"


translate italian day7_us_5f2c0f51:


    us "Cosa intendi dire?"


translate italian day7_us_c9d0b0d9:


    me "Beh, dopo la sessione."


translate italian day7_us_efd76d15:


    "Mi ha guardato sorpresa."


translate italian day7_us_85f9af1a:


    us "Ritorno a scuola, ovviamente."


translate italian day7_us_133710bb:


    th "Già, solo per me lasciare questo posto era un po' come attraversare una barriera, una frontiera, la fine di qualcosa e l'inizio di qualcos'altro."


translate italian day7_us_cbeb66da:


    "Una settimana fa era terribilmente difficile per me capire che ero stato portato via dal mio mondo abituale, e finito Dio solo sa dove.{w} Ma poi mi ci sono abituato."


translate italian day7_us_40e7e33b:


    "E ci risiamo…"


translate italian day7_us_74755571:


    th "In sostanza, l'unica differenza è che ora non provo un senso di paura e di terrore, ma un'ottusa e penetrante sensazione di incertezza."


translate italian day7_us_cdd166bb:


    us "E tu?"


translate italian day7_us_6aabcfa6:


    me "Io… Beh, troverò qualcosa da fare."


translate italian day7_us_94d9cae2:


    us "Qualcosa?"


translate italian day7_us_42be2b6f:


    "È scoppiata a ridere a crepapelle."


translate italian day7_us_90305e74:


    me "Sì, che problema c'è?"


translate italian day7_us_a5f6da24:


    us "Avresti dovuto frequentare una scuola di circo per diventare un pagliaccio!"


translate italian day7_us_5ac38cf1:


    me "Perché?"


translate italian day7_us_79d4d869:


    us "Non si riesce a smettere di ridere solo a guardarti."


translate italian day7_us_ed762271:


    me "Ma perché?"


translate italian day7_us_1c0cb259:


    us "Ti comporti sempre come una sorta di martire, un nuovo messia della madrepatria."


translate italian day7_us_67f0f7a7:


    "Beh, devo dire che c'era un fondo di verità in quelle sue parole."


translate italian day7_us_bb751cb7:


    me "Ho le mie ragioni."


translate italian day7_us_e0608260:


    "Ho mormorato, guardando fuori dalla finestra."


translate italian day7_us_96bedb40:


    us "Quali ragioni?"


translate italian day7_us_e9cde24f:


    me "Varie ragioni…{w} Perché sei così curiosa?"


translate italian day7_us_b72e8852:


    us "Hai dimenticato che sono una ragazzina?"


translate italian day7_us_3aa0974f:


    "Ha sorriso maliziosamente."


translate italian day7_us_31faa27f:


    me "Beh… stare mezz'ora con te è come una terribile tortura per me."


translate italian day7_us_7c214dde:


    us "Senti chi parla! Colui che è sempre di cattivo umore!"


translate italian day7_us_1aff923f:


    me "Cosa te lo fa pensare?"


translate italian day7_us_ebf04f0a:


    us "Sei sempre così immerso nei tuoi esami di coscienza, cercando di scoprire qualcosa, analizzando tutti intorno a te."


translate italian day7_us_1a13d024:


    "Ho fissato Ulyana con stupore."


translate italian day7_us_3637341f:


    "Non mi sarei mai aspettato che una piccola ragazzina come lei fosse in grado di formulare un giudizio così maturo."


translate italian day7_us_5730768c:


    me "...E?"


translate italian day7_us_38197147:


    us "E questo è tutto…"


translate italian day7_us_0d76fe54:


    me "Perlomeno io so come comportarmi – e non mi faccio mettere in punizione."


translate italian day7_us_b9e39a7d:


    us "È una questione di fortuna."


translate italian day7_us_7f6b716b:


    "Ha sorriso."


translate italian day7_us_a7b22250:


    me "Sì, certo!"


translate italian day7_us_26a5c7be:


    us "Se solo ieri notte…"


translate italian day7_us_5e054826:


    "Si è fermata."


translate italian day7_us_fc2d031c:


    me "«Ieri notte» cosa?"


translate italian day7_us_0cdc580c:


    us "Niente…"


translate italian day7_us_7a4616c7:


    me "No, finisci quello che stavi dicendo!"


translate italian day7_us_b58aec3f:


    "Stava per aprir bocca quando abbiamo udito dei passi dietro alla porta, e un secondo dopo Olga Dmitrievna è entrata nella stanza."


translate italian day7_us_aef6989a:


    mt "Ah, eccoti qui. Il che è ancora meglio."


translate italian day7_us_6553b668:


    "Sembrava confusa e senza parole."


translate italian day7_us_cb8b6ba5:


    mt "Beh, ho riconsiderato l'incidente di questa mattina…{w} Non è che sia diventato tutto più chiaro, ma non sembra essere una cosa grave.{w} Così, Ulyana, sei ufficialmente di nuovo in libertà."


translate italian day7_us_0cd3e52f:


    me "Se solo l'avesse capito fin dall'inizio."


translate italian day7_us_901a0e98:


    "Ho mormorato."


translate italian day7_us_574528c5:


    mt "Hai detto qualcosa?"


translate italian day7_us_3c5e2cfc:


    me "No, niente."


translate italian day7_us_73c573c4:


    mt "La partenza è imminente, è ora di fare i bagagli."


translate italian day7_us_8af1d414:


    "Dopo averci detto ciò, ha abbandonato la stanza."


translate italian day7_us_4456980d:


    us "Visto com'è finita bene?"


translate italian day7_us_c63ec5a4:


    me "Già…"


translate italian day7_us_9da1463d:


    "Ho sospirato."


translate italian day7_us_2d5f3035:


    me "Hai intenzione di fare i bagagli?"


translate italian day7_us_f2cfbfb7:


    us "Sì, credo di sì…{w} E tu?"


translate italian day7_us_bad_7d11a631:


    me "Sì, anch'io inizio a preparare le mie cose."


translate italian day7_us_bad_cf3fffbb:


    "Non ha aggiunto altro, e così ho lasciato la sua stanza."


translate italian day7_us_bad_953ac80a:


    th "Beh, sembra che la mia ultima missione in questo Campo sia conclusa – la punizione di Ulyana è stata ritirata, dopotutto."


translate italian day7_us_bad_044c011d:


    th "L'unica cosa che mi rimane da fare è abbandonare questo luogo e prepararmi per qualcosa di nuovo."


translate italian day7_us_bad_782e9fd9:


    "Non appena sono entrato nella stanza della leader del Campo, mi sono dato una rapida occhiata intorno."


translate italian day7_us_bad_9bc29860:


    th "Ho forse qualcosa da preparare?"


translate italian day7_us_bad_cae3ae11:


    "Ho infilato i miei vestiti invernali in una borsa e mi sono seduto sul letto."


translate italian day7_us_bad_b02e82a9:


    "Ero ossessionato da un'angosciate sensazione di incompletezza – sia di déjà vu, che il sospetto di aver scordato qualcosa, ma ancora non riuscivo a capire cosa fosse."


translate italian day7_us_bad_6288575d:


    th "Quando tutto sembra essere giunto al termine, io non sono ancora riuscito a trovare alcuna risposta qui, e quindi dovrò continuare la mia ricerca altrove."


translate italian day7_us_bad_1aa8b92f:


    th "Importa forse il come e il perché io sia arrivato qui, se non esiste una via di fuga?{w} E sembra che nulla dipenda da me in ogni caso."


translate italian day7_us_bad_41fa4bd9:


    "Tutto sembrava più semplice in passato."


translate italian day7_us_bad_5a9fed74:


    "Certo, non avevo grandi prospettive, le mie prospettive erano tutto fuorché strabilianti, ma almeno tutto era più o meno chiaro."


translate italian day7_us_bad_3a418180:


    "Eppure la settimana trascorsa qui ha sollevato più questioni che in tutta la mia vita precedente."


translate italian day7_us_bad_a20cefa7:


    "..."


translate italian day7_us_bad_f98266b4:


    "L'orologio ha colpito le cinque."


translate italian day7_us_bad_d924adad:


    "Ho preso la borsa e mi sono incamminato rapidamente verso la fermata del bus, preoccupandomi del fatto che potesse partire senza di me!"


translate italian day7_us_bad_abfd8483:


    "Sembrava che l'autobus fosse già lì, e con esso tutti i pionieri."


translate italian day7_us_bad_090f0c1d:


    mt "Ci sono tutti?"


translate italian day7_us_bad_14167bf5:


    "Ha iniziato Olga Dmitrievna."


translate italian day7_us_bad_044ab0d8:


    mt "Oggi lascerete questo Campo, e avrei qualche parola da dirvi."


translate italian day7_us_bad_00123e37:


    "Era visibilmente nervosa, e non riusciva a trovare le parole giuste."


translate italian day7_us_bad_019ed77a:


    mt "Spero che questa esperienza vi rimarrà nel cuore per tutta la vita, e che conserverete solo ricordi piacevoli del nostro «Sovyonok»."


translate italian day7_us_bad_b48fe53b:


    mt "Spero anche che questa esperienza vi abbia reso delle persone migliori, che abbiate imparato qualcosa di nuovo, e che vi siate fatti dei nuovi amici qui…{w} Spero… di rivedervi tutti l'anno prossimo."


translate italian day7_us_bad_42256d78:


    "La leader del Campo si è voltata.{w} Sembrava che stesse cercando di trattenere le lacrime."


translate italian day7_us_bad_80682609:


    "Non mi sarei aspettato che potesse essere una persona così emotiva."


translate italian day7_us_bad_f4683d53:


    "Anche se il suo discorso mi è sembrato totalmente insensato. Come sempre."


translate italian day7_us_bad_592d05f0:


    "I pionieri hanno lentamente iniziato a salire sul bus."


translate italian day7_us_bad_5176ce5d:


    "Mi sono guardato attorno tra la folla, in cerca di Ulyana."


translate italian day7_us_bad_422a1788:


    me "Dov'è Ulyana?"


translate italian day7_us_bad_2dd2f5e0:


    mt "Lei non verrà."


translate italian day7_us_bad_0012312c:


    "Ha detto concisamente."


translate italian day7_us_bad_ae14ffa6:


    me "Come? E perché?"


translate italian day7_us_bad_945c2cb1:


    mt "È in punizione."


translate italian day7_us_bad_55986632:


    me "Ma ha detto che…"


translate italian day7_us_bad_f037a948:


    mt "È di nuovo in punizione."


translate italian day7_us_bad_149b3064:


    me "Cosa intende dire?"


translate italian day7_us_bad_76b8fe45:


    mt "Dai, sbrigati a salire, o ce ne andremo senza di te!"


translate italian day7_us_bad_a8a52ea3:


    th "In effetti sono l'ultimo rimasto fuori."


translate italian day7_us_bad_4b08df28:


    me "Aspetti un secondo…"


translate italian day7_us_bad_8d6e2c28:


    mt "Muoviti!"


translate italian day7_us_bad_ef663ab0:


    "Mi ha spinto sul bus.{w} Ho deciso di non discutere."


translate italian day7_us_bad_4c34caf5:


    th "Alla fine, non posso sempre occuparmi di lei! Ci risiamo! Se proprio vuole essere così testarda, allora deve imparare a subirne le conseguenze!"


translate italian day7_us_bad_92535a77:


    "Mi sono seduto all'ultimo posto in fondo."


translate italian day7_us_bad_d1b5df44:


    "Non c'era alcuna persona con cui volessi sedermi in quel momento."


translate italian day7_us_bad_06550b87:


    "Ben presto però Alisa si è seduta accanto a me."


translate italian day7_us_bad_fddf23ae:


    dv "Dov'è Ulyana?"


translate italian day7_us_bad_bd9299cf:


    me "Rimasta…"


translate italian day7_us_bad_69139e02:


    dv "Perché?"


translate italian day7_us_bad_ba766f00:


    me "Punizione…"


translate italian day7_us_bad_ec5a83b6:


    "Le rispondevo a parole singole, senza mostrare alcun interesse per la conversazione."


translate italian day7_us_bad_81056a1f:


    dv "Perché?"


translate italian day7_us_bad_7479b07e:


    me "Boh…"


translate italian day7_us_bad_86e1bfff:


    dv "Aspetta, non hai nemmeno cercato di capire perché?"


translate italian day7_us_bad_d762c781:


    me "Vale forse la pena rischiare di restare indietro?"


translate italian day7_us_bad_72474ea4:


    dv "Avresti dovuto."


translate italian day7_us_bad_b0739623:


    me "Per quale ragione? Se vuoi puoi rimanere. Non siamo ancora molto lontani."


translate italian day7_us_bad_aa5269ba:


    dv "Certo, certo…"


translate italian day7_us_bad_ee6019f2:


    "Ha detto con tranquillità, e se n'è tornata al suo posto."


translate italian day7_us_bad_1ff20cee:


    "Slavya e Zhenya erano sedute sui sedili anteriori, parlando di qualcosa con entusiasmo. Lena, Miku, Shurik ed Electronik erano seduti più vicino a me e stavano giocando a carte. Alisa e qualche altra ragazza stavano leggendo una rivista."


translate italian day7_us_bad_e72a9bb8:


    "Probabilmente ero l'unico che non aveva nulla da fare."


translate italian day7_us_bad_f2666b16:


    "Mi sono accorto di sentirmi ancora un po' in colpa per quello che era successo a Ulyana."


translate italian day7_us_bad_0e4c5769:


    th "Ma cosa avrei potuto fare?{w} Se fossi rimasto con lei, avrei probabilmente perso la mia unica possibilità di andarmene…"


translate italian day7_us_bad_05830ef4:


    th "E nella mia situazione, prendere certi rischi potrebbe rivelarsi fatale!"


translate italian day7_us_bad_13511f52:


    th "E a parte questo, quando è troppo è troppo!"


translate italian day7_us_bad_f86e1ee0:


    "Eppure non riuscivo proprio a smettere di pensare a lei.{w} Che era colpa mia, che ho agito da vigliacco, che ho fatto una cosa brutta.{w} Che chiunque al mio posto avrebbe…"


translate italian day7_us_bad_654261be:


    th "Ma perché?{w} In ogni caso, chi è lei per me, perché dovrei correre dei rischi per lei?"


translate italian day7_us_bad_8ce59d82:


    "Era ancora più difficile rispondere a quella domanda."


translate italian day7_us_bad_7415ba73:


    "Durante la settimana trascorsa qui ho avuto modo di conoscere lei e gli altri abbastanza bene."


translate italian day7_us_bad_a7064953:


    th "Alla fine, in qualche modo mi sentivo responsabile di Ulyana!"


translate italian day7_us_bad_abd762d2:


    th "Ma che senso ha pensarci adesso?"


translate italian day7_us_bad_69057b86:


    th "Sto lasciando questo Campo di pionieri.{w} Sto andando verso l'ignoto."


translate italian day7_us_bad_79fbcf58:


    th "A partire da questo momento, devo prendere le mie decisioni basandomi sul presupposto che questo mondo sia estraneo a me, e molto probabilmente anche ostile."


translate italian day7_us_bad_dd2f1e94:


    th "E io non ho nessuno su cui poter contare."


translate italian day7_us_bad_d80fce90:


    "Anche se, a pensarci bene, tale stato di cose non è una novità per me – sono sempre stato solo in passato…"


translate italian day7_us_bad_a20cefa7_1:


    "..."


translate italian day7_us_bad_e1a1a085:


    "Ormai mi ero lasciato «Sovyonok» dietro alle spalle, la notte è calata su quello strano mondo. Era così buio che era come se l'autobus stesse galleggiando in un oceano nero-bluastro, e solo ogni tanto le cupe foreste e i gli oscuri campi apparivano all'orizzonte, come onde sulla sua superficie."


translate italian day7_us_bad_9c63431a:


    "In ogni caso, il paesaggio era l'ultima delle mie preoccupazioni – ero sprofondato nella mia riflessione."


translate italian day7_us_bad_ef6dcef8:


    "Mi sentivo come se avessi lasciato delle questioni irrisolte laggiù nel Campo."


translate italian day7_us_bad_bdc41394:


    "Anche se non potevo più tornarci ormai."


translate italian day7_us_bad_3dfd000d:


    th "Le cose belle, le cose brutte – tutto sarà presto dimenticato, rimarrà solo la questione del mio arrivo in questo mondo, la fine della mia vecchia vita e l'inizio di quella nuova."


translate italian day7_us_bad_0bf16c1e:


    th "In realtà non è niente di speciale – un semplice Campo di pionieri degli anni ottanta, nel quale sono stato teletrasportato dalla mia epoca…"


translate italian day7_us_bad_cd6651e8:


    th "Comunque è meglio pensare a quello che mi aspetta nel paese dove ci stiamo dirigendo."


translate italian day7_us_bad_6e3dcb07:


    th "Dopotutto, qui non ho un posto dove poter tornare.{w} Non ho una casa, niente soldi, niente amici né parenti."


translate italian day7_us_bad_2560e7b0:


    th "E tutti questi pionieri che se ne andranno nel giro di qualche ora?{w} Non rivedrò nessuno di loro, mai più.{w} E loro? Non credo che si ricorderanno di me tra qualche anno."


translate italian day7_us_bad_34c047f7:


    th "Non è un grosso problema per loro, non mi considerano il viaggiatore nel tempo, ma un ragazzo comune, del tutto simile a loro…"


translate italian day7_us_bad_a20cefa7_2:


    "..."


translate italian day7_us_bad_790bebae:


    "La strada sembrava non finire mai.{w} Quasi tutti i pionieri si erano addormentati da tempo, eppure io stavo ancora combattendo contro la voglia di fare un pisolino."


translate italian day7_us_bad_55c5b97a:


    "Sarebbe stato meglio immergersi nell'ignoto restando svegli, anche se l'ignoto ha sempre un vantaggio in questa battaglia – il flusso del tempo, che è in grado di controllare come meglio ritiene opportuno."


translate italian day7_us_bad_59d8bd33:


    "Aspetti un minuto, un'ora, ma non succede nulla. I nervi, già a pezzi, alla fine cedono allo stress, e non resta che addormentarsi..."


translate italian day7_us_good_0f8e9855:


    me "Non è che io abbia molto da preparare."


translate italian day7_us_good_fa7387e7:


    "In effetti, era la verità."


translate italian day7_us_good_07b57da2:


    us "Ti va di aiutarmi, allora?"


translate italian day7_us_good_011dac7d:


    me "Va bene, certo, perché no?"


translate italian day7_us_good_9e6ddcf0:


    "Ha iniziato a tirare i suoi vestiti fuori degli armadi e ad impilarli sul suo letto."


translate italian day7_us_good_16c1c561:


    me "Fa' attenzione, così li rovinerai tutti."


translate italian day7_us_good_a5b3f692:


    us "Fa niente! Li laverò a casa."


translate italian day7_us_good_8821f1ec:


    "Magliette, gonne, pantaloncini, abiti da sera, calze, scarpe da ginnastica, biancheria intima… La pila di vestiti cresceva ancora e ancora."


translate italian day7_us_good_1b010f5f:


    me "Sei veramente riuscita a portare tutta questa roba da sola?"


translate italian day7_us_good_2312e712:


    "Considerando la gracile costituzione di Ulyana, era difficile da credere."


translate italian day7_us_good_fc656e44:


    us "Sì, certo!"


translate italian day7_us_good_ce3666e5:


    "Ha riso."


translate italian day7_us_good_968191a9:


    us "Dai, dammi una mano."


translate italian day7_us_good_907c8b48:


    "Abbiamo iniziato ad mettere i vestiti in una grande borsa."


translate italian day7_us_good_7128760a:


    "In un primo momento ho cercato di mettere le cose con cura, ma dopo essermi accorto che era del tutto inutile, mi sono limitato a infilare tutto come capitava, giusto per farcelo stare dentro."


translate italian day7_us_good_e48f1d3a:


    "Alla fine non è rimasto più alcun vestito sul letto, e in qualche modo siamo riusciti a chiudere la borsa."


translate italian day7_us_good_848e1ff0:


    us "Beh, questo è tutto."


translate italian day7_us_good_9513cd87:


    me "Già…"


translate italian day7_us_good_153e49e8:


    "Ho guardato l'orologio.{w} Mancavano ancora circa quaranta minuti al momento della partenza."


translate italian day7_us_good_05577344:


    us "Sai, è stato divertente."


translate italian day7_us_good_cb23542f:


    me "Cioè?"


translate italian day7_us_good_150ba4fa:


    us "Beh, questa settimana, è stata divertente."


translate italian day7_us_good_75e0e5f1:


    me "Ah, già…"


translate italian day7_us_good_f2170060:


    "Ho detto distrattamente."


translate italian day7_us_good_734b6aae:


    us "Non credi?"


translate italian day7_us_good_665c7c6a:


    me "Perché…?"


translate italian day7_us_good_68413a8d:


    us "Non mi sembri molto sincero."


translate italian day7_us_good_b2352a58:


    me "Beh, devo ammettere che non sono esattamente al settimo cielo, già."


translate italian day7_us_good_14cb44fc:


    us "Ma cosa non ti è piaciuto?"


translate italian day7_us_good_5839d87f:


    th "Non è che io possa rivelarle ogni cosa proprio adesso…"


translate italian day7_us_good_d2eaf470:


    me "Vedi… Un paio di situazioni qui sono state, come potrei dire, un po' inaspettate."


translate italian day7_us_good_2ee7e228:


    us "Blah, sei così noioso…"


translate italian day7_us_good_0c55cf94:


    "Mi ha detto Ulyana, voltandosi."


translate italian day7_us_good_34c3e0de:


    me "Beh, che cosa ti aspettavi da me?"


translate italian day7_us_good_cac3332a:


    us "Sembra che tu non abbia proprio nulla da ricordare di questa esperienza."


translate italian day7_us_good_b654b924:


    me "Certo, c'è molto da ricordare."


translate italian day7_us_good_5154e89b:


    "Ho sorriso."


translate italian day7_us_good_ac91a0ae:


    us "Sì, è proprio quello che intendo dire!"


translate italian day7_us_good_e24147c1:


    "Mi ha guardato da vicino.{w} Il che mi ha messo un po' a disagio."


translate italian day7_us_good_f3274282:


    me "Che c'è?"


translate italian day7_us_good_f68e9feb:


    us "E di me, ti ricorderai di me?"


translate italian day7_us_good_39fd95ae:


    me "Certo che mi ricorderò di te. Mi ricorderò di tutti…"


translate italian day7_us_good_7b24e7b5:


    us "Di tutti…"


translate italian day7_us_good_d445de56:


    me "E di te – più di chiunque altro!"


translate italian day7_us_good_55ab6c4e:


    "Ho fatto il saluto, stando sull'attenti con la mano sul cuore."


translate italian day7_us_good_7827470a:


    us "Così va meglio!"


translate italian day7_us_good_f3bc15be:


    th "Tuttavia, ho davvero qualcosa da ricordare di lei?"


translate italian day7_us_good_37b15a62:


    th "È vero, abbiamo vissuto un sacco di «avventure» insieme, ma chi è Ulyana per me?{w} Solo una ragazzina ficcanaso di un'altra realtà…"


translate italian day7_us_good_b532bb7b:


    th "Ripensandoci però, mi importa davvero della mia situazione adesso?{w} Sin dal primo momento quando sono apparso qui, tutto è cambiato drasticamente, e così adesso sono solo curioso di sapere che cosa mi aspetta in futuro."


translate italian day7_us_good_2104e6d9:


    th "Certo, l'ignoto non è la migliore delle prospettive, ma devo ammettere che si tratta di una prospettiva mozzafiato."


translate italian day7_us_good_78852785:


    th "Se tornerò al mio mondo o meno non spetta a me."


translate italian day7_us_good_119d1deb:


    th "Ciò significa che io in realtà non ho alcuna possibilità di scelta – devo adattarmi alle condizioni di vita di questo mondo."


translate italian day7_us_good_3c4827d3:


    "E una di quelle condizioni era seduta proprio di fronte a me, con un ampio sorriso."


translate italian day7_us_good_77a7cf13:


    me "Sai, non sei poi così sciocca come mi sei sembrata all'inizio."


translate italian day7_us_good_0f029e42:


    us "Perché mai avresti dovuto considerarmi sciocca, in primo luogo?"


translate italian day7_us_good_3db311fc:


    "Mi ha chiesto, in tono offeso."


translate italian day7_us_good_2824e5e8:


    me "Sto scherzando."


translate italian day7_us_good_27972fbd:


    us "Tu e i tuoi scherzi…"


translate italian day7_us_good_96ba4ae7:


    me "Va bene, è arrivato il momento."


translate italian day7_us_good_89d3eb13:


    us "Andiamo."


translate italian day7_us_good_5b0783c9:


    "Ha detto felicemente, indicando la sua borsa."


translate italian day7_us_good_f2e007f0:


    me "Già…"


translate italian day7_us_good_379e3d71:


    "Mi sono caricato sulle spalle le sue cose.{w} Tutto quel peso mi ha quasi piegato in due."


translate italian day7_us_good_fd618572:


    "Ero grato per il fatto che la fermata dell'autobus fosse a poche centinaia di metri di distanza."


translate italian day7_us_good_6ef81a93:


    "Ho spinto la sua roba sul bus, e poi mi sono affrettato a raggiungere il mio modesto bagaglio."


translate italian day7_us_good_fee09454:


    "Dopo qualche minuto tutti i pionieri si sono radunati lì."


translate italian day7_us_good_090f0c1d:


    mt "Ci siamo tutti?"


translate italian day7_us_good_14167bf5:


    "Ha esordito Olga Dmitrievna."


translate italian day7_us_good_044ab0d8:


    mt "È arrivato per voi il momento di lasciare questo Campo, e vorrei dirvi qualche parola."


translate italian day7_us_good_00123e37:


    "Era visibilmente nervosa, e non riusciva a trovare le parole giuste."


translate italian day7_us_good_019ed77a:


    mt "Spero che questa esperienza vi rimarrà nel cuore per tutta la vita, e che conserverete solo ricordi piacevoli del campo «Sovyonok»."


translate italian day7_us_good_b48fe53b:


    mt "Spero anche che quest'esperienza vi abbia reso delle persone migliori, che abbiate imparato qualcosa di nuovo, e che vi siate fatti dei nuovi amici qui…{w} Spero… di rivedervi tutti l'anno prossimo."


translate italian day7_us_good_42256d78:


    "La leader del Campo si è voltata.{w} Sembrava che stesse trattenendo le lacrime."


translate italian day7_us_good_6a707c88:


    "Non mi sarei aspettato che potesse diventare così emotiva, ma ero completamente d'accordo con ogni sua parola."


translate italian day7_us_good_6888cb6b:


    "Forse è stata la prima volta che il suo discorso non mi è semplicemente volato sopra alle orecchie."


translate italian day7_us_good_7400c0ae:


    "Ben presto tutti sono saliti a bordo del bus.{w} Mi sono seduto all'ultimo posto, assieme a Ulyana."


translate italian day7_us_good_e3f34bf2:


    "In prima fila erano sedute Slavya e Zhenya, poi, un po' più vicino a noi, Lena, Miku, Electronik e Shurik stavano giocando a carte.{w} Alisa era stravaccata sul sedile solo due file più avanti da noi – era da sola."


translate italian day7_us_good_69fc4c0b:


    me "È finita un po' male per la tua coinquilina…"


translate italian day7_us_good_7c056817:


    us "Va bene così! Tu mi hai aiutata a preparare la borsa e a portarla qui."


translate italian day7_us_good_8f838576:


    "L'ho guardata attentamente."


translate italian day7_us_good_fb2f522b:


    th "Sembra che questo piccolo demone abbia cambiato in modo radicale il suo atteggiamento nei miei confronti."


translate italian day7_us_good_607b9495:


    th "Forse potrei essere diventato… un amico per lei?{w} Una parola strana che sembrava aver perso il suo significato per me, molto tempo fa."


translate italian day7_us_good_ebd06a5c:


    th "Non riesco a ricordare nessuno dei miei conoscenti, quando ancora ne avevo, che potrei definire «amico».{w} Forse neppure ai tempi della scuola…"


translate italian day7_us_good_ef14ed90:


    th "E adesso qualcuno mi considera un amico…"


translate italian day7_us_good_d54d3387:


    th "In ogni caso, che cosa significa tutto questo per me?"


translate italian day7_us_good_d742ed4f:


    "Mi sono sempre sentito a mio agio pensando a cose astratte, a prospettive lontane e a questioni globali, piuttosto che a semplici faccende ordinarie."


translate italian day7_us_good_70b35443:


    "E infatti, per tutto il tempo che ho trascorso in questo Campo, io e Ulyana siamo diventati amici intimi."


translate italian day7_us_good_dadbd740:


    "Lei è riuscita a risvegliare in me quei sentimenti sepolti da tempo."


translate italian day7_us_good_5cbb42fa:


    "Perché è questo ciò che significa essere compagni o amici."


translate italian day7_us_good_d53e2161:


    "Ho sorriso e le ho accarezzato dolcemente i capelli."


translate italian day7_us_good_d8ad57a4:


    us "E questo per cos'è?"


translate italian day7_us_good_d946b833:


    "Ha messo il broncio."


translate italian day7_us_good_616b5736:


    me "Così!"


translate italian day7_us_good_36244ee4:


    us "Pervertito!"


translate italian day7_us_good_9ddb8918:


    us "Mi vuoi sposare quando sarò grande?"


translate italian day7_us_good_8288e06d:


    me "Certamente!"


translate italian day7_us_good_969c2b47:


    us "Ci tengo!"


translate italian day7_us_good_3362bb2d:


    me "Oki-doki!"


translate italian day7_us_good_6fc26df0:


    us "Andiamo a giocare a carte con loro?"


translate italian day7_us_good_3fbaf00b:


    me "Perché no!"


translate italian day7_us_good_60a78af5:


    "Ci siamo riuniti intorno alla valigia che veniva usata come tavolo."


translate italian day7_us_good_72f8664f:


    "Ben presto anche Alisa si è unita a noi."


translate italian day7_us_good_f9ee83c7:


    "Ho riso molto, facendo battute e semplicemente godendomi il fatto di essere in uno stato che solitamente si chiama «felicità».{w} Una felicità genuina, proprio qui e adesso."


translate italian day7_us_good_3105c441:


    "In quel momento, il gruppo di pionieri con i quali ero riuscito a fare amicizia in quella breve settimana erano un milione di volte più importanti per me rispetto allo scoprire come fossi arrivato lì o come me ne sarei potuto andare."


translate italian day7_us_good_32e7a990:


    th "Alla fine, perché mai dovrei cercare di tornare indietro?"


translate italian day7_us_good_a20cefa7:


    "..."


translate italian day7_us_good_07039b05:


    "Si stava facendo buio.{w} La partita era finita da un pezzo, e pionieri erano tornati ai propri posti."


translate italian day7_us_good_b2a3af4b:


    "Non avevo idea di quanto tempo ci volesse per raggiungere il centro del distretto, ma sembrava un'eternità."


translate italian day7_us_good_5fd5c146:


    "Fuori dai finestrini dell'autobus un buio pesto mi stava fissando, quasi consumando l'intero mondo, comprimendosi al solo spazio occupato dall'Ikarus."


translate italian day7_us_good_8d851fd8:


    "In ogni caso, il paesaggio era l'ultima delle mie preoccupazioni – mi stavo godendo il momento."


translate italian day7_us_good_4ba60f47:


    th "Sembra che questa realtà sia del tutto normale."


translate italian day7_us_good_7e661857:


    th "E importa davvero come io sia arrivato qui se le cose si rivelano essere così belle? Sono diventato un'altra persona e ho incontrato nuovi amici."


translate italian day7_us_good_eabb99b9:


    us "A cosa stai pensando?"


translate italian day7_us_good_94622b60:


    me "Alla vita."


translate italian day7_us_good_c8e68f82:


    us "E come sta andando?"


translate italian day7_us_good_d1bfd0cf:


    me "Alla grande!"


translate italian day7_us_good_fae4f64a:


    "Ulyana ha riso a bassa voce."


translate italian day7_us_good_f4357af5:


    th "Non ho un posto dove tornare, e quindi posso scegliere qualsiasi percorso di vita io voglia."


translate italian day7_us_good_9e5ffe85:


    th "È vero, ci separeremo presto e probabilmente non rivedrò mai più la maggior parte di loro, ma resteremo per sempre amici!"


translate italian day7_us_good_d097bca7:


    "Ho sentito un piacevole calore attraversarmi il corpo."


translate italian day7_us_good_0a73f977:


    "Mi sono sentito nuovamente come un ragazzino."


translate italian day7_us_good_76539999:


    "Ulyana ha appoggiato la sua testa sulla mia spalla, e subito si è addormentata."


translate italian day7_us_good_a20cefa7_1:


    "..."


translate italian day7_us_good_481a603e:


    "A volte ci si può sentire stanchi non solo a causa del duro lavoro o di pensieri tristi, ma anche per divertimento, gioia e felicità."


translate italian day7_us_good_8cffdf27:


    "Probabilmente si vorrebbe continuare ma non si hanno le energie per farlo, con la propria anima che desidera riposare e il corpo che chiede un po' di quiete."


translate italian day7_us_good_59cfc807:


    "Mi sono addormentato con il sorriso stampato sul volto."


translate italian day7_dv_34b34bb6:


    "Delle persone mi stavano inseguendo…"


translate italian day7_dv_2fb6d076:


    "O forse non erano persone, ma macchie nere sfocate, con un paesaggio infernale come sfondo."


translate italian day7_dv_56a7165e:


    "Io correvo... correvo, inciampavo in qualcosa, mi mancava l'aria."


translate italian day7_dv_c534a505:


    "Tutta la mia essenza è stata sopraffatta dalla paura primordiale e dal terrore."


translate italian day7_dv_f6c12f28:


    "E poi un vuoto di memoria…"


translate italian day7_dv_a20cefa7:


    "..."


translate italian day7_dv_81b5abc9:


    "Ho aperto i miei occhi gonfi affinché essi potessero essere colpiti dalla luminosa luce diurna."


translate italian day7_dv_66a25612:


    "Un sapore terribile in bocca, dolori in tutto il corpo e un forte mal di testa – a quanto pare qualcuno aveva esagerato ieri sera…"


translate italian day7_dv_130627c2:


    "Dopo aver ripreso i sensi, ho iniziato a ricordare gli eventi della scorsa notte."


translate italian day7_dv_961cb5de:


    "Mi sembrava che ci fosse una bottiglia di vodka, che ho svuotato felicemente insieme con Alisa…"


translate italian day7_dv_955e4b23:


    "Ho cercato di alzarmi, ma qualcosa stava schiacciando il mio braccio sinistro."


translate italian day7_dv_169141ea:


    "Era la tranquilla e russante Alisa."


translate italian day7_dv_fed4fffc:


    "Nuda…"


translate italian day7_dv_53e612f9:


    "Tutto ciò che era accaduto mi è tornato in mente."


translate italian day7_dv_2d377264:


    "La paura è stata rapidamente sostituita da una sensazione di euforia."


translate italian day7_dv_d8bc19f3:


    "Mi sono rilassato pigramente sul letto, godendomi una bella mattinata da postumi della sbornia."


translate italian day7_dv_94b6a4c7:


    "Dopo un po' Alisa si è svegliata."


translate italian day7_dv_75b94b19:


    "L'ho baciata dolcemente e le ho detto:"


translate italian day7_dv_0a6d11b5:


    me "Buongiorno!"


translate italian day7_dv_117eed62:


    "Mi ha fissato con lo sguardo perso nel vuoto per un paio di secondi e poi è balzata in piedi gridando:"


translate italian day7_dv_b7d8bf72:


    dv "Tu! Tu!"


translate italian day7_dv_08572abe:


    me "Io cosa?"


translate italian day7_dv_4f57be91:


    "Rendendosi conto di essere nuda, Alisa mi ha strappato la coperta dalle mani e si è avvolta in essa."


translate italian day7_dv_0c9248c1:


    dv "Ieri tu…"


translate italian day7_dv_8a5850eb:


    "Ha sibilato."


translate italian day7_dv_0e8d2401:


    me "Ma è stata una tua idea…"


translate italian day7_dv_8b988724:


    dv "Ero ubriaca!"


translate italian day7_dv_bb4b7abd:


    me "Lo ero anch'io, che ci posso fare…"


translate italian day7_dv_725e3ccf:


    "Alisa mi ha guardato furiosamente, ma poi sì è calmata e si è seduta accanto a me."


translate italian day7_dv_40473235:


    dv "Va bene, quello che è successo è successo…"


translate italian day7_dv_238ce8d6:


    me "Sei sicura?"


translate italian day7_dv_0c27c20a:


    "Mi sono alzato e ho cercato di abbracciarla."


translate italian day7_dv_48a6403e:


    dv "No…"


translate italian day7_dv_bb97ed04:


    "Ha sussurrato, arrossendo."


translate italian day7_dv_5ac38cf1:


    me "Perché?"


translate italian day7_dv_9e177d54:


    dv "Perché no…"


translate italian day7_dv_f374d34a:


    "Ha scosso freneticamente la testa."


translate italian day7_dv_339d121b:


    dv "Perché sono già le quattro!"


translate italian day7_dv_a6f35be5:


    me "E allora? Non mi dire che sei in ritardo per qualcosa?"


translate italian day7_dv_3daf3890:


    dv "Partiranno senza di noi!"


translate italian day7_dv_25f7abbc:


    me "Partiranno dove?"


translate italian day7_dv_350637f6:


    dv "Oggi è l'ultimo giorno!"


translate italian day7_dv_09586d84:


    me "L'ultimo giorno di cosa?"


translate italian day7_dv_141c5aec:


    "Ho chiesto, del tutto confuso."


translate italian day7_dv_9d1223cd:


    dv "L'ultimo giorno della sessione!"


translate italian day7_dv_3d342d5a:


    me "Che cosa?!"


translate italian day7_dv_4b58184b:


    "I postumi della sbornia che sembravano essere già passati da qualche tempo sono ritornati all'improvviso."


translate italian day7_dv_24c7fd3d:


    dv "Non mi dire che non lo sapevi?"


translate italian day7_dv_975a5f59:


    "Ha chiesto Alisa, sorpresa."


translate italian day7_dv_1b078565:


    me "E così, tu lo sapevi?"


translate italian day7_dv_0f45ad2e:


    dv "Beh, sì…{w} Ma per qualche ragione l'avevo dimenticato."


translate italian day7_dv_e1f38c76:


    me "E cosa dovremmo fare ora?"


translate italian day7_dv_3490638a:


    "In realtà non era quello che mi preoccupava maggiormente in quel momento."


translate italian day7_dv_e3a67172:


    th "Se oggi è l'ultimo giorno allora ognuno andrà da qualche parte.{w} Così potrò finalmente uscire da questo Campo in qualche modo!"


translate italian day7_dv_3b3335bc:


    dv "Beh, non ne ho idea."


translate italian day7_dv_516b334e:


    me "Forse abbiamo ancora tempo!"


translate italian day7_dv_713e5769:


    "Sono balzato giù dal letto e ho iniziato a vestirmi in fretta."


translate italian day7_dv_1252f291:


    "È stato difficile, dato che il mio equilibrio era gravemente compromesso dalla sontuosa indulgenza di ieri sera."


translate italian day7_dv_9acc6e59:


    me "Che stai aspettando?"


translate italian day7_dv_b1376477:


    "Alisa non ha risposto, ma ha iniziato a vestirsi pure lei."


translate italian day7_dv_d7fddf78:


    "Un paio di minuti più tardi eravamo in piedi nella piazza deserta."


translate italian day7_dv_4c7430c4:


    me "Andiamo alla casetta della leader del Campo!"


translate italian day7_dv_3a7fd3f3:


    "Ho aperto la porta della stanza di Olga Dmitrievna con la mia chiave, ma non c'era nessuno all'interno."


translate italian day7_dv_a20cefa7_1:


    "..."


translate italian day7_dv_fc48590b:


    "Abbiamo setacciato l'intero Campo e abbiamo scoperto che eravamo rimasti soli."


translate italian day7_dv_a20cefa7_2:


    "..."


translate italian day7_dv_732ccffa:


    "Dopo essere tornati alla piazza mi sono seduto e mi sono coperto il viso con le mani, come un uomo condannato."


translate italian day7_dv_16a845b1:


    "Mi sentivo ancora male, ma ora sembrava che dovessi risolvere un problema serio."


translate italian day7_dv_4f9d4e08:


    me "Dove potrebbero essere andati?"


translate italian day7_dv_6a9c9a91:


    dv "Beh, al centro del distretto, immagino."


translate italian day7_dv_e462bbf9:


    me "Sai dov'è?"


translate italian day7_dv_eb9b0712:


    dv "Più o meno."


translate italian day7_dv_b5ff223e:


    me "È possibile raggiungerlo a piedi?"


translate italian day7_dv_3e69c35f:


    dv "Non so."


translate italian day7_dv_f85f3acb:


    me "E quanto tempo ci vorrebbe su un autobus?"


translate italian day7_dv_6ac4fbb2:


    dv "Un paio d'ore."


translate italian day7_dv_6373dd16:


    "Ho calcolato rapidamente quanti chilometri potessero essere."


translate italian day7_dv_5039789f:


    "Considerando la qualità dell'industria automobilistica e delle strade sovietiche si poteva presumere che fosse del tutto possibile arrivarci a piedi."


translate italian day7_dv_78543246:


    "Sempre che Alisa conoscesse la direzione giusta."


translate italian day7_dv_98084336:


    "Inoltre, nella mia situazione non era facile prendere quel genere di decisioni."


translate italian day7_dv_cfd1657b:


    "Ma non potevamo neppure restare lì.{w} Soli, senza cibo, in un Campo di pionieri deserto..."


translate italian day7_dv_ce8c5196:


    "Nella vita reale sarebbe decisamente una cattiva idea, e io non avevo altra scelta se non considerare tutto questo come se fosse realtà."


translate italian day7_dv_304c9144:


    me "Andiamo."


translate italian day7_dv_621eb081:


    dv "Dove?"


translate italian day7_dv_17368c07:


    me "Al centro del distretto!"


translate italian day7_dv_0ff80729:


    dv "Sei impazzito?!"


translate italian day7_dv_948f6b23:


    me "Preferisci rimanere qui?"


translate italian day7_dv_a094d465:


    dv "Ma certo! Si accorgeranno di averci lasciati indietro, e torneranno a prenderci."


translate italian day7_dv_296ed7ce:


    me "Non pensi che sia strano che non l'avessero notato durante la partenza?"


translate italian day7_dv_d8bfa1da:


    dv "Beh, sì, ma…"


translate italian day7_dv_60f3d758:


    "Ha iniziato a pensare."


translate italian day7_dv_efc9fc55:


    dv "Ma andare da qualche parte da soli…"


translate italian day7_dv_e708673b:


    me "Ma hai detto che conoscevi la strada!"


translate italian day7_dv_0db84079:


    dv "La conosco…"


translate italian day7_dv_33c04461:


    "Ha dichiarato, insicura."


translate italian day7_dv_d232a207:


    me "Allora perché esitare?!{w} Forse incontreremo qualche autobus che passerà di lì."


translate italian day7_dv_8f93dbc3:


    th "Non ne sono molto convinto io stesso, ma se c'è una via d'uscita da questo Campo, quella sembra essere un'idea abbastanza buona."


translate italian day7_dv_69c97e52:


    dv "Non so… Come vuoi…"


translate italian day7_dv_1488dd24:


    "Ha balbettato confusa."


translate italian day7_dv_79601b2c:


    me "Allora fa' i bagagli. Ci incontreremo qui tra mezz'ora."


translate italian day7_dv_3a7b0668:


    "Sono corso verso la casetta di Olga Dmitrievna."


translate italian day7_dv_47b0c421:


    "Sì, non avevo molto da portar via."


translate italian day7_dv_9e214029:


    "Ho gettato in una borsa i miei vestiti invernali e stavo per andarmene, quando un'idea interessante mi è balzata nella mente."


translate italian day7_dv_1d1dfe83:


    th "A cosa mi servono dopotutto?"


translate italian day7_dv_7f3b0a96:


    th "L'estate non sembra essere ancora finita, e quindi che senso ha trascinarsi dietro un peso superfluo?{w} Soprattutto dato il fatto che non so per quanto a lungo dovremo camminare…"


translate italian day7_dv_294dbace:


    "Dopo essermi convinto del fatto che si potesse sopravvivere a questo clima senza bisogno di un cappotto e degli stivali invernali, ho spinto nella mia tasca il cellulare con la sua batteria morente, e mi sono diretto verso la piazza."


translate italian day7_dv_68674ae5:


    "Ho dovuto aspettare Alisa per almeno una ventina di minuti."


translate italian day7_dv_d5afc1dc:


    "La sbornia sembrava essersi alleviata un po', e ciò mi ha consentito di mettere a fuoco la notte precedente."


translate italian day7_dv_ee82b163:


    th "Chi è veramente lei per me?"


translate italian day7_dv_391b61de:


    th "È stata solo una notte di sesso da ubriachi, o qualcosa di più?"


translate italian day7_dv_dac5d5ff:


    th "Naturalmente non posso parlare per Alisa.{w} È difficile anche parlare per me stesso."


translate italian day7_dv_515b15e3:


    "Ho cominciato a ricordare come ci eravamo conosciuti, di come lei mi avesse deriso e preso in giro all'inizio, della sua arroganza e sicurezza di sé…"


translate italian day7_dv_4ec52090:


    "Tutto è stato più che compensato dalla tenerezza della notte scorsa."


translate italian day7_dv_93eb0c2d:


    th "Ormai è chiaro che lei non sia la persona che vuole sembrare.{w} Ma che tipo di persona è la vera Alisa?"


translate italian day7_dv_7d2a5519:


    th "E io provo dei sentimenti nei suoi confronti?{w} Mi sento legato a lei, provo una sorta di affetto, ma forse c'è qualcosa di più?"


translate italian day7_dv_b0325c5c:


    "Le mie riflessioni sono state interrotte da Alisa."


translate italian day7_dv_a303f6cc:


    dv "Ecco qui, prendi."


translate italian day7_dv_77743949:


    "Mi ha porto uno zaino enorme."


translate italian day7_dv_4907682a:


    me "Cos'è questo?"


translate italian day7_dv_ff782f2a:


    "Ho chiesto con scetticismo."


translate italian day7_dv_e780e2ad:


    dv "La mia roba."


translate italian day7_dv_aae0b9eb:


    me "Perché così poca?"


translate italian day7_dv_4eaa9948:


    dv "Ho portato tutto quello che ci stava dentro."


translate italian day7_dv_5491b40f:


    me "Senti…{w} Non sappiamo per quanto dovremo camminare.{w} Non potresti prendere solo lo stretto necessario?"


translate italian day7_dv_44f79413:


    dv "E allora che dovrei fare, buttarle vie? Lasciarle qui?"


translate italian day7_dv_15d90504:


    "Ha sbuffato."


translate italian day7_dv_206f97a8:


    "Per prima cosa, non riuscivo proprio a capire il motivo per cui lei dovesse avere talmente tanta roba, e in secondo luogo, cosa c'era di così prezioso in tutte quelle cose?"


translate italian day7_dv_8d0625c6:


    me "Mai il dovere cavalleresco di portare tutte queste cose spetta a me, giusto?"


translate italian day7_dv_bdbe816e:


    dv "Ovvio!"


translate italian day7_dv_3aa0974f:


    "Ha sorriso sornione."


translate italian day7_dv_8d91232a:


    "Ho cercato di stimare quanto potesse pesare."


translate italian day7_dv_278bae91:


    th "Una decina di chili, immagino.{w} Posso trasportarlo per un paio di chilometri, ma non molto di più…"


translate italian day7_dv_9887bbde:


    me "Capisci che non arriveremo molto lontano con tutto questo peso."


translate italian day7_dv_d3b04189:


    dv "Beh, faremo del nostro meglio…"


translate italian day7_dv_a7bd2a57:


    me "Vuoi dire che potrò buttar via qualcosa più tardi?"


translate italian day7_dv_7ec4ae09:


    dv "Vedremo."


translate italian day7_dv_521b5dfc:


    "Ha risposto, con sguardo scintillante."


translate italian day7_dv_9886068f:


    me "Va bene, ti prendo in parola."


translate italian day7_dv_91ff7f5a:


    th "Dopotutto, dato che devo trasportarlo io, ho tutto il diritto di gettare le cose fuori da esso."


translate italian day7_dv_a07a03c0:


    "E così il nostro viaggio è iniziato."


translate italian day7_dv_92f1963f:


    "È servita circa un'ora per farmi stancare."


translate italian day7_dv_31827d05:


    "Non avrei mai pensato di avere una tale resistenza."


translate italian day7_dv_948d6304:


    "O lo zaino era facile da trasportare, o gli esercizi di ieri notte avevano allenato i miei muscoli."


translate italian day7_dv_3e917655:


    "Mi sono fermato sul ciglio della strada e ho buttato a terra le cose di Alisa."


translate italian day7_dv_e4dfaea7:


    dv "Ma che fai?"


translate italian day7_dv_6932f262:


    "Era visibilmente risentita."


translate italian day7_dv_6557adca:


    me "Vuoi portarlo tu?"


translate italian day7_dv_b2bffcd8:


    dv "Certo che no!"


translate italian day7_dv_ddbf8003:


    me "In tal caso, ho bisogno di riposare."


translate italian day7_dv_5ced9ae4:


    "Ovviamente, la decisione più razionale sarebbe stata quella di eliminare la maggior parte delle cose dallo zaino, tenendo solo lo stretto necessario, ma non riuscivo a prendere certe misure drastiche."


translate italian day7_dv_a87b4b28:


    me "Non hai detto una singola parola per l'intero tragitto…"


translate italian day7_dv_47e8d127:


    "Alisa stava fissando la strada che si estendeva in lontananza, con sguardo assente."


translate italian day7_dv_2895df22:


    dv "E di cosa dovrei parlare?"


translate italian day7_dv_e807ab10:


    me "Non lo so…{w} Ma ti stai comportando come se non fosse successo niente."


translate italian day7_dv_b9934743:


    dv "Perché, è successo qualcosa?"


translate italian day7_dv_46ec1ab9:


    "Ha chiesto in modo distratto."


translate italian day7_dv_256912af:


    me "Beh… Sai…"


translate italian day7_dv_6330e388:


    dv "Ti sei riposato abbastanza? Allora andiamo!"


translate italian day7_dv_45b6c18e:


    "Alisa ha cominciato a camminare in fretta, allontanandosi da me."


translate italian day7_dv_1d8b65b9:


    me "Ehi, aspetta un attimo!"


translate italian day7_dv_02244c64:


    "Mi sono messo in spalla il suo zaino e sono corso dietro di lei."


translate italian day7_dv_a20cefa7_3:


    "..."


translate italian day7_dv_8af27249:


    "Abbiamo camminato in silenzio per un po', Alisa era leggermente più avanti – io dietro."


translate italian day7_dv_d9f48ac6:


    "Non avevo idea di come iniziare una conversazione."


translate italian day7_dv_f9d9d427:


    "Sembrava che mi volesse far intendere che la notte appena passata non fosse stata altro che un grosso errore."


translate italian day7_dv_c81f3a7e:


    "O almeno, così mi è sembrato…"


translate italian day7_dv_e4773f56:


    "Anche se dal mio punto di vista ciò che è accaduto doveva succedere."


translate italian day7_dv_bd664ace:


    "Durante gli ultimi giorni nel Campo mi sentivo decisamente attratto da Alisa.{w} E credo che anche lei fosse attratta da me."


translate italian day7_dv_9f1719bd:


    th "In tal caso, qual è il problema...?"


translate italian day7_dv_5f62b20e:


    th "Forse non vuole che sia stata solo un'avventura di una notte?"


translate italian day7_dv_9bbf9c21:


    me "Sai…"


translate italian day7_dv_00dae805:


    "Si è voltata e mi ha guardato."


translate italian day7_dv_6afc1bb3:


    me "Se ho fatto qualcosa di sbagliato…"


translate italian day7_dv_6812f447:


    dv "Non hai fatto niente di sbagliato."


translate italian day7_dv_a31a4d9f:


    me "Ma…"


translate italian day7_dv_904f584f:


    dv "È semplicemente successo."


translate italian day7_dv_953edcaa:


    me "E così è stato solo un incidente?"


translate italian day7_dv_3ae4e7bf:


    dv "Non ho detto questo."


translate italian day7_dv_b233eb86:


    me "Allora cos'hai detto?"


translate italian day7_dv_20a5d791:


    "La mia incapacità di ottenere una risposta diretta da lei mi ha fatto perdere le staffe, ed ho alzato la voce."


translate italian day7_dv_60992bdb:


    me "Sai solo darmi accenni, ambiguità! Dov'è finita la tua semplicità, la tua sincerità? Non eri così prima!"


translate italian day7_dv_86f37595:


    dv "Le persone tendono a cambiare…"


translate italian day7_dv_47359365:


    "Si è incamminata lentamente."


translate italian day7_dv_ad786d68:


    me "Cambiare? E tu lo chiami un «cambiamento»?{w} Sei una persona completamente diversa ora! Sembri proprio Lena, davvero."


translate italian day7_dv_1647718e:


    "Alisa si è fermata, ma senza voltarsi."


translate italian day7_dv_e9b62978:


    dv "Non paragonarmi…{w} Capito?!{w} Non paragonarmi a lei!"


translate italian day7_dv_03ee962e:


    "L'ha detto in tono calmo, ma c'era una nota di rabbia nella sua voce."


translate italian day7_dv_d3383672:


    me "Cosa...?"


translate italian day7_dv_b6ed7be6:


    "Sono rimasto un po' sorpreso."


translate italian day7_dv_98eb301b:


    dv "Mi dai sempre la colpa di tutto, ma che dire di te? Sei sempre a cercarla! Persino adesso riesci a vederla in me…"


translate italian day7_dv_3837a7d9:


    me "Aspetta, che cosa…"


translate italian day7_dv_c011ba0d:


    dv "Perché sei venuto ieri, allora? Saresti potuto andare da lei! Lo sapevi…"


translate italian day7_dv_3b51cf39:


    me "Sapevo cosa?"


translate italian day7_dv_ffc2234f:


    dv "Lo sapevi che stavo aspettando solo te!"


translate italian day7_dv_cdd661a7:


    "Pare che il confessarmi una cosa del genere fosse stata una decisione difficile per Alisa, ma in un primo momento ho attribuito poca importanza alle sue parole."


translate italian day7_dv_760c9675:


    me "Bene, me l'avevi chiesto tu di venire ieri sera, così ho…"


translate italian day7_dv_0433bfa6:


    dv "Non si tratta di questo!"


translate italian day7_dv_d1db8c32:


    "Alisa ha cominciato a singhiozzare, coprendosi il viso con le mani."


translate italian day7_dv_613a279f:


    "Mi ha colto totalmente alla sprovvista."


translate italian day7_dv_5bf38ff0:


    "Non avevo idea di cosa volesse comunicarmi, e quindi era impossibile per me parlare normalmente."


translate italian day7_dv_aa4f4033:


    me "Scusa…{w} Perdonami…"


translate italian day7_dv_a9505b4f:


    dv "Smettila di scusarti!"


translate italian day7_dv_e506eadf:


    me "Ma se ho detto o fatto qualcosa di sbagliato…"


translate italian day7_dv_1a4b9468:


    dv "Allora che senso ha scusarsi? Se l'hai già fatto…{w} Sarebbe stato meglio comportarsi bene sin dall'inizio!"


translate italian day7_dv_732b1419:


    me "Ma non so come!"


translate italian day7_dv_bdcc6431:


    "Alisa ha alzato lo sguardo verso di me."


translate italian day7_dv_b4bb6756:


    "C'era talmente tanta sofferenza nel suo sguardo che non sono riuscito a sopportarlo, e sono stato costretto a voltarmi."


translate italian day7_dv_8a74fc28:


    "Siamo rimasti così per un po'…"


translate italian day7_dv_5fcaf917:


    "Ben presto sembrava essersi ripresa."


translate italian day7_dv_1b7149e8:


    dv "Ok, lasciamo perdere. Andiamo avanti."


translate italian day7_dv_7f90898b:


    "Naturalmente non avrei potuto lasciar perdere nemmeno se l'avessi voluto, ma non avevo niente da dire in ogni caso, così mi sono limitato a seguirla in silenzio."


translate italian day7_dv_a20cefa7_4:


    "..."


translate italian day7_dv_bf9809fa:


    "Il sole stava tramontando lentamente all'orizzonte."


translate italian day7_dv_546ddc40:


    "Ero immerso nei miei pensieri, dimenticandomi persino dello zaino pesante che avevo sulle spalle."


translate italian day7_dv_7d33e08e:


    "Dovevo dire qualcosa, fare qualcosa."


translate italian day7_dv_29a605f9:


    th "Alisa se lo aspetta da me.{w} Almeno qualche parola…"


translate italian day7_dv_ee5b037a:


    th "Ma come posso iniziare?"


translate italian day7_dv_3522dad7:


    "Nella mia vita ero ormai abituato a trattare solo con cose semplici."


translate italian day7_dv_26ce155f:


    "Ogni cosa sembrava potesse concludersi solo dopo pochi passi."


translate italian day7_dv_bb97fe3b:


    "Invece di considerare il complicato percorso verso il mio obiettivo, mi immaginavo solo il risultato, godendomi una vittoria in realtà mai ottenuta."


translate italian day7_dv_99fc5064:


    "Inoltre, dubito che dovrei affrontare una situazione come questa nel mondo reale."


translate italian day7_dv_6fb63bc0:


    "Ovviamente, avevo sognato a lungo di incontrare una persona a cui potesse importare di me..."


translate italian day7_dv_ff2cf0a2:


    "Ma in quel momento quella persona era di fronte a me, eppure io non provavo nulla."


translate italian day7_dv_79e93d21:


    "E non potevo nemmeno dire ad Alisa «sì» o «no», perché non sapevo cosa sarebbe accaduto in seguito."


translate italian day7_dv_cb230162:


    "Nel frattempo si è fatto buio."


translate italian day7_dv_31cc1e11:


    me "Credo che dovremmo accamparci per la notte."


translate italian day7_dv_683a8ca7:


    dv "Non manca molto."


translate italian day7_dv_3e4e35f3:


    me "Sei sicura?{w} E anche se fosse così, sarebbe meglio viaggiare durante il giorno."


translate italian day7_dv_19dee4e1:


    dv "Sei tu l'esperto…"


translate italian day7_dv_8cef92aa:


    "Ha risposto con freddezza."


translate italian day7_dv_2da2cbd9:


    "Ci siamo fermati al margine di un bosco."


translate italian day7_dv_e627ef17:


    me "Hai qualcosa di utile lì dentro?"


translate italian day7_dv_2e4b2df9:


    "Ho indicato lo zaino."


translate italian day7_dv_c665ba01:


    dv "Da' un'occhiata."


translate italian day7_dv_62531094:


    "C'erano principalmente calzature e vestiti, ma in fondo ho trovato una rivista e una scatola di fiammiferi. Quello che sarebbe bastato per accendere un fuoco."


translate italian day7_dv_55d331e0:


    "Ben presto eravamo seduti su un ceppo, riscaldandoci le mani davanti al fuoco..."


translate italian day7_dv_64dedd42:


    "Solo allora ho veramente capito di essere riuscito finalmente ad andarmene da quel dannato Campo."


translate italian day7_dv_cef2d4ff:


    th "Non ho trovato risposte; al contrario, tutto è diventato ancora più intricato dato che, suppongo, dovrò passare il resto della mia vita in questa realtà."


translate italian day7_dv_584626de:


    th "Trascurando tutte le possibili teorie avveniristiche, è chiaro che io sia da qualche parte nel sud, alla fine degli anni '80."


translate italian day7_dv_c66b43bf:


    th "Come, e cosa ancora più importante, perché io sia stato portato qui – non è più così importante adesso, dato che dovrò adattarmi in qualche modo."


translate italian day7_dv_0cc0cf06:


    th "E dovrò farlo con la ragazza che ho accanto in questo momento."


translate italian day7_dv_48a428eb:


    me "Allora, cosa faremo quando raggiungeremo il centro del distretto?"


translate italian day7_dv_9eca505b:


    dv "Chiameremo i nostri genitori, che altro!"


translate italian day7_dv_94184036:


    "Alisa ha fatto un sorriso.{w} Lei aveva qualcuno da chiamare."


translate italian day7_dv_f4eb646d:


    me "E dopo?"


translate italian day7_dv_e639a729:


    dv "E dopo sarà tutto finito."


translate italian day7_dv_e54b39e9:


    me "Cioè?"


translate italian day7_dv_35919e4b:


    dv "Beh, torneremo alle nostre case."


translate italian day7_dv_22dddcd1:


    "Tale risposta non mi ha sorpreso affatto, è solo che non avevo pensato a cosa sarebbe successo dopo la fine della sessione."


translate italian day7_dv_5c1ed51e:


    me "Tutto qui?"


translate italian day7_dv_964c391d:


    dv "Che altro ci dovrebbe essere?"


translate italian day7_dv_ee102f21:


    me "Non lo so…{w} Dopo tutto quello che è successo…"


translate italian day7_dv_be621763:


    dv "Ma non è successo proprio niente!"


translate italian day7_dv_bdfc5195:


    "Mi ha detto allegramente."


translate italian day7_dv_7a16e55f:


    me "Come sarebbe «non è successo niente»?"


translate italian day7_dv_5788094d:


    dv "Avrai l'occasione di incontrare Lena."


translate italian day7_dv_7540e341:


    "Ero sorpreso, ma Alisa l'ha detto in tono assolutamente tranquillo."


translate italian day7_dv_1deed426:


    me "Come se avessi bisogno di Lena!"


translate italian day7_dv_bad_3bbe4c98:


    dv "Non è così?"


translate italian day7_dv_bad_af26aec3:


    "Ad essere sincero, Lena era l'ultima persona a cui stavo pensando in quel momento."


translate italian day7_dv_bad_3d1847ad:


    me "No."


translate italian day7_dv_bad_7f24994f:


    dv "Allora, forse, sono io?"


translate italian day7_dv_bad_eb6f47e6:


    "Ho guardato Alisa attentamente.{w} Stava sorridendo in modo sprezzante."


translate italian day7_dv_bad_3674b5bc:


    th "E poco fa invece stava piangendo…"


translate italian day7_dv_bad_86e80d63:


    me "In che senso «sei tu»?"


translate italian day7_dv_bad_ff946cd9:


    "Ho eluso la domanda."


translate italian day7_dv_bad_50f50099:


    dv "Cosa pensi di me?"


translate italian day7_dv_bad_ae44c7f6:


    me "Non lo so…"


translate italian day7_dv_bad_0c350a0a:


    dv "Forse potresti semplicemente dirmelo in faccia?"


translate italian day7_dv_bad_90c7a728:


    "È sceso un lungo silenzio."


translate italian day7_dv_bad_ec6e249c:


    th "Alisa è in attesa di una risposta diretta, ma non so proprio cosa dirle."


translate italian day7_dv_bad_785620ea:


    th "Prima quel campo di pionieri, e ora questa ragazza, che vuole sentire da me qualcosa che non posso dirle così facilmente."


translate italian day7_dv_bad_5dc514e5:


    me "Vedi…"


translate italian day7_dv_bad_a3d0602e:


    "E poi finalmente ho capito."


translate italian day7_dv_bad_cff69fc4:


    "Ho capito che ormai era inutile cercare di fare il furbo tenendo nascoste le cose."


translate italian day7_dv_bad_881eda46:


    "Ero a un passo dall'uscire da quella prigione.{w} Il peggio era passato!"


translate italian day7_dv_bad_ef1f22d6:


    "Oppure, al contrario, il peggio doveva ancora venire, ma era giunto il momento di parlare apertamente."


translate italian day7_dv_bad_fd117b88:


    me "Vedi…{w} Quello che ti aspetti da me è quasi impossibile."


translate italian day7_dv_bad_a80e7dec:


    dv "Pare che ogni cosa sia impossibile per te."


translate italian day7_dv_bad_393056bc:


    "Ha tirato su col naso."


translate italian day7_dv_bad_bc3ffa54:


    me "È strano dirlo, ma è vero.{w} Non so cosa ne sarà di me... non necessariamente domani, ma anche tra un secondo!"


translate italian day7_dv_bad_1183f209:


    dv "E che cosa potrebbe succedere? Svanirai nel nulla?"


translate italian day7_dv_bad_b9c3fa63:


    me "È possibile."


translate italian day7_dv_bad_d5f6c4f2:


    "È scoppiata a ridere a crepapelle."


translate italian day7_dv_bad_ac122a30:


    dv "Sai, ero pronta a sentire svariate scuse da parte tua, ma non una così."


translate italian day7_dv_bad_0fdb45a6:


    me "Non sono scuse, ascolta…"


translate italian day7_dv_bad_1bc70a2f:


    "Alisa mi ha guardato con attenzione e, a quanto pare, non aveva alcuna intenzione di interrompermi."


translate italian day7_dv_bad_8de689be:


    me "Vedi, io non appartengo a questo luogo.{w} Non sto parlando del Campo, ma di tutto questo mondo.{w} Tutti voi qui siete dei perfetti sconosciuti, delle persone a me incomprensibili."


translate italian day7_dv_bad_2ee7cffc:


    dv "Come lo sei tu per noi, in effetti."


translate italian day7_dv_bad_2a05cd1b:


    me "Non sono nemmeno sicuro che voi siate del mio stesso pianeta o della mia realtà."


translate italian day7_dv_bad_cb2bf9d2:


    dv "Piantala…"


translate italian day7_dv_bad_117896c8:


    "Ha detto stancamente."


translate italian day7_dv_bad_340880cb:


    me "Immagina di addormentarti e di risvegliarti in un luogo completamente diverso.{w} E non solo un luogo diverso, ma anche un'era diversa."


translate italian day7_dv_bad_c0004c2a:


    me "Questo è quanto è successo a me!{w} Prima vivevo agli inizi del ventunesimo secolo, in una città lontano da qui."


translate italian day7_dv_bad_79a7b35b:


    me "Ero più vecchio di come sono ora.{w} Avevo la mia vita, indipendentemente dal fatto che fosse bella o brutta, ma era mia."


translate italian day7_dv_bad_e281434b:


    me "E poi una notte sono salito su un autobus, mi sono addormentato e mi sono svegliato qui."


translate italian day7_dv_bad_0743d894:


    me "E sono diventato un pioniere diciassettenne."


translate italian day7_dv_bad_560a2626:


    "Alisa mi ha guardato sorpresa."


translate italian day7_dv_bad_3439946a:


    dv "E ti aspetti che io ti creda?"


translate italian day7_dv_bad_be346c4b:


    me "Non lo so, sta a te decidere…{w} Non è stato affatto facile per me dirti queste cose."


translate italian day7_dv_bad_82b2a360:


    dv "Sei proprio un maestro narratore, davvero."


translate italian day7_dv_bad_41aa7bd4:


    me "Aspetta…"


translate italian day7_dv_bad_de80ec27:


    "Ho frugato nelle tasche e le ho mostrato il telefono."


translate italian day7_dv_bad_f584d6ef:


    "Con mia grande sorpresa, c'era ancora un po' di batteria, e lo schermo brillava vivacemente."


translate italian day7_dv_bad_15c145d7:


    me "Hai mai visto qualcosa del genere? Avete qui qualcosa di simile?"


translate italian day7_dv_bad_45f81bbc:


    "L'ho mostrato ad Alisa."


translate italian day7_dv_bad_56878f3a:


    "Ci ha giocherellato un po', premendo i tasti."


translate italian day7_dv_bad_aa4e170d:


    dv "Beh… Una specie di giocattolo d'importazione. E allora?"


translate italian day7_dv_bad_135e53c8:


    me "Non è un giocattolo, è un telefono."


translate italian day7_dv_bad_99ad611b:


    dv "E dove sono i fili, dov'è la cornetta?"


translate italian day7_dv_bad_0467a4c4:


    "Ha riso."


translate italian day7_dv_bad_2f7003bc:


    me "Non servono.{w} Purtroppo, non posso mostrarti come funziona perché non è ancora stato inventato nel vostro tempo."


translate italian day7_dv_bad_1e3907df:


    dv "Sì, certo…"


translate italian day7_dv_bad_d45cda8e:


    "Mi ha detto con scetticismo, riconsegnandomi il telefono."


translate italian day7_dv_bad_1b521e0d:


    me "In ogni caso, sta a te decidere se credermi o no.{w} Ti ho detto tutto questo solo per spiegarti in che situazione mi trovo adesso."


translate italian day7_dv_bad_48d8149b:


    me "Non so cosa mi potrebbe succedere nella prossima ora, quindi certe decisioni…"


translate italian day7_dv_bad_4741fcda:


    dv "Bene, non c'è bisogno che tu dica altro.{w} E non c'è bisogno di prendere decisioni."


translate italian day7_dv_bad_3a7fe15b:


    "Si è alzata e ha gettato un po' di legna sul fuoco."


translate italian day7_dv_bad_b6877455:


    me "Se ti ho offesa…"


translate italian day7_dv_bad_b4919381:


    dv "Nah, va tutto bene."


translate italian day7_dv_bad_3e1254a0:


    "Ha detto con freddezza."


translate italian day7_dv_bad_39cad26b:


    dv "Capisco.{w} Tu sei un visitatore venuto dal futuro, e quindi le ragazze normali come me non ti interessano."


translate italian day7_dv_bad_71b71623:


    me "Mi hai frainteso..."


translate italian day7_dv_bad_cac50a1b:


    dv "No, non l'ho fatto.{w} È una storia molto credibile, logica, e soprattutto veritiera."


translate italian day7_dv_bad_d0bafdfc:


    dv "Qualunque ragazza ci crederebbe."


translate italian day7_dv_bad_6facbf02:


    me "Quindi, pensi che io mi stia ancora inventando delle scuse?"


translate italian day7_dv_bad_9f0b84db:


    "Alisa è rimasta in silenzio."


translate italian day7_dv_bad_2284906f:


    me "O che sono completamente impazzito?"


translate italian day7_dv_bad_d558b838:


    dv "Ci si potrebbe aspettare tutto da te."


translate italian day7_dv_bad_9514ab02:


    me "Perché devi essere così?"


translate italian day7_dv_bad_258b2342:


    "Le ho chiesto, frustrato."


translate italian day7_dv_bad_30e02149:


    dv "Essere come? Non ho fatto niente di male."


translate italian day7_dv_bad_5345aad1:


    "Ha sorriso in modo sprezzante."


translate italian day7_dv_bad_0039dd59:


    me "Prova a immaginare che sia tutto vero e capire il motivo per cui mi comporto così…"


translate italian day7_dv_bad_a51804eb:


    dv "Ho già immaginato, ho già capito…"


translate italian day7_dv_bad_2534d25c:


    "Stavo per dirle qualcos'altro, ma un clacson è risuonato dalla strada."


translate italian day7_dv_bad_99a62d84:


    "C'era un autobus sul ciglio della strada.{w} Un LiAZ su una tratta regolare."


translate italian day7_dv_bad_a3ea7899:


    "Strano che non ci siamo accorti che fosse arrivato…"


translate italian day7_dv_bad_0725f998:


    th "Finalmente! La salvezza!"


translate italian day7_dv_bad_d5a384e6:


    dv "Andiamo?"


translate italian day7_dv_bad_53da10dd:


    "Mi ha chiesto Alisa in tono scialbo, del tutto privo di emozioni."


translate italian day7_dv_bad_4bfb0b4b:


    me "Sì, certo!"


translate italian day7_dv_bad_963da71b:


    "Ho afferrato il suo zaino."


translate italian day7_dv_bad_eb09504e:


    dv "Grazie, faccio da sola."


translate italian day7_dv_bad_376173e1:


    "Me l'ha tolto dalle mani."


translate italian day7_dv_bad_3fd88211:


    "Era ovvio che fosse troppo pesante per lei, ma non ho obiettato."


translate italian day7_dv_bad_b5b9d022:


    "Poco dopo stavamo seduti tranquillamente sui sedili posteriori del bus."


translate italian day7_dv_bad_994c7297:


    "Vari sentimenti stavano combattendo dentro di me. Da un lato ero felice oltre ogni limite di tornare al mio mondo; ma dall'altro, ero martoriato dai sensi di colpa per non essere riuscito a chiarire le cose con Alisa."


translate italian day7_dv_bad_26ebea5b:


    me "Mi dispiace davvero, di essermi comportato così…{w} Sicuramente ti aspettavi qualcosa di diverso…"


translate italian day7_dv_bad_af909b39:


    dv "Non mi aspettavo nulla da te."


translate italian day7_dv_bad_8cef92aa:


    "Ha risposto con freddezza."


translate italian day7_dv_bad_89272480:


    me "Volevo solo farti capire…{w} E, per quanto riguarda il nostro rapporto, tu mi piaci! Dico davvero!"


translate italian day7_dv_bad_64464ef0:


    dv "Felice di sentirtelo dire…"


translate italian day7_dv_bad_bb653366:


    "Ma la sua voce non sembrava così felice."


translate italian day7_dv_bad_65ecf8a3:


    me "Lo dico con sincerità.{w} Vedi, non so cosa succederà in seguito, ma qui e adesso…"


translate italian day7_dv_bad_0507e954:


    dv "Suppongo che ieri notte io ti sia piaciuta ancora di più."


translate italian day7_dv_bad_3b5928b5:


    me "No, hai frainteso…"


translate italian day7_dv_bad_68ac3caa:


    dv "Forse dovremmo farlo di nuovo, mentre il conducente non ci vede?"


translate italian day7_dv_bad_cddb4809:


    me "Calmati un attimo, ok? Non ho mai pensato a una cosa del genere!"


translate italian day7_dv_bad_4e1d6150:


    dv "Qual è il problema?{w} Tu lo vuoi e a me non importa, è l'unica cosa in cui sono brava…"


translate italian day7_dv_bad_30b2cc2a:


    me "Alisa, aspetta…"


translate italian day7_dv_bad_e4d66988:


    dv "Ok, adesso vorrei dormire."


translate italian day7_dv_bad_fe96cf03:


    "Si è reclinata sul suo sedile e ha chiuso gli occhi."


translate italian day7_dv_bad_079f62e5:


    me "Aspetta un secondo…"


translate italian day7_dv_bad_b5173cb6:


    dv "Sto dormendo! Parliamo più tardi!"


translate italian day7_dv_bad_af5a03bc:


    "Non ho opposto resistenza."


translate italian day7_dv_bad_699d1594:


    "Dopotutto, il sonno sarebbe stato un buon consigliere. Inoltre, Alisa aveva bisogno di tempo per calmarsi un po'."


translate italian day7_dv_bad_6304447d:


    "Anche se, naturalmente, volevo disperatamente riuscire a spiegarmi in un modo che fosse a lei comprensibile."


translate italian day7_dv_bad_71df3cde:


    "Ma mi sono trattenuto."


translate italian day7_dv_bad_1a5a841d:


    "Eppure, avevo una ferita al cuore…"


translate italian day7_dv_bad_a20cefa7:


    "..."


translate italian day7_dv_bad_4a65de63:


    "Eravamo soli sul bus."


translate italian day7_dv_bad_7d4a72bc:


    "Delle ombre oscure scorrevano fuori dal finestrino. A volte era possibile discernere un albero, un fiume, o i tralicci di una linea elettrica."


translate italian day7_dv_bad_0041c3e7:


    "Ma il paesaggio sembrava non differire da quello del Campo. Forse quel mondo era un cerchio e noi ci stavamo semplicemente muovendo intorno ad esso?"


translate italian day7_dv_bad_401b2186:


    "In ogni caso, il paesaggio era l'ultima delle mie preoccupazioni in quel momento – mi sono limitato a fissare il pavimento e a pensare al domani."


translate italian day7_dv_bad_594edbed:


    th "Cosa accadrà?{w} Questa città, questo pianeta, questa epoca? Tutto è così strano per me…"


translate italian day7_dv_bad_30bdeb77:


    th "E un'ulteriore conversazione con Alisa, che era totalmente delusa di me…"


translate italian day7_dv_bad_a20cefa7_1:


    "..."


translate italian day7_dv_bad_4eeeabc5:


    "L'attesa sembrava molto più faticosa del risultato finale."


translate italian day7_dv_bad_85334293:


    "E alla fine, le cose che sarebbero successe non sembravano più così importanti, né spaventose. Tutta la tua forza si esaurisce. Rimane solo la stanchezza e il vuoto nella tua anima."


translate italian day7_dv_bad_ab33fd37:


    "L'autobus stava guidando verso l'ignoto. Non sono riuscito a resistere, e mi sono addormentato."


translate italian day7_dv_good_b6233cde:


    dv "Non ne sarei così sicura."


translate italian day7_dv_good_7a0d2a29:


    me "Io invece lo sono. Lascia che sia io a decidere per me stesso!"


translate italian day7_dv_good_cf61573a:


    dv "Davvero?{w} E da quando hai iniziato a fare sciocchezze del genere?"


translate italian day7_dv_good_d0b801cc:


    me "Da quando…"


translate italian day7_dv_good_a586fdb0:


    "Improvvisamente mi sono ricordato di quanti anni avessi in questo mondo."


translate italian day7_dv_good_7e088224:


    me "Da diciassette anni!"


translate italian day7_dv_good_301b1799:


    dv "Bene, bene, ammirevole…{w} Ma ad essere sincera, non ho notato niente del genere."


translate italian day7_dv_good_80e71ea5:


    me "E cosa avresti dovuto notare esattamente?"


translate italian day7_dv_good_0ed90974:


    dv "Il fatto che tu sia in grado di dire qualcosa senza alcuna incertezza!"


translate italian day7_dv_good_012bdaba:


    "Aveva completamente ragione su quel punto."


translate italian day7_dv_good_570d60e3:


    me "E cosa vorresti che io dica esattamente?"


translate italian day7_dv_good_fa78d9d9:


    dv "Sai bene cosa!"


translate italian day7_dv_good_24d8e0d2:


    "Quel gioco di allusioni sarebbe potuto durare per l'eternità."


translate italian day7_dv_good_7dc7d107:


    me "Se vuoi davvero sapere cosa provo nei tuoi confronti, allora te lo dico con franchezza – non lo so con certezza."


translate italian day7_dv_good_1abe160f:


    dv "Ecco, è esattamente questo ciò che intendevo!"


translate italian day7_dv_good_e51d106b:


    "Alisa mi ha dato le spalle."


translate italian day7_dv_good_667cf5e8:


    me "Che ci posso fare, è la verità!"


translate italian day7_dv_good_421779c8:


    dv "E allora io cosa posso fare?"


translate italian day7_dv_good_30e22da3:


    me "Non ti sto chiedendo niente!"


translate italian day7_dv_good_4c06e27c:


    dv "Davvero? Ne sei proprio sicuro?"


translate italian day7_dv_good_eee643db:


    me "Certo…"


translate italian day7_dv_good_218d5a60:


    "Ho risposto dopo un po'."


translate italian day7_dv_good_a89de161:


    dv "E cos'hai fatto per tutta la scorsa settimana?"


translate italian day7_dv_good_cbe79a70:


    me "Cos'ho fatto?"


translate italian day7_dv_good_6010fd21:


    dv "Stavi chiedendo, non è così?"


translate italian day7_dv_good_014b09c2:


    me "Stavo chiedendo cosa? Per quale ragione? Non riesci a parlare in modo che la gente possa capirti? I lettori del pensiero sono in vacanza!"


translate italian day7_dv_good_8a8032f7:


    dv "Se solo tu lo volessi, riusciresti a capirlo da solo…"


translate italian day7_dv_good_206c4e41:


    me "Non posso capire senza una spiegazione!"


translate italian day7_dv_good_0ec98f8c:


    dv "Nessun problema, stiamo solo chiacchierando…"


translate italian day7_dv_good_42c9c2a4:


    "Si è alzata e si è diretta verso la strada."


translate italian day7_dv_good_b9225b9a:


    me "Adesso basta!"


translate italian day7_dv_good_b0133881:


    "Ho perso completamente la pazienza, così sono balzato in piedi, l'ho raggiunta in un paio di scatti, l'ho afferrata per il braccio e l'ho spinta verso di me."


translate italian day7_dv_good_05c5777e:


    "Ma avevo calcolato male la quantità di forza da applicare, e così siamo caduti per terra, e Alisa si è trovata sotto di me."


translate italian day7_dv_good_01ae0d80:


    dv "Cosa c'è, ieri non ti è bastato? Ne vorresti ancora un po'?"


translate italian day7_dv_good_18438f67:


    "Il suo viso era raggiante, e i suoi occhi brillavano."


translate italian day7_dv_good_444788e7:


    "Ero davvero confuso. Ho liberato il suo braccio dalla mia morsa, ma non mi sono rialzato subito."


translate italian day7_dv_good_db56aa2f:


    me "Voglio solo capire, dove ho sbagliato?"


translate italian day7_dv_good_dd60046c:


    dv "Perché pensi sempre di aver sbagliato qualcosa?"


translate italian day7_dv_good_7de960a6:


    me "Perché lo pensi tu!"


translate italian day7_dv_good_dcad2da3:


    dv "E chi te l'ha detto? Io no di certo!"


translate italian day7_dv_good_981eb02b:


    me "Ma posso vederlo!"


translate italian day7_dv_good_e3bdc4b7:


    dv "Riesci a vedere ogni cosa, non è vero?{w} Vedi ogni cosa e pensi che sia meglio non interferire, che sia meglio mentire, che sia meglio non dire nulla…"


translate italian day7_dv_good_7351a8db:


    me "Traggo le mie conclusioni in base alla logica!"


translate italian day7_dv_good_77d7b000:


    dv "E sei sicuro che questa tua logica sia davvero corretta?"


translate italian day7_dv_good_5c588803:


    "Non sapevo come rispondere a quella domanda."


translate italian day7_dv_good_40534c30:


    "Per qualche istante siamo rimasti lì così."


translate italian day7_dv_good_b315c215:


    "Ero completamente confuso, e sembrava che Alisa non volesse continuare la nostra conversazione."


translate italian day7_dv_good_121b1f59:


    "Alla fine, mi ha spinto via e si è alzata."


translate italian day7_dv_good_c09a48f6:


    dv "Perché sei qui adesso… con me?"


translate italian day7_dv_good_359b6740:


    me "Perché tutti hanno abbandonato il Campo e noi invece siamo rimasti…"


translate italian day7_dv_good_826048a2:


    dv "Non intendo questo…"


translate italian day7_dv_good_4f930f50:


    me "Beh, perché mi piaci…"


translate italian day7_dv_good_58e259d9:


    "Finalmente sono riuscito a dirglielo in faccia."


translate italian day7_dv_good_3d42189c:


    dv "Sei sicuro?{w} E che mi dici di Lena?{w} Eravate sempre insieme..."


translate italian day7_dv_good_f4da743f:


    dv "Forse perché c'è troppo da faticare con lei.{w} E invece io? Con me è più semplice – basta schioccare le dita e sono subito nel tuo letto!"


translate italian day7_dv_good_176694f4:


    th "Non direi proprio che sia così semplice con lei…"


translate italian day7_dv_good_f37462ea:


    me "Non mettermi le parole in bocca!"


translate italian day7_dv_good_2a75e18b:


    dv "Non dici niente, quindi c'è molto spazio lì dentro!{w} Mentre invece pensi per una dozzina d'altre persone!"


translate italian day7_dv_good_2563ff87:


    "Beh, aveva decisamente ragione a riguardo."


translate italian day7_dv_good_d5c105bf:


    me "Non ho pensato a nulla del genere!"


translate italian day7_dv_good_2e22f90c:


    dv "E quindi ti aspetti che io possa semplicemente credere a tutto questo?"


translate italian day7_dv_good_ccfdaf24:


    me "Non c'è bisogno di crederci. Non ti sto costringendo!{w} Ma è la verità, quindi non sto tramando niente!"


translate italian day7_dv_good_fc9f6ff5:


    dv "Le parole dovrebbero essere dimostrare con le azioni!"


translate italian day7_dv_good_30792c5e:


    th "Ma quali azioni si aspetta da parte mia?"


translate italian day7_dv_good_37530dee:


    me "Beh, potremmo farlo di nuovo…"


translate italian day7_dv_good_dae3bc9a:


    "Ho detto con un sorriso ebete."


translate italian day7_dv_good_d835fa5b:


    "Non ho proprio idea di come io abbia potuto scherzare in una situazione del genere. Soprattutto una battuta {i}del genere{/i}."


translate italian day7_dv_good_d43bb4d6:


    dv "Certo! Dai!"


translate italian day7_dv_good_7a01cb1d:


    "Mi ha detto con tranquillità, si è avvicinata al fuoco e ha cominciato a spogliarsi."


translate italian day7_dv_good_af45c70b:


    "Mi sono alzato di scatto, sono balzato verso di lei e l'ho afferrata per un braccio."


translate italian day7_dv_good_0d1b5397:


    me "Cosa fai?! Aspetta! Non dicevo sul serio!"


translate italian day7_dv_good_ed6b05b6:


    dv "Beh, qual è il problema?{w} Non ti servo solo per quello?"


translate italian day7_dv_good_53efb905:


    me "Certo che no. Non hai capito niente!"


translate italian day7_dv_good_d81dcc94:


    dv "E come dovrei capirti allora?!"


translate italian day7_dv_good_633e9a1b:


    "Si è liberata dalla mia presa e ha iniziato a piangere."


translate italian day7_dv_good_a184da49:


    "Mi sono sdraiato accanto a lei, l'ho abbracciata e ho iniziato ad accarezzarle la testa."


translate italian day7_dv_good_7c4d73aa:


    me "È tutto a posto. Calmati…"


translate italian day7_dv_good_28b62007:


    dv "Lasciami in pace!"


translate italian day7_dv_good_be68715b:


    "Mi ha detto tra le lacrime, ma non ha cercato di liberarsi."


translate italian day7_dv_good_8971dc3e:


    me "Sai, ho avuto una vita difficile prima di arrivare in questo Campo.{w} Non sono abituato a questo genere di cose… Non so come comportarmi in situazioni come questa."


translate italian day7_dv_good_da2cd44a:


    me "Sono stato solo per un lungo periodo di tempo – senza alcun amico, senza nessuno a cui tenevo davvero."


translate italian day7_dv_good_ee9729c2:


    me "Ho semplicemente dimenticato di come provare emozioni e di come amare.{w} È difficile per me."


translate italian day7_dv_good_70bdd2cd:


    me "È molto più facile non immischiarsi in questo genere di cose. Alla fine, è diventata una cosa normale per me.{w} Ed eccoci qui adesso… qualcosa che non avrei mai immaginato."


translate italian day7_dv_good_f83fac68:


    me "Ciò non significa che io sia un egoista bastardo…{w} Anche se immagino che sia così che appaio…"


translate italian day7_dv_good_18509af9:


    me "Semplicemente non riesco ad essere così disinvolto…"


translate italian day7_dv_good_cc722f9f:


    "Alisa non ha risposto, ha continuato a singhiozzare."


translate italian day7_dv_good_49a9ce42:


    me "Ma di una cosa sono certo! Non c'è niente tra me e Lena.{w} È un'amica, niente più. Non l'ho mai considerata in quel modo."


translate italian day7_dv_good_a5aef048:


    me "E io ho bisogno di te! Non solo per quello che pensi che io voglia."


translate italian day7_dv_good_9e669319:


    me "Non so proprio come dirti le cose che vorresti sentire… Comporterebbero degli obblighi…{w} Vedi, non so nemmeno cosa mi accadrà nella prossima ora. E qui…"


translate italian day7_dv_good_48c0bf6b:


    "Alisa sembrava non mi stesse ascoltando per niente, o che le mie scuse semplicemente non l'avessero raggiunta."


translate italian day7_dv_good_cd717fa3:


    th "Beh, dovrei essere fiero di me stesso – ho fatto piangere una ragazza. E quale ragazza? Alisa, che a prima vista sembrava essere totalmente estranea a qualsiasi sentimento romantico!"


translate italian day7_dv_good_06e93473:


    me "Sai, non riesco a prendere questa decisione per entrambi.{w} Posso solo dire che io sono pronto a stare con te, a sostenerti, a proteggerti!"


translate italian day7_dv_good_6ef95196:


    me "Ma tu invece…{w} Perché avresti bisogno di me?"


translate italian day7_dv_good_4a9891cd:


    me "Non c'è niente nella mia anima, non so quello che voglio da questa vita.{w} Inoltre, io sono qui adesso, ma domani potrei non esserci più…"


translate italian day7_dv_good_6196bca7:


    "Alisa ha smesso di piangere per un momento."


translate italian day7_dv_good_64da3e76:


    dv "Non capisci che avevo già preso questa decisione molto tempo fa?"


translate italian day7_dv_good_2e84db32:


    me "Allora…"


translate italian day7_dv_good_5b7bc846:


    "Mi ha guardato con gli occhi pieni di lacrime."


translate italian day7_dv_good_1ca244f1:


    "Per un istante mi è sembrato di vedere uno spiraglio di felicità in essi."


translate italian day7_dv_good_7419452b:


    "Alisa mi ha abbracciato teneramente e mi ha baciato."


translate italian day7_dv_good_0f399005:


    "È stato il miglior bacio di tutta la mia vita."


translate italian day7_dv_good_794a0ba9:


    "No, non era così passionale come quello di ieri sera, ma c'era così tanta tenerezza, fiducia e amore, che mi sono semplicemente disciolto in lei, nelle sue labbra, tra le sue braccia."


translate italian day7_dv_good_fe659328:


    "Mi sono dimenticato di tutto, del Campo, del mio passato, dei dubbi che mi tormentavano un momento prima."


translate italian day7_dv_good_e3bf8cd7:


    "Lei era tutto quello di cui avevo bisogno.{w} Ora.{w} E per sempre!"


translate italian day7_dv_good_63829ee5:


    "Quel bacio avrebbe potuto durare per l'eternità, ma improvvisamente un clacson è risuonato nelle vicinanze, come se provenisse dalla mia mente."


translate italian day7_dv_good_48538083:


    "Mi sono voltato verso la strada e ho visto un autobus.{w} Era un vecchio LiAZ, sembrava uno normale."


translate italian day7_dv_good_377572e1:


    "Siamo rimasti sdraiati e l'abbiamo osservato per un po'."


translate italian day7_dv_good_1a224f5a:


    "Poco dopo, l'autista è sceso e ci ha gridato:"


translate italian day7_dv_good_f2e98a50:


    FIXME_voice "Ehi, piccioncini! Questo è l'ultimo autobus per la città! Venite o no?"


translate italian day7_dv_good_a640ab70:


    dv "Andiamo?"


translate italian day7_dv_good_60c6ccbe:


    "Ha chiesto allegramente Alisa."


translate italian day7_dv_good_4b4b5248:


    "Siamo balzati in piedi. Ho afferrato il suo zaino, ho preso Alisa per mano e siamo corsi verso il bus."


translate italian day7_dv_good_74424fe8:


    "In un paio di secondi ci siamo messi comodi sui sedili posteriori."


translate italian day7_dv_good_b0a142e5:


    "Il conducente ci ha gridato qualcosa dalla sua postazione, ma io non lo stavo ascoltando."


translate italian day7_dv_good_7e7c36fa:


    th "Ecco qui, una nave di salvataggio, che ci sta portando verso una nuova vita!"


translate italian day7_dv_good_37a9c52a:


    "Ho guardato Alisa. Ero così ansioso di dire qualcosa, di continuare la nostra conversazione di prima. Lei dormiva tranquillamente, con la testa appoggiata sulla mia spalla."


translate italian day7_dv_good_099b09a8:


    "Non l'ho svegliata. Non ce n'era bisogno. Avremmo avuto ancora così tanto tempo davanti a noi."


translate italian day7_dv_good_4a65de63:


    "Eravamo gli unici passeggeri."


translate italian day7_dv_good_22a0d0c3:


    "Il bus stava andando veloce, lasciando dietro di sé non solo la mia {i}vita passata{/i}, ma anche la settimana che avevo trascorso nel campo di pionieri chiamato «Sovyonok»."


translate italian day7_dv_good_0a03607e:


    "Mi sono sentito più vivo in quei sette giorni che in tutti quegli anni della mia vita {i}reale{/i}.{w} E quante cose ancora mi aspettavano in futuro?"


translate italian day7_dv_good_200afadf:


    "Il comune paesaggio di campagna che scorreva oltre il finestrino non m'interessava più di tanto. Stavo guardando Alisa dormire tranquillamente."


translate italian day7_dv_good_83fb48fe:


    "Il mio cuore era pieno di calore e di pace, e la mia testa finalmente aveva smesso di essere invasa dai pensieri."


translate italian day7_dv_good_a20cefa7:


    "..."


translate italian day7_dv_good_0ec09f2e:


    "Ma la felicità è un'entità fragile. Molte cose possono distruggerla. Il tempo, per esempio."


translate italian day7_dv_good_6b42fada:


    "Più a lungo dura la felicità, e più diventa ordinaria. Le emozioni diventano noiose. Sorgono nuovi problemi e preoccupazioni."


translate italian day7_dv_good_7ba8dcd0:


    "Mi sentivo bene, ma la stanchezza che avevo accumulato non mi ha permesso di godermi appieno il momento. Mi sono addormentato..."


translate italian day7_sl_d19d9343:


    "Mi sono svegliato a causa del freddo, con i primi raggi del sole a farmi compagnia."


translate italian day7_sl_05d73391:


    "Ovviamente il sacco a pelo era caldo, e non era passato abbastanza tempo affinché esso potesse essere ricoperto di brina, ma l'aver trascorso la notte nella foresta era una novità per me."


translate italian day7_sl_8df959d7:


    "Facendo attenzione, cercando di non svegliare Slavya, mi sono sfilato dal sacco a pelo."


translate italian day7_sl_01b6883d:


    "L'aria fresca del mattino finalmente è riuscita a dissipare la mia sonnolenza, e sono stato travolto dalla serenità della notte appena passata."


translate italian day7_sl_fa705823:


    "Credo che fosse il momento più felice della mia vita."


translate italian day7_sl_78603168:


    "Intimità, tenerezza, amore, passione, tutte quelle emozioni si erano intrecciate nel mio cuore durante quelle brevi ore."


translate italian day7_sl_6b037955:


    "Ho guardato Slavya: era bellissima.{w} Sul suo viso era stampata un'espressione di assoluta pace e tranquillità."


translate italian day7_sl_2b82da62:


    th "Suppongo che sia così che dormono gli angeli…"


translate italian day7_sl_f50d77d4:


    "Dato che non avevo alcuna intenzione di svegliarla, mi sono limitato a sedermi al suo fianco, osservando l'alba di un nuovo giorno."


translate italian day7_sl_3e13649b:


    "La natura…{w} La rugiada fresca, il vento leggero, la danza delle foglie degli alberi e il riflesso del sole sulla superficie dell'acqua: tutto questo era così estraneo a me fino a ieri."


translate italian day7_sl_470e2440:


    "Le giungle di cemento di un'enorme città erano le uniche foreste che conoscevo."


translate italian day7_sl_485f4671:


    "E se qualcuno mi avesse detto che avrei potuto starmene seduto così e godermi quel genere di cose, nonostante il freddo notturno e le fastidiose zanzare, senza computer né internet, non ci avrei mai creduto."


translate italian day7_sl_e1ffc0eb:


    sl "Buongiorno!"


translate italian day7_sl_69f37ce2:


    "Ho guardato Slavya."


translate italian day7_sl_987561b6:


    me "Giorno! Hai dormito bene?"


translate italian day7_sl_3b4389a0:


    sl "Come mai prima d'ora."


translate italian day7_sl_46eff4e1:


    me "Anch'io."


translate italian day7_sl_4932445c:


    "Ha allungato la mano verso di me e mi ha baciato."


translate italian day7_sl_b7a6a5b1:


    "Quel bacio mi è sembrato eterno, e mi sono disciolto in esso."


translate italian day7_sl_4925608a:


    sl "Sarebbe proprio ora di una bella colazione."


translate italian day7_sl_fab71e09:


    "Ha finalmente detto Slavya, lasciandomi andare."


translate italian day7_sl_1066065f:


    me "Già, credo di sì."


translate italian day7_sl_78042d2e:


    sl "Per caso sai che ore sono?"


translate italian day7_sl_1e9c08b9:


    me "No, ma penso che sia ancora presto."


translate italian day7_sl_1f18c165:


    sl "Spero che la mensa sia già aperta. Ho così taaanta fame."


translate italian day7_sl_54aee577:


    me "Anch'io."


translate italian day7_sl_e0880e8b:


    "Abbiamo raccolto le nostre cose e ci siamo diretti al Campo, tenendoci per mano."


translate italian day7_sl_b9e72e01:


    "La mensa era effettivamente già aperta e, stranamente, eravamo i primi visitatori."


translate italian day7_sl_79df6cca:


    "C'era da aspettarsi che la colazione non fosse poi così sontuosa: farina d'avena, un paio di uova, pane secco e tè acquoso."


translate italian day7_sl_b27dc208:


    me "Sai, non mi è mai piaciuta la cucina del posto."


translate italian day7_sl_5e13855b:


    sl "Neppure a me, a dire il vero."


translate italian day7_sl_5e6d7806:


    "Ha riso Slavya."


translate italian day7_sl_e83d08e1:


    me "Ma non abbiamo altra scelta…{w} Ed è comunque buono quando sei a stomaco vuoto."


translate italian day7_sl_7b2eabb4:


    sl "È vero!"


translate italian day7_sl_f9be2c84:


    "A differenza mia, lei stava mangiando educatamente."


translate italian day7_sl_37662204:


    "Mentre invece io mi stavo sbrodolando di porridge, e mi stavo rovesciando addosso il tè come al solito."


translate italian day7_sl_bfeafc61:


    sl "Mangi sempre in questo modo?"


translate italian day7_sl_72cb68c0:


    me "C'è qualcosa di sbagliato?"


translate italian day7_sl_3cc85031:


    "Ho chiesto con la bocca piena."


translate italian day7_sl_e79d08b4:


    "Si è messa a ridere."


translate italian day7_sl_df14ec0d:


    me "Oh… No, non sempre. Ma mi succede, quando ho veramente fame.{w} Ovviamente posso essere più civile."


translate italian day7_sl_50f431fa:


    th "Beh, o almeno ci provo."


translate italian day7_sl_1108cd46:


    sl "Nessun problema. Non fa niente."


translate italian day7_sl_fc08829f:


    "Ben presto abbiamo finito la colazione e ci siamo diretti verso l'uscita."


translate italian day7_sl_68728339:


    "Non appena ho notato che eravamo ancora le uniche persone nella mensa, Olga Dmitrievna è apparsa sulla soglia."


translate italian day7_sl_33a6f7b8:


    mt "Oh, siete voi…{w} Beh, non vorrei chiedervi dove avete trascorso la notte…{w} E quindi…"


translate italian day7_sl_08429d75:


    "La leader del Campo era visibilmente imbarazzata."


translate italian day7_sl_e173004d:


    sl "Buongiorno anche a lei, Olga Dmitrievna!"


translate italian day7_sl_e1614169:


    "Ha detto Slavya, con un sorriso luminoso."


translate italian day7_sl_96bc1657:


    mt "Sì, giorno… giorno…{w} Va bene, non dimenticatevi di prendere tutte le vostre cose."


translate italian day7_sl_16698d4c:


    sl "Non ce lo scorderemo!"


translate italian day7_sl_996ab8e2:


    me "Cosa intendeva dire? Di nuovo un'escursione?"


translate italian day7_sl_02f1b760:


    "Ho chiesto a Slavya dopo che siamo usciti dalla mensa."


translate italian day7_sl_e5708664:


    sl "Macché!{w} Oggi è l'ultimo giorno. Te lo sei scordato?"


translate italian day7_sl_09586d84:


    me "L'ultimo giorno di cosa?"


translate italian day7_sl_86874628:


    sl "Della sessione!"


translate italian day7_sl_5a084819:


    "Dire che le sue parole mi avessero ammutolito era un eufemismo."


translate italian day7_sl_8f86373c:


    me "Come? Perché?"


translate italian day7_sl_88194eef:


    sl "Come mai sei così sorpreso? Non lo sapevi? È stato annunciato durante l'allineamento."


translate italian day7_sl_2d769d89:


    "Ma persino a quegli allineamenti a cui ho partecipato non facevo altro che dormire in piedi."


translate italian day7_sl_5cb822ee:


    me "No…"


translate italian day7_sl_54f37a06:


    sl "Ora lo sai!"


translate italian day7_sl_cfb1c9d5:


    "Slavya ha fatto un sorriso genuino."


translate italian day7_sl_536cdcc1:


    me "E cosa dovrei fare...?"


translate italian day7_sl_70f1dc1a:


    "Le sue parole hanno scatenato una marea di emozioni in me."


translate italian day7_sl_22659260:


    th "Da un lato, riuscirò finalmente ad andarmene da questo dannato Campo. Potrei trovare qualche risposta."


translate italian day7_sl_a8307773:


    th "D'altra parte – ho appena trovato una cosa molto importante per me. E ora dovrei lasciare che se ne vada?"


translate italian day7_sl_63f53767:


    sl "Cosa intendi?"


translate italian day7_sl_869602ed:


    me "Oh, niente…{w} E quand'è la partenza?"


translate italian day7_sl_4e45ffe9:


    sl "Questa sera, verso le cinque o le sei."


translate italian day7_sl_431ec0f7:


    me "Beh, c'è ancora un sacco di tempo!"


translate italian day7_sl_818ed3e4:


    sl "Sì, ma ho alcune cose da sbrigare.{w} E dovrei andare a trovare Zhenya, potrebbe essere preoccupata, dato che sono stata assente per l'intera notte."


translate italian day7_sl_c62f6955:


    th "Oh, certo, preoccuparsi di qualcuno è proprio da lei…"


translate italian day7_sl_142536ec:


    sl "Ci vediamo a pranzo!"


translate italian day7_sl_412b31af:


    "Slavya mi ha dato un bacio sulla guancia ed è corsa via."


translate italian day7_sl_9f3c503e:


    "Sono rimasto in piedi davanti all'entrata della mensa."


translate italian day7_sl_cdfd20d4:


    "Non avevo proprio nulla da fare, nulla da preparare, quindi l'unica cosa rimasta da fare era vagabondare per il Campo fino a sera e pensare, pensare, pensare…"


translate italian day7_sl_23106771:


    th "Darei qualsiasi cosa per poter liberare la mia mente in queste ultime dieci ore."


translate italian day7_sl_630dd74a:


    "Una voce mi ha tirato fuori dal mio stato di trance."


translate italian day7_sl_1acf0704:


    el "Ciao!"


translate italian day7_sl_e7fb612f:


    "Electronik stava davanti a me, allegro come sempre."


translate italian day7_sl_b5cc7e1c:


    me "Altrettanto…"


translate italian day7_sl_d60a1514:


    "Ho risposto distrattamente."


translate italian day7_sl_206f44a2:


    el "Sai, l'intero Campo sta parlando di voi!"


translate italian day7_sl_d57bdfe4:


    me "Credo di sapere perché…"


translate italian day7_sl_1c122ee3:


    el "E così… l'avete già fatto?"


translate italian day7_sl_c8bd0473:


    "Ha chiesto, ridacchiando."


translate italian day7_sl_a19056f4:


    me "Pensi davvero che siano affari tuoi?"


translate italian day7_sl_e84597be:


    el "Beh no. Volevo solo…"


translate italian day7_sl_ad94eafa:


    me "Allora sta' zitto!"


translate italian day7_sl_5c46a06d:


    "Lasciandolo solo con le sue congetture, mi sono incamminato verso la piazza."


translate italian day7_sl_dc17db89:


    th "Perché deve comportarsi in un modo che fa infuriare le persone?{w} Anche se probabilmente fa impazzire solo me…"


translate italian day7_sl_7036dfea:


    "Ero completamente immerso nei miei pensieri, senza guardarmi intorno, e non avevo notato Miku fino a quando non ci siamo urtati."


translate italian day7_sl_d8625dc6:


    mi "Oh, scusa! Dovrei fare più attenzione! Ero persa nei miei pensieri e non ti ho notato. Sai come succede… Oh, e ciao!"


translate italian day7_sl_38926eb8:


    "Ho guardato attraverso di lei e ho continuato a camminare."


translate italian day7_sl_425d7917:


    mi "Oh, Semyon, aspetta un secondo! Cosa c'è tra te e Slavya? Dimmi, è così interessante! Sembra che tutti nell'intero Campo sappiano della cosa, tranne me. No, non è che io sia curiosa, ma dal momento che tutto sembra essere così serio…"


translate italian day7_sl_933d1b5a:


    "Affermare che lei non fosse curiosa era come negare una verità universale."


translate italian day7_sl_f6740215:


    me "Niente di speciale."


translate italian day7_sl_1e997695:


    "Ho risposto, senza voltarmi verso di lei."


translate italian day7_sl_dc2db981:


    mi "Beh, se non ti va…"


translate italian day7_sl_52f2af91:


    "Ho cercato di isolarmi dalle irritazioni esterne, in modo che le successive parole di Miku mi oltrepassassero."


translate italian day7_sl_2d7ae10b:


    "La piazza è stata presto sostituita dalla spiaggia."


translate italian day7_sl_5e0ecadd:


    "Per fortuna, la gemma giovanile della scena pop sovietica non era nei dintorni."


translate italian day7_sl_0f673e81:


    "Mi sono seduto sulla sabbia e ho iniziato a fissare il fiume."


translate italian day7_sl_98d846ec:


    th "Perché diavolo l'intero Campo è così interessato a sapere del mio rapporto con Slavya?"


translate italian day7_sl_bb84fa35:


    th "Nel mio mondo nessuno avrebbe detto una parola.{w} Almeno, non così sfacciatamente."


translate italian day7_sl_c310401c:


    th "Almeno lì non riesci a sentire cosa dicono alle tue spalle."


translate italian day7_sl_0d2a8813:


    "Ero disposto a sopportare i vari pettegolezzi, fintantoché non dovessi riferire a ognuno i dettagli della mia vita privata."


translate italian day7_sl_5db83ec4:


    "Il sole si stava alzando sempre più, e io mi stavo addormentando."


translate italian day7_sl_948fae16:


    "I miei occhi erano già quasi chiusi, quando all'improvviso ho sentito qualcuno sedersi accanto a me."


translate italian day7_sl_08478924:


    "Era Alisa."


translate italian day7_sl_6a87787c:


    dv "Congratulazioni!"


translate italian day7_sl_75ba0772:


    me "Per cosa?"


translate italian day7_sl_caa43468:


    dv "Non lo sai?"


translate italian day7_sl_7a835092:


    th "Già, non bisogna essere Einstein per capirlo…"


translate italian day7_sl_321eb6b8:


    me "Ad essere sincero, mi sono proprio stancato di tutto questo…{w} Ognuno non fa altro che menzionarlo.{w} Basta!"


translate italian day7_sl_e5a5925c:


    dv "E cosa ti aspettavi?{w} È una cosa rara in questo Campo. Oserei dire anche eccezionale!"


translate italian day7_sl_ec2c2a7c:


    "Ha riso."


translate italian day7_sl_513d88a1:


    dv "E tener testa alla leader del Campo in quel modo! Persino Ulyana non ci sarebbe riuscita.{w} Nemmeno io, probabilmente..."


translate italian day7_sl_687ba52e:


    "Non sapevo come considerare quelle sue parole – se un complimento o una presa in giro."


translate italian day7_sl_9eb3d677:


    me "Sai, stai considerando la cosa in modo troppo estremo.{w} Non c'è niente di insolito per me."


translate italian day7_sl_b241d169:


    me "Inoltre, non capisco il motivo per cui tu debba ficcare il naso nella mia vita privata.{w} Non credi che siano solo affari miei e di nessun altro?"


translate italian day7_sl_e9cbda60:


    dv "Forse sì… Forse no…"


translate italian day7_sl_576ab57f:


    "Ha detto misteriosamente."


translate italian day7_sl_fc97acab:


    "Siamo rimasti seduti in silenzio per un po'."


translate italian day7_sl_749ec097:


    "Volevo che se ne andasse il prima possibile, ma Alisa stava osservando i pionieri sulla spiaggia, e non mi sembrava che avesse intenzione di andare da nessuna parte."


translate italian day7_sl_0ee3b46c:


    dv "Allora, cos'hai intenzione di fare adesso?"


translate italian day7_sl_cb23542f:


    me "Di cosa stai parlando?"


translate italian day7_sl_518e3571:


    dv "Oggi è l'ultimo giorno."


translate italian day7_sl_7321b637:


    me "Sì, e allora?"


translate italian day7_sl_9772f413:


    dv "Tutti se ne andranno."


translate italian day7_sl_23104825:


    me "E quindi?"


translate italian day7_sl_8503bc50:


    dv "Anche Slavya se ne andrà."


translate italian day7_sl_38f8adbc:


    me "E anche tu te ne andrai. E allora?"


translate italian day7_sl_26679ce6:


    dv "Vi separerete."


translate italian day7_sl_6474973e:


    th "Lo so già. Grazie."


translate italian day7_sl_467d3cb1:


    me "Ovviamente."


translate italian day7_sl_3030a38d:


    dv "E cosa ne pensi?"


translate italian day7_sl_46d67e60:


    me "Che cosa dovrei pensare? Non posso di certo restare qui per sempre."


translate italian day7_sl_cbf330c2:


    dv "Certo che no.{w} Ma cosa, vuoi lasciarla andare proprio così?"


translate italian day7_sl_1ecab4b2:


    me "No…{w} Voglio dire, cosa posso fare?"


translate italian day7_sl_fd11f974:


    "Francamente parlando, non avevo proprio idea di cosa avrei fatto in seguito.{w} Nella mia testa c'era un caos totale."


translate italian day7_sl_2aefe5ec:


    th "Riuscirò ad andarmene da qui. Questa è la cosa che desideravo maggiormente durante l'intera scorsa settimana. Ma adesso invece devo rispondere a una domanda molto più difficile."


translate italian day7_sl_4892f4fc:


    th "Come finivano le «storie d'amore estive» per gli adolescenti sovietici normali (e siamo sicuri che la mia storia d'amore sia proprio «estiva»)?{w} Tornavano tutti a casa ed era finita lì…"


translate italian day7_sl_78491662:


    th "Nonostante vivessero a migliaia di chilometri di distanza gli uni dagli altri.{w} E nel mio caso – in mondi diversi."


translate italian day7_sl_a5e6cfbd:


    th "Non è che in un paio d'ore si possa trovare una soluzione decente per un tale problema."


translate italian day7_sl_31914dbe:


    dv "Come se lo sapessi! Sta a te decidere!"


translate italian day7_sl_2da8076e:


    "Alisa ha fatto un ghigno, si è alzata ed è cosa verso il fiume."


translate italian day7_sl_2b4a56da:


    "Mancava solo circa mezz'ora al pranzo, e così mi si sono incamminato lentamente verso la mensa."


translate italian day7_sl_c60c50bf:


    "I pionieri non sono riusciti ad occupare tutti i tavoli liberi, quindi ho potuto scegliere un posto abbastanza decente, ma, con mio rammarico, la mia postazione preferita nell'angolo in fondo era già stata occupata."


translate italian day7_sl_03bda289:


    "Ero sul punto di iniziare a mangiare, quando qualcuno improvvisamente ha tirato fuori una sedia dal mio tavolo, e si è seduto."


translate italian day7_sl_d7e5ecca:


    us "Ciao! Come va?"


translate italian day7_sl_556f4ec1:


    "Era Ulyana.{w} Una versione straordinariamente gentile di lei."


translate italian day7_sl_892a8819:


    me "Andava bene fino a un secondo fa."


translate italian day7_sl_27c62366:


    "Ho sogghignato."


translate italian day7_sl_11569ff2:


    us "E dai! Perché invece non mi racconti cos'hai intenzione di fare?"


translate italian day7_sl_59c7266e:


    me "Mangiare."


translate italian day7_sl_9da1463d:


    "Ho sospirato."


translate italian day7_sl_10e5fe18:


    us "Non sto parlando di quello.{w} La sessione è finita, tu e Slavya vi dovrete lasciare, cosa succederà dopo?"


translate italian day7_sl_5c9ad26b:


    th "È come se il mio futuro fosse la questione prioritaria per tutti i residenti locali."


translate italian day7_sl_af94a60d:


    me "Ehi, qual è il tuo problema?"


translate italian day7_sl_f13441a0:


    us "Sono solo curiosa!"


translate italian day7_sl_15ec1554:


    me "Curiosa di cosa?{w} Io non ti chiedo che cosa hai intenzione di fare domani, dopodomani o tra un mese! Perché devi proprio infilarti nella vita personale degli altri?"


translate italian day7_sl_5b024fb2:


    us "Non sto cercando di infilarmi in niente."


translate italian day7_sl_c6e7d0a8:


    "Mi ha detto Ulyana, offesa."


translate italian day7_sl_85f844bf:


    us "Non sono molto brava in questo genere di cose…"


translate italian day7_sl_7b60b058:


    me "Vero, sei ancora una ragazzina."


translate italian day7_sl_81615f37:


    us "Allora dovresti spiegarmi, dato che sei tu l'adulto qui!"


translate italian day7_sl_7c3e569d:


    me "E cosa dovrei spiegarti?"


translate italian day7_sl_71f29a50:


    us "Avete intenzione di rimanere in contatto scambiandovi lettere, giusto? E poi avete intenzione di tornare insieme in questo Campo la prossima estate?"


translate italian day7_sl_e2283e70:


    th "In contatto…{w} Scambiandoci lettere…"


translate italian day7_sl_33774013:


    "Suona così selvaggio."


translate italian day7_sl_7e748919:


    "E non intendo l'idea di usare carta e penna. Mi riferisco alla possibilità di perdere Slavya per un anno, o forse anche di più.{w} O magari per sempre…"


translate italian day7_sl_946147fa:


    us "Allora?"


translate italian day7_sl_e67f8463:


    sl "Posso unirmi a voi?"


translate italian day7_sl_cf0d6604:


    "Ho alzato lo sguardo e ho visto Slavya in piedi accanto a me con un vassoio in mano."


translate italian day7_sl_5f86c688:


    us "Ok, è ora di andare."


translate italian day7_sl_b80fdba5:


    "Ulyana è saltata giù e si è precipitata verso l'angolo nel lato opposto della mensa."


translate italian day7_sl_4d6b2adc:


    sl "Di cosa stavate parlando?"


translate italian day7_sl_1ec6c417:


    me "La conosci bene. Di cosa avremmo potuto parlare?"


translate italian day7_sl_c17a722d:


    "Slavya ha sorriso."


translate italian day7_sl_750701bd:


    me "Hai finito con le tue faccende?"


translate italian day7_sl_0dad5262:


    sl "Sì! Adesso sono totalmente libera. Posso aiutarti a fare i bagagli."


translate italian day7_sl_7c158bc0:


    me "Non è che io abbia molte cose da preparare…"


translate italian day7_sl_4bb84a95:


    sl "Bene, allora forse potresti aiutare me."


translate italian day7_sl_71d3b8c8:


    me "Ma certo."


translate italian day7_sl_852ac74c:


    "Quella era l'occasione giusta per poter parlare con lei in tranquillità.{w} Anche se non avevo la minima idea di cosa parlare."


translate italian day7_sl_0494524b:


    "Ben presto abbiamo terminato il nostro pasto e ci siamo diretti verso la stanza di Slavya."


translate italian day7_sl_33094fdb:


    "Lì dentro tutto era pulito e ordinato, a differenza della stanza di Olga Dmitrievna."


translate italian day7_sl_31d68c71:


    sl "Oh, non saprei da dove iniziare…"


translate italian day7_sl_917a3612:


    "Ha tirato fuori la sua borsa da viaggio e ha cominciato a rovistare in un armadio."


translate italian day7_sl_1b0f9bba:


    "Nel frattempo mi sono seduto sul letto e ho cominciato a raccogliere i miei pensieri."


translate italian day7_sl_1cdd2e37:


    me "Hai qualche piano dopo il Campo? Qualche idea su cosa fare?"


translate italian day7_sl_af11ccdd:


    sl "Beh, sai…"


translate italian day7_sl_943eaf3a:


    "Ha alzato lo sguardo verso di me e mi ha fatto un sorriso."


translate italian day7_sl_01aec217:


    sl "Tornerò a scuola."


translate italian day7_sl_2c057d1c:


    me "Ah… Già…"


translate italian day7_sl_5c7b7c48:


    sl "E tu?"


translate italian day7_sl_79f50df9:


    th "Che dire di me?{w} Non è che io stia veramente pensando di tornare a scuola una seconda volta…"


translate italian day7_sl_2260325d:


    me "Io? Beh, anche io, immagino."


translate italian day7_sl_31433173:


    me "Anche se… Non ho neppure voglia di tornare alla mia città natale."


translate italian day7_sl_52dd0423:


    th "Seriamente parlando, anche se volessi tornare a casa, non ho proprio alcun posto dove andare."


translate italian day7_sl_70f37a53:


    sl "Perché?"


translate italian day7_sl_04bffd62:


    me "Ecco… Perché lì non c'è niente da fare. Niente di niente. E non c'è nessuno che mi aspetta lì."


translate italian day7_sl_a6afd81b:


    sl "Ma i tuoi genitori?"


translate italian day7_sl_dd0b7f61:


    me "I miei genitori…{w} Beh, sai… Loro non sono lì adesso."


translate italian day7_sl_7a41296e:


    sl "E dove sono allora?"


translate italian day7_sl_848f65f9:


    me "Stanno... lavorando all'estero."


translate italian day7_sl_0857dec3:


    sl "Ah, quindi sei un ragazzo fortunato!"


translate italian day7_sl_d2b56c39:


    "Ha sorriso allegramente."


translate italian day7_sl_5ac38cf1:


    me "Perché?"


translate italian day7_sl_347ac558:


    sl "Ti mandano la roba estera e cose simili."


translate italian day7_sl_36322c82:


    th "Non direi che la mia situazione sia così fortunata."


translate italian day7_sl_ce4797d9:


    me "Beh, comunque..."


translate italian day7_sl_6a5f7148:


    sl "Non essere triste!"


translate italian day7_sl_aa9df60c:


    th "Sembra proprio che lei non capisca quello che le sto dicendo."


translate italian day7_sl_496c0518:


    me "Non sono triste per questa cosa."


translate italian day7_sl_61615b2e:


    "Ed era vero."


translate italian day7_sl_926dd5d8:


    sl "Allora per cosa?"


translate italian day7_sl_56cbe60a:


    me "Un sacco di cose…"


translate italian day7_sl_b8748488:


    sl "Per esempio?"


translate italian day7_sl_eecf1d19:


    me "Beh, per esempio…"


translate italian day7_sl_95862924:


    "Stavo per dare inizio a un grande sfogo riguardo alla mia situazione e al mio desiderio di non separarmi da lei, quando la porta si è spalancata con un botto, e Zhenya è entrata."


translate italian day7_sl_d59f2277:


    mz "Ah, eccoti."


translate italian day7_sl_c58c9392:


    sl "Sì, ci stiamo preparando."


translate italian day7_sl_dcbd9d98:


    mz "Capisco… È un bene che io non sia venuta dieci minuti più tardi, o avrei disturbato la vostra…«preparazione»."


translate italian day7_sl_afb6e684:


    "Slavya è arrossita e ha continuato a infilare ansiosamente le sue cose nella borsa."


translate italian day7_sl_67bf96a7:


    mz "Che succede, eroico amante?"


translate italian day7_sl_146cea46:


    "Quel nomignolo mi ha fatto venire la pelle d'oca."


translate italian day7_sl_ad02f53a:


    me "Cosa?"


translate italian day7_sl_95bc0f12:


    mz "Niente…"


translate italian day7_sl_c7bb77e9:


    "Mi ha fissato intensamente per qualche secondo."


translate italian day7_sl_f9da3a8f:


    mz "Sta' attento! Adesso non sei l'unico a dipendere dalle tue scelte."


translate italian day7_sl_60a03249:


    "Non le ho risposto, e con quelle parole Zhenya ha abbandonato la stanza."


translate italian day7_sl_404c3ae1:


    sl "Che cosa intendeva dire?"


translate italian day7_sl_9ec962ea:


    me "È impossibile saperlo con esattezza."


translate italian day7_sl_f277b278:


    sl "Eppure..."


translate italian day7_sl_2922f709:


    me "Beh… Volevo parlartene io stesso."


translate italian day7_sl_b7c51fee:


    th "Sono riuscito a fare il primo passo, ma poi…"


translate italian day7_sl_d00ac2ba:


    sl "Parlarmi di cosa?"


translate italian day7_sl_b40211ef:


    "Slavya mi ha guardato attentamente."


translate italian day7_sl_4ee92ca9:


    me "È solo che, oggi ce ne andiamo, giusto?"


translate italian day7_sl_5ede84c0:


    sl "Giusto."


translate italian day7_sl_cb5149bf:


    me "E torneremo alle nostre case."


translate italian day7_sl_be32f309:


    sl "Ebbene sì."


translate italian day7_sl_774c66dd:


    me "E quando potrò rivederti?"


translate italian day7_sl_bbc1f2e4:


    "Slavya si è fatta pensierosa."


translate italian day7_sl_8a3c9e52:


    sl "Non lo so…"


translate italian day7_sl_05cb701f:


    me "Non lo so neppure io…"


translate italian day7_sl_30ac20c5:


    sl "Ma potrai scrivermi!"


translate italian day7_sl_6f3d442d:


    "Ha sorriso, ma il suo sorriso non mi ha rappacificato la mente."


translate italian day7_sl_f8d768e0:


    me "Sì, certo, ma vedi, non è la stessa cosa."


translate italian day7_sl_926dd5d8_1:


    sl "Quindi cosa?"


translate italian day7_sl_d9c6a876:


    me "Posso venire con te?"


translate italian day7_sl_a2800813:


    "Ho deciso di rischiare e di andare dritto al sodo."


translate italian day7_sl_aa131597:


    sl "E cosa farai lì?"


translate italian day7_sl_f4e53387:


    "Slavya sembrava non essere sorpresa, ma piuttosto un po' infastidita."


translate italian day7_sl_2f7570dd:


    sl "La scuola, i miei genitori…{w} E che dirà la gente?"


translate italian day7_sl_7768528c:


    "Non stavo pensando alle opinioni delle altre persone.{w} Ad essere sincero, non mi preoccupavano più di tanto."


translate italian day7_sl_6e0d3350:


    me "Beh, possiamo sempre trovare un modo!"


translate italian day7_sl_c7e0fd92:


    sl "Che modo?"


translate italian day7_sl_baa95abd:


    me "Penseremo a qualcosa!"


translate italian day7_sl_5d12fc48:


    sl "Semyon, siamo ancora troppo giovani per queste cose…"


translate italian day7_sl_0acece57:


    th "In realtà, io non lo sono."


translate italian day7_sl_2325fa76:


    me "E allora?"


translate italian day7_sl_655480b8:


    sl "Magari tra un po' di tempo…"


translate italian day7_sl_870d8d45:


    me "Di quanto tempo hai bisogno? Un anno? Due anni? Cinque?"


translate italian day7_sl_742d5557:


    sl "Lo dici come se…{w} Non lo so…"


translate italian day7_sl_dafbd76d:


    "Sembrava confusa."


translate italian day7_sl_c39dd65b:


    me "Quindi tutto quello che è successo tra di noi è stata solo una storiella estiva?"


translate italian day7_sl_262d7cae:


    "Ho iniziato ad arrabbiarmi."


translate italian day7_sl_2732ce31:


    sl "No! Certo che no! È solo…"


translate italian day7_sl_ad02f53a_1:


    me "Che cosa?"


translate italian day7_sl_d9d64c2f:


    sl "Non sono pronta a discuterne adesso. Parliamone più tardi."


translate italian day7_sl_ffe0dbb0:


    me "Più tardi quando? Mancano solo un paio d'ore alla partenza."


translate italian day7_sl_86719bd3:


    "Slavya non ha risposto e ha continuato a preparare la sua borsa."


translate italian day7_sl_e6f46fe1:


    "Mi sarei aspettato ogni tipo di reazione da parte sua, ma non questa."


translate italian day7_sl_31110d51:


    "In poche parole, mi stava respingendo."


translate italian day7_sl_c4036085:


    th "Pare che non sia interessata a continuare il nostro rapporto.{w} Questo significa che io non sono così importante per lei."


translate italian day7_sl_bad_adf2ccc7:


    me "Senti, continuo a non capire."


translate italian day7_sl_bad_90097fb1:


    sl "Che cosa?"


translate italian day7_sl_bad_2034de62:


    "Mi ha chiesto senza voltarsi."


translate italian day7_sl_bad_dbb5b4ae:


    me "Perché non vuoi…"


translate italian day7_sl_bad_3c8f9ea1:


    sl "E cosa ti aspettavi?"


translate italian day7_sl_bad_2cddef76:


    me "Beh, posso trovare un lavoro, affittare un appartamento."


translate italian day7_sl_bad_e867c7b6:


    sl "E poi cosa? Hai almeno un diploma?"


translate italian day7_sl_bad_c6d4d90d:


    th "In realtà, sì. Ce l'ho da qualche parte nel mio ripostiglio…"


translate italian day7_sl_bad_ae7c378c:


    me "Non importa!"


translate italian day7_sl_bad_af407039:


    sl "Perché no?"


translate italian day7_sl_bad_74f68a72:


    me "Se sei determinato puoi scavalcare ogni ostacolo!{w} Inoltre, non sono così insormontabili."


translate italian day7_sl_bad_a9c0b671:


    sl "Questo è quello che credi tu."


translate italian day7_sl_bad_54740717:


    me "Forse hai ragione. Ma in ogni caso, non riesco proprio a capire la tua reazione!{w} Dopotutto, se ci lasciamo, finirà tutto così, sarà la fine…"


translate italian day7_sl_bad_0f3b42d7:


    sl "Non ho detto questo."


translate italian day7_sl_bad_590152a9:


    me "Ma mi pare sia ovvio."


translate italian day7_sl_bad_fb896de0:


    sl "E cosa te lo fa pensare?!"


translate italian day7_sl_bad_544fc2bc:


    "Ha urlato."


translate italian day7_sl_bad_ca84e284:


    "Era la prima volta che vedevo Slavya comportarsi così."


translate italian day7_sl_bad_0d2c9366:


    me "Beh, mettiamola diversamente. Non voglio semplicemente tornare al luogo in cui vivo…{w} e comunicare con te solo tramite lettere o via telefono, sperando di incontrarti di nuovo."


translate italian day7_sl_bad_dc38e5e1:


    sl "Sarà difficile anche per me."


translate italian day7_sl_bad_7512585c:


    me "Ebbene, qual è il problema allora?"


translate italian day7_sl_bad_380f8d60:


    sl "Sai, non tutto dipende da noi…"


translate italian day7_sl_bad_cfeb918e:


    me "E da chi dipende, allora?!"


translate italian day7_sl_bad_30abaa79:


    th "Anche se nessuno potrebbe dire con certezza se lei abbia più ragione di me. Se dipendesse da me, non avrei mai accettato di finire in questo luogo."


translate italian day7_sl_bad_87a8fd14:


    sl "Dopotutto ci sono le regole sociali, le leggi, la morale, l'etica."


translate italian day7_sl_bad_d80f315d:


    me "E cosa c'entrano? Sono davvero così importanti?!"


translate italian day7_sl_bad_cdfc45ff:


    sl "Forse non lo sono per te, ma lo sono per me."


translate italian day7_sl_bad_2a579a0f:


    me "Va bene.{w} Sono anche disposto a comportarmi secondo le tue regole, ma c'è qualcosa di più importante."


translate italian day7_sl_bad_2952e1d9:


    sl "Forse… Ma non posso così all'improvviso…"


translate italian day7_sl_bad_3d0a9e16:


    me "Non hai molto tempo per decidere."


translate italian day7_sl_bad_e18fa2db:


    "Mi sono fermato un istante per raccogliere i miei pensieri."


translate italian day7_sl_bad_a86972c4:


    me "Io posso offrirti solo me stesso...{w} Perché è l'unica cosa che ho."


translate italian day7_sl_bad_fbcc5341:


    me "Inoltre, non so che cosa mi potrebbe accadere tra un'ora, ma sono sicuro che qualunque cosa accada, se dovessi ripartire da zero, allora lo voglio fare con te."


translate italian day7_sl_bad_7c100b18:


    sl "Le tue parole sono così belle, ma prova a immaginare come mi sento!{w} Per te è tutto così semplice, ma io ho la mia vita a cui sono abituata, e stravolgere tutto così all'improvviso…"


translate italian day7_sl_bad_34cae222:


    me "Anch'io ce ne avevo una…"


translate italian day7_sl_bad_adf03656:


    "Ho detto a bassa voce."


translate italian day7_sl_bad_e07c37b3:


    sl "Che cosa?"


translate italian day7_sl_bad_3a389e87:


    me "Sto dicendo che è inevitabile.{w} I cambiamenti fanno parte della vita, non si può vivere senza affrontarli."


translate italian day7_sl_bad_235fca6e:


    "Slavya sembrava pensierosa, si è fermata per un momento tenendo la borsa tra le mani, poi si è avvicinata, si è seduta accanto a me e ha appoggiato la testa sulla mia spalla."


translate italian day7_sl_bad_91e26f2b:


    sl "Va bene, ti credo. Se lo dici tu, allora che sia così!"


translate italian day7_sl_bad_ae141d61:


    "In quel momento mi sono sentito infinitamente felice."


translate italian day7_sl_bad_e2ff5a09:


    th "Non importa che solo una settimana fa mi trovavo in un mondo completamente diverso, in un'altra vita.{w} Tutto quello ormai fa parte del passato, e ora mi sembra come qualcosa di selvaggio o estraneo."


translate italian day7_sl_bad_f903c358:


    "Non riesco proprio a capire come potevo considerarlo normale."


translate italian day7_sl_bad_eb2a107a:


    th "Ora sono meno preoccupato per come e per mano di chi tutto questo sia accaduto."


translate italian day7_sl_bad_3548661d:


    th "Adesso non ho più bisogno di trovare risposte.{w} Perché dovrei, quando sono tutte qui, proprio davanti ai miei occhi?"


translate italian day7_sl_bad_5a380cf2:


    th "Ora mi attende un lungo percorso, mano nella mano con la persona che amo.{w} E guardare indietro non ha più alcun senso."


translate italian day7_sl_bad_5734f7c1:


    "Avrei voluto starmene seduto così per sempre, fermando il tempo, ma Slavya alla fine ha guardato l'orologio e mi ha sussurrato dolcemente all'orecchio:"


translate italian day7_sl_bad_b0527f28:


    sl "È ora."


translate italian day7_sl_bad_2df15734:


    me "Sì, esattamente.{w} Tu va' alla fermata dell'autobus, e io corro alla casetta della leader del Campo per prendere le mie cose."


translate italian day7_sl_bad_e0c8585e:


    sl "Sei così disorganizzato! Avresti dovuto preparare tutto in anticipo."


translate italian day7_sl_bad_5e6d7806:


    "Ha riso Slavya."


translate italian day7_sl_bad_34d1b774:


    me "Non ho molte cose da prendere comunque…"


translate italian day7_sl_bad_a4b3d627:


    "Sono balzato fuori dalla stanza e sono corso verso la casetta di Olga Dmitrievna."


translate italian day7_sl_bad_2be7619b:


    "La leader del Campo stava sulla soglia, sembrava mi stesse aspettando."


translate italian day7_sl_bad_6cda25dd:


    mt "Credevo che non saresti venuto…"


translate italian day7_sl_bad_1a154f74:


    "Non le ho risposto, mi sono limitato ad entrare, a mettere in una borsa i miei vestiti invernali e ad uscire dalla stanza."


translate italian day7_sl_bad_d5902b51:


    mt "Dai andiamo, è giunto il momento."


translate italian day7_sl_bad_db5dcd8e:


    "In un minuto ci siamo ritrovati alla fermata dell'autobus, insieme agli altri pionieri."


translate italian day7_sl_bad_709d3e7d:


    mt "Ci siete tutti?"


translate italian day7_sl_bad_14167bf5:


    "Ha iniziato Olga Dmitrievna."


translate italian day7_sl_bad_044ab0d8:


    mt "È arrivato per voi il momento di lasciare questo Campo, e vorrei dirvi qualche parola."


translate italian day7_sl_bad_00123e37:


    "Era visibilmente nervosa, alla disperata ricerca delle parole giuste."


translate italian day7_sl_bad_019ed77a:


    mt "Spero che questa esperienza vi rimarrà nel cuore per tutta la vita, e che conserverete solo ricordi piacevoli del campo «Sovyonok»."


translate italian day7_sl_bad_b48fe53b:


    mt "Spero anche che questa esperienza vi abbia reso delle persone migliori, che abbiate imparato qualcosa di nuovo, e che vi siate fatti dei nuovi amici qui…{w} Spero… di rivedervi tutti l'anno prossimo."


translate italian day7_sl_bad_42256d78:


    "Si è voltata.{w} Sembrava che stesse cercando di trattenere le lacrime."


translate italian day7_sl_bad_6a707c88:


    "Non mi sarei aspettato che potesse essere una persona così emotiva. Ma ero totalmente d'accordo con ogni sua parola."


translate italian day7_sl_bad_45cf8a7c:


    "Forse è stata la prima volta che le sue parole non hanno semplicemente superato le mie sorde orecchie."


translate italian day7_sl_bad_e64058b6:


    "Credo che mi mancherà questo Campo.{w} E persino Olga Dmitrievna."


translate italian day7_sl_bad_53f18694:


    th "Dopotutto, se questo è l'inizio di una nuova vita, allora perché non tornare qui ancora una volta?"


translate italian day7_sl_bad_6eb2ae8d:


    "Ci siamo lasciati «Sovyonok» alle spalle.{w} La settimana è trascorsa come un istante, ed è già giunta al termine, ma qualcosa di nuovo stava per iniziare. La mia nuova vita in un nuovo mondo.{w} Insieme a Slavya."


translate italian day7_sl_bad_d27ec580:


    "La luna splendeva, rendendo tutto talmente luminoso che sembrava fosse giorno. Al di là del finestrino infiniti campi mi scorrevano dinanzi agli occhi, a volte vestiti di grigio, ma non per questo in abiti meno allegri, decorati in lontananza con il verde scuro dei boschi possenti."


translate italian day7_sl_bad_8d851fd8:


    "In ogni caso, il paesaggio era l'ultima delle mie preoccupazioni in quell'istante – mi stavo godendo il momento."


translate italian day7_sl_bad_5fe75290:


    "Io e Slavya stavamo seduti in ultima fila, osservando gli altri pionieri."


translate italian day7_sl_bad_9a99d836:


    "Più avanti, Ulyana stava facendo casino. Correva tra i sedili e gridava.{w} Lena stava leggendo un libro e Alisa stava dormendo."


translate italian day7_sl_bad_86689381:


    th "Pare che questo mondo sia del tutto normale."


translate italian day7_sl_bad_f27eb605:


    th "E non importa davvero di come io sia arrivato qui, se tutto si è rivelato essere così perfetto. Sono diventato una persona diversa, ho incontrato Slavya."


translate italian day7_sl_bad_29dcd13b:


    sl "A cosa stai pensando?"


translate italian day7_sl_bad_94622b60:


    me "Alla vita."


translate italian day7_sl_bad_0bb42e8e:


    sl "E come sta andando?"


translate italian day7_sl_bad_1495be07:


    me "Alla grande!"


translate italian day7_sl_bad_2fd33210:


    "Ha riso dolcemente."


translate italian day7_sl_bad_9bbf9c21:


    me "Sai…"


translate italian day7_sl_bad_8b01c678:


    sl "Parliamo più tardi. Il bus mi sta cullando, vorrei fare un sonnellino se non ti dispiace."


translate italian day7_sl_bad_4bfb0b4b:


    me "Ma certo, fai pure!"


translate italian day7_sl_bad_909286be:


    "Ha appoggiato la sua testa sulla mia spalla e ben presto si è addormentata."


translate italian day7_sl_bad_a20cefa7:


    "..."


translate italian day7_sl_bad_fec04684:


    "L'imminente arrivo al centro del distretto – e un nuovo capitolo della mia vita – non mi preoccupavano affatto."


translate italian day7_sl_bad_e23b4758:


    "Quando sei al settimo cielo, le piccole cose non hanno rilevanza."


translate italian day7_sl_bad_d8cf6937:


    "Pertanto, mi sarei addormentato non appena mi sarei sentito abbastanza stanco, senza temere nulla."


translate italian day7_sl_good_3fe69130:


    me "Pensi che sia complicato solo per te!"


translate italian day7_sl_good_c966fd52:


    sl "Mi sembra che lo sia anche per te. Ma per qualche ragione non vuoi ammetterlo."


translate italian day7_sl_good_8f6ce43c:


    me "E se ti dicessi che non ho un posto dove tornare, che dallo scorso lunedì non c'è più vita per me e che devo ripartire da zero?"


translate italian day7_sl_good_e1b3f8e0:


    sl "Non lo so…{w} Non mi sembra molto plausibile."


translate italian day7_sl_good_bc56b5b9:


    me "E non dovrebbe! Ma è così."


translate italian day7_sl_good_217f925e:


    sl "E allora cosa…{w} Per favore, spiegati meglio."


translate italian day7_sl_good_53896a36:


    me "Sono venuto in questo Campo per puro caso!{w} In realtà, io sono di un altro tempo, forse persino di un altro mondo."


translate italian day7_sl_good_f88606a6:


    me "Dall'inizio del ventunesimo secolo."


translate italian day7_sl_good_41a98fea:


    me "E sono più vecchio di quanto sembro."


translate italian day7_sl_good_4a22576a:


    me "Non ho la minima idea di come io sia finito qui."


translate italian day7_sl_good_08ff2d6a:


    "Slavya mi fissava, ma non ha detto una parola."


translate italian day7_sl_good_b62b586c:


    "Quei momenti sono durati un'eternità."


translate italian day7_sl_good_05affea5:


    "Ho atteso una sua reazione, ho aspettato che si mettesse a ridere di me, che mi insultasse, o anche peggio."


translate italian day7_sl_good_f40ae6c7:


    sl "E perché mi stai dicendo tutto questo?"


translate italian day7_sl_good_31da2fb6:


    "Ha chiesto a bassa voce."


translate italian day7_sl_good_db105195:


    me "Voglio che tu capisca la mia situazione."


translate italian day7_sl_good_3bb29744:


    sl "Ma è…{w} Sembra così stupido."


translate italian day7_sl_good_4c64048c:


    me "Certo che sì! Se fossi in te, reagirei allo stesso modo."


translate italian day7_sl_good_48a19f73:


    sl "E cosa ti aspetti da me?"


translate italian day7_sl_good_53d3fd1d:


    me "Non lo so…{w} Ho sentito il bisogno di confessartelo."


translate italian day7_sl_good_837efc0e:


    sl "Non sto dicendo che tu stia mentendo e non credo che tu sia pazzo, ma devi capire, non posso accettarlo."


translate italian day7_sl_good_2e320730:


    me "Sì, lo capisco…{w} Ho solo pensato che ti avrebbe aiutato a capire la mia situazione e il motivo delle le mie azioni."


translate italian day7_sl_good_5673e9a6:


    sl "Ad essere sincera, non mi ha aiutato a capire."


translate italian day7_sl_good_0257709a:


    "Ho sospirato per la frustrazione."


translate italian day7_sl_good_96e94f68:


    sl "E la mia opinione non cambia."


translate italian day7_sl_good_2ed72018:


    me "Ma..."


translate italian day7_sl_good_0848ad63:


    sl "È meglio che tu ti prepari, dobbiamo fare i bagagli, o faremo tardi."


translate italian day7_sl_good_c744b4ed:


    me "C'è…"


translate italian day7_sl_good_07c08f1b:


    sl "Parleremo più tardi."


translate italian day7_sl_good_d9f15fcb:


    "Non avevo intenzione di discutere con lei."


translate italian day7_sl_good_a1fa17bf:


    th "Se Slavya non ha cambiato idea dopo la mia confessione, allora è inutile insistere in questo momento."


translate italian day7_sl_good_00aaa21e:


    "Afflitto, mi sono trascinato fuori, verso la casetta di Olga Dmitrievna."


translate italian day7_sl_good_f08c18e7:


    "La leader del Campo stava sulla soglia, e sembrava che mi stesse aspettando."


translate italian day7_sl_good_959e190e:


    mt "Perché sei così cupo? È forse successo qualcosa?"


translate italian day7_sl_good_2ba695f3:


    me "No, niente. Sono solo venuto a preparare le mie cose."


translate italian day7_sl_good_3e5d4e16:


    mt "Oh, andiamo, dimmi tutto. Ti sentirai meglio."


translate italian day7_sl_good_44d67563:


    me "Beh…{w} Come pensa che io possa dimostrare che sto dicendo la verità, a qualcuno che non vuole credermi?"


translate italian day7_sl_good_1bcba649:


    mt "Sei assolutamente sicuro che stai dicendo la verità?"


translate italian day7_sl_good_7468f5f7:


    me "Assolutamente!"


translate italian day7_sl_good_b06f64e7:


    mt "Allora forse a quel qualcuno serve più tempo per pensarci su."


translate italian day7_sl_good_d796d61f:


    me "Ma purtroppo di tempo non ce n'è più."


translate italian day7_sl_good_c9524a2a:


    mt "Allora non lo so."


translate italian day7_sl_good_4a9b3777:


    "Ha alzato le spalle, in segno di costernazione."


translate italian day7_sl_good_22cd2b19:


    me "Beh, è già qualcosa."


translate italian day7_sl_good_312eb4e2:


    "Ho gettato il mio abbigliamento invernale in una borsa e mi sono diretto alla fermata dell'autobus."


translate italian day7_sl_good_7e15aae1:


    "Se qualche giorno fa avessi visto un Ikarus parcheggiato davanti al cancello del Campo in attesa dei pionieri, sarei stato la persona più felice di questa realtà."


translate italian day7_sl_good_9eebbd94:


    "Ma in quel momento non ero in vena di gioire."


translate italian day7_sl_good_5b7603c0:


    "C'era ancora un sacco di tempo prima della partenza."


translate italian day7_sl_good_8ef889cc:


    "Mi sono seduto sul marciapiede e ho immerso la testa tra le mani."


translate italian day7_sl_good_d37b9e02:


    un "Perché sei così triste?"


translate italian day7_sl_good_c32db60d:


    "Una voce familiare mi ha tirato fuori dei miei pensieri."


translate italian day7_sl_good_9553ce6a:


    me "È solo che…"


translate italian day7_sl_good_c1368442:


    un "Hai litigato con Slavya?"


translate italian day7_sl_good_dd69c3a1:


    me "Qualcosa del genere."


translate italian day7_sl_good_88021e16:


    un "E qual è il motivo?"


translate italian day7_sl_good_1b550b25:


    me "Non vuole credere che io sia un alieno venuto dal futuro."


translate italian day7_sl_good_abcb2c8d:


    "Ho sorriso ironicamente."


translate italian day7_sl_good_312181fb:


    un "Neppure io ti crederei."


translate italian day7_sl_good_5ffaa56d:


    "Mi ha detto seriamente Lena."


translate italian day7_sl_good_82ba88b1:


    me "Se fossi nei vostri panni, neppure io ci crederei."


translate italian day7_sl_good_f8d26a13:


    un "Non è un grosso problema, vedrai che farete pace!"


translate italian day7_sl_good_3b40e91b:


    me "Sì, ma non è rimasto molto tempo per..."


translate italian day7_sl_good_1e436020:


    un "Per cosa?"


translate italian day7_sl_good_3782f67a:


    me "Per fare la scelta giusta…"


translate italian day7_sl_good_a20cefa7:


    "..."


translate italian day7_sl_good_a48849f7:


    "Dopo un po' di tempo l'intero Campo si è riunito alla fermata dell'autobus."


translate italian day7_sl_good_a2beb057:


    mt "Non manca nessuno?"


translate italian day7_sl_good_14167bf5:


    "Ha iniziato Olga Dmitrievna."


translate italian day7_sl_good_044ab0d8:


    mt "È arrivato per voi il momento di lasciare questo Campo, e vorrei dirvi qualche parola."


translate italian day7_sl_good_00123e37:


    "Era visibilmente nervosa, e non riusciva a trovare le parole giuste."


translate italian day7_sl_good_019ed77a:


    mt "Spero che questa esperienza rimarrà nei vostri cuori per tutta la vita, e che conserverete solo ricordi piacevoli del campo «Sovyonok»."


translate italian day7_sl_good_b48fe53b:


    mt "Spero anche che questa esperienza vi abbia reso delle persone migliori, che abbiate imparato qualcosa di nuovo, e che vi siate fatti dei nuovi amici qui…{w} Spero… di rivedervi tutti l'anno prossimo."


translate italian day7_sl_good_42256d78:


    "La leader del Campo si è voltata.{w} Sembrava che stesse cercando di trattenere le lacrime."


translate italian day7_sl_good_80682609:


    "Non mi sarei aspettato che potesse essere una persona così emotiva."


translate italian day7_sl_good_27a36bac:


    "Anche se il suo discorso mi è sembrato del tutto insensato. Come sempre, del resto."


translate italian day7_sl_good_e115ea83:


    th "Certo, è proprio qui che ho incontrato Slavya per la prima volta. Ma ora stiamo attraversando un momento difficile. E quello che succederà in seguito è assolutamente poco chiaro."


translate italian day7_sl_good_e477e76c:


    "Quindi non mi interessava sapere se mi sarebbe mancato questo posto e Olga Dmitrievna.{w} Improbabile…"


translate italian day7_sl_good_388908da:


    "Mentre i pionieri iniziavano a salire sul bus, mi sono fatto strada tra la folla per raggiungere Slavya."


translate italian day7_sl_good_2be46bfb:


    me "Sediamoci accanto."


translate italian day7_sl_good_b58c757b:


    sl "Va bene."


translate italian day7_sl_good_9087bb2a:


    "Ha detto a bassa voce."


translate italian day7_sl_good_6fa79838:


    "Mi è anche sembrato di aver scorto l'ombra di un sorriso sul suo volto."


translate italian day7_sl_good_d2835e2a:


    "Il bus sfrecciava attraverso le distese del mio nuovo mondo. Fuori i campi si susseguivano alle foreste, e poi fiumi e altipiani."


translate italian day7_sl_good_341bc523:


    "Era tutto così familiare per me, come se si trattasse della mia terra natale."


translate italian day7_sl_good_c8980b8f:


    "In ogni caso, il paesaggio era l'ultima delle mie preoccupazioni in quel momento – stavo cercando di trovare le parole giuste per iniziare una conversazione."


translate italian day7_sl_good_6c7e0e00:


    "Siamo rimasti seduti così per un paio d'ore. Tutti i miei tentativi di attaccare discorso si concludevano con risposte a monosillabi."


translate italian day7_sl_good_f10d9132:


    me "Dimenticati di quello che ti ho detto prima! Non voglio inventarmi scuse o dire che non sia vero.{w} In questo momento – la cosa più importante per me sei tu."


translate italian day7_sl_good_572579b8:


    sl "Capisco."


translate italian day7_sl_good_1b595140:


    "Mi ha guardato attentamente."


translate italian day7_sl_good_8e0333a3:


    me "Anche se a volte mi comporto nel modo sbagliato e dico cose senza senso…{w} Indipendentemente da tutto questo, la cosa più importante per me ora è stare con te! E lo sarà sempre."


translate italian day7_sl_good_552f93e2:


    me "Capisco di aver agito troppo incautamente e di aver pianificato imprese rischiose. Ma riusciremo a fare tutto al meglio. Insieme! Io e te!"


translate italian day7_sl_good_0fb1f792:


    "Slavya continuava a fissarmi."


translate italian day7_sl_good_0907c839:


    sl "Ti rendi conto che sarà difficile? Che non si può semplicemente correre alla cieca nel buio? Perché poi potrebbero sorgere così tanti problemi che non saremo in grado di risolvere."


translate italian day7_sl_good_da9a63e8:


    me "Sì, lo capisco!{w} Ma per te sono pronto a tutto."


translate italian day7_sl_good_b8586c1f:


    "Non ha detto una parola, si è limitata a baciarmi dolcemente, appoggiando la sua testa sulla mia spalla."


translate italian day7_sl_good_62038221:


    "Quello era decisamente un «sì»!"


translate italian day7_sl_good_44708f32:


    "Non volevo più disturbarla – era meglio lasciarla dormire."


translate italian day7_sl_good_e8f795a9:


    "Il punto è che finalmente Slavya mi aveva capito, aveva compreso quello che lei significava per me.{w} Si è resa conto che avrei fatto di tutto per il suo bene."


translate italian day7_sl_good_24228837:


    "E anche se stavo esagerando e stavo prendendo decisioni troppo affrettate, lei era pronta a mettermi sulla strada giusta.{w} La strada che avremmo percorso insieme!"


translate italian day7_sl_good_a20cefa7_1:


    "..."


translate italian day7_sl_good_7aa62860:


    "La notte ha cominciato a rivendicare i propri diritti, e sul bus è piombato il sonno."


translate italian day7_sl_good_5d1c29d3:


    "In un primo momento ho cercato di combatterlo, ma c'era forse senso nell'opporre resistenza e sprecare le mie energie, quando il futuro era finalmente diventato così cristallino per me?"


translate italian day7_sl_good_c01a1705:


    "I miei occhi si sono chiusi solo per un istante..."
# Decompiled by unrpyc: https://github.com/CensoredUsername/unrpyc
