translate italian strings:


    old "Да, я пойду с тобой"
    new "Sì, verrò con te"


    old "Нет, я останусь здесь"
    new "No, resterò qui"


    old "Не отвечать ей"
    new "Non rispondere"


    old "Ответить"
    new "Rispondi"


    old "Побежать за ним"
    new "Corri dietro a lui"


    old "Ничего не делать"
    new "Non fare niente"


    old "Попытаться отнять котлету"
    new "Cerca di riprendere la cotoletta"


    old "Не пытаться отнять котлету"
    new "Lascia perdere"


    old "Взять ключи"
    new "Prendi le chiavi"


    old "Не трогать"
    new "Lascia le chiavi"


    old "Похвалить книгу"
    new "Falle un complimento per il libro"


    old "Ничего не говорить"
    new "Resta in silenzio"


    old "Пойти за картами со Славей"
    new "Vai a prendere le carte con Slavya"


    old "Пойти одному"
    new "Vai da solo"


    old "Поспорить с Алисой"
    new "Scommetti con Alisa"


    old "Не спорить с Алисой"
    new "Non scommettere con Alisa"


    old "Пройти обучение"
    new "Gioca il tutorial"


    old "Пропустить обучение"
    new "Salta il tutorial"


    old "Играть турнир"
    new "Gioca il torneo"


    old "Пропустить турнир (выиграть у Алисы)"
    new "Salta il torneo (vinci contro Alisa)"


    old "Пропустить турнир (проиграть Алисе)"
    new "Salta il torneo (perdi contro Alisa)"


    old "Пропустить турнир (проиграть Ульяне)"
    new "Salta il torneo (perdi contro Ulyana)"


    old "Пропустить турнир (проиграть Лене)"
    new "Salta il torneo (perdi contro Lena)"


    old "Извини, но я уже с Леной договорился"
    new "Scusa, ma l'ho già promesso a Lena"


    old "Ладно, подожди минутку"
    new "D'accordo, aspetta un momento"


    old "Хорошо, я приду..."
    new "Va bene, ci sarò..."


    old "Знаете, меня Ольга Дмитриевна попросила вечером ей помочь..."
    new "Sa, Olga Dmitrievna mi ha chiesto di aiutarla stasera..."


    old "Стоит пойти посмотреть"
    new "Dovrei dare un'occhiata"


    old "Какая разница? Надо продолжить искать ответы"
    new "Che importa? È meglio cercare le risposte"


    old "Пожалуй, я помогу Славе"
    new "Preferisco aiutare Slavya"


    old "Думаю, помогу ребятам в постройке гигантских роботов"
    new "Credo che aiuterò i ragazzi con il loro robot gigante"


    old "Ладно, я помогу спортивному клубу"
    new "Va bene, darò una mano al circolo sportivo"


    old "Ладно, я приду"
    new "Ok, verrò"


    old "Нет, извини"
    new "No, non mi va. Mi spiace"


    old "Сбежать"
    new "Scappa via"


    old "Остаться и помочь Ульяне убраться"
    new "Resta e aiuta Ulyana nelle pulizie"


    old "Пойти к Алисе"
    new "Vai da Alisa"


    old "Пойти на дискотеку"
    new "Vai al ballo"


    old "Рассказать, что был с Алисой"
    new "Dille che sei stato con Alisa"


    old "Придумать другое оправдание"
    new "Inventati una scusa"


    old "Пойти с Мику"
    new "Vai con Miku"


    old "Не ходить с Мику"
    new "Non andare con Miku"


    old "Мне кажется, на тебе это бы смотрелось прекрасно"
    new "Credo che saresti perfetta con questo abito"


    old "Да просто так"
    new "Ero solo curioso…"


    old "Небось ворованных конфет объелась?"
    new "Overdose di caramelle rubate, giusto?"


    old "В столовой отравилась?"
    new "Colpa del cibo della mensa?"


    old "Спросить, что это"
    new "Chiedile del sacchetto"


    old "Не спрашивать"
    new "Non chiederle del sacchetto"


    old "Дать Алисе уголь"
    new "Dai il carbone attivo ad Alisa"


    old "Не давать ей уголь"
    new "Non dare il carbone attivo ad Alisa"


    old "Съесть яблоко"
    new "Mangia la mela"


    old "Не есть"
    new "Non mangiare la mela"


    old "Пошёл с Алисой"
    new "... andare con Alisa"


    old "Пошёл со Славей"
    new "... andare con Slavya"


    old "Пошёл с Ульяной"
    new "... andare con Ulyana"


    old "Пошёл с Леной"
    new "... andare con Lena"


    old "Пошёл один"
    new "... andare da solo"


    old "Налево"
    new "Vai a sinistra"


    old "Направо"
    new "Vai a destra"


    old "Сказать, что ходил с Леной"
    new "Dille che sei andato con Lena"


    old "Сказать, что ходил один"
    new "Dille che sei andato da solo"


    old "Ладно…"
    new "D'accordo..."


    old "Нет, знаешь, у меня дела ещё…"
    new "No, non posso. Sai, avrei delle cose da fare..."


    old "Согласиться"
    new "Accetta"


    old "Отказаться"
    new "Rifiuta"

    old "Пойти с Леной"
    new "Vai con Lena"


    old "Пойти со Славей"
    new "Vai con Slavya"


    old "Да, я старался!"
    new "Già, ho fatto del mio meglio!"


    old "Это всё благодаря помощи девочек!"
    new "È stato grazie all'aiuto delle ragazze!"


    old "Попытаться выхватить книгу у Алисы"
    new "Cerca di afferrare il libro dalle sue mani"


    old "Проявить осторожность"
    new "Non fare niente"


    old "Попытаться узнать, о чём спорят Лена и Алиса"
    new "Cerca di scoprire di cosa stanno discutendo Lena e Alisa"


    old "Не делать ничего, просто сидеть"
    new "Stai seduto senza fare nulla"


    old "Попытаться найти Славю"
    new "Vai a cercare Slavya"


    old "Пойти к Ульяне"
    new "Torna da Ulyana"


    old "Просто сидеть"
    new "Resta seduto"


    old "Не вмешиваться"
    new "Non interferire"


    old "Помочь Алисе"
    new "Soccorri Alisa"


    old "Грубо схватить её за руку"
    new "Afferrala bruscamente per il braccio"


    old "Попытаться остановить словами"
    new "Cerca di fermarla verbalmente"


    old "Да просто есть захотелось"
    new "Ho solo fame"


    old "Хочу взять еду для Слави"
    new "Vorrei prendere qualcosa da mangiare per Slavya"


    old "Мне не в чем оправдываться"
    new "Non ho nulla di cui giustificarmi!"


    old "Мы ничего такого не делали, чтобы перед вами оправдываться!"
    new "Non abbiamo nulla di cui giustificarci!"


    old "Пойти за голосом"
    new "Segui la voce"


    old "Не ходить за голосом"
    new "Non seguire la voce"


    old "Это всё она!"
    new "È stata tutta colpa sua!"


    old "Всё это моя вина..."
    new "È stata tutta colpa mia..."


    old "Идти вперёд"
    new "Prosegui"


    old "Повернуть назад"
    new "Torna indietro"


    old "Выбрать Юлю"
    new "Scegli Yulya"


    old "Выбрать Славю"
    new "Scegli Slavya"


    old "Выбрать Алису"
    new "Scegli Alisa"


    old "Выбрать Лену"
    new "Scegli Lena"


    old "Выбрать Ульяну"
    new "Scegli Ulyana"


    old "Выбрать Машу"
    new "Scegli Masha"
# Decompiled by unrpyc: https://github.com/CensoredUsername/unrpyc
