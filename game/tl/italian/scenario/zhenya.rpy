
translate italian zhenya_route_4060be9e:


    "Ritorna più tardi..."


translate italian zhenya_route_c9a1f434:



    "Il mio è un mondo vuoto, vuoto come lo sono io."


translate italian zhenya_route_f3335cb0:



    "Ho distrutto me stesso per diventare un mondo intero – un mondo inconsistente che mi ha devastato…"


translate italian zhenya_route_37955148:



    "Ho creduto di avere il destino di qualcuno tra le mie mani – di avere il potere di spezzare le vite degli altri."


translate italian zhenya_route_408b289a:



    "Ma l'unica cosa che facevo era vagare nell'oscurità, nascondendomi invano da ogni nuovo raggio di luce, ognuno dei quali non faceva altro che generare delle ombre fatue."


translate italian zhenya_route_2d048c95:



    "Ogni ombra era il mio riflesso senza volto, una nube di buio totale che aveva un unico obiettivo – consumarmi."


translate italian zhenya_route_d09f25e0:



    "Mi nascondevo da esse, mi fondevo con esse, loro diventavano me e io diventavo loro, e alla fine non importava più quale dei due fosse {i}dall'altro lato{/i} dello specchio."


translate italian zhenya_route_ce617998:



    "…"


translate italian zhenya_route_5685d9fa:



    "Anche se in questo caso non si tratta di un cliché o della riflessione di un rozzo perdente."


translate italian zhenya_route_34acc943:



    "È un dato di fatto."


translate italian zhenya_route_162b28bf:



    "Anche se prima era diverso, anche se prima c'erano molti mondi e la mia vita poteva essere chiamata vita."


translate italian zhenya_route_48f0c7b7:



    "Perché tutto si è concluso in un singolo istante."


translate italian zhenya_route_99b70cac:



    "All'inizio era quel dannato bus – di nuovo, ovunque! – E una folla di quei gioiosi e divertiti {i}pionieri{/i} tutt'attorno, del tutto uguali a me…"


translate italian zhenya_route_5f95db12:



    "E poi ancora e ancora – come un vivido caleidoscopio di volti, eventi, emozioni e sentimenti."


translate italian zhenya_route_2c11ec4c:



    "A chi diavolo importa ormai?!"


translate italian zhenya_route_f4cffaa7:



    "Perché sai, quando un uomo muore si preoccupa maggiormente di rimpiangere ciò che non ha avuto tempo di fare, ciò che non ha fatto, ciò che non ha detto, piuttosto che chiedersi come esattamente il suo viaggio sia finito."


translate italian zhenya_route_1c0707ba:



    "Ma io sono forse in uno di quei luoghi dove migrano le anime dopo la morte...?"


translate italian zhenya_route_7eb975bc:



    "E tutto avrebbe potuto anche essere diverso!"


translate italian zhenya_route_61b04b14:



    "Non importa che la fine possa essere una sola e sempre la stessa, se il percorso che ti porta verso essa può variare – dipende dalla propria scelta."


translate italian zhenya_route_11e39e69:



    "L'ho fatto in passato…"


translate italian zhenya_route_5cfa9630:



    "E quindi quando è stato il momento in cui è cambiata ogni cosa?"


translate italian zhenya_route_3136eb3d:



    "Le diverse versioni degli eventi, le linee della vita, il destino delle persone, sembravano intrecciarsi più volte su sé stesse, risultando in qualcosa come una corda di sventura utile solo per impiccarsi!"


translate italian zhenya_route_b9305951:



    "Quando è cambiata ogni cosa...?"


translate italian zhenya_route_bc26a936:



    "Sicuramente non in un singolo giorno o in una singola ora, ma certamente è stato così rapido che non ho nemmeno avuto il tempo di accorgermene."


translate italian zhenya_route_ce617998_1:



    "…"


translate italian zhenya_route_37e5def2:



    "I miei occhi sono stati colpiti dai raggi luminosi del sole estivo."


translate italian zhenya_route_f2bda4fc:



    "Estate – beh, chi l'avrebbe mai detto, ma che sorpresa!"


translate italian zhenya_route_7ebc85db:



    "Ma ieri era inverno…{w} O forse no?"


translate italian zhenya_route_7368e9d8:



    "So che c'è un pacchetto di sigarette e una scatola di fiammiferi nascosti nel cassetto portaoggetti del conducente."


translate italian zhenya_route_a20a3b61:



    "La mia testa ha girato vorticosamente a causa delle profonde boccate di fumo che ho inalato, e il cattivo odore del mentolo ha incatenato il mio cervello con un brivido."


translate italian zhenya_route_4fc8a5c5:



    "«Cosmos» d'esportazione."


translate italian zhenya_route_3468df1f:



    "Sapevano fare dannatamente bene la roba d'esportazione in quel periodo – a differenza di adesso!"


translate italian zhenya_route_006b2395:



    "Reattori nucleari, fucili, sigarette e il comunismo."


translate italian zhenya_route_f59ec965:



    "Un sacco di comunismo – in scatole, casse, contenitori, stive di navi, nei compartimenti di trasporto degli aerei, nei vagoni merci dei treni, persino nelle astronavi!"


translate italian zhenya_route_389b751d:



    "Sempre più comunismo – ai pigmei africani, agli Inca del Sud America, ai papuani della Nuova Guinea…"


translate italian zhenya_route_e9153e64:



    "E credo che possiamo dare un pacchetto persino ai pinguini in Antartide."


translate italian zhenya_route_00e16e71:



    "Argh, sto delirando!"


translate italian zhenya_route_8c8441f3:



    "Dopo aver consumato la terza sigaretta ho fissato il cancello e ho iniziato ad aspettare."


translate italian zhenya_route_3b0dacde:



    "Non ho nemmeno bisogno di un orologio – c'è un cronometro infallibile all'interno della mia testa."


translate italian zhenya_route_df514e2b:



    "Ci siamo quasi, solo un altro po'…"


translate italian zhenya_route_5e5b6ce1:



    "Una delle porte ha cigolato disgustosamente, si è aperta leggermente ed è spuntata una ragazza in uniforme da pioniere, che si è guardata intorno e mi ha notato."


translate italian zhenya_route_b58b784f:



    "Avvicinandosi a me ha annusato l'aria calda e pesante e sembrava essere un po' sorpresa, ma immediatamente ha sorriso e mi ha detto:"


translate italian zhenya_route_7625c414:



    slp "Ciao, devi essere il nuovo…"


translate italian zhenya_route_8433d564:



    "Le ho gridato, mi sono alzato e mi sono diretto al Campo, ignorandola."


translate italian zhenya_route_6a3f1549:



    "La prima volta è stato spassoso, poi è diventato divertente, e ora è solo un'abitudine."


translate italian zhenya_route_fe493094:



    "Una sciocca, ma ossessiva abitudine – come il mettere oggetti diversi in tasche diverse."


translate italian zhenya_route_6761052d:



    "Io, per esempio, non riesco a tenere il telefono nella tasca destra perché…"


translate italian zhenya_route_268e7fb4:



    "Che diavolo ne so!"


translate italian zhenya_route_c21732cf:



    "Ed è lo stesso anche in questo caso: insultare una ragazza di prima mattina è un buon inizio di giornata!"


translate italian zhenya_route_68c3b30c:



    "Ho incontrato un'altra ragazza pioniere sulla mia via."


translate italian zhenya_route_47974352:



    "Per la precisione, ha cercato di colpirmi sulla schiena, ma io mi sono scostato in tempo e l'ho fissata in un modo tale che la povera ragazza ha subito pensato a cose più urgenti da fare."


translate italian zhenya_route_4db95486:



    "La capanna della leader del Campo stava annegando in una tempesta di lillà…{w} Perché non può annegare davvero, dannazione?!"


translate italian zhenya_route_8df4ea96:



    "Lo schianto della porta spalancata ha distratto l'inquilina dalla lettura del suo libro."


translate italian zhenya_route_e969b169:



    "No, non mi sono mai chiesto cosa leggesse la leader del Campo."


translate italian zhenya_route_8aa38276:



    "Senza nemmeno spogliarmi mi sono gettato sul letto e mi sono stravaccato appoggiando i piedi sulla spalliera, con ancora indosso gli scarponi invernali."


translate italian zhenya_route_dc44d1f9:



    mtp "Cosa credi di fare?! E del resto, chi sei tu?!"


translate italian zhenya_route_c982b4e5:



    "Mi ha chiesto la leader del Campo, genuinamente indignata."


translate italian zhenya_route_2045575e:



    pi "Chiudi il becco, donna."


translate italian zhenya_route_bb30f1ca:



    "Le ho detto pigramente."


translate italian zhenya_route_13e9df52:



    "È rimasta pietrificata, incapace di dire una singola parola."


translate italian zhenya_route_fea18646:



    "È sempre così, ogni dannata volta."


translate italian zhenya_route_bed25db3:



    pi "O meglio ancora, dimmi, perché non ti sei ancora trovata un ragazzo? Quanti anni hai, venticinque? Quasi trenta? A quell'età dovresti aver già capito come gira il mondo!"


translate italian zhenya_route_8920f5d5:



    "Per un momento un pesante silenzio ha fluttuato nella stanza."


translate italian zhenya_route_91056f46:



    pi "Non lo sai? Te lo dico io allora – chi diavolo vorrebbe stare con una come te?!"


translate italian zhenya_route_c092ab59:



    "Una sonora risata è esplosa nella dimora della leader del Campo, o forse era più un nitrito di cavallo, mentre una melodia familiare ha risuonato di fuori."


translate italian zhenya_route_46e84c94:



    "Un paio di volte avevo tentato di scoprire da dove provenisse la musichetta della cena, così avrei potuto rimpiazzarla con qualcosa di più allegro, che sarebbe risuonato nell'intero Campo."


translate italian zhenya_route_94ec9f26:



    "Comunque non ci sono riuscito – è come se quel canto angelico non avesse bisogno né di un lettore audio, né di cavi o elettricità."


translate italian zhenya_route_66275b7d:



    "Se ne può andare al diavolo allora…"


translate italian zhenya_route_432de457:



    "Per esempio, in questo momento non mi dispiacerebbe se l'intero Campo sentisse la mia risata, se mi sentisse ridere… e ridere…"


translate italian zhenya_route_60662672:



    "Ma ridere di chi?"


translate italian zhenya_route_31e1abdf:



    "Di certo non di me!"


translate italian zhenya_route_8b953e88:



    "Rido solo perché la risata è un meraviglioso meccanismo di difesa, e un modo per placare la noia."


translate italian zhenya_route_18084d09:



    "Quanto tempo ti concede un minuto di risate? Cinque minuti di vita?"


translate italian zhenya_route_40c9f60e:



    "Beh, a me non servono proprio, io sono di fatto immortale, mi manca solo una certificazione notarile."


translate italian zhenya_route_4517b112:



    mtp "Come ti permetti di parlare in questo modo a un tuo superiore?!"


translate italian zhenya_route_ffdabf91:



    pi "Olya, mia cara, varia un po' il repertorio, ti spiace? Sinceramente, sono stufo di dover ascoltare sempre la stessa cosa più e più volte! Perché non provi con un: «Ciao, come stai?» Visto? Non è poi così difficile! Bene, ora è il tuo turno!"


translate italian zhenya_route_1489a42c:



    mtp "Che cosa...?"


translate italian zhenya_route_45586a8d:



    pi "Non {i}che cosa{/i}, è «Ciao, come stai?»"


translate italian zhenya_route_fd5c0a1b:



    pi "E io ti risponderò: «Ciao, male come sempre». E forse aggiungerò anche un «Grazie per l'interessamento!»"


translate italian zhenya_route_09e93866:



    mtp "Fuori di qui, o chiamo la polizia!"


translate italian zhenya_route_06ad96de:



    pi "E come vorresti farlo? Col piccione viaggiatore? Il telefono non funziona."


translate italian zhenya_route_d7289d90:



    mtp "Sì, ma… io…"


translate italian zhenya_route_4c828f77:



    "Ha balbettato la leader del Campo."


translate italian zhenya_route_a996a0b3:



    pi "Anche se…"


translate italian zhenya_route_89436b87:



    "Ho frugato nella mia tasca – quella di sinistra – ho tirato fuori il mio cellulare e gliel'ho lanciato."


translate italian zhenya_route_9ea2f617:



    pi "Ecco qui, prova con questo."


translate italian zhenya_route_39fe9bde:



    "La leader del Campo ha cercato di afferrare il telefono, facendolo sobbalzare più volte, ed è quasi caduto per terra."


translate italian zhenya_route_d9306537:



    mtp "Cos'è… questo?"


translate italian zhenya_route_d5c62521:



    pi "Un telefono."


translate italian zhenya_route_a411088b:



    "Le ho dato una risposta concisa e ho chiuso gli occhi."


translate italian zhenya_route_5072b3b6:



    pi "Quel genere di cosa, sai, «Drin drin! Pronto, chi parla?»."


translate italian zhenya_route_7af41437:



    mtp "Sei malato di mente?"


translate italian zhenya_route_b216930d:



    "La leader del Campo sembrava essere finalmente tornata in sé."


translate italian zhenya_route_1fdb7186:



    pi "E tu invece?"


translate italian zhenya_route_2d95eff3:



    mtp "Non capisco cosa stia succedendo qui…"


translate italian zhenya_route_6015a2a3:



    pi "Hai mai visto il film «Ricomincio da Capo»? Certo che no. Beh, comunque. Se quella tua biblioteca avesse qualche libro di psichiatria potrei anche essere in grado di rispondere alla tua domanda riguardo alla mia situazione psichica con maggiore certezza. Ecco perché per ora sono solo in grado di risponderti da semplice dilettante: «Sì, mia cara Olya, sono proprio malato!»"


translate italian zhenya_route_2c220ab9:



    "E anche in questo caso, ecco nuovamente la forte risata diabolica."


translate italian zhenya_route_ba3723c7:



    "La leader del Campo era stordita, e ha mollato il telefono. È caduto sul bordo del letto ed è rimbalzato verso di me."


translate italian zhenya_route_3340d8cc:



    "Ho ripreso il cellulare e me lo sono rimesso in tasca."


translate italian zhenya_route_d9c4ae96:



    pi "Ma non sono innamorato di te."


translate italian zhenya_route_cf7d0875:



    "Ho aggiunto, aprendo un occhio."


translate italian zhenya_route_44422a26:



    "Qualcuno ha bussato timidamente alla porta, poi si è aperta e una ragazza pioniere è apparsa sulla soglia…"


translate italian zhenya_route_b5682e52:



    pi "Oh, maledizione!"


translate italian zhenya_route_275798b7:



    "Sono saltato teatralmente giù dal letto."


translate italian zhenya_route_50556e32:



    pi "E dov'è il machete? L'hai dimenticato a casa?"


translate italian zhenya_route_efe5e7d5:



    "La ragazza ha guardato la leader del Campo, confusa."


translate italian zhenya_route_71e1b5fb:



    mtp "Questo è…"


translate italian zhenya_route_b95f6ce4:



    "Ha iniziato, poi ha sospirato e si è lasciata cadere pesantemente sul letto opposto."


translate italian zhenya_route_292e2dbc:



    "In realtà sapevo che la leader del Campo non aveva paura di me, la paura è semplicemente non inclusa nel suo programma."


translate italian zhenya_route_90ba2fd2:



    "Può provare risentimento – già, di quello ne ha a volontà; può simulare paura – senza dubbio!"


translate italian zhenya_route_a3ac6997:



    "Ma basta attendere un paio di minuti e si ritirerà, lasciandomi in pace."


translate italian zhenya_route_9318d2f9:



    "Leader del Campo dei miei stivali…{w} Se fossi nei suoi panni…!"


translate italian zhenya_route_38974101:



    unp "Forse sono venuta nel momento sbagliato…"


translate italian zhenya_route_0c34c18c:



    "Ha mormorato timidamente la ragazza, fissando il pavimento e preparandosi ad andarsene."


translate italian zhenya_route_e83f62de:



    pi "Suvvia, non essere timida!"


translate italian zhenya_route_23634e10:



    "Ho esclamato in modo amichevole, facendole cenno di sedersi accanto a me."


translate italian zhenya_route_b1c5fa5a:



    pi "Siediti qui, fa' come se fossi a casa tua!"


translate italian zhenya_route_613c7491:



    unp "No, io…"


translate italian zhenya_route_aed8f0f1:



    pi "Siediti, ho detto!"


translate italian zhenya_route_825bc5ee:



    "Le ho abbaiato."


translate italian zhenya_route_bef00662:



    "Ora {i}questa{/i} ragazza sì che riesce ad esprimere una paura genuina, spavento, e persino terrore."


translate italian zhenya_route_bdc40866:



    "Si è avvicinata lentamente e si è seduta sul mio letto, aggrappandosi alla spalliera."


translate italian zhenya_route_d117b542:



    pi "Allora, cos'hai di bello da raccontarmi...?"


translate italian zhenya_route_ce617998_2:



    "…"


translate italian zhenya_route_59f848eb:



    "A me, come ad ogni altro bambino, i genitori hanno insegnato che non si dovrebbe mai aprire la porta agli sconosciuti."


translate italian zhenya_route_6b1d45d4:



    "E che non si dovrebbe andare in qualche luogo solo perché qualcuno ti ha chiesto di farlo."


translate italian zhenya_route_c713fb4d:



    "Ma questo vale solo durante l'infanzia…"


translate italian zhenya_route_d74f573a:



    "E ora ogni cosa dipende letteralmente dal fatto che io mi fidi o no di un estraneo!"


translate italian zhenya_route_2385d8a9:



    "Sembra quasi che una vita intera sia già trascorsa, e che io abbia già avuto il tempo di invecchiare, se non fisicamente, allora mentalmente."


translate italian zhenya_route_28349501:



    "E non ricordo più cosa significhi inverno, la neve, il freddo, l'oscurità notturna, il soffio del vento che ti fa rabbrividire fino all'osso, e l'eterna fanghiglia."


translate italian zhenya_route_1e4f4c53:



    "Lo stesso discorso vale per la città, che non si è fermata per un solo minuto, sempre di corsa verso qualcosa, disinfettando il cielo sporco e le persone con i suoi tratti di camini marci e scarichi delle automobili. Tutto ciò è rimasto in un passato ormai lontano, come le pagine sbiadite di un vecchio album fotografico che è stato gettato in un magazzino come cianfrusaglia."


translate italian zhenya_route_fe1f361c:



    mz "Ehi!"


translate italian zhenya_route_b0b68304:



    "La voce della ragazza seduta accanto a me mi ha distratto da quei pensieri."


translate italian zhenya_route_5cbff5c8:



    mz "Ti sei inceppato di nuovo? Stai ancora pensando a {i}quello{/i}?"


translate italian zhenya_route_c233b6a2:



    me "Pensando a cosa?"


translate italian zhenya_route_b00e703f:



    "Me l'ha chiesto senza intenzione di offendermi, e io le ho chiesto conferma solo per il gusto di farlo – conoscevo già la risposta."


translate italian zhenya_route_a2aa1362:



    mz "Lo sai."


translate italian zhenya_route_1032aca8:



    "Stavamo seduti sul tetto della biblioteca, con le gambe a penzoloni sul bordo."


translate italian zhenya_route_95ebb22e:



    "Zhenya si è reclinata un po', con quei suoi capelli che fluttuavano magicamente nel vento."


translate italian zhenya_route_541d0415:



    "Il sole risplendeva nei nostri occhi, rendendoci strabici. Ma non era accecante, era più come un caldo velluto, e sembrava avvolgere l'intero corpo nel piacevole calore di una giornata soleggiata."


translate italian zhenya_route_76bb3a52:



    me "No…{w} Voglio dire, non lo so. Credo.{w} Ultimamente non riesco sempre a dire quello che penso. C'è solo una poltiglia di pensieri nella mia testa."


translate italian zhenya_route_d47cc9b7:



    "Mi sono fermato per un istante e ho guardato Zhenya."


translate italian zhenya_route_62828675:



    "Sembrava che fosse disinteressata non solo alla nostra conversazione, ma a tutto ciò che accadeva attorno a lei in generale."


translate italian zhenya_route_75c0b8b5:



    "Come se questa ragazza fosse in pace con tutto – con l'estate, col sole, col vento piacevole, con la nostra gioventù…"


translate italian zhenya_route_344c7b57:



    "Come se questo le bastasse per essere felice, e tutto il resto fosse solo una sciocca banalità."


translate italian zhenya_route_b71363c7:



    me "In poche parole, sai cosa intendo! Perché devi chiedermelo?"


translate italian zhenya_route_7e36ced4:



    mz "Perché un essere umano è una creatura socievole, si suppone che comunichi con i suoi simili."


translate italian zhenya_route_9551e44d:



    "Ha ghignato quasi impercettibilmente."


translate italian zhenya_route_c7fab65f:



    me "Sì, giusto, senti chi parla! Aspetta qui, vado a prendere il premio per il «Pioniere più Comunicativo dell'anno mille-novecento-ottanta-qualcosa»."


translate italian zhenya_route_0754d52f:



    mz "Che dire di me? Io, a differenza di alcune persone, ho delle responsabilità ben precise in questo Campo! Sono un membro rispettato del nostro collettivo, si potrebbe persino dire insostituibile, in un certo senso! A differenza di te!"


translate italian zhenya_route_440c2112:



    "Gli occhi di Zhenya hanno scintillato, mentre lei sorrideva beffardamente."


translate italian zhenya_route_1f5dde14:



    me "Responsabilità, sì, certo…"


translate italian zhenya_route_1dcd62cf:



    "Mi sono coperto gli occhi con il palmo della mano e mi sono reclinato all'indietro."


translate italian zhenya_route_f3cb88e5:



    me "Scherzi a parte, riesci a sentire almeno quello che dici? Responsabilità! Collettivo…"


translate italian zhenya_route_fe1f361c_1:



    mz "Ehi!"


translate italian zhenya_route_8118435a:



    "Mi ha detto severamente Zhenya, colpendomi sulla pancia."


translate italian zhenya_route_ea196633:



    me "Aahh."


translate italian zhenya_route_09cd5cef:



    "Ho gemuto per lo più teatralmente e le ho lanciato uno sguardo adirato."


translate italian zhenya_route_f115c4df:



    me "E se fossi caduto giù di sotto? E se mi fossi ferito? Eh?"


translate italian zhenya_route_9da7dc7e:



    mz "Sarebbe stato estremamente spiacevole!"


translate italian zhenya_route_1a6118f5:



    me "In questo luogo non si può nemmeno avere un'assicurazione per la vita!"


translate italian zhenya_route_91ca94c2:



    mz "Già, e non ci sarebbe nessuno che potrebbe pagarti il rimborso."


translate italian zhenya_route_25d2cb1e:



    me "Beh, nessuno…"


translate italian zhenya_route_d2c7a5fb:



    "Mi sono strofinato la nuca, ho sorriso sornione e le ho detto:"


translate italian zhenya_route_1a377d44:



    me "E tu?"


translate italian zhenya_route_cb595887:



    mz "Io…"


translate italian zhenya_route_4f246bc0:



    "Zhenya ha pensato per un secondo."


translate italian zhenya_route_192326c0:



    mz "Be', forse sì, anche se non credo che la mensa accetti i dollari, gli euro, le carte di credito, o quantomeno i rubli russi. E probabilmente non c'è alcun cambio di valuta qui. E anche se ci fosse, chissà come sta andando il tasso di cambio. Non voglio soffrire l'inflazione, sai!"


translate italian zhenya_route_e6444b06:



    me "Allora chiedi che ti paghino in oro!"


translate italian zhenya_route_c4101076:



    mz "L'oro, col cavolo!"


translate italian zhenya_route_852f8481:



    "Si è gettata su di me, cercando di farmi il solletico, o di stringermi, o di picchiarmi, o tutto insieme."


translate italian zhenya_route_ce617998_3:



    "…"


translate italian zhenya_route_197ea717:



    "Ho abbandonato la casetta della leader del Campo ridacchiando."


translate italian zhenya_route_dcebf79c:



    "Probabilmente non mi sarei dovuto aspettare qualcosa dalla conversazione con quella ragazza pioniere."


translate italian zhenya_route_6077816f:



    "E non l'ho fatto, perché mai avrei dovuto?"


translate italian zhenya_route_92d37e44:



    "Ormai è tutto noto da tempo – sono in grado di recitare quella scena da solo, come un miserabile One Man Show."


translate italian zhenya_route_c1f15886:



    "Che ironia – la recita di un singolo attore, in un teatro con un solo spettatore, in una città con un solo cittadino, in un universo con un solo essere vivente?"


translate italian zhenya_route_851ebb7b:


    "O forse \"vivente\", tra virgolette? O è meglio {i}vivente{/i}, in corsivo? Che tipo di necrologio preferiresti? Con volute o senza? Con maiuscole o senza? Che tipo di carattere vorresti? La carta dev'essere lucida o opaca?"


translate italian zhenya_route_7e9e0418:



    "Argh, ci risiamo!"


translate italian zhenya_route_f619af25:



    "Una piccola e allegra ragazzina stava salterellando lungo il sentiero."


translate italian zhenya_route_023d8bad:



    "Forse questa volta dovrei iniziare con lei?"


translate italian zhenya_route_77521125:



    "Ricordo che nella mensa ci sono degli splendidi ganci per carni…"


translate italian zhenya_route_c3e020a3:



    "O forse non dovrei...?"


translate italian zhenya_route_4e76737d:



    usp "E tu chi sei?"


translate italian zhenya_route_244be5e6:



    "Ha sbottato, venendomi appresso."


translate italian zhenya_route_90ff5d0b:



    pi "Sono il terrore che si libra nella notte! Buu!"


translate italian zhenya_route_6f0e525a:



    "Ho allargato le braccia in modo teatrale, incombendo su di lei."


translate italian zhenya_route_6c1f8778:



    usp "Non fa per niente paura."


translate italian zhenya_route_3d43d484:



    pi "Non fa paura? Beh, forse per ora…"


translate italian zhenya_route_8d8338a5:



    "Ho ghignato e ho ripreso a camminare."


translate italian zhenya_route_e10b6d74:



    usp "Ehi!"


translate italian zhenya_route_e8057229:



    "Il suo grido mi ha seguito."


translate italian zhenya_route_ce6c5674:



    usp "Aspetta."


translate italian zhenya_route_a3ba21f2:



    "La ragazzina mi ha raggiunto e mi ha preso per un braccio."


translate italian zhenya_route_a593fff2:



    pi "Non mi toccare!"


translate italian zhenya_route_842697d6:



    "Ho gridato disgustato, e con uno strattone ho scaraventato la giovane pioniera sul bordo della stradina, facendola cadere in mezzo ai cespugli."


translate italian zhenya_route_911de195:



    usp "Ahi, che male!"


translate italian zhenya_route_b7ea271a:



    "Ha cominciato a piangere."


translate italian zhenya_route_59a139cf:



    "Strano.{w} Questo sì che è {i}davvero{/i} strano."


translate italian zhenya_route_8e30c7d5:



    "Non credo che una cosa del genere sia mai accaduta prima d'ora."


translate italian zhenya_route_d16c2c64:



    "Naturalmente, questa giornata si è ripetuta così tante volte che ormai è impossibile contarle..."


translate italian zhenya_route_fe8f03bf:



    "E non intendo il fatto che la ragazzina sia volata nei cespugli, ma piuttosto non dovrebbe esserci {b}proprio nulla{/b} di nuovo!"


translate italian zhenya_route_a884f083:



    "Persino un'occorrenza così insignificante, come la si potrebbe considerare, alla fine potrebbe rivelarsi più importante di tutte le {i}ripetizioni{/i} precedenti!{w} Persino della {i}prima{/i} volta!"


translate italian zhenya_route_0abd66bb:



    "Non so perché, ma sento che è semplicemente trascorso troppo tempo da quando {i}qualcosa{/i} di nuovo fosse accaduto!"


translate italian zhenya_route_57abc691:



    "Ovviamente, questo mondo non è in alcun modo considerabile come un monotono meccanismo che svolge il proprio lavoro in modo inanimato, ma conosco già tutte le possibili varianti della trama!"


translate italian zhenya_route_d9f0d80c:



    "E quando dico {i}tutte{/i}, intendo dire proprio {b}tutte{/b}!"


translate italian zhenya_route_03df3bc1:



    "Pochi minuti fa alla casetta della leader del Campo sarebbe potuta venire la ragazza che avevo incontrato al cancello, piuttosto che quell'altra timida ragazza – e io sarei stato pronto in ogni caso."


translate italian zhenya_route_00375a72:



    "O la ragazza arrogante che accoglie i nuovi arrivati con un colpo sulla schiena – e sarei stato pronto."


translate italian zhenya_route_68cb4caf:



    "O anche questa piccola piagnucolona – e sarei stato pronto comunque…"


translate italian zhenya_route_6c0f5a1e:



    "Ma ora, in questo momento, sta succedendo qualcosa per cui non mi sento pronto!"


translate italian zhenya_route_7a13a065:



    "E soprattutto non sono pronto per le cose inaspettate!"


translate italian zhenya_route_3d9d35bf:



    "La ragazzina continuava a piangere, guardandomi con gli occhi pieni di rabbia e risentimento."


translate italian zhenya_route_7083b974:



    usp "Tu! È tutta colpa tua!"


translate italian zhenya_route_faf5a9ef:



    pi "E dai, che motivo c'è di piangere…"


translate italian zhenya_route_47520c7a:



    "Mi sono avvicinato a lei esitando e le ho porto la mano."


translate italian zhenya_route_87036d00:



    "Per un secondo la ragazzina mi ha guardato in modo perplesso, ma alla fine ha afferrato la mia mano e l'ho tirata su con uno scatto."


translate italian zhenya_route_4106821b:



    usp "Ti sei sentito in colpa, vero? È questo il punto – non puoi fare il bullo con i più piccoli!"


translate italian zhenya_route_cd7eeef3:



    pi "In colpa?"


translate italian zhenya_route_14af5cda:



    "Ho ripetuto, con il mio pensiero che vagava in lontananza."


translate italian zhenya_route_f699ca55:



    pi "Perché? A causa tua? Perché dovrei?"


translate italian zhenya_route_fbb480b9:



    usp "Ma dovresti!"


translate italian zhenya_route_07964120:



    "Ha aggrottato la fronte ed è corsa verso la casetta della leader del Campo."


translate italian zhenya_route_6082bc94:



    "A lamentarsi, credo – non so."


translate italian zhenya_route_312c4c2e:



    "Davvero non lo so!"


translate italian zhenya_route_c0b16db9:



    "Per la prima volta dopo tanto ma tanto tempo, è accaduto qualcosa del genere!"


translate italian zhenya_route_a12e16eb:



    "Solo un paio di minuti fa avrei potuto dire con certezza cosa stesse facendo ogni singolo pioniere in quell'esatto momento.{w} E adesso invece?"


translate italian zhenya_route_c8849df7:



    "Accidenti!"


translate italian zhenya_route_ce617998_4:



    "…"


translate italian zhenya_route_6e093196:



    "A cena la leader del Campo si è avvicinata a me."


translate italian zhenya_route_8fd2dc1f:



    mtp "È stato davvero sgraziato…"


translate italian zhenya_route_3dcba555:



    "Ha esordito, come se stesse raccogliendo i propri pensieri."


translate italian zhenya_route_9b4a0f57:



    mtp "I tuoi genitori hanno chiamato, e…"


translate italian zhenya_route_56a89359:



    mtp "Ma ti saresti dovuto comportare più decentemente! Almeno presentandoti come si deve!"


translate italian zhenya_route_8aa18321:



    "Mi ha detto, e la sua espressione si è tramutata."


translate italian zhenya_route_61a3b78e:



    mtp "Ma non fa niente, ti trasformeremo in un pioniere modello."


translate italian zhenya_route_051e0836:



    "In altre circostanze le avrei detto in dettaglio cosa ne penso riguardo ai pionieri e alla loro modellazione, ma adesso non ho proprio alcuna voglia di parlare – né con lei, né con nessun altro."


translate italian zhenya_route_d6438854:



    mtp "Bene allora. Tanto per cominciare dovresti fare conoscenza coi tuoi compagni."


translate italian zhenya_route_22da79ff:



    "La leader del Campo ha agitato la mano, invitando qualcuno a sedersi al mio tavolo, e due ragazze si sono avvicinate a noi."


translate italian zhenya_route_f05e1e1c:



    mtp "Ebbene, godetevi il pasto, io ho altre cose da fare."


translate italian zhenya_route_7ca779be:



    "Ha detto incerta, lasciando le due ragazze da sole con me."


translate italian zhenya_route_430dffff:



    mip "Ciao, sei nuovo qui, vero? Ti piace la musica? Iscriviti al nostro circolo musicale! Ci sono solo io adesso, ma con te saremo in due, sarà divertente, non credi?"


translate italian zhenya_route_96dfe183:



    "L'ho ignorata, continuando a mangiare intensamente."


translate italian zhenya_route_001db536:



    mzp "Lascialo stare – non vedi che è uno senza cervello?"


translate italian zhenya_route_e662491f:



    "Se la prima ragazza era semplicemente stupida, la seconda mi sembrava come quel Grumpy Cat di internet."


translate italian zhenya_route_e94109d2:



    "Hmm, e se provassi a...?"


translate italian zhenya_route_8fcc767a:



    "In effetti non l'ho mai fatto prima – un tale pensiero non mi è mai passato per la testa."


translate italian zhenya_route_33bd02a0:



    "Beh, se adesso non conosco più gli eventi futuri, allora perché non fare un tentativo?"


translate italian zhenya_route_e95a8512:



    "Mi sono alzato silenziosamente, ho preso il bicchiere di kompot e l'ho versato sulla testa della seconda ragazza pioniere."


translate italian zhenya_route_264353ba:



    pi "Buon appetito."


translate italian zhenya_route_6e8444c6:



    "Ho riassunto quell'atto di vandalismo con una voce di ghiaccio, e mi sono diretto verso l'uscita della mensa."


translate italian zhenya_route_1741b8aa:



    "La ragazza ha gridato, ha strillato, ha ansimato e ha gemuto, urlando qualcosa con voce rauca, ma non ha cercato di fermarmi."


translate italian zhenya_route_58fe7b4e:



    "In quel momento ho desiderato trovarmi in qualche altro luogo, guardare il mondo dall'alto, osservare la sofferenza degli altri dalla sommità della mia esperienza."


translate italian zhenya_route_d425dea5:



    "Ma a partire da oggi quei luoghi non esistono più."


translate italian zhenya_route_d58c2a5b:



    "All'uscita mi sono imbattuto nuovamente nella prima ragazza che avevo incontrato al cancello. Ho cercato di sorpassarla, ma…"


translate italian zhenya_route_16321643:



    "Mi ha dato un ceffone."


translate italian zhenya_route_56937f22:



    "Il suono si è immediatamente propagato all'interno della mensa, e il suo eco è rimbalzato sul soffitto."


translate italian zhenya_route_ccd4b759:



    "E nel giro di un istante è calato un silenzio assoluto."


translate italian zhenya_route_7d790b8b:



    "In meno di un secondo la mia mano si è stretta in un pugno, con le unghie che mi infilzavano il palmo, con la pelle sulle mie nocche che si estendeva quasi fino al punto di rottura…"


translate italian zhenya_route_f644afb1:



    "Stavo per picchiare selvaggiamente quella cagna…"


translate italian zhenya_route_d8822251:



    "Forse non ce l'ho fatta per una sola frazione di secondo – in mezzo a noi si è intromessa la piccola pioniera che avevo gettato tra i cespugli prima, e ora se ne stava davanti a me a braccia larghe, proteggendo la sua amica con il suo esile corpicino."


translate italian zhenya_route_3dead8bb:



    "E la cosa è finita lì…"


translate italian zhenya_route_8ce3cecf:



    "Ho smesso di stringere il pugno, la mia forza di volontà mi ha abbandonato, ho abbassato la testa, le ho oltrepassate e ho abbandonato la mensa."


translate italian zhenya_route_6d636a33:



    "Sono davvero riuscite ad essere più forti di me?"


translate italian zhenya_route_b0c195bf:



    "Com'è possibile che una cosa del genere sia potuta succedere?{w} In passato avrei…"


translate italian zhenya_route_125b75d4:



    "Ma perché proprio «avrei»? Niente mi ha impedito di farlo, così tante volte."


translate italian zhenya_route_74647d36:



    "Colpire una ragazza è una cosa da poco!{w} Ho fatto cose ben peggiori."


translate italian zhenya_route_e33546f5:



    "Sembra che questo mondo sia cambiato senza essersi preso la briga di informarmi!"


translate italian zhenya_route_ce617998_5:



    "…"


translate italian zhenya_route_e85ecdd6:



    "Gli anelli di fumo della mia sigaretta si stavano dirigendo lentamente verso Genda."


translate italian zhenya_route_3865b2a1:



    "Il pacchetto è quasi finito, e non c'è nessun posto dove potrei trovarne altre, quindi dovrò aspettare una settimana intera…"


translate italian zhenya_route_68877b02:



    "Ovviamente potrei andare dai fratelli-autistici, prendere la bottiglia di vodka e ubriacarmi – ma cosa cambierebbe?"


translate italian zhenya_route_632060af:



    "Decisioni, così complicate adesso e cosi semplici prima…"


translate italian zhenya_route_514f9a11:



    "Argh, ho già iniziato a pensare come quel maledetto fallito?!{w} No, mai!"


translate italian zhenya_route_8236fc12:



    dvp "Ce ne hai una?"


translate italian zhenya_route_96e9ab02:



    "Un'altra ragazza pioniere si è seduta accanto a me."


translate italian zhenya_route_3b3e0f18:



    pi "Che cosa?"


translate italian zhenya_route_b7ca5301:



    "Ho chiesto senza capire."


translate italian zhenya_route_eb197aa4:



    "Ha indicato la sigaretta."


translate italian zhenya_route_b5c90a40:



    pi "Il fumo fa male alla salute!"


translate italian zhenya_route_a96c9711:



    dvp "Senti chi parla."


translate italian zhenya_route_473574eb:



    "Ha sorriso."


translate italian zhenya_route_b861b089:



    pi "La mia è di gomma. Vuoi provarla?"


translate italian zhenya_route_3c4c2708:



    dvp "Non ce n'è bisogno, ti credo sulla parola."


translate italian zhenya_route_698cea6f:



    "Le ho dato una sigaretta e gliel'ho accesa."


translate italian zhenya_route_b7315f93:



    dvp "Hai fatto qualcosa di forte oggi… beh…"


translate italian zhenya_route_62f34328:



    "Mi ha detto, dopo aver smesso di tossire."


translate italian zhenya_route_0adf4ebc:



    dvp "E anche nella capanna della leader del Campo – ho sentito. E a cena. Almeno adesso non sono più io la peggiore vergogna del Campo!"


translate italian zhenya_route_f427a0c4:



    "L'ho guardata con attenzione."


translate italian zhenya_route_0cc3480e:



    "La pioniera stava fumando la sigaretta scioccamente, sorridendo in modo compiaciuto, come se l'intero mondo le avesse dato già ragione."


translate italian zhenya_route_01e0ab71:



    pi "E tu, a quanto pare, pensi solo a te stessa."


translate italian zhenya_route_8e823649:



    dvp "Che cosa? Di che stai parlando? Io… no…"


translate italian zhenya_route_1f8ac785:



    "La sua espressione si è tramutata, è persino andata un po' in panico, soffocando a causa del fumo."


translate italian zhenya_route_126ad096:



    pi "Ma hai ragione. Non sei mai stata la peggiore qui. E per quanto riguarda me, non ne sono sicuro."


translate italian zhenya_route_32eb32a4:



    dvp "Stai parlando… Così… Beh… In modo ganzo!"


translate italian zhenya_route_df5a5407:



    "Ora, una lode da parte sua era certamente quello di cui avevo bisogno."


translate italian zhenya_route_ed866e5b:



    "O forse...?"


translate italian zhenya_route_ecd96042:



    pi "Senti…"


translate italian zhenya_route_c1b91905:



    "Ho iniziato, sfoggiando uno dei miei sorrisi studiati."


translate italian zhenya_route_3ed5b05c:



    pi "Già che ci siamo, che ne dici di un drink, per così dire?"


translate italian zhenya_route_48a95115:



    dvp "Hai qualcosa?"


translate italian zhenya_route_63d5e899:



    "Gli occhi della pioniera hanno brillato."


translate italian zhenya_route_f90a9790:



    pi "Posso sempre trovare qualcosa – per una buona compagnia!"


translate italian zhenya_route_ce617998_6:



    "…"


translate italian zhenya_route_b4f43180:



    "Anche se tutto è iniziato come in un brutto romanzo che viene stampato capitolo per capitolo su un giornale di strada – alla fine ha preso una piega totalmente inaspettata."


translate italian zhenya_route_217e4924:



    "I ricordi si sono mescolati insieme in una nebbia perenne di una tinta rosso-arancione, come una strada di piastrelle gialle che conduce alla Città di Smeraldo."


translate italian zhenya_route_70fa3abc:



    "Anche se Dorothy adesso è cresciuta un po' e si guadagna da vivere facendo la prostituta, perché invece di studiare diligentemente era sempre persa nei suoi sogni di un mondo incantato."


translate italian zhenya_route_e2cae742:



    "Il Leone Codardo è caduto nella dipendenza da metanfetamine, ed è stato ucciso in uno scontro a fuoco con la polizia."


translate italian zhenya_route_94326c75:



    "Invece l'Uomo di Latta ha avuto una vita abbastanza buona – è diventato un killer professionista."


translate italian zhenya_route_1ffdfc71:



    "E lo Spaventapasseri non è cambiato per niente, gli è stata assegnata una stalla, un lavoro a bassa retribuzione in un ufficio con comodi orari di lavoro dalle nove alle sei e l'assenza di opportunità di carriera."


translate italian zhenya_route_8549b557:



    "Probabilmente non c'era alcuna città incantata all'orizzonte, in primo luogo."


translate italian zhenya_route_cddcc89f:



    "Una strada senza fine, senza meta né significato."


translate italian zhenya_route_c0fbc843:



    "Sai, a quei tempi {i}lui{/i} mi parlava davvero di cose del genere, ha cercato di mettermi in guardia, almeno. Ma io non l'ho ascoltato, e alla fine sono rimasto l'unico ad essere bloccato in questo maledetto nastro di Möbius."


translate italian zhenya_route_03818c7f:



    "Lei stava seduta accanto a me, strizzando gli occhi con malcontento."


translate italian zhenya_route_488f7b42:



    mz "Ehi! Pronto, pronto, la Terra sta chiamando!"


translate italian zhenya_route_226ec868:



    me "Ok, che c'è di sbagliato? Come se non potessi prendermi un momento per pensare!"


translate italian zhenya_route_509526cd:



    mz "I pensieri dovrebbero portare ad un risultato, e tu invece cos'hai ottenuto da essi?"


translate italian zhenya_route_7062bae2:



    me "Ebbene, che cosa ho ottenuto?"


translate italian zhenya_route_b1e74574:



    mz "L'assenza di risultati!"


translate italian zhenya_route_9524b395:



    me "L'assenza di risultati è comunque un risultato!"


translate italian zhenya_route_83479432:



    "Zhenya mi ha pizzicato dolorosamente sulla spalla e si è allontanata di un metro."


translate italian zhenya_route_ce617998_7:



    "…"


translate italian zhenya_route_67b69ec7:



    "E poi lei è apparsa."


translate italian zhenya_route_274f7aad:



    "No, Zhenya… le Zhenye ci sono sempre state al «Sovyonok», ma in quel momento ho visto la mia Zhenya per la prima volta."


translate italian zhenya_route_23e4b0e5:



    "Forse non ricordo bene cosa fosse successo prima e ciò che accadde in seguito, ma quei giorni rimarranno per sempre nei miei ricordi!"


translate italian zhenya_route_ce617998_8:



    "…"


translate italian zhenya_route_4f00cb19:



    "Ormai mi sono già reso conto che questo chiaramente non è il mio mondo congelato, ho capito che dopo un'eternità trascorsa qui sono riuscito ad entrare nuovamente in un altro di questi mondi, come ho già fatto una volta in passato."


translate italian zhenya_route_8ac15cc8:



    "Ci sono riuscito per puro caso – come se per un momento le orbite di due pianeti si fossero intersecate e io avessi avuto solo un istante di tempo per agire."


translate italian zhenya_route_27a7f3cc:



    "Se l'avessi fatto anche solo un secondo più tardi, loro sarebbero volate via, allontanandosi sempre più, con velocità stratosferica."


translate italian zhenya_route_71441e64:



    "Questo mondo è solitario e vuoto proprio come quello precedente."


translate italian zhenya_route_3c572706:



    "Ma la cosa principale è che in esso tutto è diverso!"


translate italian zhenya_route_16cc93e0:



    "E qui ci sono delle cose a me poco familiari."


translate italian zhenya_route_b518e850:



    "Oh, e ciò è davvero importante, è così importante per me!"


translate italian zhenya_route_b1f78b5b:



    "Per esempio Zhenya che non sta seduta nella biblioteca, ma sulla veranda della mensa."


translate italian zhenya_route_56aa7a6d:



    "Dopo tutto quello che è successo non sono più in grado di essere sarcastico."


translate italian zhenya_route_c8d5ca9e:



    pi "Ciao."


translate italian zhenya_route_1364bea0:



    "Le ho detto mentre mi avvicinavo a lei, ma fermandomi comunque a una distanza di sicurezza."


translate italian zhenya_route_0df38c29:



    "Zhenya ha alzato lo sguardo verso di me. La sua espressione ha lasciato a intendere che non si aspettasse di incontrare qualcuno qui."


translate italian zhenya_route_7c7aa84b:



    mzp "Ciao…"


translate italian zhenya_route_e68a1186:



    "E la sua voce non sembrava la solita – non che io l'avessi confusa con quella di qualcun altro!"


translate italian zhenya_route_63994ee4:



    me "Hai deciso di venire a cena in anticipo?"


translate italian zhenya_route_065c7777:



    mzp "No… Non lo so… Quand'è la cena?"


translate italian zhenya_route_37c6ffc1:



    "Ha farfugliato."


translate italian zhenya_route_cfdaa1d8:



    me "Cosa intendi dire con «quand'è la cena»?"


translate italian zhenya_route_9e2a8fe8:



    "La sua risposta mi ha lasciato in uno stato di torpore e ho sorriso, anche se un brivido mi ha attraversato l'intero corpo."


translate italian zhenya_route_b1f8319d:



    mzp "Ah, no, scusa, certo che so quando è la cena, è solo che… Beh…"


translate italian zhenya_route_a334b9dd:



    "Zhenya è rimasta completamente confusa e ha chinato il capo fissando i propri piedi."


translate italian zhenya_route_4f43b195:



    "Sembrava che stare lì fosse l'ultima cosa che lei volesse, ma che non avesse abbastanza forza di volontà per andarsene."


translate italian zhenya_route_490c71fc:



    "Mi è sembrato strano, ma non mi ha turbato affatto – al contrario, ero felice perché qualcosa di nuovo era accaduto, al di là del raggio d'azione del solito schema."


translate italian zhenya_route_9933d8d3:



    pi "Stai bene?"


translate italian zhenya_route_396cd3e5:



    "Ho fatto la più stupida delle domande che si potrebbe fare in una tale situazione."


translate italian zhenya_route_abee5180:



    "E cosa mi aspettavo di sentire come risposta?"


translate italian zhenya_route_12f3639c:



    mz "Sì, grazie."


translate italian zhenya_route_9508d41b:



    me "Per cosa?"


translate italian zhenya_route_be9c5dd6:



    mz "Non lo so. Beh, per avermelo chiesto."


translate italian zhenya_route_5242e190:



    me "Sì, io ho solo…"


translate italian zhenya_route_f50a4f7c:



    "Una debole sfumatura di malcontento è apparsa sul suo viso, ma subito è scomparsa, e Zhenya si è alzata lentamente."


translate italian zhenya_route_8a961b45:



    mz "Devo andare."


translate italian zhenya_route_35daecd9:



    me "Aspetta, ma non stavi andando alla mensa…?"


translate italian zhenya_route_6e2024d8:



    mz "Verrò più tardi, ho altre cose da fare."


translate italian zhenya_route_505d91ae:



    me "Va bene…"


translate italian zhenya_route_2a4f9fde:



    "Non l'ho fermata, ma ho deciso di seguirla."


translate italian zhenya_route_44b86c7f:



    "Dio solo sa quanto tempo è passato dall'ultima volta che mi è accaduto qualcosa di simile!"


translate italian zhenya_route_850d89b8:



    "Forse è successo solo durante le mie prime volte qui…"


translate italian zhenya_route_92e3d697:



    "Zhenya è andata alla piazza e si è recata in fretta verso la biblioteca."


translate italian zhenya_route_f6928ff9:



    "Non si è mai guardata attorno durante il tragitto, e quindi non ho avuto la necessità di nascondermi bene."


translate italian zhenya_route_38cda274:



    "Dopo che la porta si è chiusa alle sue spalle sono rimasto immobile, esitando."


translate italian zhenya_route_7bb3941c:



    "Allora, adesso che faccio? Dovrei bussare e entrare? Ma cosa le potrei dire?"


translate italian zhenya_route_ddf58ea4:



    "Non era necessario essere un investigatore per capire che la stavo seguendo."


translate italian zhenya_route_f287617d:



    "Electronik, che è apparso dal nulla davanti a me, mi ha fatto riemergere dai miei pensieri."


translate italian zhenya_route_3fa63434:



    elp "Che c'è, sei venuto con la lista?"


translate italian zhenya_route_8e512950:



    pi "Eh? Sì, ho appena deciso di far visita alla biblioteca."


translate italian zhenya_route_d848bc68:



    elp "Capisco, non ti disturbo allora."


translate italian zhenya_route_10ebed14:



    "Mi ha detto, e ha fatto per andarsene."


translate italian zhenya_route_b0b2d0e2:



    pi "Aspetta.{w} Non pensi che Zhenya ultimamente, beh… si comporti in modo strano, immagino?"


translate italian zhenya_route_257fa208:



    "Electronik mi ha guardato attentamente, aggrottando la fronte."


translate italian zhenya_route_dcb8b7df:



    elp "È successo qualcosa?"


translate italian zhenya_route_ac5548d9:



    me "No, niente…"


translate italian zhenya_route_0ba7fa61:



    "Era come se stessi cercando delle scuse, e una sensazione di meschinità personale mi ha avvolto – perché mai dovrei dare spiegazioni a lui?!"


translate italian zhenya_route_1c8d2b4a:



    pi "Sto solo chiedendo! Che c'è, non posso nemmeno chiedere?!"


translate italian zhenya_route_e121c03e:



    elp "No, certo che puoi…"


translate italian zhenya_route_12d357d2:



    "Ha detto nervosamente."


translate italian zhenya_route_9c4cad90:



    elp "Forse in realtà lei è diventata un po' strana.{w} Mi sembra di averlo notato negli ultimi giorni."


translate italian zhenya_route_d889c13c:



    pi "Davvero? E come si è manifestata questa stranezza?"


translate italian zhenya_route_40ac813e:



    elp "Non ne sono sicuro, lei di solito è più sicura di sé, o qualcosa del genere. Pensi che dovremmo parlare con lei?"


translate italian zhenya_route_c090949c:



    "Stavo per dirgli «tu no di certo», ma mi sono trattenuto."


translate italian zhenya_route_49ac3d86:



    pi "Va bene, lasciamo perdere. Forse si trova proprio in quello stato d'animo."


translate italian zhenya_route_094c845e:



    elp "Quale stato d'animo?"


translate italian zhenya_route_d6ae043c:



    pi "Nessuno stato d'animo! {i}Quello{/i} stato – perché non provi a indovinare, non sei forse uno dei futuri scienziati?"


translate italian zhenya_route_fd51a2f5:



    "Ho risposto bruscamente."


translate italian zhenya_route_e1d27f94:



    "Electronik ha pensato per un momento – come se stesse meditando riguardo al destino del mondo – e poi ha annuito indulgente."


translate italian zhenya_route_8734c583:



    elp "Va bene."


translate italian zhenya_route_c2c38b1e:



    "Ho allargato un po' le braccia e ho abbassato la testa, indicando chiaramente che non c'era alcun ulteriore motivo per lui di rimanere lì, e che sarebbe stato meglio tornarsene a costruire i robot, e farlo il prima possibile."


translate italian zhenya_route_809c2d88:



    "Certo, era un po' ottuso, ma questa volta mi ha capito al volo."


translate italian zhenya_route_5c848e46:



    "Dopo che Electronik se n'è andato, ho appoggiato la schiena ad un albero nelle vicinanze, in modo che non mi si potesse vedere dalle finestre, e ho iniziato a pensare."


translate italian zhenya_route_71cfb74b:



    "Per la precisione, ho cercato di pensare, perché non c'erano abbastanza fatti per un'analisi completa della situazione."


translate italian zhenya_route_1627ebc3:



    "Quando la carta da parati nella tua stanza cambia dal bianco al nero puoi sentirti sorpreso, impaurito, puoi supporre che sia stato tutto ridipinto senza che tu lo sapessi, e arrabbiarti con la persona che l'ha fatto…"


translate italian zhenya_route_26e9bd8b:



    "Ma quelle sono tutte emozioni."


translate italian zhenya_route_14f931ad:



    "Un fatto concreto in quella situazione sarebbe una nota scritta lasciata sul tavolo, che ti possa comunicare «Mio caro, ho pensato che sarebbe stato meglio per te prepararti per l'eternità in anticipo, e così ho cambiato la tinta della tua stanza in una più adatta»."


translate italian zhenya_route_90a606a4:



    "E ciò che avevo tra le mani in quel momento era: dietro di me – innumerevoli cicli; adesso – lo strano comportamento della bibliotecaria; e davanti a me – l'incertezza."


translate italian zhenya_route_8b53b821:



    "Non esattamente una solida base per formulare teorie di alcun tipo."


translate italian zhenya_route_bf4b9a89:



    "Oh, e io ero il maestro delle teorie, a dire il vero! Anche se faceva parte del passato, perché ad un certo punto avevo smesso di preoccuparmi del modo esatto in cui fossi destinato a trascorrere l'eternità in questo Campo."


translate italian zhenya_route_0bf3ea31:



    "Argh, di nuovo!"


translate italian zhenya_route_f04073f4:



    "E ogni cosa io decidessi, non era proprio da me!"


translate italian zhenya_route_dc78bf83:



    "Non la penso in quel modo, e probabilmente non l'ho mai fatto."


translate italian zhenya_route_21ad4eef:



    "È come se qualcuno avesse infilato delle parole straniere nella mia testa. Forse ho smesso di essere me stesso quando sono arrivato in un mondo alieno?"


translate italian zhenya_route_d0ea6aef:



    "L'erba ha frusciato a tradimento sotto ai miei piedi, facendo notare la mia presenza. Mi stavo avvicinando furtivamente alla biblioteca, cercando di scoprire cosa stesse accadendo all'interno di essa."


translate italian zhenya_route_56b6bff2:



    "Zhenya stava seduta ad un tavolo, fissando qualcosa davanti a sé."


translate italian zhenya_route_2a36c9b6:



    "Non aveva neppure un libro tra le mani – un pensiero mi è balenato in testa."


translate italian zhenya_route_2e2c0707:



    "Lei sembrava sconcertata, forse segnata, come qualcuno a cui è stato appena detto che restano solo poche ore di vita."


translate italian zhenya_route_928e244d:



    "Vorrei solo poterle dire che tutto andrà bene e che tra cinque giorni tutto si ripeterà, e poi ancora e ancora…"


translate italian zhenya_route_2ebd4d06:



    "Ma ciò come potrebbe essere considerato un {i}bene{/i}?"


translate italian zhenya_route_8b397834:



    "Sarebbe come dire a un paziente che morirà entro cinque giorni, ma che non deve preoccuparsi di ciò poiché sette giorni più tardi morirà nuovamente!"


translate italian zhenya_route_443724fc:



    "Sono già passati dieci minuti, ma Zhenya non si è ancora mossa. In certi momenti mi è sembrato che non stesse nemmeno respirando."


translate italian zhenya_route_416886f8:



    "Ora ho iniziato a rendermi conto con certezza che questo non è semplicemente strano, questo è {i}proprio quello{/i}! Ciò che mi ha scaraventato in questo nuovo mondo!"


translate italian zhenya_route_48b5e529:



    "Dovevo solo decidere in che modo iniziare."


translate italian zhenya_route_f942d401:



    "Potrei chiederglielo in maniera diretta, raccontarle la mia storia…"


translate italian zhenya_route_a563b120:



    mtp "Oh, eccoti qui!"


translate italian zhenya_route_d863b12a:



    "La leader del Campo era in piedi dietro di me."


translate italian zhenya_route_364c8304:



    "Mi sono voltato di scatto e le ho detto irritato:"


translate italian zhenya_route_ec8eb135:



    pi "Cosa vuoi?"


translate italian zhenya_route_e5143970:



    "Come sempre, come ero abituato."


translate italian zhenya_route_6f62e389:



    mtp "Perché stai tentennando qui? Ti sei forse dimenticato che devi ottenere la firma anche nella biblioteca?"


translate italian zhenya_route_18359698:



    "La leader del Campo sembrava non prestare attenzione alle mie parole e al mio tono."


translate italian zhenya_route_b9be1462:



    "Non come al solito, ma come qualche tempo fa."


translate italian zhenya_route_4959d934:



    "Assieme ai ricordi della mia prima apparizione in questo Campo sono arrivate le immagini a lungo dimenticate dei residenti locali – coloro che a quei tempi consideravo persone, e non semplici decorazioni."


translate italian zhenya_route_780f294e:



    pi "È solo che, come dire…"


translate italian zhenya_route_fdd19e6d:



    mtp "Non ne voglio sapere – voglio che fai firmare tutto prima di cena!"


translate italian zhenya_route_6f46c062:



    pi "Ma certo, stavo giusto andando a vedere…"


translate italian zhenya_route_5228d6ae:



    mtp "Allora vai!"


translate italian zhenya_route_5e43c0de:



    "Olga Dmitrievna mi ha seguito con lo sguardo fino alla porta, così non ho dovuto nemmeno decidere quale sarebbe stata la mia prossima mossa – la leader del Campo mi ha {i}assistito{/i} in ciò."


translate italian zhenya_route_0dda31b4:



    "La biblioteca puzzava di polvere di libri, vecchi mobili sovietici e un po' di umidità."


translate italian zhenya_route_653614f9:



    "Zhenya stava seduta senza degnarmi d'attenzione, e pare che fosse tornata in sé solo dopo che ho chiuso la porta."


translate italian zhenya_route_c95e0c61:



    pi "Riciao."


translate italian zhenya_route_e5989bb1:



    "Ho esordito con esitazione."


translate italian zhenya_route_91f4ad81:



    "Dopo tutto, ero abituato a seguire un certo schema comportamentale e comunicativo nei confronti degli abitanti del Campo, ed era difficile per me cambiare le mie abitudini."


translate italian zhenya_route_c9281090:



    "E quella leader del Campo non mi ha permesso neppure di prepararmi adeguatamente!{w} Vorrei averla strangolata con quel suo panama come sempre!"


translate italian zhenya_route_b83eb7ab:



    "Zhenya non ha risposto, ma continuava a guardarmi con diffidenza."


translate italian zhenya_route_8c8749a0:



    pi "Sono venuto per ottenere la firma per la lista…"


translate italian zhenya_route_195cbb9a:



    mzp "Va bene, da' qua."


translate italian zhenya_route_addf2dc6:



    "Ha risposto in tono freddo."


translate italian zhenya_route_3abc9bf6:



    "Ho iniziato a frugare nelle mie tasche e poi mi sono reso conto…"


translate italian zhenya_route_d14407b5:



    "No, c'è qualcosa di veramente sbagliato in me!"


translate italian zhenya_route_28462b26:



    "A questo punto dovrei essermi abituato ormai a tutto, mentre invece mi sto comportando come un bambino della prima elementare!{w} Per la precisione – come un {i}nuovo arrivato{/i} in questo mondo."


translate italian zhenya_route_057d924f:



    pi "Credo di averla persa…"


translate italian zhenya_route_86695cb0:



    "Le ho detto, facendo una faccia sconvolta."


translate italian zhenya_route_a2dd85af:



    mzp "Che peccato."


translate italian zhenya_route_acd3da05:



    "Ha risposto nello stesso tono piatto e disinteressato."


translate italian zhenya_route_369b2b04:



    "Mi sono bloccato in una specie di dilemma – se mi girassi e me ne andassi adesso, allora… allora cosa?"


translate italian zhenya_route_404fa019:



    "Ma in quel momento la mia determinazione è sbucata da chissà dove."


translate italian zhenya_route_53cfe566:



    pi "Posso farti una domanda?"


translate italian zhenya_route_e9ebecff:



    mzp "Quale domanda?"


translate italian zhenya_route_c0ce709a:



    "E davvero – quale?"


translate italian zhenya_route_7fcb23e3:



    "«Non pensi che ultimamente le pietanze della mensa siano troppo poco salate?», «Si è fatto freddo di notte qui, non credi?», «Oh a proposito, sapevi che io sono un alieno venuto dal futuro?»…"


translate italian zhenya_route_283f6bd3:



    pi "Stai bene? Ultimamente mi sembra che tu sia arrabbiata per qualcosa."


translate italian zhenya_route_632a1baf:



    "Il volto di Zhenya ha mostrato una traccia di interesse."


translate italian zhenya_route_1a27f095:



    mzp "Ma noi non ci conosciamo."


translate italian zhenya_route_eb7317c7:



    pi "Sì, certo, è solo che… Ti ho vista ieri e ho pensato…"


translate italian zhenya_route_d9cd8027:



    "Mi stavo impigliando sempre più nelle menzogne."


translate italian zhenya_route_e2013459:



    "L'incapacità di dire la verità mi stava spingendo sempre più verso l'autismo, riducendo il mio vocabolario."


translate italian zhenya_route_ccc2d340:



    mzp "Anch'io ti ho visto correre per il Campo ieri, ma ancora non ci conosciamo."


translate italian zhenya_route_65f82f5f:



    pi "No, è vero, ma io… È stata Slavya a raccontarmi di te, sì! Siete compagne di stanza, giusto?"


translate italian zhenya_route_46665132:



    mzp "E cosa ti ha detto?"


translate italian zhenya_route_07d63e82:



    "A poco a poco Zhenya stava iniziando ad uscire dal suo stato contemplativo."


translate italian zhenya_route_f3d36d37:



    pi "Niente."


translate italian zhenya_route_b3d08fc6:



    "Ho sospirato pesantemente e sono crollato sulla sedia accanto alla porta."


translate italian zhenya_route_b26c8325:



    "Fuori dalla finestra gli uccellini dalle mille voci stavano cantando le loro canzoni, i grilli frinivano, da qualche parte dell'acqua è schizzata, e da lontano provenivano i suoni pimpanti dei bambini che ridevano."


translate italian zhenya_route_7deb2005:



    "Ma tutto ciò era lì – oltre le mura della biblioteca. All'interno c'eravamo solo noi due, io e Zhenya – o meglio, una ragazza che assomigliava a Zhenya."


translate italian zhenya_route_0fb35f2f:



    "Non si trattava di paura, ma piuttosto di curiosità."


translate italian zhenya_route_79a8f2f4:



    pi "Supponiamo che io non sia io."


translate italian zhenya_route_eae3d435:



    mzp "E allora chi sei?"


translate italian zhenya_route_3797aeb4:



    "Per un momento mi sono persino sentito offeso dal fatto che lei non sapesse chi fossi io."


translate italian zhenya_route_7b7b5828:



    "Ma come, io! Oh, quell'io, proprio quello? Sì, sì, quel tizio!"


translate italian zhenya_route_6fa6e3c2:



    "I pionieri di molti mondi raccontavano leggende sulle mie imprese!"


translate italian zhenya_route_33615421:



    "Ma a questa precisa Zhenya in questo preciso mondo sembrava che non importasse un accidente di chi fossi o di cosa stessi facendo qui, e la nostra conversazione che sembrava aver catturato il suo interesse, si era trasformata in delle chiacchiere inutili."


translate italian zhenya_route_49aeaf7e:



    pi "Chi sembro?"


translate italian zhenya_route_bebec5bf:



    "Zhenya mi ha esaminato."


translate italian zhenya_route_1704c62e:



    mzp "Un pioniere?"


translate italian zhenya_route_d6914ece:



    pi "Sì, certo, un pioniere, è logico, bella battuta!"


translate italian zhenya_route_fea0eb84:



    "Ho sorriso strenuamente."


translate italian zhenya_route_7f7b1369:



    mzp "Non era una battuta."


translate italian zhenya_route_37114254:



    pi "È solo che io sembro come me – tu invece no."


translate italian zhenya_route_261dc859:



    mzp "Io non sembro come me?"


translate italian zhenya_route_4a9dd7d0:



    pi "Non sembri come te, perdio!"


translate italian zhenya_route_9fc4efee:



    "L'assalto finale è iniziato."


translate italian zhenya_route_9ca493ef:



    mzp "In che senso?"


translate italian zhenya_route_62482817:



    "Si è chiesta."


translate italian zhenya_route_dd75e4f3:



    pi "Nel senso che non sei Zhenya! No, voglio dire che non sei la bibliotecaria locale!"


translate italian zhenya_route_332c7b34:



    mzp "Chi sono io, allora?"


translate italian zhenya_route_b749f9b4:



    "Ha portato il suo sguardo di lato, come se stesse cercando qualcosa tra gli scaffali."


translate italian zhenya_route_6f01edd9:



    pi "Non lo so, è per questo che te lo sto chiedendo."


translate italian zhenya_route_d9435aaa:



    mzp "Se questo è un altro scherzo…"


translate italian zhenya_route_916dd930:



    pi "Scherzo?"


translate italian zhenya_route_7dbdbf2d:



    mzp "Beh, sai, le voci si sono già sparse…"


translate italian zhenya_route_340ec68e:



    "Si è imbarazzata ed è arrossita."


translate italian zhenya_route_27b31d2a:



    pi "No, non lo so."


translate italian zhenya_route_d191e01e:



    mzp "Con la puntina sotto al mio sedere… Mi sono alzata per prendere un libro, e mentre lo stavo cercando, Alisa…"


translate italian zhenya_route_29245267:



    "Zhenya ha abbassato lo sguardo fissando il pavimento, con gli occhi pieni di lacrime."


translate italian zhenya_route_5dabba5b:



    pi "No, non ho sentito parlare di una cosa del genere."


translate italian zhenya_route_0eb20230:



    "Anche se a pensarci bene, qualcosa di simile era accaduto in passato – Alisa aveva deciso di farle uno scherzo."


translate italian zhenya_route_9375f9d2:



    "È solo che a quei tempi non ci avevo fatto proprio caso."


translate italian zhenya_route_be40d38a:



    "E anche Zhenya aveva reagito in modo diverso, credo."


translate italian zhenya_route_945283c8:



    "Alisa…"


translate italian zhenya_route_ce617998_9:



    "…"


translate italian zhenya_route_3c8c8c92:



    dvp "Lasciami andare!"


translate italian zhenya_route_e3f1127e:



    "Ha borbottato la ragazza pioniere mentre si abbottonava la camicia, con la sua lingua che si muoveva a malapena."


translate italian zhenya_route_b5f2f446:



    "Il circolo di cibernetica era immerso nel chiaro di luna, sul tavolo c'era una bottiglia vuota, con accanto un piccolo laghetto di vodka fuoriuscito da essa."


translate italian zhenya_route_b53b097a:



    "La mia guancia bruciava e la testa mi girava così forte che non riuscivo nemmeno a capire esattamente il perché. Ma difficilmente era dovuto al solo alcol."


translate italian zhenya_route_81275182:



    dvp "Se credi che io sia quel tipo di ragazza, allora vaffanculo!"


translate italian zhenya_route_e04b78e8:



    "Le sue dita non le stavano dando retta, il bottone scivolava avanti e indietro, rifiutandosi di entrare nella posizione prestabilita."


translate italian zhenya_route_965504ff:



    pi "Ma davveeeeero?"


translate italian zhenya_route_4dd24508:



    "Ho perso la pazienza, ho afferrato la pioniera per le braccia, l'ho spinta contro il tavolo e le ho strappato la camicia con i denti."


translate italian zhenya_route_ba65372b:



    pi "Come sei allora? Come sei, ti sto chiedendo! Non è una domanda retorica – rispondimi!"


translate italian zhenya_route_93a9420e:



    "Non volevo cambiare nulla, era tutto perfetto!"


translate italian zhenya_route_f2b8909a:



    "Stavo vivendo la mia solita vita nel mio solito mondo, facendo quello che volevo!"


translate italian zhenya_route_d1d1c36c:



    "Perché diavolo avrei bisogno di questa novità!?"


translate italian zhenya_route_5db0b7d7:



    pi "Tu lo vuoi, lo so! Andare a tracannarti una bottiglia di vodka con uno sconosciuto dopo soli cinque minuti che l'hai incontrato! Al diavolo tutti voi! Voglio che tutto torni com'era prima!"


translate italian zhenya_route_b1c84289:



    "Stavo perdendo il controllo di me stesso, della situazione, del mondo intero…"


translate italian zhenya_route_b99db903:



    dvp "Lasciami andare, mi fai male!"


translate italian zhenya_route_28eb9141:



    "Ha pianto la ragazza."


translate italian zhenya_route_0da4bcdf:



    "Ma io la tenevo con fermezza, e dopo un minuto si è afflosciata e ha chiuso i suoi occhi, con delle calde e salate lacrime che scorrevano ogni tanto da essi."


translate italian zhenya_route_78f90845:



    pi "Ecco, vedi, così va molto meglio."


translate italian zhenya_route_8572d521:



    "L'ho palpata rudemente un po' ovunque, respirando affannosamente."


translate italian zhenya_route_6ca28273:



    pi "Perché tu sei mia! Perché qui siete tutti miei! Tutto questo mondo è mio!"


translate italian zhenya_route_74cdf889:



    "Ma la ragazza non ha opposto resistenza, e improvvisamente ho perso interesse, ho persino provato disgusto nei suoi confronti."


translate italian zhenya_route_c3d0fb09:



    pi "E non ho bisogno di te – ce ne sono di migliori qui!"


translate italian zhenya_route_a834cad6:



    "L'ho liberata e mi sono lasciato scivolare a terra, esausto."


translate italian zhenya_route_7a01683b:



    "La pioniera non si è mossa, si è limitata a singhiozzare in silenzio."


translate italian zhenya_route_1887db4f:



    pi "Posso avere qualsiasi ragazza qui! Qualsiasi, hai capito!? E poi prenderò il machete, e… Credi forse che non sia già successo in passato?"


translate italian zhenya_route_9bd59498:



    "Stavo gridando."


translate italian zhenya_route_35794ca1:



    pi "Pensi che puoi dirmi cosa fare?! Questo è il mio mondo, hai sentito bene, il mio! Voi tutti, non esistete neppure! Sto parlando a me stesso!"


translate italian zhenya_route_c9bc6d68:



    pi "Smettila di piagnucolare!"


translate italian zhenya_route_6b0ebc18:



    "Ho perso le ultime gocce di autocontrollo, sono balzato in piedi e mi sono appoggiato di tutto peso sulla ragazza."


translate italian zhenya_route_0c326074:



    "Lei sembrava già prossima allo svenimento."


translate italian zhenya_route_d1490e2d:



    "Mi sono chinato in avanti e le ho sussurrato all'orecchio:"


translate italian zhenya_route_db4eee40:



    pi "Sai cosa ho fatto con te? Così tante volte… Vuoi sentirlo? Oh no, ascolta ora e lo saprai!"


translate italian zhenya_route_d47d3ccb:



    "Improvvisamente sono stato preso dal panico."


translate italian zhenya_route_748351d4:



    "Era trascorso un tempo infinitamente lungo dall'{i}inizio{/i}, ma ogni secondo che passava io l'ho «vissuto», ero consapevole di me stesso come persona, riuscivo a controllare la situazione in questo mondo – e in molti altri prima di esso."


translate italian zhenya_route_ba6bab34:



    "E per la prima volta ho avuto la sensazione che io stessi scomparendo, che da un momento all'altro sarei sparito!"


translate italian zhenya_route_2f2f0661:



    "Come può essere, sono IO, non vedi? Non ti ricordi...?"


translate italian zhenya_route_1eb562e0:



    dvp "Sei un debole…"


translate italian zhenya_route_c7cfef75:



    "Il sussurro della ragazza mi ha raggiunto come se provenisse da un altro mondo."


translate italian zhenya_route_1ac9eb61:



    dvp "Un debole e patetico nessuno. Vorrei provare pietà nei tuoi confronti, ma non si può compatire qualcosa di vuoto."


translate italian zhenya_route_fd0f9179:



    "Ho indietreggiato per il terrore."


translate italian zhenya_route_7f9a4127:



    "Sul volto della pioniera si è congelata una smorfia di disgusto, e le lacrime sono scomparse come se non ci fossero mai state."


translate italian zhenya_route_8eccaba8:



    "Si è alzata dal tavolo, ha fatto un paio di passi verso di me e si è fermata."


translate italian zhenya_route_ce617998_10:



    "…"


translate italian zhenya_route_01793405:



    mz "Perché mi stai raccontando tutto questo?"


translate italian zhenya_route_455e39f2:



    "Ha chiesto pigramente Zhenya, voltando una pagina."


translate italian zhenya_route_c9efc482:



    "Le increspature scorrevano sull'acqua, sopra di noi stavano ondeggiando le foglie di una betulla, con i fasci luminosi dell'affaticante sole di mezzogiorno che splendeva tra di esse."


translate italian zhenya_route_3dd25e16:



    "Gli uccellini cantavano ad alta voce nel boschetto, e i piccoli insetti che si nascondevano nell'erba saltavano affaccendati sopra le mie gambe."


translate italian zhenya_route_b92f4659:



    pi "Beh, perché stai leggendo quel libro? Non mi dire che è la prima volta!"


translate italian zhenya_route_0584c94e:



    mz "Perché mi piace. Vedi, a te piace narrare e a me piace leggere."


translate italian zhenya_route_b801ccf7:



    "Sull'altra sponda, alla spiaggia, i pionieri si stavano divertendo, Olga Dmitrievna correva dietro a Ulyana e nel mezzo del fiume ho scorto la testa dorata di Slavya.{w} Forse si stava dirigendo qui?"


translate italian zhenya_route_bf61ccfa:



    mz "E allora?"


translate italian zhenya_route_1d91a5d4:



    pi "Che cosa?"


translate italian zhenya_route_9efbba46:



    mz "Cosa è successo dopo?"


translate italian zhenya_route_1ecf5995:



    pi "Pensavo che non ti interessasse."


translate italian zhenya_route_17ce821c:



    mz "A te sì però."


translate italian zhenya_route_beecbb90:



    "Zhenya ha sorriso."


translate italian zhenya_route_ce617998_11:



    "…"


translate italian zhenya_route_e2ddd404:



    "Mi ha sputato in faccia e se n'è andata, sbattendo forte la porta dietro di sé."


translate italian zhenya_route_ec4fada4:



    "E io sono rimasto lì."


translate italian zhenya_route_4d9c3879:



    "Cos'è tutto questo – è forse la fine?"


translate italian zhenya_route_d02cc081:



    "Ad un certo punto ho iniziato ad attendere quella fine, forse anche con impazienza."


translate italian zhenya_route_b963c577:



    "Ma è stato così tanto tempo fa, che non me lo ricordo nemmeno…"


translate italian zhenya_route_aa69bc1b:



    "E adesso?"


translate italian zhenya_route_fb8066ab:



    "Queste bambole, queste bambole senz'anima hanno distrutto il mio mondo che avevo costruito durante tutta la mia vita...{w} Semplicemente così – nel giro di un giorno...?"


translate italian zhenya_route_6072604a:



    "Ho fatto scorrere la mia mano sulla guancia – le mie dita si sono ricoperte di qualcosa di umido."


translate italian zhenya_route_ce617998_12:



    "…"


translate italian zhenya_route_3690f9af:



    "La notte era calata ormai da tempo sul Campo, nascondendo i pionieri locali sotto la sua copertura, celando la loro astuzia e malizia."


translate italian zhenya_route_a97066dd:



    "E io me ne stavo semplicemente seduto nel circolo di cibernetica, con le braccia strette attorno alle mie gambe, dondolando lentamente da un lato all'altro."


translate italian zhenya_route_78432774:



    "Forse quella ragazza pioniere tornerà adesso, dopotutto potrebbe umiliarmi ancora di più.{w} O magari anche peggio…"


translate italian zhenya_route_b724315a:



    "Ma il tempo passava e solo il canto degli uccelli notturni e il frinire dei grilli stava diventando sempre più tranquillo."


translate italian zhenya_route_5dafa38c:



    "E anche l'alba si stava avvicinando, e poi il Campo si sarebbe risvegliato e avrebbe iniziato a vivere la sua vita che improvvisamente è diventata così poco familiare per me."


translate italian zhenya_route_9b97de77:



    "Beh, chi se ne frega, al diavolo tutto questo…"


translate italian zhenya_route_f388c609:



    "Mi sono alzato in piedi con difficoltà e ho guardato la bottiglia di vodka vuota appoggiata sul tavolo."


translate italian zhenya_route_f56a8f65:



    "Sarebbe bello svenire adesso e svegliarsi domani come se nulla fosse accaduto, dimenticarsi di quest'oggi, apparire in un mondo dove mi trovo a mio agio e dove il comportamento dei pionieri è familiare e prevedibile!"


translate italian zhenya_route_7f6ffa46:



    "E tutto è cominciato con quella ragazzina…"


translate italian zhenya_route_48e2be22:



    "Non aveva mai pianto prima."


translate italian zhenya_route_930e94bf:



    "O forse l'ha fatto ma io non me ne sono mai accorto? Anche se fosse così, e allora?"


translate italian zhenya_route_69a2c6d6:



    "Allora cosa? Dovrei diventare un debole, provare dispiacere per quella stronzetta, dovrei persino scendere al {i}loro{/i} livello?"


translate italian zhenya_route_039e2cb1:



    "No, devo rimettermi in sesto!"


translate italian zhenya_route_d565336d:



    "Ho lasciato il circolo di cibernetica, sbattendo forte la porta, e ho inalato la fresca aria notturna."


translate italian zhenya_route_18c48e62:



    "Strano, non mi sembra artificiale come al solito."


translate italian zhenya_route_786313e6:



    "Allora, dove potrei andare?"


translate italian zhenya_route_c86ccfda:



    "Credo che valga la pena scovare quella ragazza pioniere e vendicarmi per quel suo sputo in faccia."


translate italian zhenya_route_2d3dd531:



    "Non avrei mai tollerato un insulto del genere in passato, e non lo farò di certo adesso!"


translate italian zhenya_route_cac4acc5:



    "Non c'era una sola anima viva nella piazza a parte un pioniere seduto su una panchina."


translate italian zhenya_route_1eaf6c8a:



    "In un primo momento non l'ho degnato di alcuna attenzione – chissà quanti ritardati abitavano qui – ma lui mi ha chiamato:"


translate italian zhenya_route_b5f924fc:



    pi2 "Ehi, Semyon!"


translate italian zhenya_route_a0e4c394:



    "«Chi diavolo stai chiamando Semyon, accidenti a te!»{w} Ho sibilato sottovoce."


translate italian zhenya_route_e16dba53:



    "Ma il pioniere non si è placato:"


translate italian zhenya_route_4a9d1da0:



    pi2 "Stai cercando qualcuno? Ma lei non è andata da quella parte."


translate italian zhenya_route_020a3c0b:



    "Sono rimasto immobile, ma subito mi sono girato e mi sono incamminato verso di lui."


translate italian zhenya_route_b26f9b1a:



    pi "E così, l'hai vista?"


translate italian zhenya_route_faffbf87:



    pi2 "Certo che l'ho vista!"


translate italian zhenya_route_e9213e07:



    "Un sorriso disgustoso è apparso sul suo volto, forse anche un ghigno, uno selvaggio, che esprimeva superiorità nei confronti dell'avversario."


translate italian zhenya_route_dd90be7e:



    pi "Sputa il rospo allora, non stiamo giocando a indovinello!"


translate italian zhenya_route_0035a9a8:



    pi2 "Aspetta, perché vai così di fretta? Siediti, parliamo con calma."


translate italian zhenya_route_1d25a44c:



    pi "Non ho niente di cui parlare con te."


translate italian zhenya_route_d656bbab:



    "Il pioniere ha pensato per un momento, poi ha frugato nelle tasche dei suoi pantaloncini, ha tirato fuori un pacchetto di «Cosmos» e me l'ha porto."


translate italian zhenya_route_bfadd262:



    pi2 "Ne vuoi una?"


translate italian zhenya_route_990a64cb:



    pi "Questo… Dove l'hai preso?"


translate italian zhenya_route_da5e25e1:



    "Non ricordo che i normali pionieri avessero delle sigarette!"


translate italian zhenya_route_35ea6809:



    "Anche se nelle ultime ventiquattr'ore sono successe così tante cose che non erano mai accadute prima…"


translate italian zhenya_route_49df45e9:



    "Lentamente ho tirato fuori una sigaretta e ho ridato il pacchetto allo strano pioniere."


translate italian zhenya_route_9075cb57:



    "Se l'è girato tra le mani per un po', e poi me l'ha lanciato nuovamente."


translate italian zhenya_route_ef0baff0:



    pi2 "Nah, prendile pure, io ho smesso."


translate italian zhenya_route_7302d938:



    "E ancora una volta quello sporco sorriso!"


translate italian zhenya_route_b9b224aa:



    "Al diavolo – però ho bisogno di catturare quella cagna insolente!"


translate italian zhenya_route_e5239908:



    pi "Ebbene? Dimmi dov'è andata!"


translate italian zhenya_route_b0bacf80:



    pi2 "Perché hai bisogno di quella povera ragazza? Lei è insignificantemente piccola sulla scala dell'Universo, e forse non esiste nemmeno in questo mondo."


translate italian zhenya_route_6e286439:



    "Ha pensato per un po', poi ha continuato."


translate italian zhenya_route_9a27b7d6:



    pi2 "Lo stesso vale per te. Sei sicuro di esistere? E anche se fosse, sei sicuro che la tua esistenza qui e adesso sia quella vera? Forse sei realmente esistito prima, ma non lo sai?"


translate italian zhenya_route_0c2d21db:



    pi2 "Oppure esisterai in futuro e in questo momento sei solo una bambola in piedi davanti a me?"


translate italian zhenya_route_7b1709f6:



    "La parola «bambola» ha bruciato dolorosamente nella mia testa, come se una Colt calibro 45 mi avesse sparato in essa, facendo schizzare il contenuto della scatola cranica tutt'attorno."


translate italian zhenya_route_3851cbd0:



    "Già, e domani tocca a Slavya pulire qui…{w} Ho sorriso involontariamente nei miei pensieri."


translate italian zhenya_route_aef5fe7f:



    pi "Chi sei tu?"


translate italian zhenya_route_797cca45:



    "La ragazza insolente che mi aveva sputato in faccia era volata via dalla mia testa ormai da tempo, assieme al mio cervello."


translate italian zhenya_route_b49e136e:



    pi "Chi sei tu, ti sto chiedendo!"


translate italian zhenya_route_3286fb9d:



    "L'ho afferrato per il foulard e l'ho spinto verso di me con una tale forza che il dannato straccio rosso si è quasi strappato."


translate italian zhenya_route_d4433ae7:



    "Ma il pioniere si è limitato a sorridere."


translate italian zhenya_route_522e4cdd:



    pi2 "Calmati, non preoccuparti più di tanto. Qui non si può morire – dovresti saperlo, l'hai vissuto centinaia di volte."


translate italian zhenya_route_393c873a:



    "Mi sono allontanato da lui, in preda al terrore."


translate italian zhenya_route_d92d4988:



    "Quella frase…{w} L'ho sicuramente già sentita prima!"


translate italian zhenya_route_de5bcfba:



    "No, anzi – l'avevo detta io stesso!"


translate italian zhenya_route_aff47745:



    "Molto tempo fa, nella mia precedente… In una delle mie vite passate."


translate italian zhenya_route_c313ce95:



    pi2 "Perché tu sei Semyon.{w} E io sono Semyon. Perché tu sei me."


translate italian zhenya_route_530a8a5f:



    "La panchina e il pioniere si sono allontanati bruscamente di un centinaio di metri di distanza da me, come se stessero precipitando dalle montagne russe, solo il suo sorriso disgustoso è rimasto dinanzi ai miei occhi."


translate italian zhenya_route_6ecb5b42:



    "E poi il mondo intero si è oscurato…"


translate italian zhenya_route_ce617998_13:



    "…"


translate italian zhenya_route_b052fa58:



    me "Beh, e poi sono scomparso."


translate italian zhenya_route_9b867c85:



    mz "Così su due piedi?"


translate italian zhenya_route_0482fa2e:



    "Zhenya teneva gli occhi fissi sul suo libro, ma sembrava che la storia avesse iniziato a catturare il suo interesse."


translate italian zhenya_route_8b0f35ac:



    me "Perché no? La partenza puntuale è una grande abilità, è risaputo, e io sono un maestro in ciò!"


translate italian zhenya_route_eab0814c:



    mz "Sì, sei un maestro in tutto…"


translate italian zhenya_route_57d8f4cb:



    "Ha posato il libro e mi ha abbracciato."


translate italian zhenya_route_257596c3:



    "Slavya, che stava chiaramente nuotando verso di noi, sembrava averlo notato e si è girata tornando indietro verso la spiaggia."


translate italian zhenya_route_6258eed6:



    me "Aspetta, non sono nemmeno arrivato alla parte principale!"


translate italian zhenya_route_a2112eb3:



    me "E comunque, non qui…"


translate italian zhenya_route_3b51a4ca:



    "Mi sono scostato timidamente da lei."


translate italian zhenya_route_ce05b572:



    "A Zhenya chiaramente non è piaciuta la cosa, e si è allontanata da me."


translate italian zhenya_route_02d00f0e:



    mz "Sono stanca di queste tue storie – sono sempre le stesse! Lascia che ti racconti la mia, per cambiare un po'."


translate italian zhenya_route_b401bc94:



    me "E cosa vorresti raccontarmi, mi chiedo?"


translate italian zhenya_route_43f0b201:



    mz "Di come sono arrivata qui, per esempio!"


translate italian zhenya_route_6a25d8e4:



    me "L'ho già sentito un centinaio di volte."


translate italian zhenya_route_46e49702:



    mz "E io ho sentito le tue storie duecento volte!"


translate italian zhenya_route_a20cefa7:



    "..."


translate italian zhenya_route_7372fd7b:



    "Per Zhenya, «Sovyonok» non è iniziato con il nostro incontro e la lista delle cose da fare."


translate italian zhenya_route_776cce52:



    "Perché ho sempre avuto, ho, e avrò {i}il mio{/i} primo giorno della sessione solo dopo che lei ha già trascorso una settimana nel Campo."


translate italian zhenya_route_6b6478fd:



    mz "Hai presente quella sensazione quando il tempo si ferma? Quando non puoi morire, anche se lo vorresti davvero?"


translate italian zhenya_route_edad5831:



    me "Beh, sì. Come qui, in questo Campo?"


translate italian zhenya_route_0766c187:



    mz "No, è sempre stato più facile per me qui. Le persone si comportano più onestamente, correttamente, nessuno fa il bullo con gli altri, non ce ne sono qui, o per lo meno, quella malizia che si percepiva ovunque allora, {i}a quei tempi{/i}, te lo ricordi?"


translate italian zhenya_route_cbc4bb84:



    "Non le ho risposto."


translate italian zhenya_route_0b199ede:



    mz "L'unica cosa che riesco a fare è leggere libri, parlare con le persone, qualcuno ha bisogno di me, qualcuno mi considera un'amica, ho delle responsabilità…"


translate italian zhenya_route_72a4670a:



    "Sembrava che Zhenya fosse sul punto di scoppiare in lacrime."


translate italian zhenya_route_360485e5:



    mz "Va bene, perché mi sto… Ad ogni modo, questa volta mi sono svegliata a causa dello scuotimento eccessivo del bus 410. Hai mai preso quell'autobus?"


translate italian zhenya_route_b91f0b42:



    "Ho annuito."


translate italian zhenya_route_ed2494b4:



    mz "Beh io no, nella nostra città non ci sono nemmeno autobus con tre cifre."


translate italian zhenya_route_13c9611e:



    mz "Non avevo ancora capito cosa stesse succedendo, e la ragazza che stava seduta accanto a me mi ha detto: «Ciao, io mi chiamo Slavya»."


translate italian zhenya_route_73f9c19e:



    mz "Slavya? Quale Slavya? Slavya chi...?"


translate italian zhenya_route_3d09c388:



    mz "Naturalmente ero confusa, ma non ho cercato di mostrarlo. Poi siamo arrivati al Campo e le cose hanno cominciato a diventare confuse…"


translate italian zhenya_route_385c7f99:



    me "E poi è successo di nuovo…"


translate italian zhenya_route_9d45a0d2:



    mz "E poi ancora…"


translate italian zhenya_route_be100ad4:



    "Zhenya ha sospirato."


translate italian zhenya_route_8110044b:



    mz "E ti ho visto molte volte. Tu con Slavya, con Alisa, con Lena…"


translate italian zhenya_route_a150cf10:



    "Ha stretto i denti in silenzio."


translate italian zhenya_route_66a94d6b:



    me "Ma quello non ero io! Ne abbiamo già parlato un centinaio di volte, e adesso ricominci!"


translate italian zhenya_route_09769e7d:



    mz "Tu, o non tu… Siete tutti come dei piselli in un baccello! Anche «io» ero nei tuoi campi, giusto? Ma quelle non erano affatto la vera «me»!"


translate italian zhenya_route_cf5fe3d9:



    me "Esatto! Distinguere l'originale dal falso non è così difficile come credi!"


translate italian zhenya_route_2a487144:



    mz "Sì, perché i falsi vengono rigenerati ogni volta, mentre gli originali rimangono gli stessi."


translate italian zhenya_route_931c85d9:



    me "Non è affatto così…"


translate italian zhenya_route_90aa467c:



    mz "Come se tu sapessi tutto!"


translate italian zhenya_route_a6c61dc1:



    me "Che importa?!"


translate italian zhenya_route_b9e0fc87:



    "L'ho abbracciata forte e ho chiuso gli occhi."


translate italian zhenya_route_a20cefa7_1:



    "..."


translate italian zhenya_route_f38c94d0:



    "Mi sono svegliato ricoperto dal sudore, il mio cuore batteva all'impazzata."


translate italian zhenya_route_95485d35:



    "Il sole che sorgeva era insopportabilmente caldo, anche se sembrava che non fossero ancora le sei o le sette."


translate italian zhenya_route_7c0f70f5:



    "Cosa diavolo mi è successo?{w} Il pioniere funesto – cos'era quello?"


translate italian zhenya_route_b8fea11b:



    "Forse era solo un sogno, un'allucinazione?"


translate italian zhenya_route_6d64ec4f:



    "In questo momento sono pronto a crederci…{w} A credere in qualsiasi cosa!"


translate italian zhenya_route_b1ec62eb:



    "Qualcuno stava camminando lentamente verso di me attraverso la piazza."


translate italian zhenya_route_54e2fa21:



    "Ho guardato meglio e ho riconosciuto la leader del Campo."


translate italian zhenya_route_f2c53477:



    mtp "E così ci risiamo?"


translate italian zhenya_route_a339aad1:



    "Mi ha detto minacciosamente, chinandosi sopra di me."


translate italian zhenya_route_d619304f:



    "Il che mi ha fatto alzare dalla panchina e ho fatto un passo indietro."


translate italian zhenya_route_00676949:



    mtp "E io che credevo che ieri si fosse trattato solo di un malinteso, e tu invece… Sono senza parole! Fare una cosa del genere ad una ragazza!"


translate italian zhenya_route_7fbc5e40:



    "Ma di cosa sta parlando?"


translate italian zhenya_route_a6084c5b:



    "Quale ragazza, che cosa vuole da me, dannazione?!"


translate italian zhenya_route_0790d314:



    "Almeno la leader del Campo sembrava sempre la stessa, il che mi ha calmato un po'."


translate italian zhenya_route_fe2f533c:



    mtp "Ebbene? Rispondimi subito! Pensi di farla franca anche questa volta? Chiamerò la polizia, porterò la questione anche alla segreteria dell'organizzazione di pionieri del Komsomol!"


translate italian zhenya_route_e0de5854:



    "Stava sbuffando così tanto che è diventata tutta rossa e ha iniziato ad ansimare."


translate italian zhenya_route_7ca78ad4:



    mtp "Nel mio Campo! Io! Io ti...!"


translate italian zhenya_route_b745da71:



    "In generale, un tale comportamento da parte della leader del Campo non mi sorprendeva nemmeno un po'."


translate italian zhenya_route_71972c56:



    "Ma mi sentivo a disagio perché la mia mente è tornata agli eventi della notte scorsa, lasciandomi una sensazione deprimente e dolorosa, simile all'imbarazzo verso qualcosa che avevo fatto da ubriaco."


translate italian zhenya_route_563df633:



    "La cosa più strana è che non mi interessava affatto della leader del Campo che stava gridando ad alta voce a un solo metro di distanza da me."


translate italian zhenya_route_17ef50dc:



    pi "Sta' zitta, ok?! Gridi come se ci fosse un incendio! Che cosa ne sai tu dei problemi? Pensi di avere un problema qui? Oh-oh-oh, una pioniera è stata quasi violentata, che disastro!"


translate italian zhenya_route_e625c257:



    "Ho stretto i denti e ho lanciato uno sguardo minaccioso verso la leader del Campo, e ciò ha causato un guizzo di paura sul suo volto."


translate italian zhenya_route_88d9cfee:



    pi "Vuoi che ti dica dei {i}veri{/i} problemi?! Non quelli del tuo piccolo mondo immaginario!"


translate italian zhenya_route_7eb5bc4b:



    pi "Anche se, puoi andare al diavolo! Sei solo una perdita di tempo!"


translate italian zhenya_route_156bb973:



    "D'altra parte, di tempo ne ho ancora un sacco qui…"


translate italian zhenya_route_cf548d39:



    "Un pensiero spaventoso improvvisamente mi è balenato in testa: e se da questo momento sarà tutto sempre {i}così{/i}...?"


translate italian zhenya_route_af5d6b63:



    "Cosa succederebbe se perdessi il controllo di questo mondo per sempre?"


translate italian zhenya_route_1073c912:



    "Ogni ragazza pioniere avrà la libertà di sputarmi in faccia e la leader del Campo verrà a rimproverarmi come con Electronik...?"


translate italian zhenya_route_6efa0286:



    "Ma questo è peggio della morte!"


translate italian zhenya_route_4764ccbb:



    pi "Comunque, devo andare…"


translate italian zhenya_route_2b1ad4f4:



    "Ho sorpassato la leader del Campo che ancora non si era ripresa dallo shock, e sono andato ovunque le mie gambe mi avrebbero portato."


translate italian zhenya_route_96ba2c48:



    "Ma in realtà, cosa significa per me la morte?"


translate italian zhenya_route_717b489b:



    "Sembra che io abbia smesso da tempo di comprendere il significato di tale concetto."


translate italian zhenya_route_d4c4a4be:



    "Perché se non posso sparire, se sono destinato a vivere per sempre la stessa settimana in questo Campo di dannati, allora forse la morte mi sta abbandonando?"


translate italian zhenya_route_8a3a5440:



    "Perdere il mio potere sul mondo, avendo un futuro incerto?"


translate italian zhenya_route_7e0405f4:



    "Ma questo è ancora peggio del destino di ogni essere umano {i}normale{/i}!"


translate italian zhenya_route_735eed4b:



    "La loro vita è un gioco con un finale triste e senza la possibilità di salvare i propri progressi."


translate italian zhenya_route_1ea7a991:



    "La mia vita è un gioco senza fine con la possibilità di salvare…"


translate italian zhenya_route_07e40943:



    "Come chiamerebbero {i}loro{/i} una vita del genere?"


translate italian zhenya_route_02cd0d67:



    "Se solo potessi ricordare com'è – essere un uomo normale…"


translate italian zhenya_route_d9801473:



    "Probabilmente la chiamerebbero inferno!"


translate italian zhenya_route_3da4e67a:



    "E questo particolare ciclo, dove persino le bambole senz'anima mi umiliano – è il suo ultimo girone."


translate italian zhenya_route_fc47ce7d:



    "Ero pronto a perdere la testa, se solo avessi potuto."


translate italian zhenya_route_4db8f15d:



    "Tutti i pensieri che avevo diligentemente allontanato da me improvvisamente sono riemersi con nuovo impeto, facendo riaffiorare sentimenti a lungo dimenticati."


translate italian zhenya_route_f7601ba0:



    "E alla fine chi aveva ragione?"


translate italian zhenya_route_14e500e2:



    "Ero sempre convinto del fatto che io avessi ragione, non {i}loro{/i}, perché io ero vivo e {i}loro{/i} scomparivano inseguendo vuote illusioni."


translate italian zhenya_route_aa9cc9b6:



    "Ma {i}loro{/i} scomparivano, mentre io rimanevo."


translate italian zhenya_route_6aec9b7e:



    "E alla fine chi ha avuto la parte migliore...?"


translate italian zhenya_route_a0734101:



    pi2 "Eh-eh."


translate italian zhenya_route_18a14147:



    "Una risatina ormai familiare è risuonata nelle vicinanze."


translate italian zhenya_route_ca7a3d8c:



    pi2 "Sembra che tu abbia capito qualcosa oggi!"


translate italian zhenya_route_5a75dcad:



    pi "So chi sei."


translate italian zhenya_route_ad7879c5:



    "La mia voce era calma, ma il mio cuore batteva all'impazzata come mai prima d'ora."


translate italian zhenya_route_aa40ee3d:



    pi2 "Oh davvero? Allora dimmi pure, sono curioso."


translate italian zhenya_route_7c80e143:



    pi "Tu sei come me, un pioniere, ma sei di un altro ciclo!"


translate italian zhenya_route_dc8a84af:



    pi2 "Ma non hai forse sempre pensato che {i}gli altri{/i} non esistessero più dopo che Semyon è partito con loro sul bus {i}quella{/i} volta? Credevi di essere rimasto da solo in tutti questi campi infiniti!"


translate italian zhenya_route_8249e6b5:



    pi "Potrei sbagliarmi…"


translate italian zhenya_route_900da79c:



    "Anche se è stato duro per me ammetterlo, una tale possibilità esisteva davvero."


translate italian zhenya_route_3cf57bf2:



    pi2 "Tu? Sbagliarti? Ma come, è impossibile!"


translate italian zhenya_route_2138916b:



    pi "Certo, certo. Di cosa hai bisogno, sei venuto qui solo per prendermi in giro per quello che ho fatto in passato?"


translate italian zhenya_route_28216e14:



    pi2 "Lo dici come se…{w} ti sentissi in colpa."


translate italian zhenya_route_c7603b8c:



    pi "Non mi sento in colpa e comunque non sono affari tuoi! Rispondi alla mia domanda."


translate italian zhenya_route_69b5c4c3:



    pi2 "Beh, forse hai ragione…"


translate italian zhenya_route_aa53adf9:



    "Il pioniere ha pensato per un momento."


translate italian zhenya_route_8c7036b9:



    pi2 "Anche se è peccato ridere del miserabile!"


translate italian zhenya_route_6d7221cb:



    "Ho improvvisamente sentito il desiderio di prendere a pugni quel suo disgustoso sorriso!"


translate italian zhenya_route_c1143dcc:



    "Dopotutto, sono forse un animaletto tremante, oppure ho anch'io i miei diritti?!"


translate italian zhenya_route_21d61111:



    "Chi {i}gli{/i} ha dato il permesso di imporre le sue regole nel mio mondo?"


translate italian zhenya_route_a902c6d7:



    pi "Va' al diavolo!"


translate italian zhenya_route_2323c424:



    "Ho gridato, balzando in piedi e colpendolo al volto più forte che potevo…"


translate italian zhenya_route_26c40b41:



    "Ma la mia mano si è limitata ad attraversare una nebbia che si è dissolta nell'aria, e ho perso l'equilibrio cadendo a terra rovinosamente."


translate italian zhenya_route_ce617998_14:



    "…"


translate italian zhenya_route_392741f8:



    "Dopo che il dolore si è placato un po', ho avuto la sensazione che qualcuno mi stesse osservando."


translate italian zhenya_route_eac3bbc2:



    "E in effetti, tra i cespugli ha fatto capolino un'impertinente ragazzina pioniere."


translate italian zhenya_route_15b8af29:



    usp "Ti sta bene!"


translate italian zhenya_route_f02f9218:



    "Mi ha fatto la linguaccia."


translate italian zhenya_route_319a4b06:



    usp "Immagino che tu non sia così coraggioso con i tuoi coetanei!"


translate italian zhenya_route_8a70a572:



    pi "Oh, tu…"


translate italian zhenya_route_4cc464f6:



    "Ero talmente sorpreso che ho persino dimenticato cosa volevo dirle."


translate italian zhenya_route_3bfb5da7:



    pi "Non ti sorprende affatto che un uomo si è appena dissolto nell'aria proprio davanti ai tuoi occhi?"


translate italian zhenya_route_754e98b4:



    usp "Beh…{w} Può succedere. Forse è un trucco? Comunque non importa, te lo sei meritato!"


translate italian zhenya_route_2a81e5c3:



    "Non riuscendo più a tollerare insulti del genere, sono balzato in piedi e mi sono fiondato verso di lei."


translate italian zhenya_route_4794ec9c:



    "Ma la ragazzina riusciva a destreggiarsi abilmente tra gli alberi, mentre io ho dovuto investire secondi preziosi nel cercare di evitare i rami che mi colpivano in volto, le collinette sporgenti, così come le gole invisibili."


translate italian zhenya_route_f8154d9c:



    "Dopo essere inciampato un paio di volte e a costo di essermi graffiato e insanguinato le mani, finalmente l'ho catturata alla piazza."


translate italian zhenya_route_1435d693:



    pi "Che c'è, non sei poi così veloce, eh? Non è così facile come fuggire da un qualsiasi imbecille!"


translate italian zhenya_route_7a05ead8:



    usp "Lasciami andare! Vattene!"


translate italian zhenya_route_3f110a9b:



    "Ha pianto la ragazzina pioniere."


translate italian zhenya_route_c8a389f1:



    "Improvvisamente ho sentito un forte colpo alla schiena, tutto è fluttuato davanti ai miei occhi, e il terreno ha cominciato ad avvicinarsi a me a velocità spaventosa."


translate italian zhenya_route_7cb62aa8:



    dvp "E quindi te la prendi anche coi bambini?"


translate italian zhenya_route_fc1209ed:



    slp "Questo è troppo, diamogli una lezione!"


translate italian zhenya_route_ecefc9dc:



    "Un colpo alla pancia…"


translate italian zhenya_route_3161f0e8:



    "Mi sono coperto istintivamente la testa con le mani."


translate italian zhenya_route_ad729ea8:



    mip "Non fai più il gradasso adesso, eh?"


translate italian zhenya_route_a66487fa:



    "Un altro colpo…"


translate italian zhenya_route_c834ca35:



    "A quel punto ero pronto a perdere conoscenza, ma, raccogliendo le mie ultime forze, mi sono sollevato sui gomiti e sono rotolato di lato per parecchi metri."


translate italian zhenya_route_edbb6f89:



    "Il sangue mi scorreva sugli occhi, immergendo in una perenne nebbia rossa la folla di pionieri che mi stava malmenando."


translate italian zhenya_route_3b6c1c0e:



    mtp "Questo non è, ovviamente, il metodo educativo più adeguato, ma è piuttosto efficace, suppongo."


translate italian zhenya_route_00b08032:



    "Non è chiaro da dove provenisse la voce della leader del Campo."


translate italian zhenya_route_a6717d6b:



    pi "Cosa state facendo?! Basta…"


translate italian zhenya_route_a20cefa7_2:



    "..."


translate italian zhenya_route_e42400d8:



    mz "Hai letto Don Chisciotte?"


translate italian zhenya_route_e52631bf:



    "Zhenya stava seduta accanto a me, facendo oscillare allegramente le sue gambe."


translate italian zhenya_route_1b25a65d:



    me "Sì, ma quasi non ricordo di cosa trattasse, ormai."


translate italian zhenya_route_0a5efd62:



    mz "Dei mulini a vento! Tutte le tue storie sono come la lotta contro i mulini a vento."


translate italian zhenya_route_955295f8:



    me "Quali mulini, ma per favore…"


translate italian zhenya_route_9af55b9b:



    mz "E anche se non ci fosse alcun mulino, te ne costruirai sicuramente uno, dato che devi per forza combattere contro qualcosa."


translate italian zhenya_route_17eb90aa:



    me "Lo stai dicendo come se io fossi una sorta di masochista!"


translate italian zhenya_route_3805b28b:



    mz "Non lo sei?"


translate italian zhenya_route_b47b6b13:



    "Ha sorriso sornione."


translate italian zhenya_route_f1b872bc:



    "Zhenya…{w} Cosa farei senza di lei?"


translate italian zhenya_route_520a3a56:



    "Certe volte ho pensato che tutto quello che mi era accaduto in questo Campo non fosse stato così brutto, se alla fine mi aspettava un premio come lei."


translate italian zhenya_route_fc9436ca:



    "L'ho avvicinata a me e l'ho baciata."


translate italian zhenya_route_d79bf9f9:



    mz "Aspetta, cosa stai… E se qualcuno ci vedesse?"


translate italian zhenya_route_a064965c:



    "Zhenya è arrossita e ha opposto resistenza, ma per lo più per fare scena."


translate italian zhenya_route_6d038d67:



    me "Di solito non te ne preoccupi."


translate italian zhenya_route_c19c6b50:



    mz "No, ma…{w} In ogni caso, cosa stavo dicendo?"


translate italian zhenya_route_d663f3b1:



    me "Dei mulini a vento?"


translate italian zhenya_route_86c65e85:



    mz "Sì… Cioè no! Voglio dire che non riesci ancora a darti una sistemazione! Come se quella volta avessi cavalcato quel bus come tutte le persone {i}normali{/i}…"


translate italian zhenya_route_4933f750:



    me "E non ti avrei mai incontrata."


translate italian zhenya_route_f8936c3d:



    mz "Come se prima tu l'avessi fatto! Sempre con Slavya, Lena, Alisa…"


translate italian zhenya_route_b7edee7e:



    "Mi ha detto in tono offeso."


translate italian zhenya_route_05457ff9:



    me "Beh, questo significa che ho fatto tutto giusto!"


translate italian zhenya_route_5034d5ab:



    mz "Va' al diavolo! Il maschio alfa con un harem, un corno!"


translate italian zhenya_route_c4265473:



    me "Smettiamola di parlare del passato, abbiamo tutta una vita davanti a noi!"


translate italian zhenya_route_a20cefa7_3:



    "..."


translate italian zhenya_route_b56763f0:



    pi3 "Ehi! Sei ancora vivo?"


translate italian zhenya_route_2847cd45:



    "L'acqua fredda è schizzata sulla mia faccia e sono riuscito ad aprire il mio occhio gonfio con difficoltà."


translate italian zhenya_route_35df17a9:



    "Su di me era chino…{w} un pioniere."


translate italian zhenya_route_a02937cb:



    pi "Di nuovo tu… Che c'è, {i}loro{/i} non hanno concluso il lavoro e quindi hai deciso di aiutarli? Bene, va' avanti allora, continua pure!"


translate italian zhenya_route_7b0d9a0b:



    "Da qualche parte nel profondo del mio animo ero persino felice di una tale situazione – finalmente questo odioso circolo vizioso si sarebbe spezzato!"


translate italian zhenya_route_cac3b2d5:



    pi3 "No, non capisci! Io non sono…{w} Io non sono lui!"


translate italian zhenya_route_c5c56f07:



    pi "Allora chi sei?"


translate italian zhenya_route_1804c309:



    "Ormai non me ne importava più nulla, sarebbe stato così bello morire all'istante, concludere questo ciclo e iniziarne uno nuovo!"


translate italian zhenya_route_83dddfb4:



    "Credo di non aver mai desiderato tanto che gli infiniti cicli della mia {i}vita{/i} proseguissero nel loro corso!"


translate italian zhenya_route_dffa8938:



    "L'unica cosa che conta è che essi rispettino le regole stabilite da lungo tempo."


translate italian zhenya_route_0022762c:



    pi3 "Non capisci...?"


translate italian zhenya_route_f7f100c4:



    "Mi ha chiesto angosciato."


translate italian zhenya_route_277c650c:



    pi "No-no-no, non ho più voglia di fare questi giochetti!"


translate italian zhenya_route_43104af6:



    "Seriamente, hanno forse deciso di mettermi nei panni di {i}quell'idiota{/i}?"


translate italian zhenya_route_d65516a5:



    "Questo è forse una specie di gioco, e io sono un niubbo per loro?{w} No, non nel mio server!"


translate italian zhenya_route_595caacc:



    pi "E comunque, non m'interessa!"


translate italian zhenya_route_6304a54d:



    "Ho cercato di alzarmi, ma l'unica cosa che sono riuscito a fare è stato gemere per il dolore, e mi sono appoggiato ad un tronco nelle vicinanze."


translate italian zhenya_route_ef375c44:



    "Tuttavia, se dovrò trascorrere altri cinque giorni e mezzo in questo dannato ciclo, dovrò sicuramente scendere a qualche compromesso."


translate italian zhenya_route_1b5fc009:



    pi "Ok, non m'importa chi sei o perché sei qui, ma se davvero vuoi aiutarmi, allora non rifiuterò."


translate italian zhenya_route_a0e4cf55:



    "Se lui è davvero colui che vuole far sembrare, allora posso aspettarmi un qualche tipo di aiuto da parte sua."


translate italian zhenya_route_b82cc3b2:



    pi3 "Sì, naturalmente…"


translate italian zhenya_route_b3db7e6e:



    pi "Portami qualcosa da bere allora."


translate italian zhenya_route_87e7c94d:



    "Il pioniere è balzato rapidamente in piedi ed è sparito tra gli alberi."


translate italian zhenya_route_d7251a80:



    pi "È andato alla mensa? Che idiota."


translate italian zhenya_route_ce617998_15:



    "…"


translate italian zhenya_route_4526b54e:



    "Il tempo stava scorrendo in modo insidioso, molto lentamente."


translate italian zhenya_route_eb8d24cc:



    "Ho perso il telefono – forse durante il combattimento, o anche prima, quindi non ho potuto dire quanto tempo fosse passato dal momento in cui il pioniere se n'era andato a prendere l'acqua."


translate italian zhenya_route_93a3cf35:



    "Ma in ogni caso avrebbe dovuto già essere qui."


translate italian zhenya_route_0748b652:



    "Le mie ferite mi stanno facendo un male insopportabile, sembra che io stia iniziando ad avere la febbre.{w} Potrebbe trattarsi di un'infezione?"


translate italian zhenya_route_5eb365a4:



    "Potrei semplicemente morire!"


translate italian zhenya_route_8d439469:



    "Almeno avrebbero potuto picchiarmi a morte!"


translate italian zhenya_route_caf72da0:



    "Invece no – devo starmene sdraiato qui ad aspettare la fine in agonia."


translate italian zhenya_route_6c406b5e:



    "Ma portate pazienza!"


translate italian zhenya_route_2f974ad0:



    "La prossima volta vi farò vedere io, eccome se vi farò vedere!"


translate italian zhenya_route_ee043568:



    "La paura della morte ha cessato di esistere per me, semplicemente perché si può provare paura solo verso qualcosa che è spaventoso."


translate italian zhenya_route_3f52e9b2:



    "Nel mio caso la morte sarebbe stata una salvezza."


translate italian zhenya_route_ec79852e:



    "Anche se mi risultava difficile credere in ciò, molto probabilmente la prossima volta tutto si sarebbe ripetuto, il che significava che avrei dovuto lottare, combattere per salvare me stesso e la mia vita!"


translate italian zhenya_route_6b91e2c7:



    "Proprio come le prime volte."


translate italian zhenya_route_1103dcdf:



    "Fare di tutto per non diventare un piagnucolone e moccioso come {i}quell'altro{/i}."


translate italian zhenya_route_3b8c1c5e:



    "Dopo esser riuscito a malapena ad alzarmi in piedi, ho incespicato verso il Campo."


translate italian zhenya_route_dad64e6b:



    "Si stava facendo buio."


translate italian zhenya_route_d6a7f5b9:



    "Probabilmente avrei dovuto chiedere a quel pioniere cos'è accaduto dopo che ho perso conoscenza durante il pestaggio nella piazza."


translate italian zhenya_route_331a0e55:



    "Forse mi stanno cercando?"


translate italian zhenya_route_73be3703:



    "O magari pensano che io sia già morto?"


translate italian zhenya_route_e64dfe95:



    "In ogni caso devo fare attenzione."


translate italian zhenya_route_a32be48c:



    "In questo momento le diverse competenze che ho acquisito durante il tempo trascorso in tutti questi cicli infiniti potrebbero rivelarsi utili."


translate italian zhenya_route_fbda2b81:



    "Ma uno storpio con una sola gamba e un solo braccio difficilmente può essere un buon combattente…"


translate italian zhenya_route_a20cefa7_4:



    "..."


translate italian zhenya_route_df258bf7:



    "A prima vista la piazza sembrava vuota – nessun pioniere, nessun uccellino, nessuno degli scoiattoli che popolavano abbondantemente la foresta."


translate italian zhenya_route_4ceb9268:



    "E un pericoloso silenzio fluttuava nell'aria, come la quiete prima di una battaglia, o come un campo minato circonvallato sia dalle persone che dagli animali."


translate italian zhenya_route_db1b0a8d:



    "Fai un solo passo e esploderai in mille pezzi."


translate italian zhenya_route_1625539c:



    "Se solo fosse davvero così facile…"


translate italian zhenya_route_e4410ee1:



    "Ma non posso stare in piedi e aspettare – devo andare in infermeria, lavare e fasciare le mie ferite, bere un antidolorifico e prendere gli antibiotici."


translate italian zhenya_route_131ab1ed:



    "I primi passi attraverso la piazza sono stati difficili da fare, più per la tensione opprimente che per le mie ferite di battaglia."


translate italian zhenya_route_51b712f7:



    "In qualche modo sono riuscito a trascinarmi attraverso essa, ho camminato lungo lo stretto sentiero, ho raggiunto l'infermeria e ho iniziato a destreggiarmi con la serratura quando all'improvviso ho sentito un fruscio familiare tra i cespugli nelle vicinanze."


translate italian zhenya_route_fcb1e9ab:



    "Certamente non si trattava di una bestia o di un uccellino, era un essere umano!"


translate italian zhenya_route_5b291f33:



    "Anche se ho compreso bene che gli scatenati pionieri non hanno la necessità di nascondersi – adesso agiscono apertamente."


translate italian zhenya_route_d979f39e:



    pi "Non so chi tu sia, nascosto lì dietro, ma faresti meglio ad uscire, altrimenti…"


translate italian zhenya_route_58a3975f:



    "Altrimenti cosa?"


translate italian zhenya_route_7a18852d:



    "In questa condizione faccio fatica persino a rappresentare una minaccia maggiore di quella piccola bimbetta arrogante!"


translate italian zhenya_route_d8883300:



    unp "Sono solo io…"


translate italian zhenya_route_7e66674c:



    "Ha detto la timida pioniera, quasi in un sussurro, ed è uscita dal suo nascondiglio."


translate italian zhenya_route_caa65ec6:



    pi "Oh, sei tu?"


translate italian zhenya_route_621736c0:



    "Ho chiesto con sarcasmo."


translate italian zhenya_route_d3c58e8d:



    pi "Da sola? E dove sono gli altri? Anche se sì, basteresti solo tu…"


translate italian zhenya_route_f45fe05b:



    unp "No, io…{w} Posso andarmene se ti sto disturbando."


translate italian zhenya_route_c572cc9a:



    pi "Rimani pure, le cose non possono andare peggio di così."


translate italian zhenya_route_81356c94:



    "Stavo ancora armeggiando invano con la serratura."


translate italian zhenya_route_85f66ba6:



    "Ovviamente, non sarebbe stato difficile per me scassinarla utilizzando un semplice stuzzicadenti o un fiammifero, ma per fare ciò avevo bisogno di dita sane, mentre le mie erano tutte ricoperte da ematomi e contusioni, e ogni loro articolazione era slogata."


translate italian zhenya_route_ff0038ce:



    "L'azione più complicata che sarei in grado di fare ora è tenere tra le mani una bandiera dei pionieri e agitarla pigramente sopra la mia testa."


translate italian zhenya_route_9cacc36a:



    "Uno stupido passatempo di per sé, ma i pionieri locali sarebbero capaci di farmi fare cose peggiori."


translate italian zhenya_route_fb78244d:



    unp "Forse posso aiutarti?"


translate italian zhenya_route_8f5ff1f3:



    pi "Come? Con il supporto morale?"


translate italian zhenya_route_5ac801c4:



    unp "Io ho una…"


translate italian zhenya_route_c68a86cc:



    "Si è fermata a metà frase e ha tirato fuori un mazzo di chiavi."


translate italian zhenya_route_1537f606:



    pi "Come mai?"


translate italian zhenya_route_92afb189:



    "Sì, non mi aspettavo qualcosa del genere, ma non ne ero molto sorpreso – in quel momento avevo altre cose per la testa."


translate italian zhenya_route_5e6a0cab:



    unp "Le ho prese da Slavya."


translate italian zhenya_route_09d5b397:



    pi "Così su due piedi? Mi stai dicendo che te le ha date senza nemmeno chiederti a cosa ti servissero?"


translate italian zhenya_route_28020918:



    unp "Beh, le ho detto che non mi sentivo bene e che non volevo disturbarla, e che sarei potuta andare all'infermeria da sola."


translate italian zhenya_route_a224ce8d:



    pi "Sì, un bel pretesto. Trucco, inganno e finzione. Si dice che dietro ad ogni uomo forte ci sia una donna forte."


translate italian zhenya_route_9a2c43a2:



    "Mi sono ricordato del secondo pacchetto di «Cosmos» che mi aveva dato il pioniere."


translate italian zhenya_route_bb832dfd:



    "Le sigarette si sono rivelate essere molto sgualcite, ma sono riuscito comunque a trovarne un paio intatte."


translate italian zhenya_route_c56adbe2:



    pi "Ok, le chiavi saranno molto utili, è stato intelligente da parte tua."


translate italian zhenya_route_7f5ee04f:



    unp "Grazie."


translate italian zhenya_route_7346cea3:



    "La ragazza ha sorriso in modo quasi impercettibile."


translate italian zhenya_route_3bfa5470:



    "E se invece non nascondesse un coltello dietro alla schiena...?{w} Da dove provengono certi pensieri, dopotutto?!"


translate italian zhenya_route_4a5fdb28:



    "La presunzione di innocenza non ha mai funzionato in questo mondo fino ad ora, e soprattutto il ritenere ognuno attorno a me ostile {i}a priori{/i} sarebbe la cosa più corretta da fare adesso."


translate italian zhenya_route_bfa3d62d:



    "Ho squadrato la ragazza con attenzione, dalla testa ai piedi."


translate italian zhenya_route_e2bce231:



    unp "Cosa...? C'è qualcosa di sbagliato in me?"


translate italian zhenya_route_f40a37e9:



    pi "Non lo so proprio. È meglio che tu me lo dica – è tutto a posto con te o no?"


translate italian zhenya_route_f6774088:



    unp "Non ti capisco…"


translate italian zhenya_route_6bda23f8:



    "La ragazza pioniere era chiaramente imbarazzata e sconvolta."


translate italian zhenya_route_80fdc055:



    "Oh bene, finché lei finge almeno di essere {i}buona{/i} dovrei approfittarne. Sarà difficile fasciare tutte le mie ferite da solo, con le braccia inflessibili."


translate italian zhenya_route_4280f591:



    pi "Non importa. Se davvero vuoi aiutarmi…"


translate italian zhenya_route_ce617998_16:



    "…"


translate italian zhenya_route_74cd6038:



    "L'infermeria è stata illuminata brillantemente dalle lampade fluorescenti appese al soffitto."


translate italian zhenya_route_ecfceff8:



    "Con qualche difficoltà mi sono arrampicato sul lettino e ho cercato di togliermi la camicia."


translate italian zhenya_route_7aaf2429:



    unp "Lascia che ti aiuti."


translate italian zhenya_route_d7c5f11f:



    pi "Va bene, fa' pure."


translate italian zhenya_route_3df4d52c:



    "Sui lati destro e sinistro della mia pancia c'erano grosse contusioni, dei lividi blu-rossastri terribili."


translate italian zhenya_route_bbafbb40:



    "Devo essermi rotto diverse costole, o forse è il contrario e solo poche costole sono rimaste intatte."


translate italian zhenya_route_6dc32eb8:



    "Ogni singolo movimento ha risuonato internamente con l'eco del dolore:"


translate italian zhenya_route_3f894e04:



    pi "Aaaargh! Non puoi fare più attenzione?"


translate italian zhenya_route_d9ee0e49:



    unp "Scusa."


translate italian zhenya_route_f99441c3:



    "La pioniera ha iniziato a spogliarmi, persino con zelo."


translate italian zhenya_route_47c8c54f:



    "Alla fine sono rimasto in pantaloncini."


translate italian zhenya_route_b9d2f671:



    unp "Aspetta, aspetta, dobbiamo pulire le ferite con lo iodio, disinfettarle!"


translate italian zhenya_route_80692109:



    pi "Sì, certo, in modo da farmi apparire come un orso bruno? No grazie, facciamolo con l'acqua ossigenata – è nel cassetto della scrivania."


translate italian zhenya_route_259d7d24:



    "La pioniera non ha obiettato."


translate italian zhenya_route_edaff1f2:



    "Anche se acqua ossigenata o iodio – bruciano allo stesso modo!"


translate italian zhenya_route_208736c6:



    "Ho dovuto fare uno sforzo disumano per non gridare ad alta voce."


translate italian zhenya_route_e6e5397a:



    unp "Fa male?"


translate italian zhenya_route_62a68b8d:



    pi "Cosa credi?!"


translate italian zhenya_route_b462f311:



    "Ho sibilato a denti stretti."


translate italian zhenya_route_5d12af1f:



    "Poi la pioniera ha cominciato a fasciarmi."


translate italian zhenya_route_c4e8acee:



    "Ogni volta che doveva afferrare la benda dall'altra estremità, era come se lei si stesse premendo su di me."


translate italian zhenya_route_a847a4e2:



    unp "Ecco fatto!"


translate italian zhenya_route_c8bb1a19:



    "Ha detto Lena, con la sensazione di aver fatto un buon lavoro."


translate italian zhenya_route_64313f9e:



    "«Sta gigioneggiando» – tale pensiero mi è balenato nella mente."


translate italian zhenya_route_909f6f6a:



    pi "Sì, così va meglio."


translate italian zhenya_route_6cdbc3d5:



    "Non che mi sentissi meglio – anzi ero semplicemente certo che sarei guarito non prima del giorno seguente."


translate italian zhenya_route_98db5fc7:



    "Ma quello si vedrà domani."


translate italian zhenya_route_ed2b786e:



    "Ho cercato di alzarmi in piedi per raggiungere l'armadietto dei medicinali, ma improvvisamente la mia testa ha iniziato a girare e mi sono dovuto sedere nuovamente sul lettino."


translate italian zhenya_route_4d858608:



    pi "Portami un po' di Analgin, e…{w} e…{w} beh, quel…"


translate italian zhenya_route_521a590f:



    "Ho davvero dimenticato il contenuto del kit di pronto soccorso di questo luogo?"


translate italian zhenya_route_ed40cc8c:



    "Ho appoggiato la mano sulla mia fronte – stava bruciando."


translate italian zhenya_route_cc8f0f79:



    pi "Qualcosa per il tetano, non so. È scritto lì, nelle istruzioni."


translate italian zhenya_route_c61d4c44:



    "La ragazza si è data da fare e ben presto è tornata con un mucchio di pillole multicolore."


translate italian zhenya_route_38a2fca3:



    pi "Cos'è tutto questo?"


translate italian zhenya_route_cb469a4d:



    unp "Quello che hai chiesto…"


translate italian zhenya_route_27722bff:



    "Certamente stavo correndo un rischio, ma ovviamente non poteva andare peggio di così."


translate italian zhenya_route_f93b1c3f:



    pi "E dell'acqua per mandarle giù."


translate italian zhenya_route_1cbefc66:



    "Ho ingoiato le pillole e sono riuscito a malapena a stirarmi sul lettino."


translate italian zhenya_route_7e492c7f:



    "La ragazza pioniere si è seduta sulla sedia lì vicino e ha iniziato a guardarmi pazientemente."


translate italian zhenya_route_45d6e425:



    "Ma cosa vuole, dopotutto?"


translate italian zhenya_route_c822b5dc:



    "La suspense era stancante."


translate italian zhenya_route_434288d8:



    "Volevo solo addormentarmi, andare alla deriva e dimenticarmi di questo pazzo mondo, almeno per un po'.{w} O ancora meglio – svegliarmi in un altro mondo!"


translate italian zhenya_route_58911e68:



    "Lasciare che finiscano quello che hanno iniziato, facendomi fuori!"


translate italian zhenya_route_67aba564:



    "O questa timida ragazza…"


translate italian zhenya_route_0f6e53e3:



    pi "Per quanto tempo hai intenzione di restare seduta qui?"


translate italian zhenya_route_1691f095:



    unp "Se ti sto disturbando, posso andarmene."


translate italian zhenya_route_6e477f3d:



    pi "No, non mi stai disturbando."


translate italian zhenya_route_57115d23:



    "Improvvisamente nella mia testa è apparsa una selvaggia ma non insensata idea!"


translate italian zhenya_route_5857b475:



    pi "Forse potresti aiutarmi…"


translate italian zhenya_route_2cf6cc0c:



    "La ragazza mi ha lanciato uno sguardo interrogativo."


translate italian zhenya_route_f7b1fdd6:



    pi "Prendi quel contenitore, lo vedi?"


translate italian zhenya_route_e2b2bbba:



    "Sono riuscito ad alzare la mano con fatica e ad indicare il farmaco da me richiesto."


translate italian zhenya_route_b0d079f8:



    pi "E ora riempi la siringa."


translate italian zhenya_route_a20cefa7_5:



    "..."


translate italian zhenya_route_30fbce41:



    mz "Stai dicendo sul serio?"


translate italian zhenya_route_46df7f73:



    "Il viso di Zhenya era in fiamme e la sua voce non sembrava sorpresa – ma impaurita!"


translate italian zhenya_route_763ea9a0:



    me "Abbastanza!"


translate italian zhenya_route_f35360cc:



    mz "No, devi essere proprio fuori di testa!"


translate italian zhenya_route_e4748fd7:



    me "Qual è il problema? Anche nel peggiore dei casi il nuovo ciclo sarebbe iniziato entro un paio di giorni, nessuno se lo sarebbe ricordato."


translate italian zhenya_route_bb4cc545:



    mz "Ma io me lo ricorderò di sicuro – questo è davvero troppo!"


translate italian zhenya_route_39419e54:



    me "Quindi la tua risposta è no?"


translate italian zhenya_route_c6cacbe6:



    mz "La mia risposta è – smettila di prenderti gioco di me!"


translate italian zhenya_route_af512106:



    "Ha cercato di alzarsi, ma l'ho afferrata fermamente per un braccio."


translate italian zhenya_route_fe1f361c_2:



    mz "Ehi!"


translate italian zhenya_route_3d06e455:



    "Invece di cercare di liberarsi, Zhenya ha riso e si è stretta a me."


translate italian zhenya_route_96a92f4a:



    mz "Sicuro che non stai scherzando? E se riuscissimo ad andarcene da qui dopotutto?"


translate italian zhenya_route_3b02a616:



    me "È già da un po' che non ne parli."


translate italian zhenya_route_7761d947:



    mz "È vero, ma adesso sì! Se nel mondo reale ti aspetti di poter dimenticare e fingere di non aver detto niente adesso, allora…"


translate italian zhenya_route_d2551cd3:



    me "No, sia qui che nel mondo reale le mie parole sono sincere!"


translate italian zhenya_route_5f3b8e22:



    mz "Anche se fosse vero…{w} È proprio difficile da credere! Così all'improvviso…"


translate italian zhenya_route_0af1865c:



    me "Cosa c'è di così improvviso? Da quanto tempo ci conosciamo? Io è da tempo che ho perso conto dei cicli!"


translate italian zhenya_route_618d05b1:



    mz "Io invece li sto ancora contando…"


translate italian zhenya_route_3129f2d7:



    "Ha detto Zhenya a bassa voce, stringendomi ancora più forte."


translate italian zhenya_route_ce617998_17:



    "…"


translate italian zhenya_route_c4a91b1d:



    "La ragazza ha riempito diligentemente la siringa con il liquido incolore e me l'ha consegnata."


translate italian zhenya_route_0fda54f1:



    pi "Ma no, quando hai visto un paziente farsi le iniezioni da solo?!"


translate italian zhenya_route_27de15a6:



    unp "Ma io non so come..."


translate italian zhenya_route_954d7da0:



    pi "Non c'è niente di difficile."


translate italian zhenya_route_23affce2:



    "È solo che per me in questo mondo c'era ancora un solo e unico tabù – il suicidio."


translate italian zhenya_route_0cffd3d6:



    "Ero pronto a morire (e addirittura lo desideravo) per mano dei folli pionieri o a causa di un'iniezione letale, ma porre fine io stesso alla mia vita…"


translate italian zhenya_route_cee6910e:



    "Per qualche motivo mi sembrava che non avrei dovuto, non saprei nemmeno dire perché, e non ci ho mai provato."


translate italian zhenya_route_7742b1d1:



    "Forse era tutto a causa di quella paura dell'incertezza che mi aveva posseduto nelle ultime ventiquattr'ore, o forse è tutto molto più facile e il motivo è il semplice istinto di autoconservazione."


translate italian zhenya_route_d3f91d78:



    "Indipendentemente da ciò, ero sicuro che se fossi stato ucciso da qualcun altro allora mi sarei svegliato immediatamente in un nuovo ciclo, ma adesso sono pronto a tutto!"


translate italian zhenya_route_be57257f:



    "L'unico problema era il fatto che quella ragazza chiaramente non aveva intenzione di aiutarmi con il mio colpo di grazia volontario."


translate italian zhenya_route_75512fb4:



    unp "No, io…{w} Che succede se faccio qualcosa di sbagliato? È meglio chiamare l'infermiera! Vado a chiamarla, torno subito."


translate italian zhenya_route_f085d7c6:



    "L'infermiera era l'ultima dannata cosa che mi serviva in quel momento!"


translate italian zhenya_route_12645575:



    pi "Aspetta!"


translate italian zhenya_route_58ec471e:



    "Sono riuscito a malapena a sollevarmi su un gomito e l'ho afferrata per un braccio."


translate italian zhenya_route_9f0b1268:



    pi "Non preoccuparti, io l'ho fatto centinaia di volte, e posso insegnarti come si fa! Non c'è niente di difficile!"


translate italian zhenya_route_8d3cf7f8:



    "La ragazza ha esitato."


translate italian zhenya_route_80585789:



    pi "Per prima cosa prendi il laccio emostatico…"


translate italian zhenya_route_150c2823:



    "Anzi aspetta, perché mai dovrebbe servirle il laccio emostatico? Quello viene usato per prelevare il sangue."


translate italian zhenya_route_43e5513f:



    pi "Ok, non prendere il laccio emostatico."


translate italian zhenya_route_dcfbef84:



    "La ragazza pioniere l'ha posato sul tavolo accanto a me."


translate italian zhenya_route_12202aea:



    "Ho spostato la mia mano sinistra di lato per facilitarla nell'impresa."


translate italian zhenya_route_1ce043ab:



    unp "Forse non dovremmo, dopotutto...?"


translate italian zhenya_route_81d5325a:



    pi "Non sentirò proprio alcun dolore."


translate italian zhenya_route_574f5a8b:



    "Le sue mani tremavano, l'ago oscillava in continuazione da un lato all'altro."


translate italian zhenya_route_8ef32db4:



    "Se non riesce a raggiungere la vena non sarà piacevole."


translate italian zhenya_route_5bc3003e:



    "L'eutanasia per mezzo dei farmaci di una cassetta di pronto soccorso sovietica è di dubbio piacere…"


translate italian zhenya_route_4f6d90df:



    unp "Non guardarmi in quel modo."


translate italian zhenya_route_5df5d92e:



    "Mi ha detto improvvisamente la ragazza."


translate italian zhenya_route_1ab9aef2:



    pi "E come ti sto guardando? Non distrarti!"


translate italian zhenya_route_11f22c5a:



    unp "Non riesco a farlo in questo modo…"


translate italian zhenya_route_9b411c3c:



    pi "Oh, ci risiamo…"


translate italian zhenya_route_de7ff5a4:



    "Ho fatto un sospiro impotente e ho strattonato la mia mano allontanandola da lei."


translate italian zhenya_route_be9f07ea:



    pi "Va bene, lasciamo perdere. Mi limiterò a dormire e domani starò meglio. Non dimenticarti di spegnere le luci e chiudi la porta quando esci!"


translate italian zhenya_route_28fb42e2:



    "La ragazza si è diretta con obbedienza verso l'uscita, ma si è fermata a metà strada."


translate italian zhenya_route_d40a0991:



    unp "Stai davvero soffrendo così tanto?"


translate italian zhenya_route_cd157731:



    pi "Sì, davvero! Ma che importa adesso? Mi limiterò a sopportarlo."


translate italian zhenya_route_a609ce96:



    unp "Potrei farlo…{w} Ma solo se chiudi gli occhi."


translate italian zhenya_route_15ac0b0f:



    pi "E quindi se li chiudo, cosa cambierà?"


translate italian zhenya_route_b895e892:



    unp "Sarà più facile per me. Come se tu dormissi."


translate italian zhenya_route_5e2e3ab6:



    "I suoi occhi sorridevano."


translate italian zhenya_route_1c1458d1:



    pi "Va bene, allora li chiudo."


translate italian zhenya_route_2fdb022d:



    "Non sembra essere nulla di spaventoso."


translate italian zhenya_route_9f19ad89:



    "La ragazza si è avvicinata a me e ha preso la siringa."


translate italian zhenya_route_790ba682:



    unp "Allora?"


translate italian zhenya_route_53a2d83d:



    pi "Sì, sì."


translate italian zhenya_route_2ffa5e6c:



    "Ho chiuso gli occhi e nello stesso istante un pensiero mi è balenato nel cervello: perché mi sto fidando di lei così ciecamente?"


translate italian zhenya_route_587bd764:



    "Solo mezz'ora fa, quando stavo piedi sotto il portico, la pensavo molto diversamente!"


translate italian zhenya_route_db889481:



    "Qualcosa mi ha premuto contro il lettino con una forza tale che non mi ha consentito di muovere né le braccia né le gambe.{w} Quella cagna!"


translate italian zhenya_route_00986cf7:



    "La ragazza pioniere si è chinata su di me, con il laccio emostatico tra le mani."


translate italian zhenya_route_2737718a:



    "Un secondo dopo me l'ha stretto attorno al collo."


translate italian zhenya_route_047476be:



    pi "Ma cosa stai…?"


translate italian zhenya_route_34a7e6ca:



    "Ho ansimato."


translate italian zhenya_route_a0fcf8d6:



    "Anche se questa era la morte che le avevo chiesto di concedermi, non doveva essere così, non finendo strangolato con un laccio emostatico! No! Aspetta, non sono pronto!"


translate italian zhenya_route_465ef323:



    "La pioniera stava stringendo sempre più forte, con una folle smorfia sul suo viso: la bocca ghignante e un sorriso diabolico, gli occhi infossati si sono spalancati e addirittura mi è sembrato di scorgere una luce innaturale in essi."


translate italian zhenya_route_ff6a97ff:



    "Ho cercato invano di liberarmi, gli infortuni e la stanchezza me l'hanno impedito."


translate italian zhenya_route_6496742f:



    "E qui comincia l'anossia – tutto inizia a diventare sfocato davanti a me. Chiudo gli occhi, ma l'impronta di quella folle smorfia rimane ancora incisa sul lato interno delle mie palpebre!"


translate italian zhenya_route_65581f39:



    "Al confine della mia mente sento il cigolio di una porta che si apre, la pioniera diabolica allenta la presa e poi scorgo il volto di un'altra ragazza al di sopra di me."


translate italian zhenya_route_b2ada642:



    slp "Andiamo! Subito!"


translate italian zhenya_route_ada3d630:



    "Mi ha sussurrato una voce familiare."


translate italian zhenya_route_9ce82d61:



    "Qualcuno mi ha aiutato ad alzarmi, mi sono appoggiato a quella persona e ci siamo allontanati dall'infermeria."


translate italian zhenya_route_ce617998_18:



    "…"


translate italian zhenya_route_871a5dfa:



    "Camminavamo piano, inciampando spesso e sbattendo contro ogni cosa."


translate italian zhenya_route_f59b944c:



    "Io stavo perdendo conoscenza e riprendendola in continuazione, ma non riuscivo proprio ad alzare la testa. Mi sono limitato a fissare i miei piedi: il selciato di fronte all'infermeria ha lasciato spazio all'asfalto della piazza, e poi ancora alla terra ricoperta di foglie e aghi di abete."


translate italian zhenya_route_5ee85201:



    "Finalmente ci siamo fermati e con difficoltà mi sono lasciato cadere a terra, guardandomi attorno."


translate italian zhenya_route_404bac47:



    "Sembra la stessa radura dove prima mi aveva portato quel pioniere…"


translate italian zhenya_route_39d09361:



    pi "Oggi sto camminando in alcuni ambienti infernali…"


translate italian zhenya_route_b92aee43:



    "Ho provato a sogghignare, ma ho subito tossito; il dorso della mia mano si è ricoperto di sangue."


translate italian zhenya_route_b080400f:



    slp "Ecco qui, bevi."


translate italian zhenya_route_43a9d8cf:



    "La ragazza mi ha consegnato una fiaschetta fredda."


translate italian zhenya_route_9e06cea6:



    "Ho bevuto diversi sorsi e ho guardato il mio salvatore."


translate italian zhenya_route_8a6b8e6c:



    "Sembrava la stessa familiare ragazza pioniere, ma qualcosa non era in sintonia con la sua solita immagine."


translate italian zhenya_route_8cfe0426:



    "Dopotutto, un sorriso sarebbe stato adatto a questa ragazza pioniere più dell'espressione di fredda determinazione che era congelata sul suo viso."


translate italian zhenya_route_c196cab9:



    "Non sapevo neppure cosa dire…"


translate italian zhenya_route_966ef311:



    "Mi sono già sbagliato una volta fidandomi di quella maniaca all'infermeria, concedendomi un momento di riposo, ma adesso dovrò stare molto attento fino alla fine!"


translate italian zhenya_route_ca5f5721:



    "Tuttavia il dolore insopportabile che avvolgeva l'intero mio corpo e la testa che mi girava in continuazione non mi hanno permesso di concentrarmi nemmeno per un istante."


translate italian zhenya_route_631179f6:



    pi "Che cos'è tutto questo…?"


translate italian zhenya_route_9e9814e8:



    slp "Shh, non c'è bisogno di parlare, peggiorerai solo le cose! So già tutto."


translate italian zhenya_route_2e7e3777:



    "Mi ha detto la ragazza, guardandomi attentamente come se, contrariamente alle sue stesse parole, si aspettasse una risposta da parte mia."


translate italian zhenya_route_70c9b40f:



    slp "So tutto di te. In realtà anch'io sono… reale."


translate italian zhenya_route_efee23f6:



    pi "E come diavolo puoi essere reale?! Ci siamo già incontrati ieri, non ho notato proprio niente di {i}reale{/i} in te!"


translate italian zhenya_route_7ac3698b:



    "Ho tossito sangue nuovamente."


translate italian zhenya_route_2513e664:



    pi "È forse un altro dei tuoi scherzi?"


translate italian zhenya_route_b033ae17:



    "Ho proseguito silenziosamente."


translate italian zhenya_route_e83d1d1c:



    pi "Non puoi semplicemente uccidermi? Non ci riesci?! Devi per forza torturarmi, schernirmi?! Vuoi che diventi pazzo?!"


translate italian zhenya_route_e3e89799:



    "E poi mi sono reso conto che tutto questo era già accaduto una volta…{w} Solo che ora i nostri due ruoli si sono invertiti."


translate italian zhenya_route_3e56f2df:



    slp "No, no, non è affatto così! Hai capito male! Non stavi parlando con me ieri. Oh, ma perché cerco di spiegarti le cose, se non riesci nemmeno a capire te stesso!"


translate italian zhenya_route_39ffacba:



    "La ragazza pioniere ha aggrottato la fronte."


translate italian zhenya_route_8b5427ea:



    slp "Non hai mai incontrato le copie di te stesso?"


translate italian zhenya_route_0ae95c2e:



    "C'era sicuramente della logica nelle sue parole, e al momento non riuscivo a trovare alcun difetto in esse."


translate italian zhenya_route_8321ad79:



    pi "Beh, forse…{w} E perché stai facendo tutto questo? Voglio dire, perché mi stai aiutando?"


translate italian zhenya_route_7d3ad2c5:



    slp "Perché tu sei come me, {i}reale{/i}! Non è scontato? Non faresti lo stesso al mio posto?"


translate italian zhenya_route_ec9da8a5:



    "Se solo sapesse quello che ho fatto in passato…"


translate italian zhenya_route_218c2c76:



    "Ma se invece non stesse mentendo?!"


translate italian zhenya_route_b6ba807e:



    "Dopotutto in passato ho già incontrato delle {i}persone{/i} oltre a me in questo mondo!"


translate italian zhenya_route_e309eca4:



    pi "In tal caso, grazie…"


translate italian zhenya_route_a5f76af6:



    "La ragazza non ha risposto nulla, si è limitata a sorridere."


translate italian zhenya_route_ad54904e:



    pi "Ma io continuo a non capire come hai fatto a…{w} Voglio dire, ho incontrato migliaia di volte «te» in passato, e tutte loro non erano altro che dei pupazzi."


translate italian zhenya_route_c3c67cfd:



    slp "È difficile da spiegare, e non c'è tempo adesso!"


translate italian zhenya_route_bf9c30b9:



    pi "Non c'è tempo, sei di fretta per qualcosa?"


translate italian zhenya_route_7bd2bccb:



    slp "Non solo io! Presto riusciremo tutti a fuggire di qui!"


translate italian zhenya_route_794059c2:



    pi "Vuoi dire – da questo ciclo verso un altro?"


translate italian zhenya_route_48a94368:



    slp "No, no! Intendo dire da questo Campo – per sempre!"


translate italian zhenya_route_c6ae4acd:



    pi "Ma che significa...?"


translate italian zhenya_route_ec482ad2:



    "Ho fatto uno sciocco sorriso."


translate italian zhenya_route_3bf95c39:



    "Il solo pensare al fatto che potesse esserci un'uscita da questo luogo mi sembrava assurdo ormai da così tanto tempo..."


translate italian zhenya_route_35c07165:



    slp "Non dirmi che preferisci rimanere qui per sempre?"


translate italian zhenya_route_4faa5838:



    pi "Beh, in un certo senso sono qui da sempre ormai…"


translate italian zhenya_route_c0cdbc2d:



    slp "Io te l'ho proposto – tu rifiuti, va bene."


translate italian zhenya_route_c4dddafe:



    pi "No, aspetta, hai capito male…"


translate italian zhenya_route_895a960c:



    "Anche se, perché «male», in realtà?"


translate italian zhenya_route_4d5b7b29:



    "Sono davvero pronto a tornare nel mondo {i}reale{/i}?"


translate italian zhenya_route_b5a196bc:



    "Ora è estraneo e ostile a me, dato che questo Campo è diventato la mia casa."


translate italian zhenya_route_a6c7a702:



    "Ma quella casa è in rovina – svaligiata e bruciata!"


translate italian zhenya_route_429f73ec:



    "Sarò in grado di costruirne una nuova?"


translate italian zhenya_route_0a336b4a:



    slp "Davvero? Allora cosa vuoi veramente?"


translate italian zhenya_route_108a6cc5:



    pi "D'accordo, ma come? Non pensi sul serio che possa crederti sulla parola, giusto? Se davvero capisci la mia situazione, allora non dovrebbe essere un problema per te spiegarmi…"


translate italian zhenya_route_438e567e:



    slp "Va bene, ma non ora – dobbiamo assicurarci che non ci stiano inseguendo!"


translate italian zhenya_route_a50296a4:



    pi "Aspetta…"


translate italian zhenya_route_79498c62:



    "Ma la pioniera mi ha ignorato, e un istante più tardi è scomparsa nella fitta vegetazione."


translate italian zhenya_route_3d661175:



    "Semplicemente meraviglioso…"


translate italian zhenya_route_2d6f3c2a:



    mz "Ma io non sono capace di cucire!"


translate italian zhenya_route_72ebe9e8:



    me "Che peccato… Una grave svista da parte della nostra governante!"


translate italian zhenya_route_7b17d3b9:



    mz "Oh, senti chi parla! Come hai intenzione di procurarti gli anelli? Stai forse pensando di arrotolarti il filo di una bobina attorno al dito?"


translate italian zhenya_route_a4eed316:



    me "Avrei un paio di idee…"


translate italian zhenya_route_c59c53ff:



    mz "Beh, sto ascoltando."


translate italian zhenya_route_84f14225:



    "Zhenya mi ha guardato con attenzione e ha sorriso, prevedendo il mio fallimento."


translate italian zhenya_route_205070b7:



    "Tuttavia, anche prima di averla incontrata avevo acquisito svariate competenze, e mettere assieme un paio di anelli non sarebbe stato difficile per me."


translate italian zhenya_route_c07bca8e:



    "L'unico problema riguardava l'apparecchiatura – non sarebbe stato possibile trovare degli strumenti di gioielleria in un campo di pionieri."


translate italian zhenya_route_69c44bc3:



    "E così non sarei stato in grado di preparare tutto nel giro di una mera settimana…"


translate italian zhenya_route_1ca203ea:



    "Se non fosse per il circolo di cibernetica!"


translate italian zhenya_route_2551c817:



    "Se si rovistasse tra il mucchio di attrezzature immagazzinate lì dentro sicuramente si potrebbe trovare abbastanza materiale per forgiare due semplici anelli."


translate italian zhenya_route_0866bdde:



    "E tenere occupati i due cibernetici locali per un paio di giorni sicuramente non sarebbe un problema, specialmente considerando il fatto che non è la prima volta che lo faccio!"


translate italian zhenya_route_bb3c0a72:



    me "È una sorpresa!"


translate italian zhenya_route_6091a866:



    mz "Anche se fosse…"


translate italian zhenya_route_b4a932b1:



    "Zhenya si è imbarazzata."


translate italian zhenya_route_b50a6e08:



    mz "Nel giro di una settimana spariranno. Lo sai che non saremo in grado di portare nulla con noi."


translate italian zhenya_route_8d6f2a2a:



    me "E allora? Cosa è più importante per te, due pezzi di metallo o...?"


translate italian zhenya_route_e5b74069:



    mz "No, certo che no…"


translate italian zhenya_route_47c8c285:



    me "Ma se ci accingiamo a farlo nel modo in cui dovrebbe essere fatto, allora anche il vestito poi…"


translate italian zhenya_route_53695934:



    "Gli occhi di Zhenya hanno brillato."


translate italian zhenya_route_05ebce7a:



    "Non c'erano problemi con l'abito, sapevo di per certo che diversi ragazzi nel campo possedevano dei pantaloni e una giacchetta, forse persino di colore grigio scuro."


translate italian zhenya_route_6208daa1:



    "Un abito estivo starebbe bene su Zhenya, ma l'abito da sposa è stata una sua idea dopotutto!{w} Che prevedibilmente aveva rigettato subito."


translate italian zhenya_route_5d04b965:



    me "Indossa il tulle!"


translate italian zhenya_route_6c3efbe4:



    mz "Dici sul serio?"


translate italian zhenya_route_658e4271:



    "Sembra che lei si sia già stancata di discutere."


translate italian zhenya_route_b98d0d5d:



    me "No… Ma puoi sempre chiedere a una delle ragazze! Credo che sia la scelta migliore."


translate italian zhenya_route_c02501ad:



    "Zhenya è diventata rossa, ha messo il broncio e si è voltata."


translate italian zhenya_route_fdf97a8a:



    mz "Allora va' tu a chiederglielo! E prendi le misure di te stesso, dato che sarai tu a doverlo indossare!"


translate italian zhenya_route_d13b52c7:



    mz "Come pensi che io possa fare una cosa del genere, dovrei forse andare da Slavya e chiederle: «Per favore, potresti cucire un abito da sposa per me?»"


translate italian zhenya_route_6b63784c:



    me "E se le dicessi che ti serve a…{w} Per un concorso di costumi!"


translate italian zhenya_route_209d81c1:



    me "No, è un po' stupido…"


translate italian zhenya_route_4e958dc2:



    me "Per un ballo in costume!"


translate italian zhenya_route_6a0bdf61:



    "Zhenya era sorpresa, ma non ha iniziato ad opporsi subito."


translate italian zhenya_route_148134b8:



    me "Ne parlerò con la leader del Campo, non preoccuparti! E non ci sarà niente di cui vergognarsi! Un matrimonio come parte della performance. Faremo eseguire a Shurik e Electronik la prima passeggiata dell'uomo sulla Luna, Miku si vestirà da Lenin in un'automobile blindata…"


translate italian zhenya_route_f1b9bc99:



    me "E noi ci sposeremo!"


translate italian zhenya_route_15cdfe60:



    mz "E a quale evento storico vorresti associare il nostro matrimonio?"


translate italian zhenya_route_33626f7e:



    me "Dio, che importa? Potrebbe anche essere il matrimonio di Caterina II e Pietro III!"


translate italian zhenya_route_341cca1a:



    mz "Oooh, che destino poco invidiabile ti aspetta allora, caro maritino mio!"


translate italian zhenya_route_8796d51d:



    "Zhenya ha sorriso."


translate italian zhenya_route_aea00f45:



    me "Non importa, è solo un esempio! Vedi, stai già facendo battute."


translate italian zhenya_route_323de6e5:



    mz "Eppure mi sembra un po'…"


translate italian zhenya_route_c8e1c467:



    me "Fidati di me!"


translate italian zhenya_route_91039827:



    mz "Va bene, se credi che tutto funzionerà…"


translate italian zhenya_route_ce617998_19:



    "…"


translate italian zhenya_route_333e14d0:



    "L'alba è iniziata, la febbre ha cominciato a farsi sentire e a diventare sempre più intensa."


translate italian zhenya_route_21698494:



    "A quanto pare, la «vera» ragazza pioniere si è persa da qualche parte nella foresta – sembra che io sia stato piantato in asso nuovamente…"


translate italian zhenya_route_4bde977b:



    "Anche se non ci speravo troppo questa volta, volevo solo che tutto tornasse com'era prima, senza il bisogno di tornare alla {i}realtà{/i}."


translate italian zhenya_route_d802c7b0:



    "Forse dall'altra parte ho trascorso solo un'ora, o forse secoli, ormai non ha più alcuna importanza."


translate italian zhenya_route_f694e34b:



    "Mi sento come un uomo in coma che si è svegliato e che non riesce a riconoscere il mondo che lo circonda."


translate italian zhenya_route_18fc7c9f:



    "L'orizzonte bruciava di un brillante bagliore scarlatto, versando nel cielo un'aurora fiammeggiante."


translate italian zhenya_route_f25fa47d:



    "E se non fossero solo gli abitanti ad essere cambiati in questo mondo?"


translate italian zhenya_route_cc66a0fd:



    "Entro pochi minuti i raggi del sole cocente sarebbero arrivati qui e mi avrebbero arso assieme a tutto questo maledetto «Sovyonok»!"


translate italian zhenya_route_f7aa9307:



    "Vorrei che la «vera» ragazza pioniere ritornasse!"


translate italian zhenya_route_a51237ea:



    "È meglio stabilirsi in un mondo {i}nuovo{/i} piuttosto che bruciare vivo in un mondo familiare!"


translate italian zhenya_route_0cac00e1:



    "Tanto più perché nessuno lì saprà mai tutto quello che ho fatto qui!"


translate italian zhenya_route_d3d6bce6:



    "A proposito di tutto quello che ho fatto…"


translate italian zhenya_route_95cdb0f9:



    pi2 "Sembra che tu sia immerso in una meditazione profonda."


translate italian zhenya_route_64f4c9df:



    "Una voce familiare è risuonata nelle vicinanze.{w} La mia voce…"


translate italian zhenya_route_9a21a38f:



    pi "Cosa vuoi?"


translate italian zhenya_route_c8492839:



    "Sono rimasto sorpreso dalla sua apparizione e anche un po' spaventato – ho avuto abbastanza problemi con i pionieri impazziti e avevo smesso persino di pensare ai miei gemelli."


translate italian zhenya_route_dc0d54d6:



    "Anche se, «i miei gemelli»...?{w} Da quanto tempo non li chiamo così?"


translate italian zhenya_route_1fbbac28:



    "Giusto, per me c'ero sempre {i}io{/i} e poi c'erano {i}loro{/i} – non abbiamo nulla in comune, siamo persone diverse, anche se ad un certo punto siamo stati gli stessi…"


translate italian zhenya_route_9f450679:



    pi2 "Forse ti stai chiedendo «quando ha iniziato ad andare tutto storto»?"


translate italian zhenya_route_449bd8e5:



    "E qui si sbagliava – non stavo pensando a quello."


translate italian zhenya_route_43ab8e00:



    pi2 "Tormentandoti di dubbi, facendoti domande alle quali non potrai mai trovare una risposta?"


translate italian zhenya_route_f91eeb18:



    pi "Voglio solo che tutto questo finisca! E la tua presenza di certo non mi è di aiuto a tale scopo!"


translate italian zhenya_route_33691edf:



    pi2 "Tutto è già finito da un pezzo, se proprio lo vuoi sapere…"


translate italian zhenya_route_4a413495:



    pi "Oh ma davvero? Non mi sembra così evidente."


translate italian zhenya_route_2212509c:



    pi2 "Per tutti tranne te! Quel giorno sull'autobus hai preso la decisione sbagliata e sei morto."


translate italian zhenya_route_22952e7f:



    "Ho esaminato il mio corpo con attenzione.{w} Fa male – il che significa che sono vivo!"


translate italian zhenya_route_2d38d332:



    pi "Non credo di sembrare un uomo morto!"


translate italian zhenya_route_8258081a:



    pi2 "Pensi che sia davvero tutto così semplice, come nei film? Vuoi ricordare il momento in cui la vita ti ha abbandonato, ricordi il tuo ultimo respiro? Il punto di transizione? I primi momenti nel nuovo livello di esistenza?"


translate italian zhenya_route_c9d54b47:



    pi "Davvero non ti capisco. Allora significa che io sono già morto, nella {i}realtà{/i}!"


translate italian zhenya_route_92313099:



    pi2 "No! Ti è stata data un'opportunità, ma tu non l'hai colta, e sei finito nel tuo inferno personale, chiamalo limbo se vuoi!"


translate italian zhenya_route_6aee1bdf:



    pi "Oh, grazie per aver reinventato la ruota, come se io non lo sapessi già!"


translate italian zhenya_route_e8ac9853:



    pi2 "Lo sapevi già, ma non sapevi che sei da solo qui dentro, e che non c'è alcuna via d'uscita!"


translate italian zhenya_route_5eb47f22:



    pi "Lo sapevo…"


translate italian zhenya_route_7c4f3efc:



    "O almeno così mi sembrava."


translate italian zhenya_route_4e47c222:



    pi "E allora che dire di te?"


translate italian zhenya_route_4822c2b3:



    pi2 "Io non sono qui! Sono solo una tua allucinazione!"


translate italian zhenya_route_24b2d9d5:



    "La sua risata disgustosa è risuonata nella foresta."


translate italian zhenya_route_0870ec03:



    "Tutto questo è dolorosamente familiare, ma perché si sta ripetendo proprio adesso?"


translate italian zhenya_route_ed1d079e:



    "È vero quando dicono che i guai non vengono mai da soli?"


translate italian zhenya_route_9ec16ea9:



    pi "Fa' come vuoi…"


translate italian zhenya_route_724dc14a:



    "Ho chiuso gli occhi e mi sono persino voltato per sicurezza."


translate italian zhenya_route_997dc7d7:



    pi3 "Non trattarlo così, lui è uno di noi! O almeno, una volta lo era…"


translate italian zhenya_route_bebf711e:



    "La voce familiare è suonata un po' diversa."


translate italian zhenya_route_5b2a770e:



    "Ora due pionieri stavano in piedi nella radura di fronte a me."


translate italian zhenya_route_8f95cdc6:



    pi2 "Bisogna combattere il fuoco con il fuoco!"


translate italian zhenya_route_a6e86815:



    pi3 "Ha già combattuto abbastanza!"


translate italian zhenya_route_c8ef17e5:



    pi2 "Non direi che è ancora abbastanza!"


translate italian zhenya_route_0c2864de:



    "Mentre loro due stavano discutendo, la realtà ha perso di ogni significato per me."


translate italian zhenya_route_2f5de2b0:



    pi3 "Guardalo, ormai è già perso nella sua mente!"


translate italian zhenya_route_044d2365:



    pi2 "Non è niente di speciale, passerà! Come in quella storia che abbiamo raccontato a Ulyana, firmata con il nome di un amico inesistente: «La gente impazzita recupera la ragione, e la gente sana impazzisce»."


translate italian zhenya_route_67371292:



    "E poi di nuovo quella risata diabolica."


translate italian zhenya_route_f0b0ba2b:



    pi2 "Pensi che questa sia la sua prima volta?"


translate italian zhenya_route_102a2a69:



    pi3 "Non lo è?"


translate italian zhenya_route_e27da733:



    pi2 "So di per certo che non lo è! Crede di ricordarsi ogni cosa, di essere pronto ad affrontare ogni situazione, conosce in anticipo il comportamento di ogni singolo pioniere! Ma ha dimenticato così tanto! Forse ha dimenticato la cosa più importante…"


translate italian zhenya_route_c19cedfa:



    uv2 "Ora basta!"


translate italian zhenya_route_c600ee72:



    "Quei due improvvisamente si sono fatti da parte, mentre la ragazza dalle orecchie di gatto si è avvicinata…"


translate italian zhenya_route_cf99867f:



    "In qualche modo ho sempre diffidato di lei, perché pensavo che non ci fosse nulla di buono da aspettarsi da parte sua."


translate italian zhenya_route_ea619637:



    "Inoltre, da qualche parte dentro di me lei mi causava una sorta di paura primordiale e bestiale, come la paura del buio."


translate italian zhenya_route_1bc7b239:



    "E non ho neppure mai creduto alle sue storie!{w} Come se lei fosse il nostro subconscio, la sua parte nascosta!"


translate italian zhenya_route_628e7777:



    "Che sciocchezze!"


translate italian zhenya_route_c0c70154:



    uv2 "Non vedete quanto è messo male? Non sei di alcun aiuto! E nemmeno tu…"


translate italian zhenya_route_9229cca9:



    "Ha puntato il dito contro il pioniere «cattivo»."


translate italian zhenya_route_cd25bf7c:



    uv2 "Va' subito all'infermeria a prendere le pillole!"


translate italian zhenya_route_9f2460f9:



    pi2 "Quali pillole...?"


translate italian zhenya_route_f3f86c57:



    "È rimasto un po' sorpreso."


translate italian zhenya_route_654f75d1:



    uv2 "Non lo so! Non sei tu il più intelligente qui? Pensa a qualcosa!"


translate italian zhenya_route_136154fe:



    "Il pioniere ha sospirato impotente e si è trascinato in direzione del Campo."


translate italian zhenya_route_e04e4c0e:



    uv2 "E tu…"


translate italian zhenya_route_8f4741a0:



    "Si è rivolta al pioniere «buono»."


translate italian zhenya_route_9d637256:



    uv2 "E tu…{w} Beh…{w} Fa' qualcosa anche tu! Sinceramente, le tue lamentele sono nauseanti, non fanno altro che peggiorare le cose!"


translate italian zhenya_route_1187e1a0:



    pi3 "Ma io volevo solo aiutare…"


translate italian zhenya_route_ea0da183:



    uv2 "Ma non stai aiutando! Quindi vai!"


translate italian zhenya_route_151f70d8:



    "La ragazza-gatto ha agitato la mano facendo un'espressione tale che un pensiero mi è apparso in testa – sembrava proprio il Grumpy Cat!"


translate italian zhenya_route_85e516ba:



    "Il pioniere non ha obiettato e ha seguito il suo gemello."


translate italian zhenya_route_5cd1f1b6:



    pi "E adesso cosa?"


translate italian zhenya_route_c0cfa3ab:



    "Ho chiesto senza troppo entusiasmo."


translate italian zhenya_route_6dd0b4ac:



    "Sono stato ingannato più di una volta e non ho intenzione di crederle proprio adesso.{w} Soprattutto a lei!"


translate italian zhenya_route_698655c7:



    "Non avrei mai pensato che di tutti gli abitanti locali sarebbe stata quella dalle orecchie di gatto a venire in mio soccorso."


translate italian zhenya_route_01ee43cb:



    "Anche se solo a parole. Dopotutto, lei si ricorda delle volte precedenti, ricorda il nostro litigio nel bus."


translate italian zhenya_route_4eddc54b:



    pi "Credi forse che potrai prenderti gioco di me nuovamente?"


translate italian zhenya_route_eb468b81:



    "Raccogliendo le mie ultime gocce di forza mi sono sollevato, con la vista oscurata a causa del dolore."


translate italian zhenya_route_968edac1:



    "Ma dove andrò a finire?"


translate italian zhenya_route_5c0580b2:



    "Avrei dovuto farmi quell'iniezione da solo…"


translate italian zhenya_route_7bea4bf4:



    "Perché ho pensato che un suicidio {i}qui{/i} possa essere la fine?"


translate italian zhenya_route_6e9bb08c:



    uv2 "Ehi!"


translate italian zhenya_route_f5f49f8f:



    "La ragazza-gatto è balzata accanto a me e mi ha sostenuto per evitare che cadessi."


translate italian zhenya_route_4991f6ed:



    pi "Tu…{w} Vuoi davvero farmi credere che mi stai aiutando?!"


translate italian zhenya_route_2c0c4fc1:



    uv2 "E cosa pensi che stia facendo in questo momento? Dopotutto io vi ho sempre aiutati, {i}tutti quanti{/i}!"


translate italian zhenya_route_74481647:



    pi "Sì, certo…"


translate italian zhenya_route_b7427c21:



    "Mi sono lasciato cadere nuovamente a terra, facendo un pesante sospiro."


translate italian zhenya_route_de2d25b8:



    "Non c'è proprio alcun luogo dove io possa andare ridotto in questo modo."


translate italian zhenya_route_19691844:



    pi "E così, quando torneranno i tuoi pionieri...?"


translate italian zhenya_route_f0748a5e:



    uv2 "Presto…"


translate italian zhenya_route_0ba6f1f4:



    "Ha risposto vagamente."


translate italian zhenya_route_0a87d3a3:



    pi "Ok, sediamoci, aspettiamo, ormai non ho nulla da perdere…"


translate italian zhenya_route_a20cefa7_6:



    "..."


translate italian zhenya_route_e9c63299:



    "L'intero Campo si è riunito nella piazza."


translate italian zhenya_route_034dc053:



    "Ulyana stava inseguendo Electronik nel vano tentativo di togliergli dalla testa lo sgualcito casco da cosmonauta fatto di cartone."


translate italian zhenya_route_4f376572:



    "La scena della prima passeggiata dell'uomo nello spazio aperto non si è svolta senza alcuni episodi divertenti: il monumento di Genda è servito come un razzo improvvisato per i cibernetici, e Shurik è riuscito ad inciampare e a cadere da esso nel momento più cruciale."


translate italian zhenya_route_48adf945:



    "Per fortuna la distanza dal suolo era significativamente più ridotta rispetto a quella dell'orbita terrestre, e l'impatto con lo sfortunato pioniere è stato proprio come nella canzone – con «il prato vicino a casa»."


translate italian zhenya_route_457c4564:



    mt "Smettetela di correre! Vi ammazzerete!"


translate italian zhenya_route_4bd9c425:



    "Si è lamentata Olga Dmitrievna."


translate italian zhenya_route_b6eca189:



    "Non saprei dire nemmeno cosa le importasse maggiormente in questa situazione – se i cibernetici o l'immagine del Campo che non ha avuto incidenti per 365 giorni l'anno da un tempo immemorabile."


translate italian zhenya_route_0b44a339:



    "Zhenya stava in piedi accanto a me e mi stava aggiustando la manica, ma io ero talmente impegnato ad osservare lo spettacolo che inizialmente non l'ho nemmeno notata."


translate italian zhenya_route_ce73fe66:



    mz "Ebbene!"


translate italian zhenya_route_ad02f53a:



    me "Che cosa?"


translate italian zhenya_route_173b40bd:



    mz "Sei sicuro che dovremmo fare questa cosa?"


translate italian zhenya_route_c62cea6e:



    me "Ma l'hai voluto tu!"


translate italian zhenya_route_5d729a3c:



    mz "Io non ho voluto niente!{w} Tu me l'hai suggerito e io ho accettato…"


translate italian zhenya_route_bbd48daa:



    me "Ne abbiamo già discusso un centinaio di volte. E sai benissimo che è per finta."


translate italian zhenya_route_8dad8ac4:



    mz "Oh, e così è per finta, eh?"


translate italian zhenya_route_cb64fecf:



    "Ha detto severamente."


translate italian zhenya_route_2cf45de0:



    me "Non è quello che intendo dire… Siamo ancora in {i}questo{/i} mondo, non scordartelo. Tra un paio di giorni tutto si ripeterà e nessuno si ricorderà di questo avvenimento."


translate italian zhenya_route_26238f0b:



    mz "Già, ma io sì!"


translate italian zhenya_route_070599c2:



    me "Allora di cosa sei maggiormente imbarazzata? Degli altri attorno a te o di te stessa?"


translate italian zhenya_route_e9fca54d:



    "Zhenya è arrossita senza aggiungere altro."


translate italian zhenya_route_aed58c1a:



    mt "Bene, preparatevi!"


translate italian zhenya_route_40fea5f9:



    "Ci ha comandato Olga Dmitrievna."


translate italian zhenya_route_a752ccba:



    me "Sei pronta?"


translate italian zhenya_route_38c18bf5:



    "Zhenya ha annuito."


translate italian zhenya_route_ce617998_20:



    "…"


translate italian zhenya_route_53d34154:



    "Dieci minuti più tardi si poteva osservare nella piazza uno spettacolo piuttosto strano per un campo di pionieri regolare."


translate italian zhenya_route_a219887e:



    "Oh, ma che dico, era strano persino per «Sovyonok»!"


translate italian zhenya_route_907927ec:



    "E anch'io mi sono sentito strano stando in piedi in mezzo ad una folla di pionieri vestito in un vecchio costume sovietico di seconda mano."


translate italian zhenya_route_3903c48c:



    "Non un formale frac, ovviamente, ma sempre meglio dei pantaloncini e della camicia a maniche corte."


translate italian zhenya_route_941c499b:



    "Mentre Zhenya era vestita di un qualcosa che assomigliava vagamente a un abito da sposa."


translate italian zhenya_route_159608c4:



    "In un'altra situazione avrei detto che sembra proprio ridicola, ma non in questo momento."


translate italian zhenya_route_9d9d5e6c:



    "Adesso quel vestito mi sembra più bello di qualsiasi altro abbigliamento dei negozi di moda pomposi."


translate italian zhenya_route_f29ba8f0:



    mt "Allora, che scena storica state per presentare? Non ricordo più."


translate italian zhenya_route_567dd942:



    "Mi ha sussurrato in un orecchio la leader del Campo."


translate italian zhenya_route_4fdd0140:



    me "Olga Dmitrievna, che importanza ha? Cominciamo!"


translate italian zhenya_route_025b7514:



    mt "Va bene…"


translate italian zhenya_route_de8c4bfc:



    "Zhenya stava in piedi, rossa come un pomodoro, senza sapere dove portare lo sguardo."


translate italian zhenya_route_a876aab3:



    "Ora sembrava provare un forte desiderio di guardarmi."


translate italian zhenya_route_0e1a4307:



    me "Andrà tutto bene."


translate italian zhenya_route_5788b9db:



    "Le ho detto piano."


translate italian zhenya_route_ff4fa4d8:



    "Miku sorrideva allegramente tra la folla di pionieri, come se si trattasse del suo stesso matrimonio – un po' sciocco forse, ma molto sincero."


translate italian zhenya_route_3c5ef511:



    "Lena è arrossita come al solito, ma di tanto in tanto lanciava qualche occhiata curiosa in nostra direzione."


translate italian zhenya_route_d006862e:



    "L'espressione di Alisa ha cercato in tutti i modi di far capire che era stata trascinata qui contro la sua volontà, e che avrebbe preferito suonare la chitarra o dormire, se solo avesse potuto."


translate italian zhenya_route_7b0dcbe4:



    "Invece Ulyana era chiaramente entusiasta di tutto questo spettacolo: ai ragazzini basta poco per sentirsi felici."


translate italian zhenya_route_002294ae:



    "In quanto a Slavya, sembrava essere davvero contenta per noi e sorrideva gentilmente."


translate italian zhenya_route_fc45d68b:



    mz "Dai, cerchiamo di finire in fretta."


translate italian zhenya_route_337d23ad:



    "Zhenya sembrava imbarazzata a morte."


translate italian zhenya_route_01822020:



    me "Va bene."


translate italian zhenya_route_280f303c:



    me "Siamo pronti."


translate italian zhenya_route_ab16cb35:



    "Ho fatto un cenno alla leader del Campo."


translate italian zhenya_route_27d5273b:



    "Ora ho dovuto recitare quello che solitamente si recita ai matrimoni."


translate italian zhenya_route_daa4ab51:



    "Ovviamente non conoscevo le parole esatte, e sono riuscito a dimenticare tranquillamente tutto quello che avevo preparato in anticipo."


translate italian zhenya_route_4208fbed:



    me "Prometto di…"


translate italian zhenya_route_9eb7ae3d:



    "Ho iniziato, con la voce tremante."


translate italian zhenya_route_145c5153:



    me "Nella salute e nella vecchiaia…"


translate italian zhenya_route_100598bd:



    "No, non era esattamente così."


translate italian zhenya_route_32f2392a:



    us "Cosa stai borbottando! Non riusciamo a sentire niente!"


translate italian zhenya_route_1770bf88:



    "Ha gridato allegramente Ulyana."


translate italian zhenya_route_2676b165:



    dv "Zitta tu, o ti metteremo al posto di Zhenya."


translate italian zhenya_route_8155be36:



    "La minaccia ha avuto un effetto immediato e Ulyana ha taciuto."


translate italian zhenya_route_90d25991:



    me "Beh, è…"


translate italian zhenya_route_3b342837:



    "Solo un minuto fa avrei giurato che fossi assolutamente tranquillo, mentre invece adesso non riuscivo a pronunciare una singola parola."


translate italian zhenya_route_cca9bafd:



    "Potevo solo immaginare cosa provasse Zhenya."


translate italian zhenya_route_1e527a61:



    "Ho preso coraggio."


translate italian zhenya_route_1d086fbd:



    me "Prometto di stare al tuo fianco nella gioia e nel dolore, in ricchezza e in povertà, in salute e in malattia, finché morte non ci separi!"


translate italian zhenya_route_7aaa9384:



    "I pionieri che fino a quel momento stavano rumoreggiando a gran voce, hanno smesso di parlare immediatamente."


translate italian zhenya_route_b4112c36:



    "Si è fatto tutto così silenzioso che riuscivo persino a sentire il mio cuore battere all'impazzata."


translate italian zhenya_route_6d361550:



    "Zhenya era ancora in piedi, con lo sguardo rivolto verso il basso."


translate italian zhenya_route_5d3c27fa:



    me "Bene, adesso tocca a te!"


translate italian zhenya_route_7b26ab8b:



    "Ha riempito d'aria i suoi polmoni e ha farfugliato:"


translate italian zhenya_route_7f141d67:



    mz "Accetto di sposarti, prometto di stare con te malata e povera. Ecco!"


translate italian zhenya_route_5c93db98:



    "La folla è scoppiata a ridere."


translate italian zhenya_route_f65f12c2:



    "Anche se per me le parole di Zhenya sono suonate piuttosto serie."


translate italian zhenya_route_fde7233b:



    mz "Dai, finiamola."


translate italian zhenya_route_e0f4c4c3:



    me "Che ne dici di un «adesso puoi baciare la sposa»?"


translate italian zhenya_route_1a014c13:



    "Ho chiesto in tono scherzoso."


translate italian zhenya_route_8492c157:



    "Gli occhi di Zhenya hanno brillato, mettendo in chiaro che quella situazione non necessitasse di alcun bacio."


translate italian zhenya_route_c8b5fcd7:



    "Credo che lei abbia ragione, avremo tempo per quello in ogni caso."


translate italian zhenya_route_a718aa85:



    mt "Bel lavoro! Grazie a tutti!"


translate italian zhenya_route_f52b105b:



    "Ha gridato Olga Dmitrievna. Electronik si è fatto avanti con una macchina fotografica."


translate italian zhenya_route_4f2d1d7d:



    "Era come se preferisse tenere un piede di porco tra le mani."


translate italian zhenya_route_6ddf4f60:



    el "Un paio di foto ricordo…"


translate italian zhenya_route_e70a6fe5:



    mz "Diamine, no!"


translate italian zhenya_route_a312257a:



    "Gli ha abbaiato Zhenya."


translate italian zhenya_route_1b473258:



    me "Dai – per l'album di famiglia."


translate italian zhenya_route_b402f943:



    "Ha esitato un po', ma alla fine ha fatto una specie di sorriso."


translate italian zhenya_route_cd93bb8a:



    me "Tutto si è rivelato come avrebbe dovuto essere! Anche meglio!"


translate italian zhenya_route_cd8ad1b8:



    "Per la prima volta in tutta questa situazione Zhenya mi ha guardato dritto negli occhi, ha sorriso e mi ha detto:"


translate italian zhenya_route_bcd11e4b:



    mz "Già, suppongo."


translate italian zhenya_route_ce617998_21:



    "…"


translate italian zhenya_route_0852e54f:



    "Mi sono svegliato quando la giornata stava già volgendo al suo termine."


translate italian zhenya_route_0bb1d15b:



    "La febbre si è abbassata, e ho notato un segno di puntura sul mio braccio destro."


translate italian zhenya_route_532743cd:



    "Forse quella dalle orecchie di gatto mi ha aiutato davvero...?"


translate italian zhenya_route_5ca624bb:



    "All'inizio non potevo credere che qui ci fosse qualcuno capace di compiere una buona azione, dopo tutto quello che era successo negli ultimi giorni."


translate italian zhenya_route_762d0ac9:



    "Con qualche difficoltà mi sono alzato e mi sono diretto verso il Campo."


translate italian zhenya_route_7dac7415:



    "Comunque non ho proprio niente da fare nella foresta, e non sembra che la situazione possa andare peggio di così."


translate italian zhenya_route_67da5e6b:



    "Di tanto in tanto un pensiero mi affiorava nella mente – devo sopportare, devo riuscire a sopravvivere a questa settimana, e poi sarà tutto più facile. Mi riprenderò fisicamente e ci sarà addirittura del tempo per riprendersi psicologicamente."


translate italian zhenya_route_d272d59d:



    "Basta non disperarsi, non mollare, non essere come {i}lui{/i}!"


translate italian zhenya_route_85b36826:



    "Preferirei morire che vivere così!"


translate italian zhenya_route_a20cefa7_7:



    "..."


translate italian zhenya_route_3a421f90:



    "Alla piazza c'era un continuo viavai di pionieri che trasportavano zaini, borse e pacchetti."


translate italian zhenya_route_cb975bdd:



    "La leader del Campo stava in piedi vicino a Genda e sembrava un piccolo monumento, in posa Akimbo e gridando con soggezione."


translate italian zhenya_route_7b2c05b0:



    "Mi sono bloccato sui miei passi, non sapendo cosa aspettarmi."


translate italian zhenya_route_f64d961c:



    "{i}Questo{/i} mondo è in continuo movimento, si tramuta dinanzi ai miei occhi: proprio ieri erano pronti ad uccidermi, e che dire di oggi?"


translate italian zhenya_route_f711d58a:



    mtp "Ehi, Semyon!"


translate italian zhenya_route_303ef93d:



    "Ha gridato la leader del Campo, guardandomi in modo minaccioso."


translate italian zhenya_route_058c857c:



    pi "Chi, io? Stai parlando con me?"


translate italian zhenya_route_98ac5311:



    mtp "Perché non sei ancora pronto?"


translate italian zhenya_route_f8ea68a4:



    pi "Pronto? Già, sono proprio pronto per andarmene…"


translate italian zhenya_route_5268cef9:



    mtp "Oggi è l'ultimo giorno della sessione! Te lo sei scordato di nuovo?! Preparati, presto!"


translate italian zhenya_route_4267b07b:



    "Non avevo voglia di discutere con lei – semplicemente mi mancavano le forze – ho smesso di essere sorpreso molto tempo fa, così ho semplicemente sbottato:"


translate italian zhenya_route_6c4fab7d:



    pi "È un problema se ci vado così?"


translate italian zhenya_route_e9b3d6f4:



    "E ho indicato i miei vestiti strappati e i lividi di cui ero ricoperto."


translate italian zhenya_route_4eb3fa9a:



    mtp "Cosa ti è successo?"


translate italian zhenya_route_ee50b2d9:



    "Ha chiesto con sincerità la leader del Campo."


translate italian zhenya_route_3c6b857f:



    mtp "Stavi correndo nella foresta e sei caduto?"


translate italian zhenya_route_077afa71:



    pi "Ma di che diavolo stai parlando? Quale foresta? Perché mai dovrei aver bisogno di correre attraverso la foresta?"


translate italian zhenya_route_64416fdd:



    mtp "Va bene, non sono affari miei… Tra mezz'ora si parte, se sarai di nuovo in ritardo ti lasceremo qui per un'altra sessione!"


translate italian zhenya_route_fde537c1:



    "Ha sorriso."


translate italian zhenya_route_a5626efa:



    "«Restare qui per un'altra sessione»…"


translate italian zhenya_route_7cbd0db3:



    "No, questo decisamente non rientra nei miei piani!"


translate italian zhenya_route_8cc96c2a:



    "Ad ogni modo, è apparsa una nuova possibilità di andarmene da questo luogo, seppur in modo così stupido!"


translate italian zhenya_route_af3b5ab5:



    "Mi sono trascinato lentamente verso la fermata del bus."


translate italian zhenya_route_ad26bb5a:



    "L'intero Campo era radunato di fronte all'autobus."


translate italian zhenya_route_a27f23dc:



    "Tutti stavano aspettando la leader del Campo – dopotutto, seguire le proprie regole non fa parte delle regole."


translate italian zhenya_route_37559c26:



    unp "Oh, cosa ti è successo...?"


translate italian zhenya_route_9673889c:



    "Mi ha chiesto maternamente la pioniera dell'infermeria."


translate italian zhenya_route_0b9afc20:



    slp "Stai bene? Hai bisogno di una benda?"


translate italian zhenya_route_dd4b010c:



    "Perché limitarvi a quello, offritemi una stecca!"


translate italian zhenya_route_3f25e7ab:



    dvp "Dev'essere caduto in un fosso frignando come una fontana."


translate italian zhenya_route_662ce80f:



    "Certo, senti chi parla…"


translate italian zhenya_route_2872693b:



    usp "Forse l'hanno attaccato i lupi!"


translate italian zhenya_route_12528bfb:



    "Solo se per lupi intendi i pionieri locali…"


translate italian zhenya_route_222786fb:



    "Me ne stavo seduto tranquillamente sul ciglio della strada, con la testa appoggiata sulle mie mani."


translate italian zhenya_route_aef7bae3:



    "Non ho nemmeno voglia di pensare a quello che accadrà domani, sia che inizierà un nuovo ciclo o che ogni cosa finirà una volta per tutte."


translate italian zhenya_route_cfa4c35a:



    "O forse c'è qualcos'altro che mi aspetta in futuro, qualcosa…{w} di migliore…?"


translate italian zhenya_route_ce617998_22:



    "…"


translate italian zhenya_route_cae10f7f:



    "Quando mi sono svegliato Zhenya non era più nelle vicinanze."


translate italian zhenya_route_65b807df:



    "Dev'essere andata ad occuparsi di alcune faccende personali."


translate italian zhenya_route_f9aba6a3:



    "Dio, perché lei ha bisogno di tutto questo gioco di pionieri?!"


translate italian zhenya_route_90bc68a6:



    "Soprattutto la notte di nozze!"


translate italian zhenya_route_9a8da883:



    "Mi sono stirato e ho sbadigliato con piacere."


translate italian zhenya_route_9d92e2de:



    "Oh beh, almeno posso farle una sorpresa – raccogliere dei fiori nella foresta."


translate italian zhenya_route_9b419756:



    "Non è molto come dono, ma so che le piacciono i mughetti."


translate italian zhenya_route_523c3f6b:



    "Non ho mai capito perché alle donne piacciono i fiori…"


translate italian zhenya_route_d823504c:



    "Perché non le ortiche o il pànace allora?"


translate italian zhenya_route_04a18347:



    "Anche quelle sono piante, e stanno bene nel loro habitat naturale, e non in bacinelle o vasetti."


translate italian zhenya_route_206496b3:



    "Anche se, che importa di tutto questo, se tutte le volte che io dono a Zhenya dei fiori, il suo viso si illumina di un sorriso raggiante?"


translate italian zhenya_route_1919fcf8:



    "Certo, forse non è solo per i fiori in sé…"


translate italian zhenya_route_f7f98ab4:



    "Con un mazzo di fiori in mano mi sono presentato nella biblioteca, spalancando rumorosamente la porta."


translate italian zhenya_route_d823e1d6:



    "Dietro alla scrivania della bibliotecaria era seduta Alisa, che stava sfogliando pigramente una rivista."


translate italian zhenya_route_4bc48b58:



    dv "Oh, lo sposino si è fatto vivo."


translate italian zhenya_route_38e73ed3:



    "Ha mormorato, guardandomi in modo accigliato."


translate italian zhenya_route_b3705464:



    me "Ehi, dov'è Zhenya...?"


translate italian zhenya_route_b20b9d5a:



    dv "Non lo so. Mi ha chiesto di sostituirla qui e se n'è andata per i fatti suoi."


translate italian zhenya_route_9edc69e2:



    "Ebbene, sì, la mia Zhenya riesce a persuadere persino Alisa."


translate italian zhenya_route_21e4d285:



    me "Va bene, allora io…"


translate italian zhenya_route_59986aee:



    dv "E per qualche ragione non mi è sembrata affatto felice."


translate italian zhenya_route_23858456:



    "Mi ha detto Alisa alle mie spalle."


translate italian zhenya_route_6b3f4d60:



    "Dov'è andata Zhenya?"


translate italian zhenya_route_48ccafa2:



    "Forse anche lei ha deciso di farmi una sorpresa, un regalo?"


translate italian zhenya_route_c1260382:



    "No, le sorprese non sono da lei, probabilmente è semplicemente andata a fare colazione."


translate italian zhenya_route_aa30435e:



    "Mi sono incamminato, anzi, sono corso verso la mensa."


translate italian zhenya_route_b11e70d8:



    "I pionieri stavano chiacchierando allegramente, i piatti risuonavano rumorosamente allo stesso modo, persino la malinconica cuoca oggi sembrava insolitamente briosa."


translate italian zhenya_route_0b93c284:



    "Ho notato Slavya e sono andato da lei."


translate italian zhenya_route_94ff2553:



    me "Ciao, hai visto Zhenya?"


translate italian zhenya_route_a850b845:



    "Slavya mi ha guardato con sorpresa, poi si è imbarazzata e ha distolto lo sguardo."


translate italian zhenya_route_ddb1c9c0:



    sl "Credo che sia andata alla fermata dell'autobus."


translate italian zhenya_route_89d4ed9c:



    me "La fermata dell'autobus? Perché?"


translate italian zhenya_route_16a22502:



    sl "Non lo so…{w} Dovresti chiederglielo tu stesso. Forse sei ancora in tempo, se ti sbrighi."


translate italian zhenya_route_2ef037d1:



    "Alla fermata dell'autobus? Alla fermata dell'autobus!"


translate italian zhenya_route_27f648fd:



    "Entro un minuto mi trovavo già oltre il cancello del Campo…"


translate italian zhenya_route_1baec8fb:



    "E ho visto Zhenya in piedi sulla strada, il suo sguardo era rivolto verso qualche luogo lontano all'orizzonte, oltre le pianure e le foreste, seguendo il luminoso sole estivo."


translate italian zhenya_route_f15d9551:



    "Una strana espressione era stampata sul suo viso: tristezza, impazienza ed angoscia, tutte mescolate tra loro, ma allo stesso tempo Zhenya sembrava così diversa…{w} Non l'ho riconosciuta."


translate italian zhenya_route_581fe4cb:



    "Migliore? No, è ancora più improbabile dell'eventualità che io domani possa riuscire a ritornare nel mondo {i}reale{/i}."


translate italian zhenya_route_d91dba14:



    "Forse era meglio prima?"


translate italian zhenya_route_924ac1c6:



    "Sembra che quel pioniere mi abbia detto che ho dimenticato un sacco di cose…"


translate italian zhenya_route_95e3c63e:



    "Ma che cosa esattamente?"


translate italian zhenya_route_867fc00d:



    me "Ehi, ti stavo cercando."


translate italian zhenya_route_8e4d71a5:



    "Mi è sembrato sciocco, e la mia voce tremava – ma dovevo dire qualcosa, così ho fatto un falso sorriso, cercando di agire in modo amichevole."


translate italian zhenya_route_8df9005a:



    mz "Cosa vuoi?"


translate italian zhenya_route_ac521edd:



    "Mi ha risposto freddamente Zhenya, senza voltarsi."


translate italian zhenya_route_fba62ae7:



    me "Mi sono svegliato, e tu non c'eri, e così…"


translate italian zhenya_route_ff81478a:



    "Zhenya si è voltata, con gli occhi che brillavano."


translate italian zhenya_route_01ebb44b:



    mz "Me ne vado."


translate italian zhenya_route_f2dc887f:



    "Mi ha detto in tono calmo."


translate italian zhenya_route_25f7abbc:



    me "Dove?"


translate italian zhenya_route_86c8528c:



    mz "A casa."


translate italian zhenya_route_fb5e4a90:



    me "Ma gli autobus non passano di qui…"


translate italian zhenya_route_e03488f6:



    "Gli autobus? Che diamine."


translate italian zhenya_route_75e90b09:



    mz "Sì che passano."


translate italian zhenya_route_93c27914:



    me "Ecco, ho preso questi per te…"


translate italian zhenya_route_2fab920e:



    "Le ho consegnato il mazzo di fiori con le mani che mi tremavano, e immediatamente sono finiti per terra."


translate italian zhenya_route_293d1e5d:



    mz "Non voglio niente da te! Tu, ieri… Non voglio conoscerti! Non so cosa sia successo, ma so che è stata tutta colpa tua!"


translate italian zhenya_route_66e6b111:



    me "Cosa è stata colpa mia?"


translate italian zhenya_route_40de19bd:



    mz "Ogni cosa! Quello stupido matrimonio, ciò che è accaduto durante la notte…"


translate italian zhenya_route_e218d65f:



    me "Ma eravamo d'accordo… Tu stessa lo volevi! Te lo ricordi, un paio di settimane fa?"


translate italian zhenya_route_f22f02db:



    mz "Non ti conoscevo nemmeno un paio di settimane fa!"


translate italian zhenya_route_0afb7a8c:



    "Già, tutto era iniziato in quel momento."


translate italian zhenya_route_89eaae80:



    "O è {i}continuato{/i} – Ormai non importa!"


translate italian zhenya_route_8de561c3:



    "Ho deciso che avrei potuto essere felice con Zhenya, ma lei è scomparsa così come era apparsa – all'improvviso."


translate italian zhenya_route_47016d9b:



    "{i}Zhenya, che stava seduta non nella biblioteca ma sulla veranda della mensa{/i} – è stato così tanto tempo fa…"


translate italian zhenya_route_a49f0e42:



    "Ho perso la speranza una volta, ho smesso di essere umano, ma poi ho incontrato lei ed era come se la vita mi avesse dato una seconda opportunità, nonostante mi trovassi in questo strano mondo."


translate italian zhenya_route_e96d834d:



    "Ma poi lei se n'è andata, e durante gli infiniti cicli ho scordato tutto. Sono tornato lo stesso di prima, come ero quando la incontrai per la prima volta. Ho smesso di essere me stesso…"


translate italian zhenya_route_8209fb60:



    "Forse {i}lui{/i} aveva ragione...?"


translate italian zhenya_route_713b6223:



    me "Ma che stai dicendo…? Ci conosciamo da…{w} Non lo so nemmeno, da quanto ci conosciamo!"


translate italian zhenya_route_53223da6:



    mz "Io non ti conosco affatto, e non ho intenzione di conoscerti!"


translate italian zhenya_route_5dc1718e:



    me "Ma aspetta, noi…"


translate italian zhenya_route_4892f21d:



    "Improvvisamente sono stato sopraffatto dalla debolezza, e mi sono lasciato cadere a terra in ginocchio, solo per evitare di sbattere la faccia sull'asfalto."


translate italian zhenya_route_2cc03992:



    "Il mio anello era molto caldo sul mio dito. Riuscivo quasi a sentirlo bruciare."


translate italian zhenya_route_4d5dd590:



    mz "Ecco qui, prendilo!"


translate italian zhenya_route_66517b70:



    "Zhenya ha tolto qualcosa dalla sua tasca e me l'ha lanciato."


translate italian zhenya_route_35c23299:



    mz "Io vado a fare una passeggiata!"


translate italian zhenya_route_f4572a11:



    "Ho rigirato il sottile anello di fidanzamento tra le mie mani e ho iniziato a piangere."


translate italian zhenya_route_f6b5b83d:



    "Non ho mai pianto così tanto prima d'ora – non in questo mondo, e neppure in quello precedente…"


translate italian zhenya_route_11c27875:



    "E poi è tutto finito."


translate italian zhenya_route_2b610b28:



    "Per sempre."


translate italian zhenya_route_8c05f068:



    "Credo che qualcosa di umano fosse ancora rimasto in me, e così ho deciso di dimenticare, solo per non rischiare di impazzire del tutto."


translate italian zhenya_route_2a64dbfe:



    "Ma ora tutti i miei ricordi sono riemersi, e con essi è ritornato il dolore – non è più così forte, dato che si è indebolito col tempo, ma mi sta comunque lacerando dentro."


translate italian zhenya_route_f7d9e51c:



    "Come se fossi stato fatto a pezzi, e l'unico frammento rimasto di me fosse quello contenente la mia coscienza."


translate italian zhenya_route_4f7b01ff:



    "Non uno, non mezzo, ma solo una triste coincidenza, un errore accettabile, nella perfetta equazione di questo mondo."


translate italian zhenya_route_302b08db:



    pi4 "Non essere così triste, fratello!"


translate italian zhenya_route_58fabdec:



    "Alcuni pionieri stavano seduti accanto a me."


translate italian zhenya_route_90087846:



    pi "Di nuovo tu?"


translate italian zhenya_route_4f5d7a29:



    pi4 "Io chi?"


translate italian zhenya_route_00129a4d:



    pi "Che differenza fa? Tu, lui, quell'altro, questo qui – siete tutti uguali!"


translate italian zhenya_route_4c2f272f:



    "Non ha risposto.{w} Si è limitato a ghignare."


translate italian zhenya_route_10c3756d:



    pi "Sei venuto qui per prendermi in giro di nuovo? Sarebbe stato meglio se mi avessi ucciso quella volta."


translate italian zhenya_route_32babbd7:



    pi4 "Ma ti sei ricordato tutto."


translate italian zhenya_route_cce9bc51:



    pi "Sì, mi sono ricordato! Ed è ancora peggio adesso!"


translate italian zhenya_route_56a872ce:



    pi4 "Sei tu l'unico da biasimare."


translate italian zhenya_route_a3eb2c09:



    pi "Da biasimare per cosa? Per aver cercato di impedirti di andartene con quella dalle orecchie di gatto? So che eri tu su quel bus!"


translate italian zhenya_route_a2bffe06:



    "Sul volto del pioniere non c'era ancora alcuna traccia di emozione."


translate italian zhenya_route_6e23c048:



    pi4 "Forse ero io, forse no – che differenza fa adesso?"


translate italian zhenya_route_2b5b71d1:



    pi "Questo significa che tu…{w} sei riuscito ad andartene?"


translate italian zhenya_route_71961912:



    pi "E sei tornato indietro solo per prenderti gioco di me?"


translate italian zhenya_route_e1828c1c:



    pi4 "Tu non hai forse fatto lo stesso?"


translate italian zhenya_route_9ebc18c6:



    pi "Smettila di farmi la morale, per l'amor di Dio!"


translate italian zhenya_route_d6d21cef:



    "Ero in ginocchio guardando Zhenya, che si stava allontanando sempre più."


translate italian zhenya_route_b7251b6d:



    "E poi è sparita dietro ad un angolo. Non tornerà mai più…"


translate italian zhenya_route_28c9f488:



    "Comunque, sapevo che quella non era Zhenya – la mia vera Zhenya era scomparsa ieri, questa era solo una bambola senz'anima che assomigliava a lei.{w} Ma le assomigliava davvero…"


translate italian zhenya_route_03273ca5:



    "Il racconto non può durare per sempre!"


translate italian zhenya_route_c7744369:



    "Questo mondo è un vero e proprio inferno, e io ne sono il demone.{w} E un demone non potrà mai conoscere la vera felicità..."


translate italian zhenya_route_e23c07ec:



    pi "Dimmi solo una cosa."


translate italian zhenya_route_330cac58:



    "Alla fine ho rotto il silenzio."


translate italian zhenya_route_d964d736:



    pi "Perché tutto questo?"


translate italian zhenya_route_64fb9aaa:



    pi4 "Ti è stata data una possibilità sin dal primo momento, una scelta – avresti potuto fare tutto diversamente. Molti hanno fatto la scelta giusta – a differenza di te, e di altri."


translate italian zhenya_route_a49737fe:



    pi "Sì, come no. Tutti sono riusciti ad andarsene e adesso vivono la loro vita felici con le loro bamboline nel mondo {i}reale{/i}."


translate italian zhenya_route_fbd62a8b:



    "Ho ghignato."


translate italian zhenya_route_0d254476:



    pi4 "Credi forse che la tua vita sia migliore?"


translate italian zhenya_route_89deef73:



    "Mi ha chiesto, sottolineando la parola «vita»."


translate italian zhenya_route_d8d4112a:



    pi "Se sei stato tu…{w} Se tu sei colui che mi ha portato via Zhenya…"


translate italian zhenya_route_46c6c1d3:



    "La rabbia stava crescendo all'interno di me, avrei voluto saltargli addosso e picchiarlo a morte!"


translate italian zhenya_route_fe54ef92:



    "Ma non ero sicuro che avesse forma fisica."


translate italian zhenya_route_a15e6140:



    "Anche se fosse, cosa cambierebbe?"


translate italian zhenya_route_523cc556:



    pi4 "Avresti potuto essere felice con ogni altra ragazza, è questo il motivo per cui sono tutte qui."


translate italian zhenya_route_a68dd09f:



    "Non mi aspettavo una risposta del genere."


translate italian zhenya_route_ac729750:



    pi "Ma nessuna di esse è reale! Le uniche vere persone qui siamo io… tu… tutti noi…{w} Zhenya…"


translate italian zhenya_route_e55f7952:



    pi4 "Questo vale solo per te, nel {i}tuo mondo{/i}. Sai che esistono realtà multiple e in ognuna di esse puoi scegliere il tuo destino. Forse è esistito un solo Semyon, ma questo Campo gli ha dato varie possibilità."


translate italian zhenya_route_0248499f:



    pi4 "Qualcuno è riuscito ad andarsene e adesso vive felicemente, come hai detto. Qualcun altro – è bloccato, come te."


translate italian zhenya_route_ab00125a:



    pi "Puoi dirmi che posto è questo? Chi sei tu?"


translate italian zhenya_route_755b6861:



    pi4 "Puoi considerarmi il leader del Campo."


translate italian zhenya_route_80725814:



    "Il pioniere ha sorriso senza alcuna traccia di scherno o ironia – e poi è scomparso."


translate italian zhenya_route_45f9e91b:



    "Mi sono sentito un po' più sollevato dopo aver ascoltato quelle sue parole…"


translate italian zhenya_route_2ce94d4c:



    "Il mondo è stato improvvisamente avvolto dalla nebbia, e un bagliore luminoso è apparso di fronte a me."


translate italian zhenya_route_9c6336fb:



    "Mi sono guardato intorno e mi sono reso conto che i pionieri erano spariti, così come l'autobus."


translate italian zhenya_route_7fbcf6f6:



    "Che altro sta succedendo?"


translate italian zhenya_route_48843b81:



    "Il cancello ha scricchiolato silenziosamente, e mi sono nascosto istintivamente dietro al monumento del pioniere."


translate italian zhenya_route_66517463:



    "Semyon si è avvicinato alla fermata dell'autobus – uno dei tanti Semyon, ma il suo volto era dolorosamente familiare."


translate italian zhenya_route_07e4236c:



    "Ormai mi sono scordato da tempo che io potrei sembrare esattamente così."


translate italian zhenya_route_cb5e1464:



    "Ha fissato la strada per un po', e si è incamminato nuovamente verso il Campo."


translate italian zhenya_route_2016aa78:



    pi "Non fidarti di lui."


translate italian zhenya_route_2eea460b:



    "Gli ho detto istintivamente."


translate italian zhenya_route_0a2309cb:



    me "E tu chi sei?!"


translate italian zhenya_route_3e4dff80:



    "Semyon sembrava sorpreso."


translate italian zhenya_route_d6271c95:



    pi "Fermo! Non fare un altro passo!"


translate italian zhenya_route_c41b6a4c:



    me "Va bene, resterò qui…"


translate italian zhenya_route_9f354a24:



    pi "L'hai visto? Ci hai parlato?"


translate italian zhenya_route_3edfb9a5:



    me "Di chi stai parlando?"


translate italian zhenya_route_65856116:



    pi "Lo sai chi…"


translate italian zhenya_route_9513cd87:



    me "Sì…"


translate italian zhenya_route_e3fcbbd3:



    pi "Cosa ti ha detto?"


translate italian zhenya_route_e88413fc:



    me "Niente di speciale…"


translate italian zhenya_route_5d3e0c2a:



    pi "Ti ha dato consigli? Ti ha detto cosa fare? Ti ha minacciato?"


translate italian zhenya_route_3a4fb558:



    me "No, niente del genere…{w} Certo, mi è sembrato un po' strano, ma niente più…"


translate italian zhenya_route_7896c94c:



    pi "Ricorda: potrebbe non essere solo! O meglio, lui è solo, ma potresti incontrare molti altri pionieri simili a lui."


translate italian zhenya_route_0effa2c7:



    me "E tu invece chi sei? Da chi ti stai nascondendo?"


translate italian zhenya_route_18d9b086:



    pi "Lo capirai…{w} Quando sarà il momento…{w} Ma ricorda: la cosa più importante è trovare l'uscita!"


translate italian zhenya_route_5adb12a4:



    "Per un istante il vento ha soffiato molto forte, e lo stesso bagliore è riapparso dinanzi a me..."


translate italian zhenya_route_c1d579b7:



    "E ancora una volta, in un istante, l'autobus e pionieri si sono materializzati davanti ai miei occhi."


translate italian zhenya_route_affe749c:



    "Non so perché io gli abbia detto quelle cose…"


translate italian zhenya_route_72e87799:



    "Ho pensato che fosse la cosa giusta da fare."


translate italian zhenya_route_175b483f:



    "Se solo mi ricordassi cosa significa rimorso e pentimento, allora forse ho provato proprio quelle sensazioni."


translate italian zhenya_route_6da9f141:



    mz "Un pioniere dev'essere sempre pronto!"


translate italian zhenya_route_174cdd90:



    "Una voce familiare è risuonata come un tuono nel bel mezzo di un cielo limpido, e un tonfo familiare sulla mia schiena mi ha fatto riemergere dallo stato catatonico in cui mi trovavo."


translate italian zhenya_route_9b924f7d:



    "Zhenya era di fronte a me."


translate italian zhenya_route_3c83f193:



    "Ho capito subito che si trattava della vera Zhenya, e non della bambola!"


translate italian zhenya_route_00a94cb1:



    pi "Ma come…"


translate italian zhenya_route_7f9e842d:



    "Erano le uniche parole che sono riuscito a sussurrare."


translate italian zhenya_route_ccbc9e0b:



    mz "Non lo so…"


translate italian zhenya_route_e0c9575f:



    "Ha sorriso, ma la sua voce tremava."


translate italian zhenya_route_a0d084f7:



    mz "Quel giorno, dopo il matrimonio mi sono svegliata in un mondo diverso…{w} Ho pensato di averti perso per sempre. E poi… e poi si sono susseguiti così tanti cicli che mi sono quasi dimenticata ogni cosa…"


translate italian zhenya_route_fc62eb93:



    pi "Perdonami."


translate italian zhenya_route_5788b9db_1:



    "Le ho detto a bassa voce."


translate italian zhenya_route_c6ad8ebd:



    pi "Perdonami per tutto."


translate italian zhenya_route_514806e6:



    mz "Ma è finita bene. Poi {i}lui{/i} è venuto da me e mi ha detto che poteva guidarmi nel modo dove eri tu. All'inizio non gli ho creduto, ma…{w} eccomi qui!"


translate italian zhenya_route_a283dd49:



    "Sono balzato in avanti e mi sono stretto tra le braccia di Zhenya."


translate italian zhenya_route_f3cdb758:



    pi "Perdonami! Avevo dimenticato… tutto, avevo dimenticato te! Ti ho fatto così tante cose brutte. Non ci ho creduto, non ci ho sperato! Perdonami!"


translate italian zhenya_route_f64a5bd9:



    mz "Argh, così mi soffochi!"


translate italian zhenya_route_70e18faf:



    "Si è liberata dalla mia morsa."


translate italian zhenya_route_d95a0aeb:



    mz "Ma adesso sono qui, quindi è tutto a posto!"


translate italian zhenya_route_97688933:



    "Mi ha detto tra le lacrime."


translate italian zhenya_route_c21de771:



    pi "Come puoi essere così tranquilla?"


translate italian zhenya_route_25a697d5:



    mz "Perché ho sempre saputo che alla fine saremmo stati insieme."


translate italian zhenya_route_af19cecb:



    "Zhenya ha alzato la sua mano destra, e sul suo dito ho visto un anello costruito con un mughetto…"


translate italian zhenya_route_d16e8b07:



    pi "Il mio l'ho perso, mi dispiace…"


translate italian zhenya_route_7f2fc500:



    mz "Non fa niente, ne compreremo uno nuovo!"


translate italian zhenya_route_06ab0532:



    "Ha sorriso."


translate italian zhenya_route_5608eebc:



    mtp "Ehi, voi due, salite sull'autobus!"


translate italian zhenya_route_7199c752:



    "Ci ha urlato scontenta Olga Dmitrievna."


translate italian zhenya_route_ce617998_23:



    "…"


translate italian zhenya_route_5fa8d405:



    "Il bus rimbalzava pacificamente sui dossi, trasportandoci lentamente verso il centro del distretto…{w} o ovunque esso si stesse dirigendo!"


translate italian zhenya_route_8e91937c:



    pi "Sai, non riesco a credere che tutto finirà."


translate italian zhenya_route_bb22c22e:



    "Ne ho passate così tante durante l'ultima settimana…"


translate italian zhenya_route_51ff3d8a:



    mz "E come sempre – hai avuto le parti più interessanti!"


translate italian zhenya_route_7730d48a:



    "Zhenya ha messo il broncio."


translate italian zhenya_route_0db5e9a1:



    pi "Non credo che preferiresti scambiarti di posto con me."


translate italian zhenya_route_7ba78cc4:



    mz "Posso immaginarlo! Ma è comunque meglio che stare seduti nella biblioteca per giorni e giorni."


translate italian zhenya_route_661c1c64:



    pi "Hai delle idee strane su cosa sia interessante…"


translate italian zhenya_route_2c6f48c5:



    mz "Ci hai forse pensato?"


translate italian zhenya_route_3a9e0089:



    pi "Riguardo a cosa?"


translate italian zhenya_route_745bf00d:



    mz "A cosa succederà domani…? Dobbiamo incontrarci in qualche modo nel mondo reale!"


translate italian zhenya_route_9c5be399:



    pi "Oh, già, fammi scrivere il tuo indirizzo."


translate italian zhenya_route_902c41be:



    mz "Sì, giusto, così mi potrai spedire subito una lettera!"


translate italian zhenya_route_d2a39874:



    "Ho sorriso stupidamente."


translate italian zhenya_route_7d57dcd6:



    mz "Me lo ricorderò, dimmi pure."


translate italian zhenya_route_5317f0d7:



    "Ci siamo scambiati i numeri e gli indirizzi."


translate italian zhenya_route_a4e5d71a:



    "È strano che non abbiamo mai avuto un'idea simile in passato."


translate italian zhenya_route_0161af1e:



    "Forse perché non speravamo di riuscire veramente ad andarcene da questo mondo, e non volevamo ricordarci le nostre vite passate."


translate italian zhenya_route_af2214a1:



    mz "Come credi che sarà? Come sempre? Ci addormenteremo?"


translate italian zhenya_route_db2a144a:



    "Zhenya si è stretta al mio braccio e si è premuta contro di me."


translate italian zhenya_route_2d69ac30:



    pi "Lo spero. Ma alla fine, non mi sembra l'opzione peggiore!"


translate italian zhenya_route_909b2197:



    mz "Direi che è una delle migliori."


translate italian zhenya_route_5464c3dd:



    "Zhenya ha sbadigliato e ha chiuso gli occhi."


translate italian zhenya_route_c7d14696:



    mz "Sogni d'oro. Ci vediamo."


translate italian zhenya_route_d0f0885a:



    "Non ho aggiunto altro e ho continuato a fissare il paesaggio che scorreva al di fuori del finestrino."


translate italian zhenya_route_1f47ecf1:



    "Mi sono lasciato il campo «Sovyonok» alle spalle, e non ci tornerò mai più."


translate italian zhenya_route_e536a6fa:



    "Sì, ne sono sicuro."


translate italian zhenya_route_900f8104:



    "Ma farà sempre parte della mia vita."


translate italian zhenya_route_e1e4cc09:



    "Lì sono cambiato, sono cambiato più volte."


translate italian zhenya_route_96ece0c6:



    "Rispetto al tempo trascorso lì, i meri cinquanta-sessant'anni che mi rimangono nel mondo reale sono solo un breve istante, ma farò in modo di viverli come un vero essere umano!"


translate italian zhenya_route_f8c0c4d9:



    mz "Sì, probabilmente sei cattivo, come ha detto quell'altro pioniere…"


translate italian zhenya_route_41d7cf0b:



    "Ha borbottato Zhenya nel sonno."


translate italian zhenya_route_ad02f53a_1:



    me "Che cosa?"


translate italian zhenya_route_1bdc5a24:



    mz "Niente, mettiti a dormire!"


translate italian zhenya_route_43560fef:



    "Ha riso e mi ha pizzicato dolorosamente."


translate italian zhenya_route_ce617998_24:



    "…"


translate italian zhenya_route_5ca93524:



    "Mi chiedo, cosa avrebbero provato gli altri al mio posto?"


translate italian zhenya_route_3882f569:



    "La stessa perdita di coscienza nei primi momenti, quella sensazione di ansia, di panico?"


translate italian zhenya_route_6cfbc782:



    "Sono tornato nel mio vecchio appartamento, ma non ho riconosciuto niente in esso."


translate italian zhenya_route_b1bb61b3:



    "Sì, è proprio come ritrovarsi in una casa in cui hai vissuto molti anni fa – tutto sembra essere al proprio posto, ma non ti ricordi niente, pensi solo che sia così che dovrebbe essere."


translate italian zhenya_route_2ed037cd:



    "Mi sono già dimenticato del giorno in cui sono scomparso da questo mondo (e non sono nemmeno sicuro che anno sia adesso), ma fuori è inverno, e ciò significa che non dev'essere passato molto tempo {i}qui{/i}."


translate italian zhenya_route_817af00a:



    "Già, non sono più un diciassettenne, ma non sembro neppure avere la mia {i}vera{/i} età!"


translate italian zhenya_route_e2e4abf4:



    "E così, alla fine si scopre che è andata bene? Il mio ritorno non ha avuto delle conseguenze impreviste?"


translate italian zhenya_route_7703e5b3:



    "Zhenya…"


translate italian zhenya_route_cab53a73:



    "Mi sono precipitato verso il tavolo e ho scritto il suo indirizzo e il numero di telefono con la mano tremante."


translate italian zhenya_route_ae449981:



    "Certamente non sarei in grado di scordarmeli, ma è comunque meglio scriverli da qualche parte, per sicurezza."


translate italian zhenya_route_827b177a:



    "E adesso cosa faccio?"


translate italian zhenya_route_dee0474e:



    "Come funziona questo mondo?"


translate italian zhenya_route_d061d06a:



    "Le leggi sono completamente diverse qui…{w} Ad essere sincero, non ho mai capito nessuna di esse."


translate italian zhenya_route_5ac00794:



    "E ora sono come un neonato che è costretto a vivere la vita di un adulto!"


translate italian zhenya_route_c294b867:



    "Ma rimandiamo tale questione a più tardi – ora quello che conta è trovare Zhenya!"


translate italian zhenya_route_103e53ea:



    "Qualcuno ha suonato il campanello…"


translate italian zhenya_route_8bd0d3e5:



    "Gli esseri umani.{w} Gli esseri umani reali, non i pionieri…"


translate italian zhenya_route_e9e7cdfa:



    "Devo aprire la porta, che altro posso fare? Finalmente sono tornato, e adesso devo vivere in questo mondo."


translate italian zhenya_route_c230ba61:



    "Mi sono incamminato lentamente verso la porta, ho ruotato la chiave nella serratura, ho stretto la maniglia e sono rimasto immobile."


translate italian zhenya_route_2f9f442b:



    mz "Allora? Per quanto ancora mi farai restare qui fuori?"


translate italian zhenya_route_3b74cebc:



    "Ho aperto la porta con uno scatto, e ho visto Zhenya…"


translate italian zhenya_route_6b1df7af:



    "Sembrava uguale a come era nel Campo."


translate italian zhenya_route_8a5df7ac:



    me "Ma…{w} com'è possibile?"


translate italian zhenya_route_120e3d2a:



    mz "Ricordi che io sono sempre arrivata nel Campo una settimana prima di te? E sono tornata una settimana prima di te."


translate italian zhenya_route_2b81f411:



    "Mi sono scordato del campo «Sovyonok», mi sono scordato del mondo reale, l'unica cosa che conta è che siamo di nuovo insieme!"


translate italian zhenya_route_85e2092d:



    "Ho abbracciato Zhenya e l'ho portata nella mia stanza."


translate italian zhenya_route_529e952d:



    mz "E quindi, ci limiteremo a stare in piedi così?"


translate italian zhenya_route_7b813b45:



    me "Ah, scusa…"


translate italian zhenya_route_c7fa01d1:



    "Zhenya è saltata giù sul pavimento con leggerezza."


translate italian zhenya_route_0edeb144:



    mz "Aspetta un attimo… Dobbiamo farlo subito, potrebbe non esserci più tempo dopo."


translate italian zhenya_route_7cfc6c8e:



    "Ha sorriso e ha iniziato a frugare nella sua borsa."


translate italian zhenya_route_cf917bd8:



    mz "Dammi la tua mano."


translate italian zhenya_route_d4a01b6d:



    "Le ho porto la mia mano, e Zhenya mi ha messo un anello al dito."


translate italian zhenya_route_9ede4597:



    "Un anello molto semplice – proprio come quello del Campo."


translate italian zhenya_route_6862cd26:



    "Sotto la fioca luce di un'impolverato lampadario che pendeva dal soffitto, un anello dorato ha brillato sul suo dito."


translate italian zhenya_route_5880720c:



    me "Forse avrei dovuto comprare gli anelli…"


translate italian zhenya_route_ee43c51d:



    mz "Beh, a giudicare dall'aspetto della tua caverna primitiva non mi sembra che tu abbia i soldi per farlo."


translate italian zhenya_route_6d386883:



    "Abbiamo riso insieme, con sincerità."


translate italian zhenya_route_037f0871:



    "Fuori dalla finestra stava cadendo una neve autentica, bianca, soffice. L'oscurità della notte veniva tagliata dalle migliaia e migliaia di luci della grande città, che stava cantando con centinaia di voci, ricordandoci che eravamo ancora vivi, e che eravamo ancora insieme."


translate italian zhenya_route_8e128b77:



    "Ho accarezzato dolcemente la guancia di Zhenya.{w} Così calda, così reale…"


translate italian zhenya_route_6abeee24:



    me "Non scomparirai questa volta?"


translate italian zhenya_route_871af259:



    mz "Ci puoi scommettere! Sei ancora in debito con me per gli anelli!"


translate italian zhenya_route_04ae1cfa:


    "Ho abbracciato Zhenya, e l'ho baciata come mai prima d'ora…"
# Decompiled by unrpyc: https://github.com/CensoredUsername/unrpyc
