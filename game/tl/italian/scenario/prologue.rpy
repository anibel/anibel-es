
translate italian prologue_24958abb:


    "Stavo facendo di nuovo lo stesso sogno."


translate italian prologue_07dac942:


    "{i}Quel{/i} sogno..."


translate italian prologue_55dfd177:


    "...ogni notte lo stesso."


translate italian prologue_9ce34629:


    "Ma non ricorderò più nulla al risveglio, come sempre."


translate italian prologue_0c90c0c8:


    "Forse è meglio così..."


translate italian prologue_8df02469:


    "Solo un flebile ricordo rimarrà, il ricordo di un cancello – leggermente aperto, come se mi stesse invitando ad entrare – situato fra due immobili statue di pionieri."


translate italian prologue_deca17df:


    "E quella strana ragazza...{w} che continua a chiedermi:"


translate italian prologue_814eff1e:


    dreamgirl "Vieni con me?"


translate italian prologue_e7b0f130:


    "Venire...?"


translate italian prologue_a2e02439:


    "Ma dove?"


translate italian prologue_fe88af01:


    "...e perché?"


translate italian prologue_4d0aa097:


    "E dove mi trovo, comunque?"


translate italian prologue_0683e19b:


    "Di certo, se tutto ciò fosse accaduto nella vita reale, mi sarei sicuramente spaventato."


translate italian prologue_233a6b9d:


    "Che altro si potrebbe provare?!"


translate italian prologue_cd46d7f7:


    "Ma questo è solo un sogno.{w} Sempre lo stesso, ogni notte."


translate italian prologue_af4b467b:


    "Dev'esserci una ragione!"


translate italian prologue_4cd30aa0:


    "Non è necessario sapere il {i}dove{/i} o il {i}perché{/i} per comprendere: qualcosa sta davvero accadendo."


translate italian prologue_2e4e4eec:


    "Qualcosa, alla disperata ricerca della mia attenzione."


translate italian prologue_7bd50019:


    "Dato che ogni cosa qui attorno a me è reale!"


translate italian prologue_354cb9c5:


    "Reale come le cose nel mio appartamento: potevo aprire il cancello, sentire il cigolio dei cardini, rimuovere la ruggine con il tocco della mano, respirare la fresca aria notturna e rabbrividire per il freddo."


translate italian prologue_5fb243fe:


    "Potevo, ma per farlo avrei dovuto alzarmi, fare un passo, muovere la mano..."


translate italian prologue_a6f914fa:


    "Ma questo è solo un sogno. Lo capisco, ma che significa? Cosa cambia del mio {i}comprendere{/i} tutto ciò?"


translate italian prologue_edb5109a:


    "Perché qui è come stare dall'altra parte dello schermo di un vecchio televisore, che combatte contro il rumore dell'immagine, sforzandosi di soddisfare i suoi spettatori senza tralasciare il minimo dettaglio."


translate italian prologue_28f18900:


    "Ma l'immagine sta diventando sempre più sfocata...{w} Probabilmente sto per svegliarmi."


translate italian prologue_a20cefa7:


    "..."


translate italian prologue_14a2c666:


    "Forse dovrei chiederle qualcosa?{w} Alla ragazza."


translate italian prologue_5da0f47a:


    "Qual è il suo nome..."


translate italian prologue_d7b38ee8:


    "Chiederle delle stelle, ad esempio..."


translate italian prologue_82d77cb1:


    "Ma perché proprio le stelle?"


translate italian prologue_51851d8b:


    "Forse sarebbe meglio chiederle del cancello!{w} Sì, il cancello!"


translate italian prologue_e5f4ee19:


    "Ne resterebbe sorpresa."


translate italian prologue_1edb57c8:


    "O meglio, dovrei chiederle di quella lettera «ё» che sta su di esso."


translate italian prologue_3234148c:


    "È stata una bella lettera..."


translate italian prologue_2367bcf1:


    "Come se non esistesse più!"


translate italian prologue_d92e974d:


    "Ma che c'entra quella lettera, il cancello e le stelle con questo luogo?"


translate italian prologue_e2138459:


    "Perché anche se posso immergermi in {i}questo{/i} sogno ogni notte, ed esso sarà presto dimenticato comunque, devo cercare le risposte, qui e adesso!"


translate italian prologue_d5050891:


    "Ecco, guardando attentamente in quella direzione, si riescono a scorgere le Nubi di Magellano..."


translate italian prologue_e2b591e8:


    "Come se fossi finito nell'emisfero australe!"


translate italian prologue_a20cefa7_1:


    "..."


translate italian prologue_22d0eaf7:


    "In un sogno ci sono dei piccoli dettagli che catturano la tua attenzione: il colore innaturale dell'erba, impossibili curvature di linee rette, o il proprio riflesso distorto – mentre il vero pericolo, quello che potrebbe porre fine ad ogni cosa qui in questo momento, appare del tutto irrilevante."


translate italian prologue_26999fe4:


    "È naturale, dato che {i}qui{/i} non si può morire."


translate italian prologue_a6da1123:


    "Ne ho la certezza – l'ho vissuto centinaia di volte."


translate italian prologue_e85b8394:


    "Ma che senso ha vivere se non si può morire?"


translate italian prologue_a0462877:


    "Dovrei chiederlo alla ragazza. Lei è del posto, dovrebbe sapere la risposta."


translate italian prologue_ee712a8b:


    "Sì, esatto!{w} Dovrei chiederle di quella civetta, ad esempio."


translate italian prologue_b0865ff2:


    "Che animale strano..."


translate italian prologue_2be63cfe:


    "Tuttavia, non importa..."


translate italian prologue_a20cefa7_2:


    "..."


translate italian prologue_814eff1e_1:


    dreamgirl "Vieni con me?"


translate italian prologue_db203a62:


    "E ogni volta devo rispondere."


translate italian prologue_7ad37ff1:


    "È l'unico modo, altrimenti il sogno non avrà mai fine, e non potrò mai svegliarmi."


translate italian prologue_027137ed:


    "Ogni volta è così difficile decidere."


translate italian prologue_34fac5fe:


    "Dove mi trovo, cosa ci faccio qui, chi è lei?"


translate italian prologue_071572e1:


    "E perché la mia vita dipende così tanto da quella risposta?"


translate italian prologue_ec721a0b:


    "O forse no...?"


translate italian prologue_51d80ee4:


    "È solo un sogno, dopotutto..."


translate italian prologue_ca1b37f9:


    "Solo un sogno..."


translate italian prologue_41cdd4ce:


    "Lo schermo del computer mi fissava come se fosse vivo."


translate italian prologue_86cfd3d1:


    "Certe volte mi pare davvero che esso abbia una propria consapevolezza, pensieri e desideri, ambizioni; che possa provare emozioni, che possa amare e soffrire."


translate italian prologue_b3960be5:


    "Come se nel nostro rapporto esso non fosse uno strumento – ero io il pezzo inanimato di plastica e circuiti."


translate italian prologue_67859f20:


    "Esiste un fondo di verità in ciò, probabilmente per il fatto che il computer rappresenta il 90%% della mia interazione con il mondo esterno."


translate italian prologue_e051da8b:


    "Imageboard anonime, qualche chat ogni tanto, raramente ICQ o Jabber, e ancora più raramente i forum."


translate italian prologue_3f938594:


    "Le persone all'altra estremità del cavo semplicemente non esistono!"


translate italian prologue_5a1a52c8:


    "Tutti loro sono semplici creazioni della sua fantasia malata, un errore nel codice sorgente o un bug del kernel, che ha assunto vita propria."


translate italian prologue_2728421d:


    "Se si guardasse la mia esistenza dall'esterno, tali pensieri potrebbero sembrare folli, e uno psicologo formulerebbe certamente una serie di diagnosi sofisticate, magari spedendomi dritto al manicomio."


translate italian prologue_9d607629:


    "Un piccolo appartamento, senza segni di riparazione o alcuna parvenza di ordine in esso, e sempre la solita vista dalla finestra: una grigia megalopoli che si muove ciclicamente giorno e notte – queste sono le condizioni della mia esistenza."


translate italian prologue_5823e040:


    "Ma ovviamente non è sempre stato così..."


translate italian prologue_7380c69b:


    "Sono nato, ho frequentato la scuola e l'ho finita – come tutti gli altri."


translate italian prologue_e5f87d9f:


    "Sono stato accettato all'università, dove ho trascorso un anno e mezzo tra sforzi e fatiche."


translate italian prologue_c895b6f5:


    "Ho cambiato spesso lavoro.{w} Certe volte andava abbastanza bene, certe volte guadagnavo anche somme decenti."


translate italian prologue_26348b28:


    "Ma tutto sembrava non essere mio, come se la storia fosse stata presa dalla biografia di qualcun altro."


translate italian prologue_111ab6cc:


    "Non stavo vivendo la mia vita appieno – continuavo a ripetere sempre la stessa monotona routine.{w} Come nel film «Ricomincio da capo»."


translate italian prologue_bab8b972:


    "È solo che non sapevo come trascorrere la giornata, e ogni giorno si ripeteva, sempre la stessa spirale viziosa.{w} Una spirale di miseria, vuoto interiore e disperazione."


translate italian prologue_76b2fe88:


    nvl clear


translate italian prologue_b41ec809:


    "Negli ultimi anni non ho fatto altro che stare tutto il giorno davanti allo schermo."


translate italian prologue_c39afb80:


    "Certe volte c'erano dei lavori occasionali, altre volte mi aiutavano i miei genitori."


translate italian prologue_dfb24146:


    "Tutto sommato, riuscivo a mantenermi."


translate italian prologue_ba1a156e:


    "Non c'è da stupirsi, dato che le mie necessità sono piuttosto ridotte."


translate italian prologue_9bc39b5c:


    "Esco molto raramente, e il mio rapporto con le altre persone consiste quasi esclusivamente nell'interazione con gli {i}anonimi{/i}, che non hanno un vero nome, sesso o età."


translate italian prologue_a3a04a99:


    "Quindi, in breve, la tipica esistenza di una tipica persona antisociale del suo tempo.{w} Una sorta di Donnie Darko in scala minore, senza visioni apocalittiche."


translate italian prologue_ac5e97c3:


    "Forse un qualche stimato autore scriverà una storia su di me, e diventerò un classico contemporaneo della letteratura moderna.{w} O forse ne scriverò una io…"


translate italian prologue_eea3f384:


    "Ad ogni modo, che senso ha illudersi – ci ho provato mille volte, ma non sono riuscito nemmeno a scrivere una semplice e breve storia."


translate italian prologue_6d041340:



    nvl clear
    "Ho cercato di imparare anche molte altre cose."


translate italian prologue_64793d14:


    "Non ho una grande abilità nel dipingere.{w} La programmazione – mi annoia.{w} Le lingue straniere – richiedono troppo tempo…"


translate italian prologue_b41193f7:


    "L'unica cosa che mi piace fare è leggere, ma non mi definirei uno studioso."


translate italian prologue_d0321015:


    "Forse sono un asso nel guardare anime e un maestro nell'inventare sciocche battute su internet."


translate italian prologue_acb955ab:


    "Se mi pagassero per questo, probabilmente sarei una persona più felice (e pure ricca), ma dubito che ciò possa riempire il vuoto dentro di me."


translate italian prologue_76b2fe88_1:


    nvl clear


translate italian prologue_8d743298:


    "Oggi è stata un'altra tipica giornata di una tipica esistenza fallimentare."


translate italian prologue_4d249ee8:


    "E oggi è il giorno in cui devo andare a quell'incontro con i compagni di università."


translate italian prologue_3d7e70ae:


    "Ad essere onesti, avrei preferito non accettare l'invito."


translate italian prologue_556a5316:


    "Che senso ha? Il tempo trascorso con loro è stato così breve."


translate italian prologue_b84fba8f:


    "Comunque, sono stato persuaso da un amico: il mio vecchio compagno di università, uno dei pochi con cui sono ancora in contatto oltre a quelli su internet."


translate italian prologue_a2136874:


    "Una gelida serata.{w} Fermata dell'autobus. Attesa."


translate italian prologue_55a843c8:


    "Non mi è mai piaciuto l'inverno.{w} Anche se l'estate non è di certo la mia stagione preferita."


translate italian prologue_6c43f774:


    "È solo che non vedo alcun motivo di preferire un certo periodo dell'anno, non importa com'è il tempo fuori se si sta ventiquattr'ore al giorno chiusi in casa."


translate italian prologue_a43f6da8:


    "Oggi l'autobus era talmente in ritardo che stavo per mandare tutto al diavolo e spendere le mie ultime poche centinaia di rubli per un taxi (per qualche motivo l'idea di tornare a casa non mi è passata per la testa)."


translate italian prologue_42c6ca3e:


    "Come sempre, milioni di pensieri mi affioravano nella mente, ma non ce n'era uno degno di essere considerato."


translate italian prologue_ebf71c4c:


    "Un qualche pensiero che possa diventare reale, prendere forma, trasformarsi in un'idea da mettere in pratica."


translate italian prologue_6b447492:


    "Forse potrei aprire una mia impresa?{w} Ma dove trovo i soldi?"


translate italian prologue_1263d7e9:


    "O forse potrei tornare a lavorare in ufficio?{w} No, non se ne parla!"


translate italian prologue_ef2e5c25:


    "Forse dovrei provare con il libero professionismo?{w} Ma quali abilità possiedo, e chi mi vorrebbe dopotutto…?"


translate italian prologue_42e509b4:


    "All'improvviso mi è apparsa in mente la mia infanzia…{w} O meglio, il periodo in cui avevo 15-17 anni."


translate italian prologue_153757bd:


    "Perché proprio quel periodo?{w} Non ne ho idea."


translate italian prologue_b3ca007f:


    "Forse perché a quei tempi era tutto molto più semplice."


translate italian prologue_74b11ddc:


    "Era più facile prendere una decisione, così difficile ora, così semplice prima."


translate italian prologue_a0da84d1:


    "Alzandomi la mattina, sapevo esattamente come sarebbe stata la mia giornata, e aspettavo con impazienza il fine settimana. Così avrei potuto riposare e avere del tempo libero per le cose che amavo: il computer, il calcio, uscire con gli amici."


translate italian prologue_7160de27:


    "E dopo, all'inizio della nuova settimana, avrei ripreso a studiare."


translate italian prologue_334f2acb:


    "A quei tempi non c'erano certe preoccupazioni come «perché», «a chi importa», «cosa cambia se lo faccio» o «cosa non cambia»."


translate italian prologue_272df669:


    "Uno stile di vita semplice, così semplice per ogni persona normale, ma così strano per me adesso."


translate italian prologue_8963d5d3:



    nvl clear
    "Quegli innocenti giorni di gioventù…{w} Fu in quel periodo che mi innamorai per la prima volta."


translate italian prologue_4a6ac0a3:


    "Il suo aspetto e il suo carattere sono ormai svaniti dalla mia mente."


translate italian prologue_7493b623:


    "Ricordo solo il suo nome, come una breve linea del profilo di un social network, e le emozioni che mi travolgevano quando stavo con lei.{w} Affetto, tenerezza, il desiderio di prendermi cura di lei e di proteggerla…"


translate italian prologue_064fa38f:


    "Sfortunatamente, non durò a lungo."


translate italian prologue_c7008ed9:


    "Dubito fortemente che oggi mi possa capitare una cosa simile."


translate italian prologue_f716a97b:


    "Probabilmente mi farebbe piacere incontrare una ragazza, ma non saprei come iniziare una conversazione, di cosa diavolo parlare e come risultare attraente."


translate italian prologue_d631396b:


    "Beh, è da molto tempo che non incontro una ragazza speciale.{w} E comunque, dove potrei incontrarla…?"


translate italian prologue_76b2fe88_2:


    nvl clear


translate italian prologue_52032d61:


    "Il rumore di un motore mi ha riportato alla realtà."


translate italian prologue_5d936a59:


    "Un autobus si è accostato alla fermata."


translate italian prologue_68c6dc22:


    "«C'è qualcosa di strano in quell'autobus» – ho pensato."


translate italian prologue_f260960e:


    "D'altronde, non importa: l'unico bus che passa qui è il 410."


translate italian prologue_3cf36c8d:


    "Le luci della strada mi scorrevano davanti agli occhi. È come se quelle fredde luci stessero scintillando dentro di me, cercando di infiammare emozioni morte da tempo."


translate italian prologue_ac957580:


    "O forse non proprio infiammare, ma piuttosto risvegliare…"


translate italian prologue_b9473edc:


    "Perché quelle emozioni sono rimaste dentro di me a lungo, addormentandosi e poi risvegliandosi nuovamente."


translate italian prologue_59544860:


    "La radio del conducente trasmetteva una melodia molto familiare.{w} Ma non la stavo ascoltando."


translate italian prologue_53102b62:


    "Stavo guardando le automobili che scorrevano fuori dal finestrino annebbiato."


translate italian prologue_0931b53b:


    "Le persone sono sempre di fretta, inseguono una loro necessità, intrappolati nei loro piccoli mondi, perché mai dovrebbero interessarsi del mio?"


translate italian prologue_5ccdd86f:


    "Probabilmente hanno i loro seri problemi di cui occuparsi, o magari hanno una vita molto più semplice."


translate italian prologue_813e6d62:


    "Non puoi saperlo con certezza, dato che ogni persona è diversa.{w} O forse no?"


translate italian prologue_e0c41f97:


    "Certe volte le azioni di una persona sono facilmente prevedibili, ma se provi a guardare dentro la sua anima vedrai soltanto un'oscurità impenetrabile."


translate italian prologue_a20cefa7_3:


    "..."


translate italian prologue_9f51c5b0:


    "L'autobus stava raggiungendo il centro, e i miei pensieri sono stati interrotti dalle luminose luci della città."


translate italian prologue_1029ada9:


    "Centinaia di insegne, migliaia di automobili, milioni di persone."


translate italian prologue_d443aa58:


    "Stavo ammirando tutto quello spettacolo, quando mi è preso un gran sonno."


translate italian prologue_d56c688b:


    "Ho chiuso gli occhi per un instante, e poi…"
# Decompiled by unrpyc: https://github.com/CensoredUsername/unrpyc
