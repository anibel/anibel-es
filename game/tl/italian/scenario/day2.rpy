
translate italian day2_main1_5a2ae8b2:


    "Stavo sognando…"


translate italian day2_main1_fc07b71b:


    "Ero in una sorta di vuoto, non c'era assolutamente niente intorno a me."


translate italian day2_main1_895753c3:


    "Ma non solo {i}intorno{/i}: ero l'unica creatura nell'universo."


translate italian day2_main1_0ceef837:


    "Come se l'universo fosse tornato al suo stadio primordiale prima del Big Bang."


translate italian day2_main1_e5affe53:


    "E come se qualcosa stesse per accadere."


translate italian day2_main1_1409c490:


    "Improvvisamente ho sentito una voce."


translate italian day2_main1_7451916c:


    "Non riuscivo a distinguere le parole, ma suonava familiare."


translate italian day2_main1_30fbd9d9:


    "La voce mi stava sussurrando qualcosa dolcemente, come se volesse calmarmi."


translate italian day2_main1_72c99b77:


    "E poi ho capito...{w} Era la voce di quella strana ragazza del bus.{w} La ragazza dei sogni."


translate italian day2_main1_08be4530:


    th "Ma cosa sta cercando di dirmi? Chi è lei...?"


translate italian day2_main1_4b4e1333:


    "Mi sono svegliato."


translate italian day2_main1_43e6696c:


    "I luminosi raggi del sole hanno colpito i miei occhi."


translate italian day2_main1_ddaaef10:


    "Era quasi mezzogiorno."


translate italian day2_main1_88d071f4:


    "Dopo essermi disteso sul letto e aver sbadigliato, ho iniziato a ricordare il giorno appena trascorso."


translate italian day2_main1_062f89ad:


    "In un batter d'occhio, tutti gli eventi di quel giorno mi sono passati davanti agli occhi: l'autobus, il Campo, gli abitanti locali."


translate italian day2_main1_32bb3fe1:


    th "No, è semplicemente sbagliato!"


translate italian day2_main1_efb38133:


    "Non era sbagliata questa situazione o il fatto che io fossi qui – era tutto sbagliato a prescindere – era sbagliato il mio atteggiamento verso quello che stava accadendo."


translate italian day2_main1_7ac9eb97:


    th "Perché ieri sono andato a dormire, e ancora prima avevo parlato tranquillamente con i pionieri del luogo, riuscendo addirittura a fare qualche battuta!"


translate italian day2_main1_31cacf90:


    th "Come ho potuto comportarmi in quel modo in una situazione del genere?!"


translate italian day2_main1_9f97589a:


    th "Dovrei essere spaventato, dovrei trasalire a ogni singolo rumore, dovrei evitare un qualunque contatto con le creature potenzialmente ostili."


translate italian day2_main1_321177f9:


    "Gli eventi del giorno precedente stavano diventando vaghi, come se avessi i postumi di una sbornia."


translate italian day2_main1_0b66f5c8:


    th "Questa realtà è come la mattina dopo una festa a base di molto alcool – all'inizio è tutto così naturale, impeccabile, totalmente normale, mentre al mattino seguente diventa un incubo, un'illustrazione grottesca della «Divina Commedia»."


translate italian day2_main1_2c6f5607:


    th "Sì, è proprio così, e ora non posso cambiare il passato."


translate italian day2_main1_7756bdbc:


    "D'altronde, probabilmente ho valutato la situazione e mi stavo comportando di conseguenza."


translate italian day2_main1_b93eac53:


    "Mi sono dato un'occhiata intorno, cercando di capire se fossi finito in qualche altro luogo, ma la stanza di Olga Dmitrievna sembrava la stessa di ieri."


translate italian day2_main1_63937111:


    "Sembrava tutto come prima, l'unica differenza era un'uniforme da pioniere appoggiata sul mio letto."


translate italian day2_main1_3b552dd7:


    "L'ho afferrata con diffidenza e me la sono messa addosso."


translate italian day2_main1_d906fb96:


    th "Almeno non dovrò andare in giro con i vestiti invernali."


translate italian day2_main1_207c3b3d:


    th "Vorrei vedere come mi sta – scommetto che sembro un pagliaccio!"


translate italian day2_main1_01922da1:


    "Ma per fare ciò avevo bisogno di uno specchio.{w} O almeno un piccolo specchietto."


translate italian day2_main1_c8ca898d:


    "Finalmente ne ho trovato uno sull'anta dell'armadietto."


translate italian day2_main1_9fe62ef5:


    me "Porca...!"


translate italian day2_main1_3e51bea7:


    "Mi sono trovato davanti ad uno sconosciuto e sono balzato indietro dallo stupore!"


translate italian day2_main1_a7207d27:


    "C'era un adolescente riflesso nello specchio!"


translate italian day2_main1_a5cf122d:


    "Mi assomigliava, ma non ero io!"


translate italian day2_main1_50576b33:


    "Dov'era finita la barba ispida, le occhiaie, la mia brutta cera, e quella mia espressione terribilmente affaticata?!"


translate italian day2_main1_b7edc738:


    "Sembrava che non fossi né tornato al passato, né in un universo parallelo, ma che mi fossi semplicemente ritrovato nel corpo di qualcun altro."


translate italian day2_main1_977f3766:


    th "Giusto, è così semplice!{w} Queste cose accadono tutti i giorni!"


translate italian day2_main1_7c803d3e:


    "Ho osservato più attentamente quello sconosciuto e solo allora mi sono accorto che ero proprio io!"


translate italian day2_main1_473252f1:


    "Però non ero l'io del giorno d'oggi – ma quello dei tempi della scuola."


translate italian day2_main1_bc978da0:


    th "Beh, almeno è già qualcosa."


translate italian day2_main1_1e928d66:


    th "Eccoci, dopotutto la {i}persona in una situazione estrema{/i} non è riuscita a notare l'elefante nel salotto."


translate italian day2_main1_78884d1e:


    th "Ma la leader del Campo l'ha notato, e la notte scorsa mi ha rimproverato per essermi rivolto a lei senza portare rispetto…"


translate italian day2_main1_e3987898:


    th "Ah, al diavolo!"


translate italian day2_main1_cbe4d96c:


    th "Dubito che qualcosa possa dipendere dal mio aspetto."


translate italian day2_main1_c20955fe:


    "Se l'orologio non mentiva, la colazione era trascorsa da tempo ormai."


translate italian day2_main1_07623b09:


    th "Oh bene, proverò a cercare qualcosa alla mensa."


translate italian day2_main1_b1e4da72:


    th "Ieri con Slavya è andata bene, no?"


translate italian day2_main1_0a24a340:


    "Quei ricordi mi hanno fatto sorridere involontariamente."


translate italian day2_main1_86af3570:


    "Il sole splendeva di fuori, una leggera brezza stava soffiando."


translate italian day2_main1_a7b6773f:


    th "Una bellissima giornata estiva."


translate italian day2_main1_d9a6ffae:


    "Era da molti anni che non mi sentivo così bene la mattina."


translate italian day2_main1_6597a8e7:


    "Tutti i problemi se n'erano andati, svaniti tra le nuvole, bianche come la neve."


translate italian day2_main1_be95a8d4:


    "Olga Dmitrievna è apparsa da chissà dove."


translate italian day2_main1_b8d13a2a:


    mt "Buongiorno, Semyon!"


translate italian day2_main1_32f6d96b:


    me "'Giorno!"


translate italian day2_main1_eda54ed8:


    "Ho sorriso, cercando di mostrare che, nonostante tutto, la mia giornata era davvero iniziata bene."


translate italian day2_main1_53e06c95:


    mt "Dato che sei arrivato appena ieri, ho deciso di non svegliarti, e la colazione ormai…"


translate italian day2_main1_d1895f30:


    mt "Ma non importa! Ecco, prendi questi!"


translate italian day2_main1_71894711:


    "Mi ha consegnato qualcosa avvolto nella carta."


translate italian day2_main1_db4f4b37:


    "A giudicare dalle macchie oleose, ci dovevano essere dei tramezzini dentro."


translate italian day2_main1_8aa9a83a:


    me "Oh, grazie!"


translate italian day2_main1_641d577e:


    mt "E adesso va' a lavarti!"


translate italian day2_main1_385627a7:


    "Stavo per andarmene."


translate italian day2_main1_22a42eb2:


    mt "Aspetta un secondo."


translate italian day2_main1_66afc59e:


    "Olga Dmitrievna è corsa velocemente nella casetta ed è tornata con una piccola borsa, che mi ha consegnato."


translate italian day2_main1_9c319d40:


    "All'interno di essa ho trovato uno spazzolino da denti, del sapone, un piccolo asciugamano e qualcos'altro – non ho guardato bene."


translate italian day2_main1_29289d61:


    mt "Un pioniere dev'essere sempre pulito e ordinato!"


translate italian day2_main1_1d1b2ba8:


    mt "Lascia che ti mostri come si mette il foulard, te lo sei messo di traverso.{w} Così sarai in grado di farlo da solo."


translate italian day2_main1_66e9a1e6:


    me "Dobbiamo proprio? Dovrei andare a lavarmi adesso."


translate italian day2_main1_4a476f09:


    th "Già, giusto, potrebbe impigliarsi nel rubinetto e strangolarmi..."


translate italian day2_main1_e511e399:


    mt "D'accordo, allora più tardi.{w} E non dimenticarti l'allineamento."


translate italian day2_main1_8eba6c7d:


    th "Matite, carta, righelli…{w} È difficile dimenticarsi certe cose!"


translate italian day2_main1_a25f95a7:


    me "Quale allineamento?"


translate italian day2_main1_41981848:


    mt "Come sarebbe «quale allineamento»?!"


translate italian day2_main1_823e3ce8:


    "Ha aggrottato la fronte."


translate italian day2_main1_ebb2ca84:


    mt "Oggi è lunedì!"


translate italian day2_main1_5afc6ea3:


    th "Strano, secondo le mie stime dovrebbe essere domenica…"


translate italian day2_main1_769920ed:


    th "Del resto, sbagliarsi di un giorno non è poi così grave."


translate italian day2_main1_a24cdacb:


    mt "Solitamente facciamo l'allineamento la mattina presto prima della colazione, ma oggi è lunedì, così lo facciamo a mezzogiorno."


translate italian day2_main1_a89a2c76:


    mt "Mi raccomando, arriva puntuale!"


translate italian day2_main1_0de6c041:


    me "D'accordo. Ma dove?"


translate italian day2_main1_a56c3145:


    mt "Alla piazza, dove se no?!"


translate italian day2_main1_49afcaa3:


    "Non c'era motivo di opporsi."


translate italian day2_main1_7ca9038d:


    "Mi sono incamminato verso la «toilette»."


translate italian day2_main1_909fa19e:


    "Sapevo già che potevo scordarmi le docce separate e il gabinetto, ma la vista di questo malfunzionante sintomo della decadenza socialista – una buffa tartaruga con il guscio di latta, zampe a forma di rubinetto e una pancia in ceramica – mi ha fatto star male."


translate italian day2_main1_fc6f68e6:


    "Non sono una persona schizzinosa, ma stando qui mi sono reso conto che c'era comunque un minimo di comfort del quale era difficile fare a meno."


translate italian day2_main1_201b58e0:


    "È spesso così – quando perdi le cose che consideravi ovvie e scontate, improvvisamente ti accorgi di quanto fossero importanti."


translate italian day2_main1_fc32f6b3:


    th "Ah, al diavolo! Come se avessi altra scelta."


translate italian day2_main1_e5cc3b97:


    "L'acqua era gelida."


translate italian day2_main1_7a2b0aea:


    "Lavarsi le mani con quest'acqua non era un problema, ma lavarsi il viso e la bocca era un'impresa difficile."


translate italian day2_main1_9d2484de:


    "Nella borsa che mi aveva dato Olga Dmitrievna però non c'era il dentifricio."


translate italian day2_main1_13ab8033:


    "Avrei potuto lavarmi i denti senza di esso, ma ho notato una piccola scatolina rotonda avvolta nell'asciugamano."


translate italian day2_main1_331eedf5:


    "«Dentifricio in polvere»."


translate italian day2_main1_60d90a2e:


    th "Carino! Un punto a favore del ritrovarsi nel passato."


translate italian day2_main1_43dc525a:


    "Mi sono lavato di fretta, anche perché l'acqua era gelida."


translate italian day2_main1_86c7f6e3:


    "Qualcuno si stava avvicinando, o meglio stava correndo verso di me."


translate italian day2_main1_47eaf14e:


    "Mi sono voltato."


translate italian day2_main1_644b2c05:


    "Era Slavya, con indosso una tuta sportiva."


translate italian day2_main1_2984b340:


    "Questa ragazza probabilmente starebbe bene con ogni cosa: con l'uniforme da pioniere, in bikini, forse persino con una tuta spaziale."


translate italian day2_main1_396d52a7:


    sl "Ehilà!"


translate italian day2_main1_65d811ca:


    me "Ueioh… Voglio dire, comva… Buon giorno! Sì…"


translate italian day2_main1_12f788c2:


    "Oh sì, davvero bravo."


translate italian day2_main1_0ec1deba:


    sl "Perché non sei venuto a colazione?"


translate italian day2_main1_9ac34b39:


    me "Ho dormito troppo a lungo."


translate italian day2_main1_a512e5ef:


    "Le ho detto, come se fossi orgoglioso di ciò."


translate italian day2_main1_531482a7:


    me "Ma Olga Dmitrievna mi ha dato dei tramezzini."


translate italian day2_main1_e3b70370:


    sl "Oh, bene allora! Ricordati dell'allineamento!"


translate italian day2_main1_85279b68:


    me "Sì, certo."


translate italian day2_main1_ea0e94f5:


    th "Come se potessi dimenticarmelo."


translate italian day2_main1_09688a75:


    sl "Bene, io devo andare! Divertiti!"


translate italian day2_main1_06aca64d:


    "Mi ha salutato con la mano ed è sparita dietro l'angolo."


translate italian day2_main1_4ef9a3ba:


    th "Mancano solo pochi minuti all'allineamento."


translate italian day2_main1_a1bcef4b:


    "Dovrei affrettarmi a tornare a «casa mia» per riportare la borsa e mangiare i tramezzini, e poi raggiungere la piazza."


translate italian day2_main1_8fc4fbf5:


    "Ho spalancato la porta della casetta della leader e mi sono precipitato al suo interno, come se stessi saltando sull'ultima carrozza di un treno in partenza."


translate italian day2_main1_8f99475f:


    "Non è stata affatto una buona idea, perché all'interno c'era Olga Dmitrievna…"


translate italian day2_main1_361b5648:


    "...che si stava vestendo!"


translate italian day2_main1_989f5b30:


    "Mi sono bloccato sui miei passi, trattenendo il respiro."


translate italian day2_main1_6f55abad:


    "Alla fine la leader del Campo mi ha notato."


translate italian day2_main1_105fc865:


    mt "Semyon!"


translate italian day2_main1_4d49e3f9:


    "Ho distolto immediatamente lo sguardo."


translate italian day2_main1_c2b62a94:


    mt "Hai mai sentito parlare del bussare alla porta?! E ora vattene!"


translate italian day2_main1_118d6aad:


    th "Già, è stato davvero goffo."


translate italian day2_main1_fa866f87:


    "Anche se ho apprezzato la vista."


translate italian day2_main1_ee01ae30:


    "Olga Dmitrievna è uscita dopo un minuto."


translate italian day2_main1_f47b36ab:


    mt "Ecco, prendi questa.{w} Adesso è anche casa tua."


translate italian day2_main1_c3639b5f:


    "Mi ha consegnato una chiave.{w} Me la sono messa in tasca."


translate italian day2_main1_fcf7f167:


    th "Casa…"


translate italian day2_main1_c9d7a8ab:


    th "Ovviamente, ignorando tutte le cose strane che sono accadute, non si può di certo dire che questo Campo sia il luogo peggiore sulla faccia della Terra, ma chiamarlo casa…"


translate italian day2_main1_8bb6d04f:


    th "Dopo appena un giorno trascorso qui!"


translate italian day2_main1_7ab9cde3:


    th "Dubito che riuscirei a considerarlo tale."


translate italian day2_main1_cd306d7c:


    mt "Va bene, andiamo, siamo in ritardo."


translate italian day2_main1_dc3be649:


    me "Ma i tramezzini...?"


translate italian day2_main1_a350f4cb:


    mt "Mangiali durante il tragitto!"


translate italian day2_main1_9898e071:


    "Stavamo passando lungo le file di casette dei pionieri, mentre io ero totalmente concentrato sui miei tramezzini e Olga Dmitrievna continuava a parlare e parlare.{w} Le sue parole suonavano come un Allegro Chirurgo giocato da una persona con il Parkinson!"


translate italian day2_main1_90484daa:


    "Ma io mi ero concentrato esclusivamente sui miei tramezzini."


translate italian day2_main1_38e0aff0:


    mt "Hai capito tutto?"


translate italian day2_main1_b6626098:


    me "Eh?"


translate italian day2_main1_3c5ea71b:


    mt "Non mi stavi ascoltando!"


translate italian day2_main1_559ab9c6:


    me "Mi scusi…"


translate italian day2_main1_ff1825ac:


    mt "Oggi è il primo giorno della tua nuova vita di pioniere!"


translate italian day2_main1_b2cb81b3:


    mt "E dovresti impegnarti affinché ciò possa portarti ad un futuro sereno!"


translate italian day2_main1_88b4ce94:


    me "Ah, già, senza dubbio…"


translate italian day2_main1_76079f83:


    mt "Non sto scherzando! Un pioniere ha molte responsabilità: partecipare alla vita sociale, aiutare i più giovani, imparare, imparare e ancora imparare!"


translate italian day2_main1_7f4c0bdf:


    mt "Tutti noi qui siamo come una grande famiglia.{w} E tu ne farai parte."


translate italian day2_main1_3f18fd02:


    th "Sì, ne farò parte…{w} Mi farò dare anche una tessera del Partito, così non dovrò più ascoltare queste sciocchezze."


translate italian day2_main1_126fe430:


    mt "Spero che quando questa esperienza sarà finita, conserverai solo bei ricordi di questo Campo."


translate italian day2_main1_c17a76a5:


    mt "Ricordi che dureranno per tutta la vita!"


translate italian day2_main1_9b154f85:


    me "E quando finirà?"


translate italian day2_main1_3b76cadb:


    mt "Perché continui a fare domande stupide?"


translate italian day2_main1_adbfd80d:


    th "Pare che non riuscirò ad ottenere alcuna risposta da lei."


translate italian day2_main1_c6e6c275:


    "È un vero peccato.{w} Questo mondo sembrava essere così amichevole, ma non si è mai preso la briga di {i}presentarsi{/i} a me."


translate italian day2_main1_edbe22f5:


    th "Forse ora potrò considerare tutto questo in modo meno tragico rispetto a ieri?"


translate italian day2_main1_cc8bfc43:


    "Sembrava che {i}quella cosa{/i} finalmente mi avesse dato tregua – {i}quella cosa{/i} non stava cercando di farmi del male, ma mi era proibito fare domande."


translate italian day2_main1_b692bf4b:


    "Certamente una tale situazione non era delle migliori, ma che ci potevo fare?{w} Una cattiva tregua è meglio di un buon conflitto."


translate italian day2_main1_bfb454f3:


    mt "La cosa più importante adesso è che tu possa impiegare il tuo tempo qui nel modo migliore possibile."


translate italian day2_main1_8e4b3ee3:


    me "Farò del mio meglio."


translate italian day2_main1_760779dc:


    "Onestamente, quella conversazione mi aveva già stancato."


translate italian day2_main1_cdb44710:


    "Sarebbe stato bello sapere dove si trovasse esattamente quel «qui»!{w} Ma…"


translate italian day2_main1_5fdbda0d:


    "Siamo arrivati alla piazza."


translate italian day2_main1_bf8d48ab:


    "I pionieri erano già tutti allineati."


translate italian day2_main1_69eaae74:


    me "Che c'è, manca qualcuno?"


translate italian day2_main1_d5cf567b:


    mt "Hmm, no. Sono tutti qui."


translate italian day2_main1_f65725c8:


    "Ha esaminato la sua coraggiosa truppa di pionieri."


translate italian day2_main1_e4178ce3:


    mt "Bene, mettiti da qualche parte."


translate italian day2_main1_2240ebeb:


    th "Strano.{w} Perché mi ha detto che non c'erano più posti dove dormire?"


translate italian day2_main1_73025a75:


    "Mentre il capo muoveva la bocca illustrandoci il programma della settimana, io osservavo i pionieri."


translate italian day2_main1_6af3ef8e:


    "Poco più avanti c'era Electronik, ancora più avanti – Lena e Slavya, e per ultime – Ulyana e Alisa."


translate italian day2_main1_80439bd7:


    th "Tutti coloro che ho incontrato sono qui."


translate italian day2_main1_d2e34ec7:


    "Olga Dmitrievna stava parlando di una qualche competizione, ma la mia attenzione si è posata sul monumento."


translate italian day2_main1_0c15405c:


    th "«Genda»..."


translate italian day2_main1_89f74fc0:


    "Non ricordo alcun rivoluzionario con un nome simile..."


translate italian day2_main1_49c36f8b:


    "Aveva una strana postura – come se si stesse guardando attorno con diffidenza, forse con sdegno, o addirittura con disprezzo."


translate italian day2_main1_ae1cb199:


    th "Probabilmente è un qualche leader locale…"


translate italian day2_main1_b6ab0cdc:


    sl "Stai sognando di nuovo ad occhi aperti?"


translate italian day2_main1_cc0015de:


    "Slavya mi ha riportato alla realtà."


translate italian day2_main1_0ceaa470:


    "Olga Dmitrievna era lì vicino."


translate italian day2_main1_31af9f80:


    mt "Ti ricordi il piano della settimana?"


translate italian day2_main1_b54906d2:


    me "Il piano?{w} Ma certo, come potrei scordarmi del Piano!"


translate italian day2_main1_003878d1:


    mt "Perfetto!"


translate italian day2_main1_a7925aca:


    "Ha guardato Slavya."


translate italian day2_main1_0e306996:


    mt "Ce l'hai?"


translate italian day2_main1_8b01f8be:


    sl "Sì."


translate italian day2_main1_b8640c11:


    "Slavya mi ha consegnato un foglio di carta."


translate italian day2_main1_5c374a56:


    mt "È la lista delle cose da fare. Ci sono quattro compiti che ti sono stati assegnati. Devi farli tutti oggi."


translate italian day2_main1_ffebe5e8:


    mt "Prima di iniziare, iscriviti a uno dei circoli. Ce ne sono alcuni nella sede principale, e un circolo musicale in un edificio a parte."


translate italian day2_main1_76437f4d:


    mt "Poi vai all'infermeria."


translate italian day2_main1_1d88b658:


    mt "E infine, visita la biblioteca."


translate italian day2_main1_f2150eaa:


    mt "Tutto chiaro?"


translate italian day2_main1_fc30fd2b:


    me "Sì."


translate italian day2_main1_a43db633:


    "Quella lista sembrava una buona occasione per poter scoprire qualcosa, dato che riguardava luoghi che non avevo ancora visitato."


translate italian day2_main1_d0722a98:


    mt "Dai allora, comincia subito."


translate italian day2_main1_abacb833:


    me "E il pranzo?"


translate italian day2_main1_794c528c:


    mt "Non preoccuparti! Ti porterò altri tramezzini. La lista è più importante!"


translate italian day2_main1_1ef3d9f1:


    sl "Buona fortuna."


translate italian day2_main1_167e9f8c:


    "Se ne sono andate così di fretta che non sono riuscito a chiedere altro."


translate italian day2_main1_6866c9c8:


    th "Ho saltato la colazione, e adesso salterò pure il pranzo."


translate italian day2_main1_ce0b356d:


    th "Questo non va bene."


translate italian day2_main1_d2a48ffa:


    th "Forse sono ancora in tempo?"


translate italian day2_main1_bb3dc1b6:


    th "Il pranzo inizia all'una di pomeriggio.{w} Ma se ci andassi potrei non avere più tempo per visitare uno dei luoghi indicati nella lista."


translate italian day2_main1_7a51eb9a:


    th "Ok, è comunque troppo presto per andare alla mensa!"


translate italian day2_musclub_5ea37963:


    "Il circolo musicale, un piccolo edificio monopiano, si trovava ad una certa distanza dagli altri edifici del Campo."


translate italian day2_musclub_69443bca:


    "Ho aperto la porta e sono entrato senza esitazione."


translate italian day2_musclub_8cb5bcfe:


    "C'erano abbastanza strumenti per un'orchestra intera: tamburi, chitarre, persino un pianoforte."


translate italian day2_musclub_aa3ba673:


    "Ho trascorso un po' di tempo a osservare da vicino ogni singolo strumento, cercando di capire di che periodo fossero, quando improvvisamente ho sentito il rumore di un qualcosa che strisciava sotto il pianoforte."


translate italian day2_musclub_babe6137:


    th "Una ragazza. Sembra che stia cercando qualcosa."


translate italian day2_musclub_baf9ce11:


    "Era a quattro zampe in una posa così allusiva che ho esitato ad aprir bocca."


translate italian day2_musclub_559ab9c6:


    me "Ehm, scusa…"


translate italian day2_musclub_0fe0c850:


    mip "Aaaah! Chi c'è?"


translate italian day2_musclub_4391282c:


    "Ha cercato di alzarsi in piedi ma il pianoforte non gliel'ha consentito."


translate italian day2_musclub_250ae924:


    mip "Ahia!"


translate italian day2_musclub_645f668d:


    "Con qualche difficoltà è riuscita a sfilarsi da là sotto."


translate italian day2_musclub_cc2dd0eb:


    me "Scusa se ti ho spaventata…"


translate italian day2_musclub_021d4634:


    mip "Fa niente! Oh, hai una lista, devi essere nuovo qui, vero?"


translate italian day2_musclub_ec29382e:


    me "Eh? Sì."


translate italian day2_musclub_46ce9eb5:


    mip "Il mio nome è Miku."


translate italian day2_musclub_2f3d5d1f:


    mi "No, davvero, non sto scherzando! Nessuno mi crede, ma è il mio vero nome. Mia madre è giapponese. Mio padre la incontrò quando stava costruendo… beh, non stava esattamente costruendo, è un ingegnere…"


translate italian day2_musclub_9fae511d:


    mi "E lì stava lavorando a un impianto nucleare! O era una diga… o forse un ponte…? Ah, non importa!"


translate italian day2_musclub_222c3455:


    "Parlava così velocemente che si era mangiata metà delle parole che voleva pronunciare."


translate italian day2_musclub_ec7942bc:


    me "Io sono Semyon."


translate italian day2_musclub_d2c6efe6:


    mi "Bene! Vuoi iscriverti al nostro circolo? Ci sono solo io adesso, ma se ti iscrivi saremo in due! Sei capace di suonare qualche strumento?"


translate italian day2_musclub_00ccccca:


    "Da quando ero diventato antisociale mi ero comprato una chitarra e avevo imparato qualche accordo, ma poi ci ho rinunciato – dato che ero solito abbandonare qualsiasi cosa richiedesse più di qualche ora del mio tempo."


translate italian day2_musclub_dd80a4a7:


    me "Beh sai, in realtà non avevo intenzione di fare nulla del genere…"


translate italian day2_musclub_946e96fa:


    mi "Oh non importa, ti insegno io! Magari la trombetta? O forse il violino? Modestamente, li conosco tutti!"


translate italian day2_musclub_b2395a19:


    "Non c'era motivo di discutere con la ragazza dell'orchestra, dato che sarei stato sommerso da un'altra ondata di parole."


translate italian day2_musclub_3783ff78:


    me "Ehi, ci penso su, intanto potresti firmare qui?"


translate italian day2_musclub_b5f95324:


    mi "Sì sì, certo, da' qua! Ricordati di ripassare qui! So anche cantare! Ti posso cantare qualche canzone tipica giapponese. O magari se non ti piacciono, qualcosa di più moderno?"


translate italian day2_musclub_1c12d36f:


    me "Ceeerto… grazie, adesso devo andare!"


translate italian day2_musclub_045fe4ca:


    mi "Certo, torna quando vuoi…"


translate italian day2_musclub_f2b7b54b:


    "Le ultime parole della sua frase sono rimaste dentro assieme a lei."


translate italian day2_musclub_2f7d7510:


    "Potrebbe essere bello starsene con la chitarra la sera, ma con una compagnia del genere…"


translate italian day2_musclub_2c9e7ab2:


    "Stavo per andarmene quando mi sono imbattuto in Alisa."


translate italian day2_musclub_db45ba49:


    "Mi ha squadrato con sospetto."


translate italian day2_musclub_5c36e901:


    dv "Che ci fai qui?"


translate italian day2_musclub_77321a84:


    me "La lista…"


translate italian day2_musclub_f2265658:


    dv "L'hai fatta firmare?"


translate italian day2_musclub_9513cd87:


    me "Sì…"


translate italian day2_musclub_6eb94c13:


    dv "Allora muoviti!"


translate italian day2_musclub_d8a05e0a:


    "Alisa è entrata, e io mi sono affrettato a lasciare quel luogo."


translate italian day2_clubs_fe574fd8:


    "Sono andato alla sede dei circoli."


translate italian day2_clubs_a3b49ee7:


    "A dire il vero, non mi sono mai piaciute le attività extrascolastiche."


translate italian day2_clubs_352ba995:


    "Quando andavo a scuola trovavo sempre qualche scusa per saltarle. E all'università non mi interessava partecipare al consiglio degli studenti."


translate italian day2_clubs_2acddae0:


    "Non mi interessava la boxe, il modellismo, o il cucito."


translate italian day2_clubs_edf42e23:


    "Quindi sono venuto qui solo per spuntare la casella."


translate italian day2_clubs_7631c2b8:


    "Dentro non c'era nessuno."


translate italian day2_clubs_68cc9679:


    "Mi sono trovato all'interno di una specie di rifugio di un piccolo appassionato di robotica: c'erano dei cavi e dei circuiti stampati sparsi ovunque, dei microchip, e sul tavolo risiedeva orgogliosamente un oscilloscopio."


translate italian day2_clubs_58c977db:


    "Ho sentito delle voci provenire dalla stanza accanto, e poi due pionieri sono apparsi."


translate italian day2_clubs_40997bda:


    "Uno era Electronik, l'altro non lo conoscevo."


translate italian day2_clubs_42afd4b2:


    el "Ciao, Semyon! Ti stavamo aspettando."


translate italian day2_clubs_4d5cf736:


    th "Sembra che lui sappia sempre tutto di tutti…"


translate italian day2_clubs_3dbd6055:


    me "Perché mi stavate aspettando?"


translate italian day2_clubs_9069fff4:


    el "Beh è ovvio, perché vuoi iscriverti al nostro circolo di cibernetica, non è vero?"


translate italian day2_clubs_1a229aa6:


    "Non mi ha lasciato rispondere."


translate italian day2_clubs_3db9f783:


    el "E questo è Shurik, è lui il capo qui!"


translate italian day2_clubs_b5112857:


    me "Suppongo siate solo voi due in questo circolo?"


translate italian day2_clubs_f6f084ef:


    el "Beh, adesso puoi dire che siamo in tre."


translate italian day2_clubs_c4b2274f:


    "Shurik si è fatto avanti e mi ha porto la mano con assertività."


translate italian day2_clubs_75eec175:


    "Il suo volto era in qualche modo familiare."


translate italian day2_clubs_93f251d0:


    sh "Benvenuto nel nostro circolo!"


translate italian day2_clubs_751ac511:


    me "Già…"


translate italian day2_clubs_b9d16cf3:


    el "Ora, ti mostro cosa abbiamo qui!{w} Fa' come se fossi a casa tua."


translate italian day2_clubs_4b6d8fbc:


    me "Ehm, ragazzi, sono venuto solo per…"


translate italian day2_clubs_e600b6dc:


    sh "I nuovi membri sono sempre i benvenuti."


translate italian day2_clubs_ac94a513:


    "L'ha detto in un modo tale che improvvisamente l'inno nazionale dell'Unione Sovietica ha iniziato a risuonare nella mia testa."


translate italian day2_clubs_8aaf9ff7:


    "È incredibile, mi ricordavo persino le parole – quando andavo a scuola avevo un quaderno con il testo dell'inno scritto sul retro."


translate italian day2_clubs_c82f982c:


    me "Beh, no, in realtà avrei solo bisogno della vostra firma per la lista."


translate italian day2_clubs_4a94d273:


    el "Ok, iscriviti al nostro circolo e firmeremo la tua lista."


translate italian day2_clubs_1438f764:


    "Ha sorriso sornione."


translate italian day2_clubs_01f5a268:


    "Mi stavo preparando per una lunga e noiosa discussione, quando ho sentito qualcuno entrare."


translate italian day2_clubs_63509643:


    "Mi sono girato e ho visto Slavya."


translate italian day2_clubs_131a6aad:


    sl "Ah, Semyon! Spero non ti stiano dando problemi?"


translate italian day2_clubs_cf9000b4:


    "Ha stretto gli occhi, guardando severamente le due future stelle dell'industria robotica."


translate italian day2_clubs_f47d1028:


    sl "Conosco bene questi due!"


translate italian day2_clubs_705bab2b:


    me "Beh, sai, in realtà mi servirebbe solo una firma per la mia lista…"


translate italian day2_clubs_f0bcf20a:


    "Ho deciso di approfittare della situazione."


translate italian day2_clubs_cdedcb52:


    sl "Nessun problema, da' qua."


translate italian day2_clubs_094e2e65:


    "Slavya ha preso il foglio e ha marciato verso Shurik."


translate italian day2_clubs_ec58ff09:


    sl "Firmalo!"


translate italian day2_clubs_204589a0:


    sh "Aspetta, non abbiamo ancora finito…"


translate italian day2_clubs_0e869d11:


    sl "Sì invece! Firmalo subito!"


translate italian day2_clubs_8ceef228:


    "Ha lanciato a Shurik uno sguardo talmente minaccioso che gli ha fatto perdere ogni possibile obiezione."


translate italian day2_clubs_845789bc:


    "Shurik ha scritto degli scarabocchi sul foglio, ho ringraziato Slavya, e poi me ne sono andato, di buonumore."


translate italian day2_library_89cf3d9c:


    "A dire il vero adoro leggere – ma trascorrere i miei giorni in una biblioteca nelle attuali circostanze non era nei miei piani."


translate italian day2_library_651b3815:


    "E quindi dovevo completare questa «tappa» il prima possibile."


translate italian day2_library_3113f296:


    "Non appena sono entrato, mi è ritornato in mente un ricordo della mia infanzia."


translate italian day2_library_dc667c83:


    "Era molto vivido."


translate italian day2_library_c1f61108:


    "Avevo sette o otto anni, ero nella biblioteca con mia madre."


translate italian day2_library_c0052ab2:


    "Mentre lei stava cercando i libri che mi servivano per la scuola, io rimasi seduto in un angolo osservando una collezione di fumetti."


translate italian day2_library_740b504e:


    "A quei tempi non capivo perché ne avessero così tanti, o perché non potessi portarmene a casa alcuni."


translate italian day2_library_10b93d8b:


    "Il concetto di proprietà privata era qualcosa che la mia mente non aveva ancora ben capito a quell'età."


translate italian day2_library_71daf897:


    "Tuttavia, in quel periodo il solo concetto di proprietà era ancora vago per me."


translate italian day2_library_26efdcce:


    "Questo ricordo mi sembrava ancora più strano ora che mi trovavo in questo Campo, dove potrebbero aver costruito il comunismo «in soli tre anni»."


translate italian day2_library_faaf4baa:


    "Il simbolismo sovietico si poteva scorgere in ogni angolo, e gli scaffali erano pieni di libri di quel tipo."


translate italian day2_library_dd1940c8:


    "Ovviamente non avevo intenzione di leggere quella roba. Fare conoscenza con un'intera collezione di testi Marxisti era l'ultima cosa al mondo che volessi fare."


translate italian day2_library_86d9517e:


    th "Ma dov'è il bibliotecario?"


translate italian day2_library_a5dd78db:


    "Non ho impiegato molto tempo a trovarla."


translate italian day2_library_075aa233:


    "Ho guardato più da vicino.{w} Capelli corti, occhiali spessi, un viso piuttosto carino."


translate italian day2_library_74556d4c:


    "Stava russando così beatamente che non ho potuto svegliarla."


translate italian day2_library_4112cbd4:


    th "Posso aspettare. Se non si sveglierà nella prossima mezz'ora, allora forse…"


translate italian day2_library_cdaec1c2:


    "Non avendo niente da fare, ho preso un libro a caso dallo scaffale più vicino."


translate italian day2_library_48a28d8c:


    "Arthur Schopenhauer, «Il mondo come volontà e rappresentazione»."


translate italian day2_library_78cf63d0:


    "L'ho aperto all'incirca nel mezzo e ho cominciato a leggere:"


translate italian day2_library_d7ca3dbf:



    nvl clear
    "L'esistenza umana, colmata da infinita fatica, povertà e sofferenza, dev'essere considerata come la spiegazione e la parafrasi dell'atto procreativo, o l'assoluta dichiarazione del desiderio di vivere; essa è inoltre la ragione per cui l'uomo deve ripagare la natura con la morte, in perenne ansia per il suo debito."


translate italian day2_library_cc4bf7fc:


    "Non è forse la prova che la nostra esistenza comporti colpevolezza?"


translate italian day2_library_26562ac7:


    "Tuttavia, l'essere umano è eterno, ogni tanto paga con la morte il dono della nascita, sperimentando in alternanza tutte le gioie e i dolori della vita, costretto a subirne gli effetti – è questo il risultato dell'approvazione della volontà di vivere."


translate italian day2_library_4cfe9a45:



    nvl clear
    "Così la paura della morte, che ci tiene saldamente legati ad essa a dispetto di tutte le miserie della vita, è veramente illusoria; ma altrettanto illusorio è l'impulso che ci ha attratto ad essa."


translate italian day2_library_0af732d3:


    "Questa attrazione di per sé potrebbe essere considerata obiettivamente come l'intrecciarsi degli sguardi bramosi di due amanti; essi sono l'espressione più pura della volontà di vivere, nella sua affermazione. Quanto è dolce e tenero!"


translate italian day2_library_a5a1d951:


    "Essa desidera il benessere, il calmo e tranquillo godimento della gioia per sé stessi, per gli altri, e per tutto il resto."


translate italian day2_library_7e8c00bf:


    "Qualcuno ha bussato alla porta."


translate italian day2_library_919288d5:


    "Ho chiuso rapidamente il libro e l'ho rimesso al suo posto."


translate italian day2_library_a7a56164:


    th "Ma che bella abitudine – bussare alla porta.{w} Dovrei farlo anch'io."


translate italian day2_library_4432512e:


    "Era Lena."


translate italian day2_library_adda1c0b:


    un "Oh…"


translate italian day2_library_1a8f3935:


    me "Ciao!"


translate italian day2_library_2f04a7f9:


    "Ho sorriso."


translate italian day2_library_ca64d35c:


    un "Ciao, io, ehm, volevo solo restituire un libro…"


translate italian day2_library_04118915:


    "In mano teneva la copia di «Via col Vento» che avevo visto ieri."


translate italian day2_library_b946894f:


    un "Oh, Zhenya sta dormendo, torno dopo…"


translate italian day2_library_6ec572fc:


    mz "Sono sveglia."


translate italian day2_library_50100392:


    "Mi sono voltato sorpreso e l'ho osservata."


translate italian day2_library_3e33ac87:


    "Mi ha squadrato da dietro la sua scrivania."


translate italian day2_library_4b484fa7:


    mz "Che cosa vuoi?"


translate italian day2_library_51266558:


    me "Mi servirebbe la tua firma qui…"


translate italian day2_library_c7f48bc6:


    mz "Da' qua."


translate italian day2_library_486518bc:


    "Ha firmato rapidamente il foglio e me l'ha riconsegnato."


translate italian day2_library_eb72bb13:


    "Aveva una tale espressione in volto che non ho osato dire altro."


translate italian day2_library_9db07b02:


    "Lena le si è avvicinata per restituire il libro, ho ringraziato Zhenya e me ne sono andato."


translate italian day2_aidpost_30420f41:


    th "Che senso ha visitare l'infermeria?"


translate italian day2_aidpost_37fed0c1:


    "Non avevo problemi di salute, l'aria di questo luogo mi aveva certamente giovato – mi sentivo più fresco di sempre."


translate italian day2_aidpost_513b3994:


    th "Ma sono costretto ad andarci."


translate italian day2_aidpost_92c7a992:


    "Sono entrato."


translate italian day2_aidpost_b0f75288:


    th "Una normale infermeria, come quella del mio medico scolastico."


translate italian day2_aidpost_cbdb3f5c:


    "Una donna di mezza età era seduta alla scrivania.{w} Ovviamente, era l'infermiera."


translate italian day2_aidpost_7b8718b9:


    "Mi ha osservato attentamente, squadrandomi mentre continuava a scrivere qualcosa."


translate italian day2_aidpost_7cce673f:


    csp "Beh, ciao… pioniere."


translate italian day2_aidpost_f14de383:


    "Mi ha detto, senza distrarsi dal suo lavoro."


translate italian day2_aidpost_b5fe60d8:


    me "Buongiorno, signora… Avrei qualcosa da…"


translate italian day2_aidpost_a3450ccf:


    csp "Siediti."


translate italian day2_aidpost_9fa1bef7:


    "Mi sono guardato intorno."


translate italian day2_aidpost_40098e3a:


    csp "Sul lettino."


translate italian day2_aidpost_04d910fd:


    "Mi sono seduto."


translate italian day2_aidpost_89b67c83:


    csp "Spogliati."


translate italian day2_aidpost_a9281d57:


    "Ha proseguito, in tono piatto."


translate italian day2_aidpost_f43fc4fb:


    me "E perché...?"


translate italian day2_aidpost_9e007bed:


    csp "Per controllarti, per ispezionarti, per valutare la tua salute, sai."


translate italian day2_aidpost_fa5c927d:


    csp "Comunque, il mio nome è Violetta, ma puoi chiamarmi Viola."


translate italian day2_aidpost_548e0299:


    "Si è voltata verso di me."


translate italian day2_aidpost_d4984f4c:


    cs "Che stai aspettando? Spogliati."


translate italian day2_aidpost_f4cd982b:


    me "Ma non ho problemi di salute. Ho questo…"


translate italian day2_aidpost_c2002e32:


    "Le ho consegnato il foglio."


translate italian day2_aidpost_c5b50119:


    cs "Più tardi."


translate italian day2_aidpost_116c6a89:


    "Si è tolta lo stetoscopio dal collo, come se volesse ispezionarmi."


translate italian day2_aidpost_89132ea0:


    "Ma poi qualcuno ha bussato alla porta."


translate italian day2_aidpost_1128b7a1:


    "L'infermiera ha risposto controvoglia:"


translate italian day2_aidpost_2e8aea9e:


    cs "Avanti!"


translate italian day2_aidpost_75d12839:


    "Dopo un istante la porta si è spalancata ed Electronik si è precipitato dentro."


translate italian day2_aidpost_7220ee64:


    el "Salve! Io… sono caduto durante la partita di calcio. Non è niente, sto bene, ma Olga Dmitrievna…"


translate italian day2_aidpost_5741b364:


    "C'era un grosso livido sotto al suo occhio."


translate italian day2_aidpost_efefe224:


    th "Dubito che fosse dovuto a un infortunio sul campo da calcio."


translate italian day2_aidpost_13a0ecdb:


    cs "Siediti, ti darò un'occhiata."


translate italian day2_aidpost_07c0e46a:


    "Gli ha detto."


translate italian day2_aidpost_8855685b:


    cs "E tu, dammi la tua lista."


translate italian day2_aidpost_d2d6a28b:


    "L'infermiera ha firmato velocemente e ha detto:"


translate italian day2_aidpost_42b88054:


    cs "Se ti fa male qualcosa, vieni immediatamente da me… pioniere."


translate italian day2_aidpost_159b2e86:


    "Ho deciso di non rispondere e sono uscito, chiudendo la porta alle mie spalle."


translate italian day2_aidpost_9ff8721b:


    th "Quell'infermiera è davvero strana…"


translate italian day2_dinner_c6f8dc13:


    "Ho deciso di andare a pranzo."


translate italian day2_dinner_c8dab275:


    th "La lista non scapperà (la farò firmare più tardi), mentre il mio stomaco non può di certo aspettare fino a cena."


translate italian day2_dinner_92787784:


    "Quei pensieri mi hanno accompagnato alla mensa."


translate italian day2_dinner_5f1442eb:


    "Ormai non c'era quasi nessuno – sembrava che la maggioranza avesse già pranzato."


translate italian day2_dinner_79c74b45:


    "Una cuoca dalle proporzioni impressionanti mi ha portato un glorioso pranzo su tre piatti: una zuppa «Ypres broth», un gulasch «alla Lavrentiy Pavlovich» guarnito di patate bollite, secondo la più folle moda del quindicesimo secolo, e un kompot «Tavola Periodica»."


translate italian day2_dinner_3f63379f:


    "Difficilmente avrebbe fatto invidia a un ristorante a cinque stelle, ma ero troppo affamato per potermi preoccupare di ciò."


translate italian day2_dinner_0327cad9:


    "Dopotutto non era così male, se confrontato con la mia tipica porzione di carne e ramen o pasta al formaggio."


translate italian day2_dinner_a677d27c:


    "Mi sono seduto al primo tavolo libero e ho iniziato a mangiare."


translate italian day2_dinner_a5341529:


    "La mia concentrazione è stata subito interrotta da un improvviso schiaffo sulla mia schiena, che mi ha fatto quasi soffocare."


translate italian day2_dinner_0d703f5f:


    "Ulyana stava in piedi davanti a me, con espressione trionfante."


translate italian day2_dinner_8de67373:


    me "Ti faccio vedere io uno di questi giorni!"


translate italian day2_dinner_d8dece45:


    us "Prendimi se riesci!"


translate italian day2_dinner_b4cfaed8:


    "Mi ha fatto la linguaccia!"


translate italian day2_dinner_0a675d44:


    us "Hai già provato una volta – non ci sei riuscito."


translate italian day2_dinner_abd93e7a:


    me "D'accordo, allora ti farò un'imboscata da qualche parte!"


translate italian day2_dinner_57a622de:


    us "Così non vale!"


translate italian day2_dinner_fccbe21c:


    me "Ma senti chi parla, la signorina «Correttezza»!"


translate italian day2_dinner_71fc19fd:


    "Ho sorriso."


translate italian day2_dinner_e00339d0:


    us "Ok, aspettami qui, vado a prendere qualcosa da mangiare, e poi pranziamo insieme."


translate italian day2_dinner_822ea040:


    "Avrei preferito farlo senza la sua compagnia. E quindi dovevo affrettarmi a finire."


translate italian day2_dinner_2d5c9aae:


    "Ma purtroppo Ulyana è tornata dopo appena mezzo minuto."


translate italian day2_dinner_d8e82882:


    "Aveva un enorme arrosto di manzo sul piatto, con attorno grosse patate bollite."


translate italian day2_dinner_b90cc3bd:


    "In confronto al mio «banchetto reale»…"


translate italian day2_dinner_bff8415e:


    me "Ma come hai... Da dove hai preso tutta quella roba?"


translate italian day2_dinner_2da2c81a:


    us "Devi sapere dove cercare!"


translate italian day2_dinner_1d43f87e:


    "Mi ha guardato e ha sorriso con tutti i suoi trentadue… o quanti ne avesse… denti."


translate italian day2_dinner_d05806b0:


    th "Non posso tollerare un simile insulto!"


translate italian day2_dinner_47797bcf:


    "Non sono mai stato bravo nel fare scherzi, e a scuola spesso mi trovavo nei panni della vittima."


translate italian day2_dinner_e27a02d2:


    "Ma dovevo fargliela pagare in qualche modo."


translate italian day2_dinner_5dafbcae:


    me "E se Olga Dmitrievna scoprisse che stai rubando dalla mensa?"


translate italian day2_dinner_d7d48f29:


    us "Non sto rubando!"


translate italian day2_dinner_1678d751:


    "Si è ribellata Ulyana."


translate italian day2_dinner_fc822a9d:


    me "Questo è quello che le diresti. Ma pensi che ti crederebbe?"


translate italian day2_dinner_88a1ff3e:


    us "E come potrebbe scoprirlo?!"


translate italian day2_dinner_911579da:


    me "Beh… dipende… da molte cose."


translate italian day2_dinner_ac96beb1:


    us "Ad esempio?"


translate italian day2_dinner_1b595140:


    "Ulyana mi ha guardato negli occhi."


translate italian day2_dinner_25334771:


    me "Portami un panino. Uno dolce!"


translate italian day2_dinner_47228a37:


    us "E dove posso trovarlo?"


translate italian day2_dinner_430fbe26:


    me "Beh, forse nello stesso posto dove hai preso tutta quella roba?"


translate italian day2_dinner_930035ce:


    "Ho indicato il suo piatto."


translate italian day2_dinner_35913bdd:


    "Ha esitato un istante."


translate italian day2_dinner_b9e1270b:


    us "D'accordo. Un panino!"


translate italian day2_dinner_0cd3249d:


    us "E prometti di non dirlo a Olga Dmitrievna!"


translate italian day2_dinner_23e8f891:


    me "Hai la mia parola, da vero Pioniere!"


translate italian day2_dinner_0b374dce:


    "È corsa verso la cucina. Senza pensarci due volte ho aperto il contenitore del pepe e l'ho svuotato nella sua bevanda."


translate italian day2_dinner_99986110:


    "Non appena ho finito, la instancabile ragazzina è tornata."


translate italian day2_dinner_5c02fe6a:


    us "Ecco qua! Ricattatore!"


translate italian day2_dinner_d22005bb:


    th "Pare non essersene accorta."


translate italian day2_dinner_c0146da9:


    me "Ok. Ora, chi finisce per ultimo il suo kompot dovrà portare indietro i vassoi."


translate italian day2_dinner_07366c5a:


    us "Non fare lo stupido!"


translate italian day2_dinner_7f82c019:


    me "Stupido? Guarda qua."


translate italian day2_dinner_78abf728:


    us "Non faccio questi giochetti da bambini."


translate italian day2_dinner_d4d00518:


    me "Senti chi parla!"


translate italian day2_dinner_6437d74d:


    "Ho sorriso malignamente."


translate italian day2_dinner_8f15074d:


    us "Ah! Guarda {i}me{/i} allora! Uno, due, tre!"


translate italian day2_dinner_d16b7597:


    "Non sono nemmeno riuscito ad afferrare il mio bicchiere, che con una rapida mossa lei aveva già ingurgitato tutta la sua bevanda."


translate italian day2_dinner_dc6679ad:


    "In un secondo la sua espressione si è tramutata in autentico terrore, le sue guance sono diventate rosse e i suoi occhi stavano per uscire dalle orbite."


translate italian day2_dinner_c1908ed9:


    "È saltata dalla sedia e si è precipitata al tavolo dove c'era l'acqua, urlando e sputando tutt'attorno."


translate italian day2_dinner_053a22e5:


    us "Tu! Tu! Tu…"


translate italian day2_dinner_b225e29e:


    "Ho deciso di non aspettarla e sono uscito, ridacchiando mentre finivo il mio panino."


translate italian day2_main2_a20cefa7:


    "..."


translate italian day2_main2_dfe2adc7:


    "Finalmente avevo raccolto tutte le firme. Adesso non mi restava che tornare da Olga Dmitrievna e consegnarle la lista."


translate italian day2_main2_98a12405:


    "Stava seduta davanti alla sua casetta, leggendo un libro."


translate italian day2_main2_bb12859e:


    "Non sembrava stesse dando il buon esempio al pioniere modello che voleva farmi diventare."


translate italian day2_main2_281d80a7:


    "Mi chiedo quali fossero le sue responsabilità a parte il fare discorsi infuocati all'allineamento, rimproverare Ulyana e occuparsi della mia crescita morale, fisica e ideologica."


translate italian day2_main2_482db2cf:


    me "Ecco qua…"


translate italian day2_main2_7cce4a44:


    "Le ho consegnato la lista."


translate italian day2_main2_f913543d:


    "Se l'è messa in tasca senza nemmeno guardarla."


translate italian day2_main2_e0ce9392:


    th "Grandioso! Avrei potuto firmarla io stesso, senza dover andare da nessuna parte…"


translate italian day2_main2_7b44c8b1:


    mt "Perfetto! Allora, hai conosciuto la nostra infermiera?"


translate italian day2_main2_9513cd87:


    me "Sì…"


translate italian day2_main2_ee81ac0c:


    "Per qualche motivo quella domanda mi ha fatto rabbrividire."


translate italian day2_main2_342a300f:


    mt "A quale circolo ti sei iscritto?"


translate italian day2_main2_8ba55fc5:


    me "A nessuno… Devo ancora pensarci."


translate italian day2_main2_af5748f9:


    mt "È un peccato. È importante che tu ti iscriva a qualcosa entro domani!"


translate italian day2_main2_02398cce:


    th "Ma certo, è ovvio!"


translate italian day2_main2_5d6ded1e:


    mt "Bene, è ora di cena."


translate italian day2_main2_b59eddaa:


    th "Finalmente! Ho proprio fame."


translate italian day2_main2_4ff192ad:


    "Siamo andati insieme alla mensa."


translate italian day2_main2_de8fec0b:


    "Ho guardato il cielo, il sole stava già tramontando."


translate italian day2_main2_962703d4:


    "Sulla veranda c'erano Alisa, Electronik..."


translate italian day2_main2_cb3d2e1e:


    "Ulyana e Slavya."


translate italian day2_main2_30f5e6dc:


    "Appena siamo arrivati, ho ascoltato il loro discorso:"


translate italian day2_main2_f04cac09:


    dv "E non chiamarmi più DvaChe, altrimenti le prendi di nuovo!"


translate italian day2_main2_a8e77ffd:


    el "Non ti ho chiamata così! Ti stai immaginando le cose!"


translate italian day2_main2_689622eb:


    us "L'ha fatto, l'ha fatto! Ho sentito tutto!"


translate italian day2_main2_4c435994:


    el "Ma se non c'eri nemmeno!"


translate italian day2_main2_ab85c187:


    us "C'ero, c'ero! Nei cespugli!"


translate italian day2_main2_992ca12a:


    sl "E dai ragazzi! Smettetela!"


translate italian day2_main2_75a4c8b7:


    th "Quindi l'occhio nero di Electronik non era dovuto a una caduta."


translate italian day2_main2_cdbfd744:


    th "L'infermiera ha fatto un buon lavoro – l'occhio nero non si vede più!"


translate italian day2_main2_03ada258:


    "Olga Dmitrievna li ha raggiunti e ha chiesto chiarimenti riguardo a quel putiferio:"


translate italian day2_main2_4bd88eb6:


    mt "Cos'è successo?"


translate italian day2_main2_b07cfe09:


    sl "Alisa e Cheesekov…"


translate italian day2_main2_ecff4d02:


    dv "Io non ho fatto niente!"


translate italian day2_main2_6d1704fc:


    "Si è stretta nelle spalle con antipatia ed è entrata nella mensa."


translate italian day2_main2_f38aed46:


    mt "Bene. È ora di cena."


translate italian day2_main2_6c8ce019:


    "Sono entrato per ultimo."


translate italian day2_main2_84208ced:


    "Non c'erano molti posti liberi."


translate italian day2_main2_26bbcb23:


    "C'erano un po' di sedie libere vicino ad Alisa, alcune sparse per la mensa, ma preferivo morire di fame piuttosto che rischiare la mia vita in sua compagnia."


translate italian day2_main2_c136a510:


    "C'era anche un posto libero vicino a Ulyana, ma non mi piaceva la cucina tradizionale cinese delle «cose striscianti»."


translate italian day2_main2_7c6eeade:


    "E infine, una sedia vuota accanto a Miku."


translate italian day2_main2_1a652dc4:


    "A quanto pare dovevo scegliere io stesso il mio veleno…"


translate italian day2_main2_8b9ac1bf:


    me "Ti spiace se mi siedo qui?"


translate italian day2_main2_a9efde1c:


    mi "Oh sì, certo! Voglio dire, no, non mi spiace! Cioè sì, puoi sederti!"


translate italian day2_main2_04d910fd:


    "Mi sono seduto."


translate italian day2_main2_6ec2d5c6:


    mi "Guarda, oggi c'è il grano saraceno. Ti piace il grano saraceno? E il pollo! Non mi piace il pollo. Beh, non è che non mi piaccia..."


translate italian day2_main2_a2e3290c:


    mi "Ma se me lo chiedessi, cosa preferirei, diciamo, manzo alla Stroganoff o ragù… No, forse un hamburger! O magari una bistecca di girello! Ti piace la bistecca di girello?"


translate italian day2_main2_f14d5492:


    me "Non sono così esigente riguardo al mio cibo."


translate italian day2_main2_ea0a7a01:


    "E quella è la pura verità."


translate italian day2_main2_af2e7a5f:


    mi "Oh davvero? Ma i dessert, sai, non sono molto buoni qui. A me piace il gelato! Ti piace il gelato? Io adoro il «48 kopeks», e il «Leningradskoe»! Oh, scusa, continuo a parlare solo di me stessa!"


translate italian day2_main2_9d46cf08:


    mi "Forse tu preferisci l'eskimo?"


translate italian day2_main2_14b91769:


    "La cena stava cominciando a darmi sui nervi, grazie a una compagnia del genere."


translate italian day2_main2_28655f21:


    "E io non sono il tipo di persona che riesce a ignorare qualcuno che mi sta parlando.{w} Persino lei."


translate italian day2_main2_c7c2dae1:


    th "Dopotutto siamo allo stesso tavolo."


translate italian day2_main2_f0878075:


    mi "Sai, una volta ho comprato un cono di gelato e ho iniziato a mangiarlo, e sai cosa è successo? Ho trovato una vite dentro! Una vera vite, t'immagini? O forse era un bullone? Non ricordo esattamente."


translate italian day2_main2_791bb7e0:


    mi "Le viti sono quelle che giri con il cacciavite, i bulloni sono quelli che giri con la chiave inglese, giusto?"


translate italian day2_main2_24441c77:


    th "Se questa fosse una gara a chi mangia più velocemente, sicuramente sarei già sul podio."


translate italian day2_main2_6cbc801e:


    me "Ok, devo andare, buon appetito!"


translate italian day2_main2_91330d37:


    "Mi sono alzato e sono uscito."


translate italian day2_main2_a6b59989:


    "Miku mi stava ancora dicendo qualcosa, ma le sue parole sono affogate nel mare di voci della mensa."


translate italian day2_main2_757dd6ab:


    "Sono uscito e mi sono seduto sugli scalini, per digerire la cena."


translate italian day2_main2_bbb07ad8:


    "Me ne stavo seduto lì a guardare la notte incombere."


translate italian day2_main2_a85b2ea6:


    "Tutto era così pieno di vita durante il giorno: ragazzini che ridevano e gridavano felici correndo per il Campo, parlando in continuazione, c'erano giochi in corso e si poteva nuotare alla spiaggia."


translate italian day2_main2_bf72d174:


    "Ma quando calava l'oscurità, il Campo si trasformava completamente."


translate italian day2_main2_1732123e:


    "I suoni del giorno venivano rimpiazzati dal silenzio, interrotto occasionalmente dal frinire dei grilli, o dal canto di un uccello notturno."


translate italian day2_main2_0a2a1026:


    "Il Campo stava andando a dormire."


translate italian day2_main2_c328b2c5:


    "Ogni singola ombra nascondeva qualcosa – forse era un fantasma? O forse uno spirito della foresta? O magari un animale selvatico? Un essere umano è l'ultima cosa che ci si potrebbe aspettare."


translate italian day2_main2_c347461a:


    "È così che appariva la notte scorsa. E anche questa."


translate italian day2_main2_2ac17ce1:


    "La gente del posto seguiva la propria routine in modo rigoroso."


translate italian day2_main2_3a690eee:


    "Durante il giorno erano loro i padroni del Campo. Ma durante la notte, sembrava che tutto appartenesse più alle forze della natura."


translate italian day2_main2_0b5df81a:


    "Qualcuno mi ha toccato la spalla."


translate italian day2_main2_47eaf14e:


    "Mi sono girato."


translate italian day2_main2_49bd4154:


    "Era Electronik."


translate italian day2_main2_6385785b:


    el "Ti va di giocare a carte?"


translate italian day2_main2_f8ca8757:


    me "A carte?"


translate italian day2_main2_6d492a61:


    el "Sì! Ho inventato un nuovo gioco. È forte!"


translate italian day2_main2_bfe132d8:


    me "Forte? E quanto?"


translate italian day2_main2_ad0877c3:


    el "Beh, prima dobbiamo trovare le carte, poi ti dirò."


translate italian day2_main2_3c6dcd82:


    me "Allora vai a cercarle, che problema c'è?"


translate italian day2_main2_2adc32e8:


    el "Solo Olga Dmitrievna ce le ha, e di certo non me le darebbe…"


translate italian day2_main2_5ac38cf1:


    me "Perché no?"


translate italian day2_main2_6c259ad3:


    el "Beh ecco, l'altra volta quando stavamo…"


translate italian day2_main2_0f60fa5f:


    "Olga Dmitrievna e Slavya sono uscite sulla veranda."


translate italian day2_main2_8831b761:


    el "Olga Dmitrievna! Semyon voleva giusto chiederle se possiamo avere le carte!"


translate italian day2_main2_09a322d4:


    me "In realtà…"


translate italian day2_main2_458888bd:


    mt "A cosa vi servono?"


translate italian day2_main2_de85b885:


    el "Abbiamo inventato un nuovo gioco!"


translate italian day2_main2_aa6ae640:


    th "Non «abbiamo», «hai»."


translate italian day2_main2_c7f522d1:


    mt "Che gioco?"


translate italian day2_main2_3d77030f:


    el "Mi servono le carte per spiegarlo."


translate italian day2_main2_ee0157c7:


    mt "Hmm. Non mi piace questa cosa…{w} Ma se Semyon è con te, allora forse va bene…"


translate italian day2_main2_0d6093c8:


    me "A dire il vero io…"


translate italian day2_main2_93e80e0a:


    sl "Andremo noi a prenderle, Olga Dmitrievna!"


translate italian day2_cards_with_sl_a755f898:


    me "Se non ti dispiace…"


translate italian day2_cards_with_sl_547903c7:


    sl "Certo che no! Andiamo."


translate italian day2_cards_with_sl_956c974d:


    "Ci siamo incamminati verso la mia casetta."


translate italian day2_cards_with_sl_2d658ff8:


    "Più o meno a metà strada Slavya si è fermata."


translate italian day2_cards_with_sl_ca62c00d:


    sl "Ehi aspetta, mi sono appena ricordata che le carte sono nella mia stanza!"


translate italian day2_cards_with_sl_7c2541af:


    th "Tempismo perfetto."


translate italian day2_cards_with_sl_1b37703b:


    me "E dove si trova?"


translate italian day2_cards_with_sl_2015af9e:


    sl "In fondo a questa stradina, andiamo!"


translate italian day2_cards_with_sl_a20cefa7:


    "..."


translate italian day2_cards_with_sl_c8a2cf84:


    "Abbiamo raggiunto una casetta che sembrava più una roulotte."


translate italian day2_cards_with_sl_113ad468:


    sl "Aspetta solo un secondo, torno subito!"


translate italian day2_cards_with_sl_5fa71576:


    "Dopo pochi secondi è tornata."


translate italian day2_cards_with_sl_ca656115:


    sl "Ecco qua!"


translate italian day2_cards_with_sl_21c26363:


    "Mi ha mostrato un mazzo di carte piuttosto logore."


translate italian day2_cards_with_sl_24f3bb91:


    me "Queste carte devono essere segnate fuori e dentro."


translate italian day2_cards_with_sl_ccc884da:


    sl "È proprio antisportivo. Che fine ha fatto il fair play?!"


translate italian day2_cards_with_sl_74535a0c:


    "Dimmelo tu. È difficile imbrogliare quando non si conoscono le regole..."


translate italian day2_cards_with_sl_ad3d2e0c:


    sl "Andiamo?"


translate italian day2_cards_with_sl_304c9144:


    me "Andiamo."


translate italian day2_cards_with_sl_a20cefa7_1:


    "..."


translate italian day2_cards_with_sl_5316a3b2:


    "Mentre tornavamo ho provato a scoprire qualcosa."


translate italian day2_cards_with_sl_6f6dbf74:


    me "Da quanto sei qui?"


translate italian day2_cards_with_sl_4a08d89f:


    sl "In questo Campo? Da circa una settimana."


translate italian day2_cards_with_sl_237356a1:


    me "Capisco… E di dove sei?"


translate italian day2_cards_with_sl_9cbcee67:


    sl "Vengo dal Nord."


translate italian day2_cards_with_sl_5fd101b5:


    me "Cioè...?"


translate italian day2_cards_with_sl_3e5e8c80:


    sl "Il freddo Nord."


translate italian day2_cards_with_sl_943eaf3a:


    "Mi ha guardato e ha sorriso."


translate italian day2_cards_with_sl_a94c6d7f:


    th "Pare che nessuno in questo Campo abbia la minima intenzione di rispondere anche alla più innocente delle domande."


translate italian day2_cards_with_sl_9256c52d:


    "Ho voluto provare con qualcosa di diverso."


translate italian day2_cards_with_sl_f2292cea:


    me "E cosa ti piace?"


translate italian day2_cards_with_sl_63f53767:


    sl "Che intendi?"


translate italian day2_cards_with_sl_1b25eb7c:


    me "Beh, i tuoi interessi?"


translate italian day2_cards_with_sl_710d465d:


    sl "Oh… Mi piace la natura."


translate italian day2_cards_with_sl_fe5ee6e6:


    "Strano. Per qualche motivo oggi non era molto loquace."


translate italian day2_cards_with_sl_219d4457:


    me "La natura? Capisco.{w} Vuoi diventare una naturalista?"


translate italian day2_cards_with_sl_92f9746d:


    sl "Direi più uno storico. Mi ha sempre interessato la storia della nostra nazione."


translate italian day2_cards_with_sl_e357f0bd:


    "Sarebbe proprio adatto a lei."


translate italian day2_cards_with_sl_2efeef27:


    "Sembrava che di tutta la gente del posto, lei fosse l'unica senza nulla da nascondere."


translate italian day2_cards_with_sl_0a42064b:


    th "E se anche lei fosse finita qui come me, ma non lo volesse dire perché non si fida di nessuno?"


translate italian day2_cards_with_sl_300c021f:


    "Ho provato a testare le acque."


translate italian day2_cards_with_sl_6c6041ec:


    me "E perché hai scelto questo Campo?"


translate italian day2_cards_with_sl_a89e29ad:


    sl "Non l'ho fatto. I miei genitori hanno ottenuto un voucher dal lavoro."


translate italian day2_cards_with_sl_8105ae57:


    th "Un altro buco nell'acqua..."


translate italian day2_cards_with_sl_49976930:


    me "Beh, e se avessi potuto scegliere?"


translate italian day2_cards_with_sl_baad4a6d:


    sl "È bello qui!{w} Non credo che avrei scelto qualche altro posto – questo luogo ti fa diventare un'altra persona!"


translate italian day2_cards_with_sl_e9ca641d:


    "Non era come la vedevo io."


translate italian day2_cards_with_sl_d84daee3:


    me "Che intendi dire con «un'altra persona»?"


translate italian day2_cards_with_sl_b1f7a78f:


    sl "Voglio dire, hai così tante possibilità. Puoi imparare così tanto, incontrare nuove persone interessanti qui!"


translate italian day2_cards_with_sl_fcc6a9dc:


    "Adesso stava parlando come la nostra leader, e la cosa mi ha allarmato."


translate italian day2_cards_with_sl_30054c86:


    "Ho deciso di smetterla con le domande, per adesso."


translate italian day2_cards_with_sl_58bb1834:


    "Appena siamo tornati, Olga Dmitrievna si è rivolta a Slavya:"


translate italian day2_cards_with_sl_fc9fa381:


    mt "Mi sono appena ricordata che le carte sono nella tua stanza!"


translate italian day2_cards_with_sl_9446991e:


    sl "È tutto a posto, ce le abbiamo."


translate italian day2_cards_with_sl_74619cda:


    mt "Bene, bene."


translate italian day2_cards_without_sl_587eb799:


    me "Andrò io a prenderle…"


translate italian day2_cards_without_sl_888bedb3:


    mt "Bene. Sono nella mia stanza, nel cassetto."


translate italian day2_cards_without_sl_8b243a91:


    "Mi sono diretto verso l'abitazione di Olga Dmitrievna."


translate italian day2_cards_without_sl_9ff4064f:


    th "E poi perché ho accettato?"


translate italian day2_cards_without_sl_ef9c0eef:


    th "Ad ogni modo, che scelta potevo avere?"


translate italian day2_cards_without_sl_c56c19ea:


    th "Non c'è niente da fare qui durante la notte, almeno mi divertirò un po'."


translate italian day2_cards_without_sl_a5b9da23:


    "Ma la mia mente continuava a mugugnare facendomi notare che non era il momento giusto per divertirsi."


translate italian day2_cards_without_sl_4158e5dc:


    th "D'altra parte, se davvero esiste un campo di pionieri, allora dev'essere opera di qualcuno."


translate italian day2_cards_without_sl_c1286124:


    th "E anche se non fosse così, la cosa più logica da fare è cercare risposte qui, e non nella foresta o nelle pianure."


translate italian day2_cards_without_sl_23bd994f:


    "Ma in quel preciso momento, {i}le risposte{/i} stavano per giocare a carte..."


translate italian day2_cards_without_sl_cf613096:


    "Ho aperto la porta con la mia chiave e sono entrato."


translate italian day2_cards_without_sl_09b88332:


    "Con mia sorpresa, non sono riuscito a trovare le carte nel cassetto."


translate italian day2_cards_without_sl_956232aa:


    "Invece era pieno di tazzine, forchette e coltelli, piatti, nastri adesivi, forbici, guanti di gomma, qualche busta di plastica, matite, e un po' di penne rotte."


translate italian day2_cards_without_sl_d96cc0f8:


    "Tutto tranne le carte."


translate italian day2_cards_without_sl_c0fa483e:


    th "Forse dovrei cercare nell'armadio?"


translate italian day2_cards_without_sl_c357b6ff:


    "Era pieno di vestiti di Olga Dmitrievna, ma una piccola scatola chiusa a chiave ha catturato la mia attenzione."


translate italian day2_cards_without_sl_d7abae60:


    "Ho tirato, ma non si apriva."


translate italian day2_cards_without_sl_394c4aa1:


    th "Forse lei sta nascondendo qualcosa?"


translate italian day2_cards_without_sl_a9798457:


    th "Forzare la serratura non è di certo una buona idea, anche se le carte fossero lì dentro."


translate italian day2_cards_without_sl_da208a8e:


    "Stavo per andarmene quando mi sono ricordato una cosa."


translate italian day2_cards_without_sl_69cc04fb:


    "Slavya si era dimenticata le chiavi ieri. Ce le avevo ancora io."


translate italian day2_cards_without_sl_505d2b4b:


    th "Forse potrei...?"


translate italian day2_cards_without_sl_7774a647:


    "Ho esitato per un istante, ma poi mi sono avvicinato alla scatola e ho iniziato a provare le chiavi."


translate italian day2_cards_without_sl_5b37b93a:


    "Di certo sarebbe stato strano trovare la chiave giusta in quel mazzo – perché mai Slavya avrebbe dovuto possedere la chiave per la cassetta personale di Olga Dmitrievna?"


translate italian day2_cards_without_sl_87702aa9:


    "Ma con mia sorpresa, una delle chiavi ha girato senza problemi."


translate italian day2_cards_without_sl_bd5226c8:


    "Improvvisamente la porta ha cigolato dietro di me."


translate italian day2_cards_without_sl_82b93712:


    "Sono balzato in piedi e mi sono guardato attorno – ma non c'era nessuno nella stanza."


translate italian day2_cards_without_sl_5ffdfa53:


    th "Forse è stato il vento...?"


translate italian day2_cards_without_sl_6e22d9c9:


    "Ho guardato attentamente di fuori. Nessuna anima viva."


translate italian day2_cards_without_sl_82fc9b6c:


    "Forse non era niente, ma mi sono sentito comunque a disagio."


translate italian day2_cards_without_sl_a20cefa7:


    "..."


translate italian day2_cards_without_sl_badefa0c:


    "Ho persino guardato tra i cespugli circostanti, ma poi ho deciso di tornare dentro per scoprire tutti gli oscuri segreti della nostra leader..."


translate italian day2_cards_without_sl_3d3069e0:


    sl "Semyon, perché ci stai mettendo così tanto?"


translate italian day2_cards_without_sl_f9a84268:


    me "Eh... io... sì..."


translate italian day2_cards_without_sl_28bdcb47:


    "Con le mani che mi tremavano ho girato la chiave e l'ho strattonata fuori dalla serratura."


translate italian day2_cards_without_sl_abc9e8c7:


    "Slavya si è avvicinata."


translate italian day2_cards_without_sl_45ebf54a:


    sl "Ah! Le mie chiavi! Credevo di averle perse! Dove le hai trovate?"


translate italian day2_cards_without_sl_52fd9c46:


    me "Erano fra i cespugli... qui fuori. Le ho appena trovate..."


translate italian day2_cards_without_sl_ab249332:


    "Grazie a Dio ero riuscito a chiudere la scatola prima che mi vedesse."


translate italian day2_cards_without_sl_71f4991e:


    me "Andiamo?"


translate italian day2_cards_without_sl_9882a00c:


    "Volevo andarmene il prima possibile da quella stanza, e dimenticarmi del mio tentativo di violare la privacy altrui."


translate italian day2_cards_without_sl_9703c95b:


    "Appena siamo tornati, Olga Dmitrievna mi ha informato:"


translate italian day2_cards_without_sl_656cc591:


    mt "Mi spiace, mi sono ricordata che le carte erano nella stanza di Slavya.{w} È andata a prenderle mentre le stavi cercando."


translate italian day2_cards_without_sl_899192e2:


    "Ho guardato Slavya, che mi ha fatto un umile sorriso."


translate italian day2_cards_without_sl_79ea7a49:


    th "Forza ragazzi, non preoccupatevi per me, andrà tutto bene…"


translate italian day2_pre_cards_9f1a8f06:


    "Slavya e Olga Dmitrievna sono entrate."


translate italian day2_pre_cards_09995e69:


    "Stavo per seguirle ma qualcuno mi ha afferrato il braccio."


translate italian day2_pre_cards_08478924:


    "Alisa."


translate italian day2_pre_cards_d0c76f00:


    "Il suo sguardo mi ha fatto venire la pelle d'oca."


translate italian day2_pre_cards_fe95bf48:


    me "Ti serve qualcosa?"


translate italian day2_pre_cards_fcaa413f:


    "Le ho chiesto con cautela."


translate italian day2_pre_cards_29ab9a8f:


    dv "Hai intenzione di giocare a quello stupido gioco?"


translate italian day2_pre_cards_d14b9495:


    me "Ehm... sì? Qualcosa in contrario?"


translate italian day2_pre_cards_a364a5cb:


    dv "No, niente."


translate italian day2_pre_cards_bce20dc9:


    "Stava per andarsene ma poi si è voltata lentamente, sorridendo."


translate italian day2_pre_cards_9900fc17:


    dv "E così sai giocare a carte?"


translate italian day2_pre_cards_11e21afc:


    me "Un pochino."


translate italian day2_pre_cards_f288a1e1:


    "Non riuscivo a capire cosa volesse da me."


translate italian day2_pre_cards_3b50c11d:


    dv "Quindi solo il Durak e nient'altro?"


translate italian day2_pre_cards_11670603:


    th "Come se tu fossi un asso del Poker."


translate italian day2_pre_cards_0624cee5:


    me "Beh, direi di sì..."


translate italian day2_pre_cards_7c7e60b4:


    dv "Allora non hai speranze."


translate italian day2_pre_cards_5ac38cf1:


    me "Perché?"


translate italian day2_pre_cards_de97c6a8:


    dv "Perché sì!"


translate italian day2_pre_cards_72a60644:


    me "Quindi conosci le regole?"


translate italian day2_pre_cards_bdbe816e:


    dv "Ma certo!"


translate italian day2_pre_cards_3ad45daf:


    me "Beh, allora sarai avvantaggiata."


translate italian day2_pre_cards_75b64a69:


    "Non vedevo il senso di proseguire quella nostra conversazione, e così mi sono diretto verso la porta."


translate italian day2_pre_cards_cbef602e:


    dv "Perché stai cercando di andartene?!"


translate italian day2_pre_cards_d3fbefa3:


    th "C'è altro di cui parlare?"


translate italian day2_pre_cards_38f554a8:


    dv "Facciamo una scommessa."


translate italian day2_pre_cards_1084810b:


    me "Che intendi?"


translate italian day2_pre_cards_abd41233:


    dv "Sei proprio un tonto! Riguardo alla partita di carte, che altro?"


translate italian day2_pre_cards_2020ce91:


    me "E su cosa vorresti scommettere?"


translate italian day2_pre_cards_8d15d8fc:


    dv "Sul fatto che vincerò io!"


translate italian day2_pre_cards_14abf4d1:


    me "È un esito molto probabile."


translate italian day2_pre_cards_47f0669e:


    "Ho convenuto con calma."


translate italian day2_pre_cards_e82a5cb1:


    dv "Che c'è, hai paura?"


translate italian day2_pre_cards_7af7e68f:


    me "No, non ho paura...{w} È solo che non mi piace scommettere quando le probabilità sono a mio sfavore."


translate italian day2_pre_cards_f43da706:


    dv "E hai anche paura di rischiare."


translate italian day2_pre_cards_7c484d79:


    th "Che osservazione astuta. Sono impressionato."


translate italian day2_pre_cards_9fba406d:


    me "Bene, allora io..."


translate italian day2_pre_cards_0d611ce5:


    dv "No invece!"


translate italian day2_pre_cards_624ed6e0:


    me "E adesso che c'è?"


translate italian day2_pre_cards_8eadd935:


    "Ho sospirato, esausto."


translate italian day2_pre_cards_e071c864:


    "Il suo blaterare di un'insensata scommessa stava cominciando a darmi sui nervi."


translate italian day2_pre_cards_3f46f6f4:


    dv "Se non accetti la scommessa, dirò a tutti che ci hai provato con me!"


translate italian day2_pre_cards_3d342d5a:


    me "Cosa?!"


translate italian day2_pre_cards_89e2898c:


    dv "Hai sentito bene!"


translate italian day2_pre_cards_75246721:


    th "Me la immagino a fare una cosa del genere..."


translate italian day2_pre_cards_96533413:


    me "Non essere stupida.{w} Chi pensi che ti crederebbe? E poi, sono qui da nemmeno due giorni..."


translate italian day2_pre_cards_9ffc882d:


    dv "Vuoi davvero tentare la fortuna?"


translate italian day2_pre_cards_1f560713:


    me "D'accordo...{w} E cosa succede se vinco io?"


translate italian day2_pre_cards_8a92943c:


    dv "Allora non dirò niente a nessuno."


translate italian day2_pre_cards_a21dfdaa:


    me "E se perdo?"


translate italian day2_pre_cards_09931ae3:


    dv "Fai di nuovo il tonto?{w} Te l'ho già detto, dirò a tutti che hai provato a sedurmi."


translate italian day2_pre_cards_d4012ccc:


    me "Quindi mi stai dicendo che dovrei dimostrare di non aver fatto qualcosa, quando effettivamente non l'ho fatto?"


translate italian day2_pre_cards_37b7659b:


    dv "Se vuoi vederla in questo modo."


translate italian day2_pre_cards_17cc9d34:


    "Non era una decisione facile."


translate italian day2_pre_cards_742c0e25:


    "Da una parte, sarebbe stato stupido accettare – non conoscevo le regole e il gioco d'azzardo non era il mio forte."


translate italian day2_pre_cards_a57f27fe:


    "Ma dall'altra – lei avrebbe potuto davvero rendermi la mia vita un inferno."


translate italian day2_pre_cards_286e380f:


    th "E comunque, posso fidarmi di lei?"


translate italian day2_pre_cards_3f4ee582:


    th "Potrebbe farlo anche se vincessi io."


translate italian day2_pre_cards_7cc3db09:


    dv "Allora, hai deciso?"


translate italian day2_pre_cards_25b3a2ba:


    "Stavo per risponderle, ma improvvisamente Lena è apparsa da dietro."


translate italian day2_pre_cards_a07b4105:


    dv "Che vuoi?"


translate italian day2_pre_cards_c93b52ae:


    un "Niente..."


translate italian day2_pre_cards_3653acd5:


    "Lena si è affrettata ad entrare."


translate italian day2_pre_cards_dbeba143:


    dv "Allora?"


translate italian day2_pre_cards_369c0b87:


    th "Potrei pentirmene amaramente..."


translate italian day2_pre_cards_788e115a:


    me "D'accordo, ci sto!"


translate italian day2_pre_cards_d1046a49:


    "Ha sorriso."


translate italian day2_pre_cards_e541d96d:


    me "Ma se vinco io..."


translate italian day2_pre_cards_33997702:


    dv "Certo, certo. Niente trucchi, non barare."


translate italian day2_pre_cards_8fa3ceb0:


    "Alisa si è girata, è salita sulle scale ed è entrata nella mensa."


translate italian day2_pre_cards_e69bd7ba:


    th "Perché lo sto facendo?"


translate italian day2_pre_cards_201eb6bf:


    th "Forse perché riesce a manipolarmi a suo piacimento?"


translate italian day2_pre_cards_5354793c:


    th "E a quanto pare ha già deciso di farlo..."


translate italian day2_pre_cards_e331dbe9:


    th "No. Non voglio essere coinvolto nei tuoi affari loschi."


translate italian day2_pre_cards_a4a97e2b:


    me "Non voglio, scusa..."


translate italian day2_pre_cards_bf45c8f3:


    dv "Vigliacco!"


translate italian day2_pre_cards_3b0ca4e8:


    "Si è stretta nelle spalle ed è salita sulle scale. E prima di entrare mi ha gridato:"


translate italian day2_pre_cards_9ccfc1da:


    dv "Preparati a subirne le conseguenze!"


translate italian day2_pre_cards_46588f42:


    th "Conseguenze...?"


translate italian day2_pre_cards_a83588c3:


    th "E se avessi fatto la scelta sbagliata?"


translate italian day2_pre_cards_c92ad65b:


    "Dopotutto, lei avrebbe potuto davvero rendermi la vita difficile qui."


translate italian day2_pre_cards_752fe6dc:


    "Ma non potevo permettere che mi coinvolgesse in qualcosa di così avventato."


translate italian day2_pre_cards_808392c5:


    "Ho fatto un grosso sospiro e sono entrato nella mensa."


translate italian day2_cards_6c6e7a1b:


    "Dentro era già tutto pronto."


translate italian day2_cards_18d88118:


    "Alcuni pionieri stavano in piedi qua e là, chiacchierando."


translate italian day2_cards_28235b58:


    "I tavoli erano stati spostati per fare spazio ai giocatori e agli spettatori."


translate italian day2_cards_bd07ee1b:


    "Mi sono guardato attorno."


translate italian day2_cards_1fda7f4b:


    "Qualcosa stava succedendo nell'angolo in fondo."


translate italian day2_cards_3f899848:


    "Quando mi sono avvicinato ho visto un cartellone sul quale era disegnato una sorta di schema."


translate italian day2_cards_5f123cf2:


    "Il mio nome era tra i partecipanti."


translate italian day2_cards_a9b3e057:


    me "Di chi è stata l'idea di tutto questo?"


translate italian day2_cards_8b5a8e17:


    "Ho chiesto a Electronik, che era nelle vicinanze."


translate italian day2_cards_7d795258:


    el "Ovviamente, del vostro umile servo!"


translate italian day2_cards_d353e361:


    "Si è chinato dinanzi me scherzosamente. La cosa mi ha messo un po' a disagio."


translate italian day2_cards_234f4ba7:


    me "E perché diamine mi trovo tra i partecipanti?"


translate italian day2_cards_24671a0f:


    "Ho chiesto con disappunto."


translate italian day2_cards_5c08eef1:


    "Fino a pochi secondi fa avevo ancora la speranza di poter eludere quel torneo – così non mi sarei dovuto preoccupare della vendetta di Alisa nel caso in cui avessi perso la scommessa."


translate italian day2_cards_1c56c3d2:


    "Fino a pochi secondi fa avevo ancora la speranza di poter eludere quel torneo – così non mi sarei dovuto preoccupare della vendetta di Alisa per il fatto di non aver accettato la scommessa."


translate italian day2_cards_9ca648ce:


    th "Ma ora quella speranza è svanita."


translate italian day2_cards_8191ef08:


    el "È stata una pura coincidenza."


translate italian day2_cards_f0331a22:


    th "Sì certo, una coincidenza – e guarda caso non c'è alcun giocatore che io non conosca."


translate italian day2_cards_dc663958:


    th "Benché la sala sia colma di decine di altri pionieri!"


translate italian day2_cards_565fd677:


    "Mi sono fatto prendere dall'ansia."


translate italian day2_cards_287334ff:


    "L'ansia del sentirsi osservato mentre si sta al centro di una stanza senza porte né finestre."


translate italian day2_cards_d9135825:


    me "E quali sono i premi?"


translate italian day2_cards_e0592077:


    "Gli ho chiesto pigramente."


translate italian day2_cards_b19ed28a:


    "Volevo distrarmi un po' dalla situazione parlando di cose superficiali."


translate italian day2_cards_98766828:


    "Electronik stava per rispondere, quando improvvisamente Ulyana è apparsa dal nulla e ha iniziato a saltargli attorno."


translate italian day2_cards_fbd0629e:


    us "Premi, premi!"


translate italian day2_cards_aa52231b:


    us "Mi pare di aver sentito qualcosa riguardo ai premi!"


translate italian day2_cards_2a82cf49:


    me "Lo sai qual è l'ethos delle Olimpiadi?"


translate italian day2_cards_c7b7a3fa:


    us "Il cosa? No."


translate italian day2_cards_41d394ae:


    me "Lo capirai quando crescerai un po'!"


translate italian day2_cards_afdea261:


    "Mi ha fatto una smorfia e ha colpito Electronik nelle costole."


translate italian day2_cards_06a30073:


    us "E allora, quali sono i premi?"


translate italian day2_cards_bdf4c52f:


    el "Beh... Non lo so.{w} Non dipende da me."


translate italian day2_cards_87bdcd1f:


    "Ha alzato le spalle."


translate italian day2_cards_7da0931b:


    th "In realtà, se proprio dovevano inventarsi questo stupido torneo, almeno potrebbero consegnare al vincitore una medaglia di cioccolato o qualcosa di simile."


translate italian day2_cards_b35ebcaf:


    "Ulyana improvvisamente è balzata in piedi ed è corsa da qualche parte."


translate italian day2_cards_95df2d6c:


    th "Magari avessi un po' del suo ottimismo..."


translate italian day2_cards_806b5dfa:


    me "E allora quali sono le regole?"


translate italian day2_cards_c4fe8ebc:


    el "Aspetta un altro po'!{w} Manca ancora qualcuno."


translate italian day2_cards_b58585ce:


    "Mi sono guardato attorno – Alisa, Slavya, Lena, Miku e Shurik erano tutti lì."


translate italian day2_cards_b6cde061:


    me "Sembra che ci siano tutti..."


translate italian day2_cards_7b898ccd:


    el "Non tutti! Manca Zhenya!"


translate italian day2_cards_0d6ad6c9:


    "Sbaglio o mi sembra un po' a disagio?"


translate italian day2_cards_51851d31:


    me "Beh, non è qui, e allora?"


translate italian day2_cards_3a674479:


    me "Metti qualcun altro al suo posto."


translate italian day2_cards_d17d3a9a:


    el "No, non posso farlo..."


translate italian day2_cards_8b484ca2:


    "Mi ha risposto, scandendo ogni sillaba."


translate italian day2_cards_1522ed9b:


    "Ho deciso di non chiedergli per quale ragione non potesse farlo."


translate italian day2_cards_ed082483:


    me "Beh, va' a cercarla allora, non so."


translate italian day2_cards_30b1301c:


    mt "Lui non può andare, è l'organizzatore dell'evento."


translate italian day2_cards_06d5bcd8:


    "La leader del Campo è spuntata dal nulla."


translate italian day2_cards_8153d6f9:


    el "Ma Olga Dmitrievna...!"


translate italian day2_cards_1d92e913:


    "Ha borbottato."


translate italian day2_cards_4e9a3025:


    mt "Andrà Semyon.{w} Giusto, Semyon?"


translate italian day2_cards_943eaf3a:


    "Mi ha guardato e ha sorriso."


translate italian day2_cards_6bdedf0e:


    th "Ma certo, chi altro...?"


translate italian day2_cards_8cc053bc:


    me "E dov'è adesso?"


translate italian day2_cards_3030d7cf:


    mt "Nella biblioteca, suppongo."


translate italian day2_cards_4366e696:


    me "Ok..."


translate italian day2_cards_f0078f92:


    "Mi sono trascinato fuori dalla porta."


translate italian day2_cards_063c8560:


    el "Per favore, fa' in fretta!"


translate italian day2_cards_e98e1cff:


    "Ma che problema ha?"


translate italian day2_cards_75952aae:


    th "Si sta facendo notte."


translate italian day2_cards_ec3b332d:


    "Prendendo tempo, mi sono incamminato lentamente verso la biblioteca."


translate italian day2_cards_7692b3b8:


    "Ma ho trovato Zhenya prima del previsto – stava seduta su una panchina nella piazza, fissando Genda, che era silente come sempre."


translate italian day2_cards_eca593b5:


    me "Che ci fai qui?{w} Tutti ti stanno cercando!"


translate italian day2_cards_17c805c8:


    mz "Sto seduta, come puoi vedere."


translate italian day2_cards_823e3ce8:


    "Ha aggrottato la fronte."


translate italian day2_cards_a15cb848:


    me "Dai, andiamo!"


translate italian day2_cards_085c1aa0:


    mz "Non voglio."


translate italian day2_cards_a595fb55:


    "Mi ha detto, distogliendo lo sguardo."


translate italian day2_cards_5ac38cf1:


    me "Perché no?"


translate italian day2_cards_0a62b9a3:


    mz "Perché no!"


translate italian day2_cards_12af78a1:


    "Mi sono seduto accanto a lei."


translate italian day2_cards_9539c19b:


    me "Senti, l'idea del torneo non piace neppure a me, ma non possiamo deludere tutti."


translate italian day2_cards_7ae8c79a:


    "Quella frase non faceva parte del mio repertorio."


translate italian day2_cards_f8b4058b:


    "Qualche giorno fa non avrei nemmeno lontanamente immaginato di dire una cosa del genere."


translate italian day2_cards_377eefc7:


    "Zhenya mi ha guardato, sorpresa."


translate italian day2_cards_59271a84:


    mz "Quindi tutti mi stanno aspettando?"


translate italian day2_cards_6d6c0c61:


    th "Non è forse quello che ho appena detto?"


translate italian day2_cards_fc30fd2b:


    me "Sì."


translate italian day2_cards_40d696ab:


    mz "Non ci vado comunque!"


translate italian day2_cards_a5db4948:


    "Ha aggrottato le sopracciglia e mi ha dato le spalle."


translate italian day2_cards_65a156a9:


    me "Ma perché?"


translate italian day2_cards_6cd0f3dd:


    "Ho allargato le braccia."


translate italian day2_cards_7311aff6:


    mz "Non sono capace di giocare a carte..."


translate italian day2_cards_4473e360:


    me "E allora?{w} Lo stesso vale per me."


translate italian day2_cards_687e7e02:


    mz "Allora come puoi giocare?"


translate italian day2_cards_a7327cc4:


    me "Cos'è, si possono fare solo le cose che si imparano dai libri?"


translate italian day2_cards_976a6d64:


    mz "Certo."


translate italian day2_cards_a4614188:


    "Era sorpresa."


translate italian day2_cards_32c9a33a:


    me "E cosa faresti se ti ritrovassi in Antartide e dovessi cacciare gli orsi polari per sopravvivere?"


translate italian day2_cards_0fa90515:


    mz "Non ci sono gli orsi polari in Antartide."


translate italian day2_cards_beecbb90:


    "Zhenya ha sorriso."


translate italian day2_cards_c2ae7af0:


    me "Non importa. È solo un esempio!"


translate italian day2_cards_8d36304d:


    me "Avanti, non è che la vita di qualcuno dipenda dall'esito di quel torneo."


translate italian day2_cards_60f3d758:


    "Ci ha pensato un attimo."


translate italian day2_cards_fc98202b:


    mz "È solo che non voglio deludere nessuno."


translate italian day2_cards_3a30e832:


    me "Giusto."


translate italian day2_cards_edd5c8c1:


    "Ho detto con sarcasmo."


translate italian day2_cards_dbd5c99b:


    mz "E non pensare a nulla di strano a riguardo!"


translate italian day2_cards_5b122da1:


    "Non ho capito cosa intendesse, ma comunque."


translate italian day2_cards_78e0f168:


    "Ovviamente, ognuno ha i suoi punti deboli."


translate italian day2_cards_be072bc7:


    "In un minuto eravamo già alla mensa."


translate italian day2_cards_56cd1a43:


    "Tutti fissavano Electronik."


translate italian day2_cards_ff4ddd7c:


    el "Allora..."


translate italian day2_cards_f8f1459d:


    "Si è schiarito la voce."


translate italian day2_cards_7ddd8bdc:


    el "Ogni round consiste in una partita."


translate italian day2_cards_0de601df:


    el "In caso di pareggio, si ripete la partita."


translate italian day2_cards_a323cae4:


    el "Dopodiché il perdente verrà eliminato, e si passerà al round successivo."


translate italian day2_cards_69f4f055:


    el "Dato che i volontari..."


translate italian day2_cards_f311836a:


    "Mi ha guardato."


translate italian day2_cards_e7dd93af:


    el "Dato che i volontari sono solo otto, ci saranno tre round."


translate italian day2_cards_e2f721ce:


    el "Tutto chiaro?"


translate italian day2_cards_5e17549c:


    "La folla ha applaudito."


translate italian day2_cards_e97014bc:


    us "E quali sono i premi? I premi, quali sono?"


translate italian day2_cards_1fcf7c30:


    sl "Ulyana, basta!"


translate italian day2_cards_3a71b956:


    "Slavya si è fatta avanti e ha tentato di acchiappare Ulyana."


translate italian day2_cards_2b37c513:


    us "Non la smetterò finché il premio non sarà mio!"


translate italian day2_cards_038f6c96:


    "Sembrava che quella ragazzina avesse abbastanza energie per teletrasportarsi su Alpha Centauri."


translate italian day2_cards_fbd0629e_1:


    us "Premi! Premi!"


translate italian day2_cards_fdbf4fb7:


    "Continuava a ripetere in continuazione."


translate italian day2_cards_7d36e3c2:


    sl "Smettila."


translate italian day2_cards_09337077:


    "Slavya ha cercato di farla ragionare."


translate italian day2_cards_313ef528:


    "Electronik sembrava avesse già le vertigini a causa di tutto quel trambusto."


translate italian day2_cards_6c19d524:


    me "D'accordo, cominciamo."


translate italian day2_cards_fc1804fc:


    "Ho detto con tranquillità, e poi mi sono rivolto a Ulyana:"


translate italian day2_cards_f0d862c6:


    me "Altrimenti non avrai alcun premio."


translate italian day2_cards_937ae63e:


    "Sembra che la mia frase l'abbia convinta, dato che si è seduta al suo posto."


translate italian day2_cards_739a28dd:


    "Slavya l'ha seguita, ringraziandomi con un sorriso mentre mi passava davanti."


translate italian day2_cards_ea7ae555:


    "I pionieri finalmente si sono sistemati ai loro posti."


translate italian day2_cards_34834de5:


    "Ho raggiunto il tavolo dov'era seduta Lena."


translate italian day2_cards_f1ce5d14:


    me "Ti dispiace?"


translate italian day2_cards_78e5e8c4:


    "Ha alzato lo sguardo ed è arrossita."


translate italian day2_cards_f9967eb5:


    me "Non preoccuparti, non conosco le regole."


translate italian day2_cards_4752f785:


    th "E come posso essere sicuro del fatto che non le conosca solo io?"


translate italian day2_cards_04d910fd:


    "Mi sono seduto."


translate italian day2_cards_569cc0a5:


    me "Sembra che dovremo giocare il primo round insieme."


translate italian day2_cards_f1be5fed:


    un "Sì."


translate italian day2_cardgame_ea01d133:


    "Finalmente Electronik ha iniziato a spiegare le regole."


translate italian demo_play_intro_accc2d8a:


    el "Guardate le vostre carte con attenzione."


translate italian demo_play_intro_5fa54f4c:


    el "Ce ne sono esattamente sei di fronte a ognuno di voi!"


translate italian demo_play_intro_ccdb8a86:


    th "Spero che tutti qui sappiano contare."


translate italian demo_play_intro_283d409f:


    el "Adesso potete guardarle."


translate italian demo_play_intro_5bf90d15:


    "Dopo che ognuno ha guardato le proprie carte, Electronik ha proseguito."


translate italian demo_play_intro_fb1ad002:


    el "Le regole qui sono simili a quelle del poker."


translate italian demo_play_intro_c3658729:


    el "Spero che tutti voi le conosciate."


translate italian demo_play_intro_b18e73e6:


    "Io conoscevo le regole, ma non ero sicuro degli altri."


translate italian demo_play_intro_5d5160d3:


    el "Prima c'è la carta alta, poi la coppia, poi due coppie, poi tre di un tipo...{w} E così via. Ma le scale e i colori non si considerano."


translate italian demo_play_intro_66ab73d4:


    el "Nel primo turno dovrete scegliere una delle carte del vostro avversario."


translate italian demo_play_intro_dbcd0300:


    el "Il vostro avversario, di conseguenza, potrà decidere se scambiare di posto due delle sue carte, e può fare al massimo due scambi."


translate italian demo_play_intro_d3a6fb9a:


    el "Oppure può decidere di lasciarle così come sono, se non gli serve la carta che avete scelto."


translate italian demo_play_intro_faea44e8:


    el "Ricordate che lo scambio delle carte dev'essere visibile all'avversario."


translate italian demo_play_intro_d5e89419:


    el "Nel turno successivo, il vostro avversario sceglierà una delle vostre carte, e potrete scambiare le vostre allo stesso modo."


translate italian demo_play_intro_ee59bb67:


    el "E così via – credo sia piuttosto chiaro."


translate italian demo_play_intro_3c934234:


    "Non era molto chiaro per me."


translate italian demo_play_intro_135cb9f4:


    us "Ehi tu, Einstein!"


translate italian demo_play_intro_5b29bd77:


    "Gli ha urlato Ulyana."


translate italian demo_play_intro_6cf11f92:


    us "Non ci ho capito niente!"


translate italian demo_play_intro_3affc0c2:


    el "Lo capirai non appena inizierete."


translate italian demo_play_intro_8dd13e36:


    "Electronik è andato verso il tavolo dello schema, abbandonando la povera Ulyana al suo destino."


translate italian demo_play_intro_ce1458ea:


    me "Inizia tu."


translate italian demo_play_intro_881ff0c4:


    "Speravo di riuscire a capirci qualcosa entro poco tempo."


translate italian demo_play_intro_3dc8ac41:


    "Lena, più perplessa del solito, ha allungato la mano per prendere una delle mie carte."


translate italian demo_play_me_defend_1_5bbc93a2:


    "Ma al centro del tavolo la sua mano si è fermata."


translate italian demo_play_me_defend_1_c4c9ab6d:


    un "Non vorresti...?"


translate italian demo_play_me_defend_1_0f88ba77:


    th "Ah già! Dovrei cercare di difendere la mia carta!"


translate italian demo_play_me_defend_1_9592b4e5:


    th "Che cosa ha detto prima Electronik...?"


translate italian demo_play_me_defend_1_7f329a7a:


    "Per cercare di confondere il mio avversario posso scambiare di posto due delle mie carte.{w} E posso farlo due volte di seguito."


translate italian demo_play_me_defend_1_30d81d36:


    "Oppure posso lasciarle come sono."


translate italian demo_play_me_defend_1_a9691ffc:


    "Dovrei proteggere quella carta o no?"


translate italian demo_play_me_defend_1_12313e8c:


    "Oppure potrei lasciare che Lena prenda la carta che ha scelto."


translate italian demo_play_me_defend_1_3f7d9aa4:


    "Poi lei potrebbe cambiare idea e sceglierne un'altra.{w} Oppure potrebbe scegliere di nuovo la stessa."


translate italian demo_play_me_select_1_2c550258:


    "Le regole stanno diventando chiare!{w} O almeno, comprensibili..."


translate italian demo_play_me_select_1_ab26be30:


    me "Adesso è il mio turno."


translate italian demo_play_me_select_1_6a346d80:


    "Posso provare a riprendere la mia carta, o sceglierne un'altra delle sue."


translate italian demo_play_rival_defend_10293eac:


    "Lena può decidere di proteggere la carta che ho scelto."


translate italian demo_play_rival_defend_092d114b:


    "Ma se osservo attentamente lo scambio delle carte, posso comunque riuscire a individuare quella che volevo."


translate italian demo_play_after_loop_a4fc9855:


    "Ce l'ho fatta!"


translate italian day_2_cards_continue_b8733e71:


    "Electronik, che stava osservando la nostra partita in silenzio, ha annuito in segno di approvazione."


translate italian day_2_cards_continue_f37b7ba7:


    "Sembra che adesso abbiamo capito come funziona."


translate italian day_2_cards_continue_a335157b:


    el "Ora, durante la fase del torneo i giocatori si scambiano le carte per tre volte, tenete d'occhio il vostro avversario – penetratelo con il vostro sguardo."


translate italian day_2_cards_continue_b27ea81d:


    "Ho ridacchiato. «Penetratelo»."


translate italian day_2_cards_continue_ae91c5b2:


    el "Che c'è di divertente?"


translate italian day_2_cards_continue_055701c5:


    me "Oh, niente."


translate italian day_2_cards_continue_394b559a:


    "Ho cercato di mantenere un'espressione seria."


translate italian day_2_cards_continue_aa08701b:


    "Mi ha fissato per un istante e poi ha proseguito."


translate italian day_2_cards_continue_3e03a63e:


    el "Alla fine si girano tutte le carte sul tavolo per vedere chi ha la mano migliore."


translate italian day_2_cards_continue_f0cced6b:


    "Electronik è tornato al suo schema."


translate italian un_play_draw_2c705b13:


    el "Pareggio! Dovete giocare di nuovo."


translate italian un_play_win_5df79197:


    "Ho vinto!"


translate italian un_play_win_90577728:


    "Seriamente – è difficile giocare a un gioco che è stato appena inventato – soprattutto se non da te."


translate italian un_play_win_4dee3b34:


    "Ma ho vinto!"


translate italian un_play_win_0b9b056b:


    "Tuttavia, il mio momento di gloria è stato rovinato dal fatto che avevo vinto contro Lena."


translate italian un_play_win_8d4faa34:


    "Non è molto sicura di sé in generale, e adesso sicuramente lo sarebbe stata ancora meno."


translate italian un_play_win_d7bb4c00:


    th "Sono talmente in imbarazzo che non riesco nemmeno a guardarla."


translate italian un_play_win_3f0a42a8:


    "Forse avrei dovuto farla vincere per migliorare la sua autostima."


translate italian un_play_win_08d9c222:


    th "Ma ho fatto una scommessa con Alisa..."


translate italian un_play_win_442b0ff1:


    "Nel frattempo, Electronik ha annunciato con orgoglio che il primo round era terminato."


translate italian un_play_win_4c17c888:


    "I nomi di coloro che erano passati al secondo round sono apparsi sullo schema poco dopo."


translate italian un_play_win_6ece79aa:


    "La semi-finale aveva come protagonisti Alisa contro Zhenya e Ulyana contro..."


translate italian un_play_win_aac891f4:


    "Me."


translate italian un_play_win_482610ae:


    "Mi sono lasciato sfuggire un sospiro di rassegnazione."


translate italian un_play_win_3e9272c6:


    "Ulyana ha immediatamente preso posto davanti a me!"


translate italian un_play_win_9d897d0d:


    us "Ha!"


translate italian un_play_win_2ed6127e:


    "Mi ha fissato sogghignando."


translate italian un_play_win_9e5e59e8:


    us "Come sei riuscito a battere Lena?"


translate italian un_play_win_69120a2e:


    us "Hai barato, per caso?"


translate italian un_play_win_140619d9:


    me "Io non sono te."


translate italian un_play_win_4f2b481d:


    me "Semplicemente so come giocare a carte."


translate italian un_play_win_14e9ade2:


    th "O almeno è quello che voglio farle credere."


translate italian un_play_win_dd90f754:


    me "E tu invece come hai fatto a battere Shurik?"


translate italian un_play_win_4dc5e73c:


    us "Ah..."


translate italian un_play_win_b7c182de:


    "Ulyana ha agitato la mano, indicando com'era stato facile."


translate italian un_play_win_e22a379f:


    us "L'ho minacciato di iscrivermi al suo circolo."


translate italian un_play_win_dc47b4bb:


    "Ha sogghignato di nuovo."


translate italian un_play_win_da4c608f:


    us "Anche tu ti farai da parte?"


translate italian un_play_win_9d811be6:


    me "Nemmeno per sogno!"


translate italian un_play_win_35ba093a:


    us "Agh..."


translate italian un_play_win_502323d6:


    us "Allora deciderò io quali carte potrai prendere!"


translate italian un_play_win_4998c00d:


    me "Ma hai sentito le regole?"


translate italian un_play_win_7625dabe:


    us "Ah, chi se ne frega!"


translate italian un_play_win_ff6428f7:


    "Sembrava davvero che non le importasse delle regole."


translate italian un_play_win_7a4f4560:


    me "Ok, ma anch'io sceglierò quali carte darti."


translate italian un_play_win_0a1ee1fc:


    us "Affare fatto!"


translate italian un_play_win_7ddb7c7e:


    "Ispirato dalla mia vittoria nel primo round, mi sono avventurato in questa scelta rischiosa."


translate italian un_play_win_fa27d833:


    "Avrei potuto oppormi, appellarmi a Electronik e far rispettare le regole, ma per qualche ragione mi sentivo veramente fiducioso dell'esito di questo round."


translate italian un_play_win_09a416bb:


    "È vero, tutto questo era contro le regole, ma io e Ulyana eravamo sulla stessa barca."


translate italian un_play_win_05c369b7:


    "Ho guardato Electronik."


translate italian un_play_win_d0aaf07c:


    el "Che le semi-finali abbiano inizio!"


translate italian un_play_win_8ce591ef:


    "Ha annunciato."


translate italian un_play_win_352aee2b:


    "Ho osservato attentamente le mie carte, assicurandomi che Ulyana non le potesse vedere."


translate italian us_play_me_defend_2_7232aa89:


    us "Ehi! Non spostare le carte, mi confondi!"


translate italian us_play_me_defend_2_25579ef8:


    "Hmm..."


translate italian us_play_draw_2c705b13:


    el "Pareggio! Dovete giocare di nuovo."


translate italian us_play_win_4c353fb8:


    us "Ehi! Non vale!"


translate italian us_play_win_49d03e3a:


    us "Avresti dovuto perdere!"


translate italian us_play_win_fb0127bc:


    "Si è gonfiata dalla rabbia, come un pallone."


translate italian us_play_win_6905d85b:


    us "Voglio una rivincita, ma questa volta devi perdere, capito?!"


translate italian us_play_win_4782a874:


    "Non ero l'unico ad aver sentito le sue parole, ma l'intera sala."


translate italian us_play_win_2286cffa:


    "Persino Electronik."


translate italian us_play_win_0e9c7f85:


    el "Le rivincite non sono ammesse!"


translate italian us_play_win_2f715ea7:


    "Ulyana l'ha ignorato completamente."


translate italian us_play_win_615b52f1:


    us "Devi perdere!"


translate italian us_play_win_f6ec2b26:


    me "Non ho la minima intenzione di giocare con te un'altra volta."


translate italian us_play_win_498ef814:


    "Le ho detto pacatamente."


translate italian us_play_win_a7075cc6:


    us "Ah, è così?"


translate italian us_play_win_7c6b0a82:


    me "Sì, è così."


translate italian us_play_win_f60b0b79:


    us "Bene, allora dirò in giro che hai molestato Alisa."


translate italian us_play_win_e4705df4:


    "Mi ha sussurrato."


translate italian us_play_win_32b78b25:


    me "Che cosa?!"


translate italian us_play_win_50dfdc11:


    "Mi sono chinato verso di lei con espressione minacciosa."


translate italian us_play_win_e2a63898:


    me "E così, stavi origliando?"


translate italian us_play_win_56390f46:


    us "Vi ho sentiti mentre passavo di lì."


translate italian us_play_win_6d4b54e2:


    "Tuttavia, è sempre meglio giocare un altro round piuttosto che..."


translate italian us_play_win_7189cb82:


    th "Lei sarebbe in grado di farlo sicuramente!"


translate italian us_play_win_0e589c83:


    "Ho sospirato, rivolgendomi a Electronik."


translate italian us_play_win_e5983d15:


    me "Non c'è problema, possiamo giocare un'altra volta."


translate italian us_play_win_0c8d9dc7:


    el "Ah, fate come vi pare..."


translate italian us_play_win_d656c5e0:


    "Ci ha detto, alzando le spalle."


translate italian us_play_win_a5e9e043:


    "Così, la rivincita è iniziata."


translate italian us2_play_draw_2c705b13:


    el "Pareggio! Dovete giocare di nuovo."


translate italian us2_play_win_31467604:


    me "Troppo facile."


translate italian us2_play_win_0f831176:


    "Mi sono stravaccato sulla sedia."


translate italian us2_play_win_57a622de:


    us "Non è giusto!"


translate italian us2_play_win_8f3148e1:


    th "Spero non mi chieda un'altra rivincita!"


translate italian us2_play_win_dd53341d:


    me "Perché?"


translate italian us2_play_win_e98bfbde:


    "Ho ridacchiato."


translate italian us2_play_win_08916ca6:


    us "Va bene..."


translate italian us2_play_win_68edce41:


    "Ulyana, offesa, si è alzata dal tavolo."


translate italian us2_play_win_aec6e03a:


    "Ho osservato lo schema del torneo, per sapere chi sarebbe stato il mio ultimo rivale."


translate italian us2_play_win_1afc5d06:


    "In quel preciso istante Alisa si è seduta al mio tavolo."


translate italian us2_play_win_6286b0f8:


    "Ho fatto uno stupido e innaturale sorriso."


translate italian us2_play_win_ad1e84f8:


    th "Come se fossi spaventato da lei!"


translate italian us2_play_win_d29e64cc:


    me "Congratulazioni."


translate italian us2_play_win_bb61fa08:


    dv "Ti pentirai di aver fatto il vigliacco."


translate italian us2_play_win_b7a1e7d5:


    th "Me ne sto pentendo già adesso..."


translate italian us2_play_win_a18f9993:


    "Non perché non avessi accettato la scommessa – ma piuttosto perché ho accettato di partecipare a questo stupido torneo."


translate italian us2_play_win_c3ff6c62:


    dv "Ti aspetti di vincere?"


translate italian us2_play_win_58f07f35:


    me "Mi aspetto che tu mantenga la tua promessa."


translate italian us2_play_win_7807add1:


    dv "D'accordo, iniziamo!"


translate italian dv_play_draw_2c705b13:


    el "Pareggio! Dovete giocare di nuovo."


translate italian day2_main3_84842450:


    "È sempre frustrante perdere..."


translate italian day2_main3_c9e7d2f7:


    "Almeno ho vinto il primo round."


translate italian day2_main3_1f69a49c:


    "Ho perso contro Dvachevskaya."


translate italian day2_main3_e25a1002:


    th "Non poteva andare peggio di così."


translate italian day2_main3_183277f1:


    th "Posso solo immaginarmi cosa accadrà domani."


translate italian day2_main3_00105f86:


    th "Potrebbe mettermi in imbarazzo all'allineamento (sempre che io riesca a svegliarmi in tempo, ovviamente), potrebbe lamentarsi con Olga Dmitrievna."


translate italian day2_main3_d73b14bb:


    th "Potrebbe spargere le voci su di me per tutto il Campo."


translate italian day2_main3_d7329552:


    th "La cosa peggiore è che tutti crederanno a lei e non a me."


translate italian day2_main3_cc371952:


    "Non sono sicuro del perché, ma ne sono sicuro al 100%%."


translate italian day2_main3_4cda6c6c:


    th "E non importa che non ho scommesso con Alisa, quello che conta è che ho vinto."


translate italian day2_main3_bac15b3e:


    th "Dato che si tratta di Alisa, posso solo immaginare cosa potrà fare domani!"


translate italian day2_main3_d20a7c98:


    th "Potrebbe ridicolizzarmi domani mattina all'allineamento, dicendo tutto a Olga Dmitrievna."


translate italian day2_main3_3e52af3c:


    th "O potrebbe spargere delle dicerie sul mio conto."


translate italian day2_main3_e1a0fa0c:


    th "La cosa peggiore è che tutti crederanno a lei e non a me.{w} Non sono sicuro del motivo, ma sono sicuro al 100%% che sarà così."


translate italian day2_main3_a8cecdb4:


    "Ho abbandonato la mensa."


translate italian day2_main3_f9f946ef:


    "Era ancora troppo presto per andare a dormire, e una passeggiata mi avrebbe fatto bene."


translate italian day2_main3_b3b9bfb2:


    th "Dove potrei andare?"


translate italian day2_aidpost_eve_dad1f28b:


    "Ho incespicato proseguendo dritto, vagando senza meta, e sono finito all'infermeria."


translate italian day2_aidpost_eve_1b18dca2:


    "Se davvero avessi avuto bisogno di cure allora sarei andato da uno psicologo, e andare a trovare l'infermiera non mi è nemmeno passato per la testa."


translate italian day2_aidpost_eve_a20cefa7:


    "..."


translate italian day2_square_eve_8c430c9f:


    "Sono arrivato alla piazza e mi sono seduto su una panchina."


translate italian day2_square_eve_dd9e540e:


    "Stare lì a fissare la statua di Genda mi è sembrato un buon passatempo."


translate italian day2_square_eve_a20cefa7:


    "..."


translate italian day2_beach_eve_4ae280fe:


    "Sono arrivato alla spiaggia."


translate italian day2_beach_eve_7700f3ad:


    "Non ero di buonumore e non avevo alcuna voglia di nuotare, ma mi sono comunque avvicinato alla riva e ho sfiorato l'acqua con la mano."


translate italian day2_beach_eve_691e5417:


    "L'acqua era abbastanza calda."


translate italian day2_beach_eve_5dc0ba05:


    "Sembrava che si stesse ancora raffreddando dopo una giornata così calda."


translate italian day2_beach_eve_06f63b4a:


    th "Beh, forse un'altra volta mi farò una nuotata…"


translate italian day2_beach_eve_a20cefa7:


    "..."


translate italian day2_dock_eve_ee198286:


    "Ho deciso di andare al molo."


translate italian day2_dock_eve_c4425934:


    "Il sole stava ancora splendendo all'orizzonte e il fiume mostrava in lontananza tutte le sue sfumature di rosso, giallo e arancione."


translate italian day2_dock_eve_8572e4d2:


    "L'acqua sembrava infuocata, ho osservato quello spettacolo mentre si affievoliva lentamente, fino a sparire del tutto."


translate italian day2_dock_eve_a20cefa7:


    "..."


translate italian day2_busstop_eve_62cce665:


    "Gli eventi di questa giornata scorrevano limpidi nella mia mente: quella maledetta e inutile lista delle cose da fare, quello stupido torneo…"


translate italian day2_busstop_eve_73a11ef4:


    "In quel momento non avevo proprio voglia di fare niente, né di parlare con nessuno. Non avevo nemmeno voglia di pensare alla mia situazione."


translate italian day2_busstop_eve_6f392ba3:


    "Sono arrivato alla piazza e mi sono seduto su una panchina, fissando la statua di Genda."


translate italian day2_busstop_eve_a20cefa7:


    "..."


translate italian day2_busstop_eve_58e66c22:


    "Non saprei dire di preciso quanto tempo fossi rimasto lì, ma alla fine il frinire dei grilli mi ha riportato alla realtà."


translate italian day2_busstop_eve_c4f332c1:


    "Mi sono alzato in piedi e mi sono incamminato verso una direzione casuale."


translate italian day2_busstop_eve_f17e0aeb:


    th "La fermata dell'autobus..."


translate italian day2_busstop_eve_301f4821:


    "Per qualche motivo è il secondo giorno consecutivo che mi ritrovavo qui."


translate italian day2_busstop_eve_31ee41ff:


    th "Forse inconsapevolmente speravo che il bus potesse tornare qui e riportarmi al mio mondo reale?"


translate italian day2_busstop_eve_b99b8886:


    th "Molto improbabile."


translate italian day2_busstop_eve_74d34bfa:


    th "Ma d'altra parte, perché no...?"


translate italian day2_busstop_eve_39c07ffa:


    "Si è già fatto buio."


translate italian day2_busstop_eve_5daf4589:


    "Me ne stavo lì in piedi, a fissare il cielo notturno."


translate italian day2_busstop_eve_4bb46255:


    "L'astronomia non mi ha mai attratto come l'astronautica."


translate italian day2_busstop_eve_b9df3f0f:


    "È sempre più facile guardare le illustrazioni di costellazioni, nebulose e galassie create dagli artisti, piuttosto che cercare di capire come misurare la velocità angolare e la massa delle stelle."


translate italian day2_busstop_eve_f721c30a:


    "Di sicuro sarei in grado di individuare l'Orsa Maggiore."


translate italian day2_busstop_eve_011b7220:


    "Ma se mi perdessi in una foresta d'inverno, la mia unica via d'uscita potrebbe essere il sapere che il muschio cresce sul lato nord degli alberi."


translate italian day2_busstop_eve_252aa5da:


    "Tuttavia, non credo che basterebbe per guidarmi fuori…"


translate italian day2_busstop_eve_ffa2f154:


    "Dopo qualche minuto sono tornato al Campo."


translate italian day2_busstop_eve_a20cefa7_1:


    "..."


translate italian day2_stage_eve_2c57c848:


    "Ho deciso di andare verso nord (o almeno dove credevo che fosse il nord)."


translate italian day2_stage_eve_ac512323:


    "Dopo qualche minuto, molto più avanti ho visto una specie di palco da concerto, con accanto diverse file di panche di legno."


translate italian day2_stage_eve_0bd0ce88:


    "Sono salito sul palco."


translate italian day2_stage_eve_ff3859bd:


    "C'era una varietà di attrezzature musicali – delle casse audio, un supporto per microfono e addirittura un pianoforte."


translate italian day2_stage_eve_85b7add9:


    "Mi sono immaginato dinanzi a un vasto pubblico, che gridava e cantava il mio nome, e i miei occhi accecati dalle luci dei riflettori."


translate italian day2_stage_eve_aa3b726d:


    "Immaginando di avere una chitarra tra le mani, ho fatto finta di eseguire un lungo e suggestivo assolo."


translate italian day2_stage_eve_7fe7938c:


    "Sono sicuro che se qualcuno mi vedesse ora, si farebbe una bella risata – uno strano tizio che sta da solo sul palco di notte dimenando le braccia, saltando come una scimmia e facendo facce strane."


translate italian day2_stage_eve_dc4028dc:


    th "Spero che nessuno mi abbia visto!"


translate italian day2_stage_eve_a44729f3:


    "Con quel pensiero in testa, sono sceso dal palco e me ne sono andato di corsa."


translate italian day2_stage_eve_a20cefa7:


    "..."


translate italian day2_football_eve_bd44db9a:


    "Sicuramente era ormai troppo tardi per giocare a calcio, e quindi il campo era deserto."


translate italian day2_football_eve_2b9a9e9a:


    "Sono rimasto lì per qualche minuto, ascoltando il silenzio invano, e poi me ne sono andato."


translate italian day2_football_eve_a20cefa7:


    "..."


translate italian day2_dv_a4dc0a8d:


    "Evvai! Ho vinto! Devo aver dato una bella lezione ad Alisa!"


translate italian day2_dv_e284eef4:


    th "Quindi ho fatto bene a scommettere con lei!"


translate italian day2_dv_6d9b3054:


    "Ora non mi restava che sperare che non spargesse quelle voci per vendicarsi del fatto che avesse perso contro di me."


translate italian day2_dv_c21f93b9:


    "Improvvisamente ho sentito il bisogno di celebrare la mia vittoria con una bella nuotata alla spiaggia."


translate italian day2_dv_32525770:


    "A dire il vero non so nuotare molto bene, ma l'idea di immergermi nelle fresche acque al chiaro di luna era allettante."


translate italian day2_dv_e75dca33:


    "Dopotutto, ieri sono stato tutto il giorno con i miei indumenti invernali, e sicuramente si sarebbe potuto spremere il sudore dai miei abiti."


translate italian day2_dv_1c3ad5c8:


    "A quest'ora non avrebbe dovuto esserci nessuno qui, quindi mi sono tolto tutti i vestiti a parte le mutande, e sono entrato in acqua."


translate italian day2_dv_90e7fd64:


    th "Se l'avessi saputo, avrei portato il mio costume da bagno."


translate italian day2_dv_affe5cb3:


    "Di solito non mi allontano oltre i 15-20 metri dalla riva, ma oggi l'euforia della vittoria mi ha spronato a battere il mio record."


translate italian day2_dv_df18f64c:


    "Nuotavo lentamente, osservando ogni movimento delle mie braccia e gambe, mantenendo il respiro costante."


translate italian day2_dv_620aa5dd:


    "Tutt'a un tratto, WHAM!"


translate italian day2_dv_de055afa:


    "Un colpo alle spalle mi ha mandato sott'acqua."


translate italian day2_dv_33564eb7:


    "Ho iniziato ad annaspare rischiando di affogare, ma sono riuscito a tornare in superficie, aggrappandomi alla boa lì vicino."


translate italian day2_dv_8fc9d4bf:


    "Mi sono girato e ho visto Alisa nuotare verso di me."


translate italian day2_dv_33864d31:


    me "Ma che diavolo stai facendo?!"


translate italian day2_dv_c3a057d6:


    dv "Che sto facendo? Mi sto congratulando con il vincitore!"


translate italian day2_dv_d6c4be35:


    me "E se fossi annegato?!"


translate italian day2_dv_f20a4a5d:


    dv "Beh, ti avrei salvato io."


translate italian day2_dv_8386bb65:


    me "Hmph, naturalmente…"


translate italian day2_dv_6409b342:


    "Era decisamente troppo pericoloso restare qui, avrebbe potuto farlo di nuovo. Ho nuotato verso la riva con tutte le mie forze."


translate italian day2_dv_556fac3c:


    "Ho cercato di riprendere fiato. Ero ricoperto di sabbia dalla testa ai piedi."


translate italian day2_dv_f577f3f7:


    "Dopo un istante Alisa è uscita dall'acqua."


translate italian day2_dv_d82da8ae:


    dv "Devo dire che sai nuotare bene!"


translate italian day2_dv_869a64b8:


    th "Io non direi proprio."


translate italian day2_dv_4a35be24:


    me "Sì, anche tu."


translate italian day2_dv_64dd4f95:


    dv "Lo so, non è evidente?"


translate italian day2_dv_cbc4bb84:


    "Sono rimasto in silenzio."


translate italian day2_dv_9a5b6429:


    dv "Hai vinto due volte contro di me oggi.{w} Questo significa che hai ripagato i tuoi due debiti."


translate italian day2_dv_ff2a842c:


    th "Di che debiti sta parlando?"


translate italian day2_dv_7d05db3b:


    "Sembra che Alisa abbia problemi nel valutare la realtà."


translate italian day2_dv_493885e5:


    me "Grazie mille…"


translate italian day2_dv_27c62366:


    "Le ho detto con sarcasmo."


translate italian day2_dv_01820ffe:


    dv "Sai, non sei un perdente come credevo…"


translate italian day2_dv_5b1b7a36:


    "Alisa stava indossando un costume da bagno che risaltava perfettamente le sue forme."


translate italian day2_dv_84e41e3b:


    "Beh, considerando tutti i difetti della sua personalità, era un punto a suo favore."


translate italian day2_dv_c6ee82bf:


    me "E da quando sarei un perdente?"


translate italian day2_dv_945c59bc:


    "Ha sorriso astutamente."


translate italian day2_dv_3bbe4c98:


    dv "Perché, non lo sei?"


translate italian day2_dv_f837dd45:


    me "Certo che no!"


translate italian day2_dv_072e019b:


    dv "E come vorresti dimostrarlo?"


translate italian day2_dv_fdf9f2c2:


    me "Non devo dimostrare proprio niente!"


translate italian day2_dv_a3b5accf:


    dv "Oh! Tutto qui?"


translate italian day2_dv_516018de:


    "Ha detto in tono tranquillo."


translate italian day2_dv_e7ec8520:


    me "Sì, tutto qui..."


translate italian day2_dv_4a405aad:


    "Siamo rimasti in silenzio."


translate italian day2_dv_1c5b30ab:


    "Un vento leggero accompagnava le onde verso la riva, riportandole poi indietro."


translate italian day2_dv_00eafdba:


    "Alisa aveva ancora lo sguardo perso nel vuoto, come se si fosse dimenticata della mia presenza."


translate italian day2_dv_c4f3bd04:


    me "Ehi, Terra chiama Alisa!"


translate italian day2_dv_2d89497e:


    "Il suo sguardo perso si è tramutato immediatamente in quello usuale."


translate italian day2_dv_b63997f5:


    dv "Comunque, alla prossima."


translate italian day2_dv_293bd21f:


    "Ha raccolto i suoi vestiti e se n'è andata."


translate italian day2_dv_a20cefa7:


    "..."


translate italian day2_dv_837271a8:


    "Era tardi ormai, ma ho deciso di restare sdraiato lì ancora per un po', osservando le stelle."


translate italian day2_dv_95ca3d6a:


    "Dopotutto, in passato non ho avuto molte occasioni per farlo."


translate italian day2_dv_8b8426ec:


    "O semplicemente cercavo di evitare ogni opportunità."


translate italian day2_dv_b48dade4:


    th "Beh, pensandoci bene, la luce lontana delle stelle impiega anni per raggiungere i nostri occhi…"


translate italian day2_dv_5739d22f:


    th "Adesso riesco a vedere quella stella solo perché molto tempo fa era ancora splendente."


translate italian day2_dv_799ecc91:


    th "E in questo preciso istante quella stella potrebbe essere già sparita, anche se riesco ancora a vedere la sua luce…"


translate italian day2_dv_1c004364:


    th "Aspetta un momento!{w} Si è portata via anche i miei vestiti!"


translate italian day2_dv_bb8c1b2a:


    "Sono balzato in piedi e mi sono guardato attorno."


translate italian day2_dv_ec32c246:


    "Senza dubbio, Alisa aveva preso la mia uniforme."


translate italian day2_dv_6ccc3e04:


    th "Dannazione!"


translate italian day2_dv_37751890:


    "E io stavo già iniziando a pensare che in fondo non fosse poi così cattiva…"


translate italian day2_dv_801f233a:


    th "Devo inventarmi qualcosa il prima possibile."


translate italian day2_dv_1c7817b8:


    th "Ovviamente potrei andare a lamentarmi da Olga Dmitrievna, ma di certo non in queste mutande bagnate…"


translate italian day2_dv_a11b7a5a:


    "Non sapevo nemmeno dove abitasse Alisa."


translate italian day2_dv_9aedec39:


    th "E bussare ad ogni porta non sarebbe di certo una buona idea."


translate italian day2_dv_faa17736:


    th "Forse dovrei andare da Slavya?"


translate italian day2_dv_691cf254:


    th "Certo, come no – in giro in mutande di notte…{w} L'unica cosa che mi manca è una rosa tra i denti!"


translate italian day2_dv_1388e5eb:


    "Ad ogni modo, sembra proprio che io sia stato fregato!"


translate italian day2_dv_a6749c0b:


    "In ogni caso devo fare qualcosa."


translate italian day2_dv_6c982559:


    "Per fortuna non è passato molto tempo da quando Alisa se n'è andata, quindi forse posso ancora raggiungerla..."


translate italian day2_dv_9744eb3a:


    "...presto!"


translate italian day2_dv_1e0acdbc:


    "In un batter d'occhio mi sono ritrovato alla piazza."


translate italian day2_dv_20bc07ef:


    "Con mia grande sorpresa, Alisa stava seduta su una panchina, sembrava annoiata."


translate italian day2_dv_0706b8a9:


    "Si era già cambiata."


translate italian day2_dv_679b46e1:


    me "Ridammeli!"


translate italian day2_dv_e5a0d224:


    dv "Sì certo, prendi qua…"


translate italian day2_dv_1063c21e:


    "Ha borbottato in tono colpevole, riconsegnandomi l'uniforme."


translate italian day2_dv_51149e68:


    me "…"


translate italian day2_dv_6cf682fe:


    dv "Non credere che ti stessi aspettando o qualcosa del genere..."


translate italian day2_dv_a71ccb66:


    "Si è voltata lentamente e si è avviata verso le casette."


translate italian day2_dv_24bb116e:


    "Sono rimasto fermo immobile."


translate italian day2_dv_a26ba0eb:


    "Non mi sarei mai aspettato un tale cambiamento da parte sua."


translate italian day2_dv_b327337f:


    th "Forse è stata presa dai sensi di colpa…"


translate italian day2_dv_4e97c063:


    th "Molto probabile…"


translate italian day2_dv_afa37a67:


    th "Comunque, è meglio stare alla larga da lei, gli eventi di questa sera non cambiano niente."


translate italian day2_dv_8b243a91:


    "Mi sono incamminato verso la casetta di Olga Dmitrievna."


translate italian day2_sl_51d58656:


    "Stavo per tornarmene al Campo, quando improvvisamente ho sentito un rumore provenire dall'altra parte del cancello."


translate italian day2_sl_5c128ad7:


    th "Chi potrebbe essere...?"


translate italian day2_sl_01f96367:


    "Vicino alla sede dei circoli mi è parso di aver visto qualcuno che si stava incamminando verso la foresta."


translate italian day2_sl_d5b66cdc:


    "Faceva così buio che l'unica cosa che sono riuscito a scorgere era un'ombra sfocata."


translate italian day2_sl_6368700c:


    "Mi sono chiesto chi potesse essere a quest'ora della notte."


translate italian day2_sl_5f2c4088:


    th "Un pioniere che sta violando le regole? Ahi ahi!"


translate italian day2_sl_bb1c83e5:


    "Ho seguito l'ombra con cautela, cercando di non far rumore."


translate italian day2_sl_707dc7b1:


    "Continuavo a passare da una parte all'altra del sentiero, e dopo un po' mi sono trovato nelle profondità della foresta, avendo perso di vista lo sconosciuto."


translate italian day2_sl_7c11ada4:


    th "Forse è meglio se torno indietro?"


translate italian day2_sl_da7dcced:


    "Gli alberi si stavano diradando, e alla fine mi sono trovato davanti alla meravigliosa vista di un piccolo laghetto in mezzo alla foresta."


translate italian day2_sl_9ae5e62c:


    "E poi ho notato Slavya…{w} Stava danzando sulla riva, togliendosi il foulard e l'uniforme, lanciandoli verso il cielo."


translate italian day2_sl_e1672e9e:


    "Quello spettacolo era ancora più magico della mia presenza in questo Campo."


translate italian day2_sl_4b243d51:


    "Slavya sembrava uno spirito della foresta, o forse una ninfa."


translate italian day2_sl_76f15cf3:


    "Appariva così naturale, più come un'antica divinità che come un essere umano."


translate italian day2_sl_0ad8db2d:


    "Tutto questo mi ricordava le teorie teologiche che avevo letto tempo fa."


translate italian day2_sl_efc6aadb:


    "Il Panteismo – cioè l'idea che Dio sia tutto quello che ci circonda."


translate italian day2_sl_df1b0b99:


    th "E se la mia presenza qui fosse dovuta alla Divina Provvidenza, piuttosto che agli alieni o ai viaggi nel tempo?"


translate italian day2_sl_9a178047:


    th "In effetti Slavya mi ha detto che ama la natura."


translate italian day2_sl_4850f462:


    th "Quindi, persino lei si rivela essere un enigma, non è così?"


translate italian day2_sl_3565ced7:


    "Ma ecco i suoi ultimi abiti che cadono a terra, e..."


translate italian day2_sl_2dc5f1ec:


    "Si è immersa nell'acqua.{w} Nuda..."


translate italian day2_sl_62969ec1:


    "Slavya si è immersa nell'acqua."


translate italian day2_sl_3f9652c6:


    "Mi vergognavo, ma non riuscivo a distogliere lo sguardo da lei."


translate italian day2_sl_62af0d4c:


    "Il chiaro di luna rifletteva la sua pelle bagnata, facendola sembrare un'antica statua greca.{w} La Venere di Milo, forse?"


translate italian day2_sl_9a56444a:


    "Quella scena era talmente magica che non c'era spazio per alcun desiderio carnale – soltanto la sublime ammirazione della pura bellezza."


translate italian day2_sl_88bcf51a:


    "Ero incantato dalla perfezione delle forme di Slavya, dimenticandomi tutto il resto."


translate italian day2_sl_1275a4ec:


    "Forse dopotutto non mi trovavo all'inferno, ma in paradiso?"


translate italian day2_sl_fed951af:


    "Improvvisamente un ramo secco si è spezzato sotto al mio piede. Slavya si è guardata attorno, ma non avrebbe potuto in alcun modo vedermi in quell'oscurità.{w} O almeno così mi sembrava."


translate italian day2_sl_4f55ff6e:


    "È uscita rapidamente dall'acqua, si è vestita di fretta ed è sparita nella foresta."


translate italian day2_sl_4c18ac16:


    "L'ho seguita silenziosamente."


translate italian day2_sl_389d811b:


    "Slavya si stava destreggiando tra gli alberi, scegliendo il percorso meno difficile ed evitando con grazia gli ostacoli, i rami caduti e le buche."


translate italian day2_sl_1f1f282b:


    "Era molto difficile tenere il suo passo. Inoltre, non dovevo far rumore. Per prima cosa, sbirciare di nascosto non è una bella cosa. E seconda cosa, restava ancora da scoprire cosa stesse effettivamente facendo lì."


translate italian day2_sl_10a06639:


    "Ad ogni modo mi è sembrato qualcosa di abbastanza innocente – non sembrava avesse a che fare con il mio arrivo in questo luogo."


translate italian day2_sl_8edf9ccd:


    "Era semplicemente {i}innocente{/i}. Non c'era bisogno di indagare oltre."


translate italian day2_sl_c2d72193:


    "Alla fine siamo ritornati alla piazza."


translate italian day2_sl_bfc4d100:


    "Slavya si è fermata e si è girata verso di me."


translate italian day2_sl_8f535089:


    sl "Credevi che non ti avessi notato?"


translate italian day2_sl_996aa1aa:


    "Ero confuso, ma ho cercato di sembrare calmo."


translate italian day2_sl_a59596f3:


    me "Da quanto?"


translate italian day2_sl_ee6a8067:


    sl "Non saprei..."


translate italian day2_sl_abc9e8c7:


    "Slavya si è avvicinata."


translate italian day2_sl_7a2e9449:


    sl "Da cinque minuti, forse."


translate italian day2_sl_c9c9cf9a:


    me "Quindi, anche al lago...?"


translate italian day2_sl_44b1529f:


    sl "Di che lago stai parlando?"


translate italian day2_sl_c97fadc0:


    me "Beh..."


translate italian day2_sl_f0f0ce17:


    "Slavya sembrava sinceramente sorpresa, quindi non riuscivo a capire se stesse facendo finta che non fosse successo nulla, oppure se..."


translate italian day2_sl_67207812:


    me "Beh, non importa."


translate italian day2_sl_8d771c48:


    "Ho cercato di fare il gentiluomo (per quanto fosse possibile in una tale situazione) e non ho aggiunto altro."


translate italian day2_sl_af1d965e:


    sl "Va bene."


translate italian day2_sl_b1dd558c:


    "Con mia sorpresa, non ha indagato oltre."


translate italian day2_sl_29a99ab6:


    sl "Che notte meravigliosa!"


translate italian day2_sl_532e90d7:


    "Slavya si è seduta su una panchina e ha guardato il cielo."


translate italian day2_sl_71dfbb36:


    me "Suppongo che notti come questa siano frequenti qui."


translate italian day2_sl_19f9cb53:


    sl "Beh, probabilmente..."


translate italian day2_sl_437776df:


    me "Perché quell'incertezza?"


translate italian day2_sl_e064e531:


    sl "Non so, devo essermi persa nei miei pensieri."


translate italian day2_sl_c233b6a2:


    me "A cosa stavi pensando?"


translate italian day2_sl_7ef301f9:


    "Mi ha fissato come se avessi qualcosa sul viso, ma poi è tornata a osservare le stelle."


translate italian day2_sl_c55641df:


    sl "Ogni tanto mi capita di immergermi nei pensieri la notte...{w} Durante il giorno sono sempre occupata e non ho tempo per rilassarmi, ma durante la notte è così tranquillo qui."


translate italian day2_sl_da5951c8:


    sl "Se non fosse per i grilli e gli uccelli notturni, ci si potrebbe sentire faccia a faccia con l'universo."


translate italian day2_sl_6c369edd:


    "Per qualche ragione non pensavo che Slavya fosse in grado di parlare di certe cose."


translate italian day2_sl_3229d777:


    me "Per me è anche troppo silenzioso qui."


translate italian day2_sl_5606d7d1:


    sl "Davvero?"


translate italian day2_sl_0c5e8544:


    me "Sì, davvero. C'è qualcosa di sbagliato in ciò?"


translate italian day2_sl_0f20c625:


    sl "Beh..."


translate italian day2_sl_fcc29073:


    sl "Va bene!"


translate italian day2_sl_91f795db:


    "Si è alzata rapidamente e si è aggiustata la gonnellina."


translate italian day2_sl_411ed0f9:


    sl "È ora di andare a dormire."


translate italian day2_sl_c3abce40:


    me "Buona notte!"


translate italian day2_sl_ee02ee2d:


    "L'ho guardata mentre si allontanava."


translate italian day2_sl_91aa9e4f:


    "La nostra conversazione poteva anche essere insignificante, ma mi è sembrato che le sue parole avessero avuto una certa profondità, un significato che sarebbe potuto apparire solo {i}qui{/i}, solo accanto a lei."


translate italian day2_sl_e881ac0f:


    "Nonostante la mia situazione, sembrava che tali momenti di pace e serenità fossero assolutamente necessari. Mi facevano sentire legato all'universo."


translate italian day2_sl_fe349bbe:


    "Oserei dire vitali – in particolare in quel momento!"


translate italian day2_sl_a20cefa7:


    "..."


translate italian day2_sl_a20cefa7_1:


    "..."


translate italian day2_sl_41f711f9:


    "Non sono sicuro di quanto tempo io fossi rimasto lì, ma dopo un po' mi è venuto sonno."


translate italian day2_un_197a0aaf:


    "Volevo solo andarmene."


translate italian day2_un_146e98d1:


    th "Com'è possibile che io abbia perso già al primo round...!"


translate italian day2_un_a3a64f91:


    "Non avevo scuse."


translate italian day2_un_5e63525e:


    "Il campo sportivo sembrava il luogo più adatto per stare un po' da solo."


translate italian day2_un_df7b3cbc:


    th "A chi verrebbe in mente di giocare a calcio a quest'ora?"


translate italian day2_un_579f8206:


    "Mi sono seduto su una panca davanti al campo da calcio e ho cominciato a pensare a quello che era appena successo."


translate italian day2_un_404f90fe:


    "Improvvisamente ho sentito dei rumori provenire dal campo di pallavolo."


translate italian day2_un_a9cd9809:


    "Guardando più da vicino, ho notato qualcuno che stava muovendo freneticamente le braccia."


translate italian day2_un_8464c860:


    th "A chi sta gesticolando?"


translate italian day2_un_1e068335:


    "Con mia grande sorpresa ho scoperto che era Lena."


translate italian day2_un_fec5d55a:


    "Stava lanciando in aria un volano e cercava di colpirlo con la racchetta."


translate italian day2_un_aa5dee50:


    "Tuttavia, a essere davvero, davvero sinceri, era proprio una schiappa."


translate italian day2_un_3355bd20:


    "L'ho guardata per un po', e poi ho deciso di avvicinarmi a lei."


translate italian day2_un_24cfa746:


    "Ho fatto il giro del campo e sono entrato in modo che potesse vedermi."


translate italian day2_un_8c20dbc2:


    "Lena è timida come un cervo, quindi avrei fatto meglio a non ripetere i miei stessi errori delle volte precedenti."


translate italian day2_un_1a8f3935:


    me "Ciao!"


translate italian day2_un_5a070b64:


    "Mi ha lanciato un'occhiata e subito ha nascosto la racchetta e il volano dietro alle sue spalle."


translate italian day2_un_9938563a:


    me "Ti piace il badminton?"


translate italian day2_un_9c33c3aa:


    un "Non esattamente…"


translate italian day2_un_dd278e82:


    me "Vedo che non stai avendo molto successo.{w} Ti serve una mano?"


translate italian day2_un_319450df:


    "A dire il vero, non ero molto bravo a badminton, ma come ogni altro bambino ci avevo giocato di tanto in tanto."


translate italian day2_un_627e964d:


    me "Ti faccio vedere."


translate italian day2_un_626232a6:


    un "Grazie."


translate italian day2_un_ac04f5d1:


    "È arrossita."


translate italian day2_un_3883f3ff:


    un "Vorrei entrare nella squadra di badminton, ma non credo di essere abbastanza brava…"


translate italian day2_un_aa50848d:


    un "Non avrei voluto provare nemmeno oggi, ma…"


translate italian day2_un_418d4bde:


    "Ha alzato lo sguardo verso di me."


translate italian day2_un_90c16780:


    un "Non ho mai avuto molta fortuna con le carte, ma dato che oggi sono riuscita a vincere… ho pensato che sarebbe stato lo stesso con il volano…"


translate italian day2_un_7d6095b6:


    "Dopo quelle sue parole mi sono reso conto che l'aver perso contro di lei era doppiamente deprimente."


translate italian day2_un_2a1db9ac:


    me "Non mi sarei mai immaginato che tu fossi interessata allo sport."


translate italian day2_un_a852076f:


    "È arrossita di nuovo."


translate italian day2_un_0f6c5c86:


    me "Oh, scusa…{w} Dai, ti faccio vedere!"


translate italian day2_un_3d8ebca6:


    "Ho preso la racchetta, ho lanciato il volano verso l'alto e…"


translate italian day2_un_b356ae1f:


    "L'ho colpito con una tale forza che è finito oltre la recinzione, sparendo fra gli alberi."


translate italian day2_un_9b63169a:


    me "Oh, mi dispiace!"


translate italian day2_un_f1725684:


    "Wow, non mi sarei mai aspettato un tiro del genere."


translate italian day2_un_5d87ca29:


    un "Non importa…{w} Anche se, era l'ultimo rimasto…"


translate italian day2_un_c4393a9f:


    me "Era l'ultimo? Allora dobbiamo andare a recuperarlo!"


translate italian day2_un_9d020e39:


    un "Meglio di no…{w} Di là c'è… Nella foresta…"


translate italian day2_un_d5d9bec0:


    me "Cosa c'è? Un goblin?"


translate italian day2_un_7c8e7385:


    "Ho riso forte."


translate italian day2_un_8a2da24a:


    un "Forse…"


translate italian day2_un_8def79fc:


    "Sembrava che io fossi l'unico in vena di scherzare."


translate italian day2_un_b96a2bcb:


    me "Avanti, non c'è niente di cui aver paura!"


translate italian day2_un_5806cffb:


    un "Beh, se verrai con me…"


translate italian day2_un_d8c460b7:


    "Siamo usciti dal campo, e ho iniziato a ispezionare gli alberi."


translate italian day2_un_965c33d2:


    "All'improvviso, il bubolo di un gufo ha trafitto la notte."


translate italian day2_un_e9ba05f1:


    "Lena si è spaventata talmente tanto che si è aggrappata stretta a me, in una sorta di abbraccio."


translate italian day2_un_8bfe647b:


    "Quella sua forte stretta era un po' imbarazzante."


translate italian day2_un_827560fe:


    "Sentire il corpo di una ragazza così vicino, sentire il suo calore!"


translate italian day2_un_12cdb438:


    "Mi ha riempito di tenerezza."


translate italian day2_un_2b91a3d1:


    "Ho istintivamente sentito il desiderio di proteggerla e di non permettere a nessuno di farle del male, anche se si fosse trattato semplicemente di un gufo o di un altro volatile notturno."


translate italian day2_un_5e49a6dc:


    "Il mio unico desiderio era che quell'abbraccio durasse per l'eternità."


translate italian day2_un_a89add5c:


    "Tuttavia, tutte le cose belle devono finire prima o poi."


translate italian day2_un_6c0ae31e:


    "Dopo un po' ho scoperto che si trattava di un piccolo gufo, che stava gridando su un ramo accanto a noi."


translate italian day2_un_12d3ce3e:


    "E si stava tenendo stretto il nostro volano."


translate italian day2_un_f751a0a0:


    me "Era di questo che avevi paura?"


translate italian day2_un_0905212f:


    un "Uh-huh…"


translate italian day2_un_69e96144:


    me "Guarda! Non fa per niente paura."


translate italian day2_un_f200dfd5:


    "Lena ha sbirciato da dietro le mie spalle."


translate italian day2_un_6543e2da:


    un "Hai ragione, non fa paura…"


translate italian day2_un_efd93d7c:


    me "Ehi, aspetta un secondo."


translate italian day2_un_ae039cd0:


    "Mi sono liberato gentilmente dal suo abbraccio e mi sono avvicinato al volatile."


translate italian day2_un_2aa9b5ff:


    "All'inizio sembrava che fosse spaventato e che volesse volare via lasciando cadere il volano."


translate italian day2_un_d348cee1:


    "Ma il piccolo gufo era ancora fermo lì."


translate italian day2_un_1ba95990:


    "Sono riuscito a prendere il volano e a portarglielo via con cautela."


translate italian day2_un_56edd61c:


    me "Ehi, guarda! Sembra mansueto!{w} Vuoi provare ad accarezzarlo?"


translate italian day2_un_db78f5e4:


    un "Magari un'altra volta...?"


translate italian day2_un_d01f4370:


    "Ho ridato il volano a Lena."


translate italian day2_un_3cd7a073:


    un "Grazie."


translate italian day2_un_783cf09d:


    "Ha sorriso dolcemente."


translate italian day2_un_4d93b5df:


    un "Devo andare."


translate italian day2_un_0f243eb9:


    me "Buona fortuna per il badminton."


translate italian day2_un_f5e7f5a8:


    "Lena ha sorriso di nuovo, e si è affrettata ad abbandonare il campo."


translate italian day2_un_4bf89e3c:


    th "Quanto dolce può essere una ragazza?!"


translate italian day2_un_a20cefa7:


    "..."


translate italian day2_us_ce80d80d:


    "Gli eventi di questa giornata continuavano a scorrermi limpidi nella mente: quella maledetta e inutile lista delle cose da fare, quello stupido torneo..."


translate italian day2_us_5cef682c:


    "Questa notte non avevo voglia di fare alcunché né di parlare con nessuno.{w} Investigare riguardo alla mia complicata situazione era l'ultima cosa sulla faccia della Terra che volessi fare questa sera."


translate italian day2_us_d9b0b0f8:


    "Mi sono diretto verso nord.{w} O almeno dove credevo che fosse il nord."


translate italian day2_us_d66455f1:


    "Era una mia abitudine sin da giovane – andare verso nord."


translate italian day2_us_a488b614:


    "Perché la zona nord del mio distretto era migliore della zona sud."


translate italian day2_us_1759ce8e:


    "E non amavo molto andare in vacanza nelle località del Mar Nero – per me i boschi e i campi erano molto meglio delle spiagge e delle dune."


translate italian day2_us_ac512323:


    "Dopo qualche minuto mi sono trovato davanti a un palco all'aperto, con attorno alcune panche di legno."


translate italian day2_us_0bd0ce88:


    "Sono salito sul palco."


translate italian day2_us_ff3859bd:


    "C'era una varietà di attrezzature musicali – delle casse audio, un supporto per microfono e addirittura un pianoforte."


translate italian day2_us_85b7add9:


    "Mi sono immaginato dinanzi ad un vasto pubblico, che gridava e cantava il mio nome, e i miei occhi accecati dalle luci dei riflettori."


translate italian day2_us_aa3b726d:


    "Immaginando di avere una chitarra tra le mani, ho fatto finta di eseguire un lungo e suggestivo assolo."


translate italian day2_us_7fe7938c:


    "Suppongo che se qualcuno mi avesse visto ora, si sarebbe fatto una bella risata – uno strano tizio che sta da solo sul palco di notte dimenando le braccia, saltando come una scimmia e facendo facce strane."


translate italian day2_us_dc4028dc:


    th "Spero che nessuno mi abbia visto!"


translate italian day2_us_fa3c839e:


    us "Ehi!"


translate italian day2_us_b65b6fe7:


    "Ho sentito provenire dall'alto."


translate italian day2_us_997dd5cf:


    "Ho alzato lo sguardo e ho visto Ulyana che penzolava da una trave sotto al soffitto."


translate italian day2_us_89f814b5:


    us "E tu che ci fai qui?"


translate italian day2_us_2ec2a349:


    me "Io stavo solo…"


translate italian day2_us_dab45ac8:


    "Inutile inventarsi scuse."


translate italian day2_us_cfae0a9c:


    me "Hai visto tutto, non è così?"


translate italian day2_us_7217c832:


    "Le ho chiesto frustrato, voltandomi."


translate italian day2_us_a7445561:


    us "Oh, vedo uno sprecato talento di chitarrista in te."


translate italian day2_us_8f216cfd:


    "Sono rimasto in silenzio."


translate italian day2_us_57071005:


    us "Ehi, dai, non mettere il broncio! È stato divertente!"


translate italian day2_us_20f32dda:


    "Ha ridacchiato."


translate italian day2_us_d0b077c3:


    me "Piuttosto divertente, eh?"


translate italian day2_us_2ff94fc7:


    "Ho sbuffato."


translate italian day2_us_df6dbf74:


    us "Già."


translate italian day2_us_69b6d036:


    "Mi ha risposto serenamente Ulyana."


translate italian day2_us_400c9ff0:


    us "Vieni."


translate italian day2_us_25f7abbc:


    me "Dove?"


translate italian day2_us_8b888dc6:


    us "Qui da me!"


translate italian day2_us_ae5dfee6:


    me "Non ho proprio intenzione di salire lassù, è inutile che provi a convincermi!"


translate italian day2_us_39336f89:


    "Non perché io abbia paura delle altezze. È solo che non aveva senso arrampicarsi lassù, giusto?"


translate italian day2_us_053e61c9:


    us "No aspetta! Vieni più vicino."


translate italian day2_us_45d1c061:


    "Sentivo che qualcosa di spiacevole stava per accadere, ma mi sono comunque avvicinato a lei."


translate italian day2_us_d926232c:


    "Quando sono arrivato sotto al punto in cui si trovava, mi ha gridato:"


translate italian day2_us_5736d39d:


    us "Prendimi!"


translate italian day2_us_b2c5ecfe:


    "Ed è saltata…"


translate italian day2_us_b9b5fa03:


    "In un istante la mia mente è stata invasa da migliaia di pensieri."


translate italian day2_us_20495ccc:


    th "Come posso afferrarla? È proprio necessario? E se morisse? E se si rompesse una gamba? Perché diavolo deve capitare tutto a me?!"


translate italian day2_us_614bbca7:


    th "La colpa è sua – basta scherzi!"


translate italian day2_us_7c464e77:


    "Wow, quanti pensieri sono apparsi e scomparsi dalla mia mente in una frazione di secondo."


translate italian day2_us_54bb57e2:


    "Mentre a volte ci vogliono anni per farne affiorare uno sensato."


translate italian day2_us_39a5f3d1:


    "Alla fine la logica e l'istinto di autoconservazione hanno avuto la meglio, e mi sono scansato."


translate italian day2_us_818e5d64:


    "Ulyana è atterrata, è rotolata per terra, si è rialzata in un istante e mi ha guardato con risentimento."


translate italian day2_us_9a98847e:


    us "Perché non mi hai presa?"


translate italian day2_us_5181be41:


    me "Ma non ti sei fatta male..."


translate italian day2_us_37dbf984:


    "Le ho risposto, distogliendo lo sguardo."


translate italian day2_us_a7d45458:


    us "E se mi fossi fatta male?"


translate italian day2_us_c1b7ead1:


    me "Ma non è stato così!{w} Che ti è preso? Hai guardato troppi film d'azione ultimamente?"


translate italian day2_us_d3a6c509:


    us "Allora non t'importa di me?"


translate italian day2_us_f396e777:


    "Ha sorriso maliziosamente."


translate italian day2_us_89c187b4:


    me "Beh, in una situazione del genere… Certo che mi importa."


translate italian day2_us_c906b0f6:


    us "Sono lusingata."


translate italian day2_us_8de3d1a0:


    me "Ehi, non farti strane idee…"


translate italian day2_us_630ea867:


    us "Ok, ok. Ti perdono per il torneo di carte."


translate italian day2_us_0a94a0ea:


    me "E tu non…"


translate italian day2_us_39d27213:


    "Non ho avuto il tempo di finire la frase – perché Ulyana è saltata giù dal palco ed è sparita nell'oscurità della notte."


translate italian day2_us_bb143e3a:


    th "Già. Un altro scherzo infantile da parte della sciocca ragazzina."


translate italian day2_us_b40ec65c:


    th "Certo, ero preoccupato per lei in quel momento."


translate italian day2_us_2b9842d5:


    th "Come avrebbe fatto chiunque al mio posto…"


translate italian day2_us_78ddfb9b:


    "Mi sono incamminato verso la mia dimora, maledicendo Ulyana un'altra volta."


translate italian day2_main4_6f1dc765:


    "Per la prima volta oggi mi sono accorto di quanto fossi stanco."


translate italian day2_main4_3fd94a8a:


    "Non c'era alcuna luce alla finestra, quindi Olga Dmitrievna doveva essersi già addormentata."


translate italian day2_main4_9390dd47:


    th "Stranamente ieri aveva aspettato il mio ritorno."


translate italian day2_main4_aceec196:


    "Sono entrato, mi sono svestito senza far rumore, e mi sono sdraiato sul letto."


translate italian day2_main4_2b052db0:


    th "Pensandoci bene, oggi non ho scoperto nulla di nuovo riguardo alla mia situazione."


translate italian day2_main4_0abec8be:


    th "Infatti ho trascorso l'intera giornata a fare cose inutili, cose che non avrei mai pensato di fare nel mondo reale."


translate italian day2_main4_4b59bf55:


    th "Anche se avevo tutto il tempo per farle."


translate italian day2_main4_0a500eea:


    th "E per quanto tempo ancora io debba restare qui – è un mistero."


translate italian day2_main4_381a6c2e:


    th "Forse per l'eternità, o forse solo per qualche altro minuto."


translate italian day2_main4_c21f9a60:


    "Non volevo pensare al passato, di come io fossi arrivato in questo Campo."


translate italian day2_main4_3d823552:


    "Per la prima volta da molto tempo mi sono sentito veramente stanco – stanco emotivamente, stanco fisicamente, e Dio solo sa cos'altro..."


translate italian day2_main4_5016abac:


    "Volevo solo che tutto e tutti sparissero – a partire dai miei stessi pensieri.{w} Volevo che questo casino si risolvesse da solo."


translate italian day2_main4_ceda5e44:


    "O almeno senza la mia attiva partecipazione."


translate italian day2_main4_dabe5250:


    th "E se dovessi restare qui per l'eternità?"


translate italian day2_main4_92283096:


    th "Allora devo abituarmi..."


translate italian day2_main4_410fa16d:


    th "Tutto qui...? Io... Non mi sento pronto...{w} Ehm..."


translate italian day2_main4_dcadb353:


    "La mia coscienza mi stava abbandonando lentamente, e stava diventando difficile concentrarsi su qualcosa di specifico."


translate italian day2_main4_72f8d69d:


    th "Forse è meglio aspettare fino a domani..."


translate italian day2_main4_ba8204f7:


    "Mi sono girato nel letto e mi sono addormentato."
# Decompiled by unrpyc: https://github.com/CensoredUsername/unrpyc
