
translate italian day4_std_morning_81731118:


    "Mi sono svegliato con un ronzio infernale nella testa."


translate italian day4_std_morning_163f48a5:


    "Quel ronzio sembrava provenire dalle profondità della mia coscienza."


translate italian day4_std_morning_dfa1885d:


    "Ma dopo aver ripreso i sensi, mi sono reso conto che la causa di quel ronzio era la mia sveglia."


translate italian day4_std_morning_c8d82a68:


    th "Strano. Da dov'è sbucata? Chi l'ha messa accanto al mio letto?"


translate italian day4_std_morning_51698244:


    "Erano le sette e mezza, secondo l'orologio."


translate italian day4_std_morning_77df9db1:


    "Olga Dmitrievna se n'era già andata, e nessuno mi ha costretto a partecipare all'allineamento."


translate italian day4_std_morning_f7391aeb:


    th "Così posso dormire ancora un po'."


translate italian day4_std_morning_08555663:


    "Ho chiuso gli occhi, ma sembrava che la mia coscienza avesse già preso il suo caffè, e che fosse pronta per una lunga giornata produttiva. Dovevo alzarmi."


translate italian day4_std_morning_dae26579:


    th "Dovrei pianificare la mia giornata. Dopotutto, devo riuscire a scoprire qualcosa almeno oggi."


translate italian day4_std_morning_213e2357:


    "Anche se non mi veniva in mente niente."


translate italian day4_std_morning_dbbf160b:


    th "Bene. Devo svegliarmi del tutto e lavarmi."


translate italian day4_std_morning_0f23f979:


    "Mentre stavo andando verso i lavabi ho incontrato Zhenya."


translate italian day4_std_morning_2b068e19:


    me "Perché ti sei svegliata così presto?"


translate italian day4_std_morning_86a5cb53:


    mz "C'è qualcosa di sbagliato nello svegliarsi presto?"


translate italian day4_std_morning_bb2e0646:


    "Me l'ha chiesto come se l'avessi insultata."


translate italian day4_std_morning_08d4c5a9:


    me "No, niente. Sono solo curioso."


translate italian day4_std_morning_44cce5e2:


    mz "Non sono affari tuoi!"


translate italian day4_std_morning_b3f5b0be:


    "Perbacco. Zhenya non è molto simpatica neppure quando è di buonumore. Ma oggi sembrava proprio furiosa."


translate italian day4_std_morning_eabc170b:


    "L'acqua fredda mi ha rianimato, la nebbia nella mia mente si è dissipata, e i miei pensieri hanno cominciato a organizzarsi."


translate italian day4_std_morning_ca15c6d0:


    "Curiosamente, mi stavo preoccupando più di trovare un buon posto alla mensa, anziché cercare risposte."


translate italian day4_std_morning_db0151db:


    "Mi sono lavato i denti e stavo per andarmene, quando un lieve rumore ha attraversato le mie orecchie."


translate italian day4_std_morning_4766e5e2:


    th "Probabilmente è uno scoiattolo, o qualche altro animale."


translate italian day4_std_morning_3cb11a08:


    "Ho sentito un altro fruscio, questa volta più distante."


translate italian day4_std_morning_2a6c3c6a:


    "Ho camminato per una decina di metri sul sentiero, in cerca della fonte di quel rumore."


translate italian day4_std_morning_e0c86c0b:


    "Nessuno. Solo la foresta mattutina."


translate italian day4_std_morning_82583edb:


    "Sono tornato ai lavabi e ho visto Miku che stava cercando qualcosa nell'erba."


translate italian day4_std_morning_152eeed8:


    "Nel vedermi ha sorriso ed è saltata in piedi davanti a me."


translate italian day4_std_morning_a1863396:


    mi "Oh, ciao! Buongiorno! Per sbaglio ho fatto cadere il dentifricio in polvere. Sto cercando di raccoglierlo."


translate italian day4_std_morning_3243720e:


    "La rugiada sull'erba non sembrava aiutarla nell'intento."


translate italian day4_std_morning_7f1bbccc:


    me "Sei sicura che sia una buona idea?"


translate italian day4_std_morning_fdcbae6e:


    mi "Beh, perché no? Che altro potrei fare? Non ne ho più! Era l'unico che avevo!"


translate italian day4_std_morning_30820f3b:


    "Ha messo il broncio come un bambino a cui era stato appena tolto il giocattolo preferito."


translate italian day4_std_morning_7ff796f1:


    me "Ecco, prendi il mio..."


translate italian day4_std_morning_fd9fb11a:


    "Ho rovistato nella mia borsa."


translate italian day4_std_morning_1ac55bec:


    "Ma niente dentifricio."


translate italian day4_std_morning_4396dacc:


    th "Hmmm… strano. Non l'avevo appena messo dentro?{w} Ho lasciato la borsa solo per un istante, e il dentifricio è già sparito."


translate italian day4_std_morning_b866f4d7:


    me "Senti, devo essermelo dimenticato…"


translate italian day4_std_morning_b5e2c150:


    "Non volevo dirle che l'avevo perso."


translate italian day4_std_morning_25a1bc7e:


    "Conoscendo la sensibilità di quella ragazza, potevo supporre che la sparizione di articoli casalinghi l'avrebbe turbata a tal punto da resettare il suo cervello per evitare che si surriscaldasse."


translate italian day4_std_morning_3f91116b:


    me "Beh, io vado…"


translate italian day4_std_morning_165f0267:


    mi "Ok, ciao! Vieni a trovarci… Voglio dire, vieni a trovarmi al circolo musicale, sono ancora da sola lì. Ma noi… Cioè io…"


translate italian day4_std_morning_d82a5e6d:


    "La voce di Miku è svanita poco a poco nella freschezza del mattino d'estate."


translate italian day4_std_morning_0c128c91:


    "Tornato alla stanza della leader, ho preso il mio telefono e ho controllato il livello della batteria."


translate italian day4_std_morning_2f9d1f0c:


    "Sarebbe bastato per un giorno, forse anche meno."


translate italian day4_std_morning_3d04d563:


    th "Di sicuro non mi servirà a molto qui, tuttavia…"


translate italian day4_std_morning_632d44dd:


    "Trovare un caricabatterie negli anni Ottanta significava doverlo inventare."


translate italian day4_std_morning_f6cc3af9:


    "Stavo per andare a colazione, quando qualcuno ha bussato alla porta."


translate italian day4_std_morning_482a0501:


    "Era Slavya."


translate italian day4_std_morning_47583a01:


    sl "Buongiorno.{w} Hai per caso visto Olga Dmitrievna?"


translate italian day4_std_morning_9e0939cf:


    "Ho iniziato a fissare il suo seno. L'altra sera mi aveva incantato davvero troppo."


translate italian day4_std_morning_2645a069:


    sl "Semyon?"


translate italian day4_std_morning_3f835829:


    "Slavya mi stava guardando preoccupata, ma ancora non riuscivo a distogliere il mio sguardo da quel punto."


translate italian day4_std_morning_a428f2b1:


    sl "C'è qualcosa che non va?"


translate italian day4_std_morning_a21b3e34:


    me "Al contrario, è tutto così…"


translate italian day4_std_morning_65e261bd:


    sl "Olga Dmitrievna...?"


translate italian day4_std_morning_535da2f1:


    me "Non lo so, non era qui quando mi sono svegliato."


translate italian day4_std_morning_1e0f3ed2:


    "Finalmente sono riuscito a controllarmi."


translate italian day4_std_morning_0d62f1e5:


    sl "Bene, ho ancora una cosa da fare, quindi ci vediamo a colazione."


translate italian day4_std_morning_145891de:


    "Slavya ha sorriso, mi ha salutato ed è corsa via."


translate italian day4_std_morning_f6cc3af9_1:


    "Stavo per andare a fare colazione, quando qualcuno ha bussato alla porta."


translate italian day4_std_morning_26171c75:


    "Era Lena."


translate italian day4_std_morning_5edd55c2:


    un "Ehmm… buongiorno."


translate italian day4_std_morning_70958877:


    me "Sì, buongiorno…"


translate italian day4_std_morning_dab051d6:


    "Sono rimasto un po' sorpreso."


translate italian day4_std_morning_6e7a684a:


    "Gli eventi della scorsa notte erano ancora freschi nella mia mente, ma non volevo parlarne."


translate italian day4_std_morning_ed2df426:


    me "Stai cercando Olga Dmitrievna, vero?"


translate italian day4_std_morning_736f44dd:


    un "Beh, no…{w} Voglio dire, sì…"


translate italian day4_std_morning_db521796:


    "Lena è tornata sé stessa – timida e imbarazzata."


translate italian day4_std_morning_c62b7542:


    "Quella danza al molo... adesso mi sembrava solo un sogno."


translate italian day4_std_morning_edd9b25a:


    me "Non è qui, mi dispiace..."


translate italian day4_std_morning_98757b47:


    un "Hmm… va bene allora…{w} io vado…"


translate italian day4_std_morning_751ac511:


    me "Ok…"


translate italian day4_std_morning_2dd7a526:


    "Dopo che Lena se n'è andata, ho pensato di essere stato troppo freddo con lei, così ho deciso che sarei stato più gentile la volta successiva che ci saremmo incontrati."


translate italian day4_std_morning_07104fa7:


    "Eppure, nonostante tutto, la mattina era brillante e stupenda."


translate italian day4_std_morning_5a3770b6:


    "Il sole splendeva sul campo «Sovyonok» sperduto nello spazio-tempo, scaldando i suoi residenti e ricaricandoli di energie da investire produttivamente nel corso della giornata."


translate italian day4_std_morning_08b429c2:


    "O, nel mio caso, di sprecarle in sforzi inutili in cerca di una spiegazione a tutto quello che stava accadendo qui."


translate italian day4_std_morning_eaabbc69:


    "Una folla insolitamente vasta si era formata davanti alla mensa."


translate italian day4_std_morning_2260fedb:


    "Naturalmente non c'era nessun altro luogo al Campo che interessasse ai pionieri più della mensa, ma perché la veranda era così affollata...?"


translate italian day4_std_morning_f5aef4ac:


    "Mi sono avvicinato per scoprire cosa stesse accadendo."


translate italian day4_fail_morning_bd4585a5:


    "Sono stato svegliato da una cricca infernale nella mia testa.{w} Era come se qualcuno stesse soffiando sul mio cranio dall'interno."


translate italian day4_fail_morning_fce68231:


    "Mi sono stropicciato gli occhi, ho sentito la sveglia suonare sul comodino, l'ho messa a tacere e mi sono rimesso a dormire."


translate italian day4_fail_morning_a20cefa7:


    "..."


translate italian day4_fail_morning_d941f014:


    "Quando mi sono svegliato era già un quarto alle nove."


translate italian day4_fail_morning_772587d6:


    th "Sembra che sia ora di alzarsi. Non posso perdermi la colazione."


translate italian day4_fail_morning_3b1d754c:


    "Mi sono alzato, mi sono stirato, e…{w} all'improvviso mi sono ricordato della sera precedente!"


translate italian day4_fail_morning_5ef37d77:


    "C'era un foglietto di carta sotto al cuscino:"


translate italian day4_fail_morning_470a435f:


    "«Sei qui per un motivo.»"


translate italian day4_fail_morning_2dc1af34:


    th "Ancora non capisco, cosa vorrà mai dire...?"


translate italian day4_fail_morning_50258f02:


    th "Come sono riuscito a mandare un messaggio a me stesso?"


translate italian day4_fail_morning_cdc1a9f5:


    th "Perché non ricordo di averlo fatto?"


translate italian day4_fail_morning_f2a17f0f:


    "Più domande che risposte.{w} Nessuna risposta, per la precisione."


translate italian day4_fail_morning_3d4304d6:


    "Ho abbandonato la stanza e ho osservato il Campo."


translate italian day4_fail_morning_25fa9b94:


    th "No, non sembra affatto un'illusione…"


translate italian day4_fail_morning_b5c3816d:


    "Ma quella nota... ha reso tutto più complicato."


translate italian day4_fail_morning_2b1800da:


    th "Di chi mi posso fidare?"


translate italian day4_fail_morning_5b2a5873:


    "Il mio stomaco ha brontolato a tradimento."


translate italian day4_fail_morning_4c70df13:


    th "Persino l'agente Mulder non svolgeva le sue indagini a stomaco vuoto."


translate italian day4_fail_morning_eaabbc69:


    "C'era una folla inaspettata attorno alla mensa."


translate italian day4_fail_morning_2260fedb:


    "Naturalmente non c'era nessun altro luogo nel Campo che interessasse ai pionieri più della mensa, ma perché la veranda era così affollata...?"


translate italian day4_fail_morning_f5aef4ac:


    "Mi sono avvicinato per capire cosa stesse accadendo."


translate italian day4_us_morning_0e224319:


    "Ho sentito il calore nel mio sogno."


translate italian day4_us_morning_62c15031:


    "A volte succede. Il cervello non è ancora completamente sveglio, ma riesci comunque a percepire il sole che colpisce i tuoi occhi, e quando ti svegli batti le ciglia per abituarti alla luce."


translate italian day4_us_morning_57bf0ff9:


    "Era una mattina stupenda.{w} Il canto degli uccellini, l'aria fresca e fragrante, il mondo immerso nella luce del giorno."


translate italian day4_us_morning_51613331:


    "Sarei rimasto volentieri a letto ancora per un po', ma… qualcosa non quadrava."


translate italian day4_us_morning_32a6b4a1:


    "Tutti gli eventi della sera precedente sono balenati nella mia mente."


translate italian day4_us_morning_be5ac746:


    "Storie spaventose e non troppo spaventose, Ulyana…"


translate italian day4_us_morning_a581c654:


    "Che se ne stava abbracciata a me russando. Non proprio quello che farebbe normalmente una ragazzina."


translate italian day4_us_morning_a84a89a7:


    "Ma la cosa non mi preoccupava affatto."


translate italian day4_us_morning_73427713:


    th "Dopotutto, è ancora presto. Di sicuro non sono ancora le sette o le otto."


translate italian day4_us_morning_bb3ada36:


    th "A chi verrebbe in mente di andare in biblioteca a quest'ora?"


translate italian day4_us_morning_5c2e2f4e:


    "Mi sono seduto e ho guardato fuori dalla finestra."


translate italian day4_us_morning_a3c34756:


    th "Tra pochi minuti il sole colpirà Ulyana dritto negli occhi."


translate italian day4_us_morning_7e2c6b03:


    me "Ed è allora che ti sveglierai!"


translate italian day4_us_morning_70e45333:


    th "Interessante, come fa a tenersi stretta a me così forte?"


translate italian day4_us_morning_a9947f93:


    th "Infatti non riesco proprio a liberarmi dalla sua presa…"


translate italian day4_us_morning_a26bbdc9:


    "In teoria, pensavo che saremmo rimasti sdraiati così ancora per una o due ore, ma improvvisamente ho sentito dei passi."


translate italian day4_us_morning_89be002b:


    th "Guai in arrivo!"


translate italian day4_us_morning_05c821d8:


    me "Svegliati! Mi hai sentito? Sveglia!"


translate italian day4_us_morning_e09dbdfe:


    "Ho iniziato a scuotere le spalle di Ulyana con cautela ma insistentemente, cercando di allentare la sua morsa, ma è stato tutto inutile."


translate italian day4_us_morning_5fe87263:


    "Nel frattempo i passi si stavano avvicinando."


translate italian day4_us_morning_ee87d584:


    th "Devo salvare me e Ulyana, ad ogni costo!"


translate italian day4_us_morning_fb5bb4b6:


    "Alzarsi in piedi sembrava impossibile – ormai l'avevo capito – quindi ho deciso di strisciare."


translate italian day4_us_morning_0178b537:


    "I miei movimenti sembravano proprio quelli di un addestramento militare (a cui non ho mai partecipato) – un soldato che trascina il suo comandante ferito sotto il fuoco nemico."


translate italian day4_us_morning_385cbf84:


    "Il comandante ha perso i sensi, il soldato è esausto, e strati di filo spinato li circondano…"


translate italian day4_us_morning_29669d4f:


    "Sono riuscito a malapena ad accovacciarmi dietro a un leggìo e a nascondere Ulyana quando la porta della biblioteca si è aperta."


translate italian day4_us_morning_d2052b63:


    "Zhenya stava in piedi sulla soglia."


translate italian day4_us_morning_c24e1c91:


    th "Sembra dedicare anima e corpo al suo lavoro, dato che è qui già alle prime luci dell'alba."


translate italian day4_us_morning_61db85c5:


    el "…aspetta!"


translate italian day4_us_morning_3417c9cb:


    "Ho sentito una voce familiare."


translate italian day4_us_morning_177ac4cc:


    th "Eh? Un visitatore? Quindi lei ha davvero un motivo per essere qui…"


translate italian day4_us_morning_ccf4620f:


    mz "Perché non potevi venire ieri sera? O anche oggi, più tardi?"


translate italian day4_us_morning_2dfa95a0:


    el "La scienza non può attendere."


translate italian day4_us_morning_229b38d1:


    mz "La scienza…"


translate italian day4_us_morning_49bd4154:


    "Era Electronik."


translate italian day4_us_morning_dfa36536:


    mz "Aspetta un secondo, lo troverò."


translate italian day4_us_morning_35e81a10:


    "Si è incamminata verso il punto dov'eravamo nascosti."


translate italian day4_us_morning_1a39bf90:


    "Sono riuscito a malapena a spostare Ulyana cosicché si trovasse sulla mia schiena, e l'ho trasportata a quattro zampe verso uno scaffale lì vicino, dove sono crollato cercando di riprendere fiato."


translate italian day4_us_morning_b01a480f:


    mz "Com'era? Cibernetica matematica...?{w} O matematica cibernetica...?"


translate italian day4_us_morning_5e51dcff:


    th "Sembra che lei non ci abbia visto."


translate italian day4_us_morning_266ebd21:


    el "Mi servirebbe qualcosa sul bombardamento di elettroni del fotovoltaico…"


translate italian day4_us_morning_dc097374:


    mz "Ma sei fuori? Credi che qui abbiamo qualcosa di militare?!"


translate italian day4_us_morning_6335428f:


    el "Non è affatto una cosa militare…"


translate italian day4_us_morning_e0071b38:


    "Per un po' di tempo entrambi sono rimasti in silenzio."


translate italian day4_us_morning_861c8f5c:


    el "Zhenya…"


translate italian day4_us_morning_6a6e65d4:


    mz "Cosa?"


translate italian day4_us_morning_ab9b10c0:


    el "Ti va di andare giù al fiume stasera…"


translate italian day4_us_morning_552a9040:


    mz "E perché?"


translate italian day4_us_morning_62e815fe:


    el "Beh…{w} Ecco…"


translate italian day4_us_morning_7f61d9af:


    mz "Ho molte cose da fare…{w} Forza, muoviti, il tuo robot ti sta aspettando."


translate italian day4_us_morning_3323f7c2:


    el "Ok…"


translate italian day4_us_morning_1e0523ce:


    "La porta che ha sbattuto è stata come un epitaffio per i turbamenti amorosi di Electronik."


translate italian day4_us_morning_67878bf4:


    th "Sembra che lui non sia un robot a sangue freddo, dopotutto."


translate italian day4_us_morning_7efc1a56:


    "Ho ridacchiato silenziosamente."


translate italian day4_us_morning_c59fe92e:


    "Comunque, non era il momento di ridere. Dovevamo fuggire dalla biblioteca in qualche modo."


translate italian day4_us_morning_89531da4:


    "Aspettare che Zhenya andasse alla mensa per la colazione sembrava la soluzione migliore per adesso."


translate italian day4_us_morning_395caa92:


    "Dopotutto lei era come un operaio comunista, non come uno dei robot di Electronik: doveva pur mangiare ogni tanto."


translate italian day4_us_morning_5672d57d:


    "Nel frattempo, Ulyana sembrava non avere alcuna intenzione di svegliarsi."


translate italian day4_us_morning_29b73a64:


    th "Almeno non sta più russando."


translate italian day4_us_morning_9a87b38e:


    "Nella biblioteca è piombato il silenzio."


translate italian day4_us_morning_928c4608:


    "Non riuscivo a vedere Zhenya, ma ero più o meno soddisfatto della situazione."


translate italian day4_us_morning_ba7c4e5e:


    "Tutto ad un tratto, dei rumori incomprensibili sono provenuti dalla scrivania: click, crack, e…{w} è partita una musica."


translate italian day4_us_morning_70db9b38:


    "L'inno sovietico?!"


translate italian day4_us_morning_ba1d06f5:


    th "Ma bene, semplicemente fantastico!"


translate italian day4_us_morning_c14cb5ff:


    "Sarebbe stato anche accettabile, se non fosse per il fatto che Zhenya ha iniziato a cantare!"


translate italian day4_us_morning_c94f8616:


    mz "Sicuro baluardo dell'amicizia fra i popoli…"


translate italian day4_us_morning_75088394:


    "Potevo solo invidiare i suoi sentimenti patriottici."


translate italian day4_us_morning_06f8fd05:


    "Purtroppo lei aveva dei problemi d'intonazione. Il mondo del pop sovietico sicuramente non sentiva la mancanza di una star chiamata Zhenya."


translate italian day4_us_morning_9877cbdf:


    "L'inno è terminato, e una voce sconosciuta ha iniziato a parlare dell'enorme successo di un piano di raccolta quinquennale."


translate italian day4_us_morning_26511e70:


    "Era una stazione radio, ovviamente."


translate italian day4_us_morning_2d3e165e:


    "Ho ascoltato con attenzione. Forse avrebbero detto qualcosa di interessante."


translate italian day4_us_morning_526eb40c:


    "Ma dopo l'elenco dei successi in campo agricolo, la voce ha iniziato ad affievolirsi, scomparendo del tutto nel giro di pochi minuti."


translate italian day4_us_morning_13ff876e:


    th "Probabilmente un disturbo del segnale…"


translate italian day4_us_morning_a0c04095:


    "Zhenya si è alzata e si è diretta verso di noi."


translate italian day4_us_morning_9e8818da:


    "La situazione è diventata critica."


translate italian day4_us_morning_356a32e8:


    "Sono riuscito a liberarmi dalla morsa di Ulyana con uno sforzo colossale."


translate italian day4_us_morning_079daa56:


    "Ero libero di andarmene, ma ero talmente arrugginito a causa di tutto quel tempo in cui sono rimasto in quella posizione, che non ho avuto la forza di alzarmi."


translate italian day4_us_morning_86aec307:


    th "È ora di prepararsi al peggio e iniziare a inventarsi delle scuse…"


translate italian day4_us_morning_a755654d:


    "Improvvisamente i passi si sono fermati."


translate italian day4_us_morning_d2163297:


    "Pare che Zhenya fosse dall'altra parte dello scaffale."


translate italian day4_us_morning_f120f183:


    "Ho sentito il fruscio delle pagine."


translate italian day4_us_morning_ce9a6b7b:


    th "Probabilmente sta cercando qualcosa."


translate italian day4_us_morning_9743d34f:


    "Ha preso un libro ed è tornata alla sua scrivania."


translate italian day4_us_morning_f06c5e02:


    "In quel momento la porta si è spalancata."


translate italian day4_us_morning_3cf16294:


    mt "Semyon?!{w} Per caso l'hai visto?"


translate italian day4_us_morning_5fd5cacf:


    "Olga Dmitrievna era decisamente senza fiato."


translate italian day4_us_morning_5419bf5d:


    mt "Ulyana?!"


translate italian day4_us_morning_ac7585a7:


    mz "No…"


translate italian day4_us_morning_80c6622f:


    "Ha risposto Zhenya, sorpresa."


translate italian day4_us_morning_519eac8f:


    "La porta ha sbattuto allo stesso modo con cui era stata aperta."


translate italian day4_us_morning_8adc70f4:


    "Sembrava che tutti ci stessero cercando, e presto anche Zhenya avrebbe cominciato a cercare qui dentro…"


translate italian day4_us_morning_2dc3e10c:


    "Ma fortunatamente in quel preciso istante la sirena della mensa ha iniziato a suonare, chiamando i pionieri per la colazione."


translate italian day4_us_morning_4d1c12f2:


    "Zhenya, essendo una persona puntuale, non ha esitato a lasciare la biblioteca in pochi minuti. Siamo rimasti solo io e Ulyana."


translate italian day4_us_morning_35353818:


    "Ed era giunto il momento di decidere cosa fare di lei…"


translate italian day4_us_morning_195ba3f0:


    "Senza più paura di essere scoperti, mi sono chinato e le ho gridato in un orecchio:"


translate italian day4_us_morning_e501c3ad:


    me "Ben svegliata, pane e marmellata!"


translate italian day4_us_morning_e9fe52a8:


    "Di colpo ha sobbalzato, cominciando a guardarsi intorno."


translate italian day4_us_morning_a94e04aa:


    "Nel vedermi, Ulyana ha spalancato gli occhi."


translate italian day4_us_morning_ba2cf72c:


    us "E tu che ci fai qui?"


translate italian day4_us_morning_571b4ce2:


    me "Sto giocando a nascondino."


translate italian day4_us_morning_ed803309:


    us "Eh?"


translate italian day4_us_morning_e29d2e60:


    me "Non importa… Hai dormito bene?"


translate italian day4_us_morning_41c2189c:


    us "Sì…"


translate italian day4_us_morning_feb4bc5f:


    "Sembrava che non si fosse ancora svegliata del tutto."


translate italian day4_us_morning_d2228502:


    me "Colazione?"


translate italian day4_us_morning_41c2189c_1:


    us "Sì…"


translate italian day4_us_morning_8bceae14:


    "Abbiamo lasciato la biblioteca."


translate italian day4_us_morning_d5d18ead:


    th "Finalmente!"


translate italian day4_us_morning_9eb55c9a:


    th "Adesso sono sicuro che non dovrò dare spiegazioni del come e del perché io e Ulyana abbiamo trascorso una notte intera lì dentro…"


translate italian day4_us_morning_71de8971:


    us "Scusa se mi sono addormentata in quel modo…"


translate italian day4_us_morning_6d36c28b:


    me "Non fa niente."


translate italian day4_us_morning_9b91052f:


    "Forse le mie parole sono sembrate poco sincere, perché mi ha guardato con incredulità."


translate italian day4_us_morning_4322ee3d:


    us "Aspetta un attimo…{w} Tu cosa ci facevi lì dentro per tutto quel tempo?"


translate italian day4_us_morning_75f4b174:


    me "Non ci crederesti…"


translate italian day4_us_morning_b515de5d:


    us "Aspetta un momento…{w} Questo significa che…"


translate italian day4_us_morning_4f6d2aa7:


    "Ulyana ha ridacchiato, si è allontanata da me e ha gridato forte:"


translate italian day4_us_morning_abd7e09d:


    us "Sono la prima per la colazione!"


translate italian day4_us_morning_74c13f1d:


    me "Niente di sorprendente, dopo un sonno così profondo…"


translate italian day4_us_morning_72667b52:


    "Ho detto, ma Ulyana ormai non poteva sentirmi, dato che era già lontana."


translate italian day4_us_morning_eaabbc69:


    "C'era una folla inaspettata davanti alla mensa."


translate italian day4_us_morning_2260fedb:


    "Naturalmente non c'era nessun altro luogo nel Campo che interessasse ai pionieri più della mensa, ma perché la veranda era così affollata...?"


translate italian day4_us_morning_f5aef4ac:


    "Mi sono avvicinato per scoprire cosa stesse accadendo."


translate italian day4_main1_3fe8cd89:


    "Pare che l'intero Campo si fosse riunito sulla veranda: c'erano tutte le ragazze che conoscevo, Olga Dmitrievna ed Electronik."


translate italian day4_main1_0c95f62a:


    "Era in corso una vivace discussione."


translate italian day4_main1_6785b764:


    "Mi sono avvicinato."


translate italian day4_main1_c74edda5:


    mt "Ah, Semyon!"


translate italian day4_main1_af775fc1:


    mt "Ma dove diavolo eri finito?!{w} Ti ho aspettato per tutta la notte e ti ho cercato sin dal primo mattino!{w} Ulyana mi ha detto che avevate lasciato la biblioteca insieme ieri sera."


translate italian day4_main1_9d5d157b:


    "Ho guardato Ulyana.{w} Stava sogghignando aspramente."


translate italian day4_main1_51149e68:


    me "…"


translate italian day4_main1_2f165ff9:


    mt "Beh, ce ne occuperemo più tardi!"


translate italian day4_main1_4c6e2dcd:


    mt "Hai per caso visto Shurik stamattina?"


translate italian day4_main1_3aabe659:


    me "No, qual è il problema?"


translate italian day4_main1_28e30b6d:


    mt "È già dal primo mattino che non riusciamo a trovarlo!"


translate italian day4_main1_70b53a62:


    th "Pionieri che spariscono – che novità."


translate italian day4_main1_547a39ce:


    mt "Ma ieri era con te?"


translate italian day4_main1_1e640e7b:


    "Si è rivolta ad Electronik."


translate italian day4_main1_8fb4fd1c:


    el "Sì…"


translate italian day4_main1_7c91c87b:


    mt "E stamattina ti sei svegliato e non l'hai più visto?"


translate italian day4_main1_62543f01:


    el "No…"


translate italian day4_main1_30b2a809:


    mt "Perché non sei venuto da me immediatamente?"


translate italian day4_main1_9f2b5808:


    el "Beh, credevo che si fosse svegliato presto per lavarsi, o altro…"


translate italian day4_main1_a5b6d3cb:


    sl "Ti aveva detto qualcosa ieri?"


translate italian day4_main1_f2c87db5:


    "È intervenuta Slavya."


translate italian day4_main1_c620bee1:


    el "Ad esempio?"


translate italian day4_main1_01cfc02e:


    sl "Che stava andando da qualche parte, per esempio?"


translate italian day4_main1_62543f01_1:


    el "No…"


translate italian day4_main1_cd7d9ef9:


    me "E cosa c'è di terribile in tutto questo?{w} Sono solo le nove del mattino. Potrebbe essere semplicemente andato a fare una passeggiata."


translate italian day4_main1_6059e846:


    sl "Tu non conosci Shurik."


translate italian day4_main1_453d16cb:


    "Mi ha guardato con espressione seria."


translate italian day4_main1_66a4843a:


    me "Beh no, non lo conosco…"


translate italian day4_main1_66774de2:


    "Ma non vedevo niente di sospetto in quella situazione."


translate italian day4_main1_fac4579f:


    mt "Va bene, niente panico. Lo troveremo!"


translate italian day4_main1_d68ecb78:


    us "Come potrebbe saltare la colazione?"


translate italian day4_main1_56aa99aa:


    "Ulyana ha sorriso."


translate italian day4_main1_3fab6262:


    dv "Esatto! È ora di mangiare."


translate italian day4_main1_a24b7420:


    "I pionieri sono entrati nella mensa."


translate italian day4_main1_7a51fac4:


    "Ma Olga Dmitrievna mi ha fermato."


translate italian day4_main1_29b74cfc:


    mt "E tu, Semyon, resta qui per favore."


translate italian day4_main1_e79d0fd5:


    me "Sì?"


translate italian day4_main1_d124d112:


    mt "Mi vorresti dire dove sei stato tutta la notte?"


translate italian day4_main1_f883183e:


    me "Beh…"


translate italian day4_main1_597e6c44:


    "Ecco una cosa a cui non avevo pensato…"


translate italian day4_main1_7a46a669:


    "Certo, la cosa non poteva di certo passare inosservata. Avrei potuto almeno pensare a qualche spiegazione logica."


translate italian day4_main1_538bd425:


    "Ma non l'ho fatto…"


translate italian day4_main1_8f6d4306:


    me "Beh, io…{w}Io e Ulyana stavamo sistemando i libri sugli scaffali. Ma poi lei mi ha chiuso dentro ed è scappata! Sono rimasto chiuso dentro fino al mattino!"


translate italian day4_main1_a1c3fbc7:


    mt "Sono stata alla biblioteca stamattina!{w} E non ti ho visto."


translate italian day4_main1_3f76b658:


    th "Sono abbastanza consapevole di ciò."


translate italian day4_main1_c0f84c22:


    me "Beh…{w} Me ne sono andato in silenzio."


translate italian day4_main1_41bc7385:


    mt "E perché Ulyana mi ha detto qualcosa di totalmente diverso?"


translate italian day4_main1_2ff7837a:


    th "Hmm, cosa potrebbe averle detto?"


translate italian day4_main1_2f2b2114:


    me "Sa che si inventa sempre certe storie!"


translate italian day4_main1_d27d2beb:


    mt "Sì, forse hai ragione…"


translate italian day4_main1_d4764288:


    "La leader si è fermata per un istante."


translate italian day4_main1_fe53b407:


    mt "Ma non pensare che io ti creda!"


translate italian day4_main1_f9dd954e:


    "Non lo pensavo…"


translate italian day4_main1_18c37203:


    mt "Ne riparleremo più tardi, non me lo dimenticherò!{w} Adesso è più importante ritrovare Shurik."


translate italian day4_main1_9513cd87:


    me "Già…"


translate italian day4_main1_7d563950:


    mt "Sei libero…"


translate italian day4_main1_af8e37ef:


    "Ancora una volta non avevo scelta di dove sedermi. L'unico posto libero era accanto ad Alisa e Ulyana."


translate italian day4_main1_ed08ccfd:


    us "Siediti."


translate italian day4_main1_ebb45b6a:


    "Mi ha indicato la sedia accanto a lei."


translate italian day4_main1_04d910fd:


    "Mi sono seduto."


translate italian day4_main1_1a2c041d:


    "Alisa sembrava la solita. Non sembrava volesse picchiarmi per il fatto che non fossi andato ad ascoltare la sua performance come le avevo promesso."


translate italian day4_main1_36580956:


    th "Beh, se non ne vuole parlare, perché dovrei farlo io?"


translate italian day4_main1_b7584acc:


    us "Non vai a prendere la colazione?"


translate italian day4_main1_97ba4a43:


    th "Buona idea!{w} Non ci avevo pensato…"


translate italian day4_main1_e2f039bb:


    "La colazione di oggi non era molto diversa da quella di ieri, ma sembrava più appetitosa."


translate italian day4_main1_ca4c8a18:


    "O forse avevo solo molta fame."


translate italian day4_main1_f0e3f8e3:


    "O forse volevo solo finire il prima possibile per evitare l'ennesimo scherzo di Ulyana."


translate italian day4_main1_15c24207:


    us "Vuoi venire alla spiaggia con noi oggi?"


translate italian day4_main1_18dfa619:


    me "Quando?"


translate italian day4_main1_516609bc:


    us "Appena dopo la colazione."


translate italian day4_main1_40ae9715:


    "In generale, rilassarsi non era nei miei piani oggi, ma se avevo intenzione di trascorrere il mio tempo pensando, perché non farlo prendendo il sole?"


translate italian day4_main1_c1286eed:


    me "Certo, perché no…"


translate italian day4_main1_a64a24e7:


    us "Ah, grandioso!"


translate italian day4_main1_b7bd4e35:


    "Ha sorriso gradevolmente."


translate italian day4_main1_2cb6f965:


    me "Scommetto che stai pianificando un altro dei tuoi scherzi?"


translate italian day4_main1_e4b476c4:


    us "No, per niente!"


translate italian day4_main1_dd0d143d:


    "Ulyana ha agitato le mani davanti a me."


translate italian day4_main1_6d3417f3:


    dv "Certo che ci sta pensando!"


translate italian day4_main1_08dcf315:


    "Alisa ha sorriso."


translate italian day4_main1_232ce05b:


    dv "Lei è sempre così!"


translate italian day4_main1_27304fd6:


    "Ho guardato Alisa con attenzione e mi è tornata in mente la sera precedente."


translate italian day4_main1_a2cb95d1:


    "Il suo strano comportamento, il nostro litigio."


translate italian day4_main1_35ddcb27:


    "Ma adesso è tornata quella di sempre."


translate italian day4_main1_c3ed033d:


    th "Forse non serve che le dica nulla a riguardo?"


translate italian day4_main1_c2e8c7a9:


    "Anche se non avevo intenzione di farlo."


translate italian day4_main1_b56982ca:


    "Alla fine, tutto quello che era accaduto la notte precedente ha smesso di sembrare inusuale, come se fosse stato tutto solo un sogno."


translate italian day4_main1_6c304510:


    us "Non è vero!"


translate italian day4_main1_1ca23a9b:


    me "Credo che Alisa abbia ragione."


translate italian day4_main1_53340723:


    us "Nessuno vuole credermi!"


translate italian day4_main1_d1036c99:


    "Ulyana ha preso il suo vassoio con i piatti sporchi e si è alzata."


translate italian day4_main1_0a9f12c1:


    us "Buon appetito!"


translate italian day4_main1_39b14315:


    "L'ha detto in un modo che non lasciava alcun dubbio – niente di buono mi avrebbe aspettato oggi alla spiaggia."


translate italian day4_main1_f86ec2e1:


    "Io e Alisa siamo rimasti da soli."


translate italian day4_main1_37176700:


    me "Sai, forse è meglio se non vengo con voi dopotutto."


translate italian day4_main1_2d5beca1:


    dv "E perché?"


translate italian day4_main1_8d93196b:


    me "Beh, avrei delle cose da fare…"


translate italian day4_main1_844c800d:


    dv "E cosa esattamente?"


translate italian day4_main1_d12f273c:


    "Mi ha guardato dritto negli occhi, e non sono riuscito a inventarmi una scusa decente."


translate italian day4_main1_6517fda2:


    me "Non ho nemmeno il costume da bagno…"


translate italian day4_main1_da8226ff:


    dv "Ti darò il mio."


translate italian day4_main1_e10064b1:


    me "Credi davvero che mi starebbe bene?"


translate italian day4_main1_8d7ab4dd:


    dv "Almeno provalo!"


translate italian day4_main1_1f304310:


    me "Non credo che ne valga la pena…"


translate italian day4_main1_3479d749:


    dv "Non fare il coniglio!{w} Ti troveremo un costume da bagno!"


translate italian day4_main1_54a8822f:


    "Quelle parole si sono aggiunte alle mie preoccupazioni."


translate italian day4_main1_0547c124:


    dv "Aspettami vicino alla mensa, torno subito."


translate italian day4_main1_505d91ae:


    me "Ok…"


translate italian day4_main1_850a4c6e:


    th "Non c'è niente di male nell'aspettarla, giusto?"


translate italian day4_main1_6488a41b:


    "Dopo aver finito la mia colazione, ho abbandonato la mensa e mi sono seduto sugli scalini."


translate italian day4_main1_52db742c:


    "I pionieri mi passavano accanto, uno dopo l'altro, impegnati nei loro affari."


translate italian day4_main1_6db745af:


    "Nessuno si è fermato.{w} Nessuno mi ha nemmeno guardato."


translate italian day4_main1_3594b82e:


    "Sembrava che non mi considerassero un estraneo, ma esattamente l'opposto – un adolescente come loro, un loro compagno, diciamo."


translate italian day4_main1_15995c83:


    "Mi sono accorto di aver iniziato a considerare questo Campo e i suoi abitanti con meno cautela rispetto al mio primo giorno qui."


translate italian day4_main1_750c0871:


    "Alisa è tornata dopo qualche minuto, come previsto."


translate italian day4_main1_1d6a18c3:


    dv "Sei pronto?"


translate italian day4_main1_eaccb1bc:


    me "Pronto per cosa?"


translate italian day4_main1_82448915:


    "Mi ha dato il costume da bagno…{w} Anche se era difficile chiamarlo costume da bagno…"


translate italian day4_main1_d7b10247:


    "Erano dei boxer di colore rosa, decorati con farfalle e fiori.{w} In effetti, {i}erano proprio{/i} dei boxer."


translate italian day4_main1_956c7f92:


    me "Forse mi pentirò di avertelo chiesto, ma dove hai preso {i}questi{/i}?"


translate italian day4_main1_6993170f:


    dv "Che c'è, hai paura di indossarli?"


translate italian day4_main1_009490c9:


    me "Beh, sai…{w} Non ho la minima intenzione di indossarli."


translate italian day4_main1_398d5637:


    "Ho apprezzato il suo scherzo, ma non volevo mettermi così in ridicolo."


translate italian day4_main1_f3133a95:


    dv "Mettiteli!"


translate italian day4_main1_8736d85c:


    "Mi ha detto in modo rude."


translate italian day4_main1_57346f05:


    me "Perché non li provi tu invece?{w} Credo che questo costume si abbini perfettamente al colore dei tuoi occhi!"


translate italian day4_main1_37b30d8c:


    dv "Facciamo una scommessa!"


translate italian day4_main1_6a537964:


    "Non avevo intenzione di accettare le sue scommesse."


translate italian day4_main1_1498c76e:


    me "No grazie."


translate italian day4_main1_b5ac4c3c:


    dv "D'accordo allora, o andrai alla spiaggia indossando questi, oppure nuoterai nudo!"


translate italian day4_main1_a6fdaa06:


    th "Considerando gli aspetti positivi e negativi delle due opzioni, la seconda sembra comunque migliore della prima."


translate italian day4_main1_1b8d0016:


    me "Non ho proprio intenzione di andare da nessuna parte!"


translate italian day4_main1_b43b3d6a:


    dv "Allora dirò a tutti che hai lasciato questi pantaloncini nella mia stanza!"


translate italian day4_main1_66ba2af9:


    me "E perché dovrei averlo fatto?"


translate italian day4_main1_37160fae:


    dv "Che ne so?"


translate italian day4_main1_42be2b6f:


    "È scoppiata a ridere."


translate italian day4_main1_9efe078e:


    "Non volevo litigare con Alisa, così alla fine ho deciso di andare alla spiaggia."


translate italian day4_main1_14c26ca5:


    "Ma non in quel ridicolo costume."


translate italian day4_main1_74372f49:


    me "Ok, vi raggiungo tra dieci minuti."


translate italian day4_main1_ea4299c9:


    dv "Non tardare!"


translate italian day4_main1_3dfe79f9:


    "Mi ha detto, correndo via."


translate italian day4_main1_77b838f6:


    "Mi sono trascinato verso la stanza della leader per prendere un asciugamano, sperando di trovare qualcosa di decente da indossare."


translate italian day4_main1_cdc7b7a7:


    "Olga Dmitrievna mi stava aspettando nella stanza."


translate italian day4_main1_044559b3:


    mt "Semyon, hai scoperto qualcosa riguardo a Shurik?"


translate italian day4_main1_d45464b9:


    me "Niente di nuovo…"


translate italian day4_main1_edefb213:


    "Mi sono avvicinato al mio letto e ho preso un asciugamano."


translate italian day4_main1_d024b380:


    mt "Stai andando da qualche parte?"


translate italian day4_main1_cb685aa8:


    me "Sì, alla spiaggia."


translate italian day4_main1_850934a0:


    mt "Aspetta. Ce l'hai un costume da bagno?{w} Se ricordo bene sei arrivato qui senza una valigia…"


translate italian day4_main1_82f7a16c:


    "Strano, il fatto che non avessi portato una valigia sembrava non averla sorpresa quando ero arrivato qui."


translate italian day4_main1_5cb822ee:


    me "No…"


translate italian day4_main1_f01befe8:


    mt "E allora cosa indosserai?"


translate italian day4_main1_9da88e5d:


    th "Bella domanda. Cosa?"


translate italian day4_main1_7479b07e:


    me "Non lo so…"


translate italian day4_main1_22a42eb2:


    mt "Aspetta un minuto."


translate italian day4_main1_81a73d46:


    "È andata verso l'armadio e ha aperto un cassetto chiuso a chiave."


translate italian day4_main1_b666fe0c:


    "Dopo un istante aveva già in mano un paio di normali pantaloncini da bagno neri."


translate italian day4_main1_21d21de6:


    th "E quelli da dove li ha presi?{w} O meglio – perché li teneva lì?"


translate italian day4_main1_e922975f:


    "Forse qualcuno della sessione precedente li aveva dimenticati…"


translate italian day4_main1_b2cd5380:


    "Considerando tutte le cose strane in questo Campo, non era poi così sorprendente trovare un costume da bagno maschile nella stanza di Olga Dmitrievna."


translate italian day4_main1_3066598f:


    me "Grazie."


translate italian day4_main1_86258ed4:


    "I pantaloncini erano proprio della misura giusta."


translate italian day4_main1_a20cefa7:


    "..."


translate italian day4_main1_742830e4:


    "Mi sono cambiato dietro la casetta e sono andato alla spiaggia."


translate italian day4_main1_cf92b9ce:


    "Quel luogo era già affollato di pionieri, ma ho riconosciuto solo Alisa e Ulyana."


translate italian day4_main1_170351a5:


    us "Vieni qui!"


translate italian day4_main1_a0ef15ef:


    "Mi sono avvicinato a loro e mi sono seduto sulla sabbia."


translate italian day4_main1_27f5a44d:


    dv "Vedo che hai trovato qualcosa di meglio…"


translate italian day4_main1_99336af1:


    "Ha fissato i miei pantaloncini e ha fatto un sorrisetto."


translate italian day4_main1_5e648c4f:


    me "Come puoi vedere..."


translate italian day4_main1_0eed83b2:


    us "Dai, andiamo a nuotare!"


translate italian day4_main1_d1aa6677:


    me "Non ho voglia.{w} Magari più tardi…"


translate italian day4_main1_d3a86de3:


    "Non mi piaceva nuotare."


translate italian day4_main1_ba1e125e:


    dv "Come vuoi."


translate italian day4_main1_a6eddd01:


    "Le ragazze sono entrate in acqua."


translate italian day4_main1_521b9f2c:


    th "Perché sono venuto qui?{w} Perché non sto cercando risposte...?"


translate italian day4_main1_a50d7f70:


    th "Dovrebbe forse interessarmi adesso?"


translate italian day4_main1_a482e6e3:


    "«Sovyonok» sembrava normale."


translate italian day4_main1_b9b33704:


    "Certo, un sacco di cose strane sono accadute in questi tre giorni e mezzo, ma prese singolarmente non erano affatto cose soprannaturali.{w} Soprattutto perché non avevo ancora trovato un qualcosa che potesse assomigliare anche vagamente a un indizio."


translate italian day4_main1_cc830959:


    "Al contrario, tutto ciò che era successo mi stava confondendo sempre più."


translate italian day4_main1_4795bad4:


    th "Ma che alternative posso avere?"


translate italian day4_main1_662e06d0:


    th "Non otterrò alcuna informazione dalla gente del posto.{w} Anche se dubito che sappiano qualcosa..."


translate italian day4_main1_4ed7892c:


    th "Forse dovrei provare ad andarmene da questo luogo?"


translate italian day4_main1_8000702a:


    th "Ma quanto lontano potrei camminare, considerando il fatto che non so neppure dove mi trovo?"


translate italian day4_main1_e75df2a1:


    th "Pare che l'unica cosa da fare sia aspettare."


translate italian day4_main1_4f43f435:


    "Dopo un po' le ragazze sono tornate.{w} Ulyana teneva qualcosa tra le mani."


translate italian day4_main1_30f4be0c:


    us "Guarda!"


translate italian day4_main1_06938095:


    "Ho portato lo sguardo verso l'alto e ho visto un crostaceo.{w} Un semplice crostaceo."


translate italian day4_main1_456ae98b:


    "Ulyana si è sdraiata accanto a me e ha cominciato a tormentarlo."


translate italian day4_main1_dd41af79:


    me "Lascia in pace quel povero animale!"


translate italian day4_main1_3b3d6cb9:


    us "Perché?!{w} È solo un crostaceo!"


translate italian day4_main1_c9e60657:


    me "E allora?{w} Anche lui ha diritto di vivere!"


translate italian day4_main1_ea38a367:


    us "Gli staccherò le chele e chiederò alla cuoca di cucinarlo per pranzo!"


translate italian day4_main1_68e2c448:


    me "Come se non avessimo altro da mangiare…"


translate italian day4_main1_4a330faf:


    "Ho guardato Alisa.{w} Sembrava essere totalmente disinteressata riguardo al divertimento di Ulyana con la povera bestiola."


translate italian day4_main1_411121e6:


    me "Diglielo!"


translate italian day4_main1_86658590:


    dv "Che c'è di male?{w} È un crostaceo, se lo merita!"


translate italian day4_main1_6a626905:


    "Sembrava che le due ragazze avessero saltato le lezioni sulla natura alla scuola elementare, e che non avessero alcuna empatia nei confronti dell'ambiente circostante…"


translate italian day4_main1_679b46e1:


    me "Dammelo!"


translate italian day4_main1_4ec958c5:


    "Ho strappato il crostaceo dalle mani di Ulyana."


translate italian day4_main1_4949cf90:


    us "Come vuoi…"


translate italian day4_main1_80d64677:


    "Ero un po' sorpreso del fatto che non avesse opposto resistenza."


translate italian day4_main1_56d514c5:


    "Ho guardato negli occhi la povera creatura."


translate italian day4_main1_c21fcd5d:


    "Quegli occhi sembravano non esprimere alcunché, ma ho pensato che se l'animale potesse parlare, si sarebbe certamente indignato, e si sarebbe rivolto alla Convenzione dell'ONU per i diritti umani."


translate italian day4_main1_70c26240:


    "A dire il vero non ero sicuro che gli sarebbe stato di molto aiuto..."


translate italian day4_main1_4595a707:


    "Ho riportato il crostaceo al fiume e l'ho liberato."


translate italian day4_main1_53896b88:


    us "Non importa, ne prenderò altri. Ce ne sono un sacco qui."


translate italian day4_main1_45927c46:


    "Ha detto Ulyana."


translate italian day4_main1_972f86d7:


    me "Non ne dubito…"


translate italian day4_main1_ca01a7fa:


    "Ho borbottato."


translate italian day4_main1_b18fe1e2:


    "Il tempo passava e mi è venuto sonno…"


translate italian day4_main1_357544c9:


    "Mi sono addormentato."


translate italian day4_main1_0c650a7c:


    "Non ricordo cosa stessi sognando, sempre che stessi sognando qualcosa, ma mi sono svegliato quando qualcuno mi ha toccato la spalla."


translate italian day4_main1_3c1fbed3:


    "Olga Dmitrievna stava davanti a me."


translate italian day4_main1_517d89d0:


    me "È venuta a nuotare anche lei?"


translate italian day4_main1_fd959960:


    "Ho chiesto, mezzo addormentato."


translate italian day4_main1_da0111bc:


    mt "No.{w} È quasi ora di pranzo e non siamo ancora riusciti a trovare Shurik."


translate italian day4_main1_82a4bab8:


    "Ha detto la leader, stando davanti a me in costume da bagno gocciolante..."


translate italian day4_main1_f95c9fc9:


    me "E?"


translate italian day4_main1_1df88814:


    mt "Voglio che tu vada a cercarlo."


translate italian day4_main1_7cc521ca:


    me "Pare che io sia l'unico pioniere in questo Campo."


translate italian day4_main1_5581e203:


    "Ero sinceramente indignato."


translate italian day4_main1_ccbda48f:


    "Ogni volta diventava sempre più chiaro che Olga Dmitrievna mi considerasse il suo fattorino, da sfruttare come schiavo.{w} O viceversa..."


translate italian day4_main1_89e79303:


    mt "Se te lo sto chiedendo è perché ho bisogno che tu mi dia una mano."


translate italian day4_main1_5aa2346a:


    "Perché proprio io?"


translate italian day4_main1_aad225f0:


    "Alla fine, dopo averci pensato un attimo, ho deciso di accettare."


translate italian day4_main1_2186b207:


    "Dopotutto mi ero ustionato le spalle dopo essermi addormentato sotto il sole, e cercare Shurik sarebbe stata l'occasione giusta per conoscere luoghi inesplorati in questo Campo."


translate italian day4_main1_505d91ae_1:


    me "Ok…"


translate italian day4_main1_b55d2be6:


    "Non sarebbe stato proprio carino andare in giro con i pantaloncini da bagno, quindi, in primo luogo, ho deciso di andare a cambiarmi."


translate italian day4_main1_a20cefa7_1:


    "..."


translate italian day4_main1_97b8a867:


    "Dieci minuti più tardi ero in piedi davanti alla casetta di Olga Dmitrievna, pensando a dove andare."


translate italian day4_busstop_84a6a4dc:


    "La fermata del bus sembrava essere una buona scelta."


translate italian day4_busstop_310157f5:


    "Mi è venuto in mente un pensiero strano – forse Shurik era nella mia stessa situazione, e ha deciso di fuggire da questo Campo, magari prendendo il bus 410."


translate italian day4_busstop_ae9e2dee:


    "Poteva davvero essere andata così, se anche lui fosse finito qui {i}per errore{/i}."


translate italian day4_busstop_2832326d:


    "Anche se la cosa era poco probabile."


translate italian day4_busstop_6d68af7d:


    th "Ma non si sa mai!"


translate italian day4_busstop_5a9376e0:


    th "E se il bus potesse davvero tornare…"


translate italian day4_busstop_794a9280:


    "Anche se era difficile da credere."


translate italian day4_busstop_d2b67202:


    "Ho deciso di aspettare qualche minuto alla fermata, per assicurarmi che non ci fosse traccia di Shurik o di chiunque altro, e poi sono tornato al Campo."


translate italian day4_busstop_3f3e9168:


    "Qualcuno è balzato oltre il cancello e si è scontrato con me.{w} L'impatto non è stato forte, quindi ho solo barcollato."


translate italian day4_busstop_548da2a4:


    "Miku stava davanti a me, massaggiandosi la fronte dolorante."


translate italian day4_busstop_37374d4c:


    me "Oh, scusa…"


translate italian day4_busstop_7459a96f:


    mi "Sto bene! È stata colpa mia! Stavo per andare al circolo musicale, ma mi sono distratta, stavo pensando a una nuova canzone… Sai, pensavo al testo e alle note. Non mi sono nemmeno accorta di essere finita qui."


translate italian day4_busstop_a9298d73:


    mi "Quindi non devi scusarti!"


translate italian day4_busstop_03f805f3:


    "Il suo tasso di parole-al-minuto ovviamente superava il limite massimo di elaborazione del mio cervello."


translate italian day4_busstop_513cdea5:


    "Ho tentato di ritirarmi frettolosamente."


translate italian day4_busstop_e170ad8d:


    me "Certo, certo… Devo andare… Sai…"


translate italian day4_busstop_206044ed:


    mi "Oh, aspetta!"


translate italian day4_busstop_6eecb271:


    "Come al solito ho cercato di andarmene abbandonando Miku e la sua cascata di parole, ma questa volta è riuscita ad afferrare il mio braccio."


translate italian day4_busstop_71a0a5fc:


    "La sua stretta mi ha dato i brividi, e mi stavo preparando a essere travolto dalla successiva ondata di parole sparate a raffica."


translate italian day4_busstop_14425216:


    mi "Mi potresti dare una mano, per favore? Solo un pochino-ino?"


translate italian day4_busstop_80eb41cf:


    "Non era certamente nella mia lista delle cose da fare oggi."


translate italian day4_busstop_d90e19df:


    me "Beh, mi piacerebbe, ma sai…"


translate italian day4_busstop_ffd83896:


    mi "Per favore!"


translate italian day4_busstop_74c53f6a:


    "Miku mi ha guardato con quella sua espressione di cucciolotto indifeso, e il mio cuore ha iniziato a sciogliersi."


translate italian day4_busstop_075057b4:


    "Di certo non mi avrebbe lasciato andare tanto facilmente."


translate italian day4_busstop_d6c51513:


    me "E che tipo di aiuto ti servirebbe esattamente?"


translate italian day4_busstop_855dcd0b:


    mi "Vorrei che mi aiutassi con la canzone! Non potrei farcela da sola! So cantare, so suonare. Ma non so fare entrambi contemporaneamente."


translate italian day4_busstop_4c33c840:


    th "Persino la nostra virtuosa ha i suoi punti deboli."


translate italian day4_busstop_4942eb5b:


    me "Beh, sai, non so suonare nessuno strumento…"


translate italian day4_busstop_9f6720fa:


    mi "Non importa! Ti farò vedere come si fa! Dai, andiamo!"


translate italian day4_busstop_4ccac507:


    th "Comunque, non sembra essere un grosso problema."


translate italian day4_busstop_e7714637:


    "Anche se..."


translate italian day4_busstop_e2956e63:


    me "Sai, mi è stato chiesto di cercare Shurik e cose simili..."


translate italian day4_busstop_48f85b22:


    "Miku mi ha guardato con frustrazione."


translate italian day4_busstop_2554171f:


    mi "Solo per un pochino!"


translate italian day4_busstop_0d2840a0:


    "Mi mancavano le parole..."


translate italian day4_busstop_be6c9d1e:


    "Mi ha trascinato con sé."


translate italian day4_busstop_59712563:


    th "Forse non ne verrà fuori nulla di buono, ma l'unico modo per liberarmi di lei è liberarmi della sua presa."


translate italian day4_busstop_0dd57599:


    "Ma non sarebbe stato molto carino."


translate italian day4_busstop_161f1f1a:


    th "Dopotutto, non può succedere niente di male!{w} Probabilmente…"


translate italian day4_busstop_82406965:


    "Dopo un minuto eravamo già dentro al circolo musicale."


translate italian day4_busstop_c75b785f:


    "Miku ha preso una chitarra."


translate italian day4_busstop_d8313998:


    mi "Ecco qua, guarda!"


translate italian day4_busstop_ddd216e1:


    "Si è seduta e ha iniziato a suonare."


translate italian day4_busstop_62735169:


    "Ho cercato di seguire i movimenti delle sue mani.{w} La melodia sembrava piuttosto semplice."


translate italian day4_busstop_074f6095:


    "Sembrava abbastanza facile da eseguire."


translate italian day4_busstop_d28a7a64:


    mi "Hai capito?"


translate italian day4_busstop_b4dbe06d:


    me "Più o meno."


translate italian day4_busstop_39da62e2:


    mi "Proviamo."


translate italian day4_busstop_4bfbb8ba:


    "Ho preso la chitarra e ho iniziato a suonare."


translate italian day4_busstop_b59ab969:


    "Non ha proprio funzionato…"


translate italian day4_busstop_3865d2b8:


    mi "Lascia che ti mostri ancora una volta come si fa."


translate italian day4_busstop_045ce299:


    "Lei suonava molto meglio di me."


translate italian day4_busstop_4db3b5a0:


    "Osservando Miku, ho pensato."


translate italian day4_busstop_ac84947f:


    th "Di certo è una chiacchierona, è ingenua e maldestra…{w} Ma ha davvero un grande talento per la musica."


translate italian day4_busstop_1e78833e:


    mi "Prova di nuovo."


translate italian day4_busstop_dfcc8234:


    "L'ho fatto molto meglio la seconda volta."


translate italian day4_busstop_763aacd2:


    mi "Wow, così va molto meglio!"


translate italian day4_busstop_d1046a49:


    "Ha sorriso."


translate italian day4_busstop_a0871d04:


    "Non era difficile, anzi – bastava ripetere le stesse note in continuazione."


translate italian day4_busstop_621a4f25:


    th "Ok, cerca di non andare fuori tempo!"


translate italian day4_busstop_20b61502:


    mi "Inizia al mio via!"


translate italian day4_busstop_35ec24e9:


    me "Ok…"


translate italian day4_busstop_697d9fca:


    mi "Pronto? Via!"


translate italian day4_busstop_52370984:


    "Era una canzone giapponese."


translate italian day4_busstop_f1a32904:


    "Francamente, non ho capito una singola parola, ma le doti vocali di Miku erano abbastanza buone.{w} Anzi, erano davvero magnifiche!"


translate italian day4_busstop_0f7f3a55:


    "Ha messo l'anima in ogni nota, in ogni parola."


translate italian day4_busstop_b6da10b3:


    th "Già, probabilmente la musica è la cosa a cui potrebbe dedicare la sua vita."


translate italian day4_busstop_f7383791:


    "Era come se non fosse lei ad aver scelto la musica, ma la musica ad aver scelto lei…"


translate italian day4_busstop_c487d162:


    "Nell'ultima mezz'ora ho visto una Miku totalmente diversa."


translate italian day4_busstop_a20cefa7:


    "..."


translate italian day4_busstop_128ef1ec:


    mi "Oh, grazie di cuore! Ti è piaciuta? Finalmente ci siamo riusciti, da sola non sarebbe stato così facile. Avrei continuato a sbagliare le parole o le note. Ma con te è stato perfetto! Ti ringrazio davvero."


translate italian day4_busstop_327d8e83:


    mi "Hai davvero del talento, sai?{w} Saper suonare in quel modo, senza alcuna preparazione…"


translate italian day4_busstop_5dbd8783:


    th "No, pare che io abbia cambiato la mia opinione su di lei troppo presto."


translate italian day4_busstop_5b2f2663:


    me "Grazie per la canzone!{w} Devo andare, ci vediamo!"


translate italian day4_busstop_df33f13a:


    mi "Grazie a te per…"


translate italian day4_busstop_f1561010:


    "Il resto della sua frase è rimasto dietro la porta."


translate italian day4_busstop_a9ebca9c:


    "Mi sono appoggiato alla parete dell'edificio, sospirando."


translate italian day4_busstop_ab0c1c58:


    "La canzone di Miku mi stava ancora girando nella testa."


translate italian day4_forest_cfbff117:


    "Mi pareva ovvio che Olga Dmitrievna e la sua squadra di pionieri avessero già esaminato ogni angolo del Campo."


translate italian day4_forest_5cc420a6:


    "Setacciandolo in lungo e in largo…"


translate italian day4_forest_305ab809:


    "Quindi probabilmente era inutile cercare Shurik alla mensa o alla spiaggia."


translate italian day4_forest_bbfd297a:


    "E nemmeno al circolo di cibernetica! Era la sua seconda casa.{w} O forse anche la prima."


translate italian day4_forest_b2a1e220:


    "Forse valeva la pena di dare un'occhiata intorno alla foresta circostante."


translate italian day4_forest_50fb2210:


    "Non avevo intenzione di addentrarmi troppo nella foresta. Altrimenti sarei stato io colui che avrebbero cercato l'indomani."


translate italian day4_forest_919b405e:


    "Nella mia vita ho visitato raramente le campagne."


translate italian day4_forest_a12c2f29:


    "Durante la mia infanzia ero solito alloggiare in una casa di campagna in estate.{w} Ma quella casa era molto vicina alla città."


translate italian day4_forest_a605f85e:


    "In questo Campo invece potevo trovare cose che non vedevo da molto tempo: fitta vegetazione, uccellini che cantavano, aria fresca."


translate italian day4_forest_a278fb75:


    "Ho trovato un prato e mi sono seduto su un ceppo."


translate italian day4_forest_6853b13e:


    th "Com'è tranquillo qui…"


translate italian day4_forest_0617ce76:


    th "Ma dove potrebbe essere Shurik?"


translate italian day4_forest_b4222e21:


    "Forse è stato portato via contro la sua volontà."


translate italian day4_forest_ef624150:


    "Questo Campo non era affatto normale, quindi le sparizioni di pionieri non dovevano essere una novità."


translate italian day4_forest_d487dd90:


    th "Una teoria intrigante – potrebbe essere stata opera della stessa forza paranormale che mi ha portato qui, o è solo opera di una qualche entità del luogo?"


translate italian day4_forest_25b671a8:


    "Immerso nei miei pensieri, non ho notato che l'erba davanti a me ha iniziato a muoversi."


translate italian day4_forest_4e2bb113:


    "Ho guardato più da vicino e ho visto uno scoiattolo."


translate italian day4_forest_fd1017fc:


    "Si è avvicinato a me con cautela e ha fissato le mie mani."


translate italian day4_forest_aa2cbac9:


    "Probabilmente era abituato a farsi dare da mangiare qui."


translate italian day4_forest_871d8c7f:


    me "Scusa, amico, non ho niente per te…"


translate italian day4_forest_94efe0a6:


    "Ovviamente lo scoiattolo non poteva capire le mie parole, e continuava a fissarmi immobile, in attesa."


translate italian day4_forest_5fbbd1e5:


    "Mi è dispiaciuto per lui, perché non avevo nemmeno una briciola di pane nelle mie tasche."


translate italian day4_forest_3b611457:


    "Mi sono reso conto che mi vergognavo persino di guardarlo negli occhi, così ho deciso di proseguire."


translate italian day4_forest_a20cefa7:


    "..."


translate italian day4_forest_97cf4b53:


    "Dopo un po' di tempo mi sono ritrovato ai lavabi."


translate italian day4_forest_8563a8ed:


    th "Quindi non è neppure nella foresta.{w} O almeno non nelle vicinanze."


translate italian day4_forest_977ea43c:


    "E addentrarmi oltre – sarebbe stato spaventoso."


translate italian day4_forest_150dfa24:


    "Mi sono avvicinato al rubinetto, mi sono tolto la camicia e ho iniziato a sciacquarmi, perché ero tutto sudato."


translate italian day4_forest_cf3e2df3:


    "Ma non è stato facile."


translate italian day4_forest_0fd38ed9:


    th "È un po' difficile versarsi l'acqua addosso senza una ciotola o un mestolo…"


translate italian day4_forest_9ff095ed:


    "Improvvisamente ho sentito dei passi provenire da dietro.{w} Mi sono girato."


translate italian day4_forest_fd05bbb8:


    "Electronik stava camminando verso di me."


translate italian day4_forest_051049d1:


    el "Stai cercando Shurik?"


translate italian day4_forest_16055c7f:


    me "Sì…{w} Anche tu?"


translate italian day4_forest_d1214932:


    el "Anch'io…"


translate italian day4_forest_959d6609:


    me "Senti, tu lo conosci meglio, dove credi che possa essere andato?"


translate italian day4_forest_1cbcc91e:


    el "Non ne ho la più pallida idea."


translate italian day4_forest_2e34b08a:


    "Mi ha risposto con tristezza."


translate italian day4_forest_d074bc24:


    me "Beh…{w} Non capisco perché si sia alzato un tale polverone. Durante la notte non era in stanza con te? Allora non può essere sparito da molto tempo…{w} Forse è andato a fare una passeggiata?"


translate italian day4_forest_5def083f:


    el "Tu non conosci Shurik!"


translate italian day4_forest_668b3318:


    "Ha detto Electronik, scaldandosi."


translate italian day4_forest_b25357aa:


    el "Lui dedica anima e corpo al suo lavoro!{w} La robotica e la cibernetica sono tutto per lui. Le persone come lui sono una su un milione! Anzi, una su un miliardo! Il suo talento è sconfinato! Io mi preoccupo per lui! È un uomo d'acciaio!"


translate italian day4_forest_503848df:


    el "È semplicemente un genio!"


translate italian day4_forest_6b3cf294:


    "In quel momento sembrava Hitler, che dava un discorso davanti a una folla."


translate italian day4_forest_fe5e3be9:


    "Persino i suoi gesti erano simili."


translate italian day4_forest_ccf562b1:


    me "Ok…{w} E allora?"


translate italian day4_forest_b4f43170:


    el "«E allora»? Non capisci proprio, vero?"


translate italian day4_forest_5cb822ee:


    me "No…"


translate italian day4_forest_37697e5c:


    el "Dedica tutto sé stesso, capisci? Dedica tutto il suo tempo libero al nostro circolo! TUTTO!"


translate italian day4_forest_2c080d1a:


    me "Quindi, sparire in questo modo è insolito da parte sua?"


translate italian day4_forest_acffd441:


    el "Certo che sì!"


translate italian day4_forest_2ce86487:


    "Electronik pareva essersi calmato un po'."


translate italian day4_forest_505d91ae:


    me "Ok…"


translate italian day4_forest_2c5335a6:


    "Mi ha guardato intensamente."


translate italian day4_forest_3aee18b1:


    el "Hai intenzione di lavarti?"


translate italian day4_forest_b37f1cb4:


    me "Non proprio…{w} Volevo solo rinfrescarmi un po'. Fa caldo."


translate italian day4_forest_4fa820b9:


    el "Anch'io."


translate italian day4_forest_2771ee33:


    "Si è guardato attorno."


translate italian day4_forest_459c94ac:


    el "Speravo ci fosse un secchio o un mestolo. Qualcosa per raccogliere l'acqua."


translate italian day4_forest_1403f59c:


    "Si, anch'io ho notato che non c'è niente."


translate italian day4_forest_01eb5767:


    el "Allora facciamolo…"


translate italian day4_forest_fe274fdb:


    "È andato dietro a uno dei rubinetti e l'ha tirato su."


translate italian day4_forest_6341f283:


    "Con mia sorpresa, lo schizzo d'acqua si è diretto verso l'alto di circa novanta gradi, invece di scorrere verso il basso."


translate italian day4_forest_5dcfd900:


    "Era possibile lavarsi in quel modo."


translate italian day4_forest_0d1796ca:


    "Nel frattempo Electronik si è sbottonato la camicia, se l'è tolta e si è accovacciato, come se si stesse tirando giù i pantaloncini."


translate italian day4_forest_746f00c8:


    "Non potevo dirlo con certezza, dato che era coperto dai lavabi dalla vita in giù."


translate italian day4_forest_f589c698:


    "Ha diretto il flusso d'acqua verso di sé e ha iniziato a canticchiare tranquillamente."


translate italian day4_forest_aabf34aa:


    el "Puliamo il mio camìn, spazzacamìn…"


translate italian day4_forest_76b7903e:


    "Sono rimasto senza parole, non sapendo cosa fare."


translate italian day4_forest_a7e62300:


    "Sembrava che lui l'avesse notato."


translate italian day4_forest_b0d91aa4:


    el "Appena ho finito te lo lascio usare."


translate italian day4_forest_01fbaabb:


    "Oddio, che brividi!"


translate italian day4_forest_4c762e8b:


    me "Beh, sai…{w} Mi sono appena ricordato che devo fare una cosa! Devo scappare!"


translate italian day4_forest_c6ce9432:


    "Di tutte le eccentricità di Electronik, quella è stata decisamente la più strana."


translate italian day4_forest_b530b3f7:


    el "Ehi! Che ti succede?{w} Una doccia fredda in un giorno così caldo è la cosa migliore di sempre!"


translate italian day4_forest_9308b010:


    me "No-no-no!{w} Non credo proprio!{w} E comunque, devo andare."


translate italian day4_forest_b9ecea82:


    "Mi sono rimesso la camicia di fretta e sono tornato nella foresta."


translate italian day4_forest_eedd04d7:


    th "Ma che gli è preso?"


translate italian day4_house_of_mt_ed200de8:


    th "Ma è proprio un'idea stupida."


translate italian day4_house_of_mt_3cdc1097:


    "Se Shurik si stesse nascondendo da qualche parte nel Campo, l'avrebbero già trovato a quest'ora (ovviamente, supponendo che volesse farsi trovare)."


translate italian day4_house_of_mt_90c7afe2:


    th "Quindi non credo proprio di poterli aiutare."


translate italian day4_house_of_mt_afd17b4a:


    "Con quel pensiero in testa sono entrato nella mia stanza e sono crollato sul letto."


translate italian day4_house_of_mt_f20ad58c:


    th "Sarebbe un problema se Olga Dmitrievna mi trovasse qui dentro..."


translate italian day4_house_of_mt_d712f4f6:


    th "Basta... smettila! Sono forse un animaletto tremante, oppure ho anch'io i miei diritti?"


translate italian day4_house_of_mt_befe5d34:


    "Non avevo voglia di fare niente in ogni caso."


translate italian day4_house_of_mt_2300b407:


    "Faceva caldo come gli altri giorni, e l'unica cosa da fare era restarsene sdraiato sul letto ad aspettare il pranzo."


translate italian day4_house_of_mt_43e095ca:


    "Stavo per addormentarmi, quando qualcuno ha bussato alla porta."


translate italian day4_house_of_mt_48555f41:


    me "Avanti."


translate italian day4_house_of_mt_482a0501:


    "Slavya è apparsa sulla soglia."


translate italian day4_house_of_mt_df14250c:


    sl "Olga Dmitrievna non è qui?"


translate italian day4_house_of_mt_5a7a7577:


    me "No."


translate italian day4_house_of_mt_63863ec8:


    sl "E tu che stai facendo?"


translate italian day4_house_of_mt_1c3f46c0:


    "Mi ha chiesto sospettosa."


translate italian day4_house_of_mt_a57d59c9:


    "Mi sono guardato, sdraiato sul letto, e le ho detto:"


translate italian day4_house_of_mt_15d05c86:


    me "Sto sdraiato…"


translate italian day4_house_of_mt_f8b93764:


    sl "Lo vedo.{w} Ma ho sentito che Olga Dmitrievna ti aveva chiesto di cercare Shurik."


translate italian day4_house_of_mt_ff471f9a:


    me "Beh, sì…"


translate italian day4_house_of_mt_e26abf96:


    sl "E?"


translate italian day4_house_of_mt_6715e0fb:


    me "Che senso ha?{w} Sono sicuro che lei ha già setacciato l'intero Campo."


translate italian day4_house_of_mt_e06cc00e:


    me "Non è passato molto tempo.{w} Perché dovremmo farci prendere dal panico?"


translate italian day4_house_of_mt_04fc551f:


    sl "Sai, potrebbe essere successo qualcosa…"


translate italian day4_house_of_mt_edee7f76:


    "Slavya mi ha guardato pensierosa."


translate italian day4_house_of_mt_b68d7b34:


    sl "Alzati."


translate italian day4_house_of_mt_34c3a4c4:


    me "Devo proprio?"


translate italian day4_house_of_mt_2551395c:


    "Ero così esausto che il solo pensiero di dover andare da qualche parte mi spaventava."


translate italian day4_house_of_mt_2bb8d573:


    sl "Sì, devi proprio!"


translate italian day4_house_of_mt_90cc9542:


    "Slavya non è una persona alla quale poter dire di no."


translate italian day4_house_of_mt_6cdf61dd:


    "Mi sono alzato con riluttanza e l'ho seguita fuori dalla stanza."


translate italian day4_house_of_mt_5aaa09bd:


    "Siamo rimasti davanti alla porta per un po', rilassandoci sotto i raggi del sole d'estate.{w} Anche se io stavo friggendo…"


translate italian day4_house_of_mt_fca62f46:


    me "Dove andiamo?"


translate italian day4_house_of_mt_276df626:


    sl "Dobbiamo cercare dappertutto."


translate italian day4_house_of_mt_57a26992:


    th "Ottima idea… Davvero geniale!"


translate italian day4_house_of_mt_018386dd:


    "La nostra prima tappa era la biblioteca."


translate italian day4_house_of_mt_42ea0267:


    "Slavya è andata da Zhenya che stava seduta alla scrivania, e le ha parlato."


translate italian day4_house_of_mt_188600a4:


    "Io sono rimasto accanto alla porta. Non volevo comunicare con quella bibliotecaria più del necessario."


translate italian day4_house_of_mt_eccb8e77:


    "Pochi minuti più tardi Slavya è tornata da me."


translate italian day4_house_of_mt_48013d82:


    me "Allora?"


translate italian day4_house_of_mt_37eba565:


    sl "Niente…"


translate italian day4_house_of_mt_4c3d69ef:


    "Ha scosso la testa."


translate italian day4_house_of_mt_e93ab54f:


    th "Come previsto."


translate italian day4_house_of_mt_a20cefa7:


    "..."


translate italian day4_house_of_mt_79e8cb2c:


    "La mensa.{w} C'era ancora un po' di tempo prima del pranzo, e così non si era ancora formata la solita folla di pionieri lì attorno."


translate italian day4_house_of_mt_e2de9d7b:


    "La mensa era deserta sia fuori che dentro."


translate italian day4_house_of_mt_84df38db:


    "Mentre Slavya parlava con i cuochi io stavo seduto a giocherellare con il dosa-sale."


translate italian day4_house_of_mt_ae189afc:


    "Un po' di sale si è rovesciato sul tavolo."


translate italian day4_house_of_mt_f8a8a439:


    "Se fossi superstizioso mi sarei preoccupato della cosa…"


translate italian day4_house_of_mt_31901972:


    "Stranamente, Shurik non era neppure qui."


translate italian day4_house_of_mt_387e3a7a:


    "Sarebbe stata una bella sorpresa se lei l'avesse trovato, ad esempio, dentro al frigorifero…"


translate italian day4_house_of_mt_a20cefa7_1:


    "..."


translate italian day4_house_of_mt_d5c5547a:


    "La nostra prossima tappa era l'infermeria."


translate italian day4_house_of_mt_60f69217:


    "Ho deciso di non entrare e di aspettare Slavya fuori."


translate italian day4_house_of_mt_6b61c0c5:


    "Nessuna novità."


translate italian day4_house_of_mt_c08b785a:


    "Alcuni pionieri stavano giocando a calcio al campo sportivo."


translate italian day4_house_of_mt_b4ae2b53:


    "Sarebbe stato difficile per Shurik perdersi in mezzo a loro."


translate italian day4_house_of_mt_b2c08c50:


    "Finalmente siamo arrivati alla sede dei circoli."


translate italian day4_house_of_mt_38b1dea9:


    me "Credi che possa trovarsi qui? Secondo me è il primo luogo in cui l'hanno cercato…"


translate italian day4_house_of_mt_7827a8db:


    sl "Entriamo."


translate italian day4_house_of_mt_40d2a09a:


    "Dentro non c'era nessuno."


translate italian day4_house_of_mt_2836519c:


    "Slavya ha aperto la porta ed è entrata nella stanza accanto."


translate italian day4_house_of_mt_f73e0745:


    "L'ho seguita."


translate italian day4_house_of_mt_0ba05a1b:


    "Tutto questo mi sembrava un po' stupido."


translate italian day4_house_of_mt_a967e5b1:


    "Era strano, specialmente perché era un'idea di Slavya."


translate italian day4_house_of_mt_9b88aabc:


    th "Voglio dire, capisco, responsabilità e simili…{w} Ma non è ovvio che lui non sia qui nel Campo?"


translate italian day4_house_of_mt_13a0360f:


    th "Dopotutto, non sta giocando a nascondino, vero?"


translate italian day4_house_of_mt_c1888233:


    sl "Non c'è nessuno nemmeno qui…"


translate italian day4_house_of_mt_6e164047:


    me "Che ti aspettavi?{w} Di trovarlo seduto dentro a un armadio?"


translate italian day4_house_of_mt_dfc5030f:


    sl "Beh!"


translate italian day4_house_of_mt_6e0f81b2:


    "Sembrava che la mia domanda l'avesse offesa."


translate italian day4_house_of_mt_bc695e77:


    me "Scusa, scusa…{w} Ma seriamente!"


translate italian day4_house_of_mt_d2e028e4:


    sl "Ti capisco…{w} Ma non possiamo trascurare alcuna possibilità."


translate italian day4_house_of_mt_1a2d3a52:


    me "Ok, senti, sinceramente, tu cosa ne pensi?"


translate italian day4_house_of_mt_1829f925:


    sl "Riguardo alla scomparsa di Shurik?"


translate italian day4_house_of_mt_fc30fd2b:


    me "Sì."


translate italian day4_house_of_mt_68504b39:


    sl "Forse si è addentrato nella foresta ed è stato rapito da uno spirito."


translate italian day4_house_of_mt_ec2c2a7c:


    "Ha detto ridendo."


translate italian day4_house_of_mt_579bb939:


    me "Che storia fantasiosa…"


translate italian day4_house_of_mt_adb6e4a6:


    th "Tuttavia, non si sa mai, in questo Campo."


translate italian day4_house_of_mt_920eda59:


    sl "Già…{w} Comunque, non è il momento giusto per scherzare!"


translate italian day4_house_of_mt_d7a9b9f5:


    me "Su col morale!{w} Lo ritroveremo!"


translate italian day4_house_of_mt_4f31da71:


    sl "Lo spero…"


translate italian day4_house_of_mt_c17a722d:


    "Slavya ha sorriso."


translate italian day4_house_of_mt_551b3c25:


    sl "Bene, io ho ancora delle cose da fare."


translate italian day4_house_of_mt_038a986f:


    me "Ci vediamo più tardi."


translate italian day4_house_of_mt_432f59be:


    "Se n'è andata, ma io sono rimasto lì ancora per un po', osservando l'armadio del circolo cibernetico."


translate italian day4_house_of_mt_a20cefa7_2:


    "..."


translate italian day4_house_of_mt_c6af67a5:


    "Ero così ispirato dalle azioni di Slavya che ho deciso di continuare a cercare da qualche altra parte."


translate italian day4_boathouse_9797429b:


    th "Forse Shurik ha deciso di raccogliere i sassolini vicino all'acqua."


translate italian day4_boathouse_140891ff:


    th "Nel peggiore dei casi troverò il suo cadavere…"


translate italian day4_boathouse_639b643a:


    th "Ma sono sicuro che non andrà così!"


translate italian day4_boathouse_ea7057f1:


    "Stavo attraversando la piazza quando qualcuno mi ha chiamato."


translate italian day4_boathouse_418a6303:


    dv "Aspetta!"


translate italian day4_boathouse_3900d745:


    "Alisa si è avvicinata e ha sorriso.{w} Ho subito percepito una trappola."


translate italian day4_boathouse_2f227a4f:


    dv "Dove stai andando?"


translate italian day4_boathouse_d6059f36:


    me "Sto cercando Shurik…{w} Me l'ha chiesto Olga Dmitrievna."


translate italian day4_boathouse_ce4ee092:


    dv "E com'è? Eccitante?"


translate italian day4_boathouse_9d114d2d:


    "Mi ha guardato intensamente negli occhi, e io le ho dato le spalle goffamente."


translate italian day4_boathouse_cba9cbf0:


    me "Non proprio…{w} Ma sai, un pioniere è scomparso."


translate italian day4_boathouse_36e1c3b7:


    dv "Non ti starai preoccupando per una cosa così insignificante?"


translate italian day4_boathouse_cb23542f:


    me "Cosa intendi dire?"


translate italian day4_boathouse_b0848313:


    dv "Sono passate solo poche ore.{w} Forse ha solo perso la cognizione del tempo mentre stava facendo una passeggiata…"


translate italian day4_boathouse_da12543c:


    "Ho avuto la sua stessa idea, ma non volevo che lo sapesse."


translate italian day4_boathouse_f7f0bc77:


    me "Sì, è vero.{w} Ma chissà cosa potrebbe essere successo…"


translate italian day4_boathouse_64f4e64e:


    dv "Lascia che ti aiuti!"


translate italian day4_boathouse_16a32560:


    me "Mmm… Con cosa?"


translate italian day4_boathouse_a3e48491:


    "Mi sono insospettito."


translate italian day4_boathouse_01b2df0c:


    dv "A cercare Shurik!"


translate italian day4_boathouse_beae491d:


    me "Oh, ma posso farlo…"


translate italian day4_boathouse_55688a4e:


    dv "E dai!"


translate italian day4_boathouse_f0fb482a:


    "Mi ha sorriso, ma c'era qualcosa di malevolo in quell'espressione."


translate italian day4_boathouse_cd88bad5:


    "Naturalmente il suo sorriso era piuttosto carino, ma mi sembrava davvero che nascondesse qualcosa."


translate italian day4_boathouse_b88b8d82:


    me "Beh, se proprio insisti…"


translate italian day4_boathouse_4e8dafb2:


    "Dato che non sapevo con certezza cosa avesse in mente, non c'era ragione di rifiutare la sua proposta."


translate italian day4_boathouse_b5f16424:


    dv "Ma prima di andare devo prendere una cosa dalla mia stanza."


translate italian day4_boathouse_12c4ee4b:


    me "Ok, allora ti aspetto."


translate italian day4_boathouse_62de1363:


    dv "Hai davvero intenzione di restare qui in piedi come un fesso?{w} Vieni anche tu!"


translate italian day4_boathouse_505d91ae:


    me "Ok…"


translate italian day4_boathouse_a2980b83:


    "Siamo arrivati alla casetta di Alisa."


translate italian day4_boathouse_7aa63adb:


    "Era come tutte le altre, tranne per un Jolly Roger pirata attaccato alla porta."


translate italian day4_boathouse_e4591d9e:


    dv "Comunque, Ulyana è la mia compagna di stanza."


translate italian day4_boathouse_53a56040:


    me "Ah."


translate italian day4_boathouse_36881cd4:


    "Siamo entrati."


translate italian day4_boathouse_983dd462:


    "Il caos totale che regnava in quella stanza mi ha ricordato il mio vecchio appartamento."


translate italian day4_boathouse_0d66d2a4:


    th "Chissà perché lo considero «vecchio»…"


translate italian day4_boathouse_225e52c0:


    "A dire il vero, mi immaginavo la stanza di una ragazza in modo diverso: lenzuola candide, pareti, pavimento e soffitto splendenti, senza nemmeno un granello di polvere."


translate italian day4_boathouse_cd30d970:


    th "Ma considerando che i due «pionieri modello» del Campo vivono insieme in questa stanza…"


translate italian day4_boathouse_dab5031c:


    "Siamo rimasti in silenzio per un po' di tempo."


translate italian day4_boathouse_f744c43e:


    me "E cosa dovevi prendere?"


translate italian day4_boathouse_765cf23a:


    dv "Eh?"


translate italian day4_boathouse_fe1ee079:


    "Sembrava che avessi interrotto i pensieri di Alisa."


translate italian day4_boathouse_72b1039b:


    dv "Già…{w} Adesso che ci penso non è qui…{w} Aspetta un attimo, torno subito."


translate italian day4_boathouse_0049a2fb:


    "Ha sorriso ed è corsa fuori."


translate italian day4_boathouse_ebc67191:


    th "Sembra che lei abbia la testa fra le nuvole oggi."


translate italian day4_boathouse_7fb057e3:


    "Ho iniziato a esaminare la stanza."


translate italian day4_boathouse_5dfa3fd6:


    "Non direi che qui ci sia stata un'esplosione, era piuttosto un bel casino."


translate italian day4_boathouse_49172fa3:


    th "A giudicare da tutto questo disastro, generato in una sola sessione, posso dire di non essere la persona più disordinata del mondo."


translate italian day4_boathouse_b3f9ad2c:


    "Le decorazioni di disordine nel mio appartamento hanno richiesto anni per essere completate!"


translate italian day4_boathouse_1f000f06:


    "Non stavo pensando a nulla in particolare, mi stavo solo guardando attorno."


translate italian day4_boathouse_76afe60b:


    "Poster di artisti sovietici, dei libri sugli scaffali, vari oggetti casalinghi…"


translate italian day4_boathouse_76d3f20d:


    "E poi mi sono reso conto che era tutto sbagliato!"


translate italian day4_boathouse_8a607983:


    th "Qualcosa sta per accadere!{w} Qualcosa di molto spiacevole!"


translate italian day4_boathouse_c1ef0499:


    th "Perché Alisa mi ha abbandonato qui tutto solo?"


translate italian day4_boathouse_3eb661ac:


    "C'era qualcosa che dovevo scoprire."


translate italian day4_boathouse_9e3a46df:


    "Il mio sesto senso mi ha suggerito che era pericoloso rimanere qui dentro."


translate italian day4_boathouse_f00b42b5:


    "Sono andato verso la porta e ho provato ad aprirla."


translate italian day4_boathouse_531d7dd3:


    "Chiusa a chiave!"


translate italian day4_boathouse_f67a6003:


    th "Ma che sorpresa!{w} Com'è riuscita a chiudermi dentro senza che io mi accorgessi di niente?"


translate italian day4_boathouse_de1fdea1:


    th "Non so cosa abbia in mente, ma devo andarmene da qui!"


translate italian day4_boathouse_dee27299:


    "Mi sono avvicinato alla finestra, l'ho aperta e sono uscito."


translate italian day4_boathouse_0ed51470:


    th "Ora potrei andarmene e farmi gli affari miei, oppure…"


translate italian day4_boathouse_b69f8c48:


    "Ho scelto la seconda opzione e ho aspettato."


translate italian day4_boathouse_e924afbe:


    th "Per quale motivo mi ha chiuso nella sua stanza?"


translate italian day4_boathouse_7b372665:


    "Volevo sapere cosa stesse accadendo, e in più volevo vedere la faccia di Alisa quando avrebbe scoperto che non ero più nella sua stanza."


translate italian day4_boathouse_4c2291c3:


    "Mi sono nascosto fra i cespugli vicino alla casetta e ho atteso."


translate italian day4_boathouse_7eae97a5:


    "Dopo un po' di tempo ho sentito dei passi."


translate italian day4_boathouse_009f6978:


    "Alisa e Olga Dmitrievna si stavano dirigendo verso la casetta."


translate italian day4_boathouse_f9acd723:


    dv "Vedrà tutto con i suoi occhi!"


translate italian day4_boathouse_bc262665:


    "Ha aperto la porta ed è entrata…"


translate italian day4_boathouse_cc2a5873:


    "Pochi secondi dopo è corsa fuori."


translate italian day4_boathouse_813cd9cc:


    dv "Beh sa, io…"


translate italian day4_boathouse_8b502027:


    "Alisa sembrava come se avesse appena vinto la gara di acchiappa-il-topo, ma all'ultimo momento avesse scoperto che non si trattava di un vero sport Olimpico."


translate italian day4_boathouse_4195d1ac:


    mt "Quindi è svanito nell'aria?"


translate italian day4_boathouse_962ac463:


    "Le ha chiesto Olga Dmitrievna, con scetticismo."


translate italian day4_boathouse_5460bb59:


    th "Ho fatto bene a chiudere la finestra dopo essere uscito!"


translate italian day4_boathouse_2178d069:


    dv "Oh no…{w} Sa…"


translate italian day4_boathouse_3f9c9a9a:


    mt "Capisco perfettamente, Dvachevskaya!"


translate italian day4_boathouse_fdf3425b:


    mt "Tu e la tua compagna – ogni volta la stessa storia! Non vi sembra di esagerare un po'?!"


translate italian day4_boathouse_38429b64:


    dv "Ma è la verità...!"


translate italian day4_boathouse_b29a5db5:


    mt "Davvero? Una storia dopo l'altra. Semyon nella tua stanza, Semyon che ti molesta. Perché continui a mentire? È una cosa orribile."


translate italian day4_boathouse_a48fe9fa:


    "Alisa sembrava davvero sconvolta e, con mia sorpresa, non stava cercando di discutere."


translate italian day4_boathouse_81dede20:


    "Da una parte era divertente vederla così, ma dall'altra – un po' mi dispiaceva per Dvachevskaya."


translate italian day4_boathouse_0e804b76:


    "Comunque se l'era cercata..."


translate italian day4_boathouse_4d1f6fb4:


    "La nostra leader finalmente ha terminato la sua ramanzina e se n'è andata."


translate italian day4_boathouse_2ef1d6a7:


    "Alisa era furiosa.{w} Ha stretto i pugni tremando, il suo viso era rosso dalla rabbia, sembrava stesse per esplodere."


translate italian day4_boathouse_2eb4d7e1:


    "Me ne stavo tra i cespugli a ridacchiare silenziosamente."


translate italian day4_boathouse_7a53a4ae:


    "Tuttavia, volevo sapere cosa avesse in mente e quindi sono uscito dal mio nascondiglio, senza paura di essere picchiato."


translate italian day4_boathouse_c7546dc1:


    me "E quindi cosa volevi mostrare esattamente alla leader?{w} Me?"


translate italian day4_boathouse_d6196049:


    "Alisa si è voltata e mi ha fissato incredula."


translate italian day4_boathouse_60e7b002:


    "Dopo un istante, la sorpresa si è trasformata in collera."


translate italian day4_boathouse_b7d8bf72:


    dv "Tu! Tuu!"


translate italian day4_boathouse_8f37a6c9:


    me "Io cosa?"


translate italian day4_boathouse_f834c3f8:


    "Si è calmata un po'."


translate italian day4_boathouse_12133f6d:


    dv "Volevo pareggiare i conti con te."


translate italian day4_boathouse_f0d6d1c8:


    "Ha sorriso."


translate italian day4_boathouse_21cbf526:


    me "Cioè?"


translate italian day4_boathouse_e8988316:


    dv "Avevi perso la scommessa.{w} Quindi ho voluto mostrare alla leader del Campo che mi stavi molestando."


translate italian day4_boathouse_0fc6db81:


    th "Mi chiedo cosa ci avrebbe visto di male Olga Dmitrievna in quella situazione, ma tralasciamo…"


translate italian day4_boathouse_0686c1b4:


    dv "Volevo vendicarmi!"


translate italian day4_boathouse_16b585f8:


    me "Di cosa?"


translate italian day4_boathouse_f3add060:


    th "Davvero, cosa ho fatto di male a questa ragazza per meritarmi la sua vendetta?"


translate italian day4_boathouse_fa0ca1ce:


    dv "Mi hai battuta al torneo di carte!"


translate italian day4_boathouse_b0dcc84f:


    th "Ma che motivo importante!{w} Senza dubbio…"


translate italian day4_boathouse_f2e007f0:


    me "Capisco…"


translate italian day4_boathouse_b6a65aa2:


    dv "Sei un perdente!"


translate italian day4_boathouse_32974cc7:


    me "E perché lo sono?"


translate italian day4_boathouse_15bab5bb:


    dv "Perché avevi paura di competere con me!"


translate italian day4_boathouse_3c1b245e:


    me "Quando?"


translate italian day4_boathouse_ef0158a3:


    dv "Al torneo di carte!"


translate italian day4_boathouse_9d2033ff:


    "Un motivo davvero serio…"


translate italian day4_boathouse_c63ec5a4:


    me "Già…"


translate italian day4_boathouse_57d2d760:


    me "Forse dovrei chiederti scusa?"


translate italian day4_boathouse_81684e59:


    "Le ho chiesto con sarcasmo."


translate italian day4_boathouse_0eebc5d9:


    dv "Fottiti!"


translate italian day4_boathouse_c0d6d356:


    "È entrata nella sua stanza, sbattendo la porta."


translate italian day4_boathouse_5e595186:


    "Non ero arrabbiato con Alisa."


translate italian day4_boathouse_663b2604:


    "Alla fine mi sarei aspettato una cosa simile da parte sua."


translate italian day4_boathouse_d12d5f50:


    "Comunque, sono riuscito a venirne fuori indenne."


translate italian day4_boathouse_2c46efd4:


    "Sono stato davvero fortunato – la sua confusione e il suo imbarazzo sono stati epici."


translate italian day4_boathouse_5030ce16:


    "Ridacchiando, mi sono allontanato dalla casetta di Alisa, che era quasi diventata una trappola mortale per me."


translate italian day4_main2_a20cefa7:


    "..."


translate italian day4_main2_48d0d4e6:


    "Sono andato a pranzo con sentimenti contrastanti – con un senso di realizzazione e con la sensazione di aver sprecato del tempo prezioso."


translate italian day4_main2_4cb70312:


    "La mensa era affollata."


translate italian day4_main2_58e949cc:


    "Non potevo passare inosservato – Olga Dmitrievna mi ha chiamato."


translate italian day4_main2_a4d48910:


    mt "Semyon, vieni con noi."


translate italian day4_main2_08ad4dba:


    "La leader del Campo, Slavya ed Electronik erano seduti ad un tavolo di quattro posti."


translate italian day4_main2_2acc25a8:


    "Ho annuito e sono andato a prendere il mio pranzo."


translate italian day4_main2_523ebbc0:


    "Questa volta ho dovuto aspettare in fila per diversi minuti."


translate italian day4_main2_c6c2e073:


    "Il menu di quest'oggi non era molto diverso da quello degli altri giorni.{w} O almeno, i piatti mi sembravano gli stessi."


translate italian day4_main2_05eb9d8b:


    "Dopo essermi seduto al tavolo e aver augurato buon appetito a tutti, Olga Dmitrievna ha detto:"


translate italian day4_main2_b397d74c:


    mt "Allora, cosa ne pensi?"


translate italian day4_main2_c233b6a2:


    me "Riguardo a cosa?"


translate italian day4_main2_67341092:


    mt "Abbiamo cercato Shurik dappertutto.{w} È già mezzogiorno, forse è successo qualcosa di brutto."


translate italian day4_main2_3d1cf378:


    "Ho notato la rima, ma non volevo farglielo notare."


translate italian day4_main2_5f3e793a:


    sl "Abbiamo cercato in tutto il Campo."


translate italian day4_main2_46ae85f2:


    el "Io ho guardato tutt'attorno alla foresta circostante."


translate italian day4_main2_cefc6cd9:


    "Olga Dmitrievna mi ha guardato."


translate italian day4_main2_f0777904:


    me "E anche io…{w} ho contribuito alle ricerche."


translate italian day4_main2_ad33fcd7:


    mt "Dobbiamo chiamare la polizia!"


translate italian day4_main2_cc3b3354:


    me "Forse dovremmo aspettare fino a sera?"


translate italian day4_main2_26be9189:


    "Ho chiesto pigramente."


translate italian day4_main2_d859378c:


    me "Può darsi che sia tornato a casa sua…"


translate italian day4_main2_29941732:


    mt "È impossibile!{w} Shurik abita a migliaia di chilometri da qui."


translate italian day4_main2_51a52fb5:


    me "Forse ha preso il treno?"


translate italian day4_main2_932e3a79:


    mt "La stazione più vicina…"


translate italian day4_main2_b7d1c016:


    "Ha fatto una pausa."


translate italian day4_main2_61df2540:


    mt "…è molto lontana…"


translate italian day4_main2_35d07915:


    "Adesso sì che la cosa stava diventando interessante."


translate italian day4_main2_3c635a17:


    th "Tutte le volte che la conversazione sta per toccare i possibili modi in cui potrei abbandonare «Sovyonok», tutti cambiano discorso."


translate italian day4_main2_91bfb245:


    me "Quanto «lontana»?"


translate italian day4_main2_a63beaef:


    mt "Davvero lontana."


translate italian day4_main2_f3bfc63b:


    "La leader del Campo mi ha guardato, cercando di farmi capire che sarebbe stato meglio non fare ulteriori domande."


translate italian day4_main2_12bcc6fb:


    sl "Allora dovremmo addentrarci più in profondità nella foresta! Forse si è perso!"


translate italian day4_main2_775f2318:


    el "Shurik porta sempre una bussola con sé."


translate italian day4_main2_3c97ead2:


    "È intervenuto Electronik."


translate italian day4_main2_df6e5065:


    "Mi chiedo cos'altro si possa trovare nel suo giubbotto magico (sempre che ne abbia uno)?"


translate italian day4_main2_feb702c4:


    "Se io mi perdessi nella foresta, una bussola mi servirebbe a poco…"


translate italian day4_main2_0ff502ce:


    mt "La polizia!{w} Dovremmo chiamare la polizia questa sera!"


translate italian day4_main2_35192e6e:


    th "Non in questo preciso istante, almeno."


translate italian day4_main2_af1d0e2a:


    me "Ok, allora lo farete più tardi…"


translate italian day4_main2_ec9674fa:


    "Sono rimasti tutti in silenzio."


translate italian day4_main2_a0920c3c:


    me "Dovremmo riuscire a trovarlo prima di sera, c'è ancora tempo."


translate italian day4_main2_97d66c28:


    mt "Se davvero si è perso, allora non abbiamo tempo da perdere!"


translate italian day4_main2_24f8e21c:


    sl "Non possiamo averne la certezza."


translate italian day4_main2_dea475da:


    mt "Allora dov'è? Dove?"


translate italian day4_main2_515c8c9d:


    "C'era un fondo di verità nelle parole della leader. «Nascondersi» per l'intera giornata in quel modo era sospetto."


translate italian day4_main2_5f25be5b:


    "Perché l'avrebbe fatto?{w} Shurik sembrava essere un pioniere piuttosto serio. Tale comportamento sarebbe stato più adatto a Ulyana."


translate italian day4_main2_2ddaebf2:


    "Quindi c'era motivo di pensare che se ne fosse andato."


translate italian day4_main2_593ca1bb:


    me "Tutto quello che si poteva fare l'abbiamo fatto. Adesso dobbiamo solo aspettare."


translate italian day4_main2_56f14e0a:


    "Slavya, Electronik e Olga Dmitrievna mi hanno guardato addolorati, senza aggiungere altro."


translate italian day4_main2_a20cefa7_1:


    "..."


translate italian day4_main2_6aa774a1:


    "Ho finito il mio pranzo, ho riportato il vassoio e me ne sono andato."


translate italian day4_main2_5abbd6f2:


    th "È ancora la prima metà della giornata…"


translate italian day4_main2_11e5ce88:


    th "E adesso?"


translate italian day4_main2_19b6f21b:


    "Il Campo sembrava essere pronto per un pisolino pomeridiano."


translate italian day4_main2_99bbc067:


    "Solo Genda mi osservava attraverso i suoi occhiali."


translate italian day4_main2_ab4e08c6:


    "Ovviamente stava osservando qualcos'altro, ma avevo la sensazione che mi stesse fissando in continuazione."


translate italian day4_main2_5f2f4e3e:


    th "Scommetto che lui sa dove si sta nascondendo Shurik.{w} Ma non può dire una parola…"


translate italian day4_main2_a3a27538:


    "La scomparsa del capo del circolo cibernetico mi ha fatto riflettere."


translate italian day4_main2_219aa2d9:


    th "Forse è qualcosa che ha a che fare col mio caso?"


translate italian day4_main2_2cf5c012:


    cs "Oh, pioniere!"


translate italian day4_main2_08272ac1:


    "L'infermiera stava davanti a me."


translate italian day4_main2_4a52b166:


    "L'ho guardata con curiosità."


translate italian day4_main2_d7304160:


    cs "Va' a prendere il mio posto all'infermeria.{w} Ho una chiamata urgente. Qualcuno è rimasto ferito."


translate italian day4_main2_de5dd677:


    me "Io?"


translate italian day4_main2_dacf7660:


    cs "Si, tu!{w} Prendi le chiavi!"


translate italian day4_main2_f30d0d14:


    "L'infermiera mi ha lanciato le chiavi ed è corsa via."


translate italian day4_main2_469e7359:


    th "Perché proprio io?{w} Non c'è nessun altro in questo Campo? E che cosa dovrei fare esattamente? Cosa farò se dovesse succedere qualcosa?"


translate italian day4_main2_7d9ad10a:


    th "Oh e adesso che faccio, ho perso la mia occasione di rifiutare…"


translate italian day4_main2_ab1de823:


    "Sono rimasto in piedi davanti alla porta, indeciso."


translate italian day4_main2_c422bd2d:


    th "Da una parte non c'è nulla di cui preoccuparsi – starò qui per mezz'oretta e poi lei tornerà…"


translate italian day4_main2_6e8191b6:


    th "Ma cosa faccio se qualcuno verrà qui in cerca di aiuto?{w} O magari con una gamba rotta? O con una ferita alla testa?"


translate italian day4_main2_78c21f2c:


    "Ho iniziato a preoccuparmi un po' troppo."


translate italian day4_main2_72b4eb4d:


    "Speravo di non dover occuparmi di ferite più gravi di qualche livido o graffio."


translate italian day4_main2_65e07906:


    "Ma allo stesso tempo non potevo negare che in una situazione grave sarei stato del tutto inutile."


translate italian day4_main2_1e5b77ba:


    th "Non so nemmeno come fare un massaggio cardiaco!"


translate italian day4_main2_d02d8b17:


    "La mia attenzione si è posata su una rivista che stava sulla scrivania dell'infermiera."


translate italian day4_main2_b5804212:


    th "Un bel modo per rilassarsi.{w} Suppongo..."


translate italian day4_main2_ec4a19eb:


    "Era intitolata «Fashion Sovietica»."


translate italian day4_main2_e9159fbe:


    "La data di pubblicazione però non c'era."


translate italian day4_main2_726e419d:


    "Non ne ero sorpreso – c'erano cose molto più strane di questa. Non ne sapevo molto di riviste sovietiche: forse davvero non riportavano la data di pubblicazione su di esse…"


translate italian day4_main2_21eab677:


    "Modelle che indossavano abiti fuori moda mi fissavano dalle pagine patinate."


translate italian day4_main2_bb76f93b:


    th "Al giorno d'oggi nessuno indosserebbe certa roba."


translate italian day4_main2_a026df63:


    "Ho ridacchiato."


translate italian day4_main2_f5d07ae7:


    th "Mi chiedo se Slavya consideri queste cose alla moda."


translate italian day4_main2_2453e3b6:


    th "Posso solo immaginare cosa accadrebbe se lei apparisse nella mia realtà indossando qualcosa del genere…"


translate italian day4_main2_864530e1:


    th "Stiamo camminando mano nella mano. Io sto indossando il mio cappotto con il cappuccio, e lei ha indosso un abito sinuoso ricoperto di pizzo e altre cose…"


translate italian day4_main2_e7482e77:


    "Sembrava che mi stessi già immaginando Slavya nel mio mondo…{w} Insieme a me…{w} E non solo Slavya."


translate italian day4_main2_19330451:


    th "Questo abito sarebbe più adatto a Ulyana, questo bel sarafan starebbe bene su Alisa, questa gonna e questo cardigan sarebbero perfetti per Lena."


translate italian day4_main2_acf18888:


    "Se solo fossero reali…"


translate italian day4_main2_45c2a3ea:


    "No, potevo vederle, sentirle, persino toccarle."


translate italian day4_main2_b793115e:


    th "Ma comunque loro sono qui, mentre io…{w} semplicemente non appartengo a questo luogo."


translate italian day4_main2_c7ecb5e1:


    th "È alieno per me.{w} Sto solo aspettando l'occasione per andarmene."


translate italian day4_main2_34ecfe30:


    th "Sto aspettando, dato che ormai nulla dipende da me…"


translate italian day4_main2_a734d9d3:


    "Ho sospirato, ho appoggiato la testa sulla scrivania e mi sono addormentato."


translate italian day4_main2_a20cefa7_2:


    "..."


translate italian day4_main2_ad69b965:


    "Sono stato svegliato dal rumore della porta che si apriva."


translate italian day4_main2_26171c75:


    "Lena è apparsa davanti a me."


translate italian day4_main2_aa82a2c6:


    th "Ieri le avevo promesso di aiutarla qui nell'infermeria..."


translate italian day4_main2_6bb5c0bf:


    "Sono diventato rosso per la vergogna e mi sono girato, facendo finta di essere impegnato in qualcosa."


translate italian day4_main2_fbb88586:


    un "Non c'è l'infermiera?{w} Allora torno più tardi…"


translate italian day4_main2_37395644:


    me "Mi ha chiesto di sostituirla!"


translate italian day4_main2_d87b9ec9:


    th "Dato che adesso sono io il responsabile della salute dei pionieri, dovrei prendermi le dovute responsabilità."


translate italian day4_main2_7b38c5ca:


    "Anche se, a dire il vero, avevo solo paura che qualcosa di brutto sarebbe accaduto per causa mia."


translate italian day4_main2_06a1509d:


    me "Qualche problema di salute?"


translate italian day4_main2_4c00f79b:


    "Ho cercato di fare il sorriso più professionale possibile, per non metterla in imbarazzo."


translate italian day4_main2_69f94ecc:


    un "Niente di grave…{w} Solo un piccolo mal di testa."


translate italian day4_main2_2357f708:


    me "D'accordo allora!"


translate italian day4_main2_27d0cf32:


    th "Magari qualche antidolorifico..."


translate italian day4_main2_f9569158:


    "Ovviamente non sapevo dove cercarli, e quindi ho impiegato un po' di tempo prima di trovarli."


translate italian day4_main2_649ec83c:


    me "Finalmente!"


translate italian day4_main2_64f21126:


    "Ho dato un analgesico a Lena."


translate italian day4_main2_082d3571:


    un "Grazie…"


translate italian day4_main2_d1046a49:


    "Ha sorriso."


translate italian day4_main2_38a347b1:


    "Non me lo sarei mai aspettato da parte sua, e mentre la fissavo ho perso il contatto con la realtà."


translate italian day4_main2_3875ba7b:


    un "Cosa c'è?"


translate italian day4_main2_cb318c1b:


    "Lena si è imbarazzata immediatamente."


translate italian day4_main2_bec7dcec:


    me "Senti, mi stavo chiedendo, ti piace questo?"


translate italian day4_main2_6cbc07c6:


    "Non so cosa m'avesse preso, ma ho afferrato quella rivista e le ho mostrato l'immagine della gonna e del cardigan, che secondo me le starebbero davvero bene."


translate italian day4_main2_f9690e49:


    "Forse avevo perso totalmente la testa, pensando e immaginando tutte loro nel mio mondo."


translate italian day4_main2_50cb5ce9:


    "O forse volevo solo distrarmi un po', in attesa che l'infermiera ritornasse."


translate italian day4_main2_1523dd90:


    "Lena ha guardato l'immagine."


translate italian day4_main2_1aa04f80:


    un "Sì, forse…"


translate italian day4_main2_bc64bbfd:


    me "Questi vestiti… sono di moda?"


translate italian day4_main2_3c68ea6c:


    un "Credo di sì…"


translate italian day4_main2_340ec68e:


    "Si è imbarazzata ed è arrossita."


translate italian day4_main2_9812399d:


    un "Perché me lo chiedi?"


translate italian day4_main2_59e7c056:


    th "Davvero, perché…"


translate italian day4_main2_082d3571_1:


    un "Ti ringrazio…"


translate italian day4_main2_dcd2e3df:


    me "Di niente, volevo solo essere sincero con te!"


translate italian day4_main2_47092547:


    un "…"


translate italian day4_main2_c4d1a45e:


    "Siamo rimasti in silenzio per un po'."


translate italian day4_main2_a417690d:


    me "Come va il tuo mal di testa?"


translate italian day4_main2_c1c02f2e:


    un "Molto meglio, grazie."


translate italian day4_main2_d1046a49_1:


    "Ha sorriso."


translate italian day4_main2_a17b8e3e:


    un "Devo andare."


translate italian day4_main2_3fc88750:


    me "Buona fortuna!"


translate italian day4_main2_7e83005b:


    "Lena è uscita e io ho continuato a sfogliare la rivista."


translate italian day4_main2_a20cefa7_3:


    "..."


translate italian day4_main2_500f0017:


    "Poco dopo qualcuno ha bussato nuovamente alla porta."


translate italian day4_main2_22afe73c:


    th "Sembra che l'infermeria sia il luogo più popolare qui."


translate italian day4_main2_b588274b:


    "Ho deciso di giocare a fare l'infermiera…{w} o meglio l'infermiere…{w} e ho detto:"


translate italian day4_main2_6b33d385:


    me "Avanti!"


translate italian day4_main2_573b3dff:


    "La porta si è aperta ed è entrata Ulyana."


translate italian day4_main2_5536cc92:


    me "Wow! Mi sono perso il momento in cui hai deciso di iniziare a bussare?"


translate italian day4_main2_9ef7bfe7:


    us "C'è qualcosa di strano se busso alla porta?"


translate italian day4_main2_d6ea162b:


    "Ha aggrottato la fronte."


translate italian day4_main2_396150e8:


    me "Dove ti fa male?"


translate italian day4_main2_cd1dfac0:


    us "E perché diavolo dovrei dirtelo?{w} Dov'è l'infermiera?"


translate italian day4_main2_7611a63b:


    me "Proprio davanti a te."


translate italian day4_main2_d2429dec:


    "Ho incrociato le gambe imponentemente, guardandola con aria interrogativa."


translate italian day4_main2_9881e8d1:


    us "È meglio che io vada.{w} Preferirei morire piuttosto che farmi curare da te."


translate italian day4_main2_7f6b716b:


    "Ha sorriso maliziosamente."


translate italian day4_main2_5ae0e9e5:


    me "Ma non mi hai nemmeno lasciato provare!"


translate italian day4_main2_2ccca5b3:


    "Ulyana ci ha pensato su."


translate italian day4_main2_2348abed:


    us "Anche se potresti darmi delle pillole."


translate italian day4_main2_fdf90b79:


    me "Che cosa ti sta dando fastidio?"


translate italian day4_main2_aa62d7ce:


    "Ci ha messo un po' a rispondere."


translate italian day4_main2_f32986c4:


    us "Ho mal di stomaco…"


translate italian day4_main2_150675b6:


    me "Sei sicura che non sia invece una testa vuota?"


translate italian day4_main2_3e1d421a:


    "Ho borbottato sottovoce."


translate italian day4_main2_76d30161:


    us "Cos'hai detto?"


translate italian day4_main2_f6290a5b:


    me "Niente. Dammi un secondo."


translate italian day4_main2_f0d4eaf6:


    "Ho trovato le pillole nel primo armadietto che ho aperto."


translate italian day4_main2_3ae2d123:


    us "Grazie, dottore!"


translate italian day4_main2_d2b56c39:


    "Mi ha sorriso allegramente."


translate italian day4_main2_45c9bd9f:


    "Guardando Ulyana non riuscivo a immaginare come una ragazzina così attiva e ottimista come lei potesse avere dei problemi di salute."


translate italian day4_main2_db45ba49:


    "Mi ha guardato con cattiveria."


translate italian day4_main2_f3f3ce8a:


    us "No, le ho lasciate tutte per te!"


translate italian day4_main2_4323f27c:


    "Ulyana è corsa via sbattendo la porta."


translate italian day4_main2_bd0d0f91:


    "Forse non avrei dovuto dire quella cosa."


translate italian day4_main2_4196aef5:


    me "Data la qualità della cucina locale, non mi sorprende."


translate italian day4_main2_f2c43065:


    us "Forse…"


translate italian day4_main2_f0d6d1c8:


    "Mi ha fatto un sorriso."


translate italian day4_main2_5c8b5c7d:


    us "Vedo che sei ancora vivo e vegeto!"


translate italian day4_main2_6e98e9f8:


    me "Beh, non mi lamento…"


translate italian day4_main2_14f237e5:


    us "Ci vediamo!"


translate italian day4_main2_4323f27c_1:


    "Ulyana è corsa via, sbattendo la porta."


translate italian day4_main2_a20cefa7_4:


    "..."


translate italian day4_main2_dbbcc16f:


    "Mi sono rimesso a studiare la moda sovietica."


translate italian day4_main2_a3a786a7:


    "Il tempo scorreva ma l'infermiera non si faceva vedere."


translate italian day4_main2_5db604a8:


    "Non stavo cercando Shurik, non stavo cercando risposte…{w} Ero semplicemente rinchiuso in una stanza a leggere una rivista."


translate italian day4_main2_900a2743:


    "Per un istante mi sono sentito soddisfatto di questa situazione…"


translate italian day4_main2_f82c0a74:


    th "Finora tutto quello che è accaduto qui può essere considerato come una gita in un campo estivo, e se qualcosa non cambierà a breve, allora potrò iniziare a preoccuparmi."


translate italian day4_main2_cbdcd68a:


    "Di nuovo qualcuno ha bussato alla porta."


translate italian day4_main2_7c25eaac:


    th "È scoppiata forse un'epidemia?"


translate italian day4_main2_482a0501:


    "Slavya è apparsa sulla soglia."


translate italian day4_main2_23d59ff0:


    sl "Oh, ciao! L'infermiera non c'è?"


translate italian day4_main2_7f88d310:


    me "Ciao. No, non è qui. La sostituisco io oggi."


translate italian day4_main2_8803d0ca:


    sl "Forte.{w} Mi servirebbe…"


translate italian day4_main2_6e6e30b3:


    "Ha esitato."


translate italian day4_main2_ad02f53a:


    me "Cosa?"


translate italian day4_main2_23a777ed:


    sl "A proposito, Semyon..."


translate italian day4_main2_626d4643:


    "Mi ha detto Slavya, guardandomi intensamente."


translate italian day4_main2_ad02f53a_1:


    me "Cosa?"


translate italian day4_main2_809b714a:


    sl "Credo di aver perso le chiavi. Non è che per caso tu le abbia viste da qualche parte?"


translate italian day4_main2_c2f310ec:


    th "Eccome se le ho viste!"


translate italian day4_main2_fe5cc028:


    me "Sì, eccole qui, le ho trovate ieri sera vicino alla mensa. Volevo ridartele ma poi mi sono dimenticato..."


translate italian day4_main2_7506f80e:


    "Ero davvero una frana nell'inventare bugie – le mie guance sono diventate rosse, i miei occhi rimbalzavano da un punto all'altro della stanza e le mie mani tremavano in continuazione."


translate italian day4_main2_71a21743:


    me "Ecco."


translate italian day4_main2_01753d9d:


    "Mi stavo preparando per una bella sgridata, ma Slavya ha semplicemente preso le chiavi e mi ha detto:"


translate italian day4_main2_edc3cf97:


    sl "Grazie."


translate italian day4_main2_1a6d0231:


    me "Allora, perché sei venuta?"


translate italian day4_main2_3e086ec1:


    "Avevo davvero bisogno di cambiare argomento."


translate italian day4_main2_12a9dd7f:


    me "Ti fa male qualcosa?"


translate italian day4_main2_87efefbd:


    sl "No, non è niente…"


translate italian day4_main2_06e847e7:


    th "Strano.{w} Non riesco a immaginare che una persona aperta come Slavya possa nascondere qualcosa."


translate italian day4_main2_0ca10a35:


    me "Se hai qualche problema, dimmelo!{w} Sono qui per questo, per guarire le persone."


translate italian day4_main2_ed4f432e:


    "Ho fatto un ampio sorriso."


translate italian day4_main2_537386e4:


    sl "No…{w} Voglio dire, sì, ma no."


translate italian day4_main2_981e60cc:


    "Sentire quelle parole da lei rendeva ogni cosa possibile, persino la divisione per zero."


translate italian day4_main2_d633a8ca:


    me "Allora, come posso aiutarti?"


translate italian day4_main2_e2229da3:


    sl "Tu?{w} Credo che tu non possa farlo."


translate italian day4_main2_d6c3c729:


    "Ha sorriso e stava per andarsene, ma si è fermata di colpo."


translate italian day4_main2_0a917d1d:


    sl "Anche se…{w} Potresti uscire un minuto?"


translate italian day4_main2_9ae266c2:


    th "Perché no?"


translate italian day4_main2_65bf82fc:


    me "Ok…"


translate italian day4_main2_cf1fe9a1:


    "Sono uscito dall'infermeria e mi sono appoggiato al muro."


translate italian day4_main2_47737877:


    th "Mi chiedo cosa stia facendo, qualcosa che non posso nemmeno vedere?"


translate italian day4_main2_8ee2b602:


    "Dopo un minuto la porta si è aperta e Slavya è uscita."


translate italian day4_main2_00d76af5:


    "Aveva fra le mani un piccolo sacchetto."


translate italian day4_main2_fda5f154:


    me "Che cos'è?"


translate italian day4_main2_1e8b4779:


    sl "Niente!"


translate italian day4_main2_ac04f5d1:


    "È arrossita."


translate italian day4_main2_b4db0146:


    sl "Grazie!"


translate italian day4_main2_c46d7c1e:


    me "Non c'è di che…"


translate italian day4_main2_ec475aa3:


    "Slavya è corsa via."


translate italian day4_main2_0f420061:


    th "Forse non avrei dovuto chiederglielo…"


translate italian day4_main2_3fc88750_1:


    me "Buona fortuna!"


translate italian day4_main2_3da1183b:


    "Le ho gridato."


translate italian day4_main2_309686e8:


    sl "Grazie!"


translate italian day4_main2_a3a10705:


    "Mi ha fatto uno dei suoi sorrisi più dolci, e poi se n'è andata."


translate italian day4_main2_a20cefa7_5:


    "..."


translate italian day4_main2_43b742c4:


    "Ho guardato l'orologio."


translate italian day4_main2_12b9bc98:


    "Si stava facendo tardi, continuavo a leggere e rileggere la rivista, ma dell'infermiera ancora nessuna traccia."


translate italian day4_main2_e60481fd:


    "All'improvviso la porta si è spalancata e Alisa è piombata dentro."


translate italian day4_main2_8fb93608:


    "Mi ha guardato con sguardo penetrante."


translate italian day4_main2_24b939bd:


    dv "Che ci fai qui?"


translate italian day4_main2_0085f606:


    me "Sto seduto…"


translate italian day4_main2_9fb02d3f:


    "Ho ammesso con sincerità."


translate italian day4_main2_2b2b99a8:


    dv "Bene allora!{w} È ancora meglio senza l'infermiera."


translate italian day4_main2_36cead21:


    "Ha mormorato."


translate italian day4_main2_46efe3f1:


    me "Stai male?"


translate italian day4_main2_81684e59:


    "Le ho chiesto in tono acido."


translate italian day4_main2_d1591f00:


    "Alisa non mi ha risposto e si è avvicinata a me."


translate italian day4_main2_7693d10f:


    dv "Levati di torno."


translate italian day4_main2_7332342b:


    me "Perché?"


translate italian day4_main2_b86ba02e:


    dv "Perché voglio aprire il cassetto, non è ovvio?!"


translate italian day4_main2_29a836d9:


    me "E per cosa?"


translate italian day4_main2_eed3c5aa:


    "Si è arrabbiata."


translate italian day4_main2_b2a1824d:


    dv "Non sono affari tuoi."


translate italian day4_main2_d0eeeaf6:


    me "Beh, a dire il vero adesso sarei io il capo qui…"


translate italian day4_main2_60f3d758:


    "Ha pensato per un momento."


translate italian day4_main2_f54686ff:


    dv "Allora dammi del carbone attivo!"


translate italian day4_main2_f841182c:


    me "Mal di stomaco?"


translate italian day4_main2_3a950d86:


    dv "Già."


translate italian day4_main2_1fcb3138:


    "Ha sorriso maliziosamente."


translate italian day4_main2_c5e0f7d5:


    "Avevo il sospetto che ci fosse sotto qualcosa…"


translate italian day4_main2_1cfd6748:


    th "Ma non si sa mai…{w} E se avesse davvero mal di stomaco?"


translate italian day4_main2_01822020:


    me "Ok."


translate italian day4_main2_e386270a:


    "Ho aperto il cassetto e ho preso un pacchetto di carbone attivo."


translate italian day4_main2_8526df6e:


    dv "Grazie!"


translate italian day4_main2_b7cee3ff:


    "Me l'ha strappato dalle mani ed è corsa via."


translate italian day4_main2_952420fb:


    me "Davvero, mal di stomaco?"


translate italian day4_main2_e6d2dba1:


    dv "Davvero!"


translate italian day4_main2_494cb28f:


    me "Non mi sembra proprio…"


translate italian day4_main2_30d58c19:


    dv "Cosa dovrei fare, vomitarti addosso affinché tu mi creda?"


translate italian day4_main2_5052e4e8:


    me "Non so esattamente quali cose terribili si possono fare col carbone attivo, ma sono sicuro che hai qualcosa in mente!"


translate italian day4_main2_6e6e30b3_1:


    "Ha esitato."


translate italian day4_main2_2e830d48:


    dv "E anche se fosse?"


translate italian day4_main2_ccdea9ff:


    me "Ecco!{w} Lo sapevo!{w} Allora di certo non ti darò un bel niente!"


translate italian day4_main2_422e86f8:


    "Alisa ha provato a raggiungere i cassetti, ma ho difeso il fortino con il torace, proprio come Alexander Matrosov."


translate italian day4_main2_da695243:


    "Nonostante la sua sfacciataggine, lei era solo una ragazza, e non aveva alcuna possibilità di riuscire a scansarmi."


translate italian day4_main2_672c3026:


    dv "Ah, che diavolo! Lo troverò da qualche altra parte."


translate italian day4_main2_4a53f5ef:


    "Si è girata ed è andata verso l'uscita."


translate italian day4_main2_579e0280:


    "Appena ha raggiunto la porta, si è fermata e mi ha guardato."


translate italian day4_main2_40117237:


    dv "Comunque, la leader del Campo e l'infermiera stanno trasportando un pioniere con una gamba rotta.{w} Non è che mi importi della cosa, ma sembrano essere in difficoltà."


translate italian day4_main2_9f4cd3fa:


    me "Sì, certo!"


translate italian day4_main2_9aa2cf7a:


    dv "Guarda con i tuoi occhi se non ci credi…"


translate italian day4_main2_0f163ab9:


    "Alisa sembrava stesse mentendo, ma d'altra parte, se sarei riuscito a tenerla sott'occhio…"


translate italian day4_main2_4ecdd924:


    "Sono andato verso la porta e ho guardato di fuori.{w} Ma non c'era nessuno."


translate italian day4_main2_c0277722:


    me "Ehm, dove sarebbero esattamente?"


translate italian day4_main2_83ca7233:


    dv "Non sono ancora arrivati qui."


translate italian day4_main2_4a9b3777:


    "Ha allargato le braccia."


translate italian day4_main2_df8028e5:


    dv "Divertiti!"


translate italian day4_main2_eeae4ddf:


    "Sono tornato alla scrivania e solo allora mi sono accorto che uno dei cassetti era aperto."


translate italian day4_main2_d82dccfe:


    th "Ma com'è riuscita a fare così in fretta?!"


translate italian day4_main2_26b03227:


    th "Mi ero voltato solo per un istante..."


translate italian day4_main2_5992178b:


    th "Ma comunque, che cosa potrebbe fare di male con del carbone attivo?"


translate italian day4_main2_a20cefa7_6:


    "..."


translate italian day4_main2_22f56cd3:


    "Mancavano solo quindici minuti alla cena, ma l'infermiera non era ancora tornata."


translate italian day4_main2_26c2ca6d:


    th "Non credo di potermene semplicemente andare da qui."


translate italian day4_main2_5cce2231:


    th "Mi ha chiesto di sorvegliare l'infermeria dopotutto…"


translate italian day4_main2_259b8764:


    th "E chissà cosa potrebbe accadere se me ne andassi…"


translate italian day4_main2_96201ae8:


    th "Chi potrebbe voler rubare qualcosa da questo luogo?"


translate italian day4_main2_084da37b:


    th "Eccetto Alisa che ruba il carbone attivo…"


translate italian day4_main2_2f835300:


    "Ho aperto nuovamente la rivista e l'ho sfogliata per l'ennesima volta."


translate italian day4_main2_c423f302:


    "La moda degli anni Ottanta non era più così divertente."


translate italian day4_main2_9cba9e2a:


    "Ho sbadigliato più volte."


translate italian day4_main2_b3a0bc3f:


    "Erano già le sei di sera, quindi la cena era già iniziata."


translate italian day4_main2_154cf8b8:


    "Sarei dovuto rimanere qui in attesa fino alla fine, ma il mio stomaco sembrava avere idee diverse a riguardo."


translate italian day4_main2_fa7b39d4:


    "Mi sono alzato e mi sono diretto con decisione verso l'uscita."


translate italian day4_main2_172df4fa:


    "Ho sentito un leggero fruscio provenire da dietro la porta."


translate italian day4_main2_0360cdef:


    th "Potrebbe essere qualcun altro con un'intossicazione alimentare, con un braccio rotto, o qualcosa di peggio?"


translate italian day4_main2_dfc8fcaa:


    "Ho sospirato pesantemente e ho tirato la maniglia."


translate italian day4_main2_4fcedea9:


    "Ma fuori non c'era nessuno."


translate italian day4_main2_c5913e31:


    "Forse era solo la mia immaginazione…"


translate italian day4_main2_63fae2b6:


    "Sono tornato dentro e ho subito notato che qualcosa era cambiato nell'infermeria."


translate italian day4_main2_fc8152bc:


    "Per la precisione, una mela è apparsa sul tavolo."


translate italian day4_main2_166e21e4:


    th "Ma da dove?"


translate italian day4_main2_263f36e7:


    "La frutta non può materializzarsi dal nulla da sola, no?"


translate italian day4_main2_7f119004:


    "Anche se in questo Campo ogni cosa era possibile…"


translate italian day4_main2_8c3d5b7c:


    "Forse qualcuno l'aveva appena portata lì."


translate italian day4_main2_a3f9bd63:


    "Forse Lena voleva ringraziarmi."


translate italian day4_main2_c9e3d9e8:


    th "Dato che è così timida e…"


translate italian day4_main2_60cc7919:


    th "Ma aspetta un momento!{w} Come avrebbe potuto farlo senza farsi vedere?"


translate italian day4_main2_91c185e1:


    th "Lena di certo non si sarebbe arrampicata sulla finestra!"


translate italian day4_main2_598572e0:


    th "Quindi dev'essere qualcos'altro…"


translate italian day4_main2_a705caba:


    "Improvvisamente sono stato sopraffatto dalla paura."


translate italian day4_main2_c2d327f7:


    th "E se quella mela fosse avvelenata?"


translate italian day4_main2_7cd7e93e:


    "Tuttavia, perché qualcuno avrebbe dovuto complicarsi la vita, quando, essendo io completamente inerme, avrebbe potuto uccidermi in modo molto più semplice?"


translate italian day4_main2_dea7837b:


    th "Oppure potrebbe essere la mela che Eva ha assaggiato nel Giardino dell'Eden?"


translate italian day4_main2_74f2512f:


    th "In tal caso non dovrei proprio mangiarla!"


translate italian day4_main2_efaf11ed:


    "In quel momento il mio stomaco mi ha fatto notare la sua presenza brontolando a tradimento."


translate italian day4_main2_0aeb7b69:


    th "Tuttavia, impiegherei un certo tempo a chiudere l'infermeria, raggiungere la mensa, prendere il vassoio..."


translate italian day4_main2_1f01a97c:


    "Il mio stomaco stava per mangiare sé stesso."


translate italian day4_main2_7e26087d:


    th "Non c'è niente di male, suppongo…"


translate italian day4_main2_49800ad1:


    "Ancora una volta ho cercato di ricordare se quella mela si trovasse lì prima."


translate italian day4_main2_4d50734a:


    th "Alla fine, deve pur provenire da qualche parte!"


translate italian day4_main2_9c6489f9:


    th "Forse è arrivata dalla finestra?"


translate italian day4_main2_20f7d3af:


    "Era leggermente socchiusa."


translate italian day4_main2_74cca6f2:


    th "Quindi qualcuno dev'essere salito sulla finestra e averla messa lì."


translate italian day4_main2_ec09e77a:


    th "Niente di speciale! Tuttavia…"


translate italian day4_main2_02c83bbb:


    "Alla fine la fame ha prevalso, e ho iniziato a masticare quella mela con gusto."


translate italian day4_main2_cab5f253:


    "Dal sapore sembrava normale."


translate italian day4_main2_cd98f167:


    th "Anche se credo che neppure il sapore del veleno più forte possa essere degustato."


translate italian day4_main2_b6572b50:


    th "Ma è troppo tardi adesso per pensare a queste cose…"


translate italian day4_main2_582156c4:


    th "No!{w} Anche se fosse una mela normale, in ogni caso, non dovrei mangiare la frutta sporca!"


translate italian day4_main2_0b227b4a:


    th "Proprio così…"


translate italian day4_main2_f6b3f760:


    "Ho trovato una ragione per il mio timore e ho rimesso la mela al suo posto."


translate italian day4_main2_38770f6e:


    "Era il momento di andare a cena."


translate italian day4_main2_13514a11:


    "Durante il tragitto ho incontrato Electronik."


translate italian day4_main2_9e8d764f:


    me "Come sta andando? Avete trovato Shurik?"


translate italian day4_main2_c6d96820:


    el "No, ancora nessuna traccia di lui…"


translate italian day4_main2_0228ef23:


    me "Non preoccuparti…{w} Lo troveremo!"


translate italian day4_main2_257dff95:


    "Ho cercato di tirargli su il morale."


translate italian day4_main2_d1dd1c27:


    el "Ma è passato troppo tempo!{w} Io continuo a cercare."


translate italian day4_main2_6ffcae27:


    me "E la cena?"


translate italian day4_main2_2b8d0068:


    el "No, trovare Shurik è più importante."


translate italian day4_main2_a1983f0e:


    "Ha borbottato pensieroso."


translate italian day4_main2_0470832e:


    "L'ho lasciato al bivio, augurandogli buona fortuna."


translate italian day4_main2_00f424c0:


    "I pionieri stavano affollando l'entrata."


translate italian day4_main2_9219d4ed:


    "Ho affrettato il passo, cercando di non essere l'ultimo della fila, almeno non oggi."


translate italian day4_main2_29a4258a:


    "E sono stato fortunato – nell'angolo in fondo c'era ancora un tavolo totalmente vuoto."


translate italian day4_main2_5d928b01:


    "Ho preso velocemente il mio pasto e mi sono seduto a quel tavolo."


translate italian day4_main2_dffdc57f:


    "La cena di questa sera consisteva in zuppa di frutta e un paio di panini."


translate italian day4_main2_eac59148:


    "Inizialmente tale pietanza mi ha sorpreso, ma si è rivelata essere davvero buona."


translate italian day4_main2_e2cd40bb:


    "Mi sono concentrato sul mangiare."


translate italian day4_main2_c81cbbaa:


    mi "Lena, andiamo là in fondo! Guarda – ci sono tre posti liberi!"


translate italian day4_main2_8bd763eb:


    "Lena e Miku stavano davanti a me."


translate italian day4_main2_e140e52b:


    un "Sono occupate?"


translate italian day4_main2_0a28548a:


    "Ha chiesto Lena."


translate italian day4_main2_82cf5563:


    me "No, sedetevi pure."


translate italian day4_main2_e8b7b6b4:


    th "Ovviamente speravo che Lena fosse da sola."


translate italian day4_main2_082d3571_2:


    un "Grazie…"


translate italian day4_main2_f1ccf9dd:


    "E subito dopo che Lena ha detto ciò, Zhenya è sbucata alle sue spalle."


translate italian day4_main2_2ab6d036:


    mz "Mi siederò qui. Non ci sono più posti liberi."


translate italian day4_main2_11d6a6d3:


    "Ha detto, appoggiando il suo vassoio sul tavolo e sedendosi, senza nemmeno preoccuparsi della mia opinione a riguardo."


translate italian day4_main2_46957efa:


    me "Certo. Fa' come se fossi a casa tua..."


translate italian day4_main2_596d1d03:


    "Ho mormorato con tristezza."


translate italian day4_main2_6fa187e2:


    mz "Che cosa?!"


translate italian day4_main2_cfaa423d:


    me "Niente..."


translate italian day4_main2_1830a553:


    "A dire il vero avrei voluto ridurre la compagnia alla sola Lena, anche se né Miku né Zhenya mi stavano dando troppi problemi."


translate italian day4_main2_eea06034:


    "A parte il fatto che una era troppo loquace e l'altra troppo arrogante."


translate italian day4_main2_1ffd1a45:


    th "Ma entrambe sono del tutto innocue, specialmente se comparate con certe altre che potrei nominare."


translate italian day4_main2_0a276ba6:


    un "Oh, devo essermi dimenticata la mia chiave…"


translate italian day4_main2_1236c393:


    mi "Non preoccuparti, prendi la mia!"


translate italian day4_main2_39b90d94:


    "Sono rimasto sorpreso dalla rapida risposta di Miku."


translate italian day4_main2_b6287b9f:


    me "Ma voi…{w} abitate nella stessa stanza?"


translate italian day4_main2_94800851:


    mi "Certamente! Non lo sapevi? Insieme. La nostra casetta è quella più arretrata. Voglio dire, quella in fondo andando da qui. Cioè, l'ultima."


translate italian day4_main2_0d57a898:


    "Non sarei rimasto sorpreso se qualcuno mi avesse detto che Lena fosse in stanza con Slavya.{w} O anche con Zhenya.{w} O persino con Electronik."


translate italian day4_main2_c29a6bc8:


    th "Ma la silenziosa e timida Lena e la eccessivamente loquace Miku, in coppia…{w} Questa sì che è una sorpresa!"


translate italian day4_main2_670ca875:


    mz "Hai trovato Shurik?"


translate italian day4_main2_40a04971:


    "Strano che Zhenya fosse interessata ai problemi di qualcuno."


translate italian day4_main2_5a7a7577:


    me "No."


translate italian day4_main2_f9068a33:


    mz "Sicuramente è andato al villaggio a comprare le sigarette.{w} O la vodka."


translate italian day4_main2_58ae4528:


    "Ha sbuffato."


translate italian day4_main2_e9d77940:


    me "Villaggio?"


translate italian day4_main2_d535d09a:


    "Da quel momento la conversazione si è fatta molto più interessante."


translate italian day4_main2_86a5cb53:


    mz "Hai qualche problema con i villaggi?"


translate italian day4_main2_1490cc40:


    "Zhenya sembrava sorpresa."


translate italian day4_main2_fd219846:


    me "Vuoi dire che c'è un villaggio nelle vicinanze?"


translate italian day4_main2_429609fe:


    mz "Credo di sì…"


translate italian day4_main2_858becb4:


    "Mi ha risposto, incerta."


translate italian day4_main2_31ee18ee:


    "Ho guardato Lena e Miku, che erano occupate a mangiare e non parevano essere interessate alla nostra conversazione."


translate italian day4_main2_9e200e27:


    me "Vuoi dire che non ne sei sicura?"


translate italian day4_main2_2c01aece:


    mz "E perché dovrebbe interessarmi?!"


translate italian day4_main2_b265fe1c:


    "Zhenya ha fissato il suo piatto."


translate italian day4_main2_3a65b125:


    me "Ma dev'esserci qualcosa nelle vicinanze."


translate italian day4_main2_e76c967c:


    mz "Ascolta..."


translate italian day4_main2_a2516dfe:


    me "Sto ascoltando."


translate italian day4_main2_391fb882:


    mz "Non lo so! Mi lasci mangiare in pace?!"


translate italian day4_main2_76b07f1d:


    th "Sembra che non otterrò alcuna informazione da lei."


translate italian day4_main2_b71b185c:


    "Anche se c'era la possibilità che lei non lo sapesse davvero..."


translate italian day4_main2_91e6868c:


    "Il tempo restante l'ho trascorso ascoltando Miku che parlava di qualche sciocchezza. Stavo lentamente impazzendo in silenzio…"


translate italian day4_main2_7c2f2de1:


    "Ovviamente, la prima cosa che ho fatto appena sono uscito è stata inalare una bella boccata d'aria fresca."


translate italian day4_main2_ef1b28a4:


    "Il sole stava tramontando."


translate italian day4_main2_153cf25d:


    th "Forse è meglio fare una passeggiata."


translate italian day4_main2_2c803352:


    th "È molto improbabile che io possa trovare qualcosa di più entusiasmante da fare durante il resto della serata. Forse così qualcosa di interessante potrebbe sorgere inaspettatamente."


translate italian day4_main2_441bd7cd:


    "Stavo per raggiungere la piazza, quando improvvisamente ho sentito una forte esplosione.{w} Era come se qualcosa fosse scoppiato."


translate italian day4_main2_2c4a9213:


    "Ero pietrificato."


translate italian day4_main2_9abc32b1:


    th "Mi trovo in un ambiente ostile, totalmente inconsapevole delle sue regole e delle sue leggi!"


translate italian day4_main2_e35e500a:


    th "Sarebbe meglio se iniziassi a correre!{w} Ma allo stesso tempo sono curioso…"


translate italian day4_main2_f6711c85:


    "Probabilmente mi sarei limitato a restare fermo lì, ma qualcuno mi ha preso per mano."


translate italian day4_main2_db010348:


    "Era Olga Dmitrievna."


translate italian day4_main2_7cc1e65d:


    mt "Perché te ne stai lì immobile?{w} Andiamo a vedere cosa è successo!"


translate italian day4_main2_51893c59:


    me "Non ce la fa proprio senza di me?"


translate italian day4_main2_df9d9f60:


    "L'ho pregata pietosamente."


translate italian day4_main2_92fa383c:


    mt "Non dovrebbe essere nulla di serio!{w} Lo spero…"


translate italian day4_main2_723aed67:


    "Quando siamo arrivati alla piazza, una vasta folla di pionieri si era già radunata attorno al monumento."


translate italian day4_main2_0b603c9e:


    "Olga Dmitrievna si è fatta largo tra la folla vigorosamente, avvicinandosi alla scena del crimine."


translate italian day4_main2_c9d63825:


    "Era chiaro che qualcuno aveva tentato di far saltare in aria Genda."


translate italian day4_main2_7a693237:


    "Ma l'attacco è fallito – il monumento era ancora in piedi."


translate italian day4_main2_f15dbe9c:


    "C'erano solo delle tracce di fuliggine alla sua base."


translate italian day4_main2_466f41f2:


    mt "Ma bene, chi è stato?"


translate italian day4_main2_fe6f2470:


    "Si è rivolta alla folla di pionieri."


translate italian day4_main2_588fe4a7:


    "Di certo non è stata opera di un'organizzazione terrorista."


translate italian day4_main2_dcd82f25:


    "Tutti questi ragazzi erano venuti qui solo per vedere cosa fosse successo."


translate italian day4_main2_1da09f52:


    "Ho notato Ulyana e Alisa tra la folla.{w} E sembrava che anche la nostra leader le avesse notate."


translate italian day4_main2_fdaff1c2:


    mt "Voi due, venite qui!"


translate italian day4_main2_c276a464:


    "Si sono avvicinate con riluttanza."


translate italian day4_main2_4013aec6:


    us "Perché sempre io?"


translate italian day4_main2_ac0e5120:


    dv "Se proprio vuole…"


translate italian day4_main2_f8a4a1d3:


    mt "Fammi vedere le mani, Dvachevskaya!"


translate italian day4_main2_0c3815a2:


    dv "Cosa c'è che non va con le mie mani?"


translate italian day4_main2_a967cbc5:


    "Ho guardato meglio e ho notato che erano imbrattate di nero."


translate italian day4_main2_6914b9a6:


    mt "Ora è tutto chiaro…{w} E con cosa hai costruito la bomba?!"


translate italian day4_main2_74595ca6:


    "La piccola terrorista sembrava non sapere se confessare o meno, ma poi ha sbottato con orgoglio:"


translate italian day4_main2_73fdb6c9:


    dv "Carbone attivo, salnitro e zolfo!"


translate italian day4_main2_e3192dfb:


    th "Aspetta un secondo!{w} Carbone attivo!{w} Quello che ha rubato dall'infermeria!"


translate italian day4_main2_2d58bb92:


    mt "Perché proprio il monumento?{w} Che cosa vi ha mai fatto quest'uomo onorato? Colui che ha combattuto per i diritti dei…"


translate italian day4_main2_23e3015c:


    dv "Comunque, è stato lui a darmi il carbone attivo."


translate italian day4_main2_276a218b:


    "Ha puntato il dito verso di me. L'intera folla mi ha fissato."


translate italian day4_main2_034b00d2:


    "I pensieri circa l'inutilità di quell'atto date le ridotte dimensioni della bomba mi hanno abbandonato all'istante."


translate italian day4_main2_a6f5f832:


    me "L'ha rubato!{w} Io non ho fatto niente!"


translate italian day4_main2_978d7670:


    mt "Anche se te l'avesse dato lui, sono sicura che Semyon non avrebbe mai preso parte ad un atto così meschino e antisociale!"


translate italian day4_main2_e7ed10ff:


    me "Sì! Sì! Esatto!"


translate italian day4_main2_d3ec559b:


    "Ho confermato."


translate italian day4_main2_a20cefa7_7:


    "..."


translate italian day4_main2_a20cefa7_8:


    "..."


translate italian day4_main2_d88f4446:


    "Avrei potuto solo immaginare per quanto a lungo ancora lei avrebbe fatto la ramanzina ad Alisa, se non fosse stato per Electronik, che è sbucato all'improvviso in quel preciso istante, gridando:"


translate italian day4_main2_e6b5d4f7:


    el "L'ho trovato! L'ho trovato!"


translate italian day4_main2_a09465a1:


    "Tutti si sono voltati verso di lui."


translate italian day4_main2_50ceb1d4:


    "Tra le mani teneva uno stivale."


translate italian day4_main2_16b80206:


    el "Eccolo!"


translate italian day4_main2_b9a254c5:


    "Electronik l'ha ondeggiato sopra la propria testa."


translate italian day4_main2_11da0634:


    el "È lo stivale di Shurik!"


translate italian day4_main2_c764847f:


    mt "Bene, calmati!{w} Dicci esattamente dove l'hai trovato!"


translate italian day4_main2_a425fd4f:


    el "Nella foresta.{w} Sul sentiero per il vecchio campo!"


translate italian day4_main2_13dffab5:


    "I pionieri hanno cominciato a sussurrare."


translate italian day4_main2_8c7b2ad8:


    all "Il vecchio campo… Il vecchio campo…"


translate italian day4_main2_d3e8a3d1:


    mt "Ne sei sicuro?"


translate italian day4_main2_d0c3eb6a:


    el "Assolutamente!"


translate italian day4_main2_438450d6:


    me "Che c'è di tanto speciale in questo vecchio campo?"


translate italian day4_main2_37372bdb:


    "Mi sono intromesso nella conversazione."


translate italian day4_main2_1b61865c:


    mt "Niente di speciale, davvero…"


translate italian day4_main2_49a81686:


    "Ha balbettato."


translate italian day4_main2_91c692ef:


    el "Una delle leggende di «Sovyonok» dice che lì si aggiri il fantasma di una giovane leader del campo. Si innamorò di un suo pioniere, ma lui la rifiutò, e così si tolse la vita…"


translate italian day4_main2_417824db:


    us "Fece hara-kiri con un coltello da cucina.{w} Il giorno seguente il ragazzo fu investito da un autobus!"


translate italian day4_main2_98631528:


    "Ulyana è sbucata dalla folla."


translate italian day4_main2_5eb46b8b:


    me "Da un autobus?"


translate italian day4_main2_94dec8ff:


    "Mi sono astenuto dal chiedere il numero della tratta."


translate italian day4_main2_8e90eaa7:


    el "Ma la scienza non dimostra l'esistenza dei fantasmi. Quindi non ci dobbiamo preoccupare di niente del genere."


translate italian day4_main2_2d8001fb:


    mt "Comunque, qualcuno dovrebbe andare a dare un'occhiata!"


translate italian day4_main2_4ab0957c:


    "Improvvisamente la folla ha iniziato a disperdersi."


translate italian day4_main2_957e36e8:


    sl "Olga Dmitrievna, è quasi notte. Non sarebbe meglio farlo domani?"


translate italian day4_main2_51b9d15d:


    "Mi sono girato e ho visto Slavya e Lena."


translate italian day4_main2_f8e498d7:


    mt "E se questa notte…{w} E se questa notte gli accadesse qualcosa…?{w} No! Dev'essere fatto oggi! Adesso!"


translate italian day4_main2_ddc6851f:


    me "Comunque, dove si trova questo luogo?"


translate italian day4_main2_747f2d95:


    "Electronik mi ha descritto più o meno il percorso e mi ha raccontato la storia del vecchio campo."


translate italian day4_main2_cb78152f:


    "La leader del Campo mi ha guardato pensierosa."


translate italian day4_main2_3ac0f1ea:


    me "Se crede che io abbia intenzione di…"


translate italian day4_main2_e13e7c4b:


    mt "Sei l'unico uomo qui."


translate italian day4_main2_81abb051:


    "Mi sono guardato attorno."


translate italian day4_main2_db2e005d:


    th "Ma certo…{w} Electronik è già sparito!"


translate italian day4_main2_0bb42214:


    "Ma non avevo voglia di passeggiare per la foresta da solo."


translate italian day4_main2_eba0fa62:


    "Se avessi potuto scegliere, avrei preferito..."


translate italian day4_main2_83894804:


    "Ma che mi è preso?{w} Perché mai sarebbe dovuta venire con me...?"


translate italian day4_main2_a89ae039:


    th "Accetterà?"


translate italian day4_main2_69f37ce2:


    "Ho guardato Slavya."


translate italian day4_main2_22e7806d:


    "Mi stava ignorando."


translate italian day4_main2_f88a9c6c:


    th "Pare che non sia la mia giornata fortunata..."


translate italian day4_main2_3bf87393:


    th "Pare che anche la sua sete di avventura abbia i suoi limiti."


translate italian day4_main2_3ecb90c0:


    "Anche se era sciocco pensare che Lena non avrebbe avuto paura."


translate italian day4_fail_867359a2:


    me "Ci devo andare da solo?!"


translate italian day4_fail_be1d7816:


    "Olga Dmitrievna sembrava pensierosa."


translate italian day4_fail_d7898f7f:


    mt "Forse hai ragione…{w} Ci andremo insieme domani mattina."


translate italian day4_fail_c890c351:


    "Siamo rimasti in silenzio per un po', poi ci siamo dispersi per il Campo."


translate italian day4_fail_6a9a81a0:


    "Tutti sembravano essersi già dimenticati dell'atto terrorista di Alisa. Forse perché dopotutto non aveva fatto molti danni."


translate italian day4_fail_727cde7c:


    "Sono tornato alla mia casetta insieme alla leader."


translate italian day4_fail_d95546c4:


    "Stavo cercando di capire per quale motivo Shurik potesse essere andato al vecchio campo."


translate italian day4_fail_6b65c5dc:


    "A lui interessavano la robotica e la cibernetica, non i fantasmi e le storie di paura."


translate italian day4_fail_9917267e:


    th "E quello stivale che c'entra con tutto ciò...?"


translate italian day4_fail_a35e5ae3:


    "Mi sono ricordato di quando avevo deciso di aspettare finché qualcosa di particolare non sarebbe accaduto…"


translate italian day4_fail_ea2f2219:


    "E poi mi sono reso conto – era proprio questo il momento!"


translate italian day4_fail_76ef374b:


    me "Olga Dmitrievna, credo che mi farò una passeggiata prima di andare a dormire."


translate italian day4_fail_11c5c177:


    mt "Va bene, ma non tornare troppo tardi!"


translate italian day4_fail_80e4f228:


    me "Ok!"


translate italian day4_fail_e3c48cd1:


    "Quando ho raggiunto la piazza ho capito che il vagabondare nella foresta o in qualche altro strano luogo non sarebbe stato poi così spaventoso…"


translate italian day4_fail_04005f5b:


    "Ma farlo nel cuore della notte...!"


translate italian day4_fail_9fa06fb8:


    "Non avevo voglia di tornare nella stanza della leader, ma dovevo trovare una torcia.{w} Forse ce n'era una nell'infermeria."


translate italian day4_fail_f0d2d342:


    th "Sono contento di non aver trascorso l'intera serata lì a leggere la rivista di moda."


translate italian day4_fail_a20cefa7:


    "..."


translate italian day4_fail_f5100b67:


    "Ho trovato quasi subito la torcia, e dopo pochi minuti ero di nuovo alla piazza, cercando di concentrarmi."


translate italian day4_fail_48a0470e:


    "Non mi rimaneva che prendere la decisione finale, prima di entrare nella foresta."


translate italian day4_fail_4590a9c1:


    "Sono passato davanti alle casette dei pionieri, lungo il sentiero della foresta, che portava al campo abbandonato..."


translate italian day4_fail_dc86bdf9:


    "Ho iniziato il mio viaggio."


translate italian day4_fail_7bd87323:


    "Electronik mi aveva detto che il vecchio edificio fu costruito subito dopo la guerra."


translate italian day4_fail_fd3c1cdc:


    "Era come una scuola dell'infanzia (o come una caserma, ho immaginato), e di sicuro poteva ospitare meno persone di questo Campo."


translate italian day4_fail_e9bbdb04:


    "Era abbandonato da una ventina d'anni."


translate italian day4_fail_a20cefa7_1:


    "..."


translate italian day4_fail_afeb43da:


    "Si era fatto piuttosto buio…"


translate italian day4_fail_4b08c931:


    "La foresta sembrava totalmente diversa di notte."


translate italian day4_fail_00c53d58:


    "Ombre misteriose si nascondevano dietro agli alberi. Fruscii sinistri e grida di uccelli notturni provenivano da ogni angolo, i rami si spezzavano proditoriamente sotto ai miei piedi."


translate italian day4_fail_7944d14b:


    "La luna splendeva nel cielo, così ho deciso di risparmiare la batteria della torcia – ero sicuro che ne avrei avuto bisogno a breve."


translate italian day4_fail_acbdce76:


    "Più mi spingevo oltre, più i miei pensieri diventavano tenebrosi."


translate italian day4_fail_e275989e:


    "Non avevo affatto paura del buio. Non credevo nei fantasmi o in altre sciocchezze paranormali, ma allo stesso tempo in questo Campo avevo assistito a così tanti fenomeni inspiegabili, che inevitabilmente ho iniziato a pensare che persino le cose più semplici potessero nascondere qualcosa."


translate italian day4_fail_f927e7d0:


    "Ovviamente il vagare per la foresta di notte non mi faceva di certo sentire a mio agio."


translate italian day4_fail_eab673b2:


    "Qualunque cosa guardassi, qualunque ombra, qualunque albero o cespuglio sembravano delle strane creature, fuoriuscite direttamente da qualche film dell'orrore o da qualche racconto di Stephen King."


translate italian day4_fail_2ca15dc2:


    "I rami erano come degli artigli scheletrici, pronti ad afferrarti per le spalle. Un ceppo immobile sembrava il cadavere di un malcapitato che non era riuscito a trovare una via d'uscita da quel labirinto. E in ogni canalone vedevo un abisso senza fondo."


translate italian day4_fail_c6ac6be9:


    "Se mi fossi trovato in questa foresta in altre circostanze, senza tutti questi bus e Campi di pionieri, non mi sarei sentito comunque al sicuro, ma almeno avrei avuto la garanzia che tutto avrebbe obbedito alle leggi della natura, e le mie preoccupazioni più grandi sarebbero state i gufi e i topi."


translate italian day4_fail_bd7ebd85:


    "Ma in questo momento c'era più di una ragione per cui essere preoccupati."


translate italian day4_fail_851bae5d:


    "Nonostante tutto ciò, sono arrivato al vecchio campo senza particolari disavventure."


translate italian day4_fail_a20cefa7_2:


    "..."


translate italian day4_fail_c9298dc3:


    "In una piccola radura circondata dagli alberi sorgeva un edificio simile a una scuola dell'infanzia."


translate italian day4_fail_ca186bf0:


    "La vernice delle mura era ricoperta di screpolature. Quasi tutte le finestre erano frantumate. C'erano crepe ovunque, e i buchi nel tetto lasciavano pensare ai postumi di un bombardamento."


translate italian day4_fail_c42f0ebd:


    "Di fronte all'edificio c'era un piccolo giardino, circondato da una recinzione arrugginita."


translate italian day4_fail_cb36b943:


    "A quanto pare in passato era un parco giochi – c'era un carosello, delle altalene, degli scivoli e altre attrazioni sovietiche."


translate italian day4_fail_ba4e4337:


    "Ma il tempo è crudele. Erano tutti rotti e mutilati dalla corrosione."


translate italian day4_fail_f3f7ddd1:


    "L'erba era così alta che mi arrivava fino alla cintura."


translate italian day4_fail_c1bb36b5:


    "La luna si è affacciata oltre le nubi in modo sinistro, illuminando il vecchio campo."


translate italian day4_fail_7a22a02d:


    "Sembrava che tutto avesse preso vita: le finestre brillavano e le pareti sembravano essere state dipinte con vernice fresca."


translate italian day4_fail_96af7e9a:


    "Ho sentito delle risate giocose. Ho visto gli spettri dei fanciulli che ridevano e scherzavano nel cortile. Una severa leader del campo stava sulla soglia, tenendo in mano un cesto di mele mature. La vecchia donna delle pulizie stava facendo un pisolino."


translate italian day4_fail_30d755a6:


    "Un brivido mi è sceso lungo la schiena."


translate italian day4_fail_389b1cbf:


    "La mia immaginazione mi stava facendo brutti scherzi, ma quel vecchio campo era così simile alle surreali immagini post-apocalittiche che avevo già visto da qualche parte, che non c'era da stupirsi delle mie visioni."


translate italian day4_fail_e652569f:


    "Ho esitato a lungo prima di osare proseguire oltre."


translate italian day4_fail_654087eb:


    th "Dopotutto, sono qui per trovare Shurik!"


translate italian day4_fail_17d0369d:


    th "Anche se non riesco ancora a capire cosa sia venuto a fare in un luogo simile…{w} Sempre che lui sia davvero qui…"


translate italian day4_fail_0e534257:


    th "L'aver trovato uno stivale nelle vicinanze non significa proprio nulla!"


translate italian day4_fail_e4b26812:


    "Tuttavia, se non era già stato divorato dai lupi (anche se non credevo ci fossero lupi in questa foresta), questo doveva essere l'unico posto in cui avrebbe potuto nascondersi."


translate italian day4_fail_067a08ab:


    "Ho attraversato il cortile con attenzione, facendomi strada tra il mare d'erba, cercando di non inciampare in qualche pezzo di metallo arrugginito."


translate italian day4_fail_4c060fb7:


    "Mi sono fermato davanti all'entrata, esitando."


translate italian day4_fail_39f1d886:


    "Una profonda e impenetrabile oscurità mi fissava dall'interno di quell'edificio."


translate italian day4_fail_443d22e0:


    "Persino la luce della luna non riusciva a illuminare un qualsiasi dettaglio al suo interno, neppure una sagoma."


translate italian day4_fail_ef3a7e9d:


    "La mia testa era piena di pensieri che mi imploravano di non proseguire oltre e di tornare indietro immediatamente, finché ero ancora in tempo."


translate italian day4_fail_9aeb4e06:


    "Ma alla fine mi sono deciso, ho acceso la torcia e ho varcato la soglia."


translate italian day4_fail_bef84d9a:


    "L'interno dell'edificio sembrava un tipico asilo."


translate italian day4_fail_11a53aa3:


    "Ricordavo ancora la mia infanzia, e quindi tutto quell'ambiente mi risultava molto familiare."


translate italian day4_fail_b863bbfe:


    "Camere con letti messi uno accanto all'altro, una grande sala giochi, dei bagni, una sala da pranzo e dei ripostigli."


translate italian day4_fail_10696fa4:


    "Ogni oggetto sembrava avesse assorbito il ricordo della gioia dei bambini che avevano vissuto qui. Di tanto in tanto la luce della mia torcia si posava su una bambola in decadenza, o su una macchinina di plastica, o su un pallone di gomma bucato, o su un gruppo di soldatini di piombo rotti."


translate italian day4_fail_186331b8:


    "Sul tavolo c'era un libro, consumato dal tempo."


translate italian day4_fail_cb2cab2c:


    "Ho sfogliato un po' di quelle pagine."


translate italian day4_fail_f381b7fd:


    "C'era scritta una storia che avevo letto molto tempo fa, quando ero ancora bambino.{w} Qualcosa sulla guerra…"


translate italian day4_fail_b27a5c2e:


    "I nemici distrussero la scuola dell'infanzia e tutti i bambini si nascosero nel piccolo seminterrato assieme agli adulti, mentre le bombe esplodevano sopra le loro teste…"


translate italian day4_fail_25b7d2ba:


    "Non ricordo come finiva quella storia, ma credo di ricordare che nessuno morì."


translate italian day4_fail_81629920:


    "Guardando quelle immagini sono stato travolto da tutte le emozioni che provai quando lessi quella storia tanto tempo fa: paura, tristezza, simpatia nei confronti di quei poveri bambini, e un senso di disperazione."


translate italian day4_fail_1c3a4b63:


    "Strano che io abbia ritrovato questo libro proprio in una tale situazione, qui e adesso."


translate italian day4_fail_107ba7e9:


    th "Forse c'è qualche entità superiore che sta scrivendo questa storia, definendo lo scenario di questa recita assurda, cucendo i costumi dei pionieri del luogo e creando scene come questa del campo abbandonato."


translate italian day4_fail_0d6ce2d3:


    th "Devo dire che ha molto talento – tutto è curato nei minimi dettagli."


translate italian day4_fail_ffa0fc7b:


    "Sebbene questo posto fosse abbandonato da molto tempo, tutto sembrava essere rimasto intatto."


translate italian day4_fail_e82fe0f4:


    "Naturalmente, il tempo non ha risparmiato nemmeno questo vecchio campo, in tanti anni dev'essere stato visitato da molte persone."


translate italian day4_fail_67a745c4:


    "Un luogo perfetto per i giovani. Qui avrebbero potuto bere alcolici, indulgere a piaceri amorosi o semplicemente bruciare tutto per divertimento."


translate italian day4_fail_5cc1d037:


    "Electronik aveva detto che giravano molte leggende su questo luogo.{w} Leggende di fantasmi e diavoli che albergavano in questo edificio..."


translate italian day4_fail_20237b24:


    "Ad esempio la storia della leader del campo che si è suicidata."


translate italian day4_fail_9f4da515:


    "Nel {i}mio{/i} mondo nessuno ci avrebbe mai creduto. Ma in questa realtà tutto era possibile."


translate italian day4_fail_5ae24796:


    "Ovviamente la paura non mi aveva abbandonato, ma dato che ero arrivato fino a questo punto, volevo andare fino in fondo."


translate italian day4_fail_f267b85f:


    "L'edificio non era grande, quindi non è stato difficile esplorare ogni sua stanza."


translate italian day4_fail_230cbcfd:


    "Ma non c'era traccia di Shurik."


translate italian day4_fail_7e30252f:


    th "Forse dovrei provare a chiamarlo ad alta voce?"


translate italian day4_fail_14b6dd58:


    th "Ma non mi sembra una grande idea."


translate italian day4_fail_7fff55da:


    "Mi sono seduto su una panca nella sala e ho sospirato frustrato."


translate italian day4_fail_4d3113b1:


    me "Fantasmi, ma dove siete?"


translate italian day4_fail_9d6a7df4:


    "Ho chiesto al vecchio campo."


translate italian day4_fail_7539188b:


    "E improvvisamente ho notato una strana cavità in un angolo della stanza."


translate italian day4_fail_3f4305c5:


    "Una piccola botola – un adulto ci sarebbe passato a malapena."


translate italian day4_fail_1154aac9:


    "E sembrava proprio che qualcuno fosse passato di lì, dato che la polvere attorno ad essa era stata pulita."


translate italian day4_fail_70f0a211:


    "Ed ecco qui il punto in cui avrei dovuto iniziare ad avere paura."


translate italian day4_fail_4b219aad:


    th "Allora Shurik è andato davvero da qualche parte…"


translate italian day4_fail_8e98e395:


    th "Cosa potrebbe essere in agguato sotto a quella botola?{w} Una cantina?{w} Che cosa starebbe facendo Shurik laggiù?"


translate italian day4_fail_f1c33359:


    "Ho tirato la botola verso l'alto aprendola, e ho puntato la torcia verso l'oscurità."


translate italian day4_fail_d6d55fdd:


    "C'era una scala che scendeva per un paio di metri, finendo su una pavimentazione in cemento."


translate italian day4_fail_f98d9bf1:


    "C'era un basso e stretto corridoio che si estendeva nell'oscurità."


translate italian day4_fail_da411e3b:


    "Ho esitato a lungo."


translate italian day4_fail_b42ebb80:


    "Da un lato non avevo proprio voglia di scendere là sotto, ma dall'altro, se davvero Shurik fosse andato lì…"


translate italian day4_fail_84882d7a:


    "Ho imprecato ad alta voce e ho iniziato a scendere."


translate italian day4_fail_ddec5394:


    "Laggiù faceva così buio che persino la torcia non mi aiutava molto."


translate italian day4_fail_30592890:


    "Uno stretto corridoio, cavi che si estendevano lungo le pareti, lampade rotte rivestite da sbarre di ferro."


translate italian day4_fail_43e505f2:


    th "L'ho già visto da qualche parte..."


translate italian day4_fail_1ed31216:


    "Lentamente ho iniziato ad addentrarmi in quel labirinto."


translate italian day4_fail_bd147fc2:


    "Il percorso proseguiva in profondità e dopo una dozzina di metri terminava con una scala dai gradini ripidi."


translate italian day4_fail_af70ec38:


    "Mi sono trovato davanti a un'enorme porta di metallo."


translate italian day4_fail_2c241583:


    "C'era un simbolo disegnato su di essa."


translate italian day4_fail_39f74a0e:


    "L'ho guardato meglio e mi sono accorto che era il simbolo di rischio biologico."


translate italian day4_fail_a60db0bf:


    th "Non mi stupisce – il vecchio campo è stato costruito appena dopo la guerra ed è durato fino agli anni Settanta."


translate italian day4_fail_2bd94a6b:


    th "Quindi si tratta di un semplice rifugio antiaereo, nulla di cui preoccuparsi…"


translate italian day4_fail_d224dd30:


    "Anche se quei pensieri mi rendevano ancora più teso."


translate italian day4_fail_4b931dd1:


    "Mi sono deciso e ho aperto la porta..."


translate italian day4_fail_d415bd1a:


    "...che immediatamente si è richiusa con un tonfo non appena ho varcato la soglia."


translate italian day4_fail_240fda6a:


    "Ho rabbrividito vistosamente, le mie mani hanno cominciato a tremare e la mia vista si è oscurata."


translate italian day4_fail_8247734a:


    "Ho ripreso i sensi solo dopo qualche secondo, e solo allora mi sono accorto che apparentemente a quella porta era attaccata una forte molla."


translate italian day4_fail_9e97fbcb:


    "Mi sono ritrovato in una stanza piuttosto spaziosa."


translate italian day4_fail_0ba401c8:


    "Sono riuscito a scorgere un letto a castello, alcuni armadi lungo le pareti, e diversi dispositivi sconosciuti collocati in un angolo della stanza."


translate italian day4_fail_a1458d50:


    "Sembrava che questo rifugio non fosse stato mai utilizzato…"


translate italian day4_fail_21c3af84:


    "Nessuno ci è mai entrato.{w} I letti erano fatti."


translate italian day4_fail_274647ee:


    "Il che era strano di per sé…"


translate italian day4_fail_fd3face8:


    "Ho esaminato attentamente ogni angolo della stanza."


translate italian day4_fail_6d5a6727:


    "Gli armadietti erano pieni di maschere antigas, tute di contenimento, varie stranezze domestiche e altre cose."


translate italian day4_fail_76cae0d1:


    "I dispositivi che avevo notato prima e che mi parevano sconosciuti erano in realtà dei misuratori di radiazioni, di pressione atmosferica e di temperatura."


translate italian day4_fail_8c887cf9:


    "In poche parole, era un tipico rifugio antiaereo."


translate italian day4_fail_6cf7d00d:


    "Non ci ero mai stato prima, ma era così che me lo immaginavo."


translate italian day4_fail_1aa39f51:


    "L'unica cosa ancora inesplorata era quella porta all'altra estremità della stanza."


translate italian day4_fail_efc3a1da:


    "Ho cercato di aprirla, ma non ci sono riuscito."


translate italian day4_fail_24bba245:


    th "Se qualcuno di recente fosse venuto quaggiù, e non lo si fosse trovato in questa stanza, allora sarebbe logico pensare che si trovi dietro a questa porta."


translate italian day4_fail_b7f23876:


    "L'ho chiamato sussurrando:"


translate italian day4_fail_c088b6e1:


    me "Shurik...?"


translate italian day4_fail_dfffcbac:


    "Nessuna risposta."


translate italian day4_fail_f8c98a55:


    th "Forse si è chiuso dentro…{w} Ma perché?"


translate italian day4_fail_234abe88:


    "Non sapevo cosa fare esattamente."


translate italian day4_fail_d965f701:


    "Forse sarebbe stato più saggio tornare l'indomani con i rinforzi, ma per qualche ragione avevo la sensazione che dietro a quella porta avrei trovato le risposte che non vedevano l'ora di farsi trovare."


translate italian day4_fail_6f36167b:


    "Ho attraversato la stanza e ho raccolto un piede di porco appoggiato in un angolo."


translate italian day4_fail_88aff48d:


    "L'ho incastrato sotto la parte inferiore della porta e ho cercato di strapparla dai suoi cardini."


translate italian day4_fail_548728ed:


    "Stranamente, dopo un attimo ce l'ho fatta; fortunatamente non era grossa e pesante come l'altra porta."


translate italian day4_fail_90069d4f:


    "La porta ha ceduto ed è piombata sul pavimento con un tonfo."


translate italian day4_fail_eb8bd4b5:


    "Ho fatto un grande respiro e ho puntato la torcia in quel passaggio."


translate italian day4_fail_55ed8e88:


    "Un altro corridoio scompariva nell'oscurità, proprio come quello che mi aveva portato qui."


translate italian day4_fail_a20cefa7_3:


    "..."


translate italian day4_fail_9276a63c:


    "Le pareti si stringevano sempre più, la mia testa quasi raggiungeva il soffitto, ma il corridoio era interminabile."


translate italian day4_fail_709d42b2:


    "Ho camminato con cautela, guardando dove mettevo i piedi. E proprio grazie a questo non ho rischiato di cadere in una grossa buca…"


translate italian day4_fail_3903f0e0:


    "La buca aveva un diametro di circa un metro e mezzo, e al di sotto di essa c'era della terra invece del cemento."


translate italian day4_fail_9b16cd8e:


    "Forse è stata causata da un'esplosione."


translate italian day4_fail_80434573:


    th "Non è troppo profonda – potrò tornare su facilmente."


translate italian day4_fail_cf5b6c1b:


    "Dopo essere saltato di sotto, mi sono ritrovato in una specie di vecchia miniera."


translate italian day4_fail_1a034f9f:


    "Le pareti e il soffitto erano rinforzati con delle travi di legno."


translate italian day4_fail_4bec4ed2:


    "Il tunnel era così lungo che la luce della mia torcia non era sufficiente a raggiungere l'estremità opposta…"


translate italian day4_fail_a20cefa7_4:


    "..."


translate italian day4_fail_8e2bc52d:


    "Pochi minuti più tardi mi sono trovato davanti a un bivio."


translate italian day4_fail_68ebea2c:


    "Era troppo pericoloso proseguire oltre, mi sarei potuto perdere."


translate italian day4_fail_0ef45387:


    "Ho deciso che fosse saggio marcare la parete in modo da poter poi ritrovare la strada, e quindi ho raccolto una pietra e ho inciso una grossa tacca sulla parete."


translate italian day4_fail_7dd017ff:


    th "Quindi..."


translate italian mine_crossroad_07a69b74:


    "Sembra esserci un altro bivio."


translate italian mine_crossroad_ff6d6d5f:


    "Sembra esserci un altro bivio."


translate italian fail_mine_return_to_start_fda95a66:


    "Sono ritornato al punto di partenza."


translate italian fail_mine_return_to_start_d452f00a:


    th "Quindi devo aver preso la direzione sbagliata da qualche parte."


translate italian fail_mine_return_to_start_2bce21f4:


    th "Continuiamo a cercare."


translate italian girls_mine_return_to_start_f89efc62:


    "Siamo ritornati al punto di partenza."


translate italian girls_mine_return_to_start_c806e46d:


    th "Quindi dobbiamo aver preso la direzione sbagliata da qualche parte."


translate italian girls_mine_return_to_start_2bce21f4:


    th "Continuiamo a cercare."


translate italian fail_mine_coalface_61204914:


    "Ero stato qui appena pochi minuti fa."


translate italian fail_mine_coalface_31787369:


    "Alla fine sono uscito dal tunnel e mi sono ritrovato in una sala piuttosto grande, con un alto soffitto."


translate italian fail_mine_coalface_3ef8c583:


    "Anche se non la si sarebbe potuta chiamare sala; dovevano averla impiegata per l'estrazione di qualcosa.{w} Probabilmente carbone, oppure oro."


translate italian fail_mine_coalface_3c54fe69:


    "Le pareti erano state tagliate da picconi o da martelli pneumatici."


translate italian fail_mine_coalface_3e6cec54:


    "In questo posto era buio pesto, quindi la mia unica salvezza era la torcia."


translate italian fail_mine_coalface_ffe6f7a7:


    th "Se si rompesse, allora sarebbe molto improbabile riuscire a tornare in superficie…"


translate italian fail_mine_coalface_58411121:


    "Il suo raggio di luce improvvisamente ha rivelato un pezzo di stoffa rosso."


translate italian fail_mine_coalface_98466bb2:


    "Un foulard!"


translate italian fail_mine_coalface_96b3272c:


    "Ormai era chiaro che Shurik fosse nei dintorni."


translate italian fail_mine_coalface_93ec21f7:


    me "Shurik! Shurik!"


translate italian fail_mine_coalface_f23d45c0:


    "Solo il mio eco mi ha risposto."


translate italian fail_mine_coalface_cc8806c6:


    th "Ma dove potrebbe essere andato?"


translate italian fail_mine_coalface_db4b78eb:


    "Non c'era altra via d'uscita."


translate italian fail_mine_coalface_33bf97c7:


    th "Forse ci sono altri luoghi in questi tunnel che non ho ancora esplorato…"


translate italian fail_mine_coalface_858b6cbe:


    th "Quindi dovrei continuare a cercare!"


translate italian fail_mine_halt_61204914:


    "Sono stato qui appena pochi minuti fa."


translate italian fail_mine_halt_739fe800:


    "Sono arrivato a una specie di accampamento dei minatori."


translate italian fail_mine_halt_9f9a3875:


    "Picconi ed elmetti sparsi ovunque, e un carro arrugginito in un angolo."


translate italian fail_mine_halt_a2b01ca8:


    "Tutti gli oggetti qui erano così datati – sembravano essere dell'inizio del Ventesimo secolo piuttosto che di metà secolo."


translate italian fail_mine_halt_631320fe:


    th "E quindi questa è proprio una miniera…{w} E ciò significa che potrei girare a vuoto per anni senza trovare una via d'uscita."


translate italian fail_mine_halt_1efc66da:


    "Ma sentivo che l'uscita era qui vicino, e quindi ho proseguito."


translate italian girls_mine_halt_e40597aa:


    "Siamo già stati qui pochi minuti fa."


translate italian girls_mine_halt_498e0e02:


    "Siamo arrivati a una specie di accampamento di minatori."


translate italian girls_mine_halt_9f9a3875:


    "Picconi ed elmetti sparsi ovunque, e un carro arrugginito in un angolo."


translate italian girls_mine_halt_a2b01ca8:


    "Tutti gli oggetti qui erano così datati – sembravano essere dell'inizio del Ventesimo secolo piuttosto che di metà secolo."


translate italian girls_mine_halt_631320fe:


    th "E quindi questa è proprio una miniera…{w} Il che significa che potremmo girare a vuoto per secoli senza riuscire a trovare una via d'uscita."


translate italian girls_mine_halt_910be1a3:


    "Ma avevo la sensazione che l'uscita fosse nelle vicinanze, e quindi abbiamo proseguito."


translate italian fail_mine_miku_29f8f3fe:


    "Sono andato avanti e ancora avanti..."


translate italian fail_mine_miku_08cda699:


    "Sembrava che questi tunnel fossero senza fine, come se fossi stato intrappolato nel labirinto del Minotauro e la terribile creatura potesse apparire da un momento all'altro..."


translate italian fail_mine_miku_6dda7c89:


    "Ero esausto e mi sono accasciato lentamente a terra, con la schiena appoggiata all'umida parete della miniera."


translate italian fail_mine_miku_98a75609:


    th "Ma dov'è quel dannato Shurik?"


translate italian fail_mine_miku_db9b9ede:


    th "E per quante ore ho vagato in questi corridoi?"


translate italian fail_mine_miku_93a335d8:


    "I miei occhi hanno iniziato a chiudersi."


translate italian fail_mine_miku_154d0a94:


    th "Solo per un istante..."


translate italian fail_mine_exit_c43624ee:


    "Dietro all'ultimo angolo una porta è apparsa nel buio."


translate italian fail_mine_exit_2236a915:


    "L'ho aperta e sono entrato in una piccola stanza."


translate italian fail_mine_exit_50ed1d0a:


    "Era completamente vuota, eccetto per…"


translate italian fail_mine_exit_6c35e784:


    "...Shurik, che se ne stava seduto in un angolo!"


translate italian fail_mine_exit_b01a7f13:


    me "Shurik!"


translate italian fail_mine_exit_bfe85680:


    "Mi ha fissato con lo sguardo pieno di terrore."


translate italian fail_mine_exit_2cfb869c:


    sh "AAH! Non avvicinarti!"


translate italian fail_mine_exit_f192c172:


    me "Shurik, sono io! Semyon!"


translate italian fail_mine_exit_fe05dcbb:


    sh "Non mi arrenderò così facilmente!"


translate italian fail_mine_exit_962a501b:


    "Ha afferrato una grossa barra di metallo dietro la sua schiena e ha cominciato a volteggiarla nell'aria!"


translate italian fail_mine_exit_ce533195:


    me "Shurik, che ti succede?! Calmati!"


translate italian fail_mine_exit_1a0db18d:


    sh "Prima mi porti qui! Poi mi fai girare a vuoto per tutta la miniera! E ora fingi di essere Semyon!"


translate italian fail_mine_exit_f7d0be86:


    th "Dev'essere andato completamente fuori di testa…"


translate italian fail_mine_exit_95727752:


    me "Shurik! Calmati! Nessuno sta fingendo di essere me! Sono davvero io!"


translate italian fail_mine_exit_d566ef9e:


    "Ho cercato di usare il tono più calmo possibile, ma ero quasi più nervoso di lui."


translate italian fail_mine_exit_c8104f36:


    sh "Semyon?"


translate italian fail_mine_exit_c9c76fb6:


    "Ha strizzato gli occhi."


translate italian fail_mine_exit_e5f3e2a0:


    sh "Sei davvero tu?"


translate italian fail_mine_exit_6034747a:


    me "Certo! E adesso dimmi cosa è successo. Come sei finito qui?"


translate italian fail_mine_exit_d1fc8105:


    "Shurik ha ripreso fiato e ha iniziato a parlare:"


translate italian fail_mine_exit_b0096a58:


    sh "Mi servivano dei componenti per il robot.{w} E ho sentito dire che c'era un rifugio antiaereo nel vecchio campo…{w} E che potevano esserci delle attrezzature qui."


translate italian fail_mine_exit_400eb8c8:


    sh "Così mi sono alzato presto la mattina e sono venuto qui…"


translate italian fail_mine_exit_04e9519a:


    "Si è fermato per un momento, cercando di affrontare il tremore delle sue mani."


translate italian fail_mine_exit_c71c2b1c:


    sh "Ho trovato delle parti utili ma poi…"


translate italian fail_mine_exit_3fd0e1a0:


    "Ho intravisto una paura primordiale nei suoi occhi."


translate italian fail_mine_exit_3bf32b52:


    sh "Quelle voci… Mi dicevano di venire qui… Sono sceso nella miniera… Poi mi dicevano di andare a destra, poi a sinistra, poi a destra, poi sinistra, destra, sinistra…"


translate italian fail_mine_exit_0c32fa11:


    "Ha fatto una risata isterica."


translate italian fail_mine_exit_3edbb0d2:


    sh "Ma adesso non mi prenderanno più! Me ne starò nascosto qui! Non mi troveranno!"


translate italian fail_mine_exit_8453558b:


    "Ha cominciato ad agitare nuovamente la barra nell'aria."


translate italian fail_mine_exit_f1661152:


    me "Shurik… Non c'è nessuno qui… Ho vagato per un sacco di tempo nella miniera come hai fatto tu, ma non ho sentito alcuna voce."


translate italian fail_mine_exit_2c5335a6:


    "Ha fissato lo sguardo su di me."


translate italian fail_mine_exit_0495a2f5:


    sh "Allora tu sei con loro!"


translate italian fail_mine_exit_fabedc6a:


    "Shurik si è precipitato verso di me."


translate italian fail_mine_exit_4dcbc4d2:


    "L'ho schivato per un pelo."


translate italian fail_mine_exit_4ca30280:


    "Ma la mia torcia non ha avuto la stessa fortuna – e ha assorbito in pieno il colpo di Shurik."


translate italian fail_mine_exit_973c06e7:


    "Ci siamo ritrovati nell'oscurità più totale."


translate italian fail_mine_exit_9978cb5a:


    "Adesso toccava a me perdere la testa…"


translate italian fail_mine_exit_6eeebb1a:


    me "Che cos'hai fatto, idiota?!"


translate italian fail_mine_exit_da6ba350:


    "Mi sono scagliato nella direzione dove credevo fosse Shurik, ma senza alcun risultato."


translate italian fail_mine_exit_68d73b99:


    me "Adesso moriremo entrambi qui sotto!"


translate italian fail_mine_exit_e68105fc:


    "Non ho ricevuto alcuna risposta da parte sua, ma soltanto una silenziosa risata isterica."


translate italian fail_mine_exit_222240df:


    sh "Va bene… Va tutto bene…{w} So dov'è l'uscita! Seguimi!"


translate italian fail_mine_exit_1b5cf4c1:


    me "Seguirti dove?! Se non posso vedere un accidente!"


translate italian fail_mine_exit_78e3c7d3:


    sh "Segui la mia voce."


translate italian fail_mine_exit_05321f51:


    "Non avevo altra scelta."


translate italian fail_mine_exit_e7e27795:


    sh "Qui…"


translate italian fail_mine_exit_013b19be:


    "La voce di Shurik è risuonata da qualche parte più avanti."


translate italian fail_mine_exit_cb4b20ed:


    me "Come fai a sapere dov'è l'uscita?"


translate italian fail_mine_exit_c98b8e2a:


    sh "Ho camminato a lungo per questi corridoi…"


translate italian fail_mine_exit_0cd4b3c9:


    me "E allora perché non te ne sei andato?"


translate italian fail_mine_exit_88482955:


    sh "Le voci…{w} Fa' silenzio! Potrebbero sentirci!"


translate italian fail_mine_exit_1e45ae53:


    "In teoria potevo anche credere che vi fossero delle forze soprannaturali in questo luogo."


translate italian fail_mine_exit_54f45c90:


    "Al campo «Sovyonok» queste cose erano all'ordine del giorno."


translate italian fail_mine_exit_06e526d9:


    "Ma non c'era ancora alcun motivo di farsi prendere dal panico."


translate italian fail_mine_exit_e559fc54:


    "Anche se le mie mani stavano tremando incessantemente."


translate italian fail_mine_exit_e7e27795_1:


    sh "Qui…"


translate italian fail_mine_exit_695378fd:


    "Seguivo la sua voce in silenzio, e ben presto ho perso la cognizione del tempo."


translate italian fail_mine_exit_a5d3cc23:


    "Abbiamo vagato per qualche minuto… o forse per qualche ora."


translate italian fail_mine_exit_3dae981c:


    "Alla fine una luce fioca è apparsa in lontananza."


translate italian fail_mine_exit_a370b3b4:


    sh "Ci siamo…"


translate italian fail_mine_exit_9a15b181:


    "Abbiamo raggiunto una parete.{w} La nostra via verso la libertà era bloccata da una grata sul soffitto."


translate italian fail_mine_exit_0b2dcdb3:


    "Mi sono tirato su e ho riconosciuto la piazza oltre quella grata, per quanto si riuscisse a vedere."


translate italian fail_mine_exit_49341337:


    me "Sollevami!"


translate italian fail_mine_exit_56091405:


    "Shurik ha obbedito."


translate italian fail_mine_exit_f0562a64:


    "Ho cominciato a colpire la grata con tutte le mie forze, tanto che dopo qualche colpo le mie mani si sono ricoperte tutte di sangue…"


translate italian fail_mine_exit_c8bf1db3:


    "Ma almeno la grata ha ceduto e sono riuscito a rimuoverla."


translate italian fail_mine_exit_8a62a2ef:


    "Dopo un minuto ci siamo ritrovati in superficie dietro al monumento di Genda."


translate italian fail_mine_exit_aefc775f:


    "Quando all'inizio avevo visto quella grata nella piazza non avrei mai immaginato che ci potesse essere un tunnel segreto sotto di essa…"


translate italian fail_mine_exit_96505b67:


    sh "Verranno a prenderti!"


translate italian fail_mine_exit_0735175d:


    "Mi sono accasciato a terra, esausto."


translate italian fail_mine_exit_a444ac12:


    me "Chi?"


translate italian fail_mine_exit_823b8704:


    sh "Verranno!"


translate italian fail_mine_exit_0f567504:


    "Mi ha detto di nuovo Shurik, e se n'è andato…"


translate italian fail_mine_exit_7a6522f6:


    "Ero talmente sfinito che non riuscivo nemmeno a pensare a delle fantasiose voci, al campo abbandonato, al rifugio antiaereo, o alla miniera."


translate italian fail_mine_exit_4d8a8dd9:


    "Quasi inconscio, ho raggiunto la casetta della leader…"


translate italian fail_mine_exit_6363b20e:


    "Ma non sono riuscito ad andare a letto subito."


translate italian fail_mine_exit_f6e23c0c:


    mt "Ma guarda chi è tornato!"


translate italian fail_mine_exit_f10bec26:


    me "Olga Dmitrievna..."


translate italian fail_mine_exit_ea404d77:


    "Le ho detto stancamente, cercando di limitare la spiegazione al minimo indispensabile."


translate italian fail_mine_exit_1e3c4bff:


    mt "Mi avevi detto che avresti fatto una «passeggiata», e invece...!"


translate italian fail_mine_exit_ffab041d:


    mt "E io... Stavo per chiamare la polizia!{w} E..."


translate italian fail_mine_exit_4370a259:


    me "Olga Dmitrievna, non possiamo rimandare a domani? Sono molto, molto stanco."


translate italian fail_mine_exit_dfbc9991:


    "La leader del Campo mi ha squadrato dalla testa ai piedi."


translate italian fail_mine_exit_7e1d09c7:


    mt "Ok, va bene..."


translate italian fail_mine_exit_5344f89f:


    "Sono rimasto sorpreso del fatto che fosse stato così facile, ma non volevo sprecare quell'occasione e quindi ho spento rapidamente la luce e mi sono infilato sotto le coperte."


translate italian fail_mine_exit_74343bec:


    "E poi la ragazza della miniera è apparsa proprio davanti ai miei occhi."


translate italian fail_mine_exit_4ae428ab:


    "L'ho vista così chiaramente che sembrava fosse proprio davanti al mio letto."


translate italian fail_mine_exit_5fc49513:


    th "No, è solo la mia immaginazione..."


translate italian fail_mine_exit_16730ca7:


    th "Ma comunque, perché non ho avuto paura laggiù? Mi ero semplicemente avvicinato a lei, parlando normalmente."


translate italian fail_mine_exit_ee91ad9c:


    th "Tutte le risposte sono qui. La ragazza con le orecchie di gatto deve sapere per forza qualcosa."


translate italian fail_mine_exit_0011fed6:


    "Forse ero troppo stanco a causa del vecchio campo, della miniera, del labirinto, e i miei nervi erano a pezzi."


translate italian fail_mine_exit_e12babdb:


    th "Avrei dovuto chiederle qualcosa..."


translate italian fail_mine_exit_4ab8d03c:


    th "Ma ho ancora tempo..."


translate italian day4_uv_c936a5db:


    "Dietro all'ultimo angolo la mia torcia ha rivelato una grande ombra."


translate italian day4_uv_df679a7b:


    "Ero pietrificato dalla paura e non sono riuscito a fare un singolo passo."


translate italian day4_uv_52523f03:


    "Mi sono ripreso dal mio torpore solo pensando al fatto che potesse essere Shurik."


translate italian day4_uv_b01a7f13:


    me "Shurik!"


translate italian day4_uv_f8c43a30:


    "Nessuna risposta…"


translate italian day4_uv_213cd37d:


    "L'ombra è sparita dietro l'angolo, e ho intravisto una sfocata sagoma umana..."


translate italian day4_uv_59b5788c:


    "Qualcuno stava seduto su una roccia.{w} O forse qualcosa..."


translate italian day4_uv_480f771d:


    "Stavo morendo dalla paura."


translate italian day4_uv_2821e6eb:


    me "Cosa ci fai qui?"


translate italian day4_uv_51e9a976:


    "Ho sentito una voce dolce:"


translate italian day4_uv_6add52f7:


    uvp "Sto cacciando i topi."


translate italian day4_uv_88e92f6a:


    "Ho aperto la bocca…{w} ma non sono riuscito a dire nulla."


translate italian day4_uv_51149e68:


    me "…"


translate italian day4_uv_c7528539:


    uvp "Per caso ne hai visto qualcuno?"


translate italian day4_uv_d695b46b:


    me "No, ma sono sicuro che ce ne sono un sacco qui…"


translate italian day4_uv_a4cdf691:


    "Ho puntato il fascio di luce sul suo volto..."


translate italian day4_uv_3abd8a8c:


    "O c'era qualcosa di sbagliato nella sua testa, o nella sua specie biologica. A giudicare da quelle orecchie e dalla coda, non sembrava proprio un'umana."


translate italian day4_uv_157dacf2:


    me "E quanti ne hai presi?"


translate italian day4_uv_02281cca:


    uvp "Ancora nessuno."


translate italian day4_uv_3d4f4933:


    "Ha risposto in modo serio."


translate italian day4_uv_e2d148b8:


    "Non sembrava ostile. Il mio istinto mi diceva che non dovevo aver timore di lei, ma allo stesso tempo non avevo proprio idea di cosa chiederle, come chiederglielo, chi fosse lei, o cosa stesse facendo."


translate italian day4_uv_5be41974:


    "Ma d'altra parte, incontrare improvvisamente qualcuno in una miniera abbandonata potrebbe provocare un attacco di cuore a causa dello spavento."


translate italian day4_uv_11cfe5f6:


    "E quella ragazza decisamente non era un pioniere del Campo!"


translate italian day4_uv_3c5fb85c:


    me "E tu... Chi sei?"


translate italian day4_uv_2372d07d:


    uvp "Probabilmente stai cercando {i}lui{/i}..."


translate italian day4_uv_2a985733:


    me "Lui chi?"


translate italian day4_uv_b9a21822:


    uvp "Quel ragazzo..."


translate italian day4_uv_ea6d6d42:


    me "Beh…{w} Sì…"


translate italian day4_uv_8fd15dcf:


    "Ho risposto dopo una breve pausa."


translate italian day4_uv_6bae3f57:


    me "Comunque..."


translate italian day4_uv_e657d961:


    "La ragazza mi ha interrotto:"


translate italian day4_uv_9d150002:


    uvp "Ha girovagato qui per molto tempo.{w} Ho provato a parlargli, ma non riusciva a sentirmi."


translate italian day4_uv_5c1c58e4:


    me "E adesso dov'è?!"


translate italian day4_uv_813a06f8:


    uvp "Non lo so…{w} Forse sta ancora vagando da qualche parte."


translate italian day4_uv_e48430c4:


    "Pareva non essere per nulla interessata alla nostra conversazione."


translate italian day4_uv_75223819:


    me "E tu…{w} Come hai fatto…{w} ad arrivare qui?"


translate italian day4_uv_b4a3ab00:


    uvp "Allo stesso modo in cui sei arrivato tu. Dalla superficie."


translate italian day4_uv_1654f26c:


    me "Senza una torcia?"


translate italian day4_uv_f2374f2b:


    uvp "E a cosa mi serve?"


translate italian day4_uv_d222fc60:


    "Mi ha guardato perplessa."


translate italian day4_uv_2250eefe:


    "Non ci capivo più niente."


translate italian day4_uv_eec7a97d:


    "La mia torcia si stava scaricando. Terrorizzato, ho cercato di darle qualche colpetto."


translate italian day4_uv_a5894410:


    "Quando la luce è tornata normale, la ragazza era sparita…"


translate italian day4_uv_38eb0973:


    th "Non so chi o cosa essa sia, ma sembra che lei possa avere le risposte che cerco."


translate italian day4_uv_4173197c:


    "La cosa più strana è che non ero spaventato da lei. Semplicemente sentivo che quella ragazza non mi avrebbe fatto del male."


translate italian day4_uv_9e917b0c:


    "Ma adesso dovevo trovare Shurik!"


translate italian day4_uv_8288c2fa:


    th "Ho come la sensazione che rivedrò ancora quella ragazza..."


translate italian day4_uv_53d1fea3:


    "Non so perché, ma ne ero sicuro al cento percento."


translate italian day4_sl_45708cef:


    sl "Andrò io con lui!"


translate italian day4_sl_678c7ff3:


    mt "Molto bene! È meglio essere in due."


translate italian day4_sl_238ce8d6:


    me "Sei sicura?"


translate italian day4_sl_30273633:


    "Ho chiesto a Slavya in un sussurro."


translate italian day4_sl_ec7a09a0:


    "Volevo esserne sicuro, ma..."


translate italian day4_sl_bf4362df:


    "Non mi ha risposto, ha solo sorriso."


translate italian day4_sl_3c3e2229:


    "Ho guardato Slavya in silenzio per qualche istante."


translate italian day4_sl_007619f0:


    th "Perché vuole sempre aiutarmi?"


translate italian day4_sl_d905d2f2:


    mt "Molto bene! Allora auguro buona fortuna ad entrambi."


translate italian day4_sl_b904fd9e:


    me "Ne avremo bisogno…"


translate italian day4_sl_ed27161f:


    "La leader del Campo e le altre ragazze ci hanno salutato e se ne sono andate per i fatti loro."


translate italian day4_sl_13a18ea4:


    "Ero abbastanza sorpreso che loro stessero considerando la nostra passeggiata notturna…{w} attraverso la foresta…{w} fino al campo abbandonato, come qualcosa di normale."


translate italian day4_sl_0bfe3d69:


    th "Beh… non ci posso fare niente."


translate italian day4_sl_2dbd94e3:


    sl "Aspetta un minuto, vado a prendere una torcia elettrica."


translate italian day4_sl_1279b922:


    th "Una torcia, buona idea..."


translate italian day4_sl_078dc3eb:


    "Mentre stavo aspettando Slavya, ho avuto uno strano pensiero."


translate italian day4_sl_9047a687:


    th "Perché lei è sempre così gentile, simpatica e desiderosa di aiutare?"


translate italian day4_sl_78b4cda5:


    th "Perché ha accettato di andare con me verso l'ignoto?"


translate italian day4_sl_6e630e74:


    th "Sono sempre quelli tranquilli che…"


translate italian day4_sl_07db949a:


    th "Dovrei forse aver paura di lei?"


translate italian day4_sl_0cebc928:


    sl "Sono tornata!"


translate italian day4_sl_f1ebc68b:


    "Slavya mi ha consegnato una torcia elettrica, sorridendo."


translate italian day4_sl_ad3d2e0c:


    sl "Andiamo?"


translate italian day4_sl_7bd87323:


    "Secondo quanto detto da Electronik, il vecchio edificio fu costruito appena dopo la guerra."


translate italian day4_sl_fd3c1cdc:


    "Era come una scuola dell'infanzia (o forse una caserma, ho pensato), e decisamente non era abbastanza grande da poter contenere tutti i pionieri di questo Campo."


translate italian day4_sl_c4f7341a:


    "Era abbandonato da una ventina d'anni…"


translate italian day4_sl_a52ca566:


    "L'oscurità è calata su di noi."


translate italian day4_sl_3868483b:


    "Sembrava che la foresta avesse paura di noi così come noi avevamo paura di lei.{w} Gli alberi si scostavano cortesemente, mettendosi ai lati del sentiero e osservandoci con pazienza."


translate italian day4_sl_59e3e4f7:


    "Un gufo ha trafitto il silenzio della notte da qualche parte nella foresta, ma più silenziosamente del solito."


translate italian day4_sl_6dd60970:


    "Non avevo paura camminando accanto a Slavya."


translate italian day4_sl_560f6e52:


    "Probabilmente in qualsiasi altra situazione, persino nel mondo reale, avrei avuto troppa paura per potermi addentrare in una foresta senza un percorso ben definito."


translate italian day4_sl_be70614e:


    "Ma ho cercato di liberarmi da tali pensieri."


translate italian day4_sl_e29f96bd:


    "Benché fosse strano, Slavya probabilmente ha placato il mio istinto di autoconservazione e la mia percezione della realtà è stata attenuata."


translate italian day4_sl_78eb5981:


    "Se le cose fossero andate avanti in quel modo, probabilmente mi sarei imbattuto in una bestia avendo il sorriso stampato sul volto…"


translate italian day4_sl_0b419961:


    me "Non hai per niente paura, vero?"


translate italian day4_sl_63f53767:


    sl "Cioè?"


translate italian day4_sl_ab195626:


    "Slavya si è girata e mi ha guardato, sorridendo in modo strano, forse per incomprensione o forse come una leggera presa in giro.{w} O forse mi stavo immaginando le cose."


translate italian day4_sl_d91626f2:


    me "Beh… La notte, la foresta, il vecchio campo…"


translate italian day4_sl_1bd8d8f4:


    sl "Sì, mi mette un po' i brividi."


translate italian day4_sl_cc73ad1f:


    me "Solo un po'?"


translate italian day4_sl_1b5187ef:


    sl "Cosa c'è, hai paura?"


translate italian day4_sl_8e0381aa:


    me "No… Cioè… Sì, ma è una reazione normale in questo tipo di situazione."


translate italian day4_sl_4891112d:


    "Non volevo mostrare la mia paura, ma in realtà non ero spaventato. Pensavo solo che avere paura fosse una reazione normale in questi casi."


translate italian day4_sl_9f24fbc1:


    "Era come se il mio corpo avesse smesso di generare anticorpi per combattere un'infezione.{w} Avevo paura non della foresta notturna, ma del fatto che stessi reagendo nel modo sbagliato a ogni cosa che accadeva."


translate italian day4_sl_f337bb5a:


    "D'altra parte, non volevo apparire come uno stupido."


translate italian day4_sl_01e9adbb:


    sl "Capisco."


translate italian day4_sl_4fb83426:


    "Slavya ha sorriso di nuovo e ha continuato a camminare."


translate italian day4_sl_a20cefa7:


    "..."


translate italian day4_sl_68cdaa0d:


    "Il sentiero, circondato da alberi e cespugli, stava diventando sempre più stretto."


translate italian day4_sl_42ad9c81:


    "Più ci spingevamo nella foresta, meno chiaramente riuscivamo a vedere la luna."


translate italian day4_sl_eb593e12:


    "Per un momento le nubi l'hanno coperta del tutto, facendoci piombare in un buio quasi totale."


translate italian day4_sl_cbdffea8:


    "Ho chiuso i miei occhi, e quando li ho riaperti la luna era ricomparsa, dipingendo l'ambiente circostante con il suo soffice tocco luminoso."


translate italian day4_sl_50e0695f:


    th "Ho appena scoperto di odiare l'oscurità!"


translate italian day4_sl_368ffb97:


    "Finalmente gli alberi si stavano diradando, e un minuto più tardi eravamo ai margini di una radura…"


translate italian day4_sl_92efe0b6:


    "C'era un vecchio edificio fatiscente, eroso da pioggia, ruggine e insetti."


translate italian day4_sl_fb10b433:


    "Ci ha sorriso in modo sinistro, con quelle sue finestre rotte che sembravano una bocca sdentata. Era come un silente rimprovero da parte dei morti nei confronti dei vivi."


translate italian day4_sl_b33715a3:


    "Una nebbia cimiteriale offuscava l'intera radura, permettendo a fantasmi, demoni, zombi e diavoli di incombere dalla foschia."


translate italian day4_sl_28fa12ec:


    "Quello spettacolo avrebbe potuto provocare un attacco di cuore a una persona normale, o almeno un attacco di panico."


translate italian day4_sl_f4b1d738:


    "Se Slavya non mi avesse preso per mano, sicuramente non sarei riuscito a sopportare tutto questo…"


translate italian day4_sl_0ab7ae30:


    sl "Così sarà meno spaventoso…"


translate italian day4_sl_06ab0532:


    "Ha sorriso."


translate italian day4_sl_9cfcb779:


    "Non sapevo cosa dire, e quindi mi sono limitato a tenerla stretta per mano."


translate italian day4_sl_f65f03d8:


    th "Ma che cosa ci faccio qui?{w} Cercando Shurik di notte nella foresta, {i}in questo luogo{/i}?!"


translate italian day4_sl_5cc597fa:


    th "Come ho potuto accettare?{w} Nessuna persona normale l'avrebbe fatto…"


translate italian day4_sl_3a771d0d:


    th "Ma il mondo attorno a me non può di certo essere considerato normale."


translate italian day4_sl_f86ea402:


    "Ho cercato di convincere me stesso che le risposte si sarebbero rivelate di lì a poco, e che non sarebbe potuta andare peggio di così."


translate italian day4_sl_0ef9ad1a:


    "Anche se a dire il vero, non è che mi fosse successo qualcosa di brutto in questi quattro giorni. E comunque, non c'era ragione di pensare che sarebbe accaduto qualcosa di brutto."


translate italian day4_sl_4ddccd16:


    "Forse è così che gira il mondo – la gente non può vivere senza avventure?"


translate italian day4_sl_ad3d2e0c_1:


    sl "Andiamo?"


translate italian day4_sl_9776d559:


    me "Uhm, sì, suppongo…"


translate italian day4_sl_0f0cb0fd:


    "Ma non abbiamo mosso un dito."


translate italian day4_sl_05bba4b4:


    "Slavya sembrava stesse aspettando che io facessi la prima mossa, ma era già tanto se riuscivo a restare immobile senza scappar via."


translate italian day4_sl_95f1e61d:


    "La luna è comparsa di nuovo da dietro le nubi, illuminando l'edificio del vecchio campo."


translate italian day4_sl_682e5f18:


    "Ora non sembrava più una tomba, ma tutte le immagini di morte nella mia testa sono diventate reali. Visioni di fantasmi sono diventate più evidenti, il lamento del vento e il fruscio dell'erba sono diventati più chiari. Si potevano sentire dei rumori sconosciuti provenire da lontano."


translate italian day4_sl_0bcb6a42:


    sl "Sembra che sia stato un bel posto, in passato…"


translate italian day4_sl_6cfd5fba:


    "Ho guardato Slavya attentamente."


translate italian day4_sl_3059e4dc:


    "Forse era spaventata, ma cercava di nasconderlo.{w} Al tale pensiero mi sono sentito un po' meglio."


translate italian day4_sl_b55719b1:


    me "Forse. Andiamo?"


translate italian day4_sl_7b8fbab7:


    sl "Certo…"


translate italian day4_sl_57efbd93:


    "Ho proseguito con cautela, guardando bene dove mettevo i piedi, cercando di non toccare la vecchia recinzione, o il carosello sgangherato, o lo scivolo arrugginito."


translate italian day4_sl_031a7b86:


    "In certi punti l'erba arrivava fino alla cintura, e ogni passo sbagliato avrebbe potuto significare una gamba rotta."


translate italian day4_sl_b0c6ca01:


    "Finalmente siamo arrivati davanti all'entrata, e ho puntato la torcia verso l'interno."


translate italian day4_sl_ca84f49d:


    "L'oscurità che mi fissava dall'interno dell'edificio era molto più spaventosa dell'ambiente esterno – almeno fuori la luna splendeva."


translate italian day4_sl_1da1942d:


    "Ho raccolto tutto il mio coraggio e ho varcato la soglia."


translate italian day4_sl_2eb693a0:


    "L'interno dell'edificio sembrava proprio come la scuola materna in cui andavo da bambino. La disposizione delle stanze, e persino i mobili marci erano simili."


translate italian day4_sl_c63ec5a4:


    me "Bene…"


translate italian day4_sl_2b54d2f5:


    "Ho rabbrividito, individuando in ogni oggetto i ricordi perduti della gioia dei bambini che erano cresciuti in questo luogo."


translate italian day4_sl_69664b95:


    sl "Raccapricciante."


translate italian day4_sl_5eabb736:


    "Slavya non mi stava più tenendo per mano, ma si teneva stretta al mio braccio, premendosi con forza contro di me."


translate italian day4_sl_85558387:


    "Tuttavia non sentivo alcun tono di paura nella sua voce."


translate italian day4_sl_592fe89e:


    me "Mi chiedo se sia stata una buona idea venire qui nel cuore della notte."


translate italian day4_sl_e330b23b:


    sl "Forse no."


translate italian day4_sl_438cf7fc:


    me "Allora torniamo indietro?"


translate italian day4_sl_251c7d47:


    sl "No! E Shurik?"


translate italian day4_sl_9a478370:


    "Mi ha risposto, con genuino stupore."


translate italian day4_sl_6d33a4b2:


    me "Beh, Shurik… Per cominciare, perché dovrebbe essere venuto in questo posto? Forse non si trova nemmeno qui."


translate italian day4_sl_1a99efc0:


    sl "E dove potrebbe essere andato altrimenti?"


translate italian day4_sl_43f9ae8d:


    me "Non lo so. Forse è stato divorato dai lupi?"


translate italian day4_sl_290c429f:


    sl "Non dirlo nemmeno per scherzo!"


translate italian day4_sl_167a71f8:


    "Slavya ha aggrottato la fronte, indietreggiando di un passo."


translate italian day4_sl_09003bb4:


    "Ormai ero talmente abituato alla sua compagnia, che quel suo rimprovero mi ha un po' turbato."


translate italian day4_sl_ff0e577b:


    me "Ok, va bene... supponiamo che non ci siano lupi qui."


translate italian day4_sl_d8ada2af:


    th "Anche se non si sa mai…"


translate italian day4_sl_74000c71:


    me "Beh, dal momento che ormai siamo qui, iniziamo a cercare."


translate italian day4_sl_ce617998:


    "…"


translate italian day4_sl_5b164ba0:


    "Abbiamo setacciato tutte le stanze dell'edificio: le sale da gioco, le camere da letto, la mensa, la cucina e i bagni. Ho persino guardato nel ripostiglio, che era pieno di scatoloni vuoti ammuffiti."


translate italian day4_sl_693b20be:


    sl "Guarda!"


translate italian day4_sl_b212fb3a:


    "Quando siamo tornati alla sala principale, Slavya ha raccolto qualcosa da terra e me l'ha mostrata."


translate italian day4_sl_0ca58b5a:


    "Era una vecchia bambola rovinata, con i capelli strappati, i fili sfilacciati, senza una mano e senza un occhio, sbiadita a causa dell'umidità."


translate italian day4_sl_23104825:


    me "E allora?"


translate italian day4_sl_c22db2eb:


    sl "Mi dispiace per lei…"


translate italian day4_sl_c1b9e2f6:


    me "Come? Per questa bambola?"


translate italian day4_sl_8b01f8be:


    sl "Sì."


translate italian day4_sl_d4817050:


    me "E perché ti dovrebbe dispiacere?"


translate italian day4_sl_95ebdaf5:


    sl "Qualcuno ha giocato con lei, ma poi l'ha gettata via e l'ha abbandonata. E lei è dovuta rimanere qui da sola per tutti questi anni, sopportando la pioggia e il freddo..."


translate italian day4_sl_37bdb5c9:


    "Non riuscivo davvero a capire se Slavya fosse seria o meno."


translate italian day4_sl_c14f498f:


    "E non credevo che fosse il momento adatto per pensare a queste cose."


translate italian day4_sl_0d679731:


    "D'altra parte, quale potrebbe essere il momento giusto per quelle cose?"


translate italian day4_sl_3ca74edc:


    "Slavya sembrava davvero sconvolta."


translate italian day4_sl_d76f93fa:


    me "Sì, forse hai ragione."


translate italian day4_sl_34900be1:


    "Ho preso la bambola e l'ho esaminata."


translate italian day4_sl_48de1c46:


    "Ricordo che una volta trovai un peluche di leone allo stesso modo in cui Slavya ha trovato questa bambola. Me lo portai a casa, lo lavai e lo trattai bene come facevo con i miei altri giocattoli, anche se ormai lui era da molto tempo in pensione."


translate italian day4_sl_3ccc4d70:


    me "Dovresti darla a Ulyana."


translate italian day4_sl_c954d69f:


    "Slavya ha riso."


translate italian day4_sl_3ae7ec76:


    "Improvvisamente il vento ha ululato lungo il corridoio, cantilenando una melodia inquietante."


translate italian day4_sl_0aa5972c:


    "Una luce è balenata davanti ai miei occhi, e io istintivamente sono trasalito, inciampando in qualcosa e cadendo per terra."


translate italian day4_sl_7c5d32f6:


    sl "Stai bene?"


translate italian day4_sl_28fa8946:


    "Slavya è venuta in mio soccorso, dandomi una mano a rialzarmi."


translate italian day4_sl_e01d393f:


    me "Sì, più o meno…"


translate italian day4_sl_f92f7071:


    "Ero inciampato su una botola."


translate italian day4_sl_e28dff1c:


    "Strano che non l'avessimo notata prima – la polvere su di essa era stata rimossa. Sembrava che qualcuno l'avesse usata non molto tempo prima."


translate italian day4_sl_473e195c:


    me "Non abbiamo intenzione di scendere là sotto, vero?"


translate italian day4_sl_0f373940:


    "Ho chiesto, cercando di anticipare la proposta di Slavya."


translate italian day4_sl_beaacb2a:


    "Si è limitata a guardarmi in modo triste."


translate italian day4_sl_3a450870:


    me "Suvvia… stiamo forse giocando agli scavatori?"


translate italian day4_sl_e7af80fb:


    "Ho gemuto, mentre cercavo di sollevare quella botola con grande sforzo."


translate italian day4_sl_637f8cc9:


    "Il mio buonsenso stava gridando, cercando di comunicarmi che non avrei dovuto farlo, ma per contro la mia logica mi suggeriva che se qualcuno mi avesse voluto uccidere, a quest'ora sarei già morto."


translate italian day4_sl_7e0c9bc6:


    "Ma è solo perché lo voleva Slavya, altrimenti non sarei mai andato là sotto!"


translate italian day4_sl_a133f4fb:


    "Un'arrugginita scala di metallo scendeva per un paio di metri, terminando in una pavimentazione di cemento sporco."


translate italian day4_sl_ceabf4e4:


    "Mi sono sporto oltre il bordo e ho illuminato lo stretto passaggio, che finiva da qualche parte nell'oscurità."


translate italian day4_sl_fb03e6d3:


    me "Cosa c'è là sotto?"


translate italian day4_sl_030f1acd:


    sl "Credo di aver sentito parlare di un rifugio antiaereo al di sotto del vecchio campo."


translate italian day4_sl_28acc263:


    me "Un rifugio antiaereo?"


translate italian day4_sl_d96d518d:


    "Beh, è abbastanza logico, considerato il periodo in cui era stato costruito."


translate italian day4_sl_8017924d:


    me "E perché mai Shurik sarebbe andato là sotto?"


translate italian day4_sl_311a3a2f:


    sl "Non lo so."


translate italian day4_sl_a1d6e601:


    th "Certamente, non dev'essere stato necessariamente lui ad aprire quella botola (e mi pare ovvio che qualcuno l'abbia aperta di recente), ma chi altro se non un folle inventore potrebbe avere la voglia di calarsi là sotto?"


translate italian day4_sl_0eb09c36:


    th "Senza considerare le teorie sui fenomeni paranormali, non possono esserci altri esseri umani in questa foresta."


translate italian day4_sl_91db26c0:


    "O almeno è quello che credevo."


translate italian day4_sl_c068a23a:


    me "D'accordo, scendo per primo. Se vedrò che è tutto a posto, allora scenderai anche tu."


translate italian day4_sl_85ed45c7:


    sl "E cosa potrebbe non essere a posto?"


translate italian day4_sl_9cb98215:


    "Slavya sembrava intimorita, ma forse me l'ha chiesto solo per educazione."


translate italian day4_sl_60d825fa:


    me "Beh, non saprei… I ratti, ad esempio. Hai paura dei ratti?"


translate italian day4_sl_84e0dd1c:


    th "Forse non avrei dovuto chiederglielo."


translate italian day4_sl_73542db8:


    "Ho tirato un grande sospiro, e mi sono immerso in quell'oscurità."


translate italian day4_sl_ce617998_1:


    "…"


translate italian day4_sl_2b476ea8:


    "Avanzavamo lentamente, tenendoci per mano."


translate italian day4_sl_e4392fea:


    "La torcia non ci stava aiutando molto in quel buio pesto. Solo occasionalmente riusciva ad illuminare pezzi di vecchi giornali e rottami metallici sparsi qua e là."


translate italian day4_sl_3c66bee2:


    "Dopo un po' sembrava che le pareti e il soffitto si stessero chiudendo su di noi. Lunghi cordoni si estendevano verso l'ignoto, mentre il passaggio diventava sempre più stretto."


translate italian day4_sl_3ffd1664:


    "Ho allargato le braccia, cercando di misurare la larghezza di quel passaggio."


translate italian day4_sl_f987b701:


    sl "Siamo ancora lontani?"


translate italian day4_sl_cb8392a3:


    "Ha chiesto Slavya tristemente."


translate italian day4_sl_8f2edfc1:


    me "Lontani da cosa? Non ne ho la minima idea."


translate italian day4_sl_6f9aa3f1:


    sl "D'accordo…"


translate italian day4_sl_af396620:


    "E subito dopo un'enorme porta di metallo è apparsa davanti a me dal nulla."


translate italian day4_sl_f6458d5d:


    sl "Oh…"


translate italian day4_sl_1135b8b4:


    "Ha sospirato Slavya dolcemente."


translate italian day4_sl_6d46c180:


    "Ho cercato di aprirla, e mi è sembrato che stesse iniziando a cedere."


translate italian day4_sl_a752ccba:


    me "Sei pronta?"


translate italian day4_sl_8a3c9e52:


    sl "Non ne sono sicura…"


translate italian day4_sl_49678fc1:


    "Anche se, francamente, come ci si potrebbe sentire pronti in una situazione del genere?"


translate italian day4_sl_4876f13f:


    "La ruota ha girato un paio di volte, cigolando terribilmente, poi finalmente ha raggiunto il suo limite e si è zittita."


translate italian day4_sl_a5571da6:


    "Ho spinto la porta con tutte le mie forze, e si è aperta con riluttanza.{w} Dietro ad essa c'era una specie di stanza…"


translate italian day4_sl_dfcada65:


    "Che si è rivelata essere proprio il rifugio antiaereo."


translate italian day4_sl_40173f81:


    "Appena entrati, le luci si sono accese da sole, facendomi indietreggiare."


translate italian day4_sl_2a01283b:


    "Probabilmente la struttura era alimentata da una fonte energetica indipendente: le persone che la costruirono hanno fatto in modo che potesse durare a lungo."


translate italian day4_sl_9d89915d:


    "Il rifugio antiaereo era esattamente come lo immaginavo. Aveva tutto il necessario per sostenere la prolungata vita umana durante la guerra: gli armadietti avevano al loro interno delle tute di contenimento, maschere antigas, prodotti in scatola e taniche per l'acqua. Davanti alla parete in fondo c'erano numerose attrezzature: misuratori di pressione, rilevatori di radiazioni, una radio, e altre cose."


translate italian day4_sl_867bb71b:


    "Sembrava che nessuno avesse mai utilizzato tutti quegli strumenti (il che non era molto sorprendente).{w} Anzi, sembrava proprio che nessuno fosse mai entrato qui dopo la sua costruzione."


translate italian day4_sl_0fa153ce:


    "Slavya mi ha guardato con aria interrogativa."


translate italian day4_sl_74d4b2c0:


    me "Beh, è chiaro che Shurik non sia qui."


translate italian day4_sl_2e01cda6:


    sl "Forse è passato di qui?"


translate italian day4_sl_ea7c0036:


    me "Non saprei. Ma se è entrato qui dentro, di certo non sarebbe potuto svanire nell'aria."


translate italian day4_sl_368d74f1:


    "Mi sono guardato attorno nuovamente e ho notato un'altra porta sulla parete alla mia sinistra."


translate italian day4_sl_156f7cf2:


    me "Forse è andato di là?"


translate italian day4_sl_03dccc13:


    "Ma quella porta non si apriva."


translate italian day4_sl_79177436:


    sl "E se si fosse barricato dall'altra parte?"


translate italian day4_sl_869dccdf:


    me "Per quale motivo?"


translate italian day4_sl_8a3c9e52_1:


    sl "Non saprei…"


translate italian day4_sl_1f8bda75:


    "Era possibile, sempre che Shurik fosse davvero entrato là dentro."


translate italian day4_sl_820a03b7:


    "A quanto sembrava, Shurik si stava nascondendo, forse stava scappando da qualcosa."


translate italian day4_sl_dde3d971:


    "Tali pensieri mi hanno messo a disagio, e ho sentito il bisogno di oltrepassare quella porta quanto prima possibile."


translate italian day4_sl_5426c6ac:


    "Ho spinto forte, ma la porta si è limitata a cigolare debolmente."


translate italian day4_sl_1fecc13d:


    sl "Ecco, prova con questo."


translate italian day4_sl_c2d7184d:


    "Slavya mi ha consegnato un piede di porco."


translate italian day4_sl_7b68d30d:


    "L'ho esaminato, cercando di capire come avrei potuto utilizzarlo per aprire quella porta."


translate italian day4_sl_33d34823:


    me "Proviamo."


translate italian day4_sl_74880975:


    "Usando tutte le mie forze, sono riuscito a staccare la porta dai cardini, ed essa si è schiantata sul pavimento."


translate italian day4_sl_05aa00af:


    "Mentre mi stavo riprendendo dallo sforzo, Slavya ha puntato la torcia verso il passaggio appena aperto."


translate italian day4_sl_00166c91:


    sl "C'è un altro corridoio qui, proprio come quello di prima."


translate italian day4_sl_a2885c69:


    me "Accidenti… Questo posto è un vero e proprio labirinto."


translate italian day4_sl_c6391d27:


    sl "Andiamo?"


translate italian day4_sl_079f62e5:


    me "Aspetta…"


translate italian day4_sl_41c2ba2c:


    "Mi sono seduto su uno dei letti, ho tolto una federa polverosa dal cuscino e con essa mi sono asciugato il sudore dalla fronte."


translate italian day4_sl_13ab9fc8:


    me "Lasciami riprendere fiato."


translate italian day4_sl_87f6e3eb:


    "Slavya si è seduta accanto a me e ha iniziato a studiare la forma delle piastrelle del pavimento."


translate italian day4_sl_48fd6cec:


    me "È meglio risparmiare la batteria della torcia adesso…"


translate italian day4_sl_406417b4:


    sl "Oh, hai ragione…"


translate italian day4_sl_aba53b88:


    "È arrossita leggermente, spegnendo la torcia."


translate italian day4_sl_de24b50e:


    me "È semplicemente inquietante…"


translate italian day4_sl_12cb2c61:


    "Mi sono reso conto di non aver considerato reali gli eventi dell'ultima mezz'ora."


translate italian day4_sl_87863660:


    "Come se non fossero accaduti proprio a me, come se fossero stati solo un sogno."


translate italian day4_sl_6a0d0b3e:


    "Proprio come quattro giorni fa, quando mi sono ritrovato per la prima volta in questo posto."


translate italian day4_sl_79ab8f59:


    "Sì, ultimamente potrei anche essermi abituato a questo mondo, a questa {i}realtà{/i} – come se per me fosse diventata una realtà a tutti gli effetti."


translate italian day4_sl_c2db5cf0:


    "E quel mio desiderio nel profondo dell'anima, di voler trovare assolutamente una via d'uscita da tutto questo, non era più così acuto."


translate italian day4_sl_a00907f6:


    "Ma adesso, in un batter d'occhio: la foresta, il vecchio campo, il rifugio antiaereo."


translate italian day4_sl_3c3e55db:


    "Non c'era assolutamente nulla di soprannaturale in tutto questo – ma non era neppure una cosa del tutto normale, come le cose della mia vita passata."


translate italian day4_sl_7b8fbab7_1:


    sl "Già…"


translate italian day4_sl_a4965e91:


    "Ha concordato Slavya."


translate italian day4_sl_00992d89:


    me "Non ti sembra un po' strano?"


translate italian day4_sl_a96c222e:


    sl "Cosa esattamente?"


translate italian day4_sl_5aaaf3dd:


    me "Beh, il fatto che siamo seduti qui, solo tu e io, nel cuore della notte. Non sarebbe stato normale in questa situazione attendere fino al mattino e chiamare la polizia?"


translate italian day4_sl_33cb1281:


    sl "Credo di sì."


translate italian day4_sl_5e20fc4d:


    "Ha risposto pensierosa."


translate italian day4_sl_aca724aa:


    me "Certo, mi sono quasi offerto volontario. Non avrei dovuto."


translate italian day4_sl_e9545c3b:


    "Beh, «quasi» non è del tutto vero, ma comunque."


translate italian day4_sl_0f83eee7:


    sl "Ma volevi solo dare una mano. Se Shurik fosse proprio qui, allora dobbiamo trovarlo il prima possibile."


translate italian day4_sl_6577f1d4:


    me "Di questo passo saremo noi i prossimi che verranno a cercare. Sai, potrebbe essersi perso in questo labirinto."


translate italian day4_sl_a336bbb9:


    "Slavya sembrava frustrata."


translate italian day4_sl_6e2ae7df:


    me "Va bene, va bene, non intendevo dire nulla del genere. Forse non è nemmeno qui, forse a quest'ora è già tornato al Campo sano e salvo."


translate italian day4_sl_c05d1b8c:


    sl "Sì, forse."


translate italian day4_sl_7b411858:


    me "Ma dovremmo controllare comunque, giusto?"


translate italian day4_sl_49cc0ae4:


    sl "Certamente!"


translate italian day4_sl_d6679b71:


    "Slavya si è alzata in piedi e mi ha porto la mano."


translate italian day4_sl_50e89832:


    "Sembrava che non fosse tanto spaventata quanto me."


translate italian day4_sl_555bb396:


    "Anche se ero stanco per il solo fatto di essere spaventato, e volevo solo che tutto questo finisse il prima possibile."


translate italian day4_sl_ce617998_2:


    "…"


translate italian day4_sl_8d29725f:


    "Questo corridoio era esattamente identico a quello precedente che ci aveva portati al rifugio antiaereo."


translate italian day4_sl_c77a261c:


    "Stavo camminando a passo lento, osservando attentamente il pavimento e controllando di tanto in tanto se il passaggio si stesse restringendo."


translate italian day4_sl_d14ade49:


    sl "Guarda!"


translate italian day4_sl_9f8002ec:


    "Slavya ha stretto forte la mia mano e ha puntato il dito verso una breccia al centro del tunnel."


translate italian day4_sl_5197c9ee:


    "Era abbastanza larga da poter risalire di sopra (o scendere di sotto)."


translate italian day4_sl_02b0275d:


    sl "Forse…"


translate italian day4_sl_62129d64:


    "Ho puntato la torcia verso il basso, ma sul fondo sembrava non esserci altro che del terreno."


translate italian day4_sl_642193e3:


    me "Credo che dovremmo dare un'occhiata…"


translate italian day4_sl_3c17ab25:


    "Saremmo potuti risalire con facilità più tardi, quindi a prima vista non sembrava esserci nulla di cui preoccuparsi."


translate italian day4_sl_bdf18d28:


    "Sono sceso con cautela e ho aiutato Slavya a fare altrettanto."


translate italian day4_sl_7a9a5a51:


    "Ci siamo ritrovati in una specie di miniera."


translate italian day4_sl_171403e3:


    "Le pareti e il soffitto erano rinforzati con delle travi di legno, e delle rotaie si allungavano in lontananza."


translate italian day4_sl_3428662f:


    "Qua e là la terra pareva premere sulla fragile struttura, rovinata dal tempo. L'intera costruzione sembrava decisamente poco affidabile."


translate italian day4_sl_a5f51cbc:


    sl "Dove pensi che ci troviamo?"


translate italian day4_sl_ee72963d:


    me "Non lo so, probabilmente è una miniera. Che cosa avrebbero potuto estrarre qui?"


translate italian day4_sl_80b2ad66:


    sl "Carbone, forse?"


translate italian day4_sl_0be49839:


    me "È probabile…"


translate italian day4_sl_c39ad14a:


    "Abbiamo proseguito lentamente."


translate italian day4_sl_29351dec:


    "Le pietre scivolavano proditoriamente sotto ai nostri piedi, il fascio di luce della nostra torcia tremava e rimbalzava da una parete all'altra, spesso generando spaventose ombre che svanivano non appena la luce ritornava su di esse."


translate italian day4_sl_253c2530:


    sl "È più spaventoso quaggiù che là sopra…"


translate italian day4_sl_1066065f:


    me "Decisamente."


translate italian day4_sl_86ad247f:


    "Più ci addentravamo nell'ignoto, più mi convincevo che Shurik non aveva niente a che fare con questo luogo!"


translate italian day4_sl_a28736d8:


    th "Non potrebbe essere stato rapito da un chupacabra, giusto?{w} D'accordo, forse non possiamo escluderlo al cento percento."


translate italian day4_sl_2c6a97fe:


    "Alla fine siamo giunti ad un bivio nel tunnel."


translate italian day4_sl_11667520:


    sl "E adesso che direzione prendiamo?"


translate italian day4_sl_595d7b8c:


    me "Non lo so."


translate italian day4_sl_2d6b4b1d:


    "Ho cercato di iniettare almeno una parvenza di calma nella mia voce, ma non avevo davvero idea di cosa fare.{w} Proprio nessuna: di dove andare, di dove fosse Shurik, e neppure del motivo per cui ci trovavamo qui!"


translate italian day4_sl_986e3d9c:


    sl "E se ci perdessimo...?"


translate italian day4_sl_afcce3c5:


    me "Giusto, hai ragione, lasciamo un segno da qualche parte, così sapremo che siamo già stati qui!"


translate italian day4_sl_97280ee5:


    "Ho preso una pietra abbastanza grande e ho inciso una croce su una delle travi di sostegno."


translate italian day4_sl_05bb4d2a:


    me "Ecco fatto, adesso sapremo di essere già passati in questo punto."


translate italian day4_sl_af1d965e:


    sl "Bene."


translate italian day4_sl_c17a722d:


    "Slavya ha sorriso."


translate italian day4_sl_bea7cbc5:


    sl "Allora, che direzione prendiamo?"


translate italian day4_sl_ce617998_3:


    "…"


translate italian sl_mine_coalface_e40597aa:


    "Siamo già stati qui."


translate italian sl_mine_coalface_5b4c3c90:


    "Finalmente il tunnel è sboccato in una stanza piuttosto grande, con un alto soffitto."


translate italian sl_mine_coalface_3ef8c583:


    "Anche se non era proprio una stanza, devono averla usata per estrarre qualcosa qui.{w} Probabilmente il carbone, o forse anche l'oro."


translate italian sl_mine_coalface_4253b858:


    "Le pareti erano state frantumate dai picconi o dai martelli pneumatici."


translate italian sl_mine_coalface_c718c063:


    "Qui regnava il buio più totale, quindi la torcia era la nostra unica salvezza."


translate italian sl_mine_coalface_b97863a3:


    th "Se si rompesse, non ne usciremmo sicuramente vivi…"


translate italian sl_mine_coalface_58411121:


    "Il raggio di luce ha illuminato un pezzo di tessuto rosso nell'angolo."


translate italian sl_mine_coalface_ca0a4f90:


    "Era il foulard di un pioniere!"


translate italian sl_mine_coalface_96b3272c:


    "Allora Shurik doveva davvero essere nelle vicinanze."


translate italian sl_mine_coalface_93ec21f7:


    me "Shurik! Shurik!"


translate italian sl_mine_coalface_cc29db24:


    sl "Shurik! Shurik!"


translate italian sl_mine_coalface_b99be3e2:


    "Solo il nostro eco ci ha risposto."


translate italian sl_mine_coalface_e27745b6:


    sl "Non preoccuparti, sono sicura che sta bene!"


translate italian sl_mine_coalface_b894ecef:


    "Francamente, in quel momento ero più preoccupato per me stesso…"


translate italian sl_mine_coalface_a411960f:


    th "Ma dove potrebbe essere andato partendo da questo punto?"


translate italian sl_mine_coalface_55719814:


    "La stanza non aveva altre uscite…"


translate italian sl_mine_coalface_2b95387f:


    th "Certo, probabilmente ci sono dei luoghi in questi tunnel che non abbiamo ancora esplorato."


translate italian sl_mine_coalface_858b6cbe:


    th "Allora dobbiamo continuare a cercare!"


translate italian sl_mine_exit_e56eea78:


    "Dopo un altro angolo la torcia ha illuminato una porta di legno."


translate italian sl_mine_exit_233631e4:


    me "Beh, è già qualcosa."


translate italian sl_mine_exit_14491239:


    "A dire il vero, ero talmente esausto da tutto quel vagare che ho smesso del tutto di pensare a Shurik. Volevo solo trovare una via d'uscita il prima possibile."


translate italian sl_mine_exit_920627d5:


    sl "Cosa ci sarà dietro?"


translate italian sl_mine_exit_fac00056:


    me "Stiamo per scoprirlo."


translate italian sl_mine_exit_dc9f0445:


    "Oltre quella porta c'era un piccolo stanzino, che sembrava il ripostiglio della caldaia di una casa, o un'altra di quelle stanze del rifugio antiaereo."


translate italian sl_mine_exit_d4b4511f:


    "Bottiglie vuote erano sparse sul pavimento, e le pareti erano piene di graffiti, il che significava che altre persone l'avevano visitata più volte prima di noi.{w} Anche se pareva essere stato molto tempo fa."


translate italian sl_mine_exit_b5d787b8:


    "Ho puntato la torcia in ogni angolo della stanza, cercando di esaminare ogni suo dettaglio, quando improvvisamente…"


translate italian sl_mine_exit_7d62949b:


    "Ho trovato Shurik rannicchiato in un angolo."


translate italian sl_mine_exit_a7c6dc11:


    sl "Shurik!"


translate italian sl_mine_exit_83931b19:


    "Ha gridato Slavya."


translate italian sl_mine_exit_54f665b3:


    "Tuttavia, sembrava che ci stesse ignorando del tutto."


translate italian sl_mine_exit_dd1de183:


    me "Eccoti finalmente! Ti abbiamo cercato per tutta la notte! Perché diavolo sei venuto qui sotto?"


translate italian sl_mine_exit_959f145c:


    "Shurik ha alzato il suo sguardo verso di me, con gli occhi persi nel vuoto."


translate italian sl_mine_exit_f335a763:


    sh "E voi chi siete?"


translate italian sl_mine_exit_7393b21b:


    me "Che vuoi dire con «voi chi siete»? Dai, alzati e andiamocene da qui."


translate italian sl_mine_exit_5b4d50f1:


    sh "Io non vado da nessuna parte con voi."


translate italian sl_mine_exit_50ed4f02:


    "Shurik ci ha dato nuovamente le spalle ed è tornato a fissare l'oscurità."


translate italian sl_mine_exit_27e3ed3d:


    sh "Volete ingannarmi un'altra volta. Mi farete girare in tondo per questi tunnel, non è così? Ma certo che lo farete! Ormai vi conosco bene!"


translate italian sl_mine_exit_284e0011:


    "La voce di Shurik si è trasformata in un sussurro infernale."


translate italian sl_mine_exit_fe9c97c8:


    me "Smettila con queste sciocchezze."


translate italian sl_mine_exit_3069c4df:


    "Stavo per avvicinarmi a lui per sollevarlo in piedi, ma Slavya mi ha fermato."


translate italian sl_mine_exit_f9e442d6:


    sl "Non è in sé."


translate italian sl_mine_exit_a13f5a0a:


    me "Beh, neppure io sono in me! Vagare tutta la notte per questi tunnel – chiunque non sarebbe in sé."


translate italian sl_mine_exit_f98911a4:


    "Lei ha scosso la testa con aria di rimprovero e si è avvicinata lentamente a Shurik, rimanendo con attenzione nel fascio di luce."


translate italian sl_mine_exit_80764f4c:


    sl "È tutto a posto, sono io, Slavya."


translate italian sl_mine_exit_af188dfb:


    sh "Davvero?"


translate italian sl_mine_exit_be9d5df3:


    "Shurik l'ha guardata e immediatamente ha iniziato a singhiozzare in silenzio."


translate italian sl_mine_exit_5e2b81c3:


    sl "Ma certo! Chi altri potrebbe essere? E c'è anche Semyon con me. Siamo venuti a cercarti. È tutto a posto adesso!"


translate italian sl_mine_exit_0be700a9:


    sh "Davvero non siete {i}loro{/i}?"


translate italian sl_mine_exit_553577fe:


    sl "Certo che no! Noi siamo noi."


translate italian sl_mine_exit_3002543e:


    "Ha provato ad alzarsi in piedi, ma sembrava disorientato, e sarebbe caduto se non fosse stato per Slavya, che l'ha afferrato per un braccio."


translate italian sl_mine_exit_dbdfe385:


    sl "Fa' attenzione! Con calma!"


translate italian sl_mine_exit_740d40c9:


    "Quando si sono avvicinati a me, ho detto:"


translate italian sl_mine_exit_a47d7467:


    me "D'accordo, è ora di uscire da qui!"


translate italian sl_mine_exit_05d7a2e2:


    sh "Non possiamo tornare indietro…"


translate italian sl_mine_exit_52f884ab:


    me "Indietro dove? Nella miniera?"


translate italian sl_mine_exit_2ba450b2:


    sh "Sì."


translate italian sl_mine_exit_24404a66:


    "Shurik parlava con calma, ma allo stesso tempo la sua voce tremava leggermente."


translate italian sl_mine_exit_5ac38cf1:


    me "E perché?"


translate italian sl_mine_exit_798ba618:


    sh "È lì dove ho… incontrato {i}loro{/i}!"


translate italian sl_mine_exit_f1b8c285:


    me "Chi sono questi {i}«loro»{/i}?"


translate italian sl_mine_exit_83aa975e:


    sh "Le voci."


translate italian sl_mine_exit_777da593:


    me "Quali voci?"


translate italian sl_mine_exit_25a17e79:


    "Stavo cominciando a perdere la pazienza."


translate italian sl_mine_exit_5d4ebca9:


    "Questo luogo e questa situazione certamente non erano normali, ma se ci fosse stato qualcosa di paranormale qui, secondo le leggi del cinema horror, si sarebbe ormai rivelato da tempo."


translate italian sl_mine_exit_15266f71:


    sl "Calmati, Shurik. Raccontaci tutto in dettaglio."


translate italian sl_mine_exit_aef80036:


    "Dopo aver fatto un paio di respiri profondi, ha iniziato:"


translate italian sl_mine_exit_25825ae0:


    sh "Avevo bisogno di alcuni pezzi di ricambio per il robot, e ho sentito dire che c'era un rifugio antiaereo nel vecchio campo. E di solito c'è un sacco di materiale interessante in questi posti. Strumenti datati, ovviamente, ma vale la pena smontarli: si possono sempre trovare delle bobine, delle resistenze..."


translate italian sl_mine_exit_3b6c99a8:


    sh "Così sono partito la mattina presto, ho pensato che avrei potuto finire in fretta e tornare in tempo per la colazione."


translate italian sl_mine_exit_579e2e33:


    sh "Avevo raccolto tutto quello che mi serviva."


translate italian sl_mine_exit_c03bb0fe:


    "Ha tolto dalla sua tasca alcune lampadine e fili vari e ce li ha mostrati."


translate italian sl_mine_exit_9d133ba2:


    sh "Ma poi, lassù nel rifugio antiaereo, ho sentito dei suoni provenire da dietro la porta. Qualcosa come gemiti o lamenti. In un primo momento ho avuto paura, ma poi ho pensato che qualcuno potesse essere in pericolo. Così ci sono andato…"


translate italian sl_mine_exit_9ec05142:


    sh "E poi ho trovato questa miniera, questo maledetto labirinto! Non riuscivo più a uscirne. E le voci, le voci che mi dicevano sempre qualcosa, a volte gridando, a volte sussurrando, e io non riuscivo mai a distinguere le parole!"


translate italian sl_mine_exit_b022d1a8:


    "Stava per scoppiare in lacrime, ma Slavya l'ha accarezzato delicatamente sulla testa, e lui ha continuato:"


translate italian sl_mine_exit_e48ebc1f:


    sh "Comunque, ho trovato questa stanza, almeno qui non si poteva sentire {i}quelle voci{/i}…"


translate italian sl_mine_exit_1eeae244:


    me "Così hai deciso di restare seduto e di attendere i soccorsi, come in un film di serie B?"


translate italian sl_mine_exit_0f9dcec3:


    "Ho chiesto con sarcasmo."


translate italian sl_mine_exit_a783d9b3:


    "Slavya mi ha guardato arrabbiata."


translate italian sl_mine_exit_d5aa4a23:


    me "Beh, in ogni caso, è ora di andarcene da qui! Se non ci sono obiezioni."


translate italian sl_mine_exit_64d006fa:


    sh "Venite, conosco una scorciatoia!"


translate italian sl_mine_exit_1f5418a3:


    me "Una scorciatoia?"


translate italian sl_mine_exit_d3c3faf7:


    sh "Sì, sapete, ho vagato a lungo per questi corridoi."


translate italian sl_mine_exit_5fabfeff:


    me "D'accordo…"


translate italian sl_mine_exit_a20cefa7:


    "..."


translate italian sl_mine_exit_31a38a29:


    "Shurik non ci aveva mentito."


translate italian sl_mine_exit_8fb267ae:


    "Infatti, dopo aver vagato per quasi due minuti nella miniera, ci siamo trovati sotto a una grata, attraverso la quale si poteva scorgere il cielo."


translate italian sl_mine_exit_401125ca:


    me "E allora perché non sei uscito da solo?"


translate italian sl_mine_exit_9fd952a9:


    sh "Provaci e vedrai."


translate italian sl_mine_exit_34b807b5:


    "Mi sono tirato su e ho provato più volte a spingere la grata."


translate italian sl_mine_exit_59e04ffd:


    th "È vero, non si può rimuoverla a mani nude.{w} Dobbiamo usare qualcosa di pesante…"


translate italian sl_mine_exit_e1eb6421:


    "Ho guardato in basso verso Slavya e Shurik."


translate italian sl_mine_exit_ea249dfd:


    sl "Vuoi provare con la torcia?"


translate italian sl_mine_exit_af0c14fb:


    me "La torcia…"


translate italian sl_mine_exit_b7873ad2:


    "Ho armeggiato un po' con essa: non era una di quelle torce cinesi scadenti degli anni 90, delle quali il nostro cottage estivo era sempre colmo, ma piuttosto era un'autentica e resistente torcia sovietica di metallo.{w} Non era esattamente come un martello, ma usandola nel modo giusto…"


translate italian sl_mine_exit_68320b1d:


    "Usandola a mo' di martello ho cominciato a colpire la grata mirando ai punti che potevano essere più fragili."


translate italian sl_mine_exit_67cc1ddd:


    "Ben presto ho avuto fortuna: ho sentito il rumore di qualcosa che si rompeva, e un paio di bulloni sono caduti."


translate italian sl_mine_exit_ce617998:


    "…"


translate italian sl_mine_exit_fb1e61f2:


    "Un minuto più tardi ero sdraiato sull'erba davanti al monumento, respirando la fresca aria notturna con sollievo.{w} Dopo tutto quel tempo trascorso in quei tunnel, qualsiasi aria sarebbe sembrata fresca."


translate italian sl_mine_exit_b4b29936:


    sh "Bene, allora io vado, vi ringrazio…"


translate italian sl_mine_exit_3598f2d1:


    "Shurik sembrava un po' perso."


translate italian sl_mine_exit_52f39431:


    "L'ho guardato con sospetto per un momento ma alla fine non ho detto niente, e mi sono limitato a salutarlo con un cenno della mano."


translate italian sl_mine_exit_da4e05bd:


    "Dopo tutto quello che era successo, non avevo né la forza né la voglia di parlare con lui."


translate italian sl_mine_exit_e0453692:


    th "E poi cosa gli potrei dire?{w} Rimproverarlo adesso sarebbe inutile, e lo stesso probabilmente vale anche per il farsi dare delle risposte."


translate italian sl_mine_exit_0df46cc3:


    sl "Che nottata…"


translate italian sl_mine_exit_07fd16f1:


    "Slavya stava seduta accanto a me, fissando la luna con sguardo sognante."


translate italian sl_mine_exit_068c4c7d:


    me "Già… Se qualche settimana fa qualcuno mi avesse detto che sarei andato in un campo di pionieri e che avrei pure strisciato per i tunnel di un rifugio antiaereo…"


translate italian sl_mine_exit_a2b22e6d:


    sl "Ma è stato divertente."


translate italian sl_mine_exit_c15d505f:


    me "In qualche modo."


translate italian sl_mine_exit_5e8427f4:


    "C'era davvero qualcosa di divertente in tutto questo.{w} Ma c'era anche qualcosa di strano: durante le ultime ore, mi sarebbe potuta accadere qualsiasi cosa, ma alla fine non è successo proprio niente."


translate italian sl_mine_exit_1ca3789d:


    "Tra qualche anno probabilmente ricorderò tutto questo come una divertente disavventura della mia giovinezza.{w} Sempre che io riesca a vivere così a lungo…"


translate italian sl_mine_exit_fa7152d2:


    me "Cosa ne pensi, credi che lui abbia davvero sentito delle voci laggiù?"


translate italian sl_mine_exit_9fee39ea:


    sl "Non saprei... È possibile, certo, ma probabilmente si è trattato solo della sua immaginazione. In una situazione del genere, chiunque inizierebbe ad avere visioni."


translate italian sl_mine_exit_b35d647f:


    "Quella non era solo la spiegazione più logica, ma anche la più probabile, perché nulla è successo a me e a Slavya."


translate italian sl_mine_exit_defa6d12:


    me "Beh, secondo il mio modesto parere, dovrebbe far visita a uno psichiatra…"


translate italian sl_mine_exit_e8c8afba:


    sl "Immagino di sì!"


translate italian sl_mine_exit_c954d69f:


    "Ha riso Slavya."


translate italian sl_mine_exit_e086255d:


    sl "Va bene, è ora di dormire."


translate italian sl_mine_exit_8613242c:


    me "Sì."


translate italian sl_mine_exit_65810382:


    "Sentivo il bisogno di dire qualcosa a Slavya.{w} Non ero sicuro di volerlo fare, ma ne sentivo il bisogno."


translate italian sl_mine_exit_8753f434:


    me "Grazie!"


translate italian sl_mine_exit_306a3019:


    sl "Per cosa?"


translate italian sl_mine_exit_d222fc60:


    "Mi ha guardato sorpresa."


translate italian sl_mine_exit_115b4f29:


    me "Non ce l'avrei mai fatta senza di te."


translate italian sl_mine_exit_22d72bcc:


    sl "Oh, figurati…"


translate italian sl_mine_exit_1c771384:


    "Slavya è arrossita."


translate italian sl_mine_exit_15a32661:


    th "Sono in debito con lei.{w} Di nuovo."


translate italian sl_mine_exit_2a83f273:


    th "Mi chiedo come potrò mai ripagarla, almeno parzialmente, per tutto quello che ha fatto per me…"


translate italian sl_mine_exit_aa269f37:


    "Ho fissato il cielo stellato, prendendo sonno lentamente."


translate italian sl_mine_exit_d50a711e:


    "Tutte le avventure di quest'oggi mi hanno reso così stanco che non avevo più la forza di fare niente."


translate italian sl_mine_exit_7ecbe987:


    me "Sai, sotto altre circostanze, avrei sicuramente dichiarato il mio amore per te…"


translate italian sl_mine_exit_c537a406:


    "Ho detto, perdendo il contatto con la realtà."


translate italian sl_mine_exit_0c6574db:


    "I pensieri di solito lavorano molto più velocemente del buonsenso..."


translate italian sl_mine_exit_7d9eccb6:


    "Ma Slavya se n'era già andata."


translate italian sl_mine_exit_2e4b7881:


    th "Forse è meglio così – almeno non mi ha sentito…"


translate italian sl_mine_exit_eeffa084:


    th "Anche se è strano – perché se n'è andata senza salutare?"


translate italian sl_mine_exit_52ccd478:


    th "Beh, non importa, ci dormirò sopra e ci penserò domani."


translate italian sl_mine_exit_c4cf6525:


    "Ero così sfinito che a malapena sono riuscito a raggiungere la casetta della leader...{w} Ma c'era una luce accesa all'interno!"


translate italian sl_mine_exit_b9666e2a:


    th "Ciò significa che mi sta aspettando."


translate italian sl_mine_exit_750b01bf:


    th "E quello, a sua volta, significa che devo prepararmi per un'altra ramanzina."


translate italian sl_mine_exit_f6601815:


    th "E questa volta sarà la più inutile di sempre!"


translate italian sl_mine_exit_d7d24cf6:


    "Non avevo il minimo desiderio di discutere con lei sul se avessi o meno ragione."


translate italian sl_mine_exit_12c908c6:


    "La sedia a sdraio lì vicino mi stava facendo cenno come un amico, come se mi stesse suggerendo di addormentarmi su di essa, dimenticandomi della miniera, di Shurik, della leader del Campo..."


translate italian sl_mine_exit_19f0e8ea:


    me "Eccola..."


translate italian sl_mine_exit_8a478121:


    "Ho detto in modo assente, continuando a fissare la mia cara sedia a sdraio."


translate italian sl_mine_exit_105fc865:


    mt "Semyon!"


translate italian sl_mine_exit_4b47445a:


    me "Sì, sì...{w} Abbiamo trovato Shurik, Slavya le racconterà tutti i dettagli domani, e adesso ho davvero voglia di dormire..."


translate italian sl_mine_exit_014b5f98:


    "Mi ha guardato confusa per un istante, e poi mi ha detto:"


translate italian sl_mine_exit_7e1d09c7:


    mt "Oh, va bene allora..."


translate italian sl_mine_exit_2ab3da5a:


    "Non so se l'aver menzionato Slavya avesse giocato il suo ruolo, ma dopo solo mezzo minuto mi trovavo già nel mio letto."


translate italian sl_mine_exit_c849f321:


    "Gli eventi del giorno appena trascorso hanno continuato a girarmi nella testa per un po', ma ben presto essa ha iniziato a farmi male, era come se la mia coscienza fosse passata in modalità sicura e si fosse riavviata."


translate italian day4_dv_55e8aea3:


    mt "E Dvachevskaya andrà con te!"


translate italian day4_dv_a7bd7fb0:


    "Era come se Olga Dmitrievna mi avesse letto nel pensiero."


translate italian day4_dv_34113b9d:


    dv "Perché proprio io?"


translate italian day4_dv_9dcfa902:


    mt "Beh, chi ha tentato di far esplodere il monumento?{w} E come se non bastasse, ha anche cercato di diffamare Semyon?!"


translate italian day4_dv_f1c01595:


    "L'ha detto in un tono tale che Alisa non ha osato opporsi."


translate italian day4_dv_ed1378b5:


    dv "Sei contento adesso?"


translate italian day4_dv_8efa4588:


    "Ha sbottato, quando siamo rimasti da soli."


translate italian day4_dv_49e69f9d:


    me "E io che c'entro?{w} Sono forse stato io colui che ha cercato di far saltare in aria il monumento?"


translate italian day4_dv_f505265f:


    "Alisa ha grugnito e si è voltata."


translate italian day4_dv_90b40594:


    dv "Io non vado da nessuna parte con te!"


translate italian day4_dv_2cbb8397:


    me "Allora non farlo!"


translate italian day4_dv_b5004dfa:


    dv "Bene allora!"


translate italian day4_dv_d1bfd0cf:


    me "Perfetto!"


translate italian day4_dv_9001d819:


    "Siamo rimasti muti per un po', senza guardarci."


translate italian day4_dv_48e0b151:


    "Ho provato a rompere il silenzio per primo."


translate italian day4_dv_7b7a2ac8:


    me "Tutto qui?"


translate italian day4_dv_7e503b02:


    dv "Tutto qui cosa?"


translate italian day4_dv_43dded4a:


    me "È tutto quello che volevi dirmi?"


translate italian day4_dv_55cf29e5:


    dv "Sì, è tutto!"


translate italian day4_dv_976566c1:


    me "Ti spiace se ti chiedo perché hai voluto far saltare in aria il monumento?"


translate italian day4_dv_288c13a3:


    dv "Ero annoiata…"


translate italian day4_dv_f39f8aa5:


    th "Ah, certo, far saltare tutto per aria sicuramente ti avrebbe rallegrata un po'!"


translate italian day4_dv_5628d51f:


    "Alisa sembrava davvero riluttante a venire con me."


translate italian day4_dv_8d7dd406:


    "Io invece mi ero deciso a cercare Shurik, dopotutto."


translate italian day4_dv_9a84d633:


    "In realtà quello che mi interessava maggiormente era visitare quel campo abbandonato."


translate italian day4_dv_e1bb0ffe:


    "Avevo come la sensazione che avrei trovato delle risposte in quel luogo."


translate italian day4_dv_19accf37:


    "Ma non mi piaceva affatto l'idea di doverci andare da solo di notte…"


translate italian day4_dv_8d8791e8:


    me "Allora, hai paura?"


translate italian day4_dv_2ff41508:


    dv "Mi stai forse sfidando?"


translate italian day4_dv_7f6b716b:


    "Ha sorriso con sarcasmo."


translate italian day4_dv_f9e95578:


    me "Oh no, non mi permetterei mai…{w} Beh, buona fortuna allora, se vuoi restare qui!"


translate italian day4_dv_2b7ea55e:


    "Mi sono girato e ho iniziato a camminare nella presunta direzione del vecchio campo."


translate italian day4_dv_4ae306eb:


    "Alisa mi ha raggiunto al margine della foresta."


translate italian day4_dv_33be1cc8:


    dv "Aspetta!{w} Vengo con te!"


translate italian day4_dv_708a9465:


    me "E cosa ti ha fatto cambiare idea così all'improvviso?"


translate italian day4_dv_27c62366:


    "Ho chiesto con ironia."


translate italian day4_dv_aa4bc8f9:


    dv "Non è necessario che tu lo sappia!"


translate italian day4_dv_eda552ee:


    "Ha ringhiato, consegnandomi una torcia elettrica."


translate italian day4_dv_30b5f8c4:


    "Una mossa saggia."


translate italian day4_dv_56fc6798:


    "Non so come avrei fatto da solo in quell'oscurità se non fosse stato per Alisa…"


translate italian day4_dv_7bd87323:


    "Electronik ha detto che il vecchio edificio era stato costruito appena dopo la guerra."


translate italian day4_dv_fd3c1cdc:


    "Era simile ad una scuola materna (o forse più a una caserma, immaginavo), ed era decisamente troppo piccola per poter contenere tutti i pionieri di questo Campo."


translate italian day4_dv_c4f7341a:


    "Era stata abbandonata da una ventina d'anni…"


translate italian day4_dv_456886a9:


    "La foresta si trasformava completamente di notte."


translate italian day4_dv_5d66727e:


    "Solo poche ore fa sembrava un piccolo boschetto dove nemmeno un cieco avrebbe potuto perdersi."


translate italian day4_dv_e7ca9c28:


    "Tuttavia, al calare dell'oscurità gli alberi sembravano sollevarsi, i cespugli improvvisamente occupavano tutto lo spazio libero, e i sentieri, così ampi alla luce del giorno, venivano trasformati in onde appena visibili nell'immenso oceano verde della foresta."


translate italian day4_dv_30605aad:


    "I tratti finali di quel dipinto erano gli uccelli, gli insetti, e le bestie, che stavano facendo un concerto, ognuno di essi aveva il proprio strumento, e tutti assieme creavano una straordinaria e spaventosa sinfonia notturna."


translate italian day4_dv_a9effafd:


    "Camminavamo lentamente, Alisa era un po' più avanti di me, e io cercavo di tenere il suo passo."


translate italian day4_dv_52d4466a:


    "Forse era un po' troppo sicura di sé.{w} L'esatto contrario di me – rabbrividivo ad ogni fruscio nei cespugli e ad ogni grido dei gufi, guardandomi attorno ansiosamente."


translate italian day4_dv_97608943:


    "Ovviamente non c'era molto da temere qui – nei boschi locali era improbabile che ci fossero bestie feroci, e sembrava che in questo mondo non esistessero altre persone oltre a quelle del Campo."


translate italian day4_dv_0c72d684:


    "Tuttavia, la paura era al livello istintivo, e cercavo di essere quanto più vigile possibile.{w} La discrezione è il lato migliore del coraggio, dopotutto."


translate italian day4_dv_ee008bda:


    "Alisa si è fermata bruscamente.{w} Così bruscamente che l'ho quasi travolta."


translate italian day4_dv_15f32c97:


    dv "E adesso dove?"


translate italian day4_dv_47e7fadb:


    me "Cosa? Beh… Sempre dritto, suppongo?"


translate italian day4_dv_7b471372:


    dv "Sei sicuro?"


translate italian day4_dv_558379f9:


    "Ho cercato di orientarmi."


translate italian day4_dv_1a602959:


    "Sembrava che stessimo andando nella direzione giusta.{w} O almeno nella direzione che ci aveva suggerito Electronik."


translate italian day4_dv_2a16d041:


    "E comunque senza una mappa, senza un GPS o senza la navigazione GLONASS, non ero nemmeno sicuro che ci trovassimo ancora sullo stesso pianeta (ovunque esso fosse)."


translate italian day4_dv_4dde0bc7:


    me "Non abbiamo mai svoltato, abbiamo sempre camminato dritto, così come ci ha detto Electronik…"


translate italian day4_dv_a44acfbb:


    dv "E allora perché non siamo ancora arrivati al vecchio campo?"


translate italian day4_dv_cd895649:


    me "Che ne so?!"


translate italian day4_dv_492a8d61:


    "Il tono rude di Alisa mi stava già dando sui nervi."


translate italian day4_dv_cebebf7d:


    dv "Ci siamo già persi?"


translate italian day4_dv_eb6a6442:


    me "Perché dovresti dire una cosa del genere? Non abbiamo percorso nemmeno un chilometro dal Campo."


translate italian day4_dv_b66acd62:


    dv "E allora?"


translate italian day4_dv_fbb515c6:


    "Sembrava visibilmente più triste."


translate italian day4_dv_023d75be:


    me "E allora niente! Se non ti va bene, puoi tornare indietro!"


translate italian day4_dv_aeb753f4:


    "Alisa mi ha squadrato per un po' con disappunto, e poi la sua espressione si è tramutata radicalmente."


translate italian day4_dv_4a85e25e:


    dv "Bene, allora me ne vado!"


translate italian day4_dv_4fdd6288:


    "Si è girata di scatto e ha iniziato a camminare con passo deciso verso il Campo."


translate italian day4_dv_cacdf009:


    me "Buona fortuna davvero!"


translate italian day4_dv_3613cae1:


    "Le ho gridato mentre si allontanava."


translate italian day4_dv_39966305:


    th "Tuttavia, Alisa potrebbe aver ragione a dire che ci siamo persi.{w} In tal caso è meglio non restare qui da solo…"


translate italian day4_dv_1f17e8a8:


    "La fresca brezza notturna ha iniziato a soffiare più forte, facendomi rabbrividire."


translate italian day4_dv_3fe492ee:


    th "Ma seguirla significherebbe perdere la faccia, o ancora peggio ammettere che lei abbia ragione.{w} No, non posso accettarlo!"


translate italian day4_dv_a158a040:


    "Probabilmente sarei rimasto lì a discutere con me stesso fino al mattino, se non fosse stato per l'urlo di Alisa…"


translate italian day4_dv_5b0d6420:


    "Le mie gambe mi hanno trascinato da sole verso il punto da cui proveniva la sua voce."


translate italian day4_dv_4739db18:


    "Dopo una ventina di metri ho rischiato di cadere in una buca, riuscendo miracolosamente a scavalcarla all'ultimo secondo."


translate italian day4_dv_41315982:


    "Ma Alisa sembrava non aver avuto la mia stessa fortuna…"


translate italian day4_dv_172dcb0f:


    "Stavo per puntare la torcia verso quell'oscurità, quando mi sono accorto che ce l'aveva lei.{w} Beh, era prevedibile."


translate italian day4_dv_c22eb442:


    me "Sei ancora viva?"


translate italian day4_dv_3b4b97c0:


    dv "Sì…"


translate italian day4_dv_20abb91b:


    "La sua voce soffocata è risuonata dal basso."


translate italian day4_dv_f3139a0d:


    me "Ti sei rotta qualcosa?"


translate italian day4_dv_ce4becd7:


    dv "Non lo so, dannazione! Idiota!"


translate italian day4_dv_44682451:


    "Un fascio di luce ha brillato all'interno della buca. Mi sono avvicinato e mi sono chinato per esaminare quanto fosse profonda."


translate italian day4_dv_dd73bb1b:


    "Il buco fatiscente era profondo circa tre metri, ma era buio pesto là sotto."


translate italian day4_dv_5d83ec49:


    dv "Aiutami ad uscire!"


translate italian day4_dv_a342db42:


    "Ha gridato Alisa con rabbia."


translate italian day4_dv_ea49f8ff:


    me "E come credi che io possa riuscirci?"


translate italian day4_dv_6be52e6b:


    dv "Non lo so! Inventati qualcosa!"


translate italian day4_dv_71d84df3:


    me "Beh, che cosa vedi laggiù? Descrivimelo."


translate italian day4_dv_af7113a7:


    dv "C'è una specie di tunnel e… Nah, è solo un tunnel."


translate italian day4_dv_b949157e:


    "Un tunnel? Che strano."


translate italian day4_dv_c9a2c926:


    dv "Dai, scendi!"


translate italian day4_dv_285fa6b8:


    me "E quella sarebbe la tua idea migliore? Forse è meglio se torno al Campo e prendo una corda."


translate italian day4_dv_7cc30c48:


    dv "Vuoi lasciarmi qui da sola?"


translate italian day4_dv_8f7c4481:


    "La voce di Alisa aveva perso gran parte della sua sfacciataggine."


translate italian day4_dv_fab07924:


    me "Ma saresti sola laggiù in ogni caso…"


translate italian day4_dv_93b5fc50:


    dv "Ehi!"


translate italian day4_dv_e2d0858a:


    "Ha gridato forte, abbagliandomi con il fascio di luce della torcia."


translate italian day4_dv_0f7a1181:


    me "Va bene, niente panico, non verrai mangiata dai troll, tornerò in un lampo!"


translate italian day4_dv_66878d88:


    "Alisa ha gridato qualcosa da là sotto, ma non sono riuscito a distinguere le parole."


translate italian day4_dv_7514d7d5:


    "Tuttavia, sono riuscito a fare solo qualche passo prima che il terreno cedesse sotto ai miei piedi, facendomi precipitare verso il basso."


translate italian day4_dv_85879201:


    "Mentre scivolavo sono riuscito ad afferrare una sottile radice che in qualche modo mi ha rallentato..."


translate italian day4_dv_de9de6d3:


    "... Ma si è spezzata quasi subito, volando giù con me."


translate italian day4_dv_ce617998:


    "…"


translate italian day4_dv_006481b5:


    dv "Sei tutto intero?"


translate italian day4_dv_63fdaa9f:


    "Sono riuscito a malapena ad aprire un occhio quando sono stato abbagliato dalla luce sopra di me."


translate italian day4_dv_aababdd9:


    me "Argh, mettila via…"


translate italian day4_dv_bdc0c338:


    "Alisa era accovacciata accanto a me e mi guardava nervosamente."


translate italian day4_dv_4ddc9f96:


    dv "Ti sei rotto una gamba?"


translate italian day4_dv_7479b07e:


    me "Non lo so…"


translate italian day4_dv_322546b6:


    "Riuscivo a muovere le braccia e le gambe – tutto sembrava essere a posto."


translate italian day4_dv_217dc2c0:


    "Data l'altezza di quella caduta, sono stato davvero fortunato ad essere ancora vivo!"


translate italian day4_dv_f32b91db:


    "Mi sono alzato in piedi con molta fatica, esaminando il luogo dell'incidente."


translate italian day4_dv_e2ce46b6:


    "Eravamo effettivamente in una specie di lungo corridoio. C'erano dei grossi cavi lungo le pareti, e delle lampade incassate dentro a pesanti supporti metallici attaccati al soffitto."


translate italian day4_dv_88cece4e:


    "In qualsiasi altro contesto avrei pensato che si trattasse di una galleria di servizio della metropolitana, ma era poco probabile che «Sovyonok» avesse già raggiunto un tale livello di infrastrutture…"


translate italian day4_dv_d359bcda:


    dv "Dove siamo?"


translate italian day4_dv_c45d86b6:


    me "Non lo so, ma dobbiamo uscire da qui!"


translate italian day4_dv_45db0ef2:


    "Era inutile cercare di risalire lungo la parete. Infatti non riuscivo proprio a capire come siamo riusciti a sopravvivere a una caduta del genere."


translate italian day4_dv_638df9b4:


    me "Beh, sembra proprio che dobbiamo cercare un'altra via d'uscita."


translate italian day4_dv_c56fe6de:


    dv "E come facciamo?"


translate italian day4_dv_9d41ebbb:


    "Ha chiesto Alisa, impallidendo lentamente."


translate italian day4_dv_7507cfac:


    "Sul suo viso non è rimasta neppure una traccia della sua solita arroganza."


translate italian day4_dv_333f2018:


    me "Vediamo… Deve pur esserci un'uscita da qualche parte, giusto?"


translate italian day4_dv_91700607:


    dv "Può darsi, ma che direzione prendiamo?"


translate italian day4_dv_c59d4b63:


    th "Bella domanda."


translate italian day4_dv_7c3c6bab:


    "A prima vista sembrava saggio dirigersi verso il Campo (o almeno dove pensavo che fosse), ma non vedevo nulla che assomigliasse a un'uscita in quelle catacombe.{w} E ciò significava che saremmo dovuti andare nella direzione opposta."


translate italian day4_dv_8bb4bd4e:


    me "Dammela!"


translate italian day4_dv_f3a15952:


    "Ho strappato la torcia dalle mani di Alisa e l'ho puntata verso l'oscurità."


translate italian day4_dv_c31cbf17:


    me "Di là!"


translate italian day4_dv_ce617998_1:


    "…"


translate italian day4_dv_7fb53b2d:


    "Procedevamo lentamente, mentre Alisa mi teneva stretto per mano."


translate italian day4_dv_bef06bb8:


    "In altre circostanze ne sarei rimasto sorpreso, imbarazzato, o persino contento (chi lo sa?), ma in quel momento l'unica cosa che mi interessava era uscire di lì il prima possibile."


translate italian day4_dv_8fa117b1:


    "I lividi che mi ero fatto durante la caduta mi stavano facendo male. Il sangue mi pulsava nella testa come un pesante pendolo. E persino lo sfarfallio continuo della torcia, che illuminava sempre le stesse pareti di cemento, non mi stava consolando."


translate italian day4_dv_e7613964:


    me "Dannazione. C'è anche Shurik…"


translate italian day4_dv_e0ebb201:


    dv "Cosa?"


translate italian day4_dv_acbe50d2:


    me "Perché siamo venuti nella foresta, in primo luogo? Per trovare Shurik!"


translate italian day4_dv_f174d829:


    dv "Oh, dimenticati di lui."


translate italian day4_dv_622bba83:


    me "Potrebbe essere caduto quaggiù proprio come noi…"


translate italian day4_dv_3d5f8ddf:


    "Da un lato Alisa aveva ragione – questo non era esattamente il momento migliore per intraprendere una missione di ricerca. Se non avessimo trovato un'uscita, allora ci sarebbe stata una missione di ricerca per noi."


translate italian day4_dv_b6c57128:


    "Ma non riuscivo proprio a scacciare quel pensiero dalla mia testa."


translate italian day4_dv_fb54620a:


    "Era come se Shurik si trovasse proprio davanti ai miei occhi, guardandomi con rimprovero."


translate italian day4_dv_b0dd778f:


    dv "Attento!"


translate italian day4_dv_9d770d3f:


    "Ha gridato Alisa."


translate italian day4_dv_79b85d9a:


    "Ho quasi sbattuto contro un'enorme porta di metallo…"


translate italian day4_dv_d0842bb5:


    "Un simbolo di rischio biologico ha catturato la mia attenzione."


translate italian day4_dv_a9087c69:


    me "Questo significa che siamo in un rifugio antiaereo…"


translate italian day4_dv_c11bed91:


    "Ho sussurrato sottovoce."


translate italian day4_dv_aa5fd937:


    dv "Sì, ho sentito qualcosa a riguardo…"


translate italian day4_dv_017f286b:


    me "Davvero? Perché non me l'hai detto prima?"


translate italian day4_dv_10384b59:


    dv "E come facevo a sapere che era importante?"


translate italian day4_dv_7ecde56e:


    me "Forse hai ragione… Bene."


translate italian day4_dv_7db5329b:


    "Ho afferrato la ruota della porta e l'ho spinta con tutte le mie forze, cercando di farla girare."


translate italian day4_dv_80cc6fd2:


    "Il metallo arrugginito ha scricchiolato, ma non voleva cedere."


translate italian day4_dv_1f8abc27:


    me "Che stai aspettando, aiutami!"


translate italian day4_dv_0398ce56:


    "Alisa ha esitato un po' ma alla fine ha afferrato la maniglia e abbiamo iniziato a spingere insieme."


translate italian day4_dv_6ec0b8eb:


    "Finalmente siamo riusciti a farla girare a la porta si è aperta."


translate italian day4_dv_47dd4261:


    "Sarebbe stato difficile scambiare quella stanza per qualcos'altro. Era proprio la stanza di un rifugio antiaereo."


translate italian day4_dv_75eef015:


    "Armadi pieni di maschere antigas e cibi in scatola, innumerevoli attrezzature, letti a castello e sistemi di ricircolo dell'aria – tutto quello che sarebbe servito per sopravvivere a una guerra nucleare.{w} O almeno per sopravvivere al primo impatto."


translate italian day4_dv_c5fd3f99:


    "Alisa si è stretta ancora più forte al mio braccio."


translate italian day4_dv_ad02f53a:


    me "Cos'hai?"


translate italian day4_dv_35f94c91:


    dv "Come «cos'ho»…?"


translate italian day4_dv_04f9ace4:


    "Ha blaterato sottovoce."


translate italian day4_dv_a1e11f2c:


    dv "Ho paura…"


translate italian day4_dv_958e50a5:


    me "E di cosa?"


translate italian day4_dv_be3d8921:


    th "Già. Di cosa?"


translate italian day4_dv_ad914bab:


    th "Siamo solo caduti in un rifugio antiaereo abbandonato, brancolando nel buio, cercando di trovare una via d'uscita…{w} Già, proprio niente di cui aver paura."


translate italian day4_dv_1664c72b:


    "Quei pensieri mi hanno fatto rabbrividire, e mi sono istintivamente avvicinato ad Alisa."


translate italian day4_dv_9f11ce03:


    "Lei non ha detto una parola, si è limitata a distogliere lo sguardo, arrossendo un po'."


translate italian day4_dv_e789012e:


    me "Beh, in ogni caso…"


translate italian day4_dv_e0ebb201_1:


    dv "Cosa?"


translate italian day4_dv_c7ebc638:


    me "Dobbiamo continuare a cercare un'uscita."


translate italian day4_dv_3614f1a5:


    dv "Ecco un'altra porta."


translate italian day4_dv_ded6f407:


    "C'era davvero un'altra porta sulla parete in fondo, uguale a quella da cui eravamo appena entrati."


translate italian day4_dv_1695119f:


    "Tuttavia, non ne voleva sapere di farsi aprire."


translate italian day4_dv_5961a3eb:


    dv "Ti serve una mano?"


translate italian day4_dv_96fbd823:


    me "No, si è proprio inceppata…"


translate italian day4_dv_7b6b83d1:


    "Mi sono guardato attorno, cercando qualcosa che potessi usare come leva, e ho notato un piede di porco nell'angolo in fondo."


translate italian day4_dv_a499094f:


    me "Perfetto!"


translate italian day4_dv_223ad1b0:


    "Alisa mi ha guardato senza capire."


translate italian day4_dv_70066e56:


    me "Attenta!"


translate italian day4_dv_b614218a:


    "Tuttavia, nemmeno con il piede di porco sono riuscito a smuovere quella porta di un solo centimetro…"


translate italian day4_dv_152a52ab:


    "Totalmente esausto, mi sono lasciato cadere su uno dei letti ordinatamente rifatti, e ho fatto un sospiro."


translate italian day4_dv_32250ada:


    me "Dobbiamo tornare indietro."


translate italian day4_dv_11b295cf:


    dv "Ma siamo appena venuti da lì!"


translate italian day4_dv_b3c4b2e1:


    "Alisa sembrava aver ripreso un po' del suo autocontrollo, e della sua arroganza."


translate italian day4_dv_f527e7c1:


    me "Beh, te ne puoi stare qui se vuoi – c'è tutto: acqua, cibo in scatola, forse anche la radio funziona…"


translate italian day4_dv_f67a7d59:


    "Mi ha guardato con cattiveria, ma non ha detto niente."


translate italian day4_dv_0c0902a5:


    "Mi faceva male dappertutto."


translate italian day4_dv_0d661aed:


    "Me ne sono accorto solo adesso che ho iniziato a riposarmi un po'."


translate italian day4_dv_0e1194d0:


    "Una ferita profonda sulla mia gamba sanguinava copiosamente, le mie mani erano ricoperte di lividi, e del sangue mi si stava seccando nei capelli."


translate italian day4_dv_d39992ac:


    me "E tu… stai bene?"


translate italian day4_dv_4b680097:


    "Le ho chiesto, forse solo per cortesia, o forse perché mi preoccupavo per lei."


translate italian day4_dv_aa62d7ce:


    "Ci ha messo un po' a rispondere."


translate italian day4_dv_413f8d3c:


    dv "Come potremmo stare bene in una situazione del genere?"


translate italian day4_dv_c71c3992:


    me "Beh, intendevo questo…"


translate italian day4_dv_7c28149f:


    "Le ho indicato la ferita sulla mia gamba."


translate italian day4_dv_8298da02:


    dv "Oh!"


translate italian day4_dv_16d734ec:


    "Ha gridato Alisa."


translate italian day4_dv_3bf9faff:


    dv "Dobbiamo disinfettarla, e subito!"


translate italian day4_dv_bee1cd2f:


    me "Non serve. Sopravvivrò."


translate italian day4_dv_3044c877:


    dv "Oh piantala! Dobbiamo farlo, o rischierai un'infezione e poi dovremo amputarti la gamba! Mio nonno ha perso la gamba in guerra per lo stesso motivo."


translate italian day4_dv_83a7a438:


    "Alisa sembrava davvero preoccupata, così non mi sono opposto."


translate italian day4_dv_21b14d25:


    me "Ok, ma siamo molto lontani dall'infermeria."


translate italian day4_dv_6c7839f5:


    dv "Aspetta."


translate italian day4_dv_f08e28b7:


    "Ha sorriso e ha cominciato a rovistare tra gli armadietti."


translate italian day4_dv_d617f7d1:


    "C'era una buona probabilità di trovare qualche tipo di medicina in un rifugio antiaereo, e poco dopo Alisa ha trovato un kit di pronto soccorso, con sguardo trionfante."


translate italian day4_dv_68a3327c:


    dv "Ok, tieni duro."


translate italian day4_dv_8cc97caa:


    me "Ci proverò."


translate italian day4_dv_6db936c0:


    "Il batuffolo di cotone imbevuto di iodio era come un ferro rovente. Ho stretto forte i denti, cercando di resistere al dolore."


translate italian day4_dv_cc381102:


    dv "Oh, andiamo, non è poi così doloroso."


translate italian day4_dv_73eef208:


    me "Lo è! Forse vorresti provare a stare al mio posto?"


translate italian day4_dv_74a128a2:


    "Con le mie altre ferite è stato più facile, e ben presto ero tutto ricoperto di macchie marroni, come un leopardo."


translate italian day4_dv_73aab3cd:


    me "Grazie…"


translate italian day4_dv_16cc6217:


    dv "Non farti strane idee, dovevamo curare quelle ferite per forza!"


translate italian day4_dv_f505265f_1:


    "Ha sbuffato Alisa, dandomi le spalle."


translate italian day4_dv_1c08f469:


    me "Certo, certo…"


translate italian day4_dv_ec44416e:


    "Mi sono trattenuto dal discutere con lei e mi sono rialzato."


translate italian day4_dv_9322dc07:


    "Persino quel breve riposo è stato abbastanza per me."


translate italian day4_dv_ca0fc878:


    me "Bene, andiamo?"


translate italian day4_dv_ce617998_2:


    "…"


translate italian day4_dv_fbd5ef22:


    "Alisa si è avvinghiata al mio braccio, e abbiamo camminato vicini."


translate italian day4_dv_5adb4b2c:


    "Siamo tornati da dov'eravamo venuti, cercando con attenzione un buco nel soffitto."


translate italian day4_dv_02fac7b0:


    "Quel tunnel sembrava non finire mai. Non riuscivamo più a trovare il punto in cui eravamo caduti, nemmeno dopo cinque o dieci minuti."


translate italian day4_dv_9f66e5e7:


    dv "Ci siamo persi?"


translate italian day4_dv_bf52bfa2:


    "Ha chiesto Alisa con tristezza."


translate italian day4_dv_3b34fb45:


    me "Di solito ci si perde nella foresta…"


translate italian day4_dv_251da636:


    "E mi è appena venuto in mente che era proprio quello che ci era successo lassù mezz'ora fa, ma per evitare di spaventarla ancora di più, ho continuato in tono sicuro:"


translate italian day4_dv_722c0913:


    me "Ma questo tunnel ha una sola direzione – è impossibile perdersi!"


translate italian day4_dv_9b43edaf:


    dv "E allora dove credi che ci porterà?"


translate italian day4_dv_61395ab8:


    "Sembrava che le mie parole non fossero riuscite a rassicurarla abbastanza..."


translate italian day4_dv_070a72a2:


    me "Ce ne andremo da qui, c'è sempre una via d'uscita."


translate italian day4_dv_6ed2d8af:


    dv "Davvero?"


translate italian day4_dv_b0ed4000:


    me "Certo che sì! I lavoratori che l'hanno costruito dovevano per forza tornare in superficie in qualche modo, non è forse così?"


translate italian day4_dv_bf62b6f0:


    dv "Sì, credo."


translate italian day4_dv_faa0cd99:


    "Mi sembrava che stesse cercando di fare la dura come al solito, ma non le riusciva molto bene. Nella penombra era evidente che stava tremando. Un'espressione di estrema fatica era stampata sul suo volto."


translate italian day4_dv_31843b52:


    me "Bene. Andiamo avanti!"


translate italian day4_dv_ce617998_3:


    "…"


translate italian day4_dv_36099f9d:


    "Dopo pochi minuti ho notato un buco nel pavimento.{w} Sembrava un cratere generato da un'esplosione, ed era abbastanza ampio da poterci passare."


translate italian day4_dv_4453f8b0:


    dv "Che c'è laggiù?"


translate italian day4_dv_7479b07e_1:


    me "Non ne ho la minima idea…"


translate italian day4_dv_606bacdd:


    "Mi sono messo in ginocchio e ho puntato la torcia verso il basso. Terreno umido, una sorta di binari."


translate italian day4_dv_d07b5181:


    me "Dev'essere una miniera."


translate italian day4_dv_47a46c70:


    "Alisa mi ha guardato con aria interrogativa."


translate italian day4_dv_1e95970b:


    "Di certo saremmo potuti scendere facilmente là sotto, ma il mio istinto mi diceva che non avremmo trovato nulla di buono laggiù – forse solo un altro vicolo cieco o un'altra porta bloccata."


translate italian day4_dv_675becdd:


    "D'altra parte, sarebbe stato ingenuo pensare di trovare una via d'uscita nelle miniere."


translate italian day4_dv_4b228483:


    me "Possiamo fare un tentativo. Nel peggiore dei casi ci limiteremo a tornare – è abbastanza basso per riuscire a risalire."


translate italian day4_dv_a4cb18c4:


    "Il suo viso ha assunto un'espressione sofferente. Poi Alisa ha fatto un passo indietro dalla luce e ha detto piano:"


translate italian day4_dv_632d7403:


    dv "Se lo dici tu…"


translate italian day4_dv_ce617998_4:


    "…"


translate italian day4_dv_78bd231b:


    "Come pensavo, era una miniera. Chilometri di rotaie arrugginite si estendevano lungo i corridoi. Riuscivo a immaginare i carri scorrere su quelle rotaie secoli fa. Le pareti erano sostenute da travi marce, e l'acqua di tanto in tanto gocciolava dal soffitto."


translate italian day4_dv_0ccb51dd:


    "Non sapevo se fosse più spaventoso qui o nei corridoi del rifugio di sopra. Direi che «lassù» era un ambiente post-apocalittico, mentre «quaggiù» regnava un medievalismo occulto."


translate italian day4_dv_89403983:


    "In ogni caso ero ancora più ansioso di uscire da qui, e dovevamo farlo il prima possibile."


translate italian day4_dv_710c355f:


    "Alisa è rimasta in silenzio assoluto da quando siamo scesi nella miniera. Era concentrata solo sul camminare, stringendomi la mano così forte che ho cominciato a preoccuparmi."


translate italian day4_dv_580235f4:


    me "Stai bene?"


translate italian day4_dv_e8936f84:


    dv "No."


translate italian day4_dv_a68bf534:


    "Ha detto a bassa voce."


translate italian day4_dv_fc571a13:


    me "Capisco che non si possa stare bene qui, ma tu come ti senti?"


translate italian day4_dv_3a950d86:


    dv "Bene."


translate italian day4_dv_d15a76eb:


    "Mi ha risposto, in tono indifferente."


translate italian day4_dv_448d2873:


    me "Meglio così, perché…"


translate italian day4_dv_9f378f6c:


    "Avevo proprio voglia di dirle «perché se andrai fuori di testa pure tu, sarà un bel massacro»."


translate italian day4_dv_64368234:


    me "Perché troveremo un'uscita. Ne sono sicuro."


translate italian day4_dv_887ed8a6:


    dv "Già…"


translate italian day4_dv_2528c1d3:


    "Non avrebbe resistito ancora a lungo."


translate italian day4_dv_24b0636a:


    th "Dobbiamo andare più veloci!"


translate italian day4_dv_0bcb931e:


    "Ho affrettato il passo, e dopo una decina di metri siamo giunti ad un bivio."


translate italian day4_dv_2ad79933:


    me "Bene…"


translate italian day4_dv_36bca938:


    dv "Allora, da che parte?"


translate italian day4_dv_af77dc81:


    "Alisa sembrava essersi scollegata totalmente dal mondo esterno, senza preoccuparsi di cosa ci sarebbe successo, o di come avremmo potuto abbandonare questo luogo."


translate italian day4_dv_411a0051:


    me "Fammi pensare..."


translate italian day4_dv_a6bcb093:


    th "A destra o a sinistra?{w} E se ci perdessimo…?"


translate italian day4_dv_4909406f:


    "Ho deciso di incidere un segno sulla parete, nel caso in cui ci fossimo perduti, ritornando di nuovo in questo punto."


translate italian day4_dv_272c439e:


    "Ho raccolto una pietra appuntita e ho inciso una croce su una delle travi."


translate italian day4_dv_803ee2f2:


    me "Ok, adesso possiamo proseguire…"


translate italian dv_mine_coalface_e40597aa:


    "Siamo già stati qui."


translate italian dv_mine_coalface_5b4c3c90:


    "Finalmente siamo arrivati a una specie di stanza piuttosto alta."


translate italian dv_mine_coalface_3ef8c583:


    "Anche se non era proprio una stanza – devono averla usata per scavare.{w} Probabilmente per raccogliere carbone, o forse oro."


translate italian dv_mine_coalface_3c54fe69:


    "Le pareti erano state spaccate con i picconi, o forse con un martello pneumatico."


translate italian dv_mine_coalface_c718c063:


    "Era buio pesto qui dentro, e quindi la nostra unica salvezza era la torcia."


translate italian dv_mine_coalface_b97863a3:


    th "Se si dovesse rompere, sarebbe la fine…"


translate italian dv_mine_coalface_58411121:


    "Ho notato uno straccio rosso sul pavimento."


translate italian dv_mine_coalface_ca0a4f90:


    "Era il foulard di un pioniere!"


translate italian dv_mine_coalface_96b3272c:


    "Shurik doveva essere qui nei dintorni."


translate italian dv_mine_coalface_93ec21f7:


    me "Shurik! Shurik!"


translate italian dv_mine_coalface_f23d45c0:


    "L'eco della mia voce risuonava nelle gallerie."


translate italian dv_mine_coalface_cb6219ee:


    dv "Perché stai gridando? Non è che lui possa sentirti..."


translate italian dv_mine_coalface_858fffec:


    me "Non si sa mai."


translate italian dv_mine_coalface_07c129c5:


    "Alisa non ha risposto, ha solo sospirato."


translate italian dv_mine_coalface_686bf61c:


    "La sala non aveva altre uscite."


translate italian dv_mine_coalface_7ea80226:


    th "Sicuramente ci sono dei luoghi in questa miniera che abbiamo trascurato…"


translate italian dv_mine_coalface_858b6cbe:


    th "E quindi dobbiamo continuare a cercare!"


translate italian dv_mine_exit_8daf197d:


    "Dopo la curva, la torcia ha rivelato una porta di legno."


translate italian dv_mine_exit_599ac8f7:


    me "Ecco qui, finalmente qualcosa!"


translate italian dv_mine_exit_16377e5b:


    dv "Lo credi davvero?"


translate italian dv_mine_exit_7434af59:


    "Alisa sembrava speranzosa."


translate italian dv_mine_exit_771aa502:


    me "Ma certo!"


translate italian dv_mine_exit_e2d76069:


    "A dire il vero, avevo i miei dubbi su cosa avremmo trovato dietro a quella porta."


translate italian dv_mine_exit_f3b9dae9:


    "Ma non avevamo comunque altra scelta."


translate italian dv_mine_exit_efd4a253:


    "Ho tirato la maniglia…"


translate italian dv_mine_exit_c4bfa0fb:


    "E siamo entrati in una specie di stanzino della caldaia, o uno sgabuzzino sotto al rifugio antiaereo."


translate italian dv_mine_exit_d9981fe3:


    "Il pavimento era ricoperto di bottiglie vuote, mozziconi di sigarette e altre sporcizie."


translate italian dv_mine_exit_2328f028:


    "Questo significava che doveva esistere un'uscita da queste caverne!"


translate italian dv_mine_exit_34338260:


    "Ho esaminato la stanza con la torcia, e nell'angolo in fondo ho visto…"


translate italian dv_mine_exit_d4831ba9:


    "Shurik, accovacciato e tutto tremante."


translate italian dv_mine_exit_b01a7f13:


    me "Shurik!"


translate italian dv_mine_exit_58f9ccc4:


    sh "Chi… chi va là?"


translate italian dv_mine_exit_377c5a6f:


    "Ha balbettato."


translate italian dv_mine_exit_4701c46e:


    me "Shurik! Ti abbiamo cercato per tutta la notte e tu te ne stai seduto qui?! Alzati!"


translate italian dv_mine_exit_3901fe16:


    sh "Non vado da nessuna parte…"


translate italian dv_mine_exit_6ebe4e41:


    "Ha iniziato con calma, ma ad ogni parola la sua voce si rafforzava sempre più, finché ha gridato a pieni polmoni."


translate italian dv_mine_exit_7979942d:


    sh "No, non andrò da nessuna parte con voi! Siete solo frutto della mia immaginazione! Non siete qui davvero! È un'allucinazione! Non è scientifico! Già, non è scientifico!"


translate italian dv_mine_exit_5857f574:


    me "Cosa vuoi dire con «non siete qui»?! Noi siamo proprio qui, e ti stavamo cercando!"


translate italian dv_mine_exit_783dbf4d:


    "Alisa fino a quel momento era rimasta in silenzio, ma poi ha lasciato andare la mia mano e ha fatto un paio di passi verso Shurik."


translate italian dv_mine_exit_8bdd7529:


    me "Cos'hai intenzione di fare...?"


translate italian dv_mine_exit_420d89ea:


    "Le ho chiesto a bassa voce."


translate italian dv_mine_exit_782e55fe:


    "Ha esitato un istante, ma poi si è avvicinata ancora di più a lui, ignorandomi."


translate italian dv_mine_exit_bfea42b5:


    "Alla fine, dopo aver raggiunto il pioniere scomparso, Alisa gli ha tirato un bel ceffone."


translate italian dv_mine_exit_3a43352d:


    dv "Tu! Siamo quasi morti per venire a cercarti! Quella dannata foresta, il rifugio, queste maledette miniere! Sono piena di lividi per colpa tua! E tu cosa fai, te ne stai seduto qui, ubriacandoti e guardando i ragnetti rosa, brutto bastardo?"


translate italian dv_mine_exit_af2a4a69:


    "Shurik l'ha fissata sbalordito."


translate italian dv_mine_exit_57632dcb:


    dv "E adesso, muovi il culo e andiamocene!"


translate italian dv_mine_exit_7e0c8149:


    sh "No…"


translate italian dv_mine_exit_fc567eb6:


    "Ha iniziato a sussurrare."


translate italian dv_mine_exit_7e0c8149_1:


    sh "No…"


translate italian dv_mine_exit_8f7b3a64:


    "Da qualche parte dentro di me, in qualche angolo remoto della mia consapevolezza, sentivo che qualcosa di veramente brutto stava per accadere."


translate italian dv_mine_exit_b5bdda9c:


    "E così è stato. Un presentimento, una premonizione, o forse una semplice analisi della situazione, dal modo in cui Shurik era seduto, dall'espressione sul suo volto, tutto indicava quale sarebbe stata la sua prossima mossa."


translate italian dv_mine_exit_94c2b3ff:


    "E forse tutto assieme. Mi sono lanciato su Alisa, il fascio di luce impazzito è rimbalzato da un punto all'altro della stanza, mostrando per un istante il volto di Shurik, contorto dalla follia."


translate italian dv_mine_exit_495d2be6:


    "Teneva in mano una sbarra di metallo, e se non avessi scansato Alisa all'istante, sicuramente le avrebbe fracassato la testa."


translate italian dv_mine_exit_0e566744:


    "Ma ce l'ho fatta."


translate italian dv_mine_exit_e6910a04:


    "Tutto è stato come un sogno: il grido lontano di una ragazza, un tonfo, la torcia che si allontanava e il silenzio surreale, interrotto solo dal respiro pesante di Shurik."


translate italian dv_mine_exit_4aee785c:


    "Mi ci è voluto un po' per riprendermi."


translate italian dv_mine_exit_de443349:


    me "Cosa stai facendo, cretino?!"


translate italian dv_mine_exit_faa7fd6f:


    "Ho controllato le mie braccia, erano a posto – la torcia aveva assorbito il colpo.{w} E quindi ci siamo ritrovati nel buio più totale…"


translate italian dv_mine_exit_2b53bb97:


    sh "Non mi prenderete! Non mi prenderete mai!"


translate italian dv_mine_exit_e324829f:


    "La risata isterica di Shurik stava svanendo rapidamente nell'oscurità."


translate italian dv_mine_exit_61203408:


    me "Ehi! Dove stai andando?! Torna subito qui!"


translate italian dv_mine_exit_e37edfba:


    sh "Non mi…"


translate italian dv_mine_exit_a8eff0da:


    "La sua voce alla fine si è dissolta in lontananza..."


translate italian dv_mine_exit_5919c4da:


    me "Che pazzoide…"


translate italian dv_mine_exit_4fd3278c:


    "Ho provato a rianimare la torcia nell'oscurità. Niente da fare, Shurik l'aveva disintegrata con quella sua sbarra. E poi ho pensato ad Alisa."


translate italian dv_mine_exit_a1e11399:


    me "Stai bene?"


translate italian dv_mine_exit_2a2977e4:


    "Ho sentito un breve singhiozzo, e poi il suo abbraccio che mi stringeva forte."


translate italian dv_mine_exit_0288e8be:


    me "Ok, va tutto bene, è tutto finito, andrà tutto bene…"


translate italian dv_mine_exit_f7c6dd5b:


    th "A dire il vero niente sta andando bene, ma devo in qualche modo consolarla, altrimenti non ne usciremo vivi."


translate italian dv_mine_exit_e77948eb:


    "Le ho accarezzato teneramente la testa."


translate italian dv_mine_exit_e7f9499f:


    th "Mi chiedo, Alisa che piange – è una cosa divertente?{w} Forse, ma non in queste circostanze…"


translate italian dv_mine_exit_a1e11f2c:


    dv "Ho paura…"


translate italian dv_mine_exit_cb460d93:


    me "Va tutto bene, anch'io ho paura…"


translate italian dv_mine_exit_abca858d:


    "Ho notato una forte ed improvvisa trasformazione in Alisa."


translate italian dv_mine_exit_7e138f85:


    th "Ma che diavolo, chiunque si spaventerebbe in questa situazione.{w} Inoltre, lei è una ragazza…"


translate italian dv_mine_exit_1e55b63f:


    th "Ma perché io invece non sono spaventato?{w} Anzi, non spaventato – perché riesco comunque a mantenere la mente lucida e dire cose sensate?"


translate italian dv_mine_exit_4a5234b5:


    "Forse perché dovevo badare non solo a me stesso?"


translate italian dv_mine_exit_8ce5aeb2:


    me "Ok, dai, dobbiamo andarcene da qui."


translate italian dv_mine_exit_5a998b89:


    dv "Aspetta, solo un secondo…"


translate italian dv_mine_exit_db7a783b:


    "Mi ha detto a bassa voce, mentre la stanza veniva illuminata dalla fioca luce intermittente di un accendino."


translate italian dv_mine_exit_c0d3e816:


    me "E quello da dov'è sbucato?"


translate italian dv_mine_exit_3d4c4201:


    dv "Prima, il monumento…"


translate italian dv_mine_exit_d01e1437:


    "Ha cercato di farmi un sorriso gentile e mi ha dato l'accendino."


translate italian dv_mine_exit_505d91ae:


    me "D'accordo…"


translate italian dv_mine_exit_863a1c4e:


    "Ho osservato rapidamente l'interno della stanza, in cerca di qualcosa da poter usare come torcia."


translate italian dv_mine_exit_d0ca8ec6:


    "Tra i detriti sul pavimento c'erano alcuni stracci strappati e un'asta di legno. Una delle bottiglie conteneva un qualche liquido che dall'odore sembrava alcool."


translate italian dv_mine_exit_c238b970:


    "In un modo o nell'altro, dopo un minuto avevo in mano un qualcosa che assomigliava ad una torcia ardente."


translate italian dv_mine_exit_93e8f263:


    me "Non so per quanto tempo durerà, dobbiamo muoverci in fretta."


translate italian dv_mine_exit_bf5b8ada:


    dv "E dove andiamo?"


translate italian dv_mine_exit_92ae42e9:


    "Alisa sembrava essersi ripresa."


translate italian dv_mine_exit_4a1ef41f:


    me "Torniamo indietro, così raggiungeremo il rifugio antiaereo. Almeno lì c'è la luce."


translate italian dv_mine_exit_ce1ee8c8:


    "Ha acconsentito in silenzio e mi ha preso per mano."


translate italian dv_mine_exit_ce617998:


    "…"


translate italian dv_mine_exit_9e59519b:


    "La torcia si stava spegnendo pian piano, dovevamo riaccenderla ogni minuto. Non c'era bisogno di essere un genio per capire che quello straccio aveva i minuti contati."


translate italian dv_mine_exit_4b60d852:


    "Avevo sempre più timore che non saremmo scampati all'oscurità di quella miniera. Non potevamo affidarci a quell'accendino."


translate italian dv_mine_exit_3d40f229:


    "Comunque, siamo riusciti a raggiungere abbastanza presto il buco dal quale eravamo scesi nella miniera – a quanto pare mi ricordavo il percorso."


translate italian dv_mine_exit_6afe7d5f:


    "Abbiamo faticato a risalire, arrampicandoci sulle rocce quasi alla cieca."


translate italian dv_mine_exit_15f32c97:


    dv "Ok, e adesso dove?"


translate italian dv_mine_exit_f590dde1:


    "Ha chiesto Alisa, riprendendo fiato."


translate italian dv_mine_exit_47bf9d1c:


    me "Cosa vuoi dire? Torniamo in quella stanza con i letti a castello e gli armadietti."


translate italian dv_mine_exit_ec19d6bf:


    dv "E dov'era?"


translate italian dv_mine_exit_5e081fd0:


    "Ho aperto la bocca per risponderle, ma mi sono reso conto di non avere la minima idea di dove fosse."


translate italian dv_mine_exit_61ac798c:


    "La buca sembrava la stessa da ogni angolazione, proprio come le pareti, il pavimento e il soffitto. Non c'era un singolo dettaglio che ci indicasse quale fosse la direzione giusta."


translate italian dv_mine_exit_f883183e:


    me "Beh…"


translate italian dv_mine_exit_7b1bbd69:


    dv "Non lo sai?"


translate italian dv_mine_exit_87e8b661:


    "Mi ha chiesto tristemente, e si è seduta per terra."


translate italian dv_mine_exit_5a7a7577:


    me "No."


translate italian dv_mine_exit_09882a0d:


    "Le ho risposto a bassa voce, spegnendo la torcia per risparmiarla, e mi sono seduto accanto a lei."


translate italian dv_mine_exit_816feedf:


    dv "Moriremo qui?"


translate italian dv_mine_exit_89d773f5:


    "Ha appoggiato la testa sulla mia spalla."


translate italian dv_mine_exit_c69e1f3a:


    "La voce di Alisa è suonata piatta, forse persino calma, ma potevo sentire che in realtà lei stava tremando – forse per il freddo, forse per la paura, o forse per entrambi."


translate italian dv_mine_exit_15d317c8:


    me "Non dire sciocchezze! Certo, le cose si stanno mettendo male, ma non è ancora il momento di dire «moriremo qui»… Vedrai che qualcuno verrà in nostro soccorso la mattina. Olga Dmitrievna chiamerà la polizia, quindi…"


translate italian dv_mine_exit_a74b8123:


    th "Mi piacerebbe crederci davvero."


translate italian dv_mine_exit_4677b777:


    dv "Capisco…"


translate italian dv_mine_exit_751ac511:


    me "Già…"


translate italian dv_mine_exit_ce617998_1:


    "…"


translate italian dv_mine_exit_b162cf42:


    "Ho perso la cognizione del tempo stando seduto lì…"


translate italian dv_mine_exit_d3c437c7:


    "Avevo il timore di andare «avanti» o «indietro», di fare una scelta. In entrambi i casi, la nostra torcia non sarebbe durata a lungo. Vagare nell'oscurità senza più alcuna fonte di luce sarebbe stato decisamente peggio che attendere i soccorsi seduti qui."


translate italian dv_mine_exit_2607a8c2:


    "Almeno così avremmo avuto ancora un po' di luce in caso di emergenza…"


translate italian dv_mine_exit_af386fc2:


    "All'improvviso l'eco di un rombo si è propagato nei corridoi."


translate italian dv_mine_exit_b03fee0e:


    th "Beh, ecco qui il nostro caso di emergenza!"


translate italian dv_mine_exit_40ea5406:


    "Quel rumore era dovuto sicuramente a una porta che si apriva."


translate italian dv_mine_exit_7b953cc3:


    me "Alzati!"


translate italian dv_mine_exit_a1c23a4f:


    "Ho tirato su Alisa bruscamente e ho acceso la torcia con le mani che mi tremavano."


translate italian dv_mine_exit_d75c13ce:


    dv "Cos'è stato?"


translate italian dv_mine_exit_93c290bf:


    "Ha chiesto con sgomento."


translate italian dv_mine_exit_e79963e5:


    me "Non lo so, ma dev'essere qualcuno. Dobbiamo scappare!"


translate italian dv_mine_exit_ce36a4aa:


    "A dire il vero, non ero sicuro che fosse una buona idea."


translate italian dv_mine_exit_228b3ef4:


    th "E se quel pazzo di un Shurik fosse ancora qui?{w} E comunque, quello di certo non è il male peggiore che posso immaginarmi!"


translate italian dv_mine_exit_43f41aac:


    "Ma non avevamo altra scelta."


translate italian dv_mine_exit_5e04ae36:


    "I sassi rotolavano sotto ai nostri piedi. Ho cercato disperatamente di non cadere giù, tenendo la torcia con una mano e trascinando Alisa con l'altra."


translate italian dv_mine_exit_19c33fc2:


    "Lei correva accanto a me in silenzio, respirando affannosamente."


translate italian dv_mine_exit_cd113210:


    "Avrei voluto davvero voltarmi e guardarla, ma non avevo né la forza né il tempo per farlo."


translate italian dv_mine_exit_8f8cca4d:


    "Alla fine la sagoma di una porta aperta è apparsa davanti ai nostri occhi, e mi sono precipitato all'interno della stanza del rifugio antiaereo, pronto ad affrontare ogni possibile nemico…"


translate italian dv_mine_exit_208a6c81:


    "Ma dentro era vuota…"


translate italian dv_mine_exit_60e62f39:


    "Ho esaminato rapidamente ogni angolo della stanza, per stanare mostri sconosciuti, Shurik, o qualsiasi altra entità maligna, ma era esattamente come l'avevamo lasciata."


translate italian dv_mine_exit_cf80a68b:


    "Tranne per la porta, l'altra porta…{w} Era aperta!"


translate italian dv_mine_exit_9e0f57f2:


    me "Qualcuno è passato di qui!"


translate italian dv_mine_exit_1c4ba667:


    "Ho annunciato trionfante."


translate italian dv_mine_exit_e12061da:


    dv "E chi?"


translate italian dv_mine_exit_f17c350e:


    me "Non lo so, forse Shurik…"


translate italian dv_mine_exit_b26a8910:


    dv "Ma come avrebbe fatto ad aprirla?"


translate italian dv_mine_exit_8a8827fd:


    me "E chi se ne frega! Andiamo!"


translate italian dv_mine_exit_ce617998_2:


    "…"


translate italian dv_mine_exit_dbf5f587:


    "C'era un altro corridoio dietro a quella porta, e saliva lentamente verso l'alto."


translate italian dv_mine_exit_6e4dbe96:


    th "Quindi ci stiamo avvicinando alla superficie."


translate italian dv_mine_exit_36ebc0c6:


    "Avevo ragione: dopo un centinaio di metri siamo arrivati a una scala che saliva fino ad una piccola botola sul soffitto."


translate italian dv_mine_exit_1c207941:


    "Non è stato molto difficile aprirla."


translate italian dv_mine_exit_4453f8b0:


    dv "Cosa c'è lassù?"


translate italian dv_mine_exit_4164ce88:


    me "Che importa, è sempre meglio che restare qui sotto!"


translate italian dv_mine_exit_6f888876:


    "Ho salito la scala e mi sono ritrovato circondato dal buio."


translate italian dv_mine_exit_6ca992b0:


    "Tuttavia, era decisamente {i}meno buio{/i} dell'oscurità che avevamo trovato là sotto. Dopo che i miei occhi si sono abituati, sono riuscito a scorgere i contorni dell'ambiente circostante, delineati dal chiaro di luna."


translate italian dv_mine_exit_8aeb5a0d:


    "Ho aiutato Alisa a salire, e siamo letteralmente saltati fuori dall'edificio."


translate italian dv_mine_exit_681894bf:


    "L'uscita dalle catacombe portava a una sorta di casa in rovina, che sembrava un'infermeria o una scuola di paese."


translate italian dv_mine_exit_24905350:


    dv "Dev'essere il vecchio campo…"


translate italian dv_mine_exit_630ee13c:


    "Alisa si è seduta su una delle altalene e si è asciugata il sudore dalla fronte con il dorso della mano."


translate italian dv_mine_exit_0adb5982:


    me "Capisco… Dunque siamo riusciti a trovarlo, dopotutto."


translate italian dv_mine_exit_ef2764bc:


    "Ho sorriso involontariamente."


translate italian dv_mine_exit_2a6404d7:


    dv "Cosa c'è da ridere?"


translate italian dv_mine_exit_76027b11:


    "Ha aggrottato la fronte e ha messo il broncio contrariata.{w} In altre parole, è tornata la solita Alisa."


translate italian dv_mine_exit_fc154605:


    me "Allora, non è più così pauroso qui, vero?"


translate italian dv_mine_exit_39ef9914:


    dv "Non ho avuto paura nemmeno là!"


translate italian dv_mine_exit_2de4d7dc:


    me "Certo, come no!"


translate italian dv_mine_exit_f8f4d1e6:


    dv "Davvero, non ero spaventata!"


translate italian dv_mine_exit_66368319:


    me "Ah, per favore…"


translate italian dv_mine_exit_0812a5df:


    "Ero arrabbiato."


translate italian dv_mine_exit_bb429265:


    th "Le sue parole mi hanno davvero ferito. Se non fosse stato per me, non sarebbe mai uscita viva da quella maledetta miniera!"


translate italian dv_mine_exit_25532b53:


    dv "Dove stai andando?"


translate italian dv_mine_exit_6773d0b2:


    "Mi ha chiesto in tono un po' meno sicuro, dopo che mi sono incamminato verso il Campo."


translate italian dv_mine_exit_19a037e5:


    me "Sto tornando al Campo."


translate italian dv_mine_exit_93b5fc50:


    dv "Ehi!"


translate italian dv_mine_exit_95ef7c80:


    "Alisa è saltata giù e ha iniziato a camminare accanto a me."


translate italian dv_mine_exit_fbe21e1f:


    "Per un istante ho avuto voglia di dirle qualcosa di sgradevole, ma non ero pronto per iniziare un altro litigio…"


translate italian dv_mine_exit_860a390f:


    "Abbiamo ritrovato piuttosto rapidamente il percorso che avevamo preso all'andata. Persino le due buche in cui eravamo caduti."


translate italian dv_mine_exit_18eb7fb3:


    "Erano a soli duecento metri dal vecchio campo, più o meno."


translate italian dv_mine_exit_ce617998_3:


    "…"


translate italian dv_mine_exit_47e214d8:


    "Mi sono fermato alla piazza, voltandomi verso Alisa."


translate italian dv_mine_exit_505d91ae_1:


    me "Bene…"


translate italian dv_mine_exit_8bfb630c:


    dv "Bene…"


translate italian dv_mine_exit_4d3e55f3:


    "Sembrava indecisa, o persino confusa."


translate italian dv_mine_exit_2e84db32:


    me "Allora…"


translate italian dv_mine_exit_887ed8a6:


    dv "Certo…"


translate italian dv_mine_exit_7f338ab0:


    "Da una parte volevo proprio gridarle addosso, rimproverarla, o persino insultarla.{w} Ma dall'altra, desideravo avere una conversazione seria con lei."


translate italian dv_mine_exit_a3c6e840:


    "Alla fine ho deciso di rimanere in silenzio, mi sono girato e mi sono incamminato lentamente verso la casetta della leader."


translate italian dv_mine_exit_5bda040e:


    "Ma qualcosa disturbava costantemente la quiete notturna.{w} Ho teso le orecchie e mi sono accorto che era il russare..."


translate italian dv_mine_exit_ff984fa0:


    "... di Shurik, che stava dormendo tranquillamente su una panchina!"


translate italian dv_mine_exit_95efcfe1:


    me "Ehi!"


translate italian dv_mine_exit_997fab3e:


    "Ho chiamato Alisa, che non si era ancora allontanata troppo."


translate italian dv_mine_exit_79d3bbc8:


    me "E tu, svegliati!"


translate italian dv_mine_exit_94f86bab:


    "Shurik è ritornato lentamente in sé, fissandoci con sguardo assente."


translate italian dv_mine_exit_a5cfef85:


    sh "Oh, è già mattina?"


translate italian dv_mine_exit_ec14641b:


    me "Mattina? Certo, come no…"


translate italian dv_mine_exit_565e9484:


    "A volte prima si parla, e poi si pensa."


translate italian dv_mine_exit_d3e52cd4:


    "Naturalmente, l'impulso viene generato nel cervello, poi la corrente passa attraverso i collegamenti neurali, trasmettendo quindi il comando al corpo."


translate italian dv_mine_exit_e6d884ea:


    "Ma certe volte il subconscio si manifesta più rapidamente della coscienza.{w} Ed è esattamente per questa ragione che il mio pugno è finito dritto sulla pancia di Shurik, e solo allora mi sono reso conto di quello che avevo fatto."


translate italian dv_mine_exit_92f15912:


    "Ha tossito, cercando di riprendere fiato, contorcendosi per il dolore, sulla panchina."


translate italian dv_mine_exit_8f6f0153:


    me "Cosa diavolo era tutta quella follia là sotto?!"


translate italian dv_mine_exit_c415cb10:


    "Mi sentivo già in colpa – non avrei dovuto trattarlo in modo così rude…"


translate italian dv_mine_exit_43efdcda:


    sh "Ma di cosa… di cosa stai parlando?"


translate italian dv_mine_exit_40c0b040:


    "Shurik mi ha guardato scioccato."


translate italian dv_mine_exit_1a18e0c9:


    me "Nella miniera!"


translate italian dv_mine_exit_a786120b:


    sh "Quale miniera?"


translate italian dv_mine_exit_d18f8765:


    "Ha girato la testa, totalmente confuso."


translate italian dv_mine_exit_1ccd3e61:


    sh "E perché sono qui?"


translate italian dv_mine_exit_a031441e:


    sh "E perché voi siete qui?"


translate italian dv_mine_exit_3f214d58:


    dv "Smettila di prenderci in giro!"


translate italian dv_mine_exit_3d3d749e:


    "Alisa ha fatto irruzione nella nostra conversazione."


translate italian dv_mine_exit_e44dae44:


    dv "Mi hai quasi uccisa là sotto! E adesso fai il finto tonto, come se niente fosse?!"


translate italian dv_mine_exit_f86b3336:


    sh "Cosa è successo?"


translate italian dv_mine_exit_7a20f42c:


    me "Davvero non te lo ricordi?"


translate italian dv_mine_exit_7e0c8149_2:


    sh "Zero totale…"


translate italian dv_mine_exit_1780b682:


    me "Ok, allora qual è l'ultima cosa che ricordi?"


translate italian dv_mine_exit_7843e662:


    "Shurik si è concentrato."


translate italian dv_mine_exit_32afcb7b:


    sh "Beh, stavo andando verso il vecchio campo la mattina. Ero sicuro di trovare dei componenti per il robot, così…"


translate italian dv_mine_exit_f95c9fc9:


    me "Così?"


translate italian dv_mine_exit_43309355:


    sh "E questo è tutto… Non ricordo più niente da allora. Poi mi sono svegliato qui."


translate italian dv_mine_exit_e94e8b48:


    "Ho fatto un pesante sospiro e mi sono voltato."


translate italian dv_mine_exit_357bf682:


    me "Beh, continua pure allora, sogni d'oro…"


translate italian dv_mine_exit_32b62074:


    dv "Ehi, dove stai...?"


translate italian dv_mine_exit_381b0677:


    "Ho ignorato Alisa e mi sono diretto verso la casetta di Olga Dmitrievna.{w} Lei è rimasta a discutere con Shurik."


translate italian dv_mine_exit_ce617998_4:


    "…"


translate italian dv_mine_exit_594f36e4:


    "Mi sono seduto sulla sedia a sdraio e ho guardato in alto verso le stelle."


translate italian dv_mine_exit_6b72237f:


    "Questa notte sembravano più luminose del solito."


translate italian dv_mine_exit_4b4166ca:


    "Forse così mi sembrava perché non molto tempo prima la mia unica fonte di luce era una debole torcia elettrica, e poi quella torcia infuocata con l'accendino."


translate italian dv_mine_exit_a3c69ac7:


    "Le stelle ovviamente sono più luminose di entrambe le torce.{w} La maggior parte delle stelle, credo, sono più luminose persino del sole, ma sono così lontane…"


translate italian dv_mine_exit_d806349a:


    me "Ebbene, perché sei venuta?"


translate italian dv_mine_exit_7fd15fd8:


    "Ho chiesto, senza nemmeno girare la testa – i passi di Alisa si potevano sentire con largo anticipo nel silenzio della notte."


translate italian dv_mine_exit_29cec6ff:


    dv "Ecco, io…"


translate italian dv_mine_exit_80ecf5fd:


    me "Cosa ti ha detto Shurik?"


translate italian dv_mine_exit_6a9ad2e5:


    dv "Mi ha detto che non si ricorda niente, che «non è scientifico» e sciocchezze simili."


translate italian dv_mine_exit_946b1e7d:


    me "Credo davvero che non si ricordi, a causa dello shock e dello stress."


translate italian dv_mine_exit_2a5b356a:


    th "E chi sono io per dirlo?{w} Mi trovavo nella sua stessa barca poco tempo fa."


translate italian dv_mine_exit_b9277d5e:


    th "Anzi, ci sono dentro ancora.{w} Soffro forse di amnesia?"


translate italian dv_mine_exit_9a5edc2d:


    me "Va bene allora, perché sei venuta qui?"


translate italian dv_mine_exit_fa9696b2:


    "D'altronde, sapevo già la risposta – ecco perché non ero andato subito a dormire ma l'ho aspettata qui."


translate italian dv_mine_exit_c9fe61bc:


    dv "Beh, io…"


translate italian dv_mine_exit_4eface95:


    "Alisa si è seduta accanto a me."


translate italian dv_mine_exit_4e4c10a9:


    dv "Volevo solo… Ringraziarti… Dopotutto, laggiù… Tu… Ecco, sai…"


translate italian dv_mine_exit_92279f92:


    me "Di niente."


translate italian dv_mine_exit_028267bd:


    "Le ho risposto gentilmente, e mi sono spinto indietro."


translate italian dv_mine_exit_9763a346:


    dv "Beh, va bene allora…"


translate italian dv_mine_exit_cb0a1b3d:


    "Si è alzata e ha fatto per andarsene."


translate italian dv_mine_exit_9492db5a:


    me "Se pensi che io sia arrabbiato con te, non lo sono. È tutto a posto."


translate italian dv_mine_exit_54ef0861:


    dv "A dire il vero non lo pensavo!"


translate italian dv_mine_exit_6f52c0b0:


    "Alisa si è alterata."


translate italian dv_mine_exit_712a2ea4:


    me "Va bene allora."


translate italian dv_mine_exit_8bfb630c_1:


    dv "Ok…"


translate italian dv_mine_exit_751ac511_1:


    me "Già…"


translate italian dv_mine_exit_a1bcd7f2:


    dv "E allora…"


translate italian dv_mine_exit_1bd407b9:


    me "Va' pure!"


translate italian dv_mine_exit_19ed4ed8:


    "Le ho detto gentilmente, agitando la mano."


translate italian dv_mine_exit_eb93b5d8:


    dv "Andrò quando voglio io!"


translate italian dv_mine_exit_cbd890f2:


    me "E quindi adesso non vuoi andare?"


translate italian dv_mine_exit_ebd91224:


    dv "Sì che lo voglio!"


translate italian dv_mine_exit_2b9ec1df:


    me "E dunque? C'è qualcosa che te lo impedisce?"


translate italian dv_mine_exit_82ddf868:


    dv "Coglione!"


translate italian dv_mine_exit_c46574f3:


    "Alisa ha battuto il piede e si è allontanata rapidamente dalla casetta della leader."


translate italian dv_mine_exit_b44e66a1:


    "Ho fatto un respiro profondo e mi sono alzato."


translate italian dv_mine_exit_d5cb476b:


    "La mia mente era annebbiata dalla fatica."


translate italian dv_mine_exit_05cac35e:


    th "Beh, almeno Olga Dmitrievna sta già dormendo, e non dovrò spiegarle ogni cosa…"


translate italian dv_mine_exit_8e63c1c0:


    "Tuttavia non mi è andata così di lusso."


translate italian dv_mine_exit_69a264ae:


    "La leader del Campo stava in piedi in mezzo alla stanza, e ovviamente si era preparata per una lunga conversazione."


translate italian dv_mine_exit_a38129f5:


    "O meglio – per un lungo Debriefing."


translate italian dv_mine_exit_d8b1d698:


    mt "Ti andrebbe di spiegarmi?"


translate italian dv_mine_exit_132385d6:


    me "Qual è il problema?{w} Non sembrava così preoccupata quando ci ha mandato da soli alla ricerca di Shurik."


translate italian dv_mine_exit_28247bed:


    mt "Allora, l'avete trovato?"


translate italian dv_mine_exit_ded1e9af:


    "Sembrava che le importasse più del fatto che io fossi rientrato tardi, piuttosto che del destino del pioniere scomparso."


translate italian dv_mine_exit_3385696b:


    me "Sì, l'abbiamo trovato.{w} Comunque, perché se ne sta in piedi lì al buio?"


translate italian dv_mine_exit_6334b91e:


    mt "Cosa?"


translate italian dv_mine_exit_f72b1120:


    me "Ho detto, perché al buio?"


translate italian dv_mine_exit_3597bffc:


    mt "Perché è ora di dormire."


translate italian dv_mine_exit_14658aae:


    "Ero decisamente d'accordo, anche se quel suo brusco cambiamento d'umore mi è sembrato un po' strano."


translate italian dv_mine_exit_a20cefa7:


    "..."


translate italian dv_mine_exit_a4baed18:


    "Ho raggiunto il mio letto, sfinito, e sono crollato su di esso senza nemmeno spogliarmi."


translate italian dv_mine_exit_b6a45476:


    th "Ma ancora, Alisa…{w} Alisa…"


translate italian dv_mine_exit_fff7cafa:


    "Non sapevo proprio cosa pensare di lei."


translate italian dv_mine_exit_f6f64c2c:


    "Non era per quel suo strano comportamento.{w} No, al contrario, il suo comportamento era del tutto coerente e comprensibile."


translate italian dv_mine_exit_b0dedce5:


    "Persino il fatto che fosse venuta a ringraziarmi."


translate italian dv_mine_exit_7bad5d82:


    "Ho pensato ad Alisa più di ogni altra cosa avvenuta questa notte."


translate italian dv_mine_exit_8dbd75e9:


    "E comunque, pensandoci bene, ogni cosa accaduta questa notte non aveva niente di soprannaturale."


translate italian dv_mine_exit_a701bda4:


    "Spiacevole – certo, spaventoso, pure agghiacciante – sicuro.{w} Ma che fosse in qualche modo legato al mio arrivo in questo Campo – improbabile."


translate italian dv_mine_exit_b249ce58:


    "Questi pensieri mi hanno cullato verso il sonno."


translate italian day4_us_aa8cb942:


    us "Puoi contare su di me!"


translate italian day4_us_13d9d85c:


    "Ho guardato Ulyana, stupito."


translate italian day4_us_2452df7b:


    "La sua sete di avventura sembrava non avere limiti."


translate italian day4_us_60164174:


    us "Pensaci! La notte, i fantasmi, un campo abbandonato – è grandioso!"


translate italian day4_us_bf708ac9:


    th "Da un lato, una compagnia del genere promette guai a non finire, ma dall'altro, è più sicuro andare in due…"


translate italian day4_us_003878d1:


    mt "Perfetto!"


translate italian day4_us_af906072:


    "Dopo aver salutato le altre ragazze e Olga Dmitrievna, siamo rimasti da soli."


translate italian day4_us_48cc7f3f:


    me "Pensi che questa sia solo una passeggiata divertente?"


translate italian day4_us_e8b3b40c:


    us "Beh, sì. Qual è il problema?"


translate italian day4_us_94876b55:


    "Ha fatto una risatina."


translate italian day4_us_b92e40c7:


    me "Non importa… Davvero…"


translate italian day4_us_ed0914b5:


    "Ho sospirato."


translate italian day4_us_2534cd63:


    us "Oh, aspetta!{w} Vado a prendere una torcia!"


translate italian day4_us_cd917d11:


    me "Ok."


translate italian day4_us_ad322fcb:


    "Stavo per suggerirglielo io stesso."


translate italian day4_us_73291f33:


    th "Sembra che non solo io debba visitare un vecchio campo abbandonato di notte, ma dovrò anche prendermi cura di una ragazzina irrequieta…"


translate italian day4_us_3ce9798d:


    "E dato che si trattava di Ulyana..."


translate italian day4_us_4da4d2d1:


    th "Dovrò stare doppiamente attento."


translate italian day4_us_7bd87323:


    "Electronik ci ha raccontato che il vecchio edificio è stato costruito appena dopo la guerra."


translate italian day4_us_fd3c1cdc:


    "Ha detto che sembrava un asilo (me lo immaginavo più come una caserma), e che quel vecchio campo era più piccolo del nostro."


translate italian day4_us_e9bbdb04:


    "È da una ventina d'anni che non veniva più usato."


translate italian day4_us_b5d9b351:


    "Ulyana è scattata in avanti, come se tutto questo fosse solo un gioco per lei."


translate italian day4_us_5f48e206:


    "Io francamente mi sentivo piuttosto a disagio."


translate italian day4_us_71ff7e44:


    "A dire il vero sarebbe stata una cosa normale nella mia situazione – vagare nella foresta di notte, in compagnia di animali spaventosi e volatili desiderosi di piombarti addosso, la luna piena, e tutto questo mondo sconosciuto in cui ero stato catapultato non molto tempo fa."


translate italian day4_us_1608aae1:


    "Forse avrei fatto meglio ad andare da solo, in modo da non dover tenere d'occhio Ulyana, che correva davanti a me."


translate italian day4_us_04ecc3b7:


    th "Ma come ha fatto a non inciampare ancora in qualcosa?"


translate italian day4_us_bd8fd797:


    me "Senti. Devi fare più attenzione…"


translate italian day4_us_d4cf3f77:


    us "Altrimenti cosa?"


translate italian day4_us_0382e70f:


    "Si è girata così di scatto che mi ha fatto venire i brividi."


translate italian day4_us_74e537ca:


    me "Altrimenti niente. Potresti farti male."


translate italian day4_us_aae12bf3:


    us "Ti preoccupi per me?"


translate italian day4_us_dd02f8d8:


    me "È ovvio. Cioè, è normale in queste situazioni."


translate italian day4_us_c0584c4d:


    "Ulyana ha messo il broncio."


translate italian day4_us_17717ee8:


    me "Ascolta…"


translate italian day4_us_6ba67613:


    "Volevo continuare la nostra conversazione – il viaggio sarebbe stato più rilassante e meno spaventoso."


translate italian day4_us_9d0c5198:


    me "Cos'era quella storia sul vecchio campo? Quello che hai detto prima alla piazza…"


translate italian day4_us_87d3ffe3:


    us "Già, è davvero un luogo spaventoso! Dicono che tutti i pionieri che ci sono andati sono morti, e che sono diventati dei fantasmi che custodiscono il loro ultimo rifugio terreno."


translate italian day4_us_7db7b174:


    me "E di cosa sarebbero morti?"


translate italian day4_us_3d71a838:


    "Era un po' difficile credere a quelle storielle."


translate italian day4_us_f62f4e9c:


    us "Che ne so? Non ero nemmeno nata in quel periodo."


translate italian day4_us_622c73dd:


    me "Ma l'hai detto con tanta sicurezza."


translate italian day4_us_2585e19d:


    us "Ho ottenuto le informazioni da una fonte affidabile!"


translate italian day4_us_578a42ac:


    me "E chi sarebbe la tua fonte affidabile? Alisa?"


translate italian day4_us_ccb042d0:


    us "Non te lo dico!"


translate italian day4_us_fa7e457b:


    me "E poi cosa è successo? Sono morti e si sono trasformati in fantasmi?"


translate italian day4_us_38197147:


    us "E questo è tutto…"


translate italian day4_us_974a0ad3:


    me "Cosa intendi dire con «questo è tutto»?"


translate italian day4_us_2dd06aae:


    us "Adesso le anime dei pionieri deceduti vagano per il campo in cerca di qualcuno che osi varcare la soglia del mondo dei morti!"


translate italian day4_us_7ba98731:


    me "Wow! Ok, muoviamoci!"


translate italian day4_us_ce617998:


    "…"


translate italian day4_us_71ed9190:


    "Il tempo scorreva. Siamo entrati nel cuore della foresta, dove gli alberi racchiudevano ogni cosa. Improvvisamente mi sono accorto che sulla foresta era piombato un silenzio di tomba."


translate italian day4_us_3e13ee96:


    "Come se gli uccelli notturni si fossero nascosti in attesa di qualcosa, e gli insetti fossero andati sotto terra. Persino il vento aveva smesso di soffiare."


translate italian day4_us_25d125dd:


    "Il chiaro di luna si faceva strada tra il fitto fogliame, e tutto ciò mi sembrava come un fascio di corde pizzicate."


translate italian day4_us_171ac87d:


    "Finalmente gli alberi hanno iniziato a diradarsi, e dinanzi a noi è apparsa una vasta radura…"


translate italian day4_us_609185f4:


    "In mezzo ad essa sorgeva un vecchio edificio che sembrava una scuola d'infanzia."


translate italian day4_us_56ad6a30:


    "Era avvolto da una fitta nebbia. Pareva che fossimo arrivati a un cimitero, e il vecchio edificio del campo sembrava proprio una cripta."


translate italian day4_us_883f8d73:


    "Secondo Ulyana in questo luogo vagavano le anime dei morti. E sembrava davvero un enorme cimitero…"


translate italian day4_us_b8d90de9:


    "Ho rabbrividito, stringendo la torcia ancora più forte."


translate italian day4_us_b92c533e:


    me "Davvero un posto terribile, a dire il vero…"


translate italian day4_us_4b95ee74:


    us "Oh, suvvia!"


translate italian day4_us_632560ea:


    "Mi ha dato una pacca sulla schiena gioiosamente, il che mi ha spaventato ancora di più."


translate italian day4_us_96025918:


    "Stavo per proseguire quando la luna è apparsa tra le nubi, illuminando la radura, il vecchio campo, e noi."


translate italian day4_us_d383d5fe:


    "Sotto quella luce tutto non appariva più così decrepito. Le pareti fatiscenti, lo scivolo arrugginito e il carosello, i pochi vetri che miracolosamente erano ancora rimasti intatti in alcune delle finestre: l'intero ambiente era diventato più vivido."


translate italian day4_us_90df7238:


    "Ho iniziato a immaginarmi dei mostri sconosciuti fuoriuscire dalla foresta, in un luogo di tenebre eterne dove nemmeno la luna riusciva a regnare."


translate italian day4_us_97e4fa69:


    th "Spero che almeno temano la luce, come i vampiri…{w} Oppure potrebbero trasformarsi in enormi lupi feroci al chiaro di luna, come i licantropi…"


translate italian day4_us_bc306349:


    us "Perché ti sei fermato?"


translate italian day4_us_1bb0a12c:


    me "Sto pensando…"


translate italian day4_us_6a81728a:


    us "A cosa?"


translate italian day4_us_61fe8457:


    me "Perché Shurik sarebbe venuto in un posto del genere?"


translate italian day4_us_5fd5e401:


    us "E che ne so? Glielo chiederemo non appena lo troviamo."


translate italian day4_us_ab70605d:


    me "Sì. Ovviamente…"


translate italian day4_us_60414457:


    "Ho borbottato, seguendo Ulyana."


translate italian day4_us_2a3004d5:


    "Adesso lei camminava con più cautela, guardando dove metteva i piedi, fermandosi ogni tanto, e persino guardandosi più volte alle spalle."


translate italian day4_us_39ff0700:


    "È naturale. In certi punti l'erba era così alta che non si poteva sapere cosa ci fosse sul terreno – rottami di metallo, pietre, vetri frantumati…"


translate italian day4_us_87203d37:


    "Finalmente abbiamo raggiunto l'entrata. Ulyana si è fermata e ha detto:"


translate italian day4_us_3110f8c7:


    us "Beh! Eccoci qua!"


translate italian day4_us_af4772b5:


    me "Come se tu avessi vinto una gara… Questo non è un gioco."


translate italian day4_us_05780ff1:


    us "Sei proprio… Sei proprio…"


translate italian day4_us_4e7dfaf3:


    me "Cosa?"


translate italian day4_us_024305f8:


    us "Noioso!"


translate italian day4_us_f39b4d19:


    "Ho fatto una faccia scontenta e sono entrato fiducioso nell'oscurità. Sarebbe stato pericoloso far entrare Ulyana per prima: pericoloso per lei, pericoloso per me, e forse anche per l'intera umanità."


translate italian day4_us_ec2f3403:


    "L'interno dell'edificio era ancora più deprimente. Mi è dispiaciuto un po' per esso.{w} O forse per le persone che ci avevano abitato in passato."


translate italian day4_us_e2350d58:


    "Certamente in passato doveva essere stato sempre colmo della gioia dei bambini. Vedevo quei pionieri correre per la sala, giocare a giochi ormai dimenticati, mentre una severa leader come Olga Dmitrievna manteneva l'ordine. Una sessione finiva, un'altra iniziava."


translate italian day4_us_b4495206:


    "Ed ora eccolo qui, fatiscente, in decomposizione, dimenticato da tutti."


translate italian day4_us_30f4be0c:


    us "Guarda!"


translate italian day4_us_ca4f2dbb:


    "Ulyana mi ha dato una bambola rovinata, che cadeva a pezzi a causa dell'umidità. Ecco un altro pezzo di passato."


translate italian day4_us_9bfb0137:


    me "Cos'ha di speciale?"


translate italian day4_us_3005a190:


    us "Niente…"


translate italian day4_us_8a70ba7f:


    "Si è allontanata dalla luce, ma sono riuscito a scorgere un'espressione triste sul suo volto."


translate italian day4_us_4280984e:


    me "Triste come un cimitero."


translate italian day4_us_614210cb:


    us "Spaventoso!"


translate italian day4_us_64a3eba4:


    me "Io non ho ancora visto niente di spaventoso."


translate italian day4_us_fbc2f69e:


    "Era davvero più rilassante dentro che fuori."


translate italian day4_us_a9de51da:


    "Sembrava proprio un cimitero, come quando si cerca una tomba specifica camminando lungo le file interminabili di lapidi, provando un certo disagio interiore, ma poi la si trova e l'anima si mette in pace.{w} Come se si stesse sdraiati accanto ad essa…"


translate italian day4_us_311704a0:


    "Ho rabbrividito e ho lasciato che la torcia esplorasse i dintorni.{w} Non c'era traccia di Shurik."


translate italian day4_us_d6c1b117:


    "E cosa mi aspettavo di trovare qui? Il suo cadavere?"


translate italian day4_us_7b1fd8de:


    me "Sembra che lui non sia qui…"


translate italian day4_us_36335213:


    us "Forse è al secondo piano?"


translate italian day4_us_b01a7f13:


    me "Shurik!"


translate italian day4_us_bf4be854:


    "L'ho chiamato ad alta voce, ma l'unica risposta che ho ottenuto è stata quella del mio eco."


translate italian day4_us_1f4ba85c:


    us "Shurik! Vieni fuori!"


translate italian day4_us_8a79c954:


    me "Come puoi vedere…"


translate italian day4_us_883708ad:


    us "Dovremmo controllare comunque."


translate italian day4_us_53c9fe63:


    me "Ok, ok…"


translate italian day4_us_ce617998_1:


    "…"


translate italian day4_us_784add86:


    "Non c'erano segni di vita nemmeno al piano superiore."


translate italian day4_us_ae6a6307:


    "Mi sono seduto sulle scale con la testa fra le mani, rassegnato."


translate italian day4_us_080b0b24:


    me "E adesso dove cerchiamo? Non possiamo setacciare l'intera foresta. E io mi sto quasi addormentando…"


translate italian day4_us_66088108:


    us "Perché fai così tanti capricci?"


translate italian day4_us_8f18aa98:


    "Ulyana era irritata dal mio comportamento."


translate italian day4_us_8285506c:


    us "Sempre a lamentarti. Come un bambino."


translate italian day4_us_f75ea3f6:


    me "Oh, perché non ho forse ragione a lamentarmi di questa situazione?"


translate italian day4_us_d92dc560:


    us "Ragione o torto – che differenza fa? Se il nostro compito è quello di cercare Shurik, dobbiamo trovarlo in ogni caso!"


translate italian day4_us_04098bfe:


    me "Ma in che modo?"


translate italian day4_us_a43ed67b:


    "Ho supplicato."


translate italian day4_us_78f6e14d:


    us "Non lo so! In qualche modo!"


translate italian day4_us_add3ace1:


    "Ulyana sembrava una maestra severa, e io il suo allievo disattento."


translate italian day4_us_1a536bbc:


    th "Non dovrebbe essere il contrario?"


translate italian day4_us_30f4be0c_1:


    us "Guarda!"


translate italian day4_us_0939ec56:


    "Ho puntato la torcia verso il punto che mi ha indicato e ho notato una botola nell'angolo, circondata dalla sporcizia.{w} Sembrava fosse stata aperta da poco."


translate italian day4_us_4e111b28:


    us "Dev'essere là sotto, di sicuro!"


translate italian day4_us_ede7df57:


    "Ulyana ha afferrato la maniglia e ha tirato con tutte le sue forze."


translate italian day4_us_92badaea:


    me "Perché dovrebbe essere andato laggiù? Forse è stato… Non saprei. Magari qualche abitante del villaggio. C'è forse un villaggio nei dintorni?"


translate italian day4_us_930ac3c9:


    us "Non lo so…"


translate italian day4_us_cb4ca95f:


    "Ha risposto Ulyana, ansimando.{w} Non era riuscita ad aprire quella botola."


translate italian day4_us_89141735:


    "Ho iniziato a pensare."


translate italian day4_us_2f3a091c:


    "Certamente non potevamo escludere del tutto la possibilità che Shurik fosse effettivamente laggiù."


translate italian day4_us_b73efd8f:


    "Dopotutto non conoscevo molto bene questo mondo. Per la precisione, non lo conoscevo affatto. Perché mi aspettavo che la gente del posto si comportasse secondo la mia logica?{w} Forse si è nascosto lì dentro per sfuggire ai lupi?"


translate italian day4_us_8e32b775:


    th "Sempre che ci siano i lupi qui. Forse ci sono solo i gufi."


translate italian day4_us_9dc26b9a:


    me "D'accordo, diamo un'occhiata…"


translate italian day4_us_a11b339c:


    "Ho teso i muscoli e sono riuscito ad aprire la botola."


translate italian day4_us_93757775:


    "Si è schiantata pesantemente sul pavimento di legno e, nello stesso istante, Ulyana si è affacciata al boccaporto, illuminando il passaggio con la torcia."


translate italian day4_us_369cfea8:


    us "È una specie di tunnel!"


translate italian day4_us_631fa55f:


    me "Un tunnel?"


translate italian day4_us_b405ffe1:


    "L'ho scansata indietro tirandola per il colletto, per poter dare un'occhiata di persona."


translate italian day4_us_d5d98923:


    us "Ehi!"


translate italian day4_us_e4344f7f:


    me "La curiosità uccise il gatto!"


translate italian day4_us_8b032119:


    "C'era davvero un lungo tunnel che scendeva in profondità. Sembrava il dungeon di un videogioco."


translate italian day4_us_53694ead:


    "A prima vista non c'era nessun pericolo: nessun'acqua profonda, niente ratti, né morti viventi…"


translate italian day4_us_4fbc46ec:


    me "Ok. Scendiamo giù per la scala. Ma fa' attenzione!"


translate italian day4_us_c0841288:


    us "Ricevuto!"


translate italian day4_us_b8db1da9:


    "Ha detto Ulyana, raggiante."


translate italian day4_us_ce617998_2:


    "…"


translate italian day4_us_c2440a9c:


    "Faceva davvero buio là sotto. La torcia non era di grande aiuto. Illuminava fiocamente le pareti di cemento, le lampade appese al soffitto e aggrovigliate tra i fili, e una marea di spazzatura sul pavimento."


translate italian day4_us_34477885:


    "Abbiamo proseguito lentamente. Tenevo Ulyana per mano, per evitare che corresse più avanti."


translate italian day4_us_f835b5ac:


    "Probabilmente ero un po' preoccupato anche per me stesso, non volendo rimanere da solo in quella fitta oscurità."


translate italian day4_us_c6aac5b3:


    me "Forse dovremmo tornare indietro? Shurik non può essersi spinto così in profondità…"


translate italian day4_us_951106fa:


    us "Forse aveva anche lui una torcia?"


translate italian day4_us_8a8c5c02:


    me "Beh, anche se avesse una torcia… Perché dovrebbe essere venuto qui?"


translate italian day4_us_3e242075:


    us "Non lo so! Non sei curioso di scoprire cosa c'è più avanti?"


translate italian day4_us_990233c8:


    me "Nemmeno un po', sono più interessato a…"


translate italian day4_us_54c2445e:


    "Non sono riuscito a finire la frase – un'enorme porta di metallo è apparsa improvvisamente davanti ai nostri occhi."


translate italian day4_us_87a07145:


    "Ho subito notato il simbolo di rischio biologico."


translate italian day4_us_28acc263:


    me "Un rifugio antiaereo?"


translate italian day4_us_5211f0b6:


    "L'ho capito subito."


translate italian day4_us_41e59a3d:


    us "Probabile."


translate italian day4_us_171958cc:


    me "Ne sai qualcosa?"


translate italian day4_us_28220b96:


    us "No. Che importa?"


translate italian day4_us_4f8c4714:


    me "Che importa...? E se ci fossero delle radiazioni?"


translate italian day4_us_7ef85af6:


    us "Che radiazioni? Da dove?"


translate italian day4_us_38d6ffbb:


    me "È vero, hai ragione…"


translate italian day4_us_9bb98464:


    "Ho afferrato la ruota della porta e ho cercato di girarla."


translate italian day4_us_8cb0acfd:


    "Con mia sorpresa sono riuscito a ruotarla, facendo un rumore che sembrava il gemito di un dinosauro morente."


translate italian day4_us_8bd067c0:


    "Alla fine la porta si è aperta, e Ulyana è balzata dentro ancora prima che me ne accorgessi."


translate italian day4_us_95efcfe1:


    me "Ehi!"


translate italian day4_us_396dea3d:


    "Dietro a quella porta probabilmente c'era la stanza principale del rifugio."


translate italian day4_us_deed307a:


    "C'erano diversi posti letto accanto a me, dei dispositivi davanti alla parete più lontana e degli armadietti. Le lampade fluorescenti sul soffitto illuminavano brillantemente l'intera stanza."


translate italian day4_us_2b06fcd1:


    me "E da dove viene l'elettricità? I generatori di riserva funzionano ancora?"


translate italian day4_us_3cb76044:


    "Ho spento la torcia per risparmiare la batteria."


translate italian day4_us_464f6f21:


    "Ulyana ha cominciato a rovistare negli armadietti, tirando fuori maschere antigas, pacchetti, e vari strumenti."


translate italian day4_us_1a51a1af:


    me "Non hai niente di meglio da fare?"


translate italian day4_us_0dcdea24:


    us "No!"


translate italian day4_us_8263b828:


    "Mi ha guardato con disappunto."


translate italian day4_us_efbfc8f1:


    "Mi sono seduto sul letto ben fatto e mi sono guardato attorno ancora una volta."


translate italian day4_us_44a51345:


    "Proprio di fronte a me, sulla parete opposta, c'era una porta identica a quella da cui siamo entrati."


translate italian day4_us_cac4f276:


    th "Forse Shurik è passato di qui (sempre che sia venuto in questo luogo)? E per quale motivo?"


translate italian day4_us_48e24bcd:


    us "Perché quel muso lungo?"


translate italian day4_us_b89cbb7e:


    me "Sono stanco…"


translate italian day4_us_9fb02d3f:


    "Le ho confessato con sincerità."


translate italian day4_us_f062a0a0:


    us "Allora riposati un po'!"


translate italian day4_us_54ad0367:


    "Ulyana si è avvicinata e mi ha spinto sul petto."


translate italian day4_us_b9943d3a:


    "Sono caduto all'indietro con sorpresa, sbattendo la testa contro il muro."


translate italian day4_us_fc7f611f:


    me "Ma che fai!"


translate italian day4_us_8776d0b9:


    "L'ho afferrata per le braccia e l'ho tirata verso di me."


translate italian day4_us_53d31d5c:


    "Ha perso l'equilibrio ed è caduta sul letto accanto a me."


translate italian day4_us_ed5f24ac:


    us "Ahia!"


translate italian day4_us_60bb0a79:


    me "Hai iniziato tu!"


translate italian day4_us_4b1f34cb:


    "Ulyana ha tirato fuori la lingua e si è rialzata."


translate italian day4_us_02aa8fe6:


    me "Allora, qual è la nostra prossima mossa?"


translate italian day4_us_2fa697bf:


    us "Lì, quell'altra porta..."


translate italian day4_us_00a82683:


    me "Oh, come no… Shurik ha predetto una guerra nucleare e ha deciso di nascondersi in anticipo, vero?"


translate italian day4_us_c399a64c:


    us "Forse sì?"


translate italian day4_us_1759bd02:


    me "Forse? Questo rifugio è troppo vicino alla superficie. Potrebbe proteggerti dalle radiazioni, ma se una bomba cadesse qui vicino…"


translate italian day4_us_3503d6d7:


    us "Sei così serio su queste cose."


translate italian day4_us_fde537c1:


    "Ha detto sorridendo."


translate italian day4_us_8a58cac1:


    "Non sapevo davvero quando essere serio con lei e quando scherzare."


translate italian day4_us_9f4bfb7f:


    "Forse era colpa della differenza di età tra noi due."


translate italian day4_us_a51509b7:


    th "Quanto potrebbe essere? Dieci anni, o forse più?"


translate italian day4_us_1ab60ac4:


    "Ho sospirato e mi sono alzato dal letto."


translate italian day4_us_99949f0f:


    me "Ok. Proviamoci."


translate italian day4_us_fad4bb18:


    "Ma la seconda porta era meno remissiva della prima."


translate italian day4_us_a23c0e66:


    "I bulloni hanno scricchiolato, ma la ruota non ha girato di un millimetro."


translate italian day4_us_9da6b067:


    me "Sembra inceppata…"


translate italian day4_us_afc3494c:


    us "Fammi provare!"


translate italian day4_us_76c86e05:


    "Ulyana si è gettata sulla porta con un piede di porco che aveva trovato chissà dove, e ha fatto leva su di esso con tutto il suo scarso peso."


translate italian day4_us_d0cada00:


    "Così forse avrebbe funzionato. L'ho aiutata, e ben presto la porta ha ceduto, schiantandosi a terra con un tonfo."


translate italian day4_us_50d5d9d2:


    "I cardini risultavano essere completamente arrugginiti."


translate italian day4_us_0c596808:


    th "Sembra che {i}non proprio tutto{/i} qui sia stato costruito per durare nel tempo."


translate italian day4_us_1df933d0:


    "Dietro a quella porta c'era un tunnel del tutto simile a quello da cui eravamo arrivati."


translate italian day4_us_4bb12c41:


    me "Non fare niente di stupido!"


translate italian day4_us_99685367:


    "Ho preso Ulyana per mano e ho varcato la soglia."


translate italian day4_us_ce617998_3:


    "…"


translate italian day4_us_8ea41ecb:


    "Era un altro tunnel infinito."


translate italian day4_us_83e78ba1:


    "Il soffitto sembrava via via sempre più basso, ma era solo una mia impressione."


translate italian day4_us_4b91848a:


    "Ulyana non sembrava affatto preoccupata. Stava canticchiando allegramente mentre camminavamo."


translate italian day4_us_6ed9af94:


    "La cosa mi stava dando sempre più fastidio."


translate italian day4_us_7a841c5f:


    me "Sembra che tu ti stia divertendo…"


translate italian day4_us_83eff6be:


    us "Certo che sì! Tu no?"


translate italian day4_us_7a50435b:


    me "No. Non vedo alcun motivo per divertirmi. Dovremmo trovare Shurik e andarcene il prima possibile!"


translate italian day4_us_d877fb1e:


    us "Potrebbe non essere nemmeno qui."


translate italian day4_us_94eb0d42:


    me "Ma allora perché diavolo…?"


translate italian day4_us_30f4be0c_2:


    us "Guarda!"


translate italian day4_us_4f298886:


    "Ulyana mi ha strappato la torcia dalle mani."


translate italian day4_us_bb4d07b2:


    "C'era una buca enorme sul pavimento, larga un paio di metri."


translate italian day4_us_3fb17507:


    us "Potrebbe essere laggiù!"


translate italian day4_us_974d3351:


    "Ha raggiunto il bordo e si è chinata."


translate italian day4_us_3e125f8d:


    us "Ci sono delle rotaie…"


translate italian day4_us_00e5898d:


    "Sembrava che ci fosse una miniera sotto a quel tunnel."


translate italian day4_us_3dd043c8:


    "La profondità del buco era abbastanza ridotta, avremmo potuto risalire con facilità, quindi le prossime parole di Ulyana mi sembravano ovvie."


translate italian day4_us_2e1a85d1:


    us "Scendiamo!"


translate italian day4_us_70ddfdf6:


    "Volevo oppormi, ma lei è saltata giù, lasciandomi nel buio totale."


translate italian day4_us_95efcfe1_1:


    me "Ehi!"


translate italian day4_us_e539b0ce:


    "Ho dovuto seguirla per forza."


translate italian day4_us_98c927dd:


    "Non so cosa avessero scavato qui, ma la miniera pareva essere stata abbandonata da tempo. Le travi erano rovinate dall'umidità, le rotaie erano arrugginite, la terra era crollata all'interno in certi punti delle pareti."


translate italian day4_us_ac3a62f7:


    "L'intero tunnel, che si estendeva verso l'ignoto, era quantomeno sospetto. Era come se potesse crollare da un momento all'altro, seppellendoci."


translate italian day4_us_da8e6eb5:


    us "Dai, andiamo!"


translate italian day4_us_e1d9394c:


    "Ulyana mi ha tirato insistentemente per il braccio."


translate italian day4_us_20e92b26:


    me "E dove? Perché? Perché mai Shurik dovrebbe essere sceso quaggiù?"


translate italian day4_us_46a40361:


    us "E se...?!"


translate italian day4_us_cfc2bc6d:


    "La sua espressione è diventata seria."


translate italian day4_us_0c35b5c3:


    us "E se invece fosse seduto qui da qualche parte, magari ferito, aspettando i soccorsi, e noi adesso tornassimo indietro, lasciandolo qui a morire…?!"


translate italian day4_us_55b2a7b1:


    "Ho valutato ancora una volta l'altezza del soffitto e mi sono incamminato lentamente dietro all'instancabile Ulyana."


translate italian day4_us_ce617998_4:


    "…"


translate italian day4_us_b17c36b3:


    "Ben presto abbiamo raggiunto un bivio."


translate italian day4_us_830688df:


    us "Andiamo a destra!"


translate italian day4_us_c6dd4736:


    me "Aspetta!"


translate italian day4_us_36ea64e3:


    "L'ho afferrata per il braccio."


translate italian day4_us_5da43ef2:


    us "Cosa c'è?"


translate italian day4_us_3688cbd4:


    me "E se fosse un vicolo cieco? O ancora peggio – un labirinto?"


translate italian day4_us_52f35a85:


    us "Beh…"


translate italian day4_us_60f3d758:


    "Ha pensato."


translate italian day4_us_c86b37ad:


    us "Allora lasciamo un segno nel punto iniziale!"


translate italian day4_us_8083c22c:


    "Ulyana ha raccolto un grande sasso da terra e ha inciso una croce su una delle travi che sostenevano il soffitto."


translate italian day4_us_f8d84cb9:


    me "Credi che basterà?"


translate italian day4_us_3e60cbf2:


    us "Certo!"


translate italian day4_us_511d63f8:


    th "Forse è meglio se scelgo io da quale parte andare. Non posso lasciare che decida una piccola ragazzina."


translate italian us_mine_coalface_e40597aa:


    "Siamo già stati qui."


translate italian us_mine_coalface_5b4c3c90:


    "Alla fine siamo usciti dal tunnel e ci siamo ritrovati in una sala con un alto soffitto."


translate italian us_mine_coalface_3ef8c583:


    "Anche se non la si poteva di certo chiamare sala – l'avevano usata per estrarre qualche minerale.{w} Forse il carbone, o magari l'oro."


translate italian us_mine_coalface_3c54fe69:


    "Le pareti erano state frantumate dai colpi di piccone, o forse da martelli pneumatici."


translate italian us_mine_coalface_c718c063:


    "In questo luogo c'era un buio totale, l'unica nostra salvezza era la torcia."


translate italian us_mine_coalface_b97863a3:


    th "Se si dovesse rompere, è improbabile che ne usciremo vivi…"


translate italian us_mine_coalface_58411121:


    "Il fascio di luce ha rivelato un pezzo di stoffa rosso nell'angolo."


translate italian us_mine_coalface_ca0a4f90:


    "Era il foulard di un pioniere!"


translate italian us_mine_coalface_96b3272c:


    "Shurik era decisamente qui da qualche parte."


translate italian us_mine_coalface_93ec21f7:


    me "Shurik! Shurik!"


translate italian us_mine_coalface_455d9f62:


    us "Shurik!"


translate italian us_mine_coalface_b99be3e2:


    "Il nostro eco ha rimbombato tra le pareti."


translate italian us_mine_coalface_15e63d5a:


    me "Spero che stia bene…"


translate italian us_mine_coalface_f87c925c:


    us "Non preoccuparti! Lo troveremo!"


translate italian us_mine_coalface_a411960f:


    th "Ma dove potrebbe essere andato?"


translate italian us_mine_coalface_686bf61c:


    "Non c'erano altre vie d'uscita da questa stanza."


translate italian us_mine_coalface_7ea80226:


    th "È probabile che ci siano posti in questi tunnel che non abbiamo ancora visitato…"


translate italian us_mine_coalface_858b6cbe:


    th "E quindi dobbiamo cercare ancora!"


translate italian us_mine_exit_26c6312e:


    "Alla fine il raggio di luce ha rivelato una porta di legno nell'oscurità."


translate italian us_mine_exit_0e7f3bff:


    us "Eccoci finalmente!"


translate italian us_mine_exit_25f7abbc:


    me "Dove?"


translate italian us_mine_exit_03e083fb:


    us "Da qualche parte. Non lo so."


translate italian us_mine_exit_e2cbe440:


    "Ma su una cosa aveva ragione – almeno eravamo riusciti ad uscire da quel labirinto."


translate italian us_mine_exit_1467f5ac:


    "Dopo tutte quelle svolte e bivi, non ero sicuro che ce l'avremmo fatta a tornare indietro ma, d'altra parte, perché mai questa miniera non avrebbe dovuto avere altre vie d'uscita?"


translate italian us_mine_exit_5ac980a3:


    "Ulyana ha aperto la porta, fissando l'oscurità."


translate italian us_mine_exit_6acdac4b:


    me "Quindi, non vuoi entrare per prima come al solito?"


translate italian us_mine_exit_52f35a85:


    us "Beh…"


translate italian us_mine_exit_01822020:


    me "Va bene."


translate italian us_mine_exit_02157de8:


    "Ho messo un piede sulla soglia."


translate italian us_mine_exit_80505c24:


    "Dall'altra parte c'era uno stanzino – forse un piccolo deposito del rifugio antiaereo."


translate italian us_mine_exit_c7136184:


    "C'erano bottiglie e mozziconi di sigarette dappertutto, e ciò significava che qualcuno era passato di qui prima di noi."


translate italian us_mine_exit_ecb7a6ba:


    "Non era proprio una cosa incoraggiante, ma almeno adesso sapevamo che ci doveva per forza essere un'altra via d'uscita dalle miniere, dato che {i}loro{/i} non potevano essere entrati dallo stesso percorso che abbiamo preso noi."


translate italian us_mine_exit_56d7df1e:


    "Il raggio di luce ha scansionato ogni angolo della stanza. Improvvisamente abbiamo scorto una figura umana…"


translate italian us_mine_exit_1925a7ca:


    "Era Shurik, accovacciato verso una parete!"


translate italian us_mine_exit_72821d25:


    me "Ehi! Eccoti finalmente! Ti abbiamo cercato per tutta la notte, e invece tu…"


translate italian us_mine_exit_212f5a54:


    "Sembrava che non ci avesse nemmeno notato, se ne stava lì a borbottare qualcosa."


translate italian us_mine_exit_b01a7f13:


    me "Shurik!"


translate italian us_mine_exit_58f9ccc4:


    sh "Chi… chi è?"


translate italian us_mine_exit_21a8f802:


    me "Come «chi»?! La tua squadra di soccorso! Alzati e andiamocene!"


translate italian us_mine_exit_42731173:


    sh "Non andrò da nessuna parte con voi!"


translate italian us_mine_exit_7c4bde06:


    "Ha mormorato."


translate italian us_mine_exit_71df32d3:


    sh "Mi guiderete di nuovo attraverso quei tunnel, ormai lo so! Non vengo da nessuna parte! Me ne starò qui, e voi non mi prenderete!"


translate italian us_mine_exit_1882e4f8:


    me "Smettila con queste assurdità!"


translate italian us_mine_exit_c5c0b112:


    "Sembrava proprio che fosse impazzito."


translate italian us_mine_exit_aeb6a5a7:


    sh "No, no! Non mi ingannerete questa volta."


translate italian us_mine_exit_8b610151:


    me "E smettila…"


translate italian us_mine_exit_9a85ecb6:


    "Mi sono avvicinato a Shurik di qualche passo, ma improvvisamente è scattato in piedi, tenendo in mano una barra di metallo."


translate italian us_mine_exit_1817dc02:


    sh "Non avvicinatevi! Lasciatemi in pace!"


translate italian us_mine_exit_bf9a997b:


    me "Calmati! Sono io, Semyon! Non mi riconosci?"


translate italian us_mine_exit_9f720364:


    sh "Semyon...? No, tu non sei Semyon!"


translate italian us_mine_exit_48892acd:


    "Ulyana, che poco fa stava accanto a me, era sparita da qualche parte."


translate italian us_mine_exit_dd35a3e1:


    sh "Tu non sei Semyon, e io adesso…"


translate italian us_mine_exit_b64a44f7:


    "Sotto il fascio di luce tremante, Shurik è scattato verso di me, brandendo la barra di metallo. Mi sono coperto istintivamente la testa con le mani, e…"


translate italian us_mine_exit_f5b87a6f:


    "Niente."


translate italian us_mine_exit_2533f390:


    "Quando ho riaperto gli occhi, Shurik era sparito. Ulyana stava accanto a me ridacchiando, tenendo tra le mani la barra di metallo."


translate italian us_mine_exit_af36de66:


    us "Come un vero scout!"


translate italian us_mine_exit_4c03fe75:


    me "Già, uno scout…"


translate italian us_mine_exit_19ad538e:


    "La risata diabolica di Shurik è risuonata da qualche parte in lontananza."


translate italian us_mine_exit_92bb7ff9:


    us "È scappato via…"


translate italian us_mine_exit_d0833b8f:


    me "Che vada all'inferno. Non m'importa se morirà quaggiù!"


translate italian us_mine_exit_ff689675:


    "Sono crollato sul pavimento e mi sono appoggiato alla parete."


translate italian us_mine_exit_3429bb5b:


    th "Se non fosse stato per Ulyana…"


translate italian us_mine_exit_c05d018c:


    "Non me l'aspettavo proprio!{w} Forse Shurik non mi avrebbe ucciso, ma avrebbe potuto ferirmi gravemente!"


translate italian us_mine_exit_95c2fa20:


    "Restare ferito in un luogo del genere significherebbe una morte certa – non sapevamo nemmeno quando sarebbero arrivati i soccorsi.{w} E come mi avrebbero trovato poi in questo labirinto?"


translate italian us_mine_exit_9eb821b3:


    "Perché diamine ho accettato di venire qui?{w} Dando retta a quella ragazzina…"


translate italian us_mine_exit_85b29f3a:


    us "Sembra che tu voglia uccidere qualcuno."


translate italian us_mine_exit_058d6daa:


    me "Se si presentasse un buon candidato..."


translate italian us_mine_exit_07e9d3d8:


    "Ulyana ha rabbrividito."


translate italian us_mine_exit_dbe0f3c1:


    me "No, non tu. Sarebbe bello darti una sculacciata, ma non c'è ragione di ucciderti."


translate italian us_mine_exit_f0d6d1c8:


    "Mi ha fatto un sorrisetto."


translate italian us_mine_exit_b16368a6:


    me "Comunque…"


translate italian us_mine_exit_50d4d0ba:


    us "Oh, tu!"


translate italian us_mine_exit_850926ff:


    me "Ok. È decisamente ora di andarcene da qui! Manderanno domani le loro squadre di salvataggio, le forze speciali, gli Acchiappafantasmi, o quel cavolo che vorranno. Io ho chiuso con questa storia."


translate italian us_mine_exit_a661ea5a:


    us "Torniamo indietro?"


translate italian us_mine_exit_479f496b:


    "Ho esaminato di nuovo la stanza e ho notato una porta alla mia sinistra."


translate italian us_mine_exit_c2e764e0:


    me "Wow."


translate italian us_mine_exit_1a153a40:


    "Era come quella del rifugio antiaereo – un'enorme porta di metallo."


translate italian us_mine_exit_e592b1be:


    "Ho spinto la ruota più volte, ma ha solo scricchiolato."


translate italian us_mine_exit_3e37cbde:


    th "Se solo avessi quel piede di porco…"


translate italian us_mine_exit_3324aa7a:


    us "Non gira?"


translate italian us_mine_exit_0c87327f:


    "Mi ha chiesto Ulyana in tono depresso."


translate italian us_mine_exit_5cb822ee:


    me "No…"


translate italian us_mine_exit_48ef60d6:


    "Non mi è rimasta più alcuna forza."


translate italian us_mine_exit_94802177:


    "In altre circostanze mi sarei dato da fare, avrei chiesto a Ulyana di aiutarmi, avrei cercato qualcosa da usare come leva, ma adesso volevo solo uscire al più presto dalla miniera."


translate italian us_mine_exit_85c47979:


    "Volevo credere che non ci sarebbe stato più alcun ostacolo tra noi e la via d'uscita."


translate italian us_mine_exit_334e8c15:


    "E quello significava per forza il ricordarsi le direzioni che avevamo preso in quel labirinto."


translate italian us_mine_exit_3486283d:


    me "Torniamo indietro."


translate italian us_mine_exit_89d3eb13:


    us "Ok."


translate italian us_mine_exit_5f443c07:


    "Ha sorriso, prendendomi per mano."


translate italian us_mine_exit_ce617998:


    "…"


translate italian us_mine_exit_e26fdffd:


    "Camminavamo ancora più lentamente nel tornare indietro attraverso il labirinto."


translate italian us_mine_exit_db3ba18f:


    "I sassi scivolavano sotto ai nostri piedi, l'acqua gocciolava dal soffitto sulle nostre teste, e sembravano gocce di stagno fuso."


translate italian us_mine_exit_dc5bc7a5:


    "Ulyana si è calmata, seguendomi in silenzio."


translate italian us_mine_exit_ea63ce77:


    me "C'è qualcosa che non va?"


translate italian us_mine_exit_5f2c0f51:


    us "Cosa vuoi dire?"


translate italian us_mine_exit_8a64dac8:


    me "È stano non sentirti dire una parola per più di un minuto."


translate italian us_mine_exit_0242fc7b:


    us "No, è tutto a posto…"


translate italian us_mine_exit_c417bbd2:


    "Ma qualcosa non era a posto di sicuro."


translate italian us_mine_exit_a92297a2:


    "Un bivio dopo l'altro."


translate italian us_mine_exit_04ad16d0:


    "Un minuto più tardi ero sicuro che dopo la prossima svolta avrei trovato la croce che aveva inciso Ulyana. Ma mi sbagliavo."


translate italian us_mine_exit_d65de998:


    "La fiducia nel mio senso dell'orientamento si stava sciogliendo ad ogni secondo che passava."


translate italian us_mine_exit_9e4e4c62:


    me "Ah, al diavolo!"


translate italian us_mine_exit_554c40e7:


    "Volevo distrarmi un po' dicendo qualcosa."


translate italian us_mine_exit_6d9d0fd7:


    us "Non preoccuparti! È solo che…"


translate italian us_mine_exit_ad02f53a:


    me "Cosa?"


translate italian us_mine_exit_c6444665:


    us "È andata proprio male. Con Shurik. E… adesso siamo bloccati qui."


translate italian us_mine_exit_a54e284d:


    me "Mi hai salvato la vita. Dovresti esserne fiera."


translate italian us_mine_exit_ccd2dd89:


    "Ho provato ad incoraggiarla, ma Ulyana non sembrava aver colto il messaggio."


translate italian us_mine_exit_b1356ab0:


    us "Ma se non gli avessi preso quella barra forse non sarebbe scappato."


translate italian us_mine_exit_51f70f47:


    me "Che fosse scappato o meno, che differenza fa adesso? Dobbiamo comunque uscire da qui, no?"


translate italian us_mine_exit_c3e3f337:


    us "Certo, ma…"


translate italian us_mine_exit_af351568:


    me "Andrà tutto bene! Quel pazzo troverà una via d'uscita, questo è poco ma sicuro."


translate italian us_mine_exit_aa7fa335:


    "Ne ero proprio convinto."


translate italian us_mine_exit_ce617998_1:


    "…"


translate italian us_mine_exit_f7cc90bf:


    "Finalmente siamo arrivati a un lungo tunnel, e su una delle sue pareti ho visto la croce di Ulyana."


translate italian us_mine_exit_3daa2b3d:


    "Lei si è rallegrata un po', e il resto del tragitto l'abbiamo percorso quasi correndo."


translate italian us_mine_exit_de2c6d73:


    me "Ce l'abbiamo fatta!"


translate italian us_mine_exit_b88638a5:


    "La luna piena splendeva nuovamente sulle nostre teste, e il decadente edificio del vecchio campo non ci sembrava più così temibile come prima.{w} Specialmente se confrontato col rifugio antiaereo e con le catacombe."


translate italian us_mine_exit_6bffdd99:


    us "È stato forte, vero?"


translate italian us_mine_exit_53c7724e:


    "Sembrava che Ulyana avesse ritrovato la sua solita allegria."


translate italian us_mine_exit_edb2bc43:


    me "Non sono troppo sicuro che sia stato «forte», ma sono felice che ne siamo usciti interi."


translate italian us_mine_exit_c6618fc6:


    us "Allora, andiamo a cercare Shurik?"


translate italian us_mine_exit_67c5f2d1:


    me "Che cosa?"


translate italian us_mine_exit_e113e81a:


    "Per un momento sono rimasto senza parole, senza riuscire nemmeno a finire la frase."


translate italian us_mine_exit_3d6467aa:


    me "Ma sei impazzita?! L'abbiamo appena trovato! Domani Olga Dmitrievna e la polizia potranno andare laggiù a catturare quel cavernicolo. Per usarlo come cavia."


translate italian us_mine_exit_3dc79f49:


    us "Ma...!"


translate italian us_mine_exit_6f3fc9d9:


    me "Niente ma! Adesso torniamo al Campo! A dormire!"


translate italian us_mine_exit_07bb3c51:


    "Mi sono incamminato con passo deciso, ignorando la furiosa Ulyana."


translate italian us_mine_exit_ce617998_2:


    "…"


translate italian us_mine_exit_680a7e90:


    "Dopo soli dieci minuti eravamo già alla piazza."


translate italian us_mine_exit_09a35d39:


    me "Ok. E questo è tutto per oggi. Sei congedato, soldato!"


translate italian us_mine_exit_d8af262d:


    "Ulyana mi ha salutato e stava per andarsene, quando improvvisamente ha cominciato a gridare, agitando le braccia come una pazza."


translate italian us_mine_exit_700adfc4:


    us "Guarda! Guarda!"


translate italian us_mine_exit_7047bfac:


    "Mi sono voltato verso le panchine e ho visto Shurik sdraiato su una di esse."


translate italian us_mine_exit_0f53a61e:


    me "Oh mio Dio…"


translate italian us_mine_exit_11b6866f:


    "È stato davvero difficile svegliarlo, come se il «cavernicolo» fosse andato in letargo."


translate italian us_mine_exit_68fdcd41:


    sh "Ah? Cosa? Dove sono?"


translate italian us_mine_exit_5d5d525a:


    "Ha borbottato nel sonno."


translate italian us_mine_exit_46b3bd18:


    me "E adesso vuoi spiegarci?"


translate italian us_mine_exit_613a674f:


    sh "Spiegarvi cosa?"


translate italian us_mine_exit_8cd1cf37:


    us "Ti abbiamo cercato per tutta la notte! E poi hai aggredito Semyon con una sbarra di metallo! E poi sei scappato via!"


translate italian us_mine_exit_c6875964:


    "Ulyana stava saltando attorno alla panchina, come se stesse per esplodere da un momento all'altro."


translate italian us_mine_exit_8e0eb312:


    sh "Cos'è successo? E perché mi trovo qui?"


translate italian us_mine_exit_6e10baeb:


    "Shurik sembrava aver ripreso i sensi."


translate italian us_mine_exit_6891b2b4:


    me "Oh, vorresti essere così gentile da spiegarci cosa è successo?! Di come tu abbia fatto a uscire dalla miniera! E soprattutto perché ci sei andato? Tutto, dall'inizio alla fine!"


translate italian us_mine_exit_517dc919:


    sh "Quale miniera?"


translate italian us_mine_exit_2dc7c7d4:


    "C'era una tale sorpresa nei suoi occhi che ho iniziato ad avere i miei dubbi.{w} Forse davvero non riusciva a ricordare niente."


translate italian us_mine_exit_70974d60:


    me "Dove sei stato nelle ultime… dodici ore?"


translate italian us_mine_exit_bbe83e3a:


    sh "Non lo so…"


translate italian us_mine_exit_1afdf66f:


    "Shurik si è seduto aggrottando la fronte, sforzandosi di ricordare."


translate italian us_mine_exit_31d31d38:


    sh "Sono andato al vecchio campo la mattina. Dicevano che c'era dell'equipaggiamento lì. Per i componenti del robot. E…"


translate italian us_mine_exit_5d2d6c1c:


    "Ci ha fissati confuso."


translate italian us_mine_exit_f95c9fc9:


    me "E?"


translate italian us_mine_exit_80c68aa7:


    sh "E questo è tutto…"


translate italian us_mine_exit_a7156351:


    me "Quindi, non ricordi niente?"


translate italian us_mine_exit_f8cef8d3:


    sh "No, niente."


translate italian us_mine_exit_855db9ee:


    me "Ok."


translate italian us_mine_exit_303044ab:


    "Mi sono seduto accanto a lui, appoggiandomi allo schienale."


translate italian us_mine_exit_6472a1a6:


    "Le stelle brillavano luminose nel cielo."


translate italian us_mine_exit_fe87209a:


    "Esse ricordano ogni cosa.{w} Persino cosa stesse facendo Shurik in quel rifugio antiaereo."


translate italian us_mine_exit_4ae29059:


    me "Shock post-traumatico."


translate italian us_mine_exit_80f898e3:


    us "Poster… cosa?"


translate italian us_mine_exit_470d1a54:


    sh "Sono i sintomi sperimentati dalle persone dopo un pesante stress. Dopo una catastrofe naturale, ad esempio."


translate italian us_mine_exit_ca0e1330:


    "Ha spiegato Shurik, con sguardo sapiente."


translate italian us_mine_exit_9f48d48f:


    me "Dovresti farti una bella dormita adesso."


translate italian us_mine_exit_035cd0d4:


    sh "Sì, ma…"


translate italian us_mine_exit_7241624a:


    me "Ne parleremo domani."


translate italian us_mine_exit_c2ac9dce:


    "Shurik mi ha guardato per un po', ma poi si è alzato e se n'è andato lentamente verso la sua casetta senza dire una parola."


translate italian us_mine_exit_f2233194:


    us "E quindi cosa gli è successo?"


translate italian us_mine_exit_f96a54b5:


    me "Probabilmente ha dimenticato tutto quello che è successo nella miniera."


translate italian us_mine_exit_356011d8:


    us "Sta mentendo!"


translate italian us_mine_exit_ad59f734:


    me "Senti, perché secondo te dovrebbe mentire?"


translate italian us_mine_exit_d58e2a3f:


    us "Così non dovrà giustificarsi del fatto che ha tentato di… attaccarti… con quella sbarra di metallo…"


translate italian us_mine_exit_858becb4:


    "Ha detto incerta."


translate italian us_mine_exit_d8eaac03:


    me "A me non sembra. E che importa adesso?"


translate italian us_mine_exit_c9dba43d:


    us "Importa! Dobbiamo scoprirlo! Il criminale dev'essere punito!"


translate italian us_mine_exit_823f9939:


    me "Se quella regola valesse anche per te, ti avrebbero arrestata un bel po' di tempo fa. O forse anche peggio."


translate italian us_mine_exit_0d7a7ad1:


    us "E questo che c'entra con me? Io non vado in giro a colpire le persone con una barra metallica!"


translate italian us_mine_exit_2a2a9dda:


    me "Non l'ha fatto apposta."


translate italian us_mine_exit_5ee2ab65:


    us "Ma sta mentendo!"


translate italian us_mine_exit_7fd78ed0:


    me "{i}Potrebbe{/i} mentire."


translate italian us_mine_exit_3aec35b1:


    "Ero davvero esausto, specialmente dopo gli eventi di questa notte, e non mi importava proprio se Shurik stesse dicendo la verità o se stesse solo facendo finta."


translate italian us_mine_exit_87c100aa:


    "Sembrava davvero che non si ricordasse niente."


translate italian us_mine_exit_93078286:


    me "Io vado a dormire."


translate italian us_mine_exit_be15443d:


    us "Allora…"


translate italian us_mine_exit_3b19a06b:


    "Ulyana ha fatto un salto ed è rimasta in punta di piedi."


translate italian us_mine_exit_e1a87c65:


    us "Buona notte!"


translate italian us_mine_exit_b5cc7e1c:


    me "Anche a te…"


translate italian us_mine_exit_3f096259:


    "Non so, ma mi sembrava che ci fosse qualcosa di speciale nella sua espressione in quell'istante, ma non mi importava."


translate italian us_mine_exit_0ee0f1a6:


    "La leader del Campo mi stava aspettando «a casa»."


translate italian us_mine_exit_523cb2dc:


    mt "Credevo che non saresti più tornato."


translate italian us_mine_exit_04cc2a2a:


    "Mi sarei aspettato ogni possibile reazione da parte di Olga Dmitrievna, tranne quella."


translate italian us_mine_exit_12012f33:


    mt "Dato che sei andato con Ulyana."


translate italian us_mine_exit_c4c1debf:


    me "Non dovrebbe essere preoccupata per noi?"


translate italian us_mine_exit_40fec304:


    mt "E perché dovrei? È tutto a posto."


translate italian us_mine_exit_d950ea90:


    me "Oh, va bene allora..."


translate italian us_mine_exit_adda73b6:


    "Non avevo né la voglia, né la forza di litigare con la leader, né di scoprire per quale ragione si comportasse in quel modo."


translate italian us_mine_exit_bc6a86e2:


    "Mi sono tolto i vestiti e sono strisciato sotto le coperte."


translate italian us_mine_exit_9f489f27:


    th "Tutto questo è davvero troppo per una singola persona."


translate italian us_mine_exit_262beff1:


    "Cercare Shurik nei sotterranei avrebbe dovuto essere un lavoro per una squadra di salvataggio professionista.{w} Farlo con Ulyana – era una cosa da folli patentati."


translate italian us_mine_exit_54005ff3:


    th "Ma in un certo senso è stato divertente."


translate italian us_mine_exit_40413c26:


    "Mi sono addormentato con il sorriso stampato sulle labbra…"


translate italian day4_un_867359a2:


    me "Non ci dovrò andare da solo, vero?"


translate italian day4_un_be1d7816:


    "Olga Dmitrievna ha pensato per un momento."


translate italian day4_un_db0f7ce7:


    mt "Hai ragione… Ci andremo insieme domani."


translate italian day4_un_c58ce529:


    "Negli ultimi minuti ho notato che Lena aveva una strana espressione sul suo volto.{w} Come se volesse dire qualcosa, ma non ne avesse il coraggio."


translate italian day4_un_08dadf4e:


    "I pionieri hanno iniziato a disperdersi, come se si fossero dimenticati di Alisa e dell'esplosione."


translate italian day4_un_958ec37b:


    "Persino la nostra leader sembrava essersi placata, e non si è opposta quando la nostra aspirante terrorista ha abbandonato la piazza, cercando di nascondersi dietro a Ulyana."


translate italian day4_un_63db6e89:


    mt "Dovremmo andare anche noi."


translate italian day4_un_9513cd87:


    me "Già…"


translate italian day4_un_a5b61259:


    "La notte è calata rapidamente sul Campo."


translate italian day4_un_feec15e3:


    "C'è solo un breve momento che intercorre tra i primi raggi del sole al tramonto fino alla completa oscurità qui al sud, o forse solo in questo mondo. Non si ha abbastanza tempo per godersi la varietà dei colori del tramonto."


translate italian day4_un_04b50f79:


    "Era troppo presto per andare a dormire, ma la leader stava tornando con decisione alla sua casetta, come se mi stesse trascinando mentalmente con sé."


translate italian day4_un_90487e63:


    me "Olga Dmitrievna, io vado a fare una passeggiata."


translate italian day4_un_5cb759d8:


    mt "Va bene…"


translate italian day4_un_ed75d184:


    "Mi ha guardato intensamente ma non ha trovato alcuna ragione per opporsi, e quindi ha alzato le spalle e ha continuato a camminare."


translate italian day4_un_e4483483:


    "Sono tornato alla piazza."


translate italian day4_un_0fb3cb29:


    "Non ero proprio interessato a guardare da vicino il lieve danno arrecato alla statua di Genda. Essa sorgeva nell'esatto centro del Campo.{w} Quando non sai dove andare, dovresti iniziare proprio da lì."


translate italian day4_un_df68a624:


    "Mi sono seduto su una panchina e ho guardato verso est. Mi chiedevo se la Terra {i}qui{/i} ruotasse su sé stessa come dovrebbe.{w} O se qui esistesse un vero nord? O un vero sud…?"


translate italian day4_un_fbfbafbb:


    "Difficile a dirsi.{w} In quel momento non avevo modo di verificare le leggi fondamentali della natura."


translate italian day4_un_e8479859:


    un "Ciao…"


translate italian day4_un_85361757:


    "Lena è apparsa accanto a me, dal nulla."


translate italian day4_un_87696cce:


    me "Ciao… Non riesci a dormire?"


translate italian day4_un_efd76d15:


    "Mi ha guardato sorpresa."


translate italian day4_un_28e80bef:


    me "Beh, sì. È ancora presto..."


translate italian day4_un_c67f7c91:


    un "Posso sedermi?"


translate italian day4_un_33712bb6:


    me "Certo, siediti pure."


translate italian day4_un_280592ba:


    "Mi sono spostato un po'."


translate italian day4_un_cb2ccc7b:


    "Dire «un po'» era un eufemismo, in realtà mi sono scansato fino all'altra estremità della panchina."


translate italian day4_un_626232a6:


    un "Grazie."


translate italian day4_un_32c8bf7a:


    "Lena si è seduta e ha iniziato a contemplare il cielo, come se si fosse dimenticata della mia presenza."


translate italian day4_un_9c9cf456:


    un "È triste…"


translate italian day4_un_02347a38:


    me "Cosa è triste?"


translate italian day4_un_e7203d10:


    un "Che Shurik sia scomparso."


translate italian day4_un_8ad48a87:


    me "Già. La situazione non è buona."


translate italian day4_un_51dccb02:


    "Lei era calma come sempre, restando in silenzio per la maggior parte del tempo, arrossendo e imbarazzandosi solo quando doveva dire o fare qualcosa."


translate italian day4_un_94abc2fa:


    "Quel suo silenzio, che potrebbe essere considerato strano da molti (incluso me), era piuttosto naturale per lei."


translate italian day4_un_d45c7222:


    "Non riuscivo a immaginarmi una Lena che sceglie con attenzione le parole giuste per iniziare una conversazione o per fare bella figura, cercando di non sembrare stupida o, al contrario, cercando di non apparire rude come Alisa."


translate italian day4_un_b00fea13:


    "Non riuscivo a paragonarla a nessun altro. Era semplicemente sé stessa."


translate italian day4_un_029e699d:


    "Questo significava forse che i miei tentativi di iniziare una conversazione con lei potessero essere considerati maleducati? Che l'espressione «diventare amici» potesse essere interpretata come un'intrusione nella sua vita privata?"


translate italian day4_un_5e028b03:


    "Ma qualcosa in quella ragazza mi attraeva."


translate italian day4_un_0ec5dc2a:


    "Forse era il mistero che la avvolgeva, e di certo non mancava dell'aspetto o del fascino femminile. Non avevo una risposta a quella domanda."


translate italian day4_un_309fa209:


    th "Ecco perché non sono ancora stato accusato apertamente di essere fastidioso…"


translate italian day4_un_703b7283:


    me "Sono sicuro che lo troveremo! Come si potrebbe fuggire da un sottomarino?"


translate italian day4_un_4f2afd96:


    "Lena non sembrava aver apprezzato la mia battuta."


translate italian day4_un_0b8134cd:


    "Questo Campo doveva apparire come un enorme sottomarino solo a me."


translate italian day4_un_58027c9c:


    un "Lo spero."


translate italian day4_un_8cd244fa:


    me "Domani Olga Dmitrievna chiamerà la polizia. Lo troveranno di sicuro!"


translate italian day4_un_86342ac5:


    un "E se durante la notte…"


translate italian day4_un_e57a9cb4:


    "La sua espressione si è fatta triste."


translate italian day4_un_74c7f5fa:


    me "Cosa pensi che possa succedergli?"


translate italian day4_un_c95128dc:


    th "Da solo, di notte, nella foresta… Tutto potrebbe succedere!"


translate italian day4_un_765a6a1c:


    un "Deve sentirsi solo."


translate italian day4_un_3d219146:


    me "Nessuno l'ha costretto ad andarci!"


translate italian day4_un_6a910ab9:


    un "E se si fosse perso?"


translate italian day4_un_2791ad53:


    me "Non sarebbe dovuto andare nella foresta da solo."


translate italian day4_un_63cda4c6:


    un "Non ti dispiace proprio niente per lui! Shurik potrebbe essere seduto in qualche posto, tutto solo…"


translate italian day4_un_dffcef38:


    me "Ma certo che mi dispiace…"


translate italian day4_un_7f15ab29:


    "Mi sono vergognato un po'."


translate italian day4_un_eda05f52:


    "Comunque Lena aveva ragione. Una persona era scomparsa."


translate italian day4_un_3b718ed7:


    un "Tutto potrebbe succedere durante la notte…"


translate italian day4_un_5000641d:


    me "Non starai pensando di andare a cercarlo adesso, vero?"


translate italian day4_un_fac6df53:


    "Non mi ha risposto, guardando qualcosa in lontananza, dove gli ultimi raggi del sole splendevano al di sopra degli antichi alberi, come se cercassero di donare alle persone un po' del loro calore."


translate italian day4_un_2807b2ef:


    me "Credi davvero che vagare per la foresta al buio sia una buona idea?"


translate italian day4_un_b354da3c:


    un "Probabilmente no."


translate italian day4_un_e49a0145:


    "Per qualche ragione credevo che fosse esattamente quello che stava pensando."


translate italian day4_un_2e282ea3:


    "Recentemente mi sembrava di riuscire a capire le intenzioni di Lena sempre più spesso, senza bisogno di alcuna parola.{w} E sembrava riuscire ad influenzarmi psicologicamente, mettendomi d'accordo con lei."


translate italian day4_un_d9cc43d9:


    "Il silenzio di Lena era molto più efficace di qualsiasi chiacchiera o tentativo di persuasione."


translate italian day4_un_b181f8ef:


    me "L'hanno già cercato durante il giorno."


translate italian day4_un_2b8cb61b:


    un "Dappertutto?"


translate italian day4_un_03f5817b:


    "Ha distolto lo sguardo dal tramonto e l'ha posato su di me."


translate italian day4_un_53cb4078:


    me "Non lo so. Credo dappertutto."


translate italian day4_un_076d7069:


    un "E il vecchio campo?"


translate italian day4_un_3727cf80:


    "Per la prima volta le sue parole hanno sottolineato una certa determinazione, e non erano vaghe o indifferenti."


translate italian day4_un_f405c42e:


    me "E dove sarebbe? Non ne ho idea."


translate italian day4_un_7ce714f3:


    un "Ce l'ha detto Electronik."


translate italian day4_un_ce3fa441:


    me "Beh. Se ti fidi di lui…"


translate italian day4_un_55fee8b0:


    "Ho scherzato stupidamente, ma Lena continuava a guardarmi con espressione seria."


translate italian day4_un_092d430e:


    me "Certo, se non è troppo lontano…"


translate italian day4_un_c90cc27f:


    un "Quindi vuoi andarci?"


translate italian day4_un_7bb63055:


    th "Certo che non voglio!"


translate italian day4_un_6ee034ca:


    me "Possiamo provare. Se è solo andata e ritorno, una cosa veloce…"


translate italian day4_un_b993fafe:


    un "Ok."


translate italian day4_un_99baefd5:


    "Lena ha sorriso e mi ha dato una torcia, apparsa dal nulla."


translate italian day4_un_34b3d127:


    me "Già. Potrebbe rivelarsi utile…"


translate italian day4_un_666cb784:


    th "Questo significa che si era già preparata in anticipo?{w} E che nulla dipende da me?"


translate italian day4_un_bdea403d:


    "Ho sospirato come se fossi condannato, e mi sono incamminato con lei verso la foresta."


translate italian day4_un_ce617998:


    "…"


translate italian day4_un_d6e969ac:


    "La notte è calata sul Campo."


translate italian day4_un_c621431e:


    "Camminavamo piano. Lena stava accanto a me, ma non troppo vicina."


translate italian day4_un_e8646f8d:


    "Era strano, sembrava che nulla la potesse spaventare."


translate italian day4_un_6f897bb8:


    th "Inoltre, non sembra preoccuparsi molto di quello che stiamo facendo, come se non stessimo camminando di notte nella foresta, come se stessimo guardando un film con altre persone che recitano la parte dei protagonisti."


translate italian day4_un_99f8ee9b:


    "In realtà Electronik aveva detto che il vecchio campo non era molto lontano, e che se avessimo camminato sempre nella stessa direzione, allora sarebbe stato difficile perdersi."


translate italian day4_un_f0457e58:


    "Dopo pochi minuti non ero affatto sicuro che stessimo camminando in linea retta, e dopo un altro po' ho iniziato a pensare che sarebbe stato un miracolo uscire di lì."


translate italian day4_un_794417f4:


    "Ma non volevo fare brutta figura davanti a Lena, così ho cercato di camminare allegramente."


translate italian day4_un_67236665:


    "La foresta era immersa nel silenzio, si potevano scorgere delle ombre tremolanti e i raggi di luna scintillanti. L'erba frusciava silenziosamente sotto ai nostri piedi, e i rami ondeggiavano sopra le nostre teste."


translate italian day4_un_cf17831d:


    "Antiche querce risiedevano accanto a giovani betulle. Grossi funghi emergevano sotto queste ultime, come se si stessero togliendo i grandi cappelli in segno di saluto."


translate italian day4_un_3805c942:


    "In qualunque altro giorno – o meglio – in qualunque altro momento della giornata, sarebbe sembrato davvero affascinante."


translate italian day4_un_3e7291b0:


    "Forse la foresta non era pericolosa neppure di notte, ma comunque rabbrividivo a ogni singola folata di vento."


translate italian day4_un_d5312815:


    un "Guarda."


translate italian day4_un_2b42b39e:


    "Lena ha indicato qualcosa davanti a noi. Ho strofinato gli occhi e ho visto un varco tra gli alberi."


translate italian day4_un_c1f40a14:


    "Dopo un minuto ci siamo trovati in una radura piuttosto grande. Al centro di essa si ergeva un vecchio edificio, che sembrava una scuola o un asilo."


translate italian day4_un_a547b60d:


    "La vernice cadeva dalle pareti. Il tetto era pieno di buchi, come dopo un bombardamento. E le finestre rotte ci guardavano con tristezza, un po' minacciose.{w} Non era una vista molto piacevole."


translate italian day4_un_fda5c27f:


    "Non ricordavo come mi ero immaginato quel luogo prima di averlo visto. Era come se tutte le immagini fossero state cancellate dalla mia memoria, sostituite da questa specie di cimitero deprimente."


translate italian day4_un_af5a1200:


    me "Beh, è proprio spaventoso…"


translate italian day4_un_a1b1799c:


    "Lena stava ancora in piedi in silenzio, ma una naturale espressione di paura è apparsa sul suo volto."


translate italian day4_un_6e55cb5d:


    un "Credi che lui sia lì dentro?"


translate italian day4_un_7479b07e:


    me "Non ne ho idea…"


translate italian day4_un_8ae2c27f:


    "Se fossi in Shurik, una casa infestata sarebbe l'ultimo dei luoghi in cui mi vorrei nascondere."


translate italian day4_un_35698e3a:


    un "Andiamo?"


translate italian day4_un_63a2b840:


    "Non ho avuto il tempo per risponderle – la luna è apparsa da dietro le nuvole e ha illuminato l'intero ambiente con nuovi colori."


translate italian day4_un_8577fae8:


    "A dire il vero con un unico colore – il bianco di tomba."


translate italian day4_un_29a44ad3:


    "Riuscivo a vedere chiaramente gli alberi lontani, avvolti nella nebbia. Era come se la temperatura fosse scesa di parecchi gradi, facendomi rabbrividire."


translate italian day4_un_9d47d7cc:


    un "Hai paura?"


translate italian day4_un_9191d5fa:


    "Mi ha chiesto con calma."


translate italian day4_un_8251b3c4:


    me "Sinceramente?"


translate italian day4_un_245b3c9a:


    "Ha sorriso quasi impercettibilmente, e mi ha preso per mano."


translate italian day4_un_9ddc743b:


    "Quel suo gesto mi avrebbe sicuramente causato una tempesta di emozioni in qualsiasi altra situazione, ma adesso era solo una necessità di base."


translate italian day4_un_763505e7:


    "Ci siamo avvicinati lentamente all'edificio."


translate italian day4_un_ce617998_1:


    "…"


translate italian day4_un_679ab4c6:


    "Camminando attraverso il parco giochi ho fatto girare il carosello, facendolo stridere in modo tetro mentre faceva mezzo giro."


translate italian day4_un_3bade428:


    "Lena ha rabbrividito, stringendomi la mano ancora più forte."


translate italian day4_un_1dd28790:


    me "Scusa… Stavo pensando alla mia infanzia."


translate italian day4_un_5a29deb9:


    un "Ti piacevano i caroselli?"


translate italian day4_un_d61e8c95:


    me "Sì… A dire il vero, non saprei, non mi ricordo. Forse. A tutti i bambini piacciono."


translate italian day4_un_ad4edfc8:


    un "A me non piacevano."


translate italian day4_un_5ac38cf1:


    me "Perché?"


translate italian day4_un_200a724b:


    un "Ho avuto le vertigini quando ci ero salita."


translate italian day4_un_42576b67:


    me "Non mi stupisce, se giri troppo velocemente."


translate italian day4_un_1f63bde1:


    un "Le altalene mi piacevano di più."


translate italian day4_un_417eff12:


    me "Beh, si può avere le vertigini anche sulle altalene!"


translate italian day4_un_961f1961:


    un "E perché?"


translate italian day4_un_7479b07e_1:


    me "Non so…"


translate italian day4_un_33548823:


    "Quella conversazione mi stava distraendo un po', e ho smesso di preoccuparmi di ogni cosa: di Shurik, del nostro viaggio notturno, di Lena…"


translate italian day4_un_efd8ecf9:


    th "Dopotutto, questo mondo non è poi così alieno."


translate italian day4_un_0a11ff96:


    "Finalmente abbiamo raggiunto le porte…"


translate italian day4_un_b6e2f0dc:


    "L'interno di quel vecchio edificio mi ricordava una scuola materna, quella che avevo frequentato da bambino."


translate italian day4_un_4b790dca:


    "A prima vista persino la sistemazione della stanza sembrava la stessa."


translate italian day4_un_b01a7f13:


    me "Shurik!"


translate italian day4_un_2554ba11:


    un "Shurik!"


translate italian day4_un_5498d175:


    "Ci ha risposto un silenzio tombale.{w} Persino il vento all'esterno si è calmato."


translate italian day4_un_833aced3:


    me "Sembra che non ci sia nessuno qui."


translate italian day4_un_6bea7733:


    un "Dovremmo controllare lo stesso."


translate italian day4_un_0edfb52c:


    "Il coraggio di Lena non finiva di stupirmi."


translate italian day4_un_a85771c8:


    "O meglio, la sua mancanza di istinto di autoconservazione."


translate italian day4_un_7163af4a:


    "Non saprei dire se quel suo comportamento risultasse strano per lei."


translate italian day4_un_a70602a7:


    me "D'accordo, facciamolo…"


translate italian day4_un_ce617998_2:


    "…"


translate italian day4_un_6f0ee1a6:


    "Abbiamo esaminato attentamente tutte le sale del vecchio campo, ho anche ispezionato la soffitta."


translate italian day4_un_587c11d8:


    "C'erano segni evidenti che la gente aveva visitato questo luogo: giornali, bottiglie vuote e altra immondizia. Ma non c'era alcuna traccia di Shurik."


translate italian day4_un_70fc2e8b:


    "Siamo ritornati alla sala dove avevamo iniziato la nostra ricerca."


translate italian day4_un_ed5bdfd6:


    me "E adesso cosa facciamo?"


translate italian day4_un_4657d7ce:


    un "Non ne ho idea…"


translate italian day4_un_c19ecb61:


    "Lena si è seduta sui gradini, fissandosi i piedi."


translate italian day4_un_a8e34a9c:


    me "Penso che dovremmo tornare indietro…"


translate italian day4_un_c0d7168c:


    "Ho proposto con cautela."


translate italian day4_un_4ca3177a:


    me "È tardi ormai e… credi davvero che riusciremmo noi due soli a setacciare l'intera foresta?"


translate italian day4_un_85add91e:


    un "Forse hai ragione."


translate italian day4_un_d3d630bf:


    "Aveva lo sguardo triste, e la sua espressione mi ha fatto capire che la ricerca non era ancora finita."


translate italian day4_un_8aa027b2:


    me "Sì, ho ragione!"


translate italian day4_un_66287c48:


    "Ho alzato le braccia in segno di rassegnazione e mi sono seduto accanto a lei."


translate italian day4_un_b2005ce0:


    me "Dobbiamo pensare al peggio…"


translate italian day4_un_98fbbb39:


    un "Vuoi forse dire che…?"


translate italian day4_un_a7f8624d:


    me "No, ma… Credi che ci siano animali selvatici nei dintorni?"


translate italian day4_un_999aca00:


    un "Ne dubito."


translate italian day4_un_65356482:


    "Lena si è subito calmata."


translate italian day4_un_e4ef2e97:


    me "Magari sta dormendo da qualche parte! Si sveglierà al mattino e tornerà al Campo."


translate italian day4_un_6e34d368:


    un "Sì, certo…"


translate italian day4_un_064ad673:


    "Sono saltato in piedi e ho iniziato a camminare in cerchio intorno alla sala."


translate italian day4_un_9ae0b3a2:


    th "Vorrei davvero abbandonare questo luogo, uscire dalla foresta, ma è come se il comportamento di Lena mi stesse trattenendo qui."


translate italian day4_un_06d65acf:


    "Volevo continuare a tentare di dissuaderla, quando ho notato qualcosa sul pavimento."


translate italian day4_un_3fde514e:


    "Era una botola. C'erano piccoli mucchi di spazzatura e polvere tutt'attorno."


translate italian day4_un_00a5fafc:


    th "Dev'essere stata aperta di recente!"


translate italian day4_un_2bed846c:


    me "Guarda."


translate italian day4_un_39f5c0fa:


    un "Credi che Shurik possa essere lì?"


translate italian day4_un_1bb4e1f7:


    "Lena si è accovacciata e ha tirato la maniglia con attenzione."


translate italian day4_un_cdff3e66:


    me "Forse non Shurik, ma qualcuno di sicuro è passato qui di recente."


translate italian day4_un_cb3016d3:


    "Mi stavo già pentendo di aver trovato quella dannata porta per l'inferno."


translate italian day4_un_f533d24e:


    un "Controlliamo?"


translate italian day4_un_a2335b11:


    "La botola non era molto pesante, quindi era possibile aprirla senza troppa fatica."


translate italian day4_un_ef14dccc:


    "Ho puntato la torcia al suo interno e ho visto una scala che scendeva per un paio di metri."


translate italian day4_un_c80ee062:


    me "Sembra una cantina…"


translate italian day4_un_a7796b9d:


    un "Scendiamo?"


translate italian day4_un_3d1667de:


    "Ho guardato Lena per un istante, cercando di capire cosa avesse in mente."


translate italian day4_un_d42ee41a:


    th "Ha forse il desiderio di avventura di Ulyana?{w} E allora dov'è il suo entusiasmo giovanile?"


translate italian day4_un_e799e894:


    th "O forse è andata un po' fuori di testa..."


translate italian day4_un_637e04ca:


    "Lena non sembrava essere una pazza."


translate italian day4_un_d58dfe0e:


    th "Ma in ogni caso, come potrei sapere con certezza che lei sia davvero un essere umano, e che io possa giudicarla secondo la logica del comportamento umano?"


translate italian day4_un_dfcf67ae:


    "Quel pensiero avrebbe dovuto spaventarmi, ma in qualche modo non meritava la mia attenzione tra i milioni di altri pensieri.{w} Alcuni di essi erano importanti – come ad esempio, cosa potesse esserci laggiù."


translate italian day4_un_ce617998_3:


    "…"


translate italian day4_un_bbdc9393:


    "Sono sceso e mi sono guardato attorno."


translate italian day4_un_3a575910:


    me "È tutto a posto."


translate italian day4_un_5222c78f:


    "Dopo essermi assicurato che non ci fosse alcun pericolo, ho chiamato Lena."


translate italian day4_un_fe24c71d:


    "Siamo rimasti in piedi nel lungo corridoio, che certamente non era una cantina."


translate italian day4_un_f3fd57d1:


    "La sua architettura assomigliava più alle segrete del KGB, o a un tunnel di manutenzione della metropolitana – non saprei dire quale delle due opzioni sia la meno peggiore."


translate italian day4_un_00ed8ed5:


    "C'erano innumerevoli cavi lungo le pareti, fissati da ganci metallici ogni mezzo metro. Delle lampade pendevano dal soffitto, coperte da macchie di ruggine. Il cemento sbriciolato scricchiolava sotto ai nostri piedi in modo spiacevole."


translate italian day4_un_35698e3a_1:


    un "Andiamo avanti?"


translate italian day4_un_c6ff2b5b:


    "Ha chiesto Lena, in modo apatico."


translate italian day4_un_43695a7a:


    me "Dove? Di là?"


translate italian day4_un_53b04f6d:


    un "Beh, sì. E se Shurik fosse lì?"


translate italian day4_un_1a763905:


    me "Perché dovrebbe essere andato lì?"


translate italian day4_un_673ea5b0:


    "In ogni caso oggi non ero proprio in grado di dirle di no, così abbiamo messo da parte la nostra paura e ci siamo addentrati nel buio."


translate italian day4_un_ce617998_4:


    "…"


translate italian day4_un_0431d71e:


    "Lena camminava al mio fianco, tenendomi per mano."


translate italian day4_un_5e66f0b6:


    "Il silenzio del sotterraneo veniva interrotto solo dal rumore dei nostri passi e dall'acqua che gocciolava dal soffitto."


translate italian day4_un_cdc7cb75:


    "Camminavamo lentamente, forse anche troppo. Improvvisamente sono stato travolto da un'ondata di claustrofobia."


translate italian day4_un_7b3a6c14:


    "Ho stretto i denti e pure la torcia, ma ho subito allentato la presa, per paura di danneggiare la nostra unica fonte di luce."


translate italian day4_un_fa5b94fe:


    "Lena è rimasta in silenzio, e quel suo silenzio era più fragoroso di qualsiasi parola. Ho iniziato ad avere paura."


translate italian day4_un_3b137961:


    me "Di' qualcosa."


translate italian day4_un_9faa94d8:


    un "Porta."


translate italian day4_un_ad02f53a:


    me "Cosa?"


translate italian day4_un_9faa94d8_1:


    un "C'è una porta."


translate italian day4_un_72630fc0:


    "Ha indicato qualcosa davanti a noi."


translate italian day4_un_934026f6:


    "Siamo giunti a un'enorme porta metallica, che aveva un simbolo di rischio biologico stampato su di essa."


translate italian day4_un_eb8f4603:


    me "Sembra un rifugio antiaereo…"


translate italian day4_un_f1f136f3:


    un "Sì. Ne ho sentito parlare."


translate italian day4_un_9e352857:


    me "Perché si trova proprio qui?"


translate italian day4_un_316a211f:


    un "Non ne ho idea. Forse per la crisi dei missili cubana."


translate italian day4_un_9ecf579f:


    me "Cubana?"


translate italian day4_un_41b4cc8d:


    "Ho stimato approssimativamente quale potesse essere il periodo in cui fu costruito il Campo, e aveva un certo senso."


translate italian day4_un_c02a1e81:


    "Tuttavia, costruire un rifugio antiaereo in un luogo come questo sarebbe come costruire un aeroporto in un villaggio sperduto.{w} Non è abbastanza profondo ed è troppo lontano dalla civiltà."


translate italian day4_un_225dc90a:


    "La ruota della porta ha scricchiolato. Ho dovuto spingere con tutte le mie forze, prima di riuscire a farla ruotare un paio di volte."


translate italian day4_un_4b931dd1:


    "Mi sono deciso e ho aperto la porta con difficoltà..."


translate italian day4_un_856f240c:


    "Siamo entrati nella stanza che sembrava essere il «quartiere residenziale»."


translate italian day4_un_dfdbb734:


    "C'erano alcuni letti, degli armadi, delle attrezzature scientifiche. Si erano preparati accuratamente per un'apocalisse nucleare."


translate italian day4_un_a4794e3a:


    "Ma non abbiamo trovato alcuna traccia di Shurik."


translate italian day4_un_d5312815_1:


    un "Guarda."


translate italian day4_un_f0153e82:


    "Lena teneva in mano una pistola lanciarazzi, sorridendo."


translate italian day4_un_a1e442ec:


    me "Perché ci dovrebbe servire?"


translate italian day4_un_9a220deb:


    un "Per combattere i mostri."


translate italian day4_un_e23f4f15:


    me "Non ci sono mostri qui."


translate italian day4_un_7c7f5007:


    "O almeno volevo crederci."


translate italian day4_un_1318a9ad:


    un "Se lo dici tu…"


translate italian day4_un_6d9f91ff:


    me "Te lo garantisco!"


translate italian day4_un_df288870:


    "Non volevo turbarla, e così ho infilato la pistola lanciarazzi nella mia cintura. Sarebbe potuta tornarci utile…"


translate italian day4_un_f457cf0f:


    "Abbiamo setacciato la stanza un'altra volta."


translate italian day4_un_7131cb1c:


    "C'erano due uscite.{w} La prima era la porta da dov'eravamo entrati, la seconda era un'altra porta esattamente identica, sulla parete a sinistra."


translate italian day4_un_3dda53ec:


    "Per un istante mi sono emozionato, avevo il desiderio di raggiungere la fine di questo labirinto e di scoprire quale premio mi attendesse lì."


translate italian day4_un_ae82ae84:


    "Tuttavia, questo non era di certo un videogioco, e non avevamo l'opzione di poter salvare la partita."


translate italian day4_un_8be5d580:


    un "Forse con questo?"


translate italian day4_un_634ac583:


    "Lena mi ha consegnato un grosso piede di porco."


translate italian day4_un_ad83807e:


    me "No. Prima voglio provare senza."


translate italian day4_un_c87458f4:


    "Ma la ruota della porta non si è mossa di un millimetro – ha solo scricchiolato in modo sinistro."


translate italian day4_un_901746cb:


    me "Ok. Dammelo."


translate italian day4_un_2ece48fa:


    "Con il piede di porco è stato fin troppo facile."


translate italian day4_un_c8250092:


    "Alla fine l'ostacolo è crollato, abbattendosi sul pavimento con un tonfo."


translate italian day4_un_50d5d9d2:


    "I cardini erano completamente arrugginiti."


translate italian day4_un_13d39abc:


    "Ho puntato la torcia attraverso il passaggio. C'era un corridoio, proprio come quello da cui eravamo arrivati."


translate italian day4_un_35698e3a_2:


    un "Andiamo?"


translate italian day4_un_2f236b34:


    "Era come se Lena mi stesse costantemente spingendo in avanti."


translate italian day4_un_d3520d70:


    me "Dove vai così di fretta?"


translate italian day4_un_338e0f1a:


    un "Io? Non…"


translate italian day4_un_340ec68e:


    "È arrossita, confusa."


translate italian day4_un_e6e2a051:


    th "Ebbene, ancora… Come dovrei considerarla?{w} Una volta non ha paura di niente, e subito dopo si smarrisce a causa di una parola."


translate italian day4_un_2968b0cc:


    me "Sembra che tu non abbia paura di niente."


translate italian day4_un_1c959e2d:


    un "Non lo so. Di cosa dovrei aver paura?"


translate italian day4_un_aa5f81a3:


    un "Ci sei tu a proteggermi…"


translate italian day4_un_3f6ffa1d:


    "Ha aggiunto, quasi in silenzio..."


translate italian day4_un_a766ab32:


    "Quindi Lena contava su di me. Credeva in me indipendentemente da tutto…"


translate italian day4_un_4bac4a4b:


    th "È possibile.{w} Stupido, ingenuo, ma possibile."


translate italian day4_un_cb8a29df:


    "Sapevo chiaramente che non potevo proteggere nessuno, neppure me stesso. Nulla dipendeva da me in questo mondo."


translate italian day4_un_72c7c023:


    "Le forze che mi hanno portato qui avrebbero potuto fare qualunque cosa esse volessero!"


translate italian day4_un_e908fa11:


    "Il che non significava per forza che la morte mi dovesse aspettare in ogni angolo di questo tunnel, avrebbe potuto insidiarsi ovunque in questo Campo…"


translate italian day4_un_b749d8c0:


    me "Andiamo!"


translate italian day4_un_ce617998_5:


    "…"


translate italian day4_un_04e4c88e:


    "Ho provato a camminare più velocemente, ma la cosa sembrava non aver disturbato Lena, che riusciva a tenere il passo con facilità."


translate italian day4_un_6fa018f9:


    "Questo corridoio era esattamente come il precedente.{w} In ogni suo dettaglio."


translate italian day4_un_06e7b9dd:


    "Non c'era niente di sconvolgente in tutto ciò, ma ad un certo punto ho avuto la sensazione che stessimo girando in tondo."


translate italian day4_un_b5e87c10:


    "La torcia nella mia mano continuava a tremare visibilmente. Il fascio di luce rimbalzava da una parete all'altra, dal pavimento al soffitto, e ad un certo punto ha evidenziato una grossa buca dinanzi a noi…"


translate italian day4_un_787a88e3:


    "Non era molto profonda, e siamo riusciti a scorgere delle rotaie."


translate italian day4_un_d5a761a9:


    un "Cosa ci sarà laggiù?"


translate italian day4_un_2b1cc4e3:


    me "Sembra una miniera."


translate italian day4_un_f533d24e_1:


    un "Andiamo a vedere?"


translate italian day4_un_403cf625:


    me "Perché invece non proseguiamo in questo corridoio?"


translate italian day4_un_13849591:


    un "Non lo so, credo che dovremmo scendere laggiù."


translate italian day4_un_30068944:


    "Ho valutato l'altezza – sarebbe stato facile per noi risalire."


translate italian day4_un_b6d2f56b:


    me "Ok. Vediamo cosa c'è là sotto."


translate italian day4_un_4b5ac164:


    "Sono saltato giù e ho aiutato Lena a scendere."


translate italian day4_un_540f2604:


    "Era proprio una miniera."


translate italian day4_un_b1af84b4:


    th "Chissà cos'hanno scavato qui."


translate italian day4_un_37b84aa9:


    me "Che minerali ci sono in questa zona?"


translate italian day4_un_0da946d4:


    un "Non lo so."


translate italian day4_un_7839cf57:


    me "Beh, già, che domanda stupida. Sembra che ormai non ne sia rimasto più alcuno…"


translate italian day4_un_b53b2290:


    "Ci siamo addentrati nell'oscurità."


translate italian day4_un_a7838803:


    "Era difficile camminare, perché non sapevo dove mettere i piedi – tra assi di legno barcollanti e terreno instabile."


translate italian day4_un_9cee4921:


    "E non riuscivo nemmeno a stare vicino alla parete – il tunnel era così stretto che eravamo obbligati a rimanere sempre in mezzo alle rotaie, e io non volevo lasciare la mano di Lena."


translate italian day4_un_2c6a97fe:


    "Finalmente siamo arrivati ad un bivio."


translate italian day4_un_f604608d:


    me "Ma bene."


translate italian day4_un_7b74056c:


    un "E adesso dove andiamo?"


translate italian day4_un_d2b4c033:


    me "Dove? Non sono nemmeno sicuro che ce la faremo ad uscire di qui, è come se stessimo giocando a Pac-Man…"


translate italian day4_un_9b63fa7b:


    un "Giocando a cosa?"


translate italian day4_un_a4513cee:


    me "Non importa. In questo modo ci perderemo."


translate italian day4_un_f05312d6:


    un "E se ci fosse un'uscita?"


translate italian day4_un_99f6233a:


    me "Potrebbe esserci… E se invece non ci fosse?"


translate italian day4_un_7749b4db:


    un "Allora pensi che dovremmo tornare indietro?"


translate italian day4_un_93f7497e:


    "Mi sono morso il labbro fino a farlo sanguinare, e ho gridato più forte che potevo."


translate italian day4_un_b01a7f13_1:


    me "Shurik!"


translate italian day4_un_3def6bf8:


    "Il forte eco è risuonato in tutte le direzioni contemporaneamente. Facendo addirittura cadere del terreno in certi punti del soffitto."


translate italian day4_un_a6afb8d5:


    me "Come puoi vedere…"


translate italian day4_un_d25a0ad8:


    un "Allora andrò da sola."


translate italian day4_un_ad02f53a_1:


    me "Cosa?!"


translate italian day4_un_ff99c828:


    "Ho ghignato stupidamente."


translate italian day4_un_64e96062:


    me "Da sola? E dove?"


translate italian day4_un_e3e62f26:


    un "Dobbiamo trovare Shurik. Lui potrebbe…"


translate italian day4_un_55c7ffde:


    "Lena è arrossita di colpo e ha fissato il terreno."


translate italian day4_un_6003d47f:


    me "No-no-no, così non va bene. Se proprio dobbiamo andare, ci andremo insieme."


translate italian day4_un_3f6ce067:


    un "Ok. Allora andiamo."


translate italian day4_un_08bcf5c5:


    "Ha sorriso e mi ha preso per mano."


translate italian day4_un_f6728d01:


    th "Ma come ci riesce..."


translate italian day4_un_c9cc1313:


    me "Però prima dovremmo…"


translate italian day4_un_d8a01162:


    "Ho raccolto da terra una pietra appuntita e ho inciso una «X» su una delle travi che sostenevano le pareti."


translate italian day4_un_01e8bf07:


    me "Così sapremo qual è il punto di partenza."


translate italian un_mine_coalface_e40597aa:


    "Siamo già stati qui."


translate italian un_mine_coalface_5b4c3c90:


    "Finalmente il tunnel è sboccato in una grande stanza con un alto soffitto."


translate italian un_mine_coalface_3ef8c583:


    "Anche se difficilmente la si potrebbe chiamare stanza. Sembrava una grotta scavata dai minatori.{w} Forse avevano estratto il carbone, o magari l'oro."


translate italian un_mine_coalface_3c54fe69:


    "Le pareti erano state frantumate da picconate o da martelli pneumatici."


translate italian un_mine_coalface_65a5fdb8:


    "C'era un buio totale qui dentro, la nostra unica salvezza era la torcia."


translate italian un_mine_coalface_fadb68bd:


    "Se si fosse rotta sarebbe stato un grosso problema…"


translate italian un_mine_coalface_58411121:


    "Ho notato un pezzo di stoffa rosso in un angolo."


translate italian un_mine_coalface_ca0a4f90:


    "Era il foulard di un pioniere!"


translate italian un_mine_coalface_96b3272c:


    "Shurik era decisamente nei dintorni."


translate italian un_mine_coalface_93ec21f7:


    me "Shurik! Shurik!"


translate italian un_mine_coalface_ca9e3fa1:


    un "Shurik! Shurik!"


translate italian un_mine_coalface_b99be3e2:


    "Solo il nostro eco risuonava in quei sotterranei."


translate italian un_mine_coalface_c1938ec1:


    un "Non dev'essere molto lontano, dato che abbiamo trovato il suo foulard qui."


translate italian un_mine_coalface_e80273e8:


    "A dire il vero, io mi stavo chiedendo dove fosse effettivamente il «qui»…"


translate italian un_mine_coalface_a411960f:


    th "Dove potrebbe essere andato partendo da questo punto?"


translate italian un_mine_coalface_686bf61c:


    "Non c'erano altre uscite in quella stanza."


translate italian un_mine_coalface_7ea80226:


    th "Certamente, è possibile che non abbiamo esplorato ogni singolo angolo di questi tunnel…"


translate italian un_mine_coalface_858b6cbe:


    th "E ciò significa che dobbiamo continuare a cercarlo!"


translate italian un_mine_exit_0e2f1383:


    "Dietro l'ennesimo angolo, una porta di legno è apparsa davanti a noi."


translate italian un_mine_exit_6585ac5f:


    me "Almeno è qualcosa…"


translate italian un_mine_exit_3875ba7b:


    un "Come?"


translate italian un_mine_exit_81846d80:


    me "Almeno non è un altro bivio."


translate italian un_mine_exit_97b34350:


    un "Cosa ci sarà lì dentro?"


translate italian un_mine_exit_75b7c7b4:


    me "Non abbiamo altra scelta, dobbiamo controllare."


translate italian un_mine_exit_1f752f8a:


    "Ho tirato la maniglia con decisione e ho aperto la porta."


translate italian un_mine_exit_351ceb93:


    "Dietro ad essa c'era una stanza, forse era una delle stanze di manutenzione del rifugio antiaereo."


translate italian un_mine_exit_a8afc189:


    "Bottiglie vuote e mozziconi di sigarette erano sparsi ovunque. Le pareti erano tutte ricoperte di scarabocchi."


translate italian un_mine_exit_9c847221:


    th "Questo significa che ci dev'essere un'altra uscita qui vicino!"


translate italian un_mine_exit_dd71665d:


    "Non volevo credere che le persone che avevano abbandonato quella stanza fossero passate da dove siamo arrivati noi."


translate italian un_mine_exit_f89bf0e3:


    "Purtroppo Shurik non era nemmeno qui."


translate italian un_mine_exit_1cc1d82f:


    me "Oh…"


translate italian un_mine_exit_614dca6b:


    "Mi sono lasciato scivolare lungo la parete, fino a terra."


translate italian un_mine_exit_f5df0746:


    me "Siamo stati dappertutto."


translate italian un_mine_exit_c57bbc05:


    un "Non dappertutto."


translate italian un_mine_exit_a8a4d8de:


    "Lena ha indicato una porta nell'angolo."


translate italian un_mine_exit_4b1b3ebe:


    "Era simile a quella che portava al rifugio antiaereo."


translate italian un_mine_exit_7208c8c6:


    me "Probabilmente c'è davvero un'uscita, come hai detto tu."


translate italian un_mine_exit_35698e3a:


    un "Andiamo?"


translate italian un_mine_exit_9f46626d:


    me "Riposiamoci un attimo."


translate italian un_mine_exit_b993fafe:


    un "Ok."


translate italian un_mine_exit_fd0aed6a:


    "Lena si è seduta accanto a me, molto vicino, e mi ha tenuto la mano."


translate italian un_mine_exit_6e5e2fd8:


    un "Va tutto bene."


translate italian un_mine_exit_e54b39e9:


    me "Cosa vuoi dire?"


translate italian un_mine_exit_840b2721:


    un "Che non abbiamo trovato Shurik."


translate italian un_mine_exit_8f9391d8:


    me "Dovremmo pensare a come uscire piuttosto…"


translate italian un_mine_exit_533890b7:


    un "Usciremo."


translate italian un_mine_exit_f9b93501:


    me "Sì. Dovrei ricordare la strada."


translate italian un_mine_exit_6590db25:


    "O almeno così mi sembrava."


translate italian un_mine_exit_e04c5cfb:


    un "Io non sono per niente spaventata."


translate italian un_mine_exit_f7f16276:


    "Ha detto improvvisamente, dopo una breve pausa."


translate italian un_mine_exit_69d18c31:


    me "È una cosa buona."


translate italian un_mine_exit_d0909642:


    un "Perché sono insieme a te."


translate italian un_mine_exit_2f2d5a31:


    "Ad un tratto, abbiamo sentito un rumore provenire da dietro la porta della miniera."


translate italian un_mine_exit_debe8be2:


    "Sono balzato in piedi e ho cercato affannosamente qualcosa da usare come arma di difesa."


translate italian un_mine_exit_266bb45a:


    "Il rumore dei passi pesanti si faceva sempre più vicino."


translate italian un_mine_exit_75fc727b:


    "Alla fine la porta si è aperta e Shurik è apparso sulla soglia."


translate italian un_mine_exit_c305b061:


    "Sono rimasto pietrificato in silenzio, fissandolo."


translate italian un_mine_exit_80c1c240:


    sh "Eccovi finalmente! Credevate forse di riuscire a sfuggirmi?"


translate italian un_mine_exit_ad02f53a:


    me "Cosa?"


translate italian un_mine_exit_f4b92687:


    sh "Credevate davvero che non vi avrei trovato? Ma ci sono riuscito!"


translate italian un_mine_exit_5db31dbf:


    "Non era sano di mente, questo era certo: il suo volto era distorto da una smorfia spaventosa, i suoi occhi brillavano dietro ai suoi occhiali. Il pioniere scomparso teneva in mano una sbarra di metallo."


translate italian un_mine_exit_6f664776:


    me "Sei impazzito?! Siamo noi!"


translate italian un_mine_exit_2e350af4:


    sh "Sì, lo vedo che siete voi!"


translate italian un_mine_exit_2bec6403:


    "Si è avvicinato di qualche passo."


translate italian un_mine_exit_7bb9ce4b:


    "Ho protetto Lena istintivamente."


translate italian un_mine_exit_6cb76a93:


    sh "Pensavate di potervi prendere gioco di me? Portandomi qua e là?! «A destra, a sinistra, a destra, a sinistra»?! E io vi seguivo, vi seguivo…"


translate italian un_mine_exit_a94436e5:


    "Ha alzato la sbarra di metallo."


translate italian un_mine_exit_039ca5ae:


    "Tutto il resto è accaduto al rallentatore."


translate italian un_mine_exit_7e0cb307:


    "Shurik, che si è scagliato contro di noi."


translate italian un_mine_exit_38d16829:


    "Io, che ho spinto via Lena."


translate italian un_mine_exit_61257df0:


    "La sbarra, che si avvicinava lentamente alla mia testa..."


translate italian un_mine_exit_7276c335:


    "... La mia mano che ha abbandonato la torcia, facendola roteare nell'aria..."


translate italian un_mine_exit_fb557852:


    "E poi, il buio totale. Il respiro affannoso. Il sangue che pulsava nelle tempie. E poi silenzio – un terribile, pesante silenzio, che si fondeva con l'oscurità."


translate italian un_mine_exit_ee0f0bdf:


    "Ho brancolato nel buio, cercando una parete con le mani, quando ho sentito il tocco di qualcuno."


translate italian un_mine_exit_d7fd3730:


    un "Non aver paura."


translate italian un_mine_exit_2233ca2f:


    "Ho sentito una voce familiare accanto a me."


translate italian un_mine_exit_79b75990:


    me "Dove sei, maledetto lunatico?!"


translate italian un_mine_exit_a5efbe41:


    "Ho gridato."


translate italian un_mine_exit_6a0e7f4b:


    un "Se n'è andato."


translate italian un_mine_exit_90465eb1:


    me "Da che parte? Dov'è andato?"


translate italian un_mine_exit_a742d1cc:


    "La voce di Lena non sembrava troppo calma, ma non era neppure come si suppone debba essere in una tale situazione."


translate italian un_mine_exit_7606245e:


    un "Calmati."


translate italian un_mine_exit_73e0f4a4:


    "Mi ha abbracciato con tenerezza e si è stretta a me."


translate italian un_mine_exit_e2d3e825:


    "Ho cercato di riprendere i sensi, di riprendere fiato, di adattare i miei occhi all'oscurità."


translate italian un_mine_exit_8ef5b76e:


    me "E adesso cosa facciamo?"


translate italian un_mine_exit_6729a574:


    un "Hai una pistola."


translate italian un_mine_exit_34ee4713:


    me "Bene. E a chi dovrei sparare?"


translate italian un_mine_exit_dae1cf79:


    un "È caricata con un razzo."


translate italian un_mine_exit_5b8394c2:


    "Forse aveva ragione."


translate italian un_mine_exit_e10fcac5:


    "Ho tolto la pistola dalla mia cintura, puntandola di lato, e ho sparato un colpo."


translate italian un_mine_exit_e38eb2e8:


    "La stanza si è illuminata di un bagliore rosso."


translate italian un_mine_exit_c7063a79:


    "Il razzo, finito in un angolo, sembrava un fuoco d'artificio."


translate italian un_mine_exit_7fae402f:


    un "Muoviamoci, non durerà a lungo."


translate italian un_mine_exit_25f7abbc:


    me "Dove?"


translate italian un_mine_exit_fdecf719:


    "Lena ha indicato la seconda porta."


translate italian un_mine_exit_6d6ad7e8:


    "Non è stato difficile aprirla, e ci siamo immersi nell'oscurità…"


translate italian un_mine_exit_ce617998:


    "…"


translate italian un_mine_exit_bf010e6a:


    "Il bagliore del razzo si stava lentamente attenuando."


translate italian un_mine_exit_be25fae0:


    "Inciampavo ad ogni passo, sono pure caduto un paio di volte, ma senza rallentare."


translate italian un_mine_exit_9fb2c558:


    th "Se si spegnesse…"


translate italian un_mine_exit_c6df834b:


    "Finalmente abbiamo visto una luce davanti a noi, e siamo arrivati davanti a una scala che portava verso una grata sul soffitto. Il razzo ha fischiato e si è spento del tutto."


translate italian un_mine_exit_fc8c5efd:


    me "Grazie al cielo…"


translate italian un_mine_exit_6ebe51f8:


    "Abbiamo scoperto di essere esattamente sotto la statua di Genda."


translate italian un_mine_exit_b28b9392:


    "La grata era piuttosto robusta, ma siamo riusciti ad aprirla dopo aver rotto i bulloni con la torcia."


translate italian un_mine_exit_63a68c76:


    "Appena raggiunta la superficie sono crollato a terra, esausto."


translate italian un_mine_exit_729724c7:


    me "È stato terribile…"


translate italian un_mine_exit_772c30bb:


    "Lena si è seduta accanto a me..."


translate italian un_mine_exit_f5eeaa60:


    me "Proprio tremendo…"


translate italian un_mine_exit_7b02c40e:


    "A quel punto non m'importava più nulla di questo mondo, del Campo, del bus 410, o della mia vita precedente."


translate italian un_mine_exit_0cde231e:


    "La cosa più strana e brutta è che non c'era nulla di soprannaturale in quegli eventi."


translate italian un_mine_exit_d36716df:


    "Shurik era diventato pazzo, folle.{w} Niente di strano in ciò, lo sarei diventato anch'io al posto suo…"


translate italian un_mine_exit_b064b37c:


    "Lena mi ha accarezzato i capelli con tenerezza, sorridendo."


translate italian un_mine_exit_97e317a3:


    un "È tutto finito."


translate italian un_mine_exit_b46c791f:


    me "Non ne sono sicuro… C'è un pioniere impazzito che vaga per la foresta! Si potrebbe anche dire – un maniaco omicida!"


translate italian un_mine_exit_dd9e978b:


    un "Credo che si riprenderà."


translate italian un_mine_exit_2cdd58d5:


    me "Si riprenderà? Non ne sono proprio convinto!"


translate italian un_mine_exit_7fc6d167:


    un "La cosa più importante è che noi stiamo bene."


translate italian un_mine_exit_89d38d04:


    "Stava sorridendo ancora."


translate italian un_mine_exit_b923095e:


    me "Ma come fai a essere così calma?"


translate italian un_mine_exit_79468419:


    un "Te l'ho detto prima, non ho niente da temere quando sto con te."


translate italian un_mine_exit_f739c9b0:


    "Era vero. Probabilmente ero riuscito a salvare entrambi là sotto."


translate italian un_mine_exit_c066a1fa:


    "Ma è stato solo per puro caso, nulla più."


translate italian un_mine_exit_507b51cd:


    "Se Shurik fosse stato più agile…{w} o più folle…"


translate italian un_mine_exit_de6f6646:


    me "Grazie per il complimento."


translate italian un_mine_exit_735a4f9a:


    un "Non c'è di che."


translate italian un_mine_exit_1f9cc7f6:


    me "Anche se credo che saremmo dovuti rimanere qui in primo luogo."


translate italian un_mine_exit_332127e8:


    un "Probabilmente hai ragione."


translate italian un_mine_exit_4bde9263:


    "Ha detto quietamente."


translate italian un_mine_exit_e41e554d:


    "Mi è venuto un gran sonno.{w} A causa dello stress, della stanchezza, della notte fonda…"


translate italian un_mine_exit_38af4b55:


    "Sarebbe stato meglio andare a dormire."


translate italian un_mine_exit_db493765:


    th "Ma dovrei alzarmi e camminare fino alla casetta della leader…{w} Non sono pronto per una cosa del genere."


translate italian un_mine_exit_1dff0f5a:


    "I miei occhi si sono chiusi solo per un istante…"


translate italian un_mine_exit_ce617998_1:


    "…"


translate italian un_mine_exit_43cab7ab:


    "Le stelle mi fissavano dal cielo notturno.{w} Migliaia di stelle, forse anche milioni."


translate italian un_mine_exit_0f63591e:


    "Adesso la loro luce non sembrava più così fredda e distante – al contrario, esse brillavano per me, come se stessero sussurrando tra di loro, raccontandosi delle allegre favole."


translate italian un_mine_exit_46e33d3d:


    "Favole di una galassia molto lontana, di soffici maialini viola, di misteriose cinture asteroidali dove molte navi erano scomparse, di un impavido capitano di astronave e del suo coraggioso equipaggio, di tesori straordinari e di vette irraggiungibili su un pianeta ai confini dell'universo…"


translate italian un_mine_exit_318652a2:


    th "Mi chiedo, è forse un sogno?"


translate italian un_mine_exit_7c64a5d0:


    "Mi sono alzato un poco e ho scoperto che la mia testa era appoggiata sul grembo di Lena."


translate italian un_mine_exit_4e391c09:


    me "Ho dormito a lungo?"


translate italian un_mine_exit_11be2cb0:


    "Ho chiesto confuso, ma senza avere fretta di alzarmi."


translate italian un_mine_exit_60386181:


    un "Non lo so. Non ho un orologio."


translate italian un_mine_exit_0467a4c4:


    "Ha ridacchiato."


translate italian un_mine_exit_240fb495:


    me "Più o meno quanto?"


translate italian un_mine_exit_71b8ed88:


    un "Beh. Forse una ventina di minuti."


translate italian un_mine_exit_aba21a0c:


    me "Ah… Allora va bene."


translate italian un_mine_exit_adab8aab:


    "Mi sono sdraiato di nuovo, sentendomi felice e tranquillo come mai prima d'ora. Gli eventi della notte stavano diventando sempre più distanti, come se stessi iniziando a dimenticarmeli, così come le favole che mi erano state raccontate poco prima dalle stelle."


translate italian un_mine_exit_d5ce5430:


    un "Shurik è tornato."


translate italian un_mine_exit_3d342d5a:


    me "Che cosa?!"


translate italian un_mine_exit_abe0afc6:


    "Sono balzato in piedi all'istante."


translate italian un_mine_exit_d4879afa:


    un "Eccolo là, sta dormendo su una panchina."


translate italian un_mine_exit_44f88c82:


    "Lena ha indicato le panchine, con aria sorpresa."


translate italian un_mine_exit_63d48677:


    me "E tu cosa…?"


translate italian un_mine_exit_fbd96cba:


    un "Stavi dormendo così bene che non ho voluto svegliarti."


translate italian un_mine_exit_502b0fe3:


    "Un brivido mi è sceso lungo la schiena, perché quel suo comportamento non solo era strano, ma anche inappropriato. La cosa mi ha spaventato anche più della follia di Shurik."


translate italian un_mine_exit_c0778843:


    "Un pazzo lunatico, che solo mezz'ora fa aveva tentato di ucciderci, arriva qui, si sdraia su una panchina addormentandosi, e lei se ne sta tranquilla come se niente fosse?"


translate italian un_mine_exit_f1ca775b:


    un "Nulla di cui preoccuparsi, sembrava proprio esausto…"


translate italian un_mine_exit_a948a0c5:


    "Lena è arrossita, confusa."


translate italian un_mine_exit_e06c41ca:


    un "Camminava barcollando e non ci ha nemmeno guardati. E se io avessi fatto un rumore…"


translate italian un_mine_exit_0c721333:


    "Stava quasi per piangere."


translate italian un_mine_exit_a8068fc6:


    me "Va bene. Non preoccuparti."


translate italian un_mine_exit_cfe251db:


    "C'era una certa logica nelle sue parole.{w} Probabilmente ha fatto la cosa giusta."


translate italian un_mine_exit_ae234167:


    "In ogni caso, dovevamo interrogare a fondo Shurik."


translate italian un_mine_exit_15e2c240:


    "Mi sono alzato di scatto, mi sono avvicinato rapidamente alla panchina sulla quale dormiva, e l'ho schiaffeggiato."


translate italian un_mine_exit_82e16e17:


    sh "Ahia!"


translate italian un_mine_exit_99fd0181:


    "Si è svegliato all'istante."


translate italian un_mine_exit_4372f7aa:


    sh "Ma che diavolo?!"


translate italian un_mine_exit_215fb22b:


    me "Cosa avevi in mente, bastardo?!"


translate italian un_mine_exit_13919f7d:


    sh "Cosa?"


translate italian un_mine_exit_a83772c6:


    "Shurik mi ha fissato con sguardo spaventato e per di più normale."


translate italian un_mine_exit_d21f05af:


    me "Che cos'è stato tutto quel delirio laggiù?!"


translate italian un_mine_exit_c53304b3:


    sh "Cosa, dove?"


translate italian un_mine_exit_55aa43d1:


    me "Nei sotterranei, nella miniera, nel rifugio antiaereo! Sei andato completamente fuori di testa?"


translate italian un_mine_exit_6ce75e3c:


    sh "Non ti capisco…"


translate italian un_mine_exit_f1ccf3ee:


    "Si è guardato attorno."


translate italian un_mine_exit_5b89be3d:


    sh "Perché sono qui?"


translate italian un_mine_exit_2b4eca13:


    me "E dove dovresti essere secondo te?! Oh, aspetta, lo so – dovresti essere in manicomio!"


translate italian un_mine_exit_1cd4852c:


    sh "Ero andato al vecchio campo al mattino per cercare i pezzi di ricambio, e poi…"


translate italian un_mine_exit_fc07db1d:


    un "Non ricordi nulla di ciò che è successo dopo?"


translate italian un_mine_exit_cf2bfb89:


    "Ha chiesto Lena, dopo essersi avvicinata a noi."


translate italian un_mine_exit_b74741dc:


    sh "Sì, e io…"


translate italian un_mine_exit_82e63599:


    me "Non fingere."


translate italian un_mine_exit_2be01380:


    "Gli ho detto con calma, e mi sono seduto accanto a lui."


translate italian un_mine_exit_37d99f1d:


    "Però sembrava davvero che non ricordasse niente."


translate italian un_mine_exit_eb7f67e1:


    sh "Non ci sto capendo niente… Non è scientifico!"


translate italian un_mine_exit_925090f3:


    me "E a chi importa. Non credere che io mi fidi di te."


translate italian un_mine_exit_ef21e5d1:


    sh "La perdita di memoria non può avvenire in questo modo…"


translate italian un_mine_exit_224d6a7e:


    "Shurik parlava da solo, mormorando qualcosa, senza degnarci di attenzione."


translate italian un_mine_exit_a8b5b91a:


    un "Andiamo."


translate italian un_mine_exit_3a1bc23d:


    "Ha detto Lena pacatamente."


translate italian un_mine_exit_5b76ac8a:


    me "Vuoi lasciarlo qui così?"


translate italian un_mine_exit_5ec8c2d4:


    un "Non è in sé adesso, deve farsi una dormita."


translate italian un_mine_exit_34e11933:


    me "È pericoloso lasciare questo psicopatico qui da solo! Potrebbe strangolare Electronik con una bobina durante la notte!"


translate italian un_mine_exit_e672ac80:


    un "Andrà tutto bene. Fidati di me."


translate italian un_mine_exit_fd384959:


    "Non avevo motivo di non fidarmi di lei."


translate italian un_mine_exit_5b31be1d:


    "Tuttavia, non avevo neppure alcun motivo di fidarmi di lei."


translate italian un_mine_exit_fbd85565:


    th "Ma d'altra parte, che importa adesso? Voglio solo addormentarmi il prima possibile!"


translate italian un_mine_exit_505d91ae:


    me "Va bene…"


translate italian un_mine_exit_19c4dce2:


    "Ci siamo allontanati da Shurik, che stava ancora borbottando qualcosa a sé stesso."


translate italian un_mine_exit_ce617998_2:


    "…"


translate italian un_mine_exit_cd10cb1d:


    un "Eccoci."


translate italian un_mine_exit_1d9b8f65:


    me "Cosa? Dove?"


translate italian un_mine_exit_41a8b963:


    un "Io devo andare più avanti."


translate italian un_mine_exit_561828ec:


    "Lena ha sorriso."


translate italian un_mine_exit_ac77630c:


    "Non stavo pensando proprio a niente durante il nostro tragitto, mi sono limitato a seguirla, senza accorgermi che eravamo già arrivati alla casetta di Olga Dmitrievna."


translate italian un_mine_exit_9513cd87:


    me "Sì…"


translate italian un_mine_exit_8cf9c83d:


    un "Grazie… per oggi."


translate italian un_mine_exit_6c156bd2:


    me "Non devi ringraziarmi. È già un bene che siamo tornati vivi. Vedremo cosa Shurik avrà da dire domani!"


translate italian un_mine_exit_05ed2d0a:


    un "Grazie comunque…"


translate italian un_mine_exit_9e3e0847:


    "Ha detto misteriosamente, distogliendo lo sguardo."


translate italian un_mine_exit_e88d2104:


    me "Di niente."


translate italian un_mine_exit_6ecba0e3:


    un "Bene, è ora di andare!"


translate italian un_mine_exit_6b46b235:


    "Lena si è girata rapidamente e si è incamminata in direzione della sua casetta."


translate italian un_mine_exit_692879ab:


    "Improvvisamente ho avuto la sensazione che ci fosse qualcosa che non andava, qualcosa di strano."


translate italian un_mine_exit_3b0e393f:


    th "Dopo tutto quello che ci è successo, se ne va con un semplice «bene, è ora di andare?»"


translate italian un_mine_exit_864250dc:


    th "Solitamente finisce in altro modo in questi casi, non è vero?"


translate italian un_mine_exit_1a76d291:


    "Anche se non so esattamente cosa mi aspettassi da lei…"


translate italian un_mine_exit_90db22de:


    "La stanchezza mi ha colpito nuovamente. Camminando con fatica, sono entrato nella stanza."


translate italian un_mine_exit_9dc23163:


    "La leader era seduta sul mio letto..."


translate italian un_mine_exit_65d8d90c:


    mt "Semyon..."


translate italian un_mine_exit_5c8a950f:


    "Ha esordito in tono triste."


translate italian un_mine_exit_3300a99a:


    mt "Dove sei stato?"


translate italian un_mine_exit_4e8fd4d6:


    me "Beh... io..."


translate italian un_mine_exit_18329aed:


    "Quella sua reazione mi ha sorpreso, mi ero già preparato per una lavata di capo."


translate italian un_mine_exit_8644ddd6:


    me "Sono andato a cercare Shurik."


translate italian un_mine_exit_b166a941:


    mt "Da solo?"


translate italian un_mine_exit_57cf671c:


    me "No, con Lena."


translate italian un_mine_exit_46b3ba5a:


    me "Sì, da solo."


translate italian un_mine_exit_49d63af4:


    mt "E quindi...{w} Che mi dici di Shurik?"


translate italian un_mine_exit_786bc7ef:


    "Sembrava che Olga Dmitrievna fosse davvero preoccupata per la sorte del pioniere scomparso."


translate italian un_mine_exit_c8bc02e9:


    th "Cosa c'è di così strano?"


translate italian un_mine_exit_b9bcf031:


    th "La leader può anche trattare il suo lavoro con leggerezza, ma ciò non significa che debba per forza essere una persona senza cuore!"


translate italian un_mine_exit_111b3046:


    me "Sta bene...{w} Potrebbe stare peggio, in una situazione del genere."


translate italian un_mine_exit_564980ed:


    mt "Sono contenta.{w} Adesso va' a dormire."


translate italian un_mine_exit_47b3e3c3:


    me "Aspetti, ma..."


translate italian un_mine_exit_29dbd358:


    "Ha spento rapidamente la luce, e ciò significava che la nostra conversazione era terminata, e non ho potuto chiederle altro."


translate italian un_mine_exit_9a3906dd:


    th "Anche se, cosa le potrei chiedere?"


translate italian un_mine_exit_f504d3d8:


    th "Del perché non mi abbia rimproverato?"


translate italian un_mine_exit_a20cefa7:


    "..."


translate italian un_mine_exit_a129a5fd:


    th "Eppure, Lena…"


translate italian un_mine_exit_c654370d:


    th "Cosa è successo oggi?"


translate italian un_mine_exit_c01dd812:


    "I miei pensieri hanno rallentato il passo, prima di fermarsi del tutto…"
# Decompiled by unrpyc: https://github.com/CensoredUsername/unrpyc
