
translate italian epilogue_uv_7e594d31:


    "È interessante come degli eventi in apparenza del tutto semplici possano influenzare una persona."


translate italian epilogue_uv_07e4edc8:


    "Ad esempio, ci sono molti casi di DOC (disturbo ossessivo-compulsivo) tra i bambini, che quando camminano per strada devono per forza mettere i piedi sulle piastrelle, evitando le crepe."


translate italian epilogue_uv_466ed2a9:


    "Non sembra niente di speciale, ma se non si riesce a sbarazzarsene in tempo – semplicemente dimenticandosene, o decidendosi a calpestare le crepe – potrebbe diventare davvero un grosso problema in futuro.{w} Ad esempio ci si potrebbe abituare a guardare per terra mentre si cammina, finendo sotto a una macchina…"


translate italian epilogue_uv_a2969cc6:


    "Io non ho mai avuto alcun disturbo ossessivo-compulsivo, ma eventi di poca rilevanza mi causavano spesso gravi problemi."


translate italian epilogue_uv_da2ea9fd:


    "In realtà, cosa c'è di sbagliato nell'incontrare una strana ragazza con le orecchie di gatto in un campo di pionieri ai confini del tempo e dello spazio, o forse persino tra mondi diversi…?"


translate italian epilogue_uv_a337cb21:


    "Non sembra niente di speciale."


translate italian epilogue_uv_9c59ff6c:


    "Forse mi sono abituato all'idea che la mia vita precedente non esistesse affatto. C'era solo questo Campo e questa vita."


translate italian epilogue_uv_9a051ead:


    "Ecco perché una specie di ragazza-gatto era un semplice dettaglio in questo paese delle meraviglie."


translate italian epilogue_uv_a79e039c:


    "È solo un altro personaggio in questo gioco, né migliore né peggiore di qualunque altro."


translate italian epilogue_uv_9dda5d32:


    "Ma da qualche parte nel profondo della mia anima sapevo che non era così."


translate italian epilogue_uv_389909c0:


    "Tutti gli abitanti del Campo sembravano normali, a volte anche troppo normali, senza nulla di straordinario."


translate italian epilogue_uv_f8f578c5:


    "Accidenti!{w} Si potrebbe persino dire che la mia vita qui fosse noiosa."


translate italian epilogue_uv_581326c5:


    "E adesso…"


translate italian epilogue_uv_2486bb90:


    "La mia testa era sul punto di esplodere a causa dei pensieri, così ho aperto gli occhi e sono saltato giù dal letto."


translate italian epilogue_uv_33e7bf55:


    "La stanza della leader del Campo era la stessa di sempre."


translate italian epilogue_uv_e0cf3746:


    "Fuori dalla finestra un uccellino cinguettava monotonamente, cercando di far scoppiare sistematicamente i capillari nei miei occhi con il suo canto."


translate italian epilogue_uv_13ad0098:


    "L'orologio mostrava le undici di mattina."


translate italian epilogue_uv_82f332bb:


    "Perché Olga Dmitrievna non mi ha svegliato?{w} Ma che importa…"


translate italian epilogue_uv_2677a9c2:


    "Almeno sono riuscito a dormire un po' di più."


translate italian epilogue_uv_10e06630:


    "Anche se non mi è sembrato comunque abbastanza."


translate italian epilogue_uv_bc948b8d:


    "Era come se il mondo intero stesse ridendo di me – il sole luminoso, la brezza leggera che soffiava, che sussurrava delicatamente qualcosa alle chiome degli alberi, e quel cattivo uccellino che si era portato appresso alcuni compagni, ovviamente con l'intento di rovinare la mia stabilità mentale."


translate italian epilogue_uv_7c50b983:


    "L'acqua dei lavabi era gelata come al solito."


translate italian epilogue_uv_bc68213e:


    "Mentre mi stavo lavando la faccia mi sono reso conto di aver dimenticato nella stanza lo spazzolino da denti, il dentifricio in polvere e l'asciugamano…"


translate italian epilogue_uv_99507a71:


    "Strano che io non avessi dimenticato anche la mia testa…"


translate italian epilogue_uv_252255bb:


    "Non riuscivo ad immaginare un peggiore inizio di giornata."


translate italian epilogue_uv_4e71467e:


    "In quel momento mi sembrava che fosse il giorno peggiore tra tutti quelli che avevo trascorso in questo Campo."


translate italian epilogue_uv_6808eaed:


    "Anche se non c'erano molte possibilità: poco più di un centinaio di ore, certamente non erano abbastanza per abituarmi all'idea di una nuova casa."


translate italian epilogue_uv_65142f6d:


    "Casa…{w} Quella parola mi ha colpito dolorosamente."


translate italian epilogue_uv_2899cae6:


    "Dovrò restare qui per sempre?!{w} Ma perché, come, a quale scopo...?"


translate italian epilogue_uv_cb64a07a:


    "Mi sono seduto impotente sull'erba e ho sepolto il viso tra le mani."


translate italian epilogue_uv_0b1b3e6f:


    "All'improvviso mi è tornata in mente quella ragazza.{w} Chissà quali altri personaggi fantastici incontrerò qui."


translate italian epilogue_uv_5f2574e3:


    pi "Una splendida giornata, non trovi?"


translate italian epilogue_uv_96275f4a:


    "Non mi sono nemmeno scomodato ad alzare la testa."


translate italian epilogue_uv_197a972e:


    me "Già, non potrebbe andare meglio di così…"


translate italian epilogue_uv_e290bdf2:


    "Ho risposto, rimanendo nella stessa posizione."


translate italian epilogue_uv_94fa158a:


    pi "Perché così sarcastico?"


translate italian epilogue_uv_0f3afb25:


    me "Sarcastico?"


translate italian epilogue_uv_fd41eafe:


    "Sono rimasto sorpreso e ho alzato lo sguardo, ma non c'era nessuno nelle vicinanze."


translate italian epilogue_uv_f4ec4bfa:


    "Forse me lo sono immaginato?{w} No, qualcuno ha parlato, ne sono sicuro…"


translate italian epilogue_uv_e99cb8c8:


    "O forse sto impazzendo?"


translate italian epilogue_uv_0ec9dbdb:


    "Quella miniera sotterranea, Shurik, le voci, la ragazza-gatto, altre voci…{w} C'è qualcosa qui di cui potrei avere la certezza?!"


translate italian epilogue_uv_c66393c3:


    "Ho colpito il lavabo duramente con un pugno, così forte che ho sobbalzato per il dolore, massaggiandomi la mano e imprecando."


translate italian epilogue_uv_43ab9e79:


    me "Dannazione!"


translate italian epilogue_uv_ad63e7a6:


    "Improvvisamente ho sentito qualcuno fuoriuscire dai cespugli di fronte a me.{w} O qualcosa…"


translate italian epilogue_uv_bd8c5549:


    uvp "Ciao!"


translate italian epilogue_uv_cf471270:


    "Era la ragazza di ieri."


translate italian epilogue_uv_401e4c6c:


    me "Ciao…"


translate italian epilogue_uv_faae32c5:


    "Non sapevo cosa dire."


translate italian epilogue_uv_a12cedc0:


    th "Non so proprio cosa aspettarmi da lei."


translate italian epilogue_uv_206da196:


    "Probabilmente è una bestia infernale che è venuta qui per porre fine alla mia esistenza.{w} Ovviamente se non era già finita da un pezzo…"


translate italian epilogue_uv_1f57fbc9:


    "La ragazza-gatto mi ha fissato."


translate italian epilogue_uv_1330e6b3:


    uvp "Fa male?"


translate italian epilogue_uv_59cc3474:


    "Ha guardato la mia mano."


translate italian epilogue_uv_9513cd87:


    me "Sì…"


translate italian epilogue_uv_87af141e:


    "Ho risposto timidamente."


translate italian epilogue_uv_2fea0ff6:


    uvp "Allora perché?"


translate italian epilogue_uv_038a9e0d:


    me "Perché cosa?"


translate italian epilogue_uv_09987e32:


    uvp "Perché l'hai colpito?"


translate italian epilogue_uv_45f8c00f:


    me "Perché ero arrabbiato…"


translate italian epilogue_uv_f6c23508:


    "Non avevo la minima idea di come dovessi parlare con lei."


translate italian epilogue_uv_812a95dc:


    "Anche solo una parola, una frase, o un gesto sbagliato potevano essermi fatali."


translate italian epilogue_uv_d817b84a:


    uvp "Arrabbiato con chi?"


translate italian epilogue_uv_550c69d7:


    "Ma la ragazza non sembrava mostrare alcun segno di ostilità – sembrava solo che volesse sapere cosa stava accadendo."


translate italian epilogue_uv_bd67e015:


    me "Con me stesso."


translate italian epilogue_uv_39c66d97:


    uvp "Capisco."


translate italian epilogue_uv_783cf09d:


    "Ha sorriso debolmente."


translate italian epilogue_uv_4d984aa7:


    me "Senti…{w} Non so come dirtelo, ma te lo devo chiedere…"


translate italian epilogue_uv_98d00e80:


    "Sono rimasto in silenzio per un attimo, prendendo coraggio."


translate italian epilogue_uv_99970e7f:


    me "Tu chi sei...?"


translate italian epilogue_uv_72e1758a:


    "La ragazza mi ha guardato senza capire."


translate italian epilogue_uv_606596a1:


    uvp "Io sono io, ovviamente!"


translate italian epilogue_uv_a9a44f6b:


    "Ha detto con fiducia."


translate italian epilogue_uv_a343b282:


    me "No, non ne dubito, ma…{w} Come potrei dire…?{w} Non si incontra molto spesso un essere umano dotato di orecchie e coda, sai."


translate italian epilogue_uv_3cea47dd:


    "La ragazza-gatto ha esaminato attentamente le parti da me menzionate e poi mi ha chiesto:"


translate italian epilogue_uv_41fa5d91:


    uvp "Perché?"


translate italian epilogue_uv_7a2286cb:


    "Ero del tutto confuso."


translate italian epilogue_uv_ed40cefe:


    "Se nei primi minuti ho pensato che fosse necessario avere paura, che sarei dovuto stare molto attento, ora sembrava essere più uno spirito benevolo che un mostro pericoloso."


translate italian epilogue_uv_6ed8834b:


    me "Perché nel mio mondo…"


translate italian epilogue_uv_cf33afc2:


    "Ero assolutamente convinto del fatto che non dovessi nasconderle niente."


translate italian epilogue_uv_60290bff:


    me "Nel mio mondo, questo accade solo nelle favole."


translate italian epilogue_uv_1911cc51:


    uvp "Cos'è una favola?"


translate italian epilogue_uv_acab5d31:


    me "Beh…{w} È una storia che non può accadere nella realtà."


translate italian epilogue_uv_822aded0:


    uvp "Quindi io in realtà non esisto?"


translate italian epilogue_uv_63d3671f:


    "La ragazza si è messa a ridere."


translate italian epilogue_uv_c54c6597:


    me "Non sono nemmeno sicuro che {i}io{/i} esista…"


translate italian epilogue_uv_f0d6ba43:


    uvp "Ma io posso toccarti – quindi esisti."


translate italian epilogue_uv_fd243037:


    "Mi sono sentito a disagio al solo pensiero che lei potesse toccarmi."


translate italian epilogue_uv_f31233fd:


    me "Beh, allora diciamo che esisto, sì…"


translate italian epilogue_uv_d7fa0877:


    "Ho indietreggiato."


translate italian epilogue_uv_a630f25b:


    uvp "Santarellino!"


translate italian epilogue_uv_c453d5a6:


    "La ragazza ha sorriso, agitando la coda."


translate italian epilogue_uv_3baee6cb:


    "È seguito un silenzio imbarazzante."


translate italian epilogue_uv_fb3cf2d4:


    "Durante tutto il tempo che ho trascorso in questo Campo, ho vissuto come in un sogno."


translate italian epilogue_uv_6f42630e:


    "A volte quando si dorme ci si rende conto che tutto ciò che accade nel cervello è solo frutto della propria immaginazione, e bisogna solo desiderare…"


translate italian epilogue_uv_d5887cda:


    "La pensavo così negli ultimi cinque giorni, era una convinzione che risiedeva da qualche parte nel profondo della mia anima."


translate italian epilogue_uv_3eed1590:


    "Anche se tutto mi sembrava come se stesse accadendo nella realtà.{w} O almeno, non riuscivo proprio a svegliarmi…"


translate italian epilogue_uv_c4904eec:


    me "Voglio solo capire…"


translate italian epilogue_uv_ad2b8ad1:


    uvp "Che cosa?"


translate italian epilogue_uv_6842d279:


    "Mi ha chiesto, continuando a sorridere."


translate italian epilogue_uv_913229f6:


    me "Come sono arrivato qui…{w} E come uscire di qui."


translate italian epilogue_uv_ab49bee4:


    "All'improvviso ho sentito delle voci provenire dal sentiero, e mi sono voltato."


translate italian epilogue_uv_efa22042:


    "Slavya e Zhenya si stavano avvicinando lentamente verso i lavabi."


translate italian epilogue_uv_bd0cee6c:


    me "E tu…"


translate italian epilogue_uv_8391e4f1:


    "Ma la ragazza era scomparsa."


translate italian epilogue_uv_84dc2808:


    me "L'avete vista, l'avete vista?!"


translate italian epilogue_uv_d436b474:


    "Come se avessi avuto bisogno di qualcuno che mi confermasse l'esistenza di quella creatura.{w} Per poter avere la certezza di non essere impazzito."


translate italian epilogue_uv_e48273b0:


    "Che ironia – impazzire mentre si è nel Paese di Oz…{w} Anche se il solo arrivare in questo luogo era già di per sé un biglietto di sola andata per il manicomio."


translate italian epilogue_uv_fd3163fc:


    "E anche se mi svegliassi proprio ora, non potrei avere alcuna certezza che si tratti di un sogno."


translate italian epilogue_uv_bf07f552:


    sl "Visto chi?"


translate italian epilogue_uv_fcc1f8e9:


    "Le ragazze mi hanno guardato senza capire."


translate italian epilogue_uv_aeb84719:


    me "Qualcuno vicino a me…"


translate italian epilogue_uv_4569b623:


    "Riuscivo a malapena a parlare."


translate italian epilogue_uv_300fbab5:


    "Dunque si è trattato di un'illusione, eh?{w} Quindi io sono davvero…"


translate italian epilogue_uv_d9eb3a0e:


    mz "Dovresti farti visitare da uno bravo, sul serio!"


translate italian epilogue_uv_8c01a57f:


    "Ha sbuffato Zhenya, passando davanti a me.{w} Slavya ha sorriso in segno di scusa e si è precipitata dietro di lei."


translate italian epilogue_uv_bda4e78f:


    "Quindi è tutto così semplice...?"


translate italian epilogue_uv_34121b4d:


    "Avevo bisogno solo di una cosa in quel momento – che qualcuno in questo incubo potesse credere che io fossi ancora sano di mente!"


translate italian epilogue_uv_965e44b3:


    "L'intero Campo e i pionieri non mi sembravano così reali come il giorno precedente."


translate italian epilogue_uv_cf1121e2:


    "Dopotutto, solo fino a una settimana fa la mia vita era del tutto normale, come quella di una tipica persona del suo tempo, ma adesso…"


translate italian epilogue_uv_cd153fa8:


    "Forse tutto quello che è successo mi ha fatto pensare che tutto stesse andando come avrebbe dovuto?{w} Che fosse normale – essere, esistere qui."


translate italian epilogue_uv_baf7b7ad:


    "E ora gli ultimi avanzi del mio buonsenso stavano cercando disperatamente di aggrapparsi a quella ragazza felina con le orecchie di animale, cercando di dimostrare alla gente locale che io non fossi un pazzo."


translate italian epilogue_uv_2c9d53b3:


    "Visto – tu non esisti, proprio tu!{w} Sono io ad essere nel tuo racconto, non tu nel mio…"


translate italian epilogue_uv_0ef30d5d:


    "La mia testa stava per esplodere."


translate italian epilogue_uv_9ee7c0ba:


    "Se non fosse stato per la sirena del pranzo, forse mi sarei precipitato in biblioteca, in cerca di libri di medicina con istruzioni per la lobotomia."


translate italian epilogue_uv_9cefc21f:


    "La mensa era affollata come al solito.{w} Alcune cose non cambiano mai."


translate italian epilogue_uv_aea05e6b:


    "Per la prima volta ho guardato tutti i pionieri locali da un'angolazione leggermente diversa."


translate italian epilogue_uv_8946754d:


    "In effetti, perché è tutto così…{w} impeccabile?"


translate italian epilogue_uv_aaec030d:


    "Come se qualcuno avesse creato questo Campo di pionieri raccogliendo accuratamente il materiale da internet, senza preoccuparsi di visitare di persona un luogo del genere."


translate italian epilogue_uv_824ea295:


    "Era proprio come un vecchio film sovietico – la severa leader del Campo, i pionieri modello.{w} Persino Ulyana.{w} E persino Alisa…"


translate italian epilogue_uv_9b5ba514:


    "Tutti entro i limiti della normalità."


translate italian epilogue_uv_fd4fc82b:


    "E allora da dov'è sbucata quella strana ragazza?"


translate italian epilogue_uv_8c4c3dff:


    "Sembrava come una macchia di vernice che si trovava sul dipinto solo perché a uno sciatto restauratore tremavano le mani."


translate italian epilogue_uv_a17c46ba:


    "Improvvisamente Electronik si è seduto accanto a me."


translate italian epilogue_uv_1acf0704:


    el "Ehilà!"


translate italian epilogue_uv_ede75644:


    "Ha sorriso."


translate italian epilogue_uv_401e4c6c_1:


    me "Ehi…"


translate italian epilogue_uv_0f55d42c:


    el "A cosa stai pensando?"


translate italian epilogue_uv_9553ce6a:


    me "Così…"


translate italian epilogue_uv_af68277f:


    "Non c'era assolutamente niente di sbagliato nel parlare apertamente con chiunque.{w} Non più…"


translate italian epilogue_uv_97647879:


    me "Hai per caso visto qualcuno di strano in giro?"


translate italian epilogue_uv_f20e3f00:


    el "Strano?"


translate italian epilogue_uv_ed10791f:


    me "Beh…{w} Non del tutto umano."


translate italian epilogue_uv_5aa34a60:


    el "Non ti capisco."


translate italian epilogue_uv_765c7d3f:


    "Electronik ha fatto una risata."


translate italian epilogue_uv_4594de55:


    me "Una ragazza con le orecchie di gatto e una coda."


translate italian epilogue_uv_8d388b1a:


    "Ho detto concisamente."


translate italian epilogue_uv_552282c3:


    el "Una ragazza con le orecchie e la coda...?"


translate italian epilogue_uv_ba9b1a5b:


    "Ha pensato un attimo."


translate italian epilogue_uv_50507c2c:


    el "Come fai a sapere di lei?"


translate italian epilogue_uv_4925911e:


    me "Beh, supponiamo che io l'abbia vista."


translate italian epilogue_uv_d7d4400d:


    "La conversazione si stava facendo interessante."


translate italian epilogue_uv_ed9cb410:


    el "«Supponiamo», o l'hai vista davvero?"


translate italian epilogue_uv_9b84c34b:


    me "Che differenza fa?"


translate italian epilogue_uv_3f421fba:


    "Electronik mi ha fissato con sguardo serio."


translate italian epilogue_uv_d8483462:


    el "C'è una leggenda riguardante una ragazza-gatto. Dice che lei è solita rubare il cibo e piccoli oggetti, evitando le persone."


translate italian epilogue_uv_b9b6131d:


    me "Davvero?{w} Una leggenda?"


translate italian epilogue_uv_34955a96:


    me "E a chi assomiglia questa ragazza?"


translate italian epilogue_uv_abf7efe2:


    el "Io non l'ho mai vista.{w} Si dice che abbia una coda e delle orecchie, come l'hai descritta tu."


translate italian epilogue_uv_5587f86a:


    me "C'è altro?"


translate italian epilogue_uv_537d8131:


    el "Beh, questa leggenda è antica, non ne so molto."


translate italian epilogue_uv_c93b36d7:


    me "E chi credi possa saperne di più?"


translate italian epilogue_uv_f4cf968f:


    el "Chiedi a Olga Dmitrievna – lei è stata in questo Campo molte volte, ancora prima di diventarne la leader."


translate italian epilogue_uv_3118d7bc:


    me "Va bene, grazie."


translate italian epilogue_uv_e316c160:


    "Mi sono alzato e sono andato a cercare la leader del Campo."


translate italian epilogue_uv_e6413ca6:


    "Scommetto che si sta facendo un sonnellino dopo pranzo."


translate italian epilogue_uv_03870281:


    "Forse Olga Dmitrievna era l'unico elemento ad essere un po' fuori luogo nel quadro di un campo ideale – di certo non era una leader «modello»."


translate italian epilogue_uv_8014570c:


    "Ho aperto la porta e sono entrato con decisione."


translate italian epilogue_uv_b95f1f60:


    "E avevo ragione, si trovava proprio sul letto e stava leggendo un libro, come previsto."


translate italian epilogue_uv_743fc179:


    mt "Semyon… Stai battendo di nuovo la fiacca?"


translate italian epilogue_uv_22f38a3f:


    me "In realtà, sì, ma comunque.{w} Volevo chiederle una cosa."


translate italian epilogue_uv_8822fa3d:


    "Mi ha fissato, e io ho esitato un po'."


translate italian epilogue_uv_79e62762:


    "Forse non dovrei essere così diretto con lei?"


translate italian epilogue_uv_c6104e29:


    "Era come se il mio firewall interno si fosse attivato, e non riuscivo a dire alla leader del Campo che oggi avevo visto la ragazza-gatto."


translate italian epilogue_uv_f6bdf29b:


    "Forse mi ero davvero abituato ad essere vago, evitando le domande dirette, mentendo e trattenendo le parole."


translate italian epilogue_uv_ba8beeb1:


    "In soli quattro giorni!{w} Anche se immagino che fosse una mia abitudine anche nella vita precedente…"


translate italian epilogue_uv_c5d9d3db:


    me "Olga Dmitrievna, ero a pranzo…{w} Ho parlato con Electronik.{w} Mi ha detto di una leggenda su una ragazza-gatto, che presumibilmente vive qui."


translate italian epilogue_uv_a59cb894:


    me "Mi ha anche detto che probabilmente lei sa qualcosa in più a riguardo…"


translate italian epilogue_uv_1c523a1a:


    mt "Perché ti interessa?"


translate italian epilogue_uv_a1ac3a05:


    "La leader del Campo mi ha lanciato uno sguardo penetrante e si è alzata in piedi."


translate italian epilogue_uv_048f18e0:


    me "Sono solo curioso!{w} Mi piacciono queste cose!"


translate italian epilogue_uv_90c72986:


    "Ho cercato di sorridere, ma non ha migliorato le cose."


translate italian epilogue_uv_c224c61a:


    mt "Questo è un campo del tutto normale."


translate italian epilogue_uv_f5092004:


    "Come no!{w} Del tutto normale!{w} Allora non voglio nemmeno sapere cosa significhi «insolito» – suppongo sia qualcosa con zombie e maniaci omicidi!"


translate italian epilogue_uv_02e02e2f:


    "«Nightmare – Dal profondo di Sovyonok»…"


translate italian epilogue_uv_50ca27d3:


    me "Sì, capisco, ma…{w} Electronik mi ha detto che è da molto tempo che lei è in questo Campo."


translate italian epilogue_uv_0e31da79:


    mt "Sì…"


translate italian epilogue_uv_d4764288:


    "La leader del Campo stava pensando a qualcosa."


translate italian epilogue_uv_d290e285:


    mt "Se proprio ti interessa, esiste davvero una leggenda.{w} Dice che da qualche parte nei boschi si aggiri una strana ragazza-gatto, e che a volte ruba il cibo e piccoli oggetti dalle casette. Ha paura della gente, e se qualcuno la vede accidentalmente, allora lei fugge."


translate italian epilogue_uv_388bac12:


    "Era piuttosto diverso da quello che avevo visto io stamattina."


translate italian epilogue_uv_633cf645:


    "Quella ragazza sembrava fosse interessata a me, mi studiava, cercava di capire, ma non aveva affatto paura."


translate italian epilogue_uv_9696cc5e:


    me "E lei..."


translate italian epilogue_uv_4d18c559:


    mt "Se l'ho mai vista?"


translate italian epilogue_uv_bc6747d3:


    "Olga Dmitrievna mi ha interrotto."


translate italian epilogue_uv_df6f468a:


    mt "No, non l'ho vista, ma ero sul punto di riuscire a farlo in passato, più di una volta.{w} Bottiglie, latte, dentifricio in polvere, e un sacco di altre cose a volte sparivano dalla mia stanza.{w} Erano scomparse misteriosamente senza lasciare alcuna traccia."


translate italian epilogue_uv_91589aaf:


    "Avevo completamente dimenticato il fatto che in realtà tutti questi pionieri non esistono, e mi sono limitato ad ascoltare attentamente la leader del Campo."


translate italian epilogue_uv_1d9fdb2a:


    mt "Ero curiosa di sapere dove fossero sparite quelle cose.{w} Ma alla fine non c'è alcuna prova della sua esistenza…"


translate italian epilogue_uv_b102f359:


    "Ha sorriso, come ad indicare che la nostra conversazione era giunta al suo termine."


translate italian epilogue_uv_26a5c68f:


    me "E se esistesse davvero?"


translate italian epilogue_uv_13679a66:


    mt "Perché sei così interessato a questa storia?"


translate italian epilogue_uv_149896fd:


    me "E chi non lo sarebbe…?"


translate italian epilogue_uv_3c4868cf:


    "Anche se avevo ancora molte altre cose a cui pensare."


translate italian epilogue_uv_2c3aefa4:


    mt "Non lo so…"


translate italian epilogue_uv_983cc84e:


    "La leader del Campo è rimasta pensierosa per qualche secondo, e poi è tornata alla sua solita espressione di {i}Madre-di-tutti-i-campi{/i}."


translate italian epilogue_uv_4b9bfbfc:


    mt "Beh, sembra proprio che tu abbia deciso di rilassarti alla grande!{w} Fa' qualcosa di utile…"


translate italian epilogue_uv_76e6e018:


    "Ha fatto una pausa e si è stirata in un modo tale che ha fatto apparire delle rughe che alla sua età non dovrebbero nemmeno esistere."


translate italian epilogue_uv_6d49ea8f:


    mt "Forza… Mettiti al lavoro!"


translate italian epilogue_uv_31709fb0:


    "Ho sospirato e sono uscito."


translate italian epilogue_uv_7ed6de8c:


    "Tutto sommato, nessuno dei pionieri aveva mai visto una ragazza con le orecchie di gatto, avevano solo sentito delle «leggende»."


translate italian epilogue_uv_4dbdaf04:


    "Beh, forse dovrei cercarla!"


translate italian epilogue_uv_2dc25f6f:


    "Mi sono già ripreso dagli eventi di questa mattina, e i miei pensieri esistenziali avevano ormai smesso di fare pressione sul mio cervello, quindi riuscivo a vedere tutto con raziocinio, più o meno."


translate italian epilogue_uv_d50f0920:


    "Era necessario perché avevo ancora un sacco di domande: come sono finito in questo luogo? Per quale ragione? Perché? E dov'era questo «qui»?"


translate italian epilogue_uv_4c7c2569:


    "E la ragazza-gatto era il catalizzatore di tutto."


translate italian epilogue_uv_72f94960:


    "Ma non volevo iniziare a inventarmi nuovamente delle teorie su mondi paralleli, esperimenti alieni, o sull'aldilà."


translate italian epilogue_uv_f5c40cb4:


    "Mi avevano stancato già dalle prime ore in questo Campo."


translate italian epilogue_uv_213d5389:


    "Forse ho solo bisogno di mettere in disparte quelle teorie e guardare in faccia la realtà."


translate italian epilogue_uv_1c978c35:


    "Anche se questo mondo esistesse indipendentemente dalla mia volontà e io dovessi rimanere qui per sempre, il come io ci sia arrivato resta comunque senza alcuna risposta.{w} È più utile concentrarsi sullo studiare attentamente questo mondo e analizzare i possibili eventi che potrebbero capitarmi."


translate italian epilogue_uv_1c050fb2:


    "Lo farò fino a prova contraria. Fino ad allora devo supporre di essere bloccato qui.{w} Per molto tempo, o magari anche per l'eternità."


translate italian epilogue_uv_385b5708:


    "La sedia a sdraio cigolava melodiosamente a ogni mio movimento, i rami di lillà cercavano di schiaffeggiarmi, e i vili uccellini mattutini se ne stavano in silenzio per lungo tempo, apparentemente stanchi di tutto il caldo estivo."


translate italian epilogue_uv_a3843ea8:


    "Le cime degli alberi sembravano le punte taglienti delle alte mura di un castello medievale, sperduto nei boschi sconfinati."


translate italian epilogue_uv_6f43b891:


    "E queste casette, la piazza, il monumento di Genda, l'edificio della sede dei circoli, tutto era un piccolo villaggio di contadini, racchiuso tra le mura millenarie di una tetra fortezza, nascosta in un boschetto."


translate italian epilogue_uv_641e9777:


    "E noi tutti qui – che fossero persone come me o ragazze-gatto – non eravamo altro che i servi di un genio del male che si nascondeva da qualche parte là fuori…"


translate italian epilogue_uv_cab4416d:


    "Sarebbe tutto più facile se il Campo si trovasse in cima ad una collina."


translate italian epilogue_uv_6da72447:


    "Perché migliaia di anni fa il posto migliore per una fortezza era sempre da qualche parte in alto."


translate italian epilogue_uv_17159f54:


    "Probabilmente quel {i}qualcuno{/i} non ha nulla da temere.{w} Ma in tal caso non se ne starebbe così ben nascosto dietro alle cime degli alberi..."


translate italian epilogue_uv_c913f529:


    "Ho sospirato e ho chiuso gli occhi."


translate italian epilogue_uv_01d78855:


    "La mia consapevolezza ha cominciato ad abbandonarmi lentamente, e ben presto il mondo intero è sbiadito nelle tenebre."


translate italian epilogue_uv_ce617998:


    "…"


translate italian epilogue_uv_1380294c:


    "E ancora una volta, dinanzi ai miei occhi, come un tappeto senza fine – la strada, circondata da boschi e campi, si estende all'infinito."


translate italian epilogue_uv_6ebd931c:


    "Il bus sta andando da qualche parte, lasciando dietro a sé tutto quello che conoscevo, che ho amato e odiato."


translate italian epilogue_uv_a25e5688:


    "Una ragazza misteriosa è china su di me e mi sta sussurrando dolcemente qualcosa all'orecchio."


translate italian epilogue_uv_aa083268:


    "Sono davvero interessato alle sue parole."


translate italian epilogue_uv_7e91da95:


    "Ma non riesco a sentirla – riesco a vedere tutto in modo parziale, vedo tutto tranne il suo viso."


translate italian epilogue_uv_a12c5467:


    "E sento tutto tranne la sua voce."


translate italian epilogue_uv_064711df:


    "E percepisco ogni cosa, tranne la mia vita…"


translate italian epilogue_uv_e65ed0cf:


    "Mi sono svegliato sentendomi soffocare e ho cominciato a inalare avidamente grosse boccate d'aria."


translate italian epilogue_uv_c51374b9:


    "Ben presto il mio respiro si è ristabilizzato, e ho avuto una sete insopportabile."


translate italian epilogue_uv_9dcbb59a:


    "Ma... Che cos'era quel sogno?{w} E chi era quella ragazza?"


translate italian epilogue_uv_e8dd80d0:


    "Di nuovo queste domande…"


translate italian epilogue_uv_4dfb5dfb:


    "Forse sono già morto, e sono finito all'inferno?"


translate italian epilogue_uv_934cbd27:


    "Può un essere umano percepire che la sua vita è ormai finita e che può solo passare alla fase successiva dell'esistenza?"


translate italian epilogue_uv_8908c3e8:


    "La mia testa stava per esplodere a causa di tali pensieri cosmici."


translate italian epilogue_uv_f58c6165:


    "Che cosa ho fatto per meritarmi tutto questo?{w} Sono solo un tizio qualunque, non un filosofo, non un teologo, non uno scienziato."


translate italian epilogue_uv_197dea6a:


    "E io non voglio capire la sostanza delle cose, la ragione dell'universo.{w} Non sto chiedendo più di quanto basti per vivere la mia vita in pace, e poi assopirmi in un sonno eterno quando tutto sarà finito."


translate italian epilogue_uv_8d9ab812:


    "Ma invece no – devo restare intrappolato qui!"


translate italian epilogue_uv_666232c3:


    "Lacrime di frustrazione mi hanno inumidito gli occhi."


translate italian epilogue_uv_451a9255:


    "E perché dev'essere sempre così?!"


translate italian epilogue_uv_64cbaa90:


    "Ho cominciato a pensare che se mi fossi scervellato ancora un po', allora o avrei capito {i}l'essenza di tutte le cose{/i}, o sarei impazzito completamente."


translate italian epilogue_uv_2773c75d:


    "Alla fine è iniziato tutto da quando ho preso quel bus – sono salito, mi sono addormentato e…"


translate italian epilogue_uv_e44cd0ef:


    "C'era una ragazza.{w} Forse uno dei pionieri locali, forse anche quella dalle orecchie di gatto!"


translate italian epilogue_uv_9e1bfd29:


    "Beh, perché no?{w} Non posso escluderlo del tutto.{w} E così ora mi trovo qui…"


translate italian epilogue_uv_b758af67:


    "I miei pensieri si sono fermati. Le piante di salsola rotolavano avanti e indietro nel mio cervello deserto, rimbalzando sulle pareti della struttura che impediva alle mie orecchie di cadere."


translate italian epilogue_uv_833a0181:


    "E… questo è tutto."


translate italian epilogue_uv_31afad13:


    "Ora sono qui – all'inferno, in paradiso, nel purgatorio, o qualunque cosa esso sia.{w} Sembra che dovrò trascorrere l'eternità in questo luogo…"


translate italian epilogue_uv_d5cd5ce9:


    "Per qualche ragione mi sono fissato su quella teoria."


translate italian epilogue_uv_092fb80e:


    "È strano che io non abbia considerato il fatto che un'entità spirituale non dovrebbe essere dotata dei cinque sensi – la vista, l'udito, il tatto…"


translate italian epilogue_uv_c0c03087:


    "Tutto sommato, a prima vista non è cambiato un granché dentro di me, rispetto a una settimana fa.{w} Beh, a parte l'essere diventato più giovane."


translate italian epilogue_uv_2470ab86:


    "Allora cosa c'è di sbagliato in quello che mi è accaduto?"


translate italian epilogue_uv_e3c414c9:


    "Potrei persino considerare tutto questo come una seconda possibilità!"


translate italian epilogue_uv_7d31568f:


    "Alla fine, non dovrebbe essere così difficile trovare un posto di lavoro, affittare una stanza in un ostello…"


translate italian epilogue_uv_5e133ffb:


    "Adesso sono un diciassettenne – sono ancora in buona salute, il mio cervello è in uno stato eccellente, la mia energia è al suo apice.{w} Potrei provare a realizzare qualcosa…"


translate italian epilogue_uv_7c5d0d68:


    "Ma cosa mi ha impedito di farlo in passato?{w} Ero davvero così vecchio nella vita precedente?"


translate italian epilogue_uv_89b8aeef:


    "Mi inceppavo come un vecchio fucile, e senza un aiuto esterno non avrei mai potuto sfuggire a quel ciclo di introspezioni."


translate italian epilogue_uv_f9d80d04:


    "Fortunatamente Lena è apparsa accanto alla casetta della leader del Campo."


translate italian epilogue_uv_bd946546:


    un "Ciao."


translate italian epilogue_uv_dde1f147:


    "Ho alzato lo sguardo con riluttanza e ho guardato attraverso di lei per alcuni secondi."


translate italian epilogue_uv_401e4c6c_2:


    me "Ciao…"


translate italian epilogue_uv_805ea37c:


    un "Ehm… io…"


translate italian epilogue_uv_080288a3:


    "Si è fermata, ha distolto lo sguardo ed è arrossita."


translate italian epilogue_uv_6f6c98f9:


    un "Beh, non importa.{w} Posso sedermi qui con te?"


translate italian epilogue_uv_66a21e69:


    me "Certo."


translate italian epilogue_uv_d407d0dc:


    "Lena si è appollaiata sul bordo di una sedia a sdraio."


translate italian epilogue_uv_2cd0045b:


    un "A cosa stai pensando?"


translate italian epilogue_uv_62f30bbf:


    me "Sto cercando di capire le leggi dell'universo."


translate italian epilogue_uv_bb75283f:


    un "E come sta andando?"


translate italian epilogue_uv_b84d3377:


    "Ha chiesto con espressione seria."


translate italian epilogue_uv_73e269e8:


    me "Non troppo bene…{w} Probabilmente sono oltre la comprensione umana."


translate italian epilogue_uv_c520fb7c:


    un "Allora forse non dovresti pensarci?"


translate italian epilogue_uv_e6abf674:


    me "Ma che ci posso fare?{w} Sono costretto."


translate italian epilogue_uv_b672514d:


    un "Perché?"


translate italian epilogue_uv_2376d290:


    "Giusto – perché?"


translate italian epilogue_uv_f0d30338:


    "È davvero di così vitale importanza?"


translate italian epilogue_uv_c1b551f0:


    "In passato non mi turbavano questioni del genere, e riuscivo a convivere con certe cose."


translate italian epilogue_uv_fb3cd06a:


    "Adesso invece cos'è cambiato così drasticamente?{w} Se non ci pensassi neppure qui, dove mi trovo in condizioni abbastanza diverse, che male ci sarebbe?"


translate italian epilogue_uv_1f42124c:


    "Se proprio non ho scelta…"


translate italian epilogue_uv_8473c25d:


    me "Non lo so.{w} Pare che nulla dipenda da me."


translate italian epilogue_uv_4ac82be2:


    "Sembrava che Lena volesse dire qualcosa, ma non l'ha fatto."


translate italian epilogue_uv_701bd2e2:


    me "E tu probabilmente sei venuta da Olga Dmitrievna?"


translate italian epilogue_uv_f3db2cab:


    un "Sì…{w} Voglio dire… no.{w} Ma ad essere sincera…"


translate italian epilogue_uv_ad02f53a:


    me "Cosa?"


translate italian epilogue_uv_e3352c88:


    un "Certe volte mi sento allo stesso modo."


translate italian epilogue_uv_cb23542f:


    me "Cosa intendi?"


translate italian epilogue_uv_8c2a3988:


    un "Che niente dipende da me."


translate italian epilogue_uv_8f838576:


    "L'ho guardata attentamente."


translate italian epilogue_uv_dade88e8:


    "Era seduta tenendo lo sguardo basso, giocherellando nervosamente con una manciata di fiori di campo – erano gialli come i raggi ardenti del sole estivo."


translate italian epilogue_uv_e499d007:


    un "Beh, sai…{w} Questi fiori, per esempio – stavano crescendo in pace, e io invece li ho strappati da terra, e questo è tutto!"


translate italian epilogue_uv_845b4bf2:


    "Era letteralmente sul punto di scoppiare a piangere.{w} Non mi sembrava affatto la Lena che conoscevo."


translate italian epilogue_uv_a3df48c6:


    me "Beh, sì…{w} Ad essere sincero, non so cosa dirti."


translate italian epilogue_uv_e07cb63d:


    un "Anche per noi vale lo stesso – veniamo strappati via, e poi…"


translate italian epilogue_uv_54d3c161:


    "Si è alzata di scatto e si è precipitata verso la piazza, senza salutare."


translate italian epilogue_uv_8e47f89e:


    "Lena, come tutti gli altri pionieri, sembrava essere reale.{w} Non un frutto della mia fantasia, non una bambola, non un NPC."


translate italian epilogue_uv_7a418be8:


    "Qualche ora fa ero sicuro di essere l'unico individuo reale qui.{w} E probabilmente anche quella ragazza dalle orecchie di gatto."


translate italian epilogue_uv_b456282f:


    "Vorrei incontrarla di nuovo…"


translate italian epilogue_uv_0bcb04b7:


    "Mi trovo in uno stato simile alla privazione spirituale – tutti i miei sensi funzionano correttamente, ma ho ancora un tremendo bisogno di sapere perché io sia qui."


translate italian epilogue_uv_72ae7014:


    "Come se il bisogno di avere un'identità, di trovare il mio posto in questo mondo, di determinarne la natura, le leggi e i meccanismi, fossero diventati di vitale importanza, proprio come la necessità di cibo e riposo."


translate italian epilogue_uv_5c3cb8c8:


    "Dev'essere la peggiore punizione per un essere umano – avere il desiderio di capire qualcosa, ma non averne la capacità. Desiderare di cambiare qualcosa, ma non avere l'opportunità per farlo. Lottare per qualcosa, ma non essere in grado di farlo."


translate italian epilogue_uv_837013f3:


    "Non me lo sono mai chiesto seriamente prima d'ora – perché sono venuto al mondo, quanto tempo devo trascorrere in esso, cosa dovrei fare, e cosa succederà in futuro."


translate italian epilogue_uv_075f5465:


    "E adesso, dopo aver scoperto quest'altro mondo, non mi resta che ripensare a quelle domande."


translate italian epilogue_uv_40e0457e:


    "Suppongo che se fosse possibile fuggire da qui, allora tornerei alla mia vita tranquilla, dove non ho di questi problemi."


translate italian epilogue_uv_250cbd1f:


    "Ma non posso tornarci…"


translate italian epilogue_uv_4b055169:


    "È così difficile affrontare un compito che non può essere portato a termine."


translate italian epilogue_uv_2d594681:


    "Se avessi potuto scegliere il mio destino, allora non avrei mai scelto quel bus, questo campo, queste ragazze – sarei rimasto lì, nel mio appartamento, di fronte al monitor."


translate italian epilogue_uv_fbae2843:


    "Non si può domandare a una persona di fare qualcosa che non è in grado di fare."


translate italian epilogue_uv_b711afb2:


    "È meglio trascorrere il resto della propria vita a scavare in miniera piuttosto che cercare di capire qualcosa che proprio non riesce a entrarti nella testa!"


translate italian epilogue_uv_e0d958d5:


    "In fin dei conti non c'è altro modo."


translate italian epilogue_uv_b3b8f58b:


    "Naturalmente si potrebbe cercare di fare uno sforzo coraggioso, provando a non pensarci, accettando la realtà così com'è e iniziare una nuova vita."


translate italian epilogue_uv_785611e4:


    "Ma il negare tutto ciò che succede attorno a noi non fa parte della nostra natura."


translate italian epilogue_uv_16a336ea:


    "Se fossi accusato di un crimine che non ho mai commesso, naturalmente cercherei di oppormi per dimostrare la mia innocenza."


translate italian epilogue_uv_37f26571:


    "A nessuno verrebbe in mente di voler vivere in carcere piuttosto che nella propria casa, qualunque essa sia."


translate italian epilogue_uv_90c2fe99:


    "E adesso sono io colui che è stato gettato fuori dal proprio mondo, dentro a questa «prigione»."


translate italian epilogue_uv_4c3a0f63:


    "Anche se si tratta di una prigione senza limitazioni, senza strette pareti né sbarre sulle finestre, è comunque una prigione – non ho chiesto di venire qui, e il mio unico desiderio adesso è quello di ritornare a casa."


translate italian epilogue_uv_bb5f8ea5:


    "E l'unica chiave per la via d'uscita è appesa poco più in alto di quanto io possa raggiungere con un salto."


translate italian epilogue_uv_c0218e20:


    "E non c'è niente di utile attorno a me – nessuna sedia, nessuno sgabello, nemmeno una piccola roccia che potrei mettere sotto ai miei piedi.{w} E non c'è nessuno che mi possa aiutare."


translate italian epilogue_uv_d6449651:


    "Basterebbero pochi centimetri in più. Solo pochi centimetri mi separano dalla libertà, dal comprendere ogni cosa…"


translate italian epilogue_uv_5c3993ed:


    "Forse è impossibile per me tornare a casa, ma almeno sarei in grado di uscire da questa prigione!"


translate italian epilogue_uv_37c1ea83:


    "È esattamente quello che ho desiderato negli ultimi cinque giorni…"


translate italian epilogue_uv_a20cefa7:


    "..."


translate italian epilogue_uv_9565e04f:


    "La notte è scesa lentamente sul Campo."


translate italian epilogue_uv_cac0bbd5:


    "La cena ha avuto luogo senza di me, e la leader del Campo si è fermata più volte davanti alla sedia a sdraio, cercando di dirmi qualcosa."


translate italian epilogue_uv_2a186eff:


    "Ma io ero immerso in un altro mondo – il mondo della mia gabbia e della sua chiave, legata con un nodo gordiano al filo d'Arianna."


translate italian epilogue_uv_a308a72b:


    "Non è passato molto tempo prima che la luce nella casetta si spegnesse e i grilli iniziassero il loro deplorevole piagnisteo tutt'attorno."


translate italian epilogue_uv_49f57976:


    "Da qualche parte in lontananza una civetta stava fischiando ad una frequenza di trentasette fischi al minuto."


translate italian epilogue_uv_e5c6354e:


    "Me ne stavo seduto battendo i piedi ritmicamente, cercando di accompagnarla."


translate italian epilogue_uv_58b687db:


    "Improvvisamente ho sentito un fruscio tra i cespugli intorno a me, e uno scoiattolo con una ghianda tra i denti è salito su un albero.{w} Probabilmente si stava affrettando a raggiungere la sua famiglia…"


translate italian epilogue_uv_be1bbc12:


    "Il fascino della notte d'estate era ricco come sempre, ma non mi ha influenzato per niente."


translate italian epilogue_uv_9a927fac:


    "I meccanismi del mio cervello, e probabilmente anche della mia anima, avevano bisogno di manutenzione, non di un semplice lavoro frettoloso e di una verniciata fresca."


translate italian epilogue_uv_cf53bd0d:


    "Ho deciso di fare una passeggiata."


translate italian epilogue_uv_978811c4:


    "Perché a pensarci, sono rimasto seduto su quella sedia a sdraio per quasi tutto il giorno.{w} Ho persino fatto un pisolino…"


translate italian epilogue_uv_56500c49:


    "Ognuno nel Campo era ormai stanco. Tutti dormivano."


translate italian epilogue_uv_b459cef6:


    "La stradina tra le casette dei pionieri, la piazza con la statua di Genda, il sentiero della foresta, tutte queste cose si susseguivano come le illustrazioni di un libro per bambini.{w} Il mondo intero sembrava un dipinto."


translate italian epilogue_uv_bc0e1e5a:


    "E fino a ieri non la pensavo in questo modo…"


translate italian epilogue_uv_abe4b874:


    "E la ragione di tutto questo era quella strana ragazza con le orecchie di animale, una ragazza-gatto (anche se lei potrebbe benissimo essere un qualche altro animale)."


translate italian epilogue_uv_3e0e6364:


    "È come se si fosse attivato un meccanismo di auto-difesa da quando sono apparso qui, che non mi ha permesso di «cercare risposte» o di «agguantare la situazione»."


translate italian epilogue_uv_0b56ea8e:


    "C'erano sempre cose più importanti, come ad esempio marinare l'allineamento, trovare un buon posto alla mensa, o fuggire di nascosto dalla leader del Campo."


translate italian epilogue_uv_4dfa815d:


    "Ogni singola cosa a parte il pensare perché io sia finito qui, o come ne possa uscire.{w} Ogni cosa…"


translate italian epilogue_uv_1e2d6285:


    "Mi sono seduto su un ceppo e ho chiuso gli occhi."


translate italian epilogue_uv_d3f7944b:


    "Milioni di immagini hanno invaso la mia mente, e tra esse c'era una strada, un bus, e quella ragazza.{w} Sì, la ragazza con le orecchie da animale.{w} Era proprio lei la ragazza sull'autobus!"


translate italian epilogue_uv_3d57006a:


    "Ho aperto rapidamente gli occhi e me la sono trovata davanti."


translate italian epilogue_uv_30954eab:


    "All'inizio mi è sembrato come un sogno o un'allucinazione, ma una folata di vento freddo mi ha riportato subito alla realtà."


translate italian epilogue_uv_91d91e6b:


    "La strana ragazza se ne stava seduta a cospargere lo zucchero su un fungo."


translate italian epilogue_uv_51149e68:


    me "…"


translate italian epilogue_uv_08781329:


    uvp "Così crescono meglio."


translate italian epilogue_uv_b9daa03b:


    "Mi ha detto con espressione seria."


translate italian epilogue_uv_a444ac12:


    me "Chi?"


translate italian epilogue_uv_70580914:


    "Ho pronunciato con difficoltà."


translate italian epilogue_uv_d9e00995:


    uvp "I funghi, chi altro!"


translate italian epilogue_uv_7a3eab5d:


    me "I funghi...?"


translate italian epilogue_uv_246a2b06:


    uvp "Sì, i funghi."


translate italian epilogue_uv_2a236b27:


    me "A cosa ti servono quei funghi?"


translate italian epilogue_uv_de01302f:


    uvp "Per mangiarli, naturalmente!{w} Altrimenti perché avrei bisogno dei funghi?"


translate italian epilogue_uv_5942e96a:


    "Mi ha detto imbronciata."


translate italian epilogue_uv_8213e247:


    "Non sapevo proprio come iniziare una conversazione con lei, perché ero sicuro che la cosa che sembrava essere una strana ragazza con le orecchie da animale mi avesse portato qui, e fare amicizia con un essere superiore non sarebbe stata una buona cosa."


translate italian epilogue_uv_c5d78fa3:


    uvp "Per caso hai altro zucchero?"


translate italian epilogue_uv_58838469:


    "Mi ha chiesto all'improvviso."


translate italian epilogue_uv_941e5272:


    me "Non lo so… No.{w} Perché ti serve?"


translate italian epilogue_uv_3fe32671:


    uvp "Come «perché»?!{w} Te l'ho appena detto – per i funghi!{w} Così crescono meglio."


translate italian epilogue_uv_82ed9336:


    "Nonostante tutto, la ragazza sembrava perfettamente normale."


translate italian epilogue_uv_8261b96b:


    "Certamente non si vedeva spesso un essere umano con le orecchie e la coda di animale (e sembravano davvero reali), ma a parte questo…"


translate italian epilogue_uv_abb3995c:


    me "Te ne posso portare un po'…"


translate italian epilogue_uv_b283d28e:


    "Non so perché l'ho detto, forse volevo solo guadagnarmi la sua fiducia."


translate italian epilogue_uv_b18f4a9e:


    "Sempre che la lettura della mente non fosse una delle sue abilità."


translate italian epilogue_uv_cb5b23ec:


    uvp "Grazie, non ce n'è bisogno. Mi basterà."


translate italian epilogue_uv_65bf82fc:


    me "Bene…"


translate italian epilogue_uv_b4cd8a03:


    uvp "E?"


translate italian epilogue_uv_ad02f53a_1:


    me "Cosa?"


translate italian epilogue_uv_f81f2044:


    uvp "Perché sei venuto?"


translate italian epilogue_uv_a91b9930:


    "Che cosa intende dire?{w} Venuto in questa radura o venuto in questo Campo?{w} Oppure in questa vita…?"


translate italian epilogue_uv_1602edd7:


    "La mia testa stava per esplodere a causa dei pensieri esistenziali."


translate italian epilogue_uv_7b2d5c2d:


    "Ancora un piccolo sforzo…{w} e tutto sarebbe diventato cristallino."


translate italian epilogue_uv_171177e4:


    "No, «le risposte» e «la comprensione» non sono apparse dal nulla. Il sistema di sicurezza è appena stato attivato – attenzione, le porte si stanno chiudendo, non c'è più spazio per altre persone sul treno, la prossima fermata è…"


translate italian epilogue_uv_af4e6296:


    "Il treno se n'è andato e io sono rimasto in piedi nella stazione senza il mio bagaglio, indossando una giacca leggera, congelando a meno venti gradi Celsius."


translate italian epilogue_uv_49c71752:


    me "Stavo solo facendo una passeggiata!"


translate italian epilogue_uv_6ea6f839:


    "Quelle parole mi sono risuonate nella testa."


translate italian epilogue_uv_0ab3f93c:


    "Mi sentivo come un vichingo del medioevo che si trovava da solo contro un centinaio di nemici.{w} Nessun dubbio, nessun rimpianto, solo una ferrea volontà di morire con onore!"


translate italian epilogue_uv_9c5fc54a:


    uvp "Capisco…"


translate italian epilogue_uv_d37bab0a:


    "Mi ha risposto disinteressata, tornando al suo lavoro."


translate italian epilogue_uv_35d04d84:


    me "Eri tu? Quella sull'autobus?"


translate italian epilogue_uv_7d58e7f3:


    uvp "Ero io…"


translate italian epilogue_uv_a307d27a:


    "Mi ha risposto pigramente, senza distaccarsi dai suoi funghi."


translate italian epilogue_uv_d147d16f:


    me "Perché mi hai portato qui?"


translate italian epilogue_uv_e03d589f:


    uvp "Non ti ho portato."


translate italian epilogue_uv_8b05cf4e:


    me "Allora chi è stato?"


translate italian epilogue_uv_5dcae342:


    uvp "Non lo so."


translate italian epilogue_uv_7d60f019:


    me "Ma tu eri sull'autobus?"


translate italian epilogue_uv_63ea92fa:


    uvp "Sì."


translate italian epilogue_uv_9bfb0137:


    me "E?"


translate italian epilogue_uv_ad2b8ad1_1:


    uvp "Cosa?"


translate italian epilogue_uv_0225570c:


    "Gridarle addosso avrebbe potuto essere pericoloso, ma non sono riuscito a farne a meno."


translate italian epilogue_uv_afdca9b7:


    me "Come {i}cosa{/i}?! Rispondimi!"


translate italian epilogue_uv_0fca1ec3:


    uvp "Mi hai chiesto qualcosa?"


translate italian epilogue_uv_ea44f8ab:


    "Sembrava che la conversazione non le interessasse affatto."


translate italian epilogue_uv_9af412a5:


    me "Chi sei tu?"


translate italian epilogue_uv_efd76d15:


    "Mi ha guardato sorpresa."


translate italian epilogue_uv_ec74d78b:


    uvp "Io sono io!"


translate italian epilogue_uv_834c0fe5:


    me "Va bene…{w} Di dove sei?"


translate italian epilogue_uv_f09a8f60:


    uvp "Non lo so…{w} Sono sempre stata qui."


translate italian epilogue_uv_29be4a1c:


    me "Come sarebbe – sempre?{w} Devi pur essere venuta da qualche parte, no?"


translate italian epilogue_uv_4de354e1:


    uvp "Non lo so…"


translate italian epilogue_uv_064c8591:


    "Per un momento ho dubitato che l'essere che avevo davanti fosse un'entità onnipotente."


translate italian epilogue_uv_d676fe96:


    "Certo, questa ragazza è un po' straordinaria (per la precisione, assolutamente straordinaria) e quel giorno sull'autobus lei era con me, ma forse non ha nulla a che fare con il mio arrivo qui. Tutto è possibile…"


translate italian epilogue_uv_e4a40e96:


    me "Allora cosa sai?"


translate italian epilogue_uv_4fd84444:


    uvp "So come far provviste per l'inverno."


translate italian epilogue_uv_9d48f5b9:


    "Ha sorriso con dolcezza e semplicità."


translate italian epilogue_uv_3881d52a:


    me "Quali provviste?"


translate italian epilogue_uv_0ace2a6c:


    uvp "Beh, sai! Per l'inverno!{w} Potrebbe fare molto freddo…"


translate italian epilogue_uv_e5d08bc9:


    uvp "Anche se non ho mai visto l'inverno in questo luogo."


translate italian epilogue_uv_a7966ed3:


    me "E come avresti fatto a non vederlo, dato che sei qui da sempre?"


translate italian epilogue_uv_5dcae342_1:


    uvp "Non lo so."


translate italian epilogue_uv_1b595140:


    "Mi ha guardato intensamente."


translate italian epilogue_uv_8c4a8a38:


    uvp "Comunque!{w} Sei proprio noioso!"


translate italian epilogue_uv_7d21a175:


    me "Io?!"


translate italian epilogue_uv_a8361046:


    uvp "Sì, tu."


translate italian epilogue_uv_ef114c48:


    me "E come ti starei annoiando?"


translate italian epilogue_uv_1169b08a:


    uvp "Con le tue domande stupide."


translate italian epilogue_uv_1373dd1c:


    me "Stupide...?{w} Sono stato trascinato fuori dal mio mondo abituale, sono stato abbandonato qui, e ora le mie domande ti sembrano stupide?!"


translate italian epilogue_uv_d5ed42a0:


    uvp "Sì."


translate italian epilogue_uv_9087bb2a:


    "Ha risposto serenamente."


translate italian epilogue_uv_cee893c3:


    me "Allora quali domande secondo te non sarebbero stupide?"


translate italian epilogue_uv_22217845:


    uvp "Dove trovare lo zucchero, ad esempio."


translate italian epilogue_uv_c71c2028:


    me "Nella mensa, dove se no?!{w} Ce n'è un sacco lì se vuoi rubarlo – ho sentito dire che lo fai spesso."


translate italian epilogue_uv_b1cb93f2:


    uvp "Io non rubo!"


translate italian epilogue_uv_624d137a:


    "La ragazza dalle orecchie di animale si è indignata."


translate italian epilogue_uv_5c53c3a4:


    uvp "Lo prendo in prestito!{w} Poi ritorna comunque!"


translate italian epilogue_uv_b6908ceb:


    me "Che torni oppure no…{w} Non mi interessa! Voglio capire come e perché io sono qui! Voglio tornare indietro!"


translate italian epilogue_uv_c0d6abbb:


    uvp "A me non interessa."


translate italian epilogue_uv_45da52b8:


    "La ragazza si è alzata e si è ripulita."


translate italian epilogue_uv_3ab93971:


    me "Ma a me sì!{w} Se non sei tu la colpevole di tutto questo, allora devi almeno conoscere le risposte!"


translate italian epilogue_uv_6832591f:


    me "Qualcuno deve pur conoscerle..."


translate italian epilogue_uv_a4748218:


    "Mi ha guardato intensamente e si è messa a ridere."


translate italian epilogue_uv_310acf5d:


    uvp "Sei proprio divertente!"


translate italian epilogue_uv_32c4ee43:


    "Ho perso completamente la pazienza."


translate italian epilogue_uv_acb541be:


    me "Smettila di prendermi in giro! Tu! Sono stufo di te! Perché proprio a me?! Sono forse peggiore di chiunque altro?! Non ho mai chiesto niente di «divertente» come questo! Lasciami in pace! Non ho mai dato fastidio a nessuno! Non ho mai fatto del male a nessuno! Che cosa ti ho fatto?! Sparisci! Lasciami vivere in pace!"


translate italian epilogue_uv_10514786:


    "L'intero mio corpo tremava, il sangue mi pulsava nella testa a suon di cannonate, e la mia vista si è oscurata."


translate italian epilogue_uv_da5992ef:


    uvp "Ti sei sfogato adesso?"


translate italian epilogue_uv_1faf32bf:


    "Mi ha chiesto con calma."


translate italian epilogue_uv_59558a54:


    "Avevo voglia di distruggerla.{w} Almeno così avrei saputo se era fatta di carne e sangue oppure no."


translate italian epilogue_uv_bef37e18:


    uvp "Va bene, andiamo."


translate italian epilogue_uv_477fc689:


    me "Dove?"


translate italian epilogue_uv_d1be1fae:


    uvp "Lo vedrai."


translate italian epilogue_uv_e23148b5:


    "La ragazza mi ha fatto cenno di seguirla e mi ha guidato nelle profondità della foresta."


translate italian epilogue_uv_ce617998_1:


    "…"


translate italian epilogue_uv_f63b1b8b:


    "Dopo un po' di tempo ho iniziato ad avere il vago sospetto che stessimo girando in tondo."


translate italian epilogue_uv_97a205cc:


    "Alla fine ho deciso di fare una prova e ho lasciato cadere una carta di caramella in un punto visibile."


translate italian epilogue_uv_a6536d01:


    "Come previsto, dopo un po' me la sono ritrovata davanti."


translate italian epilogue_uv_03e5b22a:


    me "Aspetta."


translate italian epilogue_uv_9d8a1030:


    "Non ho osato afferrarle il bracco, così ho cercato di fermarla verbalmente."


translate italian epilogue_uv_ad2b8ad1_2:


    uvp "Che c'è?"


translate italian epilogue_uv_58955638:


    me "Dove mi stai portando?"


translate italian epilogue_uv_77a8b489:


    uvp "Te l'ho detto, lo vedrai!"


translate italian epilogue_uv_5b06e11d:


    me "Ma stiamo girando in tondo."


translate italian epilogue_uv_c379a4f1:


    uvp "E allora?"


translate italian epilogue_uv_20b0f9b4:


    me "Cosa vuol dire «e allora»?{w} Non arriveremo da nessuna parte girando in tondo!"


translate italian epilogue_uv_c5a6ae3e:


    uvp "L'hai detto tu stesso, che stavi solo facendo una passeggiata…"


translate italian epilogue_uv_ab154d49:


    "La ragazza sembrava strana, illogica, persino arrogante, come se provasse piacere nel prendersi gioco di me."


translate italian epilogue_uv_4c94d3cf:


    me "Non mi muoverò di un centimetro finché non mi spiegherai che cosa sta succedendo."


translate italian epilogue_uv_d8d14dde:


    uvp "Ok…"


translate italian epilogue_uv_077824e8:


    "Si è fermata e si è appoggiata a un grande albero."


translate italian epilogue_uv_b3106bae:


    uvp "Ero annoiata! È sempre la stessa cosa qui, ancora e poi ancora…{w} Ma di recente un sacco di cose nuove sono accadute, e adesso…"


translate italian epilogue_uv_2827a713:


    me "Adesso cosa?"


translate italian epilogue_uv_3bdae7a5:


    uvp "Adesso posso parlare con te."


translate italian epilogue_uv_d1046a49:


    "Ha sorriso."


translate italian epilogue_uv_60f5333f:


    me "E prima non potevi farlo?"


translate italian epilogue_uv_020b0a78:


    uvp "Beh, certamente potevo.{w} Forse…"


translate italian epilogue_uv_29f71b5c:


    me "Allora cosa è successo?"


translate italian epilogue_uv_5dcae342_2:


    uvp "Non lo so."


translate italian epilogue_uv_9ffdf555:


    me "Allora perché pensi che sia successo qualcosa, in primo luogo?"


translate italian epilogue_uv_60a48766:


    uvp "Ho avuto una sensazione."


translate italian epilogue_uv_0657f52c:


    me "Una sensazione, eh…?"


translate italian epilogue_uv_0b4ccb3b:


    "Ho fatto un sospiro impotente."


translate italian epilogue_uv_a531a3e7:


    "Forse lei è semplicemente una pazza che è fuggita dal manicomio, e non un essere onnipotente?"


translate italian epilogue_uv_2c88fada:


    me "E dai, siamo seri."


translate italian epilogue_uv_2df5ed7c:


    uvp "Ok, va bene!"


translate italian epilogue_uv_92199a18:


    "La ragazza mi ha guardato intensamente."


translate italian epilogue_uv_8e9d069a:


    me "Chi sei e cosa sta succedendo qui?"


translate italian epilogue_uv_b9debf29:


    uvp "Non lo so!"


translate italian epilogue_uv_7a944f53:


    "Mi ha risposto sempre con la solita espressione sul suo viso."


translate italian epilogue_uv_be4b5d36:


    me "E allora chi lo sa?"


translate italian epilogue_uv_b9debf29_1:


    uvp "Non lo so!"


translate italian epilogue_uv_007d8828:


    me "Va bene. Io vado a dormire."


translate italian epilogue_uv_b364eb6c:


    "Non avevo più la forza di parlare con lei, di ascoltare le sue sciocchezze, di camminare in tondo nella foresta."


translate italian epilogue_uv_a7f2edd7:


    "Anche se lei conoscesse le risposte e mi potesse riportare nel mondo consueto e tutto il resto, non l'avrebbe fatto sicuramente oggi.{w} Per oggi può bastare!"


translate italian epilogue_uv_d878403f:


    "Se dovessi restare un solo secondo in più, non rimarrebbe più nessuno da cui tornare."


translate italian epilogue_uv_89b1c070:


    uvp "Buona notte."


translate italian epilogue_uv_4e7cba70:


    "Mi ha detto caldamente."


translate italian epilogue_uv_5c1ed51e:


    me "Tutto qui?"


translate italian epilogue_uv_e6b99436:


    uvp "Cos'altro vorresti? Stai andando a dormire."


translate italian epilogue_uv_e4426188:


    "Ha ragione…"


translate italian epilogue_uv_994a9bf3:


    me "Ok. Ce l'hai almeno un nome?"


translate italian epilogue_uv_c07ebab2:


    uvp "Un nome?"


translate italian epilogue_uv_ff627021:


    me "Sì, un nome!"


translate italian epilogue_uv_4de354e1_1:


    uvp "Non lo so…"


translate italian epilogue_uv_be4b5d36_1:


    me "E chi dovrebbe saperlo?"


translate italian epilogue_uv_b9debf29_2:


    uvp "Non lo so!"


translate italian epilogue_uv_8225d824:


    me "Va bene. Come dovrei chiamarti allora?"


translate italian epilogue_uv_4de354e1_2:


    uvp "Non lo so…"


translate italian epilogue_uv_6942194c:


    me "Ma ogni persona… ogni cosa al mondo dovrebbe avere un nome, non c'è altro modo…"


translate italian epilogue_uv_4de002b5:


    "Mi ha guardato senza capire."


translate italian epilogue_uv_08ce5481:


    me "Va bene, allora ti chiamerò Yulya!"


translate italian epilogue_uv_7b802b26:


    uvp "Yulya…"


translate italian epilogue_uv_69f1b777:


    "Non so perché io abbia scelto quel nome. Forse perché è la prima cosa che mi è venuta in mente."


translate italian epilogue_uv_b9209126:


    uvp "Va bene."


translate italian epilogue_uv_d2b56c39:


    "Ha sorriso allegramente."


translate italian epilogue_uv_7d1b4f7a:


    uv "E ora devo andare!"


translate italian epilogue_uv_61d7e5e8:


    "La ragazza ha mosso le orecchie ed è scomparsa nella foresta."


translate italian epilogue_uv_6e5f9416:


    "Mi sono trascinato lentamente verso il Campo."


translate italian epilogue_uv_fa5ab919:


    "Ora mi sembrava di essere in una sorta di tragicommedia."


translate italian epilogue_uv_2bbab062:


    "Gli esseri superiori non possono essere degli idioti con il livello di sviluppo di un bambino dell'asilo, non è forse così?"


translate italian epilogue_uv_a774658e:


    "Anche se è possibile che non percepiscano il mondo come lo percepiamo noi…"


translate italian epilogue_uv_e30b61a2:


    "Ero esausto. Non riuscivo a comprendere appieno tutto quello che mi era successo di recente."


translate italian epilogue_uv_a70e7a5c:


    "Le risposte potrebbero trovarsi proprio davanti ai miei occhi, ho solo bisogno di allungare la mano.{w} Potrebbe trattarsi di quel famoso sgabello che mi consentirebbe di raggiungere la chiave."


translate italian epilogue_uv_674811f4:


    "E come potrei essere così calmo in una situazione del genere?"


translate italian epilogue_uv_1e24e1ff:


    "Forse sono solo stanco…{w} Stanco morto."


translate italian epilogue_uv_634747fb:


    "Olga Dmitrievna mi aspettava davanti alla porta della sua casetta."


translate italian epilogue_uv_87b272e3:


    mt "Finalmente!{w} Dove sei stato a vagabondare nel cuore della notte?"


translate italian epilogue_uv_19d8b6a9:


    "Le ho lanciato un'occhiata severa."


translate italian epilogue_uv_2dc57d68:


    "Mi chiedo, lei è reale?{w} Non ho alcun dubbio riguardo a quella ragazza, Yulya.{w} Lei non è di certo un essere umano, ma almeno esiste."


translate italian epilogue_uv_afc131f8:


    "E che dire di Olga Dmitrievna, degli altri pionieri?"


translate italian epilogue_uv_234e69a7:


    me "Sa, ho incontrato la ragazza-gatto."


translate italian epilogue_uv_975e5089:


    mt "Certo, certo…"


translate italian epilogue_uv_393056bc:


    "Ha ridacchiato."


translate italian epilogue_uv_e7cbadb2:


    me "Beh, non è costretta a credermi se non vuole."


translate italian epilogue_uv_f0a7d1a8:


    mt "Non voglio e non ti credo!"


translate italian epilogue_uv_099b3cc6:


    me "Come vuole."


translate italian epilogue_uv_339a89c8:


    "Ho detto con voce stanca, facendomi strada verso la stanza."


translate italian epilogue_uv_6f09955a:


    mt "Semyon, domani…"


translate italian epilogue_uv_48e685fd:


    "Non ho sentito il resto delle sue parole."


translate italian epilogue_uv_bd727701:


    "Rimaneva solo una cosa da capire – se qualcosa dipendesse da me oppure no."


translate italian epilogue_uv_07b2b5de:


    "Dovrei cercare di afferrare disperatamente quella chiave, o è la ragazza-gatto la risposta che cerco?"


translate italian epilogue_uv_e8bc9d3a:


    "Forse l'unica cosa che resta da fare è capire?"


translate italian epilogue_uv_949ae8dc:


    "Non è detto che l'indizio si debba manifestare a me in modo direttamente comprensibile!"


translate italian epilogue_uv_5c917e85:


    "Magari conoscevo da sempre quelle «risposte», ma dovevo semplicemente scoprire il loro significato…?"


translate italian epilogue_uv_86ab4f38:


    "E se tutto questo non fosse reale e stessi semplicemente sognando?"


translate italian epilogue_uv_30cf5a85:


    "E se mi fossi solo immaginato il dover «trovare le risposte», il «capire le risposte»?"


translate italian epilogue_uv_d1653018:


    "Forse non c'è alcuna gabbia, nessuna prigione, e la chiave è già appesa alla lampada bruciata nella mia stanza, che si è trasformata in un campo di pionieri, abitato da persone surreali ma piene di vita, che sembrano così comprensive e così preziose."


translate italian epilogue_uv_b8815151:


    "Beh, no…{w} Io non sono né un mago, né un oracolo, né un Dio…"


translate italian epilogue_uv_8b291a5a:


    "Voltaire una volta disse: «L'illusione è il primo dei piaceri». Forse la mia abilità migliore è l'illusione…"


translate italian epilogue_uv_0eb0dd52:


    "I pensieri si connettevano l'uno con l'altro, pronti a continuare a girare in tondo fino al mattino, ma il meccanismo di protezione si è attivato nuovamente, togliendo corrente all'intero sistema, invece di formattare il mio cervello come l'ultima volta."


translate italian epilogue_uv_3c841c5b:


    "Sono stato svegliato da un rumore infernale nella mia testa.{w} È risultato essere la sveglia."


translate italian epilogue_uv_5ef84dab:


    "Dopo essere riuscito con difficoltà a trovare il suo pulsante, l'ho spenta."


translate italian epilogue_uv_d7e76fb1:


    me "Olga Dmitrievna, sono le sette del mattino, perché così presto?"


translate italian epilogue_uv_4e6b16ec:


    "Ho detto in direzione dell'altro letto, aprendo gli occhi."


translate italian epilogue_uv_45634255:


    "Ma non c'era nessuno lì.{w} La leader del Campo dev'essere andata da qualche parte."


translate italian epilogue_uv_a3857818:


    "Mi sono alzato con difficoltà e mi sono guardato allo specchio.{w} Un semplice pioniere, nulla più."


translate italian epilogue_uv_9ae770c4:


    "Se qualcuno mi mostrasse delle foto di me in mezzo a questi pionieri, non mi riconoscerei."


translate italian epilogue_uv_3880648b:


    "Fuori il sole splendeva brillantemente – la natura sembrava stesse ridendo di me."


translate italian epilogue_uv_1580acda:


    "Mi sono incamminato lentamente verso i lavabi."


translate italian epilogue_uv_d341469e:


    "Durante il tragitto ho incontrato solo pochi pionieri, che chiaramente andavano di fretta."


translate italian epilogue_uv_d056bcc0:


    "Strano. Dove stanno andando tutti a quest'ora...?"


translate italian epilogue_uv_7a6be6b8:


    "L'acqua fredda mi ha rinfrescato un po', e tutti i ricordi degli ultimi giorni si sono riversati nella mia testa."


translate italian epilogue_uv_28ae18f5:


    "Beh, dovrei trovare Yulya e cercare di essere più educato e corretto questa volta."


translate italian epilogue_uv_ceb6fa03:


    "Dopotutto, se non si riesce a capire nemmeno un individuo della propria razza basandosi sulle proprie idee e sulla propria visione del mondo, allora come si potrebbe comunicare con un essere totalmente alieno?"


translate italian epilogue_uv_a1eafc73:


    "Perché gli alieni o le ragazze-gatto dovrebbero seguire le mie stesse leggi morali e trarre le mie stesse conclusioni logiche?"


translate italian epilogue_uv_7a1f73f3:


    "Loro potrebbero percepire l'universo in maniera del tutto diversa, e potrebbero considerare gli esseri umani come delle semplici formiche…"


translate italian epilogue_uv_79206dac:


    "Noi esseri umani non ci facciamo problemi nell'uccidere una zanzara fastidiosa, quindi perché a queste entità dovrebbe importare di me o dei miei insignificanti problemi?"


translate italian epilogue_uv_995e66c0:


    "Anche se potrebbe essere l'esatto contrario…{w} E Yulya potrebbe non avere alcuna colpa…"


translate italian epilogue_uv_a1d45f75:


    "C'è stato un fruscio tra i cespugli, e ho scorto un paio di orecchie familiari."


translate italian epilogue_uv_fc936817:


    me "Ti vedo."


translate italian epilogue_uv_bd81185d:


    "Yulya ha abbandonato a malincuore il suo nascondiglio."


translate italian epilogue_uv_1e212057:


    uv "E dai…"


translate italian epilogue_uv_94f2abe5:


    "Ha sbuffato, imbronciata."


translate italian epilogue_uv_0117c38f:


    me "Mi stavi spiando?"


translate italian epilogue_uv_5774bd7a:


    uv "Sì. Perché, non è permesso?"


translate italian epilogue_uv_b3e42019:


    me "Beh, probabilmente è permesso.{w} Volevo parlarti comunque."


translate italian epilogue_uv_58bb260d:


    uv "Davvero?{w} Anch'io."


translate italian epilogue_uv_0d6456d9:


    me "Di cosa?"


translate italian epilogue_uv_d8362ad8:


    "Mi sono chiesto."


translate italian epilogue_uv_8d36f7b7:


    uv "Sta succedendo qualcosa di strano qui…"


translate italian epilogue_uv_df3a694c:


    me "Strano? Che cosa?"


translate italian epilogue_uv_c6f62156:


    uv "Non lo so…{w} È solo che… ognuno si comporta in modo diverso rispetto al solito. Dev'essere successo qualcosa.{w} Prima tu, poi tutti gli altri…"


translate italian epilogue_uv_c17e7890:


    me "Di cosa stai parlando?{w} Non riesco a capire."


translate italian epilogue_uv_f1645aa5:


    uv "Non lo so, è solo una sensazione, e…{w} quando sento qualcosa, poi succede sempre!"


translate italian epilogue_uv_0b2a7882:


    "Yulya sembrava sconsolata e anche un po' impaurita."


translate italian epilogue_uv_3202acd0:


    "In questo momento non sembra affatto un essere superiore."


translate italian epilogue_uv_5756b2ff:


    me "Va bene, cerchiamo di indagare insieme."


translate italian epilogue_uv_311b83b6:


    uv "No, nessuno dovrebbe vedermi! Vediamoci qui tra un'ora, nel frattempo…"


translate italian epilogue_uv_57b757e5:


    "Un rumore di passi pesanti è risuonato dalla stradina – qualcuno stava correndo."


translate italian epilogue_uv_316ba4bb:


    "Mi sono voltato e ho visto Slavya avvicinarsi."


translate italian epilogue_uv_25cb17a4:


    "Yulya è sparita nella foresta, come previsto."


translate italian epilogue_uv_a87c71d7:


    me "Ciao."


translate italian epilogue_uv_5154e89b:


    "Ho sorriso."


translate italian epilogue_uv_cc470635:


    sl "Sì, ciao…"


translate italian epilogue_uv_9ee3c581:


    "Sembrava molto nervosa, con le guance rosse, gli occhi che brillavano e le trecce arruffate."


translate italian epilogue_uv_54314043:


    sl "Semyon, c'è…{w} Seguimi, lo vedrai con i tuoi occhi!"


translate italian epilogue_uv_1449335c:


    me "Che è successo?!"


translate italian epilogue_uv_e2810698:


    "Slavya aveva un'espressione talmente supplichevole che ho deciso di non discutere."


translate italian epilogue_uv_68329b9d:


    "Mi ha preso per mano e mi ha trascinato verso la piazza, a quanto pare l'intero Campo era radunato lì."


translate italian epilogue_uv_70843b37:


    "Dopo l'ultima curva ho guardato oltre la folla di pionieri che stavano fissando in direzione di Genda, e ho visto…"


translate italian epilogue_uv_041893cc:


    "In lontananza, dove prima c'erano campi e foreste, stava lentamente cominciando ad apparire una città!"


translate italian epilogue_uv_9ecdd8e3:


    "In un primo momento era sbiadita, come se fosse fatta di fumo, ma poi è diventata più solida, come se stesse emergendo dalla nebbia."


translate italian epilogue_uv_2ae80add:


    "Sky City – è la prima cosa che mi è venuta in mente."


translate italian epilogue_uv_39cee6c6:


    "Ma, che fosse strano oppure no, non c'era niente di particolare in essa, era solo una normale città – ce n'erano un sacco sparse per il nostro paese."


translate italian epilogue_uv_d6dcd9e8:


    "La si potrebbe considerare normale, persino noiosa."


translate italian epilogue_uv_808be7d4:


    "La tipica città che non merita attenzione quando le si passa davanti in automobile, o quando la si sorvola in aereo."


translate italian epilogue_uv_f1408521:


    "Ma vedere una cosa del genere qui e adesso, in questo Campo, sperduto tra i vasti campi e foreste di un universo parallelo, dove le uniche forme di vita presenti sono i pionieri locali, gli scoiattoli e le zanzare…"


translate italian epilogue_uv_2444ccc9:


    "Persino una ragazza-gatto non appare poi così strana in confronto a questo!"


translate italian epilogue_uv_2412f0b1:


    "Slavya continuava a stringermi forte la mano."


translate italian epilogue_uv_22d16f07:


    "I pionieri bisbigliavano qualcosa, come se avessero paura di parlarne ad alta voce, per timore che quell'illusione potesse scomparire, oppure il contrario – che potesse diventare più reale, devastando il piccolo campo «Sovyonok»."


translate italian epilogue_uv_727cd6f7:


    "Ma la città è rimasta lì dov'era, come una bianca scogliera che si innalzava sopra ad una valle."


translate italian epilogue_uv_4a0ef863:


    "Non c'era niente di speciale – niente grattacieli giganteschi né particolari costrutti architettonici, c'erano solo semplici case di vari colori e altezze, edifici industriali e un ospedale, o qualcosa di simile."


translate italian epilogue_uv_c096e4e5:


    "Non si potevano distinguere le persone o le automobili da quella distanza, ma ero sicuro che la città brulicasse di vita propria, indipendente dal nostro Campo."


translate italian epilogue_uv_26629e57:


    "Mi chiedo se qualcuno laggiù fosse salito sopra a un tetto e ci stesse guardando attraverso un binocolo, cercando di scorgere i pionieri che affollavano la piazza?"


translate italian epilogue_uv_ca6dbdb2:


    "In effetti mi è parso di vedere un riflesso di luce in cima a uno degli edifici più alti."


translate italian epilogue_uv_58def530:


    "Aspetta un secondo…{w} Un binocolo!"


translate italian epilogue_uv_2215b7ef:


    "Secondo le mie stime, la città era distante circa due o tre chilometri, quindi dovevo trovare un binocolo o un cannocchiale per poter scoprire qualcosa di più!"


translate italian epilogue_uv_part2_62ac5531:


    "Sono corso da Olga Dmitrievna."


translate italian epilogue_uv_part2_be449a3c:


    me "La vede anche lei?"


translate italian epilogue_uv_part2_92063617:


    mt "Sì, certo."


translate italian epilogue_uv_part2_c155417f:


    "Mi ha risposto un po' tesa."


translate italian epilogue_uv_part2_064c2399:


    me "Ci serve un binocolo! O un cannocchiale!"


translate italian epilogue_uv_part2_6bfa8108:


    "La leader del Campo ha smesso di guardare la città in lontananza, e mi ha fissato senza capire."


translate italian epilogue_uv_part2_238336f0:


    mt "Un binocolo…{w} Sì. Vai al circolo di cibernetica. Dovrebbero essercene alcuni."


translate italian epilogue_uv_part2_e0d4808f:


    "Sono guizzato in direzione del circolo."


translate italian epilogue_uv_part2_6db36340:


    "Electronik e Shurik stavano armeggiando con qualcosa come al solito, e sembrava che non avessero idea di ciò che stava accadendo nel Campo."


translate italian epilogue_uv_part2_b752e3f7:


    me "Un binocolo! Presto!"


translate italian epilogue_uv_part2_7a052c21:


    "Ho gridato, cercando di non soffocare."


translate italian epilogue_uv_part2_7d73585c:


    el "Che succede?"


translate italian epilogue_uv_part2_da743cb2:


    me "Non l'avete vista?{w} Ah diamine, ve ne state rinchiusi qui col vostro robot come dei gufi!{w} Avete un binocolo o un cannocchiale?!"


translate italian epilogue_uv_part2_d081d1f8:


    "Ho pronunciato lentamente ogni singola parola, come un cavernicolo che aveva appena imparato a parlare."


translate italian epilogue_uv_part2_e68ec0c8:


    sh "Potresti spiegarci con calma cosa sta succedendo?"


translate italian epilogue_uv_part2_94ede6ba:


    me "Non c'è tempo per spiegare, datemi il binocolo e correte alla piazza!"


translate italian epilogue_uv_part2_42e74a51:


    "I giovani cibernetici non hanno opposto resistenza. Dopo un minuto abbiamo raggiunto gli altri pionieri, e con il binocolo ho esaminato attentamente la città che era apparsa da chissà dove."


translate italian epilogue_uv_part2_ea239d21:


    "A prima vista non ci vedevo niente di speciale, mi ha dato l'impressione di essere una città della mia epoca, l'inizio del 21° secolo: cartelloni pubblicitari, antenne paraboliche, pompose insegne di negozi, {i}moderne{/i} automobili…"


translate italian epilogue_uv_part2_5a52abce:


    mt "Da' qua!"


translate italian epilogue_uv_part2_4fe722fb:


    "La leader del Campo mi ha strappato il binocolo dalle mani."


translate italian epilogue_uv_part2_11af697f:


    uv "E così quello sarebbe il tuo mondo?"


translate italian epilogue_uv_part2_d815dd97:


    "Mi sono voltato e ho visto Yulya che stava in piedi davanti a me."


translate italian epilogue_uv_part2_b876afc0:


    me "Mi avevi detto che nessuno dovrebbe vederti…"


translate italian epilogue_uv_part2_4726bdfc:


    uv "Sì, ma…"


translate italian epilogue_uv_part2_60f3d758:


    "Si è persa nei suoi pensieri."


translate italian epilogue_uv_part2_15c6c678:


    uv "Adesso nessuno mi vede."


translate italian epilogue_uv_part2_6ee3ebcd:


    "Ai pionieri non interessava la strana ragazza dalle orecchie di gatto, erano troppo impegnati ad osservare la città in lontananza."


translate italian epilogue_uv_part2_1e313794:


    me "Sai cos'è quella… e perché?"


translate italian epilogue_uv_part2_c34ebf47:


    uv "N-no."


translate italian epilogue_uv_part2_9697f09d:


    "Ha risposto dopo una breve pausa."


translate italian epilogue_uv_part2_9ca446d1:


    uv "Non è mai successo nulla del genere prima d'ora! Proprio nulla."


translate italian epilogue_uv_part2_ee658045:


    me "E cosa è successo prima?"


translate italian epilogue_uv_part2_65321879:


    uv "Beh. Sei venuto, poi te ne sei andato, poi sei tornato di nuovo…"


translate italian epilogue_uv_part2_7f19b333:


    me "E tu cosa hai fatto per tutto questo tempo?"


translate italian epilogue_uv_part2_74ba4a64:


    uv "Te l'ho già detto – stavo facendo provviste per l'inverno!"


translate italian epilogue_uv_part2_f8af74e9:


    "Non riuscivo proprio a capire, a Yulya questa cosa non sembrava per niente strana, o era solo una brava attrice?"


translate italian epilogue_uv_part2_65fa2c6e:


    "Tuttavia, chi avrebbe bisogno di preparare un tale teatrino solo per me?{w} Io non sono altro che un porcellino d'india dentro a una gabbia!"


translate italian epilogue_uv_part2_9da8e58a:


    me "Non ti credo.{w} Ne sai sicuramente qualcosa…"


translate italian epilogue_uv_part2_a6cdc653:


    mt "Semyon."


translate italian epilogue_uv_part2_1781e744:


    "Mi sono voltato verso la leader del Campo."


translate italian epilogue_uv_part2_e8f4b983:


    mt "Non ho idea di cosa stia succedendo, ma sembra tutto molto strano.{w} Dovremmo chiamare la polizia!"


translate italian epilogue_uv_part2_04f49c36:


    "Olga Dmitrievna ha reagito nel suo solito modo, come avrebbe fatto chiunque in una situazione del genere."


translate italian epilogue_uv_part2_dd2c1757:


    "Mi sono voltato verso Yulya per informarla della cosa in modo trionfante, ma la ragazza-gatto era già sparita."


translate italian epilogue_uv_part2_13051a08:


    mt "Cosa potrebbe essere?"


translate italian epilogue_uv_part2_97006f10:


    "Ha chiesto a bassa voce la leader del Campo, senza rivolgersi a nessuno in particolare."


translate italian epilogue_uv_part2_a35ec295:


    "Non sapevo se rivelarle ogni cosa, del futuro, del mio viaggio nel passato…"


translate italian epilogue_uv_part2_04d1fe96:


    "Come posso essere certo che quella città sia veramente della mia epoca?"


translate italian epilogue_uv_part2_8097992c:


    "Potrebbe semplicemente trattarsi di un'altra illusione, e il comportamento dei pionieri potrebbe far parte di essa?{w} E se invece non lo fosse…?"


translate italian epilogue_uv_part2_ff886f0c:


    me "Questo…"


translate italian epilogue_uv_part2_f58236c0:


    "Non sono riuscito a dire altro."


translate italian epilogue_uv_part2_310e737e:


    us "Forte! Forte!"


translate italian epilogue_uv_part2_b833fa40:


    "Ha gridato Ulyana, apparendo dal nulla."


translate italian epilogue_uv_part2_da11e595:


    us "Andiamo a dare un'occhiata!"


translate italian epilogue_uv_part2_6b336dbd:


    dv "Oh, sì. Sicuramente ci staranno aspettando."


translate italian epilogue_uv_part2_e7b79051:


    sl "Aspettate un momento. Dovremmo pensarci bene prima!"


translate italian epilogue_uv_part2_2ca18f35:


    "Lena si è avvicinata a noi, ma non ha detto nulla."


translate italian epilogue_uv_part2_35ebcb3b:


    sl "Cosa ne pensi, Semyon?"


translate italian epilogue_uv_part2_e522b546:


    "Mi ha chiesto Slavya fissandomi negli occhi, come se si aspettasse una risposta immediata."


translate italian epilogue_uv_part2_5de47c68:


    "Perché dovrebbe importare a qualcuno di cosa ne penso?"


translate italian epilogue_uv_part2_d822ea1f:


    "Cosa ho io in più di loro, per poter essere considerato onnisciente?"


translate italian epilogue_uv_part2_6cd79bd9:


    "Che importa se sono arrivato da poco in questo mondo? Le cose che stanno accadendo sono incomprensibili per me così come lo sono per loro."


translate italian epilogue_uv_part2_5c1e6f2d:


    me "Non ne ho idea! Anzi, andiamo a vedere con i nostri occhi…{w} Non sembra essere troppo lontana."


translate italian epilogue_uv_part2_13848e7c:


    mt "Ma è pericoloso!"


translate italian epilogue_uv_part2_850d94f4:


    un "Non è forse pericoloso anche restare qui?"


translate italian epilogue_uv_part2_67db1ed2:


    "Ha chiesto timidamente Lena. Tutti i pionieri, inclusa la leader del Campo, l'hanno guardata senza capire."


translate italian epilogue_uv_part2_d17d5e4a:


    un "Beh, voglio dire…{w} Se quella città è apparsa così all'improvviso, allora potrebbe accadere anche qualcos'altro, e noi…"


translate italian epilogue_uv_part2_b104c7ae:


    "Sembrava totalmente confusa, e quindi ha smesso di parlare."


translate italian epilogue_uv_part2_d1a7ad29:


    dv "Beh, ha ragione."


translate italian epilogue_uv_part2_1086056b:


    "Ha detto pigramente Alisa."


translate italian epilogue_uv_part2_d2fa28f9:


    dv "Non so esattamente cosa stia succedendo, ma non ho intenzione di starmene seduta qui ad aspettare che accada qualcos'altro!{w} Quindi, se nessuno vuole…"


translate italian epilogue_uv_part2_af0e79a1:


    us "Anch'io, anch'io!"


translate italian epilogue_uv_part2_bc284071:


    "L'ha interrotta Ulyana."


translate italian epilogue_uv_part2_399ecda1:


    sl "Beh, e allora? E se fosse solo un'illusione?"


translate italian epilogue_uv_part2_73dd7efd:


    me "Allora non abbiamo niente da perdere."


translate italian epilogue_uv_part2_98c7b6d0:


    "Ero un po' sorpreso dal loro acceso desiderio di scoprire la natura di quel fenomeno, dato che prima non facevano altro che cambiare argomento quando tentavo di parlare con loro riguardo alla mia misteriosa apparizione in questo mondo."


translate italian epilogue_uv_part2_c28ab8e4:


    "Forse si tratta di un enigma dentro a un altro enigma?{w} Come due sfere chiuse, anche noi siamo all'interno del guscio più esterno, dove tutto è chiaro per loro ma estraneo a me, e quella città è semplicemente l'intersezione tra le due sfere."


translate italian epilogue_uv_part2_1de398c6:


    "Tuttavia, se fosse così, allora probabilmente mi trovo sul confine – senza sapere nulla né dell'una né dell'altra."


translate italian epilogue_uv_part2_a1204b82:


    "Olga Dmitrievna era dubbiosa."


translate italian epilogue_uv_part2_a6810ea8:


    "Tutti gli altri non aspettavano altro che una sua approvazione o un suo divieto."


translate italian epilogue_uv_part2_24b65eee:


    "Non mi stupisce: nella vita quotidiana del Campo, rigorosa come un orologio svizzero, lei era un tiranno e un despota, ma quando i confini di «Sovyonok» sono stati ampliati e un misterioso nemico è apparso tra loro, lei non riusciva a tenere le redini del potere tra le sue mani, iniziando a farsi prendere dal panico, a sentirsi inquieta sul trono, che stava perdendo il suo fascino."


translate italian epilogue_uv_part2_15a8d7cc:


    "Questo potrebbe essere il momento giusto per cominciare ad agire con decisione!"


translate italian epilogue_uv_part2_24f6e173:


    me "Credo che dovremmo andare a controllare!"


translate italian epilogue_uv_part2_f47733f4:


    "Per quanto ancora dovrei restare qui, sperando di trovare {i}qualche{/i} risposta?! Dove le potrei trovare? Alla mensa? Nella casetta della leader del Campo? Accanto alla statua di Genda?"


translate italian epilogue_uv_part2_02724db8:


    "Non ho niente da fare qui, soprattutto adesso che si è presentata una tale opportunità!"


translate italian epilogue_uv_part2_ebcff3e9:


    mt "Ma…"


translate italian epilogue_uv_part2_a5b8262d:


    "Ha obiettato timidamente Olga Dmitrievna."


translate italian epilogue_uv_part2_63fc37d3:


    me "E se fossimo già morti o qualcosa del genere…?"


translate italian epilogue_uv_part2_1770827f:


    "Dovevo motivare gli altri in qualche modo, ma non volevo rivelare la verità su di me."


translate italian epilogue_uv_part2_94700582:


    un "Come sarebbe… morti?"


translate italian epilogue_uv_part2_664ee673:


    "Un'espressione terrorizzata ha pervaso il volto di Lena."


translate italian epilogue_uv_part2_4eb2832a:


    me "Non sto dicendo letteralmente morti, ma tutto può succedere! Non possiamo dire con certezza che cosa stia succedendo qui, e quindi dobbiamo considerare ogni possibile teoria!"


translate italian epilogue_uv_part2_d9abbf18:


    "Lena ha iniziato a piangere e Slavya ha cercato di consolarla."


translate italian epilogue_uv_part2_862ecd27:


    us "Così è ancora più divertente!"


translate italian epilogue_uv_part2_5ce4cf42:


    "Ed ecco Ulyana, per la quale nemmeno la morte potrebbe essere una scusa valida per agire con cautela."


translate italian epilogue_uv_part2_58c57031:


    dv "Dovremmo andarci comunque!"


translate italian epilogue_uv_part2_d7b7c1f7:


    "Ha detto allegramente Alisa."


translate italian epilogue_uv_part2_1f1e9ea8:


    "Mi sono voltato e ho guardato Shurik ed Electronik."


translate italian epilogue_uv_part2_ad26e5b2:


    el "Beh… Forse è meglio se noi restiamo qui, per misurare le distanze, per fare calcoli…"


translate italian epilogue_uv_part2_855db9ee:


    me "Capisco."


translate italian epilogue_uv_part2_8f02b6c0:


    "In realtà non mi sarei aspettato alcun aiuto da parte di quei due."


translate italian epilogue_uv_part2_f785cd61:


    mt "Allora preparatevi, prendete tutto quello che vi potrebbe servire…"


translate italian epilogue_uv_part2_acef7f53:


    "Olga Dmitrievna ha iniziato a darsi da fare."


translate italian epilogue_uv_part2_4c76d45a:


    mt "Torce elettriche, vestiti pesanti, walkie-talkie. Avete un walkie-talkie nel circolo?"


translate italian epilogue_uv_part2_69e4a0cc:


    "Si è rivolta ai cibernetici.{w} Hanno annuito affermativamente."


translate italian epilogue_uv_part2_ce617998:


    "…"


translate italian epilogue_uv_part2_ea5dec1f:


    "Dopo mezz'ora la «squadra investigativa» si è riunita alla piazza: io, Alisa, Lena, Slavya, e Ulyana."


translate italian epilogue_uv_part2_811b54d3:


    "Mi è stato dato uno zaino piuttosto pesante, pieno di vestiti caldi, di torce elettriche, di qualche altra cosa, e nelle mie mani tenevo un walkie-talkie a onde corte."


translate italian epilogue_uv_part2_d8c59b44:


    "Sarei sicuramente riuscito a camminare quei due o tre chilometri portandomelo sulle spalle, ma perché non distribuire gli oggetti a tutti i partecipanti?"


translate italian epilogue_uv_part2_bc26005b:


    me "Ehm, sa…"


translate italian epilogue_uv_part2_850ca1bb:


    "Ho lasciato cadere lo zaino a terra e ho guardato la leader del Campo."


translate italian epilogue_uv_part2_e33faa0d:


    me "Io sono l'unico uomo qui e lei mi immobilizza! Tutti dovrebbero trasportare la propria parte!"


translate italian epilogue_uv_part2_f9473cf8:


    "Le ragazze hanno esitato."


translate italian epilogue_uv_part2_556ce63a:


    "La prima ad avvicinarsi allo zaino è stata Ulyana. Ha preso una torcia elettrica e una giacca pesante, mugugnando."


translate italian epilogue_uv_part2_3cd3be0e:


    "Il resto del gruppo ha seguito il suo esempio."


translate italian epilogue_uv_part2_492c5c1a:


    "Non mi sarei aspettato una tale audacia da parte mia, e mi sarei aspettato ancora meno che loro potessero accettare senza far domande."


translate italian epilogue_uv_part2_7f3e103d:


    "Persino i pionieri del posto potevano comportarsi in modo logico in una situazione critica come questa."


translate italian epilogue_uv_part2_ced536c8:


    "Pochi minuti più tardi eravamo davanti al cancello del Campo."


translate italian epilogue_uv_part2_544fcb0f:


    mt "Spero possiate capire che non posso venire con voi…{w} Devo occuparmi degli altri, e…"


translate italian epilogue_uv_part2_3372f455:


    me "È tutto a posto, non c'è bisogno di giustificarsi."


translate italian epilogue_uv_part2_e2da94f6:


    "Non contavo molto sull'aiuto di Olga Dmitrievna, soprattutto perché c'era un fondo di verità nelle sue parole."


translate italian epilogue_uv_part2_6626bed3:


    mt "Buona fortuna!{w} E non dimenticate di restare in contatto via radio!"


translate italian epilogue_uv_part2_ce617998_1:


    "…"


translate italian epilogue_uv_part2_61cd3a24:


    "Il sole estivo ci tormentava senza pietà."


translate italian epilogue_uv_part2_89211204:


    "Il maglione caldo nello zaino è una sorta di presa in giro. Sarei disposto a dare via tutte le nostre torce e persino il walkie-talkie, per avere in cambio il cappello panama della leader del Campo e una bottiglia d'acqua."


translate italian epilogue_uv_part2_6d59a79c:


    "E perché nessuno ha pensato di portarsi dietro una fiaschetta d'acqua?"


translate italian epilogue_uv_part2_9dff38be:


    dv "Oh…"


translate italian epilogue_uv_part2_a8e7c03d:


    "I vestiti addosso ad Alisa diminuivano ad ogni minuto che passava."


translate italian epilogue_uv_part2_d5817008:


    dv "Facciamo una sosta!"


translate italian epilogue_uv_part2_90621c2d:


    "Ho rallentato e mi sono schermato gli occhi con la mano per riuscire a vedere la città, che si trovava alla nostra destra."


translate italian epilogue_uv_part2_3103df8d:


    "Non sembrava essersi avvicinata nemmeno di un centimetro."


translate italian epilogue_uv_part2_162c5d13:


    us "Perché non andiamo direttamente verso di essa?"


translate italian epilogue_uv_part2_1678d751:


    "Si è ribellata Ulyana."


translate italian epilogue_uv_part2_b1c773b0:


    me "E come? Passando attraverso le foreste e i campi?"


translate italian epilogue_uv_part2_ded4003e:


    us "Non m'importa! Sempre meglio che girarci attorno!"


translate italian epilogue_uv_part2_7adb53b7:


    sl "Faremo prima seguendo la strada. Guarda! C'è una curva {i}laggiù{/i}."


translate italian epilogue_uv_part2_8b245306:


    "Slavya stava cercando di farla ragionare."


translate italian epilogue_uv_part2_857c395f:


    dv "Fate come volete…"


translate italian epilogue_uv_part2_d6c4b72d:


    "Alisa si è lasciata cadere sull'erba sul ciglio della strada."


translate italian epilogue_uv_part2_b413fa09:


    dv "Io mi prendo una pausa."


translate italian epilogue_uv_part2_ee90e64d:


    "Ci siamo sistemati all'ombra di un piccolo cespuglio nei pressi della linea elettrica."


translate italian epilogue_uv_part2_37702a41:


    "Ho tirato fuori il binocolo e ho scrutato la città."


translate italian epilogue_uv_part2_4f498b93:


    "Le palazzine brillavano ancora in lontananza."


translate italian epilogue_uv_part2_b58174dc:


    "Perché non ci stiamo avvicinando? O magari è il calore che sta creando un'illusione ottica?"


translate italian epilogue_uv_part2_6ead5d25:


    "Un cespuglio lì vicino ha frusciato leggermente. Mi sono voltato e ho visto un paio di orecchie di gatto spuntare fuori da esso."


translate italian epilogue_uv_part2_f948448a:


    me "Aspettate, torno subito…"


translate italian epilogue_uv_part2_af746624:


    "Dopo essermi fatto strada tra i cespugli, ho fatto cenno a Yulya di seguirmi, e dopo esserci allontanati abbastanza in modo che nessuno potesse vederci, abbiamo iniziato a parlare."


translate italian epilogue_uv_part2_f2554bb0:


    me "E così ci stavi seguendo?"


translate italian epilogue_uv_part2_bb30009f:


    uv "Beh, certo. Anch'io sono interessata!"


translate italian epilogue_uv_part2_721821a4:


    me "Ah, capisco.{w} E tu non sai cosa ci aspetta laggiù, vero?"


translate italian epilogue_uv_part2_2590e0ab:


    uv "Certo che no!"


translate italian epilogue_uv_part2_06ab0532:


    "Ha sorriso."


translate italian epilogue_uv_part2_90fa743d:


    me "E non hai idea del perché non ci stiamo avvicinando di un millimetro alla città?"


translate italian epilogue_uv_part2_cce2dbb5:


    uv "In realtà sì."


translate italian epilogue_uv_part2_b528e2bc:


    "Ha risposto tranquillamente."


translate italian epilogue_uv_part2_da0567b3:


    me "Allora perché?"


translate italian epilogue_uv_part2_9f61aef1:


    uv "Perché non potete lasciare questo luogo."


translate italian epilogue_uv_part2_aad4d61a:


    "Ho fatto un grande sospiro e mi sono appoggiato all'albero solitario."


translate italian epilogue_uv_part2_34d58fa0:


    me "Forse è arrivato il momento di dirmi tutto?"


translate italian epilogue_uv_part2_60f3d758_1:


    "Sembrava persa nei suoi pensieri."


translate italian epilogue_uv_part2_beaccc1b:


    uv "Prima era diverso.{w} Sei venuto, e poi… poi te ne sei andato, e poi sei tornato di nuovo. E anche se sei riuscito ad andartene, dopo è stato ancora lo stesso. E quella città… è il tuo mondo…"


translate italian epilogue_uv_part2_c89b97c8:


    me "Perché lo consideri sempre {i}il mio{/i} mondo?"


translate italian epilogue_uv_part2_c01f7fd7:


    uv "Perché non fa parte di questo luogo!"


translate italian epilogue_uv_part2_1c70a91b:


    "Yulya era esasperata."


translate italian epilogue_uv_part2_43d1686d:


    me "E cos'è questo luogo? Puoi essere più chiara?"


translate italian epilogue_uv_part2_056a2259:


    "Certamente non ero arrabbiato con lei – mi sembrava che la ragazza-gatto non mi stesse nascondendo nulla di importante, ma o lei non capiva le mie domande, oppure non conosceva le risposte."


translate italian epilogue_uv_part2_9abf8151:


    uv "Non lo so."


translate italian epilogue_uv_part2_5a9a2edc:


    me "D'accordo. Facciamo un passo alla volta.{w} Da quanto tempo sei qui?"


translate italian epilogue_uv_part2_af20e0a2:


    uv "Da sempre."


translate italian epilogue_uv_part2_8a11ab08:


    me "«Da sempre» è una definizione troppo complessa per un essere umano.{w} Anche se probabilmente non sei umana."


translate italian epilogue_uv_part2_89141735:


    "Mi sono perso nei miei pensieri."


translate italian epilogue_uv_part2_2d026c0d:


    me "E io che ci faccio qui?"


translate italian epilogue_uv_part2_48657b0c:


    uv "Anche tu sei sempre stato qui."


translate italian epilogue_uv_part2_03adb46e:


    me "Ma com'è possibile? Mi sono forse sdoppiato? Perché non ricordo nulla del genere."


translate italian epilogue_uv_part2_79ba8a6b:


    uv "Beh. Sei tu e non sei tu. Ce ne sono molti, di te."


translate italian epilogue_uv_part2_ddf16d12:


    me "E di te ce n'è una sola?"


translate italian epilogue_uv_part2_819d8828:


    uv "Sì, io sono l'unica."


translate italian epilogue_uv_part2_41c98a55:


    "Yulya ha sorriso amabilmente, agitando la coda."


translate italian epilogue_uv_part2_5de7b061:


    me "Sempre la stessa per tutti i miei {i}me stessi paralleli{/i}?"


translate italian epilogue_uv_part2_04cf541e:


    uv "Qualcosa del genere."


translate italian epilogue_uv_part2_5d95f80c:


    me "Allora, sei come… onnipotente?"


translate italian epilogue_uv_part2_0ce08539:


    "Non sono riuscito a trovare un termine migliore."


translate italian epilogue_uv_part2_6ccdf245:


    uv "Non lo so."


translate italian epilogue_uv_part2_f55362eb:


    me "Hai forse delle abilità soprannaturali? Puoi volare, puoi lanciare palle di fuoco, o teletrasportarti?"


translate italian epilogue_uv_part2_e4b3a5ba:


    uv "Riesco a far provviste per l'inverno."


translate italian epilogue_uv_part2_3d4f4933:


    "Mi ha risposto in tono serio."


translate italian epilogue_uv_part2_4e34062e:


    me "Consideriamolo come un «no».{w} E quale sarebbe lo scopo di… tutto questo?"


translate italian epilogue_uv_part2_6ccdf245_1:


    uv "Non lo so."


translate italian epilogue_uv_part2_8eabed28:


    "Se prima riuscivo in qualche modo a mantenere la calma, dopo tutti quei suoi «non lo so» stavo cominciando a perdere la pazienza."


translate italian epilogue_uv_part2_997bc03d:


    me "Sai una cosa…"


translate italian epilogue_uv_part2_2a454c26:


    sl "Semyon!"


translate italian epilogue_uv_part2_5f087d0d:


    "Ha gridato Slavya."


translate italian epilogue_uv_part2_2e9e672c:


    "Mi sono voltato e quando ho guardato indietro verso punto in cui si trovava Yulya, lei era già scomparsa."


translate italian epilogue_uv_part2_d9f91079:


    me "Ci risiamo…"


translate italian epilogue_uv_part2_4a436c24:


    "Ho digrignato i denti e sono tornato dalle ragazze."


translate italian epilogue_uv_part2_ce617998_2:


    "…"


translate italian epilogue_uv_part2_a204f5f2:


    "Ormai stavamo camminando da diverse ore, ma la città si trovava sempre alla stessa distanza."


translate italian epilogue_uv_part2_a9df582b:


    "Non ci stavamo avvicinando né allontanando di un singolo metro."


translate italian epilogue_uv_part2_ca6e81ce:


    us "Ve l'avevo detto che saremmo dovuti andare dritti verso di essa!"


translate italian epilogue_uv_part2_d2785117:


    dv "Sta' zitta!{w} Proprio non capisco…"


translate italian epilogue_uv_part2_540edccf:


    un "Forse è davvero un'illusione?"


translate italian epilogue_uv_part2_36d8976c:


    "Ha esordito Lena, che non aveva detto ancora una singola parola."


translate italian epilogue_uv_part2_b103b945:


    me "No, persino i miraggi nel deserto spariscono se ti avvicini abbastanza ad essi.{w} Probabilmente…"


translate italian epilogue_uv_part2_d83880ec:


    dv "Cosa facciamo?"


translate italian epilogue_uv_part2_a5fdb009:


    "Ha chiesto Alisa, indignata."


translate italian epilogue_uv_part2_5a93ad2a:


    dv "Ne ho proprio abbastanza di tutto questo. Torniamo indietro!"


translate italian epilogue_uv_part2_17dea409:


    sl "Allora vai se proprio vuoi. Nessuno te lo impedisce!"


translate italian epilogue_uv_part2_a4fde9ae:


    "Le ha ringhiato Slavya."


translate italian epilogue_uv_part2_7e935856:


    "Tutti l'hanno guardata sbigottiti. Era davvero strano sentire certe parole dal pioniere modello."


translate italian epilogue_uv_part2_1413dbd3:


    sl "Perché devi sempre essere insoddisfatta di ogni singola cosa? Se proprio non ti piace – allora porta il culo da un'altra parte!"


translate italian epilogue_uv_part2_f392c23f:


    dv "Nessuno ti ha chiesto niente…"


translate italian epilogue_uv_part2_b75cedb9:


    "Le ha risposto Alisa, titubante."


translate italian epilogue_uv_part2_ad0de36d:


    un "Beh, ha ragione!"


translate italian epilogue_uv_part2_a9448a42:


    "Lena si è intromessa nella conversazione."


translate italian epilogue_uv_part2_0a78c8a6:


    un "Sono stanca delle tue lamentele!"


translate italian epilogue_uv_part2_610dac36:


    "Ulyana si è spaventata, nascondendosi dietro di me."


translate italian epilogue_uv_part2_3f49c33b:


    dv "Oh, tu sei qui solo per fare la figura della brava ragazza alle spese degli altri! Sicuro! È più facile mettersi in risalto quando vengo messa in disparte!"


translate italian epilogue_uv_part2_f9318593:


    un "Credimi – non verrai più messa da nessuna parte dopo quello che ti farò!"


translate italian epilogue_uv_part2_75eeb62b:


    "Lena si è scagliata contro Alisa, e Slavya si è intromessa tra le due."


translate italian epilogue_uv_part2_cc189774:


    sl "Fatti da parte, me ne occupo io!"


translate italian epilogue_uv_part2_d1a7ebe0:


    us "Smettetela!"


translate italian epilogue_uv_part2_1689ea1f:


    "Ha gridato Ulyana, in lacrime."


translate italian epilogue_uv_part2_dfa2690e:


    "Non avevo idea di cosa fare in una situazione del genere, ma non potevo di certo lasciare che si scannassero."


translate italian epilogue_uv_part2_71564beb:


    "Tuttavia, non c'è stato bisogno del mio intervento: Yulya è sbucata dal nulla, si è intromessa tra le ragazze, sibilando di rabbia."


translate italian epilogue_uv_part2_12b53b2f:


    uv "Non dovrebbe essere così! Non va bene! Non dovreste comportarvi in questo modo! Non è questo il vostro scopo!"


translate italian epilogue_uv_part2_24b87964:


    "Le ragazze hanno sobbalzato indietreggiando. Alisa ha strillato, Slavya ha adottato una sorta di posizione da combattimento e Lena teneva in mano un coltello che aveva tirato fuori da chissà dove."


translate italian epilogue_uv_part2_405c714f:


    sl "E tu chi sei?"


translate italian epilogue_uv_part2_cd104290:


    un "Chi è?"


translate italian epilogue_uv_part2_852891ee:


    dv "Cos'è?"


translate italian epilogue_uv_part2_cdd96158:


    "Non riuscivo a capire se stessero parlando di Yulya o di me."


translate italian epilogue_uv_part2_457e2c5c:


    me "Aspettate…"


translate italian epilogue_uv_part2_6f6f163b:


    "Mi sono messo tra loro e la ragazza-gatto."


translate italian epilogue_uv_part2_0b28dad0:


    me "Posso spiegarvi tutto."


translate italian epilogue_uv_part2_893718f4:


    "Come se sentissi il bisogno di scusarmi…{w} Nei confronti di chi? Per quale ragione? Dovrebbe essere lei a spiegare ogni cosa!"


translate italian epilogue_uv_part2_37d7b496:


    "Le ragazze si sono calmate un po'."


translate italian epilogue_uv_part2_314afab1:


    me "Beh… Conoscete tutti la leggenda della ragazza-gatto? Ecco, questa è…"


translate italian epilogue_uv_part2_6905fe00:


    "Yulya continuava a sibilare, ma più silenziosamente."


translate italian epilogue_uv_part2_1b195105:


    sl "Lei lo sa."


translate italian epilogue_uv_part2_2ff596a2:


    "Ha detto Slavya, con freddezza."


translate italian epilogue_uv_part2_3183db72:


    sl "Lei sa cos'è quella città. Lei sa tutto!"


translate italian epilogue_uv_part2_f4f28a92:


    me "Lei non sa nulla."


translate italian epilogue_uv_part2_6ccdf245_2:


    uv "Io non so nulla."


translate italian epilogue_uv_part2_b1ca2485:


    "Ha confermato Yulya."


translate italian epilogue_uv_part2_6c17c857:


    dv "Gliel'hai chiesto nel modo corretto?"


translate italian epilogue_uv_part2_2b82084e:


    "Alisa si è alzata, schioccandosi le nocche."


translate italian epilogue_uv_part2_293737cc:


    me "Ma che diavolo state combinando?! È ora di finirla! Prima l'autobus, poi il Campo, poi voi, poi lei, e adesso pure quella maledetta città!"


translate italian epilogue_uv_part2_f8b31659:


    "Ho iniziato a urlare, digrignando i denti, e probabilmente generando scintille dai miei occhi."


translate italian epilogue_uv_part2_b458ed0a:


    me "Quando è troppo è troppo! Ne ho abbastanza di tutte queste incertezze! O mi spiegate cosa diavolo sta accadendo qui, o non mi muoverò più di un millimetro! Uccidetemi se volete, non m'interessa!"


translate italian epilogue_uv_part2_86355b31:


    me "Pensate davvero che io sia disposto a subire ogni cosa? Che io possa far finta che non stia succedendo niente e che io non capisca niente, indipendentemente da tutto? Non m'importa! Ogni cosa ha un limite…"


translate italian epilogue_uv_part2_0ca24177:


    "Ho concluso il mio discorso in modo un po' più calmo, dopo essere rimasto senza fiato."


translate italian epilogue_uv_part2_92f97560:


    "Il sangue mi martellava nelle tempie, il mondo si è oscurato per un istante, e poi si è colorato di tinte inimmaginabili."


translate italian epilogue_uv_part2_0bf17dcb:


    "Le ragazze mi guardavano sorprese, e forse anche intimorite."


translate italian epilogue_uv_part2_a5c34365:


    "Yulya ha smesso di sibilare ed è tornata alla sua solita indifferenza."


translate italian epilogue_uv_part2_505d91ae:


    me "D'accordo…"


translate italian epilogue_uv_part2_f9b7b6f4:


    "Mi sono incamminato lungo la strada in direzione della città, che si allontanava di un passo ad ogni passo che facevo verso di essa."


translate italian epilogue_uv_part2_58aebb5d:


    "Nessuno mi ha seguito…"


translate italian epilogue_uv_part2_ce617998_3:


    "…"


translate italian epilogue_uv_part2_9db18933:


    "Il sole stava iniziando a tramontare, e la città, ancora visibile a est, si stava dissolvendo progressivamente nella foschia."


translate italian epilogue_uv_part2_97bdb375:


    "Non riuscivo a capire dove si trovasse veramente, perché i campi davanti ad essa erano interrotti dalle foreste, ma gli edifici si innalzavano sopra gli alberi come l'elemento principale del diorama."


translate italian epilogue_uv_part2_9811a37e:


    "La città è rimasta muta, come se stesse galleggiando sul mondo, guardando in basso con silente rimprovero verso i miei patetici tentativi di raggiungerla."


translate italian epilogue_uv_part2_899035c2:


    "Secondo i miei calcoli ho camminato molto più di due o tre chilometri, che mi è sembrata la distanza effettiva di quella città. Mi sono fermato a riposare all'ombra un paio di volte, e la freschezza serale è diventata il mio salvatore."


translate italian epilogue_uv_part2_3c799697:


    "Forse non c'era alcuna città, e io stavo correndo come un criceto sulla ruota, sentendo la fatica della distanza percorsa, ma il mio punto di arrivo si trovava sempre alla stessa distanza, al di fuori della gabbia…"


translate italian epilogue_uv_part2_a6ba1110:


    "Mi sono tornati in mente i miei pensieri di ieri riguardo a quella chiave attaccata al soffitto."


translate italian epilogue_uv_part2_1fd60c00:


    "Adesso mi tornerebbe utile! Forse in realtà non c'è alcuna risposta. Sono troppo stanco anche solo a pensarci…{w} E quello sfogo furioso davanti alle ragazze…"


translate italian epilogue_uv_part2_e14ad1c1:


    "Ah, al diavolo!"


translate italian epilogue_uv_part2_86264158:


    "Mi sono seduto sul ciglio della strada, sollevando una piccola tempesta di polvere da terra, che mi ha avvolto facendomi tossire."


translate italian epilogue_uv_part2_ab8b4683:


    "Tutta quella polvere mi ha fatto lacrimare."


translate italian epilogue_uv_part2_d0c5e298:


    "Quando ho riaperto gli occhi, Yulya si trovava di fronte a me."


translate italian epilogue_uv_part2_71403012:


    me "Pare che essere un felino abbia i suoi vantaggi – puoi muoverti molto più rapidamente degli esseri umani."


translate italian epilogue_uv_part2_aeea0184:


    uv "Dove stai andando?"


translate italian epilogue_uv_part2_663c6ac0:


    "Ha chiesto, ignorando le mie parole."


translate italian epilogue_uv_part2_b0e31945:


    me "Sto andando proprio là!"


translate italian epilogue_uv_part2_90c9d82c:


    "Ho agitato la mano in direzione della città."


translate italian epilogue_uv_part2_37fe8f5c:


    uv "Ma perché?"


translate italian epilogue_uv_part2_0ee3d648:


    me "Dove sono gli altri?"


translate italian epilogue_uv_part2_fa0bf095:


    uv "Sono tornati al Campo."


translate italian epilogue_uv_part2_855db9ee_1:


    me "Capisco."


translate italian epilogue_uv_part2_740dbe23:


    uv "Allora, perché stai andando lì?"


translate italian epilogue_uv_part2_b51017bc:


    me "Perché…"


translate italian epilogue_uv_part2_89141735_1:


    "Mi sono perso nei miei pensieri."


translate italian epilogue_uv_part2_369bcbed:


    me "Che altro dovrei fare? Cosa potrebbe succedere se rimanessi seduto qui?"


translate italian epilogue_uv_part2_d06bfe4a:


    uv "Niente."


translate italian epilogue_uv_part2_73cd06af:


    me "È proprio questo il punto."


translate italian epilogue_uv_part2_6af42011:


    "Yulya mi ha guardato intensamente e si è seduta accanto a me."


translate italian epilogue_uv_part2_479c6fcf:


    uv "Vorrei poterti aiutare…"


translate italian epilogue_uv_part2_520e4d28:


    "Ha detto improvvisamente a bassa voce."


translate italian epilogue_uv_part2_b020cad8:


    me "Allora aiutami!"


translate italian epilogue_uv_part2_80f6bbf6:


    uv "Non so come…"


translate italian epilogue_uv_part2_583e4d51:


    me "Parlami di quella città, di questo mondo! Perché sono qui? Come faccio a tornare?"


translate italian epilogue_uv_part2_bfefff38:


    uv "Non lo so…{w} Beh, voglio dire…{w} È sempre stato così: arrivi al Campo in autobus, trascorri una settimana qui e poi, indipendentemente dal risultato, tutto si ripete. E… e… ce ne sono molti di te, sono tutti diversi."


translate italian epilogue_uv_part2_1eebdf41:


    me "In che senso {i}diversi{/i}?"


translate italian epilogue_uv_part2_99fb7c6c:


    uv "Non come te! Tu sei l'unico così, ecco perché posso parlare solo con te, gli altri non capiscono…"


translate italian epilogue_uv_part2_0180ce19:


    me "Che cosa non capiscono?"


translate italian epilogue_uv_part2_37b06b96:


    uv "A volte scompaiono."


translate italian epilogue_uv_part2_be1ebd85:


    "Yulya ha continuato a parlare, ignorando la mia domanda."


translate italian epilogue_uv_part2_ad623b81:


    uv "È così che dovrebbe essere. Non ho idea del perché, ma dev'essere così. Loro non tornano dopo quei sette giorni, ma poi ne arrivano degli altri, che sono del tutto simili a quelli precedenti. All'inizio siete tutti uguali, quasi identici."


translate italian epilogue_uv_part2_a74fdca1:


    uv "Le differenze si notano dopo un po' di tempo. Soprattutto dopo che hanno capito che tutto si ripeterà ancora e poi ancora."


translate italian epilogue_uv_part2_43def3e2:


    me "Questo significa che anch'io sono destinato a girare in tondo?"


translate italian epilogue_uv_part2_0e00b743:


    uv "Non lo so. Questa volta è diverso."


translate italian epilogue_uv_part2_32193853:


    me "Perché accade tutto questo? Chi sono quegli {i}altri{/i}?"


translate italian epilogue_uv_part2_4de002b5:


    "Mi ha guardato senza capire."


translate italian epilogue_uv_part2_a5fc162e:


    uv "Loro sono te…"


translate italian epilogue_uv_part2_67914ffa:


    me "Cosa significa che sono {i}me{/i}?"


translate italian epilogue_uv_part2_660e4ce1:


    uv "Tu e tutti gli altri…"


translate italian epilogue_uv_part2_8f96fdab:


    "Ha brontolato, cercando di trovare le parole giuste."


translate italian epilogue_uv_part2_cf6949e5:


    uv "Siete tutti uguali!"


translate italian epilogue_uv_part2_781b326f:


    me "Quindi sono dei cloni o qualcosa del genere?"


translate italian epilogue_uv_part2_dc13f3f4:


    uv "Cosa sono i «cloni»?"


translate italian epilogue_uv_part2_b2232755:


    me "Delle copie.{w} Come due funghi identici che raccogli per l'inverno!"


translate italian epilogue_uv_part2_2b8c7e9e:


    uv "Non esistono due funghi identici."


translate italian epilogue_uv_part2_2cde1e69:


    "Ha detto Yulya, in modo serio."


translate italian epilogue_uv_part2_262a8646:


    me "Allora immagina che possano esistere!"


translate italian epilogue_uv_part2_2bb025d9:


    uv "Funghi identici…{w} Forse… Non lo so."


translate italian epilogue_uv_part2_5d277179:


    me "Oddio, questo è proprio un incubo…"


translate italian epilogue_uv_part2_84ad17c2:


    "Ho appoggiato la testa sulle ginocchia e ho chiuso gli occhi."


translate italian epilogue_uv_part2_19e9263c:


    "Siamo rimasti seduti in silenzio per alcuni minuti."


translate italian epilogue_uv_part2_893bd690:


    "Ero davvero interessato a sapere cosa le passasse per la testa."


translate italian epilogue_uv_part2_f9abbf96:


    "Dopotutto, anche se lei non sapesse davvero nulla di tutto questo, doveva avere comunque un'opinione riguardo a tutto ciò che stava accadendo."


translate italian epilogue_uv_part2_8f211b5f:


    me "Va bene. Allora cosa dovrei fare?"


translate italian epilogue_uv_part2_d1215d87:


    "Ho chiesto in tono serio."


translate italian epilogue_uv_part2_4de002b5_1:


    "Mi ha guardato di nuovo senza capire."


translate italian epilogue_uv_part2_cad610a0:


    me "Che cosa mi consigli di fare?"


translate italian epilogue_uv_part2_9abf8151_1:


    uv "Non lo so."


translate italian epilogue_uv_part2_48f8fcee:


    me "Cosa faresti al mio posto?"


translate italian epilogue_uv_part2_4b09c0b5:


    uv "Io...?{w} Al tuo posto...?"


translate italian epilogue_uv_part2_8495dc4f:


    uv "Ma non potrebbe mai succedere!"


translate italian epilogue_uv_part2_05864633:


    "Yulya ha riso forte."


translate italian epilogue_uv_part2_f798a921:


    uv "Tu sei tu, e io sono io!"


translate italian epilogue_uv_part2_615ead6d:


    me "Prova a immaginarlo! Tu hai la tua mente, allora dovresti avere anche la fantasia!"


translate italian epilogue_uv_part2_1fbe04a5:


    uv "Beh…{w} Non cambierei nulla."


translate italian epilogue_uv_part2_5ac38cf1:


    me "Perché no?"


translate italian epilogue_uv_part2_3f04a191:


    uv "Perché è così che deve andare, è meglio ed è meno preoccupante."


translate italian epilogue_uv_part2_c080aa07:


    me "Mi stai dicendo che è meno preoccupante starsene seduti nel Campo attendendo le infinite ripetizioni?"


translate italian epilogue_uv_part2_0e8e1d0f:


    uv "Sì."


translate italian epilogue_uv_part2_8face0e3:


    "Non potevo essere d'accordo. Semplicemente non potevo."


translate italian epilogue_uv_part2_ff9f8675:


    "Finché posso sperare di riuscire a cambiare qualcosa, o anche solo provarci, allora devo agire!"


translate italian epilogue_uv_part2_e41273e8:


    "Mi sono alzato da terra e ho marciato dritto verso la città in lontananza, che spiccava tra i raggi cremisi del tramonto."


translate italian epilogue_uv_part2_ce617998_4:


    "…"


translate italian epilogue_uv_part2_a10a51f7:


    "Alla fine la strada ha preso una svolta improvvisa. Gli edifici, che prima si innalzavano sopra agli alberi, adesso erano scomparsi dalla mia vista, e il paesaggio di fronte a me è stato rimpiazzato da delle periferie industriali."


translate italian epilogue_uv_part2_f021e5ae:


    "Era già buio. Alcune stelle brillavano sparse nel cielo, ancora visibili dal punto in cui mi trovavo, ma si dissolvevano al di sopra della città, che era colma delle lontane luci di finestre e lampioni."


translate italian epilogue_uv_part2_47d682d4:


    "Una silenziosa e oscura notte regnava alle mie spalle, interrotta solo dal gorgheggio di un sonnolento volatile notturno, dal fruscio dell'erba e dal ronzio monotono degli insetti. Mentre di fronte a me c'era un formicaio umano che non poteva smettere di brulicare nemmeno per un minuto, scintillando allegramente con le sue migliaia di luci e gridando con milioni di voci."


translate italian epilogue_uv_part2_29ef898b:


    "La strada che stavo percorrendo sembrava strana. Era vuota, come se fosse deserta, abbandonata sia dalle automobili che dalle persone."


translate italian epilogue_uv_part2_c4ec3c82:


    "Come uno stretto sentiero che si allontanava da {i}questo{/i} mondo verso un altro."


translate italian epilogue_uv_part2_6b67a347:


    "Per tutto questo tempo Yulya si è limitata a camminare accanto a me in silenzio."


translate italian epilogue_uv_part2_62569e6a:


    me "Perché sei qui?"


translate italian epilogue_uv_part2_420d89ea:


    "Le ho chiesto pacatamente."


translate italian epilogue_uv_part2_686e7c65:


    uv "Perché devo stare con te."


translate italian epilogue_uv_part2_5ac38cf1_1:


    me "Perché?"


translate italian epilogue_uv_part2_29bfa65a:


    uv "Che strano. La città è sempre stata sul lato destro, mentre ora si trova di fronte a noi."


translate italian epilogue_uv_part2_6ab05308:


    "Ha ignorato la mia domanda."


translate italian epilogue_uv_part2_d2669c69:


    me "Non molto più strano di tutto il resto.{w} Ho camminato per circa dieci o venti chilometri, e sembra ancora così lontana!"


translate italian epilogue_uv_part2_cf738eeb:


    uv "Lontana?"


translate italian epilogue_uv_part2_44fcb674:


    me "Forse non riusciremo nemmeno a raggiungerla!"


translate italian epilogue_uv_part2_3b18e37c:


    "Mi sono fermato e ho cercato di immaginare cosa mi avrebbe aspettato lì."


translate italian epilogue_uv_part2_fe135b74:


    "Quella città è forse simile alla mia? Di quali strutture è costituita, chi ci vive…?"


translate italian epilogue_uv_part2_55dee337:


    "E perché ho subito pensato che fosse abitata da persone? Che non fosse solo un'illusione, uno degli sporchi trucchi di questo mondo malato?"


translate italian epilogue_uv_part2_959e8bb8:


    "Persino Yulya ha detto che sta vedendo tutto questo per la prima volta!"


translate italian epilogue_uv_part2_60491530:


    "Improvvisamente il walkie-talkie che mi portavo appresso ha fatto un suono sgradevole, e la voce agitata di Olga Dmitrievna mi ha detto:"


translate italian epilogue_uv_part2_7e5a4c32:


    mt "Semyon, mi ricevi? Semyon!"


translate italian epilogue_uv_part2_e81bfa10:


    "Dopo un istante ho premuto il pulsante e ho risposto."


translate italian epilogue_uv_part2_fc30fd2b:


    me "Sì."


translate italian epilogue_uv_part2_027e452f:


    mt "Semyon, stanno accadendo così tante cose! Torna subito a vedere!"


translate italian epilogue_uv_part2_d64c8037:


    me "Che cosa? Di cosa si tratta? Che sta succedendo?"


translate italian epilogue_uv_part2_39ad62fd:


    "Ho chiesto senza troppo interesse."


translate italian epilogue_uv_part2_3e4d60db:


    "La leader del Campo, i pionieri, l'intero Campo: me li sono lasciati alle spalle, non solo una decina di chilometri, ma molti di più."


translate italian epilogue_uv_part2_ce29470a:


    "Tutto ciò che è accaduto in questi ultimi giorni ha perso ogni significato, come un incubo che si inizia a dimenticare."


translate italian epilogue_uv_part2_095e79da:


    "E adesso, quando c'era un vero mondo davanti ai miei occhi…{w} Non avevo altra scelta se non quella di credere che una volta raggiunta quella città, tutto sarebbe finito."


translate italian epilogue_uv_part2_9f2bee1d:


    mt "Semyon, sbrigati! Non c'è tempo per spiegare! È un disastro…"


translate italian epilogue_uv_part2_63619bef:


    "L'interferenza è aumentata, soffocando la voce della leader del Campo."


translate italian epilogue_uv_part2_e46e5542:


    uv "Ci andrai?"


translate italian epilogue_uv_part2_dbcf576f:


    "Ha chiesto Yulya, agitata."


translate italian epilogue_uv_part2_b6d2279b:


    me "Dove? Indietro? No, perché dovrei?"


translate italian epilogue_uv_part2_f27606ef:


    uv "Ma…"


translate italian epilogue_uv_part2_44fa2fc7:


    me "Vuoi che io torni indietro?"


translate italian epilogue_uv_part2_1b462d8f:


    uv "Non lo so. Tu cosa vuoi?"


translate italian epilogue_uv_part2_0e3de8d0:


    "Davvero. Che cosa voglio?"


translate italian epilogue_uv_part2_4efd7251:


    "Arrivare in quella città – quello che stavo cercando di fare per l'intera giornata? O forse si trattava di qualcosa di più generale – uscire da questo mondo?"


translate italian epilogue_uv_part2_79ceb492:


    "E se fosse successo davvero qualcosa nel Campo?"


translate italian epilogue_uv_part2_75a77f12:


    "Quel pensiero mi ha attraversato dolorosamente la testa."


translate italian epilogue_uv_part2_cf213262:


    "Potrebbe essere successo qualcosa a Slavya, Lena, Ulyana, e Alisa…"


translate italian epilogue_uv_part2_6f66ce9d:


    "E perché la domanda «che me ne importa?» non mi stava tormentando?"


translate italian epilogue_uv_part2_5163e5b3:


    "Ma – sì. Ci tengo davvero!"


translate italian epilogue_uv_part2_48ba4ca4:


    "Ultimamente mi sono legato molto a loro, mi sono abituato troppo alla vita nel Campo!"


translate italian epilogue_uv_part2_3aad34f9:


    "Ma cosa accadrà alla città? Scomparirà improvvisamente così com'è apparsa?"


translate italian epilogue_uv_part2_39c34ac9:


    "E se fosse tutto come Yulya me l'ha descritto...?{w} No, non dovrei permetterlo."


translate italian epilogue_uv_part2_f53442e6:


    "Il walkie-talkie ha sibilato, ma non volevo spegnerlo, nel caso in cui mi avessero voluto comunicare qualcos'altro."


translate italian epilogue_uv_part2_37a46732:


    uv "Allora, che cos'hai deciso?"


translate italian epilogue_uv_part2_44b18e64:


    "Dovevo prendere una decisione."


translate italian epilogue_uv_city_959e0edc:


    "Al diavolo quel «Sovyonok»!{w} Non esiste nessun Campo, nessun pioniere!{w} E davvero non mi importa di sapere perché io sia arrivato qui. L'unica cosa che conta adesso è uscire, e in fretta!"


translate italian epilogue_uv_city_54348e42:


    me "Andrò verso la città."


translate italian epilogue_uv_city_588dbb53:


    "Ho alzato le spalle, cercando di esprimere un minimo di interesse."


translate italian epilogue_uv_city_78170aac:


    "Yulya mi ha fissato per un po', ma poi ha fatto un paio di passi in avanti, come se mi stesse invitando a seguirla."


translate italian epilogue_uv_city_2c36965d:


    uv "Beh, vieni o no?"


translate italian epilogue_uv_city_ce617998:


    "…"


translate italian epilogue_uv_city_7b1bb487:


    "Per la prima volta in tutta questa giornata mi è sembrato che ci stessimo avvicinando un po' di più a quella città."


translate italian epilogue_uv_city_fcfa0bc9:


    "Ero quasi sicuro che l'avrei finalmente raggiunta, e poi ho sentito il suono di un clacson alle mie spalle…"


translate italian epilogue_uv_city_16335221:


    "Un autobus, un Ikarus, si stava avvicinando a noi, falciando l'oscurità della notte con le sue luci."


translate italian epilogue_uv_city_c896c1bf:


    "Mi sono messo al margine della strada."


translate italian epilogue_uv_city_fd94b862:


    me "Quello è…"


translate italian epilogue_uv_city_f05fcf4f:


    "Yulya non c'era."


translate italian epilogue_uv_city_84bca6ed:


    "Come fa ad apparire e a scomparire così rapidamente?"


translate italian epilogue_uv_city_421162ac:


    "Il bus si è fermato lentamente davanti a me, la porta si è aperta, e…"


translate italian epilogue_uv_city_8e42683f:


    "...è scesa Olga Dmitrievna!"


translate italian epilogue_uv_city_58184ba9:


    mt "Semyon! Perché non sei tornato? Te l'avevo chiesto!"


translate italian epilogue_uv_city_d0bc54d8:


    me "Potrei farle la stessa domanda! Mi ha detto che era successo un disastro, e invece…"


translate italian epilogue_uv_city_5cb759d8:


    mt "Va bene…"


translate italian epilogue_uv_city_89ab1420:


    "Improvvisamente si è addolcita."


translate italian epilogue_uv_city_49846fd4:


    mt "Sali sull'autobus."


translate italian epilogue_uv_city_ff029072:


    me "Che cosa...? E per quanto riguarda la città?"


translate italian epilogue_uv_city_a770878a:


    mt "È lì che stiamo andando – per vedere con i nostri occhi!"


translate italian epilogue_uv_city_fb7ddfe9:


    "Ho esitato."


translate italian epilogue_uv_city_135f17b8:


    "Da un lato sarebbe più veloce andarci in autobus, questa strada dovrebbe portare alla città. D'altra parte, le azioni della leader del Campo mi sembrano sospette."


translate italian epilogue_uv_city_cb631dc4:


    "Stiamo andando comunque nella stessa direzione…"


translate italian epilogue_uv_city_71d3b8c8:


    me "D'accordo."


translate italian epilogue_uv_city_164e884d:


    "Ho accettato e sono salito sul bus."


translate italian epilogue_uv_city_ce617998_1:


    "…"


translate italian epilogue_uv_city_cf9891a5:


    "L'Ikarus si stava dirigendo verso la città misteriosa, sobbalzando sopra ai dossi."


translate italian epilogue_uv_city_12d134cd:


    "Secondo me stavamo andando troppo piano! Mi sentivo così stanco che i miei occhi hanno iniziato a chiudersi."


translate italian epilogue_uv_city_c8562488:


    "E Yulya?{w} L'ho abbandonata da sola laggiù?"


translate italian epilogue_uv_city_9b99b80c:


    "Sembrava proprio che tutti i pionieri del campo «Sovyonok» fossero saliti su questo bus!"


translate italian epilogue_uv_city_3d477727:


    "Forse lei mi voleva dire qualcosa…"


translate italian epilogue_uv_city_22a67c2b:


    "Tuttavia, nonostante la mia lotta contro Morfeo, il mio forte russare è risuonato dal mio sedile dopo un paio di minuti."


translate italian epilogue_uv_city_742d6e3a:


    "Finalmente è tutto finito!{w} Oppure no?"


translate italian epilogue_uv_city_ef1df980:


    "Desideravo così tanto uscire da qui, cercando di non addormentarmi."


translate italian epilogue_uv_city_77be2dc1:


    "Forse il motivo era la stanchezza?"


translate italian epilogue_uv_city_3eb6b561:


    "Probabilmente no. Era come se mi avessero somministrato un sedativo.{w} Non si può perdere conoscenza in questo modo, e io non ho mai sofferto di narcolessia!"


translate italian epilogue_uv_city_eeb3a5ac:


    "I miei pensieri scorrevano veloci e tranquilli, come un ampio fiume."


translate italian epilogue_uv_city_1aaadbd7:


    "Niente avrebbe potuto interferire con il loro flusso, perché stavo dormendo…"


translate italian epilogue_uv_city_0a064f5d:


    "Ma allo stesso tempo mi sentivo come consapevole!"


translate italian epilogue_uv_city_73eda9e0:


    "Sto per aprire gli occhi e…"


translate italian epilogue_uv_city_7027362e:


    "Un vecchio, scheggiato soffitto mi stava osservando. Una lunga crepa lo divideva in due parti, come se stesse minacciando di crollarmi addosso da un momento all'altro."


translate italian epilogue_uv_city_8c1abb55:


    "La ventola del computer ronzava tranquillamente, ma piuttosto rumorosamente. Migliaia di particelle di polvere danzavano splendidamente nell'aria."


translate italian epilogue_uv_city_d103a6b9:


    "Il vento gemeva fuori dalla finestra, leccando il vetro ricoperto di ghiaccio e poi volando via, facendo roteare nell'aria i fiocchi di neve al suo passaggio."


translate italian epilogue_uv_city_0c034f2c:


    "La fredda e luminosa luna invernale stava contemplando il {i}mio{/i} mondo, ammiccando allegramente."


translate italian epilogue_uv_city_416493f0:


    "C'era un fastidioso ronzio nella mia testa pesante, non mi sentivo molto bene. Nella bocca sentivo un retrogusto spiacevole, come se avessi ingoiato il contenuto di un posacenere e mi fossi sciacquato la bocca con della vodka."


translate italian epilogue_uv_city_fd751d18:


    "In un primo momento non me ne sono accorto."


translate italian epilogue_uv_city_ff227b7b:


    "Come se ieri stessi andando da qualche parte, e poi…"


translate italian epilogue_uv_city_6b1de54a:


    "E ho fatto uno strano sogno – l'estate, il Campo, i pionieri e la loro leader, la strana ragazza dalle orecchie di gatto, e la città in lontananza."


translate italian epilogue_uv_city_9715094f:


    "Mi dimentico sempre di come finiscono i miei sogni.{w} Ho raggiunto la città oppure no...?"


translate italian epilogue_uv_city_c814cbd9:


    "Aspetta un attimo…{w} Era davvero un sogno?"


translate italian epilogue_uv_city_5ecfb0de:


    "Ho trascorso quasi una settimana in quel Campo!"


translate italian epilogue_uv_city_aabf4b52:


    "Anche se l'orologio non era d'accordo con me – secondo le sue lancette sono passate solo dodici ore."


translate italian epilogue_uv_city_1845cce3:


    "Mi sono seduto sul letto e ho provato a ricordare…"


translate italian epilogue_uv_city_890c3f51:


    "Le mie emozioni e i miei sentimenti erano molto vividi, ma gli avvenimenti di quel sogno sembravano essere avvolti nella foschia."


translate italian epilogue_uv_city_a9378da6:


    "Mi ricordavo le ragazze, le loro lacrime e i loro sorrisi, ricordavo la gioia e la tristezza che avevamo vissuto insieme, ma i loro volti e le loro sagome erano nascoste nell'ombra."


translate italian epilogue_uv_city_079e3950:


    "E c'era una qualche miniera, una specie di rifugio antiaereo, e poi un'isola, e poi siamo partiti.{w} E quelle dannate cavallette, che disturbavano il mio sonno!"


translate italian epilogue_uv_city_4da0715a:


    "E Slavya, e Lena, e Alisa, e Ulyana, e Masha…{w} e Yulya!"


translate italian epilogue_uv_city_f2e3ea45:


    "Ma c'era qualcos'altro, qualcosa al di là del sogno. La mia vita {i}successiva{/i}, apparentemente come un nuovo livello di esistenza."


translate italian epilogue_uv_city_71c0a0b1:


    "Sì, mi ero svegliato molte volte, avevo incontrato quelle ragazze {i}da sveglio{/i} in questo mondo, e poi è ricominciato tutto da capo…"


translate italian epilogue_uv_city_f4b5c749:


    "La mia testa non era in grado di sostenere un tale carico di informazioni, la mia memoria traboccava. Ogni secondo dimenticavo qualcosa, e non riuscivo a capire cosa fosse reale e cosa non lo fosse."


translate italian epilogue_uv_city_f51c0761:


    "Tutti gli eventi del Campo si sono fusi in un'unica e spettacolare immagine – un collage costituito da centinaia di opere di centinaia di artisti con stili diversi."


translate italian epilogue_uv_city_4455d2a3:


    "Frammenti di sentimenti, ombre di emozioni, parti di ricordi. La mia vita era stata fatta a pezzi, e poi quei pezzi sono stati ricuciti maldestramente, cosicché ora essa era esattamente come quella bambola trasandata che avevo trovato al vecchio campo assieme a Ulyana.{w} Aspetta… o forse era Slavya?!"


translate italian epilogue_uv_city_dd9d01a3:


    "La notte in cui eravamo andati in cerca di Shurik mi è balenata dinanzi agli occhi in ogni suo dettaglio, ma non riuscivo proprio a ricordare il volto della ragazza che stava con me in quel momento."


translate italian epilogue_uv_city_d15cc5f5:


    "{i}Il sogno{/i} stava diventando sempre più annebbiato ogni secondo che passava. Forse presto sarà tutto dimenticato?{w} Non posso permettere che ciò accada! Devo ricordare!"


translate italian epilogue_uv_city_0a28bc9e:


    "Anche senza i dettagli, anche senza i volti, magari anche senza i nomi…"


translate italian epilogue_uv_city_c64f8bb5:


    "Sono sicuro che non si è trattato solo di un sogno!{w} Era qualcosa di più grande, qualcosa di molto importante per me…"


translate italian epilogue_uv_city_ece79837:


    "Una voce ha iniziato a risuonare nella mia testa ma non era la mia, era di qualcun altro."


translate italian epilogue_uv_city_7d1637b6:


    "All'inizio era silenziosa e distante, ma poi si è fatta sempre più forte, e ben presto riuscivo a distinguere le parole."


translate italian epilogue_uv_city_6c11e880:


    "Ma probabilmente era solo la mia immaginazione – un semplice impulso nervoso mentale contiene più pensieri di migliaia di libri!"


translate italian epilogue_uv_city_18d6a6f0:


    "Ma ricordo di aver lasciato qualcosa di incompiuto in quel sogno."


translate italian epilogue_uv_city_a59d3cd1:


    "La città?{w} No, l'avevo raggiunta."


translate italian epilogue_uv_city_e19d7211:


    "Yulya!"


translate italian epilogue_uv_city_a529acd3:


    "Dal profondo della mia coscienza è venuta a galla l'immagine di una fanciulla-gatto. Mi stava fissando con rabbia, muovendo le orecchie in modo divertente."


translate italian epilogue_uv_city_8ec1f570:


    "Forse non è stato un sogno...? La settimana che ho trascorso nel Campo forse non è stata un sogno?!"


translate italian epilogue_uv_city_7f69f2b6:


    "Ho camminato attorno alla stanza, guardando fuori dalla finestra. Il mondo esterno era lo stesso di {i}ieri{/i}."


translate italian epilogue_uv_city_f65a8fd8:


    "Ero sul punto di impazzire!"


translate italian epilogue_uv_city_e729f7fe:


    "Il silenzio del mio appartamento è stato interrotto dal suono del campanello."


translate italian epilogue_uv_city_52f1355e:


    "Chi potrebbe essere a quest'ora?!"


translate italian epilogue_uv_city_d1f773e3:


    "All'inizio non volevo aprire la porta. Ero persino preoccupato, ma poi ho pensato che fosse una buona occasione per verificare – forse anche {i}questo{/i} era un sogno?"


translate italian epilogue_uv_city_6dcbff83:


    "Le persone nei sogni e nella realtà sono completamente diverse!"


translate italian epilogue_uv_city_e05fd94e:


    "Aprirò la porta e lo scoprirò!"


translate italian epilogue_uv_city_118a9de7:


    "Ho raggiunto la porta d'ingresso in soli due balzi, ho tirato la maniglia senza nemmeno dare un'occhiata allo spioncino, e…"


translate italian epilogue_uv_city_cc41fdb9:


    "Le ragazze stavano davanti a me…{w} Le ragazze del Campo!"


translate italian epilogue_uv_city_91ccfacc:


    "Ma qualcosa era cambiato in loro."


translate italian epilogue_uv_city_e2eac827:


    "Non ricordavo esattamente ogni loro dettaglio di come apparivano nel sogno. Tuttavia, l'acconciatura di Slavya…{w} E Ulyana sembrava più alta."


translate italian epilogue_uv_city_85b6f90b:


    "Per un attimo un pesante silenzio si è posato su di noi. Ero pietrificato, i miei pensieri erano congelati, non riuscivo a percepire alcun timore o qualsiasi altra emozione."


translate italian epilogue_uv_city_c9f8a031:


    "Le ragazze non sembravano affatto sorprese…"


translate italian epilogue_uv_city_51149e68:


    me "…"


translate italian epilogue_uv_city_9104ac12:


    sl "Ciao!"


translate italian epilogue_uv_city_f64f0819:


    "Ha detto Slavya, allegramente."


translate italian epilogue_uv_city_aa88bb70:


    us "Perché te ne stai lì imbambolato?!"


translate italian epilogue_uv_city_517f79f3:


    "Ha esclamato Ulyana."


translate italian epilogue_uv_city_0296ee39:


    me "Ma come avete…"


translate italian epilogue_uv_city_3109432e:


    "Sono riuscito a pronunciare qualche parola, ma senza riuscire a formulare una domanda sensata."


translate italian epilogue_uv_city_0a8eba07:


    "Avevo talmente tante cose da chiedere!"


translate italian epilogue_uv_city_2e4ca1c2:


    un "Calmatevi, ragazze. Non si dovrebbe spaventare una persona in questo modo!"


translate italian epilogue_uv_city_58da8a51:


    "Mi sono ricordato di Lena, della nostra vita dopo il Campo…"


translate italian epilogue_uv_city_7418a938:


    "E di Slavya, di quella nostra conversazione notturna alla fermata."


translate italian epilogue_uv_city_34324030:


    "E di Ulyana. Quel nostro incontro all'università."


translate italian epilogue_uv_city_4135852b:


    "E di Alisa che era venuta al mio concerto."


translate italian epilogue_uv_city_68c476d7:


    "E persino Masha in quell'altro mondo."


translate italian epilogue_uv_city_e64bad3c:


    "Ma come...?"


translate italian epilogue_uv_city_a5ba39bf:


    dv "Beh, proprio così. Per farla breve…"


translate italian epilogue_uv_city_368e3ebe:


    "Alisa ha iniziato a parlare con disinvoltura."


translate italian epilogue_uv_city_bf9f2b06:


    dv "Non essere così sorpreso. Pensavi di essere l'unico...?"


translate italian epilogue_uv_city_ff147745:


    ma "Al campo «Sovyonok»."


translate italian epilogue_uv_city_e9b81946:


    dv "Sì, sì, al «Sovyonok». Non eri l'unico!"


translate italian epilogue_uv_city_9a17ab72:


    us "C'eravamo anche noi!"


translate italian epilogue_uv_city_3465113b:


    me "Ma come avete fatto… tutte insieme, qui…"


translate italian epilogue_uv_city_c643907f:


    sl "Il punto è che ognuna di noi ha avuto il proprio Campo, e tu hai avuto il tuo. Voglio dire, nei nostri campi ti comportavi in modo… diverso."


translate italian epilogue_uv_city_5307db81:


    "Slavya sembrava imbarazzata."


translate italian epilogue_uv_city_e63f8ffa:


    us "Totalmente diverso! Mi hai inseguito con quella torta, sono riuscita a scappare per un pelo!"


translate italian epilogue_uv_city_8512ffc9:


    ma "Ci siamo ricordate di tutto questo ancora prima di te, e abbiamo capito che non erano dei semplici sogni."


translate italian epilogue_uv_city_0fb77060:


    me "Aspettate un momento, ma com'è possibile… È passato solo un giorno."


translate italian epilogue_uv_city_def4bb08:


    un "No, è andata avanti a lungo. In un primo momento lo pensavo anch'io, di essere l'unica ad aver sognato quel Campo, ma poi la gente da {i}quel luogo{/i} ha iniziato a diventare sempre più reale. E anche tu…"


translate italian epilogue_uv_city_9fe29739:


    "Ha fatto un profondo sospiro."


translate italian epilogue_uv_city_d14e992a:


    un "E quelle cose terribili…"


translate italian epilogue_uv_city_d04d005c:


    "Sembrava che Lena fosse sul punto di scoppiare a piangere da un momento all'altro."


translate italian epilogue_uv_city_58742dd8:


    dv "Smettila! Te l'ho già ripetuto migliaia di volte che non era lui, era un altro Semyon!"


translate italian epilogue_uv_city_1c47284d:


    me "Cosa vuoi dire, «un altro Semyon»...?"


translate italian epilogue_uv_city_1ace01ff:


    sl "Vedi, esistono molti sogni così, molti Campi, ognuno di noi ne ha uno personale. E in ciascuno di essi tu ti comporti in modo diverso. Da qualche parte è ancora così..."


translate italian epilogue_uv_city_57819faa:


    "Ha guardato Lena con sguardo triste e ha proseguito:"


translate italian epilogue_uv_city_d2a2bfc1:


    sl "Molte delle tue incarnazioni ci hanno raccontato di altri Campi e delle altre versioni di noi… Ma là eravamo solo dei pupazzi, che giocavano dei ruoli predefiniti."


translate italian epilogue_uv_city_37bdf439:


    us "Io non sono un pupazzo!"


translate italian epilogue_uv_city_1678d751:


    "Ulyana era indignata."


translate italian epilogue_uv_city_2800154c:


    dv "E ovviamente ci eravamo stancate di tutto questo! Abbiamo iniziato a cercare una via d'uscita, e una volta sono riuscita ad entrare nel mondo di Miku."


translate italian epilogue_uv_city_f109e0d1:


    ma "Non chiamarmi così!"


translate italian epilogue_uv_city_42898380:


    me "Ma… Ricordo che… il Campo era completamente diverso lì, stavamo girando un film e…"


translate italian epilogue_uv_city_1b96ba71:


    ma "Che differenza fa? In ogni caso il sogno si ripeteva."


translate italian epilogue_uv_city_51149e68_1:


    me "…"


translate italian epilogue_uv_city_a5cd4aee:


    dv "Beh, poi siamo riuscite a collegare i mondi restanti in un mondo unico per tutte noi cinque. E a quel punto è stato un gioco da ragazzi!"


translate italian epilogue_uv_city_ab0f8bf4:


    "Alisa ha sorriso, e ho riconosciuto subito quel sorriso."


translate italian epilogue_uv_city_87568219:


    sl "L'unica cosa che ci rimaneva da fare era trovare te."


translate italian epilogue_uv_city_b4cf4929:


    me "Trovare me? E perché?"


translate italian epilogue_uv_city_988a30f2:


    "No, certo che non mi dispiace!"


translate italian epilogue_uv_city_43800bc3:


    "Finalmente tutto si è messo a posto! «Sovyonok», i pionieri, la leader del Campo, queste ragazze…"


translate italian epilogue_uv_city_d37fcc88:


    me "E invece Yulya?"


translate italian epilogue_uv_city_71927170:


    un "Chi?"


translate italian epilogue_uv_city_f9f2a365:


    me "Yulya, la ragazza-gatto. Non l'avete vista?"


translate italian epilogue_uv_city_2bdb69db:


    sl "Sì, ne ho sentito parlare…"


translate italian epilogue_uv_city_e3f80252:


    me "Ma come…?"


translate italian epilogue_uv_city_d69ea088:


    "Mi sentivo ancora in colpa per aver abbandonato Yulya."


translate italian epilogue_uv_city_09622045:


    dv "È così che stanno le cose. Ognuna di noi aveva un solo mondo tutto suo, mentre tu ne avevi tanti."


translate italian epilogue_uv_city_367090df:


    sl "E poi abbiamo capito che l'unico ad essere connesso con la realtà eri tu."


translate italian epilogue_uv_city_e0a04c2e:


    me "E come l'avete capito?"


translate italian epilogue_uv_city_e29b42f5:


    ma "Ce l'ha detto il pioniere. Voglio dire, il «te stesso» che affermava di essere stato in quel sogno per lungo tempo."


translate italian epilogue_uv_city_d180f7cd:


    us "Un qualche pazzoide! Probabilmente era andato totalmente fuori di testa!"


translate italian epilogue_uv_city_c08f848b:


    "Non riuscivo a infilare nella mia testa tutte quelle cose che mi stavano dicendo."


translate italian epilogue_uv_city_6bdc8ef5:


    me "Ok, aspettate…"


translate italian epilogue_uv_city_670ed473:


    "Ho cercato di mettere assieme tutti i pezzi."


translate italian epilogue_uv_city_4220c762:


    me "Allora il Campo non è stato un sogno? Tutto stava accadendo da molto tempo? Ci sono stati molti {i}campi{/i} dove eravamo tutti noi? Ma ognuna di voi ce ne aveva solo uno, mentre io invece ne ho avuti molti...? No, non riesco a capire."


translate italian epilogue_uv_city_0e688c6f:


    un "Più o meno è così."


translate italian epilogue_uv_city_1f90b603:


    us "E basta mettere il broncio. Sei così noioso! È stato molto divertente! Ad esempio, non avevo mai visitato un campo di pionieri! Probabilmente neppure tu."


translate italian epilogue_uv_city_156e1688:


    me "E chi era dietro a tutto questo? E perché?"


translate italian epilogue_uv_city_b1c7cc7f:


    sl "È per questo che siamo venute qui da te – per scoprirlo."


translate italian epilogue_uv_city_0eeb4092:


    me "Ma come avete fatto a trovarmi?"


translate italian epilogue_uv_city_a5ee8aa0:


    "All'improvviso sono stato assalito dai dubbi, e persino dalla paura."


translate italian epilogue_uv_city_85f93595:


    "Ulyana mi ha dato un pizzicotto."


translate italian epilogue_uv_city_38b5e0e1:


    me "Ahia!"


translate italian epilogue_uv_city_47abcbd1:


    us "Adesso sai che non si tratta di un sogno!"


translate italian epilogue_uv_city_50ea836a:


    ma "Non è stato difficile."


translate italian epilogue_uv_city_6b8e3cb6:


    dv "I tuoi cloni sapevano essere piuttosto loquaci, se volevano."


translate italian epilogue_uv_city_0ad194ed:


    un "O se li costringevi a parlare…"


translate italian epilogue_uv_city_6be3f064:


    "Ha detto Lena, quietamente."


translate italian epilogue_uv_city_a67950e2:


    sl "E così, prima ci siamo incontrate nella realtà, e poi siamo venute da te."


translate italian epilogue_uv_city_778a81fa:


    us "Accogli i tuoi ospiti!"


translate italian epilogue_uv_city_76517ead:


    me "Incredibile…"


translate italian epilogue_uv_city_22a5a4b0:


    "La mia vita si è stravolta in un batter d'occhio."


translate italian epilogue_uv_city_5d555141:


    "Ovviamente non ricordavo quel sogno in ogni suo dettaglio – o quei sogni, come detto dalle ragazze – ma anch'io avevo la sensazione che quel Campo fosse qualcosa di più complesso."


translate italian epilogue_uv_city_056a21ff:


    "Esso ci ha riuniti, prima {i}là{/i} e ora {i}qui{/i}."


translate italian epilogue_uv_city_5573410f:


    "Rimaneva solo la questione di Yulya.{w} Ma potrei mai conoscere la risposta?{w} Forse posso semplicemente dimenticarmi di lei, così come mi sono dimenticato di tutto il resto."


translate italian epilogue_uv_city_087cd72d:


    me "Va bene. Spero che mi raccontiate ogni cosa in dettaglio."


translate italian epilogue_uv_city_49cc0ae4:


    sl "Certo!"


translate italian epilogue_uv_city_cfc15b56:


    dv "Siamo venute qui per questo."


translate italian epilogue_uv_city_5034671b:


    me "Dai, entrate. Non state lì sulla soglia…"


translate italian epilogue_uv_city_d5bf3b5e:


    me "Tè, caffè, balliamo?"


translate italian epilogue_uv_city_0dd900f4:


    "Ho aggiunto, cercando di calmarmi e di riprendermi."


translate italian epilogue_uv_city_01827c2a:


    "Lena mi ha fissato in modo spiacevole."


translate italian epilogue_uv_city_a722bd37:


    me "Va bene, va bene, danzeremo più tardi…"


translate italian epilogue_uv_city_d99864b1:


    "Ogni storia ha il suo inizio e la sua fine."


translate italian epilogue_uv_city_4d69c034:


    "Ogni storia ha il suo contorno, la sua sinossi, i suoi contenuti, i punti chiave, un prologo e un epilogo."


translate italian epilogue_uv_city_754a4f66:


    "E non esiste libro che, se letto nuovamente, non riesca a rivelare nuovi dettagli che non sono stati notati in precedenza."


translate italian epilogue_uv_city_39cdaa6f:


    "Ma ogni libro ha la sua ultima pagina. E dopo averla girata, dobbiamo rimettere il libro sullo scaffale."


translate italian epilogue_uv_city_611077c4:


    "Solo per aprirne uno nuovo domani…"


translate italian epilogue_uv_ulya_01822020:


    me "Va bene."


translate italian epilogue_uv_ulya_4569b623:


    "Mi sono deciso a parlare."


translate italian epilogue_uv_ulya_8f6b1a03:


    uv "Come hai detto?"


translate italian epilogue_uv_ulya_0726abef:


    "Yulya era sorpresa."


translate italian epilogue_uv_ulya_0459b6ff:


    me "Va bene, torniamo indietro.{w} Non ti aspettavi una risposta del genere?"


translate italian epilogue_uv_ulya_3aa0974f:


    "Ha sorriso furbescamente."


translate italian epilogue_uv_ulya_13afe092:


    uv "No. Perché lo pensi?"


translate italian epilogue_uv_ulya_94105ae3:


    me "Perché dici sempre che tutto si ripete dopo sette giorni, e che non potrei uscire comunque da qui…"


translate italian epilogue_uv_ulya_1a2b07d6:


    uv "Allora lo scopriremo!"


translate italian epilogue_uv_ulya_4c07374f:


    me "Oh, lo scopriremo?{w} Non dovremmo scoprire qualcosa riguardo a quella città, innanzitutto?"


translate italian epilogue_uv_ulya_b4e02fac:


    uv "Sta a te decidere."


translate italian epilogue_uv_ulya_8f838576:


    "L'ho guardata intensamente."


translate italian epilogue_uv_ulya_5b06f4d4:


    "Qualcosa sembrava essere cambiato in Yulya, ma non riuscivo a capire cosa fosse esattamente."


translate italian epilogue_uv_ulya_1c771708:


    me "Ho già deciso…"


translate italian epilogue_uv_ulya_42765c89:


    "Ho mormorato."


translate italian epilogue_uv_ulya_ba638d57:


    uv "Allora andiamo!"


translate italian epilogue_uv_ulya_1bc90a19:


    "La ragazza-gatto si è fatta strada attraverso i campi con fiducia, in direzione della foresta."


translate italian epilogue_uv_ulya_219f2350:


    me "Ehi, aspetta, dove stai andando?!"


translate italian epilogue_uv_ulya_88d9a691:


    uv "Così impiegheremo meno tempo."


translate italian epilogue_uv_ulya_bb93d8ba:


    me "Meno tempo...?"


translate italian epilogue_uv_ulya_0c925c52:


    "I pensieri hanno iniziato a vorticare furiosamente nella mia testa, fondendosi in uno solo, che probabilmente non sarebbe stato piacevole da accettare."


translate italian epilogue_uv_ulya_e9681ae3:


    me "Dunque per tutto questo tempo…"


translate italian epilogue_uv_ulya_1402ff26:


    "Sono riuscito a raggiungerla in soli due balzi e l'ho afferrata per la coda."


translate italian epilogue_uv_ulya_b96afcb5:


    me "… lo sapevi già dall'inizio?{w} Che sarei potuto arrivare qui senza il bisogno di vagare per l'intera giornata?!"


translate italian epilogue_uv_ulya_6a9c4595:


    "Yulya ha sibilato furiosamente mostrando i denti. Ho lasciato andare la sua coda."


translate italian epilogue_uv_ulya_7d489e88:


    uv "No!{w} Voglio dire, forse sì, ma non c'è niente per te laggiù!"


translate italian epilogue_uv_ulya_9af1bc42:


    me "Nella città?"


translate italian epilogue_uv_ulya_0e8e1d0f:


    uv "Sì."


translate italian epilogue_uv_ulya_5ac38cf1:


    me "Perché?"


translate italian epilogue_uv_ulya_a755a034:


    uv "Non lo so. E comunque non dovrebbe essere così. Non è giusto! Non riesco a spiegartelo – lo so e basta!"


translate italian epilogue_uv_ulya_e46208f1:


    "Mi sono voltato, osservando le luci in lontananza."


translate italian epilogue_uv_ulya_27c22bd1:


    "Mi chiedo se le cose possano andare peggio di così."


translate italian epilogue_uv_ulya_1d8e7c07:


    "Non che sia tutto terribile in questo momento, ma cosa succederà se quella città non dovesse scomparire? E cosa è successo giù al Campo? Che cosa significavano le parole di Olga Dmitrievna? Allora perché esitare, se ho già preso la mia decisione?"


translate italian epilogue_uv_ulya_e744e646:


    me "Va bene, andiamo…"


translate italian epilogue_uv_ulya_ce617998:


    "…"


translate italian epilogue_uv_ulya_164351d0:


    "Abbiamo camminato attraverso la foresta per quasi tutto il tragitto, procedendo occasionalmente lungo le pianure."


translate italian epilogue_uv_ulya_cadcee8c:


    "Yulya sembrava estremamente soddisfatta di qualcosa."


translate italian epilogue_uv_ulya_d7695174:


    me "Mi vorresti spiegare che ti sta succedendo?"


translate italian epilogue_uv_ulya_a5018e29:


    uv "È tutto così insolito."


translate italian epilogue_uv_ulya_e1c6333b:


    me "Perché prima non era così, perché tutto si ripeteva dopo sette giorni…{w} Sì, l'ho già sentito."


translate italian epilogue_uv_ulya_e6d7190e:


    "Si è limitata a sorridere."


translate italian epilogue_uv_ulya_a86bf17a:


    me "Chissà cos'è successo giù al Campo…?"


translate italian epilogue_uv_ulya_311c443d:


    uv "Guarda!"


translate italian epilogue_uv_ulya_1ac599e9:


    "Il cancello del Campo era visibile in lontananza, oltre gli alberi."


translate italian epilogue_uv_ulya_7dc8253b:


    "Abbiamo affrettato il passo."


translate italian epilogue_uv_ulya_ce617998_1:


    "…"


translate italian epilogue_uv_ulya_bc110277:


    "Era chiaro che ci fosse qualcosa di sbagliato! – «Sovyonok» sembrava abbandonato."


translate italian epilogue_uv_ulya_35d98a27:


    "Era come se i pionieri fossero andati a dormire, ma non c'era alcuna luce, ad eccezione dei lampioni. E perché Olga Dmitrievna non ci stava aspettando con la sua squadra di decontaminazione?"


translate italian epilogue_uv_ulya_f8c2ea35:


    "Prima mi aveva detto che era successo qualcosa…"


translate italian epilogue_uv_ulya_af8150cd:


    "Sono stato travolto da uno strano senso di ansia.{w} Ho guardato in direzione di Genda, e…"


translate italian epilogue_uv_ulya_f10b5d1e:


    me "La città! La città è scomparsa!"


translate italian epilogue_uv_ulya_3dcb7711:


    "Anche Yulya sembrava sorpresa, sorpresa per davvero. Quindi non l'ho sommersa di domande."


translate italian epilogue_uv_ulya_0b9c8da0:


    "Dopo aver raggiunto la casetta della leader del Campo, mi sono fermato esitando un istante."


translate italian epilogue_uv_ulya_187b5819:


    "Davanti ai miei occhi scorrevano le immagini di quello che sarebbe potuto accadere."


translate italian epilogue_uv_ulya_9b3de332:


    "Tuttavia, sono riuscito a riprendermi rapidamente."


translate italian epilogue_uv_ulya_27c40073:


    me "No, tutto questo non ha alcun senso!"


translate italian epilogue_uv_ulya_d4106a37:


    "Non c'era niente che suggerisse che questa storia di fantascienza si fosse trasformata all'improvviso in un thriller sanguinario."


translate italian epilogue_uv_ulya_14081a40:


    "Ho tirato la maniglia della porta con determinazione e sono entrato nella stanza."


translate italian epilogue_uv_ulya_4b142943:


    "Non c'era nessuno al suo interno…"


translate italian epilogue_uv_ulya_990400df:


    "Beh, non ha senso restare qui allora!"


translate italian epilogue_uv_ulya_1b3b6813:


    "Mi sono precipitato verso la casetta di Ulyana e Alisa."


translate italian epilogue_uv_ulya_c80ffad8:


    "Senza pensarci due volte ho bussato alla porta…"


translate italian epilogue_uv_ulya_7254fc56:


    "Ma solo il silenzio mi ha risposto."


translate italian epilogue_uv_ulya_01d1d520:


    "Ho girato intorno alla casetta e ho sbirciato dalla finestra."


translate italian epilogue_uv_ulya_b0b2c18b:


    "Dentro era vuota."


translate italian epilogue_uv_ulya_c18e9490:


    "Beh, questo non significa nulla…{w} Potrebbero essere andate da qualche parte!"


translate italian epilogue_uv_ulya_4921445e:


    "Sono corso alla mensa."


translate italian epilogue_uv_ulya_0633fc4a:


    "Forse hanno deciso di cenare più tardi..."


translate italian epilogue_uv_ulya_d1faa6ec:


    "Ma la porta era chiusa a chiave."


translate italian epilogue_uv_ulya_7f2ca87a:


    "Sono tornato alla piazza e mi sono lasciato cadere su una panchina, esausto."


translate italian epilogue_uv_ulya_6e6530d1:


    uv "Forse è meglio fermarsi?"


translate italian epilogue_uv_ulya_522d9099:


    "Yulya, che mi aveva seguito in silenzio, mi ha chiesto disinteressata."


translate italian epilogue_uv_ulya_e3b9d717:


    "Sì, è vero. Perché mi sono fatto prendere dal panico...?"


translate italian epilogue_uv_ulya_fc975872:


    "Ma se invece fosse davvero successo qualcosa…?"


translate italian epilogue_uv_ulya_5d937815:


    uv "Beh, io ho fame. Vado a cercare qualcosa da mangiare."


translate italian epilogue_uv_ulya_dc61ff3f:


    "Non mi sono opposto, e in un istante Yulya è sparita silenziosamente nella notte."


translate italian epilogue_uv_ulya_92c8e965:


    "O è qualche loro stupido scherzo (cosa piuttosto improbabile), o tutti sono davvero scomparsi da qualche parte!"


translate italian epilogue_uv_ulya_f6778e37:


    "L'evento in sé non è affatto più strano del mio arrivo qui, o della misteriosa città che è apparsa e poi svanita, ma…"


translate italian epilogue_uv_ulya_4fef03bd:


    "Ormai ero abituato a questo Campo e a tutti i suoi abitanti. E ora, un tale colpo di scena..."


translate italian epilogue_uv_ulya_dacde494:


    "Avevo già iniziato ad accettare queste nuove circostanze, ma adesso tutto è stato stravolto nuovamente!"


translate italian epilogue_uv_ulya_70a36eec:


    "Ho imprecato, coprendomi il volto con le mani."


translate italian epilogue_uv_ulya_98d346b6:


    "Probabilmente sarei rimasto seduto lì per diversi minuti finché le lacrime non avrebbero iniziato a scorrere, ma…"


translate italian epilogue_uv_ulya_14c6fc57:


    "Un rumore assordante ha quasi frantumato i miei timpani.{w} Sembrava un tuono."


translate italian epilogue_uv_ulya_f0593949:


    "Ma da dove è venuto? Non c'era una singola nuvola nel cielo."


translate italian epilogue_uv_ulya_d7f3983d:


    "Ho aperto gli occhi e ho visto qualcuno di fronte a me."


translate italian epilogue_uv_ulya_7561217b:


    me "Tu… tu…"


translate italian epilogue_uv_ulya_c0870411:


    "Ho pronunciato, balbettando."


translate italian epilogue_uv_ulya_8e73390d:


    "Una tempesta stava incombendo nel cielo terso, dei lampi hanno oscurato il volto dell'uomo che stava in piedi dinanzi a me."


translate italian epilogue_uv_ulya_52297f6e:


    pi "Perché quella faccia triste, Semyon?"


translate italian epilogue_uv_ulya_d4875625:


    me "Chi sei tu?"


translate italian epilogue_uv_ulya_a5efbe41:


    "Ho urlato."


translate italian epilogue_uv_ulya_ae1f4d5e:


    pi "Calmati, non c'è bisogno di innervosirsi. I neuroni non si rigenerano, sai."


translate italian epilogue_uv_ulya_163770ab:


    "Il mio primo istinto è stato quello di fuggire, ma il mio corpo non ha obbedito al comando."


translate italian epilogue_uv_ulya_8aa5c206:


    "I lampi stavano ancora imperversando, e non vi era alcuna possibilità di vedere con chiarezza il volto di quel pioniere."


translate italian epilogue_uv_ulya_a4822e0d:


    "Tuttavia, ero sicuro di aver già sentito quella voce da qualche parte."


translate italian epilogue_uv_ulya_0b34c373:


    me "Chi sei?"


translate italian epilogue_uv_ulya_ca57930f:


    "Ho chiesto in tono più calmo."


translate italian epilogue_uv_ulya_06a6a402:


    pi "Non hai forse delle domande più appropriate?"


translate italian epilogue_uv_ulya_20e14b1f:


    me "Dove sono spariti tutti?"


translate italian epilogue_uv_ulya_7e18cb0c:


    pi "Hanno giocato la loro parte, ora non abbiamo più bisogno di loro."


translate italian epilogue_uv_ulya_7b84c73a:


    me "Parte? Quale parte?"


translate italian epilogue_uv_ulya_32e5b62e:


    pi "La loro parte."


translate italian epilogue_uv_ulya_71a264d4:


    me "E la città?"


translate italian epilogue_uv_ulya_387afb71:


    pi "Quale città?"


translate italian epilogue_uv_ulya_da03225b:


    "Mi è sembrato di sentire una nota di sorpresa nella sua voce, o forse era solo la mia immaginazione."


translate italian epilogue_uv_ulya_1cb27603:


    me "La città. Laggiù!"


translate italian epilogue_uv_ulya_6546a605:


    "Ho agitato la mano in direzione di Genda."


translate italian epilogue_uv_ulya_88b778cd:


    pi "Ah, stai parlando di…"


translate italian epilogue_uv_ulya_6f2d5bcd:


    "Si è fermato per un istante."


translate italian epilogue_uv_ulya_53131bc2:


    pi "È colpa tua!"


translate italian epilogue_uv_ulya_d3bd0899:


    me "Non capisco."


translate italian epilogue_uv_ulya_b8796610:


    pi "È perché non ci stai provando abbastanza!"


translate italian epilogue_uv_ulya_fa68beb0:


    "Il pioniere ha alzato la voce."


translate italian epilogue_uv_ulya_d7e55f45:


    me "Io… Io…"


translate italian epilogue_uv_ulya_a98feb47:


    pi "Prova a pensare! Pensaci! Da quanto tempo sei qui? Cinque giorni? Sei? O forse ventisei?"


translate italian epilogue_uv_ulya_e0eaa37b:


    me "Sei… probabilmente…"


translate italian epilogue_uv_ulya_1190dcda:


    pi "Sei? Perché sei? Sei sicuro che siano sei? Perché ne sei convinto? Perché hai contato le albe e i tramonti? O perché hai guardato la data sul tuo cellulare?"


translate italian epilogue_uv_ulya_7479b07e:


    me "Non lo so…"


translate italian epilogue_uv_ulya_aa8531fb:


    "Il pioniere ha iniziato a perdere le staffe. Non avevo idea di cosa stesse parlando."


translate italian epilogue_uv_ulya_9e98cfe6:


    pi "Sei… Va bene, facciamo sei."


translate italian epilogue_uv_ulya_62a72b45:


    "Ha detto con calma."


translate italian epilogue_uv_ulya_c747fdad:


    pi "E non sei mai stato qui prima d'ora?"


translate italian epilogue_uv_ulya_5cb822ee:


    me "No…"


translate italian epilogue_uv_ulya_94259d94:


    pi "Ne sei proprio sicuro?"


translate italian epilogue_uv_ulya_7479b07e_1:


    me "Non lo so…"


translate italian epilogue_uv_ulya_24167162:


    pi "E se ti dicessi che sei stato già qui?"


translate italian epilogue_uv_ulya_016bd72c:


    "Non avevo assolutamente idea di come reagire alle sue parole. Qualcosa mi stava accadendo, com'è stato al primo giorno, qualcosa di inspiegabile, di terribile, e forse anche pericoloso."


translate italian epilogue_uv_ulya_6656d4b0:


    me "Ma non ricordo…"


translate italian epilogue_uv_ulya_3ab912f7:


    pi "A che ti serve ricordare?"


translate italian epilogue_uv_ulya_695225db:


    "Ha taciuto, sembrava perso nei suoi pensieri."


translate italian epilogue_uv_ulya_76244013:


    pi "E così, non ricordi davvero. Beh, te lo dirò io allora."


translate italian epilogue_uv_ulya_6a29747a:


    "Mi riusciva difficile distinguere le sue parole tra i tuoni della tempesta."


translate italian epilogue_uv_ulya_2d849aac:


    "I miei occhi stavano lacrimando a causa del bagliore dei lampi, ma avevo timore persino a sbattere le palpebre, per evitare che quel pioniere potesse scomparire in un istante."


translate italian epilogue_uv_ulya_c440a75f:


    "Forse mi dirà davvero come sono arrivato qui."


translate italian epilogue_uv_ulya_b17892d5:


    pi "Non è la prima volta che sei qui. Non so quanti cicli tu abbia fatto, ma non importa."


translate italian epilogue_uv_ulya_e453e3db:


    me "Ma perché? Io non ricordo niente."


translate italian epilogue_uv_ulya_c23e302b:


    pi "Perché non dovresti ricordare! Non dovresti!"


translate italian epilogue_uv_ulya_419b652c:


    "Ha gridato."


translate italian epilogue_uv_ulya_44b2f075:


    me "Scusa…"


translate italian epilogue_uv_ulya_10dc660b:


    "Ho deciso di non interromperlo."


translate italian epilogue_uv_ulya_4d99f82b:


    pi "Già, non è la tua prima volta qui…"


translate italian epilogue_uv_ulya_f48ef569:


    "Ha continuato in tono più calmo."


translate italian epilogue_uv_ulya_2a0f20ef:


    pi "Ma questa volta è speciale. Sarà l'ultima per te."


translate italian epilogue_uv_ulya_89fb5012:


    "Non riuscivo a capire cosa volesse dire con «la mia ultima volta»."


translate italian epilogue_uv_ulya_be736c58:


    "Ciò significa forse che riuscirò ad andarmene da qui e che tornerò alla mia vita normale? Oppure vuol dire che morirò qui...?"


translate italian epilogue_uv_ulya_6563fe0f:


    pi "Beh, è solo la mia opinione."


translate italian epilogue_uv_ulya_b2744c5a:


    "È seguito un lungo silenzio."


translate italian epilogue_uv_ulya_57996fa1:


    "Alla fine ho deciso di fargli una domanda."


translate italian epilogue_uv_ulya_b45a54a4:


    me "Come sai tutto questo?"


translate italian epilogue_uv_ulya_ea18c8d8:


    pi "Come? Diciamo che io sono un tuo fratello di sventura. E…"


translate italian epilogue_uv_ulya_3f1ecfd9:


    "Un'esplosione ha tuonato nelle vicinanze. Sembrava che un fulmine avesse colpito un albero ad appena una decina di metri di distanza da me."


translate italian epilogue_uv_ulya_ab12a71f:


    "Mi sono coperto istintivamente la testa con le mani e sono caduto sotto alla panchina."


translate italian epilogue_uv_ulya_fe32bb4a:


    "Non saprei dire per quanto tempo io fossi rimasto nascosto lì, ma quando mi sono ripreso un po' dallo shock i tuoni si erano già placati."


translate italian epilogue_uv_ulya_5deace4a:


    "Ho aperto gli occhi…"


translate italian epilogue_uv_ulya_b956f2ed:


    "Al posto del pioniere ora c'era Yulya."


translate italian epilogue_uv_ulya_8c9a0417:


    uv "E così hai incontrato il tuo collega."


translate italian epilogue_uv_ulya_9ef50886:


    "Mi ha fatto un sorriso, porgendomi la mano.{w} Ero scioccato e non sapevo cosa fare, così sono rimasto lì sotto."


translate italian epilogue_uv_ulya_22e36827:


    uv "Per quanto tempo hai intenzione di restare lì?"


translate italian epilogue_uv_ulya_b4517927:


    "Mi sono rialzato in piedi con difficoltà."


translate italian epilogue_uv_ulya_66d35f4b:


    me "Chi era quello?"


translate italian epilogue_uv_ulya_e4fd6319:


    "Se proprio fossi costretto a scegliere il male minore, allora preferisco fidarmi di Yulya piuttosto che di quel misterioso pioniere."


translate italian epilogue_uv_ulya_b558a3fc:


    uv "Beh, come dire…"


translate italian epilogue_uv_ulya_60f3d758:


    "Si è persa nei suoi pensieri."


translate italian epilogue_uv_ulya_7f1538dd:


    uv "Sai, non sei l'unico. Ad essere più precisi, sei l'unico qui, ma… Ce ne sono molti altri in questo Campo. Davvero tanti. E ci sono anche più copie di te stesso. Sono in molti anche loro."


translate italian epilogue_uv_ulya_1a2e4c0f:


    "Non ho capito niente."


translate italian epilogue_uv_ulya_2560e52e:


    uv "Alcuni di loro possono apparire in altri luoghi. Come quel pioniere, ad esempio."


translate italian epilogue_uv_ulya_b888bb40:


    me "Quindi tu sapevi tutto, ma non me l'hai detto?"


translate italian epilogue_uv_ulya_eab3261b:


    uv "Non me l'hai chiesto."


translate italian epilogue_uv_ulya_b53b2a58:


    "Ha alzato le spalle."


translate italian epilogue_uv_ulya_23e3138a:


    me "Dovrei avere paura di lui?"


translate italian epilogue_uv_ulya_2feb68a6:


    uv "Non credo che dovresti… Anche se…"


translate italian epilogue_uv_ulya_1adb473f:


    me "Dovrei o no?"


translate italian epilogue_uv_ulya_221d3699:


    "La mia ansia è cresciuta."


translate italian epilogue_uv_ulya_089ed32b:


    uv "Dubito che possa prendere… forma fisica…{w} Ma devi stare attento con lui."


translate italian epilogue_uv_ulya_5b247995:


    "Mi sono state gettate addosso talmente tante informazioni che avrei necessitato quantomeno di una settimana per poterle comprendere appieno."


translate italian epilogue_uv_ulya_c8cd06b9:


    "Ma pur considerando ciò, mi sono reso conto che tutte quelle informazioni non mi davano ancora alcuna risposta."


translate italian epilogue_uv_ulya_565ca87a:


    me "Quindi ce ne sono molti come me…"


translate italian epilogue_uv_ulya_0e8e1d0f_1:


    uv "Sì."


translate italian epilogue_uv_ulya_8e1e107d:


    me "Ma in che modo sono… Come siamo arrivati qui?"


translate italian epilogue_uv_ulya_9f91c876:


    uv "Non te lo so dire con esattezza. So solo che sei proprio tu ad essere venuto qui. Per la precisione, una sola persona è venuta qui."


translate italian epilogue_uv_ulya_03009af2:


    "Adesso Yulya non sembrava più la strana ragazza dalle orecchie di gatto che spargeva zucchero tra i funghi."


translate italian epilogue_uv_ulya_84949186:


    "Era l'esatto contrario. C'era fiducia nei suoi occhi e persino una certa arroganza. E nessun segno della sua precedente ingenuità infantile."


translate italian epilogue_uv_ulya_321365d8:


    me "Allora tu chi sei?"


translate italian epilogue_uv_ulya_739f08ea:


    uv "Io?"


translate italian epilogue_uv_ulya_fc30fd2b:


    me "Sì."


translate italian epilogue_uv_ulya_1227e8ef:


    uv "A dire il vero, non ne ho idea…"


translate italian epilogue_uv_ulya_c459add4:


    "L'ho guardata a bocca aperta."


translate italian epilogue_uv_ulya_8579e67d:


    me "Come sarebbe… che non ne hai idea?"


translate italian epilogue_uv_ulya_6aad0d0b:


    uv "L'unica cosa che ricordo è che ero qui. Non ricordo altro. Come se non ci fosse nient'altro. Devo osservarti… osservare tutti voi, ed io esisto in tutti i campi contemporaneamente."


translate italian epilogue_uv_ulya_ffc4f24e:


    "Dopo essermi ripreso dallo shock, ho continuato."


translate italian epilogue_uv_ulya_5561955a:


    me "E non sai chi sei?"


translate italian epilogue_uv_ulya_4fa8ffca:


    uv "Esatto!"


translate italian epilogue_uv_ulya_ec2c2a7c:


    "Ha riso."


translate italian epilogue_uv_ulya_a6440285:


    me "Ma è impossibile! Beh, capisco l'amnesia… Ma gli esseri onnipotenti non possono soffrire di amnesia."


translate italian epilogue_uv_ulya_104bc8ca:


    uv "E chi ti ha detto che io sia onnipotente?"


translate italian epilogue_uv_ulya_d4713cb0:


    me "Non lo sei?"


translate italian epilogue_uv_ulya_079e4740:


    uv "A dire il vero non so fare niente. Non posso interferire con il flusso naturale degli eventi."


translate italian epilogue_uv_ulya_ef49cb55:


    me "Ma non sapevi forse… o avevi visto?"


translate italian epilogue_uv_ulya_5659cee4:


    uv "So qualcosina. Ho visto qualcosina…"


translate italian epilogue_uv_ulya_7530075b:


    me "E che ne sarà… Beh, sai…"


translate italian epilogue_uv_ulya_f1760393:


    "Non sapevo come formulare la domanda."


translate italian epilogue_uv_ulya_5dd4b838:


    uv "Niente. Di solito passano sette giorni e uno di voi lascia il Campo. Potrei anche dire che… scompare. Poi uno nuovo arriva e tutto si ripete… Il numero dei campi, tuttavia, resta invariato."


translate italian epilogue_uv_ulya_9ca0629f:


    me "Ne abbiamo già parlato prima.{w} Scompare… dove?"


translate italian epilogue_uv_ulya_9abf8151:


    uv "Non lo so."


translate italian epilogue_uv_ulya_fbf20f76:


    me "Ma non può essere! Mi hai già detto così tante cose, e…"


translate italian epilogue_uv_ulya_65f552e4:


    uv "Credi che io ti stia nascondendo qualcosa?"


translate italian epilogue_uv_ulya_5a7a7577:


    me "No."


translate italian epilogue_uv_ulya_50bd82b0:


    "Ho detto esausto."


translate italian epilogue_uv_ulya_aeafe187:


    "Forse non dovrei mettere in dubbio le sue parole.{w} Almeno per ora…"


translate italian epilogue_uv_ulya_c0c55507:


    me "Magari non scompaiono, ma ritornano all'inizio di un altro giro?"


translate italian epilogue_uv_ulya_0c75893a:


    uv "Potrebbe essere, ma non credo che sia così."


translate italian epilogue_uv_ulya_231065b3:


    me "E allora cosa mi sta succedendo adesso?"


translate italian epilogue_uv_ulya_bf4facf7:


    uv "Non riesco a spiegare nemmeno questo. Ma so di per certo che doveva succedere prima o poi. Ed è una cosa piuttosto insolita. Il corso degli eventi è diverso questa volta."


translate italian epilogue_uv_ulya_b37bbbdc:


    me "Anche il pioniere ha detto qualcosa di simile…"


translate italian epilogue_uv_ulya_de6bbaa6:


    uv "Probabilmente ne sa più di me."


translate italian epilogue_uv_ulya_c9bf7d2f:


    me "Ma come posso comprendere tutto questo?"


translate italian epilogue_uv_ulya_e98d5cf2:


    "Ho sospirato profondamente, coprendomi la testa con le mani."


translate italian epilogue_uv_ulya_039b4986:


    uv "Credo che lo scopriremo molto presto."


translate italian epilogue_uv_ulya_36f8874d:


    me "Ok… Ma alla fine, tu chi sei?"


translate italian epilogue_uv_ulya_dd718d6c:


    "Yulya ha messo il broncio."


translate italian epilogue_uv_ulya_e75258a6:


    uv "Te l'ho già detto!"


translate italian epilogue_uv_ulya_40dd1aca:


    me "Quindi hai delle perdite di memoria?"


translate italian epilogue_uv_ulya_710e003a:


    uv "No…"


translate italian epilogue_uv_ulya_60f3d758_1:


    "Ha pensato per un momento."


translate italian epilogue_uv_ulya_012d131e:


    uv "Probabilmente non ho una memoria. Non c'era nessun me prima di questo Campo… Io non esistevo."


translate italian epilogue_uv_ulya_a337cbd5:


    me "Ma…"


translate italian epilogue_uv_ulya_cd5a786f:


    "Non sono riuscito a finire la frase."


translate italian epilogue_uv_ulya_e545f709:


    "Considerando tutto ciò che era accaduto qui, potevo solo fare supposizioni."


translate italian epilogue_uv_ulya_43398793:


    "Il che significa che non dovrei essere sorpreso nel vedere una ragazza con le orecchie di animale."


translate italian epilogue_uv_ulya_fd6b4e40:


    me "Sei una specie di osservatore qui?"


translate italian epilogue_uv_ulya_23eb114f:


    uv "Forse…{w} Credo che sia il termine più appropriato."


translate italian epilogue_uv_ulya_55692694:


    me "E che mi dici di tutti i pionieri che sono scomparsi dopo aver giocato le loro parti?"


translate italian epilogue_uv_ulya_f9e487df:


    uv "Di cosa stai parlando?"


translate italian epilogue_uv_ulya_2a654aba:


    me "Quel pioniere ha detto che…"


translate italian epilogue_uv_ulya_645769e4:


    uv "Le loro parti…"


translate italian epilogue_uv_ulya_f5cac582:


    "Ha mosso le orecchie in modo divertente, facendomi sorridere."


translate italian epilogue_uv_ulya_73acaf44:


    uv "Sì, c'è qualcosa di vero. Perché solo tu sei reale qui… Voglio dire, tu, quel pioniere, e tutti gli altri tuoi simili."


translate italian epilogue_uv_ulya_1b32cfea:


    me "Com'è possibile?"


translate italian epilogue_uv_ulya_e2e98778:


    uv "Te l'ho detto – non lo so! Si potrebbe supporre che io sia apparsa qui con una conoscenza limitata delle cose, niente di più. Ci sono cose delle quali non sono sicura, e ci sono cose che tu conosci meglio di me."


translate italian epilogue_uv_ulya_505d91ae:


    me "Va bene…"


translate italian epilogue_uv_ulya_2520f29d:


    "In effetti le sue parole mi sembravano avere un certo senso."


translate italian epilogue_uv_ulya_7a5d2ca6:


    "Sempre che si possa parlare di qualcosa di sensato in una situazione del genere…"


translate italian epilogue_uv_ulya_644ea9c2:


    me "E cosa proponi di fare adesso?"


translate italian epilogue_uv_ulya_d348b8ed:


    uv "Di aspettare e di vedere come andranno le cose."


translate italian epilogue_uv_ulya_009e7e71:


    me "Già, non c'è modo di cambiarle…"


translate italian epilogue_uv_ulya_3e1d421a:


    "Ho mormorato."


translate italian epilogue_uv_ulya_c6510216:


    uv "Comunque, è ora di cena!"


translate italian epilogue_uv_ulya_e2e98a90:


    "Ha sorriso allegramente, agitando la coda."


translate italian epilogue_uv_ulya_9a80cd18:


    me "Non sei già andata a cercare qualcosa da mangiare?"


translate italian epilogue_uv_ulya_8634c528:


    uv "Ma non ho trovato nulla!{w} Non ne ho avuto il tempo – ho visto il lampo e sono tornata subito."


translate italian epilogue_uv_ulya_fe33debe:


    uv "Andiamo!"


translate italian epilogue_uv_ulya_badab978:


    "Si è diretta verso la mensa."


translate italian epilogue_uv_ulya_5d070f50:


    "Sarà meglio restare con lei."


translate italian epilogue_uv_ulya_7f3217c7:


    "Forse non c'è stato alcun pioniere…"


translate italian epilogue_uv_ulya_9fbc5272:


    "E se avessi la conferma di non essere l'unico in grado di vederlo, allora ci sarebbe meno spazio per i dubbi riguardo alla mia salute mentale, che ultimamente è un po' traballante."


translate italian epilogue_uv_ulya_ce617998_2:


    "…"


translate italian epilogue_uv_ulya_d74a75da:


    "Yulya si è seduta al tavolo e mi ha guardato."


translate italian epilogue_uv_ulya_3513c03c:


    uv "Quindi?"


translate italian epilogue_uv_ulya_ad02f53a:


    me "Quindi cosa?"


translate italian epilogue_uv_ulya_fe7e1a41:


    uv "La cena."


translate italian epilogue_uv_ulya_9de3289a:


    me "Sì, la cena…"


translate italian epilogue_uv_ulya_9a2e9e53:


    "Ho sbadigliato.{w} Ormai la mezzanotte era trascorsa da un pezzo."


translate italian epilogue_uv_ulya_78d7a896:


    me "A proposito, come sei riuscita ad aprire la mensa?"


translate italian epilogue_uv_ulya_195ecce2:


    uv "Bisogna avere le competenze giuste!"


translate italian epilogue_uv_ulya_1812f423:


    "Non c'era bisogno di fare ulteriori domande a riguardo."


translate italian epilogue_uv_ulya_53b7f24a:


    me "Forse è meglio se andiamo a dormire?"


translate italian epilogue_uv_ulya_11e4ef3b:


    uv "Prepara la cena. Adesso."


translate italian epilogue_uv_ulya_01ef5a6e:


    "Mi ha detto perentoriamente."


translate italian epilogue_uv_ulya_de5dd677:


    me "Io?"


translate italian epilogue_uv_ulya_d7494819:


    uv "Non vedo nessun altro qui."


translate italian epilogue_uv_ulya_c530b477:


    me "O… Ok…"


translate italian epilogue_uv_ulya_17968fb5:


    "Mi sono incamminato lentamente verso la cucina."


translate italian epilogue_uv_ulya_f36628c2:


    "A causa di tutto quel parlare di cibo mi è venuta una gran fame."


translate italian epilogue_uv_ulya_30f02899:


    "Dopo aver trovato una confezione di uova nel frigorifero, ho chiesto a Yulya:"


translate italian epilogue_uv_ulya_8908e489:


    me "Ehi, ti vanno le uova strapazzate?"


translate italian epilogue_uv_ulya_fd28a4ac:


    uv "Beh… forse."


translate italian epilogue_uv_ulya_da13cf98:


    "Ho iniziato a cucinare."


translate italian epilogue_uv_ulya_cba09001:


    "È divertente il modo in cui si sono sviluppate le cose…"


translate italian epilogue_uv_ulya_1b2b5e44:


    "Dei bizzarri e incomprensibili eventi mi stanno accadendo, e io me ne sto qui a preparare la cena.{w} Sperando che le cose possano migliorare."


translate italian epilogue_uv_ulya_3e302763:


    "Una peculiarità russa, già…"


translate italian epilogue_uv_ulya_2e254a1c:


    "Tuttavia, sono abbastanza sicuro che non accadrà nulla di brutto."


translate italian epilogue_uv_ulya_05c7e10a:


    "E se dovessero sorgere dei problemi inaspettati, Yulya mi proteggerà."


translate italian epilogue_uv_ulya_ee05990f:


    "Anche se mi ha detto di non avere grandi abilità, in un momento critico potrei certamente contare su di lei.{w} Semplicemente non c'è nessun altro su cui contare."


translate italian epilogue_uv_ulya_cd9b2003:


    "Anche se fino a ieri nulla dipendeva da me, almeno potevo avere una vaga idea di quello che stava accadendo. E invece adesso…"


translate italian epilogue_uv_ulya_8445a01a:


    "L'unica cosa rimasta da fare è sedersi comodamente sulla sedia e aspettare il finale."


translate italian epilogue_uv_ulya_4559a5ad:


    "Ho aggiunto un po' di formaggio grattugiato, della maionese, un po' di sale e pepe e ho versato nella padella quel miscuglio di ingredienti."


translate italian epilogue_uv_ulya_db55b061:


    "Ho anche trovato un salame nel frigo, e una fetta di pane sulla mensola accanto ad esso."


translate italian epilogue_uv_ulya_360e960d:


    "Ben presto ci stavamo gustando le uova strapazzate, seduti in silenzio."


translate italian epilogue_uv_ulya_3d29c745:


    uv "Delizioso!"


translate italian epilogue_uv_ulya_09de456b:


    me "Niente di speciale… Un'ordinaria cena da scapolo."


translate italian epilogue_uv_ulya_f569df04:


    uv "Non ho mai mangiato niente di simile."


translate italian epilogue_uv_ulya_666aed83:


    me "Probabilmente è meglio dei funghi e delle noci?"


translate italian epilogue_uv_ulya_7e790588:


    uv "Basta così."


translate italian epilogue_uv_ulya_6b572d3d:


    "Mi ha risposto contrariata."


translate italian epilogue_uv_ulya_5ffa82a0:


    "Dopo aver finito la nostra cena, le ho detto:"


translate italian epilogue_uv_ulya_9e70d737:


    me "Raccontami qualcosa di te… Anche se… probabilmente non hai niente da raccontare."


translate italian epilogue_uv_ulya_2661c728:


    uv "Probabilmente."


translate italian epilogue_uv_ulya_8b90ba81:


    me "Da quanto tempo sei qui?"


translate italian epilogue_uv_ulya_e0e4e466:


    uv "Non lo so. Da tanto tempo…"


translate italian epilogue_uv_ulya_16b7a3e5:


    me "Ma sei per lo più umana?"


translate italian epilogue_uv_ulya_0db604b4:


    uv "Beh, molto probabilmente!"


translate italian epilogue_uv_ulya_47949dcf:


    me "Allora come riesci ad essere in diversi luoghi contemporaneamente…?"


translate italian epilogue_uv_ulya_f1ee5506:


    uv "È difficile da spiegare, nemmeno io lo capisco. Supponiamo che io sia l'unica qui adesso di fronte a te."


translate italian epilogue_uv_ulya_ba6a7718:


    "Potevo accettare una supposizione del genere."


translate italian epilogue_uv_ulya_57f0d6d3:


    "Alla fine, anche se il fatto che Yulya avesse molte facce era un semplice mistero tra molti altri, sicuramente non era quello principale."


translate italian epilogue_uv_ulya_eef3f95b:


    "O almeno, così ho pensato."


translate italian epilogue_uv_ulya_7b8d131e:


    me "Bene, supponiamo che sia così… E quindi cosa ne sai del mondo?"


translate italian epilogue_uv_ulya_22fde84e:


    uv "So leggere e scrivere, se è quello che intendi."


translate italian epilogue_uv_ulya_ec2c2a7c_1:


    "Ha riso."


translate italian epilogue_uv_ulya_9c18fd82:


    me "No, non intendevo quello. Sai in quale regione si trova questo Campo?"


translate italian epilogue_uv_ulya_710e003a_1:


    uv "No…"


translate italian epilogue_uv_ulya_b8b45cc7:


    me "Allora forse sai che io non sono di questo luogo… non sono di questa epoca… non di questa realtà."


translate italian epilogue_uv_ulya_f754bafd:


    uv "Beh, lo so, ma non conosco i dettagli."


translate italian epilogue_uv_ulya_a05ef932:


    me "E se ti chiedessi che anno è?"


translate italian epilogue_uv_ulya_35d6ab99:


    uv "Non ti saprei rispondere."


translate italian epilogue_uv_ulya_a2964860:


    "Ho fatto un profondo respiro e ho guardato fuori dalla finestra."


translate italian epilogue_uv_ulya_df22040a:


    uv "In generale, posso dire che ci sono alcune cose che so meglio di te, e altre che so un po' meno…"


translate italian epilogue_uv_ulya_8adc0e91:


    me "E quindi cosa dovremmo fare?"


translate italian epilogue_uv_ulya_b2dad4f2:


    uv "Come ti ho già detto – dobbiamo aspettare."


translate italian epilogue_uv_ulya_d5ef5aac:


    me "Aspettare… E cosa faremo nel frattempo?"


translate italian epilogue_uv_ulya_045a7a56:


    uv "Tu cosa suggeriresti?"


translate italian epilogue_uv_ulya_89b0220f:


    me "Non saprei… Ma non offenderti se ti dico che non ho una gran voglia di arrampicarmi sugli alberi."


translate italian epilogue_uv_ulya_f8b7d908:


    uv "Ok, ok. Allora forse potresti raccontarmi qualcosa di te?"


translate italian epilogue_uv_ulya_256129cd:


    me "Non ora."


translate italian epilogue_uv_ulya_7c1ea423:


    uv "Come vuoi… Allora cosa facciamo?"


translate italian epilogue_uv_ulya_6ae279d3:


    me "Non lo so. Forse dovremmo semplicemente stare seduti e aspettare. Non avrei nulla in contrario."


translate italian epilogue_uv_ulya_64a7a78d:


    uv "Ma è così noioso!"


translate italian epilogue_uv_ulya_fc71db18:


    me "Non vedo molte alternative."


translate italian epilogue_uv_ulya_305b323e:


    uv "Forza, andiamo… dai…"


translate italian epilogue_uv_ulya_60f3d758_2:


    "Ha pensato un attimo."


translate italian epilogue_uv_ulya_088f9ff2:


    "Ho guardato Yulya intensamente."


translate italian epilogue_uv_ulya_d8abb286:


    "Sembra proprio che lei consideri divertente ogni cosa che sta accadendo."


translate italian epilogue_uv_ulya_3de491d4:


    "Inoltre, pare che la situazione attuale non le sembri affatto strana."


translate italian epilogue_uv_ulya_2d96fef7:


    "Secondo lei non c'è niente di strano in tutto questo."


translate italian epilogue_uv_ulya_f43abfa2:


    me "Sai, tutto ciò che sta accadendo non è molto normale per me. Non capisco nemmeno perché io non me ne stia nascosto sotto al mio letto, tremando ad ogni singolo rumore."


translate italian epilogue_uv_ulya_2e5a2d95:


    uv "Allora forse sei un tipo coraggioso."


translate italian epilogue_uv_ulya_6b9955f7:


    "Ho sorriso senza aggiungere altro."


translate italian epilogue_uv_ulya_cf3d8309:


    uv "Ho un'idea!"


translate italian epilogue_uv_ulya_8f838576_1:


    "L'ho guardata con interesse."


translate italian epilogue_uv_ulya_83a6f072:


    uv "No, me la sono dimenticata di nuovo…"


translate italian epilogue_uv_ulya_802fc6e1:


    "Yulya si è grattata le orecchie di gatto."


translate italian epilogue_uv_ulya_9b21e657:


    uv "Ah, sì! Ora ricordo! Andiamo a nuotare!"


translate italian epilogue_uv_ulya_e229c5a7:


    "L'ho guardata incredulo."


translate italian epilogue_uv_ulya_1c6063a9:


    me "Forse sarebbe meglio andare a dormire?"


translate italian epilogue_uv_ulya_4ef6df25:


    uv "E dai!"


translate italian epilogue_uv_ulya_a03f69df:


    "Ha messo il broncio."


translate italian epilogue_uv_ulya_b5792957:


    "In effetti la giornata è stata davvero calda, e io ero inzuppato di sudore. Sarebbe stato più facile addormentarsi con questo caldo dopo essersi lavati."


translate italian epilogue_uv_ulya_304c9144:


    me "Va bene. Andiamo."


translate italian epilogue_uv_ulya_ce617998_3:


    "…"


translate italian epilogue_uv_ulya_645846e3:


    "Pochi minuti più tardi siamo arrivati alla spiaggia."


translate italian epilogue_uv_ulya_7e0cced3:


    "Mi sono disteso pigramente sulla sabbia, guardando il fiume."


translate italian epilogue_uv_ulya_a1e9fb5f:


    "E immediatamente mi sono sentito completamente esausto."


translate italian epilogue_uv_ulya_c42573a4:


    me "Sai, in realtà non mi piace nuotare."


translate italian epilogue_uv_ulya_1b88e3de:


    uv "Cioè non sai nuotare?"


translate italian epilogue_uv_ulya_de0a7798:


    "Yulya si è seduta accanto a me."


translate italian epilogue_uv_ulya_5ea21bc9:


    me "Beh, non è che io non sappia nuotare…{w} Comunque, non importa! I pionieri – posso anche accettarlo, ma com'è potuta scomparire un'intera città?"


translate italian epilogue_uv_ulya_cee1d719:


    uv "Te l'ho già detto – non lo so.{w} Andiamo a nuotare!"


translate italian epilogue_uv_ulya_58f195ab:


    me "Non voglio…"


translate italian epilogue_uv_ulya_c07212df:


    "Non mi è mai piaciuto mostrare agli altri che sono un pessimo nuotatore."


translate italian epilogue_uv_ulya_356ba22a:


    uv "E dai! Smettila! Puoi schizzarti nell'acqua vicino alla riva!"


translate italian epilogue_uv_ulya_d577923b:


    me "Va bene… Aspetta, vado a prendere il mio costume da bagno."


translate italian epilogue_uv_ulya_066199aa:


    "Ma Yulya sembrava non ascoltarmi e…{w} ha iniziato a spogliarsi."


translate italian epilogue_uv_ulya_8f7a778e:


    me "Oh. Tu… ehhh…"


translate italian epilogue_uv_ulya_8f6b1a03_1:


    uv "Cosa?"


translate italian epilogue_uv_ulya_62b43e76:


    me "Hai intenzione di nuotare così?"


translate italian epilogue_uv_ulya_4e4ac7db:


    uv "Sì, c'è qualcosa che non va?"


translate italian epilogue_uv_ulya_2be3a365:


    "Mi sono voltato in tempo."


translate italian epilogue_uv_ulya_66bb80fd:


    me "Non ti vergogni un po'?"


translate italian epilogue_uv_ulya_addfa65b:


    "Ho cercato di parlare nel modo più calmo possibile, ma la ragione per cui non l'ho guardata era perché il mio viso era in fiamme, piuttosto che per educazione."


translate italian epilogue_uv_ulya_2a7fa430:


    uv "E perché dovrei? Perché ci sei tu?"


translate italian epilogue_uv_ulya_ec2c2a7c_2:


    "Ha riso."


translate italian epilogue_uv_ulya_5187f093:


    me "Sai che ti dico!"


translate italian epilogue_uv_ulya_b82d4c86:


    "Mi sono girato. Yulya era già immersa nell'acqua fino al collo."


translate italian epilogue_uv_ulya_059a514d:


    uv "Vieni qui!"


translate italian epilogue_uv_ulya_eb9cb597:


    "Ho pensato che nulla di male sarebbe successo se fossi rimasto in mutande. Mi sono spogliato e sono entrato in acqua."


translate italian epilogue_uv_ulya_1c12c14a:


    me "Brrr… Che freddo!"


translate italian epilogue_uv_ulya_6252b6e3:


    uv "Muoviti e ti scalderai! Fa' come me!"


translate italian epilogue_uv_ulya_831ca62f:


    "Ha iniziato a saltellare e ad agitare le braccia nell'acqua."


translate italian epilogue_uv_ulya_12554f6d:


    "Mi sentivo ancora a disagio."


translate italian epilogue_uv_ulya_21c8990f:


    uv "Sei diventato tutto rosso."


translate italian epilogue_uv_ulya_85e87f2d:


    "Mi ha detto Yulya, eccitata."


translate italian epilogue_uv_ulya_c105b60f:


    me "Certo che lo sono. Accanto a me c'è una ragazza nuda… o almeno un umanoide femminile."


translate italian epilogue_uv_ulya_cc8ea77e:


    uv "E?"


translate italian epilogue_uv_ulya_36ed3616:


    me "E questo è tutto!"


translate italian epilogue_uv_ulya_8e053d85:


    "Mi sono voltato, e nello stesso momento ho sentito l'abbraccio di Yulya."


translate italian epilogue_uv_ulya_846952dc:


    me "Questo… Anche questo fa parte dei tuoi piani?"


translate italian epilogue_uv_ulya_7b9304db:


    "Mi sono accorto che le mie parole non avevano alcun senso, ma non riuscivo a pensare a niente di meglio."


translate italian epilogue_uv_ulya_8f6b1a03_2:


    uv "Che cosa?"


translate italian epilogue_uv_ulya_00ee5007:


    me "Beh è… Sai…"


translate italian epilogue_uv_ulya_0a634eed:


    "Improvvisamente mi sono reso conto di essere abbracciato alle mie spalle da un essere onnipotente nudo, che era in grado di esistere in realtà multiple simultaneamente."


translate italian epilogue_uv_ulya_ef505b03:


    "Il mio cervello si è immediatamente sovraccaricato di errori per la mancanza di risorse di sistema. Ed eccola qui – la schermata blu della morte."


translate italian epilogue_uv_ulya_d4c218f1:


    "Le mie gambe hanno ceduto, e ho iniziato ad affondare.{w} Yulya mi ha sostenuto."


translate italian epilogue_uv_ulya_7035f158:


    uv "Non c'è bisogno di annegare!"


translate italian epilogue_uv_ulya_51d45ef4:


    "Mi stava ancora tenendo abbracciato.{w} Ora la sentivo ancora più forte…"


translate italian epilogue_uv_ulya_91df6206:


    me "Cos'hai intenzione di fare…?"


translate italian epilogue_uv_ulya_be5ea40d:


    uv "Nuotare, ovviamente!"


translate italian epilogue_uv_ulya_2dc1a8de:


    me "Quindi non stai alludendo a niente?"


translate italian epilogue_uv_ulya_7e490fd7:


    "Ho cercato di mantenere un tono calmo, ma non mi è riuscito molto bene."


translate italian epilogue_uv_ulya_69d9c023:


    uv "Per esempio?"


translate italian epilogue_uv_ulya_39165438:


    me "Beh… anche se non ti ricordi molte cose, essenzialmente ti comporti ragionevolmente…{w} Quindi, dovresti capirlo."


translate italian epilogue_uv_ulya_1c394603:


    uv "Beh, non lo so…"


translate italian epilogue_uv_ulya_54a2bc36:


    "Ha detto sornione."


translate italian epilogue_uv_ulya_1bdbd74a:


    "All'improvviso ho sentito qualcosa avvinghiarsi attorno alle mie cosce."


translate italian epilogue_uv_ulya_9ff69ab7:


    "Ho cercato di scostarmi, ma Yulya mi teneva stretto."


translate italian epilogue_uv_ulya_055bd048:


    "Inoltre muoversi in acqua è molto più difficile che muoversi in superficie."


translate italian epilogue_uv_ulya_c7494045:


    uv "È solo la mia coda!"


translate italian epilogue_uv_ulya_1efb85e5:


    me "Una coda… Non trovi strano che tu ne abbia una?"


translate italian epilogue_uv_ulya_18295dff:


    uv "Cosa c'è, non ti piace la mia coda? Non calpestare la mia coda, o sarà peggio per te!"


translate italian epilogue_uv_ulya_aff91d3f:


    me "Aspetta un secondo…"


translate italian epilogue_uv_ulya_f608ae54:


    "Mi sono fermato un istante, cercando di ricordare una cosa."


translate italian epilogue_uv_ulya_df14bfd6:


    me "Allora hai visto anche tu quel cartone animato?"


translate italian epilogue_uv_ulya_3ec3958b:


    uv "Quale cartone animato?"


translate italian epilogue_uv_ulya_1c1b1ab5:


    me "Quello con la frase che hai appena detto."


translate italian epilogue_uv_ulya_e2b99f78:


    uv "Beh io…"


translate italian epilogue_uv_ulya_c12e5015:


    "Ha pensato per un momento."


translate italian epilogue_uv_ulya_04b40d70:


    uv "Non è un modo di dire…?{w} Una specie di proverbio."


translate italian epilogue_uv_ulya_7b85edf4:


    me "Quindi non sai chi l'ha detta?"


translate italian epilogue_uv_ulya_ec8b60db:


    uv "No… Ma che importa!"


translate italian epilogue_uv_ulya_fdd16430:


    "Yulya ha riso."


translate italian epilogue_uv_ulya_31308d65:


    "La sua coda stava continuando a sondare il mio corpo."


translate italian epilogue_uv_ulya_c62be51e:


    "Forse potrebbe essere che Yulya abbia dei ricordi della sua vita prima del Campo?"


translate italian epilogue_uv_ulya_1c773453:


    "Forse sono nascosti nel profondo, o sono repressi."


translate italian epilogue_uv_ulya_135f2864:


    "Deve aver pur sentito quella frase da qualche parte!"


translate italian epilogue_uv_ulya_144b5de1:


    "Anche se potrebbe averla sentita semplicemente da un qualche pioniere…"


translate italian epilogue_uv_ulya_8d8c4af3:


    "Ma in questo periodo quello show forse non è ancora stato inventato."


translate italian epilogue_uv_ulya_0191cc8f:


    "L'unica spiegazione possibile è che Yulya sia imprigionata in questo Campo, proprio come me.{w} Solo che è un po' più misteriosa."


translate italian epilogue_uv_ulya_89471135:


    "Quello che è successo in seguito è successo da sé."


translate italian epilogue_uv_ulya_750ac835:


    "Anche se quello che stavo per fare era pericoloso, alla fine sono un essere umano, dopotutto!"


translate italian epilogue_uv_ulya_3fb50882:


    "In quel momento non stavo considerando Yulya come un essere onnipotente, avevo tra le braccia una ragazza del tutto ordinaria."


translate italian epilogue_uv_ulya_a20cefa7:


    "..."


translate italian epilogue_uv_ulya_d70204eb:


    "Ho trasportato la dormiente Yulya alla mia casetta.{w} Era sorprendentemente leggera."


translate italian epilogue_uv_ulya_a4ad8aa8:


    "L'ho appoggiata su un letto, sono crollato accanto a lei e ho chiuso gli occhi…"


translate italian epilogue_uv_ulya_e40d9ea3:


    "Ma tutti i miei tentativi di addormentarmi sono stati vani.{w} Avevo una sete terribile."


translate italian epilogue_uv_ulya_e74c6a6a:


    "Silenziosamente, in modo da non svegliare Yulya, mi sono alzato dal letto, mi sono messo i pantaloncini e sono uscito."


translate italian epilogue_uv_ulya_4e13b8c2:


    "Durante il tragitto verso i lavabi ho ripensato a tutto quello che era successo."


translate italian epilogue_uv_ulya_1bab0291:


    "Non mi importava se fosse giusto o sbagliato. No, non era quello il problema."


translate italian epilogue_uv_ulya_d913a13b:


    "Semplicemente non ho avuto altra scelta in quelle circostanze."


translate italian epilogue_uv_ulya_edf2e8e3:


    "Ciò che contava erano le possibili conseguenze."


translate italian epilogue_uv_ulya_77a3bfd6:


    "Anche se Yulya era un po' diversa da me, diciamo, lei era più vicina a chi (o a cosa) stava dietro a tutto questo.{w} Solo perché lei ne sapeva più di me."


translate italian epilogue_uv_ulya_ec7e2bac:


    "Il che significa…{w} Comunque, fare supposizioni o trarre conclusioni alla cieca è un compito difficile in questo Campo, ormai ne sono sicuro."


translate italian epilogue_uv_ulya_ce617998_4:


    "…"


translate italian epilogue_uv_ulya_53310946:


    "Mi sono dissetato e poi mi sono lavato. Me ne stavo tornando alla casetta della leader del Campo, fischiettando un pezzo da «Peer Gynt» di Grieg, quando qualcuno mi ha chiamato."


translate italian epilogue_uv_ulya_71c2dd40:


    pi "Come sei riuscito a…?"


translate italian epilogue_uv_ulya_b03222c9:


    "Ho sobbalzato, mi sono voltato e ho visto la sagoma di quel pioniere."


translate italian epilogue_uv_ulya_5e973abc:


    "Non avevo alcuna possibilità di riuscire a vederlo in volto in quell'oscurità."


translate italian epilogue_uv_ulya_7b78cd99:


    me "Quindi sei tu… di nuovo?"


translate italian epilogue_uv_ulya_9837d783:


    pi "«Io» chi? Non nel senso che io sia colui che hai già incontrato in precedenza, ma piuttosto – chi sono io, in realtà?"


translate italian epilogue_uv_ulya_d3bd0899_1:


    me "Non capisco."


translate italian epilogue_uv_ulya_1b8a1e41:


    "Yulya mi aveva detto che costui non era in grado di farmi del male, e quindi mi sono calmato un po'."


translate italian epilogue_uv_ulya_2a3661e7:


    pi "Non importa.{w} Comunque, ho visto come ci avete dato dentro prima!"


translate italian epilogue_uv_ulya_98007ecd:


    "Quelle parole mi hanno fatto venire la pelle d'oca."


translate italian epilogue_uv_ulya_5cfa92d6:


    "Indipendentemente da quanto bella possa essere un'esperienza, è sempre frustrante scoprire che qualcuno ti stava spiando durante qualcosa di così intimo."


translate italian epilogue_uv_ulya_73a1fd25:


    me "Non ti ha mai insegnato nessuno che non si dovrebbe spiare le persone?"


translate italian epilogue_uv_ulya_77ddb932:


    pi "Sei arrabbiato."


translate italian epilogue_uv_ulya_c45ede0e:


    me "No. Perché dovrei esserlo…"


translate italian epilogue_uv_ulya_346c99a6:


    "Ho sbuffato e mi sono girato verso di lui, cercando invano di vedere il suo volto."


translate italian epilogue_uv_ulya_772467b0:


    pi "E cosa ti ha detto la ragazza dalle orecchie di gatto?"


translate italian epilogue_uv_ulya_02007f83:


    me "Lei ha un nome!"


translate italian epilogue_uv_ulya_e1e64156:


    "Mi sentivo molto legato a Yulya, e quindi non ho esitato a prendere le sue difese."


translate italian epilogue_uv_ulya_cba6f894:


    pi "Un nome? E quale sarebbe? L'osservatore onnipotente? Il doppio volto di Giano?"


translate italian epilogue_uv_ulya_25dedf39:


    me "Yulya…"


translate italian epilogue_uv_ulya_fefef18f:


    "Ho detto in tono un po' incerto."


translate italian epilogue_uv_ulya_cf46188f:


    pi "Yulya?!"


translate italian epilogue_uv_ulya_73e6bc8d:


    "Si è messo a ridere come una iena."


translate italian epilogue_uv_ulya_b58f3a60:


    pi "E chi gliel'ha dato? Tu, probabilmente."


translate italian epilogue_uv_ulya_9bfb401d:


    me "Bene, dimmi cosa vuoi e poi sparisci!"


translate italian epilogue_uv_ulya_c16c1f2a:


    pi "Va bene, va bene, Vostra Altezza! Ma non essere arrabbiato!"


translate italian epilogue_uv_ulya_dfa62206:


    "Si è inchinato goffamente."


translate italian epilogue_uv_ulya_4e88db3f:


    "Quella scena era talmente grottesca che mi ha fatto venire i brividi."


translate italian epilogue_uv_ulya_ba66f329:


    pi "E cosa ti ha detto la cosiddetta Yulya?"


translate italian epilogue_uv_ulya_2dd45817:


    "Ho deciso che non aveva senso nascondergli le informazioni che avevo scoperto di recente."


translate italian epilogue_uv_ulya_a8058b44:


    me "Mi ha detto che ci sono molti altri campi. Che tu sei come me. E che dopo sette giorni ce ne andiamo e non torniamo mai più."


translate italian epilogue_uv_ulya_708e2129:


    "Mi sentivo disgustato dal fatto che mi trovassi nella sua stessa barca, ma i fatti sono fatti."


translate italian epilogue_uv_ulya_8d323880:


    pi "Sì, è quasi corretto. E cosa ti ha detto di sé?"


translate italian epilogue_uv_ulya_99d4c76e:


    me "Credi che siano affari tuoi? Sicuramente saprai già tutto senza bisogno che te lo dica io."


translate italian epilogue_uv_ulya_23fad338:


    pi "Sì, so qualcosa. Ho avuto a che fare con lei in passato."


translate italian epilogue_uv_ulya_afb9b3c2:


    "Quel suo tono era così nauseante che non avevo più voglia di continuare quella conversazione."


translate italian epilogue_uv_ulya_63e70168:


    me "Beh, se non c'è altro…"


translate italian epilogue_uv_ulya_e52964ff:


    pi "No, aspetta un attimo! Non ho ancora finito."


translate italian epilogue_uv_ulya_4b2daec2:


    me "Che c'è adesso?"


translate italian epilogue_uv_ulya_c5eeecce:


    pi "Vuoi che ti dica cosa lei ti sta nascondendo?"


translate italian epilogue_uv_ulya_cd917d11:


    me "Va' avanti."


translate italian epilogue_uv_ulya_97dc66db:


    "Ero sicuro che Yulya non mi stesse nascondendo nulla, ma nella mia situazione ogni informazione avrebbe potuto rivelarsi utile."


translate italian epilogue_uv_ulya_5f0edde5:


    pi "«Non sono mai tornati»?… Non è vero, sono tornati eccome! Anche tu sei tornato… O almeno l'hai fatto in passato."


translate italian epilogue_uv_ulya_787e2d21:


    me "Non capisco."


translate italian epilogue_uv_ulya_bc5bc614:


    pi "Ti ho già detto che sei stato qui prima d'ora. Così, dopo sette giorni non importa se te ne vai in autobus o se ti addormenti nella foresta – ti ritroverai comunque seduto sul bus 410."


translate italian epilogue_uv_ulya_5c4cab43:


    me "Ma come…?"


translate italian epilogue_uv_ulya_666d09fb:


    pi "Lei ti ha detto che «noi» semplicemente scompariamo, non è così?"


translate italian epilogue_uv_ulya_7fe9a557:


    "Non ho risposto."


translate italian epilogue_uv_ulya_de006685:


    pi "Beh, non posso avere la certezza che lei stia mentendo. Forse non conosce ogni cosa, ma quello che ti sto dicendo è la verità! Tu e io siamo stati qui molte volte. Certamente io sono tornato molte più volte di te, ma questo non importa."


translate italian epilogue_uv_ulya_c310b5dd:


    me "Ok, allora perché non ricordo niente?"


translate italian epilogue_uv_ulya_b00c1e22:


    pi "Tra un po' di tempo ti tornerà in mente tutto. Anche a me è successa la stessa cosa."


translate italian epilogue_uv_ulya_70d504c6:


    me "E mi stai dicendo che non c'è via di scampo?"


translate italian epilogue_uv_ulya_f4dc0758:


    pi "Esattamente."


translate italian epilogue_uv_ulya_916a3a3b:


    me "Ma ricordo che mi hai detto che gli eventi di quest'oggi non sono mai accaduti prima, giusto?"


translate italian epilogue_uv_ulya_8761e86f:


    pi "Giusto. Questa è la prima volta."


translate italian epilogue_uv_ulya_76023e32:


    me "E come ti spieghi questa cosa?"


translate italian epilogue_uv_ulya_86cd0059:


    pi "Non c'è niente da spiegare! Speri forse che qualcosa cambierà? Non cambierà un bel niente!"


translate italian epilogue_uv_ulya_8d1f0c27:


    me "Staremo a vedere."


translate italian epilogue_uv_ulya_2117dd83:


    "Mi sono voltato con decisione e mi sono allontanato."


translate italian epilogue_uv_ulya_aba701a6:


    "Stranamente il pioniere non ha cercato di fermarmi."


translate italian epilogue_uv_ulya_021b03c0:


    "Sono tornato alla casetta e ho trovato Yulya, che si stava pettinando la coda."


translate italian epilogue_uv_ulya_9364ca36:


    uv "Hai di nuovo parlato con lui."


translate italian epilogue_uv_ulya_e861403d:


    me "Come lo sai?"


translate italian epilogue_uv_ulya_105c8aa8:


    "Ho iniziato ad avere dei dubbi."


translate italian epilogue_uv_ulya_4632840a:


    uv "Così. Di solito si fa vedere quando sei da solo."


translate italian epilogue_uv_ulya_9513cd87:


    me "Sì…"


translate italian epilogue_uv_ulya_cc8ea77e_1:


    uv "E?"


translate italian epilogue_uv_ulya_4d5b668d:


    me "Mi ha detto che nessuno scompare. Che «noi» torniamo sempre."


translate italian epilogue_uv_ulya_92ef5f8d:


    "Yulya non ha risposto."


translate italian epilogue_uv_ulya_3581e356:


    me "È così?"


translate italian epilogue_uv_ulya_7f260d43:


    uv "A dire il vero, non lo so…"


translate italian epilogue_uv_ulya_407f9e91:


    "Mi sono seduto sul letto accanto a lei e ho cominciato a pensare."


translate italian epilogue_uv_ulya_ce617998_5:


    "…"


translate italian epilogue_uv_ulya_b838dd19:


    "Non so per quanto a lungo fossimo rimasti seduti, ma ben presto ho iniziato ad assonnarmi."


translate italian epilogue_uv_ulya_c69a0894:


    uv "Andiamo a guardare le stelle."


translate italian epilogue_uv_ulya_304c9144_1:


    me "Andiamo."


translate italian epilogue_uv_ulya_fd40ac42:


    "Avevo una grande voglia di dormire, ma allo stesso tempo sentivo un senso di incompletezza, e speravo che l'aria fresca avrebbe schiarito i miei pensieri."


translate italian epilogue_uv_ulya_63937d5a:


    "Pochi minuti più tardi eravamo alla piazza."


translate italian epilogue_uv_ulya_0734e246:


    "Mi sono seduto sulla panchina, e Yulya ha appoggiato la sua testa sulle mie gambe, fissando il cielo."


translate italian epilogue_uv_ulya_5c9504e1:


    uv "Non è magnifico…?"


translate italian epilogue_uv_ulya_9513cd87_1:


    me "Lo è…"


translate italian epilogue_uv_ulya_4900029d:


    "Siamo rimasti in silenzio a lungo..."


translate italian epilogue_uv_ulya_ceb783e7:


    me "Cosa succederà domani?"


translate italian epilogue_uv_ulya_62cb2255:


    uv "Che vuoi dire?"


translate italian epilogue_uv_ulya_ec806ff9:


    me "Beh, domani è il settimo giorno… Quindi è il momento di andarsene."


translate italian epilogue_uv_ulya_22006ea3:


    uv "Vuoi andartene?"


translate italian epilogue_uv_ulya_6343bef6:


    "Improvvisamente sono stato colto da un pensiero – quella domanda mi era già stata fatta molte volte."


translate italian epilogue_uv_ulya_56494f6b:


    me "Secondo le tue e le sue teorie, anche se non volessi andarmene – sono costretto."


translate italian epilogue_uv_ulya_72be10d8:


    uv "Allora andiamocene."


translate italian epilogue_uv_ulya_d1046a49:


    "Ha sorriso."


translate italian epilogue_uv_ulya_dab936c8:


    me "Vuoi venire con me?"


translate italian epilogue_uv_ulya_167bb087:


    uv "Certo che sì! Dopo tutto questo devi assolutamente sposarmi!"


translate italian epilogue_uv_ulya_ec3468be:


    me "Non credo che esista alcuna legge che possa approvare il matrimonio con una ragazza-gatto."


translate italian epilogue_uv_ulya_158aae48:


    uv "Allora la troveremo."


translate italian epilogue_uv_ulya_39fba389:


    me "Ok, seriamente, cosa accadrà domani?"


translate italian epilogue_uv_ulya_e652c25e:


    uv "Domani notte arriverà l'autobus…"


translate italian epilogue_uv_ulya_aa9c1d3b:


    me "Ne sei sicura?"


translate italian epilogue_uv_ulya_21e652c5:


    uv "Assolutamente sicura! Viene sempre."


translate italian epilogue_uv_ulya_f95c9fc9:


    me "E?"


translate italian epilogue_uv_ulya_6c27c947:


    uv "E noi ci saliremo."


translate italian epilogue_uv_ulya_f4eb646d:


    me "E poi?"


translate italian epilogue_uv_ulya_733740cb:


    uv "Perché mi stai facendo tutte queste domande stupide?!"


translate italian epilogue_uv_ulya_1c70a91b:


    "Ha protestato Yulya."


translate italian epilogue_uv_ulya_55b0a935:


    uv "Come faccio a saperlo?"


translate italian epilogue_uv_ulya_c25ae0cb:


    me "Beh, tira a indovinare!"


translate italian epilogue_uv_ulya_a7a02427:


    uv "E… torneremo al tuo mondo."


translate italian epilogue_uv_ulya_1f7d095b:


    me "Sarebbe…"


translate italian epilogue_uv_ulya_2a7a8a3b:


    "Mi sono fermato."


translate italian epilogue_uv_ulya_f20e460f:


    me "Vedremo."


translate italian epilogue_uv_ulya_9da1463d:


    "Ho sospirato."


translate italian epilogue_uv_ulya_510df513:


    me "E che mi dici di te e dei tuoi mille volti?"


translate italian epilogue_uv_ulya_4e31d83e:


    uv "Io sono così solo {i}qui{/i}, giusto? Laggiù mi limiterò ad essere me stessa."


translate italian epilogue_uv_ulya_50cdb6bd:


    me "Certo, con le orecchie di gatto e una coda…"


translate italian epilogue_uv_ulya_6c4c0225:


    "Yulya mi ha colpito dolorosamente nelle costole."


translate italian epilogue_uv_ulya_1a723568:


    me "Ehi! Sto solo scherzando!"


translate italian epilogue_uv_ulya_cc66d7ef:


    uv "Magari senza coda."


translate italian epilogue_uv_ulya_2a65d4fd:


    "Ha detto con tristezza."


translate italian epilogue_uv_ulya_a97d3385:


    me "No, no! Mi piace! Non pensare a una cosa del genere!"


translate italian epilogue_uv_ulya_5cd38101:


    "Non ha aggiunto altro, si è limitata a chiudere gli occhi."


translate italian epilogue_uv_ulya_412334c0:


    uv "E com'è {i}nel tuo mondo{/i}...?"


translate italian epilogue_uv_ulya_8459d4e8:


    me "È diverso – non saprei come spiegartelo. Alcune cose sono migliori, altre sono peggiori. Abbiamo internet…"


translate italian epilogue_uv_ulya_a026df63:


    "Ho ridacchiato."


translate italian epilogue_uv_ulya_1f38f517:


    uv "E io cosa potrei fare lì?"


translate italian epilogue_uv_ulya_59625bdc:


    me "Beh, non lo so… Risolveremo i problemi man mano che si presenteranno."


translate italian epilogue_uv_ulya_4eb5d154:


    uv "D'accordo…"


translate italian epilogue_uv_ulya_ce617998_6:


    "…"


translate italian epilogue_uv_ulya_c5c1cbec:


    "Siamo rimasti seduti lì a lungo, parlando di svariate cose superficiali."


translate italian epilogue_uv_ulya_21be8af1:


    "Alla fine Yulya ha detto:"


translate italian epilogue_uv_ulya_c4f8d6ec:


    uv "Andiamo a dormire!"


translate italian epilogue_uv_ulya_805f21bb:


    me "Già, tempismo perfetto!"


translate italian epilogue_uv_ulya_0c0452b3:


    "Sono stato travolto da una terribile ondata di fatica."


translate italian epilogue_uv_ulya_43478fd5:


    uv "Domani dovremo fare i bagagli."


translate italian epilogue_uv_ulya_f89ed32e:


    me "Ma io non ho niente da portarmi dietro."


translate italian epilogue_uv_ulya_e5d2ff76:


    uv "Ma guarda un po' – lui non ha niente da portarsi dietro… La sai una cosa… Io invece ho molto da portarmi dietro!"


translate italian epilogue_uv_ulya_e473fa2f:


    me "Per esempio?"


translate italian epilogue_uv_ulya_8ffd00b8:


    "Ho ridacchiato bonariamente."


translate italian epilogue_uv_ulya_00bce6ba:


    "Non avevo proprio idea di cosa volesse portarsi appresso."


translate italian epilogue_uv_ulya_4716ebb6:


    uv "Che mi dici delle mie provviste per l'inverno? Mele! Noci! Funghi!"


translate italian epilogue_uv_ulya_779061ff:


    me "Credi davvero che ti servirà tutta quella roba?"


translate italian epilogue_uv_ulya_bf9129e5:


    uv "Certo!"


translate italian epilogue_uv_ulya_2cb5dbd1:


    "Si è alzata di scatto, guardandomi con severità."


translate italian epilogue_uv_ulya_83ff6f89:


    uv "Io vado a dormire, tu fa' come ti pare!"


translate italian epilogue_uv_ulya_dde67e73:


    "Con quelle parole Yulya ha marciato a passo svelto verso la casetta della leader del Campo."


translate italian epilogue_uv_ulya_197dfa8a:


    me "Ehi, io non…"


translate italian epilogue_uv_ulya_a0076c62:


    "Mi sono precipitato dietro a lei."


translate italian epilogue_uv_ulya_ce617998_7:


    "…"


translate italian epilogue_uv_ulya_e426ad6a:


    "Appena dopo essere entrata nella stanza, Yulya si è spogliata, si è intrufolata nel letto e ha tirato la coperta sopra la sua testa."


translate italian epilogue_uv_ulya_beb6a4da:


    me "Non volevo offenderti… Se proprio vuoi prendere le tue provviste, allora va bene."


translate italian epilogue_uv_ulya_cf6c57e8:


    "Dopotutto potrebbero esserci d'aiuto, specialmente in una situazione dove tutto potrebbe succedere."


translate italian epilogue_uv_ulya_e70508ab:


    uv "Stavo solo scherzando!"


translate italian epilogue_uv_ulya_122cb80a:


    "Ha sbirciato da sotto la coperta, tirando fuori la lingua."


translate italian epilogue_uv_ulya_c41d257d:


    uv "E adesso – dormi!"


translate italian epilogue_uv_ulya_54256e31:


    "Mi ha ordinato Yulya."


translate italian epilogue_uv_ulya_b03955ac:


    "Non avevo nulla in contrario, così mi sono svestito, mi sono sdraiato accanto a lei, e ci siamo addormentati abbracciati…"


translate italian epilogue_uv_ulya_3b5be134:


    "Ho fatto un sogno davvero strano."


translate italian epilogue_uv_ulya_fe1efafe:


    "Beh, ovviamente il fatto di ritrovarsi in questo Campo era strano già di per sé, incomparabile con qualsiasi sogno notturno normale."


translate italian epilogue_uv_ulya_57a861c9:


    "Tuttavia, in questo sogno ogni cosa sembrava talmente reale che…"


translate italian epilogue_uv_ulya_6627fa0f:


    "Ma quando mi sono svegliato non riuscivo a ricordare più nulla."


translate italian epilogue_uv_ulya_ecbc37b5:


    "Ricordavo solo delle vaghe immagini di un lungo viale, un vecchio autobus arrugginito, il cancello traballante del Campo, le statue dei pionieri distrutte e una piazza abbandonata."


translate italian epilogue_uv_ulya_54f753f7:


    "Correvo avanti e indietro per «Sovyonok», come se volessi trovare qualcuno.{w} Ma chi fosse esattamente quel qualcuno resterà un mistero per me."


translate italian epilogue_uv_ulya_ef297c74:


    "Quando mi sono svegliato, il sole stava già tramontando."


translate italian epilogue_uv_ulya_30bf0240:


    "Un luminoso raggio di luce si stava facendo strada tra la polvere alla deriva nel silenzio della stanza, tinteggiandola con tutti i colori dell'arcobaleno."


translate italian epilogue_uv_ulya_75ec2ab9:


    "I riflessi sul vetro assumevano contorni diversi: da un'angolazione era un semplice raggio di sole, da un'altra angolazione sembrava un pirata che stava agitando la sua sciabola."


translate italian epilogue_uv_ulya_7e0b301d:


    "Mi sono stirato pigramente, sbadigliando."


translate italian epilogue_uv_ulya_5bd995a8:


    "Girandomi dall'altro lato ho visto Yulya che dormiva tranquillamente accanto a me."


translate italian epilogue_uv_ulya_85ea5c99:


    uv "Buongiorno!"


translate italian epilogue_uv_ulya_b5c033aa:


    "Ha detto Yulya, restando ad occhi chiusi."


translate italian epilogue_uv_ulya_658bead9:


    me "È già sera."


translate italian epilogue_uv_ulya_f7e9404b:


    uv "Il giorno inizia non appena ti svegli."


translate italian epilogue_uv_ulya_4bfd9907:


    me "Parli proprio come facevo io."


translate italian epilogue_uv_ulya_4db5c195:


    uv "Allora adesso hai cambiato idea?"


translate italian epilogue_uv_ulya_42efed39:


    me "Beh, non lo so… Durante questa settimana mi sono abituato ad alzarmi presto la mattina."


translate italian epilogue_uv_ulya_837b5c70:


    "Non era del tutto vero, dato che non sempre mi svegliavo al sorgere del sole, e spesso saltavo gli allineamenti."


translate italian epilogue_uv_ulya_f98565f1:


    uv "Vuoi dire che sei cambiato?"


translate italian epilogue_uv_ulya_d8e2c47f:


    "Ha ridacchiato."


translate italian epilogue_uv_ulya_3c6b3cb2:


    me "Credo che ognuno nella mia situazione cambierebbe, almeno un po'."


translate italian epilogue_uv_ulya_07fe3436:


    "Mi sembrava del tutto naturale."


translate italian epilogue_uv_ulya_4514ec9d:


    uv "Sì, forse. Non lo metto in dubbio."


translate italian epilogue_uv_ulya_90c7a728:


    "È seguito un lungo silenzio."


translate italian epilogue_uv_ulya_82c716ab:


    "Quel genere di silenzio dove non ci si sente tormentati dalla necessità di dover dire qualcosa solo per porre fine all'opprimente imbarazzo da esso scaturito.{w} Quel silenzio valeva più di mille parole."


translate italian epilogue_uv_ulya_eb401598:


    me "Allora, oggi si parte?"


translate italian epilogue_uv_ulya_2fd40082:


    "Le ho chiesto."


translate italian epilogue_uv_ulya_ef2303bf:


    uv "Penso di sì."


translate italian epilogue_uv_ulya_d68b5157:


    me "Pensi? Vuoi dire che non ne sei sicura?"


translate italian epilogue_uv_ulya_6510370b:


    uv "Non del tutto. È la prima volta che accade una cosa del genere."


translate italian epilogue_uv_ulya_8f9e293b:


    me "Intendi la sparizione dei pionieri."


translate italian epilogue_uv_ulya_b6003605:


    uv "Non solo quello…"


translate italian epilogue_uv_ulya_ec3291c7:


    "È scesa dal letto e ha cominciato a vestirsi."


translate italian epilogue_uv_ulya_0fbf0070:


    "L'ho guardata incantato, incapace di distogliere lo sguardo."


translate italian epilogue_uv_ulya_836b34d3:


    uv "Che c'è da guardare? Alzati! È tempo di fare i bagagli!"


translate italian epilogue_uv_ulya_2c312632:


    me "Ti riferisci ai tuoi frutti di bosco e ai funghi?"


translate italian epilogue_uv_ulya_bf9129e5_1:


    uv "Ovviamente!"


translate italian epilogue_uv_ulya_6935bf62:


    "Yulya si è messa in postura Akimbo, guardandomi con risentimento."


translate italian epilogue_uv_ulya_1bc3348a:


    uv "Vuoi che io abbandoni tutte le mie provviste?"


translate italian epilogue_uv_ulya_f7e824d2:


    me "Certo che no…"


translate italian epilogue_uv_ulya_acb58b15:


    "A dire il vero, non me ne fregava proprio niente delle sue provviste."


translate italian epilogue_uv_ulya_e0aa82ad:


    "Non ha alcun senso portarsi dietro tutte quelle cose in un viaggio di sola andata.{w} O almeno, un viaggio senza un biglietto di ritorno sicuro."


translate italian epilogue_uv_ulya_ce617998_8:


    "…"


translate italian epilogue_uv_ulya_cd0c429e:


    "Pochi minuti più tardi ci siamo ritrovati nella stessa radura dove il giorno precedente avevo incontrato Yulya mentre spargeva lo zucchero sui funghi."


translate italian epilogue_uv_ulya_2dad5b75:


    "È andata dietro a un albero, ha scavato tra il fogliame e ha tirato fuori un grosso sacco."


translate italian epilogue_uv_ulya_f3f023d1:


    me "Tutta questa roba…"


translate italian epilogue_uv_ulya_0877fc5a:


    uv "Non è tutto. Questo è solo lo stretto necessario."


translate italian epilogue_uv_ulya_6e778374:


    "Non sarei riuscito a dire quello che pensavo in modo gentile, così sono rimasto in silenzio."


translate italian epilogue_uv_ulya_e2dea26f:


    "Il sacco era pieno di funghi, frutti di bosco, mele, noci – tutti mescolati."


translate italian epilogue_uv_ulya_a3d7b55b:


    "Ho subito avuto dei seri dubbi riguardo all'idoneità al consumo umano di quell'assortimento."


translate italian epilogue_uv_ulya_d9056b1a:


    uv "Prendilo."


translate italian epilogue_uv_ulya_1331401f:


    "Ha detto Yulya, indicando il sacco con un sorrisetto."


translate italian epilogue_uv_ulya_59d43f12:


    me "Beh, spero che tu possa capire che non sono in grado di…"


translate italian epilogue_uv_ulya_ece439a7:


    uv "No, no, tu sei forte. Guarda come sei grosso!"


translate italian epilogue_uv_ulya_7430ffbb:


    "Ha indicato la differenza tra le nostre stature."


translate italian epilogue_uv_ulya_018dab19:


    "Ho provato a sollevare il sacco.{w} Pesava almeno una trentina di chili."


translate italian epilogue_uv_ulya_3e3eb9ad:


    me "Senti, prendiamone solo un po' – non saremo comunque in grado di mangiare tutto questo."


translate italian epilogue_uv_ulya_dbcde99e:


    "Stavo cercando di barare."


translate italian epilogue_uv_ulya_a0fad7c3:


    uv "Perché no? Gli inverni sono lunghi in quei posti."


translate italian epilogue_uv_ulya_957d4b84:


    me "E tu come lo sai?"


translate italian epilogue_uv_ulya_c5cc1a96:


    uv "Ho tirato a indovinare."


translate italian epilogue_uv_ulya_b07beae2:


    "Ho cercato di fare la smorfia più dolorosa possibile."


translate italian epilogue_uv_ulya_1d232d3e:


    me "Posso farcela al massimo fino alla piazza. Abbi pietà di me!"


translate italian epilogue_uv_ulya_58312911:


    "Yulya ha pensato un po'."


translate italian epilogue_uv_ulya_fd4177df:


    uv "Va bene, allora butta via le bacche!"


translate italian epilogue_uv_ulya_85411cb9:


    "Che idea geniale – liberarsi delle cose meno pesanti."


translate italian epilogue_uv_ulya_a6d25ea4:


    "Tuttavia non ho obiettato, e ho messo la mano nel sacco."


translate italian epilogue_uv_ulya_eb11115c:


    me "Non si può dire che sia diventato più leggero."


translate italian epilogue_uv_ulya_c1d08cf6:


    "Ho detto, dopo aver finito con le bacche."


translate italian epilogue_uv_ulya_89f72f94:


    uv "Non ne voglio sapere!"


translate italian epilogue_uv_ulya_b51aad30:


    "Ho sospirato e sono sprofondato a terra impotente, accanto al sacco."


translate italian epilogue_uv_ulya_6e976e33:


    me "Allora portiamolo a turno."


translate italian epilogue_uv_ulya_eb28f6df:


    uv "Ma è pesante!"


translate italian epilogue_uv_ulya_62526242:


    "Ha esclamato Yulya."


translate italian epilogue_uv_ulya_75383d27:


    me "È quello che sto cercando di dirti…"


translate italian epilogue_uv_ulya_2de9ff55:


    uv "Voglio dire, è pesante per me."


translate italian epilogue_uv_ulya_8894ed1f:


    me "Come se fosse leggero per me…"


translate italian epilogue_uv_ulya_f848c25b:


    "Ho mormorato sottovoce, cercando di non farmi sentire da Yulya."


translate italian epilogue_uv_ulya_cf87a84d:


    uv "Per te è leggero!"


translate italian epilogue_uv_ulya_545fb987:


    "Forse non avrei dovuto sottovalutare le sue orecchie di gatto."


translate italian epilogue_uv_ulya_9744fa26:


    me "Va bene, torneremo qui più tardi, adesso sarebbe bello mangiare qualcosa!"


translate italian epilogue_uv_ulya_a4e53ca8:


    uv "Sì, forse…"


translate italian epilogue_uv_ulya_de229a07:


    "Ha sorriso, porgendomi la mano."


translate italian epilogue_uv_ulya_ce617998_9:


    "…"


translate italian epilogue_uv_ulya_95c684b4:


    "La mensa si è riempita dell'odore di patate fritte."


translate italian epilogue_uv_ulya_d7b798a9:


    "Yulya gironzolava attorno a me, guardandomi cucinare."


translate italian epilogue_uv_ulya_eb7752aa:


    "A dire il vero, non ho mai provato alcun particolare piacere nel cucinare."


translate italian epilogue_uv_ulya_c91f29dc:


    "Ho sempre voluto mangiare all'istante, senza aspettare che tutti gli ingredienti fossero pronti, che l'acqua bollisse, che gli gnocchi galleggiassero…"


translate italian epilogue_uv_ulya_1fed802b:


    "Ecco perché di solito mi bastavano dei semplici panini o il cibo dei fast food."


translate italian epilogue_uv_ulya_d48b29e9:


    uv "Sei così bravo!"


translate italian epilogue_uv_ulya_4cb72b17:


    me "Te l'ho detto, non c'è niente di speciale in questo."


translate italian epilogue_uv_ulya_fec78416:


    uv "Ma… Ma…"


translate italian epilogue_uv_ulya_576e6d7a:


    "Stava cercando le parole giuste."


translate italian epilogue_uv_ulya_f256b99d:


    uv "Ma… Sei bravo comunque!"


translate italian epilogue_uv_ulya_ff60c702:


    me "Sono contento che ti piaccia."


translate italian epilogue_uv_ulya_5e40bb44:


    uv "Certo che mi piace!"


translate italian epilogue_uv_ulya_920e0bc4:


    "A giudicare dal modo in cui Yulya stava divorando le patate, era evidente che le fosse piaciuta la mia semplice creazione culinaria."


translate italian epilogue_uv_ulya_31397184:


    uv "Sei preoccupato?"


translate italian epilogue_uv_ulya_482b5f41:


    me "Non si parla con la bocca piena!"


translate italian epilogue_uv_ulya_96b2f2bb:


    uv "Che giovinastro!"


translate italian epilogue_uv_ulya_9e367860:


    me "Devi imparare a comportarti bene in una società civile."


translate italian epilogue_uv_ulya_442d1b23:


    uv "E allora in che tipo di società siamo qui adesso?"


translate italian epilogue_uv_ulya_13170ccd:


    "Yulya ha allargato le braccia e si è messa a ridere."


translate italian epilogue_uv_ulya_86f5a471:


    "Era vero, la mensa era deserta.{w} Proprio come il resto del Campo…"


translate italian epilogue_uv_ulya_68468e81:


    me "Cosa ne pensi, dove sono spariti tutti?"


translate italian epilogue_uv_ulya_61bc63e5:


    uv "Che ne so…"


translate italian epilogue_uv_ulya_164ed80e:


    "Ha preso l'ultimo pezzo con la forchetta e se l'è infilato abilmente in bocca."


translate italian epilogue_uv_ulya_13a24fe6:


    uv "Ti mancano?"


translate italian epilogue_uv_ulya_1d8c00d5:


    me "Un po'."


translate italian epilogue_uv_ulya_8f18ceca:


    uv "E allora io?! Non sono un semplice cane randagio!"


translate italian epilogue_uv_ulya_21b90d49:


    "Ecco un'altra citazione…{w} Questa volta non ci ho fatto molto caso."


translate italian epilogue_uv_ulya_081edf35:


    me "Nonostante tu sia una specie di gatto, conosci davvero bene le tue citazioni."


translate italian epilogue_uv_ulya_1072caf5:


    uv "Ti senti solo?"


translate italian epilogue_uv_ulya_616608b4:


    me "Beh, adesso non sono solo."


translate italian epilogue_uv_ulya_682c5cba:


    uv "Siamo solo noi due…"


translate italian epilogue_uv_ulya_9237bb74:


    me "Ma se non fosse successo quello che è successo, forse noi…"


translate italian epilogue_uv_ulya_3a69bd1e:


    "Mi sono fermato per un istante."


translate italian epilogue_uv_ulya_aebf8ccd:


    me "Quindi, nelle altre realtà noi due non eravamo così…"


translate italian epilogue_uv_ulya_7a792120:


    uv "Legati?"


translate italian epilogue_uv_ulya_a0ab92e9:


    "Yulya mi ha aiutato a finire la frase."


translate italian epilogue_uv_ulya_fc30fd2b_1:


    me "Esatto."


translate italian epilogue_uv_ulya_55d14e50:


    uv "No."


translate italian epilogue_uv_ulya_4cb51946:


    me "Strano."


translate italian epilogue_uv_ulya_fd80ccaf:


    uv "Perché?"


translate italian epilogue_uv_ulya_e7a7b1ad:


    me "Beh, perché ci sono un sacco di campi, come dici tu, e secondo la teoria della probabilità…"


translate italian epilogue_uv_ulya_c508ce25:


    me "Significa che tutto ciò che è accaduto negli ultimi due giorni è stato davvero qualcosa di eccezionale…"


translate italian epilogue_uv_ulya_ffa95805:


    uv "Lo dici come se fosse una cosa brutta."


translate italian epilogue_uv_ulya_f837dd45:


    me "Certo che no!"


translate italian epilogue_uv_ulya_c9b0b0be:


    "Ho sentito il sangue arrivarmi alla testa."


translate italian epilogue_uv_ulya_7651e2ab:


    me "Proprio strano. Ci sono un sacco di cose strane qui. Ecco perché uso quella parola così spesso. In ogni caso, stai proprio cercando il pelo nell'uovo."


translate italian epilogue_uv_ulya_0834f49a:


    uv "Chi, io?! Niente affatto!"


translate italian epilogue_uv_ulya_ce3666e5:


    "Ha riso."


translate italian epilogue_uv_ulya_a3c453d6:


    me "No, davvero, devi sempre cercare il pelo nell'uovo! Soprattutto quando si tratta delle parole!"


translate italian epilogue_uv_ulya_81f608cc:


    uv "E perché dovrei farlo?"


translate italian epilogue_uv_ulya_48537224:


    me "Non riesco proprio a immaginarmi perché. Dovresti saperlo meglio di me…"


translate italian epilogue_uv_ulya_47309df6:


    "È come se ad ogni mia argomentazione lei avesse la risposta pronta. Come se sapesse in anticipo quello che le stavo per dire."


translate italian epilogue_uv_ulya_457518f0:


    me "Lascia perdere… Allora, andiamo?"


translate italian epilogue_uv_ulya_fd5ba6b4:


    uv "Dove?"


translate italian epilogue_uv_ulya_3096cd01:


    "Mi sono accorto di aver commesso un grave errore, adesso sicuramente vorrà trascinarmi nuovamente verso il suo sacco."


translate italian epilogue_uv_ulya_149eedf2:


    me "Ehm… c'è ancora molto tempo fino all'arrivo del bus. Forse potremmo…"


translate italian epilogue_uv_ulya_cc52b4f1:


    uv "Ah, giusto! Le provviste!"


translate italian epilogue_uv_ulya_a1d07ef9:


    "Mi sono grattato il mento e l'ho seguita. Epic fail!"


translate italian epilogue_uv_ulya_ce617998_10:


    "…"


translate italian epilogue_uv_ulya_1c0485da:


    "Portarsi dietro circa venticinque chili non era facile."


translate italian epilogue_uv_ulya_6712960c:


    "Persino per un breve tratto."


translate italian epilogue_uv_ulya_c17bf6e0:


    "Dovevo fermarmi ogni cinquantina di metri per riposare."


translate italian epilogue_uv_ulya_75fc6814:


    "Yulya si è affrettata cercando di incoraggiarmi, ma stava solo peggiorando le cose."


translate italian epilogue_uv_ulya_0e135c3b:


    "Non saprei dire se mi meritassi una medaglia di una qualche strana competizione di trasporto-a-breve-distanza-del-sacco-di-una-ragazza-gatto, ma dopo circa un'ora sono riuscito a portarlo alla fermata dell'autobus."


translate italian epilogue_uv_ulya_087877b3:


    "Sono crollato a terra, esausto."


translate italian epilogue_uv_ulya_b0d3f9c0:


    me "Ecco fatto…"


translate italian epilogue_uv_ulya_e76aa14c:


    uv "Sei proprio un eroe! Visto, non c'era niente di cui preoccuparsi!"


translate italian epilogue_uv_ulya_72d66e35:


    "Mi faceva male dappertutto, i miei muscoli bruciavano, fiumi di sudore scorrevano sulla mia fronte e sentivo un forte bruciore agli occhi."


translate italian epilogue_uv_ulya_fa5868f8:


    "Non so quale premio mi spettasse per aver vinto, ma questa competizione sembrava far parte delle Olimpiadi per persone mentalmente disabili – anche se avevo vinto, ero comunque un ritardato."


translate italian epilogue_uv_ulya_fd9fe70f:


    uv "Guarda! Sta arrivando il bus!"


translate italian epilogue_uv_ulya_3d342d5a:


    me "Che cosa?!"


translate italian epilogue_uv_ulya_c00b6b38:


    "Sono balzato in piedi. La fatica è sparita all'istante."


translate italian epilogue_uv_ulya_50978573:


    "Sembrava davvero che qualcosa si stesse avvicinando in lontananza."


translate italian epilogue_uv_ulya_b941dcba:


    "Ho strizzato gli occhi e ho visto l'Ikarus."


translate italian epilogue_uv_ulya_bedf97cb:


    "Ma perché?"


translate italian epilogue_uv_ulya_42a3ce3a:


    "Yulya mi aveva detto che sarebbe arrivato verso notte."


translate italian epilogue_uv_ulya_eb16fe39:


    "E inoltre, mi sarei aspettato un LiAZ."


translate italian epilogue_uv_ulya_ce26c4df:


    "Poco dopo l'autobus si è fermato di fronte a noi e la porta si è aperta, invitandoci ad entrare."


translate italian epilogue_uv_ulya_6de31db1:


    "Il sedile del conducente era vuoto, ma ciò non mi ha sorpreso. Cose molto più strane erano accadute qui."


translate italian epilogue_uv_ulya_13faf4dc:


    uv "Andiamo?"


translate italian epilogue_uv_ulya_3a532c56:


    "Yulya è saltata con disinvoltura sul primo scalino, facendomi cenno di seguirla."


translate italian epilogue_uv_ulya_a7b876ab:


    me "Non pensi sia strano che gli autobus possano guidarsi da soli?"


translate italian epilogue_uv_ulya_943ed485:


    uv "Beh…"


translate italian epilogue_uv_ulya_60f3d758_3:


    "Ci ha pensato su."


translate italian epilogue_uv_ulya_0375339e:


    uv "Certo, è strano, ma tu stesso hai detto che ci sono un sacco di cose strane qui!"


translate italian epilogue_uv_ulya_b1d8564c:


    "Sì, aveva proprio ragione."


translate italian epilogue_uv_ulya_8dec57e5:


    "E comunque, non avevo altra scelta."


translate italian epilogue_uv_ulya_6d318d6a:


    "Sia che fossi rimasto o che fossi salito su quel bus, non avrei potuto comunque avere la certezza di cosa sarebbe accaduto in seguito."


translate italian epilogue_uv_ulya_e63e6485:


    "Ma almeno la seconda opzione poteva darmi l'illusoria speranza di riuscire ad andarmene da questo Campo."


translate italian epilogue_uv_ulya_db41b274:


    "Ho fatto un sospiro, e con un ultimo sforzo ho sollevato il sacco da terra e sono salito sul bus."


translate italian epilogue_uv_ulya_ce617998_11:


    "…"


translate italian epilogue_uv_ulya_e6421b37:


    "Abbiamo guidato per circa un'ora."


translate italian epilogue_uv_ulya_43dae48d:


    "Yulya chiacchierava continuamente riguardo a qualcosa, ma io non la stavo ascoltando."


translate italian epilogue_uv_ulya_ca1945f3:


    "Studiavo con attenzione lo sfrecciante paesaggio al di fuori del finestrino.{w} Foreste, campi, fiumi, boschi, altri campi… Assolutamente nulla di sospetto."


translate italian epilogue_uv_ulya_f8021fd2:


    "Da un lato ciò riusciva a darmi una qualche speranza, ma dall'altro era segno che alla fine non sarebbe cambiato nulla, poiché in questo mondo tutto sembrava lento e misurato, seguendo le sue regole consolidate."


translate italian epilogue_uv_ulya_302508d2:


    "O almeno era così qualche giorno fa."


translate italian epilogue_uv_ulya_a66cfed7:


    "Anche se forse non dovrei trarre conclusioni dopo essere stato qui solo una settimana…"


translate italian epilogue_uv_ulya_ec0eddf1:


    uv "Beh! Ora sono sicura che tutto andrà per il meglio!"


translate italian epilogue_uv_ulya_8f838576_2:


    "L'ho guardata attentamente."


translate italian epilogue_uv_ulya_6a4e1b53:


    "Orecchie di gatto, una lunga coda che si muoveva ritmicamente avanti e indietro, le zanne un po' allargate. Yulya sembrava il personaggio di una fiaba."


translate italian epilogue_uv_ulya_ad15388e:


    "Deciderò più tardi cosa fare di lei nel mondo reale."


translate italian epilogue_uv_ulya_1eeff60a:


    "Mi stavo trasformando in una grande vena pulsante, pronta a scoppiare da un momento all'altro."


translate italian epilogue_uv_ulya_a487c0e8:


    "Stavo aspettando...{w} In attesa di come sarebbe andata a finire."


translate italian epilogue_uv_ulya_8bce2f18:


    "Sarebbe potuta finire in qualsiasi modo. La cosa che consideravo importante era non impazzire prima del tempo."


translate italian epilogue_uv_ulya_2cfc3b5a:


    "I miei nervi erano tesi al limite, e ho cercato di non pensare a niente."


translate italian epilogue_uv_ulya_61cf1417:


    "Ma era impossibile riuscire a distrarsi."


translate italian epilogue_uv_ulya_8bb8720a:


    uv "Ehi! Mi stai ascoltando almeno?"


translate italian epilogue_uv_ulya_7401b3c5:


    me "Eh? No, scusa, ero assorto nei miei pensieri…"


translate italian epilogue_uv_ulya_da7fd880:


    uv "Beh, come sempre!"


translate italian epilogue_uv_ulya_366345bd:


    "Ha detto imbronciata."


translate italian epilogue_uv_ulya_e11c2266:


    me "Questo è un grande momento, sai… Abbiamo la possibilità di andarcene… di uscire da questo Campo. Quindi è un momento molto importante per me."


translate italian epilogue_uv_ulya_b2811648:


    uv "Sì, è importante anche per me, ma non sono preoccupata."


translate italian epilogue_uv_ulya_d44a746c:


    me "Semplicemente non riesci a vederlo nel suo contesto."


translate italian epilogue_uv_ulya_6afd4281:


    uv "Cosa intendi?"


translate italian epilogue_uv_ulya_b9ed7480:


    me "Prima vivevo in un mondo diverso – quello reale, o almeno credo. E quindi io ho qualcosa da poter confrontare con questo Campo. E tu invece, si potrebbe dire che sei nata qui."


translate italian epilogue_uv_ulya_f5055106:


    uv "No, non sono nata qui…"


translate italian epilogue_uv_ulya_cfc51cbb:


    me "E come lo sai?"


translate italian epilogue_uv_ulya_3a5cf8cd:


    uv "Lo so e basta! Lo so…"


translate italian epilogue_uv_ulya_ce617998_12:


    "…"


translate italian epilogue_uv_ulya_a850329e:


    "L'autobus sobbalzava tranquillamente sopra ai dossi, scuotendomi sempre più."


translate italian epilogue_uv_ulya_4512f1df:


    "Il volante girava da solo, e ben presto ho smesso di notare l'assenza di un conducente."


translate italian epilogue_uv_ulya_ad87b7fb:


    "I miei occhi hanno cominciato a chiudersi, e mi sono assopito…"


translate italian epilogue_uv_ulya_5fabbbed:


    "Una ragazza si è chinata sopra di me."


translate italian epilogue_uv_ulya_02d2b574:


    "Mi ha sussurrato dolcemente qualcosa all'orecchio…{w} Così teneramente, che…"


translate italian epilogue_uv_ulya_4fc9e3f4:


    "Ho sentito un forte colpo alla testa."


translate italian epilogue_uv_ulya_53a6f078:


    uv "Ehi, non dormire!"


translate italian epilogue_uv_ulya_ba101d53:


    me "Ti sto disturbando?"


translate italian epilogue_uv_ulya_2d838a38:


    "Ho chiesto infastidito, massaggiandomi la testa contusa."


translate italian epilogue_uv_ulya_8e590003:


    uv "Non mi stai ascoltando!"


translate italian epilogue_uv_ulya_f7b19f66:


    me "Se riusciamo a fuggire da qui, avremo un sacco di tempo per parlare."


translate italian epilogue_uv_ulya_b61f7d2c:


    uv "Se… se… E se invece non riuscissimo?"


translate italian epilogue_uv_ulya_f4a33a5c:


    me "Allora avremo ancora più tempo."


translate italian epilogue_uv_ulya_5db31537:


    "Ho cercato di sorridere, ma il solo pensare all'eventualità di non riuscire ad andarcene da questo Campo mi spaventava molto, quindi mi sono limitato a ghignare stupidamente."


translate italian epilogue_uv_ulya_13b1a056:


    uv "Forse è meglio se non ce ne andiamo! Perché nel tuo mondo… Non so cosa mi aspetta lì, ma… Tu non mi degnerai di alcuna attenzione!"


translate italian epilogue_uv_ulya_959ccee3:


    me "Lo farò, te lo prometto!"


translate italian epilogue_uv_ulya_67844860:


    "L'ho abbracciata con delicatezza."


translate italian epilogue_uv_ulya_7853c29e:


    uv "Davvero-davvero?"


translate italian epilogue_uv_ulya_c7d80c45:


    me "Davvero-davvero!"


translate italian epilogue_uv_ulya_ae142e7e:


    "Yulya si è appoggiata a me."


translate italian epilogue_uv_ulya_134e4880:


    uv "E mi ascolterai?"


translate italian epilogue_uv_ulya_771aa502:


    me "Certamente!"


translate italian epilogue_uv_ulya_9eb6accf:


    uv "E sarai sempre d'accordo con me?"


translate italian epilogue_uv_ulya_8288e06d:


    me "Sicuro!"


translate italian epilogue_uv_ulya_06fb0086:


    "Si è scostata e mi ha guardato negli occhi."


translate italian epilogue_uv_ulya_9e863299:


    uv "Va bene, allora ti perdono!"


translate italian epilogue_uv_ulya_4e48f9de:


    "Una sonora risata ha riempito l'interno dell'autobus."


translate italian epilogue_uv_ulya_c74b3c37:


    uv "Ma fa' attenzione, non dimenticartelo, e non cercare di ingannarmi!"


translate italian epilogue_uv_ulya_45c72629:


    me "Vuoi che firmi un contratto?"


translate italian epilogue_uv_ulya_63cdfc3b:


    uv "Un contratto di matrimonio?"


translate italian epilogue_uv_ulya_7b2837ac:


    me "Non ho comunque alcuna proprietà per quello…"


translate italian epilogue_uv_ulya_1c13dd09:


    uv "Ma io sì!"


translate italian epilogue_uv_ulya_55396f10:


    "Yulya ha indicato il suo sacco, che giaceva nel corridoio dell'autobus."


translate italian epilogue_uv_ulya_621c7bd3:


    me "Bene, allora…"


translate italian epilogue_uv_ulya_c9689908:


    "Ho provato a ricordare cosa avessi portato oltre ai miei spiccioli."


translate italian epilogue_uv_ulya_b16647b1:


    me "Ecco qua!"


translate italian epilogue_uv_ulya_96266e14:


    "Ho tolto il mio telefono cellulare dalla tasca.{w} Ormai era già scarico."


translate italian epilogue_uv_ulya_b3557c43:


    uv "Che cos'è?"


translate italian epilogue_uv_ulya_b0ef2520:


    "Mi ha chiesto Yulya incuriosita, prendendo il cellulare."


translate italian epilogue_uv_ulya_589cf2f8:


    me "Una cosa molto utile nel mio mondo – ti permette di comunicare con le persone a distanza."


translate italian epilogue_uv_ulya_63b9af07:


    uv "Wow, è davvero forte! Eccellente, questo andrà bene!"


translate italian epilogue_uv_ulya_bd622277:


    "Ha appoggiato il telefono sul sedile."


translate italian epilogue_uv_ulya_90bff781:


    uv "Affare fatto allora? Da parte mia ci sono le mie provviste per l'inverno, e da parte tua… quella cosa!"


translate italian epilogue_uv_ulya_01822020_1:


    me "Ok, affare fatto."


translate italian epilogue_uv_ulya_4a22ceaf:


    uv "Bene, ora siamo marito e moglie!"


translate italian epilogue_uv_ulya_945c59bc:


    "Ha sorriso sornione."


translate italian epilogue_uv_ulya_3118ae12:


    me "Ehi, aspetta…"


translate italian epilogue_uv_ulya_d41d1cae:


    "All'improvviso mi sono accorto che quella conversazione era iniziata con un contratto di matrimonio."


translate italian epilogue_uv_ulya_8d32181d:


    me "In ogni caso, senza una registrazione ufficiale…"


translate italian epilogue_uv_ulya_934db5ab:


    uv "Quando arriveremo nel tuo mondo registreremo il nostro matrimonio. Un uomo senza una donna è come una cucina senza un coltello."


translate italian epilogue_uv_ulya_f5e62b0b:


    me "Sempre che ci arriviamo, ovviamente…"


translate italian epilogue_uv_ulya_a540e1eb:


    "Yulya non ha aggiunto altro, si è limitata a fissare davanti a sé ad occhi spalancati."


translate italian epilogue_uv_ulya_9a70d2e5:


    "Ho seguito il suo sguardo e mi sono reso conto che l'autobus era affollato di persone!"


translate italian epilogue_uv_ulya_08aceedd:


    "Tutti loro indossavano la solita uniforme, e assomigliavano esattamente a quello strano pioniere."


translate italian epilogue_uv_ulya_b9a82855:


    "Nessuno di loro ha detto una singola parola. Si sono limitati a guardarmi in silenzio."


translate italian epilogue_uv_ulya_0638a60a:


    "Non riuscivo a vedere i loro volti. L'abbigliamento, le sagome – sì, ma i loro volti erano immersi in delle ombre che sembravano essere apparse dal nulla."


translate italian epilogue_uv_ulya_c09707fb:


    "Era come se al posto della testa avessero dei piccoli buchi neri."


translate italian epilogue_uv_ulya_131aff14:


    "Ho iniziato a tremare, e mi è venuta la pelle d'oca."


translate italian epilogue_uv_ulya_1d2d7a9b:


    "Yulya mi ha stretto la mano."


translate italian epilogue_uv_ulya_71296442:


    "Con uno sforzo enorme mi sono voltato verso di lei."


translate italian epilogue_uv_ulya_e0da3ec4:


    "Anche lei sembrava essere spaventata."


translate italian epilogue_uv_ulya_13859e61:


    "Se fossi stato da solo in quel momento probabilmente avrei perso i sensi.{w} Ma ora non ero responsabile solo della mia vita."


translate italian epilogue_uv_ulya_2c2b16f4:


    me "Chi… siete… voi…"


translate italian epilogue_uv_ulya_4569b623_1:


    "Ho chiesto con difficoltà."


translate italian epilogue_uv_ulya_9074e15e:


    "I pionieri hanno mantenuto quel terrificante silenzio."


translate italian epilogue_uv_ulya_d76ff1af:


    "Respirare è diventato difficile. Ero senza fiato, ma il mio cervello si è rifiutato di risolvere il problema della mancanza di ossigeno, e così ho perso conoscenza per un momento."


translate italian epilogue_uv_ulya_ede926b3:


    uv "Chi siete?! Cosa volete?!"


translate italian epilogue_uv_ulya_60f8aa06:


    "Ho sentito l'urlo di Yulya proprio mentre riprendevo i sensi."


translate italian epilogue_uv_ulya_2b71358d:


    "Uno dei pionieri si è alzato dalla prima fila e si è diretto verso di noi."


translate italian epilogue_uv_ulya_8de32484:


    pi "Ciao, Semyon! Abbiamo deciso di tornare «a casa» anche noi."


translate italian epilogue_uv_ulya_2dbb2a67:


    all "Anche noi… anche noi…"


translate italian epilogue_uv_ulya_bbf8617d:


    "Un coro di voci ha riempito il bus."


translate italian epilogue_uv_ulya_b2f2241f:


    pi "Vi terremo compagnia."


translate italian epilogue_uv_ulya_faae32c5:


    "Non sapevo cosa dire."


translate italian epilogue_uv_ulya_43efcd48:


    "Questa era la situazione più strana che avessi mai vissuto in questo Campo."


translate italian epilogue_uv_ulya_a067eaab:


    "Né Yulya, né la città, né la scomparsa dei pionieri, né quella strana persona in piedi davanti a me, nessuno di essi era comparabile con questo…"


translate italian epilogue_uv_ulya_af6577bb:


    uv "Ti ho fatto una domanda!"


translate italian epilogue_uv_ulya_b9e0f9fd:


    "Sembrava che Yulya fosse ancora in grado di parlare con lucidità.{w} Nulla di sorprendente. Alla fine lei era ancora, come si potrebbe dire, un essere straordinario."


translate italian epilogue_uv_ulya_9ce8487e:


    pi "Te lo ripeto. Stiamo semplicemente tornando a casa."


translate italian epilogue_uv_ulya_5913ba9c:


    "La sua voce era isterica."


translate italian epilogue_uv_ulya_ecaa6f59:


    pi "Vi stiamo forse disturbando?"


translate italian epilogue_uv_ulya_7cde2b94:


    all "Disturbando… disturbando…"


translate italian epilogue_uv_ulya_116fe00f:


    uv "Che cosa volete da noi?"


translate italian epilogue_uv_ulya_728d6df5:


    pi "Niente di niente."


translate italian epilogue_uv_ulya_09008b6b:


    all "Niente… niente…"


translate italian epilogue_uv_ulya_9f89e0ef:


    uv "Non ti credo! So che state facendo tutto questo per un motivo!"


translate italian epilogue_uv_ulya_021210b3:


    pi "È solo la tua immaginazione, animale!"


translate italian epilogue_uv_ulya_6bd9eaf0:


    uv "Forse io sono un animale. Ma tu cosa sei? Un fantasma, un'ombra?"


translate italian epilogue_uv_ulya_1ee124eb:


    pi "Può essere. Persino i fantasmi hanno i loro diritti."


translate italian epilogue_uv_ulya_d438c040:


    uv "L'unico diritto che hai è quello sparire da qui!"


translate italian epilogue_uv_ulya_f5fb328d:


    "Yulya ha gridato così forte che le mie orecchie hanno tuonato. Ma né il pioniere né i suoi compagni hanno reagito alla cosa."


translate italian epilogue_uv_ulya_98717eb9:


    pi "Credo che resteremo."


translate italian epilogue_uv_ulya_955f7cb8:


    "Ha detto malignamente."


translate italian epilogue_uv_ulya_8e29f23e:


    me "Ma come ci sei riuscito? E tutti gli altri…"


translate italian epilogue_uv_ulya_ee804b19:


    "Finalmente ho trovato la forza di parlare."


translate italian epilogue_uv_ulya_21629221:


    pi "È semplice, mio caro amico! Intelligenza collettiva. Ne hai mai sentito parlare?"


translate italian epilogue_uv_ulya_7b70b2c1:


    me "E… allora?"


translate italian epilogue_uv_ulya_24b9de6e:


    pi "Ognuno di noi ha voluto augurarti buona fortuna contemporaneamente, questo è tutto."


translate italian epilogue_uv_ulya_e60d1472:


    all "Fortuna… fortuna…"


translate italian epilogue_uv_ulya_b80d896b:


    pi "Di persona, direi. Questo è il motivo per cui siamo qui."


translate italian epilogue_uv_ulya_9fab6928:


    me "Ma cosa succederà in seguito...?"


translate italian epilogue_uv_ulya_8da302f0:


    pi "Francamente, non ne ho idea."


translate italian epilogue_uv_ulya_c8ba98b9:


    uv "Andatevene!"


translate italian epilogue_uv_ulya_f00f1e41:


    "Ha urlato di nuovo Yulya."


translate italian epilogue_uv_ulya_7f35eb53:


    pi "Sta' buona tu – a cuccia!"


translate italian epilogue_uv_ulya_94f08d02:


    pi "Vedi…"


translate italian epilogue_uv_ulya_fbc96e29:


    "Si è rivolto nuovamente a me."


translate italian epilogue_uv_ulya_d743b5e5:


    pi "Non voglio nasconderti niente. Hai delle buone probabilità di uscire da qui. Non so esattamente perché, ma le tue azioni hanno infranto il ciclo. I cicli, per essere precisi, tutti i nostri cicli. E c'è la possibilità che non dovremo lasciare il Campo ogni sette giorni e tornare indietro l'ottavo, risvegliandoci in questo bus."


translate italian epilogue_uv_ulya_db8702b3:


    uv "Allora fate senza! Morite e basta!"


translate italian epilogue_uv_ulya_f3e7b92e:


    pi "Non è così semplice. Perché non dovremmo avere un istinto di auto-conservazione?"


translate italian epilogue_uv_ulya_6747b594:


    pi "Dovremmo, giusto?"


translate italian epilogue_uv_ulya_a3c4358b:


    "Ha chiesto ai pionieri."


translate italian epilogue_uv_ulya_11042548:


    all "Dovremmo… dovremmo…"


translate italian epilogue_uv_ulya_7d1b8fb3:


    "Hanno risposto in coro."


translate italian epilogue_uv_ulya_fdc1027f:


    pi "Così abbiamo deciso di tenervi compagnia."


translate italian epilogue_uv_ulya_d3565aac:


    pi "Ora, o ce la faremo ad uscire tutti insieme, oppure svaniremo tutti per sempre. O inizierà un altro ciclo. Tutte e tre le opzioni sono accettabili per noi."


translate italian epilogue_uv_ulya_71af10ce:


    uv "E se morissimo per colpa vostra?!"


translate italian epilogue_uv_ulya_96f68338:


    pi "Beh, allora non morirete da soli. Moriremo con onore."


translate italian epilogue_uv_ulya_861f9a54:


    all "Onore… onore…"


translate italian epilogue_uv_ulya_57fefea4:


    pi "O forse pensate che le nostre vite…"


translate italian epilogue_uv_ulya_446df447:


    "Ha riso..."


translate italian epilogue_uv_ulya_f303d177:


    pi "Beh, chiamiamole vite…"


translate italian epilogue_uv_ulya_a123f077:


    all "Vite… vite…"


translate italian epilogue_uv_ulya_651e05d0:


    pi "Che non siano così importanti come le vostre?"


translate italian epilogue_uv_ulya_ec2d2644:


    "Yulya non sapeva cosa rispondere."


translate italian epilogue_uv_ulya_20807278:


    pi "E non pensate che siamo tutti qui. In realtà la maggior parte di noi non ce l'ha fatta a salire su questo autobus, nonostante ci fossero abbastanza biglietti per tutti."


translate italian epilogue_uv_ulya_5d22fc9e:


    uv "Tu… tu…"


translate italian epilogue_uv_ulya_88523b73:


    "Ha sibilato Yulya."


translate italian epilogue_uv_ulya_f501ead7:


    pi "È troppo tardi ormai. Vi suggerisco di calmarvi e di godervi il viaggio. Le hostess vi offriranno cibo e bevande entro un paio di minuti."


translate italian epilogue_uv_ulya_2b07621f:


    "Ha fatto un ghigno, tornandosene lentamente al suo posto."


translate italian epilogue_uv_ulya_3d9eb82e:


    "Yulya è balzata in piedi, ma l'ho fermata."


translate italian epilogue_uv_ulya_83d116b4:


    uv "Lasciami andare! Lo farò a pezzi!"


translate italian epilogue_uv_ulya_c6ddc0c1:


    me "Non ce n'è bisogno… Cosa vuoi fargli? Che cosa cambierebbe?"


translate italian epilogue_uv_ulya_6e1e8524:


    "Mi ha guardato sorpresa, ma alla fine si è seduta."


translate italian epilogue_uv_ulya_30c0cbc9:


    uv "Allora cosa, mi stai forse suggerendo di non fare nulla e semplicemente aspettare?"


translate italian epilogue_uv_ulya_e2f12d3a:


    me "Che altro possiamo fare?"


translate italian epilogue_uv_ulya_9cc2138c:


    uv "Non lo so, ma non dovremmo starcene seduti qui…"


translate italian epilogue_uv_ulya_a77d3972:


    me "Se c'è una cosa che ho imparato in questo Campo, è che poco dipende da me. In realtà, niente di niente. Soprattutto adesso. Specialmente qui. Non ho la minima idea di cosa fare per cacciarli fuori di qui, ma anche se sapessi come, sono sicuro che non cambierebbe proprio nulla, niente potrebbe influenzare il risultato finale."


translate italian epilogue_uv_ulya_a78021fb:


    uv "Forse hai ragione…"


translate italian epilogue_uv_ulya_c9107c2d:


    "Ha detto Yulya in tono triste."


translate italian epilogue_uv_ulya_065ddf5d:


    me "Possiamo solo aspettare, dunque.{w} Non ci vorrà molto…"


translate italian epilogue_uv_ulya_476454b2:


    "Ha appoggiato la sua testa sulla mia spalla."


translate italian epilogue_uv_ulya_ce617998_13:


    "…"


translate italian epilogue_uv_ulya_46ee7ab5:


    "Stavamo viaggiando ormai da molto tempo, mi è sembrata un'eternità.{w} Il sole era già tramontato ed è calata la notte."


translate italian epilogue_uv_ulya_f2b3c332:


    "I secondi duravano ore, i minuti – duravano anni."


translate italian epilogue_uv_ulya_1d001f7b:


    "I pionieri sono rimasti in un silenzio tombale, e persino quell'altro, quello arrogante, non ha tentato di iniziare una conversazione con noi."


translate italian epilogue_uv_ulya_63c82e69:


    "Quel silenzio era più forte di qualsiasi grido."


translate italian epilogue_uv_ulya_a6d65f6d:


    "Un semplice battito di mani sarebbe stato sufficiente a frantumare i miei timpani in mille pezzi."


translate italian epilogue_uv_ulya_27bb93cc:


    "Anche io e Yulya siamo rimasti in silenzio."


translate italian epilogue_uv_ulya_b8a45f9a:


    "Era già stato detto tutto, non sono rimaste altre parole da pronunciare."


translate italian epilogue_uv_ulya_8a17dcd9:


    "Mi sentivo completamente vuoto. La mia testa era pesante come il piombo e sembrava che fossi già morto, ma Dio e il Diavolo si erano appena dimenticati di me, e così avrei dovuto trascorrere l'eternità da solo con questa silente folla di pionieri."


translate italian epilogue_uv_ulya_d8530ed5:


    "Sono davvero i miei compagni di sventura?"


translate italian epilogue_uv_ulya_ee366b28:


    "Sono davvero arrivati in questo Campo anche loro, trascorrendo una settimana e poi andandosene…?{w} E poi ripetendo sempre lo stesso ciclo…?"


translate italian epilogue_uv_ulya_be5a413d:


    "Forse mi sarebbe toccata la stessa sorte, come mi ha detto quel pioniere?"


translate italian epilogue_uv_ulya_4231c343:


    "Tutto è possibile.{w} Tutto è possibile in questo momento…"


translate italian epilogue_uv_ulya_ce617998_14:


    "…"


translate italian epilogue_uv_ulya_79c1defa:


    "Ho sentito il russare tranquillo di Yulya – si era addormentata."


translate italian epilogue_uv_ulya_7ac9cad9:


    "Tutto quello stress dev'essere stato davvero troppo per lei."


translate italian epilogue_uv_ulya_bbfcefb4:


    "O forse aveva esaurito tutte le sue energie per gridare."


translate italian epilogue_uv_ulya_6a4ba34d:


    "In ogni caso, è meglio affrontare la fine mentre si dorme."


translate italian epilogue_uv_ulya_8c9884a6:


    "Ora ero assolutamente certo che questa fosse veramente la fine."


translate italian epilogue_uv_ulya_413ab6f9:


    "È stupido aggrapparsi alla speranza, mentre si sta in piedi sull'orlo di un precipizio, circondato da innumerevoli nemici."


translate italian epilogue_uv_ulya_c1ca73df:


    "E anche se ci fosse la minima possibilità di salvarsi, non la si considererebbe comunque."


translate italian epilogue_uv_ulya_9ad0b36a:


    "Forse non dovrebbe essere così, e qualcun altro sarebbe più ottimista al mio posto…"


translate italian epilogue_uv_ulya_9d4ef48c:


    "Dovevo solo compiere l'ultimo passo."


translate italian epilogue_uv_ulya_1802ad39:


    "Ma perché poi?{w} Queste non sono delle scale, ma un ascensore – arriverò a destinazione in ogni caso."


translate italian epilogue_uv_ulya_e8c2c59c:


    "Ho chiuso gli occhi e la mia coscienza mi ha abbandonato immediatamente.{w} Mi sono addormentato."


translate italian epilogue_uv_ulya_3d844e6d:


    "Non saprei dire se fosse durato un'ora o un'eternità…"


translate italian epilogue_uv_ulya_1c9980b4:


    "Il vuoto non è il luogo migliore per la riflessione."


translate italian epilogue_uv_ulya_e6517a54:


    "Quando non c'è nulla, l'essenza del mondo normale scompare. Alla fine, anche i pensieri sono una parte di esso, seppur immateriale."


translate italian epilogue_uv_ulya_c43fb6ec:


    "È difficile riuscire ad immaginare dove termini l'esistenza. Una persona non dovrebbe essere in grado di comprenderlo."


translate italian epilogue_uv_ulya_040c5913:


    "Ho ripreso conoscenza nella più totale oscurità. Per essere precisi, ho ripreso la capacità di percepire me stesso come persona. Non c'era nient'altro, né intorno a me, né all'interno di me."


translate italian epilogue_uv_ulya_c8cdb262:


    "Era come se avessi cessato di esistere, come se fossi stato cancellato dall'universo. Ma non ero morto, no. Mi sentivo completamente diverso, come se mi fossi spostato dallo spazio tridimensionale ad uno spazio monodimensionale. Il mondo intero si era ridotto alle dimensioni di un singolo punto."


translate italian epilogue_uv_ulya_cd386160:


    "Quel punto ero io, era ogni cosa. Era tutto e niente. Tuttavia la mia coscienza, i miei ricordi, la mia anima, tutto si è compresso nello stesso piccolo puntino."


translate italian epilogue_uv_ulya_05274ca7:



    nvl clear
    "Non avevo paura. Forse perché non ce n'era bisogno, o forse perché ancora non riuscivo a capire cosa stesse accadendo."


translate italian epilogue_uv_ulya_c8872b3a:


    "Ho perso la cognizione del tempo. Il concetto di tempo qui semplicemente non esisteva."


translate italian epilogue_uv_ulya_8f5b6bf9:


    "Ecco perché mi risultava impossibile dire per quanto {i}tempo{/i} io fossi rimasto in quel vuoto. Alla fine, dopo un'ora o dopo un anno, ho iniziato a sentire una voce calma. Non riuscivo a capire le sue parole, ma si stava rivolgendo a me. A dire il vero, non c'era nessun altro qui."


translate italian epilogue_uv_ulya_592b8e1f:


    bush "Semyon… Semyon…"


translate italian epilogue_uv_ulya_e79d0fd5:


    me "Sì?"


translate italian epilogue_uv_ulya_051cae6f:


    "Ho risposto, come se avessi alzato la cornetta del telefono."


translate italian epilogue_uv_ulya_20edba06:


    bush "Piacere di conoscerti."


translate italian epilogue_uv_ulya_a66b5a14:


    "La voce sembrava molto vicina."


translate italian epilogue_uv_ulya_e7620f07:



    nvl clear
    me "…"


translate italian epilogue_uv_ulya_0f765061:


    bush "Anche se ci conosciamo già da molto tempo, in realtà."


translate italian epilogue_uv_ulya_0b34c373_1:


    me "Chi sei?"


translate italian epilogue_uv_ulya_336dcb9a:


    bush "Io sono te."


translate italian epilogue_uv_ulya_5ff017df:


    me "Sono morto...?"


translate italian epilogue_uv_ulya_58a18029:


    bush "No. Niente del genere!"


translate italian epilogue_uv_ulya_380cc324:


    "La voce era tranquilla, come se stesse parlando ad un vecchio amico."


translate italian epilogue_uv_ulya_2de7bf97:


    me "Cosa mi sta succedendo?"


translate italian epilogue_uv_ulya_60e86f67:


    bush "Hai raggiunto la fine del tuo percorso."


translate italian epilogue_uv_ulya_a7f92d54:


    me "Quale percorso?"


translate italian epilogue_uv_ulya_6b49f2aa:


    bush "Sei riuscito ad abbandonare il Campo."


translate italian epilogue_uv_ulya_aa801636:



    nvl clear
    "Il Campo… sì, mi ricordo! Il campo di pionieri «Sovyonok»! Improvvisamente mi sono ricordato quegli ultimi sette giorni. Mi sono ricordato di com'ero stato lasciato lì da solo, insieme con Yulya."


translate italian epilogue_uv_ulya_72917697:


    me "E dov'è…"


translate italian epilogue_uv_ulya_66bc60d2:


    bush "Yulya?"


translate italian epilogue_uv_ulya_fc30fd2b_2:


    me "Sì."


translate italian epilogue_uv_ulya_e4066547:


    bush "È proprio qui."


translate italian epilogue_uv_ulya_6af67ff0:


    "Mi sono guardato attorno, ma riuscivo a vedere solo il vuoto."


translate italian epilogue_uv_ulya_dfae19f4:


    me "Non la vedo."


translate italian epilogue_uv_ulya_31083230:


    bush "Non dovresti vederla, dovresti percepirla."


translate italian epilogue_uv_ulya_98f21fc2:


    "Non ero per niente teso, come se mi stessi riprendendo da un'anestesia."


translate italian epilogue_uv_ulya_fb31367b:



    nvl clear
    me "Non percepisco… non percepisco niente."


translate italian epilogue_uv_ulya_44de386e:


    bush "Devi solo riuscire a ricordare…"


translate italian epilogue_uv_ulya_be28a3d4:


    "Dopo quelle parole, migliaia di immagini, sagome, odori, pensieri, sono riapparsi nella mia mente. Il Campo! Allora ci sono stato per davvero! Con Slavya, con Lena, con Alisa, con Ulyana, con Masha… E poi… Ma come? Ricordo chiaramente di essermene andato e di essere tornato nel mondo reale. E poi ho incontrato Slavya alla fermata dell'autobus… No, ho incontrato Ulyana all'università. Anzi, ho incontrato Alisa ad un concerto! Ma avevo anche vissuto con Lena per molti anni! No, ho avuto una vita completamente diversa con Masha. Ma com'è possibile che siano accadute tutte quelle cose? Ricordavo gli eventi di quasi cinque vite diverse in ogni loro singolo dettaglio. Ma tutto era accaduto contemporaneamente… E sono sicuro che fosse tutto reale!"


translate italian epilogue_uv_ulya_63fe795d:



    nvl clear
    me "Come…?"


translate italian epilogue_uv_ulya_243def4b:


    bush "Sì, tutto questo in realtà è successo a te."


translate italian epilogue_uv_ulya_04098bfe:


    me "Ma come?"


translate italian epilogue_uv_ulya_b9df3187:


    bush "Hai lasciato il Campo. Più di una volta. Poi hai vissuto la tua vita normale. Poi sei tornato nuovamente e te ne sei andato ancora…"


translate italian epilogue_uv_ulya_d749d691:


    me "Quindi è come se fossi morto e poi risorto?"


translate italian epilogue_uv_ulya_e7832e23:


    bush "No. Si può supporre che tutto sia accaduto contemporaneamente."


translate italian epilogue_uv_ulya_3aad82ef:


    me "Non capisco…"


translate italian epilogue_uv_ulya_a1dd0efd:


    bush "Non è necessario."


translate italian epilogue_uv_ulya_dd924d72:


    me "E chi erano quegli altri pionieri?"


translate italian epilogue_uv_ulya_bb23dee2:


    bush "Anche loro erano te."


translate italian epilogue_uv_ulya_51149e68:


    me "…"


translate italian epilogue_uv_ulya_3beec0f4:


    bush "Gli altri lati di te."


translate italian epilogue_uv_ulya_d3bd0899_2:


    me "Non capisco."


translate italian epilogue_uv_ulya_f0aeb1ea:



    nvl clear
    bush "Ogni persona è un universo infinito. Le sue azioni determinano lo svolgersi della sua esistenza. Ti ricordi quelle cinque volte perché in quelle occasioni hai compiuto delle scelte che ti hanno portato fuori dal Campo. Le tue altre repliche hanno agito in modo diverso in qualche altro punto. Tutto qui – è così semplice!"


translate italian epilogue_uv_ulya_3ad789cf:


    "Per me non era così semplice."


translate italian epilogue_uv_ulya_dc739d10:


    me "E cos'è quel Campo?"


translate italian epilogue_uv_ulya_50953e17:


    bush "Consideralo come un banco di prova."


translate italian epilogue_uv_ulya_0c39b064:


    me "E tu chi sei?"


translate italian epilogue_uv_ulya_eb217c40:


    bush "Io sono te, ma da un tempo molto distante. Non il tempo come lo conosci tu. Sono al di là e al di sopra di tutti voi. Puoi considerarmi il Semyon primordiale."


translate italian epilogue_uv_ulya_82187263:


    me "Dunque sei tu la ragione di tutto questo?"


translate italian epilogue_uv_ulya_85323ec4:


    bush "Non è esattamente così. Tutto ciò non è avvenuto secondo la mia volontà. Ma posso controllare questi eventi."


translate italian epilogue_uv_ulya_b1002e1e:


    me "Ma perché...?"


translate italian epilogue_uv_ulya_8d4e756e:


    bush "Hai qualcosa in contrario?"


translate italian epilogue_uv_ulya_8ec353e5:


    "Mi sono perso nei miei pensieri. O almeno mi sono perso quanto più ho potuto, dato l'oscuro stato in cui mi trovavo."


translate italian epilogue_uv_ulya_7479b07e_2:


    me "Non lo so…"


translate italian epilogue_uv_ulya_ec67e222:



    nvl clear
    bush "Se potessi scegliere adesso, quando ormai sai tutto e ricordi ogni cosa, sceglieresti davvero di ricominciare da zero dimenticandoti di tutto questo?"


translate italian epilogue_uv_ulya_7479b07e_3:


    me "Non lo so…"


translate italian epilogue_uv_ulya_b81c8065:


    "Non ero pronto a rispondere a una domanda così difficile."


translate italian epilogue_uv_ulya_f9d2f152:


    me "Comunque, dov'è Yulya?"


translate italian epilogue_uv_ulya_e4066547_1:


    bush "È proprio qui."


translate italian epilogue_uv_ulya_de833a1f:


    me "Dove?"


translate italian epilogue_uv_ulya_273c343b:


    bush "Dentro di te."


translate italian epilogue_uv_ulya_05dc229b:


    me "Com'è possibile...?"


translate italian epilogue_uv_ulya_f7dbb331:


    bush "Yulya è il tuo mondo interiore, la tua coscienza che ha preso forma."


translate italian epilogue_uv_ulya_ad9639d3:


    "Non riuscivo a capire le sue parole."


translate italian epilogue_uv_ulya_40ec5fc2:



    nvl clear
    bush "Non è necessario che tu capisca, te l'ho già detto."


translate italian epilogue_uv_ulya_a337cbd5_1:


    me "Ma…"


translate italian epilogue_uv_ulya_7b47e97d:


    "Sentivo che mi mancava."


translate italian epilogue_uv_ulya_017e0287:


    bush "Non preoccuparti. Lei sarà sempre con te."


translate italian epilogue_uv_ulya_ea951d31:


    "Non credo di aver desiderato un'unione del genere ieri."


translate italian epilogue_uv_ulya_c7814762:


    bush "La cosa più difficile è trovare la pace interiore, non la pace con un nemico. E tu ci sei riuscito. Se davvero vuoi amarla, devi prima imparare ad amare te stesso."


translate italian epilogue_uv_ulya_51149e68_1:


    me "…"


translate italian epilogue_uv_ulya_4f767ea3:


    bush "Beh, questo è tutto. È giunto il momento per me di lasciarti."


translate italian epilogue_uv_ulya_3d6b4645:


    me "Aspetta! Che ne sarà di me?"


translate italian epilogue_uv_ulya_3c28c280:


    bush "Tornerai indietro."


translate italian epilogue_uv_ulya_08b0cbb8:


    "Già non capivo cosa fosse reale e cosa non lo fosse. O quante realtà esistessero…"


translate italian epilogue_uv_ulya_25f7abbc:


    me "E dove?"


translate italian epilogue_uv_ulya_73290988:


    bush "Dipende solo da te."


translate italian epilogue_uv_ulya_3aad82ef_1:


    me "Non capisco…"


translate italian epilogue_uv_ulya_e8aa7b7a:


    bush "Ora ti sei ricordato tutti i possibili scenari."


translate italian epilogue_uv_ulya_040643f7:



    nvl clear
    "Ha fatto una lunga pausa, e io non avevo il coraggio di dire nulla, per evitare di interromperlo accidentalmente."


translate italian epilogue_uv_ulya_1813dbe0:


    bush "Quindi, hai almeno cinque possibili opzioni."


translate italian epilogue_uv_ulya_ca002c69:


    me "Mi ricorderò di… tutto questo?"


translate italian epilogue_uv_ulya_05b44d12:


    bush "Spetta a te decidere."


translate italian epilogue_uv_ulya_67c8ae4f:


    "Una traccia di emozione è apparsa nella sua voce per un istante – forse gioia o forse insoddisfazione."


translate italian epilogue_uv_ulya_2569d811:


    bush "Addio."


translate italian epilogue_uv_ulya_a337cbd5_2:


    me "Ma…"


translate italian epilogue_uv_ulya_76b858e0:


    "Sentivo che la voce non mi avrebbe parlato mai più."


translate italian epilogue_uv_ulya_76b2fe88:


    nvl clear


translate italian epilogue_uv_ulya_ce617998_15:


    "…"


translate italian epilogue_uv_ulya_8011f019:


    "Il tempo scorreva. Ho provato a decidere cosa fare in seguito. Se tutto è come ha detto quella voce, allora non mi resta che desiderare."


translate italian epilogue_uv_ulya_9f15618d:


    "A quel punto potevo dire di essermi calmato del tutto, o persino rallegrato un po'. Ero del tutto sicuro che fossi riuscito ad andarmene da quel Campo incantato. Inoltre, un futuro luminoso mi attendeva. Non è che lo credessi, riuscivo proprio a ricordarlo!"


translate italian epilogue_uv_ulya_40f78451:


    "Ma cosa dovrei scegliere…"


translate italian epilogue_uv_ulya_1424cf31:



    nvl clear
    "Ho chiuso gli occhi e ho immaginato chiaramente il volto di Slavya che mi sorrideva. Lei era sempre disposta ad aiutarmi, e non mi ha mai rimproverato nulla. Con lei mi sono sempre sentito tranquillo e al sicuro. E quell'incontro alla fermata dell'autobus… Credo di non aver mai pensato di stare con lei nella vita reale. Ma sarebbe fantastico, ne sono sicuro!"


translate italian epilogue_uv_ulya_c62fe1c5:



    nvl clear
    "Nel corso di tutti quegli anni Lena era diventata veramente preziosa per me. In realtà ricordo bene tutto il tempo trascorso con lei. Mi ricordo i nostri bambini, ricordo i momenti di gioia e di dolore, ricordo tutto. Sarebbe difficile rifiutare tutto questo."


translate italian epilogue_uv_ulya_b374bc69:



    nvl clear
    "O Alisa, che abbraccia caldamente la sua chitarra? Con lei non ci si può annoiare. Forse è la vita che ho sempre sognato – semplicemente rilassarsi e divertirsi? Forse è la ragazza che ho sempre desiderato – un maschiaccio fedele? L'immagine di Alisa nei miei pensieri sorrideva sornione."


translate italian epilogue_uv_ulya_a8c0d905:



    nvl clear
    "Anche la cresciuta Ulyana sembrava una buona opzione. Naturalmente nel Campo non l'avevo mai trattata come una donna, ma adesso... Ovviamente anche lei aveva i suoi difetti, ma allo stesso tempo c'era un qualcosa di magico in lei, un qualcosa di attraente."


translate italian epilogue_uv_ulya_248b80e1:



    nvl clear
    "E, naturalmente, Miku… la Masha di quell'altra vita. Era veramente preziosa per me. Forse {i}laggiù{/i} nel Campo si è trattato solo di un piccolo episodio, e io potrei essere una persona completamente diversa dal «me stesso» degli altri percorsi, ma è meglio così! Nuove opportunità, nuove prospettive, e la persona che amo, una persona con la quale probabilmente ho condiviso tutta la mia vita."


translate italian epilogue_uv_ulya_1f451166:



    nvl clear
    "Ma è comunque triste che io non potrò rivedere mai più Yulya…"


translate italian epilogue_uv_ulya_1805ca4b:


    uv "Certo che puoi!"


translate italian epilogue_uv_ulya_be7a8c7c:


    "La sua voce è risuonata nella mia testa."


translate italian epilogue_uv_ulya_3e8460dd:


    me "Cosa? Dove sei?"


translate italian epilogue_uv_ulya_8b87fadc:


    uv "Sono proprio qui. Come ha detto lui."


translate italian epilogue_uv_ulya_23a24352:


    me "Tu lo sapevi...?"


translate italian epilogue_uv_ulya_2c864d61:


    uv "No. L'ho capito solo quando siamo arrivati qui."


translate italian epilogue_uv_ulya_bf6c77a3:


    me "Ti va bene così? Non ti mancherò?"


translate italian epilogue_uv_ulya_938ec256:


    uv "Certo che non mi mancherai! Sarò sempre con te!"


translate italian epilogue_uv_ulya_51149e68_2:


    me "…"


translate italian epilogue_uv_ulya_9f6f75fc:


    uv "Ti ricordi la promessa che mi hai fatto?"


translate italian epilogue_uv_ulya_ad02f53a_1:


    me "Quale promessa?"


translate italian epilogue_uv_ulya_989f34dd:


    uv "Che resterai sempre con me!"


translate italian epilogue_uv_ulya_9513cd87_2:


    me "Sì…"


translate italian epilogue_uv_ulya_25c487ea:


    uv "Non pensare nemmeno di infrangerla! Ti terrò d'occhio!"


translate italian epilogue_uv_ulya_19a7bed2:



    nvl clear
    "Non so se riuscirò mai a parlare di nuovo con lei in questo modo. Forse si potrebbe semplicemente impazzire per tutto questo, ma per adesso sono felice così."


translate italian epilogue_uv_ulya_601ae3ae:


    me "D'accordo. Non me lo scorderò."


translate italian epilogue_uv_ulya_b9d89c90:


    uv "Allora non ti resta che fare una scelta."


translate italian epilogue_uv_ulya_8b728ec7:


    "Ho percepito Yulya scomparire, svanire nel labirinto della mia mente."


translate italian epilogue_uv_ulya_3d5436bd:


    me "Bene, è giunto il momento!"


translate italian epilogue_uv_ulya_867f9349:


    "Ho chiuso i miei occhi, e…"


translate italian epilogue_uv_ulya_76b2fe88_1:


    nvl clear


translate italian epilogue_uv_ulya_ce617998_16:


    "…"


translate italian epilogue_uv_ulya_442941ca:


    "……"


translate italian epilogue_uv_ulya_b010f1b9:


    "………"


translate italian epilogue_uv_ulya_04c1f529:


    "… I miei occhi sono stati colpiti dall'abbagliante luce solare."


translate italian epilogue_uv_ulya_a7f58933:


    "Non mi sorprende – dopotutto è estate."


translate italian epilogue_uv_ulya_e544a3a0:


    "Di nuovo quello strano sogno."


translate italian epilogue_uv_ulya_8dcc0023:


    "Perché mi capita così spesso ultimamente?"


translate italian epilogue_uv_ulya_7a5325b0:


    "Forse c'è un motivo!"


translate italian epilogue_uv_ulya_4a2fcf60:


    "Ma perché?"


translate italian epilogue_uv_ulya_0212e193:


    "Un campo di pionieri, delle ragazze, la leader..."


translate italian epilogue_uv_ulya_19d5f5ed:


    "Non sono mai stato al Campo."


translate italian epilogue_uv_ulya_e018ccaf:


    "Forse non te lo ricordi?"


translate italian epilogue_uv_ulya_00b53382:


    "Molto divertente…"


translate italian epilogue_uv_ulya_51119d14:


    "E se invece fosse così?"


translate italian epilogue_uv_ulya_49a0f6ca:


    "Fidati di me, mi ricordo chiaramente tutto quello che mi è successo da quando avevo tre anni."


translate italian epilogue_uv_ulya_22e170fb:


    "E prima di allora probabilmente ero inconsapevole."


translate italian epilogue_uv_ulya_b1a60837:


    "Anche se fosse così, tutto questo non può essere una coincidenza."


translate italian epilogue_uv_ulya_413b28d9:


    "Perché no?"


translate italian epilogue_uv_ulya_6eb9c6a9:


    "Ci sono persone che vedono un gatto nero nei loro sogni ogni notte, e non succede nulla!"


translate italian epilogue_uv_ulya_e04a3f3a:


    "Una frase inventata all'istante?"


translate italian epilogue_uv_ulya_1db77900:


    "Non me la sono inventata. L'ho letta di recente."


translate italian epilogue_uv_ulya_85928523:


    "Un gatto nero è una cosa, un sogno così dettagliato è un'altra."


translate italian epilogue_uv_ulya_d2a78cd5:


    "Io non vedo molta differenza."


translate italian epilogue_uv_ulya_21380b7e:


    "Ok. Allora cosa mi dici di quella voce?"


translate italian epilogue_uv_ulya_1b443b08:


    "Quale voce?"


translate italian epilogue_uv_ulya_c90f6cbf:


    "Non fingere di non capire."


translate italian epilogue_uv_ulya_9087d0df:


    "Non ne avevo intenzione."


translate italian epilogue_uv_ulya_ac53bb9c:


    "La voce che ti ha detto di fare una scelta."


translate italian epilogue_uv_ulya_fb191570:


    "Ebbene, una voce. E allora?"


translate italian epilogue_uv_ulya_8b52ac40:


    "C'era un motivo."


translate italian epilogue_uv_ulya_2edc20f2:


    "Ma non ricordo altro – il sogno è terminato lì."


translate italian epilogue_uv_ulya_8545d90e:


    "Forse te lo dimentichi al risveglio?"


translate italian epilogue_uv_ulya_82f9910a:


    "Forse, ma…"


translate italian epilogue_uv_ulya_355a343b:


    "Ma cosa?"


translate italian epilogue_uv_ulya_09d75c72:


    "Questo significa che hai fatto effettivamente una scelta."


translate italian epilogue_uv_ulya_e1a2e98c:


    "Non ricordo nemmeno le opzioni."


translate italian epilogue_uv_ulya_6dd1edbf:


    "Ce n'erano forse alcune?"


translate italian epilogue_uv_ulya_4f6b1ca8:


    "Avrebbero dovuto esserci!"


translate italian epilogue_uv_ulya_5d58786f:


    "Come sempre, non cerchi di trattare in modo serio nemmeno le cose importanti!"


translate italian epilogue_uv_ulya_9ccde1c2:


    "Oddio! È così importante!"


translate italian epilogue_uv_ulya_a58e1faa:


    "Non ha senso parlare con te..."


translate italian epilogue_uv_ulya_a20cefa7_1:


    "..."


translate italian epilogue_uv_ulya_bcd935d1:


    "Recentemente ho iniziato a notare di avere qualcosa di simile a una doppia personalità."


translate italian epilogue_uv_ulya_9923c087:


    "Un monologo interiore è certamente un fenomeno normale per qualsiasi persona, ma quando si trasforma in un dialogo…"


translate italian epilogue_uv_ulya_ce76dee3:


    "Beh, c'è ancora molta strada da fare per poter essere considerato un caso clinico, perché parlo con me stesso e non con un'altra personalità dentro la mia mente."


translate italian epilogue_uv_ulya_357f3a65:


    "Forse con il mio subconscio…"


translate italian epilogue_uv_ulya_6ace32a6:


    "Ma non importa!"


translate italian epilogue_uv_ulya_aa527417:


    "Mi sono voltato dall'altro lato.{w} Il sole accecante brillava ancora nei miei occhi."


translate italian epilogue_uv_ulya_85ea5c99_1:


    uv "Buongiorno!"


translate italian epilogue_uv_ulya_e1ffc0eb:


    sl "Buongiorno!"


translate italian epilogue_uv_ulya_83a3b49d:


    dv "Buongiorno!"


translate italian epilogue_uv_ulya_5567bace:


    un "Buongiorno!"


translate italian epilogue_uv_ulya_cb60f8f8:


    us "Buongiorno!"


translate italian epilogue_uv_ulya_e8f1372a:


    ma "Buongiorno!"


translate italian epilogue_uv_ulya_cfb99cb8:


    dreamgirl "Buongiorno!"


translate italian epilogue_uv_ulya_0a6d11b5:


    me "Buongiorno!"


translate italian epilogue_uv_ulya_348dbde8:


    "Ho risposto."


translate italian epilogue_uv_ulya_bc8dedc6:


    "Scelta… Ho forse mai avuto scelta…?"


translate italian epilogue_uv_ulya_d99864b1:


    "Ogni storia ha il suo inizio e la sua fine."


translate italian epilogue_uv_ulya_4d69c034:


    "Ogni storia ha il suo contorno, la sua sinossi, i suoi contenuti, i punti chiave, un prologo e un epilogo."


translate italian epilogue_uv_ulya_754a4f66:


    "E non esiste libro che, se letto nuovamente, non riesca a rivelare nuovi dettagli che non sono stati notati in precedenza."


translate italian epilogue_uv_ulya_39cdaa6f:


    "Ma ogni libro ha la sua ultima pagina. E dopo averla girata, dobbiamo rimettere il libro sullo scaffale."


translate italian epilogue_uv_ulya_611077c4:


    "Solo per aprirne uno nuovo domani…"
# Decompiled by unrpyc: https://github.com/CensoredUsername/unrpyc
