
translate italian epilogue_main_7a98964b:


    "Un dolore acuto pulsava attraverso tutto il mio corpo."


translate italian epilogue_main_7c0ecf1c:


    "Era particolarmente forte nelle tempie."


translate italian epilogue_main_28766e67:


    "Era come se fossero pronte a frantumarsi in mille piccoli pezzi, dando al vento il libero accesso alla mia testa vuota."


translate italian epilogue_main_1f5640c9:


    "Probabilmente non sarei stato in grado di sopportare anche solo cinque minuti di tale tortura, se non avessi aperto gli occhi in tempo."


translate italian epilogue_main_4d647778:


    "Ero {i}da qualche parte{/i}, ma mi era impossibile dire esattamente dove."


translate italian epilogue_main_81be611c:


    "La mia mente era offuscata dalla nebbia, i miei pensieri erano confusi."


translate italian epilogue_main_0c4d877e:


    "L'esatto momento che intercorre tra l'inconscio e la coscienza, quando inizi a ricordare vividamente i tuoi sogni."


translate italian epilogue_main_e050dc5d:


    "Stavo andando da qualche parte in autobus e stavo per addormentarmi, quando improvvisamente una ragazza si è avvicinata a me e ha cominciato a parlare rapidamente."


translate italian epilogue_main_5d58d05f:


    "Non sono stato capace di capire una singola parola, ma la ragazza sembrava molto turbata."


translate italian epilogue_main_6bdcd82e:


    "Non riuscivo a capire che cosa volesse da me."


translate italian epilogue_main_21564d96:


    "Il tempo scorreva, e lei continuava a parlare e parlare."


translate italian epilogue_main_bb180213:


    "Stava diventando davvero fastidiosa."


translate italian epilogue_main_23d9757b:


    "Avrei voluto chiederle di smetterla, o quantomeno di calmarsi un po', ma non ce l'ho fatta – forse perché non riuscivo a dire nulla, o perché le mie parole non l'avevano raggiunta."


translate italian epilogue_main_14eb51b4:


    "Non sono riuscito nemmeno a vedere il suo volto. Stavo semplicemente ascoltando e guardando, guardando e ascoltando..."


translate italian epilogue_main_685c283d:


    "Allora forse non era così importante per me."


translate italian epilogue_main_f176077d:


    "Sicuramente non si ha la necessità di ricordarsi l'aspetto di una zanzara che ti ronza attorno impedendoti di dormire."


translate italian epilogue_main_79c81bc5:


    "Di sicuro non si potrebbe rilevare la frequenza di battito delle sue ali, o l'inclinazione della sua proboscide."


translate italian epilogue_main_065ff797:


    "E quella ragazza era solo una delle milioni di voci che interferivano con la concentrazione, con il pensiero, con il desiderio di addormentarsi…"


translate italian epilogue_main_66488078:


    "E più la voce si faceva forte, più era difficile per me riuscire a capire le sue parole."


translate italian epilogue_main_829ffeb0:


    "L'interno del bus, i sedili trasandati, il pavimento irregolare, il soffitto arrugginito, il vetro incrinato – tutto stava scivolando via dalla mia mente.{w} Assieme a lei."


translate italian epilogue_main_1281b344:


    "E poi ho sentito un sollievo indescrivibile."


translate italian epilogue_main_de7746b8:


    "Non importava quanto fossero stati reali il bus e la ragazza – per me non erano altro che una fastidiosa zanzara."


translate italian epilogue_main_6f529678:


    "Ed eccomi qui, sospeso nel vuoto più totale, in caduta libera dentro a un sogno…"


translate italian epilogue_main_ed7f45f7:


    "Wow, i sogni possono diventare così assurdi…{w} Non era esattamente un incubo, ma sicuramente non qualcosa di piacevole."


translate italian epilogue_main_4ad85600:


    "Mi sono alzato, mi sono massaggiato il viso per tornare alla realtà, ho fatto un forte sbadiglio e mi sono preparato a lavarmi."


translate italian epilogue_main_2ebebacc:


    "Era una mattina decisamente grigia."


translate italian epilogue_main_6695bb75:


    "Incapace di trovare il mio kit per l'igiene sul comodino, ho deciso che avrei potuto farne a meno."


translate italian epilogue_main_66d354ed:


    "È stato difficile vestirsi – le mie mani tremavano dannatamente."


translate italian epilogue_main_571f5d50:


    "Mi sono guardato allo specchio e ho controllato la mia barba ispida."


translate italian epilogue_main_c4b53f19:


    "Chissà se Olga Dmitrievna ha un rasoio da qualche parte…"


translate italian epilogue_main_602cb68c:


    "Solo dopo che sono uscito dalla stanza mi sono improvvisamente reso conto di non essere in un campo di pionieri!"


translate italian epilogue_main_f8f2d3b4:


    "Sono tornato al mio appartamento!"


translate italian epilogue_main_7132ec6d:


    "E appena uscito dalla stanza sono finito nel corridoio, e non fuori dalla casetta!"


translate italian epilogue_main_d7d976c7:


    "Sono stato sopraffatto dallo sgomento, dalla sorpresa, dal timore, e persino dal terrore."


translate italian epilogue_main_f8f1fe90:


    "Ma come?!"


translate italian epilogue_main_4b63500a:


    "Mi sono seduto sul letto e ho sepolto il viso tra le mani, cercando di ricordare la catena di eventi di ieri."


translate italian epilogue_main_c0692396:


    "Sì, il bus, l'ultimo giorno della sessione."


translate italian epilogue_main_28a24bd1:


    "Sì, mi sono addormentato…{w} e mi sono svegliato di nuovo a «casa»."


translate italian epilogue_main_e6a0051c:


    "Ebbene, in una certa misura, potrebbe anche sembrare abbastanza logico."


translate italian epilogue_main_9b3b626a:


    "Probabilmente il culmine del mio sbalordimento si è manifestato durante i primi secondi."


translate italian epilogue_main_2b7cb0bd:


    "Dopotutto, il fatto che io fossi tornato in tal modo dopo un'assenza di una settimana non è più strano della mia improvvisa apparizione in un qualche campo di pionieri degli anni ottanta."


translate italian epilogue_main_94b8b09a:


    "Gli eventi degli ultimi due giorni mi sono balenati dinanzi agli occhi in un istante."


translate italian epilogue_main_4cdec556:


    "Quel misterioso pioniere, le sue parole…"


translate italian epilogue_main_16f67736:


    "Ma allora come sono riuscito a tornare alla realtà?"


translate italian epilogue_main_302cfd4b:


    "Secondo le sue teorie, sarei dovuto restare bloccato {i}laggiù{/i} per l'eternità, in quanto non c'era alcuna via di scampo!"


translate italian epilogue_main_da19ba98:


    "D'altra parte, non ero l'unico."


translate italian epilogue_main_c8c85310:


    "Quell'altro pioniere alla fermata dell'autobus e quella voce misteriosa avevano sottolineato il contrario – che esisteva una via d'uscita."


translate italian epilogue_main_d51d1680:


    "Ciò potrebbe significare che l'ho trovata, che sono riuscito a sfuggire dal ciclo infinito?{w} Ma come?"


translate italian epilogue_main_2d5435f1:


    "Tuttavia, non ero sicuro se io dovessi gioire o piangere – durante la settimana scorsa mi ero ormai abituato ai miei monologhi interiori, alla ricerca di risposte e all'analisi approfondita di ogni cosa, quindi non potevo semplicemente accettare tutto così com'era, senza cercare di capire cosa mi fosse successo esattamente."


translate italian epilogue_main_797ee454:


    "Certo, lui mi aveva detto che io ero l'unico ad aver visto gli altri miei simili già nel primo giro, ma ciò vuol forse dire qualcosa?"


translate italian epilogue_main_6d0aec47:


    "In ogni caso, tutte le sue teorie e le sue idee sono crollate istantaneamente come un castello di carte!"


translate italian epilogue_main_117f74f8:


    "Ora non mi restava che decidere come reagire a tutto questo."


translate italian epilogue_main_f5b5600e:


    "Ovviamente dovrei gioire!{w} Dopotutto sono tornato al mondo reale."


translate italian epilogue_main_9d077188:


    "Forse gli ultimi sette giorni sono stati solo un sogno."


translate italian epilogue_main_ef3f3b75:


    "In effetti, non c'è assolutamente alcuna prova che io sia stato veramente {i}laggiù{/i}."


translate italian epilogue_main_6bf2c0b3:


    "Non vedo la mia uniforme da pioniere da nessuna parte, sembro avere l'età che avevo prima.{w} Il mio telefono è sul tavolo, completamente carico."


translate italian epilogue_main_9854060f:


    "Ma non si può ingannare la propria memoria.{w} Un semplice sogno non può provocare delle emozioni così terribilmente reali."


translate italian epilogue_main_e2ba07bf:


    "Ricordo ancora ogni singolo dettaglio di quella settimana."


translate italian epilogue_main_b42bf578:


    "Forse sono stato in coma per tutto il tempo?"


translate italian epilogue_main_7db54ad1:


    "Mi sono lasciato sfuggire una risata ironica."


translate italian epilogue_main_37cb3d33:


    "Nah, mi sembra improbabile."


translate italian epilogue_main_59c846f5:


    "E dunque... tutto è bene quel che finisce bene?"


translate italian epilogue_main_fdf1c653:


    "Le ultime ore trascorse nel Campo sono balenate nella mia mente."


translate italian epilogue_main_376c1a1c:


    "In realtà non speravo di uscirne – sia da quel Campo che da quella realtà – ed ero praticamente pronto per un'altra settimana.{w} E poi un'altra, e poi un'altra ancora…"


translate italian epilogue_main_bff86508:


    "Avevo accettato il mio ruolo, mi ero riconciliato col mio destino."


translate italian epilogue_main_4a86a5af:


    "E cosa dovrei fare adesso, dopo tutte le cose che sono successe?"


translate italian epilogue_main_c6b1ea80:


    "Ho fatto un sospiro rassegnato, mi sono alzato dal letto con notevole fatica, e mi sono messo davanti al computer."


translate italian epilogue_main_0dba5f6f:


    "Che strano, secondo l'orologio sono passate solo quattordici ore dalla mia scomparsa da questo mondo, e non una settimana intera."


translate italian epilogue_main_295da44b:



    nvl clear
    "C'è un nuovo messaggio in chat…"


translate italian epilogue_main_8ffdf5b6:


    "{i}ciao Semyon! ieri... è stato troppo forte! c sntiamo + tardi! :))){/i}"


translate italian epilogue_main_da755414:


    "Il mio amico del college. È stato lui che mi ha invitato a quella festa con il resto dei suoi amici, a cui stavo per partecipare."


translate italian epilogue_main_6c9bac4f:


    "Improvvisamente ho ripreso i sensi: un forte mal di testa, le vertigini, tutti i sintomi di una sbornia."


translate italian epilogue_main_393d03d9:


    "Sembra che io sia proprio andato a quella festa ieri. E mi sono ubriacato alla grande… Questo significa che non c'è stato nessun campo di pionieri? Ma allora, da dove provenivano tutti quei ricordi, tutti quei sentimenti, tutte quelle emozioni?"


translate italian epilogue_main_dc6e0b16:



    nvl clear
    "Era tutto così incomprensibile che mi sono infuriato. Ho cominciato a gridare oscenità, cercando di strapparmi i capelli e martellando la tastiera con i pugni. Mi sono fermato solo dopo che tutti i tasti erano volati in aria."


translate italian epilogue_main_f36bf9ba:


    "Perché dovrebbe importarmi così tanto? Nessuno si preoccupa quando un sogno o un'allucinazione finisce. Inoltre, le persone di solito sono contente che ciò accada. Ero davvero così desideroso di rimanere in quel mondo, che il mio improvviso ritorno alla realtà si è rivelato essere così indesiderato per me?"


translate italian epilogue_main_5f5b219a:



    nvl clear
    "E poi un pensiero semplice e ovvio mi ha attraversato la mente. Forse sto semplicemente impazzendo…"


translate italian epilogue_main_2500664b:


    "In effetti, i pazzi hanno spesso visioni che percepiscono come realtà. Inoltre ero affetto da tutti i sintomi tipici della follia: non ho mai lasciato le quattro pareti della mia prigione, non ho mai socializzato e ho un sacco di problemi psicologici. Beh, questo spiegherebbe molte cose!"


translate italian epilogue_main_18e12dde:



    nvl clear
    "La mia tetra e nervosa risata ha echeggiato nella stanza."


translate italian epilogue_main_92e8ada7:


    "Immagino che sia giunto per me il momento di far visita a un terapista. O meglio uno psicologo. O forse dovrei solo farmi rinchiudere in un manicomio?"


translate italian epilogue_main_636725da:


    me "«Un cavallo! Un cavallo! Metà del mio regno per un cavallo!»"


translate italian epilogue_main_5f0e3c02:


    "Ho gridato a pieni polmoni tra gli attacchi della mia risata maniacale."


translate italian epilogue_main_ecb4a2ab:


    me "«Non discutere con lei! È una pazza!»"


translate italian epilogue_main_56059efa:


    "Il colpo dei miei vicini contro la parete è riuscito a calmarmi in qualche modo."


translate italian epilogue_main_5eb2202b:



    nvl clear
    "Se sto marcando così male, allora forse solo una lobotomia potrebbe aiutarmi."


translate italian epilogue_main_ce617998:


    "…"


translate italian epilogue_main_258159ac:


    "Sono rimasto seduto per diverse ore, completamente fuori dal mondo. Senza far nulla, senza pensare a nulla, limitandomi a fissare lo schermo, da tempo entrato in modalità standby. Alla fine ho mosso il mouse e ho aperto un paio di schede nel browser. Allora, che c'è di nuovo? F5, F5!"


translate italian epilogue_main_81eac3b9:


    "Come se tutti gli eventi della scorsa settimana non fossero mai accaduti. Dai, devi per forza soffrire per un'allucinazione? Era reale? Certamente. Un sacco di ricordi? Sicuro. Sembrava abbastanza vera? Decisamente sì! Qualche prova? Nessuna."


translate italian epilogue_main_6e5d6669:



    nvl clear
    "Il verdetto è chiaro, come lo è il mio futuro. Ho sospettato a lungo che il mio stile di vita non mi stesse facendo per niente bene. Non ero pronto per un evento così insolito, ma che importa? Dovrò essere più pronto per il mio prossimo viaggio."


translate italian epilogue_main_ce617998_1:


    "…"


translate italian epilogue_main_76b2fe88:


    nvl clear


translate italian epilogue_main_8e39de8c:


    "La notte. Un flusso infinito di fili e immagini hanno allontanato la mia mente dai pensieri tristi e dalle introspezioni. E poi la finestrella di un nuovo messaggio è apparsa sullo schermo del computer."


translate italian epilogue_main_b336ac6e:


    message "Ciao, come va?"


translate italian epilogue_main_e3463832:


    "Che modo originale per iniziare una conversazione."


translate italian epilogue_main_e7ce88cd:


    me "Bene."


translate italian epilogue_main_931ab60c:


    "Il profilo del contatto era completamente vuoto. Nemmeno un soprannome – solo un numero di nove cifre. Non si può indovinare che tipo di persona ci sia all'altra estremità basandosi semplicemente su un numero, giusto? Certo, in futuro potrebbe essere tutto diverso. Identificatori digitali unificati al posto dei nomi, un paio di simboli per una biografia, diversi byte di personalità e di tre bit e mezzo di sentimenti…"


translate italian epilogue_main_dd99d86f:



    nvl clear
    message "Sei tornato a casa tutto intero?"


translate italian epilogue_main_387fcb02:


    "Probabilmente è qualcuno della festa di ieri."


translate italian epilogue_main_830dea84:


    me "A quanto pare."


translate italian epilogue_main_707b84c6:


    message "Ti ricordi tutto quello che ti ho detto ieri, vero?"


translate italian epilogue_main_f5ebe454:


    me "Beh... Non credo."


translate italian epilogue_main_62a3a2c6:


    "Dopo una lunga pausa, è apparso un altro messaggio."


translate italian epilogue_main_ca8f6404:



    nvl clear
    message "Capisco."


translate italian epilogue_main_998b1726:


    "Onestamente, non me ne frega niente di chi sia costui è del perché mi stia scrivendo. Ma finché questo individuo non mi darà problemi, posso facilmente tollerare la sua presenza nella mia lista dei contatti."


translate italian epilogue_main_33de6e67:


    message "Beh, allora va bene. Ci incontreremo di nuovo. Ne ho la certezza."


translate italian epilogue_main_f5239078:


    "Certo, non vedo l'ora, sì..."


translate italian epilogue_main_c92f92b9:



    nvl clear
    me "D'accordo."


translate italian epilogue_main_e24c9d81:


    message "Ciao!"


translate italian epilogue_main_0e18a85a:


    "Non ho risposto."


translate italian epilogue_main_f8d54840:


    "Questo strano e incasinato giorno finalmente si stava avvicinando lentamente alla sua fine. Dopo essere andato a letto, ho ripercorso ancora una volta nella mia mente tutti quegli eventi che sembravano essere così reali per me. Dove mi risveglierò domani? Torneranno ancora quelle allucinazioni? No, probabilmente mi dimenticherò di tutto."


translate italian epilogue_main_95611f9f:



    nvl clear
    "Strano, ma adesso sento che tutto questo sia davvero reale. {i}In quel luogo{/i} tutto era una finzione, un gioco di un'immaginazione deformata, o comunque qualche strana fantasia, e {i}qui{/i} invece è reale. A volte scomodo, a volte si sviluppa in modo diverso da come si vorrebbe, a volte ti fa sorprese come questa, ma è sempre la realtà!"


translate italian epilogue_main_fb80d3eb:


    "Mi sono addormentato con questi pensieri in testa."


translate italian epilogue_main_ce617998_2:


    "…"


translate italian epilogue_main_76b2fe88_1:


    nvl clear


translate italian epilogue_main_c07a412b:


    "A poco a poco la mia vita ha ripreso il suo corso naturale, e ho iniziato a dimenticare gli eventi di quella settimana. Dopotutto, niente di brutto era accaduto. Anche se fosse stato effettivamente reale, chi se ne frega? Sono stati solo sette giorni passati in un campo di pionieri. Non ero stato ucciso, nessuno mi aveva rapito per usarmi come cavia, non mi era stato fatto il lavaggio del cervello, quindi perché dovrebbe importarmi del motivo?"


translate italian epilogue_main_d7bc5196:



    nvl clear
    "E se davvero fosse stata un'allucinazione, potevo solo sperare che non si ripetesse."


translate italian epilogue_main_76b2fe88_2:


    nvl clear


translate italian epilogue_main_1c95a7cd:


    "Ero ancora un recluso. Il computer e internet erano i miei unici amici, e la tastiera era l'unico canale di comunicazione che mi collegava al mondo esterno. Il tasto F5 è diventata la cosa più importante della mia vita. Pensandoci bene però, non era poi così male."


translate italian epilogue_main_76b2fe88_3:


    nvl clear


translate italian epilogue_main_88474bfc:


    "Eppure un giorno un insieme familiare di cifre è spuntato sullo schermo – quello strano contatto è entrato di nuovo in chat."


translate italian epilogue_main_68c6e51f:


    message "Ehi!"


translate italian epilogue_main_a87c71d7:


    me "Ehi."


translate italian epilogue_main_15fd667b:


    message "Come te la passi?"


translate italian epilogue_main_e7ce88cd_1:


    me "Bene."


translate italian epilogue_main_c56a1499:


    "Probabilmente l'ho conosciuto da qualche parte. Ma chi se ne frega, comunque…"


translate italian epilogue_main_fcdab43c:



    nvl clear
    message "Qualche novità?"


translate italian epilogue_main_dd5093d8:


    me "Non direi."


translate italian epilogue_main_d166e979:


    message "La stabilità è un indice di vera classe?"


translate italian epilogue_main_dd69c3a1:


    me "Si potrebbe dire così."


translate italian epilogue_main_e43109d5:


    message "Oh, capisco."


translate italian epilogue_main_e99ffbdc:


    "La conversazione era finita per me, ma lui non sembrava pensarla allo stesso modo."


translate italian epilogue_main_efe11ea9:



    nvl clear
    message "Quindi, non è cambiato niente?"


translate italian epilogue_main_8dd0767d:


    me "No. Perché, dovrebbe?"


translate italian epilogue_main_6fa5b1d2:


    message "Beh, non è che si possano vivere quel genere di cose ogni giorno! E poi non tutti riescono a viverle dopotutto…"


translate italian epilogue_main_1084810b:


    me "Di cosa stai parlando?"


translate italian epilogue_main_3c014c6c:


    message "Non l'hai ancora capito?"


translate italian epilogue_main_67b3ef9f:


    "Stavo giusto iniziando."


translate italian epilogue_main_a160c0c8:



    nvl clear
    me "Vorrei che mi spiegassi."


translate italian epilogue_main_ba495f57:


    message "Non ti ricordi del Campo?"


translate italian epilogue_main_907853ad:


    "È stata come una scossa elettrica! Tutti gli eventi di quella settimana immediatamente hanno inondato la mia mente. Le mie mani hanno cominciato a tremare, sudavo freddo. Mi sono voltato con orrore, già immaginando che avrei visto alieni, fantasmi, o il Cupo Mietitore. Ma non c'era nessuno nella mia stanza, tranne me, come sempre. Sono corso verso il corridoio, ho controllato in cucina e in bagno – ancora nessuno. Alla fine, sono tornato al computer. La stessa finestra continuava a lampeggiare costantemente, come se si trattasse di un messaggio perfettamente normale."


translate italian epilogue_main_56c768f9:



    nvl clear
    me "Chi sei tu?"


translate italian epilogue_main_b69f1bc8:


    "Le mie mani tremavano talmente tanto che non riuscivo a premere i tasti giusti."


translate italian epilogue_main_56d58b67:


    message "Oh, e così te lo ricordi?"


translate italian epilogue_main_30227704:


    "Forse le mie allucinazioni sono tornate? O magari non ho mai lasciato il Campo? O forse è tutto un sogno, dopotutto? O…"


translate italian epilogue_main_54a73f72:


    me "Non so cosa dire."


translate italian epilogue_main_3a4edaaa:


    "Ho ammesso con sincerità. In ogni caso, sentivo che quell'entità (che era l'unica definizione che mi è venuta in mente) non avesse bisogno di una connessione internet per comunicare con me."


translate italian epilogue_main_bbccea89:



    nvl clear
    message "Puoi anche rimanere in silenzio, è tutto chiaro come il sole xD"


translate italian epilogue_main_a073eb1b:


    "Quella faccina alla fine della frase sembrava il diavolo in persona, che spalancava le sue fauci deformate, pronto a divorarmi."


translate italian epilogue_main_e1d6b6aa:


    me "Cosa mi sta succedendo?"


translate italian epilogue_main_d850b239:


    "Alla fine sono riuscito a fargli quella domanda."


translate italian epilogue_main_5e3cf096:


    message "Niente. Niente di niente. Stai solo vivendo la tua vita normale."


translate italian epilogue_main_39faba7a:


    me "Ma il Campo… Era reale?"


translate italian epilogue_main_93656c09:


    message "E tu cosa credi?"


translate italian epilogue_main_4515fb38:


    me "Non ne sono sicuro."


translate italian epilogue_main_83608308:


    message "Se ti è sembrato reale, allora era reale. Se invece non ti è sembrato, allora non lo era. È così semplice."


translate italian epilogue_main_d1986394:



    nvl clear
    "Non è per niente semplice!"


translate italian epilogue_main_a87d627d:


    me "Che ne sarà di me adesso?"


translate italian epilogue_main_9db6b90d:


    "Ho sussurrato."


translate italian epilogue_main_ab2d839e:


    message "Nulla di cui preoccuparsi."


translate italian epilogue_main_7b4418d5:


    "Sembrava davvero che riuscisse a sentirmi senza bisogno di internet. Il terrore mi ha avvolto completamente, ho chiuso gli occhi e mi sono arricciato come una palla."


translate italian epilogue_main_dfa8be82:


    me "Perché, perché mi stai facendo questo?"


translate italian epilogue_main_4d3bcab6:


    "Stavo impazzendo. Le parole uscivano dalla mia bocca da sole. Sequenze inutili e illogiche di parole."


translate italian epilogue_main_9ee3c724:



    nvl clear
    me "Perché? Perché io? Perché? Per quale ragione? Lasciami in pace! Non ho fatto nulla di male!"


translate italian epilogue_main_ae870164:


    "Diversi minuti più tardi ho trovato il coraggio di aprire un occhio e ho guardato il monitor."


translate italian epilogue_main_148f12c7:


    message "Tu sei la ragione di tutto."


translate italian epilogue_main_3f769b22:


    "La finestra del messaggio tremolava con freddezza."


translate italian epilogue_main_5f3254a2:


    me "E adesso cosa accadrà?"


translate italian epilogue_main_a1f21fcf:


    message "Oh, lo scoprirai presto! :)"


translate italian epilogue_main_76b2fe88_4:


    nvl clear


translate italian epilogue_main_74902b45:


    "Tutto è iniziato ad oscurarsi davanti ai miei occhi, il rumore mi ha riempito le orecchie e ho iniziato ad avere le vertigini. Sentivo che la mia anima stava abbandonando il suo involucro fisico. Gli eventi degli ultimi giorni sono diventati confusi, come se tutto fosse successo a qualcun altro. Poi i ricordi del Campo hanno cominciato a svanire, come se qualcuno stesse usando una gomma per cancellare completamente le parole scritte con una matita. Poco dopo non sentivo più nulla, come se mi fossi immerso in uno stato di beatitudine cosmica."


translate italian epilogue_main_f7590cd3:


    "Se qualcuno mi avesse visto adesso, avrebbe notato che stavo sorridendo…"


translate italian epilogue_main_a20cefa7:


    "..."


translate italian epilogue_main_529298f6:


    me "Eh... Sono un po' bloccato qui."


translate italian epilogue_main_6f3ba371:


    "Ho premuto ripetutamente il pulsante di segnalazione di spam, ho chiuso la finestra, sono passato al browser e ho guardato l'orologio."


translate italian epilogue_main_612c456e:


    "È ora di andare, o farò tardi..."


translate italian epilogue_main_a20cefa7_1:


    "..."


translate italian epilogue_un_bad_0da63856:


    "Ci sono momenti in cui la realtà diventa irrilevante, insignificante, indegna di attenzione."


translate italian epilogue_un_bad_6693cd84:


    "Ci sono momenti in cui il tuo tormento spirituale eclissa tutto il resto."


translate italian epilogue_un_bad_092e6505:


    "E anche se il mondo finisse – non te ne accorgeresti.{w} Se un coltello dovesse trafiggerti – non lo noteresti.{w} Persino il bollire per l'eternità nei calderoni dell'inferno sembrerebbe solo un piccolo inconveniente..."


translate italian epilogue_un_bad_92427898:


    "Alla fin fine, ci sono problemi più gravi di tutto ciò…"


translate italian epilogue_un_bad_ab4e3f76:


    "Quando ho aperto gli occhi mi sono reso conto che qualcosa non andava."


translate italian epilogue_un_bad_089c801c:


    "Ho impiegato un po' di tempo per tornare ai miei sensi, per schiarirmi la mente."


translate italian epilogue_un_bad_2638c66a:


    "Alla fine mi sono reso conto che non ero su un autobus, ma…{w} nel mio vecchio appartamento."


translate italian epilogue_un_bad_7c7bf187:


    "Beh, c'era da aspettarselo."


translate italian epilogue_un_bad_724c6d60:


    "Come se avessi trascorso un'intera settimana a preparare un esame e, all'ultimo istante, l'avessi fallito clamorosamente."


translate italian epilogue_un_bad_c9575f91:


    "E il risultato di questo fallimento è stato il mio ritorno al mondo reale…"


translate italian epilogue_un_bad_95da6ae5:


    "Tuttavia, ora non mi sembra affatto più reale del campo «Sovyonok»."


translate italian epilogue_un_bad_f7d76256:


    "Non c'è da meravigliarsi. La realtà è ciò che puoi ascoltare, sentire, toccare e gustare.{w} E tutto questo era veramente laggiù."


translate italian epilogue_un_bad_85abf9cd:


    "Quel mondo era reale fino al minimo dettaglio. A volte mi sembrava più che fosse la mia vita passata, ad essere una finzione."


translate italian epilogue_un_bad_f527260f:


    "E ora devo ricordare come riuscire ad esistere qui…"


translate italian epilogue_un_bad_459b2ee7:


    "Eppure... perché dovrei?{w} Mi sento come qualcuno che è stato gettato fuori da un'automobile in corsa senza nemmeno accorgersene."


translate italian epilogue_un_bad_fd341608:


    "Giacendo sul ciglio della strada, con le braccia e le gambe rotte, mentre l'automobile scompare nella notte, portando con sé le ultime vestigie di speranza."


translate italian epilogue_un_bad_b99b2263:


    "Lena…"


translate italian epilogue_un_bad_6f0311f7:


    "La sua immagine è emersa così chiaramente nella mia mente febbricitante, che ho avuto un insopportabile bisogno di scoppiare a piangere."


translate italian epilogue_un_bad_ae8f3acd:


    "Anzi – volevo gridare, strapparmi i capelli, battere i pugni sulla parete, emettere urla disumane."


translate italian epilogue_un_bad_f7554966:


    "Tuttavia, c'era un vuoto nella mia anima."


translate italian epilogue_un_bad_c0d3b99f:


    "Ho cercato invano di trovare almeno l'eco di un dolore, un senso di colpa, o un dispiacere nei suoi confronti, ma non ho trovato proprio nulla."


translate italian epilogue_un_bad_18791a54:


    "Me ne stavo sdraiato immobile a fissare il soffitto…"


translate italian epilogue_un_bad_53f73d1e:


    "Non ero affatto interessato al come o al perché io fossi tornato qui."


translate italian epilogue_un_bad_5e02e068:


    "Alla fin fine, a chi importa della burocrazia quando si vende l'anima al diavolo, tutte le formalità legali del contratto, le firme, i timbri, i sigilli?"


translate italian epilogue_un_bad_db394f22:


    "Ciò che conta è il risultato.{w} E questo è il risultato che ho ottenuto."


translate italian epilogue_un_bad_5e75b8df:


    "No, non è per il fatto di essere tornato in questa realtà!{w} Se avessi raggiunto la destinazione di quel bus, difficilmente sarebbe cambiato qualcosa per me."


translate italian epilogue_un_bad_53be3572:


    "L'unica cosa che conta veramente è ciò che è successo a Lena.{w} Ed è successo a causa delle mie azioni.{w} Ne sono assolutamente certo."


translate italian epilogue_un_bad_a3b352cb:


    "Tutto sommato, lei non avrebbe potuto farlo senza un valido motivo…{w} No, Lena non era così!{w} Quindi è stata tutta colpa mia!"


translate italian epilogue_un_bad_7f92c544:


    "È difficile riuscire a vivere, sapendo di essere la ragione della morte di una persona."


translate italian epilogue_un_bad_1b453803:


    "Come se fossi stato io stesso a tenere in mano quel coltello, e con calma le avessi tagliato delicatamente le vene, osservandola morire lentamente.{w} E poi sono corso via…"


translate italian epilogue_un_bad_bd552dae:


    "Di certo non ho potuto fare nulla in quella situazione, ma mi sembrava comunque di essermi comportato da codardo."


translate italian epilogue_un_bad_1f993547:


    "Anzi, peggio ancora…"


translate italian epilogue_un_bad_0c858154:


    "Ma è davvero così importante fornire una giusta definizione delle mie azioni?"


translate italian epilogue_un_bad_c763d1a6:


    "Mi sono maledetto per essere rimasto così calmo nel pensare a quella situazione."


translate italian epilogue_un_bad_d14867bb:


    "Dopotutto, dovrei essere in lutto per Lena e incolpare me stesso…{w} Ma ora nulla dipende più da me.{w} Se non sono stato in grado in quell'occasione…"


translate italian epilogue_un_bad_f7620350:


    "Ho rabbrividito, il mio corpo ha iniziato a tremare, mi mancava il respiro."


translate italian epilogue_un_bad_7a23b79f:


    "L'istinto di autoconservazione ha sopraffatto i sensi di colpa per un po', e ho barcollato in cucina per prendere un calmante."


translate italian epilogue_un_bad_8325eee3:


    "Sono sempre presenti nella credenza di ogni persona antisociale come me."


translate italian epilogue_un_bad_06a8cbb9:


    "Dopo aver preso metà delle pillole che erano nel pacchetto, sono tornato nella mia stanza e ho acceso il computer.{w} Rimanere in silenzio era insopportabile."


translate italian epilogue_un_bad_561abd10:


    "Ho fatto partire la prima canzone che ho trovato, e ben presto mi sono reso conto che probabilmente era il pezzo più deprimente dell'intera mia collezione musicale nel mio computer."


translate italian epilogue_un_bad_2233adb0:


    "Comunque non ho voluto spegnerlo – quei suoni hanno contribuito a smorzare i miei pensieri."


translate italian epilogue_un_bad_5fa296f2:


    "Devo decidere cosa fare della mia vita."


translate italian epilogue_un_bad_d453809f:


    "Ma di una cosa sono certo – tutto ciò che è successo al Campo, la mia improvvisa apparizione, il mio ritorno inaspettato, niente di tutto ciò ha più alcuna importanza per me."


translate italian epilogue_un_bad_a36af85e:


    "E non solo quello, non mi interessano neppure il contesto o le ragioni di tali eventi."


translate italian epilogue_un_bad_6fb09a75:


    "L'unica cosa che contava era Lena."


translate italian epilogue_un_bad_081d7480:


    "Ho pensato con orrore alla parola «era».{w} In effetti, se n'è andata per sempre…"


translate italian epilogue_un_bad_cc0ad615:


    "Naturalmente, è possibile che si fosse trattato solo di un sogno, e che lei non fosse mai esistita."


translate italian epilogue_un_bad_810ea4a2:


    "D'altronde, anche questa realtà potrebbe essere il delirio di qualcuno."


translate italian epilogue_un_bad_0403a983:


    "Perché no?{w} Se le persone soffrono per la perdita dei loro cari {i}qui{/i}, perché dovrei considerare la morte di Lena {i}là{/i} come un semplice frutto della mia fantasia malata?!"


translate italian epilogue_un_bad_ff643764:


    "Ho visto tutto con i miei occhi. Ho percepito lo shock, la paura e lo spavento…"


translate italian epilogue_un_bad_88638ccf:


    "Dannazione!{w} Per me non {i}è stato{/i} reale, lo {i}è{/i} ancora adesso."


translate italian epilogue_un_bad_9dedbadc:


    "Lo è, e lo sarà sempre!"


translate italian epilogue_un_bad_e2eb86dd:


    "E sono sicuro che non sto sognando ad occhi aperti!"


translate italian epilogue_un_bad_e27cd146:


    "Anche se forse sarebbe stato meglio sognarsi tutto…"


translate italian epilogue_un_bad_59ef3021:


    "I terribili accordi delle chitarre tuonavano dagli altoparlanti.{w} Sembrava un requiem.{w} Un requiem per me…"


translate italian epilogue_un_bad_cb6f7baf:


    "E ora dovrei convivere con questo senso di colpa?{w} No, non posso sopportarlo!"


translate italian epilogue_un_bad_034f1df0:


    "La mia mente non è l'esempio perfetto di stabilità, ma…{w} Nemmeno una persona con una mente stabile sarebbe in grado di sopportare uno shock come questo."


translate italian epilogue_un_bad_167e249d:


    "E mi sembra già d'impazzire!"


translate italian epilogue_un_bad_14cf330d:


    "Ho cercato di reprimere tutti quei pensieri.{w} No, non per non dimenticare, ma per riposare un po'."


translate italian epilogue_un_bad_6ff084a7:


    "Solo per un istante…"


translate italian epilogue_un_bad_41d75612:


    "Ma non ha funzionato – il dolore continuava a torturarmi ancora e poi ancora.{w} Riuscivo a percepirlo anche fisicamente.{w} Ad ogni modo, il dolore fisico è sempre più debole di quello mentale…"


translate italian epilogue_un_bad_04d0ffcc:


    "Sono caduto a terra, stringendo le mie gambe e cominciando a dondolare avanti e indietro in posizione fetale."


translate italian epilogue_un_bad_d629c94e:


    "Il sangue mi pompava nella testa così pesantemente che era come se il mio cranio potesse frantumarsi da un momento all'altro."


translate italian epilogue_un_bad_ddf2c20b:


    "Ho colpito involontariamente una gamba del tavolo, e una piccola candela è rotolata da sotto una pila di fogli."


translate italian epilogue_un_bad_a52bd6cd:


    "Una piccola candela, lunga una ventina di centimetri, era piegata a circa novanta gradi, ma manteneva ancora la sua forma. Lo stoppino sporgeva da un'estremità."


translate italian epilogue_un_bad_210e40a9:


    "Ho cercato un accendino e l'ho accesa."


translate italian epilogue_un_bad_510f67d0:


    "In memoria di Lena…"


translate italian epilogue_un_bad_55f5fa34:


    "Forse adesso lei è in un altro mondo, e si sente meglio di…"


translate italian epilogue_un_bad_ce617998:


    "…"


translate italian epilogue_un_bad_be1b0bed:


    "Mi sono seduto sul pavimento e ho guardato la candela consumarsi lentamente, mentre la cera colava sulle mie dita."


translate italian epilogue_un_bad_a54003ea:


    "Non sentivo alcun dolore – probabilmente il mio sistema nervoso era così esausto che non era in grado di trasmettere gli impulsi nervosi al cervello."


translate italian epilogue_un_bad_ef0a06ae:


    "Il fuoco mi ha calmato un po' – mi sono limitato a fissare la fiamma, senza pensare a niente."


translate italian epilogue_un_bad_160075f5:


    "Finalmente un po' di pace mentale…"


translate italian epilogue_un_bad_ce617998_1:


    "…"


translate italian epilogue_un_bad_f07d7c82:


    "La candela si è già consumata per metà."


translate italian epilogue_un_bad_bf7d5546:


    "All'improvviso ho immaginato che la mia vita fosse proprio quella candela."


translate italian epilogue_un_bad_f97deac3:


    "Non solo la mia – la vita di ogni persona."


translate italian epilogue_un_bad_7d4f29f6:


    "Tutto ciò che ci è stato donato dalla nascita è la sua lunghezza."


translate italian epilogue_un_bad_a017ae8e:


    "Ma tutto può succedere – il vento potrebbe soffiare, la mano che la tiene potrebbe tremare, o lo stoppino potrebbe smettere di bruciare…{w} E così una vita potrebbe finire prematuramente."


translate italian epilogue_un_bad_2ebac29f:


    "Ma dopotutto, ogni candela è diversa!"


translate italian epilogue_un_bad_550851ec:


    "Come ad esempio questa – 9 centesimi; quelle a doppio spessore – 20 centesimi; e quelle grandi, spesso larghe come il manico di un badile – 1,85."


translate italian epilogue_un_bad_986feabd:


    "Mi chiedo: la candela di Lena si è spenta prematuramente, oppure era semplicemente più piccola delle altre?"


translate italian epilogue_un_bad_d3c73c95:


    "Anche se dubito che ce ne possa essere una più piccola della mia…"


translate italian epilogue_un_bad_0a488632:


    "Ho ruotato la candela tra le mie mani."


translate italian epilogue_un_bad_06a3d48f:


    "È interessante – potrei decidere di spegnere la fiamma da un momento all'altro, e il gioco è fatto…"


translate italian epilogue_un_bad_1c8c0f19:


    "Ma allo stesso tempo la vita non è la cera, non è possibile combinare due piccole candele per farne una più grande."


translate italian epilogue_un_bad_fe0373f7:


    "Vorrei poter dare a Lena quello che rimane della mia."


translate italian epilogue_un_bad_038be31f:


    "A chiunque ne abbia bisogno più di me!"


translate italian epilogue_un_bad_317fb567:


    "A cosa mi serve? Non sento più il dolore dalla cera che mi cola sulle dita, la sua fiamma non mi sta dando alcun calore, riesce a malapena ad illuminare la stanza."


translate italian epilogue_un_bad_d887153f:


    "In poche parole, è uno spreco di stoppino, di cera e di ossigeno…"


translate italian epilogue_un_bad_7bf48038:


    "Ho spento la candela."


translate italian epilogue_un_bad_87239164:


    "Quel gesto non ha innescato alcuna emozione in me."


translate italian epilogue_un_bad_aed890bf:


    "Mi sono alzato lentamente e sono andato in bagno…"


translate italian epilogue_un_bad_ce617998_2:


    "…"


translate italian epilogue_un_bad_a61ee024:


    me "Tagliare lungo il braccio, non attraverso il polso…{w} Tutti gli altri lo fanno attraverso il polso, ma tu lo farai lungo il…"


translate italian epilogue_un_bad_50614f77:


    "L'immagine di Lena è apparsa davanti ai miei occhi."


translate italian epilogue_un_bad_efd90378:


    "Stava sorridendo."


translate italian epilogue_un_bad_8c835c15:


    me "Ci incontreremo sicuramente di nuovo…{w} Mi dispiace così tanto…"


translate italian epilogue_un_bad_9ee997b0:


    "L'acqua calda, le ombre e i sogni morenti mi hanno portato finalmente un po' di pace."


translate italian epilogue_un_bad_16861ec1:


    "Potrebbe anche essere che io non fossi mai stato in quel Campo."


translate italian epilogue_un_bad_487e661d:


    "O che non sia mai tornato indietro."


translate italian epilogue_un_bad_1c0fe513:


    "Ma ormai non importa…"


translate italian epilogue_un_bad_cb89588c:


    "Riesco quasi a percepire fisicamente l'abbraccio di Lena."


translate italian epilogue_un_bad_c4d43861:


    me "Andrà tutto bene…{w} Prenderemo l'autobus insieme e arriveremo a destinazione…{w} Dove nessuno ci potrà mai trovare…{w} Dove saremo felici insieme…"


translate italian epilogue_un_bad_7e153b32:


    "Le forze mi hanno abbandonato, e ho iniziato ad affondare nell'acqua, che stava già fuoriuscendo dal bordo della vasca."


translate italian epilogue_un_bad_8c835c15_1:


    me "Ci incontreremo di sicuro…{w} Perdonami…"


translate italian epilogue_un_bad_ce617998_3:


    "…"


translate italian epilogue_un_good_ced219ed:


    un "Svegliati!"


translate italian epilogue_un_good_2e7081b6:


    "Ho cominciato lentamente a tornare in me."


translate italian epilogue_un_good_451af48e:


    "È come se io stessi cadendo da una scogliera talmente alta che le nuvole erano alla deriva nelle vicinanze."


translate italian epilogue_un_good_6188d36b:


    "Mi stavo avvicinando sempre più al terreno, ma non avevo paura di morire…"


translate italian epilogue_un_good_456fff17:


    "Ho aperto gli occhi."


translate italian epilogue_un_good_9e2c1e5b:


    un "Riusciresti a dormire per tutta la vita in questo modo!"


translate italian epilogue_un_good_ed8bfd35:


    "Lena mi ha guardato con espressione seria."


translate italian epilogue_un_good_0bd39427:


    me "Ah? Che cosa? Siamo già arrivati...?"


translate italian epilogue_un_good_79937daa:


    un "Arrivati dove?"


translate italian epilogue_un_good_211eccb4:


    "Mi sono guardato intorno."


translate italian epilogue_un_good_5efc1f6b:


    "Una panchina, la piazza, Genda…"


translate italian epilogue_un_good_aedb5b04:


    "Ma come?{w} Ero sull'autobus, stavamo andando al centro del distretto."


translate italian epilogue_un_good_576a105f:


    un "Perché mi guardi come se avessi visto un fantasma?"


translate italian epilogue_un_good_8bb49e0e:


    me "Ma noi…{w} Perché io sono ancora qui?!"


translate italian epilogue_un_good_a5d4a4e2:


    "Sono stato assalito dalla paura."


translate italian epilogue_un_good_0113bcd7:


    "Non uscirò mai da questo posto?"


translate italian epilogue_un_good_973d03cf:


    me "Noi eravamo…{w} Sull'autobus… Insieme…"


translate italian epilogue_un_good_5f6db029:


    un "Sull'autobus?"


translate italian epilogue_un_good_ef4c9ffe:


    me "Ieri…"


translate italian epilogue_un_good_77a0ecbc:


    un "Ieri?"


translate italian epilogue_un_good_18b26cc6:


    me "Sì, ieri! Non ti ricordi?"


translate italian epilogue_un_good_d6dd39b3:


    "Lena ha iniziato a pensare."


translate italian epilogue_un_good_5bd0cf6d:


    un "Ieri…{w} Non mi ricordo niente di simile.{w} Forse hai solo fatto un brutto sogno."


translate italian epilogue_un_good_7bef23f0:


    "Considerando che tutto ciò che era accaduto sembrava già un'allucinazione, allora doveva trattarsi di un sogno dentro a un altro sogno."


translate italian epilogue_un_good_5f1d7a74:


    me "Ok, vediamo un po'…"


translate italian epilogue_un_good_54beb87a:


    "Ho cercato di pensare secondo la logica."


translate italian epilogue_un_good_b23cd5f9:


    me "La sessione si è conclusa ieri, tutti i pionieri se ne sono andati, ma noi siamo rimasti…{w} E poi abbiamo preso un autobus e ci siamo diretti verso il centro del distretto."


translate italian epilogue_un_good_a3e6cada:


    "Lena sembrava davvero perplessa."


translate italian epilogue_un_good_5a05f88f:


    un "Mi stai prendendo in giro?"


translate italian epilogue_un_good_5d17104b:


    me "Non sto prendendo in giro nessuno!"


translate italian epilogue_un_good_be6c71e6:


    un "Oggi è l'ultimo giorno."


translate italian epilogue_un_good_56a8dc53:


    me "Aspetta…{w} Quindi, quanto tempo sono stato in questo Campo?"


translate italian epilogue_un_good_71c0ad20:


    "Lena ha iniziato a contare sulle dita."


translate italian epilogue_un_good_45719f1b:


    un "Dovrebbero essere... sette giorni."


translate italian epilogue_un_good_1163b74b:


    me "Non otto?"


translate italian epilogue_un_good_0262d06c:


    un "No, sette, ne sono sicura!"


translate italian epilogue_un_good_7b2a2cf9:


    "Ho sospirato esausto e mi sono coperto il viso con le mani."


translate italian epilogue_un_good_90cfa1fe:


    "Ecco un altro enigma da aggiungere alla lunga lista dei misteri di questo Campo."


translate italian epilogue_un_good_85181956:


    me "Beh, ok…{w} Anche se ho la sensazione che non riusciremo ad andarcene nemmeno oggi."


translate italian epilogue_un_good_b672514d:


    un "Perché?"


translate italian epilogue_un_good_4d730f88:


    me "Sesto senso."


translate italian epilogue_un_good_8d11582d:


    un "Sei un po' strano oggi."


translate italian epilogue_un_good_5b132458:


    me "Suppongo, ho le mie ragioni…"


translate italian epilogue_un_good_9d577beb:


    un "Posso chiederti quali?"


translate italian epilogue_un_good_8f838576:


    "Mi ha fissato con attenzione."


translate italian epilogue_un_good_e86e77ad:


    me "Va bene."


translate italian epilogue_un_good_5ce3e082:


    "Certo, perché no?"


translate italian epilogue_un_good_d2bb6bf4:


    "Quando ho avuto finalmente occasione di lasciare questo Campo, tutto è fallito…"


translate italian epilogue_un_good_87127cb1:


    "E quindi cosa avevo da perdere?"


translate italian epilogue_un_good_0ee8c6fb:


    me "Vediamo, da dove comincio…?"


translate italian epilogue_un_good_9e69a9ba:


    un "Dall'inizio."


translate italian epilogue_un_good_a3854fdc:


    me "Sì, certo…{w} Beh, sono nato, ho studiato, mi sono sposato…{w} Err… Beh, ok, non mi sono ancora sposato…"


translate italian epilogue_un_good_a20e9f2e:


    un "Sei ancora in tempo."


translate italian epilogue_un_good_dce47469:


    me "Sì, ma…{w} Beh… In poche parole, come dire, io non sono di questo mondo."


translate italian epilogue_un_good_97c2c800:


    "Dallo sguardo che mi ha dato, era chiaro che non mi avesse capito affatto."


translate italian epilogue_un_good_1072a05d:


    me "Sono arrivato qui per caso. Non so nemmeno io come."


translate italian epilogue_un_good_7887dd77:


    un "E allora com'è il tuo mondo?"


translate italian epilogue_un_good_c2b423ab:


    "Mi ha chiesto con serietà."


translate italian epilogue_un_good_d1d836af:


    me "È… Beh…{w} È praticamente lo stesso, ma una ventina d'anni più avanti rispetto al… vostro mondo."


translate italian epilogue_un_good_fb5752ef:


    un "Ci sono i viaggi nello spazio?"


translate italian epilogue_un_good_f2fa462b:


    me "Beh, a dire il vero ci sono anche in questa epoca.{w} Tuttavia, non abbiamo fatto ancora grandi progressi in quel campo."


translate italian epilogue_un_good_6b84b606:


    un "Sembra interessante!"


translate italian epilogue_un_good_bac480a2:


    "Mi risultava difficile capire se Lena mi stesse prendendo sul serio o se facesse finta."


translate italian epilogue_un_good_632121f6:


    me "Vivevo in un luogo totalmente diverso. Lontano, a nord, a giudicare dal paesaggio. In una grande città."


translate italian epilogue_un_good_94abe78c:


    un "Non sono mai stata in una grande città…"


translate italian epilogue_un_good_e8e0a840:


    me "Forse è meglio così."


translate italian epilogue_un_good_9790301a:


    un "Mi farai vedere la tua casa?"


translate italian epilogue_un_good_96768537:


    "Sembrava che stesse considerando la conversazione in modo serio tanto quanto me."


translate italian epilogue_un_good_7e5baac7:


    me "Beh, non sono nemmeno sicuro che esista in questo mondo…"


translate italian epilogue_un_good_8d8e7fcf:


    un "E dai!"


translate italian epilogue_un_good_e49a9950:


    "Lena ha messo il broncio."


translate italian epilogue_un_good_481c2e78:


    me "Beh, certo, lo farò…"


translate italian epilogue_un_good_df776ecb:


    un "Come sei arrivato qui?"


translate italian epilogue_un_good_cf4b3576:


    me "Una sera ho preso un autobus, la tratta 410 e… mi sono risvegliato qui.{w} Non so altro."


translate italian epilogue_un_good_39b2719c:


    un "La tratta 410! È quella che passa proprio per questo Campo!"


translate italian epilogue_un_good_57ebfa1e:


    me "Sì, ma il mio 410 viaggiava in un altro luogo… e in un altro tempo."


translate italian epilogue_un_good_1c2133a9:


    un "Vorresti tornare là?"


translate italian epilogue_un_good_78a5a750:


    "Mi ha chiesto Lena, e ho notato una nota di tristezza nella sua voce."


translate italian epilogue_un_good_ad816568:


    me "Non saprei nemmeno come…"


translate italian epilogue_un_good_cf0505aa:


    un "E se lo sapessi?"


translate italian epilogue_un_good_f078db34:


    me "Mi stai facendo una domanda difficile…"


translate italian epilogue_un_good_be29202a:


    un "Che c'è che non va?"


translate italian epilogue_un_good_72d6612e:


    me "Niente, ma…{w} Vedi, ho cominciato solo di recente ad adattarmi a questo mondo.{w} Le mie prime ore in questo Campo sono state un tale shock per me che… Non so…"


translate italian epilogue_un_good_5f555b1e:


    un "Va bene, hai ancora tempo per pensarci."


translate italian epilogue_un_good_b1d14b10:


    "Tempo per pensarci?"


translate italian epilogue_un_good_5540c8a2:


    "Anche se Lena mi credesse, sembrava che considerasse tutta quella situazione come un piccolo inconveniente."


translate italian epilogue_un_good_59c33efd:


    me "In ogni caso, non dipende da me.{w} Probabilmente anche se volessi restare…{w} Capisci, colui che mi ha portato qui, potrebbe riportarmi indietro alla stessa maniera."


translate italian epilogue_un_good_c17e95b3:


    un "Io credo che ogni cosa che accade nella vita di una persona abbia una ragione.{w} Quindi, tu devi essere qui per qualche motivo."


translate italian epilogue_un_good_1b2ffe6d:


    me "Può essere…"


translate italian epilogue_un_good_5db9f7be:


    un "E se non sei ancora tornato…{w} Forse c'è bisogno di te qui."


translate italian epilogue_un_good_da6af767:


    me "Beh, non posso escluderlo del tutto."


translate italian epilogue_un_good_9a80266d:


    mt "Ehi! È ora di fare i bagagli!"


translate italian epilogue_un_good_9719a190:


    "La voce della leader del Campo è risuonata in lontananza."


translate italian epilogue_un_good_7ef75530:


    un "L'hai sentita?"


translate italian epilogue_un_good_2bf6ead1:


    me "Non ho molto da preparare…"


translate italian epilogue_un_good_c09b5abb:


    un "Ma io sì!{w} Allora ci vediamo alla fermata!"


translate italian epilogue_un_good_86be10c0:


    me "Vuoi che ti aiuti?"


translate italian epilogue_un_good_8fa9fd07:


    un "Grazie, ma non ce n'è bisogno."


translate italian epilogue_un_good_22f7b476:


    "Lena ha sorriso ed corsa verso la sua casetta."


translate italian epilogue_un_good_6c54000c:


    "Sono rimasto seduto sulla panchina, cercando di far fronte allo shock."


translate italian epilogue_un_good_c391d6da:


    "I miei pensieri erano ancora avvolti nel caos, le varie teorie si sostituivano a vicenda."


translate italian epilogue_un_good_094c98fd:


    "Da un lato, il fatto che io non riesca ad andarmene da questo Campo non è più strano del mio misterioso arrivo qui, ma…"


translate italian epilogue_un_good_a33f4f66:


    "E adesso? Cosa dovrei fare?{w} C'è forse qualche possibilità che io possa lasciare questo Campo se ci provassi di nuovo?"


translate italian epilogue_un_good_675b9b96:


    "In ogni caso, che senso ha essere nervoso?{w} Ormai so perfettamente che nulla dipende da me."


translate italian epilogue_un_good_70c222fc:


    "Forse devo solo sedermi e godermi il viaggio?"


translate italian epilogue_un_good_ef2764bc:


    "Non sono riuscito a trattenere un sorriso."


translate italian epilogue_un_good_b2d4b2d6:


    "Soprattutto ora, quando ho un motivo in più per non dovermi affrettare ad abbandonare questo luogo.{w} E il motivo è – Lena."


translate italian epilogue_un_good_3ed77c37:


    "Forse lei ha ragione, forse io sono qui per un motivo."


translate italian epilogue_un_good_4a181a23:


    "Alla fine, anche se potessi restringere il campo delle possibili spiegazioni ad un minimo ragionevole, ne rimarrebbero comunque mille e una."


translate italian epilogue_un_good_ce005bce:


    "E se continuassi ad analizzarle in modo estenuante, potrei rischiare di impazzire."


translate italian epilogue_un_good_251ac201:


    "Forse è giunto il momento di fare una scelta?"


translate italian epilogue_un_good_72f1add5:


    "Forse tutto sommato questo mondo non è poi così male.{w} Tanto più che, per la prima volta dopo tanto tempo, ho qualcosa per cui vivere."


translate italian epilogue_un_good_994914a4:


    "È ancora una cosa vaga, ma comunque esiste!"


translate italian epilogue_un_good_8ae8f091:


    "E ora non solo ho il potere di trattenerla, ma anche di svilupparla in qualcosa di più grande."


translate italian epilogue_un_good_88e5892d:


    "Con tali pensieri mi sono alzato e mi sono incamminato verso la casetta di Olga Dmitrievna per mettere in borsa le mie umili cose e lasciare il campo «Sovyonok» per sempre."


translate italian epilogue_un_good_ce617998:


    "…"


translate italian epilogue_un_good_752fc388:



    nvl clear
    "..."


translate italian epilogue_un_good_2000e780:


    "Potrebbe sembrare strano (ed era proprio così che mi è sembrato), ma alla fine abbiamo raggiunto il centro del distretto. Mi ero addormentato sull'autobus. Quando mi sono svegliato, ho iniziato a correre avanti e indietro per il bus, senza fiato, ma ben presto mi sono reso conto che quella lunga settimana era finita!"


translate italian epilogue_un_good_a8aed4e9:


    "Finalmente sono riuscito a liberarmi di quel Campo. Tutto il tempo che ho trascorso lì mi è sembrato molto più di sette giorni… e poi è tutto finito."


translate italian epilogue_un_good_33d7cf72:


    "Ben presto gli eventi si susseguirono con una velocità impressionante."


translate italian epilogue_un_good_b7912630:


    "Non c'era più tempo per le introspezioni, o per la «ricerca di risposte». Inoltre, di tanto in tanto mi dimenticavo completamente del Campo."


translate italian epilogue_un_good_6383c486:


    "Come tutti coloro che si ritrovano in un ambiente del tutto estraneo, io ero completamente perso. Non avevo documenti, nemmeno un semplice certificato di nascita, e nessuna possibilità di guadagnarmi da vivere – le figure professionali come l'esperto di computer o l'operatore di call center non erano richieste."


translate italian epilogue_un_good_b54c638a:



    nvl clear
    "Lena tornò alla sua vita normale. Dovette diplomarsi alla scuola e si preparò per l'università. Il Campo di pionieri si trovava nel sud dell'Unione Sovietica, proprio come avevo pensato. Lena viveva in una città con una popolazione di circa centomila abitanti. Era facile arrivarci in autobus dal centro del distretto."


translate italian epilogue_un_good_c7376b62:


    "Non c'era niente di speciale in quella città. Una grande fabbrica, file di case a cinque piani, capanne di legno in periferia, un negozio di alimentari che chiudeva alle sette di sera e un supermercato, un vero e proprio Eden per un fanatico dello shopping, con un'ampia scelta tra scarpe invernali, cappotti in pelle di pecora per le donne e cappelli di topo muschiato per gli uomini."


translate italian epilogue_un_good_02306dd9:


    "In passato avrei cercato di scappare da un luogo come quello, ma ormai era diventata la mia casa. I casi di morte per fame erano rari nel ventesimo secolo in Europa. Riuscii a trovare un lavoro. Fui assunto come assistente di un tornitore in una fabbrica e mi fu assegnata anche una camera in un ostello."


translate italian epilogue_un_good_e8a877a1:



    nvl clear
    "Nel corso del tempo, quel lavoro inizialmente difficile e strano iniziò a darmi una certa soddisfazione. Ovviamente parziale, che riguardava la mia capacità di riuscire a guadagnarmi la pagnotta. Il tempo passava e feci carriera, diventando il capo di un intero reparto di lavoro."


translate italian epilogue_un_good_6bd80c6f:


    "I miei colleghi erano sorpresi dal mio talento e dalla mia perseveranza, e lo ero pure io. Lena si laureò, mentre invece io trascorrevo diverse notti con una calcolatrice in mano, progettando diagrammi di spese ed entrate, e alla fine mi convinsi a chiedere a Lena di sposarmi."


translate italian epilogue_un_good_8de1f0df:


    "Ora non ricordo esattamente se lei avesse necessitato di molto tempo per pensarci, se mi avesse proposto un contratto di matrimonio, o se avesse protestato per la mancanza di una dote, ma in breve tempo ci trasferimmo in un appartamento comune vicino alla fabbrica, che costava due terzi del mio stipendio."


translate italian epilogue_un_good_ce390941:



    nvl clear
    "Grazie all'istruzione sovietica gratuita per tutti, o forse grazie alla tenacia di mia moglie, alla fine diventai uno studente part time presso il Politecnico locale."


translate italian epilogue_un_good_467cd7ed:


    "In un primo momento protestai, dicendo che un vero proletario non aveva bisogno di agire come i pidocchiosi intellettuali, ma poi capii che un diploma mi avrebbe reso la vita più facile."


translate italian epilogue_un_good_d5213709:


    "Non fu difficile studiare grazie alle conoscenze che già possedevo dal mio precedente tentativo di laurearmi. Il tempo scorreva e il paese cambiava. Naturalmente sapevo che sarebbe dovuto accadere, ma comunque i cambiamenti spuntavano all'improvviso."


translate italian epilogue_un_good_5f973fa2:



    nvl clear
    "Si potrebbe paragonare la cosa all'essere travolti da una valanga in montagna. Puoi prepararti nel miglior modo possibile, ma alla fine verrai sepolto in ogni caso. La fabbrica fu privatizzata e poi chiuse i battenti."


translate italian epilogue_un_good_7676e0eb:


    "Il lavoro in nero come autista privato del catorcio «Kopeyka» che ereditai dal mio suocero non era un business molto redditizio. Ero anche pronto a far domanda per una posizione di leader del campo al «Sovyonok», quando un lontano parente di Lena morì e ci lasciò in eredità un monolocale da qualche parte nella Russia Centrale."


translate italian epilogue_un_good_c490afc8:


    "Dopo una riunione di famiglia decidemmo di trasferirci. Accogliemmo i primi anni '90 nell'angusta cucina di una Khrushchovka, guardando «Il Lago dei Cigni». Grazie a dei lavori occasionali riuscii a mettere da parte un po' di denaro e alla fine decidemmo di trasferirci in una città più grande. E, ovviamente, scelsi la mia città natale…"


translate italian epilogue_un_good_b16966f7:



    nvl clear
    "Decidemmo di vendere l'appartamento e investimmo quei soldi in attività. Per un certo periodo abbiamo vissuto nel lusso, acquistando pellicce e cappotti di visone, costose auto straniere, pranzi in ristoranti e viaggi in altri paesi. Come molti altri in quel periodo avevamo avuto la fortuna di riuscire ad arrivare in alto partendo da poco."


translate italian epilogue_un_good_082d8313:


    "La mia attività riguardava la vendita al dettaglio di materiali da costruzione. Era un mercato redditizio al momento perché la gente guadagnava molto, e aveva molto da spendere per ville lussuose."


translate italian epilogue_un_good_200309d6:


    "Forse quello stile di vita sarebbe potuto andare avanti, ma gli anni '90 venivano chiamati «sbarazzini» per una ragione. Dopo essermi imbattuto in truffe e corruzione, rimanemmo con il frigorifero vuoto, il portafoglio vuoto e il sapore amaro in bocca."


translate italian epilogue_un_good_f5d9692f:



    nvl clear
    "E fu proprio in quel periodo che cercai di ricordare il mio status di «uomo-venuto-dal-futuro». I luoghi di scommesse che erano impensabili nell'epoca sovietica iniziarono a spuntare come funghi, e iniziai a scommettere su tutti gli avvenimenti che mi ricordavo dalla mia vita precedente. Tuttavia, dovetti affrontare molte delusioni, dato che le partite furono vinte da squadre totalmente diverse da quelle che mi ricordavo. Persino la Coppa del Mondo del 1994 non fu vinta dal Brasile."


translate italian epilogue_un_good_353ba4d9:


    "Mentre guardavo le partite in TV tutto andava come prevedevo, ma ogni volta che tentavo di scommettere i miei soldi su qualche evento, allora persino gli sfavoriti cominciavano a battere i favoriti."


translate italian epilogue_un_good_ea090535:


    "Oltre a quello cercai di perseguire la carriera di consulente politico. Tuttavia, non si veniva accettati in una posizione del genere senza contatti speciali o qualsiasi cosa che avrebbe reso le persone interessate. Non so per quanto tempo avrei continuato con tutti quei tormenti, se non fosse stato per un evento che mi stravolse la vita – io e Lena abbiamo avuto un bambino."


translate italian epilogue_un_good_c402b437:



    nvl clear
    "In tutta fretta iniziai a cercare in tutti i modi di guadagnare quanto servisse per mantenere la mia famiglia. Mi ricordai di alcuni vecchi amici di mio padre, e per mezzo di loro, fingendo di essere un lontano parente di me stesso, trovai un posto come analista junior in una banca. Allo stesso tempo, tentai nuovamente di laurearmi (o meglio, di {i}portare a termine{/i} quello che avevo già iniziato)."


translate italian epilogue_un_good_0c3a6b1b:


    "Il tempo passava lentamente. Il denaro era appena sufficiente a riempire i nostri stomaci. Era come se vivessi al lavoro per giorni, cercando di trovare un po' di tempo per i miei esami. Lena rimaneva a casa con il bambino. Fu allora che iniziai a scrivere… Me lo ricordo come se fosse ieri. Fu dopo la mezzanotte, mentre stavo seduto in cucina davanti al mio vecchio e logoro i386, uccidendo selvaggiamente i mostri in «DOOM II»."


translate italian epilogue_un_good_fa74f193:


    "Avevo un'infinita voglia di dormire, ma i videogiochi riuscivano a distrarmi almeno un po' da tutte quelle giornate uggiose. Dopo aver completato l'ennesimo livello fui improvvisamente sommerso dai pensieri riguardanti tutto quello che avevo vissuto negli ultimi anni. Era decisamente troppo per una singola persona. Fu allora che decisi di scrivere tutto ciò che mi era successo."


translate italian epilogue_un_good_65ff89ca:



    nvl clear
    "Aprii un editor di testo e iniziai a scrivere qualche centinaio di parole, con il chiaro intento di continuare l'indomani. Tuttavia, il giorno seguente me ne dimenticai, e pure la settimana seguente."


translate italian epilogue_un_good_a55760f5:


    "Circa un mese dopo, Lena, che stava scrivendo qualcosa sul computer, mi fece tornare in mente il mio libro incompiuto. Lo rilessi e lo cancellai, terrorizzato. In quel momento iniziai a dubitare del fatto che potessi scrivere una cosa del genere. Dopotutto, si trattava di un intero romanzo, e non fui in grado nemmeno di mettere assieme due frasi."


translate italian epilogue_un_good_a0fb86f8:


    "Inoltre, non è facile scrivere di sé stessi… Il mio lato creativo fu messo temporaneamente in un angolo. Festeggiai l'arrivo dell'anno 2000 come specialista con responsabilità di capo dipartimento. Durante il trasloco in un altro appartamento ritrovai il mio vecchio i386 tra la spazzatura che doveva essere gettata via, e mi ricordai del mio tentativo di scrivere la mia biografia."


translate italian epilogue_un_good_b64fc8f9:



    nvl clear
    "Ho riso. Che sciocco ero stato. Tuttavia, dopo qualche tempo aprii il mio computer portatile e scrissi una specie di bozza, una breve storia di un giovane uomo disoccupato senza aspirazioni particolari, che si è ritrovato su un altro pianeta. Lo stile, l'ortografia e la punteggiatura, così come l'idea stessa, erano perfettamente abbinati ad una trama così stupida."


translate italian epilogue_un_good_06d5c1a8:


    "Mostrai la storia a mia moglie. Lei rise, ma disse che era ben scritto e mi consigliò di continuare."


translate italian epilogue_un_good_62204972:


    "Circa un anno dopo, il cassetto adibito al contenimento delle mie opere letterarie era pieno. Trovai la cosa piuttosto strana, anche perché passavo intere giornate al lavoro, a volte anche durante le vacanze, e il mio tempo libero cercavo di trascorrerlo con la mia famiglia. Tuttavia, era proprio così."


translate italian epilogue_un_good_16103041:



    nvl clear
    "In quel periodo decisi di pubblicare i miei racconti. Come con qualsiasi altro lavoro da principianti, la maggior parte delle mie storie furono respinte, ma un paio di esse furono accettate (compresa la mia prima opera). Ero al settimo cielo. Naturalmente non mi pagarono niente, ma le mie opere venivano lette! Il che significava che qualcuno le avrebbe considerate interessanti."


translate italian epilogue_un_good_d17d0576:


    "Il tempo scorreva… Abbiamo avuto un secondo bambino. Durante il mio tempo libero mi dedicai alla scrittura di un romanzo. No, non la mia biografia, né riguardo al Campo di pionieri. Decisi di non toccare quei temi mai più. Tutto ciò che accadde anni fa sembrava essere la volontà di Dio, della cui carità non si deve dubitare."


translate italian epilogue_un_good_bc5da8f3:


    "Chissà cosa sarebbe accaduto se non mi fossi svegliato in un autobus nei pressi dei cancelli del campo «Sovyonok»?"


translate italian epilogue_un_good_d7be6fd0:


    "Un anno più tardi il romanzo era terminato. Dopo aver infastidito ogni possibile editore famoso, riuscii a raggiungere un accordo con una piccola azienda. L'editore, un uomo anziano che indossava una logora giacca di tweed e occhiali talmente spessi che quasi toccavano i suoi folti baffi grigi, mi disse:"


translate italian epilogue_un_good_d813e73f:



    nvl clear
    "«Il suo livello è mediocre, giovanotto… mediocre, sì. Ma l'idea è interessante… interessante, sì. Bene, forse le servirebbe fare più pratica. E, ehm… leggere di più… sì…»"


translate italian epilogue_un_good_d13fddf7:


    "Mi ricorderò di quelle sue parole per il resto della mia vita. Distribuii agli amici e ai colleghi le copie che mi erano state inviate secondo il nostro contratto. Lena, che lesse il mio romanzo solo dopo che fu pubblicato, mi disse che le era veramente piaciuto. Per lo più per compiacermi, suppongo."


translate italian epilogue_un_good_78ec671a:


    "Il tempo passava… Si stava inesorabilmente avvicinando il giorno in cui ero stato portato via dal mio mondo precedente per essere gettato in questa vita, dal misterioso Campo di pionieri fino a dove mi trovo adesso. Ora sono uno scrittore di successo (almeno in base al numero di copie vendute), ho una moglie meravigliosa, e due figli. La mia vita si è trasformata del tutto."


translate italian epilogue_un_good_e0db8497:



    nvl clear
    "La mezzanotte di oggi è l'inizio di qualcosa di nuovo.{i}Domani{/i} è il giorno che aspetto da così tanti anni…"


translate italian epilogue_un_good_2cd0045b:


    un "A cosa stai pensando?"


translate italian epilogue_un_good_83704a72:


    "Ho aperto gli occhi e ho visto Lena."


translate italian epilogue_un_good_8e609a05:


    "Ha appoggiato una tazza di tè sul tavolo vicino alla mia poltrona."


translate italian epilogue_un_good_72199a1c:


    "La legna scoppiettava tranquillamente nel camino, e fuori infuriava una tempesta di neve."


translate italian epilogue_un_good_932e1068:


    "Mi sono avvolto in un plaid e ho guardato Lena."


translate italian epilogue_un_good_f07f7371:


    me "Sai che giorno è oggi?"


translate italian epilogue_un_good_6b22e1e1:


    un "No, che giorno è?"


translate italian epilogue_un_good_e152372b:


    me "Ti ricordi cosa ti dissi tanto tempo fa al Campo?"


translate italian epilogue_un_good_049b021f:


    un "No."


translate italian epilogue_un_good_13821bda:


    "Non mi sorprende. Durante tutti questi anni non ho mai più menzionato il mio misterioso arrivo in questo mondo."


translate italian epilogue_un_good_1814dbd5:


    me "Ti ricordi quando ti dissi che non ero di questo mondo?"


translate italian epilogue_un_good_d6dd39b3_1:


    "Lena ha pensato."


translate italian epilogue_un_good_9ebd484f:


    un "Beh, qualcosa del genere… Sì…"


translate italian epilogue_un_good_3ed0a2cc:


    me "Ecco, oggi è il giorno in cui, tanti anni fa, presi l'autobus 410 e mi risvegliai in quel Campo di pionieri."


translate italian epilogue_un_good_ffd733f0:


    un "Quindi, si tratta di un anniversario?"


translate italian epilogue_un_good_561828ec:


    "Ha sorriso Lena."


translate italian epilogue_un_good_ca6a2d01:


    me "Sì, una specie di anniversario…"


translate italian epilogue_un_good_58a9940e:


    un "È un anniversario triste per te?"


translate italian epilogue_un_good_0d7f2c30:


    me "No, per niente!"


translate italian epilogue_un_good_4013090d:


    un "E io non ricordo quasi nulla di quel periodo..."


translate italian epilogue_un_good_e68e962b:


    me "Forse è meglio così…"


translate italian epilogue_un_good_dd47136a:


    "Ho alimentato il fuoco con altra legna, e ho preso la tazza dal tavolo."


translate italian epilogue_un_good_429c57e7:


    me "Forse dovrei scrivere un romanzo.{w} Così potrai leggerlo e ricordare."


translate italian epilogue_un_good_b63153d6:


    un "Credi che sia una buona idea?"


translate italian epilogue_un_good_aed0e1bf:


    "L'espressione di Lena si è fatta seria."


translate italian epilogue_un_good_1b89c45a:


    "È vero, mi ero promesso che non avrei mai scritto di quella storia."


translate italian epilogue_un_good_2141272e:


    me "Forse non lo è.{w} Dopotutto, quello che è successo appartiene solo a noi."


translate italian epilogue_un_good_e5f761b0:


    un "È proprio quello che penso anch'io."


translate italian epilogue_un_good_82c3c74a:


    "Ho sospirato e ho guardato il fuoco."


translate italian epilogue_un_good_b5944be5:


    me "Sai, la vita è come una candela. Qualcuno ce l'ha corta, qualcuno ce l'ha lunga… E potrebbe spegnersi in qualsiasi momento."


translate italian epilogue_un_good_76bc0c24:


    un "Allora la nostra vita è come questo camino."


translate italian epilogue_un_good_d76f93fa:


    me "Sì, forse."


translate italian epilogue_un_good_5c7f770a:


    "L'ho guardata e ho sorriso."


translate italian epilogue_un_good_1064aa29:


    me "Dormirò ancora per un po'."


translate italian epilogue_un_good_9271b549:


    "Lena mi ha baciato e ha fissato il fuoco con sguardo sognante."


translate italian epilogue_un_good_18bde327:


    "I miei occhi si sono chiusi e tutta la mia realtà si è compressa nel crepitio della legna."


translate italian epilogue_un_good_b130e182:


    "Ho iniziato a cadere lentamente, in qualche luogo lontano."


translate italian epilogue_un_good_da5a6c7c:


    "No, non era un sogno. Era più come un caldo etere, che avvolgeva delicatamente l'intera mia esistenza…"


translate italian epilogue_un_good_ce617998_1:


    "…"


translate italian epilogue_us_f08a51bb:


    "Mi è sembrato di non aver dormito affatto."


translate italian epilogue_us_b10d729b:


    "Succede sempre. Basta chiudere gli occhi e addormentarsi, la lancetta delle ore compie molti giri, arriva la mattina e ci si sveglia, ma è come se fosse trascorso solo un battito di ciglia."


translate italian epilogue_us_f8bebe71:


    "Ho sbadigliato talmente tanto che mi sono quasi rotto la mascella, balzando in piedi a causa del dolore."


translate italian epilogue_us_ab939c0c:


    "C'era qualcosa che non andava…"


translate italian epilogue_us_2a6b0fba:


    "Anzi, non «qualcosa» – ogni cosa!"


translate italian epilogue_us_7c5d345f:


    "Ero di nuovo nel mio appartamento."


translate italian epilogue_us_d3ab1684:


    "Ma...{w} Ma come può essere?"


translate italian epilogue_us_b97b6ee6:


    "Sono stato preso dal panico e ho iniziato a correre intorno alla stanza, nella speranza di calmarmi un po'."


translate italian epilogue_us_32fd8672:


    "La stanchezza fisica spesso può sopraffare quella emotiva."


translate italian epilogue_us_2c5d0412:


    "La mia testa era vuota, la paura e il terrore mi hanno avvolto del tutto, e un qualche tipo di canzone risuonava nella mia mente – forse una preghiera, o forse semplicemente un miscuglio incoerente di pensieri stava cercando, se non di calmarmi, almeno di distrarmi dal mio panico."


translate italian epilogue_us_1645c72d:


    "È passata almeno mezz'ora prima che crollassi a terra, esausto, con lo sguardo fisso verso il soffitto."


translate italian epilogue_us_80b0da80:


    "Sembra che io non sia andato da nessuna parte. Il vecchio lampadario mi guardava con cattiveria, con le sue lampade polverose. Le crepe nell'intonaco erano esattamente le stesse di prima, e la carta da parati scollata non è scivolata giù di un solo millimetro."


translate italian epilogue_us_8d04f2aa:


    "Era davvero un sogno...?"


translate italian epilogue_us_69c49704:


    "Ma non può essere!{w} Non è possibile!"


translate italian epilogue_us_afbfd5dd:


    "Ho trascorso un'intera settimana in quel campo di pionieri."


translate italian epilogue_us_164fbde5:


    "Sono sicuro di esserci stato. Ricordo tutto perfettamente, dal mio risveglio a bordo del bus fino alla partenza finale…{w} Nessun sogno, nessuna allucinazione potrebbe essere così reale."


translate italian epilogue_us_ca2a0b33:


    "In qualche modo mi sono rialzato, sono andato in cucina, ho riempito un bicchiere di acqua e sono tornato in camera."


translate italian epilogue_us_d0a7feb4:


    "Il sangue mi stava ancora martellando nelle tempie, ma almeno il terrore dei primi minuti se n'era andato.{w} Oppure stava solo facendo una pausa."


translate italian epilogue_us_c3ffb486:


    "Mi sono concentrato sull'ultima cosa che ricordavo. Cioè la partenza dal Campo."


translate italian epilogue_us_7136c5b9:


    "La notte, il bus che sobbalzava sopra ai dossi, lo sporco e oscuro vetro oltre il quale quasi nulla era visibile, e i pionieri…"


translate italian epilogue_us_2ce6d3ca:


    "Ero assolutamente convinto che non sarei mai più ritornato qui.{w} O semplicemente non l'avevo preso in considerazione."


translate italian epilogue_us_8dcc331d:


    "In ogni caso, mi stavo già preparando per il nostro arrivo al centro del distretto entro poche ore, e stavo già pensando alla mia prossima mossa in quella nuova vita."


translate italian epilogue_us_d8532661:


    "Oppure no?"


translate italian epilogue_us_0babffab:


    me "Accidenti!"


translate italian epilogue_us_0e363452:


    "Ho ruggito e mi sono afferrato i capelli con tutte le mie forze."


translate italian epilogue_us_5058105d:


    me "Non riesco a ricordare!"


translate italian epilogue_us_60547951:


    "Le ultime ore in quel mondo si sono sciolte confusamente in un miraggio, come se un Renoir ubriaco stesse completando la sua opera con un rullo invece di un pennello."


translate italian epilogue_us_3302407b:


    me "Ma non è poi così male.{w} Aspetta un attimo, perché dovrebbe esserlo? Al contrario, è tutto a posto! Anzi, potrei anche dire perfetto!"


translate italian epilogue_us_d0f0488f:


    me "Sono riuscito ad andarmene da quel maledetto mondo e sono tornato a casa. La cosa più importante ora è non tornarci nuovamente.{w} Potrebbe succedere, certo che potrebbe!"


translate italian epilogue_us_ce617998:


    "…"


translate italian epilogue_us_f896d7c0:


    me "Ma potrebbe anche non succedere più!{w} In effetti, non ho nulla di cui preoccuparmi adesso, tutto andrà bene! Ovviamente mi sono immaginato tutto. Esatto, mi sono immaginato tutto!"


translate italian epilogue_us_4d149f2f:


    me "Non importa che tutto sembrava così reale, una cosa del genere proprio non poteva succedere. Per niente! Posso affermarlo con assoluta certezza. La scienza moderna afferma che è impossibile. Non è concesso!"


translate italian epilogue_us_f4546a9f:


    "Ho riso ad alta voce."


translate italian epilogue_us_33c6265f:


    "Una voce interiore ha cercato di fermare la diarrea verbale che stava schizzando fuori dalla mia bocca, ma senza successo."


translate italian epilogue_us_27fabe99:


    "Era come se i miei discorsi e i miei pensieri fossero delle entità separate e indipendenti."


translate italian epilogue_us_4a73860c:


    "Il mio cervello mi esortava a calmarmi e a cercare di analizzare la situazione, mentre la mia lingua cercava semplicemente di alleviare lo stress lanciando in aria parole senza senso."


translate italian epilogue_us_94206733:


    "Alla fine sono riuscito a riprendermi in qualche modo. Ho tirato le tende e ho guardato fuori dalla finestra."


translate italian epilogue_us_a5c5546b:


    "La città notturna sembrava esattamente la stessa della settimana scorsa."


translate italian epilogue_us_450bbc84:


    "Quella vista ha riportato la mia mente alla realtà, almeno in una certa misura."


translate italian epilogue_us_533137c4:


    "Dopotutto, se adesso tutto sembra normale e nulla di sospetto o di soprannaturale sta accadendo, allora non si è trattato forse di un semplice sogno?"


translate italian epilogue_us_94950be4:


    "In sostanza, ci sono solo due opzioni possibili."


translate italian epilogue_us_06d20129:


    "La prima – accettare il fatto che fosse stato tutto un sogno, e calmarmi.{w} La seconda – fidarmi dei miei sentimenti e accettare il fatto che il Campo, il bus e gli altri pionieri fossero reali."


translate italian epilogue_us_193a19e2:


    "Ad ogni modo, qualunque cosa io scelga, non otterrò alcuna risposta."


translate italian epilogue_us_adf47948:


    "È divertente, ero in cerca di quelle risposte da una settimana intera (o almeno facevo finta di cercarle) senza riuscire a trovare nulla, ma alla fine sono riuscito ad andarmene comunque. E adesso?"


translate italian epilogue_us_62a5f438:


    "L'enigma rimane, e altre domande sono affiorate…"


translate italian epilogue_us_2b19ebd4:


    "Alla fine ero esausto, e mi sono lasciato cadere sul letto. In pochi secondi ho iniziato a russare beatamente…"


translate italian epilogue_us_ce617998_1:


    "…"


translate italian epilogue_us_54d920bf:



    nvl clear
    "Molto tempo è passato da quando ero tornato dal campo «Sovyonok». E molto è cambiato nella mia vita."


translate italian epilogue_us_bd3a1548:


    "La prima settimana l'ho trascorsa a scervellarmi cercando di riassumere in dettaglio tutto quello che era successo, disegnando grafici e diagrammi, postando messaggi nei forum che parlano di fenomeni paranormali. Ho anche pensato di fissare un appuntamento con un sensitivo."


translate italian epilogue_us_9462ce27:


    "Alla fine non ho ottenuto proprio nulla, come ci si aspetterebbe. Non c'è da stupirsi. Ero un semplice essere umano, ma quegli eventi – erano chiaramente opera di una qualche intelligenza superiore."


translate italian epilogue_us_6101364c:


    "Se un cavernicolo si fosse ritrovato nei primi anni del ventunesimo secolo, persino lui avrebbe capito più di me."


translate italian epilogue_us_7ec6a1be:



    nvl clear
    "Beh, all'inizio penserebbe che un telefono cellulare che trasmette la voce delle persone a decine di chilometri di distanza sia un miracolo voluto dalla divinità."


translate italian epilogue_us_29d72188:


    "Ma è facile abituarsi."


translate italian epilogue_us_e0d7c1dc:


    "Forse se frequentasse l'istituto secondario e superiore sarebbe anche in grado di capire come funzionano i dispositivi di comunicazione senza fili."


translate italian epilogue_us_82144eab:


    "È solo un esempio, seppur esagerato. Surreale proprio come tutto ciò che ho vissuto."


translate italian epilogue_us_65950373:


    "E come lui, anch'io probabilmente non riuscirei a capire come funziona questa cosa, e chi ci sia dietro. E dubito che potrei abituarmi ad essa, se dovesse capitarmi nuovamente."


translate italian epilogue_us_9bdf285f:



    nvl clear
    "Ma una domanda mi ha turbato fin da allora – perché? Perché io?"


translate italian epilogue_us_7b47c1ee:


    "Che cosa ho (o non ho) fatto per meritarmi questa sfortuna (o fortuna)?"


translate italian epilogue_us_5535f094:


    "Certo, nelle storie di fantascienza queste cose capitano spesso a persone normali, ma quello è solo un caso."


translate italian epilogue_us_716452f4:


    "Ero assolutamente convinto che quegli eventi si fossero verificati per qualche ragione. Proprio come il tipico eroe delle storie di fantascienza che ha viaggiato un migliaio di anni avanti nel tempo, abbandonato nel luogo e nel tempo sbagliati."


translate italian epilogue_us_6a21d9ba:



    nvl clear
    "Ma nel futuro nessuno gli avrebbe creduto. Tutti avrebbero pensato che fosse solo un pazzo."


translate italian epilogue_us_9087af0c:


    "E nel mio caso? Mi aspettavano al Campo (o almeno, la leader del Campo mi aspettava). Come potrei spiegare questa cosa?"


translate italian epilogue_us_7d414027:


    "Forse sono stato scelto casualmente per essere studiato in seguito? Anche se così fosse, domandarsi il «perché» non solo è ragionevole, ma è ancora più importante di tutto il resto."


translate italian epilogue_us_ce617998_2:


    "…"


translate italian epilogue_us_4284aa16:



    nvl clear
    "Dopo non essere riuscito a trovare alcuna spiegazione soddisfacente, sono tornato alla mia vita normale."


translate italian epilogue_us_28ca60d9:


    "Ma ora non stavo sempre seduto al computer ventiquattr'ore al giorno, spolverando in continuazione il tasto F5 – nuovi interessi si sono fatti spazio nella mia vita (ma non saprei dire esattamente quali), e improvvisamente mi è venuta voglia di laurearmi."


translate italian epilogue_us_e0d94309:


    "Non per poter trovare un lavoro, e non perché fosse «necessario», ma solo perché mi sono ricordato di quanto divertente era stato il mio primo anno."


translate italian epilogue_us_ddf05860:


    "Chiacchierare con i compagni di corso, giorni di divertimento giovanile spensierato, un sacco di energia che avevo e che mi mancava così tanto negli ultimi anni."


translate italian epilogue_us_9b73f6c1:


    "Un uomo saggio una volta disse «Ogni uomo che legge troppo e usa troppo poco il suo cervello, alla fine cade in pigre abitudini di pensiero». È difficile non essere d'accordo."


translate italian epilogue_us_6a62e6c6:



    nvl clear
    "Un uomo ordinario non se ne sta seduto perché è pigro, ma perché una certa materia non è di alcun interesse per lui."


translate italian epilogue_us_b4927a3f:


    "Ad esempio, a me non interessa lavorare a maglia. Quindi mi vorresti dire che sono un fannullone, se non faccio un paio di maglioni di lana al mese?"


translate italian epilogue_us_67291156:


    "Ma l'essere troppo pigri per aprire un libro – è una cosa che posso capire. Anche se mi piace leggere, se penso che dovrò far scorrere un intero romanzo, pagina per pagina…"


translate italian epilogue_us_68cd3f08:


    "E anche se fosse interessante, anche se potesse catturare completamente la mia attenzione in modo tale da non riuscire a distogliere lo sguardo da esso, la parte interessante arriverebbe solo in seguito. Tutto quello che basterebbe ora è andare a prendere un libro da uno scaffale e aprirlo. Ma sono troppo pigro per fare ciò…"


translate italian epilogue_us_83bbcd46:


    "Non so esattamente cosa mi avesse influenzato, ma quasi la metà del mio tempo (invece del cinque-dieci percento di prima) l'ho impiegato per fare qualcosa di utile – leggere, scrivere, studiare qualcosa di nuovo, fare sport (o almeno, fare esercizi la mattina)."


translate italian epilogue_us_01e63747:



    nvl clear
    "A volte penso che io sia stato influenzato dal Campo."


translate italian epilogue_us_6b3df02c:


    "Non sarebbe da escludere del tutto – durante tutti i sette giorni trascorsi lì, partecipavo alle attività sociali, comunicavo verbalmente con gli altri, in un modo che prima avrebbe causato in me terrore e una profonda introversione."


translate italian epilogue_us_c9700033:


    "D'altra parte, non è così facile cambiare la personalità di qualcuno in una sola settimana."


translate italian epilogue_us_9deeccfb:


    "Specialmente se si tratta di una persona testarda come me."


translate italian epilogue_us_ae21b9f8:



    nvl clear
    "Ma è facile indirizzare quella persona verso una nuova direzione, mostrargli una linea guida."


translate italian epilogue_us_cfcb2a60:


    "Tuttavia non avrei mai creduto che sarebbe stato possibile con me."


translate italian epilogue_us_7d323ccc:


    "In ogni caso, tali cambiamenti mi hanno giovato talmente tanto che ho cercato di non pensare a ciò che li aveva scatenati."


translate italian epilogue_us_ae7018a0:


    "Che diavolo, nessuno si chiederebbe il «perché», nel caso in cui riuscisse a fare jackpot puntando il suo ultimo paio di pantaloni."


translate italian epilogue_us_ce617998_3:


    "…"


translate italian epilogue_us_b60d6130:



    nvl clear
    "In estate mi sono iscritto nuovamente all'università, e ho iniziato gli studi in autunno."


translate italian epilogue_us_f8eaaeec:


    "Il tempo scorreva. Partecipavo con entusiasmo alle lezioni e ai seminari, e mi preparavo per le prove e per gli esami con un livello di entusiasmo che non mi sarei mai aspettato da me stesso."


translate italian epilogue_us_792db99f:


    "Sono riuscito ad entrare a far parte del gruppo con sorprendente facilità. Benché io fossi più vecchio della maggior parte degli studenti, questo non era un problema."


translate italian epilogue_us_cae314c1:


    "Forse è a causa della mia naturale immaturità, o dei cambiamenti nella mia personalità, non ne sono sicuro. Molto probabilmente la risposta si trova da qualche parte nel mezzo."


translate italian epilogue_us_6135d2ed:



    nvl clear
    "La gioia nel comunicare con le persone, che avevo perso molti anni fa, è tornata."


translate italian epilogue_us_aafbc9f6:


    "Era facile andare d'accordo con gli altri, i loro problemi non mi sembravano così banali e distanti. La vita normale, che prima consideravo solo come una massa grigia deprimente, ha iniziato a brillare di nuovi colori."


translate italian epilogue_us_71461a28:


    "A volte mi sembrava di essermi trasformato in una bambola, di essere diventato un soldatino di piombo tra altri milioni identici a me, messi tutti in riga su uno scaffale di un negozio di giocattoli."


translate italian epilogue_us_e2123de9:


    "Ma in questo negozio, oltre alla raggiante vetrina che brillava con le scritte fantasiose che descrivevano le offerte della nuova stagione e gli sconti di Natale, c'era un magazzino, dove venivano gettati tutti i prodotti difettosi: un orsacchiotto senza una zampa, un camion dei vigili del fuoco che aveva bisogno di un meccanico, un Transformer che sembrava più un forno a microonde invece che un potente robot, dei puzzle assemblati in bizzarre immagini post-moderne…"


translate italian epilogue_us_bfbc90e1:



    nvl clear
    "Il mio posto era tra quei giocattoli rotti."


translate italian epilogue_us_b863937a:


    "E mentre alcuni di essi avrebbero potuto trovare rifugio in una comunità di beneficenza o in un orfanotrofio, il mio unico destino era la discarica, e l'essere riciclato."


translate italian epilogue_us_1be16bf7:


    "E quindi non riuscivo a godere di un tale cambiamento nella mia vita!"


translate italian epilogue_us_ce617998_4:


    "…"


translate italian epilogue_us_be1991b7:


    "Era l'ultima lezione del corso più difficile di questa sessione."


translate italian epilogue_us_ead34c7c:


    "Alla maggior parte degli studenti non piaceva."


translate italian epilogue_us_18d7148a:


    "A causa della sua complessità e dell'insegnante difficile da capire, suppongo."


translate italian epilogue_us_31f9553d:


    "Ma io trovavo una sorta di piacere nel trattare con tabelle, grafici e diagrammi."


translate italian epilogue_us_65f71ceb:


    "Separando i vari elementi, li disponevo nel giusto ordine in righe e colonne, sommando, sottraendo, dividendo e moltiplicando, e utilizzandoli per ottenere un quadro completo di qualsiasi fenomeno."


translate italian epilogue_us_9f752038:


    "Nessun numero poteva sfuggire al mio sguardo attento. Sarebbero stati tutti catturati, calcolati, analizzati. A ognuno di essi sarebbe stato assegnato un indice e un posto nella casella corretta."


translate italian epilogue_us_f15dbc2c:


    "Dopo essere arrivata, a ogni cifra sarebbe stato assegnato un letto a castello, una divisa da lavoro e degli abiti normali, e sarebbe stata mandata a svolgere il suo compito."


translate italian epilogue_us_620a5afb:


    "Alcuni sarebbero stati mandati in trincea dal 9 al 5, altri avrebbero marciato per affrontare le medie, e altri ancora avrebbero cercato di colpire una regressione lineare sulla linea di fuoco nemica…"


translate italian epilogue_us_bb1b122e:


    odn "A cosa stai pensando?"


translate italian epilogue_us_ec822f80:


    "A malincuore, ho smesso di scrivere e ho guardato il mio compagno di corso."


translate italian epilogue_us_51f07f25:


    me "Sto prendendo appunti, come puoi vedere."


translate italian epilogue_us_6f6e3c30:


    odn "Oh, ma a che serve!{w} Basterà leggere il libro degli esercizi!"


translate italian epilogue_us_a2c7fb06:


    me "Leggerò anche quello."


translate italian epilogue_us_d93f9cb9:


    odn "Mi sorprendi sempre."


translate italian epilogue_us_baaee44b:


    me "Che c'è di così sorprendente?"


translate italian epilogue_us_41e93016:


    odn "Hai intenzione di ottenere un diploma di prima classe, non è vero?"


translate italian epilogue_us_21b742e8:


    me "Non ci ho mai pensato…"


translate italian epilogue_us_096e8101:


    odn "Non mi dire che trovi interessanti queste cose!"


translate italian epilogue_us_ef73bda8:


    "Mi sono ricordato che solo un momento fa avevo immaginato me stesso blasonato su uno striscione innalzato da un reggimento di cifre, e non ho potuto fare a meno di sorridere."


translate italian epilogue_us_56f957c4:


    odn "Nella vita non ti serviranno mai tutte queste cose!"


translate italian epilogue_us_93d43576:


    me "Tutto servirà.{w} Almeno come cultura generale."


translate italian epilogue_us_a5b0bb2a:


    "Il compagno di corso ha sorriso con sarcasmo."


translate italian epilogue_us_38f2115a:


    me "Scommetto che non hai mai letto anche un solo libro in tutta la tua vita."


translate italian epilogue_us_d6011e93:


    odn "E allora?"


translate italian epilogue_us_f13d64cb:


    "Ha chiesto con aria di sfida."


translate italian epilogue_us_fcfd9bb3:


    me "Sto solo affermando un dato di fatto."


translate italian epilogue_us_c7273c00:


    odn "E tu sei sempre con la testa fra le nuvole."


translate italian epilogue_us_69ed5808:


    "Aveva ragione su questo. Anche se avevo riacquistato parte delle mie competenze sociali perdute, spesso perdevo il contatto con la realtà, sognando ad occhi aperti."


translate italian epilogue_us_33265fad:


    me "Lo dici come se fosse una cosa sbagliata!"


translate italian epilogue_us_39ec8d69:


    odn "E non c'è niente di sbagliato nemmeno nel non leggere libri!"


translate italian epilogue_us_6ba8578f:


    me "L'essere umano non può accontentarsi solo di cose materiali."


translate italian epilogue_us_ef353f7b:


    "Ho detto filosoficamente, prendendolo in giro."


translate italian epilogue_us_da783828:


    odn "Oh, sei così deprimente, yawn!"


translate italian epilogue_us_ce617998_5:


    "…"


translate italian epilogue_us_5c98c047:


    "La lezione stava giungendo al termine, e ho iniziato a pianificare il resto della giornata."


translate italian epilogue_us_26180779:


    "Devo acquistare qualcosa da mangiare, e poi completare un progetto e inviarlo al cliente. E devo scrivere le mie annotazioni…"


translate italian epilogue_us_44324923:


    "E poi la sera potrò leggere o guardare qualcosa."


translate italian epilogue_us_75613726:


    "A meno che qualcuno mi telefoni o mi scriva. Non ho cose urgenti da fare, quindi posso dedicare un po' di tempo ai miei compagni."


translate italian epilogue_us_21b43907:


    odn "Oh, mancano solo altri cinque minuti…"


translate italian epilogue_us_5620292c:


    "Ho guardato il mio telefono e mi sono reso conto che è passato esattamente un anno dopo il mio ritorno dal campo «Sovyonok»."


translate italian epilogue_us_c2222e39:


    "La mia anima è stata avvolta dal calore nel ripensare a quei tempi, e ho sorriso beatamente."


translate italian epilogue_us_f6b9bef5:


    "Quegli eventi non mi tornano in mente così spesso ormai."


translate italian epilogue_us_4510141f:


    "Ovviamente non si può semplicemente dimenticare una cosa del genere, quei momenti bizzarri e straordinari che resteranno incisi nella mia memoria per sempre."


translate italian epilogue_us_da6f1a84:


    "Normalmente col tempo anche i momenti più felici sbiadiscono, e rimane solo un ricordo di essi, niente di più. Ma con la settimana trascorsa al «Sovyonok» era diverso."


translate italian epilogue_us_c7712fd2:


    "Ricordavo ogni singolo dettaglio: il terrore dei primi minuti dopo il risveglio nel bus. Il primo giorno – difficile, pieno di sorprese e incontri meravigliosi. Scherzi allegri e spensierati con Ulyana, basti pensare a quello scherzo dei fantasmi, per esempio."


translate italian epilogue_us_3b043f92:


    "Avrebbe fatto invidia ai migliori comici sovietici!"


translate italian epilogue_us_1133bb6a:


    "E quei pionieri avevano certe espressioni!{w} Stavano morendo dal ridere!"


translate italian epilogue_us_88f6794d:


    "Sarebbe proprio bello incontrare Ulyana nella vita reale."


translate italian epilogue_us_5fdf8a75:


    "Beh, anche lei ha i suoi difetti, è iperattiva, e non ha idea di cosa siano le buone maniere, ma…"


translate italian epilogue_us_ad5fa5b0:


    "Non si incontra così spesso una persona talmente sincera, ingenuamente allegra ed energica come lei."


translate italian epilogue_us_957cd999:


    "Forse mi ha regalato un po' della sua energia…"


translate italian epilogue_us_732f5a0a:


    "E il fatto di averla abbandonata mi rendeva ancora più triste."


translate italian epilogue_us_66623e13:


    "Naturalmente si è trattato solo di un sogno, un frutto della mia immaginazione."


translate italian epilogue_us_59330073:


    "Ma si possono provare emozioni anche nei confronti dei personaggi dei libri e dei film.{w} Ridere e piangere assieme a loro, condividere il loro dolore."


translate italian epilogue_us_41ee4507:


    "La stessa cosa vale per me – ho la sensazione che se avessi agito diversamente, questa storia avrebbe avuto un lieto fine ancora più gioioso."


translate italian epilogue_us_e8ee1985:


    "Tuttavia, sono sicuro che Ulyana è felice nel suo mondo."


translate italian epilogue_us_10bf53a2:


    "Non riesco proprio a immaginarmela sconvolta per più di cinque minuti. Il suo ottimismo la aiuterà a superare ogni cosa, anche le situazioni più difficili."


translate italian epilogue_us_5432fef5:


    "Mi chiedo come sarà da grande…"


translate italian epilogue_us_ce617998_6:


    "…"


translate italian epilogue_us_78903e24:


    "La campanella ha suonato. Il mio compagno di classe si è alzato in piedi e mi ha guardato intensamente."


translate italian epilogue_us_320bd80d:


    odn "Ok, ci vediamo… Posso copiare i tuoi appunti più tardi?"


translate italian epilogue_us_22448110:


    me "Se tu li avessi scritti durante la lezione non ti servirebbe copiarli da me."


translate italian epilogue_us_0c8f943d:


    odn "Allora, posso?"


translate italian epilogue_us_9f5895ba:


    me "Ok, va bene."


translate italian epilogue_us_31844f25:


    odn "Saluti!"


translate italian epilogue_us_3e06fe65:


    "Ha sorriso e ha abbandonato la sala."


translate italian epilogue_us_0912f764:


    "Ho raccolto lentamente tutti i miei quaderni e i libri di testo, li ho messi nello zaino e mi sono guardato attorno nella sala ormai deserta."


translate italian epilogue_us_c11e20a5:


    "Per un attimo ho quasi potuto sentire l'odore della conoscenza diffusa nell'aria, come il ponte di una nave imbevuto di sale marino."


translate italian epilogue_us_d5ee5f01:


    "Migliaia e migliaia di studenti venivano qui per studiare, per imparare qualcosa di nuovo o per dimenticare qualcosa che sapevano."


translate italian epilogue_us_4f2c8f9f:


    "Alcuni dormivano durante le lezioni, alcuni scrivevano prestando attenzione, proprio come me, ma nessuno era del tutto indifferente."


translate italian epilogue_us_36afd483:


    "Quelli a cui non importava se ne stavano a casa, tirando le tende e tenendo lo sguardo fisso sullo schermo del computer."


translate italian epilogue_us_a3f5dd2b:


    "Ho sospirato con riluttanza e ho abbandonato l'aula con la ferma intenzione di non sprecare questa giornata."


translate italian epilogue_us_303f415b:


    "La campanella ha suonato. Il mio compagno di classe si è alzato, mi ha salutato e se n'è andato."


translate italian epilogue_us_7de17a9f:


    "Ho iniziato mettere nello zaino i miei quaderni e i libri di testo, quando improvvisamente ho sentito la voce di qualcuno davanti a me."


translate italian epilogue_us_ec6b06a5:


    usg "Scusa, è questa l'aula numero trentaquattro?"


translate italian epilogue_us_446ad0da:


    me "C'è un cartello sulla porta."


translate italian epilogue_us_e87f24be:


    usg "No, non c'è!"


translate italian epilogue_us_a7b9d37d:


    "La voce di quella ragazza suonava offesa."


translate italian epilogue_us_974f3678:


    me "Qualcuno deve averlo rimosso."


translate italian epilogue_us_faab6aa2:


    usg "Allora, è l'aula trentaquattro?"


translate italian epilogue_us_fc30fd2b:


    me "Sì."


translate italian epilogue_us_18a6d4fc:


    "Alla fine ho chiuso lo zaino e ho alzato la testa, con l'intenzione di alzarmi in piedi."


translate italian epilogue_us_053ad9b8:


    "Quella ragazza era terribilmente familiare."


translate italian epilogue_us_04dca07f:


    me "Per caso ci siamo già…?"


translate italian epilogue_us_41fdb5dd:


    usg "Sì, ho pensato la stessa cosa."


translate italian epilogue_us_f2f12dc1:


    "Sembrava davvero sorpresa."


translate italian epilogue_us_94093acc:


    me "Ma non ricordo dove potrei averti incontrata."


translate italian epilogue_us_6c0a1771:


    usg "Nemmeno io…"


translate italian epilogue_us_8cc9c861:


    me "Di che anno sei?"


translate italian epilogue_us_7aad28e6:


    usg "Primo anno."


translate italian epilogue_us_b8aea2a0:


    me "Oh, sei ancora agli inizi..."


translate italian epilogue_us_fb117d69:


    "La ragazza ha fatto un sorriso."


translate italian epilogue_us_080a02d2:


    usg "Tutto è ancora davanti a me!"


translate italian epilogue_us_c49f13e1:


    me "Come se io avessi già superato tutto…"


translate italian epilogue_us_48cc1cb9:


    usg "E tu invece?"


translate italian epilogue_us_dd578b4d:


    me "Quarto anno."


translate italian epilogue_us_cfefc74e:


    usg "Allora, è difficile?"


translate italian epilogue_us_8f48e6be:


    me "Il primo anno sì."


translate italian epilogue_us_05e57566:


    usg "Lo sapevo!"


translate italian epilogue_us_1e8379d1:


    "Ha detto in tono sconvolto."


translate italian epilogue_us_04f0c0cd:


    me "Ma poi diventa più facile…{w} È sempre più facile dopo essersi abituati."


translate italian epilogue_us_638e6427:


    usg "A volte sono troppo pigra per fare le cose…"


translate italian epilogue_us_3685e98e:


    me "Già, succede anche quello."


translate italian epilogue_us_da4ba352:


    usg "Per caso hai ancora qualche materiale del primo anno?{w} Se mi aiutassi, forse sarebbe più facile per me passare gli esami."


translate italian epilogue_us_123037fe:


    "Ovviamente non avevo niente, il mio primo anno era finito molto tempo fa."


translate italian epilogue_us_934f55fc:


    "Ma improvvisamente ho sentito un forte desiderio di chiederle il suo numero di telefono, così le ho mentito."


translate italian epilogue_us_14b5f47a:


    me "Sì, forse…{w} Ma devo controllare."


translate italian epilogue_us_463c9a51:


    usg "Grandioso!"


translate italian epilogue_us_5274a2e2:


    "Ci siamo scambiati i numeri di telefono."


translate italian epilogue_us_96f1754d:


    me "Il mio nome è Semyon.{w} E tu come ti chiami?"


translate italian epilogue_us_f46d0d3c:


    usg "Ulyana."


translate italian epilogue_us_fcd06166:


    "E solo allora ho capito chi mi ricordava quella ragazza!"


translate italian epilogue_us_8a86e6b2:


    "Esatto, è proprio Ulyana, ma di cinque anni più matura!"


translate italian epilogue_us_919cb1ad:


    "Per un attimo sono rimasto senza parole."


translate italian epilogue_us_9f328817:


    us "Che c'è? Si tratta di un nome comune!{w} Era anche il nome di Lenin!"


translate italian epilogue_us_89c17322:


    "Mi ha detto, mettendo il broncio."


translate italian epilogue_us_f5b985ac:


    me "No… È solo che…{w} Sei mai stata in un campo di pionieri?"


translate italian epilogue_us_4af695ee:


    us "Sì, quando ero ragazzina…{w} Perché me lo chiedi?"


translate italian epilogue_us_cf8568cf:


    "In un batter d'occhio, tutti gli eventi accaduti al campo «Sovyonok» mi sono balenati davanti agli occhi."


translate italian epilogue_us_63854d93:


    "Allora non era un sogno!"


translate italian epilogue_us_cba25f57:


    us "Anche se quelli erano dei campi estivi, non «di pionieri»…"


translate italian epilogue_us_903a86f3:


    "Ha aggiunto Ulyana."


translate italian epilogue_us_9445daad:


    me "Quindi non indossavi un'uniforme?"


translate italian epilogue_us_c3110504:


    us "Quella con un fazzoletto rosso?{w} Certo che no! Che mostruosità!"


translate italian epilogue_us_56d4e39c:


    "Ha riso forte."


translate italian epilogue_us_3649081a:


    us "Ma perché me lo chiedi?"


translate italian epilogue_us_8f44567c:


    "Stavo per raccontarle tutto, ma poi mi sono reso conto che mi avrebbe considerato uno psicopatico."


translate italian epilogue_us_6b2b31a6:


    "Dopotutto, questa ragazza aveva molti più anni dell'Ulyana che ricordavo."


translate italian epilogue_us_ae985c6d:


    "Forse semplicemente le assomigliava…"


translate italian epilogue_us_3cd4f58f:


    me "Ho fatto un sogno…{w} E penso di averti vista lì.{w} Una sorta di déjà vu."


translate italian epilogue_us_d1a9fc56:


    us "Deja… cosa?"


translate italian epilogue_us_ec20dd52:


    me "È quando si vede qualcosa per la prima volta, ma si ritiene di averla già vista prima."


translate italian epilogue_us_abc2ab7a:


    us "Beh, ora che ci penso…"


translate italian epilogue_us_ce43a5b5:


    "La sua espressione si è fatta seria..."


translate italian epilogue_us_78382c80:


    us "Credo di averti visto da qualche parte anch'io.{w} Sicuramente!{w} Forse è quella cosa che hai appena detto…"


translate italian epilogue_us_e8c05531:


    me "Una coincidenza interessante, non trovi?"


translate italian epilogue_us_2170a31a:


    us "Non lo so…{w} Comunque! Ti chiamerò al telefono! Ci sentiamo!"


translate italian epilogue_us_5de584b6:


    "Ha riso ed è corsa fuori dall'aula."


translate italian epilogue_us_0dcbddab:


    "L'aspetto, il comportamento, il modo di parlare... Quella è decisamente la vera Ulyana!"


translate italian epilogue_us_894a40f8:


    "Cosa significa allora, che il sogno sta diventando realtà?"


translate italian epilogue_us_23b134c2:


    "Allora non posso che esserne felice!"


translate italian epilogue_us_9e9cd5d5:


    "E soprattutto, rivedrò ancora quella gioiosa ragazza, questo è sicuro!"


translate italian epilogue_dv_9bc20c27:


    "Sembra che io stia invecchiando…"


translate italian epilogue_dv_2be57040:


    "La giornata era finita e la sbornia avrebbe dovuto essere già sparita, ma mi sentivo ancora male e avevo le vertigini."


translate italian epilogue_dv_6be0ab43:


    "Nonostante tutti i miei sforzi non ero in grado di aprire gli occhi. Sono solo riuscito a girarmi."


translate italian epilogue_dv_99485000:


    "Mi chiedo cosa mi aspetti nel centro del distretto?{w} Come me la caverò?{w} Forse non sarà poi così male?"


translate italian epilogue_dv_df8947d0:


    "Beh certo, non saprei proprio da dove cominciare, ma d'altra parte questo mondo non sembra ostile. Conosco un sacco di cose, tutti parlano russo…"


translate italian epilogue_dv_cdc9d191:


    "Allora forse non mi accadrà niente di brutto...?"


translate italian epilogue_dv_bf9b8fdd:


    "Improvvisamente mi sono sentito molto male."


translate italian epilogue_dv_910fe671:


    "Dannazione, credevo che gli effetti della notte scorsa fossero ormai passati."


translate italian epilogue_dv_098084b9:


    "Sono riuscito a raggiungere il bagno, muovendomi istintivamente."


translate italian epilogue_dv_80b1d831:


    "Dopo un po' di tempo, quando il mio sporco lavoro era giunto al termine, sono tornato in camera."


translate italian epilogue_dv_7d131181:


    "E poi mi sono reso conto…"


translate italian epilogue_dv_7b407504:


    "Sono tornato nel mio vecchio appartamento!"


translate italian epilogue_dv_04cada73:


    "Tuttavia, non è così vecchio..."


translate italian epilogue_dv_bdfdf404:


    "Sembra che nulla sia cambiato durante la mia assenza."


translate italian epilogue_dv_e99eda04:


    "Gli strati di polvere non sono aumentati per niente."


translate italian epilogue_dv_49074c47:


    "La posizione della tastiera, l'angolazione dello schermo e la macchia di tè lasciata dalla tazza sul tavolo hanno infilzato i miei occhi."


translate italian epilogue_dv_47eb912e:


    "In teoria, una persona nelle mie condizioni non dovrebbe riuscire a notare certi dettagli!"


translate italian epilogue_dv_55584c51:


    "Ad ogni modo, l'intera stanza era come se lampeggiasse di colori diversi, attirando la mia attenzione sui minimi dettagli."


translate italian epilogue_dv_46f98924:


    "Le calze giacevano per terra esattamente come le avevo lasciate. La coperta spiegazzata era ancora sul letto disfatto così com'era prima, la finestra sporca mostrava sempre la stessa vista della strada, come un cinescopio incrinato."


translate italian epilogue_dv_76d36664:


    "Ho cercato invano di essere terrorizzato, o quantomento di provare paura."


translate italian epilogue_dv_185107f9:


    "Forse è l'effetto dello shock, un disturbo da stress post-traumatico o qualcosa del genere?"


translate italian epilogue_dv_dc9713bf:


    "Dopotutto, alcune ore fa non avrei mai pensato di tornare al mondo reale."


translate italian epilogue_dv_ce3c1db8:


    "Il campo di pionieri, le ragazze, Alisa, il misterioso centro del distretto dove sarei dovuto arrivare se non mi fossi svegliato nel mio appartamento. Tutte quelle cose stavano cominciando a diventare reali per me."


translate italian epilogue_dv_3b4fb89e:


    "Forse la mia fantastica vacanza al campo «Sovyonok» è stata solo un sogno?"


translate italian epilogue_dv_bb1b5b1b:


    "I miei piedi hanno cominciato a disegnare cerchi intorno alla stanza.{w} Tenevo le mani dietro la schiena e ho iniziato a pensare a tutto quello che era successo."


translate italian epilogue_dv_05941cbe:


    "Solo una cosa è certa: sono tornato così improvvisamente com'ero arrivato {i}là{/i}."


translate italian epilogue_dv_b0e545e2:


    "Senza alcuna spiegazione logica."


translate italian epilogue_dv_c2f030da:


    "Forse c'era un qualche tipo di spiegazione, ma era ben oltre la mia (o la umana) comprensione."


translate italian epilogue_dv_54e489ca:


    "Che si trattasse di un trucco di forze interstellari, o di una mente suprema, o di esperimenti governativi (quest'ultima opzione mi è sembrata la meno probabile), nessuna di esse ha lasciato un qualche indizio."


translate italian epilogue_dv_94c0ae0c:


    "Se non sono riuscito a trovare delle risposte nel Campo, sarebbe sciocco aspettarsi di trovarle {i}qui{/i}."


translate italian epilogue_dv_f55be16f:


    "Sarebbe come cercare le prove in una casa che è stata bruciata fino alla cenere."


translate italian epilogue_dv_077e08fa:


    "Anche se un crimine fosse stato commesso, tutte le prove sarebbero state distrutte dall'incendio."


translate italian epilogue_dv_be44e243:


    "Forse se avessi le capacità e le attrezzature di un criminologo potrei riuscire a trovare qualcosa, ma…"


translate italian epilogue_dv_dea456b1:


    "I ricordi della settimana trascorsa nel Campo sono tutto ciò che mi resta."


translate italian epilogue_dv_9362d886:


    "Ma non si è trattato di un sogno. Ne sono sicuro!"


translate italian epilogue_dv_5bfae656:


    "Il mio mal di testa era assolutamente vero. I miei sentimenti e le mie emozioni erano genuini, non erano un semplice frutto dei sogni."


translate italian epilogue_dv_a78cb617:


    "Quindi io ci sono stato, ho trascorso un'intera settimana al campo «Sovyonok», con la leader e con i pionieri…{w} e con Alisa."


translate italian epilogue_dv_a2226b0b:


    "Il ricordo di lei mi ha fatto sospirare."


translate italian epilogue_dv_42975286:


    "Sarebbe stato bello partire con lei…"


translate italian epilogue_dv_78c33026:


    "Aspetta un attimo!{w} Cosa c'è di sbagliato in tutto questo?!{w} Dovrei essere contento di essere tornato a casa, nel mondo reale!"


translate italian epilogue_dv_db34fc11:


    "O forse no...?"


translate italian epilogue_dv_0b94b031:


    "Ora come ora non riesco ancora a valutare ragionevolmente la situazione."


translate italian epilogue_dv_2f3f1dc7:


    "Non riuscivo a capire se desiderassi rimanere là, o se godermi il mio ritorno che avevo atteso così a lungo."


translate italian epilogue_dv_d091ab38:


    "Ho notato una moneta da cinque rubli sul tavolo. L'ho presa e ho cominciato a lanciarla in aria, senza alcuna ragione, contando il numero di teste e croci."


translate italian epilogue_dv_ba6c1813:


    "Molto tempo fa, forse anche a scuola, ho sentito dire che pur lanciando una moneta un numero infinito di volte, le probabilità di testa e croce non sono esattamente del cinquanta percento ciascuna."


translate italian epilogue_dv_b79ab5f5:


    "Forse perché una moneta non ha una forma e un peso ideali, o per qualche altra ragione."


translate italian epilogue_dv_0906061c:


    "Comunque sia, dopo un po' di tempo ho contato 70 teste e 71 croci."


translate italian epilogue_dv_a3037b78:


    "Quindi la teoria della relatività mente…{w} Ma non posso giudicarla, non ne so molto…"


translate italian epilogue_dv_6766bb1d:


    "Però in alcune lingue si dice «testa» e «coda»."


translate italian epilogue_dv_b5103a20:


    "Perché «coda»? Per qualche ragione mi fa pensare agli scoiattoli."


translate italian epilogue_dv_0dc3131c:


    "Forse c'è una certa logica in ciò – uno scoiattolo in effetti ha una coda."


translate italian epilogue_dv_31eac3c5:


    "Questa teoria mi sembrava strana, ma allo stesso tempo non riuscivo a trovare alcuna dimostrazione che potesse confutarla."


translate italian epilogue_dv_ba47df6c:


    "Questo significa che gli scoiattoli appaiono più frequentemente delle teste?"


translate italian epilogue_dv_736afd2f:


    "Ma bisogna anche considerare che gli scoiattoli hanno una testa, quindi…"


translate italian epilogue_dv_80525988:


    "Mi sono afferrato per i capelli e ho sospirato, esausto."


translate italian epilogue_dv_73fcc8eb:


    "Non è il momento di pensare a certe stupidaggini!"


translate italian epilogue_dv_51221043:


    "Ma d'altra parte, è il momento giusto per fare cosa?{w} Che posso fare? Cosa è sotto il mio controllo?"


translate italian epilogue_dv_4c4bbb47:


    "Se non mi sono preso la briga di trovare le risposte durante quella settimana, che senso ha pensarci adesso?"


translate italian epilogue_dv_bc27a2ca:


    "Forse dovrei solo cercare di tornare alla mia vita normale…?"


translate italian epilogue_dv_6ce763b8:


    "Il termine «vita normale» era doloroso."


translate italian epilogue_dv_3ceed523:


    "Perché ho deciso di usare proprio quelle parole?{w} Avrei potuto dire «passata», «vecchia» o qualcos'altro!"


translate italian epilogue_dv_4e6dae94:


    "Probabilmente perché è quello a cui ero abituato – ecco la risposta più ovvia."


translate italian epilogue_dv_1e1bd7d8:


    "Come si suol dire, quello a cui ci si abitua diventa una seconda natura per sé stessi…"


translate italian epilogue_dv_87ae0d50:


    "Immediatamente tutti gli eventi della scorsa settimana hanno galleggiato davanti ai miei occhi."


translate italian epilogue_dv_1614d0af:


    "Avevo davvero così tanta voglia di restare lì?"


translate italian epilogue_dv_b3b1bf62:


    "Mi è bastata davvero solo una settimana per abituarmi a Olga Dmitrievna che se la prendeva con me in ogni istante, agli scherzi e alle battute di Ulyana, o alla sfacciataggine e alle prese in giro di Alisa?"


translate italian epilogue_dv_612f35d1:


    "Non ho una risposta a quella domanda, ma mi pare ovvio che io ne senta la mancanza."


translate italian epilogue_dv_07307489:


    "Sono sicuro che non tornerò mai più in quel mondo."


translate italian epilogue_dv_da4fdd9d:


    "Anche se non si può dire con certezza che la forza che mi ha portato lì non possa decidere di intrattenersi ancora una volta.{w} E poi ancora, e ancora…"


translate italian epilogue_dv_06277978:


    "È possibile."


translate italian epilogue_dv_eccd0d95:


    "Ma per ora ho quello che ho – un'avventura che mi ricorderò per il resto dei miei giorni."


translate italian epilogue_dv_b8699e2f:


    "Dannazione, quante persone possono sperimentare una cosa simile?{w} Persino dei libri, persino dei sogni del genere sono del tutto impensabili!"


translate italian epilogue_dv_e7a9c5b6:


    "La cosa più importante è che non si trattava dell'isola maledetta del dottor Moreau, ma di un normale campo sovietico con dei normali pionieri."


translate italian epilogue_dv_41392bfa:


    "E quella loro normalità è stata ricoperta dalla mia misteriosa comparsa in quel luogo."


translate italian epilogue_dv_4f464055:


    "In ogni caso, non è stata la peggiore esperienza della mia vita.{w} Anzi, forse è stata una delle migliori!"


translate italian epilogue_dv_1a6feebe:


    "Certo, è triste che tutto sia stato lasciato a metà, ma ogni storia ha la sua fine."


translate italian epilogue_dv_aa9a36c1:


    "È un bene che si sia conclusa in questo modo.{w} Chissà cos'altro sarebbe potuto accadere…"


translate italian epilogue_dv_ce617998:


    "…"


translate italian epilogue_dv_4bbd270b:



    nvl clear
    "Il tempo scorreva. Gli eventi che avevano avuto luogo nel Campo adesso mi sembravano come un affascinante romanzo che avevo letto tempo fa."


translate italian epilogue_dv_5faded9b:


    "No, non me li sono scordati. Potrei descriverli nei minimi dettagli."


translate italian epilogue_dv_2a6f7dd3:


    "Ricordo come da bambino ero solito nascondermi sotto ad una coperta con una torcia elettrica e leggere fino all'alba un libro affascinante di avventure spaziali."


translate italian epilogue_dv_ba6313e4:


    "Certe volte non riuscivo nemmeno ad alzarmi per andare a scuola. I miei genitori erano soliti sgridarmi per questo, ogni notte sfogliavo attentamente ogni singola pagina, cercando di assorbire ogni singolo dettaglio di quella storia."


translate italian epilogue_dv_b9465d76:



    nvl clear
    "Ho provato la stessa cosa nei confronti del Campo. La realtà (se la si potrebbe chiamare così) ora sembrava più simile ad un libro."


translate italian epilogue_dv_84be21a5:


    "Non credo che sia stata una cattiva esperienza. Alla fine, la cosa più importante sono i ricordi, e non importa se questi assumono la forma di una sequenza di immagini, foto, video o audio."


translate italian epilogue_dv_a20cefa7:


    "..."


translate italian epilogue_dv_0775f137:



    nvl clear
    "Ora ogni autobus mi dava un senso di paura e di speranza."


translate italian epilogue_dv_41f919cf:


    "All'inizio continuavo a prendere il bus 410 senza uno scopo preciso. Più volte mi sono addormentato sullo stesso sedile."


translate italian epilogue_dv_c066fcc3:


    "Ma non è successo più niente. Nemmeno una traccia di un qualcosa di soprannaturale."


translate italian epilogue_dv_a3314542:


    "Dopo un po' di tempo ho smesso di fare quei viaggi senza meta."


translate italian epilogue_dv_ce617998_1:


    "…"


translate italian epilogue_dv_32ab7558:



    nvl clear
    "Qualcosa è cambiato nella mia vita, semplicemente non poteva essere altrimenti!"


translate italian epilogue_dv_d3186f16:


    "Un paio di settimane dopo il mio ritorno dal Campo ho preso la mia chitarra che giaceva abbandonata in un angolo, ricoperta di polvere."


translate italian epilogue_dv_77eccfd0:


    "Ho provato a suonare un paio di accordi, e ciò mi ha ricordato Alisa – la sua immagine era inseparabile da quello strumento."


translate italian epilogue_dv_56c6309a:


    "All'inizio non è andata molto bene – forse non avevo talento per la musica. Poi ho iniziato a leggere libri e a guardare lezioni video."


translate italian epilogue_dv_f6f2a4ed:


    "D'altra parte, tutti i miei sforzi non erano più un fardello per me. Mi sono limitato a suonare e ad affinare le mie abilità."


translate italian epilogue_dv_30a27c7e:


    "E come accade con qualsiasi tipo di attività, tra cui la musica, la pratica rende perfetti."


translate italian epilogue_dv_56ead4c3:


    "Circa sei mesi più tardi mi sono reso conto che avevo raggiunto un certo livello. Almeno ora potevo agevolmente riprodurre parti delle mie canzoni preferite."


translate italian epilogue_dv_07a11c4d:



    nvl clear
    "Soddisfatto dei miei progressi, ho messo un paio di annunci sui forum di musica, con l'intenzione di formare la mia band."


translate italian epilogue_dv_47a62878:


    "Non avevo ancora idea di che tipo di musica volessi suonare; i miei gusti andavano dalla musica pesante alle colonne sonore degli anime."


translate italian epilogue_dv_ede9ef2c:


    "All'inizio mi è sembrato impossibile."


translate italian epilogue_dv_c41adf0a:


    "Avrei dovuto comunicare con degli sconosciuti! E se saremmo stati troppo diversi? E comunque, come avrei potuto spiegare agli altri il tipo di musica che volevo fare?"


translate italian epilogue_dv_2510a2ef:


    "Ma tutto si è risolto in modo molto più semplice."


translate italian epilogue_dv_41b1dc0a:



    nvl clear
    "I potenziali candidati si sono rivelati essere non così spaventosi, e i nostri gusti erano più o meno gli stessi."


translate italian epilogue_dv_367522a6:


    "Tutto sommato, condividevamo gli stessi obiettivi, così i problemi dovuti all'incomprensione potevano essere risolti."


translate italian epilogue_dv_03fe9757:


    "E ben presto abbiamo formato una band."


translate italian epilogue_dv_7c51a82e:


    "Improvvisamente ho scoperto il mio talento come compositore."


translate italian epilogue_dv_6e2dd77c:


    "Dopo aver trascorso intere giornate e notti con l'editor di note e col sintetizzatore, ho iniziato a comporre delle cose carine. O almeno così mi dicevano gli altri membri della band, e le persone a cui facevo ascoltare i miei pezzi."


translate italian epilogue_dv_2e540bca:



    nvl clear
    "Ben presto abbiamo dato il nostro primo concerto."


translate italian epilogue_dv_1ab31e80:


    "In un locale irrilevante che non era molto più spazioso della mia stanza, con i tre quarti del nostro pubblico che non era esattamente sobrio, ero immensamente ansioso e l'impianto audio aveva costanti problemi. Ma tutto questo non mi infastidiva, perché ero felice."


translate italian epilogue_dv_dc28a178:


    "Dal momento che era il nostro primo passo verso il successo!"


translate italian epilogue_dv_b6db561e:


    "Tuttavia, c'è voluto un sacco di tempo prima che fossimo invitati nuovamente."


translate italian epilogue_dv_ac45d183:


    "Con grande difficoltà siamo riusciti a registrare tre brani e li abbiamo pubblicati su internet. Le recensioni erano tutt'altro che entusiaste, ma la cosa non mi ha turbato più di tanto. Era un inizio."


translate italian epilogue_dv_b178ebc9:



    nvl clear
    "Forse alcuni consideravano la nostra musica come un semplice passatempo o una breve ossessione, ma per me era una cosa seria."


translate italian epilogue_dv_74103c40:


    "Dopotutto, ero stato io a fondare la band, e tutta la musica l'avevo scritta io."


translate italian epilogue_dv_78c27567:


    "Mi sentivo responsabile per il destino della mia creazione. Ho impiegato ogni minuto del mio tempo libero ad affinare le mie abilità, e dopo un anno potevo dire con certezza di non essere il chitarrista peggiore del mondo."


translate italian epilogue_dv_1a44fbb5:



    nvl clear
    "E poi è arrivata una certa popolarità."


translate italian epilogue_dv_425eb152:


    "Facevamo concerti quasi ogni settimana, e finalmente venivamo pagati. Non importava che fossero solo cinquecento rubli a persona, la cosa che contava era che qualcuno provava interesse verso di noi, e che era disposto a pagarci (anche se di solito eravamo solo una warm-up per altre band)."


translate italian epilogue_dv_ce617998_2:


    "…"


translate italian epilogue_dv_6e06d90f:


    "Stavo seduto davanti al mio monitor, tormentando una chitarra."


translate italian epilogue_dv_5d68b7b8:


    "La data di un concerto imminente era cerchiata in modo evidente sul calendario del desktop."


translate italian epilogue_dv_b8653b2e:


    "Il solo pensiero mi faceva venire la pelle d'oca."


translate italian epilogue_dv_42802d30:


    "Per la prima volta saremmo stati i protagonisti del palco.{w} E avremmo potuto presentare il nostro album."


translate italian epilogue_dv_89bb111d:


    "Chissà quanti dischi venderemo?{w} Naturalmente l'epoca dei CD è ormai finita, ma ci sono ancora alcuni che li apprezzano…"


translate italian epilogue_dv_78b39126:


    "E magari alcune persone li compreranno solo per supportare la band."


translate italian epilogue_dv_67aef320:


    "Ho guardato la data ancora una volta.{w} Sì, è passato esattamente un anno da quando sono tornato dal campo «Sovyonok»."


translate italian epilogue_dv_299e685a:


    "Il mio sorriso era riflesso sullo schermo oscurato."


translate italian epilogue_dv_ed82539c:


    "Mi chiedo se ad Alisa piacerebbe la mia musica, se lei esistesse in questo mondo.{w} Non credo proprio!"


translate italian epilogue_dv_1c7acd8a:


    "Lei preferiva i classici del rock sovietico.{w} Ma chi lo sa…"


translate italian epilogue_dv_1e250ea6:


    "Ho suonato un paio di pezzi complicati e poi ho concluso soddisfatto, perché il suono della mia chitarra superava di gran lunga quello delle chitarre «Ural» sovietiche, e la mia abilità superava quella dei chitarristi di quell'epoca."


translate italian epilogue_dv_663b0360:


    "Mi chiedo se avrei mai potuto raggiungere questi risultati senza l'aiuto di quegli straordinari eventi.{w} Improbabile…"


translate italian epilogue_dv_4e2974c6:


    "Nulla di strano. Un sacco di gente cambia radicalmente dopo essersi avvicinata troppo all'orlo del precipizio."


translate italian epilogue_dv_82a1b7c9:


    "Certo, nulla di pericoloso successe allora, ma uno shock psicologico è sempre meglio di un grave pericolo."


translate italian epilogue_dv_3c4da4a0:


    "Ho ripassato la mia parte un'ultima volta, ho riposto la chitarra nella custodia, ho preparato la borsa e mi sono diretto verso l'uscita."


translate italian epilogue_dv_199c3750:


    "Fuori faceva freddo, proprio come {i}quel{/i} giorno…"


translate italian epilogue_dv_e801f24b:


    "Mi sono avvicinato lentamente alla fermata dell'autobus 410."


translate italian epilogue_dv_55024429:


    "Che coincidenza interessante – sto andando al club prendendo proprio quell'autobus."


translate italian epilogue_dv_11dda01e:


    "Ho contato i secondi impazientemente."


translate italian epilogue_dv_a0a306c4:


    "Ben presto il bus è arrivato. Ha aperto pigramente le sue porte e ha inghiottito qualche passeggero."


translate italian epilogue_dv_fbe14004:


    "Era una notte d'inverno, proprio come {i}quella volta{/i}.{w} Le automobili sfrecciavano, infiammando la città con le loro luci, mentre io mi stavo preparando a dormire."


translate italian epilogue_dv_688e4dad:


    "Più ci avvicinavamo alla zona del centro, più gente c'era per strada."


translate italian epilogue_dv_ca09470b:


    "Alcuni di loro stavano chiacchierando allegramente, altri stavano osservando magnifiche vetrine di boutique, e altri ancora si stavano affrettando da qualche parte, senza prestare attenzione ad alcunché."


translate italian epilogue_dv_25cb0d1d:


    "Oggi non avevo alcuna voglia di tornare in quel Campo.{w} In qualsiasi altro momento forse sì, ma non oggi!"


translate italian epilogue_dv_535c0d02:


    "Devo suonare al mio concerto, il resto se ne può andare al diavolo."


translate italian epilogue_dv_9b124b7d:


    "Avevo un chiaro obbiettivo, e nient'altro aveva importanza!"


translate italian epilogue_dv_ce617998_3:


    "…"


translate italian epilogue_dv_3ddfa4cf:


    "Il club era molto più grande di quello nel quale avevamo suonato il nostro primo concerto, ma al momento era ancora vuoto.{w} Stavano facendo le prove generali."


translate italian epilogue_dv_633c3b22:


    "Non riuscivo a sentire i tamburi attraverso gli altoparlanti del monitor, e ho cercato disperatamente di spiegarlo al tecnico del suono."


translate italian epilogue_dv_d28a96c6:


    "I suoni sono a posto, i gadget sono installati, la scaletta è stata completata. Pronti a spaccare?"


translate italian epilogue_dv_ab38f79d:


    "Ma era un giorno speciale. Non stavamo facendo le prove. Gli altri ragazzi si stavano riscaldando prima del nostro concerto.{w} E quindi saremmo saliti sul palco per ultimi."


translate italian epilogue_dv_2dc1b4a6:


    "Mentre ascoltavo la prima band, ho notato che avevano ancora un sacco di lacune."


translate italian epilogue_dv_e867ef2d:


    "D'altra parte, non eravamo molto diversi da loro qualche tempo fa."


translate italian epilogue_dv_3561335d:


    "Più si avvicinava il nostro momento, e più mi sentivo teso."


translate italian epilogue_dv_b07e6f10:


    "Accidenti, stavo letteralmente tremando per l'ansia!"


translate italian epilogue_dv_4daf3bc3:


    "Ho deciso di non ascoltare l'altra band e di aspettare con calma nel camerino."


translate italian epilogue_dv_d6ae003b:


    "A giudicare dalle loro espressioni, i miei compagni erano nella mia stessa situazione."


translate italian epilogue_dv_00e13f2c:


    "Stavamo in silenzio.{w} In realtà non avevamo nulla di cui parlare."


translate italian epilogue_dv_8986e3df:


    "Le nostre parti erano già state apprese e perfezionate tempo fa."


translate italian epilogue_dv_a7cff533:


    "Solo il nervosismo ci avrebbe potuto ostacolare, ma avevamo già una certa esperienza, e così sarebbe andato tutto bene.{w} O almeno, io la pensavo in quel modo."


translate italian epilogue_dv_ce617998_4:


    "…"


translate italian epilogue_dv_da0729f4:


    "E ora, finalmente, è giunto il momento!"


translate italian epilogue_dv_40b909bb:


    "Non ricordo in che modo siamo saliti sul palco e come abbiamo salutato il pubblico."


translate italian epilogue_dv_a65dc811:


    "La mia coscienza si è accesa quando i primi accordi della nostra prima canzone sono risuonati dagli altoparlanti."


translate italian epilogue_dv_58bfcc73:


    "Mentre ero seduto nella sala trucco cercavo disperatamente di riscaldare le mie dita gelate, ma ora esse saltavano da una corda all'altra senza commettere il minimo errore, come se fossero incantate."


translate italian epilogue_dv_15afa28b:


    "Un pensiero mi è balenato nella mente – come sono riuscito a non commettere ancora un singolo errore?"


translate italian epilogue_dv_76c871e1:


    "Quando suonavo tutte queste parti a casa o durante le prove non ci pensavo per niente, tutto era spontaneo."


translate italian epilogue_dv_b60a105d:


    "Ma adesso, trovandomi di fronte ad un vasto pubblico, era diverso."


translate italian epilogue_dv_a8ba8d2a:


    "Da qualche parte nel profondo della mia mente c'era la preoccupazione che a breve le mie dita sarebbero scivolate e…{w} Grazie al cielo tale pensiero non ha interferito con il mio suonare!"


translate italian epilogue_dv_9819f44a:


    "La nostra performance è quasi giunta al termine, e si stava avvicinando il momento del difficile assolo che avevo perfezionato per così tanto tempo."


translate italian epilogue_dv_e621a17f:


    "A quel punto il mio nervosismo è svanito, e ho suonato ogni singola nota in modo impeccabile, addirittura meglio di come avevo fatto alle prove."


translate italian epilogue_dv_ed12c375:


    "Alla fine la nostra prestazione si è conclusa, e la sala da concerto è esplosa in una marea di applausi."


translate italian epilogue_dv_552e80e1:


    "Ho sorriso goffamente (o almeno così mi è sembrato) e mi sono inchinato più volte con il resto della band."


translate italian epilogue_dv_4ff24831:


    "Il pubblico ci gridava «vogliamo il bis!». Anche senza il loro incitamento mi sarebbe piaciuto suonare ancora qualcos'altro, ma avevamo esaurito le canzoni, dato che eravamo un gruppo agli esordi."


translate italian epilogue_dv_ce617998_5:


    "…"


translate italian epilogue_dv_485788f0:


    "L'euforia ha prevalso nel camerino, mentre ci facevamo i complimenti e ci abbracciavamo. L'eco dell'apertura delle bottiglie di champagne ha risuonato nei corridoi."


translate italian epilogue_dv_6ab4e9e3:


    "Siamo riusciti a vendere tutti i nostri dischi, il che mi ha reso ancora più felice."


translate italian epilogue_dv_1e7ce85a:


    "Finalmente anch'io sono riuscito a diventare qualcuno!"


translate italian epilogue_dv_a744e489:


    "È impossibile prevedere il futuro della nostra band, o di tutto questo affare della musica."


translate italian epilogue_dv_7c912a9c:


    "Ma una cosa è certa – ho finalmente trovato qualcosa che mi interessa e a cui posso dedicare tutta la mia vita."


translate italian epilogue_dv_701ac863:


    "E inoltre posso anche rendere felici le persone che mi ascoltano."


translate italian epilogue_dv_32dffc6c:


    "Probabilmente in quel momento ero l'uomo più felice del mondo!{w} O almeno quello era di certo il momento più bello della mia vita."


translate italian epilogue_dv_ce617998_6:


    "…"


translate italian epilogue_dv_abdb027b:


    "Il club si stava svuotando, gli strumenti sono stati messi nelle custodie, e stavo aspettando gli altri membri della band per tornare a casa."


translate italian epilogue_dv_290d0099:


    "Ho osservato il palco su cui mi trovavo appena un'ora fa."


translate italian epilogue_dv_46ed0349:


    "Ancora una volta sono stato avvolto da gioia, piacere ed emozione."


translate italian epilogue_dv_b9afffc3:


    "Questa non sarà l'ultima volta che apparirò su un palco, ne sono certo."


translate italian epilogue_dv_d3215206:


    "E forse mi aspettano cose ancora più grandi, come ad esempio le grandi sale da concerto e gli stadi olimpici. Chi lo sa?{w} Tutto è possibile adesso!"


translate italian epilogue_dv_1e17703e:


    "Dopo aver salutato i miei compagni mi sono diretto verso la fermata del bus, e sono riuscito miracolosamente a prendere in tempo l'autobus 410."


translate italian epilogue_dv_748039c5:


    "Dentro non c'era nessuno. Ho preso posto dietro l'autista, proprio come allora. Una melodia familiare stava suonando alla radio."


translate italian epilogue_dv_5ef8eb1d:


    "Forse un giorno anche le mie canzoni verranno trasmesse."


translate italian epilogue_dv_6570bb77:


    "In qualche modo, questi viaggi sono simili agli eventi che ebbero luogo un anno fa."


translate italian epilogue_dv_76a32943:


    "Voglio dire, in questo momento tutto è come previsto, ho raggiunto i miei obbiettivi dopo un duro lavoro, e il risultato mi soddisfa."


translate italian epilogue_dv_9a35ddf5:


    "Ma sto tornando come un uomo diverso.{w} Un uomo cambiato, in un certo senso…"


translate italian epilogue_dv_674125ad:


    dvg "Ehi!"


translate italian epilogue_dv_1b10feb5:


    "Mi sono voltato e ho visto una ragazza lì vicino."


translate italian epilogue_dv_cf74f4a4:


    "Il suo tono non sembrava troppo educato, ma non era nemmeno ostile."


translate italian epilogue_dv_c2b1de8c:


    dvg "Mi è piaciuta la vostra performance!"


translate italian epilogue_dv_73aab3cd:


    me "Grazie…"


translate italian epilogue_dv_6b60c623:


    "Il buio all'interno del club mi impediva di vedere in dettaglio il suo viso, ma mi sembrava estremamente familiare."


translate italian epilogue_dv_bd0cee6c:


    me "E tu…"


translate italian epilogue_dv_1d56b639:


    "Ho fatto un passo in avanti."


translate italian epilogue_dv_531bb64d:


    "Esattamente!{w} Di fronte a me c'era proprio Alisa!"


translate italian epilogue_dv_e3137c54:


    "Sono rimasto immobile, incapace di dire una singola parola."


translate italian epilogue_dv_6a3ab760:


    dvg "Io cosa?"


translate italian epilogue_dv_5b8f2192:


    me "Niente…{w} Mi sembra di averti già vista da qualche parte."


translate italian epilogue_dv_ba143499:


    "Ho deciso di non fare domande dirette riguardo al campo di pionieri."


translate italian epilogue_dv_6fa8d283:


    "Dopotutto, può darsi che quella ragazza assomigli ad Alisa.{w} O qualcosa del genere…"


translate italian epilogue_dv_6827cc68:


    "In ogni caso, mi pareva ovvio che lei non mi avesse mai incontrato prima.{w} O almeno faceva finta che fosse così…"


translate italian epilogue_dv_0f19819d:


    dvg "Beh, probabilmente…"


translate italian epilogue_dv_ae3e36fe:


    "Mi ha guardato."


translate italian epilogue_dv_5a477656:


    me "Mi chiamo Semyon."


translate italian epilogue_dv_0c70048e:


    "Forzando un finto sorriso, le ho porto la mia mano."


translate italian epilogue_dv_9cd81f75:


    dvg "Lo so."


translate italian epilogue_dv_c0d3e816:


    me "E come?"


translate italian epilogue_dv_5c2b6b63:


    dvg "L'ho letto su internet."


translate italian epilogue_dv_021e0faf:


    "Ah, certo…"


translate italian epilogue_dv_339fce09:


    me "E tu sei...?"


translate italian epilogue_dv_be5fc071:


    dvg "Alisa…"


translate italian epilogue_dv_1ebb64bd:


    "Ha risposto un po' imbarazzata."


translate italian epilogue_dv_04cf27b7:


    "Allora avevo ragione!{w} Ma quanto avevo ragione...?"


translate italian epilogue_dv_2ec84ac1:


    me "Senti, sei mai stata in un campo di pionieri da giovane?"


translate italian epilogue_dv_ed23c5e1:


    dv "N… no…{w} Perché me lo chiedi?"


translate italian epilogue_dv_e1ab806a:


    me "È solo che…{w} Mi sembra di averti incontrata lì."


translate italian epilogue_dv_98abf1e8:


    dv "E come avresti potuto incontrarmi lì, se non ci sono mai stata?"


translate italian epilogue_dv_ec2c2a7c:


    "Ha riso."


translate italian epilogue_dv_85d7f47f:


    dv "Tutti voi musicisti siete piuttosto strani!"


translate italian epilogue_dv_7bd57e71:


    me "Non ne dubito."


translate italian epilogue_dv_98885eac:


    dv "Beh… Volevo solo dirti che la vostra musica non è male."


translate italian epilogue_dv_0c7cecbc:


    "Alisa si è voltata e ha fatto per andarsene."


translate italian epilogue_dv_dadc6a74:


    "Sentivo di dover fare qualcosa, di dover dire qualcosa."


translate italian epilogue_dv_48603599:


    me "Verrai al nostro prossimo concerto?"


translate italian epilogue_dv_8e1cb694:


    dv "Quando?"


translate italian epilogue_dv_37503a95:


    "A dire il vero non ne avevo idea."


translate italian epilogue_dv_e56b5b6f:


    me "Presto…"


translate italian epilogue_dv_26dbb3bd:


    dv "Sì, forse…{w} Mandami un messaggio!"


translate italian epilogue_dv_3b03af3a:


    me "Sì, certo…"


translate italian epilogue_dv_1cf7fbad:


    "Ci siamo scambiati i contatti."


translate italian epilogue_dv_b256ad93:


    dv "Allora ci vediamo, probabilmente!"


translate italian epilogue_dv_f57169e1:


    "Ha agitato la mano ed è uscita."


translate italian epilogue_dv_1e45c266:


    "Ovviamente non potevo dire con certezza se si trattasse della Alisa che avevo conosciuto nel Campo."


translate italian epilogue_dv_9835dc5e:


    "Innumerevoli teorie sono state annullate da un'assoluta assenza di fatti."


translate italian epilogue_dv_c0c56290:


    "Ma in quel momento era l'ultima cosa che mi interessava, dato che ero inondato di emozioni, sia per il concerto che per l'incontro con Alisa."


translate italian epilogue_dv_b44b240b:


    me "Dopotutto, niente mi impedisce di chiamarla, e poi si vedrà…"


translate italian epilogue_dv_7bdf75da:


    "Ho sorriso, mi sono messo in spalla la chitarra e mi sono incamminato con i miei compagni verso la fermata del bus 410."


translate italian epilogue_sl_d4b0f999:


    "Ci sono sogni dai quali non ci si vorrebbe mai risvegliare."


translate italian epilogue_sl_63d02893:


    "È come galleggiare su un caldo fiume, verso un luogo lontano oltre l'orizzonte, guardando beatamente il mondo insensato, nascosto dietro alle nuvole."


translate italian epilogue_sl_f9b4fc62:


    "Il passato è lasciato alle spalle, i suoi echi non ti strappano l'anima.{w} E il futuro è proprio qui, basta allungare la mano."


translate italian epilogue_sl_410a9fd5:


    "Non importa ciò che ti attende lì, il processo stesso di immersione in questo mondo fatato di serenità e di felicità è di per sé molto più importante."


translate italian epilogue_sl_aa0f75b6:


    "Ho sempre creduto che l'universo si trovasse in uno stato simile, prima di esplodere, e che un giorno vi ritornerà."


translate italian epilogue_sl_38f385ca:


    "La nostra vita è solo un batter d'occhio, solo un secondo in confronto all'immensità dell'universo, la nascita e la morte delle stelle è solo un minuto, e quelle delle galassie sono semplici ore."


translate italian epilogue_sl_9c9a7284:


    "Ma nonostante ciò, pur mettendo assieme tutte quelle cose, non si riuscirebbe a generare neppure un singolo giorno."


translate italian epilogue_sl_bd631aa1:


    "Nessuno pensa a come vivere ogni secondo della propria vita, allora importa davvero sapere cosa faccia una semplice goccia di un fiume in piena, che scorre con impeto oltre l'orizzonte esistenziale?"


translate italian epilogue_sl_1f69fba6:


    "Ho aperto gli occhi e mi sono stirato beatamente."


translate italian epilogue_sl_1210b0d8:


    "Non avevo voglia di alzarmi, dato che mi sentivo immerso in una perfetta beatitudine, avvolto in una coperta."


translate italian epilogue_sl_590f6c27:


    "Dopotutto, non devo andare da nessuna parte.{w} Come al solito, a dire il vero…"


translate italian epilogue_sl_be7b0f0b:


    "E non ho alcun piano per oggi."


translate italian epilogue_sl_003b132f:


    "Allora perché non dovrei limitarmi a dormire?"


translate italian epilogue_sl_bd93e9e9:


    "Mi sono voltato e ho fissato la vecchia carta da parati che ricopriva la parete storta."


translate italian epilogue_sl_0a1ee678:


    "Chissà quando è stata l'ultima volta che ho rinnovato questa stanza?{w} E perché hanno la carta da parati qui, invece del legno?"


translate italian epilogue_sl_433cc26c:


    "Un vago sospetto ha iniziato a risuonare da qualche parte nel profondo del mio animo."


translate italian epilogue_sl_771e1b84:


    "Ho scaraventato la coperta in aria, sono saltato in piedi e mi sono guardato intorno."


translate italian epilogue_sl_9b6e0d96:


    "Sì, ero di nuovo nel mio appartamento."


translate italian epilogue_sl_2f2ad9f9:


    "All'inizio sono rimasto paralizzato."


translate italian epilogue_sl_18a8a5cb:


    "Lo shock è stato così forte che il mio cervello non era in grado di elaborare ciò che stava succedendo."


translate italian epilogue_sl_ea18891a:


    "Me ne stavo lì in piedi a fissare lo schermo del computer sul tavolo di fronte a me."


translate italian epilogue_sl_047fe1ee:


    "Non riuscivo a pensare. Mi sono persino dimenticato di dover respirare."


translate italian epilogue_sl_3c6ecad1:


    "Alla fine la mia mente ha cominciato a schiarirsi."


translate italian epilogue_sl_fb89f263:


    "Ma cosa è successo?{w} Mi sono addormentato sul bus, andando verso il centro del distretto?{w} Sì, qualcosa del genere..."


translate italian epilogue_sl_aa71a533:


    "E Slavya era seduta al mio fianco.{w} E adesso invece mi ritrovo qui…"


translate italian epilogue_sl_9762c4e5:


    "Sembra che durante la settimana che ho trascorso lì nel Campo, io mi fossi talmente abituato all'idea che non sarei mai più tornato alla mia vita precedente, che adesso non so proprio come reagire a tutto questo."


translate italian epilogue_sl_a44e9759:


    "Dopotutto, all'inizio non desideravo altro che tornare a casa…"


translate italian epilogue_sl_b4b3948b:


    "Ma alla fine, tutto è stato come un brutto film. Alla fine sono tornato, ma sono ancora sotto shock."


translate italian epilogue_sl_af8879d8:


    "No, non si tratta di paura.{w} Piuttosto, sono curioso… e sconvolto."


translate italian epilogue_sl_29f47261:


    "Dopotutto mi ero già preparato ad iniziare una nuova vita con Slavya, lasciandomi tutti i problemi alle spalle – le imprecazioni, la sofferenza, le introspezioni, le questioni irrisolte e i progetti per il futuro."


translate italian epilogue_sl_57794fd0:


    "Ovviamente quella vita non avrebbe potuto essere peggiore di questa, almeno."


translate italian epilogue_sl_5c585492:


    "Ma ora non c'è modo di tornare là…"


translate italian epilogue_sl_e0b5fc71:


    "D'altra parte, se una settimana fa qualcuno mi avesse detto che sarei stato mandato in un mondo diverso proprio così, in un batter d'occhio, non ci avrei mai creduto."


translate italian epilogue_sl_21b9702d:


    "E allora cosa potrebbe impedire ad un evento così straordinario di ripetersi nuovamente?"


translate italian epilogue_sl_d390fb44:


    "Mi sono trascinato con fatica verso la cucina, ho preso un bicchiere d'acqua e sono tornato nella stanza."


translate italian epilogue_sl_b6e3af01:


    "Quel liquido ghiacciato con un disgustoso retrogusto di cloro mi ha rianimato in qualche modo."


translate italian epilogue_sl_e5e5f203:


    "Adesso devo decidere quale sarà la mia prossima mossa."


translate italian epilogue_sl_41ef88b1:


    "All'improvviso mi sono reso conto di non riuscire a sopportare il silenzio."


translate italian epilogue_sl_80b426bb:


    "Dopo aver acceso il computer ho fatto partire una canzone a caso nel lettore, e sono riuscito a calmarmi un po'."


translate italian epilogue_sl_a0bf71cd:


    "Seriamente parlando – che ci posso fare?{w} Recentemente mi sono reso conto che nulla dipende da me."


translate italian epilogue_sl_a90c22ac:


    "Per volere di qualcuno sono stato tirato fuori dal mio mondo abituale, e poi sono stato rimandato indietro."


translate italian epilogue_sl_35575ac5:


    "Non ho trovato alcuna risposta durante la settimana nel Campo."


translate italian epilogue_sl_c9372110:


    "E che senso ha pensarci adesso…?"


translate italian epilogue_sl_d4e2d25c:


    "Devo dimenticare tutto quello che è successo, come se fosse stato un brutto sogno!"


translate italian epilogue_sl_32fbcaae:


    "Forse si è trattato di semplici allucinazioni, ma ormai non m'interessa."


translate italian epilogue_sl_9e130725:


    "L'unica cosa che mi impediva di dimenticare del tutto quella breve esperienza era Slavya…"


translate italian epilogue_sl_6d445c91:


    "Mi è venuto in mente il suo sorriso, e tutto il tempo che abbiamo trascorso insieme."


translate italian epilogue_sl_4ef0184b:


    "Dall'incontro del primo giorno fino alla notte nella foresta, e la nostra partenza dal Campo."


translate italian epilogue_sl_51bc49e7:


    "Il cuore mi batteva furiosamente nel petto, diffondendo un opprimente dolore in tutto il corpo."


translate italian epilogue_sl_36b7144d:


    "Mi è piaciuta fin dal primo momento.{w} Era così dolce, premurosa, comprensiva…"


translate italian epilogue_sl_98add8a2:


    "Proprio come quella eroina degli anime – qual era il suo nome? Una ragazza che aveva aiutato un tipo come me ad affrontare la depressione."


translate italian epilogue_sl_264c0834:


    "E a pensarci bene, non esistono persone del genere nella vita reale!"


translate italian epilogue_sl_5aa379da:


    "Slavya non chiedeva nulla in cambio, non aveva bisogno di incoraggiamento, non si aspettava di essere capita o apprezzata per quello che faceva."


translate italian epilogue_sl_0a40af6e:


    "Era semplicemente sé stessa – una ragazza che non può esistere nella vita reale…"


translate italian epilogue_sl_c52a2b6d:


    "A pensarci bene, lei era esattamente così."


translate italian epilogue_sl_bcc4565b:


    "È come se io avessi visto un unico sogno lungo una settimana."


translate italian epilogue_sl_057f727b:


    "Un sogno riguardante un campo di pionieri, gli adolescenti sovietici e la loro leader, un sogno riguardante calde notti d'estate e raduni attorno a un falò, spensierati giochi per bambini e semplici gioie umane, un istante che è durato una settimana, e l'eterna estate…"


translate italian epilogue_sl_dccb98b2:


    "Ma non solo «visto».{w} Io ci sono stato, io ne ho fatto parte!"


translate italian epilogue_sl_d77755f6:


    "I miei occhi si sono involontariamente riempiti di lacrime.{w} Non lacrime di dolore o di disperazione – ma lacrime di rammarico e di malinconia."


translate italian epilogue_sl_d1d01df5:


    "Anche se è tutto finito, ho sperimentato qualcosa che la maggior parte delle persone non riuscirebbe nemmeno a immaginarsi."


translate italian epilogue_sl_dc042d8c:


    "L'immagine di Slavya è balenata nella mia mente, chiara come il sole."


translate italian epilogue_sl_75314796:


    "Quanto avrei voluto andarmene dal Campo insieme a lei…"


translate italian epilogue_sl_b649e1a2:


    "Forse adesso non avrei nulla da offrirle, ma ho ancora tutta la vita davanti."


translate italian epilogue_sl_4d7ca51b:


    "Credo che quegli eventi mi abbiano insegnato un sacco di cose."


translate italian epilogue_sl_0b2cd74c:


    "Non ho potuto fare a meno di pensare che tutto sembrasse troppo perfetto, come se fosse stato scritto basandosi su una semplice trama."


translate italian epilogue_sl_ad88d4c0:


    "Un eroe perdente, un avvenimento fantastico, e una meravigliosa trasformazione…"


translate italian epilogue_sl_eab34488:


    "Ma credo sia una cosa impossibile.{w} O almeno, non in questa vita…"


translate italian epilogue_sl_a1f94223:


    "Forse domani o tra una settimana tornerò ad essere la stessa persona di prima?"


translate italian epilogue_sl_e3775951:


    "Beh, è l'esito più probabile."


translate italian epilogue_sl_84316324:


    "Solo una cosa è certa – non sarò mai più in grado di tornare in quel luogo…"


translate italian epilogue_sl_ce617998:


    "…"


translate italian epilogue_sl_dfcf3261:



    nvl clear
    "Circa un mese è passato da quando sono tornato dal campo «Sovyonok». O meglio, dal giorno in cui mi ero risvegliato."


translate italian epilogue_sl_65020c5a:


    "Sono tornato alla mia usuale vita da solitario, navigando in internet per giorni, uscendo solo per fare acquisti. Come previsto, ho cominciato ad abituarmi alla cosa."


translate italian epilogue_sl_5b8761f3:


    "Beh, ovviamente c'erano anche momenti di malinconia e di depressione, una persona come me proprio non può farne a meno, ma non duravano più di tanto. Tranne quelle volte in cui, durante il sonno, riuscivo a tornare all'estate, in quel Campo misterioso…"


translate italian epilogue_sl_c49c4047:


    "Ma al risveglio cercavo di scacciare quei pensieri il prima possibile. Dopotutto, che senso ha sognare di diventare un mago?"


translate italian epilogue_sl_7bbf60a6:



    nvl clear
    "Se anche i miracoli potessero accadere (e io ero la prova vivente che era così!), allora accadrebbero in modo del tutto indipendente dalla nostra volontà. La parola «dolce» non ti consente di sentire un sapore dolce, indipendentemente da quante volte la pronunci…"


translate italian epilogue_sl_9a22d402:


    "Ma tuttavia qualcosa è cambiato nella mia esistenza. Prima non pensavo mai al mio futuro, in realtà non mi importava quanto a lungo avrei vissuto, se ancora per una settimana o per quarant'anni, ma adesso riesco a farlo con ottimismo."


translate italian epilogue_sl_5c734fc5:


    "Non è che io abbia provato a cambiare le cose per diventare una persona diversa, ma il mondo ora mi sembra più comprensibile. Non semplice, e neppure amichevole, no, per nulla cordiale, ma semplicemente comprensibile."


translate italian epilogue_sl_d59449dc:


    "Prima non riuscivo proprio a far fronte ad alcuni fatti o eventi, anche se capivo in qualche modo che avvenivano per una ragione. Adesso invece ho cominciato a vedere la vita come qualcosa di più semplice – ciò che accade, accade per il meglio!"


translate italian epilogue_sl_e1feb5f7:



    nvl clear
    "O almeno accade perché deve accadere, e io posso conviverci. E se davvero non ci riuscissi, allora non posso farci nulla. Credo di aver iniziato a sorridere più spesso, o almeno non ho sempre quello sguardo di profonda frustrazione universale ventiquattr'ore al giorno."


translate italian epilogue_sl_a49c2c8d:


    "Persino i miei contatti (su internet, ovviamente) hanno notato un certo cambiamento in me. E le loro opinioni hanno convenuto che tali cambiamenti sono positivi."


translate italian epilogue_sl_cffd8fe2:


    "Ora posso dire di sentirmi pronto a vivere. Grazie a «Sovyonok» e ai suoi abitanti.{w} Senza di loro non ci sarei mai riuscito…"


translate italian epilogue_sl_ce617998_1:


    "…"


translate italian epilogue_sl_76b2fe88:


    nvl clear


translate italian epilogue_sl_61db16fc:


    "Era l'inizio di gennaio."


translate italian epilogue_sl_0d80cbdc:


    "La città si era appena ripresa dalla sbornia di Capodanno, le strade erano quasi deserte durante il giorno. Per non parlare delle notti, durante le quali l'unico straniero che si poteva scorgere camminando per strada sembrava totalmente immerso in certi suoi affari, correndo rapidamente verso una qualche destinazione che non ammetteva ritardo."


translate italian epilogue_sl_f357f57f:


    "Io non avevo una tale destinazione, ho solo deciso di passeggiare un po' – è necessario prendere una boccata d'aria fresca ogni tanto."


translate italian epilogue_sl_6c6bdb78:


    "Una notte d'inverno è il momento migliore per stare da soli con sé stessi."


translate italian epilogue_sl_8cf56a1b:



    nvl clear
    "Ho sempre pensato che la vera solitudine può essere percepita maggiormente tra una folla, piuttosto che in un deserto cocente, o in una pianura sconfinata, o sulla cima di una montagna ricoperta di neve."


translate italian epilogue_sl_761bb15b:


    "Nel flusso di persone, parole, pensieri e aspirazioni, ognuno aveva il proprio obbiettivo e la propria direzione."


translate italian epilogue_sl_2ffddc18:


    "E io ero l'unico a passeggiare tra loro, senza meritare alcun tipo di attenzione da parte degli altri."


translate italian epilogue_sl_c5c605e3:


    "Forse loro sono come dei vettori che si muovono in direzioni diverse? Dei vettori che non si incontrano mai nel sistema cartesiano."


translate italian epilogue_sl_e35f54cf:



    nvl clear
    "Qui è la stessa cosa – queste persone stanno camminando, stanno correndo in una certa direzione, e io mi sto trascinando in un'altra."


translate italian epilogue_sl_6ec15f5e:


    "Ma mi sento davvero solo?{w} Prima – decisamente sì. Ma ora – altamente improbabile."


translate italian epilogue_sl_ad5015f4:


    "Tra le luminose e accecanti luci della città, tra il rumore delle auto e della folla vorticosa, riuscivo a godere di una sinfonia celestiale di silenzio!"


translate italian epilogue_sl_c26cab4f:


    "Ma ora è diverso, ci sono solo io qui fuori, quindi mi trovo in solitudine non solo all'interno di me stesso, ma anche in tutto il resto del mondo."


translate italian epilogue_sl_804dc10a:



    nvl clear
    "Essere un granello di sabbia in un deserto non è come essere una goccia d'acqua in un oceano!"


translate italian epilogue_sl_cc6ceeaf:


    "Tra la folla nessuno mi notava, nessuno mi degnava di attenzione, ma adesso tutti coloro che stanno sbirciando fuori dalle loro finestre staranno pensando: «Perché non se ne sta semplicemente a casa sua?», «Forse ha qualche questione urgente da sbrigare?», «Probabilmente è un pazzo», «O magari è solo un ubriacone?»…"


translate italian epilogue_sl_87c3c17d:


    "Non mi è mai piaciuto uscire dal gregge quando non era opportuno, così l'oppormi alla società non è mai stata un'opzione valida per me."


translate italian epilogue_sl_1e8b6cba:


    "Ma adesso è diverso."


translate italian epilogue_sl_c230e647:



    nvl clear
    "Adesso è come se cercassi di dire a tutti: «Guardate! Anch'io posso essere felice! A voi serve il vostro televisore e la vostra cioccolata calda, ma a me basta la neve, la notte, una stanza buia e un noioso schermo! Guardate! E non sono di certo peggiore di voi!»"


translate italian epilogue_sl_53d3760b:


    "Sento la tristezza nel mio cuore, ma è una tristezza serena."


translate italian epilogue_sl_bb50df60:


    "Ho davvero bisogno delle cose che loro hanno e che io non ho?"


translate italian epilogue_sl_7d88b51c:


    "Forse ho qualcosa di molto più prezioso?"


translate italian epilogue_sl_ce617998_2:


    "…"


translate italian epilogue_sl_950f1be7:


    "Non mi sono nemmeno accorto di essere arrivato alla fermata del bus 410."


translate italian epilogue_sl_7535addf:


    me "Quanti ricordi…"


translate italian epilogue_sl_8da3fa57:


    "Mi sono seduto e ho frugato nelle tasche, cercando le sigarette.{w} Tuttavia, il pacchetto era vuoto."


translate italian epilogue_sl_a54cf057:


    me "Va beh…"


translate italian epilogue_sl_fd5a0f69:


    "L'ho accartocciato e l'ho gettato nel cestino."


translate italian epilogue_sl_a81c77bf:


    me "Almeno non mi rovinerò la salute!"


translate italian epilogue_sl_a97808f5:


    "Una stella solitaria brillava nel cielo."


translate italian epilogue_sl_264e6e2b:


    "Oggi ho letto su internet che, secondo i calcoli degli scienziati, uno dei più affascinanti eventi astronomici si era appena concluso, e che potremo vederlo nuovamente solo tra decenni."


translate italian epilogue_sl_e61de771:


    "Forse la stella che sto vedendo adesso in realtà è già esplosa, e ora non è altro che una splendida immagine nel cielo notturno? Da qualche parte nei pressi di un pianeta sperduto ai margini della galassia."


translate italian epilogue_sl_7ad4fe45:


    "Tuttavia, perché esiste una cosa o l'altra?"


translate italian epilogue_sl_28b32374:


    "Solo perché si trova semplicemente lì, solo perché ha una forma e perché la possiamo toccare?"


translate italian epilogue_sl_c62c06d1:


    "O è perché noi crediamo che essa esista?"


translate italian epilogue_sl_604959ba:


    "A prima vista la risposta è semplice.{w} Ma d'altra parte…"


translate italian epilogue_sl_13a6a7c7:


    "Anche se quella stella fosse già morta, noi riusciamo comunque a vedere la sua fredda luce."


translate italian epilogue_sl_21b7ddca:


    "Forse ha aiutato una persona ad uscire da una foresta innevata, forse ha dato speranza a qualcuno, o un po' di calore a qualcun altro."


translate italian epilogue_sl_19248a5c:


    "Ma come potrebbe un semplice oggetto astronomico, che è esploso Dio solo sa quando, fare tutto questo?{w} Come possono miliardi di persone credere in qualcosa che in realtà non esiste...?"


translate italian epilogue_sl_311843a1:


    "Ma il semplice credere in qualcosa non ha mai reso tale cosa reale.{w} O almeno, non che io sappia…"


translate italian epilogue_sl_ce617998_3:


    "…"


translate italian epilogue_sl_8844dc3d:


    "La nevicata si è fatta più intensa, e ora è diventata una vera e propria tempesta di neve.{w} Mezzanotte."


translate italian epilogue_sl_d1e598ce:


    "Forse la mia carrozza si trasformerà in una zucca in pochi istanti?"


translate italian epilogue_sl_f2165086:


    "Forse il mio ritorno dal campo «Sovyonok» è la fine della mia versione del racconto di «Cenerentola»?"


translate italian epilogue_sl_ced18126:


    "E anche se fosse, è davvero un finale così brutto?"


translate italian epilogue_sl_9f4f4f69:


    "Dopotutto, ogni racconto contiene almeno un briciolo di verità."


translate italian epilogue_sl_f7a557f1:


    "Ricordando Slavya, mi sono alzato in piedi, mi sono scosso dalla neve mi sono incamminato lentamente nella notte."


translate italian epilogue_sl_1eaf8011:


    "Non sentivo alcun dolore, non avevo alcun rimpianto."


translate italian epilogue_sl_aabe0924:


    "Forse tutto questo è solo un altro episodio di quella storia, di quel caloroso sogno che volevo non finisse mai."


translate italian epilogue_sl_498f7c4e:


    "E perché mai non potrebbe ripetersi ancora, se è già successo una volta?"


translate italian epilogue_sl_8ebdfc5e:


    "E se potessi incontrare Slavya nella vita reale...?"


translate italian epilogue_sl_a20cefa7:


    "..."


translate italian epilogue_sl_8074d8af:


    "Stavo per andarmene, ma improvvisamente qualcuno è emerso dal silenzioso velo bianco ed è corso sotto il tetto della fermata dell'autobus."


translate italian epilogue_sl_96af921b:


    "Era chiaramente una ragazza, e aveva una lunga treccia che sbucava fuori dal suo cappuccio."


translate italian epilogue_sl_81772974:


    "Se ne stava in piedi dandomi le spalle, così non ho potuto vederla in viso."


translate italian epilogue_sl_501a8b3a:


    slg "Scusa, l'ultimo autobus è già passato?"


translate italian epilogue_sl_454f3f2b:


    "Ha chiesto senza voltarsi."


translate italian epilogue_sl_14528e2c:


    "Dove potrebbe essere diretta così tardi?"


translate italian epilogue_sl_0a4debed:


    "La 410 dev'essere la tratta più maledetta in questa città."


translate italian epilogue_sl_6e4ac050:


    "Non solo a volte riesce a portarti in un'altra realtà, ma non arriva neppure quando ne hai bisogno."


translate italian epilogue_sl_451ccc51:


    me "È rimasto ancora un bus che passa di qui, poco dopo la mezzanotte, credo."


translate italian epilogue_sl_d722e8b7:


    slg "Grazie."


translate italian epilogue_sl_839f9131:


    "Dal suo tono era chiaro che stesse sorridendo."


translate italian epilogue_sl_58d296bc:


    "Per un po' di tempo mi sono limitato a stare seduto in silenzio, esaminando la ragazza."


translate italian epilogue_sl_09bca910:


    "Alla fine ha parlato:"


translate italian epilogue_sl_ffb89fb4:


    slg "Dove vai così tardi?"


translate italian epilogue_sl_a945e3a8:


    "Davvero le interessa dove vado?"


translate italian epilogue_sl_68e9d064:


    me "Stavo solo facendo una passeggiata…"


translate italian epilogue_sl_f2f10b4d:


    "Ho risposto pensieroso."


translate italian epilogue_sl_9eb3ac16:


    me "E tu… sei qui per qualcosa di importante?"


translate italian epilogue_sl_a8743c64:


    slg "Beh, non proprio…{w} Sto semplicemente tornando a casa."


translate italian epilogue_sl_591089ca:


    me "Dove abiti?"


translate italian epilogue_sl_9a483b3c:


    "Non ho potuto fare a meno di chiederglielo."


translate italian epilogue_sl_f9bd2f63:


    slg "Lontano…"


translate italian epilogue_sl_99a29ab7:


    "Ha detto vagamente."


translate italian epilogue_sl_64f7f202:


    me "Io vivo nelle vicinanze."


translate italian epilogue_sl_a7590312:


    slg "È una bella zona."


translate italian epilogue_sl_c75aa891:


    "Io invece direi che non ce n'è una peggiore di questa."


translate italian epilogue_sl_263611b4:


    me "Non ho avuto molta scelta…"


translate italian epilogue_sl_13f66e78:


    slg "Non ti piace?"


translate italian epilogue_sl_24cdca72:


    me "Non lo so, ormai mi ci sono abituato."


translate italian epilogue_sl_837a1356:


    slg "Credo che una persona possa essere felice ovunque lei voglia!"


translate italian epilogue_sl_907b6d20:


    me "Forse…"


translate italian epilogue_sl_2612f632:


    slg "Beh, pensaci!{w} La gente vive persino in Antartide… nelle stazioni, intendo.{w} E anche nel deserto del Sahara! E in molti altri luoghi. Quello che importa sono gli abitanti stessi!"


translate italian epilogue_sl_a82636de:


    me "Difficile essere in disaccordo con te."


translate italian epilogue_sl_76f34f02:


    "Il mio commento è suonato così filosofico che ho sorriso inconsciamente."


translate italian epilogue_sl_f55284f9:


    slg "Non mi sembri troppo sincero."


translate italian epilogue_sl_724d66a5:


    me "E perché lo penserebbe, madame?"


translate italian epilogue_sl_8baa587f:


    "Ha ridacchiato."


translate italian epilogue_sl_de9d3a74:


    slg "Credo che tu sia una brava persona!"


translate italian epilogue_sl_b4d548da:


    "Poi si è voltata, e…"


translate italian epilogue_sl_7f9ddde9:


    "Ho capito perché la sua voce, la sua postura e tutto il resto mi sembravano così familiari."


translate italian epilogue_sl_5d2bdfb2:


    "Slavya si trovava di fronte a me."


translate italian epilogue_sl_d7bb50f0:


    "Per un istante sono rimasto senza parole, ma solo per un attimo."


translate italian epilogue_sl_30204e6b:


    "Naturalmente poteva trattarsi semplicemente di una ragazza che assomigliava alla Slavya del mio sogno.{w} Oppure stavo sperimentando il più forte dei déjà vu…"


translate italian epilogue_sl_7c085c9f:


    "Questa città non è un campo di pionieri, io non sto indossando una divisa, e non è estate adesso."


translate italian epilogue_sl_a32736d4:


    "Cercando di convincermi che si trattasse solo di un'illusione, ho deciso di farle una domanda."


translate italian epilogue_sl_82ad3926:


    me "Ci siamo mai incontrati prima?"


translate italian epilogue_sl_6ac4a2e6:


    "Mi ha scrutato con attenzione e ha sorriso."


translate italian epilogue_sl_38e86f4c:


    slg "Non credo…{w} Ma il tuo viso mi sembra familiare."


translate italian epilogue_sl_40a6ca02:


    me "Tu… sei mai stata in un campo di pionieri da giovane?"


translate italian epilogue_sl_73927ac0:


    slg "Certo che no!"


translate italian epilogue_sl_ce3666e5:


    "Ha riso."


translate italian epilogue_sl_c7fd5288:


    slg "Quando sono nata non c'erano più."


translate italian epilogue_sl_0d9f82ca:


    "Oh, giusto..."


translate italian epilogue_sl_2d02b5b6:


    slg "Anche se…{w} Ho fatto un sogno di recente…"


translate italian epilogue_sl_60f3d758:


    "Ha pensato per un po'."


translate italian epilogue_sl_83d4403a:


    me "Anch'io.{w} Voglio dire, riguardo a un campo di pionieri."


translate italian epilogue_sl_fe5d4dde:


    slg "Forse è lì che ci siamo incontrati."


translate italian epilogue_sl_e26e8716:


    "Ha detto con serietà."


translate italian epilogue_sl_fd914013:


    me "Forse."


translate italian epilogue_sl_4c079f2d:


    slg "A proposito, il mio nome è Slavya. In realtà il mio nome completo è Slavyana, ma tutti mi chiamano Slavya. Puoi farlo anche tu!"


translate italian epilogue_sl_b4002489:


    "Stavo per dirle che lo sapevo già, ma poi mi sono trattenuto."


translate italian epilogue_sl_b54dd65c:


    "Ad ogni modo, è così importante sapere perché la Slavya del mio sogno è in piedi proprio qui, proprio adesso di fronte a me, in questa gelida fermata del magico bus numero 410?"


translate italian epilogue_sl_a4217a2f:


    "Dopotutto, ci sono sogni dai quali non vorresti mai risvegliarti!"


translate italian epilogue_sl_94f8232c:


    me "E io mi chiamo Semyon…"


translate italian epilogue_sl_e62d08e6:


    sl "Che bel nome."


translate italian epilogue_sl_6170b881:


    me "Anche il mio inizia per «S»."


translate italian epilogue_sl_8b01f8be:


    sl "Già."


translate italian epilogue_sl_8b7f6f51:


    me "Allora, vivi lontano, non è vero?"


translate italian epilogue_sl_af4e649f:


    sl "Beh, non esattamente…"


translate italian epilogue_sl_7cfc34fb:


    "Mi ha detto il nome del quartiere."


translate italian epilogue_sl_f14fd67f:


    me "Pensavo che tu fossi del sud."


translate italian epilogue_sl_70f37a53:


    sl "Perché?"


translate italian epilogue_sl_e902ce8b:


    me "Non lo so…{w} Forse perché assomigli a un pioniere."


translate italian epilogue_sl_90481d5f:


    sl "Oh, tu e i tuoi pionieri!"


translate italian epilogue_sl_c954d69f:


    "Slavya ha riso."


translate italian epilogue_sl_ee42356b:


    me "Forse non ci saremmo mai incontrati qui se non fosse stato per loro."


translate italian epilogue_sl_70f37a53_1:


    sl "Perché?"


translate italian epilogue_sl_cfc2bc6d:


    "La sua espressione si è fatta seria."


translate italian epilogue_sl_e8b9f9a3:


    me "Stavo… solo scherzando…{w} Dimmi, pensi anche tu che il mondo esiste perché noi crediamo in esso?"


translate italian epilogue_sl_17172b5c:


    sl "Quella è una sorta di idealismo soggettivo…"


translate italian epilogue_sl_bbc1f2e4:


    "Slavya ci ha pensato su."


translate italian epilogue_sl_eb6cea32:


    me "Ultimamente mi sono avvicinato a questa scuola di pensiero."


translate italian epilogue_sl_5095dd33:


    sl "Beh, non lo so...{w} Non è che ne sappia molto di queste materie."


translate italian epilogue_sl_e15fe545:


    me "E allora quali sono le tue materie?"


translate italian epilogue_sl_d1046a49:


    "Ha sorriso."


translate italian epilogue_sl_719e7c45:


    sl "Diverse."


translate italian epilogue_sl_7f708c46:


    me "Non ne dubito!"


translate italian epilogue_sl_d1aa5e5c:


    sl "Ma sono appena passati pochi minuti da quando ci siamo incontrati!"


translate italian epilogue_sl_79ac6bd7:


    me "Ma pare che siano molti di più."


translate italian epilogue_sl_1cc42161:


    sl "Per qualche ragione lo penso anch'io…"


translate italian epilogue_sl_cc1284f3:


    "È seguito un imbarazzante silenzio."


translate italian epilogue_sl_644f4e1f:


    "La nevicata si è placata, ma l'autobus non si vedeva ancora."


translate italian epilogue_sl_d86d06b8:


    "Dopo aver guardato l'orologio ho pensato che ormai non sarebbe più arrivato."


translate italian epilogue_sl_6a9e8821:


    "Slavya sembrava avere ancora più freddo adesso, dato che le sue guance erano diventate ancora più rosse e le sue mani si muovevano freneticamente, cercando di scaldare il suo corpo."


translate italian epilogue_sl_161b1e49:


    me "Pare che il bus 410 non arriverà più."


translate italian epilogue_sl_1555b86b:


    sl "Suppongo…"


translate italian epilogue_sl_1e8379d1:


    "Ha detto con tristezza."


translate italian epilogue_sl_732bddc6:


    me "Se vuoi…"


translate italian epilogue_sl_8fe9063e:


    "Ho iniziato, dopo aver preso coraggio."


translate italian epilogue_sl_e07c37b3:


    sl "Cosa?"


translate italian epilogue_sl_beb50f2a:


    me "Puoi stare a casa mia…"


translate italian epilogue_sl_4f36139c:


    sl "Ma ci siamo appena conosciuti."


translate italian epilogue_sl_0467a4c4:


    "Ha riso."


translate italian epilogue_sl_bd3e21d9:


    "Non sembrava spaventata nemmeno un po'."


translate italian epilogue_sl_6b12c510:


    me "Sì, capisco, ma fa così freddo qui fuori…"


translate italian epilogue_sl_d03f0acf:


    sl "Non sei spaventato?"


translate italian epilogue_sl_c93fe8e6:


    "Slavya mi ha guardato scherzosamente."


translate italian epilogue_sl_10f87319:


    me "Io? Dovrei esserlo?"


translate italian epilogue_sl_b71c6525:


    sl "Chissà… Chissà…"


translate italian epilogue_sl_58f5f795:


    me "Io te l'ho proposto, non pensare a niente del genere…"


translate italian epilogue_sl_c99f65be:


    "Ho iniziato a giustificarmi."


translate italian epilogue_sl_1ee2e52a:


    sl "Capisco!"


translate italian epilogue_sl_ec2c2a7c:


    "Ha riso."


translate italian epilogue_sl_0cc7ab77:


    sl "Se non ti dispiace, ovviamente…"


translate italian epilogue_sl_7ec51fd8:


    "Anche se sembrava impossibile, è arrossita ancora di più."


translate italian epilogue_sl_71f4991e:


    me "Andiamo?"


translate italian epilogue_sl_c1235c80:


    sl "Aspettiamo ancora un po', forse arriverà."


translate italian epilogue_sl_71d3b8c8:


    me "Ok."


translate italian epilogue_sl_ce617998_4:


    "…"


translate italian epilogue_sl_42acf572:


    sl "Cosa fai nel tuo tempo libero?"


translate italian epilogue_sl_ac2505d0:


    "Mi ha chiesto Slavya, dopo un po'."


translate italian epilogue_sl_cebb61ed:


    me "Io… Beh…{w} Lavoro a casa, per così dire."


translate italian epilogue_sl_03bc366f:


    "Non potevo semplicemente dirle che non facevo niente."


translate italian epilogue_sl_36ffb439:


    sl "Sembra bello!"


translate italian epilogue_sl_75f4e79a:


    me "Perché?"


translate italian epilogue_sl_d74e7a4a:


    sl "Non so…{w} Hai un sacco di tempo libero, non sei costretto ad andare da nessuna parte! E così via…"


translate italian epilogue_sl_b78c6e26:


    me "E tu?"


translate italian epilogue_sl_b620325b:


    "Le ho chiesto, cercando di cambiare argomento."


translate italian epilogue_sl_c42d7da0:


    sl "Io studio."


translate italian epilogue_sl_606db6d9:


    me "Vuoi avere una laurea in etnografia?"


translate italian epilogue_sl_01f26b1d:


    sl "E che cos'è?"


translate italian epilogue_sl_38a71a20:


    "Mi ha chiesto seriamente."


translate italian epilogue_sl_a52d1d58:


    me "Non importa…"


translate italian epilogue_sl_5bca72ca:


    sl "Sarò un'ecologista."


translate italian epilogue_sl_f19cb2e9:


    me "Beh, una carriera di tutto rispetto…"


translate italian epilogue_sl_ac8018d5:


    "Abbiamo chiacchierato di varie cose, ma l'autobus continuava a non farsi vedere."


translate italian epilogue_sl_8231d531:


    "Era già l'una passata.{w} Il tempo scorreva così in fretta."


translate italian epilogue_sl_e3127a21:


    "Credo che Slavya avesse capito quello che stavo pensando."


translate italian epilogue_sl_ad3d2e0c:


    sl "Andiamo?"


translate italian epilogue_sl_06ab0532:


    "Ha sorriso."


translate italian epilogue_sl_a6a0a962:


    "Mi sono alzato in piedi e ci siamo incamminati lentamente verso la mia casa."


translate italian epilogue_sl_312acdec:


    "Mi teneva delicatamente per il braccio, guardandomi negli occhi."


translate italian epilogue_sl_ae697d22:


    sl "Sai, sono sicura che quest'anno sarà migliore di quello appena passato!"


translate italian epilogue_sl_a20cefa7_1:


    "..."
# Decompiled by unrpyc: https://github.com/CensoredUsername/unrpyc
