
translate italian epilogue_mi_4273f0c8:


    "Perché a volte un sogno è solo un sogno."


translate italian epilogue_mi_12b67fb5:


    "Non c'è bisogno di andare su internet, alla ricerca di un'interpretazione del sogno che ha invaso il tuo inconscio in modo così sgarbato. Non bisogna considerarlo un segno, una profezia, una previsione del futuro."


translate italian epilogue_mi_e27e5ad0:


    "A volte un sogno è solo un sogno, una storia bellissima che non può accadere nella vita reale, una fiaba abbozzata dal tuo cervello a riposo e dipinta dalla tua immaginazione."


translate italian epilogue_mi_5d3e8602:


    "A volte un sogno è solo un sogno..."


translate italian epilogue_mi_f429eb4f:


    "Anche se è suddiviso in più parti."


translate italian epilogue_mi_5192a263:


    ma "Accidenti, sono così stanca di tutto questo…"


translate italian epilogue_mi_b6e6bdf4:


    my "Eh?"


translate italian epilogue_mi_7ac33094:


    "Ho alzato lo sguardo e ho visto una ragazza china sopra di me."


translate italian epilogue_mi_39748bd3:


    my "Di cosa sei stanca esattamente?"


translate italian epilogue_mi_73e7a292:


    ma "Di tutto!"


translate italian epilogue_mi_0e1ee137:


    my "Non è che qualcuno ti abbia costretto… o che ti stia costringendo!"


translate italian epilogue_mi_eb1fb061:


    ma "Beh…{w} Non è così semplice!"


translate italian epilogue_mi_d9924f3f:


    my "Oh, invece sì che lo è!"


translate italian epilogue_mi_fc671583:


    ma "Accidenti, sei sempre così…"


translate italian epilogue_mi_f98fd3f1:


    mt "Basta chiacchierare! Dai, mettiamoci al lavoro!"


translate italian epilogue_mi_f45e1707:


    "Una voce che non avrebbe tollerato alcuna obiezione è arrivata da lontano."


translate italian epilogue_mi_a3bffb92:


    "L'autobus si è svuotato in un istante."


translate italian epilogue_mi_ab07d7f4:


    "La ragazza con la quale stavo discutendo ha messo il broncio come una bambina. Poi si è voltata di scatto, ha battuto i tacchi e si è diretta verso l'uscita."


translate italian epilogue_mi_07837af7:


    "Il suo nome era Masha."


translate italian epilogue_mi_44f92a18:


    mt "Ok, sei pronto?"


translate italian epilogue_mi_514d8a74:


    "È apparsa una donna. Il suo viso esprimeva agonia per le sorti dell'intera l'umanità, arroganza, concentrazione e fatica, tutto contemporaneamente."


translate italian epilogue_mi_da181777:


    my "Sì… Certo…"


translate italian epilogue_mi_83cfac39:


    mt "Eccellente!"


translate italian epilogue_mi_a1a01658:


    mt "Pronti…{w} azione!"


translate italian epilogue_mi_86b86796:


    "Non mi sono neppure accorto di quando avesse abbandonato il bus, che una cinepresa è apparsa dal nulla, inquadrandomi."


translate italian epilogue_mi_d2175e55:


    "Ho chiuso gli occhi – esatto, dovrei dormire secondo il copione."


translate italian epilogue_mi_726cd530:


    "Quanto tempo devo restare così? Dieci secondi? Venti?"


translate italian epilogue_mi_5c06941f:


    "Dovrebbe essere sufficiente."


translate italian epilogue_mi_44a112d2:


    "Ho aperto gli occhi lentamente e ho iniziato ad esaminare il bus con lo sguardo perso nel vuoto."


translate italian epilogue_mi_c58cb760:


    "Ecco, non ho la minima idea del perché io mi trovi qui!"


translate italian epilogue_mi_c96b254d:


    "Cos'è questo?! Un Ikarus? Ma perché?"


translate italian epilogue_mi_c77489ba:


    "E quindi dovrei fare l'espressione più sorpresa di sempre!"


translate italian epilogue_mi_28036f42:


    "Dovrei immaginarmi seduto sul wc a leggere un libro, e che all'improvviso la porta si spalanchi e mi trovi davanti Jean Reno con una pistola puntata verso di me."


translate italian epilogue_mi_1d68d7b6:


    "Escusez-moi, non vede che sarei occupato a – perdoni il mio francese – fare un bisognino?"


translate italian epilogue_mi_81e3cf06:


    "Ne resterebbe di certo sorpreso!"


translate italian epilogue_mi_74e80531:


    "Non ho resistito e sono scoppiato a ridere."


translate italian epilogue_mi_92ab1fe0:


    mt "Stop! Stop!"


translate italian epilogue_mi_5aaa9752:


    "Ha urlato la regista."


translate italian epilogue_mi_f84967b1:


    mt "Questo è un oltraggio! Stiamo già girando da una settimana, e non abbiamo in mano ancora niente!"


translate italian epilogue_mi_d4d147e6:


    "È balzata davanti a me."


translate italian epilogue_mi_ca068b02:


    mt "Qual è il problema adesso?"


translate italian epilogue_mi_f4f4dacd:


    my "Sa, io…"


translate italian epilogue_mi_d82fb8a9:


    "Beh, non credo che apprezzerebbe la mia storiella su Jean Reno…"


translate italian epilogue_mi_27f98f9f:


    my "Mi dispiace…"


translate italian epilogue_mi_399d102b:


    "È stato difficile guardarla negli occhi."


translate italian epilogue_mi_548b6f7a:


    "Non è che fossi preoccupato di mandare all'aria l'ennesima ripresa, ma piuttosto del fatto di scoppiare a ridere nuovamente."


translate italian epilogue_mi_ed0fe155:


    mt "Ok, pausa pranzo!"


translate italian epilogue_mi_4d90d1cf:


    "La nostra regista si è tolta il cappello panama e ha iniziato a usarlo come ventaglio."


translate italian epilogue_mi_d7d387fd:


    mt "Fa così caldo…"


translate italian epilogue_mi_2c1992b5:


    sh "Beh, è estate dopotutto."


translate italian epilogue_mi_1d5a2e57:


    "Shurik è comparso dal nulla, dimostrando di essere dotato di spirito di osservazione."


translate italian epilogue_mi_193c877e:


    mt "Toh, zuccone! Faresti meglio a occuparti della cinepresa!"


translate italian epilogue_mi_2f643813:


    "Shurik ha schivato tranquillamente il Panama che gli è stato gettato addosso ed è tornato alla sua cinepresa."


translate italian epilogue_mi_0781974d:


    "Lui è sempre così – tranquillo, saggio e…{w} noioso, monotono e abbastanza prevedibile."


translate italian epilogue_mi_3b0ed2d0:


    "Sono sceso dal bus e mi sono diretto alla mensa per mangiare un boccone."


translate italian epilogue_mi_11a4fd66:


    "E all'entrata mi sono imbattuto in Masha..."


translate italian epilogue_mi_eb88f1fb:


    ma "Allora?"


translate italian epilogue_mi_48975165:


    my "Niente…"


translate italian epilogue_mi_30261bba:


    ma "Non c'è da stupirsi."


translate italian epilogue_mi_23457c21:


    my "Mangiamo qualcosa?"


translate italian epilogue_mi_5a6ffa85:


    "Mi ha fissato per un po', senza capire."


translate italian epilogue_mi_c8eb94cd:


    ma "Ok, andiamo."


translate italian epilogue_mi_a274cb3e:


    "È strano, sembra che Masha abbia già visitato la mensa – cos'è, non ha avuto tempo di pranzare?"


translate italian epilogue_mi_00d4b994:


    "L'ho esaminata a fondo da dietro – le doppie code di cavallo erano davvero lunghe e penzolavo da un lato all'altro, colpendo con regolarità tutto ciò che incontravano lungo il loro tragitto."


translate italian epilogue_mi_5b40950c:


    my "Sei sicura che quei capelli non ti diano fastidio?"


translate italian epilogue_mi_a5498145:


    "Ho chiesto a Masha dopo che ci siamo seduti al tavolo."


translate italian epilogue_mi_64c890f7:


    ma "Qual è il problema?"


translate italian epilogue_mi_5e7e8446:


    my "Beh, sono un po'…{w} troppo lunghi, no?"


translate italian epilogue_mi_518927bf:


    ma "Fa parte del mio personaggio, scemo!"


translate italian epilogue_mi_be22885b:


    my "Come se prima fossero più corti…"


translate italian epilogue_mi_b990f6b1:


    "Mi ha lanciato uno sguardo adirato e ha iniziato a mescolare con decisione il contenuto del suo pacchetto di spaghetti istantanei."


translate italian epilogue_mi_afce6747:


    ma "Prima o poi questa dieta mi farà venire la gastrite."


translate italian epilogue_mi_a6971a64:


    my "A me è già venuta…"


translate italian epilogue_mi_fad116df:


    ma "Oh, certo, scommetto che ti sei già bevuto tutto il materiale di scena insieme con i ragazzi!"


translate italian epilogue_mi_289391d1:


    my "E cosa c'è di sbagliato?{w} Io non vengo mica pagato per questo lavoro, giusto perché tu lo sappia! E dopotutto, il ruolo di protagonista è molto difficile! Non mi bevo le cineprese o i nastri di registrazione!"


translate italian epilogue_mi_513a7fe4:


    ma "Oh, come se fossi candidato a un premo Oscar!"


translate italian epilogue_mi_1e087707:


    my "Chissà… Chissà…"


translate italian epilogue_mi_5175d639:


    "Mi sono reclinato sulla sedia e ho acceso una sigaretta."


translate italian epilogue_mi_673b46b1:


    ma "Per tua informazione, questo è un campo per bambini, nel caso te lo fossi dimenticato!"


translate italian epilogue_mi_8a8d8f52:


    my "Ma davvero? E dove sarebbero questi bambini?"


translate italian epilogue_mi_6f794cba:


    "Masha si è limitata a tirare su col naso."


translate italian epilogue_mi_b3a659cb:


    my "E tra l'altro, tutti i grandi attori hanno dovuto iniziare con cose semplici…{w} Alcuni di loro, per esempio, hanno iniziato con una tesi di laurea…"


translate italian epilogue_mi_bbbe361d:


    ma "Nessuno sano di mente penserebbe mai di girare una spazzatura come questa!"


translate italian epilogue_mi_f47e1355:


    "Si è guardata attorno cautamente con sguardo intimidito, assicurandosi che nessuno fosse nella mensa a parte noi due, e ha proseguito."


translate italian epilogue_mi_5cd04ce7:


    ma "Solo la nostra Olga Dmitrievna l'avrebbe fatto."


translate italian epilogue_mi_c4de2e46:


    my "Non è che qualcuno ti abbia costretta a partecipare."


translate italian epilogue_mi_143442a9:


    ma "Oh davvero?"


translate italian epilogue_mi_9912a5af:


    "Mi ha detto lentamente, con amarezza."


translate italian epilogue_mi_00cde85b:


    ma "Come se avessi avuto scelta. Lo stesso vale per te. E per chiunque altro…"


translate italian epilogue_mi_d9a5ec9c:


    my "Beh, per cominciare, avresti potuto proporre un tema diverso."


translate italian epilogue_mi_ba9e80db:


    ma "Proporre un tema…{w} Quale tema…"


translate italian epilogue_mi_20caf834:


    "Per tutto questo tempo Masha ha continuato a versare il sale nella sua coppetta di pasta, e sembrava non avesse intenzione di fermarsi."


translate italian epilogue_mi_53e387c4:


    my "Vedo che ti piace salato…"


translate italian epilogue_mi_20f54ffc:


    ma "Cos...?"


translate italian epilogue_mi_124641aa:


    "Ho indicato la sua coppetta con lo sguardo."


translate italian epilogue_mi_321a700f:


    ma "Dannazione!"


translate italian epilogue_mi_a30b21a1:


    "Ha sbattuto la saliera sopra al tavolo e ha abbassato tristemente lo sguardo verso la collinetta di sale che, come un iceberg che lottava contro il surriscaldamento globale, si stava rifiutando di sciogliersi nel brodo."


translate italian epilogue_mi_f206e4c8:


    ma "Oh, e dai…"


translate italian epilogue_mi_111d10a4:


    "Improvvisamente mi è dispiaciuto per Masha."


translate italian epilogue_mi_ad4d8e3a:


    "In ogni caso, non sarei comunque riuscito a digerire questa schifezza – i panini sono molto meglio."


translate italian epilogue_mi_e6bf5c70:


    my "Tieni."


translate italian epilogue_mi_41d4a8f6:


    "Ho spinto la mia tazza verso di lei."


translate italian epilogue_mi_7d3aefd1:


    "L'ha presa in silenzio e ha cominciato a mangiare."


translate italian epilogue_mi_5307eb63:


    my "Non mi ringrazi nemmeno?"


translate italian epilogue_mi_6d330cb9:


    ma "Ma è stata tutta colpa tua!"


translate italian epilogue_mi_9536b49c:


    my "Che cosa, esattamente?"


translate italian epilogue_mi_c8bad511:


    ma "Mi hai distratto e io…{w} In ogni caso, è colpa tua!"


translate italian epilogue_mi_8c82cb1d:


    my "Ah, se lo dici tu…"


translate italian epilogue_mi_90823446:


    "Masha ha continuato a mangiare in silenzio."


translate italian epilogue_mi_e695e8fb:


    "Ho finito di fumare e ho spento la sigaretta sullo spigolo del tavolo."


translate italian epilogue_mi_2f0cd122:


    my "Che cosa non ti piace esattamente?"


translate italian epilogue_mi_62eb8e6c:


    ma "Beh, in realtà è accettabile, devi solo abituarti – gli spaghetti sono gustosi."


translate italian epilogue_mi_af79cf2d:


    "Ho guardato Masha in un modo tale che ha chiarito immediatamente quanto fosse stata stupida e irrilevante la sua risposta."


translate italian epilogue_mi_a4712259:


    ma "Ah, stai parlando del film…"


translate italian epilogue_mi_e9b89811:


    my "Del film."


translate italian epilogue_mi_9e1884cf:


    ma "Prendi il mio personaggio, per esempio!{w} Anche se non m'importa di usarlo come esempio, è la parte peggiore del copione! Hai almeno riletto quello che hai scritto?"


translate italian epilogue_mi_a744ace5:


    "Nei primi due giorni sì – poi la pigrizia ha preso il sopravvento."


translate italian epilogue_mi_d3a9b814:


    my "Beh, l'ho letto…"


translate italian epilogue_mi_bf175512:


    ma "E quindi, ti sembra normale?"


translate italian epilogue_mi_166834c3:


    my "Che cosa esattamente?"


translate italian epilogue_mi_90e5468d:


    ma "Il personaggio, il mio personaggio!"


translate italian epilogue_mi_1ebebef3:


    my "Cosa c'è di sbagliato?"


translate italian epilogue_mi_dee08048:


    "Ho deciso di fare il finto tonto fino alla fine."


translate italian epilogue_mi_7d9ee071:


    ma "Smettila di prendermi in giro!"


translate italian epilogue_mi_25e9c59a:


    "Se gli sguardi potessero uccidere, la mia morte sarebbe stata piuttosto dolorosa."


translate italian epilogue_mi_534485c8:


    my "Va bene, va bene…{w} Calmati."


translate italian epilogue_mi_bc30c075:


    "Masha ha posato il suo sguardo su di me."


translate italian epilogue_mi_f7f9f25d:


    my "Cosa c'è che non va col tuo personaggio? È solo una normale ragazza ritardata…{w} Dopotutto ce ne sono così tante in giro, non è così?"


translate italian epilogue_mi_db110686:


    "All'inizio è diventata rossa, poi blu, poi è diventata verde."


translate italian epilogue_mi_979d0955:


    "Forse i colori sul suo viso non avevano seguito esattamente quella sequenza, ma ad ogni modo Masha ha assunto un'espressione spaventosa."


translate italian epilogue_mi_69b1e1a6:


    ma "Lo sai, è proprio disgustoso."


translate italian epilogue_mi_83a2f0c6:


    "Mi ha detto, calmandosi all'improvviso."


translate italian epilogue_mi_d122e6e9:


    my "Penso che sia perfettamente normale…{w} Il cast di un film – o di un libro, se è per questo – non può consistere solo di personaggi simpatici, intelligenti e sexy. Abbiamo ancora bisogno di ruoli di supporto."


translate italian epilogue_mi_baea95af:


    ma "Di supporto, ma non ritardati!"


translate italian epilogue_mi_b4e96502:


    my "Non è che non esistano nella vita reale…"


translate italian epilogue_mi_8951537d:


    ma "Ma io non sono così!"


translate italian epilogue_mi_429736bb:


    my "Il personaggio e l'attore sono due cose distinte!"


translate italian epilogue_mi_cb5eb573:


    "La sua testardaggine ha cominciato a darmi sui nervi."


translate italian epilogue_mi_97cf9711:


    ma "La gente mi vedrà e penserà: Masha è una ritardata!"


translate italian epilogue_mi_9439a255:


    "Mi sono coperto il viso con le mani, appoggiando la fronte sul tavolo."


translate italian epilogue_mi_7928ecf8:


    my "Ok, allora cosa suggeriresti?"


translate italian epilogue_mi_82e7adc3:


    ma "Non lo so…{w} È tutto così sbagliato!"


translate italian epilogue_mi_b9def702:


    my "E secondo te migliorerai le cose continuando a piangere e a lamentarti?"


translate italian epilogue_mi_499a3ecd:


    ma "Non mi sto lamentando!"


translate italian epilogue_mi_4fa7d502:


    my "Oh, come se io non sentissi i tuoi piagnistei…"


translate italian epilogue_mi_d6c365e1:


    "Beh, preferirei non sentirli, se solo potessi."


translate italian epilogue_mi_51775003:


    ma "Sai che ti dico?!"


translate italian epilogue_mi_012acf66:


    "Ho aperto un occhio e ho guardato in alto l'espressione infuriata di Masha."


translate italian epilogue_mi_5cec30a4:


    my "Che cosa?"


translate italian epilogue_mi_99d8ebb3:


    ma "Sei solo un bastardo insensibile!"


translate italian epilogue_mi_7893761a:


    my "Come desidera, mademoiselle!"


translate italian epilogue_mi_c5902aa6:


    ma "E…"


translate italian epilogue_mi_44ce257f:


    "Non è riuscita a terminare la frase – la porta si è spalancata, e un grido allegro ci ha raggiunto."


translate italian epilogue_mi_c3914184:


    sa "Masha-nyan! Semyon-kun!"


translate italian epilogue_mi_f994b25d:


    "Oh, ecco che arriva un'altra pazza furiosa…"


translate italian epilogue_mi_6bdb159c:


    "Oddio, cos'ho fatto di male per meritarmi tutto questo…?"


translate italian epilogue_mi_d10088d4:


    "Ho girato la testa, lanciando uno sguardo scettico alla ragazza che era appena entrata nella mensa."


translate italian epilogue_mi_9a041eba:


    "Sasha.{w} Una delle protagoniste."


translate italian epilogue_mi_239efec8:


    "Se si confronta la Sasha della vita reale con il personaggio che le è stato assegnato, allora le lamentele di Masha circa l'ingiustizia di questo mondo non sembrano poi così prive di fondamento."


translate italian epilogue_mi_92638c32:


    "D'altra parte non si può di certo ignorare il talento di recitazione di Sasha – diventa davvero una persona completamente diversa sullo schermo!"


translate italian epilogue_mi_c612ee04:


    ma "Te l'ho già detto mille volte – non chiamarmi più Masha-nyan!"


translate italian epilogue_mi_fe3b21aa:


    "Masha ha stretto i denti."


translate italian epilogue_mi_8b59578c:


    sa "Ok, allora ti chiamerò Mashyan! Posso, posso?"


translate italian epilogue_mi_389929aa:


    "Sasha ha afferrato una sedia, l'ha portata verso di noi e si è seduta accanto a me."


translate italian epilogue_mi_a334a043:


    ma "Rimbambita…{w} Com'è possibile che certe persone riescano ad entrare all'università?"


translate italian epilogue_mi_4b8e4bdb:


    sa "Nya! Nya! Di cosa state parlando?"


translate italian epilogue_mi_c8b5ead4:


    "Sasha sembrava ignorare l'opinione di Masha riguardo alla sua incompetenza professionale."


translate italian epilogue_mi_c7467996:


    my "Oh, stavamo giusto discutendo di cosa esattamente non piace a Masha-nyan del nostro copione."


translate italian epilogue_mi_298983e2:


    ma "Te lo do io il tuo Nyan!"


translate italian epilogue_mi_84102612:


    "Ha cercato di colpirmi sulla fronte con un pugno, ma sono riuscito a schivarlo, e il suo colpo è finito sul tavolo."


translate italian epilogue_mi_f5ad59fd:


    "Sasha ha emesso un breve urlo e ha iniziato a tremare dalla paura."


translate italian epilogue_mi_9038c77e:


    my "Visto com'è scontenta? Una giovane mente curiosa, che non accetta alcun compromesso, pronta per un lungo e tedioso lavoro, con spirito di sacrificio e la negazione del fragile mondo materiale. E tutto per cosa? Per la verità!"


translate italian epilogue_mi_bd2c4c91:


    sa "Wow, sei così intelligente!"


translate italian epilogue_mi_3ef4a660:


    "Ha mormorato Sasha, afferrandomi il braccio."


translate italian epilogue_mi_852d3af8:


    "Peccato che tu invece non lo sia…"


translate italian epilogue_mi_c573c728:


    ma "Nooo, sei proprio senza speranza…"


translate italian epilogue_mi_f76258c4:


    my "Lo considero come un complimento."


translate italian epilogue_mi_e76f1572:


    "Masha ha sfilato il suo telefono dalla tasca e ha controllato l'ora."


translate italian epilogue_mi_10fe4a30:


    ma "Ok, è ora di andare."


translate italian epilogue_mi_1b4f07fe:


    my "Dove stai andando? La tua parte inizia solo al secondo giorno."


translate italian epilogue_mi_fd715212:


    ma "Sei tu che dovresti andare, stupido!"


translate italian epilogue_mi_ed7479aa:


    sa "Anch'io, anch'io!"


translate italian epilogue_mi_b3fcc6a8:


    "Sasha saltellava intorno, ancora aggrappata al mio braccio."


translate italian epilogue_mi_12e0f2c5:


    my "Suppongo di sì…"


translate italian epilogue_mi_3cd326b5:


    "Mi sono alzato, ho spinto la sedia sotto al tavolo e guardato Masha."


translate italian epilogue_mi_1e6a6efa:


    my "Ci vediamo."


translate italian epilogue_mi_f04f64a5:


    ma "Oh, andatevene…"


translate italian epilogue_mi_99aa653e:


    "Ha sbuffato, e si è voltata."


translate italian epilogue_mi_eb124edb:


    sa "Ittekimasu!"


translate italian epilogue_mi_102232fe:


    ma "Sparishu…"


translate italian epilogue_mi_400f3980:


    "Mentre quelle ultime parole raggiungevano le mie orecchie, Sasha mi è passata davanti, seguita dai pacchetti di pasta vuoti lanciati alle sue spalle…"


translate italian epilogue_mi_a20cefa7:


    "..."


translate italian epilogue_mi_cf8c504b:


    "Me ne stavo seduto sul bus, sfogliando pigramente il copione."


translate italian epilogue_mi_5c5ba007:


    "E quindi chi è l'idiota che ha scritto questa roba?"


translate italian epilogue_mi_f3c1a9e9:


    "Ah, già. Sarei io…"


translate italian epilogue_mi_5e46e93b:


    "Ho messo da parte la pila di fogli in formato A4 e ho portato lo sguardo fuori dal finestrino."


translate italian epilogue_mi_aceecbb8:


    "Faceva proprio un gran caldo."


translate italian epilogue_mi_d760f21d:


    "E lo si poteva dire anche senza essere il «Capitan Ovvio» Shurik."


translate italian epilogue_mi_2ef129f9:


    "Prendiamo l'asfalto, per esempio. Non importa proprio niente che ieri fosse inverno secondo il copione – si sa che deve fondere qui e adesso."


translate italian epilogue_mi_9380d7a8:


    "E alle cavallette non importa – loro continuano a fare le prove comunque."


translate italian epilogue_mi_856be263:


    "A proposito, quali sono quelli che stridono?{w} Sono i grilli o le cavallette? O forse le cicale?{w} Nah, quelle friniscono…"


translate italian epilogue_mi_64bfb5a6:


    "Accidenti, qui non c'è nemmeno la connessione internet per poter fare una rapida ricerca…"


translate italian epilogue_mi_0ef08c24:


    "Ma perché dovrei farlo? Lo script è stato ormai approvato così com'è, e non può più essere modificato."


translate italian epilogue_mi_a078b3cc:


    "E comunque, a chi importa delle cavallette nel nostro film?"


translate italian epilogue_mi_e5cca34a:


    "Anche se non sarebbe una cattiva idea girare un film horror in puro stile B-movie americano degli anni cinquanta."


translate italian epilogue_mi_3f1a9553:


    "Come, ad esempio, «Mio Cugino è una Cavalletta». O «Il Giorno del Grillo». «Qualcuno Volò sul Nido della Cicala»…"


translate italian epilogue_mi_ff14da99:


    "Se dovessi trascorrere ancora altri cinque minuti qui, diventerei io stesso un insetto!"


translate italian epilogue_mi_578e7bc1:


    "Oh, perché non ci sono i condizionatori nei vecchi autobus sovietici…?"


translate italian epilogue_mi_a93c1fe4:


    mt "Ehilà! Pronto! C'è nessuno in casa?"


translate italian epilogue_mi_cc507cf7:


    "Un altoparlante è spuntato all'improvviso, rivolto verso di me, e dietro a esso ho riconosciuto il volto di Olga Dmitrievna nell'aria bollente."


translate italian epilogue_mi_f4a4def9:


    my "Si prega di lasciare un messaggio dopo il…"


translate italian epilogue_mi_619302ff:


    "Ho sentito un colpo."


translate italian epilogue_mi_5d7f43c9:


    my "Ehi! Non si può trattare così il protagonista! Ha letto almeno il mio copione?"


translate italian epilogue_mi_ffdbc81e:


    mt "Sta' zitto!{w} Questa dovrà essere la nostra ultima ripresa nel bus!"


translate italian epilogue_mi_968a77a4:


    "Volevo anch'io che la scena del bus finisse – dopotutto è meglio stare fuori che dentro a questo forno a quattro ruote."


translate italian epilogue_mi_61c207fa:


    "La cinepresa di Shurik pendeva su di me."


translate italian epilogue_mi_3cf97e79:


    "Ho recitato come se fossi davvero disperato riguardo a quello che stava succedendo, imperversando attraverso il bus, precipitandomi di fuori, correndo tutt'attorno e, infine, crollando sul marciapiede…"


translate italian epilogue_mi_d536fac8:


    mt "Stop!"


translate italian epilogue_mi_cb64514a:


    "Olga Dmitrievna ha sorriso con approvazione."


translate italian epilogue_mi_fdafbf2c:


    mt "Beh, sembra proprio che tu sia in grado di fare le cose per bene quando ti impegni!"


translate italian epilogue_mi_753f8975:


    "Certo, ho agito disperatamente perché mi sono impegnato, come no..."


translate italian epilogue_mi_f8e1553e:


    mt "E ora – l'incontro con Slavya."


translate italian epilogue_mi_25e7e67f:


    ro "Magari facciamo una pausa...?"


translate italian epilogue_mi_5b25a91f:


    "Ha supplicato Router."


translate italian epilogue_mi_59395cbc:


    mt "Non siamo qui in vacanza! Torna subito alla tua cinepresa!"


translate italian epilogue_mi_0378d59e:


    "Ha abbassato la testa e si è trascinato verso la sua postazione."


translate italian epilogue_mi_36a7ec5f:


    mt "Azione!"


translate italian epilogue_mi_c787cab8:


    "Ho fissato il cancello del Campo."


translate italian epilogue_mi_ef82b877:


    "Ho mantenuto lo sguardo sulle statue dei pionieri, facendo qualche passo in avanti."


translate italian epilogue_mi_0b401918:


    "Sasha è apparsa da dietro il cancello."


translate italian epilogue_mi_078abde8:


    sa "Ciao, Semyon-kun! Il mio nome è Slavya! Yoroshiku-ne!"


translate italian epilogue_mi_bf59de13:


    "Un silenzio attonito si è posato su set."


translate italian epilogue_mi_d7a685d0:


    "Sembrava che persino le cavallette avessero improvvisamente interrotto il loro concerto, per guardare Sasha."


translate italian epilogue_mi_916c7ee7:


    mt "Avrei una sola domanda da farti."


translate italian epilogue_mi_209e0706:


    "La voce terribilmente calma di Olga Dmitrievna ci ha raggiunto da dietro le mie spalle."


translate italian epilogue_mi_1c639c9a:


    mt "Hai almeno letto il copione?"


translate italian epilogue_mi_dfa4c8a5:


    sa "Certo che l'ho fatto, ma è così noioso!"


translate italian epilogue_mi_b521b673:


    "Sarebbe stato inutile piangere, implorare, andare in crisi isterica – non eravamo riusciti a trovare un'altra attrice qui."


translate italian epilogue_mi_15068e6a:


    mt "Ci rimangono solo tre settimane di tempo in questo campo, e tu…"


translate italian epilogue_mi_3895407a:


    "Si è seduta sulla sua sedia da regista e ha cominciato a usare il suo cappello panama come ventaglio."


translate italian epilogue_mi_e2203c1f:


    mt "Ragazzi, vi rendete conto che non ce la faremo mai a finire tutto se continuiamo a perdere tempo in questo modo! Abbiamo ancora un sacco di scene da girare! Potreste almeno essere un tantino più seri? Sembra che io sia l'unica a cui importa della cosa…"


translate italian epilogue_mi_1b8271ef:


    "Il bastone non avrebbe di certo funzionato con Sasha, quindi forse dovevamo provare con una carota."


translate italian epilogue_mi_7f682ff5:


    sa "Mi dispiace…{w} Ci proverò di nuovo."


translate italian epilogue_mi_2f8fa8d8:


    mt "Sarà meglio per te."


translate italian epilogue_mi_b09d6e1c:


    "Ho cercato di immaginarmi il protagonista che esplorava l'ambiente sconosciuto, con milioni di teorie che affollavano la sua testa. Lui è sconvolto, non sa cosa pensare di tutto questo, il suo cervello è pronto a esplodere, la sua coscienza comincia a vagare, e poi ecco qui – una ragazza, l'esempio perfetto del pioniere medio, appare davanti ai suoi occhi, e lui non ha la minima idea di cosa stia accadendo."


translate italian epilogue_mi_6e8305c4:


    "Lei si limita a salutare il protagonista, mandandolo dalla leader del Campo…"


translate italian epilogue_mi_04183b60:


    "Ho fatto un paio di passi in avanti, e Sasha è apparsa da dietro il cancello."


translate italian epilogue_mi_2d51c8a5:


    sa "Tu devi essere nuovo qui?"


translate italian epilogue_mi_e46c798b:


    my "…"


translate italian epilogue_mi_c4155f1c:


    "Ho visto con la coda dell'occhio la lente della macchina da presa puntata verso di me, e ho cercato di fare una faccia davvero sorpresa."


translate italian epilogue_mi_74e29a93:


    sa "In tal caso, è necessario che tu vada dalla nostra leader del Campo…"


translate italian epilogue_mi_794febdd:


    mt "Allora sei capace di farlo! Basta che tu lo voglia veramente!"


translate italian epilogue_mi_2424e856:


    "Olga Dmitrievna si è asciugata il sudore dalla fronte."


translate italian epilogue_mi_01ddb43c:


    mt "Mi farai venire i capelli grigi se andrai avanti con tutti i tuoi fronzoli!{w} E dobbiamo ancora fare tutto il montaggio… Hah…"


translate italian epilogue_mi_9b67587c:


    "Ha dato un'occhiata all'orologio."


translate italian epilogue_mi_76501b5e:


    mt "Ok, per oggi basta così."


translate italian epilogue_mi_8e0afbc8:


    "Difficilmente potrei definirmi un maniaco del lavoro, ma nonostante ciò mi sembrava che fosse ancora troppo presto per fermare le riprese, dato che eravamo già molto in ritardo rispetto alle previsioni."


translate italian epilogue_mi_31a1857e:


    my "Non pensa che…"


translate italian epilogue_mi_0826aeda:


    mt "Certo! Ma sono già sfinita!"


translate italian epilogue_mi_50a46ecb:


    my "Eh, come desidera…"


translate italian epilogue_mi_db0f23eb:


    "Ho alzato le spalle e mi sono diretto al Campo."


translate italian epilogue_mi_04fdda5e:


    "Si stava facendo sera…"


translate italian epilogue_mi_a20cefa7_1:


    "..."


translate italian epilogue_mi_5cbd3ea0:


    "Stasera ho cenato con Masha, Sasha e Alisa."


translate italian epilogue_mi_988b5808:


    "Davanti a me c'era un'enorme ciotola piena di patate fritte e funghi."


translate italian epilogue_mi_13434154:


    "Era uno dei vantaggi dell'estate – le verdure fresche dell'orto."


translate italian epilogue_mi_8aa3e2bb:


    ma "Forse faresti meglio a risparmiare il tuo impegno per le riprese."


translate italian epilogue_mi_ba41249e:


    my "Oh, forse ti piacciono di più gli spaghetti istantanei?"


translate italian epilogue_mi_36b02c46:


    dv "Di cosa stai parlando, Sasha è una grande chef!"


translate italian epilogue_mi_7a3a0d77:


    "Ha borbottato Alisa."


translate italian epilogue_mi_49910a9c:


    ma "Nessuno ti ha mai detto che è da maleducati parlare con la bocca piena?"


translate italian epilogue_mi_ccfd297e:


    dv "E tu non fai altro che lamentarti…"


translate italian epilogue_mi_1c59bfd0:


    sa "Io… Io…"


translate italian epilogue_mi_508e3ad2:


    "Sasha era sul punto di scoppiare in lacrime."


translate italian epilogue_mi_2b2f274d:


    ma "Oh Signore…{w} Dai, calmati! È tutto delizioso!"


translate italian epilogue_mi_1b24a1e0:


    sa "Nyaa! Masha-nyan, vuoi che faccia qualcosa…"


translate italian epilogue_mi_2d21996b:


    "Masha le ha lanciato uno sguardo così terribile che persino Sasha ha capito che sarebbe stato meglio non aggiungere altro."


translate italian epilogue_mi_df502e69:


    ma "Comunque, dimmi sinceramente…"


translate italian epilogue_mi_d9f1b919:


    my "Non te lo dico!"


translate italian epilogue_mi_4718ae9f:


    ma "Ma non sai nemmeno quello che volevo chiederti!"


translate italian epilogue_mi_2596d838:


    my "Lo so…"


translate italian epilogue_mi_6a9e9866:


    ma "Allora, cos'è che stavo per chiederti?"


translate italian epilogue_mi_7eb71829:


    "Mi ha fissato con sguardo trionfante."


translate italian epilogue_mi_11c965cb:


    my "Perché ho scritto una tale spazzatura di copione…"


translate italian epilogue_mi_6ceed182:


    ma "Diamine!"


translate italian epilogue_mi_5fcef207:


    "Alisa e Sasha hanno fatto una risata."


translate italian epilogue_mi_a13d417a:


    ma "Ah, vedo che a voi sta bene così!"


translate italian epilogue_mi_c5a0a029:


    dv "Qual è il problema? È estate, siamo circondati dalla natura, c'è una spiaggia qui vicino, è tutto grandioso!"


translate italian epilogue_mi_bb713d3d:


    ma "Avresti potuto visitare la Turchia, perché sei venuta qui?"


translate italian epilogue_mi_2dd77e41:


    "Alisa ha guardato Masha con aria assente, cominciando a scavare nel proprio piatto."


translate italian epilogue_mi_7d88023f:


    ma "Ebbene?"


translate italian epilogue_mi_d0742f96:


    "Ha puntato la sua forchetta verso di me."


translate italian epilogue_mi_4ac27a40:


    my "Non temere la forchetta, ma il cucchiaio, un sol colpo e il cranio è spacciato!"


translate italian epilogue_mi_2e15dd85:


    ma "Seriamente – vi sembra normale?"


translate italian epilogue_mi_1efc89c2:


    sa "Semyon-kun è kawaii!"


translate italian epilogue_mi_a16d8757:


    "Sasha si è avvinghiata al mio braccio e ha annuito vigorosamente."


translate italian epilogue_mi_3a95f27d:


    dv "Qual è il problema, comunque? Lui ci ha regalato una vacanza gratis."


translate italian epilogue_mi_1f119ac6:


    ma "Non so nemmeno cosa ci faccia io qui."


translate italian epilogue_mi_0c881678:


    my "Beh, non è che qualcuno ti stia costringendo a restare."


translate italian epilogue_mi_7b75257b:


    "Ho detto con freddezza."


translate italian epilogue_mi_0bbe7c29:


    ma "Oh, se solo la mia volontà contasse qualcosa…"


translate italian epilogue_mi_3d475d4b:


    my "E tu che cosa fai per tutto il tempo?{w} Non fai altro che importunare gli altri, nonostante manchi ancora molto alla tua prima apparizione in scena, secondo il copione. Ti stai preparando per il ruolo, eh?"


translate italian epilogue_mi_e5e97d5d:


    "L'ho interrotta."


translate italian epilogue_mi_ad5371b1:


    ma "Oh, forse mi sto preparando!"


translate italian epilogue_mi_a1263512:


    dv "Doppio standard!"


translate italian epilogue_mi_6c54bdc2:


    ma "Triplo, dannazione!"


translate italian epilogue_mi_ea3c71bd:


    my "Allora, che fai, in realtà?"


translate italian epilogue_mi_338067db:


    ma "Beh, che cosa ti aspetti che io faccia?"


translate italian epilogue_mi_84d72c1c:


    my "Io? Niente. Sono solo curioso."


translate italian epilogue_mi_45e76bef:


    ma "Leggo libri…"


translate italian epilogue_mi_ec13eb7f:


    "Ha risposto timidamente."


translate italian epilogue_mi_9a9c5850:


    my "Sicuramente sono libri per migliorare le tue doti di attrice..."


translate italian epilogue_mi_07729729:


    ma "Piantala, ok? Siamo seduti qui, Dio solo sa dove – niente internet, niente segnale telefonico. Niente di niente!{w} Se non avessi portato con me il repellente per zanzare…"


translate italian epilogue_mi_71495458:


    sa "Io l'ho portato, nya!"


translate italian epilogue_mi_8538763c:


    "Masha le ha lanciato uno sguardo tale che Sasha improvvisamente ha iniziato ad occupare molto meno spazio del solito."


translate italian epilogue_mi_b9cb210e:


    dv "Io proprio non ti capisco… Che senso ha lamentarsi per tutto il tempo…? Saresti potuta restare a casa…"


translate italian epilogue_mi_010cbc3b:


    ma "Ah, ho forse chiesto la tua opinione?"


translate italian epilogue_mi_caa0783d:


    "Masha si è alzata di scatto e si è diretta verso l'uscita."


translate italian epilogue_mi_5354f480:


    sa "Masha-nyan è arrabbiata…"


translate italian epilogue_mi_47d1149e:


    "Ha detto Sasha in tono angosciato."


translate italian epilogue_mi_a21ed268:


    dv "Eh, se è già «arrabbiata» a questo punto, allora aspetta di vedere come sarà alla fine delle riprese…"


translate italian epilogue_mi_f23678e8:


    my "Un «Non Aprite Quel Campo» con una «Druzhba», eh…?"


translate italian epilogue_mi_bced139f:


    "Curiosamente, non riuscivo davvero a capire perché Masha fosse così fuori di sé."


translate italian epilogue_mi_587f2ffc:


    "Certo, fin dall'inizio non aveva avuto alcun entusiasmo sia per la sceneggiatura che per l'idea di girare il nostro film in un campo di pionieri dimenticato da Dio."


translate italian epilogue_mi_c1ebda1e:


    "Ma comunque, solitamente non è così aggressiva."


translate italian epilogue_mi_0912ae25:


    "Forse è per colpa del caldo?{w} Oppure a causa delle cavallette?"


translate italian epilogue_mi_c7b9f0f9:


    "Ma certo! Le cavallette!{w} Ci stanno facendo patire le pene dell'inferno al posto di una bella dormita!"


translate italian epilogue_mi_11922441:


    "Vorrei solo avere un pulsante per poter rilasciare una sorta di gas velenoso – ed ecco fatto…"


translate italian epilogue_mi_bb476699:


    sa "A cosa stai pensando?"


translate italian epilogue_mi_5fa3b6bf:


    my "Alle cavallette!"


translate italian epilogue_mi_0c3815a2:


    dv "Cosa c'è che non va con esse?"


translate italian epilogue_mi_45f1bddf:


    my "Forse sarebbe stato meglio domandarsi «cosa c'è che va con esse»."


translate italian epilogue_mi_4c2e02b2:


    sa "Le cavallette – non sono nya."


translate italian epilogue_mi_5b4d3391:


    my "Perché?"


translate italian epilogue_mi_5ae0bd3f:


    sa "Beh, sono così… spaventose…"


translate italian epilogue_mi_7ea55766:


    my "Non ti piacciono gli insetti, in generale?"


translate italian epilogue_mi_5c8ae25c:


    sa "Affatto!"


translate italian epilogue_mi_a9e9f873:


    my "Neppure a me…"


translate italian epilogue_mi_eb6b2adb:


    "È piombato un pesante silenzio."


translate italian epilogue_mi_33c07f3e:


    "Indipendentemente da cosa avesse per la testa Masha, almeno lei era sempre in grado di mantenere viva una conversazione.{w} Anche se si trattava di cavallette."


translate italian epilogue_mi_b69d5469:


    "Me la sono immaginata mentre parlava di quelle creature infernali che osano non solo appestare questo mondo imperfetto già di suo, ma anche di continuare a far rumore per tutta la notte, interrompendo il nostro riposo interiore e il nostro equilibrio mentale…"


translate italian epilogue_mi_5f4cc6c3:


    "Olga Dmitrievna si è avvicinata al nostro tavolo."


translate italian epilogue_mi_c7675ebb:


    mt "Pronti per le riprese notturne?"


translate italian epilogue_mi_6ba4a203:


    my "Riprese notturne? Ma non abbiamo ancora completato le scene iniziali…{w} E ha detto che per oggi abbiamo finito…"


translate italian epilogue_mi_0dc170ff:


    mt "Dobbiamo sfruttare al massimo il tempo che ci è stato concesso! Altrimenti non ce la faremo mai!"


translate italian epilogue_mi_a996136e:


    "Non potevo di certo dire che ero già stanco morto, e inoltre, dovevo mostrare almeno un briciolo di impegno per il successo del mio progetto."


translate italian epilogue_mi_78cf53f5:


    my "D'accordo. Cosa dobbiamo girare?"


translate italian epilogue_mi_2a270e1d:


    mt "La scena notturna di Slavya nel bosco."


translate italian epilogue_mi_d88ec507:


    sa "Nani-nani?"


translate italian epilogue_mi_f18d55d7:


    "Sasha ha sentito che stavamo parlando del suo personaggio, ed è balzata verso di noi."


translate italian epilogue_mi_2fe8b72b:


    mt "Ma ti preeeego…"


translate italian epilogue_mi_d489d61c:


    "Ha iniziato Olga Dmitrievna con voce tesa."


translate italian epilogue_mi_41db4dd8:


    mt "Segui il copione questa volta!"


translate italian epilogue_mi_1897c7ff:


    sa "Wakarimashita!"


translate italian epilogue_mi_ed85db76:


    mt "E smettila di parlare così…"


translate italian epilogue_mi_ba5e3d3e:


    "Ha stretto i denti, si è voltata e se n'è andata."


translate italian epilogue_mi_f0d9fc84:


    dv "Di cosa parla la scena?"


translate italian epilogue_mi_a8c746ea:


    my "Non hai letto proprio niente del copione?"


translate italian epilogue_mi_7ac752e2:


    dv "Certo che no!"


translate italian epilogue_mi_1ac9e5f7:


    "Ha detto Alisa, con orgoglio."


translate italian epilogue_mi_cea1d5da:


    dv "Mi basta leggere solo le scene dove appaio io."


translate italian epilogue_mi_31409fb0:


    "Non avevo intenzione di farle un discorso insensato riguardo alla necessità dell'avere un quadro completo per comprendere appieno il proprio personaggio."


translate italian epilogue_mi_4c582a03:


    my "Guarderò Slavya danzare sulla riva del lago nella foresta…"


translate italian epilogue_mi_5f43ddfa:


    sa "Nya-nya! È così romantico!"


translate italian epilogue_mi_537ab6db:


    my "…E lei nel frattempo si spoglia!"


translate italian epilogue_mi_65c466a7:


    sa "Nya?"


translate italian epilogue_mi_03d57659:


    "Sasha mi ha fissato senza capire."


translate italian epilogue_mi_fd1e9efb:


    "Il significato delle mie parole ha albeggiato nella sua mente dopo alcuni istanti."


translate italian epilogue_mi_29afe1be:


    sa "S…spogliarmi?"


translate italian epilogue_mi_dea901ac:


    my "Rilassati, non intendo completamente."


translate italian epilogue_mi_009c29dd:


    sa "Beh, se sarai tu a vedermi…"


translate italian epilogue_mi_ac04f5d1:


    "È arrossita."


translate italian epilogue_mi_e51d66bf:


    "Certo, sembra che questa ragazza non solo avesse guardato troppi anime di recente, ma che non fosse nemmeno così intelligente dalla nascita…"


translate italian epilogue_mi_2e162410:


    "Ma perché Olga Dmitrievna ha dovuto assegnarci i ruoli in questo modo?"


translate italian epilogue_mi_9290596e:


    "Se fosse per me, avrei preferito assegnare il ruolo di Slavya a Masha, e quello di Miku a Sasha."


translate italian epilogue_mi_52b336ff:


    "In ogni caso spetta al regista decidere, e Olga Dmitrievna è l'unico regista che abbiamo."


translate italian epilogue_mi_8974ad73:


    dv "Per caso, succederà {i}quella cosa{/i}?"


translate italian epilogue_mi_2dcb646d:


    sa "Quale cosa?"


translate italian epilogue_mi_f0ac2d1e:


    "Sasha ha fissato Alisa con lo sguardo perso nel vuoto."


translate italian epilogue_mi_449f4ec4:


    my "No."


translate italian epilogue_mi_24bd3c14:


    dv "Cosa vuoi dire, niente di niente?"


translate italian epilogue_mi_604efd9d:


    my "Non in quella scena."


translate italian epilogue_mi_f0ac5acd:


    dv "Ah, quindi vuoi dire che {i}lo farete{/i} un po' più {i}avanti{/i}."


translate italian epilogue_mi_26c83201:


    "Alisa ha sfoggiato un sorriso tagliente."


translate italian epilogue_mi_c5d3868a:


    my "Se ti incuriosisce così tanto, perché non vai a leggerti il copione?"


translate italian epilogue_mi_6e91e7e9:


    dv "Nah, non mi incuriosisce per niente."


translate italian epilogue_mi_f445a737:


    "Alisa si è alzata dal tavolo."


translate italian epilogue_mi_e08e50aa:


    dv "Ok, io vado."


translate italian epilogue_mi_79ef6a3f:


    sa "Ahem, di cosa stavate parlando? Cos'era «quella cosa»?"


translate italian epilogue_mi_1e98cfca:


    my "Non è niente.{w} Vai a prepararti – il sole sta già tramontando."


translate italian epilogue_mi_91e87806:


    sa "Nya-nya!"


translate italian epilogue_mi_7cbc1a04:


    "Sasha si è alzata e si è precipitata verso l'uscita."


translate italian epilogue_mi_248dbf88:


    "Ho finito il mio piatto di patate e ho fatto un grande sospiro."


translate italian epilogue_mi_80428fb2:


    "Beh, di questo passo non riusciremo a girare nemmeno metà del nostro film."


translate italian epilogue_mi_398ea31a:


    "Credo che potremo mettere semplicemente un «La storia continua» nei titoli di coda."


translate italian epilogue_mi_25434a55:


    "E trasformare il tutto in una saga epica, creata dagli studenti di cinema e televisione di un'università di infima categoria."


translate italian epilogue_mi_b5b110eb:


    "Improvvisamente mi sono accorto che in realtà non me ne importava più niente…"


translate italian epilogue_mi_292fb47d:


    sh "Sei pronto?"


translate italian epilogue_mi_95ff9942:


    "Mi sono voltato e ho visto Shurik."


translate italian epilogue_mi_8b6b532c:


    my "Non è che io abbia molto per cui prepararmi…{w} Pensi di riuscire a gestire queste riprese notturne?"


translate italian epilogue_mi_02458859:


    sh "Oh… In qualche modo…"


translate italian epilogue_mi_5141016c:


    my "Va bene, cerchiamo di andare avanti allora."


translate italian epilogue_mi_a20cefa7_2:


    "..."


translate italian epilogue_mi_d4c057a8:


    "Finalmente l'aria è diventata più fresca la sera.{w} Anzi, forse anche troppo."


translate italian epilogue_mi_334bdb32:


    "Ero seduto su un ceppo e stavo cercando di scaldarmi nella mia camicia leggera, mentre gli altri stavano posizionando le telecamere e le luci."


translate italian epilogue_mi_c38f964d:


    ma "Perché mai dovrei partecipare a tutto questo? Il mio personaggio non è nemmeno in questa scena!"


translate italian epilogue_mi_8944594a:


    "Masha stava allestendo un tessuto riflettente tra gli alberi."


translate italian epilogue_mi_373f8138:


    mt "E chi altro? Siamo sempre a corto di personale!"


translate italian epilogue_mi_19ae045d:


    ma "Beh, allora avreste dovuto portare più persone! Tutti sarebbero stati sicuramente felici di partecipare alla produzione di un tale capolavoro."


translate italian epilogue_mi_b468297c:


    mt "Ti stai forse lamentando di qualcosa?"


translate italian epilogue_mi_f9ea9881:


    "Olga Dmitrievna le ha lanciato uno sguardo minaccioso."


translate italian epilogue_mi_73f6d6b1:


    ma "N… no, affatto, va tutto bene…"


translate italian epilogue_mi_ee161131:


    "Probabilmente la nostra regista era l'unica persona qui con la quale Masha non osava discutere."


translate italian epilogue_mi_711dadab:


    "Chissà perché?"


translate italian epilogue_mi_a20cefa7_3:


    "..."


translate italian epilogue_mi_4e70f7d1:


    "Shurik si è seduto accanto a me."


translate italian epilogue_mi_7cda594d:


    sh "Cosa ne pensi, abbiamo qualche possibilità di riuscire a girare la scena nel giro di due o tre riprese?"


translate italian epilogue_mi_a45d1642:


    my "Non ne ho idea… Non è che dipenda da me…"


translate italian epilogue_mi_61482050:


    "Ho fatto un cenno verso Sasha. Stava saltando energicamente su e giù in riva al lago in fase di riscaldamento per il suo ruolo."


translate italian epilogue_mi_fbf62cb1:


    sh "Oh, capisco…"


translate italian epilogue_mi_9687a380:


    "Shurik ha sospirato con rassegnazione e si è diretto verso la macchina da presa."


translate italian epilogue_mi_b1ce9623:


    mt "Bene! Iniziamo."


translate italian epilogue_mi_8b3f4a18:


    "Mi sono nascosto dietro a un albero."


translate italian epilogue_mi_00dabc53:


    mt "Camera! Azione!"


translate italian epilogue_mi_84b9f42b:


    "Sasha è passata davanti a me, quasi danzando."


translate italian epilogue_mi_f2d8c1c4:


    "Non ho potuto fare a meno di notare che nonostante tutti gli effetti di luci supplementari, combinati con il chiaro di luna, lei sembrasse semplicemente ridicola."


translate italian epilogue_mi_db64b3c4:


    "Ma quelli erano problemi del cameraman – forse è meglio così."


translate italian epilogue_mi_686c7490:


    "Sono uscito lentamente da dietro l'albero, e l'ho seguita."


translate italian epilogue_mi_a24c889e:


    "Sasha camminava lentamente lungo la riva, quasi sospesa da terra, togliendosi il foulard e sbottonandosi la camicia durante il tragitto."


translate italian epilogue_mi_dac31f5f:


    "In realtà, era davvero eccezionale."


translate italian epilogue_mi_7dd610b2:


    "Scommetto che potrebbe diventare una grande attrice del cinema muto."


translate italian epilogue_mi_efe25434:


    "Poco dopo, Sasha è scomparsa nella foresta. Ho preso qualche momento per distanziarmi dal litorale, e poi ho iniziato a seguirla."


translate italian epilogue_mi_d536fac8_1:


    mt "Stop!"


translate italian epilogue_mi_1b2c2d2e:


    "Sasha si è precipitata immediatamente fuori dalla foresta."


translate italian epilogue_mi_7e7cffad:


    sa "Allora? Allora? Com'è andata?"


translate italian epilogue_mi_78bad556:


    mt "Eccellente! Perfetto! Se solo potessimo fare tutto in questo modo – al primo tentativo! Allora riusciremmo a finire in tempo!"


translate italian epilogue_mi_9ae232e3:


    my "L'ha detto come se ormai non ce la potessimo fare in tempo."


translate italian epilogue_mi_e56d1312:


    "Ho fatto notare con scetticismo."


translate italian epilogue_mi_ef558f42:


    mt "Ce la faremo, ci puoi scommettere!"


translate italian epilogue_mi_cfa206cd:


    ma "Ahem, come sarebbe, tutto qui?"


translate italian epilogue_mi_fb33457e:


    "Masha è apparsa dal nulla all'improvviso."


translate italian epilogue_mi_72fd9d16:


    mt "Volevi qualcos'altro?"


translate italian epilogue_mi_d28ef18c:


    ma "Abbiamo passato tre ore a preparare la scena, e dopo un minuto è già finita?"


translate italian epilogue_mi_9e2ea357:


    mt "Questo è il cinema! Il cinema, sai! Arte! Potrebbero volerci anni per riuscire ad ottenere la scena perfetta! Si potrebbe continuare a girare un film di cinque minuti per decenni!"


translate italian epilogue_mi_e60cdb23:


    "Olga Dmitrievna era particolarmente buffa, agitando le braccia in quel modo."


translate italian epilogue_mi_40ef1a37:


    ma "Certo…"


translate italian epilogue_mi_11d5d9f7:


    "Ha borbottato Masha con un filo di voce..."


translate italian epilogue_mi_ab8a7c84:


    ma "Questo è tutto?"


translate italian epilogue_mi_438a5ff3:


    mt "Sì, per stasera è tutto."


translate italian epilogue_mi_2e22c834:


    ma "Allora buona notte a tutti!"


translate italian epilogue_mi_8227612c:


    "Ha gettato quel pezzo di tessuto riflettente per terra e si è affrettata verso il Campo."


translate italian epilogue_mi_7b48d8d6:


    "Ho borbottato in fretta i miei saluti a tutti, ho afferrato il copione e mi sono precipitato dietro a lei."


translate italian epilogue_mi_a20cefa7_4:


    "..."


translate italian epilogue_mi_333411d8:


    my "Ehi!"


translate italian epilogue_mi_58a251f7:


    "L'ho raggiunta quando ormai eravamo alla piazza."


translate italian epilogue_mi_17c3f085:


    ma "Oh, e adesso che c'è?"


translate italian epilogue_mi_f72fd8e2:


    "L'espressione di Masha indicava un'estrema stanchezza."


translate italian epilogue_mi_6e8fe88f:


    my "Senti… ti andrebbe di fare una breve passeggiata prima di andare a dormire?"


translate italian epilogue_mi_cc3d4e3e:


    ma "E perché dovrei?"


translate italian epilogue_mi_4de002b5:


    "Mi ha guardato, con incomprensione."


translate italian epilogue_mi_d67add20:


    my "Beh, è solo che…{w} È ancora così presto! Scommetto che non saresti mai andata a dormire a quest'ora in città."


translate italian epilogue_mi_accf297a:


    ma "In città…{w} Questa non è la città, sai. Qui tre giorni contano come uno."


translate italian epilogue_mi_36f15bc5:


    my "E dai! È ancora presto!"


translate italian epilogue_mi_fcbee1a6:


    ma "Oh, d'accordo…"


translate italian epilogue_mi_943eaf3a:


    "Mi ha guardato, sorridendo."


translate italian epilogue_mi_6d8953b3:


    ma "Basta che non duri troppo!"


translate italian epilogue_mi_443f7100:


    my "Signora! Sì signora!"


translate italian epilogue_mi_21bb5d23:


    ma "Perché te lo porti dietro per tutto il tempo?"


translate italian epilogue_mi_799227a4:


    "Ha indicato il pacco di fogli che tenevo tra le mani."


translate italian epilogue_mi_58f54863:


    my "Beh, non si sa mai cosa potrebbe succedere…"


translate italian epilogue_mi_a20cefa7_5:


    "..."


translate italian epilogue_mi_6299873c:


    "Poco più tardi siamo arrivati alla spiaggia."


translate italian epilogue_mi_ee06f3f9:


    ma "Che luna meravigliosa!"


translate italian epilogue_mi_41699bfb:


    "Masha ha iniziato a spogliarsi e si è precipitata verso il fiume."


translate italian epilogue_mi_9ee4fa9d:


    "Non so cosa mi aspettassi, perché aveva un bikini sotto a quella sua uniforme da pioniere."


translate italian epilogue_mi_ca443530:


    "Affascinato, l'ho fissata mentre dimenava le gambe in acqua, il che ha sollevato notevoli spruzzi tutt'attorno a lei."


translate italian epilogue_mi_14ea44c3:


    my "E così sei ancora capace di divertirti!"


translate italian epilogue_mi_91d35e95:


    "Le ho gridato."


translate italian epilogue_mi_e2efb894:


    "Mi ha guardato offesa, si è avvicinata e si è seduta accanto a me."


translate italian epilogue_mi_2a3057e6:


    ma "Sì, è come se tutti voi mi consideraste una vecchia megera, di recente."


translate italian epilogue_mi_afbc1bb1:


    my "Certo che no…{w} Ma sei ancora una gran piagnona."


translate italian epilogue_mi_64383958:


    ma "E ho le mie ragioni!"


translate italian epilogue_mi_7d565efc:


    my "Non direi… Non ne vedo alcuna in particolare."


translate italian epilogue_mi_c7de3691:


    "Masha è balzata in piedi e si è allontanata da me di parecchi passi."


translate italian epilogue_mi_ca00ddd3:


    my "Oh, e dai!"


translate italian epilogue_mi_ec961d69:


    ma "Non «edai-are»! È solo che... non puoi sempre essere un tale... un tale..."


translate italian epilogue_mi_7ddbab17:


    my "Una tale cosa?"


translate italian epilogue_mi_5dc90687:


    ma "Non ti accorgi mai di niente…"


translate italian epilogue_mi_67ff3dab:


    "Ha appoggiato la testa sulle mie ginocchia e ha aggiunto sottovoce:"


translate italian epilogue_mi_b9140652:


    ma "E non capisci mai niente…"


translate italian epilogue_mi_b4a3d493:


    my "Sì, quello sono io – insensibile, stupido e ottuso."


translate italian epilogue_mi_8961f09f:


    "Masha ha fatto una risata appena udibile."


translate italian epilogue_mi_d58a855b:


    ma "Già, esattamente…"


translate italian epilogue_mi_25675418:


    ma "Andiamo a nuotare?"


translate italian epilogue_mi_18eae243:


    my "Ma fa freddo!"


translate italian epilogue_mi_7e8feba7:


    ma "Non ci vorrà molto."


translate italian epilogue_mi_520e734c:


    my "Oh... ok."


translate italian epilogue_mi_a20cefa7_6:


    "..."


translate italian epilogue_mi_9bbf09a7:


    "L'acqua era sorprendentemente calda, quindi non c'era il rischio di congelare."


translate italian epilogue_mi_cb0a184a:


    "Mi sono seduto sull'asciugamano lasciato lì da qualcuno e ho inalato una grande boccata di leggera aria notturna."


translate italian epilogue_mi_38d4cee4:


    "Masha si è messa di fronte a me e ha appoggiato la testa sulle mie gambe."


translate italian epilogue_mi_404343d4:


    ma "Ti sto dando fastidio?"


translate italian epilogue_mi_6829da13:


    my "N… no, per niente…"


translate italian epilogue_mi_ab837517:


    ma "Mi sento un po' stanca."


translate italian epilogue_mi_a5dd8896:


    "Mi ha guardato dritto negli occhi, ma ho evitato il suo sguardo."


translate italian epilogue_mi_b362f783:


    ma "Perché non può essere tutto com'era prima?"


translate italian epilogue_mi_d70ebca4:


    my "E come?"


translate italian epilogue_mi_00d724da:


    ma "E dai…"


translate italian epilogue_mi_02c9d340:


    my "È stata una decisione congiunta, lo sai…"


translate italian epilogue_mi_ea890fe1:


    ma "Ma comunque…"


translate italian epilogue_mi_e9145191:


    "Ha stretto le sue braccia attorno alla mia vita, e mi sono sentito ancora più a disagio."


translate italian epilogue_mi_b97bb617:


    ma "Perché siamo venuti qui, in primo luogo?"


translate italian epilogue_mi_2baba03c:


    my "Per girare un film!"


translate italian epilogue_mi_bb9ef712:


    "Ho cercato di riderci su con una battuta."


translate italian epilogue_mi_04598414:


    ma "Se non fosse stato per te, non mi sarei mai iscritta a questa scuola…{w} Avremmo potuto già…"


translate italian epilogue_mi_040d056a:


    "Ha sospirato."


translate italian epilogue_mi_47224973:


    ma "Ma adesso…"


translate italian epilogue_mi_e953effd:


    my "Non credo che sia una buona idea scavare nel passato."


translate italian epilogue_mi_655cfe62:


    ma "Forse hai ragione. Ma ad essere sinceri, continuo comunque a pensare che non avremmo dovuto…"


translate italian epilogue_mi_1f24965d:


    my "Ma fino a prova contraria sei stata tu a volerlo!"


translate italian epilogue_mi_981566ae:


    ma "Non è vero!"


translate italian epilogue_mi_c2f0d527:


    "Ha detto con risentimento."


translate italian epilogue_mi_3788408c:


    my "Oh, certo! Allora forse sono stato io ad insistere, vero?!"


translate italian epilogue_mi_5686c565:


    ma "Sì, sei stato tu!"


translate italian epilogue_mi_27ebdb73:


    "Si è voltata, ma senza andarsene."


translate italian epilogue_mi_1221beae:


    my "Sei sempre stata così… Non ti piace questo… Non ti piace quello… Perché non puoi semplicemente considerare la vita in modo più semplice? Datti un'occhiata in giro, è un'estate bellissima! È una notte stupenda! Stiamo girando un film grandioso! In realtà, tutto è cosi stupendo…"


translate italian epilogue_mi_b4204e75:


    ma "E tu ci credi davvero?"


translate italian epilogue_mi_a5b0e9d3:


    "La sua era davvero una domanda degna di nota."


translate italian epilogue_mi_bfa1d965:


    my "Sì… Più o meno. Non è che si possa sempre avere tutto il meglio…{w} Ma cosa c'è di così male?"


translate italian epilogue_mi_538b1472:


    ma "Ogni cosa…"


translate italian epilogue_mi_1b164615:


    "Sembrava pronta a scoppiare in lacrime."


translate italian epilogue_mi_96380f1d:


    ma "Questo stupido campo… Quel tuo copione.{w} Tutto! Ogni cosa è sbagliata! E tu non sei più un bambino! Dovresti pensare a farti una famiglia, ad avere dei figli, a trovarti un posto di lavoro…"


translate italian epilogue_mi_56a282e0:


    my "Ehi!{w} Ne abbiamo già discusso un migliaio di volte!"


translate italian epilogue_mi_c7401555:


    ma "Già… E ci siamo lasciati proprio per quello."


translate italian epilogue_mi_04774682:


    my "È stata colpa tua, senti. Precisamente a causa del tuo atteggiamento in stile «tutto è sbagliato»."


translate italian epilogue_mi_0ab1e883:


    ma "Oh, il tuo invece è «tutto va alla grande», per compensare il mio!{w} Solo gli stolti gioiscono di ogni cosa che li circonda."


translate italian epilogue_mi_af9c4714:


    my "Non sto gioendo… O meglio, sto gioendo di cose per le quali vale la pena gioire! Mi piace il mio lavoro. Fare cinema mi da soddisfazione."


translate italian epilogue_mi_9c47bb7a:


    "Ne sono proprio convinto?"


translate italian epilogue_mi_bbfdf3e3:


    ma "Allora significa che non hai trovato piacere in me?"


translate italian epilogue_mi_39cd6f4b:


    "Si è alzata e si è appoggiata al mio petto."


translate italian epilogue_mi_995dd1da:


    my "Ahem, forse non dovremmo...?"


translate italian epilogue_mi_4b4f76bd:


    ma "Qual è il problema? Non vuoi?"


translate italian epilogue_mi_3f27c2c0:


    "Non sapevo proprio come risponderle."


translate italian epilogue_mi_dd178882:


    "Da un lato, non volevo riaprire vecchie ferite, ma dall'altro – non ero certamente nella posizione giusta per rifiutare."


translate italian epilogue_mi_113ceef8:


    "E non mi importerebbe se qualcuno ci vedesse in questo istante…"


translate italian epilogue_mi_32d9bac4:


    my "Ma perché? A cosa ci porterà?"


translate italian epilogue_mi_64fd49e3:


    ma "Che importa…"


translate italian epilogue_mi_9eb4aadf:


    my "Beh… In tal caso…"


translate italian epilogue_mi_a20cefa7_7:


    "..."


translate italian epilogue_mi_f1dcbafd:


    "Il chiaro di luna che si rifletteva nelle goccioline d'acqua sulla sua pelle nuda, la sabbia appiccicosa, la frescura della notte d'estate e il calore del suo corpo, il respiro pesante e i rantoli di piacere – tutto è finito dopo pochi istanti…"


translate italian epilogue_mi_a20cefa7_8:


    "..."


translate italian epilogue_mi_71390bb9:


    my "Suppongo che sia il momento di dormire.{w} Domani sarà una dura giornata – Olga Dmitrievna ci spremerà al massimo."


translate italian epilogue_mi_d5b285ba:


    "Mi stavo abbottonando lentamente la camicia mentre guardavo Masha vestirsi."


translate italian epilogue_mi_d1464071:


    ma "Non voglio dormire da sola…"


translate italian epilogue_mi_a68bf534:


    "Ha mormorato."


translate italian epilogue_mi_337290ce:


    my "Che c'è, hai paura delle cavallette?"


translate italian epilogue_mi_4562c02a:


    ma "Non sono cavallette, sono grilli! E non ho paura di loro…"


translate italian epilogue_mi_42869b08:


    "Ha distolto lo sguardo ed è arrossita un po'."


translate italian epilogue_mi_7ee55b20:


    ma "È solo che… beh… è insolito."


translate italian epilogue_mi_f7e0940e:


    "Dopo tutto questo tempo dovresti esserti già abituata."


translate italian epilogue_mi_b1cabb3f:


    "Improvvisamente ho pensato che non c'era niente di sbagliato in ciò.{w} Soprattutto dopo tutto quello che è successo…"


translate italian epilogue_mi_e9e6e7f9:


    my "Va bene…"


translate italian epilogue_mi_97c79ba2:


    ma "Ma dove?"


translate italian epilogue_mi_19aed9c9:


    "Masha mi ha guardato con aria interrogativa."


translate italian epilogue_mi_c405517a:


    my "Ci sono un paio di casette libere, riservate per altre scene.{w} Beh, suppongo che possiamo sfruttarle per un po'."


translate italian epilogue_mi_7dc61d1e:


    ma "Andiamo."


translate italian epilogue_mi_ab3f1fe7:


    "Ha sorriso, si è alzata, mi ha preso per il braccio, e lentamente ci siamo diretti verso la piazza…"


translate italian epilogue_mi_a20cefa7_9:


    "..."


translate italian epilogue_mi_6c0b31c2:


    "Un abbagliante spiraglio di luce mattutina ha colpito i miei occhi."


translate italian epilogue_mi_cbb4c74c:


    "Non potevo stirarmi – Masha stava dormendo sul mio braccio sinistro."


translate italian epilogue_mi_050d92e1:


    "Sono riuscito a malapena a sfilare il mio arto da sotto la sua testa, e l'ho massaggiato a lungo, cercando di ripristinare il flusso di sangue."


translate italian epilogue_mi_d84db049:


    "Grattandomi la barba ispida, mi sono chinato verso i miei pantaloncini per prendere le sigarette."


translate italian epilogue_mi_37be2fed:


    "Forse non avremmo dovuto spingerci così a fondo ieri sera."


translate italian epilogue_mi_15aaeab9:


    "Adesso non mi lascerà più in pace."


translate italian epilogue_mi_583a04fd:


    "Beh, forse no…{w} Non fa parte della sua natura."


translate italian epilogue_mi_412be39c:


    "Ma un cattivo retrogusto rimarrà comunque."


translate italian epilogue_mi_66fafbd5:


    "E adesso lei ha qualcosa da rimproverarmi, se solo le darò la giusta occasione per farlo."


translate italian epilogue_mi_7502f7b0:


    "Ancora {i}un'ulteriore{/i} discussione – non è che fosse a corto di esse."


translate italian epilogue_mi_d81d21a1:


    "Non riuscendo a trovare le sigarette nei miei pantaloncini, ho pensato che dovevo averle lasciate nella mia stanza, così mi sono vestito maldestramente e sono uscito."


translate italian epilogue_mi_a46f5080:


    "O Signore, fa' che piova un po', ne abbiamo proprio bisogno – è impossibile lavorare in un tale forno."


translate italian epilogue_mi_ee1382fb:


    "Ed è un continuo, giorno e notte, giorno e notte…"


translate italian epilogue_mi_8aef407b:


    "Strofinandomi gli occhi, ho arrancato lungo il sentiero e non ho notato che qualcuno si stava avvicinando, non finché mi è venuto addosso."


translate italian epilogue_mi_43c329b2:


    "Era Router."


translate italian epilogue_mi_14d3fbf7:


    ro "Ciao Semyon!"


translate italian epilogue_mi_c54301b0:


    my "Ciao!"


translate italian epilogue_mi_c942874a:


    "Gli ho gridato."


translate italian epilogue_mi_95f95256:


    "Infine sono arrivato alla casetta, ho aperto la porta e sono entrato."


translate italian epilogue_mi_9fc335aa:


    "Olga Dmitrievna era nella stanza.{w} Mi ha perforato con il suo sguardo."


translate italian epilogue_mi_d8b1d698:


    mt "Ti andrebbe di spiegarmi?"


translate italian epilogue_mi_cabd0ae8:


    my "Spiegare cosa?"


translate italian epilogue_mi_cd667d28:


    mt "Dove hai trascorso tutta la notte?!"


translate italian epilogue_mi_a30d42e4:


    "Sembrava più che minacciosa."


translate italian epilogue_mi_71fcb370:


    my "Ero con Masha... Qual è il problema?"


translate italian epilogue_mi_2e8f7dad:


    "Dopotutto, l'avrebbe capito chiunque, quindi non aveva senso nascondere la cosa."


translate italian epilogue_mi_3d8b8a51:


    mt "Con chi?"


translate italian epilogue_mi_af66aeeb:


    "Olga Dmitrievna mi ha guardato sorpresa."


translate italian epilogue_mi_3b56ad7d:


    my "Con Masha."


translate italian epilogue_mi_d18d27a7:


    "Ho risposto tranquillamente."


translate italian epilogue_mi_7fb436a3:


    mt "Non abbiamo nessuna Masha qui."


translate italian epilogue_mi_a55c62b2:


    my "Ora tocca a me essere sorpreso…"


translate italian epilogue_mi_c04c6271:


    mt "Semyon, il tuo è un comportamento che non si addice ad un vero pioniere!"


translate italian epilogue_mi_22f7322a:


    my "Ah, stiamo già girando? Ma dove sono le telecamere?"


translate italian epilogue_mi_fbd62a8b:


    "Ho sorriso."


translate italian epilogue_mi_105fc865:


    mt "Semyon!"


translate italian epilogue_mi_2151a69f:


    my "Oh, e dai…"


translate italian epilogue_mi_fe1c1f1f:


    "Mi sono diretto verso il mio letto e ho aperto il cassetto."


translate italian epilogue_mi_47e0b7e7:


    "Tuttavia, il mio zaino non era nella stanza."


translate italian epilogue_mi_3321c1b4:


    my "Ehm... Ha per caso visto il mio zaino?"


translate italian epilogue_mi_5c5b691a:


    mt "Uno zaino?"


translate italian epilogue_mi_efd76d15:


    "Mi ha guardato, sorpresa."


translate italian epilogue_mi_67d1a8d9:


    my "Sì. Era proprio qui."


translate italian epilogue_mi_e6ca34e7:


    mt "Ma sei arrivato qui senza alcun bagaglio…"


translate italian epilogue_mi_b9e79bf2:


    my "Che cosa?"


translate italian epilogue_mi_ff99c828:


    "Ho fatto uno sciocco sorriso."


translate italian epilogue_mi_74a4c096:


    mt "E non ho ancora finito con te!"


translate italian epilogue_mi_221aaa4e:


    my "Olga Dmitrievna, capisco che lei possa essere di buonumore stamane e che lei abbia voglia di scherzare, ma…"


translate italian epilogue_mi_f8da58c6:


    mt "Come posso essere di buonumore quando un pioniere, un futuro membro del Komsomol, si comporta così?!"


translate italian epilogue_mi_5a64d54b:


    "Un pioniere? Un membro del Komsomol? C'è decisamente qualcosa che non va."


translate italian epilogue_mi_118c25a9:


    my "Olga Dmitrievna, ha dormito bene? Tutto ok?"


translate italian epilogue_mi_06543492:


    mt "Come ti permetti di parlarmi in quel modo?!"


translate italian epilogue_mi_6a4d0b42:


    my "Ahem, qual è il suo problema?"


translate italian epilogue_mi_eae86bdc:


    mt "Per farla breve, voglio vederti all'allineamento entro mezz'ora! E in quanto a me, devo ancora riuscire a trovare Miku."


translate italian epilogue_mi_ad54eb5e:


    "Miku?{w} Sembra proprio che abbiano voglia di prenderci in giro."


translate italian epilogue_mi_443f7100_1:


    my "Signora! Sissignora!"


translate italian epilogue_mi_987f165b:


    "L'ho salutata e ho marciato al di fuori della stanza."


translate italian epilogue_mi_287b8014:


    "Devo dirlo a Masha – così ci faremo quattro risate."


translate italian epilogue_mi_bbbfbbde:


    "Ho spinto con forza la porta della casetta e sono entrato."


translate italian epilogue_mi_b191b3f3:


    my "Masha, non crederai a quello che…"


translate italian epilogue_mi_7225eb18:


    "Masha stava seduta in un angolo sopra a un letto, con il viso completamente pallido, tenendosi le gambe strette tra le braccia."


translate italian epilogue_mi_850753d4:


    my "Che è successo?"


translate italian epilogue_mi_ab6c8876:


    "Sono balzato accanto a lei."


translate italian epilogue_mi_36e80422:


    ma "Lena è appena entrata…"


translate italian epilogue_mi_8143f26a:


    my "Lena?{w} Ma sarebbe dovuta arrivare qui tra tre giorni…"


translate italian epilogue_mi_a1cd396c:


    ma "Non era la nostra Lena… {i}Quella{/i}…{w} Era solo qualcosa che somigliava a lei…"


translate italian epilogue_mi_6e1e7593:


    "Masha stava tremando, un'espressione di orrore era stampata sul suo volto."


translate italian epilogue_mi_c0db5536:


    my "Calmati! Pensiamoci un attimo!"


translate italian epilogue_mi_adfebabf:


    ma "Stava parlando di una specie di allineamento, di fare le pulizie, della biblioteca, del circolo musicale…"


translate italian epilogue_mi_4342489d:


    "Ha iniziato a respirare affannosamente."


translate italian epilogue_mi_e50ade73:


    ma "E… e…{w} Non si trattava di uno scherzo! Ne sono sicura!"


translate italian epilogue_mi_eab4b893:


    "Mi sono ricordato della mia conversazione di poco fa con Olga Dmitrievna."


translate italian epilogue_mi_8877ce04:


    my "Ok, calmati! Non ci capisco niente!"


translate italian epilogue_mi_dd83c236:


    ma "Non era la nostra Lena… Era…{w} La Lena del nostro copione!"


translate italian epilogue_mi_84cf572d:


    "Ha alzato i suoi occhi intrisi di lacrime verso di me, e ho capito subito che questo non era affatto uno scherzo, e tutto il mio coraggio è svanito in un istante."


translate italian epilogue_mi_a944f157:


    my "Ma è ridicolo! Non può essere…"


translate italian epilogue_mi_ccbb0e0e:


    "Anche se, ricordando la mia conversazione con la regista…"


translate italian epilogue_mi_758f8c33:


    ma "No! No! Ne sono sicura!"


translate italian epilogue_mi_dfc67743:


    "Si è gettata tra le mie braccia."


translate italian epilogue_mi_ba41a0fe:


    "Ho provato a calmarla un po'."


translate italian epilogue_mi_2ea136fe:


    "Dobbiamo pensare in modo logico."


translate italian epilogue_mi_020471bd:


    my "Dobbiamo pensare in modo logico! Stanno solo cercando di farci uno scherzo, questo è tutto! Stavo parlando con Olga Dmitrievna, e lei…"


translate italian epilogue_mi_9465eed8:


    "E lei cosa? Anche lei mi è sembrata strana?"


translate italian epilogue_mi_f5c1c55b:


    "Nah, non posso dirlo a Masha…"


translate italian epilogue_mi_6d353af2:


    my "Alla fine, si faranno la loro bella risata e la finiranno."


translate italian epilogue_mi_fd57f767:


    ma "No, dev'essere successo qualcosa… Qualcosa di terribile…{w} Ne sono sicura…"


translate italian epilogue_mi_8974dd9f:


    "Masha stava singhiozzando tra le mie braccia."


translate italian epilogue_mi_40789c4e:


    my "Ok, veniamo al dunque. Parliamo con qualcuno.{w} Ad esempio, con Router – che di certo non è il burlone del villaggio."


translate italian epilogue_mi_95c191c3:


    "A dire il vero, a questo punto ero decisamente interessato a scoprire di che tipo di scherzo di trattasse, uno scherzo che è riuscito a portare Masha alla disperazione, lei che è sempre così calma e preparata ad affrontare ogni situazione."


translate italian epilogue_mi_40ef1a37_1:


    ma "Va bene…"


translate italian epilogue_mi_78e86177:


    "Si è asciugata le lacrime e si è alzata dal letto."


translate italian epilogue_mi_40e78541:


    my "Ti sei ripresa un po' adesso?"


translate italian epilogue_mi_0d0c986a:


    ma "Ci sto provando…"


translate italian epilogue_mi_f9eeb73d:


    "Masha ha sorriso debolmente."


translate italian epilogue_mi_cfd5f29b:


    ma "Dai, andiamo…"


translate italian epilogue_mi_279dbfa1:


    "Abbiamo abbandonato la stanza."


translate italian epilogue_mi_04e16600:


    ma "Adesso dove si va?"


translate italian epilogue_mi_9164b34c:


    my "Olga Dmitrievna ha menzionato un qualche allineamento. Andiamo alla piazza – molto probabilmente saranno lì."


translate italian epilogue_mi_499411c9:


    "Masha mi ha preso per mano, ma l'ha lasciata andare quasi subito."


translate italian epilogue_mi_00660cbc:


    "Ho deciso di non guardarla, perché sapevo già quale espressione avesse in volto, così ci siamo diretti verso la piazza."


translate italian epilogue_mi_a20cefa7_10:


    "..."


translate italian epilogue_mi_20361ed5:


    "L'allineamento (se questo è il termine giusto per indicare le persone radunate attorno al monumento) era piuttosto affollato."


translate italian epilogue_mi_eb1f0748:


    "Non ho riconosciuto neppure la metà delle persone lì radunate, e questo fatto era già strano di per sé."


translate italian epilogue_mi_e02d4ff6:


    "Olga Dmitrievna stava cercando di metterli tutti in fila."


translate italian epilogue_mi_c44b27a4:


    "Sono andato da lei senza esitazione."


translate italian epilogue_mi_1ddcb8c6:


    my "Olga Dmitrievna, sa…"


translate italian epilogue_mi_9c242b70:


    mt "Oh, e c'è anche Miku!"


translate italian epilogue_mi_0d425fdb:


    "Mi ha interrotto."


translate italian epilogue_mi_0f83334b:


    mt "Eccellente! Ne parleremo dopo l'allineamento."


translate italian epilogue_mi_809254d9:


    my "Di che allineamento sta parlando…{w} Non crede di aver scherzato abbastanza? Per l'amor di Dio, lo scherzo è bello finché dura poco!"


translate italian epilogue_mi_4de002b5_1:


    "Mi ha guardato confusa."


translate italian epilogue_mi_a82eeb20:


    mt "Ma di cosa stai parlando?"


translate italian epilogue_mi_be2c12c6:


    my "Di tutto questo!"


translate italian epilogue_mi_5427ae40:


    "Masha ha guardato timidamente da dietro le mie spalle."


translate italian epilogue_mi_7609eb24:


    ma "Olga Dmitrievna, sul serio, non è più così divertente…"


translate italian epilogue_mi_b8ebaa25:


    mt "Proprio non vi capisco…"


translate italian epilogue_mi_47e141b6:


    my "E dai, siamo seri, torniamo alle riprese!"


translate italian epilogue_mi_deb210f9:


    mt "Quali riprese?"


translate italian epilogue_mi_ea1863fe:


    "L'ho guardata attentamente, e per la prima volta mi sono davvero reso conto che forse non stava scherzando."


translate italian epilogue_mi_e369f105:


    "Olga Dmitrievna non è mai stata un'attrice decente, e quindi dubito fortemente che quella sua espressione stupefatta non sia vera…"


translate italian epilogue_mi_f385c640:


    "Che casino…"


translate italian epilogue_mi_55374b08:


    my "Ok, supponiamo che…{w} Ci racconti la sua versione."


translate italian epilogue_mi_717e5682:


    mt "La versione di che cosa?"


translate italian epilogue_mi_0da1d3c7:


    "Stava ancora sbattendo le palpebre fissando la mia bocca."


translate italian epilogue_mi_f33afa6e:


    my "Ho capito."


translate italian epilogue_mi_1af119c3:


    "Ho afferrato Masha per il braccio e l'ho trascinata con me lontano dalla piazza."


translate italian epilogue_mi_3dccb76f:


    ma "Ehi, aspetta! Dove stiamo andando?"


translate italian epilogue_mi_e5c4f40a:


    my "A cercare qualcuno di meno folle."


translate italian epilogue_mi_4742eb97:


    "Le grida di Olga Dmitrievna ci hanno seguito, ma le ho semplicemente ignorate."


translate italian epilogue_mi_a20cefa7_11:


    "..."


translate italian epilogue_mi_c718bb72:


    "Ben presto siamo arrivati alla sede dei circoli."


translate italian epilogue_mi_03304259:


    "Secondo il copione, dovrebbe trovarsi proprio qui."


translate italian epilogue_mi_448b06ba:


    my "Andiamo."


translate italian epilogue_mi_f81758d9:


    "Masha non ha risposto, si è limitata a seguirmi in silenzio."


translate italian epilogue_mi_bd1a2f8e:


    "Ho spalancato la porta bruscamente e siamo entrati."


translate italian epilogue_mi_d2107504:


    "Router era proprio lì, come mi aspettavo."


translate italian epilogue_mi_7feccd3d:


    my "Che sta succedendo?"


translate italian epilogue_mi_dc50fc49:


    ro "Oh, Semyon! E sei in compagnia! Ciao! Sei venuto qui per iscriverti al nostro circolo?"


translate italian epilogue_mi_b9c61f4c:


    "Sembrava che ci fosse qualcosa di sbagliato anche in lui."


translate italian epilogue_mi_c2ed7553:


    my "Perché non sei all'allineamento?"


translate italian epilogue_mi_81684e59:


    "Ho chiesto con sarcasmo."


translate italian epilogue_mi_e3b581e3:


    ro "Ehm…{w} Beh vedi, devo finire una cosa qui."


translate italian epilogue_mi_db19aca1:


    my "Ah, non importa, dicci solo che sta succedendo."


translate italian epilogue_mi_a9ea475f:


    ro "Mi sono perso qualcosa?"


translate italian epilogue_mi_bb291d76:


    "Proprio come aveva fatto Olga Dmitrievna, mi ha fissato senza alcun segno di comprensione."


translate italian epilogue_mi_3286aba0:


    my "Perché diavolo tutti si stanno comportando come se fossimo dei veri pionieri in un vero campo di pionieri?"


translate italian epilogue_mi_50edcb0a:


    ro "Perché, non è forse così?"


translate italian epilogue_mi_08a831fd:


    my "Perché, lo è?"


translate italian epilogue_mi_95e46bc6:


    ro "Beh, in realtà…"


translate italian epilogue_mi_3a9f1644:


    my "E che mi dici del film?"


translate italian epilogue_mi_5fe1cb2a:


    ro "Quale film?"


translate italian epilogue_mi_48f2e671:


    my "Il film… Quello che stavamo girando!"


translate italian epilogue_mi_71aef04d:


    ro "Semyon, non riesco proprio a capire di cosa tu stia parlando…"


translate italian epilogue_mi_114b3078:


    "Solo ora mi sono accorto che tutti mi stavano chiamando Semyon…"


translate italian epilogue_mi_3af97c5c:


    "Beh, a dire il vero anche Sasha mi chiamava sempre «Semyon-kun»…"


translate italian epilogue_mi_2bdf9616:


    "Ma il suo era chiaramente un modo scherzoso, mentre costoro…"


translate italian epilogue_mi_a28e10ab:


    "Non è che fosse molto rilevante, ma il mio nome non è di certo Semyon!"


translate italian epilogue_mi_fc0ed84b:


    "Ho indietreggiato con orrore e mi sono scontrato con Masha."


translate italian epilogue_mi_4a4eea7d:


    "Ha emesso un breve grido."


translate italian epilogue_mi_179c9e45:


    ro "E tu, Miku…"


translate italian epilogue_mi_1a34a91c:


    "Non stavo più ascoltando…"


translate italian epilogue_mi_2264c5bc:


    "Ho afferrato Masha per mano e siamo corsi fuori."


translate italian epilogue_mi_65d0e61a:


    "Sono riuscito a fermarmi solo alla fermata dell'autobus."


translate italian epilogue_mi_90269e81:


    "E dopo essermi voltato ho visto Masha che cercava di riprendere fiato."


translate italian epilogue_mi_e8deb2c6:


    "L'ho lasciata andare e mi sono seduto sul ciglio della strada."


translate italian epilogue_mi_2a47862d:


    my "Sembra che siamo nei guai…"


translate italian epilogue_mi_9ddc03d3:


    ma "..."


translate italian epilogue_mi_f91617dc:


    my "Non capisci? Tutti qui si comportano esattamente com'è scritto nel mio copione!"


translate italian epilogue_mi_e0b753af:


    "Mi ha fissato con sguardo assente per un po', e poi mi ha detto:"


translate italian epilogue_mi_50ba87dc:


    ma "Già, pare proprio di sì…{w} Ma cosa… Perché?"


translate italian epilogue_mi_44093b8e:


    my "E che ne so…"


translate italian epilogue_mi_8389024e:


    ma "Allora, cosa facciamo?"


translate italian epilogue_mi_326ad2f9:


    my "Non lo so…"


translate italian epilogue_mi_a35f3a53:


    "Masha era sul punto di scoppiare in lacrime."


translate italian epilogue_mi_d4708852:


    my "Oh, smettila! Non ne ho proprio idea! Come potrei sfornare una soluzione così su due piedi, in una situazione del genere?"


translate italian epilogue_mi_01180556:


    ma "Ti capisco, ma…"


translate italian epilogue_mi_f59e1b10:


    my "E comunque, c'è ancora una possibilità che tutto questo sia solo uno stupido scherzo…{w} Beh, uno decisamente esagerato."


translate italian epilogue_mi_63ef8b13:


    ma "E se invece…?"


translate italian epilogue_mi_10030a3d:


    my "Beh, se non lo fosse… Allora agiremo in base alle circostanze."


translate italian epilogue_mi_3e730633:


    "Ho sospirato e mi sono rimesso in piedi."


translate italian epilogue_mi_6f413eb8:


    my "Almeno andiamo a mangiare qualcosa.{w} Credo che nonostante tutto ciò che sta accadendo qui, dev'esserci comunque qualcosa da mangiare."


translate italian epilogue_mi_ee84d14d:


    "Masha ha fatto un sorriso appena percettibile e mi ha seguito."


translate italian epilogue_mi_a20cefa7_12:


    "..."


translate italian epilogue_mi_4cb70312:


    "La mensa era stracolma."


translate italian epilogue_mi_08c3283d:


    "Già all'allineamento avevo notato che c'era molta più gente di quanta ce n'era ieri."


translate italian epilogue_mi_e35a16d4:


    "Perché tutti questi bambini sono venuti qui? In un campo abbandonato?"


translate italian epilogue_mi_039e411f:


    "No, c'è qualcosa di terribilmente sbagliato…"


translate italian epilogue_mi_c99f77e3:


    "Tuttavia, il cibo veniva servito, così io e Masha abbiamo preso i vassoi e ci siamo seduti lontano dagli altri."


translate italian epilogue_mi_fb2c31f0:


    "Ho mangiato intensamente, cercando di non alzare la testa dal mio piatto e di non guardare le persone che ci circondavano."


translate italian epilogue_mi_67b9e789:


    "Comunque non è stato di grande aiuto – dopo un paio di minuti qualcuno si è presentato al nostro tavolo."


translate italian epilogue_mi_02f457e1:


    "Sono riuscito a malapena ad alzare lo sguardo, e ho visto Lena."


translate italian epilogue_mi_0ece5a44:


    un "Posso...?"


translate italian epilogue_mi_a86cd0ce:


    "Ha chiesto timidamente."


translate italian epilogue_mi_29741375:


    ma "Certo, fai pure!"


translate italian epilogue_mi_790d5a8d:


    "Mentre pensavo a come avrei dovuto risponderle, Masha ha tirato fuori una sedia e con un cenno ha invitato Lena a sedersi."


translate italian epilogue_mi_dac35953:


    un "Non eri te stessa stamattina..."


translate italian epilogue_mi_94cc4744:


    "Ha detto, allarmata."


translate italian epilogue_mi_684d739f:


    ma "Beh… Mi sono alzata dal lato sbagliato del letto…"


translate italian epilogue_mi_f8d7c8a1:


    "Masha ha riso nervosamente."


translate italian epilogue_mi_6c544c99:


    "Lena mi ha lanciato un'occhiata e poi ha distolto rapidamente lo sguardo."


translate italian epilogue_mi_7bc737ed:


    my "Allora, come va?"


translate italian epilogue_mi_c3c1a949:


    "Ho esordito con le domande più generali."


translate italian epilogue_mi_b993fafe:


    un "Bene."


translate italian epilogue_mi_52d92c19:


    my "Ah…"


translate italian epilogue_mi_26eb2fcd:


    "Sinceramente non avevo idea di cosa discutere con lei."


translate italian epilogue_mi_30741e46:


    "Inoltre, Lena sembrava piuttosto strana – mentre solitamente era sempre allegra e di buonumore, adesso sembrava molto timida e schiva."


translate italian epilogue_mi_827f779d:


    un "Avete notizie di Slavya?"


translate italian epilogue_mi_7abdce27:


    my "Perché, cosa è successo?"


translate italian epilogue_mi_0c42474d:


    un "Beh, nessuno l'ha vista da ieri…{w} È come se all'inizio voi due foste scomparsi… E adesso siete qui… Ma Slavya è…"


translate italian epilogue_mi_d0cb10a2:


    "Ha fatto delle lunghe pause tra una frase e l'altra."


translate italian epilogue_mi_f9611d76:


    "Ancora non ho capito – o si tratta di una recitazione impeccabile, oppure quella seduta davanti a noi non è la nostra Lena."


translate italian epilogue_mi_449f4ec4_1:


    my "No."


translate italian epilogue_mi_81d2b836:


    ma "Non l'abbiamo vista."


translate italian epilogue_mi_69a8f246:


    un "Beh, spero che riesca a tornare, in qualche modo."


translate italian epilogue_mi_9b627c74:


    my "Sì, lo speriamo anche noi."


translate italian epilogue_mi_6d56d6ac:


    "Slavya…{w} Sasha era decisamente in grado di perdersi in pieno giorno – non sarebbe di certo una cosa sorprendente."


translate italian epilogue_mi_531f4fb8:


    "Stavo finendo di mangiare la mia gommosa farina d'avena, quando improvvisamente Olga Dmitrievna è apparsa nella mensa, agitando le braccia."


translate italian epilogue_mi_3d70faed:


    "Si è guardata intorno senza emozione, e dopo avermi lanciato uno sguardo si è incamminata verso il nostro tavolo, a passo svelto."


translate italian epilogue_mi_0bab5229:


    mt "Oh, eccovi qui! Dobbiamo parlare!"


translate italian epilogue_mi_762bfea5:


    "Ha sibilato sottovoce proprio davanti a me."


translate italian epilogue_mi_af771b64:


    "Non valeva la pena opporsi, così mi sono alzato, ho fatto un cenno a Masha, e abbiamo seguito Olga Dmitrievna."


translate italian epilogue_mi_45d3dfab:


    "Dopo essere usciti, si è voltata di scatto e ci ha chiesto:"


translate italian epilogue_mi_ec4d970a:


    mt "Allora, dove siete stati ieri sera?"


translate italian epilogue_mi_97132f75:


    my "Perché dovrebbe interessarle…?"


translate italian epilogue_mi_8e0229dd:


    "Ho alzato le spalle."


translate italian epilogue_mi_349b0aba:


    mt "Dovrebbe! Sapete che Slavya è scomparsa?"


translate italian epilogue_mi_4f716193:


    my "L'abbiamo saputo da Lena."


translate italian epilogue_mi_e6a0d896:


    mt "Esattamente! Sono andata a cercarla e…{w} L'ho trovata!"


translate italian epilogue_mi_b34ccd5d:


    "Sembrava che stesse cercando disperatamente di trattenere le lacrime."


translate italian epilogue_mi_16fc9de2:


    my "Beh, è una buona notizia."


translate italian epilogue_mi_9175e7a8:


    mt "Venite, vi faccio vedere."


translate italian epilogue_mi_001b9dd0:


    "Ci ha detto a bassa voce, e si è diretta verso la foresta."


translate italian epilogue_mi_01ce7e3e:


    "Ho guardato Masha."


translate italian epilogue_mi_e0d7e61d:


    ma "Forse non dovremmo?"


translate italian epilogue_mi_13269186:


    my "Che c'è di male?"


translate italian epilogue_mi_ab7331fb:


    "Tuttavia, la mia voce interiore era d'accordo con lei."


translate italian epilogue_mi_a14a2f57:


    ma "Va bene allora… Ma io ho paura."


translate italian epilogue_mi_7851bc23:


    my "Andrà tutto bene…"


translate italian epilogue_mi_bef6e9f9:


    "Ho cercato di sorridere, ma anche senza il bisogno di uno specchio era ovvio che il mio sorriso fosse talmente falso da farmi star male."


translate italian epilogue_mi_c390a24d:


    "Ben presto siamo arrivati in una piccola radura."


translate italian epilogue_mi_77b14888:


    mt "Laggiù…"


translate italian epilogue_mi_0498059b:


    "Olga Dmitrievna ha puntato il dito verso un albero."


translate italian epilogue_mi_477f49be:


    "Ho fatto qualche passo in avanti e improvvisamente ho notato un braccio umano mozzato.{w} In realtà non sembrava nemmeno mozzato, era come se fosse stato strappato."


translate italian epilogue_mi_6e403fb6:


    "Masha ha sbirciato da dietro le mie spalle, e immediatamente ha urlato inorridita."


translate italian epilogue_mi_3d6cfafe:


    my "Non guardare!"


translate italian epilogue_mi_efa05797:


    "È scoppiata in lacrime e si è gettata tra le braccia di Olga Dmitrievna."


translate italian epilogue_mi_3352219f:


    "Ho fatto un notevole sforzo: ho dovuto guardare dietro l'albero per assicurarmi che si trattasse effettivamente di Slavya."


translate italian epilogue_mi_a45eb573:


    "C'erano parti umane sparse ovunque: un braccio mutilato, una gamba dilaniata in più punti, un pezzo di carne che fuoriusciva dal petto, una testa con i capelli imbrattati di sangue coagulato, e degli occhi vitrei profondamente infossati."


translate italian epilogue_mi_6ae474eb:


    "Mi è venuto da vomitare, e istintivamente ho distolto lo sguardo."


translate italian epilogue_mi_7cb2ad73:


    my "Chi può aver fatto tutto questo...?"


translate italian epilogue_mi_a0c3cc82:


    mt "Che ne so!"


translate italian epilogue_mi_bd343a2b:


    "Ha detto Olga Dmitrievna, in tono sorprendentemente freddo come il ghiaccio."


translate italian epilogue_mi_2cd46005:


    mt "In tutto il Campo, voi due siete gli unici senza un alibi."


translate italian epilogue_mi_9ef4463b:


    "Masha continuava a piangere, ora sulla mia spalla."


translate italian epilogue_mi_8746139c:


    my "Che intende dire, crede seriamente che avremmo potuto fare… una cosa del genere?!"


translate italian epilogue_mi_76171197:


    "L'ho fissata infuriato."


translate italian epilogue_mi_2deb3a6a:


    "Ora non importa più cosa stia accadendo qui – che siano scherzi stupidi o qualcosa di più grave – una persona è stata assassinata!"


translate italian epilogue_mi_4cc32fd1:


    "Una persona che conoscevamo bene.{w} Slavya, Sasha – non fa alcuna differenza!"


translate italian epilogue_mi_9b82e676:


    mt "Beh, no…"


translate italian epilogue_mi_c0ab7802:


    "Ha esitato."


translate italian epilogue_mi_9c0b8144:


    mt "Ma qualcuno l'ha fatto!"


translate italian epilogue_mi_19cc5970:


    my "Direi che è stata opera di qualche animale selvatico. Probabilmente lupi."


translate italian epilogue_mi_5b070411:


    mt "Non ci sono mai stati lupi in questa regione…"


translate italian epilogue_mi_0b9bc04a:


    my "Allora crede che possa essere stato un essere umano?!"


translate italian epilogue_mi_e504c1f3:


    mt "Beh, non c'è nessun altro da incolpare…"


translate italian epilogue_mi_9d5abb5c:


    my "No, questo non ha alcun senso… Semplicemente non può…"


translate italian epilogue_mi_03e931a3:


    "Ho stretto Masha ancora più forte."


translate italian epilogue_mi_32a03025:


    "I suoi singhiozzi si sono placati."


translate italian epilogue_mi_4385d694:


    mt "In ogni caso, dobbiamo chiamare la polizia."


translate italian epilogue_mi_d61b9628:


    my "Certo che dobbiamo! Andiamo!"


translate italian epilogue_mi_a20cefa7_13:


    "..."


translate italian epilogue_mi_978dd8c8:


    "Il centro del distretto è abbastanza lontano dal Campo…{w} La polizia non riuscirà a raggiungerci in breve tempo…"


translate italian epilogue_mi_dad64e6b:


    "Stava calando la notte."


translate italian epilogue_mi_209abbb1:


    "Ero seduto nella piazza e stavo osservando il tramonto."


translate italian epilogue_mi_cd30d9b9:


    "Masha dormiva nella stanza di Olga Dmitrievna."


translate italian epilogue_mi_6eda1825:


    "Era sconvolta molto più di noi.{w} Per molte ragioni…"


translate italian epilogue_mi_a5a8139e:


    "C'erano due domande principali nella mia mente: la prima – che cosa stava succedendo qui, e la seconda – chi ha ucciso Slavya.{w} Oppure Sasha. In ogni caso, chi era l'assassino?"


translate italian epilogue_mi_a645528a:


    "La prima domanda poteva essere rinviata per il momento.{w} Ma non avevo ancora idea di come affrontare la seconda."


translate italian epilogue_mi_b9e6fa5a:


    "Nonostante tutta la confusione, la possibilità che fosse stata sbranata dalle belve selvatiche sembrava essere la spiegazione più ragionevole."


translate italian epilogue_mi_919c8c4f:


    "Non mi importa se ci sono o meno i lupi qui…{w} Potrebbe anche essere stato un orso! O un tasso… Ma certamente non un essere umano!"


translate italian epilogue_mi_a5f91356:


    "Nessuno sarebbe in grado di fare una cosa simile, senza strumenti aggiuntivi."


translate italian epilogue_mi_252102f2:


    "E quelle ferite indicavano chiaramente che la sfortunata vittima era stata smembrata a mani nude (o zampe)."


translate italian epilogue_mi_44e8a885:


    "Anche se è vero che un essere umano infuriato può dotarsi di una forza sovrumana…"


translate italian epilogue_mi_e0069242:


    "Ma di tutte le persone qui presenti, chi potrebbe essere in grado di fare qualcosa del genere? Router? Lena? O magari Olga Dmitrievna?"


translate italian epilogue_mi_a4cb3fed:


    "No, che pensiero stupido."


translate italian epilogue_mi_d3f93a18:


    "Ma se anche fosse, c'erano molti bambini oltre a loro nella mensa e in piazza."


translate italian epilogue_mi_b10ee23b:


    "Ma ho davvero intenzione di interrogare ogni singola persona?{w} È un compito che dovrebbe spettare alla polizia!"


translate italian epilogue_mi_b9939397:


    "Allora cosa c'è di cui preoccuparsi? Forse non ho paura per me stesso…{w} ma per Masha…"


translate italian epilogue_mi_b2e7370c:


    "Giusto, devo verificare come sta!"


translate italian epilogue_mi_ca00c08d:


    "Ero sul punto di alzarmi quando improvvisamente ho sentito la presenza di qualcuno."


translate italian epilogue_mi_4a224c84:


    "Mi sono voltato e ho visto Lena."


translate italian epilogue_mi_86d435bf:


    un "Oh, non volevo spaventarti…"


translate italian epilogue_mi_7851bc23_1:


    my "Nessun problema…"


translate italian epilogue_mi_c67f7c91:


    un "Posso sedermi qui?"


translate italian epilogue_mi_3cdc5a84:


    my "Sì, siediti, prego."


translate italian epilogue_mi_e29e5371:


    "Si è aggiustata la gonnellina con cura, si è seduta e ha cominciato a fissare il tramonto."


translate italian epilogue_mi_3b7b5e4d:


    un "Com'è triste tutto questo…"


translate italian epilogue_mi_4170725c:


    my "Già, non c'è molto da rallegrarsi."


translate italian epilogue_mi_7d25e023:


    un "Cosa ne pensi, chi potrebbe essere stato?"


translate italian epilogue_mi_4612d3fc:


    my "Come faccio a saperlo? Certamente non sono stato io."


translate italian epilogue_mi_4f66a37f:


    un "No, no! Non è che io stia sospettando di te!"


translate italian epilogue_mi_e77f4d13:


    "Ha iniziato ad agitare le mani in modo buffo."


translate italian epilogue_mi_09ae1847:


    my "Bene, grazie."


translate italian epilogue_mi_851fed46:


    "Ho sorriso tristemente."


translate italian epilogue_mi_41c9cd8c:


    un "Ma dev'essere stata una persona molto cattiva e crudele."


translate italian epilogue_mi_6fa72eff:


    my "Beh, non necessariamente…"


translate italian epilogue_mi_d713736e:


    "Lena mi ha fissato, confusa."


translate italian epilogue_mi_c2861722:


    my "Forse si è trattato di un raptus improvviso. Dopotutto, nella vita di tutti i giorni i maniaci possono apparire come delle brave persone."


translate italian epilogue_mi_b9867686:


    un "Quindi, stai dicendo che…?"


translate italian epilogue_mi_0ae367b8:


    "I suoi occhi si sono spalancati e ha aperto leggermente la bocca."


translate italian epilogue_mi_dbe54858:


    un "Che potrebbe essere stato uno di noi...?"


translate italian epilogue_mi_ee63da1c:


    my "Beh, non lo sto affermando con certezza, questo è sicuro… Ma la cerchia dei sospetti è piuttosto estesa."


translate italian epilogue_mi_a69b68b1:


    un "No… Nessuno dei bambini avrebbe potuto farlo… Nessuno…"


translate italian epilogue_mi_e69771c0:


    my "In ogni caso, spetta alla polizia occuparsene…"


translate italian epilogue_mi_a4f042ce:


    "Abbiamo trascorso un altro po' di tempo seduti in silenzio."


translate italian epilogue_mi_1918988a:


    "Stavo per andarmene, quando Lena improvvisamente mi ha sussurrato:"


translate italian epilogue_mi_090aef2c:


    un "O potresti…"


translate italian epilogue_mi_b9e79bf2_1:


    my "Che cosa?"


translate italian epilogue_mi_4745db66:


    "Un sorriso innaturale è apparso sul suo volto."


translate italian epilogue_mi_7b836a93:


    "Un sorriso diabolico – si potrebbe dire."


translate italian epilogue_mi_298ef616:


    un "O potresti essere stato tu ad ucciderla?"


translate italian epilogue_mi_a4b858b3:


    my "Certo che no!"


translate italian epilogue_mi_683f39d5:


    un "Ma voi due non andavate molto d'accordo…"


translate italian epilogue_mi_a6716552:


    my "Perché la pensi così? Siamo stati sempre bene insieme."


translate italian epilogue_mi_82a95096:


    "Ormai è diventato chiaro che Lena, molto probabilmente, non stesse parlando di me."


translate italian epilogue_mi_a34c7a38:


    "Più precisamente, non del me di ieri.{w} E non della Slavya il cui nome è Sasha."


translate italian epilogue_mi_536e6181:


    "Ne ero sicuro."


translate italian epilogue_mi_a0e48a85:


    my "No, non sono stato io!"


translate italian epilogue_mi_0f6c6eab:


    un "Va bene, va bene, rilassati, non ti sto accusando…"


translate italian epilogue_mi_ba4db5a4:


    "Mi ha lanciato uno sguardo, e per un istante i suoi occhi mi sono sembrati rossi."


translate italian epilogue_mi_7f322486:


    "Forse era solo il riflesso del tramonto…"


translate italian epilogue_mi_0ed60aab:


    my "Sono contento che tu mi abbia capito."


translate italian epilogue_mi_8dc0ae33:


    un "Ma l'hai detto tu stesso che si dovrebbe sospettare di chiunque."


translate italian epilogue_mi_09973a1a:


    my "Non ti ho detto di «sospettare di chiunque»! Intendevo dire che non sappiamo chi sia il criminale! Non ancora…"


translate italian epilogue_mi_cb7116cc:


    un "Oh, capisco. Beh, spero di non diventare la prossima vittima!"


translate italian epilogue_mi_a73c91c7:


    "Ha detto ad alta voce ed è scoppiata a ridere."


translate italian epilogue_mi_e7ba1f7e:


    "Il modo in cui l'ha fatto mi ha messo i brividi."


translate italian epilogue_mi_ccdf7460:


    un "Ok, è ora di andare."


translate italian epilogue_mi_7bec5d07:


    "Si è alzata e ha lasciato rapidamente la piazza."


translate italian epilogue_mi_d1c55a8c:


    "Ho deciso di non indugiare e mi sono diretto alla casetta di Olga Dmitrievna."


translate italian epilogue_mi_a20cefa7_14:


    "..."


translate italian epilogue_mi_50fa59ee:


    "Ormai era completamente buio..."


translate italian epilogue_mi_ee446fb5:


    "Masha stava ancora dormendo, così ho deciso di non svegliarla, e sono salito sul letto accanto a lei, silenziosamente."


translate italian epilogue_mi_e4f4f44e:


    "La nostra regista (o la leader del Campo, come veniva chiamata dai residenti locali) non si è fatta ancora vedere."


translate italian epilogue_mi_87554d1b:


    "Se lei dovesse entrare proprio ora, la cosa sarebbe piuttosto imbarazzante.{w} Beh, potrebbe esserlo, per lo meno."


translate italian epilogue_mi_1cd7d952:


    "In ogni caso, chi se ne frega? Una persona è stata uccisa – non è il momento di pensare a certe sciocchezze!"


translate italian epilogue_mi_8f3d22b2:


    "E comunque, dobbiamo ancora scoprire che cosa sta succedendo – perché tutti continuano a comportarsi come se fossero dei veri pionieri?"


translate italian epilogue_mi_9ef4ae27:


    "Sembra che fino a quando non avremo scoperto l'identità del criminale, tutto questa ridicola sceneggiata non si fermerà."


translate italian epilogue_mi_f240c5d7:


    "La vera domanda è: qual è la causa e qual è l'effetto…?"


translate italian epilogue_mi_a59fd30e:


    "O tutti continuavano a fingere perché Slavya era stata uccisa (non riuscivo ancora ad accettare che Sasha fosse la vittima, così ho continuato a convincermi del fatto che si trattasse effettivamente di Slavya), o Slavya era stata uccisa perché tutti continuavano a fingere…"


translate italian epilogue_mi_c6188591:


    "Dannazione! Non ci capisco più niente, e tutto questo pensare non mi sta portando da nessuna parte!"


translate italian epilogue_mi_90f7eafc:


    "La porta ha scricchiolato debolmente."


translate italian epilogue_mi_cdaff57d:


    "Cautamente mi sono alzato un po' sui gomiti e ho guardato nel buio."


translate italian epilogue_mi_108f56f1:


    "Pochi istanti tortuosi sono trascorsi, ma alla fine non è entrato nessuno."


translate italian epilogue_mi_05be7ae4:


    "Beh, credo che sia inutile starsene sdraiati ad aspettare."


translate italian epilogue_mi_080212dd:


    "Probabilmente è solo il vento che soffia o qualcosa di simile, ma così a notte fonda, quando tutti stanno dormendo…"


translate italian epilogue_mi_68b1a2b9:


    "Mi sono alzato, ho raccolto i miei pantaloni nel buio e mi sono guardato attorno."


translate italian epilogue_mi_7d8bf151:


    "Era improbabile riuscire a trovare qualcosa qui che potessi usare come arma."


translate italian epilogue_mi_63056596:


    "Ma almeno dovevo provarci."


translate italian epilogue_mi_e04f66bc:


    "Rovistando tra i mobili della stanza sono incappato in un cassetto chiuso a chiave all'interno dell'armadietto."


translate italian epilogue_mi_40adda07:


    "Certo, mi sarebbe piaciuto trovare una pistola o almeno un qualche tipo di piede di porco. Tuttavia, avevo capito che non dovevo sperarci troppo."


translate italian epilogue_mi_55acc250:


    "Ma quel cassetto doveva essere stato chiuso a chiave per qualche motivo!"


translate italian epilogue_mi_10eae588:


    "Ho scostato l'armadio dal muro, cercando di stimare dove potesse trovarsi esattamente quel cassetto sul lato posteriore, e ho tirato un calcio forte e preciso (facendo attenzione a non svegliare Masha)."


translate italian epilogue_mi_07f9ee90:


    "Il compensato si è incrinato, e alcuni oggetti sono fuoriusciti dal cassetto."


translate italian epilogue_mi_db4073e9:


    "Biancheria intima...{w} altra biancheria...{w} fiammiferi."


translate italian epilogue_mi_d0a78133:


    "Oh, potrebbero tornarmi utili."


translate italian epilogue_mi_c88871a5:


    "Mi sono messo in tasca la scatola di fiammiferi, e nello stesso istante ho sentito degli strani rumori provenire dall'altro lato della porta, come se qualche animale selvatico la stesse graffiando."


translate italian epilogue_mi_9795e900:


    "Certamente se si trattasse del vero criminale, non potrei avere alcuna possibilità contro di lui…"


translate italian epilogue_mi_274d3fca:


    "Ma non dovrei escludere del tutto che potesse essere opera di un «pioniere» locale."


translate italian epilogue_mi_6e7c4600:


    "Che potrebbe essere impazzito o che fosse semplicemente folle fin dall'inizio…"


translate italian epilogue_mi_3d71d05e:


    "In ogni caso, dovrei essere comunque in grado di mettere KO uno qualsiasi dei residenti locali.{w} Anche se si presentasse davanti a me con un coltello…"


translate italian epilogue_mi_760217c6:


    "Il pensare a un'arma mi ha fatto venir la pelle d'oca."


translate italian epilogue_mi_0c0e46bd:


    "No, non dovrei essere così presuntuoso!"


translate italian epilogue_mi_c7ea3de8:


    "Se dovessi imbattermi in qualcuno armato di coltello, ad esempio, allora dovrei semplicemente serrare la porta, barricare tutte le entrate e chiedere aiuto!"


translate italian epilogue_mi_2466c05f:


    "Probabilmente si può trovare un'arma da fuoco qui…{w} Anche se…"


translate italian epilogue_mi_c189c152:


    "La porta ha scricchiolato nuovamente, aprendosi di qualche centimetro."


translate italian epilogue_mi_1ad9f73d:


    "Ho chiuso gli occhi per un secondo e poi li ho spalancati violentemente, raccogliendo tutte le mie forze."


translate italian epilogue_mi_d2ae2e3c:


    "Ho sentito un grido soffocato e ho visto Alisa a braccia e gambe aperte sulla soglia."


translate italian epilogue_mi_e13ab7da:


    my "Ah, sei solo tu…"


translate italian epilogue_mi_606c5803:


    "Ho tirato un sospiro di sollievo."


translate italian epilogue_mi_93228cc9:


    my "Non spaventare le persone in quel modo! Vuoi farmi venire un infarto?"


translate italian epilogue_mi_63ed4b6c:


    "Si è rialzata, si è spolverata lentamente i vestiti e mi ha guardato con aria colpevole."


translate italian epilogue_mi_77613451:


    dv "Scusami…"


translate italian epilogue_mi_91d22172:


    my "Va bene. È solo che… potrebbe essere pericoloso girare da soli per il campo durante la notte – con tutte le cose che stanno accadendo di recente…"


translate italian epilogue_mi_2a7a8a3b:


    "Mi sono bloccato."


translate italian epilogue_mi_f9d90a33:


    "«Stanno accadendo» implica un qualcosa ancora in corso.{w} Come se qualcosa debba ancora succedere a breve..."


translate italian epilogue_mi_126375f0:


    my "Intendo dire, che sono successe…"


translate italian epilogue_mi_a3a258d8:


    dv "È proprio per questo che sono venuta…"


translate italian epilogue_mi_0fc06d24:


    "Alisa stava fissando il pavimento, tremando un po'."


translate italian epilogue_mi_2517d987:


    dv "È già tardi, ma Ulyana non è ancora tornata."


translate italian epilogue_mi_71da9f7d:


    "Ha alzato lo sguardo verso di me, ed ho notato le lacrime che le scorrevano sulle guance."


translate italian epilogue_mi_31b3f8f3:


    my "Dove potrebbe essere andata?"


translate italian epilogue_mi_f8590b39:


    "Non è che me ne importasse molto, ma gliel'ho chiesto perché sentivo che avevo il dovere di calmare la situazione."


translate italian epilogue_mi_b82284e4:


    "È palese che non ci sia assolutamente niente da fare in questo campo durante la notte – se ci fosse l'elettricità, allora starebbero tutti guardando i film sui loro computer portatili, altrimenti leggerebbero qualche libro al lume di candela."


translate italian epilogue_mi_32d70287:


    "Ma girovagare al chiaro di luna non dovrebbe rientrare in quelle attività…"


translate italian epilogue_mi_bf433c67:


    dv "Non lo so…"


translate italian epilogue_mi_0709f70a:


    my "Va bene, cerchiamo di non saltare a conclusioni affrettate…"


translate italian epilogue_mi_23b8137e:


    "Mi sono avvicinato al tavolo e ho preso l'orologio."


translate italian epilogue_mi_26bbbc79:


    "Mezzanotte.{w} Certo, tutti dovrebbero essere già nelle loro stanze a quest'ora."


translate italian epilogue_mi_65e215d0:


    "Un pensiero spaventoso improvvisamente mi è balenato nella mente.{w} Dov'è Olga Dmitrievna...?"


translate italian epilogue_mi_2f7416f2:


    my "Andiamo a parlare fuori. Non voglio svegliare Ma… Miku…"


translate italian epilogue_mi_11889848:


    "Appena siamo usciti, mi sono seduto sui gradini e ho iniziato a pensare."


translate italian epilogue_mi_5be4562e:


    "Alisa stava in piedi accanto a me, torcendosi le mani con sguardo sofferente mentre guardava la luna piena."


translate italian epilogue_mi_203b67d9:


    dv "Dobbiamo chiamare la polizia."


translate italian epilogue_mi_cb6ef91a:


    my "Li abbiamo già chiamati."


translate italian epilogue_mi_fb4190cc:


    dv "Ma prima era solo Slavya, adesso anche Ulyana è scomparsa…"


translate italian epilogue_mi_e63e04ce:


    my "E anche Olga Dmitrievna. L'hai vista per caso?"


translate italian epilogue_mi_b8e9ed52:


    dv "No… Ma che cosa, anche lei è…?"


translate italian epilogue_mi_b680e150:


    "Alisa è scoppiata a piangere."


translate italian epilogue_mi_c0fe2e06:


    my "Non sappiamo ancora niente, ma…"


translate italian epilogue_mi_01f7b9c3:


    "Ciò nonostante, è chiaro che qui sta succedendo qualcosa!"


translate italian epilogue_mi_06d2a65f:


    "Anche se non facessero finta di essere dei pionieri e qualcosa stesse veramente accadendo al mondo, in questo momento c'erano alte probabilità di sparire del tutto senza prima aver ottenuto alcuna risposta."


translate italian epilogue_mi_d6e99617:


    "Quindi devo concentrarmi sulla sopravvivenza…"


translate italian epilogue_mi_d8fea1da:


    "«Sopravvivenza»…{w} Quella parola è stata come un forte dolore dietro alla nuca."


translate italian epilogue_mi_aa6ca21f:


    "«Sopravvivenza»…{w} Appena ieri non avrei mai pensato di dover considerare una cosa del genere…"


translate italian epilogue_mi_1b3de5ca:


    "La tranquilla notte estiva regnava su ogni cosa attorno a noi – non un soffio di vento, non un fruscio d'erba né il movimento delle foglie avrebbero potuto essere sentiti.{w} Niente di niente."


translate italian epilogue_mi_0bbb84bd:


    "C'era solo il riflesso della luna sui vetri delle finestre, le nuvole sparse nel cielo, e ancora un'impenetrabile oscurità tutt'attorno – dicono che quando fissi dritto nell'abisso, l'abisso fissa dritto dentro di te…"


translate italian epilogue_mi_845dfb1b:


    "E le cavallette!{w} Quelle maledette creature si stanno dando il cambio alla postazione da DJ!"


translate italian epilogue_mi_77894e87:


    "Come se fossero costantemente in servizio – il turno diurno finisce e, dopo un numero sufficiente di ore di sonno, inizia quello notturno.{w} Poi quello notturno si conclude, e…"


translate italian epilogue_mi_f03db694:


    "Devo riuscire finalmente a trovare un insetticida – almeno potrei annientare un paio di legioni, sperando di riuscire a rendere in qualche modo vivibile la zona intorno alla mia casetta."


translate italian epilogue_mi_ae29cb48:


    "Certo, non potrei sicuramente disintegrarle tutte…"


translate italian epilogue_mi_6e9aee0c:


    "E che diavolo, probabilmente ci sono più cavallette in questo luogo che esseri umani sull'intero pianeta…"


translate italian epilogue_mi_18e03cde:


    dv "L'hai sentito...?"


translate italian epilogue_mi_bdaf90db:


    "Alisa si è abbassata e si è rannicchiata verso di me.{w} Stava tremando tutta."


translate italian epilogue_mi_b9e79bf2_2:


    my "Sentito cosa?"


translate italian epilogue_mi_21f9a60f:


    "Ho teso le orecchie."


translate italian epilogue_mi_a969edc9:


    "Sembrava che qualcuno stesse camminando lentamente lungo il sentiero, verso la nostra casetta."


translate italian epilogue_mi_d7c542e6:


    "Non riuscivo a vedere niente nell'oscurità, ma i passi si stavano avvicinando sempre più."


translate italian epilogue_mi_53d2a1ff:


    my "Ok, non preoccuparti… Non avrà il coraggio di attaccarci entrambi…"


translate italian epilogue_mi_9e7e2c33:


    "Anche se non ne ero proprio convinto."


translate italian epilogue_mi_62e9f0fe:


    "Le mie mani istintivamente hanno iniziato a cercare qualcosa da usare come arma di difesa."


translate italian epilogue_mi_93b5fc50:


    dv "Ehi!"


translate italian epilogue_mi_ec37960b:


    my "Scusa…"


translate italian epilogue_mi_e042b998:


    "Ben presto ho trovato un pezzo di tubo sotto alla panca."


translate italian epilogue_mi_58a4016f:


    "Non è di grande aiuto, ovviamente – è troppo spesso e scomodo da maneggiare correttamente – ma è sempre meglio che stare a mani nude."


translate italian epilogue_mi_a37831a4:


    "Chiunque si farebbe la pipì addosso alla sola vista di questa mia mostruosa arma di difesa!{w} Molto probabilmente dalle risate…"


translate italian epilogue_mi_1b95dc17:


    "Non c'è niente che io possa fare – siamo dei facili bersagli qui, quindi preferirei passare all'attacco."


translate italian epilogue_mi_9bcff4c0:


    "Non si poteva dire che fossi impavido e coraggioso, in realtà era esattamente il contrario – stavo davvero impazzendo dalla paura."


translate italian epilogue_mi_b9ce7582:


    "Sembrava proprio che non avessi altra scelta…"


translate italian epilogue_mi_f55c7e59:


    "Ho fatto un profondo respiro e mi sono diretto lentamente nell'oscurità."


translate italian epilogue_mi_439b90ad:


    "Ben presto ho potuto distinguere meglio la sagoma dello straniero.{w} Non molto alto, uno strano taglio di capelli, una gonnellina…"


translate italian epilogue_mi_86e127e7:


    "Doveva essere Lena!"


translate italian epilogue_mi_ef6c8558:


    "Ho abbassato il tubo."


translate italian epilogue_mi_af67a563:


    my "Che ci fai qui a notte fonda?"


translate italian epilogue_mi_ef1f0ae2:


    un "Io… Io…"


translate italian epilogue_mi_2c21a7bc:


    "Il volto di Lena era ricoperto dalle lacrime."


translate italian epilogue_mi_448b06ba_1:


    my "Vieni."


translate italian epilogue_mi_a6d8a438:


    "L'ho accompagnata da Alisa."


translate italian epilogue_mi_5b1b0f13:


    "Dopo essersi ripresa, Lena ha iniziato a raccontarci cosa era successo."


translate italian epilogue_mi_41ff22a7:


    un "Me ne stavo seduta da sola e… è già abbastanza tardi e… ma Miku non è ancora tornata… eppure io… e quello che è successo a Slavya… e io…"


translate italian epilogue_mi_3019d555:


    "Non riuscivo proprio a riconoscere Lena in lei – una ragazza singhiozzante seduta di fronte a me – dopo quello che avevo visto alla piazza poche ore fa, quel suo sorriso diabolico e quelle fiamme infernali nei suoi occhi."


translate italian epilogue_mi_772757c7:


    "Forse è stata solo la mia immaginazione…{w} O forse no…"


translate italian epilogue_mi_97352ac0:


    my "Per caso sai dove potrebbero essere Ulyana e Olga Dmitrievna?"


translate italian epilogue_mi_3932a02b:


    "Ha alzato il suo sguardo sorpreso verso di me."


translate italian epilogue_mi_74ae6a8d:


    un "No, perché… sono scomparse anche loro?"


translate italian epilogue_mi_52f6ea86:


    my "Beh, non possiamo esserne sicuri, ma…"


translate italian epilogue_mi_4297ae4e:


    dv "Semyon…"


translate italian epilogue_mi_5b9b3c9e:


    "In un primo momento non mi ero reso conto che si stava rivolgendo a me."


translate italian epilogue_mi_366fbd9e:


    my "Eh?"


translate italian epilogue_mi_d6783e26:


    dv "Non credi che sia diventato tutto un po' troppo silenzioso qui?"


translate italian epilogue_mi_9c6951b5:


    my "È notte, stanno dormendo tutti, non è forse così…?"


translate italian epilogue_mi_09a7148c:


    "Ho teso le orecchie.{w} In effetti, non c'erano altri rumori a parte quelle dannate cavallette."


translate italian epilogue_mi_ae49ef98:


    "Voglio dire, proprio nessun suono, niente di niente!"


translate italian epilogue_mi_c86f665e:


    my "Di solito com'è questo posto?"


translate italian epilogue_mi_bdbb2826:


    un "Tranquillo…"


translate italian epilogue_mi_7e596111:


    "Ha sussurrato Lena."


translate italian epilogue_mi_100a8b2f:


    dv "Silenzioso, ma non come adesso."


translate italian epilogue_mi_20845d8d:


    "Non potevo permettere che si facessero prendere dal panico."


translate italian epilogue_mi_0b7cd763:


    "Anche se io stesso stavo iniziando a perdere la testa."


translate italian epilogue_mi_c62e4545:


    "In effetti avevo tutte le ragioni del mondo per farlo: persone che sparivano nel nulla, terribili omicidi, gli amici che continuavano a fingere di essere dei pionieri."


translate italian epilogue_mi_aa1ba754:


    "Stavo per impazzire…"


translate italian epilogue_mi_d00c5bcd:


    my "Ok, cerchiamo di pensare razionalmente."


translate italian epilogue_mi_fcc1f8e9:


    "Le ragazze mi hanno guardato con aria interrogativa."


translate italian epilogue_mi_630f46ee:


    my "La gente non può semplicemente sparire senza lasciare traccia!"


translate italian epilogue_mi_6b952a33:


    un "Questo significa che…"


translate italian epilogue_mi_480a55d0:


    my "No, questo non significa proprio nulla!"


translate italian epilogue_mi_8fff31b7:


    "Improvvisamente abbiamo sentito un fruscio tra i cespugli dietro la casetta."


translate italian epilogue_mi_ee4a8cbd:


    "Non ho neppure avuto il tempo di afferrare il tubo, che Router è emerso dal buio."


translate italian epilogue_mi_b7ab4b8d:


    ro "Ah, eccovi qui!"


translate italian epilogue_mi_6d30a2a8:


    "Non sembrava essere molto preoccupato di tutto ciò che stava accadendo."


translate italian epilogue_mi_2986d3b8:


    my "Oh Signore, smettila di balzare davanti alle persone in quel modo! Finirai col far venire un infarto alle ragazze!"


translate italian epilogue_mi_776c97bc:


    "Anche se non sapevo con certezza chi di noi fosse più spaventato – se loro o io."


translate italian epilogue_mi_43b66a40:


    ro "Sì, scusate…"


translate italian epilogue_mi_38116f5e:


    my "Perché sei venuto?"


translate italian epilogue_mi_d2c06568:


    ro "Avete visto Shurik?"


translate italian epilogue_mi_b84b7fde:


    my "No…"


translate italian epilogue_mi_75fdeb9e:


    "Pare che la metà della popolazione del Campo sia semplicemente sparita nelle ultime ore."


translate italian epilogue_mi_a5873d1b:


    ro "Inoltre, è troppo tranquillo tutt'attorno…"


translate italian epilogue_mi_b01d3ea3:


    my "L'abbiamo già capito!"


translate italian epilogue_mi_763991f9:


    ro "Non c'è luce da nessuna parte, un silenzio tombale…"


translate italian epilogue_mi_c4497e2f:


    my "Oh, smettila di alimentare la tensione qui! Mi sento già male!"


translate italian epilogue_mi_a90284ea:


    ro "Cosa? Io non sto facendo niente…"


translate italian epilogue_mi_d4f9df56:


    "Mi ha fatto un sorriso enigmatico, che mi ha dato i brividi."


translate italian epilogue_mi_569deb9a:


    my "Va bene, voi rimanete qui, mentre io do un'occhiata attorno al Campo!"


translate italian epilogue_mi_c87950e7:


    un "Da solo?"


translate italian epilogue_mi_8ab3285c:


    "Già, probabilmente non è una buona idea…{w} Dovrei pensare prima di parlare."


translate italian epilogue_mi_c90f163e:


    my "Ehm…"


translate italian epilogue_mi_862e0a1f:


    dv "Verrò con te!"


translate italian epilogue_mi_0f4ce8dc:


    my "Va bene, allora voi due rimanete qui! E non dimenticatevi che Ma… Miku sta ancora dormendo nella casetta."


translate italian epilogue_mi_785bce8b:


    ro "Signor sì, signore!"


translate italian epilogue_mi_ee6a930d:


    "Che coglione! Se mi avesse fatto quel sorriso intontito in qualsiasi altra situazione…"


translate italian epilogue_mi_a20cefa7_15:


    "..."


translate italian epilogue_mi_a20cefa7_16:


    "..."


translate italian epilogue_mi_982fd05a:


    "Circa mezz'ora dopo, la nostra ricerca ci ha portati alla piazza."


translate italian epilogue_mi_9112918f:


    "Tutte le casette erano vuote...{w} Sembrava che noi cinque fossimo gli unici rimasti in tutto il Campo."


translate italian epilogue_mi_594f012c:


    "Ho zoppicato assieme al mio pezzo di tubo."


translate italian epilogue_mi_5326720a:


    "In un primo momento mi è sembrato che fosse così leggero, ma dopo essermelo trascinato dietro per un po' di tempo, ho capito che mi sarei potuto stancare persino portando una borsa di piume."


translate italian epilogue_mi_0feb0847:


    dv "Ma dove diavolo sono finiti tutti…"


translate italian epilogue_mi_f5ac9909:


    "Alisa sembrava essere più o meno calma.{w} Il terrore dei primi minuti, quando passavamo da una casetta vuota ad un'altra, sembrava essere già passato."


translate italian epilogue_mi_d5218c9a:


    my "Beh, almeno possiamo essere certi che non si tratta della stessa cosa… di quello che è successo a Slavya!"


translate italian epilogue_mi_59c3611a:


    dv "Come fai a esserne sicuro?"


translate italian epilogue_mi_b7312eb9:


    my "Pensaci… Dove sono i corpi?"


translate italian epilogue_mi_06fb3902:


    "Le ho chiesto in un sussurro, per paura di spaventarla."


translate italian epilogue_mi_4b68666f:


    dv "Ma dove sono tutti allora?"


translate italian epilogue_mi_bb984b76:


    my "Scomparsi...?"


translate italian epilogue_mi_d01110d6:


    "Una spiegazione poco credibile…{w} Ma a pensarci bene, nessuna delle cose accadute nelle ultime ventiquattr'ore era credibile."


translate italian epilogue_mi_7c45825b:


    dv "Non avrebbero potuto semplicemente andarsene e… proprio così…"


translate italian epilogue_mi_d63d25c0:


    my "Non è che dipenda da noi adesso. E, in ogni caso, domani è un altro giorno."


translate italian epilogue_mi_72f142cf:


    "Ho cercato di fare un sorriso."


translate italian epilogue_mi_ba81381a:


    my "Torniamo dagli altri!"


translate italian epilogue_mi_593113c9:


    dv "Sì…"


translate italian epilogue_mi_01f64c2f:


    "Ha concordato, con un pizzico di indecisione."


translate italian epilogue_mi_413d048d:


    "Siamo riusciti a fare solo qualche passo quando un fruscio tra i cespugli è arrivato da dietro."


translate italian epilogue_mi_848599fc:


    "Mi sono voltato di scatto e ho subito fissato il punto da cui proveniva quel rumore."


translate italian epilogue_mi_4b8f2dde:


    "Alisa si è aggrappata al mio braccio talmente forte da farmi male, e io ho stretto la presa attorno al tubo."


translate italian epilogue_mi_e0ac0d35:


    dv "Chi… c'è…?"


translate italian epilogue_mi_326ad2f9_1:


    my "Non ne ho idea…"


translate italian epilogue_mi_6e7af154:


    "Ho cercato di mettere a fuoco, ma ancora non riuscivo a distinguere nulla nell'oscurità."


translate italian epilogue_mi_333411d8_1:


    my "Ehi!"


translate italian epilogue_mi_ffe234c7:


    "Non avevo idea di quale dovesse essere la mia prossima mossa."


translate italian epilogue_mi_c42d06d1:


    "Il fruscio improvvisamente si è placato, e la notte è tornata al suo solito caos di grilli."


translate italian epilogue_mi_52152cc5:


    my "Potrebbe essere qualche animale selvatico…"


translate italian epilogue_mi_fefef18f:


    "Ho supposto, con insicurezza."


translate italian epilogue_mi_42a83dbb:


    dv "Un... animale selvatico?"


translate italian epilogue_mi_42e4e478:


    "Mi ha stretto il braccio ancora più forte."


translate italian epilogue_mi_049c7fdc:


    dv "Ma non ci sono animali selvatici qui intorno!"


translate italian epilogue_mi_7996282d:


    my "Uhm, uno scoiattolo, per esempio, non saprei…"


translate italian epilogue_mi_066810ef:


    dv "Uno scoiattolo?"


translate italian epilogue_mi_6ae4ca96:


    my "Sì, uno scoiattolo."


translate italian epilogue_mi_b95f9fd2:


    dv "Beh, forse…"


translate italian epilogue_mi_f87596bf:


    my "Andiamo?"


translate italian epilogue_mi_3b4b97c0:


    dv "Sì…"


translate italian epilogue_mi_a20cefa7_17:


    "..."


translate italian epilogue_mi_e258274a:


    "Un minuto più tardi ci stavamo avvicinando alla casetta di Olga Dmitrievna."


translate italian epilogue_mi_103eb1f8:


    "Router e Lena sono rimasti in silenzio, rabbrividendo leggermente quando ci hanno visti arrivare."


translate italian epilogue_mi_4e1a88c8:


    ro "Allora, cosa avete scoperto?"


translate italian epilogue_mi_5b845686:


    my "Non c'è nessuno là fuori."


translate italian epilogue_mi_dcb7a8c7:


    un "Che significa… nessuno?"


translate italian epilogue_mi_699e5734:


    dv "Sono spariti tutti…"


translate italian epilogue_mi_ca44847e:


    "Ha detto Alisa tra le lacrime."


translate italian epilogue_mi_200d2256:


    "Le ragazze si sono abbracciate piangendo, e io mi sono seduto accanto a Router, lasciando finalmente cadere a terra quel mio tubo aberrante."


translate italian epilogue_mi_e985afc7:


    ro "Tutto qui?"


translate italian epilogue_mi_6a716cb7:


    my "Tutto qui…"


translate italian epilogue_mi_f347bf4c:


    ro "Ma com'è possibile?"


translate italian epilogue_mi_1659118a:


    my "Davvero non lo so… Forse è un rapimento alieno?"


translate italian epilogue_mi_8303b1d0:


    ro "Che sciocchezze!"


translate italian epilogue_mi_eefcb0ea:


    my "Vuoi giocare a fare il detective, Sherlock?"


translate italian epilogue_mi_e44817f8:


    ro "Beh, potrebbe essere stato lo stesso maniaco…"


translate italian epilogue_mi_0b807932:


    my "Non ti sembra che sia... un po' troppo per una singola persona, non ti pare?"


translate italian epilogue_mi_32b76f96:


    ro "Credo che tu abbia ragione… Ma chi… che cosa può essere stato allora?"


translate italian epilogue_mi_a29f386e:


    my "C'è ancora la remota possibilità che possa trattarsi di uno stupido scherzo…"


translate italian epilogue_mi_b5d68c97:


    ro "E allora Slavya? Anche quello era uno stupido scherzo?"


translate italian epilogue_mi_0b63e0e1:


    my "No, lei è stata chiaramente uccisa da qualcuno."


translate italian epilogue_mi_a1f1a290:


    ro "Ma da chi?"


translate italian epilogue_mi_2c5335a6:


    "Mi ha guardato ad occhi stretti."


translate italian epilogue_mi_f1897fd9:


    my "E che ne so! Magari sei stato tu!"


translate italian epilogue_mi_262d7cae:


    "Mi stavo innervosendo."


translate italian epilogue_mi_37e35222:


    ro "Oppure tu!"


translate italian epilogue_mi_699cf6d5:


    my "Io no di sicuro."


translate italian epilogue_mi_9da52b6d:


    ro "Come puoi dimostrarlo?"


translate italian epilogue_mi_e3275cbc:


    my "E perché diavolo dovrei dimostrare qualcosa a te?"


translate italian epilogue_mi_d7c7a76d:


    ro "Sei l'unico senza un alibi!"


translate italian epilogue_mi_9bb0e846:


    my "Ah, quindi tu invece ce l'hai, eh?"


translate italian epilogue_mi_4bae00e6:


    ro "Ero nella mia stanza quella notte – Shurik mi è testimone!"


translate italian epilogue_mi_76243b78:


    my "Certo, ma purtroppo Shurik è temporaneamente indisponibile…"


translate italian epilogue_mi_daca0afa:


    "Router ha stretto i denti e si è voltato."


translate italian epilogue_mi_71d9105b:


    ro "Comunque, sono io colui che ha un alibi qui. Non ho intenzione di giustificarmi! Sono sicuro di quello che dico!"


translate italian epilogue_mi_3b810b08:


    my "Sono contento che tu creda in quello che dici… Ma anch'io sono sicuro di me stesso! E Miku può testimoniare – sono stato con lei per tutto il tempo."


translate italian epilogue_mi_c814e87f:


    ro "Non proprio la miglior testimone possibile…"


translate italian epilogue_mi_094c7520:


    my "Esattamente come il tuo!"


translate italian epilogue_mi_74d118df:


    "Non avevo idea di quanto tempo avremmo trascorso a litigare in quel modo, se Lena non fosse intervenuta nella nostra conversazione."


translate italian epilogue_mi_0284f524:


    un "Ragazzi, dai, smettetela…"


translate italian epilogue_mi_bf352153:


    my "Hai ragione, scusa…"


translate italian epilogue_mi_44cacce6:


    ro "Già…"


translate italian epilogue_mi_400b9c27:


    "Ora non è davvero il momento migliore per questo genere di cose."


translate italian epilogue_mi_9b909de6:


    my "In ogni caso, dobbiamo restare uniti…"


translate italian epilogue_mi_a7fd75a5:


    dv "Ci aiuterà a non scomparire?"


translate italian epilogue_mi_0046a8da:


    "Ha chiesto timidamente Alisa."


translate italian epilogue_mi_70bbf125:


    my "Non sono sicuro di tutta questa storia delle sparizioni, ma almeno dovrebbe aiutarci a rimanere in vita!"


translate italian epilogue_mi_4e5b4343:


    "Ho guardato l'orologio – erano quasi le quattro di mattina!{w} Il sole sarebbe sorto presto!"


translate italian epilogue_mi_709706dd:


    my "Ok, squadrone! È ora di coricarsi, o verremo spazzati via completamente."


translate italian epilogue_mi_46466e3f:


    un "Ma…"


translate italian epilogue_mi_35fb6c26:


    my "Niente ma!"


translate italian epilogue_mi_71ed3f9b:


    "Nessun altro ha cercato di opporsi, e siamo entrati silenziosamente nella stanza."


translate italian epilogue_mi_cf4f8759:


    ma "Ah? Che sta succedendo...?"


translate italian epilogue_mi_b6b544d2:


    "Pare che abbiamo svegliato Masha."


translate italian epilogue_mi_1addbba1:


    my "È tutto a posto, continua a dormire…"


translate italian epilogue_mi_688bd005:


    "Ma lei si era già seduta sul letto, guardando la nostra piccola festicciola con espressione assente."


translate italian epilogue_mi_bb72e793:


    ro "Beh, vedi, c'è…"


translate italian epilogue_mi_13d057b3:


    un "Se ne sono andati tutti! Scomparsi! Sono spariti tutti!"


translate italian epilogue_mi_ae15ba1a:


    "Lena è scoppiata a piangere e si è gettata tra le braccia di Masha."


translate italian epilogue_mi_a35f21fb:


    ma "Come? Che sta succedendo? Potreste spiegarmi in modo chiaro?"


translate italian epilogue_mi_a22c92b6:


    my "Beh, per farla breve…"


translate italian epilogue_mi_3a6935fd:


    "Ho fatto schioccare il collo e ho cercato compulsivamente le sigarette nella mia tasca. Non riuscendo a trovarle, ho sospirato, e le ho detto:"


translate italian epilogue_mi_ccc382cd:


    my "Sembra che siamo rimasti solo noi cinque in tutto il Campo…"


translate italian epilogue_mi_e8e8d6ba:


    ro "Il che significa che siamo tutti in questa stanza!"


translate italian epilogue_mi_1f6dec3c:


    my "Gioite! Il Capitan Ovvio è tornato! Sarai il primo a fare da guardia!"


translate italian epilogue_mi_6c21d46f:


    ro "Eh?"


translate italian epilogue_mi_c12f9eb4:


    my "Niente! Pensi davvero che sia una cosa saggia dormire tutti nello stesso momento?"


translate italian epilogue_mi_f1b10303:


    ro "Beh, non lo è… ma…"


translate italian epilogue_mi_d54d0a6f:


    my "Esatto!"


translate italian epilogue_mi_20db871e:


    "Gli ho porto il tubo e mi sono avvicinato lentamente al letto dove Lena stava ancora singhiozzando tra le braccia di Masha."


translate italian epilogue_mi_db756b30:


    my "Beh, se non vi dispiace…"


translate italian epilogue_mi_1f2560d6:


    "Mi sono sfilato gli stivali, ho scavalcato le ragazze e mi sono girato verso la parete."


translate italian epilogue_mi_844b71c4:


    ma "Ma come…"


translate italian epilogue_mi_4576c932:


    my "Basta… Dormite!"


translate italian epilogue_mi_03f7efc1:


    "Ero così esausto – sia fisicamente che, cosa ancora più importante, emotivamente – che non ero in grado di fare alcunché, di pensare a nulla, di comunicare con nessuno."


translate italian epilogue_mi_1dff787e:


    "Avevo un disperato bisogno di dormire un po'…{w} Anche troppo disperato…"


translate italian epilogue_mi_e3998c81:


    "I sussurri di Masha, i singhiozzi di Lena, la voce di Alisa – tutto si è fuso in una tranquilla ninna nanna, e così mi sono addormentato."


translate italian epilogue_mi_a20cefa7_18:


    "..."


translate italian epilogue_mi_0540bbef:


    "Mi sono svegliato forzatamente a causa di qualcuno che stava cercando con insistenza di mettere a dura prova il mio equilibrio."


translate italian epilogue_mi_df4372df:


    "Equilibrio in senso fisico, più che mentale – Router era chino su di me e mi stava scuotendo la spalla."


translate italian epilogue_mi_b9e79bf2_3:


    my "Che c'è?"


translate italian epilogue_mi_fc10bec9:


    ro "È il tuo turno adesso!"


translate italian epilogue_mi_3e837789:


    my "Per quanto a lungo ho dormito?"


translate italian epilogue_mi_49aa35d6:


    ro "Circa un'ora."


translate italian epilogue_mi_dab8428f:


    my "Solo un'ora? Resta di guardia…"


translate italian epilogue_mi_2ddefabc:


    ro "Ma sto già sonnecchiando…"


translate italian epilogue_mi_d3a7a6c6:


    "Già, una fonte di energia infinita non guasterebbe al nostro povero Router!{w} Anche se non farebbe male neppure a me…"


translate italian epilogue_mi_e9e6e7f9_1:


    my "Oh, d'accordo…"


translate italian epilogue_mi_44cfa98c:


    "Mi sono alzato con notevole sforzo, ho scavalcato Lena e Masha, che dormivano abbracciate, e sono saltato sul pavimento."


translate italian epilogue_mi_7f4ec5bf:


    "La stanza sembrava leggermente più luminosa."


translate italian epilogue_mi_edfb1f7d:


    my "Si sta facendo mattina…"


translate italian epilogue_mi_44cacce6_1:


    ro "Già…"


translate italian epilogue_mi_e2db42a2:


    "Router ha cercato di andare al mio posto sul letto, ma l'ho afferrato per il colletto, l'ho tirato giù sul pavimento e gli ho gettato addosso una coperta e un cuscino."


translate italian epilogue_mi_004e3a87:


    ro "Quindi tu puoi e io…"


translate italian epilogue_mi_4d46ae5d:


    my "E tu non puoi!"


translate italian epilogue_mi_402485e7:


    "A pensarci bene, perché non può…?{w} In ogni caso, non è così importante adesso."


translate italian epilogue_mi_12e4b3cb:


    "Ho preso la sedia, l'ho trascinata verso la porta, mi sono seduto e ho iniziato ad aspettare."


translate italian epilogue_mi_1fa1a644:


    "«Impazienza» sembrava essere il termine migliore per descrivere quello che provavo."


translate italian epilogue_mi_e34cb82f:


    "Da un momento all'altro la porta si spalancherà e un maniaco irromperà nella stanza… O gli ultimi abitanti di questo campo scompariranno…{w} Oppure io scomparirò…"


translate italian epilogue_mi_c3a2c9f0:


    "In ogni caso, qualcosa sarebbe successo – me lo sentivo."


translate italian epilogue_mi_cc495d0c:


    "Router russava alla grande, le ragazze stavano dormendo al sicuro, ed anch'io stavo cominciando a sonnecchiare."


translate italian epilogue_mi_5b9c71e5:


    "Ho stretto il tubo più forte – la sensazione del freddo metallo contro la mia pelle nuda in qualche modo mi ha riportato ai miei sensi."


translate italian epilogue_mi_01c45f1f:


    "Non devo addormentarmi!"


translate italian epilogue_mi_cb07ce8b:


    "Io sono la loro unica speranza adesso.{w} E anche la mia…"


translate italian epilogue_mi_746b4d5c:


    "Ma è così difficile resistere…{w} Soltanto un paio di minuti…"


translate italian epilogue_mi_7d4bb7ad:


    "I miei occhi sembravano avere vita propria, aprendosi e chiudendosi spontaneamente mentre la mia mente vagava."


translate italian epilogue_mi_31518315:


    "Ho sentito un fruscio provenire da dietro la porta.{w} No, dev'essere la mia immaginazione – sono ancora quelle dannate cavallette!"


translate italian epilogue_mi_10788c07:


    "Che cosa stanno tramando…?"


translate italian epilogue_mi_b1e7167b:


    "Forse è tutta colpa delle cavallette?! Sono piombate sul Campo come uno sciame di locuste, divorando ogni cosa…"


translate italian epilogue_mi_0284a5b0:


    "Una visione di me che venivo divorato dalle cavallette è apparsa nella mia mente…{w} Una tempesta disgustosa stava fermentando nella mia testa."


translate italian epilogue_mi_e4e546d0:


    "Quelle loro lunghe zampette, le loro ali, una testa enorme e gli occhi… colmi di oscurità!"


translate italian epilogue_mi_b49efc2e:


    "Occhi che riflettono l'abisso."


translate italian epilogue_mi_2aa219d6:


    "Basta che guardi una cavalletta negli occhi – e non sarai più lo stesso…"


translate italian epilogue_mi_ce617998:


    "…"


translate italian epilogue_mi_ed851b67:


    "Ecco cosa – io scappo e loro mi inseguono! Ce ne sono a migliaia, anzi milioni!"


translate italian epilogue_mi_e0b1f94a:


    "Sto scavalcando disperatamente pozzi e calanchi, sulle colline, tra le rocce!"


translate italian epilogue_mi_e424f733:


    "Ma a loro non importa – loro hanno le ali!{w} Già, quelle loro sporche ali, che generano quel fastidioso rumore!"


translate italian epilogue_mi_e39b3c47:


    "Probabilmente non avrebbero bisogno dei denti in ogni caso – a loro basta sbattere quelle orribili ali sempre più velocemente, generando l'onda sonora dell'apocalisse."


translate italian epilogue_mi_ba3e9f48:


    "No!{w} Non riesco più a sopportare quel rumore infernale! Sto cadendo, sto cercando disperatamente di coprirmi le orecchie con le mani…"


translate italian epilogue_mi_38997562:


    "Per favore, basta! Aiutatemi…"


translate italian epilogue_mi_ce617998_1:


    "…"


translate italian epilogue_mi_c7d7a7a5:


    "Mi sono svegliato con un forte dolore dietro alla nuca."


translate italian epilogue_mi_9d40e697:


    "Dopo aver aperto gli occhi, mi sono reso conto di essere sdraiato sul pavimento."


translate italian epilogue_mi_3529a363:


    "Sembra che mi sia addormentato e che sia caduto dalla sedia…"


translate italian epilogue_mi_4a584bb7:


    "Erano le nove."


translate italian epilogue_mi_26659c8d:


    "Dannazione, pare che io abbia dormito per cinque ore di fila!{w} Può essere successa ogni cosa!"


translate italian epilogue_mi_fb4b0b46:


    "Terrorizzato, mi sono tastato freneticamente, ma tutto sembrava essere al suo posto."


translate italian epilogue_mi_13e9f254:


    "Fuh… C'è mancato poco."


translate italian epilogue_mi_f74f70f2:


    "Ho risvegliato il mio squadrone."


translate italian epilogue_mi_18abd84b:


    ro "È già il mio turno...?"


translate italian epilogue_mi_70ab00a2:


    my "Hai dormito anche troppo! Mentre io sono rimasto seduto qui… per tutta la notte…"


translate italian epilogue_mi_6f311047:


    "Certo, era una bugia, ma dire loro che avevo trascorso la notte scappando dalle cavallette non era un'opzione valida."


translate italian epilogue_mi_bcc18214:


    ro "Mi dispiace…"


translate italian epilogue_mi_3d2c6bb8:


    "Router ha abbassato la testa."


translate italian epilogue_mi_5f6d175a:


    my "Non preoccuparti…"


translate italian epilogue_mi_7a8ced10:


    ma "Aw, sono proprio affamata."


translate italian epilogue_mi_6f990f62:


    "Masha si è stirata sorridendomi dolcemente."


translate italian epilogue_mi_49c498a0:


    ma "E perché siete tutti qui?"


translate italian epilogue_mi_bae123b1:


    un "Non ti ricordi niente?"


translate italian epilogue_mi_7556a267:


    ma "No, qual è il problema?"


translate italian epilogue_mi_699e5734_1:


    dv "Sono spariti tutti…"


translate italian epilogue_mi_e38a29fe:


    "Ha detto Alisa, in tono cupo."


translate italian epilogue_mi_20f54ffc_1:


    ma "Cos'hai detto?"


translate italian epilogue_mi_70a3cd5c:


    dv "Tutti sono spariti… Svaniti nel nulla…"


translate italian epilogue_mi_d79f8f4c:


    "Masha ha guardato fuori dalla finestra senza dire niente.{w} Sembra che adesso se lo sia ricordato…"


translate italian epilogue_mi_6cc461d6:


    my "Beh… Andare a mangiare qualcosa è decisamente una buona idea!"


translate italian epilogue_mi_0f0c21bc:


    "Nessuno aveva obiezioni a riguardo."


translate italian epilogue_mi_a624eeb3:


    "Non è che fossi in vena di mangiare, come tutti gli altri, ma alla fine tutti noi sapevamo che era necessario ripristinare le nostre energie."


translate italian epilogue_mi_a20cefa7_19:


    "..."


translate italian epilogue_mi_f13a4b32:


    "Ben presto eravamo seduti nella mensa, mangiando in silenzio qualsiasi cosa fosse commestibile."


translate italian epilogue_mi_a9c1d18e:


    ma "Volete che vi prepari qualcosa?"


translate italian epilogue_mi_22f7f7dc:


    "Ha chiesto Masha tranquillamente."


translate italian epilogue_mi_f862db93:


    my "Magari per pranzo. Per ora va bene così."


translate italian epilogue_mi_1356cf00:


    "Mi ha guardato con espressione delusa."


translate italian epilogue_mi_695687ee:


    ro "Ok, allora cosa facciamo adesso?"


translate italian epilogue_mi_c3a696ad:


    my "Dobbiamo andarcene da qui."


translate italian epilogue_mi_13582c80:


    un "Ma la polizia…"


translate italian epilogue_mi_a12d2714:


    my "Dobbiamo andarcene da qui!"


translate italian epilogue_mi_5254c792:


    "Ho ripetuto ostinatamente."


translate italian epilogue_mi_e1353452:


    dv "Ma come?"


translate italian epilogue_mi_06988cc4:


    my "In autobus."


translate italian epilogue_mi_c6062e4b:


    dv "È altamente improbabile che ne arriverà qualcuno. Non passano molto spesso da queste parti…"


translate italian epilogue_mi_688cf4d4:


    my "Dovrebbe esserci un orario qui da qualche parte."


translate italian epilogue_mi_d1c129fd:


    "Ho cercato di ricordare se io avessi mai notato una qualche mappa degli autobus o un orario da qualche parte.{w} Probabilmente no…"


translate italian epilogue_mi_989754e9:


    my "Allora ce ne andremo a piedi! Qualsiasi cosa è meglio che stare qui!"


translate italian epilogue_mi_91dba49d:


    un "Ma è una strada molto lunga…"


translate italian epilogue_mi_5d634972:


    my "Beh, potremmo chiedere un passaggio."


translate italian epilogue_mi_8d827cef:


    ro "Qui, in mezzo al nulla? Ne dubito…"


translate italian epilogue_mi_37160b7d:


    my "Oh, smettila di drammatizzare!"


translate italian epilogue_mi_df404933:


    "Anche se a pensarci bene, era proprio il momento giusto per drammatizzare…"


translate italian epilogue_mi_d94b9a8c:


    ro "Non sto drammatizzando, ma…"


translate italian epilogue_mi_dec09dcb:


    my "Basta, fine della storia."


translate italian epilogue_mi_739f8c27:


    dv "Che cosa diremo quando raggiungeremo la città?"


translate italian epilogue_mi_3893f203:


    my "Diremo loro la verità – che eravamo venuti qui per girare un film, e poi…"


translate italian epilogue_mi_85899043:


    ro "Quale film?"


translate italian epilogue_mi_c2b1462d:


    my "Oh, piantatela di fare i buffoni, ok?"


translate italian epilogue_mi_4cb424ce:


    "Ho sepolto il viso tra le mani, sfinito."


translate italian epilogue_mi_aa01c424:


    my "Anche se ieri poteva essere un po' divertente (anche se in realtà non lo era), adesso è ora di finirla!"


translate italian epilogue_mi_535db658:


    ro "Non ti capisco…"


translate italian epilogue_mi_861228df:


    my "Oh, vi va ancora di fingere di non essere venuti qui per girare un film, ma per giocare il ruolo di pionieri?"


translate italian epilogue_mi_4c950402:


    dv "Non capisco proprio niente di quello che stai dicendo…"


translate italian epilogue_mi_c4b533d9:


    "Alisa ha guardato tutti con sguardo sorpreso."


translate italian epilogue_mi_f3ebcebc:


    dv "Ma noi siamo pionieri…"


translate italian epilogue_mi_0b861b18:


    ma "Quali pionieri, per l'amor di Dio?!"


translate italian epilogue_mi_8fdf19ba:


    "Masha, che era rimasta in silenzio per tutto il tempo, si è intromessa."


translate italian epilogue_mi_b017a002:


    ma "È il ventunesimo secolo là fuori! Già, proprio così, pionieri!"


translate italian epilogue_mi_2eff6d6b:


    dv "Vent…"


translate italian epilogue_mi_8eeb0718:


    ro "...unesimo?"


translate italian epilogue_mi_3e7184e9:


    "Sembrava che fossimo davvero in un casino assoluto…"


translate italian epilogue_mi_6a191ff8:


    my "Allora, volete dirmi che voi tutti siete… beh, che noi tutti siamo… dei normali pionieri che sono appena giunti qui in vacanza per una settimana o giù di lì?"


translate italian epilogue_mi_d30e024c:


    "È sceso un silenzio tombale."


translate italian epilogue_mi_5f2878d9:


    dv "Beh… sì…"


translate italian epilogue_mi_01ce7e3e_1:


    "Ho guardato Masha."


translate italian epilogue_mi_9f98315f:


    "Pare che neppure lei sappia come reagire a tutto questo."


translate italian epilogue_mi_91e2a42f:


    "Le ho fatto cenno, indicando la porta."


translate italian epilogue_mi_547acb5a:


    my "Scusateci un secondo…"


translate italian epilogue_mi_296d110b:


    "Dopo essere usciti, le ho chiesto:"


translate italian epilogue_mi_9397443d:


    my "Cosa ne pensi?"


translate italian epilogue_mi_697b0ee3:


    ma "Che dire…"


translate italian epilogue_mi_20e75221:


    "Si è messa in una posa da filosofo greco."


translate italian epilogue_mi_8d1032a5:


    ma "Sembra che ne siano davvero convinti. E che alcune persone, di nome Semyon e Miku, fossero nei nostri panni fino a ieri."


translate italian epilogue_mi_b74846e4:


    my "Alcune…"


translate italian epilogue_mi_94ce0421:


    ma "Beh, suppongo che tu le conosca molto meglio di me!"


translate italian epilogue_mi_b78f3341:


    my "Oh, andiamo!"


translate italian epilogue_mi_e5e97d5d_1:


    "L'ho interrotta."


translate italian epilogue_mi_2e070fe6:


    my "Non potresti inventarti una teoria un po' più verosimile?"


translate italian epilogue_mi_bc6cebcc:


    ma "Non ho nessuna teoria a dire il vero… Ormai non so più cosa pensare…"


translate italian epilogue_mi_b4d52943:


    my "Nemmeno io…"


translate italian epilogue_mi_f6aad95c:


    ma "E comunque, l'inferno è sceso su questo luogo anche senza considerare i pionieri! Omicidi! Decine di persone che scompaiono senza lasciar traccia! Questa dovrebbe essere la nostra priorità adesso."


translate italian epilogue_mi_c7c73a32:


    "Ho guardato Masha attentamente."


translate italian epilogue_mi_fb6ae8d4:


    "Mi chiedo come riesca a mantenere il sangue freddo in una situazione del genere? Certo, anch'io sto cercando di mantenere la calma, ma…"


translate italian epilogue_mi_144a299a:


    ma "E quindi…"


translate italian epilogue_mi_b9e79bf2_4:


    my "Che cosa?"


translate italian epilogue_mi_1a88a249:


    ma "Rimandiamo questa questione."


translate italian epilogue_mi_7b4c77c1:


    my "Ma almeno un'ipotesi?"


translate italian epilogue_mi_40161182:


    ma "Credo che sia in qualche modo legato alle sparizioni. È lecito ritenere che uno… o anche due o tre… esseri umani non avrebbero potuto far sparire così tante persone in una volta sola…"


translate italian epilogue_mi_ad288150:


    my "Sono d'accordo."


translate italian epilogue_mi_14d957a2:


    ma "Forse questo fenomeno in qualche modo li ha influenzati."


translate italian epilogue_mi_71666196:


    my "Potrebbe essere…"


translate italian epilogue_mi_1677698d:


    ma "Ad ogni modo, è più facile credere che siamo in una realtà parallela o qualcosa di simile, ad esempio."


translate italian epilogue_mi_0466237c:


    my "Sai, ormai sono disposto a credere a tutto…"


translate italian epilogue_mi_63797f2e:


    ma "Allora cerchiamo di attenerci a questa teoria, per adesso."


translate italian epilogue_mi_a27cf3ba:


    my "Va bene…"


translate italian epilogue_mi_f60fe5df:


    ma "Andiamo?"


translate italian epilogue_mi_16ade63c:


    my "Tu vai, ti raggiungo tra un minuto."


translate italian epilogue_mi_d0722fc6:


    ma "È pericoloso stare tutto solo…"


translate italian epilogue_mi_23baa87a:


    my "Seriamente, cosa c'è di così pericoloso? È pieno giorno, e siamo tutti qui."


translate italian epilogue_mi_5a3ed4bf:


    ma "Va bene."


translate italian epilogue_mi_036327ef:


    "Mi ha fatto un debole sorriso ed è tornata nella mensa."


translate italian epilogue_mi_57d8a15d:


    "Comunque, anche il suo comportamento è abbastanza strano."


translate italian epilogue_mi_c1100f55:


    "Pare che gli ultimi giorni abbiano cambiato radicalmente tutto e tutti!{w} Inclusa Masha…"


translate italian epilogue_mi_deca8d3b:


    "Ieri – shock, paura e brividi. Oggi – una mente fredda e ragionamenti sensati."


translate italian epilogue_mi_8cd31d45:


    "Diamine! È persino più sicura di me!"


translate italian epilogue_mi_55e9d52c:


    "Potrebbe essere grazie alla bella dormita?{w} No, c'è qualcosa di sbagliato anche in lei!"


translate italian epilogue_mi_968e3ce8:


    "Certo, almeno non crede di essere un pioniere come tutti gli altri, però…"


translate italian epilogue_mi_750bd51e:


    "Se qualcosa fosse accaduto a loro, allora forse è accaduto anche a me…"


translate italian epilogue_mi_1df2225f:


    "No! No, non può essere, tutto sembra andare bene…"


translate italian epilogue_mi_5c2c6e71:


    "Mi sono tastato più volte, per capire se qualcosa fosse cambiato in me."


translate italian epilogue_mi_361e4259:


    "Ok, devo mantenere la calma e l'autocontrollo…"


translate italian epilogue_mi_b72dfe95:


    "Mi sono alzato e stavo per tornarmene alla mensa, quando improvvisamente ho sentito una vocina provenire da dietro le mie spalle."


translate italian epilogue_mi_45e11c59:


    "Come se qualcuno mi stesse chiamando."


translate italian epilogue_mi_20953507:


    "Sono sceso dalla veranda e ho fatto qualche passo in direzione di quella voce."


translate italian epilogue_mi_9f8b3255:


    "Qualcuno mi stava spiando da dietro un albero."


translate italian epilogue_mi_a19d3e12:


    "Era Ulyana.{w} No, non Ulyana, ma qualcosa che le assomigliava…"


translate italian epilogue_mi_a9617350:


    "Sono rimasto pietrificato."


translate italian epilogue_mi_30c877bb:


    "Metà del suo volto era lacerato, a brandelli. La sua mascella sorridente tremava nervosamente, come se stesse ridendo. Un occhio iniettato di sangue, fuoriuscito per metà dalla sua orbita, ha ruotato improvvisamente, puntando la pupilla verso di me."


translate italian epilogue_mi_70b8148d:


    "Ho smesso di respirare.{w} Mi sentivo come se stessi per morire lì all'istante, a causa dello shock."


translate italian epilogue_mi_e5b6bc80:


    us "Semyon…"


translate italian epilogue_mi_b5deb76e:


    "La cosa che assomigliava a Ulyana mi ha chiamato per nome."


translate italian epilogue_mi_5da5e5ae:


    "Le mie gambe hanno iniziato a tremare."


translate italian epilogue_mi_85dad370:


    "Ho cercato di restare in piedi, ho cercato di urlare, ho cercato di scappare…"


translate italian epilogue_mi_c9e0ed8c:


    "Ma la mia coscienza aveva già abbandonato il mio corpo, che ormai non era altro che una bambola di pezza, che si muoveva per volontà di un burattinaio invisibile."


translate italian epilogue_mi_118f2951:


    "Un solo secondo in più e…"


translate italian epilogue_mi_bfc45034:


    ma "Ehi!"


translate italian epilogue_mi_dca1260c:


    "Istintivamente mi sono girato e ho visto Masha in piedi sulla veranda, e poi immediatamente ho riportato lo sguardo verso «Ulyana»."


translate italian epilogue_mi_5007383a:


    "Ma era sparita…"


translate italian epilogue_mi_d04071e3:


    "Masha sembrava aver notato il pallore mortale sul mio volto ed è balzata verso di me in un batter d'occhio."


translate italian epilogue_mi_49a38e33:


    ma "Che c'è? Cos'è successo?"


translate italian epilogue_mi_1c8f07f7:


    my "Ho… Ho appena visto Ulyana… Proprio lì…"


translate italian epilogue_mi_02db51d2:


    "Ho puntato il dito verso l'albero."


translate italian epilogue_mi_f7f58c4b:


    "Certo, la vista della Slavya smembrata era stata davvero raccapricciante…"


translate italian epilogue_mi_c633ec22:


    "Ma almeno lei era morta e non se ne andava in giro in pieno giorno con la faccia a pezzi, e non mi chiamava per nome (anche se quello non era il mio nome)."


translate italian epilogue_mi_3c3e173c:


    "Ho esaurito le ultime gocce della mia forza e sono crollato in ginocchio."


translate italian epilogue_mi_d3701ebe:


    ma "Cosa? Dove?"


translate italian epilogue_mi_5217a1fa:


    "Masha mi stava scuotendo per le spalle."


translate italian epilogue_mi_b957be9d:


    my "Non era lei… Era… Non saprei…"


translate italian epilogue_mi_9bf74aca:


    ma "Riprenditi!"


translate italian epilogue_mi_409bc2a3:


    "Ho sentito un forte schiaffo sul viso, e ho guardato Masha con aria interrogativa."


translate italian epilogue_mi_32cd69c5:


    "La mia guancia bruciava, ma ho ripreso lentamente i sensi."


translate italian epilogue_mi_e5d90224:


    my "Ulyana era lì… aveva la faccia…{w} In sostanza, era uno zombie. Sì, un'Ulyana zombie. Questa è la spiegazione più razionale che posso darti!"


translate italian epilogue_mi_f159f577:


    "Masha mi ha guardato sbalordita."


translate italian epilogue_mi_ddba048a:


    ma "Tutto può succedere qui..."


translate italian epilogue_mi_0f8b803f:


    my "Se l'avessi vista con i tuoi occhi…"


translate italian epilogue_mi_cbdc4e80:


    ma "Grazie a Dio non l'ho vista! Vieni, ti porto un bicchiere d'acqua!"


translate italian epilogue_mi_a20cefa7_20:


    "..."


translate italian epilogue_mi_33498a6c:


    "Ho descritto a tutti il mio incontro con quella creatura, in ogni dettaglio."


translate italian epilogue_mi_3eab3cd4:


    ro "Posso solo dire una cosa – la scienza non può spiegare quello che sta accadendo qui in questo momento!"


translate italian epilogue_mi_db0c1724:


    dv "La scienza? È una scandalosa diavoleria quella che sta succedendo qui! È opera di Satana in persona! Peccato che non ci sia una chiesa qui vicino… Per poter accendere una candela…"


translate italian epilogue_mi_6a561d69:


    "Lena è rimasta in silenzio, cercando di calmare i suoi singhiozzi."


translate italian epilogue_mi_1a7c1a1e:


    "Mi sono ripreso quasi del tutto, e adesso ero in grado di pensare in modo lucido."


translate italian epilogue_mi_0a4cb391:


    my "Ora è perfettamente chiaro che dobbiamo andarcene da qui!"


translate italian epilogue_mi_83ad4a9c:


    ro "Beh, non sono un vero scienziato… E nemmeno un esperto di fenomeni paranormali, ma, se ricordo bene, tutti i demoni e gli spiriti sono legati a un qualche luogo. Pertanto, se riuscissimo ad andarcene da questo luogo…"


translate italian epilogue_mi_f6b9796e:


    ma "Oh, ma che soluzione geniale!"


translate italian epilogue_mi_be002e4f:


    "Masha ha sorriso con scetticismo."


translate italian epilogue_mi_ad288150_1:


    my "Sono d'accordo."


translate italian epilogue_mi_605106a3:


    "Ho esclamato, ignorandola."


translate italian epilogue_mi_f8f4d1e6:


    dv "Anch'io!"


translate italian epilogue_mi_bd8d303c:


    "Lena ha fatto un cenno appena percettibile."


translate italian epilogue_mi_375aadac:


    my "Allora è deciso!"


translate italian epilogue_mi_0505734e:


    "Ho battuto il pugno sul tavolo e mi sono alzato in piedi."


translate italian epilogue_mi_ae1a89b2:


    ma "Aspetta! Non è che siamo tutti d'accordo su questo…"


translate italian epilogue_mi_cd0d8c85:


    my "Perché dovremmo esitare?! Non ho intenzione di trovarmi nuovamente davanti a quella cosa!"


translate italian epilogue_mi_a7480fc6:


    ma "E dove andremo? Ti rendi conto di quanto sia lontana la città, come faremo a raggiungerla a piedi?"


translate italian epilogue_mi_e0790bc4:


    my "Dannazione, pensi davvero che nessuno passi mai per quella strada? Un'automobile, un autobus – qualunque cosa, ce la faremo in qualche modo!"


translate italian epilogue_mi_26c4382d:


    ro "Non ne sarei così sicuro…"


translate italian epilogue_mi_b9590f03:


    my "Allora credi che sia meglio restare qui?"


translate italian epilogue_mi_ee0e2d60:


    ro "Beh, non ho detto questo…"


translate italian epilogue_mi_59962f16:


    un "Fa paura qui… Soprattutto di notte…"


translate italian epilogue_mi_e2ce5d0c:


    "Ha sussurrato Lena, a bassa voce."


translate italian epilogue_mi_8690ec90:


    ma "Questo non significa che dobbiamo andarcene da qualche parte!"


translate italian epilogue_mi_3cd95137:


    "Ha detto Masha ad alta voce."


translate italian epilogue_mi_395daf08:


    "L'ho guardata ad occhi stretti.{w} No, qualcosa era sicuramente cambiato in lei."


translate italian epilogue_mi_c748f801:


    "Forse dovrei parlare con lei in privato."


translate italian epilogue_mi_c2ecd2a9:


    "O forse no?{w} Forse anche lei è…"


translate italian epilogue_mi_a8506c23:


    my "Va bene, allora votiamo!"


translate italian epilogue_mi_f2f360c8:


    "Ho detto in un tono abbastanza fiducioso, ma avevo i miei dubbi."


translate italian epilogue_mi_f988541d:


    "E se Masha avesse ragione? Forse non dovremmo vagare senza meta?"


translate italian epilogue_mi_0c03293b:


    "Da un lato, non ho idea di cosa ci potrebbe aspettare oltre le porte di questo Campo.{w} Ma dall'altro – si è davvero scatenato l'inferno qui…"


translate italian epilogue_mi_40ef1a37_2:


    ma "Va bene…"


translate italian epilogue_mi_b53b2a58:


    "Ha alzato le spalle."


translate italian epilogue_mi_0a36094e:


    "E poi non avevo intenzione di stare un solo minuto in più in questo luogo!"


translate italian epilogue_mi_5f20a77f:


    my "Partiamo!"


translate italian epilogue_mi_2b34d1a9:


    ma "Restiamo."


translate italian epilogue_mi_f4738552:


    dv "Proviamoci – forse saremo davvero fortunati e riusciremo a farci dare un passaggio."


translate italian epilogue_mi_5bf88fe1:


    ro "Partiamo…"


translate italian epilogue_mi_a7287f96:


    "Tutti hanno portato lo sguardo su Lena."


translate italian epilogue_mi_151b8dfc:


    un "Non voglio restare qui…"


translate italian epilogue_mi_4e16488f:


    "Pare che la mia opinione non fosse determinante per l'esito di quella votazione."


translate italian epilogue_mi_1521e234:


    "Va bene, allora partiamo!"


translate italian epilogue_mi_e130e273:


    my "Bene!"


translate italian epilogue_mi_3059b01f:


    "Sono balzato in piedi. Tutti gli altri sono rimasti seduti con la testa abbassata."


translate italian epilogue_mi_d04a0580:


    my "Dal momento che abbiamo deciso…"


translate italian epilogue_mi_bc561ca0:


    ma "Bene, dal momento che abbiamo deciso…"


translate italian epilogue_mi_7b696593:


    "Masha si è alzata lentamente e mi ha fissato."


translate italian epilogue_mi_5638fcf6:


    dv "E la nostra roba?"


translate italian epilogue_mi_97d81d27:


    "Ha chiesto goffamente Alisa."


translate italian epilogue_mi_445fb36b:


    ro "Credi davvero che dovremmo preoccuparcene in questo momento?"


translate italian epilogue_mi_ea7b081f:


    "Ha borbottato Router in modo cupo."


translate italian epilogue_mi_31933f01:


    dv "Sì, probabilmente hai ragione…"


translate italian epilogue_mi_75139ff0:


    "Beh, nel mio caso la mia roba era già svanita misteriosamente…"


translate italian epilogue_mi_a20cefa7_21:


    "..."


translate italian epilogue_mi_ede53218:


    "Dopo pochi minuti eravamo già alla fermata dell'autobus."


translate italian epilogue_mi_6049b2ea:


    ma "La città è in quella direzione."


translate italian epilogue_mi_38c29669:


    "Masha ha indicato l'orizzonte."


translate italian epilogue_mi_ec1be65f:


    my "O almeno, era lì l'altro ieri…"


translate italian epilogue_mi_fbd62a8b_1:


    "Ho sorriso."


translate italian epilogue_mi_67d59d3c:


    ro "Sì, è in quella direzione."


translate italian epilogue_mi_4264eb79:


    "Ha confermato Router."


translate italian epilogue_mi_283c52cb:


    "Siamo rimasti in silenzio per qualche istante, e poi ci siamo incamminati lentamente verso la nostra meta."


translate italian epilogue_mi_a20cefa7_22:


    "..."


translate italian epilogue_mi_a20cefa7_23:


    "..."


translate italian epilogue_mi_97183294:


    "Il clima estivo era insopportabilmente torrido, fondendo l'asfalto."


translate italian epilogue_mi_c0fe229c:


    "Ogni cosa a centinaia di metri di distanza – la strada, gli alberi, l'erba, il cielo – si scioglieva in una foschia di calore luminoso che sembrava più una nebbia ardente."


translate italian epilogue_mi_f47addae:


    "Stavo sudando come una fontana, e ho iniziato a pensare che avrei preso un colpo di calore."


translate italian epilogue_mi_770d11e5:


    "Certo, sarebbe stata una grande idea portarci dietro più acqua, ma l'unica cosa che contava quando ci stavamo preparando era portare i nostri culi il più lontano possibile da quel maledettissimo Campo."


translate italian epilogue_mi_4d43cd01:


    "Alla fine mi sentivo come se fossi sul punto di perdere i sensi."


translate italian epilogue_mi_7aadfff4:


    "Solo il miserabile zillare delle cavallette mi teneva ancora in contatto con la realtà."


translate italian epilogue_mi_c7b1fe99:


    "Ma certo!{w} Questo mezzogiorno infernale è davvero la loro parte preferita della giornata."


translate italian epilogue_mi_3e8862d2:


    "A loro non importava del caldo cocente, o dell'umidità pari a zero, o della mancanza di una qualsiasi corrente d'aria."


translate italian epilogue_mi_9a28a9d2:


    "Al contrario, loro si sentivano alla grande – una cornice perfetta per l'atto finale della loro ultima sinfonia apocalittica…"


translate italian epilogue_mi_13fa06ef:


    "Probabilmente saremmo dovuti restare al Campo, dopotutto."


translate italian epilogue_mi_abedaf26:


    "O almeno avremmo dovuto iniziare questa escursione un po' più tardi, verso sera."


translate italian epilogue_mi_5a9c692b:


    "Anche se vagare per una strada sperduta nel cuore della notte non sarebbe stata di certo una buona idea…"


translate italian epilogue_mi_7ea693c4:


    "Ho sentito un grido soffocato provenire da dietro le mie spalle."


translate italian epilogue_mi_3c7e799a:


    "Qualche decina di metri più indietro, Alisa è riuscita a malapena a sostenere Lena, impedendole di crollare a terra."


translate italian epilogue_mi_aec78f4c:


    "Mi sono precipitato verso di loro."


translate italian epilogue_mi_1d810d31:


    my "Qual è il problema?"


translate italian epilogue_mi_ea512261:


    dv "È svenuta!"


translate italian epilogue_mi_ad5a6d31:


    "Ho aiutato a portare Lena sul ciglio della strada. Almeno c'era un po' di ombra in un piccolo boschetto."


translate italian epilogue_mi_ac1911e9:


    ma "E con questo si conclude la nostra escursione…"


translate italian epilogue_mi_6a8b4df5:


    "Ha detto Masha in tono cupo, lasciandosi cadere sull'erba."


translate italian epilogue_mi_916de10c:


    ma "Vi avevo avvertiti!"


translate italian epilogue_mi_348aff25:


    my "Bene, ma cosa cambia adesso…"


translate italian epilogue_mi_b117497a:


    "Abbiamo viaggiato per almeno un paio d'ore, e quindi tornare adesso al Campo era decisamente fuori questione.{w} Beh, almeno non prima di sera, quando il caldo si sarebbe placato un po'."


translate italian epilogue_mi_faf254ea:


    ro "E non abbiamo ancora visto nemmeno l'ombra di un'automobile…"


translate italian epilogue_mi_864e06fe:


    "Mi sono voltato e ho guardato Router.{w} Si è tolto la camicia e ha cominciato a strizzare il sudore da essa."


translate italian epilogue_mi_7ba1bf38:


    "Alisa stava cercando di risvegliare Lena."


translate italian epilogue_mi_38622444:


    my "Se quello è un colpo di sole…"


translate italian epilogue_mi_f913a1de:


    dv "Smettila!"


translate italian epilogue_mi_c8f00552:


    "Mi ha gridato."


translate italian epilogue_mi_25c82915:


    "Se si tratta davvero un colpo di sole, allora sono guai seri – non riuscirà a muoversi di un singolo passo."


translate italian epilogue_mi_961bbfe5:


    "Ho camminato lungo la strada, schermando con la mano i miei occhi dalla luce del sole, e ho fissato l'orizzonte."


translate italian epilogue_mi_fdbd24c7:


    "Sempre la stessa linea asfaltata si estendeva fin oltre l'orizzonte, nessun segno di civiltà.{w} Proprio nessun segno di presenza umana."


translate italian epilogue_mi_3444bdc0:


    "Era come se fossimo le uniche persone rimaste, non solo nel Campo, ma nell'intero mondo."


translate italian epilogue_mi_c43dec15:


    "Anche se pensando a quello che è successo prima, non era proprio una grande sorpresa."


translate italian epilogue_mi_3915eb70:


    "Sono tornato dagli altri e mi sono seduto su un ceppo."


translate italian epilogue_mi_46d4b9bb:


    my "Credo che dovremmo andare in avanscoperta."


translate italian epilogue_mi_4c4c5aba:


    "Ho guardato Router."


translate italian epilogue_mi_750d0f10:


    my "Per controllare cosa c'è più avanti… Forse troveremo qualcosa… E voi intanto potere rimanere qui per un po'."


translate italian epilogue_mi_5a93ab85:


    dv "Ma…"


translate italian epilogue_mi_1ef5403c:


    my "Per adesso lei non può comunque andare da nessuna parte."


translate italian epilogue_mi_2ff95471:


    "Lena stava distesa ad occhi chiusi, respirando affannosamente."


translate italian epilogue_mi_202c17a8:


    ma "Va bene, andate."


translate italian epilogue_mi_5dd3dee9:


    "Ha detto Masha con calma."


translate italian epilogue_mi_3b32e781:


    "Mi è appena venuto in mente che avevo programmato di fare un discorso con lei…{w} Ma non è certamente questo il momento più adatto."


translate italian epilogue_mi_5175cd14:


    my "Andiamo?"


translate italian epilogue_mi_250c1da5:


    "Router ha alzato le spalle e mi ha seguito."


translate italian epilogue_mi_a20cefa7_24:


    "..."


translate italian epilogue_mi_f402d84a:


    "Abbiamo camminato per una quindicina di minuti. Le ragazze erano ormai molto indietro."


translate italian epilogue_mi_9aa33312:


    ro "Cosa ne pensi, qualche possibilità di incontrare qualcuno?"


translate italian epilogue_mi_a3533961:


    "Ha chiesto tranquillamente."


translate italian epilogue_mi_9ef048f4:


    my "Non lo so!"


translate italian epilogue_mi_987deb91:


    "Ho risposto in tono seccato – non avevo voglia di parlare in quel momento."


translate italian epilogue_mi_0a3b55b3:


    "Gli infiniti campi si estendevano verso l'orizzonte, cosparsi di poche macchie di alberi e colline."


translate italian epilogue_mi_e710db3b:


    "Il paesaggio sembrava così normale e tranquillo che, di fatto, provocava una sensazione di ansia."


translate italian epilogue_mi_68224858:


    "Se ci fossimo trovati nelle pianure dell'inferno, ci aspetteremmo di essere attaccati da ogni angolo, in modo da essere pronti ad affrontare il peggio."


translate italian epilogue_mi_ebc7f617:


    "Ma invece ci sentivamo proprio come delle piccole gocce di vernice sulla tela di un pittore senza nome che dipingeva l'estate della Russia centrale.{w} O dovunque siamo in questo momento…"


translate italian epilogue_mi_b17071ab:


    "Tutto questo non ha fatto altro che alimentare le nostre paure…"


translate italian epilogue_mi_6ae2ce2f:


    ro "Guarda!"


translate italian epilogue_mi_631bd070:


    "Ha gridato Router."


translate italian epilogue_mi_5b9ca325:


    "Ho strizzato gli occhi e ho visto che più avanti la strada mostrava finalmente una curva."


translate italian epilogue_mi_d049f728:


    my "Allora, cosa c'è di così speciale?"


translate italian epilogue_mi_0d705a9c:


    ro "Dev'esserci qualcosa laggiù!"


translate italian epilogue_mi_7e25d624:


    my "E cosa te lo fa pensare?"


translate italian epilogue_mi_15aa17c6:


    ro "Beh, dato che c'è una curva… E che non abbiamo fatto altro che camminare sempre dritto…"


translate italian epilogue_mi_ae92f472:


    my "Va bene, andiamo a dare un'occhiata."


translate italian epilogue_mi_36b49d85:


    "Dopo pochi minuti siamo finalmente arrivati…{w} alle porte del campo «Sovyonok»…"


translate italian epilogue_mi_971b05e7:


    "Sono crollato sul marciapiede, affondando il viso tra le mani."


translate italian epilogue_mi_0fd2ac9d:


    "Non ero spaventato. In realtà, non provavo proprio alcuna emozione, solo la fatica e il vuoto dentro di me."


translate italian epilogue_mi_bb76d279:


    ro "Ma… ma… come?!"


translate italian epilogue_mi_9840e224:


    "Ha borbottato Router."


translate italian epilogue_mi_429571ae:


    my "Non ne ho idea…{w} E comunque, che differenza fa? Non è che sia più strano di tutte le cose che mi sono successe negli ultimi giorni."


translate italian epilogue_mi_550ecf00:


    "Ho guardato verso di lui, esausto."


translate italian epilogue_mi_9f5fe769:


    "Le labbra di Router tremavano, un'espressione di orrore mortale era scolpita sul suo volto."


translate italian epilogue_mi_2bf4d850:


    my "Torniamo a prendere le ragazze."


translate italian epilogue_mi_ca7fc9e6:


    "Non l'ho aspettato, e mi sono diretto faticosamente verso il punto dove le avevamo lasciate."


translate italian epilogue_mi_a20cefa7_25:


    "..."


translate italian epilogue_mi_9c57ac94:


    ma "Trovato niente?"


translate italian epilogue_mi_407fb42e:


    "Ha chiesto Masha, mentre mangiava alcune fragole di origine incerta."


translate italian epilogue_mi_e5032065:


    my "Sì… un campo…"


translate italian epilogue_mi_25d0d643:


    ma "Che campo?"


translate italian epilogue_mi_efd76d15_1:


    "Sembrava sorpresa."


translate italian epilogue_mi_11995523:


    my "Il nostro… campo…"


translate italian epilogue_mi_20f54ffc_2:


    ma "Cosa?"


translate italian epilogue_mi_31b8751f:


    "Masha aveva un'espressione talmente ridicola adesso, che non sono riuscito a trattenermi dal fare un sorriso ironico."


translate italian epilogue_mi_855bda49:


    ma "Ma voi siete andati avanti, non indietro."


translate italian epilogue_mi_c6243ca7:


    my "Avanti… E «Sovyonok» è proprio lì."


translate italian epilogue_mi_7ae81d9e:


    ma "Ehi, smettila di scherzare!"


translate italian epilogue_mi_8e302a5d:


    "Ha esclamato."


translate italian epilogue_mi_c483e315:


    my "Sono serissimo…"


translate italian epilogue_mi_5a93ab85_1:


    dv "Ma…"


translate italian epilogue_mi_01ffaf07:


    "Ha detto Alisa a bassa voce."


translate italian epilogue_mi_ab26c060:


    "Stava seduta sull'erba, cullando la testa di Lena, che stava ancora dormendo."


translate italian epilogue_mi_8b206af0:


    my "Già, proprio così…"


translate italian epilogue_mi_20dcd489:


    "Ho detto stancamente, lasciandomi cadere a terra."


translate italian epilogue_mi_bf48949e:


    ma "Ok, allora significa che torneremo lì."


translate italian epilogue_mi_65d91c76:


    "Ho guardato Masha acutamente, e improvvisamente mi sono reso conto che non potevo più rimandare la nostra discussione."


translate italian epilogue_mi_de289d79:


    my "Ehi, che ti succede?"


translate italian epilogue_mi_9af1ef36:


    ma "Cosa intendi?"


translate italian epilogue_mi_6e540bea:


    "Il suo sguardo suggeriva che questo non era il momento adatto per le chiacchiere."


translate italian epilogue_mi_ebb6fb85:


    my "Ieri eri spaventata a morte, e invece oggi hai questo tuo atteggiamento del «queste cose mi accadono sempre»."


translate italian epilogue_mi_825a0cff:


    ma "Ma che senso ha farsi prendere dal panico?"


translate italian epilogue_mi_783cf09d:


    "Ha abbozzato un sorriso appena visibile."


translate italian epilogue_mi_f484fdd8:


    my "Se non ti conoscessi…"


translate italian epilogue_mi_c6b1f443:


    "Ho smesso di parlare, mi sono morso il labbro e mi sono voltato."


translate italian epilogue_mi_db691b48:


    "Ma in realtà, la conosco davvero così bene?{w} Certo, abbiamo trascorso tanti anni insieme, ma…"


translate italian epilogue_mi_e6e88a9f:


    "Dopotutto, mi è sempre importato maggiormente di me stesso.{w} O meglio, ho sempre creduto che lei fosse soddisfatta di ogni cosa."


translate italian epilogue_mi_d1b363f3:


    "E lei era sempre la stessa per me.{w} La stessa in qualunque cosa, anche quando ci eravamo lasciati. Come se non fosse una vera persona, ma il mio oggetto preferito."


translate italian epilogue_mi_cf143826:


    "Pensandoci bene, non ho proprio idea di cosa stia accadendo nella sua anima. Semplicemente non mi è mai importato!"


translate italian epilogue_mi_63ac3909:


    "Quindi, il suo comportamento in queste circostanze non è certo più bizzarro di ogni altra cosa che ci sta accadendo."


translate italian epilogue_mi_2ff98e18:


    "E ripensando a quel nostro discorso alla spiaggia due sere fa…"


translate italian epilogue_mi_2969b51f:


    "Probabilmente lei è molto più forte di me."


translate italian epilogue_mi_b28f3085:


    ma "Ehi! Mi stai ascoltando almeno?!"


translate italian epilogue_mi_e9afa089:


    my "Oh, scusa, mi sono perso nei miei pensieri. Sono successe così tante cose…"


translate italian epilogue_mi_bcebe051:


    "Mi ha guardato severamente."


translate italian epilogue_mi_3508c9b1:


    ma "Andiamo al campo! O qualunque luogo esso sia… Se è sempre lo stesso campo…"


translate italian epilogue_mi_a27cf3ba_1:


    my "D'accordo…"


translate italian epilogue_mi_6352bf59:


    "In realtà non è che avessimo altra scelta – qualche altra ora in questo caldo torrido avrebbe solo causato altre vittime…"


translate italian epilogue_mi_7241af63:


    ma "Ma dobbiamo trasportarla in qualche modo."


translate italian epilogue_mi_a516c62b:


    my "Sì… Solo un secondo, fammi pensare."


translate italian epilogue_mi_af54c066:


    "Ho trovato due bastoni abbastanza lunghi e robusti nel boschetto accanto a noi."


translate italian epilogue_mi_e3e90986:


    "Dopo essere tornato da loro, ho ordinato a Router di togliersi la camicia."


translate italian epilogue_mi_9efba5a0:


    "Ha cercato di opporsi, ma ben presto si è reso conto che era inutile discutere."


translate italian epilogue_mi_914261b2:


    "Mi sono tolto la camicia pure io, e abbiamo costruito una sorta di barella, legando il tessuto ai bastoni."


translate italian epilogue_mi_4a929c62:


    ma "Sei sicuro che reggerà?"


translate italian epilogue_mi_0f7b726d:


    "Masha ha guardato la nostra costruzione con aria perplessa, ma alla fine ha mollato un paio di forti colpi, garantendone la robustezza, e ha sorriso."


translate italian epilogue_mi_044b0bcd:


    my "Reggerà, almeno fino a quando arriveremo al campo…"


translate italian epilogue_mi_a20cefa7_26:


    "..."


translate italian epilogue_mi_5aba3686:


    "Era difficile camminare con questo caldo, ma lo è stato ancora di più trasportare i feriti – cioè, Lena."


translate italian epilogue_mi_0c231086:


    "Più volte sono quasi svenuto e mi sono quasi trovato a terra."


translate italian epilogue_mi_3c08fc74:


    "Router stava ancora peggio – sia Masha che Alisa si sono scambiate di posto con lui ogni tanto."


translate italian epilogue_mi_b8ecaed9:


    "Tuttavia, il mio orgoglio non mi ha permesso di chiedere loro di sostituirmi nemmeno per un minuto."


translate italian epilogue_mi_8915be50:


    "Certo, è stupido comportarsi così in circostanze del genere, ma non ne ho potuto fare a meno."


translate italian epilogue_mi_539b7522:


    "Ecco un altro difetto della mia personalità, oltre all'insensibilità, alla disattenzione verso gli altri – persino verso i miei cari – e altre caratteristiche decisamente poco nobili."


translate italian epilogue_mi_f81c0c30:


    "Alla fine siamo riusciti a raggiungere in qualche modo le porte del campo, e sono crollato sull'erba completamente esausto, all'ombra della statua del pioniere sconosciuto."


translate italian epilogue_mi_867157a6:


    ma "È davvero il nostro campo…"


translate italian epilogue_mi_bdc084f3:


    "Masha indugiava sulle sue parole."


translate italian epilogue_mi_0947ef24:


    ma "E in effetti, sembra proprio lo stesso."


translate italian epilogue_mi_8fa854c5:


    dv "Dobbiamo portarla in una casetta."


translate italian epilogue_mi_01ffaf07_1:


    "Ha proposto Alisa a bassa voce."


translate italian epilogue_mi_e31180f6:


    my "Già…"


translate italian epilogue_mi_c4f3ee83:


    "Mi sono alzato con notevole sforzo e mi sono preparato per l'ultima fatica."


translate italian epilogue_mi_a20cefa7_27:


    "..."


translate italian epilogue_mi_6ba66183:


    "La scelta più ovvia era la casetta di Olga Dmitrievna."


translate italian epilogue_mi_15104e27:


    "Molto probabilmente perché eravamo riusciti a trascorrere l'ultima notte lì dentro, senza troppi problemi.{w} Almeno eravamo ancora vivi…"


translate italian epilogue_mi_59348943:


    "Io e Router ci siamo lasciati cadere esausti su un letto, mentre le ragazze stavano spogliando Lena sull'altro letto."


translate italian epilogue_mi_0a658695:


    ma "Non guardate!"


translate italian epilogue_mi_72499cac:


    "Ci ha detto Masha severamente."


translate italian epilogue_mi_6731b709:


    my "Non è che ci sia qualcosa per cui valga la pena guardare…"


translate italian epilogue_mi_6eb72cdb:


    "Ho sbuffato e mi sono voltato, dando a Router una dura gomitata nelle costole, facendogli seguire il mio esempio."


translate italian epilogue_mi_e2b706b0:


    ma "Noi andiamo a prendere un po' d'acqua. E probabilmente dovremmo anche cercare qualcosa da mangiare alla mensa…"


translate italian epilogue_mi_db46de88:


    my "È pericoloso andare da soli…"


translate italian epilogue_mi_f76f1de1:


    ma "Vuoi venire con me?"


translate italian epilogue_mi_fde537c1:


    "Ha sorriso."


translate italian epilogue_mi_833e3637:


    my "No grazie… Almeno prendete quel tubo con voi."


translate italian epilogue_mi_fefef18f_1:


    "Ho detto in tono incerto."


translate italian epilogue_mi_f8f2e420:


    ma "Pensi davvero che ci sarà di aiuto?"


translate italian epilogue_mi_6450d358:


    "Ho sospirato pesantemente e ho chiuso gli occhi."


translate italian epilogue_mi_59f84e9e:


    my "Beh… Fate attenzione allora…"


translate italian epilogue_mi_cf8ac78f:


    "La porta si è chiusa dopo un secondo."


translate italian epilogue_mi_4eaf6646:


    ro "Stai dormendo?"


translate italian epilogue_mi_0fe78771:


    "Il sussurro di Router mi ha raggiunto."


translate italian epilogue_mi_863570d0:


    my "Come se si potesse dormire qui…"


translate italian epilogue_mi_a7be59ec:


    "Tutto a un tratto, ho sentito i gemiti di Lena provenire dall'altro letto."


translate italian epilogue_mi_d9d00dfc:


    "Mi sono sollevato sui gomiti e l'ho guardata."


translate italian epilogue_mi_af832e65:


    "Lena si stava rigirando sotto la coperta."


translate italian epilogue_mi_efc1edac:


    "Improvvisamente, ha spalancato gli occhi e si è voltata di scatto verso di me."


translate italian epilogue_mi_ca6611ca:


    "Un sorriso diabolico era stampato sul suo volto.{w} Lo stesso che avevo visto l'altra sera…"


translate italian epilogue_mi_3eb7004f:


    un "Fa caldo, non è vero?"


translate italian epilogue_mi_f1709333:


    "È scoppiata a ridere."


translate italian epilogue_mi_51df1151:


    "Un brivido mi è sceso lungo la schiena. La mia capacità di parlare è svanita del tutto."


translate italian epilogue_mi_8199e005:


    my "Tu… tu…"


translate italian epilogue_mi_6c21d46f_1:


    ro "Che c'è?"


translate italian epilogue_mi_586ee385:


    "Ha chiesto Router, mezzo addormentato."


translate italian epilogue_mi_f2047f00:


    "Mi sono voltato bruscamente verso di lui."


translate italian epilogue_mi_8aab4a10:


    my "Guarda!"


translate italian epilogue_mi_fe583ccc:


    "Ho agitato le mani, indicando il letto di Lena."


translate italian epilogue_mi_6c21d46f_2:


    ro "Che cosa?"


translate italian epilogue_mi_58d3724b:


    "Si è alzato e ha guardato il punto che avevo indicato, e poi ha guardato me."


translate italian epilogue_mi_49b544cf:


    "Appena mi sono voltato ho visto Lena dormire tranquillamente."


translate italian epilogue_mi_866a81a4:


    my "Non l'hai sentita?"


translate italian epilogue_mi_6c21d46f_3:


    ro "Cosa?"


translate italian epilogue_mi_9cb45ed8:


    my "La sua… Lena era sveglia… e ha detto…"


translate italian epilogue_mi_0717c0bb:


    ro "Oh, sembra che anche tu abbia preso un colpo di sole…"


translate italian epilogue_mi_4d9e42bd:


    "Si è girato verso la parete."


translate italian epilogue_mi_ea5146f0:


    "Ma sono assolutamente certo di averlo visto e sentito…"


translate italian epilogue_mi_0c0d3615:


    "Beh, può anche essere che il caldo mi abbia dato alla testa, dopotutto…"


translate italian epilogue_mi_42499ac9:


    "Ma mi è sembrata esattamente come quell'altra sera.{w} E non era Lena!"


translate italian epilogue_mi_ef3c872b:


    "Per lo meno non è la Lena che conosco! Come se fosse posseduta dal demonio!"


translate italian epilogue_mi_8209af39:


    "Mi sono sdraiato e ho tirato una coperta sopra la mia testa."


translate italian epilogue_mi_dead6866:


    "Avevo davvero voglia di dormire, ma la consapevolezza del fatto che mi sarei potuto non svegliare più se mi fossi addormentato mi stava tenendo vigile e attento."


translate italian epilogue_mi_a600ff76:


    "Non sono sicuro di quanto tempo io abbia trascorso sdraiato in quel modo, sussultando a ogni fruscio, ma alla fine la porta si è spalancata e dei passi sono risuonati nella stanza."


translate italian epilogue_mi_3cdfabf7:


    "Ho sbirciato da sotto la coperta e ho intravisto Alisa e Masha che sono entrate con un secchio pieno d'acqua e alcuni sacchi."


translate italian epilogue_mi_f6d3ba26:


    ma "Vedo che vi siete un po' rintontiti qui."


translate italian epilogue_mi_2e3f558d:


    my "Sì, qualcosa del genere…"


translate italian epilogue_mi_24d62045:


    "Ho risposto con voce tremante, cercando di fare un sorriso."


translate italian epilogue_mi_3657be4d:


    my "È tutto a posto?"


translate italian epilogue_mi_57a73d0f:


    ma "Sembra di sì."


translate italian epilogue_mi_fb21cabf:


    "Ho lanciato di nuovo uno sguardo verso Lena, ma stava ancora dormendo tranquillamente."


translate italian epilogue_mi_bfd0137f:


    "Forse è stata davvero solo la mia immaginazione?"


translate italian epilogue_mi_7f889c60:


    "Certo, considerando l'estremo stress emotivo e la fatica fisica, era plausibile che potessi avere delle allucinazioni…"


translate italian epilogue_mi_074f63d2:


    "Mi sono alzato con notevole sforzo, ho preso una tazza dal tavolo e ho raccolto un po' d'acqua."


translate italian epilogue_mi_543f3a10:


    my "Allora, quali sono i piani?"


translate italian epilogue_mi_98b3a8c1:


    ma "Per adesso dobbiamo restare qui."


translate italian epilogue_mi_2ceeb90b:


    dv "Ma Olga Dmitrievna ha chiamato la polizia…"


translate italian epilogue_mi_b75cedb9:


    "Ha mormorato Alisa."


translate italian epilogue_mi_9eab3900:


    ma "Sì, ma tutti sono scomparsi dopo che l'ha fatto."


translate italian epilogue_mi_0b0513f7:


    my "Comunque, finché Lena non si riprenderà…"


translate italian epilogue_mi_d162c751:


    "Se mai dovessi vederla nuovamente allo stesso modo di poco fa, francamente preferirei che non si riprenda affatto…"


translate italian epilogue_mi_5d8b05ff:


    "Alisa si è sdraiata sul letto di Lena, e Masha si è seduta accanto a me."


translate italian epilogue_mi_09de083e:


    ma "Dormi un po'."


translate italian epilogue_mi_4719c4e5:


    "Mi ha accarezzato delicatamente la testa."


translate italian epilogue_mi_9c51ff5e:


    my "Come potrei dormire mentre sta accadendo tutto questo…"


translate italian epilogue_mi_239bed8a:


    ma "Faremo la guardia. Sembri davvero stanco!"


translate italian epilogue_mi_3c89501d:


    "Credo che abbia ragione."


translate italian epilogue_mi_8a2a3476:


    my "Ok, ma solo un pisolino! Svegliatemi prima di sera."


translate italian epilogue_mi_43c3e662:


    "Non ha aggiunto altro, limitandosi a sorridere."


translate italian epilogue_mi_b70a39b4:


    my "Promettimelo!"


translate italian epilogue_mi_17742508:


    ma "Te lo prometto!"


translate italian epilogue_mi_1ce87451:


    "Ha risposto Masha a bassa voce."


translate italian epilogue_mi_ba17b8a7:


    "Ho chiuso gli occhi e immediatamente mi sono immerso in un sogno."


translate italian epilogue_mi_a20cefa7_28:


    "..."


translate italian epilogue_mi_934c20c6:


    "Rieccole – le cavallette."


translate italian epilogue_mi_beaa4cc0:


    "Questa volta era il loro re, un enorme mostro verde palustre con lunghe gambe storte, un'apertura alare simile a quella di un aeroplano, e un paio di enormi occhi nerissimi."


translate italian epilogue_mi_f3c2c459:


    "Era chino proprio sopra di me, sorridendo e farfugliando qualcosa ad alta voce."


translate italian epilogue_mi_507578a0:


    "Non riuscivo a capire nulla del loro linguaggio, ma a giudicare dal tono, non si trattava di niente di buono."


translate italian epilogue_mi_e7fe809f:


    "Ho cercato di scappare, ma lui mi stringeva con il suo artiglio, in modo nefasto."


translate italian epilogue_mi_4977fdd5:


    "Riuscivo a sentire le mie ossa spezzarsi, le costole rompersi, penetrando nei miei organi interni con le loro schegge taglienti."


translate italian epilogue_mi_7da28214:


    "Sentivo centinaia di martelli che mi colpivano dentro il cranio, la mia visione si è offuscata e poco dopo fontane di sangue e di carne schizzavano in tutte le direzioni…"


translate italian epilogue_mi_a20cefa7_29:


    "..."


translate italian epilogue_mi_48f17ebc:


    "Mi sono alzato di scatto sudando freddo. L'eco del mio grido risuonava ancora nella stanza."


translate italian epilogue_mi_f92e8047:


    "Mi ci sono voluti diversi secondi per mettere a fuoco ogni cosa."


translate italian epilogue_mi_6b7f9fb7:


    "Mi sono guardato attorno freneticamente e mi sono reso conto che era già sera, ed era completamente buio fuori."


translate italian epilogue_mi_d897367d:


    "Santo cielo, non avevamo spento la luce ieri..."


translate italian epilogue_mi_1d5cb079:


    "Masha giaceva addormentata accanto a me, mentre Router era rannicchiato sul pavimento."


translate italian epilogue_mi_95cddf67:


    "Lena era sdraiata sull'altro letto, avvolta stretta nella coperta."


translate italian epilogue_mi_52614955:


    "Eppure Alisa non c'era."


translate italian epilogue_mi_333411d8_2:


    my "Ehi!"


translate italian epilogue_mi_f5a3eeec:


    "Ho svegliato Masha con un paio di pizzicotti."


translate italian epilogue_mi_20f54ffc_3:


    ma "Cosa?"


translate italian epilogue_mi_6f181833:


    "Ha ripreso lentamente i sensi."


translate italian epilogue_mi_66e200e7:


    my "Non ti avevo forse chiesto...?!"


translate italian epilogue_mi_cd89b571:


    ma "Scusa… Mi sono addormentata…"


translate italian epilogue_mi_c592cc74:


    "Il suo viso esprimeva una tale colpevolezza, che mi ha fatto star male."


translate italian epilogue_mi_4ab7a146:


    "Non avrei dovuto contare su di lei.{w} Almeno avrei potuto impostare una sveglia…"


translate italian epilogue_mi_95dc5bb2:


    "Ho svegliato anche Router."


translate italian epilogue_mi_10190a88:


    my "Dov'è Alisa?"


translate italian epilogue_mi_e77cd9de:


    ma "Non lo so…"


translate italian epilogue_mi_4b1dca0c:


    "Ha risposto Masha, preoccupata."


translate italian epilogue_mi_bb853559:


    my "E allora chi dovrebbe saperlo? Stavamo dormendo tutti!"


translate italian epilogue_mi_8b687bf3:


    ma "Beh, stava dormendo lì, e poi…"


translate italian epilogue_mi_e01ba3e1:


    "I suoi occhi guizzavano da un lato all'altro della stanza, con aria colpevole."


translate italian epilogue_mi_ed2f886e:


    my "E poi ti sei addormentata!"


translate italian epilogue_mi_f5196784:


    "Ha chinato la testa e si è lasciata sfuggire un pesante sospiro."


translate italian epilogue_mi_20b8375c:


    my "Oh, grandioso, semplicemente grandioso!"


translate italian epilogue_mi_0a0f07b3:


    "Ho martellato i pugni sul tavolo, facendo sobbalzare Masha."


translate italian epilogue_mi_b0069244:


    my "Ok, resta seduta qui, andrò a cercarla!"


translate italian epilogue_mi_020772e0:


    ma "Da solo...?"


translate italian epilogue_mi_49838f90:


    "Il suo viso improvvisamente ha assunto un'espressione impaurita."


translate italian epilogue_mi_550b76e2:


    my "Sì, da solo! Non eri tu quella che mi aveva detto che non c'era ragione di farsi prendere dal panico?!"


translate italian epilogue_mi_93f232b2:


    ma "Sì, ma non…"


translate italian epilogue_mi_ff3b375f:


    my "Oh, allora bene! Andrò da solo!"


translate italian epilogue_mi_c8d6b6d8:


    ma "Ma…"


translate italian epilogue_mi_42faecaa:


    my "O vuoi davvero rimanere qui da sola con Lena?"


translate italian epilogue_mi_96cd1356:


    "Masha è rimasta in silenzio."


translate italian epilogue_mi_68e74430:


    ro "È pericoloso andare da solo…"


translate italian epilogue_mi_2dca72b2:


    "Ho notato che Router sembrava essersi risvegliato del tutto."


translate italian epilogue_mi_81495ac1:


    my "E non è forse pericoloso che lei stia qui tutta sola?"


translate italian epilogue_mi_8d4fc7d5:


    "Non era il caso di discutere, così mi sono alzato, ho afferrato il mio fidato pezzo di tubo e mi sono diretto verso la porta."


translate italian epilogue_mi_f498c94c:


    "In ogni caso, dal momento che le persone stavano scomparendo nel nulla, non c'era alcun luogo dove potersi sentire al sicuro."


translate italian epilogue_mi_3bd941f9:


    "Naturalmente, era anche possibile che Alisa fosse andata da qualche parte di sua volontà.{w} In bagno, per esempio."


translate italian epilogue_mi_cfe4baf3:


    "Anche se, a giudicare da quanto sembrava spaventata, credo che in quel caso avrebbe svegliato Masha, o semplicemente si sarebbe trattenuta fino al mattino…"


translate italian epilogue_mi_9ce03507:


    "Diamine, tutto tranne il dover vagare da solo per il campo!"


translate italian epilogue_mi_979b3743:


    "Mi sono fatto forza, ho tirato la maniglia e sono andato fuori."


translate italian epilogue_mi_e426c290:


    "Mi sono guardato attorno, e poi…"


translate italian epilogue_mi_97b95495:


    "Ho visto Alisa…{w} sospesa nell'aria, impiccata ad un albero proprio di fronte alla casetta…"


translate italian epilogue_mi_03acb541:


    "Un dolore acuto mi ha trafitto le tempie, e sono crollato a terra…"


translate italian epilogue_mi_19e8325e:


    "L'orrore è stato così travolgente che ho letteralmente sentito il mio intero corpo stringersi tra catene d'acciaio."


translate italian epilogue_mi_c71ffa7a:


    "Accecato dal terrore, sono riuscito a malapena a strisciare indietro verso la stanza e a richiudere la porta dietro di me."


translate italian epilogue_mi_78bb1682:


    ma "Cos'è successo?!"


translate italian epilogue_mi_3c910ff4:


    "Masha si è precipitata verso di me."


translate italian epilogue_mi_3d6cfafe_1:


    my "Non guardare!"


translate italian epilogue_mi_36ea64e3:


    "L'ho afferrata per un braccio."


translate italian epilogue_mi_6f6e2c1b:


    my "Non andare fuori!"


translate italian epilogue_mi_39555e25:


    ma "Qual è il problema?!"


translate italian epilogue_mi_ce032298:


    "Ha allungato la mano per raggiungere la maniglia della porta. Ho cercato di fermarla, ma ero così debole che sono riuscito solo ad emettere un gemito di disperazione."


translate italian epilogue_mi_f0dd9ecf:


    "Dopo aver aperto la porta, Masha ha urlato, pietrificata."


translate italian epilogue_mi_0e2c4200:


    "Raccogliendo le mie ultime forze, mi sono alzato e ho calciato la porta in modo da chiuderla."


translate italian epilogue_mi_fd6cf8ba:


    "L'istante seguente Masha è svenuta, cadendo nuovamente tra le mie braccia."


translate italian epilogue_mi_9b50f023:


    "L'ho riportata al suo letto e l'ho messa accanto al tremante Router."


translate italian epilogue_mi_8e5b2243:


    "La sua espressione mi suggeriva chiaramente che non era per nulla interessato a sapere cosa fosse successo."


translate italian epilogue_mi_c545824e:


    "Tutte quelle urla hanno svegliato Lena. Si è alzata faticosamente dal letto e ci ha guardati."


translate italian epilogue_mi_012448da:


    un "Che sta succedendo...?"


translate italian epilogue_mi_6c39322b:


    "Ha chiesto in un sussurro."


translate italian epilogue_mi_3f27c2c0_1:


    "Non sapevo cosa risponderle."


translate italian epilogue_mi_7a995e66:


    "Un urlo mi stava risalendo lungo la gola, ma sono riuscito a soffocarlo con un sospiro."


translate italian epilogue_mi_ed8a0e3f:


    "Lena era terrorizzata, ma non sapeva ancora cosa fosse accaduto di preciso."


translate italian epilogue_mi_e35e5625:


    my "Non… andare… fuori…"


translate italian epilogue_mi_f9065ef9:


    "Ho gemuto."


translate italian epilogue_mi_657108f7:


    "A differenza di Masha, lei non era un tipo a cui andava di discutere."


translate italian epilogue_mi_cca21832:


    "Un silenzio opprimente è sceso su di noi."


translate italian epilogue_mi_761c4b2e:


    "Persino le cavallette sembravano essersi placate, per aggiungere suspense alla situazione."


translate italian epilogue_mi_1bafb56b:


    "Masha stava ancora giacendo sul mio grembo, Router fremeva in un angolo del letto, e Lena era seduta di fronte a me con un'espressione di disperazione sul suo volto segnato."


translate italian epilogue_mi_3874b4a4:


    "Ho fissato la porta senza battere ciglio, in attesa del momento in cui si sarebbe spalancata e…"


translate italian epilogue_mi_d8767c7a:


    "La mia paura non poteva manifestarsi in nessuna forma concreta – un maniaco omicida, un mostro, un alieno…{w} Era l'orrore dell'ignoto, dell'oscurità, del vuoto assoluto."


translate italian epilogue_mi_ce811c64:


    "Non avevo paura di essere fatto a pezzi come Slavya, o di diventare uno zombie come Ulyana."


translate italian epilogue_mi_170b2003:


    "Avevo paura di sparire all'improvviso, di evaporare da questo mondo senza lasciar traccia…"


translate italian epilogue_mi_e374bab2:


    "Masha si è lasciata sfuggire un lamento silenzioso."


translate italian epilogue_mi_4fce6de4:


    ma "Ho appena fatto un sogno terribile…"


translate italian epilogue_mi_908757cc:


    my "Non era un sogno…"


translate italian epilogue_mi_bb759ffc:


    "Ha spalancato gli occhi e mi ha guardato terrorizzata."


translate italian epilogue_mi_198e3f4c:


    ma "Allora…là fuori?!"


translate italian epilogue_mi_3d6cfafe_2:


    my "Non guardare!"


translate italian epilogue_mi_741958cd:


    "Le ho gridato, abbracciandola all'istante."


translate italian epilogue_mi_b2cd5a0c:


    "Lena ha iniziato a piangere forte, è scesa dal letto e si è stretta a me, tutta tremante."


translate italian epilogue_mi_bd486098:


    "Lunghi minuti si sono trascinati in un silenzio angosciante."


translate italian epilogue_mi_282e0eee:


    ma "Allora… cosa facciamo adesso?"


translate italian epilogue_mi_10a44f66:


    "Ha chiesto Masha in lacrime."


translate italian epilogue_mi_b44a635d:


    "Adesso non sembrava più così sicura di sé.{w} Ad ogni modo, eravamo tutti nella stessa situazione…"


translate italian epilogue_mi_777a7686:


    "Il tempo passava, ma non è successo altro."


translate italian epilogue_mi_af951e50:


    "Dovevamo prendere una decisione."


translate italian epilogue_mi_104c4b70:


    "Non posso dire di essermi ripreso del tutto dopo quello che avevo appena visto, ma la mia mente stava diventando un po' più lucida."


translate italian epilogue_mi_bf92a608:


    "Di una cosa ero sicuro – non potevamo restare qui.{w} E non perché fosse più pericoloso stare in questa stanza piuttosto che altrove…!"


translate italian epilogue_mi_b1129cb1:


    "Ma solo perché non riuscivo a sopportare di dover stare a pochi metri dal cadavere di Alisa."


translate italian epilogue_mi_81f3f410:


    "Dev'essere un'esperienza ancora più straziante per le ragazze..."


translate italian epilogue_mi_5ec6958d:


    my "Dobbiamo andarcene da qui…"


translate italian epilogue_mi_5788b9db:


    "Ho detto a bassa voce."


translate italian epilogue_mi_2f93c801:


    "Masha e Lena mi stringevano ancora più forte."


translate italian epilogue_mi_7cabd0eb:


    ro "Dobbiamo…"


translate italian epilogue_mi_48701c6a:


    "Router ha smesso di tremare e si è alzato dal letto."


translate italian epilogue_mi_32912129:


    my "Come ti senti?"


translate italian epilogue_mi_3329e7d9:


    "Ho chiesto a Lena."


translate italian epilogue_mi_d00d3d0f:


    "Non mi ha risposto."


translate italian epilogue_mi_95fe422d:


    my "Riesci a camminare?"


translate italian epilogue_mi_3c68ea6c:


    un "Forse…"


translate italian epilogue_mi_5d37a806:


    "Mi sono liberato delicatamente dai loro abbracci e mi sono alzato."


translate italian epilogue_mi_f90fc978:


    my "Dobbiamo correre! Di notte sarà più facile andare."


translate italian epilogue_mi_d58a29ab:


    ma "Ma…"


translate italian epilogue_mi_8f19793e:


    "Masha ha cercato di fare un'umile obiezione."


translate italian epilogue_mi_73866edf:


    my "Sì, lo so che abbiamo deciso di rimanere, ma… Ora dobbiamo uscire di qui!"


translate italian epilogue_mi_951f9710:


    "Mi sono rivolto a Router."


translate italian epilogue_mi_ef2e4fda:


    my "Capisci che… loro non dovrebbero vederlo."


translate italian epilogue_mi_a91e5c2f:


    "Ha annuito quasi impercettibilmente."


translate italian epilogue_mi_2aa79f94:


    "Anche se Router non sapeva che cosa fosse successo esattamente ad Alisa, era ovvio che la sua fervida immaginazione avesse ormai evocato talmente tante teorie raccapriccianti, che probabilmente lui era ancora più depresso di me."


translate italian epilogue_mi_7037899d:


    my "Ok, ci terremo per mano e andremo avanti. Io andrò per primo e vi farò strada. Masha sarà la seconda, poi Lena, e infine tu."


translate italian epilogue_mi_6b024c96:


    "Ho guardato le ragazze."


translate italian epilogue_mi_dd76c6a4:


    "Stavano sedute con la testa china, singhiozzando in silenzio."


translate italian epilogue_mi_549e20f2:


    my "Pronti?"


translate italian epilogue_mi_c5d5810d:


    "Nessuno ha risposto – ovviamente era una domanda inutile – non si può mai essere pronti per una situazione del genere."


translate italian epilogue_mi_35423a3b:


    "Ho preso Masha per mano, mi sono girato e ho guardato Lena e Router in modo serio."


translate italian epilogue_mi_f91862ca:


    my "Ok, ora chiudete gli occhi e non apriteli per nessuna ragione al mondo, capito?"


translate italian epilogue_mi_44cacce6_2:


    ro "Sì…"


translate italian epilogue_mi_f7a66aa8:


    "Lena ha annuito."


translate italian epilogue_mi_7983c203:


    "Mi sono fatto coraggio, ho spalancato la porta e sono uscito, tenendo Masha per mano."


translate italian epilogue_mi_eec4cf2b:


    my "Attenti agli scalini!"


translate italian epilogue_mi_f298ccdd:


    "Appena siamo usciti, ho lanciato un'ultima occhiata verso Alisa."


translate italian epilogue_mi_bb16154d:


    "Quello che vedevo in questo momento non mi ha fatto crollare a terra, anche se il mio cuore batteva all'impazzata, mi sentivo male, avevo le vertigini e mi veniva da vomitare."


translate italian epilogue_mi_0573dd61:


    "Lei era sospesa a circa cinque metri da terra. Diversi foulard legati insieme a mo' di corda erano stretti attorno al suo collo."


translate italian epilogue_mi_cdda0258:


    "Gli occhi sporgenti, la lingua di fuori, la putrefazione già evidente sul suo corpo – sembrava che non fosse morta da un paio d'ore, ma almeno da un paio di giorni."


translate italian epilogue_mi_c76687ee:


    "Un'espressione di infinita agonia e disumana sofferenza era stampata sul suo volto."


translate italian epilogue_mi_a33a94a5:


    "Era chiaro che non avrebbe potuto fare una cosa del genere a sé stessa."


translate italian epilogue_mi_788c07e6:


    "Mio Dio, chi può aver fatto tutto questo? Siamo forse all'inferno...?"


translate italian epilogue_mi_aa8da201:


    "Ho affrettato il mio passo lontano da quella scena diabolica, trascinandomi dietro tutti gli altri."


translate italian epilogue_mi_a20cefa7_30:


    "..."


translate italian epilogue_mi_912b3321:


    "Ho ordinato loro di aprire gli occhi solo quando avevamo raggiunto la piazza."


translate italian epilogue_mi_d2d78eec:


    ma "Lei è… è ancora lì?"


translate italian epilogue_mi_55b4c0e5:


    "Ha chiesto Masha, senza lasciar andare la mia mano."


translate italian epilogue_mi_413cf259:


    "Mi sono limitato ad annuire."


translate italian epilogue_mi_8c308047:


    un "Chi?! Alisa?!"


translate italian epilogue_mi_c6b1fe3f:


    "Ha gridato forte Lena, scoppiando in lacrime."


translate italian epilogue_mi_6e61e470:


    "Masha l'ha abbracciata, cercando di confortarla."


translate italian epilogue_mi_5d58c3b8:


    ro "Cosa le è successo?"


translate italian epilogue_mi_54683d6b:


    "Ha borbottato Router, vacillante."


translate italian epilogue_mi_50c8a92f:


    my "È meglio che tu non lo sappia."


translate italian epilogue_mi_290e56ee:


    "Sono rimasto lì in piedi per un po', osservando la luna."


translate italian epilogue_mi_ac9a276c:


    "Lena singhiozzava, Router camminava in cerchio attorno a noi, Masha si è seduta su una panchina e ha seppellito il viso tra le mani."


translate italian epilogue_mi_f077aeea:


    my "Ok, è giunto il momento…"


translate italian epilogue_mi_54ac6199:


    "Ho detto a bassa voce, ma nessuno si è mosso."


translate italian epilogue_mi_727a919f:


    my "Non possiamo rimanere qui!"


translate italian epilogue_mi_38a7da59:


    "Sono andato verso Masha, mi sono chinato e l'ho presa per mano."


translate italian epilogue_mi_2178548f:


    "Ha alzato il suo sguardo ricoperto di lacrime e ha annuito."


translate italian epilogue_mi_891afcb1:


    "Ci siamo incamminati lentamente verso la fermata dell'autobus…"


translate italian epilogue_mi_a20cefa7_31:


    "..."


translate italian epilogue_mi_d6e969ac:


    "La notte è calata sul campo."


translate italian epilogue_mi_2b88b9e3:


    "Durante il tragitto, ho notato che le cavallette erano quasi tranquille."


translate italian epilogue_mi_4d1b313b:


    "Dopo essere restate in silenzio per un po' di tempo, ora avevano ricominciato la loro fastidiosa sinfonia, ma questa volta devo ammettere che era molto meno opprimente, più silenziosa e, si potrebbe dire, un po' più rispettosa."


translate italian epilogue_mi_24889ac6:


    "La loro musica progressiva diurna è stata sostituita da un Notturno per violino e pianoforte."


translate italian epilogue_mi_af1d202f:


    "C'era forse qualche possibilità che fossero consapevoli di quello che stava accadendo qui...?"


translate italian epilogue_mi_5a24d3d5:


    "Naturalmente, supponendo che si trattasse di cavallette infernali, era possibile…"


translate italian epilogue_mi_efbb6c03:


    "Siamo arrivati alla fermata del bus e ci siamo fermati, esitando."


translate italian epilogue_mi_b2bc2a02:


    ma "E adesso dove?"


translate italian epilogue_mi_e3229b58:


    "Ha chiesto Masha, asciugandosi le lacrime."


translate italian epilogue_mi_1ca97523:


    my "Ok, abbiamo già provato ad andare a destra… Adesso andiamo a sinistra."


translate italian epilogue_mi_bf0ef4ba:


    ma "Ma non c'è nessuna città da quella parte…"


translate italian epilogue_mi_c9afa297:


    my "E solo pochi giorni fa non c'era un campo di pionieri a destra…"


translate italian epilogue_mi_b4e45382:


    un "Forse non dovremmo…"


translate italian epilogue_mi_49be4260:


    "Lena singhiozzava in silenzio."


translate italian epilogue_mi_f2ab6c8a:


    my "Non abbiamo altra scelta… Non ho intenzione di restare in questo luogo!"


translate italian epilogue_mi_93a07a2a:


    "Eravamo sul punto di incamminarci quando abbiamo sentito dei rumori avvicinarsi in lontananza."


translate italian epilogue_mi_bb6a2944:


    "Ho stretto gli occhi e ho intravisto qualcuno che si stava dirigendo verso di noi lungo la strada."


translate italian epilogue_mi_cbc33115:


    ro "Un autobus!"


translate italian epilogue_mi_2b6b2a9a:


    "Ha esclamato Router con gioia."


translate italian epilogue_mi_89a22708:


    my "Zitto, stupido! Quello non è un autobus! Delle persone si stanno avvicinando!"


translate italian epilogue_mi_6f0bbf9c:


    "Gli ho sibilato."


translate italian epilogue_mi_aea41fc4:


    ma "Una marea di persone…"


translate italian epilogue_mi_c67e1ffd:


    "Ha detto Masha ansiosamente."


translate italian epilogue_mi_f5f53cd8:


    "Ormai era chiaro che dovevamo andarcene da lì."


translate italian epilogue_mi_353f7742:


    "Mi sono voltato e ho visto anche qualcuno che si stava avvicinando lungo la strada dalla direzione opposta.{w} Il rumore stava diventando sempre più forte."


translate italian epilogue_mi_a3e8eadb:


    "Lo zillare di cavallette... Anche se non era esattamente quello – era una versione distorta molto più forte, come se passasse attraverso un amplificatore per chitarra."


translate italian epilogue_mi_1f4d46ef:


    my "Correte!"


translate italian epilogue_mi_c34e863e:


    "Ho gridato, ma nessuno si è mosso."


translate italian epilogue_mi_b6e2867e:


    my "Oh, al diavolo!"


translate italian epilogue_mi_f6db9d2f:


    "Ho afferrato Masha e Lena per mano e mi sono precipitato verso il campo."


translate italian epilogue_mi_f19be6a7:


    "Le ragazze erano come ipnotizzate, e così ho dovuto trascinarmele dietro letteralmente."


translate italian epilogue_mi_fae82ef7:


    "Dopo essere arrivati alla piazza, mi sono fermato per riprendere fiato, e solo allora mi sono reso conto che avevamo lasciato indietro Router."


translate italian epilogue_mi_b1c873bb:


    ma "Dobbiamo tornare a prenderlo!"


translate italian epilogue_mi_41f008ff:


    "Ha urlato Masha, precipitandosi verso la fermata dell'autobus."


translate italian epilogue_mi_a900b142:


    my "Ma sei pazza?!"


translate italian epilogue_mi_f149054d:


    "L'ho tenuta stretta per mano, costringendola a fermarsi."


translate italian epilogue_mi_c95121c1:


    my "Vuoi che facciamo tutti la sua stessa fine?!"


translate italian epilogue_mi_3d0e96c9:


    "Lena tremava così selvaggiamente che la mia mano destra fremeva insieme a lei."


translate italian epilogue_mi_27fc3c78:


    my "Continuate a correre!"


translate italian epilogue_mi_3a8faa33:


    ma "E dove?!"


translate italian epilogue_mi_d149a305:


    my "Non lo so, nella foresta!"


translate italian epilogue_mi_b84b36c8:


    "Tornare nella stanza di Olga Dmitrievna non era un'opzione valida."


translate italian epilogue_mi_2e4f2c35:


    "Il mio istinto di autoconservazione mi stava anche suggerendo che non saremmo dovuti restare nemmeno all'aperto."


translate italian epilogue_mi_4215b313:


    "Ci siamo precipitati verso la foresta."


translate italian epilogue_mi_4716024f:


    "Tenevo strette le mani delle due ragazze."


translate italian epilogue_mi_0e01cbac:


    "Correvano con difficoltà, soprattutto Lena, che stava oscillando e dondolando. È caduta un paio di volte, ma l'ho subito aiutata a rialzarsi, spronandola a suon di grida, e trascinandomela dietro."


translate italian epilogue_mi_b9aff25b:


    "Passare attraverso gli alberi è stata la parte più difficile."


translate italian epilogue_mi_30941c88:


    "Anche se la luna piena splendeva nel cielo, nell'oscurità della foresta continuavo a incappare in rocce, rami, intoppi e fosse."


translate italian epilogue_mi_90e827b2:


    "Le ortiche stavano martoriando le mie gambe e le foglie mi colpivano in faccia, causandomi dei graffi dolorosi."


translate italian epilogue_mi_2795f897:


    "Il mio cuore batteva all'impazzata. Ogni respiro era un doloroso fischio nei miei polmoni. Il sangue mi pompava così forte nella testa che sembrava che il mio cranio fosse pronto a esplodere da un momento all'altro, a causa della pressione."


translate italian epilogue_mi_5379c1e1:


    "Ho smesso di percepire il mio corpo – le mie gambe mi portavano avanti da sole."


translate italian epilogue_mi_1df107da:


    "Finalmente abbiamo raggiunto una radura e ci siamo fermati."


translate italian epilogue_mi_2a7f7693:


    "Esauste, le ragazze sono crollate a terra, mentre io mi sono seduto su un ceppo d'albero."


translate italian epilogue_mi_274d0040:


    "Chi erano quelli? Persone o demoni? O qualcos'altro?"


translate italian epilogue_mi_a42af439:


    "E cos'era quel rumore infernale, da dove proveniva?"


translate italian epilogue_mi_53300ec8:


    "Non sono riuscito a vederli esattamente a causa del buio, ma forse è meglio così."


translate italian epilogue_mi_b8132fa2:


    "Dopo aver ripreso un po' i sensi, mi sono avvicinato alle ragazze."


translate italian epilogue_mi_1b7c0a78:


    "Lena giaceva sul terreno, impallidita a morte. Sembrava che avesse completamente perso la ragione."


translate italian epilogue_mi_9cc83bd6:


    "Masha stava seduta, abbracciandosi le gambe e dondolando da un lato all'altro."


translate italian epilogue_mi_60b21c95:


    my "Dobbiamo fare qualcosa…"


translate italian epilogue_mi_38896a66:


    "Ho detto, dopo essermi seduto accanto a lei."


translate italian epilogue_mi_51e97271:


    ma "È troppo tardi… Moriremo tutti…"


translate italian epilogue_mi_5a9cdae9:


    "Ha sussurrato con voce cupa."


translate italian epilogue_mi_226dce17:


    my "Siamo ancora vivi, il che significa che c'è ancora speranza."


translate italian epilogue_mi_0a647cc5:


    ma "No, non c'è più niente ormai… È finita…"


translate italian epilogue_mi_872ed3da:


    "L'ho abbracciata e l'ho stretta forte."


translate italian epilogue_mi_85ff07ec:


    my "No, non è finita…"


translate italian epilogue_mi_a20cefa7_32:


    "..."


translate italian epilogue_mi_f54fc1f7:


    "Mi sono seduto, fissando il buio della foresta."


translate italian epilogue_mi_cb4ba357:


    "Le cavallette erano udibili a malapena adesso."


translate italian epilogue_mi_dda9e6e8:


    "Se non fosse per il respiro affannoso di Masha, si potrebbe pensare che fosse morta."


translate italian epilogue_mi_661c87b5:


    ma "Perdonami…"


translate italian epilogue_mi_c1e898ec:


    "Ha sussurrato a bassa voce."


translate italian epilogue_mi_9a8df94b:


    my "Per cosa?"


translate italian epilogue_mi_ae39ac0b:


    ma "Per tutto… Per essere sempre così… Per tutto quello che non siamo riusciti a ottenere…"


translate italian epilogue_mi_256eea8e:


    my "Non è il momento giusto per queste cose…"


translate italian epilogue_mi_0fd16807:


    "Le ho detto teneramente."


translate italian epilogue_mi_2b77fde2:


    ma "Non ci rimane molto tempo… Quindi direi che è il momento giusto."


translate italian epilogue_mi_403d5760:


    "Mi ha guardato, sorridendo debolmente."


translate italian epilogue_mi_4b94df60:


    ma "Perdonami, se solo non fossi stata così egoista…"


translate italian epilogue_mi_d16187bb:


    "L'ho guardata negli occhi, i suoi occhi dolenti senza fondo, e ho visto una calda lacrima scenderle lungo la guancia."


translate italian epilogue_mi_2110e4c0:


    my "No, sono io colui che dovrebbe scusarsi…"


translate italian epilogue_mi_a70dec20:


    "Non avevo assolutamente idea di cosa dire in una situazione del genere."


translate italian epilogue_mi_a84db421:


    "Le avevo fatto così male, le avevo causato così tanto dolore e ora, all'ultimo minuto, non riesco a pensare a qualcosa di diverso da un vuoto «mi dispiace»."


translate italian epilogue_mi_852082f5:


    "Sono stato sopraffatto dalla rabbia per la mia impotenza, per non essere in grado di proteggere l'unica persona che mi era cara."


translate italian epilogue_mi_bf98df2f:


    my "Sai, ti ho sempre amata…"


translate italian epilogue_mi_f538b416:


    ma "Lo so…"


translate italian epilogue_mi_a68bf534_1:


    "Ha detto a bassa voce."


translate italian epilogue_mi_7a73e50c:


    "Masha ha sorriso di nuovo, anche se il suo viso tremava e le lacrime scorrevano lungo le sue guance."


translate italian epilogue_mi_8cbe7e05:


    my "E ti amo ancora…"


translate italian epilogue_mi_f538b416_1:


    ma "Lo so…"


translate italian epilogue_mi_6a903ceb:


    "Non riuscivo a sopportarlo – volevo allontanarmi, chiudere gli occhi…"


translate italian epilogue_mi_8e9b5288:


    "Ma non posso cedere alla debolezza in questi ultimi minuti!"


translate italian epilogue_mi_d1d01520:


    "Mi sono chinato e l'ho baciata focosamente."


translate italian epilogue_mi_99de19ed:


    "Il tempo per noi si è fermato.{w} Questo maledetto campo, la notte oscura, tutte quelle creature infernali, e persino le dannate cavallette hanno semplicemente cessato di esistere!"


translate italian epilogue_mi_b67167b0:


    "L'intero universo ha cessato di esistere – siamo rimasti solo noi due…"


translate italian epilogue_mi_ce617998_2:


    "…"


translate italian epilogue_mi_daa91f05:


    "Proprio quando ero quasi disposto a credere che fosse tutto solo un brutto sogno, la foresta ha preso vita con un fruscio."


translate italian epilogue_mi_79516c27:


    "Il fogliame ha tremato a distanza, e uno sciame di vaghe sagome è emerso dal buio."


translate italian epilogue_mi_57818ffd:


    "Non riuscivo ancora a vederle in dettaglio, ma ero certo che si trattasse delle stesse «creature» che avevamo visto prima alla fermata dell'autobus."


translate italian epilogue_mi_aa083e71:


    "L'intero mio corpo si è bloccato. Avevo timore di fare anche solo un singolo respiro."


translate italian epilogue_mi_58343c59:


    ma "E quindi, è così che finirà…"


translate italian epilogue_mi_d8b53ec6:


    "Ha sussurrato Masha, stringendomi forte."


translate italian epilogue_mi_6bd68c39:


    "No, questa non è la fine!"


translate italian epilogue_mi_198fcb2e:


    "Improvvisamente ho capito che dovevo lottare per lei fino all'ultima goccia del mio sangue."


translate italian epilogue_mi_1139d9cb:


    "Finché c'era una minima possibilità di sopravvivenza!"


translate italian epilogue_mi_c7c16c8a:


    "Mi sono alzato di scatto, l'ho tirata su e mi sono precipitato verso Lena."


translate italian epilogue_mi_118fa0c9:


    "Era sdraiata, terribilmente pallida, con gli occhi spalancati, e sembrava non riuscisse a capire cosa stesse accadendo attorno a lei."


translate italian epilogue_mi_29d61edf:


    "Ho iniziato a scuoterla per le spalle."


translate italian epilogue_mi_97a2b531:


    my "Alzati!"


translate italian epilogue_mi_7e5612ad:


    "Nessuna reazione."


translate italian epilogue_mi_fefd014c:


    "Le ho dato un paio di schiaffi, e lei si è lasciata sfuggire un sordo lamento."


translate italian epilogue_mi_09ccb2a2:


    my "Alzati! Subito!"


translate italian epilogue_mi_27ba2774:


    "Mentre la scuotevo, lei mi fissava con espressione sbalordita."


translate italian epilogue_mi_1f4d46ef_1:


    my "Corriamo!"


translate italian epilogue_mi_e900bc14:


    "Ho afferrato le ragazze per le mani e mi sono precipitato verso l'ignoto, il più lontano possibile da «quelle cose»."


translate italian epilogue_mi_7a43a1d3:


    "Inciampavo, mi rialzavo, aiutavo Lena e Masha a rialzarsi, facendomi strada attraverso il fitto fogliame. Alla fine, il rumore si è placato, lasciandoci da soli con il silenzioso zillare delle cavallette."


translate italian epilogue_mi_1464e77a:


    "La luce ha brillato in lontananza."


translate italian epilogue_mi_a20cefa7_33:


    "..."


translate italian epilogue_mi_2bf52109:


    "Ben presto siamo arrivati a una piccola radura, completamente circondata dagli alberi. C'era un edificio che assomigliava ad un asilo."


translate italian epilogue_mi_74cb7d4d:


    my "Cos'è quello?"


translate italian epilogue_mi_46835ec1:


    "Ho balbettato."


translate italian epilogue_mi_809c3b70:


    un "L'edificio del vecchio campo…"


translate italian epilogue_mi_0cb7a1ff:


    "Ha detto Lena debolmente."


translate italian epilogue_mi_bb7631a3:


    "Pareva che stesse tornando in sé."


translate italian epilogue_mi_7c3c1f20:


    "Mi sono fermato, esitando."


translate italian epilogue_mi_153ace4d:


    "Certo, era abbastanza ingenuo pensare che avremmo potuto trovare rifugio là dentro – era ovvio che nessun luogo potesse essere sicuro per noi, considerando tutto quello che stava accadendo."


translate italian epilogue_mi_aa5cdda1:


    "D'altra parte, eravamo sul punto di crollare.{w} Soprattutto le ragazze."


translate italian epilogue_mi_4015903e:


    "Ho fatto un paio di passi incerti verso l'edificio, ho sentito nuovamente il fruscio provenire da dietro. Sono rimasto immobile, incapace di voltarmi."


translate italian epilogue_mi_84eb76e0:


    "Il rumore stava diventando sempre più forte, e improvvisamente abbiamo sentito il solito rumore diabolico."


translate italian epilogue_mi_1aaec2c1:


    "Con sforzo sovrumano, mi sono voltato."


translate italian epilogue_mi_e9aee5ea:


    "Dietro di noi c'era una folla…{w} Una folla di piccole ragazzine."


translate italian epilogue_mi_dc80e866:


    "O meglio, una folla di Ulyane…"


translate italian epilogue_mi_fddbd92f:


    "Erano tutte esattamente uguali all'Ulyana che avevo visto stamattina – con la stessa faccia lacerata e lo stesso sorriso terribilmente distorto."


translate italian epilogue_mi_9e72eb1c:


    "E tutte loro emettevano lo stesso suono infernale, lo stesso zillo, un rumore diabolico che mi ha gelato il sangue."


translate italian epilogue_mi_52648267:


    "Siamo rimasti pietrificati, incapaci di fare un singolo passo."


translate italian epilogue_mi_b22355a1:


    "Tutto ad un tratto, una delle «Ulyane» si è avvicinata a noi dalla folla e…{w} ha tirato fuori la testa mozzata di Router da dietro la sua schiena."


translate italian epilogue_mi_f58f26f9:


    "Una smorfia di infinito orrore era incisa permanentemente sul suo volto."


translate italian epilogue_mi_67e4b031:


    "Sono quasi svenuto quando lei ha iniziato a parlare:"


translate italian epilogue_mi_24eb79a9:


    us "Ehilà, Semyon! Come te la passi?"


translate italian epilogue_mi_64c1dacf:


    "La sua voce era come un proiettile che mi è penetrato dritto nel cervello. Ho urlato e sono fuggito verso il vecchio edificio, trascinandomi dietro le ragazze."


translate italian epilogue_mi_85b7581c:


    "Devo ammettere che in quel momento non m'importava di Masha e Lena."


translate italian epilogue_mi_6fa07538:


    "Forse anche loro stavano urlando, forse anche loro erano state avvolte dal terrore – non lo so."


translate italian epilogue_mi_e3a0e8e4:


    "La cosa più importante ora era andarcene il più lontano possibile da quei mostri."


translate italian epilogue_mi_2da23a26:


    "Siamo balzati dentro l'edificio, e improvvisamente il terreno è scomparso sotto ai nostri piedi e siamo caduti nell'oscurità…"


translate italian epilogue_mi_a20cefa7_34:


    "..."


translate italian epilogue_mi_a20cefa7_35:


    "..."


translate italian epilogue_mi_6c66893a:


    "Mi sono risvegliato nel buio più assoluto."


translate italian epilogue_mi_a490ddde:


    "Mi faceva male dappertutto, soprattutto le gambe, ma in qualche modo sono riuscito ad alzarmi, tenendomi appoggiato alla parete."


translate italian epilogue_mi_20b70770:


    "Il suono delle «Ulyane» era sparito, il che mi ha calmato un po'."


translate italian epilogue_mi_7f05575c:


    "Ho chiamato le ragazze con un filo di voce:"


translate italian epilogue_mi_02b36d0b:


    my "Masha… Lena…"


translate italian epilogue_mi_4fc4bd86:


    "Ho percepito un movimento accanto a me, e ho sentito qualcuno afferrarmi la gamba."


translate italian epilogue_mi_23250185:


    "Ho istintivamente fatto un balzo indietro, ma nello stesso momento ho sentito una debole voce provenire da terra:"


translate italian epilogue_mi_5854ece9:


    ma "Siamo ancora vivi...?"


translate italian epilogue_mi_00cfd6e5:


    my "Pare di sì…"


translate italian epilogue_mi_a313a700:


    "Ho aiutato Masha rialzarsi."


translate italian epilogue_mi_c8171111:


    "Improvvisamente, una bagliore luminoso è apparso a un paio di metri di distanza da noi."


translate italian epilogue_mi_97874b9d:


    "Mi sono appoggiato al muro, tremando, ma ho subito capito che si trattava di Lena, che aveva acceso una torcia elettrica. Da dove l'ha presa?"


translate italian epilogue_mi_9341d7f5:


    my "Dove l'hai trovata...?"


translate italian epilogue_mi_70effa3b:


    "Lena si è avvicinata, zoppicando."


translate italian epilogue_mi_8f0774a8:


    un "L'avevo portata con me per sicurezza… ieri…"


translate italian epilogue_mi_956b67a0:


    "Ho preso la torcia e ho scansionato l'area."


translate italian epilogue_mi_1fddf6ec:


    "Eravamo in una sorta di tunnel – le pareti erano rivestite di cavi, le lampade spente pendevano dal soffitto."


translate italian epilogue_mi_64a7a549:


    ma "Dove siamo?"


translate italian epilogue_mi_365e331e:


    my "Non lo so… Sembra una sorta di rifugio antiaereo…"


translate italian epilogue_mi_cf6427db:


    "Ho alzato lo sguardo e ho visto un buco decisamente enorme, oltre il quale si poteva a malapena distinguere il corridoio del vecchio edificio."


translate italian epilogue_mi_9b78bde9:


    "Probabilmente il pavimento era marcio, e siamo caduti qui sotto."


translate italian epilogue_mi_02fb066a:


    "A prima vista, non c'era modo di risalire – era troppo alto, e non c'è nulla su cui potersi arrampicare."


translate italian epilogue_mi_b5a405a0:


    my "Allora, cosa facciamo, proseguiamo?"


translate italian epilogue_mi_bbde6bdd:


    "Ho chiesto, senza aspettarmi alcuna risposta."


translate italian epilogue_mi_ec980db8:


    ma "Non lo so… Ma dovremmo controllare."


translate italian epilogue_mi_2264a05e:


    "Ho puntato la torcia davanti a noi e ho fatto qualche passo incerto."


translate italian epilogue_mi_bfec5f9c:


    "Ero completamente ricoperto di lividi e graffi, ma potevo ancora camminare."


translate italian epilogue_mi_4ef21e5f:


    un "Ahia…"


translate italian epilogue_mi_bc66543c:


    "Ha gridato Lena dietro di me."


translate italian epilogue_mi_1d810d31_1:


    my "Cosa c'è?"


translate italian epilogue_mi_4bad9ddd:


    un "Credo di essermi slogata la caviglia…"


translate italian epilogue_mi_d2070307:


    "Sinceramente mi aspettavo che la frase seguente fosse il solito cliché in stile «andate avanti senza di me, sarò solo un peso per voi», ma ovviamente non avrebbe mai potuto dire una cosa del genere."


translate italian epilogue_mi_7f15c298:


    my "Va bene, appoggiati a me."


translate italian epilogue_mi_b462a513:


    "Mi sono avvicinato a Lena e le ho offerto la mia spalla."


translate italian epilogue_mi_2d5feb7b:


    "Ben presto le pareti di pietra sono diventate di legno, e siamo finiti in una specie di miniera."


translate italian epilogue_mi_ff0e72ed:


    ma "Facciamo una pausa!"


translate italian epilogue_mi_347e63c2:


    "Ha implorato Masha."


translate italian epilogue_mi_fd1bdbfc:


    "Ho accompagnato cautamente Lena verso terra e mi sono seduto accanto a lei."


translate italian epilogue_mi_b1f368d2:


    my "Beh, almeno quei mostri sembrano essere spariti…"


translate italian epilogue_mi_710a57f3:


    ma "Ma per quanto ancora…?"


translate italian epilogue_mi_6450d358_1:


    "Ho sospirato pesantemente e ho chiuso gli occhi."


translate italian epilogue_mi_b587d808:


    ma "Cosa pensi che stia succedendo qui?"


translate italian epilogue_mi_49190220:


    "La voce di Masha tremava un po', ma suonava comunque più o meno sicura."


translate italian epilogue_mi_7bfb3264:


    "Sembra che sia così esausta e sfinita da non avere più alcuna forza per avere paura."


translate italian epilogue_mi_576daca7:


    my "Non lo so… E onestamente, non lo voglio sapere!"


translate italian epilogue_mi_18e5c515:


    ma "Ma… riusciremo a uscire?"


translate italian epilogue_mi_6731092c:


    my "Certamente!"


translate italian epilogue_mi_0ffb9fba:


    "Non riuscivo a vedere il suo viso, ma ero sicuro che stesse sorridendo."


translate italian epilogue_mi_e5153f6b:


    "Improvvisamente la torcia ha lampeggiato e poi si è spenta del tutto."


translate italian epilogue_mi_1c74b1ee:


    "L'ho colpita più volte col palmo della mano, le pareti della miniera si sono illuminate nuovamente con una luce fioca, e ho immediatamente capito che qualcosa non andava!"


translate italian epilogue_mi_df6b624b:


    "Lena non c'era più!"


translate italian epilogue_mi_6ec38792:


    my "Lena è sparita…"


translate italian epilogue_mi_9db6b90d:


    "Ho sussurrato."


translate italian epilogue_mi_20f54ffc_4:


    ma "Che cosa?"


translate italian epilogue_mi_cfb3fc4f:


    "Masha mi ha afferrato per la manica e ha fissato il punto in cui Lena era seduta un momento fa."


translate italian epilogue_mi_a4a54acd:


    ma "Ma? Come?"


translate italian epilogue_mi_72683e3e:


    "Ha iniziato a piagnucolare sommessamente."


translate italian epilogue_mi_83e118dc:


    ma "Moriremo… Moriremo di sicuro!"


translate italian epilogue_mi_6fca0341:


    "Anch'io avevo davvero voglia di scoppiare a piangere."


translate italian epilogue_mi_0b13bed1:


    "L'oscurità ci ha circondato, come in una morsa."


translate italian epilogue_mi_784f3191:


    "La torcia stava lentamente morendo – e con essa, le nostre possibilità di sopravvivenza."


translate italian epilogue_mi_57aee98a:


    my "Alzati, dobbiamo andare. La batteria si sta scaricando."


translate italian epilogue_mi_ed94e63d:


    "Un paio di minuti più tardi siamo arrivati ad un bivio."


translate italian epilogue_mi_dd3ac820:


    my "Che diavolo è, una specie di labirinto?"


translate italian epilogue_mi_2eb0d079:


    "Ho sibilato sottovoce."


translate italian epilogue_mi_0238f96f:


    ma "Torniamo indietro?"


translate italian epilogue_mi_a595d43b:


    my "No. Non se ne parla."


translate italian epilogue_mi_a3a11b35:


    "Masha si è stretta contro di me ancora più forte."


translate italian epilogue_mi_4ea6e0a7:


    ma "Allora dove?"


translate italian epilogue_mi_326ad2f9_2:


    my "Non lo so…"


translate italian epilogue_mi_f7db3185:


    "Ma dovevamo scegliere in ogni caso."


translate italian epilogue_mi_d59a939e:


    "E ho deciso di andare a destra..."


translate italian epilogue_mi_ab9caf40:


    "Ben presto siamo arrivati a un altro bivio."


translate italian epilogue_mi_5dea2762:


    ma "E adesso dove?"


translate italian epilogue_mi_7a1ec301:


    my "Atteniamoci alla direzione che abbiamo scelto prima..."


translate italian epilogue_mi_3507d86b:


    "Abbiamo vagato a lungo, non avevamo più le né la forza né la voglia di proseguire."


translate italian epilogue_mi_5afc66a6:


    "Ero così assetato che ero disposto a leccare l'umidità delle pareti."


translate italian epilogue_mi_9e241c26:


    ma "Sediamoci… Solo per un attimo… Un poco…"


translate italian epilogue_mi_367b10bc:


    my "Non possiamo! Dobbiamo andare avanti!"


translate italian epilogue_mi_493613af:


    ma "Solo un minuto…"


translate italian epilogue_mi_84b04f60:


    "Ha supplicato Masha con angoscia."


translate italian epilogue_mi_e9e6e7f9_2:


    my "D'accordo…"


translate italian epilogue_mi_54517287:


    "Ha appoggiato la testa sulla mia spalla."


translate italian epilogue_mi_3c3d66ba:


    "La torcia ha iniziato a lampeggiare sempre più spesso, così ho deciso di spegnerla, in modo da risparmiare la batteria."


translate italian epilogue_mi_58343c59_1:


    ma "È finita…"


translate italian epilogue_mi_ce617998_3:


    "…"


translate italian epilogue_mi_2ffc89fb:


    "Non so quanto tempo fosse passato."


translate italian epilogue_mi_245bdc9c:


    "Avevo un disperato bisogno di dormire. I miei occhi si chiudevano da soli."


translate italian epilogue_mi_fd42b024:


    "Come ci eravamo incontrati la prima volta..."


translate italian epilogue_mi_29183e7d:


    "È stato tanto tempo fa..."


translate italian epilogue_mi_1f88cd59:


    my "Te lo ricordi...?"


translate italian epilogue_mi_0be4039d:


    "Ho chiesto in un sussurro."


translate italian epilogue_mi_b6f57941:


    ma "Che cosa...?"


translate italian epilogue_mi_059b8596:


    "La voce di Masha tremava."


translate italian epilogue_mi_d00cf9c5:


    my "Come ci siamo conosciuti..."


translate italian epilogue_mi_bfd44c3c:


    ma "Sì..."


translate italian epilogue_mi_4d08847b:


    "Ha cercato di ridere, ma immediatamente ha cominciato a tossire."


translate italian epilogue_mi_c09c2b0c:


    ma "Dormirò per un po', va bene?"


translate italian epilogue_mi_db20ce9f:


    my "Va bene, ma solo per un po', altrimenti dormirai di nuovo per l'intera giornata..."


translate italian epilogue_mi_8229d523:


    ma "Beh, ma tu mi sveglierai...?"


translate italian epilogue_mi_5e3404c1:


    my "Certamente..."


translate italian epilogue_mi_2f0abbfd:


    "L'ho baciata e ho chiuso gli occhi."


translate italian epilogue_mi_20a50466:


    "L'oblio si stava avvicinando, ma non mi importava – ero stato trasportato indietro nel tempo in quel preciso giorno..."


translate italian epilogue_mi_a20cefa7_36:


    "..."


translate italian epilogue_mi_a20cefa7_37:


    "..."


translate italian epilogue_mi_dc909087:


    "Mi faceva male dappertutto..."


translate italian epilogue_mi_ccf84f7d:


    "Forse il mio cervello non riusciva a riposare perché era continuamente disturbato da migliaia di segnali provenienti dalle mie terminazioni nervose."


translate italian epilogue_mi_ca2bcd31:


    "Mi sono alzato in qualche modo e ho svegliato Masha."


translate italian epilogue_mi_20c2627d:


    "Dopotutto, avevamo intenzione di sopravvivere..."


translate italian epilogue_mi_d737a002:


    "Per quanto a lungo siamo rimasti in questa miniera...?"


translate italian epilogue_mi_48f216c9:


    "Un bivio dopo l'altro, un tunnel dopo l'altro."


translate italian epilogue_mi_2c9bce0d:


    "Masha stava sussurrando qualcosa in continuazione."


translate italian epilogue_mi_c8ad0429:


    "«Sinistra»… «destra»… «sinistra»… «destra»…"


translate italian epilogue_mi_d9534268:


    "Non credo che sarei in grado di sopportare un altro bivio..."


translate italian epilogue_mi_b8aa830c:


    "Ma improvvisamente la torcia ha evidenziato un passaggio stretto nel buio, e ben presto ci siamo trovati in un'apertura."


translate italian epilogue_mi_853bf27c:


    ma "Guarda!"


translate italian epilogue_mi_8060d8b7:


    "Masha stava indicando qualcosa nel buio."


translate italian epilogue_mi_c367b132:


    "Ho puntato la torcia verso quel punto, e ho visto un foulard per terra."


translate italian epilogue_mi_a8f16511:


    ma "Questo non è..."


translate italian epilogue_mi_6d40bd4c:


    my "No! Andiamo!"


translate italian epilogue_mi_cd9d1345:


    "Alla fine ce l'abbiamo fatta a uscire da quel labirinto, e siamo finiti in una piccola stanza."


translate italian epilogue_mi_6fffd30f:


    "C'erano bottiglie rotte sparse su tutto il pavimento, tubature lungo le pareti, che terminavano in valvole arrugginite. Su tutte le pareti erano stati scarabocchiati una marea di graffiti."


translate italian epilogue_mi_c51d3130:


    my "Almeno lo scenario è diverso…"


translate italian epilogue_mi_9da1463d:


    "Ho sospirato."


translate italian epilogue_mi_a2d6a840:


    "La torcia si era quasi scaricata, e dovevo scuoterla ogni dieci secondi per recuperare un po' di batteria."


translate italian epilogue_mi_ccc4b1b1:


    "Masha si è lasciata cadere a terra, esausta."


translate italian epilogue_mi_7ddfed17:


    my "Non possiamo fermarci. Sono sicuro che l'uscita è qui vicino!"


translate italian epilogue_mi_e98886ba:


    ma "E come lo sai...?"


translate italian epilogue_mi_3a8d7079:


    "L'ho guardata."


translate italian epilogue_mi_36c6417c:


    "Era evidente che non avesse più energie – Masha stava combattendo contro la fatica, e avrebbe potuto perdere conoscenza da un momento all'altro."


translate italian epilogue_mi_b87232f7:


    my "Lo so e basta!"


translate italian epilogue_mi_046fd47c:


    "Ho sorriso, cercando di incoraggiarla."


translate italian epilogue_mi_fdd1561c:


    ma "Beh… In tal caso…"


translate italian epilogue_mi_2d188ff6:


    "Si è appoggiata contro la parete e si è rialzata con difficoltà."


translate italian epilogue_mi_d86bb40d:


    my "Resta seduta qui per adesso, vado a vedere cosa c'è più avanti…"


translate italian epilogue_mi_d2de1f1d:


    ma "Ho paura…"


translate italian epilogue_mi_811b3088:


    "Masha tremava."


translate italian epilogue_mi_376a198b:


    my "Non mi allontanerò di molto!"


translate italian epilogue_mi_600c1cfd:


    "Ha alzato gli occhi verso di me, speranzosa, facendomi un cenno quasi impercettibile."


translate italian epilogue_mi_e3edc783:


    "Mi sono guardato attorno e ho notato una pesante porta di ferro."


translate italian epilogue_mi_44b1af16:


    "I miei tentativi di aprirla sono stati vani – era completamente arrugginita e non aveva intenzione di smuoversi."


translate italian epilogue_mi_c3fa1cc1:


    "Ah, quel nostro pezzo di tubo sarebbe proprio utile adesso…"


translate italian epilogue_mi_2f004e19:


    "Ma non c'era altra scelta!"


translate italian epilogue_mi_a331b385:


    "Ho appoggiato la gamba sulla parete e ho scagliato tutto il mio peso contro la maniglia.{w} I miei muscoli si sono gonfiati, la mia fronte si è inondata di sudore, la mia vista si è oscurata…"


translate italian epilogue_mi_d24acfc5:


    "Ma la porta non si è mossa di un millimetro, ha solo emanato un debole cigolio."


translate italian epilogue_mi_3fd97b24:


    my "Va tutto bene, solo un secondo…"


translate italian epilogue_mi_e0d77cc3:


    "Ho gridato a Masha, cercando di riprendere fiato."


translate italian epilogue_mi_b203e2e6:


    my "Devo trovare un qualche bastone."


translate italian epilogue_mi_cbec9827:


    "Sono corso verso Masha, mi sono chinato e le ho sorriso."


translate italian epilogue_mi_f977f9ea:


    my "C'è sicuramente una via d'uscita dietro a quella porta!"


translate italian epilogue_mi_ad4d86a9:


    ma "Oh…"


translate italian epilogue_mi_21d01a89:


    "Mi ha guardato con aria stanca."


translate italian epilogue_mi_c0cfd78b:


    "Sono uscito dalla stanza e ho cominciato a setacciare la miniera alla ricerca di qualcosa di adatto."


translate italian epilogue_mi_55ef4138:


    "La torcia era sul punto di morire e, dopo aver emesso il suo ultimo fascio di luce, si è spenta del tutto…"


translate italian epilogue_mi_f94d2e95:


    "Mi sono fatto prendere dal panico e ho cominciato a camminare alla cieca, cercando di tornare indietro."


translate italian epilogue_mi_e071baa4:


    "Tuttavia continuavo a sbattere contro le pareti, e non riuscivo proprio a ricordare la via del ritorno."


translate italian epilogue_mi_c391fe01:


    my "Masha! Masha!"


translate italian epilogue_mi_80d8e32e:


    "Ho urlato, sperando che potesse sentirmi."


translate italian epilogue_mi_1b079b4b:


    "All'improvviso ho sentito un rumore provenire da dietro le mie spalle, si è accesa una luce e la mia ombra è apparsa sul pavimento, allungandosi lontano verso l'oscurità."


translate italian epilogue_mi_03cecdb6:


    "Ero pietrificato dal terrore."


translate italian epilogue_mi_67aec98b:


    "Ho sentito una voce apparentemente familiare."


translate italian epilogue_mi_2e95fbdc:


    un "Cosa c'è che non va Semyon, ti sei perso?"


translate italian epilogue_mi_b5c21e08:


    "Mi sono voltato lentamente e ho visto Lena che teneva in mano un torcia fiammeggiante."


translate italian epilogue_mi_19ff5743:


    "Il suo volto era sfigurato da un ghigno disumano."


translate italian epilogue_mi_d7da412d:


    un "Sono venuta per te!"


translate italian epilogue_mi_c256bf58:


    "Quella era la stessa Lena che avevo visto in piazza ieri sera."


translate italian epilogue_mi_2f54a629:


    "La stessa che avevo visto nella stanza di Olga Dmitrievna."


translate italian epilogue_mi_48cecf56:


    "Era Lena e, allo stesso tempo, non era Lena…"


translate italian epilogue_mi_363ad07d:


    "Quella creatura era più simile alle «Ulyane»…"


translate italian epilogue_mi_cdd889d8:


    un "Sto venendo a prenderti!"


translate italian epilogue_mi_9148c42c:


    my "Tu… tu… chi sei?"


translate italian epilogue_mi_28b5ea24:


    "Ho sibilato."


translate italian epilogue_mi_50e370da:


    un "Sono Lena! Non mi riconosci?"


translate italian epilogue_mi_a0df0b59:


    "È scoppiata in una risata diabolica."


translate italian epilogue_mi_32ddb218:


    un "Ti sei completamente dimenticato di me mentre ti divertivi con loro?"


translate italian epilogue_mi_84b9a0ea:


    my "Io… io…"


translate italian epilogue_mi_cfc352ff:


    "Le parole mi sono rimaste bloccate in gola."


translate italian epilogue_mi_3198ebd7:


    un "Ma va bene! Non è rimasto più nessuno adesso! Solo tu ed io!"


translate italian epilogue_mi_47c53372:


    my "Quindi tu… sei stata tu a fare tutto questo…?"


translate italian epilogue_mi_ef814bbe:


    un "Bingo! Non tutto, naturalmente…"


translate italian epilogue_mi_1346f417:


    "Per un secondo la sua espressione si è fatta colpevole, ma poi è tornata a sorridere."


translate italian epilogue_mi_ba906862:


    un "Slavya è stata la prima! Era sempre così fastidiosa! Era la «Signorina Affidabilità»! Diligente, sempre pronta a lavorare duramente, sempre in ogni luogo, sempre d'accordo con tutti! Che schifo! Sapessi quanto ha urlato quando le stavo strappando le braccia... È un peccato che tu non sia riuscito a vedere tutto!"


translate italian epilogue_mi_c255c3bb:


    "La sua bocca ha cominciato a tremare, la sua saliva colava fuori da essa, e i suoi occhi hanno ruotato."


translate italian epilogue_mi_a183bd23:


    un "E poi Alisa… «Tutti mi danno fastidio, non ho bisogno di nessuno»! Ora pende in pace appesa ad un albero! Tuttavia non è stato facile sollevarla fin lassù, ma l'effetto è stato grandioso! Come avete urlato tutti alla sua vista? Come dei suini in un macello!"


translate italian epilogue_mi_34cb066a:


    "Il volto di Lena era contorto da una smorfia di piacere malato."


translate italian epilogue_mi_16487ed9:


    un "Bene, ora resta solo quella cagna di una Miku – e poi sarà tutto finito!"


translate italian epilogue_mi_4879df12:


    "Ha tirato fuori da dietro le sue spalle un grosso machete, imbrattato di sangue."


translate italian epilogue_mi_4fd3d6cb:


    un "Beh, in realtà…"


translate italian epilogue_mi_604a0a8b:


    "Mi è sembrato che si fosse persa nei suoi pensieri."


translate italian epilogue_mi_3c2cd797:


    un "Non so esattamente cosa fosse quella merda che assomigliava a Ulyana… Lei e gli altri pionieri non sono stati uccisi da me… Ma importa forse qualcosa adesso?"


translate italian epilogue_mi_9daeafd9:


    "Ha scosso la sua torcia, spegnendola all'istante."


translate italian epilogue_mi_7e9959b4:


    un "Aspetta qui, farò in fretta!"


translate italian epilogue_mi_006d380a:


    "La sua risata diabolica è risuonata alle mie spalle."


translate italian epilogue_mi_eb5af906:


    "Questo significa che sta andando da Masha!"


translate italian epilogue_mi_6bbab6e5:


    "Devo fare qualcosa, e in fretta!"


translate italian epilogue_mi_64fdda0c:


    "Ma non ho assolutamente idea su quale direzione prendere – il buio mi ha disorientato completamente."


translate italian epilogue_mi_b632416a:


    "Al diavolo!"


translate italian epilogue_mi_c66c511c:


    "Sono corso nella direzione dove credevo fosse andata Lena."


translate italian epilogue_mi_0eae989f:


    "Poco dopo ho sbattuto violentemente contro un muro. Mi sono girato, cercando di muovermi lentamente, ma sono inciampato su un altro ostacolo, cadendo per terra e gridando dal dolore."


translate italian epilogue_mi_999e1188:


    "Cercando di rimettermi in piedi, mi sono reso conto che non potevo applicare alcun peso alla mia gamba sinistra.{w} Sembra che sia rotta…"


translate italian epilogue_mi_4b8b6c31:


    "Ho provato a strisciare, ma era tutto inutile – l'intero terreno della miniera era ricoperto di pietre aguzze, rotaie arrugginite e assi rotte."


translate italian epilogue_mi_a3fa3479:


    "Dopo un breve tratto le mie braccia sono diventate dei pezzi di carne sanguinanti, e non riuscivo più ad aprire gli occhi a causa della sabbia e della polvere."


translate italian epilogue_mi_2d13ba42:


    "Ho frugato nelle mie tasche e ho tirato fuori la scatola di fiammiferi."


translate italian epilogue_mi_47fd5c48:


    "Ho provato ad accendere un fiammifero con le mani tremanti, ma non sono riuscito nell'intento."


translate italian epilogue_mi_10e3d62a:


    my "Calmati… Calmati…"


translate italian epilogue_mi_d09690e2:


    "Ho mormorato."


translate italian epilogue_mi_0187584b:


    "Finalmente, dopo il quinto tentativo, sono riuscito ad illuminare l'ambiente con una luce fioca."


translate italian epilogue_mi_58360f3f:


    "Mi sono orientato rapidamente e ho capito da quale parte dovessi andare."


translate italian epilogue_mi_fc8120bd:


    "Sembrava che fossero passati solo pochi secondi prima di essermi ritrovato nella stanza dove avevo lasciato Masha."


translate italian epilogue_mi_8c229ba9:


    "L'ultimo fiammifero si è spento, e sono riuscito giusto a scorgere Lena, che stava alzando il machete al di sopra di Masha."


translate italian epilogue_mi_9c2f56ee:


    "Mi sono gettato nella loro direzione, urlando e tuffandomi in avanti."


translate italian epilogue_mi_deada465:


    "Le mie mani improvvisamente hanno urtato qualcosa. Sono caduto a terra e ho cominciato a dimenarmi selvaggiamente, colpendo ogni cosa attorno a me."


translate italian epilogue_mi_67c9b16b:


    "La maggior parte dei miei colpi andava a vuoto, ma poi i miei pugni si sono scontrati con qualcosa di soffice. Ho sentito un lamento, e un silenzio di tomba si è posato tutt'attorno."


translate italian epilogue_mi_3be834f5:


    "Mi sono sdraiato supino e ho iniziato a respirare affannosamente, inalando l'aria secca con avidità."


translate italian epilogue_mi_22b173a8:


    my "Masha?"


translate italian epilogue_mi_805bb83c:


    "Ho chiamato."


translate italian epilogue_mi_a4178908:


    "Ho sentito dei deboli singhiozzi."


translate italian epilogue_mi_0019cb32:


    my "Sei ancora viva?"


translate italian epilogue_mi_268d1f74:


    ma "Sì…"


translate italian epilogue_mi_e3a26dcc:


    "Ha sussurrato."


translate italian epilogue_mi_d1ec164c:


    my "Ti ha colpita?"


translate italian epilogue_mi_ab472d5e:


    ma "No…"


translate italian epilogue_mi_055645cd:


    "Ho cominciato a frugare nel buio e ben presto ho trovato la torcia e un accendino."


translate italian epilogue_mi_3401954a:


    "Ombre deformi hanno cominciato a danzare sulle pareti, e ho visto il corpo di Lena disteso sul pavimento.{w} Sembrava essere ancora viva."


translate italian epilogue_mi_3aa51417:


    "Dovevo finirla."


translate italian epilogue_mi_e3f3c64d:


    ma "No…"


translate italian epilogue_mi_f72c4a89:


    "Masha è strisciata verso di me e mi ha afferrato la gamba."


translate italian epilogue_mi_be4dbf99:


    my "Ma non possiamo semplicemente…"


translate italian epilogue_mi_e3f3c64d_1:


    ma "Non è necessario…"


translate italian epilogue_mi_71ab98c7:


    "Naturalmente Lena non era la vera colpevole…"


translate italian epilogue_mi_9218c96e:


    "Ma lei è il diavolo in persona!"


translate italian epilogue_mi_e8f60892:


    "Non possiamo lasciare in vita un essere umano come lei (sempre che la si possa chiamare così)."


translate italian epilogue_mi_2046e79a:


    "Ma dopo aver guardato Masha mi sono reso conto che non potevo… Proprio non potevo…"


translate italian epilogue_mi_1c94cf88:


    "Ho raccolto il machete dal pavimento, e rigirandolo tra le mie mani ho capito che l'avrei potuto utilizzare come leva."


translate italian epilogue_mi_56ceedde:


    "Avvicinandomi alla porta, ho infilato il machete nella ruota, e ho spinto più forte che potevo."


translate italian epilogue_mi_6904897c:


    "La porta ha cigolato e si è aperta. Un flusso di aria fresca ha invaso la stanza."


translate italian epilogue_mi_92b62876:


    my "Siamo salvi…"


translate italian epilogue_mi_44e2cc8f:


    "Sono tornato da Masha, l'ho aiutata a rialzarsi e siamo usciti dalla stanza, abbandonando Lena svenuta sul pavimento."


translate italian epilogue_mi_800c5a39:


    "Oltre la porta c'era un breve corridoio che terminava con una scala sulla parete, la quale portava ad una grata."


translate italian epilogue_mi_c6d7deb7:


    "Sono salito in cima e ho colpito l'estremità con tutte le mie forze."


translate italian epilogue_mi_379678b6:


    "La grata è caduta con un tonfo e siamo usciti…"


translate italian epilogue_mi_790139a0:


    "Mi sono lasciato cadere sull'erba, esausto, e mi sono guardato attorno."


translate italian epilogue_mi_b402b18c:


    "Di nuovo la piazza, di nuovo il campo…"


translate italian epilogue_mi_144666f0:


    "La brillante luce rossa dell'alba si stava innalzando a est, bruciando le cime di una foresta lontana."


translate italian epilogue_mi_698a2bad:


    "Le ultime stelle mi hanno strizzato l'occhio, ignare di tutto quello che era successo qui, in questo inferno…"


translate italian epilogue_mi_9f24a4f9:


    "Masha ha appoggiato la testa sulla mia spalla e ha indicato il cielo con mano tremante."


translate italian epilogue_mi_58999707:


    ma "Vorrei che fossimo lassù…"


translate italian epilogue_mi_a7b77934:


    "Anche se eravamo riusciti a fuggire da quella prigione, nulla era finito – le folle di Ulyane zombie vagavano per il campo, una diabolica Lena giaceva in quelle catacombe, e chissà ancora quali altre diavolerie aveva in serbo per noi questo luogo."


translate italian epilogue_mi_26d8a756:


    ma "Ho proprio bisogno di dormire…"


translate italian epilogue_mi_3b7315ab:


    "Non sapevo cosa risponderle."


translate italian epilogue_mi_85ad9044:


    "Perché non sapevo dove saremmo potuti scappare, non sapevo dove poter essere al sicuro."


translate italian epilogue_mi_d036f439:


    "Non sapevo come tornare al nostro mondo…"


translate italian epilogue_mi_7956f114:


    my "Dormi…"


translate italian epilogue_mi_bc298dba:


    "Ho accarezzato la testa di Masha."


translate italian epilogue_mi_c4c2efec:


    ma "Senti, questa sì che sarebbe una bella trama per un film!"


translate italian epilogue_mi_33b4df45:


    "È scoppiata a ridere."


translate italian epilogue_mi_5f9f4f73:


    "L'ho guardata atterrito."


translate italian epilogue_mi_887f26c3:


    "Masha stava sorridendo."


translate italian epilogue_mi_bc65e8e5:


    "Ma il suo sorriso non era diabolico come quello di Lena. Era puro, sincero, forse anche infantile."


translate italian epilogue_mi_9de01d70:


    "Tutta la sua stanchezza era sparita, e i suoi occhi brillavano allegramente."


translate italian epilogue_mi_7122db66:


    my "Che… cosa ti è successo?"


translate italian epilogue_mi_70580914:


    "Riuscivo a malapena a parlare."


translate italian epilogue_mi_8ec442e1:


    ma "Va tutto bene!"


translate italian epilogue_mi_bd43e6c0:


    "Si è appoggiata di nuovo alla mia spalla."


translate italian epilogue_mi_9d008617:


    ma "Quella era l'ultima scena!"


translate italian epilogue_mi_35287c9d:


    "Improvvisamente ho sentito le forze abbandonarmi."


translate italian epilogue_mi_23661344:


    "La mia vista si è oscurata, la mia coscienza ha cominciato a volar via da qualche parte."


translate italian epilogue_mi_f491b17d:


    "Stavo sdraiato in silenzio, riuscivo a sentire solo il respiro di Masha."


translate italian epilogue_mi_b2f531df:


    "Non il suono delle cavallette – come se fossero tutte morte – ma solo il respiro regolare di Masha."


translate italian epilogue_mi_99fe2e9b:


    "E il suo sorriso…"


translate italian epilogue_mi_a20cefa7_38:


    "..."


translate italian epilogue_mi_a20cefa7_39:


    "..."


translate italian epilogue_mi_7ffeaac0:


    "Ho faticato ad aprire gli occhi."


translate italian epilogue_mi_9af7bec5:


    "Il chiaro di luna era riflesso sull'acqua, salterellando allegramente sulle onde, una brezza leggera notturna soffiava, e in qualche luogo lontano un gufo si è fatto sentire."


translate italian epilogue_mi_ea099931:


    "Ho guardato Masha che dormiva tranquillamente accanto a me, coperta dalla mia camicia."


translate italian epilogue_mi_654b8602:


    "Che sogno…"


translate italian epilogue_mi_38e7da2f:


    "Il mio telefono mostrava l'ora – le tre del mattino!"


translate italian epilogue_mi_846c7ecb:


    "Sarà meglio andare a letto, o domani Olga Dmitrievna ci…"


translate italian epilogue_mi_d3fe6684:


    "Stavo per svegliare Masha, ma improvvisamente ho notato un copione aperto e una penna accanto a lei."


translate italian epilogue_mi_6c272b09:


    "Ho sfogliato rapidamente diverse pagine e ne sono rimasto inorridito – era del tutto identico a quello che era successo nel mio sogno!"


translate italian epilogue_mi_bbc14d5c:


    "Mi è venuta la nausea, e il mio sguardo si è oscurato."


translate italian epilogue_mi_fb974fc0:


    "Masha si è girata, ha aperto gli occhi e mi ha guardato."


translate italian epilogue_mi_5c118b9c:


    ma "Sei sveglio?"


translate italian epilogue_mi_aa1fbd9c:


    my "Hai… scritto tu tutto questo?"


translate italian epilogue_mi_ef225471:


    "Ho chiesto, con voce impastata."


translate italian epilogue_mi_41585d2c:


    ma "Sì, cosa c'è che non va?"


translate italian epilogue_mi_9ad49ae0:


    "Non sapevo come risponderle.{w} Proprio non riuscivo a capire cosa stesse accadendo."


translate italian epilogue_mi_efefdbbe:


    ma "Dormivi così dolcemente che non ho voluto svegliarti. E le cavallette zillavano così forte… Così ho…"


translate italian epilogue_mi_d1046a49:


    "Ha sorriso."


translate italian epilogue_mi_eb681808:


    my "Aspetta, ma noi siamo qui adesso?! Voglio dire, non siamo dei pionieri, stiamo girando il film qui e tutto quel genere di cose, non è vero?"


translate italian epilogue_mi_fed3ba60:


    ma "Certo, ovviamente…"


translate italian epilogue_mi_d6404691:


    "Masha mi ha guardato senza capire."


translate italian epilogue_mi_75cc25f6:


    "Forse mi sono svegliato, ho letto quello che aveva scritto, e poi mi sono addormentato nuovamente...?"


translate italian epilogue_mi_0e0ceee1:


    ma "Stai sudando!"


translate italian epilogue_mi_4b123fd7:


    "Ha frugato nelle tasche della sua uniforme di pioniere che giaceva accanto a lei e mi ha dato un fazzoletto.{w} Un tipico fazzoletto russo ricamato con fiori colorati."


translate italian epilogue_mi_a7d1b840:


    "L'ho preso e improvvisamente ho sentito un calore avvolgermi. Lo stesso calore che si prova quando una persona cara ti fa un regalo."


translate italian epilogue_mi_f60fe5df_1:


    ma "Andiamo?"


translate italian epilogue_mi_99462d82:


    "Masha ha iniziato a vestirsi."


translate italian epilogue_mi_665516ab:


    my "Sì… andiamo…"


translate italian epilogue_mi_839949ee:


    "Sono riuscito ad alzarmi a malapena, e mi sono diretto verso il fiume."


translate italian epilogue_mi_fd5cfac0:


    "L'acqua fredda mi ha fatto rivivere, e quel sogno ha smesso di sembrare così reale."


translate italian epilogue_mi_2c25b971:


    "Dopotutto, era solo un sogno, non è vero?"


translate italian epilogue_mi_9dd5fe3e:


    "Mentre tornavamo ci siamo tenuti per mano per tutto il tempo, e Masha chiacchierava senza interruzione."


translate italian epilogue_mi_b3e9120f:


    "Siamo arrivati alla piazza."


translate italian epilogue_mi_0ef867d4:


    ma "Spero che quelle creature mi lasceranno dormire almeno stanotte…"


translate italian epilogue_mi_e59f817f:


    "Ha detto pensierosa."


translate italian epilogue_mi_15c31469:


    my "Chi?"


translate italian epilogue_mi_914e5307:


    ma "Le cavallette!"


translate italian epilogue_mi_fbeb45b9:


    my "Ah… già, sarebbe fantastico…"


translate italian epilogue_mi_8425d0a6:


    ma "Bene, io vado…"


translate italian epilogue_mi_2d38f6b9:


    "Mi ha guardato intensamente, si è voltata e ha fatto per andarsene."


translate italian epilogue_mi_77ececca:


    my "Aspetta!"


translate italian epilogue_mi_15f28b4d:


    "L'ho presa per mano, l'ho spinta verso di me e l'ho baciata intensamente."


translate italian epilogue_mi_3d50e8ce:


    "Perché a volte, un sogno – è solo un sogno..."


translate italian epilogue_mi_d792d7ba:


    "E la realtà – è solo la realtà..."
# Decompiled by unrpyc: https://github.com/CensoredUsername/unrpyc
