
translate italian day6_main_8ae28bb6:


    "Non saprei dire a che ora Olga Dmitrievna mi avesse svegliato, ma ancora prima di aprire gli occhi ho percepito l'incombere della morte."


translate italian day6_main_8d76b64a:


    "Mi faceva male dappertutto, mi girava la testa, la mia mente era annebbiata."


translate italian day6_main_d8a5165b:


    mt "Semyon! Svegliati immediatamente o ti perderai la colazione e, cosa ancora più importante, l'allineamento!"


translate italian day6_main_1950340c:


    "Sembrava che la leader del Campo avesse intenzione di sfruttarmi al massimo anche oggi, come tutti gli altri giorni del resto."


translate italian day6_main_10ee39fe:


    "Ho borbottato qualcosa di incomprensibile, mi sono coperto la testa col lenzuolo e mi sono girato verso la parete."


translate italian day6_main_fd3267f5:


    th "Alla fine, la rabbia di Olga Dmitrievna non si spingerà mai oltre le semplici parole."


translate italian day6_main_511dc216:


    "O almeno volevo crederci davvero."


translate italian day6_main_a5a51a86:


    mt "Alzati subito, o…"


translate italian day6_main_7898d95e:


    "«O cosa?» – volevo dirle in tono trionfante, ma sono rimasto in silenzio."


translate italian day6_main_181f6035:


    "Non per la paura, ma solo perché ero troppo pigro per aprir bocca."


translate italian day6_main_45358519:


    th "Davvero, cosa potrebbe farmi?"


translate italian day6_main_53306e73:


    th "Una sgridata all'allineamento?{w} Appendere la mia foto sul muro della vergogna?"


translate italian day6_main_2308a0d4:


    th "O usarmi come cavia in esperimenti alieni disumani?"


translate italian day6_main_ffaac6cf:


    th "Beh, accetterei pure quello, ma adesso lasciami dormire ancora un paio d'ore."


translate italian day6_main_4547c60b:


    mt "Va bene… Ma se salterai l'allineamento…!"


translate italian day6_main_68fee20c:


    "La porta ha sbattuto dietro la leader del Campo, e il sonno che stava per svanire è riapparso nella mia mente."


translate italian day6_main_a20cefa7:


    "..."


translate italian day6_main_4dec4f4d:


    "Mi sono svegliato abbagliato dalla luce solare."


translate italian day6_main_8f17d018:


    "Era l'una di pomeriggio secondo il mio cellulare, che stava spremendo le sue ultime gocce di batteria ancora rimaste."


translate italian day6_main_9409b38e:


    "Stranamente non sentivo più alcun dolore, la mia mente si è schiarita, e tutto sommato, la giornata è iniziata abbastanza bene."


translate italian day6_main_6142f969:


    "Dopo essermi stirato, facendo finta di fare esercizi, sono uscito dalla casetta e mi sono diretto verso i lavabi."


translate italian day6_main_5b04907d:


    th "È vero, ho saltato la colazione, ma il pranzo arriverà a breve, quindi non dovrei preoccuparmi se dovrò rimanere un po' a stomaco vuoto come ieri."


translate italian day6_main_7052ffcc:


    "Durante il tragitto ho incontrato un pioniere il cui volto in qualche modo mi sembrava stranamente familiare."


translate italian day6_main_1593a66d:


    "Ma stavo camminando così in fretta che non ho potuto vederlo meglio. E quando mi sono voltato era già sparito dietro l'angolo."


translate italian day6_main_609cc4c0:


    "Non c'era nessuno nei pressi dei lavabi."


translate italian day6_main_8bf50f95:


    "Probabilmente erano tutti occupati con i compiti di Olga Dmitrievna, e Dio solo sa cos'altro."


translate italian day6_main_81c75e91:


    "Mi sono lavato rapidamente i denti, la faccia, e stavo per andarmene, quando improvvisamente ho sentito il rumore dell'acqua che scorreva dal rubinetto sul lato opposto."


translate italian day6_main_ca1b7d89:


    "Un pioniere si è appoggiato al lavabo."


translate italian day6_main_8ce7bba1:


    "Non riuscivo a vederlo in faccia, ma a giudicare dai suoi lineamenti sembrava quello che avevo visto pochi minuti fa."


translate italian day6_main_003b19dd:


    pi "Come te la passi? Il sole è così splendente oggi!"


translate italian day6_main_75daf906:


    "Ho guardato verso il cielo, schermandomi gli occhi con il palmo della mano."


translate italian day6_main_725738da:


    me "Già, niente di speciale…"


translate italian day6_main_6ef01be6:


    "Non era facile riuscire a vedere meglio quel pioniere – a causa della sua postura e del sole accecante che si rifletteva sulla superficie dell'acqua e sul metallo dei lavabi, la sua faccia era oscurata e non ero in grado di distinguere un singolo dettaglio."


translate italian day6_main_cdb07297:


    pi "La leader del Campo è molto arrabbiata oggi... davvero arrabbiata!"


translate italian day6_main_cb4b580d:


    "La sua voce era terribilmente familiare."


translate italian day6_main_2fa452e8:


    me "Beh, lei è sempre così…"


translate italian day6_main_c0d7b8c0:


    pi "Vero, tu devi saperlo bene, già…"


translate italian day6_main_319ce924:


    "Ho provato a ricordare se avessi già sentito quella voce o visto quel pioniere in passato."


translate italian day6_main_efa43c61:


    pi "Beh, ci vediamo…"


translate italian day6_main_7c10e79b:


    "Ha chiuso il rubinetto e si è incamminato rapidamente verso il sentiero della foresta."


translate italian day6_main_fe539829:


    "Per un attimo ho pensato di seguirlo, di fermarlo, ma mi sono subito levato l'idea dalla testa, dato che non valeva la pena di rovinare una mattina bella come questa, con inutili sospetti e speculazioni."


translate italian day6_main_de1f880f:


    "La natura brillava di vita: le lussureggianti cime degli alberi stavano danzando ritmicamente assieme al vento, sussurrandosi tra loro; la brezza accarezzava delicatamente l'alta erba smeralda; gli uccelli tubavano nell'ombra, nascondendosi dalla calura estiva; i boschi e i campi, che si estendevano fino all'orizzonte, si dissolvevano nella calda luce solare."


translate italian day6_main_010548de:


    th "Non saprei dire esattamente in che mese siamo, ma sembra essere piena estate."


translate italian day6_main_fc0175bf:


    "Ricordavo in dettaglio le vacanze estive della mia infanzia e giovinezza – tempi di divertimento, di svago e di gioia spensierata."


translate italian day6_main_c43f265a:


    th "Giochi per bambini, quanti ce n'erano…"


translate italian day6_main_a11227aa:


    th "Non mi rifiuterei di giocare a nascondino o a giochi di guerra neppure adesso.{w} O anche oscillare col bungee."


translate italian day6_main_bb2be5d8:


    th "O magari costruire un castello di sabbia e popolarlo di soldatini, pronti a difendere il loro padrone fino all'ultima goccia del loro sangue di plastica."


translate italian day6_main_a20cefa7_1:


    "..."


translate italian day6_main_d2032a24:


    "Sono andato alla piazza e mi sono seduto su una panchina, in attesa del pranzo.{w} Non doveva mancare molto."


translate italian day6_main_aac9dcb2:


    "Di tanto in tanto dei pionieri mi passavano davanti, a volte da soli, a volte in coppia, o anche in gruppi da tre."


translate italian day6_main_2be25cd9:


    "Ma non conoscevo nessuno di loro – Alisa, Ulyana, Lena, e Slavya non si vedevano da nessuna parte."


translate italian day6_main_e960bd40:


    "Pensieri circa l'insensatezza dell'esistenza giravano nel mio cervello, ma non mi preoccupavano più di tanto, in una giornata così bella."


translate italian day6_main_fa556551:


    th "A chi verrebbe in mente di autocommiserarsi per aver vissuto la propria vita invano, o per averla persa prematuramente, sotto i raggi di un sole così accogliente?"


translate italian day6_main_fe4d7ca8:


    th "Di certo non a me!"


translate italian day6_main_86f706e5:


    "Ho alzato lo sguardo verso Genda."


translate italian day6_main_ad19e3a6:


    "Stava meditando, come sempre."


translate italian day6_main_32b69a4b:


    th "Lui sicuramente non si pone domande inutili."


translate italian day6_main_4eb2c759:


    "Mi sono ricordato le mie prime ore in questo Campo, e il giorno precedente: l'angoscia, l'ansia e la paura."


translate italian day6_main_e55e0216:


    th "E tutto quello sembra ormai essere così remoto adesso, anche se è passato poco tempo."


translate italian day6_main_d7baf9b3:


    th "Riuscirò ad andarmene da qui o no…?"


translate italian day6_main_0330e53e:


    "Non mi importava così tanto come prima."


translate italian day6_main_83657a58:


    th "Forse sono già morto.{w} Allora questa dev'essere l'ultima fermata, si prega di scendere dal treno…"


translate italian day6_main_ecb0d206:


    pi "A cosa stai pensando?"


translate italian day6_main_a9c9e2a5:


    "Ho alzato lo sguardo e ho visto lo stesso pioniere di prima."


translate italian day6_main_323f65fe:


    "Non riuscivo ancora a vedere il suo volto, dato che il sole mi stava accecando."


translate italian day6_main_91a6c9e2:


    me "Sai, alla vita…"


translate italian day6_main_6af5686f:


    "Sembrava che volesse sedersi accanto a me, ma invece il pioniere è rimasto lì dov'era, si è solo girato di fianco, il che ha posto fine alla mia speranza di riuscire a vederlo in faccia."


translate italian day6_main_a0e278fd:


    me "Senti, ci siamo già incontrati in qualche luogo? Non credo di ricordarmi di te…"


translate italian day6_main_80d5b78e:


    pi "Beh, diciamo che sai chi sono."


translate italian day6_main_fb74ce55:


    me "Ma non lo so."


translate italian day6_main_b5b3bfa4:


    "Ho riso sinceramente."


translate italian day6_main_e0782e42:


    pi "Qui – non lo sai."


translate italian day6_main_b8948d3e:


    "Ha risposto concisamente."


translate italian day6_main_f2e007f0:


    me "Capisco…"


translate italian day6_main_79123fc6:


    "Sinceramente, non è che non volessi parlare, ma non sapevo cosa dire."


translate italian day6_main_50b86b79:


    "E la mia anima era così a suo agio che la cosa non mi ha infastidito."


translate italian day6_main_f6e6d257:


    pi "È la tua prima volta qui?"


translate italian day6_main_232e0bb1:


    "Mi ha fatto una domanda, ma il suo tono lasciava intendere che si aspettasse solo una conferma."


translate italian day6_main_1703d110:


    me "Sì… E tu?"


translate italian day6_main_43d309aa:


    pi "Io?"


translate italian day6_main_00b5ddd1:


    "Si è fermato per qualche secondo."


translate italian day6_main_8d5ee990:


    pi "No, non è la mia prima volta qui…{w} Posso dire di aver visitato questo Campo ogni anno, a partire dalla mia infanzia."


translate italian day6_main_df6d3916:


    "Una tale risposta ha catturato il mio interesse."


translate italian day6_main_e7458b35:


    me "Beh…{w} E com'era… prima…?"


translate italian day6_main_d64a6b28:


    pi "È sempre lo stesso…{w} Olga Dmitrievna è la leader del Campo, sempre gli stessi pionieri, sempre gli stessi allineamenti la mattina, e sempre i soliti sciagurati incidenti."


translate italian day6_main_bd473b02:


    "Per un momento mi è sembrato che fossi io a pronunciare quelle parole, e non lui."


translate italian day6_main_fe8d0158:


    me "Interessante…"


translate italian day6_main_4f1daffa:


    pi "È solo che a ogni nuovo…"


translate italian day6_main_a6820f71:


    "Ha esitato."


translate italian day6_main_1bda58b4:


    pi "…anno…{w} Succedono cose sempre più interessanti, e si riesce a capire meglio la situazione."


translate italian day6_main_cb23542f:


    me "Di cosa stai parlando?"


translate italian day6_main_225bea57:


    "Quella conversazione ha scatenato la mia curiosità."


translate italian day6_main_b46780c4:


    th "È un peccato che io non riesca a distinguere il volto di questo pioniere."


translate italian day6_main_aa6afd6a:


    pi "Ebbene, ogni sessione nel Campo ricorda quella precedente."


translate italian day6_main_831f663f:


    "Ha detto con calma."


translate italian day6_main_5f789b50:


    me "Può darsi…{w} Questa è la mia prima sessione."


translate italian day6_main_2015f3db:


    pi "Si vede."


translate italian day6_main_531b4fd4:


    "Ha fatto un ghigno."


translate italian day6_main_cdab879e:


    pi "Ma pare che non sia l'ultima."


translate italian day6_main_93624028:


    me "Beh, è divertente qui e tutto il resto, ma…{w} Sai come si suol dire, «non c'è posto migliore della propria…»"


translate italian day6_main_aefb5c47:


    pi "Casa, ma prima devi riuscire a tornare indietro!"


translate italian day6_main_d12dd680:


    "A quel punto mi è sembrato ormai chiaro che quel tizio mi stesse nascondendo qualcosa."


translate italian day6_main_3a65de66:


    "Per la precisione, si era spinto un po' oltre la usuale normalità di questo Campo, ed era troppo diverso dagli abitanti locali."


translate italian day6_main_b22130bc:


    me "Cosa vuoi dire?{w} Pensi che io sia bloccato qui per sempre o qualcosa del genere?"


translate italian day6_main_8a6dbb06:


    "Gli ho chiesto, scandendo ogni parola."


translate italian day6_main_70a4fcf8:


    "Il pioniere non ha avuto il tempo di rispondere, perché la sirena del pranzo ha suonato."


translate italian day6_main_65eca27b:


    "Mi sono voltato verso l'altoparlante, e quando poi mi sono girato di nuovo verso di lui, era già scomparso."


translate italian day6_main_7136b652:


    "Migliaia di teorie e congetture sono apparse nella mia testa all'istante, ma mi sono fermato, ricordandomi di tutta l'apparente normalità di questo Campo."


translate italian day6_main_d654b2a7:


    "Dopotutto, niente di soprannaturale era successo in questi cinque giorni."


translate italian day6_main_fbd5c1ad:


    "Inoltre, ogni cosa qui sembrava troppo naturale, a volte persino noiosa."


translate italian day6_main_d6045179:


    th "Forse quel pioniere non intendeva dire nulla del genere, e io l'ho solo frainteso?"


translate italian day6_main_070cce50:


    "Con quei pensieri in testa, mi sono diretto verso la mensa, con l'intenzione di abbuffarmi."


translate italian day6_main_e0e16d60:


    "A volte mi sembrava che il pranzo qui fosse simile a quello delle mense per i poveri durante una carestia."


translate italian day6_main_74f00562:


    "I pionieri correvano tutt'attorno, spingendosi l'un l'altro, cercando di sgattaiolare per essere i primi e sedersi al tavolo più comodo."


translate italian day6_main_d065bf1d:


    "Stavo in piedi in tutta tranquillità, attendendo pazientemente che la cuoca mi desse la mia razione di cibo."


translate italian day6_main_10ad7f24:


    "Per pranzo oggi c'era una Okroshka (che non mi piaceva molto), e cotolette con patate."


translate italian day6_main_57891974:


    "Mi sono seduto in un angolo e mi sono rallegrato mentalmente per il fatto di poter mangiare in pace."


translate italian day6_main_74749cbe:


    "Il mio tavolo era il più lontano dalla cucina. Potevo ragionevolmente sperare che i pionieri che erano alla ricerca di un posto libero non avrebbero raggiunto quel tavolo."


translate italian day6_main_c3b3dadd:


    "O almeno, l'avrebbero raggiunto solo se non ci fossero stati altri posti liberi."


translate italian day6_main_92a1900b:


    "Tuttavia, proprio quando stavo per iniziare, Slavya, Shurik ed Electronik sono apparsi tra la folla."


translate italian day6_main_252cd706:


    sl "Possiamo?"


translate italian day6_main_f0cecb70:


    "Non avevo nulla contro la loro compagnia."


translate italian day6_main_8f9071f9:


    me "Ma certo."


translate italian day6_main_2b21bf5c:


    "Il pranzo era sorprendentemente tranquillo, persino Electronik non stava blaterando come al solito."


translate italian day6_main_3763e444:


    "Finalmente ho finito il mio pasto, stravaccandomi sulla sedia e, soddisfatto, ho fatto schioccare la lingua."


translate italian day6_main_8ef508e6:


    me "Sentite, avete per caso incontrato un certo pioniere…{w} L'ho incontrato oggi…{w} Sapete, era così…"


translate italian day6_main_59b52b32:


    "Improvvisamente mi sono reso conto di non sapere come descriverlo."


translate italian day6_main_01a7d78d:


    me "Cioè, è più o meno alto come me, stessa carnagione…"


translate italian day6_main_789d0b9f:


    sl "Difficile a dirsi basandosi su una descrizione del genere."


translate italian day6_main_45a469ba:


    "Slavya ha sorriso."


translate italian day6_main_b0d50ab6:


    el "Beh, metà dei ragazzi del Campo sono come hai descritto, a dire il vero."


translate italian day6_main_06cde8a8:


    "Tutto sommato avevano ragione."


translate italian day6_main_eaba70e6:


    sh "Perché ce lo stai chiedendo?"


translate italian day6_main_30d66e9b:


    me "È solo che l'ho incontrato oggi.{w} E mi sembra di non averlo mai visto prima."


translate italian day6_main_58193b9c:


    el "Cerca nella mensa!{w} Non credo che voglia saltare il pranzo!"


translate italian day6_main_11b8b706:


    th "Perché non ci ho pensato prima?!"


translate italian day6_main_44cf71eb:


    me "Hai ragione!{w} Bene ragazzi, buon appetito!"


translate italian day6_main_f11b8b6b:


    "Mi sono alzato e ho camminato lentamente lungo le file di tavoli."


translate italian day6_main_be7f7c50:


    th "Lena e Zhenya sono sedute laggiù. Ho fatto loro un sorriso amichevole."


translate italian day6_main_0a699e1d:


    th "Alisa e Ulyana stanno ridendo e discutendo di qualcosa."


translate italian day6_main_58fae8be:


    th "Olga Dmitrievna, circondata dai pionieri…{w} Per fortuna che non mi ha notato."


translate italian day6_main_73c2f32d:


    th "Non è rimasto quasi alcun posto libero, ma il tizio di stamattina non si vede da nessuna parte."


translate italian day6_main_bf019437:


    th "La situazione si sta facendo sempre più interessante…"


translate italian day6_main_2814bfb0:


    th "Sembra che io non lo possa trovare qui dentro."


translate italian day6_main_da518989:


    th "Forse ha già pranzato?"


translate italian day6_main_75e20332:


    "Mi sono diretto verso l'uscita."


translate italian day6_main_c68ce9bc:


    "Faceva così caldo fuori che era come se ci si potesse sciogliere non appena usciti dall'ombra."


translate italian day6_main_9f9676f7:


    "Improvvisamente mi è venuto sonno."


translate italian day6_main_be0bca85:


    "Ho sbadigliato e mi sono diretto verso la casetta della leader del Campo, al fine di sperimentare il principio di Archimede."


translate italian day6_main_c150c0d1:


    "Alla fine, era improbabile che Olga Dmitrievna potesse tornare a breve (o almeno credevo), e avrei avuto sicuramente un paio d'ore per riposare."


translate italian day6_main_207c30bb:


    th "Dopotutto, non credo che ci siano altre opzioni.{w} Soprattutto con questo caldo."


translate italian day6_main_ee0c51ab:


    "La freschezza celestiale all'interno della mia stanza è diventata la mia salvezza."


translate italian day6_main_95912f70:


    "Mi sono tolto i vestiti e sono saltato nel letto."


translate italian day6_main_3da3d910:


    "I grilli frinivano pigramente all'esterno, il vento scuoteva le tende della finestra, e io sono affondato nel mondo dei sogni…"


translate italian day6_main_a20cefa7_2:


    "..."


translate italian day6_main_cd340537:


    "Il bussare alla porta mi ha svegliato.{w} I colpi erano lievi ma insistenti."


translate italian day6_main_cc72393f:


    "Mi sono alzato e sono andato ad aprire, con riluttanza."


translate italian day6_main_ae0d3213:


    "Stranamente, non c'era nessuno sulla soglia."


translate italian day6_main_2a6b67de:


    "Mi sono strofinato gli occhi e mi sono guardato attorno.{w} Stessa cosa, non c'era nessuno."


translate italian day6_main_c567b577:


    th "Un sogno, forse?"


translate italian day6_main_ca2d8082:


    "Erano già le quattro."


translate italian day6_main_ce2e12fc:


    "Mi sentivo stanco."


translate italian day6_main_b2353364:


    th "Forse non avrei dovuto dormire di pomeriggio.{w} Dopo tutto, sapevo a cosa andavo incontro!"


translate italian day6_main_111fd3b2:


    "Ho messo gli stivali, sono uscito dalla stanza e ho chiuso la porta a chiave."


translate italian day6_main_45ecefad:


    th "E adesso dove vado? Tutto sommato non manca molto alla cena."


translate italian day6_main_ff6f5b1a:


    "Improvvisamente ho avuto voglia di rinfrescarmi un po', così sono andato verso la spiaggia."


translate italian day6_main_9f56ddfa:


    "Il sole stava già tramontando.{w} Il buio arriva presto qui al sud."


translate italian day6_main_100dfcab:


    "Ho strizzato gli occhi e ho osservato il disco fiammeggiante."


translate italian day6_main_dc571f95:


    pi "Chi era quello?"


translate italian day6_main_c4ddbe2a:


    "Mi sono spaventato."


translate italian day6_main_1a9fc9dc:


    "Il ragazzo misterioso di prima stava in piedi di fronte a me.{w} E ancora una volta il suo viso non era visibile a causa della luce brillante."


translate italian day6_main_9330aefd:


    "Per un istante il sole mi ha accecato del tutto, e quindi non ho potuto vedere nulla a parte i contorni sfocati degli oggetti."


translate italian day6_main_f4ab4a03:


    me "Mi stai pedinando?"


translate italian day6_main_ba4fcc35:


    pi "No, ero solo di passaggio."


translate italian day6_main_99107c75:


    "Ha risposto tranquillamente."


translate italian day6_main_7cf46969:


    me "Allora dovresti saperlo meglio di me!"


translate italian day6_main_b73b893a:


    pi "E così non hai visto nessuno?"


translate italian day6_main_b4c5d120:


    me "Esatto."


translate italian day6_main_c9d6ddfd:


    "Mi sono stropicciato gli occhi, ma la cosa non mi ha aiutato molto."


translate italian day6_main_efd14637:


    me "Perché non ti ho visto a pranzo?"


translate italian day6_main_9c68f611:


    "Ho chiesto senza mezzi termini."


translate italian day6_main_7b536ae4:


    pi "Non avevo fame."


translate italian day6_main_b703fc38:


    "Ha riso."


translate italian day6_main_61bd5e66:


    "Ho sbattuto le palpebre un paio di volte e la mia visione è tornata normale. Ma il pioniere è misteriosamente scomparso di nuovo."


translate italian day6_main_eec03d52:


    me "Che diavolo sta succedendo qui?!"


translate italian day6_main_9b11e809:


    "Ora avevo l'assoluta certezza che c'era qualcosa di strano in quel tizio."


translate italian day6_main_76509461:


    th "Dev'essere direttamente coinvolto in tutto ciò che sta accadendo qui!"


translate italian day6_main_47c82893:


    th "Devo scoprire di più."


translate italian day6_main_96aea495:


    th "Per cominciare è meglio assicurarsi che io non mi sia sbagliato, e che lui non sia un semplice pioniere locale."


translate italian day6_main_c9a31bdf:


    "Era molto affollato alla spiaggia.{w} Sembrava che l'intero Campo fosse venuto qui."


translate italian day6_main_a3ae7c9c:


    th "Beh, non c'è da meravigliarsi, dato questo caldo."


translate italian day6_main_d272a3e6:


    "Olga Dmitrievna stava in piedi un po' più distante dagli altri, tenendo d'occhio il suo esercito di pionieri."


translate italian day6_main_7806ecc0:


    "Proprio nel momento in cui ho pensato che venire qui non fosse stata una grande idea, lei mi ha notato."


translate italian day6_main_105fc865:


    mt "Ehi Semyon!"


translate italian day6_main_245e3bea:


    mt "Non ti ho visto all'allineamento! E in generale non fai altro che oziare per tutto il giorno!"


translate italian day6_main_3f27c2c0:


    "Non sapevo cosa risponderle."


translate italian day6_main_75ee262f:


    mt "E come credi di poter diventare un vero pioniere…?"


translate italian day6_main_28cd78c0:


    "Ha continuato, in tono più calmo."


translate italian day6_main_b900c6f1:


    me "Olga Dmitrievna, in questo Campo…{w} c'è forse un pioniere… che mi assomiglia molto?"


translate italian day6_main_5faf7021:


    "Sembrava sorpresa."


translate italian day6_main_ee235fec:


    mt "Forse, non lo so…{w} Perché me lo chiedi?"


translate italian day6_main_92ba045e:


    me "Oh, sono solo curioso…"


translate italian day6_main_db5e08e9:


    mt "Beh, abbiamo un sacco di ragazzi qui."


translate italian day6_main_7227b86f:


    me "Va bene, non importa…"


translate italian day6_main_2bbb0cb6:


    "Qualcuno ha urlato dalla riva del fiume e Olga Dmitrievna si è precipitata in quella direzione."


translate italian day6_main_a1ecea1f:


    th "Beh, sembra che io sia riuscito ad evitare una ramanzina."


translate italian day6_main_89b6f9be:


    "Mi sono sdraiato sulla sabbia rovente e ho osservato i pionieri giocare nell'acqua."


translate italian day6_main_88733a8a:


    "Dopo pochi minuti, Ulyana si è avvicinata a me."


translate italian day6_main_f62bc905:


    us "Perché sei così triste?"


translate italian day6_main_600e482e:


    me "Non sono triste."


translate italian day6_main_371d353a:


    us "Beh, allora perché te ne stai seduto qui tutto solo?"


translate italian day6_main_1bb0a12c:


    me "Per pensare…"


translate italian day6_main_6a81728a:


    us "A cosa?"


translate italian day6_main_df1dac3f:


    "Ho deciso di risparmiarle la mia storia sul misterioso pioniere."


translate italian day6_main_6da88382:


    me "Beh, immagina che tu stia parlando con una persona strana…{w} E devi scoprire che cosa esattamente in quella persona sia così strano."


translate italian day6_main_95250360:


    us "Semplice! Glielo chiederei direttamente!"


translate italian day6_main_ce3666e5:


    "Ha fatto una risata."


translate italian day6_main_f9b97b16:


    me "Troppo facile così…{w} Ma non ti risponderebbe."


translate italian day6_main_01b8a656:


    us "E come lo sai? Hai provato a chiederglielo?"


translate italian day6_main_4e9618f8:


    me "No, ma mi sembra ovvio…"


translate italian day6_main_03ccbb4d:


    "Ulyana non ha risposto, si è limitata a sedersi accanto a me, sospirando stancamente."


translate italian day6_main_bb0c418e:


    us "Mi sono fatta proprio una bella nuotata."


translate italian day6_main_98a645b3:


    me "E se quella persona sapesse qualcosa, ma non volesse dirtela?"


translate italian day6_main_09228ed5:


    us "Allora costringila!"


translate italian day6_main_65e449ac:


    th "Ma come...?"


translate italian day6_main_9fa67ce4:


    "Siamo rimasti a parlare di altre cose ancora per un po', e infine mi sono alzato in piedi, ci siamo salutati, e me ne sono andato."


translate italian day6_main_1da41780:


    th "È ora di trovare il misterioso straniero, e ottenere tutte le risposte."


translate italian day6_main_b30b8dd4:


    "Il primo luogo in cui cercare era la fermata del bus."


translate italian day6_main_560be1b3:


    th "In fin dei conti, è questo il luogo dove tutto è iniziato!"


translate italian day6_main_38c39f41:


    "La strada era deserta, come sempre.{w} Solo piccoli vortici di polvere le facevano visita occasionalmente."


translate italian day6_main_4a8cd2f2:


    "Stavo per voltarmi e ritornare al Campo, ma poi ho sentito qualcuno sussurrare."


translate italian day6_main_aa3f5830:


    pi "Non fidarti di lui!"


translate italian day6_main_c3c01734:


    "Qualcuno si stava nascondendo dietro alla statua."


translate italian day6_main_2475793e:


    "Era un pioniere, e mi stava dando le spalle."


translate italian day6_main_c32fda68:


    "E ancora una volta, la sua voce mi è sembrata stranamente familiare."


translate italian day6_main_0a2309cb:


    me "E tu chi sei?!"


translate italian day6_main_8c7ee3c8:


    "Ho fatto qualche passo verso di lui."


translate italian day6_main_d6271c95:


    pi "Fermo! Non fare un altro passo!"


translate italian day6_main_f0d11be8:


    "Per qualche motivo sono rimasto immobile."


translate italian day6_main_68a3cebc:


    "Da qualche parte dentro di me ero sicuro che fosse meglio non discutere con lui."


translate italian day6_main_c41b6a4c:


    me "Va bene, resterò qui…"


translate italian day6_main_9f354a24:


    pi "L'hai visto? Ci hai parlato?"


translate italian day6_main_ae9a3d04:


    "Ha chiesto nervosamente."


translate italian day6_main_3edfb9a5:


    me "Di chi stai parlando?"


translate italian day6_main_f3effee3:


    "Aveva indosso un'uniforme da pioniere."


translate italian day6_main_65856116:


    pi "Lo sai chi…"


translate italian day6_main_08a2904f:


    "Certo che lo sapevo!{w} Stava parlando di quello strano pioniere che avevo incontrato prima."


translate italian day6_main_9513cd87:


    me "Sì…"


translate italian day6_main_424c8856:


    "Ho risposto dopo qualche momento di silenzio."


translate italian day6_main_e3fcbbd3:


    pi "Cosa ti ha detto?"


translate italian day6_main_137679fe:


    "Mi ha chiesto in tono implorante."


translate italian day6_main_e88413fc:


    me "Niente di speciale…"


translate italian day6_main_5d3e0c2a:


    pi "Ti ha dato consigli? Ti ha detto cosa fare? Ti ha minacciato?"


translate italian day6_main_3a4fb558:


    me "No, niente del genere…{w} Certo, mi è sembrato un po' strano, ma niente più…"


translate italian day6_main_7896c94c:


    pi "Ricorda: potrebbe non essere solo! O meglio, lui è solo, ma potresti incontrare molti altri pionieri simili a lui."


translate italian day6_main_0effa2c7:


    me "E tu invece chi sei? Da chi ti stai nascondendo?"


translate italian day6_main_18d9b086:


    pi "Lo capirai…{w} quando sarà il momento…{w} Ma ricorda: la cosa più importante è trovare l'uscita!"


translate italian day6_main_ea099937:


    "Tutto ad un tratto ha soffiato una forte raffica di vento, strappando le foglie dagli alberi e gettandomi in faccia un vecchio sacchetto di carta. Mi sono schermato istintivamente, chiudendo gli occhi."


translate italian day6_main_3b1e051a:


    "Quando ho guardato nuovamente in direzione della statua, il pioniere era scomparso."


translate italian day6_main_cb002221:


    "In quel momento sono stato sopraffatto dalla paura.{w} Una vera paura, quasi tangibile."


translate italian day6_main_5eb7b77f:


    "Ricordo di essermi spaventato durante le mie prime ore in questo Campo."


translate italian day6_main_2ad95628:


    "Ma in quell'occasione tutto attorno a me sembrava innocuo e amichevole. Invece adesso «Sovyonok» aveva rivelato le sue fauci, che erano pronte a divorarmi."


translate italian day6_main_a7216041:


    "E dinanzi a me – l'ignoto…"


translate italian day6_main_6c246aae:


    "Avevo la pelle d'oca, la gola secca, mi tremavano le mani. Ho cercato di ignorare tutto e sono tornato nel Campo."


translate italian day6_main_8983704d:


    "Era già ora di cena, ma non avevo voglia di andare alla mensa."


translate italian day6_main_c10e6db8:


    "Dopo l'incontro con il secondo misterioso pioniere, il solo pensare alle persone in questo luogo mi dava i brividi."


translate italian day6_main_3f3d68fb:


    th "Non so neppure chi o cosa essi siano."


translate italian day6_main_d902b9fb:


    th "E anche se fin'ora non è successo niente di male, ciò non significa che io mi possa fidare di loro!"


translate italian day6_main_94d63846:


    "Mi sono fermato davanti alla sede dei circoli, ma poi mi sono reso conto che non era il posto migliore dove stare – mi sarei potuto imbattere in qualcuno, ed era una cosa che non volevo accadesse in quel momento."


translate italian day6_main_fdbf6225:


    "Quasi di corsa, mi sono affrettato verso la foresta, ho camminato per qualche metro lungo il sentiero, e alla fine mi sono fermato."


translate italian day6_main_35b23e96:


    th "Da un lato – il misterioso pioniere…{w} Beh, due, a dire il vero."


translate italian day6_main_d208b9b7:


    th "Dall'altro – tutti gli altri abitanti di questo Campo, che sembrano totalmente normali."


translate italian day6_main_8ae0dc11:


    "Ad ogni modo, la mia decisione del restare in disparte e lasciare che tutto si risolvesse da sé non era più un'opzione valida."


translate italian day6_main_0b638a1b:


    "A questo punto mi è sembrato chiaro – dovevo fare qualcosa per conto mio."


translate italian day6_main_5fd8c6ed:


    th "Ma cosa esattamente...?"


translate italian day6_main_5225748d:


    "Probabilmente tutti stavano cenando in quel momento, quindi ho potuto cogliere l'occasione per andare alla casetta di Olga Dmitrievna senza farmi notare."


translate italian day6_main_f9f20d48:


    "Ho deciso di prendere come prima cosa i vestiti che indossavo quando ero arrivato qui, e anche il cellulare."


translate italian day6_main_346b48a6:


    th "Al momento non sono in grado di pensare ad un piano, quindi per adesso è meglio se mi nascondo nella foresta."


translate italian day6_main_1840851c:


    "Sono balzato sulle scale e mi sono precipitato nella stanza."


translate italian day6_main_e7710e4b:


    "I miei vestiti erano esattamente dove li avevo lasciati, e il mio telefono era dove mi aspettavo che fosse – sotto al cuscino."


translate italian day6_main_b5791e64:


    "L'ho afferrato rapidamente e stavo per mettermelo in tasca, quando ho notato qualcosa sullo schermo."


translate italian day6_main_4eda2e3c:


    "La finestra dei messaggi era aperta."


translate italian day6_main_3d971385:


    "Il testo diceva:"


translate italian day6_main_a645ed9e:


    "«Ti stai sbagliando, Semyon! Ti stai sbagliando di grosso!»"


translate italian day6_main_d3429bef:


    "Era come se la mia anima avesse abbandonato il corpo."


translate italian day6_main_a9617350:


    "Sono rimasto paralizzato."


translate italian day6_main_8962b7ae:


    "Ero tutto tremante, il sangue mi pulsava così forte nelle vene, era come se il mio cranio stesse per esplodere a causa della pressione."


translate italian day6_main_4e8980f6:


    "Mi ci è voluto almeno un minuto per riprendere i sensi."


translate italian day6_main_b2d0ecc1:


    th "Olga Dmitrievna avrebbe potuto digitare il messaggio…{w} Oppure uno dei pionieri."


translate italian day6_main_5b297fa5:


    th "Non è difficile capire come farlo, anche se non hanno familiarità con questa tecnologia."


translate italian day6_main_98fe4021:


    th "Ma nessuno oltre a me dovrebbe sapere dove tengo il telefono!"


translate italian day6_main_2d7a0195:


    "Questo evento è stata la goccia che ha fatto traboccare il vaso."


translate italian day6_main_d3412ceb:


    "Mi sono fiondato fuori dalla stanza, deciso a non tornare mai più."


translate italian day6_main_a20cefa7_3:


    "..."


translate italian day6_main_d6e969ac:


    "È calata la notte sul Campo."


translate italian day6_main_8e9948b3:


    "Sono rimasto seduto nella foresta per un paio d'ore, trasalendo ad ogni minimo rumore."


translate italian day6_main_a921011f:


    "Il posto che avevo scelto era abbastanza distante dal sentiero, quindi sarebbe stato difficile trovarmi."


translate italian day6_main_b808e2cc:


    "Nel frattempo mi ero già fatto qualche idea sul da farsi.{w} Cercare di scappare, o cercare di uccidere ogni singola persona nel Campo…"


translate italian day6_main_ecefb337:


    "D'altra parte, stavo cercando di convincermi che non avrei dovuto comportarmi in quel modo a causa di un qualche strano pioniere. O a causa di quel messaggio sul telefono…"


translate italian day6_main_f4c17cd7:


    th "Tutto questo mi suggerisce che ogni cosa che accade qui è tutt'altro che normale, ma non c'è alcuna prova che qualcuno nel Campo abbia a che fare con ciò."


translate italian day6_main_09d97100:


    "Sarei potuto restare qui fino al mattino, perso nei miei pensieri, ma ho sentito dei passi provenire da qualche parte nelle vicinanze..."


translate italian day6_main_34d72ebd:


    pi "E così alla fine hai deciso di fuggire?"


translate italian day6_main_c9a36b10:


    "Mi sono girato, ma non riuscivo a vedere il volto del pioniere nel buio."


translate italian day6_main_06562ffd:


    "Tuttavia, ero abbastanza sicuro che si trattasse di {i}lui{/i}."


translate italian day6_main_cb29da41:


    "In quel momento non ho avuto paura."


translate italian day6_main_c09ac6c3:


    "Per la precisione, ero così esausto fisicamente che mi ero già preparato per ogni possibile corso degli eventi, ma in qualche modo riuscivo a ragionare in modo appropriato, e a mantenere la conversazione."


translate italian day6_main_0a666ca8:


    me "Non la chiamerei fuga."


translate italian day6_main_6f0a1453:


    "Ho risposto dopo un po', estendendo ogni parola."


translate italian day6_main_2f8ebf8c:


    pi "Davvero? Allora cos'è?"


translate italian day6_main_4ca5f33e:


    me "Una ritirata tattica…"


translate italian day6_main_871ad7d7:


    pi "Brillante!"


translate italian day6_main_1141c237:


    "È scoppiato a ridere."


translate italian day6_main_32251891:


    me "Senti, perché non mi dici cosa sta succedendo, chi sei e che cosa vuoi da me?"


translate italian day6_main_2f1ec6d2:


    pi "Cosa ti ha detto {i}quel tizio{/i} giù al cancello?"


translate italian day6_main_794b660f:


    "Sembrava che non avesse nemmeno sentito la mia domanda."


translate italian day6_main_07ceaf40:


    me "Mi ha detto di non fidarmi di te."


translate italian day6_main_bc2b8f99:


    "Ho mentito."


translate italian day6_main_8f076af1:


    "Ma d'altra parte, ho pensato che fosse quello che intendesse dire."


translate italian day6_main_b22a7523:


    pi "Beh, lui è sempre così… Scappa, si nasconde…"


translate italian day6_main_85e1a38c:


    "Potevo sentire l'irritazione nella sua voce."


translate italian day6_main_49ff021e:


    me "Non so esattamente che tipo di teatrino stai facendo qui, ma non ho intenzione di farne parte!"


translate italian day6_main_f1508df8:


    pi "Oh, perché no…{w} Dopotutto, sei tu il protagonista!"


translate italian day6_main_565563b0:


    "Non riuscivo a vedere il suo volto, ma avrei scommesso che stava sorridendo."


translate italian day6_main_26e04ee0:


    me "Allora spiegami la mia parte!"


translate italian day6_main_e4cb38e3:


    pi "Sai, all'inizio ero come te…{w} La prima volta è andato tutto liscio."


translate italian day6_main_6ca36315:


    pi "Poi sono scappato, ho cercato di capire cosa stesse succedendo, sono impazzito, e li ho persino…{w} li ho torturati per farmi dire la verità!"


translate italian day6_main_a667d35b:


    "Ha fatto una risata folle."


translate italian day6_main_5eb260fc:


    "Ho rabbrividito."


translate italian day6_main_8b9a92c3:


    pi "Ma è stato tutto inutile!"


translate italian day6_main_6b9446af:


    "Ha proseguito, dopo essersi calmato un po'."


translate italian day6_main_2a471045:


    pi "Inutile…{w} E poi ho cominciato a notare le lacune…"


translate italian day6_main_a7d4acbc:


    pi "All'inizio riuscivo a sentire solo delle voci – da qualche parte in lontananza, altre volte nella mia testa."


translate italian day6_main_3dc38f3b:


    pi "Poi delle vaghe sagome sono apparse.{w} Poi lentamente hanno iniziato a prendere forma fisica."


translate italian day6_main_c695ff8a:


    pi "E infine, sono entrati nel mio mondo! Potevo toccarli, presentarli agli altri pionieri…"


translate italian day6_main_81e2b75f:


    pi "Ed erano tutti diversi! Diversi! Capisci? Diversi!"


translate italian day6_main_54c1072c:


    "Ha cominciato a gridare."


translate italian day6_main_634696b6:


    pi "E tutto si ripeteva ancora e poi ancora. Puoi abituarti ai cicli, ma…"


translate italian day6_main_b4bb03eb:


    pi "Poi ho capito come entrare nei loro mondi, come interagire con gli altri. Ma ho scoperto di non essere l'unico. Siamo in tanti!"


translate italian day6_main_62a7ca22:


    pi "Oggi ne hai visto almeno un altro!"


translate italian day6_main_7e8f7bb7:


    "Ha taciuto."


translate italian day6_main_f4b1a463:


    "Non sapevo cosa chiedergli e non volevo interrompere la sua storia, così mi sono limitato ad attendere."


translate italian day6_main_0c7f5431:


    "Dopo un minuto, ha proseguito."


translate italian day6_main_bb9de06b:


    pi "Non è così semplice, ovviamente…{w} E non è sempre possibile…"


translate italian day6_main_7fd20c4d:


    pi "Solo in determinate circostanze...{w} Quando si provano forti emozioni, ad esempio."


translate italian day6_main_fb2863a3:


    "Mi sono immediatamente ricordato di quando l'avevo incontrato la prima volta, in quel momento non avevo provato alcuna emozione particolare."


translate italian day6_main_f7c370d3:


    pi "Inclusi i momenti in cui ti senti felice…"


translate italian day6_main_75932a4e:


    "Era come se riuscisse a leggermi nel pensiero."


translate italian day6_main_505d91ae:


    me "Capisco…"


translate italian day6_main_88477eb9:


    me "Quindi stai cercando di dirmi che esistono dei mondi paralleli che si intersecano in questo Campo, dove io vengo sostituito da te o da quell'altro tizio alla fermata del bus…?"


translate italian day6_main_ee412b1d:


    pi "Sì, qualcosa del genere…"


translate italian day6_main_7c3f9b3e:


    "La sua risposta non mi ha sorpreso affatto."


translate italian day6_main_04ff5586:


    "Alla fine, è palese che ogni cosa che accadeva qui fosse ben oltre i limiti della comprensione umana, e questa teoria non sembrava nemmeno essere così surreale."


translate italian day6_main_b263087c:


    me "Ma hai detto che tutto si ripete…"


translate italian day6_main_c5256338:


    pi "Già, si ripete."


translate italian day6_main_90478ae9:


    me "E che cosa accadrà…{w} dopo?"


translate italian day6_main_af16c541:


    pi "Si ricomincia da capo – ti sveglierai nel bus, arriverai al Campo, incontrerai Olga Dmitrievna, le ragazze, Electronik…"


translate italian day6_main_eea0fa70:


    me "Ma se è successo a te, allora perché dovrebbe capitare anche a me?"


translate italian day6_main_cae5da7b:


    pi "Succede a tutti!"


translate italian day6_main_25c5c857:


    "Ancora una volta, si è messo a ridere istericamente."


translate italian day6_main_571d1544:


    me "E quanti «cicli» hai sperimentato fin'ora?"


translate italian day6_main_7768eb2b:


    pi "Ormai ho smesso di contarli…{w} All'inizio però cercavo di tenerli a mente.{w} Forse qualche centinaio…"


translate italian day6_main_7927bc7d:


    "Non c'è da stupirsi, era chiaro come il sole che quel tizio fosse affetto da una progressiva degenerazione mentale."


translate italian day6_main_72d6794a:


    me "Ma hai cercato una via d'uscita, non è vero?"


translate italian day6_main_ab51e1d2:


    "Non ha risposto."


translate italian day6_main_dfec187b:


    me "Era di questo che mi parlava il ragazzo alla fermata."


translate italian day6_main_e710f157:


    pi "Perché lui non capisce niente, dannazione!"


translate italian day6_main_332b374a:


    "Ha gridato."


translate italian day6_main_31847263:


    pi "Perché è proprio come te! Sempre a scappare, a nascondersi!"


translate italian day6_main_42cd9689:


    me "Cosa suggeriresti allora?"


translate italian day6_main_d5f8131d:


    pi "Se avessi qualche suggerimento, non me ne starei qui a parlare con te."


translate italian day6_main_3a64a45c:


    me "D'accordo, ma ci hai provato, vero?"


translate italian day6_main_5887898e:


    pi "Pensi forse di saperne più di me, dal momento che sono qui ormai da moltissimo tempo?"


translate italian day6_main_c276f7a2:


    "Ha chiesto allegramente."


translate italian day6_main_c38656e1:


    me "Beh, questo è evidente…"


translate italian day6_main_94935db0:


    pi "Certo, ho provato a scappare da qui."


translate italian day6_main_6bc52dbe:


    pi "Laggiù…"


translate italian day6_main_ee7633ba:


    "Ha fatto un cenno verso la foresta."


translate italian day6_main_57f82592:


    pi "…non c'è assolutamente niente.{w} Solo alberi.{w} Quando una volta ho camminato per diversi giorni di fila e sono svenuto per la fatica, alla fine mi sono ritrovato nuovamente sul bus."


translate italian day6_main_00d4b823:


    pi "Lo stesso vale per la strada – è infinita."


translate italian day6_main_24fd33fb:


    pi "Parlare di questa cosa con qualcuno è inutile. Probabilmente l'avrai già capito da solo."


translate italian day6_main_fb6303a8:


    me "Sì."


translate italian day6_main_677a1d30:


    "Sono intervenuto."


translate italian day6_main_ea337e24:


    pi "Anche se cerchi di spiegare la situazione in modo diretto, nel migliore dei casi vieni considerato un idiota."


translate italian day6_main_a3fc2851:


    me "Che cosa succede se provi a rimanere nel bus?"


translate italian day6_main_75d11f10:


    pi "È inutile.{w} Prima o poi qualcuno ti troverà."


translate italian day6_main_3ba30819:


    pi "Ho anche cercato di passare tutto il tempo lì dentro.{w} Alla fine mi sono addormentato, e tutto è ricominciato da capo."


translate italian day6_main_472d9432:


    me "E se lo facessi partire?"


translate italian day6_main_5782e23f:


    pi "Non ci sono le chiavi.{w} E non ho alcuna competenza nel furto d'auto."


translate italian day6_main_9e6c1787:


    me "Un ciclo maledetto…"


translate italian day6_main_8712b475:


    pi "Esattamente!"


translate italian day6_main_d39e8b29:


    "È piombato un lungo silenzio."


translate italian day6_main_1d014408:


    "Alla fine ho chiesto:"


translate italian day6_main_99ff1407:


    me "E che dire delle persone qui intorno? Non hanno alcun ragionevole sospetto?!"


translate italian day6_main_92239f1a:


    pi "Sospetto?"


translate italian day6_main_bbf472a0:


    "Ha fatto un'altra risata."


translate italian day6_main_b33ce14d:


    pi "L'unica cosa sospetta è che non importa quanto tu cerchi di spiegare loro le cose, continueranno a fissarti ad occhi spalancati!"


translate italian day6_main_24929dab:


    pi "Le persone…{w} Anch'io avevo paura di loro, all'inizio."


translate italian day6_main_a250d056:


    pi "Poi li ho usati come cavie in vari esperimenti...{w} E adesso non li considero più umani. Sono dei semplici pupazzi, delle marionette!"


translate italian day6_main_74deb4fc:


    pi "Puoi facilmente prevedere ogni loro reazione, ogni azione, ogni singola parola!"


translate italian day6_main_de26d074:


    me "Quindi non vale la pena temerli, giusto?"


translate italian day6_main_e38b3c64:


    pi "Devi temere solo te stesso…"


translate italian day6_main_47ff30a4:


    "Ha detto piano."


translate italian day6_main_9180ae03:


    pi "Hai idea di quanto sia divertente vedere le ossa umane spezzarsi dopo una lenta torsione...?"


translate italian day6_main_642dbde1:


    th "Sembra che costui abbia perso completamente la ragione."


translate italian day6_main_d97fd181:


    me "Senti, capisco tutto, ma…"


translate italian day6_main_c5e21a03:


    pi "Anche quell'altro pensa che sia un po' eccessivo..."


translate italian day6_main_88c33a5e:


    pi "Ma non importa. L'unico reale qui sei tu!"


translate italian day6_main_90713cb4:


    "Il pioniere si è fermato di colpo."


translate italian day6_main_8520460f:


    me "Beh, no, non sono d'accordo con te."


translate italian day6_main_e8e26d07:


    pi "Probabilmente starai pensando «perché non ho controllato tutto questo da solo?», Non è vero? «Perché non ho provato a raggiungere il villaggio o la città più vicina?»."


translate italian day6_main_7c6f498a:


    me "Sì, ho avuto tali pensieri."


translate italian day6_main_691f28f6:


    pi "«Perché non mi sono insospettito? Perché mi ritrovavo a fare certe cose invece di cercare risposte?»"


translate italian day6_main_2c057d1c:


    me "Beh… Sì…"


translate italian day6_main_fa8bd2c9:


    pi "Non farci caso!{w} È normale.{w} Tutti noi ci comportiamo così la prima volta qui."


translate italian day6_main_a5bbe860:


    me "Ma quanti altri hai visto finora?"


translate italian day6_main_aa579a81:


    pi "Non molti."


translate italian day6_main_447e6d41:


    "Il pioniere si è perso nei suoi pensieri."


translate italian day6_main_dcc4b12f:


    pi "Una decina, forse…{w} Ma sono sicuro che ce ne sono molti altri!"


translate italian day6_main_011e2d8a:


    me "E…{w} sono tutti…{w} così?"


translate italian day6_main_65c1d01f:


    pi "In effetti, sì.{w} Cambia solo qualche dettaglio, ma la base è sempre la stessa – non c'è modo di andarsene da qui!"


translate italian day6_main_01815b85:


    pi "E, a proposito, tu sei proprio come la maggior parte di loro!"


translate italian day6_main_509c847e:


    pi "L'unica cosa che ti distingue è che hai saputo tutto da me, invece di sperimentare ogni cosa in prima persona."


translate italian day6_main_e03c0d9a:


    "Non so se avrei dovuto ringraziarlo per questo…"


translate italian day6_main_4b5045fd:


    me "Bene, e adesso?"


translate italian day6_main_f3d36d37:


    pi "Niente."


translate italian day6_main_d9259528:


    "Ha detto concisamente."


translate italian day6_main_36cf3956:


    "Ho chiuso gli occhi e mi sono perso nei miei pensieri.{w} E quello è stato un errore fatale."


translate italian day6_main_0925d989:


    "Quando li ho riaperti, non ho più visto nessuno, proprio come le altre volte."


translate italian day6_main_6d094282:


    th "Quindi adesso è tutto chiaro…{w} Ma in realtà, come potrebbe tutto quello che mi ha rivelato guidarmi verso una risposta?"


translate italian day6_main_387743f2:


    th "D'accordo, non sono l'unico. D'accordo, tutto si ripete.{w} Ma qual è la ragione di tutto questo?"


translate italian day6_main_ad7a645c:


    th "E, cosa più importante – dov'è la via d'uscita...?"


translate italian day6_main_f75ae07b:


    "L'unica cosa utile che sono riuscito a cavare da quella conversazione è il sapere che non dovevo temere le persone del luogo."


translate italian day6_main_a7aff17c:


    "E questo significava molto per me adesso, dato che sarebbe stato molto meglio dormire in una stanza calda, piuttosto che nella foresta."


translate italian day6_main_2c3edc12:


    "Ho sospirato, ho raccolto le mie cose e mi sono diretto verso il Campo."


translate italian day6_main_89090f90:


    "Mentre camminavo, la conversazione con il mio compagno di sventure continuava ad apparire e a scomparire dalla mia mente."


translate italian day6_main_b3935654:


    th "Perché non gli ho chiesto del messaggio sul mio cellulare? E di alcune altre cose…{w} Aveva accennato a qualcosa, dopotutto…"


translate italian day6_main_222e2152:


    th "Forse se avessi scoperto ulteriori dettagli, avrei potuto trarre delle conclusioni."


translate italian day6_main_bd2b9931:


    "Tuttavia, avevo la sensazione che quello non sarebbe stato il nostro ultimo incontro."


translate italian day6_main_168f0e75:


    "Le luci erano accese nella casetta di Olga Dmitrievna."


translate italian day6_main_535a1938:


    "Ho aperto la porta con delicatezza e sono entrato."


translate italian day6_main_f6e23c0c:


    mt "Parli del diavolo, ed ecco che arriva!"


translate italian day6_main_30eab556:


    "Mi ha detto la leader del Campo, con rabbia."


translate italian day6_main_5ad08176:


    me "Olga Dmitrievna, sono così stanco ora, quindi cerchiamo di rimandare la lezione."


translate italian day6_main_54224d71:


    mt "Stanco di cosa, mi chiedo?"


translate italian day6_main_2e9daa0c:


    me "Di tutto!"


translate italian day6_main_df4d1541:


    "Ho sbottato bruscamente."


translate italian day6_main_0193dcd6:


    "La leader del Campo mi ha guardato sorpresa."


translate italian day6_main_33eccc5f:


    "Mi sono buttato sul letto, senza spogliarmi."


translate italian day6_main_307422bd:


    mt "Semyon, un pioniere modello non dovrebbe comportarsi così!"


translate italian day6_main_b8a5fff1:


    me "E come dovrebbe comportarsi allora?"


translate italian day6_main_dccb57d2:


    mt "Beh, non così..."


translate italian day6_main_9a8666dc:


    "La leader del Campo sembrava in difficoltà, come se stesse faticando a trovare le parole giuste."


translate italian day6_main_fd3b5cff:


    mt "Deve rispettare i suoi superiori!"


translate italian day6_main_56af3994:


    me "Io la rispetto immensamente, può starne certa..."


translate italian day6_main_105fc865_1:


    mt "Semyon!"


translate italian day6_main_8b538c95:


    "Il mio sarcasmo non è passato inosservato."


translate italian day6_main_87be519f:


    me "E adesso se permette vorrei dormire."


translate italian day6_main_f10bfcfd:


    mt "Aspetta..."


translate italian day6_main_b616628c:


    me "Tra l'altro, quanto è distante la città più vicina? Quando arriva il prossimo bus? Come posso andarmene da qui?"


translate italian day6_main_a2cc86f7:


    "Ho chiesto, in tono sorprendentemente calmo."


translate italian day6_main_68f9d7af:


    "Non c'è stata risposta, e non avrebbe dovuto essercene alcuna."


translate italian day6_main_d16f1af1:


    "Le ho detto tutto questo solo per liberarmi di lei."


translate italian day6_main_6cba906b:


    me "Perché non dice niente?"


translate italian day6_main_3dfe0def:


    mt "Sono stanca, parliamone domani."


translate italian day6_main_03e11cff:


    "La leader del Campo si è alzata e ha spento la luce."


translate italian day6_main_e49c22a1:


    th "Sì, è tutto come mi ha detto quel tizio…"


translate italian day6_main_0d4a118f:


    th "Ma perché conversavo così tranquillamente con lui, mi chiedo?"


translate italian day6_main_c64fd468:


    th "Avrei dovuto tremare dalla paura…"


translate italian day6_main_c516c7b0:


    "Quel pioniere non sembrava molto credibile, ma avevo la sensazione che non fosse in grado di farmi del male."


translate italian day6_main_7af9b647:


    "Appena ho pensato che sarebbe stata una buona idea chiedergli qualcosa di diverso, la stanchezza ha preso il sopravvento e mi sono addormentato."


translate italian day6_un_f1700078:


    "Mi sono svegliato perché qualcuno mi stava scuotendo le spalle."


translate italian day6_un_1a840eb3:


    "Era troppo faticoso per me aprire gli occhi, così mi sono limitato a gemere pateticamente."


translate italian day6_un_6b65ae8f:


    mt "È ora di svegliarsi! Ti perderai l'allineamento!"


translate italian day6_un_df156736:


    "Una volta capito cosa volesse da me Olga Dmitrievna e che ora fosse, mi sono girato verso la parete."


translate italian day6_un_cb9038f3:


    "Ero così stanco che avevo voglia di strozzare la leader del Campo, solo per farla smettere di importunarmi mentre cercavo di riprendermi dopo la giornata infernale di ieri."


translate italian day6_un_05acfb3e:


    mt "Semyon! Alzati subito!"


translate italian day6_un_ceeafad7:


    "Mi sono fatto forza, ho aperto gli occhi e mi sono messo seduto."


translate italian day6_un_1e98f221:


    me "Olga Dmitrievna…{w} Capisco, ma ieri è stata una giornata molto dura per me…"


translate italian day6_un_e57c744c:


    me "Posso dormire almeno oggi?"


translate italian day6_un_10659a77:


    "Ho supplicato."


translate italian day6_un_215606b1:


    mt "Non se ne parla! L'allineamento è obbligatorio per ogni pioniere!{w} E tu l'hai già saltato un paio di volte."


translate italian day6_un_703bafc8:


    "La mia testa era completamente vuota, e così non ho potuto pensare ad alcuna obiezione."


translate italian day6_un_f9754f3e:


    "Dopo un paio di minuti eravamo già in fila alla piazza."


translate italian day6_un_22b326f3:


    "Mi stavo addormentando in piedi, cercando con grande sforzo di non cadere per terra, e così non ho sentito nemmeno una parola di quello che stava annunciando Olga Dmitrievna."


translate italian day6_un_df687591:


    "La maggior parte dei pionieri sembrava sentirsi allo stesso modo."


translate italian day6_un_58d2f9bd:


    "Electronik sbadigliava costantemente, Alisa aveva enormi occhiaie, solo Ulyana sembrava essere piena di energie, come sempre."


translate italian day6_un_316f08e7:


    "Ho fatto scorrere nuovamente lo sguardo lungo la fila di pionieri, ma non ho visto Lena."


translate italian day6_un_ecc81dcb:


    th "Che strano.{w} È sempre stata una ragazza diligente e seria, non è da lei perdersi tali eventi."


translate italian day6_un_3d4c4b35:


    th "D'altra parte, è stata una giornata pesante anche per lei. Così stressante.{w} Probabilmente è caduta in depressione…"


translate italian day6_un_f49ecdba:


    "Anche se un tale comportamento da parte sua mi ha davvero sorpreso."


translate italian day6_un_9512b0f6:


    "Voglio dire, avevo il sospetto che lei non fosse il tipo di persona che voleva sembrare, ma non avrei mai immaginato un cambiamento così drastico."


translate italian day6_un_f2c60111:


    "Lena – paradossalmente! – mi ricordava un po' Alisa, era persino più dura e brutale di lei, in certi momenti."


translate italian day6_un_e4d65836:


    "E ora non ero sicuro di come comportarmi in sua presenza – avevo semplicemente timore di lei."


translate italian day6_un_ff2fa3df:


    "Finalmente l'allineamento è giunto al termine, e i pionieri si sono trascinati verso la colazione."


translate italian day6_un_20de4020:


    "Miku mi ha raggiunto nei pressi della mensa."


translate italian day6_un_b0f1c022:


    mi "Semyon, buongiorno! Dormito bene? Hai sognato qualcosa? Come stai? Pronto per la colazione?"


translate italian day6_un_8d857931:


    "Come sempre, ha blaterato senza interruzione, decorando il tutto con un dolce sorriso."


translate italian day6_un_e7ce88cd:


    me "Sto bene."


translate italian day6_un_56b00510:


    "Ho risposto pigramente."


translate italian day6_un_ec604917:


    mi "Oggi è una giornata un po' brutta… Forse a causa del tempo – è piuttosto buio, forse pioverà… Quindi sì! Ognuno è triste e cupo! E così ho pensato: forse è successo qualcosa, ma nessuno mi ha informata."


translate italian day6_un_aafa05de:


    mi "Riesci a immaginarlo! Qualcosa è successo e tutti lo sanno tranne me. Ero triste a causa di questo, ma poi…"


translate italian day6_un_bb18abf7:


    me "Non preoccuparti, non ti perderai la fine del mondo."


translate italian day6_un_27c62366:


    "Ho risposto con sarcasmo."


translate italian day6_un_57d2c9aa:


    mi "Eh?"


translate italian day6_un_94555fb6:


    "Sembrava così immersa nel suo monologo che stava ignorando completamente quello che accadeva intorno a lei."


translate italian day6_un_a7320a03:


    me "E se per caso tu dovessi perdertela – te lo farò sapere io…"


translate italian day6_un_c63fd243:


    me "Niente. Buon appetito."


translate italian day6_un_63855039:


    "Ho marciato verso la mensa con passo deciso, per ottenere la mia porzione giornaliera di grassi, proteine e carboidrati."


translate italian day6_un_88b41ad3:


    "Con mia sorpresa, un posto nell'angolo più lontano era libero."


translate italian day6_un_c21d2e19:


    "Mi sono seduto, sforzandomi di mantenere un'espressione che potesse far intendere a tutti che non c'era alcun motivo di sedersi accanto a me, a meno che non si fosse trattato di un'emergenza."


translate italian day6_un_b5a8b072:


    "Volevo soltanto stare da solo e pensare.{w} Inoltre, tenere la mente lontana dalle altre cose mi faceva sentire meno assonnato."


translate italian day6_un_c0c4e707:


    sl "Ciao, ti spiace se mi siedo qui?"


translate italian day6_un_3501408a:


    "Non avevo notato Slavya, che stava lì in piedi."


translate italian day6_un_d80e4062:


    th "Strano…"


translate italian day6_un_9513cd87:


    me "Fai pure…"


translate italian day6_un_d20f4795:


    "Le ho detto, dopo aver esitato un secondo."


translate italian day6_un_59a6b536:


    sl "Sei di cattivo umore?"


translate italian day6_un_997c151d:


    me "Un po'."


translate italian day6_un_e4d522d4:


    sl "È successo qualcosa?"


translate italian day6_un_b97e3fd5:


    me "Non proprio…"


translate italian day6_un_87297b64:


    sl "Non c'è bisogno di dirmelo se non vuoi."


translate italian day6_un_cbce3f49:


    me "Non c'è niente da dire."


translate italian day6_un_6488c1ac:


    "Abbiamo mangiato in silenzio, finché non le ho chiesto:"


translate italian day6_un_79ee911e:


    me "Dove sei andata la notte scorsa?"


translate italian day6_un_e1994127:


    sl "Io? Oh, volevo solo stare un po' da sola."


translate italian day6_un_4a2c94f6:


    me "Questo non è da te."


translate italian day6_un_f59ecd73:


    "Mi sono sentito un po' più vivo."


translate italian day6_un_c6212685:


    sl "Veramente? Beh, forse…{w} Anche tu, non capita spesso di vederti così cupo."


translate italian day6_un_0139b861:


    th "Potrebbe aver ragione."


translate italian day6_un_d560542b:


    "Persino nelle situazioni più sfavorevoli ho sempre cercato di avere una visione positiva delle cose."


translate italian day6_un_e4c10516:


    "Non è che fossi ottimista, è solo che cercavo di non sentirmi troppo giù."


translate italian day6_un_24507d3d:


    "Ricordando la mia vita precedente, era una cosa naturale – una volta che permetti alla depressione di prendere il sopravvento su di te, il cappio inizia a guardarti in modo molto suggestivo."


translate italian day6_un_6ab05865:


    me "Forse."


translate italian day6_un_0565c647:


    "Quando Slavya ha finito la sua colazione, io stavo ancora raccogliendo pigramente il mio porridge con il cucchiaio."


translate italian day6_un_d23277e3:


    sl "Io vado…"


translate italian day6_un_e4eadbc7:


    me "A proposito, hai visto Lena?"


translate italian day6_un_e00071aa:


    sl "No, perché me lo chiedi?"


translate italian day6_un_2283b18c:


    me "Non l'ho vista all'allineamento stamattina.{w} È strano… Non è da lei."


translate italian day6_un_5d832dbe:


    sl "Forse hai ragione.{w} Ma non credo sia un grosso problema, ad essere onesti."


translate italian day6_un_be2f234f:


    me "Sì, certo, mi stavo solo chiedendo…"


translate italian day6_un_2da87938:


    "Sono rimasto seduto ancora per un po', e poi me ne sono andato senza aver finito la mia colazione."


translate italian day6_un_fdfb7569:


    th "Oggi è piuttosto cupo.{w} La prima giornata uggiosa da quando sono qui.{w} In qualche modo mi ero già abituato al caldo e allo splendore del sole."


translate italian day6_un_f69f6bef:


    "Il calore che riusciva a placarsi soltanto di notte sembrava essere una componente insostituibile di questo luogo."


translate italian day6_un_d29d26f8:


    "Pare che anche in questo posto il tempo cambiasse, dopotutto."


translate italian day6_un_d31c084d:


    th "Come se si tramutasse in base al mio umore…"


translate italian day6_un_df21ce17:


    th "È la prova che questo mondo o i suoi creatori sono degli esseri senzienti e hanno grandi capacità narrative."


translate italian day6_un_b83c2795:


    "Certe volte avrei voluto davvero considerare solo una delle mie teorie, concentrandomi su di essa e dimenticandomi tutte le altre."


translate italian day6_un_6752541e:


    "Decidere che fosse opera degli alieni, o che fossi in un universo parallelo.{w} O la stregoneria, o gli esperimenti militari..."


translate italian day6_un_f82edbaf:


    "Sarebbe stato sufficiente sceglierne una e non pensarci mai più!"


translate italian day6_un_89aa71e4:


    "Dovevo smetterla di pensare a tutte le possibili spiegazioni a questa situazione, passando costantemente da una all'altra.{w} Sarebbe bastato concentrarsi su una sola di esse."


translate italian day6_un_78520b75:


    th "Ma è impossibile."


translate italian day6_un_4b4dd244:


    th "Non ho (quasi) nessuna prova.{w} Non mi è ancora successo niente di straordinario."


translate italian day6_un_a0795afe:


    "È vero, alcune cose strane erano accadute. Ma sarebbero potute accadere anche nel mondo reale."


translate italian day6_un_35362687:


    "Dopo l'ennesimo ciclo di pensieri di quel genere, mi sono ritrovato alla fermata della tratta 410, davanti al cancello del campo «Sovyonok»."


translate italian day6_un_4aa5ce10:


    th "Nessuna risposta, nessun suggerimento, nessun indizio…"


translate italian day6_un_4fa5905f:


    "Andavo dove mi portavano le mie gambe."


translate italian day6_un_f9c294f6:


    "Era tranquillo nella «zona residenziale».{w} Non c'era anima viva, ad essere precisi."


translate italian day6_un_6a121a11:


    "Il mio stupore è aumentato ancora di più quando Electronik è apparso da dietro l'angolo."


translate italian day6_un_a3828cd7:


    "Stavo per chiamarlo ma mi sono fermato, perché si stava dirigendo un po' troppo impetuosamente verso una direzione sconosciuta."


translate italian day6_un_49160043:


    th "È stano, non è da lui…"


translate italian day6_un_4fa01f29:


    th "E comunque, di cosa potrei parlare con Electronik?{w} E sì, dovrei iniziare io la conversazione…"


translate italian day6_un_7e840313:


    th "Sembra che io non abbia proprio idea di cosa fare."


translate italian day6_un_42f4c858:


    "Tuttavia, sarebbe stato interessante seguirlo per scoprire dove si stesse dirigendo."


translate italian day6_un_de5de174:


    "Da bambino mi piaceva giocare alle spie, e adesso avevo l'occasione di mettermi nei panni di una vera spia!"


translate italian day6_un_5a5a09fe:


    "Ho deciso di non seguirlo accovacciato di nascosto, ma l'ho semplicemente pedinato senza far rumore, mantenendo le distanze."


translate italian day6_un_6b9662bf:


    "Ben presto siamo arrivati alla biblioteca.{w} Electronik ha bussato ed è entrato."


translate italian day6_un_ad78315b:


    "Mi sono messo dietro a un grande albero, in modo che nessuno potesse vedermi, e ho atteso."


translate italian day6_un_a20cefa7:


    "..."


translate italian day6_un_c1a2fbe8:


    "Non si è fatto vedere per parecchio tempo."


translate italian day6_un_2cee12ba:


    th "Forse la mia è stata un'idea stupida, perché in realtà, cosa c'è di strano nel fatto che lui sia andato alla biblioteca?{w} Forse voleva semplicemente prendere qualcosa da leggere?"


translate italian day6_un_41a8466c:


    th "Stava camminando così rapidamente, e allora?"


translate italian day6_un_539d4165:


    th "Forse ha delle cose da sbrigare…"


translate italian day6_un_5b0195d4:


    "I miei pensieri sono stati interrotti dal rumore di una porta che ha sbattuto violentemente."


translate italian day6_un_66094c34:


    "Ho guardato in direzione della biblioteca e ho visto Electronik, che stava correndo via, e Zhenya che lo inseguiva, urlandogli:"


translate italian day6_un_07d3d267:


    mz "Non voglio più sentire niente del genere! E non ti voglio più vedere!"


translate italian day6_un_d825c319:


    "Sono passati davanti a me, ma ovviamente erano così coinvolti nell'inseguimento che non si sono accorti della mia presenza."


translate italian day6_un_17db73bb:


    "L'intera situazione sembrava piuttosto divertente, e avevo decisamente bisogno di scoprire di più a riguardo."


translate italian day6_un_8bbec564:


    th "Mi chiedo, verso quale destinazione potrebbe affrettarsi Electronik in quel modo?"


translate italian day6_aidpost_5f9ed739:


    "Forse all'infermeria?"


translate italian day6_aidpost_ceb48884:


    th "Certo che no, che idea stupida..."


translate italian day6_dinner_e22dfa0f:


    "Nascondersi nella mensa non sarebbe stata una cattiva idea."


translate italian day6_dinner_78f58283:


    "Ma non era neppure lì."


translate italian day6_after_map_e40ce72e:


    th "Sì, è abbastanza ovvio che sia corso dritto verso la sua tana al circolo di cibernetica."


translate italian day6_after_map_fcae6aa1:


    "Sono entrato senza bussare, ma non ho trovato nessuno all'interno."


translate italian day6_after_map_249528f8:


    me "Electronik, sono io!"


translate italian day6_after_map_71f484c6:


    "Ho sentito dei passi provenire dalla stanza accanto, e ben presto Electronik è spuntato da lì."


translate italian day6_after_map_13e9141f:


    el "Ciao Semyon! Stavo solo…"


translate italian day6_after_map_930b325a:


    "I suoi occhi si muovevano con aria colpevole, e sulla sua camicia c'erano evidenti tracce di sudore."


translate italian day6_after_map_1e590c6b:


    me "Impegnato nello sport, vedo.{w} Scattante."


translate italian day6_after_map_3573e50d:


    el "Io…{w} Hai visto tutto?"


translate italian day6_after_map_594616b0:


    "Mi ha chiesto, rassegnato."


translate italian day6_after_map_ccf2f1f7:


    me "Sì, per puro caso. Così ho deciso di farti visita per sapere come stai, e per chiederti cosa sia successo."


translate italian day6_after_map_603882a4:


    el "Niente di speciale, davvero…"


translate italian day6_after_map_61b29886:


    th "Già, fuggire da una bibliotecaria infuriata – non è niente di speciale…"


translate italian day6_after_map_0a1f52d0:


    me "Puoi dirmelo."


translate italian day6_after_map_260966c7:


    "Ho fatto un sorriso disarmante."


translate italian day6_after_map_e53cf926:


    el "Davvero? E non lo dirai in giro…"


translate italian day6_after_map_88f39b4b:


    me "Ma certo! Sarò muto come un pesce! Te lo giuro!"


translate italian day6_after_map_8636977a:


    "Forse ho esagerato un po', ma sembrava che io l'avessi convinto."


translate italian day6_after_map_ab8f7115:


    el "Va bene."


translate italian day6_after_map_a4e10992:


    "Ha fatto un profondo respiro, raccogliendo le forze."


translate italian day6_after_map_4b01cbdb:


    el "Sai, Zhenya mi è piaciuta fin dal primo giorno…"


translate italian day6_after_map_3763685c:


    "Dopo quelle parole ho sentito il bisogno di lasciarmi cadere a terra e rotolare sul pavimento, scosso da forti attacchi di risate, ma mi sono trattenuto per rispetto nei suoi confronti. E le sue parole seguenti mi sono entrate in un orecchio e mi sono uscite dall'altro."


translate italian day6_after_map_7d9d9a52:


    el "…e così mi sono deciso. Beh, hai già visto com'è andata a finire…"


translate italian day6_after_map_58f9de3f:


    th "Un tipo coraggioso, senza dubbio."


translate italian day6_after_map_b8456c80:


    me "Beh…{w} Provaci ancora e vedrai che avrai successo!"


translate italian day6_after_map_1a0d5f6e:


    "Gli ho dato una pacca sulla spalla, riuscendo a malapena a trattenere le risate."


translate italian day6_after_map_3ebcd212:


    el "Grazie per il sostegno."


translate italian day6_after_map_d5ffee81:


    "Ha sorriso tristemente."


translate italian day6_after_map_3b5c973e:


    me "Ok, adesso devo andare, ho un po' di cose da fare."


translate italian day6_after_map_6e47a0d1:


    "Sono piombato fuori dall'edificio come un proiettile, e finalmente ho potuto ridere liberamente."


translate italian day6_after_map_a3fe05d4:


    th "Anche se, pensandoci bene, Electronik e Zhenya formerebbero una bella coppia."


translate italian day6_after_map_5ea74e09:


    th "Sembrano fatti l'uno per l'altra.{w} Strano che Zhenya l'abbia respinto…"


translate italian day6_after_map_9c84def6:


    "Mi sono diretto verso la piazza, pensando ancora a quell'avvenimento."


translate italian day6_after_map_4c7202cc:


    th "Alla fine Electronik non è poi così sempliciotto..."


translate italian day6_after_map_27837c94:


    th "Così su due piedi, ha dichiarato il suo amore, pur essendo consapevole che la cosa potesse non andare a buon fine.{w} O magari l'ha fatto solo perché è semplice come il pane…?"


translate italian day6_after_map_b874d159:


    th "In entrambi i casi, il suo è stato un gesto sincero e spontaneo."


translate italian day6_after_map_d0e49694:


    th "Persone più complessate potrebbero impiegare ore, giorni, mesi, anni a pensare a come dichiararsi nel modo migliore possibile, quali conseguenze potrebbero esserci, e se ne valga la pena.{w} Io sicuramente farei così."


translate italian day6_after_map_16546c05:


    th "No, anzi, l'ho già fatto."


translate italian day6_after_map_604e72aa:


    th "Lui invece l'ha detto e basta.{w} Senza successo, naturalmente, ma sarebbe potuta andare anche diversamente…"


translate italian day6_after_map_69934023:


    "Tali pensieri mi hanno reso totalmente malinconico, tanto che la sirena del pranzo, che solitamente attendevo con impazienza, non ha innescato alcuna emozione dentro di me."


translate italian day6_after_map_db39b8db:


    "La mensa era piena zeppa.{w} Sembrava che il Campo stesse lentamente emergendo dalla depressione mattutina."


translate italian day6_after_map_583f6bbf:


    "Forse era anche merito del sole che è appena apparso tra le nuvole, o forse era per qualcos'altro che mi sono perso mentre stavo pedinando Electronik."


translate italian day6_after_map_252e9ca8:


    "Gli unici posti liberi erano vicino a Ulyana e Alisa."


translate italian day6_after_map_09b90b56:


    "Ho raccolto le mie forze e mi sono incamminato verso di loro – dopotutto mangiare era ancora una necessità."


translate italian day6_after_map_ec04d37b:


    me "Posso sedermi qui con voi?"


translate italian day6_after_map_b4f1aecf:


    dv "Oh, zitto e siediti."


translate italian day6_after_map_57fa2035:


    us "Perché sei così cupo?"


translate italian day6_after_map_35ce6c29:


    me "Magari sei tu a essere troppo allegra.{w} Io sto cercando di mantenere l'equilibrio energetico nell'universo."


translate italian day6_after_map_a064eabe:


    "Ulyana ha ridacchiato."


translate italian day6_after_map_8c8bd74b:


    "Stranamente, le ragazze non mi hanno preso in considerazione, e hanno parlato delle loro faccende."


translate italian day6_after_map_cc02e7a3:


    "In un primo momento ho pensato che fosse una cosa positiva, ma poi mi è sembrato che mi stessero ignorando."


translate italian day6_after_map_2b3338c6:


    me "A proposito, dov'è Lena?"


translate italian day6_after_map_11255df4:


    "A causa di tutti i miei pensieri mi ero completamente scordato di lei."


translate italian day6_after_map_3e69c35f:


    dv "Non lo so."


translate italian day6_after_map_d4d17245:


    "Ha risposto Alisa, distrattamente."


translate italian day6_after_map_84eb34c3:


    me "Non si è ancora fatta vedere?"


translate italian day6_after_map_f355226b:


    dv "Come puoi notare…"


translate italian day6_after_map_3fd95c7c:


    "Mi sono guardato attorno, ma non l'ho vista da nessuna parte."


translate italian day6_after_map_9fcb4846:


    me "E nessuno ha sentito niente di lei?"


translate italian day6_after_map_e8936f84:


    dv "No."


translate italian day6_after_map_ae30c1d7:


    me "Non vi sembra strano?"


translate italian day6_after_map_c1788a1d:


    us "Cosa c'è di così strano?{w} Forse sta leggendo da qualche parte, o magari sta dormendo, o qualcos'altro."


translate italian day6_after_map_01b5873a:


    me "Sembra più che tu stia parlando di te stessa."


translate italian day6_after_map_f3820ec1:


    dv "Non sono affari che ti riguardano, vero?"


translate italian day6_after_map_c501a590:


    "È intervenuta rabbiosamente Alisa."


translate italian day6_after_map_61d482ed:


    me "Beh, se una persona è scomparsa…"


translate italian day6_after_map_37715109:


    dv "Non eri così preoccupato quando Shurik è scomparso!"


translate italian day6_after_map_74053528:


    me "Non è la stessa cosa…"


translate italian day6_after_map_0e2114f3:


    dv "Ma davvero? E perché, mi chiedo?"


translate italian day6_after_map_1b8b414c:


    "Non avevo alcuna risposta a quella domanda, e così mi sono limitato a fissare Alisa con sguardo assente, prima di tornare al mio piatto."


translate italian day6_after_map_517d0fc9:


    "Lei non ha insistito nel continuare la conversazione."


translate italian day6_after_map_a20cefa7:


    "..."


translate italian day6_after_map_b686c259:


    "Il pranzo si è concluso. Mi sono alzato in silenzio e sono uscito dalla mensa senza salutare."


translate italian day6_after_map_9d0e81e1:


    th "E così, Lena è scomparsa…{w} E adesso cosa devo fare?"


translate italian day6_after_map_be3ae4b1:


    th "D'altra parte, perché dovrei fare qualcosa?{w} Perché proprio io?{w} Dove mi trovo, e perché sono in questo luogo, chi sono tutte quelle persone?"


translate italian day6_after_map_25de3652:


    th "Non posso essere del tutto certo che Lena sia ciò che sembra."


translate italian day6_after_map_eeb3cd87:


    th "Forse tutto questo non esiste nemmeno, quindi perché dovrei preoccuparmi...?"


translate italian day6_after_map_fd2a5fac:


    th "Anche se per me Lena è ancora la stessa Lena.{w} La modesta e tranquilla ragazza che avevo incontrato il primo giorno."


translate italian day6_after_map_7edba5d0:


    th "E persino quel suo strano comportamento non è riuscito ad influenzare il mio atteggiamento nei suoi confronti."


translate italian day6_after_map_22235f9d:


    th "Alla fine, non posso avere la certezza che io esista ancora.{w} Così, fintanto che il mondo appare logico (o almeno in una certa misura), devo giocare secondo le sue regole!"


translate italian day6_after_map_fd5f28b2:


    "Mi sono incamminato velocemente verso la casetta della leader del Campo."


translate italian day6_after_map_c028139c:


    "Una volta dentro, ho visto Olga Dmitrievna sdraiata sul letto, stava leggendo un libro."


translate italian day6_after_map_1246e12e:


    me "Per caso sa dov'è Lena?"


translate italian day6_after_map_017acf74:


    mt "No, perché me lo chiedi?"


translate italian day6_after_map_07ba9e65:


    me "Non si trova da nessuna parte. Ha saltato sia la colazione che il pranzo!"


translate italian day6_after_map_9dd49ab7:


    mt "E allora?"


translate italian day6_after_map_4de002b5:


    "Mi ha guardato senza capire."


translate italian day6_after_map_1c44d4fc:


    me "Come sarebbe a dire «e allora»?{w} Quando Shurik è scomparso l'intero Campo si è mobilitato a cercarlo fin dal primo mattino."


translate italian day6_after_map_fe72fec3:


    mt "Non ti capisco."


translate italian day6_after_map_6fa16493:


    "C'era di nuovo qualcosa di strano nella leader del Campo.{w} Si stava comportando in maniera del tutto incomprensibile, illogica."


translate italian day6_after_map_ad45a807:


    me "Pensa che sia normale? Allora, dov'è lei adesso?"


translate italian day6_after_map_9c19b5ad:


    mt "Non lo so."


translate italian day6_after_map_53405852:


    "Ha risposto tranquillamente."


translate italian day6_after_map_b8029b05:


    me "Questo è troppo!"


translate italian day6_after_map_6ea78a01:


    "Stavo iniziando a perdere la pazienza."


translate italian day6_after_map_8009c859:


    mt "Chiedi a Miku – è la sua compagna di stanza, dopotutto."


translate italian day6_after_map_c6b76c70:


    th "È una buona idea, perché ovviamente non otterrò alcuna risposta qui."


translate italian day6_after_map_b424b5bf:


    "Sono uscito dalla stanza sbattendo la porta, e sono andato a cercare la casetta di Miku e Lena."


translate italian day6_after_map_b83fcbd4:


    "Fortunatamente la ragazza dell'orchestra mi aveva detto dove abitava, il che mi ha permesso di raggiungere la porta della loro casetta dopo appena un minuto."


translate italian day6_after_map_df84c43e:


    "Avrei dovuto bussare, ma per qualche motivo non ci riuscivo."


translate italian day6_after_map_3bda9055:


    "Dopo qualche respiro profondo mi sono deciso e ho bussato."


translate italian day6_after_map_c7ed3d71:


    mi "Avanti!"


translate italian day6_after_map_3417c9cb:


    "Ho sentito una voce familiare."


translate italian day6_after_map_7299645f:


    me "Ciao… Sai dov'è Lena?"


translate italian day6_after_map_02794ad5:


    mi "No… Oggi non l'ho vista. La stai cercando, vero?"


translate italian day6_after_map_374cfeae:


    me "Non pensi che sia strano?"


translate italian day6_after_map_9d504cc9:


    "A quel punto ho iniziato a sospettare che tutti mi stessero nascondendo qualcosa riguardo a Lena, di una cospirazione in corso, del mio arrivo in questo luogo, o dell'assassinio di Kennedy, e centinaia di altre cose terribili."


translate italian day6_after_map_2f93b124:


    mi "Beh… Sai, forse… Pensavo che se ne fosse andata da qualche parte e poi mi sono persa nelle mie faccende – la colazione, il circolo musicale, le pulizie, e poi il pranzo, e poi, e poi…"


translate italian day6_after_map_f7d66ec2:


    me "Va bene, ho capito… E invece ieri? Ti è sembrata strana?"


translate italian day6_after_map_e9cf8521:


    mi "Beh… È tornata tardi ed è andata subito a dormire, non sono riuscita a notare qualcosa di particolare."


translate italian day6_after_map_86efd052:


    "Sembrava che non potessi ottenere alcuna risposta neppure da lei."


translate italian day6_after_map_3066598f:


    me "Grazie."


translate italian day6_after_map_d9b28d3a:


    "Ho detto aspramente, e me ne sono andato."


translate italian day6_after_map_6130e681:


    "In quel momento mi è sembrato che la Lena scomparsa fosse l'unica persona viva in quel mucchio di manichini parlanti, e dovevo assolutamente trovarla!"


translate italian day6_after_map_e739b113:


    "Ma sembrava impossibile riuscire a farlo da solo, così sono andato in cerca di aiuto."


translate italian day6_after_map_a35b54c4:


    th "Chi sarebbe il più disposto ad aiutarmi?{w} Naturalmente Slavya!"


translate italian day6_after_map_a328c184:


    "Probabilmente a quell'ora era impegnata con le pulizie in qualche luogo.{w} Ad esempio alla piazza."


translate italian day6_after_map_b89225c8:


    "Così sono andato lì."


translate italian day6_after_map_15d046bb:


    "Il mio sesto senso non mi ha tradito."


translate italian day6_after_map_1a8f3935:


    me "Ciao!"


translate italian day6_after_map_9104ac12:


    sl "Ehi!"


translate italian day6_after_map_2c385c8f:


    me "Hai visto Lena?"


translate italian day6_after_map_e00071aa:


    sl "No. Perché me lo chiedi?"


translate italian day6_after_map_c72e7da6:


    me "Nessuno l'ha vista da questa mattina, non c'era durante la colazione, e nemmeno a pranzo."


translate italian day6_after_map_cffc6886:


    sl "Strano."


translate italian day6_after_map_ec0bd519:


    me "Anch'io penso che sia, per usare un eufemismo, «strano».{w} Puoi aiutarmi a cercarla?"


translate italian day6_after_map_ce25ce10:


    sl "Oh, mi dispiace.{w} Magari più tardi? Devo finire di pulire qui…"


translate italian day6_after_map_19e9e137:


    "È stato come un fulmine a ciel sereno."


translate italian day6_after_map_96afba80:


    "Ho indietreggiato di qualche passo, e sono corso via da quel luogo."


translate italian day6_after_map_235cdb53:


    th "No, quella non era lei! È come se qualcuno l'avesse rimpiazzata!{w} Non solo lei, ma anche tutti gli altri abitanti del Campo."


translate italian day6_after_map_7012273e:


    th "Che sta succedendo?{w} La cosa più strana è che tutto questo non ha niente a che fare con me, ma con Lena!"


translate italian day6_after_map_e760a29d:


    th "Forse anche lei è arrivata qui allo stesso modo in cui sono arrivato io?"


translate italian day6_after_map_83f4ce09:


    th "Esatto!{w} Potrebbe essere questo il motivo per cui si comporta timidamente per la maggior parte del tempo."


translate italian day6_after_map_105dafff:


    th "No, aspetta…{w} Ma come può conoscere Alisa allora…?"


translate italian day6_after_map_bf8e6d5c:


    th "No, qualcosa non torna."


translate italian day6_after_map_cb18b97c:


    "La mia testa stava per esplodere, mi mancava il respiro."


translate italian day6_after_map_915d90ea:


    "Dopo aver ripreso fiato ho dato un'occhiata attorno, e mi sono ritrovato alla fermata del bus. Mi sono seduto sul marciapiede, coprendomi il viso con le mani."


translate italian day6_after_map_cbafac0b:


    th "Se prima niente dipendeva da me, ma tutto scorreva relativamente liscio, allora adesso sono impotente, come sempre del resto, ma la situazione è totalmente diversa."


translate italian day6_after_map_a980985a:


    th "Guardare la battaglia da lontano senza pericolo di essere coinvolto ed essere sul fronte senza riuscire ad aiutare le persone a cui tieni, sono due cose totalmente diverse."


translate italian day6_after_map_a20cefa7_1:


    "..."


translate italian day6_after_map_fff50899:


    "Me ne stavo seduto."


translate italian day6_after_map_f3fefe18:


    "Il tempo passava e il sole ha iniziato a tramontare."


translate italian day6_after_map_62739121:


    th "Probabilmente la cena è già iniziata."


translate italian day6_after_map_3edce0f9:


    th "Ma che differenza fa, non mi va comunque di mangiare."


translate italian day6_after_map_41cc0c56:


    "Mi sono alzato con fatica e ho arrancato verso il Campo, con le gambe gommose."


translate italian day6_after_map_7b58f3a3:


    th "Come sempre, l'unica cosa rimasta da fare è aspettare…"


translate italian day6_after_map_a4552563:


    "Ho deciso di andare alla spiaggia."


translate italian day6_after_map_a2ad895b:


    th "Lì potrò pensare in pace, mentre tutti sono a cena."


translate italian day6_after_map_4bbcd690:


    th "Ma pensare a cosa? Quanto si può pensare?"


translate italian day6_after_map_6de0d6ca:


    "Tuttavia, i miei piani sono stati rovinati."


translate italian day6_after_map_9fc97989:


    "Sulla spiaggia ho incontrato Zhenya, il che mi ha sorpreso molto."


translate italian day6_after_map_eb39fdd5:


    me "Sei venuta qui anche tu?"


translate italian day6_after_map_47641ee9:


    "Mi ha guardato da dietro gli occhiali."


translate italian day6_after_map_43937691:


    mz "Pensi che io non sia un essere umano?"


translate italian day6_after_map_d9f75732:


    me "No, non intendevo dire questo."


translate italian day6_after_map_ed1dc0ad:


    mz "Allora cosa intendevi?"


translate italian day6_after_map_562a22e9:


    me "Niente…{w} A proposito, hai visto Lena?"


translate italian day6_after_map_c48a46a8:


    mz "No."


translate italian day6_after_map_f2e007f0:


    me "Capisco…"


translate italian day6_after_map_4e3a8dcb:


    mz "Cosa vuoi da lei?"


translate italian day6_after_map_2dfb23fa:


    me "Beh… Nessuno l'ha vista da ieri sera."


translate italian day6_after_map_e8b5756a:


    mz "Pensi che qualcuno potrebbe perdersi in questo Campo?"


translate italian day6_after_map_56d4e39c:


    "Ha riso forte."


translate italian day6_after_map_cf3d96a9:


    me "Shurik ci è riuscito…"


translate italian day6_after_map_a4dfd52d:


    mz "Quello è stato un caso speciale…{w} Quei ragazzi sono come due piselli in un baccello, non si sa mai cosa aspettarsi da loro."


translate italian day6_after_map_6ebc2860:


    "Mi sono subito ricordato l'incidente avvenuto in mattinata."


translate italian day6_after_map_340d0570:


    me "Dimmi…{w} Perché stavi rincorrendo Electronik questa mattina?"


translate italian day6_after_map_9889b720:


    "Zhenya sembrava a disagio."


translate italian day6_after_map_63dcdb44:


    mz "Non sono affari tuoi."


translate italian day6_after_map_efa19ecb:


    me "Stavo solo chiedendo."


translate italian day6_after_map_54df90eb:


    mz "Perché è uno stupido!"


translate italian day6_after_map_36d3e865:


    "Dopo quelle parole mi ha dato le spalle."


translate italian day6_after_map_33d14fd3:


    me "Forse non dovresti essere così dura nei suoi confronti."


translate italian day6_after_map_82514e53:


    "Non mi era molto chiaro se stessi difendendo Electronik o se stessi solo cercando di mantenere la conversazione."


translate italian day6_after_map_16cd0044:


    mz "E allora come dovrei trattarlo?"


translate italian day6_after_map_5f7ad39e:


    me "Beh, dagli una possibilità…{w} Sai, un atto del genere richiede molto coraggio."


translate italian day6_after_map_b2adba60:


    mz "Lo dici… come se fosse qualcosa di speciale, una sorta di prodezza.{w} Tu saresti in grado di farlo?"


translate italian day6_after_map_846fcae4:


    "Ho pensato, e dopo un po' le ho risposto."


translate italian day6_after_map_e05aaca3:


    me "Non lo so…{w} Non ne ho avuto ancora l'occasione."


translate italian day6_after_map_a9c3105e:


    mz "Spero che l'avrai presto."


translate italian day6_after_map_a7ce299f:


    "Mi ha detto bruscamente, e si è diretta verso la mensa."


translate italian day6_after_map_a6b3e5a5:


    "Mi sono seduto sulla sabbia e ho pensato."


translate italian day6_after_map_e55707cf:


    th "È vero, Electronik è stato in grado di farlo, ma potrei io fare altrettanto?{w} Questa è una bella domanda, una grande domanda."


translate italian day6_after_map_182cea07:


    th "Se ne avessi l'occasione…{w} Ma quale, e quando?"


translate italian day6_after_map_df7d6c66:


    th "È sempre più facile pensare a qualcosa di effimero, per essere pronto ad affrontare decine di possibili situazioni, per portarsi avanti e prevedere le innumerevoli conseguenze..."


translate italian day6_after_map_20124702:


    th "Ma la maggior parte delle volte tutto va diversamente."


translate italian day6_after_map_7fad78b3:


    th "Persino un piccolo evento potrebbe bastare a rovinare tutti i tuoi piani."


translate italian day6_after_map_04ab0520:


    th "E se non sei pronto a {i}farlo{/i} in qualsiasi istante, in qualsiasi momento, in qualsiasi circostanza, se ti senti pronto solo quando tutto è esattamente come hai previsto, allora è molto improbabile che tu riesca ad ottenere qualcosa di concreto."


translate italian day6_after_map_ab503b46:


    th "Pertanto, l'unica risposta corretta alla domanda di Zhenya è «no»."


translate italian day6_after_map_60b33880:


    th "Non una risposta ambigua, forzata, incerta, timida.{w} Semplicemente un «no»."


translate italian day6_after_map_7889693d:


    th "Esistono due semplici risposte: «sì» e «no»."


translate italian day6_after_map_f4bb7bbe:


    "È sempre stato difficile per me capirlo."


translate italian day6_after_map_90e26c1c:


    "In mezzo a quei due estremi mi sono sempre inventato innumerevoli risposte come «forse», «può darsi», «probabilmente», e «non sono sicuro ma ci proverò»…"


translate italian day6_after_map_a20cefa7_2:


    "..."


translate italian day6_after_map_e51168a0:


    "Ero così immerso nei miei pensieri che non mi sono accorto del buio che aveva avvolto l'intero Campo."


translate italian day6_after_map_3aa3c67c:


    "In altre circostanze sarei andato a dormire a quest'ora…{w} Ma cercare Lena di notte non era una buona idea."


translate italian day6_after_map_d8482e89:


    "Mi sono alzato e ho vagato lentamente senza meta."


translate italian day6_after_map_c84711ef:


    "Ben presto il sentiero mi ha portato al campo sportivo."


translate italian day6_after_map_97ec2962:


    "Sono rimasto lì per un momento, stavo per andarmene, quando ho sentito dei rumori nelle vicinanze."


translate italian day6_after_map_cfc15ee6:


    th "Clap, un altro clap…{w} Qualcosa di terribilmente familiare!"


translate italian day6_after_map_32a97e95:


    "Sono corso verso il campo da pallavolo e ho visto Lena, che stava cercando di colpire il volano con la racchetta, senza successo."


translate italian day6_after_map_59811faa:


    "Sono rimasto pietrificato a lungo."


translate italian day6_after_map_550358a1:


    "La mia testa era completamente vuota, mi sono limitato a guardarla, con una gioia immensa.{w} Gioia di averla ritrovata, gioia nel vederla."


translate italian day6_after_map_f86b9a25:


    "Alla fine ho ripreso i sensi e mi sono deciso ad avvicinarmi a lei…"


translate italian day6_after_map_d092d462:


    "Ma dopo un paio di passi mi sono fermato."


translate italian day6_after_map_cf3bc831:


    th "E adesso cosa le dico?{w} «Sono contento di averti ritrovata»?{w} «Dove sei stata, mi stavo preoccupando»?"


translate italian day6_after_map_de715a93:


    th "Dopo la conversazione di ieri è molto improbabile che lei voglia vedermi…"


translate italian day6_after_map_e0f727b2:


    th "E cosa le dirò se mi chiederà perché la stavo cercando?{w} Del perché io fossi preoccupato?"


translate italian day6_after_map_4baf4e5a:


    th "Non lo so neppure io…{w} Probabilmente perché non si faceva vedere da troppo tempo."


translate italian day6_after_map_db5327a9:


    th "Forse anche se non si fosse trattato di lei, ma di qualche altra persona, mi sarei preoccupato allo stesso modo."


translate italian day6_after_map_9e166d2c:


    th "Forse se ieri mi fossi comportato diversamente, oggi lei non sarebbe scomparsa."


translate italian day6_after_map_b1d2df25:


    th "Non sono sicuro, ma cercherò di inventarmi qualcosa di adeguato…"


translate italian day6_after_map_5c5017b8:


    "Ho fatto un altro passo e mi sono fermato di nuovo."


translate italian day6_after_map_36e92b6c:


    th "Probabilmente, forse, può darsi, non è sicuro…"


translate italian day6_after_map_fac2cdca:


    th "Ancora e ancora, quelle parole sono apparse nella mia testa, nella mia vita…"


translate italian day6_after_map_ee8a9b71:


    th "Inconsapevolmente, involontariamente!"


translate italian day6_after_map_10d6f972:


    th "Ma perché? A quale scopo?"


translate italian day6_after_map_6179f9a3:


    th "Devo prendere una decisione! Una volta per tutte!"


translate italian day6_after_map_beab36a3:


    th "Anche se…{w} Esistono due semplici parole – «sì» e «no»."


translate italian day6_after_map_bc795975:


    "Finalmente era tutto chiaro!"


translate italian day6_after_map_40942432:


    "Sono andato verso il campo sportivo, mi sono avvicinato a Lena, ho sorriso e le ho detto:"


translate italian day6_after_map_1a8f3935_1:


    me "Ehi!"


translate italian day6_after_map_f0d43f46:


    "Si è voltata e mi ha guardato."


translate italian day6_after_map_e8479859:


    un "Ciao…"


translate italian day6_after_map_0974d8de:


    me "Sei stata assente per tutto il giorno."


translate italian day6_after_map_abad00d7:


    un "Sì… Stavo camminando qui attorno."


translate italian day6_after_map_158d2b03:


    "Il suo tono era calmo e tranquillo, senza alcuna traccia di imbarazzo o timidezza.{w} Senza alcuna emozione, a dire il vero."


translate italian day6_after_map_9cac764a:


    me "Eravamo preoccupati per te."


translate italian day6_after_map_a9e86fe4:


    "Anche se l'unico che si era preoccupato per lei ero io."


translate italian day6_after_map_334f3921:


    un "Non avreste dovuto."


translate italian day6_after_map_696f7e51:


    me "Ma non puoi…{w} Non puoi semplicemente sparire in quel modo!"


translate italian day6_after_map_f1268435:


    "Ho cercato di sorridere, per evitare che le mie parole suonassero come un rimprovero."


translate italian day6_after_map_608904a9:


    un "Non credo che a qualcuno importi della cosa."


translate italian day6_after_map_d1150751:


    me "A me sì."


translate italian day6_after_map_b672514d:


    un "Perché?"


translate italian day6_after_map_24fb8611:


    "Ho notato la sorpresa nei suoi occhi."


translate italian day6_after_map_e8345567:


    me "Perché… Perché non va bene!"


translate italian day6_after_map_d982cf8e:


    "Non importa quanto cercassi di essere sincero, le parole mi uscivano tutte sbagliate."


translate italian day6_after_map_4738c0dc:


    un "Ah! Capisco…{w} Ok, non lo farò più."


translate italian day6_after_map_0f363f3f:


    "Non sembrava essere interessata alla nostra conversazione."


translate italian day6_after_map_ec43a852:


    "Siamo rimasti in silenzio."


translate italian day6_after_map_615bc5a9:


    "Non sapevo proprio cos'altro dire, e Lena sembrava essere a suo agio in quel silenzio."


translate italian day6_after_map_6215010e:


    me "Sei brava?"


translate italian day6_after_map_d0fd1810:


    "Ho chiesto alla fine, indicando la racchetta."


translate italian day6_after_map_15f9eed8:


    un "Non molto…"


translate italian day6_after_map_6bc45b3c:


    me "Se vuoi, posso…"


translate italian day6_after_map_8fa9fd07:


    un "No, non voglio."


translate italian day6_after_map_afff794c:


    "È andata verso la panca, appoggiando la racchetta e il volano."


translate italian day6_after_map_505d91ae:


    me "Va bene…"


translate italian day6_after_map_7e92ea41:


    "A dire il vero, non mi sarei aspettato una risposta del genere."


translate italian day6_after_map_3cebee2a:


    un "Vuoi guardare le stelle con me?"


translate italian day6_after_map_3b03af3a:


    me "Certo…"


translate italian day6_after_map_76efef38:


    "Mi sono seduto accanto a lei."


translate italian day6_after_map_a39c7cb2:


    "Lena ha contemplato attentamente il cielo."


translate italian day6_after_map_67f740f6:


    "Una miriade di piccole luci brillavano sopra di noi. Alcune erano più luminose, altre erano appena visibili."


translate italian day6_after_map_e0e85a4c:


    "Non ho mai capito cosa ci sia di tanto speciale nello stare seduti a guardare le stelle."


translate italian day6_after_map_f99d691b:


    "Dopotutto, qui sulla Terra non sono altro che dei piccoli puntini luminosi, ed è improbabile che chi le guarda sappia che tipo di corpi celesti esse siano, così come la loro dimensione, o quanto siano distanti."


translate italian day6_after_map_543454aa:


    "Certo, è così romantico godersi tutto quello spettacolo, ma per me è come guardare un muro di mattoni. Un muro con le sue crepe, le sue irregolarità, le sue forme."


translate italian day6_after_map_e767efc0:


    "Un cielo stellato in miniatura."


translate italian day6_after_map_6ff78afb:


    me "Allora, cosa ci vedi?"


translate italian day6_after_map_1663efd6:


    un "Le stelle…"


translate italian day6_after_map_60e76402:


    "Ha detto misteriosamente, a testa in su."


translate italian day6_after_map_cdb6a705:


    me "Sì, lo so, le vedo anch'io.{w} Ma cosa c'è di così speciale in esse?"


translate italian day6_after_map_47fad4d7:


    un "Non lo so…{w} Sembra che possano parlare con me. Che ci siano delle persone lassù. Altre persone, con le loro vite, probabilmente molto migliori delle nostre, e anche loro forse in questo momento stanno guardando il cielo, e vedono la Terra, vedono me e te."


translate italian day6_after_map_b51220d8:


    me "La voce delle stelle…"


translate italian day6_after_map_73da0b9c:


    th "Ho già sentito quella frase da qualche parte."


translate italian day6_after_map_548f7ac2:


    un "Si potrebbe chiamarla così."


translate italian day6_after_map_092f8254:


    me "Teoria interessante."


translate italian day6_after_map_1b701aca:


    un "No no, la teoria è piuttosto comune."


translate italian day6_after_map_8603b4ab:


    "Lena mi ha guardato."


translate italian day6_after_map_33f7f134:


    "Sotto la fioca luce lunare, ho notato una piccola lacrima scorrerle giù per la guancia.{w} O almeno così mi è sembrato…"


translate italian day6_after_map_92d22845:


    me "Suppongo di essere più una persona pratica, diciamo."


translate italian day6_after_map_15c1d9fb:


    "Non ha aggiunto altro, limitandosi a fissare nuovamente il cielo."


translate italian day6_after_map_a6f3957e:


    me "Ma se ci pensi, ovviamente, lo spazio è infinito, ci sono milioni di pianeti, migliaia di galassie…{w} È affascinante!"


translate italian day6_after_map_94f87d77:


    "La mia voce era stranamente allegra."


translate italian day6_after_map_51cb2968:


    un "Non c'è bisogno di parlare di tutte queste cose."


translate italian day6_after_map_cb23542f:


    me "Di cosa?"


translate italian day6_after_map_0ed52f26:


    un "Di questo…{w} Di tutto…"


translate italian day6_after_map_23571ed6:


    me "No, le stelle sono…{w} sono veramente belle…"


translate italian day6_after_map_61cd4196:


    un "Perché sei venuto?"


translate italian day6_after_map_1ed10390:


    "Ora stava piangendo di sicuro."


translate italian day6_after_map_4cd41dd4:


    me "Io… io…{w} ti stavo cercando."


translate italian day6_after_map_41cfffd5:


    un "Perché?"


translate italian day6_after_map_9432886b:


    me "Non so perché! L'ho fatto e basta!"


translate italian day6_after_map_4cd400a2:


    un "Mi hai trovata, sei felice adesso?"


translate italian day6_after_map_f883183e:


    me "Beh…"


translate italian day6_after_map_5eca3ce7:


    "Ho mormorato."


translate italian day6_after_map_90b68121:


    un "Perché non sei andato da Alisa?"


translate italian day6_after_map_9f078a0b:


    me "E questo che c'entra?"


translate italian day6_after_map_53406a5d:


    un "Stai cercando di dire che non si tratta di lei?"


translate italian day6_after_map_214e0fd5:


    me "Sì, è esattamente quello che intendo dire! Perché stai di nuovo parlando di queste cose? Credevo ci fossimo già chiariti a riguardo."


translate italian day6_after_map_d2905358:


    "Ovviamente non avevamo chiarito un bel niente.{w} Per la precisione, non avevamo nemmeno iniziato."


translate italian day6_after_map_3fd16509:


    "Tuttavia, non capivo il motivo di tutto questo."


translate italian day6_after_map_a5ef42ee:


    un "Credi davvero che io ne voglia parlare?"


translate italian day6_after_map_3ae4ee1d:


    me "Se non vuoi allora perché?"


translate italian day6_after_map_f20d2d44:


    un "Perché…{w} Perché…{w} È a causa tua!"


translate italian day6_after_map_fd549485:


    me "Cosa è a causa mia?"


translate italian day6_after_map_e358f2de:


    un "Tu e Alisa…"


translate italian day6_after_map_4fa9efef:


    me "Io e Alisa cosa?{w} Non abbiamo una relazione… Non c'è proprio niente tra di noi!"


translate italian day6_after_map_bdc08ff0:


    un "Non mentirmi…"


translate italian day6_after_map_50c351a6:


    "Lena si è voltata, singhiozzando silenziosamente."


translate italian day6_after_map_73766a33:


    th "Perché è così importante per lei?"


translate italian day6_after_map_dcecdc26:


    me "Che altro posso dire se non vuoi credermi?"


translate italian day6_after_map_6706f668:


    un "Dimmi la verità!"


translate italian day6_after_map_61fa1f64:


    me "Te l'ho già detta!"


translate italian day6_after_map_aa3b5bb0:


    un "Allora va' da lei!"


translate italian day6_after_map_0aa84a7b:


    me "Perché dovrei andare da lei? Non voglio andare da nessuna parte!"


translate italian day6_after_map_5041432a:


    un "Perché te ne stai seduto qui a torturarmi?"


translate italian day6_after_map_80e9ae79:


    me "Oh Signore! E come ti starei torturando?"


translate italian day6_after_map_cf8bd28b:


    "Lena non ha risposto."


translate italian day6_after_map_f59f13df:


    me "Ti ho cercata per l'intera giornata, perché ero preoccupato per te! Sono venuto qui perché tu eri qui! Sono seduto qui perché voglio stare qui! Cos'è che non capisci?"


translate italian day6_after_map_70497417:


    "Ha smesso di piangere per un istante."


translate italian day6_after_map_dd41c6da:


    un "È vero?"


translate italian day6_after_map_771aa502:


    me "Certo!"


translate italian day6_after_map_2630532a:


    un "Significo qualcosa per te?"


translate italian day6_after_map_97962bdb:


    "Pur considerando tutte le cose che le avevo appena detto, quella domanda mi è venuta addosso in modo imprevisto."


translate italian day6_after_map_ea6d6d42:


    me "Beh…{w} Sì…"


translate italian day6_after_map_dba41e75:


    un "Se solo tu potessi sentirti in questo momento!"


translate italian day6_after_map_2844b0b9:


    "Ha pianto, si è alzata e si è diretta a passo svelto verso l'uscita del campo sportivo."


translate italian day6_after_map_9e8ba2ed:


    me "Aspetta!"


translate italian day6_after_map_ea828f9c:


    "L'ho raggiunta e l'ho afferrata per il braccio, ma lei si è liberata dalla mia presa."


translate italian day6_after_map_6afc93f8:


    un "Non mi toccare!"


translate italian day6_after_map_5ad79041:


    "Adesso Lena era diventata una persona totalmente diversa."


translate italian day6_after_map_c1e1344b:


    "E non potevo dire che fosse diventata aggressiva, assertiva o super-volitiva.{w} Era semplicemente sicura di quello che stava facendo."


translate italian day6_after_map_42af3d5b:


    me "Aspetta!{w} Che cosa ho detto? È la verità!"


translate italian day6_after_map_96759709:


    un "Conserva tutte queste sciocchezze per lei!"


translate italian day6_after_map_78f10d19:


    "Ho provato a fermarla di nuovo, ma mi ha guardato così intensamente che non ho avuto il coraggio di oppormi."


translate italian day6_after_map_26abaae4:


    me "Prova a immaginare come mi sento!{w} Ti inventi un sacco di cose stupide, e adesso pensi che io sia colpevole di tutti i peccati del mondo!"


translate italian day6_after_map_3abf163e:


    un "Perché ti dovrebbe importare?"


translate italian day6_after_map_18650fcd:


    me "Mi importa!"


translate italian day6_after_map_c2fbebf3:


    "Si è fermata per un attimo e mi ha guardato."


translate italian day6_after_map_3b50af60:


    un "Sono tutte bugie!"


translate italian day6_after_map_861f4d27:


    "Ho battuto la fronte su un palo di metallo, per la rabbia."


translate italian day6_after_map_999750cd:


    un "Puoi anche picchiarti da solo, non mi importa!"


translate italian day6_after_map_8da3ba3c:


    "Avevo visto Lena in svariati modi, ma non l'avevo mai vista spietata e indifferente."


translate italian day6_after_map_1db6eb05:


    me "Aspetta, parliamone con calma!"


translate italian day6_after_map_019acfa8:


    un "Non c'è niente di cui parlare!"


translate italian day6_after_map_b44c2c2d:


    "Ha attraversato il campo da calcio e io mi sono trascinato dietro a lei, cercando invano di convincerla ad ascoltarmi."


translate italian day6_after_map_e6426ab5:


    th "Non so perché io stia facendo tutto questo. Per dimostrarle che ho ragione?"


translate italian day6_after_map_db9e6092:


    th "Per non essere frainteso?{w} Così potrò apparire migliore ai suoi occhi?"


translate italian day6_after_map_666d032c:


    th "O c'è qualche altra ragione?"


translate italian day6_after_map_2fd24a5d:


    "In ogni caso, in quel momento mi sembrava che fosse una necessità."


translate italian day6_after_map_b3e9120f:


    "Siamo giunti alla piazza."


translate italian day6_after_map_197a0325:


    "Lena era piuttosto veloce, riuscivo a malapena a tenere il suo passo."


translate italian day6_after_map_3cdd393e:


    "Dovevo dire qualcosa. La mia testa era piena di varie idee, ma nessuna di esse era adatta alla situazione."


translate italian day6_after_map_9e8ba2ed_1:


    me "Aspetta!"


translate italian day6_after_map_ba080751:


    "Nessuna risposta."


translate italian day6_after_map_70cd7a9e:


    me "Ascolta…"


translate italian day6_after_map_ba080751_1:


    "Nessuna risposta."


translate italian day6_after_map_d175e20e:


    me "Vuoi smetterla?!"


translate italian day6_after_map_ba080751_2:


    "Nessuna risposta."


translate italian day6_after_map_a3922398:


    me "Sveglieremo l'intero Campo!"


translate italian day6_after_map_b23d87cc:


    un "E allora? Che differenza fa?"


translate italian day6_after_map_e688e51e:


    "Stranamente in quel momento ero più preoccupato del nostro comportamento, piuttosto che di Lena."


translate italian day6_after_map_54f55e48:


    "Si è fermata."


translate italian day6_after_map_673b1993:


    un "Grazie per avermi accompagnata, adesso andrò da sola."


translate italian day6_after_map_53d3d956:


    "Mi ha detto con amarezza."


translate italian day6_after_map_9a44e183:


    me "Non mi hai ancora ascoltato!"


translate italian day6_after_map_3a8fc4c1:


    un "A me invece sembra che io ti abbia già ascoltato, anche troppo!"


translate italian day6_after_map_14f0e6dc:


    me "Allora cosa vuoi da me?"


translate italian day6_after_map_fd132569:


    un "Io? Da te? Proprio niente!"


translate italian day6_after_map_a1a64448:


    "Sembrava molto più sicura di me, non c'era alcuna traccia di dubbio nei suoi occhi."


translate italian day6_after_map_2f879a85:


    me "Cosa posso dire…? Cosa posso fare…?{w} Dannazione!"


translate italian day6_after_map_ed3bf88e:


    "Stavo per scoppiare in lacrime."


translate italian day6_after_map_4b3132f4:


    un "Non essere così emotivo. Non è successo nulla."


translate italian day6_after_map_f6b5a1cc:


    me "Non è successo nulla?! Qualcosa è successo di sicuro!"


translate italian day6_after_map_488ea261:


    un "Non ti perdi niente se mi ignori."


translate italian day6_after_map_6750969b:


    me "Lascia che sia io a deciderlo!"


translate italian day6_after_map_de950caa:


    un "No, seriamente, perché?{w} Ci sono così tante altre cose piacevoli che potresti fare.{w} E io, francamente, non ne ho bisogno comunque."


translate italian day6_after_map_9f0f0f1e:


    me "Se non ne hai bisogno, allora perché continui sempre a menzionare Alisa?!"


translate italian day6_after_map_d67764cb:


    "La sua espressione si è tramutata."


translate italian day6_after_map_11f8377a:


    un "Questi non sono affari tuoi, lo vuoi capire?!"


translate italian day6_after_map_5fab1698:


    me "No, adesso sono affari miei!"


translate italian day6_after_map_52a66644:


    "Mi ha guardato come se volesse uccidermi."


translate italian day6_after_map_6a9541dc:


    un "Perché continui a importunarmi? Hai lei? Allora va' da lei! Non ti rifiuterà, te lo assicuro."


translate italian day6_after_map_17a396ba:


    "Lena ha iniziato ad urlare, con i capelli arruffati, il volto arrossato e gli occhi iniettati di sangue."


translate italian day6_after_map_d2b2d0c2:


    me "Calmati! Non voglio andare da lei. Perché dovresti pensarlo? Guarda – io sono qui, e non sto andando da nessuna parte."


translate italian day6_after_map_87b3deb0:


    "Sembrava essersi calmata un po'."


translate italian day6_after_map_3a8b8317:


    dv "Che sta succedendo qui?"


translate italian day6_after_map_166adabe:


    "Mi sono voltato e ho visto Alisa che stava masticando pigramente un panino."


translate italian day6_after_map_9bba946f:


    th "Nel posto sbagliato al momento sbagliato. Ecco l'esempio perfetto."


translate italian day6_after_map_10e07777:


    "Sono rimasto immobile, non sapendo cosa dire."


translate italian day6_after_map_41dceae9:


    "Ma Lena è stata più sveglia."


translate italian day6_after_map_f335a36d:


    un "È giunto il momento. Prendilo, è tutto tuo!"


translate italian day6_after_map_e2e97404:


    "Alisa sembrava sorpresa.{w} Non mi stupisce, dato che non aveva sentito l'intera conversazione."


translate italian day6_after_map_7cac8762:


    dv "Che cosa?"


translate italian day6_after_map_cfaec870:


    un "Ho detto, prendilo!"


translate italian day6_after_map_32fc49d5:


    "Nel giro di un istante Lena è tornata la solita – calma e imperturbabile."


translate italian day6_after_map_20cf849d:


    dv "Cosa dovrei prendere?"


translate italian day6_after_map_0216fe85:


    un "Lui!"


translate italian day6_after_map_3f535c7c:


    "Ha puntato il dito verso di me, con fare sprezzante."


translate italian day6_after_map_21610fc5:


    un "Si lamenta sempre di quanto lui ti ami, di come non riesca a vivere senza di te, e cose del genere."


translate italian day6_after_map_e0ebb201:


    dv "Che cosa?"


translate italian day6_after_map_61fafefa:


    "Gli occhi di Alisa si sono spalancati."


translate italian day6_after_map_5135656c:


    me "No, è tutto falso…{w} Lena sta solo scherzando."


translate italian day6_after_map_d9758ca6:


    "Ho ridacchiato nervosamente."


translate italian day6_after_map_9849559e:


    un "Perché? È esattamente così che stanno le cose."


translate italian day6_after_map_6a9072d6:


    me "Senti, c'è un limite a tutto."


translate italian day6_after_map_f25c750f:


    "Le ho detto a bassa voce."


translate italian day6_after_map_bd03c02c:


    me "Devi per forza fare la vittima?"


translate italian day6_after_map_a94aa304:


    un "Perché? Ti ho già detto di andare da lei."


translate italian day6_after_map_0ece655b:


    dv "Non so di cosa state parlando, ma non voglio essere coinvolta!"


translate italian day6_after_map_c981c313:


    un "Non lo sai?"


translate italian day6_after_map_51cf0a90:


    "Le ha sussurrato dolcemente, ma nella sua voce c'era una nota di rabbia."


translate italian day6_after_map_73a43983:


    un "Di nuovo la stessa cosa?!"


translate italian day6_after_map_16082370:


    "Alisa l'ha guardata spaventata."


translate italian day6_after_map_e75b572c:


    dv "Senti, capisco che voi, beh, mm…{w} Ma non lo so… Davvero… non una singola parola, neppure un singolo pensiero su di lui! Lo giuro!"


translate italian day6_after_map_efca6f44:


    un "Non lo sai?!"


translate italian day6_after_map_75b8e738:


    "Con uno scatto fulmineo, Lena si è scagliata contro Alisa e, ancor prima che potessi rendermene conto, l'ha colpita in pieno volto con un potente gancio destro."


translate italian day6_after_map_c097944c:


    "Beh, avrei capito un semplice schiaffo, ma un colpo così forte avrebbe potuto romperle la mascella!"


translate italian day6_after_map_1bd51a91:


    "Alisa è crollata a terra, e sembrava aver perso conoscenza."


translate italian day6_after_map_91c4ce70:


    th "Cosa dovrei fare adesso?"


translate italian day6_after_map_660ef966:


    "Ho deciso che qualunque cosa accadesse, non mi riguardava."


translate italian day6_after_map_7596bb80:


    th "Ma d'altra parte, come potrei restare indifferente?"


translate italian day6_after_map_41d6bd2d:


    th "Sono o non sono un vero uomo?!"


translate italian day6_after_map_1b093c3a:


    me "Ma cosa fai?!"


translate italian day6_after_map_dc606069:


    "Mi sono precipitato verso la ragazza svenuta e ho cercato di capire se fosse ancora viva."


translate italian day6_after_map_5e987c49:


    me "Sei completamente fuori di testa? Se vuoi che me ne vada, che la smetta di parlare, o qualsiasi altra cosa – bene! Ma questa è pura follia! Forse hai bisogno di una camicia di forza, in modo che nessun altro si faccia male per causa tua?!"


translate italian day6_after_map_0710111a:


    "Lena è rimasta in piedi, stringendo i pugni."


translate italian day6_after_map_246bc5b9:


    "Avrebbe potuto anche rompersi la mano, con un colpo del genere."


translate italian day6_after_map_8bf70748:


    un "Tu… Tu…{w} Non capisci proprio niente!"


translate italian day6_after_map_8373e0fe:


    "Le lacrime sgorgavano dai suoi occhi, scivolando sul suo viso, ed è fuggita dalla piazza."


translate italian day6_after_map_3ccb1990:


    "Mi sembrava ancora di sentire i singhiozzi di Lena mentre stavo cercando di rianimare Alisa."


translate italian day6_after_map_b0111b39:


    "Finalmente si è ripresa."


translate italian day6_after_map_605ea41f:


    me "Sei viva? Stai bene?"


translate italian day6_after_map_f537afe9:


    "Alisa ha mosso la mascella a destra e a sinistra."


translate italian day6_after_map_898a6e8c:


    dv "Vivrò…"


translate italian day6_after_map_d84b48c5:


    "L'ho aiutata ad alzarsi in piedi."


translate italian day6_after_map_64e51ae5:


    dv "Te l'avevo detto che lei è così…"


translate italian day6_after_map_6146f956:


    me "Beh, non importa ora! Devi andare all'infermeria!"


translate italian day6_after_map_93df0dc6:


    dv "È troppo tardi per l'infermeria…{w} Mi faccio una bella dormita e ci andrò domani mattina."


translate italian day6_after_map_3e4756aa:


    me "Va bene, allora ti accompagno."


translate italian day6_after_map_2807c5a8:


    dv "Non posso rifiutare."


translate italian day6_after_map_853a9bed:


    "Mi ha detto, cercando di fare un sorriso."


translate italian day6_after_map_e82487ee:


    "Siamo rimasti in silenzio per l'intero tragitto."


translate italian day6_after_map_705d46ac:


    "Probabilmente non era in grado di parlare comunque, e io non riuscivo a trovare le parole giuste. È stato tutto un grosso shock per me."


translate italian day6_after_map_08c54854:


    "Mi sentivo come un semplice spettatore di un dramma del quale ero appena diventato il protagonista…"


translate italian day6_after_map_f6503a89:


    "Dopo che la porta si è chiusa dietro ad Alisa, sono rimasto lì a guardare la sua casetta a lungo."


translate italian day6_after_map_7775e100:


    th "Cosa dovrei fare dopo tutto questo?"


translate italian day6_after_map_d541a4e0:


    "La stanchezza ha preso il sopravvento. Avevo bisogno di dormire."


translate italian day6_after_map_f4e9f6bc:


    th "Credo che Lena ora si trovi in uno stato che non le permette di valutare la realtà in modo adeguato."


translate italian day6_after_map_0b692d89:


    "Completamente a pezzi, mi sono lasciato cadere sul letto, e mi sono addormentato all'istante."


translate italian day6_us_932d85c4:


    "Non posso dire che la mia giornata fosse iniziata bene.{w} Al contrario, mi sono svegliato completamente a pezzi."


translate italian day6_us_44869d66:


    "La stanza era immersa in una intensa luce solare. Gli uccellini cinguettavano fuori dalla finestra, a cuor leggero."


translate italian day6_us_3d1fd928:


    "Mi sono messo seduto con riluttanza e mi sono stirato."


translate italian day6_us_26166937:


    "Lentamente ho iniziato a ricordare gli eventi del giorno precedente."


translate italian day6_us_ba99f4d8:


    "La gita, quello stupido gioco di fantasmi, Ulyana che si era addormentata, e…"


translate italian day6_us_8eb6fcb7:


    "... Un bacio!"


translate italian day6_us_9ab1bc4f:


    "Sono arrossito immediatamente.{w} È stato così sorprendente che mi ha scaraventato fuori dalla realtà per un momento."


translate italian day6_us_89d1c6c1:


    th "Il problema principale è – come dovrei considerarla adesso?{w} A prima vista non è successo niente di speciale, ma…"


translate italian day6_us_b9f2d616:


    th "Inoltre, lei è l'ultima ragazza in tutto il Campo dalla quale ci si potrebbe aspettare un gesto del genere."


translate italian day6_us_7b429d44:


    "Io la consideravo solo una sorellina, niente di più."


translate italian day6_us_d11fbcdb:


    "Ma per lei, a quanto pare, sono più di un fratello maggiore…"


translate italian day6_us_b3cdc0ae:


    "Anche se forse mi stavo ponendo troppe domande, e in realtà non c'era alcun significato nascosto, alcune motivazioni profonde, né allusioni a conclusioni di vasta portata, in quel suo bacio.{w} Forse si è trattato di un semplice gesto di gratitudine?"


translate italian day6_us_22c8c3a5:


    th "Ulyana è decisamente in grado di compiere azioni straordinarie.{w} E potrebbe facilmente trasformare cose semplici in cose complicate."


translate italian day6_us_bd31a746:


    "Alla fine ho deciso di rimandare la questione al momento in cui l'avrei incontrata faccia a faccia."


translate italian day6_us_b202d75c:


    "L'orologio mostrava le undici.{w} E ciò significava che Olga Dmitrievna non mi aveva svegliato per la colazione."


translate italian day6_us_4dc8c2d9:


    "Tuttavia, non era un problema. Non mancava molto al pranzo."


translate italian day6_us_aca3d21b:


    "Ho raccolto la mia borsa di accessori da bagno e sono andato a lavarmi."


translate italian day6_us_3701bba9:


    "Non ho incontrato nessuno durante il tragitto."


translate italian day6_us_8ddbdf53:


    th "Probabilmente sono tutti impegnati con le loro faccende."


translate italian day6_us_a5a2f280:


    "Anche nei pressi dei lavabi era tutto deserto."


translate italian day6_us_9c48b593:


    "Dopo soli cinque minuti, mi sono trovato nuovamente davanti alla porta della stanza della leader del Campo, pensando a cosa fare."


translate italian day6_us_a5889f74:


    "Senza riuscire a pensare a nulla di utile, mi sono seduto sulla sedia a sdraio, e probabilmente avrei oziato fino all'ora di pranzo, se non fosse stato per una voce familiare che ho sentito."


translate italian day6_us_9104ac12:


    sl "Ciao!"


translate italian day6_us_d058f15f:


    "Ho alzato lo sguardo e ho visto Slavya che mi sorrideva dolcemente."


translate italian day6_us_a87c71d7:


    me "Ciao."


translate italian day6_us_452193c9:


    sl "Hai di nuovo dormito troppo a lungo?"


translate italian day6_us_6581dd6b:


    me "Pare proprio di sì…"


translate italian day6_us_269ed985:


    sl "Non va bene…"


translate italian day6_us_472ae0f8:


    me "Forse… E tu dove stai andando?"


translate italian day6_us_dd46fbd4:


    "In realtà non mi interessavano molto i piani di Slavya per quest'oggi, ma ho voluto mantenere viva la conversazione."


translate italian day6_us_619b2083:


    sl "Beh, ho alcune cose da fare."


translate italian day6_us_f2e007f0:


    me "Capisco…"


translate italian day6_us_54d766e4:


    "Siamo rimasti in silenzio.{w} Stava per andarsene, quando improvvisamente le ho chiesto:"


translate italian day6_us_bdfd5ce1:


    me "Ti spiacerebbe stare qui con me per un po'?"


translate italian day6_us_ba1218a0:


    sl "Certo, perché no?"


translate italian day6_us_97a08ca0:


    "Slavya si è seduta accanto a me."


translate italian day6_us_d1c22b75:


    sl "C'è qualcosa che ti turba, non è vero?"


translate italian day6_us_78b966f1:


    me "No, perché lo pensi?"


translate italian day6_us_9ac0f8e4:


    sl "Posso leggertelo in faccia."


translate italian day6_us_ca162b6d:


    "Ha riso piano."


translate italian day6_us_a6ff12a6:


    me "Non proprio…"


translate italian day6_us_f22c4266:


    "Anche se probabilmente Slavya aveva ragione – qualcosa mi stava {i}veramente{/i} turbando.{w} Ovviamente, ero preoccupato per gli eventi della notte scorsa."


translate italian day6_us_60656899:


    me "Senti, hai forse qualche fratello o sorella più piccoli?"


translate italian day6_us_bb4d3f88:


    sl "Sì."


translate italian day6_us_b8863b83:


    me "E che tipo di rapporto hai con loro?"


translate italian day6_us_85a61fa8:


    sl "Mi trovo abbastanza bene."


translate italian day6_us_6d77584d:


    "Ha fatto un ampio sorriso."


translate italian day6_us_9aed200b:


    sl "E tu?"


translate italian day6_us_5cb822ee:


    me "No, non ne ho…"


translate italian day6_us_96096e29:


    "Ho detto, dopo una breve pausa."


translate italian day6_us_cfdfe577:


    sl "Allora perché me lo chiedi?"


translate italian day6_us_0f0d016f:


    me "Non so, è solo che…"


translate italian day6_us_f36a24a2:


    me "Riesci sempre a capirli?"


translate italian day6_us_e7a9578f:


    sl "Ci provo."


translate italian day6_us_0f15cfb0:


    me "A volte non riesco proprio a capire cosa vogliano i ragazzini…"


translate italian day6_us_7519a16d:


    "Improvvisamente mi sono fermato per un istante."


translate italian day6_us_b771fb63:


    th "Slavya non sa che in realtà io sono molto più vecchio di come sembro.{w} Per lei, anch'io sono ancora un ragazzino."


translate italian day6_us_5baee7ee:


    me "Quelli più giovani…"


translate italian day6_us_70f37a53:


    sl "Perché?"


translate italian day6_us_f4a5584e:


    me "Beh, non sempre riesco a capire le motivazioni delle loro azioni. Spesso si comportano illogicamente."


translate italian day6_us_257da516:


    sl "Non eri anche tu come loro, poco tempo fa?"


translate italian day6_us_efd76d15:


    "Mi ha guardato sorpresa."


translate italian day6_us_80ee985d:


    me "Certamente, lo ero…"


translate italian day6_us_de2e65e6:


    "In quel momento mi è sembrato che fosse stato molto tempo fa.{w} E pensando a Ulyana, quel «molto» è diventato un abisso di anni."


translate italian day6_us_b77a46d6:


    sl "Bene! Pensa a com'eri tu a quell'età."


translate italian day6_us_774a19b3:


    me "Ero diverso…"


translate italian day6_us_3e1005b1:


    "O almeno mi sembrava."


translate italian day6_us_457d02d4:


    sl "Buon per te allora!"


translate italian day6_us_c63c3cdc:


    "Slavya ha riso nuovamente."


translate italian day6_us_71b0154f:


    me "E tu?{w} Anche tu facevi cose stupide?"


translate italian day6_us_d85b7a18:


    sl "Dipende dall'età di cui stiamo parlando."


translate italian day6_us_79e03e6b:


    me "Beh, diciamo…{w} All'età di quattordici anni, ad esempio…"


translate italian day6_us_a25fc2aa:


    sl "Beh, diverse cose stavano accadendo…"


translate italian day6_us_5e20fc4d:


    "Ha detto pensierosa."


translate italian day6_us_0536f6a9:


    me "Ok, è tutto bello, ma non rende le cose più chiare…"


translate italian day6_us_d97a7610:


    sl "Forse è così che dovrebbe essere?"


translate italian day6_us_963937db:


    me "Può darsi…"


translate italian day6_us_f0a4acf4:


    "Slavya si è alzata, mi ha salutato e se n'è andata. Ho chiuso gli occhi e mi sono addormentato."


translate italian day6_us_a20cefa7:


    "..."


translate italian day6_us_c8a36022:


    "Il suono della sirena del pranzo mi ha svegliato."


translate italian day6_us_33663426:


    "Anche se il suono era tranquillo e veniva da lontano, il mio corpo era come un automa meccanico, e al tempo stabilito mi sono presentato davanti alla porta della mensa…"


translate italian day6_us_c789b795:


    "Ed è lì che ho incontrato Ulyana."


translate italian day6_us_da3a622a:


    us "Ehilà!"


translate italian day6_us_d2510e4c:


    me "Ehm… Sì…"


translate italian day6_us_886b57c4:


    us "Io ho già mangiato!"


translate italian day6_us_9bc7d8f8:


    "Ha riso vivacemente ed è corsa via."


translate italian day6_us_33ab27ac:


    "Non ho provato a fermarla.{w} Dopotutto, non avevo niente da dirle."


translate italian day6_us_ec659793:


    "Ho preso il mio pasto e mi stavo dirigendo verso il mio posto preferito in fondo alla mensa, quando improvvisamente qualcuno mi ha afferrato per il braccio."


translate italian day6_us_04f58a0e:


    dv "Siediti qui!"


translate italian day6_us_f294c7bb:


    "La voce di Alisa suonava minacciosa, ma allo stesso tempo supplichevole."


translate italian day6_us_29f71b5c:


    me "Che è successo?"


translate italian day6_us_73ab0e4f:


    dv "Dobbiamo parlare di una cosa."


translate italian day6_us_cb7dcf26:


    "Mi ha detto in tono più morbido."


translate italian day6_us_505d91ae:


    me "Va bene…"


translate italian day6_us_12af78a1:


    "Mi sono seduto accanto a lei."


translate italian day6_us_c233b6a2:


    me "Allora, di cosa vuoi parlare?"


translate italian day6_us_c1a63ca7:


    dv "Tu eri lì ieri…{w} Quando lei ha fatto quella stupida scenetta con il lenzuolo?"


translate italian day6_us_ae83cf7a:


    th "È inutile mentire ad Alisa."


translate italian day6_us_627457bb:


    me "Sì, ero lì…"


translate italian day6_us_3cf2f629:


    dv "È proprio… È proprio…"


translate italian day6_us_56d4e39c:


    "È scoppiata a ridere."


translate italian day6_us_bf0fda5f:


    dv "Ma perché diavolo l'avete fatto?"


translate italian day6_us_d35441c0:


    "Alisa è diventata subito seria, fissandomi con rimprovero."


translate italian day6_us_4954bdac:


    me "Perché non lo chiedi a lei?"


translate italian day6_us_6153932c:


    dv "No, lo sto chiedendo a te adesso, ed esigo una risposta!"


translate italian day6_us_fccc1215:


    me "Non lo so!"


translate italian day6_us_ccbb1994:


    "Ho gracchiato."


translate italian day6_us_bd86e9cc:


    me "Forse è meglio se mi dici perché la leader del Campo non ha preso provvedimenti?"


translate italian day6_us_c46902a7:


    dv "E cosa avrebbe dovuto fare?"


translate italian day6_us_2552c9ac:


    me "Penso che tu lo sappia…"


translate italian day6_us_84ea8c71:


    "L'ho guardata con scetticismo."


translate italian day6_us_a306c7bc:


    dv "Beh, semplicemente non aveva capito chi fosse."


translate italian day6_us_65256f6f:


    "Devo ammetterlo, le capacità deduttive di Olga Dmitrievna lasciavano molto a desiderare."


translate italian day6_us_251f6e0f:


    dv "E poi…{w} Sei stato tu a riportarla indietro ieri notte?"


translate italian day6_us_311e429f:


    me "No, sono stati gli elfi di Shakespeare."


translate italian day6_us_12b4f273:


    "Ho detto in modo beffardo."


translate italian day6_us_1e5bc39c:


    me "Qual è il problema?"


translate italian day6_us_b3b67d03:


    dv "Nessun problema…"


translate italian day6_us_2e06d203:


    "Alisa ha fissato il suo piatto e si è concentrata sul mangiare."


translate italian day6_us_6cc1bbe6:


    dv "E poi…{w} Se n'è andata da qualche parte durante la notte, tornando piuttosto felice…"


translate italian day6_us_f95c9fc9:


    me "E?"


translate italian day6_us_79b4bb58:


    dv "Non ne sai niente?"


translate italian day6_us_37ad7929:


    me "Anche se lo sapessi, perché dovrebbe interessarti?"


translate italian day6_us_8fb93608:


    "Mi ha fissato intensamente."


translate italian day6_us_304d61e3:


    dv "Beh, mi interessa."


translate italian day6_us_3856e844:


    th "Non avrei mai pensato che lei potesse preoccuparsi per qualcuno, a parte sé stessa."


translate italian day6_us_df1ea57f:


    me "Non è successo niente del genere, se proprio lo vuoi sapere…"


translate italian day6_us_744c373a:


    dv "No, non si tratta di questo…"


translate italian day6_us_6d91ce66:


    "Ha risposto concisamente, senza guardarmi."


translate italian day6_us_9e028cbf:


    "Forse non è stata una buona idea parlare di questo argomento."


translate italian day6_us_4db446fd:


    dv "A proposito, cosa intendevi dire con «niente del genere»?"


translate italian day6_us_3ffb4733:


    me "Che vuoi dire?"


translate italian day6_us_ae504ccb:


    dv "Quello che mi hai appena detto!"


translate italian day6_us_357ddbfd:


    me "Che cosa ti ho appena detto?"


translate italian day6_us_bb9ef712:


    "Ho cercato di prenderla in giro per confonderla."


translate italian day6_us_93884669:


    dv "Ehi… Ascoltami bene! Ti tengo d'occhio."


translate italian day6_us_465a1be8:


    me "Oh, ho così tanta paura!"


translate italian day6_us_46433dd4:


    "Le ho detto aspramente, ho preso il mio vassoio e ho abbandonato il tavolo."


translate italian day6_us_868ec380:


    "Sicuramente non ero intimorito dalle parole di Alisa, anche se suonavano come una minaccia."


translate italian day6_us_f2a61bc9:


    "In realtà mi interessava più la sua preoccupazione per Ulyana…"


translate italian day6_us_3a2303e3:


    "Di tutti gli abitanti del luogo, lei era sicuramente l'ultima persona che potesse preoccuparsi per qualcuno."


translate italian day6_us_523908a6:


    "Sono uscito dalla mensa e mi sono ritrovato in un vero e proprio forno.{w} Il sole splendeva così spietatamente che nessuno avrebbe potuto restare sotto i suoi raggi cocenti troppo a lungo."


translate italian day6_us_5a8af8ba:


    "Era giunto il momento di trovare qualche luogo fresco e ombreggiato."


translate italian day6_us_8816790a:


    "E la spiaggia sembrava l'opzione migliore."


translate italian day6_us_7ffbd155:


    "Stranamente, la spiaggia non era affollata."


translate italian day6_us_d78798fb:


    "Forse la maggior parte dei pionieri aveva paura del caldo torrido, o forse semplicemente avevano deciso di fare un pisolino dopo pranzo."


translate italian day6_us_539fca33:


    "Non avevo con me il costume da bagno, così mi sono limitato a restare seduto sotto a un ombrellone, osservando i bagnanti."


translate italian day6_us_aee79133:


    "Non c'era nessuno che conoscevo, ed era meglio così, ciò mi ha dato l'opportunità di pensare in tranquillità."


translate italian day6_us_6339194e:


    th "Sono già trascorsi cinque giorni e mezzo dal mio arrivo qui, e cosa è cambiato?"


translate italian day6_us_e1327ac0:


    th "Ci sono stati un paio di eventi inspiegabili, ma del tutto innocui, e tutto sommato è un normale Campo di pionieri."


translate italian day6_us_becbdeab:


    "E la cosa mi faceva ancora più paura."


translate italian day6_us_1d93d559:


    th "Che cosa succederà in seguito?"


translate italian day6_us_e2f93f20:


    th "Un altro giorno, un'altra settimana, e cosa accadrà dopo questa sessione?{w} Non ho un posto dove tornare."


translate italian day6_us_efe9c612:


    th "Forse in questo momento non sono ancora nato.{w} O forse anche peggio…"


translate italian day6_us_07aa477a:


    "Dopo aver ripensato a tutto questo, la mia mente è tornata agli eventi della sera precedente."


translate italian day6_us_b188d2fb:


    "Strano, ma mi preoccupavo molto più di prima."


translate italian day6_us_5818c1ec:


    "Da un lato volevo dire qualcosa per rendere più chiara la situazione, ma dall'altro volevo evitare ogni possibile incontro con Ulyana per paura di fare la figura dello stupido, facendo o dicendo qualcosa di sbagliato."


translate italian day6_us_d6d9a6e5:


    "Ho chiuso gli occhi e mi sono assopito per un po'."


translate italian day6_us_a20cefa7_1:


    "..."


translate italian day6_us_3615e0f8:


    "Quando mi sono svegliato, ho visto Lena al mio fianco."


translate italian day6_us_4be9f1c5:


    me "Da quanto tempo sei seduta qui?"


translate italian day6_us_84bd5d09:


    "Ho chiesto sorpreso."


translate italian day6_us_267018a5:


    un "Non molto."


translate italian day6_us_74e632e7:


    "Strano che lei non mi avesse svegliato."


translate italian day6_us_ea63ce77:


    me "È successo qualcosa?"


translate italian day6_us_7d85bdfc:


    "Non so perché ma il mio sesto senso mi diceva che lei avrebbe voluto dirmi qualcosa, ma che non ne avesse il coraggio."


translate italian day6_us_50dd705e:


    un "No. E a te?"


translate italian day6_us_8cd2864d:


    me "Nemmeno."


translate italian day6_us_cc4a1ab6:


    un "Sei sicuro?"


translate italian day6_us_771aa502:


    me "Ma certo!"


translate italian day6_us_2f04a7f9:


    "Ho sorriso."


translate italian day6_us_d9b000a0:


    un "Allora di cosa vuoi parlare?"


translate italian day6_us_0f1517b5:


    me "Perché pensi che io voglia parlare?"


translate italian day6_us_cdbc06cf:


    un "Mi sembra di sì."


translate italian day6_us_dd0d8a90:


    me "No, non proprio."


translate italian day6_us_a5f21b45:


    "È seguito un lungo silenzio."


translate italian day6_us_1458865a:


    me "Hai dei fratelli o sorelle più piccoli?"


translate italian day6_us_50dd705e_1:


    un "No. E tu?"


translate italian day6_us_8cd2864d_1:


    me "Nemmeno io."


translate italian day6_us_9e52f44a:


    un "Allora perché ti interessa?"


translate italian day6_us_d659d744:


    me "Certe volte mi sembra di non riuscire a capire proprio i ragazzini."


translate italian day6_us_959d479a:


    un "Anche a me."


translate italian day6_us_f41e692e:


    "Il volto di Lena non esprimeva alcuna emozione."


translate italian day6_us_898b12b0:


    un "Ma probabilmente è così che devono andare le cose."


translate italian day6_us_2eb02c21:


    me "Cosa vuoi dire?{w} Come li dovrei trattare allora?"


translate italian day6_us_c80e177b:


    un "Trattali nel modo in cui credi sia giusto."


translate italian day6_us_096e3a82:


    me "Beh, è proprio questo il punto – non riesco a capire cosa sia giusto e cosa sia sbagliato."


translate italian day6_us_0263b300:


    un "Quanto più si pensa, tanto più ci si pentirà dei propri errori."


translate italian day6_us_c256f910:


    me "Beh, hai ragione, ma non mi è di grande aiuto…"


translate italian day6_us_f82ea479:


    "Ero un po' stupito da quelle parole profonde da parte di Lena."


translate italian day6_us_9437b7c4:


    "Certamente me l'aveva detto timidamente e con umiltà, tuttavia..."


translate italian day6_us_5e0eb09d:


    me "Vuoi dire che dovrei agire in base alla prima cosa che mi balza nella testa?"


translate italian day6_us_60bde9d8:


    "Ho riso."


translate italian day6_us_61e40971:


    un "Come dice il proverbio, il primo pensiero è quello giusto."


translate italian day6_us_8500866b:


    me "E tu ci credi?"


translate italian day6_us_cb890890:


    un "Non lo so…"


translate italian day6_us_e6ff543f:


    "Siamo rimasti seduti lì per un po', poi lei si è alzata."


translate italian day6_us_ccdf7460:


    un "Bene, devo andare."


translate italian day6_us_ce5c1ffe:


    me "Grazie per la conversazione!"


translate italian day6_us_584c5532:


    un "Non c'è di che!"


translate italian day6_us_d7a43efd:


    "Lena ha sorriso debolmente."


translate italian day6_us_7c4da760:


    "Mi sono sdraiato e ho fissato il sole cocente."


translate italian day6_us_d79d6192:


    th "Il primo pensiero è quello giusto, eh?{w} E se il mio primo pensiero fosse quello di ricambiare il suo bacio?"


translate italian day6_us_9d9f68a5:


    th "A proposito, qual {i}è stato{/i} il mio primo pensiero in quel momento?"


translate italian day6_us_e288f243:


    "Mi sono sforzato di ricordare, ma non ha funzionato.{w} Nemmeno dei frammenti di pensiero sono emersi nel mio cervello."


translate italian day6_us_7e684fb8:


    "Tuttavia, in quella situazione avrei potuto soltanto seguire il consiglio di Lena, dal momento che tutto era accaduto così all'improvviso."


translate italian day6_us_a20cefa7_2:


    "..."


translate italian day6_us_28dd5f2a:


    "Il sole stava tramontando lentamente, così mi sono alzato e mi sono diretto verso la mensa, dato che il mio stomaco mi stava spiegando che persino i protagonisti di una storia così fantastica avevano bisogno di pasti regolari."


translate italian day6_us_793d9878:


    "Qualcuno mi ha chiamato dalla piazza."


translate italian day6_us_b4ae4166:


    "Era Electronik.{w} È corso verso di me, cercando di riprendere fiato, e mi ha detto:"


translate italian day6_us_45391550:


    el "Semyon…"


translate italian day6_us_9aa9ea53:


    me "Sì, probabilmente sono io."


translate italian day6_us_caf0b8c7:


    el "Prendila."


translate italian day6_us_5e22b544:


    "Mi ha porto una specie di chiave."


translate italian day6_us_51c2c17c:


    me "Che cos'è?"


translate italian day6_us_24914d26:


    el "È la chiave del nostro circolo."


translate italian day6_us_9a940e9d:


    me "E a cosa mi dovrebbe servire?"


translate italian day6_us_9fd6666c:


    el "Me l'ha chiesto Olga Dmitrievna…{w} Dato che oggi non potrò tornarci."


translate italian day6_us_64143d09:


    me "Allora dalla a Shurik."


translate italian day6_us_23cd6835:


    el "Ha altre cose da fare."


translate italian day6_us_483eac77:


    me "Allora tienila tu."


translate italian day6_us_74e85ae4:


    el "Non posso."


translate italian day6_us_5ac38cf1:


    me "Perché no?"


translate italian day6_us_7a4a266c:


    el "Senti, prendila e basta."


translate italian day6_us_c8e2439f:


    me "E a cosa mi serve? Che cosa ci devo fare?"


translate italian day6_us_83054bac:


    el "Me la ridarai più tardi."


translate italian day6_us_17717ee8:


    me "Ascolta…"


translate italian day6_us_34709e3e:


    el "Beh, io devo andare!"


translate italian day6_us_a5cf9eb3:


    "Ha spinto la chiave nella mia mano ed è corso via."


translate italian day6_us_378693dd:


    th "Non importa come lo si guardi, lui è decisamente strano…"


translate italian day6_us_ca9eac02:


    "Tuttavia, non ho dato grande peso alla cosa, e mi sono messo la chiave in tasca."


translate italian day6_us_3f520ea8:


    "Sono arrivato alla mensa in anticipo."


translate italian day6_us_b5e4650e:


    "Per fortuna era già aperta, ma il cibo non veniva ancora servito."


translate italian day6_us_cc81d257:


    "Sono andato al mio posto preferito nell'angolo in fondo alla sala, e mi sono messo a giocherellare con uno stuzzicadenti."


translate italian day6_us_2a72bf60:


    "Ben presto ho avuto la sensazione che qualcuno stesse in piedi accanto a me."


translate italian day6_us_94e4a1c1:


    "Ho alzato lo sguardo e ho visto Olga Dmitrievna."


translate italian day6_us_e4be0ed5:


    "Ci siamo fissati per un po'."


translate italian day6_us_fdb677a4:


    me "Ha bisogno di qualcosa?"


translate italian day6_us_4c09a32f:


    "Alla fine ho rotto il silenzio."


translate italian day6_us_acc07115:


    mt "Senti, Semyon…{w} Capisco tutto, ma…"


translate italian day6_us_ad02f53a:


    me "Cosa?"


translate italian day6_us_7c87b14a:


    "Ho chiesto, sorpreso."


translate italian day6_us_70193a9b:


    mt "Credi che io non abbia notato la tua assenza ieri notte?"


translate italian day6_us_a96d8927:


    th "«Assenza» è un po' esagerato...{w} Direi piuttosto che ero uscito per un momento."


translate italian day6_us_e7288eb6:


    mt "Allora?"


translate italian day6_us_1e5bc39c_1:


    me "Qual è il problema?"


translate italian day6_us_421df7f4:


    mt "Anch'io credevo che non fosse nulla di particolare, ma ho voluto controllare per sicurezza."


translate italian day6_us_8782263a:


    "L'espressione della leader del Campo si stava tramutando sempre più in un cipiglio."


translate italian day6_us_6100ad4b:


    me "Quindi, ha controllato?"


translate italian day6_us_27f521dd:


    "Non riuscivo ancora a capire di cosa stesse parlando."


translate italian day6_us_6b3e0c5d:


    mt "Sì, ho controllato!{w} E sai cosa!{w} Per un vero pioniere è… è…! È semplicemente inaccettabile! È vergognoso! È ignobile! Soprattutto con lei!"


translate italian day6_us_7e6d6b00:


    "È diventata rossa per lo sforzo."


translate italian day6_us_443c0493:


    mt "Non voglio nemmeno sentire le tue scuse pietose!"


translate italian day6_us_fd11f26f:


    "Una vaga idea di cosa stesse parlando ha albeggiato su di me."


translate italian day6_us_185a8790:


    me "Allora, che cosa è successo, vorrebbe spiegarmelo?"


translate italian day6_us_6ca85047:


    mt "E ora hai il coraggio di fare l'innocente! Tu! Con Ulyana!"


translate italian day6_us_cae6c168:


    "Ora è diventato tutto chiaro come il sole."


translate italian day6_us_07246779:


    me "Avrei solo due cose da dirle. La prima…"


translate italian day6_us_262d7cae:


    "Stavo cominciando a innervosirmi."


translate italian day6_us_b3aa00a8:


    me "Non è successo niente del genere. Io non c'entro proprio nulla con tutto questo."


translate italian day6_us_ca25a37e:


    mt "Questo lo dici tu…"


translate italian day6_us_4ff3e72d:


    me "Seconda cosa…"


translate italian day6_us_a3058a6d:


    "L'ho interrotta bruscamente."


translate italian day6_us_7de2af8d:


    me "Chi gliel'ha detto?"


translate italian day6_us_da2aca8e:


    "La risposta era ovvia."


translate italian day6_us_231ed06e:


    "Ulyana non l'avrebbe fatto. E se avesse deciso di farmi ancora un altro scherzo, l'avrebbe fatto molto prima. Dopo tutto il tempo trascorso qui ho imparato a capirla piuttosto bene."


translate italian day6_us_83723d62:


    th "E quindi resta un solo possibile candidato…"


translate italian day6_us_d1eaca67:


    mt "Che differenza fa?"


translate italian day6_us_4d01563d:


    me "No, no! La fa eccome!"


translate italian day6_us_e5fc00e7:


    mt "Vorresti spiegarmi che cosa ti è preso, allora?"


translate italian day6_us_703eeb00:


    "Olga Dmitrievna sembrava un po' sorpresa dalla mia veemenza."


translate italian day6_us_3d718732:


    me "Non c'è niente da spiegare!"


translate italian day6_us_9829aaef:


    "Mi sono alzato di scatto e mi sono diretto verso l'uscita."


translate italian day6_us_19709348:


    mt "Semyon, aspetta…"


translate italian day6_us_653b8455:


    "Mi sono lasciato alle spalle le deboli farneticazioni della leader del Campo."


translate italian day6_us_2080cf3d:


    "Se si agisce in modo deciso contro di lei, allora la smette di essere così sicura di sé."


translate italian day6_us_4d5ffb3d:


    "Adesso dovevo mettere le cose in chiaro con Dvachevskaya."


translate italian day6_us_50e7f679:


    "Finché non mi coinvolgeva nei suoi scherzi, o mi coinvolgeva solo indirettamente, potevo sopportarlo."


translate italian day6_us_abec8829:


    "E dire che avevo iniziato addirittura a considerarla una persona non così cattiva!"


translate italian day6_us_412c8322:


    "Alla piazza ho incontrato Slavya."


translate italian day6_us_c8aa9582:


    me "Sai dov'è Alisa?"


translate italian day6_us_b3c273bf:


    sl "Si, lo so, perché?"


translate italian day6_us_18c5db17:


    me "Allora dimmelo!"


translate italian day6_us_5ac2d11c:


    "Il mio tono era decisamente scortese, ma sembrava che lei non avesse dato troppo peso alla cosa."


translate italian day6_us_26d880cd:


    sl "È al circolo musicale."


translate italian day6_us_95bfa391:


    "Slavya aveva ragione, ho incontrato Alisa vicino al circolo musicale.{w} Anche se era in compagnia di Miku."


translate italian day6_us_8a7949da:


    me "Vorresti spiegarmi?"


translate italian day6_us_ba464770:


    "Non avevo tempo da perdere con i convenevoli."


translate italian day6_us_a36d4b17:


    dv "No. Cosa te lo fa pensare?"


translate italian day6_us_01ffd8fc:


    "Mi ha fatto un sorriso simpatico."


translate italian day6_us_c01cc6ab:


    mi "Oh, Semyon, ciao! Che bello averti qui insieme a noi! Forse in tre potremmo suonare qualcosa? Ho scritto una nuova canzone e, sai, è così divertente! Sono sicuro che ti piacerà, te lo prometto!"


translate italian day6_us_90c9fd46:


    mi "Oppure potremmo scegliere qualcosa di più datato…"


translate italian day6_us_6dae2579:


    "Miku ha interrotto la nostra conversazione."


translate italian day6_us_0259c598:


    me "Potresti…"


translate italian day6_us_57d2c9aa:


    mi "Che cosa?"


translate italian day6_us_90f2e3fb:


    "Mi ha guardato con il sorriso di una bambina innocente. In quel momento ho capito che cosa avesse nel cervello – coniglietti, orsacchiotti, micetti, ma nessun segno di intelligenza."


translate italian day6_us_b05f8c8c:


    me "Ho bisogno di parlare da solo con Alisa."


translate italian day6_us_7cbb1d69:


    "Le ho detto in un tono che non ammetteva disaccordo."


translate italian day6_us_94b93a6a:


    mi "Ok, magari più tardi allora…"


translate italian day6_us_980963c1:


    "Sembrava sconvolta..."


translate italian day6_us_6cdf2111:


    th "Ma non m'interessa!"


translate italian day6_us_1cb52a30:


    me "Allora…"


translate italian day6_us_0b01579b:


    dv "Non capisco cosa tu voglia dire."


translate italian day6_us_c11d0305:


    "Mi ha detto Alisa, offesa."


translate italian day6_us_f16ef830:


    dv "E comunque, devo andare!"


translate italian day6_us_17bb7bcb:


    "Si è voltata e ha fatto per andarsene."


translate italian day6_us_75807571:


    "Le ho afferrato bruscamente il braccio."


translate italian day6_us_ba86d4d5:


    dv "Ma che stai facendo?!"


translate italian day6_us_e5fbd716:


    "La ragazza era davvero spaventata."


translate italian day6_us_27478259:


    me "Ehi, non così in fretta!"


translate italian day6_us_97c863d3:


    "Si è fermata e si è voltata verso di me."


translate italian day6_us_c644793a:


    me "Non ho intenzione di ucciderti..."


translate italian day6_us_30c1f806:


    "Le mie parole avrebbero dovuto suonare come uno scherzo, ma Alisa ha sussultato."


translate italian day6_us_ec416668:


    me "Chi ha detto alla leader del Campo che Ulyana è venuta da me ieri notte?"


translate italian day6_us_3e69c35f:


    dv "Non lo so."


translate italian day6_us_e657f196:


    "Ha piagnucolato lamentosamente."


translate italian day6_us_c3effd86:


    me "Ne abbiamo discusso stamattina, ricordi?"


translate italian day6_us_0d3c2df7:


    "Non ha detto nulla, continuava a fissarmi con la paura nei suoi occhi."


translate italian day6_us_45e343a5:


    me "E soprattutto – che senso ha dire migliaia di bugie?"


translate italian day6_us_ad8d4b66:


    dv "Davvero non so di cosa tu stia parlando!"


translate italian day6_us_5dbc6805:


    "I suoi occhi si sono riempiti di lacrime."


translate italian day6_us_7e881d63:


    "Improvvisamente mi sono sentito come se un fulmine mi avesse colpito."


translate italian day6_us_97da91c1:


    th "E se davvero lei non fosse colpevole?{w} Perché ho subito pensato a lei?{w} Chiunque avrebbe potuto vederci, ieri notte."


translate italian day6_us_8cce8ea8:


    th "Non si sa mai. Persino Ulyana avrebbe potuto spargere la voce."


translate italian day6_us_b77a7f8b:


    me "Ne sei sicura?"


translate italian day6_us_09073ec6:


    "Ho chiesto severamente."


translate italian day6_us_3a950d86:


    dv "Sì."


translate italian day6_us_cea34798:


    "Ho pensato per un attimo, e Alisa ne ha approfittato per fuggire."


translate italian day6_us_b2d3e6d4:


    th "L'unica cosa rimasta da fare è chiederlo direttamente a Olga Dmitrievna."


translate italian day6_us_7149be6f:


    "L'ho trovata sulla veranda della mensa."


translate italian day6_us_37e4ac0e:


    "Il sole era quasi scomparso oltre l'orizzonte."


translate italian day6_us_636e78e7:


    mt "Sei tornato, dopotutto…"


translate italian day6_us_8c1c36bb:


    "Ha esordito."


translate italian day6_us_1a590f52:


    me "Olga Dmitrievna, potrebbe dirmi onestamente chi gliel'ha detto?"


translate italian day6_us_61a9bb1f:


    me "Ho appena parlato con Dvachevskaya – non è stata lei. Ulyana non l'avrebbe mai fatto. Allora chi è stato?"


translate italian day6_us_373aadf9:


    "In realtà non sapevo esattamente perché mi interessasse scoprire chi fosse stato."


translate italian day6_us_7e4fe70a:


    th "All'inizio ero arrabbiato con Alisa, ma adesso…"


translate italian day6_us_d1318244:


    mt "Beh…"


translate italian day6_us_6e6e30b3:


    "Ha esitato."


translate italian day6_us_eeb01990:


    mt "Una strana ragazza…{w} Ma comunque non…"


translate italian day6_us_09fba842:


    me "Una strana ragazza?"


translate italian day6_us_7d0924ac:


    mt "Beh, sì... Non sembrava del nostro Campo."


translate italian day6_us_80770549:


    me "E perché le ha detto quella cosa?"


translate italian day6_us_336d97d4:


    mt "Come faccio a saperlo?!"


translate italian day6_us_e6e7decd:


    me "Olga Dmitrievna, ma questo è..."


translate italian day6_us_5c589d2a:


    "Ho fatto un profondo respiro e mi sono voltato."


translate italian day6_us_ed919e5f:


    th "Perché deve mentire così spudoratamente?"


translate italian day6_us_483b4ef7:


    th "Anche se forse sta cercando di proteggere qualcuno?{w} Ad esempio...{w} Accidenti, non ne ho proprio idea!"


translate italian day6_us_4366e696:


    me "D'accordo..."


translate italian day6_us_76102a17:


    "Ho detto a bassa voce, allontanandomi a passo svelto dalla mensa."


translate italian day6_us_25f025cf:


    "Non avevo più alcuna voglia di stare ad ascoltare le stupide bugie di Olga Dmitrievna."


translate italian day6_us_e2e7f765:


    th "La cosiddetta «leader del Campo»..."


translate italian day6_us_b46ab80b:


    "Ho vagato senza meta, completamente perso nei miei pensieri."


translate italian day6_us_390dbdba:


    us "Sei di guardia?"


translate italian day6_us_16e9259d:


    "Una voce allegra è spuntata da dietro."


translate italian day6_us_fb91294d:


    "Era Ulyana."


translate italian day6_us_31ff7fd5:


    me "Niente di speciale, solo…"


translate italian day6_us_c6821b44:


    "Sembrava non esserci alcun modo di scoprire chi fosse l'informatore di Olga Dmitrievna, e l'incidente di ieri è stato messo a tacere in qualche modo, così ho deciso di non pensarci."


translate italian day6_us_09d23ad2:


    us "Che cosa hai in programma per stasera?"


translate italian day6_us_5203c9d1:


    me "Niente suppongo…"


translate italian day6_us_29fcfde5:


    "Dopo tutti i miei frenetici tentativi di scoprire la verità, mi sentivo inspiegabilmente in colpa nei confronti di Alisa e di Ulyana."


translate italian day6_us_8eec6359:


    "Anche se non riuscivo proprio a capire perché dovessi sentirmi in colpa nei confronti di Ulyana."


translate italian day6_us_876ae211:


    us "Allora ho una proposta."


translate italian day6_us_094d6b91:


    me "Quale proposta?"


translate italian day6_us_288bb33e:


    us "Ho portato una videocassetta da casa…"


translate italian day6_us_6d3b56e5:


    "I suoi occhi hanno brillato in modo cospiratorio."


translate italian day6_us_f95c9fc9_1:


    me "E?"


translate italian day6_us_2b9ac9b1:


    th "Non riesco nemmeno a ricordare quando sia stata l'ultima volta che ho visto o sentito qualcosa su una cassetta."


translate italian day6_us_b54f9cfb:


    us "C'è registrato un film bellissimo!"


translate italian day6_us_33f86965:


    me "E dove potremmo trovare un videoregistratore qui?"


translate italian day6_us_9b9a6c28:


    "Ho detto quelle parole con la mente rivolta al passato, così avrei potuto sapere se le mie supposizioni riguardo al periodo in cui mi trovavo fossero corrette."


translate italian day6_us_635b72e6:


    us "Al circolo di cibernetica, ovviamente!"


translate italian day6_us_4ce76f25:


    me "Non c'è niente lì."


translate italian day6_us_449236f5:


    "Ho detto fiduciosamente."


translate italian day6_us_6fdda8ad:


    us "E che dici della stanza sul retro?"


translate italian day6_us_fc831b50:


    "Aveva ragione.{w} Poteva essere lì."


translate italian day6_us_2d3bdcfb:


    me "Beh, forse…{w} Ma comunque è troppo tardi, sarà chiuso adesso."


translate italian day6_us_ba96048e:


    "Quella strana sensazione di colpevolezza nei suoi confronti non mi consentiva di rifiutare immediatamente la sua proposta."


translate italian day6_us_d82bc37c:


    us "Possiamo passare dalla finestra!"


translate italian day6_us_17cfa934:


    "Ha ghignato maliziosamente."


translate italian day6_us_75cef782:


    me "Beh, sai..."


translate italian day6_us_6533f6ed:


    us "Sì, forse hai ragione. Magari avessimo una chiave…"


translate italian day6_us_2b00c58d:


    "Ha riflettuto Ulyana."


translate italian day6_us_71a7833d:


    th "Ma io ce l'ho!{w} Proprio nella mia tasca."


translate italian day6_us_433d2c22:


    "Mi era appena venuto in mente."


translate italian day6_us_4e580acc:


    me "Ora che ci penso, sì! Ho una chiave!"


translate italian day6_us_001cfbf1:


    "La prossima volta forse è meglio pensare prima dire qualcosa."


translate italian day6_us_24a62d16:


    us "Grandioso! Allora vado subito a prendere la cassetta, aspettami qui, torno subito."


translate italian day6_us_8b7b1283:


    "È sparita ancor prima che io potessi aprir bocca."


translate italian day6_us_983ee57d:


    "È strano che lei non fosse sorpresa del fatto che la chiave per il circolo di cibernetica fosse misteriosamente apparsa nella mia tasca nel posto giusto al momento giusto."


translate italian day6_us_b974419e:


    "Ad ogni modo, dovevo decidere quale sarebbe stata la mia prossima mossa."


translate italian day6_us_d21cc0d2:


    "La cosa più giusta e logica sarebbe stata quella di non andare da nessuna parte con lei.{w} Ma il mio senso di colpa ha preso il sopravvento…"


translate italian day6_us_dddd87f0:


    th "Anche se non dovrebbe esserci nulla di male nel guardare un film insieme a lei."


translate italian day6_us_1b5d4998:


    th "Ma d'altra parte, con Ulyana anche le cose più innocue possono trasformarsi in un circo a tre piste.{w} Per non dire altro."


translate italian day6_us_f97a40c8:


    "Stavo già dimenticando quello che era accaduto questa sera, dei miei frenetici tentativi di ottenere la verità da Alisa e da Olga Dmitrievna…"


translate italian day6_us_a20cefa7_3:


    "..."


translate italian day6_us_1dc2a2a7:


    "Dopo solo un paio di minuti è tornata."


translate italian day6_us_bcf9c5aa:


    us "Qualche ripensamento?"


translate italian day6_us_df7954e6:


    "Ho guardato Ulyana e mi sono sentito un po' a disagio."


translate italian day6_us_1c3f5164:


    us "Andiamo?"


translate italian day6_us_a4c1c729:


    me "Senti, sai, le tue idee sono…{w} come dire…{w} un po' pericolose."


translate italian day6_us_e1f1e8c6:


    me "E alla fine tutto finirà male di nuovo."


translate italian day6_us_5d3c20b3:


    us "Hai paura?"


translate italian day6_us_bcadcefa:


    me "No, non ho paura."


translate italian day6_us_7714aa0d:


    me "È solo che sono un uomo adulto, e questi giochetti non mi interessano."


translate italian day6_us_35cd0fa9:


    us "Un uomo adulto?"


translate italian day6_us_ec2c2a7c:


    "Si è messa a ridere."


translate italian day6_us_359cc1c1:


    th "In effetti mi sono dimenticato che ho al massimo diciassette anni."


translate italian day6_us_9a385a27:


    us "Cosa te lo fa pensare?"


translate italian day6_us_6f30efaf:


    "È davvero difficile da accettare, ma non avevo nulla da dirle a riguardo."


translate italian day6_us_0c18ac79:


    "Ripensandoci – durante tutto il tempo che ho trascorso qui, non ho mai dimostrato alcuna accortezza particolare, né un'esperienza di vita, e neppure una fredda ed equilibrata valutazione della situazione."


translate italian day6_us_98eb7069:


    "Ma la vera domanda è – ho mai mostrato tali cose in {i}quell'altra{/i} vita?"


translate italian day6_us_242a5814:


    th "E infatti, anche se ci penso…{w} Posso ricordarmi alcuni esempi, naturalmente, ma…"


translate italian day6_us_7a9e2886:


    me "Va bene, andiamo."


translate italian day6_us_c52254db:


    "Ero così offeso dalle parole di Ulyana che le ho chiesto:"


translate italian day6_us_537e718c:


    me "E com'è un uomo adulto secondo te?"


translate italian day6_us_1998fdba:


    us "Uno che è responsabile delle proprie azioni. Uno che non fa nulla senza prima pensare alle conseguenze. Una persona che è in grado di prendersi cura non solo di sé stesso, ma anche degli altri."


translate italian day6_us_27bca7e1:


    me "E tutto quello non ti riguarda di sicuro."


translate italian day6_us_bb9ef712_1:


    "Ho cercato di riderci sopra."


translate italian day6_us_c23eb093:


    us "Comunque non mi interessa fingere di essere così."


translate italian day6_us_ddb5934e:


    "Ha sorriso."


translate italian day6_us_7efaef44:


    "Sì, aveva assolutamente ragione.{w} Io non possedevo alcuna di quelle qualità."


translate italian day6_us_31f38e05:


    "Ma ho sempre pensato che bastasse avere l'età."


translate italian day6_us_35c738f1:


    "In generale, sembrava proprio che non vi fosse alcuna differenza tra me e le altre persone, e che avrei potuto essere esattamente come loro, se solo l'avessi voluto."


translate italian day6_us_bd922584:


    th "È forse tutto sbagliato?"


translate italian day6_us_0a98cbd7:


    "In realtà – no, perché in questo Campo ho cercato di comportarmi nel modo più logico, ragionevole e adeguato possibile…{w} Proprio come un adulto!"


translate italian day6_us_53d1df42:


    th "Ma se persino Ulyana la pensa diversamente…"


translate italian day6_us_bc130c9d:


    th "Allora come posso uscire da qui?!"


translate italian day6_us_cc350778:


    "Siamo entrati. Ulyana ci ha messo un po' a trovare l'interruttore della luce."


translate italian day6_us_ded0fd48:


    us "Et voilà!"


translate italian day6_us_5e0cd80f:


    me "Ben fatto, brava!"


translate italian day6_us_77e18087:


    "Le ho fatto un applauso sarcastico."


translate italian day6_us_ad0cb0ae:


    us "Ehi! Non prendermi in giro!"


translate italian day6_us_e16a3b7b:


    "Ha messo il broncio."


translate italian day6_us_80d646aa:


    me "Va bene, e adesso?"


translate italian day6_us_3671b8c0:


    us "Ecco qui!"


translate italian day6_us_d42abb00:


    "Ulyana si è avvicinata alla porta della seconda stanza, ha esitato per un istante e l'ha aperta bruscamente."


translate italian day6_us_724ee48f:


    "Non c'era molto spazio all'interno. Sembrava più un ripostiglio."


translate italian day6_us_7c05bc4a:


    "Scatoloni di varie dimensioni erano accatastati uno sopra l'altro, formando una sorta di Alpi in miniatura; vari dispositivi sparsi qua e là mi ricordavano la mente caotica di uno scienziato; scaffali pieni di libri sulla parete in fondo chiarivano che questo non era un semplice sgabuzzino per cianfrusaglie inutilizzate."


translate italian day6_us_d39afe89:


    "Alla mia destra c'era un televisore con un videoregistratore appoggiato accanto ad esso."


translate italian day6_us_a3c51985:


    "Stranamente non era di marca sovietica – direi piuttosto giapponese.{w} Almeno a giudicare dal suo aspetto."


translate italian day6_us_62c4c780:


    "Non c'è da stupirsi, in quel periodo (o più precisamente – in questo periodo), esistevano anche le importazioni."


translate italian day6_us_8bf0de68:


    us "Che ti avevo detto!"


translate italian day6_us_709cc631:


    "Ulyana ha sorriso trionfante."


translate italian day6_us_ff471f9a:


    me "Sì, sì…"


translate italian day6_us_6c9f5d6f:


    us "Hai mai visto qualcosa di simile prima d'ora?"


translate italian day6_us_756a29e3:


    me "Sì…"


translate italian day6_us_46586735:


    "Forse anch'io ho un registratore. Probabilmente sta in qualche angolo nel mio appartamento, ricoperto di polvere."


translate italian day6_us_cd67e98a:


    us "Sembra che tu non ne sia affatto sorpreso."


translate italian day6_us_95322256:


    me "Perché dovrei esserne sorpreso?"


translate italian day6_us_2c209cc5:


    us "È una TV giapponese, con un videoregistratore! Non mi dire che vedi spesso queste cose!"


translate italian day6_us_69f8f8a0:


    me "Hmm... non troppo spesso, ultimamente."


translate italian day6_us_78674bdc:


    "Ho concordato pigramente."


translate italian day6_us_7505e340:


    us "Sei così cupo…"


translate italian day6_us_469db7d9:


    us "Comunque, ok! Prendi qua!"


translate italian day6_us_4e5aa691:


    "Mi ha dato la cassetta.{w} Me la sono girata tra le mani, ma non vedevo alcun adesivo o etichetta su di essa."


translate italian day6_us_fb03e6d3:


    me "Cosa c'è registrato?"


translate italian day6_us_d23e2c34:


    us "Non lo so esattamente."


translate italian day6_us_9137ce1a:


    "Ha riflettuto."


translate italian day6_us_3bb74eea:


    us "Ma sono sicura che è qualcosa di molto interessante!"


translate italian day6_us_4121ba2c:


    "Ben presto il logo di una famosa azienda cinematografica è apparso sullo schermo."


translate italian day6_us_7331bf96:


    us "Ecco, ecco, guarda!"


translate italian day6_us_4653fbcc:


    "Ulyana ha persino iniziato ad agitarsi con impazienza."


translate italian day6_us_1b32e37e:


    me "Sto guardando…"


translate italian day6_us_f7aa1f59:


    "Pochi minuti più tardi mi sono reso conto che si trattava di un celebre film degli anni ottanta. Parlava di un robot venuto dal futuro, inviato per uccidere qualcuno nel passato, cosicché negli anni a venire non sarebbe nato un eroe…"


translate italian day6_us_f027422b:


    "Coincidenza interessante.{w} Si potrebbe dire che anch'io fossi venuto dal futuro."


translate italian day6_us_5a80f756:


    th "E quindi? La mia missione è quella di uccidere qualcuno dei residenti locali?"


translate italian day6_us_aa60fb72:


    "Non sono riuscito a trattenermi dal ridere."


translate italian day6_us_686edcc3:


    us "Cosa c'è di così divertente?"


translate italian day6_us_1a66e4d6:


    "Ulyana mi ha guardato con aria di rimprovero."


translate italian day6_us_3c5e2cfc:


    me "Oh, no, niente."


translate italian day6_us_b1a006e1:


    us "Sembra che il film non ti piaccia!"


translate italian day6_us_731f7b7d:


    me "Il film è carino.{w} L'ho visto diverse volte."


translate italian day6_us_433e89df:


    us "Ma com'è possibile?"


translate italian day6_us_41c9ba10:


    "I suoi occhi si sono spalancati per la sorpresa."


translate italian day6_us_78ab486a:


    me "Beh…{w} Un mio amico mi aveva dato una cassetta."


translate italian day6_us_6532261a:


    us "Hmm…"


translate italian day6_us_e2092d77:


    "Ulyana mi ha fissato intensamente."


translate italian day6_us_a9137dac:


    me "Va bene, non distraiamoci!"


translate italian day6_us_e868da76:


    "Ho indicato lo schermo."


translate italian day6_us_993a1e1e:


    "Quando eravamo a circa metà film, ho sentito qualcuno che cercava di aprire la porta d'ingresso."


translate italian day6_us_a2f7bc76:


    "Si dice che quando uno dei sensi è indebolito, gli altri vengono amplificati.{w} Ero abbastanza cieco, ma riuscivo a sentire perfettamente."


translate italian day6_us_0eebb0ce:


    me "La luce!"


translate italian day6_us_9db6b90d:


    "Ho sussurrato."


translate italian day6_us_b7ef3111:


    us "Cosa?"


translate italian day6_us_c18f0f1a:


    me "Spegni la luce!"


translate italian day6_us_39642f61:


    "Ulyana si è resa conto che questo non era il momento adatto per discutere con me, e si è precipitata verso l'interruttore.{w} Nel frattempo ho premuto il pulsante di pausa."


translate italian day6_us_955fefc7:


    "Ho sentito dei passi nella stanza accanto, e ho scorto il fascio di luce tremolante di una torcia da sotto la porta."


translate italian day6_us_bf71caa2:


    th "Mi chiedo chi abbia deciso di intrufolarsi nel circolo di cibernetica di notte?"


translate italian day6_us_de372b78:


    mt "Non sono nemmeno qui."


translate italian day6_us_d099a808:


    "Era la voce di Olga Dmitrievna."


translate italian day6_us_9b04210e:


    "Ben presto la porta d'ingresso si è richiusa, e ho fatto un sospiro di sollievo."


translate italian day6_us_505772f4:


    me "Lo vedi? È successo di nuovo, tutto per colpa tua!"


translate italian day6_us_b7ef3111_1:


    us "Che cosa?"


translate italian day6_us_50398fc3:


    "Ulyana mi ha fissato sorpresa."


translate italian day6_us_bd77af7a:


    me "Per quale altra ragione sarebbe venuta qui di notte?{w} Ci sta sicuramente cercando!"


translate italian day6_us_2405d879:


    me "Alisa probabilmente non si preoccupa della tua assenza, ma io sono registrato presso la dimora della leader del Campo!"


translate italian day6_us_60f3d758:


    "Ha pensato per un po'."


translate italian day6_us_2072b7ef:


    us "Beh, e allora?"


translate italian day6_us_e4a7abab:


    me "Come allora…? Perché sei sempre in cerca di guai? Sembra quasi che ti piaccia."


translate italian day6_us_10573fa6:


    us "Così è più divertente!"


translate italian day6_us_08fab8ce:


    me "Secondo te è divertente essere costantemente rimproverati e puniti?"


translate italian day6_us_0e694a7f:


    us "Chi non risica non rosica!"


translate italian day6_us_7a6bb0f1:


    "Ha riso allegramente."


translate italian day6_us_ff350d77:


    me "Alla fine bisogna anche rispettare le regole.{w} Beh, fin'ora sei riuscita a scamparla, ma chissà cosa potrebbe accadere la prossima volta! Potrebbe finire molto male!"


translate italian day6_us_43d44c0e:


    us "Stai brontolando come una nonnina!"


translate italian day6_us_3db311fc:


    "Mi ha detto, offesa."


translate italian day6_us_23e36b36:


    me "E chi ha detto di recente che io non posso essere considerato un adulto?"


translate italian day6_us_cff7648a:


    us "Lascia perdere, finiamo di guardare il film."


translate italian day6_us_d07a4ff2:


    "Ho deciso di non discutere con lei."


translate italian day6_us_e7c03f19:


    th "Alla fine, siamo già sulla lista di Olga Dmitrievna, e un'ora prima, un'ora dopo – che differenza fa?"


translate italian day6_us_e487a8f7:


    "Ad ogni scena spaventosa Ulyana sobbalzava, urlava e afferrava il mio braccio."


translate italian day6_us_0958d1fb:


    "Il film non mi appassionava molto, ma facevo comunque finta di essere interessato."


translate italian day6_us_12d2bbe0:


    "Oltre al robot c'era anche un personaggio buono che era stato mandato dal futuro per fermare i suoi piani malvagi."


translate italian day6_us_9a8a93f1:


    th "Forse io non sono qui con l'obiettivo di diventare una macchina assassina senza scrupoli, ma, al contrario, sono qui per cambiare le cose o per fermare qualcuno?"


translate italian day6_us_a140a31c:


    th "Per fare in modo che qualcosa non accada, ad esempio, o per impedire a qualcuno di compiere qualche malefatta…"


translate italian day6_us_2d270f7c:


    th "L'analogia è piuttosto interessante, ma non è basata su fatti reali."


translate italian day6_us_dd48914d:


    th "Non ho un fucile tra le mani, non sto indossando una giacca di pelle e non ho una moto da guidare…"


translate italian day6_us_30190641:


    th "Non ho nemmeno gli occhiali da sole!"


translate italian day6_us_8d2a1158:


    us "Guarda, guarda!"


translate italian day6_us_e99c1fdf:


    "Un'altra scena di un inseguimento con sparatorie e distruzioni di vari veicoli a quattro ruote ed elicotteri stava scorrendo sullo schermo."


translate italian day6_us_7d3685bb:


    me "Pensi che il tizio buono vincerà?"


translate italian day6_us_d6b61a8f:


    "Ovviamente sapevo già come finiva il film."


translate italian day6_us_6c856ecd:


    us "Ma certo!{w} Il bene trionfa sempre!"


translate italian day6_us_15e55f1b:


    "Ha detto con serietà."


translate italian day6_us_90e45f3b:


    "Considerando che Ulyana ha spesso avuto successo nelle sue malefatte, il significato della parola «bene» è risultato piuttosto distorto, detto da lei."


translate italian day6_us_3bc8c3ba:


    us "Aspetta! Ma tu l'hai già visto."


translate italian day6_us_0886d374:


    me "Sì, volevo solo sapere la tua opinione…"


translate italian day6_us_1a8f9a59:


    "Poco dopo il film si è concluso."


translate italian day6_us_8b49353c:


    "Durante le scene finali Ulyana correva per la stanza, cercando di ottenere la migliore visuale possibile di tutto ciò che stava accadendo sullo schermo, senza perdersi nulla."


translate italian day6_us_1c0e27c2:


    us "Accidenti!"


translate italian day6_us_80984763:


    "Ha tirato un sospiro di sollievo quando i titoli di coda hanno iniziato a scorrere sullo schermo."


translate italian day6_us_c874b643:


    me "Ti è piaciuto?"


translate italian day6_us_ae076645:


    us "E me lo chiedi?! Certo che mi è piaciuto!"


translate italian day6_us_f2e007f0_1:


    me "Bene allora…"


translate italian day6_us_8b59d260:


    us "Ma vedo che a te non è piaciuto."


translate italian day6_us_c4bf4ad4:


    "Ha strizzato gli occhi, sornione."


translate italian day6_us_09b227d4:


    me "Beh, è un film per bambini, dopotutto."


translate italian day6_us_f868e368:


    us "Ah... Davvero?"


translate italian day6_us_ed8eb672:


    me "Beh…{w} Sì, e allora?"


translate italian day6_us_565373fe:


    us "Niente."


translate italian day6_us_9827d8ef:


    "Ha sbadigliato e si è seduta per terra appoggiandosi a una pila di scatoloni."


translate italian day6_us_46f6e5ee:


    me "Forse è ora di andare a dormire."


translate italian day6_us_b1586995:


    us "Sono troppo stanca – non vado da nessuna parte."


translate italian day6_us_b8e7bf20:


    me "Come vuoi.{w} È meglio che io vada, allora."


translate italian day6_us_ba7d98ab:


    us "Ehi, aspetta!"


translate italian day6_us_80abfaf2:


    "Ulyana è balzata in piedi immediatamente e mi ha afferrato per un braccio."


translate italian day6_us_38a6668d:


    us "Vuoi lasciarmi qui tutta sola?"


translate italian day6_us_4cad9cd9:


    me "Beh, se vuoi posso trasportarti alla tua stanza."


translate italian day6_us_fefef18f:


    "Le ho detto in tono incerto."


translate italian day6_us_3f9a7fc5:


    "Dopotutto l'avevo già fatto prima."


translate italian day6_us_b1823081:


    us "No! Voglio dormire qui!"


translate italian day6_us_94133a62:


    "Ha frugato nello scatolone accanto a lei e ha tirato fuori una specie di coperta e dei cuscini."


translate italian day6_us_4bff6f94:


    me "Di nuovo la stessa storia…"


translate italian day6_us_25f24811:


    "Ho sospirato."


translate italian day6_us_10c6982a:


    me "Pensaci un attimo. Olga Dmitrievna ci sta già cercando. E se passassimo tutta la notte qui…"


translate italian day6_us_443d35ef:


    us "Beh, a dire il vero, lei sta cercando te. Ciò non ha nulla a che fare con me."


translate italian day6_us_be3315b6:


    "Ha riso Ulyana."


translate italian day6_us_fc9e5e8f:


    me "Anche se fosse, verresti catturata anche tu!"


translate italian day6_us_ff3c9f13:


    us "Cosa te lo fa pensare?{w} E anche se mi beccassero, va bene così, ormai ci sono abituata!"


translate italian day6_us_7eb76897:


    me "Perché sei così testarda? È solo un paio di centinaia di metri fino alla tua stanza."


translate italian day6_us_737dc545:


    "E inoltre, non si poteva di certo dire che Ulyana fosse così tanto esausta."


translate italian day6_us_4c76cf23:


    us "Sono così stanca!"


translate italian day6_us_3a4d70a0:


    "Si è avvolta in una coperta, si è allontanata da me e ha cominciato a fingere di russare diligentemente."


translate italian day6_us_6cacb19d:


    me "Va bene allora, ci vediamo domani."


translate italian day6_us_8f11bfa0:


    "Ho tentato di andarmene, ma Ulyana ha di nuovo afferrato la mia mano."


translate italian day6_us_69f4e5e6:


    me "Beh, e adesso che c'è?"


translate italian day6_us_8627abec:


    "Non mi ha risposto. Sembrava che l'avessi spaventata."


translate italian day6_us_feabf980:


    "In quel momento non sapevo proprio cosa fare."


translate italian day6_us_873663f3:


    th "La nostra piccola avventura con la videocassetta era una cosa stupida fin dall'inizio, ma ora tutto questo va ben oltre la mia comprensione."


translate italian day6_us_2ba6c73f:


    "Non riuscivo proprio a comprendere la logica che stava dietro alle sue azioni. Quale fosse il significato e la motivazione."


translate italian day6_us_0ffe8ded:


    "Ma nonostante tutto questo, non potevo dirle fermamente «no»."


translate italian day6_us_08b7211c:


    "Qualcosa mi bloccava. Forse un senso del dovere, o forse per pietà, o forse la pazienza che si deve avere nella comunicazione con i ragazzini."


translate italian day6_us_849cfae0:


    me "Va bene, cosa vuoi da me?"


translate italian day6_us_f1881724:


    us "Dormiamo insieme!"


translate italian day6_us_9a9afc69:


    "Mi pare di averlo già visto da qualche parte."


translate italian day6_us_b01cd732:


    me "Beh, suppongo…"


translate italian day6_us_4645bec9:


    "Ho spento le luci e mi sono sdraiato al suo fianco."


translate italian day6_us_ca6c74b8:


    "Per fortuna era estate, altrimenti il dormire sul pavimento di legno non sarebbe stata una grande idea."


translate italian day6_us_2ed1402b:


    th "E non vale la pena di menzionare l'assenza di un materasso."


translate italian day6_us_376cd472:


    us "Perché hai accettato così facilmente?"


translate italian day6_us_12bc6723:


    "Potevo sentire una nota di risentimento nella sua voce."


translate italian day6_us_87a707b8:


    me "Me l'hai chiesto – così l'ho fatto."


translate italian day6_us_e8799476:


    "In realtà, ero totalmente convinto che Ulyana si sarebbe addormentata rapidamente, come sempre, così avrei potuto portarla alla sua stanza."


translate italian day6_us_7e1e0002:


    th "In linea di principio, si tratta di un buon piano, nonostante il fatto che io dovrò fare di nuovo la bestia da soma."


translate italian day6_us_08c4f608:


    us "Ehi, parlami!"


translate italian day6_us_b1158062:


    me "Dormi e basta. Sei tu quella che voleva dormire."


translate italian day6_us_0bf1db7d:


    us "Parliamo!"


translate italian day6_us_c233b6a2_1:


    me "Di cosa?"


translate italian day6_us_c034a3dd:


    us "Dimmi qualcosa di interessante su di te, storie divertenti della tua vita."


translate italian day6_us_0668a617:


    me "La mia vita non è molto ricca di azione."


translate italian day6_us_2776dad8:


    us "Proprio niente da raccontare?"


translate italian day6_us_97091cb6:


    th "Anche se avessi qualcosa di interessante, raccontarglielo non sarebbe una buona idea."


translate italian day6_us_67eea6e1:


    me "Niente di niente."


translate italian day6_us_bb57e7a2:


    us "Non può essere così brutto!"


translate italian day6_us_9779d985:


    "Mi sono voltato verso Ulyana."


translate italian day6_us_691925cb:


    "Giaceva ad occhi spalancati, fissando il soffitto."


translate italian day6_us_e0f7ef7d:


    me "Può esserlo, credimi."


translate italian day6_us_22602e79:


    us "Ma è così noioso!"


translate italian day6_us_ba42ab02:


    me "Sì, a volte…"


translate italian day6_us_0e03ae69:


    "È rimasta in silenzio per un po', e poi ha sorriso, dicendomi:"


translate italian day6_us_77b3d75b:


    us "Ma qui ti sei divertito!"


translate italian day6_us_c10011a5:


    me "Oh, questo è sicuro!"


translate italian day6_us_89bea129:


    "Ho riso."


translate italian day6_us_563002a7:


    us "Ti ricorderai di questo Campo?"


translate italian day6_us_601cfdfa:


    th "Prima devo riuscire ad andarmene di qui."


translate italian day6_us_d06923c2:


    me "Certamente."


translate italian day6_us_21d8894d:


    us "E degli altri?"


translate italian day6_us_50b56044:


    me "Sì…"


translate italian day6_us_f4bcf7be:


    us "E di me?"


translate italian day6_us_aa854ae9:


    me "Anche di te…"


translate italian day6_us_93a0d0b4:


    "Stavo dicendo tutto in tono piatto e, ad essere sinceri, senza pensare al significato delle sue domande."


translate italian day6_us_e73afb35:


    us "Anch'io mi ricorderò di te…"


translate italian day6_us_a60d2254:


    "Ha sussurrato Ulyana."


translate italian day6_us_3c6c3d52:


    "E subito ho sentito la sua testa appoggiarsi al mio petto, e la sua mano avvolgersi intorno al mio collo."


translate italian day6_us_9f8d7b41:


    me "Comoda?"


translate italian day6_us_0162dcba:


    us "Sì."


translate italian day6_us_acaae73d:


    "Ha mormorato."


translate italian day6_us_2f598c90:


    me "Non hai trovato un posto migliore?"


translate italian day6_us_f1d4b5e2:


    "Ha scosso leggermente la testa."


translate italian day6_us_1943e342:


    th "Beh, lasciamo che se ne stia un po' così."


translate italian day6_us_47e54bbf:


    th "Dopotutto, forse in questo modo si addormenterà prima."


translate italian day6_us_a20cefa7_4:


    "..."


translate italian day6_us_94caab83:


    "Probabilmente non erano ancora trascorsi cinque minuti quando ho deciso di controllare se si fosse addormentata, scuotendole leggermente la spalla."


translate italian day6_us_b7ef3111_2:


    us "Cosa c'è?"


translate italian day6_us_322c760e:


    "Ha chiesto senza alzare lo sguardo."


translate italian day6_us_16a426b8:


    "Ho sentito una nota insolita nella sua voce."


translate italian day6_us_f075d0eb:


    me "Perché non stai dormendo?"


translate italian day6_us_a63d8c58:


    us "Senti chi parla!"


translate italian day6_us_a20cefa7_5:


    "..."


translate italian day6_us_28e2eb35:


    "Il tempo scorreva insidiosamente lento."


translate italian day6_us_d499dbc6:


    "Ho deciso di aspettare almeno altri dieci minuti prima di controllare nuovamente."


translate italian day6_us_8efc776c:


    "Ma i secondi sembravano ore…"


translate italian day6_us_7d75fee5:


    "I miei occhi hanno iniziato a chiudersi lentamente.{w} Ho fatto fatica a rimanere sveglio."


translate italian day6_us_d580dc80:


    "Ho sbattuto le palpebre per un istante e poi mi sono svegliato, solo per rendermi conto che stavo per immergermi in un sonno profondo."


translate italian day6_us_f9df56b2:


    th "Forse potrei chiudere gli occhi solo per un minuto?{w} Cosa ci sarebbe di male?"


translate italian day6_us_6abdb446:


    "Tale pensiero mi è balenato nella testa."


translate italian day6_us_b8b42840:


    "E subito la mia mente si è spenta..."


translate italian day6_sl_662a5ccc:


    "Non ho dormito bene."


translate italian day6_sl_84b3863a:


    "Continuavo a svegliarmi per un paio di minuti e poi mi riaddormentavo nuovamente."


translate italian day6_sl_8076c467:


    "Non ho sognato molto. Solo alcune immagini grottesche, dimenticate al mattino."


translate italian day6_sl_b893b471:


    "Olga Dmitrievna mi ha svegliato."


translate italian day6_sl_93a88bb1:


    mt "È ora di alzarsi!"


translate italian day6_sl_c96aaf8e:


    "Mi sono strofinato gli occhi e l'ho guardata con aria interrogativa."


translate italian day6_sl_f6a4fbf2:


    me "Che ore sono?"


translate italian day6_sl_42495f59:


    mt "Abbastanza tardi!"


translate italian day6_sl_ea0df862:


    "Mi faceva male dappertutto e la mia testa era pesante come una palla da bowling."


translate italian day6_sl_e419b053:


    mt "Riprenditi e vai a lavarti, subito!"


translate italian day6_sl_e9440aad:


    "Mi sono alzato con obbedienza, ho preso la mia borsa, e sono uscito."


translate italian day6_sl_4a70bb48:


    "Non avevo voglia di parlare con la leader del Campo in quel momento."


translate italian day6_sl_f672dc60:


    th "Sicuramente mi aspetta una bella ramanzina.{w} Una pesante."


translate italian day6_sl_1c2ef58a:


    th "Ma è meglio rimandarla al più tardi possibile."


translate italian day6_sl_06732be2:


    "Dopotutto, ho sempre rimandato le cose."


translate italian day6_sl_36a0fcdc:


    "Cercando di non addormentarmi in piedi, in qualche modo sono riuscito a raggiungere i lavabi.{w} L'acqua fredda mi ha fatto riprendere i sensi."


translate italian day6_sl_ae8a5bf9:


    th "Forse dovrei far visita a Slavya? Ho ancora un po' di tempo libero."


translate italian day6_sl_a714f4e1:


    th "Solo per scusarmi con lei…"


translate italian day6_sl_7daba806:


    th "D'altra parte, perché dovrei giustificarmi?{w} È stata una sua idea."


translate italian day6_sl_1398f30a:


    th "E poi cosa, che cosa ho fatto di sbagliato?"


translate italian day6_sl_8e04721c:


    th "A parte l'essermi immischiato in un'altra stupida situazione, quando tutte le probabilità sono contro di me…"


translate italian day6_sl_3d8cc6b9:


    "Ho fatto un profondo respiro e sono tornato alla stanza di Olga Dmitrievna, con la ferma intenzione di proclamarmi innocente, o almeno mostrare la mia determinazione."


translate italian day6_sl_cb36cd72:


    "Ma la stanza era vuota."


translate italian day6_sl_efc1096d:


    "Mi sono seduto sul letto, immergendo il viso tra le mie mani."


translate italian day6_sl_6a13efce:


    th "Devo inventarmi una scusa. Adesso. Una bella e credibile scusa."


translate italian day6_sl_114a09ac:


    th "Se lei non dovesse credere alla verità.{w} Beh, non mi sorprenderebbe – se fossi in lei, non ci crederei nemmeno io…"


translate italian day6_sl_e967e0d0:


    "Non riuscivo a pensare a nulla di sensato."


translate italian day6_sl_ae02f020:


    "Me ne stavo seduto lì, guardando il sorgere del sole."


translate italian day6_sl_f25133e7:


    "La porta ha scricchiolato silenziosamente, e la leader del Campo è entrata nella stanza."


translate italian day6_sl_3a4485f1:


    me "Olga Dmitrievna, vorrei spiegarle nuovamente che…"


translate italian day6_sl_cf0c0005:


    mt "Non ce n'è bisogno…"


translate italian day6_sl_c5e1ce92:


    "L'ho guardata, sorpreso."


translate italian day6_sl_79c7318e:


    mt "Capisco tutto!"


translate italian day6_sl_91b3c16d:


    me "Allora capisce anche che non era ciò che sembrava?"


translate italian day6_sl_6889d390:


    mt "Perché, sembrava esattamente quello che era!{w} Non voglio criticare te o Slavya. Ho capito tutto, la pubertà, gli ormoni, tutto quel genere di cose…"


translate italian day6_sl_47b5cf5f:


    "Ho sospirato stancamente, affondando nuovamente la testa tra le mani."


translate italian day6_sl_93a58ce6:


    me "Cosa devo dirle affinché lei mi creda?"


translate italian day6_sl_c8c68b8a:


    mt "Niente.{w} Te l'ho detto già detto – capisco tutto."


translate italian day6_sl_8ba64c1f:


    me "Non capisce proprio niente…"


translate italian day6_sl_b459e062:


    "Ho borbottato sottovoce."


translate italian day6_sl_f9270db9:


    mt "Bene, è ora dell'allineamento!"


translate italian day6_sl_106dc472:


    "Mi sono alzato in piedi e mi sono trascinato stancamente dietro di lei."


translate italian day6_sl_dd234e0f:


    me "Ma potrei almeno farle visita?"


translate italian day6_sl_b71e2412:


    mt "Certo che no."


translate italian day6_sl_7318b2da:


    "La leader del Campo mi ha guardato con grande sorpresa, come se la mia domanda fosse stata del tutto inappropriata."


translate italian day6_sl_c3fad59f:


    mt "Ho già avvertito l'infermiera di non farti entrare."


translate italian day6_sl_c2e10946:


    me "Beh, posso almeno sapere come sta?"


translate italian day6_sl_a809ce74:


    mt "Sembra che Slavya si sia proprio ammalata."


translate italian day6_sl_66a3dff1:


    "Non sono riuscito a chiederle quanto fosse grave, dato che avevamo già raggiunto la piazza."


translate italian day6_sl_90404e3b:


    mt "Mettiti in fila con gli altri."


translate italian day6_sl_2dca417a:


    "Ho preso posto accanto ad Electronik."


translate italian day6_sl_4f3f853d:


    el "Allora, cos'è successo ieri?"


translate italian day6_sl_ae5c7f77:


    "Mi ha sussurrato."


translate italian day6_sl_cb23542f:


    me "Di cosa stai parlando?"


translate italian day6_sl_e1649ec8:


    el "Beh, sei improvvisamente scomparso."


translate italian day6_sl_9553ce6a:


    me "Niente di speciale…"


translate italian day6_sl_ca0c06f1:


    th "Raccontare tutto all'intero Campo è l'ultima cosa che voglio."


translate italian day6_sl_3490597b:


    th "Dopotutto, se persino la leader del Campo ha frainteso la situazione, allora chissà cosa penserebbero gli altri…"


translate italian day6_sl_b37413a9:


    "Anche se stavo già cominciando a pensare che non avesse poi così tanto {i}frainteso{/i}."


translate italian day6_sl_e6dd2114:


    "L'intero allineamento è consistito in un appello, e nell'annuncio che oggi le docce sarebbero rimaste chiuse per lavori."


translate italian day6_sl_244fe891:


    "Alla fine i pionieri si sono diretti verso la mensa per far colazione, chiacchierando allegramente.{w} Li ho seguiti miseramente."


translate italian day6_sl_ad482dd4:


    "Francamente parlando, non ero proprio in vena di mangiare."


translate italian day6_sl_09b5d477:


    "Dopo aver passeggiato un po' davanti alle porte della mensa me ne sono andato."


translate italian day6_sl_39e8f495:


    "Non so come sia successo, ma alla fine mi sono ritrovato esattamente davanti all'infermeria."


translate italian day6_sl_1a778ee7:


    "Era come se il pilota automatico mi avesse portato lì."


translate italian day6_sl_770ba296:


    "Ho fissato la porta, totalmente indeciso sul da farsi."


translate italian day6_sl_703f77b3:


    th "Non mi lasceranno entrare in alcun modo…{w} Cosa posso fare?"


translate italian day6_sl_bf506fe5:


    th "Dovrei aspettare la sentenza di Olga Dmitrievna, se mai arrivasse?{w} Dovrei aspettare che la voce si sparga per il Campo?"


translate italian day6_sl_917651b4:


    th "E quello sarebbe ancora peggio!{w} Sguardi da ogni angolo, l'alienazione totale…"


translate italian day6_sl_45615f79:


    "Questo è ciò che sarebbe accaduto di sicuro."


translate italian day6_sl_6a4721bc:


    th "No, non ho fatto niente di male, ma nella cerchia chiusa di questa bizzarra società, tale comportamento verrebbe considerato, se non proprio un peccato mortale, sicuramente una grave perversione."


translate italian day6_sl_2f9f3e25:


    th "E ciò significa che non ho più nulla da perdere!"


translate italian day6_sl_991e1a72:


    "Ho bussato senza alcuna esitazione."


translate italian day6_sl_89ac2598:


    cs "Solo un momento!"


translate italian day6_sl_01d2073c:


    "Ho tirato la maniglia, ma la porta non si è aperta."


translate italian day6_sl_13207c20:


    th "Hmm, è chiusa a chiave?"


translate italian day6_sl_e26ebd50:


    "Dopo un attimo ho sentito la chiave girare nella toppa, e l'infermiera è apparsa."


translate italian day6_sl_a5eeb60e:


    cs "Buongiorno, pioniere…"


translate italian day6_sl_32f6d96b:


    me "Salve!"


translate italian day6_sl_df3861de:


    "Ho risposto con sicurezza."


translate italian day6_sl_0e5cbc98:


    cs "Lo sai che non ti è permesso entrare qui?"


translate italian day6_sl_1654d9af:


    me "Beh, lo so…{w} Ma se potesse…"


translate italian day6_sl_e5ee009f:


    cs "Nah… Non è così che funzionano le cose!"


translate italian day6_sl_7665ceec:


    "Ha detto scherzosamente."


translate italian day6_sl_a786531b:


    cs "Gli ordini sono ordini, dopotutto.{w} Se proprio me lo chiedi, personalmente non ci trovo niente di immorale, ma Olga Dmitrievna…"


translate italian day6_sl_8b820e8f:


    me "Forse posso fare qualcosa...?"


translate italian day6_sl_5d8ff9ca:


    cs "No, non puoi."


translate italian day6_sl_cb795a40:


    "L'ho guardata con rabbia, mi sono voltato e mi sono allontanato con lentezza teatrale."


translate italian day6_sl_34d50336:


    "Non sapevo cosa fare…"


translate italian day6_sl_2e89c23b:


    "Mi sono limitato a camminare, battendo il pugno sugli alberi che si trovavano sulla mia via."


translate italian day6_sl_a20cefa7:


    "..."


translate italian day6_sl_a2e15f1e:


    "Mi sono ripreso da quell'esplosione di rabbia solo quando avevo le mani gravemente ferite, e il labbro sanguinante a causa dei morsi."


translate italian day6_sl_1947d5ea:


    "E la cosa più interessante è che tutto il mio incredibile risveglio in uno sconosciuto campo di pionieri si è trasformato nel più banale dei drammi quotidiani.{w} Un dramma stupido come l'intera mia esistenza."


translate italian day6_sl_82da43b7:


    "Ancora una volta ho fatto qualcosa di sbagliato, ho pensato a qualcosa di sbagliato, e mi hanno totalmente frainteso…"


translate italian day6_sl_a51631b5:


    th "Bene, allora lascerò che le cose vadano come devono andare…"


translate italian day6_sl_c0292f4c:


    "Non c'era nessuno nei pressi della mensa."


translate italian day6_sl_c23a417d:


    "Ho fatto un sospiro di sollievo e mi sono seduto nell'angolo più remoto, sperando che nessuno mi vedesse."


translate italian day6_sl_ac6511c6:


    "Dopo aver inghiottito in qualche modo la mia colazione, stavo per andarmene, quando Miku si è precipitata verso il mio tavolo."


translate italian day6_sl_337999bd:


    mi "Oh, buongiorno, Semyon! Ho dormito un po' troppo a lungo oggi… Posso sedermi qui accanto a te?"


translate italian day6_sl_705add58:


    "Si è seduta senza attendere la mia risposta."


translate italian day6_sl_4b92e09f:


    me "Beh, stavo per andarmene…"


translate italian day6_sl_579406ce:


    mi "È vero che tu e Slavya siete… così? Beh, sai di cosa sto parlando."


translate italian day6_sl_5b30cd59:


    "Mi ha fissato da vicino, sbattendo le palpebre."


translate italian day6_sl_b9e0f0b2:


    me "Chi te l'ha detto?"


translate italian day6_sl_e3364d6a:


    "Beh, mi aspettavo che le voci si sarebbero sparse rapidamente.{w} Ma non così rapidamente!"


translate italian day6_sl_331ec19b:


    "Ho iniziato lentamente ad andare su tutte le furie."


translate italian day6_sl_f4fd2d5d:


    mi "Me l'ha detto Zhenya, e probabilmente gliel'ha detto Cheesekov. E lui l'ha sentito da Olga Dmitrievna. Non fraintendermi – non sono affari miei, ma… Se non vuoi, non dirmelo! Ma se non è un segreto…"


translate italian day6_sl_a3ccfa9b:


    me "Hai ragione. Non voglio dirtelo."


translate italian day6_sl_1203e79e:


    "Mi sono alzato e mi sono diretto rapidamente verso l'uscita."


translate italian day6_sl_23fd5e58:


    "Era una giornata afosa.{w} Il che ha amplificato la mia rabbia, dal momento che odio l'afa."


translate italian day6_sl_57084d4c:


    "In questo momento però tutto avrebbe potuto farmi perdere le staffe.{w} Ma dovevo pensare a come mi sarei dovuto comportare."


translate italian day6_sl_50ccb861:


    "Non volevo incontrare nessuno, così sono andato nella foresta, dove potevo pensare in tutta tranquillità."


translate italian day6_sl_1d5b76c1:


    "Dopo essermi fermato in una radura, mi sono seduto su un ceppo, ho preso un bastone e ho iniziato a giocherellarci."


translate italian day6_sl_4104b753:


    th "Se nulla dipende da me, allora l'unica cosa rimasta da fare è aspettare."


translate italian day6_sl_8763d3f6:


    th "Ma…{w} In realtà, cosa posso fare?"


translate italian day6_sl_6c70ad8a:


    th "Assaltare l'infermeria?{w} E anche se ci riuscissi, cosa accadrebbe dopo?"


translate italian day6_sl_630e01c1:


    th "Cercare di far ragionare Olga Dmitrievna?{w} È quasi impossibile…"


translate italian day6_sl_3b57bdd9:


    "Stranamente, tutto questo enigma si è trasformato in una semplice situazione quotidiana.{w} Se fosse accaduto nel mio mondo, non ci sarebbe stato alcun problema."


translate italian day6_sl_115eb200:


    "Dai, davvero, qual è il problema – anche se ho dormito con quella ragazza, chi se ne frega?"


translate italian day6_sl_964f46c9:


    "Anche se fossi davvero un diciassettenne, non sarebbe successo niente di terribile in ogni caso… Sarebbe stato un po' più imbarazzante, ma sempre tollerabile…"


translate italian day6_sl_f32a5b64:


    th "E adesso?{w} Non sono di certo padrone del mio destino…"


translate italian day6_sl_46c5eccc:


    "Tuttavia, lo sono mai stato?{w} C'è stato forse qualcosa nell'intera mia esistenza che fosse dipeso da me?"


translate italian day6_sl_a9996173:


    "Avevo mai raggiunto un obiettivo che mi ero posto?"


translate italian day6_sl_b7175618:


    th "E adesso che ne ho raggiunto uno, non riesco a gestirlo…"


translate italian day6_sl_19ac515c:


    th "Forse sono sempre stato un semplice spettatore fin dall'inizio?{w} La mia misteriosa apparizione in questo Campo, tutti i residenti locali, tutte le cose che sono successe…"


translate italian day6_sl_ee7dab86:


    th "Tutto questo è solo uno strano gioco, e io sono una semplice pedina."


translate italian day6_sl_4f0726c7:


    th "Beh, anche se fosse, cercherò di giocare al meglio il mio ruolo!"


translate italian day6_sl_8af8d5f1:


    "Mi sono alzato e mi sono diretto al Campo, pronto a far strage."


translate italian day6_sl_e773fa2d:


    "Il piano non è stato definito in dettaglio nella mia testa, ma ormai mi sono deciso!"


translate italian day6_sl_06b4910d:


    "Non c'era nessuno nella stanza della leader del Campo."


translate italian day6_sl_be72f150:


    "Ho frugato negli armadietti e ho preso un sacco a pelo, i miei vestiti invernali, una torcia elettrica e alcuni altri indumenti che ho trovato."


translate italian day6_sl_e5172865:


    "Appena sono uscito mi sono imbattuto in Alisa."


translate italian day6_sl_5959997f:


    dv "Stai andando in campeggio?"


translate italian day6_sl_a90c5245:


    me "Sì, in campeggio."


translate italian day6_sl_df3861de_1:


    "L'ho rassicurata."


translate italian day6_sl_07a7866b:


    dv "Beh, buona fortuna!"


translate italian day6_sl_79ac8364:


    "Mi ha fatto un sorriso sornione e se n'è andata per i fatti suoi."


translate italian day6_sl_885cf3e5:


    "Ho affrettato il passo verso l'infermeria."


translate italian day6_sl_8c0459e9:


    th "Anche l'infermiera è un essere umano dopotutto, dovrà pur andare a pranzo."


translate italian day6_sl_c3cfebc5:


    "Mi sono nascosto tra i cespugli, in attesa."


translate italian day6_sl_a20cefa7_1:


    "..."


translate italian day6_sl_32d9f799:


    "Il tempo è volato via veloce.{w} Nella mia mente tutto era chiaro, ed ero molto determinato."


translate italian day6_sl_aa94e82e:


    "Non avevo pensato ad un piano, a parte i primi passi – semplicemente non m'importava."


translate italian day6_sl_5236e874:


    th "La cosa più importante è comportarsi come meglio si crede – e lasciare che tutto il resto accada da sé."


translate italian day6_sl_15377a38:


    "Alla fine l'infermiera è uscita dall'edificio."


translate italian day6_sl_0c4f3316:


    "Si è guardata intorno e, dopo essersi assicurata che non ci fosse alcun pericolo, ha chiuso la porta a chiave e si è recata di corsa verso la mensa."


translate italian day6_sl_808818b1:


    "Dopo che è sparita dietro l'angolo mi sono avvicinato alla finestra in pochi balzi, e ho iniziato a bussare freneticamente su di essa."


translate italian day6_sl_7d8aa336:


    "Slavya è apparsa poco dopo."


translate italian day6_sl_0e4abe5d:


    sl "Semyon, che ci fai qui?{w} Non ti è permesso…"


translate italian day6_sl_3ce9b29e:


    me "Come ti senti?"


translate italian day6_sl_6d6acda2:


    sl "Sto bene…{w} Ma Olga Dmitrievna mi ha detto di rimanere qui per un altro giorno…"


translate italian day6_sl_d1eb8d48:


    th "A me ha detto qualcosa di totalmente diverso!"


translate italian day6_sl_e00f006d:


    me "Ok. Esci."


translate italian day6_sl_8df31520:


    "Le ho detto con fermezza."


translate italian day6_sl_ba36c8c0:


    sl "Di cosa stai parlando? A quale scopo?"


translate italian day6_sl_fb77ae46:


    "Ha chiesto con timore."


translate italian day6_sl_7a4ad964:


    me "Esci subito! Ti spiegherò tutto più tardi!"


translate italian day6_sl_44e59b45:


    "Mi ha guardato da vicino per un po', ma poi ha aperto la finestra senza ulteriori obiezioni."


translate italian day6_sl_c0792059:


    "L'ho aiutata a saltare giù."


translate italian day6_sl_05a7a65b:


    sl "Ok, e adesso?"


translate italian day6_sl_b749d8c0:


    me "Andiamo!"


translate italian day6_sl_4a86fb6e:


    "L'ho presa per mano e mi sono diretto verso la foresta."


translate italian day6_sl_2754ebb3:


    "Slavya si è fermata pochi minuti più tardi."


translate italian day6_sl_aa07a769:


    sl "Semyon, ma cosa stiamo facendo?"


translate italian day6_sl_70ae8bb8:


    me "Che c'è di sbagliato? Credi che si siano comportati in modo corretto nei nostri confronti?"


translate italian day6_sl_149bd1f8:


    sl "No, non lo credo, ma sarà ancora peggio in questo modo."


translate italian day6_sl_8c2bd012:


    me "E perché? Abbiamo il diritto di agire nel modo che reputiamo giusto!"


translate italian day6_sl_9ee0cfcb:


    sl "Ok, ma cosa succederà dopo?"


translate italian day6_sl_936b630d:


    "Non ci avevo ancora pensato."


translate italian day6_sl_40898a23:


    "Il mio piano stava andando abbastanza bene fino ad ora, ma non avevo voglia di pensare al passo successivo."


translate italian day6_sl_6bf1e9fc:


    me "Guarda!"


translate italian day6_sl_b46c28f2:


    "Ho aperto la borsa e le ho mostrato le cose che avevo preso dalla stanza della leader del Campo."


translate italian day6_sl_e26abf96:


    sl "E?"


translate italian day6_sl_0fa153ce:


    "Slavya mi ha guardato con aria interrogativa."


translate italian day6_sl_ccf6bd0d:


    me "Non lo so…{w} Resteremo nella foresta per un po'…{w} Per mostrare a tutti che la nostra opinione conta!"


translate italian day6_sl_c23e3fba:


    sl "Ma è una cosa stupida e infantile!"


translate italian day6_sl_59c4f9d9:


    me "Beh, se proprio non vuoi…"


translate italian day6_sl_d4d98b56:


    "Mi ha guardato in modo lamentevole."


translate italian day6_sl_eb5caabf:


    sl "Farò come dici tu…"


translate italian day6_sl_ecece0fc:


    "Quelle parole mi hanno fatto venire la pelle d'oca."


translate italian day6_sl_0834429f:


    th "«Come dici tu»…"


translate italian day6_sl_6428f6b8:


    "Nessuno mi aveva mai detto una cosa del genere."


translate italian day6_sl_c747ecee:


    me "In ogni caso…{w} Credo che questa sia la decisione migliore per il momento!"


translate italian day6_sl_6f9aa3f1:


    sl "D'accordo…"


translate italian day6_sl_ecfe56f5:


    "Ho appoggiato per terra il sacco a pelo e ci siamo seduti su di esso."


translate italian day6_sl_032a8535:


    sl "Allora, cosa facciamo?"


translate italian day6_sl_7479b07e:


    me "Non lo so…"


translate italian day6_sl_83657023:


    "Non sono mai stato così vicino a proclamare il fallimento."


translate italian day6_sl_6f0e498c:


    "No, non stavo mettendo in discussione la mia scelta, ma non avevo idea di cosa avremmo fatto in seguito."


translate italian day6_sl_9d1d5c5c:


    me "Tu cosa suggerisci?"


translate italian day6_sl_8cefafd9:


    sl "Sarebbe bello avere qualcosa da mangiare…{w} Hai portato qualcosa con te?"


translate italian day6_sl_a5ff15c9:


    th "Ed è proprio quello che mi sono scordato…"


translate italian day6_sl_5cb822ee:


    me "No, non ho niente…"


translate italian day6_sl_0080cf26:


    "Ho risposto lentamente."


translate italian day6_sl_6f9aa3f1_1:


    sl "Va bene…"


translate italian day6_sl_0b938548:


    "Siamo rimasti seduti in silenzio."


translate italian day6_sl_e893c99b:


    th "Mi chiedo se ci stiano cercando.{w} Ma anche se lo facessero, avrebbero scarse probabilità di successo – la foresta è abbastanza grande."


translate italian day6_sl_e870b5fc:


    th "Ma il cibo…{w} Fino a sera avrò fame anch'io…"


translate italian day6_sl_55e17cab:


    me "Ok resta qui, torno subito."


translate italian day6_sl_29684dd8:


    sl "Dove stai andando?"


translate italian day6_sl_b8a8c899:


    me "A prendere qualcosa da mangiare!"


translate italian day6_sl_a81ddd10:


    sl "Forse dovremmo andare insieme?"


translate italian day6_sl_a428b9a5:


    me "No… Torno in un batter d'occhio!"


translate italian day6_sl_775467e0:


    "Sono tornato al Campo a passo svelto, lasciando Slavya sola nella radura."


translate italian day6_sl_bbc0779a:


    "Ero assolutamente certo che non sarebbe andata da nessuna parte e mi avrebbe aspettato.{w} Avrei voluto avere la stessa certezza nel ricordarmi la via del ritorno..."


translate italian day6_sl_d8d6f4b9:


    th "Anche se non avessero ancora iniziato a cercarci, quantomeno avranno scoperto che Slavya è sparita, quindi devo muovermi con cautela e cercare di non farmi notare."


translate italian day6_sl_60719f55:


    th "In generale, ogni singolo pioniere potrebbe essere una minaccia ora. Non solo quelli che mi conoscono – un rapimento così evidente sarebbe noto a tutti in breve tempo."


translate italian day6_sl_90bcafae:


    "Anche se arrivare alla mensa in pochi scatti non è stato affatto difficile, non avevo idea di cosa avrei fatto in seguito."


translate italian day6_sl_566963d3:


    th "Non posso semplicemente andare là dentro come se niente fosse…"


translate italian day6_sl_4a1751e3:


    us "Ti stai nascondendo?"


translate italian day6_sl_e9efaf89:


    "Mi sono spaventato a morte."


translate italian day6_sl_6377c405:


    "Ulyana stava in piedi dietro di me."


translate italian day6_sl_a1efee9e:


    us "Hai fame?"


translate italian day6_sl_c4b83f7b:


    "Mi ha fissato."


translate italian day6_sl_9ba98794:


    us "E dai, perché sei così silenzioso?"


translate italian day6_sl_d6658e34:


    "Per un attimo sono rimasto senza parole e non riuscivo a pensare a una risposta decente."


translate italian day6_sl_961f4581:


    us "Allora, perché sei venuto?"


translate italian day6_sl_1a3f56c5:


    us "Oh, davvero...?"


translate italian day6_sl_e7ec8520:


    me "Sì, davvero..."


translate italian day6_sl_b7e4de4c:


    us "Ok, torno subito!"


translate italian day6_sl_30a664f4:


    "Con mia grande sorpresa, Ulyana non ha fatto domande imbarazzanti."


translate italian day6_sl_e2f777eb:


    "In un batter d'occhio è sparita dalla mia vista, oltrepassando le porte della mensa."


translate italian day6_sl_a977e8fd:


    "Mi chiedevo se dovessi fidarmi di lei e aspettarla, o se dovessi fuggire prima che tornasse con i rinforzi...?"


translate italian day6_sl_ab25b248:


    "Tuttavia Ulyana ha subito interrotto i miei dubbi, ricomparendo dopo un paio di minuti, con un sacchetto di plastica tra le sue mani."


translate italian day6_sl_9a7ad094:


    us "Ecco qui!"


translate italian day6_sl_b493cbf1:


    "Il sacchetto era pieno di panini e qualche pacchetto di kefir."


translate italian day6_sl_65a156a9:


    me "Perché mi stai aiutando?"


translate italian day6_sl_336a06df:


    us "Beh… Perché è una cosa coraggiosa… e… forte!"


translate italian day6_sl_945c59bc:


    "Ha sorriso sornione."


translate italian day6_sl_83d1e252:


    us "Salutami Slavya!"


translate italian day6_sl_233fc538:


    "Ulyana mi ha salutato con la mano ed è corsa via."


translate italian day6_sl_f6f60dcb:


    "Sono rimasto lì confuso per un po', ma poi sono tornato nella foresta."


translate italian day6_sl_2ae59a66:


    "Non era proprio una cosa da Ulyana ma, francamente, le ero grato."


translate italian day6_sl_a3e91759:


    "Alla fine non sarei riuscito di certo ad ottenere il cibo da solo durante il giorno, senza finire nei guai…"


translate italian day6_sl_f703ec01:


    "Quando sono arrivato nella radura ho trovato Slavya seduta sul sacco a pelo nella stessa identica posizione di prima."


translate italian day6_sl_482db2cf:


    me "Ecco qua…"


translate italian day6_sl_8dadd6b7:


    "Le ho consegnato il sacchetto."


translate italian day6_sl_e7830fb4:


    sl "Hai fatto così in fretta!"


translate italian day6_sl_0b70dfeb:


    "Ha detto con entusiasmo."


translate italian day6_sl_254d4d53:


    me "Beh, ho trovato aiuto…"


translate italian day6_sl_af5c4880:


    "Slavya mi ha lanciato uno sguardo interrogativo, ma non sono sceso nei dettagli."


translate italian day6_sl_1ec45799:


    "Abbiamo mangiato alacremente, senza dire una parola."


translate italian day6_sl_563de47a:


    "Alla fine ho sospirato con soddisfazione."


translate italian day6_sl_af6fb71f:


    me "Buono, davvero niente male."


translate italian day6_sl_8b01f8be:


    sl "Già."


translate italian day6_sl_a4965e91:


    "Ha concordato Slavya."


translate italian day6_sl_eacac508:


    sl "E adesso cosa facciamo?"


translate italian day6_sl_d60537c4:


    me "Hai forse qualche suggerimento?"


translate italian day6_sl_1210502c:


    sl "Riguardo a cosa?"


translate italian day6_sl_6a46b67c:


    me "Beh, almeno su come comportarci con la leader del Campo."


translate italian day6_sl_29040378:


    sl "Non lo so…{w} Ma sei sicuro che…"


translate italian day6_sl_748c8dfc:


    me "Sì, lo sono!"


translate italian day6_sl_e5e97d5d:


    "L'ho interrotta."


translate italian day6_sl_ccf4cd89:


    me "Si tratta di un atto di protesta, dopotutto!{w} Lei dev'essere consapevole del fatto che non può darmi ordini…"


translate italian day6_sl_2a7a8a3b:


    "Ho balbettato."


translate italian day6_sl_704441dd:


    me "… che non può darci ordini!"


translate italian day6_sl_4d7af59e:


    "Mi è sembrato di aver detto qualcosa di stupido."


translate italian day6_sl_97d14a00:


    "Anche se ero davvero il fattorino di Olga Dmitrievna, Slavya era…"


translate italian day6_sl_f9d7b013:


    sl "Sei proprio divertente!"


translate italian day6_sl_b7bd4e35:


    "Mi ha fatto un sorriso simpatico."


translate italian day6_sl_ee9341f5:


    sl "Potremmo semplicemente andare a parlare con lei.{w} Ma se preferisci restare seduto qui e aspettare, per me va bene lo stesso."


translate italian day6_sl_6c4c506e:


    me "Ho già cercato di parlare con lei…"


translate italian day6_sl_c77d0992:


    sl "Sei sicuro di averlo fatto nel modo giusto?"


translate italian day6_sl_32522388:


    th "Quale sarebbe il modo giusto?"


translate italian day6_sl_002325fc:


    "Ad essere sincero, non conoscevo la risposta."


translate italian day6_sl_33279267:


    "Certi discorsi imbarazzanti sono sempre stati qualcosa di insormontabile per me."


translate italian day6_sl_28d5f3e2:


    "Scommetto che se avessi saputo come affrontare tali casi, le cose sarebbero andate in modo diverso."


translate italian day6_sl_6650b9b5:


    "Naturalmente, dal momento che ero timoroso e che mi sentivo in colpa, non avevo alcuna possibilità di essere sicuro di me…"


translate italian day6_sl_7479b07e_1:


    me "Non lo so…"


translate italian day6_sl_c954d69f:


    "Slavya è scoppiata a ridere."


translate italian day6_sl_3d342d5a:


    me "Cosa c'è?!"


translate italian day6_sl_31892cbe:


    sl "Niente…{w} Mi sento come una principessa che è stata salvata dal principe azzurro, dal castello della strega cattiva."


translate italian day6_sl_8f838576:


    "L'ho guardata attentamente."


translate italian day6_sl_e89f228b:


    "Seppur tutto ciò fosse molto romantico, ho pensato che quello che aveva appena detto fosse una montagna di sciocchezze."


translate italian day6_sl_97cde972:


    th "Anche se, Olga Dmitrievna come strega cattiva…{w} Ha un certo senso!"


translate italian day6_sl_88e6b2de:


    me "Sì… L'analogia è quella giusta."


translate italian day6_sl_f51cd4bc:


    "L'ho detto scandendo ogni singola parola."


translate italian day6_sl_3915d229:


    sl "E le principesse solitamente non hanno alcuna possibilità di scelta in questi casi."


translate italian day6_sl_aabed66d:


    "Ha continuato, in tono criptico."


translate italian day6_sl_1cabe26b:


    th "Nessuna scelta?{w} Impossibile!"


translate italian day6_sl_e89a598d:


    th "Sono io colui che non ha alcuna scelta qui – stando seduto e aspettando di tornare miracolosamente alla mia realtà."


translate italian day6_sl_63017633:


    th "Slavya ha forse qualcosa in comune con me?{w} Lei è un pioniere modello, lavora duramente ed è bellissima."


translate italian day6_sl_c7da1f43:


    "Doveva avere centinaia se non migliaia di possibilità di scelta!"


translate italian day6_sl_db88a5ef:


    th "Probabilmente questo è il momento che ho sognato per tutta la mia vita – una ragazza così irraggiungibile come Slavya non solo sta seduta accanto a me, parlando e ridendo, ma dipende totalmente da me!"


translate italian day6_sl_a49c6922:


    th "Ma è un bene o un male?{w} E quale sarà la mia prossima mossa?"


translate italian day6_sl_18e9a332:


    "C'erano più domande che risposte."


translate italian day6_sl_9a4e7e0b:


    "Se è già abbastanza difficile per me prendere una decisione riguardante la mia vita, e assumermi la responsabilità delle mie azioni, come avrei potuto farlo per entrambi...?"


translate italian day6_sl_afb4e267:


    "Prima mi ero sempre tenuto alla larga da tali problemi, nella speranza che tutto si potesse risolvere da sé, senza bisogno del mio contributo."


translate italian day6_sl_3057669a:


    "Ma ora non potevo più agire in quel modo."


translate italian day6_sl_d1c57701:


    "E non solo per il fatto che mi trovavo chissà dove e in quale periodo."


translate italian day6_sl_2b586af9:


    me "Sai, forse mi sono sbagliato."


translate italian day6_sl_0fa153ce_1:


    "Slavya mi ha guardato con aria interrogativa."


translate italian day6_sl_3b122786:


    me "Non otterremo nulla stando seduti qui.{w} E non solo! È stupido e infantile – avevi ragione."


translate italian day6_sl_82f6ffb1:


    sl "E cosa dovremmo fare allora?"


translate italian day6_sl_b749d8c0_1:


    me "Andiamo!"


translate italian day6_sl_91946be4:


    "Ci siamo alzati. Ho preso il sacco a pelo e ci siamo diretti verso il Campo, tenendoci per mano."


translate italian day6_sl_98346aa3:


    "Ben presto siamo arrivati alla piazza, dove Olga Dmitrievna stava in piedi come se ci stesse aspettando."


translate italian day6_sl_57e9c38f:


    mt "Ma quanto tempo!"


translate italian day6_sl_7b0b0261:


    "Ci ha detto arrabbiata."


translate italian day6_sl_bee8d492:


    mt "Avete qualche giustificazione a riguardo?"


translate italian day6_sl_a0bfe3f6:


    "Ho risposto tranquillamente."


translate italian day6_sl_ccda0fcf:


    "Ho risposto con aria di sfida."


translate italian day6_sl_7f368bf8:


    mt "Oh, ma davvero?{w} Va bene allora. Quindi pensi che tutto questo sia normale? Benissimo…"


translate italian day6_sl_7ab0fbc2:


    "Si è fermata per un istante e si è rivolta a Slavya."


translate italian day6_sl_b7ba9ce3:


    mt "Torna alla tua stanza.{w} Ho già capito che non è colpa tua."


translate italian day6_sl_e290e93e:


    "Slavya non si è mossa di un millimetro. Si è limitata ad abbassare lo sguardo, stringendomi la mano ancora più forte."


translate italian day6_sl_1ed1a34c:


    mt "Slavya!"


translate italian day6_sl_da106b03:


    me "Come può vedere, lei non vuole."


translate italian day6_sl_c53ecc7f:


    mt "Cosa le hai detto per sedurla?"


translate italian day6_sl_e5c8bdf9:


    me "Oh mio Dio, non le ho detto niente!"


translate italian day6_sl_262d7cae:


    "Stavo perdendo la pazienza."


translate italian day6_sl_57062f56:


    me "Perché pensa che abbiamo fatto qualcosa di sbagliato?{w} Non abbiamo fatto niente! Affatto!"


translate italian day6_sl_9b7d56c6:


    mt "Come se non l'avessi visto!"


translate italian day6_sl_f0d6d1c8:


    "Ha sorriso."


translate italian day6_sl_dcebf1f8:


    me "Allora, cos'ha visto?"


translate italian day6_sl_a5f17bb1:


    "Ha fatto una pausa, ma ha continuato un secondo più tardi con lo stesso tono:"


translate italian day6_sl_17e9592b:


    mt "Ho visto abbastanza."


translate italian day6_sl_d3772757:


    me "Wow, è così brava a trarre conclusioni! Perché non chiede se le danno un lavoro nei servizi segreti?"


translate italian day6_sl_4e67b316:


    mt "Smettila con questa maleducazione!"


translate italian day6_sl_5f44af84:


    me "Non avevo nemmeno intenzione di iniziare."


translate italian day6_sl_d808c8a3:


    "Le ho detto con sarcasmo."


translate italian day6_sl_3e824ff1:


    mt "Va bene, il tuo tempo è scaduto e non sono disposta a continuare questa discussione inutile. Sei in punizione fino alla fine della sessione."


translate italian day6_sl_34787a27:


    me "Oh ma davvero? E come ha intenzione di impormelo?"


translate italian day6_sl_e8cb9456:


    mt "Ti chiuderò a chiave nella tua stanza!"


translate italian day6_sl_9f712cc6:


    me "E se mi opponessi?"


translate italian day6_sl_af66aeeb:


    "Olga Dmitrievna mi ha guardato sorpresa."


translate italian day6_sl_aafc257a:


    "È stato allora che mi sono accorto che una folla si stava radunando attorno a noi."


translate italian day6_sl_a198bf9e:


    "Potevo scorgere Lena, Alisa e Ulyana in mezzo alla folla."


translate italian day6_sl_46cac45a:


    mt "Cosa intendi dire con «opporti»?"


translate italian day6_sl_e7f3c77f:


    "Mi ha chiesto lentamente."


translate italian day6_sl_563e55f9:


    me "Intendo dire esattamente quello che ho detto."


translate italian day6_sl_3a35e669:


    mt "Ma non puoi! Un vero pioniere…"


translate italian day6_sl_2f21154b:


    me "Bene, allora significa che io sono uno falso!"


translate italian day6_sl_680ddca2:


    "Olga Dmitrievna ha fatto una breve pausa, come se stesse raccogliendo le sue forze."


translate italian day6_sl_08cd4a35:


    mt "Credi che io non possa costringerti?"


translate italian day6_sl_832f577a:


    me "E come pensa di riuscirci?"


translate italian day6_sl_30d5768e:


    "Ho riso con enfasi."


translate italian day6_sl_b2ab1d1c:


    th "Ignorando le varie forze mistiche che non sto tenendo in considerazione in questo momento, la leader del Campo effettivamente non ha proprio idea di come obbligarmi."


translate italian day6_sl_25d8873c:


    mt "Sai che ti dico?!"


translate italian day6_sl_ad02f53a:


    me "Cosa?"


translate italian day6_sl_c051b56f:


    "Sembrava che Olga Dmitrievna avesse già capito di aver perso, ma che ancora non potesse ammettere la sconfitta per non ferire il suo orgoglio e la sua posizione di «più-esperta-e-saggia-in-tutto-il-Campo»."


translate italian day6_sl_91d81026:


    mt "Io…{w} Io…{w} Manderò un rapporto alla tua scuola! Non sarai più accettato al Komsomol! Non sarai più in grado di iscriverti al Partito!"


translate italian day6_sl_4a0430d0:


    "Non ho avuto modo di oppormi dinanzi a tali gravi minacce."


translate italian day6_sl_32847c55:


    mt "Accettalo, non hai altra scelta in ogni caso!"


translate italian day6_sl_4f1d9777:


    "Slavya, che era rimasta in silenzio fino ad ora, è intervenuta improvvisamente nella discussione:"


translate italian day6_sl_0fbed59a:


    sl "Olga Dmitrievna, perché deve sempre pensare di avere ragione su tutto? Perché deve sempre dare la colpa a Semyon? Che cosa c'è di così grave in quello che è successo? Che cosa ha… cosa abbiamo fatto di male? Almeno è sicura di cosa ci sta accusando?"


translate italian day6_sl_63d41120:


    sl "E se fosse stato chiunque altro al mio posto, lei avrebbe reagito allo stesso modo?"


translate italian day6_sl_d6544d57:


    mt "Slavya, dovresti sapere che…"


translate italian day6_sl_133f5bcb:


    sl "È questo il punto, io so tutto! Io lo conosco, io so che Semyon non ha fatto nulla di male. So che lei ha dei pregiudizi nei suoi confronti. E comunque senza alcuna ragione evidente. Anche se lui cerca di fare del suo meglio!"


translate italian day6_sl_e25671be:


    sl "È vero, a volte sembra strano, ma so che ci mette tutto il suo cuore e la sua anima. E lei vorrebbe rimproverarlo per il fatto di essere sé stesso?"


translate italian day6_sl_9a7a49a4:


    "Ero affascinato dal monologo di Slavya, ascoltandola senza riuscire a capire se mi stesse elogiando, se stesse dichiarando i fatti com'erano, o se stesse esprimendo la sua opinione (che probabilmente era di parte)."


translate italian day6_sl_adaf79ec:


    "Era come se le sue parole fossero volate sopra alle mie orecchie."


translate italian day6_sl_c51c54a3:


    "Mi bastava sentire la melodia della sua voce – era ipnotizzante, e mi ha portato lontano da quella donna malata e da quello stupido campo di pionieri, mentre calmava e curava le ferite della mia anima."


translate italian day6_sl_90afe97b:


    mt "Va bene, non so più cosa fare con voi…"


translate italian day6_sl_a9bda318:


    "Olga Dmitrievna si è calmata un po'."


translate italian day6_sl_8d10714b:


    mt "Ma non pensate che io approvi il vostro comportamento!"


translate italian day6_sl_60c09bca:


    "Si è voltata, si è fatta strada tra la folla e si è diretta verso la propria casetta."


translate italian day6_sl_64e8ffbe:


    "Mi sarei aspettato una tempesta di applausi, come nei film americani, ma la folla ha iniziato a disperdersi rapidamente una volta che lo spettacolo è giunto al suo termine."


translate italian day6_sl_b164096b:


    "E Ulyana mi ha fatto l'occhiolino, sornione."


translate italian day6_sl_3e600926:


    sl "Beh, sai, abbiamo un po' esagerato."


translate italian day6_sl_2ffca3fd:


    "Ho guardato Slavya, stava sorridendo."


translate italian day6_sl_431c06db:


    me "Niente affatto!{w} Io avrei continuato a discutere con lei fino a tarda notte, e tu invece sei riuscita a farti valere in modo giusto e chiaro…{w} Non sarei mai riuscito a trovare parole giuste come le tue, nemmeno se avessi impiegato una settimana intera a preparare il discorso!"


translate italian day6_sl_a441fb1c:


    sl "Oh, e dai…"


translate italian day6_sl_d3368ac8:


    me "Cosa? È la pura verità, nessuna bugia né complimento!"


translate italian day6_sl_092ec533:


    "Mi ha guardato negli occhi ed è scoppiata a ridere."


translate italian day6_sl_28b09c2f:


    sl "Ho solo detto la prima cosa che mi è passata per la testa, è venuta fuori così."


translate italian day6_sl_2266dea3:


    me "Il primo pensiero è quello giusto – è così che si dice."


translate italian day6_sl_e330b23b:


    sl "Suppongo."


translate italian day6_sl_c13a9458:


    "Mi sono accorto solo ora che ci stavamo ancora tenendo per mano."


translate italian day6_sl_cc04a8ea:


    "Dopo essermi sentito un po' in imbarazzo, ho cercato di liberarmi dalla sua presa, ma Slavya non voleva lasciarmi.{w} Il che mi ha dato i brividi per un istante."


translate italian day6_sl_0b0dc7e3:


    sl "Dove andiamo?"


translate italian day6_sl_6ba416ef:


    "Non avevo intenzione di tornare nella mia stanza.{w} Più precisamente, non volevo proprio rimanere nel Campo."


translate italian day6_sl_e59ccd54:


    me "Cosa diresti se ti suggerissi di tornare nella foresta?"


translate italian day6_sl_21f85f08:


    "Slavya mi ha guardato sorpresa."


translate italian day6_sl_54f96f7f:


    me "Beh, sai, è solo che a volte…"


translate italian day6_sl_05b93dff:


    sl "Ok, andiamo!"


translate italian day6_sl_1d885fd1:


    "Non mi aspettavo una risposta così rapida e non avevo idea di come finire la frase."


translate italian day6_sl_d574deca:


    "Stavo per dirle di una sensazione che provo a volte – quando si vuole andare da qualche parte al di fuori della propria zona di conforto.{w} Almeno per un po', almeno per un paio d'ore."


translate italian day6_sl_59c2b914:


    "Lo facevo spesso in passato – visitando distretti sconosciuti o addirittura altre città nelle vicinanze."


translate italian day6_sl_abd50e2b:


    "Nuovi luoghi e scenari calmavano il mio spirito, e mi aiutavano a capire che il mondo non si limitava alle sole pareti del mio appartamento, o al percorso quotidiano verso l'università o verso il posto di lavoro."


translate italian day6_sl_e1e86b71:


    "E così vedevo che c'erano persone che stavano meglio o peggio di me."


translate italian day6_sl_0d93e329:


    "E quindi i miei problemi non erano più così gravi come sembravano."


translate italian day6_sl_b18f3f5e:


    "Improvvisamente ho sorriso."


translate italian day6_sl_e07c37b3:


    sl "Cosa c'è?"


translate italian day6_sl_41274eff:


    me "Niente, mi sono appena ricordato…{w} Non importa! Andiamo!"


translate italian day6_sl_3529c624:


    "Ci siamo diretti verso la foresta, sempre tenendoci per mano."


translate italian day6_sl_359460da:


    "Alla fine siamo arrivati ad una radura, probabilmente quella dove ci eravamo seduti un'ora fa. O forse era una diversa, dato che non riuscivo a distinguere l'una dall'altra."


translate italian day6_sl_817547ad:


    sl "Qui?"


translate italian day6_sl_0fa153ce_2:


    "Mi ha lanciato uno sguardo interrogativo."


translate italian day6_sl_e4bedbb9:


    me "No, andiamo più avanti."


translate italian day6_sl_af1d965e:


    sl "Ok."


translate italian day6_sl_5261c24b:


    "Dopo circa un'altra mezz'ora di vagabondaggio per la foresta, siamo giunti ad un piccolo lago."


translate italian day6_sl_66a1624c:


    me "Che bel posto!"


translate italian day6_sl_9bb4b00c:


    sl "Hai proprio ragione…"


translate italian day6_sl_3605a2b3:


    "Slavya ha fatto un sorriso enigmatico."


translate italian day6_sl_b45d6866:


    me "Allora suggerirei di fermarci qui!"


translate italian day6_sl_e54e1256:


    sl "È proprio un bel posto."


translate italian day6_sl_4f7b9290:


    "Ho appoggiato a terra il sacco a pelo e mi sono seduto su di esso."


translate italian day6_sl_ddd76d2d:


    "Slavya ha fatto lo stesso."


translate italian day6_sl_b7cba703:


    me "E adesso che facciamo?"


translate italian day6_sl_48ee8384:


    sl "Non lo so… Andare nella foresta è stata una tua idea…"


translate italian day6_sl_21a837aa:


    me "Sì, mia…"


translate italian day6_sl_2e80f38f:


    th "Devo prendere nuovamente una decisione.{w} Ma non ho proprio idea di cosa potremmo fare."


translate italian day6_sl_d3c3f841:


    th "Potremmo parlare, per esempio.{w} Ma di cosa...?"


translate italian day6_sl_428107ab:


    sl "Oh, andiamo, non essere così ansioso!"


translate italian day6_sl_c954d69f_1:


    "Slavya ha riso."


translate italian day6_sl_66f97779:


    me "Non lo sono…"


translate italian day6_sl_033b4661:


    sl "A me sembra di sì!"


translate italian day6_sl_a63fa39d:


    me "Beh, è solo perché…"


translate italian day6_sl_7bf46dbb:


    sl "È tutto a posto."


translate italian day6_sl_0fa7e23d:


    me "Se lo dici tu."


translate italian day6_sl_b9b338a4:


    "Ho sospirato, fissando il terreno."


translate italian day6_sl_9269f3f7:


    sl "Forse potresti parlarmi di te?"


translate italian day6_sl_59929532:


    me "Beh, non c'è molto da dire..."


translate italian day6_sl_ac0560c7:


    sl "È impossibile che non ci sia nulla di interessante nella vita di una persona!"


translate italian day6_sl_9eeceb0e:


    me "E chi te lo dice?{w} Io sono l'esempio vivente…"


translate italian day6_sl_dec3abfe:


    sl "Forse tu…"


translate italian day6_sl_60f3d758:


    "Ha esitato."


translate italian day6_sl_8e71c1af:


    sl "Forse non mi vuoi dire qualcosa?"


translate italian day6_sl_8b677452:


    me "Ad esempio cosa?"


translate italian day6_sl_762cefe1:


    sl "Non lo so…{w} Forse mi stai nascondendo qualcosa?"


translate italian day6_sl_869dccdf:


    me "Perché dovrei?"


translate italian day6_sl_446d678d:


    sl "Come faccio a saperlo? Dovresti saperlo meglio di me."


translate italian day6_sl_7dff98c4:


    me "Beh…{w} Non lo so…"


translate italian day6_sl_3c9fa231:


    sl "L'hai detto come se…"


translate italian day6_sl_d2b04ecc:


    me "Come?"


translate italian day6_sl_94474f8e:


    sl "Nel modo sbagliato!"


translate italian day6_sl_f7fcc72d:


    "Slavya si è messa a ridere nuovamente."


translate italian day6_sl_ee707346:


    "Sono sicuro che nessuno a parte lei si sarebbe sentito così tranquillo qui."


translate italian day6_sl_2d42f5c8:


    me "Va bene, quindi che cosa vorresti sapere?"


translate italian day6_sl_50dc5f53:


    sl "Beh, parlami di te. Qualcosa di interessante che è successo nella tua vita!"


translate italian day6_sl_e9233663:


    "In quel momento ero assolutamente certo che non ci fosse mai stato nulla di interessante nella mia vita."


translate italian day6_sl_dfb2a4e4:


    me "Ok, fammi pensare…"


translate italian day6_sl_8a84040d:


    "Ho fatto una faccia furba."


translate italian day6_sl_40e09782:


    me "Quando ero bambino – a circa cinque o sei anni – c'era una piccola capanna vicino alla casa di campagna della mia famiglia."


translate italian day6_sl_6642f6b7:


    me "Era abbastanza robusta, costruita con tavole di legno, il tetto in carta catramata, per poter essere utilizzata come rifugio antiaereo in caso di guerra."


translate italian day6_sl_23e95695:


    me "Così un giorno stavo seduto sul tetto di quella capanna con qualcuno e…{w} improvvisamente sono caduto giù."


translate italian day6_sl_4ea8163e:


    me "E dietro ad essa c'era un fragoleto."


translate italian day6_sl_78f3b806:


    me "E durante quei secondi mentre stavo cadendo, ho pensato – è finita, il Cupo Mietitore è venuto a prendermi, e tutta la mia vita mi è scorsa davanti. Una vita lunga appena sei anni, ma comunque…"


translate italian day6_sl_a269facd:


    me "Così sono caduto sopra quel fragoleto e, ovviamente, non mi sono fatto niente, dato che era piuttosto morbido."


translate italian day6_sl_b0563737:


    me "Ed ecco la storia…"


translate italian day6_sl_c954d69f_2:


    "Slavya si è messa a ridere."


translate italian day6_sl_0ea44879:


    sl "Ma è così sciocco!"


translate italian day6_sl_1c6992b3:


    me "Per me non lo era, in quel momento."


translate italian day6_sl_320e2edd:


    sl "E invece adesso come ti sembra?"


translate italian day6_sl_851543b3:


    me "Non lo so…{w} Immagino sia sciocco…"


translate italian day6_sl_3aa2017b:


    sl "Bene.{w} Raccontami qualcos'altro!"


translate italian day6_sl_26ec147e:


    me "Lo considereresti sciocco allo stesso modo…"


translate italian day6_sl_e1a615ef:


    sl "No, non lo farò!"


translate italian day6_sl_1cc1d82f:


    me "Eh…"


translate italian day6_sl_384065ad:


    "Ho pensato di nuovo."


translate italian day6_sl_d9e5ad4e:


    me "Una volta sono anche caduto in uno stagno, da un ponte, e stavo quasi per annegare."


translate italian day6_sl_745d222d:


    sl "Le tue storie sono tutte…"


translate italian day6_sl_75767d26:


    "Ha detto in tono annoiato."


translate italian day6_sl_c0ec9074:


    me "Te l'ho detto – non c'è molto da dire, quindi ti ho raccontato la prima storia che mi è venuta in mente."


translate italian day6_sl_3eb56ac0:


    sl "Ah, a proposito, il lago!"


translate italian day6_sl_b0c344be:


    "Slavya ha guardato l'acqua."


translate italian day6_sl_3049af19:


    sl "Andiamo a nuotare."


translate italian day6_sl_fcde635e:


    me "Non sono molto bravo a nuotare.{w} E poi non ho niente da mettermi addosso."


translate italian day6_sl_81cd01eb:


    sl "Oh, andiamo!"


translate italian day6_sl_6db70b0c:


    "Mi ha fatto un sorriso accattivante."


translate italian day6_sl_e8fc1211:


    me "Beh, non saprei..."


translate italian day6_sl_b7400b08:


    "Naturalmente volevo accettare, ma ho comunque esitato."


translate italian day6_sl_72436a13:


    sl "Dai!"


translate italian day6_sl_41c34d45:


    "Slavya è corsa verso il lago ridendo, sbottonandosi la camicia durante il tragitto."


translate italian day6_sl_a62eafb3:


    me "Beh… Hmm…"


translate italian day6_sl_5b6dae07:


    "Mi sono alzato dal sacco a pelo e l'ho seguita lentamente."


translate italian day6_sl_e09166ac:


    "Quando sono arrivato Slavya stava già schizzando nell'acqua, e tutti i suoi vestiti erano sparsi in giro."


translate italian day6_sl_87701f3c:


    sl "Vieni qui!"


translate italian day6_sl_7e260440:


    me "Beh, io non…{w} Non credo sia una buona idea…"


translate italian day6_sl_5eca3ce7:


    "Ho borbottato lentamente."


translate italian day6_sl_12241ecd:


    sl "Avanti! È divertente qui!"


translate italian day6_sl_6217020f:


    "Non sembrava per nulla imbarazzata della mia presenza."


translate italian day6_sl_d8852a28:


    "Mentre io ero il primo a voler distogliere lo sguardo."


translate italian day6_sl_6bf6e07b:


    th "Ma ecco il dilemma – cosa sarebbe più appropriato?{w} Se per Slavya è tutto a posto e io mi mostrerò imbarazzato – potrebbe forse offendersi?"


translate italian day6_sl_a7e3e304:


    th "Ma quella non è la cosa più importante ora!"


translate italian day6_sl_4fe83cce:


    me "Non ho portato il mio costume da bagno..."


translate italian day6_sl_96942526:


    "Credo che non fosse la scusa più adatta in quel momento."


translate italian day6_sl_cbfa9c42:


    sl "E allora? Nemmeno io..."


translate italian day6_sl_475bb393:


    "Ha detto Slavya, con voce un po' più bassa."


translate italian day6_sl_8160a6c3:


    sl "Non aver paura."


translate italian day6_sl_8cac9455:


    me "Non ho paura! Non c'è niente di cui aver paura..."


translate italian day6_sl_6dfc8e2e:


    sl "Allora vieni qui."


translate italian day6_sl_3f27c2c0:


    "Non sapevo cosa dire."


translate italian day6_sl_1a6a0ccb:


    "Probabilmente avrei voluto accondiscendere – ma in modo completamente diverso.{w} Non in maniera così spontanea e disinvolta."


translate italian day6_sl_3ef72e17:


    th "E comunque – perché Slavya è così {i}amichevole{/i} nei miei confronti?{w} O forse si tratta di qualcosa di più?{w} Allora io davvero non capisco!"


translate italian day6_sl_d88264e4:


    sl "Sta a te decidere!"


translate italian day6_sl_1e8379d1:


    "Ha detto in tono deluso."


translate italian day6_sl_7cd8cd17:


    sl "Se proprio non vuoi, almeno accendi il fuoco."


translate italian day6_sl_7332342b:


    me "Per quale motivo?"


translate italian day6_sl_936f6483:


    sl "Perché sì!{w} Quando uscirò dall'acqua – avrò bisogno di asciugarmi in qualche modo!"


translate italian day6_sl_6829db90:


    me "Oh, sì… Va bene…"


translate italian day6_sl_d608686c:


    "Mi sono diretto rapidamente verso il sacco a pelo, raccogliendo dei ramoscelli durante il tragitto."


translate italian day6_sl_fb820884:


    "Raccogliere la legna non era affatto un problema, ma come sarei riuscito ad accendere il fuoco…?"


translate italian day6_sl_c0609f2f:


    "Non avevo né una scatola di fiammiferi, né un accendino."


translate italian day6_sl_eee42951:


    "Come ogni persona istruita, sapevo che è possibile farlo mediante l'attrito di un legno contro un altro, ma non l'avevo mai fatto prima.{w} Beh, non avevo altre idee…"


translate italian day6_sl_e5ff5614:


    "Ho iniziato a sfregare un legno contro l'altro, lentamente."


translate italian day6_sl_2723a1cb:


    "Fortunatamente i pezzi di legno erano abbastanza asciutti, perché altrimenti non avrei avuto alcuna possibilità."


translate italian day6_sl_27361633:


    "Francamente ero abbastanza scettico riguardo alle mie possibilità, ma dopo appena un minuto è apparso un po' di fumo, e si è accesa una debole fiamma, scomparendo un istante più tardi."


translate italian day6_sl_f5d403da:


    "Almeno era chiaro che sarebbe stato possibile accendere il fuoco in quel modo."


translate italian day6_sl_75c51157:


    "Mi ci sono voluti altri venti minuti di fatica e sofferenze, ma alla fine sono riuscito ad ottenere un piccolo fuoco che ardeva accanto al sacco a pelo."


translate italian day6_sl_b893bafe:


    "Stavo gettando pigramente dei rami su di esso per alimentarlo."


translate italian day6_sl_b50189b8:


    "Succede così – un essere umano è in grado di adattarsi ad ogni situazione, e così probabilmente riuscirebbe a sopravvivere persino in una foresta innevata."


translate italian day6_sl_2545e174:


    "I miei pensieri di sopravvivenza sono stati interrotti da Slavya, che improvvisamente è apparsa proprio sopra di me."


translate italian day6_sl_14c08f52:


    sl "Wow! Ci sei riuscito!"


translate italian day6_sl_ad95378a:


    "Ho alzato lo sguardo e poi l'ho distolto immediatamente."


translate italian day6_sl_190c6df5:


    "Aveva indosso una camicia, ma era tutta inzuppata, e non riusciva a nascondere molto."


translate italian day6_sl_79ca2420:


    "Non è stato un problema finché si trovava nell'acqua, ma averla lì vicino era totalmente diverso."


translate italian day6_sl_9fe63324:


    sl "Nessuno si potrebbe perdere assieme a te."


translate italian day6_sl_e7783d47:


    "Slavya si è seduta, appoggiando la sua schiena alla mia."


translate italian day6_sl_12a92a61:


    sl "Spero di riuscire ad asciugarmi presto."


translate italian day6_sl_4baac495:


    "Per qualche motivo lo speravo anch'io."


translate italian day6_sl_4166759d:


    sl "Allora?"


translate italian day6_sl_ad02f53a_1:


    me "Cosa?"


translate italian day6_sl_ce38d8f8:


    "Ho chiesto in tono confuso."


translate italian day6_sl_d43342bf:


    sl "Dimmi qualcos'altro!"


translate italian day6_sl_77005469:


    me "Non so davvero…"


translate italian day6_sl_f574621f:


    "Era come se qualcosa mi si fosse bloccato in gola – la narrazione era decisamente fuori questione."


translate italian day6_sl_a1ced71a:


    sl "Improvvisamente sei diventato così teso."


translate italian day6_sl_d82bbbfe:


    th "Davvero, perché dovrei esserlo?"


translate italian day6_sl_09834bc7:


    me "Non è niente, è solo che…"


translate italian day6_sl_64fb8da4:


    "Non riuscivo più a starmene seduto lì così."


translate italian day6_sl_10e70ee5:


    "Gli istinti animali mi stavano annebbiando la mente."


translate italian day6_sl_e909c247:


    me "Pensi che…{w} sia normale?"


translate italian day6_sl_e07c37b3_1:


    sl "Che cosa?"


translate italian day6_sl_877d42ed:


    me "Stare seduti così?"


translate italian day6_sl_0da077c9:


    sl "Cosa c'è che non va?"


translate italian day6_sl_b060c097:


    "Sembrava che Slavya proprio non capisse…"


translate italian day6_sl_7d63ae9b:


    me "Beh, tu sei…{w} Davvero non capisci…?"


translate italian day6_sl_1fd5a2a5:


    "Ho sospirato impotente."


translate italian day6_sl_86c2396f:


    sl "Beh… Non ne sono sicura…"


translate italian day6_sl_2295ea06:


    "Ha detto con malizia."


translate italian day6_sl_ba65d0ea:


    sl "Cosa ne pensi?"


translate italian day6_sl_a15c0346:


    me "Beh, io…{w} Io…"


translate italian day6_sl_75404f9d:


    sl "Forse non dovremmo trattenerci?"


translate italian day6_sl_b31684a0:


    "Le sue parole mi hanno attraversato la testa come un lampo."


translate italian day6_sl_69908825:


    th "È consenziente!"


translate italian day6_sl_618577b1:


    "Qualcosa di meraviglioso è sbocciato tra di noi, qualcosa che non riuscivo nemmeno a immaginare sei giorni fa."


translate italian day6_sl_f6734dd5:


    "Forse eccola qui – la ragione per cui mi trovavo in questo Campo?"


translate italian day6_sl_64e5abef:


    sl "È stato meraviglioso!"


translate italian day6_sl_9513cd87:


    me "Sì…"


translate italian day6_sl_a2d3eaed:


    "Per un po' ci siamo limitati a restare vicini."


translate italian day6_sl_add6d7aa:


    sl "Si sta facendo freddo."


translate italian day6_sl_62823e20:


    me "Un attimo."


translate italian day6_sl_3a7548c7:


    "Ho aperto il sacco a pelo e siamo entrati insieme."


translate italian day6_sl_d5fe4f59:


    "Slavya ha appoggiato la sua testa sulla mia spalla."


translate italian day6_sl_0cc68dfe:


    sl "Ti spiace se mi faccio un sonnellino? Mi sento così stanca..."


translate italian day6_sl_3b03af3a:


    me "Certo, fai pure…"


translate italian day6_sl_755c0af5:


    "Ben presto il suo respiro è diventato profondo e costante."


translate italian day6_sl_3634f3f3:


    "Il vento scuoteva lentamente le foglie degli alberi attorno a noi, l'erba ondeggiava al nostro fianco, e i minuscoli moscerini ronzavano fastidiosamente sopra le nostre teste."


translate italian day6_sl_0ece9306:


    "Improvvisamente mi sono reso conto che questo era il posto più bello che avessi mai visto in vita mia."


translate italian day6_sl_b249ce58:


    "Pensando a ciò, mi sono addormentato."


translate italian day6_sl_ce617998:


    "…"


translate italian day6_sl_a20cefa7_2:


    "..."


translate italian day6_sl_dcbe279a:


    "Ho aperto gli occhi e ho scorto la parte superiore di una quercia ruggire al di sopra di me."


translate italian day6_sl_4ec20237:


    "Un forte vento si è alzato, facendomi rabbrividire fino all'osso."


translate italian day6_sl_35f2530a:


    "La luna piena mi stava fissando – era calata la notte."


translate italian day6_sl_b7d4386e:


    "Non sapevo esattamente che ora fosse, ma non aveva molta importanza."


translate italian day6_sl_41242fed:


    "Slavya stava ancora dormendo."


translate italian day6_sl_3df19969:


    "L'ho avvolta delicatamente nel sacco a pelo, mi sono vestito e mi sono diretto verso il lago."


translate italian day6_sl_5666a2a3:


    "Avevo molta sete e non c'erano altre opzioni disponibili."


translate italian day6_sl_7a95c9e6:


    th "Inoltre, non credo che l'acqua qui sia avvelenata."


translate italian day6_sl_1829982a:


    th "Non siamo vicino a Chernobyl o simili."


translate italian day6_sl_fab822c4:


    th "O almeno lo spero."


translate italian day6_sl_45db8adc:


    "Ho raccolto un po' d'acqua con le mani e me le sono portate con entusiasmo davanti alla bocca."


translate italian day6_sl_c60407cd:


    "L'acqua era così fresca che sembrava provenire da una sorgente, e non da un lago."


translate italian day6_sl_1f3eb0be:


    "Il mio riflesso mi fissava, offuscato dalla superficie dell'acqua e sfiorato dal chiaro di luna."


translate italian day6_sl_023f70e4:


    "E sembrava sorridere."


translate italian day6_sl_342dc487:


    th "Strano, non l'avevo nemmeno notato…"


translate italian day6_sl_0a38142d:


    "Rallegrato, sono tornato indietro, con l'intento di dormire fino al mattino..."


translate italian day6_sl_855eb22e:


    "... quando improvvisamente ho sentito un ramo spezzarsi nelle vicinanze."


translate italian day6_sl_f17137d4:


    "Mi sono voltato per vedere chi fosse e ho scorto qualcuno dietro ad un albero."


translate italian day6_sl_1d2d3e1b:


    me "Chi va là?"


translate italian day6_sl_323cb2fa:


    "Ho gridato, preoccupato."


translate italian day6_sl_3a866645:


    FIXME_voice "Non c'è bisogno di urlare in quel modo."


translate italian day6_sl_c555bb93:


    "La voce sembrava familiare."


translate italian day6_sl_7ccaacb2:


    "Mi sono avvicinato e ho visto Lena."


translate italian day6_sl_2821e6eb:


    me "Che ci fai qui?"


translate italian day6_sl_8543722f:


    un "Sto facendo una passeggiata."


translate italian day6_sl_2864c058:


    me "A quest'ora?!"


translate italian day6_sl_920912b6:


    un "Perché, non posso?"


translate italian day6_sl_b9c06d63:


    me "No, è solo che è un po' strano."


translate italian day6_sl_28dbd9ca:


    "Mi sono appoggiato ad un albero, meditando."


translate italian day6_sl_2d954428:


    th "Lei è sicuramente qui per un motivo."


translate italian day6_sl_a5f26668:


    th "Inoltre, abbiamo faticato parecchio per arrivare in questo luogo. Non basta una semplice «passeggiata» per raggiungerlo!"


translate italian day6_sl_9c114242:


    th "E anche di notte, Slavya che dorme a una decina di passi di distanza è chiaramente visibile..."


translate italian day6_sl_f95c9fc9:


    me "E?"


translate italian day6_sl_3875ba7b:


    un "Che cosa?"


translate italian day6_sl_35f65f56:


    me "Stavi per dire qualcosa?"


translate italian day6_sl_38f1c7fb:


    un "Dovrei?"


translate italian day6_sl_87e50db3:


    me "Dipende da te."


translate italian day6_sl_4b0db65e:


    un "Non capisco di cosa tu stia parlando."


translate italian day6_sl_ffb6e8f5:


    me "Va bene…{w} Sei rimasta qui a lungo?"


translate italian day6_sl_4ae58fde:


    un "Beh, non così a lungo…"


translate italian day6_sl_8b16db24:


    me "E per quanto?"


translate italian day6_sl_dc72cd9c:


    un "Non molto!"


translate italian day6_sl_2406c221:


    "Ha tagliato corto."


translate italian day6_sl_93ecd6c4:


    me "Va bene…{w} Tutto qui?"


translate italian day6_sl_0da946d4:


    un "Non so."


translate italian day6_sl_e7e5f09c:


    "Siamo rimasti in silenzio ancora per un po'."


translate italian day6_sl_1350bc39:


    un "Forse è meglio se tornate indietro. Slavya potrebbe prendersi un raffreddore."


translate italian day6_sl_65796b62:


    me "E così hai visto tutto?"


translate italian day6_sl_2fe9320f:


    un "Sarebbe stato difficile non vederlo."


translate italian day6_sl_bd831b71:


    me "Se pensi che…"


translate italian day6_sl_17b466c5:


    un "Beh, cosa dovrei pensare, eh?{w} È tutto chiaro come il sole."


translate italian day6_sl_8c370f90:


    me "Suppongo…{w} Quindi non dirai niente a nessuno?"


translate italian day6_sl_1ab636d8:


    un "Cosa c'è da dire?{w} Dopo il vostro spettacolo oggi alla piazza, non serve aggiungere altro."


translate italian day6_sl_d7ec763f:


    me "Non era uno spettacolo…{w} Abbiamo solo…"


translate italian day6_sl_e767eb14:


    th "È come se io mi stessi giustificando dinanzi a lei."


translate italian day6_sl_5e5d8d97:


    me "E comunque non sono affari tuoi!"


translate italian day6_sl_80519381:


    un "Esattamente.{w} Sono affari vostri.{w} Ma stai attento. Fa' attenzione."


translate italian day6_sl_1084810b:


    me "Di cosa stai parlando?"


translate italian day6_sl_f1725204:


    un "Se tutto ti viene così spontaneo, probabilmente non lo stai apprezzando appieno.{w} Anche se dovresti."


translate italian day6_sl_787e2d21:


    me "Non ti capisco."


translate italian day6_sl_30f3651c:


    un "Beh, come Slavya. Bam, la prima volta è magico.{w} E allora si fa un altro tentativo, e poi un altro ancora, e alla fine ci si stanca.{w} Forse."


translate italian day6_sl_f14d3856:


    "Lena si è interrotta."


translate italian day6_sl_9e6298f4:


    un "È questo che voglio dire – devi apprezzare quello che hai, anche se non ti costa molto sforzo per ottenerlo."


translate italian day6_sl_6d7c4722:


    me "Se pensi che io consideri tutto questo come un gioco, ti sbagli di grosso."


translate italian day6_sl_44fccd69:


    un "Non lo penso.{w} Ma è possibile."


translate italian day6_sl_2b60d758:


    me "E comunque!{w} Che ne dici di restare fuori dalle cose che non ti riguardano?"


translate italian day6_sl_356d8152:


    un "Io ti sto solo dando dei consigli."


translate italian day6_sl_94c6774e:


    me "Non mi sembra di averteli chiesti."


translate italian day6_sl_7967d505:


    un "Penso che dovresti prestare maggiore attenzione a quello che fai o che stai progettando di fare, così da non rischiare di avere dolorosi rimpianti in futuro."


translate italian day6_sl_54eac6ad:


    me "Senti, tutto questo sta iniziando a suonare come una minaccia!"


translate italian day6_sl_12de8fbf:


    th "Eh, sembra proprio di sì…"


translate italian day6_sl_5361e5b6:


    un "Beh, forse è così…"


translate italian day6_sl_71411d8c:


    "Mi ha detto in modo misterioso."


translate italian day6_sl_766131be:


    me "Quindi tu…"


translate italian day6_sl_cad298e3:


    un "No, per niente…"


translate italian day6_sl_d8b4d5a6:


    un "Almeno non per ora."


translate italian day6_sl_7dd3d65d:


    "Ha aggiunto in un sussurro."


translate italian day6_sl_51cf710a:


    "Lena si è girata e si è incamminata nelle profondità della foresta."


translate italian day6_sl_5f2d5d23:


    "Non ho provato a fermarla."


translate italian day6_sl_a4493a46:


    "Quando era quasi scomparsa dalla mia vista, ho notato che teneva qualcosa in mano, che brillava al chiaro di luna."


translate italian day6_sl_77992443:


    th "Che ragazza strana…"


translate italian day6_sl_4850ebe2:


    "Ho sospirato e sono tornato da Slavya."


translate italian day6_sl_bd5a6473:


    "Il calore del suo corpo mi ha aiutato a sconfiggere il freddo notturno."


translate italian day6_sl_8fbd4ba7:


    "Speravo che anche Slavya provasse lo stesso nei miei confronti…"


translate italian day6_dv_c15cbee2:


    "Mi sono svegliato presto, quando Olga Dmitrievna stava ancora dormendo. Mi sono stirato e mi sono seduto sul letto."


translate italian day6_dv_4f2d9d90:


    "Mi sentivo sorprendentemente bene, anche se sarebbe stato meglio dormire per l'intero giorno, dopo tutto quello che era successo la notte scorsa."


translate italian day6_dv_2702e092:


    "Silenziosamente, in modo da non svegliare la leader del Campo, ho preso la borsa con le mie cose da bagno e sono uscito."


translate italian day6_dv_0cbfa63c:


    "Mi sono dimenticato di guardare l'ora, ma sembravano le sei o le sette di mattina."


translate italian day6_dv_1e1204ff:


    "L'intero Campo stava ancora dormendo."


translate italian day6_dv_1857932a:


    "L'erba si stava scrollando di dosso la rugiada notturna, una leggera nebbia aleggiava sul terreno, e il sole stava iniziando a spuntare dalle cime degli alberi, ancora non riscaldato del tutto, un po' come la lampada fluorescente nella casa di fronte alla mia."


translate italian day6_dv_62f242c5:


    "Ho fatto un respiro profondo, inalando una boccata d'aria fresca, pensando che persino un nottambulo come me poteva godersi ogni tanto i piaceri del primo mattino."


translate italian day6_dv_1fc263e9:


    "Le «strade» del Campo erano deserte, ovviamente."


translate italian day6_dv_6b08ba9d:


    "L'acqua qui è sempre stata fredda, ma oggi sembrava proprio che fosse arrivata direttamente dalle profondità antartiche.{w} Persino le mie mani si sono intorpidite a causa di essa…"


translate italian day6_dv_07d87721:


    "Dopo aver finito di occuparmi della mia igiene, ho deciso di passeggiare per il Campo per un po'."


translate italian day6_dv_c8815fd4:


    th "Olga Dmitrievna sta ancora dormendo, manca ancora un bel po' alla colazione, e quindi non ho niente da fare…"


translate italian day6_dv_e44d834c:


    "Sono arrivato alla piazza e ho guardato Genda."


translate italian day6_dv_37cdd1ec:


    th "A quel tizio non importa che sia giorno o notte. È sempre vigile!"


translate italian day6_dv_4b83221f:


    "La mia brillante battuta mi ha fatto sorridere, e mi sono diretto verso la rimessa per le barche."


translate italian day6_dv_714f4f97:


    "Mentre camminavo ho canticchiato una canzone, del tutto convinto di essere l'unico nei paraggi."


translate italian day6_dv_24dad558:


    "Con mia grande sorpresa ho notato qualcuno seduto sul molo, con i piedi a penzoloni nell'acqua."


translate italian day6_dv_b284c8b7:


    "Alisa."


translate italian day6_dv_b3358970:


    "Mi sono avvicinato e poi mi sono fermato, non sapendo cosa dirle."


translate italian day6_dv_31a54a49:


    "Poi improvvisamente si è voltata e mi ha fissato."


translate italian day6_dv_39426f9a:


    dv "È da tanto che sei qui?"


translate italian day6_dv_5cb822ee:


    me "No…"


translate italian day6_dv_6373b237:


    dv "Perché non stai dormendo?"


translate italian day6_dv_b78c6e26:


    me "E tu perché?"


translate italian day6_dv_bc3de2ca:


    "Mi ha dato nuovamente le spalle, fissando il fiume."


translate italian day6_dv_48bb9e3b:


    th "Sembra che io non sia l'unico di buonumore oggi."


translate italian day6_dv_ed9a68b7:


    "Almeno non si comportava in modo impertinente come al solito."


translate italian day6_dv_186c0eae:


    dv "Non riuscivo a dormire."


translate italian day6_dv_f2e007f0:


    me "Capisco…"


translate italian day6_dv_c203a783:


    dv "A proposito di ieri…{w} Ti è tutto chiaro, vero?"


translate italian day6_dv_ae56873f:


    me "Che cosa esattamente?"


translate italian day6_dv_5a2cc2c6:


    dv "Oh, per l'amor di Dio!"


translate italian day6_dv_0239f53c:


    th "Non sembra aver preso bene la mia risposta."


translate italian day6_dv_f6787280:


    me "D'accordo, sì, ho capito tutto…"


translate italian day6_dv_bc2b8f99:


    "Ho mentito."


translate italian day6_dv_db8773f6:


    dv "Ok allora… Bene…"


translate italian day6_dv_c8722ed0:


    "Improvvisamente mi ha guardato dritto negli occhi e ha sorriso."


translate italian day6_dv_114bfae2:


    dv "In tal caso ho dei piani per te stasera!"


translate italian day6_dv_0b273a12:


    me "Eh?{w} Che tipo di piani?"


translate italian day6_dv_d0e6c06e:


    dv "Non è necessario che tu lo sappia!"


translate italian day6_dv_0ba7958b:


    me "Beh, dato che riguardano me…"


translate italian day6_dv_0ac4aace:


    dv "Lo saprai quando sarà il momento."


translate italian day6_dv_67da3356:


    me "Potresti darmi almeno un indizio?"


translate italian day6_dv_9f6452c6:


    dv "No!"


translate italian day6_dv_58acf86d:


    "Si è alzata e si è avvicinata a me."


translate italian day6_dv_7c3046d7:


    "Mi sono scansato, un po' impaurito."


translate italian day6_dv_225e8e60:


    dv "Non fare niente di stupido!"


translate italian day6_dv_3af4c20c:


    "Mi ha detto Alisa, allontanandosi dal molo."


translate italian day6_dv_f1425e57:


    th "Mi chiedo cosa intendesse dire.{w} E quali sarebbero questi piani...?"


translate italian day6_dv_544d6ac4:


    "Non avevo intenzione di essere coinvolto in qualche atto terrorista nei confronti di una statua, o cose del genere…"


translate italian day6_dv_9ede4c1b:


    "Inoltre, ero ancora ossessionato da quella discussione con Lena ieri sull'isola."


translate italian day6_dv_8241f1ad:


    th "Lei probabilmente è ancora arrabbiata…{w} E non sono riuscito a spiegarle tutto come volevo."


translate italian day6_dv_4184783d:


    th "Anche se non saprei neppure cosa dirle se ne avessi l'occasione."


translate italian day6_dv_0566e7d0:


    "Mi sentivo in colpa per tutto quello che era successo, ma non riuscivo a capire di cosa esattamente fossi colpevole."


translate italian day6_dv_7ea39484:


    "Il Campo stava cominciando a svegliarsi..."


translate italian day6_dv_9af1e38e:


    "Mancava ancora almeno un'ora alla colazione, ma non avevo voglia di tornare alla stanza della leader del Campo, così ho deciso di aspettare nei pressi della mensa."


translate italian day6_dv_c58fe4ea:


    th "Almeno sarò il primo ad entrare, per una volta!"


translate italian day6_dv_25d2dc92:


    "Mi sono seduto sui gradini e ho guardato il sorgere del sole."


translate italian day6_dv_75d19de5:


    "È bello starsene lì ad osservare l'alba di un nuovo giorno."


translate italian day6_dv_cd46fe4c:


    "Ero immerso in una tale pace e tranquillità che non c'era spazio per pensieri negativi nella mia mente, come invece accadeva spesso."


translate italian day6_dv_743da966:


    "Mi stavo semplicemente facendo il bagno immerso nei caldi raggi della stella celeste."


translate italian day6_dv_cfe6a6d0:


    "In qualsiasi altro luogo o circostanza, probabilmente avrei potuto persino affermare di essere felice."


translate italian day6_dv_5492e5b8:


    un "Buongiorno! Sei in anticipo oggi…"


translate italian day6_dv_3edc6eb9:


    "Ho alzato lo sguardo e ho visto Lena in piedi di fronte a me.{w} Stava sorridendo."


translate italian day6_dv_0c9902bf:


    me "Anche tu…"


translate italian day6_dv_f6ff66c7:


    "Tutti gli eventi della notte scorsa mi sono balenati davanti agli occhi."


translate italian day6_dv_17717ee8:


    me "Senti…"


translate italian day6_dv_5aa4cd1c:


    un "Se si tratta di ieri, lascia perdere!"


translate italian day6_dv_cd5b1943:


    "Mi ha interrotto bruscamente."


translate italian day6_dv_54f0d7cf:


    un "Non so cosa mi è preso…{w} È stato tutto…{w} Dimenticalo e basta!"


translate italian day6_dv_8fb93608:


    "Mi ha fissato."


translate italian day6_dv_b9aa504e:


    "Lena sembrava molto più sicura del solito."


translate italian day6_dv_0b610250:


    me "Se solo fosse così semplice…"


translate italian day6_dv_96b90470:


    un "Non c'è nulla di complicato in questo!"


translate italian day6_dv_1a20f786:


    "Si è seduta vicino a me."


translate italian day6_dv_6fd2c686:


    "Così vicino che mi sono sentito un po' in imbarazzo, ma non ho cercato di allontanarmi da lei."


translate italian day6_dv_b6d571c6:


    un "Certe volte io e Alisa litighiamo."


translate italian day6_dv_77ff46df:


    th "Già, me ne sono accorto."


translate italian day6_dv_025de025:


    me "Capisco…{w} Ma in questo caso, è stata colpa mia."


translate italian day6_dv_a9702184:


    un "Chi lo sa…"


translate italian day6_dv_069a2ec8:


    "Ha risposto, persa nei suoi pensieri."


translate italian day6_dv_a4255212:


    me "Ed ecco perché…{w} Sai…{w} Mi sento a disagio."


translate italian day6_dv_99d1db6f:


    me "Forse è davvero mia la colpa…{w} O meglio, non sto cercando di dire che io non sia da biasimare, ma…"


translate italian day6_dv_5d2a7a44:


    "Sembravo un bambino della quinta elementare che si stava giustificando davanti alla maestra."


translate italian day6_dv_b0c06763:


    un "Va bene, non pensarci!{w} Come ti ho già detto, non è successo niente di grave!"


translate italian day6_dv_bd04f9b5:


    me "Se lo dici tu…"


translate italian day6_dv_bd742d8d:


    un "Adesso devi solo pensare a come comportarti d'ora in avanti…"


translate italian day6_dv_dc477843:


    me "Ci risiamo!"


translate italian day6_dv_9b902801:


    "Mi sono coperto il viso con le mani, mostrando chiaramente che non avevo assolutamente idea di cosa stesse succedendo, e che tutto era, per usare un eufemismo, molto spiacevole per me."


translate italian day6_dv_bc39bcd1:


    me "Potresti esprimerti più chiaramente?"


translate italian day6_dv_41cfffd5:


    un "E perché?"


translate italian day6_dv_67a6d56e:


    "Ha chiesto evasivamente."


translate italian day6_dv_ebf71d2d:


    me "Senti, se non so che cosa ho fatto di sbagliato, allora non posso fare di meglio!{w} Non posso sapere come «comportarmi» come dici tu."


translate italian day6_dv_f2f9031f:


    un "Credo che lo capirai…"


translate italian day6_dv_86da72f4:


    me "Anche a me piacerebbe crederci, ma…"


translate italian day6_dv_27475010:


    "La porta della mensa si è spalancata all'improvviso, e Olga Dmitrievna è venuta fuori."


translate italian day6_dv_03906774:


    mt "Avete già fame di prima mattina?"


translate italian day6_dv_759d5254:


    "Ha chiesto scherzosamente."


translate italian day6_dv_5567bace:


    un "Buongiorno!"


translate italian day6_dv_4fb197ff:


    "Ha detto allegramente Lena. Si è alzata e si è diretta all'interno della mensa."


translate italian day6_dv_35698e3a:


    un "Andiamo?"


translate italian day6_dv_1c08f469:


    me "Sì, certo…"


translate italian day6_dv_d2b198a7:


    "Non avevamo finito il nostro discorso, ma i pionieri sono apparsi dal nulla e hanno inondato il luogo. Lena era già seduta quando ho preso il mio pasto, era circondata da Miku, Zhenya e Slavya. Purtroppo non c'erano più sedie libere al loro tavolo."


translate italian day6_dv_b59dc367:


    "Sentendomi giù di morale e senza voler salutare nessuno, mi sono diretto verso il mio posto preferito nell'angolo in fondo alla sala."


translate italian day6_dv_64b85b7a:


    "Ho masticato la mia farina d'avena (era piuttosto gommosa oggi) e ho sorseggiato un po' di tè freddo. Poi ho iniziato a guardare gli altri."


translate italian day6_dv_9224c445:


    "Sembravano esserci tutti."


translate italian day6_dv_4b50e0bf:


    "Alcuni stavano chiacchierando, altri, come Electronik e Shurik, erano occupati a masticare, e altri ancora stavano fissando qualcosa imbambolati, me incluso."


translate italian day6_dv_3ddb5562:


    "Stavo per andarmene quando Ulyana è apparsa al mio tavolo, con il suo vassoio colmo di cibo."


translate italian day6_dv_0a9f12c1:


    us "Buon appetito!"


translate italian day6_dv_eb56d1ba:


    "Ha sbottato."


translate italian day6_dv_ceffc1ce:


    me "Stai rubando di nuovo?"


translate italian day6_dv_25072178:


    "Ho chiesto, fissando il suo vassoio."


translate italian day6_dv_93b62c6f:


    us "Se voglio crescere ho bisogno di molte calorie!"


translate italian day6_dv_3c9d580c:


    me "Già, dovresti proprio crescere un po'…"


translate italian day6_dv_9db6b90d:


    "Ho sussurrato."


translate italian day6_dv_b7ef3111:


    us "Che cosa?"


translate italian day6_dv_19ce3442:


    "Non credo che mi avesse sentito."


translate italian day6_dv_84662d7a:


    me "Niente…"


translate italian day6_dv_75e20332:


    "Me ne sono andato."


translate italian day6_dv_a2b6719e:


    us "Stai già andando?"


translate italian day6_dv_afe56f0b:


    "Ha urlato alle mie spalle, scoraggiata.{w} Non le ho dato alcuna risposta."


translate italian day6_dv_2f41a894:


    "Dovevo riportare la borsa con gli accessori da bagno nella mia stanza – non avevo voglia di portarmela dietro tutto il giorno."


translate italian day6_dv_8dc289f1:


    th "Tra l'altro, quali sono i miei piani per oggi?"


translate italian day6_dv_ae9fb3ad:


    "Non mi è venuto in mente niente."


translate italian day6_dv_409f5623:


    "Mi sentivo come se qualcuno stesse per presentarsi per invitarmi a fare un viaggio affascinante."


translate italian day6_dv_f321b160:


    "Ho lasciato la stanza della leader del Campo e mi sono fermato per un attimo, poi ho sentito una voce chiamarmi."


translate italian day6_dv_2a454c26:


    sl "Semyon!"


translate italian day6_dv_c50dbd79:


    "Era Slavya."


translate italian day6_dv_8543a313:


    sl "Sei occupato?"


translate italian day6_dv_5cb822ee_1:


    me "No…"


translate italian day6_dv_0b8f24fb:


    sl "Potresti aiutarmi?"


translate italian day6_dv_3df40f08:


    th "Ma certo…"


translate italian day6_dv_3e0102a8:


    "Non avevo voglia di cercare risposte in una giornata splendida come questa."


translate italian day6_dv_32de2538:


    th "Dopotutto un paio di dozzine di ore non cambieranno la mia situazione."


translate italian day6_dv_3eeb5def:


    me "Di cosa si tratta?"


translate italian day6_dv_ac32d9f9:


    sl "Dovresti passare al circolo di Cibernetica…{w} Non so esattamente a quale scopo, ti diranno tutto loro."


translate italian day6_dv_6954719a:


    th "Di nuovo gli sciagurati inventori…"


translate italian day6_dv_65bf82fc:


    me "D'accordo…"


translate italian day6_dv_309686e8:


    sl "Grazie!"


translate italian day6_dv_0167daaa:


    "Mi ha fatto un bel sorriso ed è corsa via."


translate italian day6_dv_dd9995b4:


    "E quindi un viaggio nella tana del nemico mi stava aspettando – la tana del malvagio Dr. Electronik e del diabolico professor Shurik.{w} Dottor Jekyll e Mr. Hyde…"


translate italian day6_dv_1ad8199f:


    "Ho aperto la porta con decisione e sono entrato."


translate italian day6_dv_45391550:


    el "Semyon…"


translate italian day6_dv_1d3a15b7:


    "Mi ha detto Electronik in tono un po' minaccioso, distogliendo lo sguardo da un qualche congegno che stava avvitando con un cacciavite."


translate italian day6_dv_81505d19:


    me "Mi manda Slavya."


translate italian day6_dv_e75769fe:


    el "Lo so."


translate italian day6_dv_f95c9fc9:


    me "E?"


translate italian day6_dv_772dd018:


    el "Cosa?"


translate italian day6_dv_7c46f700:


    "La seconda porta si è aperta, ed è entrato Shurik."


translate italian day6_dv_839ded9e:


    sh "Sei Semyon."


translate italian day6_dv_cf64a48c:


    "Mi ha salutato nello stesso tono di Electronik."


translate italian day6_dv_1e1be213:


    me "Beh, sì, sono sempre io…"


translate italian day6_dv_27c62366:


    "Ho scherzato."


translate italian day6_dv_ccd5a1e0:


    me "Allora, di cosa avete bisogno?"


translate italian day6_dv_782ffade:


    el "Abbiamo un importante compito per te!"


translate italian day6_dv_a2516dfe:


    me "Sto ascoltando."


translate italian day6_dv_74f9f6cb:


    sh "L'infermiera è passata di qui ieri."


translate italian day6_dv_c8c1467c:


    "Si è fermato, come se non avesse intenzione di proseguire."


translate italian day6_dv_f95c9fc9_1:


    me "E quindi?"


translate italian day6_dv_58c13b19:


    "Ho chiesto cautamente."


translate italian day6_dv_cce022c6:


    el "E quindi abbiamo un'importante missione per te."


translate italian day6_dv_63eb63d4:


    me "Quello l'ho già capito…"


translate italian day6_dv_c8416a10:


    sh "Devi consegnarle un pacco."


translate italian day6_dv_0997405a:


    me "Non potete farlo voi? L'infermeria è a pochi passi da qui."


translate italian day6_dv_c40a9bb9:


    "Ho chiesto scettico."


translate italian day6_dv_b6cbf345:


    el "Non possiamo."


translate italian day6_dv_47d017ef:


    "Ha detto Electronik, sorpreso."


translate italian day6_dv_379f1dd8:


    th "Che strana coppia, davvero."


translate italian day6_dv_11a96bf4:


    me "Capisco…{w} Spero non si tratti di un altro sacco pesante?"


translate italian day6_dv_f1caa021:


    el "No, no, niente del genere!"


translate italian day6_dv_f4166d68:


    "Ha sorriso."


translate italian day6_dv_0031a3ab:


    me "Va bene allora…"


translate italian day6_dv_1888335c:


    "Ho accettato con cautela."


translate italian day6_dv_3a378445:


    sh "Aspetta qui."


translate italian day6_dv_4ef4398f:


    "Shurik è scomparso dietro alla porta, e poco dopo è ritornato con in mano un qualcosa avvolto nella carta."


translate italian day6_dv_b9fafe93:


    sh "Fa' attenzione."


translate italian day6_dv_d9c0add8:


    me "Va bene, va bene."


translate italian day6_dv_abb01373:


    "Ho preso il pacchetto. Se non mi stavano passando la chiave per i cancelli perlati, sembrava comunque che mi stessero consegnando la Torcia Olimpica."


translate italian day6_dv_c3714a4b:


    me "Posso andare adesso?"


translate italian day6_dv_89559a2a:


    el "Certamente."


translate italian day6_dv_a193f014:


    "Mi ha detto minacciosamente Electronik."


translate italian day6_dv_98287c05:


    sh "Ma fa' attenzione!"


translate italian day6_dv_d7cdf7b2:


    "Ha supplicato Shurik."


translate italian day6_dv_9dd454a1:


    "Appena uscito, ho esaminato quel pacchetto."


translate italian day6_dv_e18730b6:


    "Non mi avevano vietato di aprirlo."


translate italian day6_dv_2c50bd85:


    th "Tuttavia, è meglio proseguire."


translate italian day6_dv_42b9bac1:


    "Mi sono incamminato rapidamente verso la piazza e, assicurandomi che nessuno mi stesse seguendo, mi sono infiltrato nella foresta."


translate italian day6_dv_32712073:


    "Sono rimasto lì in piedi per un po', indeciso su da farsi, guardando il pacchetto."


translate italian day6_dv_76fdd246:


    th "Mi chiedo.{w} Cosa c'è qui dentro?{w} Perché è così importante per loro?{w} Perché non possono farlo da soli?"


translate italian day6_dv_b468a82a:


    "Avevo un sacco di domande."


translate italian day6_dv_fb32ffc9:


    "Ho aperto il pacchetto e ho visto una bottiglia di vodka «Stolichnaya»…"


translate italian day6_dv_420166b4:


    th "Bene, e allora?{w} Dei pionieri che portano una bottiglia di vodka all'infermiera.{w} O meglio, lo sto facendo io al posto loro.{w} Cose che accadono tutti i giorni…"


translate italian day6_dv_87a11b40:


    "Ho riavvolto la bottiglia e mi sono grattato la testa."


translate italian day6_dv_d78595da:


    th "Questo non è più strano, ma stupido."


translate italian day6_dv_72e21fc3:


    th "Talmente stupido che non riesco a pensare ad una spiegazione intelligente."


translate italian day6_dv_93a5c183:


    "Sono rimasto lì per un po' e ho pensato che non dovevo farmi sfuggire una tale opportunità. Mi sono diretto rapidamente alla casetta della leader del Campo."


translate italian day6_dv_521a35e9:


    "Grazie a Dio, Olga Dmitrievna non c'era. Sono entrato di nascosto e ho cominciato a frugare nel cassetto del tavolo."


translate italian day6_dv_e0aa3f01:


    "Poco dopo avevo già travasato la vodka in una bottiglia di soda, mentre al suo posto ho messo l'acqua per le piante."


translate italian day6_dv_089fc8e9:


    "Contento del mio piano ingegnoso, sono uscito e mi sono affrettato verso l'infermeria."


translate italian day6_dv_1e5e0588:


    th "Scherzi a parte, che cosa c'è di sbagliato in ciò?{w} Mi hanno quasi teso una trappola!{w} Se la leader del Campo mi avesse visto..."


translate italian day6_dv_ef0f0a3b:


    th "Se qualcun altro mi avesse visto?"


translate italian day6_dv_2fc32561:


    th "Sarei riuscito a gestire la cosa…"


translate italian day6_dv_cc444139:


    th "Quindi non sarebbe stato affatto un problema!"


translate italian day6_dv_c8139598:


    "Ho raccolto tutte le mie forze e ho bussato alla porta dell'infermeria."


translate italian day6_dv_5d7721f0:


    cs "Ciao…{w} pioniere!"


translate italian day6_dv_99266ac9:


    me "Salve!{w} Avrei un pacchetto per lei – una fiasca per la festa…{w} Voglio dire, un pacchetto per il suo armadietto."


translate italian day6_dv_a2debb84:


    "Ero così euforico che ho cominciato a dire sciocchezze."


translate italian day6_dv_a9d81cd2:


    cs "Aah…"


translate italian day6_dv_c2732a68:


    "Ha detto."


translate italian day6_dv_7d07968d:


    cs "Da' qua."


translate italian day6_dv_aecf76f3:


    "Le ho consegnato il pacchetto."


translate italian day6_dv_b68868ff:


    "L'infermiera l'ha infilato nel cassetto della scrivania senza guardarlo."


translate italian day6_dv_bda69ee6:


    cs "Non l'hai aperto, vero?"


translate italian day6_dv_f837dd45:


    me "Certo che no!"


translate italian day6_dv_3d78efca:


    "Ho sbottato."


translate italian day6_dv_aedf1019:


    cs "Bravo ragazzo."


translate italian day6_dv_76aba977:


    me "Va bene, allora io…"


translate italian day6_dv_40e2f3e6:


    cs "Vai, vai… pioniere."


translate italian day6_dv_4aaea0be:


    "Ho chiuso la porta dietro di me, emettendo un sospiro."


translate italian day6_dv_23623d22:


    th "In caso di necessità posso sempre addossare la colpa ai due pazzi inventori, e nella mia situazione dovrei proprio approfittare anche del più semplice metodo di alleviamento dello stress!"


translate italian day6_dv_0cd6a776:


    "Sono arrivato alla piazza.{w} L'orologio mostrava mezzogiorno. E ciò significava che il pranzo era imminente."


translate italian day6_dv_e4fa05b8:


    "Mi sono seduto sulla panchina con la ferma intenzione di non muovermi da lì fino a quando il segnale del «vieni-e-serviti» della sirena sarebbe risuonato nell'aria."


translate italian day6_dv_c6cc3d58:


    "I pionieri correvano tutt'attorno.{w} Sorridendo e gridando allegramente."


translate italian day6_dv_94906696:


    th "Sì, la giornata è decisamente meravigliosa."


translate italian day6_dv_cdd7919b:


    "Ho lanciato uno sguardo verso la piazza, e ho visto Lena avvicinarsi a me."


translate italian day6_dv_f3d744ae:


    un "Ti stai rilassando?"


translate italian day6_dv_6b635703:


    me "Qualcosa del genere…"


translate italian day6_dv_b8aa5db1:


    "Si è seduta accanto a me ed ha alzato gli occhi al cielo."


translate italian day6_dv_2c5438ac:


    un "Bella giornata, eh?"


translate italian day6_dv_31663314:


    me "Già…"


translate italian day6_dv_fde4d309:


    "Era un po' imbarazzante. Lena sembrava diversa. Lei non era imbarazzata, non stava arrossendo, era sicura di sé."


translate italian day6_dv_c20baf86:


    me "Allora, riguardo a quel nostro discorso…"


translate italian day6_dv_8891daf5:


    un "Ti ho già detto tutto."


translate italian day6_dv_8cd8b52e:


    me "Sì, ma…"


translate italian day6_dv_3cd3c424:


    un "Lascia perdere."


translate italian day6_dv_48f1666a:


    me "Se lo dici tu…"


translate italian day6_dv_a4f042ce:


    "Siamo rimasti seduti in silenzio ancora per un po'."


translate italian day6_dv_2c070d08:


    me "Non ci riesco proprio, non così…"


translate italian day6_dv_dd328974:


    un "Allora, cosa ne pensi di Alisa?"


translate italian day6_dv_d22f0a40:


    "Mi ha chiesto, ignorando la mia richiesta."


translate italian day6_dv_591ad32f:


    me "Cosa penso di Alisa?"


translate italian day6_dv_86e16e26:


    "Ho ripetuto, perso nei miei pensieri."


translate italian day6_dv_943e93f2:


    me "Davvero non so…{w} Perché me lo chiedi?"


translate italian day6_dv_caa0bbb4:


    un "Sei così preoccupato per ieri."


translate italian day6_dv_633b4e6d:


    me "Beh, certo… Ma questo che c'entra?"


translate italian day6_dv_a7c0899d:


    un "C'entra eccome!"


translate italian day6_dv_ce3666e5:


    "Ha riso."


translate italian day6_dv_9b6ef66d:


    me "Supponiamo di sì…"


translate italian day6_dv_235d1b4e:


    un "E allora?"


translate italian day6_dv_e7450e08:


    me "Beh, lei…"


translate italian day6_dv_2350d104:


    "E ho cominciato di nuovo a pensare."


translate italian day6_dv_111d6d4f:


    th "Davvero, cosa penso di lei?"


translate italian day6_dv_d2293c40:


    me "Beh, lei…{w} A volte può essere arrogante."


translate italian day6_dv_ab97ad5d:


    un "Giusto."


translate italian day6_dv_a0062ec1:


    th "Che altro potrei dire?{w} Ora che ci penso, non la conosco affatto.{w} A volte il modo in cui si comporta mi infastidisce."


translate italian day6_dv_c86c4e80:


    me "Se ti stavi chiedendo se io provo qualcosa di negativo nei suoi confronti, allora no."


translate italian day6_dv_631a37e5:


    me "Voglio dire, ogni tanto si fa prendere la mano, ma ognuno ha i suoi difetti…"


translate italian day6_dv_4c144815:


    "Lena mi ha fissato."


translate italian day6_dv_93207c79:


    un "Davvero?"


translate italian day6_dv_97bb8cf6:


    me "Ho detto qualcosa di sbagliato?"


translate italian day6_dv_7bc1d385:


    un "No, non proprio…"


translate italian day6_dv_544a3a75:


    "Si è messa a ridere nuovamente, proprio prima che arrivasse il suono della sirena del pranzo."


translate italian day6_dv_5aac5414:


    un "Va bene, ci vediamo!"


translate italian day6_dv_7c13b2c0:


    "Non l'ho fermata, anche se avevo ancora molte domande da farle."


translate italian day6_dv_42970b85:


    th "Ma come potrei farle tali domande?{w} E soprattutto – è davvero il caso di insistere?"


translate italian day6_dv_3869395e:


    "Mi sono alzato e mi sono trascinato con fatica verso la mensa."


translate italian day6_dv_14e6034a:


    "C'erano solo pochi posti liberi, quindi mi sono dovuto sedere con Electronik e Shurik."


translate italian day6_dv_497bae1a:


    el "L'hai consegnato?"


translate italian day6_dv_9513cd87:


    me "Sì…"


translate italian day6_dv_40231c6f:


    "Ho risposto pigramente, infilzando il grano saraceno con la mia forchetta."


translate italian day6_dv_807920a2:


    el "Hai avuto problemi?"


translate italian day6_dv_a0c4afad:


    me "Nessun problema…{w} Comunque, cosa c'era lì dentro?"


translate italian day6_dv_01cfac1d:


    el "Non l'hai aperto, vero?"


translate italian day6_dv_2c5335a6:


    "Mi ha fissato."


translate italian day6_dv_5cb822ee_2:


    me "No…"


translate italian day6_dv_fa45ce2d:


    el "Ottimo!"


translate italian day6_dv_385405d4:


    "Dopo aver finito il mio pasto, sono uscito."


translate italian day6_dv_bebd5afd:


    "Improvvisamente mi è venuta voglia di sbadigliare."


translate italian day6_dv_2984a468:


    th "Fare un pisolino non sarebbe una cattiva idea."


translate italian day6_dv_f4822807:


    "Mi sono diretto verso la stanza della leader del Campo."


translate italian day6_dv_b85dfb0f:


    "Era come se il sole estivo stesse per sciogliere l'intero pianeta, assieme a tutti i suoi abitanti."


translate italian day6_dv_4e7b8856:


    "Non c'era alcun luogo dove potersi nascondere – la luce del sole sembrava insidiarsi in ogni angolo. Alzando gli occhi verso il cielo si rischiava di diventare ciechi."


translate italian day6_dv_4d2a4c1c:


    "Usando le mie ultime gocce di forza ho tirato la maniglia, sono entrato nella stanza e mi sono lasciato cadere sul letto."


translate italian day6_dv_aefac231:


    "Naturalmente non mi aspettavo che la stanza fosse fresca, ma era molto meglio che fuori."


translate italian day6_dv_802ba619:


    "Mi sono disteso beatamente sul letto e ho chiuso gli occhi."


translate italian day6_dv_799e8d13:


    "Stranamente, il sonno ha disertato. Sono stato sopraffatto dai pensieri, che hanno iniziato a brulicarmi in testa."


translate italian day6_dv_601d58f7:


    "Ecco un'altra teoria, una soluzione all'enigma del come io fossi arrivato qui.{w} Ecco una spiegazione di ciò che stava accadendo qui.{w} Ed ecco la vera natura di tutti coloro che dimoravano in questo luogo."


translate italian day6_dv_8152340b:


    "Mi sono ricordato le parole di Lena, che mi aveva chiesto: «Che ne pensi di Alisa?»"


translate italian day6_dv_baeb4f56:


    th "Scherzi a parte, che cosa penso veramente di lei?{w} Cosa posso pensare di una persona che ho conosciuto solo da un paio di giorni?{w} Perché dovrei trarre delle conclusioni basandomi solo su un paio di parole o di azioni?"


translate italian day6_dv_4881f3b0:


    th "Anche se le sue azioni…"


translate italian day6_dv_a026df63:


    "Ho riso un po'."


translate italian day6_dv_5492f5b1:


    "Qualsiasi organizzazione seria potrebbe trarre le giuste conclusioni basandosi sull'attentato al monumento."


translate italian day6_dv_27abcde7:


    th "E allora perché a me invece…{w} non importa?"


translate italian day6_dv_e0e83cfe:


    th "Forse davvero non penso niente di lei?"


translate italian day6_dv_79c3d71e:


    th "Perché dovrei essere interessato ad una persona che non è interessante?"


translate italian day6_dv_32ee260b:


    th "No, è proprio assurdo…"


translate italian day6_dv_7e8c00bf:


    "Qualcuno ha bussato alla porta..."


translate italian day6_dv_6b33d385:


    me "Avanti!"


translate italian day6_dv_d96562ca:


    "Ero troppo pigro per alzarmi, così mi sono limitato a mettermi seduto."


translate italian day6_dv_cc6d016b:


    "La porta si è aperta ed è apparsa Alisa.{w} Che coincidenza…"


translate italian day6_dv_e8401a0f:


    "Si è fermata, incerta, e mi ha fissato per qualche secondo."


translate italian day6_dv_344347eb:


    me "Suppongo tu stia cercando la leader del Campo?"


translate italian day6_dv_6a8dcc12:


    th "E perché mai dovrebbe?"


translate italian day6_dv_e8936f84:


    dv "No."


translate italian day6_dv_e2960fb5:


    me "Allora cosa c'è?"


translate italian day6_dv_a09125aa:


    dv "Stavo cercando te."


translate italian day6_dv_5e777c99:


    "Sembrava calma e per nulla aggressiva."


translate italian day6_dv_15c7a20d:


    "Mi sono trattenuto, pensando che non c'era ragione di aver paura di lei quando si comportava in quel modo – normalmente."


translate italian day6_dv_7332342b:


    me "E perché?"


translate italian day6_dv_7d4b9078:


    dv "Volevo chiarire le cose una volta per tutte."


translate italian day6_dv_0ed62df2:


    "Si è seduta sul letto opposto di fronte a me."


translate italian day6_dv_ae56873f_1:


    me "Che cosa, esattamente?"


translate italian day6_dv_4cf6f6d7:


    dv "Volevo eliminare eventuali incomprensioni o malintesi."


translate italian day6_dv_32c1cb65:


    me "Va bene, ci sto!"


translate italian day6_dv_d6f5f041:


    dv "Hai parlato con Lena oggi?"


translate italian day6_dv_fc30fd2b:


    me "Sì."


translate italian day6_dv_21a68fd8:


    dv "Riguardo a cosa?"


translate italian day6_dv_96467507:


    me "Nulla di serio…"


translate italian day6_dv_3e9127d1:


    dv "Stai mentendo."


translate italian day6_dv_e9a6a178:


    "Alisa sembrava tesa e preoccupata.{w} Ha accavallato le gambe e si è morsa il labbro."


translate italian day6_dv_6300bc11:


    dv "Tu capisci che…{w} Tutti pensano che sia sempre colpa mia, di tutto quello che succede?"


translate italian day6_dv_0288549d:


    me "Beh, in effetti lo è."


translate italian day6_dv_c41aa4a5:


    "Ho detto sconsideratamente."


translate italian day6_dv_69364372:


    "Una scintilla è balenata nei suoi occhi, ma ha continuato a parlare con calma:"


translate italian day6_dv_f0c03665:


    dv "Forse…{w} Ma in questo caso…{w} Io e Lena, siamo sempre così – anche se è lei a iniziare a discutere, alla fine lei è sempre quella innocente e io mi prendo tutta la colpa!"


translate italian day6_dv_63fdbbbc:


    me "Quindi mi stai dicendo che Lena…{w} A dire il vero, non capisco cosa intendi dire."


translate italian day6_dv_60dc8a1d:


    dv "Non capisci mai niente, vero?!"


translate italian day6_dv_d1fbe814:


    "Ribolliva dalla rabbia."


translate italian day6_dv_c727773e:


    th "Strano, non mi definirei un tipo ottuso."


translate italian day6_dv_3ce5fd8b:


    me "Allora spiegami."


translate italian day6_dv_25156efc:


    dv "Lo sai che piaci a Lena, vero?"


translate italian day6_dv_1aa433e1:


    "Dopo quelle parole è arrossita."


translate italian day6_dv_5cb822ee_3:


    me "No…"


translate italian day6_dv_e0299b09:


    "Ho risposto con sincerità."


translate italian day6_dv_37a6038e:


    dv "Bene, adesso lo sai…"


translate italian day6_dv_9ecfc70c:


    me "Adesso lo so…"


translate italian day6_dv_ae829ad1:


    "Un silenzio imbarazzante si è posato sulla stanza."


translate italian day6_dv_62543cf8:


    "Stavo elaborando quello che mi aveva appena detto, e lei sembrava indecisa su come continuare la conversazione."


translate italian day6_dv_768d0b51:


    th "Piaccio a Lena...{w} Me ne sarei mai accorto?{w} Improbabile."


translate italian day6_dv_49ebd22b:


    "Le mie intuizioni sono generalmente buone, ma quando si tratta di me e dei miei sentimenti non funzionano affatto."


translate italian day6_dv_23bbf0e9:


    "La cosa più strana è che ho accolto quella notizia quasi senza alcuna preoccupazione."


translate italian day6_dv_23eb34e0:


    "Al momento stavo ascoltando Alisa, ero concentrato solo su di lei. Il resto poteva aspettare."


translate italian day6_dv_4b721f97:


    dv "Lei è gelosa di te, pensa che io possa portarti via da lei!{w} Lo pensa sempre, capisci? Anche se è esattamente il contrario! Tutti guardano lei perché lei è un angelo, e nessuno presta attenzione a me!"


translate italian day6_dv_61d7ee92:


    dv "Per loro io sono solo una spalla su cui piangere…"


translate italian day6_dv_3bc3b19b:


    "Alisa ha iniziato a gridare."


translate italian day6_dv_095c18d7:


    me "Senti, io…{w} Non essere così sconvolta! Io non la penso così riguardo a te!"


translate italian day6_dv_ecfa445c:


    dv "Naturalmente…"


translate italian day6_dv_c2c94b69:


    "Non avevo idea di cosa l'avesse spinta ad essere così sincera nei miei confronti.{w} E non sapevo come reagire.{w} Inoltre, non ero affatto preoccupato."


translate italian day6_dv_c62c09a6:


    "Era come se stessi guardando l'episodio di una serie televisiva, e che tutto quello che stava accadendo all'interno di questa stanza fosse in realtà dall'altra parte dello schermo."


translate italian day6_dv_ef389ffb:


    me "Capisco, sono io la ragione di tutto questo…{w} Ma oggi Lena mi ha detto che a lei sta bene così."


translate italian day6_dv_5eb2b915:


    dv "È proprio così che fa! Probabilmente avrai capito da solo che non è così tranquilla come vuole apparire!"


translate italian day6_dv_5b3e26db:


    th "Sì, ho iniziato a farci caso."


translate italian day6_dv_650270d4:


    me "Se c'è qualcosa che posso fare…"


translate italian day6_dv_323fb304:


    dv "Dimmi…"


translate italian day6_dv_092b3b93:


    "Alisa ha alzato lo sguardo.{w} Solo allora mi sono accorto che stava piangendo."


translate italian day6_dv_8c4f1a22:


    "Una sorta di preoccupazione si è insidiata all'interno di me.{w} Non potevo più considerare quella conversazione come se fossi un estraneo."


translate italian day6_dv_c7e864ed:


    dv "Ti piace Lena? O ti piaccio io?"


translate italian day6_dv_da3f2db4:


    "Mi sarei dovuto aspettare una domanda del genere, ma non ero ancora pronto a dare una risposta."


translate italian day6_dv_84e48168:


    me "Se me lo chiedi così…"


translate italian day6_dv_a0eda2ae:


    th "Lena è carina, naturalmente…{w} Ma anche Alisa ha i suoi pregi."


translate italian day6_dv_a769edd0:


    "Inoltre, dal modo in cui si stava presentando al momento, era decisamente in grado di competere con Lena."


translate italian day6_dv_a4fad65f:


    me "In realtà non vi conosco molto bene, nessuna delle due…{w} A una domanda del genere…{w} È difficile rispondere così su due piedi."


translate italian day6_dv_9a3e6f7f:


    "Se avessi potuto dirle «no», l'avrei fatto subito…"


translate italian day6_dv_73ec7242:


    "Ma anche l'altra opzione avrebbe comportato le sue conseguenze."


translate italian day6_dv_56680e86:


    "E non sapevo quale delle due scegliere."


translate italian day6_dv_f673f8e3:


    dv "Volevi che la situazione si risolvesse da sé."


translate italian day6_dv_80322d80:


    me "Certo, ma…"


translate italian day6_dv_32cbd9c4:


    dv "Perché di nuovo «ma»?"


translate italian day6_dv_3ea35b71:


    me "Perché «di nuovo»?"


translate italian day6_dv_4c7ded3a:


    dv "Perché «perché»?"


translate italian day6_dv_f21d1a90:


    me "Senti, fare così non ci porterà a niente…"


translate italian day6_dv_3baacd9d:


    dv "Allora rispondimi!"


translate italian day6_dv_db0ec79f:


    "La sua espressione è tornata alla sua consueta e sfacciata arroganza."


translate italian day6_dv_d1744488:


    me "Ho bisogno di tempo per pensare…"


translate italian day6_dv_f097abfb:


    dv "Bene, allora pensaci su!"


translate italian day6_dv_b750f4ae:


    "Alisa si è alzata di scatto e si è incamminata verso la porta."


translate italian day6_dv_5783bb1b:


    dv "Non dimenticare che abbiamo dei piani per stasera!"


translate italian day6_dv_70813d40:


    "Ha detto sulla soglia, sorridendo."


translate italian day6_dv_d7d9b64d:


    "La porta si è richiusa alle sue spalle prima che potessi dire qualcosa."


translate italian day6_dv_94e61f1e:


    th "Piani? Che tipo di piani?"


translate italian day6_dv_76c05f84:


    "Quella chiacchierata con Alisa ha lasciato un segno profondo dentro di me."


translate italian day6_dv_37b565c4:


    th "E così, piaccio a Lena…"


translate italian day6_dv_0e2e48a7:


    th "Di per sé, una tale notizia potrebbe scioccare e spaventare. E soprattutto sono costretto a fare una scelta. Una scelta difficile, per usare un eufemismo. E qualunque decisione io prenda, sarà sbagliata per qualcuno."


translate italian day6_dv_7e235707:


    th "Se solo potessi rifiutare entrambe…"


translate italian day6_dv_e7446732:


    "Anche se non mi piaceva il termine «rifiutare»."


translate italian day6_dv_92bca9c0:


    th "Se solo potessi rispondere alla sua domanda con un semplice «no», sarebbe tutto più semplice."


translate italian day6_dv_4cf63875:


    "Ma sapevo che non avrebbe potuto funzionare in quel modo."


translate italian day6_dv_1aaf9536:


    th "Dopo aver parlato con Alisa, mi sono reso conto che nulla sarebbe stato più lo stesso."


translate italian day6_dv_0b14c784:


    "Non avrebbe potuto esserlo, indipendentemente da tutto.{w} E io dovevo prendere una decisione…"


translate italian day6_dv_0fa075d4:


    th "Dato che ho già scartato il «no», allora devo scegliere l'altra opzione.{w} Sempre che io sia in grado di sopportarla."


translate italian day6_dv_66117c1c:


    "Ma ero sicuro che di tutte le possibili conseguenze, non ce n'era una che potesse soddisfare tutti, ed era proprio quella che desideravo."


translate italian day6_dv_0086716d:


    "La scelta mi sembrava ovvia: Lena.{w} Un ragazza carina, dolce, gentile."


translate italian day6_dv_6cd9e693:


    "Tuttavia, c'era qualcosa in Alisa che mi attraeva…"


translate italian day6_dv_be0923e0:


    "Improvvisamente la porta si è spalancata, e Olga Dmitrievna è balzata dentro."


translate italian day6_dv_0bae5ad4:


    mt "Stai bighellonando di nuovo!"


translate italian day6_dv_fe458b8c:


    me "Sto solo riposando…{w} Fa così caldo là fuori…"


translate italian day6_dv_2ebe60a9:


    mt "Vai ad aiutare Slavya. Ha bisogno di una mano."


translate italian day6_dv_25f7abbc:


    me "Dove?"


translate italian day6_dv_7a91aa07:


    "Ho chiesto pigramente."


translate italian day6_dv_a18b435d:


    mt "Al campo sportivo."


translate italian day6_dv_5c421508:


    "A malincuore, mi sono alzato e sono uscito."


translate italian day6_dv_ee607f36:


    "Dopotutto non mi avrebbe fatto male tenere la mente un po' occupata."


translate italian day6_dv_a7fe7928:


    "C'erano un sacco di persone al campo sportivo.{w} Alcuni stavano giocando a calcio, altri giocavano a badminton.{w} I bambini più piccoli si stavano semplicemente rincorrendo."


translate italian day6_dv_2a454c26_1:


    sl "Semyon!"


translate italian day6_dv_876e414e:


    "Slavya si è avvicinata a me."


translate italian day6_dv_bf8808dd:


    me "Ha bisogno della mia assistenza?"


translate italian day6_dv_ecb3efc7:


    "Ho chiesto, in tono ufficiale."


translate italian day6_dv_b793031f:


    sl "Sì…{w} Senti, per caso hai preso la chiave dell'infermeria?"


translate italian day6_dv_5a7a7577:


    me "No."


translate italian day6_dv_9babb1c2:


    th "Perché sempre io?!"


translate italian day6_dv_93bd5577:


    sl "L'hai visitata spesso, recentemente."


translate italian day6_dv_ac1c31df:


    th "Suppongo di sì…"


translate italian day6_dv_5cb822ee_4:


    me "No…"


translate italian day6_dv_234b5492:


    sl "Allora forse sai chi potrebbe averla presa?"


translate italian day6_dv_3119e7b5:


    me "No. Perché, cosa è successo?"


translate italian day6_dv_da46fa34:


    sl "L'infermiera mi ha chiesto di prestarle la mia chiave – sai che io ho una chiave per ogni edificio nel Campo.{w} Ha detto che la sua è sparita."


translate italian day6_dv_4070ac03:


    me "Forse l'ha persa…"


translate italian day6_dv_0fe6b15a:


    "Ho alzato le spalle."


translate italian day6_dv_1197e9fc:


    sl "No, dice che non è possibile."


translate italian day6_dv_85cd4b82:


    me "Non lo so, ok?"


translate italian day6_dv_35b300f9:


    sl "Ok."


translate italian day6_dv_0049a2fb:


    "Ha sorriso ed è corsa via."


translate italian day6_dv_52ec3723:


    "Mi sono diretto alla casetta della leader del Campo, determinato a dormire finalmente."


translate italian day6_dv_02ad66d4:


    "Olga Dmitrievna non c'era, grazie a Dio, così mi sono lasciato cadere sul letto e ho perso conoscenza."


translate italian day6_dv_a20cefa7:


    "..."


translate italian day6_dv_4df22cd5:


    "Non ricordo esattamente che cosa io avessi sognato, ma mi ha lasciato un forte segno, come spesso fanno i sonnellini pomeridiani."


translate italian day6_dv_ea3f5806:


    "Con grande sforzo sono sceso dal letto e ho guardato l'orologio.{w} Erano appena passate le sette."


translate italian day6_dv_53b55b1b:


    th "Non mi sorprende dopotutto – c'era da aspettarsi che avrei dormito per l'intera giornata."


translate italian day6_dv_23d4a9c7:


    th "Anche se, a quanto pare, ho saltato pure la cena..."


translate italian day6_dv_31e3afb8:


    "Quel pensiero mi ha messo a disagio, e mi sono diretto verso l'uscita."


translate italian day6_dv_ee3bcd9a:


    "Mi sono fermato sulla soglia e ho pensato a quale sarebbe stata la mia prossima mossa."


translate italian day6_dv_df471347:


    "Non so per quanto tempo sarei rimasto lì – la mia testa era ancora pesante dopo il sonno – se qualcuno non mi avesse chiamato."


translate italian day6_dv_280c3ef3:


    "Slavya stava camminando a passo svelto verso di me."


translate italian day6_dv_1e187d3c:


    sl "Semyon…"


translate italian day6_dv_eef5cfb9:


    "Ha iniziato, senza fiato."


translate italian day6_dv_fce6b834:


    sl "Sei sicuro di non avere la chiave dell'infermeria?"


translate italian day6_dv_cafb72c1:


    me "Certo che no, perché dovrei averne bisogno?"


translate italian day6_dv_af3f3208:


    sl "Beh, forse sai chi l'ha presa?"


translate italian day6_dv_cddb9533:


    me "No, non lo so. Perché?"


translate italian day6_dv_a19bb019:


    sl "Beh… Qualcosa è scomparso."


translate italian day6_dv_8b677452:


    me "Che cosa?"


translate italian day6_dv_b9cf32d1:


    sl "Qualcosa di importante…"


translate italian day6_dv_eba68a85:


    "Ha esitato."


translate italian day6_dv_28ce78b4:


    sl "Un medicinale."


translate italian day6_dv_eb5ac138:


    th "Quindi tutto questo casino è dovuto a un qualche medicinale?{w} Anche se…"


translate italian day6_dv_89141735:


    "Ho riflettuto."


translate italian day6_dv_19d82568:


    th "Voglio dire, ho la bottiglia nella mia stanza…{w} Forse si riferisce a quella?"


translate italian day6_dv_bbccfea0:


    "Ho cercato di mantenere la stessa espressione, senza lasciar trapelare nulla di sospetto."


translate italian day6_dv_065dee42:


    me "Ok.{w} Non lo so, mi dispiace…"


translate italian day6_dv_6f9aa3f1:


    sl "Va bene…"


translate italian day6_dv_280be8fe:


    "Ha detto Slavya, frustrata."


translate italian day6_dv_e22d0105:


    "L'ho guardata e ho cercato di capire quanto fosse grave la situazione per me, o se fossi effettivamente nei guai."


translate italian day6_dv_dffe083c:


    th "Da un lato, la vodka da quella bottiglia… l'avevo proprio…{w} rubata."


translate italian day6_dv_935f06ca:


    th "Ma d'altronde, ha detto che «è sparita», senza implicare che l'abbia presa qualcuno."


translate italian day6_dv_383cfe31:


    th "Anche se è altamente improbabile che lei mi possa dire che una bottiglia di vodka è stata rubata dall'infermeria."


translate italian day6_dv_910fce4e:


    th "Quindi da come l'ha detto, sembra tutto a posto. Il problema è un altro."


translate italian day6_dv_1072ee7f:


    "Improvvisamente ho avuto una paura immensa."


translate italian day6_dv_73f94476:


    th "E se scoprissero che sono stato io?"


translate italian day6_dv_65b4be16:


    th "Anche se non si trattasse di quella bottiglia, scopriranno comunque che la vodka non c'è più."


translate italian day6_dv_dea23e04:


    "In poche parole, dovevo darmi una mossa!{w} E la cosa più importante da fare adesso era sbarazzarsi di tutte le prove!"


translate italian day6_dv_bea31ad1:


    "Sono balzato in piedi, ho nascosto la bottiglia sotto la mia cintura e sono corso verso la foresta.{w} Seppellirla da qualche parte era la decisione migliore al momento."


translate italian day6_dv_27c88b77:


    "Ho impiegato un po' di tempo a cercare un luogo adatto, senza essermi accorto che la notte era già scesa sul Campo."


translate italian day6_dv_768e9627:


    "Una voce familiare mi ha fatto sobbalzare."


translate italian day6_dv_93b5fc50:


    dv "Ehi!"


translate italian day6_dv_66bf0ead:


    "Ho fatto fatica a voltarmi verso di lei, cercando di mantenere un'espressione più casuale possibile."


translate italian day6_dv_dbd3bc52:


    dv "Allora. Che sta succedendo qui?"


translate italian day6_dv_385b3fe9:


    me "Oh, sto solo facendo una passeggiata…"


translate italian day6_dv_405fbe4e:


    "La mia voce suonava tranquilla, ma l'avevo già capito – non potevo ingannare Alisa."


translate italian day6_dv_4529043a:


    dv "Da solo? Di notte? Nella foresta?"


translate italian day6_dv_4e1558a6:


    me "Beh, sì… Che c'è di male?"


translate italian day6_dv_fcd85f45:


    dv "Niente. Proprio niente."


translate italian day6_dv_1b595140:


    "Mi ha fissato."


translate italian day6_dv_76aba977_1:


    me "Va bene, allora io…"


translate italian day6_dv_c250ee11:


    dv "Aspetta!{w} Non ti stai dimenticando qualcosa?"


translate italian day6_dv_01f23cac:


    me "Non credo."


translate italian day6_dv_4daa4260:


    dv "Avevamo un accordo!"


translate italian day6_dv_c233b6a2:


    me "Quale?"


translate italian day6_dv_59e88892:


    dv "Per stanotte!"


translate italian day6_dv_1a41c767:


    "Forse aveva ragione. Mi aveva detto qualcosa del genere."


translate italian day6_dv_d72e579c:


    me "È vero, ma sai…"


translate italian day6_dv_afd1c304:


    dv "No, non lo so! E adesso andiamo!"


translate italian day6_dv_d3dbfb33:


    "Si è voltata, facendomi cenno di seguirla."


translate italian day6_dv_2a3aebf5:


    "Non c'era motivo di discutere – qualsiasi parola o azione mi avrebbero tradito."


translate italian day6_dv_fe0a8a5c:


    th "E poi…"


translate italian day6_dv_1faec534:


    "Sospirando, l'ho seguita."


translate italian day6_dv_b5027457:


    "Dopo un paio di minuti abbiamo raggiunto la sua casetta."


translate italian day6_dv_4d608c0d:


    "Mentre stavamo camminando, ero impegnato a cercare di trovare l'occasione e il luogo più adatti per poter nascondere la vodka, ma Alisa è rimasta così vicina che non ho potuto fare alcunché."


translate italian day6_dv_dbeba143:


    dv "Ebbene?"


translate italian day6_dv_1b595140_1:


    "Mi ha fissato."


translate italian day6_dv_d6f22291:


    me "Ebbene cosa?"


translate italian day6_dv_e3e21bbe:


    "Ero talmente nervoso che i miei capelli erano impregnati di sudore."


translate italian day6_dv_2ffa378a:


    "L'unica cosa di cui mi preoccupavo in quel momento era di come poter tenere nascosta la bottiglia in modo che lei non se ne accorgesse."


translate italian day6_dv_988e7f2d:


    dv "Che mi dici?"


translate italian day6_dv_a4388070:


    me "Riguardo a cosa?"


translate italian day6_dv_3614794f:


    "Alisa ha riflettuto."


translate italian day6_dv_8c1b9d53:


    dv "Ok, credo che tu abbia ragione – ti ho invitato io, quindi dovrei decidere io."


translate italian day6_dv_505d91ae:


    me "Va bene…"


translate italian day6_dv_dea40394:


    "Ho accettato, incerto."


translate italian day6_dv_92e1ccd1:


    dv "Aspetta un secondo…"


translate italian day6_dv_cb6c3ebb:


    "Ha frugato sotto al letto e dopo pochi secondi…{w} ha tirato fuori la bottiglia di vodka «Stolichnaya»!"


translate italian day6_dv_0ab734f4:


    "Ero sotto shock."


translate italian day6_dv_3ff96290:


    dv "Sorpreso, eh?"


translate italian day6_dv_36d2fce0:


    th "Certo!{w} Non per la vodka in sé, ma per il fatto che è chiaramente la bottiglia che ho riportato io all'infermeria stamattina."


translate italian day6_dv_1d557f44:


    "Il torbido liquido contenuto in essa me l'ha fatto capire subito."


translate italian day6_dv_4d1c9363:


    dv "Allora?"


translate italian day6_dv_13181c03:


    "Alisa mi ha guardato in modo cospiratorio."


translate italian day6_dv_ad02f53a:


    me "Cosa?"


translate italian day6_dv_9a78a547:


    dv "Vuoi berne un sorso?"


translate italian day6_dv_235f9753:


    "Non sapevo proprio cosa risponderle."


translate italian day6_dv_d54f3e81:


    th "Quella che tiene tra le mani è solo acqua sporca. La vera vodka si trova nella bottiglia sotto alla mia cintura."


translate italian day6_dv_8d2027c8:


    me "Beh, forse è meglio se…"


translate italian day6_dv_77dfc065:


    dv "Non dirmi che sei astemio!"


translate italian day6_dv_9859878b:


    "Ha messo il broncio."


translate italian day6_dv_d2ea3d33:


    me "Non so… No, è solo che…"


translate italian day6_dv_c9e23ee7:


    dv "Bene allora! Aspetta un attimo!"


translate italian day6_dv_f816894f:


    "In un istante Alisa ha afferrato due bicchieri e li ha riempiti fino all'orlo."


translate italian day6_dv_3a7f0ff5:


    dv "Facciamolo!"


translate italian day6_dv_bdc908c8:


    "Ha detto, porgendomi uno dei bicchieri, e ha ingurgitato il suo prima che potessi dire qualsiasi cosa."


translate italian day6_dv_31c05c43:


    dv "Augh!"


translate italian day6_dv_8ad16087:


    "Ha boccheggiato."


translate italian day6_dv_17c04f77:


    "E in quel momento si è resa conto…"


translate italian day6_dv_1df814f8:


    dv "Ma che diavolo è questo?!"


translate italian day6_dv_e54a6b95:


    "Ha iniziato a sputare."


translate italian day6_dv_c73f3fec:


    dv "E la chiamano vodka?!"


translate italian day6_dv_420183fc:


    "Nonostante i miei timori, non ho potuto fare a meno di ridere."


translate italian day6_dv_b2fde6e2:


    dv "Che c'è di tanto divertente?!"


translate italian day6_dv_6858883c:


    "Ha gridato, diventando tutta rossa."


translate italian day6_dv_9ed6914d:


    dv "Ti ho chiesto, cosa c'è di divertente?!"


translate italian day6_dv_6c56b254:


    me "In realtà è divertente."


translate italian day6_dv_0c423888:


    dv "Ti stai divertendo, eh? Bene, bevi il tuo allora – vedremo poi chi riderà!"


translate italian day6_dv_2488dfae:


    "Non avevo alcuna intenzione di assaggiare l'acqua stantia, ma solo per evitare di insospettirla le ho chiesto:"


translate italian day6_dv_7150554e:


    me "Allora, che gusto ha?"


translate italian day6_dv_a0c02db8:


    dv "Di acqua andata a male."


translate italian day6_dv_e0350d8e:


    "Ha detto, irritata."


translate italian day6_dv_8d978d04:


    dv "Volevo solo rilassarmi un po', fare un po' di baldoria raffinata, e invece…"


translate italian day6_dv_84e0b485:


    me "Baldoria raffinata…"


translate italian day6_dv_4243032b:


    "Ho ripetuto, prima che un nuovo attacco di risate mi piegasse a metà."


translate italian day6_dv_43e1408d:


    dv "Ehi, smettila!"


translate italian day6_dv_df3fece3:


    "Ha gridato Alisa, scagliandosi contro di me."


translate italian day6_dv_5fb59874:


    "Stava cercando di colpirmi, di mordermi, o qualcosa del genere. Forse il suo piano era quello di schiacciarmi a morte con la sua autorità. Ho afferrato le sue mani, e tutto quello che poteva fare era sussultare dalla rabbia, impotente."


translate italian day6_dv_88ce415c:


    "Tutto stava andando bene, ma poi Alisa è scivolata improvvisamente, iniziando a cadere."


translate italian day6_dv_550fc70f:


    "Ho lasciato andare le sue mani, e così è caduta proprio sopra di me, dandomi una testata dritto sul naso."


translate italian day6_dv_3d629c43:


    "Ho gemuto dal dolore, scrollandomela di dosso e girandomi di lato."


translate italian day6_dv_bc81e122:


    dv "Cosa abbiamo qui?"


translate italian day6_dv_6d5303d4:


    "Non appena ho capito che aveva trovato la mia bottiglia, tutto il dolore è improvvisamente scomparso."


translate italian day6_dv_679b46e1:


    me "Ridammela!"


translate italian day6_dv_7be7066b:


    "Ho cercato di riprendere la bottiglia, ma Alisa è saltata giù dal letto con agilità ed è corsa verso un angolo della stanza."


translate italian day6_dv_8e960ce3:


    dv "Vediamo un po'!"


translate italian day6_dv_f8e2a6f3:


    "Ha immediatamente rimosso il tappo e ha bevuto un bel sorso."


translate italian day6_dv_652453a6:


    "In un attimo il suo viso è diventato tutto rosso e i suoi occhi sembravano pronti a saltar fuori dalle orbite."


translate italian day6_dv_bc888301:


    dv "Che… che cos'è questo?!"


translate italian day6_dv_17c09957:


    "Ha chiesto con voce stridula, mentre ritornava in sé."


translate italian day6_dv_a4a2a1fa:


    me "È la vera vodka…"


translate italian day6_dv_2f3f0691:


    "Sembrava che alla fine io avessi accettato la mia sconfitta."


translate italian day6_dv_38cbafd3:


    dv "Ma dove l'hai presa...?"


translate italian day6_dv_c5adf9aa:


    me "Pensavi davvero di aver rubato la bottiglia giusta dall'infermeria?"


translate italian day6_dv_cac1539b:


    "Ha annuito."


translate italian day6_dv_009a5837:


    me "Beh certo, la bottiglia era quella giusta, ma il contenuto…"


translate italian day6_dv_a99785e2:


    dv "Ragazzo, tu sì che sei astuto!"


translate italian day6_dv_eadf401a:


    "Aveva già iniziato a farfugliare."


translate italian day6_dv_ec4582f5:


    "Alisa si è avvicinata alla finestra, ha buttato fuori l'acqua sporca dal mio bicchiere, e l'ha riempito con la vera vodka."


translate italian day6_dv_3a7f0ff5_1:


    dv "Dai!"


translate italian day6_dv_413213a1:


    me "Non voglio!"


translate italian day6_dv_09bcc5f4:


    th "Davvero, questo non è né il momento né il luogo adatto."


translate italian day6_dv_d7cfce4d:


    dv "Bevilo! O dirò a tutti che hai rubato la bottiglia!"


translate italian day6_dv_348ce313:


    me "Ma sei stata tu!"


translate italian day6_dv_b144a585:


    dv "Beh, la vodka ce l'avevi tu."


translate italian day6_dv_20df12ec:


    me "Non puoi dimostrarlo!"


translate italian day6_dv_8e3540a1:


    dv "Pensi davvero che ne abbia bisogno?"


translate italian day6_dv_da707ec9:


    "Mi ha detto malignamente."


translate italian day6_dv_edfca763:


    "Insicuro sul da farsi, ho fissato il mio bicchiere."


translate italian day6_dv_8e14b6e7:


    th "Quello che importa è sbarazzarsi della vodka, e questo è solo uno dei modi per farlo…"


translate italian day6_dv_a1cabb40:


    "Ho fatto un grande respiro e ho ingurgitato l'intero contenuto del mio bicchiere con un unico rapido movimento."


translate italian day6_dv_a5f5245d:


    "Un istante più tardi me ne sono pentito."


translate italian day6_dv_257940d2:


    me "Hai qualcosa… da mangiare?!"


translate italian day6_dv_4e45e644:


    dv "Aspetta!"


translate italian day6_dv_0e2d45d5:


    "Ha detto con entusiasmo, e ha tirato fuori un'enorme salsiccia dal suo cassetto."


translate italian day6_dv_da1a8792:


    "L'ho strappata dalle sue mani e le ho dato un bel morso."


translate italian day6_dv_acf11e47:


    me "Ahh…"


translate italian day6_dv_9c8b084d:


    dv "Allora, com'è stato?"


translate italian day6_dv_bdd0a304:


    me "E me lo chiedi…{w} Non è che io beva superalcolici tutti i giorni, eh!"


translate italian day6_dv_2919a3a5:


    "Non capivo proprio come io avessi potuto accettare così facilmente."


translate italian day6_dv_9c8b084d_1:


    dv "Ebbene, com'è stato?"


translate italian day6_dv_183e652e:


    me "Come…{w} Come una gomma Orbit al sapore di vodka!"


translate italian day6_dv_e5ec287e:


    dv "Va bene, è tempo di un altro giro, allora!"


translate italian day6_dv_ece6c281:


    "Ha riaperto la bottiglia e ha riempito i due bicchieri a metà."


translate italian day6_dv_947b745d:


    me "Non credi che sia abbastanza?"


translate italian day6_dv_9e0a34e8:


    dv "Affatto!"


translate italian day6_dv_dc7ac58a:


    me "Beh, dal momento che abbiamo già iniziato…"


translate italian day6_dv_8778e051:


    "L'alcol ha cominciato a farsi sentire. Senza pensarci due volte, ho ingurgitato la vodka."


translate italian day6_dv_28cad17a:


    "Alisa era decisamente meno esperta – stava avendo qualche problema con il suo secondo bicchiere."


translate italian day6_dv_84399dec:


    dv "Ugh…{w} Come si…"


translate italian day6_dv_5f08e9f7:


    "Sembrava già ubriaca fradicia."


translate italian day6_dv_17750322:


    me "Se hai paura di scottarti – sta' lontano dal fuoco!"


translate italian day6_dv_3f99c8fe:


    "Ho detto allegramente, versandomene un altro po'."


translate italian day6_dv_02260da3:


    "Da qualche parte dentro di me ero consapevole del fatto che fosse ora di smetterla, ma ormai la pistola era già stata caricata."


translate italian day6_dv_4ff95f61:


    dv "E a me no?"


translate italian day6_dv_0cb8369f:


    "Alisa era indignata."


translate italian day6_dv_5097c292:


    me "Non ne hai avuto abbastanza?"


translate italian day6_dv_a4506eb9:


    dv "Affatto!"


translate italian day6_dv_d009fa63:


    "Mi ha strappato la bottiglia dalle mani e ha iniziato a bere direttamente da essa."


translate italian day6_dv_ea0035fe:


    me "Ehi!{w} Abbiamo dei bicchieri, sai!"


translate italian day6_dv_99af8ac3:


    dv "Che si fottano!"


translate italian day6_dv_6dee2c25:


    "Ha gridato, ed è scoppiata a ridere."


translate italian day6_dv_b13a1ec7:


    dv "Tutto questo è davvero forte!"


translate italian day6_dv_f6dadb64:


    "Ha detto, riprendendo fiato."


translate italian day6_dv_bddbfd2a:


    me "Certo, non è male."


translate italian day6_dv_49f40434:


    "Ho guardato la bottiglia – due terzi se n'erano già andati."


translate italian day6_dv_4fc941de:


    "Io ero decisamente ubriaco a quel punto, ma Alisa sembrava messa molto peggio."


translate italian day6_dv_c365b5cf:


    th "D'altra parte, n'è rimasta poca!"


translate italian day6_dv_f1f22231:


    "Ho sospirato, versando la vodka rimanente nei nostri bicchieri, metà a me e metà ad Alisa."


translate italian day6_dv_1ddb42b6:


    dv "E questo è tutto!"


translate italian day6_dv_ddc629f8:


    "Dopo aver pronunciato quelle parole ha gettato la bottiglia fuori dalla finestra."


translate italian day6_dv_4037402e:


    me "E se la trovassero?"


translate italian day6_dv_31a41270:


    dv "Oh, chi se ne frega!"


translate italian day6_dv_b4d6e8a8:


    "Abbiamo riso entrambi."


translate italian day6_dv_1e6410a5:


    me "Va bene allora. A tutte le signore presenti!"


translate italian day6_dv_f9f12001:


    "Ho grugnito e ho mandato giù l'ultimo sorso."


translate italian day6_dv_1db3dad4:


    dv "E a tutti i signori!"


translate italian day6_dv_4a63f2cd:


    "Lei ha seguito il mio esempio."


translate italian day6_dv_04ff19b4:


    dv "Uh, ho le vertigini…"


translate italian day6_dv_96ca161a:


    "Si è sbottonata un po' la camicia."


translate italian day6_dv_8f838576:


    "L'ho guardata attentamente."


translate italian day6_dv_4fce3e9d:


    th "È sempre stata così bella?{w} Oppure chiunque mi potrebbe sembrare bello a questo punto...?"


translate italian day6_dv_49ff7c76:


    "Qualunque cosa fosse, non sono riuscito a distogliere lo sguardo da lei."


translate italian day6_dv_11513fad:


    dv "Ehi, sembra che tu mi stia facendo delle lastre!"


translate italian day6_dv_93c166ce:


    "Ha riso."


translate italian day6_dv_44b2f075:


    me "Scusa…"


translate italian day6_dv_e18f175c:


    "Imbarazzato, ho fissato i miei piedi."


translate italian day6_dv_59610270:


    dv "Puoi guardare se proprio t'interessa."


translate italian day6_dv_06bac448:


    "Con qualche sforzo, ho aperto un occhio e ho visto che stava continuando a sbottonarsi la camicia."


translate italian day6_dv_f506e4ea:


    "Non riuscivo a pronunciare alcuna parola.{w} Inoltre, a che servono le parole in un momento del genere?"


translate italian day6_dv_a20cefa7_1:


    "..."


translate italian day6_dv_1f2532e5:


    "E poi è successo quello che doveva succedere."


translate italian day6_dv_ec3e3dbf:


    "In quel momento non ero in grado di pensare cosa fosse giusto o sbagliato."


translate italian day6_dv_3c562aeb:


    "E non volevo – mi sentivo bene."


translate italian day6_dv_a20cefa7_2:


    "..."


translate italian day6_dv_4d03f762:


    "La lampadina si è bruciata, o qualcuno di noi due aveva spento la luce, ad ogni modo la stanza è piombata nell'oscurità."


translate italian day6_dv_0a606dd1:


    "Mi sono guardato attorno e ho pensato a una cosa."


translate italian day6_dv_422a1788:


    me "Dov'è Ulyana?"


translate italian day6_dv_126253a8:


    dv "Cosa c'è, hai paura che qualcuno possa entrare e trovarci qui?"


translate italian day6_dv_2dc438e7:


    "Ha sorriso calorosamente."


translate italian day6_dv_2d3ba9c4:


    me "Potrebbe accadere…"


translate italian day6_dv_ae2894c9:


    dv "Le ho chiesto di trascorrere la notte nella stanza di un'altra ragazza."


translate italian day6_dv_4ff54a12:


    me "Aspetta, allora…{w} avevi pianificato tutto questo?"


translate italian day6_dv_8c03dbe0:


    dv "Beh, certamente non una cosa del genere…"


translate italian day6_dv_938d5502:


    "Mi ha guardato intensamente, ha sorriso e ha chiuso gli occhi."


translate italian day6_dv_c63ec5a4:


    me "Già…"


translate italian day6_dv_6c190789:


    "Ho fissato il soffitto per un po', e ben presto potevo sentire un respiro profondo e tranquillo – Alisa si era addormentata."


translate italian day6_dv_1899453f:


    "La testa ha iniziato a farmi male – l'alcool stava dando i suoi effetti."


translate italian day6_dv_8fbfda03:


    "La cosa migliore da fare in quel caso era seguire il suo esempio e dormire un po'.{w} Non sono nemmeno riuscito a concludere quel pensiero, che mi ero già addormentato."
# Decompiled by unrpyc: https://github.com/CensoredUsername/unrpyc
