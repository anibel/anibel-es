
translate italian day3_main1_c980a67d:


    "La notte mi ha lasciato una certa inquietudine. Mi sono svegliato esausto, sia fisicamente che mentalmente."


translate italian day3_main1_d58e9a94:


    "Mi succede sempre: le immagini sono molto vivide nei sogni, è come trovarsi in un campione d'incassi Hollywoodiano con una trama complessa, attori strapagati ed effetti speciali da milioni di dollari. E poi ti svegli la mattina senza ricordare niente."


translate italian day3_main1_5cd53aa6:


    "Questo è quello che stavo cercando di fare – ripristinare quelle immagini. Ma quella mia zona di memoria ormai era vuota – forse archiviata, o forse formattata."


translate italian day3_main1_cc6f7f6e:


    "Erano le sette di mattina, secondo l'orologio."


translate italian day3_main1_4b2cb0f9:


    th "Devo essermi svegliato presto oggi."


translate italian day3_main1_a9a851c3:


    "Olga Dmitrievna stava ancora dormendo, avvolta stretta nelle coperte."


translate italian day3_main1_2ee0953d:


    th "Avvolta così stretta...? Con questo caldo...? Bah..."


translate italian day3_main1_de284251:


    "Mi sono alzato, mi sono stirato e sono andato davanti allo specchio per darmi un'occhiata."


translate italian day3_main1_eb6077b0:


    th "È meglio se mi faccio la barba."


translate italian day3_main1_832ce63c:


    "Che strana idea. Prima due volte al mese erano abbastanza, ma adesso era come se il mio aspetto avesse importanza!"


translate italian day3_main1_4e0332f2:


    "Tuttavia, non ho trovato alcun rasoio."


translate italian day3_main1_cbc809e5:


    th "Perché mai mi dovrebbe servire, se sembro avere al massimo diciassette anni?"


translate italian day3_main1_eddcb947:


    th "Dunque...{w} La colazione inizia alle nove, quindi significa che ho ancora un sacco di tempo."


translate italian day3_main1_26c1194a:


    "Che avrei potuto impiegare per passeggiare e lavarmi, ad esempio."


translate italian day3_main1_4144add1:


    "L'acqua non era gelida come l'altro giorno."


translate italian day3_main1_289c3fa7:


    "Al contrario – la sua freschezza mi ha aiutato a svegliarmi e a riportarmi alla realtà (sempre che tutto questo potesse essere considerato reale)."


translate italian day3_main1_399d144c:


    "Il dentifricio in polvere non sembrava essere una cosa così antiquata, dopotutto. E non mi sembrava che fosse molto diverso dal comune dentifricio."


translate italian day3_main1_300a064a:


    "Quando sono tornato, Olga Dmitrievna era già sveglia."


translate italian day3_main1_d6087460:


    "Stava in piedi davanti allo specchio, aggiustandosi i capelli."


translate italian day3_main1_b8d13a2a:


    mt "Buongiorno, Semyon!"


translate italian day3_main1_3773033f:


    me "Giorno."


translate italian day3_main1_c0392c73:


    "A causa delle poche ore di sonno le mie orecchie stavano ancora fischiando, e i miei pensieri erano tutti aggrovigliati. Tuttavia non mi sono pentito di essermi svegliato così presto, invece di aver dormito per altre due o tre ore come al solito."


translate italian day3_main1_52b8b2dd:


    mt "Perché ti sei svegliato così presto?"


translate italian day3_main1_a3d19ed0:


    me "Non so, così…"


translate italian day3_main1_3e828ab9:


    mt "E ti sei dimenticato di nuovo il foulard. Lascia che ti…"


translate italian day3_main1_e3b2ca3e:


    me "Faccio da solo, non è un problema… Me lo metto mentre vado alla mensa."


translate italian day3_main1_74f457d8:


    mt "Non credo proprio, mettilo adesso!{w} Ti sei forse dimenticato che c'è l'allineamento prima della colazione?"


translate italian day3_main1_3ac43883:


    th "E come potrei scordarmelo?"


translate italian day3_main1_a9616761:


    me "Cosa faremo oggi all'allineamento?"


translate italian day3_main1_ac9273a5:


    mt "Vi illustrerò i piani della giornata."


translate italian day3_main1_a3da2eed:


    me "E quali sono i piani della giornata?"


translate italian day3_main1_689ac639:


    "Francamente, non mi importava di {i}cosa{/i} avrebbe fatto oggi la coraggiosa truppa di pionieri. Mi interessava piuttosto {i}dove{/i} la nostra impavida leader avrebbe mandato la sua truppa nelle prossime dodici-quindici ore.{w} Così forse avrei potuto evitare le linee nemiche."


translate italian day3_main1_045f8001:


    mt "Lo scoprirai all'allineamento!"


translate italian day3_main1_945c59bc:


    "Mi ha detto, sorridendo furbamente."


translate italian day3_main1_fb73d398:


    me "Olga Dmitrievna, sa, mi fa male lo stomaco – forse potrei…"


translate italian day3_main1_6e70eda4:


    me "Beh, sa…"


translate italian day3_main1_c0565634:


    me "Non dovrebbe essere un grosso problema se saltassi l'allineamento per una volta. Potrebbe non essere una bella cosa se non riuscissi a trattenermi proprio lì…"


translate italian day3_main1_923b9fdb:


    th "Ma da dove ho trovato tutto questo coraggio...?"


translate italian day3_main1_063c7cf2:


    mt "Ti do altri cinque minuti."


translate italian day3_main1_95a2da76:


    me "Credo che me ne serviranno di più.{w} Potrebbe dirmi i piani del giorno adesso, per favore?"


translate italian day3_main1_7ffb918a:


    me "Io posso provarci, ma se succederà qualcosa…"


translate italian day3_main1_bdf2ab42:


    mt "Oh, d'accordo!{w} Oggi abbiamo in programma la pulizia del territorio, l'organizzazione dei libri all'interno della biblioteca, e altri compiti minori. E, ovviamente, questa sera ci sarà il ballo."


translate italian day3_main1_fa2ec241:


    mt "Guarisci presto!"


translate italian day3_main1_dc6aa38c:


    mt "Dovrai partecipare alle attività del Campo oggi!"


translate italian day3_main1_585915f9:


    th "Come no!"


translate italian day3_main1_c2355535:


    me "Ma certo! E ora se non le spiace…"


translate italian day3_main1_41c48b54:


    mt "Forza, è ora di darsi una mossa!"


translate italian day3_main1_2f2e2be3:


    "Sono uscito dalla casetta e mi sono nascosto tra i cespugli, aspettando che Olga Dmitrievna se ne andasse."


translate italian day3_main1_8eee44c3:


    "A dire il vero, non avevo motivo di comportarmi così."


translate italian day3_main1_740a8290:


    "Ma qualcosa mi diceva che non avrebbe avuto senso partecipare all'allineamento."


translate italian day3_main1_a20cefa7:


    "..."


translate italian day3_main1_1466986b:


    "Dopo che la leader del Campo se n'è andata, sono tornato dentro e ho deciso di attendere la colazione lì."


translate italian day3_main1_24e4a223:


    th "Beh, non ho alcuna voglia di partecipare alle attività di oggi."


translate italian day3_main1_dfefa0e2:


    th "Pulire e organizzare – non sono cose che qualcuno nella mia situazione dovrebbe fare."


translate italian day3_main1_0ed74327:


    "Erano già passati due giorni, ed ero ancora al punto di partenza."


translate italian day3_main1_a8e4db15:


    "Tutta la gente del posto sembrava perfettamente normale. Non erano coinvolti in una cospirazione, né stavano tramando qualcosa con gli alieni, e parevano non considerare minimamente i viaggi nel tempo."


translate italian day3_main1_e9ea2720:


    "Certo, ognuno di loro aveva le sue peculiarità. Ma non erano di certo qualcosa di impossibile."


translate italian day3_main1_e9429608:


    "Se avessi incontrato questi pionieri nel mio mondo reale, li avrei considerati del tutto normali."


translate italian day3_main1_469fa83c:


    "A dire il vero mi sembravano più normali persino di me stesso."


translate italian day3_main1_32fd1d9f:


    "Questi pensieri parevano tutti logici, la cosa illogica era l'assenza di risposte."


translate italian day3_main1_097dcaf5:


    th "Gente normale in un luogo anormale."


translate italian day3_main1_2f89955c:


    th "Mi pare d'aver già sentito quella frase da qualche parte..."


translate italian day3_main1_23beae7b:


    th "Decine di pionieri (o anche di più) vivono le loro vite e fanno le loro cose giorno dopo giorno. Non capiscono che il mondo attorno a loro potrebbe non essere quello che sembra."


translate italian day3_main1_c2aba579:


    th "Ok.{w} Ma questo è il loro punto di vista.{w} Io invece non mi sento a mio agio qui al campo «Sovyonok», indipendentemente da come mi sembrano le persone."


translate italian day3_main1_83ea97da:


    th "L'importante adesso è capire se queste persone abbiano a che fare con tutto quello che mi è successo."


translate italian day3_main1_4427373a:


    th "Mi daranno finalmente qualche risposta?"


translate italian day3_main1_6d2752d6:


    "O avrei dovuto cercare la verità altrove?"


translate italian day3_main1_a20cefa7_1:


    "..."


translate italian day3_main1_ffb0123f:


    "Forse mi sono perso di nuovo nei miei pensieri – non c'era la solita folla affamata attorno alla mensa."


translate italian day3_main1_2fba02c8:


    "E neppure al suo interno."


translate italian day3_main1_fe31d110:


    "La maggioranza probabilmente aveva fatto colazione prima delle nove."


translate italian day3_main1_378c20a9:


    th "Meglio così. Meno persone significa più ossigeno a disposizione."


translate italian day3_main1_51afaa36:


    "Lena era seduta nell'angolo più remoto della mensa, raccogliendo pigramente con la sua forchetta una poltiglia che assomigliava vagamente al porridge."


translate italian day3_main1_1fcd8de0:


    "Fare colazione in sua compagnia sembrava essere una buona idea. Così avremmo potuto parlare in pace."


translate italian day3_main1_33a88c19:


    th "O almeno ci proveremo."


translate italian day3_main1_e70262c5:


    "Mi stavo avvicinando al suo tavolo quando qualcuno mi ha afferrato per la manica."


translate italian day3_main1_d960d8bf:


    "Era Zhenya, la bibliotecaria."


translate italian day3_main1_c2aade8d:


    mz "Prendi il tuo vassoio e vieni. Dobbiamo parlare."


translate italian day3_main1_51149e68:


    me "…"


translate italian day3_main1_be6dc4ed:


    mz "Che stai aspettando?"


translate italian day3_main1_0b5fe8db:


    "Ero un po' confuso."


translate italian day3_main1_1d988865:


    me "Perdonami, ma non ti sembra un po'… affrettato?"


translate italian day3_main1_7ff5889f:


    mz "Che c'è che non va? Prendi la tua colazione e siediti."


translate italian day3_main1_ab08d3e6:


    "Suppongo che lei considerasse il suo comportamento totalmente normale."


translate italian day3_breakfast_un_c5c4ca0e:


    "Ero sicuro che a Lena non sarebbe dispiaciuto se mi fossi seduto accanto a lei."


translate italian day3_breakfast_un_20c17916:


    mz "Aspetta un attimo!"


translate italian day3_breakfast_un_9a143b5c:


    me "Scusa, magari più tardi."


translate italian day3_breakfast_un_1643039e:


    "Mi sono liberato gentilmente dalla sua presa e sono andato verso l'angolo in fondo alla mensa."


translate italian day3_breakfast_un_a9a20700:


    "Zhenya stava gridando qualcosa alle mie spalle, ma ho cercato di ignorarla."


translate italian day3_breakfast_un_b5bcbe39:


    me "Buongiorno!"


translate italian day3_breakfast_un_5cbc2f90:


    "Dopo aver sentito la bibliotecaria gridare, Lena ha continuato a guardarmi furtivamente per un po'."


translate italian day3_breakfast_un_480fc425:


    un "Giorno…"


translate italian day3_breakfast_un_49f1d3e1:


    "Stranamente non è arrossita come al solito, invece ha sorriso."


translate italian day3_breakfast_un_80a13296:


    me "Posso sedermi qui? Vado a prendere la colazione e torno subito."


translate italian day3_breakfast_un_3d4036d2:


    un "Certo."


translate italian day3_breakfast_un_a20cefa7:


    "..."


translate italian day3_breakfast_un_ea39cb42:


    "Dopo un solo minuto mi trovavo già davanti a lei con alcuni piatti sul mio vassoio: il porridge, due uova in camicia, quattro fette di pane, una salsiccia bollita e un bicchiere di kompot."


translate italian day3_breakfast_un_9aeb930e:


    me "Buon appetito!"


translate italian day3_breakfast_un_626232a6:


    un "Grazie."


translate italian day3_breakfast_un_5a8d98a1:


    "Cercavo di mangiare il più educatamente possibile – senza masticare troppo rumorosamente, senza spargere pezzi di cibo o sbrodolarmi di kompot."


translate italian day3_breakfast_un_7bb18871:


    th "Non sembra troppo difficile riuscire a mangiare come un essere umano e non come un maiale!"


translate italian day3_breakfast_un_997be624:


    "Come sempre Lena era silenziosa, il che voleva dire che toccava a me rompere il ghiaccio."


translate italian day3_breakfast_un_c7fca932:


    me "Vai al ballo stasera?"


translate italian day3_breakfast_un_1aa04f80:


    un "Forse…"


translate italian day3_breakfast_un_34d1d8f6:


    "È rimasta in silenzio per un po'."


translate italian day3_breakfast_un_44be8128:


    un "E tu?"


translate italian day3_breakfast_un_799c0105:


    me "Non ho ancora deciso…"


translate italian day3_breakfast_un_911940f8:


    "Anche se non avevo proprio voglia di andarci."


translate italian day3_breakfast_un_cc5d1c46:


    "I balli non mi piacevano fin dai tempi della scuola…"


translate italian day3_breakfast_un_49e77139:


    un "Perché?"


translate italian day3_breakfast_un_3956a59c:


    me "Cosa intendi?"


translate italian day3_breakfast_un_121175cf:


    "La mia mente era ancora lontana, ai tempi della scuola."


translate italian day3_breakfast_un_d3f48c81:


    un "Perché non ci vuoi andare?"


translate italian day3_breakfast_un_949e4fbd:


    me "Non ho detto che non ci voglio andare…"


translate italian day3_breakfast_un_da258ccf:


    un "Ok allora."


translate italian day3_breakfast_un_19437dd5:


    me "Ma se vuoi invitarmi..."


translate italian day3_breakfast_un_153a3cb2:


    "Lena è arrossita e ha distolto lo sguardo."


translate italian day3_breakfast_un_d6e377c8:


    un "Beh, io non..."


translate italian day3_breakfast_un_f61b93da:


    me "Scusa, stavo solo scherzando."


translate italian day3_breakfast_un_aa3ce0f5:


    "Mi sono sentito in imbarazzo – probabilmente era meglio non fare certe {i}battute{/i}."


translate italian day3_breakfast_un_b993fafe:


    un "Va bene."


translate italian day3_breakfast_un_3198fcbf:


    "Mi ha detto con indifferenza, arrossendo ancora di più e concentrandosi sulla sua colazione."


translate italian day3_breakfast_un_32938f90:


    th "Perché mi sono fissato su quel ballo come se non ci fosse altro di cui parlare?{w} Aspetta! In realtà una cosa c'è..."


translate italian day3_breakfast_un_998716c3:


    me "Tra l'altro, per caso sai che cosa voleva dirmi Zhenya?"


translate italian day3_breakfast_un_d0ef9648:


    un "Eh?"


translate italian day3_breakfast_un_d9512f33:


    me "Voglio dire, quando sono entrato... voleva dirmi qualcosa."


translate italian day3_breakfast_un_66762623:


    un "Non lo so..."


translate italian day3_breakfast_un_855db9ee:


    me "Capisco."


translate italian day3_breakfast_un_67900051:


    "La conversazione sembrava aver raggiunto un vicolo cieco, quindi mi sono concentrato sul mio pasto."


translate italian day3_breakfast_un_359fdf19:


    "Lena ha finito prima di me."


translate italian day3_breakfast_un_82988e32:


    un "Devo andare. Ci vediamo più tardi!"


translate italian day3_breakfast_un_6384aca8:


    me "A dopo!"


translate italian day3_breakfast_mz_e36ce35f:


    "Non so perché, ma ho accettato il suo invito."


translate italian day3_breakfast_mz_25dd7e46:


    th "E se potessi scoprire qualcosa da lei?{w} Dopotutto è un topo di biblioteca."


translate italian day3_breakfast_mz_3159bf22:


    "Dopo un minuto ero già seduto davanti a Zhenya, con la mia colazione sul vassoio: del porridge, due uova, quattro fette di pane, una salsiccia e un bicchiere di kompot."


translate italian day3_breakfast_mz_9aeb930e:


    me "Buon appetito!"


translate italian day3_breakfast_mz_ca4481dc:


    mz "Grazie."


translate italian day3_breakfast_mz_8df9aaff:


    "Ho cercato di essere il più gentile possibile, e anche di mangiare in modo educato – masticando decentemente senza spargere pezzi di cibo o gocce di kompot."


translate italian day3_breakfast_mz_ebae22b0:


    me "Quindi, di cosa mi volevi parlare?"


translate italian day3_breakfast_mz_f4ef416b:


    mz "Oggi alla biblioteca…"


translate italian day3_breakfast_mz_f6feb39d:


    "E da quel punto non l'ho più ascoltata, perché subito mi sono venute in mente le parole di Olga Dmitrievna riguardo al programma di quest'oggi."


translate italian day3_breakfast_mz_63e91f2f:


    mz "…ricordatelo!"


translate italian day3_breakfast_mz_0e394660:


    me "Eh? Cosa?"


translate italian day3_breakfast_mz_8af6f3a4:


    mz "Ricordati di passare alla biblioteca dopo pranzo."


translate italian day3_breakfast_mz_7332342b:


    me "E perché?"


translate italian day3_breakfast_mz_486c6399:


    mz "Ma mi stavi ascoltando o no?"


translate italian day3_breakfast_mz_5a7a7577:


    me "No."


translate italian day3_breakfast_mz_9fb02d3f:


    "Ho ammesso, con sincerità."


translate italian day3_breakfast_mz_b4215fb3:


    "Il volto di Zhenya ha assunto una colorazione rossa, poi verde, e dopo ancora viola."


translate italian day3_breakfast_mz_198f044e:


    mz "Oggi! Dopo pranzo! Tu! Nella biblioteca! Capito, somaro?"


translate italian day3_breakfast_mz_a758b252:


    th "Somaro?{w} Che razza di insulto è?{w} Anche se un pioniere non dovrebbe mai imprecare, come potrei scordarmi questa regola?"


translate italian day3_breakfast_mz_0b0c4f9e:


    me "Oh, sì, certo! Perdonami! Ci puoi scommettere, ci sarò!"


translate italian day3_breakfast_mz_63573b0e:


    "A dire il vero non avevo intenzione di andare da nessuna parte oggi, ma se vuoi far parlare qualcuno, devi prima fare qualche sacrificio."


translate italian day3_breakfast_mz_55998771:


    me "Comunque, in che anno siamo adesso? Ho proprio perso il conto."


translate italian day3_breakfast_mz_42d07202:


    "Ho deciso di giocare in modo leale."


translate italian day3_breakfast_mz_f7f9fcdc:


    "Mi ha guardato in modo perplesso e mi ha detto:"


translate italian day3_breakfast_mz_8ff4b6fa:


    mz "Ma ti sei bevuto il cervello?"


translate italian day3_breakfast_mz_6303a3f4:


    me "A dire il vero, me lo sono bevuto già da qualche giorno."


translate italian day3_breakfast_mz_133cbf39:


    "Zhenya non mi ha risposto."


translate italian day3_breakfast_mz_b1214d8e:


    me "Allora, in che anno siamo?"


translate italian day3_breakfast_mz_085d124a:


    "Le ho chiesto di nuovo, sorridendo gentilmente."


translate italian day3_breakfast_mz_4899008d:


    mz "Senti, perché non ti fai un giro all'infermeria?"


translate italian day3_breakfast_mz_fddf5e90:


    me "Lì mi diranno in che anno siamo?"


translate italian day3_breakfast_mz_4686520d:


    mz "Oh certo! E molto altro!"


translate italian day3_breakfast_mz_63613035:


    "Zhenya si è alzata e se n'è andata verso l'uscita."


translate italian day3_breakfast_mz_6999c456:


    me "Aspetta, non hai finito la tua colazione…"


translate italian day3_breakfast_mz_e9ec16dc:


    "Ma sembrava che non avesse alcuna intenzione di tornare."


translate italian day3_main2_0c42e260:


    "Stavo finendo di masticare la mia salsiccia, che sembrava più un cavo telefonico."


translate italian day3_main2_d8c4da3f:


    me "Chissà se hanno internet qui?"


translate italian day3_main2_c0d3e816:


    me "Non credo."


translate italian day3_main2_7ce38afc:


    "Un pioniere che stava seduto al tavolo adiacente mi aveva sentito parlare da solo, e mi stava osservando. Gli ho sorriso e l'ho salutato con la mano."


translate italian day3_main2_cec89b26:


    "Dopo questo mio saluto amichevole, per qualche motivo, se n'è andato di fretta abbandonando il suo pasto."


translate italian day3_main2_b91a15cc:


    th "Basta mangiare – è ora di cercare indizi."


translate italian day3_main2_db7938e1:


    "Dopo qualche minuto sono arrivato all'infermeria."


translate italian day3_main2_80770b79:


    th "Non ho niente da fare lì."


translate italian day3_main2_495d074a:


    "L'infermiera sembrava premurosa, ma era meglio starle alla larga comunque."


translate italian day3_main2_0c5af512:


    "Una tale benevolenza è un po' spaventosa!"


translate italian day3_main2_4357f4cf:


    "Forse lei mi avrebbe detto qualcosa, ma non avevo il coraggio di entrare lì dentro."


translate italian day3_main2_7ce2c0d2:


    th "E comunque, non ho alcun motivo di entrarci.{w} Proprio nessuno."


translate italian day3_main2_f48ed40c:


    "Immerso nei miei pensieri, non avevo notato l'infermiera, che stava in piedi sulla veranda."


translate italian day3_main2_e091fcfe:


    cs "Ehilà… pioniere!"


translate italian day3_main2_b93477a9:


    "Si è avvicinata."


translate italian day3_main2_b306dd8f:


    cs "Stai male? Sei venuto per farti curare?"


translate italian day3_main2_23823de6:


    me "No, no… Io… stavo solo camminando qui attorno…"


translate italian day3_main2_10ca59a3:


    cs "Oh, vedo che sei uno scansafatiche."


translate italian day3_main2_d264f0d9:


    cs "Già che sei qui, avrei un compito importante per te."


translate italian day3_main2_4a52b166:


    "L'ho guardata storto."


translate italian day3_main2_54a8b79a:


    th "La parola «compito» non mi piace – sembra qualcosa di inevitabile."


translate italian day3_main2_82b55e26:


    "L'infermiera ha sorriso sprezzante."


translate italian day3_main2_972a2399:


    cs "Entrambi mi aiuterete a compilare un inventario dei medicinali che sono arrivati oggi."


translate italian day3_main2_35ca7b8b:


    th "Entrambi?{w} A chi si riferisce?{w} Forse ho capito male."


translate italian day3_main2_51c41ef6:


    th "Ma quale parola potrebbe essere confusa con «entrambi»?"


translate italian day3_main2_8e5e0396:


    un "Ehi ciao…"


translate italian day3_main2_e8479859:


    un "Ciao…"


translate italian day3_main2_3b501dc9:


    "Lena è spuntata da dietro l'infermiera."


translate italian day3_main2_f4ec6c0a:


    "Strano che io non l'avessi notata."


translate italian day3_main2_32f43013:


    cs "Venite qui dopo cena. Ho già spiegato tutto a Lena, ti dirà cosa dovrete fare."


translate italian day3_main2_1b0bb99e:


    th "Medicinali, eh? Arrivati oggi?"


translate italian day3_main2_edbbd70f:


    th "Olga Dmitrievna mi ha detto che il prossimo bus sarebbe passato solo tra qualche giorno."


translate italian day3_main2_513f14a5:


    me "E così abbiamo avuto visite oggi?"


translate italian day3_main2_d589ffac:


    cs "Sì, sono arrivati dalla città questa mattina. Perché me lo chiedi?"


translate italian day3_main2_0baf4d95:


    me "Per curiosità…"


translate italian day3_main2_29261639:


    cs "D'accordo… pioniere!{w} Allora stasera, dopo cena, verrai qui!"


translate italian day3_main2_918e5db5:


    un "Forse io…"


translate italian day3_main2_f1184b36:


    "Lena è rimasta in disparte per tutto il tempo."


translate italian day3_main2_5a680cf2:


    "Di certo era in grado di restare invisibile come un abile ninja."


translate italian day3_main2_0dac54fc:


    un "Non c'è problema, posso farlo da sola."


translate italian day3_main2_c4e56f03:


    "Strano, alla mensa mi è sembrata più vivace."


translate italian day3_main2_07ecb7bb:


    cs "Non credo proprio – ci sono un sacco di scatoloni!"


translate italian day3_main2_189f0241:


    cs "E un pioniere dev'essere sempre disponibile!"


translate italian day3_main2_92699031:


    cs "Non è forse così... pioniere?"


translate italian day3_main2_96567aee:


    "Mi ha chiesto con un sorriso poco gentile."


translate italian day3_main2_fb4b4310:


    "Non avevo intenzione di trascorrere l'intera serata a fare una cosa del genere, e inoltre avevo cose più importanti da fare."


translate italian day3_main2_b7a89246:


    "Ma rifiutare sarebbe stato davvero inappropriato."


translate italian day3_main2_e73ac8a5:


    "Se non fosse per Lena che stava lì vicino, avrei sicuramente trovato il modo di evitare la cosa."


translate italian day3_helpaccept_752f2744:


    cs "Ottimo!{w} È così che un pioniere dovrebbe rispondere!"


translate italian day3_helpaccept_512b33c1:


    "E dopo quelle parole mi sono sentito ancora più vicino al cosiddetto «Sii preparato», il motto degli scout."


translate italian day3_helpaccept_eb225364:


    cs "Adesso potete andare."


translate italian day3_helpaccept_85a9dc87:


    "Ho guardato Lena, che stava ancora fissando il terreno."


translate italian day3_helpaccept_83ee98b6:


    "Suppongo che con tutto quel tempo che trascorreva a fissare per terra, lei avesse ormai imparato ogni singolo dettaglio della vita e delle abitudini dei vari insetti."


translate italian day3_helpaccept_80cb7fbb:


    "E in più, Lena amava leggere, quindi probabilmente leggeva anche libri di biologia, botanica e simili."


translate italian day3_helpaccept_84a80626:


    th "Forse vuole diventare un'entomologa?"


translate italian day3_helpaccept_a20cefa7:


    "..."


translate italian day3_helpaccept_9a9ab591:


    un "E adesso dove andrai?"


translate italian day3_helpaccept_e6c70fa7:


    "Le sue parole mi hanno fatto emergere dai miei pensieri oscuri."


translate italian day3_helpaccept_928a1e98:


    "Siamo rimasti in piedi nella piazza per un po', e le mie fantasie sulla vita degli insetti erano solo un pretesto per evitare quella situazione imbarazzante."


translate italian day3_helpaccept_553ade04:


    th "Comunque, perché la considero imbarazzante?"


translate italian day3_helpaccept_4f92bcbe:


    un "Non dimenticarti allora, d'accordo?"


translate italian day3_helpaccept_56e472ea:


    me "Dimenticarmi cosa?"


translate italian day3_helpaccept_7ae819c0:


    un "Beh, stasera… Dopo cena… All'infermeria…"


translate italian day3_helpaccept_0734476b:


    "Nel suo sguardo ho notato un atteggiamento scontroso."


translate italian day3_helpaccept_d7ab165c:


    th "No, è assolutamente impossibile!"


translate italian day3_helpaccept_41dde6d3:


    me "Va bene, va bene. Appena dopo cena sarò al tuo servizio."


translate italian day3_helpaccept_31f1800b:


    "Ormai mi era scappata quella frase – nel sentirla Lena è arrossita ancora di più."


translate italian day3_helpaccept_3d2683cd:


    "Dovevo trovare il modo per alleviare quella situazione."


translate italian day3_helpaccept_e57a438b:


    me "Perché l'infermiera ti ha chiesto di farlo?"


translate italian day3_helpaccept_dc5220db:


    un "Non lo so.{w} Stavo seduta su una panchina a leggere un libro, quando è arrivata e…"


translate italian day3_helpaccept_3d5bff62:


    th "È intelligente chiederlo a una persona affidabile, che non potrebbe mai rifiutare."


translate italian day3_helpaccept_5364658a:


    me "Ok.{w} Va bene allora, a dopo cena…"


translate italian day3_helpaccept_37b80610:


    un "Ok.{w} Allora io vado."


translate italian day3_helpaccept_4bfb0b4b:


    me "Sì, certo!"


translate italian day3_helpaccept_92254f5c:


    "Si è incamminata ed è sparita dietro alla sede dei circoli, e io sono rimasto lì in piedi per un po'."


translate italian day3_helpreject_28e3d09b:


    th "Forse non avrei dovuto mentire."


translate italian day3_helpreject_78a68a86:


    "È meglio pensarci due volte prima di dire qualcosa."


translate italian day3_helpreject_64ea43f2:


    cs "Ma davvero? Ed esattamente cosa ti ha chiesto di fare?"


translate italian day3_helpreject_90328db4:


    "Le cose stavano prendendo una brutta piega, ma avevo deciso di andare fino in fondo."


translate italian day3_helpreject_20aa6bf2:


    me "La sua stanza…{w} Mi ha chiesto di fare le pulizie, di ordinare le cose e così via."


translate italian day3_helpreject_eb1f3b24:


    me "Scommetto sa che casino c'è lì dentro!"


translate italian day3_helpreject_71a1959c:


    cs "Ma certo, per caso ti ha anche chiesto di sistemarle il guardaroba?"


translate italian day3_helpreject_bd7955c4:


    "L'infermiera mi ha lanciato uno sguardo così intenso che la sconfitta di Napoleone a Waterloo era una cosa da poco se comparata al mio attuale fiasco."


translate italian day3_helpreject_4d02b8d1:


    "Stavo per dirle qualcosa, ma lei ha proseguito."


translate italian day3_helpreject_bd364f86:


    cs "Va bene… pioniere.{w} Ci arrangeremo senza di te."


translate italian day3_helpreject_48cb1418:


    me "Mi dispiace davvero. Ma, vedete…"


translate italian day3_helpreject_a1643280:


    "Ho portato il mio sguardo su Lena."


translate italian day3_helpreject_bd547c7b:


    "Lei continuava a fissare il terreno, arrossendo sempre più."


translate italian day3_helpreject_28645b37:


    un "Non c'è problema… Me la caverò…"


translate italian day3_helpreject_084c94a3:


    me "Va bene allora."


translate italian day3_helpreject_ffe91023:


    "È sceso un silenzio imbarazzante."


translate italian day3_helpreject_6c088e5b:


    "Volevo solo sprofondare nel terreno per la vergogna di essermi inventato quella stupida bugia che doveva aver offeso Lena."


translate italian day3_helpreject_1c49e0e4:


    un "Beh, ci vediamo allora."


translate italian day3_helpreject_670845c1:


    me "Ci vediamo!"


translate italian day3_helpreject_5ae42b21:


    "L'infermiera stava ancora sorridendo dispettosamente."


translate italian day3_helpreject_d6986490:


    "Mi sono girato e mi sono allontanato, quasi correndo."


translate italian day3_helpreject_5c0a5a05:


    "A dire il vero avevo proprio voglia di correre lontano, ma il buonsenso mi ha suggerito di camminare normalmente."


translate italian day3_helpreject_53287076:


    "Mi sono ripreso dopo aver raggiunto la piazza."


translate italian day3_main3_16660e40:


    th "E adesso?"


translate italian day3_main3_212cd6fc:


    "Ho deciso di passare dalla mia stanza, per prendere il mio telefono."


translate italian day3_main3_a2b246f4:


    th "Non so dove andrò a finire oggi, ma un orologio a portata di mano è sempre utile!"


translate italian day3_main3_a387e44b:


    th "Anche se sarebbe più comodo un orologio da polso."


translate italian day3_main3_2aff1bdf:


    "Improvvisamente ho sentito una melodia strana, che sovrastava i tipici rumori del Campo."


translate italian day3_main3_f9785632:


    "L'ho ascoltata con attenzione.{w} Sembrava una chitarra elettrica."


translate italian day3_main3_adb705e3:


    "Tre semplici accordi che si ripetevano, nient'altro."


translate italian day3_main3_f1b3a7a5:


    "Tuttavia, quella melodia era calda, come se passasse attraverso un amplificatore."


translate italian day3_main3_93557c84:


    th "Potrebbe esserci un tale lusso qui?"


translate italian day3_main3_a012ce31:


    "Il suono proveniva chiaramente dal palco."


translate italian day3_main3_da2b4460:


    th "Chi potrebbe essere?"


translate italian day3_main3_32f4a81d:


    th "Chiunque esso sia, chi se ne importa? Ho cose più importanti da fare."


translate italian day3_house_of_mt_c93a9c6d:


    "Sono tornato nella stanza della leader."


translate italian day3_house_of_mt_81f57dce:


    "Olga Dmitrievna stava sdraiata pigramente sul suo letto, leggendo un libro."


translate italian day3_house_of_mt_ca064f09:


    mt "Semyon! Che ci fai qui?"


translate italian day3_house_of_mt_ff169076:


    th "Potrei farle la stessa domanda."


translate italian day3_house_of_mt_ba187c2f:


    me "Mi sono dimenticato una cosa…{w} Così sono tornato."


translate italian day3_house_of_mt_63ff0148:


    mt "Non c'eri all'allineamento!"


translate italian day3_house_of_mt_12ae07e8:


    me "Oh, già. Mi perdoni…"


translate italian day3_house_of_mt_c5cabca5:


    mt "Va bene.{w} Questo non nega il fatto che ora tu debba fare qualcosa di utile."


translate italian day3_house_of_mt_cea8428c:


    "È quello che stavo pensando anch'io.{w} Ma non credo avessimo la stessa idea sul significato di «utile»."


translate italian day3_house_of_mt_e473fa2f:


    me "Ad esempio?"


translate italian day3_house_of_mt_2410adae:


    mt "Potresti andare alla piazza, aiutando nelle pulizie. Slavya sta supervisionando i lavori lì."


translate italian day3_house_of_mt_4bc77a00:


    th "Strano, sono appena passato di lì ma non ho visto nessuno."


translate italian day3_house_of_mt_bce5b940:


    mt "O potresti fare un salto dai ragazzi al circolo di elettronica – so che serviva loro una mano."


translate italian day3_house_of_mt_280f15cf:


    th "Oh no, di nuovo quei «Padri dell'Invenzione»!"


translate italian day3_house_of_mt_ad452bb7:


    mt "O ancora, potresti dare una mano al circolo sportivo."


translate italian day3_house_of_mt_e2d85953:


    me "Cosa stanno facendo lì?"


translate italian day3_house_of_mt_1ab1a7d4:


    mt "Stanno ristrutturando il campo da calcio: stanno aggiustando le panche, cambiando le reti, e così via."


translate italian day3_house_of_mt_00d98ab0:


    "Francamente, nessuna di quelle opzioni faceva per me."


translate italian day3_house_of_mt_3f6ade2d:


    th "Ma perché deve sempre caricarmi di lavoro?"


translate italian day3_house_of_mt_14075888:


    th "Scherzi a parte, eccetto Slavya, nessuno qui sembra alzare un dito, anzi li vedo sempre correre, divertirsi, riposarsi."


translate italian day3_house_of_mt_88a88f60:


    "Tutti i suoi tentativi di caricarmi di lavoro sembravano avere un unico scopo – ostacolare la mia ricerca della verità."


translate italian day3_house_of_mt_c75c7820:


    "Era come se tutte le mie profonde e importanti domande esistenziali fossero spazzate via con una scopa, come polvere, assieme a tutta l'altra spazzatura."


translate italian day3_house_of_mt_11998600:


    "Nel mio caso, tenere la mente un po' occupata mi avrebbe fatto bene, ma come un paziente che soffre di cancrena e continua a rimandare l'amputazione, io avrei mantenuto le mie convinzioni."


translate italian day3_house_of_mt_c8e476b7:


    me "Sa, avevo altri piani."


translate italian day3_house_of_mt_f11d83c9:


    mt "Oh, davvero? Che piani?"


translate italian day3_house_of_mt_8efbf7e2:


    "Mi ha chiesto, alzandosi dal letto."


translate italian day3_house_of_mt_f883183e:


    me "Beh…"


translate italian day3_house_of_mt_eec772da:


    th "Non posso semplicemente dirle quello che sto pensando di fare!"


translate italian day3_house_of_mt_b155d7d8:


    th "Ma d'altra parte, perché no?"


translate italian day3_house_of_mt_e7621f5a:


    th "No, è troppo rischioso!"


translate italian day3_house_of_mt_d4332ab2:


    th "Almeno adesso sembro aver trovato un fragile equilibrio, e niente sembra minacciarmi.{w} O almeno così mi pare."


translate italian day3_house_of_mt_4c7fb1b8:


    mt "Come pensavo!"


translate italian day3_house_of_mt_508a7e16:


    "A {i}cosa{/i} stesse pensando esattamente era un mistero."


translate italian day3_house_of_mt_3a32c862:


    mt "Forse non hai ancora capito quanto sia importante partecipare alla vita sociale.{w} È l'unico modo per diventare un pioniere modello, che sia di esempio per gli altri."


translate italian day3_house_of_mt_34d07d39:


    "Non avevo intenzione di sorbirmi un altro dei suoi soliti discorsi, e così ho accettato una delle opzioni."


translate italian day3_house_of_mt_3a33dc3e:


    th "Ogni tanto le risposte alle nostre domande si trovano nei luoghi più improbabili.{w} O almeno è quello che spero..."


translate italian day3_house_of_mt_0137337f:


    mt "Allora? Devo scegliere io per te?"


translate italian day3_house_of_mt_d6a8a466:


    "Mi ha chiesto in tono autoritario."


translate italian day3_house_of_mt_a0d17775:


    me "Posso decidere da solo, grazie..."


translate italian day3_house_of_mt_1553fbf7:


    "Ho borbottato, e ho abbandonato la stanza."


translate italian day3_square_b65de720:


    "Bisogna sempre scegliere il male minore, così dicono – beh, in questo caso il minore dei tre."


translate italian day3_square_0685b6b9:


    "Slavya non era affatto un male – quindi la mia scelta era ovvia."


translate italian day3_square_c3fd147b:


    "Qualsiasi persona normale desidera vivere in un luogo pulito, ma la pulizia della casa mi è sempre sembrata come gli esercizi in palestra – è vero, ti fa bene, ma non fa per me."


translate italian day3_square_bd3a88ba:


    "Tuttavia, l'idea di pitturare le panche o di aiutare i due futuri geni della scienza russa (o sovietica?) mi piaceva ancora meno."


translate italian day3_square_52919e7f:


    "C'erano una dozzina di pionieri nella piazza."


translate italian day3_square_b54a584e:


    th "Da dove sono sbucati, mi chiedo?"


translate italian day3_square_70ff76da:


    "Mi sono avvicinato a Slavya."


translate italian day3_square_1a8f3935:


    me "Ehi ciao!"


translate italian day3_square_c2326fc9:


    sl "Oh ciao! Sei venuto a dare una mano?"


translate italian day3_square_eac909f4:


    me "Beh, non di mia spontanea volontà, ovviamente…"


translate italian day3_square_f0f5042d:


    "Ho ripensato alla cosa."


translate italian day3_square_01e9adbb:


    sl "Capisco."


translate italian day3_square_85d5b601:


    sl "Prendi questa scopa, la tua zona è quella attorno al monumento."


translate italian day3_square_59f34355:


    "Francamente non mi pareva che ci fosse bisogno di spazzare lì. Mi sembrava tutto abbastanza pulito."


translate italian day3_square_8c05f3f7:


    "Anche se c'erano ancora delle carte per terra."


translate italian day3_square_a20cefa7:


    "..."


translate italian day3_square_4bf5b11e:


    "Dopo aver spazzato per un po', sono tornato da Slavya, che stava seduta su una panchina a riposare."


translate italian day3_square_3d0d567b:


    me "Che bella giornata, eh…?"


translate italian day3_square_93a88d17:


    sl "Sì, anche se fa un po' troppo caldo."


translate italian day3_square_2a6651bc:


    "Si è schermata gli occhi con la mano e ha guardato il cielo."


translate italian day3_square_d1bfa7c3:


    me "Sei proprio una lavoratrice instancabile!"


translate italian day3_square_b61ed746:


    sl "Ah, e dai! È solo che mi piace aiutare gli altri."


translate italian day3_square_69d18c31:


    me "È una cosa buona."


translate italian day3_square_5c7b7c48:


    sl "E tu invece?"


translate italian day3_square_08572abe:


    me "Io cosa?"


translate italian day3_square_7b636361:


    sl "Beh, sembra che il lavoro sociale sia per te un fardello?"


translate italian day3_square_d76f93fa:


    me "Beh, probabile."


translate italian day3_square_70f37a53:


    sl "E perché?"


translate italian day3_square_7479b07e:


    me "Davvero non saprei…"


translate italian day3_square_03e6aa1b:


    th "Seriamente – spero non creda davvero che le possa rivelare tutte le mie idee a riguardo!"


translate italian day3_square_5c2a445c:


    th "Potrebbe annegare in tutto quel mare di idee, ci mancherebbe!"


translate italian day3_square_b87ff550:


    sl "Forse non ti piace stare in compagnia?"


translate italian day3_square_b9c3fa63:


    me "È possibile."


translate italian day3_square_f5936064:


    "C'era un fondo di verità nelle parole di Slavya.{w} Suppongo che lei fosse brava a capire le persone."


translate italian day3_square_553e30c8:


    "O almeno, a capire me."


translate italian day3_square_cff06c81:


    th "Tuttavia, come ci riesce, dato che nemmeno io riesco a capire me stesso?"


translate italian day3_square_6466d398:


    sl "Che cosa farai dopo che questa esperienza sarà finita?"


translate italian day3_square_e54b39e9:


    me "Cosa vuoi dire?"


translate italian day3_square_15860af3:


    sl "Beh, vuoi andare all'università? O vuoi imparare un mestiere?"


translate italian day3_square_08e5bdfd:


    "Ero già un gran maestro in tutti i miei mestieri – guardare cartoni animati e navigare in internet."


translate italian day3_square_862a00b6:


    me "Non ho ancora deciso… e tu?"


translate italian day3_square_65f5243e:


    sl "Abbiamo una piccola fattoria, così vorrei aiutare i miei genitori lì."


translate italian day3_square_42782762:


    "Che strano – una fattoria privata nell'Unione Sovietica, dove tutto dovrebbe essere di tutti?"


translate italian day3_square_a5764d60:


    "Ma non ho voluto indagare oltre."


translate italian day3_square_c14f0a08:


    sl "E i tuoi genitori che mestiere fanno?"


translate italian day3_square_f74fad8a:


    "I miei genitori non sono un buon argomento di cui parlare."


translate italian day3_square_2975c9e6:


    "Non perché non siano a posto. Anzi è il contrario, loro sono delle persone meravigliose."


translate italian day3_square_0a57324e:


    "Ma non era il momento giusto per parlare di loro."


translate italian day3_square_884ea735:


    me "Mio padre lavora nel comitato cittadino, e mia madre è un'insegnante."


translate italian day3_square_0130c38d:


    "Non è esattamente così.{w} È più una mezza verità."


translate italian day3_square_a9edc837:


    sl "Forte."


translate italian day3_square_5451282f:


    "Sembrava davvero che considerasse forte la cosa."


translate italian day3_square_907b6d20:


    me "Sì, probabile…"


translate italian day3_square_14c9829e:


    "La conversazione ha raggiunto un vicolo cieco, e quindi ho cercato di distogliere lo sguardo da Slavya – guardando giù ai suoi piedi, poi verso il cielo, poi lì attorno."


translate italian day3_square_d0928e48:


    sl "Sai, credo che anche tu te la caverai."


translate italian day3_square_edee7f76:


    "Mi ha detto pensierosa."


translate italian day3_square_e4df3286:


    th "Cosa intende dire esattamente?"


translate italian day3_square_4752f785:


    th "E che significa quel «anche tu»?"


translate italian day3_square_1e5c4921:


    me "G-grazie…"


translate italian day3_square_87af141e:


    "Ho mormorato."


translate italian day3_square_e18b6de3:


    me "Ma cosa intendi dire?"


translate italian day3_square_2f5cbfd7:


    sl "Mi pare che tu sia troppo pessimista, a volte."


translate italian day3_square_be54772f:


    th "Non le sembra un po troppo, ehm, diretto?"


translate italian day3_square_2edc6114:


    me "Forse."


translate italian day3_square_a6743adf:


    sl "Ma te la caverai!"


translate italian day3_square_2edc6114_1:


    me "Forse."


translate italian day3_square_723945e3:


    "Quella conversazione mi ha messo a disagio."


translate italian day3_square_80cd1196:


    "Dopotutto, non ha detto nulla di particolare."


translate italian day3_square_fbf07be0:


    "A dire il vero, chiunque avrebbe potuto trarre le stesse conclusioni di Slavya."


translate italian day3_square_e331d636:


    "Eppure non era la prima volta che pareva riuscisse a guardarmi nell'anima."


translate italian day3_square_a3f06af0:


    me "Dai, continuiamo a pulire."


translate italian day3_square_f021de41:


    sl "Aha."


translate italian day3_square_31b44837:


    "Ha fatto un sorriso e ha ripreso la sua scopa."


translate italian day3_square_5475d679:


    "Non avrei mai pensato che lo spazzare potesse essere così piacevole."


translate italian day3_square_68594816:


    "No, non ero arrabbiato per quella conversazione. Al contrario, è stato bello sentirmelo dire da lei."


translate italian day3_square_a5d8ba12:


    "Per la prima volta qualcuno stava parlando della mia vita senza criticarmi, senza darmi un qualche stupido consiglio basato solo sulla propria esperienza, senza cercare di scavare nella mia mente, e senza giungere a conclusioni affrettate."


translate italian day3_square_e172c3fb:


    "Era bello avere il sostegno di qualcuno."


translate italian day3_square_c7b3226b:


    th "No, non esattamente!"


translate italian day3_square_01f6d7a7:


    "Era bello avere il sostegno {i}di Slavya{/i}."


translate italian day3_square_a20cefa7_1:


    "..."


translate italian day3_square_5daea6b4:


    "Poco dopo la sirena ci ha chiamati per il pranzo. Ho posato la scopa con un sospiro di sollievo e un senso di realizzazione, e siamo andati alla mensa."


translate italian day3_clubs_7484a246:


    "Beh, forse non così gigante."


translate italian day3_clubs_f63f81c0:


    "È stata davvero una decisione difficile."


translate italian day3_clubs_2a9a2232:


    th "Infatti, qual è il punto di trascorrere del tempo extra con degli eccentrici appassionati di fantascienza della prima metà del Ventesimo secolo?"


translate italian day3_clubs_2344f7dc:


    th "E se invece riuscissi a farmi dire qualcosa di utile da quei due?"


translate italian day3_clubs_f8ea823f:


    th "O almeno potrei provarci."


translate italian day3_clubs_f58fc437:


    "Stando in piedi sulla soglia del circolo, ho esitato per un istante. Finalmente mi sono liberato dei miei dubbi, ho aperto la porta e sono entrato."


translate italian day3_clubs_a57a7173:


    "Electronik e Shurik erano chini sul tavolo, studiando qualcosa con attenzione."


translate italian day3_clubs_247d8ad8:


    me "Ehilà, cibernetici! Come andiamo?"


translate italian day3_clubs_0a24ed23:


    "Ho cercato di essere il più amichevole possibile."


translate italian day3_clubs_994b5e60:


    el "Ciao! Ti stavamo aspettando!"


translate italian day3_clubs_2b8da8a8:


    me "Ma guarda, che strano..."


translate italian day3_clubs_ed2501a3:


    "Electronik infatti poteva essere considerato «l'Oracolo»."


translate italian day3_clubs_736892e2:


    me "Allora, di cosa avete bisogno?"


translate italian day3_clubs_b2eaaab0:


    sh "Ci aiuterai a completare il nostro robot."


translate italian day3_clubs_8b36978a:


    me "Non credo proprio.{w} Non ci capisco niente di queste cose."


translate italian day3_clubs_df6d570c:


    sh "Non preoccuparti! Ti spiegheremo tutto noi.{w} Tanto più ci odierai, tanto più imparerai…"


translate italian day3_clubs_0e64dff5:


    el "Forza, iniziamo!"


translate italian day3_clubs_14702694:


    "Ha esordito Electronik, con allegria."


translate italian day3_clubs_034978a3:


    me "Capisco, ma vorrei parlare di qualcosa di più importante, ragazzi. Dobbiamo parlare di cose serie…{w} Avrei una domanda, e voi sembrate avere la risposta."


translate italian day3_clubs_51e049ed:


    sh "E quale sarebbe la domanda?"


translate italian day3_clubs_db02021d:


    "Mi ha chiesto incredulo."


translate italian day3_clubs_3dcaa86c:


    me "Credete sia possibile viaggiare nel tempo?"


translate italian day3_clubs_d54b6c5c:


    el "Perché ci stai chiedendo una cosa del genere?"


translate italian day3_clubs_c7fd889f:


    "Electronik all'improvviso ha assunto un'espressione seria."


translate italian day3_clubs_09834bc7:


    me "Non è solo una mia idea…"


translate italian day3_clubs_1b1a3349:


    me "Ieri ho letto un libro alla biblioteca – «La Macchina del Tempo» di H.G. Wells. Dovreste averlo letto anche voi. E quindi, ecco, ci stavo pensando."


translate italian day3_clubs_2db74799:


    sh "O-oh…"


translate italian day3_clubs_a1121260:


    el "Perché, vuoi provare a viaggiare nel futuro, per vedere come sarà la vita?"


translate italian day3_clubs_9cd1fcea:


    me "No, non esattamente.{w} Sono più interessato ai viaggi indietro nel tempo."


translate italian day3_clubs_684146a1:


    el "Perché?"


translate italian day3_clubs_01e55b39:


    me "Non lo so.{w} Perché no…?"


translate italian day3_clubs_e63df87d:


    me "Allora, voi cosa ne pensate?"


translate italian day3_clubs_7094c1fe:


    sh "La teoria della relatività generale ipotizza l'esistenza dei ponti di Einstein-Rosen, meglio conosciuti come «tunnel spaziali».{w} Ma non ci capiresti granché in ogni caso."


translate italian day3_clubs_4d434542:


    th "Scommetto di no."


translate italian day3_clubs_fa38dcf0:


    sh "Comunque, con una tale supposizione incontreremmo un mare di paradossi."


translate italian day3_clubs_d7d34f50:


    sh "Ad esempio, se tu viaggiassi indietro nel tempo e uccidessi il te stesso del passato, allora significherebbe che il {i}tu{/i} del presente non potrebbe esistere."


translate italian day3_clubs_14901a5e:


    me "Beh… Pare proprio così…"


translate italian day3_clubs_c54945d0:


    el "In poche parole, è tutto troppo poco scientifico."


translate italian day3_clubs_99f562b4:


    me "Capisco, ma se fosse possibile in qualche modo...?"


translate italian day3_clubs_999acc60:


    me "Voglio dire, mi servirebbe una qualche macchina, un dispositivo, o un programma per poter viaggiare nel passato?"


translate italian day3_clubs_fc3a598d:


    me "O sarebbe sufficiente addormentarsi in un luogo e svegliarsi in un altro tempo e un'altra realtà?"


translate italian day3_clubs_722901ae:


    sh "Purtroppo la scienza moderna non ha ancora le risposte che stai cercando."


translate italian day3_clubs_14c45b6b:


    th "Ma guarda! È lo stesso tipo di frase che potevo leggere sulle riviste di «Tecnologia-Gioventù».{w} Ne ho qualche copia sugli scaffali della mia stanza."


translate italian day3_clubs_855db9ee:


    me "Capisco."


translate italian day3_clubs_4ca8b273:


    th "Sembra che venire qui non sia stata una buona idea…"


translate italian day3_clubs_84c5e8c5:


    sh "Tuttavia, supponendo che le leggi attualmente conosciute della fisica non siano del tutto corrette…{w} O anche se queste leggi fossero corrette ma non ne conoscessimo tutti gli aspetti…"


translate italian day3_clubs_a81295d2:


    sh "Allora sarebbe certamente possibile."


translate italian day3_clubs_2ad79933:


    me "Ok…"


translate italian day3_clubs_d061ee8b:


    el "Oppure potrebbe essere opera, ad esempio, di una qualche razza più evoluta, che ha una conoscenza superiore delle leggi della natura."


translate italian day3_clubs_2ad79933_1:


    me "Ok…"


translate italian day3_clubs_d6791c6b:


    sh "Sarebbero in grado di costruire una macchina del tempo o di viaggiare nel tempo in qualche altro modo."


translate italian day3_clubs_1fe7cd27:


    me "Ok..."


translate italian day3_clubs_fde5f662:


    th "Quindi, vuol dire che potrei essere stato spedito nel passato dai fratelli maggiori della razza umana?"


translate italian day3_clubs_738b4f20:


    th "Un'idea interessante.{w} E non è poi così illogica, a dire il vero."


translate italian day3_clubs_ec5fec4e:


    me "E come si potrebbe trovare questi… gli esseri di cui state parlando… se ci si trovasse già nel passato?"


translate italian day3_clubs_3d1c4a13:


    el "E come diavolo credi che potremmo saperlo?!"


translate italian day3_clubs_e6875f9c:


    "Entrambi hanno riso forte."


translate italian day3_clubs_d66197e0:


    me "Beh, grazie ragazzi!"


translate italian day3_clubs_385627a7:


    "Stavo per andarmene."


translate italian day3_clubs_4469b6cb:


    el "Ehi, aspetta! E il nostro robot...?"


translate italian day3_clubs_a54d7085:


    me "Sono sicuro che ve la caverete anche senza di me!"


translate italian day3_clubs_ebde4c68:


    "Le teorie dei giovani cibernetici erano abbastanza logiche ma, ahimè, ero ancora al punto di partenza nella mia ricerca della verità."


translate italian day3_clubs_aaeb79f3:


    "Ho passeggiato per un po' per il Campo e alla fine mi sono diretto verso la mensa."


translate italian day3_playground_us_8f643492:


    "Speravo che se avessi finto di aiutarli al circolo sportivo, allora avrei potuto liberarmi di tutto il lavoro."


translate italian day3_playground_us_0eacc803:


    th "Sicuramente hanno una marea di iscritti e se la caverebbero anche senza un paio di braccia in più."


translate italian day3_playground_us_cf36ec08:


    "Comunque, sarà meglio dare un'occhiata e vedere come stanno le cose al campo sportivo – almeno per sapere cosa sta succedendo là."


translate italian day3_playground_us_1ea33f5c:


    th "Giusto per avere qualcosa da dire nel caso in cui Olga Dmitrievna scoprisse che il suo «pioniere modello» si libera delle responsabilità e trascura le attività socialmente utili!"


translate italian day3_playground_us_994a3de0:


    th "Non si sa mai. Posso immaginarmi punizioni peggiori di un pestaggio in una riunione di partito..."


translate italian day3_playground_us_19538e61:


    "E una tale punizione non faceva parte dei miei piani."


translate italian day3_playground_us_d1f23116:


    "Dopo essere arrivato al campo, quello che vedevo non erano dei lavori di riparazione, di pulizia o di pittura, ma una partita di calcio."


translate italian day3_playground_us_5c2f927e:


    "Stavano giocando in cinque contro sei."


translate italian day3_playground_us_6a412919:


    "Ho guardato meglio i giocatori e ho riconosciuto Ulyana."


translate italian day3_playground_us_bb01375d:


    th "Non voglio interferire, mi limiterò a guardare."


translate italian day3_playground_us_4d2c583d:


    "Le due squadre erano palesemente sbilanciate."


translate italian day3_playground_us_3156dd24:


    "Una squadra era formata esclusivamente da ragazzini di circa dodici anni."


translate italian day3_playground_us_826e23c2:


    "L'altra squadra invece era di soli adolescenti."


translate italian day3_playground_us_2cec2c18:


    "E inoltre, questi ultimi avevano un giocatore in più – Ulyana."


translate italian day3_playground_us_9cd2564c:


    "Oh bene, ma una ragazza che vantaggio può dare alla squadra?"


translate italian day3_playground_us_e419209b:


    "Poco dopo ho capito di essermi sbagliato."


translate italian day3_playground_us_8eabf611:


    "Ulyana aveva una buona tecnica di dribbling, scavalcando i difensori uno dopo l'altro."


translate italian day3_playground_us_ec740a50:


    "Di certo le mancava il concetto di lavoro di squadra, ma in questo caso non le serviva affatto – riusciva a segnare una rete dopo l'altra."


translate italian day3_playground_us_4ef98874:


    "Mi sono avvicinato al campo."


translate italian day3_playground_us_ad1edcfd:


    us "Ehi! Gioca con noi! A loro manca un giocatore."


translate italian day3_playground_us_c1521ece:


    me "Beh, suppongo che questo si possa considerare come «attività socialmente utile»!"


translate italian day3_playground_us_c8c094f8:


    us "Abbiamo già finito con i lavori!"


translate italian day3_playground_us_59c94eb5:


    "Ha protestato Ulyana, con una nota di risentimento."


translate italian day3_playground_us_c5377880:


    "Ho guardato nuovamente il campo sportivo – le panche erano state appena pitturate, e alle porte erano state messe delle reti nuove."


translate italian day3_playground_us_f94073b8:


    th "Come sono riusciti a fare tutto questo?"


translate italian day3_playground_us_c9ad2b53:


    us "Dai, entra nella loro squadra!"


translate italian day3_playground_us_c387cb1e:


    th "Beh, dato che ho rifiutato l'altra volta, dovrei accettare adesso."


translate italian day3_playground_us_fa4c2e93:



    nvl clear
    "Palla al centro. Iniziamo."


translate italian day3_playground_us_231641a0:


    "Al primo tentativo sono riuscito a mandare la palla nell'angolo superiore della rete avversaria con un tiro delicato."


translate italian day3_playground_us_4e23a665:


    "Dopotutto non era così difficile segnare – il campo era grande al massimo una cinquantina di metri, e il portiere avversario non riusciva nemmeno a toccare la traversa."


translate italian day3_playground_us_6f9f7610:


    "Dopo un po' eravamo già in pareggio, nonostante la mia squadra fosse stata indietro di sette o otto reti quando ero entrato in campo."


translate italian day3_playground_us_f50e5ca2:


    "Per correttezza c'è da dire che anche la squadra di Ulyana ha segnato un paio di goal con me in campo."


translate italian day3_playground_us_676b6f90:


    "Più precisamente, li ha segnati lei."


translate italian day3_playground_us_4c72f6da:


    "Non si può di certo dire che lei avesse il piede di Ronaldinho o la precisione di Beckham – la sua tattica consisteva nel calciare la palla con la punta del piede verso spazi vuoti, e slanciarsi verso la porta avversaria."


translate italian day3_playground_us_0a5728b2:



    nvl clear
    "Tuttavia, quella tattica sembrava funzionare. Io non ero in grado di controllare ogni avversario e i miei compagni di squadra non mi erano di grande aiuto."


translate italian day3_playground_us_d2f46bc1:


    "Ovviamente giocavo meglio di tutti loro e teoricamente avrei potuto segnare quante volte volessi, ma non mi sembrava corretto.{w} Conta il partecipare, non il vincere, giusto?"


translate italian day3_playground_us_6cc66158:


    "Ma dopo l'annuncio di Ulyana:"


translate italian day3_playground_us_0de2793e:


    us "Calci di rigore!{w} Chi segna vince la partita!"


translate italian day3_playground_us_36f04bc6:


    "Non avevo intenzione di perdere."


translate italian day3_playground_us_002ee4a4:


    "Al momento la mia squadra era in vantaggio per 11:9, e quindi non avevo motivo di oppormi."


translate italian day3_playground_us_1c08886f:


    us "Tu stai in porta, io tiro. Poi ci scambiamo."


translate italian day3_playground_us_bcaae21b:



    nvl clear
    me "Un tiro per ognuno?"


translate italian day3_playground_us_df6dbf74:


    us "Sì."


translate italian day3_playground_us_04bd3ea8:


    "Mi sono messo in porta e mi sono concentrato."


translate italian day3_playground_us_db151b99:


    "Ha preso la rincorsa e…{w} l'ha tirata dritta tra le mie mani."


translate italian day3_playground_us_57a622de:


    us "Non vale!"


translate italian day3_playground_us_16bdda4a:


    me "Cosa non vale?"


translate italian day3_playground_us_098f4867:


    us "Tirerò un'altra volta."


translate italian day3_playground_us_3065208e:


    me "Ok, fa' pure."


translate italian day3_playground_us_d602bb2d:


    "Le ho fatto un sorriso."


translate italian day3_playground_us_a6e809e4:


    "Il suo secondo tiro sarebbe stato più utile nel rugby."


translate italian day3_playground_us_57923b1f:


    me "Vuoi provare di nuovo?"


translate italian day3_playground_us_76b2fe88:


    nvl clear


translate italian day3_playground_us_5ebb10b9:


    us "Assolutamente no!"


translate italian day3_playground_us_393056bc:


    "Mi ha risposto, sbuffando."


translate italian day3_playground_us_f88907e2:


    us "Ma non riuscirai mai a segnare contro di me!"


translate italian day3_playground_us_f4d2ad6b:


    me "Lo vedremo…"


translate italian day3_playground_us_05c10dae:


    "Sono riuscito a metterla dentro con un tiro preciso nell'angolo inferiore destro della porta."


translate italian day3_playground_us_9f646b2e:


    "Ulyana non si è mossa nemmeno."


translate italian day3_playground_us_94dc722a:


    "Ad ogni modo, non sarebbe cambiato nulla – i tiri come quello erano imprendibili."


translate italian day3_playground_us_34ad2df9:


    me "Sembra che la mia squadra abbia vinto."


translate italian day3_playground_us_a01972f6:


    "È rimasta in silenzio e mi ha guardato con risentimento."


translate italian day3_playground_us_24a977a7:


    "Dovevo alzarle il morale in qualche modo."


translate italian day3_playground_us_f5bed6f9:


    me "Non prendertela!{w} Io ho solo…"


translate italian day3_playground_us_e3b0eb4b:


    "Ed ecco che la sirena della mensa ha risuonato nell'ambiente circostante, indicando che il pranzo era pronto."


translate italian day3_playground_us_f6bba3b9:


    us "D'accordo, pareggerò i conti con te un'altra volta!"


translate italian day3_playground_us_a66acadc:


    "Si è allontanata verso la mensa ridendo e salutandomi con la mano."


translate italian day3_playground_us_19a2ba8b:


    "Ho palleggiato per un po' e poi l'ho seguita."


translate italian day3_stage_dv_57fadfbd:


    "Arrivato al palco, ho scorto la sagoma di Alisa su di esso."


translate italian day3_stage_dv_14e3da9e:


    "Quella ragazza ce la stava mettendo tutta – con gli occhi chiusi e con un piede appoggiato sulla cassa audio, dondolando al ritmo della musica."


translate italian day3_stage_dv_ebaf2447:


    "Non riuscivo a riconoscere la melodia, ma ero sicuro di averla già sentita da qualche parte."


translate italian day3_stage_dv_bbd1630b:


    th "Qualcosa del rock sovietico, immagino."


translate italian day3_stage_dv_b91c885c:


    "Ero in grado di elencare una dozzina di gruppi che suonavano quel genere, ma non ero un esperto."


translate italian day3_stage_dv_abb4c6d0:


    "Quella canzone sembrava piuttosto semplice, consisteva in soli tre accordi."


translate italian day3_stage_dv_61c07cfb:


    "Probabilmente pure io sarei riuscito a suonarla, dopo un po' di pratica."


translate italian day3_stage_dv_3ae0fcac:


    th "Mi chiedo dove Alisa abbia imparato a suonare."


translate italian day3_stage_dv_3e58d153:


    "Nell'Unione Sovietica quel genere di musica non aveva mai goduto del rispetto popolare."


translate italian day3_stage_dv_923112f9:


    "La futura rockstar non mi aveva ancora notato."


translate italian day3_stage_dv_3aea360d:


    "Era come se stesse cercando di diventare una cosa sola con la musica, di risuonare in ogni nota, in ogni terzina."


translate italian day3_stage_dv_d8f81d0f:


    "Di colpo i grandi cantanti rock degli anni Ottanta mi sono tornati in mente – molti di loro erano devoti alla musica come lei."


translate italian day3_stage_dv_a08c8857:


    th "Se solo indossasse un abito più adatto a quel periodo, invece di quella uniforme da pioniere, sicuramente si potrebbe notare la differenza."


translate italian day3_stage_dv_8ec8402b:


    "La sua {i}performance{/i} stava per terminare."


translate italian day3_stage_dv_2a480da6:


    "Suonando gli ultimi accordi, finalmente Alisa si è accorta della mia presenza."


translate italian day3_stage_dv_13716aa5:


    dv "Ti è piaciuto?"


translate italian day3_stage_dv_4f67d38e:


    "Non sembrava così sorpresa di vedermi."


translate italian day3_stage_dv_4041176b:


    me "Sì, è stato forte."


translate italian day3_stage_dv_944e8574:


    dv "Come se tu fossi in grado di fare di meglio!"


translate italian day3_stage_dv_495c9ac7:


    "Ha sorriso astutamente."


translate italian day3_stage_dv_149c1b07:


    me "Non ho mai detto che sarei in grado di farlo meglio.{w} Non sono bravo a suonare la chitarra."


translate italian day3_stage_dv_65c958c7:


    dv "Non ne dubito."


translate italian day3_stage_dv_bd025218:


    me "Già."


translate italian day3_stage_dv_d16a7088:


    "Non sapendo cos'altro aggiungere, mi stavo preparando per andarmene."


translate italian day3_stage_dv_f72c72fe:


    dv "Aspetta."


translate italian day3_stage_dv_ad02f53a:


    me "Cosa c'è?"


translate italian day3_stage_dv_4657233d:


    dv "Sai già della festa di stasera?"


translate italian day3_stage_dv_f14dc8aa:


    me "Credevo che si trattasse di un ballo…"


translate italian day3_stage_dv_f48c6409:


    dv "Ballo, festa – che differenza fa?"


translate italian day3_stage_dv_823e3ce8:


    "Ha aggrottato la fronte."


translate italian day3_stage_dv_c6a323d5:


    me "Sì, ne sono al corrente, perché?"


translate italian day3_stage_dv_a67a2d53:


    dv "Hai intenzione di andarci?"


translate italian day3_stage_dv_2625e9c4:


    "Dopo avermelo chiesto, si è allontanata da me."


translate italian day3_stage_dv_0e943fd7:


    me "Non ne sono sicuro… E tu?"


translate italian day3_stage_dv_2e685a48:


    dv "Cosa dovrei fare io lì? Osservare una mandria di idioti?"


translate italian day3_stage_dv_30498730:


    "C'era un fondo di verità nelle sue parole, ma sentire un tale tono di disprezzo mi ha sorpreso."


translate italian day3_stage_dv_a94386e7:


    "Era chiaro che non fosse l'idea del ballo quella che la infastidiva."


translate italian day3_stage_dv_977aad9c:


    me "Ma perché?"


translate italian day3_stage_dv_a35f4984:


    dv "Cosa vuoi dire con «perché»?"


translate italian day3_stage_dv_99ad6855:


    me "Beh, perché non vuoi venire?{w} Le feste da ballo dovrebbero essere divertenti…"


translate italian day3_stage_dv_c217ed8b:


    dv "Lo credi davvero?"


translate italian day3_stage_dv_868730ed:


    "Mi ha chiesto, infastidita."


translate italian day3_stage_dv_9d80b9cf:


    me "Non lo so.{w} Ma credevo che tu…"


translate italian day3_stage_dv_86c3ec28:


    dv "Non mi sono mai piaciute."


translate italian day3_stage_dv_0d425fdb:


    "Mi ha risposto, interrompendomi."


translate italian day3_stage_dv_c0fab6ae:


    dv "Non c'è proprio nulla da fare in quelle feste!"


translate italian day3_stage_dv_97523377:


    me "Capisco…{w} E allora cosa farai stasera?"


translate italian day3_stage_dv_a2c6cc1c:


    "Le ho chiesto, solo per portare avanti la conversazione."


translate italian day3_stage_dv_81a1f427:


    dv "Continuerò a esercitarmi."


translate italian day3_stage_dv_2c8d8df5:


    me "Per cosa?"


translate italian day3_stage_dv_096cd639:


    dv "Per la canzone, coglione! L'hai pure sentita!"


translate italian day3_stage_dv_d4729704:


    "Alisa sembrava davvero fuori di sé. Qualche altra domanda sbagliata e avrei dovuto fuggire per salvarmi la pelle."


translate italian day3_stage_dv_5f54b3cd:


    me "E chi l'ha scritta?"


translate italian day3_stage_dv_b4427906:


    dv "Io!"


translate italian day3_stage_dv_2d4780ec:


    me "E così, l'hai scritta tutta tu?"


translate italian day3_stage_dv_5977fdff:


    dv "Tutta io!"


translate italian day3_stage_dv_67492ba0:


    me "Forte…"


translate italian day3_stage_dv_4933fe62:


    "Uno strano silenzio ha fluttuato nell'aria."


translate italian day3_stage_dv_621c7bd3:


    me "Ok allora…"


translate italian day3_stage_dv_caeeba52:


    dv "Non vuoi ascoltarla?"


translate italian day3_stage_dv_98aef877:


    me "Non l'ho appena ascoltata?"


translate italian day3_stage_dv_0433bfa6:


    dv "Non è quello che intendo!"


translate italian day3_stage_dv_e16a3b7b:


    "Ha messo il broncio."


translate italian day3_stage_dv_83ff5116:


    dv "La canzone intera! Questa è solo una bozza per adesso."


translate italian day3_stage_dv_0d499a44:


    me "Ah… D'accordo, allora fammela sentire…"


translate italian day3_stage_dv_2af74aa2:


    dv "Non adesso."


translate italian day3_stage_dv_9e500e36:


    me "Beh, allora non farlo…"


translate italian day3_stage_dv_46a31379:


    "Non capivo cosa volesse da me."


translate italian day3_stage_dv_3564e101:


    dv "Allora non vuoi che la suoni."


translate italian day3_stage_dv_1594019e:


    th "Mi sembra frustrata a causa della mia risposta."


translate italian day3_stage_dv_9a66cc72:


    "Ero già pronto a ricevere una chitarra in testa o qualcosa di peggio."


translate italian day3_stage_dv_d7b3342b:


    "Ma vedevo solo un'espressione addolorata sul suo viso."


translate italian day3_stage_dv_02a64e1d:


    me "Sì che lo voglio – te l'ho detto."


translate italian day3_stage_dv_57a1e5c0:


    dv "Allora vieni qui stasera – la suonerò per te."


translate italian day3_stage_dv_421da051:


    th "«Per me»?{w} Che tenera."


translate italian day3_stage_dv_964231c7:


    me "Ma stasera c'è il ballo."


translate italian day3_stage_dv_c9fb6705:


    dv "Hai detto che non ci andrai!"


translate italian day3_stage_dv_42ff6b7c:


    th "Non ho detto questo…"


translate italian day3_stage_dv_2c3eb69e:


    "Francamente non avevo voglia di andarci, ma rischiare di finire nei guai con DvaChe non era una buona idea."


translate italian day3_stage_dv_117c8fe1:


    me "A Olga Dmitrievna non piacerà questa cosa…"


translate italian day3_stage_dv_73cb60c5:


    dv "Che vada al diavolo!"


translate italian day3_stage_dv_c8090db3:


    "Ha gridato Alisa, iniziando a innervosirsi."


translate italian day3_stage_dv_7f329f3b:


    "Preferisco scegliere la folle chitarra di Dvachevskaya piuttosto che il ridicolizzarmi alla festa di ballo."


translate italian day3_stage_dv_0d0c3fa8:


    dv "E bravo ragazzo."


translate italian day3_stage_dv_1f246e2c:


    "Ha sorriso in modo strano."


translate italian day3_stage_dv_19538e61:


    "Non era esattamente quello che avevo pianificato."


translate italian day3_stage_dv_2567c047:


    dv "Oh, sparisci, non è che importi più di tanto neppure a me!"


translate italian day3_stage_dv_412caa87:


    "Ha tirato su col naso e si è allontanata, offesa."


translate italian day3_stage_dv_a97da226:


    "Mi stavo chiedendo se avessi preso la decisione giusta quando all'improvviso la sirena della mensa mi ha interrotto, chiamando i pionieri per il pranzo."


translate italian day3_stage_dv_9256eb27:


    "Ho guardato in direzione della mensa."


translate italian day3_stage_dv_20b3b612:


    th "Suppongo sia ora di andare – la fame è una spina tagliente."


translate italian day3_stage_dv_402e53da:


    "Mi sono voltato di nuovo, intento a chiamare Alisa affinché venisse con me."


translate italian day3_stage_dv_c0edcd63:


    "Ma ho avuto l'impressione che fosse un po' triste."


translate italian day3_stage_dv_be5bcccb:


    me "Ogni musicista dovrebbe sapere quando è ora di smettere."


translate italian day3_stage_dv_3e1d421a:


    "Ho borbottato sottovoce."


translate italian day3_main4_a20cefa7:


    "..."


translate italian day3_main4_e3a9b50b:


    "«Chi marcia insieme fianco a fianco? È la nostra squadra di pionieri!»"


translate italian day3_main4_9086ffe8:


    "I pionieri {i}fianco a fianco{/i} solitamente marciavano verso la mensa, qui al campo «Sovyonok»."


translate italian day3_main4_ffd49d36:


    "Dovevo trovare un posto tranquillo dove nessuno mi potesse disturbare, come invece era accaduto l'altra volta."


translate italian day3_main4_45805593:


    th "Così almeno potrò mangiare in pace!"


translate italian day3_main4_5b7d20b3:


    "E affinché ciò fosse possibile, non sarei dovuto arrivare per ultimo."


translate italian day3_main4_3d31ce74:


    th "Ma di una cosa sono certo: tutto ciò che accade qui non dipende dalla mia volontà…"


translate italian day3_main4_4cb70312:


    "La mensa era decisamente affollata."


translate italian day3_main4_7168ef23:


    "Olga Dmitrievna stava davanti all'ingresso, con l'occhio vigile come quello di un falco."


translate italian day3_main4_764d9df7:


    mt "Allora Semyon, hai lavorato sodo oggi?"


translate italian day3_main4_aac8720f:


    me "Abbastanza sodo."


translate italian day3_main4_4e3031c8:


    mt "Ben fatto, davvero ben fatto! Ma la parte più difficile deve ancora arrivare!"


translate italian day3_main4_f1d5cd91:


    th "Non ho dubbi…"


translate italian day3_main4_63b5e296:


    mt "Bene, siediti lì accanto alle ragazze."


translate italian day3_main4_4cd2fcde:


    "Mi ha indicato il tavolo accanto al pilastro.{w} Slavya, Ulyana e Lena erano sedute lì."


translate italian day3_main4_65e51c2d:


    th "Non male come compagnia.{w} O almeno, non la peggiore..."


translate italian day3_main4_74860e53:


    "Ho preso il mio pasto e sono andato da loro."


translate italian day3_main4_4bf10d5d:


    me "Vi spiace se mi siedo qui?"


translate italian day3_main4_a4520622:


    "Mi sono sentito un po' stupido nel chiederglielo, dato che comunque non c'erano più posti liberi."


translate italian day3_main4_ae35202b:


    sl "Siediti pure!"


translate italian day3_main4_ae6727cd:


    us "Accomodati."


translate italian day3_main4_2622f621:


    "Lena è rimasta in silenzio."


translate italian day3_main4_fe3ea323:


    "Oggi il pranzo consisteva in un piatto di boršč (forse con dentro un po' di carne, ma non ne sono sicuro), del pollame (probabilmente un «gallus domesticus») con patate fritte, e un tradizionale bicchiere di kompot."


translate italian day3_main4_6e7c603f:


    "La cucina del posto mi piaceva sempre più."


translate italian day3_main4_b6d69ff6:


    "Credo di essere giunto alla conclusione di non avere comunque alcuna scelta. E quindi sarebbe stato inutile lamentarsi."


translate italian day3_main4_123f8387:


    th "Grazie a Dio almeno c'è {i}qualcosa{/i} da mangiare."


translate italian day3_main4_b15bd6e7:


    sl "Vieni al ballo stasera?"


translate italian day3_main4_595d7b8c:


    me "Non lo so."


translate italian day3_main4_6e25c9b3:


    th "Anche se ho già fissato un appuntamento con Alisa..."


translate italian day3_main4_4c1600f4:


    us "Verrà sicuramente! Non ha nient'altro da fare!"


translate italian day3_main4_99ce3e5f:


    "Ha detto Ulyana, allegramente."


translate italian day3_main4_56e1d2cc:


    me "Tu ci andrai di sicuro…"


translate italian day3_main4_aa1aa469:


    us "Ma certo! Come potrei perdermi l'occasione di vedere quanto sarai ridicolo."


translate italian day3_main4_15dfe923:


    "In effetti aveva ragione, così ho deciso di non risponderle."


translate italian day3_main4_b78c6e26:


    me "E tu invece?"


translate italian day3_main4_3329e7d9:


    "Ho chiesto a Lena."


translate italian day3_main4_31b01f67:


    un "Sì…"


translate italian day3_main4_521b6831:


    "Mi ha risposto, concisamente."


translate italian day3_main4_56d548d4:


    sl "Visto? Allora dovresti andarci anche tu."


translate italian day3_main4_f6a30a37:


    "Mi ha detto Slavya, come se non avessi altra scelta."


translate italian day3_main4_e75e0db2:


    us "Ricordati di indossare il tuo cappotto."


translate italian day3_main4_0033c883:


    "A quanto pare Ulyana era talmente compiaciuta della sua battuta che ha fatto una grande risata."


translate italian day3_main4_bb5ceb0f:


    "Ma io davvero non avevo niente da indossare al ballo."


translate italian day3_main4_006252b0:


    "Il mio guardaroba consisteva semplicemente nell'uniforme da pioniere e nei vestiti invernali, che sarebbero stati inappropriati persino per la sera."


translate italian day3_main4_99eeab2f:


    me "E tu come ti vestirai, buffona?"


translate italian day3_main4_0063fafa:


    us "Se-gre-to!"


translate italian day3_main4_85dd239b:


    me "Sarà un vestitino per bambini dell'asilo?"


translate italian day3_main4_8d42266c:


    "Ulyana è diventata rossa dalla rabbia, pare che fossi riuscito a offenderla."


translate italian day3_main4_90fa481a:


    us "No, indosserò una tuta di contenimento, per evitare di prendere un'infezione da te!"


translate italian day3_main4_405ea5ee:


    me "Chissà a quale tipo di infezione ti riferisci."


translate italian day3_main4_a840c516:


    sl "Ragazzi, basta! Non litigate!"


translate italian day3_main4_1c4b960b:


    us "L'idiozia, ovviamente!"


translate italian day3_main4_59986520:


    "Ulyana sembrava nuovamente molto compiaciuta della sua, secondo lei, «brillante» battuta."


translate italian day3_main4_7312ace7:


    me "Sai, se hai già l'influenza non puoi prendere il raffreddore."


translate italian day3_main4_dbe6fc9f:


    "Giocare in due è più divertente."


translate italian day3_main4_4d08faa9:


    us "Cosa vorresti insinuare?"


translate italian day3_main4_8d6a94af:


    me "Oh niente, assolutamente niente…"


translate italian day3_main4_2ed6c63f:


    "Ho distolto lo sguardo, sorridendo."


translate italian day3_main4_b155806b:


    us "Intendi dire che...?"


translate italian day3_main4_a852076f:


    "È diventata di nuovo rossa dalla rabbia."


translate italian day3_main4_36af1581:


    me "Non intendo dire niente."


translate italian day3_main4_e7059468:


    un "Ragazzi…"


translate italian day3_main4_94c9b804:


    "Dato che è stata Lena ad intervenire, forse era davvero il momento di smetterla."


translate italian day3_main4_e4c12ca4:


    us "Te la sei cercata! Ti faccio vedere io!"


translate italian day3_main4_b9d24980:


    me "Vedere cosa? Che finalmente crescerai un po'?"


translate italian day3_main4_5fea1e70:


    "Invece di rispondere, Ulyana ha afferrato il suo piatto pieno di boršč e l'ha versato sulla mia testa."


translate italian day3_main4_9f238d85:


    "Questo gioco stava prendendo una brutta piega…"


translate italian day3_main4_670b85c3:


    me "Oh, brutta piccola…!"


translate italian day3_main4_874c2400:


    "È saltata dalla sedia e ha provato a fuggire."


translate italian day3_main4_3d220a8d:


    "Ma questa volta non ce l'ha fatta – sono riuscito ad afferrarla per il braccio."


translate italian day3_main4_e4c890fc:


    th "Ehm ok, e adesso che faccio? Non posso semplicemente sbatterle la testa sul tavolo!"


translate italian day3_main4_8f1d5041:


    "Questa pantomima è durata qualche secondo."


translate italian day3_main4_8811fef6:


    "Improvvisamente, Ulyana ha afferrato un bicchiere pieno di kompot e me l'ha schizzato in faccia."


translate italian day3_main4_3958434a:


    "E così è riuscita a liberarsi dalla mia presa."


translate italian day3_main4_7e7c5066:


    "Si è precipitata verso il bancone, e l'ho inseguita."


translate italian day3_main4_7321633c:


    "Tutto quel putiferio ha causato il rovesciamento di parecchi tavoli, una marea di piatti rotti, cinque pionieri feriti, e ci ha lasciati totalmente esausti."


translate italian day3_main4_b1e8f257:


    th "Una sorta di pareggio."


translate italian day3_main4_17b22f81:


    th "Un pareggio combattuto.{w} Forse un po' troppo combattuto."


translate italian day3_main4_e6d45acc:


    "Siamo rimasti uno di fronte all'altra, cercando di riprendere fiato."


translate italian day3_main4_0224f2b4:


    me "Dimmi che non ti comporterai più così!"


translate italian day3_main4_54810cc5:


    us "E tu?!"


translate italian day3_main4_23a87547:


    "Olga Dmitrievna è apparsa alle nostre spalle."


translate italian day3_main4_2948bc41:


    "In effetti, un tale trambusto non poteva di certo passare inosservato."


translate italian day3_main4_501e435e:


    mt "Ma bene, siete soddisfatti adesso?"


translate italian day3_main4_a3227502:


    "La sua voce era calma, ma ero sicuro che si trattava solo della quiete prima della tempesta."


translate italian day3_main4_9799cd00:


    mt "E adesso chi pulisce tutto questo casino?!"


translate italian day3_main4_643530f6:


    "Come avevo previsto."


translate italian day3_main4_a71a3e52:


    mt "Chi, mi chiedo, chi?!"


translate italian day3_main4_cc05f4a5:


    us "Lui!"


translate italian day3_main4_6e0e9ab6:


    "Ha risposto Ulyana, sicura di sé."


translate italian day3_main4_8cc8ea72:


    me "Lei!"


translate italian day3_main4_078c536a:


    "Ho protestato io, in tono meno sicuro."


translate italian day3_main4_221ac838:


    mt "Entrambi!"


translate italian day3_main4_cd7733bf:


    "E così la leader del Campo ha messo fine al nostro battibecco."


translate italian day3_main4_abca6dac:


    "Sinceramente, non credevo di avere più colpe di lei."


translate italian day3_main4_d4b025ce:


    "Anche se Ulyana non sembrava mostrare il minimo senso di colpa."


translate italian day3_main4_3297e1dc:


    us "Bah!{w} Io non farò nulla! Non ho tempo per queste cose!"


translate italian day3_main4_79a78d45:


    us "È tutta colpa sua, ha iniziato lui!"


translate italian day3_main4_fa057752:


    me "Non è vero!"


translate italian day3_main4_0f7ff14d:


    us "Oh sì che è vero!"


translate italian day3_main4_179e42ee:


    mt "Adesso basta con queste stupidaggini!"


translate italian day3_main4_97932981:


    mt "Semyon, vai a prendere una scopa, un secchio, degli stracci, insomma, quel genere di cose, nell'armadietto, e lo stesso vale anche per te!!!"


translate italian day3_main4_ccad0a84:


    "Ha guardato Ulyana con uno sguardo talmente infuocato che mi è un po' dispiaciuto per la povera ragazza."


translate italian day3_main4_f438658b:


    mt "Tu!!!{w} Inizia immediatamente a raccogliere i piatti rotti!"


translate italian day3_main4_097f4a0a:


    "Olga Dmitrievna ha fatto un profondo respiro e ha proseguito:"


translate italian day3_main4_12c1e19a:


    mt "Non fai altro che combinare guai!{w} Quante volte te l'ho ripetuto…?"


translate italian day3_main4_051185fa:


    "Ho deciso di saltare la lezione e sono andato verso l'armadietto accanto all'uscita."


translate italian day3_main4_2ffc3634:


    "All'improvviso ho avuto una grande idea – perché non fuggire proprio adesso?"


translate italian day3_main4_27e1f3fe:


    "È vero, Olga Dmitrievna mi aveva già beccato, ma non ero io il colpevole."


translate italian day3_main4_9c178a5f:


    "Certamente non ero quel {i}pioniere modello{/i} che cercava di farmi diventare, ma in confronto a Ulyana…"


translate italian day3_main4_4ec22d66:


    "Comunque, la colpa principale era sua."


translate italian day3_main4_87ca6467:


    th "Non ho tempo da perdere qui, a pulire tutto questo casino.{w} Devo cercare le risposte!"


translate italian day3_main4_bfb5e7fb:


    "«La verità è là fuori» mi è apparsa nella mente."


translate italian day3_library_sl_ccebe3fc:


    "La soluzione è venuta da sé."


translate italian day3_library_sl_9cad1570:


    "Mentre Olga Dmitrievna stava rimproverando Ulyana, io me la sono squagliata verso l'uscita che grazie al cielo era lì vicino."


translate italian day3_library_sl_2a352a63:


    "Appena fuori, ho esitato per un istante, e mi sono imbattuto in Electronik."


translate italian day3_library_sl_f9674fb7:


    "Pareva stesse lavorando fino a tardi al circolo."


translate italian day3_library_sl_023f2a34:


    el "Dove vai così di fretta?"


translate italian day3_library_sl_6e69e20d:


    me "Io...?{w} La mia navetta parte tra dieci minuti."


translate italian day3_library_sl_e64f933d:


    el "Eh?{w} Ehi, aspetta!"


translate italian day3_library_sl_d7d54b7f:


    "Le parole di Electronik si sono perse nell'aria mentre mi allontanavo."


translate italian day3_library_sl_1afa7cdc:


    "Me ne sono andato verso la piazza."


translate italian day3_library_sl_f44b49d9:


    me "Non è di certo il luogo migliore per nascondersi."


translate italian day3_library_sl_863b1e12:


    "Nei momenti di grande stress e fatica, spesso comincio a parlare da solo."


translate italian day3_library_sl_f5dc1ea7:


    "Beh, in realtà non parlo, piuttosto borbotto sottovoce."


translate italian day3_library_sl_6c76fea4:


    "Mi aiuta a concentrarmi e a trovare la soluzione migliore in breve tempo."


translate italian day3_library_sl_2e3c65cd:


    me "Ok, e adesso?{w} Dove potrei andare...?"


translate italian day3_library_sl_ea2b71fd:


    "Il luogo migliore sarebbe stato quello con meno gente possibile."


translate italian day3_library_sl_bfa9bfb0:


    th "E quindi dovrei andare verso la foresta."


translate italian day3_library_sl_a088dacb:


    "Poco dopo mi stavo già rilassando, seduto su un ceppo accanto al sentiero."


translate italian day3_library_sl_caca443f:


    th "Questa sì che è stata una bella fuga!"


translate italian day3_library_sl_15e7cffc:


    th "Spero che Ulyana abbia imparato la lezione! Ormai non è più una bambina!"


translate italian day3_library_sl_cbda97c3:


    th "Già…{w} Non è più una bambina…"


translate italian day3_library_sl_be69861b:


    "Poi però un pensiero mi è passato per la testa: forse avevo gestito male la situazione."


translate italian day3_library_sl_fb96d515:


    th "In effetti una parte di quel caos è anche colpa mia."


translate italian day3_library_sl_e732f41b:


    th "E per cominciare, avrei potuto evitare di accendere la scintilla di tutto quel casino."


translate italian day3_library_sl_89141735:


    "La cosa mi ha fatto riflettere."


translate italian day3_library_sl_72cbabba:


    sl "Com'è il tempo oggi?"


translate italian day3_library_sl_5d2bdfb2:


    "Slavya stava proprio davanti a me."


translate italian day3_library_sl_a99618e8:


    th "Ma com'è riuscita ad avvicinarsi senza farsi sentire?{w} Grazie al cielo non è Olga Dmitrievna."


translate italian day3_library_sl_84662d7a:


    me "Bello…"


translate italian day3_library_sl_78884236:


    "Quella è stata l'unica parola che sono riuscito a far uscire dalla mia bocca."


translate italian day3_library_sl_c9b49156:


    sl "Pensavo di trovarti qui."


translate italian day3_library_sl_5ac38cf1:


    me "Perché?"


translate italian day3_library_sl_f1262d21:


    sl "Non saprei, è solo che…"


translate italian day3_library_sl_72426581:


    "Ha rivolto lo sguardo al cielo."


translate italian day3_library_sl_42471a85:


    sl "C'è poca gente qui."


translate italian day3_library_sl_becb13d6:


    th "Allora Slavya pensa davvero che io abbia una qualche fobia sociale?!{w} A dire il vero però, ha tutte le ragioni del mondo per pensarlo."


translate italian day3_library_sl_bf693f95:


    me "E tu cosa… ci fai qui?"


translate italian day3_library_sl_94fc3843:


    sl "Ti stavo cercando."


translate italian day3_library_sl_9fa90903:


    "Ha sorriso di nuovo, ma questa volta mi è sembrato un po' diverso. Un po' più amichevole, credo."


translate italian day3_library_sl_8052945a:


    me "Me? E perché?"


translate italian day3_library_sl_b57bc8ed:


    sl "Indovina?"


translate italian day3_library_sl_a5d87f4f:


    "Ho incrociato le braccia e mi sono preparato a subire una bella ramanzina."


translate italian day3_library_sl_e2e2018f:


    sl "Capisco che tu possa essere meno colpevole di Ulyana."


translate italian day3_library_sl_66b23847:


    th "Adesso l'assistente della leader del Campo sta prendendo le mie difese!{w} Ora mi sento ancora più patetico."


translate italian day3_library_sl_4fa4d55e:


    th "Siamo seri, non stiamo mica giocando a {i}poliziotto buono – poliziotto cattivo{/i}!"


translate italian day3_library_sl_50c80811:


    sl "Ma dal momento che hai deciso di non aiutarla, almeno potresti aiutare me."


translate italian day3_library_sl_d322f1a0:


    "Dopo avermi detto ciò, ha nuovamente portato lo sguardo verso il cielo."


translate italian day3_library_sl_7b2c24d7:


    th "Mi chiedo cosa stia guardando lassù?"


translate italian day3_library_sl_a9c6d0dd:


    me "E cosa dovrei fare?"


translate italian day3_library_sl_b90384cf:


    sl "Dobbiamo sistemare i libri nella biblioteca."


translate italian day3_library_sl_28c44327:


    "Sempre meglio che stare in punizione con Ulyana.{w} E comunque, era un'offerta che non potevo rifiutare."


translate italian day3_library_sl_c7aaed10:


    me "Al vostro servizio, mademoiselle. O madame?"


translate italian day3_library_sl_ec2c2a7c:


    "Ha riso."


translate italian day3_library_sl_e6b3e7b4:


    sl "Allora mi segua, monsieur!"


translate italian day3_library_sl_f4346447:


    "Dopo un paio di minuti eravamo davanti alla biblioteca."


translate italian day3_library_sl_4faed1c0:


    "Per tutto il tragitto mi sono guardato attorno, per paura di incontrare Olga Dmitrievna."


translate italian day3_library_sl_e82faf41:


    "Slavya non sembrava aver notato la mia preoccupazione."


translate italian day3_library_sl_9a6e5488:


    th "Comunque è piuttosto strano che non mi abbia rimproverato."


translate italian day3_library_sl_1c2eed77:


    "Beh, se dobbiamo continuare l'analogia, direi che Slavya è decisamente il poliziotto buono."


translate italian day3_library_sl_f58d39b1:


    "La luce splendente del sole trafiggeva le finestre della biblioteca, rivelando una miriade di particelle di polvere che danzavano tra i fasci luminosi."


translate italian day3_library_sl_35c68aa3:


    "È quel tipo di polvere che ha quel peculiare odore che non può essere confuso con nessun altro."


translate italian day3_library_sl_1d93103b:


    "L'odore di quelle vecchie e morenti opere dei classici del marxismo-leninismo."


translate italian day3_library_sl_325f1bcd:


    th "Mi chiedo se l'odore della polvere dei libri di narrativa sia lo stesso di quello proveniente dai libri di chimica e fisica."


translate italian day3_library_sl_826e40a7:


    sl "Inizia da quegli scaffali."


translate italian day3_library_sl_17c95df4:


    "Slavya mi ha indicato gli scaffali accanto al busto di Lenin."


translate italian day3_library_sl_46f0983a:


    me "Ok, ma cosa dovrei fare esattamente?"


translate italian day3_library_sl_968cfe00:


    sl "Devi rimuovere tutti i libri dagli scaffali e appoggiarli per terra.{w} Li spolveriamo e poi li rimettiamo a posto."


translate italian day3_library_sl_6febb73f:


    "A giudicare da tutta quella polvere, la lettura chiaramente non era l'attività preferita dei pionieri."


translate italian day3_library_sl_3a98efa2:


    me "Hai idea di dove possa essere Zhenya?"


translate italian day3_library_sl_a1e6dd7a:


    sl "Non saprei, probabilmente è andata da qualche parte."


translate italian day3_library_sl_ea719463:


    th "Strano."


translate italian day3_library_sl_a2356fab:


    "Esistevano solo due luoghi dove si avrebbe potuto incontrare Zhenya – la biblioteca e la mensa."


translate italian day3_library_sl_ac2cba61:


    "Ho iniziato a darmi da fare."


translate italian day3_library_sl_0eb6b971:


    "Non ho mai maneggiato tanti libri in vita mia."


translate italian day3_library_sl_ce83d765:


    "Forse perché sono abituato a leggere dallo schermo di un computer o di un tablet."


translate italian day3_library_sl_de4998dd:


    "Per prima cosa, gran parte degli e-book non sono a pagamento, e seconda cosa, non si può trovare tutto in forma cartacea."


translate italian day3_library_sl_77254c81:


    sl "Come sta andando?"


translate italian day3_library_sl_515b792e:


    "Mi ha chiesto Slavya, da dietro gli scaffali."


translate italian day3_library_sl_31dbcb74:


    me "Sta andando bene, piano piano..."


translate italian day3_library_sl_3799372a:


    me "Hai mai letto qualcosa di questo genere?"


translate italian day3_library_sl_8f3fa4b1:


    sl "Che genere?"


translate italian day3_library_sl_5625a91d:


    me "Beh, riguardo al comunismo…"


translate italian day3_library_sl_d51d54a0:


    sl "Nah, preferisco più i libri di storia.{w} E di avventura."


translate italian day3_library_sl_7f0819cb:


    me "Anch'io…"


translate italian day3_library_sl_a3f8af02:


    "A dire il vero la letteratura storica non era la mia preferita, ma la lotta di classe socialista non poteva di certo competere con essa!"


translate italian day3_library_sl_eae58056:


    sl "Ehi, non essere arrabbiato con Ulyana."


translate italian day3_library_sl_ec1593ec:


    me "Non lo sono..."


translate italian day3_library_sl_e1c77be8:


    "A dire il vero, quello che era accaduto alla mensa non mi aveva toccato più di tanto.{w} Inoltre, ero riuscito a pulirmi dagli avanzi del pranzo in un laghetto della foresta."


translate italian day3_library_sl_ca83718e:


    sl "Non lo fa con cattiveria."


translate italian day3_library_sl_04a51e77:


    me "Cattiveria o no, si dovrebbe comunque pensare prima di agire!{w} Ah, che importa..."


translate italian day3_library_sl_0f8bf2e5:


    "Ho proseguito la mia attività scorrendo tra dozzine di libri di autori a me totalmente sconosciuti."


translate italian day3_library_sl_92df4c4a:


    th "Tra un minuto me li sarò già dimenticati, e non saprò mai di cosa avessero scritto."


translate italian day3_library_sl_4012f849:


    th "È il destino di molti scrittori – finire nel dimenticatoio assieme alle loro frasi, imprigionate tra pesanti copertine ricoperte di polvere, abbandonate sullo scaffale della biblioteca di un inesistente campo di pionieri."


translate italian day3_library_sl_73956111:


    sl "Hai finito?"


translate italian day3_library_sl_f8226aa9:


    me "Sì, credo di sì."


translate italian day3_library_sl_7bfe1a7a:


    sl "Anch'io.{w} Ok, facciamo così, io li spolvero e tu li rimetti a posto."


translate italian day3_library_sl_f38a2469:


    me "Ma non ricordo in che ordine erano."


translate italian day3_library_sl_57b94aaa:


    sl "Non fa niente. Nessuno li leggerà comunque."


translate italian day3_library_sl_68a9ffa6:


    th "Pare che lei abbia un sorriso diverso per ogni occasione."


translate italian day3_library_sl_19451d64:


    "Ho notato che i suoi sorrisi nascondevano diverse sfumature di emozioni – gioia, felicità, tristezza…"


translate italian day3_library_sl_853f82b2:


    "Ma erano così fugaci che forse me li sono solo immaginati."


translate italian day3_library_sl_5ff6b0ce:


    "Questa volta il suo sorriso mi è sembrato giocoso."


translate italian day3_library_sl_1e475060:


    me "Sì, probabile.{w} O almeno, io non leggerei questa roba."


translate italian day3_library_sl_08cfb744:


    "Slavya stava spolverando le copertine con un panno umido, mentre io riponevo i libri sugli scaffali."


translate italian day3_library_sl_3a2c6311:


    "Eravamo una bella squadra – avevamo quasi finito."


translate italian day3_library_sl_9fdf4842:


    sl "Perché non hai tolto niente dagli scaffali in cima?"


translate italian day3_library_sl_4c1d6d10:


    me "Quali scaffali?"


translate italian day3_library_sl_f8c41e21:


    "Ho guardato verso l'alto."


translate italian day3_library_sl_cbcf5cb7:


    me "Scusa, non li ho visti."


translate italian day3_library_sl_705bea28:


    sl "Fa niente."


translate italian day3_library_sl_f9e5b237:


    me "Aspetta un secondo!{w} Prendo la sedia."


translate italian day3_library_sl_164801d0:


    "Persino con la mia considerevole statura non sarei riuscito ad arrivare lassù."


translate italian day3_library_sl_5c3aa9a0:


    "Sono salito sulla sedia e ho cominciato a rimuovere i libri e passarli giù a Slavya."


translate italian day3_library_sl_6f01b19b:


    "Dopo un po' sono rimasti solo pochi libri nell'angolo più lontano dello scaffale."


translate italian day3_library_sl_a29261f6:


    "Mi è sembrato che non fosse così difficile riuscire a raggiungerli."


translate italian day3_library_sl_45a87cde:


    "Data la mia usuale pigrizia, non avevo voglia di spostare la sedia.{w} E il fatto di non averla spostata..."


translate italian day3_library_sl_57e655b2:


    "Si è rivelato essere un errore fatale…"


translate italian day3_library_sl_3b6da452:


    "Ho aperto gli occhi, e mi sono ritrovato sopra Slavya."


translate italian day3_library_sl_64eccf44:


    me "Stai… Ti ho… Sei viva?!"


translate italian day3_library_sl_df45d6bc:


    "Mi sono spaventato a morte."


translate italian day3_library_sl_9f4025f7:


    th "Anche se adesso sono solo diciassettenne, il cadere da una sedia sopra a una ragazza è pericoloso!"


translate italian day3_library_sl_0451d1e4:


    sl "Sto bene."


translate italian day3_library_sl_55c4c14d:


    "Il suo viso era distante solo un paio di centimetri dal mio."


translate italian day3_library_sl_f3139a0d:


    me "Ti sei rotta qualcosa?"


translate italian day3_library_sl_3f137cac:


    sl "Non credo."


translate italian day3_library_sl_673a4231:


    sl "Saresti davvero un pessimo acrobata."


translate italian day3_library_sl_ce3666e5:


    "Mi ha detto ridendo."


translate italian day3_library_sl_e2d2475f:


    me "Di sicuro."


translate italian day3_library_sl_e10fbc50:


    "L'ho guardata negli occhi.{w} Solo guardata."


translate italian day3_library_sl_382be24f:


    "Non sapevo cosa dire...{w} E anche Slavya, o semplicemente aspettava che dicessi qualcosa io."


translate italian day3_library_sl_f0551a4a:


    th "In questa situazione potrebbe succedere qualcosa…"


translate italian day3_library_sl_5ff7b840:


    th "Le sue labbra sono così vicine alle mie!"


translate italian day3_library_sl_44711d46:


    "Un naturale desiderio si stava risvegliando in me, ma era {i}giusto{/i}?"


translate italian day3_library_sl_f481ef93:


    "Volevo alzarmi, ma non ci riuscivo."


translate italian day3_library_sl_ef3ea7a1:


    "Slavya se ne stava lì in silenzio, guardandomi negli occhi."


translate italian day3_library_sl_5ab0d3a1:


    "All'improvviso, la sirena della mensa è risuonata nell'aria."


translate italian day3_library_sl_8e8e0763:


    sl "È ora di cena."


translate italian day3_library_sl_751ac511:


    me "Già…"


translate italian day3_library_sl_d8eca9e9:


    sl "Allora andiamo?{w} O vuoi restare così ancora per un po'?"


translate italian day3_library_sl_e4a6b1bd:


    "Ancora una volta ha sorriso, e questa volta mi è sembrato un sorriso furbo, come se stesse alludendo a qualcosa."


translate italian day3_library_sl_050169d2:


    me "Andiamo…"


translate italian day3_library_sl_19989263:


    "Non mi sono mosso di un centimetro."


translate italian day3_library_sl_0f8c2844:


    sl "Beh, se vogliamo andare, forse dovresti prima alzarti."


translate italian day3_library_sl_85a7ebd8:


    me "Ovviamente…"


translate italian day3_library_sl_e6991c34:


    "Era come se una forza sconosciuta mi stesse trattenendo a terra."


translate italian day3_library_sl_8f6ef867:


    "Slavya pareva essersene accorta e lentamente è strisciata fuori sotto di me."


translate italian day3_library_sl_c0e00208:


    sl "E tu resti lì?"


translate italian day3_library_sl_ec2c2a7c_1:


    "Mi ha chiesto, divertita."


translate italian day3_library_sl_491711dd:


    "A quel punto finalmente mi sono ripreso e sono riuscito ad alzarmi."


translate italian day3_library_sl_8a8c94c1:


    me "Perdonami…"


translate italian day3_library_sl_a707867b:


    sl "È tutto a posto."


translate italian day3_library_sl_cd4fe72c:


    sl "Ma non abbiamo ancora finito qui.{w} Dovremo pulire il resto più tardi."


translate italian day3_library_sl_85279b68:


    me "Certamente."


translate italian day3_library_sl_92690f30:


    "Sono rimasto in silenzio per tutto il tragitto verso la mensa."


translate italian day3_library_sl_f1edeb24:


    "Slavya stava divagando su certi suoi affari qui nel Campo, ma di fatto era un monologo, e non la stavo ascoltando con attenzione."


translate italian day3_library_sl_d67f2f47:


    "Giunti alla mensa, ho visto Olga Dmitrievna sulla veranda."


translate italian day3_library_sl_2c96d9df:


    mt "Non mi devi qualche spiegazione, giovanotto?"


translate italian day3_library_sl_c3fe9d67:


    "Ecco che cosa mi ero scordato…"


translate italian day3_library_sl_5cb822ee:


    me "No…"


translate italian day3_library_sl_18279173:


    mt "Com'è andata la tua pulizia alla mensa?"


translate italian day3_library_sl_65bf82fc:


    me "Eh, abbastanza bene…"


translate italian day3_library_sl_09a04ec4:


    mt "Non è esattamente quello che mi ha detto Ulyana."


translate italian day3_library_sl_3074c0ad:


    me "È stata tutta colpa sua!"


translate italian day3_library_sl_6fc0750c:


    mt "E io cosa ti avevo detto di fare?"


translate italian day3_library_sl_6ced5f4c:


    mt "Ti ho ordinato di pulire assieme a lei!{w} E tu invece che fai?!"


translate italian day3_library_sl_d2aa1636:


    "Non sapevo cosa risponderle, dato che sotto sotto sapevo che aveva ragione."


translate italian day3_library_sl_418c13dd:


    sl "Non lo rimproveri!{w} Semyon mi ha aiutata nella biblioteca."


translate italian day3_library_sl_38689622:


    mt "Oh, davvero?"


translate italian day3_library_sl_7314bee2:


    sl "Sì!"


translate italian day3_library_sl_bc20eb0b:


    mt "Beh, è una cosa buona, ma…{w} Per farla breve, salterai la cena oggi!"


translate italian day3_library_sl_a2911372:


    me "Cosa?! E perché?!"


translate italian day3_library_sl_902a9a2f:


    "Ho protestato."


translate italian day3_library_sl_5ec7a533:


    "Tuttavia, la mia protesta sembrava più la richiesta di un bis alla mensa del carcere – scelta coraggiosa e audace, ma stupida e controproducente."


translate italian day3_library_sl_b77f1870:


    mt "Speriamo ti serva da lezione!"


translate italian day3_library_sl_fcc1c309:


    "In quel momento Olga Dmitrievna sembrava più un comandante legionario dell'antica Roma, piuttosto che la leader di un piccolo campo di pionieri."


translate italian day3_library_sl_5bdcf334:


    "Non sapevo come oppormi."


translate italian day3_library_sl_ae8c4442:


    sl "Lo perdoni!{w} Mi prenderò io la responsabilità!"


translate italian day3_library_sl_d4764288:


    "La leader del Campo ci ha pensato su un attimo."


translate italian day3_library_sl_777093a7:


    mt "D'accordo, se proprio insisti!"


translate italian day3_library_sl_309686e8:


    sl "Grazie!"


translate italian day3_library_sl_73aab3cd:


    me "Grazie…"


translate italian day3_library_sl_34c73a83:


    "Ci siamo affrettati ad entrare."


translate italian day3_library_sl_4df43400:


    sl "Beh, io vado dalle ragazze, mi sono messa d'accordo con loro.{w} Stammi bene!"


translate italian day3_library_sl_3ec70e0f:


    "Mi ha salutato con la mano."


translate italian day3_library_sl_99d3acba:


    "Chissà dove sarei finito se non fosse stato per lei?!"


translate italian day3_library_sl_0eba76f0:


    "Non c'erano altri posti liberi vicino a Slavya, e comunque non conoscevo quelle persone."


translate italian day3_cleaning_us_99ef151c:


    "Nah, fuggire non sarebbe stata una buona idea."


translate italian day3_cleaning_us_93c60cc4:


    "Innanzitutto perché Olga Dmitrievna mi aveva già beccato, e disubbidire nuovamente avrebbe solo aggravato la mia situazione."


translate italian day3_cleaning_us_394c9d9e:


    "E inoltre, anch'io ero in parte colpevole…"


translate italian day3_cleaning_us_2f94b8f1:


    th "Anche se in fondo è stata tutta colpa sua!"


translate italian day3_cleaning_us_25f82315:


    "Ma se non avessi reagito così, probabilmente avremmo evitato tutto quel macello."


translate italian day3_cleaning_us_996e7fec:


    th "Probabilmente…"


translate italian day3_cleaning_us_3e5b8732:


    "Ho aperto l'armadietto e ho preso una scopa e una paletta."


translate italian day3_cleaning_us_f469532f:


    "Quando sono tornato, Olga Dmitrievna non era più lì con Ulyana."


translate italian day3_cleaning_us_24ab10f2:


    me "Se n'è andata?"


translate italian day3_cleaning_us_7e694dd0:


    us "Come puoi vedere!"


translate italian day3_cleaning_us_afe5cb8e:


    "Ulyana sembrava irritata, tutto il suo usuale entusiasmo giovanile era svanito nel nulla."


translate italian day3_cleaning_us_3b4a6242:


    me "Ok, aspetta un attimo, prima vado a lavarmi un po'."


translate italian day3_cleaning_us_a464805b:


    "Le ho dato un'occhiataccia e mi sono diretto verso l'uscita."


translate italian day3_cleaning_us_80aa159b:


    "Mi sono levato di dosso gli avanzi del pranzo e sono tornato nella mensa."


translate italian day3_cleaning_us_8bf25c46:


    me "Beh, è inevitabile – dobbiamo pulire tutto."


translate italian day3_cleaning_us_0713a7b2:


    us "È stata tutta colpa tua!"


translate italian day3_cleaning_us_97c9d07e:


    "Quel suo sguardo mi ha dato i brividi."


translate italian day3_cleaning_us_771aa502:


    me "Ma certo, come no!"


translate italian day3_cleaning_us_12685598:


    me "È sempre colpa mia, di tutto!{w} Sono io il disastro naturale qui."


translate italian day3_cleaning_us_005b5a5d:


    us "Oh, sta' zitto…"


translate italian day3_cleaning_us_abd7f53b:


    "È comunque strano che non abbia cercato di liberarsi della punizione."


translate italian day3_cleaning_us_87e97c80:


    "Ulyana avrebbe potuto fuggire e lasciarmi da solo, ma per qualche ragione ha fatto l'esatto contrario, raccogliendo diligentemente i piatti frantumati, lavando il pavimento, tirando su le sedie e i tavoli."


translate italian day3_cleaning_us_ad056558:


    "A dire il vero, stava facendo tutto così in fretta che non riuscivo a mantenere il suo ritmo."


translate italian day3_cleaning_us_a3e4be71:


    me "Ma guarda, tutto ad un tratto hai iniziato a comportarti come si deve?"


translate italian day3_cleaning_us_a9babbd5:


    us "È che non ho intenzione di passare tutto il giorno qui, scemo!"


translate italian day3_cleaning_us_9fc103c6:


    "La sua voce suonava ancora offesa."


translate italian day3_cleaning_us_19fa1fbf:


    me "Senti, devi capire che non puoi comportarti così…{w} O almeno non dovresti esagerare…"


translate italian day3_cleaning_us_fbfed032:


    "Per qualche motivo ho deciso di darle una lezione morale."


translate italian day3_cleaning_us_e142a9de:


    us "Ma io non ho fatto niente!{w} Hai iniziato tu a prendermi in giro."


translate italian day3_cleaning_us_4f354c21:


    "Ulyana ha preso un secchio e uno straccio ed è andata verso l'angolo in fondo alla mensa."


translate italian day3_cleaning_us_d71063b0:


    th "Sembra ancora arrabbiata."


translate italian day3_cleaning_us_b32f5157:


    "Vedendo la montagna di cocci tutt'attorno, mi sono reso conto che avevamo fatto davvero una catastrofe."


translate italian day3_cleaning_us_7300ad57:


    th "Per fortuna le forchette e i cucchiai sono di metallo – almeno avremo ancora qualcosa con cui poter mangiare.{w} Ma sono rimasti davvero pochi piatti..."


translate italian day3_cleaning_us_e66e1571:


    us "Ehi…"


translate italian day3_cleaning_us_81282f69:


    "Ulyana mi ha chiamato."


translate italian day3_cleaning_us_cbdad383:


    "L'ho raggiunta."


translate italian day3_cleaning_us_557a9104:


    us "Non riesco a capire, perché mi odii così tanto?"


translate italian day3_cleaning_us_8e1a6421:


    "La sua espressione è diventata talmente seria che ero pronto a credere che non si trattasse di un altro dei suoi scherzi."


translate italian day3_cleaning_us_dffd29c3:


    me "Perché pensi questo?"


translate italian day3_cleaning_us_f0847682:


    us "Non ne ho idea, ecco perché te lo sto chiedendo."


translate italian day3_cleaning_us_e5f99c8b:


    me "Io non ti odio.{w} È solo che certe volte ti comporti un po'…{w} Beh, sai."


translate italian day3_cleaning_us_8c306ba5:


    "Era la pura verità."


translate italian day3_cleaning_us_a00845ac:


    us "Un po' come?{w} Non ne ho idea."


translate italian day3_cleaning_us_cc439301:


    "Ha alzato lo sguardo su di me, con gli occhi pieni di curiosità."


translate italian day3_cleaning_us_4470295b:


    me "Beh, per cominciare, che senso aveva rovesciarmi addosso il kompot?"


translate italian day3_cleaning_us_c656e178:


    us "Te la sei cercata."


translate italian day3_cleaning_us_65b7d3f3:


    "Mi ha risposto, sorridendo per la prima volta dopo che abbiamo iniziato a pulire."


translate italian day3_cleaning_us_2bb47ab4:


    me "Sì, certo..."


translate italian day3_cleaning_us_47d09bdb:


    "Ho fatto un profondo sospiro."


translate italian day3_cleaning_us_0621ba40:


    me "E allora, cosa ti aspetti che facciano gli altri?"


translate italian day3_cleaning_us_ff47fe7e:


    us "Niente."


translate italian day3_cleaning_us_2f6a59ef:


    "Ha risposto aspramente."


translate italian day3_cleaning_us_8e2b133b:


    "Il discorso era chiuso, quindi ho continuato le mie pulizie in silenzio."


translate italian day3_cleaning_us_a20cefa7:


    "..."


translate italian day3_cleaning_us_86688e12:


    "Abbiamo impiegato qualche ora a rimettere tutto a posto."


translate italian day3_cleaning_us_0a8d709c:


    "Alla fine, tutte le stoviglie frantumante sono sparite. I tavoli e le sedie erano al loro posto, il pavimento era bello pulito."


translate italian day3_cleaning_us_d6447ba7:


    "Ci siamo seduti sul bancone, respirando affannosamente, godendoci il nostro meritato riposo."


translate italian day3_cleaning_us_a4eb05ec:


    me "Adesso vedi quanta fatica si deve fare per rimediare a uno stupido scherzo."


translate italian day3_cleaning_us_d9f878c6:


    us "Ma io non sono per niente stanca!"


translate italian day3_cleaning_us_fc386561:


    "Anche se la sua fronte ricoperta di sudore diceva tutt'altro."


translate italian day3_cleaning_us_8e0db1a9:


    me "Oh bene, buon per te…"


translate italian day3_cleaning_us_926c6d27:


    us "Allora, cosa facciamo adesso?"


translate italian day3_cleaning_us_2a831701:


    me "Non so tu, ma io vorrei andarmene…"


translate italian day3_cleaning_us_2fe78b26:


    us "No, aspetta!"


translate italian day3_cleaning_us_86e9c04d:


    us "Devi ancora…"


translate italian day3_cleaning_us_6e6e30b3:


    "Ha esitato."


translate italian day3_cleaning_us_cbab93c7:


    us "...aiutarmi con un'altra piccola cosa!"


translate italian day3_cleaning_us_341c6bf4:


    me "Stai pensando a un altro stupido scherzo?"


translate italian day3_cleaning_us_7862b445:


    us "Ci puoi scommettere!"


translate italian day3_cleaning_us_6d77584d:


    "Ha fatto un ampio sorriso."


translate italian day3_cleaning_us_b72aa87d:


    me "Non sono il tuo assistente.{w} Non ho intenzione di subire altre punizioni oggi."


translate italian day3_cleaning_us_005a932e:


    us "Ok, ecco l'accordo!{w} Se mi aiuterai adesso, non ti farò più scherzi!"


translate italian day3_cleaning_us_d5e744c0:


    "La proposta era allettante, ma in qualche modo non riuscivo a fidarmi di Ulyana, nemmeno per un istante."


translate italian day3_cleaning_us_24184228:


    th "Beh, chiedere non costa nulla."


translate italian day3_cleaning_us_72d07964:


    me "E quale sarebbe il tuo piano astuto?"


translate italian day3_cleaning_us_8e695095:


    us "Ruberemo le caramelle!"


translate italian day3_cleaning_us_3d342d5a:


    me "Cosa?!"


translate italian day3_cleaning_us_cce1c4dc:


    "Mi sarei dovuto aspettare una cosa simile da parte sua."


translate italian day3_cleaning_us_82811001:


    th "Le caramelle sono per i bambini…"


translate italian day3_cleaning_us_f544383f:


    us "Fra poco la cuoca andrà fuori a gettare la spazzatura, nessuno ci potrà vedere!"


translate italian day3_cleaning_us_df96cac2:


    me "Non voglio essere coinvolto!"


translate italian day3_cleaning_us_44f47ba7:


    us "Ah, vabbè!"


translate italian day3_cleaning_us_99aa653e:


    "Ha sbuffato e si è allontanata."


translate italian day3_cleaning_us_1838e844:


    us "Allora lo farò da sola!"


translate italian day3_cleaning_us_821e8719:


    me "E io non te lo permett…"


translate italian day3_cleaning_us_e1f72ec9:


    "Non sono nemmeno riuscito a finire la frase, che Ulyana era già saltata sul bancone, ha aperto il mobiletto, e ha iniziato a frugare in esso."


translate italian day3_cleaning_us_e182607e:


    me "Ehi, smettila!{w} Non hai avuto abbastanza problemi oggi con Olga Dmitrievna?!"


translate italian day3_cleaning_us_6586cba7:


    "Non mi ha risposto."


translate italian day3_cleaning_us_fdb724d1:


    me "Non te la caverai con una semplice punizione di pulizia per una cosa del genere."


translate italian day3_cleaning_us_2992696a:


    "Ulyana ha chiuso il mobiletto.{w} Tra le sue mani teneva un enorme sacchetto di caramelle."


translate italian day3_cleaning_us_244f004d:


    me "Ma sei impazzita...?! Rimettilo subito al suo posto!"


translate italian day3_cleaning_us_5cc17f12:


    "Mi ha fatto la linguaccia ed è scattata verso l'uscita secondaria."


translate italian day3_cleaning_us_8d50913c:


    "Non poteva semplicemente finire così!{w} Mi sono precipitato dietro a lei."


translate italian day3_cleaning_us_499e4041:


    "Certo, la ragazza era partita in vantaggio, ma ho impiegato tutte le mie energie per riuscire a raggiungerla."


translate italian day3_cleaning_us_0ab77e34:


    th "Non mi scapperà un'altra volta!"


translate italian day3_cleaning_us_a0344103:


    "L'ho rincorsa attraverso la piazza…"


translate italian day3_cleaning_us_971a61ea:


    "Abbiamo svoltato all'edificio del circolo musicale…"


translate italian day3_cleaning_us_c8e8d113:


    "E siamo finiti sul sentiero della foresta."


translate italian day3_cleaning_us_596c2686:


    "Avevo quasi preso Ulyana, quando di colpo si è fermata…"


translate italian day3_cleaning_us_13e17fb4:


    "Non sono riuscito a frenare in tempo, così l'ho travolta in pieno, e siamo caduti scompostamente."


translate italian day3_cleaning_us_31ba2a96:


    "Abbiamo rotolato sull'erba..."


translate italian day3_cleaning_us_9cd22020:


    me "Presa!"


translate italian day3_cleaning_us_7aeb2243:


    "Ho gridato, trionfante."


translate italian day3_cleaning_us_c67ac3de:


    us "No, non è vero…"


translate italian day3_cleaning_us_ed702531:


    "Mi ha risposto, vergognandosi."


translate italian day3_cleaning_us_0ec6d3ef:


    "Ulyana stava sdraiata proprio sotto di me."


translate italian day3_cleaning_us_92dbd985:


    "Il suo viso era molto vicino al mio."


translate italian day3_cleaning_us_cb6f36c0:


    "Potevo sentire il suo respiro irregolare e il calore del suo corpo."


translate italian day3_cleaning_us_57515ee8:


    th "Certo, adesso è ancora una ragazzina, ma presto diventerà una donna."


translate italian day3_cleaning_us_b08ceff8:


    "Era piuttosto imbarazzante."


translate italian day3_cleaning_us_c19988a4:


    us "Mi vuoi violentare?"


translate italian day3_cleaning_us_436a485d:


    "Mi ha chiesto, tornando in sé."


translate italian day3_cleaning_us_0c857a98:


    me "Vuoi che lo faccia?"


translate italian day3_cleaning_us_b4df4200:


    "Comunque, era più un gioco per lei."


translate italian day3_cleaning_us_0fca62ac:


    us "Fallo!"


translate italian day3_cleaning_us_0f58246c:


    "Mi ha fatto un sorriso malizioso e ha sbuffato tranquillamente.{w} O me lo sono immaginato?"


translate italian day3_cleaning_us_568b5895:


    me "Non sono proprio in vena…"


translate italian day3_cleaning_us_44f47ba7_1:


    us "Ah, come vuoi!"


translate italian day3_cleaning_us_bd670e0e:


    "Ulyana si è alzata e mi ha morso il naso."


translate italian day3_cleaning_us_b47815ea:


    "Quello non me lo sarei aspettato, e ho indietreggiato, toccandomi il naso dolorante."


translate italian day3_cleaning_us_4e8535ee:


    "Quel breve momento di disattenzione le è bastato per fuggire e allontanarsi di qualche metro."


translate italian day3_cleaning_us_28d12718:


    us "Stai attento!{w} Te ne pentirai!"


translate italian day3_cleaning_us_5cabe167:


    "Mi ha detto ridendo, ed è sparita oltre gli alberi."


translate italian day3_cleaning_us_780778e4:


    "Il sacchetto di caramelle è rimasto lì per terra accanto a me."


translate italian day3_cleaning_us_6e19bf3f:


    th "Mi chiedo se l'abbia lasciato di proposito?"


translate italian day3_cleaning_us_11007c59:


    "Era quasi ora di cena, quindi dovevo affrettarmi a riportarlo indietro."


translate italian day3_cleaning_us_ff6853b7:


    "...E preferibilmente (molto preferibilmente) passare inosservato."


translate italian day3_cleaning_us_f7b4ae2b:


    "Se qualcuno mi avesse beccato gli avrei detto che era stata Ulyana a rubarle, ovviamente…"


translate italian day3_cleaning_us_e4f68848:


    th "Ma mi crederanno?"


translate italian day3_cleaning_us_492cba74:


    "Olga Dmitrievna mi stava aspettando all'entrata della mensa…"


translate italian day3_cleaning_us_2299f5be:


    mt "Ben fatto, Semyon!"


translate italian day3_cleaning_us_fb544be4:


    me "Fatto cosa?"


translate italian day3_cleaning_us_1ed8ab85:


    "Ho nascosto il sacchetto dietro alle mie spalle."


translate italian day3_cleaning_us_9d2438b5:


    "Purtroppo era trasparente, ed era troppo grande per poterlo mettere in tasca."


translate italian day3_cleaning_us_7746de39:


    mt "I lavori di pulizia. Adesso è tutto pulito e in ordine."


translate italian day3_cleaning_us_59d93851:


    me "Beh, sì…"


translate italian day3_cleaning_us_a8ce4ea0:


    mt "E dov'è Ulyana?"


translate italian day3_cleaning_us_f7c4eac6:


    th "Non ne ho idea!"


translate italian day3_cleaning_us_c7337a6d:


    me "Lei… verrà presto…"


translate italian day3_cleaning_us_881d262a:


    mt "Ok allora. Entra e mangia."


translate italian day3_cleaning_us_ad1ee043:


    "Sono entrato nella mensa."


translate italian day3_cleaning_us_5a56cd46:


    "Ovviamente era affollatissima."


translate italian day3_cleaning_us_0a6a1ce5:


    "E adesso come avrei fatto a riportare il sacchetto senza farmi notare?"


translate italian day3_cleaning_us_4bcefbd3:


    th "Certo, potrei farlo più tardi, ma che me ne faccio adesso?!"


translate italian day3_cleaning_us_2a454c26:


    sl "Semyon!"


translate italian day3_cleaning_us_71e7861c:


    "Mi sono girato. Slavya era davanti a me."


translate italian day3_cleaning_us_da29ffa7:


    sl "Wow, e quello cos'è?"


translate italian day3_cleaning_us_6651226b:


    "È riuscita a vedere il sacchetto, che non sono riuscito a nascondere in tempo."


translate italian day3_cleaning_us_5a9cf61b:


    th "Beccato con le mani nel sacco, o meglio, nel sacchetto! È ora di prepararsi per una bella sgridata."


translate italian day3_cleaning_us_1a98bf30:


    me "Queste sono…{w} caramelle…"


translate italian day3_cleaning_us_81dfe51a:


    sl "E dove le hai prese?"


translate italian day3_cleaning_us_d9ec0a9f:


    th "Le ho rubate, dannazione…"


translate italian day3_cleaning_us_934cbef8:


    me "Me le ha date Ulyana."


translate italian day3_cleaning_us_320366d1:


    sl "Oh capisco. Sempre la solita storia?"


translate italian day3_cleaning_us_5d6eebdb:


    me "Cosa vuoi dire?"


translate italian day3_cleaning_us_63f30cbb:


    sl "Non è la prima volta che ruba le caramelle."


translate italian day3_cleaning_us_11ef631b:


    th "Ma che strano..."


translate italian day3_cleaning_us_ee1d96ff:


    sl "Ci penso io, dai qua."


translate italian day3_cleaning_us_73aab3cd:


    me "Grazie…"


translate italian day3_cleaning_us_03e8e491:


    "E così Slavya mi ha salvato di nuovo."


translate italian day3_cleaning_us_1751857b:


    "Ha preso il sacchetto e l'ha riportato al suo posto."


translate italian day3_cleaning_us_574fb3bf:


    "Non mi interessava sapere quale spiegazione avrebbe dato nel riconsegnare quel sacchetto. E così mi sono guardato attorno in cerca di un posto libero."


translate italian day3_main5_144331d6:


    "Sembrava che io dovessi sedermi con Electronik e Shurik."


translate italian day3_main5_967e239c:


    "Non c'erano altri posti liberi."


translate italian day3_main5_67af4832:


    me "Come va, signori?!"


translate italian day3_main5_cc50a49a:


    "Tutte le volte che dovevo parlare con loro, sentivo il bisogno di stuzzicarli."


translate italian day3_main5_887a8a04:


    "Forse era un po' troppo rischioso comportarsi così, ma i {i}fratelli elettronici{/i} erano la mia fonte primaria di positività."


translate italian day3_main5_1d386f5f:


    me "Come state?"


translate italian day3_main5_489d4411:


    el "Bene. E tu?"


translate italian day3_main5_7cf58fb3:


    me "Alti e bassi."


translate italian day3_main5_2e8cf482:


    sh "È successo qualcosa?"


translate italian day3_main5_ad9962cd:


    me "Molte cose."


translate italian day3_main5_a4a08fa3:


    el "Ce le vuoi raccontare?"


translate italian day3_main5_b216e161:


    me "Nah, forse un'altra volta."


translate italian day3_main5_44ead865:


    sh "Come vuoi."


translate italian day3_main5_b491ceac:


    "Ha alzato le spalle."


translate italian day3_main5_3ca98c02:


    el "Andremo al ballo dopo cena!"


translate italian day3_main5_b528275f:


    "Ha ridacchiato Electronik."


translate italian day3_main5_e1a72042:


    me "Lo so."


translate italian day3_main5_d1f8823e:


    el "Tu chi vorresti invitare?"


translate italian day3_main5_7b2a4bde:


    me "Non ho ancora deciso.{w} E tu?"


translate italian day3_main5_7d7e9876:


    el "Io… Beh, ecco…"


translate italian day3_main5_94a58e2f:


    "Sembrava che quella domanda l'avesse colto alla sprovvista."


translate italian day3_main5_7607ce16:


    me "Chiedilo a Ulyana. Le farebbe piacere."


translate italian day3_main5_8da8904a:


    el "No grazie!"


translate italian day3_main5_5753b37f:


    "Electronik ha rigettato subito l'idea."


translate italian day3_main5_5fe023fb:


    me "E tu, Shurik, dovresti chiederlo ad Alisa."


translate italian day3_main5_e778b8e0:


    sh "Meglio di no."


translate italian day3_main5_1d41d511:


    "Mi ha risposto in modo più tranquillo del suo compare."


translate italian day3_main5_246c6e50:


    me "Oh, andiamo ragazzi, sarà divertente!"


translate italian day3_main5_29c157fd:


    el "E comunque...{w} Abbiamo delle cose da fare!{w} Dobbiamo finire il nostro robot!"


translate italian day3_main5_41e8d12f:


    me "Oh, ma è un'idea geniale!{w} Invitate il vostro robot al ballo! Sa ballare?"


translate italian day3_main5_9eb621e9:


    sh "Non sa nemmeno camminare."


translate italian day3_main5_067ae991:


    "Shurik probabilmente non aveva colto il mio sarcasmo."


translate italian day3_main5_04d8b901:


    el "Perché no, potrebbe essere l'occasione per mostrare a tutti i nostri progressi!"


translate italian day3_main5_345dabaf:


    sh "E cosa potremmo mostrargli?"


translate italian day3_main5_69ce28cd:


    el "Già, hai ragione..."


translate italian day3_main5_b54d08ef:


    "Entrambi si sono messi a fissare i loro piatti, delusi."


translate italian day3_main5_a20cefa7:


    "..."


translate italian day3_main5_a20cefa7_1:


    "..."


translate italian day3_main5_a20cefa7_2:


    "..."


translate italian day3_main5_3f89d2ed:


    "La cena era finita e i pionieri si stavano disperdendo."


translate italian day3_main5_ad0e1b9c:


    me "Che cosa indosserete al ballo?"


translate italian day3_main5_83b23459:


    "Ho chiesto a Shurik ed Electronik."


translate italian day3_main5_3b3ff830:


    el "Non abbiamo niente di particolare da indossare. Andremo così come siamo adesso."


translate italian day3_main5_4bca7d01:


    "Ha indicato la sua uniforme."


translate italian day3_main5_51acbdbc:


    th "Sembra che a loro non importi più di tanto di come vestirsi per il ballo."


translate italian day3_main5_46388e2b:


    th "Beh, perché dovrebbe importare a me allora?!"


translate italian day3_main5_a2a4c2bc:


    th "I miei vestiti invernali non vanno bene, andrò anch'io in uniforme come loro."


translate italian day3_main5_0058648c:


    me "Quando inizia?"


translate italian day3_main5_129ca123:


    el "Dopo le nove."


translate italian day3_main5_855db9ee:


    me "Capito."


translate italian day3_main5_a8b4968b:


    "Ho abbandonato la mensa, inalando la fresca aria della sera."


translate italian day3_main5_52c76225:


    "Ricordo ancora quelle rare volte in cui avevo partecipato a un ballo, ai tempi della scuola."


translate italian day3_main5_b1cb0198:


    "Ansia, insicurezza, persino paura…"


translate italian day3_main5_68f4a1be:


    "Non sapevo ballare, non sapevo come reagire se qualcuno mi avesse invitato, non avevo il coraggio di invitare qualcuno."


translate italian day3_main5_c972d742:


    "In poche parole, non mi trovavo a mio agio."


translate italian day3_main5_742d0473:


    "Ed era ancora più spiacevole vedere gli altri che riuscivano a divertirsi."


translate italian day3_main5_fa8fda07:


    "Non era invidia.{w} Era più lo sconcerto del vedere che le persone fossero in grado di godersi qualcosa che era così strano per me."


translate italian day3_main5_1ecbd57e:


    "Mancava ancora molto tempo all'ora del ballo, così ho pensato di andare a dormire un po' per recuperare le energie."


translate italian day3_main5_629eed15:


    "Dopo essere entrato nella stanza della leader, mi sono lasciato cadere sul letto e ho chiuso gli occhi."


translate italian day3_main5_a20cefa7_3:


    "..."


translate italian day3_main5_f7195c18:


    "Stranamente, mi sono svegliato all'ora giusta senza il bisogno di una sveglia."


translate italian day3_main5_da045b54:


    "Erano le nove esatte."


translate italian day3_main5_2da36184:


    "Io che mi alzo all'ora giusta, che cosa strana."


translate italian day3_main5_97bec1c5:


    "Tuttavia mi sentivo sgualcito."


translate italian day3_main5_dab6de06:


    th "Forse il dormire a quest'ora non è stata una buona idea."


translate italian day3_main5_48355910:


    "Avevo un appuntamento con Alisa invece del ballo."


translate italian day3_main5_d91422b8:


    "Era ora di andarci, dunque."


translate italian day3_main5_abd3bafa:


    "Ma avevo promesso di aiutare Lena..."


translate italian day3_main5_f2246ca0:


    th "E cosa dovrei fare adesso?"


translate italian day3_main5_5dd208f8:


    "Beh, ci devo andare per forza."


translate italian day3_main5_a74ee93a:


    "Dopo pochi minuti ho raggiunto la piazza."


translate italian day3_main5_c5292350:


    "Attorno al monumento erano state installate delle casse audio e un equipaggiamento da DJ, e gli alberi erano decorati con delle luci."


translate italian day3_main5_bfae52bf:


    th "Ah, un tipico ballo di paese."


translate italian day3_main5_b71fb061:


    "C'erano già molti pionieri, ma nessuno che conoscevo, quindi mi sono seduto su una panchina e ho aspettato."


translate italian day3_main5_bf88823d:


    th "Dopotutto non devo ballare."


translate italian day3_main5_ba5eaafc:


    "Forse mi basterà stare seduto e parlare con qualcuno."


translate italian day3_main5_f62bc905:


    us "Ciao, faccia triste."


translate italian day3_main5_c9bf617b:


    "Ulyana."


translate italian day3_main5_b9767350:


    me "Hai qualcosa in mente?"


translate italian day3_main5_ab2952f4:


    us "Vieni, balliamo insieme!"


translate italian day3_main5_9528c3cd:


    me "È ancora troppo presto…{w} Non c'è nemmeno la musica."


translate italian day3_main5_0225548f:


    us "Blah, sei così noioso!"


translate italian day3_main5_386b2a74:


    th "Già, certamente non sono una buona compagnia per un evento del genere."


translate italian day3_main5_7ad70676:


    "È corsa via."


translate italian day3_main5_a11f2300:


    th "E così Ulyana indossa una specie di abito da sera.{w} Piuttosto buffo."


translate italian day3_main5_9104ac12:


    sl "Ciao!"


translate italian day3_main5_3c7474d6:


    "Slavya."


translate italian day3_main5_a87c71d7:


    me "Ehi."


translate italian day3_main5_81c4dbcb:


    "Si è seduta accanto a me."


translate italian day3_main5_c2bffcd7:


    sl "Come va?"


translate italian day3_main5_e7ce88cd:


    me "Bene."


translate italian day3_main5_0d67c838:


    sl "Perché sei così triste?"


translate italian day3_main5_b97e3fd5:


    me "Non lo sono…"


translate italian day3_main5_795edb64:


    sl "Ok. Beh, ballare sicuramente ti tirerà su il morale!"


translate italian day3_main5_907b6d20:


    me "Forse…"


translate italian day3_main5_0981716b:


    sl "Non dimenticarti di concedermi un ballo!"


translate italian day3_main5_5fc53cc4:


    "Ha riso ed è corsa verso la postazione da DJ."


translate italian day3_main5_aedce5d2:


    "La situazione stava diventando complicata. Di certo non sarei riuscito a restarmene seduto per tutta la serata."


translate italian day3_main5_bd946546:


    un "Ciao."


translate italian day3_main5_d106dd84:


    "Lena si è avvicinata."


translate italian day3_main5_8bd25464:


    me "Oh, ciao, anche tu sei qui…"


translate italian day3_main5_6837bff6:


    th "C'è da stupirsi?"


translate italian day3_main5_f1be5fed:


    un "Sì."


translate italian day3_main5_4457b2a1:


    me "Ok…{w} Sei pronta a spaccare?"


translate italian day3_main5_a4468653:


    "La battuta non è stata delle migliori."


translate italian day3_main5_47092547:


    un "…"


translate italian day3_main5_c5ac7756:


    "Lena è arrossita e ha abbassato lo sguardo."


translate italian day3_main5_8c3325f1:


    me "Beh, forse non è una grande idea – spaccare tutto…"


translate italian day3_main5_80702140:


    un "Ok, allora io…"


translate italian day3_main5_279eeea1:


    me "Ok…"


translate italian day3_main5_bc5b46e9:


    "Se n'è andata."


translate italian day3_main5_a20cefa7_4:


    "..."


translate italian day3_main5_94d94b39:


    "Sembrava che l'intero campo si fosse radunato alla piazza."


translate italian day3_main5_3d7269a4:


    "I pionieri si sono riuniti in vari gruppi, parlando, scherzando, ridendo."


translate italian day3_main5_e3412cd3:


    "Alla postazione da DJ, Ulyana stava litigando con Olga Dmitrievna per quanto riguarda la playlist di questa sera."


translate italian day3_main5_461da4d2:


    "Ed eccola lì.{w} La musica è iniziata."


translate italian day3_main5_e9f89819:


    "Non conoscevo esattamente quelle canzoni, ma si poteva dire che rientrassero nel genere «Classici del pop sovietico»."


translate italian day3_main5_337cd741:


    "I pionieri continuavano a parlare ignorando la musica."


translate italian day3_main5_5f0a6bf8:


    "È sempre difficile fare il primo passo.{w} Specialmente quando hai la certezza di essere l'unico che lo farà…"


translate italian day3_main5_f14850ff:


    "Ulyana sembrava non rientrare nella categoria."


translate italian day3_main5_1b76cd8a:


    "Ha raggiunto il centro della piazza e ha gridato forte:"


translate italian day3_main5_617994a2:


    us "Che cosa state aspettando?!"


translate italian day3_main5_62d41fb8:


    "Ed ha iniziato a dimenarsi comicamente."


translate italian day3_main5_b59f99e6:


    "Sì, «dimenarsi» è la parola giusta.{w} Non si potrebbe trovare un termine migliore."


translate italian day3_main5_530ffc25:


    "Era così ridicola e divertente che non ho potuto trattenermi dal ridere."


translate italian day3_main5_da8b7f2f:


    "E lei mi ha notato."


translate italian day3_main5_015e0ef0:


    us "Ehi! Semyon!"


translate italian day3_main5_11709dee:


    "Ho fatto finta di non sentirla."


translate italian day3_main5_81e15cab:


    us "Non fingere di non sentire! Vieni qui con me!"


translate italian day3_main5_0acd34c9:


    "Ho continuato a ignorarla."


translate italian day3_main5_57b6835b:


    "I pionieri si sono pian piano accorti che dovevano in qualche modo contribuire alla serata, e hanno iniziato a ballare."


translate italian day3_main5_0cfd660b:


    "A me sembrava tutto così sciocco."


translate italian day3_main5_6ca388ff:


    th "Agitare braccia e gambe sulle note di qualche vecchia canzone dimenticata da tempo?{w} Suvvia, siamo seri!"


translate italian day3_main5_785dd32e:


    "Certamente io non avrei saputo fare di meglio, ma quella cosa non poteva di certo essere chiamata ballo."


translate italian day3_main5_7023662f:


    sl "Ehi, Semyon, perché te ne stai lì seduto?{w} Non vuoi ballare?"


translate italian day3_main5_19346aa1:


    "Ero così immerso nei miei pensieri che non avevo neppure notato la presenza di Slavya."


translate italian day3_main5_6a4b94d5:


    me "Non proprio…"


translate italian day3_main5_d7a9e07a:


    sl "Sei sicuro?"


translate italian day3_main5_d1046a49:


    "Ha sorriso."


translate italian day3_main5_65003d93:


    me "Magari più tardi."


translate italian day3_main5_7ccf3096:


    th "Diamine! Ma cosa ci faccio io qui?!"


translate italian day3_evening_sl_906aa1af:


    th "Che senso ha andare ad un ballo se poi te ne stai seduto in un angolo?"


translate italian day3_evening_sl_007e73c0:


    me "Ok, ma solo per un momento…"


translate italian day3_evening_sl_d0c11d8b:


    sl "Questo è lo spirito giusto!"


translate italian day3_evening_sl_55ab51a9:


    "Ha allungato la mano verso di me."


translate italian day3_evening_sl_e6ed4325:


    "Eravamo al centro della folla danzante."


translate italian day3_evening_sl_0dd1c4e2:


    "Beh, in realtà io me ne stavo fermo, mentre Slavya si stava riscaldando, ondeggiando al ritmo della musica."


translate italian day3_evening_sl_588cc129:


    "Mi stavo già pentendo di aver accettato quello spiacevole invito."


translate italian day3_evening_sl_8bd9ab89:


    th "Ok, eccomi qui, di nuovo in viaggio, e adesso?"


translate italian day3_evening_sl_3987eb50:


    "Ho esaminato i pionieri che stavano danzando attorno a noi."


translate italian day3_evening_sl_7cf46727:


    th "No! Non sono pronto per tutto questo."


translate italian day3_evening_sl_6a467bdf:


    "La musica si è fermata di colpo."


translate italian day3_evening_sl_dd530cb7:


    "Alcuni stavano ancora ballando, ma la maggioranza aveva smesso."


translate italian day3_evening_sl_a231e522:


    mt "Niente panico!"


translate italian day3_evening_sl_cdf4ba52:


    "La leader ha preso il controllo della postazione da DJ e ha aggiustato qualcosa."


translate italian day3_evening_sl_80ab8af2:


    mt "Prossima canzone – che le Signore invitino i Gentiluomini!"


translate italian day3_evening_sl_69f37ce2:


    "Ho guardato Slavya."


translate italian day3_evening_sl_e0a7c20b:


    "Mi ha porto la mano sorridendo."


translate italian day3_evening_sl_660ca959:


    "Non si dovrebbe rifiutare la proposta di una ragazza."


translate italian day3_evening_sl_480cbce8:


    "Abbiamo danzato per qualche minuto."


translate italian day3_evening_sl_3ff4b37b:


    "Il suo petto ha iniziato a muoversi più velocemente, mentre il suo viso diventava sempre più rosso."


translate italian day3_evening_sl_98bcf9db:


    "Slavya mi ha fissato negli occhi."


translate italian day3_evening_sl_77871c15:


    "Ho provato ad abbassare lo sguardo, a guardare verso gli altri pionieri, o da qualche altra parte."


translate italian day3_evening_sl_a4bce591:


    "Era una strana sensazione: il mio corpo rabbrividiva di tanto in tanto, ma era un brivido piacevole, senza alcuna preoccupazione o vergogna."


translate italian day3_evening_sl_d4eb018d:


    "La mia mente è diventata miracolosamente tranquilla."


translate italian day3_evening_sl_f5c6ebae:


    "Ho capito che non volevo lasciar andare quella ragazza, e di essere pronto a ballare con lei per l'eternità!"


translate italian day3_evening_sl_5cf561ad:


    "Ulyana si è fermata per un istante, saltellando accanto a noi e facendo un sorriso malizioso."


translate italian day3_evening_sl_67912668:


    "Forse me lo sono solo immaginato, ma ho avuto la sensazione che quella insolente ragazzina stesse alludendo a qualcosa."


translate italian day3_evening_sl_1eafcf89:


    "Le ho fatto una smorfia paurosa, ma lei si era già allontanata."


translate italian day3_evening_sl_a428f2b1:


    sl "C'è qualcosa che non va?"


translate italian day3_evening_sl_e6509443:


    "Mi ha chiesto Slavya, tranquillamente."


translate italian day3_evening_sl_fb700e8f:


    me "È tutto a posto...{w} Voglio dire, sta andando alla grande."


translate italian day3_evening_sl_46fbad31:


    "Non ero ancora arrivato al punto di balbettare, ma c'ero quasi."


translate italian day3_evening_sl_df85f677:


    sl "Sembri così teso."


translate italian day3_evening_sl_89875d8e:


    me "Un po'."


translate italian day3_evening_sl_9fb02d3f:


    "Le ho rivelato, con sincerità."


translate italian day3_evening_sl_478c945d:


    sl "Ai ragazzi non piace ballare."


translate italian day3_evening_sl_48176bfb:


    me "Immagino di sì..."


translate italian day3_evening_sl_43c3e662:


    "Non ha aggiunto altro, si è limitata a sorridere."


translate italian day3_evening_sl_a20cefa7:


    "..."


translate italian day3_evening_sl_b2671e23:


    "La canzone è terminata."


translate italian day3_evening_sl_97f9a3fb:


    "Stavo ancora tenendo Slavya per mano, ma lei ha abbandonato con facilità la mia presa."


translate italian day3_evening_sl_42e06c7b:


    sl "Grazie per il ballo!"


translate italian day3_evening_sl_39a579dc:


    me "Dovrei essere io a ringraziarti."


translate italian day3_evening_sl_ff3d4c64:


    "Ci siamo guardati per un po'."


translate italian day3_evening_sl_ed185ba1:


    "La canzone successiva è iniziata."


translate italian day3_evening_sl_69421a79:


    sl "Un altro?"


translate italian day3_evening_sl_0a5e88b4:


    me "No, credo che mi riposerò un po'."


translate italian day3_evening_sl_144a26aa:


    "Ho cercato di abbandonare la piazza il prima possibile."


translate italian day3_evening_sl_46643faf:


    "In qualche modo sono finito alla spiaggia..."


translate italian day3_evening_sl_b8ec268f:


    th "Perché sono fuggito?"


translate italian day3_evening_sl_52f987f3:


    "Sembrava una fuga vera e propria."


translate italian day3_evening_sl_a727c48b:


    th "Il ballo è andato bene e Slavya era contenta."


translate italian day3_evening_sl_c5789e6f:


    "Ma c'era qualcosa che non andava, il solo pensare a quel ballo mi rendeva agitato."


translate italian day3_evening_sl_915c2c39:


    "Forse avevo perso il controllo della situazione e ho ceduto alle emozioni?"


translate italian day3_evening_sl_073908ff:


    "Mi capita raramente."


translate italian day3_evening_sl_e79ce22b:


    "Io cerco sempre di essere calmo e di valutare bene la situazione.{w} Ma forse questa volta non ci sono riuscito…"


translate italian day3_evening_sl_cd66cbff:


    "Mi sono seduto sulla sabbia e ho osservato il fiume."


translate italian day3_evening_sl_8a61b36d:


    "La luna splendeva al di sopra dell'acqua."


translate italian day3_evening_sl_71e227a7:


    "Sono stato travolto dai ricordi."


translate italian day3_evening_sl_98e1accf:



    nvl clear
    "Avevo sei anni. Andai a pescare con mio padre. Tirai goffamente l'amo nell'acqua e attesi. Un minuto, due, dieci. Nessun pesce. Nel frattempo, mio padre ne catturava uno dopo l'altro."


translate italian day3_evening_sl_01fe6588:


    me "Ma come ci riesci?"


translate italian day3_evening_sl_454a26a0:


    "Mi spiegò la tecnica. Ma non ricordo tutti i dettagli."


translate italian day3_evening_sl_cd97be40:


    "Avevo nove anni. Accanto alla nostra casa c'era un oscuro edificio in rovina, pieno di fantasmi, vampiri, o semplicemente vagabondi. Un'instabile scala di legno che portava al secondo piano, sospesa sopra l'abisso – un buco nel seminterrato."


translate italian day3_evening_sl_0a0165e9:


    "Ero spaventato, ma nonostante ciò salivo e scendevo da quella scala ogni giorno. Una mattonella mi cadde sul piede e mi si ruppe un'unghia."


translate italian day3_evening_sl_742d2cdc:


    "Avevo dodici anni. La mia prima vittoria ad un torneo di videogiochi. Era un gioco di combattimento per console. Una folla immaginaria applaudiva la mia strabiliante vittoria."


translate italian day3_evening_sl_6fffdf4d:



    nvl clear
    "Avevo quindici anni. Segnai una serie di meravigliosi goal di tacco. E un tiro stupendo a trenta metri dalla porta, che finì in rete proprio sotto la traversa…"


translate italian day3_evening_sl_20e21081:


    "Avevo diciassette anni. Il mio primo amore. Immagini transitorie, un'ombra scivolò via sparendo nella foschia della città. La cercai oltre l'angolo, passando di casa in casa, ma era sparita…"


translate italian day3_evening_sl_b039b984:


    "I successivi ricordi erano più nitidi, ma meno vividi."


translate italian day3_evening_sl_294cee60:


    "I ricordi dell'infanzia e della giovinezza invece erano confusi. Mancavano i dettagli, e non riuscivo a ricordare il volto delle persone. Certe volte mi sembravano delle tele ricoperte di spruzzi di vernice."


translate italian day3_evening_sl_f7b289c8:


    "Ma le insite emozioni di quei vaghi ricordi erano molto più fervide di quelle dei miei ricordi più recenti."


translate italian day3_evening_sl_76b2fe88:


    nvl clear


translate italian day3_evening_sl_a20cefa7_1:


    "..."


translate italian day3_evening_sl_6792bf1f:


    sl "Eccoti!"


translate italian day3_evening_sl_f493af42:


    "Slavya si è chinata verso di me.{w} Ma non mi sono spaventato."


translate italian day3_evening_sl_93544105:


    sl "Perché te ne sei andato così presto?"


translate italian day3_evening_sl_3b07d9cd:


    me "Volevo stare un po' da solo."


translate italian day3_evening_sl_e79530ba:


    "Era come se mi fossi rinchiuso dentro a una conchiglia, senza contatti con il mondo esterno, rispondendo alla stessa maniera a tutti gli stimoli.{w} Ero come «Le tre scimmie sagge» giapponesi.{w} «Non vedo, non sento, non parlo»."


translate italian day3_evening_sl_3a8dd74d:


    sl "Non è magnifico qui?"


translate italian day3_evening_sl_21eebc87:


    "Slavya stava osservando il fiume."


translate italian day3_evening_sl_63d245c0:


    me "Sì, lo è…"


translate italian day3_evening_sl_d3e0ec8b:


    sl "Ti va un altro ballo?"


translate italian day3_evening_sl_70849ba4:


    me "No, grazie. Sono a posto."


translate italian day3_evening_sl_c8cd5039:


    "Dentro di me sapevo che non dovevo essere così duro con Slavya, ma ero come una marionetta, costretto a dire cose che non avrei detto in altre circostanze."


translate italian day3_evening_sl_29dcd13b:


    sl "A cosa stai pensando?"


translate italian day3_evening_sl_f0e61106:


    me "A niente…"


translate italian day3_evening_sl_3da81d34:


    sl "Non si può non pensare a niente!"


translate italian day3_evening_sl_b29aedbf:


    "Ha messo il broncio, ma subito dopo ha fatto un sorriso, facendomi capire che non si era offesa."


translate italian day3_evening_sl_4ab2d20d:


    sl "Si pensa sempre a qualcosa, anche senza accorgersene."


translate italian day3_evening_sl_6ab05865:


    me "Può essere."


translate italian day3_evening_sl_fbb41494:


    sl "E adesso a cosa stai pensando?"


translate italian day3_evening_sl_a3db8fcb:


    me "Sto pensando ai gufi."


translate italian day3_evening_sl_870d8d2c:


    "Le ho detto la prima cosa che mi è passata per la testa."


translate italian day3_evening_sl_37437f1e:


    sl "Perché ai gufi?"


translate italian day3_evening_sl_ce3666e5:


    "Ha riso."


translate italian day3_evening_sl_595d7b8c:


    me "Non lo so."


translate italian day3_evening_sl_cd57019d:


    sl "Hai mai visto un gufo?"


translate italian day3_evening_sl_d490fd5c:


    me "Certo."


translate italian day3_evening_sl_05e130d4:


    th "Che domanda sciocca…"


translate italian day3_evening_sl_1537169b:


    sl "Ti piacciono?"


translate italian day3_evening_sl_66bebe85:


    me "Sono dei normali uccelli, come tutti gli altri."


translate italian day3_evening_sl_1dcb4b49:


    sl "Sono uccelli notturni."


translate italian day3_evening_sl_26b4f833:


    "Ha precisato."


translate italian day3_evening_sl_c6d4e117:


    me "Sì, uccelli notturni."


translate italian day3_evening_sl_67f5dc8a:


    th "Uccelli notturni..."


translate italian day3_evening_sl_f08d71cf:


    "Ho pensato a un gufo – una piccola palla piumata con due occhi enormi."


translate italian day3_evening_sl_35f51a31:


    sl "E tu sei un gufo notturno?"


translate italian day3_evening_sl_e54b39e9:


    me "Cosa?"


translate italian day3_evening_sl_0db8f540:


    sl "Sai, ci sono le allodole e i gufi. Ad alcuni piace alzarsi presto la mattina, altri preferiscono dormire fino a tardi."


translate italian day3_evening_sl_e90153b6:


    "Le sue parole mi hanno distolto dai miei pensieri."


translate italian day3_evening_sl_a6f160d4:


    "Ho immaginato la mia stanza, piena di robaccia, una montagna di piatti sporchi da lavare, la chitarra abbandonata in un angolo, la cravatta appesa al lampadario, e, come ciliegina sulla torta, il mucchio di calzini sporchi sotto al letto."


translate italian day3_evening_sl_4ac82707:


    "Ero decisamente un gufo notturno.{w} La notte era il mio periodo."


translate italian day3_evening_sl_2e21ef54:


    "Ma in qualche modo riuscivo ad alzarmi presto in questo Campo."


translate italian day3_evening_sl_dc4d60ee:


    me "Lo so."


translate italian day3_evening_sl_b712550e:


    sl "E quindi tu cosa sei?"


translate italian day3_evening_sl_1d219eb9:


    me "Un gufo notturno, credo…{w} Mi piace dormire."


translate italian day3_evening_sl_6b0f0e89:


    sl "E io sono un'allodola.{w} Prima mi alzo, più cose posso fare durante il giorno."


translate italian day3_evening_sl_8392621f:


    "Non avendo mai niente di speciale da fare, non vedevo alcuna differenza tra il giorno e la notte.{w} Anzi, la notte era più calma e tranquilla rispetto al giorno."


translate italian day3_evening_sl_09bf6d3d:


    me "Slavya, non hai proprio nessun problema nella tua vita?"


translate italian day3_evening_sl_5240e44d:


    sl "Cosa intendi dire?"


translate italian day3_evening_sl_a4614188:


    "Mi ha chiesto incuriosita."


translate italian day3_evening_sl_77f4a782:


    me "Sei sempre così felice, pronta ad aiutare gli altri, così vogliosa di fare."


translate italian day3_evening_sl_7f5e995c:


    me "Come se niente possa turbarti."


translate italian day3_evening_sl_af407039:


    sl "Perché?!"


translate italian day3_evening_sl_ec2c2a7c:


    "Ha riso."


translate italian day3_evening_sl_cbff0809:


    sl "Sono solo una persona qualunque."


translate italian day3_evening_sl_7c783ece:


    th "È vero.{w} Una persona qualunque in un luogo qualunque."


translate italian day3_evening_sl_cce49fa3:


    me "A volte mi sento come un intruso qui."


translate italian day3_evening_sl_52f705e3:


    sl "Non ti piace questo Campo?"


translate italian day3_evening_sl_8f568239:


    me "Non intendo solo il Campo.{w} Nella mia precedente…"


translate italian day3_evening_sl_2a7a8a3b:


    "Mi sono bloccato."


translate italian day3_evening_sl_4b44803d:


    th "Non dovrei rivelare i miei segreti, nemmeno a una come lei."


translate italian day3_evening_sl_5d943aa7:


    me "Anche a casa.{w} Non mi sento simile agli altri. Non mi sento di appartenere a quel luogo…"


translate italian day3_evening_sl_4b441221:


    sl "E dai, smettila! Non devi pensare così."


translate italian day3_evening_sl_81571d87:


    th "Fino a un momento fa mi stavo isolando dal resto del mondo, e adesso invece sto aprendo il mio cuore a questa ragazza."


translate italian day3_evening_sl_15fa3132:


    th "Che mi sta succedendo?"


translate italian day3_evening_sl_97fb9be9:


    me "No, davvero. In altre circostanze non mi avresti nemmeno considerato.{w} Noi due siamo così diversi. Io sono pigro, poco socievole e non ho particolari abilità."


translate italian day3_evening_sl_45a7a568:


    me "Sarei l'ultima persona che noteresti in una folla di una grande città.{w} Anche se è piuttosto raro per me uscire di casa."


translate italian day3_evening_sl_618ab2ac:


    sl "Semyon, mi stai spaventando."


translate italian day3_evening_sl_453d16cb:


    "Mi ha guardato in modo serio."


translate italian day3_evening_sl_d4064844:


    "Ero in imbarazzo, ma non ho distolto lo sguardo."


translate italian day3_evening_sl_aad6bab5:


    me "Non è forse così?"


translate italian day3_evening_sl_738f16d6:


    sl "Certo che no!{w} Tu sei tu. Non esiste nessun altro come te. Ti manca solo un po' di autostima e di pazienza, e vedrai che riuscirai a fare tutto quello che vorrai!{w} Ne sono certa."


translate italian day3_evening_sl_c4f4bcb6:


    "Ho guardato il cielo."


translate italian day3_evening_sl_3b021425:


    me "Se solo fosse così semplice come dici…"


translate italian day3_evening_sl_b574b5d1:


    sl "Ma non è per nulla difficile! Comincia da adesso!"


translate italian day3_evening_sl_1ad35916:


    me "Cominciare cosa?"


translate italian day3_evening_sl_7109d569:


    sl "A cambiare!"


translate italian day3_evening_sl_ee1a91b3:


    th "Così, su due piedi?"


translate italian day3_evening_sl_ea49f8ff:


    me "E come credi che si possa fare?"


translate italian day3_evening_sl_541b7562:


    sl "Dovremmo fare qualcosa di utile!"


translate italian day3_evening_sl_e473fa2f:


    me "Ad esempio?"


translate italian day3_evening_sl_7db509b3:


    sl "Hmm…"


translate italian day3_evening_sl_60f3d758:


    "Ha pensato un attimo."


translate italian day3_evening_sl_44f18559:


    sl "Puliamo la piazza.{w} Il ballo dev'essere già finito!"


translate italian day3_evening_sl_6e3178d0:


    me "Solo noi due?"


translate italian day3_evening_sl_bd88c88c:


    th "Saremmo in grado di fare tutto da soli?"


translate italian day3_evening_sl_be7f9c31:


    sl "Non c'è molto da fare.{w} Il primo passo è quello che conta!"


translate italian day3_evening_sl_c3348a75:


    th "Sono d'accordo con lei su quel punto, ma togliere le luci dagli alberi in questo buio, spostare quelle pesanti casse audio e spazzare…"


translate italian day3_evening_sl_03eb9a2e:


    "Niente di tutto questo era sulla mia lista delle cose da fare."


translate italian day3_evening_sl_0e6e2d6e:


    "Ma non sapevo come rifiutare la sua proposta in modo cortese."


translate italian day3_evening_sl_615361ab:


    me "Forse è meglio se lo facciamo domani?{w} Assieme agli altri?"


translate italian day3_evening_sl_c0da7d07:


    sl "Perché rimandare a domani?{w} Non sarebbe magnifico trovare la piazza bella pulita durante l'allineamento?"


translate italian day3_evening_sl_8cede868:


    th "Per prima cosa, gli allineamenti non mi piacciono nemmeno."


translate italian day3_evening_sl_5b4109bc:


    me "Senti, ti capisco…{w} Ma ci sarebbe un po' troppo da fare a quest'ora!"


translate italian day3_evening_sl_bbc1f2e4:


    "Slavya ci ha pensato su."


translate italian day3_evening_sl_245f16e1:


    sl "Sì, forse hai ragione."


translate italian day3_evening_sl_3e8d56f1:


    th "Anch'io ogni tanto posso aver ragione.{w} E lei può avere torto."


translate italian day3_evening_sl_d746a7a3:


    sl "Allora…"


translate italian day3_evening_sl_83de587e:


    "Si è stirata e ha sbadigliato."


translate italian day3_evening_sl_3b5d4e01:


    sl "Dormiamoci su!"


translate italian day3_evening_sl_3cd13b17:


    "Una buona idea, ma io volevo parlare con lei ancora un po'."


translate italian day3_evening_sl_186a1c59:


    me "Non è troppo presto…?"


translate italian day3_evening_sl_bf68bee5:


    sl "No!"


translate italian day3_evening_sl_8b29d788:


    "Ha detto contrariata."


translate italian day3_evening_sl_440467e0:


    sl "Se vuoi potrai dormire tutto il giorno domani."


translate italian day3_evening_sl_3dd3f806:


    me "Raccontami una favola allora!"


translate italian day3_evening_sl_75c35567:


    "Ho detto la prima cosa che mi è passata per la testa."


translate italian day3_evening_sl_e8eb4335:


    sl "Conosco solo quelle che ho letto nei libri…{w} Probabilmente le conosci anche tu."


translate italian day3_evening_sl_807b7383:


    th "Forse sì…"


translate italian day3_evening_sl_744ed324:


    "Che fiasco."


translate italian day3_evening_sl_ad3d2e0c:


    sl "Allora andiamo?"


translate italian day3_evening_sl_55ab51a9_1:


    "Mi ha porto la mano."


translate italian day3_evening_sl_6199f44f:


    me "Starò qui ancora un po', ho bisogno di aria fresca."


translate italian day3_evening_sl_76d0ac27:


    me "Tu puoi andare se vuoi! Buona notte!"


translate italian day3_evening_sl_5f3273c1:


    sl "Buona notte!"


translate italian day3_evening_sl_ec475aa3:


    "Slavya è corsa via."


translate italian day3_evening_sl_22e44be2:


    "Prima di andarmene volevo assaporare la mia totale sconfitta."


translate italian day3_evening_sl_4ff0543c:


    "Volevo che Slavya se ne andasse il prima possibile dopo quella mia brillante idea. Raccontarmi una favola? Ma che mi è preso?"


translate italian day3_evening_sl_335c00f5:


    th "Ecco come vanno le cose. Provi a parlare normalmente con una ragazza e alla fine si trasforma tutto in un fallimento totale."


translate italian day3_evening_sl_368a09ed:


    "Ho fissato il cielo notturno."


translate italian day3_evening_sl_7de6936f:


    "Le stelle, quelle piccole luci di corpi celesti remoti, brillavano allegramente come se stessero ridendo di me."


translate italian day3_evening_sl_b61a3a4c:


    "Mi sono chinato e ho sepolto il viso nella sabbia."


translate italian day3_evening_sl_10854c7a:


    "Dopo un istante mi sono accorto di quanto fossi stato stupido. Come sempre."


translate italian day3_evening_sl_a8978170:


    "Mi sono tirato su, sputando granelli di sabbia, e sono tornato alla casetta della leader."


translate italian day3_evening_sl_a20cefa7_2:


    "..."


translate italian day3_evening_sl_5195ec0b:


    "La schiena mi prudeva selvaggiamente durante l'intero tragitto."


translate italian day3_evening_sl_81f35693:


    "Anzi, non solo la schiena, ma dappertutto."


translate italian day3_evening_sl_533207f2:


    "Non c'era da stupirsi, dato che non mi lavavo decentemente da qualche giorno, mentre qui faceva davvero caldo, e quindi sudavo come un maiale."


translate italian day3_evening_sl_17474fa5:


    th "Dovrei cercare del sapone e un asciugamano."


translate italian day3_evening_sl_aadfea7e:


    "Sicuramente li avrei trovati nella stanza di Olga Dmitrievna."


translate italian day3_evening_sl_f8c52b26:


    "Dopo qualche minuto ero già alla piazza con i miei accessori da bagno."


translate italian day3_evening_sl_b0cbc573:


    th "Adesso devo solo trovare il bagno pubblico."


translate italian day3_evening_sl_3b2b772c:


    "È stato facile. L'edificio era proprio sul margine della foresta."


translate italian day3_evening_sl_662916c1:


    th "È davvero la posizione perfetta per un bagno pubblico."


translate italian day3_evening_sl_d2fefd9e:


    "Non credevo ci fosse Baba Yaga dentro, ma le luci all'interno erano accese."


translate italian day3_evening_sl_2ee9293b:


    me "E chi ha avuto la brillante idea di farsi una sauna nel cuore della notte?"


translate italian day3_evening_sl_2eb0d079:


    "Ho borbottato sottovoce."


translate italian day3_evening_sl_68584892:


    "C'era solo un problema – si trattava di un ragazzo o di una ragazza?"


translate italian day3_evening_sl_c7224776:


    "Nel primo caso potevamo stare dentro insieme senza problemi."


translate italian day3_evening_sl_289b8414:


    th "Non mi trovo molto a mio agio in questi bagni pubblici, ma posso farcela questa volta."


translate italian day3_evening_sl_0c723b2c:


    "Nel secondo caso invece avrei dovuto aspettare il mio turno, o mi sarei dovuto grattare per tutta la notte a causa del prurito."


translate italian day3_evening_sl_99683490:


    th "Chissà quanto a lungo dovrò aspettare…"


translate italian day3_evening_sl_9c652616:


    "Ho sbirciato dalla finestra.{w} Ma non riuscivo a vedere nulla a causa del vapore."


translate italian day3_evening_sl_de4b06b3:


    "Improvvisamente Slavya è apparsa davanti ai miei occhi, come da una fitta nebbia."


translate italian day3_evening_sl_3300e119:


    "Ovviamente era completamente nuda, come lo sarebbe stato chiunque lì dentro."


translate italian day3_evening_sl_f8f1d6e9:


    "L'ho fissata esterrefatto."


translate italian day3_evening_sl_45d87efb:


    "Non avevo mai visto una ragazza nuda così da vicino, nemmeno attraverso un vetro."


translate italian day3_evening_sl_4029c0f3:


    "Il mio corpo ha reagito di conseguenza."


translate italian day3_evening_sl_0ad798c3:


    "Gli impulsi nervosi si sono diffusi dai miei occhi al resto del mio corpo."


translate italian day3_evening_sl_c0894e89:


    "Non avrei abbandonato quella mia postazione di osservazione nemmeno se improvvisamente fosse scoppiata una guerra."


translate italian day3_evening_sl_addbff34:


    "Ma Slavya non si è accorta di me."


translate italian day3_evening_sl_defd30a4:


    "Si stava lavando piacevolmente. Sciacquandosi i capelli, strofinandosi la spugna sul corpo, e versandosi addosso l'acqua da un secchio."


translate italian day3_evening_sl_4cdeecd0:


    "Poi ha iniziato a lavarsi i capelli."


translate italian day3_evening_sl_f24eb2b8:


    "Non riuscivo a immaginare quanto tempo le servisse per lavarsi tutti quei capelli, ma i minuti scorrevano come secondi, essendo io così incantato da quello spettacolo."


translate italian day3_evening_sl_a20cefa7_3:


    "..."


translate italian day3_evening_sl_9d93e651:


    "Finalmente ha finito di lavarsi, sospirando con soddisfazione, e si è diretta verso la porta."


translate italian day3_evening_sl_a20cefa7_4:


    "..."


translate italian day3_evening_sl_4ad1145a:


    "Mi sono ripreso non appena lei è entrata nello spogliatoio, e mi sono nascosto nei cespugli lì vicino."


translate italian day3_evening_sl_9d9ca7f3:


    "Avrei fatto meglio a non sfidare la sorte e ad andarmene subito, ma ormai era troppo tardi."


translate italian day3_evening_sl_bf131a4a:


    "Slavya è apparsa sulla veranda dopo un instante, è rimasta lì per un po', godendosi la brezza notturna, e si è incamminata in direzione del mio nascondiglio."


translate italian day3_evening_sl_c31ff7a6:


    "Avevo decine di scuse nella mia mente, ma lei mi è passata davanti senza nemmeno degnare di uno sguardo i cespugli dentro i quali mi stavo nascondendo."


translate italian day3_evening_sl_efff4fc1:


    th "Che fortuna..."


translate italian day3_evening_sl_62c6e320:


    "Sono uscito con cautela e sono tornato alla casetta di Olga Dmitrievna, dimenticando completamente il motivo per cui mi ero recato in quel luogo."


translate italian day3_evening_sl_a085be61:


    "Ero totalmente esausto e avevo solo voglia di lasciarmi cadere sul letto e staccare la spina."


translate italian day3_evening_sl_d8f66d7a:


    "I miei occhi non potevano più aspettare, chiudendosi da soli di tanto in tanto."


translate italian day3_evening_sl_a936b78e:


    "Era pericoloso persino battere le ciglia."


translate italian day3_evening_sl_3d48081a:


    "Non conoscevo ancora abbastanza bene questo Campo per poter raggiungere la mia destinazione a occhi chiusi."


translate italian day3_evening_sl_a20cefa7_5:


    "..."


translate italian day3_evening_sl_44fae48f:


    "Improvvisamente Slavya è apparsa davanti a me."


translate italian day3_evening_sl_3820771a:


    me "..."


translate italian day3_evening_sl_6de32e31:


    sl "Stai andando a dormire?"


translate italian day3_evening_sl_da49764c:


    me "Beh... sì..."


translate italian day3_evening_sl_97fc1ed6:


    "In quel momento mi è tornata in mente la sua immagine nella sauna, e ho distolto timidamente lo sguardo."


translate italian day3_evening_sl_b78c6e26:


    me "E tu?"


translate italian day3_evening_sl_7405f4b0:


    sl "Sto facendo le pulizie!"


translate italian day3_evening_sl_2214025f:


    "Mi ha mostrato la scopa che teneva dietro la schiena."


translate italian day3_evening_sl_dfc6b36d:


    "Slavya, con una scopa in una piazza deserta nel cuore della notte, sembrava la strega di un racconto per bambini."


translate italian day3_evening_sl_e07c37b3:


    sl "Cosa c'è?"


translate italian day3_evening_sl_62fd010a:


    me "Niente.{w} Ma credi che sia una buona idea fare le pulizie dopo essersi fatti il bagno?"


translate italian day3_evening_sl_f52a7f9e:


    sl "E tu come fai a sapere che mi sono fatta il bagno?"


translate italian day3_evening_sl_28bebef0:


    "Ho sentito il terrore invadermi completamente, il sudore freddo ha iniziato a scorrermi lungo la schiena, i miei pensieri si sono congelati, non riuscivo a inventarmi una scusa."


translate italian day3_evening_sl_3820771a_1:


    me "..."


translate italian day3_evening_sl_a684124e:


    "Slavya mi stava ancora fissando sorpresa, in attesa di una risposta."


translate italian day3_evening_sl_888019d6:


    "Pareva che la mia vita dipendesse da quella risposta."


translate italian day3_evening_sl_12308c9c:


    me "Beh. I tuoi capelli sono bagnati..."


translate italian day3_evening_sl_1f26ca7d:


    sl "Oh, hai ragione."


translate italian day3_evening_sl_5d0b4e88:


    "Volevo solo essere inghiottito dalle profondità della Terra, per sparire completamente da questo mondo così com'ero sparito dal {i}mio{/i}."


translate italian day3_evening_sl_c493719d:


    sl "Anche tu stai andando a farti un bagno?"


translate italian day3_evening_sl_2e67d98e:


    me "Io... beh..."


translate italian day3_evening_sl_3a1790bb:


    "Slavya ha guardato la borsa che tenevo tra le mani."


translate italian day3_evening_sl_92d5dd33:


    me "Sì, ci stavo andando! Buona notte allora!"


translate italian day3_evening_sl_6b587b4c:


    "Mi sono girato e sono andato verso la sauna."


translate italian day3_evening_sl_63c688cb:


    th "Che disastro, che vergogna..."


translate italian day3_evening_sl_09404bd9:


    "Camminavo lentamente, maledicendomi per tutto quello che era successo."


translate italian day3_evening_sl_ee7118ff:


    th "Per prima cosa non avrei dovuto spiare."


translate italian day3_evening_sl_e5405609:


    th "E se proprio dovevo farlo, almeno avrei potuto stare più attento e inventarmi una scusa quando ero ancora in tempo."


translate italian day3_evening_sl_45531a98:


    th "Oh..."


translate italian day3_evening_sl_a20cefa7_6:


    "..."


translate italian day3_evening_sl_0cfbc599:


    "Non ho impiegato molto tempo a lavarmi."


translate italian day3_evening_sl_9dc5cc01:


    "Sembrava che anche la notte avesse avuto il suo ballo, o forse un concerto.{w} Le stelle erano le luci. Gli uccellini e gli insetti erano l'orchestra, un gufo era il direttore. Il vento e il fruscio delle foglie erano gli applausi del pubblico."


translate italian day3_evening_sl_36d666ac:


    "La notte appare molto più bella quando te ne stai nella foresta pulito e lavato."


translate italian day3_evening_sl_501fc9a9:


    "All'improvviso ho sentito un rumore provenire dai cespugli lì vicino."


translate italian day3_evening_sl_7492209c:


    "Mi ha fatto rabbrividire, ma non mi sono spaventato più di tanto."


translate italian day3_evening_sl_c160bc4e:


    th "Potrebbe essere uno scoiattolo o qualche altro animale."


translate italian day3_evening_sl_a710287b:


    th "Comunque è meglio controllare."


translate italian day3_evening_sl_add052f9:


    "Mi sono avvicinato ai cespugli."


translate italian day3_evening_sl_f110d6af:


    "Ma non c'era niente."


translate italian day3_evening_sl_869f707c:


    th "Forse me lo sono immaginato..."


translate italian day3_evening_sl_bd3b5e0d:


    "Con corpo pulito e mente fresca, sono tornato nella casetta della leader."


translate italian day3_evening_sl_1424964a:


    "Olga Dmitrievna stava già dormendo."


translate italian day3_evening_sl_01ffcfe1:


    "Sono entrato nel letto senza svestirmi e mi sono coperto con il lenzuolo."


translate italian day3_evening_sl_b675339f:


    "Non riuscivo ad addormentarmi, pensando a Slavya, alla sua nudità, immagini che non avrebbero potuto essere oscurate dal mio desiderio di scoprire la verità su questo luogo."


translate italian day3_evening_un_dee15e4b:


    "Un solo altro invito a ballare e il mio orgoglio sarebbe stato rovinato del tutto."


translate italian day3_evening_un_f61e6089:


    "Ho iniziato a pensare a una scusa appropriata per andarmene, aspettando il momento giusto per farlo, quando ho visto Lena.{w} Si stava avvicinando lentamente verso di me."


translate italian day3_evening_un_b9dd006a:


    un "Forse dovremmo andare?"


translate italian day3_evening_un_25f7abbc:


    me "E dove?"


translate italian day3_evening_un_f12b72b3:


    "Ero così immerso nei miei pensieri che non ho capito esattamente cosa volesse dire."


translate italian day3_evening_un_bf618d8b:


    un "All'infermeria.{w} Ma se vuoi restare qui a ballare…"


translate italian day3_evening_un_a35ed640:


    "Dubito che lei avrebbe danzato. Era rimasta in disparte per tutta la serata."


translate italian day3_evening_un_5b6584a0:


    me "No, non voglio ballare!{w} Andiamo."


translate italian day3_evening_un_ea985af0:


    th "Almeno non mi toccherà rimanere qui come un timido nerd!"


translate italian day3_evening_un_329854dd:


    "Davvero, cercare di restare nascosto in un angolo non era molto piacevole."


translate italian day3_evening_un_c5a5dd23:


    th "Persino un elefante in una cristalleria sarebbe più agile di me sulla pista da ballo."


translate italian day3_evening_un_3db6c6b7:


    "Non avevo la minima intenzione di ballare, in primo luogo."


translate italian day3_evening_un_68b672da:


    un "Bene. Andiamo all'infermeria?"


translate italian day3_evening_un_ef4b7608:


    "Lena mi ha fatto riemergere dai miei pensieri."


translate italian day3_evening_un_3b8ef189:


    "Siamo rimasti fermi davanti alla mensa per un po'."


translate italian day3_evening_un_95bffd3a:


    me "Sì, certo… Grazie!"


translate italian day3_evening_un_2e3d9fb1:


    un "Di cosa?"


translate italian day3_evening_un_f8a6296f:


    "Mi ha chiesto sorpresa."


translate italian day3_evening_un_177d6e89:


    me "Beh… Per avermi portato via da là…"


translate italian day3_evening_un_cea5943f:


    "Non avrei dovuto dirle che danzare non è il mio forte."


translate italian day3_evening_un_966aaa9d:


    me "Era…{w} sai… così noioso là!"


translate italian day3_evening_un_fb773468:


    un "Non ti piace ballare, vero?"


translate italian day3_evening_un_d153fd8d:


    "Non c'era una singola traccia di sarcasmo nella sua sincera, e forse anche infantile, espressione."


translate italian day3_evening_un_5e7d152e:


    "Sembrava davvero che lei non capisse."


translate italian day3_evening_un_bae3aca3:


    me "Già. Non mi piace per niente. Non fa per me."


translate italian day3_evening_un_5b72d7bd:


    un "Nemmeno a me.{w} Nessuno mi invita mai a ballare."


translate italian day3_evening_un_2f7bb5d6:


    "Lena è arrossita e ha fissato il terreno come al solito."


translate italian day3_evening_un_4cb51946:


    me "Strano."


translate italian day3_evening_un_940302b3:


    un "Cosa è strano?"


translate italian day3_evening_un_f9720baf:


    me "Beh… Che nessuno ti inviti."


translate italian day3_evening_un_4429143b:


    un "Lo credi davvero?"


translate italian day3_evening_un_ca11e47a:


    "Mi ha guardato di nuovo con incomprensione e sorpresa."


translate italian day3_evening_un_92393659:


    "Mi sono sentito confuso e non sapevo come rispondere."


translate italian day3_evening_un_34c195e3:


    me "Sì! Certamente!{w} Se mi piacesse ballare, ti avrei invitata senz'altro."


translate italian day3_evening_un_082d3571:


    un "Grazie…"


translate italian day3_evening_un_e99c1593:


    "Non abbiamo detto una singola parola per il resto del tragitto."


translate italian day3_evening_un_542f1cb7:


    "Lena ovviamente era molto confusa dal mio goffo complimento, e io non sapevo quale argomento scegliere per la nostra conversazione."


translate italian day3_evening_un_c0de7e94:


    "A quell'ora era buio pesto, e il tetro edificio dell'infermeria, immerso nella nebbia notturna, sembrava proprio una casa infestata."


translate italian day3_evening_un_2079dd47:


    "Ho sentito un forte desiderio di girarmi e di andarmene senza fare alcun rumore."


translate italian day3_evening_un_13fbafd7:


    "Ho lanciato una rapida occhiata a Lena e ho notato che era la stessa di sempre – timida, riservata, insicura, ma per nulla spaventata."


translate italian day3_evening_un_6cdc223c:


    "La cosa mi ha fatto sentire ancora più a disagio."


translate italian day3_evening_un_095403d0:


    th "Non è possibile che lei non sia spaventata, mentre invece io..."


translate italian day3_evening_un_cc26ac1a:


    "Improvvisamente il bubolo di un gufo nelle vicinanze mi ha fatto rabbrividire."


translate italian day3_evening_un_cc73b68c:


    "Lena sembrava non averlo sentito, o sembrava che non le importasse, oppure che non avesse paura di esso."


translate italian day3_evening_un_9b14946c:


    "La terza opzione era improbabile, ma non volevo chiederglielo, mostrando il mio timore."


translate italian day3_evening_un_24cab478:


    "Sono entrato nell'infermeria e ho trovato l'interruttore della luce nell'oscurità."


translate italian day3_evening_un_49249630:


    me "L'infermiera verrà qui più tardi?"


translate italian day3_evening_un_404b569f:


    un "No, non verrà più…"


translate italian day3_evening_un_e0ce2859:


    th "Ok, non verrà più, ho capito…{w} Aspetta un secondo…{w} Che significa «non verrà più»?"


translate italian day3_evening_un_e1a4709a:


    me "Ah, capisco…"


translate italian day3_evening_un_5fa11b3d:


    "Non è che io avessi paura di restare da solo con Lena.{w} Dentro a una stanza chiusa.{w} Di notte.{w} Senza nessuno nei dintorni..."


translate italian day3_evening_un_36602009:


    th "Solo nei film accade qualcosa di brutto in questi casi, giusto?"


translate italian day3_evening_un_0216a72c:


    "È solo che mi trovavo insieme a Lena, e non con Ulyana o con Slavya."


translate italian day3_evening_un_83f4508f:


    "E ciò cambiava drasticamente il mio atteggiamento nei confronti delle cose che accadevano."


translate italian day3_evening_un_9e2d7d99:


    un "Ecco gli scatoloni."


translate italian day3_evening_un_923e6e0a:


    "Ha indicato gli scatoloni, accatastati alla rinfusa."


translate italian day3_evening_un_9066f7f1:


    "Ce n'erano una dozzina."


translate italian day3_evening_un_75fde160:


    "Ci vorrà molto più di dieci minuti per sistemarli tutti."


translate italian day3_evening_un_3a066bb2:


    "Ho appoggiato sul tavolo uno di quegli scatoloni e ho iniziato a rimuoverne il contenuto."


translate italian day3_evening_un_30cdf24e:


    "C'erano delle bende. Tanti piccoli pacchetti di bende."


translate italian day3_evening_un_b4c9e1a9:


    un "Ecco, prendi questo."


translate italian day3_evening_un_ec9bb664:


    "Lena mi ha consegnato un foglio di carta."


translate italian day3_evening_un_b5daf913:


    "Era una specie di tabella, e ho subito capito che dovevamo scrivere il nome a sinistra, la descrizione nel mezzo, e la quantità a destra."


translate italian day3_evening_un_50cb3d48:


    th "Non è esattamente un database ma è sufficiente."


translate italian day3_evening_un_48b8c403:


    "Abbiamo iniziato a lavorare a pieno ritmo."


translate italian day3_evening_un_2b543552:


    un "Semyon…"


translate italian day3_evening_un_ad02f53a:


    me "Cosa?"


translate italian day3_evening_un_0c8d272a:


    "Ho guardato Lena."


translate italian day3_evening_un_3500c639:


    "Mi ha fissato per qualche istante, come se volesse fare o dire qualcosa, ma poi ha abbassato nuovamente lo sguardo."


translate italian day3_evening_un_8da69387:


    un "No, niente."


translate italian day3_evening_un_9f980148:


    "Era fisicamente difficile per me stare seduto senza dire una parola.{w} Tuttavia non osavo attaccare discorso, non solo per il fatto che non avessi idea di cosa parlare, ma anche perché mi vergognavo. Praticamente ogni mia frase avrebbe potuto metterla in imbarazzo."


translate italian day3_evening_un_2b543552_1:


    un "Semyon…"


translate italian day3_evening_un_e79d0fd5:


    me "Sì?"


translate italian day3_evening_un_dbc3003f:


    un "Stai contando sempre le stesse."


translate italian day3_evening_un_e1d4a194:


    "In effetti ho iniziato a togliere e a catalogare nel «database» le bende che avevo appena registrato."


translate italian day3_evening_un_87731a54:


    me "Oh, scusa."


translate italian day3_evening_un_d00d3d0f:


    "Non ha risposto."


translate italian day3_evening_un_6b863b6b:


    me "Ehi, tu di dove sei? Voglio dire, da dove vieni? Cioè, dove sei nata? O meglio, dove abiti?"


translate italian day3_evening_un_4dbf341d:


    un "Beh, io…{w} C'è una città non lontano da qui…"


translate italian day3_evening_un_652ee7d0:


    me "Non lontano da qui. E precisamente?"


translate italian day3_evening_un_e807e738:


    un "Da qualche parte…"


translate italian day3_evening_un_adc0b8f3:


    "Sembrava che lei non ne volesse parlare."


translate italian day3_evening_un_270ee4bf:


    th "Forse anche Lena mi sta nascondendo qualcosa?"


translate italian day3_evening_un_1663f57c:


    "Se si fosse trattato di Olga Dmitrievna avrei potuto anche capire, ma Lena..."


translate italian day3_evening_un_7edc04e0:


    th "Non è assolutamente da lei."


translate italian day3_evening_un_4b79f78a:


    me "È un segreto?"


translate italian day3_evening_un_f4abdb32:


    un "No…{w} È solo…"


translate italian day3_evening_un_8a3717f6:


    me "Quindi... È anch'essa da qualche parte nel sud?"


translate italian day3_evening_un_9857e8f9:


    "Forse avrei dovuto pensare a qualcosa di più originale."


translate italian day3_evening_un_6791c4d1:


    "Ogni mia singola idea improvvisa veniva fuori nel modo sbagliato, e il mio tentativo di giocare a fare l'investigatore stava andando anche peggio."


translate italian day3_evening_un_fa0dd942:


    "E ho pensato al sud solo perché l'unico campo di pionieri dell'Unione Sovietica di cui ricordavo l'esistenza era un certo «Artek»."


translate italian day3_evening_un_c81cda2f:


    un "… Sì…"


translate italian day3_evening_un_e39caf93:


    "Lena ha esitato a rispondere."


translate italian day3_evening_un_45925f2f:


    "Non saprei dire con certezza se stesse mentendo oppure no."


translate italian day3_evening_un_3dfaf878:


    un "Non ti piace qui?"


translate italian day3_evening_un_38d9fd3d:


    th "Mi chiedo quale parte del mio discorso le abbia suggerito una cosa simile?"


translate italian day3_evening_un_8610e699:


    me "No, no! Certo che mi piace qui!"


translate italian day3_evening_un_f13d454c:


    "Quella mia falsa cordialità suonava poco sincera, per compiacimento."


translate italian day3_evening_un_1a377d44:


    me "E invece tu?"


translate italian day3_evening_un_cc213e9d:


    un "Mi piace.{w} È molto tranquillo qui, ci sono molti libri da leggere nella biblioteca.{w} E la gente è a posto…"


translate italian day3_evening_un_a3126422:


    me "A posto… ma non proprio tutti…"


translate italian day3_evening_un_b672514d:


    un "Perché?"


translate italian day3_evening_un_6cde6e35:


    "Credevo che quell'ultima frase fosse rimasta solo nei miei pensieri."


translate italian day3_evening_un_4c03a0c5:


    "E invece l'avevo detta ad alta voce."


translate italian day3_evening_un_339a1158:


    me "Beh, sai…{w} Ulyana, ad esempio – è come una batteria Energizer con una dissipazione di potenza disastrosa."


translate italian day3_evening_un_d39ab564:


    un "Una batteria…{w} cosa?"


translate italian day3_evening_un_4d8feba6:


    "Forse avevo davvero viaggiato indietro nel tempo."


translate italian day3_evening_un_6421514b:


    me "Non importa."


translate italian day3_evening_un_a656cdf8:


    me "O anche Alisa! Il detto «Un pioniere è l'ispirazione per i più giovani» di certo non le si addice! Se tutti iniziassero a comportarsi come lei, il paese collasserebbe nel giro di una ventina d'anni…"


translate italian day3_evening_un_61114f6a:


    th "Tuttavia, pensandoci bene, si potrebbe dire che tutti avessero seguito l'esempio di Alisa Dvachevskaya negli anni Ottanta, dove probabilmente mi trovo io adesso."


translate italian day3_evening_un_f187db47:


    un "In realtà lei non è come sembra."


translate italian day3_evening_un_76bd4f62:


    me "«Non è come sembra» – sembrare come?"


translate italian day3_evening_un_0a526940:


    un "Come l'hai descritta."


translate italian day3_evening_un_61c3831d:


    me "Innanzitutto, non ho detto niente di lei.{w} Ho solo fatto notare come lei non sia il migliore esempio da seguire."


translate italian day3_evening_un_21b0641e:


    un "Beh… Forse."


translate italian day3_evening_un_25b8be5c:


    me "Sembra che tu la conosca piuttosto bene."


translate italian day3_evening_un_cd1ed0e4:


    un "Può darsi."


translate italian day3_evening_un_2bf6a5e9:


    "Le ho fatto quella domanda solo per continuare la conversazione, non mi sarei aspettato una risposta del genere."


translate italian day3_evening_un_b3051fc6:


    "Alisa e Lena erano così diverse. Non riuscivo a credere che si conoscessero così bene."


translate italian day3_evening_un_68730dd3:


    un "Abitiamo nella stessa città."


translate italian day3_evening_un_508a08eb:


    "Come se mi avesse letto nel pensiero."


translate italian day3_evening_un_2a8c5047:


    un "Abbiamo alcuni amici in comune, anche se lei ha un anno in più di me."


translate italian day3_evening_un_eaa5b39b:


    me "Ok. È un po' strano.{w} Anzi, no. Sono solo sorpreso."


translate italian day3_evening_un_25ed7170:


    un "Tutti si sorprendono."


translate italian day3_evening_un_d7a43efd:


    "Lena ha sorriso debolmente."


translate italian day3_evening_un_0bf46b97:


    "Ho afferrato la seconda scatola."


translate italian day3_evening_un_f520e822:


    "Analgin, carbone attivo, Analgin, carbone attivo, soluzione salina, permanganato di potassio, Furacin, Analgin…"


translate italian day3_evening_un_88d5fb70:


    "Lena rispondeva quasi sempre a monosillabi."


translate italian day3_evening_un_2594eb2e:


    "Come potevo comunicare con lei se ogni nostra singola conversazione finiva sempre in un monologo o in un imbarazzante silenzio?"


translate italian day3_evening_un_d0a225ef:


    "Queste cose non facevano per me."


translate italian day3_evening_un_69f23bef:


    "Certe volte sembrava che nascondesse qualcosa dietro alla sua maschera di timidezza.{w} Ma cosa?"


translate italian day3_evening_un_52bebeb3:


    me "Sai, ho letto un libro non molto tempo fa…{w} Ti piacciono i racconti di fantascienza?"


translate italian day3_evening_un_267018a5:


    un "Non tanto."


translate italian day3_evening_un_eda10b2f:


    th "Dannazione. Un altro buco nell'acqua."


translate italian day3_evening_un_41202424:


    me "Beh, se non ti piacciono allora…{w} Che cosa ti piace leggere?"


translate italian day3_evening_un_25828f24:


    un "Diverse cose…"


translate italian day3_evening_un_c28a338c:


    "La conversazione non stava andando molto bene e ho deciso di troncarla lì."


translate italian day3_evening_un_c7228702:


    "Chissà perché, ma stavo ancora pensando al ballo di oggi."


translate italian day3_evening_un_7e05f767:


    "Quelle sensazioni di disagio, di malessere e di vergogna mi hanno nuovamente sopraffatto."


translate italian day3_evening_un_9a396b7b:


    "Sotto certi aspetti io e Lena siamo simili. Anche io sono timido e insicuro riguardo alle cose che non comprendo o che non sono in grado di fare."


translate italian day3_evening_un_54d0f688:


    th "Forse come prima cosa dovrei cercare di superare le mie paure."


translate italian day3_evening_un_cd4e8ca1:


    th "E ciò potrebbe aiutarmi a capirla meglio."


translate italian day3_evening_un_7c4840c7:


    "Alla fine mi sono deciso."


translate italian day3_evening_un_f8fb534c:


    me "Sono rimaste solo poche scatole…"


translate italian day3_evening_un_f1be5fed:


    un "Sì."


translate italian day3_evening_un_e1fb3662:


    me "Ehi. Avrei un'idea.{w} Che ne dici di andare alla mensa dopo?{w} Credo che abbiano portato lì il registratore dopo il ballo."


translate italian day3_evening_un_9a25521c:


    th "E perché mai l'avrebbero dovuto portare proprio alla mensa?"


translate italian day3_evening_un_3db7cd22:


    "Ho esitato."


translate italian day3_evening_un_41cfffd5:


    un "Per che cosa?"


translate italian day3_evening_un_4125d583:


    "Il suo sguardo di sincera curiosità indicava che non aveva la minima idea di cosa le stessi proponendo."


translate italian day3_evening_un_1279bc8e:


    me "Beh… A essere onesti, non mi piace ballare perché non ne sono capace.{w} Ecco perché mi trovavo a disagio durante il ballo."


translate italian day3_evening_un_054e8b5f:


    me "Forse potremmo provare insieme."


translate italian day3_evening_un_8ebabae6:


    un "Ma io…"


translate italian day3_evening_un_7c35a05f:


    "Ha smesso per un attimo di sistemare i medicinali, è arrossita e mi ha guardato dritto negli occhi."


translate italian day3_evening_un_7f15ab29:


    "Non so cosa mi fosse preso."


translate italian day3_evening_un_72e48efd:


    th "È davvero un'idea stupida."


translate italian day3_evening_un_99ad9909:


    me "Ma va bene, se non vuoi…{w} Non insisto!"


translate italian day3_evening_un_d8e58db9:


    un "E se qualcuno ci vedesse?"


translate italian day3_evening_un_5226ed1b:


    "Non avevo pensato a questa cosa."


translate italian day3_evening_un_9cb1a370:


    "Non era un grosso problema.{w} Anche se…"


translate italian day3_evening_un_606af4d5:


    me "Tutti staranno dormendo a quest'ora.{w} E nessuno andrebbe alla mensa di notte!"


translate italian day3_evening_un_3855e2ee:


    un "E come facciamo a entrare?"


translate italian day3_evening_un_6e8ce990:


    "Ecco un altro piccolo dettaglio a cui non avevo pensato."


translate italian day3_evening_un_962242d8:


    me "Beh…"


translate italian day3_evening_un_e9f68551:


    "Avevo le chiavi di Slavya, ma non volevo che si sapesse in giro. Qualcuno si sarebbe potuto fare delle idee sul fatto che io le avessi rubate."


translate italian day3_evening_un_964defe1:


    "In quel momento mi sono pentito del fatto di non aver preso con me il telefono cellulare dalla stanza della leader."


translate italian day3_evening_un_4ee977cd:


    "Olga Dmitrievna aveva iniziato a parlare, e me l'ero scordato."


translate italian day3_evening_un_6f016ce7:


    "Comunque, dovevo inventarmi qualcosa per deviare l'attenzione da quello scomodo dettaglio."


translate italian day3_evening_un_7e3db1d5:


    me "Ehmm, che tipo di musica ascolti?"


translate italian day3_evening_un_9c66e26a:


    un "Diversi tipi…{w} Ma non sono particolarmente appassionata."


translate italian day3_evening_un_f8be0238:


    me "Allora immagina che adesso stia suonando…{w} Voglio dire, immagina… immaginiamo di sentirla."


translate italian day3_evening_un_52f34a61:


    un "E come?"


translate italian day3_evening_un_0c4b535c:


    me "Come se suonasse nella tua testa!"


translate italian day3_evening_un_43e2549c:


    "La musica del ballo mi risuonava ancora nella mente.{w} Mi ricordavo bene sia le note che le parole."


translate italian day3_evening_un_db88fd32:


    un "Non sono sicura di riuscirci."


translate italian day3_evening_un_7b5327be:


    me "Puoi provarci."


translate italian day3_evening_un_3c68ea6c:


    un "Forse…"


translate italian day3_evening_un_04cfc77c:


    "Ha accettato la mia proposta!"


translate italian day3_evening_un_7ddfa955:


    "Nel caso di Lena, quel «forse» poteva essere considerato come un «sì»."


translate italian day3_evening_un_a20cefa7:


    "..."


translate italian day3_evening_un_067ecc79:


    "Il tempo restante l'abbiamo trascorso ordinando i medicinali, annotandone i nomi e le quantità."


translate italian day3_evening_un_f7242642:


    "Dopo un tale colpo di fortuna, dovevo stare attento a non sbagliare nemmeno una parola."


translate italian day3_evening_un_3c9b2687:


    "Anche se sono rimasto in silenzio per la maggior parte del tempo."


translate italian day3_evening_un_72af65bb:


    "Poco dopo anche l'ultima scatola è stata sistemata."


translate italian day3_evening_un_5b5ee5ef:


    "Ho consegnato a Lena la lista compilata, e ho iniziato a fissarla negli occhi, come se avessi visto l'Uomo delle Nevi su un monociclo mentre faceva il giocoliere con i maialini.{w} Era incredibile, spaventoso, e soprattutto, ipnotico."


translate italian day3_evening_un_2342bdd6:


    "Tutto ad un tratto si è messa a ridere."


translate italian day3_evening_un_3d342d5a:


    me "Cosa c'è?!"


translate italian day3_evening_un_6f2b6d18:


    un "Quel tuo sguardo…"


translate italian day3_evening_un_094d6b91:


    me "Cos'ha di strano?"


translate italian day3_evening_un_d460b3a8:


    un "È buffo."


translate italian day3_evening_un_f90f07e5:


    me "Davvero...?"


translate italian day3_evening_un_791682db:


    un "Sì. Allora, dove vogliamo andare?"


translate italian day3_evening_un_25f7abbc_1:


    me "Dove?"


translate italian day3_evening_un_530d77f3:


    "Quelle sue ultime parole mi hanno colto impreparato, avevo completamente dimenticato quello di cui avevamo parlato poco fa."


translate italian day3_evening_un_9ac6ce51:


    "A dire il vero, per un istante mi ero totalmente distaccato da questo mondo."


translate italian day3_evening_un_3d8fbc00:


    un "Beh, sai…{w} A ballare."


translate italian day3_evening_un_48e08966:


    "Lena è arrossita e sul suo volto si è formata una strana espressione di vergogna, timidezza e timore."


translate italian day3_evening_un_558e6a65:


    me "Ah, certo! Scusa, ero perso nei miei pensieri.{w} Andiamo al molo."


translate italian day3_evening_un_38967a03:


    "Non so perché io avessi scelto quel luogo."


translate italian day3_evening_un_c980d140:


    "Forse perché ci saremmo potuti imbattere in qualche pioniere nella piazza, nella «zona residenziale», o alla mensa, ma sicuramente non al molo.{w} O almeno era quello che credevo."


translate italian day3_evening_un_cd9d64c3:


    "O forse mi ispirava quella grande e luminosa sfera bianca che si rifletteva sull'acqua."


translate italian day3_evening_un_5626210f:


    "E oggi era luna piena."


translate italian day3_evening_un_7a658aae:


    "Non so come, ma mi è venuto spontaneo proporre quel luogo."


translate italian day3_evening_un_c8879bbd:


    me "Se non ti piace, potremmo…"


translate italian day3_evening_un_ac6b93bc:


    un "No. Quello è davvero un bel posto."


translate italian day3_evening_un_0899349d:


    "Lena ha chiuso la porta a chiave e ci siamo incamminati verso il molo."


translate italian day3_evening_un_2baf966e:


    "La notte è calata sul Campo dormiente."


translate italian day3_evening_un_9380078a:


    un "Prendiamo la via principale?"


translate italian day3_evening_un_22b1f5c5:


    me "Perché? Facciamo prima a passare per la foresta. C'è un bel sentiero lì."


translate italian day3_evening_un_2745ec45:


    un "Però. Potrebbe esserci…"


translate italian day3_evening_un_c8d94649:


    me "Cosa? Un gufo?"


translate italian day3_evening_un_60bde9d8:


    "Ho riso."


translate italian day3_evening_un_7b76c769:


    un "Oh, tu…"


translate italian day3_evening_un_fa1386f8:


    "Sul suo viso è balenata un'ombra di malcontento."


translate italian day3_evening_un_b2a637c4:


    me "Scusami, non volevo."


translate italian day3_evening_un_68893216:


    un "È buio lì."


translate italian day3_evening_un_413580ee:


    me "Sei spaventata?"


translate italian day3_evening_un_9c33c3aa:


    un "Non tanto…"


translate italian day3_evening_un_732bddc6:


    me "Se vuoi possiamo…"


translate italian day3_evening_un_a798f094:


    un "No, va bene…{w} Ma posso…?"


translate italian day3_evening_un_44ced226:


    "Senza aver terminato la frase si è aggrappata al mio braccio."


translate italian day3_evening_un_17ef55d6:


    un "Ti… dispiace?"


translate italian day3_evening_un_006ab8be:


    "Adesso toccava a me arrossire."


translate italian day3_evening_un_771aa502:


    me "Fai pure!"


translate italian day3_evening_un_c41c7779:


    "Abbiamo attraversato la foresta."


translate italian day3_evening_un_90e23667:


    "Anche se non era esattamente una foresta.{w} Era più un boschetto, che separava il Campo dal molo."


translate italian day3_evening_un_8ba91e28:


    "Era largo qualche centinaio di metri."


translate italian day3_evening_un_e7d25333:


    "Anche se fossi stato da solo non avrei avuto paura di attraversare quel boschetto."


translate italian day3_evening_un_b12f537c:


    "Ma la paura di Lena in qualche modo era contagiosa."


translate italian day3_evening_un_cd8be3f9:


    "I rami ondeggiavano sopra di noi.{w} Ho rabbrividito, e Lena si è stretta al mio braccio ancora di più."


translate italian day3_evening_un_4a492d8c:


    me "Non avere paura. Dev'essere uno scoiattolo."


translate italian day3_evening_un_0905212f:


    un "Sì…"


translate italian day3_evening_un_51f323e1:


    "Siamo arrivati al molo.{w} La notte era meravigliosa."


translate italian day3_evening_un_52acfaa6:


    "Mi sono avvicinato alla riva e ho chiamato Lena."


translate italian day3_evening_un_6bf1e9fc:


    me "Guarda!"


translate italian day3_evening_un_5732579f:


    "Il molo, la rimessa per le barche e la luna erano riflessi nell'acqua."


translate italian day3_evening_un_4de4a131:


    "Sembrava un altro mondo, con la superficie dell'acqua che pareva essere un portale verso il Paese delle Meraviglie.{w} Avresti potuto tuffarti e ritrovarti dall'altra parte."


translate italian day3_evening_un_983cba26:


    me "Mi concede un ballo?"


translate italian day3_evening_un_cc87c2d0:


    "Ho allungato la mano verso di lei e mi sono inchinato maldestramente."


translate italian day3_evening_un_36d02627:


    "Lena ha esitato.{w} Forse ho esagerato con i miei manierismi."


translate italian day3_evening_un_d77a4476:


    me "Non preoccuparti, nemmeno io so ballare."


translate italian day3_evening_un_981f5474:


    th "Perché «nemmeno»?{w} Lei non ha mai detto di non saper ballare, anche se a me pare ovvio."


translate italian day3_evening_un_b993fafe:


    un "Ok."


translate italian day3_evening_un_d32a46e3:


    "Lena mi ha dato la mano, l'ho portata dolcemente un po' in disparte e ho messo delicatamente le mie braccia intorno alla sua vita."


translate italian day3_evening_un_2c69e0e2:


    "Siamo rimasti così per qualche secondo."


translate italian day3_evening_un_6cb44a45:


    un "E adesso?"


translate italian day3_evening_un_bdc4a7dc:


    me "Beh. Non saprei…{w} Te la ricordi la canzone?"


translate italian day3_evening_un_38866932:


    un "Non benissimo…{w} Ma me la ricordo."


translate italian day3_evening_un_cdeaa5d1:


    me "Bene! Balliamo il valzer come nei film."


translate italian day3_evening_un_e0124613:


    un "E come si fa?"


translate italian day3_evening_un_dd7ae836:


    "Invece di risponderle ho iniziato a muovermi in cerchio con lei."


translate italian day3_evening_un_2d6366e8:


    me "Visto? Non è poi così difficile!"


translate italian day3_evening_un_a35af87c:


    un "Sì…"


translate italian day3_evening_un_480cbce8:


    "Abbiamo ballato il valzer per parecchi minuti."


translate italian day3_evening_un_0e0749ce:


    "O qualsiasi ballo esso fosse…"


translate italian day3_evening_un_16ce780a:


    "Riuscivo a sentire il suo calore, anche se non eravamo poi così attaccati."


translate italian day3_evening_un_3ff4b37b:


    "Il suo petto si sollevava pesantemente e il suo volto diventava sempre più rosso."


translate italian day3_evening_un_2c5d4171:


    "Lena cercava di evitare il mio sguardo, portando il suo da una parte all'altra."


translate italian day3_evening_un_fdcc8c70:


    "E all'improvviso mi sono reso conto di non essermi mai sentito così."


translate italian day3_evening_un_70d5f90d:


    "La tenerezza ha superato la realtà, come se di colpo mi fossi trovato in un altro mondo, un mondo migliore."


translate italian day3_evening_un_2d5d1563:


    "Ho capito che non volevo lasciar andare quella ragazza, e che avrei rinunciato a tutto pur di andare avanti a danzare con lei per sempre."


translate italian day3_evening_un_b3998185:


    "Mi sono stretto a Lena ancora di più, e solo allora mi ha guardato negli occhi."


translate italian day3_evening_un_ef0ec8a2:


    "C'era sorpresa e confusione nel suo sguardo, ma nessun segno di paura."


translate italian day3_evening_un_5c5e383c:


    "Non aveva paura di me e non mi ha respinto."


translate italian day3_evening_un_cc8b4535:


    un "E dicevi di non saper ballare."


translate italian day3_evening_un_1ec032b9:


    me "Perché è così…"


translate italian day3_evening_un_615f491b:


    "Ero confuso."


translate italian day3_evening_un_20470c50:


    "Non mi sarei aspettato una tale reazione da parte sua."


translate italian day3_evening_un_9dc68533:


    "Dov'era finita la sua insicurezza, la sua timidezza, la sua paura?"


translate italian day3_evening_un_48c7652f:


    me "Anche tu… Danzi piuttosto bene…"


translate italian day3_evening_un_466707e6:


    un "Lo so!"


translate italian day3_evening_un_f8ce941d:


    "Mi è parso di scorgere un sorriso furbo sul suo volto."


translate italian day3_evening_un_ea63114f:


    "O forse me lo sono immaginato…"


translate italian day3_evening_un_3e1fa0d9:


    "No, ci potrei scommettere, l'ho visto davvero."


translate italian day3_evening_un_160309c6:


    th "Ma com'è possibile?"


translate italian day3_evening_un_d8f01710:


    "Non sembrava proprio essere la timida e riservata Lena che conoscevo."


translate italian day3_evening_un_d941a3d5:


    th "Che cosa dovrei dirle?{w} Che cosa dovrei fare?"


translate italian day3_evening_un_93db6486:


    "Potevo solo continuare a ondeggiare con lei, ondeggiare in quella danza, che stava diventando sempre più strana col passare dei minuti."


translate italian day3_evening_un_c81d9d23:


    mt "Semyon! Semyon, dove sei?"


translate italian day3_evening_un_438b28d5:


    "La voce proveniva dal margine della foresta."


translate italian day3_evening_un_e5902723:


    th "Dannazione, proprio adesso!"


translate italian day3_evening_un_105fc865:


    mt "Semyon!"


translate italian day3_evening_un_980eb22f:


    "Era Olga Dmitrievna.{w} Doveva essersi preoccupata non vedendomi tornare per così tanto tempo."


translate italian day3_evening_un_22027067:


    "Avrei dovuto immaginare che l'instancabile leader si sarebbe messa a cercare un pioniere {i}scomparso{/i}!"


translate italian day3_evening_un_d26f19fa:


    th "Avrei dovuto avvisarla prima."


translate italian day3_evening_un_f5e453bf:


    th "Oh! Ormai è inutile pensarci..."


translate italian day3_evening_un_74229088:


    "Lena mi ha guardato con aria interrogativa."


translate italian day3_evening_un_42de22f4:


    un "Forse è meglio se non ci facciamo vedere insieme."


translate italian day3_evening_un_f14f0728:


    me "Perché?{w} Andiamo a dirle che è tutto a posto."


translate italian day3_evening_un_211c6e09:


    un "No. Nascondiamoci e torniamo al Campo più tardi."


translate italian day3_evening_un_93c1d6e9:


    th "La sua idea è strana.{w} Stava andando tutto così bene..."


translate italian day3_evening_un_5e1c6dc6:


    th "Almeno qualcosa ha iniziato a funzionare.{w} O almeno credo…"


translate italian day3_evening_un_04f3f218:


    th "Ma cos'è quel {i}qualcosa{/i}?"


translate italian day3_evening_un_c5b4a9e1:


    me "Ok, va bene."


translate italian day3_evening_un_83831235:


    "Ho deciso di non oppormi, sarebbe stato inopportuno in una situazione del genere."


translate italian day3_evening_un_e073d1d4:


    "Olga Dmitrievna ha gridato ancora per un po' e poi se n'è andata."


translate italian day3_evening_un_2308a96f:


    un "Andiamo."


translate italian day3_evening_un_9513cd87:


    me "Sì…"


translate italian day3_evening_un_63803f24:


    "Lena non mi ha tenuto per il braccio durante il ritorno."


translate italian day3_evening_un_a604adb5:


    "Ero un po' frustrato, ma non ero abbastanza sicuro per poter fare il primo passo."


translate italian day3_evening_un_31cc1eca:


    "Siamo rimasti di nuovo in silenzio. Non era una cosa da Lena iniziare una conversazione, e io non sapevo cosa dirle dopo tutto quello che era accaduto al molo."


translate italian day3_evening_un_89f67cb6:


    "Continuava a tenere lo sguardo basso per tutto il tragitto, ritornando alla sua «usuale» espressione."


translate italian day3_evening_un_985381f5:


    "Che cambiamento bizzarro!"


translate italian day3_evening_un_46255706:


    "O meglio, la cosa bizzarra era quel suo sorriso al molo, e quelle sue parole."


translate italian day3_evening_un_a20cefa7_1:


    "..."


translate italian day3_evening_un_4f71cd7e:


    "Sì è fermata alla piazza."


translate italian day3_evening_un_ccdf7460:


    un "Bene. Devo andare."


translate italian day3_evening_un_9513cd87_1:


    me "Ok…"


translate italian day3_evening_un_375d8a14:


    un "Grazie… per la serata…"


translate italian day3_evening_un_9513cd87_2:


    me "Sì…"


translate italian day3_evening_un_26a8bbab:


    "Lena si è girata e si è incamminata verso la sua casetta, mentre io sono rimasto fermo dov'ero."


translate italian day3_evening_un_59d28a2d:


    th "Cosa diavolo è successo?"


translate italian day3_evening_un_8e35b95f:


    th "Quella danza con lei, quel suo improvviso cambio di personalità, e alla fine tutto è tornato come prima."


translate italian day3_evening_un_a2d69018:


    "Era come se avessi abbracciato un'altra Lena in quel momento."


translate italian day3_evening_un_a5692cd4:


    "È proprio così! Quella non era lei. Sembrava una persona totalmente diversa."


translate italian day3_evening_un_c579109c:


    th "Forse non la conosco abbastanza bene?"


translate italian day3_evening_un_92dafc98:


    "Già da prima avevo l'impressione che Lena nascondesse qualcosa sotto quella maschera di timidezza."


translate italian day3_evening_un_79930909:


    "Non bastano due soli giorni per capire una persona."


translate italian day3_evening_un_c154d46b:


    th "Diamine! Come dovrei comportarmi?!{w} Forse mi sono sognato tutto!"


translate italian day3_evening_un_a20cefa7_2:


    "..."


translate italian day3_evening_un_f4822807:


    "Sono entrato nella stanza della leader."


translate italian day3_evening_un_cec9d8ef:


    "L'intero Campo stava già dormendo, così nessuno avrebbe potuto vedermi."


translate italian day3_evening_un_dd369d3a:


    th "E anche se fosse?{w} Che c'è di male se un pioniere vuole farsi una passeggiata di notte?!"


translate italian day3_evening_un_35589041:


    "Non appena mi sono trovato davanti alla porta, ho sentito un rumore provenire da dietro le mie spalle."


translate italian day3_evening_un_c6f6cc81:


    "Ho sentito un fruscio fra i rami dei cespugli dietro di me, come se qualcuno si fosse mosso lì dentro."


translate italian day3_evening_un_3bc47b84:


    th "Non dovrebbero esserci animali selvatici nei dintorni…"


translate italian day3_evening_un_2bdac6ec:


    th "Di sicuro qualcuno mi stava spiando!"


translate italian day3_evening_un_8591c155:


    "In quel preciso istante mi sono precipitato verso quella direzione, facendomi strada tra i cespugli e guardandomi attorno."


translate italian day3_evening_un_258661a5:


    "Ma era troppo buio e non ho visto niente e nessuno."


translate italian day3_evening_un_d49de170:


    "Non avrebbe avuto senso indagare oltre. Anche se qualcuno mi avesse spiato, sarebbe già riuscito ad allontanarsi."


translate italian day3_evening_un_f54ea053:


    "Sono tornato alla casetta."


translate italian day3_evening_un_76db5439:


    "La luce al suo interno era accesa."


translate italian day3_evening_un_d336a541:


    th "Pare che sia ancora sveglia."


translate italian day3_evening_un_37715fc9:


    mt "Semyon! Ma dove sei stato?!"


translate italian day3_evening_un_72311896:


    me "Cosa intende dire con «dove»?{w} Io e Lena siamo stati all'infermeria a sistemare le scatole di medicinali. Le avevo detto che ci sarei andato questa sera."


translate italian day3_evening_un_d858aa01:


    mt "Ma davvero? E quando me l'avresti detto? Vedo che ci avete messo molto! Sono stata all'infermeria mezz'ora fa, era chiusa e le luci erano spente!"


translate italian day3_evening_un_8125eef3:


    me "Beh… Ecco… Abbiamo deciso di fare una passeggiata."


translate italian day3_evening_un_a91b818e:


    mt "Io mi preoccupo per te mentre tu ti fai una passeggiata!"


translate italian day3_evening_un_a81f88a1:


    me "Ha ragione, scusi…{w} La prossima volta la avviserò in anticipo."


translate italian day3_evening_un_140addaa:


    mt "Sì, per favore!{w} Adesso va' a dormire, è già tardi!"


translate italian day3_evening_un_254cc408:


    "Era difficile non essere d'accordo con lei."


translate italian day3_evening_un_f2607f9a:


    th "È stata a tutti gli effetti una giornata faticosa."


translate italian day3_evening_un_89fdb957:


    "Ero troppo stanco per riuscire a pensare a tutto ciò che era accaduto questa sera, e comunque dubito che avrei potuto pensare a qualcosa di decente…"


translate italian day3_evening_un_95c84c3a:


    th "La cosa principale è che non ho la più pallida idea di come reagire a tutto questo."


translate italian day3_evening_un_d0119574:


    th "A Lena, al Campo, a questo intero mondo..."


translate italian day3_evening_un_d12034ba:


    "In pratica non ho trascorso nemmeno un minuto a cercare delle risposte.{w} Inoltre, è come se io volessi evitare di trovarle, per paura che queste si potessero rivelare spaventose."


translate italian day3_evening_un_5db7cdc5:


    "E Lena...{w} Il tempo che ho trascorso con lei era molto più importante di tutti i miei tentativi di ritornare nel mondo {i}reale{/i}."


translate italian day3_evening_un_9e50ec2f:


    "Avrei potuto preoccuparmi a lungo, cercando di capire cosa io avessi fatto di giusto e cosa io non avessi fatto, ma ero troppo esausto..."


translate italian day3_evening_us_8e722b79:


    "Slavya se n'è andata."


translate italian day3_evening_us_1a407541:


    "Forse non avrei dovuto rifiutare il suo invito dopo tutto quello che aveva fatto per me, ma non sono adatto a questo tipo di attività."


translate italian day3_evening_us_324a06ef:


    "I pionieri continuavano a ballare."


translate italian day3_evening_us_d21cca78:


    th "Almeno si stanno godendo il ballo."


translate italian day3_evening_us_ea24578a:


    "Anche Olga Dmitrievna stava ballando."


translate italian day3_evening_us_ea82e2f0:


    "L'ho trovato un po' fuori luogo.{w} La leader dovrebbe mantenere l'ordine."


translate italian day3_evening_us_78fb937a:


    th "Specialmente dato che non è più diciassettenne."


translate italian day3_evening_us_f2be2b10:


    "Olga Dmitrievna si è avvicinata a me, come se avesse percepito che qualcuno osava dubitare del suo professionismo."


translate italian day3_evening_us_0f6bc7b5:


    mt "Perché non stai ballando?"


translate italian day3_evening_us_8717f26e:


    me "Non mi va…"


translate italian day3_evening_us_b0e280b5:


    mt "Beh, come vuoi."


translate italian day3_evening_us_945c59bc:


    "Ha sorriso furbescamente."


translate italian day3_evening_us_31a7a515:


    mt "Allora avrei un lavoretto per te!"


translate italian day3_evening_us_e473fa2f:


    me "Un lavoretto, cioè...?"


translate italian day3_evening_us_a1f730b2:


    "Tutto sarebbe stato meglio che danzare."


translate italian day3_evening_us_bc92d844:


    mt "Hai fatto un bel lavoro oggi alla mensa, ma…{w} Penso che tu non abbia ancora ripagato appieno il tuo debito."


translate italian day3_evening_us_8941bd9d:


    me "Ahhh?"


translate italian day3_evening_us_8403b986:


    mt "Ulyana!"


translate italian day3_evening_us_123f5418:


    us "Cosa?!"


translate italian day3_evening_us_7f1e0dc0:


    mt "Vieni qui."


translate italian day3_evening_us_86cbd5c6:


    "Ulyana si è avvicinata a noi, riluttante."


translate italian day3_evening_us_1e9714fe:


    mt "Vedo che ti sei sfogata abbastanza questa sera."


translate italian day3_evening_us_6c304510:


    us "No, non è vero!"


translate italian day3_evening_us_eb9a46a9:


    "La leader sembrava avesse ragione, dato che Ulyana stava sudando come un maiale."


translate italian day3_evening_us_9a131f0d:


    mt "Avrei un compito per te e Semyon."


translate italian day3_evening_us_4b8e25e2:


    us "Ma Olga Dmitrievna...!"


translate italian day3_evening_us_4aca71e6:


    "Ha implorato Ulyana."


translate italian day3_evening_us_c588b18c:


    us "C'è il ballo adesso! Ed è tardi!"


translate italian day3_evening_us_63ea0fa5:


    mt "Non ci vorrà molto tempo.{w} Slavya stava sistemando i libri nella biblioteca, ma non è riuscita a finire…"


translate italian day3_evening_us_c4a407cc:


    mt "Sono rimasti solo un paio di scaffali."


translate italian day3_evening_us_e566dcaf:


    us "Ma…"


translate italian day3_evening_us_c2f28fc6:


    mt "Niente ma!"


translate italian day3_evening_us_888f7373:


    th "Sì, per l'amor di Dio."


translate italian day3_evening_us_8b0f570d:


    "Non mi piace lavorare, ma piuttosto che ballare…"


translate italian day3_evening_us_7f594afe:


    me "Io sono pronto!"


translate italian day3_evening_us_d0e475bd:


    mt "Ben detto, Semyon! Bravo ragazzo! Un vero pioniere! Dovresti seguire il suo esempio, Ulyana!"


translate italian day3_evening_us_8378599b:


    "Tuttavia Ulyana non sembrava aver apprezzato un tale esempio."


translate italian day3_evening_us_88adb859:


    mt "Bene allora, andate!{w} L'intero Campo conta su di voi!"


translate italian day3_evening_us_d172a747:


    us "Non mi scorderò di questo!"


translate italian day3_evening_us_86a7768d:


    "Mi ha sibilato digrignando i denti."


translate italian day3_evening_us_7cf397bf:


    "Il Campo era stupendo di sera.{w} Il silenzio e la tranquillità mi davano un senso di pace."


translate italian day3_evening_us_b69e6c0f:


    "Solo la distante musica proveniente dalla piazza mi ricordava che non ero solo."


translate italian day3_evening_us_38d5d35e:


    "E ovviamente anche Ulyana, che era appena tornata."


translate italian day3_evening_us_79dbaa1d:


    me "Avresti potuto fare le pulizie col tuo abito da sera."


translate italian day3_evening_us_a026df63:


    "Ho ridacchiato."


translate italian day3_evening_us_fb48ad0c:


    us "È tutta colpa tua!"


translate italian day3_evening_us_74af0838:


    "Aveva il fiatone e il suo volto era tutto rosso come un pomodoro, e non mi sarei sorpreso se tutto d'un tratto le fosse uscito il vapore dalle orecchie."


translate italian day3_evening_us_0ab904a2:


    us "Sì! È tutta!{w} Colpa! Tua!"


translate italian day3_evening_us_6a9fa49b:


    me "Perché è sempre colpa mia?"


translate italian day3_evening_us_0f170283:


    us "Se solo avessi tenuto chiusa quella bocca!"


translate italian day3_evening_us_21221d76:


    me "Che cosa sarebbe cambiato?{w} Ce l'avrebbe fatto fare comunque."


translate italian day3_evening_us_5a606f08:


    us "Tu…"


translate italian day3_evening_us_a2d9c3e3:


    "Non riusciva a parlare normalmente e quindi si è limitata a sibilare."


translate italian day3_evening_us_5bbc2e7c:


    me "E anche se fossi rimasto in silenzio?{w} Credi che ti avrebbe permesso di continuare a divertirti?"


translate italian day3_evening_us_c1625886:


    "Ulyana mi ha guardato."


translate italian day3_evening_us_49ae3aab:


    "Pareva essersi calmata un po'."


translate italian day3_evening_us_8fa6a151:


    us "Cosa ne sai tu! Non t'importa nemmeno del ballo!{w} Non sai nemmeno ballare!"


translate italian day3_evening_us_9574dd57:


    me "E anche se non sapessi ballare?"


translate italian day3_evening_us_05b51e5d:


    us "Se avessi ballato con me…{w} Sarebbe stato divertente!"


translate italian day3_evening_us_f51acc4b:


    "È tornata alla sua usuale modalità «infantile»."


translate italian day3_evening_us_b3acf935:


    me "L'avrei fatto…{w} Ma hai visto come sono andate le cose…"


translate italian day3_evening_us_1d9ef291:


    "Non abbiamo potuto verificare chi fosse il ballerino migliore tra noi due."


translate italian day3_evening_us_90a2bbc6:


    "Siamo arrivati alla biblioteca. Fuori era già calata l'oscurità."


translate italian day3_evening_us_f49d96cb:


    "Sono rimasto sorpreso da quella strana richiesta da parte di Olga Dmitrievna…{w} o forse era più un ordine…{w} sistemare i libri di notte."


translate italian day3_evening_us_bc244cfb:


    th "È così bizzarro."


translate italian day3_evening_us_a7e2aa20:


    "Dentro faceva buio e l'interruttore della luce non funzionava, scattando spiacevolmente ad ogni pressione."


translate italian day3_evening_us_d3f5e62a:


    "Forse funzionava correttamente, ma le luci non si accendevano lo stesso."


translate italian day3_evening_us_f09b37ef:


    us "Aspetta, prendo le candele."


translate italian day3_evening_us_681ab9c1:


    "Stavo per chiederle come sapeva dove si trovassero le candele e i fiammiferi, quando due piccole luci sono apparse sul tavolo davanti a me."


translate italian day3_evening_us_7827470a:


    us "Così va meglio!"


translate italian day3_evening_us_574bbbd5:


    "Ha detto Ulyana, soddisfatta di sé."


translate italian day3_evening_us_3f38f9a0:


    me "E adesso?"


translate italian day3_evening_us_fbf7b967:


    us "Adesso cosa?"


translate italian day3_evening_us_c349ed5f:


    "Mi ha guardato con aria interrogativa."


translate italian day3_evening_us_050fe076:


    me "Che cosa facciamo adesso?"


translate italian day3_evening_us_8232daec:


    us "Pff, e che ne so?"


translate italian day3_evening_us_393056bc:


    "Ha ridacchiato."


translate italian day3_evening_us_b57abe01:


    me "Bene."


translate italian day3_evening_us_6046a48f:


    "Ho camminato attorno agli scaffali."


translate italian day3_evening_us_a5101d38:


    th "Olga Dmitrievna ha detto qualcosa riguardo a «un paio di scaffali»..."


translate italian day3_evening_us_0e2a5ebd:


    "Ho toccato le copertine dei libri e mi sono assicurato che non fossero ricoperte di polvere."


translate italian day3_evening_us_f637ef9a:


    "Sembrava che qualcuno avesse già svolto un ottimo lavoro."


translate italian day3_evening_us_d054667b:


    "Dopo aver esaminato l'intera biblioteca non ho trovato proprio niente da spolverare.{w} Olga Dmitrievna dev'essersi sbagliata."


translate italian day3_evening_us_eb36cc9f:


    "Improvvisamente ho sentito dei passi alle mie spalle."


translate italian day3_evening_us_cca19138:


    "La vecchia pavimentazione scricchiolante ha tradito Ulyana, che cercava di avvicinarsi a me furtivamente."


translate italian day3_evening_us_08ef6f67:


    us "Buu!"


translate italian day3_evening_us_468f92d5:


    "Mi sono girato."


translate italian day3_evening_us_9e09f5fa:


    me "Oddio, sono così spaventato, ti prego…"


translate italian day3_evening_us_80eb9adc:


    us "Ah, piantala!"


translate italian day3_evening_us_e2993c31:


    "Si è girata con risentimento."


translate italian day3_evening_us_4ab03e66:


    me "Sembra tutto pulito qui, quindi…"


translate italian day3_evening_us_c1661c00:


    "Le file di libri debolmente illuminate mi guardavano con aria di rimprovero."


translate italian day3_evening_us_4c1d899a:


    "Autori ignoti di anni ormai dimenticati da tempo…"


translate italian day3_evening_us_51f94d8f:


    th "Mi chiedo se qualcuno se li ricorda ancora?{w} Zhenya sicuramente sì."


translate italian day3_evening_us_b78d5ef3:


    "Sicuramente se li ricordava tutti."


translate italian day3_evening_us_fce45449:


    us "Ok. Siediti."


translate italian day3_evening_us_dc9d3ef6:


    "Ulyana ha portato una sedia accanto a me.{w} Mi sono seduto su di essa."


translate italian day3_evening_us_fecfa4ed:


    me "Va bene, e adesso?"


translate italian day3_evening_us_5d9c94b9:


    us "Raccontiamoci delle storie spaventose.{w} Abbiamo due sedie e due candele, quindi potremmo raccontarci due storie, una io e una tu."


translate italian day3_evening_us_cd917d11:


    me "Ok."


translate italian day3_evening_us_0e57cf1c:


    "Ho accettato senza pensare."


translate italian day3_evening_us_c76d5efb:


    "Non avevo la minima intenzione di tornare al ballo e, per quanto io ne potessi sapere, non c'era niente da fare nel Campo di notte."


translate italian day3_evening_us_1da6e1e4:


    "Inoltre conoscevo un paio di storielle che avrebbero potuto terrorizzare Ulyana."


translate italian day3_evening_us_a38cf680:


    "E la cosa sarebbe stata fantastica."


translate italian day3_evening_us_959cb1f4:


    me "Inizia tu."


translate italian day3_evening_us_1549669b:


    us "Ok."


translate italian day3_evening_us_906544fe:


    "Si è messa comoda, ha abbracciato lo schienale e ha avvicinato la candela al proprio volto quanto più possibile."


translate italian day3_evening_us_517ac6ae:



    nvl clear
    "{i}C'era una volta un ragazzo che viveva in un villaggio molto lontano. Un ragazzo normale. Andava a scuola, giocava con gli amici. Non c'era niente di speciale in lui.{/i}"


translate italian day3_evening_us_e153dd08:


    "{i}Un giorno scommise con i suoi amici che sarebbe riuscito a entrare nella casa abbandonata senza avere paura…{/i}"


translate italian day3_evening_us_82b72ef3:


    "Ulyana ha fatto una lunga pausa."


translate italian day3_evening_us_4d3eecb3:


    me "E quindi, cosa c'era in quella casa?"


translate italian day3_evening_us_cd35d04d:


    us "Non interrompermi!"


translate italian day3_evening_us_4d2c229a:


    me "Te la stai inventando al volo?"


translate italian day3_evening_us_39cf5c52:


    "Ha messo il broncio e ha proseguito."


translate italian day3_evening_us_f5578357:


    "{i}Dicono che una strega aveva vissuto in quella casa, e la gente riusciva ancora a vedere il suo fantasma che si aggirava nella notte. Nessuno lo sapeva con certezza, ma tutti ne erano spaventati.{/i}"


translate italian day3_evening_us_f3da10f9:


    "{i}Il ragazzo disse che erano tutte stupidaggini, e che fosse pronto a trascorrere un'intera notte in quella casa. E così fece. Ma la mattina seguente non ritornò.{/i}"


translate italian day3_evening_us_31aee4ac:


    "{i}Fu trovato impiccato.{/i}"


translate italian day3_evening_us_eb137a30:



    nvl clear
    "Ulyana sì è fermata di nuovo."


translate italian day3_evening_us_fd837679:


    me "Entusiasmante…{w} Si è impiccato con i lacci delle scarpe?"


translate italian day3_evening_us_abfdf469:


    "Ho detto con scetticismo."


translate italian day3_evening_us_306e9012:


    "Ha nuovamente aggrottato la fronte."


translate italian day3_evening_us_6a86fe31:


    me "È già finita?"


translate italian day3_evening_us_c25bebac:


    us "Certo che no!"


translate italian day3_evening_us_81736160:


    "{i}Seppellirono il corpo del ragazzo. Come si dovrebbe fare, con la bara e la pala.{/i}"


translate italian day3_evening_us_387cb49c:


    "{i}I suoi amici e parenti erano addolorati, ma nulla l'avrebbe riportato indietro.{/i}"


translate italian day3_evening_us_4498a264:


    "{i}Dopo qualche giorno le persone iniziarono a sparire dal villaggio. Nessuno sapeva dove o perché. Semplicemente svanivano nell'aria.{/i}"


translate italian day3_evening_us_2b80bb65:


    "{i}Gli abitanti del villaggio vollero chiamare la polizia, ma un amico del ragazzo impiccato disse loro che aveva visto il ragazzo. Nessuno gli credette all'inizio, ma la gente continuava a sparire.{/i}"


translate italian day3_evening_us_0b0a71f5:



    nvl clear
    "{i}Allora gli abitanti decisero di disseppellire la bara. C'erano lunghi graffi sulla parte interna del coperchio. Ma il cadavere non era più lì.{/i}"


translate italian day3_evening_us_c6ee723c:


    "{i}Dov'era andato?{/i}"


translate italian day3_evening_us_afd82e5f:


    "Ulyana ha allontanato la candela dal suo viso e ha fatto una faccia spaventosa (o almeno così credeva)."


translate italian day3_evening_us_725f29b0:


    "{i}Alla fine tutti gli abitanti del villaggio sparirono e alcune persone che erano passate accanto alla casa della strega affermarono di aver visto due fantasmi.{/i}"


translate italian day3_evening_us_76b2fe88:


    nvl clear


translate italian day3_evening_us_c65a5a14:


    "Ulyana ha spento la candela ed è rimasta in silenzio."


translate italian day3_evening_us_56417cdb:


    "Sembrava che la storia fosse finita."


translate italian day3_evening_us_d1bfd0cf:


    me "Complimenti!"


translate italian day3_evening_us_bfb0f205:


    "Ho applaudito."


translate italian day3_evening_us_5bdb65dc:


    me "Non proprio da premio Pulitzer, ma comunque…"


translate italian day3_evening_us_d6c3ca28:


    us "Sei spaventato?{w} Terrorizzato?"


translate italian day3_evening_us_6c2b1b7e:


    me "Oh sì, sto tremando tutto…"


translate italian day3_evening_us_f4067405:


    us "Oh, e dai…{w} Raccontami la tua storia allora.{w} Scommetto che non mi farà nessun effetto!"


translate italian day3_evening_us_59133844:



    nvl clear
    "Ho raccolto i miei pensieri e ho deciso di raccontarle una storia che avevo letto qualche mese prima sul blog di un amico."


translate italian day3_evening_us_0864a00e:


    "Era davvero un bravo narratore, mi piaceva il suo stile. E quindi il mio successo era garantito."


translate italian day3_evening_us_cf924028:


    "Ovviamente non ricordavo le parole esatte, e quindi l'avrei raccontata a parole mie."


translate italian day3_evening_us_56bad404:


    "{i}Esiste una stazione spaziale, l'ultima frontiera dell'umanità, situata al confine di una civiltà aliena ostile. È il terzo mese di tregua.{/i}"


translate italian day3_evening_us_0fe7b509:


    "{i}In realtà non la si potrebbe definire tregua, ma piuttosto una «non guerra». Una piccola manciata di sopravvissuti è esausta a causa degli interminabili giorni di assedio.{/i}"


translate italian day3_evening_us_6dc81529:


    "{i}Da una parte non possono ritirarsi abbandonando l'avamposto. D'altra parte, sanno che non resisterebbero nemmeno un minuto se il nemico decidesse di attaccare.{/i}"


translate italian day3_evening_us_04d2b110:



    nvl clear
    "{i}Disperazione – è questo il termine giusto per descrivere la loro situazione.{/i}"


translate italian day3_evening_us_68e6fa98:


    "{i}Il cibo scarseggia, rimangono solo poche munizioni, che in caso di attacco nemico servirebbero più per dare un saluto di benvenuto.{/i}"


translate italian day3_evening_us_0f0fc437:


    "{i}L'unica cosa che abbonda è l'aria e l'acqua – grazie al sistema di rigenerazione.{/i}"


translate italian day3_evening_us_420be6e0:


    "{i}Quelle persone non si parlano ormai da un paio di settimane. Forse semplicemente non sono interessate a sprecare il loro tempo per comunicare, quando la morte è così vicina.{/i}"


translate italian day3_evening_us_b4d08157:


    "{i}O meglio, vicina alla corazza della stazione, spessa qualche metro, dentro alla quale si trovano rinchiusi.{/i}"


translate italian day3_evening_us_f910168c:


    "{i}L'unica cosa da fare è aspettare i rinforzi o l'attacco del nemico. Una qualsiasi di queste due opzioni potrebbe porre fine a questa tortura.{/i}"


translate italian day3_evening_us_9e98cab6:



    nvl clear
    "{i}Questa situazione è come una partita a scacchi contro un avversario invisibile. Vale a dire, la parte più difficile – quella finale. Ogni mossa sbagliata potrebbe portare alla sconfitta. La loro, o quella del nemico.{/i}"


translate italian day3_evening_us_6880febd:


    "{i}Ma il nemico lo sa? Anche lui potrebbe aver timore di fare la mossa sbagliata. Nel frattempo gli umani muovono il loro re sulla scacchiera. Ma solo di una posizione in avanti, e poi nuovamente di una posizione indietro.{/i}"


translate italian day3_evening_us_38c79fcf:


    "{i}Ogni altra possibile mossa li porterebbe alla sconfitta sicura. Un passo in avanti è un'implicita minaccia di attacco. E un passo indietro è una deliberata necessità di difesa. Il gioco sarebbe molto più facile se i due sfidanti si trovassero uno di fronte all'altro. Persino il miglior giocatore di scacchi non è in grado di nascondere il proprio sguardo nervoso, o la goccia di sudore occasionale sulla propria fronte.{/i}"


translate italian day3_evening_us_22f0a385:


    "{i}Da una parte ogni movimento è inutile, ognuno sa che la situazione è senza speranza, perché nessuno vuole fare una mossa per sfuggire alla sconfitta imminente.{/i}"


translate italian day3_evening_us_35856953:



    nvl clear
    "{i}D'altra parte, vedendo l'avversario di fronte a sé, sapendo che è nelle tue stesse condizioni e come te potrebbe fare un errore, si potrebbe giocare facilmente il tutto per tutto. E lo stesso vale per l'avversario.{/i}"


translate italian day3_evening_us_4973842c:


    "{i}Queste sono le condizioni dei pochi uomini e donne sopravvissuti ai confini dell'universo. Da diversi giorni ormai non ci sono stati ordini dal quartier generale.{/i}"


translate italian day3_evening_us_7c9be39f:


    "{i}Anche se gli ultimi ordini non contenevano nulla di utile, solo la usuale richiesta di mantenere le difese.{/i}"


translate italian day3_evening_us_f87c860d:


    "{i}Una casella avanti, una casella indietro. L'eterna oscillazione di un pendolo.{/i}"


translate italian day3_evening_us_f34c1a35:


    "{i}All'inizio del quarto mese il comandante decide di ritirarsi.{/i}"


translate italian day3_evening_us_f2ccbac0:


    "{i}Le vite dei suoi soldati sono più importanti dei mitici ideali dell'umanità. Nessuno lo giudicherà per questo.{/i}"


translate italian day3_evening_us_f5762547:



    nvl clear
    "{i}Tanto più perché non sono ancora in grado di cambiare nulla.{/i}"


translate italian day3_evening_us_0117105e:


    "{i}Le preparazioni non prevedono molto tempo. La cosa più preziosa che si potrebbe portare con sé da una nave che affonda è la propria vita.{/i}"


translate italian day3_evening_us_ce3c3dd4:


    "{i}L'intero equipaggio della stazione è pronto a bordo della nave di salvataggio. La sequenza di lancio è iniziata. Tre, due, uno… ma niente. Le porte della baia non si aprono.{/i}"


translate italian day3_evening_us_ec3c9d93:


    "{i}Mandano dei meccanici per verificare la presenza di malfunzionamenti. Ma non trovano nulla. Le porte si dovrebbero aprire. Ma nemmeno la seconda volta si aprono. E neppure la terza…{/i}"


translate italian day3_evening_us_e838b9cb:


    "{i}Il comandante ordina al suo equipaggio di lasciare la stazione nelle capsule di salvataggio. Ma nemmeno quelle sembrano funzionare. Deve trattarsi di guasti tecnici. L'equipaggio cerca invano di comunicare con il quartier generale.{/i}"


translate italian day3_evening_us_a22d8215:



    nvl clear
    "{i}Dopo pochi giorni l'amministratore delegato si accorge che non c'è niente sul radar. Niente di niente – nessun pianeta, nessun asteroide, nessuna nave nemica. Solo un'oscurità senza limiti…{/i}"


translate italian day3_evening_us_44deaf56:


    "{i}Il cibo inizia a scarseggiare nel giro di un mese. Tutti pensano che questa è la fine. Ma nessuno muore, non dopo un giorno, non dopo una settimana, e nemmeno dopo un mese.{/i}"


translate italian day3_evening_us_6d909058:


    "{i}Come se il cibo non fosse più necessario per sostenere la vita umana.{/i}"


translate italian day3_evening_us_fe89102b:


    "{i}In questo momento la maggior parte dell'equipaggio impazzisce. Alcuni di loro rimangono chiusi nelle loro stanze e pregano, altri vagano intorno alla stazione, altri ancora cercano di suicidarsi.{/i}"


translate italian day3_evening_us_7ed3bc1a:


    "{i}Ma né i colpi a bruciapelo di una pistola al plasma, né le immersioni idroniche, e neppure semplicemente il taglio delle vene porta ad alcun risultato.{/i}"


translate italian day3_evening_us_405821aa:



    nvl clear
    "{i}I giorni passano, mesi o forse anni – nessuno ormai tiene traccia del tempo. La gente impazzita recupera la ragione, e la gente sana impazzisce. Tutto si ripete ciclicamente.{/i}"


translate italian day3_evening_us_a8f61a7d:


    "{i}Alla fine, l'equipaggio accetta il proprio destino.{/i}"


translate italian day3_evening_us_f0789661:


    "{i}Cominciano a inventare varie attività: messe in scena teatrali, tornei sportivi, lettura di libri scritti da loro stessi.{/i}"


translate italian day3_evening_us_520553c5:


    "{i}Molte famiglie si formano, e altrettante si disgregano.{/i}"


translate italian day3_evening_us_df10e82a:


    "{i}È un flusso infinito di tempo, un ciclo chiuso di vite umane. E solo il buio sul radar ricorda loro il vuoto esterno. Ma c'è un vuoto anche dentro la stazione…{/i}"


translate italian day3_evening_us_dd21be31:


    "{i}Alla fine la gente smette di vivere. Non fanno altro che starsene sdraiati nelle loro camere e dormire. All'inizio non per l'intera giornata, ma dopo un po' cadono in una specie di catalessi.{/i}"


translate italian day3_evening_us_134f2ab0:



    nvl clear
    "{i}Ognuno di loro ha i propri sogni – qualcuno ritorna alla propria giovinezza, qualcuno ritrova l'amore perduto, qualcuno difende gli ideali dell'umanità con un'arma tra le mani. Altri ancora vagano nello spazio…{/i}"


translate italian day3_evening_us_d9aa3d87:


    "{i}Una nave di salvataggio, reduce dalle scaramucce con il nemico, riesce ad agganciarsi alla stazione. La squadra di salvataggio penetra all'interno.{/i}"


translate italian day3_evening_us_0bf36ead:


    "{i}L'intera stazione sembra un pezzo di rottami metallici che avevano galleggiato nello spazio per migliaia di anni. Il reattore era spento da tempo. C'erano segni dei colpi di laser sulle pareti. L'attrezzatura era tutta devastata.{/i}"


translate italian day3_evening_us_6b174e6c:


    "{i}In quasi ogni stanza hanno trovato gli scheletri delle persone che morirono orribilmente difendendo la frontiera dell'umanità.{/i}"


translate italian day3_evening_us_ab489ab3:


    "{i}Soltanto un messaggio crittato inviato al quartier generale indicava che qualcosa non andava.{/i}"


translate italian day3_evening_us_43b9f5d0:



    nvl clear
    "{i}«Salvateci!...{/i}"


translate italian day3_evening_us_93ed5913:


    "{i}…vogliamo solo morire…»{/i}"


translate italian day3_evening_us_6b713cd4:


    "Ho concluso la mia storia e ho guardato Ulyana."


translate italian day3_evening_us_2c2388cc:


    "Il suo viso era nascosto nell'oscurità."


translate italian day3_evening_us_768354f5:


    me "Beh, ecco la mia storia…"


translate italian day3_evening_us_e7066533:


    "Ho spento la fiamma."


translate italian day3_evening_us_3d0f0f54:


    "Ulyana ha strillato, è balzata dalla sedia e si è gettata verso di me."


translate italian day3_evening_us_574fb6aa:


    "Siamo caduti per terra."


translate italian day3_evening_us_a4d2b1c8:


    me "Ehi, che ti succede?"


translate italian day3_evening_us_3005a190:


    us "Niente…"


translate italian day3_evening_us_3cff4607:


    "Sembrava molto spaventata."


translate italian day3_evening_us_871c17c7:


    "Sembrava che la mia storia avesse fatto centro."


translate italian day3_evening_us_c163059f:


    "Mi sono congratulato mentalmente con me stesso."


translate italian day3_evening_us_4c250e85:


    "Anche se Ulyana non si stava divertendo affatto."


translate italian day3_evening_us_355d45cd:


    "Si è stretta attorno a me, rabbrividendo e singhiozzando."


translate italian day3_evening_us_638d6acb:


    me "Che c'è che non va, piccola peste?"


translate italian day3_evening_us_0192e2f2:


    "Le ho accarezzato la testa."


translate italian day3_evening_us_6dc624b2:


    me "È solo una storia.{w} Non è reale."


translate italian day3_evening_us_24578402:


    us "Tu e le tue stupide storie!"


translate italian day3_evening_us_00a07cdc:


    "Ulyana si è stretta a me ancora più forte."


translate italian day3_evening_us_14314e51:


    me "Ti ha spaventata così tanto?"


translate italian day3_evening_us_c463eeea:


    us "Sì…"


translate italian day3_evening_us_d47079d5:


    "Francamente, non mi aspettavo una risposta così sincera da parte sua."


translate italian day3_evening_us_0268fff6:


    me "È tutto a posto."


translate italian day3_evening_us_f776f380:


    th "Il tempo cura tutte le ferite. Si riprenderà."


translate italian day3_evening_us_3e0531a1:


    us "Senti, Semyon…"


translate italian day3_evening_us_67c5f2d1:


    me "Sì?"


translate italian day3_evening_us_62db7763:


    us "No, niente."


translate italian day3_evening_us_dc1b0573:


    "Ha nascosto il suo viso nel mio petto."


translate italian day3_evening_us_f8253995:


    "I minuti passavano."


translate italian day3_evening_us_a92c5503:


    me "Senti, ti capisco… Ma forse dovremmo andare?"


translate italian day3_evening_us_0cd20c05:


    "Ho atteso una sua risposta.{w} Ulyana russava tranquillamente."


translate italian day3_evening_us_61d4bcc5:


    me "Ehi! Svegliati!"


translate italian day3_evening_us_094ae4ba:


    th "Com'è possibile addormentarsi per la paura?"


translate italian day3_evening_us_ba0cb0ae:


    me "Svegliati, ho detto!"


translate italian day3_evening_us_71063c36:


    "Nessuna reazione."


translate italian day3_evening_us_c589ce7a:


    "Ho cercato di alzarmi."


translate italian day3_evening_us_20833c6d:


    "Ulyana certamente non pesava più di quaranta chili, ma immagina di essere sdraiato sotto a un tale peso…"


translate italian day3_evening_us_81eadbc5:


    "È davvero difficile riuscire ad alzarsi!"


translate italian day3_evening_us_2f516faf:


    "Si potrebbe pensare che Ulyana fosse morta, se non fosse per il suo respiro."


translate italian day3_evening_us_19979d09:


    "Di certo avrei potuto fare qualche sforzo in più..."


translate italian day3_evening_us_ceb2ee9d:


    th "Ma così la sveglierei, e tutto ricomincerebbe da capo!"


translate italian day3_evening_us_c52a9778:


    th "Una situazione poco invidiabile."


translate italian day3_evening_us_feb9b84c:


    th "Anche se potrei sempre aspettare che si svegli da sola."


translate italian day3_evening_us_96e8f382:


    th "Certamente non dormirà fino al mattino dopo una storia del genere."


translate italian day3_evening_us_dd9bfa6a:


    "Ho guardato il cielo stellato fuori dalla finestra."


translate italian day3_evening_us_44c11d50:


    th "Mi chiedo se là fuori da qualche parte nell'universo ci sia un avamposto con un equipaggio fantasma…"


translate italian day3_evening_us_8ac1f25d:


    "I miei occhi si sono chiusi lentamente, e in un attimo mi sono addormentato..."


translate italian day3_evening_dv_701cf26a:


    "Dopo pochi minuti mi sono avvicinato al palco."


translate italian day3_evening_dv_50cb2f7e:


    th "Forse non è stata una buona idea accettare il suo invito?"


translate italian day3_evening_dv_13334dce:


    th "Il solo fatto di parlare con Dvachevskaya è pericoloso già di per sé…"


translate italian day3_evening_dv_bdfb9d61:


    th "D'altra parte non ci tengo ad andare al ballo."


translate italian day3_evening_dv_b864db36:


    th "Cosa potrei fare là?{w} Mi metterei solo in ridicolo…"


translate italian day3_evening_dv_5815ef70:


    th "No, preferisco guardare Alisa mentre suona la chitarra."


translate italian day3_evening_dv_1340350c:


    "Avvicinandomi ho visto una persona seduta sul bordo del palco, con le gambe a penzoloni."


translate italian day3_evening_dv_5ee37cbd:


    dv "E così alla fine sei venuto!"


translate italian day3_evening_dv_171149e1:


    "Alisa ha messo la chitarra da parte ed è saltata giù dal palco."


translate italian day3_evening_dv_7c8a24e7:


    "Mi è parso di scorgere una traccia di contentezza sul suo viso.{w} Suonare per qualcuno è certamente meglio che farlo per sé stessi."


translate italian day3_evening_dv_2abbfc44:


    me "Come puoi vedere…"


translate italian day3_evening_dv_64374587:


    dv "Allora, cosa facciamo?"


translate italian day3_evening_dv_6c4f856c:


    me "Che intendi con «cosa»?"


translate italian day3_evening_dv_554549ca:


    dv "Te lo sto chiedendo."


translate italian day3_evening_dv_c222ae63:


    me "Beh... volevi..."


translate italian day3_evening_dv_e1d0c3f8:


    "All'improvviso ho avuto il dubbio che questa situazione fosse solo un'altra delle sue solite burle, e che non mi sarei dovuto aspettare di sentire una qualche canzone."


translate italian day3_evening_dv_c87e1fd0:


    me "Io vado a dormire. Addio!"


translate italian day3_evening_dv_4f545b83:


    "Mi sono girato e ho agitato la mano lentamente, in modo intenzionale."


translate italian day3_evening_dv_93b5fc50:


    dv "Ehi!"


translate italian day3_evening_dv_744f6c22:


    "Alisa mi ha afferrato per la manica."


translate italian day3_evening_dv_ad02f53a:


    me "Cosa c'è?"


translate italian day3_evening_dv_18f4b1ad:


    dv "Non vuoi ascoltarla?"


translate italian day3_evening_dv_ba6356e6:


    me "Ascoltare cosa? Se nemmeno tu sai esattamente che cosa vuoi fare."


translate italian day3_evening_dv_3ce16a8b:


    dv "Ma certo che lo so!"


translate italian day3_evening_dv_55df167e:


    "Ha preso la chitarra e ha fatto vibrare le corde con il plettro un paio di volte."


translate italian day3_evening_dv_a2494e1f:


    dv "Siediti accanto a me."


translate italian day3_evening_dv_eaadd141:


    "Non ho obiettato e in un istante ero già al suo fianco."


translate italian day3_evening_dv_55b9978e:


    th "Così va meglio."


translate italian day3_evening_dv_69e988e0:


    th "Non partecipare al ballo, andarmene adesso, e poi cos'altro farò?"


translate italian day3_evening_dv_994a7e72:


    "Il ruolo di estraneo era più adatto a un osservatore passivo, e non al desideroso cercatore di risposte che volevo essere."


translate italian day3_evening_dv_6a5d85fb:


    dv "Allora, adesso ti faccio vedere come si suona questa canzone."


translate italian day3_evening_dv_9fb4d40d:


    me "Così su due piedi?"


translate italian day3_evening_dv_7f046cee:


    dv "Cosa intendi con «su due piedi»?"


translate italian day3_evening_dv_3eb0fb19:


    me "È da molto tempo che non maneggio una chitarra..."


translate italian day3_evening_dv_22bda204:


    th "Si potrebbe facilmente intuire quanto tempo sia passato dall'ultima volta che l'ho utilizzata. Dato che la mia capacità di suonarla farebbe ridere i polli."


translate italian day3_evening_dv_87662605:


    dv "Non è difficile!"


translate italian day3_evening_dv_db47df41:


    "Ho lanciato un'occhiata intensa ad Alisa."


translate italian day3_evening_dv_2f5dd571:


    "Ormai mi pareva chiaro che non fosse in grado di esprimere i propri sentimenti."


translate italian day3_evening_dv_f464d10c:


    "Avrebbe potuto anche abbandonare il suo usuale temperamento per qualche istante, rivelandosi così una ragazza normale, simpatica, e pure amichevole."


translate italian day3_evening_dv_05afbea0:


    "E quando sarebbe tornata al suo solito comportamento, le cose si sarebbero messe male nuovamente."


translate italian day3_evening_dv_0d77d9e0:


    th "Ma quale delle due è la vera Alisa?"


translate italian day3_evening_dv_93b5fc50_1:


    dv "Ehi!"


translate italian day3_evening_dv_ad02f53a_1:


    me "Cosa?"


translate italian day3_evening_dv_265b67ba:


    dv "Smettila di sognare a occhi aperti!"


translate italian day3_evening_dv_94598a92:


    me "Oh, sì, scusa..."


translate italian day3_evening_dv_a38c32a6:


    dv "Allora, stai guardando?"


translate italian day3_evening_dv_dc57eba2:


    me "Sì. Che canzone è?"


translate italian day3_evening_dv_4d022e04:


    dv "Quella che ho suonato stamattina, ovviamente!"


translate italian day3_evening_dv_46559df1:


    me "Oh… Ok. Sto ascoltando."


translate italian day3_evening_dv_13b39fa1:


    "Alisa si è preparata e ha iniziato a suonare."


translate italian day3_evening_dv_ce617998:


    "…"


translate italian day3_evening_dv_56072db3:


    dv "Così si fa!{w} Hai capito tutto?"


translate italian day3_evening_dv_2a7d102c:


    th "Potrei aver capito, ma probabilmente non sarei in grado di replicarlo."


translate italian day3_evening_dv_51584317:


    me "Come dire…"


translate italian day3_evening_dv_52e5d545:


    dv "Non preoccuparti, i risultati vengono con la pratica."


translate italian day3_evening_dv_8f9b4924:


    "Aveva indubbiamente ragione su questo punto, ma io ho sempre avuto problemi con la pratica nella mia vita."


translate italian day3_evening_dv_91dd4d67:


    me "Beh..."


translate italian day3_evening_dv_77bdacb3:


    "Mi ha dato la chitarra e ho provato a suonare il primo accordo."


translate italian day3_evening_dv_59bff6c6:


    "Era difficile, era come se le mie dita fossero legate da un filo e non volessero stare nella posizione giusta."


translate italian day3_evening_dv_008caeac:


    "In passato sapevo farlo meglio, ma in quel momento non sarei riuscito a suonare nemmeno «Brilla brilla la stellina»."


translate italian day3_evening_dv_dbeba143:


    dv "Allora?"


translate italian day3_evening_dv_2074dc26:


    me "Aspetta un momento..."


translate italian day3_evening_dv_4145963f:


    dv "Sembra che tu non possa diventare una rockstar!"


translate italian day3_evening_dv_91623ab5:


    "Ha riso bonariamente."


translate italian day3_evening_dv_4608b651:


    me "Forse è meglio così..."


translate italian day3_evening_dv_f6a527e5:


    "Ho sibilato, cercando invano di ricordare la sequenza delle corde da far vibrare."


translate italian day3_evening_dv_4366e696:


    me "Beh..."


translate italian day3_evening_dv_54c3617f:


    "La chitarra ha iniziato a fare dei suoni diabolici, che sembravano i gemiti di un dinosauro ferito."


translate italian day3_evening_dv_a4415adc:


    "Le dita della mia mano sinistra continuavano a sbagliare posizione e la mia mano destra non riusciva a reggere il ritmo."


translate italian day3_evening_dv_7c983f00:


    me "Potremmo chiamarlo rock progressivo."


translate italian day3_evening_dv_334edc91:


    dv "Progressivo? Bene, bene! Da' qua!"


translate italian day3_evening_dv_13705a4a:


    "Ha ripreso la chitarra. Tra le sue mani suonava in modo completamente diverso."


translate italian day3_evening_dv_11e95cca:


    "Per un momento mi sono sentito in imbarazzo a causa della mia goffaggine."


translate italian day3_evening_dv_b5823702:


    "Alisa magari non era al livello di un chitarrista professionista, ma comunque riusciva a suonare con sicurezza le melodie semplici."


translate italian day3_evening_dv_195bfa48:


    "Doveva essersi esercitata molto..."


translate italian day3_evening_dv_561d4770:


    dv "Stavolta hai capito?"


translate italian day3_evening_dv_d9ffec9a:


    "Ha terminato e mi ha ridato la chitarra."


translate italian day3_evening_dv_c97fadc0:


    me "Allora..."


translate italian day3_evening_dv_e9ad5daa:


    "La seconda volta mi è riuscita meglio, ma ero ancora lontano dal livello di Alisa."


translate italian day3_evening_dv_9422d1b3:


    "O da un livello decente."


translate italian day3_evening_dv_7c284714:


    "Ero consapevole di quanto fossi scarso rispetto a lei."


translate italian day3_evening_dv_c3b32ce7:


    th "Sembra così facile – basta suonare la canzone con tre accordi!"


translate italian day3_evening_dv_34a8a8e1:


    dv "Questa melodia però è un po' troppo semplice."


translate italian day3_evening_dv_ee31e548:


    "Ho messo da parte la chitarra."


translate italian day3_evening_dv_ed1bcf7d:


    "Alisa si è persa nei suoi pensieri per un momento."


translate italian day3_evening_dv_f28fc90f:


    dv "Persino un bambino dell'asilo saprebbe suonarla meglio!"


translate italian day3_evening_dv_e7c47ec2:


    me "Grazie,{w} io ci ho provato!"


translate italian day3_evening_dv_e131c7ad:


    dv "Ho notato!"


translate italian day3_evening_dv_bd64fe14:


    "Forse voleva dimostrare la sua evidente superiorità nel suonare la chitarra."


translate italian day3_evening_dv_a9370cae:


    me "Non tutti possono essere dei musicisti."


translate italian day3_evening_dv_26e899f1:


    dv "Già, non proprio..."


translate italian day3_evening_dv_343fa444:


    "Alisa ha guardato in alto verso il cielo."


translate italian day3_evening_dv_799ffa98:


    dv "Beh, tu non puoi esserlo, questo è sicuro!"


translate italian day3_evening_dv_38d5b464:


    me "E perché?"


translate italian day3_evening_dv_c3abde57:


    dv "Avresti dovuto sentirti!"


translate italian day3_evening_dv_43afff04:


    "Ho iniziato a pentirmi di essere venuto qui."


translate italian day3_evening_dv_67787d9f:


    th "Beh, non farci caso. Perché Alisa dovrebbe farmi questo?{w} È ovvio che io sia un terribile chitarrista."


translate italian day3_evening_dv_8c75e8f3:


    me "Certo.{w} Ma se mi esercitassi un po'..."


translate italian day3_evening_dv_26bc6b0e:


    dv "Allora, ti eserciterai?"


translate italian day3_evening_dv_968fea51:


    me "Non lo so... Forse."


translate italian day3_evening_dv_bca2071d:


    "Tuttavia la chitarra era l'ultimo dei miei problemi. Mi trovavo in uno strano mondo dal quale non potevo andarmene...{w} E ciò era più importante di una stupida discussione con Alisa."


translate italian day3_evening_dv_67458183:


    "Tutto a un tratto ho percepito una fastidiosa autocommiserazione, indifferenza, e il bisogno di lasciare questo posto, di dimenticarmi questa serata."


translate italian day3_evening_dv_ca179b6a:


    "Quei sentimenti mi hanno lasciato un'impressione sgradevole e un ardente desiderio di fare (o dire) qualcosa di stupido."


translate italian day3_evening_dv_3fcbf50b:


    me "Come se cambiasse qualcosa!"


translate italian day3_evening_dv_1145d96e:


    dv "Eh?"


translate italian day3_evening_dv_c372f019:


    me "Se imparassi a suonare la chitarra!"


translate italian day3_evening_dv_9d34d000:


    dv "Di certo non ce la faresti! La faresti strimpellare per un paio di giorni e poi l'abbandoneresti in un angolo."


translate italian day3_evening_dv_e7eaa70e:


    "Le parole di Alisa mi hanno trafitto il cuore."


translate italian day3_evening_dv_5187f093:


    me "Sai che ti dico?!"


translate italian day3_evening_dv_35264e84:


    "Oh beh, perché avrebbe dovuto {i}saperlo{/i}? Le importava forse qualcosa?"


translate italian day3_evening_dv_05bd68b6:


    "Stavo reagendo in questo modo solo perché aveva detto la verità, e perché era stata proprio Alisa a dirmelo in faccia?"


translate italian day3_evening_dv_64d153d7:


    me "Una persona non può essere in grado di fare bene ogni cosa!"


translate italian day3_evening_dv_b4c62b8a:


    dv "Probabilmente no...{w} Ma tu dovresti saper fare bene almeno una cosa."


translate italian day3_evening_dv_6528a29b:


    me "Quindi mi stai dicendo che io non so fare niente?"


translate italian day3_evening_dv_a44772be:


    dv "E che ne so?"


translate italian day3_evening_dv_92d27981:


    me "Sai, io..."


translate italian day3_evening_dv_21c57864:


    th "Io cosa?"


translate italian day3_evening_dv_380c1152:


    th "Dovrei dirle delle mie abilità al computer?{w} O dei libri che ho letto o i film che ho visto?"


translate italian day3_evening_dv_6660ca80:


    me "Ah bene, questa conversazione non ha senso."


translate italian day3_evening_dv_4a20a990:


    dv "Inutile dirlo."


translate italian day3_evening_dv_d48bc21c:


    me "Tu pensi solo a divertirti!"


translate italian day3_evening_dv_cb92aa34:


    dv "E tu non fai altro che lamentarti!"


translate italian day3_evening_dv_297beca3:


    me "Va bene, basta così!"


translate italian day3_evening_dv_a08e0ef8:


    "Sono saltato giù dal palco e me ne sono andato da quel luogo."


translate italian day3_evening_dv_8bd551d4:


    dv "Bene, vattene allora!"


translate italian day3_evening_dv_11b833b9:


    "Ho sentito Alisa gridare dietro di me."


translate italian day3_evening_dv_9d84e4e8:


    me "Sto già andando!"


translate italian day3_evening_dv_2eb0d079:


    "Ho sibilato."


translate italian day3_evening_dv_35bec68e:


    "Il percorso mi ha riportato alla piazza."


translate italian day3_evening_dv_51f9be11:


    "Slavya era al centro di essa, e stava spazzando l'area."


translate italian day3_evening_dv_d2abd3ca:


    sl "Oh, Semyon, ciao! Credevo che tutti stessero già dormendo."


translate italian day3_evening_dv_4477db54:


    me "No... E tu che stai facendo?"


translate italian day3_evening_dv_753b1df5:


    sl "Sto pulendo qui dopo il ballo. Perché non sei venuto?"


translate italian day3_evening_dv_79a8a3cb:


    me "Ecco..."


translate italian day3_evening_dv_4bd32d93:


    sl "E dove sei stato?"


translate italian day3_evening_dv_bbc9f7af:


    me "Io..."


translate italian day3_evening_dv_66b39d2d:


    "Non avevo la minima intenzione di dire a Slavya di come mi ero ridicolizzato al palco da concerto con la chitarra davanti ad Alisa, mentre l'intero Campo si stava divertendo alla piazza."


translate italian day3_evening_dv_b498ffb4:


    me "Ho fatto una passeggiata."


translate italian day3_evening_dv_527c6bb3:


    sl "Ok... Ma saresti dovuto venire. È stato divertente."


translate italian day3_evening_dv_77b6031e:


    me "Sono contento che tu ti sia divertita."


translate italian day3_evening_dv_a7c92b66:


    "Ho sudato come un maiale oggi, a causa delle mie preoccupazioni, o semplicemente perché la notte era calda."


translate italian day3_evening_dv_c79602cb:


    me "Ehi, sai forse dove potrei farmi un bagno?"


translate italian day3_evening_dv_46023748:


    sl "Certo. Vai dritto, poi un po' a sinistra sul sentiero, poi a destra, e troverai la sauna."


translate italian day3_evening_dv_3820771a:


    me "..."


translate italian day3_evening_dv_56fc315a:


    sl "Vuoi che ti accompagni?"


translate italian day3_evening_dv_90d3bf06:


    me "No, grazie. Me la caverò!"


translate italian day3_evening_dv_8a838751:


    "Non volevo proprio disturbarla ulteriormente."


translate italian day3_evening_dv_844a11d0:


    "Mi sono incamminato verso la direzione che mi aveva indicato Slavya."


translate italian day3_evening_dv_7fd553fe:


    "Mi prudeva tutto."


translate italian day3_evening_dv_1ac85161:


    th "Come ho fatto a non accorgermene prima?"


translate italian day3_evening_dv_b2840d09:


    "E piuttosto, cos'altro ancora non avevo notato?"


translate italian day3_evening_dv_19111076:


    th "Se le cose continueranno ad andare in questa maniera, mi toccherà restare in questo mondo per sempre."


translate italian day3_evening_dv_cd1088c8:


    "Ho rabbrividito al solo pensiero."


translate italian day3_evening_dv_ea4df3bd:


    "E anche per il fatto che non fossi per nulla terrorizzato, né spaventato."


translate italian day3_evening_dv_d95e35f6:


    "Proprio come se fosse un normale flusso di eventi."


translate italian day3_evening_dv_0ba45212:


    "Ho intravisto un edificio dietro agli alberi."


translate italian day3_evening_dv_4c8601db:


    th "E perché i pionieri si lavano proprio qui?"


translate italian day3_evening_dv_c7b24c86:


    th "In effetti Olga Dmitrievna aveva menzionato qualcosa riguardo a certi lavori alle docce."


translate italian day3_evening_dv_7cf75d85:


    me "Ma adesso non importa..."


translate italian day3_evening_dv_c5ff1076:


    "Dentro c'erano dei saponi e degli asciugamani puliti, e quindi mi sono lavato rapidamente, sono uscito e ho fatto un grande respiro, riempiendomi della fresca aria notturna."


translate italian day3_evening_dv_9f7fc9a5:


    th "Ah, è così bello quando la pulizia fisica porta alla pulizia spirituale!"


translate italian day3_evening_dv_00142a80:


    "Tutti i miei problemi, seppur non spariti del tutto, sono diventati meno opprimenti. È come se l'acqua non avesse lavato solo lo sporco, ma anche l'ansia dalla mia mente."


translate italian day3_evening_dv_9b68d7c1:


    "Ho sbadigliato e mi sono incamminato verso la casetta della leader, strofinandomi gli occhi."


translate italian day3_evening_dv_8c18ef81:


    "Non sono riuscito a fare nemmeno pochi passi, quando Alisa è saltata fuori dai cespugli."


translate italian day3_evening_dv_e58bfb0c:


    dv "Tu... Che ci fai qui?"


translate italian day3_evening_dv_aa2954ff:


    me "Mi sono lavato."


translate italian day3_evening_dv_e2ccd354:


    "È ritornata in sé e ha parlato di nuovo."


translate italian day3_evening_dv_d87c29ac:


    dv "E?"


translate italian day3_evening_dv_ad02f53a_2:


    me "E cosa?"


translate italian day3_evening_dv_410dff3b:


    dv "Che ne so?!"


translate italian day3_evening_dv_b7d92302:


    me "Allora perché parli se non lo sai?"


translate italian day3_evening_dv_0adca3fb:


    "Tutta la mia pace interiore è svanita in un istante, per fare spazio alla stanchezza e all'irritazione."


translate italian day3_evening_dv_e44bfa1d:


    me "Ok, allora se non ti spiace..."


translate italian day3_evening_dv_a5e7a3c6:


    dv "Aspetta..."


translate italian day3_evening_dv_f3274282:


    me "Che c'è ancora?"


translate italian day3_evening_dv_761956ea:


    dv "Giù al palco, se hai pensato che io... beh... non volevo offenderti..."


translate italian day3_evening_dv_1f988280:


    me "È tutto a posto, non mi sono offeso. Non sono capace di suonare la chitarra. Nulla di speciale, molte persone non la sanno suonare."


translate italian day3_evening_dv_403f582b:


    "Ho sentito un'evidente nota di irritazione nella mia voce, ma non ho potuto farci niente. Avrei voluto dirle tutto in faccia qui, adesso."


translate italian day3_evening_dv_5b7b1ded:


    dv "Esatto, non sei capace!"


translate italian day3_evening_dv_aee5d595:


    "Alisa ovviamente stava trattenendo le risate."


translate italian day3_evening_dv_d5503c43:


    me "Ok, ne ho abbastanza di te!"


translate italian day3_evening_dv_a3a48b46:


    "Mi ha fatto proprio incazzare."


translate italian day3_evening_dv_423dbcda:


    "In un altro momento avrei iniziato a gridarle addosso...{w} O forse peggio."


translate italian day3_evening_dv_f72c72fe:


    dv "Aspetta."


translate italian day3_evening_dv_d177607c:


    me "Vuoi lasciarmi in pace?!"


translate italian day3_evening_dv_cc1a995c:


    "Alisa mi ha seguito senza provare a fermarmi."


translate italian day3_evening_dv_0cc8c845:


    dv "Davvero, non volevo offenderti..."


translate italian day3_evening_dv_2cebe654:


    "Una forza sconosciuta mi ha fatto voltare verso di lei."


translate italian day3_evening_dv_d09cc761:


    me "Va bene. E adesso posso andare?"


translate italian day3_evening_dv_4907e592:


    dv "Sì... Certo..."


translate italian day3_evening_dv_664f2019:


    "Sotto il chiaro di luna Alisa sembrava molto bella, con una lieve espressione di colpevolezza sul suo viso."


translate italian day3_evening_dv_aa3cefba:


    th "Se solo riuscisse a tenere chiusa quella bocca..."


translate italian day3_evening_dv_74c9cbb9:


    dv "Perché stai sogghignando?"


translate italian day3_evening_dv_d857ef67:


    th "Anche se..."


translate italian day3_evening_dv_356af705:


    me "Niente. Stavo solo pensando che se tu fossi un po' più gentile..."


translate italian day3_evening_dv_0fd482e1:


    dv "Allora cosa?"


translate italian day3_evening_dv_389e70d3:


    "È meglio non dire certe cose, le persone potrebbero farsi strane idee, a seconda delle loro capacità intellettive."


translate italian day3_evening_dv_a0eb57ae:


    me "Allora niente."


translate italian day3_evening_dv_6f263bd2:


    dv "Finisci la frase!"


translate italian day3_evening_dv_c27026d4:


    me "Non c'è niente da finire. Sono molto stanco e voglio andare a dormire."


translate italian day3_evening_dv_d69aa454:


    dv "Fermo lì!"


translate italian day3_evening_dv_301d2f4b:


    me "Non costringermi a fuggire da te!"


translate italian day3_evening_dv_5e3d8340:


    dv "Come se tu potessi farlo!"


translate italian day3_evening_dv_60769081:


    "Poteva aver ragione. Ero già esausto a causa di questa giornata. E, ovviamente, non avevo intenzione di correre dopo essermi appena lavato."


translate italian day3_evening_dv_ba176b89:


    dv "Allora, cosa?"


translate italian day3_evening_dv_ad02f53a_3:


    me "Cosa «cosa»?"


translate italian day3_evening_dv_a8a0c403:


    dv "Se solo fossi più gentile, allora cosa?"


translate italian day3_evening_dv_ef8f1d34:


    me "Provaci e lo scoprirai."


translate italian day3_evening_dv_279c0b72:


    dv "E perché dovrei fare qualcosa per uno scemo come te...?"


translate italian day3_evening_dv_fb217e62:


    me "Allora non farlo. Io che c'entro?"


translate italian day3_evening_dv_772db0f1:


    "Ho sospirato e mi sono trascinato verso la piazza."


translate italian day3_evening_dv_227a4155:


    dv "E per quanto riguarda te?"


translate italian day3_evening_dv_ad02f53a_4:


    me "Cosa?"


translate italian day3_evening_dv_dd2be400:


    "Le ho risposto, senza voltarmi."


translate italian day3_evening_dv_7bc2ed19:


    dv "Beh, se io lo facessi, allora tu...?"


translate italian day3_evening_dv_a70104fb:


    me "Non so di cosa tu stia parlando, ma se ti fa piacere facciamo finta che anche io ti abbia detto che «lo farei»."


translate italian day3_evening_dv_9a1f6cf1:


    dv "Stronzo!"


translate italian day3_evening_dv_779b2571:


    "Mi ha gridato Alisa, andandosene verso la sauna."


translate italian day3_evening_dv_76cd70be:


    "La piazza brillava pulita come le strade tedesche, che in quegli anni erano lavate con lo shampoo, o così dicevano."


translate italian day3_evening_dv_ac1742ca:


    th "Grazie mille, Slavya!"


translate italian day3_evening_dv_881163f0:


    th "E io non ho fatto altro che litigare con Alisa..."


translate italian day3_evening_dv_2a8b99c4:


    th "A quale scopo?"


translate italian day3_evening_dv_7fd16daf:


    th "Come se fosse stato davvero necessario!"


translate italian day3_evening_dv_ea92f05b:


    th "E quel suo comportamento..."


translate italian day3_evening_dv_6c061942:


    "In qualsiasi altra circostanza avrei pensato di piacere ad Alisa e che lei stesse nascondendo i propri sentimenti dietro alla sua maleducazione."


translate italian day3_evening_dv_01f29ad6:


    th "Ma adesso non importa comunque!"


translate italian day3_evening_dv_fa76e542:


    "Relazioni, amore, o quant'altro..."


translate italian day3_evening_dv_12677de3:


    th "Lei potrebbe non essere nemmeno umana!"


translate italian day3_evening_dv_e9aaa3dd:


    th "E se invece lei fosse davvero umana, e tutto questo stesse accadendo nel mondo {i}reale{/i}...?"


translate italian day3_evening_dv_92236afc:


    "Ho liberato la mente da quei pensieri e mi sono diretto verso la casetta della leader. La stanchezza ha preso il sopravvento su di me."


translate italian day3_evening_dv_2579577e:


    "Un ostacolo invisibile mi ha bloccato davanti alla porta."


translate italian day3_evening_dv_71e467af:


    th "Olga Dmitrievna di sicuro vorrà sapere dove sono stato e perché non sono venuto al ballo."


translate italian day3_evening_dv_aabe8f1c:


    th "Mi dovrò preparare per l'interrogatorio."


translate italian day3_evening_dv_bcc9746d:


    "Considerando l'assenza di Alisa al ballo, non sarebbe stato difficile intuire che l'avevamo saltato insieme."


translate italian day3_evening_dv_5df6ba98:


    th "D'altra parte, dovrei forse dirle che invece di andare al ballo Alisa si è esercitata con la chitarra, e io invece ero occupato con la mia introspezione?"


translate italian day3_evening_dv_4739fcde:


    th "Niente di speciale, a dire il vero..."


translate italian day3_evening_dv_8a5e9765:


    "Non importava il perché io me ne fossi andato."


translate italian day3_evening_dv_0b609861:


    th "Quindi…"


translate italian day3_evening_dv_abdca19a:


    "Allo stesso momento però ho pensato che forse sarebbe stato meglio tenere la bocca chiusa."


translate italian day3_evening_dv_bdf7e286:


    th "Beh, deciderò quando sarà il momento."


translate italian day3_evening_dv_ed6ae9d5:


    "Ho sospirato e ho tirato la maniglia."


translate italian day3_evening_dv_e96fefb3:


    "Olga Dmitrievna mi stava aspettando, con indosso l'abito da sera."


translate italian day3_evening_dv_47f7a7d5:


    mt "Allora Semyon, dove sei stato?"


translate italian day3_evening_dv_48199bb7:


    me "Io…"


translate italian day3_evening_dv_ae28ee74:


    me "A dire il vero sono stato al palco da concerto con Alisa."


translate italian day3_evening_dv_baaff23f:


    mt "E cosa ci facevate là?"


translate italian day3_evening_dv_0c8efc3f:


    me "Abbiamo suonato la chitarra…"


translate italian day3_evening_dv_30a21108:


    me "Io…{w} stavo solo facendo un giro."


translate italian day3_evening_dv_b166a941:


    mt "Da solo?"


translate italian day3_evening_dv_9513cd87:


    me "Sì…"


translate italian day3_evening_dv_db050801:


    mt "Va bene, non importa. Perché non sei venuto al ballo?"


translate italian day3_evening_dv_52fe9abf:


    me "Beh, francamente, non mi piace ballare…"


translate italian day3_evening_dv_ff3e3b04:


    mt "E allora?{w} Quello era un evento speciale del Campo, tutti i pionieri avrebbero dovuto partecipare!"


translate italian day3_evening_dv_1b595140:


    "Mi ha guardato intensamente."


translate italian day3_evening_dv_3607aec3:


    mt "Avresti potuto anche stare seduto in silenzio, se proprio volevi."


translate italian day3_evening_dv_2993aaa0:


    me "Mi dispiace…"


translate italian day3_evening_dv_ed0914b5:


    "Ho sospirato."


translate italian day3_evening_dv_dbc34155:


    mt "Ok."


translate italian day3_evening_dv_d14fc77f:


    "Olga Dmitrievna ha addolcito lo sguardo."


translate italian day3_evening_dv_41704f8b:


    mt "E ora a letto, presto!"


translate italian day3_evening_dv_9c792872:


    th "La tempesta è passata."


translate italian day3_evening_dv_ce617998_1:


    "…"


translate italian day3_evening_dv_5649b7bb:


    "Mi rigiravo nel letto ma non riuscivo a dormire."


translate italian day3_evening_dv_7bb07423:


    "La mia mente era colma di pensieri riguardanti la musica, chitarre, e Alisa…"


translate italian day3_evening_dv_1a6a274a:


    th "Mi chiedo, potrei diventare un musicista professionista?"


translate italian day3_evening_dv_c5ba79b7:


    th "E comunque, lo voglio davvero?"


translate italian day3_evening_dv_24a063fd:


    th "Perché dovrei {i}volerlo{/i}...? Certamente sarebbe divertente, ma niente di più."


translate italian day3_evening_dv_bc447272:


    th "O forse è Alisa il motivo?"


translate italian day3_evening_dv_66b20a8a:


    th "Lei è proprio un vampiro energetico – ti succhia tutta l'energia dal corpo!"


translate italian day3_evening_dv_ce617998_2:


    "…"


translate italian day3_evening_dv_b5247e67:


    "I miei occhi hanno iniziato a chiudersi, e in un istante mi sono addormentato."


translate italian day3_fail_6f4c676f:


    "Slavya è ritornata alla pista da ballo."


translate italian day3_fail_5b8f0841:


    "Sono rimasto seduto ancora un po' e poi mi sono allontanato con cautela, assicurandomi che nessuno mi vedesse."


translate italian day3_fail_a20cefa7:


    "..."


translate italian day3_fail_ab41d603:


    "Non avevo proprio voglia di vedere nessuno dopo una tale serata «di successo»."


translate italian day3_fail_57cdff00:


    "Il posto più tranquillo qui era la fermata dell'autobus, che probabilmente non sarebbe stata visitata mai più dal bus 410…"


translate italian day3_fail_5c5d8e3f:


    "Ma!{w} Mi sono lasciato sfuggire un urlo."


translate italian day3_fail_32f73f0c:


    "C'era un bus proprio di fronte a me.{w} Esattamente come quello del primo giorno."


translate italian day3_fail_193dd370:


    "Ero pietrificato."


translate italian day3_fail_6770889d:


    me "Come, cosa, come, perché...?"


translate italian day3_fail_83e56da8:


    "Subito mi sono tornate in mente tutte le mie teorie riguardo a come io avessi raggiunto questo Campo."


translate italian day3_fail_8d9231e3:


    "Mi sono reso conto che in questi ultimi giorni mi ero abituato talmente tanto alla vita del luogo, che avevo iniziato a scordarmi che tutte le cose che accadevano qui non erano affatto normali."


translate italian day3_fail_dada5103:


    "Me ne stavo lì a guardare quel maledetto Ikarus. Poi mi sono dato uno schiaffo per assicurarmi che non fosse solo un'illusione. Il bus era ancora lì."


translate italian day3_fail_28a25175:


    th "Se è qui significa che è ora di tornare a casa!"


translate italian day3_fail_b7190439:


    me "Sayonara, pionieri!"


translate italian day3_fail_8ff4f863:


    "Mi sono precipitato verso l'entrata…"


translate italian day3_fail_a0123424:


    "E mi sono ritrovato per terra.{w} Il naso mi faceva un male cane."


translate italian day3_fail_9b2c898b:


    "Mi sono alzato in piedi e ho cercato di capire cosa fosse successo."


translate italian day3_fail_929396d1:


    th "Probabilmente ho sbattuto contro qualcosa..."


translate italian day3_fail_8d0f3707:


    "Il bus era più che reale al tatto."


translate italian day3_fail_b50ff2e2:


    "Ho provato ad allungare la mano oltre la porta…{w} Ma c'era una sorta di muro invisibile."


translate italian day3_fail_ca7b4ca3:


    "Sono stato sopraffatto da una paura quasi bestiale."


translate italian day3_fail_2dda9f46:


    "Paura di tutto – del Campo, dei suoi abitanti e di questo bus."


translate italian day3_fail_6d8da21b:


    th "Ma come diavolo sono finito qui?"


translate italian day3_fail_835b1bec:


    th "Cos'è questo maledetto Ikarus dove non riesco a entrare?{w} Perché sta accadendo proprio a me?!"


translate italian day3_fail_908aa7d1:


    "Improvvisamente ha soffiato un forte vento, che mi ha fatto barcollare."


translate italian day3_fail_dea6ec52:


    "Mi sono girato e ho visto un foglietto di carta sotto alla ruota dell'autobus."


translate italian day3_fail_2ef2aa5c:


    "C'era scritto qualcosa."


translate italian day3_fail_3b835d43:


    "{i}Sei qui per un motivo.{/i}"


translate italian day3_fail_b1b730db:


    "Quella grafia irregolare sembrava familiare."


translate italian day3_fail_a9d82379:


    th "Sono sicuro di averla già vista da qualche parte…"


translate italian day3_fail_28815dfd:


    "E poi mi sono reso conto!{w} Ho raccolto un piccolo pezzo di carbone da terra e ho scarabocchiato la stessa frase sul retro del foglietto."


translate italian day3_fail_aa5a386f:


    "La grafia era identica!"


translate italian day3_fail_078ccf4d:


    "La mia mente si è schiarita."


translate italian day3_fail_e5eddf45:


    th "Mi sono mandato un messaggio dal futuro! È proprio così!"


translate italian day3_fail_b57a15b1:


    th "Anzi no.{w} Dal passato…"


translate italian day3_fail_38859e59:


    th "Dannazione! Non ci capisco niente!"


translate italian day3_fail_f958927a:


    "In ogni caso, era proprio la mia grafia."


translate italian day3_fail_6844d216:


    "Sicuramente non sarebbe stata difficile da replicare, ma ero certo che quel messaggio fosse stato scritto da me."


translate italian day3_fail_04f28a5d:


    "Dopo aver rigirato quel foglietto tra le mie mani, ho deciso di provare ad entrare nuovamente nel bus."


translate italian day3_fail_e5a11986:


    "Il muro invisibile era ancora lì."


translate italian day3_fail_54febcf3:


    "Ho girato tutt'attorno all'Ikarus, ho picchiettato le sue ruote, ho sbirciato dai finestrini."


translate italian day3_fail_8425e955:


    "Tutto sembrava perfettamente normale.{w} Ma in realtà non lo era."


translate italian day3_fail_5386dfcb:


    "I sassi grossi rimbalzavano sui vetri senza causare alcun danno."


translate italian day3_fail_53e2e58a:


    "Nessun effetto, nemmeno un graffio!"


translate italian day3_fail_78354201:


    "Mi sono seduto sul marciapiede, sospirando debolmente."


translate italian day3_fail_35da3684:


    "Pensandoci, quel foglietto di carta alludeva a qualcosa…"


translate italian day3_fail_751d8d30:


    "E sembrava che qui non ci fosse alcun pericolo evidente."


translate italian day3_fail_105fc865:


    mt "Semyon!"


translate italian day3_fail_07e68b3f:


    th "Pare che la leader mi stia cercando."


translate italian day3_fail_06c92db3:


    th "Interessante, chissà cosa dirà nel vedere questo bus?{w} Insisterà ancora riguardo al fatto che «qui non passano bus per l'intera settimana»?"


translate italian day3_fail_c3a1e6ea:


    "Mi sono alzato rapidamente e sono corso verso Olga Dmitrievna."


translate italian day3_fail_d4b7729b:


    me "E allora che mi dice di quello?!"


translate italian day3_fail_dc7df731:


    "Ho sbottato trionfante, agitando la mano verso la strada."


translate italian day3_fail_9de9961c:


    mt "Dire di cosa?"


translate italian day3_fail_afc5a641:


    "Mi ha risposto sorpresa."


translate italian day3_fail_47eaf14e:


    "Mi sono girato."


translate italian day3_fail_26562526:


    "Il bus era sparito…{w} allo stesso modo in cui era apparso."


translate italian day3_fail_f0ef1916:


    "Il grido trionfante mi si è spento nella gola."


translate italian day3_fail_0c4809e3:


    mt "Avanti, è ora di dormire. Andiamo!"


translate italian day3_fail_639b476b:


    me "Ma... ma..."


translate italian day3_fail_6334b91e:


    mt "Cosa?"


translate italian day3_fail_be80cde7:


    me "Il bus! C'era un bus proprio lì! Appena un momento fa!"


translate italian day3_fail_ef1ece89:


    mt "È impossibile."


translate italian day3_fail_1ee77ef5:


    "Mi ha detto tranquillamente."


translate italian day3_fail_ed2597b4:


    "Ho fissato il volto di Olga Dmitrievna. O era un'ottima attrice, oppure davvero non aveva visto nulla."


translate italian day3_fail_65b9f63e:


    "Allora forse ero io che vedevo le cose?"


translate italian day3_fail_41e67f91:


    th "Ma non può essere, ho visto davvero quel maledetto 410!"


translate italian day3_fail_09631ec6:


    me "Basta mentire!"


translate italian day3_fail_5788b9db:


    "Le ho detto a bassa voce."


translate italian day3_fail_5afe237b:


    mt "Semyon, non capisco."


translate italian day3_fail_c27faba3:


    me "La smetta di dire bugie! C'era un bus! È stata lei, mi costringe a stare qui! Perché?!"


translate italian day3_fail_9e2a7291:


    "Ho stretto i denti, cercando di parlare in tono calmo."


translate italian day3_fail_a34667cc:


    mt "Mi stai spaventando... È ora di andare a letto!"


translate italian day3_fail_f4cdc187:


    th "Non mi dirà niente come al solito."


translate italian day3_fail_1665b82c:


    "Però davvero, avevo molta voglia di dormire."


translate italian day3_fail_c0511a05:


    "Ho camminato rapidamente passando davanti alla leader, senza degnarla di uno sguardo."


translate italian day3_fail_01dfa1f8:


    "Non sono riuscito a prendere sonno per un po' di tempo, e solo quel foglietto di carta spiegazzato con la scritta «Sei qui per un motivo» mi ha ricordato che gli eventi di questi tre giorni erano reali."
# Decompiled by unrpyc: https://github.com/CensoredUsername/unrpyc
